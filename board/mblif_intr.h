/**
  **************** (C) COPYRIGHT 2011 SCT Performance, LLC *********************
  * File Name          : mblif_intr.h
  * Device / System    : SCT products using the "CommonCode" platform
  * Author             : Quyen Leba, Patrick Downs
  *
  * Version            : 1 
  * Date               : 01/10/2013
  * Description        : 
  *                    : 
  *
  *
  *
  ******************************************************************************
  */

#ifndef __MBLIF_INTR_H
#define __MBLIF_INTR_H

#include <arch/gentype.h>

void mblif_intr_init();
u8 mblif_intr_link(u8 cmd, u16 opcode, u8 *data, u16 *datalength);

#endif  //__MBLIF_INTR_H
