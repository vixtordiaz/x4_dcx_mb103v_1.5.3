/**
  **************** (C) COPYRIGHT 2011 SCT Performance, LLC *********************
  * File Name          : datalogfeatures_bt11.c
  * Device / System    : SCT products using the "CommonCode" platform
  * Author             : Quyen Leba
  *
  * Version            : 1
  * Date               : 05/02/2011
  * Description        :
  *                    :
  *
  *
  * History            : 05/02/2011 M. Davis
  *                    :   Add SCT Copyright header,
  *
  ******************************************************************************
  */

#include <board/genplatform.h>
#include "datalogfeatures.h"

//from datalogfeatures.c
extern const DlxBlock DefaultDatalogFeatureBlock_AIN1;
extern const DlxBlock DefaultDatalogFeatureBlock_AIN2;
extern const DlxBlock DefaultDatalogFeatureBlock_AIN3;
extern const DlxBlock DefaultDatalogFeatureBlock_AIN4;
extern const DlxBlock DefaultDatalogFeatureBlock_AIN5_VBREV1;
extern const DlxBlock DefaultDatalogFeatureBlock_AIN5_VBREV2;
extern const DlxBlock DefaultDatalogFeatureBlock_AIN6_VBREV1;
extern const DlxBlock DefaultDatalogFeatureBlock_AIN6_VBREV2;

//iTSX w/ VB Rev B.1
#define DATALOGFEATURE_COLLECTION_A_COUNT       2
const DlxBlock *DefaultDatalogFeatureBlockCollectionA[DATALOGFEATURE_COLLECTION_A_COUNT] =
{
    &DefaultDatalogFeatureBlock_AIN5_VBREV1,
    &DefaultDatalogFeatureBlock_AIN6_VBREV1
};

//iTSX w/ VB Rev B.[2..3]
#define DATALOGFEATURE_COLLECTION_B_COUNT       6
const DlxBlock *DefaultDatalogFeatureBlockCollectionB[DATALOGFEATURE_COLLECTION_B_COUNT] =
{
    &DefaultDatalogFeatureBlock_AIN1,
    &DefaultDatalogFeatureBlock_AIN2,
    &DefaultDatalogFeatureBlock_AIN3,
    &DefaultDatalogFeatureBlock_AIN4,
    &DefaultDatalogFeatureBlock_AIN5_VBREV2,
    &DefaultDatalogFeatureBlock_AIN6_VBREV2
};

/**
 *  datalogfeatures_getInputCount
 *  
 *  @brief Determine how many analog inputs available
 *
 *  @param[in] vbrev VB board revision
 *
 *  @retval u8 number of analog inputs
 *  
 *  @authors Quyen Leba
 */
u8 datalogfeatures_getInputCount(u8 vbrev)
{
    u8  count;

    if (vbrev <= 1)
    {
        count = DATALOGFEATURE_COLLECTION_A_COUNT;
    }
    else
    {
        count = DATALOGFEATURE_COLLECTION_B_COUNT;
    }

    return count;
}

/**
 *  datalogfeatures_getInputCollection
 *  
 *  @brief Get analog input collection by mb & vb revisions
 *
 *  @param[in] vbrev VB board revision
 *
 *  @retval DlxBlock**
 *  
 *  @authors Quyen Leba
 */
const DlxBlock** datalogfeatures_getInputCollection(u8 vbrev)
{
    if (vbrev <= 1)
    {
        return DefaultDatalogFeatureBlockCollectionA;
    }
    else
    {
        return DefaultDatalogFeatureBlockCollectionB;
    }
}

/**
 *  datalogfeatures_calc_ain_7
 *  
 *  @brief Calculate input voltage of AIN7
 *
 *  @param[in] adccount 4095 for 3.3V
 *
 *  @retval DlxBlock**
 *  
 *  @authors Quyen Leba
 */
float datalogfeatures_calc_ain_7(u32 adccount)
{
    //doesn't have AIN7
    return -1;
}

/**
 *  datalogfeatures_calc_ain_8
 *  
 *  @brief Calculate input voltage of AIN8
 *
 *  @param[in] adccount 4095 for 3.3V
 *
 *  @retval DlxBlock**
 *  
 *  @authors Quyen Leba
 */
float datalogfeatures_calc_ain_8(u32 adccount)
{
    //doesn't have AIN8
    return -1;
}
