/**
 *  ************* Copyright 2013 SCT Performance, LLC ******
 *  @file properties_ab_bt11.c
 *  @brief Handles the properties data structure for the iTSX
 *         app board.
 *  
 *  @authors Patrick Downs
 *  @date 11/06/2013
 *  ********************************************************
 */

#include <string.h>
#include <common/statuscode.h>
#include <board/bootloader_ab_bt11.h>
#include "properties_ab_bt11.h"

//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
void properties_ab_init()
{
}

//------------------------------------------------------------------------------
// Get AppBoard properties
// Outputs: u8  *info (vb properties; must be able to store 48 bytes)
//          u32 *info_length
// Return:  u8  status
// Engineer: Quyen Leba
// Note: AppBoard firmware is the BT11 firmware
//------------------------------------------------------------------------------
u8 properties_ab_getinfo(u8 *info, u32 *info_length)
{
    PROPERTIES_AB_INFO properties_ab_info;
    u8  status;

    if (!info || !info_length)
    {
        return S_INPUT;
    }
    *info_length = 0;
    
    memset((char*)&properties_ab_info,0,sizeof(properties_ab_info));
    properties_ab_info.operating_mode = 0;
    properties_ab_info.failsafeboot_version = 0;
    properties_ab_info.mainboot_version = 0;
    properties_ab_info.app_signature = 0;
    properties_ab_info.app_version = 0;
    properties_ab_info.code_sector_size = 1024;
    properties_ab_info.board_rev = 0;
    properties_ab_info.secboot_startsector = 0xFF;  //invalid
    properties_ab_info.secboot_maxcodesize = 0;     //invalid
    properties_ab_info.app_startsector = 0;
    properties_ab_info.app_maxcodesize = 256*1024;

    status = bootloader_ab_getoperatingmode(&properties_ab_info.operating_mode);
    if (status == S_SUCCESS)
    {
        switch(properties_ab_info.operating_mode)
        {
        case 0x6E2A7315:
        case 0x2D81F35A:
            break;
        default:
            status = bootloader_ab_getversion(&properties_ab_info.app_version);
            break;
        }
    }

    if (status == S_SUCCESS)
    {
        memcpy((char*)info,(char*)&properties_ab_info,sizeof(properties_ab_info));
        *info_length = sizeof(properties_ab_info);
    }
    
    return status;
}
