/**
  **************** (C) COPYRIGHT 2011 SCT Performance, LLC *********************
  * File Name          : timer.h
  * Device / System    : SCT products using the "CommonCode" platform
  * Author             : Quyen Leba
  *
  * Version            : 1 
  * Date               : 05/02/2011
  * Description        : 
  *                    : 
  *
  *
  * History            : 05/02/2011 M. Davis
  *                    :   Add SCT Copyright header,
  *
  ******************************************************************************
  */

#ifndef __TIMER_H
#define __TIMER_H

#include <arch/gentype.h>

typedef void (*TimerTaskFunc)(void *priv);

#define TASKTIMER_ONESHOT                       TRUE
#define TASKTIMER_REPEAT                        FALSE

#define TASKTIMER_TASK_COUNT_MAX                4
typedef enum
{
    TaskTimerTaskType_None                      = 0,
    TaskTimerTaskType_TesterPresent             = 1,
    TaskTimerTaskType_PreventAppTimeout         = 2,
    TaskTimerTaskType_SignalFullpowerMode       = 3,
}TaskTimerTaskType;

typedef struct
{
    u8  app_timeout     : 1;
    u8  board_timeout   : 1;
    u8  reserved        : 6;
    u64 msec;
    u32 app_current_ms;
    u32 app_timeout_ms;
    u32 board_timeout_ms;
    struct
    {
        TimerTaskFunc taskfunc;
        void *taskpriv;
        u32 timeout_ms;
        u32 current_ms;
        TaskTimerTaskType tasktype;
        struct
        {
            u8  oneshot     : 1;
            u8  abort       : 1;
            u8  reserved    : 6;
        }control;
    }task[TASKTIMER_TASK_COUNT_MAX];
}TIMER_INFO;
extern TIMER_INFO timer_info;

enum
{
    TIMER_PRESCALE_FAST,
    TIMER_PRESCALE_REGULAR,
    TIMER_PRESCALE_SLOW,
};

void timer_init();
void timer_task_null_func(void *priv);
void timer1_init();
void timer1_deinit();
u16 timer1_getcounter();
void timer1_resetcounter();
void timer1_msec_counter_update();

void timer2_init();
void timer2_deinit();
u16 timer2_getcounter();
void timer2_resetcounter();
void timer2_control(FunctionalState state);
void timer2_pwm(u32 full_period, u32 high_period);

/*
void timer3_init();
void timer3_intr_handler();
*/

void timer4_init(u8 prescale_type);
void timer4_pwm_duty_cycle(u8 ch, u8 duty_cycle);

void timer5_init();
void timer5_wait_ms(u32 time_ms);
void timer5_wait_us(u32 time_us);
/*
void timer5_intr_handler();
*/

void timer7_init();
u8 timer7_setup(u32 time_ms, bool oneshot, TaskTimerTaskType tasktype,
                  TimerTaskFunc taskfunc, void *taskpriv);
void timer7_terminate_alltasks();
void timer7_intr_handler();

//this is a free running time counter for Board and App Level Timeouts
#define timeout_counter_init()              timer1_init()
#define timeout_counter_restart()           timer1_resetcounter();timer_info.msec = 0
#define timeout_counter_gettime_in_msec()   (timer_info.msec)
#define timeout_counter_gettime_in_sec()    (timer_info.msec/1000)
#define app_timeout_set(ms)                 timer_info.app_timeout = 0;timer_info.app_timeout_ms = (ms+timer_info.msec)
#define app_timeout()                       (timer_info.app_timeout == 1)
#define board_timeout_set(ms)               timer_info.board_timeout = 0;timer_info.board_timeout_ms = (ms+timer_info.msec)
#define board_timeout()                     (timer_info.board_timeout == 1)

#define delay_timer_init()                  timer5_init()
#define delay_timer_wait_ms(m)              timer5_wait_ms(m)
#define delay_timer_wait_us(u)              timer5_wait_us(u)

#define tasktimer_init()                    timer7_init()
#define tasktimer_setup(ms,os,id,f,p)       timer7_setup(ms,os,id,f,p)
#define tasktimer_terminate_alltasks()      timer7_terminate_alltasks()

#define get_systick_count()                 SysTick->VAL

#define get_timer_value_for_srand()         ((timer1_getcounter() << 16) | timer2_getcounter())

#endif  //__TIMER_H
