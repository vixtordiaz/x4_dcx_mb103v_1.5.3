/**
 *  ************* Copyright 2013 SCT Performance, LLC ******
 *  @file properties_vb.h
 *  @brief Handles the properties data structure for the iTSX
 *         app board.
 *  
 *  @authors Patrick Downs
 *  @date 11/06/2013
 *  ********************************************************
 */

#ifndef __PROPERTIES_VB_H
#define __PROPERTIES_VB_H

typedef struct
{
    u32 operating_mode;
    u32 failsafeboot_version;
    u32 mainboot_version;
    u32 app_signature;
    u32 app_version;
    u16 code_sector_size;
    u8  board_rev;
    u8  secboot_startsector;
    u16 secboot_maxcodesize;
    u8  app_build;
    u8  app_startsector;
    u32 app_maxcodesize;
    u8  reserved[16];
}PROPERTIES_VB_INFO;

u8 properties_vb_getinfo(u8 *info, u32 *info_length);
u8 properties_vb_validate_version_condition();

#endif  //__PROPERTIES_VB_H
