/**
  **************** (C) COPYRIGHT 2011 SCT Performance, LLC *********************
  * File Name          : commlink.c
  * Device / System    : SCT products using the "CommonCode" platform
  * Author             : Quyen Leba
  *
  * Version            : 1 
  * Date               : 05/02/2011
  * Description        : 
  *                    : 
  *
  *
  * History            : 05/02/2011 M. Davis
  *                    :   Add SCT Copyright header,
  *
  ******************************************************************************
  */

#include <board/genplatform.h>
#include <board/indicator.h>
#include <board/timer.h>
#include <common/crc32.h>
#include <common/crypto_blowfish.h>
#include <common/crypto_messageblock.h>
#include <common/housekeeping.h>
#include <common/statuscode.h>
#include "commlink.h"

CommLinkInfo commlink_info;

u8 *responseDataBuffer;

//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
u8 commlink_init(CommLinkBaudrate baud)
{
    commlink_info.packetstate = COMMLINK_SPW_START;
    commlink_info.container.wPayloadOffset = 0;
    commlink_info.use_encryption = 1;
    commlink_info.frame_avail = 0;
    commlink_info.passthrough = 0;
    commlink_info.loopback = 0;
    commlink_info.link = CommLinkInfoLink_None;
#if FORCE_COMMLINK_STATUS_AS_OPENED
    commlink_info.link = CommLinkInfoLink_Opened;
#endif
    commlink_info.data_ready = 0;
    commlink_info.baudrate = baud;
    commlink_info.speedsync_signature = 0x71C3810A; //not any significant number
    
    commlink_info.mode = CommLinkInfoMode_Responder;

    commlink_clear_halt_sending_ack();

    commlink_info.connection_history.PriorConnect = 0;
    commlink_info.connection_history.PriorDisconnect = 0;
    commlink_info.connection_history.PriorSessionOpen = 0;
    commlink_info.connection_history.PriorSessionClose = 0;
    commlink_info.connection_history.LastConnect = 0;
    commlink_info.connection_history.LastDisconnect = 0;
    commlink_info.connection_history.LastSessionOpen = 0;
    commlink_info.connection_history.LastSessionClose = 0;
    commlink_info.connection_history.LastTransmission = 0;
    
    memset(&commlink_info.cmdresp, 0, sizeof(CommandResponse));

    commlink_hal_init();

    return S_SUCCESS;
}

//------------------------------------------------------------------------------
// commlink_speedsyncrequest & commlink_speedsyncconfirm are to confirm speed
// changed succefully
// Output:  u32 *sync_signature
// Return:  u8  status
// Engineer: Quyen Leba
//------------------------------------------------------------------------------
u8 commlink_speedsyncrequest(u32 *sync_signature)
{
    srand(get_timer_value_for_srand());
    commlink_info.speedsync_signature = (u32)(((double)rand()/((double)RAND_MAX + (double)1))*0xE2A15B4C);
    *sync_signature = commlink_info.speedsync_signature;
    return S_SUCCESS;
}

//------------------------------------------------------------------------------
// commlink_speedsyncrequest & commlink_speedsyncconfirm are to confirm speed
// changed succefully
// Input:   u32 sync_value
// Return:  u8  status
// Engineer: Quyen Leba
//------------------------------------------------------------------------------
u8 commlink_speedsyncconfirm(u32 sync_value)
{
    u32 buffer[32];
    u32 crc32e_calc;
    u32 i;

    //math: fill buffer with sync_signature and calc crc32e on that buffer
    for(i=0;i<32;i++)
    {
        buffer[i] = commlink_info.speedsync_signature;
    }
    crc32e_reset();
    crc32e_calc = crc32e_calculateblock(0xFFFFFFFF,buffer,sizeof(buffer)/4);
    if (crc32e_calc == sync_value)
    {
        housekeeping_removeitem(HouseKeepingType_CommLinkSpeedSync);
        return S_SUCCESS;
    }
    else
    {
        commlink_init(USE_COMMLINK_BAUD);
        return S_FAIL;
    }
}

//------------------------------------------------------------------------------
// Enable/Disable passthrough mode. This mode will pass all data
// to debug console
// Input:   bool en (TRUE: enable)
// Engineer: Quyen Leba
//------------------------------------------------------------------------------
void commlink_passthrough_config(bool en)
{
    commlink_info.passthrough = 0;
    if (en)
    {
        commlink_info.passthrough = 1;
    }
}

//------------------------------------------------------------------------------
// Enable/Disable Bluetooth loopback mode. This mode will echo any data rx'd over
// bluetooth. This is a debug aid for app development.
// Input:   bool en (TRUE: enable)
// Engineer: Quyen Leba
//------------------------------------------------------------------------------
void commlink_loopback_config(bool en)
{
    commlink_info.loopback = 0;
    if (en)
    {
        commlink_info.loopback = 1;
    }
}

//------------------------------------------------------------------------------
// Update the LED indicator flag with the combined "link" and "session" state of the 
// communications between the MainBoard and the UI device and/or UI app, and update 
// the .link field in the global commlink_info struct with the "st" value. 
//
// When this function is called directly from either the "link" or "session" interrupt 
// handlers, the "st" argument is determined by the state-change that triggered the ISR. 
//
// Otherwise, except during accessory initialization or firmware-update operations, this 
// function is only called through indicator.c/indicator_link_status(), and "st" has 
// already been set according to the state of the hardware "link" and "session" signals. 
//
// Note that indicator_link_status() calls this function through the Mico 
// platform-specific "HAL" mechanism. On the iTSX platform, the "HAL"  translation maps 
// indicator_link_status() calls to bluetooth.c/bluetooth_status_indicator_update().
// 
//
// Input:  CommLinkInfoLinkStatus st -  
//         (CommLinkInfoLink_None, CommLinkInfoLink_Connected, CommLinkInfoLink_Opened)
// Output: call indicator_set() or indicator_clear(), commlink_info.link global set 
//         to input value
// Returns: S_SUCCESS if st is valid, otherwise S_NOTSUPPORT 
// Engineer: Quyen Leba
//------------------------------------------------------------------------------
u8 commlink_update_link_status(CommLinkInfoLinkStatus st)
{
    switch(st)
    {
    case CommLinkInfoLink_Opened:
        indicator_set(Indicator_LinkOpened);
        break;
    case CommLinkInfoLink_Connected:
        indicator_set(Indicator_LinkConnected);
        break;
    case CommLinkInfoLink_None:
        indicator_clear();
        break;
    default:
        return S_NOTSUPPORT;
    }
    commlink_info.link = st;

    return S_SUCCESS;
}

CommLinkInfoLinkStatus commlink_get_link_status()
{
    return commlink_info.link;
}


//------------------------------------------------------------------------------
// Reset/discard current command when the command isn't completed in time
// Engineer: Quyen Leba
//------------------------------------------------------------------------------
void commlink_reset_command_processor()
{
    commlink_info.packetstate = COMMLINK_SPW_START;
}

/**
 *  commlink_process_incoming_command_byte
 *  
 *  @brief Process incomming command data when in responder mode
 *  
 *  @param [in] in Incoming data byte
 *  @retval returns a STATUS_CODE enum
 *  
 *  @details Process commlink recieved byte which will form the incoming command data.
 *  
 *  @authors Quyen Leba, Patrick Downs
 */
void commlink_process_incoming_command_byte(u8 in)
{
    u16 DataLen;

    //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    //[START][OK][CmdCode][2:payload_len][4:crc32e][n:payload][STOP]
    //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    switch(commlink_info.packetstate)
    {
    case COMMLINK_SPW_START:
        if (in == COMMLINK_SOF)
        {
            commlink_reset_command_watchdog();
            commlink_info.packetstate = COMMLINK_SPW_CMDSTATUS;
        }
        else 
        {
            commlink_info.packetstate = COMMLINK_SPW_ERR;
        }
        break;
    case COMMLINK_SPW_CMDSTATUS:
        commlink_info.container.cmdStatus = in;
        commlink_info.packetstate = COMMLINK_SPW_CMDCODE;
        break;
    case COMMLINK_SPW_CMDCODE:
        commlink_info.container.cmdCode = in;
        commlink_info.packetstate = COMMLINK_SPW_PAYLOADLEN0;
        break;
    case COMMLINK_SPW_PAYLOADLEN0:
        commlink_info.container.wPayloadOffset = 0;
        commlink_info.container.wPayloadlen = in;
        commlink_info.packetstate = COMMLINK_SPW_PAYLOADLEN1;
        break;
    case COMMLINK_SPW_PAYLOADLEN1:
        DataLen = in;
        commlink_info.container.wPayloadlen |= DataLen << 8; 
        commlink_info.packetstate = COMMLINK_SPW_CRC32E0;
        break;
    case COMMLINK_SPW_CRC32E0:
        commlink_info.container.dwCRC32E = in;
        commlink_info.packetstate = COMMLINK_SPW_CRC32E1;
        break;
    case COMMLINK_SPW_CRC32E1:
        commlink_info.container.dwCRC32E |= ((u32)in) << 8;
        commlink_info.packetstate = COMMLINK_SPW_CRC32E2;
        break;
    case COMMLINK_SPW_CRC32E2:
        commlink_info.container.dwCRC32E |= ((u32)in) << 16;
        commlink_info.packetstate = COMMLINK_SPW_CRC32E3;
        break;
    case COMMLINK_SPW_CRC32E3:
        commlink_info.container.dwCRC32E |= ((u32)in) << 24;
        if (commlink_info.container.wPayloadlen == 0)
        {
            commlink_info.packetstate = COMMLINK_SPW_END;
        }
        else if (commlink_info.container.wPayloadlen > (MAX_COMMAND_DATALENGTH+16))
        {
            commlink_info.packetstate = COMMLINK_SPW_ERR;
        }
        else
        {
            commlink_info.packetstate = COMMLINK_SPW_DATA;
        }
        break;
    case COMMLINK_SPW_DATA:
        commlink_info.container.pPayload[commlink_info.container.wPayloadOffset] = in;
        if(commlink_info.container.wPayloadOffset < (commlink_info.container.wPayloadlen-1))
        {   
            commlink_info.container.wPayloadOffset++;
        }
        else
        {
            commlink_info.packetstate = COMMLINK_SPW_END;
            commlink_info.container.wPayloadOffset = 0;
        }
        
        if(commlink_info.container.wPayloadOffset > (MAX_COMMAND_DATALENGTH+16))
        {
            commlink_info.container.wPayloadOffset = 0;
            commlink_info.packetstate = COMMLINK_SPW_ERR;
        }
        break;
    case COMMLINK_SPW_END:
        if (in == COMMLINK_EOF)
        {
            commlink_info.packetstate = COMMLINK_SPW_START;
            if(commlink_processframe_simplewrapper() == S_SUCCESS)
            {
                cmdif_interrupt_command(commlink_info.container.cmdCode, (void*)NULL, NULL);
            }
        }
        else
        {
            commlink_info.packetstate = COMMLINK_SPW_ERR;
        }
        commlink_reset_command_watchdog();
        break;
    default:
        in = commlink_get_incoming_byte();
        commlink_info.packetstate = COMMLINK_SPW_ERR;
        break;
    }
    
    if(commlink_info.packetstate == COMMLINK_SPW_ERR)
    {
        commlink_info.packetstate = COMMLINK_SPW_START;
        return;
    }
}

/**
 *  commlink_process_incoming_response_byte
 *  
 *  @brief Process incomming repsonse data when in commander mode
 *  
 *  @param [in] in Parameter_Description
 *  @retval returns a STATUS_CODE enum
 *  
 *  @details Process commlink recieved byte which will form the incoming response data.
 *  
 *  @authors Quyen Leba, Patrick Downs
 */
void commlink_process_incoming_response_byte(u8 in)
{
    //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    //[START][OK][CmdCode][2:payload_len][4:crc32e][responseAck][responseCmd][n:payload][STOP]
    //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    switch(commlink_info.packetstate)
    {
    case COMMLINK_SPW_START:
        if (in == COMMLINK_SOF)
        {
            commlink_reset_command_watchdog();
            commlink_info.packetstate = COMMLINK_SPW_CMDSTATUS;
        }
        else
        {
            commlink_info.packetstate = COMMLINK_SPW_ERR;
        }
        break;
    case COMMLINK_SPW_CMDSTATUS:
        commlink_info.container.cmdStatus = (u8)in;
        commlink_info.packetstate = COMMLINK_SPW_CMDCODE;
        break;
    case COMMLINK_SPW_CMDCODE:
        commlink_info.container.cmdCode = (u8)in;
        commlink_info.packetstate = COMMLINK_SPW_PAYLOADLEN0;
        break;
    case COMMLINK_SPW_PAYLOADLEN0:
        commlink_info.container.wPayloadOffset = 0;
        commlink_info.container.wPayloadlen = (u8)in;
        commlink_info.packetstate = COMMLINK_SPW_PAYLOADLEN1;
        break;
    case COMMLINK_SPW_PAYLOADLEN1:
        commlink_info.container.wPayloadlen |= ((u8)in << 8);
        commlink_info.packetstate = COMMLINK_SPW_CRC32E0;
        break;
    case COMMLINK_SPW_CRC32E0:
        commlink_info.container.dwCRC32E = (u8)in;
        commlink_info.packetstate = COMMLINK_SPW_CRC32E1;
        break;
    case COMMLINK_SPW_CRC32E1:
        commlink_info.container.dwCRC32E |= ((u8)in << 8);
        commlink_info.packetstate = COMMLINK_SPW_CRC32E2;
        break;
    case COMMLINK_SPW_CRC32E2:
        commlink_info.container.dwCRC32E |= ((u8)in << 16);
        commlink_info.packetstate = COMMLINK_SPW_CRC32E3;
        break;
    case COMMLINK_SPW_CRC32E3:
        commlink_info.container.dwCRC32E |= ((u8)in << 24);
        commlink_info.packetstate = COMMLINK_SPW_RESPONSEACK;
        break;
    case COMMLINK_SPW_RESPONSEACK:
        commlink_info.container.responseAck = (u8)in;
        commlink_info.packetstate = COMMLINK_SPW_RESPONSECMDCODE;
        break;
    case COMMLINK_SPW_RESPONSECMDCODE:
        if (commlink_info.container.wPayloadlen % 4)
        {
            commlink_info.packetstate = COMMLINK_SPW_ERR;
        }
        else if (commlink_info.container.wPayloadlen > (MAX_COMMAND_DATALENGTH+16))
        {
            commlink_info.packetstate = COMMLINK_SPW_ERR;
        }
        else if (commlink_info.container.wPayloadlen > 0)
        {
            commlink_info.packetstate = COMMLINK_SPW_DATA;
        }
        else
        {
            commlink_info.packetstate = COMMLINK_SPW_END;
        }
        commlink_info.container.responseCmdCode = (u8)in;
        break;
    case COMMLINK_SPW_DATA:
        commlink_info.container.pPayload[commlink_info.container.wPayloadOffset] = in;
        if (commlink_info.container.wPayloadOffset < (commlink_info.container.wPayloadlen-1))
        {
            commlink_info.container.wPayloadOffset++;
        }
        else
        {
            commlink_info.packetstate = COMMLINK_SPW_END;
            commlink_info.container.wPayloadOffset = 0;
        }
        break;
    case COMMLINK_SPW_END:
        if (in == COMMLINK_EOF)
        {
            commlink_info.packetstate = COMMLINK_SPW_START;
            commlink_processframe_simplewrapper();
        }
        else
        {
            commlink_info.packetstate = COMMLINK_SPW_ERR;
        }
        break;
    default:
        commlink_info.packetstate = COMMLINK_SPW_ERR;
        break;
    }

    if(commlink_info.packetstate == COMMLINK_SPW_ERR)
    {
        commlink_info.packetstate = COMMLINK_SPW_START;
    }
}

/**
 *  commlink_processframe_simplewrapper
 *  
 *  @brief Process the complete frame recieved from commlink
 *  
 *  @retval returns a STATUS_CODE enum
 *  
 *  @details Recived Data Format depends on the commlink mode. 
 *          If Commlink Mode == Responder, we recieve COMMAND Data: 
 *          [cmdStatus(OK)][CmdCode][2:payload_len][4:crc32e][n:payload]
 *          If Commlink Mode == Commander, we recieved RESPONSE Data:
 *          [cmdStatus(OK)][CmdCode][2:payload_len][4:crc32e][responseAck][responseCmd][n:payload]
 *  
 *  @authors Quyen Leba, Patrick Downs
 */
u8 commlink_processframe_simplewrapper()
{
    u8  buffer[12];
    u32 paddingcount;
    u32 crc32e;
    
    crc32e_reset();
    memset(buffer,0xFF,sizeof(buffer));
    
    buffer[0] = commlink_info.container.cmdStatus;
    buffer[1] = commlink_info.container.cmdCode;
    buffer[2] = commlink_info.container.wPayloadlen & 0xFF;
    buffer[3] = commlink_info.container.wPayloadlen >> 8;
    //buffer[4..7] 4 Bytes CRC Set to 0xFF by memset for CRC calulation
    if(commlink_info.mode == CommLinkInfoMode_Commander)
    {
        // Check response data
        buffer[8] = commlink_info.container.responseAck;
        buffer[9] = commlink_info.container.responseCmdCode;
        //buffer[10..11] 2 Bytes Padding Set to 0xFF by memset for CRC calulation 
        crc32e = crc32e_calculateblock(0xFFFFFFFF,(u32*)buffer,(12/4));
    }
    else // CommLinkMode == Responder
    {
        crc32e = crc32e_calculateblock(0xFFFFFFFF,(u32*)buffer,(8/4));
    }
    
    paddingcount = commlink_info.container.wPayloadlen % 4;
    if (paddingcount)
    {
        paddingcount = 4 - paddingcount;
        memset((char*)&commlink_info.container.pPayload[commlink_info.container.wPayloadlen],0xFF,paddingcount);
        crc32e = crc32e_calculateblock(crc32e,(u32*)commlink_info.container.pPayload,
                                       1+commlink_info.container.wPayloadlen/4);
    }
    else
    {
        crc32e = crc32e_calculateblock(crc32e,(u32*)commlink_info.container.pPayload,
                                       commlink_info.container.wPayloadlen/4);
    }

    if (crc32e == commlink_info.container.dwCRC32E)
    {
        commlink_info.data_ready = 1;
        return S_SUCCESS;
    }
    else
    {
        return S_CRC32E;
    }
}

//------------------------------------------------------------------------------
// Input:   u16 max_len_allowed (that 'data' can hold)
// Output:  u8  *data
//          u16 *len (length of data)
// Return:  u8  status
//------------------------------------------------------------------------------
u8 commlink_receive_command(u8 *data, u16 *len, u16 max_len_allowed)
{
    u8  tmpbuffer[(MAX_COMMAND_DATALENGTH+16)];
    u32 decrypted_datalength;
    u16 count;
    u8  status;
    
    //[START][OK][CmdCode][2:payload_len][4:crc32e][n:payload][STOP]
    if (commlink_info.data_ready)
    {
        commlink_info.data_ready = 0;

        status = S_SUCCESS;
        data[0] = commlink_info.container.cmdStatus;
        data[1] = commlink_info.container.cmdCode;
        data[2] = commlink_info.container.wPayloadlen & 0xFF;;
        data[3] = commlink_info.container.wPayloadlen >> 8;
        count = 4;

        if (commlink_info.container.wPayloadlen > 0)
        {
            status = 
                crypto_messageblock_decrypt(commlink_info.container.pPayload,
                                            commlink_info.container.wPayloadlen,
                                            tmpbuffer,
                                            &decrypted_datalength,TRUE,
                                            CRYPTO_USE_EXTERNAL_KEY);
            if (status == S_SUCCESS)
            {
                if ((decrypted_datalength+4) <= max_len_allowed)
                {
                    memcpy(&data[4],tmpbuffer,decrypted_datalength);
                    count += decrypted_datalength;
                }
                else
                {
                    status = S_OUTOFRANGE;
                }
            }
            else
            {
                count = 0;
            }
        }
        *len = count;
        return status;
    }
    else
    {
        return S_FAIL;
    }
}

//------------------------------------------------------------------------------
// Transmit acknowledgment or data to user-interface app
//
// Input:   CMDIF_COMMAND cmd, 
//          CMDIF_ACK resp, 
//          u8 *data, 
//          u16 datalen
//
// Return: u8 status...
//          S_SUCCESS         OK
//          S_SESSION_DROPPED a Bluetooth accessory-to-app session drop occured during execution of this fn
//          S_INPUT           invalid args for crypto_messageblock_encrypt() 
//          S_HALTED          fatal BT-11 Bluetooth module lock-up (only w/ BT-11 firmware prior to rev 110522H)
//
// Engineer: Quyen Leba, Patrick Downs, Mark Davis
//------------------------------------------------------------------------------
u8 commlink_send_ack(CMDIF_COMMAND cmd, CMDIF_ACK resp, u8 *data, u16 datalen)
{
    u8  buffer[12];
    u8  SbCommand[16];
    u32 crc32e;
    u32 paddingcount;
    u8 msgblk[2048+16];
    u32 msgblklen;
    u8  status;

    // The commlink_info.halted bit prevent sending ack when connection is
    // lost due to disconnection, communication device crashed, etc
    // In case of BT-11 Bluetooth module has crashed,
    // which only occurs with BT-11 firmware before v110522H. The crash is 
    // only recoverable by cycling power on the accessory. After recovery, it may be 
    // necessary to manually un-pair the accessory, using the hardware un-pairing 
    // feature, and then re-pair the accessory with the user's mobile device.
    if (commlink_info.halted)
    {
        return S_HALTED;
    }

    memset(buffer,0xFF,sizeof(buffer));
    buffer[0] = 0;                  // Status
    buffer[1] = CMDIF_CMD_ACK;      // Command
    buffer[2] = datalen & 0xFF;     // Length 0
    buffer[3] = datalen >> 8;       // Length 1
    buffer[8] = resp;               // Resp code
    buffer[9] = cmd;                // Resp cmd

    if (commlink_info.use_encryption)
    {
        msgblklen = 0;
        if (datalen)
        {
            status = crypto_messageblock_encrypt(data,(u32)datalen,
                                                 msgblk,&msgblklen,
                                                 CryptoMessageBlockLength_UptoMax, CRYPTO_USE_EXTERNAL_KEY);
            if (status != S_SUCCESS)
            {
                return status;
            }
        }

        buffer[2] = msgblklen & 0xFF;   // Length 0
        buffer[3] = msgblklen >> 8;     // Length 1
        
        crc32e_reset();
        crc32e = crc32e_calculateblock(0xFFFFFFFF,(u32*)buffer,sizeof(buffer)/4);
        crc32e = crc32e_calculateblock(crc32e,(u32*)msgblk,msgblklen/4);

        buffer[4] = crc32e & 0xFF;      // CRC32E
        buffer[5] = crc32e >> 8;
        buffer[6] = crc32e >> 16;
        buffer[7] = crc32e >> 24;

        SbCommand[0] = COMMLINK_SOF;
        status = commlink_tx(SbCommand, 1, TRUE);
        if (status == S_SUCCESS)
        {
            status = commlink_tx(buffer, 10, TRUE);
            if (status == S_SUCCESS)
            {
                status = commlink_tx(msgblk, msgblklen, TRUE);
                if (status == S_SUCCESS)
                {
                    SbCommand[0] = COMMLINK_EOF;
                    status = commlink_tx(SbCommand, 1, TRUE);
                }
            }
        }
    }
    else  // commlink_info.use_encryption == FALSE
    {
        crc32e_reset();
        crc32e = crc32e_calculateblock(0xFFFFFFFF,(u32*)buffer,sizeof(buffer)/4);
        
        paddingcount = datalen % 4;
        if (paddingcount)
        {
            u8  tmpbuff[4];
            paddingcount = 4 - paddingcount;
            crc32e = crc32e_calculateblock(crc32e,(u32*)data,datalen/4);
            memset(tmpbuff,0xFF,sizeof(tmpbuff));
            memcpy(tmpbuff,&data[(datalen/4)*4],4-paddingcount);
            crc32e = crc32e_calculateblock(crc32e,(u32*)tmpbuff,1);
        }
        else // paddingcount % 4 == 0
        {
            crc32e = crc32e_calculateblock(crc32e,(u32*)data,datalen/4);
        }
        
        buffer[4] = crc32e & 0xFF;      // CRC32E
        buffer[5] = crc32e >> 8;
        buffer[6] = crc32e >> 16;
        buffer[7] = crc32e >> 24;

        SbCommand[0] = COMMLINK_SOF;
        status = commlink_tx(SbCommand, 1, TRUE);
        if (status == S_SUCCESS)
        {
            status = commlink_tx(buffer, 10, TRUE);
            if (status == S_SUCCESS)
            {
                status = commlink_tx(data, datalen, TRUE);
                if (status == S_SUCCESS)
                {
                    SbCommand[0] = COMMLINK_EOF;
                    status = commlink_tx(SbCommand, 1, TRUE);
                }
            }
        }
    }

    return status;
}

/**
 *  commlink_change_mode
 *  
 *  @brief Updates the CommLinkMode
 *  
 *  @param [in] newmode         New CommLinkMode Enum to set the mode to
 *  @retval                     none
 *  
 *  @details Commlink functions in a Command/Response method. Typically the mainboard(MB)
 *          is responsible for recieveing commands and responding with command 
 *          status. MB commlink mode is Responder and Appboard(AB) is Commander. 
 *          This command allows you to set change modes for situation that roles
 *          to be reversed.
 *  
 *  @authors Patrick Downs
 */

void commlink_change_mode(CommLinkMode newmode)
{
    commlink_info.mode = newmode;
    
    // Garbage cleanup
    if(responseDataBuffer)
    {
        __free(responseDataBuffer);
        responseDataBuffer = NULL;
    }    
}

//-----------------------------------------------------------------------------
// Send a command
// Inputs:  CMDIF_COMMAND cmd
//          const u8 *data (allow empty data; i.e. datalength is zero)
//          u32 datalength
//          bool flushbeforesendcmd (TRUE: flush all responses first)
// Return:  STATUS_CODE
// Engineer: Quyen Leba
//-----------------------------------------------------------------------------
u8 commlink_send_cmd(const CMDIF_COMMAND cmd,
                     const u8 *data, u32 datalength,
                     bool flushbeforesendcmd)
{
    //Command Frame = Command Overhead + Max Data Length + CryptoMessageBlock Overhead
    u8  command_frame[10+MAX_COMMAND_DATALENGTH+16];
    u32 command_frame_length;
    u16 encrypteddata_length;
    u32 crc32e;
    u8  status;

    if (flushbeforesendcmd)
    {
        //commlink_flush();
    }

    if (datalength == 0 || data == NULL)
    {
        command_frame_length = 10;
        command_frame[0] = 0x04;
        command_frame[1] = 0x00;  //ERROR_OK
        command_frame[2] = cmd;
        command_frame[3] = 0;     //cmd data length: 0
        command_frame[4] = 0;
        command_frame[5] = (u8)0xFF;
        command_frame[6] = (u8)0xFF;
        command_frame[7] = (u8)0xFF;
        command_frame[8] = (u8)0xFF;
        command_frame[9] = 0x06;

        crc32e_reset();
        crc32e = crc32e_calculateblock(0xFFFFFFFF, (u32*)&command_frame[1], 8/4);

        command_frame[5] = (crc32e & 0xFF);
        command_frame[6] = (crc32e >> 8);
        command_frame[7] = (crc32e >> 16);
        command_frame[8] = (crc32e >> 24);

        status = commlink_tx(command_frame,command_frame_length, FALSE);

        return status;
    }
    else if (datalength > MAX_COMMAND_DATALENGTH)
    {
        return S_BADCONTENT;
    }
    else
    {
        //encrypted data starts at command_frame[9]
        status = crypto_messageblock_encrypt((u8*)data,datalength,
                                             &command_frame[9],(u32*)&encrypteddata_length, 
                                             CryptoMessageBlockLength_UptoMax, CRYPTO_USE_EXTERNAL_KEY);
        if (status == S_SUCCESS)
        {
            command_frame_length = 10+encrypteddata_length;
            command_frame[0] = 0x04;
            command_frame[1] = 0x00;  //ERROR_OK
            command_frame[2] = cmd;
            command_frame[3] = (encrypteddata_length & 0xFF);
            command_frame[4] = (encrypteddata_length >> 8);
            command_frame[5] = (char)0xFF;
            command_frame[6] = (char)0xFF;
            command_frame[7] = (char)0xFF;
            command_frame[8] = (char)0xFF;

            crc32e_reset();
            crc32e = crc32e_calculateblock(0xFFFFFFFF,(u32*)&command_frame[1],
                                           (8+encrypteddata_length)/4);

            command_frame[5] = (crc32e & 0xFF);
            command_frame[6] = (crc32e >> 8);
            command_frame[7] = (crc32e >> 16);
            command_frame[8] = (crc32e >> 24);

            command_frame[9+encrypteddata_length] = 0x06;

            status = commlink_tx(command_frame,command_frame_length, FALSE);

            return status;
        }
        else
        {
            return status;
        }
    }
}

/**
 *  commlink_basicCmdAck
 *  
 *  @brief Sends a command over commlink
 *  
 *  @param [in] cmd CMDIF_COMMAND command to be sent
 *  @param [in] cmdData Data to be sent with command, if no data set to NULL
 *  @param [in] cmdDatalength Length of data to be sent, if no data set to NULL
 *  @param [out] pxResponse Pointer to where response data will be saved
 *  @param [in] ms_timeout Ammount of time in miliseconds to wait for response
 *  @retval returns a STATUS_CODE enum
 *  
 *  @details When in commander mode, sends a command over commlink
 *  
 *  @authors Quyen Leba, Patrick Downs
 */
u8 commlink_basicCmdAck(const CMDIF_COMMAND cmd,
                        const u8 *cmdData, u16 cmdDatalength,
                        CommandResponse *pxResponse, u32 ms_timeout)
{
    u8  status;

    status = commlink_send_cmd(cmd,cmdData,cmdDatalength,TRUE);
    if (status != S_SUCCESS)
    {
        return status;
    }
    status = S_SUCCESS;

    status = commlink_get_response(cmd,ms_timeout, &commlink_info.cmdresp);
    if(status == S_SUCCESS)
    {
        *pxResponse = commlink_info.cmdresp;
    }

    return status;
}


//------------------------------------------------------------------------------
// Get response of a command
// Inputs:  C_CMDIF::CMDIF_COMMAND cmd (to match with response)
//          u32 ms_timeout
// Output:  CommandResponse *pxResponse
// Return:  u8  status
// Engineer: Quyen Leba/Patrick Downs
//------------------------------------------------------------------------------
u8 commlink_get_response(CMDIF_COMMAND cmd, u32 ms_timeout,
                         CommandResponse *pxResponse)
{
    u8  status;
    
    board_timeout_set(ms_timeout);
    while(!board_timeout())
    {
        if (commlink_info.data_ready)
        {
            commlink_info.data_ready = 0;
                       
            if(commlink_info.container.responseCmdCode == cmd)
            {
                if(commlink_info.container.responseCmdCode == CMDIF_ACK_WAIT)
                {
                    board_timeout_set(30000);
                    continue;
                }
                
                pxResponse->cmdStatus = commlink_info.container.cmdStatus;
                pxResponse->cmdAck = commlink_info.container.cmdCode;
                pxResponse->responseCode = commlink_info.container.responseAck;
                pxResponse->responseCmd = commlink_info.container.responseCmdCode;
                //            pxResponse->datalength = commlink_info.container.wPayloadlen;
                if (commlink_info.container.wPayloadlen)
                {
                    if(responseDataBuffer)
                    {
                        __free(responseDataBuffer);
                        responseDataBuffer = NULL;
                    }
                    responseDataBuffer = __malloc(commlink_info.container.wPayloadlen);
                    if(responseDataBuffer)
                    {
                        pxResponse->data = responseDataBuffer;
                        status = 
                            crypto_messageblock_decrypt(commlink_info.container.pPayload,
                                                        commlink_info.container.wPayloadlen,
                                                        pxResponse->data,
                                                        &pxResponse->datalength,TRUE,
                                                        CRYPTO_USE_EXTERNAL_KEY);
                    }
                    else
                    {
                        status = S_MALLOC;
                    }
                }
                else
                {
                    status = S_SUCCESS;
                }
            }
            else
            {
                status = S_UNMATCH;
            }
            
            return status;
        }
    }
    
    return S_TIMEOUT;
}
