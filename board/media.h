/**
  **************** (C) COPYRIGHT 2011 SCT Performance, LLC *********************
  * File Name          : media.h
  * Device / System    : SCT products using the "CommonCode" platform
  * Author             : Quyen Leba
  *
  * Version            : 1 
  * Date               : 05/02/2011
  * Description        : 
  *                    : 
  *
  *
  * History            : 05/02/2011 M. Davis
  *                    :   Add SCT Copyright header,
  *
  ******************************************************************************
  */

#ifndef __MEDIA_H
#define __MEDIA_H

#define MEDIA_TRAILING_DATA     0x00000000

u8 media_ftlinit(u16 chunk_index, u32 chunk_size);
u8 media_ftldeinit();
u8 media_ftlread(u8 *data, u32 datalength, u32 *readlength);
u8 media_ftlwrite(u8 *data, u32 datalength);
u8 media_ftlwrite_failsafe(u8 *data, u32 datalength);
u8 media_create_fs_journal();
u8 media_delete_fs_journal();

#endif  //__MEDIA_H