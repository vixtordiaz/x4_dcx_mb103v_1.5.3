/**
**************** (C) COPYRIGHT 2011 SCT Performance, LLC *********************
* File Name          : debugif.c
* Device / System    : SCT products using the "CommonCode" platform
* Author             : Patrick Downs
*
* Version            : 1 
* Date               : 05/02/2011
* Description        : 
*                    : 
*
*
* History            : 05/23/2011 P. Downs
*                    :   Created
*
******************************************************************************
*/

#include <string.h>
#include <board/genplatform.h>
#include <device_config.h>
#include <common/statuscode.h>
#include <common/usbcmdhandler.h>
#include "debugif.h"


// redirect which usart port to run bluetooth module
#define debugif_hal(func)           usartB_##func

struct
{
    u8 debugRxBuffer[64];
    u32 debugRxSize;
    u8 debugRxDataReady;

    DEBUGIF_IN_MODE debugif_in_mode;
}debugif_info;

//------------------------------------------------------------------------------
// Debug hardware initialization
// Return:  u8  status
//------------------------------------------------------------------------------
u8 debugif_init()
{
#if !CONFIG_FOR_TEST
    const u8 msg[] = "\n\rDebug Interface Initalized";
#endif
    
    debugif_uart_rx_reset();
    
    debugif_info.debugif_in_mode = DEBUGIF_IN_NORMAL;
    
    debugif_hal(init)(BAUD115200,UART_NO_FLOWCONTROL,UART_USE_INTERRUPT);
    
#if !CONFIG_FOR_TEST    // Don't send debug init message if test app
    debugif_out((u8*)msg, strlen((char*)msg));
#endif
    
    return S_SUCCESS;
}

//------------------------------------------------------------------------------
// Debug hardware output function
//------------------------------------------------------------------------------
void debugif_out(u8* dataout, u32 size)
{
    u32 i;    
    
    for(i=0;i<size;i++)
    {
        debugif_hal(tx)(dataout[i]);
    }
}

//------------------------------------------------------------------------------
// Debug hardware input function
// Return:  u8  status
//------------------------------------------------------------------------------
u8 debugif_in(u8* datain, u32 *size)
{
    if (debugif_info.debugRxDataReady)
    {
        memcpy(datain, debugif_info.debugRxBuffer, debugif_info.debugRxSize);
        *size = debugif_info.debugRxSize;
        debugif_info.debugRxDataReady = 0;
        debugif_info.debugRxSize = 0;
        return S_SUCCESS;
    }
    return S_FAIL;
}

//------------------------------------------------------------------------------
// Debug hardware input function
//------------------------------------------------------------------------------
void debugif_setmode(DEBUGIF_IN_MODE mode)
{
    debugif_info.debugif_in_mode = mode;
}

//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
void debugif_uart_rx_irq_handler()
{
    u8 in;

    in = debugif_hal(rx)();
    
    debugif_uart_rx_process_input(in);
}

//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
void debugif_uart_rx_process_input(u8 inbyte)
{
    if(debugif_info.debugif_in_mode == DEBUGIF_IN_PASSTHROUGH)
    {
        debugif_info.debugRxBuffer[debugif_info.debugRxSize++] = inbyte; // Save data byte to buffer
        debugif_info.debugRxDataReady = 1; // Set data ready flag
    }
    else
    {
        if(inbyte == 0x08) // Backspace
        {
            if(debugif_info.debugRxSize != 0)
            {
//                debugif_hal(tx)(inbyte); // ECHO
                debugif_info.debugRxSize--;                
            }
        }
        else
        {
//            debugif_hal(tx)(inbyte); // ECHO
            debugif_info.debugRxBuffer[debugif_info.debugRxSize++] = inbyte; // Save data byte to buffer
        }
        
        if(debugif_info.debugRxSize != 0)
        {  
            if((inbyte == '\r') ||
               (debugif_info.debugRxSize == sizeof(debugif_info.debugRxBuffer))) // Enter or Buffer full
            {
                debugif_info.debugRxSize--;
                debugif_info.debugRxDataReady = 1; // Set data ready flag
            }
        }
    }
}


//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
void debugif_uart_rx_reset(void)
{
    // Inialize rx buffer variablesdata 
    memset(debugif_info.debugRxBuffer, 0, sizeof(debugif_info.debugRxBuffer));
    debugif_info.debugRxSize = 0;
    debugif_info.debugRxDataReady = 0;  
}
