/**
 *  ************* Copyright 2013 SCT Performance, LLC ******
 *  @file properties_ab_bt11.h
 *  @brief Handles the properties data structure for the iTSX
 *         app board.
 *  
 *  @authors Patrick Downs
 *  @date 11/06/2013
 *  ********************************************************
 */

#ifndef __PROPERTIES_AB_X4_H
#define __PROPERTIES_AB_X4_H

typedef struct
{
        u32 operating_mode;
        u32 failsafeboot_version;
        u32 mainboot_version;
        u32 app_signature;
        u32 app_version;
        u16 code_sector_size;
        u8  board_rev;
        u8  secboot_startsector;
        u16 secboot_maxcodesize;
        u8  app_build;
        u8  app_startsector;
        u32 app_maxcodesize;
        u8  reserved[16];
}PROPERTIES_AB_INFO;

void properties_ab_init();
u8 properties_ab_getinfo(u8 *info, u32 *info_length);
void properties_ab_getpropertiesafterupdate();

#endif  //__PROPERTIES_AB_X4_H