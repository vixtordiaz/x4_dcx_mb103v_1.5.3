/**
  **************** (C) COPYRIGHT 2011 SCT Performance, LLC *********************
  * File Name          : bootloader_ab_ken.c
  * Device / System    : SCT products using the "CommonCode" platform
  * Author             : Quyen Leba
  *
  * Version            : 1 
  * Date               : 05/02/2011
  * Description        : 
  *                    : 
  *
  *
  * History            : 05/02/2011 M. Davis
  *                    :   Add SCT Copyright header,
  *
  ******************************************************************************
  */

//------------------------------------------------------------------------------
// Log error point: 0x70xx -> 0x73xx
//------------------------------------------------------------------------------

#include <string.h>
#include <ctype.h>
#include <device_config.h>
#include <common/statuscode.h>
#include <common/crc32.h>
#include <common/crypto_blowfish.h>
#include <common/tea.h>
#include <common/filecrypto.h>
#include <common/bootsettings.h>
#include "bootloader_ab_ken.h"
#include <board/indicator.h>
#include <common/housekeeping.h>
#include "commlink.h"

#define APPBOARD_KEN257_FW_FILENAME                     "LWTSApp.bin"

struct
{
    struct
    {
        u8  init        :1;
        u8  reserved    :7;
    }control;
    u32 operating_mode;
    u32 fwcrc32e;
    F_FILE* fptr;
    u32 calccrc32e;
}bootloader_ab_info = 
{
    .control.init = 0,
    .fwcrc32e = 0,
    .fptr = NULL,
};

u8 __abupdater_init(FirmwareHeader *fwheader);
u8 __abupdater_do(u8 *data, u16 datalength);
u8 __abupdater_validate(u32 fwcrc32e);

/**
 *  bootloader_ab_start_bootloader
 *  
 *  @brief Forces the App Board to reset and boot into main bootloader
 *  
 *  @retval 			returns a STATUS_CODE enum
 *  
 *  @details Set force App board to boot into main bootloader. This is useful for
 *             firmware/tune revision update to report progress to the GUI.
 *  
 *  @authors Patrick Downs
 *  
 */
u8 bootloader_ab_start_bootloader(BootloaderMode mode, bool backlighton)
{
    return S_NOTSUPPORT;
}

/**
 *  bootloader_ab_setbootmode
 *  
 *  @brief Set Appboard boot mode
 *  
 *  @param [in] newmode	    New boot mode a BootloaderMode enum
 *  @retval 				returns a STATUS_CODE enum
 *  
 *  @details Sets the boot mode for the appboard. This will save a new 
 *           bootmode into it's bootsettings and then reset itself.
 *  
 *  @authors Patrick Downs
 *  
 */
u8 bootloader_ab_setbootmode(BootloaderMode newmode)
{
    if(newmode == BootloaderMode_AB_Application)
    {
        cmdif_internalresponse_ack(CMDIF_CMD_WIFI_UPDATES_APPLY,CMDIF_ACK_DONE, NULL, 0);
    }
    
    return S_SUCCESS;
}

/**
 *  bootloader_ab_progressbar
 *  
 *  @brief Display progress bar and message to GUI
 *  
 *  @param [in] percentage Percentage completer 0-100
 *  @param [in] message     Short string to display with progress
 *  @retval 				returns a STATUS_CODE enum
 *  
 *  @details Displays the progress bar and string while in app board main
 *              bootloader.
 *  
 *  @authors Patrick Downs
 *  
 */
u8 bootloader_ab_progressbar(u8 percentage, u8 *message)
{
    u8 buffer[64];
    u32 datalen;
    u8 status;
    
    buffer[0] = percentage;
    datalen = 1;
    if(strlen((char const*)message) < sizeof(buffer)-1)
    {
        strcpy((char*)buffer+1, (char const*)message);
        datalen += strlen((char const*)message); // Add stringlength to data length
        datalen++;                  // Add byte for NULL
    }
    
    status = cmdif_internalresponse_ack(CMDIF_CMD_WIFI_UPDATES_APPLY,CMDIF_ACK_PROGRESSBAR,buffer, datalen);
    
    return status;
}

/**
 *  bootloader_ab_resetandrun
 *  
 *  @brief      Reset and run the app board 
 *  
 *  @retval     none
 *  
 *  @details    Resets the app board and puts commlink back to Responder mode
 *              typically after an update to run App boot mode.
 *           
 *  @authors Patrick Downs
 *  
 */
void bootloader_ab_resetandrun()
{
    cmdif_internalresponse_ack(CMDIF_CMD_WIFI_UPDATES_APPLY,CMDIF_ACK_APP_OUTOFDATE, NULL, 0);
    
    return;
}

//------------------------------------------------------------------------------
// Input:   FirmwareHeader fwheader (No Encryption)
// Return:  u8  status
// Engineer: Quyen Leba/Patrick Downs
//------------------------------------------------------------------------------
u8 bootloader_ab_init(FirmwareHeader fwheader)
{
    u8  status;
    
    status = __abupdater_init(&fwheader);
    
    crc32e_sw_reset();
    bootloader_ab_info.calccrc32e = 0xFFFFFFFF;
    
    return status;
}

//------------------------------------------------------------------------------
// Input:   FirmwareHeader fwheader (No Encryption)
// Return:  u8  status
// Engineer: Quyen Leba/Patrick Downs
//------------------------------------------------------------------------------
u8 bootloader_ab_init_file(FirmwareHeader fwheader)
{
    if (bootloader_ab_info.fptr)
    {
        genfs_closefile(bootloader_ab_info.fptr);
        bootloader_ab_info.fptr = NULL;
    }
    bootloader_ab_info.fptr = genfs_user_openfile(AB_FWUPDATE_TEMP,"wb");
    if (!bootloader_ab_info.fptr)
    {
        return S_OPENFILE;
    }
    
    bootloader_ab_info.control.init = 1;
    bootloader_ab_info.fwcrc32e = fwheader.firmware_crc32e;
    
    crc32e_sw_reset();
    bootloader_ab_info.calccrc32e = 0xFFFFFFFF;
    
    return S_SUCCESS;
}

//------------------------------------------------------------------------------
// Inputs:  u8  *data (encrypted with critcal key)
//          u32 datalength (multiple of 8 bytes)
// Return:  u8  status
// Engineer: Quyen Leba
//------------------------------------------------------------------------------
u8 bootloader_ab_do(u8 *data, u32 datalength)
{
    // Calculate CRC32 by software for validation
    bootloader_ab_info.calccrc32e = crc32e_sw_calculateblock(bootloader_ab_info.calccrc32e,
                                                             (u32*)data,datalength/4);
    crypto_blowfish_decryptblock_critical(data,datalength);
    
    return __abupdater_do(data,datalength);
}

//------------------------------------------------------------------------------
// Inputs:  u8  *data (encrypted with critcal key)
//          u32 datalength (multiple of 8 bytes)
// Return:  u8  status
// Engineer: Quyen Leba/Patrick Downs
//------------------------------------------------------------------------------
u8 bootloader_ab_do_file(u8 *data, u32 datalength)
{
    u32 bytecount;
    
    if (bootloader_ab_info.fptr)
    {
        // Calculate CRC32 by software for validation
        bootloader_ab_info.calccrc32e = crc32e_sw_calculateblock(bootloader_ab_info.calccrc32e,
                                                                 (u32*)data,datalength/4);

        bytecount = fwrite(data,1,datalength,bootloader_ab_info.fptr);
        if(bytecount == datalength)
        {
            return S_SUCCESS;
        }
        else
        {
            return S_WRITEFILE;
        }
    }
    else
    {
        return S_OPENFILE;
    }
}

//------------------------------------------------------------------------------
// Validate VehicleBoard firmware
// Return:  u8  status
// Engineer: Quyen Leba
//------------------------------------------------------------------------------
u8 bootloader_ab_validate()
{
    u8  status;

    status = __abupdater_validate(bootloader_ab_info.fwcrc32e);
    return status;
}

//------------------------------------------------------------------------------
// Validate VehicleBoard firmware
// Return:  u8  status - S_LOADFILE should indicate to calling function that
//              bootloader_ab_updatebyfile() must be called next to auctually load
//              the firmware.
// Engineer: Quyen Leba/Patrick Downs
//------------------------------------------------------------------------------
u8 bootloader_ab_validate_file()
{
    if (bootloader_ab_info.fptr)
    {
        genfs_closefile(bootloader_ab_info.fptr);
        bootloader_ab_info.fptr = NULL;
    }

    if (bootloader_ab_info.fwcrc32e != bootloader_ab_info.calccrc32e)
    {
        return S_CRC32E;
    }
    
    return S_LOADFILE;
}

//------------------------------------------------------------------------------
// Program AppBoard firmware data from a file
// Inputs:  u8  *data
//          u16 datalength (multiple of 8 bytes)
// Return:  u8  status
// Engineer: Quyen Leba
//------------------------------------------------------------------------------
u8 bootloader_ab_updatebyfile()
{
#define FW_BUFFER_SIZE    2048
#define BLUETOOTH_SESSION_EXTI_LINE EXTI_Line9
#define BLUETOOTH_LINK_EXTI_LINE    EXTI_Line11
    F_FILE *tarfptr;
    u8  status;
    u8 *readbuffer;
    u32 filesize;
    u32 bytecount;

    status = S_FAIL;
    readbuffer = NULL;
    tarfptr = NULL;

    indicator_set(Indicator_TaskInProgress);
    indicator_set_normaloperation(FALSE); // Lock indicator for atleast 10 seconds

    if (bootloader_ab_info.fptr)
    {
        genfs_closefile(bootloader_ab_info.fptr);
        bootloader_ab_info.fptr = NULL;
    }
    bootloader_ab_info.fptr = genfs_user_openfile(AB_FWUPDATE_TEMP,"rb");
    if (!bootloader_ab_info.fptr)
    {
        return S_OPENFILE;
    }

    readbuffer = __malloc(FW_BUFFER_SIZE);
    if (!readbuffer)
    {
        status = S_MALLOC;
        goto bootloader_ab_updatebyfile_done;
    }

    tarfptr = genfs_user_openfile(APPBOARD_KEN257_FW_FILENAME,"w");
    if (tarfptr == NULL)
    {
        status = S_OPENFILE;
        goto bootloader_ab_updatebyfile_done;
    }
    
    fseek(bootloader_ab_info.fptr,0,SEEK_END);
    filesize = ftell(bootloader_ab_info.fptr);
    fseek(bootloader_ab_info.fptr,0,SEEK_SET);

    status = S_SUCCESS;
    while(filesize && status == S_SUCCESS)
    {
        if(filesize >= FW_BUFFER_SIZE)
        {
            bytecount = fread(readbuffer,1,FW_BUFFER_SIZE,bootloader_ab_info.fptr);
        }
        else
        {
            bytecount = fread(readbuffer,1,filesize,bootloader_ab_info.fptr);
        }
        filesize -= bytecount;
        crypto_blowfish_decryptblock_critical(readbuffer,bytecount);

//        status = __abupdater_do(readbuffer, bytecount);
//        if (status != S_SUCCESS)
//        {
//            break;
//        }
        if (fwrite(readbuffer,1,bytecount,tarfptr) != bytecount)
        {
            status = S_WRITEFILE;
            goto bootloader_ab_updatebyfile_done;
        }
    }

    if(status == S_SUCCESS)
    {
        indicator_set_normaloperation(TRUE);
        indicator_set(Indicator_TaskSuccess);   // Success, go green, normalmode LED
        indicator_set_normaloperation(FALSE);
    }
    else
    {
        indicator_set_normaloperation(TRUE);
        indicator_set(Indicator_TaskFail);      // Error, stay RED no normalmode LED
        indicator_set_normaloperation(FALSE);
    }

    housekeeping_additem(HouseKeepingType_IndicatorTimeout, 3000);// Lock indicator for atleast 3 seconds

bootloader_ab_updatebyfile_done:
    if (bootloader_ab_info.fptr)
    {
        genfs_closefile(bootloader_ab_info.fptr);
        bootloader_ab_info.fptr = NULL;
    }
    if (tarfptr)
    {
        genfs_closefile(tarfptr);
    }
    if (readbuffer)
    {
        __free(readbuffer);
    }
    return status;
}

//------------------------------------------------------------------------------
// Init AppBoard in bootloader
// Input:   FirmwareHeader *fwheader
// Return:  u8  status
// Engineer: Quyen Leba
//------------------------------------------------------------------------------
u8 __abupdater_init(FirmwareHeader *fwheader)
{
//    u8  status;
//    bootloader_ab_info.control.init = 0;
//    status = bluetooth_enter_bootloader();
//    if (status != S_SUCCESS)
//    {
//        log_push_error_point(0x7020);
//        return status;
//    }
    bootloader_ab_info.control.init = 1;
    bootloader_ab_info.fwcrc32e = fwheader->firmware_crc32e;

	return S_SUCCESS;
}

//------------------------------------------------------------------------------
// Program AppBoard firmware data
// Inputs:  u8  *data
//          u16 datalength (multiple of 8 bytes)
// Return:  u8  status
// Engineer: Quyen Leba
//------------------------------------------------------------------------------
u8 __abupdater_do(u8 *data, u16 datalength)
{
    return S_NOTSUPPORT;
}

//------------------------------------------------------------------------------
// Validate AppBoard firmware data
// Input:   u32 fwcrc32e
// Return:  u8  status
// Engineer: Quyen Leba
//------------------------------------------------------------------------------
u8 __abupdater_validate(u32 fwcrc32e)
{
    if (bootloader_ab_info.control.init == 0)
    {
        log_push_error_point(0x7050);
        return S_ERROR;
    }
    else if (bootloader_ab_info.fwcrc32e != bootloader_ab_info.calccrc32e)
    {
        log_push_error_point(0x7051);
        return S_CRC32E;
    }

    return S_SUCCESS;
}

//------------------------------------------------------------------------------
// Get AppBoard firmware version
// Output:  u32 *fwversion
// Return:  u8  status
// Engineer: Quyen Leba
//------------------------------------------------------------------------------
u8 bootloader_ab_getversion(u32 *fwversion)
{
    F_FILE *fptr;
    FileCryptoHeaderInfo general_header;
    u8  *bptr;
    u32 bytecount;
    u32 readcount;
    u32 index;
    u32 crc32e_calc;
    u8  buffer[512];

    //if error, force *fwversion to ZERO to accept anything, and AppBoard
    //will decide how/if to use it
    *fwversion = 0;
    fptr = genfs_user_openfile(APPBOARD_KEN257_FW_FILENAME,"r");
    if (!fptr)
    {
        goto bootloader_ab_getversion_done;
    }
    
    bytecount = fread((char*)buffer,1,sizeof(buffer),fptr);
    if (bytecount != sizeof(buffer))
    {
        //this file can't be less than 512b
        goto bootloader_ab_getversion_done;
    }
    crypto_blowfish_decryptblock_external_key((u8*)buffer,sizeof(buffer));
    memcpy((char*)&general_header,buffer,sizeof(FileCryptoHeaderInfo));
    if (general_header.hversion != FILECRYPTO_GENERAL_HEADER_VERSION_MAX)
    {
        goto bootloader_ab_getversion_done;
    }
    else if (general_header.ftype != FileCryptoFileType_FileBundle ||
             general_header.fsubtype != FileCryptoFileSubType_Firmware)
    {
        goto bootloader_ab_getversion_done;
    }

    bptr = buffer+4;
    crc32e_reset();
    if (general_header.hsize > sizeof(buffer))
    {
        bytecount = sizeof(buffer);
    }
    else
    {
        bytecount = general_header.hsize;
    }
    index = bytecount;
    crc32e_calc = crc32e_calculateblock(0xFFFFFFFF,(u32*)bptr,(bytecount-4)/4);

    while (index < general_header.hsize)
    {
        if ((general_header.hsize - index) > sizeof(buffer))
        {
            readcount = sizeof(buffer);
        }
        else
        {
            readcount = general_header.hsize - index;
        }
        bytecount = fread((char*)buffer,1,readcount,fptr);
        if (bytecount > 0 && (bytecount%4 == 0))
        {
            crypto_blowfish_decryptblock_external_key((u8*)buffer,bytecount);
            crc32e_calc = crc32e_calculateblock(crc32e_calc,
                                                (u32*)buffer,bytecount/4);
        }
        else
        {
            break;
        }
        index += bytecount;
    }

    if (crc32e_calc != general_header.hcrc32e)
    {
        goto bootloader_ab_getversion_done;
    }
    *fwversion = general_header.contentversion;

bootloader_ab_getversion_done:
    if (fptr)
    {
        genfs_closefile(fptr);
    }
    return S_SUCCESS;
}

//------------------------------------------------------------------------------
// set AppBoard operating mode
// Output:  u32 *mode
// Return:  u8  status
// Engineer: Quyen Leba
//------------------------------------------------------------------------------
u8 bootloader_ab_setoperatingmode(u32 mode)
{
    bootloader_ab_info.operating_mode = mode;
    return S_SUCCESS;
}

//------------------------------------------------------------------------------
// Get AppBoard operating mode
// Output:  u32 *mode
// Return:  u8  status
// Engineer: Quyen Leba
//------------------------------------------------------------------------------
u8 bootloader_ab_getoperatingmode(u32 *mode)
{
    *mode = bootloader_ab_info.operating_mode;
    return S_SUCCESS;
}