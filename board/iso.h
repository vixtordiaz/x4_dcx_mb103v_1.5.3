/**
  **************** (C) COPYRIGHT 2011 SCT Performance, LLC *********************
  * File Name          : iso.h
  * Device / System    : SCT products using the "CommonCode" platform
  * Author             : Rhonda Rusak
  *
  * Version            : 1 
  * Date               : 08/11/2014
  * Description        : 
  *                    : 
  *
  *
  ******************************************************************************
  */

#ifndef __ISO_H
#define __ISO_H

#include <arch/gentype.h>


u8 iso_init(VehicleCommType *vehiclecommtype);
u8 iso_tx(u8 *cmd_frame, u16 tx_datalength, u16 txflags, u16 rxflags);
u8 iso_rx(u8 *rxdatabuffer, u16 *rxdatabufferlength, u32 opcode_length, 
          u16 rxflags, u32 timeout);

#endif  //__ISO_H