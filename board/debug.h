/**
  **************** (C) COPYRIGHT 2011 SCT Performance, LLC *********************
  * File Name          : debug.h
  * Device / System    : SCT products using the "CommonCode" platform
  * Author             : Quyen Leba
  *
  * Version            : 1 
  * Date               : 05/02/2011
  * Description        : 
  *                    : 
  *
  *
  * History            : 05/02/2011 M. Davis
  *                    :   Add SCT Copyright header,
  *
  ******************************************************************************
  */

#ifndef __DEBUG_H
#define __DEBUG_H

#define debug(string)

#endif    //__DEBUG_H
