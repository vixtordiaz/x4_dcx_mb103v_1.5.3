/**
  **************** (C) COPYRIGHT 2011 SCT Performance, LLC *********************
  * File Name          : bluetooth.h
  * Device / System    : SCT products using the "CommonCode" platform
  * Author             : Quyen Leba
  *
  * Version            : 1 
  * Date               : 05/02/2011
  * Description        : 
  *                    : 
  *
  *
  * History            : 05/02/2011 M. Davis
  *                    :   Add SCT Copyright header,
  *
  ******************************************************************************
  */

#ifndef __BLUETOOTH_H
#define __BLUETOOTH_H

#include <arch/gentype.h>
#include <common/cmdif.h>

#define BLUETOOTH_MAX_DEVICE_COUNT          7

typedef enum 
{
    SB_COMMAND_MODE,
    SB_AUTOMATIC_MODE
}SBOperationMode_T;

typedef enum
{
    // BT module specific UartSpeeds
    BT_2400_BAUD      = 0x00,
    BT_4800_BAUD      = 0x01,
    BT_7200_BAUD      = 0x02,
    BT_9600_BAUD      = 0x03,
    BT_19200_BAUD     = 0x04,
    BT_38400_BAUD     = 0x05,
    BT_57600_BAUD     = 0x06,
    BT_115200_BAUD    = 0x07,
    BT_230400_BAUD    = 0x08,
    BT_460800_BAUD    = 0x09,
    BT_921600_BAUD    = 0x0A,
}SbBaudrate;

typedef enum
{
    SB_PORT_1   = 1,
    SB_PORT_2   = 2,
    SB_PORT_3   = 3,
    SB_PORT_4   = 4,
    SB_PORT_5   = 5,
    SB_PORT_6   = 6,
    SB_PORT_7   = 7,
}SbPortNumber;

static const u32 SbTranslatedBaudrate[] =
{
    //IMPORTANT: the index follow enum of SbBaudrate
    [0] = 2400,
    [1] = 4800,
    [2] = 7200,
    [3] = 9600,
    [4] = 19200,
    [5] = 38400,
    [6] = 57600,
    [7] = 115200,
    [8] = 230400,
    [9] = 460800,
    [10] = 921600,
};

//Simply Blue command/response frame parsing state
//Please refer simply blue software user guide reference document for the description
//of packet structure
typedef enum
{
    SBPACKET_START,
    SBPACKET_TYPE,
    SBPACKET_OPCODE,
    SBPACKET_DATALENGTH0,
    SBPACKET_DATALENGTH1,
    SBPACKET_CHECKSUM,
    SBPACKET_DATA,
    SBPACKET_END,
    SBPACKET_ERR,
}SbPacketState_T;

typedef enum
{
    SBSTATUS_OK = 1,
    SBSTATUS_ERROR,
    SBSTATUS_NO_TIMER,
    SBSTATUS_CHECKSUM_ERROR,
    SBSTATUS_ETX_NOT_FOUND,
    SBSTATUS_UART_INCOMPLETE_TRANSFER,
    SBSTATUS_TIMEOUT,
    SBSTATUS_INPROCESS,
}SBStatus_T;

typedef struct 
{
    u8   bStart;             
    u8   bType;              
    u8   bOpcode;
    u16  wPayloadlen;
    u16  wPayloadOffset;
    u8   bChecksum;
    //u8   pPayload[340];
    u8   pPayload[2048];
    u8   bEnd;
}SBEvent_T;

typedef enum
{
    ESTABLISHED,
    RELEASED,
    READY,
    WAITING,
}SBLinkDataStatus;

//local device information
typedef struct
{
    int   PortsToOpen;              /* The port numbers that are opened on the local device */
    SBStatus_T SbCmdStatus;            /* Status of sent command */
    SbPacketState_T SbPacketState;  /* Current Sb packet byte position */
}SbDeviceInfoType;

typedef u8 BdAddrType[6];
/* This type is used to store the user-friendly name of a Bluetooth device. 
   The name is '\0' terminated. 
   LMX9830 accepts 40 Bytes in the name. */
typedef u8 BtNameType[40];  // Specific to LMX9830. Comment if not used.
typedef u8 BdDeviceClass[3];
typedef u8 BdFixedPIN[17];  // Max PIN size is 16 bytes 

typedef struct
{
    BdAddrType DeviceAddr;
    BdDeviceClass DeviceClass;
    BtNameType DeviceName;
    u8 RemoteCommNum;
}BtDeviceInfo;

// Sb Opcodes
#define GAP_INQUIRY                     0x00
#define GAP_DEVICE_FOUND                0x01
#define GAP_REMOTE_DEVICE_NAME          0x02
#define GAP_READ_LOCAL_NAME             0x03
#define GAP_WRITE_LOCAL_NAME            0x04
#define GAP_READ_LOCAL_BDA              0x05
#define GAP_SET_SCANMODE                0x06
#define SPP_SET_PORT_CONFIG             0x07
#define SPP_GET_PORT_CONFIG             0x08
#define SPP_PORT_CONFIG_CHANGED         0x09
#define SPP_ESTABLISH_LINK              0x0A
#define SPP_LINK_ESTABLISHED            0x0B
#define SPP_INCOMING_LINK_ESTABLISHED   0x0C
#define SPP_RELEASE_LINK                0x0D
#define SPP_LINK_RELEASED               0x0E
#define SPP_SEND_DATA                   0x0F
#define SPP_INCOMING_DATA               0x10
#define SPP_TRANSPARENT_MODE            0x11
#define SPP_CONNECT_DEFAULT_CON         0x12
#define SPP_STORE_DEFAULT_CON           0x13
#define SPP_GET_LIST_DEFAULT_CON        0x14
#define SPP_DELETE_DEFAULT_CON          0x15
#define GAP_GET_FIXED_PIN               0x16
#define GAP_SET_FIXED_PIN               0x17
#define GAP_GET_SECURITY_MODE           0x18
#define GAP_SET_SECURITY_MODE           0x19
#define RESTORE_FACTORY_SETTINGS        0x1A
#define GAP_REMOVE_PAIRING              0x1B
#define GAP_LIST_PAIRED_DEVICES         0x1C
#define FORCE_MASTER_ROLE               0x1D
#define SDAP_SERVICE_REQUEST            0x1E
#define GET_PORTS_TO_OPEN               0x1F
#define READ_RSSI                       0x20
#define GAP_ENTER_SNIFF_MODE            0x21
#define SET_PORTS_TO_OPEN               0x22
#define CHANGE_NVS_UART_SPEED           0x23
#define SB_TEST_MODE                    0x24
#define MODULE_READY                    0x25    
#define BT_RESET                        0x26
#define CHANGE_LOCAL_BDADDRESS          0x27
#define STORE_CLASS_OF_DEVICE           0x28
#define ENABLE_SDP_RECORD               0x29
#define DELETE_SDP_RECORDS              0x2A
#define STORE_SPP_RECORD                0x2B    // Not available for LMX9830.. Better not to be used anyway
#define STORE_DUN_RECORD                0x2C    // Not available for LMX9830.. Better not to be used anyway
#define STORE_FAX_RECORD                0x2D    // Not available for LMX9830.. Better not to be used anyway
#define STORE_OPP_RECORD                0x2E    // Not available for LMX9830.. Better not to be used anyway
#define STORE_FTP_RECORD                0x2F    // Not available for LMX9830.. Better not to be used anyway
#define STORE_SYNC_RECORD               0x30    // Not available for LMX9830.. Better not to be used anyway
#define STORE_SDP_RECORD                0x31
#define SDAP_CONNECT                    0x32
#define SDAP_DISCONNECT                 0x33
#define SDAP_CONNECTION_LOST            0x34
#define SDAP_SERVICE_BROWSE             0x35
#define SDAP_SERVICE_SEARCH             0x36
#define GAP_EXIT_SNIFF_MODE             0x37
#define GAP_ENTER_PARK_MODE             0x38
#define GAP_EXIT_PARK_MODE              0x39
#define GAP_ENTER_HOLD_MODE             0x3A
#define GAP_SET_LINK_POLICY             0x3B
#define GAP_GET_LINK_POLICY             0x3C
#define GAP_POWER_SAVE_MODE_CHANGED     0x3D
#define SPP_PORT_STATUS_CHANGED         0x3E
#define SDAP_ATTRIBUTE_REQUEST          0x3F
#define SPP_GET_PORT_STATUS             0x40
#define SPP_PORT_SET_DTR                0x41
#define SPP_PORT_SET_RTS                0x42
#define SPP_PORT_BREAK                  0x43
#define SPP_PORT_OVERRUN_ERROR          0x44
#define SPP_PORT_PARITY_ERROR           0x45
#define SPP_PORT_FRAMING_ERROR          0x46
#define FIRMWARE_UPGRADE                0x47    // LMX9820 specific
#define WRITE_ROM_PATCH                 0x47    // LMX9830 specific
#define CHANGE_UART_SETTINGS            0x48
#define READ_OPERATION_MODE             0x49
#define WRITE_OPERATION_MODE            0x4A
#define RF_TEST_MODE                    0x4B
#define SET_DEFAULT_LINK_POLICY         0x4C
#define GET_DEFAULT_LINK_POLICY         0x4D
#define SET_EVENT_FILTER                0x4E
#define GET_EVENT_FILTER                0x4F
#define GAP_ACL_ESTABLISHED             0x50
#define GAP_ACL_TERMINATED              0x51
#define DISABLE_TL                      0x52    
#define TL_ENABLED                      0x53
//unused entry 
#define SET_DEFAULT_LINK_TIMEOUT        0x55
#define GET_DEFAULT_LINK_TIMEOUT        0x56
#define SPP_SET_LINK_TIMEOUT            0x57
#define SPP_GET_LINK_TIMEOUT            0x58
#define GAP_SET_AUDIO_CONFIG            0x59
#define GAP_GET_AUDIO_CONFIG            0x5A
#define SET_DEFAULT_AUDIO_CONFIG        0x5B
#define GET_DEFAULT_AUDIO_CONFIG        0x5C
#define GAP_ESTABLISH_SCO_LINK          0x5D
#define GAP_RELEASE_SCO_LINK            0x5E
#define GAP_MUTE_MIC                    0x5F
#define GAP_SET_VOLUME                  0x60
#define GAP_GET_VOLUME                  0x61
#define GAP_CHANGE_SCO_PACKET           0x62
#define SET_DEFAULT_LINK_LATENCY        0x63
#define GET_DEFAULT_LINK_LATENCY        0x64
#define HCI_COMMAND                     0x65    // LMX9830 specific
#define AWAIT_INITIALIZATION_EVENT      0x66    // LMX9830 specific
#define ENTER_BLUETOOTH_MODE            0x66    // LMX9830 specific
#define SET_CLOCK_FREQUENCY             0x67    // LMX9830 specific    
#define GET_CLOCK_FREQUENCY             0x68    // LMX9830 specific
#define SET_CLOCK_AND_BAUDRATE          0x69    // LMX9830 specific
//unused entry 
#define SET_GPIO_WPU                    0x6B    // LMX9830 specific
#define GET_GPIO_STATE                  0x6C    // LMX9830 specific
#define SET_GPIO_DIRECTION              0x6D    // LMX9830 specific
#define SET_GPIO_OUTPUT_HIGH            0x6E    // LMX9830 specific
#define SET_GPIO_OUTPUT_LOW             0x6F    // LMX9830 specific
#define READ_MEM                        0x70    // LMX9830 specific
#define WRITE_MEM                       0x71    // LMX9830 specific
#define READ_NVS                        0x72    // LMX9830 specific
#define WRITE_NVS                       0x73    // LMX9830 specific
#define NVS_CHANGED_EVENT               0x73    // LMX9830 specific
#define SET_PCM_SLAVE_CONFIG            0x74    // LMX9830 specific
#define GAP_GET_PIN                     0x75    // LMX9830 specific

#define REQ	0x52
#define CFM	0x43
#define IND	0x69
#define RES	0x72
#define STX	0x02
#define ETX	0x03

typedef enum
{
    ERROR_OK                                = 0,
    ERROR_INVAID_NO_OF_PARAMETERS           = 1,
    ERROR_DURATION_OUT_OF_RANGE             = 2,
    ERROR_INVALID_MODE                      = 3,
    ERROR_TIMEOUT                           = 4,
    ERROR_UNKNOWN_ERROR                     = 5,
    ERROR_NAME_TOO_LONG                     = 6,
    ERROR_INVALID_DISCOVERABILITY_PARAMETER = 7,
    ERROR_INVALID_CONNECTABILITY_PARAMETER  = 8,
    ERROR_INVALID_SECURITY_MODE             = 9,
    ERROR_LINKEY_DOES_NOT_EXIST             = 0x0A,
    ERROR_CONNECTION_FAILED                 = 0x0B,
    ERROR_TRUNCATED_ANSWER                  = 0x0C,
    ERROR_RESULT_TOO_LARGE                  = 0x0D,
    ERROR_NOT_POSSIBLE_TO_ENTER_TESTMODE    = 0x0E,
    ERROR_ILLEGAL_TESTMODE                  = 0x0F,
    ERROR_RESET_TO_NSC_BDADDRESS            = 0x10,
    ERROR_UART_SPEED_OUT_OF_RANGE           = 0x11,
    ERROR_INVALID_PORT                      = 0x12,
    ERROR_ILLEGAL_STATE_VALUE               = 0x13,
    ERROR_IDENTIFIER_OUT_OF_RANGE           = 0x14,
    ERROR_RECORD_ALREADY_IN_SELECTED_STATE  = 0x15,
    ERROR_INVALID_AUTHENTICATION_VALUE      = 0x16,
    ERROR_INVALID_ENCRYPTION_VALUE          = 0x17,
    ERROR_MAXIMUM_NO_OF_SERVICE_RECORDS_REACHED = 0x18,
    ERROR_WRITING_TO_NVS                    = 0x19,
    ERROR_INVALID_ROLE                      = 0x1A,
    ERROR_LIMIT                             = 0x1B,
    ERROR_UNEXPECTED                        = 0x1C,
    ERROR_UNABLE_TO_SEND                    = 0x1D,
    ERROR_CURRENTLY_NO_BUFFER               = 0x1E,
    ERROR_NO_CONNECTION                     = 0x1F,
    ERROR_SPP_INVALID_PORT                  = 0x20,
    ERROR_SPP_PORT_NOT_OPEN                 = 0x21,
    ERROR_SPP_PORT_BUSY                     = 0x22,
    ERROR_SPP_MULTIPLE_CONNECTIONS          = 0x23,
    ERROR_SPP_MULTIPLE_TRANSPARENT          = 0x24,
    ERROR_SPP_DEFAULT_CONNECTION_NOT_STORED = 0x25,
    ERROR_SPP_AUTOMATIC_CONNECTION_PROGRESSING  = 0x26,
    ERROR_UNSPECIFIED_ERROR                 = 0x27,
    ERROR_IDENTIFIER_NOT_IN_USE             = 0x28,
    ERROR_INVALID_SUPPORTED_FAXCLASS_VALUE  = 0x29,
    ERROR_TOO_MANY_SUPPORTED_FORMATS        = 0x2A,
    ERROR_TOO_MANY_DATASTORES               = 0x2B,
    ERROR_ATTEMPT_FAILED                    = 0x2C,
    ERROR_ILLEGAL_LINK_POLICY               = 0x2D,
    ERROR_PINCODE_TOO_LONG                  = 0x2E,
    ERROR_PARITY_BIT_OUT_OF_RANGE           = 0x2F,
    ERROR_STOP_BIT_OUT_OF_RANGE             = 0x30,
    ERROR_ILLEGAL_LINK_TIMEOUT              = 0x31,
    ERROR_COMMAND_DISALLOWED                = 0x32,
    ERROR_ILLEGAL_AUDIO_CODEC_TYPE          = 0x33,
    ERROR_ILLEGAL_AUDIO_AIR_FORMAT          = 0x34,
    ERROR_SDP_RECORD_TOO_LONG               = 0x35,
    ERROR_SDP_FAILED_TO_CREATE_RECORD       = 0x36,
    ERROR_SET_VOLUME_FAILED                 = 0x37,
    ERROR_ILLEGAL_PACKAGE_TYPE              = 0x38,
    ERROR_INVALID_CODEC_SETTING             = 0x39,
    
    //-------------------------------------------------------//

    RFCS_NO_ERROR               = 0x00,
    RFCS_INVALID_DLC            = 0x01,
    RFCS_INVALID_PORT           = 0x02,
    RFCS_DLC_ESTABLISH_FAILED   = 0x03,
    RFCS_ACCESS_REJECTED        = 0x04,
    RFCS_INVALID_CONNECTION     = 0x05,
    RFCS_UNSPECIFIED_ERROR      = 0xFF,
    
    //-------------------------------------------------------//

    RFCR_DLC_DISC_LOCAL_DEVICE  = 0x00,   //The local device has disconnected the DLC.
    RFCR_DLC_DISC_REMOTE_DEVICE = 0x01,   //The remote device has disconnected the DLC.
    RFCR_DLC_DISC_ACL_FAILURE   = 0x02,   //ACL link failure/ link supervision timeout.
    RFCR_DLC_DISC_LOWER_LAYER   = 0x03,   //Lower layer (e.g. L2CAP) has disconnected the DLC.

    //-------------------------------------------------------//

    ERROR_STAT_OK               = ERROR_OK,
    ERROR_STAT_TIMEOUT,
    ERROR_STAT_ERROR,
    ERROR_STAT_UNSPECIFIED,
}BluetoothErrorCode;

u8 bluetooth_init(SbBaudrate btbaud);
SBStatus_T bluetooth_autodetectmodulebaud(SbBaudrate prefer_btbaud,
                                          SbBaudrate *detected_btbaud);
void bluetooth_changebaudrate(SbBaudrate btbaud);
void bluetooth_uart_tx(u8 *data, u16 length);
void bluetooth_uart_rx_irq_handler();
u8 bluetooth_receive_command(u8 *data, u16 *len);
u32 bluetooth_send_command(CMDIF_COMMAND cmd, u8 *data, u16 len);
u8 bluetooth_send_ack(CMDIF_COMMAND cmd, CMDIF_ACK resp, u8 *data, u16 len);
u8 bluetooth_send_ack_nowait(CMDIF_COMMAND cmd, CMDIF_ACK resp, u8 *data, u16 len);
u8 bluetooth_send_rawspp(u8 *txbuffer, u16 length);
u8 bluetooth_checkconnection(SbPortNumber btport);

#endif    //__BLUETOOTH_H
