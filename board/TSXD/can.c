/**
  **************** (C) COPYRIGHT 2011 SCT Performance, LLC *********************
  * File Name          : can.c
  * Device / System    : SCT products using the "CommonCode" platform
  * Author             : Quyen Leba
  *
  * Version            : 1 
  * Date               : 05/02/2011
  * Description        : 
  *                    : 
  *
  *
  * History            : 05/02/2011 M. Davis
  *                    :   Add SCT Copyright header,
  *
  ******************************************************************************
  */

#include <stm32f10x_gpio.h>
#include <stm32f10x_rcc.h>
#include <stm32f10x_type.h>
#include <common/statuscode.h>
#include "can.h"

#define PCLKCAN             36000

//static CAN_MSGOBJ can_txmsgobj = CAN_TX_MSGOBJ;
//static CAN_MSGOBJ can_rxmsgobj = CAN_RX_MSGOBJ_NORMAL;
CanRxMsg can_rxframe;

//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
void can_init_HSI(CAN_INITMODE initmode)
{
    CAN_InitTypeDef        CAN_InitStructure;
    CAN_FilterInitTypeDef  CAN_FilterInitStructure;
    GPIO_InitTypeDef       GPIO_InitStructure;
    u32 filter;
    
    /* CAN Periph clock enable */
    RCC_APB1PeriphClockCmd(RCC_APB1Periph_CAN, ENABLE);
    /* GPIOB enable*/
    RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOB | RCC_APB2Periph_GPIOG | 
                           RCC_APB2Periph_AFIO, ENABLE);
    
    GPIO_PinRemapConfig(GPIO_Remap1_CAN, ENABLE);
    
    /* Configure CAN pin: RX */
    GPIO_InitStructure.GPIO_Pin = GPIO_Pin_8;
    GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IPU;
    GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
    GPIO_Init(GPIOB, &GPIO_InitStructure);
    
    /* Configure CAN pin: TX */
    GPIO_InitStructure.GPIO_Pin = GPIO_Pin_9;
    GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AF_PP;
    GPIO_Init(GPIOB, &GPIO_InitStructure);  
    
    /* Configure CAN pin: CAN_En */
    GPIO_InitStructure.GPIO_Pin = GPIO_Pin_0;
    GPIO_InitStructure.GPIO_Mode = GPIO_Mode_Out_PP;
    GPIO_Init(GPIOG, &GPIO_InitStructure);
    
    /* CAN register init */
    CAN_DeInit();
    CAN_StructInit(&CAN_InitStructure);
    
    /* CAN cell init */
    CAN_InitStructure.CAN_TTCM=DISABLE;
    CAN_InitStructure.CAN_ABOM=DISABLE;
    CAN_InitStructure.CAN_AWUM=DISABLE;
    CAN_InitStructure.CAN_NART=DISABLE;
    CAN_InitStructure.CAN_RFLM=DISABLE;  
    CAN_InitStructure.CAN_TXFP=DISABLE;
    CAN_InitStructure.CAN_Mode=CAN_Mode_Normal;
    CAN_InitStructure.CAN_SJW=CAN_SJW_3tq;
    if (PCLKCAN == 36000)
    {
        CAN_InitStructure.CAN_BS1=CAN_BS1_13tq;
        CAN_InitStructure.CAN_BS2=CAN_BS2_4tq;
    }
    else    //32000
    {
        CAN_InitStructure.CAN_BS1=CAN_BS1_12tq;
        CAN_InitStructure.CAN_BS2=CAN_BS2_3tq;
    }
    CAN_InitStructure.CAN_Prescaler=4;
    CAN_Init(&CAN_InitStructure);
    
    //Standard ID CAN filter init
    //filter = (((0x5E0 & 0x1FFF) << 5) << 16) | CAN_ID_STD;
    if (initmode == CAN_INITMODE_7EX)
    {
        filter = (((u32)(0x7E0 & 0x1FFF) << 5) << 16) | CAN_ID_STD;
    }
    else //if (initmode == CAN_INITMODE_NORMAL)
    {
        filter = (((u32)(0x4A0 & 0x1FFF) << 5) << 16) | CAN_ID_STD;
    }
    
    CAN_FilterInitStructure.CAN_FilterNumber=0;
    CAN_FilterInitStructure.CAN_FilterMode=CAN_FilterMode_IdMask;//CAN_FilterMode_IdList;
    CAN_FilterInitStructure.CAN_FilterScale=CAN_FilterScale_32bit;
    CAN_FilterInitStructure.CAN_FilterIdHigh=(filter >> 16);
    CAN_FilterInitStructure.CAN_FilterIdLow=(filter & 0x0000FFFF);
    CAN_FilterInitStructure.CAN_FilterMaskIdHigh=(filter >> 16);
    CAN_FilterInitStructure.CAN_FilterMaskIdLow=(filter & 0x0000FFFF);
    CAN_FilterInitStructure.CAN_FilterFIFOAssignment=CAN_FIFO0;
    CAN_FilterInitStructure.CAN_FilterActivation=ENABLE;
    CAN_FilterInit(&CAN_FilterInitStructure);
    
    //Extended ID CAN filter init
    filter = ((0x18EFFA03 & 0x1FFFF) << 3) | CAN_ID_EXT;
    CAN_FilterInitStructure.CAN_FilterNumber=1;
    CAN_FilterInitStructure.CAN_FilterMode=CAN_FilterMode_IdMask;//CAN_FilterMode_IdList;
    CAN_FilterInitStructure.CAN_FilterScale=CAN_FilterScale_32bit;
    CAN_FilterInitStructure.CAN_FilterIdHigh=(filter >> 16);
    CAN_FilterInitStructure.CAN_FilterIdLow=(filter & 0x0000FFFF);
    CAN_FilterInitStructure.CAN_FilterMaskIdHigh=(filter >> 16);
    CAN_FilterInitStructure.CAN_FilterMaskIdLow=(filter & 0x0000FFFF);
    
    CAN_FilterInitStructure.CAN_FilterFIFOAssignment=CAN_FIFO1;
    CAN_FilterInitStructure.CAN_FilterActivation=ENABLE;
    CAN_FilterInit(&CAN_FilterInitStructure);
    
    GPIO_ResetBits(GPIOG, GPIO_Pin_0); // Enable CAN_En (active low)
    
    return;
}

//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
void can_init(CAN_INITMODE initmode)
{
    CAN_InitTypeDef        CAN_InitStructure;
    CAN_FilterInitTypeDef  CAN_FilterInitStructure;
    GPIO_InitTypeDef       GPIO_InitStructure;
    u32 filter;
    
    /* CAN Periph clock enable */
    RCC_APB1PeriphClockCmd(RCC_APB1Periph_CAN, ENABLE);
    /* GPIOB enable*/
    RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOB | RCC_APB2Periph_GPIOG | 
                           RCC_APB2Periph_AFIO, ENABLE);
    
    GPIO_PinRemapConfig(GPIO_Remap1_CAN, ENABLE);
    
    /* Configure CAN pin: RX */
    GPIO_InitStructure.GPIO_Pin = GPIO_Pin_8;
    GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IPU;
    GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
    GPIO_Init(GPIOB, &GPIO_InitStructure);
    
    /* Configure CAN pin: TX */
    GPIO_InitStructure.GPIO_Pin = GPIO_Pin_9;
    GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AF_PP;
    GPIO_Init(GPIOB, &GPIO_InitStructure);  
    
    /* Configure CAN pin: CAN_En */
    GPIO_InitStructure.GPIO_Pin = GPIO_Pin_0;
    GPIO_InitStructure.GPIO_Mode = GPIO_Mode_Out_PP;
    GPIO_Init(GPIOG, &GPIO_InitStructure);
    
    /* CAN register init */
    CAN_DeInit();
    CAN_StructInit(&CAN_InitStructure);
    
    /* CAN cell init */
    CAN_InitStructure.CAN_TTCM=DISABLE;
    CAN_InitStructure.CAN_ABOM=DISABLE;
    CAN_InitStructure.CAN_AWUM=DISABLE;
    CAN_InitStructure.CAN_NART=DISABLE;
    CAN_InitStructure.CAN_RFLM=DISABLE;  
    CAN_InitStructure.CAN_TXFP=DISABLE;
    CAN_InitStructure.CAN_Mode=CAN_Mode_Normal;
    CAN_InitStructure.CAN_SJW=CAN_SJW_3tq;
    CAN_InitStructure.CAN_BS1=CAN_BS1_13tq;
    CAN_InitStructure.CAN_BS2=CAN_BS2_4tq;
    CAN_InitStructure.CAN_Prescaler=4;
    CAN_Init(&CAN_InitStructure);
    
    //Standard ID CAN filter init
    //filter = (((0x5E0 & 0x1FFF) << 5) << 16) | CAN_ID_STD;
    if (initmode == CAN_INITMODE_7EX)
    {
        filter = (((u32)(0x7E0 & 0x1FFF) << 5) << 16) | CAN_ID_STD;
    }
    else //if (initmode == CAN_INITMODE_NORMAL)
    {
        filter = (((u32)(0x4A0 & 0x1FFF) << 5) << 16) | CAN_ID_STD;
    }
    
    CAN_FilterInitStructure.CAN_FilterNumber=0;
    CAN_FilterInitStructure.CAN_FilterMode=CAN_FilterMode_IdMask;//CAN_FilterMode_IdList;
    CAN_FilterInitStructure.CAN_FilterScale=CAN_FilterScale_32bit;
    CAN_FilterInitStructure.CAN_FilterIdHigh=(filter >> 16);
    CAN_FilterInitStructure.CAN_FilterIdLow=(filter & 0x0000FFFF);
    CAN_FilterInitStructure.CAN_FilterMaskIdHigh=(filter >> 16);
    CAN_FilterInitStructure.CAN_FilterMaskIdLow=(filter & 0x0000FFFF);
    CAN_FilterInitStructure.CAN_FilterFIFOAssignment=CAN_FIFO0;
    CAN_FilterInitStructure.CAN_FilterActivation=ENABLE;
    CAN_FilterInit(&CAN_FilterInitStructure);
    
    //Extended ID CAN filter init
    filter = ((0x18EFFA03 & 0x1FFFF) << 3) | CAN_ID_EXT;
    CAN_FilterInitStructure.CAN_FilterNumber=1;
    CAN_FilterInitStructure.CAN_FilterMode=CAN_FilterMode_IdMask;//CAN_FilterMode_IdList;
    CAN_FilterInitStructure.CAN_FilterScale=CAN_FilterScale_32bit;
    CAN_FilterInitStructure.CAN_FilterIdHigh=(filter >> 16);
    CAN_FilterInitStructure.CAN_FilterIdLow=(filter & 0x0000FFFF);
    CAN_FilterInitStructure.CAN_FilterMaskIdHigh=(filter >> 16);
    CAN_FilterInitStructure.CAN_FilterMaskIdLow=(filter & 0x0000FFFF);
    
    CAN_FilterInitStructure.CAN_FilterFIFOAssignment=CAN_FIFO1;
    CAN_FilterInitStructure.CAN_FilterActivation=ENABLE;
    CAN_FilterInit(&CAN_FilterInitStructure);
    
    GPIO_ResetBits(GPIOG, GPIO_Pin_0); // Enable CAN_En (active low)
    
    return;
}

//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
void can_setfiltermode_standardid()
{
}

//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
void can_setfiltermode_extendedid()
{
}

//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
void can_deinit()
{
    /* CAN Periph clock enable */
    RCC_APB1PeriphClockCmd(RCC_APB1Periph_CAN, DISABLE);

    GPIO_PinRemapConfig(GPIO_Remap1_CAN, DISABLE);    
    GPIO_SetBits(GPIOG, GPIO_Pin_0); // Disable CAN_En (active low)
    
    /* CAN register init */
    //CAN_DeInit();
}

//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
void can_setTxMsgObj(CAN_MSGOBJ msgobj)
{
    //can_txmsgobj = msgobj;
}

void can_setRxMsgObj(CAN_MSGOBJ msgobj)
{
    //can_rxmsgobj = msgobj;
}

//------------------------------------------------------------------------------
// Function Name  : can_tx_s
// Description    : CAN Transmit
// Input          : None
// Output         : None
// Return         : return S_SUCCESS or S_FAIL
//------------------------------------------------------------------------------
u8 can_tx_s(CanTxMsg TxMessage)
{
    u32 i = 0;
    u8 TransmitMailbox = 0;
    
    /* transmit */    
    TransmitMailbox=CAN_Transmit(&TxMessage);
    
    i = 0;
    while((CAN_TransmitStatus(TransmitMailbox) != CANTXOK) && (i != 0xFFF))
    {
        i++;
    }
    if(i == 0xFFF)
    {
        return S_FAIL;
    }
    else
    {
        return S_SUCCESS;
    }
}

//------------------------------------------------------------------------------
// Function Name  : can_tx
// Description    : CAN Transmit
// Input          : None
// Output         : None
// Return         : return S_SUCCESS or S_FAIL
//------------------------------------------------------------------------------
u8 can_tx(CanTxMsg TxMessage)
{
    u8 TransmitMailbox = 0;
    
    /* transmit */
    TransmitMailbox=CAN_Transmit(&TxMessage);
    while(CAN_TransmitStatus(TransmitMailbox) != CANTXOK);
    return S_SUCCESS;
}

//------------------------------------------------------------------------------
// Function Name  : can_tx
// Description    : CAN Transmit
// Input          : None
// Output         : None
// Return         : return S_SUCCESS or S_FAIL
//------------------------------------------------------------------------------
u8 can_tx_ext(CanTxMsg TxMessage)
{
    u8 TransmitMailbox = 0;
    
    /* transmit */
    TransmitMailbox=CAN_Transmit(&TxMessage);
    while(CAN_TransmitStatus(TransmitMailbox) != CANTXOK);
    return S_SUCCESS;
}

//------------------------------------------------------------------------------
// Function Name  : CAN_RX
// Description    : CAN Receive
// Input          : u32 timeout (simple count)
// Output         : None
// Return         : return S_SUCCESS or S_FAIL
//------------------------------------------------------------------------------
u8 can_rx(u32 timeout)
{
    u32 i = 0;

    while((CAN_MessagePending(CAN_FIFO0) < 1) && (i < timeout))
    {
        i++;
    }

    if(i >= timeout)
    {
        return S_FAIL;
    }
    
    /* receive */
    CAN_Receive(CAN_FIFO0, &can_rxframe);
    
    return S_SUCCESS; /* Test Passed */
}

//------------------------------------------------------------------------------
// Function Name  : CAN_RX
// Description    : CAN Receive
// Input          : u32 timeout (simple count)
// Output         : None
// Return         : return S_SUCCESS or S_FAIL
//------------------------------------------------------------------------------
u8 can_rx_ext(u32 timeout)
{
    u32 i = 0;

    while((CAN_MessagePending(CAN_FIFO1) < 1) && (i < timeout))
    {
        i++;
    }

    if(i >= timeout)
    {
        return S_FAIL;
    }
    
    /* receive */
    CAN_Receive(CAN_FIFO1, &can_rxframe);
    
    return S_SUCCESS; /* Test Passed */
}
