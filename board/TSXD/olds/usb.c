/**
  **************** (C) COPYRIGHT 2011 SCT Performance, LLC *********************
  * File Name          : usb.c
  * Device / System    : SCT products using the "CommonCode" platform
  * Author             : Quyen Leba
  *
  * Version            : 1 
  * Date               : 05/02/2011
  * Description        : 
  *                    : 
  *
  *
  * History            : 05/02/2011 M. Davis
  *                    :   Add SCT Copyright header,
  *
  ******************************************************************************
  */

#include <stm32f10x_it.h>
#include <stm32f10x_rcc.h>
#include <stm32f10x_gpio.h>
#include <USBLib/include/usb_regs.h>
#include <USBLib/include/usb_core.h>
#include "usb.h"

//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
#define USB_DISCONNECT                      GPIOG  
#define USB_DISCONNECT_PIN                  GPIO_Pin_13 
#define RCC_APB2Periph_GPIO_DISCONNECT      RCC_APB2Periph_GPIOG 

//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
void usb_disconnect_config()
{
    GPIO_InitTypeDef GPIO_InitStructure;
    
    //Enable USB_DISCONNECT GPIO clock
    RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIO_DISCONNECT, ENABLE);
    
    GPIO_InitStructure.GPIO_Pin = USB_DISCONNECT_PIN;
    GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
    GPIO_InitStructure.GPIO_Mode = GPIO_Mode_Out_PP;
    GPIO_Init(USB_DISCONNECT, &GPIO_InitStructure);
    GPIO_ResetBits(USB_DISCONNECT, USB_DISCONNECT_PIN);  
}

//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
void usb_poweroff()
{
    //disable all ints and force USB reset
    _SetCNTR(CNTR_FRES);
    //clear interrupt status register
    _SetISTR(0);
    //Disable the Pull-Up
    usb_cable_config(DISABLE);
    //switch-off device
    _SetCNTR(CNTR_FRES + CNTR_PDWN);
}

//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
void usb_cable_config(FunctionalState newstate)
{
    GPIO_InitTypeDef GPIO_InitStructure;
    if (newstate != DISABLE)
    {
        //USB_DISCONNECT_PIN used as USB pull-up
        GPIO_InitStructure.GPIO_Pin = USB_DISCONNECT_PIN;
        GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
        GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IN_FLOATING;
        GPIO_Init(USB_DISCONNECT, &GPIO_InitStructure);
    }
    else
    {
        //USB_DISCONNECT_PIN used as USB pull-up
        GPIO_InitStructure.GPIO_Pin = USB_DISCONNECT_PIN;
        GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
        GPIO_InitStructure.GPIO_Mode = GPIO_Mode_Out_PP;
        GPIO_Init(USB_DISCONNECT, &GPIO_InitStructure);
        GPIO_ResetBits(USB_DISCONNECT, USB_DISCONNECT_PIN);        
    }
}

//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
void usb_set_clock()
{
    //USBCLK = PLLCLK
    RCC_USBCLKConfig(RCC_USBCLKSource_PLLCLK_1Div5);
    
    //Enable USB clock
    RCC_APB1PeriphClockCmd(RCC_APB1Periph_USB, ENABLE);
}

//------------------------------------------------------------------------------
//Restores system clocks and power while exiting suspend mode
//------------------------------------------------------------------------------
void usb_leave_lowpowermode()
{
    DEVICE_INFO *pInfo = &Device_Info;
    
    //Set the device state to the correct state
    if (pInfo->Current_Configuration != 0)
    {
        //Device configured
        bDeviceState = CONFIGURED;
    }
    else
    {
        bDeviceState = ATTACHED;
    }
}

//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
void usb_init()
{
    pInformation = &Device_Info;
    pInformation->ControlState = 2;
    pProperty = &Device_Property;
    pUser_Standard_Requests = &User_Standard_Requests;
    //Initialize devices one by one
    pProperty->Init();
}