/**
  **************** (C) COPYRIGHT 2011 SCT Performance, LLC *********************
  * File Name          : usb_pwr.h
  * Device / System    : SCT products using the "CommonCode" platform
  * Author             : Quyen Leba
  *
  * Version            : 1 
  * Date               : 05/02/2011
  * Description        : 
  *                    : 
  *
  *
  * History            : 05/02/2011 M. Davis
  *                    :   Add SCT Copyright header,
  *
  ******************************************************************************
  */

#ifndef __USB_PWR_H
#define __USB_PWR_H

#include <arch/gentype.h>
#include <USBLib/include/usb_core.h>
#include <USBLib/include/usb_regs.h>

typedef enum _USB_PWR_RESUME_STATE
{
    RESUME_EXTERNAL,
    RESUME_INTERNAL,
    RESUME_LATER,
    RESUME_WAIT,
    RESUME_START,
    RESUME_ON,
    RESUME_OFF,
    RESUME_ESOF
}USB_PWR_RESUME_STATE;

typedef enum _USB_PWR_DEVICE_STATE
{
    UNCONNECTED,
    ATTACHED,
    POWERED,
    SUSPENDED,
    ADDRESSED,
    CONFIGURED
}USB_PWR_DEVICE_STATE;

void usb_pwr_suspend();
void usb_pwr_resume_init();
void usb_pwr_resume(USB_PWR_RESUME_STATE eResumeSetVal);
RESULT usb_pwr_poweron();
RESULT usb_pwr_poweroff();

//USB device status
extern vu32 bDeviceState;
//true when suspend is possible
extern volatile bool fSuspendEnabled;

#endif	//__USB_PWR_H
