/**
  **************** (C) COPYRIGHT 2011 SCT Performance, LLC *********************
  * File Name          : mass_mal.h
  * Device / System    : SCT products using the "CommonCode" platform
  * Author             : Quyen Leba
  *
  * Version            : 1 
  * Date               : 05/02/2011
  * Description        : 
  *                    : 
  *
  *
  * History            : 05/02/2011 M. Davis
  *                    :   Add SCT Copyright header,
  *
  ******************************************************************************
  */

/******************** (C) COPYRIGHT 2008, 2009 SCT LLC *************************
* File Name          : mas_mal.h
* Device / System    : TSX - (Touch Screen eXtreme) Tuner System... Vehicle Interface Module
* Author             : (C) COPYRIGHT 2008 STMicroelectronics - used with permission... all
*                      subsequent modifications copyright SCT LLC
*
* Version            :
* Date               : 09/22/2008 STMicroelectronics
* Description        : TSX Vehicle Interface Module (dongle) application - 
*                      header file for mass_mal.c - driver for mass storage media access layer
*
* Rev History, most recent first
* $History: /CommonCode/Mico/board/TSXD/mass_mal.h $
*  
*  ****************** Version 1 ****************** 
*  User: qleba   Date: 2011-05-17   Time: 23:01:23-04:00 
*  Updated in: /CommonCode/Mico/board/TSXD 
*  
*  ****************** Version 1 ****************** 
*  User: qleba   Date: 2010-11-01   Time: 12:21:05-04:00 
*  Updated in: /mico/board/TSXD 
*  
*  ****************** Version 2 ****************** 
*  User: mdavis   Date: 2009-09-30   Time: 23:18:04-04:00 
*  Updated in: /TSX Vehicle Interface/Firmware/TSXVehicleInterface_PDlatest/include 
*  development build 1.3.101.2... documentary changes only - 
*  Add or update copyright header to latest standard. 
*  
*/

/******************** (C) COPYRIGHT 2008 STMicroelectronics ********************
* File Name          : mass_mal.h
* Author             : MCD Application Team
* Version            : V1.1.2
* Date               : 09/22/2008
* Description        : Header for mass_mal.c file.
********************************************************************************
* THE PRESENT FIRMWARE WHICH IS FOR GUIDANCE ONLY AIMS AT PROVIDING CUSTOMERS
* WITH CODING INFORMATION REGARDING THEIR PRODUCTS IN ORDER FOR THEM TO SAVE TIME.
* AS A RESULT, STMICROELECTRONICS SHALL NOT BE HELD LIABLE FOR ANY DIRECT,
* INDIRECT OR CONSEQUENTIAL DAMAGES WITH RESPECT TO ANY CLAIMS ARISING FROM THE
* CONTENT OF SUCH FIRMWARE AND/OR THE USE MADE BY CUSTOMERS OF THE CODING
* INFORMATION CONTAINED HEREIN IN CONNECTION WITH THEIR PRODUCTS.
*******************************************************************************/

/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __MASS_MAL_H
#define __MASS_MAL_H

/* Includes ------------------------------------------------------------------*/
/* Exported types ------------------------------------------------------------*/
/* Exported constants --------------------------------------------------------*/
#define MAL_OK   0
#define MAL_FAIL 1
#define MAX_LUN  1

/* Exported macro ------------------------------------------------------------*/
/* Exported functions ------------------------------------------------------- */

u16 MAL_Init(u8 lun);
u16 MAL_GetStatus (u8 lun);
u16 MAL_Read(u8 lun, u32 Memory_Offset, u8 *Readbuff, u16 Transfer_Length);
u16 MAL_Write(u8 lun, u32 Memory_Offset, u8 *Writebuff, u16 Transfer_Length);
int ftl_writesector1(void *data, unsigned long sector);
int ftl_readsector1(void *data, unsigned long sector);

#endif /* __MASS_MAL_H */

/******************* (C) COPYRIGHT 2008 STMicroelectronics *****END OF FILE****/
