/**
  **************** (C) COPYRIGHT 2011 SCT Performance, LLC *********************
  * File Name          : button.c
  * Device / System    : SCT products using the "CommonCode" platform
  * Author             : Quyen Leba
  *
  * Version            : 1 
  * Date               : 05/02/2011
  * Description        : 
  *                    : 
  *
  *
  * History            : 05/02/2011 M. Davis
  *                    :   Add SCT Copyright header,
  *
  ******************************************************************************
  */

#include <stm32f10x_gpio.h>
#include "system.h"
#include "button.h"

//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
void button_init()
{
    GPIO_InitTypeDef GPIO_InitStructure;
    /* Configure PF.4, as input pull up */
    GPIO_InitStructure.GPIO_Pin = GPIO_Pin_4;
    GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
    GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IPU;
    GPIO_Init(GPIOF, &GPIO_InitStructure);    
}

//------------------------------------------------------------------------------
// Active LOW button
//------------------------------------------------------------------------------
u8 button_getState(void)
{
    return GPIO_ReadInputDataBit(GPIOF, GPIO_Pin_4);
}
