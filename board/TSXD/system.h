/**
  **************** (C) COPYRIGHT 2011 SCT Performance, LLC *********************
  * File Name          : system.h
  * Device / System    : SCT products using the "CommonCode" platform
  * Author             : Quyen Leba
  *
  * Version            : 1 
  * Date               : 05/02/2011
  * Description        : 
  *                    : 
  *
  *
  * History            : 05/02/2011 M. Davis
  *                    :   Add SCT Copyright header,
  *
  ******************************************************************************
  */

#ifndef __SYSTEM_H
#define __SYSTEM_H

#include <stdlib.h>
#include <stm32f10x_type.h>

#define DEFAULT_TUNE_FOLDER_NAME            "STRATEGY"
#define DEFAULT_TUNE_VERSION_NAME           "tuneversion.txt"

void emi_init();

#endif