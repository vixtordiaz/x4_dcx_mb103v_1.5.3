/**
  **************** (C) COPYRIGHT 2011 SCT Performance, LLC *********************
  * File Name          : led.c
  * Device / System    : SCT products using the "CommonCode" platform
  * Author             : Quyen Leba
  *
  * Version            : 1 
  * Date               : 05/02/2011
  * Description        : 
  *                    : 
  *
  *
  * History            : 05/02/2011 M. Davis
  *                    :   Add SCT Copyright header,
  *
  ******************************************************************************
  */

#include <stm32f10x_gpio.h>
#include "led.h"

//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
void led_init()
{
    GPIO_InitTypeDef GPIO_InitStructure;
    /* Configure PF.0, PF.1 as output OD */
    GPIO_InitStructure.GPIO_Pin = GPIO_Pin_0 | GPIO_Pin_1;
    //GPIO_InitStructure.GPIO_Mode = GPIO_Mode_Out_OD;
    GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
    GPIO_InitStructure.GPIO_Mode = GPIO_Mode_Out_PP;
    GPIO_Init(GPIOF, &GPIO_InitStructure);
    led_setState(OFF);
}

//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
void led_setState(LED_State state)
{
    switch(state)
    {
    case OFF:
        GPIO_SetBits(GPIOF, GPIO_Pin_0 | GPIO_Pin_1);
        break;
    case RED:
        GPIO_SetBits(GPIOF, GPIO_Pin_0);
        GPIO_ResetBits(GPIOF, GPIO_Pin_1);        
        break;
    case GREEN:
        GPIO_SetBits(GPIOF, GPIO_Pin_1);
        GPIO_ResetBits(GPIOF, GPIO_Pin_0);
        break;
    case YELLOW:
        GPIO_ResetBits(GPIOF, GPIO_Pin_0 | GPIO_Pin_1);
        break;
    default:
        break;
    }
}
