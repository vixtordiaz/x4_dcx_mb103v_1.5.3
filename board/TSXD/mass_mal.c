/**
  **************** (C) COPYRIGHT 2011 SCT Performance, LLC *********************
  * File Name          : mass_mal.c
  * Device / System    : SCT products using the "CommonCode" platform
  * Author             : Quyen Leba
  *
  * Version            : 1 
  * Date               : 05/02/2011
  * Description        : 
  *                    : 
  *
  *
  * History            : 05/02/2011 M. Davis
  *                    :   Add SCT Copyright header,
  *
  ******************************************************************************
  */

/******************** (C) COPYRIGHT 2008, 2009 SCT LLC *************************
* File Name          : mas_mal.c
* Device / System    : TSX - (Touch Screen eXtreme) Tuner System... Vehicle Interface Module
* Author             : (C) COPYRIGHT 2008 STMicroelectronics - used with permission... all
*                      subsequent modifications copyright SCT LLC
*
* Version            :
* Date               : 09/22/2008 STMicroelectronics
* Description        : TSX Vehicle Interface Module (dongle) application - 
*                      driver for mass storage media access layer
*
* Rev History, most recent first
* $History: /CommonCode/Mico/board/TSXD/mass_mal.c $
*  
*  ****************** Version 1 ****************** 
*  User: qleba   Date: 2011-05-17   Time: 23:01:23-04:00 
*  Updated in: /CommonCode/Mico/board/TSXD 
*  
*  ****************** Version 1 ****************** 
*  User: qleba   Date: 2010-11-01   Time: 12:21:05-04:00 
*  Updated in: /mico/board/TSXD 
*  
*  ****************** Version 2 ****************** 
*  User: mdavis   Date: 2009-09-30   Time: 23:17:35-04:00 
*  Updated in: /TSX Vehicle Interface/Firmware/TSXVehicleInterface_PDlatest/app 
*  development build 1.3.101.2... documentary changes only - 
*  Add or update copyright header to latest standard. 
*  
*/

/******************** (C) COPYRIGHT 2008 STMicroelectronics ********************
* File Name          : mass_mal.c
* Author             : MCD Application Team
* Version            : V1.1.2
* Date               : 09/22/2008
* Description        : Medium Access Layer interface
********************************************************************************
* THE PRESENT FIRMWARE WHICH IS FOR GUIDANCE ONLY AIMS AT PROVIDING CUSTOMERS
* WITH CODING INFORMATION REGARDING THEIR PRODUCTS IN ORDER FOR THEM TO SAVE TIME.
* AS A RESULT, STMICROELECTRONICS SHALL NOT BE HELD LIABLE FOR ANY DIRECT,
* INDIRECT OR CONSEQUENTIAL DAMAGES WITH RESPECT TO ANY CLAIMS ARISING FROM THE
* CONTENT OF SUCH FIRMWARE AND/OR THE USE MADE BY CUSTOMERS OF THE CODING
* INFORMATION CONTAINED HEREIN IN CONNECTION WITH THEIR PRODUCTS.
*******************************************************************************/

/* Includes ------------------------------------------------------------------*/
#include "fsmc_nand.h"
#include "nand_if.h"
#include "mass_mal.h"
#include "usb_hw_config.h"
#include "mlayer.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define xl_open ml_open
#define xl_read ml_read
#define xl_write ml_write
#define xl_close ml_close
#define xl_getmaxsector ml_getmaxsector
#define xl_init ml_init

#define XL_READ ML_READ
#define XL_WRITE ML_WRITE

/* Private typedef -----------------------------------------------------------*/
/* Private define ------------------------------------------------------------*/
/* Private macro -------------------------------------------------------------*/
/* Private variables ---------------------------------------------------------*/
u32 Mass_Memory_Size[2];
u32 Mass_Block_Size[2];
u32 Mass_Block_Count[2];
vu32 Status =0;

/* Private function prototypes -----------------------------------------------*/
/* Private functions ---------------------------------------------------------*/
/*******************************************************************************
* Function Name  : MAL_Init
* Description    : Initializes the Media on the STM32
* Input          : None
* Output         : None
* Return         : None
*******************************************************************************/
u16 MAL_Init(u8 lun)
{
  u16 status = MAL_OK;
  
  if(lun == 0)
      status = ml_init();
  else 
      return MAL_FAIL;
  
  return status;
}
/*******************************************************************************
* Function Name  : MAL_Write
* Description    : Write sectors
* Input          : None
* Output         : None
* Return         : None
*******************************************************************************/
u16 MAL_Write(u8 lun, u32 Memory_Offset, u8 *Writebuff, u16 Transfer_Length)
{
  int status = 0;
  u32 FtlOffset = Memory_Offset/2048;
  u32 BlockOffset = Memory_Offset%2048;
  char *sectorbuf;
      
  if(lun == 0)
  {
      if(Transfer_Length < 2048)
      {
          sectorbuf = malloc(2048);
          if(sectorbuf == NULL)
              return MAL_FAIL;
          
          status = ftl_readsector1(sectorbuf, FtlOffset); // 2048byte Micron
          memcpy(sectorbuf+BlockOffset, Writebuff, Transfer_Length);
          status = ftl_writesector1(sectorbuf, FtlOffset); // 2048byte Micron
          free(sectorbuf);          
          if(status!=0)
          {                    
              return MAL_FAIL;
          }
      }
      else
      {          
          //status = ftl_writesector1(Writebuff, (Memory_Offset/512)); // 512byte ST part
          status = ftl_writesector1(Writebuff, FtlOffset); // 2048byte Micron
          if(status!=0)
          {                    
              return MAL_FAIL;
         }
      }
  }
  else
  {
      return MAL_FAIL;
  }
          
  return MAL_OK;
}

/*******************************************************************************
* Function Name  : MAL_Read
* Description    : Read sectors
* Input          : None
* Output         : None
* Return         : Buffer pointer
*******************************************************************************/
u16 MAL_Read(u8 lun, u32 Memory_Offset, u8 *Readbuff, u16 Transfer_Length)
{
  int status = 0;
  u32 FtlOffset = Memory_Offset/2048;
  u32 BlockOffset = Memory_Offset%2048;
  
  char *sectorbuf = malloc(2048);
  if(sectorbuf == NULL)
      return MAL_FAIL;
  
  if(lun == 0)
  {
      //status = ftl_readsector1(Readbuff, (Memory_Offset/512)); // 512byte ST part
      status = ftl_readsector1(sectorbuf, FtlOffset); // 2048byte Micron
      memcpy(Readbuff, sectorbuf+BlockOffset, 512);
      free(sectorbuf);
      if(status!=0)
          return MAL_FAIL;
  }
  else
  {
      free(sectorbuf);
      return MAL_FAIL;      
  }
          
  return MAL_OK;
}

/*******************************************************************************
* Function Name  : MAL_GetStatus
* Description    : Get status
* Input          : None
* Output         : None
* Return         : None
*******************************************************************************/
u16 MAL_GetStatus (u8 lun)
{
  NAND_IDTypeDef NAND_ID;

  if(lun == 0)
  {
      FSMC_NAND_ReadID(&NAND_ID);
      if (NAND_ID.Device_ID != 0 )
      {
          // only one zone is used 
          Mass_Block_Count[0] = 524288;//NAND_ZONE_SIZE * NAND_BLOCK_SIZE * NAND_MAX_ZONE ;
          Mass_Block_Size[0]  = 512;//NAND_PAGE_SIZE;
          Mass_Memory_Size[0] = (Mass_Block_Count[0] * Mass_Block_Size[0]);     
          return MAL_OK;
      }
  }
  
  return MAL_FAIL;
}

int ftl_writesector1(void *data, unsigned long sector)
{
/*  fnPr("writesector: %d\n",sector); */
/*  if (((long)data)&3) fnPr("FTL: not aligned write!\n"); */

	if (xl_open(sector,1,XL_WRITE)) return 1;

	if (xl_write((unsigned char *)data))
	{
		(void)xl_close();
		return 1;
	}

	if (xl_close()) return 1;

	return 0;
}

int ftl_readsector1(void *data, unsigned long sector)
{
/*  fnPr("readsector: %d\n",sector); */
/*  if (((long)data)&3) fnPr("FTL: not aligned read!\n"); */

	if (xl_open(sector,1,XL_READ)) return 1;

	if (xl_read((unsigned char *)data))
	{
		(void)xl_close();
		return 1;
	}

	if (xl_close()) return 1;

	return 0;
}



/******************* (C) COPYRIGHT 2008 STMicroelectronics *****END OF FILE****/
