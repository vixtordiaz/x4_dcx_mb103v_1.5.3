/**
  **************** (C) COPYRIGHT 2011 SCT Performance, LLC *********************
  * File Name          : fs.h
  * Device / System    : SCT products using the "CommonCode" platform
  * Author             : Quyen Leba
  *
  * Version            : 1 
  * Date               : 05/02/2011
  * Description        : 
  *                    : 
  *
  *
  * History            : 05/02/2011 M. Davis
  *                    :   Add SCT Copyright header,
  *
  ******************************************************************************
  */

#ifndef __FS_H
#define __FS_H

#include <arch/gentype.h>
#include "api_f.h"

void fs_init();
u8 fs_reinit();
u8 fs_format();
F_DRIVER * fs_driver_init();

#endif  //__FS_H
