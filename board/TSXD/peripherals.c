/**
  **************** (C) COPYRIGHT 2011 SCT Performance, LLC *********************
  * File Name          : peripherals.c
  * Device / System    : SCT products using the "CommonCode" platform
  * Author             : Quyen Leba
  *
  * Version            : 1 
  * Date               : 05/02/2011
  * Description        : 
  *                    : 
  *
  *
  * History            : 05/02/2011 M. Davis
  *                    :   Add SCT Copyright header,
  *
  ******************************************************************************
  */

#include <stm32f10x_rcc.h>
#include <board/power.h>
#include <board/timer.h>
#include <board/rtc.h>
#include <board/peripherals.h>
#include <common/obd2can.h>
#include "button.h"
#include "nand.h"
#include "led.h"

//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
void peripherals_init()
{
    //Enable GPIOA, GPIOB, GPIOC, GPIOD, GPIOE, GPIOF, GPIOG and AFIO clocks
    RCC_APB2PeriphClockCmd
        (RCC_APB2Periph_GPIOA | RCC_APB2Periph_GPIOB |RCC_APB2Periph_GPIOC |
         RCC_APB2Periph_GPIOD | RCC_APB2Periph_GPIOE | RCC_APB2Periph_GPIOF |
         RCC_APB2Periph_GPIOG | RCC_APB2Periph_AFIO,
         ENABLE);  

    RCC_APB2PeriphClockCmd(RCC_APB2Periph_AFIO, ENABLE);
    
    //Enable CRC clock
    RCC_AHBPeriphClockCmd(RCC_AHBPeriph_CRC, ENABLE);
    
    timer1_init();
    timer2_init();
    
    //timer_init();
    //timer1_init();
    //emi_init();
    //Init_USB();
    power_init();
    
    adc_init(ADC_DATALOG);
    led_init();
    button_init();
    led_setState(YELLOW);
    rtc_init();
    nand_init();
    //TODOQ: bluetooth
}
