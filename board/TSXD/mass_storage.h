/**
  **************** (C) COPYRIGHT 2011 SCT Performance, LLC *********************
  * File Name          : mass_storage.h
  * Device / System    : SCT products using the "CommonCode" platform
  * Author             : Quyen Leba
  *
  * Version            : 1 
  * Date               : 05/02/2011
  * Description        : 
  *                    : 
  *
  *
  * History            : 05/02/2011 M. Davis
  *                    :   Add SCT Copyright header,
  *
  ******************************************************************************
  */

/******************** (C) COPYRIGHT 2009 SCT LLC *************************
* File Name          : mass_storage.h
* Device / System    : TSX - (Touch Screen eXtreme) Tuner System... Vehicle Interface Module
* Author             : adapted form STMicroelectronics STM32F10x demo application, with
*                      modifications by P. Downs, Q.Leba, M. Davis
*
* Version            :
* Date               : 10/23/2008
* Description        : header for mass_storage.c -- USB mass-storage functionality, using NAND memory
*
* Rev History, most recent first
* $History: /CommonCode/Mico/board/TSXD/mass_storage.h $
*  
*  ****************** Version 1 ****************** 
*  User: qleba   Date: 2011-05-17   Time: 23:01:23-04:00 
*  Updated in: /CommonCode/Mico/board/TSXD 
*  
*  ****************** Version 1 ****************** 
*  User: qleba   Date: 2010-11-01   Time: 12:21:05-04:00 
*  Updated in: /mico/board/TSXD 
*  
*  ****************** Version 3 ****************** 
*  User: mdavis   Date: 2009-09-25   Time: 10:17:15-04:00 
*  Updated in: /TSX Vehicle Interface/Firmware/TSXVehicleInterface_PDlatest/include 
*  Merging production-programming updates with Build 52. 
*  
*/

/******************** (C) COPYRIGHT 2008 STMicroelectronics ********************
* File Name          : mass_storage.h
* Author             : MCD Application Team
* Version            : V1.1.2
* Date               : 09/22/2008
* Description        : This file contains all the functions prototypes for the
*                      Mass Storage firmware driver.
********************************************************************************
* THE PRESENT FIRMWARE WHICH IS FOR GUIDANCE ONLY AIMS AT PROVIDING CUSTOMERS
* WITH CODING INFORMATION REGARDING THEIR PRODUCTS IN ORDER FOR THEM TO SAVE TIME.
* AS A RESULT, STMICROELECTRONICS SHALL NOT BE HELD LIABLE FOR ANY DIRECT,
* INDIRECT OR CONSEQUENTIAL DAMAGES WITH RESPECT TO ANY CLAIMS ARISING FROM THE
* CONTENT OF SUCH FIRMWARE AND/OR THE USE MADE BY CUSTOMERS OF THE CODING
* INFORMATION CONTAINED HEREIN IN CONNECTION WITH THEIR PRODUCTS.
*******************************************************************************/

/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __MASS_STORAGE_H
#define __MASS_STORAGE_H

/* Includes ------------------------------------------------------------------*/
#include <stm32f10x_lib.h>
#include <USBLib/include/usb_lib.h>
#include "usb_istr.h"
#include "usb_pwr.h"
#include "usb_hw_config.h"

/* Exported types ------------------------------------------------------------*/
/* Exported constants --------------------------------------------------------*/
/* Exported macro ------------------------------------------------------------*/
/* Exported functions ------------------------------------------------------- */
void Mass_Storage_Init(void);
void Mass_Storage_Start (void);
void Mass_Storage_Stop (void);
// void Mass_Storage_Recovery (void);
void Mass_Storage_Select();

#endif /* __MASS_STORAGE_H */

/******************* (C) COPYRIGHT 2008 STMicroelectronics *****END OF FILE****/
