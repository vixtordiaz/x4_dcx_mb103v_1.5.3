/**
  **************** (C) COPYRIGHT 2011 SCT Performance, LLC *********************
  * File Name          : bluetooth_api.h
  * Device / System    : SCT products using the "CommonCode" platform
  * Author             : Quyen Leba
  *
  * Version            : 1 
  * Date               : 05/02/2011
  * Description        : 
  *                    : 
  *
  *
  * History            : 05/02/2011 M. Davis
  *                    :   Add SCT Copyright header,
  *
  ******************************************************************************
  */

#ifndef __BLUETOOTH_API_H
#define __BLUETOOTH_API_H

#include <arch/gentype.h>
#include "bluetooth.h"

SBStatus_T SbSendCommand(u8* SbCommand, u16 Size);
SBStatus_T SbWaitForCfm(u8 Opcode);
SBStatus_T SbWaitForCfmNoWait(u8 Opcode);
SBStatus_T SbWaitForCfmTimeOut(u8 Opcode);

SBStatus_T SbConfigureLocalDevice();
SBStatus_T SbResetFactorySettings();
SBStatus_T SbSetOperationMode(SBOperationMode_T mode);
SBStatus_T SbResetDevice();
SBStatus_T SbWaitResetResp();
SBStatus_T SbChangeNvsUartSpeed(SbBaudrate btbaud);
SBStatus_T SbSetNvsDefaultAudioSettings(u8 CodecType, u8 AirFormat);
void SbNvsWriteDeviceName(BtNameType Name);
SBStatus_T SbNvsReadDeviceName(BtNameType Name, u8 *MaxNameLength);
void SbNvsWriteDeviceClass(BdDeviceClass Class);
void SbNvsWriteBdAddr(BdAddrType BdAddr);
u8 SbNvsReadFixedPIN(BdFixedPIN Name);
void SbNvsWritePIN(BdFixedPIN PIN);
void SbNvsReadBdAddr(BdAddrType BdAddr);
SBStatus_T SbDeleteSdpRecords();
SBStatus_T SbStoreSdpRecord(u8* entry, u16 length);
SBStatus_T SbSetOperationMode(SBOperationMode_T mode);
SBStatus_T SbEnterTransparentMode(u8 LocalPort);
void SbSendUartBreak();
u32 SbInquiry();
void SbRemoteDeviceName(u8 DeviceNum);
u8 SbGetComm(u8 DevNum);
SBStatus_T SbEstablishLink(u8 DevNum);
SBStatus_T SbReleaseLink(u8 LocalPortNo);
SBStatus_T SbSPPSendData(u8 LocalPortNo, u8* buffer, u16 len);
SBStatus_T SbSPPSendDataNoWait(u8 LocalPortNo, u8* buffer, u16 len);
u32 SbGetPortStatus(u8 portnumber, u8* response);

#endif  //__BLUETOOTH_API_H
