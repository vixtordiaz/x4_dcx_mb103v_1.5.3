/**
  **************** (C) COPYRIGHT 2011 SCT Performance, LLC *********************
  * File Name          : clock.c
  * Device / System    : SCT products using the "CommonCode" platform
  * Author             : Quyen Leba
  *
  * Version            : 1 
  * Date               : 05/02/2011
  * Description        : 
  *                    : 
  *
  *
  * History            : 05/02/2011 M. Davis
  *                    :   Add SCT Copyright header,
  *
  ******************************************************************************
  */

#include <stm32f10x_rcc.h>
#include <stm32f10x_flash.h>
#include <stm32f10x_type.h>
#include <board/clock.h>
#include <common/statuscode.h>

#define USE_INTERNAL_CLOCK          0

static volatile ErrorStatus HSEStartUpStatus = SUCCESS;

//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
void clock_init_old()
{
    /* SYSCLK, HCLK, PCLK2 and PCLK1 configuration -------------------------*/
    /* RCC system reset(for debug purpose) */
    RCC_DeInit();
    
    /* Enable HSE */
    RCC_HSEConfig(RCC_HSE_ON);
    
    /* Wait till HSE is ready */
    HSEStartUpStatus = RCC_WaitForHSEStartUp();
    
    if(HSEStartUpStatus == SUCCESS)
    {
        /* Enable Prefetch Buffer */
        FLASH_PrefetchBufferCmd(FLASH_PrefetchBuffer_Enable);
        
        /* Flash 2 wait state */
        FLASH_SetLatency(FLASH_Latency_2);
        
        /* HCLK = SYSCLK */
        RCC_HCLKConfig(RCC_SYSCLK_Div1); 
        
        /* PCLK2 = HCLK */
        RCC_PCLK2Config(RCC_HCLK_Div1); 
        
        /* PCLK1 = HCLK/2 */
        RCC_PCLK1Config(RCC_HCLK_Div2);
        
        /* ADCCLK = PCLK2/6 */
        RCC_ADCCLKConfig(RCC_PCLK2_Div6);
        
        // PLLCLK = 8MHz * 9 = 72 MHz 
        RCC_PLLConfig(RCC_PLLSource_HSE_Div1, RCC_PLLMul_9);
        
        // Enable PLL  
        RCC_PLLCmd(ENABLE);
        
        // Wait till PLL is ready 
        while(RCC_GetFlagStatus(RCC_FLAG_PLLRDY) == RESET)
        {
        }
        
        // Select PLL as system clock source 
        RCC_SYSCLKConfig(RCC_SYSCLKSource_PLLCLK);
        
        // Wait till PLL is used as system clock source 
        while(RCC_GetSYSCLKSource() != 0x08)
        {
        }    
    }
    
    /* If HSE is not detected at program startup */
    if(HSEStartUpStatus == ERROR)
    {
        // while(1);
        //TODOQ: strncpy(ErrHandlerMsg,"\n\rRCC_Configuration()... error starting HSE clock",ERR_HNDLR_MAX_MSG_LEN);
        //TODOQ: error_handler (INIT_ERROR, 4, TRUE);
        /* Generate NMI exception */
        //NVIC_SetSystemHandlerPendingBit(SystemHandler_NMI);
    }
    
    return;
}

void clock_init()
{
    u8  status;

    //SYSCLK, HCLK, PCLK2 and PCLK1 configuration
    //RCC system reset(for debug purpose)
    RCC_DeInit();
    
    status = S_SUCCESS;
    if (USE_INTERNAL_CLOCK)
    {
        RCC_LSICmd(ENABLE);
        RCC_HSICmd(ENABLE);
    }
    else
    {
        //Enable HSE
        RCC_HSEConfig(RCC_HSE_ON);
        //Wait till HSE is ready
        HSEStartUpStatus = RCC_WaitForHSEStartUp();
        if(HSEStartUpStatus != SUCCESS)
        {
            status = S_FAIL;
        }
    }
    
    if (status == S_SUCCESS)
    {
        //Enable Prefetch Buffer
        FLASH_PrefetchBufferCmd(FLASH_PrefetchBuffer_Enable);
        
        //Flash 2 wait state
        FLASH_SetLatency(FLASH_Latency_2);
        
        //HCLK = SYSCLK
        RCC_HCLKConfig(RCC_SYSCLK_Div1); 
        
        //PCLK2 = HCLK
        RCC_PCLK2Config(RCC_HCLK_Div1); 
        
        //PCLK1 = HCLK/2
        RCC_PCLK1Config(RCC_HCLK_Div2);
        
        //ADCCLK = PCLK2/6
        RCC_ADCCLKConfig(RCC_PCLK2_Div6);
        
        if (USE_INTERNAL_CLOCK)
        {
            // PLLCLK = 8MHz/2 * 16 = 64 MHz
            RCC_PLLConfig(RCC_PLLSource_HSI_Div2, RCC_PLLMul_16);
        }
        else
        {
            //PLLCLK = 8MHz * 9 = 72 MHz 
            RCC_PLLConfig(RCC_PLLSource_HSE_Div1, RCC_PLLMul_9);
        }
        
        // Enable PLL  
        RCC_PLLCmd(ENABLE);
        
        // Wait till PLL is ready 
        while(RCC_GetFlagStatus(RCC_FLAG_PLLRDY) == RESET)
        {
        }
        
        // Select PLL as system clock source 
        RCC_SYSCLKConfig(RCC_SYSCLKSource_PLLCLK);
        
        // Wait till PLL is used as system clock source 
        while(RCC_GetSYSCLKSource() != 0x08)
        {
        }
    }
    
    /* If HSE is not detected at program startup */
    //if(HSEStartUpStatus == ERROR)
    {
        // while(1);
        //TODOQ: strncpy(ErrHandlerMsg,"\n\rRCC_Configuration()... error starting HSE clock",ERR_HNDLR_MAX_MSG_LEN);
        //TODOQ: error_handler (INIT_ERROR, 4, TRUE);
        /* Generate NMI exception */
        //NVIC_SetSystemHandlerPendingBit(SystemHandler_NMI);
    }
    
    return;
}
