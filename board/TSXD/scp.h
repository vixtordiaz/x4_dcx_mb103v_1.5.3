/**
  **************** (C) COPYRIGHT 2011 SCT Performance, LLC *********************
  * File Name          : scp.h
  * Device / System    : SCT products using the "CommonCode" platform
  * Author             : Quyen Leba
  *
  * Version            : 1 
  * Date               : 05/02/2011
  * Description        : 
  *                    : 
  *
  *
  * History            : 05/02/2011 M. Davis
  *                    :   Add SCT Copyright header,
  *
  ******************************************************************************
  */

#ifndef __SCP_H
#define __SCP_H

#include <arch/gentype.h>

#define SCP_MODE_FUNCTIONAL         0
#define SCP_MODE_NODE               1

#define SCP_NOCRC                   0
#define SCP_CRC                     1

//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
// Pulse values of low speed regular scp
//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
#define PWM_1_ACTIVE_LS_24BIT           192
#define PWM_0_ACTIVE_LS_24BIT           480
#define PWM_SOF_ACTIVE_LS_24BIT         960
#define PWM_BRK_ACTIVE_LS_24BIT         1280
#define BIT_TIME_LS_24BIT               704
#define SOF_EOD_TIME_LS_24BIT           1472

#define PWM_1_ACTIVE_MIN_LS_24BIT       120
#define PWM_1_ACTIVE_MAX_LS_24BIT       220

#define PWM_0_ACTIVE_MIN_LS_24BIT       390 
#define PWM_0_ACTIVE_MAX_LS_24BIT       608
#define PWM_SOF_ACTIVE_MIN_LS_24BIT     928
#define PWM_SOF_ACTIVE_MAX_LS_24BIT     1120
#define PWM_BRK_ACTIVE_MIN_LS_24BIT     1216
#define PWM_BRK_ACTIVE_MAX_LS_24BIT     1376

#define BIT_TIME_MIN_LS_24BIT           440
#define BIT_TIME_MAX_LS_24BIT           864
#define SOF_EOD_MIN_LS_24BIT            1444
#define SOF_EOD_MAX_LS_24BIT            2016
#define EOD_TIME_LS_24BIT               1440

//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
// Pulse values of high speed regular scp
//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
#define PWM_1_ACTIVE_HS_24BIT           101
#define PWM_0_ACTIVE_HS_24BIT           224
#define PWM_SOF_ACTIVE_HS_24BIT         464
#define PWM_BRK_ACTIVE_HS_24BIT         608
#define BIT_TIME_HS_24BIT               342
#define SOF_EOD_TIME_HS_24BIT           716

#define PWM_1_ACTIVE_MIN_HS_24BIT       30
#define PWM_1_ACTIVE_MAX_HS_24BIT       110

#define PWM_0_ACTIVE_MIN_HS_24BIT       180
#define PWM_0_ACTIVE_MAX_HS_24BIT       350
#define PWM_SOF_ACTIVE_MIN_HS_24BIT     440
#define PWM_SOF_ACTIVE_MAX_HS_24BIT     580
#define PWM_BRK_ACTIVE_MIN_HS_24BIT     608
#define PWM_BRK_ACTIVE_MAX_HS_24BIT     388

#define BIT_TIME_MIN_HS_24BIT           352
#define BIT_TIME_MAX_HS_24BIT           432
#define SOF_EOD_MIN_HS_24BIT            736
#define SOF_EOD_MAX_HS_24BIT            1008
#define EOD_TIME_HS_24BIT               672

#define PWM_1_ACTIVE_HS_24BIT_2         96
#define PWM_0_ACTIVE_HS_24BIT_2         224
#define PWM_SOF_ACTIVE_HS_24BIT_2       480
#define PWM_BRK_ACTIVE_HS_24BIT_2       608
#define BIT_TIME_HS_24BIT_2             352
#define SOF_EOD_TIME_HS_24BIT_2         736
#define EOD_TIME_HS_24BIT_2             736

//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
// Pulse values of low speed 32bit scp
//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
#define PWM_1_ACTIVE_LS_32BIT           192
#define PWM_0_ACTIVE_LS_32BIT           480
#define PWM_SOF_ACTIVE_LS_32BIT         960
#define PWM_BRK_ACTIVE_LS_32BIT         1280
#define BIT_TIME_LS_32BIT               704
#define SOF_EOD_TIME_LS_32BIT           1472

#define PWM_1_ACTIVE_MIN_LS_32BIT       120
#define PWM_1_ACTIVE_MAX_LS_32BIT       220

#define PWM_0_ACTIVE_MIN_LS_32BIT       390 
#define PWM_0_ACTIVE_MAX_LS_32BIT       608
#define PWM_SOF_ACTIVE_MIN_LS_32BIT     928
#define PWM_SOF_ACTIVE_MAX_LS_32BIT     1120
#define PWM_BRK_ACTIVE_MIN_LS_32BIT     1216
#define PWM_BRK_ACTIVE_MAX_LS_32BIT     1376

#define BIT_TIME_MIN_LS_32BIT           440
#define BIT_TIME_MAX_LS_32BIT           864
#define SOF_EOD_MIN_LS_32BIT            1444
#define SOF_EOD_MAX_LS_32BIT            2016
#define EOD_TIME_LS_32BIT               1440

//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
// Pulse values of high speed 32bit scp
//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
#define PWM_1_ACTIVE_HS_32BIT           101
#define PWM_0_ACTIVE_HS_32BIT           224
#define PWM_SOF_ACTIVE_HS_32BIT         464
#define PWM_BRK_ACTIVE_HS_32BIT         608
#define BIT_TIME_HS_32BIT               342
#define SOF_EOD_TIME_HS_32BIT           716

#define PWM_1_ACTIVE_MIN_HS_32BIT       30
#define PWM_1_ACTIVE_MAX_HS_32BIT       110

#define PWM_0_ACTIVE_MIN_HS_32BIT       180
#define PWM_0_ACTIVE_MAX_HS_32BIT       350
#define PWM_SOF_ACTIVE_MIN_HS_32BIT     440
#define PWM_SOF_ACTIVE_MAX_HS_32BIT     580
#define PWM_BRK_ACTIVE_MIN_HS_32BIT     608
#define PWM_BRK_ACTIVE_MAX_HS_32BIT     388

#define BIT_TIME_MIN_HS_32BIT           352
#define BIT_TIME_MAX_HS_32BIT           432
#define SOF_EOD_MIN_HS_32BIT            736
#define SOF_EOD_MAX_HS_32BIT            1008
#define EOD_TIME_HS_32BIT               672

#define PWM_1_ACTIVE_HS_32BIT_2         96
#define PWM_0_ACTIVE_HS_32BIT_2         224
#define PWM_SOF_ACTIVE_HS_32BIT_2       480
#define PWM_BRK_ACTIVE_HS_32BIT_2       608
#define BIT_TIME_HS_32BIT_2             352
#define SOF_EOD_TIME_HS_32BIT_2         736
#define EOD_TIME_HS_32BIT_2             736

//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
#define SCP_FUNCADDRREQ             0x6A
#define SCP_FUNCADDRRSP             0x6B

#define SCP_FuncAddrReq             SCP_FUNCADDRREQ
#define SCP_FuncAddrRsp             SCP_FUNCADDRRSP
#define SCP_ECM_ID                  0x10
#define SCP_DEVICENODE_RESPONSE_ID  0xF0
#define SCP_KEYONFEPS_DEVICENODE_ID 0x05
#define SCP_MAX_TX_MSG_LENGTH       12  //TODOQ: check these numbers again
#define SCP_MAX_RX_MSG_LENGTH       12

typedef enum
{
    SCP_TYPE_24BIT,
    SCP_TYPE_32BIT,
}SCP_TYPE;

typedef enum
{
    SCP_LOW_SPEED,
    SCP_HIGH_SPEED,
}SCP_SPEED;

//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
void scp_init(SCP_TYPE scptype, SCP_SPEED scpspeed);
void scp_lock_ifr();
void scp_unlock_ifr();
u8 scp_tx(u8 *databuffer, u16 datalength, u8 mode);
u8 scp_rx(u8 *databuffer, u16 *datalength, u8 mode, u8 nodeaddress);

#endif	//__SCP_H
