/**
  **************** (C) COPYRIGHT 2011 SCT Performance, LLC *********************
  * File Name          : timer.c
  * Device / System    : SCT products using the "CommonCode" platform
  * Author             : Quyen Leba
  *
  * Version            : 1 
  * Date               : 05/02/2011
  * Description        : 
  *                    : 
  *
  *
  * History            : 05/02/2011 M. Davis
  *                    :   Add SCT Copyright header,
  *
  ******************************************************************************
  */

#include <stm32f10x_type.h>
#include <stm32f10x_rcc.h>
#include <stm32f10x_tim.h>
#include <board/timer.h>

//------------------------------------------------------------------------------------------------------------------------------------------------
// Init timer1 for VPW & PWM (J1850)
// Engineer: Mark Davis
// Date: Jan 13, 2009
//------------------------------------------------------------------------------------------------------------------------------------------------
void timer1_init()
{
    TIM_TimeBaseInitTypeDef  TIM_TimeBaseStructure;

    //Enable TIMER 1
    RCC_APB2PeriphClockCmd(RCC_APB2Periph_TIM1 , ENABLE);
    
    //Generates TIM1 clock input, for J1850... Ford scp (PWM) - GM vpw 
    //TIM1CLK = 48 MHz, Prescaler = 0, TIM1 counter clock = 72 MHz
    
    //Set up the clock source for 48MHz
    TIM_TimeBaseStructure.TIM_Prescaler = 0;
    TIM_TimeBaseStructure.TIM_CounterMode = TIM_CounterMode_Up;
    TIM_TimeBaseStructure.TIM_Period = 0xFFFF;
    TIM_TimeBaseStructure.TIM_ClockDivision = 0;
    TIM_TimeBaseStructure.TIM_RepetitionCounter = 0;
    TIM_TimeBaseInit(TIM1, &TIM_TimeBaseStructure);
    
    //TIM1 counter enable
    TIM_Cmd(TIM1, ENABLE);
}

//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
void timer1_deinit()
{
    TIM_DeInit(TIM1);
    // TIM1 clock disable
//    RCC_APB1PeriphClockCmd(RCC_APB1Periph_TIM1, DISABLE);
    // TIM1 disable counter
    TIM_Cmd(TIM1, DISABLE);
}

//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
u16 timer1_getcounter()
{
    return TIM_GetCounter(TIM1);
}

//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
void timer1_resetcounter()
{
    TIM_SetCounter(TIM1,0);
}


//------------------------------------------------------------------------------
// This timer is used by VPW
//------------------------------------------------------------------------------
void timer2_init()
{
    TIM_TimeBaseInitTypeDef  TIM_TimeBaseStructure;
    
    //TIM2 clock enable
    RCC_APB1PeriphClockCmd(RCC_APB1Periph_TIM2, ENABLE);
    
    TIM_DeInit(TIM2);
    //Time base configuration
    TIM_TimeBaseStructure.TIM_Period = 0x7FFF;
    TIM_TimeBaseStructure.TIM_Prescaler = 1;
    TIM_TimeBaseStructure.TIM_ClockDivision = 0;
    TIM_TimeBaseStructure.TIM_CounterMode = TIM_CounterMode_Up;
    TIM_TimeBaseInit(TIM2, &TIM_TimeBaseStructure);
    
    //TIM2 enable counter
    TIM_Cmd(TIM2, ENABLE);
    
    //TIM IT enable
    TIM_ITConfig(TIM2, TIM_IT_Update, ENABLE);
}

//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
void timer2_deinit()
{
    TIM_DeInit(TIM2);
    // TIM2 clock disable
    RCC_APB1PeriphClockCmd(RCC_APB1Periph_TIM2, DISABLE);
    // TIM2 disable counter
    TIM_Cmd(TIM2, DISABLE);
}

//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
u16 timer2_getcounter()
{
    return TIM_GetCounter(TIM2);
}

//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
void timer2_resetcounter()
{
    TIM_SetCounter(TIM2,0);
}
