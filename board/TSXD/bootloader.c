/**
  **************** (C) COPYRIGHT 2011 SCT Performance, LLC *********************
  * File Name          : bootloader.c
  * Device / System    : SCT products using the "CommonCode" platform
  * Author             : Quyen Leba
  *
  * Version            : 1 
  * Date               : 05/02/2011
  * Description        : 
  *                    : 
  *
  *
  * History            : 05/02/2011 M. Davis
  *                    :   Add SCT Copyright header,
  *
  ******************************************************************************
  */

#include <arch/gentype.h>
#include <board/genplatform.h>
#include <board/bootloader.h>

typedef void (*pFunction)(void);
#define BOOTLOADER_REGULAR_ADDRESS      0x00000000

//------------------------------------------------------------------------------
// Jump to bootloader
// Input:   u8  bootloadertype
// Return   u8  status
// Engineer: Patrick Downs, Quyen Leba
//------------------------------------------------------------------------------
void bootloader_jump_to_main_bootloader()
{
    u32 JumpAddress;
    pFunction Jump_To_Bootloader;
    
    JumpAddress = *(vu32*)(BOOTLOADER_REGULAR_ADDRESS + 4);
    Jump_To_Bootloader = (pFunction)JumpAddress;
    // Initialize user application's Stack Pointer 
    __MSR_MSP(*(vu32*) BOOTLOADER_REGULAR_ADDRESS);
    Jump_To_Bootloader();
}
