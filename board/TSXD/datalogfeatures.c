/**
  **************** (C) COPYRIGHT 2011 SCT Performance, LLC *********************
  * File Name          : datalogfeatures.c
  * Device / System    : SCT products using the "CommonCode" platform
  * Author             : Quyen Leba
  *
  * Version            : 1 
  * Date               : 05/02/2011
  * Description        : 
  *                    : 
  *
  *
  * History            : 05/02/2011 M. Davis
  *                    :   Add SCT Copyright header,
  *
  ******************************************************************************
  */

#include "datalogfeatures.h"

const DlxHeader DefaultDatalogFeatureHeader =
{
    .dlxVersion     = DlxVersion,
    .ecm            = 0,                        // ecm type
    .description    = {"Datalog Features"},     // Optional file description
    .strategy       = {0},  // Strategy Partnumber, etc., if DLF is for a specific vehicle
    .oemType        = 0,    // vehicle mfr
    .commType       = 0,    // Communications type (CAN, SCP, VPW, etc)
    .commLevel      = 0,    // Communications level (KWP2000, ISO14229, and future)
    .Reserverd1     = 0,
    .count          = ANALOG_INPUT_COUNT,
    .Reserved       = {0},   // padding... header is 128 bytes
};

//NOTE: ANALOG_INPUT_COUNT needs to match the constant array
#if (ANALOG_INPUT_COUNT != 4)
#error ANALOG_INPUT_COUNT is incorrect for DefaultDatalogFeatureBlocks[]
#endif

const DlxBlock DefaultDatalogFeatureBlocks[ANALOG_INPUT_COUNT] =
{ 
    {
        .pidModuleType      = 0, 
        .pidShortName       = AIN0_SHORTNAME,
        .pidName            = AIN0_NAME,
        .pidAddr            = 0,
        .pidSize            = 0,
        .pidBP              = 0,
        .pidBit             = 0,
        .pidType            = 3,
        .pidBitMapped       = 0,
        .pidReserved1       = {0},
        .pidMultiplier      = 0,
        .pidOffset          = 0,
        .pidRangeMin        = AIN0_MIN,
        .pidRangeMax        = AIN0_MAX,
        .pidUnitID          = 0,
        .pidReserved2       = {0},    
    },
    {
        .pidModuleType      = 0, 
        .pidShortName       = AIN1_SHORTNAME,
        .pidName            = AIN1_NAME,
        .pidAddr            = 1,
        .pidSize            = 0,
        .pidBP              = 0,
        .pidBit             = 0,
        .pidType            = 3,
        .pidBitMapped       = 0,
        .pidReserved1       = {0},
        .pidMultiplier      = 0,
        .pidOffset          = 0,
        .pidRangeMin        = AIN1_MIN,
        .pidRangeMax        = AIN1_MAX,
        .pidUnitID          = 0,
        .pidReserved2       = {0},    
    },
    {
        .pidModuleType      = 0, 
        .pidShortName       = AIN2_SHORTNAME,
        .pidName            = AIN2_NAME,
        .pidAddr            = 2,
        .pidSize            = 0,
        .pidBP              = 0,
        .pidBit             = 0,
        .pidType            = 3,
        .pidBitMapped       = 0,
        .pidReserved1       = {0},
        .pidMultiplier      = 0,
        .pidOffset          = 0,
        .pidRangeMin        = AIN2_MIN,
        .pidRangeMax        = AIN2_MAX,
        .pidUnitID          = 0,
        .pidReserved2       = {0},    
    },    
    {
        .pidModuleType      = 0, 
        .pidShortName       = AIN3_SHORTNAME,
        .pidName            = AIN3_NAME,
        .pidAddr            = 3,
        .pidSize            = 0,
        .pidBP              = 0,
        .pidBit             = 0,
        .pidType            = 3,
        .pidBitMapped       = 0,
        .pidReserved1       = {0},
        .pidMultiplier      = 0,
        .pidOffset          = 0,
        .pidRangeMin        = AIN3_MIN,
        .pidRangeMax        = AIN3_MAX,
        .pidUnitID          = 0,
        .pidReserved2       = {0},    
    }
};
