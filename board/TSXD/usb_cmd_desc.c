/**
  **************** (C) COPYRIGHT 2011 SCT Performance, LLC *********************
  * File Name          : usb_cmd_desc.c
  * Device / System    : SCT products using the "CommonCode" platform
  * Author             : Quyen Leba
  *
  * Version            : 1 
  * Date               : 05/02/2011
  * Description        : 
  *                    : 
  *
  *
  * History            : 05/02/2011 M. Davis
  *                    :   Add SCT Copyright header,
  *
  ******************************************************************************
  */

/* Includes ------------------------------------------------------------------*/
#include "usb_cmd_desc.h"

const u8 UCMD_DeviceDescriptor[UCMD_SIZ_DEVICE_DESC] =
{
    0x12,   /* bLength  */
    0x01,   /* bDescriptorType */
    0x00,   /* bcdUSB, version 2.00 */
    0x02,
    0x00,   /* bDeviceClass : each interface define the device class */
    0x00,   /* bDeviceSubClass */
    0x00,   /* bDeviceProtocol */
    USB_MAX_PACKET_SIZE,   /* bMaxPacketSize0 0x40 = 64 */
    0x59,   /* idVendor  (0x5059) */
    0x50,
    0x45,   /* idProduct (0x4045) */
    0x40,
    0x00,   /* bcdDevice 2.00*/
    0x02,
    1,              /* index of string Manufacturer  */
    /**/
    2,              /* index of string descriptor of product*/
    /* */
    3,              /* */
    /* */
    /* */
    0x01    /*bNumConfigurations */
};
const u8 UCMD_ConfigDescriptor[UCMD_SIZ_CONFIG_DESC] =
{
    
    0x09,   /* bLength: Configuation Descriptor size */
    0x02,   /* bDescriptorType: Configuration */
    UCMD_SIZ_CONFIG_DESC,
    
    0x00,
    0x01,   /* bNumInterfaces: 1 interface */
    0x01,   /* bConfigurationValue: */
    /*      Configuration value */
    0x00,   /* iConfiguration: */
    /*      Index of string descriptor */
    /*      describing the configuration */
    0x80,   /* bmAttributes: */
    /*      bus powered */
    0xFA,   /* MaxPower 500 mA */
    
    /******************** Descriptor of UCMD interface ********************/
    /* 09 */
    0x09,   /* bLength: Interface Descriptor size */
    0x04,   /* bDescriptorType: */
    /*      Interface descriptor type */
    0x00,   /* bInterfaceNumber: Number of Interface */
    0x00,   /* bAlternateSetting: Alternate setting */
    0x02,   /* bNumEndpoints*/
    0xFF,   /* bInterfaceClass: */
    0xFF,   /* bInterfaceSubClass : */
    0xFF,   /* nInterfaceProtocol */
    4,          /* iInterface: */
    /* 18 */
    0x07,   /*Endpoint descriptor length = 7*/
    0x05,   /*Endpoint descriptor type */
    0x01,   /*Endpoint address (IN, address 1) */
    0x02,   /*Bulk endpoint type */
    0x40,   /*Maximum packet size (64 bytes) */
    0x00,
    0x00,   /*Polling interval in milliseconds */
    /* 25 */
    0x07,   /*Endpoint descriptor length = 7 */
    0x05,   /*Endpoint descriptor type */
    0x82,   /*Endpoint address (OUT, address 2) */
    0x02,   /*Bulk endpoint type */
    0x40,   /*Maximum packet size (64 bytes) */
    0x00,
    0x00     /*Polling interval in milliseconds*/
        /*32*/
};
const u8 UCMD_StringLangID[UCMD_SIZ_STRING_LANGID] =
{
    UCMD_SIZ_STRING_LANGID,
    0x03,
    0x09,
    0x04
}
;      /* LangID = 0x0409: U.S. English */
const u8 UCMD_StringVendor[UCMD_SIZ_STRING_VENDOR] =
{
    UCMD_SIZ_STRING_VENDOR, /* Size of manufaturer string */
    0x03,           /* bDescriptorType = String descriptor */
    /* Manufacturer: "SCT LLC" */
    'S', 0, 'C', 0, 'T', 0, ' ', 0, 'L', 0, 'L', 0, 'C', 0
};
const u8 UCMD_StringProduct[UCMD_SIZ_STRING_PRODUCT] =
{
    UCMD_SIZ_STRING_PRODUCT,
    0x03,
    /* Product name: "SCT TSX Dongle" */
    'S', 0, 'C', 0, 'T', 0, ' ', 0, 'T', 0, 'S', 0, 'X', 0, ' ', 0, 'D', 0,
    'o', 0, 'n', 0, 'g', 0, 'l', 0, 'e', 0
        
};

u8 UCMD_StringSerial[UCMD_SIZ_STRING_SERIAL] =
{
    UCMD_SIZ_STRING_SERIAL,
    0x03,
    /* Serial number*/
    'T', 0, 'S', 0, 'X', 0, 'D', 0, '0', 0, '0', 0, '1', 0
};
const u8 UCMD_StringInterface[UCMD_SIZ_STRING_INTERFACE] =
{
    UCMD_SIZ_STRING_INTERFACE,
    0x03,
    /* Interface 0: "TSXD CMD" */
    'T', 0, 'S', 0, 'X', 0, 'D', 0, ' ', 0, 'C', 0, 'M', 0, 'D', 0
};
