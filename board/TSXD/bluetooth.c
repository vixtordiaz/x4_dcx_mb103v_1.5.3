/**
  **************** (C) COPYRIGHT 2011 SCT Performance, LLC *********************
  * File Name          : bluetooth.c
  * Device / System    : SCT products using the "CommonCode" platform
  * Author             : Quyen Leba
  *
  * Version            : 1 
  * Date               : 05/02/2011
  * Description        : 
  *                    : 
  *
  *
  * History            : 05/02/2011 M. Davis
  *                    :   Add SCT Copyright header,
  *
  ******************************************************************************
  */

#include <stdio.h>
#include <string.h>
#include <stm32f10x_gpio.h>
#include <stm32f10x_rcc.h>
#include <common/statuscode.h>
#include <common/crypto_blowfish.h>
#include <common/crypto_messageblock.h>
#include <common/cmdif_func_filexfer.h>
#include <board/debug.h>
#include <fs/genfs.h>
#include "usart.h"
#include "bluetooth.h"
#include "bluetooth_api.h"
#include "led.h"

// redirect which usart port to run bluetooth module
#define bluetooth_hal(func)         usart2_##func

#define RAWSPP_SEND_DATA_SIZE       256

extern filexfer_info filexferinfo;                  //from cmdif_func_filexfer.c
extern rawspp_xfer_info rawsppxferinfo;             //from cmdif_func_filexfer.c

//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
SbDeviceInfoType    SbDevInfo;          //local device context information
SBEvent_T           SbEvent;            //Current result of Simply Blue response
BtDeviceInfo        DeviceInfoList[BLUETOOTH_MAX_DEVICE_COUNT];
SBLinkDataStatus    SbLinkDataStatus;   //SPP_Link_Status
u32 DevPtr = 0;

//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
USARTBAUD bluetooth_translatebaud(SbBaudrate btbaud);
void bluetooth_uart_tx(u8 *data, u16 length);
void SbProcessFrame();

//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
SBStatus_T SbApiInit()
{
    /* Create a mailbox for inter-task communication */
	SbDevInfo.SbCmdStatus = SBSTATUS_OK;
    
    SbDevInfo.PortsToOpen     = 0;
    SbDevInfo.SbPacketState   = SBPACKET_START;

    SbEvent.wPayloadOffset = 0;

    rawsppxferinfo.xfer_status = RAWSPP_WAITING;
    SbLinkDataStatus = RELEASED;

    return SBSTATUS_OK;
}

//------------------------------------------------------------------------------
// Initialize Bluetooth
// Input:   SbBaudrate btbaud (a preferred baudrate)
// Return:  u8  status
// Engineer: Quyen Leba
//------------------------------------------------------------------------------
u8 bluetooth_init(SbBaudrate btbaud)
{
    SbBaudrate detected_btbaud;
    SBStatus_T btstatus;
    
    SbApiInit();
    
    //detect the current bluetooth module baudrate
    btstatus = bluetooth_autodetectmodulebaud(btbaud,&detected_btbaud);
    if (btstatus == SBSTATUS_OK)
    {
        if (detected_btbaud != btbaud)
        {
            //config the module to the preferred baudrate
            btstatus = SbChangeNvsUartSpeed(btbaud);
            if (btstatus == SBSTATUS_OK)
            {
                //double check
                btstatus = bluetooth_autodetectmodulebaud(btbaud,
                                                          &detected_btbaud);
                if (detected_btbaud != btbaud)
                {
                    btstatus = SBSTATUS_ERROR;
                }
            }
        }
    }
    
    if (btstatus != SBSTATUS_OK)
    {
        return S_FAIL;
    }
    return S_SUCCESS;
}

//------------------------------------------------------------------------------
// Auto detect bluetooth module baudrate
// Input:   SbBaudrate  preferred_btbaud
// Output:  SbBaudrate  *detected_btbaud
// Return:  SBStatus_T  btstatus
// Engineer: Quyen Leba
//------------------------------------------------------------------------------
SBStatus_T bluetooth_autodetectmodulebaud(SbBaudrate preferred_btbaud,
                                          SbBaudrate *detected_btbaud)
{
    SbBaudrate baudlist[3] = {BT_115200_BAUD,BT_9600_BAUD,BT_230400_BAUD};
    BtNameType Name;
    u8  MaxNameLength;
    u32 i;
    SBStatus_T btstatus;
    
    bluetooth_hal(init)(bluetooth_translatebaud(preferred_btbaud));
    btstatus = SbNvsReadDeviceName(Name,&MaxNameLength);
    if (btstatus == SBSTATUS_OK)
    {
        *detected_btbaud = preferred_btbaud;
        return SBSTATUS_OK;
    }
    
    for(i=0;i<3;i++)
    {
        if (baudlist[i] != preferred_btbaud)
        {
            bluetooth_changebaudrate(baudlist[i]);
            btstatus = SbNvsReadDeviceName(Name,&MaxNameLength);
            if (btstatus == SBSTATUS_OK)
            {
                *detected_btbaud = baudlist[i];
                break;
            }
        }
    }
    return btstatus;
}

//------------------------------------------------------------------------------
// Translate to actual baud rate
// Return:  u32 baudrate
//------------------------------------------------------------------------------
USARTBAUD bluetooth_translatebaud(SbBaudrate btbaud)
{
    if ((u32)btbaud > (u32)sizeof(SbTranslatedBaudrate))
    {
        return (USARTBAUD)0;
    }
    else
    {
        return (USARTBAUD)SbTranslatedBaudrate[(u32)btbaud];
    }
}

//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
void bluetooth_changebaudrate(SbBaudrate btbaud)
{
    bluetooth_hal(changebaudrate)(bluetooth_translatebaud(btbaud));
}

//------------------------------------------------------------------------------
// Input:   u8 *data
//          u16 length
//------------------------------------------------------------------------------
void bluetooth_uart_tx(u8 *data, u16 length)
{
    u16 i = 0;
    while(i < length)
    {
        bluetooth_hal(tx)(data[i++]);
    }
}

//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
void bluetooth_uart_rx_irq_handler()
{
    u8  in;
    u16 DataLen;
    
    if(bluetooth_hal(getitstatus)() != RESET)
    {
        switch(SbDevInfo.SbPacketState) 
        {
        case SBPACKET_START:
            in = bluetooth_hal(rx)();
            if (in == STX) 
            {
                SbDevInfo.SbPacketState = SBPACKET_TYPE;
            }
            else 
            {
                SbDevInfo.SbPacketState = SBPACKET_ERR;
            }
            break;
        case SBPACKET_TYPE:
            in = bluetooth_hal(rx)();
            if (in == CFM || in == IND || in == RES) 
            {
                SbDevInfo.SbPacketState = SBPACKET_OPCODE;
                SbEvent.bType = in;
            }
            else 
            {
                SbDevInfo.SbPacketState = SBPACKET_ERR;
            }
            break;
        case SBPACKET_OPCODE:
            in = bluetooth_hal(rx)();
            SbEvent.bOpcode = in;
            SbDevInfo.SbPacketState = SBPACKET_DATALENGTH0;
            break;
        case SBPACKET_DATALENGTH0:
            SbEvent.wPayloadlen = bluetooth_hal(rx)();
            SbDevInfo.SbPacketState = SBPACKET_DATALENGTH1;
            break;
        case SBPACKET_DATALENGTH1:
            DataLen = bluetooth_hal(rx)();
            SbEvent.wPayloadlen |= DataLen << 8; 
            SbDevInfo.SbPacketState = SBPACKET_CHECKSUM;
            break;
        case SBPACKET_CHECKSUM:
            in = bluetooth_hal(rx)();
            SbEvent.bChecksum = SbEvent.bType + SbEvent.bOpcode + 
                (SbEvent.wPayloadlen >> 8) + (SbEvent.wPayloadlen & 0x00FF);
            if (SbEvent.bChecksum == in) 
            {
                SbDevInfo.SbPacketState = SBPACKET_DATA;
            }
            else 
            {
                SbDevInfo.SbPacketState = SBPACKET_ERR;
            }
            break;
        case SBPACKET_DATA:
            SbEvent.pPayload[SbEvent.wPayloadOffset] = bluetooth_hal(rx)();
            if(SbEvent.wPayloadOffset < (SbEvent.wPayloadlen-1))
            {   
                SbEvent.wPayloadOffset++;
            }
            else
            {
                SbDevInfo.SbPacketState = SBPACKET_END;
                SbEvent.wPayloadOffset = 0;
            }
            
            if(SbEvent.wPayloadOffset > 330)
                SbDevInfo.SbPacketState = SBPACKET_ERR;
            
            break;
        case SBPACKET_END:  
            SbEvent.bEnd = bluetooth_hal(rx)();
            if (SbEvent.bEnd == ETX)
            {
                SbProcessFrame();   //process the frame once received
                SbDevInfo.SbPacketState = SBPACKET_START;
            }
            else 
            {
                SbDevInfo.SbPacketState = SBPACKET_ERR;
            }
            break;
        default:
            // invalid Packet received from SB usart,skipping of bytes
            // to next valid packet is not incorporated. simply blue assumes
            // error free usart transport. No further processing.
            // apps should call SbGetPacketState() and do proper exit or
            // analyze on error state.
            SbDevInfo.SbPacketState = SBPACKET_ERR;
            break;
        }
        if(SbDevInfo.SbPacketState == SBPACKET_ERR)
        {
            u8 outputbuf[5];

            SbDevInfo.SbPacketState = SBPACKET_START;
            sprintf((char*)outputbuf, "%X", in);
            debug("PE!");
            debug(outputbuf);
            return;
        }
    }
}

//------------------------------------------------------------------------------
// This function is being called from SbTimerTask to process the received complete
//    simply blue packet.This routine unblocks the pending events to denote a simply command
//    is confirmed and completed.
//------------------------------------------------------------------------------
void SbProcessFrame()
{    
    if (SbEvent.bType == CFM) 
    {
        //signal that sb command is completed
        SbDevInfo.SbCmdStatus = SBSTATUS_OK;
    }

    switch (SbEvent.bOpcode) 
    {
    case GAP_DEVICE_FOUND:
        memcpy(DeviceInfoList[DevPtr].DeviceAddr, SbEvent.pPayload, 6);
        memcpy(DeviceInfoList[DevPtr].DeviceClass, SbEvent.pPayload+6, 3);
        DevPtr++;
        return;// SBSTATUS_INPROCESS; 
    case SPP_LINK_ESTABLISHED:
        SbLinkDataStatus = ESTABLISHED;
        led_setState(GREEN);
        debug("\n\rSPP Link Established");
        return;
    case SPP_INCOMING_LINK_ESTABLISHED:
        SbLinkDataStatus = ESTABLISHED;
        led_setState(GREEN);
        debug("\n\rSPP Incoming Link Established");
        return;
    case SPP_LINK_RELEASED:             
        SbLinkDataStatus = RELEASED;
        led_setState(OFF);
        switch(SbEvent.pPayload[0])
        {
        case RFCR_DLC_DISC_LOCAL_DEVICE:
            debug("\n\rThe local device has disconnected the DLC");
            break;
        case RFCR_DLC_DISC_REMOTE_DEVICE:
            debug("\n\rThe remote device has disconnected the DLC");
            break;
        case RFCR_DLC_DISC_ACL_FAILURE:
            debug("\n\rACL link failure/ link supervision timeout");
            break;
        case RFCR_DLC_DISC_LOWER_LAYER:
            debug("\n\rLower layer (e.g. L2CAP) has disconnected the DLC");
            break;
        default:
            break;
        }        
        return;
    case GAP_ESTABLISH_SCO_LINK:        /* to be implemented by the user */ 
        return;
    case GAP_RELEASE_SCO_LINK:          /* to be implemented by the user */ 
        return;
    case SPP_SEND_DATA:                 /* to be implemented by the user */ 
        return;
    case SPP_INCOMING_DATA:
        // ---------------------------------------------------------------------
        // TSX command receive, caluclates the checksum for Tsx Command then 
        //  indicates data ready
        rawsppxferinfo.len = SbEvent.wPayloadlen-3;   // minus 3 bytes of SPP Data command
        if(rawsppxferinfo.len > 330)
        {
            debug("\n\rErr Length too long!");
            return;
        }
        if(rawsppxferinfo.filexfermode == TRUE)
        {
            memcpy(&filexferinfo.databuffer[rawsppxferinfo.databytesent],
                   SbEvent.pPayload+3, SbEvent.wPayloadlen-3);
            rawsppxferinfo.databytesent += (SbEvent.wPayloadlen-3);
            if(rawsppxferinfo.databytesent == filexferinfo.blocklength)
            {
                rawsppxferinfo.xfer_status = RAWSPP_READY;
            }
        }
        else
        {
            memcpy(rawsppxferinfo.buffer,
                   SbEvent.pPayload+3, SbEvent.wPayloadlen-3);
            rawsppxferinfo.xfer_status = RAWSPP_READY;
        }
        return;      
    case SDAP_SERVICE_BROWSE:           /* to be implemented by the user */
        return;
    case SDAP_SERVICE_REQUEST:          /* to be implemented by the user */ 
        return;
    case MODULE_READY:
        /* At this point, the Simply Blue device is reset and ready to work */
        return;
    default:
        break;
	}
}

//------------------------------------------------------------------------------
// Output:  u8  *data
//          u16 *len (length of data)
// Return:  u8  status
//------------------------------------------------------------------------------
u8 bluetooth_receive_command(u8 *data, u16 *len)
{
    u32 checksum;
    u32 checksumCalc;
    u32 i;
    s16 TotalByteCount=0;
    s16 RxByteCount=0;
    u8  databuffer[330];
    u8  tmpbuffer[330];
    u32 dataindex;
    u8  decrypted_databuffer[128];
    u32 decrypted_datalength;
    u8  status;
    
    if (rawsppxferinfo.xfer_status == RAWSPP_READY)
    {
        //TODO:
        //NOTE: this assume at least 8 bytes came in
        memcpy((void*)tmpbuffer,(void*)rawsppxferinfo.buffer,8);
        crypto_blowfish_decryptblock((void*)tmpbuffer,8);
        TotalByteCount = tmpbuffer[2];
        TotalByteCount |= tmpbuffer[3]<<8;
        
        if(TotalByteCount > sizeof(databuffer))
        {
            rawsppxferinfo.xfer_status = RAWSPP_WAITING;
            debug("\n\rTotal Byte Count too large!");
            return S_FAIL;
        }
        
        memcpy((void*)data,(void*)tmpbuffer,4);
        dataindex = 4;
        
        checksum = 0;
        checksum |= (u32)tmpbuffer[4];
        checksum |= (u32)(tmpbuffer[5]<<8);
        checksum |= (u32)(tmpbuffer[6]<<16);
        checksum |= (u32)(tmpbuffer[7]<<24);
        
        checksumCalc = tmpbuffer[0] + tmpbuffer[1] + 
            tmpbuffer[2] + tmpbuffer[3];
        
        while(1)
        {
            rawsppxferinfo.xfer_status = RAWSPP_WAITING;
            memcpy(databuffer+RxByteCount,
                   rawsppxferinfo.buffer, rawsppxferinfo.len);
            RxByteCount += rawsppxferinfo.len;
            if((TotalByteCount - RxByteCount)<1)
            { 
                break;
            }
            else
            {
                debug("\n\rTsx_SPP_Recieve_Command waiting while loop");
                //RTC_Reset();
                while(rawsppxferinfo.xfer_status == RAWSPP_WAITING)
                {
                    /*
                    if(RTC_GetValue()>(timeout/RTC_TICK_VALUE))
                    {
                        usartDebug_Printf("\n\rTsx_SPP_Recieve_Command Timeout");
                        break;          
                    }
                    */
                }
            }
        }

        for(i=8;i<RxByteCount;i++)
        {
            checksumCalc += databuffer[i];
        }
        
        if(checksum == checksumCalc)
        {
            if (RxByteCount > 8)
            {
                status = 
                    crypto_messageblock_decrypt((u8*)&databuffer[8],RxByteCount-8,
                                                decrypted_databuffer,
                                                &decrypted_datalength);
                //not exact but can't be more than this
                if (status != S_SUCCESS)
                {
                    return status;
                }
                else if (decrypted_datalength > (RxByteCount-8))
                {
                    return S_FAIL;
                }
                memcpy((void*)&data[dataindex], decrypted_databuffer,
                       decrypted_datalength);
                dataindex += decrypted_datalength;
                data[dataindex] = 0; // Null terminate
            }
            return S_SUCCESS;
        }
        else
        {
            debug("\n\rChecksum Bad!");
            return S_FAIL;
        }
    }
    else
    {
        return S_FAIL;
    }
}

//-----------------------------------------------------------------------------
//
//-----------------------------------------------------------------------------
u32 bluetooth_send_command(CMDIF_COMMAND cmd, u8 *data, u16 len)
{
    u8 buffer[330];
    u32 checksum;
    u32 i;
    u32 status;
    
    if (len > (330 - (4+4)))
    {
        return S_FAIL;
    }
    
    memcpy(buffer+8, data, len);
    len += 4 + 4;                 // Add bytes for length(CMD)+(CHECKSUM)+(Data)
    buffer[0] = ERROR_OK;
    buffer[1] = cmd;
    buffer[2] = len;
    buffer[3] = (len >> 8);
    checksum = buffer[0] + buffer[1] + buffer[2] + buffer[3];

    for(i=8;i<len;i++)
    {
        checksum += buffer[i];
    }
    buffer[4] = checksum;
    buffer[5] = checksum>>8;
    buffer[6] = checksum>>16;
    buffer[7] = checksum>>24;

    status = SbSPPSendData(0x01, (void*)buffer, len);
    
    return status;
}

//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
u8 bluetooth_send_ack(CMDIF_COMMAND cmd, CMDIF_ACK resp, u8 *data, u16 len)
{
    u8  buffer[330];
    u32 checksum;
    u32 i;
    u32 status;
    
    memcpy(buffer+10, data, len);   // Copy in data
    len += 6 + 4;                   // Add bytes for length(CMD)+(CHECKSUM)+(Data)
    buffer[0] = ERROR_OK;           // Status
    buffer[1] = CMDIF_CMD_ACK;      // Command
    buffer[2] = len;                // Length 0
    buffer[3] = len>>8;             // Length 1    
    buffer[8] = resp;               // Resp code
    buffer[9] = cmd;                // Resp cmd        

    checksum = buffer[0] + buffer[1] + buffer[2] + buffer[3] + 
        buffer[8] + buffer[9];
    for(i=10;i<len;i++)             // Calculate checksum
    {
        checksum += buffer[i];
    }
    buffer[4] = checksum;           // Save checksum in command
    buffer[5] = checksum>>8;
    buffer[6] = checksum>>16;
    buffer[7] = checksum>>24;

    status = SbSPPSendData(0x01, (void*)buffer, len);
    return status;
}

//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
u8 bluetooth_send_ack_nowait(CMDIF_COMMAND cmd, CMDIF_ACK resp, u8 *data, u16 len)
{
    u8  buffer[330];
    u32 checksum;
    u32 i;
    u32 status;
    
    memcpy(buffer+10, data, len);   // Copy in data
    len += 6 + 4;                   // Add bytes for length(CMD)+(CHECKSUM)+(Data)
    buffer[0] = ERROR_OK;           // Status
    buffer[1] = CMDIF_CMD_ACK;      // Command
    buffer[2] = len;                // Length 0
    buffer[3] = len>>8;             // Length 1    
    buffer[8] = resp;               // Resp code
    buffer[9] = cmd;                // Resp cmd        

    checksum = buffer[0] + buffer[1] + buffer[2] + buffer[3] + 
        buffer[8] + buffer[9];
    for(i=10;i<len;i++)             // Calculate checksum
    {
        checksum += buffer[i];
    }
    buffer[4] = checksum;           // Save checksum in command
    buffer[5] = checksum>>8;
    buffer[6] = checksum>>16;
    buffer[7] = checksum>>24;

    status = SbSPPSendDataNoWait(0x01, (void*)buffer, len);
    return status;
}

//-----------------------------------------------------------------------------
//
//-----------------------------------------------------------------------------
u8 bluetooth_send_rawspp(u8 *txbuffer, u16 length)
{
    u16 bufferindex;
    SBStatus_T status;
    u16 txlen;
    
    bufferindex = 0;
    
    while(bufferindex < length)
    {
        txlen = length - bufferindex;
        if (txlen > RAWSPP_SEND_DATA_SIZE)
        {
            status = SbSPPSendData(0x01,
                                   txbuffer+bufferindex, RAWSPP_SEND_DATA_SIZE);
        }
        else
        {
            status = SbSPPSendData(0x01,
                                   txbuffer+bufferindex, txlen);
        }
        
        if (status != SBSTATUS_OK)
        {
            debug("\nError Tx Block!");
        }

        bufferindex += RAWSPP_SEND_DATA_SIZE;
    }
    
    return S_SUCCESS;
}

//------------------------------------------------------------------------------
// Checks the local bluetooth port for a connection status
// Input:   SbPortNumber btport
// Return:  u8 status
//------------------------------------------------------------------------------
u8 bluetooth_checkconnection(SbPortNumber btport)
{
    u8 response_data[3];
    
    if(SbGetPortStatus(btport, response_data) == SBSTATUS_OK)
    {
        if(response_data[0] &= 0x80)
        {
            return S_SUCCESS;   // DLC Availble
        }
    }
    return S_FAIL;  // No DLC availble or error
}
