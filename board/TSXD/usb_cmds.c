/**
  **************** (C) COPYRIGHT 2011 SCT Performance, LLC *********************
  * File Name          : usb_cmds.c
  * Device / System    : SCT products using the "CommonCode" platform
  * Author             : Quyen Leba
  *
  * Version            : 1 
  * Date               : 05/02/2011
  * Description        : 
  *                    : 
  *
  *
  * History            : 05/02/2011 M. Davis
  *                    :   Add SCT Copyright header,
  *
  ******************************************************************************
  */

#include <board/bootloader.h>
#include <board/delays.h>
#include <board/genplatform.h>
#include <common/statuscode.h>

bool        USB_Mass_Storage_Enable = FALSE;
bool        BootloaderJumpRequest = FALSE;

/* Private functions ---------------------------------------------------------*/

//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
void USB_CMDS_JumpToBootloaderRequest(void)
{
    BootloaderJumpRequest = TRUE;
}

u8 USB_CMDS_JumpToBootloaderNotification(void)
{
    return BootloaderJumpRequest;
}

void USB_CMDS_JumpToBootloader(void)
{
    delays(500,'m');
    bootloader_jump_to_main_bootloader();
}

/*******************************************************************************
* Function Name  : USB_IsMassStorageEnable
* Description    : Returns the current state of the USB_Mass_Storage_Enable flag
*                  (static local to usb_cmds.c)
* Input          : None
* Output         : None
* Return         : USB_Mass_Storage_Enable set   - TRUE
*                  USB_Mass_Storage_Enable clear - FALSE
*******************************************************************************/
bool USB_IsMassStorageEnable(void)
{
    return USB_Mass_Storage_Enable;
}


/*******************************************************************************
* Function Name  : USB_DisableMassStorage
* Description    : Clears the USB_Mass_Storage_Enable flag 
*                  (static local to usb_cmds.c)
* Input          : None
* Output         : None
* Return         : None
*******************************************************************************/
void USB_DisableMassStorage(void)
{
    USB_Mass_Storage_Enable = FALSE;
}

/*******************************************************************************
* Function Name  : USB_EnableMassStorage
* Description    : Sets the USB_Mass_Storage_Enable flag 
*                  (static local to usb_cmds.c)
* Input          : None
* Output         : None
* Return         : None
*******************************************************************************/
void USB_EnableMassStorage(void)
{
    USB_Mass_Storage_Enable = TRUE;
}
