/**
  **************** (C) COPYRIGHT 2011 SCT Performance, LLC *********************
  * File Name          : bluetooth_api.c
  * Device / System    : SCT products using the "CommonCode" platform
  * Author             : Quyen Leba
  *
  * Version            : 1 
  * Date               : 05/02/2011
  * Description        : 
  *                    : 
  *
  *
  * History            : 05/02/2011 M. Davis
  *                    :   Add SCT Copyright header,
  *
  ******************************************************************************
  */

#include <string.h>
#include <stm32f10x_usart.h>
#include <stm32f10x_gpio.h>
#include <board/genplatform.h>
#include <board/rtc.h>
#include <board/delays.h>
#include "bluetooth_api.h"
#include "bluetooth.h"

extern SbDeviceInfoType SbDevInfo;
extern SBEvent_T SbEvent;
extern BtDeviceInfo DeviceInfoList[BLUETOOTH_MAX_DEVICE_COUNT];
extern u32 DevPtr;
extern SBLinkDataStatus SbLinkDataStatus;

/******************************************************************************  
    Function: SbSendCommand

	Description
	-----------
    This function sends the specified simply blue command bytes over usart.
    The routine is a blocking call except for SPP_SEND_DATA command. The routine
    either will timeout or will be unblocked as soon as simply blue software
    receives and processes corresponding CFM message for the simply blue command
    being sent. SbProcessFrame routine unblocks the pending event.

	Arguments
	---------
    SbCommand : simply blue command bytes  
    Size      : size of simply blue command frame

    Returns
	-------
    A SBStatus_T value indicating result of the operation.
	
*******************************************************************************/
SBStatus_T SbSendCommand(u8* SbCommand, u16 Size)
{
    u16 checksum;
   
    SbCommand[0]    = STX;
    SbCommand[1]    = REQ;
    checksum        = SbCommand[1] + SbCommand[2] + SbCommand[3] + SbCommand[4];
    SbCommand[5]    = checksum % 256;
	
    SbCommand[Size - 1] = ETX;
        
    bluetooth_uart_tx(SbCommand, Size);

    SbDevInfo.SbCmdStatus = SBSTATUS_INPROCESS;
    
    if (SbCommand[2] == SPP_SEND_DATA) 
    {
        return SBSTATUS_OK;
    }
    
    // PD !?! may need to add some code to make right
    if(SbDevInfo.SbPacketState == SBPACKET_ERR)
        return SBSTATUS_ERROR;
    
    return SBSTATUS_OK;
}

/******************************************************************************  
    Function: SbWaitForCfm

	Description
	-----------
    Wait for confirmation of a specified simply blue command. This version of
    the function can block.

	Arguments
	---------
    u8 Opcode : the simply blue command opcode that was sent prior to this wait for confirm0  

    Returns
	-------
    Always SBStatus_T SBSTATUS_OK, if the function returns at all
	
*******************************************************************************/
SBStatus_T SbWaitForCfm(u8 Opcode)
{
    while(1)
    {
        if((SbEvent.bOpcode == Opcode) && (SbEvent.bType == CFM) 
           && (SbDevInfo.SbCmdStatus == SBSTATUS_OK))
        {
            return SBSTATUS_OK;
        }
    }
}

//TODOQ: this, clean this
SBStatus_T SbWaitForCfmNoWait(u8 Opcode)
{
    u32 timeout;
    //!?! PD. need time out?
    rtc_reset();
    timeout = (rtc_getvalue() + 2000);
    while(1)
    {
        if((SbEvent.bOpcode == Opcode) && (SbEvent.bType == CFM) 
           && (SbDevInfo.SbCmdStatus == SBSTATUS_OK))
        {
            return SBSTATUS_OK;
        }
        else if(rtc_getvalue() > timeout)
        {
            return SBSTATUS_TIMEOUT;
        }
    }
}

/******************************************************************************  
    Function: SbWaitForCfmTimeOut

	Description
	-----------
    Wait for confirmation of a specified simply blue command. This version returns
    a time-out if the confirmation doesn't come back in a reasonable interval.

	Arguments
	---------
    u8 Opcode : the simply blue command opcode that was sent prior to this wait for confirm  

    Returns
	-------
    SBStatus_T SBSTATUS_OK if a valid confirmation occurs before the time-out. 
    Otherwise, SBSTATUS_TIMEOUT
	
*******************************************************************************/
SBStatus_T SbWaitForCfmTimeOut(u8 Opcode)
{
    u32 TO = 0;
    
    while(1)
    {
        if((SbEvent.bOpcode == Opcode) && (SbEvent.bType == CFM) 
           && (SbDevInfo.SbCmdStatus == SBSTATUS_OK))
        {
            return SBSTATUS_OK;
        }

        else
        {
           TO++;
        }
        
        if(TO>=0x001FFFFF)
        {
            return SBSTATUS_TIMEOUT;
        }
    }
}

/******************************************************************************  
Function: SbConfigureLocalDevice

Description
-----------
This function resets the simply blue device to factory settings first, next
deletes all sdp records, sets operation mode as Command mode, and finally does 
Software RESET on local simply blue device.Please check simply blue
software documentation for the details of these operations.

Arguments
---------
None   

Returns
-------
A SBStatus_T value indicating result of the operation.

*******************************************************************************/
SBStatus_T SbConfigureLocalDevice()
{
    SBStatus_T status;
    
    status = SbResetFactorySettings();
    
    if (status == SBSTATUS_OK) 
    {
        status = SbSetOperationMode(SB_COMMAND_MODE);
        if (status == SBSTATUS_OK) 
        {                
            status = SbResetDevice();
            bluetooth_changebaudrate(BT_9600_BAUD);
            delays(1000,'m');
            status = SBSTATUS_OK;
        }
    }
    return status;
}

/******************************************************************************  
Function: SbResetFactorySettings

Description
-----------
This function initializes the simply blue device opeartion with factory settings
please check simply blue software documentation for the details.

Arguments
---------
None  : 

Returns
-------
A SBStatus_T value indicating result of the operation.

*******************************************************************************/
SBStatus_T SbResetFactorySettings()
{
    u16 payloadlen;
    u8  SbCommand[7];
    
    payloadlen = 0x0000;
    
    SbCommand[2] = RESTORE_FACTORY_SETTINGS; 
    SbCommand[3] = (payloadlen & 0x00FF); // payload size is stored 
    SbCommand[4] = (payloadlen >> 8);     // in little endian fashion
    
    SbSendCommand(SbCommand, 7 + payloadlen);
    delays(500,'m');
    return SBSTATUS_OK;
    
}

/******************************************************************************  
Function: SbResetDevice

Description
-----------
This function does Software RESET on local simply blue device.Please check simply blue
software documentation for the details.

Arguments
---------
None   

Returns
-------
A SBStatus_T value indicating result of the operation.

*******************************************************************************/
SBStatus_T SbResetDevice()
{
    u16 payloadlen;
    u8  SbCommand[7];
    
    payloadlen = 0x0000;
    
    SbCommand[2] = BT_RESET; 
    SbCommand[3] = (payloadlen & 0x00FF); // payload size is stored 
    SbCommand[4] = (payloadlen >> 8);     // in little endian fashion
    
    SbSendCommand(SbCommand, 7 + payloadlen);
    return SBSTATUS_OK;
}

/******************************************************************************  
Function: SbWaitResetResp

Description
-----------
Wait for the reset response 

Arguments
---------
None   

Returns
-------
A SBStatus_T value indicating result of the operation.

*******************************************************************************/
SBStatus_T SbWaitResetResp()
{
    SbDevInfo.SbCmdStatus = SBSTATUS_OK;
    while(1)
    {
        if((SbEvent.bType == CFM) && 
           (SbDevInfo.SbPacketState == SBPACKET_START))
        {
            return SBSTATUS_OK;        
        }
    }
}

/******************************************************************************  
Function: SbChangeNvsUartSpeed

Description
-----------
Changes baud rate saved in the NVS.

Arguments
---------
CodecType : valid values follow SbCodecType_T enum type
AirFormat : valid values follow SbAirFormat_T enum type.

Returns
-------
A SBStatus_T value indicating whether default audio settings are stored successfully in 
simply Blue NVS or not.
*******************************************************************************/
SBStatus_T SbChangeNvsUartSpeed(SbBaudrate btbaud)
{
    u16 payloadlen;
    u8  SbCommand[9];
    
    payloadlen = 0x0001;
    
    SbCommand[2] = CHANGE_NVS_UART_SPEED; 
    SbCommand[3] = (payloadlen & 0x00FF); // payload size is stored 
    SbCommand[4] = (payloadlen >> 8);     // in little endian fashion
    
    SbCommand[6] = btbaud;
	
    SbSendCommand(SbCommand, 7 + payloadlen);
    if (SbWaitForCfmTimeOut(SbCommand[2]) == SBSTATUS_OK)
    {  
        SbResetDevice();
        SbWaitResetResp();
        
        return SBSTATUS_OK;
    }
    
    return SBSTATUS_ERROR;
}

/******************************************************************************  
Function: SbSetNvsDefaultAudioSettings

Description
-----------
This function sets the default audio settings in Simply Blue NVS storage. These
settings are used while creating bluetooth audio link, to configure AAI bus and any external
audio codec boards connected to simply blue board.

Arguments
---------
CodecType : valid values follow SbCodecType_T enum type
AirFormat : valid values follow SbAirFormat_T enum type.

Returns
-------
A SBStatus_T value indicating whether default audio settings are stored successfully in 
simply Blue NVS or not.
*******************************************************************************/
SBStatus_T SbSetNvsDefaultAudioSettings(u8 CodecType, u8 AirFormat)
{
    u16 payloadlen;
    u8  SbCommand[9];
    
    payloadlen = 0x0002;
    
    SbCommand[2] = SET_DEFAULT_AUDIO_CONFIG; 
    SbCommand[3] = (payloadlen & 0x00FF); // payload size is stored 
    SbCommand[4] = (payloadlen >> 8);     // in little endian fashion
    
    SbCommand[6] = CodecType;
    SbCommand[7] = AirFormat;
	
    return SbSendCommand(SbCommand, 7 + payloadlen);
}
/******************************************************************************  
Function: SbNvsWriteDeviceName

Description
-----------
This function writes the local device name in the NVS.

Arguments
---------
Name          : Name of the local device will be fetched in to this parameter. The applications
should make sure to pass in the same size as BtNameType specifies in sb.h

Returns
-------
Status of the command, TRUE command successfull, FALSE command failed

*******************************************************************************/
void SbNvsWriteDeviceName(BtNameType Name)
{
    u16 payloadlen;
    u8  SbCommand[7+sizeof(BtNameType)];
    u8  len = strlen((char *)Name);
    
    payloadlen = len + 1;   // Payload length
    
    SbCommand[2] = GAP_WRITE_LOCAL_NAME; 
    SbCommand[3] = (payloadlen & 0x00FF); // payload size is stored 
    SbCommand[4] = (payloadlen >> 8);     // in little endian fashion
    SbCommand[6] = len;
    strcpy((char *)SbCommand+7,(char *)Name);
    
    SbSendCommand(SbCommand, 7 + payloadlen);
    SbWaitForCfm(SbCommand[2]);
    
}

/******************************************************************************  
Function: SbNvsReadDeviceName

Description
-----------
This function gets the local device name

Arguments
---------
Name          : Name of the local device will be fetched in to this parameter. The applications
should make sure to pass in the same size as BtNameType specifies in sb.h

MaxNameLength : The actual length of the local device name that is being received.

Offset        : unused parameter

Modifies
-------
Name          : If successful, BlueTooth device name   
MaxNameLength : If successful, length of the BlueTooth device name, 0 if time-out occurs


Returns
-------
SBStatus_T status : SBSTATUS_TIMEOUT if a time-out occurs while waiting for BT command 
confirmation. Otherwise, SBSTATUS_OK


*******************************************************************************/
SBStatus_T SbNvsReadDeviceName(BtNameType Name, u8 *MaxNameLength)
{
    u16 payloadlen;
    u8  SbCommand[7];
    u8  len;
    int    i;
    SBStatus_T status = SBSTATUS_OK;
    
    payloadlen = 0x0000;
    
    SbCommand[2] = GAP_READ_LOCAL_NAME; 
    SbCommand[3] = (payloadlen & 0x00FF); // payload size is stored 
    SbCommand[4] = (payloadlen >> 8);     // in little endian fashion
    
    len    = 0;
    
    SbSendCommand(SbCommand, 7 + payloadlen);
    status = SbWaitForCfmTimeOut(SbCommand[2]);
    if(status != SBSTATUS_OK)
        return SBSTATUS_TIMEOUT;
    
    len=SbEvent.pPayload[1];
    for (i=0;i<len;i++) 
    {
        Name[i]=SbEvent.pPayload[i+2];
    }
    Name[i] = 0x00; // Null terminate
    *MaxNameLength = len;
    
    return status;
}

/******************************************************************************  
Function: SbNvsWriteDeviceClass

Description
-----------
Writes a BlueTooth device class in the nvs area of the BlueTooth module

Arguments
---------
BdDeviceClass Class:  BlueTooth device-class code (3-byte array)

Returns
-------
Nothing

*******************************************************************************/
void SbNvsWriteDeviceClass(BdDeviceClass Class)
{
    int i;
    u16 payloadlen;
    u8  SbCommand[10];
    
    payloadlen = 0x0003;
    
    SbCommand[2] = STORE_CLASS_OF_DEVICE; 
    SbCommand[3] = (payloadlen & 0x00FF); // payload size is stored 
    SbCommand[4] = (payloadlen >> 8);     // in little endian fashion
    for(i=0;i<3;i++)
    {
        SbCommand[6+i] = Class[2-i];
    }
    
    SbSendCommand(SbCommand, 7 + payloadlen);
    SbWaitForCfmTimeOut(SbCommand[2]);
    
}
/******************************************************************************  
Function: SbNvsWriteBdAddr

Description
-----------
This function writes the local device's Bluetooth Device Address

Arguments
---------
BdAddr : BD address of the local device will be fetched into this parameter. 


Returns
-------
None.

*******************************************************************************/
void SbNvsWriteBdAddr(BdAddrType BdAddr)
{
    int i;
    u16 payloadlen;
    u8  SbCommand[13];
    
    payloadlen = 0x0006;
    
    SbCommand[2] = CHANGE_LOCAL_BDADDRESS; 
    SbCommand[3] = (payloadlen & 0x00FF); // payload size is stored 
    SbCommand[4] = (payloadlen >> 8);     // in little endian fashion
    for(i=0;i<6;i++)
    {
        SbCommand[6+i] = BdAddr[5-i];
    }
    
    SbSendCommand(SbCommand, 7 + payloadlen);
    SbWaitForCfm(SbCommand[2]);
    
}
/******************************************************************************  
Function: SbNvsReadFixedPIN

Description
-----------
This function gets the local device PIN

Arguments
---------
Name          : PIN Name of the local device will be fetched in to this parameter. The applications
should make sure to pass in the same size as BtNameType specifies in sb.h

Returns
-------
Length of the PIN

*******************************************************************************/
u8 SbNvsReadFixedPIN(BdFixedPIN Name)
{
    u16 payloadlen;
    u8  SbCommand[7];
    u8  len;
    int    i;
    
    payloadlen = 0x0000;
    
    SbCommand[2] = GAP_GET_FIXED_PIN; 
    SbCommand[3] = (payloadlen & 0x00FF); // payload size is stored 
    SbCommand[4] = (payloadlen >> 8);     // in little endian fashion
    
    len    = 0;
    
    SbSendCommand(SbCommand, 7 + payloadlen);
    SbWaitForCfm(SbCommand[2]);
    
    len=SbEvent.pPayload[1];
    for (i=0;i<len;i++) 
    {
        Name[i]=SbEvent.pPayload[i+2];
    }
    
    return len;
}
/******************************************************************************  
Function: SbNvsWritePIN

Description
-----------
This function writes the local device's Bluetooth PIN

Arguments
---------
BdFixedPIN : PIN local device will be fetched into this parameter. 


Returns
-------
None.

*******************************************************************************/
void SbNvsWritePIN(BdFixedPIN PIN)
{
    int i;
    u16 payloadlen;
    u16 len = 0;
    u8  SbCommand[15];
    
    len = (u16)strlen((char *)PIN);
    
    payloadlen = len + 1; // Payloadlen is size of 1 + pinlength
    
    SbCommand[2] = GAP_SET_FIXED_PIN; 
    SbCommand[3] = (payloadlen & 0x00FF); // payload size is stored 
    SbCommand[4] = (payloadlen >> 8);     // in little endian fashion
    SbCommand[6] = (u8)len;
    for(i=0;i<len;i++)
    {
        SbCommand[7+i] = PIN[i];
    }
    SbSendCommand(SbCommand, 7 + payloadlen);
    SbWaitForCfm(SbCommand[2]);
}


/******************************************************************************  
Function: SbNvsReadBdAddr

Description
-----------
This function gets the local device's Bluetooth Device Address

Arguments
---------
BdAddr : BD address of the local device will be fetched into this parameter. 


Returns
-------
None.

*******************************************************************************/
void SbNvsReadBdAddr(BdAddrType BdAddr)
{
    u16 payloadlen;
    u8  SbCommand[7];
    int i;
    
    payloadlen = 0x0000;
    
    SbCommand[2] = GAP_READ_LOCAL_BDA; 
    SbCommand[3] = (payloadlen & 0x00FF); // payload size is stored 
    SbCommand[4] = (payloadlen >> 8);     // in little endian fashion
    
    SbSendCommand(SbCommand, 7 + payloadlen);
    SbWaitForCfmTimeOut(SbCommand[2]);
    
    for (i=1;i<7;i++) 
    {
        BdAddr[i-1]=SbEvent.pPayload[i];
    }               
}

/******************************************************************************  
Function: SbDeleteSdpRecords

Description
-----------
This function deletes all the SDP records on the local device.

Arguments
---------
None.

Returns
-------
A SBStatus_T value indicating result of the operation.

*******************************************************************************/
SBStatus_T SbDeleteSdpRecords()
{
    u16 payloadlen;
    u8  SbCommand[7];
    
    payloadlen = 0x0000;
    
    SbCommand[2] = DELETE_SDP_RECORDS; 
    SbCommand[3] = (payloadlen & 0x00FF); // payload size is stored 
    SbCommand[4] = (payloadlen >> 8);     // in little endian fashion
    
    SbSendCommand(SbCommand, 7 + payloadlen);
    SbWaitForCfm(SbCommand[2]);
    if(SbEvent.pPayload[0] == ERROR_OK)
        return SBSTATUS_OK;
    else
        return SBSTATUS_ERROR;
}

/******************************************************************************  
Function: SbStoreSdpRecord

Description
-----------
This function stores a SDP record on the local device.

Arguments
---------
entry  : sdp record bytes
length : length of the sdp record

Returns
-------
A SBStatus_T value indicating result of the operation.

*******************************************************************************/
SBStatus_T SbStoreSdpRecord(u8* entry, u16 length)
{
    u16 i;
    u16 payloadlen;
    u8  SbCommand[340];
    
    payloadlen = length;
    
    SbCommand[2] = STORE_SDP_RECORD; 
    SbCommand[3] = (payloadlen & 0x00FF); // payload size is stored 
    SbCommand[4] = (payloadlen >> 8);     // in little endian fashion
    
    for (i=0;i<length;i++)
        SbCommand[6 + i] = entry[i];	
    
    SbSendCommand(SbCommand, 7 + payloadlen);
    SbWaitForCfm(SbCommand[2]);
    if(SbEvent.pPayload[0] == ERROR_OK)
        return SBSTATUS_OK;
    else
        return SBSTATUS_ERROR;
}

/******************************************************************************  
Function: SbSetOperationMode

Description
-----------
This function sets simply blue device operation mode.Please check simply blue
software documentation for the details.

Arguments
---------
mode  : simply blue operation mode that follows SBOperationMode_T enum.

Returns
-------
A SBStatus_T value indicating result of the operation.

*******************************************************************************/
SBStatus_T SbSetOperationMode(SBOperationMode_T mode)
{
    u16 payloadlen;
    u8  SbCommand[8];
    
    payloadlen = 0x0001;
    
    SbCommand[2] = WRITE_OPERATION_MODE; 
    SbCommand[3] = (payloadlen & 0x00FF); // payload size is stored 
    SbCommand[4] = (payloadlen >> 8);     // in little endian fashion
    SbCommand[6] = mode;
    
    SbSendCommand(SbCommand, 7 + payloadlen);
    SbWaitForCfmTimeOut(SbCommand[2]);
    if(SbEvent.pPayload[0] == ERROR_OK)
        return SBSTATUS_OK;
    else
        return SBSTATUS_ERROR;
}

/******************************************************************************  
Function: SbEnterTransparentMode

Description
-----------
This function sets simply blue device into Transparent Mode. To exit use
SbSendUartBreak() function.
NOTE: Transparent mode can only be used when only ONE comm port is in use!


Arguments
---------
LocalPort  : Local comm port to put into transparent. 

Returns
-------
A SBStatus_T value indicating result of the operation.

*******************************************************************************/
SBStatus_T SbEnterTransparentMode(u8 LocalPort)
{
    u16 payloadlen;
    u8  SbCommand[8];
    
    payloadlen = 0x0001;
    
    SbCommand[2] = SPP_TRANSPARENT_MODE; 
    SbCommand[3] = (payloadlen & 0x00FF); // payload size is stored 
    SbCommand[4] = (payloadlen >> 8);     // in little endian fashion
    SbCommand[6] = LocalPort;
    
    SbSendCommand(SbCommand, 7 + payloadlen);
    SbWaitForCfm(SbCommand[2]);
    if(SbEvent.pPayload[0] == ERROR_OK)
        return SBSTATUS_OK;
    else
        return SBSTATUS_ERROR;
}



/****************************************************************************
Function: SbSendUartBreak

Description
-----------
This function switches the module to command mode.
This function sends a UART Break to leave transparent mode.

Arguments
---------
None.

Returns
-------
None. 

****************************************************************************/
void SbSendUartBreak()
{
    //USART_SendBreak(USART2);
    
    
    GPIO_InitTypeDef GPIO_InitStructure;
    
    //while(USART_GetFlagStatus(USART2, USART_FLAG_IDLE) == RESET);
    while(USART_GetFlagStatus(USART2, USART_FLAG_TC) == RESET);
    
    // -- GPIO_Configuration --    
    // Configure USART2 RTS and USART2 Tx as alternate function push-pull 
    GPIO_InitStructure.GPIO_Pin = GPIO_Pin_2;
    GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
    GPIO_InitStructure.GPIO_Mode = GPIO_Mode_Out_PP;
    GPIO_Init(GPIOA, &GPIO_InitStructure);
    GPIO_ResetBits(GPIOA, GPIO_Pin_1);
    delays(100,'u');
    GPIO_SetBits(GPIOA, GPIO_Pin_1);
    
    // Configure USART2 RTS and USART2 Tx as alternate function push-pull 
    GPIO_InitStructure.GPIO_Pin = GPIO_Pin_2;
    GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AF_PP;
    GPIO_Init(GPIOA, &GPIO_InitStructure);
    
    return;
}

/****************************************************************************
Function: SbInquiry

Description
-----------
Searches for discoverable devices in range. When device found indicators
are recieved, device info is stored in DeviceInfoList array. 

Arguments
---------
None.

Returns
-------
DevPtr - Number of devices found. 

****************************************************************************/
u32 SbInquiry()
{
    u16 payloadlen;
    //    SBStatus_T status;
    u8  SbCommand[9];
    DevPtr = 0;       // Pointer to DeviceInfoList
    
    payloadlen = 0x0003;
    
    SbCommand[2] = GAP_INQUIRY; 
    SbCommand[3] = (payloadlen & 0x00FF); // payload size is stored 
    SbCommand[4] = (payloadlen >> 8);     // in little endian fashion
    // --- PayLoad Data ---
    SbCommand[6] = 0x0F; // Wait 10 seconds for duration of inquiry (0x1 = 1.28s, 0x30 max)
    SbCommand[7] = 0;  // num of resps (0-0xff) 0 is unlimited
    SbCommand[8] = 0;  // num of resps (0-0xff) 0 is unlimited
    // --------------------
    /*status = */SbSendCommand(SbCommand, 7 + payloadlen);
    SbWaitForCfm(SbCommand[2]);
    
    return DevPtr; // Return device count    
}

/****************************************************************************
Function: SbRemoteDeviceName

Description
-----------
Retrieves the device's friendly name, then stores it in the appropriate
device info array    

Arguments
---------
DeviceNum - Number of the device in the DeveiceInfoArray that you want to 
request the name from.

Returns
-------
None. 

****************************************************************************/
void SbRemoteDeviceName(u8 DeviceNum)
{
    u16 payloadlen;
    //    SBStatus_T status;
    u8  SbCommand[14];
    u8 retry = 0;
    
    payloadlen = 0x0006;
    
    SbCommand[2] = GAP_REMOTE_DEVICE_NAME; 
    SbCommand[3] = (payloadlen & 0x00FF); // payload size is stored 
    SbCommand[4] = (payloadlen >> 8);     // in little endian fashion
    
    // --- PayLoad Data ---
    memcpy(SbCommand+6, DeviceInfoList[DeviceNum].DeviceAddr, 6);
    // --------------------
    for(retry=0;retry<3;retry++)
    {
        /*status = */SbSendCommand(SbCommand, 7 + payloadlen);
        SbWaitForCfm(SbCommand[2]);
        
        if(SbEvent.pPayload[0] == 0x00) // Check Status 0 = Error_OK
        {
            memset(DeviceInfoList[DeviceNum].DeviceName, 0, sizeof(BtNameType));
            memcpy(DeviceInfoList[DeviceNum].DeviceName, SbEvent.pPayload+8, SbEvent.pPayload[7]);                    
            return;
        }
        else
        {
            delays(1,'m');
            break;
        }
    }
    
    strcpy((char *)DeviceInfoList[DeviceNum].DeviceName, "Name Error");
    return;
}

/****************************************************************************
Function: SbGetComm

Description
-----------
Gets the comm port number from the remote device.

Arguments
---------
DeviceNum - Number of the device in the DeveiceInfoArray that you want to 
request the name from.

Returns
-------
None. 

****************************************************************************/
u8 SbGetComm(u8 DevNum)
{
    u16 payloadlen;
    //    SBStatus_T status;
    u8  SbCommand[13];
    
    // SDAP Connect
    payloadlen = 0x0006;
    
    SbCommand[2] = SDAP_CONNECT; 
    SbCommand[3] = (payloadlen & 0x00FF); // payload size is stored 
    SbCommand[4] = (payloadlen >> 8);     // in little endian fashion
    memcpy(SbCommand+6, DeviceInfoList+DevNum,6); // Copy device addr into cmd
    /*status = */SbSendCommand(SbCommand, 7 + payloadlen);
    SbWaitForCfm(SbCommand[2]);
    
    //if(status != SBSTATUS_OK)
    //    return 1; // Error
    
    // SDAP Service Browse: SPP 0x1101 (Get remote comm port)
    payloadlen = 0x0002;
    SbCommand[2] = SDAP_SERVICE_BROWSE; 
    SbCommand[3] = (payloadlen & 0x00FF); // payload size is stored 
    SbCommand[4] = (payloadlen >> 8);     // in little endian fashion
    SbCommand[6] = 0x01;    // SPP 0x1101 byte swapped
    SbCommand[7] = 0x11; 
    /*status = */SbSendCommand(SbCommand, 7 + payloadlen);
    SbWaitForCfm(SbCommand[2]);
    
    //if(status != SBSTATUS_OK)
    //    return 1; // Error
    
    if(SbEvent.pPayload[0] == 0x00) // Check Status 0 = Error_OK
    {
        DeviceInfoList[DevNum].RemoteCommNum = SbEvent.pPayload[6]; // Comm port number
    }
    else
        return 1;
    
    // SDAP Disconnect
    payloadlen = 0x0000;
    SbCommand[2] = SDAP_DISCONNECT; 
    SbCommand[3] = (payloadlen & 0x00FF); // payload size is stored 
    SbCommand[4] = (payloadlen >> 8);     // in little endian fashion
    /*status = */SbSendCommand(SbCommand, 7 + payloadlen);
    SbWaitForCfm(SbCommand[2]);
    
    //if(status != SBSTATUS_OK)
    //    return 1; // Error
    
    return 0; // Success
}

/******************************************************************************  
Function: SbEstablishLink

Description
-----------
This function initates establishment of ACL link with remote device that is 
specified by passed in Bd Address. The serial port link is established between 
specified local port and remote port on the local and remote devices. 

This function waits on Mbox events untill simply blue CFM message and 
link_established messages are processed.

Arguments
---------
DeviceNum - Number of the device in the DeveiceInfoArray that you want to 
request the name from.

Related Info stored in DeviceInfoList array -
BdAddr       : Bd Address of the remote device
LocalPortNo  : port number of the local device
RemotePortNo : port number of the remote device

Returns
-------
A SBStatus_T value indicating result of the operation.

*******************************************************************************/
SBStatus_T SbEstablishLink(u8 DevNum)
{
    u16 payloadlen;
    
    u8  SbCommand[15];
    
    payloadlen = 0x0008;
    
    SbCommand[2] = SPP_ESTABLISH_LINK; 
    SbCommand[3] = (payloadlen & 0x00FF); // payload size is stored 
    SbCommand[4] = (payloadlen >> 8);     // in little endian fashion
    
    // -- Payload Data ---
    SbCommand[6] = 0x01;           // Localport
    memcpy(SbCommand+7, DeviceInfoList[DevNum].DeviceAddr, 6); //BDA        
    SbCommand[13] = DeviceInfoList[DevNum].RemoteCommNum;      // Remoteport
    // -------------------
    SbSendCommand(SbCommand, 7 + payloadlen);
    SbWaitForCfm(SbCommand[2]);
    if(SbEvent.pPayload [0] == 0)
    {
        return SBSTATUS_OK;
    }
    else
        return SBSTATUS_ERROR;
}
/******************************************************************************  
Function: SbReleaseLink

Description
-----------
This function initates release of SPP link associated with the port number specified
on the local device.

This function waits on Mbox events untill simply blue CFM message and 
link_released messages are processed.

Arguments
---------
LocalPortNo  : port number of the local device

Returns
-------
A SBStatus_T value indicating result of the operation.

*******************************************************************************/
SBStatus_T SbReleaseLink(u8 LocalPortNo)
{
    u16 payloadlen;
    u8  SbCommand[8];
    
    payloadlen = 0x0001;
    
    SbCommand[2] = SPP_RELEASE_LINK; 
    SbCommand[3] = (payloadlen & 0x00FF); // payload size is stored 
    SbCommand[4] = (payloadlen >> 8);     // in little endian fashion
    
    SbCommand[6] = LocalPortNo; 
	
    SbSendCommand(SbCommand, 7 + payloadlen);
    SbWaitForCfm(SbCommand[2]);
    
    if(SbEvent.pPayload [0] == 0)
    {
        return SBSTATUS_OK;
    }
    
    return SBSTATUS_ERROR;
}

/******************************************************************************  
Function: SbSPPSendData

Description
-----------
This function uses the SPP profile (Command Mode) to send data to the remote
device

Arguments
---------
LocalPortNo  : port number of the local device
Buffer - Data to send
Len - Length of data to be sent

Returns
-------
A SBStatus_T value indicating result of the operation.

*******************************************************************************/
SBStatus_T SbSPPSendData(u8 LocalPortNo, u8* buffer, u16 len)
{
    if(SbLinkDataStatus != ESTABLISHED)  // Check link status
        return SBSTATUS_ERROR;
    
    u16 payloadlen;
    u8  SbCommand[340];
    
    if(len > (sizeof(SbCommand)-10))
    {
        // Data length is too big
        return SBSTATUS_ERROR;   
    }
    
    payloadlen = len+3;
    
    SbCommand[2] = SPP_SEND_DATA; 
    SbCommand[3] = (payloadlen & 0x00FF); // payload size is stored 
    SbCommand[4] = (payloadlen >> 8);     // in little endian fashion
    
    SbCommand[6] = LocalPortNo;
    SbCommand[7] = (len & 0x00FF); // payload size is stored 
    SbCommand[8] = (len >> 8);     // in little endian fashion
    
	memcpy(SbCommand+9, buffer, len); // Copy data to send
    
    SbSendCommand(SbCommand, 7 + payloadlen);
    SbWaitForCfm(SbCommand[2]);
    if(SbEvent.pPayload [0] == 0)
    {
        return SBSTATUS_OK;
    }
    
    return SBSTATUS_ERROR;
}

/******************************************************************************  
Function: SbSPPSendDataNoWait

Description
-----------
This function uses the SPP profile (Command Mode) to send data to the remote
device

Arguments
---------
LocalPortNo  : port number of the local device
Buffer - Data to send
Len - Length of data to be sent

Returns
-------
A SBStatus_T value indicating result of the operation.

*******************************************************************************/
SBStatus_T SbSPPSendDataNoWait(u8 LocalPortNo, u8* buffer, u16 len)
{
    if(SbLinkDataStatus != ESTABLISHED)  // Check link status
        return SBSTATUS_ERROR;
    
    u16 payloadlen;
    u8  SbCommand[340];
    
    if(len > (sizeof(SbCommand)-10))
    {
        // Data length is too big
        return SBSTATUS_ERROR;   
    }
    
    payloadlen = len+3;
    
    SbCommand[2] = SPP_SEND_DATA; 
    SbCommand[3] = (payloadlen & 0x00FF); // payload size is stored 
    SbCommand[4] = (payloadlen >> 8);     // in little endian fashion
    
    SbCommand[6] = LocalPortNo;
    SbCommand[7] = (len & 0x00FF); // payload size is stored 
    SbCommand[8] = (len >> 8);     // in little endian fashion
    
	memcpy(SbCommand+9, buffer, len); // Copy data to send
    
    SbSendCommand(SbCommand, 7 + payloadlen);
    SbWaitForCfmNoWait(SbCommand[2]);
    if(SbEvent.pPayload [0] == 0)
    {
        return SBSTATUS_OK;
    }
    
    return SBSTATUS_ERROR;
}

/****************************************************************************
Function: SbGetPortStatus

Description
-----------
Get the current state of the modem status and line status at application level. This
command resets the value (to 0) of the following members of the port status:
OverrunError
ParityError
FramingError
BreakLength
The value of DSR and CTS are only changed when new values are received from the
remote device! 

Arguments
---------
The port number of the port to check.

Returns
-------


****************************************************************************/
u32 SbGetPortStatus(u8 portnumber, u8* response)
{
    u16 payloadlen;
    u8  SbCommand[8];
    
    payloadlen = 0x0001;
    
    SbCommand[2] = SPP_GET_PORT_STATUS; 
    SbCommand[3] = (payloadlen & 0x00FF); // payload size is stored 
    SbCommand[4] = (payloadlen >> 8);     // in little endian fashion
    
    SbCommand[6] = portnumber;
    
    SbSendCommand(SbCommand, 7 + payloadlen);
    SbWaitForCfm(SbCommand[2]);
    
    if(SbEvent.pPayload[0] == 0)
    {
        response[0] = SbEvent.pPayload[2]; // Return the port status byte
        response[1] = SbEvent.pPayload[3]; //(2 Bytes)Return the break length in ms, 
        response[2] = SbEvent.pPayload[4]; //a 0 is no break
        return SBSTATUS_OK;
    }
    
    return SBSTATUS_ERROR;
}
