/**
  **************** (C) COPYRIGHT 2011 SCT Performance, LLC *********************
  * File Name          : usb_cmd_desc.h
  * Device / System    : SCT products using the "CommonCode" platform
  * Author             : Quyen Leba
  *
  * Version            : 1 
  * Date               : 05/02/2011
  * Description        : 
  *                    : 
  *
  *
  * History            : 05/02/2011 M. Davis
  *                    :   Add SCT Copyright header,
  *
  ******************************************************************************
  */

/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __USB_CMD__DESC_H
#define __USB_CMD__DESC_H

/* Includes ------------------------------------------------------------------*/
#include "stm32f10x_type.h"
#include "usb_conf.h"
/* Exported types ------------------------------------------------------------*/
/* Exported constants --------------------------------------------------------*/
/* Exported macro ------------------------------------------------------------*/
/* Exported define -----------------------------------------------------------*/
#define UCMD_SIZ_DEVICE_DESC              18
#define UCMD_SIZ_CONFIG_DESC              32

#define UCMD_SIZ_STRING_LANGID            4
#define UCMD_SIZ_STRING_VENDOR            16
#define UCMD_SIZ_STRING_PRODUCT           30
#define UCMD_SIZ_STRING_SERIAL            26
#define UCMD_SIZ_STRING_INTERFACE         18

/* Exported functions ------------------------------------------------------- */
extern const u8 UCMD_DeviceDescriptor[UCMD_SIZ_DEVICE_DESC];
extern const u8 UCMD_ConfigDescriptor[UCMD_SIZ_CONFIG_DESC];

extern const u8 UCMD_StringLangID[UCMD_SIZ_STRING_LANGID];
extern const u8 UCMD_StringVendor[UCMD_SIZ_STRING_VENDOR];
extern const u8 UCMD_StringProduct[UCMD_SIZ_STRING_PRODUCT];
extern u8 UCMD_StringSerial[UCMD_SIZ_STRING_SERIAL];
extern const u8 UCMD_StringInterface[UCMD_SIZ_STRING_INTERFACE];

#endif /* __USB_CMD__DESC_H */
