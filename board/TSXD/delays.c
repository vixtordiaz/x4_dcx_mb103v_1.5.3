/**
  **************** (C) COPYRIGHT 2011 SCT Performance, LLC *********************
  * File Name          : delays.c
  * Device / System    : SCT products using the "CommonCode" platform
  * Author             : Quyen Leba
  *
  * Version            : 1 
  * Date               : 05/02/2011
  * Description        : 
  *                    : 
  *
  *
  * History            : 05/02/2011 M. Davis
  *                    :   Add SCT Copyright header,
  *
  ******************************************************************************
  */

#include <stm32f10x_systick.h>
#include <board/delays.h>
#include "systick.h"

//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
void delays(u32 time, u8 prefix)
{
    if(prefix == 'm')
    {
        SysTick_SetReload(72000);
    }
    else
    {
        SysTick_SetReload(72);
    }

    systick_setTimingDelay(time);
    systick_expireTimingDelay();
    
    /* Disable the SysTick Counter */
    SysTick_CounterCmd(SysTick_Counter_Disable);
    
    /* Clear the SysTick Counter */
    SysTick_CounterCmd(SysTick_Counter_Clear);
}
