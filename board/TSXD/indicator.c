/**
  **************** (C) COPYRIGHT 2011 SCT Performance, LLC *********************
  * File Name          : indicator.c
  * Device / System    : SCT products using the "CommonCode" platform
  * Author             : Quyen Leba
  *
  * Version            : 1 
  * Date               : 05/02/2011
  * Description        : 
  *                    : 
  *
  *
  * History            : 05/02/2011 M. Davis
  *                    :   Add SCT Copyright header,
  *
  ******************************************************************************
  */

#include <common/statuscode.h>
#include <board/indicator.h>

//------------------------------------------------------------------------------
// Clear any low-level indications
// Return:  u8  status
// Engineer: Quyen Leba
//------------------------------------------------------------------------------
u8 indicator_clear()
{
    return S_SUCCESS;
}

//------------------------------------------------------------------------------
// Set low-level indication for some important task
// Input:   Indicator indicator
// Return:  u8  status
// Engineer: Quyen Leba
//------------------------------------------------------------------------------
u8 indicator_set(Indicator indicator)
{
    switch(indicator)
    {
    case USB_SYNC:
        break;
    case SETTINGS_BACKUP_FAILED:
        break;
    case SETTINGS_CORRUPTED:
        break;
    case ECM_UPLOADING:
        break;
    case ECM_ERASED:
        break;
    case ECM_DOWNLOADING:
        break;
    case DATALOG_SCAN:
        break;
    case DATALOG_START:
        break;
    default:
        break;
    }
    return S_SUCCESS;
}