/**
  **************** (C) COPYRIGHT 2011 SCT Performance, LLC *********************
  * File Name          : vpw.c
  * Device / System    : SCT products using the "CommonCode" platform
  * Author             : Quyen Leba
  *
  * Version            : 1 
  * Date               : 05/02/2011
  * Description        : 
  *                    : 
  *
  *
  * History            : 05/02/2011 M. Davis
  *                    :   Add SCT Copyright header,
  *
  ******************************************************************************
  */

#include <stm32f10x_rcc.h>
#include <stm32f10x_gpio.h>
#include <stm32f10x_tim.h>
#include <board/genplatform.h>

#if (SUPPORT_COMM_VPW)

#include <board/timer.h>
#include <board/delays.h>
#include <common/statuscode.h>
#include "vpw.h"

#define VPWPORT         GPIOE
#define VPWTXPIN        GPIO_Pin_11     //PE11
#define VPWRXPIN        GPIO_Pin_14     //PE14
#define VPWTIM          TIM2
#define Init_VPWTimer() timer2_init()
#define __SCPPORT       GPIOA
#define __PWMTXpPIN     GPIO_Pin_8      //PA8

u32     tmrcnt;

//------------------------------------------------------------------------------
// Private Prototypes
//------------------------------------------------------------------------------
void timer_start();
void timer_stop();
u16 timer_get();
void set_vpwbus_active();
void set_vpwbus_passive();
void wait_vpw_ls_bus_idle();
void wait_vpw_hs_bus_idle();
u8 peek_vpwbus();

//------------------------------------------------------------------------------
// Engineer: Quyen Leba
// Date: Jan 28, 2008
//------------------------------------------------------------------------------
void vpw_init()
{
    GPIO_InitTypeDef GPIO_InitStructure;

    RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOA | RCC_APB2Periph_GPIOE , ENABLE);

    //Config PWM_TXp
    GPIO_StructInit(&GPIO_InitStructure);
    GPIO_InitStructure.GPIO_Pin = __PWMTXpPIN;
    GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
    GPIO_InitStructure.GPIO_Mode = GPIO_Mode_Out_PP;
    GPIO_Init(__SCPPORT, &GPIO_InitStructure);
    //Make sure PWM_TXp does not interfere with VPW_TX
    GPIO_WriteBit(__SCPPORT,__PWMTXpPIN,Bit_SET);

    // Configure VPW_TX
    GPIO_StructInit(&GPIO_InitStructure);
    GPIO_InitStructure.GPIO_Pin = VPWTXPIN;
    GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
    GPIO_InitStructure.GPIO_Mode = GPIO_Mode_Out_PP;
    GPIO_Init(VPWPORT, &GPIO_InitStructure);

    //Config VPW_RX
    GPIO_StructInit(&GPIO_InitStructure);
    GPIO_InitStructure.GPIO_Pin = VPWRXPIN;
    GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
    GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IN_FLOATING;
    GPIO_Init(GPIOE, &GPIO_InitStructure);

    // Set VPW_TX low for passive bus
    set_vpwbus_passive();
    
    Init_VPWTimer();
}

//------------------------------------------------------------------------------
// Reset timer counter
// Engineer: Quyen Leba
// Date: Jan 29, 2008
//------------------------------------------------------------------------------
static inline void timer_start()
{
    //timer2_resetcounter();
    TIM_Cmd(VPWTIM, ENABLE);
    timer2_resetcounter();
}
//------------------------------------------------------------------------------
// Stop timer
// Engineer: Quyen Leba
// Date: Jan 29, 2008
//------------------------------------------------------------------------------
static inline void timer_stop()
{
    TIM_Cmd(VPWTIM, DISABLE);
}
//------------------------------------------------------------------------------
// Get current count value of timer
// Engineer: Quyen Leba
// Date: Jan 29, 2008
//------------------------------------------------------------------------------
static inline u16 timer_get()
{
    //+12 to calibrate it with the time to set/reset pin
    return (timer2_getcounter() /*+ 12*/);
}
//------------------------------------------------------------------------------
// Set VPW TX bus to active state
// Engineer: Quyen Leba
// Date: Jan 29, 2008
//------------------------------------------------------------------------------
static inline void set_vpwbus_active()
{
    GPIO_SetBits(VPWPORT, VPWTXPIN);    //MMBT3904
}
//------------------------------------------------------------------------------
// Reset VPW bus to passive state
// Engineer: Quyen Leba
// Date: Jan 29, 2008
//------------------------------------------------------------------------------
static inline void set_vpwbus_passive()
{
    GPIO_ResetBits(VPWPORT, VPWTXPIN);  //MMBT3904
}
//------------------------------------------------------------------------------
// Check if VPW bus in active state
// Engineer: Quyen Leba
// Date: Jan 29, 2008
//------------------------------------------------------------------------------
static inline u8 is_set_vpwbus_active()
{
    return GPIO_ReadInputDataBit(VPWPORT, VPWRXPIN);
}
//------------------------------------------------------------------------------
// Read VPW bus state
// Engineer: Quyen Leba
// Date: Jan 29, 2008
//------------------------------------------------------------------------------
static inline u8 peek_vpwbus()
{
    return GPIO_ReadInputDataBit(VPWPORT, VPWRXPIN);
}
//------------------------------------------------------------------------------
// Wait until VPW bus is idle
// Engineer: Quyen Leba
// Date: Jan 29, 2008
//------------------------------------------------------------------------------
void wait_vpw_ls_bus_idle()
{
    timer_start();
    while(timer_get() < (RX_IFS_MIN + us2cnt(5)))    // wait for minimum IFS symbol
    {
        if(is_set_vpwbus_active()) timer_start();    // restart timer1 when bus not idle
    }
}
/*
//------------------------------------------------------------------------------
// Check if VPW bus in passive state
// Engineer: Quyen Leba
// Date: Jan 29, 2008
//------------------------------------------------------------------------------
static inline u8 bit_is_clear(void)
{
    return (u8)((~(GPIO_ReadBit(VPWPORT, VPWRXPIN))) & 0x0001);
}
*/
/*
//------------------------------------------------------------------------------
// Check if VPW bus in active state
// Engineer: Quyen Leba
// Date: Jan 29, 2008
//------------------------------------------------------------------------------
static inline u8 bit_is_set(void)
{
    return (u8)(GPIO_ReadBit(VPWPORT, VPWRXPIN));
}
*/

//------------------------------------------------------------------------------
// Send a vpw frame (low speed)
// Inputs:  u8  *databuffer (must be allocated with datalength+1)
//          u8  datalength (max 12 bytes)
// Returns: u8  errorcode (local J1850 error code)
// Engineer: Quyen Leba
// Date: Jan 29, 2008
//------------------------------------------------------------------------------
u8 vpw_ls_tx(u8 *databuffer, u8 datalength)
{
    u32 timer_count;
    u8  temp_byte;  // temporary byte store
    u8  nbits;      // bit position counter within a byte        
    u16 delay;      // bit delay time

    if (datalength > 12)
    {
        // error, message to long, see SAE J1850
        return J1850_RETURN_CODE_DATA_ERROR;
    }

    databuffer[datalength] = obd2_j1850crc(databuffer, datalength);

    wait_vpw_ls_bus_idle();    // wait for idle bus

    timer_start();
    set_vpwbus_active();    // set bus active

    while(1)    // transmit SOF symbol
    {
        timer_count = timer_get();
        if (timer_count >= TX_SOF)
        {
            break;
        }
    }

    do
    {
        temp_byte = *databuffer;    // store byte temporary
        nbits = 8;
        while (nbits--)     // send 8 bits
        {
            if(nbits & 1)   // start always with passive symbol
            {
                set_vpwbus_passive();    // set bus passive
                set_vpwbus_passive();    // set bus passive
                timer_start();
                delays(100,'n');
                // send correct pulse lenght
                delay = (temp_byte & 0x80) ? TX_LONG : TX_SHORT;
                while ((timer_get()) <= delay)    // wait
                {
                    if(peek_vpwbus())
                    {
                        // error, bus collision!
                        timer_stop();
                        return J1850_RETURN_CODE_BUS_ERROR;
                    }
                }
            }
            else    // send active symbol
            {
                set_vpwbus_active();    // set bus active
                timer_start();
                // send correct pulse lenght
                delay = (temp_byte & 0x80) ? TX_SHORT : TX_LONG;
                while (timer_get() <= delay);    // wait
                // no error check needed, ACTIVE dominates
            }
            temp_byte <<= 1;    // next bit
        }// end nbits while loop
        ++databuffer;           // next byte from buffer
    }while(datalength--);       // end datalength do loop
 
    set_vpwbus_passive();       // send EOF symbol
    timer_start();
    while (timer_get() <= TX_EOF);  // wait for EOF complete

    timer_stop();

    return J1850_RETURN_CODE_OK;    // no error
}

//------------------------------------------------------------------------------
// Receive J1850 VPW frame (max 12 bytes)
// Outputs: u8  *databuffer
//          u16 *datalength
// Returns: Number of received bytes OR in case of error, error code with
//          bit 7 set as error indication
// Returns: u8  errorcode (local J1850 error code)
// Engineer: Quyen Leba
// Date: Jan 29, 2008
//------------------------------------------------------------------------------
u8 vpw_ls_rx(u8 *databuffer, u16 *datalength)
{
    u8 nbits;               // bit position counter within a byte
    u8 nbytes;              // number of received bytes
    u8 bit_state;           // used to compare bit state, active or passive
    u8 timeout_count = 0;

    *datalength = 0;

    //wait for responds, return after 100ms IDLE with error
    timer_start();
    while(!is_set_vpwbus_active())      // run as long bus is passive (IDLE)
    {
        if(timer_get() >= WAIT_100us)   // check for 100us
        {
            ++timeout_count;
            timer_start();
        }
        if(timeout_count >= 40*4)       // check for respons timeout 40 * 100us
        {
            timer_stop();
            // error, no responds within timeout (0x85)
            return J1850_RETURN_CODE_NO_DATA | 0x80;
        }
    }

    // wait for SOF
    timer_start();
    // run as long bus is active (SOF is an active symbol)
    while(is_set_vpwbus_active())
    {
        if(timer_get() >=  RX_SOF_MAX)
        {
            // error on SOF timeout (0x84)
            return J1850_RETURN_CODE_DATA_ERROR | 0x80;
        }
    }

    timer_stop();
    if(timer_get() < RX_SOF_MIN)
    {
        // error, symbole was not SOF (0x83)
        return J1850_RETURN_CODE_BUS_ERROR | 0x80;
    }

    bit_state = is_set_vpwbus_active(); // store actual bus state
    timer_start();
    for(nbytes = 0; nbytes <= MAX_VPW_DATA_LENGTH_LS; ++nbytes)
    {
        nbits = 8;
        do
        {
            // compare last with actual bus state, wait for change
            while(is_set_vpwbus_active() == bit_state)
            {
                if(timer_get() >= RX_EOD_MIN)   // check for EOD symbol
                {
                    timer_stop();
                    //return nbytes;      // return number of received bytes
                    *datalength = nbytes;
                    return J1850_RETURN_CODE_OK;
                }
            }

            *databuffer <<= 1;

            bit_state = is_set_vpwbus_active(); // store actual bus state
            timer_stop();
            if(timer_get() < (RX_SHORT_MIN - us2cnt(5)))
            {
                // error, pulse was to short (0x86)
                return J1850_RETURN_CODE_DATA | 0x80;
            }

            // check for short active pulse = "1" bit
            if ((timer_get() < (RX_SHORT_MAX  - us2cnt(2))) &&
                (!is_set_vpwbus_active()))
            {
                *databuffer |= 1;
            }

            // check for long passive pulse = "1" bit
            if ((timer_get() > (RX_LONG_MIN +  - us2cnt(2))) &&
               (timer_get() < RX_LONG_MAX) && (is_set_vpwbus_active()))
            {
                *databuffer |= 1;
            }
            timer_start();  // restart timer
        }while(--nbits);    // end 8 bit while loop
        databuffer++;       // store next byte
    }// end 12 byte for loop

    // return after a maximum of 12 bytes
    timer_stop();
    return J1850_RETURN_CODE_LENGTH_ERROR;
}

//------------------------------------------------------------------------------
// Send a VPW break
//------------------------------------------------------------------------------
void vpw_send_break()
{
    wait_vpw_ls_bus_idle();             // wait for idle bus

    timer_start();    
    set_vpwbus_active();            // set bus active

    while(1)                        // transmit SOF symbol
    {
        if (timer_get() >= TX_BRK)
        {
            break;
        }
    }
    set_vpwbus_passive();           // send EOF symbol

    timer_start();
    while (timer_get() <= TX_EOF);  // wait for EOF complete
    timer_stop();
}

//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
static inline void wait_vpw_hs_bus_idle()
{
    timer_start();
    // wait for minimum IFS symbol
    while(timer_get() < (RX_IFS_MIN_HS+  us2cnt(5)))
    {
        if(is_set_vpwbus_active())
        {
            timer_start();  // restart timer1 when bus not idle
        }
    }
}

//------------------------------------------------------------------------------
// Send a vpw frame (high speed)
// Inputs:  u8  *databuffer (must be allocated with datalength+1)
//          u32 datalength
// Returns: u8  errorcode (local J1850 error code)
// Note: this function puts a checksum byte at databuffer[datalength],
// so, make sure have an extra byte when define databuffer
//------------------------------------------------------------------------------
u8 vpw_hs_tx(u8 *databuffer, u16 datalength)
{
    u8  temp_byte;      // temporary byte store
    u8  nbits;          // bit position counter within a byte
    u16 delay;          // bit delay time
	u8  skip;
	u16 fallTime;
	u16 setupTime;

    databuffer[datalength] = obd2_j1850crc(databuffer, datalength);

    wait_vpw_hs_bus_idle();     // wait for idle bus

    timer_start();
    set_vpwbus_active();        // set bus active

    while(1)    // transmit SOF symbol
    {
        if (timer_get() >= TX_SOF_HS)
        {
            break;
        }
    }
	set_vpwbus_passive();
	timer_start();
	// Measure the amount of time it takes for the bus to fall
    while(peek_vpwbus());
	fallTime = timer_get();
    if(fallTime > (TX_SHORT_HS / 2))
    {
        fallTime = 0;
    }
	setupTime = us2cnt(7); // Compensates for setup time after SOF

    do
    {
        temp_byte = *databuffer;    // store byte temporary
        nbits = 8;
        skip = 4;
        while (nbits--)             // send 8 bits
        {
            //magic # are +2 vs -3: bench standalone & eepod
            if(nbits & 1)   // start always with passive symbol
            {
                set_vpwbus_passive();
                timer_start();
				delay = (temp_byte & 0x80) ? (TX_LONG_HS-setupTime) : (TX_SHORT_HS-setupTime);	//from code after Stephan's timing
                //while (timer_get() <= (delay/*+us2cnt(1)*/))    // wait //+us2cnt(2)
				while (timer_get() <= (delay + us2cnt(2)))	//from code after Stephan's timing
                {
                    if(!skip && peek_vpwbus())	//from code after Stephan's timing
                    {
                        timer_stop();
                        // error, bus collision!
                        return J1850_RETURN_CODE_BUS_ERROR;
                    }
                    if(skip)
                    {
                        skip--;
                    }
                }
            }
            else    // send active symbol
            {
                set_vpwbus_active();
                timer_start();
				delay = (temp_byte & 0x80) ? (TX_SHORT_HS-fallTime) : (TX_LONG_HS-fallTime);	//from code after Stephan's timing
                while (timer_get() <= (delay));    // wait     //-us2cnt(3)
                // no error check needed, ACTIVE dominates
            }
            temp_byte <<= 1;    // next bit
            skip = 4;
            setupTime = 0;
        }// end nbits while loop
        ++databuffer;    // next byte from buffer
    }while(datalength--);// end datalength do loop
    //} while(--datalength);// end datalength do loop
 
    set_vpwbus_passive();    // send EOF symbol

    timer_start();
    while (timer_get() <= TX_EOF_HS); // wait for EOF complete

    timer_stop();
    return J1850_RETURN_CODE_OK;    // no error
}

//------------------------------------------------------------------------------
// Receive a VPW message in high speed
// Output:  u8  *databuffer
//          u32 *datalength
// Return:  u8  errorcode (local J1850 error code)
//------------------------------------------------------------------------------
u8 vpw_hs_rx(u8 *databuffer, u16 *datalength)
{
    u8 timeout_count;
    u8  nbits; 
    u32 nbytes, tbytes;
	u32 time;
    u8  bit_state;

    tbytes = 0;
    //wait for responds, return after 100ms IDLE with error
    timeout_count = 0;
    timer_start();
    while(!is_set_vpwbus_active())      // run as long bus is passive (IDLE)
    {
        if(timer_get() >= (WAIT_25us))  // check for 50us
        {
            ++timeout_count;
            timer_start();
        }
        if(timeout_count >= (40*4))     // check for respons timeout 40 * 50us
        {
            timer_stop();
            // error, no responds within 100ms (0x85)
            return J1850_RETURN_CODE_NO_DATA | 0x80;
        }
    }

    // wait for SOF
    timer_start();
    while(is_set_vpwbus_active())   // run as long bus is active (SOF is an active symbol)
    {
        if(timer_get() >=  (RX_SOF_MAX_HS))
        {
            // error on SOF timeout (0x83)
            return J1850_RETURN_CODE_BUS_ERROR | 0x80;
        }
    }

    timer_stop();

    if(timer_get() < (RX_SOF_MIN_HS))
    {
        // error, symbol was not SOF (0x84)
        return J1850_RETURN_CODE_DATA_ERROR | 0x80;
    }

    bit_state = is_set_vpwbus_active();    // store actual bus state
    timer_start();

    for(nbytes = 0; nbytes <= MAX_VPW_DATA_LENGTH_HS; ++nbytes)
    {
        nbits = 8;
        do
        {
            while(is_set_vpwbus_active() == bit_state) // compare last with actual bus state, wait for change
            {
                if((timer_get()) >= (RX_EOD_MIN_HS))    // check for EOD symbol
                {
                    timer_stop();
                    //time = time - (RX_EOD_MIN_HS+us2cnt(4));
                    //return nbytes;    // return number of received bytes
                    *datalength = nbytes;
                    return J1850_RETURN_CODE_OK;
                }
            }
            *databuffer <<= 1;

            bit_state = is_set_vpwbus_active();    // store actual bus state
            timer_stop();
			time = timer_get();

			if(time < (RX_SHORT_MIN_HS)) return J1850_RETURN_CODE_BUS_BUSY | 0x80;	// error, pulse was to short 0x82
            
            // check for short active pulse = "1" bit
            if((time < (RX_SHORT_MAX_HS)) && 
               !(bit_state))
            {
                *databuffer |= 1;
            }

            // check for long passive pulse = "1" bit
            if((time > (RX_LONG_MIN_HS)) &&
               (time < (RX_LONG_MAX_HS+us2cnt(3))) && 
               bit_state )
            {
                *databuffer |= 1;
            }

            timer_start();    // restart timer
        }while(--nbits);// end 8 bit while loop
        ++databuffer;    // store next byte
        tbytes++;
    }// end byte for loop

    timer_stop();
    return J1850_RETURN_CODE_LENGTH_ERROR;
}

#endif  //#if (SUPPORT_COMM_VPW)
