/**
  **************** (C) COPYRIGHT 2011 SCT Performance, LLC *********************
  * File Name          : usb_cmds.h
  * Device / System    : SCT products using the "CommonCode" platform
  * Author             : Quyen Leba
  *
  * Version            : 1 
  * Date               : 05/02/2011
  * Description        : 
  *                    : 
  *
  *
  * History            : 05/02/2011 M. Davis
  *                    :   Add SCT Copyright header,
  *
  ******************************************************************************
  */

/******************** (C) COPYRIGHT 2009 SCT LLC *************************
* File Name          : usb_cmds.h
* Device / System    : TSX - (Touch Screen eXtreme) Tuner System... Vehicle Interface Module
* Author             : P. Downs, Q.Leba, M. Davis
*
* Version            :
* Date               : 08/04/2009
* Description        : TSX Vehicle Interface Module (dongle) application - 
*                      header file for usb_cmds.c - functions for processing USB commands 
*                      from a host computer
*
* Rev History, most recent first
* $History: /CommonCode/Mico/board/TSXD/usb_cmds.h $
*  
*  ****************** Version 1 ****************** 
*  User: qleba   Date: 2011-05-17   Time: 23:01:23-04:00 
*  Updated in: /CommonCode/Mico/board/TSXD 
*  
*  ****************** Version 1 ****************** 
*  User: qleba   Date: 2010-11-01   Time: 12:21:06-04:00 
*  Updated in: /mico/board/TSXD 
*  
*  ****************** Version 7 ****************** 
*  User: qleba   Date: 2010-02-25   Time: 15:39:10-05:00 
*  Updated in: /TSX Vehicle Interface/Firmware/TSXVehicleInterface_PDlatest/include 
*  add USB commands to support NAND FTL read/write, etc; allow production file 
*  loading fast without bad block problems. 
*  
*  ****************** Version 6 ****************** 
*  User: mdavis   Date: 2009-10-26   Time: 16:02:34-04:00 
*  Updated in: /TSX Vehicle Interface/Firmware/TSXVehicleInterface_PDlatest/include 
*  Dev. build 1.3.101.16... 
*  Implement USB command to temporarily enable serial-debug I/O on release builds.
*  Clean up some white-space issues.
*  
*  ****************** Version 5 ****************** 
*  User: mdavis   Date: 2009-10-16   Time: 11:41:26-04:00 
*  Updated in: /TSX Vehicle Interface/Firmware/TSXVehicleInterface_PDlatest/include 
*  Dev build 1.3.101.8 (release candidate 1.3.102.0 RC1)... 
*  Changes to support production-serialization... revise USB_CMDS_Start() prototype
*  to match changes in usb_cmds.c
*
*  ****************** Version 4 ****************** 
*  User: mdavis   Date: 2009-10-01   Time: 21:29:32-04:00 
*  Updated in: /TSX Vehicle Interface/Firmware/TSXVehicleInterface_PDlatest/include 
*  Development release 1.3.101.3... Add standardized copyright header. 
*  
*/

#ifndef __USB_CMDS_H
#define __USB_CMDS_H

#include "usb_bot.h"

/* Exported macro ------------------------------------------------------------*/
#define USB_ENC_BLOCK_LENGTH        64

//enum
//{
//    USB_CMD_PASSTHROUGH             = 49u,
//    USB_CMD_CANDETECTED             = 52u,
//    USB_CMD_SCPDETECTED             = 53u,
//    USB_CMD_VPWDETECTED             = 54u,
//
//    USB_CMD_NAND_ADDRESS            = 101,
//    USB_CMD_NAND_READ               = 102,
//    USB_CMD_NAND_WRITE              = 103,
//    USB_CMD_FTL_INIT                = 105,
//    USB_CMD_FTL_READ                = 106,
//    USB_CMD_FTL_WRITE               = 107,
//    USB_CMD_REINIT_FILESYSTEM       = 108,
//    USB_CMD_FTL_CHUNK_INDEX         = 109,
//    
//    USB_CMD_SET_ADDRESS             = 64u,
//    USB_CMD_WRITE_MEM               = 65u,
//    USB_CMD_READ_MEM                = 66u,
//    USB_CMD_SET_BULK_TR_LENGTH      = 67u,
//    USB_CMD_WRITE_MEM_BULK          = 68u,
//    USB_CMD_READ_MEM_BULK           = 69u,
//    USB_CMD_WRITE_FILE		    = 70u,
//    USB_CMD_WRITE_FILE_DATA         = 71u,
//    USB_CMD_SEND_FILE               = 72u,
//    USB_CMD_SEND_FILE_DATA          = 73u,
//    USB_CMD_SEND_IMAGE              = 74u,
//    USB_CMD_SEND_IMAGE_DATA         = 75u,
//    USB_CMD_WRITE_IMAGE             = 76u,
//    USB_CMD_WRITE_IMAGE_DATA        = 77u,
//    USB_CMD_GET_LISTING             = 78u,
//    USB_CMD_GET_LISTING_NEXT        = 79u,
//    USB_CMD_GET_FREE_SPACE          = 80u,
//    USB_CMD_FORMAT_MMC              = 81u,
//    USB_CMD_DELETE_FILE             = 82u,
//    USB_CMD_START_BOOTLOADER        = 83u,
//    USB_CMD_SEND_SETTINGS           = 84u,
//    USB_CMD_WRITE_SETTINGS          = 85u,
//    USB_CMD_KEY_REQUEST             = 86u,
//    USB_CMD_KEY_AUTHENTICATE        = 87u,
//    USB_CMD_INIT_SETTINGS           = 88u,
//    USB_CMD_ENABLE_LOCK             = 89u,
//    USB_CMD_ENABLE_DEBUGP           = 90u,
//    USB_CMD_DISABLE_DEBUGP          = 91u,
//    USB_CMD_LIVEWIRE_FW_VERSION     = 92u,
//    USB_CMD_LIVEWIRE_TUNE_VERSION   = 93u,
//    USB_CMD_CLEAR_SERIAL            = 94u,
//
//    USB_CMD_GET_VERSION             = 110u,     //send 16chars. ex: BOOT2.01 (bootload) or "1.1.123" (app)
//    USB_CMD_GET_SERIAL_OTP          = 111u,
//    USB_CMD_SET_SERIAL_OTP          = 112u,
//    USB_CMD_GET_SERIAL_SETTINGS     = 113u,
//    USB_CMD_SET_SERIAL_SETTINGS     = 114u,
//    USB_CMD_14229DETECTED           = 115u,
//    USB_CMD_14229DISENGAGED         = 116u,
//    
//    USB_CMD_ENC_GET_SETTINGS        = 130u,
//    USB_CMD_ENC_SET_SETTINGS        = 131u,
//    USB_CMD_ENC_RESET_SETTINGS      = 132u,
//    USB_CMD_ENC_INIT_SETTINGS       = 133u,
//    USB_CMD_FORCE_STOCK             = 134u,
//    USB_CMD_ENABLE_SER_DBG          = 135u,
//};
/* Exported functions ------------------------------------------------------- */
u8 USB_CMDS_Start (bool productionCmd);
void UCMD_Select();
void USB_CMDS_StageRequest(u8 usb_request, u16 usb_value, u16 usb_index,
                           u16 usb_length);
void USB_CMDS_StageProcess(void);
bool USB_IsMassStorageEnable(void);
void USB_DisableMassStorage(void);
void USB_EnableMassStorage(void);
void USB_CMDS_JumpToBootloaderRequest(void);
u8 USB_CMDS_JumpToBootloaderNotification(void);
void USB_CMDS_JumpToBootloader(void);

#define USB_Send(b,l)       USB_EP2_IN_Bulk(b,l)
#define USB_Receive(b,l)    USB_EP1_OUT_Bulk(b,l)

#endif
