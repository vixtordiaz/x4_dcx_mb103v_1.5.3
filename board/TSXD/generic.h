/**
  **************** (C) COPYRIGHT 2011 SCT Performance, LLC *********************
  * File Name          : generic.h
  * Device / System    : SCT products using the "CommonCode" platform
  * Author             : Quyen Leba
  *
  * Version            : 1 
  * Date               : 05/02/2011
  * Description        : 
  *                    : 
  *
  *
  * History            : 05/02/2011 M. Davis
  *                    :   Add SCT Copyright header,
  *
  ******************************************************************************
  */

#ifndef __GENERIC_H
#define __GENERIC_H

#include <stdlib.h>

#define MAX_BUFFER_LENGTH                   4096
#define MAX_BUFFER_MALLOC_LENGTH            MAX_BUFFER_LENGTH

//#define     __malloc(s)     malloc(s)
//#define     __free(p)       free(p)

void* __malloc(size_t s);
void __free(void* p);

#endif	//__GENERIC_H