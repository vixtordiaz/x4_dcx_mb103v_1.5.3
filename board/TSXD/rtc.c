/**
  **************** (C) COPYRIGHT 2011 SCT Performance, LLC *********************
  * File Name          : rtc.c
  * Device / System    : SCT products using the "CommonCode" platform
  * Author             : Quyen Leba
  *
  * Version            : 1 
  * Date               : 05/02/2011
  * Description        : 
  *                    : 
  *
  *
  * History            : 05/02/2011 M. Davis
  *                    :   Add SCT Copyright header,
  *
  ******************************************************************************
  */

#include <stm32f10x_rcc.h>
#include <stm32f10x_pwr.h>
#include <stm32f10x_bkp.h>
#include <stm32f10x_rtc.h>
#include <board/rtc.h>

#define RTC_PRESCALE_VAULE    62
#define RTC_CLK_SOURCE_VALUE  62500
#define RTC_TICK_VALUE        1/(RTC_CLK_SOURCE_VALUE/(RTC_PRESCALE_VAULE+1))

//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
void rtc_init()
{
    /* Enable PWR and BKP clocks */
    RCC_APB1PeriphClockCmd(RCC_APB1Periph_PWR | RCC_APB1Periph_BKP, ENABLE);
    
    /* Allow access to BKP Domain */
    PWR_BackupAccessCmd(ENABLE);
    
    /* Reset Backup Domain */
    BKP_DeInit();
    
    /* Enable LSE */
    //RCC_LSEConfig(RCC_LSE_ON);
    /* Wait till LSE is ready */
    //while (RCC_GetFlagStatus(RCC_FLAG_LSERDY) == RESET)
    //{}
    
    //RCC_LSICmd(ENABLE);
    //while (RCC_GetFlagStatus(RCC_FLAG_LSIRDY) == RESET)
    //{}
    
    /* Select HSE_Div128 as RTC Clock Source */
    RCC_RTCCLKConfig(RCC_RTCCLKSource_HSE_Div128);
    //RCC_RTCCLKConfig(RCC_RTCCLKSource_LSI);
    
    /* Enable RTC Clock */
    RCC_RTCCLKCmd(ENABLE);
    
    /* Wait for RTC registers synchronization */
    RTC_WaitForSynchro();
    
    /* Wait until last write operation on RTC registers has finished */
    RTC_WaitForLastTask();
    
    /* Set RTC prescaler: set RTC period to 1sec */
    //RTC_SetPrescaler(32767000); /* RTC period = RTCCLK/RTC_PR = (32.768 KHz)/(32767000+1) for 1ms tick*/
    RTC_SetPrescaler(RTC_PRESCALE_VAULE); /* RTC period = RTCCLK/RTC_PR = (62.5 KHz)/(62+1) for 1.008ms tick*/
    //RTC_SetPrescaler(39);
    
    /* Wait until last write operation on RTC registers has finished */
    RTC_WaitForLastTask();
}

//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
void rtc_reset()
{
    /* Wait until last write operation on RTC registers has finished */
    RTC_WaitForLastTask();
    RTC_SetCounter(0x0);
    RTC_WaitForLastTask();
}

//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
u32 rtc_getvalue()
{
    return RTC_GetCounter();
}
