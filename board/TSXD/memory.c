/**
  **************** (C) COPYRIGHT 2011 SCT Performance, LLC *********************
  * File Name          : memory.c
  * Device / System    : SCT products using the "CommonCode" platform
  * Author             : Quyen Leba
  *
  * Version            : 1 
  * Date               : 05/02/2011
  * Description        : 
  *                    : 
  *
  *
  * History            : 05/02/2011 M. Davis
  *                    :   Add SCT Copyright header,
  *
  ******************************************************************************
  */

/******************** (C) COPYRIGHT 2008, 2009 SCT LLC *************************
* File Name          : memory.c
* Device / System    : TSX - (Touch Screen eXtreme) Tuner System... Vehicle Interface Module
* Author             : (C) COPYRIGHT 2008 STMicroelectronics - used with permission... all
*                      subsequent modifications copyright SCT LLC
*
* Version            :
* Date               : 09/22/2008 STMicroelectronics
* Description        : TSX Vehicle Interface Module (dongle) application - 
*                      Memory management layer (MicroSD card)
*
* Rev History, most recent first
* $History: /CommonCode/Mico/board/TSXD/memory.c $
*  
*  ****************** Version 1 ****************** 
*  User: qleba   Date: 2011-05-17   Time: 23:01:23-04:00 
*  Updated in: /CommonCode/Mico/board/TSXD 
*  
*  ****************** Version 1 ****************** 
*  User: qleba   Date: 2010-11-01   Time: 12:21:05-04:00 
*  Updated in: /mico/board/TSXD 
*  
*  ****************** Version 4 ****************** 
*  User: mdavis   Date: 2009-10-13   Time: 17:14:08-04:00 
*  Updated in: /TSX Vehicle Interface/Firmware/TSXVehicleInterface_PDlatest/app 
*  Development build 1.3.101.7... 
*  Clean up a few warnings
*  
*  ****************** Version 3 ****************** 
*  User: mdavis   Date: 2009-09-30   Time: 23:17:35-04:00 
*  Updated in: /TSX Vehicle Interface/Firmware/TSXVehicleInterface_PDlatest/app 
*  development build 1.3.101.2... documentary changes only - 
*  Add or update copyright header to latest standard. 
*  
*/

/******************** (C) COPYRIGHT 2008 STMicroelectronics ********************
* File Name          : memory.c
* Author             : MCD Application Team
* Version            : V1.1.2
* Date               : 09/22/2008
* Description        : Memory management layer
********************************************************************************
* THE PRESENT FIRMWARE WHICH IS FOR GUIDANCE ONLY AIMS AT PROVIDING CUSTOMERS
* WITH CODING INFORMATION REGARDING THEIR PRODUCTS IN ORDER FOR THEM TO SAVE TIME.
* AS A RESULT, STMICROELECTRONICS SHALL NOT BE HELD LIABLE FOR ANY DIRECT,
* INDIRECT OR CONSEQUENTIAL DAMAGES WITH RESPECT TO ANY CLAIMS ARISING FROM THE
* CONTENT OF SUCH FIRMWARE AND/OR THE USE MADE BY CUSTOMERS OF THE CODING
* INFORMATION CONTAINED HEREIN IN CONNECTION WITH THEIR PRODUCTS.
*******************************************************************************/

/* Includes ------------------------------------------------------------------*/

#include "memory.h"
#include "usb_scsi.h"
#include "usb_bot.h"
#include <USBLib/include/usb_regs.h>
#include <USBLib/include/usb_mem.h>
#include "usb_conf.h"
#include "usb_hw_config.h"
#include "mass_mal.h"

/* Private typedef -----------------------------------------------------------*/
/* Private define ------------------------------------------------------------*/
/* Private macro -------------------------------------------------------------*/
/* Private variables ---------------------------------------------------------*/
vu32 Block_Read_count = 0;
vu32 Block_offset;
vu32 Counter = 0;
u32  Idx;
u8 Data_Buffer[BULK_MAX_PACKET_SIZE *8]; /* 512 bytes*/
u8 TransferState = TXFR_IDLE;
/* Extern variables ----------------------------------------------------------*/
extern u8 Bulk_Data_Buff[BULK_MAX_PACKET_SIZE];  /* data buffer*/
extern u16 Data_Len;
extern u8 Bot_State;
extern Bulk_Only_CBW CBW;
extern Bulk_Only_CSW CSW;
extern u32 Mass_Memory_Size[2];
extern u32 Mass_Block_Size[2];

/* Private function prototypes -----------------------------------------------*/
/* Extern function prototypes ------------------------------------------------*/
/* Private functions ---------------------------------------------------------*/

/*******************************************************************************
* Function Name  : Read_Memory
* Description    : Handle the Read operation from the microSD card.
* Input          : None.
* Output         : None.
* Return         : None.
*******************************************************************************/
void Read_Memory(u8 lun, u32 Memory_Offset, u32 Transfer_Length)
{
  static u32 Offset, Length;

  if (TransferState == TXFR_IDLE )
  {
    Offset = Memory_Offset * Mass_Block_Size[lun];
    Length = Transfer_Length * Mass_Block_Size[lun];
    TransferState = TXFR_ONGOING;
  }

  if (TransferState == TXFR_ONGOING )
  {
    if (!Block_Read_count)
    {
      MAL_Read(lun ,
               Offset ,
               Data_Buffer,
               Mass_Block_Size[lun]);

      UserToPMABufferCopy(Data_Buffer, ENDP1_TXADDR, BULK_MAX_PACKET_SIZE);
      Block_Read_count = Mass_Block_Size[lun] - BULK_MAX_PACKET_SIZE;
      Block_offset = BULK_MAX_PACKET_SIZE;
    }
    else
    {
      UserToPMABufferCopy(Data_Buffer + Block_offset, ENDP1_TXADDR, BULK_MAX_PACKET_SIZE);
      Block_Read_count -= BULK_MAX_PACKET_SIZE;
      Block_offset += BULK_MAX_PACKET_SIZE;
    }

    SetEPTxCount(ENDP1, BULK_MAX_PACKET_SIZE);
    SetEPTxStatus(ENDP1, EP_TX_VALID);
    Offset += BULK_MAX_PACKET_SIZE;
    Length -= BULK_MAX_PACKET_SIZE;

    CSW.dDataResidue -= BULK_MAX_PACKET_SIZE;
    //Led_RW_ON();
  }
  if (Length == 0)
  {
    Block_Read_count = 0;
    Block_offset = 0;
    Offset = 0;
    Bot_State = BOT_DATA_IN_LAST;
    TransferState = TXFR_IDLE;
    //Led_RW_OFF();
  }
}

/*******************************************************************************
* Function Name  : Write_Memory
* Description    : Handle the Write operation to the microSD card.
* Input          : None.
* Output         : None.
* Return         : None.
*******************************************************************************/
void Write_Memory(u8 lun, u32 Memory_Offset, u32 Transfer_Length)
{
  static u32 W_Offset, W_Length, W_OffsetAdj;
//  u32 SectorSaveCount = 2048;//4 * Mass_Block_Size[lun]; // 2048 block size;

  u32 temp =  Counter + 64;

  if (TransferState == TXFR_IDLE )
  {          
    W_Offset = Memory_Offset * Mass_Block_Size[lun];
    W_Length = Transfer_Length * Mass_Block_Size[lun];
    TransferState = TXFR_ONGOING;
  }

  if (TransferState == TXFR_ONGOING )
  {
    for (Idx = 0 ; Counter < temp; Counter++)
    {
      *(Data_Buffer + Counter) = Bulk_Data_Buff[Idx++];
    }

    W_Offset += Data_Len;
    W_Length -= Data_Len;
    
    //if((W_Length % 512) == 0)
    //{
    //    SectorSaveCount = 0;        
    //}
    
    //if(W_Length<2048)
    //{
    //    SectorSaveCount = Transfer_Length * Mass_Block_Size[lun]; // Mutiples of 512 block size
    //}

    //if (!(W_Length % Mass_Block_Size[lun]))
    if (!(W_Length % 512/*SectorSaveCount*/))
    {
      W_OffsetAdj = W_Offset - Counter;
      MAL_Write(lun, W_OffsetAdj, Data_Buffer, Counter); //SectorSaveCount Mass_Block_Size[lun] SectorSaveCount Mass_Block_Size[lun]
      Counter = 0;
    }

    CSW.dDataResidue -= Data_Len;
    SetEPRxStatus(ENDP2, EP_RX_VALID); /* enable the next transaction*/

    //Led_RW_ON();
  }

  if ((W_Length == 0) || (Bot_State == BOT_CSW_Send))
  {
    Counter = 0;
    Set_CSW (CSW_CMD_PASSED, SEND_CSW_ENABLE);
    TransferState = TXFR_IDLE;
    //Led_RW_OFF();
  }
}

/******************* (C) COPYRIGHT 2008 STMicroelectronics *****END OF FILE****/

