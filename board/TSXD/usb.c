/**
  **************** (C) COPYRIGHT 2011 SCT Performance, LLC *********************
  * File Name          : usb.c
  * Device / System    : SCT products using the "CommonCode" platform
  * Author             : Quyen Leba
  *
  * Version            : 1 
  * Date               : 05/02/2011
  * Description        : 
  *                    : 
  *
  *
  * History            : 05/02/2011 M. Davis
  *                    :   Add SCT Copyright header,
  *
  ******************************************************************************
  */

#include <string.h>
#include <USBLib/include/usb_lib.h>
#include <board/delays.h>
#include <board/rtc.h>
#include <board/usb.h>
#include "usb_hw_config.h"
#include "usb_istr.h"
#include "usb_pwr.h"
#include "usb_cmds.h"
#include <common/statuscode.h>


extern ONE_DESCRIPTOR Device_Descriptor;
extern ONE_DESCRIPTOR Config_Descriptor;
extern ONE_DESCRIPTOR String_Descriptor[5];

extern const DEVICE_PROP UCMD_Device_Property;
extern const USER_STANDARD_REQUESTS UCMD_User_Standard_Requests;
extern const DEVICE UCMD_Device_Table;
extern const ONE_DESCRIPTOR UCMD_Device_Descriptor;
extern const ONE_DESCRIPTOR UCMD_Config_Descriptor;
extern const ONE_DESCRIPTOR UCMD_String_Descriptor[5];

/*******************************************************************************
* Function Name  : USB_CMDS_Start
* Description    : Starts the usb cmds.
* Input          : bool productionCmd - indicate if this is a production 
*                  serialization operation - if TRUE, LED indicates serialization
*                  status, and function doesn't look for a serial-debug break.
* Output         : Affects device configuration and behavior, depending on command
* Return         : status
*******************************************************************************/
u8 usb_init()
{
    u8 input = 0;
    u8 TO = 2;
    u32 lastTime, RTC_now;
    bool LED_on = FALSE;
    bool serializationDone;
    bool uartBreak = FALSE;
    int flashCount = 3;
    
    PowerOff();
    
    USB_Cable_Config(DISABLE);
    delays(100,'m');
        
    Set_USBClock();
    UCMD_Select();
    
//    setup_pkg->packet.request = 0xFF;
    
    //Initialize USB structure properties
    memcpy((void*)&Device_Property,(void*)&UCMD_Device_Property,
           sizeof(DEVICE_PROP));
    memcpy((void*)&User_Standard_Requests,(void*)&UCMD_User_Standard_Requests,
           sizeof(USER_STANDARD_REQUESTS));
    memcpy((void*)&Device_Table,(void*)&UCMD_Device_Table,sizeof(DEVICE));
    memcpy((void*)&Device_Descriptor,(void*)&UCMD_Device_Descriptor,
           sizeof(ONE_DESCRIPTOR));
    memcpy((void*)&Config_Descriptor,(void*)&UCMD_Config_Descriptor,
           sizeof(ONE_DESCRIPTOR));
    memcpy((void*)String_Descriptor,(void*)&UCMD_String_Descriptor,
           5*sizeof(ONE_DESCRIPTOR));
    
    USB_Init();
    
    serializationDone = FALSE;

    while(TO)
    {
        if(bDeviceState != UNCONNECTED)
        {
            lastTime = RTC_now = rtc_getvalue();

//            while(!usartDebug_InputNoWait((void*)&input))
            while(!uartBreak)
            {
                if (USB_IsMassStorageEnable())
                {
                    TO = 0;
                    break;
                }
                else if (USB_CMDS_JumpToBootloaderNotification() == TRUE)
                {
                    USB_CMDS_JumpToBootloader();
                }
                
                // We're waiting for production serialization, or performing
                // USB commands hosted by a SCT desktop application...
                // continuously flash the LED in yellow 
                else   
                {
                    // if production mode, update serializationDone
//                    if (productionCmd && !serializationDone)
//                    {    
////                        serializationDone = SettingsUpdated;
////                        if (serializationDone)
////                        {
////                            // Set the LED to constant yellow
////                            LED_Set(YELLOW);
////                            LED_on = TRUE;
////                        }
//                    }
                    
                    RTC_now = rtc_getvalue();
                    
                    if (RTC_now >= lastTime + 200)
                    {
                        if (!LED_on)
                        {
                            // if production serialization, drop every fourth flash   
                            // to distinguish from normal USB command mode
//                            if(!productionCmd || (productionCmd && flashCount)) 
//                                LED_Set(YELLOW);
 
                            flashCount ++;
                    
                            if (flashCount == 4)
                                flashCount = 0;
                            // always say we turned the LED on
                            LED_on = TRUE;
                        }
                        
                        else
                        {
//                            if (!productionCmd || (productionCmd && !serializationDone))
//                            {
//    //                            LED_Set(OFF);
//    //                            LED_on = FALSE;
//                            }
                        }
                        
                    
                        lastTime = RTC_now;
        
                    }
                }
                
                // If we've completed production serialization, or this is 
                // non-production entry into USB-command on a release build,
                // and we've enabled serial debug, simulate a break character
//                if((productionCmd && serializationDone) || 
//                   (!productionCmd && FirmwareVersion.s.Build == 0 && 
//                    usartDebug_getMode() == debugUART_RxTx))
//                {  
//                    Delay(2000,ms);
//                    uartBreak = TRUE;
//                }
//                else                
//                    // Otherwise, a character on the serial-debug port will do the job
//                    uartBreak = (bool)usartDebug_InputNoWait((void*)&input);

            } // end - while(!uartBreak)

            // if we left the while(!uartBreak) loop, break out of while(TO)
            break;
            
        } // end - if(bDeviceState != UNCONNECTED)

        // look for two occurrances of bDeviceState == UNCONNECTED
        TO--;
        
        //TODO: add mechanism  to update bDeviceState... disconnect not being detected

        delays(500,'m'); // Wait half second
        
        
    } // end - while(TO)
    
    // turn off USB power 
    PowerOff();

    if(uartBreak)
    {
//        LED_Set(OFF);
        // LED_on = FALSE;
//        usartDebug_Printf("\n\rUCMD... break");
        return S_USERABORT;   
    }
      
    else
    {
//        usartDebug_Printf("\n\rWait for UCMD... timed-out");
    }

    return S_TIMEOUT;   
}