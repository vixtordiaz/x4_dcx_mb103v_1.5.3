/**
  **************** (C) COPYRIGHT 2011 SCT Performance, LLC *********************
  * File Name          : systick.c
  * Device / System    : SCT products using the "CommonCode" platform
  * Author             : Quyen Leba
  *
  * Version            : 1 
  * Date               : 05/02/2011
  * Description        : 
  *                    : 
  *
  *
  * History            : 05/02/2011 M. Davis
  *                    :   Add SCT Copyright header,
  *
  ******************************************************************************
  */

#include <stm32f10x_systick.h>
#include "systick.h"

static vu32 TimingDelay = 0;

//------------------------------------------------------------------------------
// Function Name  : systick_init
// Description    : Configure a SysTick Base time to 1 us.
// Input          : None
// Output         : None
// Return         : None
//------------------------------------------------------------------------------
void systick_init()
{
    /* Configure HCLK clock as SysTick clock source */
    SysTick_CLKSourceConfig(SysTick_CLKSource_HCLK);
    
    /* SysTick interrupt each 100 Hz with HCLK equal to 72MHz */
    SysTick_SetReload(72000);
    
    /* Enable the SysTick Interrupt */
    SysTick_ITConfig(ENABLE);
}

//------------------------------------------------------------------------------
// Function Name  : systick_decrementTimingDelay
// Description    : Decrements the TimingDelay variable.
// Input          : None
// Output         : TimingDelay
// Return         : None
//------------------------------------------------------------------------------
void systick_decrementTimingDelay(void)
{
    if (TimingDelay != 0x00)
    {
        TimingDelay--;
    }
}

//------------------------------------------------------------------------------
// Function Name  : systick_setTimingDelay
// Description    : Decrements the TimingDelay variable.
// Input          : None
// Output         : TimingDelay
// Return         : None
//------------------------------------------------------------------------------
void systick_setTimingDelay(vu32 timeout)
{
    TimingDelay = timeout;
    /* Enable the SysTick Counter */
    SysTick_CounterCmd(SysTick_Counter_Enable);
}

//------------------------------------------------------------------------------
// Function Name  : systick_expireTimingDelay
// Description    : wait until TimingDelay expired
// Input          : None
// Output         : TimingDelay
// Return         : None
//------------------------------------------------------------------------------
void systick_expireTimingDelay()
{
    while(TimingDelay);
}
