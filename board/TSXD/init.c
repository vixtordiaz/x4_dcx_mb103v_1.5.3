/**
  **************** (C) COPYRIGHT 2011 SCT Performance, LLC *********************
  * File Name          : init.c
  * Device / System    : SCT products using the "CommonCode" platform
  * Author             : Quyen Leba
  *
  * Version            : 1 
  * Date               : 05/02/2011
  * Description        : 
  *                    : 
  *
  *
  * History            : 05/02/2011 M. Davis
  *                    :   Add SCT Copyright header,
  *
  ******************************************************************************
  */

#include <board/genplatform.h>
#include <board/clock.h>
#include <board/interrupt.h>
#include <board/peripherals.h>
#include <board/flash.h>
#include <common/cmdif.h>
#include <common/crypto_blowfish.h>
#include <common/statuscode.h>
#include <common/settings.h>
#include "system.h"
#include "led.h"
#include "systick.h"
#include "bluetooth.h"
#include "init.h"

extern u8 SbLinkDataStatus;

//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
void init()
{
    u8  status;
    
    clock_init();
    USB_Disconnect_Config();
    //TODOQ: loadsettings
    settings_load();
    interrupt_init();
    systick_init();
    peripherals_init();
    fs_init();
	cmdif_init();
    crypto_blowfish_init();
    
    if (flash_isoldstructure_settings())
    {
        status = flash_convert_to_mico();
    }
    
    //bluetooth_init(BT_115200_BAUD);
    //bluetooth_init(BT_230400_BAUD);   //TODOQ: check its status
    bluetooth_init(BT_460800_BAUD);
    if (bluetooth_checkconnection(SB_PORT_1) == S_SUCCESS)
    {
        SbLinkDataStatus = ESTABLISHED;
        led_setState(GREEN);
    }
    
    /*  //TODO:
    Version_DeviceCheck();
#if (SUPPORT_STRATEGYTUNE)
    //Version_TuneCheck();
#endif
    //Version_FirmwareCheck();
    */
}
