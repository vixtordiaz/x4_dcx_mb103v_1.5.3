/**
  **************** (C) COPYRIGHT 2011 SCT Performance, LLC *********************
  * File Name          : power.c
  * Device / System    : SCT products using the "CommonCode" platform
  * Author             : Quyen Leba
  *
  * Version            : 1 
  * Date               : 05/02/2011
  * Description        : 
  *                    : 
  *
  *
  * History            : 05/02/2011 M. Davis
  *                    :   Add SCT Copyright header,
  *
  ******************************************************************************
  */

#include <stm32f10x_rcc.h>
#include <stm32f10x_gpio.h>
#include <stm32f10x_tim.h>
#include <stm32f10x_type.h>
#include <common/statuscode.h>
#include <board/power.h>
#include <board/delays.h>
#include "adc.h"

void vpp_ctrl_disable();
int vpp_ctrl_enable(float VoltageDesired, VppState VPPTypeEnable);

//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
void power_init()
{
    GPIO_InitTypeDef GPIO_InitStructure;

    RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOC | RCC_APB2Periph_GPIOG, ENABLE);
    
    /* Configure PC.9 for VPP_CTRL, PWM control of voltage out put for VPP*/
    GPIO_InitStructure.GPIO_Pin = GPIO_Pin_9;
    GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
    GPIO_InitStructure.GPIO_Mode = GPIO_Mode_Out_PP;
    GPIO_Init(GPIOC, &GPIO_InitStructure);
    GPIO_ResetBits(GPIOC, GPIO_Pin_9);
    
    // Configure PG.8:VPP_SCI_TX_6, PG.9:VPP_SCI_TX_9, PG.10:VPP_SCI_TX_12, 
    // PG.11:VPP_SCI_TX_14 to ENABLE/DISABLE VPP on SCI pins
    // PG.12:FEPS_EN ENABLE/DISABLE FEPS
    GPIO_InitStructure.GPIO_Pin = 
        GPIO_Pin_8 | GPIO_Pin_9 | GPIO_Pin_10 | GPIO_Pin_11 | GPIO_Pin_12;
    GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
    GPIO_InitStructure.GPIO_Mode = GPIO_Mode_Out_PP;
    GPIO_Init(GPIOG, &GPIO_InitStructure);
    GPIO_ResetBits(GPIOG,
                   GPIO_Pin_8 | GPIO_Pin_9 | GPIO_Pin_10 | GPIO_Pin_11 |
                   GPIO_Pin_12);
    
    Vpp_Ctrl(Vpp_OFF);
    
    /* Configure PE.0 for 5V output and PE.1 for 12V output enables */
    GPIO_InitStructure.GPIO_Pin = GPIO_Pin_0 | GPIO_Pin_1;
    GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
    GPIO_InitStructure.GPIO_Mode = GPIO_Mode_Out_PP;
    GPIO_Init(GPIOE, &GPIO_InitStructure);
    GPIO_ResetBits(GPIOE, GPIO_Pin_0|GPIO_Pin_1);
    
    return;
}

//------------------------------------------------------------------------------
// Turn on/off and route Vpp to ODB2
// Input:   VppState state (Vpp_OFF, Vpp_FEPS, Vpp_SCI_6, Vpp_SCI_9, Vpp_SCI_12, Vpp_SCI_14)
// Engineer: Quyen Leba
// Date: Jan 28, 2008
//------------------------------------------------------------------------------
u8 Vpp_Ctrl(VppState state)
{
    switch(state)
    {
    case Vpp_OFF:
        vpp_ctrl_disable();
        break;
    case Vpp_FEPS:
        vpp_ctrl_enable(18, Vpp_FEPS);
        break;
    case Vpp_SCI_6:
    case Vpp_SCI_9:
    case Vpp_SCI_12:
    case Vpp_SCI_14:
        //TODO: for now, SCI not supported
        vpp_ctrl_disable();
        break;
    default:
        vpp_ctrl_disable();
        break;
    }
    return S_SUCCESS;
}

//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
// *** Predefined PWM CCR4_Val values *** 
#define VPP20 130 // Duty cycle value for 20V
#define VPP18 190 // Duty cycle value for 18V
int vpp_ctrl_enable(float VoltageDesired, VppState VPPTypeEnable)
{
    TIM_TimeBaseInitTypeDef  TIM_TimeBaseStructure;
    TIM_OCInitTypeDef  TIM_OCInitStructure;
    GPIO_InitTypeDef GPIO_InitStructure;
    u16 CCR4_Val = 320;
    float VoltageRead = 0;
    
    // Voltages that will need to be generated for VPP
    if(VoltageDesired == 18)
    {
        CCR4_Val = VPP18;
    }
    else if(VoltageDesired == 20)
    {
        VoltageDesired = 19.90; // Need to lower, ADC max range is 20V
        CCR4_Val = VPP20;
    }
    else
    {
        return S_FAIL; // Voltage not suppported
    }
    
    // Enable TIMER 8
    RCC_APB2PeriphClockCmd(RCC_APB2Periph_TIM8 , ENABLE);
    
    // Configure PC.9 for VPP_CTRL, PWM control of voltage out put for VPP
    // Setup TIM8 Ch4 for PWM
    GPIO_InitStructure.GPIO_Pin = GPIO_Pin_9;
    GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
    GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AF_PP;
    GPIO_Init(GPIOC, &GPIO_InitStructure);
    
    /* TIM8 Configuration ---------------------------------------------------
    Generates PWM signal on CH4(GPIOC.P9) for the TPS61170 VPP_Ctrl signal 
    TIM8CLK = 72 MHz, Prescaler = 0, TIM1 counter clock = 72 MHz
    TIM8_Period = 720
    TIM8 frequency = TIM8CLK/(TIM8_Period + 1) = 100 KHz
    TIM8->CCR4 = 360
    - TIM8 Channel4 duty cycle = TIM8->CCR4 / (TIM8_Period + 1) = 50% 
    ----------------------------------------------------------------------- */
    
    /* Time Base configuration */
    TIM_TimeBaseStructure.TIM_Prescaler = 0;
    TIM_TimeBaseStructure.TIM_CounterMode = TIM_CounterMode_Up;
    TIM_TimeBaseStructure.TIM_Period = 720;
    TIM_TimeBaseStructure.TIM_ClockDivision = 0;
    TIM_TimeBaseStructure.TIM_RepetitionCounter = 0;
    TIM_TimeBaseInit(TIM8, &TIM_TimeBaseStructure);
    
    /* Channel 4 Configuration in PWM mode */
    TIM_OCInitStructure.TIM_OCMode = TIM_OCMode_PWM1;  
    TIM_OCInitStructure.TIM_OutputState = TIM_OutputState_Enable;
    TIM_OCInitStructure.TIM_OutputNState = TIM_OutputNState_Enable;  
    TIM_OCInitStructure.TIM_Pulse = CCR4_Val;
    TIM_OCInitStructure.TIM_OCPolarity = TIM_OCPolarity_Low;
    TIM_OCInitStructure.TIM_OCNPolarity = TIM_OCNPolarity_High;
    TIM_OCInitStructure.TIM_OCIdleState = TIM_OCIdleState_Set;
    TIM_OCInitStructure.TIM_OCNIdleState = TIM_OCIdleState_Reset;
    TIM_OC4Init(TIM8, &TIM_OCInitStructure);
    
    /* TIM8 counter enable */
    TIM_Cmd(TIM8, ENABLE);
    
    /* TIM8 Main Output Enable */
    TIM_CtrlPWMOutputs(TIM8, ENABLE);
    delays(1, 'm'); // Give time to stabilize voltage
    
    while (1)
    {
        if(adc_read(ADC_VPP, &VoltageRead) == S_FAIL)
        {
            Vpp_Ctrl(Vpp_OFF);
            return S_FAIL;
        }
        
        if(VoltageRead >= VoltageDesired)
        {
            break;
        }
        else
        {
            if(VoltageRead<VoltageDesired)
            {
                if(CCR4_Val > 0)
                {
                    CCR4_Val -= 1;
                }
                else              
                {
                    Vpp_Ctrl(Vpp_OFF);
                    return S_FAIL;              
                }
            }
            TIM_OCInitStructure.TIM_Pulse = CCR4_Val;
            TIM_OC4Init(TIM8, &TIM_OCInitStructure);
            delays(1, 'm'); // Give time to stabilize voltage         
        }      
    }
    
    // Enable the voltage output
    if(VPPTypeEnable == Vpp_FEPS)
    {
        GPIO_SetBits(GPIOG, GPIO_Pin_12); // Only FEPS currenly supported
    }
    else
    {
        Vpp_Ctrl(Vpp_OFF);
        return S_FAIL;
    }
    
    return S_SUCCESS;    
}

//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
void vpp_ctrl_disable()
{
    GPIO_InitTypeDef GPIO_InitStructure;
    
    /* TIM1 counter enable */
    TIM_Cmd(TIM8, DISABLE);
    /* TIM1 Main Output Enable */
    TIM_CtrlPWMOutputs(TIM8, DISABLE);
    /* Configure PC.9 for VPP_CTRL, PWM control of voltage out put for VPP*/
    GPIO_InitStructure.GPIO_Pin = GPIO_Pin_9;
    GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
    GPIO_InitStructure.GPIO_Mode = GPIO_Mode_Out_PP;
    GPIO_Init(GPIOC, &GPIO_InitStructure);
    GPIO_ResetBits(GPIOC, GPIO_Pin_9);
    
    delays(1, 'm'); // Give time to stabilize voltage
    
    GPIO_ResetBits(GPIOG,
                   GPIO_Pin_8 | GPIO_Pin_9 | GPIO_Pin_10 | GPIO_Pin_11 |
                   GPIO_Pin_12);
}

//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
void V5VExt_Ctrl(FunctionalState State)
{
    if(State == ENABLE)
    {
        GPIO_SetBits(GPIOE, GPIO_Pin_0);
    }
    else
    {
        GPIO_ResetBits(GPIOE, GPIO_Pin_0);
    }
}

//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
void Ext12V_Config(FunctionalState State)
{
    if(State == ENABLE)
    {
        GPIO_SetBits(GPIOE, GPIO_Pin_1);
    }
    else
    {
        GPIO_ResetBits(GPIOE, GPIO_Pin_1);  
    }
}