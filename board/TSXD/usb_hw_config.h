/**
  **************** (C) COPYRIGHT 2011 SCT Performance, LLC *********************
  * File Name          : usb_hw_config.h
  * Device / System    : SCT products using the "CommonCode" platform
  * Author             : Quyen Leba
  *
  * Version            : 1 
  * Date               : 05/02/2011
  * Description        : 
  *                    : 
  *
  *
  * History            : 05/02/2011 M. Davis
  *                    :   Add SCT Copyright header,
  *
  ******************************************************************************
  */

/******************** (C) COPYRIGHT 2008, 2009 SCT LLC *************************
* File Name          : hw_config.h
* Device / System    : TSX - (Touch Screen eXtreme) Tuner System... Vehicle Interface Module
* Author             : (C) COPYRIGHT 2008 STMicroelectronics - used with permission... all
*                      subsequent modifications copyright SCT LLC
*
* Version            :
* Date               : 09/22/2008 STMicroelectronics
* Description        : TSX Vehicle Interface Module (dongle) application - 
*                      header file for hw_config.c - functions and resources for 
*                      managing hardware on the device
*
* Rev History, most recent first
* $History: /CommonCode/Mico/board/TSXD/usb_hw_config.h $
*  
*  ****************** Version 1 ****************** 
*  User: qleba   Date: 2011-05-17   Time: 23:01:23-04:00 
*  Updated in: /CommonCode/Mico/board/TSXD 
*  
*  ****************** Version 1 ****************** 
*  User: qleba   Date: 2010-11-01   Time: 12:21:06-04:00 
*  Updated in: /mico/board/TSXD 
*  
*  ****************** Version 2 ****************** 
*  User: mdavis   Date: 2009-09-30   Time: 15:32:51-04:00 
*  Updated in: /TSX Vehicle Interface/Firmware/TSXVehicleInterface_PDlatest/include 
*  development build 1.3.101.1... Add or update copyright header to latest 
*  standard. 
*
*/

/******************** (C) COPYRIGHT 2008 STMicroelectronics ********************
* File Name          : hw_config.h
* Author             : MCD Application Team
* Version            : V1.1.2
* Date               : 09/22/2008
* Description        : Hardware Configuration & Setup
********************************************************************************
* THE PRESENT FIRMWARE WHICH IS FOR GUIDANCE ONLY AIMS AT PROVIDING CUSTOMERS
* WITH CODING INFORMATION REGARDING THEIR PRODUCTS IN ORDER FOR THEM TO SAVE TIME.
* AS A RESULT, STMICROELECTRONICS SHALL NOT BE HELD LIABLE FOR ANY DIRECT,
* INDIRECT OR CONSEQUENTIAL DAMAGES WITH RESPECT TO ANY CLAIMS ARISING FROM THE
* CONTENT OF SUCH FIRMWARE AND/OR THE USE MADE BY CUSTOMERS OF THE CODING
* INFORMATION CONTAINED HEREIN IN CONNECTION WITH THEIR PRODUCTS.
*******************************************************************************/

/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __USB_HW_CONFIG_H
#define __USB_HW_CONFIG_H

/* Includes ------------------------------------------------------------------*/
#include "stm32f10x_type.h"

/* Exported types ------------------------------------------------------------*/
/* Exported constants --------------------------------------------------------*/
/* Exported macro ------------------------------------------------------------*/
/* Exported define -----------------------------------------------------------*/
#define BULK_MAX_PACKET_SIZE              0x00000040
#define USE_WAITN
#define USB_DISCONNECT                    GPIOG  
#define USB_DISCONNECT_PIN                GPIO_Pin_13 
#define RCC_APB2Periph_GPIO_DISCONNECT    RCC_APB2Periph_GPIOG 

/* Exported functions ------------------------------------------------------- */
void Set_USBClock(void);
void Enter_LowPowerMode(void);
void Leave_LowPowerMode(void);
void USB_Interrupts_Config(void);
void USB_Configured_LED(void);
void USB_NotConfigured_LED(void);
void USB_Cable_Config (FunctionalState NewState);
void USB_Disconnect_Config(void);
void Get_SerialNum(void);
void MAL_Config(void);

#endif	//__USB_HW_CONFIG_H

/******************* (C) COPYRIGHT 2008 STMicroelectronics *****END OF FILE****/
