/**
  **************** (C) COPYRIGHT 2011 SCT Performance, LLC *********************
  * File Name          : interrupt.c
  * Device / System    : SCT products using the "CommonCode" platform
  * Author             : Quyen Leba
  *
  * Version            : 1 
  * Date               : 05/02/2011
  * Description        : 
  *                    : 
  *
  *
  * History            : 05/02/2011 M. Davis
  *                    :   Add SCT Copyright header,
  *
  ******************************************************************************
  */

#include <stm32f10x_nvic.h>
#include <board/interrupt.h>

//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
void interrupt_init()
{
    interrupt_ctrl(ENABLE);
}

//------------------------------------------------------------------------------
// Function Name  : interrupt_config
// Description    : Configures the used IRQ Channels and sets their priority.
// Input          : FunctionalState intrCtrl - DISABLE, ENABLE
// Output         : None
// Return         : None
//------------------------------------------------------------------------------
void interrupt_ctrl(FunctionalState intrCtrl)
{
    NVIC_InitTypeDef NVIC_InitStructure;
    
    NVIC_DeInit();
    
    //TODOQ:
    //Set the Vector Table base address at 0x08000000 *+0x2000 for Bootloader
    NVIC_SetVectorTable(NVIC_VectTab_FLASH, 0x10000);
    
    //NVIC_SetVectorTable(NVIC_VectTab_FLASH, 0); // No bootloader addresss
    
    NVIC_PriorityGroupConfig(NVIC_PriorityGroup_1);
    
    //Configure the SysTick handler priority
    NVIC_SystemHandlerPriorityConfig(SystemHandler_SysTick, 0, 0);
    
    // Enable the TIM6 gloabal Interrupt 
    NVIC_InitStructure.NVIC_IRQChannel = TIM6_IRQChannel;
    NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 1;
    NVIC_InitStructure.NVIC_IRQChannelSubPriority = 1;
    NVIC_InitStructure.NVIC_IRQChannelCmd = intrCtrl;    
    NVIC_Init(&NVIC_InitStructure);
    
    NVIC_InitStructure.NVIC_IRQChannel = USB_LP_CAN_RX0_IRQChannel;
    NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 0;
    NVIC_InitStructure.NVIC_IRQChannelSubPriority = 1;
    NVIC_InitStructure.NVIC_IRQChannelCmd = intrCtrl;
    NVIC_Init(&NVIC_InitStructure);
    
    NVIC_InitStructure.NVIC_IRQChannel = USB_HP_CAN_TX_IRQChannel;
    NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 0;
    NVIC_InitStructure.NVIC_IRQChannelSubPriority = 0;
    NVIC_InitStructure.NVIC_IRQChannelCmd = intrCtrl;
    NVIC_Init(&NVIC_InitStructure);
    
    //Enable the USART2 Interrupt
    NVIC_InitStructure.NVIC_IRQChannel = USART2_IRQChannel;
    NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 1;
    NVIC_InitStructure.NVIC_IRQChannelSubPriority = 0;
    NVIC_InitStructure.NVIC_IRQChannelCmd = intrCtrl;
    NVIC_Init(&NVIC_InitStructure);
}