/**
  **************** (C) COPYRIGHT 2011 SCT Performance, LLC *********************
  * File Name          : flash.c
  * Device / System    : SCT products using the "CommonCode" platform
  * Author             : Quyen Leba
  *
  * Version            : 1 
  * Date               : 05/02/2011
  * Description        : 
  *                    : 
  *
  *
  * History            : 05/02/2011 M. Davis
  *                    :   Add SCT Copyright header,
  *
  ******************************************************************************
  */

#include <stm32f10x_flash.h>
#include <string.h>
#include <board/genplatform.h>
#include <board/indicator.h>
#include <common/statuscode.h>
#include <common/settings.h>
#include <fs/genfs.h>
#include <board/flash.h>

//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
#define SETTINGS_SECTOR_SIZE        0x800
#define SETTINGS_ADDR               0x0807D000
#define SETTINGS_ADDR_S0            SETTINGS_ADDR                               //0x0807D000
#define SETTINGS_ADDR_S1            SETTINGS_ADDR_S0 + SETTINGS_SECTOR_SIZE     //0x0807D800
#define SETTINGS_ADDR_S2            SETTINGS_ADDR_S1 + SETTINGS_SECTOR_SIZE     //0x0807E000
#define SETTINGS_ADDR_S3            SETTINGS_ADDR_S2 + SETTINGS_SECTOR_SIZE     //0x0807E800
#define SETTINGS_ADDR_S4            SETTINGS_ADDR_S3 + SETTINGS_SECTOR_SIZE     //0x0807F000
#define SETTINGS_ADDR_S5            SETTINGS_ADDR_S4 + SETTINGS_SECTOR_SIZE     //0x0807F800

#define OLDSETTINGS_ADDR            (SETTINGS_ADDR_S0)
//SETTINGS_ADDR_S1: unused
#define BOOTLOADER_SIGNATURE_ADDR   (SETTINGS_ADDR_S2)
#define CRITICAL_ADDR               (SETTINGS_ADDR_S3)
#define TUNE_ADDR                   (SETTINGS_ADDR_S4)
#define DATALOGGENERAL_ADDR         (SETTINGS_ADDR_S5)

#define CRITICAL_FILENAME           "crsb.scf"
#define TUNE_FILENAME               "tusb.scf"
#define DATALOGGENERAL_FILENAME     "dgsb.scf"
#define OLDSETTINGS_FILENAME        "ossb.scf"

//------------------------------------------------------------------------------
// Save bootloader type signature into BOOTLOADER_SIGNATURE_ADDR
// Inputs:  BootloaderType bootloadertype
//          u8  *signature
//          u8  signaturelength (must be multiple of 4 bytes)
// Return:  u8  status
// Engineer: Quyen Leba
//------------------------------------------------------------------------------
u8 flash_save_bootloader_signature(BootloaderType bootloadertype)
{
#define BOOTLOADERTYPE_REGULAR_SIGNATURE    0xAAAAAAAA
#define BOOTLOADERTYPE_RECOVERY_SIGNATURE   0xFFFFFFFF
#define BOOTLOADERTYPE_APP_SIGNATURE        0x00000000
#define BOOTLOADERTYPE_PRODUCTION_SIGNATURE 0xA5A5A5A5
    FLASH_Status FlashStatus;
    u32 signature;
    u32 signature_cmp;
    u8  status;
    
    switch(bootloadertype)
    {
    case BooloaderType_Regular:
        signature = BOOTLOADERTYPE_REGULAR_SIGNATURE;
        break;
    case BooloaderType_Recovery:
        signature = BOOTLOADERTYPE_RECOVERY_SIGNATURE;
        break;
    case BooloaderType_App:
        signature = BOOTLOADERTYPE_APP_SIGNATURE;
        break;
    case BooloaderType_Production:
        signature = BOOTLOADERTYPE_PRODUCTION_SIGNATURE;
        break;
    default:
        return S_INPUT;
    }
    
    status = S_SUCCESS;
    
    FLASH_Unlock();
    FlashStatus = FLASH_ErasePage(BOOTLOADER_SIGNATURE_ADDR);
    if (FlashStatus != FLASH_COMPLETE)
    {
        status = S_FAIL;
        goto flash_save_bootloader_signature_done;
    }
    
    // Erasure will inherently set the word at the beginning of the erased
    // page to 0xFFFFFFFF, which corresponds to RECOVERY_BOOT. If a different
    // boot-load type has been specified in the boot-load argument, force the
    // first word in the page to the corresponding signature.
    if(bootloadertype != BooloaderType_Recovery)
    {
        FlashStatus = FLASH_ProgramWord(BOOTLOADER_SIGNATURE_ADDR,
                                        signature);
        if (FlashStatus != FLASH_COMPLETE)
        {
            status = S_FAIL;
            goto flash_save_bootloader_signature_done;
        }
    }//if(bootloadertype != BooloaderType_Recovery)...
    
    signature_cmp = *(u32*)BOOTLOADER_SIGNATURE_ADDR;
    if(signature_cmp != signature)
    {
        status = S_BADCONTENT;
    }
    
flash_save_bootloader_signature_done:
    FLASH_Lock();
    return status;
}

//------------------------------------------------------------------------------
// Inputs:  FlashSettingsArea flashsettingsarea
//          u8 *settingsdata
//          u32 settingsdatalength (must be multiple of 4 bytes)
// Return:  u8  status
// Engineer: Quyen Leba
//------------------------------------------------------------------------------
u8 flash_save_setting(FlashSettingsArea flashsettingsarea,
                      u8 *settingsdata, u32 settingsdatalength)
{
    u32 flashaddr;
    u32 *settingsdataptr;
    u32 *tmpdataptr;
    u8  *filename;
    F_FILE *sfptr;
    u32 bytecount;
    u32 i;
    u8  status;
    FLASH_Status FlashStatus = FLASH_COMPLETE;

    //TODOQ: check settingsdatalength, must be multiple of 4 bytes

    switch(flashsettingsarea)
    {
    case CriticalArea:
        flashaddr = CRITICAL_ADDR;
        filename = CRITICAL_FILENAME;
        break;
    case TuneArea:
        flashaddr = TUNE_ADDR;
        filename = TUNE_FILENAME;
        break;
    case DatalogGeneralArea:
        flashaddr = DATALOGGENERAL_ADDR;
        filename = DATALOGGENERAL_FILENAME;
        break;
    case OldSettingsArea:
        flashaddr = OLDSETTINGS_ADDR;
        filename = OLDSETTINGS_FILENAME;
        break;
    default:
        return S_INPUT;
    }

    status = S_SUCCESS;
    //1st, make sure able to backup settings to a file before doing anything
    //to the settings in flash
    if (filename)
    {
        sfptr = __fopen((char*)filename,"wb");
        if (sfptr == NULL)
        {
            status = S_OPENFILE;
        }
        else
        {
            bytecount = f_write(settingsdata,1,settingsdatalength,sfptr);
            if (bytecount != settingsdatalength)
            {
                status = S_WRITEFILE;
            }
        }
        __fclose(sfptr);
    }
    
    if (status != S_SUCCESS)
    {
        indicator_set(SETTINGS_BACKUP_FAILED);
        return S_WRITEFILE;
    }

    settingsdataptr = (u32*)settingsdata;
    // unlock the Flash Program Erase controller 
    FLASH_Unlock();

    FlashStatus = FLASH_ErasePage(flashaddr);
    
    // write new settings to flash
    for(i=0;i<settingsdatalength/4;i++)
    {
        FlashStatus = FLASH_ProgramWord(flashaddr+(i*4), settingsdataptr[i]);
        if (FlashStatus != FLASH_COMPLETE)
        {
            status = S_FAIL;
            break;
        }
    }
    FLASH_Unlock();

    // double check new settings in flash
    tmpdataptr = (u32*)flashaddr;
    for(i=0;i<settingsdatalength/4;i++)
    {
        if (tmpdataptr[i] != settingsdataptr[i])
        {
            status = S_BADCONTENT;
            break;
        }
    }

    if (status != S_SUCCESS)
    {
        indicator_set(SETTINGS_CORRUPTED);
        return S_BADCONTENT;
    }

    return S_SUCCESS;
}

//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
u8 flash_load_setting(FlashSettingsArea flashsettingsarea,
                      u8 *settingsdata, u32 settingsdatalength)
{
    //TODOQ: old structure settings need conversion
    switch(flashsettingsarea)
    {
    case CriticalArea:
        memcpy((char*)settingsdata,(char*)CRITICAL_ADDR,settingsdatalength);
        break;
    case TuneArea:
        memcpy((char*)settingsdata,(char*)TUNE_ADDR,settingsdatalength);
        break;
    case DatalogGeneralArea:
        memcpy((char*)settingsdata,(char*)DATALOGGENERAL_ADDR,settingsdatalength);
        break;
    case OldSettingsArea:
        memcpy((char*)settingsdata,(char*)OLDSETTINGS_ADDR,settingsdatalength);
        break;
    default:
        return S_INPUT;
    }

    return S_SUCCESS;
}

//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
typedef struct
{
    u8  Sct[4];
    u8  serial_num[13];
    u32 DeviceType;
    u32 MarketType;
    u32 BluetoothBaud;
    u16 settingsVersion;
    u32 settingsCRC;
}OldSettingsInfo;

typedef struct
{
    u8  Sct[4];
    u8  eec_type;           // indicates which EEC this cal is for
    u8  comm_type;
    u8  married;            // indicates that this device is married
    u8  married_cnt;        // how many times married's been cleared
    u8  married_strategy[40];
    u8  num_of_proc;        // number of processors detected
    u8  Dwnld_Fail;	        // Processor erased, but failed download
    u8  EPATS[14];          // holds evhicle EPATS info
    u8  VIN[18];
    u8  VID[256];           // holds larder vehicle ID info block which includes the trans ID block found in CAN processors.
    u32 stockfilesize;
    u8  prev_VIN[18];
    u8  A1_on;              //analog 1 on
    u8  A2_on;              //analog 2 on
    u8  Equate1;            //equation for analog input channel 1 
    u8  Equate2;            //equation for analog input channel 2
    u8  Analog1_Des[19];    //analog input description channel 1
    u8  Analog2_Des[19];    //analog input description channel 2
    u8  A1_Units[11];       //analog units display channel 1
    u8  A2_Units[11];       //analog units display channel 2
    u8  A1_selected_num;    //Number in the selection of PIDs, need to clear this if any changes made to channel
    u8  A2_selected_num;    //Number in the selection of PIDs, need to clear this if any changes made to channel  
    u8  customtune_filename[40];        // Save the file name of the strategy file for the vehicle which the device is married to
    u8  strategytune_filename_pcm0[40]; // Save the file name of the strategy file for the vehicle which the device is married to
    u8  strategytune_filename_pcm1[40]; // Save the file name of the strategy file for the vehicle which the device is married to
    u8  option_filename_pcm0[40];       // Save the file name of the option file for the vehicle which the device is married to
    u8  option_filename_pcm1[40];       // Save the file name of the option file for the vehicle which the device is married to
    u8  strategyfilecount;              // Number of strategy/option files to apply
    u8  TuneDescription[50];
    u8  DLF_filename[20];
    u32 DLfilesize[10];     //filesize of datalogX.dat
    u8  Selected_PIDs[30];  //30 for now
    u8  datalogprecision;
    u8  record_monitor;
    u8  record_file[14];
    CustomEq          AnalogCustomEq[2];
    u8  TuneType;
    FirmwareVersion_T FirmwareVersionSettings;
    u8  TuneRevision[MAX_TUNE_VER_STRING];
    u32 settingsCRC;        // Holding location for settings CRC
}OldSettingsGeneral;

#define INFOSTARTADDR           (0x0807E000 + 0x800)
#define GENERALSTARTADDR        (INFOSTARTADDR + 0x800)

//------------------------------------------------------------------------------
// Check if old settings structure
// Return:  bool isoldsettings (TRUE: old settings found)
// Engineer: Quyen Leba
//------------------------------------------------------------------------------
bool flash_isoldstructure_settings()
{
    u32 settingsgeneraladdress;
    u8  sct[4];
    u8  i;
    
    memcpy(&sct,(u8*)INFOSTARTADDR,4);
    if (strncmp((char*)sct,"SCT0",4) != 0)
    {
        return FALSE;
    }
    
    settingsgeneraladdress = GENERALSTARTADDR+3*1024;
    for(i=0;i<3;i++)
    {
        memcpy((char*)sct,(u8*)(settingsgeneraladdress),4);
        if (strncmp((char*)sct,"OLD1",4) == 0)
        {
            return FALSE;
        }
        else if (strncmp((char*)sct,"SCT1",4) == 0)
        {
            return TRUE;
        }
        //set to next block
        settingsgeneraladdress -= 1024;
    }
    return FALSE;
}

//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
u8 flash_convert_to_mico()
{
    OldSettingsInfo oldsettingsinfo;
    OldSettingsGeneral oldsettingsgeneral;
    F_FILE *fptr;
    u8  buffer[1024];
    u32 settingsgeneraladdress;
    u8  sct[4];
    bool isvalidsettingsgeneralfound;
    u8  status;
    u8  i;
    
    //##########################################################################
    //find and load current(old) settings
    //##########################################################################
    status = S_SUCCESS;
    memcpy(&oldsettingsinfo,(u8*)INFOSTARTADDR,
           sizeof(OldSettingsInfo));
    if (strncmp((char*)oldsettingsinfo.Sct,"SCT0",4) != 0)
    {
        return S_INVALIDSETTINGS;
    }
    
    isvalidsettingsgeneralfound = FALSE;
    //start from the last block
    settingsgeneraladdress = GENERALSTARTADDR+3*1024;
    for(i=0;i<3;i++)
    {
        memcpy((char*)sct,(u8*)(settingsgeneraladdress),4);
        if (strncmp((char*)sct,"OLD1",4) == 0)
        {
            return S_INVALIDSETTINGS;
        }
        else if (strncmp((char*)sct,"SCT1",4) == 0)
        {
            isvalidsettingsgeneralfound = TRUE;
            break;
        }
        //set to next block
        settingsgeneraladdress -= 1024;
    }
    
    if (isvalidsettingsgeneralfound == FALSE)
    {
        return S_INVALIDSETTINGS;
    }
    
    memcpy(&oldsettingsgeneral,(u8*)settingsgeneraladdress,
           sizeof(OldSettingsGeneral));
    
    fptr = __fopen("pmsb.bak","wb");
    if (fptr == NULL)
    {
        return S_OPENFILE;
        //flash_convert_to_mico_done
    }
    
    //##########################################################################
    //save a backup of current(old) settings
    //##########################################################################
    memset(buffer,0xFF,sizeof(buffer));
    memcpy(buffer,(char*)&oldsettingsinfo,sizeof(OldSettingsInfo));
    f_write(buffer,1,sizeof(buffer),fptr);
    
    memset(buffer,0xFF,sizeof(buffer));
    memcpy(buffer,(char*)&oldsettingsgeneral,sizeof(OldSettingsGeneral));
    f_write(buffer,1,sizeof(buffer),fptr);
    
    __fclose(fptr);
    fptr = NULL;
    
    //##########################################################################
    //IMPORTANT: init settings
    //##########################################################################
    status = settings_setproductiondefault();
    if (status != S_SUCCESS)
    {
        goto flash_convert_to_mico_done;
    }
    
    //##########################################################################
    //convert to new settings structures
    //##########################################################################
    SETTINGS_CRITICAL(devicetype) = oldsettingsinfo.DeviceType;
    SETTINGS_CRITICAL(markettype) = oldsettingsinfo.MarketType;
    SETTINGS_CRITICAL(bluetoothbaudrate) = oldsettingsinfo.BluetoothBaud;
    memset(SETTINGS_CRITICAL(serialnumber),0xFF,
           sizeof(SETTINGS_CRITICAL(serialnumber)));
    memcpy(SETTINGS_CRITICAL(serialnumber),oldsettingsinfo.serial_num,13);
    
    SETTINGS_TUNE(veh_type) = (u16)oldsettingsgeneral.eec_type;
    if (oldsettingsgeneral.married)
    {
        SETTINGS_SetMarried();
        if (oldsettingsgeneral.Dwnld_Fail)
        {
            SETTINGS_SetDownloadFail();
        }
    }
    SETTINGS_TUNE(stockfilesize) = oldsettingsgeneral.stockfilesize;
    
    SETTINGS_TUNE(married_count) = oldsettingsgeneral.married_cnt;
    memcpy(SETTINGS_TUNE(vin),oldsettingsgeneral.VIN,17);
    SETTINGS_TUNE(vin)[17] = NULL;
    memcpy(SETTINGS_TUNE(prev_vin),oldsettingsgeneral.prev_VIN,17);
    SETTINGS_TUNE(prev_vin)[17] = NULL;
    
    memcpy(SETTINGS_TUNE(epats),oldsettingsgeneral.EPATS,14);
    memcpy(SETTINGS_TUNE(vid),oldsettingsgeneral.VID,256);
    
    SETTINGS_TUNE(flashtype) = oldsettingsgeneral.TuneType;
    if (oldsettingsgeneral.TuneType == 0x51)    //CMDIF_ACT_CUSTOM_FLASHER
    {
        memcpy(SETTINGS_TUNE(tunefilenames)[0],
               oldsettingsgeneral.customtune_filename,40);
        memcpy(SETTINGS_TUNE(tunedescriptions)[0],
               oldsettingsgeneral.TuneDescription,39);
        SETTINGS_TUNE(tunedescriptions)[0][39] = NULL;
    }
    else    //assume CMDIF_ACT_PRELOADED_FLASHER
    {
        memcpy(SETTINGS_TUNE(tunefilenames)[0],
               oldsettingsgeneral.strategytune_filename_pcm0,40);
        memcpy(SETTINGS_TUNE(tunefilenames)[1],
               oldsettingsgeneral.strategytune_filename_pcm1,40);
        memcpy(SETTINGS_TUNE(optionfilenames)[0],
               oldsettingsgeneral.option_filename_pcm0,40);
        memcpy(SETTINGS_TUNE(optionfilenames)[1],
               oldsettingsgeneral.option_filename_pcm1,40);
    }
    
    settings_update(TRUE);
    
flash_convert_to_mico_done:
    if (fptr)
    {
        __fclose(fptr);
    }
    return status;
}
