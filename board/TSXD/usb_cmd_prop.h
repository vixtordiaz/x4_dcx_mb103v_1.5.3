/**
  **************** (C) COPYRIGHT 2011 SCT Performance, LLC *********************
  * File Name          : usb_cmd_prop.h
  * Device / System    : SCT products using the "CommonCode" platform
  * Author             : Quyen Leba
  *
  * Version            : 1 
  * Date               : 05/02/2011
  * Description        : 
  *                    : 
  *
  *
  * History            : 05/02/2011 M. Davis
  *                    :   Add SCT Copyright header,
  *
  ******************************************************************************
  */

/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __usb_cmd_prop_H
#define __usb_cmd_prop_H
/* Includes ------------------------------------------------------------------*/
#include "stm32f10x_type.h"
/* Exported types ------------------------------------------------------------*/
/* Exported constants --------------------------------------------------------*/
#define Ucmd_GetConfiguration          NOP_Process
/* #define Ucmd_SetConfiguration          NOP_Process*/
#define Ucmd_GetInterface              NOP_Process
#define Ucmd_SetInterface              NOP_Process
#define Ucmd_GetStatus                 NOP_Process
#define Ucmd_ClearFeature              NOP_Process
#define Ucmd_SetEndPointFeature        NOP_Process
#define Ucmd_SetDeviceFeature          NOP_Process
/*#define Ucmd_SetDeviceAddress          NOP_Process*/

/* MASS Storage Requests*/
#define GET_MAX_LUN                0xFE
#define UCMD_STORAGE_RESET         0xFF
#define LUN_DATA_LENGTH            1

/* Exported macro ------------------------------------------------------------*/
/* Exported functions ------------------------------------------------------- */
void UCMD_init(void);
void UCMD_Reset(void);
void Ucmd_SetConfiguration(void);
void Ucmd_ClearFeature(void);
void Ucmd_SetDeviceAddress (void);
void UCMD_Status_In (void);
void UCMD_Status_Out (void);
RESULT UCMD_Data_Setup(u8);
RESULT UCMD_NoData_Setup(u8);
RESULT UCMD_Get_Interface_Setting(u8 Interface, u8 AlternateSetting);
u8 *UCMD_GetDeviceDescriptor(u16 );
u8 *UCMD_GetConfigDescriptor(u16);
u8 *UCMD_GetStringDescriptor(u16);
u8 *Ucmd_Get_Max_Lun(u16 Length);

#endif /* __usb_cmd_prop_H */
