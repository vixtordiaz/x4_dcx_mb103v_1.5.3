/**
  **************** (C) COPYRIGHT 2011 SCT Performance, LLC *********************
  * File Name          : mass_storage.c
  * Device / System    : SCT products using the "CommonCode" platform
  * Author             : Quyen Leba
  *
  * Version            : 1 
  * Date               : 05/02/2011
  * Description        : 
  *                    : 
  *
  *
  * History            : 05/02/2011 M. Davis
  *                    :   Add SCT Copyright header,
  *
  ******************************************************************************
  */

/******************** (C) COPYRIGHT 2009 SCT LLC *************************
* File Name          : mass_storage.c
* Device / System    : TSX - (Touch Screen eXtreme) Tuner System... Vehicle Interface Module
* Author             : adapted from STMicroelectronics STM32F10x demo application, with
*                      modifications by P. Downs, Q.Leba, M. Davis
*
* Version            :
* Date               : 10/23/2008
* Description        : USB mass-storage functionality, using NAND memory
*
* Rev History, most recent first
* $History: /CommonCode/Mico/board/TSXD/mass_storage.c $
*  
*  ****************** Version 1 ****************** 
*  User: qleba   Date: 2011-05-17   Time: 23:01:23-04:00 
*  Updated in: /CommonCode/Mico/board/TSXD 
*  
*  ****************** Version 1 ****************** 
*  User: qleba   Date: 2010-11-01   Time: 12:21:05-04:00 
*  Updated in: /mico/board/TSXD 
*  
*  ****************** Version 10 ****************** 
*  User: mdavis   Date: 2009-11-23   Time: 09:04:05-05:00 
*  Updated in: /TSX Vehicle Interface/Firmware/TSXVehicleInterface_PDlatest/app 
*  v1.3.102.4 revisions to production-support, for file-load (strategy folder) 
*  and serialization. Rework Mass_Storage_Start (). 
*  
*  ****************** Version 9 ****************** 
*  User: mdavis   Date: 2009-11-11   Time: 22:41:10-05:00 
*  Updated in: /TSX Vehicle Interface/Firmware/TSXVehicleInterface_PDlatest/app 
*  Change calls to revised checkForProductionLoadFile() fn. In 
*  Mass_Storage_Start (), make use of the new machinery added to usb_bot.c / 
*  Mass_Storage_Out(), to reliably detect completion of the product 
*  strategy-folder file load operation. 
*  
*  ****************** Version 8 ****************** 
*  User: mdavis   Date: 2009-10-23   Time: 11:58:06-04:00 
*  Updated in: /TSX Vehicle Interface/Firmware/TSXVehicleInterface_PDlatest/app 
*  FW ver 1.3.101.13 (development build, "TEST_UNSERIALIZED_LOCKOUT" defined 
*  in main.c )... 
*  Rework Mass_Storage_Start() to use Real Time Clock to periodically check for the
*  presence of the /strategyloaded.dat file, instead of Delay() calls.
*
*  ****************** Version 7 ****************** 
*  User: mdavis   Date: 2009-10-16   Time: 11:40:31-04:00 
*  Updated in: /TSX Vehicle Interface/Firmware/TSXVehicleInterface_PDlatest/app 
*  Dev build 1.3.101.8 (release candidate 1.3.102.0 RC1)... 
*  Changes to support production-serialization... Implement continuous flashing 
*  red LED while mass-storage operation is in effect.
*
*  ****************** Version 6 ****************** 
*  User: mdavis   Date: 2009-10-09   Time: 16:10:58-04:00 
*  Updated in: /TSX Vehicle Interface/Firmware/TSXVehicleInterface_PDlatest/app 
*  development build 1.3.101.6... 
*  Revise calls to error_handler(), to handle changes to the error-reporting system.
*
*  ****************** Version 5 ****************** 
*  User: mdavis   Date: 2009-10-01   Time: 21:30:38-04:00 
*  Updated in: /TSX Vehicle Interface/Firmware/TSXVehicleInterface_PDlatest/app 
*  Development release 1.3.101.3... Edit usartDebug_Printf message in 
*  Mass_Storage_Start(), to aid start-up debug. 
*  
*  ****************** Version 4 ****************** 
*  User: mdavis   Date: 2009-09-30   Time: 15:22:02-04:00 
*  Updated in: /TSX Vehicle Interface/Firmware/TSXVehicleInterface_PDlatest/app 
*  development build 1.3.101.1...  correct debug-message typo 
*  
*  ****************** Version 3 ****************** 
*  User: mdavis   Date: 2009-09-25   Time: 10:18:09-04:00 
*  Updated in: /TSX Vehicle Interface/Firmware/TSXVehicleInterface_PDlatest/app 
*  Merging production-programming updates with Build 52. 
*  
*/

/******************** (C) COPYRIGHT 2008 STMicroelectronics ********************
* File Name          : mass_storage.c
* Author             : MCD Application Team
* Version            : V1.1.2
* Date               : 09/22/2008
* Description        : This file provides a set of functions needed to manage the
*                      communication between the STM32F10x USB and the SD Card 
*                      and NAND Flash .
********************************************************************************
* THE PRESENT FIRMWARE WHICH IS FOR GUIDANCE ONLY AIMS AT PROVIDING CUSTOMERS
* WITH CODING INFORMATION REGARDING THEIR PRODUCTS IN ORDER FOR THEM TO SAVE TIME.
* AS A RESULT, STMICROELECTRONICS SHALL NOT BE HELD LIABLE FOR ANY DIRECT,
* INDIRECT OR CONSEQUENTIAL DAMAGES WITH RESPECT TO ANY CLAIMS ARISING FROM THE
* CONTENT OF SUCH FIRMWARE AND/OR THE USE MADE BY CUSTOMERS OF THE CODING
* INFORMATION CONTAINED HEREIN IN CONNECTION WITH THEIR PRODUCTS.
*******************************************************************************/

/* Includes ------------------------------------------------------------------*/
#include "main.h"
#include "mass_storage.h"
#include "usb_lib.h"
#include "hw_config.h"
#include "rtc.h"
#include "io_board.h"
#include "usb_lib.h"
#include "usb_istr.h"
#include "fsmc_nand.h"
#include "nand_if.h"
#include "ftl.h"
#include "mlayer.h"
#include "llayer.h"
#include "systick.h"
#include "uartdebug.h"
#include "settings.h"
#include "error_handler.h"
#include "tsx_bluetooth.h"

/* Private typedef -----------------------------------------------------------*/
/* Private define ------------------------------------------------------------*/
/* Private macro -------------------------------------------------------------*/
/* Private variables ---------------------------------------------------------*/
extern ONE_DESCRIPTOR Device_Descriptor;
extern ONE_DESCRIPTOR Config_Descriptor;
extern ONE_DESCRIPTOR String_Descriptor[5];

extern const DEVICE_PROP MASS_Device_Property;
extern const USER_STANDARD_REQUESTS MASS_User_Standard_Requests;
extern const DEVICE MASS_Device_Table;
extern const ONE_DESCRIPTOR MASS_Device_Descriptor;
extern const ONE_DESCRIPTOR MASS_Config_Descriptor;
extern const ONE_DESCRIPTOR MASS_String_Descriptor[5];
/* Private function prototypes -----------------------------------------------*/
/* Private functions ---------------------------------------------------------*/

/*******************************************************************************
* Function Name  : Mass_Storage_Init
* Description    : Initializes the peripherals used by the mass storage driver.
* Input          : None
* Output         : None
* Return         : None
*******************************************************************************/
void Mass_Storage_Init(void)
{
  /* Disable the Pull-Up*/
  USB_Cable_Config(DISABLE);
}

/*******************************************************************************
* Function Name  : Mass_Storage_Start
* Description    : Starts the mass storage demo.
* Input          : None
* Output         : None
* Return         : None
*******************************************************************************/
// NOTE! If having problems with USB, make sure CAN is not initialized!!
void Mass_Storage_Start (void)
{
    u8 input = 0;
    u8 TO = 2;
    bool strategyLoadComplete = FALSE;
    
    // release the active flash-drive volume
    f_delvolume(0);

    // TuneFileLoadStopped is a global bool, defined in globals.c it is only set
    // by usb_bot.c/Mass_Storage_Out() if a 5-second pause occurs during the 
    // production "/strategy" folder transfer process... normally signifyiing
    // completion.
    TuneFileLoadStopped = FALSE;

    RTC_Reset();  
 
    PowerOff();
    
    USB_Cable_Config(DISABLE);
    Delay(100, ms);
    
    /* Enable and GPIOD clock */
    //USB_Disconnect_Config();
    //PowerOff();
    
    Set_USBClock();
    
    Mass_Storage_Select();
    
    //Initialize USB structure properties
    memcpy((void*)&Device_Property,(void*)&MASS_Device_Property,
           sizeof(DEVICE_PROP));
    memcpy((void*)&User_Standard_Requests,(void*)&MASS_User_Standard_Requests,
           sizeof(USER_STANDARD_REQUESTS));
    memcpy((void*)&Device_Table,(void*)&MASS_Device_Table,sizeof(DEVICE));
    memcpy((void*)&Device_Descriptor,(void*)&MASS_Device_Descriptor,
           sizeof(ONE_DESCRIPTOR));
    memcpy((void*)&Config_Descriptor,(void*)&MASS_Config_Descriptor,
           sizeof(ONE_DESCRIPTOR));
    memcpy((void*)String_Descriptor,(void*)&MASS_String_Descriptor,
           5*sizeof(ONE_DESCRIPTOR));
    
    MassStoreStarted = TRUE;
    USB_Init();
  
    
    // If the production programming flag isn't set, this is a massstorage debug command
    if (!testBootSignature(PROD_BOOT_SIGNATURE))
    {
        if(bDeviceState == UNCONNECTED)
        {  
            usartDebug_Printf("\n\rUSB Configured Successfully waiting for connect...");
            usartDebug_Printf("\n\rPress any key to cancel");
        }
            
        while(TO)
        {
            if(bDeviceState != UNCONNECTED)
            {
                usartDebug_Printf("\n\rUSB Connected as Mass-Storage Device!");
                usartDebug_Printf("\n\rPress any key or disconnect USB when done..");
                               
                while(!usartDebug_InputNoWait((void*)&input))
                {
                }
                
                break;

            }
            
            if(usartDebug_InputNoWait((void*)&input))
                break;

            TO--;
            Delay(500, ms); // Wait half second
        }
        
        // close the mass-storage USB connection
        Mass_Storage_Stop();

        LED_Set(OFF);

        Delay (5000,ms);
            
        // reconnect to the flash file-system
        f_init();
        f_enterFS();  
        if (f_initvolume(0,f_ftlinit,0))
        {
            // let the error-handler indicate the strategy load complete status...
            // module processing will halt there
            strncpy(ErrHandlerMsg,"\n\rfailed on f_initvolume() after mass-storage abort",ERR_HNDLR_MAX_MSG_LEN);
            error_handler(MASS_STG_ERROR, 1, FALSE); // There's no return from error_handler()
        }

        // (else)
        usartDebug_Printf("\n\rMass storage aborted");
        
        PowerOff();
        return;
            
    } // end - if (!testBootSignature(PROD_BOOT_SIGNATURE))... serial-debug mass-storage mode

    // (else)
    // Production-programming flag is set... mass-storage mode for production strategy-folder (tune-file) load
    // allow some time for the USB state machine to move out of unconnected
    Delay(500, ms); // Wait half second

    if(bDeviceState == UNCONNECTED)
    {  
        strncpy(ErrHandlerMsg,"\n\rUSB must be connected in order to do production programming",ERR_HNDLR_MAX_MSG_LEN);
        Mass_Storage_Stop();
        
        error_handler(MASS_STG_ERROR, 2, FALSE); // There's no return from error_handler()
    }
        
    usartDebug_Printf("\n\rUSB connected for production load of tune files (\"strategy\" folder)");
            
    while(1)
    {        
        if(!TuneFileLoadIncomplete && TuneFileLoadStopped)
        {
            // close the mass-storage USB connection
            Mass_Storage_Stop();

            Delay (5000,ms);
            
            // reconnect to the flash file-system
            f_init();
            f_enterFS();  
            if (f_initvolume(0,f_ftlinit,0))
            {
                // let the error-handler indicate the strategy load complete status...
                // module processing will halt there
                strncpy(ErrHandlerMsg,"\n\rload of STRATEGY folder completed",ERR_HNDLR_MAX_MSG_LEN);
                error_handler(MASS_STG_ERROR, 3, FALSE); // There's no return from error_handler()
            }

            // check the /strategy folder, with validation of the contents against the values in
            // "/strategyloaded.dat"
            strategyLoadComplete = checkStrategyFolderLoad(TRUE);    

            if (strategyLoadComplete)
            {
                LED_Set(GREEN);
                // valid tune-version file is present... we're done with 
                // the load operation. Clear the production-programming flag
                SetBootloadBlock(APP_BOOT);
                usartDebug_Printf("\n\rStrategy folder loaded... production flag cleared");
                
                // let the error-handler indicate the strategy load complete status...
                // module processing will halt there
                strncpy(ErrHandlerMsg,"\n\rload of STRATEGY folder completed",ERR_HNDLR_MAX_MSG_LEN);
                error_handler(MASS_STG_ERROR, 4, FALSE); // There's no return from error_handler()
            }
            
            // strategy folder load ended prematurely
            // (else)
            {
#if 0
                // let the error-handler indicate the strategy load complete status...
                // module processing will halt there
                strncpy(ErrHandlerMsg,"\n\rload of STRATEGY folder incomplete",ERR_HNDLR_MAX_MSG_LEN);
                error_handler(MASS_STG_ERROR, 5, FALSE); // There's no return from error_handler()
#endif
            }           
        }
    }
}

/*******************************************************************************
* Function Name  : Mass_Storage_Start
* Description    : Starts the mass storage demo.
* Input          : None
* Output         : None
* Return         : None
*******************************************************************************/
void Mass_Storage_Stop (void)
{   
  PowerOff();
  return;  
}

#if 0 // Unused function 
/*******************************************************************************
* Function Name  : Mass_Storage_Recovery
* Description    : Erases the NAND Flash Content.
* Input          : None
* Output         : None
* Return         : None
*******************************************************************************/
void Mass_Storage_Recovery (void)
{
    int j = 0, ret = 0;
    /* Disble the JoyStick interrupts */
    IntExtOnOffConfig(DISABLE);
    
    // Erase sectors
    for(j=0;j<1024;j++)
    {
        ret = ll_erase(j);
    }
    if(ret != 0)
    {
        IntExtOnOffConfig(ENABLE);
        return; // Error on erase      
    }
        
    ret = ml_format();//hl_format();
    if(ret != 0)
    {
        IntExtOnOffConfig(ENABLE);
        return; // Error on FTL format
    }
    
    ret = ml_init();
    if(ret != 0)
    {   
        IntExtOnOffConfig(ENABLE);
        return; // Error on FTL init      
    }
    ret = ml_getmaxsector(); 
  
    ret = f_init();       
    ret = f_enterFS();  
    ret = f_initvolume(0,f_ftlinit,0);
    ret = f_chdrive(0);
    ret = f_format(0,F_FAT16_MEDIA); 
    if(ret != 0)
        return; // Error on FAT format
    
    IntExtOnOffConfig(ENABLE);
    
    return; //Success
}
#endif

/******************* (C) COPYRIGHT 2008 STMicroelectronics *****END OF FILE****/
