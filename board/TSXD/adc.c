/**
  **************** (C) COPYRIGHT 2011 SCT Performance, LLC *********************
  * File Name          : adc.c
  * Device / System    : SCT products using the "CommonCode" platform
  * Author             : Quyen Leba
  *
  * Version            : 1 
  * Date               : 05/02/2011
  * Description        : 
  *                    : 
  *
  *
  * History            : 05/02/2011 M. Davis
  *                    :   Add SCT Copyright header,
  *
  ******************************************************************************
  */

#include "stm32f10x_adc.h"
#include "stm32f10x_rcc.h"
#include "stm32f10x_gpio.h"
#include <common/statuscode.h>
#include "system.h"
#include "adc.h"

//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
void adc_init(ADC_MODE mode)
{
    ADC_InitTypeDef ADC_InitStructure;
    GPIO_InitTypeDef GPIO_InitStructure;
    
    /* Enable ADC1 and GPIOC clock */
    RCC_APB2PeriphClockCmd(RCC_APB2Periph_ADC1 | RCC_APB2Periph_GPIOC, ENABLE);
    
    /* Configure PC.04 (ADC Channel14) as analog input -------------------------*/
    GPIO_InitStructure.GPIO_Pin = GPIO_Pin_0|GPIO_Pin_1|GPIO_Pin_2|GPIO_Pin_3
                                  |GPIO_Pin_4|GPIO_Pin_5;
    GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AIN;
    GPIO_Init(GPIOC, &GPIO_InitStructure); 
    
    /* DMA1 channel1 configuration ----------------------------------------------*/
    /*
    DMA_DeInit(DMA1_Channel1);
    DMA_InitStructure.DMA_PeripheralBaseAddr = ADC1_DR_Address;
    DMA_InitStructure.DMA_MemoryBaseAddr = (u32)&ADC1ConvertedValue;
    DMA_InitStructure.DMA_DIR = DMA_DIR_PeripheralSRC;
    DMA_InitStructure.DMA_BufferSize = 1;
    DMA_InitStructure.DMA_PeripheralInc = DMA_PeripheralInc_Disable;
    DMA_InitStructure.DMA_MemoryInc = DMA_MemoryInc_Disable;
    DMA_InitStructure.DMA_PeripheralDataSize = DMA_PeripheralDataSize_HalfWord;
    DMA_InitStructure.DMA_MemoryDataSize = DMA_MemoryDataSize_HalfWord;
    DMA_InitStructure.DMA_Mode = DMA_Mode_Circular;
    DMA_InitStructure.DMA_Priority = DMA_Priority_High; 
    DMA_InitStructure.DMA_M2M = DMA_M2M_Disable;
    DMA_Init(DMA1_Channel1, &DMA_InitStructure);  
    */
    /* Enable DMA1 channel1 */
    //DMA_Cmd(DMA1_Channel1, ENABLE);
    
    // Enable the internet temperature sensor and Vref internal channel
    ADC_TempSensorVrefintCmd(ENABLE);
    
    /* ADC1 configuration ------------------------------------------------------*/
    ADC_InitStructure.ADC_Mode = ADC_Mode_Independent;
    ADC_InitStructure.ADC_ScanConvMode = DISABLE;
    ADC_InitStructure.ADC_ContinuousConvMode = DISABLE;
    ADC_InitStructure.ADC_ExternalTrigConv = ADC_ExternalTrigConv_None;
    ADC_InitStructure.ADC_DataAlign = ADC_DataAlign_Right;
    ADC_InitStructure.ADC_NbrOfChannel = 1;
    ADC_Init(ADC1, &ADC_InitStructure);
    
    /* ADC1 regular channel14 configuration */ 
    ADC_RegularChannelConfig(ADC1, ADC_Channel_12, 1, ADC_SampleTime_55Cycles5);
    
    /* Enable ADC1 */
    ADC_Cmd(ADC1, ENABLE);
    
    /* Enable ADC1 reset calibaration register */   
    ADC_ResetCalibration(ADC1);
    /* Check the end of ADC1 reset calibration register */
    while(ADC_GetResetCalibrationStatus(ADC1));
    
    /* Start ADC1 calibaration */
    ADC_StartCalibration(ADC1);
    /* Check the end of ADC1 calibration */
    while(ADC_GetCalibrationStatus(ADC1));
         
    /* Start ADC1 Software Conversion */ 
    ADC_SoftwareStartConvCmd(ADC1, ENABLE);    
    while(ADC_GetSoftwareStartConvStatus(ADC1));
    
    // Disable ADC1 to save power
    ADC_Cmd(ADC1, DISABLE);
}

//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
u8 adc_read(ADC_CHANNEL channel, float *DataValueFloat)
{
    u16 DataValue = 0;
    
    // Enable ADC1 
    ADC_Cmd(ADC1, ENABLE);

    switch(channel)
    {
      case ADC_VPP:
        ADC_RegularChannelConfig(ADC1, ADC_Channel_15, 1, ADC_SampleTime_55Cycles5);
        break;
      case ADC_VBAT:
        ADC_RegularChannelConfig(ADC1, ADC_Channel_14, 1, ADC_SampleTime_55Cycles5);
        break;
      case ADC_AIN1:
        ADC_RegularChannelConfig(ADC1, ADC_Channel_10, 1, ADC_SampleTime_55Cycles5);
        break;
      case ADC_AIN2:
        ADC_RegularChannelConfig(ADC1, ADC_Channel_11, 1, ADC_SampleTime_55Cycles5);
        break;
      case ADC_AIN3:
        ADC_RegularChannelConfig(ADC1, ADC_Channel_12, 1, ADC_SampleTime_55Cycles5);
        break;
      case ADC_AIN4:
        ADC_RegularChannelConfig(ADC1, ADC_Channel_13, 1, ADC_SampleTime_55Cycles5);
        break;
      default:
        // Disable ADC1 to save power
        ADC_Cmd(ADC1, DISABLE);
        return S_FAIL;       
    }
    
    /* Wake up ADC1 from power down */ 
    ADC_SoftwareStartConvCmd(ADC1, ENABLE);    
    while(ADC_GetSoftwareStartConvStatus(ADC1));
    
    /* Start ADC1 Software Conversion */
    ADC_SoftwareStartConvCmd(ADC1, ENABLE);    
    while(ADC_GetSoftwareStartConvStatus(ADC1));
    
    // Wait for converison to complete
    while(ADC_GetFlagStatus(ADC1, ADC_FLAG_EOC) == RESET);    
    
    DataValue = ADC_GetConversionValue(ADC1);
    
    ADC_Cmd(ADC1, DISABLE); // Disable ADC1 to save power

    if(channel == ADC_VPP)
    {
        *DataValueFloat = DataValue/204.8;  // 0V-20V
    }
    else if((channel == ADC_VBAT) || (channel == ADC_AIN3) || 
            (channel == ADC_AIN4))
    {
        *DataValueFloat = DataValue/271.26; // Constant for 0V to 15V        
    }
    else if((channel == ADC_AIN1) || (channel == ADC_AIN2))
    {
        *DataValueFloat = DataValue/812.70; // Constant for 0V to 5V
    }
    else
    {
        return S_FAIL;
    }
    
    return S_SUCCESS;
}
