/**
  **************** (C) COPYRIGHT 2011 SCT Performance, LLC *********************
  * File Name          : systick.h
  * Device / System    : SCT products using the "CommonCode" platform
  * Author             : Quyen Leba
  *
  * Version            : 1 
  * Date               : 05/02/2011
  * Description        : 
  *                    : 
  *
  *
  * History            : 05/02/2011 M. Davis
  *                    :   Add SCT Copyright header,
  *
  ******************************************************************************
  */

#ifndef __SYSTICK_H
#define __SYSTICK_H

#include <arch/gentype.h>

void systick_init();
void systick_decrementTimingDelay(void);
void systick_setTimingDelay(vu32 timeout);
void systick_expireTimingDelay();

#endif  //__TIMER_H
