/**
  **************** (C) COPYRIGHT 2011 SCT Performance, LLC *********************
  * File Name          : usb_cmd_prop.c
  * Device / System    : SCT products using the "CommonCode" platform
  * Author             : Quyen Leba
  *
  * Version            : 1 
  * Date               : 05/02/2011
  * Description        : 
  *                    : 
  *
  *
  * History            : 05/02/2011 M. Davis
  *                    :   Add SCT Copyright header,
  *
  ******************************************************************************
  */

/* Includes ------------------------------------------------------------------*/

#include "usb_cmd_desc.h"
#include "usb_pwr.h"
#include "usb_bot.h"
#include "usb_hw_config.h"
#include "memory.h"
#include "mass_mal.h"
#include "usb_cmd_prop.h"
#include <USBLib/include/usb_lib.h>
#include "usb_cmds.h"
/* Private typedef -----------------------------------------------------------*/
/* Private define ------------------------------------------------------------*/
/* Private macro -------------------------------------------------------------*/
/* Private variables ---------------------------------------------------------*/

const DEVICE UCMD_Device_Table =
{
    UCMD_EP_NUM,
    1
};

const DEVICE_PROP UCMD_Device_Property =
{
    UCMD_init,
    UCMD_Reset,
    UCMD_Status_In,
    UCMD_Status_Out,
    UCMD_Data_Setup,
    UCMD_NoData_Setup,
    UCMD_Get_Interface_Setting,
    UCMD_GetDeviceDescriptor,
    UCMD_GetConfigDescriptor,
    UCMD_GetStringDescriptor,
    0,
    0x40 /*MAX PACKET SIZE*/
};

const USER_STANDARD_REQUESTS UCMD_User_Standard_Requests =
{
    Ucmd_GetConfiguration,
    Ucmd_SetConfiguration,
    Ucmd_GetInterface,
    Ucmd_SetInterface,
    Ucmd_GetStatus,
    Ucmd_ClearFeature,
    Ucmd_SetEndPointFeature,
    Ucmd_SetDeviceFeature,
    Ucmd_SetDeviceAddress
};

const ONE_DESCRIPTOR UCMD_Device_Descriptor =
{
    (u8*)UCMD_DeviceDescriptor,
    UCMD_SIZ_DEVICE_DESC
};

const ONE_DESCRIPTOR UCMD_Config_Descriptor =
{
    (u8*)UCMD_ConfigDescriptor,
    UCMD_SIZ_CONFIG_DESC
};

const ONE_DESCRIPTOR UCMD_String_Descriptor[5] =
{
    {(u8*)UCMD_StringLangID, UCMD_SIZ_STRING_LANGID},
    {(u8*)UCMD_StringVendor, UCMD_SIZ_STRING_VENDOR},
    {(u8*)UCMD_StringProduct, UCMD_SIZ_STRING_PRODUCT},
    {(u8*)UCMD_StringSerial, UCMD_SIZ_STRING_SERIAL},
    {(u8*)UCMD_StringInterface, UCMD_SIZ_STRING_INTERFACE},
};

/* Extern variables ----------------------------------------------------------*/
DEVICE_PROP Device_Property;
USER_STANDARD_REQUESTS User_Standard_Requests;
DEVICE Device_Table;
ONE_DESCRIPTOR Device_Descriptor;
ONE_DESCRIPTOR Config_Descriptor;
ONE_DESCRIPTOR String_Descriptor[5];

/* Private function prototypes -----------------------------------------------*/
/* Extern function prototypes ------------------------------------------------*/
/* Private functions ---------------------------------------------------------*/
/*******************************************************************************
* Function Name  : UCMD_init
* Description    : UCMD init routine.
* Input          : None.
* Output         : None.
* Return         : None.
*******************************************************************************/
void UCMD_init()
{
    /* Update the serial number string descriptor with the data from the unique
    ID*/
    Get_SerialNum();
    
    pInformation->Current_Configuration = 0;
    
    /* Connect the device */
    PowerOn();
    
    /* USB interrupts initialization */
    /* clear pending interrupts */
    _SetISTR(0);
    wInterrupt_Mask = IMR_MSK;
    /* set interrupts mask */
    _SetCNTR(wInterrupt_Mask);
    
    bDeviceState = UNCONNECTED;
}

/*******************************************************************************
* Function Name  : UCMD_Reset
* Description    : UCMD reset routine.
* Input          : None.
* Output         : None.
* Return         : None.
*******************************************************************************/
void UCMD_Reset()
{
    /* Set the device as not configured */
    Device_Info.Current_Configuration = 0;
    
    /* Current Feature initialization */
    pInformation->Current_Feature = UCMD_ConfigDescriptor[7];
    
    /* Set UCMD DEVICE with the default Interface*/
    pInformation->Current_Interface = 0;
    SetBTABLE(BTABLE_ADDRESS);
    
    /* Initialize Endpoint 0 */
    SetEPType(ENDP0, EP_CONTROL);
    SetEPTxStatus(ENDP0, EP_TX_NAK);
    SetEPRxAddr(ENDP0, ENDP0_RXADDR);
    SetEPRxCount(ENDP0, UCMD_Device_Property.MaxPacketSize);
    SetEPTxAddr(ENDP0, ENDP0_TXADDR);
    Clear_Status_Out(ENDP0);
    SetEPRxValid(ENDP0);
    
    /* Initialize Endpoint 1 */
    SetEPType(ENDP2, EP_BULK);
    SetEPTxAddr(ENDP2, ENDP2_TXADDR);
    SetEPTxStatus(ENDP2, EP_TX_NAK);
    SetEPRxStatus(ENDP2, EP_RX_DIS);
    
    /* Initialize Endpoint 2 */
    SetEPType(ENDP1, EP_BULK);
    SetEPRxAddr(ENDP1, ENDP1_RXADDR);
    SetEPRxCount(ENDP1, UCMD_Device_Property.MaxPacketSize);
    SetEPRxStatus(ENDP1, EP_RX_VALID);
    SetEPTxStatus(ENDP1, EP_TX_DIS);
    
    
    SetEPRxCount(ENDP0, UCMD_Device_Property.MaxPacketSize);
    SetEPRxValid(ENDP0);
    
    /* Set the device to response on default address */
    SetDeviceAddress(0);
    
    bDeviceState = ATTACHED;
}

/*******************************************************************************
* Function Name  : Ucmd_SetConfiguration
* Description    : Handle the SetConfiguration request.
* Input          : None.
* Output         : None.
* Return         : None.
*******************************************************************************/
void Ucmd_SetConfiguration(void)
{
    if (pInformation->Current_Configuration != 0)
    {
        /* Device configured */
        bDeviceState = CONFIGURED;
        
        ClearDTOG_TX(ENDP1);
        ClearDTOG_RX(ENDP2);
    }
}

/*******************************************************************************
* Function Name  : Ucmd_ClearFeature
* Description    : Handle the ClearFeature request.
* Input          : None.
* Output         : None.
* Return         : None.
*******************************************************************************/
//void Ucmd_ClearFeature(void)
//{
//    /* when the host send a CBW with invalid signature or invalid length the two
//    Endpoints (IN & OUT) shall stall until receiving a UCMD Reset     */
//    if (CBW.dSignature != BOT_CBW_SIGNATURE)
//        Bot_Abort(BOTH_DIR);
//}

/*******************************************************************************
* Function Name  : Ucmd_SetConfiguration.
* Description    : Udpade the device state to addressed.
* Input          : None.
* Output         : None.
* Return         : None.
*******************************************************************************/
void Ucmd_SetDeviceAddress (void)
{
    bDeviceState = ADDRESSED;
}
/*******************************************************************************
* Function Name  : UCMD_Status_In
* Description    : UCMD Status IN routine.
* Input          : None.
* Output         : None.
* Return         : None.
*******************************************************************************/
void UCMD_Status_In(void)
{
    return;
}

/*******************************************************************************
* Function Name  : UCMD_Status_Out
* Description    : UCMD Status OUT routine.
* Input          : None.
* Output         : None.
* Return         : None.
*******************************************************************************/
void UCMD_Status_Out(void)
{
    return;
}

/*******************************************************************************
* Function Name  : UCMD_Data_Setup.
* Description    : Handle the data class specific requests..
* Input          : RequestNo.
* Output         : None.
* Return         : RESULT.
*******************************************************************************/
RESULT UCMD_Data_Setup(u8 RequestNo)
{
    u8      *(*CopyRoutine)(u16);
    
    CopyRoutine = NULL;
    
    if ((Type_Recipient == (CLASS_REQUEST | INTERFACE_RECIPIENT))
        && (RequestNo == GET_MAX_LUN) && (pInformation->USBwValue == 0)
            && (pInformation->USBwIndex == 0) && (pInformation->USBwLength == 0x01))
    {
        //!?! CopyRoutine = Ucmd_Get_Max_Lun;
    }
    else
    {
        return USB_UNSUPPORT;
    }
    
    if (CopyRoutine == NULL)
    {
        return USB_UNSUPPORT;
    }
    
    pInformation->Ctrl_Info.CopyData = CopyRoutine;
    pInformation->Ctrl_Info.Usb_wOffset = 0;
    (*CopyRoutine)(0);
    
    return USB_SUCCESS;
    
}

/*******************************************************************************
* Function Name  : UCMD_NoData_Setup.
* Description    : Handle the no data class specific requests.
* Input          : RequestNo.
* Output         : None.
* Return         : RESULT.
*******************************************************************************/
RESULT UCMD_NoData_Setup(u8 RequestNo)
{
    if (Type_Recipient == (VENDOR_REQUEST | OTHER_RECIPIENT))
    {
        usbcmds_get_setup_packet_data
            (RequestNo,ByteSwap(pInformation->USBwValue),
             ByteSwap(pInformation->USBwIndex),
             ByteSwap(pInformation->USBwLength));
        
        return USB_SUCCESS;
    }
    else if ((Type_Recipient == (CLASS_REQUEST | INTERFACE_RECIPIENT))
        && (RequestNo == UCMD_STORAGE_RESET) && (pInformation->USBwValue == 0)
            && (pInformation->USBwIndex == 0) && (pInformation->USBwLength == 0x00))
    {
        /* Initialize Endpoint 1 */
        ClearDTOG_TX(ENDP1);
        
        /* Initialize Endpoint 2 */
        ClearDTOG_RX(ENDP2);
        
        return USB_SUCCESS;
    }
    return USB_UNSUPPORT;
}

/*******************************************************************************
* Function Name  : UCMD_Get_Interface_Setting
* Description    : Test the interface and the alternate setting according to the
*                  supported one.
* Input          : u8 Interface, u8 AlternateSetting.
* Output         : None.
* Return         : RESULT.
*******************************************************************************/
RESULT UCMD_Get_Interface_Setting(u8 Interface, u8 AlternateSetting)
{
    if (AlternateSetting > 0)
    {
        return USB_UNSUPPORT;/* in this application we don't have AlternateSetting*/
    }
    else if (Interface > 0)
    {
        return USB_UNSUPPORT;/*in this application we have only 1 interfaces*/
    }
    return USB_SUCCESS;
}

/*******************************************************************************
* Function Name  : UCMD_GetDeviceDescriptor
* Description    : Get the device descriptor.
* Input          : u16 Length.
* Output         : None.
* Return         : None.
*******************************************************************************/
u8 *UCMD_GetDeviceDescriptor(u16 Length)
{
    return Standard_GetDescriptorData(Length, &Device_Descriptor );
}

/*******************************************************************************
* Function Name  : UCMD_GetConfigDescriptor
* Description    : Get the configuration descriptor.
* Input          : u16 Length.
* Output         : None.
* Return         : None.
*******************************************************************************/
u8 *UCMD_GetConfigDescriptor(u16 Length)
{
    return Standard_GetDescriptorData(Length, &Config_Descriptor );
}

/*******************************************************************************
* Function Name  : UCMD_GetStringDescriptor
* Description    : Get the string descriptors according to the needed index.
* Input          : u16 Length.
* Output         : None.
* Return         : None.
*******************************************************************************/
u8 *UCMD_GetStringDescriptor(u16 Length)
{
    u8 wValue0 = pInformation->USBwValue0;
    
    if (wValue0 > 5)
    {
        return NULL;
    }
    else
    {
        return Standard_GetDescriptorData(Length, &String_Descriptor[wValue0]);
    }
}

/*******************************************************************************
* Function Name  : Ucmd_Get_Max_Lun
* Description    : Handle the Get Max Lun request.
* Input          : u16 Length.
* Output         : None.
* Return         : None.
*******************************************************************************/
u8 *Ucmd_Get_Max_Lun(u16 Length)
{
    if (Length == 0)
    {
        pInformation->Ctrl_Info.Usb_wLength = LUN_DATA_LENGTH;
        return 0;
    }
    else
    {
        return 0;//((u8*)(&Max_Lun));
    }
}
