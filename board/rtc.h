/**
  **************** (C) COPYRIGHT 2011 SCT Performance, LLC *********************
  * File Name          : rtc.h
  * Device / System    : SCT products using the "CommonCode" platform
  * Author             : Quyen Leba
  *
  * Version            : 1 
  * Date               : 05/02/2011
  * Description        : 
  *                    : 
  *
  *
  * History            : 05/02/2011 M. Davis
  *                    :   Add SCT Copyright header,
  *
  ******************************************************************************
  */

#ifndef __RTC_H
#define __RTC_H

#include <arch/gentype.h>

void rtc_init();
void rtc_reset();
u32 rtc_getvalue();

#endif  //__RTC_H
