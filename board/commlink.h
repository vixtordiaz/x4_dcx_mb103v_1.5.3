/**
  **************** (C) COPYRIGHT 2011 SCT Performance, LLC *********************
  * File Name          : commlink.h
  * Device / System    : SCT products using the "CommonCode" platform
  * Author             : Quyen Leba
  *
  * Version            : 1 
  * Date               : 05/02/2011
  * Description        : 
  *                    : 
  *
  *
  * History            : 05/02/2011 M. Davis
  *                    :   Add SCT Copyright header,
  *
  ******************************************************************************
  */

#ifndef __COMMLINK_H
#define __COMMLINK_H

// generate a compiler error for typedef values outside allowed range
#pragma diag_error=Pe229

#include <arch/gentype.h>
#include <common/cmdif.h>

#define COMMLINK_SOF            0x04
#define COMMLINK_EOF            0x06

#define MAX_COMMAND_DATALENGTH          2048

typedef enum
{
    //use in Simple Packet Wrapper
    COMMLINK_SPW_START,
    COMMLINK_SPW_CMDSTATUS,
    COMMLINK_SPW_CMDCODE,
    COMMLINK_SPW_PAYLOADLEN0,
    COMMLINK_SPW_PAYLOADLEN1,
    COMMLINK_SPW_CRC32E0,
    COMMLINK_SPW_CRC32E1,
    COMMLINK_SPW_CRC32E2,
    COMMLINK_SPW_CRC32E3,
    COMMLINK_SPW_RESPONSEACK,
    COMMLINK_SPW_RESPONSECMDCODE,
    COMMLINK_SPW_DATA,
    COMMLINK_SPW_END,
    COMMLINK_SPW_ERR,
}CommLinkPacketState;

typedef struct
{
    u8      cmdStatus;
    u8      cmdCode;    
    u16     wPayloadlen;
    u16     wPayloadOffset;
    u32     dwCRC32E;
    u8      responseAck;
    u8      responseCmdCode;
    u8      pPayload[(MAX_COMMAND_DATALENGTH+16)];    
}CommLinkFrameContainer;

typedef struct
{
    //all in seconds
    u32 PriorConnect;
    u32 PriorDisconnect;
    u32 PriorSessionOpen;
    u32 PriorSessionClose;

    u32 LastConnect;
    u32 LastDisconnect;
    u32 LastSessionOpen;
    u32 LastSessionClose;
    
    u32 LastTransmission;
}CommLinkHistory;

// the following typedef is used as a 2-bit 'link' field in the CommLinkInfo struct...
// -DO NOT- assign any negative values, or values greater than 3
typedef enum
{
    CommLinkInfoLink_None           = 0,
    CommLinkInfoLink_Connected      = 1,
    CommLinkInfoLink_Opened         = 2,
}CommLinkInfoLinkStatus;

// Limit range of preceding typedef enum to 0 - 3 (0x00 - 0x03).
// The "#pragma diag_error=Pe229" at the top of this file causes a compiler 
// error to be generated when this limit is exceeded
typedef struct
{
    CommLinkInfoLinkStatus checkRange : 2;
}CommLinkInfoLinkStatus_RangeConfirm;

// -DO NOT- change the following existing enum
typedef enum
{
    COMMLINK_2400_BAUD              = 0x00,
    COMMLINK_4800_BAUD              = 0x01,
    COMMLINK_7200_BAUD              = 0x02,
    COMMLINK_9600_BAUD              = 0x03,
    COMMLINK_19200_BAUD             = 0x04,
    COMMLINK_38400_BAUD             = 0x05,
    COMMLINK_57600_BAUD             = 0x06,
    COMMLINK_115200_BAUD            = 0x07,
    COMMLINK_230400_BAUD            = 0x08,
    COMMLINK_460800_BAUD            = 0x09,
    COMMLINK_921600_BAUD            = 0x0A,
}CommLinkBaudrate;

typedef enum
{
    CommLinkInfoMode_Commander      = 0,
    CommLinkInfoMode_Responder      = 1,
}CommLinkMode;

// Limit range of preceding typedef enum to 0 - 16 (0x00 - 0x0F).
// The "#pragma diag_error=Pe229" at the top of this file causes a compiler 
// error to be generated when this limit is exceeded
typedef struct
{
    CommLinkBaudrate checkRange : 4;
}CommLinkBaudrate_RangeConfirm;

typedef struct
{
    u8  cmdStatus;
    u8  cmdAck;
    u8  responseCode;
    u8  responseCmd;
    u8  *data;
    u32 datalength;
}CommandResponse;

typedef struct
{
    u8  use_encryption      : 1;
    u8  frame_avail         : 1;
    u8  passthrough         : 1;
    u8  data_ready          : 1;
    CommLinkInfoLinkStatus  link  : 2;    //0: none, 1: Connected, 2: Opened (iAP)
    u8  halted              : 1;
    u8  loopback            : 1;

    u32 speedsync_signature;

    CommLinkMode            mode;   // 0: Commander, 1: Responder - Mainboard typically Responder mode
    CommLinkPacketState     packetstate;
    CommLinkBaudrate        baudrate;
    CommLinkFrameContainer  container;
    CommandResponse         cmdresp;
    CommLinkHistory         connection_history;
}CommLinkInfo;

extern CommLinkInfo commlink_info;

#define commlink_set_halt_sending_ack()         commlink_info.halted = 1
#define commlink_clear_halt_sending_ack()       commlink_info.halted = 0
#define commlink_is_halt_sending_ack()          (commlink_info.halted == 1)

#define commlink_set_connection_history_session_open()  \
    commlink_info.connection_history.PriorSessionOpen = commlink_info.connection_history.LastSessionOpen;   \
    commlink_info.connection_history.LastSessionOpen = timeout_counter_gettime_in_sec()

#define commlink_set_connection_history_session_close()  \
    commlink_info.connection_history.PriorSessionClose = commlink_info.connection_history.LastSessionClose; \
    commlink_info.connection_history.LastSessionClose = timeout_counter_gettime_in_sec()

#define commlink_set_connection_history_session_connect()  \
    commlink_info.connection_history.PriorConnect = commlink_info.connection_history.LastConnect;   \
    commlink_info.connection_history.LastConnect = timeout_counter_gettime_in_sec()

#define commlink_set_connection_history_session_disconnect()  \
    commlink_info.connection_history.PriorDisconnect = commlink_info.connection_history.LastDisconnect; \
    commlink_info.connection_history.LastDisconnect = timeout_counter_gettime_in_sec()

#define commlink_get_device_name(n)             commlink_hal_get_device_name(n)

#define commlink_status_indicator_update()      commlink_hal_status_indicator_update()
#define commlink_reset_transmission_watchdog()  commlink_info.connection_history.LastTransmission = timeout_counter_gettime_in_sec()

#define commlink_change_baudrate(b)             commlink_hal_change_baudrate(b)
#define commlink_tx(data,len,session_required)  commlink_hal_tx(data,len,session_required)

u8 commlink_init(CommLinkBaudrate baud);
u8 commlink_speedsyncrequest();
u8 commlink_speedsyncconfirm();
void commlink_passthrough_config(bool en);
void commlink_loopback_config(bool en);
u8 commlink_change_baudrate(CommLinkBaudrate baudrate);

u8 commlink_update_link_status(CommLinkInfoLinkStatus st);
CommLinkInfoLinkStatus commlink_get_link_status();
void commlink_process_incoming_command_byte(u8 in);
void commlink_process_incoming_response_byte(u8 in);
u8 commlink_processframe_simplewrapper();
u8 commlink_receive_command(u8 *data, u16 *len, u16 max_len_allowed);
void commlink_change_mode(CommLinkMode newmode);
u8 commlink_send_ack(CMDIF_COMMAND cmd, CMDIF_ACK resp, u8 *data, u16 datalen);
u8 commlink_basicCmdAck(const CMDIF_COMMAND cmd,
                        const u8 *cmdData, u16 cmdDatalength,
                        CommandResponse *pxResponse, u32 ms_timeout);
u8 commlink_get_response(CMDIF_COMMAND cmd, u32 ms_timeout,
                         CommandResponse *pxResponse);
u8 commlink_send_cmd(const CMDIF_COMMAND cmd,
                     const u8 *data, u32 datalength,
                     bool flushbeforesendcmd);

#endif    //__COMMLINK_H
