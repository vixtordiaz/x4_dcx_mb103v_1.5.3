/**
  **************** (C) COPYRIGHT 2011 SCT Performance, LLC *********************
  * File Name          : sci.c
  * Device / System    : SCT products using the "CommonCode" platform
  * Author             : Rhonda Rusak
  *
  * Version            : 1 
  * Date               : 05/13/2014
  * Description        : 
  *                    : 
  *
  *
  *
  ******************************************************************************
  */

#include <string.h>
#include <board/mblexec.h>
#include <board/delays.h>
#include <common/statuscode.h>
#include <common/obd2def.h>
#include <common/obd2sci.h>
#include "sci.h"


u8 sci_init(VehicleCommType *vehiclecommtype, VehicleCommLevel *vehiclecommlevel)
{
    VehicleCommConfig comm_config;
    u8  buffer[4];
    u8  status; 
    
    comm_config.raw.value = 0;
    comm_config.sci.speed = SCI_INITSPEED_L;
 
    buffer[0] = CommType_SCIA;
    buffer[1] = CommLevel_Default;
    buffer[2] = comm_config.raw.value & 0xFF;
    buffer[3] = comm_config.raw.value >> 8;

    mblexec_call(MBLEXEC_OPCODE_SET_VEHICLE_COMM,buffer,sizeof(buffer),
                          NULL,NULL);
    
    /*Find out sci comm type:  SCIA and SCIB*/
    status = obd2sci_get_sci_commtype_commlevel(vehiclecommtype, vehiclecommlevel);
    if(status != S_SUCCESS)
    {
        status = S_FAIL; 
    }
    
    return status; 
}

//------------------------------------------------------------------------------
// SCI tx function (tx for both: SCIA/SCIB)
// Inputs:  u8 *cmd_frame,
//          u16 txflags
//          u16 tx_datalength,
// Return:  status; 
// Engineer: Ricardo Cigarroa
//------------------------------------------------------------------------------
u8 sci_tx(u8 *cmd_frame, u8 tx_datalength, u16 txflags)
{
    u8  buffer[8];
    u8  status;
    
    memset(buffer,0,sizeof(buffer));
    buffer[0] = (u8)(txflags & 0xFF);
    buffer[1] = (u8)((txflags >> 8) & 0xFF);

    memcpy((char*)&buffer[2],(char*) cmd_frame,tx_datalength);

    status = mblexec_call(MBLEXEC_OPCODE_SEND_SIMPLE_MSG,buffer,tx_datalength+2,
                          NULL,NULL);
    return status;
}
//------------------------------------------------------------------------------
// SCI rx function (rx for both: SCIA/SCIB)
// Inputs:  u8 *rxdatabuffer,
//          u16 *rxdatabufferlength,
//          u32 opcode_length,
//          u16 rxflags,
// Return:  status; 
// Engineer: Ricardo Cigarroa
//------------------------------------------------------------------------------
u8 sci_rx(u8 *rxdatabuffer, u16 *rxdatabufferlength, u16 rxflags, u32 timeout)
{
    u8  buffer[4];
    u8  status;
 
    memset(buffer,0,sizeof(buffer));       
    buffer[0] = (u8)(rxflags & 0xFF);
    buffer[1] = (u8)((rxflags >> 8) & 0xFF);
    buffer[2] = timeout;
    buffer[3] = timeout >> 8;


    status = mblexec_call(MBLEXEC_OPCODE_RECEIVE_SIMPLE_MSG,
                          buffer,sizeof(buffer),
                          rxdatabuffer,rxdatabufferlength);
    
    return status;
}