/**
  **************** (C) COPYRIGHT 2011 SCT Performance, LLC *********************
  * File Name          : power.h
  * Device / System    : SCT products using the "CommonCode" platform
  * Author             : Quyen Leba
  *
  * Version            : 1 
  * Date               : 05/02/2011
  * Description        : 
  *                    : 
  *
  *
  * History            : 05/02/2011 M. Davis
  *                    :   Add SCT Copyright header,
  *
  ******************************************************************************
  */

#ifndef __POWER_H
#define __POWER_H

#include <arch/gentype.h>

typedef enum
{
    Vpp_OFF         = 0,
    Vpp_FEPS        = 1,
    Vpp_SCI_AE_6    = 2,
    Vpp_SCI_BT_9    = 3,
    Vpp_SCI_BE_12   = 4,
    Vpp_SCI_AT_14   = 5,
}VppState;

u8 Vpp_Ctrl(VppState state);
void V5VExt_Ctrl(FunctionalState state);
void Init_VPP();
bool Pwr_USB_powered();

void power_init();
#define power_setvpp(state)     Vpp_Ctrl(state)

#endif  //__POWER_H
