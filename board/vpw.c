/**
  **************** (C) COPYRIGHT 2011 SCT Performance, LLC *********************
  * File Name          : vpw.c
  * Device / System    : SCT products using the "CommonCode" platform
  * Author             : Quyen Leba
  *
  * Version            : 1 
  * Date               : 05/02/2011
  * Description        : 
  *                    : 
  *
  *
  * History            : 05/02/2011 M. Davis
  *                    :   Add SCT Copyright header,
  *
  ******************************************************************************
  */

#include <board/genplatform.h>
#include <board/timer.h>
#include <board/delays.h>
#include <common/statuscode.h>
#include "vpw.h"

#define VPW_FLAGS_HIGHSPEED                     (1<<0)
#define VPW_FLAGS_BREAK                         (1<<1)

extern funcptr_mblif_link* mblif_link;              //from mblif.c

//------------------------------------------------------------------------------
// Inputs:  bool boosted (TRUE: boosted power mode)
//          bool highspeed (TRUE: high speed mode)
//
// Return:  u8  status
// Engineer: Quyen Leba, Patrick Downs
// Date: Jan 28, 2008
//------------------------------------------------------------------------------
u8 vpw_init(bool boosted, bool highspeed)
{
    VehicleCommConfig comm_config;
    u8  buffer[4];
    u8  status;

    comm_config.raw.value = 0;
    // Voltage Boost - This is typically always ENABLED
    if (boosted)
    {
        comm_config.vpw.boosted = 1;
    }    
    // High or low speed baud.
    if (highspeed)
    {
        comm_config.vpw.highspeed = 1;
    }

    buffer[0] = CommType_VPW;
    buffer[1] = CommLevel_Default;
    buffer[2] = comm_config.raw.value & 0xFF;
    buffer[3] = comm_config.raw.value >> 8;
    //no privdata

    status = mblexec_call(MBLEXEC_OPCODE_SET_VEHICLE_COMM,buffer,sizeof(buffer),
                          NULL,NULL);
    return status;
}

//------------------------------------------------------------------------------
// Inputs:  u8  *databuffer
//          u16 datalength
//          u8  speedmode (VPW_LOW_SPEED, VPW_HIGH_SPEED)
// Return:  u8  status
// Engineer: Quyen Leba
//------------------------------------------------------------------------------
u8 vpw_tx(u8 *databuffer, u16 datalength)
{
    u16 flags;
    u8  status;

    flags = 0;
    status = mblif_link(MBLIF_CMD_WRITE_BUFFER,0,databuffer,&datalength);
    if (status != S_SUCCESS)
    {
        return status;
    }
    status = mblexec_call(MBLEXEC_OPCODE_SEND_SIMPLE_MSG,
                          (u8*)&flags,sizeof(flags),NULL,NULL);
    return status;
}

//------------------------------------------------------------------------------
// Input:   u8  speedmode (VPW_LOW_SPEED, VPW_HIGH_SPEED)
// Outputs: u8  *databuffer
//          u16 *datalength
// Return:  u8  status
// Engineer: Quyen Leba
//------------------------------------------------------------------------------
u8 vpw_rx(u8 *databuffer, u16 *datalength, u16 timeout, u8 mode, u8 funcaddr)
{
    u8 buffer[4];
    u16 flags;
    u8  status;
    
    flags = 0;
        
    if(mode == VPW_MODE_FUNCTIONAL)
    {
        flags |= VPW_FLAGS_FUNCTIONALADDR;
        flags |= (u16)(funcaddr<<8);
    }
    buffer[0] = flags;
    buffer[1] = flags >> 8;
    buffer[2] = timeout;
    buffer[3] = timeout >> 8;
    
    status = mblexec_call(MBLEXEC_OPCODE_RECEIVE_SIMPLE_MSG,
                          buffer,sizeof(buffer),databuffer,datalength);
    return status;
}

//------------------------------------------------------------------------------
// Inputs:  u8  *tx_databuffer
//          u16 tx_datalength
//          u8  speedmode (VPW_LOW_SPEED, VPW_HIGH_SPEED)
// Outputs: u8  *rx_databuffer
//          u16 *rx_datalength
// Return:  u8  status
// Engineer: Quyen Leba
//------------------------------------------------------------------------------
u8 vpw_txrx_simple(u8 *tx_databuffer, u16 tx_datalength,
                   u8 *rx_databuffer, u16 *rx_datalength)
{
    u8  vpw_txbuffer[8+32];
    u16 sendflags;
    u16 receiveflags;
    u8  status;

    *rx_datalength = 0;
    if (tx_datalength > 32)
    {
        return S_INPUT;
    }

    sendflags = 0;
    receiveflags = 0; // TODOP Does this need timeout flag set?

    memset((char*)vpw_txbuffer,0,sizeof(vpw_txbuffer));
    *(u16*)(&vpw_txbuffer[0]) = sendflags;
    *(u16*)(&vpw_txbuffer[4]) = receiveflags;
    memcpy((char*)&vpw_txbuffer[8],(char*)tx_databuffer,tx_datalength);

    status = mblexec_call(MBLEXEC_OPCODE_SEND_RECEIVE_SIMPLE_MSG,
                          vpw_txbuffer,tx_datalength+8,
                          rx_databuffer,rx_datalength);
    return status;
}

//------------------------------------------------------------------------------
// Send a VPW break
// Return:  u8  status
// Engineer: Quyen Leba
//------------------------------------------------------------------------------
u8 vpw_send_break()
{
    u16 flags;
    u8  status;

    flags = VPW_FLAGS_BREAK;
    status = mblexec_call(MBLEXEC_OPCODE_SEND_SIMPLE_MSG,
                          (u8*)&flags,sizeof(flags),NULL,NULL);
    return status;
}
