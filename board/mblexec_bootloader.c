/**
  **************** (C) COPYRIGHT 2011 SCT Performance, LLC *********************
  * File Name          : mblexec_bootloader.c
  * Device / System    : SCT products using the "CommonCode" platform
  * Author             : Quyen Leba
  *
  * Version            : 1 
  * Date               : 05/02/2011
  * Description        : 
  *                    : 
  *
  *
  * History            : 05/02/2011 M. Davis
  *                    :   Add SCT Copyright header,
  *
  ******************************************************************************
  */

#include <board/genplatform.h>
#include <common/statuscode.h>
#include "mblexec_bootloader.h"

extern funcptr_mblif_link* mblif_link;              //from mblif.c

//------------------------------------------------------------------------------
// Set bootloader mode
// Input:   BootloaderMode bootmode
// Engineer: Quyen Leba
//------------------------------------------------------------------------------
u8 mblexec_bootloader_setboot(BootloaderMode bootmode)
{
    MBLIF_CONTROL *pcontrol;
    u8  buffer[256];
    u8  data[2];
    u16 length;
    u8  status;

    data[0] = ((u16)bootmode) & 0xFF;
    data[1] = ((u16)bootmode) >> 8;
    length = 2;
    status = mblif_link(MBLIF_CMD_EXEC,
                        MBLEXEC_OPCODE_BOOTLOADER_SETBOOT,
                        data,&length);
    if (status != S_SUCCESS)
    {
        return status;
    }

    app_timeout_set(500);
    while(1)
    {
        status = mblif_link(MBLIF_CMD_GET_STATUS,0,buffer,&length);
        if (status == S_SUCCESS && length == 4)
        {
            pcontrol = (MBLIF_CONTROL*)&buffer;
            if (pcontrol->exec_done)
            {
                if (pcontrol->exec_ok)
                {
                    status = S_SUCCESS;
                }
                else
                {
                    status = pcontrol->status;
                }
                break;
            }
        }//if (status ...
        else if (app_timeout())
        {
            status = S_TIMEOUT;
            break;
        }
    }//while(1)...

    return status;
}

//------------------------------------------------------------------------------
// Set bootloader init
// Input:   FirmwareHeader *fwheader
// Engineer: Quyen Leba
//------------------------------------------------------------------------------
u8 mblexec_bootloader_init(FirmwareHeader *firmware_header)
{
    MBLIF_CONTROL *pcontrol;
    u8  buffer[256];
    u16 length;
    u8  status;

    length = sizeof(FirmwareHeader);
    status = mblif_link(MBLIF_CMD_EXEC,
                        MBLEXEC_OPCODE_BOOTLOADER_INIT,
                        (u8*)firmware_header,&length);
    if (status != S_SUCCESS)
    {
        return status;
    }

    app_timeout_set(500);
    while(1)
    {
        status = mblif_link(MBLIF_CMD_GET_STATUS,0,buffer,&length);
        if (status == S_SUCCESS && length == 4)
        {
            pcontrol = (MBLIF_CONTROL*)&buffer;
            if (pcontrol->exec_done)
            {
                if (pcontrol->exec_ok)
                {
                    status = S_SUCCESS;
                }
                else
                {
                    status = pcontrol->status;
                }
                break;
            }
        }//if (status ...
        else if (app_timeout())
        {
            status = S_TIMEOUT;
            break;
        }
    }//while(1)...

    return status;
}

//------------------------------------------------------------------------------
// Program bootloader data
// Inputs:  u8  *data
//          u32 datalength
// Engineer: Quyen Leba
//------------------------------------------------------------------------------
u8 mblexec_bootloader_do(u8 *data, u32 datalength)
{
    MBLIF_CONTROL *pcontrol;
    u8  buffer[256];
    u16 length;
    u8  status;

    length = datalength;
    status = mblif_link(MBLIF_CMD_EXEC,
                        MBLEXEC_OPCODE_BOOTLOADER_DO,
                        data,&length);
    if (status != S_SUCCESS)
    {
        return status;
    }

    app_timeout_set(500);
    while(1)
    {
        status = mblif_link(MBLIF_CMD_GET_STATUS,0,buffer,&length);
        if (status == S_SUCCESS && length == 4)
        {
            pcontrol = (MBLIF_CONTROL*)&buffer;
            if (pcontrol->exec_done)
            {
                if (pcontrol->exec_ok)
                {
                    status = S_SUCCESS;
                }
                else
                {
                    status = pcontrol->status;
                }
                break;
            }
        }//if (status ...
        else if (app_timeout())
        {
            status = S_TIMEOUT;
            break;
        }
    }//while(1)...

    return status;
}

//------------------------------------------------------------------------------
// Validate firmware in flash
// Input:   u32 fwcrc32e
// Engineer: Quyen Leba
//------------------------------------------------------------------------------
u8 mblexec_bootloader_validate(u32 fwcrc32e)
{
    MBLIF_CONTROL *pcontrol;
    u8  buffer[256];
    u16 length;
    u8  status;

    length = 4;
    status = mblif_link(MBLIF_CMD_EXEC,
                        MBLEXEC_OPCODE_BOOTLOADER_VALIDATE,
                        (u8*)&fwcrc32e,&length);
    if (status != S_SUCCESS)
    {
        return status;
    }

    app_timeout_set(500);
    while(1)
    {
        status = mblif_link(MBLIF_CMD_GET_STATUS,0,buffer,&length);
        if (status == S_SUCCESS && length == 4)
        {
            pcontrol = (MBLIF_CONTROL*)&buffer;
            if (pcontrol->exec_done)
            {
                if (pcontrol->exec_ok)
                {
                    status = S_SUCCESS;
                }
                else
                {
                    status = pcontrol->status;
                }
                break;
            }
        }//if (status ...
        else if (app_timeout())
        {
            status = S_TIMEOUT;
            break;
        }
    }//while(1)...

    return status;
}
