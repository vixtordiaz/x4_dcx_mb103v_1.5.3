/**
  **************** (C) COPYRIGHT 2011 SCT Performance, LLC *********************
  * File Name          : lcd_helper.h
  * Device / System    : SCT products using the "CommonCode" platform
  * Author             : Quyen Leba
  *
  * Version            : 1 
  * Date               : 05/02/2011
  * Description        : 
  *                    : 
  *
  *
  * History            : 05/02/2011 M. Davis
  *                    :   Add SCT Copyright header,
  *
  ******************************************************************************
  */

#ifndef __LCD_HELPER_H
#define __LCD_HELPER_H

typedef struct
{
    u8 x;           // Symbol size in no X pixels
    u8 y;           // Symbol size in no Y pixels
}SYMHEAD;

typedef struct SYMBOL
{
    SYMHEAD xycoor; // Symbol header
    u8  b[1024];    // Symbol data
}SYMBOL;

u8      gputimg(const u8 *filename, u8 z, u8 w);
void    gclearareafast(u8 c1, u8 l1, u8 c2, u8 l2);
void    gputsrpos(u8 x, u8 y, u8* text);
void    gputscpos(u8 x, u8 y, u8* text);

#endif

//------------------------------------------------------------------------------------------------------------------------------------------------
// EOF
//------------------------------------------------------------------------------------------------------------------------------------------------
