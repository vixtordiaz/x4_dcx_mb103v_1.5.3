/**
  **************** (C) COPYRIGHT 2011 SCT Performance, LLC *********************
  * File Name          : peripherals.c
  * Device / System    : SCT products using the "CommonCode" platform
  * Author             : Quyen Leba
  *
  * Version            : 1 
  * Date               : 05/02/2011
  * Description        : 
  *                    : 
  *
  *
  * History            : 05/02/2011 M. Davis
  *                    :   Add SCT Copyright header,
  *
  ******************************************************************************
  */

#include <91x_gpio.h>
#include <91x_scu.h>
#include <board/power.h>
#include <board/timer.h>
#include <board/rtc.h>
#include "usb.h"
#include "system.h"

//------------------------------------------------------------------------------
// Engineer: Quyen Leba
// Date: Jan 18, 2008
//------------------------------------------------------------------------------
void peripherals_init()
{
    SCU_APBPeriphClockConfig(__GPIO0, ENABLE);
    GPIO_DeInit(GPIO0);
    SCU_APBPeriphClockConfig(__GPIO1, ENABLE);
    GPIO_DeInit(GPIO1);
    SCU_APBPeriphClockConfig(__GPIO2, ENABLE);
    GPIO_DeInit(GPIO2);
    SCU_APBPeriphClockConfig(__GPIO3, ENABLE);
    GPIO_DeInit(GPIO3);
    SCU_APBPeriphClockConfig(__GPIO4, ENABLE);
    GPIO_DeInit(GPIO4);
    SCU_APBPeriphClockConfig(__GPIO5, ENABLE);
    GPIO_DeInit(GPIO5);
    SCU_APBPeriphClockConfig(__GPIO6, ENABLE);
    GPIO_DeInit(GPIO6);
    SCU_APBPeriphClockConfig(__GPIO7, ENABLE);
    GPIO_DeInit(GPIO7);
    SCU_APBPeriphClockConfig(__GPIO8, ENABLE);
    GPIO_DeInit(GPIO8);
    SCU_APBPeriphClockConfig(__GPIO9, ENABLE);
    GPIO_DeInit(GPIO9);

    SCU_APBPeriphClockConfig(__CAN, ENABLE);
    SCU_APBPeriphReset(__CAN, DISABLE);
    
    timer_init();
    timer1_init();
    emi_init();
    usb_init();
    power_init();
    keypad_init();
    lcd_init();

    /*  //TODO:
    if (settings.Backlight)
    {
        LCD_Backlight(ON);
    }
    else
    {
        LCD_Backlight(OFF);
    }

    if (settings.Contrast > 100)
    {
        settings.Contrast = 100;
    }
    LCD_ghw_cont_set(settings.Contrast);
    */
    LCD_Backlight(ON);
    LCD_ghw_cont_set(100);
    
    buzzer_init();
    rtc_init();
}
