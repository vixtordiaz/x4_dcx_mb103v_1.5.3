/**
  **************** (C) COPYRIGHT 2011 SCT Performance, LLC *********************
  * File Name          : emi.c
  * Device / System    : SCT products using the "CommonCode" platform
  * Author             : Quyen Leba
  *
  * Version            : 1 
  * Date               : 05/02/2011
  * Description        : 
  *                    : 
  *
  *
  * History            : 05/02/2011 M. Davis
  *                    :   Add SCT Copyright header,
  *
  ******************************************************************************
  */

#include "91x_emi.h"
#include "91x_gpio.h"
#include "91x_scu.h"
#include "system.h"

//------------------------------------------------------------------------------
// Engineer: Quyen Leba
// Date: Jan 18, 2008
//------------------------------------------------------------------------------
void emi_init()
{
    GPIO_InitTypeDef GPIO_InitStructure;
    EMI_InitTypeDef EMI_InitStruct;

    EMI_DeInit();

    // Config CS
    GPIO_StructInit(&GPIO_InitStructure);
    GPIO_InitStructure.GPIO_Pin =
        GPIO_Pin_4 | GPIO_Pin_5 | GPIO_Pin_6 | GPIO_Pin_7;
    GPIO_InitStructure.GPIO_Direction = GPIO_PinOutput;
    GPIO_InitStructure.GPIO_Type = GPIO_Type_PushPull;
    GPIO_InitStructure.GPIO_Alternate = GPIO_OutputAlt2;
    GPIO_Init(GPIO0, &GPIO_InitStructure);
    
    // Config data & address port
    GPIO_StructInit(&GPIO_InitStructure);
    GPIO_InitStructure.GPIO_Pin = 
        GPIO_Pin_0 | GPIO_Pin_1 | GPIO_Pin_2 | GPIO_Pin_3 | GPIO_Pin_4 | 
        GPIO_Pin_5 | GPIO_Pin_6;
    GPIO_InitStructure.GPIO_Direction = GPIO_PinOutput;
    GPIO_InitStructure.GPIO_Type = GPIO_Type_PushPull;
    GPIO_InitStructure.GPIO_Alternate = GPIO_OutputAlt2;
    GPIO_Init(GPIO7, &GPIO_InitStructure);

    GPIO_StructInit(&GPIO_InitStructure);
    GPIO_InitStructure.GPIO_Pin = GPIO_Pin_All;
    GPIO_InitStructure.GPIO_Direction = GPIO_PinOutput;
    GPIO_InitStructure.GPIO_Type = GPIO_Type_PushPull;
    GPIO_InitStructure.GPIO_Alternate = GPIO_OutputAlt2;
    GPIO_Init(GPIO8, &GPIO_InitStructure);

    GPIO_StructInit(&GPIO_InitStructure);
    GPIO_InitStructure.GPIO_Pin = GPIO_Pin_All;
    GPIO_InitStructure.GPIO_Direction = GPIO_PinOutput;
    GPIO_InitStructure.GPIO_Type = GPIO_Type_PushPull;
    GPIO_InitStructure.GPIO_Alternate = GPIO_OutputAlt2;
    GPIO_Init(GPIO9, &GPIO_InitStructure);

    SCU_EMIModeConfig(SCU_EMI_MUX);
    //SCU_EMIBCLKDivisorConfig(SCU_EMIBCLK_Div1);
    GPIO_EMIConfig(ENABLE);

    EMI_StructInit(&EMI_InitStruct);
    EMI_InitStruct.EMI_Bank_IDCY = 0xf;
    // Number of wait states for read accesses
    // This member can be: 0x01,0x02,0x03,...0x1F (Reset value:0x1F "31 cycles")
    EMI_InitStruct.EMI_Bank_WSTRD = 0x1f;
    EMI_InitStruct.EMI_Bank_WSTWR = 0x1f;
    EMI_InitStruct.EMI_Bank_WSTROEN = 0xf;
    EMI_InitStruct.EMI_Bank_WSTWEN = 0xf;
    EMI_InitStruct.EMI_Bank_MemWidth = EMI_Width_HalfWord;
    EMI_InitStruct.EMI_Bank_WriteProtection = EMI_Bank_NonWriteProtect;
    //EMI_InitStruct.EMI_Burst_and_PageModeRead_Selection = EMI_NormalMode;
    EMI_InitStruct.EMI_PageModeRead_Selection = EMI_NormalMode;

    //TODO: Just use one setup for now
    EMI_Init (EMI_Bank1,&EMI_InitStruct);       //LCD
    EMI_Init (EMI_Bank0,&EMI_InitStruct);       //NAND

    EMI_StructInit(&EMI_InitStruct);
    EMI_InitStruct.EMI_Bank_IDCY = 0x0f;
    EMI_InitStruct.EMI_Bank_WSTRD = 0x1f;
    EMI_InitStruct.EMI_Bank_WSTWR = 0x1f;
    EMI_InitStruct.EMI_Bank_WSTROEN = 0x0f;
    EMI_InitStruct.EMI_Bank_WSTWEN = 0x0f;
    EMI_InitStruct.EMI_Bank_MemWidth = EMI_Width_HalfWord;
    EMI_InitStruct.EMI_Bank_WriteProtection = EMI_Bank_NonWriteProtect;
    //EMI_InitStruct.EMI_Burst_and_PageModeRead_Selection = EMI_NormalMode;
    EMI_InitStruct.EMI_PageModeRead_Selection = EMI_NormalMode;
    EMI_Init (EMI_Bank3,&EMI_InitStruct);       //USB

    EMI_StructInit(&EMI_InitStruct);
    EMI_InitStruct.EMI_Bank_IDCY = 0x1;
    EMI_InitStruct.EMI_Bank_WSTRD = 0x3;
    EMI_InitStruct.EMI_Bank_WSTWR = 0x3;
    EMI_InitStruct.EMI_Bank_WSTROEN = 0x1;
    EMI_InitStruct.EMI_Bank_WSTWEN = 0x01;
    EMI_InitStruct.EMI_Bank_MemWidth = EMI_Width_HalfWord;
    EMI_InitStruct.EMI_Bank_WriteProtection = EMI_Bank_NonWriteProtect;
    //EMI_InitStruct.EMI_Burst_and_PageModeRead_Selection = EMI_NormalMode;
    EMI_InitStruct.EMI_PageModeRead_Selection = EMI_NormalMode;
    EMI_Init (EMI_Bank2,&EMI_InitStruct);       //SRAM
}