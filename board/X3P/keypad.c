/**
  **************** (C) COPYRIGHT 2011 SCT Performance, LLC *********************
  * File Name          : keypad.c
  * Device / System    : SCT products using the "CommonCode" platform
  * Author             : Quyen Leba
  *
  * Version            : 1 
  * Date               : 05/02/2011
  * Description        : 
  *                    : 
  *
  *
  * History            : 05/02/2011 M. Davis
  *                    :   Add SCT Copyright header,
  *
  ******************************************************************************
  */

#include <91x_gpio.h>
#include <board/delays.h>
#include "init.h"
#include "system.h"

//------------------------------------------------------------------------------
// Engineer: Quyen Leba
//------------------------------------------------------------------------------
void keypad_init()
{
    GPIO_InitTypeDef GPIO_InitStructure;
    // Config SW1..6 @ P5[2..7]
    GPIO_StructInit(&GPIO_InitStructure);
    GPIO_InitStructure.GPIO_Pin =
        GPIO_Pin_2 | GPIO_Pin_3 | GPIO_Pin_4 | GPIO_Pin_5 | GPIO_Pin_6 |
        GPIO_Pin_7;
    GPIO_InitStructure.GPIO_Direction = GPIO_PinInput;
    GPIO_InitStructure.GPIO_Type = GPIO_Type_PushPull;
    GPIO_Init(GPIO5, &GPIO_InitStructure);
}

//------------------------------------------------------------------------------
// Engineer: Quyen Leba
//------------------------------------------------------------------------------
u8 keypad_poll()
{
    u8  key;
    u16 count;

    key = GPIO_Read(GPIO5);

    if ((key & 0xFC) != 0xFC)
    {
        delays(100,'u');
        key = GPIO_Read(GPIO5);
        
        if ((key & KEYUP) == KEYUP) key = KEYUP;
        else if ((key & KEYDOWN) == KEYDOWN) key = KEYDOWN;
        else if ((key & KEYLEFT) == KEYLEFT) key = KEYLEFT;
        else if ((key & KEYRIGHT) == KEYRIGHT) key = KEYRIGHT;
        else if ((key & KEYENTER) == KEYENTER) key = KEYENTER;
        else if ((key & KEYCANCEL) == KEYCANCEL) key = KEYCANCEL;
        else key = KEYNONE;
        
        if (key != KEYNONE)
        {
            count = 0;
            while((GPIO_Read(GPIO5) & 0xFC) == key)
            {
                delays(500,'u');
                //TODO: adjust this to get key hold 2 sec
                if (count >= 500) break;
                count++;
            }   
            //TODO: allow disable buzzer on keypress
            Buzzer(KEYGOOD);
        }
    }
    else
    {
        key = KEYNONE;
    }
    return key;
}

//------------------------------------------------------------------------------
// Input:   u8  key         key to wait for
//          u8  allowexit   allow exit/cancel/esc key to abort wait (TRUE/FALSE)
// Return   u8  keystatus   last key pressed
// Engineer: Quyen Leba
//------------------------------------------------------------------------------
u8 keypad_pollwait(u8 key, u8 allowexit)
{
    u8  i;

    while(1)
    {
        i = getKey();
        if (i == key)
        {
            break;
        }
        else if ((allowexit == TRUE) && (i == KEYCANCEL))
        {
            break;
        }
    }
    return i;
}
