/**
  **************** (C) COPYRIGHT 2011 SCT Performance, LLC *********************
  * File Name          : lcd.c
  * Device / System    : SCT products using the "CommonCode" platform
  * Author             : Quyen Leba
  *
  * Version            : 1 
  * Date               : 05/02/2011
  * Description        : 
  *                    : 
  *
  *
  * History            : 05/02/2011 M. Davis
  *                    :   Add SCT Copyright header,
  *
  ******************************************************************************
  */

#include <91x_type.h>
#include <91x_gpio.h>
#include "lcd.h"

//------------------------------------------------------------------------------
// Engineer: Quyen Leba
// Date: Jan 18, 2008
//------------------------------------------------------------------------------
void lcd_init()
{
    GPIO_InitTypeDef GPIO_InitStructure;
    
    // Config LCD Backlight
    GPIO_StructInit(&GPIO_InitStructure);
    GPIO_InitStructure.GPIO_Pin = GPIO_Pin_0;
    GPIO_InitStructure.GPIO_Direction = GPIO_PinOutput;
    GPIO_InitStructure.GPIO_Type = GPIO_Type_PushPull;
    GPIO_InitStructure.GPIO_Alternate = GPIO_OutputAlt1;
    GPIO_Init(GPIO1, &GPIO_InitStructure);

    LCD_ginit();
    LCD_ghw_cont_set(95);
    LCD_gselvp(0);
    LCD_gsetcpos(0,0);
}

//------------------------------------------------------------------------------------------------------------------------------------------------
// Engineer: Quyen Leba
// Date: Jan 18, 2008
//------------------------------------------------------------------------------------------------------------------------------------------------
void LCD_Backlight(u8 state)
{
    if (state == OFF)
    {
        GPIO_WriteBit(GPIO1,GPIO_Pin_0,Bit_RESET);
    }
    else
    {
        GPIO_WriteBit(GPIO1,GPIO_Pin_0,Bit_SET);
    }
}

//------------------------------------------------------------------------------------------------------------------------------------------------
// Draw a rectangle on LCD
//------------------------------------------------------------------------------------------------------------------------------------------------
void LCD_DrawRectangle(u32 x1, u32 y1, u32 x2, u32 y2)
{
    LCD_gsetpos(x1, y1);
    LCD_glineto(x2,y1);
    LCD_gsetpos(x1,y2);
    LCD_glineto(x2,y2);
    LCD_gsetpos(x1,y1);
    LCD_glineto(x1,y2);
    LCD_gsetpos(x2, y1);
    LCD_glineto(x2,y2);
}

//------------------------------------------------------------------------------------------------------------------------------------------------
// EOF
//------------------------------------------------------------------------------------------------------------------------------------------------
