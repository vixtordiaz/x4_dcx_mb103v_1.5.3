/**
  **************** (C) COPYRIGHT 2011 SCT Performance, LLC *********************
  * File Name          : usb_descriptors.c
  * Device / System    : SCT products using the "CommonCode" platform
  * Author             : Quyen Leba
  *
  * Version            : 1 
  * Date               : 05/02/2011
  * Description        : 
  *                    : 
  *
  *
  * History            : 05/02/2011 M. Davis
  *                    :   Add SCT Copyright header,
  *
  ******************************************************************************
  */

/******************** (C) COPYRIGHT 2008, 2009 SCT LLC *************************
* File Name          : usb_hw.c
* Device / System    : X3-Plus Tuner 
* Author             : J. Hess, R. Rusak, P. Downs, Q.Leba, D. Lin, M. Davis
* Version            : 
* Date               : 9/2/08
* Description        : X3-Plus USB struct var initialization
*
* Rev History, most recent first
* $History: /CommonCode/Mico/board/X3P/usb_descriptors.c $
*  
*  ****************** Version 1 ****************** 
*  User: qleba   Date: 2011-05-17   Time: 23:01:24-04:00 
*  Updated in: /CommonCode/Mico/board/X3P 
*  
*  ****************** Version 1 ****************** 
*  User: qleba   Date: 2010-11-01   Time: 12:21:09-04:00 
*  Updated in: /mico/board/X3P 
*  
*  ****************** Version 5 ****************** 
*  User: mdavis   Date: 2009-03-24   Time: 11:18:21-04:00 
*  Updated in: /X3-PLUS/X3P/board 
*  Revise ...FS_Config_Descriptor(s) and ...HS_Config_Descriptor(s) with 
*  correct 0x80 code in 'bmAttributes' element for bus-power, no wakeup. Bit 7 
*  of this element is reserved for USB 1.0 only, with a one in this bit 
*  indicating bus-powered. A one in bit 6 indicates self-powered operation for 
*  USB 1.1 and USB 2.0. Also revise  MaxPower element in these structs to 
*  indicate 400mA operating current. 
*  
*  ****************** Version 4 ****************** 
*  User: mdavis   Date: 2009-03-12   Time: 12:13:08-04:00 
*  Updated in: /X3-PLUS/X3P/board 
*  fix USB 1.1 download problem. 
*  
*  ****************** Version 3 ****************** 
*  User: mdavis   Date: 2009-03-09   Time: 17:05:54-04:00 
*  Updated in: /X3-PLUS/X3P/board 
*  Fix broken USB 1.1 operation. 
*  
*  ****************** Version 2 ****************** 
*  User: mdavis   Date: 2009-02-20   Time: 20:25:18-05:00 
*  Updated in: /X3-PLUS/X3P/board 
*  Applying standardized documentation headers to all source files, using the 
*  SourceGear Vault "Keyword Expansion" facility. 
*                      
*******************************************************************************/

#include "usb_descriptors.h"

const USB_DEVICE_DESCRIPTOR USB_FS_Device_Descriptor =
{
    USB_DEVICE_DESCRIPTOR_LEN,              // length
    0x01,                                   // bDescriptorType
    0x0200,                                 // Version 1.1 USB spec Word
    0x00,                                   // Class
    0x00,                                   // bDeviceSubClass
    0x00,                                   // bDeviceProtocol
    0x40,                                   // bMaxPacketSize0
    USB_VID,                                // idVendor
    USB_PID,                                // idProduct
    0x0001,                                 // bcdDevice
    0x01,                                   // iManufacturer
    0x02,                                   // iProduct
    0x00,                                   // iSerialNumber
    0x01,                                   // bNumConfigurations
};

const USB_CONFIGURATION_DESCRIPTOR USB_FS_Config_Descriptor =
{
    USB_CONFIGURATION_DESCRIPTOR_LEN,       // Length of the configuration Descriptor
    0x02,                                   // Configuration Descriptor
    USB_FS_CONFIG_LENGTH,                   // Total length of the configurations,interface and class/endpoint
    0x01,                                   // No. of interface supported
    0x01,                                   // Configuration Value
    0x00,                                   // Index of string descriptor
    0x80,                                   // power source... bus powered, no remote wake-up
    0xc0,                                   // 400 mA
};

const USB_INTERFACE_DESCRIPTOR USB_FS_Interface_Descriptor_Default =
{
    USB_INTERFACE_DESCRIPTOR_LEN,           //Length of standard interface descriptor
    0x04,                                   //Standard Interface Type
    0x00,                                   //No of Interface
    0x00,                                   //Alternate Setting Selector
    0x02,                                   //No of Endpoints
    0xFF,                                   //Interface Class
    0xFF,                                   //Interface Sub Class
    0xFF,                                   //Interface Protocol
    0x00,                                   //Index of interface string descriptor
};

const USB_INTERFACE_DESCRIPTOR USB_Interface_Descriptor_Default =
{
    USB_INTERFACE_DESCRIPTOR_LEN,           //Length of standard interface descriptor
    0x04,                                   //Standard Interface Type
    0x00,                                   //No of Interface
    0x00,                                   //Alternate Setting Selector
    0x02,                                   //No of Endpoints
    0x00,                                   //Interface Class
    0x00,                                   //Interface Sub Class
    0x00,                                   //Interface Protocol
    0x00,                                   //Index of interface string descriptor
};

const USB_INTERFACE_DESCRIPTOR USB_Interface_Descriptor_Alternate1 =
{
    USB_INTERFACE_DESCRIPTOR_LEN,           //Length of standard interface descriptor
    0x04,                                   //Standard Interface Type
    0x00,                                   //No of Interface
    0x01,                                   //Alternate Setting Selector
    0x02,                                   //No of Endpoints
    0x00,                                   //Interface Class
    0x00,                                   //Interface Sub Class
    0x00,                                   //Interface Protocol
    0x00,                                   //Index of interface string descriptor
};

const USB_INTERFACE_DESCRIPTOR USB_Interface_Descriptor_Alternate2 =
{
    USB_INTERFACE_DESCRIPTOR_LEN,           //Length of standard interface descriptor
    0x04,                                   //Standard Interface Type
    0x00,                                   //No of Interface
    0x02,                                   //Alternate Setting Selector
    0x02,                                   //No of Endpoints
    0x00,                                   //Interface Class
    0x00,                                   //Interface Sub Class
    0x00,                                   //Interface Protocol
    0x00,                                   //Index of interface string descriptor
};

const USB_DEVICE_DESCRIPTOR USB_HS_Device_Descriptor =
{
    USB_DEVICE_DESCRIPTOR_LEN,              // length
    0x01,                                   // bDescriptorType
    0x0200,                                 // Version 2.0 USB spec Word
    0x00,                                   // Class
    0x00,                                   // bDeviceSubClass
    0x00,                                   // bDeviceProtocol
    0x40,                                   // bMaxPacketSize0
    USB_VID,                                // idVendor
    USB_PID,                                // idProduct
    0x0001,                                 // bcdDevice
    0x01,                                   // iManufacturer
    0x02,                                   // iProduct
    0x00,                                   // iSerialNumber
    0x01,                                   // bNumConfigurations
};

const USB_CONFIGURATION_DESCRIPTOR USB_HS_Config_Descriptor =
{
    //stupid sizeof make it 10bytes
    USB_CONFIGURATION_DESCRIPTOR_LEN,       // Length of the configuration Descriptor
    0x02,                                   // Configuration Descriptor
    USB_HS_CONFIG_LENGTH,                   // Total length of the configurations,interface and class/endpoint
    0x01,                                   // No. of interface supported
    0x01,                                   // Configuration Value
    0x00,                                   // Index of string descriptor
    0x80,                                   // power source... bus powered, no remote wake-up
    0xc0,                                   // 400 mA
};

const USB_INTERFACE_DESCRIPTOR USB_HS_Interface_Descriptor_Default =
{
    USB_INTERFACE_DESCRIPTOR_LEN,           //Length of standard interface descriptor
    0x04,                                   //Standard Interface Type
    0x00,                                   //No of Interface
    0x00,                                   //Alternate Setting Selector
    0x02,                                   //No of Endpoints
    0xFF,                                   //Interface Class
    0xFF,                                   //Interface Sub Class
    0xFF,                                   //Interface Protocol
    0x00,                                   //Index of interface string descriptor
};

const USB_INTERFACE_DESCRIPTOR USB_HS_Interface_Descriptor_Alternate1 =
{
    USB_INTERFACE_DESCRIPTOR_LEN,           //Length of standard interface descriptor
    0x04,                                   //Standard Interface Type
    0x00,                                   //No of Interface
    0x01,                                   //Alternate Setting Selector
    0x02,                                   //No of Endpoints
    0x00,                                   //Interface Class
    0x00,                                   //Interface Sub Class
    0x00,                                   //Interface Protocol
    0x00,                                   //Index of interface string descriptor
};

const USB_INTERFACE_DESCRIPTOR USB_HS_Interface_Descriptor_Alternate2 =
{
    USB_INTERFACE_DESCRIPTOR_LEN,           //Length of standard interface descriptor
    0x04,                                   //Standard Interface Type
    0x00,                                   //No of Interface
    0x02,                                   //Alternate Setting Selector
    0x02,                                   //No of Endpoints
    0x00,                                   //Interface Class
    0x00,                                   //Interface Sub Class
    0x00,                                   //Interface Protocol
    0x00,                                   //Index of interface string descriptor
};

const USB_INTERFACE_DESCRIPTOR USB_HS_Interface_Descriptor_Alternate3 =
{
    USB_INTERFACE_DESCRIPTOR_LEN,           //Length of standard interface descriptor
    0x04,                                   //Standard Interface Type
    0x00,                                   //No of Interface
    0x03,                                   //Alternate Setting Selector
    0x02,                                   //No of Endpoints
    0x00,                                   //Interface Class
    0x00,                                   //Interface Sub Class
    0x00,                                   //Interface Protocol
    0x00,                                   //Index of interface string descriptor
};

const USB_ENDPOINT_DESCRIPTOR USB_EP2_IN_HS_BULK_64_Descriptor =
{
    USB_ENDPOINT_DESCRIPTOR_LEN,            //Length of Standard Endpoint Descriptor
    0x05,                                   //Standard Endpoint Type
    0x82,                                   //Enndpoint Address
    0x02,                                   //Endpoint Characteristic
    0x0200,                                 //Endpoint Max Packet Size
    0x01,                                   //Interval for Polling Endpoint for data transfer
};


// maxPacketSize is limited to 64 bytes in FS
const USB_ENDPOINT_DESCRIPTOR USB_EP2_IN_FS_BULK_64_Descriptor =
{
    USB_ENDPOINT_DESCRIPTOR_LEN,            //Length of Standard Endpoint Descriptor
    0x05,                                   //Standard Endpoint Type
    0x82,                                   //Enndpoint Address
    0x02,                                   //Endpoint Characteristic
    0x040,                                  //Endpoint Max Packet Size
    0x01,                                   //Interval for Polling Endpoint for data transfer
};

const USB_ENDPOINT_DESCRIPTOR USB_EP1_OUT_HS_BULK_64_Descriptor =
{
    USB_ENDPOINT_DESCRIPTOR_LEN,            //Length of Standard Endpoint Descriptor
    0x05,                                   //Standard Endpoint Type
    0x01,                                   //Enndpoint Address
    0x02,                                   //Endpoint Characteristic
    0x0200,                                 //Endpoint Max Packet Size
    0x01,                                   //Interval for Polling Endpoint for data transfer
};

// maxPacketSize is limited to 64 bytes in FS
const USB_ENDPOINT_DESCRIPTOR USB_EP1_OUT_FS_BULK_64_Descriptor =
{
    USB_ENDPOINT_DESCRIPTOR_LEN,            //Length of Standard Endpoint Descriptor
    0x05,                                   //Standard Endpoint Type
    0x01,                                   //Enndpoint Address
    0x02,                                   //Endpoint Characteristic
    0x040,                                 //Endpoint Max Packet Size
    0x01,                                   //Interval for Polling Endpoint for data transfer
};

const USB_ENDPOINT_DESCRIPTOR USB_Iso_In_Zero_Descriptor =
{
    USB_ENDPOINT_DESCRIPTOR_LEN,            //Length of Standard Endpoint Descriptor
    0x05,                                   //Standard Endpoint Type
    0x82,                                   //Enndpoint Address
    0x01,                                   //Endpoint Characteristic
    0x0000,                                 //Endpoint Max Packet Size
    0x01,                                   //Interval for Polling Endpoint for data transfer
};

const USB_ENDPOINT_DESCRIPTOR USB_Iso_In_1024_Descriptor =
{
    USB_ENDPOINT_DESCRIPTOR_LEN,            //Length of Standard Endpoint Descriptor
    0x05,                                   //Standard Endpoint Type
    0x82,                                   //Enndpoint Address
    0x01,                                   //Endpoint Characteristic
    0x0400,                                 //Endpoint Max Packet Size
    0x01,                                   //Interval for Polling Endpoint for data transfer
};

const USB_ENDPOINT_DESCRIPTOR USB_Iso_In_1020_Descriptor = //For Full speed only.
{
    USB_ENDPOINT_DESCRIPTOR_LEN,            //Length of Standard Endpoint Descriptor
    0x05,                                   //Standard Endpoint Type
    0x82,                                   //Enndpoint Address
    0x01,                                   //Endpoint Characteristic
    0x03FC,                                 //Endpoint Max Packet Size
    0x01,                                   //Interval for Polling Endpoint for data transfer
};

const USB_ENDPOINT_DESCRIPTOR USB_Iso_In_512_Descriptor =
{
    USB_ENDPOINT_DESCRIPTOR_LEN,            //Length of Standard Endpoint Descriptor
    0x05,                                   //Standard Endpoint Type
    0x82,                                   //Enndpoint Address
    0x01,                                   //Endpoint Characteristic
    0x0200,                                 //Endpoint Max Packet Size
    0x01,                                   //Interval for Polling Endpoint for data transfer
};

const USB_ENDPOINT_DESCRIPTOR USB_Iso_In_128_Descriptor =
{
    USB_ENDPOINT_DESCRIPTOR_LEN,            //Length of Standard Endpoint Descriptor
    0x05,                                   //Standard Endpoint Type
    0x82,                                   //Enndpoint Address
    0x01,                                   //Endpoint Characteristic
    0x0080,                                 //Endpoint Max Packet Size
    0x01,                                   //Interval for Polling Endpoint for data transfer
};

const USB_ENDPOINT_DESCRIPTOR USB_Int_In_Descriptor =
{
    USB_ENDPOINT_DESCRIPTOR_LEN,            //Length of Standard Endpoint Descriptor
    0x05,                                   //Standard Endpoint Type
    0x82,                                   //Set Int endpoint to 2 for ATAPI mode
    0x03,                                   //Endpoint Characteristic
    0x0010,                                 //Endpoint Max Packet Size  //0x0008
    0x0A,                                   //Interval for Polling Endpoint for data transfer
};

const USB_ENDPOINT_DESCRIPTOR USB_Bulk_In_512_Descriptor =
{
    USB_ENDPOINT_DESCRIPTOR_LEN,            //Length of Standard Endpoint Descriptor
    0x05,                                   //Standard Endpoint Type
    0x82,                                   //Set Int endpoint to 2 for ATAPI mode
    0x02,                                   //Endpoint Characteristic
    0x0200,                                 //Endpoint Max Packet Size
    0x10,                                   //Interval for Polling Endpoint for data transfer
};

const USB_ENDPOINT_DESCRIPTOR USB_Bulk_In_256_Descriptor =
{
    USB_ENDPOINT_DESCRIPTOR_LEN,            //Length of Standard Endpoint Descriptor
    0x05,                                   //Standard Endpoint Type
    0x82,                                   //Set Int endpoint to 2 for ATAPI mode
    0x02,                                   //Endpoint Characteristic
    0x0100,                                 //Endpoint Max Packet Size
    0x10,                                   //Interval for Polling Endpoint for data transfer
};

const USB_ENDPOINT_DESCRIPTOR USB_Bulk_In_64_Descriptor =
{
    USB_ENDPOINT_DESCRIPTOR_LEN,            //Length of Standard Endpoint Descriptor
    0x05,                                   //Standard Endpoint Type
    0x82,                                   //Set Int endpoint to 2 for ATAPI mode
    0x02,                                   //Endpoint Characteristic
    0x0040,                                 //Endpoint Max Packet Size
    0x10,                                   //Interval for Polling Endpoint for data transfer
};

const USB_STRING_DESCRIPTOR USB_Standard_Product_String =	     
{
    0x30,           // length                   
    0x03,           // descriptor type = string 
    'S' , 0x00,
    'C' , 0x00,
    'T' , 0x00,
    ' ' , 0x00,
    'D' , 0x00,
    'E' , 0x00,
    'V' , 0x00,
    'I' , 0x00,
    'C' , 0x00,
    'E' , 0x00,
    ' ' , 0x00,
    'v' , 0x00,
    '3' , 0x00,
    '.' , 0x00,
    '0' , 0x00,
};

const USB_STRING_DESCRIPTOR USB_Standard_Manufacturer_String =
{
    38,             // length                   
    0x03,           // descriptor type = constant string
    'S' , 0x00,
    'C' , 0x00,
    'T' , 0x00,
    ' ' , 0x00,
    'A' , 0x00,
    'c' , 0x00,
    'q' , 0x00,
    'u' , 0x00,
    'i' , 0x00,
    's' , 0x00,
    'i' , 0x00,
    't' , 0x00,
    'o' , 0x00,
    'n' , 0x00,
    ' ' , 0x00,
    'L' , 0x00,
    'L' , 0x00,
    'C' , 0x00,
};

const USB_STRING_ID USB_Standard_String_ID =
{
    0x04,
    0x03,
    0x09,
    0x04,
};																 

const u8 Test_Packet[] =
{
    //0x00, 0x00,
    //0x00, 0x80, // syn pattern
    0xc3,
    0x00, 0x00,
    0x00, 0x00,
    0x00, 0x00,
    0x00, 0x00,
    0x00, 0xaa,
    0xaa, 0xaa,
    0xaa, 0xaa,
    0xaa, 0xaa,
    0xaa, 0xee,  //aa*4
    0xee, 0xee,
    0xee, 0xee,
    0xee, 0xee,
    0xee, 0xfe,  //ee*4
    0xff, 0xff,
    0xff, 0xff,
    0xff, 0xff,
    0xff, 0xff,
    0xff, 0xff,  //FF*6
    0xff, 0x7f,
    0xbf, 0xdf,
    0xef, 0xf7,
    0xfb, 0xfd,
    0xfc, 0x7e,
    0xbf, 0xdf,
    0xef, 0xf7,
    0xfb, 0xfd,
    0x7e,
    0xb6, 0xce,  // crc   --6D73
    //0xff, 0xf7,
};

const USB_DEVICE_QUALIFIER USB_HS_Device_Qualifier_Info =
{
    USB_DEVICE_QUALIFIER_LEN,               //length of HS Device Descriptor
    0x06,                                   //HS Device Qualifier Type
    0x0200,                                 //USB 2.0 version
    0x00,                                   //Device class
    0x00,                                   //Device SubClass
    0x00,                                   //Device Protocol Code
    0x40,                                   //Maximum Packet SIze for other speed
    0x01,                                   //Number of Other speed configurations
    0x00,
};

const USB_DEVICE_QUALIFIER USB_FS_Device_Qualifier_Info =
{
    USB_DEVICE_QUALIFIER_LEN,               //length of HS Device Descriptor
    0x06,                                   //HS Device Qualifier Type
    0x0200,                                 //USB 2.0 version
    0x00,                                   //Device class
    0x00,                                   //Device SubClass
    0x00,                                   //Device Protocol Code
    0x40,                                   //Maximum Packet SIze for other speed
    0x01,                                   //Number of Other speed configurations
    0x00,
};

const USB_OTHER_SPEED_CONFIG_DESCRIPTOR USB_FS_Config_Descriptor_Info =
{
    USB_OTHER_SPEED_CONFIG_DESCRIPTOR_LEN,  // length of other speed configuration descriptor
    0x07,                                   // Other speed configuration Type
    USB_FS_CONFIG_LENGTH,                   // Total length
    0x01,                                   // No of interface supported
    0x01,                                   // Configuration Value
    0x00,                                   // Index of string descriptor
    0x80,                                   // power source... bus powered, no remote wake-up
    0xc0,                                   // 400 mA
};

const USB_OTHER_SPEED_CONFIG_DESCRIPTOR USB_HS_Config_Descriptor_Info =
{
    USB_OTHER_SPEED_CONFIG_DESCRIPTOR_LEN,  // length of other speed configuration descriptor
    0x07,                                   // Other speed configuration Type
    USB_HS_CONFIG_LENGTH,                   // Total length
    0x01,                                   // No of interface supported
    0x01,                                   // Configuration Value
    0x00,                                   // Index of string descriptor
    0x80,                                   // power source... bus powered, no remote wake-up
    0xc0,                                   // 400 mA
};
