/**
  **************** (C) COPYRIGHT 2011 SCT Performance, LLC *********************
  * File Name          : usb.c
  * Device / System    : SCT products using the "CommonCode" platform
  * Author             : Quyen Leba
  *
  * Version            : 1 
  * Date               : 05/02/2011
  * Description        : 
  *                    : 
  *
  *
  * History            : 05/02/2011 M. Davis
  *                    :   Add SCT Copyright header,
  *
  ******************************************************************************
  */

#include <string.h>
#include <91x_gpio.h>
#include <board/delays.h>
#include "usb_descriptors.h"
#include "generic.h"
#include "usb.h"

USB_TypeDef *USB_Struct;
USB_DEVICE  USB_Device;
u32         USB_MAX_PACKET_LENGTH = USB_FS_MAX_PACKET_LENGTH;   //TODO: set this correctly
u8          debugtask;
u8          enumcount;
u8          DeviceConfigValue;
u8          USB_Speed_Status;

//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
u32 usb_getchipid();
void usb_softreset();
void usb_unlockdevice();
u8 usb_getvbusstatus();
void usb_pullup_dp1k5(bool pullup);
void usb_enableep();
u16 usb_getinterruptstatus();

void usb_send_devicedescriptor(USB_SETUP_PACKET *setup_pkg);
void usb_send_configurationdescriptor(USB_SETUP_PACKET *setup_pkg);
void usb_send_otherspeeddescriptor(USB_SETUP_PACKET *setup_pkg);
void usb_send_stringid();
void usb_send_manufacturerid();
void usb_send_productstring();
void usb_send_qualifierdescriptor();
void usb_send_zeropacket();

void usb_getconfig();
void usb_setconfig(USB_SETUP_PACKET *setup_pkg);
void usb_getinterface(USB_SETUP_PACKET *setup_pkg);
void usb_setinterface(USB_SETUP_PACKET *setup_pkg);
void usb_clearfeature(USB_SETUP_PACKET *setup_pkg);
void usb_setfeature(USB_SETUP_PACKET *setup_pkg);
void usb_send_devicestatus();
void usb_send_interfacestatus();
void usb_send_epstatus();
void usb_syncframe(USB_SETUP_PACKET *setup_pkg);
void usb_getstatus(USB_SETUP_PACKET *setup_pkg);

void usb_isrreset();
void usb_ep0setup_isr_handler();

//------------------------------------------------------------------------------
// Engineer: Quyen Leba
// Date: Jan 23, 2008
//------------------------------------------------------------------------------
void usb_init()
{
    GPIO_InitTypeDef GPIO_InitStructure;

    USB_Speed_Status = USB_FS;
    USB_Struct = (USB_TypeDef *)USB_StartAddr;
    
    //Config external interrupt pin for USB, P3.4
    GPIO_StructInit(&GPIO_InitStructure);
    GPIO_InitStructure.GPIO_Pin = GPIO_Pin_4;
    GPIO_InitStructure.GPIO_Direction = GPIO_PinInput;
    GPIO_InitStructure.GPIO_Type = GPIO_Type_PushPull ;
    GPIO_Init (GPIO3, &GPIO_InitStructure);

    usb_softreset();
    
    //enable device
    USB_Struct->Address |= USB_ADDRESS_DEVEN;
    //TODO:may turn this off in suspend mode
    USB_Struct->Mode |= USB_MODE_CLKAON;
    //pull up DP with 1.5kohm
    USB_Struct->Mode |= USB_MODE_SOFTCT;
    //start with default address ZERO
    USB_Struct->Address &= 0x80;                //set address to 0

    u32 id = usb_getchipid();

    //config interrupt type
    USB_Struct->IntrConf = 0x56;                //intr: pulsed, active low
                                                //CDBGMOD:      ACK
                                                //CDBGMODIN:    ACK
                                                //CDBGMODOUT:   ACK, NYET, NAK
    //clear all interrupt flags
    USB_Struct->InterruptL = 0xFFFF;
    USB_Struct->InterruptH = 0x03FF;
    //enable only interrupt for bus reset
    USB_Struct->IntrEnableL = USB_INTRENA_IEBRST;
    //enable interrupt
    USB_Struct->Mode |= USB_MODE_GLINTENA;
}

//------------------------------------------------------------------------------
// Get USB ChipID
// Return:  u32 (should be 0x00158230)
// Engineer: Quyen Leba
// Date: Jan 23, 2008
//------------------------------------------------------------------------------
u32 usb_getchipid()
{
    u32 id;
    id = ((USB_Struct->ChipIDH) & 0x0FF)<<16;
    id |= USB_Struct->ChipIDL;
    return id;
}

//------------------------------------------------------------------------------
// Reset USB device
// Engineer: Quyen Leba
// Date: Nov 25, 2008
// VERY IMPORTANT: 500ms delay after this soft reset
//------------------------------------------------------------------------------
void usb_softreset()
{
    USB_Struct->Mode |= USB_MODE_SFRESET;
    USB_Struct->Mode &= ~USB_MODE_SFRESET;
    delays(500,'m');    //Quyen: this delay is crucial
}

//------------------------------------------------------------------------------
// When resume, issue this command before attempt to write to registers
// Engineer: Quyen Leba
// Date: Jan 23, 2008
//------------------------------------------------------------------------------
void usb_unlockdevice()
{
    USB_Struct->UnlockDevice = 0xAA37;
}

//------------------------------------------------------------------------------
// Get VBUS status to determine if USB is plugged in
// Return:  u8 status (SET, RESET)
// Engineer: Quyen Leba
// Date: Jan 23, 2008
//------------------------------------------------------------------------------
u8 usb_getvbusstatus()
{
    if ((USB_Struct->Mode & USB_MODE_VBUSSTAT) == USB_MODE_VBUSSTAT)
    {
        return SET;
    }
    else return RESET;
}

//------------------------------------------------------------------------------
// Enable or disable the 1.5k ohm resistor on DP pin
// Engineer: Quyen Leba
// Date: Nov 25, 2008
//------------------------------------------------------------------------------
void usb_pullup_dp1k5(bool pullup)
{
    if (pullup)
    {
        USB_Struct->Mode |= USB_MODE_SOFTCT;
    }
    else
    {
        USB_Struct->Mode &= ~USB_MODE_SOFTCT;
    }
}

//------------------------------------------------------------------------------
// Enable EP1 & EP2 (OUT & IN)
// Engineer: Quyen Leba
// Date: Jan 23, 2008
//------------------------------------------------------------------------------
void usb_enableep()
{
    USB_Struct->EpIndex = USB_EPIDX_EP1_IN;
    USB_Struct->EpMaxPacketSize = 0;
    USB_Struct->EpIndex = USB_EPIDX_EP1_OUT;
    USB_Struct->EpMaxPacketSize = 0;

    USB_Struct->EpIndex = USB_EPIDX_EP2_IN;
    USB_Struct->EpMaxPacketSize = 0;
    USB_Struct->EpIndex = USB_EPIDX_EP2_OUT;
    USB_Struct->EpMaxPacketSize = 0;

    if (USB_Speed_Status == USB_HS)
    {
        USB_Struct->EpIndex = USB_EPIDX_EP1_OUT;
        USB_Struct->EpMaxPacketSize = USB_HS_MAX_PACKET_LENGTH;
    }
    else
    {
        USB_Struct->EpIndex = USB_EPIDX_EP1_OUT;
        USB_Struct->EpMaxPacketSize = USB_FS_MAX_PACKET_LENGTH;
    }

    if (USB_Speed_Status == USB_HS)
    {
        USB_Struct->EpIndex = USB_EPIDX_EP2_IN;
        USB_Struct->EpMaxPacketSize = USB_HS_MAX_PACKET_LENGTH;
    }
    else
    {
        USB_Struct->EpIndex = USB_EPIDX_EP2_IN;
        USB_Struct->EpMaxPacketSize = USB_FS_MAX_PACKET_LENGTH;
    }

    USB_Struct->EpIndex = USB_EPIDX_EP1_IN;
    USB_Struct->EpType = USB_EPTYPE_BULK;
    USB_Struct->EpIndex = USB_EPIDX_EP1_OUT;
    USB_Struct->EpType = USB_EPTYPE_ENABLE | USB_EPTYPE_BULK;

    USB_Struct->EpIndex = USB_EPIDX_EP2_IN;
    USB_Struct->EpType = USB_EPTYPE_ENABLE | USB_EPTYPE_BULK | USB_EPTYPE_DBLBUF;

    USB_Struct->EpIndex = USB_EPIDX_EP2_OUT;
    USB_Struct->EpType = USB_EPTYPE_BULK;

    //USB_Struct->IntrEnableL |= USB_INTRENA_IEP1RX | USB_INTRENA_IEP2TX;
    //USB_Struct->IntrEnableL |= USB_INTRENA_IEP1RX | USB_INTRENA_IEP2TX;
}

//------------------------------------------------------------------------------
// Engineer: Quyen Leba
// Date: Jan 23, 2008
//------------------------------------------------------------------------------
void usb_tx(u8 *buffer, u32 length)
{
    s32 len = (s32)(length);
    u32 i;
    u16 *ptr = (u16*)buffer;

    USB_Struct->EpIndex = USB_EPIDX_EP2_IN;
    //USB_Struct->CtrlFunc = USB_CTRL_DSEN;

    while(len > 0)
    {
        if (len < USB_MAX_PACKET_LENGTH)
        {
            // wait > 200ns to read buffer status
            delays(USB_STATUS_READ_DELAY,'n');
            
            if (USB_Speed_Status == USB_HS)
            {
                // double buffer used for EP2 IN... HS - wait until both buffers are empty
                while(USB_Struct->BufferStatus & USB_BUFSTAT_BOTHFULL);       
            }
            else // USB_Speed_Status == USB_FS 
            {
                // double buffer used for EP2 IN... FS - wait until at least one buffer is empty
                while((USB_Struct->BufferStatus & USB_BUFSTAT_BOTHFULL) == USB_BUFSTAT_BOTHFULL);
            }

            // send the remainder of the packet 
            USB_Struct->BufferLength = (u16)(len);
            i=0;
            while(i<(u32)len)
            {
                USB_Struct->Data = *ptr;
                ptr++;
                i+=2;
            }
            
            // last data... leave the loop
            break;
        }
        
        else
        {
            delays(USB_STATUS_READ_DELAY,'n');    // wait > 200ns to read buffer status
            
            if (USB_Speed_Status == USB_HS)
            {
                // double buffer used for EP2 IN... HS - wait until both buffers are empty
                while(USB_Struct->BufferStatus & USB_BUFSTAT_BOTHFULL);       
            } 
            else // USB_Speed_Status == USB_FS
            {
                // double buffer used for EP2 IN... FS - wait until at least one buffer is empty
                while((USB_Struct->BufferStatus & USB_BUFSTAT_BOTHFULL) == USB_BUFSTAT_BOTHFULL);
            }

            // send a full packet
            USB_Struct->BufferLength = USB_MAX_PACKET_LENGTH;
            for(i=0;i<USB_MAX_PACKET_LENGTH/2;i++)
            {
                USB_Struct->Data = *ptr;
                ptr++;
            }
            len -= USB_MAX_PACKET_LENGTH;
            
        }
    }
}

//------------------------------------------------------------------------------
// Engineer: Quyen Leba
// Date: Jan 23, 2008
//------------------------------------------------------------------------------
u32 usb_rx(u8 *buffer, u32 length)
{
    u32 len;
    u32 i,count,timeout;
    u16 *ptr = (u16*)buffer;

    USB_Struct->EpIndex = USB_EPIDX_EP1_OUT;

    count = 0;
    while(1)
    {
        timeout = 250000;
        do
        {
            len = USB_Struct->BufferLength;
            //timeout--;
        }while((len == 0) && (timeout != 0));
        
        
        if (timeout == 0)                   //time out
        {
            return 0;
        }
        else if ((count + len) < length)    //still more data
        {
            for(i=0;i<len;i+=2)
            {
                *ptr = USB_Struct->Data;
                ptr++;
                count+=2;
            }
            //USB_Struct->CtrlFunc = USB_CTRL_CLBUF;  //new debug
            //USB_Struct->InterruptL = USB_INTR_EP1RX;
        }
        else                                //last packet
        {
            for(;count<length;)
            {
                *ptr = USB_Struct->Data;
                ptr++;
                count+=2;
            }
            //TODO: flush RX buffer
            //USB_Struct->CtrlFunc = USB_CTRL_CLBUF;  //new debug
            //USB_Struct->InterruptL = USB_INTR_EP1RX;
            return count;
        }
    }
}

//------------------------------------------------------------------------------
// Flush all USB buffer
// Engineer: Quyen Leba
//------------------------------------------------------------------------------
void usb_flush()
{
    USB_Struct->EpIndex = USB_EPIDX_EP1_OUT;
    USB_Struct->CtrlFunc = USB_CTRL_CLBUF;
    USB_Struct->CtrlFunc = USB_CTRL_CLBUF;
    USB_Struct->EpIndex = USB_EPIDX_EP2_OUT;
    USB_Struct->CtrlFunc = USB_CTRL_CLBUF;
    USB_Struct->CtrlFunc = USB_CTRL_CLBUF;
}

//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
u16 usb_getinterruptstatus()
{
    return USB_Struct->InterruptL;
}

//------------------------------------------------------------------------------
// Send full-speed device descriptor to HOST
// Input:   USB_SETUP_PACKET *setup_pkg (the SETUP packet)
//          USB_Speed_Status (global)
// Engineer: Quyen Leba
// Date: Jan 25, 2008
//------------------------------------------------------------------------------
void usb_send_devicedescriptor(USB_SETUP_PACKET *setup_pkg)
{
    u8  len,i;
    u16 *ptr;
    USB_DEVICE_DESCRIPTOR *usb_dev_descriptor;

    if (USB_Speed_Status == USB_HS)
    {
        usb_dev_descriptor = (USB_DEVICE_DESCRIPTOR*)&USB_HS_Device_Descriptor;
    }
    else
    {
        usb_dev_descriptor = (USB_DEVICE_DESCRIPTOR*)&USB_FS_Device_Descriptor;
    }

    //check for # of bytes HOST want
    if (setup_pkg->packet.length < usb_dev_descriptor->bLength)
    {
        len = setup_pkg->packet.length;
    }
    else
    {
        len = usb_dev_descriptor->bLength;
    }

    i=0;
    ptr = (u16*)usb_dev_descriptor;
    USB_Struct->EpIndex = USB_EPIDX_CONTROL_IN;
    USB_Struct->CtrlFunc = USB_CTRL_DSEN;
    USB_Struct->BufferLength = (u16)len;
    while(i<len)
    {
        USB_Struct->Data = *ptr;
        ptr++;
        i+=2;
    }
    while((USB_Struct->InterruptL & USB_INTR_EP0TX) == 0);      //Wait for In Token Ack
    USB_Struct->InterruptL = USB_INTR_EP0TX;

    USB_Struct->EpIndex = USB_EPIDX_CONTROL_OUT;
    USB_Struct->CtrlFunc = USB_CTRL_STATUS;
}

//------------------------------------------------------------------------------
// High-speed: Response to Config Descriptor request from HOST
// Input:   USB_SETUP_PACKET *setup_pkg (the SETUP packet)
//          USB_Speed_Status (global)
// Engineer: Quyen Leba
// Date: Jan 25, 2008
//------------------------------------------------------------------------------
void usb_send_configurationdescriptor(USB_SETUP_PACKET *setup_pkg)
{
    u16 len,i;
    u8  _descriptor[128];
    u16 *ptr;
    USB_CONFIGURATION_DESCRIPTOR    *usb_configuration_descriptor;

    if (USB_Speed_Status == USB_HS)
    {
        usb_configuration_descriptor = (USB_CONFIGURATION_DESCRIPTOR*)&USB_HS_Config_Descriptor;
    }
    else
    {
        usb_configuration_descriptor = (USB_CONFIGURATION_DESCRIPTOR*)&USB_FS_Config_Descriptor;
    }
    
    len = (u16)usb_configuration_descriptor->bLength;

    USB_Struct->EpIndex = USB_EPIDX_CONTROL_IN;
    USB_Struct->CtrlFunc = USB_CTRL_DSEN;

    //Send only Configuration descriptor
    if(setup_pkg->packet.length <= len)
    {
        
        ptr = (u16*)&USB_HS_Config_Descriptor;	//set pointer to configuration descriptor
        USB_Struct->BufferLength = setup_pkg->packet.length;

		for(i=0; i<setup_pkg->packet.length; i+=2)
        {
            USB_Struct->Data = *ptr;
            ptr++;
        }

        while((USB_Struct->InterruptL & USB_INTR_EP0TX) == 0);    //Wait for In Token Ack
        USB_Struct->InterruptL = USB_INTR_EP0TX;
        
        USB_Struct->EpIndex = USB_EPIDX_CONTROL_OUT;
        USB_Struct->CtrlFunc = USB_CTRL_STATUS;
    }
    //send Configuration, Interface and Endpoint Descriptor
    else
    {
        if(setup_pkg->packet.length >= 32)
        {
            len = 0;
            if (USB_Speed_Status == USB_HS)
            {
                memcpy((void*)&_descriptor[len],(u8*)&USB_HS_Config_Descriptor,USB_HS_Config_Descriptor.bLength);
                len+=(u16)USB_HS_Config_Descriptor.bLength;

                memcpy((void*)&_descriptor[len],(u8*)&USB_HS_Interface_Descriptor_Default,USB_HS_Interface_Descriptor_Default.bLength);
                len+=(u16)USB_HS_Interface_Descriptor_Default.bLength;

                memcpy((void*)&_descriptor[len],(u8*)&USB_EP1_OUT_HS_BULK_64_Descriptor,USB_EP1_OUT_HS_BULK_64_Descriptor.bLength);
                len+=(u16)USB_EP1_OUT_HS_BULK_64_Descriptor.bLength;

                memcpy((void*)&_descriptor[len],(u8*)&USB_EP2_IN_HS_BULK_64_Descriptor,USB_EP2_IN_HS_BULK_64_Descriptor.bLength);
                len+=(u16)USB_EP2_IN_HS_BULK_64_Descriptor.bLength;
            }
            else
            {
                memcpy((void*)&_descriptor[len],(u8*)&USB_HS_Config_Descriptor,USB_FS_Config_Descriptor.bLength);
                len+=(u16)USB_FS_Config_Descriptor.bLength;
    
                memcpy((void*)&_descriptor[len],(u8*)&USB_FS_Interface_Descriptor_Default,USB_FS_Interface_Descriptor_Default.bLength);
                len+=(u16)USB_FS_Interface_Descriptor_Default.bLength;

                memcpy((void*)&_descriptor[len],(u8*)&USB_EP1_OUT_FS_BULK_64_Descriptor,USB_EP1_OUT_FS_BULK_64_Descriptor.bLength);
                len+=(u16)USB_EP1_OUT_FS_BULK_64_Descriptor.bLength;

                memcpy((void*)&_descriptor[len],(u8*)&USB_EP2_IN_FS_BULK_64_Descriptor,USB_EP2_IN_FS_BULK_64_Descriptor.bLength);
                len+=(u16)USB_EP2_IN_FS_BULK_64_Descriptor.bLength;
            }
            
            ptr = (u16*)_descriptor;	//set pointer to configuration descriptor
            USB_Struct->BufferLength = len;

            for(i=0; i<len; i+=2)
            {
                USB_Struct->Data = *ptr;
                ptr++;
            }

            while((USB_Struct->InterruptL & USB_INTR_EP0TX) == 0);    //Wait for In Token Ack
            USB_Struct->InterruptL = USB_INTR_EP0TX;
            
            USB_Struct->EpIndex = USB_EPIDX_CONTROL_OUT;
            USB_Struct->CtrlFunc = USB_CTRL_STATUS;
        }
    }
}

//------------------------------------------------------------------------------
// Send alternative speed configuration descriptor to HOST
// Input:   USB_SETUP_PACKET *setup_pkg (the SETUP packet)
//          USB_Speed_Status (global)
// Engineer: Quyen Leba
// Date: Jan 25, 2008
//------------------------------------------------------------------------------
void usb_send_otherspeeddescriptor(USB_SETUP_PACKET *setup_pkg)
{
    u8  len,i;
    u16 *ptr;
    USB_OTHER_SPEED_CONFIG_DESCRIPTOR *usb_other_speed_descriptor;

    if (USB_Speed_Status == USB_HS)
    {
        usb_other_speed_descriptor = (USB_OTHER_SPEED_CONFIG_DESCRIPTOR*)&USB_HS_Config_Descriptor_Info;
    }
    else
    {
        usb_other_speed_descriptor = (USB_OTHER_SPEED_CONFIG_DESCRIPTOR*)&USB_FS_Config_Descriptor_Info;
    }

    //check for # of bytes HOST want
    if (setup_pkg->packet.length < usb_other_speed_descriptor->bLength)
    {
        len = setup_pkg->packet.length;
    }
    else
    {
        len = usb_other_speed_descriptor->bLength;
    }

    i=0;
    ptr = (u16*)usb_other_speed_descriptor;
    USB_Struct->EpIndex = USB_EPIDX_CONTROL_IN;
    USB_Struct->CtrlFunc = USB_CTRL_DSEN;
    USB_Struct->BufferLength = (u16)len;
    while(i<len)
    {
        USB_Struct->Data = *ptr;
        ptr++;
        i+=2;
    }
    while((USB_Struct->InterruptL & USB_INTR_EP0TX) == 0);      //Wait for In Token Ack
    USB_Struct->InterruptL = USB_INTR_EP0TX;

    USB_Struct->EpIndex = USB_EPIDX_CONTROL_OUT;
    USB_Struct->CtrlFunc = USB_CTRL_STATUS;
}

//------------------------------------------------------------------------------
// Send standard string ID to HOST
// Engineer: Quyen Leba
// Date: Jan 25, 2008
//------------------------------------------------------------------------------
void usb_send_stringid()
{
    u8  i;
    u16 *ptr;

    i=0;
    ptr = (u16*)&USB_Standard_String_ID;
    USB_Struct->EpIndex = USB_EPIDX_CONTROL_IN;
    USB_Struct->CtrlFunc = USB_CTRL_DSEN;
    USB_Struct->BufferLength = (u16)USB_Standard_String_ID.bLength;

    while(i<USB_Standard_String_ID.bLength)
    {
        USB_Struct->Data = *ptr;
        ptr++;
        i+=2;
    }
    while((USB_Struct->InterruptL & USB_INTR_EP0TX) == 0);      //Wait for In Token Ack
    USB_Struct->InterruptL = USB_INTR_EP0TX;

    USB_Struct->EpIndex = USB_EPIDX_CONTROL_OUT;
    USB_Struct->CtrlFunc = USB_CTRL_STATUS;
}

//------------------------------------------------------------------------------
// Send manufacturer string to HOST, i.e. company name
// Engineer: Quyen Leba
// Date: Jan 25, 2008
//------------------------------------------------------------------------------
void usb_send_manufacturerid()
{
    u8  i;
    u16 *ptr;

    i=0;
    ptr = (u16*)&USB_Standard_Manufacturer_String;
    USB_Struct->EpIndex = USB_EPIDX_CONTROL_IN;
    USB_Struct->CtrlFunc = USB_CTRL_DSEN;
    USB_Struct->BufferLength = (u16)USB_Standard_Manufacturer_String.bLength;

    while(i<USB_Standard_Manufacturer_String.bLength)
    {
        USB_Struct->Data = *ptr;
        ptr++;
        i+=2;
    }
    while((USB_Struct->InterruptL & USB_INTR_EP0TX) == 0);      //Wait for In Token Ack
    USB_Struct->InterruptL = USB_INTR_EP0TX;

    USB_Struct->EpIndex = USB_EPIDX_CONTROL_OUT;
    USB_Struct->CtrlFunc = USB_CTRL_STATUS;
}

//------------------------------------------------------------------------------
// Send product string to HOST, i.e. device name, first thing show up when HOST
// seek a driver
// Engineer: Quyen Leba
// Date: Jan 25, 2008
//------------------------------------------------------------------------------
void usb_send_productstring()
{
    u8  i;
    u16 *ptr;

    i=0;
    ptr = (u16*)&USB_Standard_Product_String;
    USB_Struct->EpIndex = USB_EPIDX_CONTROL_IN;
    USB_Struct->CtrlFunc = USB_CTRL_DSEN;
    USB_Struct->BufferLength = (u16)USB_Standard_Product_String.bLength;

    while(i<USB_Standard_Product_String.bLength)
    {
        USB_Struct->Data = *ptr;
        ptr++;
        i+=2;
    }
    while((USB_Struct->InterruptL & USB_INTR_EP0TX) == 0);      //Wait for In Token Ack
    USB_Struct->InterruptL = USB_INTR_EP0TX;

    USB_Struct->EpIndex = USB_EPIDX_CONTROL_OUT;
    USB_Struct->CtrlFunc = USB_CTRL_STATUS;
}

//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
void usb_send_qualifierdescriptor()
{
}

//------------------------------------------------------------------------------
// Engineer: Quyen Leba
// Date: Jan 23, 2008
//------------------------------------------------------------------------------
void usb_send_zeropacket()
{
    delays(200,'n');    //wait 200ns for buffer status
    USB_Struct->EpIndex = USB_EPIDX_EP1_OUT;
    //while(USB_Struct->BufferStatus & USB_BUFSTAT_BOTHFULL);                 //double buffer used for EP2 IN
    USB_Struct->BufferLength = 0;
    //USB_Struct->CtrlFunc = USB_CTRL_VENP;
}

//------------------------------------------------------------------------------
// Send current device configuration (0: not configured) to HOST
// Engineer: Quyen Leba
// Date: Jan 25, 2008
//------------------------------------------------------------------------------
void usb_getconfig()
{
    USB_Struct->EpIndex = USB_EPIDX_CONTROL_IN;
    USB_Struct->CtrlFunc = USB_CTRL_DSEN;
    USB_Struct->BufferLength = 0x1;
    if(USB_Device.BITS.State == USB_CONFIGURED)
    {
        USB_Struct->Data = DeviceConfigValue;
    }
    else
    {
        USB_Struct->Data = 0;
        DeviceConfigValue = 0;
    }
    while((USB_Struct->InterruptL & USB_INTR_EP0TX) == 0);      //Wait for In Token Ack
    USB_Struct->InterruptL = USB_INTR_EP0TX;

    USB_Struct->EpIndex = USB_EPIDX_CONTROL_OUT;
    USB_Struct->CtrlFunc = USB_CTRL_STATUS;
}

//------------------------------------------------------------------------------
// 
// Engineer: Quyen Leba
// Date: Jan 25, 2008
//------------------------------------------------------------------------------
void usb_setconfig(USB_SETUP_PACKET *setup_pkg)
{
    if(USB_Device.BITS.State == USB_DEFAULT)
    {
        USB_Struct->EpIndex = USB_EPIDX_EP0SETUP;
        USB_Struct->CtrlFunc = USB_CTRL_STALL;
        return;
    }
    else if(USB_Device.BITS.State == USB_ADDRESSED)
    {
        if (setup_pkg->packet.value == 0x0001)
        {
            USB_Device.BITS.State = USB_CONFIGURED;
            DeviceConfigValue = 1;
            usb_enableep();
        }
        else
        {
            USB_Device.BITS.State = USB_CONFIGURED;
            DeviceConfigValue = 0;
        }
    }
    else    //configured
    {
        if (setup_pkg->packet.value == 0x0001)
        {
            DeviceConfigValue = 1;
            usb_enableep();
        }
        else
        {
            USB_Device.BITS.State = USB_ADDRESSED;
            DeviceConfigValue = 0;
        }
    }
    USB_Struct->EpIndex = USB_EPIDX_CONTROL_IN;
    USB_Struct->CtrlFunc = USB_CTRL_STATUS;
    USB_Struct->EpIndex = USB_EPIDX_CONTROL_OUT;
    USB_Struct->CtrlFunc = USB_CTRL_STALL;
}

//------------------------------------------------------------------------------
// Send current device interface to HOST (must be in CONFIGURED state)
// Engineer: Quyen Leba
// Date: Jan 25, 2008
//------------------------------------------------------------------------------
void usb_getinterface(USB_SETUP_PACKET *setup_pkg)
{
    if ((setup_pkg->packet.index != 0) || (USB_Device.BITS.State == USB_ADDRESSED))
    {
        USB_Struct->EpIndex = USB_EPIDX_EP0SETUP;
        USB_Struct->CtrlFunc = USB_CTRL_STALL;
    }
    else
    {
        USB_Struct->EpIndex = USB_EPIDX_CONTROL_IN;
        USB_Struct->CtrlFunc = USB_CTRL_DSEN;
        USB_Struct->BufferLength = 0x01;
        USB_Struct->Data = USB_Device.BITS.Alt_Interface;           //this value set by USB_SetInterface

        while((USB_Struct->InterruptL & USB_INTR_EP0TX) == 0);      //Wait for In Token Ack
        USB_Struct->InterruptL = USB_INTR_EP0TX;
    
        USB_Struct->EpIndex = USB_EPIDX_CONTROL_OUT;
        USB_Struct->CtrlFunc = USB_CTRL_STATUS;
    }
}

//------------------------------------------------------------------------------
// Set device interface by HOST request (must be in CONFIGURED state)
// Engineer: Quyen Leba
// Date: Jan 25, 2008
//------------------------------------------------------------------------------
void usb_setinterface(USB_SETUP_PACKET *setup_pkg)
{
    if (USB_Device.BITS.State == USB_ADDRESSED)
    {
        USB_Struct->EpIndex = USB_EPIDX_EP0SETUP;
        USB_Struct->CtrlFunc = USB_CTRL_STALL;
    }
    else
    {
        if (setup_pkg->packet.index == 0x00 && setup_pkg->packet.value == 0x0000)
            USB_Device.BITS.Alt_Interface = 0x00;       //default setting
		else if (setup_pkg->packet.index == 0x00 && setup_pkg->packet.value == 0x0002)
			USB_Device.BITS.Alt_Interface = 0x01; //Alternate setting1  
		else if (setup_pkg->packet.index == 0x00 && setup_pkg->packet.value == 0x0002)
			USB_Device.BITS.Alt_Interface = 0x02; //Alternate setting2 
		else//(setup_pkg->packet.index == 0x00 && setup_pkg->packet.value == 0x0003)
			USB_Device.BITS.Alt_Interface = 0x03; //Alternate setting3 

        usb_enableep();

        USB_Struct->EpIndex = USB_EPIDX_CONTROL_IN;
        USB_Struct->CtrlFunc = USB_CTRL_STATUS;
        USB_Struct->EpIndex = USB_EPIDX_CONTROL_OUT;
        USB_Struct->CtrlFunc = USB_CTRL_STALL;
    }
}

//------------------------------------------------------------------------------
// Engineer: Quyen Leba
// Date: Jan 25, 2008
//------------------------------------------------------------------------------
void usb_clearfeature(USB_SETUP_PACKET *setup_pkg)
{
    switch(USB_Device.BITS.State)
    {
      case USB_ADDRESSED:
        if (setup_pkg->packet.length != 0) break;

        if (setup_pkg->packet.index != 0)
        {
            USB_Struct->EpIndex = USB_EPIDX_CONTROL_IN;
            USB_Struct->CtrlFunc = USB_CTRL_STALL;
        }
        else
        {
            if (setup_pkg->packet.value == 0x0000)
            {
                USB_Device.BITS.Halt = 0;
                USB_Struct->EpIndex = USB_EPIDX_CONTROL_IN;
                USB_Struct->CtrlFunc = USB_CTRL_STATUS;
            }
            else if (setup_pkg->packet.value == 0x0001)
            {
                USB_Device.BITS.Remote_Wakeup = 0;
                USB_Struct->EpIndex = USB_EPIDX_CONTROL_IN;
                USB_Struct->CtrlFunc = USB_CTRL_STATUS;
            }
        }
        break;
      case USB_CONFIGURED:
        if (setup_pkg->packet.length != 0) break;

        if (setup_pkg->packet.value == 0)
        {
            switch(setup_pkg->packet.index)
            {
              case 0x0080:
                USB_Struct->EpIndex = USB_EPIDX_CONTROL_IN;
                break;
              case 0x0000:
                USB_Struct->EpIndex = USB_EPIDX_CONTROL_OUT;
                break;
              case 0x0081:
                USB_Struct->EpIndex = USB_EPIDX_EP1_IN;
                break;
              case 0x0001:
                USB_Struct->EpIndex = USB_EPIDX_EP1_OUT;
                break;
              case 0x0082:
                USB_Struct->EpIndex = USB_EPIDX_EP2_IN;
                break;
              case 0x0002:
                USB_Struct->EpIndex = USB_EPIDX_EP2_OUT;
                break;
              case 0x0083:
                USB_Struct->EpIndex = USB_EPIDX_EP3_IN;
                break;
              case 0x0003:
                USB_Struct->EpIndex = USB_EPIDX_EP3_OUT;
                break;
              case 0x0084:
                USB_Struct->EpIndex = USB_EPIDX_EP4_IN;
                break;
              case 0x0004:
                USB_Struct->EpIndex = USB_EPIDX_EP4_OUT;
                break;
              case 0x0085:
                USB_Struct->EpIndex = USB_EPIDX_EP5_IN;
                break;
              case 0x0005:
                USB_Struct->EpIndex = USB_EPIDX_EP5_OUT;
                break;
              case 0x0086:
                USB_Struct->EpIndex = USB_EPIDX_EP6_IN;
                break;
              case 0x0006:
                USB_Struct->EpIndex = USB_EPIDX_EP6_OUT;
                break;
              case 0x0087:
                USB_Struct->EpIndex = USB_EPIDX_EP7_IN;
                break;
              case 0x0007:
                USB_Struct->EpIndex = USB_EPIDX_EP7_OUT;
                break;
              default:
                break;
            }
            //TODO: disable FIFO
            USB_Struct->EpType &= ~USB_EPTYPE_ENABLE;
            USB_Struct->EpType |= USB_EPTYPE_ENABLE;

            USB_Struct->CtrlFunc &= ~USB_CTRL_STALL;
            //TODO: reset my stall-flag

            USB_Struct->EpIndex = USB_EPIDX_CONTROL_IN;
            USB_Struct->CtrlFunc = USB_CTRL_STATUS;
        }
        else if (setup_pkg->packet.value == 0x0001)
        {
            USB_Device.BITS.Remote_Wakeup = 0;
            USB_Struct->EpIndex = USB_EPIDX_CONTROL_IN;
            USB_Struct->CtrlFunc = USB_CTRL_STATUS;
        }
        break;
      default:
        break;
    }
}

//------------------------------------------------------------------------------
// Engineer: Quyen Leba
// Date: Jan 25, 2008
//------------------------------------------------------------------------------
void usb_setfeature(USB_SETUP_PACKET *setup_pkg)
{
    switch(USB_Device.BITS.State)
    {
      case USB_DEFAULT:
        if (setup_pkg->packet.length != 0) break;

        if (setup_pkg->packet.value == USB_TESTMODE)    //TODO: not done yet
        {
            switch(setup_pkg->packet.index)
            {
              case USB_TEST_J_w:
                USB_Struct->EpIndex = USB_EPIDX_CONTROL_IN;
                USB_Struct->CtrlFunc = USB_CTRL_STATUS;
                USB_Struct->TestMode |= USB_TEST_FORCEHS;
                USB_Struct->TestMode |= USB_TEST_JSTATE;
                break;
              case USB_TEST_K_w:
                USB_Struct->EpIndex = USB_EPIDX_CONTROL_IN;
                USB_Struct->CtrlFunc = USB_CTRL_STATUS;
                USB_Struct->TestMode |= USB_TEST_FORCEHS;
                USB_Struct->TestMode |= USB_TEST_KSTATE;
                break;
              case USB_TEST_SE0_NAK_w:
                USB_Struct->EpIndex = USB_EPIDX_CONTROL_IN;
                USB_Struct->CtrlFunc = USB_CTRL_STATUS;
                USB_Struct->TestMode |= USB_TEST_FORCEHS;
                USB_Struct->TestMode |= USB_TEST_SE0_NAK;
                break;
              case USB_TEST_PACKET_w:
                USB_Struct->EpIndex = USB_EPIDX_CONTROL_IN;
                USB_Struct->CtrlFunc = USB_CTRL_STATUS;
                USB_Struct->CtrlFunc = USB_CTRL_DSEN;

                u8 i;
                USB_Struct->BufferLength = 56;
                for(i=0;i<28;i++)
                {
                    USB_Struct->Data = 0xAA55;
                }
                USB_Struct->TestMode |= USB_TEST_FORCEHS;
                USB_Struct->TestMode |= USB_TEST_PRBS;  //!?!
                break;
              case USB_TEST_FORCE_ENABLE_w:
                USB_Struct->EpIndex = USB_EPIDX_CONTROL_OUT;
                USB_Struct->CtrlFunc = USB_CTRL_STATUS;
                break;
              default:
                break;
            }
        }
        else
        {
            USB_Struct->EpIndex = USB_EPIDX_CONTROL_IN;
            USB_Struct->CtrlFunc = USB_CTRL_STALL;
        }
      case USB_ADDRESSED:   //not done
        if (setup_pkg->packet.length != 0) break;

        if ((setup_pkg->packet.index & 0xFF00) != 0)
        {
            USB_Struct->EpIndex = USB_EPIDX_CONTROL_IN;
            USB_Struct->CtrlFunc = USB_CTRL_STALL;
        }
        break;
    }

    switch(setup_pkg->packet.value)
    {
      case USB_TESTMODE:    //TODO
        break;
      case USB_EP_HALT:
        break;
      case USB_DEVICE_REMOTE_WAKEUP:
        break;
        
    }
}

//------------------------------------------------------------------------------
// Engineer: Quyen Leba
// Date: Jan 25, 2008
//------------------------------------------------------------------------------
void usb_send_devicestatus()
{
    u8 status = 0;
	status = USB_Device.BITS.Remote_Wakeup;
	status <<= 1;
	status |= USB_Device.BITS.Self_Powered;

    USB_Struct->EpIndex = USB_EPIDX_CONTROL_IN;
    USB_Struct->CtrlFunc = USB_CTRL_DSEN;
    USB_Struct->BufferLength = 2;
    USB_Struct->Data = status;
}

//------------------------------------------------------------------------------
// Engineer: Quyen Leba
// Date: Jan 25, 2008
//------------------------------------------------------------------------------
void usb_send_interfacestatus()
{
    USB_Struct->EpIndex = USB_EPIDX_CONTROL_IN;
    USB_Struct->CtrlFunc = USB_CTRL_DSEN;
    USB_Struct->BufferLength = 2;
    USB_Struct->Data = 0;
}

//------------------------------------------------------------------------------
// Engineer: Quyen Leba
// Date: Jan 25, 2008
//------------------------------------------------------------------------------
void usb_send_epstatus()
{
    u8  status;

    status = USB_Device.BITS.Halt;
    
    USB_Struct->EpIndex = USB_EPIDX_CONTROL_IN;
    USB_Struct->CtrlFunc = USB_CTRL_DSEN;
    USB_Struct->BufferLength = 2;
    USB_Struct->Data = status;
}

//------------------------------------------------------------------------------
// Engineer: Quyen Leba
// Date: Jan 25, 2008
//------------------------------------------------------------------------------
void usb_syncframe(USB_SETUP_PACKET *setup_pkg)
{
    if (setup_pkg->packet.index == 0)
    {
        USB_Struct->EpIndex = USB_EPIDX_EP0SETUP;
        USB_Struct->CtrlFunc = USB_CTRL_STALL;
    }
}

//------------------------------------------------------------------------------
// Engineer: Quyen Leba
// Date: Jan 25, 2008
//------------------------------------------------------------------------------
void usb_getstatus(USB_SETUP_PACKET *setup_pkg)
{
    if(USB_Device.BITS.State == USB_DEFAULT)
    {
        USB_Struct->EpIndex = USB_EPIDX_EP0SETUP;
        USB_Struct->CtrlFunc = USB_CTRL_STALL;
    }
    else
    {
        if (USB_Device.BITS.State == USB_ADDRESSED)
        {
            if ((setup_pkg->packet.req_type == 0x81) || 
               ((setup_pkg->packet.req_type == 0x82) && (setup_pkg->packet.index != 0x0000)))
            {
                USB_Struct->EpIndex = USB_EPIDX_EP0SETUP;
                USB_Struct->CtrlFunc = USB_CTRL_STALL;
            }
            else
            {
                if(setup_pkg->packet.req_type == 0x80) usb_send_devicestatus();
                else if(setup_pkg->packet.req_type == 0x81) usb_send_interfacestatus();
                else if(setup_pkg->packet.req_type == 0x82) usb_send_epstatus();
            }
        }
        else
        {
            if(setup_pkg->packet.req_type == 0x80) usb_send_devicestatus();
            else if(setup_pkg->packet.req_type == 0x81) usb_send_interfacestatus();
            else if(setup_pkg->packet.req_type == 0x82) usb_send_epstatus();
            //else if(setup_pkg->packet.req_type == 0x21) USB_GetCBICommand();
        }
    }
}

//------------------------------------------------------------------------------
// Handle USB reset request for enumeration
// Engineer: Quyen Leba
// Date: Jan 24, 2008
//------------------------------------------------------------------------------
void usb_isrreset()
{
    USB_Speed_Status = USB_FS;

    USB_Struct->Address = 0x80;
    USB_Struct->IntrConf = 0x56;
    //USB_Struct->Mode &= ~USB_MODE_SOFTCT;
    //USB_Struct->Mode |= USB_MODE_SOFTCT;

    /*
    USB_Struct->EpIndex = USB_EPIDX_SETUP;
    //USB_Struct->EpType &= ~USB_EPTYPE_ENABLE;
    USB_Struct->EpType = USB_EPTYPE_ENABLE;// | USB_EPTYPE_BULK;
    //USB_Struct->EpMaxPacketSize = 0x40;

    USB_Struct->EpIndex = USB_EPIDX_CONTROL_IN;
    USB_Struct->EpType = USB_EPTYPE_ENABLE;// | USB_EPTYPE_BULK;
    //USB_Struct->EpMaxPacketSize = 0x40;

    USB_Struct->EpIndex = USB_EPIDX_CONTROL_OUT;
    USB_Struct->EpType = USB_EPTYPE_ENABLE | USB_EPTYPE_BULK;
    //USB_Struct->EpMaxPacketSize = 0x40;
    */

    // enable required interrupts for enumeration
    USB_Struct->IntrEnableL = USB_INTRENA_IEBRST | USB_INTRENA_IERESM | USB_INTRENA_IESUSP | USB_INTRENA_IEHS_STA | \
                                USB_INTRENA_IEP0SETUP | USB_INTRENA_IEPCTRLTX | USB_INTRENA_IEPCTRLRX;
}

//------------------------------------------------------------------------------
// Handle all USB SETUP packets, each packet should be 8 bytes
// Engineer: Quyen Leba
// Date: Jan 24, 2008
//------------------------------------------------------------------------------
void usb_ep0setup_isr_handler()
{
    u16 rxcount;
    USB_SETUP_PACKET setup_pkg;
    
    USB_Struct->EpIndex = USB_EPIDX_SETUP;
    rxcount = (u16)USB_Struct->BufferLength;

    if (rxcount >= 8)
    {
        setup_pkg.rawdata.s[0] = USB_Struct->Data;
        setup_pkg.rawdata.s[1] = USB_Struct->Data;
        setup_pkg.rawdata.s[2] = USB_Struct->Data;
        setup_pkg.rawdata.s[3] = USB_Struct->Data;

        if ((setup_pkg.packet.req_type & 0x7F) == 0x43)
        {
            //debug
            USB_Struct->EpIndex = USB_EPIDX_CONTROL_OUT;
            USB_Struct->CtrlFunc = USB_CTRL_STATUS;
            //process vendor specific commands
            //TODOQ: this is our old usb commands USB_CMDS(&setup_pkg);
            USB_CMDS(&setup_pkg);
            /*
            USB_Struct->InterruptL = USB_INTR_EP1RX;
            USB_Struct->InterruptL = USB_INTR_EP1TX;
            USB_Struct->InterruptL = USB_INTR_EP2TX;
            USB_Struct->InterruptL = USB_INTR_EP2RX;
            USB_Struct->EpIndex = USB_EPIDX_CONTROL_OUT;
            USB_Struct->CtrlFunc = USB_CTRL_STATUS;
            */
            //USB_Flush();
            return;
        }
        
        switch (setup_pkg.packet.request)
        {
            //------------------------------------------------------------------
          case 0x06:    //descriptors
            switch (setup_pkg.packet.value)
            {
              case 0x0100:  //get device descriptor
                usb_send_devicedescriptor(&setup_pkg);
                break;
              case 0x0200:  //get conf descriptor
                usb_send_configurationdescriptor(&setup_pkg);
                break;
              case 0x0300:  //send string ID
                usb_send_stringid();
                break;
              case 0x0301:  //send manufacturer ID
                usb_send_manufacturerid();
                break;
              case 0x0302:  //send product string
                usb_send_productstring();
                break;
              case 0x0600:  //send qualifier descriptor
                usb_send_qualifierdescriptor();
                break;
              case 0x0700:  //send other speed
                usb_send_otherspeeddescriptor(&setup_pkg);
                break;
              default:      //unknown request, stall CONTROL IN/OUT
                USB_Struct->EpIndex = USB_EPIDX_CONTROL_IN;
                USB_Struct->CtrlFunc = USB_CTRL_STALL;
                USB_Struct->EpIndex = USB_EPIDX_CONTROL_OUT;
                USB_Struct->CtrlFunc = USB_CTRL_STALL;
                //TODO: check if spec want to stall EP0SETUP
                break;
            }
            break;
            //------------------------------------------------------------------
          case 0x05:    //set address
            switch (USB_Device.BITS.State)
            {
              default:
              case USB_DEFAULT:
                USB_Device.BITS.State = USB_ADDRESSED;
                USB_Struct->Address = 0x80 | setup_pkg.packet.value;
                USB_Struct->EpIndex = USB_EPIDX_CONTROL_IN;
                USB_Struct->CtrlFunc = USB_CTRL_STATUS;
                break;
              case USB_CONFIGURED:
              case USB_ADDRESSED:
                if (setup_pkg.packet.value == 0) USB_Device.BITS.State = USB_DEFAULT;

                USB_Struct->Address = 0x80 | setup_pkg.packet.value;
                USB_Struct->EpIndex = USB_EPIDX_CONTROL_IN;
                USB_Struct->CtrlFunc = USB_CTRL_STATUS;
                break;
            }
            break;
            //------------------------------------------------------------------
          case 0x08:    //get configuration
            usb_getconfig();
            break;
            //------------------------------------------------------------------
          case 0x09:    //set configuration
            usb_setconfig(&setup_pkg);
            break;
            //------------------------------------------------------------------
          case 0x0A:    //get interface
            usb_getinterface(&setup_pkg);
            break;
            //------------------------------------------------------------------
          case 0x0B:    //set interface
            usb_setinterface(&setup_pkg);
            break;
            //------------------------------------------------------------------
          case 0x0C:    //sync frame
            usb_syncframe(&setup_pkg);
            break;
            //------------------------------------------------------------------
          case 0x01:    //clear feature
            usb_clearfeature(&setup_pkg);
            break;
            //------------------------------------------------------------------
          case 0x03:    //set feature
            usb_setfeature(&setup_pkg);
            break;
            //------------------------------------------------------------------
          case 0x00:    //get status
            usb_getstatus(&setup_pkg);
            break;
            //------------------------------------------------------------------
          default:
            USB_Struct->EpIndex = USB_EPIDX_CONTROL_IN;
            USB_Struct->CtrlFunc = USB_CTRL_STALL;
            USB_Struct->EpIndex = USB_EPIDX_CONTROL_OUT;
            USB_Struct->CtrlFunc = USB_CTRL_STALL;
            //TODO: check if spec want to stall EP0SETUP
            break;
        }
    }
}

//------------------------------------------------------------------------------
// Handle all USB internal interrupt request
// Engineer: Quyen Leba
// Date: Jan 23, 2008
//------------------------------------------------------------------------------
void usb_isr_handler()
{
    u32 intr_stat;

    intr_stat = (USB_Struct->InterruptH << 8);
    intr_stat |= USB_Struct->InterruptL;
    intr_stat &= USB_Struct->IntrEnableL;

    //SETUP packet arrival
    if (intr_stat & USB_INTR_EP0SETUP)
    {
        usb_ep0setup_isr_handler();
        USB_Struct->InterruptL = USB_INTR_EP0SETUP;
    }

    //a bus reset detected
    if (intr_stat & USB_INTR_BRESET)
    {
        usb_isrreset();
        USB_Struct->InterruptL = USB_INTR_BRESET;
    }

    if (intr_stat & USB_INTR_BRESET)
    {
        u8 xx = 0;
        xx = 1;
    }
    
    //device go to suspend mode
    if (intr_stat & USB_INTR_SUSP)
    {
        if (intr_stat & USB_INTR_RESUME)
        {
            USB_Struct->InterruptL = USB_INTR_SUSP | USB_INTR_RESUME;
        }
        else
        {
            //TODO:handle suspend
            USB_Struct->InterruptL = USB_INTR_SUSP;
        }
    }
    //device go to resume mode
    else if (intr_stat & USB_INTR_RESUME)
    {
        //TODO: handle resume
        usb_unlockdevice();
        USB_Struct->InterruptL = USB_INTR_RESUME;
    }

    //device go to high speed mode
    if (intr_stat & USB_INTR_HS_STAT)   //full->high speed
    {
        USB_Speed_Status = USB_HS;
        USB_Struct->InterruptL = USB_INTR_HS_STAT;
        USB_MAX_PACKET_LENGTH = USB_HS_MAX_PACKET_LENGTH;
    }

    //HOST send a packet through EP1
    if (intr_stat & USB_INTR_EP1RX)     //EP1 OUT
    {
        USB_Struct->InterruptL = USB_INTR_EP1RX;
    }

    //HOST receive a packet through EP2
    if (intr_stat & USB_INTR_EP2TX)     //EP2 IN
    {
        USB_Struct->InterruptL = USB_INTR_EP2TX;
    }
}
