/**
  **************** (C) COPYRIGHT 2011 SCT Performance, LLC *********************
  * File Name          : timer.c
  * Device / System    : SCT products using the "CommonCode" platform
  * Author             : Quyen Leba
  *
  * Version            : 1 
  * Date               : 05/02/2011
  * Description        : 
  *                    : 
  *
  *
  * History            : 05/02/2011 M. Davis
  *                    :   Add SCT Copyright header,
  *
  ******************************************************************************
  */

#include <91x_tim.h>
#include <91x_rtc.h>
#include <board/timer.h>

//------------------------------------------------------------------------------
// Engineer: Quyen Leba
// Date: Jan 18, 2008
//------------------------------------------------------------------------------
void timer_init(void)
{
    TIM_InitTypeDef TIM_InitStructure;

    // Enable the clock for TIM0 and TIM1
    SCU_APBPeriphClockConfig(__TIM01, ENABLE);
    // Enable the clock for TIM2 and TIM3
    SCU_APBPeriphClockConfig(__TIM23, ENABLE);
    TIM_DeInit(TIM0);
    TIM_DeInit(TIM1);
    TIM_DeInit(TIM2);
    TIM_DeInit(TIM3);

    //TIM0 Structure Initialization
    TIM_StructInit(&TIM_InitStructure);

    //TIM0 Configuration in PWM Mode
    TIM_InitStructure.TIM_Mode = TIM_OCM_CHANNEL_1;//TIM_PWM;
    TIM_InitStructure.TIM_Clock_Source = TIM_CLK_APB;
    TIM_InitStructure.TIM_Prescaler = 0x00;
    TIM_InitStructure.TIM_Pulse_Level_1 = TIM_HIGH;
    TIM_InitStructure.TIM_Period_Level = TIM_LOW;
    TIM_InitStructure.TIM_Pulse_Length_1 = 10;
    //TIM_InitStructure.TIM_Pulse_Length_2 = 0;
    //TIM_InitStructure.TIM_Full_Period = 0;
    TIM_Init (TIM0, &TIM_InitStructure);

    //Start the counter of TIM0
    TIM_CounterCmd(TIM0, TIM_START);
}

//------------------------------------------------------------------------------
// Init timer1 for VPW & PWM (J1850)
// Engineer: Quyen Leba
// Date: Jan 28, 2008
//------------------------------------------------------------------------------
void timer1_init()
{
    TIM_InitTypeDef TIM_InitStructure;

    // Enable the clock for TIM2 and TIM3
    SCU_APBPeriphClockConfig(__TIM01, ENABLE);
    TIM_DeInit(TIM1);
    TIM_StructInit(&TIM_InitStructure);

    TIM_CounterCmd(TIM1, TIM_STOP);
    
    // TIM2 Configuration in PWM Mode
    TIM_InitStructure.TIM_Mode = TIM_OCM_CHANNEL_1;//TIM_PWM;
    TIM_InitStructure.TIM_Clock_Source = TIM_CLK_APB;
    TIM_InitStructure.TIM_Prescaler = 0;//1;
    TIM_InitStructure.TIM_OC1_Modes = TIM_TIMING;
    TIM_InitStructure.TIM_Pulse_Level_1 = TIM_HIGH;
    //TIM_InitStructure.TIM_Period_Level = TIM_LOW;
    TIM_InitStructure.TIM_Pulse_Length_1 = 0xFFF0;//0x10;
    //TIM_InitStructure.TIM_Full_Period = 0xFFF0;
    TIM_Init (TIM1, &TIM_InitStructure);

    TIM_CounterCmd(TIM1, TIM_START);
}

//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
u16 timer1_getcounter()
{
    return ((u16)TIM1->CNTR);
}

//------------------------------------------------------------------------------
// Init timer2 for Vpp power supply
// Engineer: Quyen Leba
// Date: Jan 28, 2008
//------------------------------------------------------------------------------
void timer2_init(void)
{
    TIM_InitTypeDef TIM_InitStructure;

    // Enable the clock for TIM2 and TIM3
    SCU_APBPeriphClockConfig(__TIM23, ENABLE);
    TIM_DeInit(TIM2);
    TIM_StructInit(&TIM_InitStructure);

    TIM_CounterCmd(TIM2, TIM_STOP);
    
    // TIM2 Configuration in PWM Mode
    TIM_InitStructure.TIM_Mode = TIM_PWM;
    TIM_InitStructure.TIM_Clock_Source = TIM_CLK_APB;
    TIM_InitStructure.TIM_Prescaler = 0x0;
    TIM_InitStructure.TIM_Pulse_Level_1 = TIM_HIGH;
    TIM_InitStructure.TIM_Period_Level = TIM_LOW;
    TIM_InitStructure.TIM_Pulse_Length_1 = 0x300;
    TIM_InitStructure.TIM_Full_Period = 0x3FF;
    TIM_Init (TIM2, &TIM_InitStructure);

    TIM_CounterCmd(TIM2, TIM_START);
}

//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
u16 timer2_getcounter()
{
    return ((u16)TIM2->CNTR);
}

//------------------------------------------------------------------------------
// Engineer: Quyen Leba
// Date: Jan 28, 2008
//------------------------------------------------------------------------------
void timer2_control(FunctionalState state)
{
    if (state == OFF)
    {
        TIM_CounterCmd(TIM2, TIM_STOP);
    }
    else
    {
        TIM_CounterCmd(TIM2, TIM_START);
    }
}

//------------------------------------------------------------------------------
// Engineer: Quyen Leba
// Date: Jan 28, 2008
//------------------------------------------------------------------------------
void timer2_pwm(u32 full_period, u32 high_period)
{
    TIM_InitTypeDef TIM_InitStructure;

    TIM_StructInit(&TIM_InitStructure);
    TIM_CounterCmd(TIM2, TIM_STOP);

    // TIM2 Configuration in PWM Mode
    TIM_InitStructure.TIM_Mode = TIM_PWM;
    TIM_InitStructure.TIM_Clock_Source = TIM_CLK_APB;
    TIM_InitStructure.TIM_Prescaler = 0x0;
    TIM_InitStructure.TIM_Pulse_Level_1 = TIM_HIGH;
    TIM_InitStructure.TIM_Period_Level = TIM_LOW;
    TIM_InitStructure.TIM_Pulse_Length_1 = high_period;
    TIM_InitStructure.TIM_Full_Period = full_period;
    TIM_Init (TIM2, &TIM_InitStructure);

    TIM_CounterCmd(TIM2, TIM_START);
}
