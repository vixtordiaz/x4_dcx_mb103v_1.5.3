/**
  **************** (C) COPYRIGHT 2011 SCT Performance, LLC *********************
  * File Name          : lcd.h
  * Device / System    : SCT products using the "CommonCode" platform
  * Author             : Quyen Leba
  *
  * Version            : 1 
  * Date               : 05/02/2011
  * Description        : 
  *                    : 
  *
  *
  * History            : 05/02/2011 M. Davis
  *                    :   Add SCT Copyright header,
  *
  ******************************************************************************
  */

#ifndef __LCD_H
#define __LCD_H

#include <gdisp.h>
#include <gdisphw.h>
#include <gdispcfg.h>
#include "lcd_helper.h"

void    lcd_init(void);
void    LCD_Backlight(u8 state);
//void    LCD_DrawRectangle(u32 x1, u32 y1, u32 x2, u32 y2);

#define LCD_ginit()                         ginit()
#define LCD_gclrvp()                        gclrvp()
#define LCD_gputs(s)                        gputs(s)
#define LCD_gsetcpos(x,y)                   gsetcpos(x,y)
#define LCD_gsetpos(x,y)                    gsetpos(x,y)
#define LCD_gselvp(n)                       gselvp(n)
#define LCD_gselfont(f)                     gselfont(f)
#define LCD_gsetmode(m)                     gsetmode(m)
#define LCD_ghw_cont_set(n)                 ghw_cont_set(n)
#define LCD_gputsym(x,y,sym)                gputsym(x,y,sym)
#define LCD_ginvertvp(xs,ys,xe,ye)          ginvertvp(xs,ys,xe,ye)
#define LCD_gmoveto(x,y)                    gmoveto(x,y)
#define LCD_glineto(x,y)                    glineto(x,y);
#define LCD_gputimage(filename,z,w)         gputimg(filename,z,w)
#define LCD_gclearareafast(c1,l1,c2,l2)     gclearareafast(c1,l1,c2,l2)
#define LCD_gputsrpos(x,y,string)           gputsrpos(x,y,string)
#define LCD_gputscpos(x,y,string)           gputscpos(x,y,string)
#define LCD_grectangle(xs,ys,xe,ye)         grectangle(xs,ys,xe,ye)
#define LCD_gfillvp(xs,ys,xe,ye,f)          gfillvp(xs,ys,xe,ye,f)
#define LCD_SetContrastFullRange(c)         ghw_cont_set(c)

#endif  //__LCD_H
