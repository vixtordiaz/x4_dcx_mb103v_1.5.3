/**
  **************** (C) COPYRIGHT 2011 SCT Performance, LLC *********************
  * File Name          : interrupt.c
  * Device / System    : SCT products using the "CommonCode" platform
  * Author             : Quyen Leba
  *
  * Version            : 1 
  * Date               : 05/02/2011
  * Description        : 
  *                    : 
  *
  *
  * History            : 05/02/2011 M. Davis
  *                    :   Add SCT Copyright header,
  *
  ******************************************************************************
  */

#include <91x_vic.h>
#include <91x_wiu.h>
#include <board/interrupt.h>

//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
#define WIU_USB     WIU_Line4
#define USB_Line    4

#define WIU_SW1     WIU_Line10
#define WIU_SW2     WIU_Line11
#define WIU_SW3     WIU_Line12
#define WIU_SW4     WIU_Line13
#define WIU_SW5     WIU_Line14
#define WIU_SW6     WIU_Line15
#define WIU_SWALL   WIU_SW1 | WIU_SW2 | WIU_SW3 | WIU_SW4 | WIU_SW5 | WIU_SW6
#define SW1_Line    10
#define SW2_Line    11
#define SW3_Line    12
#define SW4_Line    13
#define SW5_Line    14
#define SW6_Line    15

//------------------------------------------------------------------------------
// Engineer: Quyen Leba
// Date: Jan 24, 2008
//------------------------------------------------------------------------------
void usb_interrupt_init()
{
    WIU_InitTypeDef WIU_InitStructure;

    WIU_ClearITPendingBit(WIU_USB);

    WIU_InitStructure.WIU_Line = WIU_USB;
    WIU_InitStructure.WIU_TriggerEdge = WIU_FallingEdge;
    WIU_Init(&WIU_InitStructure);

    SCU_WakeUpLineConfig(USB_Line);
}

//------------------------------------------------------------------------------
// Engineer: Quyen Leba
// Date: Jan 18, 2008
//------------------------------------------------------------------------------
void interrupt_init()
{
    // Enable VIC clock
    SCU_AHBPeriphClockConfig(__VIC,ENABLE);
    VIC_DeInit();

    // Enable WIU clock
    SCU_APBPeriphClockConfig(__WIU, ENABLE);
    WIU_DeInit();
    
    // Enable the WIU
    WIU_Cmd(ENABLE);

    usb_interrupt_init();

    //VIC_DeInit();
    
    //USB
    // Configure the External interrupt group 0 priority
    VIC_Config(EXTIT0_ITLine, VIC_IRQ, 10);
    // Enable the External interrupt group 0
    VIC_ITCmd(EXTIT0_ITLine, ENABLE);
}

//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
void interrupt_ctrl(FunctionalState intrCtrl)
{
    WIU_Cmd(intrCtrl);
}
