/**
  **************** (C) COPYRIGHT 2011 SCT Performance, LLC *********************
  * File Name          : can.c
  * Device / System    : SCT products using the "CommonCode" platform
  * Author             : Quyen Leba
  *
  * Version            : 1 
  * Date               : 05/02/2011
  * Description        : 
  *                    : 
  *
  *
  * History            : 05/02/2011 M. Davis
  *                    :   Add SCT Copyright header,
  *
  ******************************************************************************
  */

#include <91x_gpio.h>
#include <common/genmanuf.h>
#include <common/statuscode.h>
#include <board/X3P/system.h>
#include <board/delays.h>
#include "can.h"

#if (SUPPORT_COMM_CAN)

enum
{
    CAN_MODE_HIGHSPEED,
    CAN_MODE_SILENT,
};

#define CANPORT         GPIO5
#define CAN_TX_PIN      GPIO_Pin_1
#define CAN_RX_PIN      GPIO_Pin_0
#define CAN_EN_PORT     GPIO1
#define CAN_EN_PIN      GPIO_Pin_4

CanRxMsg can_rxframe;
static CAN_MSGOBJ can_txmsgobj = CAN_TX_MSGOBJ;
static CAN_MSGOBJ can_rxmsgobj = CAN_RX_MSGOBJ_NORMAL;

//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
void can_xcr_mode(u8 mode)
{
    if(mode == CAN_MODE_HIGHSPEED)
    {
        // Clear to enable "HIGH SPEED" mode (ENABLE)
        GPIO_WriteBit(CAN_EN_PORT, CAN_EN_PIN, Bit_RESET);
    }
    else
    {
        // Set to enable "SILENT" mode (DISABLE)
        GPIO_WriteBit(CAN_EN_PORT, CAN_EN_PIN, Bit_SET);
    }
}

//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
void can_init(CAN_INITMODE initmode)
{
    GPIO_InitTypeDef    GPIO_InitStructure;
    CAN_InitTypeDef     CAN_InitStructure;

    // Config CAN_ENn pin
    GPIO_StructInit(&GPIO_InitStructure);
    GPIO_InitStructure.GPIO_Pin=CAN_EN_PIN;
    GPIO_InitStructure.GPIO_Direction=GPIO_PinOutput;
    GPIO_InitStructure.GPIO_Type=GPIO_Type_PushPull;
    GPIO_InitStructure.GPIO_Alternate=GPIO_OutputAlt1;
    //GPIO_InitStructure.GPIO_IPInputConnected=GPIO_IPInputConnected_Disable;
    GPIO_InitStructure.GPIO_IPConnected=GPIO_IPConnected_Disable;
    GPIO_Init(CAN_EN_PORT,&GPIO_InitStructure);

    // Set the GPIO1 port pin 4, enable HIGHSPEED CAN
    can_xcr_mode(CAN_MODE_HIGHSPEED);

    // Config CAN RX pin
    GPIO_StructInit(&GPIO_InitStructure); 
    GPIO_InitStructure.GPIO_Pin=CAN_RX_PIN;
    GPIO_InitStructure.GPIO_Direction=GPIO_PinInput;
    GPIO_InitStructure.GPIO_Type=GPIO_Type_PushPull;
    //GPIO_InitStructure.GPIO_IPConnected=GPIO_IPInputConnected_Enable;
    GPIO_InitStructure.GPIO_IPConnected=GPIO_IPConnected_Enable;
    GPIO_InitStructure.GPIO_Alternate=GPIO_InputAlt1;
    GPIO_Init(CANPORT,&GPIO_InitStructure);

    // Config CAN TX pin
    GPIO_StructInit(&GPIO_InitStructure);
    GPIO_InitStructure.GPIO_Pin=CAN_TX_PIN;
    GPIO_InitStructure.GPIO_Direction=GPIO_PinOutput;
    GPIO_InitStructure.GPIO_Type=GPIO_Type_PushPull;
    //GPIO_InitStructure.GPIO_IPConnected=GPIO_IPInputConnected_Enable;
    GPIO_InitStructure.GPIO_IPConnected=GPIO_IPConnected_Enable;
    GPIO_InitStructure.GPIO_Alternate=GPIO_OutputAlt2;
    GPIO_Init(CANPORT,&GPIO_InitStructure);

    // configure the message objects 
    CAN_SetUnusedAllMsgObj();
    //CAN_InvalidateAllMsgObj();
    CAN_SetTxMsgObj(CAN_TX_MSGOBJ, CAN_STD_ID, DISABLE);
    CAN_SetTxMsgObj(CAN_TX_EXTOBJ, CAN_EXT_ID, DISABLE);
    //!?! debug
    //CAN_SetRxMsgObj(CAN_RX_MSGOBJ_NORMAL, CAN_STD_ID, 0x728, 0x7FF, TRUE);
    //CAN_SetRxMsgObj(CAN_RX_MSGOBJ_NORMAL, CAN_STD_ID, 0x6A0, 0x7FF, TRUE);
    if (initmode == CAN_INITMODE_7EX)
    {
        CAN_SetRxMsgObj(CAN_RX_MSGOBJ_NORMAL, CAN_STD_ID, 0x7E0, 0x7FF, TRUE);
    }
    else //if (initmode == CAN_INITMODE_NORMAL)
    {
        CAN_SetRxMsgObj(CAN_RX_MSGOBJ_NORMAL, CAN_STD_ID, 0x5E0, 0x7FF, TRUE);
    }
    CAN_SetRxMsgObj(CAN_RX_MSGOBJ_EXT, CAN_EXT_ID, 0x18EFFA03, 0x18EFFA03, TRUE);
    CAN_SetRxMsgObj(CAN_RX_MSGOBJ_DATALOG, CAN_STD_ID, 0x5E8, 0x5FF, TRUE);
  
    // initialize the CAN at a standard bitrate, interrupts disabled
    CAN_InitStructure.CAN_ConfigParameters=0x0;
    CAN_InitStructure.CAN_Bitrate=CAN_BITRATE_500K;
    CAN_Init(&CAN_InitStructure);

    // initialize the CAN at a standard bitrate
    CAN_StructInit(&CAN_InitStructure);
    CAN_EnterInitMode(CAN_CR_CCE);
    // 500kb/s -> PCLK: 48Mhz/(BRP = 4) -> Bit time = 24tq,  TSEG1 = 16Tq, TSEG2 = 7Tq, SJW = 4 
    CAN_SetTiming(16, 7, 4, 4);     // (TSEG1, TSEG2, SJW, BRP)
    CAN_LeaveInitMode();
}

//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
void can_setTxMsgObj(CAN_MSGOBJ msgobj)
{
    can_txmsgobj = msgobj;
}

void can_setRxMsgObj(CAN_MSGOBJ msgobj)
{
    can_rxmsgobj = msgobj;
}

//------------------------------------------------------------------------------
// Send a CAN frame with standard ID
//------------------------------------------------------------------------------
u8 can_tx(CanTxMsg txframe)
{
    u32 TO = 0;

    //delays(300,'u');
    if(CAN_GetFlagStatus(CAN_SR_BOFF))
    {
        // Can Hardware has detected Bus Off State
        CAN_LeaveInitMode(); // Clear INIT bit in control register
        while(CAN_GetFlagStatus(CAN_SR_BOFF)) // Wait for Bus OFf to clear
        {  
            TO++;
            if(TO >= 10000)
            { 
                return S_FAIL; // Stuck for over 10 seconds FAIL
            }
            delays(1,'m');
        }
    }

    // Send message
    if(CAN_SendMessage(can_txmsgobj, (canmsg*)&txframe) == SUCCESS)
    {
        //while (CAN_WaitEndOfTx() != SUCCESS);
        // wait until end of transmission
        while(CAN_GetFlagStatus(CAN_SR_TXOK) == 0)
        {
            if (TO++ > 4000000)
            {
                CAN_ClearFlagStatus(CAN_SR_TXOK);
                return S_TIMEOUT;
            }
        }//while(CAN_GetFlagStatus(CAN_SR_TXOK) == 0)...
        CAN_ClearFlagStatus(CAN_SR_TXOK);
        return S_SUCCESS;
    }
    else
    {
        return S_FAIL;
    }
}

//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
u8 can_tx_s(CanTxMsg txframe)
{
    u32 TO = 0;

    if(CAN_GetFlagStatus(CAN_SR_BOFF))
    {
        // Can Hardware has detected Bus Off State
        CAN_LeaveInitMode(); // Clear INIT bit in control register
        while(CAN_GetFlagStatus(CAN_SR_BOFF)) // Wait for Bus OFf to clear
        {  
            TO++;
            if(TO >= 10000)
            { 
                return S_FAIL; // Stuck for over 10 seconds FAIL
            }
            delays(1,'m');   
        }
    }

    // Send message
    if(CAN_SendMessage(can_txmsgobj, (canmsg*)&txframe) == SUCCESS)
    {
        //while (CAN_WaitEndOfTx() != SUCCESS);
        // wait until end of transmission
        while(CAN_GetFlagStatus(CAN_SR_TXOK) == 0)
        {
            if (TO++ > 300000)
            {
                CAN_ClearFlagStatus(CAN_SR_TXOK);
                return S_TIMEOUT;
            }
        }//while(CAN_GetFlagStatus(CAN_SR_TXOK) == 0)...
        CAN_ClearFlagStatus(CAN_SR_TXOK);
        return S_SUCCESS;
    }
    else
    {
        return S_FAIL;
    }
}

//------------------------------------------------------------------------------
// Send a CAN frame with extended ID
//------------------------------------------------------------------------------
u8 can_tx_ext(CanTxMsg txframe)
{
    u32 TO = 0;

    if(CAN_GetFlagStatus(CAN_SR_BOFF))
    {
        // Can Hardware has detected Bus Off State
        CAN_LeaveInitMode(); // Clear INIT bit in control register
        while(CAN_GetFlagStatus(CAN_SR_BOFF)) // Wait for Bus OFf to clear
        {  
            TO++;
            if(TO >= 10000)
            { 
                return S_FAIL; // Stuck for over 10 seconds FAIL
            }
            delays(1,'m');   
        }
    }

    // Send message
    if(CAN_SendMessage(CAN_TX_EXTOBJ, (canmsg*)&txframe) == SUCCESS)
    {
        // wait until end of transmission
        while(CAN_GetFlagStatus(CAN_SR_TXOK) == 0)
        {
            if (TO++ > 300000)
            {
                CAN_ClearFlagStatus(CAN_SR_TXOK);
                return S_TIMEOUT;
            }
        }//while(CAN_GetFlagStatus(CAN_SR_TXOK) == 0)...
        CAN_ClearFlagStatus(CAN_SR_TXOK);
        return S_SUCCESS;
    }
    else
    {
        return S_FAIL;
    }
}

//------------------------------------------------------------------------------
// Receive a CAN frame with standard ID
//------------------------------------------------------------------------------
u8 can_rx(u32 timeout)
{
    u32 to = timeout;

    while(to)
    {
        if (CAN_ReceiveMessage(can_rxmsgobj, TRUE,
                               (CanTxMsg*)&can_rxframe) == SUCCESS)
        {
            return S_SUCCESS;
        }
        to--;
        delays(250,'n');
    }
    return S_TIMEOUT;
}

//------------------------------------------------------------------------------
// Receive a CAN frame with extended ID
//------------------------------------------------------------------------------
u8 can_rx_ext(u32 timeout)
{
    u32 to = timeout;

    while(to)
    {
        if (CAN_ReceiveMessage(CAN_RX_MSGOBJ_EXT, TRUE,
                               (CanTxMsg*)&can_rxframe) == SUCCESS)
        {
            return S_SUCCESS;
        }
        to--;
        delays(250,'n');
    }
    return S_TIMEOUT;
}

#endif  //#if (SUPPORT_COMM_CAN)