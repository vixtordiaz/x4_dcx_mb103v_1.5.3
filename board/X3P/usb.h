/**
  **************** (C) COPYRIGHT 2011 SCT Performance, LLC *********************
  * File Name          : usb.h
  * Device / System    : SCT products using the "CommonCode" platform
  * Author             : Quyen Leba
  *
  * Version            : 1 
  * Date               : 05/02/2011
  * Description        : 
  *                    : 
  *
  *
  * History            : 05/02/2011 M. Davis
  *                    :   Add SCT Copyright header,
  *
  ******************************************************************************
  */

#ifndef __USB_H
#define __USB_H

#include <arch/gentype.h>

#define USB_StartAddr       0x30100000;    //   SCT Driver

typedef struct __USB_DEVICE
{
    struct __USB_DEVICE_BITS
    {
        u8  Remote_Wakeup	:	1,
            Halt			:	1,
            Self_Powered	:	1,
            Endpt_Halt		:	1,
            Test_Mode		:	1,
            State			:	3;
        u8  CBI_Detected	:	1;
        u8  DMA_Test_Mode	:	8;
        u8  Alt_Interface	:	2,
            CBW_Data		:	1,
            DMA_State		:	3,
            Big_Endian		:	1;
	}BITS;
}USB_DEVICE;

typedef struct
{
    vu8     Address;            //00  USB device address & enable
        u8      __reserve1[7];
    vu16    EpMaxPacketSize;    //04
        u8      __reserve2[6];
    vu16    EpType;             //08
        u8      __reserve3[6];
    vu16    Mode;               //0C  power-down option, global interrupt enable, SoftConnect
        u8      __reserve4[6];
    vu8     IntrConf;           //10  intterupt source, trigger mode, output polarity
        u8      __reserve5[3];
    vu8     OTG;                //12
        u8      __reserve6[3];
    vu16    IntrEnableL;        //14
        u8      __reserve7[2];
    vu16    IntrEnableH;
        u8      __reserve8[2];
    vu16    InterruptL;         //18
        u8      __reserve9[2];
    vu16    InterruptH;
        u8      __reserveA[2];
    vu16    BufferLength;       //1C
        u8      __reserveB[2];
    vu8     BufferStatus;       //1E
        u8      __reserveC[3];
    vu16    Data;               //20
        u8      __reserveD[14];
    vu8     CtrlFunc;           //28
        u8      __reserveE[7];
    vu8     EpIndex;            //2C
        u8      __reserveF[7];
    vu8     DMACommand;         //30
        u8      __reserveG[7];
    vu16    DMATransferCounterL;//34
        u8      __reserveH[2];
    vu16    DMATransferCounterH;
        u8      __reserveI[2];
    vu16    DMAConf;            //38
        u8      __reserveJ[6];
    vu8     DMAHardware;        //3C
        u8      __reserveK[39];
    vu16    DMAIntrReason;      //50
        u8      __reserveL[6];
    vu16    DMAIntrEnable;      //54
        u8      __reserveM[6];
    vu8     DMAEp;              //58
        u8      __reserveN[23];
    vu16    DMABurstCounter;    //64
        u8      __reserveO[22];
    vu16    ChipIDL;            //70
        u8      __reserveP[2];
    vu16    ChipIDH;
        u8      __reserveQ[2];
    vu16    FrameNumber;        //74
        u8      __reserveR[6];
    vu16    Scratch;            //78
        u8      __reserveS[6];
    vu16    UnlockDevice;       //7C
        u8      __reserveT[14];
    vu8     TestMode;           //84
    
} USB_TypeDef;

typedef union _DATA8
{
    u8  u[8];
    u16 s[4];
    u32 l[2];
}DATA8;

typedef struct ____USB_SETUP_PKG
{
    u8  req_type;
    u8  request;
    u16 value;
    u16 index;
    u16 length;
}__USB_SETUP_PKG;

typedef union
{
    DATA8           rawdata;
    __USB_SETUP_PKG packet;
}USB_SETUP_PACKET;

#define USB_STATUS_READ_DELAY           300     // nS to wait from tx command to buffer-status read 
#define USB_HS_MAX_PACKET_LENGTH        0x200
#define USB_FS_MAX_PACKET_LENGTH        0x40

#define USB_ADDRESS_DEVEN               (1<<7)  //1 = device enable

#define USB_MODE_SOFTCT                 (1<<0)  //1 = enable connection of 1.5kohm pullup resistor on pin RPU to DP
#define USB_MODE_PWRON                  (1<<1)  //suspend pin out control
#define USB_MODE_WKUPCS                 (1<<2)  //1 = enable wakeup from suspend through a valid register read
#define USB_MODE_GLINTENA               (1<<3)  //1 = enable global interrupt
#define USB_MODE_SFRESET                (1<<4)  //write 1, followed by 0 -> software-initiated reset
#define USB_MODE_GOSUSP                 (1<<5)  //write 1, followed by 0 -> activate suspend mode
#define USB_MODE_SNDRSU                 (1<<6)  //write 1, followed by 0 -> generate a 10ms upstream resume signal
#define USB_MODE_CLKAON                 (1<<7)  //1 = always on 
#define USB_MODE_VBUSSTAT               (1<<8)
#define USB_MODE_DMACLKON               (1<<9)  //1 = on

#define USB_INTRCONF_INTLVL             (1<<1)  //0 = level, 1 = pulsed
#define USB_INTRCONF_INTPOL             (1<<0)  //0 = active low, 1 = active high

#define USB_INTRENA_IEP2TX              (1<<15) //1 = enable interrupt
#define USB_INTRENA_IEP2RX              (1<<14)
#define USB_INTRENA_IEP1TX              (1<<13)
#define USB_INTRENA_IEP1RX              (1<<12)
#define USB_INTRENA_IEP0TX              (1<<11)
#define USB_INTRENA_IEP0RX              (1<<10)
#define USB_INTRENA_IEPCTRLTX           (1<<11)
#define USB_INTRENA_IEPCTRLRX           (1<<10)
#define USB_INTRENA_IEP0SETUP           (1<<8)  //1 = enable intr for setup data received on ep0
#define USB_INTRENA_IEVBUS              (1<<7)  //VBUS sensing
#define USB_INTRENA_IEDMA               (1<<6)  //DMA Interrupt Reason register change detection
#define USB_INTRENA_IEHS_STA            (1<<5)  //highspeed status change
#define USB_INTRENA_IERESM              (1<<4)  //resume state
#define USB_INTRENA_IESUSP              (1<<3)  //suspend state
#define USB_INTRENA_IEPSOF              (1<<2)  //pseudo start of frame
#define USB_INTRENA_IESOF               (1<<1)  //start of frame
#define USB_INTRENA_IEBRST              (1<<0)  //bus reset

#define USB_EPDIR_IN                    1       //TX
#define USB_EPDIR_OUT                   0       //RX
#define USB_EPIDX_EP0SETUP              (1<<5)  //1 = SETUP buff, 0 = data buffer
#define USB_EPIDX_SETUP                 (USB_EPIDX_EP0SETUP)
#define USB_EPIDX_CONTROL_OUT           (USB_EPDIR_OUT)
#define USB_EPIDX_CONTROL_IN            (USB_EPDIR_IN)
#define USB_EPIDX_EP1_OUT               ((1<<1) | USB_EPDIR_OUT)
#define USB_EPIDX_EP1_IN                ((1<<1) | USB_EPDIR_IN)
#define USB_EPIDX_EP2_OUT               ((2<<1) | USB_EPDIR_OUT)
#define USB_EPIDX_EP2_IN                ((2<<1) | USB_EPDIR_IN)
#define USB_EPIDX_EP3_OUT               ((3<<1) | USB_EPDIR_OUT)
#define USB_EPIDX_EP3_IN                ((3<<1) | USB_EPDIR_IN)
#define USB_EPIDX_EP4_OUT               ((4<<1) | USB_EPDIR_OUT)
#define USB_EPIDX_EP4_IN                ((4<<1) | USB_EPDIR_IN)
#define USB_EPIDX_EP5_OUT               ((5<<1) | USB_EPDIR_OUT)
#define USB_EPIDX_EP5_IN                ((5<<1) | USB_EPDIR_IN)
#define USB_EPIDX_EP6_OUT               ((6<<1) | USB_EPDIR_OUT)
#define USB_EPIDX_EP6_IN                ((6<<1) | USB_EPDIR_IN)
#define USB_EPIDX_EP7_OUT               ((7<<1) | USB_EPDIR_OUT)
#define USB_EPIDX_EP7_IN                ((7<<1) | USB_EPDIR_IN)

#define USB_CTRL_CLBUF                  (1<<4)  //1 = force clear RX buffer, toggle twice if use double buffer
#define USB_CTRL_VENP                   (1<<3)  //1 = validate endpoint, # of bytes
#define USB_CTRL_DSEN                   (1<<2)  //1 = go to data stage (from setup stage)
                                                //CONTROL OUT, set to go to data stage otherwise ISP1582 will NAK data transfers
                                                //CONTROL IN, set before write to TX FIFO & validate endpoint
                                                //if no data stage required, set STATUS bit after setup stage
#define USB_CTRL_STATUS                 (1<<1)  //0 = send NAK
                                                //1 = send empty package following IN token (IN), send ACK following OUT token (OUT)
                                                //only use for CONTROL (IN/OUT)
#define USB_CTRL_STALL                  (1<<0)  //1 = stall the indexed endpoint (do not use for iso transfer)

#define USB_BUFSTAT_EMPTY               (0x0)   //Buffers are not filled
#define USB_BUFSTAT_ONEFULL_A           (0x1)   //One of the buffers is filled
#define USB_BUFSTAT_ONEFULL_B           (0x2)   //One of the buffers is filled
#define USB_BUFSTAT_BOTHFULL            (0x3)   //Both buffers are filled

//apply to indexed pointer
#define USB_EPTYPE_NOEMPKT              (1<<4)  //1 = no empty packet, use with DMA
#define USB_EPTYPE_ENABLE               (1<<3)  //1 = enable FIFO
#define USB_EPTYPE_DBLBUF               (1<<2)  //1 = enable double buffer
#define USB_EPTYPE_NONE                 (0x0)
#define USB_EPTYPE_ISO                  (0x1)
#define USB_EPTYPE_BULK                 (0x2)
#define USB_EPTYPE_INTR                 (0x3)

//clear by write 1 to the bit
#define USB_INTR_EP2TX                  (1<<15) //1 = interrupt occurs
#define USB_INTR_EP2RX                  (1<<14)
#define USB_INTR_EP1TX                  (1<<13)
#define USB_INTR_EP1RX                  (1<<12)
#define USB_INTR_EP0TX                  (1<<11)
#define USB_INTR_EP0RX                  (1<<10)
#define USB_INTR_EP0SETUP               (1<<8)
#define USB_INTR_VBUS                   (1<<7)  //LOW to HIGH transition
#define USB_INTR_DMA                    (1<<6)  //change in DMA Interrupt Reason
#define USB_INTR_HS_STAT                (1<<5)  //1 = full-speed to high-speed
#define USB_INTR_RESUME                 (1<<4)  //suspend to resume
#define USB_INTR_SUSP                   (1<<3)  //active to suspend was detected on bus
#define USB_INTR_PSOF                   (1<<2)  //PSOF or micro-PSOF received
#define USB_INTR_SOF                    (1<<1)  //SOF or micro-SOF received
#define USB_INTR_BRESET                 (1<<0)  //USB bus reset detected

#define USB_TEST_FORCEHS                (1<<7)
#define USB_TEST_FORCEFS                (1<<4)
#define USB_TEST_PRBS                   (1<<3)
#define USB_TEST_KSTATE                 (1<<2)
#define USB_TEST_JSTATE                 (1<<1)
#define USB_TEST_SE0_NAK                (1<<0)

#define  USB_TESTMODE              0x0002
#define  USB_EP_HALT                0
#define  USB_DEVICE_REMOTE_WAKEUP   1

#define  USB_TEST_J_w               0x0100
#define  USB_TEST_K_w               0x0200
#define  USB_TEST_SE0_NAK_w         0x0300
#define  USB_TEST_PACKET_w          0x0400
#define  USB_TEST_FORCE_ENABLE_w    0x0500

extern USB_TypeDef *USB_Struct;

//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
void usb_init();
void usb_tx(u8 *buffer, u32 length);
u32 usb_rx(u8 *buffer, u32 length);
void usb_flush();
void usb_isr_handler();

#endif	//__USB_H
