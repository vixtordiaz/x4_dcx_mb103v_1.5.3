/**
  **************** (C) COPYRIGHT 2011 SCT Performance, LLC *********************
  * File Name          : vpw.h
  * Device / System    : SCT products using the "CommonCode" platform
  * Author             : Quyen Leba
  *
  * Version            : 1 
  * Date               : 05/02/2011
  * Description        : 
  *                    : 
  *
  *
  * History            : 05/02/2011 M. Davis
  *                    :   Add SCT Copyright header,
  *
  ******************************************************************************
  */

#ifndef __VPW_H__
#define __VPW_H__

#include <arch/gentype.h>
#include <common/obd2.h>

#define MCU_XTAL                                48000000

// define error return codes
#define J1850_RETURN_CODE_OK                    0
#define J1850_RETURN_CODE_UNKNOWN               1
#define J1850_RETURN_CODE_BUS_BUSY              2
#define J1850_RETURN_CODE_BUS_ERROR             3
#define J1850_RETURN_CODE_DATA_ERROR            4
#define J1850_RETURN_CODE_NO_DATA               5
#define J1850_RETURN_CODE_DATA                  6
#define J1850_RETURN_CODE_LENGTH_ERROR          7

// convert microseconds to counter values
#define us2cnt(us) ((unsigned int)((unsigned long)(us) / (1000000L / (float)((unsigned long)MCU_XTAL / 1L))))

#define WAIT_100us              us2cnt(100)        // 100us, used to count 100ms

// define J1850 VPW timing requirements in accordance with SAE J1850 standard
// all pulse width times in us
// transmitting pulse width
#define TX_SHORT                us2cnt(64)        // Short pulse nominal time
#define TX_LONG                 us2cnt(128)        // Long pulse nominal time
#define TX_SOF                  us2cnt(200)        // Start Of Frame nominal time
#define TX_EOD                  us2cnt(200)        // End Of Data nominal time
#define TX_EOF                  us2cnt(280)        // End Of Frame nominal time
#define TX_BRK                  us2cnt(300)        // Break nominal time
#define TX_IFS                  us2cnt(300)        // Inter Frame Separation nominal time

// see SAE J1850 chapter 6.6.2.5 for preferred use of In Frame Respond/Normalization pulse
#define TX_IFR_SHORT_CRC        us2cnt(64)    // short In Frame Respond, IFR contain CRC
#define TX_IFR_LONG_NOCRC       us2cnt(128)    // long In Frame Respond, IFR contain no CRC

// receiving pulse width
#define RX_SHORT_MIN            us2cnt(34)    // minimum short pulse time
#define RX_SHORT_MAX            us2cnt(96)    // maximum short pulse time
#define RX_LONG_MIN             us2cnt(96)    // minimum long pulse time
#define RX_LONG_MAX             us2cnt(163)    // maximum long pulse time
#define RX_SOF_MIN              us2cnt(163)    // minimum start of frame time
#define RX_SOF_MAX              us2cnt(239)    // maximum start of frame time
#define RX_EOD_MIN              us2cnt(163)    // minimum end of data time
#define RX_EOD_MAX              us2cnt(239)    // maximum end of data time
#define RX_EOF_MIN              us2cnt(239)    // minimum end of frame time, ends at minimum IFS
#define RX_BRK_MIN              us2cnt(239)    // minimum break time
#define RX_IFS_MIN              us2cnt(280)    // minimum inter frame separation time, ends at next SOF

// see chapter 6.6.2.5 for preferred use of In Frame Respond/Normalization pulse
#define RX_IFR_SHORT_MIN        us2cnt(34)        // minimum short in frame respond pulse time
#define RX_IFR_SHORT_MAX        us2cnt(96)        // maximum short in frame respond pulse time
#define RX_IFR_LONG_MIN         us2cnt(96)        // minimum long in frame respond pulse time
#define RX_IFR_LONG_MAX         us2cnt(163)        // maximum long in frame respond pulse time

#define WAIT_50us               us2cnt(50)        // 50us, used to count 100ms
#define WAIT_25us               us2cnt(25)      // 25us

// define J1850 High speed VPW timing requirements in accordance with SAE J1850 standard (sort of)
// all pulse width times in us
// transmitting pulse width
//#define TX_SHORT_HS             us2cnt(16)        // Short pulse nominal time
//#define TX_LONG_HS              us2cnt(32)        // Long pulse nominal time
//#define TX_SOF_HS               us2cnt(50)        // Start Of Frame nominal time
//Stephan's timing
#define TX_SHORT_HS             us2cnt(12)        // Short pulse nominal time
#define TX_LONG_HS              us2cnt(28)        // Long pulse nominal time
#define TX_SOF_HS               us2cnt(48)        // Start Of Frame nominal time
#define TX_EOD_HS               us2cnt(50)        // End Of Data nominal time
#define TX_EOF_HS               us2cnt(70)        // End Of Frame nominal time
#define TX_BRK_HS               us2cnt(75)        // Break nominal time
#define TX_IFS_HS               us2cnt(75)        // Inter Frame Separation nominal time

// see SAE J1850 chapter 6.6.2.5 for preferred use of In Frame Respond/Normalization pulse
#define TX_IFR_SHORT_CRC_HS     us2cnt(16)    // short In Frame Respond, IFR contain CRC
#define TX_IFR_LONG_NOCRC_HS    us2cnt(32)    // long In Frame Respond, IFR contain no CRC

// receiving pulse width
/*
#define RX_SHORT_MIN_HS         us2cnt(5)//6    // minimum short pulse time
#define RX_SHORT_MAX_HS         us2cnt(22)//24    // maximum short pulse time
#define RX_LONG_MIN_HS          us2cnt(22)//24    // minimum long pulse time
#define RX_LONG_MAX_HS          us2cnt(43)//41    // maximum long pulse time
#define RX_SOF_MIN_HS           us2cnt(40)//41    // minimum start of frame time
*/

#define RX_SHORT_MIN_HS         us2cnt(6)//6    // minimum short pulse time
#define RX_SHORT_MAX_HS         us2cnt(24)//24    // maximum short pulse time
#define RX_LONG_MIN_HS          us2cnt(24)//24    // minimum long pulse time
#define RX_LONG_MAX_HS          us2cnt(41)//41    // maximum long pulse time
#define RX_SOF_MIN_HS           us2cnt(41)//41    // minimum start of frame time

#define RX_SOF_MAX_HS           us2cnt(60)    // maximum start of frame time
#define RX_EOD_MIN_HS           us2cnt(41)    // minimum end of data time
#define RX_EOD_MAX_HS           us2cnt(60)    // maximum end of data time
#define RX_EOF_MIN_HS           us2cnt(60)    // minimum end of frame time, ends at minimum IFS
#define RX_BRK_MIN_HS           us2cnt(60)    // minimum break time
#define RX_IFS_MIN_HS           us2cnt(65)    // minimum inter frame separation time, ends at next SOF

// see chapter 6.6.2.5 for preferred use of In Frame Respond/Normalization pulse
#define RX_IFR_SHORT_MIN_HS     us2cnt(6)        // minimum short in frame respond pulse time
#define RX_IFR_SHORT_MAX_HS     us2cnt(24)        // maximum short in frame respond pulse time
#define RX_IFR_LONG_MIN_HS      us2cnt(24)        // minimum long in frame respond pulse time
#define RX_IFR_LONG_MAX_HS      us2cnt(41)        // maximum long in frame respond pulse time

#define MAX_VPW_DATA_LENGTH_LS  12
#define MAX_VPW_DATA_LENGTH_HS  4096    //TODOQ: is this # correct?

//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
void vpw_init();
u8 vpw_ls_tx(u8 *databuffer, u8 datalength);
u8 vpw_ls_rx(u8 *databuffer, u16 *datalength);
u8 vpw_hs_tx(u8 *databuffer, u16 datalength);
u8 vpw_hs_rx(u8 *databuffer, u16 *datalength);
void vpw_send_break();

#endif  //__VPW_H__
