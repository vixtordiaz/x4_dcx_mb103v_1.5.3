/**
  **************** (C) COPYRIGHT 2011 SCT Performance, LLC *********************
  * File Name          : fs.c
  * Device / System    : SCT products using the "CommonCode" platform
  * Author             : Quyen Leba
  *
  * Version            : 1 
  * Date               : 05/02/2011
  * Description        : 
  *                    : 
  *
  *
  * History            : 05/02/2011 M. Davis
  *                    :   Add SCT Copyright header,
  *
  ******************************************************************************
  */

#include <common/statuscode.h>
#include "system.h"
#include "llayer.h"
#include "ftl.h"
#include "api_f.h"
#include "fat.h"
#include "fs.h"

//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
void fs_init()
{
    u32 ret;
    F_SPACE f_space_info;
    
    ret = f_init();
    ret = f_enterFS();  
    ret = f_initvolume(0,f_ftlinit,0);
    if(ret != 0)
    {
        ret = f_format(0,F_FAT16_MEDIA);
        if(ret != 0)
        {
            //usartDebug_Printf("\n\rFormat FAIL");
            // while(1);
            
            //TODOQ:
            //strncpy(ErrHandlerMsg,"\n\rInit_System()... fail in NAND flash-drive FAT format",ERR_HNDLR_MAX_MSG_LEN);
            //error_handler (INIT_ERROR, 3, TRUE);
        }
    }
    ret = f_chdrive(0);
    ret = f_getfreespace(0,&f_space_info);
    ret = f_chdir(DEFAULT_TUNE_FOLDER_NAME);
}

//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
u8 fs_reinit()
{
    u32 f_errorcode;
    u32 statusresponse;
    
    statusresponse = S_FAIL;
    f_errorcode = f_init();
    if (f_errorcode == 0)
    {
        f_errorcode = f_enterFS();
        if (f_errorcode == 0)
        {
            f_errorcode = f_initvolume(0,f_ftlinit,0);
            if (f_errorcode == 0)
            {
                f_errorcode = f_chdrive(0);
                if (f_errorcode == 0)
                {
                    f_errorcode = f_chdir(DEFAULT_TUNE_FOLDER_NAME);
                    if (f_errorcode == 0)
                    {
                        statusresponse = S_SUCCESS;
                    }
                }
            }
        }
    }
    return statusresponse;
}

//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
u8 fs_format()
{
    u32 response;
    u32 i;
    
    for(i=0;i<2000;i++)
    {
        response = ll_erase(i);
        //delays(100,'u');
    }
    response = ml_format();
    response = ml_init();
    
    if (response)
    {
        response = S_FAIL;
        goto fs_format_done;
    }
    else
    {
        response = f_init();
        response = f_enterFS();
        response = f_initvolume(0,f_ftlinit,0);
        response = f_chdrive(0);
        if (response)
        {
            response = S_FAIL;
            goto fs_format_done;
        }
        else
        {
            response = f_format(0,F_FAT16_MEDIA);
            if (response)
            {
                response = S_FAIL;
                goto fs_format_done;
            }
            else
            {
                response = f_mkdir(DEFAULT_TUNE_FOLDER_NAME);
                response = f_chdir(DEFAULT_TUNE_FOLDER_NAME);
                
                if (response != 0)
                {
                    response = S_FAIL;
                }
            }
        }
    }

fs_format_done:
    return response;
}

//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
F_DRIVER * fs_driver_init()
{
    return f_ftlinit(0);
}
