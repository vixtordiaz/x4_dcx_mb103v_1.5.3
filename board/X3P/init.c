/**
  **************** (C) COPYRIGHT 2011 SCT Performance, LLC *********************
  * File Name          : init.c
  * Device / System    : SCT products using the "CommonCode" platform
  * Author             : Quyen Leba
  *
  * Version            : 1 
  * Date               : 05/02/2011
  * Description        : 
  *                    : 
  *
  *
  * History            : 05/02/2011 M. Davis
  *                    :   Add SCT Copyright header,
  *
  ******************************************************************************
  */

#include <board/interrupt.h>
#include <common/cmdif.h>
#include <common/settings.h>
#include "system.h"

void init()
{
    clock_init();
    settings_load();
    peripherals_init();
    interrupt_init();
    adc_init(ADC_DATALOG);
    
    LCD_gclrvp();
    LCD_gselfont(&msfont);
    LCD_gputscpos(0,0,"TESTING testing 123");
    //TODOQ: LCD_gputscpos(0,0,*Language->TXT_InitFileSystem);

    fs_init();
	cmdif_init();

    /*  //TODO:
    Version_DeviceCheck();
#if (SUPPORT_STRATEGYTUNE)
    //Version_TuneCheck();
#endif
    //Version_FirmwareCheck();
    */
}
