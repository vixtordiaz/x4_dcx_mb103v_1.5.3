/**
  **************** (C) COPYRIGHT 2011 SCT Performance, LLC *********************
  * File Name          : flash.c
  * Device / System    : SCT products using the "CommonCode" platform
  * Author             : Quyen Leba
  *
  * Version            : 1 
  * Date               : 05/02/2011
  * Description        : 
  *                    : 
  *
  *
  * History            : 05/02/2011 M. Davis
  *                    :   Add SCT Copyright header,
  *
  ******************************************************************************
  */

#include <string.h>
#include <91x_fmi.h>
#include <common/statuscode.h>
#include <board/flash.h>

#define FMI_SETTINGS_STARTADDR  (0x00080000)
#define FMI_SETTINGS_ADDR_S0    (FMI_SETTINGS_STARTADDR)
#define FMI_SETTINGS_ADDR_S1    (FMI_SETTINGS_ADDR_S0+8*1024)
#define FMI_SETTINGS_ADDR_S2    (FMI_SETTINGS_ADDR_S1+8*1024)
#define FMI_SETTINGS_ADDR_S3    (FMI_SETTINGS_ADDR_S2+8*1024)
#define SETTINGS_ADDRESS        (FMI_SETTINGS_ADDR_S3)
#define SETTINGS_SECTOR         (FMI_B1S3)

#define CRITICAL_SECTOR         (FMI_B1S0)
#define CRITICAL_ADDR           (FMI_SETTINGS_ADDR_S0)
#define TUNE_SECTOR             (FMI_B1S1)
#define TUNE_ADDR               (FMI_SETTINGS_ADDR_S1)
#define DATALOGGENERAL_SECTOR   (FMI_B1S2)
#define DATALOGGENERAL_ADDR     (FMI_SETTINGS_ADDR_S2)
#define OLDSETTINGS_SECTOR      (FMI_B1S3)
#define OLDSETTINGS_ADDR        (FMI_SETTINGS_ADDR_S3)

//------------------------------------------------------------------------------
// Save bootloader type signature into BOOTLOADER_SIGNATURE_ADDR
// Inputs:  BootloaderType bootloadertype
//          u8  *signature
//          u8  signaturelength (must be multiple of 4 bytes)
// Return:  u8  status
// Engineer: Quyen Leba
//------------------------------------------------------------------------------
u8 flash_save_bootloader_signature(BootloaderType bootloadertype)
{
    return S_SUCCESS;
}

//------------------------------------------------------------------------------
// Input:   FlashSettingsArea flashsettingsarea
//          u8  *settingsdata
//          u32 settingsdatalength
// Return:  u8  status
// Engineer: Quyen Leba
// Note: all settings area of X3P are in same bank; i.e. erase one area means
// erase all
//------------------------------------------------------------------------------
u8 flash_save_setting(FlashSettingsArea flashsettingsarea,
                      u8 *settingsdata, u32 settingsdatalength)
{
    u32 i;
    u16 *ptr = (u16*)settingsdata;
    u32 flashaddr;
    vu32 flashsector;

    switch(flashsettingsarea)
    {
    case CriticalArea:
        flashaddr = CRITICAL_ADDR;
        flashsector = CRITICAL_SECTOR;
        break;
    case TuneArea:
        flashaddr = TUNE_ADDR;
        flashsector = TUNE_SECTOR;
        break;
    case DatalogGeneralArea:
        flashaddr = DATALOGGENERAL_ADDR;
        flashsector = DATALOGGENERAL_SECTOR;
        break;
    case OldSettingsArea:
        flashaddr = OLDSETTINGS_ADDR;
        flashsector = OLDSETTINGS_SECTOR;
        break;
    default:
        return S_INPUT;
    }
    
    FMI_WriteProtectionCmd(flashsector,DISABLE);
    FMI_EraseSector(flashsector);
    FMI_WaitForLastOperation(FMI_BANK_1);

    for (i=0;i<settingsdatalength;i+=2)
    {
        FMI_WriteHalfWord(flashaddr,*ptr);
        flashaddr+=2;
        ptr++;
        FMI_WaitForLastOperation(FMI_BANK_1);
    }
    return S_SUCCESS;
}

//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
u8 flash_load_setting(FlashSettingsArea flashsettingsarea,
                      u8 *settingsdata, u32 settingsdatalength)
{
    //TODOQ: old structure settings need conversion
    switch(flashsettingsarea)
    {
    case CriticalArea:
        memcpy((char*)settingsdata,(char*)CRITICAL_ADDR,settingsdatalength);
        break;
    case TuneArea:
        memcpy((char*)settingsdata,(char*)TUNE_ADDR,settingsdatalength);
        break;
    case DatalogGeneralArea:
        memcpy((char*)settingsdata,(char*)DATALOGGENERAL_ADDR,settingsdatalength);
        break;
    case OldSettingsArea:
        memcpy((char*)settingsdata,(char*)OLDSETTINGS_ADDR,settingsdatalength);
        break;
    default:
        return S_INPUT;
    }

    return S_SUCCESS;
}

//------------------------------------------------------------------------------
// Check if settings is using old structure
// Return:  bool isoldsettings (TRUE: old)
//------------------------------------------------------------------------------
bool flash_isoldstructure_settings()
{
    u8 *oldsettings_ptr;

    oldsettings_ptr = (u8*)OLDSETTINGS_ADDR;
    
    if (strncmp((char*)oldsettings_ptr,"SCT",3) == 0)
    {
        return TRUE;
    }
    else if (strncmp((char*)oldsettings_ptr,"sct",3) == 0)
    {
        return TRUE;
    }
    else if (strncmp((char*)oldsettings_ptr,"Sct",3) == 0)
    {
        return TRUE;
    }
    else
    {
        return FALSE;
    }
}

