/**
  **************** (C) COPYRIGHT 2011 SCT Performance, LLC *********************
  * File Name          : adc.h
  * Device / System    : SCT products using the "CommonCode" platform
  * Author             : Quyen Leba
  *
  * Version            : 1 
  * Date               : 05/02/2011
  * Description        : 
  *                    : 
  *
  *
  * History            : 05/02/2011 M. Davis
  *                    :   Add SCT Copyright header,
  *
  ******************************************************************************
  */

#ifndef __ADC_H
#define __ADC_H

#define ANALOG_INPUT_COUNT              2

typedef enum
{
    ADC_AIN1                = 0,
    ADC_AIN2                = 1,
    //ADC_AIN3,
    //ADC_AIN4,
    //ADC_TEMPSENSOR,
    //ADC_VREFINT
	ADC_VPP                 = 20,
    ADC_VBAT                = 21,
}ADC_CHANNEL;

typedef enum
{
    ADC_FLASH,
    ADC_DATALOG,
}ADC_MODE;

// AIN0
#define AIN0_SHORTNAME      "AIN1"
#define AIN0_NAME           "Analog Input 1"
#define AIN0_MIN            0
#define AIN0_MAX            5

// AIN1
#define AIN1_SHORTNAME      "AIN2"
#define AIN1_NAME           "Analog Input 2"
#define AIN1_MIN            0
#define AIN1_MAX            5

void adc_init(ADC_MODE mode);
u8 adc_read(ADC_CHANNEL ch, float *DataValueFloat);

#endif  //__ADC_H
