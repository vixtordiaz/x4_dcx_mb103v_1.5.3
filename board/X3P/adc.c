/**
  **************** (C) COPYRIGHT 2011 SCT Performance, LLC *********************
  * File Name          : adc.c
  * Device / System    : SCT products using the "CommonCode" platform
  * Author             : Quyen Leba
  *
  * Version            : 1 
  * Date               : 05/02/2011
  * Description        : 
  *                    : 
  *
  *
  * History            : 05/02/2011 M. Davis
  *                    :   Add SCT Copyright header,
  *
  ******************************************************************************
  */

#include "91x_scu.h"
#include "91x_gpio.h"
#include "91x_adc.h"
#include <common/statuscode.h>
#include "system.h"
#include "adc.h"

//------------------------------------------------------------------------------
// Module definition
//------------------------------------------------------------------------------
#define ADC_MAX     33.0
#define ADC_RANGE   10230.0
#define ADC_PTMAX   3300
#define ADC_PTRANGE 1023
#define CH1_MUL     291.0
#define CH1_DIV     191.0
#define CH2_MUL     291.0
#define CH2_DIV     191.0

#define VBATT_MUL   122.0
#define VBATT_DIV   22.0
#define VPP_MUL     115.0
#define VPP_DIV     15.0

#define VBATT_CH    ADC_Channel_2
#define VPP_CH      ADC_Channel_3
#define EXT1_CH     ADC_Channel_0
#define EXT2_CH     ADC_Channel_1
#define VBATT_IO    GPIO_ANAChannel2
#define VPP_IO      GPIO_ANAChannel3
#define EXT1_IO     GPIO_ANAChannel0
#define EXT2_IO     GPIO_ANAChannel1
#define VBATT_OVF   ADC_FLAG_OV_CH_2
#define VPP_OVF     ADC_FLAG_OV_CH_3
#define EXT1_OVF    ADC_FLAG_OV_CH_0
#define EXT2_OVF    ADC_FLAG_OV_CH_1

static ADC_MODE adc_current_mode    = ADC_DATALOG;

//------------------------------------------------------------------------------
// Engineer: Quyen Leba
// Date: Jan 28, 2008
//------------------------------------------------------------------------------
void adc_init(ADC_MODE mode)
{
    ADC_InitTypeDef ADC_InitStructure;

    //Enable the clock for the ADC
    SCU_APBPeriphClockConfig(__ADC, ENABLE);
    ADC_DeInit();

    adc_current_mode = mode;
    if (mode == ADC_FLASH)
    {
        GPIO_ANAPinConfig(VBATT_IO, ENABLE);
        GPIO_ANAPinConfig(VPP_IO, ENABLE);
    }
    else
    {
        //Configure the GPIO4 pin 0,1 as analog input
        GPIO_ANAPinConfig(EXT1_IO, ENABLE);
        GPIO_ANAPinConfig(EXT2_IO, ENABLE);
    }

    ADC_StructInit(&ADC_InitStructure);

    ADC_InitStructure.ADC_Channel_2_Mode = ADC_NoThreshold_Conversion;
    ADC_InitStructure.ADC_Channel_3_Mode = ADC_NoThreshold_Conversion;
    ADC_InitStructure.ADC_Select_Channel = ADC_Channel_2;
    //Silicon Rev H limitation: ADC scan & continuous mode cannot be used together
    //See Errata 2.14
    ADC_InitStructure.ADC_Scan_Mode = DISABLE;

    ADC_InitStructure.ADC_Conversion_Mode = ADC_Continuous_Mode;

    // Enable the ADC
    ADC_Cmd(ENABLE);

    ADC_PrescalerConfig(0x0);
    ADC_Init(&ADC_InitStructure);
    ADC_ClearFlag(ADC_FLAG_ECV);
    ADC_ConversionCmd(ADC_Conversion_Start);
}

//------------------------------------------------------------------------------
// Engineer: Quyen Leba
// Date: Sep 10, 2008
//------------------------------------------------------------------------------
void adc_selectChannel(u16 ch)
{
#define ADC_CHANNEL_MASK    0xFE3F
    ADC->CR &= ADC_CHANNEL_MASK;
    ADC->CR |= ch << 0x6;
    //ADC_ConversionCmd(ADC_Conversion_Start);
}

//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
u8 adc_read(ADC_CHANNEL ch, float *DataValueFloat)
{
    ADC_MODE mode;
    u16 channel;
    s32 timeout = 5000;
    float convert_ratio;
    u16 DataValue = 0;

    //TODOQ: not correct
    
    switch(ch)
    {
    case ADC_VPP:
        mode = ADC_FLASH;
        channel = VPP_CH;
        break;
    case ADC_VBAT:
        mode = ADC_FLASH;
        channel = VBATT_CH;
        break;
    case ADC_AIN1:
        mode = ADC_DATALOG;
        channel = EXT1_CH;
        break;
    case ADC_AIN2:
        mode = ADC_DATALOG;
        channel = EXT2_CH;
        break;
    default:
        *DataValueFloat = 0;
        return S_FAIL;
    }
    
    if (adc_current_mode != mode)
    {
        adc_init(mode);
    }
    adc_selectChannel(channel);
    
    while ((ADC_GetFlagStatus(ADC_FLAG_ECV) != SET) && (--timeout > 0));
    if (timeout > 0)
    {
        DataValue = ADC_GetConversionValue(channel);
        ADC_ClearFlag(ADC_FLAG_ECV);
    }
    else
    {
        *DataValueFloat = 0;
        return S_FAIL;
    }
    
    switch(ch)
    {
    case ADC_AIN1:
        convert_ratio = (float)CH1_MUL / CH1_DIV * ADC_MAX / ADC_RANGE;
        break;
    case ADC_AIN2:
        convert_ratio = (float)CH2_MUL / CH2_DIV * ADC_MAX / ADC_RANGE;
        break;
    case ADC_VPP:
        convert_ratio = (float)VPP_MUL / VPP_DIV * ADC_MAX / ADC_RANGE;
        break;
    case ADC_VBAT:
        convert_ratio = (float)VBATT_MUL/VBATT_DIV * ADC_MAX/ADC_RANGE + 0.5;
        break;
    default:
        *DataValueFloat = 0;
        return S_FAIL;
    }
    
    *DataValueFloat = ((float)DataValue) * convert_ratio;
    return S_SUCCESS;
}
