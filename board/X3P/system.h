/**
  **************** (C) COPYRIGHT 2011 SCT Performance, LLC *********************
  * File Name          : system.h
  * Device / System    : SCT products using the "CommonCode" platform
  * Author             : Quyen Leba
  *
  * Version            : 1 
  * Date               : 05/02/2011
  * Description        : 
  *                    : 
  *
  *
  * History            : 05/02/2011 M. Davis
  *                    :   Add SCT Copyright header,
  *
  ******************************************************************************
  */

#ifndef __SYSTEM_H
#define __SYSTEM_H

#include <91x_type.h>
#include <fs/genfs.h>
#include "generic.h"
#include "keypad.h"
#include "lcd.h"
#include "adc.h"

#define RAM_exe __ramfunc

#define DEFAULT_TUNE_FOLDER_NAME            "SCT"
#define DEFAULT_TUNE_VERSION_NAME           "tuneversion.txt"
#define DEFAULT_DATALOG_FOLDER_NAME         DEFAULT_TUNE_FOLDER_NAME
#define DEFAULT_DATALOG_FILE_EXTENSION      "dlx"

typedef enum {
    NOAUDIO,
    STARTUP,
    BEEP,
    KEYGOOD,
    ERRORMSG,
    PROGRAMDONE,
    JOGWHEELLEFT,
    JOGWHEELRIGHT
}AudioCode;

void clock_init();
void emi_init();
void keypad_init();
u8 keypad_poll();
u8 keypad_pollwait(u8 key, u8 allowexit);

void buzzer_init();
void play_sounds(AudioCode choice);
void peripherals_init();
void fs_init();
u8 fs_reinit();
u8 fs_format();
F_DRIVER * fs_driver_init();

#define Buzzer(choice)      play_sounds(choice)
#define Buzzer_Beep()       Buzzer(KEYGOOD)

#endif