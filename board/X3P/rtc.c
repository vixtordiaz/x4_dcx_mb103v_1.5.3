/**
  **************** (C) COPYRIGHT 2011 SCT Performance, LLC *********************
  * File Name          : rtc.c
  * Device / System    : SCT products using the "CommonCode" platform
  * Author             : Quyen Leba
  *
  * Version            : 1 
  * Date               : 05/02/2011
  * Description        : 
  *                    : 
  *
  *
  * History            : 05/02/2011 M. Davis
  *                    :   Add SCT Copyright header,
  *
  ******************************************************************************
  */

#include <91x_rtc.h>
#include <91x_scu.h>
#include <board/rtc.h>

//------------------------------------------------------------------------------
// Engineer: Quyen Leba
// Date: Jan 18, 2008
//------------------------------------------------------------------------------
void rtc_init()
{
    RTC_DATE date;
    //RTC_ALARM alarm;

    SCU_APBPeriphClockConfig(__RTC,ENABLE);     //enable RTC clock
    SCU_APBPeriphReset(__RTC,DISABLE);          //RTC out of RESET state

    //configure date & time structures
    date.day = 1;
    date.weekday = 1;
    date.month = 1;
    date.year = 8;
    date.century = 21;

    //set time & date
    RTC_SetDate(date);

    rtc_reset();

#if 0
    alarm.day = 7;
    alarm.hours = 19;
    alarm.minutes = 0;
    alarm.seconds = 10;
    //set alarm
    RTC_SetAlarm(alarm);
#endif

    RTC_ClearFlag(RTC_FLAG_Alarm);  //clear alarm flag
    //RTC_AlarmCmd(ENABLE);         //enable alarm
    //while (!RTC_GetFlagStatus(RTC_FLAG_Alarm));
    //RTC_GetTime(BCD,&time);       //time structure must be equal to alarm time
}

//------------------------------------------------------------------------------
// Clear RTC time to ZERO
// Engineer: Quyen Leba
//------------------------------------------------------------------------------
void rtc_reset()
{
    RTC_TIME time;

    time.hours = 0;
    time.minutes = 0;
    time.seconds = 0;
    time.milliseconds = 0;

    RTC_SetTime(time);
}

//------------------------------------------------------------------------------
// Read minutes, second & milisecond
// Outputs: u8  *min
//          u8  *sec
//          u16 *msec
// Engineer: Quyen Leba
//------------------------------------------------------------------------------
void RTC_ReadCounterBIN(u8 *hour, u8 *min, u8 *sec, u16 *msec)
{
    RTC_TIME time;

    RTC_GetTime(BINARY,&time);
    *hour   = time.hours;
    *min    = time.minutes;
    *sec    = time.seconds;
    *msec   = time.milliseconds;
}

//------------------------------------------------------------------------------
// Read RTC time in milisecond count
// Engineer: Quyen Leba
//------------------------------------------------------------------------------
//TODOQ: u32 RTC_ReadCounterBCD(void)
u32 rtc_getvalue()
{
    RTC_TIME    time;
    u32         count;

    RTC_GetTime(BINARY,&time);

    count  = time.milliseconds;
    count += time.seconds * (1000);
    count += time.minutes * (1000*60);
    count += time.hours   * (1000*60*60);
    return count;
}
