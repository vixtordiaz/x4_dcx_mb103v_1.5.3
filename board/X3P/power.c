/**
  **************** (C) COPYRIGHT 2011 SCT Performance, LLC *********************
  * File Name          : power.c
  * Device / System    : SCT products using the "CommonCode" platform
  * Author             : Quyen Leba
  *
  * Version            : 1 
  * Date               : 05/02/2011
  * Description        : 
  *                    : 
  *
  *
  * History            : 05/02/2011 M. Davis
  *                    :   Add SCT Copyright header,
  *
  ******************************************************************************
  */

#include <91x_gpio.h>
#include <91x_tim.h>
#include <board/power.h>
#include <board/timer.h>
#include <board/delays.h>
#include <common/statuscode.h>

#define V5VPORT         GPIO1
#define V5V_PIN         GPIO_Pin_1

#define FEPSPORT        GPIO1
#define FEPS_PIN        GPIO_Pin_2

#define SCIPWRPORT      GPIO2
#define SCI_6_PWR_PIN   GPIO_Pin_0
#define SCI_9_PWR_PIN   GPIO_Pin_1
#define SCI_12_PWR_PIN  GPIO_Pin_2
#define SCI_14_PWR_PIN  GPIO_Pin_3

#define PWMPWRCTRLPORT  GPIO6
#define PWMPWRCTRL_PIN  GPIO_Pin_4

#define PWRSWSTATUSPORT  GPIO7
#define PWRSWSTATUS_PIN  GPIO_Pin_7

//------------------------------------------------------------------------------
// Private Prototypes
//------------------------------------------------------------------------------
void    Vpp_Switch(FunctionalState state);
void    Vpp_Set18V(void);
void    Vpp_Set20V(void);

//------------------------------------------------------------------------------
// Config power for Vpp & external 5V
// Engineer: Quyen Leba
// Date: Jan 28, 2008
//------------------------------------------------------------------------------
void power_init()
{
    GPIO_InitTypeDef GPIO_InitStructure;
    
    // GPIO7_Pin_7, LCD_RED, used to read power-switch status
    GPIO_StructInit(&GPIO_InitStructure);
    GPIO_InitStructure.GPIO_Pin = PWRSWSTATUS_PIN;
    GPIO_InitStructure.GPIO_Direction = GPIO_PinInput;
    GPIO_InitStructure.GPIO_Type = GPIO_Type_PushPull;
    GPIO_Init(PWRSWSTATUSPORT, &GPIO_InitStructure);
    
    //Config power route control for SCI
    GPIO_StructInit(&GPIO_InitStructure);
    GPIO_InitStructure.GPIO_Pin =
        SCI_6_PWR_PIN | SCI_9_PWR_PIN | SCI_12_PWR_PIN | SCI_14_PWR_PIN;
    GPIO_InitStructure.GPIO_Direction = GPIO_PinOutput;
    GPIO_InitStructure.GPIO_Type = GPIO_Type_PushPull;
    GPIO_InitStructure.GPIO_Alternate = GPIO_OutputAlt1;
    GPIO_Init(SCIPWRPORT, &GPIO_InitStructure);
    GPIO_WriteBit(SCIPWRPORT,SCI_6_PWR_PIN,Bit_RESET);
    GPIO_WriteBit(SCIPWRPORT,SCI_9_PWR_PIN,Bit_RESET);
    GPIO_WriteBit(SCIPWRPORT,SCI_12_PWR_PIN,Bit_RESET);
    GPIO_WriteBit(SCIPWRPORT,SCI_14_PWR_PIN,Bit_RESET);
    
    //Cofig power route control for FEPS
    GPIO_StructInit(&GPIO_InitStructure);
    GPIO_InitStructure.GPIO_Pin = FEPS_PIN;
    GPIO_InitStructure.GPIO_Direction = GPIO_PinOutput;
    GPIO_InitStructure.GPIO_Type = GPIO_Type_PushPull;
    GPIO_InitStructure.GPIO_Alternate = GPIO_OutputAlt1;
    GPIO_Init(GPIO1, &GPIO_InitStructure);
    GPIO_WriteBit(FEPSPORT,FEPS_PIN,Bit_RESET);
    
    //Cofig power route control for 5V external supply
    GPIO_StructInit(&GPIO_InitStructure);
    GPIO_InitStructure.GPIO_Pin = V5V_PIN;
    GPIO_InitStructure.GPIO_Direction = GPIO_PinOutput;
    GPIO_InitStructure.GPIO_Type = GPIO_Type_PushPull;
    GPIO_InitStructure.GPIO_Alternate = GPIO_OutputAlt1;
    GPIO_Init(V5VPORT, &GPIO_InitStructure);
    V5VExt_Ctrl(OFF);
    
    timer2_init();
    Vpp_Switch(OFF);
    
    //Config PWM P6.4 to control Vpp power supply
    GPIO_StructInit(&GPIO_InitStructure);
    GPIO_InitStructure.GPIO_Pin = PWMPWRCTRL_PIN;
    GPIO_InitStructure.GPIO_Direction = GPIO_PinOutput;
    GPIO_InitStructure.GPIO_Type = GPIO_Type_PushPull;
    //GPIO_InitStructure.GPIO_IPInputConnected = GPIO_IPInputConnected_Enable;
    GPIO_InitStructure.GPIO_IPConnected = GPIO_IPConnected_Enable;
    GPIO_InitStructure.GPIO_Alternate = GPIO_OutputAlt2;
    GPIO_Init(PWMPWRCTRLPORT, &GPIO_InitStructure);
    
    Vpp_Set18V();
    Vpp_Ctrl(Vpp_OFF);
}

//------------------------------------------------------------------------------
// Turn on/off and route Vpp to OBD2
// Input:   VppState state (Vpp_OFF, Vpp_FEPS, Vpp_SCI_6, Vpp_SCI_9, Vpp_SCI_12, Vpp_SCI_14)
//          FunctionalState calib (ENABLE, ON, DISABLE, OFF) - calibrate Vpp
// Engineer: Quyen Leba
// Date: Jan 28, 2008
//------------------------------------------------------------------------------
u8 Vpp_Ctrl(VppState state)
{
    Vpp_Switch(OFF);
    //Vpp_Set20V();
    switch(state)
    {
    case Vpp_OFF:
        GPIO_WriteBit(FEPSPORT,FEPS_PIN,Bit_RESET);
        GPIO_WriteBit(SCIPWRPORT,SCI_6_PWR_PIN,Bit_RESET);
        GPIO_WriteBit(SCIPWRPORT,SCI_9_PWR_PIN,Bit_RESET);
        GPIO_WriteBit(SCIPWRPORT,SCI_12_PWR_PIN,Bit_RESET);
        GPIO_WriteBit(SCIPWRPORT,SCI_14_PWR_PIN,Bit_RESET);
        break;
    case Vpp_FEPS:
        Vpp_Switch(ON);
        Vpp_Set18V();
        GPIO_WriteBit(FEPSPORT,FEPS_PIN,Bit_SET);
        delays(50,'m');
        break;
    case Vpp_SCI_6:
        Vpp_Switch(ON);
        Vpp_Set20V();
        GPIO_WriteBit(SCIPWRPORT,SCI_6_PWR_PIN,Bit_SET);
        break;
    case Vpp_SCI_9:
        Vpp_Switch(ON);
        Vpp_Set20V();
        GPIO_WriteBit(SCIPWRPORT,SCI_9_PWR_PIN,Bit_SET);
        break;
    case Vpp_SCI_12:
        Vpp_Switch(ON);
        Vpp_Set20V();
        GPIO_WriteBit(SCIPWRPORT,SCI_12_PWR_PIN,Bit_SET);
        break;
    case Vpp_SCI_14:
        Vpp_Switch(ON);
        Vpp_Set20V();
        GPIO_WriteBit(SCIPWRPORT,SCI_14_PWR_PIN,Bit_SET);
        break;
    default:
        GPIO_WriteBit(FEPSPORT,FEPS_PIN,Bit_RESET);
        GPIO_WriteBit(SCIPWRPORT,SCI_6_PWR_PIN,Bit_RESET);
        GPIO_WriteBit(SCIPWRPORT,SCI_9_PWR_PIN,Bit_RESET);
        GPIO_WriteBit(SCIPWRPORT,SCI_12_PWR_PIN,Bit_RESET);
        GPIO_WriteBit(SCIPWRPORT,SCI_14_PWR_PIN,Bit_RESET);
        break;
    }
    return S_SUCCESS;
}

//------------------------------------------------------------------------------
// Turn on & off 5V supply for external analog inputs
// Input:   u8 state (ENABLE, ON, DISABLE, OFF)
// Engineer: Quyen Leba
// Date: Jan 28, 2008
//------------------------------------------------------------------------------
void V5VExt_Ctrl(FunctionalState state)
{
    if (state == OFF)
    {
        GPIO_WriteBit(V5VPORT,V5V_PIN,Bit_RESET);
    }
    else
    {
        GPIO_WriteBit(V5VPORT,V5V_PIN,Bit_SET);
    }
}

//------------------------------------------------------------------------------
// Turn on & off Vpp, also act as a primer
// Input:   FunctionalState state (ENABLE, ON, DISABLE, OFF)
// Engineer: Quyen Leba
// Date: Jan 28, 2008
// TODO: it's not totally off
//------------------------------------------------------------------------------
void Vpp_Switch(FunctionalState state)
{
    //disable Vpp routing all other ports
    GPIO_WriteBit(SCIPWRPORT,SCI_6_PWR_PIN,Bit_RESET);
    GPIO_WriteBit(SCIPWRPORT,SCI_9_PWR_PIN,Bit_RESET);
    GPIO_WriteBit(SCIPWRPORT,SCI_12_PWR_PIN,Bit_RESET);
    GPIO_WriteBit(SCIPWRPORT,SCI_14_PWR_PIN,Bit_RESET);
    GPIO_WriteBit(FEPSPORT,FEPS_PIN,Bit_RESET);
    
    if (state == ON || state == ENABLE)
    {
        timer2_control(ON);
        timer2_pwm(1023, 700);
    }
    else
    {
        timer2_pwm(1023, 0);
        timer2_control(OFF);
    }
}

//------------------------------------------------------------------------------
// Set Vpp to 18V (DC = 69.7431%)
// Engineer: Quyen Leba
// Date: Jan 28, 2008
//------------------------------------------------------------------------------
void Vpp_Set18V(void)
{
    TIM_InitTypeDef TIM_InitStructure;
    
    TIM_CounterCmd(TIM2, TIM_STOP);
    
    TIM_StructInit(&TIM_InitStructure);
    // TIM2 Configuration in PWM Mode
    TIM_InitStructure.TIM_Mode = TIM_PWM;
    TIM_InitStructure.TIM_Clock_Source = TIM_CLK_APB;
    TIM_InitStructure.TIM_Prescaler = 0x0;
    TIM_InitStructure.TIM_Pulse_Level_1 = TIM_HIGH;
    TIM_InitStructure.TIM_Period_Level = TIM_LOW;
    TIM_InitStructure.TIM_Pulse_Length_1 = 745;
    TIM_InitStructure.TIM_Full_Period = 1023;
    TIM_Init (TIM2, &TIM_InitStructure);
    TIM_Init (TIM2, &TIM_InitStructure);
    
    TIM_CounterCmd(TIM2, TIM_START);
    
    delays(100,'u');
}

//------------------------------------------------------------------------------
// Set Vpp to 20V (20V: DC = 77.4923%)
// Engineer: Quyen Leba
// Date: Jan 28, 2008
//------------------------------------------------------------------------------
void Vpp_Set20V(void)
{
    TIM_InitTypeDef TIM_InitStructure;
    
    TIM_CounterCmd(TIM2, TIM_STOP);
    
    TIM_StructInit(&TIM_InitStructure);
    // TIM2 Configuration in PWM Mode
    TIM_InitStructure.TIM_Mode = TIM_PWM;
    TIM_InitStructure.TIM_Clock_Source = TIM_CLK_APB;
    TIM_InitStructure.TIM_Prescaler = 0x0;
    TIM_InitStructure.TIM_Pulse_Level_1 = TIM_HIGH;
    TIM_InitStructure.TIM_Period_Level = TIM_LOW;
    TIM_InitStructure.TIM_Pulse_Length_1 = 830;
    TIM_InitStructure.TIM_Full_Period = 1023;
    TIM_Init (TIM2, &TIM_InitStructure);
    TIM_Init (TIM2, &TIM_InitStructure);
    
    TIM_CounterCmd(TIM2, TIM_START);
    
    delays(100,'u');
}

//------------------------------------------------------------------------------
// Engineer: Quyen Leba
// Date: Jan 28, 2008
//------------------------------------------------------------------------------
void Init_VPP(void)
{
    GPIO_InitTypeDef GPIO_InitStructure;
    
    // Config all VPP signals and turn them off
    GPIO_StructInit(&GPIO_InitStructure);
    GPIO_InitStructure.GPIO_Pin =
        GPIO_Pin_0 | GPIO_Pin_1 | GPIO_Pin_2 | GPIO_Pin_3;
    GPIO_InitStructure.GPIO_Direction = GPIO_PinOutput;
    GPIO_InitStructure.GPIO_Type = GPIO_Type_PushPull;
    //GPIO_InitStructure.GPIO_IPConnected = GPIO_IPConnected_Disable;
    GPIO_InitStructure.GPIO_Alternate = GPIO_OutputAlt1;
    GPIO_Init(GPIO2, &GPIO_InitStructure);
    GPIO_Write(GPIO2, 0x00);
    
    // Config all VPP signals and turn them off
    GPIO_StructInit(&GPIO_InitStructure);
    GPIO_InitStructure.GPIO_Pin = GPIO_Pin_4;
    GPIO_InitStructure.GPIO_Direction = GPIO_PinOutput;
    GPIO_InitStructure.GPIO_Type = GPIO_Type_PushPull;
    //GPIO_InitStructure.GPIO_IPConnected = GPIO_IPConnected_Disable;
    GPIO_InitStructure.GPIO_Alternate = GPIO_OutputAlt1;
    GPIO_Init(GPIO6, &GPIO_InitStructure);
    GPIO_WriteBit(GPIO6, GPIO_Pin_4, Bit_RESET);
    
    return;
}

// Report whether the device is running from OBD-2 power or USB power
// returns True if device is currently powered through USB (no OBD-2 power) 
bool Pwr_USB_powered(void)
{
    return((bool)(GPIO_Read(PWRSWSTATUSPORT) & PWRSWSTATUS_PIN));
    
}
