/**
  **************** (C) COPYRIGHT 2011 SCT Performance, LLC *********************
  * File Name          : delays.c
  * Device / System    : SCT products using the "CommonCode" platform
  * Author             : Quyen Leba
  *
  * Version            : 1 
  * Date               : 05/02/2011
  * Description        : 
  *                    : 
  *
  *
  * History            : 05/02/2011 M. Davis
  *                    :   Add SCT Copyright header,
  *
  ******************************************************************************
  */

#include <91x_tim.h>
#include <board/delays.h>

//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
void delays(u32 time, u8 prefix)
{
    u32 i;
    u32 p;
    
    if (prefix == 'n') p = time/100;    //very inaccurate, at least time = 200
    else if (prefix != 'u') p = time * 563;//(time * 1000)/2;
    else p = (time * 50)/87;//time/2;
    
    for(i=0;i<=p;i++)
    {
        TIM_ClearFlag(TIM0,TIM_FLAG_OC1);
        TIM_CounterCmd(TIM0, TIM_CLEAR);
        while (TIM_GetFlagStatus(TIM0,TIM_FLAG_OC1) == RESET);
    }
}
