/**
  **************** (C) COPYRIGHT 2011 SCT Performance, LLC *********************
  * File Name          : scp.c
  * Device / System    : SCT products using the "CommonCode" platform
  * Author             : Quyen Leba
  *
  * Version            : 1 
  * Date               : 05/02/2011
  * Description        : 
  *                    : 
  *
  *
  * History            : 05/02/2011 M. Davis
  *                    :   Add SCT Copyright header,
  *
  ******************************************************************************
  */

#include <string.h>
#include <91x_gpio.h>
#include <91x_tim.h>
#include <board/genplatform.h>
#include <board/timer.h>
#include <board/interrupt.h>
#include <common/statuscode.h>
#include <common/obd2.h>

#if (SUPPORT_COMM_SCP)

#include "scp.h"

bool debug_isscphighspeed = FALSE;

//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
// Pulse values of low speed 32-bit scp
//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
#define PWM_1_ACTIVE_LS_32BIT           (192 + 32)
#define PWM_0_ACTIVE_LS_32BIT           (480 - 8)
#define PWM_SOF_ACTIVE_LS_32BIT         (960 + 8)
#define PWM_BRK_ACTIVE_LS_32BIT         1280
#define BIT_TIME_LS_32BIT               (704 + 16)
#define SOF_EOD_TIME_LS_32BIT           (1472 - 10)

#define PWM_1_ACTIVE_MIN_LS_32BIT       120     //246
#define PWM_1_ACTIVE_MAX_LS_32BIT       220     //275

#define PWM_0_ACTIVE_MIN_LS_32BIT       390     //492
#define PWM_0_ACTIVE_MAX_LS_32BIT       608     //550
#define PWM_SOF_ACTIVE_MIN_LS_32BIT     928     //912
#define PWM_SOF_ACTIVE_MAX_LS_32BIT     1120    //1064
#define PWM_BRK_ACTIVE_MIN_LS_32BIT     1216
#define PWM_BRK_ACTIVE_MAX_LS_32BIT     1376

#define BIT_TIME_MIN_LS_32BIT           440
#define BIT_TIME_MAX_LS_32BIT           864
#define SOF_EOD_MIN_LS_32BIT            1444
#define SOF_EOD_MAX_LS_32BIT            2016
#define EOD_TIME_LS_32BIT               1440

//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
// Pulse values of high speed 32-bit scp
//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
#define PWM_1_ACTIVE_HS_32BIT           (101 + 0)
#define PWM_0_ACTIVE_HS_32BIT           (224 + 4)
#define PWM_SOF_ACTIVE_HS_32BIT         (464 + 20)  //30.25
#define PWM_BRK_ACTIVE_HS_32BIT         608
#define BIT_TIME_HS_32BIT               (342 + 24)
#define SOF_EOD_TIME_HS_32BIT           (716 + 36)  //31.33

#define PWM_1_ACTIVE_MIN_HS_32BIT       30
#define PWM_1_ACTIVE_MAX_HS_32BIT       110
#define PWM_0_ACTIVE_MIN_HS_32BIT       180
#define PWM_0_ACTIVE_MAX_HS_32BIT       350

#define PWM_SOF_ACTIVE_MIN_HS_32BIT     440
#define PWM_SOF_ACTIVE_MAX_HS_32BIT     580
/*
#define PWM_1_ACTIVE_MIN_HS_32BIT       94  //30          //94
#define PWM_1_ACTIVE_MAX_HS_32BIT       173 //110         //173
#define PWM_0_ACTIVE_MIN_HS_32BIT       220 //180         //220
#define PWM_0_ACTIVE_MAX_HS_32BIT       300 //350         //300

#define PWM_SOF_ACTIVE_MIN_HS_32BIT     477 //440         //477
#define PWM_SOF_ACTIVE_MAX_HS_32BIT     558 //580         //558
*/
#define PWM_BRK_ACTIVE_MIN_HS_32BIT     608
#define PWM_BRK_ACTIVE_MAX_HS_32BIT     388

#define BIT_TIME_MIN_HS_32BIT           352
#define BIT_TIME_MAX_HS_32BIT           432
#define SOF_EOD_MIN_HS_32BIT            736
#define SOF_EOD_MAX_HS_32BIT            1008
#define EOD_TIME_HS_32BIT               672

#define PWM_1_ACTIVE_HS_2_32BIT         96
#define PWM_0_ACTIVE_HS_2_32BIT         224
#define PWM_SOF_ACTIVE_HS_2_32BIT       480
#define PWM_BRK_ACTIVE_HS_2_32BIT       608
#define BIT_TIME_HS_2_32BIT             352
#define SOF_EOD_TIME_HS_2_32BIT         736
#define EOD_TIME_HS_2_32BIT             736

typedef union
{
    u16 tp[5];
    struct
    {
        u16 SOF_Active;
        u16 SOF_EOD_Time;
        u16 Pulse_1_Active;
        u16 Pulse_0_Active;
        u16 Bit_Time;
    }pulse;
}SCPTXPULSE_Struct;

typedef union
{
    u16 rp[10];
    struct
    {
        u16 SOF_Active_Min;
        u16 SOF_Active_Max;
        u16 Pulse_0_Active_Min;
        u16 Pulse_0_Active_Max;
        u16 SOF_EOD_Time;
        u16 Pulse_1_Active;
        u16 Pulse_0_Active;
        u16 Bit_Time;
        u16 SOF_EOD_Min;
        u16 SOF_EOD_Max;
    }pulse;
}SCPRXPULSE_Struct;

//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
#define SCP_MAX_RETRY       3
#define SCPPORT             GPIO6
#define SCPTXpPIN           GPIO_Pin_3
#define SCPTXnPIN           GPIO_Pin_7
#define SCPRXPIN            GPIO_Pin_2
#define SCPTIM              TIM1
#define __VPWTXPIN          GPIO_Pin_1
#define scp_inittimer()     timer1_init()

bool                        IFRLock         = FALSE;
u8                          SCP_NodeAddr    = SCP_DEVICENODE_RESPONSE_ID;
u8                          SCP_ECUAddr     = SCP_ECM_ID;

SCPTXPULSE_Struct           *pSCP_TX_Pulse  = NULL;
SCPRXPULSE_Struct           *pSCP_RX_Pulse  = NULL;

//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
const SCPTXPULSE_Struct     SCP_TX_Pulse_24Bit_HS =
{
    .tp[0] = 2*(PWM_SOF_ACTIVE_HS_24BIT*23)/31,
    .tp[1] = 2*(SOF_EOD_TIME_HS_24BIT*23)/31,
    .tp[2] = 2*(PWM_1_ACTIVE_HS_24BIT*23)/31,
    .tp[3] = 2*(PWM_0_ACTIVE_HS_24BIT*23)/31,
    .tp[4] = 2*(BIT_TIME_HS_24BIT*23)/31,
};

const SCPRXPULSE_Struct     SCP_RX_Pulse_24Bit_HS =
{
    .rp[0] = 2*(PWM_SOF_ACTIVE_MIN_HS_24BIT*23)/31,
    .rp[1] = 2*(PWM_SOF_ACTIVE_MAX_HS_24BIT*23)/31,
    .rp[2] = 2*(PWM_0_ACTIVE_MIN_HS_24BIT*23)/31,
    .rp[3] = 2*(PWM_0_ACTIVE_MAX_HS_24BIT*23)/31,
    .rp[4] = 2*(EOD_TIME_HS_24BIT*23)/31,
    .rp[5] = 2*(PWM_1_ACTIVE_HS_24BIT*23)/31,
    .rp[6] = 2*(PWM_0_ACTIVE_HS_24BIT*23)/31,
    .rp[7] = 2*(BIT_TIME_HS_24BIT*23)/31,
    .rp[8] = 2*(SOF_EOD_MIN_HS_24BIT*23)/31,
    .rp[9] = 2*(SOF_EOD_MAX_HS_24BIT*23)/31,
};

const SCPTXPULSE_Struct     SCP_TX_Pulse_24Bit_LS =
{
    .tp[0] = 2*(PWM_SOF_ACTIVE_LS_24BIT*24)/31,
    .tp[1] = 2*(SOF_EOD_TIME_LS_24BIT*24)/31,
    .tp[2] = 2*(PWM_1_ACTIVE_LS_24BIT*24)/31,
    .tp[3] = 2*(PWM_0_ACTIVE_LS_24BIT*24)/31,
    .tp[4] = 2*(BIT_TIME_LS_24BIT*24)/31,
};

const SCPRXPULSE_Struct     SCP_RX_Pulse_24Bit_LS =
{
    .rp[0] = 2*(PWM_SOF_ACTIVE_MIN_LS_24BIT*24)/31,
    .rp[1] = 2*(PWM_SOF_ACTIVE_MAX_LS_24BIT*24)/31,
    .rp[2] = 2*(PWM_0_ACTIVE_MIN_LS_24BIT*24)/31,
    .rp[3] = 2*(PWM_0_ACTIVE_MAX_LS_24BIT*24)/31,
    .rp[4] = 2*(EOD_TIME_LS_24BIT*24)/31,
    .rp[5] = 2*(PWM_1_ACTIVE_LS_24BIT*24)/31,
    .rp[6] = 2*(PWM_0_ACTIVE_LS_24BIT*24)/31,
    .rp[7] = 2*(BIT_TIME_LS_24BIT*24)/31,
    .rp[8] = 2*(SOF_EOD_MIN_LS_24BIT*24)/31,
    .rp[9] = 2*(SOF_EOD_MAX_LS_24BIT*24)/31,
};

//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
const SCPTXPULSE_Struct     SCP_TX_Pulse_32Bit_HS =
{
    .tp[0] = 2*(PWM_SOF_ACTIVE_HS_32BIT*23)/31,
    .tp[1] = 2*(SOF_EOD_TIME_HS_32BIT*23)/31,
    .tp[2] = 2*(PWM_1_ACTIVE_HS_32BIT*23)/31,
    .tp[3] = 2*(PWM_0_ACTIVE_HS_32BIT*23)/31,
    .tp[4] = 2*(BIT_TIME_HS_32BIT*23)/31,
};

const SCPRXPULSE_Struct     SCP_RX_Pulse_32Bit_HS =
{
    .rp[0] = 2*(PWM_SOF_ACTIVE_MIN_HS_32BIT*23)/31,
    .rp[1] = 2*(PWM_SOF_ACTIVE_MAX_HS_32BIT*23)/31,
    .rp[2] = 2*(PWM_0_ACTIVE_MIN_HS_32BIT*23)/31,
    .rp[3] = 2*(PWM_0_ACTIVE_MAX_HS_32BIT*23)/31,
    .rp[4] = 2*(EOD_TIME_HS_32BIT*23)/31,
    .rp[5] = 2*(PWM_1_ACTIVE_HS_32BIT*23)/31,
    .rp[6] = 2*(PWM_0_ACTIVE_HS_32BIT*23)/31,
    .rp[7] = 2*(BIT_TIME_HS_32BIT*23)/31,
    .rp[8] = 2*(SOF_EOD_MIN_HS_32BIT*23)/31,
    .rp[9] = 2*(SOF_EOD_MAX_HS_32BIT*23)/31,
};

const SCPTXPULSE_Struct     SCP_TX_Pulse_32Bit_LS =
{
    .tp[0] = 2*(PWM_SOF_ACTIVE_LS_32BIT*24)/31,
    .tp[1] = 2*(SOF_EOD_TIME_LS_32BIT*24)/31,
    .tp[2] = 2*(PWM_1_ACTIVE_LS_32BIT*24)/31,
    .tp[3] = 2*(PWM_0_ACTIVE_LS_32BIT*24)/31,
    .tp[4] = 2*(BIT_TIME_LS_32BIT*24)/31,
};

const SCPRXPULSE_Struct     SCP_RX_Pulse_32Bit_LS =
{
    .rp[0] = 2*(PWM_SOF_ACTIVE_MIN_LS_32BIT*24)/31,
    .rp[1] = 2*(PWM_SOF_ACTIVE_MAX_LS_32BIT*24)/31,
    .rp[2] = 2*(PWM_0_ACTIVE_MIN_LS_32BIT*24)/31,
    .rp[3] = 2*(PWM_0_ACTIVE_MAX_LS_32BIT*24)/31,
    .rp[4] = 2*(EOD_TIME_LS_32BIT*24)/31,
    .rp[5] = 2*(PWM_1_ACTIVE_LS_32BIT*24)/31,
    .rp[6] = 2*(PWM_0_ACTIVE_LS_32BIT*24)/31,
    .rp[7] = 2*(BIT_TIME_LS_32BIT*24)/31,
    .rp[8] = 2*(SOF_EOD_MIN_LS_32BIT*24)/31,
    .rp[9] = 2*(SOF_EOD_MAX_LS_32BIT*24)/31,
};

//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
#define SCP_IN()              GPIO_ReadBit(SCPPORT,SCPRXPIN)
void SCP_timer_start();
void SCP_timer_stop();
u16 SCP_timer_get();
u8 SCP_CheckIdleBus(u16 time);
u8 scp_ifr_rx(u8 id);

u8 scp_recv(u8 *databuffer, u16 *datalength, u8 mode, u8 nodeaddress);

//------------------------------------------------------------------------------
// Init GPIO for SCP, isolate VPW from J1850 bus
// Inputs:  SCP_TYPE scptype
//          SCP_SPEED scpspeed
// Engineer: Quyen Leba
// Date: Jan 30, 2008
//------------------------------------------------------------------------------
void scp_init(SCP_TYPE scptype, SCP_SPEED scpspeed)
{
    GPIO_InitTypeDef GPIO_InitStructure;

    //Config SCP_TX & VPW_TX
    GPIO_StructInit(&GPIO_InitStructure);
    GPIO_InitStructure.GPIO_Pin = SCPTXpPIN | SCPTXnPIN | __VPWTXPIN;
    GPIO_InitStructure.GPIO_Direction = GPIO_PinOutput;
    GPIO_InitStructure.GPIO_Type = GPIO_Type_PushPull;
    GPIO_InitStructure.GPIO_Alternate = GPIO_OutputAlt1;
    GPIO_Init(SCPPORT, &GPIO_InitStructure);
    //Config SCP_RX
    GPIO_StructInit(&GPIO_InitStructure);
    GPIO_InitStructure.GPIO_Pin = SCPRXPIN;
    GPIO_InitStructure.GPIO_Direction = GPIO_PinInput;
    GPIO_InitStructure.GPIO_Type = GPIO_Type_PushPull;
    GPIO_Init(SCPPORT, &GPIO_InitStructure);
    //Make sure PWM_TXp does not interfere with VPW_TX
    GPIO_WriteBit(SCPPORT,SCPTXpPIN,Bit_SET);
    GPIO_WriteBit(SCPPORT,SCPTXnPIN,Bit_RESET);
    GPIO_WriteBit(SCPPORT,__VPWTXPIN,Bit_RESET);

    if (scpspeed == SCP_HIGH_SPEED)
    {
        debug_isscphighspeed = TRUE;
        if (scptype == SCP_TYPE_32BIT)
        {
            pSCP_TX_Pulse = (SCPTXPULSE_Struct*)&SCP_TX_Pulse_32Bit_HS;
            pSCP_RX_Pulse = (SCPRXPULSE_Struct*)&SCP_RX_Pulse_32Bit_HS;
        }
        else
        {
            pSCP_TX_Pulse = (SCPTXPULSE_Struct*)&SCP_TX_Pulse_24Bit_HS;
            pSCP_RX_Pulse = (SCPRXPULSE_Struct*)&SCP_RX_Pulse_24Bit_HS;
        }
    }
    else
    {
        debug_isscphighspeed = FALSE;
        if (scptype == SCP_TYPE_32BIT)
        {
            pSCP_TX_Pulse = (SCPTXPULSE_Struct*)&SCP_TX_Pulse_32Bit_LS;
            pSCP_RX_Pulse = (SCPRXPULSE_Struct*)&SCP_RX_Pulse_32Bit_LS;
        }
        else
        {
            pSCP_TX_Pulse = (SCPTXPULSE_Struct*)&SCP_TX_Pulse_24Bit_LS;
            pSCP_RX_Pulse = (SCPRXPULSE_Struct*)&SCP_RX_Pulse_24Bit_LS;
        }
    }

    scp_inittimer();
}

//------------------------------------------------------------------------------
// Reset timer counter
// Engineer: Quyen Leba
// Date: Jan 29, 2008
//------------------------------------------------------------------------------
static inline void SCP_timer_start(void)
{
    TIM_CounterCmd(SCPTIM, TIM_START);
    SCPTIM->CNTR = 0;    //write to clear
}
//------------------------------------------------------------------------------
// Stop timer
// Engineer: Quyen Leba
// Date: Jan 29, 2008
//------------------------------------------------------------------------------
//static inline void SCP_timer_stop(void)
//{
//    TIM_CounterCmd(SCPTIM, TIM_STOP);
//}
//------------------------------------------------------------------------------
// Get current count value of timer
// Engineer: Quyen Leba
// Date: Jan 29, 2008
//------------------------------------------------------------------------------
static inline u16 SCP_timer_get(void)
{
	return ((u16)(SCPTIM->CNTR) + 24);      //+24 to calibrate it with the time to set/reset pin
    //return ((u16)(SCPTIM->CNTR));
}

//------------------------------------------------------------------------------
// Checks to see if bus is in idle state
// Input:   u16 time (PWM_EOD_MAX)
// Return:  u8  status
//------------------------------------------------------------------------------
u8 SCP_CheckIdleBus(u16 time)
{
    u32 i;

    SCP_timer_start();
    for(i=0;i<100000;i++)
    {
        while(!SCP_IN())    // Wait for rising edge
        {
            if(SCP_timer_get() >= time) // watch for IFS from passive pulse length
            {
                return S_SUCCESS;       // It is IFS start transmitting		
            }
        }
        SCP_timer_start();
    }
    return S_TIMEOUT;
}

//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
void scp_lock_ifr()
{
    IFRLock = TRUE;
}

void scp_unlock_ifr()
{
    IFRLock = FALSE;
}

//------------------------------------------------------------------------------
// Input:   u8  id
//
// Used to send messages on the SCP bus. The buffer being passed to this 
// fucntion should be formatted as J1850 spec requires.
//
// Ex. Priority Byte - Dest Addr - node_addr - '8 byte max message'
// * CRC will be appended automatically within function
//
// Parameters to Pass:
// - 1st pass pointer to message buffer containing J1850 message excluding CRC. 
// - 2nd pass number of bytes in the message excluding CRC.                                                                             
//------------------------------------------------------------------------------
u8 scp_ifr_rx(u8 id)
{
    u8  bit;
    u16 pulse;
    u8  rx_ifr;

    // Msg was sent, watch for IFR
    for(bit=0;bit<8;bit++)
    {
        rx_ifr = rx_ifr << 1;       // Shift in bits
        SCP_timer_start();          // Reset timer 1 for use w/ timeout

        while(SCP_IN() == Bit_RESET)    // Wait for rising edge
        {
            if(SCP_timer_get()>((2419*24)/31)/*2016*/)    //39us
            {
                // Timeout after 51us the max time allowed for EOD
                // may need to change to HS/LS varible
                return S_TIMEOUT;
            }
        }

        SCP_timer_start();
        while(SCP_IN() != Bit_RESET)    // Wait for falling edge
        {
            if(SCP_timer_get()>((2016*24)/31)/*2016*/)
            {
                // Timeout after 51us or bit periods
                return S_TIMEOUT;
            }
        }

        pulse = SCP_timer_get() + 10 + 8;
        if(pulse < pSCP_RX_Pulse->pulse.Pulse_0_Active_Min)
        {
            // Received a '1'
            rx_ifr |= 0x01;
        }
        else if(pulse <= pSCP_RX_Pulse->pulse.Pulse_0_Active_Max)
        {
            // Recieved '0'
            // do nothing
        }
        else
        {
            // Jump straight to error and restart recieve function
            return S_FAIL;
        }
    }

    if(rx_ifr == id)
    {
        return S_SUCCESS;
    }
    else
    {
        if (IFRLock == TRUE)
        {
            return S_SUCCESS;
        }
        return S_BADCONTENT;
    }
}

//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
/*__arm __interwork*/
u8 scp_tx(u8 *databuffer, u16 datalength, u8 mode)
{
    u8  txbuffer[SCP_MAX_TX_MSG_LENGTH+1];
    u8  retry;
    u8  txattempt;
    u8  ifrattempt;
    u8  status;

    if (datalength > SCP_MAX_TX_MSG_LENGTH)
    {
        return S_INPUT;
    }

    memcpy(txbuffer,databuffer,datalength);
    txbuffer[datalength] = obd2_j1850crc(databuffer, datalength);
    datalength++;

    txattempt = 0;
    ifrattempt = 0;
scp_tx_begin:
    interrupt_ctrl(ENABLE);
    retry = 0;
    while(retry++ <= SCP_MAX_RETRY)
    {
        if (SCP_CheckIdleBus(pSCP_RX_Pulse->pulse.SOF_EOD_Max) == S_SUCCESS)
        {
            break;
        }
    }
    if (retry > SCP_MAX_RETRY)
    {
        status = S_TIMEOUT;
        goto scp_tx_done;
    }

    interrupt_ctrl(DISABLE);
    if (scp_Tx(txbuffer,datalength,pSCP_TX_Pulse->tp))
    {
        txattempt++;
        if (txattempt > 3)
        {
            status = S_FAIL;
            goto scp_tx_done;
        }
        goto scp_tx_begin;
    }
    
    if (mode == SCP_MODE_NODE)
    {
        status = scp_ifr_rx(databuffer[1]);
    }
    else    //SCP_MODE_FUNCTIONAL
    {
        status = scp_ifr_rx(0x10);
    }
    interrupt_ctrl(ENABLE);
    
    if (status != S_SUCCESS)
    {
        ifrattempt++;
        if (ifrattempt > SCP_MAX_RETRY)
        {
            status = S_FAIL;
            goto scp_tx_done;
        }
        goto scp_tx_begin;
    }

scp_tx_done:
    interrupt_ctrl(ENABLE);
    return status;
}

//------------------------------------------------------------------------------
// Input:   u8  mode (SCP_MODE_NODE, SCP_MODE_FUNCTIONAL)
// Outputs: u8  *databuffer
//          u16 *datalength
//          u8  mode (SCP_MODE_NODE or SCP_MODE_FUNCTIONAL)
//          u8  nodeaddress (only applicable if mode is SCP_MODE_NODE)
// Return:  u8  status
//
// Used to send messages on the SCP bus. The buffer being passed to this 
// fucntion should be formatted as J1850 spec requires.
//
// Ex. Priority Byte - Dest Addr - node_addr - '8 byte max message'
// * CRC will be appended automatically within function
//------------------------------------------------------------------------------
__arm __interwork u8 scp_recv(u8 *databuffer, u16 *datalength,
                              u8 mode, u8 nodeaddress)
{
    u8  status;
    u16 databytecount;
    u8  msg_crc;
    u8  calc_crc;
    u8  buffer[24];

    *datalength = 0;

    interrupt_ctrl(DISABLE);

    if (mode == SCP_MODE_NODE)
    {
        if (debug_isscphighspeed)
        {
            databytecount = scp_Rx(buffer, pSCP_RX_Pulse->rp, nodeaddress);
        }
        else
        {
            databytecount = scp_Rx(buffer, pSCP_RX_Pulse->rp, nodeaddress);
        }
        //databytecount = scp_Rx(buffer, pSCP_RX_Pulse->rp, nodeaddress);
    }
    else//SCP_MODE_FUNCTIONAL
    {
        if (debug_isscphighspeed)
        {
            databytecount = scp_Rx(buffer, pSCP_RX_Pulse->rp, SCP_FuncAddrRsp);
        }
        else
        {
            databytecount = scp_Rx(buffer, pSCP_RX_Pulse->rp, SCP_FuncAddrRsp);
        }
        //databytecount = scp_Rx(buffer, pSCP_RX_Pulse->rp, SCP_FuncAddrRsp);
    }

    if(databytecount != 0xff)
    {
        databytecount--;
    }
    else
    {
        status = S_FAIL;
        goto scp_recv_error;
    }

    if (databytecount > SCP_MAX_RX_MSG_LENGTH)
    {
        status = S_BADCONTENT;
        goto scp_recv_error;
    }

    msg_crc = buffer[databytecount];
    calc_crc = obd2_j1850crc(buffer,databytecount);
    
    if (msg_crc != calc_crc)
    {
        status = S_BADCRCFAIL;
        goto scp_recv_error;
    }
    *datalength = databytecount;
    memcpy(databuffer,buffer,databytecount);
    
    interrupt_ctrl(ENABLE);
    return S_SUCCESS;

scp_recv_error:
    interrupt_ctrl(ENABLE);
    return status;
}

//------------------------------------------------------------------------------
// Wait for a SCP message with timeout
// TODOQ: clean up this
//------------------------------------------------------------------------------
u8 scp_rx(u8 *databuffer, u16 *datalength, u8 mode, u8 nodeaddress)
{
    u8  status;
    u32 timeout = 500;//32//16;

    while(timeout)
    {
        status = scp_recv(databuffer,datalength,mode,nodeaddress);
        if (status == S_SUCCESS)
        {
            break;
        }
        else
        {
            timeout--;
        }
    }
    if (timeout == 0)
    {
        return S_FAIL;
    }
    else
    {
        return S_SUCCESS;
    }
}

#endif  //#if (SUPPORT_COMM_SCP)
