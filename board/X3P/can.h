/**
  **************** (C) COPYRIGHT 2011 SCT Performance, LLC *********************
  * File Name          : can.h
  * Device / System    : SCT products using the "CommonCode" platform
  * Author             : Quyen Leba
  *
  * Version            : 1 
  * Date               : 05/02/2011
  * Description        : 
  *                    : 
  *
  *
  * History            : 05/02/2011 M. Davis
  *                    :   Add SCT Copyright header,
  *
  ******************************************************************************
  */

#ifndef __CAN_H
#define __CAN_H

#include "91x_can.h"

#define CanTxMsg                	    canmsg
#define CanRxMsg                	    canmsg

#define CanMsgParseStdFrame(id)         {CAN_STD_ID, id, 8,}
#define CanMsgParseExtFrame(id,len)     {CAN_EXT_ID, id, len,}
#define CanMsgFrameData(frame)          frame.Data
#define CanMsgFrameDataLength(frame)    frame.Dlc
#define CanMsgFrameStandardAddr(frame)  frame.Id
#define CanMsgFrameExtendedAddr(frame)  frame.Id

extern CanRxMsg can_rxframe;

#define RX_EL_TIME_OUT                  100000
#define RX_L_TIME_OUT                   10000
#define RX_M_TIME_OUT                   4000
#define RX_S_TIME_OUT                   2000

typedef enum
{
    CAN_TX_MSGOBJ           = 10,
    CAN_TX_EXTOBJ           = 12,
    CAN_RX_MSGOBJ_NORMAL    = 15,
    CAN_RX_MSGOBJ_DATALOG   = 16,
    CAN_RX_MSGOBJ_EXT       = 18,
}CAN_MSGOBJ;

typedef enum
{
    CAN_INITMODE_NORMAL,
    CAN_INITMODE_7EX,
}CAN_INITMODE;

void can_init(CAN_INITMODE initmode);
void can_deinit();
void can_setTxMsgObj(CAN_MSGOBJ msgobj);
void can_setRxMsgObj(CAN_MSGOBJ msgobj);
u8 can_tx_s(CanTxMsg TxMessage);
u8 can_tx(CanTxMsg TxMessage);
u8 can_tx_ext(CanTxMsg txframe);
u8 can_rx(u32 timeout);
u8 can_rx_ext(u32 timeout);

#endif  //__CAN_H
