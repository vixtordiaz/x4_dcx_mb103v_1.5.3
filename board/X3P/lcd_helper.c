/**
  **************** (C) COPYRIGHT 2011 SCT Performance, LLC *********************
  * File Name          : lcd_helper.c
  * Device / System    : SCT products using the "CommonCode" platform
  * Author             : Quyen Leba
  *
  * Version            : 1 
  * Date               : 05/02/2011
  * Description        : 
  *                    : 
  *
  *
  * History            : 05/02/2011 M. Davis
  *                    :   Add SCT Copyright header,
  *
  ******************************************************************************
  */

#include <91x_type.h>
#include <common/statuscode.h>
#include "system.h"
#include "lcd.h"
#include "lcd_helper.h"

//------------------------------------------------------------------------------
// This routine grabs the image file given from filesystem and put on screen
// Return: u8 status (S_FAIL,S_SUCCESS)
// Engineer: Rhonda Burns
// Date: 8/2/06
//------------------------------------------------------------------------------
u8 gputimg(const u8 *filename,
           u8 z, //coordination
           u8 w)
{
    F_FILE  *imgfile;
    u8      fluff[4], x,y;
    u8      blank, error = S_SUCCESS;
    SYMBOL  *image;

    image = (SYMBOL*)__malloc(sizeof(SYMBOL));

    //gclrvp();                                                           // Clear display
    //gsetpos(z,w);                                                       // Set cusor position

    if((imgfile = __fopen((void*)filename,"r"))!= NULL)                  // Open file
    {
        f_read(fluff, 1, sizeof(fluff), imgfile);                       // Do not need first 4 bytes
        if((f_read(&x,1,sizeof(x),imgfile)) == 1)                       // Get size of x cooridinate
        {
            image->xycoor.x = x;                                        // Put x in structure
            f_read(&blank, 1, sizeof(blank), imgfile);                  // Don't need this byte
            f_read(&y,1,sizeof(y),imgfile);                             // Grab size of y cooridinate
            f_read(&blank, 1, sizeof(blank), imgfile);                  // Don't need this byte
            image->xycoor.y = y;                                        // Put y in structure
            if((f_read(image->b, 1, sizeof(image->b), imgfile)) > 1)    // Get body of image
            {
                LCD_gputsym(z,w,(PGSYMBOL)image);                       // Put image on screen
            }
            else
            {
                error = S_FAIL;
            }
        }
        else
        {
            error = S_FAIL;
        }

        __fclose(imgfile);                                                    // Close file

    }
    else
    {
        error = S_FAIL;
    }

    __free(image);                                                        // __free memory

    return(error);
}

//------------------------------------------------------------------------------
// Clear an area by filling with space
//------------------------------------------------------------------------------
void gclearareafast(u8 c1, u8 l1, u8 c2, u8 l2)
{
    u8 i,j;
    
    for (i=l1;i<l2;i++)
    {
        LCD_gsetcpos(c1,i);
        for (j=c1;j<c2;j++)
        {
            LCD_gputs(" ");
        }
    }
}

//------------------------------------------------------------------------------
// Print string on LCD at pixel position (x,y)
//------------------------------------------------------------------------------
void gputsrpos(u8 x, u8 y, u8* text)
{
    LCD_gsetpos(x, y);
    LCD_gputs((void*)text);
}

//------------------------------------------------------------------------------
// Print string on LCD at char position (x,y)
//------------------------------------------------------------------------------
void gputscpos(u8 x, u8 y, u8* text)
{
    LCD_gsetcpos(x, y);
    LCD_gputs((void*)text);
}
