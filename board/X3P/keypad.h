/**
  **************** (C) COPYRIGHT 2011 SCT Performance, LLC *********************
  * File Name          : keypad.h
  * Device / System    : SCT products using the "CommonCode" platform
  * Author             : Quyen Leba
  *
  * Version            : 1 
  * Date               : 05/02/2011
  * Description        : 
  *                    : 
  *
  *
  * History            : 05/02/2011 M. Davis
  *                    :   Add SCT Copyright header,
  *
  ******************************************************************************
  */

#ifndef __KEYPAD_H
#define __KEYPAD_H

#include <arch/gentype.h>

enum
{
    KEYUP           = 0xF4,     //P5.3
    KEYDOWN         = 0xBC,     //P5.6
    KEYLEFT         = 0xEC,     //P5.4
    KEYRIGHT        = 0xDC,     //P5.5
    KEYENTER        = 0x7C,     //P5.7
    KEYSELECT       = 0x7C,     //P5.7
    KEYCANCEL       = 0xF8,     //P5.2
    KEYALL          = 0xFC,
    KEYNONE         = 0xFF,
    KEYNOTREQUIRED  = 0xFA,
};

#define KEYPAD_ALLOWABORT   TRUE
#define KEYPAD_DENYABORT    FALSE

#define getKey()            keypad_poll()
#define getKeyWait(k,ax)    keypad_pollwait(k,ax)

#endif