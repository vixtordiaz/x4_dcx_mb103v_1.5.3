/**
  **************** (C) COPYRIGHT 2011 SCT Performance, LLC *********************
  * File Name          : usb_descriptors.h
  * Device / System    : SCT products using the "CommonCode" platform
  * Author             : Quyen Leba
  *
  * Version            : 1 
  * Date               : 05/02/2011
  * Description        : 
  *                    : 
  *
  *
  * History            : 05/02/2011 M. Davis
  *                    :   Add SCT Copyright header,
  *
  ******************************************************************************
  */

/******************** (C) COPYRIGHT 2008, 2009 SCT LLC *************************
* File Name          : usb_hw.h
* Device / System    : X3-Plus Tuner 
* Author             : J. Hess, R. Rusak, P. Downs, Q.Leba, D. Lin, M. Davis
* Version            : 
* Date               : 9/2/08
* Description        : Public vars, functions, defs, structs, etc. for usb_hw.c
*
* Rev History, most recent first
* $History: /CommonCode/Mico/board/X3P/usb_descriptors.h $
*  
*  ****************** Version 1 ****************** 
*  User: qleba   Date: 2011-05-17   Time: 23:01:24-04:00 
*  Updated in: /CommonCode/Mico/board/X3P 
*  
*  ****************** Version 1 ****************** 
*  User: qleba   Date: 2010-11-01   Time: 12:21:09-04:00 
*  Updated in: /mico/board/X3P 
*  
*  ****************** Version 4 ****************** 
*  User: mdavis   Date: 2009-03-12   Time: 12:13:08-04:00 
*  Updated in: /X3-PLUS/X3P/board 
*  fix USB 1.1 download problem. 
*  
*  ****************** Version 3 ****************** 
*  User: mdavis   Date: 2009-03-09   Time: 17:05:54-04:00 
*  Updated in: /X3-PLUS/X3P/board 
*  Fix broken USB 1.1 operation. 
*  
*  ****************** Version 2 ****************** 
*  User: mdavis   Date: 2009-02-20   Time: 20:25:18-05:00 
*  Updated in: /X3-PLUS/X3P/board 
*  Applying standardized documentation headers to all source files, using the 
*  SourceGear Vault "Keyword Expansion" facility. 
*
*******************************************************************************/

#ifndef __USB_DESCRIPTORS_H
#define __USB_DESCRIPTORS_H

#include <arch/gentype.h>

#define USB_VID                 0x5059
#define USB_PID                 0x3101

#define	ENDPT_NUMBER			2
#define	INTERFACE_NO			1

#define	HS_ENDPT_NUMBER			2
#define	HS_INTERFACE_NO			1

/*
#define	USB_FS_CONFIG_LENGTH    (sizeof(USB_CONFIGURATION_DESCRIPTOR) + \
                                INTERFACE_NO * (sizeof(USB_INTERFACE_DESCRIPTOR) + \
                                ENDPT_NUMBER * sizeof(USB_ENDPOINT_DESCRIPTOR)))

#define	USB_HS_CONFIG_LENGTH    sizeof(USB_CONFIGURATION_DESCRIPTOR) + \
                                HS_INTERFACE_NO * (sizeof(USB_INTERFACE_DESCRIPTOR) + \
                                HS_ENDPT_NUMBER * sizeof(USB_ENDPOINT_DESCRIPTOR))
*/

#define USB_DEVICE_DESCRIPTOR_LEN               18
#define USB_DEVICE_QUALIFIER_LEN                10
#define USB_CONFIGURATION_DESCRIPTOR_LEN        9
#define USB_OTHER_SPEED_CONFIG_DESCRIPTOR_LEN   9
#define USB_INTERFACE_DESCRIPTOR_LEN            9
#define USB_ENDPOINT_DESCRIPTOR_LEN             7
#define USB_STRING_ID_LEN                       4
#define USB_STRING_DESCRIPTOR_LEN               32

#define	USB_FS_CONFIG_LENGTH    USB_CONFIGURATION_DESCRIPTOR_LEN + \
                                INTERFACE_NO * USB_INTERFACE_DESCRIPTOR_LEN + \
                                ENDPT_NUMBER * USB_ENDPOINT_DESCRIPTOR_LEN

#define	USB_HS_CONFIG_LENGTH    USB_CONFIGURATION_DESCRIPTOR_LEN + \
                                HS_INTERFACE_NO * USB_INTERFACE_DESCRIPTOR_LEN + \
                                HS_ENDPT_NUMBER * USB_ENDPOINT_DESCRIPTOR_LEN


typedef struct __USB_DEVICE_DESCRIPTOR                  //18bytes
{
    u8  bLength;
    u8  bDescriptorType;
    u16 bcdUSB;
    u8  bDeviceClass;
    u8  bDeviceSubClass;
    u8  bDeviceProtocol;
    u8  bMaxPacketSize0;
    u16 idVendor;
    u16 idProduct;
    u16 bcdDevice;
    u8  iManufacturer;
    u8  iProduct;
    u8  iSerialNumber;
    u8  bNumConfigurations;
}USB_DEVICE_DESCRIPTOR;

typedef struct __USB_DEVICE_QUALIFIER                   //10bytes
{
    u8  bLength;            //length of HS Device Descriptor
    u8  bQualifier;         //HS Device Qualifier Type
    u16 wVersion;           //USB 2.0 version
    u8  bDeviceClass;       //Device class
    u8  bDeviceSubClasss;   //Device SubClass
    u8  bProtocol;          //Device Protocol Code
    u8  MaxPktSize;         //Maximum Packet SIze for other speed
    u8  bOther_Config;      //Number of Other speed configurations
    u8  Reserved;
}USB_DEVICE_QUALIFIER;

typedef struct __USB_CONFIGURATION_DESCRIPTOR           //9bytes
{
    u8  bLength;
    u8  bDescriptorType;
    u16 wTotalLength;
    u8  bNumInterfaces;
    u8  bConfigurationValue;
    u8  iConfiguration;
    u8  bmAttributes;
    u8  MaxPower;
}USB_CONFIGURATION_DESCRIPTOR;

typedef struct __USB_OTHER_SPEED_CONFIG_DESCRIPTOR      //9bytes
{
    u8  bLength;                    // length of other speed configuration descriptor
    u8  bDescriptorType;            // Other speed configuration Type
    u16 wTotalLength;               // Total length
    u8  bNumInterface;              // No of interface supported
    u8  bConfigurationValue;        // Configuration Value
    u8  String_Index_Descriptor;    // Index of string descriptor
    u8  bmConfigCharacteristic;     // bit-map... 7: always 1, 6: power source, 5: remote wake-up
    u8  bMaxPower;                  // Maximun Power
}USB_OTHER_SPEED_CONFIG_DESCRIPTOR;

typedef struct __USB_INTERFACE_DESCRIPTOR               //9bytes
{
    u8  bLength;
    u8  bDescriptorType;
    u8  bInterfaceNumber;
    u8  bAlternateSetting;
    u8  bNumEndpoints;
    u8  bInterfaceClass;
    u8  bInterfaceSubClass;
    u8  bInterfaceProtocol;
    u8  iInterface;
}USB_INTERFACE_DESCRIPTOR;

typedef struct __USB_ENDPOINT_DESCRIPTOR                //7bytes
{
    u8  bLength;
    u8  bDescriptorType;
    u8  bEndpointAddress;
    u8  bmAttributes;
    u16 wMaxPacketSize;
    u8  bInterval;
}USB_ENDPOINT_DESCRIPTOR;

typedef struct __USB_STRING_ID                          //4bytes
{
    u8  bLength;
    u8  bDescriptorType;
    u8  bString[2];
}USB_STRING_ID;

typedef struct __USB_STRING_DESCRIPTOR                  //32bytes max
{
    u8  bLength;
    u8  bDescriptorType;
    u8  bString[36];
}USB_STRING_DESCRIPTOR;

#define USB_DEFAULT         0
#define USB_CONFIGURED      1
#define USB_ADDRESSED       2

#define USB_HS              2
#define USB_FS              1
#define USB_LS              0

extern const USB_DEVICE_DESCRIPTOR              USB_FS_Device_Descriptor;
extern const USB_CONFIGURATION_DESCRIPTOR       USB_FS_Config_Descriptor;
extern const USB_INTERFACE_DESCRIPTOR           USB_FS_Interface_Descriptor_Default;
extern const USB_INTERFACE_DESCRIPTOR           USB_Interface_Descriptor_Default;
extern const USB_INTERFACE_DESCRIPTOR           USB_Interface_Descriptor_Alternate1;
extern const USB_INTERFACE_DESCRIPTOR           USB_Interface_Descriptor_Alternate2;
extern const USB_DEVICE_DESCRIPTOR              USB_HS_Device_Descriptor;
extern const USB_CONFIGURATION_DESCRIPTOR       USB_HS_Config_Descriptor;
extern const USB_INTERFACE_DESCRIPTOR           USB_HS_Interface_Descriptor_Default;
extern const USB_INTERFACE_DESCRIPTOR           USB_HS_Interface_Descriptor_Alternate1;
extern const USB_INTERFACE_DESCRIPTOR           USB_HS_Interface_Descriptor_Alternate2;
extern const USB_INTERFACE_DESCRIPTOR           USB_HS_Interface_Descriptor_Alternate3;
extern const USB_ENDPOINT_DESCRIPTOR            USB_Bulk_In_512_Descriptor;
extern const USB_ENDPOINT_DESCRIPTOR            USB_Bulk_In_256_Descriptor;
extern const USB_ENDPOINT_DESCRIPTOR            USB_Bulk_In_64_Descriptor;
extern const USB_STRING_DESCRIPTOR              USB_Standard_Product_String;
extern const USB_STRING_DESCRIPTOR              USB_Standard_Manufacturer_String;
extern const USB_STRING_ID                      USB_Standard_String_ID;
extern const USB_DEVICE_QUALIFIER               USB_FS_Device_Qualifier_Info;
extern const USB_DEVICE_QUALIFIER               USB_HS_Device_Qualifier_Info;
extern const USB_OTHER_SPEED_CONFIG_DESCRIPTOR  USB_FS_Config_Descriptor_Info;
extern const USB_OTHER_SPEED_CONFIG_DESCRIPTOR  USB_HS_Config_Descriptor_Info;

extern const USB_ENDPOINT_DESCRIPTOR            USB_Int_In_Descriptor;
extern const USB_ENDPOINT_DESCRIPTOR            USB_Iso_In_128_Descriptor;
extern const USB_ENDPOINT_DESCRIPTOR            USB_Iso_In_512_Descriptor;
extern const USB_ENDPOINT_DESCRIPTOR            USB_Iso_In_1020_Descriptor;
extern const USB_ENDPOINT_DESCRIPTOR            USB_Iso_In_1024_Descriptor;
extern const USB_ENDPOINT_DESCRIPTOR            USB_Iso_In_Zero_Descriptor;
extern const USB_ENDPOINT_DESCRIPTOR            USB_EP2_IN_HS_BULK_64_Descriptor;
extern const USB_ENDPOINT_DESCRIPTOR            USB_EP2_IN_FS_BULK_64_Descriptor;
extern const USB_ENDPOINT_DESCRIPTOR            USB_EP1_OUT_HS_BULK_64_Descriptor;
extern const USB_ENDPOINT_DESCRIPTOR            USB_EP1_OUT_FS_BULK_64_Descriptor;

#endif	//__USB_DESCRIPTORS_H
