/**
  **************** (C) COPYRIGHT 2011 SCT Performance, LLC *********************
  * File Name          : clock.c
  * Device / System    : SCT products using the "CommonCode" platform
  * Author             : Quyen Leba
  *
  * Version            : 1 
  * Date               : 05/02/2011
  * Description        : 
  *                    : 
  *
  *
  * History            : 05/02/2011 M. Davis
  *                    :   Add SCT Copyright header,
  *
  ******************************************************************************
  */

#include "91x_scu.h"
#include "91x_fmi.h"
#include "system.h"

//------------------------------------------------------------------------------
// Set Clock to 96MHz from 25MHz crystal.
// Code executed from flash with 2 waitstate
// Engineer: Quyen Leba
// Date: Jan 18, 2008
//------------------------------------------------------------------------------
void clock_init()
{
#ifdef __DEBUG_JTAG_
    FMI_Config(FMI_READ_WAIT_STATE_2,FMI_WRITE_WAIT_STATE_0, FMI_PWD_ENABLE,
               FMI_LVD_ENABLE,FMI_FREQ_HIGH);
#endif

    SCU_MCLKSourceConfig(SCU_MCLK_OSC);
    SCU_PCLKDivisorConfig(SCU_PCLK_Div2);

    //this is done in bootloader
#ifdef __DEBUG_JTAG_
    FMI_BankRemapConfig(0x3, 0x2, 0x0, 0x80000);
#endif
    //because of bootloader
    //FMI_BankRemapConfig(0, 6, 0x80000, 0x0);
    //fPLL = (2 x N x fOSC)/(M x 2^P)

    // PLL = 96 MHz with N=192, M=25, P=2
    SCU_PLLFactorsConfig(192,25,2);
    //SCU_PLLFactorsConfig(128,25,4);

    SCU_PLLCmd(ENABLE);                        // PLL Enabled
    SCU_MCLKSourceConfig(SCU_MCLK_PLL);        // MCLK = PLL
}
