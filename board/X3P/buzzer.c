/**
  **************** (C) COPYRIGHT 2011 SCT Performance, LLC *********************
  * File Name          : buzzer.c
  * Device / System    : SCT products using the "CommonCode" platform
  * Author             : Quyen Leba
  *
  * Version            : 1 
  * Date               : 05/02/2011
  * Description        : 
  *                    : 
  *
  *
  * History            : 05/02/2011 M. Davis
  *                    :   Add SCT Copyright header,
  *
  ******************************************************************************
  */

#include <91x_gpio.h>
#include <board/delays.h>
#include "system.h"

#define _SI0 0x1F10
#define _DO  0x1DD1
#define _RE  0x1A9E
#define _MI  0x179A
#define _FA  0x164A
#define _SOL 0x13DA
#define _LA  0x11CB
#define _SI  0x0F90
#define _DO2 0x0EE8
#define _M   0x0088

#define min 0x001
#define sq  0x010
#define q   0x020
#define qp  0x040
#define c   0x080
#define cp  0x100
#define m   0x200

// global declared in main.c, indicates LCD_Type back-door riutine is running
extern u8 LCD_BackDoor_cnt;

u32 keygood[3] = {100,100,0};
u32 error[8] = {300,100,200000,1,300,100,0,0};

#ifdef BANKS_ENG
u32 startup[] = {
    300,    120,    300,    1,
    5000,   4,      1,      1,
    300,    55,     300,    1,
    5000,   2,      1,      1,
    225,    400,    300,    1,
    225,    0,      0
};

u32 programfinished[] = {                      
    225,    120,    300,    1,
    5000,   2,      1,      1,
    225,    400,    300,    1,
    5000,   2,      1,      1,
    225,    0,      0
};
#else
u32 startup[] = {
    275,    50,     1000,   1,
    300,    50,     1000,   1,
    325,    50,     1000,   1,
    275,    50,     1000,   1,
    300,    50,     1000,   1,
    325,    50,     1000,   1,
    225,    50,     1000,   1,
    250,    50,     1000,   1,
    275,    50,     1000,   1,
    225,    50,     1000,   1,
    250,    50,     1000,   1,
    275,    50,     1000,   1,
    200,    100,    0
};
u32 programfinished[15] = {
    175,    200,    200000, 1,
    150,    200,    200000, 1,
    100,    200,    200000, 1,
    75,     300,    0};
#endif

//------------------------------------------------------------------------------
// Engineer: Quyen Leba
// Date: Jan 18, 2008
//------------------------------------------------------------------------------
void buzzer_init()
{
    GPIO_InitTypeDef GPIO_InitStructure;
    // Config Buzzer
    GPIO_StructInit(&GPIO_InitStructure);
    GPIO_InitStructure.GPIO_Pin = GPIO_Pin_6;
    GPIO_InitStructure.GPIO_Direction = GPIO_PinOutput;
    GPIO_InitStructure.GPIO_Type = GPIO_Type_PushPull;
    GPIO_InitStructure.GPIO_Alternate = GPIO_OutputAlt1;
    GPIO_Init(GPIO6, &GPIO_InitStructure);
}

//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
void play_note(u16 note, u16 d)
{
    u16 i;
    u16 TH = 6*note/4;
    u16 TL = 6*note*3/4;
    
    for(i=0; i<d; i++)
    {
        GPIO_WriteBit(GPIO6, GPIO_Pin_6, Bit_SET);
        delays(TH,'u');
        GPIO_WriteBit(GPIO6, GPIO_Pin_6, Bit_RESET);
        delays(TL,'u');
    }
    return;
}

//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
void play_sounds(AudioCode choice)
{
    u8 n=0;
    u32 *sndptr;

    //TODO:
#if 0
    //if not doing the LCD_Type back door, USB bus-powered and the buzzer 
    //is silenced, just return
    if(!LCD_BackDoor_cnt && Pwr_USB_powered() && 
       settingsFlags.BuzzerDisabledUSB)
    {
        return;
    }
#endif

    switch(choice)
    {
    case KEYGOOD:
        sndptr = keygood;
        break;
    case ERRORMSG:
        sndptr = error;
        break;       
    case PROGRAMDONE:
        sndptr = programfinished;
        break;
    case STARTUP:
        sndptr = startup;
        break;
    case NOAUDIO:
        return;
    default:
        return;
    }

    // Configure the GPIO1 Port
    // Timer 1 Output Compare function pin (P1.6) configured as Alternate
    // function pushpull.
    //GPIO_Config (GPIO1 , 0x0040 , GPIO_OUT_PP );
    GPIO_WriteBit(GPIO6, GPIO_Pin_6, Bit_RESET);
    
    while(1)
    {
        if(sndptr[n] != 0)
        {
            play_note(sndptr[n],sndptr[n+1]);
        }
        else
        {
            GPIO_WriteBit(GPIO6, GPIO_Pin_6, Bit_RESET);
            break;      	
        }
        n += 2;    // increment through the melody
    }

    return;
}
