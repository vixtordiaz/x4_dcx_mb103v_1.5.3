/**
  **************** (C) COPYRIGHT 2011 SCT Performance, LLC *********************
  * File Name          : can.c
  * Device / System    : SCT products using the "CommonCode" platform
  * Author             : Quyen Leba
  *
  * Version            : 1 
  * Date               : 05/02/2011
  * Description        : 
  *                    : 
  *
  *
  * History            : 05/02/2011 M. Davis
  *                    :   Add SCT Copyright header,
  *
  ******************************************************************************
  */

#include <string.h>
#include <board/mblexec.h>
#include <common/statuscode.h>
#include <common/obd2def.h>
#include <common/obd2can.h>
#include "can.h"

CanRxMsg can_rxframe;

//------------------------------------------------------------------------------
// Inputs:  CAN_INITMODE initmode
//          u32 custom_filter_addr (for CAN_INITMODE_CUSTOM)
//------------------------------------------------------------------------------
void can_init(CAN_INITMODE initmode, u32 custom_filter_addr)
{
    VehicleCommConfig comm_config;
    u8  buffer[8];
    u8  status;
    
    comm_config.raw.value = 0;
    if (initmode == CAN_INITMODE_7EX)
    {
        comm_config.can.mode = CAN_INITMODE_7EX;
    }
    else if (initmode == CAN_INITMODE_CUSTOM)
    {
        comm_config.can.mode = CAN_INITMODE_CUSTOM;
    }
    else
    {
        comm_config.can.mode = CAN_INITMODE_NORMAL;
    }
    comm_config.can.id29bit = 0;
    comm_config.can.padding_byte = obd2can_get_padding_byte();

    buffer[0] = CommType_CAN;
    buffer[1] = CommLevel_Default;
    buffer[2] = comm_config.raw.value & 0xFF;
    buffer[3] = comm_config.raw.value >> 8;
    memcpy((char*)&buffer[4],(char*)&custom_filter_addr,4);

    status = mblexec_call(MBLEXEC_OPCODE_SET_VEHICLE_COMM,buffer,sizeof(buffer),
                          NULL,NULL);
    if (status != S_SUCCESS)
    {
        //TODO
    }
}

//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
void can_setfiltermode_standardid()
{
}

//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
void can_setfiltermode_extendedid()
{
}

//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
void can_deinit()
{
    //TODO
}

//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
void can_setTxMsgObj(CAN_MSGOBJ msgobj)
{
    //can_txmsgobj = msgobj;
}

void can_setRxMsgObj(CAN_MSGOBJ msgobj)
{
    //can_rxmsgobj = msgobj;
}

//------------------------------------------------------------------------------
// Function Name  : can_tx_s
// Description    : CAN Transmit
// Input          : None
// Output         : None
// Return         : return S_SUCCESS or S_FAIL
//------------------------------------------------------------------------------
u8 can_tx_s(CanTxMsg TxMessage)
{
    u8  buffer[2 + sizeof(CanTxMsg)];
    u8  status;
    
    buffer[0] = 0;
    buffer[1] = 0;
    memcpy((char*)&buffer[2],(char*)&TxMessage,sizeof(CanTxMsg));

    status = mblexec_call(MBLEXEC_OPCODE_SEND_SIMPLE_MSG,buffer,sizeof(buffer),
                          NULL,NULL);
    return status;
}

//------------------------------------------------------------------------------
// Function Name  : can_tx
// Description    : CAN Transmit
// Input          : None
// Output         : None
// Return         : return S_SUCCESS or S_FAIL
//------------------------------------------------------------------------------
u8 can_tx(CanTxMsg TxMessage)
{
    u8  buffer[2 + sizeof(CanTxMsg)];
    u8  status;
    
    buffer[0] = 0;
    buffer[1] = 0;
    memcpy((char*)&buffer[2],(char*)&TxMessage,sizeof(CanTxMsg));

    status = mblexec_call(MBLEXEC_OPCODE_SEND_SIMPLE_MSG,buffer,sizeof(buffer),
                          NULL,NULL);
    return status;
}

//------------------------------------------------------------------------------
// Function Name  : can_tx
// Description    : CAN Transmit
// Input          : None
// Output         : None
// Return         : return S_SUCCESS or S_FAIL
//------------------------------------------------------------------------------
u8 can_tx_ext(CanTxMsg TxMessage)
{
    u8  buffer[2 + sizeof(CanTxMsg)];
    u8  status;

    buffer[0] = 0;
    buffer[1] = 0;
    memcpy((char*)&buffer[2],(char*)&TxMessage,sizeof(CanTxMsg));

    status = mblexec_call(MBLEXEC_OPCODE_SEND_SIMPLE_MSG,buffer,sizeof(buffer),
                          NULL,NULL);
    return status;
}

//------------------------------------------------------------------------------
// Function Name  : CAN_RX
// Description    : CAN Receive
// Input          : u32 timeout
// Output         : None
// Return         : return S_SUCCESS or S_FAIL
//------------------------------------------------------------------------------
u8 can_rx(u32 timeout)
{
    u8  buffer[4];
    u8  returnbuffer[sizeof(can_rxframe)];
    u16 returnbufferlength;
    u8  status;
    
    buffer[0] = 0;
    buffer[1] = 0;
    buffer[2] = timeout;
    buffer[3] = timeout >> 8;

    status = mblexec_call(MBLEXEC_OPCODE_RECEIVE_SIMPLE_MSG,
                          buffer,sizeof(buffer),
                          returnbuffer,&returnbufferlength);
    if (status == S_SUCCESS)
    {
        if (returnbufferlength == sizeof(can_rxframe))
        {
            memcpy((char*)&can_rxframe,(char*)returnbuffer,returnbufferlength);
        }
        else
        {
            status = S_BADCONTENT;
        }
    }
    return status;
}

//------------------------------------------------------------------------------
// Function Name  : CAN_RX
// Description    : CAN Receive
// Inputs         : 
// Output         : 
// Return         : status
//------------------------------------------------------------------------------
u8 can_rxcomplex(u8 *privdata, u32 privdatalength,
                 u8 *rxdatabuffer, u16 *rxdatabufferlength)
{
    u8  buffer[256];
    u8  status;

    buffer[0] = 0;  //flags
    buffer[1] = 0;  //flags
    buffer[2] = 0;  //flags
    buffer[3] = 0;  //flags
    memcpy((char*)&buffer[4],(char*)privdata,privdatalength);

    status = mblexec_call(MBLEXEC_OPCODE_RECEIVE_COMPLEX_MSG,
                          buffer,privdatalength+4,
                          rxdatabuffer,
                          rxdatabufferlength);
    return status;
}

//------------------------------------------------------------------------------
// Description    : CAN Receive w/ extended ID
// Input          : u32 timeout
// Output         : None
// Return         : return S_SUCCESS or S_FAIL
//------------------------------------------------------------------------------
u8 can_rx_ext(u32 timeout)
{
    u8  buffer[4];
    u16 flags;
    u8  returnbuffer[256];
    u16 returnbufferlength;
    u8  status;

    flags = 1;
    buffer[0] = flags;
    buffer[1] = flags >> 8;
    buffer[2] = timeout;
    buffer[3] = timeout >> 8;
    status = mblexec_call(MBLEXEC_OPCODE_RECEIVE_SIMPLE_MSG,
                          buffer,sizeof(buffer),
                          returnbuffer,&returnbufferlength);
    if (status == S_SUCCESS)
    {
        if (returnbufferlength == sizeof(can_rxframe))
        {
            memcpy((char*)&can_rxframe,(char*)returnbuffer,returnbufferlength);
        }
        else
        {
            status = S_BADCONTENT;
        }
    }
    return status;
}
