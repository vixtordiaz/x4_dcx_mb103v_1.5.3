/**
  **************** (C) COPYRIGHT 2011 SCT Performance, LLC *********************
  * File Name          : bootloader_vb.h
  * Device / System    : SCT products using the "CommonCode" platform
  * Author             : Quyen Leba
  *
  * Version            : 1 
  * Date               : 05/02/2011
  * Description        : 
  *                    : 
  *
  *
  * History            : 05/02/2011 M. Davis
  *                    :   Add SCT Copyright header,
  *
  ******************************************************************************
  */

#ifndef __BOOTLOADER_VB_H
#define __BOOTLOADER_VB_H

#include <arch/gentype.h>
#include <board/bootloader.h>

#define VB_FAILSAFE_BOOTLOADER_OPERATING_MODE   0xE71A955B
#define VB_MAIN_BOOTLOADER_OPERATING_MODE       0x36F585C2
#define VB_APPLICATION_OPERATING_MODE           0x81E41C43

u8 bootloader_vb_setbootmode(BootloaderMode newmode);
u8 bootloader_vb_getbootmode(BootloaderMode *mode);
u8 bootloader_vb_forceboot_booten_pin();
void bootloader_vb_resetandrun();
u8 bootloader_vb_init(FirmwareHeader fwheader);
u8 bootloader_vb_do(u8 *data, u32 datalength);
u8 bootloader_vb_validate();

#endif  //__BOOTLOADER_VB_H
