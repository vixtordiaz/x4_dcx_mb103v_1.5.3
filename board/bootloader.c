/**
  **************** (C) COPYRIGHT 2011 SCT Performance, LLC *********************
  * File Name          : bootloader.c
  * Device / System    : SCT products using the "CommonCode" platform
  * Author             : Quyen Leba
  *
  * Version            : 1 
  * Date               : 05/02/2011
  * Description        : 
  *                    : 
  *
  *
  * History            : 05/02/2011 M. Davis
  *                    :   Add SCT Copyright header,
  *
  ******************************************************************************
  */

//------------------------------------------------------------------------------
// Log error point: 0x7400 -> 0x74FF
//------------------------------------------------------------------------------

#include <string.h>
#include <arch/gentype.h>
#include <board/genplatform.h>
#include <board/bootloader.h>

#include <board/genplatform.h>

#include <board/properties_vb.h>
#include <common/crc32.h>
#include <common/bootsettings.h>
#include <common/settings.h>
#include <common/crypto_blowfish.h>
#include <common/filecrypto.h>
#include <common/statuscode.h>
#include <common/itoa.h>
#include <common/tea.h>
#include <common/statuscode.h>

typedef void (*pFunction)(void);
typedef u8 (*pFunctionWriteFlash)(u8*,u32);
#define BOOTLOADER_FAILSAFE_ADDRESS         (FLASH_FAILSAFE_BOOT_ADDRESS)
#define BOOTLOADER_MAIN_RAM_ADDRESS         0x20008000
#define BOOTLOADER_MAIN_FLASH_ADDRESS       (FLASH_MAIN_BOOT_ADDRESS)
#define BOOTLOADER_MAIN_SIZE                (FLASH_MAIN_BOOT_SIZE)
#define BOOTLOADER_APPLICATION_ADDRESS      (FLASH_APPLICATION_ADDRESS)
#define BOOTLOADER_APPLICATION_SIZE_MAX     (FLASH_APPLICATION_SIZE)

struct
{
    u8  appboard        :1;
    u8  vehicleboard    :1;
    u8  updatebyfile    :1;
    u8  reserved        :5;
}bootloader_info;

extern u8 cc3000_patchprogrammer();
extern u32 cc3000_patchprogrammer_read_updateversion_uint();

//------------------------------------------------------------------------------
// Private prototypes
//------------------------------------------------------------------------------
u8 bootloader_validate_main_bootloader(u32 cmp_crc32e);
u8 bootloader_validate_application();
u8 bootloader_updatefromfile_do(u8 *filename, u8 *progressbarmsg);
u8 bootloader_updatefromfile_checkfileforupdate(u8 *filename, bool *usefailsafeboot);

//------------------------------------------------------------------------------
// Init bootloader. It handles starting device in boot or application
// Engineer: Quyen Leba
//------------------------------------------------------------------------------
void bootloader_init()
{
    bootloader_info.appboard = 0;
    bootloader_info.vehicleboard = 0;
}

//------------------------------------------------------------------------------
// Jump failsafe bootloader
// Engineer: Quyen Leba
//------------------------------------------------------------------------------
void bootloader_jump_to_failsafe_bootloader()
{
    u32 JumpAddress;
    pFunction Jump_To_Bootloader;

    if (BOOTSETTINGS_GetBootMode() != BootloaderMode_MB_FailSafe)
    {
        BOOTSETTINGS_SetBootMode(BootloaderMode_MB_FailSafe);
        bootsettings_update();
    }
    
    JumpAddress = *(vu32*)(BOOTLOADER_FAILSAFE_ADDRESS + 4);
    Jump_To_Bootloader = (pFunction)JumpAddress;
    // Initialize user application's Stack Pointer 
    __set_MSP(*(vu32*) BOOTLOADER_FAILSAFE_ADDRESS);
    Jump_To_Bootloader();
    while(1);
}

//------------------------------------------------------------------------------
// Jump to main bootloader. Because main bootloader can be modified,
// validate it before use. Main bootloader is run from RAM, and must
// be loaded from FLASH first.
// Engineer: Quyen Leba
//------------------------------------------------------------------------------
void bootloader_jump_to_main_bootloader()
{
    u32 JumpAddress;
    pFunction Jump_To_Bootloader;

    if (bootloader_validate_main_bootloader
        (BOOTSETTINGS_GetMainBootCRC32E()) == S_SUCCESS)
    {
        memcpy((char*)BOOTLOADER_MAIN_RAM_ADDRESS,
               (char*)BOOTLOADER_MAIN_FLASH_ADDRESS,BOOTLOADER_MAIN_SIZE);

        if (BOOTSETTINGS_GetBootMode() != BootloaderMode_MB_Main)
        {
            BOOTSETTINGS_SetBootMode(BootloaderMode_MB_Main);
            bootsettings_update();
        }
        
        JumpAddress = *(vu32*)(BOOTLOADER_MAIN_RAM_ADDRESS + 4);
        Jump_To_Bootloader = (pFunction)JumpAddress;
        // Initialize user application's Stack Pointer 
        __set_MSP(*(vu32*) BOOTLOADER_MAIN_RAM_ADDRESS);
        Jump_To_Bootloader();
        while(1);
    }
}

//------------------------------------------------------------------------------
// Jump to application.
// Engineer: Quyen Leba
//------------------------------------------------------------------------------
void bootloader_jump_to_application()
{
    u32 JumpAddress;
    pFunction Jump_To_Application;

    if (bootloader_validate_application() == S_SUCCESS)
    {
        if (BOOTSETTINGS_GetBootMode() != BootloaderMode_MB_Application)
        {
            BOOTSETTINGS_SetBootMode(BootloaderMode_MB_Application);
            bootsettings_update();
        }
        
        JumpAddress = *(vu32*)(BOOTLOADER_APPLICATION_ADDRESS + 4);
        Jump_To_Application = (pFunction)JumpAddress;
        // Initialize user application's Stack Pointer 
        __set_MSP(*(vu32*) BOOTLOADER_APPLICATION_ADDRESS);
        Jump_To_Application();
        while(1);
    }
}

//------------------------------------------------------------------------------
// Validate main bootloader
// Input:   u32 cmp_crc32e
// Return:  u8  status (S_SUCCESS: good)
// Engineer: Quyen Leba
//------------------------------------------------------------------------------
u8 bootloader_validate_main_bootloader(u32 cmp_crc32e)
{
    u32 calc_crc32e;

    crc32e_reset();
    calc_crc32e = crc32e_calculateblock(0xFFFFFFFF,
                                        (u32*)BOOTLOADER_MAIN_FLASH_ADDRESS,
                                        BOOTLOADER_MAIN_SIZE/4);
    if (calc_crc32e != cmp_crc32e)
    {
        return S_FAIL;
    }
    return S_SUCCESS;
}

//------------------------------------------------------------------------------
// Validate application stored in FLASH
// Return:  u8  status (S_SUCCESS: good)
// Engineer: Quyen Leba
//------------------------------------------------------------------------------
u8 bootloader_validate_application()
{
    u32 calc_crc32e;

    if (BOOTSETTINGS_GetAppLength() > BOOTLOADER_APPLICATION_SIZE_MAX)
    {
        return S_BADCONTENT;
    }
    crc32e_reset();
    calc_crc32e = crc32e_calculateblock
        (0xFFFFFFFF,(u32*)BOOTLOADER_APPLICATION_ADDRESS,
         BOOTSETTINGS_GetAppLength()/4);
    if (calc_crc32e != BOOTSETTINGS_GetAppCRC32E())
    {
        return S_FAIL;
    }
    return S_SUCCESS;
}

//------------------------------------------------------------------------------
// Set boot mode in bootsettings
// Input:   u16 bootmode
// Return:  u8  status
// Engineer: Quyen Leba
//------------------------------------------------------------------------------
u8 bootloader_setboot(BootloaderMode bootmode)
{
    u8  status;

    status = S_SUCCESS;
    if (bootmode != BOOTSETTINGS_GetBootMode())
    {
        switch(bootmode)
        {
        case BootloaderMode_MB_FailSafe:
        case BootloaderMode_MB_Main:
        case BootloaderMode_MB_Application:
            BOOTSETTINGS_SetBootMode((u16)bootmode);
            status = bootsettings_update();
            break;
        default:
            return S_INPUT;
        }
    }
    return status;
}

/**
 *  bootloader_updatefromfile_init_ab_boot
 *  
 *  @brief Validates firmware files and sets boot flags
 *  
 *  @retval  status  S_EXPIRED - Needs update
 *  
 *  @details Validates the firmware files and then sets the appropriate UpdateFromFile 
 *           bootflags. This will return status of firmware file so that 
 *           the update process can be assesed before attempting to start
 *           the update from file process.
 *           This needs to be called prior to bootloader_updatefromfile_start.
 *  
 *  @authors Patrick Downs
 */
u8 bootloader_updatefromfile_init_ab_boot()
{
    u8 status;
    
    status = bootloader_updatefromfile_checkfileforupdate(AB_MAINBOOT_FILENAME, NULL);
    if(status == S_EXPIRED)
    {
        BOOTSETTINGS_SetAbBootUpdateFromFileDone(FALSE); // Bootloader Update required
        bootsettings_update();
    }
    
    return status;
}

/**
 *  bootloader_updatefromfile_init_ab_app
 *  
 *  @brief Validates firmware files and sets boot flags
 * 
 *  @retval  status  S_EXPIRED - Needs update
 *  
 *  @details Validates the firmware files and then sets the appropriate UpdateFromFile 
 *           bootflags. This will return status of firmware file so that 
 *           the update process can be assesed before attempting to start
 *           the update from file process.
 *           This needs to be called prior to bootloader_updatefromfile_start.
 *  
 *  @authors Patrick Downs
 */
u8 bootloader_updatefromfile_init_ab_app()
{
    u8 status;
    
    status = bootloader_updatefromfile_checkfileforupdate(AB_APP_FILENAME, NULL);
    if(status == S_EXPIRED)
    {
        BOOTSETTINGS_SetAbAppUpdateFromFileDone(FALSE); // App Update required
        bootsettings_update();
    }
    
    return status;
}

/**
 *  bootloader_updatefromfile_init_vb_boot
 *  
 *  @brief Validates firmware files and sets boot flags
 * 
 *  @retval  status  S_EXPIRED - Needs update
 *  
 *  @details Validates the firmware files and then sets the appropriate UpdateFromFile 
 *           bootflags. This will return status of firmware file so that 
 *           the update process can be assesed before attempting to start
 *           the update from file process.
 *           This needs to be called prior to bootloader_updatefromfile_start.
 *  
 *  @authors Patrick Downs
 */
u8 bootloader_updatefromfile_init_vb_boot()
{
    u8 status;
    
    status = bootloader_updatefromfile_checkfileforupdate(VB_MAINBOOT_FILENAME, NULL);
    if(status == S_EXPIRED)
    {
        BOOTSETTINGS_SetVbBootUpdateFromFileDone(FALSE); // Bootloader Update required
        bootsettings_update();
    }

    return status;
}

/**
 *  bootloader_updatefromfile_init_vb_app
 *  
 *  @brief Validates firmware files and sets boot flags
 * 
 *  @retval  status  S_EXPIRED - Needs update
 *  
 *  @details Validates the firmware files and then sets the appropriate UpdateFromFile 
 *           bootflags. This will return status of firmware file so that 
 *           the update process can be assesed before attempting to start
 *           the update from file process.
 *           This needs to be called prior to bootloader_updatefromfile_start.
 *  
 *  @authors Patrick Downs
 */
u8 bootloader_updatefromfile_init_vb_app()
{
    u8 status;
    
    status = bootloader_updatefromfile_checkfileforupdate(VB_APP_FILENAME, NULL);
    if(status == S_EXPIRED)
    {
        BOOTSETTINGS_SetVbAppUpdateFromFileDone(FALSE); // App Update required
        bootsettings_update();
    }

    return status;
}

/**
 *  bootloader_updatefromfile_init_mb_boot
 *  
 *  @brief Validates firmware files and sets boot flags
 * 
 *  @retval  status  S_EXPIRED - Needs update
 *  
 *  @details Validates the firmware files and then sets the appropriate UpdateFromFile 
 *           bootflags. This will return status of firmware file so that 
 *           the update process can be assesed before attempting to start
 *           the update from file process.
 *           This needs to be called prior to bootloader_updatefromfile_start.
 *  
 *  @authors Patrick Downs
 */
u8 bootloader_updatefromfile_init_mb_boot()
{
    u8 status;
    bool usefsboot;
    
    // Check the "usefsboot" flag to determine how to setboot for MB update    
    status = bootloader_updatefromfile_checkfileforupdate(MB_MAINBOOT_FILENAME, &usefsboot);
    if(status == S_EXPIRED)
    {
        BOOTSETTINGS_SetMbBootUpdateFromFileDone(FALSE); // Bootloader Update required
        if (usefsboot) // If use of Failsafe bootloader required
        {
            bootloader_setboot(BootloaderMode_MB_FailSafe);
        }
        bootsettings_update();
    }

    return status;
}

/**
 *  bootloader_updatefromfile_init_mb_app
 *  
 *  @brief Validates firmware files and sets boot flags
 * 
 *  @retval  status  S_EXPIRED - Needs update
 *  
 *  @details Validates the firmware files and then sets the appropriate UpdateFromFile 
 *           bootflags. This will return status of firmware file so that 
 *           the update process can be assesed before attempting to start
 *           the update from file process.
 *           This needs to be called prior to bootloader_updatefromfile_start.
 *  
 *  @authors Patrick Downs
 */
u8 bootloader_updatefromfile_init_mb_app()
{
    u8 status;
    bool usefsboot;
    
    // Check the "usefsboot" flag to determine how to setboot for MB update    
    status = bootloader_updatefromfile_checkfileforupdate(MB_APP_FILENAME, &usefsboot);
    if(status == S_EXPIRED)
    {
        BOOTSETTINGS_SetMbAppUpdateFromFileDone(FALSE); // App Update required
        if (usefsboot) 
        {
            bootloader_setboot(BootloaderMode_MB_FailSafe); // If use of Failsafe bootloader required
        }
        bootsettings_update();
    }

    return status;
}

/**
 *  bootloader_updatefromfile_start
 *  
 *  @brief Starts the Update From File process
 *  
 *  @retval none
 *  
 *  @details Starts the Update From File process, bootloader_updatefromfile_init
 *           must be called prior to calling this.
 *  
 *  @authors Patrick Downs
 */
void bootloader_updatefromfile_start()
{
    // Check to see if Mainboard needs to be updated
    if(!BOOTSETTINGS_GetMbBootUpdateFromFileDone() || 
       !BOOTSETTINGS_GetMbAppUpdateFromFileDone())
    {
        // If it does, reboot
        if (BOOTSETTINGS_GetBootMode() != BootloaderMode_MB_Main)
        {
            BOOTSETTINGS_SetBootMode(BootloaderMode_MB_Main);
            bootsettings_update();
        }
        watchdog_set();
        while(1);
    }
    else
    {
        // If no MB update, check other boards.
        bootloader_updatefromfile_checkifdone();
    }
}

/**
 *  bootloader_updatefromfile_checkfileforupdate
 *  
 *  @brief Checks if the firmware file has valid update
 *  
 *  @retval returns a STATUS_CODE enum
 *  
 *  @details Validates the firmware file and then compares the version
 *           to detemine if firmware update is required
 *  
 *  @authors Patrick Downs
 */
u8 bootloader_updatefromfile_checkfileforupdate(u8 *filename, bool *usefailsafeboot)
{
    u8 status;
    
    status = bootloader_validate_file(filename, usefailsafeboot);
    if(status == S_SUCCESS)
    {
        status = bootloader_comparefwversion_file(filename);
    }    
       
    return status;
}


/**
 *  bootloader_validate_file
 *  
 *  @brief Validates the firmware file
 *  
 *  @param [in]     filename Filename of firmware file to check
 *  @param [out]    usefailsafeboot (NULL allowed)
 *
 *  @retval returns a STATUS_CODE enum
 *  
 *  @details Checks the CRCs of the header and the firmware content
 *  
 *  @authors Patrick Downs
 */
u8 bootloader_validate_file(u8 *filename, bool *usefailsafeboot)
{
    FirmwareHeader firmware_header_local;
    F_FILE* fw;    
    u32 bytecount;
    u32 calccrc;
    s32 fwsize;
    u8 filedata[2048];
    u8 status;

    if (usefailsafeboot)
    {
        *usefailsafeboot = FALSE; // Default is false
    }
    
    fw = genfs_user_openfile(filename,"rb");
    if (fw)
    {
        // Validate FWU Header Data
        bytecount = fread((u8*)&firmware_header_local,1,sizeof(FirmwareHeader),fw);
        if(bytecount == sizeof(FirmwareHeader))
        {
            crypto_blowfish_decryptblock_critical((u8*)&firmware_header_local,sizeof(FirmwareHeader));
            
            status = bootloader_validate_fwheader(firmware_header_local);
            if(status == S_SUCCESS)
            {
                // Validate Firmware Data
                fwsize = firmware_header_local.firmware_size;
                crc32e_reset();
                calccrc = 0xFFFFFFFF;
                while(fwsize)
                {
                    bytecount = fread((u8*)&filedata,1,sizeof(filedata),fw);
                    if(bytecount < 1)
                    {
                        status = S_READFILE;
                        break;
                    }
                    fwsize -= bytecount;
                    calccrc = crc32e_calculateblock(calccrc,(u32*)filedata,bytecount/4);
                }
                if(calccrc != firmware_header_local.firmware_crc32e)
                {
                    status = S_CRC32E;
                }
                
                // Check if UseFailsafeBootloader flag is set
                if(firmware_header_local.firmware_flags & FIRMWARE_FLAGS_UPDATE_WITH_FAILSAFE)
                {
                    if (usefailsafeboot)
                    {
                        *usefailsafeboot = TRUE;
                    }
                }
            }
        }
        else
        {
            status = S_READFILE;
        }
    }
    else
    {
        status = S_OPENFILE;
    }
    
    if(fw)
    {
        genfs_closefile(fw);
    }

    return status;
}

/**
 *  bootloader_comparefwversion_file
 *  
 *  @brief Compares the firmware file version to the current firmware version
 *  
 *  @param [in]     filename Filename of firmware file to check
 *
 *  @retval returns a STATUS_CODE enum
 *  
 *  @details Compares the firmware file version to the current firmware version
 *  
 *  @authors Patrick Downs
 */
u8 bootloader_comparefwversion_file(u8 *filename)
{
    FirmwareHeader firmware_header_local;
    F_FILE* fw;    
    u32 bytecount;
    u8 status;
    PROPERTIES_MB_INFO mbprops;
    PROPERTIES_VB_INFO vbprops;
    PROPERTIES_AB_INFO abprops;
    u32 currentFwVersion;
    u32 info_length;

    fw = genfs_user_openfile(filename,"rb");
    if (fw)
    {
        // Read FWU Header Data
        bytecount = fread((u8*)&firmware_header_local,1,sizeof(FirmwareHeader),fw);
        if(bytecount == sizeof(FirmwareHeader))
        {
            crypto_blowfish_decryptblock_critical((u8*)&firmware_header_local,sizeof(FirmwareHeader));
            
            // Compare the version of FWU to current firmware version            
            switch(firmware_header_local.hardware)
            {
            case HARDWARE_MainBoard_RevA:
                status = properties_mb_getinfo((u8*)&mbprops, &info_length);
                if(status == S_SUCCESS)
                {
                    if(firmware_header_local.firmware_flags & FIRMWARE_FLAGS_APPLICATION)
                    {
                        currentFwVersion = mbprops.app_version;
                    }
                    else if(firmware_header_local.firmware_flags & FIRMWARE_FLAGS_MAIN_BOOTLOADER)
                    {
                        currentFwVersion = mbprops.mainboot_version;
                    }
                    else
                    {
                        status = S_ERROR;
                    }
                }
                else
                {
                    log_push_error_point(0x7412); // Failed to read properties to get version
                }
                break;
            case HARDWARE_VehicleBoard_RevB:
                status = properties_vb_getinfo((u8*)&vbprops, &info_length);
                if(status == S_SUCCESS)
                {
                    if(firmware_header_local.firmware_flags & FIRMWARE_FLAGS_APPLICATION)
                    {
                        currentFwVersion = vbprops.app_version;
                    }
                    else if(firmware_header_local.firmware_flags & FIRMWARE_FLAGS_MAIN_BOOTLOADER)
                    {
                        currentFwVersion = vbprops.mainboot_version;
                    }
                    else
                    {
                        status = S_ERROR;
                    }
                }
                else
                {
                    log_push_error_point(0x7413); // Failed to read properties to get version
                }
                break;
            case HARDWARE_AppBoardKen257_RevA:
            case HARDWARE_AppBoardiTSX_RevA:
            case HARDWARE_AppBoardX4_RevA:
                status = properties_ab_getinfo((u8*)&abprops, &info_length);
                if(status == S_SUCCESS)
                {
                    if(firmware_header_local.firmware_flags & FIRMWARE_FLAGS_APPLICATION)
                    {
                        currentFwVersion = abprops.app_version;
                    }
                    else if(firmware_header_local.firmware_flags & FIRMWARE_FLAGS_MAIN_BOOTLOADER)
                    {
                        currentFwVersion = abprops.mainboot_version;
                    }
                    else
                    {
                        status = S_ERROR;
                    }
                }
                else
                {
                    log_push_error_point(0x7414); // Failed to read properties to get version
                }
                break;
            default:
                // NOT Supported
                log_push_error_point(0x7415); // Failed to read properties to get version
                status = S_WRONG_HARDWARE;
                break;
            }
            
            //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
            // Check version
            //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
            //version is in format: 11.22.333.444
            //11:market, 22:hardware, 333:main_version, 444:build_version
            //only use 333.444 for firmware version comparison
            if(status == S_SUCCESS)
            {
                if ((currentFwVersion % 1000000) > (firmware_header_local.firmware_version  % 1000000))
                {
                    status = S_NO_UPDATE; // No update, FWU version is older
                }
                else if ((currentFwVersion % 1000000) == (firmware_header_local.firmware_version % 1000000))
                {
                    status = S_NO_UPDATE; // device has the same firmware version
                    
                    if (firmware_header_local.firmware_flags & FIRMWARE_FLAGS_FORCE_UPDATE) 
                    {
                        status = S_EXPIRED; // Force update flag is set, update allowed
                    }
                }
                else
                {
                    status = S_EXPIRED; // FWU version is newer
                }
            }
        }
        genfs_closefile(fw);
    }
    else
    {
        status = S_OPENFILE;
    }

    return status;
}


/**
 *  bootloader_wifipatch_checkforupdate
 *  
 *  @brief Checks if a newer wifi patch needs to be loaded
 *  
 *  @retval returns a STATUS_CODE enum
 *  
 *  @details Compares patch version in bootsettings with version embedded in
 *              firmware. If they don't match, an update will be performed
 *  
 *  @authors Patrick Downs
 */
void bootloader_wifipatch_checkforupdate()
{
    u8 status;
    u32 wifipatchversion;
    
    // Check if the Wifi Patch needs to be updated
    wifipatchversion = cc3000_patchprogrammer_read_updateversion_uint();
    if(wifipatchversion != BOOTSETTINGS_GetWifiPatchVersion())
    {
        // TODOPD: Put Appboard in mainboot to show updating and pass progress 
        status = cc3000_patchprogrammer();
        if(status == S_SUCCESS || status == S_NO_UPDATE)
        {
            BOOTSETTINGS_SetWifiPatchVersion(wifipatchversion);
            bootsettings_update();
        }
    }
}

/**
 *  bootloader_updatefromfile_checkifdone_mb
 *  
 *  @brief Checks if an Update From File is still pending 
 *  
 *  @retval returns a STATUS_CODE enum
 *  
 *  @details Checks the bootsettings flags to see if there are any updates from file
 *              that need to be completed.
 *  
 *  @authors Patrick Downs
 */
u8 bootloader_updatefromfile_checkifdone_mb()
{
    u8  status;
    
    status = S_SUCCESS;
    
    if(BOOTSETTINGS_GetMbBootUpdateFromFileDone() == FALSE || 
       BOOTSETTINGS_GetMbAppUpdateFromFileDone() == FALSE)
    {
        status = BOOTSETTINGS_GetMbBootUpdateStatus();  // Check if MB bootloader update completed properly
        if(status == S_SUCCESS)
        {
            BOOTSETTINGS_SetMbBootUpdateFromFileDone(TRUE);
            bootsettings_update();
        }
        else
        {
            return status;
        }
        status = BOOTSETTINGS_GetMbBootUpdateStatus();  // Check if MB app update completed properly
        if(status == S_SUCCESS)
        {
            BOOTSETTINGS_SetMbAppUpdateFromFileDone(TRUE);
            bootsettings_update();
        }
        else
        {
            return status;
        }
    }
    
    return status;
}

/**
 *  bootloader_updatefromfile_checkifdone
 *  
 *  @brief Checks if an Update From File is still pending 
 *  
 *  @retval returns a STATUS_CODE enum
 *  
 *  @details Checks the bootsettings flags to see if there are any updates from file
 *              that need to be completed.
 *  
 *  @authors Patrick Downs
 */
void bootloader_updatefromfile_checkifdone()
{
    bool ab_boot_done;
    bool ab_app_done;
    bool vb_boot_done;
    bool vb_app_done;
    u8  progressstring[40];
    u8  buffer[16];
    u8  fwcurrent;
    u8  fwcount;
    u8  status;
    
    status = bootloader_updatefromfile_checkifdone_mb();
    if(status != S_SUCCESS)
    {
        return;
    }
    
    // Check if addition firmware files need to be updated
    ab_boot_done = (bool)BOOTSETTINGS_GetAbBootUpdateFromFileDone();
    ab_app_done = (bool)BOOTSETTINGS_GetAbAppUpdateFromFileDone();
    vb_boot_done = (bool)BOOTSETTINGS_GetVbBootUpdateFromFileDone();
    vb_app_done = (bool)BOOTSETTINGS_GetVbAppUpdateFromFileDone();
    
    if(!ab_boot_done || !ab_app_done || !vb_boot_done || !vb_app_done)
    {
        fwcount = 0;
        fwcurrent = 1;
        
        if(!ab_boot_done)
            fwcount++;
        if(!ab_app_done)
            fwcount++;
        if(!vb_boot_done)
            fwcount++;
        if(!vb_app_done)
            fwcount++;
        
        if(ab_boot_done == FALSE) // Run AB Bootloader update from file
        {
            //sprintf(progressstring, "Updating Firmware %d/%d", fwcurrent, fwcount);
            strcpy((char*)progressstring,"Updating Firmware ");
            itoa(fwcurrent,buffer,10); strcat((char*)progressstring,(char*)buffer);
            strcat((char*)progressstring,"/");
            itoa(fwcount,buffer,10); strcat((char*)progressstring,(char*)buffer);

            status = bootloader_updatefromfile_do(AB_MAINBOOT_FILENAME, progressstring); 
            if(status == S_NO_UPDATE || status == S_SUCCESS || 
               status == S_OPENFILE) // No File, no update
            {
                BOOTSETTINGS_SetAbBootUpdateFromFileDone(TRUE); // Done
                bootsettings_update();
            }
            fwcurrent++;
        }
        
        bootloader_ab_start_bootloader(BootloaderMode_AB_Main, TRUE); // Put App Board in Mainboot for progress bar
        
        if(vb_boot_done == FALSE) // Run VB bootloader update from file
        {
            //sprintf(progressstring, "Updating Firmware %d/%d", fwcurrent, fwcount);
            strcpy((char*)progressstring,"Updating Firmware ");
            itoa(fwcurrent,buffer,10); strcat((char*)progressstring,(char*)buffer);
            strcat((char*)progressstring,"/");
            itoa(fwcount,buffer,10); strcat((char*)progressstring,(char*)buffer);

            status = bootloader_updatefromfile_do(VB_MAINBOOT_FILENAME, progressstring); 
            if(status == S_NO_UPDATE || status == S_SUCCESS || 
               status == S_OPENFILE) // No File, no update
            {
                BOOTSETTINGS_SetVbBootUpdateFromFileDone(TRUE); // Done
                bootsettings_update();
            }
            fwcurrent++;
        }
        if(vb_app_done == FALSE) // Run VB app update from file
        {
            //sprintf(progressstring, "Updating Firmware %d/%d", fwcurrent, fwcount);
            strcpy((char*)progressstring,"Updating Firmware ");
            itoa(fwcurrent,buffer,10); strcat((char*)progressstring,(char*)buffer);
            strcat((char*)progressstring,"/");
            itoa(fwcount,buffer,10); strcat((char*)progressstring,(char*)buffer);

            status = bootloader_updatefromfile_do(VB_APP_FILENAME, progressstring);
            if(status == S_SUCCESS || status == S_NO_UPDATE ||
               status == S_OPENFILE) // No File, no update
            {
                BOOTSETTINGS_SetVbAppUpdateFromFileDone(TRUE); // Done
                bootsettings_update();
            }
            fwcurrent++;
        }
        
        if(ab_app_done == FALSE) // Run AB app update from file
        {
            //sprintf(progressstring, "Updating Firmware %d/%d", fwcurrent, fwcount);
            strcpy((char*)progressstring,"Updating Firmware ");
            itoa(fwcurrent,buffer,10); strcat((char*)progressstring,(char*)buffer);
            strcat((char*)progressstring,"/");
            itoa(fwcount,buffer,10); strcat((char*)progressstring,(char*)buffer);

            status = bootloader_updatefromfile_do(AB_APP_FILENAME, progressstring);
            if(status == S_SUCCESS || status == S_NO_UPDATE ||
               status == S_OPENFILE)
            {
                BOOTSETTINGS_SetAbAppUpdateFromFileDone(TRUE); // Done
                bootsettings_update();
                bootloader_ab_resetandrun();
            }
            fwcurrent++;
        }
        else
        {   
            bootloader_ab_setbootmode(BootloaderMode_AB_Application);
        }
    }
    
    return;
}

/**
 *  bootloader_updatefromfile_do
 *  
 *  @brief Runs an update from file bootloader function
 *  
 *  @param [in] filename Parameter_Description
 *  @retval returns a STATUS_CODE enum
 *  
 *  @details Runs a firmware update from a file saved in the filesystem.
 *  
 *  @authors Patrick Downs
 */
u8 bootloader_updatefromfile_do(u8 *filename, u8 *progressbarmsg)
{
    u32 bytecount;
    s32 fwsize;
    u16 sector_size;
    u8 filedata[FLASH_SECTOR_SIZE];
    u8 status;
    F_FILE* fw;
    u8 percentage;
    u8 value;
    u32 progress_bytesdone;
    u32 progres_filesize;
    
    // Open Vehicle Board Firmware (FWU) file
    fw = genfs_user_openfile(filename,"rb");
    if (!fw)
    {
        status = S_OPENFILE;
        goto bootloader_updatefromfile;
    }

    // Read the header data from FWU file
    bytecount = fread(filedata,1,sizeof(FirmwareHeader),fw);
    if(bytecount != sizeof(FirmwareHeader))
    {
        status = S_READFILE;
        goto bootloader_updatefromfile;
    }
    
    // Init the bootloader with firmware header data
    status = bootloader_setup_session(filedata, &sector_size, FALSE);
    if(status != S_SUCCESS)
        goto bootloader_updatefromfile;
    
    // Get the firmware size
    fwsize = fgetfilesize(fw) - sizeof(FirmwareHeader); 
    
    // Prepare progress bar
    percentage = 0;
    progress_bytesdone = 0;
    progres_filesize = fwsize;
    
    while(fwsize)
    {
        bytecount = fread(filedata,1,sector_size,fw);
        if(bytecount == 0)
        {
            status = S_READFILE;
            goto bootloader_updatefromfile;
        }
        fwsize -= bytecount;
        
        // Write the firmware data to flash
        status = bootloader_write_flash(filedata, bytecount); 
        if(status != S_SUCCESS)
            goto bootloader_updatefromfile;
        
        progress_bytesdone += bytecount;
        value = (progress_bytesdone*100)/progres_filesize;
        if (value > percentage)
        {
            percentage = value;
            bootloader_ab_progressbar(percentage, progressbarmsg);
        }
    }
    
    status = bootloader_validate(); // Firmware all written, validate checksums
    if (status == S_LOADFILE)
    {
        status = bootloader_updatebyfile();
    }
    
bootloader_updatefromfile:
    genfs_closefile(fw);    
    return status;
}


//------------------------------------------------------------------------------
// Setup a bootloader session
// Input:   u8  *firmware_header (enrypted with critical key)
// Output:  u16 *sector_size
// Return:  u8  status (S_NO_UPDATE: skip, S_SUCCESS: do the update)
// Engineer: Quyen Leba
//------------------------------------------------------------------------------
u8 bootloader_setup_session(u8 *firmware_header, u16 *sector_size, bool updatebyfile)
{
    u8  status;
    FirmwareHeader firmware_header_local;
    
    bootloader_info.vehicleboard = 0;
    bootloader_info.appboard = 0;
    bootloader_info.updatebyfile = updatebyfile;
    *sector_size = FLASH_SECTOR_SIZE;
    
    firmware_header_local = *(FirmwareHeader*)firmware_header;
    crypto_blowfish_decryptblock_critical((u8*)&firmware_header_local,sizeof(FirmwareHeader));    
    
    status = bootloader_validate_fwheader(firmware_header_local);
    if(status != S_SUCCESS)
       goto BL_SETUP_DONE;
        
    //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    // Supported Hardware Types
    //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    switch(firmware_header_local.hardware)
    {
    case HARDWARE_VehicleBoard_RevB:
        bootloader_info.vehicleboard = 1;
        status = bootloader_vb_init(firmware_header_local);
        break;
    case HARDWARE_AppBoardKen257_RevA:
        bootloader_info.updatebyfile = 1;   //always update by file
    case HARDWARE_AppBoardiTSX_RevA:
        bootloader_info.appboard = 1;
        // NOTE PD: iTSX UpdateByFile works around the normal bootloader scheme 
        // by transferring the firmware file in a regular "bootload_ab_do" function
        // and then use the file during the validation step to update the
        // firmware. This allowed for ab update without changes to the mobile app. 
        // Going forward we should just transfer the FWU file using normal
        // file transfer method, then set appropriate bootsettings flags
        // and run "bootloader_updatefromfile()"
        if(bootloader_info.updatebyfile)
        {
            status = bootloader_ab_init_file(firmware_header_local);
        }
        else
        {
            status = bootloader_ab_init(firmware_header_local);
        }
        break;
    case HARDWARE_AppBoardX4_RevA:
        bootloader_info.appboard = 1;
        status = bootloader_ab_init(firmware_header_local);
        break;
    default:
        // NOT Supported
        status = S_WRONG_HARDWARE;        
    }

BL_SETUP_DONE:    
    if (status != S_SUCCESS)
    {
        bootloader_info.vehicleboard = 0;
        bootloader_info.appboard = 0;
        *sector_size = 0;
        bootloader_info.updatebyfile = 0;
    }
    return status;
}

//------------------------------------------------------------------------------
// Input:   FirmwareHeader fwheader (encrypted with critcal key)
// Return:  u8  status
// Engineer: Quyen Leba
//------------------------------------------------------------------------------
u8 bootloader_validate_fwheader(FirmwareHeader fwheader)
{
    u32 *wptr;
    u32 calc_crc32e;
    u32 tea_key[4];
    u32 i;
//    u32 calc_signature;
//    bool is_backdating_allowed;
    u8  status;

    status = S_FAIL;
    //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    // Validate firmware header
    //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    wptr = (u32*)&fwheader;
    wptr++;
    crc32e_reset();
    calc_crc32e = crc32e_calculateblock(0xFFFFFFFF,
                                        (u32*)wptr,(sizeof(FirmwareHeader)-4)/4);
    if (calc_crc32e != fwheader.hcrc)
    {
//        log_push_error_point(0x7400);
        return S_CRC32E;
    }
    if (fwheader.hversion > FILECRYPTO_GENERAL_HEADER_VERSION_MAX)
    {
//        log_push_error_point(0x7401);
        return S_NOTSUPPORT;
    }
    if (fwheader.ftype != FILETYPE_FIRMWARE)
    {
//        log_push_error_point(0x7402);
        return S_INVALIDTYPE;
    }
    
    //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    // Check Serial Number - use MainBoard serial number
    //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    if (memcmp((char*)SETTINGS_CRITICAL(serialnumber),
               (char*)fwheader.serialnumber,sizeof(fwheader.serialnumber)) != 0)
    {
        u32 sum1,sum2;

        for (i=0,sum1=0,sum2=0;i<sizeof(fwheader.serialnumber);i++)
        {
            sum1 += fwheader.serialnumber[i];   //all ZERO
            sum2 += fwheader.serialnumber[i];   //all 0xFF
        }
        if (sum1 != 0 && sum2 != (16*0xFF))
        {
            log_push_error_point(0x7404);
            return S_SERIALUNMATCH;
        }
    }

    //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    // Check Backdating
    //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    //this must always use tea US market
    tea_key[0] = TEA_KEY_0_M(US);
    tea_key[1] = TEA_KEY_0_M(US);
    tea_key[2] = TEA_KEY_0_M(US);
    tea_key[3] = TEA_KEY_0_M(US);
    wptr = (u32*)fwheader.serialnumber;
    for(i=0;i<sizeof(fwheader.serialnumber)/8;i++)
    {
        tea_encryption(wptr,tea_key);
        wptr+=2;
    }
    
//    crc32e_reset();
//    calc_signature = crc32e_calculateblock(0xFFFFFFFF,
//                                           (u32*)fwheader.serialnumber,
//                                           sizeof(fwheader.serialnumber)/4);
//    if (calc_signature == fwheader.signature ||
//        BOOTSETTINGS_GetEngSign() == ENGINEERING_SIGNATURE)
//    {
//        is_backdating_allowed = TRUE;
//    }
    
    if (fwheader.firmware_size == 0 || (fwheader.firmware_size % 8))
    {
//        log_push_error_point(0x7410);
        return S_NOTFIT;
    }
    
    //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    //
    //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    switch(fwheader.firmware_flags & 0x07)
    {
    case FIRMWARE_FLAGS_APPLICATION:
    case FIRMWARE_FLAGS_MAIN_BOOTLOADER:
        status = S_SUCCESS;
        break;
    default:
//        log_push_error_point(0x7411);
        return S_ERROR;
    }
    
    return status;
}


//------------------------------------------------------------------------------
// Write bootloader data to flash
// Inputs:  u8  *data (encrypted)
//          u32 datalength (must be multiple of 8)
// Return:  u8  status
// Engineer: Quyen Leba
//------------------------------------------------------------------------------
u8 bootloader_write_flash(u8 *data, u32 datalength)
{
    u8  status;

    status = S_ERROR;
    if (bootloader_info.vehicleboard)
    {
        status = bootloader_vb_do(data,datalength);
    }
    else if (bootloader_info.appboard)
    {
        if(bootloader_info.updatebyfile)
        {
            status = bootloader_ab_do_file(data,datalength);
        }
        else
        {
            status = bootloader_ab_do(data,datalength);
        }
    }
    return status;
}

//------------------------------------------------------------------------------
// Validate bootloader
// Inputs:  u32 crc32e
// Return:  u8  status
// Engineer: Quyen Leba
//------------------------------------------------------------------------------
u8 bootloader_validate()
{
    u8  status;

    status = S_ERROR;
    if (bootloader_info.vehicleboard)
    {
        status = bootloader_vb_validate();
    }
    else if (bootloader_info.appboard)
    {
        if(bootloader_info.updatebyfile)
            status = bootloader_ab_validate_file();            
        else
            status = bootloader_ab_validate();
    }
    return status;
}

//------------------------------------------------------------------------------
// Validate bootloader
// Inputs:  u32 crc32e
// Return:  u8  status
// Engineer: Quyen Leba
//------------------------------------------------------------------------------
u8 bootloader_updatebyfile(void)
{
    u8  status;

    status = S_ERROR;
    if (bootloader_info.appboard && bootloader_info.updatebyfile)
    {
        status = bootloader_ab_updatebyfile();
    }
    return status;
}

//------------------------------------------------------------------------------
// Clear engineering signature
// Return:  u8  status
// Engineer: Quyen Leba
// Note: just in case this signature is somehow set
//------------------------------------------------------------------------------
u8 bootloader_clearengineeringsignature()
{
    BOOTSETTINGS_ClearEngSign();
    return bootsettings_update();
}

//------------------------------------------------------------------------------
// Force a validation of application
// Return:  u8  status
// Engineer: Quyen Leba
//------------------------------------------------------------------------------
u8 bootloader_force_application_validation()
{
    u32 calc_crc32e;

    if (bootloader_validate_application() == S_SUCCESS)
    {
        if (BOOTSETTINGS_GetBootMode() != BootloaderMode_MB_Application)
        {
            BOOTSETTINGS_SetBootMode(BootloaderMode_MB_Application);
            return bootsettings_update();
        }
        return S_SUCCESS;
    }

    crc32e_reset();
    calc_crc32e = crc32e_calculateblock(0xFFFFFFFF,
                                        (u32*)BOOTLOADER_APPLICATION_ADDRESS,
                                        BOOTLOADER_APPLICATION_SIZE_MAX/4);
    if (BOOTSETTINGS_GetBootMode() != BootloaderMode_MB_Application ||
        BOOTSETTINGS_GetAppCRC32E() != calc_crc32e ||
        BOOTSETTINGS_GetAppLength() != BOOTLOADER_APPLICATION_SIZE_MAX)
    {
        BOOTSETTINGS_SetBootMode(BootloaderMode_MB_Application);
        BOOTSETTINGS_SetAppCRC32E(calc_crc32e);
        BOOTSETTINGS_SetAppLength(BOOTLOADER_APPLICATION_SIZE_MAX);
        return bootsettings_update();
    }
    return S_SUCCESS;
}

//------------------------------------------------------------------------------
// Force a validation of mainboot
// Return:  u8  status
// Engineer: Quyen Leba
//------------------------------------------------------------------------------
u8 bootloader_force_mainboot_validation()
{
    u32 calc_crc32e;
    
    crc32e_reset();
    calc_crc32e = crc32e_calculateblock(0xFFFFFFFF,
                                        (u32*)BOOTLOADER_MAIN_FLASH_ADDRESS,
                                        BOOTLOADER_MAIN_SIZE/4);
    if (BOOTSETTINGS_GetBootMode() != BootloaderMode_MB_Main ||
        BOOTSETTINGS_GetMainBootCRC32E() != calc_crc32e)
    {
        BOOTSETTINGS_SetBootMode(BootloaderMode_MB_Main);
        BOOTSETTINGS_SetMainBootCRC32E(calc_crc32e);
        return bootsettings_update();
    }
    return S_SUCCESS;
}
