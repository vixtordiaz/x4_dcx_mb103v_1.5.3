/**
  **************** (C) COPYRIGHT 2011 SCT Performance, LLC *********************
  * File Name          : sci.h
  * Device / System    : SCT products using the "CommonCode" platform
  * Author             : Rhonda Rusak
  *
  * Version            : 1 
  * Date               : 05/13/2014
  * Description        : 
  *                    : 
  *
  *
  ******************************************************************************
  */

#ifndef __SCI_H
#define __SCI_H

#include <arch/gentype.h>
      
//  SCI/ K_Line TX Flags
//  bit 0 = 1 =send SCIB / 0 = send SCIA
//  bit 1 = 1 check for 0x22 answer in SCIA / 0 do not check
//  bit 2 = 1 use checksum / 0 do not use checksum


// SCI/ K_Line RX Flags
// bit 0 = 1 = RX SCIB / 0 = RX SCIA
// bit 1 = 1 = skip 0x80 on RX / 0 = do not skip 0x80 on RX
// bit 2 = 1 = check checksum / 0 = do not check checksum
// bit 3 = 1 = wait extra time / 0 = do not wait extra time

 

typedef enum
{
    SCI_INITSPEED_L    = 0,
    SCI_INITSPEED_M    = 1,
    SCI_INITSPEED_H    = 2,
}SCI_INITSPEED;


u8 sci_init(VehicleCommType *vehiclecommtype, VehicleCommLevel *vehiclecommlevel);
u8 sci_tx(u8 *message, u8 data_length, u16 txflags);
u8 sci_rx(u8 *rxdatabuffer, u16 *rxdatabufferlength, u16 rxflags, u32 timeout);

#endif  //__SCI_H