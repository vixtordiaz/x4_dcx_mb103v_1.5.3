/**
  **************** (C) COPYRIGHT 2011 SCT Performance, LLC *********************
  * File Name          : adc.h
  * Device / System    : SCT products using the "CommonCode" platform
  * Author             : Quyen Leba
  *
  * Version            : 1 
  * Date               : 05/02/2011
  * Description        : 
  *                    : 
  *
  *
  * History            : 05/02/2011 M. Davis
  *                    :   Add SCT Copyright header,
  *
  ******************************************************************************
  */

#ifndef __ADC_H
#define __ADC_H

#include <device_config.h>

typedef enum
{
    ADC_AIN1                = 0,
    ADC_AIN2                = 1,
    ADC_AIN3                = 2,
    ADC_AIN4                = 3,
    ADC_AIN5                = 4,
    ADC_AIN6                = 5,
    ADC_AIN7                = 6,
    ADC_AIN8                = 7,
    ADC_TEMPSENSOR          = 11,
    ADC_VREFINT             = 12,
    ADC_VPP                 = 20,
    ADC_VBAT                = 21,
}ADC_CHANNEL;

typedef enum
{
    ADC_FLASH,
    ADC_DATALOG,
}ADC_MODE;

void adc_init(ADC_MODE mode);
u8 adc_read(ADC_CHANNEL channel, float *DataValueFloat);
u8 adc_config_gpio1_out(bool isHighOut);
u8 adc_peek_gpio2_in(bool *ok);

#endif  //__ADC_H