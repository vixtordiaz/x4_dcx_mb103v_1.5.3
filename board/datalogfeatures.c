/**
  **************** (C) COPYRIGHT 2011 SCT Performance, LLC *********************
  * File Name          : datalogfeatures.c
  * Device / System    : SCT products using the "CommonCode" platform
  * Author             : Quyen Leba
  *
  * Version            : 1
  * Date               : 05/02/2011
  * Description        :
  *                    :
  *
  *
  * History            : 05/02/2011 M. Davis
  *                    :   Add SCT Copyright header,
  *
  ******************************************************************************
  */

#include <board/genplatform.h>
#include <common/statuscode.h>
#include <common/crypto_blowfish.h>
#include <common/file.h>
#include <common/crc32.h>
#include <common/obd2datalog.h>
#include "datalogfeatures.h"

extern const DlxBlock** datalogfeatures_getInputCollection(u8 vbrev);

const DlxBlock DefaultDatalogFeatureBlock_AIN1 =
{
    .pidModuleType      = 0,
    .pidShortName       = AIN1_SHORTNAME,
    .pidName            = AIN1_NAME,
    .pidAddr            = ADC_AIN1,
    .pidSize            = 0,
    .pidBP              = 0,
    .pidBit             = 0,
    .pidType            = 3,
    .pidBitMapped       = 0,
    .reserved1          = 0,
    .pidFlags           = (1<<4) | (1<<3) | (0),
    .pidMultiplier      = 0,
    .pidOffset          = 0,
    .pidRangeMin        = AIN1_MIN,
    .pidRangeMax        = AIN1_MAX,
    .pidUnitID          = 0,
    .packetStartPos     = 0,
    .pidOemType         = OemType_Unknown,
    .pidCommType        = CommType_Unknown,
    .pidCommLevel       = CommLevel_Unknown,
    .uiControlType      = 0,
    .oscDataBytePos     = 0,
    .oscControlBytePos  = 0,
    .oscControlByteMask = 0,
    .oscUiPidGroup      = 0,
    .reserved2          = 0,
    .enumID             = 0,
    .reserved3          = {0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0},
};

const DlxBlock DefaultDatalogFeatureBlock_AIN2 =
{
    .pidModuleType      = 0,
    .pidShortName       = AIN2_SHORTNAME,
    .pidName            = AIN2_NAME,
    .pidAddr            = ADC_AIN2,
    .pidSize            = 0,
    .pidBP              = 0,
    .pidBit             = 0,
    .pidType            = 3,
    .pidBitMapped       = 0,
    .reserved1          = 0,
    .pidFlags           = (1<<4) | (1<<3) | (0),
    .pidMultiplier      = 0,
    .pidOffset          = 0,
    .pidRangeMin        = AIN2_MIN,
    .pidRangeMax        = AIN2_MAX,
    .pidUnitID          = 0,
    .packetStartPos     = 0,
    .pidOemType         = OemType_Unknown,
    .pidCommType        = CommType_Unknown,
    .pidCommLevel       = CommLevel_Unknown,
    .uiControlType      = 0,
    .oscDataBytePos     = 0,
    .oscControlBytePos  = 0,
    .oscControlByteMask = 0,
    .oscUiPidGroup      = 0,
    .reserved2          = 0,
    .enumID             = 0,
    .reserved3          = {0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0},
};

const DlxBlock DefaultDatalogFeatureBlock_AIN3 =
{
    .pidModuleType      = 0,
    .pidShortName       = AIN3_SHORTNAME,
    .pidName            = AIN3_NAME,
    .pidAddr            = ADC_AIN3,
    .pidSize            = 0,
    .pidBP              = 0,
    .pidBit             = 0,
    .pidType            = 3,
    .pidBitMapped       = 0,
    .reserved1          = 0,
    .pidFlags           = (1<<4) | (1<<3) | (0),
    .pidMultiplier      = 0,
    .pidOffset          = 0,
    .pidRangeMin        = AIN3_MIN,
    .pidRangeMax        = AIN3_MAX,
    .pidUnitID          = 0,
    .packetStartPos     = 0,
    .pidOemType         = OemType_Unknown,
    .pidCommType        = CommType_Unknown,
    .pidCommLevel       = CommLevel_Unknown,
    .uiControlType      = 0,
    .oscDataBytePos     = 0,
    .oscControlBytePos  = 0,
    .oscControlByteMask = 0,
    .oscUiPidGroup      = 0,
    .reserved2          = 0,
    .enumID             = 0,
    .reserved3          = {0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0},
};

const DlxBlock DefaultDatalogFeatureBlock_AIN4 =
{
    .pidModuleType      = 0,
    .pidShortName       = AIN4_SHORTNAME,
    .pidName            = AIN4_NAME,
    .pidAddr            = ADC_AIN4,
    .pidSize            = 0,
    .pidBP              = 0,
    .pidBit             = 0,
    .pidType            = 3,
    .pidBitMapped       = 0,
    .reserved1          = 0,
    .pidFlags           = (1<<4) | (1<<3) | (0),
    .pidMultiplier      = 0,
    .pidOffset          = 0,
    .pidRangeMin        = AIN4_MIN,
    .pidRangeMax        = AIN4_MAX,
    .pidUnitID          = 0,
    .packetStartPos     = 0,
    .pidOemType         = OemType_Unknown,
    .pidCommType        = CommType_Unknown,
    .pidCommLevel       = CommLevel_Unknown,
    .uiControlType      = 0,
    .oscDataBytePos     = 0,
    .oscControlBytePos  = 0,
    .oscControlByteMask = 0,
    .oscUiPidGroup      = 0,
    .reserved2          = 0,
    .enumID             = 0,
    .reserved3          = {0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0},
};

const DlxBlock DefaultDatalogFeatureBlock_AIN5_VBREV1 =
{
    .pidModuleType      = 0,
    .pidShortName       = AIN5_SHORTNAME,
    .pidName            = AIN5_NAME,
    .pidAddr            = ADC_AIN5,
    .pidSize            = 0,
    .pidBP              = 0,
    .pidBit             = 0,
    .pidType            = 3,
    .pidBitMapped       = 0,
    .reserved1          = 0,
    .pidFlags           = (1<<4) | (1<<3) | (0),
    .pidMultiplier      = 0,
    .pidOffset          = 0,
    .pidRangeMin        = AIN5_MIN,
    .pidRangeMax        = AIN5_MAX_VBREV1,
    .pidUnitID          = 0,
    .packetStartPos     = 0,
    .pidOemType         = OemType_Unknown,
    .pidCommType        = CommType_Unknown,
    .pidCommLevel       = CommLevel_Unknown,
    .uiControlType      = 0,
    .oscDataBytePos     = 0,
    .oscControlBytePos  = 0,
    .oscControlByteMask = 0,
    .oscUiPidGroup      = 0,
    .reserved2          = 0,
    .enumID             = 0,
    .reserved3          = {0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0},
};

const DlxBlock DefaultDatalogFeatureBlock_AIN5_VBREV2 =
{
    .pidModuleType      = 0,
    .pidShortName       = AIN5_SHORTNAME,
    .pidName            = AIN5_NAME,
    .pidAddr            = ADC_AIN5,
    .pidSize            = 0,
    .pidBP              = 0,
    .pidBit             = 0,
    .pidType            = 3,
    .pidBitMapped       = 0,
    .reserved1          = 0,
    .pidFlags           = (1<<4) | (1<<3) | (0),
    .pidMultiplier      = 0,
    .pidOffset          = 0,
    .pidRangeMin        = AIN5_MIN,
    .pidRangeMax        = AIN5_MAX,
    .pidUnitID          = 0,
    .packetStartPos     = 0,
    .pidOemType         = OemType_Unknown,
    .pidCommType        = CommType_Unknown,
    .pidCommLevel       = CommLevel_Unknown,
    .uiControlType      = 0,
    .oscDataBytePos     = 0,
    .oscControlBytePos  = 0,
    .oscControlByteMask = 0,
    .oscUiPidGroup      = 0,
    .reserved2          = 0,
    .enumID             = 0,
    .reserved3          = {0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0},
};

const DlxBlock DefaultDatalogFeatureBlock_AIN6_VBREV1 =
{
    .pidModuleType      = 0,
    .pidShortName       = AIN6_SHORTNAME,
    .pidName            = AIN6_NAME,
    .pidAddr            = ADC_AIN6,
    .pidSize            = 0,
    .pidBP              = 0,
    .pidBit             = 0,
    .pidType            = 3,
    .pidBitMapped       = 0,
    .reserved1          = 0,
    .pidFlags           = (1<<4) | (1<<3) | (0),
    .pidMultiplier      = 0,
    .pidOffset          = 0,
    .pidRangeMin        = AIN6_MIN,
    .pidRangeMax        = AIN6_MAX_VBREV1,
    .pidUnitID          = 0,
    .packetStartPos     = 0,
    .pidOemType         = OemType_Unknown,
    .pidCommType        = CommType_Unknown,
    .pidCommLevel       = CommLevel_Unknown,
    .uiControlType      = 0,
    .oscDataBytePos     = 0,
    .oscControlBytePos  = 0,
    .oscControlByteMask = 0,
    .oscUiPidGroup      = 0,
    .reserved2          = 0,
    .enumID             = 0,
    .reserved3          = {0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0},
};

const DlxBlock DefaultDatalogFeatureBlock_AIN6_VBREV2 =
{
    .pidModuleType      = 0,
    .pidShortName       = AIN6_SHORTNAME,
    .pidName            = AIN6_NAME,
    .pidAddr            = ADC_AIN6,
    .pidSize            = 0,
    .pidBP              = 0,
    .pidBit             = 0,
    .pidType            = 3,
    .pidBitMapped       = 0,
    .reserved1          = 0,
    .pidFlags           = (1<<4) | (1<<3) | (0),
    .pidMultiplier      = 0,
    .pidOffset          = 0,
    .pidRangeMin        = AIN6_MIN,
    .pidRangeMax        = AIN6_MAX,
    .pidUnitID          = 0,
    .packetStartPos     = 0,
    .pidOemType         = OemType_Unknown,
    .pidCommType        = CommType_Unknown,
    .pidCommLevel       = CommLevel_Unknown,
    .uiControlType      = 0,
    .oscDataBytePos     = 0,
    .oscControlBytePos  = 0,
    .oscControlByteMask = 0,
    .oscUiPidGroup      = 0,
    .reserved2          = 0,
    .enumID             = 0,
    .reserved3          = {0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0},
};

const DlxBlock DefaultDatalogFeatureBlock_AIN7_24V =
{
    .pidModuleType      = 0,
    .pidShortName       = AIN7_SHORTNAME,
    .pidName            = AIN7_NAME,
    .pidAddr            = ADC_AIN7,
    .pidSize            = 0,
    .pidBP              = 0,
    .pidBit             = 0,
    .pidType            = 3,
    .pidBitMapped       = 0,
    .reserved1          = 0,
    .pidFlags           = (1<<4) | (1<<3) | (0),
    .pidMultiplier      = 0,
    .pidOffset          = 0,
    .pidRangeMin        = AIN7_MIN,
    .pidRangeMax        = AIN7_24V_MAX,
    .pidUnitID          = 0,
    .packetStartPos     = 0,
    .pidOemType         = OemType_Unknown,
    .pidCommType        = CommType_Unknown,
    .pidCommLevel       = CommLevel_Unknown,
    .uiControlType      = 0,
    .oscDataBytePos     = 0,
    .oscControlBytePos  = 0,
    .oscControlByteMask = 0,
    .oscUiPidGroup      = 0,
    .reserved2          = 0,
    .enumID             = 0,
    .reserved3          = {0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0},
};

const DlxBlock DefaultDatalogFeatureBlock_AIN7_15V =
{
    .pidModuleType      = 0,
    .pidShortName       = AIN7_SHORTNAME,
    .pidName            = AIN7_NAME,
    .pidAddr            = ADC_AIN7,
    .pidSize            = 0,
    .pidBP              = 0,
    .pidBit             = 0,
    .pidType            = 3,
    .pidBitMapped       = 0,
    .reserved1          = 0,
    .pidFlags           = (1<<4) | (1<<3) | (0),
    .pidMultiplier      = 0,
    .pidOffset          = 0,
    .pidRangeMin        = AIN7_MIN,
    .pidRangeMax        = AIN7_15V_MAX,
    .pidUnitID          = 0,
    .packetStartPos     = 0,
    .pidOemType         = OemType_Unknown,
    .pidCommType        = CommType_Unknown,
    .pidCommLevel       = CommLevel_Unknown,
    .uiControlType      = 0,
    .oscDataBytePos     = 0,
    .oscControlBytePos  = 0,
    .oscControlByteMask = 0,
    .oscUiPidGroup      = 0,
    .reserved2          = 0,
    .enumID             = 0,
    .reserved3          = {0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0},
};

const DlxBlock DefaultDatalogFeatureBlock_AIN8_15V =
{
    .pidModuleType      = 0,
    .pidShortName       = AIN8_SHORTNAME,
    .pidName            = AIN8_NAME,
    .pidAddr            = ADC_AIN8,
    .pidSize            = 0,
    .pidBP              = 0,
    .pidBit             = 0,
    .pidType            = 3,
    .pidBitMapped       = 0,
    .reserved1          = 0,
    .pidFlags           = (1<<4) | (1<<3) | (0),
    .pidMultiplier      = 0,
    .pidOffset          = 0,
    .pidRangeMin        = AIN8_MIN,
    .pidRangeMax        = AIN8_24V_MAX,
    .pidUnitID          = 0,
    .packetStartPos     = 0,
    .pidOemType         = OemType_Unknown,
    .pidCommType        = CommType_Unknown,
    .pidCommLevel       = CommLevel_Unknown,
    .uiControlType      = 0,
    .oscDataBytePos     = 0,
    .oscControlBytePos  = 0,
    .oscControlByteMask = 0,
    .oscUiPidGroup      = 0,
    .reserved2          = 0,
    .enumID             = 0,
    .reserved3          = {0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0},
};

const DlxBlock DefaultDatalogFeatureBlock_AIN8_5V =
{
    .pidModuleType      = 0,
    .pidShortName       = AIN8_SHORTNAME,
    .pidName            = AIN8_NAME,
    .pidAddr            = ADC_AIN8,
    .pidSize            = 0,
    .pidBP              = 0,
    .pidBit             = 0,
    .pidType            = 3,
    .pidBitMapped       = 0,
    .reserved1          = 0,
    .pidFlags           = (1<<4) | (1<<3) | (0),
    .pidMultiplier      = 0,
    .pidOffset          = 0,
    .pidRangeMin        = AIN8_MIN,
    .pidRangeMax        = AIN8_5V_MAX,
    .pidUnitID          = 0,
    .packetStartPos     = 0,
    .pidOemType         = OemType_Unknown,
    .pidCommType        = CommType_Unknown,
    .pidCommLevel       = CommLevel_Unknown,
    .uiControlType      = 0,
    .oscDataBytePos     = 0,
    .oscControlBytePos  = 0,
    .oscControlByteMask = 0,
    .oscUiPidGroup      = 0,
    .reserved2          = 0,
    .enumID             = 0,
    .reserved3          = {0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0},
};

/**
 *  datalogfeatures_calcCRC32E
 *  
 *  @brief Calculate CRC32E of device feature default file
 *
 *  @param[in]  vbrev VB board revision
 *  @param[out] dlxheader unencypted header info (NULL allowed)
 *
 *  @retval u32 CRC32E value
 *  
 *  @authors Quyen Leba
 */
u32 datalogfeatures_calcCRC32E(u8 vbrev, DlxHeader* dlxheader)
{
    DlxHeader __dlxheader;
    DlxBlock dlxblock;
    const DlxBlock** dlxblock_ptr;
    u32 calc_crc32e;
    u8  analogcount;
    u8  i;

    analogcount = datalogfeatures_getInputCount(vbrev);
    dlxblock_ptr = datalogfeatures_getInputCollection(vbrev);

    //calculate content CRC32E
    calc_crc32e = 0xFFFFFFFF;
    for(i=0;i<analogcount;i++)
    {
        memcpy((char*)&dlxblock,(char*)dlxblock_ptr[i],sizeof(DlxBlock));
#if DATALOG_USE_ENCRYPTION
        crypto_blowfish_encryptblock_external_key((u8*)&dlxblock,sizeof(DlxBlock));
#endif
        calc_crc32e = crc32e_sw_calculateblock(calc_crc32e, (u32*)&dlxblock, sizeof(DlxBlock)/4);
    }

    //prepare header
    memset((char*)&__dlxheader,0,sizeof(__dlxheader));

    __dlxheader.dlxVersion = DATALOG_DLX_CURRENT_VERSION;
    __dlxheader.ecm = 0;
    strcpy((char*)__dlxheader.description,"Datalog Features");
    //dlxheader.strategy -- keep it empty
    __dlxheader.header_crc32e = 0xFFFFFFFF;
    __dlxheader.content_crc32e = calc_crc32e;
    __dlxheader.count = analogcount;
    __dlxheader.features = DATALOG_FEATURES_FLAGS;

    __dlxheader.header_crc32e = crc32e_sw_calculateblock(0xFFFFFFFF, (u32*)&__dlxheader, sizeof(DlxHeader)/4);
    if (dlxheader)
    {
        memcpy((char*)dlxheader,(char*)&__dlxheader,sizeof(DlxHeader));
    }
#if DATALOG_USE_ENCRYPTION
    crypto_blowfish_encryptblock_external_key((u8*)&__dlxheader,sizeof(DlxHeader));
#endif
    calc_crc32e = crc32e_sw_calculateblock(0xFFFFFFFF, (u32*)&__dlxheader, sizeof(DlxHeader)/4);

    for(i=0;i<analogcount;i++)
    {
        memcpy((char*)&dlxblock,(char*)dlxblock_ptr[i],sizeof(DlxBlock));
#if DATALOG_USE_ENCRYPTION
        crypto_blowfish_encryptblock_external_key((u8*)&dlxblock,sizeof(DlxBlock));
#endif
        calc_crc32e = crc32e_sw_calculateblock(calc_crc32e, (u32*)&dlxblock, sizeof(DlxBlock)/4);
    }

    return calc_crc32e;
}

/**
 *  datalogfeatures_check_file
 *  
 *  @brief Check if datalog feature file requires update
 *
 *  @param[in]  filename
 *  @param[in]  vb_boardrevision VB board revision
 *
 *  @retval u8 status
 *  
 *  @authors Quyen Leba
 */
u8 datalogfeatures_check_file(const u8 *filename, u8 vb_boardrevision)
{
    DlxHeader dlxheader;
    F_FILE *fptr;
    u32 crc32e_calc;
    u32 crc32e_file;
    u8  status;

    fptr = genfs_user_openfile(filename, "r");
    if (!fptr)
    {
        //log_push_error_point(0x4914);     //duplicated, abandone 0x4914
        return S_OPENFILE;
    }
    status = file_calcfilecrc32e(fptr, &crc32e_file);
    genfs_closefile(fptr);
    
    if(status != S_SUCCESS)
    {
        log_push_error_point(0x495E);
        return status;
    }

    crc32e_calc = datalogfeatures_calcCRC32E(vb_boardrevision,&dlxheader);
    if (crc32e_file != crc32e_calc)
    {
        log_push_error_point(0x4957);
        return S_CRC32E;
    }

    return S_SUCCESS;
}

/**
 *  datalogfeatures_check_file
 *  
 *  @brief Create new datalog feature file
 *
 *  @param[in]  filename
 *  @param[in]  vb_boardrevision VB board revision
 *
 *  @retval u8 status
 *  
 *  @authors Quyen Leba
 */
u8 datalogfeatures_create_file(const u8 *filename, u8 vb_boardrevision)
{
    F_FILE *fptr;
    const DlxBlock** dlxblock_ptr;
    DlxHeader dlxheader;
    DlxBlock dlxblock;
    u32 bytecount;
    u8  analogcount;
    u8  i;
    u8  status;

    fptr = genfs_user_openfile((u8 const*)filename, "w");
    if (!fptr)
    {
        log_push_error_point(0x4958);
        return S_OPENFILE;
    }

    switch(vb_boardrevision)
    {
    case VehicleBoard_BoardRev1:
    case VehicleBoard_BoardRev2:
    case VehicleBoard_BoardRev3:
    case VehicleBoard_BoardRev4:
        //do nothing
        break;
    default:
        log_push_error_point(0x4959);
        status = S_FAIL;
        goto datalogfeatures_create_file_done;
    }

    datalogfeatures_calcCRC32E(vb_boardrevision,&dlxheader);    //just to get complete unencrypted header
    //abandone errorpoint 0x492E

#if DATALOG_USE_ENCRYPTION
    crypto_blowfish_encryptblock_external_key((u8*)&dlxheader,sizeof(DlxHeader));
#endif
    bytecount = fwrite((char*)&dlxheader,1,sizeof(DlxHeader),fptr);
    if (bytecount != sizeof(DlxHeader))
    {
        log_push_error_point(0x4915);
        status = S_FAIL;
        goto datalogfeatures_create_file_done;
    }

    analogcount = datalogfeatures_getInputCount(vb_boardrevision);
    dlxblock_ptr = datalogfeatures_getInputCollection(vb_boardrevision);

    for(i=0;i<analogcount;i++)
    {
        memcpy((char*)&dlxblock,(char*)dlxblock_ptr[i],sizeof(DlxBlock));
#if DATALOG_USE_ENCRYPTION
        crypto_blowfish_encryptblock_external_key((u8*)&dlxblock,sizeof(DlxBlock));
#endif
        bytecount = fwrite((char*)&dlxblock,1,sizeof(DlxBlock),fptr);
        if (bytecount != sizeof(DlxBlock))
        {
            log_push_error_point(0x4916);
            status = S_FAIL;
            goto datalogfeatures_create_file_done;
        }
    }
    status = S_SUCCESS;
    
datalogfeatures_create_file_done:
    if (fptr)
    {
        genfs_closefile(fptr);
    }

    return status;
}
