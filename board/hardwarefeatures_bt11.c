/**
  **************** (C) COPYRIGHT 2011 SCT Performance, LLC *********************
  * File Name          : hardwarefeatures.c
  * Device / System    : SCT products using the "CommonCode" platform
  * Author             : Quyen Leba
  *
  * Version            : 1
  * Date               : 05/02/2011
  * Description        :
  *                    :
  *
  *
  * History            : 05/02/2011 M. Davis
  *                    :   Add SCT Copyright header,
  *
  ******************************************************************************
  */

#include <board/genplatform.h>
#include <common/statuscode.h>
#include "hardwarefeatures.h"

/**
 *  hardwarefeatures_has_wifi
 *  
 *  @brief Determine if WiFi hardware is available (iTSX device)
 *
 *  @retval bool TRUE: has WiFi
 *  
 *  @authors Quyen Leba
 */
bool hardwarefeatures_has_wifi()
{
    return FALSE;
}
