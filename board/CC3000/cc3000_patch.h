/**
  **************** (C) COPYRIGHT 2013 SCT Performance, LLC *********************
  * File Name          : cc3000_patch.h
  * Device / System    : SCT products using the "CommonCode" platform
  * Author             : Patrick Downs
  *
  * Version            : 1 
  * Date               : 09/26/2013
  * Description        : 
  *                    : 
  *
  *
  *
  ******************************************************************************
  */

#ifndef __PATCHPROG_H
#define __PATCHPROG_H

u8 cc3000_patchprogrammer_check();
void cc3000_readUpdatePatchVersion(u8* pVersion);

#endif  //__PATCHPROG_H

