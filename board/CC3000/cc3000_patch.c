
/*****************************************************************************
 *
 * {PatchProgrammer.c}
 *
 * Burn Patches to EEPROM
 *
 * Copyright (C) 2013 Texas Instruments Incorporated - http://www.ti.com/ 
 * ALL RIGHTS RESERVED 
 *                                                                                                                                                                                                                                                                     
 *
 *****************************************************************************/

#include <string.h>
#include <stdio.h>
#include <device_config.h>
#include <board/delays.h>
#include <board/MB103V/cc3000_hal.h>
#include "hci.h"
#include "wlan.h"
#include "nvmem.h"
#include "security.h"
#include "socket.h"
#include "cc3000.h"
#include "netapp.h"
#include "hci.h"
#include <fs/genfs.h>
#include <common/statuscode.h>
#include "cc3000_patch_driver.h"
#include "cc3000_patch_fw.h"
#include "cc3000_patch.h"
#include <common/debug/debug_output.h>

#define DISABLE                                        (0)

#define ENABLE                                        (1)
#define PIN_HIGH                              0xFF
#define PIN_LOW                              (!PIN_HIGH)

const unsigned char cRMdefaultParams[128] = { 0x03, 0x00, 0x01, 0x01, 0x14, 0x14, 0x00, 0x27, 0x27, 0x27, 0x27, 0x27, 0x27, 0x27, 0x27, 0x27, 0x27, 0x27, 0x27, 
    0x27, 0x27, 0x23, 0x25, 0x25, 0x25, 0x25, 0x25, 0x25, 0x25, 0x25, 0x25, 0x23, 0x23, 0x23, 0x23, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
    0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x50, 0x50, 0x50, 0x50, 0x50, 0x50, 0x50, 0x50, 0x50, 0x50, 0x50, 0x50, 0x50, 0x50, 0x50, 0x50, 0x50,
    0x50, 0x50, 0x50, 0x50, 0x50, 0x50, 0x50, 0x50, 0x50, 0x50, 0x50, 0x50, 0x50, 0x50, 0x50, 0x50, 0x50, 0x50, 0x01, 0x77, 0x80, 0x1D, 0x1F, 0x22, 0x26, 0x28, 0x29,
    0x1A, 0x1F, 0x22, 0x24, 0x26, 0x28, 0x16, 0x1D, 0x1E, 0x20, 0x24, 0x25, 0x1E, 0x2D, 0x01, 0x02, 0x02, 0x02, 0x02, 0x00, 0x15, 0x15, 0x15, 0x11, 0x15, 0x15, 0x0E, 0x00};

unsigned char ucStatus_Dr, ucStatus_FW, return_status = 0xFF;;
signed char mac_status = -1;
unsigned char counter = 0;

// array to store RM parameters from EEPROM
unsigned char cRMParamsFromEeprom[128];

// array to store MAC address from EEPROM
unsigned char cMacFromEeprom[MAC_ADDR_LEN];

unsigned char     is_allocated[NVMEM_RM_FILEID + 1];
unsigned char     is_valid[NVMEM_RM_FILEID + 1];
unsigned char     write_protected[NVMEM_RM_FILEID + 1];
unsigned short     file_address[NVMEM_RM_FILEID + 1];
unsigned short     file_length[NVMEM_RM_FILEID + 1];

// 2 dim array to store address and length of new FAT
unsigned short aFATEntries[2][NVMEM_RM_FILEID + 1] = 
/*  address     */  {{0x50,     0x1f0,     0x390,     0x1390,     0x2390,     0x4390,     0x6390,     0x63a0,     0x63b0,     0x63f0,     0x6430,     0x6830},
/*  length    */    {0x1a0,     0x1a0,     0x1000,     0x1000,     0x2000,     0x2000,     0x10,     0x10,     0x40,     0x40,     0x400,     0x200}};
     /* 0. NVS */
     /* 1. NVS Shadow */
     /* 2. Wireless Conf */
     /* 3. Wireless Conf Shadow */
     /* 4. BT (WLAN driver) Patches */
     /* 5. WiLink (Firmware) Patches */
     /* 6. MAC addr */
     /* 7. Frontend Vars */
     /* 8. IP config */
     /* 9. IP config Shadow */
     /* 10. Bootloader Patches */
     /* 11. Radio Module params */
     /* 12. AES128 for smart config */
     /* 13. user file */
     /* 14. user file */
     /* 15. user file */

//*****************************************************************************
//
//! fat_read_content
//!
//! @param[out] is_allocated  array of is_allocated in FAT table:\n
//!                         an allocated entry implies the address and length of the file are valid.
//!                           0: not allocated; 1: allocated.
//! @param[out] is_valid  array of is_valid in FAT table:\n
//!                         a valid entry implies the content of the file is relevant.
//!                           0: not valid; 1: valid.
//! @param[out] write_protected  array of write_protected in FAT table:\n
//!                         a write protected entry implies it is not possible to write into this entry.
//!                           0: not protected; 1: protected.
//! @param[out] file_address  array of file address in FAT table:\n
//!                         this is the absolute address of the file in the EEPROM.
//! @param[out] file_length  array of file length in FAT table:\n
//!                         this is the upper limit of the file size in the EEPROM.
//!
//! @return on succes 0, error otherwise
//!
//! @brief  parse the FAT table from eeprom 
//
//*****************************************************************************
unsigned char fat_read_content(unsigned char *is_allocated,
                               unsigned char *is_valid,
                               unsigned char *write_protected,
                               unsigned short *file_address,
                               unsigned short *file_length)
{
    unsigned short  index;
    unsigned char   ucStatus;
    unsigned char   fatTable[48];
    unsigned char*  fatTablePtr = fatTable;
    
    // read the FAT
    ucStatus = nvmem_read(16, 48, 4, fatTable); 
    
    fatTablePtr = fatTable;
    
    for (index = 0; index <= NVMEM_RM_FILEID; index++)
    {
        *is_allocated++ = (*fatTablePtr) & BIT0;
        *is_valid++ = ((*fatTablePtr) & BIT1) >> 1;
        *write_protected++ = ((*fatTablePtr) & BIT2) >> 2;
        *file_address++ = (*(fatTablePtr+1)<<8) | (*fatTablePtr) & (BIT4|BIT5|BIT6|BIT7);
        *file_length++ = (*(fatTablePtr+3)<<8) | (*(fatTablePtr+2)) & (BIT4|BIT5|BIT6|BIT7);
        
        // move to next file ID
        fatTablePtr += 4;  
    }
    
    return ucStatus;
}

//*****************************************************************************
//
//! fat_write_content
//!
//! @param[in] file_address  array of file address in FAT table:\n
//!                         this is the absolute address of the file in the EEPROM.
//! @param[in] file_length  array of file length in FAT table:\n
//!                         this is the upper limit of the file size in the EEPROM.
//!
//! @return on succes 0, error otherwise
//!
//! @brief  parse the FAT table from eeprom 
//
//*****************************************************************************
unsigned char fat_write_content(
                                                                unsigned short const *file_address,
                                                                unsigned short const *file_length)
{
    unsigned short  index = 0;
    unsigned char   ucStatus;
    unsigned char   fatTable[48];
    unsigned char*  fatTablePtr = fatTable;
    
    // first, write the magic number
    ucStatus = nvmem_write(16, 2, 0, "LS"); 
    
    for (; index <= NVMEM_RM_FILEID; index++)
    {
        // write address low char and mark as allocated
        *fatTablePtr++ = (unsigned char)(file_address[index] & 0xff) | BIT0;
        
        // write address high char
        *fatTablePtr++ = (unsigned char)((file_address[index]>>8) & 0xff);
        
        // write length low char
        *fatTablePtr++ = (unsigned char)(file_length[index] & 0xff);
        
        // write length high char
        *fatTablePtr++ = (unsigned char)((file_length[index]>>8) & 0xff);        
    }
    
    // second, write the FAT
    ucStatus = nvmem_write(16, 48, 4, fatTable); 
    
    // third, we want to erase any user files
    memset(fatTable, 0, sizeof(fatTable));
    ucStatus = nvmem_write(16, 16, 52, fatTable); 
    
    return ucStatus;
}

//*****************************************************************************
//
//! main
//!
//!  @param  None
//!
//!  @return none
//!
//!  @brief  The main loop is executed here
//
//*****************************************************************************
#define PATCH_BACKUP_FILENAME   "patchbackup.bin"
u8 patchprogrammer(void)
{
    unsigned char   *pRMParams;
    F_FILE *file;
    s32 fbytes;
    
    // init board and request to load with no patches.
    // this is in order to overwrite restrictions to write to specific places in EEPROM
    cc3000_deinit();
    cc3000_init(1);

    for(int i = 0; i < 10; i++)
        delays_counter(DELAYS_COUNTER_100MS); // 1 second delay

    // read MAC address
    mac_status = nvmem_get_mac_address(cMacFromEeprom);
    
    return_status = 1;
    
    while ((return_status) && (counter < 3))
    {
        // read RM parameters
        return_status = nvmem_read(NVMEM_RM_FILEID, 128, 0, cRMParamsFromEeprom); 
        
        counter++;
    }
    
    // if RM file is not valid, load the default one
    if (counter == 3)
    {
         pRMParams = (unsigned char *)cRMdefaultParams;
    }
    else
    {
        pRMParams = cRMParamsFromEeprom;
    }
    
    file = genfs_user_openfile(PATCH_BACKUP_FILENAME,"wb");
    if (file == NULL)
    {
        return S_OPENFILE;
    }
    
    // Save mac address
    fbytes = fwrite((char*)(cMacFromEeprom), 1, sizeof(cMacFromEeprom), file);
    // Save eeprom parameters
    fbytes += fwrite((char*)(cRMParamsFromEeprom), 1, sizeof(cRMParamsFromEeprom), file);
    
    genfs_closefile(file);
    
    if(fbytes != (sizeof(cMacFromEeprom) + sizeof(cRMParamsFromEeprom)))
    {
        // Failed to backup data
        return S_FAIL;
    }
    
    return_status = 1;
    
    while (return_status)
    {
        // write new FAT
        return_status = fat_write_content(aFATEntries[0], aFATEntries[1]);
    }
    
    for(int i = 0; i < 10; i++)
        delays_counter(DELAYS_COUNTER_100MS); // 1 second delay
    
    return_status = 1;
    
    while (return_status)
    {
        // write RM parameters
        return_status = nvmem_write(NVMEM_RM_FILEID, 128, 0, pRMParams); 
    }
    
    return_status = 1;
    
    // write back the MAC address, only if exist
    if (mac_status == 0)
    {
        // zero out MCAST bit if set
        cMacFromEeprom[0] &= 0xfe;
        while (return_status)
        {
            return_status = nvmem_set_mac_address(cMacFromEeprom);
        }
    }
    
    ucStatus_Dr = 1;
    
    while (ucStatus_Dr)
    {
        //writing driver patch to EEPRROM - PROTABLE CODE
        // Note that the array itself is changing between the different Service Packs    
        ucStatus_Dr = nvmem_write_patch(NVMEM_WLAN_DRIVER_SP_FILEID, sizeof(wlan_drv_patch), wlan_drv_patch);        
    }
    
    ucStatus_FW = 1;
    
    while (ucStatus_FW)
    {
        //writing FW patch to EAPRROM  - PROTABLE CODE
        //Note that the array itself is changing between the different Service Packs   
        ucStatus_FW = nvmem_write_patch(NVMEM_WLAN_FW_SP_FILEID, sizeof(fw_patch), fw_patch);
    }
    
    // init board and request to load with patches.
    cc3000_init(0);
    
    return S_SUCCESS;
}

/**
 *  cc3000_readPatchVersion
 *  
 *  @brief                     Read Patch version
 *  
 *  @param [out] pVersion     buffer where data should be copied, atleast 2 bytes in size
 *  @retval                 returns a STATUS_CODE enum
 *  
 *  @details                 Reads the patch version from the EEPROM
 *  
 *  @authors Patrick Downs
 *  
 */
void cc3000_readUpdatePatchVersion(u8* pVersion)
{    
    pVersion[0] = CC3000_FW_VERSION_MINOR;
    pVersion[1] = CC3000_FW_VERSION_MAJOR;
    pVersion[2] = 0;
}

u8 cc3000_patchprogrammer_check()
{
    unsigned char pVersion[6];
    u8 status;
    
    status = S_FAIL;
    
    memset(pVersion, 0, sizeof(pVersion));
    cc3000_readPatchVersion(pVersion);
        
    if((CC3000_FW_VERSION_MAJOR > pVersion[0]) || (CC3000_FW_VERSION_MINOR > pVersion[1]))
    {
            status = patchprogrammer();
            if(status == 0)
            {
                status = S_SUCCESS;                
            }
            else
            {
                status = S_FAIL;
            }
    }
    else
    {
        status = S_NO_UPDATE;
    }
    
    return status;
}




