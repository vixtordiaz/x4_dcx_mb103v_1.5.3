/**
  **************** (C) COPYRIGHT 2011-2014 SCT Performance, LLC ****************
  * File Name          : cc3000.h
  * Device / System    : SCT products using the "CommonCode" platform
  * Author             : Quyen Leba
  *
  * Version            : 1 
  * Date               : 05/02/2011
  * Description        : 
  *                    : 
  *
  ******************************************************************************
  */

//------------------------------------------------------------------------------
// Log error point: 0x7100 -> 0x71FF
//------------------------------------------------------------------------------

#include <string.h>
#include <stdio.h>
#include <time.h>
#include <device_config.h>
#include <fs/genfs.h>
#include <board/genplatform.h>
#include <board/delays.h>
#include <common/statuscode.h>
#include <common/cmdif_func_wifi.h>
#include <common/debug/debug_output.h>

#include "hci.h"
#include "wlan.h"
#include "nvmem.h"
#include "security.h"
#include "socket.h"
#include "netapp.h"
#include "cc3000_patch.h"
#include <common/debug/debug_output.h>
#include <fs/genfs.h>

#define NETAPP_IPCONFIG_MAC_OFFSET                      (20)

#define CC3000_HOSTNAME_MAXLENGTH                       32

#define SOCKET_TIMEOUT_MS   200

struct
{
    volatile bool isInitialized;
    volatile u32 ulSmartConfigFinished;
    volatile u32 ulCC3000Connected;
    volatile u32 ulCC3000DHCP;
    volatile u32 OkToDoShutDown;
    volatile u32 ulCC3000DHCP_configured;
    volatile u8  ucStopSmartConfig;
    volatile u8  ulSocket;
    volatile u8  hostname[CC3000_HOSTNAME_MAXLENGTH];
    volatile u32 hostip;
}cc3000_info;

//#define MAX_SOCKETS 32
//boolean closed_sockets[MAX_SOCKETS] = {false, false, false, false};
s32 opensocketid = -1;

void cc3000_UsynchCallback(long lEventType, char *data, unsigned char length);
char *sendWLFWPatch(unsigned long *Length);
char *sendDriverPatch(unsigned long *Length);
char *sendBootLoaderPatch(unsigned long *Length);
u8 cc3000_parseHttpHeader(u8 *header, u32* headerlen, u32* contentlength);
void cc3000_restart(void);
void cc3000_getIpConfig(CC3000_IPCONFIG *ipconfig);
u8 cc3000_connectUDP(u32 destIP, u16 destPort);
u8 cc3000_openSocket(u32 destIP, u16 destPort, CC3000_SOCKETTYPE type);
u8 cc3000_closeSocket();
u8 cc3000_recv(u8 *rxbuffer, u32 rxsize, s32 socketid, s32 *bytes);
void cc3000_progress(u8 progress);
u8 cc3000_isValidIp4(const u8 *str);
u8 cc3000_ipToInt(const u8 *ip, u32 *output);

//
// Smart Config Prefix
//
char aucCC3000_prefix[] = {'T', 'T', 'T'};

u8 appboard_Revision = 0xFF;

//------------------------------------------------------------------------------
// 
//------------------------------------------------------------------------------
#ifdef __DEBUG_JTAG_
void cc3000_debug()
{
    CC3000_IPCONFIG ipconfig;
    u8 outbuf[80];
//    struct tm * ptm;
//    u32 time;
    volatile int status;
    char setDHCP = 0;
    char setProfile = 0;
    unsigned char pVersion[6];
    u8 device_name[] = "X4 Pat";
    
//    u32 aucIP = 0x2000A8C0; 
//    u32 aucSubnetMask = 0x00FFFFFF;
//    u32 aucDefaultGateway = 0x0100A8C0;
//    u32 aucDNSServer = 0x0100A8C0;
    
    // SEMA Intranet settings
    // No DHCP
    // Static IP: 192.168.101.102
    // Gateway: 192.168.101.1
    // DNS: no DNS
    u32 aucIP = 0x6665A8C0;
    u32 aucSubnetMask = 0x00FFFFFF;
    u32 aucDefaultGateway = 0x0165A8C0;
    u32 aucDNSServer = 0x00000000;
    
    if(setDHCP)
    {
        if(setDHCP == 2)
        {
            aucIP = 0x00;
            aucSubnetMask = 0x00;
            aucDefaultGateway = 0x00;
            aucDNSServer = 0x0100A8C0;
        }
        
        status = netapp_dhcp((unsigned long*)&aucIP, (unsigned long*)&aucSubnetMask, 
                             (unsigned long*)&aucDefaultGateway, (unsigned long*)&aucDNSServer);
        
        cc3000_info.ulCC3000Connected = 0;
        cc3000_info.ulCC3000DHCP = 0;
        
//        debug_out("Restarting wlan...");
//        //reset the CC3000
//        wlan_stop();
//        delays(2000,'m');
//        wlan_start(0);
        debug_out("Done");
    }
    
    if(setProfile)
    {
        CC3000_AP_RESULTS result;
        u8 key[32];
        u32 priority;
        
        switch(setProfile)
        {
        case 1:
            strcpy((char*)key, (const char*)"iHaveNowhereToPlugIn");
            strcpy((char*)result.ssidName, (const char*)"SCTDyno_Secured");
            result.isValid = 1;
            result.security = WLAN_SEC_WPA;
            break;
        case 2:
            strcpy((char*)key, (const char*)"a0a0a0a0a0");
            strcpy((char*)result.ssidName, (const char*)"SCT_Training");
            result.isValid = 1;
            result.security = WLAN_SEC_WPA2; //WLAN_SEC_WEP;
            break;
        case 3:
            cc3000_disconnect();            
//            status = cc3000_connectToAP("SCT_Training", NULL, "A0A0A0A0A0", WLAN_SEC_WEP);
            break;
        default:
            break;            
        }
        
        result.ssidLength = strlen((char*)result.ssidName);
        priority = 1;
        
        status = cc3000_deleteProfiles(255);
        
        status = cc3000_addProfile(&result, key, strlen((char*)key), priority);
    }
    
    
    debug_out("Waiting for connection....");
    while(!cc3000_info.ulCC3000Connected)
    {
        
    }
    debug_out("Connected");
    
    
//    aucIP = 0x00;
//    aucSubnetMask = 0x00;
//    aucDefaultGateway = 0x00;
//    aucDNSServer = 0x00;
//    
//    status = netapp_dhcp((unsigned long*)&aucIP, (unsigned long*)&aucSubnetMask, 
//                         (unsigned long*)&aucDefaultGateway, (unsigned long*)&aucDNSServer);
    
    debug_out("Waiting for DHCP....");
    while(!cc3000_info.ulCC3000DHCP)
    {
        
    }
    debug_out("Done");
    
    cc3000_getIpConfig(&ipconfig);
    debug_out("CC3000 Info:");
    strcpy((char*)outbuf, "IP address: ");
    strcat((char*)outbuf, (char const*)ipconfig.ipaddress);
    debug_out(outbuf);
    strcpy((char*)outbuf, "Subnet mask: ");
    strcat((char*)outbuf, (char const*)ipconfig.subnetmask);
    debug_out(outbuf);
    strcpy((char*)outbuf, "Gateway: ");
    strcat((char*)outbuf, (char const*)ipconfig.gateway);
    debug_out(outbuf);
    strcpy((char*)outbuf, "DHCP server: ");
    strcat((char*)outbuf, (char const*)ipconfig.dhcpserver);
    debug_out(outbuf);
    strcpy((char*)outbuf, "DNS server: ");
    strcat((char*)outbuf, (char const*)ipconfig.dnsserver);
    debug_out(outbuf);
    strcpy((char*)outbuf, "MAC address: ");
    strcat((char*)outbuf, (char const*)ipconfig.macaddress);
    debug_out(outbuf);
    memset(outbuf, 0, sizeof(outbuf));
    strcpy((char*)outbuf, "Connected to SSID: ");
    memcpy(outbuf+strlen((const char*)outbuf), ipconfig.ssid, 32);
    debug_out(outbuf);
    
    memset(pVersion, 0, sizeof(pVersion));
    cc3000_readPatchVersion(pVersion);
    
    sprintf((char*)outbuf, "Patch Version: %d.%d", pVersion[0], pVersion[1]);
    debug_out(outbuf);
    
    status = mdnsAdvertiser(1,(char*)device_name,strlen((char*)device_name));
    
//    status = cc3000_getGeoLocation();
//    if(status != S_SUCCESS)
//    {
//        
//    }
        
//    status = cc3000_getNtpTime(&time);
//    if(status == S_SUCCESS)
//    {
//        ptm = gmtime((time_t const*)&time);
//        
//        memset(outbuf, 0, sizeof(outbuf));
//        strftime((char*)outbuf, sizeof(outbuf)-1, "%c", ptm);
//        debug_out(outbuf);
//    }
    
    bool wait = TRUE;
    
    while(wait);
    
//    status = cmdif_func_wifi_check_update(0x00);
    
    //status = cc3000_httpPostRequest("postcatcher.in", "/catchers/52a603c990d65b0200000011", "\\USER\\outfile.txt", "\\USER\\infile.txt", 80, (PROGRESS_REPORT_FUNC_SIMPLE)cc3000_progress);
//    status = cc3000_httpPostRequest("posttestserver.com", "/post.php", "\\USER\\outfile.txt", "\\USER\\infile.txt", 80, (PROGRESS_REPORT_FUNC_SIMPLE)cc3000_progress);
//    status = cc3000_httpPostRequest("services.sctflash.com", "/api/CurrentDeviceConfiguration", "\\USER\\outfile.txt", "\\USER\\infile.txt", 80, (PROGRESS_REPORT_FUNC_SIMPLE)cc3000_progress);
//    if(status != S_SUCCESS)
//    {
//        status = S_FAIL;
//    }
      
//    memset(outbuf, 0, sizeof(outbuf));
//    status = cc3000_httpGetRequest("www.sctflash.com", "/PDF/6600.pdf", "\\USER\\6600.pdf", 80, (PROGRESS_REPORT_FUNC_SIMPLE)cc3000_progress);
//    if(status != S_SUCCESS)
//    {
//        status = S_FAIL;
//    }
    
//    status = cc3000_httpGetRequest("manuals.info.apple.com", "/MANUALS/1000/MA1643/en_US/macbook_air-13-inch-mid-2013_quick_start.pdf", "\\USER\\apple.pdf", 80, (PROGRESS_REPORT_FUNC_SIMPLE)cc3000_progress);
//    if(status != S_SUCCESS)
//    {
//        status = S_FAIL;
//    }

//    status = cc3000_httpGetRequest("eng-n7p6-PD.sctflash.local", "/test2.bin", "test2.bin", 8080);
//    if(status != S_SUCCESS)
//    {
//        status = S_FAIL;
//    }
    
    while(1);
}
#endif

void cc3000_progress(u8 progress)
{
    u8 buffer[10];
    sprintf((char*)buffer, "%d", progress);
    debug_out(buffer);
}

void cc3000_startup()
{    
    if(cc3000_info.isInitialized == FALSE)
    {
        cc3000_init(0);
    }
}

bool cc3000_isPoweredOn(void)
{    
    if(cc3000_info.isInitialized)
    {
        return TRUE;
    }
    else
    {
        return FALSE;
    }
}
        
bool cc3000_isConnected(void)
{
    if(cc3000_info.ulCC3000Connected)
        return TRUE;
    else
        return FALSE;
}

bool cc3000_isDhcpDone(void)
{
    if(cc3000_info.ulCC3000DHCP)
        return TRUE;
    else
        return FALSE;
}

void cc3000_displayWifiInfo()
{
    CC3000_IPCONFIG ipconfig;
    u8 outbuf[80];
    unsigned char pVersion[6];
    
    cc3000_getIpConfig(&ipconfig);
    debug_out("CC3000 Info:");
    strcpy((char*)outbuf, "IP address: ");
    strcat((char*)outbuf, (char const*)ipconfig.ipaddress);
    debug_out(outbuf);
    strcpy((char*)outbuf, "Subnet mask: ");
    strcat((char*)outbuf, (char const*)ipconfig.subnetmask);
    debug_out(outbuf);
    strcpy((char*)outbuf, "Gateway: ");
    strcat((char*)outbuf, (char const*)ipconfig.gateway);
    debug_out(outbuf);
    strcpy((char*)outbuf, "DHCP server: ");
    strcat((char*)outbuf, (char const*)ipconfig.dhcpserver);
    debug_out(outbuf);
    strcpy((char*)outbuf, "DNS server: ");
    strcat((char*)outbuf, (char const*)ipconfig.dnsserver);
    debug_out(outbuf);
    strcpy((char*)outbuf, "MAC address: ");
    strcat((char*)outbuf, (char const*)ipconfig.macaddress);
    debug_out(outbuf);
    memset(outbuf, 0, sizeof(outbuf));
    strcpy((char*)outbuf, "Connected to SSID: ");
    memcpy(outbuf+strlen((const char*)outbuf), ipconfig.ssid, 32);
    debug_out(outbuf);
    
    memset(pVersion, 0, sizeof(pVersion));
    cc3000_readPatchVersion(pVersion);
    
    sprintf((char*)outbuf, "Patch Version: %d.%d", pVersion[0], pVersion[1]);
    debug_out(outbuf);    
}

u8 cc3000_setMdnsAdvertiser(u8 *device_name)
{
    u8 status;
    
    cc3000_init_cs(); // IMPORTANT: All public functions require this!!
    
    status = mdnsAdvertiser(1,(char*)device_name,strlen((char*)device_name));
    if(status == 0)
    {
        status = S_SUCCESS;
    }
    else
    {
        status = S_FAIL;
    }
    
    return status;
}

//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
void cc3000_UsynchCallback(long lEventType, char *data, unsigned char length)
{
    if (lEventType == HCI_EVNT_WLAN_ASYNC_SIMPLE_CONFIG_DONE)
    {
        cc3000_info.ulSmartConfigFinished = 1;
        cc3000_info.ucStopSmartConfig = 1;  
    }
    
    if (lEventType == HCI_EVNT_WLAN_UNSOL_CONNECT)
    {
        cc3000_info.ulCC3000Connected = 1;
    }
    
    if (lEventType == HCI_EVNT_WLAN_UNSOL_DISCONNECT)
    {        
        cc3000_info.ulCC3000Connected = 0;
        cc3000_info.ulCC3000DHCP = 0;
        cc3000_info.ulCC3000DHCP_configured = 0;
    }
    
    if (lEventType == HCI_EVNT_WLAN_UNSOL_DHCP)
    {
        // Notes: 
        // 1) IP config parameters are received swapped
        // 2) IP config parameters are valid only if status is OK, i.e. ulCC3000DHCP becomes 1

        // only if status is OK, the flag is set to 1 and the addresses are valid
        if ( *(data + NETAPP_IPCONFIG_MAC_OFFSET) == 0)
        {
            //Note: IP address is data[3].data[2].data[1].data[0]
            cc3000_info.ulCC3000DHCP = 1;
        }
        else
        {
            cc3000_info.ulCC3000DHCP = 0;
        }
    }

    if (lEventType == HCI_EVENT_CC3000_CAN_SHUT_DOWN)
    {
        cc3000_info.OkToDoShutDown = 1;
    }
}

//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
char *sendWLFWPatch(unsigned long *Length)
{
    *Length = 0;
    return NULL;
}

//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
char *sendDriverPatch(unsigned long *Length)
{
    *Length = 0;
    return NULL;
}

//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
char *sendBootLoaderPatch(unsigned long *Length)
{
    *Length = 0;
    return NULL;
}

/**
 *  cc3000_init
 *  
 *  @brief Init CC3000 module
 *  
 *  @param [in] cRequestPatch   0 run patch, 1 don't run patch code
 *  @param [in] wlanonly        0 Init SPI and WLAN, 1 Init WLAN only
 *  @retval                     none
 *  
 *  @details Initialize hardware interfaces, data structures, and CC3000 module
 *  
 *  @authors Quyen Leba, Patrick Downs
 *  
 */
void cc3000_init(unsigned short cRequestPatch)
{
    s32 delay;
    
    cc3000_hal_init();
    
    if(cc3000_info.isInitialized)
    {
        //reset the CC3000
        wlan_stop();
        
        delay = 20;
        while(delay)
        {
            delays_counter(TIME_LOOP_COUNT_100MS);
            delay--;
        }
    }
    
    cc3000_info.isInitialized = TRUE;
    
    wlan_init(cc3000_UsynchCallback,
              sendWLFWPatch, sendDriverPatch, sendBootLoaderPatch,
              cc3000_hal_readinterruptpin,
              cc3000_hal_wlaninterruptenable, cc3000_hal_wlaninterruptdisable,
              cc3000_hal_writewlanpin);
    
    //trigger a WLAN device
    wlan_start(cRequestPatch); // 0 run patch, 1 don't run patch code
    
    //mask out all non-required events from CC3000
    wlan_set_event_mask(HCI_EVNT_WLAN_KEEPALIVE | HCI_EVNT_WLAN_UNSOL_INIT |
                        HCI_EVNT_WLAN_ASYNC_PING_REPORT);
    
    // Update netapp timeout values, set aucInactivity to 20 seconds;
    UINT32 aucDHCP       = 14400;
    UINT32 aucARP        = 3600;
    UINT32 aucKeepalive  = 10;
    UINT32 aucInactivity = 20;
    netapp_timeout_values(&aucDHCP, &aucARP, &aucKeepalive, &aucInactivity);
}

void cc3000_deinit()
{
    cc3000_info.isInitialized = FALSE;
    cc3000_info.ulCC3000DHCP = 0;
    cc3000_info.ulCC3000Connected = 0;
    cc3000_info.ulSmartConfigFinished = 0;
    cc3000_info.ulSocket = 0;
    
    cc3000_hal_disable();
    delays(100,'m');
}

//------------------------------------------------------------------------------
/**
 *  cc3000_restart
 *  
 *  @brief         Restart the CC3000 module
 *  
 *  @retval     none
 *  
 *  @details     Restarts the CC3000 module
 *  
 *  @authors Patrick Downs
 *  
 */
void cc3000_restart()
{
    //reset the CC3000
    cc3000_deinit();
    cc3000_init(0);
    
//    wlan_stop();
//    delays(2000,'m');
//    wlan_start(0);
}

/**
 *  cc3000_init_cs
 *  
 *  @brief Appboard Rev 0 ChipSelect work around
 *  
 *  @retval                     none
 *  
 *  @details Power on and init the CC3000 before asserting CS
 *  
 *  @authors Patrick Downs
 *  
 */
void cc3000_init_cs()
{
    if(appboard_Revision == 0 || appboard_Revision == 0xFF)
    {
        if(cc3000_info.isInitialized == FALSE)
        {
            cc3000_init(0);
        }
    }
}

/**
 *  cc3000_deinit_cs
 *  
 *  @brief Appboard Rev 0 ChipSelect work around
 *  
 *  @retval                     none
 *  
 *  @details Power off and deinit the CC3000 before asserting different CS
 *  
 *  @authors Patrick Downs
 *  
 */
void cc3000_deinit_cs()
{
    if(appboard_Revision == 0 || appboard_Revision == 0xFF)
    {
        cc3000_deinit();
        delays_counter(TIME_LOOP_COUNT_100MS);
    }
}

/**
 *  cc3000_smartconfig
 *  
 *  @brief         Handles individual smartconfig processes
 *  
 *  @param [in] opcode    CC3000_SMARTCONFIG_OPCODE enum 
 *
 *  @retval     returns a STATUS_CODE enum
 *  
 *  @details     Performs individual smart config processes by opcode
 *  
 *  @authors Patrick Downs
 *  
 */
u8 cc3000_smartConfig(CC3000_SMARTCONFIG_OPCODE opcode)
{
    u8 status;
    
    cc3000_init_cs(); // IMPORTANT: All public functions require this!!
        
    switch(opcode)
    {
    case CC3000_SMARTCONFIG_OPCODE_START:
        cc3000_smartconfig_start();
        break;
    case CC3000_SMARTCONFIG_OPCODE_ABORT:
        cc3000_smartconfig_abort();
        break;
    case CC3000_SMARTCONFIG_OPCODE_FINISH:
        cc3000_smartconfig_finish();
        break;
    default:
        break;
    }
    
    status = S_SUCCESS;
    
    return status;
}

/**
 *  cc3000_smartconfig_start
 *  
 *  @brief         Start the smartconfig process
 *  
 *  @retval     none
 *  
 *  @details     This will put the CC3000 in the smartconfig mode which allows 
 *              the use of a smart phone or computer to send the WIFI AP connection
 *              details using a smartconfig enabled app.
 *  
 *  @authors Quyen Leba, Patrick Downs
 *  
 */
void cc3000_smartconfig_start()
{
    // Simple Config Prefix
    u8 aucCC3000_prefix[] = {'T', 'T', 'T'};

    cc3000_info.ulSmartConfigFinished = 0;
    cc3000_info.ulCC3000Connected = 0;
    cc3000_info.ulCC3000DHCP = 0;
    cc3000_info.OkToDoShutDown = 0;

    //reset all the previous configuration
    wlan_ioctl_set_connection_policy(DISABLE, DISABLE, DISABLE);
    wlan_ioctl_del_profile(255);

    //wait until CC3000 is disconnected
    while (cc3000_info.ulCC3000Connected == 1)
    {
        delays_counter(TIME_LOOP_COUNT_100MS);
    }
    debug_out("Disconnected");

    wlan_smart_config_set_prefix((char*)aucCC3000_prefix);

    debug_out("Starting Smartconfig..");
    
    //start the SmartConfig start process
    wlan_smart_config_start(1);
}

/**
 *  cc3000_smartconfig_abort
 *  
 *  @brief         Abort the smartconfig process
 *  
 *  @retval     none
 *  
 *  @details     Restarts the CC3000 module to cancel smartconfig
 *  
 *  @authors Patrick Downs
 *  
 */
void cc3000_smartconfig_abort()
{
    //reset the CC3000
    cc3000_restart();
}

/**
 *  cc3000_smartconfig_isdone
 *  
 *  @brief  Check if the smartconfig process is done
 *  
 *  @retval     bool TRUE or FALSE
 *  
 *  @details    Returns the status of the smartconfig process
 *
 *  @authors Patrick Downs
 *  
 */
bool cc3000_smartconfig_isdone()
{
    if(cc3000_info.ulSmartConfigFinished)
    {
        return TRUE;
    }
    else
    {
        return FALSE;
    }
}

/**
 *  cc3000_smartconfig_finish
 *  
 *  @brief      Finish the smartconfig process
 *  
 *  @retval     none
 *  
 *  @details    Once the smartconfig process is done, use finsih command to complete
 *              process.
 *  
 *  @authors Patrick Downs
 *  
 */
void cc3000_smartconfig_finish()
{
    char device_name[] = "SCT X4";
    
    // TODOPD: Use this key for production or when we have our own Smartconfig APP
//    const u8 smartconfigkey[] = "69B1D1958B438D65"; //New AES Key "69B1D1958B438D65"
    
    //old AES Key from example code "smartconfigAES16"
    const u8 smartconfigkey[] = {0x73,0x6d,0x61,0x72,0x74,0x63,0x6f,0x6e,0x66,0x69,0x67,0x41,0x45,0x53,0x31,0x36}; 
    
    //wait for Smart config to finish
    while (cc3000_info.ulSmartConfigFinished == 0)
    {
        //TODOQ: timeout here
        delays_counter(TIME_LOOP_COUNT_100MS);
    }
    debug_out("Done");

    //create new entry for AES encryption key
    nvmem_create_entry(NVMEM_AES128_KEY_FILEID,16);

    //write AES key to NVMEM
    aes_write_key((unsigned char *)(&smartconfigkey[0]));
    
    //decrypt configuration information and add profile
    wlan_smart_config_process();

    //configure to connect automatically to the AP retrieved in the smart config process
    wlan_ioctl_set_connection_policy(DISABLE, DISABLE, ENABLE);
    
    debug_out("Added new profile");
    debug_out("Restarting wlan...");

    //reset the CC3000
    cc3000_restart();

    debug_out("Done");
    
    // Mask out all non-required events
    wlan_set_event_mask(HCI_EVNT_WLAN_KEEPALIVE | HCI_EVNT_WLAN_UNSOL_INIT |
                        HCI_EVNT_WLAN_ASYNC_PING_REPORT | HCI_EVNT_BSD_TCP_CLOSE_WAIT);
    
    debug_out("Waiting for connection to AP...");
    while(!cc3000_info.ulCC3000Connected)
    {
        
    }
    
    debug_out("Waiting for IP from DHCP...");
    while(!cc3000_info.ulCC3000DHCP)
    {
        
    }
    
    debug_out("Sending mDNS to signal smart config finished...");
    
    mdnsAdvertiser(1,device_name,strlen(device_name));
        
    debug_out("Done");
}

/**
 *  cc3000_patchprogrammer
 *  
 *  @brief         Run the patch programmer
 *  
 *  @retval     returns a STATUS_CODE enum
 *  
 *  @details     Updates the patch code in the CC3000 EEPROM with the latest 
 *              binary data.
 *  
 *  @authors Patrick Downs
 *  
 */
u8 cc3000_patchprogrammer()
{
    u8 status;
    
    cc3000_init_cs(); // IMPORTANT: All public functions require this!!
    
    status = cc3000_patchprogrammer_check();
    
    return status;
}

/**
 *  cc3000_get_patchprogrammer_version_uint
 *  
 *  @brief         Read patch version in uint
 *  
 *  @retval     returns a STATUS_CODE enum
 *  
 *  @details     Read patch version as uint representation 
 *  
 *  @authors Patrick Downs
 *  
 */
u32 cc3000_patchprogrammer_read_updateversion_uint()
{
    u8 pVersion[6];
    
    cc3000_readUpdatePatchVersion(pVersion);
    
    return (u32)((pVersion[1]*1000) +  pVersion[0]);
}

/**
 *  cc3000_getMacAddress
 *  
 *  @brief                 Read Mac Address from eeprom
 *  
 *  @param [out] mac     buffer where mac address will be copied, see MAC_ADDR_LEN
 *  @retval             returns a STATUS_CODE enum
 *  
 *  @details Reads the the mac address of the CC3000 from the internal EEPROM.
 *  
 *  @authors Patrick Downs
 *  
 *  @sa    cc3000_setMacAddress
 *  
 */
u8 cc3000_getMacAddress(u8* mac)
{
    cc3000_init_cs(); // IMPORTANT: All public functions require this!!
    
    u8 status;
    u8 macFromEeprom[MAC_ADDR_LEN];
    
    // read MAC address
    status = nvmem_get_mac_address(macFromEeprom);
    if(status == 0) // Success
    {
        memcpy(mac, macFromEeprom, sizeof(MAC_ADDR_LEN));
        return S_SUCCESS;
    }
    
    return S_FAIL;
}

/**
 *  cc3000_setMacAddress
 *  
 *  @brief         Write Mac Address to eeprom    
 *  
 *  @param [in] newmac buffer containing mac address to be written
 *  @retval     returns a STATUS_CODE enum
 *  
 *  @details     Writes the mac address to the CC3000 eeprom. The default mac is 
 *              assigned at the factory. This function is typically only used to
 *              restore the mac aftering being erased during the patch update process.
 *  
 *  @authors Patrick Downs
 *  
 *  @sa cc3000_getMacAddress
 *  
 */
u8 cc3000_setMacAddress(u8* newmac)
{
    u8 status;
    
    cc3000_init_cs(); // IMPORTANT: All public functions require this!!
    
    // read MAC address
    status = nvmem_set_mac_address(newmac);
    if(status == 0) // Success
    {
        return S_SUCCESS;
    }
    
    return S_FAIL;
}

/**
 *  cc3000_readPatchVersion
 *  
 *  @brief                     Read Patch version
 *  
 *  @param [out] pVersion     buffer where data should be copied, atleast 2 bytes in size
 *  @retval                 returns a STATUS_CODE enum
 *  
 *  @details                 Reads the patch version from the EEPROM
 *  
 *  @authors Patrick Downs
 *  
 */
u8 cc3000_readPatchVersion(u8* pVersion)
{
    cc3000_init_cs(); // IMPORTANT: All public functions require this!!
    
    memset(pVersion, 0, sizeof(pVersion));
    
    if(nvmem_read_sp_version(pVersion) != 0)
    {
        return S_FAIL;
    }
    
    return S_SUCCESS;
}
 
/**
 *  cc3000_addProfile
 *  
 *  @brief         Adds a connection profile
 *  
 *  @param [in] result         structure containing AP information recieved from the AP scan
 *  @param [in] key         security key
 *  @param [in] keylen         security key length in bytes
 *  @param [in] priority     priority number for number 1-7
 *  
 *  @retval                 returns a STATUS_CODE enum
 *  
 *  @details Details
 *  
 *  @authors Patrick Downs
 *  
 *  @sa cc3000_deleteProfiles
 */
u8 cc3000_addProfile(CC3000_AP_RESULTS *result, u8 *key, u32 keylen, u32 priority)
{
    int status;
    
    cc3000_init_cs(); // IMPORTANT: All public functions require this!!
    
    if((priority > 7) || (priority < 1))
    {
        return S_INPUT; // Priority out of range
    }
    
    wlan_ioctl_set_connection_policy(DISABLE, DISABLE, DISABLE);
    
    switch (result->security)
    {
    case WLAN_SEC_UNSEC://None
         {
            status = wlan_add_profile(result->security,     // security type
                                      result->ssidName,     // SSID
                                      result->ssidLength,   // SSID length
                                      NULL,                 // BSSID
                                      priority,             // Priority
                                      0, 0, 0, 0, 0);
            break;
         }
        
    case WLAN_SEC_WEP://WEP
        {
            status = wlan_add_profile(result->security,     // security type
                                      result->ssidName,     // SSID
                                      result->ssidLength,   // SSID length
                                      NULL,                 // BSSID
                                      priority,             // Priority
                                      keylen,               // KEY length
                                      0,                    // KEY index
                                      0,                    
                                      key,                  // KEY
                                      0);
            break;
        }
        
    case WLAN_SEC_WPA://WPA
    case WLAN_SEC_WPA2://WPA2
        {
            status = wlan_add_profile(WLAN_SEC_WPA2,        // security type
                                      result->ssidName,     // SSID
                                      result->ssidLength,   // SSID Length
                                      NULL,                 // BSSID
                                      priority,             // Priority
                                      0x18,                 // PairwiseCipher
                                      0x1e,                 // GroupCipher
                                      2,                    // KEY management
                                      key,                  // KEY
                                      keylen);              // KEY length                
            
            break;
        }
    }
    
    if(status == -1) // -1 is fail
    {
        return S_FAIL;
    }
    
    return S_SUCCESS;
}

/**
 *  cc3000_deleteProfiles
 *  
 *  @brief     Delete connection profiles
 *  
 *  @param [in] priority     Profile Priority to be deleted (1-7). Or 255 to delete all profiles. 
 *  @retval                 returns a STATUS_CODE enum
 *  
 *  @details Deletes the connection profile from the EEPROM.
 *  
 *  @authors Patrick Downs
 */
u8 cc3000_deleteProfiles(u32 priority)
{    
    cc3000_init_cs(); // IMPORTANT: All public functions require this!!
    
    if(((priority > 7) || (priority < 1)) && (priority != 255))
    {
        return S_INPUT; // Priority out of range
    }
    
    if(wlan_ioctl_set_connection_policy(DISABLE, DISABLE, DISABLE) != 0)
    {
        return S_FAIL;
    }
    
    if(wlan_ioctl_del_profile(priority) != 0)
    {
        return S_FAIL;
    }
    
    return S_SUCCESS;
}

/**
 *  cc3000_setDhcp
 *  
 *  @brief  Configure the DHCP settings
 *
 *  @param [in] ip              IP Address for static, set to 0x00 for DHCP
 *  @param [in] subnetmask      Subnet Mask, set to 0x00 for DHCP
 *  @param [in] defaultgateway  Default Gateway, set to 0x00 for DHCP
 *  @param [in] dnsserver       DNS Server
 * 
 *  @retval                 returns a STATUS_CODE enum
 *  
 *  @details 
 *  
 *  @authors Patrick Downs
 */
u8 cc3000_setDhcp(u32 ip, u32 subnetmask, u32 defaultgateway, u32 dnsserver)
{
    u32 status;
    
    cc3000_init_cs(); // IMPORTANT: All public functions require this!!
    
    status = netapp_dhcp((unsigned long*)&ip, (unsigned long*)&subnetmask, 
                             (unsigned long*)&defaultgateway, (unsigned long*)&dnsserver);
    
    //reset the CC3000
    cc3000_restart();
    
    return status;
}



/**
 *  cc3000_resetandconnect
 *  
 *  @brief     
 * 
 *  @retval                 returns a STATUS_CODE enum
 *  
 *  @details 
 *  
 *  @authors Patrick Downs
 */
#define CC3000_RESETANDCONNECT_TIMEOUT_MS 10000
u8 cc3000_resetandconnect(bool waitforconnect)
{
    u32 timeout;
    
    cc3000_init_cs(); // IMPORTANT: All public functions require this!!
    
    //configure to connect automatically to the APs in profiles
    wlan_ioctl_set_connection_policy(DISABLE, DISABLE, ENABLE);
    
    //reset the CC3000
    cc3000_restart();
    
    // Mask out all non-required events
    wlan_set_event_mask(HCI_EVNT_WLAN_KEEPALIVE | HCI_EVNT_WLAN_UNSOL_INIT |
                        HCI_EVNT_WLAN_ASYNC_PING_REPORT);
    
    if(waitforconnect)
    {
        timeout = CC3000_RESETANDCONNECT_TIMEOUT_MS / 100; // Delay in ms loop count
        while((cc3000_isConnected() == FALSE) || (cc3000_isDhcpDone() == FALSE)) // Wait for reconnect
        {
            delays_counter(TIME_LOOP_COUNT_100MS);
            timeout--;
            if(timeout == 0)
            {
                break;
            }
        }
    }
    
    return S_SUCCESS;
}

/**
 *  cc3000_parseHttpHeader
 *  
 *  @brief         Parses the HTTP header recieved from server.
 *  
 *  @param [in] header             buffer containing the complete header data
 *  @param [out] headerlen         location where the header length will be returned
 *  @param [out] contentlength    location where HTTP body length will be returned 
 *  @retval                     returns a STATUS_CODE enum
 *  
 *  @details     Parses the HTTP header recieve from the server to get the fields that
 *              are of interest. Currently it only parses the content length field 
 *              which is used to know when we have recieved a complete file. Other
 *              fields could be easily added to the function.
 *  
 *  @authors Patrick Downs
 */
#define HTTP_FIELD_CONTENTLENGTH        "Content-Length: "
#define HTTP_RESP_200                   "200 OK"
 u8 cc3000_parseHttpHeader(u8 *header, u32* headerlen, u32* contentlength)
{
    char *pHeaderEnd;
    char *pFieldStart;
    u8 status;

    status = S_FAIL;
    
    // Find end of header empty line
    pHeaderEnd = strstr((char const*)header, "\r\n\r\n"); 
    if(pHeaderEnd)
    {
        pHeaderEnd += 4;
        *headerlen = (pHeaderEnd - (char*)header); // Save header length
        
        // Check for Response "200 OK"
        pFieldStart = strstr((char*)header, HTTP_RESP_200);
        if(pFieldStart)
        {
            // Find the Header Field Name "Content-Length"
            pFieldStart = strstr((char*)header, HTTP_FIELD_CONTENTLENGTH);
            if(pFieldStart)
            {
                pFieldStart += strlen(HTTP_FIELD_CONTENTLENGTH);
                *contentlength = atoi(pFieldStart);
                status = S_SUCCESS;
            }
            else
            {
                status = S_INPUT; // Can't find content-length in header
            }
        }
        else
        {
            status = S_FAIL; // Failed, bad response from sever
        }
    }
    else
    {
        status = S_FAIL; // No header found
    }
    
    return status;
}

/**
 *  cc3000_recv
 *  
 *  @brief Socket Recieve
 *  
 *  @param [out] buffer       pointer tp recieve data buffer
 *  @param [in] bufferindex     starting index where to copy data
 *  @param [in] socketid        socket handle to recieve from
 *
 *  @retval                     returns a STATUS_CODE enum
 *  
 *  @details Handles socket recieve
 *  
 *  @authors Patrick Downs
 */
#define CC3000_RECV_NONBLOCKING_TIMEOUT_MS  10000
u8 cc3000_recv(u8 *rxbuffer, u32 rxsize, s32 socketid, s32 *bytes)
{
    u8 status;
    u32 timeout;
    
    timeout = CC3000_RECV_NONBLOCKING_TIMEOUT_MS / SOCKET_TIMEOUT_MS;
    do
    {
        *bytes = recv(opensocketid, rxbuffer, rxsize, NULL);
        timeout--;
        if(timeout == 0)
        {
            *bytes = -1; // Force Timeout
            log_push_error_point(0x7101);
        }
    }while(*bytes == 0);
    
    if(*bytes < 0)
    {
        status = S_FAIL;    // Socket timeout error
        log_push_error_point(0x7100);
    }
    else
    {
        status = S_SUCCESS;
    }
    
    return status;
}

/**
 *  cc3000_handleHttpRequest
 *  
 *  @brief Perform HTTP POST request
 *  
 *  @param [in] host            host address to POST data to ex. www.sctflash.com
 *  @param [in] txbuffer        request data to be send
 *  @param [in] txlength        length of data to send
 *  @param [in] rxfile          filename of where to save the response data
 *  @param [in] port            Port override, NULL for HTTP default port 80, otherwise set a different port number
 *  @param [in] progress_report_func    A func ptr to report progress
 *  @retval                     returns a STATUS_CODE enum
 *  
 *  @details Handles HTTP request
 *  
 *  @authors Patrick Downs
 */
#define CC3000_DOWNLOAD_TEMP_FILENAME   "dltmp.bin"
u8 cc3000_handleHttpRequest(const u8 *host, const u8* txbuffer, u32 txlength, const u8* rxfile, u16 port,
                         PROGRESS_REPORT_FUNC_SIMPLE progress_report_func)
{
    F_FILE *file;
    u8  buffer[1468];
    u8  tempfilename[128];
    u32 bufferindex;
    s32 bytes;
    u32 ip;
    s32 status;
    s32 fbytes;
    u32 contentlength;
    u32 headerlength;
    bool rxHeaderDone;
    u8  percentage;
    u8  percentage_prev;
    
    percentage_prev = 101;
    percentage = 0;
    
    if(strlen((const char*)rxfile) < sizeof(tempfilename))
    {
        memset(tempfilename, 0, sizeof(tempfilename));
        genfs_getpathfromfilename((u8*)rxfile, tempfilename);
        strcat((char*)tempfilename, CC3000_DOWNLOAD_TEMP_FILENAME);
    }
    else
    {
        return S_INPUT;
    }
    
    file = genfs_general_openfile(tempfilename,"wb");
    if (file)
    {
        memset(buffer, 0, sizeof(buffer));
        
        cc3000_init_cs(); // IMPORTANT: All public functions require this!!
        
        status = cc3000_getHostByName(host, &ip);
        if (status == S_SUCCESS)
        {
            status = cc3000_openSocket(ip, port, CC3000_SOCKETTYPE_TCP);
            if (status == S_SUCCESS)
            {
                status = send(opensocketid, txbuffer, txlength, NULL);
                if(status != -1)
                {
                    fbytes = 0;        
                    bufferindex = 0;
                    rxHeaderDone = FALSE;
                    
                    // Recive HTTP Header & Body Data
                    while(1)
                    {
                        status = cc3000_recv((buffer+bufferindex), (sizeof(buffer)-bufferindex), opensocketid, &bytes);
                        if(status == S_SUCCESS)
                        {
                            if(rxHeaderDone == FALSE)
                            {
                                // Handle HTTP Header
                                bufferindex += bytes;                                
                                if(strstr((char const*)buffer, "\r\n\r\n")) // Watch for end of header
                                {
                                    status = cc3000_parseHttpHeader(buffer, &headerlength, &contentlength);
                                    if(status == S_SUCCESS)
                                    {
                                        rxHeaderDone = TRUE;
                                        fbytes += fwrite((char*)(buffer+headerlength), 1, 
                                                         (bufferindex-headerlength), file);
                                        bufferindex = 0;
                                    }
                                    else
                                    {
                                        log_push_error_point(0x7102); // Failed to parse header
                                        status = S_FAIL;
                                        break;
                                    }
                                }
                                if(bufferindex >= sizeof(buffer))
                                {
                                    bufferindex = 0;
                                }
                            }
                            else
                            {
                                fbytes += fwrite((char*)buffer, 1, bytes, file);
                                percentage = (fbytes*100)/contentlength;
                                if (percentage != percentage_prev)
                                {
                                    percentage_prev = percentage;
                                    if (progress_report_func != NULL)
                                    {
                                        progress_report_func(percentage);
                                    }
                                }
                            }
                            if(fbytes >= contentlength)
                            {
                                status = S_SUCCESS; // When all data rx'd we're done
                                break;
                            }
                        }
                        else
                        {
                            break;
                        }
                    }
                }
                else
                {
                    log_push_error_point(0x7103); // Failed to send data
                    status = S_ERROR; // Failed to send data
                }
                
                // Close connections out, cleanup
                if(opensocketid >= 0)
                {
                    cc3000_closeSocket(opensocketid);
                }
            }
            else
            {
                log_push_error_point(0x7104); 
                status = S_ERROR; // Failed to open socket
            }
        }
        else
        {
            log_push_error_point(0x7105);
            status = S_ERROR; // Failed to resolve host IP
        }
        
        genfs_closefile(file);
        if(status == S_SUCCESS)
        {
            // Rename download temp file to rxfile name
            genfs_getfilenamefrompath((u8*)rxfile, buffer);
            status = genfs_renamefile(tempfilename, buffer);
            if(status != S_SUCCESS)
            {
                log_push_error_point(0x710B); // Rename File Fail
            }
        }
    }
    else
    {
        log_push_error_point(0x7106); // Open File Fail
        status = S_OPENFILE;
    }
    
    return status;
}

/**
 *  cc3000_httpPostRequest
 *  
 *  @brief Perform HTTP POST request
 *  
 *  @param [in] host            host address to POST data to ex. www.sctflash.com
 *  @param [in] resource        path of resource to POST to ex. "/post.php"
 *  @param [in] txfile          filename containing data for body of POST request
 *  @param [in] rxfile          filename of where to save the response data
 *  @param [in] port            Port override, NULL for HTTP default port 80, otherwise set a different port number
 *  @param [in] progress_report_func    A func ptr to report progress
 *  @retval                     returns a STATUS_CODE enum
 *  
 *  @details Performs a HTTP POST request
 *  
 *  @authors Patrick Downs, Quyen Leba
 */
u8 cc3000_httpPostRequest(const u8 *host, const u8 *resource, const u8* txfile, const u8* rxfile, u16 port,
                         PROGRESS_REPORT_FUNC_SIMPLE progress_report_func)
{
    F_FILE *file;
    u32 fbytes;
    u8  buffer[1468];
    s32 status;
    u32 contentlength;
    u8 body[600];
    
    status = S_FAIL;
    
    file = genfs_general_openfile(txfile,"rb");
    if (file)
    {
        memset(body, 0, sizeof(body));
        fbytes = fread((char*)body, 1,sizeof(body), file);
        if(fbytes > 0)
        {
            //"POST /test/interface.php HTTP/1.1\r\nHost: hostname.com\r\nContent-Length: 23\r\nContent-Type: application/x-www-form-urlencoded;\r\n\r\n&function=xxxxxxxxxxxxx";
            contentlength = strlen((char const*)body);
            u8 contenttype[] = "application/json";
            
            sprintf((char*)buffer, "POST %s HTTP/1.1\r\nHost: %s\r\nContent-Length: %d\r\nContent-Type: %s\r\n\r\n%s", 
                    resource, host, contentlength, contenttype, body);  
            status = S_SUCCESS;
        }
        else
        {
            status = S_READFILE;
        }
        genfs_closefile(file);        
    }
    else
    {
        status = S_OPENFILE;
    }
    
    if(status == S_SUCCESS)
    {
        status = cc3000_handleHttpRequest(host, buffer, strlen((char const*)buffer), rxfile, port, 
                                          progress_report_func);
    }
    
    return status;
}


/**
 *  cc3000_httpGetRequest
 *  
 *  @brief Perform HTTP get request
 *  
 *  @param [in] host            host address to get resource from ex. www.sctflash.com
 *  @param [in] resource        path of resource to get ex. "/PDF/file.pdf"
 *  @param [in] rxfile          filename of where to save the resource data
 *  @param [in] port            Port override, NULL for HTTP default port 80, otherwise set a different port number
 *  @param [in] progress_report_func    A func ptr to report progress
 *  @retval                     returns a STATUS_CODE enum
 *  
 *  @details Performs a HTTP get request for a file from a website
 *  
 *  @authors Patrick Downs, Quyen Leba
 */
u8 cc3000_httpGetRequest(const u8 *host, const u8 *resource, const u8* rxfile, u16 port,
                         PROGRESS_REPORT_FUNC_SIMPLE progress_report_func)
{
    u8  buffer[1468];
    s32 status;
    
    memset(buffer, 0, sizeof(buffer));
    sprintf((char*)buffer, (char const*)"GET %s HTTP/1.1\r\nHost: %s\r\nConnection: close\r\n\r\n", resource, host);

    status = cc3000_handleHttpRequest(host, buffer, strlen((char const*)buffer), rxfile, port, 
                                      progress_report_func);
    return status;
}


/**
 *  cc3000_getGeoLocation
 *  
 *  @brief Get geolocation from freegeoip.net
 *  
 *  @param [out] retLocData     pointer to memory where location data will be returned
 *  @param [in] maxsize         max size of the return buffer
 *  @retval                     returns a STATUS_CODE enum
 *  
 *  @details Details
 *  
 *  @authors Patrick Downs
 */
u8 cc3000_getGeoLocation(u8* retLocData, u32 maxsize)
{
    s32 status;
    u32 ip;
    u8 buffer[512];
    s32 bytes;
    
    cc3000_init_cs(); // IMPORTANT: All public functions require this!!
    
    status = cc3000_getHostByName("freegeoip.net", &ip);
    if(status == S_SUCCESS)
    {
        status = cc3000_openSocket(ip, 80, CC3000_SOCKETTYPE_TCP);
        if(status != S_SUCCESS)
        {
            goto CC3000_GEOLOC_DONE;
        }
        
        memset(buffer, 0, sizeof(buffer));
        
        sprintf((char*)buffer, (char const*)"GET /json/ HTTP/1.0\r\nConnection: close\r\n\r\n");
               
        status = send(opensocketid, buffer, strlen((char*)buffer), NULL);
        if(status == -1)
        {
            status = S_FAIL;
            goto CC3000_GEOLOC_DONE;
        }
        
        memset(buffer, 0, sizeof(buffer));
        
        bytes = recv(opensocketid, buffer, sizeof(buffer), NULL);
        if(bytes < 0)
        {
            status = S_FAIL;
            goto CC3000_GEOLOC_DONE;
        }
        
        if(bytes < maxsize)
        {
            memcpy(retLocData, buffer, bytes);
        }
        else
        {
            memcpy(retLocData, buffer, maxsize);
        }
        
        debug_out(buffer);
    }
    
CC3000_GEOLOC_DONE:
    // Close connections out, cleanup
    if(opensocketid >= 0)
    {
        cc3000_closeSocket(opensocketid);
    }
    
    return status;
}

/**
 *  cc3000_getNtpTime
 *  
 *  @brief Get unix time from NTP server pool
 *  
 *  @param [out] unixtime   return parameter for unix time
 *  @retval                 returns a STATUS_CODE enum
 *  
 *  @details Gets the GMT timestamp in seconds since 01/01/1970 from the one of
 *            the US NTP server pool sites. 
 *  
 *  @authors Patrick Downs
 */
u8 cc3000_getNtpTime(u32 *unixtime)
{
    cc3000_init_cs(); // IMPORTANT: All public functions require this!!
    
    static const char timeReqA[] = { 227,  0,  6, 236 };
    static const char timeReqB[] = {  49, 78, 49,  52 };
    s32 status;
    u32 ip;
    u32 servertime;
    u8 buffer[48];
    
    *unixtime = 0;
    
    status = cc3000_getHostByName("0.us.pool.ntp.org", &ip);
    if(status == S_SUCCESS)
    {
        status = cc3000_openSocket(ip, 123, CC3000_SOCKETTYPE_UDP);
        if(status != S_SUCCESS)
        {
            goto CC3000_GETTIME_DONE;
        }
        
        memset(buffer, 0, sizeof(buffer));
        memcpy(buffer, timeReqA, sizeof(timeReqA));
        memcpy(buffer+4, timeReqB, sizeof(timeReqB));
               
        status = send(opensocketid, buffer, sizeof(buffer), NULL);
        if(status == -1)
        {
            status = S_FAIL;
            goto CC3000_GETTIME_DONE;
        }
        
        memset(buffer, 0, sizeof(buffer));
        
        status = recv(opensocketid, buffer, sizeof(buffer), NULL);
        if(status == -1)
        {
            status = S_FAIL;
            goto CC3000_GETTIME_DONE;
        }
        
        servertime = (((u32)buffer[40] << 24) |
             ((u32)buffer[41] << 16) |
                 ((u32)buffer[42] <<  8) |
                     (u32)buffer[43]) - 2208988800UL;
        
        *unixtime = servertime;
        
        status = S_SUCCESS;
    }
    
CC3000_GETTIME_DONE:    
    // Close connections out, cleanup
    if(opensocketid >= 0)
    {
        cc3000_closeSocket(opensocketid);
    }
    
    return status;
}

/**
 *  cc3000_openSocket
 *  
 *  @brief Opens a socket with host
 *  
 *  @param [in] destIP Parameter_Description
 *  @param [in] destPort Parameter_Description
 *  @param [in] type Parameter_Description
 *  @retval Return_Description
 *  
 *  @details Details
 *  
 *  @authors Patrick Downs
 */
u8 cc3000_openSocket(u32 destIP, u16 destPort, CC3000_SOCKETTYPE type)
{
    sockaddr    socketAddress;
    u32 timeout = SOCKET_TIMEOUT_MS;
    
    // Create the socket(s)
    // socket   = SOCK_STREAM, SOCK_DGRAM, or SOCK_RAW
    // protocol = IPPROTO_TCP, IPPROTO_UDP or IPPROTO_RAW
    
    if(type == CC3000_SOCKETTYPE_UDP)
    {
        opensocketid = socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP);
    }
    else if(type == CC3000_SOCKETTYPE_TCP)
    {
        opensocketid = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);
    }    
    
    if (opensocketid == -1)
    {
        log_push_error_point(0x7107);
        return S_FAIL;
    }
    
    // Set socket timeout for non-blocking
    setsockopt(opensocketid, SOL_SOCKET, SOCKOPT_RECV_TIMEOUT, &timeout, sizeof(&timeout));
    
    // Try to open the socket
    memset(&socketAddress, 0x00, sizeof(socketAddress));
    socketAddress.sa_family = AF_INET;

    // Set the Port Number
    socketAddress.sa_data[0] = (destPort & 0xFF00) >> 8;
    socketAddress.sa_data[1] = (destPort & 0x00FF);
    socketAddress.sa_data[2] = destIP >> 24;
    socketAddress.sa_data[3] = destIP >> 16;
    socketAddress.sa_data[4] = destIP >> 8;
    socketAddress.sa_data[5] = destIP;
        
    if (connect(opensocketid, &socketAddress, sizeof(socketAddress)) == -1)
    {
        cc3000_closeSocket(opensocketid);
        log_push_error_point(0x7108);
        return S_FAIL;
    }
    
    return S_SUCCESS;
}

/**
 *  cc3000_closeSocket
 *  
 *  @brief Close a socket with host
 *  
 *  @retval Return_Description
 *  
 *  @details Details
 *  
 *  @authors Patrick Downs
 */
u8 cc3000_closeSocket()
{
    closesocket(opensocketid);
    opensocketid = -1; // Mark as invalid
    
    return S_SUCCESS;
}

/**
 *  cc3000_getHostByName
 *  
 *  @brief Brief
 *  
 *  @param [in] hostname Parameter_Description
 *  @param [in] ip Parameter_Description
 *  @retval Return_Description
 *  
 *  @details Details
 *  
 *  @authors Patrick Downs
 */
u8 cc3000_getHostByName(const u8 *hostname, u32 *ip)
{
    int status;
    int retry = 3;
    
    // Check if hostname is auctually an IP address
    status = cc3000_isValidIp4(hostname);
    if(status == S_SUCCESS)
    {
        // It's an IP address
        // Convert from ascii IP to integer IP
        status = cc3000_ipToInt(hostname, ip);
    }
    else
    {
        if(strlen((char const*)hostname) > CC3000_HOSTNAME_MAXLENGTH)
        {
            status = S_INPUT;
        }
        else if(strcmp((char const*)hostname, (char const*)cc3000_info.hostname) == 0) // Same hostname, already resolved IP
        {
            *ip = cc3000_info.hostip;
            status = S_SUCCESS;
        }
        else // New Hostname to resolve
        {
            while(retry)
            {
                status = gethostbyname((char*)hostname, strlen((char const*)hostname), (unsigned long*)ip);
                if(status > 0)
                {
                    break;
                }
                else
                {
                    retry--;
                }
                delays(500,'m');
            }
            
            if(retry == 0)
            {
                status = S_TIMEOUT;
            }
            else
            {
                strcpy((char*)cc3000_info.hostname, (char const*)hostname);
                cc3000_info.hostip = *ip;
                status = S_SUCCESS;
            }
        }
    }

    return status;
}


/**
 *  cc3000_isValidIp4
 *  
 *  @brief Check if string contains valid IP address
 *  
 *  @param [in] hostname Parameter_Description
 *
 *  @retval S_SUCCESS - Valid, S_FAIL - Not valid
 *  
 *  @details Details
 *  
 *  @authors Patrick Downs
 */
u8 cc3000_isValidIp4 (const u8 *str) 
{
    int segs = 0;   /* Segment count. */
    int chcnt = 0;  /* Character count within segment. */
    int accum = 0;  /* Accumulator for segment. */

    /* Catch NULL pointer. */
    if (str == NULL)
        return S_INPUT;

    /* Process every character in string. */
    
    while (*str != '\0') {
        /* Segment changeover. */

        if (*str == '.') {
            /* Must have some digits in segment. */

            if (chcnt == 0)
                return S_FAIL;

            /* Limit number of segments. */

            if (++segs == 4)
                return S_FAIL;

            /* Reset segment values and restart loop. */

            chcnt = accum = 0;
            str++;
            continue;
        }
 
        /* Check numeric. */

        if ((*str < '0') || (*str > '9'))
            return S_FAIL;

        /* Accumulate and check segment. */

        if ((accum = accum * 10 + *str - '0') > 255)
            return S_FAIL;

        /* Advance other segment specific stuff and continue loop. */

        chcnt++;
        str++;
    }

    /* Check enough segments and enough characters in last segment. */

    if (segs != 3)
        return S_FAIL;

    if (chcnt == 0)
        return S_FAIL;

    /* Address okay. */

    return S_SUCCESS;
}

/**
 *  cc3000_ipToInt
 *  
 *  @brief Converty ascii IP to integer IP
 *  
 *  @param [in]  ip     Parameter_Description
 *  @param [out] output Parameter_Description
 *
 *  @retval S_SUCCESS - Valid, S_FAIL - Not valid
 *  
 *  @details Details
 *  
 *  @authors Patrick Downs
 */
u8 cc3000_ipToInt(const u8 *ip, u32* output)
{
    u32 ip_int = 0;    
    int i;
    const u8 *start;

    start = ip;
    for (i = 0; i < 4; i++) {
        // The digit being processed
        char c;
        // The value of this byte
        int n = 0;
        while (1) {
            c = * start;
            start++;
            if (c >= '0' && c <= '9') {
                n *= 10;
                n += c - '0';
            }
            // We insist on stopping at "." if we are still parsing
            // the first, second, or third numbers. If we have reached
            // the end of the numbers, we will allow any character
            else if ((i < 3 && c == '.') || i == 3) {
                break;
            }
            else {
                return S_FAIL;
            }
        }
        if (n >= 256) {
            return S_FAIL;
        }
        ip_int *= 256;
        ip_int += n;
        //ip_int |= (n << i*8); // Reversed byte order
    }
    
    *output = ip_int;
    
    return S_SUCCESS;
}


/**
 *  cc3000_getIpConfig
 *  
 *  @brief Brief
 *  
 *  @param [in] ipconfig Parameter_Description
 *  @retval Return_Description
 *  
 *  @details Details
 *  
 *  @authors Patrick Downs
 */
void cc3000_getIpConfig(CC3000_IPCONFIG *ipconfig)
{
    tNetappIpconfigRetArgs inf;
    
    cc3000_init_cs(); // IMPORTANT: All public functions require this!!
    
    netapp_ipconfig(&inf);
    
    // IP Address
    sprintf((char*)ipconfig->ipaddress,"%d.%d.%d.%d", inf.aucIP[3], inf.aucIP[2],inf.aucIP[1],
            inf.aucIP[0]);
    
    // Subnet Mask
    sprintf((char*)ipconfig->subnetmask, "%d.%d.%d.%d", inf.aucSubnetMask[3], inf.aucSubnetMask[2],
            inf.aucSubnetMask[1],inf.aucSubnetMask[0]);

    // Gateway 
    sprintf((char*)ipconfig->gateway, " %d.%d.%d.%d", inf.aucDefaultGateway[3], inf.aucDefaultGateway[2],
            inf.aucDefaultGateway[1],inf.aucDefaultGateway[0]);
    
    // DHCP Server
    sprintf((char*)ipconfig->dhcpserver, "%d.%d.%d.%d", inf.aucDHCPServer[3], inf.aucDHCPServer[2],
            inf.aucDHCPServer[1],inf.aucDHCPServer[0]);
    
    // DNS Server
    sprintf((char*)ipconfig->dnsserver, "%d.%d.%d.%d", inf.aucDNSServer[3], inf.aucDNSServer[2],
            inf.aucDNSServer[1],inf.aucDNSServer[0]);
    
    // Mac Address
    sprintf((char*)ipconfig->macaddress, "%01X:%01X:%01X:%01X:%01X:%01X", inf.uaMacAddr[5], inf.uaMacAddr[4], 
            inf.uaMacAddr[3], inf.uaMacAddr[2], inf.uaMacAddr[1],inf.uaMacAddr[0]);
    
    // SSID
    memset(ipconfig->ssid, 0, sizeof(ipconfig->ssid));
    memcpy(ipconfig->ssid, inf.uaSSID, 32);
}

/**
 *  cc3000_scanSSID
 *  
 *  @brief  Handles scanning for Access Point functions
 *  
 *  @param [out] results    Return buffer to contain AP CC3000_AP_RESULTS data
 *  @param [in] opcode      Opcode for task 
 *  @retval                 returns a STATUS_CODE enum
 *  
 *  @details Handles the functions related to scanning for Access Point SSIDs.
 *          1. CC3000_SCAN_OPCODE_START - will start scan and returns the first 
 *              CC3000_AP_RESULTS data found and total count of SSIDs found including the
 *              one retrieved at this step. Scanning can take up to 10 seconds, 
 *              the default of CC3000.
 *          2. CC3000_SCAN_OPCODE_GETSSID - retrieves the next CC3000_AP_RESULTS data
 *              found, continue retrieving until you reach the count provided by first
 *              CC3000_AP_RESULTS data. Also the count in each retrieved data will
 *              decrement indicating the count left to be retrieved.
 *          3. CC3000_SCAN_OPCODE_STOP - Causes module to exit scanning mode.
 *  
 *  @authors Patrick Downs
 */
u8 cc3000_scanSSID(CC3000_AP_RESULTS *results, CC3000_SCAN_OPCODE opcode)
{
    u8 status;
    
    cc3000_init_cs(); // IMPORTANT: All public functions require this!!
    
    switch(opcode)
    {
    case CC3000_SCAN_OPCODE_START:
        status = cc3000_startScanSSID(results);
        break;
    case CC3000_SCAN_OPCODE_STOP:
        status = cc3000_stopScan();
        break;
    case CC3000_SCAN_OPCODE_GETSSID:
        status = cc3000_getNextSSID(results);
        break;
    default:
        status = S_INPUT;
        break;
    }
    
    return status;
}

/**
 *  cc3000_startScanSSID
 *  
 *  @brief Starts the scan for access points. 
 *  
 *  @param [out] results    Location to return first CC3000_AP_RESULTS data found
 *  @retval                 returns a STATUS_CODE enum
 *  
 *  @details Start scan and returns the first CC3000_AP_RESULTS data found 
 *              and total count of SSIDs found including the one retrieved at 
 *              this step. Scanning can take up to 10 seconds, the default of CC3000.
 *  
 *  @authors Patrick Downs
 */
u8 cc3000_startScanSSID(CC3000_AP_RESULTS *results)
{
    const unsigned long intervalTime = 2000;
    u32 time;
    
    cc3000_init_cs(); // IMPORTANT: All public functions require this!!
    
    // 'time' can be set as 1 for default scan time of 10mins, 
    // otherwise value is in miliseconds
    time = 4000; // Scan for 4 seconds
    if(wlan_ioctl_set_scan_params(time, 100, 100, 5, 0x7FF, -80, 0, 205, 
                                  (unsigned long * ) &intervalTime) != 0)
    {
        // FAIL
        return S_FAIL;
    }
    
    delays(4500,'m'); // Give it time to scan
    
    // Grab first result and return
    if(wlan_ioctl_get_scan_results(0, (u8*)results) != 0)
    {
        // FAIL
        return S_FAIL;
    }
    
    return S_SUCCESS;
}
    
/**
 *  cc3000_getNextSSID
 *  
 *  @brief Retrieves the next CC3000_AP_RESULTS data from SSID scan.
 *  
 *  @param [out] results    Location to return next CC3000_AP_RESULTS data found
 *  @retval                 returns a STATUS_CODE enum
 *  
 *  @details    retrieves the next CC3000_AP_RESULTS data found, continue 
 *              retrieving until you reach the count provided by first
 *              CC3000_AP_RESULTS data. Also the count in each retrieved data will
 *              decrement indicating the count left to be retrieved.
 *  
 *  @authors Patrick Downs
 */
u8 cc3000_getNextSSID(CC3000_AP_RESULTS *results)
{
    cc3000_init_cs(); // IMPORTANT: All public functions require this!!
    
    if(wlan_ioctl_get_scan_results(0, (u8*)results) != 0)
    {
        // Fail
        return S_FAIL;
    }
    
    return S_SUCCESS;
}   

/**
 *  cc3000_stopScan
 *  
 *  @brief Stops to the scan for SSIDs
 *  
 *  @retval     returns a STATUS_CODE enum
 *  
 *  @details Stops to the scan for SSIDs
 *  
 *  @authors Patrick Downs
 */
u8 cc3000_stopScan(void)
{
    const unsigned long intervalTime = 2000;
    u32 time;
    
    cc3000_init_cs(); // IMPORTANT: All public functions require this!!
    
    // Stop scanning
    time = 0; // Time of 0 stops scanning
    
    if(wlan_ioctl_set_scan_params(time, 100, 100, 5, 0x7FF, -80, 0, 205, 
                                  (unsigned long * ) &intervalTime) != 0)
    {
        // FAIL
        return S_FAIL;
    }
    
    return S_SUCCESS;
}

/**
 *  cc3000_disconnect
 *  
 *  @brief Brief
 *  
 *  @retval Return_Description
 *  
 *  @details Details
 *  
 *  @authors Patrick Downs
 */
u8 cc3000_disconnect(void)
{
    cc3000_init_cs(); // IMPORTANT: All public functions require this!!

    cc3000_info.ulCC3000Connected = 0;
    if(wlan_disconnect() != 0)
    {
        // CC3000 already disconnected...
        return S_FAIL;
    }
    
    return S_SUCCESS;
}

/**
 *  cc3000_connectToAP
 *  
 *  @brief Brief
 *  
 *  @param [in] ssid Parameter_Description
 *  @param [in] bssid Parameter_Description
 *  @param [in] key Parameter_Description
 *  @param [in] keylen Parameter_Description
 *  @param [in] secmode Parameter_Description
 *  @retval Return_Description
 *  
 *  @details Details
 *  
 *  @authors Patrick Downs, Quyen Leba
 */
u8 cc3000_connectToAP(u8* ssid, u8* bssid, u8* key, u8 keylen, s32 secmode)
{
    volatile int status;

    cc3000_init_cs(); // IMPORTANT: All public functions require this!!
    
    // Validate inputs
    if((secmode < 0) || (secmode > 3))
    {
        // Fail, must be 0-3
        return S_INPUT;
    }
    
    if(strlen((char const*) ssid) > SSID_MAX_LEN)
    {
        //Fail, SSID length too long
        return S_INPUT;
    }

    if (strlen((char*)key) != keylen || keylen > KEY_MAX_LEN)
    {
        //Fail, key length too long
        return S_INPUT;
    }

//    status = wlan_ioctl_set_connection_policy(0, 0, 0);
//    if(status != 0)
//    {
//        // Fail
//        return S_FAIL;
//    }
    
//    status = wlan_ioctl_del_profile(255);
//    if(status != 0)
//    {
//        // Fail
//        return S_FAIL;        
//    }
    
//    int TO = 10;
//    while(cc3000_info.ulCC3000Connected == 1 && TO)
//    {
//        delays(100,'m'); // Wait for disconnected
//        TO--;
//    }
    
    status = wlan_connect(secmode, (char *)ssid, strlen((char const*)ssid), bssid, 
                    (unsigned char *)key, keylen);
//    if(status != 0)
//    {
//        // Fail
//        return S_FAIL;
//    }
    
//    while(cc3000_info.ulCC3000Connected == 0)
//    {
//        delays(100,'m'); // Wait for disconnected
//        TO--;   
//    }
    
    // Now neeed to wait for 'HCI_EVNT_WLAN_UNSOL_CONNECT' in CC3000_UsynchCallback
    return S_SUCCESS;
}
