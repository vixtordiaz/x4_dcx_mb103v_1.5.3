/**
  **************** (C) COPYRIGHT 2011 SCT Performance, LLC *********************
  * File Name          : wifi_hal.h
  * Device / System    : SCT products using the "CommonCode" platform
  * Author             : Quyen Leba
  *
  * Version            : 1 
  * Date               : 05/02/2011
  * Description        : 
  *                    : 
  *
  *
  * History            : 05/02/2011 M. Davis
  *                    :   Add SCT Copyright header,
  *
  ******************************************************************************
  */

#ifndef __WIFI_HAL_H
#define __WIFI_HAL_H

#include <arch/gentype.h>
#include <board/CC3000/cc3000.h>
#include <common/function_ptr_defs.h>

typedef enum
{
    WLAN_SEC_UNSEC  = 0x00,
    WLAN_SEC_WEP    = 0x01,
    WLAN_SEC_WPA    = 0x02,
    WLAN_SEC_WPA2   = 0x03,
}WIFI_HAL_SECTYPE;

typedef struct
{
    u32 count;  // SSIDs found
    u32 status; // Scan status

    // AP found
    u8  ssidName[SSID_MAX_LEN+1];    // Null terminated SSID max length 32
    u8  rssi;
    u8  passcode[KEY_MAX_LEN+1];     // Null terminated passcode, max length 32
    u8  passcodeLen;
    u8  security;
    u8  priority;                    // 1-7, 1 = first to connect, 7 = last to connect
    u8  bssid[6];
}WIFI_HAL_AP_INFO;

// IP V4
typedef struct
{
    u8 ipaddress[16];
    u8 subnetmask[16];
    u8 gateway[16];
    u8 dhcpserver[16];
    u8 dnsserver[16];
    u8 macaddress[18];
    u8 ssid[SSID_MAX_LEN+1];
}WIFI_HAL_IPCONFIG;

typedef enum
{
    WIFI_STATUS_POWER_OFF                = 0x00,
    WIFI_STATUS_NOT_CONNECTED            = 0x01,
    WIFI_STATUS_CONNECTED_WAITINGDHCP    = 0x02,
    WIFI_STATUS_CONNECTED_READY          = 0x03,
}WIFI_STATUS;

typedef struct
{
    WIFI_STATUS status;
    WIFI_HAL_AP_INFO connectedap;
    WIFI_HAL_IPCONFIG ipconfig;
}WIFI_STATUS_INFO;

typedef enum
{
    WIFI_HAL_SCAN_OPCODE_START          = 0x00,
    WIFI_HAL_SCAN_OPCODE_STOP           = 0x01,
    WIFI_HAL_SCAN_OPCODE_GETSSID        = 0x02,
}WIFI_HAL_SCAN_OPCODE;

void wifi_hal_init_on_powerup(void);
u8 wifi_hal_startup(void);
u8 wifi_hal_getmacaddress(u8 *macaddress);
u8 wifi_hal_setmdnsadvertiser(u8 *devicename);
u8 wifi_hal_getwifiversion_string(u8 *version);

u8 wifi_hal_scan_start(WIFI_HAL_AP_INFO *ap_result);
u8 wifi_hal_scan_next(WIFI_HAL_AP_INFO *ap_result);
u8 wifi_hal_scan_stop();

u8 wifi_hal_profile_add(WIFI_HAL_AP_INFO *ap_info);
u8 wifi_hal_profile_delete(u8 priority);

u8 wifi_hal_resetandconnect();
u8 wifi_hal_connect(WIFI_HAL_AP_INFO *ap_info);
u8 wifi_hal_disconnect();

void wifi_hal_status(WIFI_STATUS_INFO *retwifiinfo);

u8 wifi_hal_download_file(const u8 *hostaddress, const u8 *srcfilepath,
                          const u8 *dstfilepath, PROGRESS_REPORT_FUNC_SIMPLE progress_report_func);
u8 wifi_hal_post_request(const u8 *hostaddress, const u8* postbodyfilepath, const u8 *srcfilepath,
                         const u8 *responsefilepath, PROGRESS_REPORT_FUNC_SIMPLE progress_report_func);
u8 wifi_hal_handle_http_request(const u8 *hostaddress, const u8* txbuffer, u32 txlength,
                         const u8 *rxfile, PROGRESS_REPORT_FUNC_SIMPLE progress_report_func);

u8 wifi_hal_parse_url(const u8 *url, u8 *host, u8 *resource);

u8 wifi_hal_set_dhcp(u32 ip, u32 subnetmask, u32 defaultgateway, u32 dnsserver);

u8 wifi_hal_getNtpTime(u32* unixtime);

#endif  //__WIFI_HAL_H