/**
  **************** (C) COPYRIGHT 2011 SCT Performance, LLC *********************
  * File Name          : wifi_hal.h
  * Device / System    : SCT products using the "CommonCode" platform
  * Author             : Quyen Leba
  *
  * Version            : 1 
  * Date               : 05/02/2011
  * Description        : 
  *                    : 
  *
  *
  * History            : 05/02/2011 M. Davis
  *                    :   Add SCT Copyright header,
  *
  ******************************************************************************
  */

#include <string.h>
#include <stdio.h>
#include <board/genplatform.h>
#include <board/bootloader.h>
#include <board/rtc.h>

#if USE_CC3000_WIFI
#include <board/CC3000/cc3000.h>
#include <board/CC3000/hostdriver/cc3000_common.h>
#endif
#include <common/statuscode.h>
#include <common/itoa.h>
#include <common/statuscode.h>
#include <common/debug/debug_output.h>

extern u8 cc3000_setMdnsAdvertiser(u8 *device_name);

WIFI_STATUS_INFO wifiinfo;

void wifi_hal_ap_info_struct_translator(WIFI_HAL_AP_INFO *ap_info, CC3000_AP_RESULTS *ap_result);
void wifi_hal_ap_result_struct_translator(CC3000_AP_RESULTS *ap_result, WIFI_HAL_AP_INFO *ap_info);
void wifi_hal_ipconfig_struct_translator(WIFI_HAL_IPCONFIG *dest, CC3000_IPCONFIG source);

//------------------------------------------------------------------------------
// Translate WIFI_HAL_AP_INFO to CC3000_AP_RESULTS
// Input:   WIFI_HAL_AP_INFO *ap_result
// Output:  CC3000_AP_RESULTS *ap_result
// Engineer: Quyen Leba
//------------------------------------------------------------------------------
void wifi_hal_ap_info_struct_translator(WIFI_HAL_AP_INFO *ap_info, CC3000_AP_RESULTS *ap_result)
{
    ap_result->count = ap_info->count;
    ap_result->status = ap_info->status;

    ap_result->isValid = 1;
    ap_result->rssi = ap_info->rssi;
    ap_result->security = ap_info->security;
    ap_result->ssidLength = strlen((const char*)ap_info->ssidName);
    ap_result->timestamp = 0x00;

    memcpy((char*)ap_result->ssidName,
           (char*)ap_info->ssidName,
           sizeof(ap_result->ssidName));
    memcpy((char*)ap_result->bssid,
           (char*)ap_info->bssid,
           sizeof(ap_result->bssid));
}

//------------------------------------------------------------------------------
// Translate WIFI_HAL_AP_INFO to CC3000_AP_RESULTS
// Input:   WIFI_HAL_AP_INFO *ap_result
// Output:  CC3000_AP_RESULTS *ap_result
// Engineer: Quyen Leba
//------------------------------------------------------------------------------
void wifi_hal_ipconfig_struct_translator(WIFI_HAL_IPCONFIG *dest, CC3000_IPCONFIG source)
{
    memcpy(dest->ipaddress,  source.ipaddress,  sizeof(dest->ipaddress));
    memcpy(dest->subnetmask, source.subnetmask, sizeof(dest->subnetmask));
    memcpy(dest->gateway,    source.gateway,    sizeof(dest->gateway));
    memcpy(dest->dhcpserver, source.dhcpserver, sizeof(dest->dhcpserver));
    memcpy(dest->macaddress, source.macaddress, sizeof(dest->macaddress));
    memcpy(dest->ssid,       source.ssid,       sizeof(dest->ssid));
}

//------------------------------------------------------------------------------
// Translate CC3000_AP_RESULTS to WIFI_HAL_AP_INFO
// Input:   CC3000_AP_RESULTS *ap_result
// Output:   WIFI_HAL_AP_INFO *ap_result
// Engineer: Quyen Leba
//------------------------------------------------------------------------------
void wifi_hal_ap_result_struct_translator(CC3000_AP_RESULTS *ap_result, WIFI_HAL_AP_INFO *ap_info)
{
    memset((char*)ap_info,0,sizeof(WIFI_HAL_AP_INFO));

    ap_info->count = ap_result->count;
    ap_info->status = ap_result->status;

    ap_info->rssi = ap_result->rssi;
    ap_info->security = ap_result->security;
    ap_info->priority = 0;
    
    memcpy((char*)ap_info->ssidName,
           (char*)ap_result->ssidName,
           sizeof(ap_info->ssidName));
    memcpy((char*)ap_info->bssid,
           (char*)ap_result->bssid,
           sizeof(ap_info->bssid));
}

/**
 *  wifi_hal_init_on_powerup
 *  
 *  @brief Initialize the Wifi hardware on board power up
 *
 *  @retval     none
 *  
 *  @details Initialize hardware interfaces, data structures, and CC3000 module
 *  
 *  @authors Patrick Downs
 */
void wifi_hal_init_on_powerup(void)
{
    u8 status;
    PROPERTIES_AB_INFO abprops;
    u32 proplen;
    
    cc3000_init(0);
    
    bootloader_wifipatch_checkforupdate();
        
    // Check appboard rev
    status = properties_ab_getinfo((u8*)&abprops, &proplen);    
    if(abprops.board_rev == 0 || status != S_SUCCESS)
    {
        // If revision 0, we cannot start CC3000 on power up.
        // CC3000 must be init when needed and then deinit when tasks complete
        // so it does not conflict with VB over SPI.
        cc3000_deinit();
        return;
    }
}

/**
 *  wifi_hal_set_dhcp
 *  
 *  @brief Configure the DHCP settings
 *  
 *  @param [in] ap_info
 *  @param [out] ap_info
 *
 *  @retval u8  status
 *  
 *  @details Details
 *  
 *  @authors Patrick Downs
 */
u8 wifi_hal_set_dhcp(u32 ip, u32 subnetmask, u32 defaultgateway, u32 dnsserver)
{
    u8  status;
    
    status = cc3000_setDhcp(ip, subnetmask, defaultgateway, dnsserver);

    return status;
}


/**
 *  wifi_hal_scan_start
 *  
 *  @brief Start wifi scan for access points
 *  
 *  @param [out] ap_info
 *
 *  @retval u8  status
 *  
 *  @details Details
 *  
 *  @authors Patrick Downs, Quyen Leba
 */
u8 wifi_hal_scan_start(WIFI_HAL_AP_INFO *ap_info)
{
    CC3000_AP_RESULTS __result;
    u8  status;

    status = cc3000_startScanSSID(&__result);
    if (status == S_SUCCESS)
    {
        wifi_hal_ap_result_struct_translator(&__result,ap_info);
    }

    return status;
}

/**
 *  wifi_hal_scan_next
 *  
 *  @brief Continue wifi scan for access points
 *  
 *  @param [out] ap_info
 *
 *  @retval u8  status
 *  
 *  @details Details
 *  
 *  @authors Patrick Downs, Quyen Leba
 */
u8 wifi_hal_scan_next(WIFI_HAL_AP_INFO *ap_info)
{
    CC3000_AP_RESULTS __result;
    u8  status;

    status = cc3000_getNextSSID(&__result);
    if (status == S_SUCCESS)
    {
        wifi_hal_ap_result_struct_translator(&__result, ap_info);
    }

    return status;
}

/**
 *  wifi_hal_scan_stop
 *  
 *  @brief Stop wifi scan for access points
 *  
 *  @retval u8  status
 *  
 *  @details Details
 *  
 *  @authors Patrick Downs, Quyen Leba
 */
u8 wifi_hal_scan_stop()
{
    return cc3000_stopScan();
}

/**
 *  wifi_hal_profile_add
 *  
 *  @brief Adds profile to module and sets priority
 *  
 *  @retval u8  status
 *  
 *  @details Saves AP info in module with a connection priority 1-7
 *  
 *  @authors Patrick Downs
 */
u8 wifi_hal_profile_add(WIFI_HAL_AP_INFO *ap_info)
{
    u8 status;
    CC3000_AP_RESULTS __result;

    wifi_hal_profile_delete(0xFF);  //delete all profiles

    if (ap_info->passcodeLen >= sizeof(ap_info->passcode))
    {
        status = S_INPUT;
    }
    else
    {
        wifi_hal_ap_info_struct_translator(ap_info,&__result);
        status = cc3000_addProfile(&__result, (u8*)ap_info->passcode, ap_info->passcodeLen, ap_info->priority);
    }

    return status;
}

/**
 *  wifi_hal_profile_delete
 *  
 *  @brief Delete profile at a specified priority number
 *  
 *  @retval u8  status
 *  
 *  @details Details
 *  
 *  @authors Patrick Downs
 */
u8 wifi_hal_profile_delete(u8 priority)
{
    return cc3000_deleteProfiles(priority);
}

/**
 *  wifi_hal_resetandconnect
 *  
 *  @brief Resets the module and sets connection policy to connect profile APs
 *  
 *  @retval Return_Description
 *  
 *  @details Details
 *  
 *  @authors Patrick Downs
 */
#define CC3000_STATIC_IP    0
u8 wifi_hal_resetandconnect()
{
    u8 status = S_SUCCESS;

#if CC3000_STATIC_IP    
    // SEMA Intranet settings
    // No DHCP
    // Static IP: 192.168.101.102
    // Gateway: 192.168.101.1
    // DNS: no DNS
    u32 aucIP = 0x6665A8C0;
    u32 aucSubnetMask = 0x00FFFFFF;
    u32 aucDefaultGateway = 0x0165A8C0;
    u32 aucDNSServer = 0x00000000;
    status = wifi_hal_set_dhcp(aucIP, aucSubnetMask, aucDefaultGateway, aucDNSServer);
#else 
//    status = wifi_hal_set_dhcp(0, 0, 0, 0);
#endif
    
    if(status == S_SUCCESS)
    {
        status = cc3000_resetandconnect(FALSE);
    }
    
    return status;
}

/**
 *  wifi_hal_connect
 *  
 *  @brief Connect to an AP
 *  
 *  @param [in] ap_info 
 *  @retval status
 *  
 *  @details Details
 *  
 *  @authors Patrick Downs
 */
u8 wifi_hal_connect(WIFI_HAL_AP_INFO *ap_info)
{
    u8  status;
    
    status = cc3000_connectToAP(ap_info->ssidName, ap_info->bssid,
                                ap_info->passcode, ap_info->passcodeLen,
                                (u32)ap_info->security);
    
    return status;
}

/**
 *  wifi_hal_disconnect
 *  
 *  @brief Disconnect from currently AP
 *  
 *  @retval Return_Description
 *  
 *  @details Details
 *  
 *  @authors Patrick Downs
 */
u8 wifi_hal_disconnect()
{
    wifi_hal_profile_delete(0xFF);      //delete all profiles
    cc3000_disconnect();
    wifi_hal_resetandconnect();
    return S_SUCCESS;
}

/**
 *  wifi_hal_smartconfig
 *  
 *  @brief Handles the smartconfig process
 *  
 *  @retval Return_Description
 *  
 *  @details Details
 *  
 *  @authors Patrick Downs, Quyen Leba
 */
u8 wifi_hal_smartconfig()
{
    u8 status;

    status = S_FAIL;
//    status = cc3000_smartConfig(CC3000_SMARTCONFIG_OPCODE opcode);

    return status;
}

/**
 *  wifi_hal_setmdnsadvertiser
 *  
 *  @brief Sets the mDNS advertiser name
 *  
 *  @retval none
 *  
 *  @details Sets the mDNS advertiser name
 *  
 *  @authors Patrick Downs
 */
u8 wifi_hal_setmdnsadvertiser(u8 *devicename)
{
    u8 status;
    
    status = cc3000_setMdnsAdvertiser(devicename);
    
    return status;
}
    
/**
 *  Get wifi mac address
 *  
 *  @brief Brief
 *  
 *  @param [out] macaddress MAC address in ASCII (NULL terminated)
 *
 *  @retval status
 *  
 *  @details Details
 *  
 *  @authors Quyen Leba
 */
u8 wifi_hal_getmacaddress(u8 *macaddress)
{
    u8  macaddr[MAC_ADDR_LEN];
    u8  tmpString[16];
    u8  i;
    u8  status;

    macaddress[0] = NULL;
    status = cc3000_getMacAddress(macaddr);
    if (status == S_SUCCESS)
    {
        for(i=0;i<MAC_ADDR_LEN;i++)
        {
            itoa(macaddr[i],tmpString,16);
            strcat((char*)macaddress,(char*)tmpString);
            if (i+1 < MAC_ADDR_LEN)
            {
                strcat((char*)macaddress,":");
            }
        }
    }
    return status;
}

/**
 *  Get wifi mac address
 *  
 *  @brief Brief
 *  
 *  @param [out] macaddress MAC address in ASCII (NULL terminated)
 *
 *  @retval status
 *  
 *  @details Details
 *  
 *  @authors Quyen Leba
 */
u8 wifi_hal_getwifiversion_string(u8 *version)
{
    u8  buffer[16];
    u8  pVersion[6];
    u8  status;
    
    status = cc3000_readPatchVersion(pVersion);
    if(status == S_SUCCESS)
    {
        //sprintf((char*)version, "%d.%d", pVersion[0], pVersion[1]);
        itoa(pVersion[0],buffer,10);
        strcpy((char*)version,(char*)buffer);
        strcat((char*)version,".");
        itoa(pVersion[1],buffer,10);
        strcat((char*)version,(char*)buffer);
    }
    
    return status;    
}

/**
 *  wifi_hal_startup
 *  
 *  @brief Power on wifi if it's off and wait for connection
 *  
 *  @param [in] update_request
 *
 *  @retval u8  status
 *  
 *  @details Details
 *  
 *  @authors Patrick Downs
 */
u8 wifi_hal_startup(void)
{
    bool connected;
    bool dhcpdone;
    u32 time_mscount;    
    u8 status;
    
    memset(&wifiinfo, 0, sizeof(wifiinfo));
    
    cc3000_startup();
    
    status = S_FAIL;
    
    // Connnect to AP
    time_mscount = rtc_getvalue();
    do
    {
        connected = cc3000_isConnected();
        
        if ((rtc_getvalue() - time_mscount) > 5000) // 5 second timeout
        {
            status = S_TIMEOUT;
            break;
        }
    }
    while(connected == FALSE);
        
    if(connected)
    {
        // Wait for IP from DHCP
        time_mscount = rtc_getvalue();
        do
        {
            dhcpdone = cc3000_isDhcpDone();
            
            if ((rtc_getvalue() - time_mscount) > 10000) // 10 second timeout
            {
                status = S_TIMEOUT;
                break;
            }
        }
        while(dhcpdone == FALSE);
        
        status = S_SUCCESS;
    }
    
    return status;
}

/**
 *  wifi_hal_status
 *  
 *  @brief 
 *
 *  @retval u8  status
 *  
 *  @details Details
 *  
 *  @authors Patrick Downs
 */
void wifi_hal_status(WIFI_STATUS_INFO *retwifiinfo)
{
    //u8 status;
    CC3000_IPCONFIG ipconfig;    
    u8 mac_addr[MAC_ADDR_LEN];
    
    memset(&wifiinfo, 0, sizeof(wifiinfo));
    
    wifiinfo.status = WIFI_STATUS_POWER_OFF;
    
    // Need to return mac address    
    cc3000_getMacAddress(mac_addr);
    
    sprintf((char*)wifiinfo.ipconfig.macaddress, "%02X:%02X:%02X:%02X:%02X:%02X", mac_addr[0], mac_addr[1], 
            mac_addr[2], mac_addr[3], mac_addr[4], mac_addr[5]);
    
    // Init IP to zeros
    strcpy((char*)wifiinfo.ipconfig.ipaddress, "0.0.0.0");
    
    if(cc3000_isPoweredOn())
    {
        wifiinfo.status = WIFI_STATUS_NOT_CONNECTED;
        if(cc3000_isConnected())
        {
            wifiinfo.status = WIFI_STATUS_CONNECTED_WAITINGDHCP;
            if(cc3000_isDhcpDone())
            {
                wifiinfo.status = WIFI_STATUS_CONNECTED_READY;
                
                cc3000_getIpConfig(&ipconfig);
                // For now, WIFI_HAL_IPCONFIG == CC3000_IPCONFIG so we can memcpy it
                memcpy((void*)&wifiinfo.ipconfig, (const void*)&ipconfig, sizeof(wifiinfo.ipconfig));
            }
        }
    }
    
    memcpy(retwifiinfo, (void const*)&wifiinfo, sizeof(WIFI_STATUS_INFO));
}

//------------------------------------------------------------------------------
// Download a file
// Inputs:  const u8 *hostaddress ("www.sctflash.com")
//          const u8 *srcfilepath ("/SCT/SCT.bin")
//          const u8 *dstfilepath ("\\USER\\SCT.bin")
//          PROGRESS_REPORT_FUNC progress_report_func
// Return:  u8  status
// Engineer: Quyen Leba
//------------------------------------------------------------------------------
#define WIFI_HAL_DOWNLOAD_FILE_RETRY    2
u8 wifi_hal_download_file(const u8 *hostaddress, const u8 *srcfilepath,
                          const u8 *dstfilepath, PROGRESS_REPORT_FUNC_SIMPLE progress_report_func)
{
    u8  status;
    u8 retry;
    bool retry_resetandconnect;
    
    status = wifi_hal_startup();
    if(status == S_SUCCESS)
    {        
        retry = WIFI_HAL_DOWNLOAD_FILE_RETRY;
        retry_resetandconnect = TRUE;
        while(retry)
        {
            status = cc3000_httpGetRequest(hostaddress, srcfilepath, dstfilepath, 80,
                                           progress_report_func);
            if(status == S_SUCCESS)
            {
                break;
            }
            else
            {
                retry--;
                if(retry == 0 && retry_resetandconnect)
                {
                    // Failed, reset and re-connect to AP
                    log_push_error_point(0x7109); // Fail retrys, try disconnect and reconnect to AP
                    retry_resetandconnect = FALSE;
                    cc3000_resetandconnect(TRUE);                
                    retry = WIFI_HAL_DOWNLOAD_FILE_RETRY;
                }
            }
        }
        
        if(!retry)
        {
            log_push_error_point(0x710A); // Completely failed download 
            status = S_TIMEOUT;
        }
    }

    return status;
}

//------------------------------------------------------------------------------
// Download a file
// Inputs:  const u8 *hostaddress ("www.sctflash.com")
//          const u8 *srcfilepath ("/SCT/SCT.bin")
//          const u8 *dstfilepath ("\\USER\\SCT.bin")
//          PROGRESS_REPORT_FUNC progress_report_func
// Return:  u8  status
// Engineer: Quyen Leba
//------------------------------------------------------------------------------
u8 wifi_hal_post_request(const u8 *hostaddress, const u8* postbodyfilepath, const u8 *srcfilepath,
                         const u8 *responsefilepath, PROGRESS_REPORT_FUNC_SIMPLE progress_report_func)
{
    u8  status;
    status = wifi_hal_startup();
    if(status == S_SUCCESS)
    {
        status = cc3000_httpPostRequest(hostaddress, postbodyfilepath, srcfilepath, responsefilepath, 80,
                                        progress_report_func);
    }
    
    return status;
}

//------------------------------------------------------------------------------
// Handle Http Request
// Inputs:  const u8 *hostaddress ("www.sctflash.com")
//          const u8 *srcfilepath ("/SCT/SCT.bin")
//          const u8 *dstfilepath ("\\USER\\SCT.bin")
//          PROGRESS_REPORT_FUNC progress_report_func
// Return:  u8  status
// Engineer: Patrick Downs
//------------------------------------------------------------------------------
u8 wifi_hal_handle_http_request(const u8 *hostaddress, const u8* txbuffer, u32 txlength,
                         const u8 *rxfile, PROGRESS_REPORT_FUNC_SIMPLE progress_report_func)
{
    u8  status;
    status = wifi_hal_startup();
    if(status == S_SUCCESS)
    {
        status = cc3000_handleHttpRequest(hostaddress, txbuffer, txlength, rxfile, 80, 
                                          progress_report_func);
    }
    
    return status;
}


/**
 *  wifi_hal_parse_url
 *  
 *  @brief Parse URL to get host and resource strings
 *  
 *  @param [in]  url    input URL to parse
 *  @param [out] host   return location for host string
 *  @param [out] resource return location for resource string
 *
 *  @retval u8  status
 *  
 *  @details Parse an input URL to get the host and resource strings required for
 *              WIFI functions
 *  
 *  @authors Patrick Downs
 */
u8 wifi_hal_parse_url(const u8 *url, u8 *host, u8 *resource)
{
    char *start;
    char *end;
    int length;
    
    start = strstr((char const*)url, "://"); // Skip "http://" if present
    if(start)    
        start+=3;
    else
        start = (char*)url;
    
    end = strchr(start, '/');
    if(end)
    {
        length = (end - start);
        memcpy(host, start, length);
        host[length] = 0x00; // Save host string        
    }
    else
        return S_INPUT;
    
    strcpy((char*)resource, end); // Save resource string
    
    return S_SUCCESS;
}

/**
 *  wifi_hal_getNtpTime
 *  
 *  @brief Get unix time from NTP server pool
 *  
 *  @param [out] unixtime   return parameter for unix time
 *  @retval u8  status
 *  
 *  @details Gets the GMT timestamp in seconds since 01/01/1970 from the one of
 *              the US NTP server pool sites.
 *  
 *  @authors Patrick Downs
 */
u8 wifi_hal_getNtpTime(u32* unixtime)
{
    u8 status;
    
    status = wifi_hal_startup();
    if(status == S_SUCCESS)
    {
        status = cc3000_getNtpTime(unixtime);
    }
    
    return status;
}

