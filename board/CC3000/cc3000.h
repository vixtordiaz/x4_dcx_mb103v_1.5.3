/**
  **************** (C) COPYRIGHT 2011 SCT Performance, LLC *********************
  * File Name          : cc3000.h
  * Device / System    : SCT products using the "CommonCode" platform
  * Author             : Quyen Leba
  *
  * Version            : 1 
  * Date               : 05/02/2011
  * Description        : 
  *                    : 
  *
  *
  * History            : 05/02/2011 M. Davis
  *                    :   Add SCT Copyright header,
  *
  ******************************************************************************
  */

#ifndef __CC3000_H
#define __CC3000_H

#include <arch/gentype.h>
#include <common/function_ptr_defs.h>

#define SSID_MAX_LEN            32 // Can't be changed, defined in AppBoard
#define KEY_MAX_LEN             32 // Can't be changed, defined in AppBoard

typedef struct
{
    u32 count;  // SSIDs found
    u32 status; // Scan status
    
    // AP found
    u16 isValid         :1;
    u16 rssi            :7;
    u16 security        :2;
    u16 ssidLength      :6;
    u16 timestamp;
    u8  ssidName[32];
    u8  bssid[6];
}CC3000_AP_RESULTS;

// IP V4
typedef struct
{
    u8 ipaddress[16];
    u8 subnetmask[16];
    u8 gateway[16];
    u8 dhcpserver[16];
    u8 dnsserver[16];
    u8 macaddress[18];
    u8 ssid[SSID_MAX_LEN+1];
}CC3000_IPCONFIG;

typedef enum
{
    CC3000_SOCKETTYPE_UDP = 0x00,
    CC3000_SOCKETTYPE_TCP = 0x01,
}CC3000_SOCKETTYPE;

typedef enum
{
    CC3000_SCAN_OPCODE_START    = 0x00,
    CC3000_SCAN_OPCODE_STOP     = 0x01,
    CC3000_SCAN_OPCODE_GETSSID  = 0x02,
}CC3000_SCAN_OPCODE;

typedef enum
{
    CC3000_SMARTCONFIG_OPCODE_START     = 0x00,
    CC3000_SMARTCONFIG_OPCODE_ABORT     = 0x01,
    CC3000_SMARTCONFIG_OPCODE_FINISH    = 0x02,
}CC3000_SMARTCONFIG_OPCODE;

typedef enum
{
    CC3000_SETCONNECT_CONNECT           = 0x00,
    CC3000_SETCONNECT_DISCONNECT        = 0x01,
}CC3000_SETCONNECT_OPCODE;

typedef enum
{
    CC3000_HTTP_REQUEST_GET             = 0x00,
    CC3000_HTTP_REQUEST_POST            = 0x01,
}CC3000_HTTP_REQUEST_TYPE;

void cc3000_init(unsigned short cRequestPatch);
void cc3000_deinit();

void cc3000_init_cs();
void cc3000_deinit_cs();

u8 cc3000_getMacAddress(u8* mac);
u8 cc3000_setMacAddress(u8* newmac);
void cc3000_startup();
bool cc3000_isConnected(void);
bool cc3000_isDhcpDone(void);
bool cc3000_isPoweredOn(void);
void cc3000_displayWifiInfo();
u8 cc3000_readPatchVersion(u8* pVersion);
void cc3000_getIpConfig(CC3000_IPCONFIG *ipconfig);
u8 cc3000_setDhcp(u32 ip, u32 subnetmask, u32 defaultgateway, u32 dnsserver);

u8 cc3000_smartConfig(CC3000_SMARTCONFIG_OPCODE opcode);
void cc3000_smartconfig_start();
void cc3000_smartconfig_abort();
void cc3000_smartconfig_finish();

u8 cc3000_scanSSID(CC3000_AP_RESULTS *results, CC3000_SCAN_OPCODE opcode);
u8 cc3000_startScanSSID(CC3000_AP_RESULTS *results);
u8 cc3000_getNextSSID(CC3000_AP_RESULTS *results);
u8 cc3000_stopScan(void);

u8 cc3000_addProfile(CC3000_AP_RESULTS *result, u8 *key, u32 keylen, u32 priority);
u8 cc3000_deleteProfiles(u32 priority);

u8 cc3000_resetandconnect(bool waitforconnect);
u8 cc3000_disconnect(void);
u8 cc3000_connectToAP(u8* ssid, u8* bssid, u8* key, u8 keylen, s32 secmode);

u8 cc3000_getNtpTime(u32 *unixtime);
u8 cc3000_getGeoLocation();

void cc3000_debug();

u8 cc3000_patchprogrammer();
u32 cc3000_patchprogrammer_read_updateversion_uint();

u8 cc3000_getHostByName(const u8 *hostname, u32 *ipconfig);
u8 cc3000_httpGetRequest(const u8 *host, const u8 *resource, const u8* rxfile, u16 port,
                         PROGRESS_REPORT_FUNC_SIMPLE progress_report_func);

u8 cc3000_httpPostRequest(const u8 *host, const u8 *resource, const u8* txfile, const u8* rxfile, u16 port,
                         PROGRESS_REPORT_FUNC_SIMPLE progress_report_func);

u8 cc3000_handleHttpRequest(const u8 *host, const u8* txbuffer, u32 txlength, const u8* rxfile, u16 port,
                         PROGRESS_REPORT_FUNC_SIMPLE progress_report_func);

#endif  //__CC3000_Hw