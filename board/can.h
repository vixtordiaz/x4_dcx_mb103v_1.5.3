/**
  **************** (C) COPYRIGHT 2011 SCT Performance, LLC *********************
  * File Name          : can.h
  * Device / System    : SCT products using the "CommonCode" platform
  * Author             : Quyen Leba
  *
  * Version            : 1 
  * Date               : 05/02/2011
  * Description        : 
  *                    : 
  *
  *
  * History            : 05/02/2011 M. Davis
  *                    :   Add SCT Copyright header,
  *
  ******************************************************************************
  */

#ifndef __CAN_H
#define __CAN_H

#include <arch/gentype.h>

#define CanMsgParseStdFrame(id)         {id, 0x00, CAN_ID_STD, CAN_RTR_DATA, 8,}
#define CanMsgParseExtFrame(id,len)     {0x00, id, CAN_ID_EXT, CAN_RTR_DATA, len,}
#define CanMsgFrameData(frame)          frame.Data
#define CanMsgFrameDataLength(frame)    frame.DLC
#define CanMsgFrameStandardAddr(frame)  frame.StdId
#define CanMsgFrameExtendedAddr(frame)  frame.ExtId

extern CanRxMsg can_rxframe;

typedef enum
{
    CAN_TX_MSGOBJ           = 10,
    CAN_TX_EXTOBJ           = 12,
    CAN_RX_MSGOBJ_NORMAL    = 15,
    CAN_RX_MSGOBJ_DATALOG   = 16,
    CAN_RX_MSGOBJ_EXT       = 18,
}CAN_MSGOBJ;

typedef enum
{
    CAN_INITMODE_NORMAL     = 0,
    CAN_INITMODE_7EX        = 1,
    CAN_INITMODE_CUSTOM     = 2,
}CAN_INITMODE;

void can_init(CAN_INITMODE initmode, u32 custom_filter_addr);
void can_setfiltermode_standardid();
void can_setfiltermode_extendedid();
void can_deinit();
void can_setTxMsgObj(CAN_MSGOBJ msgobj);
void can_setRxMsgObj(CAN_MSGOBJ msgobj);
u8 can_tx_s(CanTxMsg TxMessage);
u8 can_tx(CanTxMsg TxMessage);
u8 can_tx_ext(CanTxMsg txframe);
u8 can_rx(u32 timeout);
u8 can_rxcomplex(u8 *privdata, u32 privdatalength,
                 u8 *rxdatabuffer, u16 *rxdatabufferlength);
u8 can_rx_ext(u32 timeout);

#endif  //__CAN_H
