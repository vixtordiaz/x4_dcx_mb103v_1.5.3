/**
  **************** (C) COPYRIGHT 2011 SCT Performance, LLC *********************
  * File Name          : genplatform.h
  * Device / System    : SCT products using the "CommonCode" platform
  * Author             : Quyen Leba
  *
  * Version            : 1 
  * Date               : 05/02/2011
  * Description        : 
  *                    : 
  *
  *
  * History            : 05/02/2011 M. Davis
  *                    :   Add SCT Copyright header,
  *
  ******************************************************************************
  */

#ifndef __GENPLATFORM_H
#define __GENPLATFORM_H

#include <device_config.h>

#if USE_WIFI
#include <board/CC3000/wifi_hal.h>
#endif

#include <board/peripherals.h>
#include <board/bootloader_vb.h>
#include <board/hardware_access.h>
#include <board/adc.h>
#include <board/datalogfeatures.h>
#include <board/mblexec.h>
#include <board/mblif.h>
#include <board/media.h>
#include <board/can.h>
#include <board/sci.h>
#include <board/iso.h>
#include <board/vpw.h>
#include <board/scp.h>
#include <board/debugif.h>

#if __MB103V_PLATFORM__
#include <board/MB103V/mb103v.h>
#elif __MB215V_PLATFORM__
#include <board/MB215V/mb215v.h>
#else
#error "gentype.h: Invalid PLATFORM"
#endif

#if __COMMLINK_HAL_BLUETOOTH__
#include <board/board_bluetooth.h>
#include <board/bootloader_ab_bt11.h>
#include <board/properties_ab_bt11.h>
#elif __COMMLINK_HAL_KEN__
#include <board/board_ken.h>
#include <board/bootloader_ab_ken.h>
#include <board/properties_ab_ken.h>
#elif __COMMLINK_HAL_X4__
#include <board/board_x4.h>
#include <board/bootloader_ab_x4.h>
#include <board/properties_ab_x4.h>
#else
#error genplatform.h: COMMLINK_HAL
#endif

#endif    //__GENPLATFORM_H
