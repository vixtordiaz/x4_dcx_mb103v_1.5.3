/**
  **************** (C) COPYRIGHT 2011 SCT Performance, LLC *********************
  * File Name          : iso.c
  * Device / System    : SCT products using the "CommonCode" platform
  * Author             : Rhonda Rusak
  *
  * Version            : 1 
  * Date               : 05/13/2014
  * Description        : 
  *                    : 
  *
  *
  *
  ******************************************************************************
  */

#include <string.h>
#include <board/mblexec.h>
#include <board/delays.h>
#include <common/statuscode.h>
#include <common/obd2def.h>
#include <common/obd2iso.h>
#include "iso.h"


//------------------------------------------------------------------------------
// Initialize ISO protocols (ISO9141/ISO14230)
// Inputs:  u8 status; 
//          VehicleCommType *vehiclecommtype
// Return:  status; 
// Engineer: Ricardo Cigarroa
//------------------------------------------------------------------------------
u8 iso_init(VehicleCommType *vehiclecommtype)
{
    u8  status = S_FAIL; 
    u8  buffer[4];
    
    switch(*vehiclecommtype)
    {
        case CommType_KLINE:
             status = obd2iso_ping(CommType_KLINE);
             break;
        case CommType_KWP2000:
             status = obd2iso_ping(CommType_KWP2000);
             break;
        default:
             /*vehiclecommtype is unknown.Init is 
             required to find out commtype.*/    
             break;                  
    }
    if(status == S_SUCCESS)
    {
        return S_SUCCESS; 
    }

    /*We init with ccmmtype KLINE, but the vehicle board tries to init both, 
      KLINE and KWP200*/
    memset(buffer,0,sizeof(buffer));
    
    buffer[0] = CommType_KLINE;
    buffer[1] = CommLevel_Default;
    
    mblexec_call(MBLEXEC_OPCODE_SET_VEHICLE_COMM,buffer,sizeof(buffer),
                 NULL,NULL);
    
    /*Check iso comm type*/
    status = obd2iso_ping(CommType_KLINE);
    if(status != S_SUCCESS)
    {
        status = obd2iso_ping(CommType_KWP2000);
        if(status == S_SUCCESS)
        {
            *vehiclecommtype = CommType_KWP2000;
        }
        else
        {
            status = S_FAIL; 
        }
    }
    else
    {
        *vehiclecommtype = CommType_KLINE;
    }
    
    return status; 
}

//------------------------------------------------------------------------------
// ISO tx function
// Inputs:  u8 *cmd_frame,
//          u16 tx_datalength,
//          u16 txflags,
//          u16 rxflags,
// Return:  status; 
// Engineer: Ricardo Cigarroa
//------------------------------------------------------------------------------
u8 iso_tx(u8 *cmd_frame, u16 tx_datalength, u16 txflags, u16 rxflags)
{
    u8  txbuffer[8+32];
    u8  status;
    u16 temp = 0;

    memset(txbuffer,0,sizeof(txbuffer)); 
    
    temp = txflags; 
    memcpy((char*)&txbuffer[0],&temp, 2);        //tx flags
   
    temp = rxflags;
    memcpy((char*)&txbuffer[2],&temp, 2);        //rx flags for

    memcpy((char*)&txbuffer[8],(char*)cmd_frame,tx_datalength);

    status = mblexec_call(MBLEXEC_OPCODE_SEND_SIMPLE_MSG,
                          txbuffer,tx_datalength+8,
                          NULL,NULL);
    
    return status;
}

//------------------------------------------------------------------------------
// ISO rx function
// Inputs:  u8 *rxdatabuffer,
//          u16 *rxdatabufferlength,
//          u32 opcode_length,
//          u16 rxflags,
// Return:  status; 
// Engineer: Ricardo Cigarroa
//------------------------------------------------------------------------------
u8 iso_rx(u8 *rxdatabuffer, u16 *rxdatabufferlength, u32 opcode_length, 
          u16 rxflags, u32 timeout)
{
    u8  buffer[4];
    u8  status;
    u16 temp = rxflags;

    memset(buffer,0,sizeof(buffer));
    memcpy((char*)&buffer[0],&temp,2);     //rxflags
    buffer[2] = timeout;
    buffer[3] = timeout >> 8;

    status = mblexec_call(MBLEXEC_OPCODE_RECEIVE_SIMPLE_MSG,
                          buffer,sizeof(buffer),
                          rxdatabuffer,rxdatabufferlength);
    if(status != S_SUCCESS)
    {
        //error point
        status = S_FAIL; 
    }
    
    return status;
}
