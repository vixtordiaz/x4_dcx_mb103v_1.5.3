/**
  **************** (C) COPYRIGHT 2011 SCT Performance, LLC *********************
  * File Name          : scp.c
  * Device / System    : SCT products using the "CommonCode" platform
  * Author             : Quyen Leba
  *
  * Version            : 1 
  * Date               : 05/02/2011
  * Description        : 
  *                    : 
  *
  *
  * History            : 05/02/2011 M. Davis
  *                    :   Add SCT Copyright header,
  *
  ******************************************************************************
  */

#include <string.h>
#include <board/genplatform.h>
#include <board/timer.h>
#include <board/interrupt.h>
#include <common/statuscode.h>
#include <common/obd2.h>
#include <common/obd2scp.h>
#include <board/mblexec.h>
#include "scp.h"

//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
void scp_init(SCP_TYPE scptype, SCP_SPEED scpspeed)
{
    VehicleCommConfig comm_config;
    u8  buffer[4];
    u8  status;
    
    comm_config.raw.value = 0;
    
    if (scpspeed == SCP_HIGH_SPEED)
    {
        comm_config.scp.highspeed = TRUE;
    }
    else
    {
        comm_config.scp.highspeed = FALSE;
    }

    if (scptype == SCP_TYPE_32BIT)
    {
        buffer[0] = CommType_SCP32;
    }
    else
    {
        buffer[0] = CommType_SCP;
    }

    buffer[1] = CommLevel_Default;
    buffer[2] = comm_config.raw.value & 0xFF;
    buffer[3] = comm_config.raw.value >> 8;
    //no privdata

    status = mblexec_call(MBLEXEC_OPCODE_SET_VEHICLE_COMM,buffer,sizeof(buffer),
                          NULL,NULL);
    if (status != S_SUCCESS)
    {
        //TODO
    }
}

//------------------------------------------------------------------------------
// Send a SCP frame
// Inputs:  u8  *databuffer
//          u16 datalength
//          u8  mode
// Return:  u8  status
// Engineer: Quyen Leba, Patrick Downs
//------------------------------------------------------------------------------
u8 scp_tx(u8 *databuffer, u16 datalength, u8 mode)
{
    u8  scp_txbuffer[2+32];
    u8  status;
    
    if(mode == SCP_MODE_FUNCTIONAL)
    {
        scp_txbuffer[0] = SCP_FLAGS_FUNCTIONALADDR;
        scp_txbuffer[1] = 0;
    }
    else
    {
        scp_txbuffer[0] = 0;
        scp_txbuffer[1] = 0;
    }
    
    memcpy((char*)&scp_txbuffer[2],(char*)databuffer,datalength);
    status = mblexec_call(MBLEXEC_OPCODE_SEND_SIMPLE_MSG,scp_txbuffer,datalength+2,
                          NULL,NULL);
    return status;
}

//------------------------------------------------------------------------------
// Receive a raw SCP frame
//  [0xC4][Responsed_Id][ECM_ID][Responsed_CMD][n:data]
//  [0xC4][Responsed_Id][ECM_ID][0x7F][CMD][errorcode/data]
// Inputs:  u8  mode
//          u8  tester_frame_address
//          u8  tester_ifr_address
//          u32 timeout
// Outputs: u8  *databuffer
//          u16 *datalength
// Return:  u8  status
// Engineer: Quyen Leba
//------------------------------------------------------------------------------
u8 scp_rx(u8 *databuffer, u16 *datalength, u8 mode,
          u8 tester_frame_address, u8 tester_ifr_address, u32 timeout)
{
    u8  buffer[4];
    u8  status;
    
    buffer[0] = tester_frame_address;
    buffer[1] = tester_ifr_address;
    buffer[2] = timeout;
    buffer[3] = timeout >> 8;

    status = mblexec_call(MBLEXEC_OPCODE_RECEIVE_SIMPLE_MSG,
                          buffer,sizeof(buffer),
                          databuffer,datalength);
    return status;
}

//------------------------------------------------------------------------------
// Receive data block from SCP. Expect data as following:
//  [C4 F0 10 cmd] [6: data1]
//  [C4 F0 10 cmd] [6: data2]
//  [C4 F0 10 cmd] [6: data3]
//  ...
//  [C4 F0 10 CMD] [#: dataN] where # <= 6 depend on expecting length
// Inputs:  u8  *privdata
//          u32 privdatalength
//          u8  mode
//          u8  tester_frame_address
//          u8  tester_ifr_address
// Outputs: u8  *rxdatabuffer
//          u16 *rxdatabufferlength
// Return:  u8  status
// Engineer: Quyen Leba
//------------------------------------------------------------------------------
u8 scp_rx_complex(u8 *privdata, u32 privdatalength,
                  u8 *rxdatabuffer, u16 *rxdatabufferlength, u8 mode,
                  u8 tester_frame_address, u8 tester_ifr_address)
{
    u8  buffer[256];
    u8  status;

    buffer[0] = tester_frame_address;
    buffer[1] = tester_ifr_address;
    buffer[2] = 0;  //flags
    buffer[3] = 0;  //flags
    memcpy((char*)&buffer[4],(char*)privdata,privdatalength);

    status = mblexec_call(MBLEXEC_OPCODE_RECEIVE_COMPLEX_MSG,
                          buffer,privdatalength+4,
                          rxdatabuffer,
                          rxdatabufferlength);
    return status;
}

//------------------------------------------------------------------------------
// Send a SCP frame and receive its response frame
// Inputs:  u8  *tx_databuffer
//          u16 tx_datalength
//          u8  tx_mode
//          u8  rx_mode
//          u8  tester_frame_address
//          u8  tester_ifr_address
// Outputs: u8  *rx_datalength
//          u16 *datalength
// Return:  u8  status
// Engineer: Quyen Leba
//------------------------------------------------------------------------------
u8 scp_txrx_simple(u8 *tx_databuffer, u16 tx_datalength, u8 tx_mode,
                   u8 *rx_databuffer, u16 *rx_datalength, u8 rx_mode,
                   u8 tester_frame_address, u8 tester_ifr_address)
{
    u8  scp_txbuffer[8+32];
    u8  status;
    
    scp_txbuffer[0] = 0;
    scp_txbuffer[1] = 0;
    scp_txbuffer[2] = 0;
    scp_txbuffer[3] = 0;
    scp_txbuffer[4] = tester_frame_address;
    scp_txbuffer[5] = tester_ifr_address;
    scp_txbuffer[6] = 0;
    scp_txbuffer[7] = 0;
    memcpy((char*)&scp_txbuffer[8],(char*)tx_databuffer,tx_datalength);

    status = mblexec_call(MBLEXEC_OPCODE_SEND_RECEIVE_SIMPLE_MSG,
                          scp_txbuffer,tx_datalength+8,
                          rx_databuffer,rx_datalength);
    return status;
}

//------------------------------------------------------------------------------
// Read a block of data from ECU using $35 $36 $37
// Inputs:  u32 address
//          MemoryAddressType addrtype
//          u16 length
// Output:  u8  *buffer
// Return:  u8  status
// Engineer: Quyen Leba
//------------------------------------------------------------------------------
u8 scp_readblock_bycmd35single36single37(u32 address,
                                         MemoryAddressType addrtype,
                                         u16 length, u8 *buffer,
                                         u8 tester_frame_address,
                                         u8 tester_ifr_address)
{
    u8  status;
    u16 bufferlength;
    struct
    {
        u32 flags;
        u8  tester_frame_address;
        u8  tester_ifr_address;
        u16 length;
        u32 address;
        MemoryAddressType addrtype;
        u8  reserved[19];
    }readblock_info = 
    {
        .flags = 0,
        .tester_frame_address = tester_frame_address,
        .tester_ifr_address = tester_ifr_address,
        .length = length,
        .address = address,
        .addrtype = addrtype,
    };
    memset((char*)readblock_info.reserved,0,sizeof(readblock_info.reserved));
    //Bit[3..0]: helper type
    readblock_info.flags = Obd2scpHelper_ReadblockByCmd35single36single37;

    status = mblexec_call(MBLEXEC_OPCODE_READBLOCK_HELPER,
                          (u8*)&readblock_info,sizeof(readblock_info),
                          buffer,&bufferlength);
    return status;
}
