/**
  **************** (C) COPYRIGHT 2011 SCT Performance, LLC *********************
  * File Name          : peripherals.h
  * Device / System    : SCT products using the "CommonCode" platform
  * Author             : Quyen Leba
  *
  * Version            : 1 
  * Date               : 05/02/2011
  * Description        : 
  *                    : 
  *
  *
  * History            : 05/02/2011 M. Davis
  *                    :   Add SCT Copyright header,
  *
  ******************************************************************************
  */

#ifndef __PERIPHERALS_H
#define __PERIPHERALS_H

#include <arch/gentype.h>

void peripherals_init();
void peripherals_init_vehicleboard();

#endif  //__PERIPHERALS_H
