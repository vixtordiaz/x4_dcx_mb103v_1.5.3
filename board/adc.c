/**
  **************** (C) COPYRIGHT 2011 SCT Performance, LLC *********************
  * File Name          : adc.c
  * Device / System    : SCT products using the "CommonCode" platform
  * Author             : Quyen Leba
  *
  * Version            : 1 
  * Date               : 05/02/2011
  * Description        : 
  *                    : 
  *
  *
  * History            : 05/02/2011 M. Davis
  *                    :   Add SCT Copyright header,
  *
  ******************************************************************************
  */

#include <string.h>
#include <arch/gentype.h>
#include <board/genplatform.h>
#include <common/statuscode.h>
#include <board/mblexec.h>
#include "adc.h"

//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
void adc_init(ADC_MODE mode)
{
    adcmb_init();
}

//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
u8 adc_read(ADC_CHANNEL channel, float *DataValueFloat)
{
    u8  buffer[256];
    u16 bufferlength;
    u8  ch;
    u8  status;

    switch(channel)
    {
    case ADC_AIN7:
    case ADC_AIN8:
        status = adcmb_read(channel,DataValueFloat);
        break;
    default:
        ch = (u8)channel;
        status = mblexec_call(MBLEXEC_OPCODE_GET_AIN,&ch,1,
                              (u8*)buffer,&bufferlength);
        if (status == S_SUCCESS)
        {
            if (bufferlength == 4)
            {
                *DataValueFloat = *((float*)buffer);
            }
            else
            {
                status = S_FAIL;
            }
        }
        break;
    }
    return status;
}

//------------------------------------------------------------------------------
// Config GPIO2_OUT of VehicleBoard
// Input:   bool isHighOut (TRUE:HIGH, FALSE:low)
// Return:  u8  status
// Engineer: Quyen Leba
//------------------------------------------------------------------------------
u8 adc_config_gpio1_out(bool isHighOut)
{
    MBLEXEC_HWCONFIG hwconfig;
    u16 bufferlength;
    u8  status;

    status = mblexec_call(MBLEXEC_OPCODE_GET_HWCONFIG,NULL,0,
                          (u8*)&hwconfig,&bufferlength);
    if (status == S_SUCCESS && hwconfig.gpio1_out != isHighOut)
    {
        hwconfig.gpio1_out = 0;
        if (isHighOut)
        {
            hwconfig.gpio1_out = 1;
        }
        status = mblexec_call(MBLEXEC_OPCODE_SET_HWCONFIG,
                              (u8*)&hwconfig,sizeof(MBLEXEC_HWCONFIG),
                              NULL,NULL);
    }

    return status;
}

//------------------------------------------------------------------------------
// Get GPIO1_IN state of VehicleBoard
// Input:   u8  high
// Output:  bool *ok
// Return:  u8  state (Bit_SET:HIGH, Bit_RESET:low)
// Engineer: Quyen Leba
//------------------------------------------------------------------------------
u8 adc_peek_gpio2_in(bool *ok)
{
    MBLEXEC_HWCONFIG hwconfig;
    u16 bufferlength;
    u8  state;
    u8  status;

    state = Bit_RESET;
    status = mblexec_call(MBLEXEC_OPCODE_GET_HWCONFIG,NULL,0,
                          (u8*)&hwconfig,&bufferlength);
    if (status == S_SUCCESS)
    {
        if (hwconfig.gpio2_in)
        {
            state = Bit_SET;
        }
        *ok = TRUE;
    }
    else
    {
        *ok = FALSE;
    }
    
    return state;
}
