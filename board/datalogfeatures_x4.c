/**
  **************** (C) COPYRIGHT 2011 SCT Performance, LLC *********************
  * File Name          : datalogfeatures_x4.c
  * Device / System    : SCT products using the "CommonCode" platform
  * Author             : Quyen Leba
  *
  * Version            : 1
  * Date               : 05/02/2011
  * Description        :
  *                    :
  *
  *
  * History            : 05/02/2011 M. Davis
  *                    :   Add SCT Copyright header,
  *
  ******************************************************************************
  */


#include <board/genplatform.h>
#include "datalogfeatures.h"

#define ADC_AIN7_15V_MAX                15
#define ADC_AIN7_15V_MUL                1518
#define ADC_AIN7_15V_DIV                409500.0
#define ADC_AIN8_5V_MAX                 5
#define ADC_AIN8_5V_MUL                 506
#define ADC_AIN8_5V_DIV                 409500.0

//from datalogfeatures.c
extern const DlxBlock DefaultDatalogFeatureBlock_AIN7_15V;
extern const DlxBlock DefaultDatalogFeatureBlock_AIN8_5V;

//X4 has 2 analog inputs at Firewire port
#define DATALOGFEATURE_COLLECTION_COUNT         2
const DlxBlock *DefaultDatalogFeatureBlockCollection[DATALOGFEATURE_COLLECTION_COUNT] =
{
    &DefaultDatalogFeatureBlock_AIN7_15V,
    &DefaultDatalogFeatureBlock_AIN8_5V
};

/**
 *  datalogfeatures_getInputCount
 *  
 *  @brief Determine how many analog inputs available
 *
 *  @param[in] vbrev VB board revision
 *
 *  @retval u8 number of analog inputs
 *  
 *  @authors Quyen Leba
 */
u8 datalogfeatures_getInputCount(u8 vbrev)
{
    return DATALOGFEATURE_COLLECTION_COUNT;
}

/**
 *  datalogfeatures_getInputCollection
 *  
 *  @brief Get analog input collection by mb & vb revisions
 *
 *  @param[in] vbrev VB board revision
 *
 *  @retval DlxBlock**
 *  
 *  @authors Quyen Leba
 */
const DlxBlock** datalogfeatures_getInputCollection(u8 vbrev)
{
    return DefaultDatalogFeatureBlockCollection;
}

/**
 *  datalogfeatures_calc_ain_7
 *  
 *  @brief Calculate input voltage of AIN7
 *
 *  @param[in] adccount 4095 for 3.3V
 *
 *  @retval DlxBlock**
 *  
 *  @authors Quyen Leba
 */
float datalogfeatures_calc_ain_7(u32 adccount)
{
    return adccount*ADC_AIN7_15V_MUL/ADC_AIN7_15V_DIV;
}

/**
 *  datalogfeatures_calc_ain_8
 *  
 *  @brief Calculate input voltage of AIN8
 *
 *  @param[in] adccount 4095 for 3.3V
 *
 *  @retval DlxBlock**
 *  
 *  @authors Quyen Leba
 */
float datalogfeatures_calc_ain_8(u32 adccount)
{
    return adccount*ADC_AIN8_5V_MUL/ADC_AIN8_5V_DIV;
}
