/**
  **************** (C) COPYRIGHT 2011 SCT Performance, LLC *********************
  * File Name          : scp.h
  * Device / System    : SCT products using the "CommonCode" platform
  * Author             : Quyen Leba
  *
  * Version            : 1 
  * Date               : 05/02/2011
  * Description        : 
  *                    : 
  *
  *
  * History            : 05/02/2011 M. Davis
  *                    :   Add SCT Copyright header,
  *
  ******************************************************************************
  */

#ifndef __SCP_H
#define __SCP_H

#include <arch/gentype.h>
#include <common/obd2def.h>

#define SCP_FLAGS_FUNCTIONALADDR     (1<<0)

#define SCP_MODE_FUNCTIONAL         1
#define SCP_MODE_NODE               0

#define SCP_NOCRC                   0
#define SCP_CRC                     1

//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
#define SCP_FUNCADDRREQ             0x6A
#define SCP_FUNCADDRRSP             0x6B

#define SCP_FuncAddrReq             SCP_FUNCADDRREQ
#define SCP_FuncAddrRsp             SCP_FUNCADDRRSP
#define SCP_ECM_ID                  0x10
#define SCP_DEVICENODE_RESPONSE_ID  0xF0
#define SCP_KEYONFEPS_DEVICENODE_ID 0x05
#define SCP_MAX_TX_MSG_LENGTH       12  //TODOQ: check these numbers again
#define SCP_MAX_RX_MSG_LENGTH       12

typedef enum
{
    SCP_TYPE_24BIT,
    SCP_TYPE_32BIT,
}SCP_TYPE;

typedef enum
{
    SCP_LOW_SPEED,
    SCP_HIGH_SPEED,
}SCP_SPEED;

//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
void scp_init(SCP_TYPE scptype, SCP_SPEED scpspeed);
void scp_lock_ifr();
void scp_unlock_ifr();
u8 scp_tx(u8 *databuffer, u16 datalength, u8 mode);
u8 scp_rx(u8 *databuffer, u16 *datalength, u8 mode,
          u8 tester_frame_address, u8 tester_ifr_address, u32 timeout);
u8 scp_rx_complex(u8 *privdata, u32 privdatalength,
                  u8 *databuffer, u16 *datalength, u8 mode,
                  u8 tester_frame_address, u8 tester_ifr_address);
u8 scp_txrx_simple(u8 *tx_databuffer, u16 tx_datalength, u8 tx_mode,
                   u8 *rx_databuffer, u16 *rx_datalength, u8 rx_mode,
                   u8 tester_frame_address, u8 tester_ifr_address);

u8 scp_readblock_bycmd35single36single37(u32 address,
                                         MemoryAddressType addrtype,
                                         u16 length, u8 *buffer,
                                         u8 tester_frame_address,
                                         u8 tester_ifr_address);

#endif    //__SCP_H
