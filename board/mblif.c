/**
  **************** (C) COPYRIGHT 2011 SCT Performance, LLC *********************
  * File Name          : mblif.c
  * Device / System    : SCT products using the "CommonCode" platform
  * Author             : Quyen Leba
  *
  * Version            : 1 
  * Date               : 05/02/2011
  * Description        : 
  *                    : 
  *
  *
  * History            : 05/02/2011 M. Davis
  *                    :   Add SCT Copyright header,
  *
  ******************************************************************************
  */

#include <string.h>
#include <board/genplatform.h>
#include <board/delays.h>
#include <common/statuscode.h>
#include "mblif.h"
#include "mblif_intr.h"
#include "mblif_dma.h"

#define MAX_READBUFFER_SIZE         4096

//for now
//#define mblif_sendbyte(b)           spi_master_send_byte(b)
//#define mblif_receivebyte()         spi_master_receive_byte()
#define mblif_isbusy()              (gpio_read_dav_v_busy_pin() == GPIO_PIN_STATUS_HIGH)
u8 mblif_transferbyte(u8 txbyte, u8* recvbyte);

//------------------------------------------------------------------------------
// Private prototypes
//------------------------------------------------------------------------------
funcptr_mblif_link* mblif_link = NULL;
u8 mblif_set_mode(MBLIF_MODE mode);
u8 mblif_get_mode(MBLIF_MODE *mode);

//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
void mblif_init()
{
    u8 status;
    MBLIF_MODE mode;
    
    // On startup MBLIF is in interrupt mode by default to allow communication
    // with older vehicleboard versions and bootloader 
    mblif_link = &mblif_intr_link;
    
    // Check to see if DMA mblif mode is supported
    status = mblif_get_mode(&mode);
    if(status == S_SUCCESS)
    {
        status = mblif_set_mode(MBLIF_MODE_TYPE);
        if(status != S_SUCCESS)
        {
            // Stay in interrupt mode
            gpio_reset_vehicle_board();
            spi_master_moduleinitonly(FALSE);
            mblif_link = &mblif_intr_link;
        }
    }
    //else old vehicle board FW doesn't support DMA
    
    return;
}

//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
void mblif_force_interruptmode()
{
    gpio_reset_vehicle_board();
    spi_master_moduleinitonly(FALSE);
    mblif_link = &mblif_intr_link;
}

//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
u8 mblif_set_mode(MBLIF_MODE mode)
{
    u8 status;
    
    status = mblexec_call(MBLEXEC_OPCODE_SET_MBLIF_MODE, (u8*)&mode, sizeof(MBLIF_MODE),
                          NULL, NULL);
    return status;
}

//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
u8 mblif_get_mode(MBLIF_MODE *mode)
{
    u8 status;
    u16 length;
    
    status = mblexec_call(MBLEXEC_OPCODE_GET_MBLIF_MODE, NULL, NULL,
                          (u8*)mode, &length);
    return status;
}