/**
  **************** (C) COPYRIGHT 2011 SCT Performance, LLC *********************
  * File Name          : delays.c
  * Device / System    : SCT products using the "CommonCode" platform
  * Author             : Quyen Leba
  *
  * Version            : 1 
  * Date               : 05/02/2011
  * Description        : 
  *                    : 
  *
  *
  * History            : 05/02/2011 M. Davis
  *                    :   Add SCT Copyright header,
  *
  ******************************************************************************
  */

#include <board/delays.h>

vu32 TimingDelay = 0;

//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
void delays(u32 timeout, u8 prefix)
{
    if(prefix == 'm')
    {
        SysTick_Config(SystemCoreClock / 1000);
    }
    else
    {
        SysTick_Config(SystemCoreClock / 1000000);
    }

    TimingDelay = timeout;
    while(TimingDelay);
}

//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
void delays_counter(u32 count)
{
    volatile u32 i = 0;
    while(1)
    {
        if (i++ > count)
        {
            break;
        }
    }
}
