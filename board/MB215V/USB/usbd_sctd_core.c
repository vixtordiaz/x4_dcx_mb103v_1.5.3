/**
  ******************************************************************************
  * @file    usbd_sctd_core.c
  * @author  MCD Application Team
  * @version V1.0.0
  * @date    22-July-2011
  * @brief   This file provides all the SCTD core functions.
  *
  *
  ******************************************************************************
  * @attention
  *
  * THE PRESENT FIRMWARE WHICH IS FOR GUIDANCE ONLY AIMS AT PROVIDING CUSTOMERS
  * WITH CODING INFORMATION REGARDING THEIR PRODUCTS IN ORDER FOR THEM TO SAVE
  * TIME. AS A RESULT, STMICROELECTRONICS SHALL NOT BE HELD LIABLE FOR ANY
  * DIRECT, INDIRECT OR CONSEQUENTIAL DAMAGES WITH RESPECT TO ANY CLAIMS ARISING
  * FROM THE CONTENT OF SUCH FIRMWARE AND/OR THE USE MADE BY CUSTOMERS OF THE
  * CODING INFORMATION CONTAINED HEREIN IN CONNECTION WITH THEIR PRODUCTS.
  *
  * <h2><center>&copy; COPYRIGHT 2011 STMicroelectronics</center></h2>
  ******************************************************************************
  */ 

/* Includes ------------------------------------------------------------------*/
#include "usbd_sctd_core.h"
#include "usbd_req.h"
#include <common/usb_callback.h>
#include <device_config.h>

extern USB_CALLBACK_INFO usb_callback_info;

/** @addtogroup STM32_USB_OTG_DEVICE_LIBRARY
  * @{
  */


/** @defgroup SCTD_CORE 
  * @brief SCT Device core module
  * @{
  */ 

/** @defgroup SCTD_CORE_Private_TypesDefinitions
  * @{
  */ 
/**
  * @}
  */ 


/** @defgroup SCTD_CORE_Private_Defines
  * @{
  */ 

/**
  * @}
  */ 


/** @defgroup SCTD_CORE_Private_Macros
  * @{
  */ 
/**
  * @}
  */ 


/** @defgroup SCTD_CORE_Private_FunctionPrototypes
  * @{
  */ 
uint8_t  USBD_SCTD_Init (void  *pdev, 
                            uint8_t cfgidx);

uint8_t  USBD_SCTD_DeInit (void  *pdev, 
                              uint8_t cfgidx);

uint8_t  USBD_SCTD_Setup (void  *pdev, 
                             USB_SETUP_REQ *req);

uint8_t  USBD_SCTD_DataIn (void  *pdev, 
                              uint8_t epnum);


uint8_t  USBD_SCTD_DataOut (void  *pdev, 
                               uint8_t epnum);

uint8_t  *USBD_SCTD_GetCfgDesc (uint8_t speed, 
                                      uint16_t *length);

#ifdef USB_OTG_HS_CORE  
uint8_t  *USBD_SCTD_GetOtherCfgDesc (uint8_t speed, 
                                      uint16_t *length);
#endif


uint8_t USBD_SCTD_CfgDesc[USB_SCTD_CONFIG_DESC_SIZ];




/**
  * @}
  */ 


/** @defgroup SCTD_CORE_Private_Variables
  * @{
  */ 


USBD_Class_cb_TypeDef  USBD_SCTD_cb = 
{
  USBD_SCTD_Init,
  USBD_SCTD_DeInit,
  USBD_SCTD_Setup,
  NULL, /*EP0_TxSent*/  
  NULL, /*EP0_RxReady*/
  USBD_SCTD_DataIn,
  USBD_SCTD_DataOut,
  NULL, /*SOF */ 
  NULL,  
  NULL,     
  USBD_SCTD_GetCfgDesc,
#ifdef USB_OTG_HS_CORE  
  USBD_SCTD_GetOtherCfgDesc,
#endif
};

#ifdef USB_OTG_HS_INTERNAL_DMA_ENABLED
  #if defined ( __ICCARM__ ) /*!< IAR Compiler */
    #pragma data_alignment=4   
  #endif
#endif /* USB_OTG_HS_INTERNAL_DMA_ENABLED */
/* USB SCT device Configuration Descriptor */
/*   All Descriptors (Configuration, Interface, Endpoint, Class, Vendor */
__ALIGN_BEGIN uint8_t USBD_SCTD_CfgDesc[USB_SCTD_CONFIG_DESC_SIZ] __ALIGN_END =
{
  0x09,   /* bLength: Configuation Descriptor size */
  USB_DESC_TYPE_CONFIGURATION,   /* bDescriptorType: Configuration */
  USB_SCTD_CONFIG_DESC_SIZ,
  
  0x00,
  0x01,   /* bNumInterfaces: 1 interface */
  0x01,   /* bConfigurationValue: */
  0x04,   /* iConfiguration: */
  0xC0,   /* bmAttributes: */
  DEFAULT_USB_POWER,  /* see device_config.h */
  
  /********************  SCT Device interface ********************/
  0x09,   /* bLength: Interface Descriptor size */
  0x04,   /* bDescriptorType: */
  0x00,   /* bInterfaceNumber: Number of Interface */
  0x00,   /* bAlternateSetting: Alternate setting */
  0x03,   /* bNumEndpoints*/
  0xFF,   /* bInterfaceClass: */
  0xFF,   /* bInterfaceSubClass : */
  0xFF,   /* nInterfaceProtocol */
  0x05,          /* iInterface: */  //TODOQ: 0x05 or 0x04 here???
  /********************  SCT Device Endpoints ********************/
  0x07,   /*Endpoint descriptor length = 7*/
  0x05,   /*Endpoint descriptor type */
  SCTD_IN_EP,   /*Endpoint address (IN, address 1) */
  0x02,   /*Bulk endpoint type */
  LOBYTE(SCTD_MAX_PACKET),
  HIBYTE(SCTD_MAX_PACKET),
  0x00,   /*Polling interval in milliseconds */
  
  0x07,   /*Endpoint descriptor length = 7 */
  0x05,   /*Endpoint descriptor type */
  SCTD_OUT_EP,  /*Endpoint address (OUT, address 1) */
  0x02,   /*Bulk endpoint type */
  LOBYTE(SCTD_MAX_PACKET),
  HIBYTE(SCTD_MAX_PACKET),
  0x00,    /*Polling interval in milliseconds*/

  0x07,   /*Endpoint descriptor length = 7*/
  0x05,   /*Endpoint descriptor type */
  SCTD_IN_ALT_EP,   /*Endpoint address (IN, address 1) */
  0x02,   /*Bulk endpoint type */
  LOBYTE(SCTD_MAX_PACKET),
  HIBYTE(SCTD_MAX_PACKET),
  0x00,   /*Polling interval in milliseconds */
};
#ifdef USB_OTG_HS_CORE 
 #ifdef USB_OTG_HS_INTERNAL_DMA_ENABLED
   #if defined ( __ICCARM__ ) /*!< IAR Compiler */
     #pragma data_alignment=4   
   #endif
 #endif /* USB_OTG_HS_INTERNAL_DMA_ENABLED */
__ALIGN_BEGIN uint8_t USBD_SCTD_OtherCfgDesc[USB_SCTD_CONFIG_DESC_SIZ] __ALIGN_END =
{
  
  0x09,   /* bLength: Configuation Descriptor size */
  USB_DESC_TYPE_OTHER_SPEED_CONFIGURATION,   
  USB_SCTD_CONFIG_DESC_SIZ,
  
  0x00,
  0x01,   /* bNumInterfaces: 1 interface */
  0x01,   /* bConfigurationValue: */
  0x04,   /* iConfiguration: */
  0xC0,   /* bmAttributes: */
  DEFAULT_USB_POWER,  /* see device_config.h */
  
  /********************  SCT Device interface ********************/
  0x09,   /* bLength: Interface Descriptor size */
  0x04,   /* bDescriptorType: */
  0x00,   /* bInterfaceNumber: Number of Interface */
  0x00,   /* bAlternateSetting: Alternate setting */
  0x02,   /* bNumEndpoints*/
  0xFF,   /* bInterfaceClass: */
  0xFF,   /* bInterfaceSubClass : */
  0xFF,   /* nInterfaceProtocol */
  0x05,          /* iInterface: */  //TODOQ: 0x05 or 0x04 ???
  /********************  SCT Device Endpoints ********************/
  0x07,   /*Endpoint descriptor length = 7*/
  0x05,   /*Endpoint descriptor type */
  SCTD_IN_EP,   /*Endpoint address (IN, address 1) */
  0x02,   /*Bulk endpoint type */
  0x40,
  0x00,
  0x00,   /*Polling interval in milliseconds */
  
  0x07,   /*Endpoint descriptor length = 7 */
  0x05,   /*Endpoint descriptor type */
  SCTD_OUT_EP,  /*Endpoint address (OUT, address 1) */
  0x02,   /*Bulk endpoint type */
  0x40,
  0x00,
  0x00     /*Polling interval in milliseconds*/
};
#endif 

#ifdef USB_OTG_HS_INTERNAL_DMA_ENABLED
  #if defined ( __ICCARM__ ) /*!< IAR Compiler */
    #pragma data_alignment=4   
  #endif
#endif /* USB_OTG_HS_INTERNAL_DMA_ENABLED */
//__ALIGN_BEGIN static uint8_t  USBD_MSC_MaxLun  __ALIGN_END = 0;   //TODOQ:

#ifdef USB_OTG_HS_INTERNAL_DMA_ENABLED
  #if defined ( __ICCARM__ ) /*!< IAR Compiler */
    #pragma data_alignment=4   
  #endif
#endif /* USB_OTG_HS_INTERNAL_DMA_ENABLED */
__ALIGN_BEGIN static uint8_t  USBD_SCTD_AltSet  __ALIGN_END = 0;

/**
  * @}
  */ 


/** @defgroup SCTD_CORE_Private_Functions
  * @{
  */ 

/**
* @brief  USBD_SCTD_Init
*         Initialize  the SCT Device configuration
* @param  pdev: device instance
* @param  cfgidx: configuration index
* @retval status
*/
uint8_t  USBD_SCTD_Init (void  *pdev, 
                            uint8_t cfgidx)
{
  USBD_SCTD_DeInit(pdev , cfgidx );
  
  /* Open EP IN */
  DCD_EP_Open(pdev,
              SCTD_IN_EP,
              SCTD_EPIN_SIZE,
              USB_OTG_EP_BULK);
  
  /* Open EP OUT */
  DCD_EP_Open(pdev,
              SCTD_OUT_EP,
              SCTD_EPOUT_SIZE,
              USB_OTG_EP_BULK);
  
  DCD_EP_Open(pdev,
              SCTD_IN_ALT_EP,
              SCTD_EPIN_SIZE,
              USB_OTG_EP_BULK);

  /* Init the BOT  layer */
  //  MSC_BOT_Init(pdev);     //TODOQ:
  usb_callback_info.cbw.id = CB_CBW_ID;
  SCTUSB_SetFlowControl(CB_FC_IDLE);

  DCD_EP_Flush(pdev, SCTD_OUT_EP);
  DCD_EP_Flush(pdev, SCTD_IN_EP);
  DCD_EP_Flush(pdev, SCTD_IN_ALT_EP);
  DCD_EP_PrepareRx(pdev,SCTD_OUT_EP,(uint8_t *)usb_callback_info.bulkbuffer,CB_CSW_LENGTH);
  
  return USBD_OK;
}

/**
* @brief  USBD_SCTD_DeInit
*         DeInitilaize  the SCT Device configuration
* @param  pdev: device instance
* @param  cfgidx: configuration index
* @retval status
*/
uint8_t  USBD_SCTD_DeInit (void  *pdev, 
                              uint8_t cfgidx)
{
  /* Close MSC EPs */
  DCD_EP_Close (pdev , SCTD_IN_EP);
  DCD_EP_Close (pdev , SCTD_OUT_EP);
  DCD_EP_Close (pdev , SCTD_IN_ALT_EP);
  
  /* Un Init the BOT layer */
  //MSC_BOT_DeInit(pdev);       //TODOQ:
  SCTUSB_SetFlowControl(CB_FC_IDLE);
  return USBD_OK;
}
/**
* @brief  USBD_SCTD_Setup
*         Handle the MSC specific requests
* @param  pdev: device instance
* @param  req: USB request
* @retval status
*/
uint8_t  USBD_SCTD_Setup (void  *pdev, USB_SETUP_REQ *req)
{
  
  switch (req->bmRequest & USB_REQ_TYPE_MASK)
  {

  /* Class request */
  case USB_REQ_TYPE_CLASS :
      //TODOQ: currently we don't use this anyway
//    switch (req->bRequest)
//    {
//    case BOT_GET_MAX_LUN :  //TODOQ: fix this crap
//
//      if((req->wValue  == 0) && 
//         (req->wLength == 1) &&
//         ((req->bmRequest & 0x80) == 0x80))
//      {
//        USBD_MSC_MaxLun = USBD_STORAGE_fops->GetMaxLun();
//        if(USBD_MSC_MaxLun > 0)
//        {
//           USBD_CtlSendData (pdev,
//                             &USBD_MSC_MaxLun,
//                              1);
//        }
//        else
//        {
//          USBD_CtlError(pdev , req);
//          return USBD_FAIL; 
//          
//        }
//      }
//      else
//      {
//         USBD_CtlError(pdev , req);
//         return USBD_FAIL; 
//      }
//      break;
//      
//    case BOT_RESET :
//      if((req->wValue  == 0) && 
//         (req->wLength == 0) &&
//        ((req->bmRequest & 0x80) != 0x80))
//      {      
//         MSC_BOT_Reset(pdev);
//      }
//      else
//      {
//         USBD_CtlError(pdev , req);
//         return USBD_FAIL; 
//      }
//      break;
//
//    default:
//       USBD_CtlError(pdev , req);
//       return USBD_FAIL; 
//    }
//    break;
  /* Interface & Endpoint request */
  case USB_REQ_TYPE_STANDARD:
    switch (req->bRequest)
    {
    case USB_REQ_GET_INTERFACE :
      USBD_CtlSendData (pdev,
                        &USBD_SCTD_AltSet,
                        1);
      break;
      
    case USB_REQ_SET_INTERFACE :
      USBD_SCTD_AltSet = (uint8_t)(req->wValue);
      break;
    
    case USB_REQ_CLEAR_FEATURE:  
      
      /* Flush the FIFO and Clear the stall status */    
      DCD_EP_Flush(pdev, (uint8_t)req->wIndex);
      
      /* Re-activate the EP */      
      DCD_EP_Close (pdev , (uint8_t)req->wIndex);
      if((((uint8_t)req->wIndex) & 0x80) == 0x80)
      {
        DCD_EP_Open(pdev,
                    ((uint8_t)req->wIndex),
                    SCTD_EPIN_SIZE,
                    USB_OTG_EP_BULK);
      }
      else
      {
        DCD_EP_Open(pdev,
                    ((uint8_t)req->wIndex),
                    SCTD_EPOUT_SIZE,
                    USB_OTG_EP_BULK);
      }
      
      /* Handle BOT error */
      //MSC_BOT_CplClrFeature(pdev, (uint8_t)req->wIndex);  //TODOQ:
      break;
      
    }  
    break;
   
  default:
    break;
  }
  return USBD_OK;
}

/**
* @brief  USBD_SCTD_DataIn
*         handle data IN Stage
* @param  pdev: device instance
* @param  epnum: endpoint index
* @retval status
*/
uint8_t  USBD_SCTD_DataIn (void  *pdev, uint8_t epnum)
{
    usb_callback_ep1in(pdev,epnum);
    return USBD_OK;
}

/**
* @brief  USBD_SCTD_DataOut
*         handle data OUT Stage
* @param  pdev: device instance
* @param  epnum: endpoint index
* @retval status
*/
uint8_t  USBD_SCTD_DataOut (void  *pdev, uint8_t epnum)
{
    usb_callback_ep2out(pdev , epnum);
    return USBD_OK;
}

/**
* @brief  USBD_SCTD_GetCfgDesc 
*         return configuration descriptor
* @param  speed : current device speed
* @param  length : pointer data length
* @retval pointer to descriptor buffer
*/
uint8_t  *USBD_SCTD_GetCfgDesc (uint8_t speed, uint16_t *length)
{
  *length = sizeof (USBD_SCTD_CfgDesc);
  return USBD_SCTD_CfgDesc;
}

/**
* @brief  USBD_SCTD_GetOtherCfgDesc 
*         return other speed configuration descriptor
* @param  speed : current device speed
* @param  length : pointer data length
* @retval pointer to descriptor buffer
*/
#ifdef USB_OTG_HS_CORE  
uint8_t  *USBD_SCTD_GetOtherCfgDesc (uint8_t speed, 
                                      uint16_t *length)
{
  *length = sizeof (USBD_SCTD_OtherCfgDesc);
  return USBD_SCTD_OtherCfgDesc;
}
#endif
/**
  * @}
  */ 


/**
  * @}
  */ 


/**
  * @}
  */ 

/******************* (C) COPYRIGHT 2011 STMicroelectronics *****END OF FILE****/
