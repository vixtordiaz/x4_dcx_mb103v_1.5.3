/**
  **************** (C) COPYRIGHT 2011 SCT Performance, LLC *********************
  * File Name          : nand.c
  * Device / System    : SCT products using the "CommonCode" platform
  * Author             : Quyen Leba
  *
  * Version            : 1 
  * Date               : 05/02/2011
  * Description        : 
  *                    : 
  *
  *
  * History            : 05/02/2011 M. Davis
  *                    :   Add SCT Copyright header,
  *
  ******************************************************************************
  */

#include <stm32f10x_rcc.h>
#include <stm32f10x_type.h>
#include <stm32f10x_gpio.h>
#include <stm32f10x_fsmc.h>
#include "nand.h"

#define FSMC_Bank_NAND     FSMC_Bank2_NAND
#define Bank_NAND_ADDR     Bank2_NAND_ADDR 
#define Bank2_NAND_ADDR    ((u32)0x70000000)

//------------------------------------------------------------------------------
// Private macro
//------------------------------------------------------------------------------
#define ROW_ADDRESS \
    (Address.Page + (Address.Block + (Address.Zone * NAND_ZONE_SIZE)) * \
        NAND_BLOCK_SIZE)


//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
void nand_init()
{
    RCC_AHBPeriphClockCmd(RCC_AHBPeriph_FSMC, ENABLE);
    
    GPIO_InitTypeDef GPIO_InitStructure; 
    FSMC_NANDInitTypeDef FSMC_NANDInitStructure;
    FSMC_NAND_PCCARDTimingInitTypeDef  p;
    
    RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOD | RCC_APB2Periph_GPIOE | 
                           RCC_APB2Periph_GPIOF | RCC_APB2Periph_GPIOG, ENABLE);
    
    // GPIO Configuration
    // CLE, ALE, D0->D3, NOE, NWE and NCE2  NAND pin configuration
    GPIO_InitStructure.GPIO_Pin = 
        GPIO_Pin_11 | GPIO_Pin_12 | GPIO_Pin_14 | GPIO_Pin_15 | GPIO_Pin_0 |
            GPIO_Pin_1 | GPIO_Pin_4 | GPIO_Pin_5 | GPIO_Pin_7;                                  
    GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
    GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AF_PP;
    
    GPIO_Init(GPIOD, &GPIO_InitStructure); 
    
    // D4->D7 NAND pin configuration
    GPIO_InitStructure.GPIO_Pin =
        GPIO_Pin_7 | GPIO_Pin_8 | GPIO_Pin_9 | GPIO_Pin_10;
    
    GPIO_Init(GPIOE, &GPIO_InitStructure);
    
    // NWAIT NAND pin configuration
    GPIO_InitStructure.GPIO_Pin = GPIO_Pin_6;   							 
    GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
    GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IPU;
    
    GPIO_Init(GPIOD, &GPIO_InitStructure); 
    
    // FSMC Configuration
    p.FSMC_SetupTime = 0x3;
    p.FSMC_WaitSetupTime = 0x3;
    p.FSMC_HoldSetupTime = 0x2;
    p.FSMC_HiZSetupTime = 0x1;
    
    FSMC_NANDInitStructure.FSMC_Bank = FSMC_Bank2_NAND;
    FSMC_NANDInitStructure.FSMC_Waitfeature = FSMC_Waitfeature_Enable;
    FSMC_NANDInitStructure.FSMC_MemoryDataWidth = FSMC_MemoryDataWidth_8b;
    FSMC_NANDInitStructure.FSMC_ECC = FSMC_ECC_Enable;
    FSMC_NANDInitStructure.FSMC_ECCPageSize = FSMC_ECCPageSize_2048Bytes;
    FSMC_NANDInitStructure.FSMC_AddressLowMapping = FSMC_AddressLowMapping_Direct;
    FSMC_NANDInitStructure.FSMC_TCLRSetupTime = 0x00;
    FSMC_NANDInitStructure.FSMC_TARSetupTime = 0x00;
    FSMC_NANDInitStructure.FSMC_CommonSpaceTimingStruct = &p;
    FSMC_NANDInitStructure.FSMC_AttributeSpaceTimingStruct = &p;
    
    FSMC_NANDInit(&FSMC_NANDInitStructure);
    FSMC_NANDCmd(FSMC_Bank2_NAND, ENABLE);
    
    // Enable the ECC check hardware and set ECC page size
    //FSMC_NANDECCCmd(FSMC_Bank2_NAND, ENABLE);//->PCR2 |= (u32)0x00000040; //PCR_ECCEN_Set
    //FSMC_Bank2->PCR2 |= (u32)0x011 << 17; //2048 bytes
}

/******************************************************************************
* Function Name  : FSMC_NAND_ReadID
* Description    : Reads NAND memory's ID.
* Input          : - NAND_ID: pointer to a NAND_IDTypeDef structure which will hold
*                    the Manufacturer and Device ID.
* Output         : None
* Return         : None
*******************************************************************************/
void nand_readid(NAND_IDTypeDef* NAND_ID)
{
    u32 data = 0;
    
    /* Send Command to the command area */ 	
    *(vu8 *)(Bank_NAND_ADDR | CMD_AREA) = 0x90;
    *(vu8 *)(Bank_NAND_ADDR | ADDR_AREA) = 0x00;
    
    /* Sequence to read ID from NAND flash */	
    data = *(vu32 *)(Bank_NAND_ADDR | DATA_AREA);
    
    NAND_ID->Maker_ID   = ADDR_1st_CYCLE (data);
    NAND_ID->Device_ID  = ADDR_2nd_CYCLE (data);
    NAND_ID->Third_ID   = ADDR_3rd_CYCLE (data);
    NAND_ID->Fourth_ID  = ADDR_4th_CYCLE (data);  
}

/******************************************************************************
* Function Name  : FSMC_NAND_WriteSmallPage
* Description    : This routine is for writing one or several 512 Bytes Page size.
* Input          : - pBuffer: pointer on the Buffer containing data to be written   
*                  - Address: First page address
*                  - NumPageToWrite: Number of page to write  
* Output         : None
* Return         : New status of the NAND operation. This parameter can be:
*                   - NAND_TIMEOUT_ERROR: when the previous operation generate 
*                     a Timeout error
*                   - NAND_READY: when memory is ready for the next operation 
*                  And the new status of the increment address operation. It can be:
*                  - NAND_VALID_ADDRESS: When the new address is valid address
*                  - NAND_INVALID_ADDRESS: When the new address is invalid address
*******************************************************************************/
u32 nand_writeSmallPage(u8 *pBuffer, NAND_ADDRESS Address, u32 NumPageToWrite)
{
    u32 index = 0x00, numpagewritten = 0x00, addressstatus = NAND_VALID_ADDRESS;
    u32 status = NAND_READY, size = 0x00;
    
    while((NumPageToWrite != 0x00) && (addressstatus == NAND_VALID_ADDRESS) && (status == NAND_READY))
    {
        /* Page write command and address */
        *(vu8 *)(Bank_NAND_ADDR | CMD_AREA) = NAND_CMD_AREA_A;
        *(vu8 *)(Bank_NAND_ADDR | CMD_AREA) = NAND_CMD_WRITE0;
        
        *(vu8 *)(Bank_NAND_ADDR | ADDR_AREA) = 0x00;  
        *(vu8 *)(Bank_NAND_ADDR | ADDR_AREA) = ADDR_1st_CYCLE(ROW_ADDRESS);  
        *(vu8 *)(Bank_NAND_ADDR | ADDR_AREA) = ADDR_2nd_CYCLE(ROW_ADDRESS);  
        *(vu8 *)(Bank_NAND_ADDR | ADDR_AREA) = ADDR_3rd_CYCLE(ROW_ADDRESS);  
        
        /* Calculate the size */
        size = NAND_PAGE_SIZE + (NAND_PAGE_SIZE * numpagewritten);
        
        /* Write data */
        for(; index < size; index++)
        {
            *(vu8 *)(Bank_NAND_ADDR | DATA_AREA) = pBuffer[index];
        }
        
        *(vu8 *)(Bank_NAND_ADDR | CMD_AREA) = NAND_CMD_WRITE_TRUE1;
        
        /* Check status for successful operation */
        status = nand_getStatus();
        
        if(status == NAND_READY)
        {
            numpagewritten++;
            
            NumPageToWrite--;
            
            /* Calculate Next small page Address */
            addressstatus = nand_addressIncrement(&Address);    
        }    
    }
    
    return (status | addressstatus);
}

/******************************************************************************
* Function Name  : FSMC_NAND_ReadSmallPage
* Description    : This routine is for sequential read from one or several 
*                  512 Bytes Page size.
* Input          : - pBuffer: pointer on the Buffer to fill  
*                  - Address: First page address
*                  - NumPageToRead: Number of page to read
* Output         : None
* Return         : New status of the NAND operation. This parameter can be:
*                   - NAND_TIMEOUT_ERROR: when the previous operation generate 
*                     a Timeout error
*                   - NAND_READY: when memory is ready for the next operation 
*                  And the new status of the increment address operation. It can be:
*                  - NAND_VALID_ADDRESS: When the new address is valid address
*                  - NAND_INVALID_ADDRESS: When the new address is invalid address
*******************************************************************************/
u32 nand_readSmallPage(u8 *pBuffer, NAND_ADDRESS Address, u32 NumPageToRead)
{
    u32 index = 0x00, numpageread = 0x00, addressstatus = NAND_VALID_ADDRESS;
    u32 status = NAND_READY, size = 0x00;
    
    while((NumPageToRead != 0x0) && (addressstatus == NAND_VALID_ADDRESS))
    {	   
        /* Page Read command and page address */
        *(vu8 *)(Bank_NAND_ADDR | CMD_AREA) = NAND_CMD_AREA_A; 
        
        *(vu8 *)(Bank_NAND_ADDR | ADDR_AREA) = 0x00; 
        *(vu8 *)(Bank_NAND_ADDR | ADDR_AREA) = ADDR_1st_CYCLE(ROW_ADDRESS); 
        *(vu8 *)(Bank_NAND_ADDR | ADDR_AREA) = ADDR_2nd_CYCLE(ROW_ADDRESS); 
        *(vu8 *)(Bank_NAND_ADDR | ADDR_AREA) = ADDR_3rd_CYCLE(ROW_ADDRESS); 
        
        *(vu8 *)(Bank_NAND_ADDR | CMD_AREA) = NAND_CMD_AREA_TRUE1; 
        
        /* Calculate the size */
        size = NAND_PAGE_SIZE + (NAND_PAGE_SIZE * numpageread);
        
        /* Get Data into Buffer */    
        for(; index < size; index++)
        {
            pBuffer[index]= *(vu8 *)(Bank_NAND_ADDR | DATA_AREA);
        }
        
        numpageread++;
        
        NumPageToRead--;
        
        /* Calculate page address */           			 
        addressstatus = nand_addressIncrement(&Address);
    }
    
    status = nand_getStatus();
    
    return (status | addressstatus);
}

/******************************************************************************
* Function Name  : FSMC_NAND_WriteSpareArea
* Description    : This routine write the spare area information for the specified 
*                  pages addresses.
* Input          : - pBuffer: pointer on the Buffer containing data to be written 
*                  - Address: First page address
*                  - NumSpareAreaTowrite: Number of Spare Area to write
* Output         : None
* Return         : New status of the NAND operation. This parameter can be:
*                   - NAND_TIMEOUT_ERROR: when the previous operation generate 
*                     a Timeout error
*                   - NAND_READY: when memory is ready for the next operation 
*                  And the new status of the increment address operation. It can be:
*                  - NAND_VALID_ADDRESS: When the new address is valid address
*                  - NAND_INVALID_ADDRESS: When the new address is invalid address
*******************************************************************************/
u32 nand_writeSpareArea(u8 *pBuffer, NAND_ADDRESS Address, u32 NumSpareAreaTowrite)
{
    u32 index = 0x00, numsparesreawritten = 0x00, addressstatus = NAND_VALID_ADDRESS;
    u32 status = NAND_READY, size = 0x00; 
    
    while((NumSpareAreaTowrite != 0x00) && (addressstatus == NAND_VALID_ADDRESS) && (status == NAND_READY))
    {
        /* Page write Spare area command and address */
        *(vu8 *)(Bank_NAND_ADDR | CMD_AREA) = NAND_CMD_AREA_C;
        *(vu8 *)(Bank_NAND_ADDR | CMD_AREA) = NAND_CMD_WRITE0;
        
        *(vu8 *)(Bank_NAND_ADDR | ADDR_AREA) = 0x00; 
        *(vu8 *)(Bank_NAND_ADDR | ADDR_AREA) = ADDR_1st_CYCLE(ROW_ADDRESS); 
        *(vu8 *)(Bank_NAND_ADDR | ADDR_AREA) = ADDR_2nd_CYCLE(ROW_ADDRESS); 
        *(vu8 *)(Bank_NAND_ADDR | ADDR_AREA) = ADDR_3rd_CYCLE(ROW_ADDRESS); 
        
        /* Calculate the size */ 
        size = NAND_SPARE_AREA_SIZE + (NAND_SPARE_AREA_SIZE * numsparesreawritten);
        
        /* Write the data */ 
        for(; index < size; index++)
        {
            *(vu8 *)(Bank_NAND_ADDR | DATA_AREA) = pBuffer[index];
        }
        
        *(vu8 *)(Bank_NAND_ADDR | CMD_AREA) = NAND_CMD_WRITE_TRUE1;
        
        /* Check status for successful operation */
        status = nand_getStatus();
        
        if(status == NAND_READY)
        {
            numsparesreawritten++;      
            
            NumSpareAreaTowrite--;  
            
            /* Calculate Next page Address */
            addressstatus = nand_addressIncrement(&Address);
        }       
    }
    
    return (status | addressstatus);
}

/******************************************************************************
* Function Name  : FSMC_NAND_ReadSpareArea
* Description    : This routine read the spare area information from the specified
*                  pages addresses.
* Input          : - pBuffer: pointer on the Buffer to fill  
*                  - Address: First page address
*                  - NumSpareAreaToRead: Number of Spare Area to read
* Output         : None
* Return         : New status of the NAND operation. This parameter can be:
*                   - NAND_TIMEOUT_ERROR: when the previous operation generate 
*                     a Timeout error
*                   - NAND_READY: when memory is ready for the next operation 
*                  And the new status of the increment address operation. It can be:
*                  - NAND_VALID_ADDRESS: When the new address is valid address
*                  - NAND_INVALID_ADDRESS: When the new address is invalid address
*******************************************************************************/
u32 nand_readSpareArea(u8 *pBuffer, NAND_ADDRESS Address, u32 NumSpareAreaToRead)
{
    u32 numsparearearead = 0x00, index = 0x00, addressstatus = NAND_VALID_ADDRESS;
    u32 status = NAND_READY, size = 0x00;
    
    while((NumSpareAreaToRead != 0x0) && (addressstatus == NAND_VALID_ADDRESS))
    {     
        /* Page Read command and page address */     
        *(vu8 *)(Bank_NAND_ADDR | CMD_AREA) = NAND_CMD_AREA_C;
        
        *(vu8 *)(Bank_NAND_ADDR | ADDR_AREA) = 0x00; 
        *(vu8 *)(Bank_NAND_ADDR | ADDR_AREA) = ADDR_1st_CYCLE(ROW_ADDRESS);     
        *(vu8 *)(Bank_NAND_ADDR | ADDR_AREA) = ADDR_2nd_CYCLE(ROW_ADDRESS);     
        *(vu8 *)(Bank_NAND_ADDR | ADDR_AREA) = ADDR_3rd_CYCLE(ROW_ADDRESS);    
        
        *(vu8 *)(Bank_NAND_ADDR | CMD_AREA) = NAND_CMD_AREA_TRUE1;
        
        /* Data Read */
        size = NAND_SPARE_AREA_SIZE +  (NAND_SPARE_AREA_SIZE * numsparearearead);
        
        /* Get Data into Buffer */
        for ( ;index < size; index++)
        {
            pBuffer[index] = *(vu8 *)(Bank_NAND_ADDR | DATA_AREA);
        }
        
        numsparearearead++;
        
        NumSpareAreaToRead--;
        
        /* Calculate page address */           			 
        addressstatus = nand_addressIncrement(&Address);
    }
    
    status = nand_getStatus();
    
    return (status | addressstatus);
}

/******************************************************************************
* Function Name  : FSMC_NAND_EraseBlock
* Description    : This routine erase complete block from NAND FLASH
* Input          : - Address: Any address into block to be erased
* Output         : None
* Return         : New status of the NAND operation. This parameter can be:
*                   - NAND_TIMEOUT_ERROR: when the previous operation generate 
*                     a Timeout error
*                   - NAND_READY: when memory is ready for the next operation 
*******************************************************************************/
u32 nand_eraseBlock(NAND_ADDRESS Address)
{
    *(vu8 *)(Bank_NAND_ADDR | CMD_AREA) = NAND_CMD_ERASE0;
    
    *(vu8 *)(Bank_NAND_ADDR | ADDR_AREA) = ADDR_1st_CYCLE(ROW_ADDRESS);
    *(vu8 *)(Bank_NAND_ADDR | ADDR_AREA) = ADDR_2nd_CYCLE(ROW_ADDRESS);
    *(vu8 *)(Bank_NAND_ADDR | ADDR_AREA) = ADDR_3rd_CYCLE(ROW_ADDRESS);
    
    *(vu8 *)(Bank_NAND_ADDR | CMD_AREA) = NAND_CMD_ERASE1; 
    
    return (nand_getStatus());
}

/******************************************************************************
* Function Name  : FSMC_NAND_Reset
* Description    : This routine reset the NAND FLASH
* Input          : None
* Output         : None
* Return         : NAND_READY
*******************************************************************************/
u32 nand_reset()
{
    *(vu8 *)(Bank_NAND_ADDR | CMD_AREA) = NAND_CMD_RESET;
    
    return (NAND_READY);
}

/******************************************************************************
* Function Name  : FSMC_NAND_GetStatus
* Description    : Get the NAND operation status
* Input          : None
* Output         : None
* Return         : New status of the NAND operation. This parameter can be:
*                   - NAND_TIMEOUT_ERROR: when the previous operation generate 
*                     a Timeout error
*                   - NAND_READY: when memory is ready for the next operation    
*******************************************************************************/
u32 nand_getStatus()
{
    u32 timeout = 0x1000000, status = NAND_READY;
    
    status = nand_readStatus(); 
    
    /* Wait for a NAND operation to complete or a TIMEOUT to occur */
    while ((status != NAND_READY) &&( timeout != 0x00))
    {
        status = nand_readStatus();
        timeout --;      
    }
    
    if(timeout == 0x00)
    {          
        status =  NAND_TIMEOUT_ERROR;      
    } 
    
    /* Return the operation status */
    return (status);      
}
/******************************************************************************
* Function Name  : FSMC_NAND_ReadStatus
* Description    : Reads the NAND memory status using the Read status command 
* Input          : None
* Output         : None
* Return         : The status of the NAND memory. This parameter can be:
*                   - NAND_BUSY: when memory is busy
*                   - NAND_READY: when memory is ready for the next operation    
*                   - NAND_ERROR: when the previous operation gererates error   
*******************************************************************************/
u32 nand_readStatus()
{
    u32 data = 0x00, status = NAND_BUSY;
    
    /* Read status operation ------------------------------------ */
    *(vu8 *)(Bank_NAND_ADDR | CMD_AREA) = NAND_CMD_STATUS;
    data = *(vu8 *)(Bank_NAND_ADDR);
    
    if((data & NAND_ERROR) == NAND_ERROR)
    {
        status = NAND_ERROR;
    } 
    else if((data & NAND_READY) == NAND_READY)
    {
        status = NAND_READY;
    }
    else
    {
        status = NAND_BUSY; 
    }
    
    return (status);
}

/******************************************************************************
* Function Name  : NAND_AddressIncrement
* Description    : Increment the NAND memory address
* Input          : - Address: address to be incremented.
* Output         : None
* Return         : The new status of the increment address operation. It can be:
*                  - NAND_VALID_ADDRESS: When the new address is valid address
*                  - NAND_INVALID_ADDRESS: When the new address is invalid address
*******************************************************************************/
u32 nand_addressIncrement(NAND_ADDRESS* Address)
{
    u32 status = NAND_VALID_ADDRESS;
    
    Address->Page++;
    
    if(Address->Page == NAND_BLOCK_SIZE)
    {
        Address->Page = 0;
        Address->Block++;
        
        if(Address->Block == NAND_ZONE_SIZE)
        {
            Address->Block = 0;
            Address->Zone++;
            
            if(Address->Zone == NAND_MAX_ZONE)
            {
                status = NAND_INVALID_ADDRESS;
            }
        }
    } 
    
    return (status);
}
