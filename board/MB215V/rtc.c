/**
  **************** (C) COPYRIGHT 2011 SCT Performance, LLC *********************
  * File Name          : rtc.c
  * Device / System    : SCT products using the "CommonCode" platform
  * Author             : Quyen Leba
  *
  * Version            : 1 
  * Date               : 05/02/2011
  * Description        : 
  *                    : 
  *
  *
  * History            : 05/02/2011 M. Davis
  *                    :   Add SCT Copyright header,
  *
  ******************************************************************************
  */

#include <board/rtc.h>

#define RTC_PRESCALE_VAULE          (125-1)

vu32 rtc_sec = 0;
vu32 rtc_msec = 0;

//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
void rtc_init()
{
    RTC_InitTypeDef   RTC_InitStructure;
    RTC_TimeTypeDef   RTC_TimeStructure;
    RTC_DateTypeDef   RTC_DateStructure;
    
    // Enable PWR and BKP clocks
    RCC_APB1PeriphClockCmd(RCC_APB1Periph_PWR, ENABLE);
    // Allow access to BKP Domain
    PWR_BackupAccessCmd(ENABLE);

    // Enable the LSI OSC
    RCC_LSICmd(ENABLE);
    // Wait till LSI is ready
    while(RCC_GetFlagStatus(RCC_FLAG_LSIRDY) == RESET);

    // Select the RTC Clock Source
    RCC_RTCCLKConfig(RCC_RTCCLKSource_LSI);
#define __RTC_SynchPrediv       0xFF
#define __RTC_AsynchPrediv      0x7F

    // Enable RTC Clock
    RCC_RTCCLKCmd(ENABLE);
    
    // Wait for RTC registers synchronization
    RTC_WaitForSynchro();
    
    // Write to the first RTC Backup Data Register
    RTC_WriteBackupRegister(RTC_BKP_DR0, 0x32F2);

    // Set the Time
    RTC_TimeStructure.RTC_Hours   = 0;
    RTC_TimeStructure.RTC_Minutes = 0;
    RTC_TimeStructure.RTC_Seconds = 0;
    
    // Set the Date
    RTC_DateStructure.RTC_Month = 2;
    RTC_DateStructure.RTC_Date = 16;
    RTC_DateStructure.RTC_Year = 11; 
    RTC_DateStructure.RTC_WeekDay = RTC_Weekday_Wednesday; 

    // Calendar Configuration
    RTC_InitStructure.RTC_AsynchPrediv = __RTC_AsynchPrediv;
    RTC_InitStructure.RTC_SynchPrediv =  __RTC_SynchPrediv;
    RTC_InitStructure.RTC_HourFormat = RTC_HourFormat_24;
    RTC_Init(&RTC_InitStructure);

    RTC_SetDate(RTC_Format_BIN,&RTC_DateStructure);
    RTC_SetTime(RTC_Format_BIN,&RTC_TimeStructure);
}

//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
void rtc_reset()
{
    RTC_TimeTypeDef   RTC_TimeStructure;

    RTC_TimeStructure.RTC_Hours   = 0x00;
    RTC_TimeStructure.RTC_Minutes = 0x00;
    RTC_TimeStructure.RTC_Seconds = 0x00;

    // Wait for RTC registers synchronization
    RTC_WaitForSynchro();
    RTC_SetTime(RTC_Format_BIN,&RTC_TimeStructure);
}

//------------------------------------------------------------------------------
// Return:  u32 rtc_value_in_ms
//------------------------------------------------------------------------------
u32 rtc_getvalue()
{
    RTC_TimeTypeDef   RTC_TimeStructure;

    RTC_GetTime(RTC_Format_BIN,&RTC_TimeStructure);
    return ((RTC_TimeStructure.RTC_Hours * 3600) + (RTC_TimeStructure.RTC_Minutes * 60) + RTC_TimeStructure.RTC_Seconds)*1000;
}
