/**
  **************** (C) COPYRIGHT 2011 SCT Performance, LLC *********************
  * File Name          : i2c.c
  * Device / System    : SCT products using the "CommonCode" platform
  * Author             : Quyen Leba
  *
  * Version            : 1 
  * Date               : 05/02/2011
  * Description        : 
  *                    : 
  *
  *
  * History            : 05/02/2011 M. Davis
  *                    :   Add SCT Copyright header,
  *
  ******************************************************************************
  */

#include <string.h>
#include <arch/gentype.h>
#include <common/statuscode.h>
#include "misc.h"
#include "i2c.h"

#define I2C_NAME                I2C1
#define I2C_SPEED               100000
#define I2C_DEFAULT_TIMEOUT     ((u32)0x1000 * 10)

#define I2C_PORT                GPIOB
#define I2C_GPIO_AF             GPIO_AF_I2C1
#define I2C_SCL_PIN             GPIO_Pin_6
#define I2C_SCL_PIN_SOURCE      GPIO_PinSource6
#define I2C_SDA_PIN             GPIO_Pin_7
#define I2C_SDA_PIN_SOURCE      GPIO_PinSource7

struct
{
    u32 timeout;
}i2c_info;

//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
u8 i2c_init()
{
    I2C_InitTypeDef  I2C_InitStructure;
    GPIO_InitTypeDef GPIO_InitStructure;

    i2c_info.timeout = I2C_DEFAULT_TIMEOUT;

    // Enable clock
    I2C_DeInit(I2C_NAME);
    RCC_APB1PeriphClockCmd(RCC_APB1Periph_I2C1,ENABLE);

    GPIO_PinAFConfig(I2C_PORT, I2C_SCL_PIN_SOURCE, I2C_GPIO_AF);
    GPIO_PinAFConfig(I2C_PORT, I2C_SDA_PIN_SOURCE, I2C_GPIO_AF);

    GPIO_InitStructure.GPIO_Pin =  I2C_SCL_PIN | I2C_SDA_PIN;
    GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AF;
    GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
    GPIO_Init(I2C_PORT, &GPIO_InitStructure);

    // I2C configuration
    I2C_InitStructure.I2C_Mode = I2C_Mode_I2C;
    I2C_InitStructure.I2C_DutyCycle = I2C_DutyCycle_2;
    I2C_InitStructure.I2C_Ack = I2C_Ack_Enable;
    I2C_InitStructure.I2C_OwnAddress1 = 0x00;
    I2C_InitStructure.I2C_AcknowledgedAddress = I2C_AcknowledgedAddress_7bit;
    I2C_InitStructure.I2C_ClockSpeed = I2C_SPEED;
    
    I2C_Init(I2C_NAME, &I2C_InitStructure);
    I2C_Cmd(I2C_NAME, ENABLE);
    
    return S_SUCCESS;
}

//------------------------------------------------------------------------------
// I2C1 error interrupt handler
//------------------------------------------------------------------------------
void I2C1_ErIrqHandler(void)
{
    // Check on SMBALERT flag and clear it
    if (I2C_GetITStatus(I2C_NAME, I2C_IT_SMBALERT))
    {
        I2C_ClearITPendingBit(I2C_NAME, I2C_IT_SMBALERT);
        //SMbusAlertOccurred++;
    }
    // Check on Time out flag and clear it
    if (I2C_GetITStatus(I2C_NAME, I2C_IT_TIMEOUT))
    {
        I2C_ClearITPendingBit(I2C_NAME, I2C_IT_TIMEOUT);
    }
    // Check on Arbitration Lost flag and clear it
    if (I2C_GetITStatus(I2C_NAME, I2C_IT_ARLO))
    {
        I2C_ClearITPendingBit(I2C_NAME, I2C_IT_ARLO);
    }
    // Check on PEC error flag and clear it
    if (I2C_GetITStatus(I2C_NAME, I2C_IT_PECERR))
    {
        I2C_ClearITPendingBit(I2C_NAME, I2C_IT_PECERR);
    }
    // Check on Overrun/Underrun error flag and clear it
    if (I2C_GetITStatus(I2C_NAME, I2C_IT_OVR))
    {
        I2C_ClearITPendingBit(I2C_NAME, I2C_IT_OVR);
    } 
    // Check on Acknowledge failure error flag and clear it
    if (I2C_GetITStatus(I2C_NAME, I2C_IT_AF))
    {
        I2C_ClearITPendingBit(I2C_NAME, I2C_IT_AF);
    }
    // Check on Bus error flag and clear it
    if (I2C_GetITStatus(I2C_NAME, I2C_IT_BERR))
    {
        I2C_ClearITPendingBit(I2C_NAME, I2C_IT_BERR);
    }
}

//------------------------------------------------------------------------------
// I2C1 event interrupt handler
//------------------------------------------------------------------------------
void I2C1_EvIrqHandler(void)
{
}

//------------------------------------------------------------------------------
// Write bytes to I2C device
// Inputs:  u8  slaveaddr
//          u8  writeaddr
//          u8  *data
//          u32 datalength
// Return:  u8  status
// Engineer: Quyen Leba
//------------------------------------------------------------------------------
u8 i2c_writebytes(u8 slaveaddr, u8 writeaddr, u8 *data, u32 datalength)
{
    u8  *bptr;
    u32 bytecount;
    u32 timeout;
    
    // Test BUSY Flag
    timeout = i2c_info.timeout * 10;
    while (I2C_GetFlagStatus(I2C_NAME,I2C_FLAG_BUSY))
    {
        if((timeout--) == 0) return S_TIMEOUT;
    }

    // Enable the I2C peripheral
    I2C_AcknowledgeConfig(I2C_NAME,ENABLE);
    I2C_GenerateSTART(I2C_NAME, ENABLE);

    // Test SB Flag
    timeout = i2c_info.timeout;
    while (!I2C_GetFlagStatus(I2C_NAME,I2C_FLAG_SB))
    {
        if((timeout--) == 0) return S_TIMEOUT;
    }
    
    // Send device address for write
    I2C_Send7bitAddress(I2C_NAME, slaveaddr, I2C_Direction_Transmitter);

    // Test ADDR Flag
    timeout = i2c_info.timeout;
    while (!I2C_CheckEvent(I2C_NAME, I2C_EVENT_MASTER_TRANSMITTER_MODE_SELECTED)) 
    {
        if((timeout--) == 0) return S_TIMEOUT;
    }
    
    // Send the device's internal address to write to
    I2C_SendData(I2C_NAME, writeaddr);
    
    // Test TXE FLag (data sent)
    timeout = i2c_info.timeout;
    while ((!I2C_GetFlagStatus(I2C_NAME,I2C_FLAG_TXE)) && (!I2C_GetFlagStatus(I2C_NAME,I2C_FLAG_BTF)))  
    {
        if((timeout--) == 0) return S_TIMEOUT;
    }

    bytecount = 0;
    bptr = data;
    while(bytecount++ < datalength)
    {
        // Send data byte
        I2C_SendData(I2C_NAME, *bptr++);

        // Test TXE FLag (data sent)
        timeout = i2c_info.timeout;
        while ((!I2C_GetFlagStatus(I2C_NAME,I2C_FLAG_TXE)) && (!I2C_GetFlagStatus(I2C_NAME,I2C_FLAG_BTF)))  
        {
            if((timeout--) == 0) return S_TIMEOUT;
        }
    }
    I2C_GenerateSTOP(I2C_NAME,ENABLE);
    I2C_AcknowledgeConfig(I2C_NAME,DISABLE);

    return S_SUCCESS;  
}

//------------------------------------------------------------------------------
// Read bytes from I2C device
// Inputs:  u8  slaveaddr
//          u8  readaddr
//          u8  *data
//          u32 datalength
// Return:  u8  status
// Engineer: Quyen Leba
//------------------------------------------------------------------------------
u8 i2c_readbytes(u8 slaveaddr, u8 readaddr, u8 *data, u32 datalength)
{
    u8  *bptr;
    u32 bytecount;
    u32 timeout;
    
    // Test BUSY Flag
    timeout = i2c_info.timeout * 10;
    while (I2C_GetFlagStatus(I2C_NAME,I2C_FLAG_BUSY))
    {
        if((timeout--) == 0) return S_TIMEOUT;
    }

    // Enable the I2C peripheral
    I2C_AcknowledgeConfig(I2C_NAME,ENABLE);
    I2C_GenerateSTART(I2C_NAME, ENABLE);

    // Test SB Flag
    timeout = i2c_info.timeout;
    while (!I2C_GetFlagStatus(I2C_NAME,I2C_FLAG_SB))
    {
        if((timeout--) == 0) return S_TIMEOUT;
    }
    
    // Send device address for write
    I2C_Send7bitAddress(I2C_NAME, slaveaddr, I2C_Direction_Transmitter);

    // Test ADDR Flag
    timeout = i2c_info.timeout;
    while (!I2C_CheckEvent(I2C_NAME, I2C_EVENT_MASTER_TRANSMITTER_MODE_SELECTED)) 
    {
        if((timeout--) == 0) return S_TIMEOUT;
    }
    
    // Send the device's internal address to read from
    I2C_SendData(I2C_NAME, readaddr);
    
    // Test TXE FLag (data sent)
    timeout = i2c_info.timeout;
    while ((!I2C_GetFlagStatus(I2C_NAME,I2C_FLAG_TXE)) && (!I2C_GetFlagStatus(I2C_NAME,I2C_FLAG_BTF)))  
    {
        if((timeout--) == 0) return S_TIMEOUT;
    }
    
    // Send START condition a second time
    I2C_GenerateSTART(I2C_NAME, ENABLE);
    
    // Test SB Flag
    timeout = i2c_info.timeout;
    while (!I2C_GetFlagStatus(I2C_NAME,I2C_FLAG_SB)) 
    {
        if((timeout--) == 0) return S_TIMEOUT;
    }
    
    // Send address for read
    I2C_Send7bitAddress(I2C_NAME, slaveaddr, I2C_Direction_Receiver);
    
    // Test on ADDR Flag
    timeout = i2c_info.timeout;
    while (!I2C_CheckEvent(I2C_NAME, I2C_EVENT_MASTER_RECEIVER_MODE_SELECTED))   
    {
        if((timeout--) == 0) return S_TIMEOUT;
    }

    bytecount = 0;
    bptr = data;
    while(bytecount++ < datalength)
    {
        timeout = i2c_info.timeout;
        while (!I2C_GetFlagStatus(I2C_NAME,I2C_FLAG_RXNE)) 
        {
            if((timeout--) == 0) return S_TIMEOUT;
        }
        if (bytecount == datalength)
        {
            I2C_GenerateSTOP(I2C_NAME,ENABLE);
            I2C_AcknowledgeConfig(I2C_NAME,DISABLE);
        }
        *bptr++ = I2C_ReceiveData(I2C_NAME);
    }

    return S_SUCCESS;  
}
