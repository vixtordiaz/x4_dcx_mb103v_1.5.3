/**
  **************** (C) COPYRIGHT 2011 SCT Performance, LLC *********************
  * File Name          : gpio.h
  * Device / System    : SCT products using the "CommonCode" platform
  * Author             : Quyen Leba
  *
  * Version            : 1 
  * Date               : 05/02/2011
  * Description        : 
  *                    : 
  *
  *
  * History            : 05/02/2011 M. Davis
  *                    :   Add SCT Copyright header,
  *
  ******************************************************************************
  */

#ifndef __GPIO_H
#define __GPIO_H

#include <arch/gentype.h>

#define COMMLINK_LINK_BIT_MASK      1
#define COMMLINK_LINK_BIT_POS       0

#define COMMLINK_SESSION_BIT_MASK   2
#define COMMLINK_SESSION_BIT_POS    1

#define COMMLINK_MODE_BIT_MASK      8
#define COMMLINK_MODE_BIT_POS       3

typedef enum
{
    GPIO_PIN_STATUS_LOW             = 0,
    GPIO_PIN_STATUS_HIGH            = 1,
}GPIO_PIN_STATUS;

void gpio_init();
void gpio_dav_busy_pin_init();
void gpio_read_dav_v_busy_intr();
bool gpio_dav_v_busy_strobed();
GPIO_PIN_STATUS gpio_read_dav_v_busy_pin();
u8 gpio_read_dav_a_busy_pin();
u8 gpio_get_board_rev();
bool gpio_is_fullpower_available();
void gpio_use_fullpower_mode(bool yes);
void gpio_reset_app_board();
void gpio_reset_vehicle_board();
u32 gpio_peek_commlink_connection_status();
u8 gpio_force115kBaud_app_board(bool enable);
u8 gpio_peek_commlink_CTS_status();
void gpio_set_booten(bool state);
bool gpio_is_5vsw_powergood();
void gpio_usb_mux_status_pin_init(void);
void gpio_usb_mux_status_pin_intr(void);
void gpio_block_remote_ui(bool yes);
void gpio_enable_usb(bool enable);

#endif	//__GPIO_H
