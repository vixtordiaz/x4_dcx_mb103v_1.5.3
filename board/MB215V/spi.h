/**
  **************** (C) COPYRIGHT 2011 SCT Performance, LLC *********************
  * File Name          : spi.h
  * Device / System    : SCT products using the "CommonCode" platform
  * Author             : Quyen Leba
  *
  * Version            : 1 
  * Date               : 05/02/2011
  * Description        : 
  *                    : 
  *
  *
  * History            : 05/02/2011 M. Davis
  *                    :   Add SCT Copyright header,
  *
  ******************************************************************************
  */

#ifndef __SPI_H
#define __SPI_H

#include <arch/gentype.h>

typedef enum
{
    SPI_CS_TYPE_0       = 0x00,
    SPI_CS_TYPE_1       = 0x01,
    SPI_CS_TYPE_NONE    = 0xFF,
}SPI_CS_TYPE;

// General SPI init functions
void spi_master_init();
void spi_master_moduleinitonly(bool useCRC);
void spi_gpio_init();

// Chip Selects
void spi_master_enable_cs0();
void spi_master_disable_cs0();
void spi_master_enable_cs1();
void spi_master_disable_cs1();

// Polling functions
void spi_master_polling_init();
u8 spi_master_send_byte(u8 databyte);
u8 spi_master_receive_byte();

// DMA functions
void spi_master_dma_init(bool useCRC);
void spi_master_dma_transfer_data(u8 *txbuffer, u8 *rxbuffer, u32 buffersize, bool usetxirq);
u8 spi_master_dma_transfer_waitforcomplete(u32 timeoutms);
u8 spi_master_dma_transfer_checkcrc();

#endif	//__SPI_H
