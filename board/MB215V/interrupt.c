/**
  **************** (C) COPYRIGHT 2011 SCT Performance, LLC *********************
  * File Name          : interrupt.c
  * Device / System    : SCT products using the "CommonCode" platform
  * Author             : Quyen Leba
  *
  * Version            : 1 
  * Date               : 05/02/2011
  * Description        : 
  *                    : 
  *
  *
  * History            : 05/02/2011 M. Davis
  *                    :   Add SCT Copyright header,
  *
  ******************************************************************************
  */

#include <misc.h>
#include <board/genplatform.h>
#include <board/interrupt.h>

//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
void interrupt_init(FunctionalState state)
{
    interrupt_ctrl(state);
    interrupt_sw_init();
}

//------------------------------------------------------------------------------
// Function Name  : interrupt_config
// Description    : Configures the used IRQ Channels and sets their priority.
// Input          : FunctionalState intrCtrl - DISABLE, ENABLE
// Output         : None
// Return         : None
//------------------------------------------------------------------------------
void interrupt_ctrl(FunctionalState intrCtrl)
{
    NVIC_InitTypeDef NVIC_InitStructure;

    NVIC_SetVectorTable(NVIC_VectTab_FLASH, FLASH_APPLICATION_ADDRESS);

    //Group3: PreemptionPriority (0-7), SubPriority (0-1)
    NVIC_PriorityGroupConfig(NVIC_PriorityGroup_3);

    //Configure the SysTick handler priority
    NVIC_SetPriority(SysTick_IRQn,0x0);

    //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    // Interrupt priorities
    // Systick:     0-0
    // EXTI15_10:   0-1
    // TIM1:        1-0 (App & Board timeout timer)
    // UART1:       1-1
    // TIM5:        2-0 (board timeout timer)
    // TIM3:        2-1 (app timeout timer)
    // USB:         3-0 & 3-1
    // UART3:       4-0
	// EXTI0:       4-1 (SW Intr)
    // EXTI9_5:     5-0
    // TIM7:        5-1 (task timer)
	// USB:         6-0
    // TIM2:        6-1 (Commlink Command timeout)
    // EXTI1:       7-0 (SW Intr)
    // EXTI2:       7-1 (SW Intr)
    //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
#error not equiv MB103V yet
    //Enable the USART1 Interrupt
    NVIC_InitStructure.NVIC_IRQChannel = USART1_IRQn;
    NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 1;
    NVIC_InitStructure.NVIC_IRQChannelSubPriority = 1;
    NVIC_InitStructure.NVIC_IRQChannelCmd = intrCtrl;
    NVIC_Init(&NVIC_InitStructure);

    //Enable the USART3 Interrupt
    NVIC_InitStructure.NVIC_IRQChannel = USART3_IRQn;
    NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 4;
    NVIC_InitStructure.NVIC_IRQChannelSubPriority = 0;
    NVIC_InitStructure.NVIC_IRQChannelCmd = intrCtrl;
    NVIC_Init(&NVIC_InitStructure);

#if !CONFIG_FOR_TEST
    NVIC_InitStructure.NVIC_IRQChannel = EXTI9_5_IRQn;
    NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 5;
    NVIC_InitStructure.NVIC_IRQChannelSubPriority = 0;
    NVIC_InitStructure.NVIC_IRQChannelCmd = intrCtrl;
    NVIC_Init(&NVIC_InitStructure);

    NVIC_InitStructure.NVIC_IRQChannel = EXTI15_10_IRQn;
    NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 0;
    NVIC_InitStructure.NVIC_IRQChannelSubPriority = 1;
    NVIC_InitStructure.NVIC_IRQChannelCmd = intrCtrl;
    NVIC_Init(&NVIC_InitStructure);
#endif

    //software interrupt
    NVIC_InitStructure.NVIC_IRQChannel = EXTI_Line0;
    NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 4;
    NVIC_InitStructure.NVIC_IRQChannelSubPriority = 1;
    NVIC_InitStructure.NVIC_IRQChannelCmd = intrCtrl;
    NVIC_Init(&NVIC_InitStructure);

    NVIC_InitStructure.NVIC_IRQChannel = EXTI_Line1;
    NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 7;
    NVIC_InitStructure.NVIC_IRQChannelSubPriority = 0;
    NVIC_InitStructure.NVIC_IRQChannelCmd = intrCtrl;
    NVIC_Init(&NVIC_InitStructure);

    NVIC_InitStructure.NVIC_IRQChannel = EXTI_Line2;
    NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 7;
    NVIC_InitStructure.NVIC_IRQChannelSubPriority = 1;
    NVIC_InitStructure.NVIC_IRQChannelCmd = intrCtrl;
    NVIC_Init(&NVIC_InitStructure);

    //timer interrupt
    NVIC_InitStructure.NVIC_IRQChannel = TIM1_UP_TIM10_IRQn;
    NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 1;
    NVIC_InitStructure.NVIC_IRQChannelSubPriority = 0;
    NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;
    NVIC_Init(&NVIC_InitStructure);

    NVIC_InitStructure.NVIC_IRQChannel = TIM2_IRQn;
    NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 6;
    NVIC_InitStructure.NVIC_IRQChannelSubPriority = 1;
    NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;
    NVIC_Init(&NVIC_InitStructure);

    NVIC_InitStructure.NVIC_IRQChannel = TIM3_IRQn;
    NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 2;
    NVIC_InitStructure.NVIC_IRQChannelSubPriority = 1;
    NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;
    NVIC_Init(&NVIC_InitStructure);

    NVIC_InitStructure.NVIC_IRQChannel = TIM5_IRQn;
    NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 2;
    NVIC_InitStructure.NVIC_IRQChannelSubPriority = 0;
    NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;
    NVIC_Init(&NVIC_InitStructure);
    
    NVIC_InitStructure.NVIC_IRQChannel = TIM7_IRQn;
    NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 5;
    NVIC_InitStructure.NVIC_IRQChannelSubPriority = 1;
    NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;
    NVIC_Init(&NVIC_InitStructure);
}
