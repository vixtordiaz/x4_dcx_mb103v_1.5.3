/**
  **************** (C) COPYRIGHT 2011 SCT Performance, LLC *********************
  * File Name          : init.c
  * Device / System    : SCT products using the "CommonCode" platform
  * Author             : Quyen Leba
  *
  * Version            : 1 
  * Date               : 05/02/2011
  * Description        : 
  *                    : 
  *
  *
  * History            : 05/02/2011 M. Davis
  *                    :   Add SCT Copyright header,
  *
  ******************************************************************************
  */

#include <board/genplatform.h>
#include <board/clock.h>
#include <board/interrupt.h>
#include <board/peripherals.h>
#include <board/delays.h>
#include <board/indicator.h>
#include <fs/genfs.h>
#include <common/cmdif.h>
#include <common/crypto_blowfish.h>
#include <common/housekeeping.h>
#include <common/obd2.h>
#include <common/statuscode.h>
#include <common/settings.h>
#include <common/version.h>
#include "system.h"
#include "led.h"
#include "init.h"

extern obd2_info gObd2info;             /* from obd2.c */

//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
void init()
{
    clock_init();
#ifndef __DEBUG_JTAG_
    if(flash_enable_protections() != S_SUCCESS)
        led_emergency_indicator_system_error();
#endif
    interrupt_init(DISABLE);
    crypto_blowfish_init();
    peripherals_init();
    housekeeping_init();
    obd2_info_init(&gObd2info);
    obd2datalog_datalogmode_init();

#ifndef __TEST_FIRMWARE_BY_COMMLINK__
    if (settings_check() != S_SUCCESS)
    {
        led_emergency_indicator_settings_error();
    }
    if (version_check() != S_SUCCESS)
    {
        led_emergency_indicator_version_error();
    }
#endif
    
    bootloader_updatefromfile_checkifdone(); // Check for pending file updates
}

//------------------------------------------------------------------------------
// Reset to factory default
// Return:  u8  status
// Engineer: Quyen Leba
//------------------------------------------------------------------------------
u8 init_factorydefault()
{
    return board_set_factory_default();
}
