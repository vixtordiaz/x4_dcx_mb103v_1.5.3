/**
  **************** (C) COPYRIGHT 2011 SCT Performance, LLC *********************
  * File Name          : nand.h
  * Device / System    : SCT products using the "CommonCode" platform
  * Author             : Quyen Leba
  *
  * Version            : 1 
  * Date               : 05/02/2011
  * Description        : 
  *                    : 
  *
  *
  * History            : 05/02/2011 M. Davis
  *                    :   Add SCT Copyright header,
  *
  ******************************************************************************
  */

#ifndef __NAND_H
#define __NAND_H

typedef struct
{
    u8 Maker_ID;
    u8 Device_ID;
    u8 Third_ID;
    u8 Fourth_ID;
}NAND_IDTypeDef;

typedef struct 
{
    u16 Zone;
    u16 Block;
    u16 Page;
} NAND_ADDRESS;

/* NAND Area definition  for STM3210E-EVAL Board RevD */
#define CMD_AREA                   (u32)(1<<16)  /* A16 = CLE  high */
#define ADDR_AREA                  (u32)(1<<17)  /* A17 = ALE high */

#define DATA_AREA                  ((u32)0x00000000) 

/* FSMC NAND memory command */
#define	NAND_CMD_AREA_A            ((u8)0x00)
#define	NAND_CMD_AREA_B            ((u8)0x01)
#define NAND_CMD_AREA_C            ((u8)0x50)
#define NAND_CMD_AREA_TRUE1        ((u8)0x30)

#define NAND_CMD_WRITE0            ((u8)0x80)
#define NAND_CMD_WRITE_TRUE1       ((u8)0x10)
	
#define NAND_CMD_ERASE0            ((u8)0x60)
#define NAND_CMD_ERASE1            ((u8)0xD0)  

#define NAND_CMD_READID            ((u8)0x90)	
#define NAND_CMD_STATUS            ((u8)0x70)
#define NAND_CMD_LOCK_STATUS       ((u8)0x7A)
#define NAND_CMD_RESET             ((u8)0xFF)

/* NAND memory status */
#define NAND_VALID_ADDRESS         ((u32)0x00000100)
#define NAND_INVALID_ADDRESS       ((u32)0x00000200)
#define NAND_TIMEOUT_ERROR         ((u32)0x00000400)
#define NAND_BUSY                  ((u32)0x00000000)
#define NAND_ERROR                 ((u32)0x00000001)
#define NAND_READY                 ((u32)0x00000040)

/* FSMC NAND memory parameters */
/* Old ST 512mbit part params
#define NAND_PAGE_SIZE             ((u16)0x0200) // 512 bytes per page w/o Spare Area 
#define NAND_BLOCK_SIZE            ((u16)0x0020) // 32x512 bytes pages per block 
#define NAND_ZONE_SIZE             ((u16)0x0400) // 1024 Block per zone 
#define NAND_SPARE_AREA_SIZE       ((u16)0x0010) // last 16 bytes as spare area 
#define NAND_MAX_ZONE              ((u16)0x0004) // 4 zones of 1024 block 
*/
// Micron 2gb part params
#define NAND_PAGE_SIZE             ((u16)0x0800)//  2048 Bytes per page w/o spare area 
#define NAND_BLOCK_SIZE            ((u16)0x0040) // 64x2048 bytes pages per block 
#define NAND_ZONE_SIZE             ((u16)0x0800) // 2048 blocks total
#define NAND_SPARE_AREA_SIZE       ((u16)0x0040) /* last 64 bytes as spare area */
#define NAND_MAX_ZONE              ((u16)0x0001) /* 1 zones of 2048 block */

/* FSMC NAND memory address computation */
#define ADDR_1st_CYCLE(ADDR)       (u8)((ADDR)& 0xFF)               /* 1st addressing cycle */
#define ADDR_2nd_CYCLE(ADDR)       (u8)(((ADDR)& 0xFF00) >> 8)      /* 2nd addressing cycle */
#define ADDR_3rd_CYCLE(ADDR)       (u8)(((ADDR)& 0xFF0000) >> 16)   /* 3rd addressing cycle */
#define ADDR_4th_CYCLE(ADDR)       (u8)(((ADDR)& 0xFF000000) >> 24) /* 4th addressing cycle */

/* Exported macro ------------------------------------------------------------*/
/* Exported functions ------------------------------------------------------- */
void nand_init();
void nand_readid(NAND_IDTypeDef* NAND_ID);
u32 nand_writeSmallPage(u8 *pBuffer, NAND_ADDRESS Address, u32 NumPageToWrite);
u32 nand_readSmallPage(u8 *pBuffer, NAND_ADDRESS Address, u32 NumPageToRead);
u32 nand_writeSpareArea(u8 *pBuffer, NAND_ADDRESS Address, u32 NumSpareAreaTowrite);
u32 nand_readSpareArea(u8 *pBuffer, NAND_ADDRESS Address, u32 NumSpareAreaToRead);
u32 nand_eraseBlock(NAND_ADDRESS Address);
u32 nand_reset();
u32 nand_getStatus();
u32 nand_readStatus();
u32 nand_addressIncrement(NAND_ADDRESS* Address);

#endif  //__NAND_H