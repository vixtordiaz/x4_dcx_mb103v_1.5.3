/**
  **************** (C) COPYRIGHT 2011 SCT Performance, LLC *********************
  * File Name          : uartC.h
  * Device / System    : SCT products using the "CommonCode" platform
  * Author             : Quyen Leba
  *
  * Version            : 1 
  * Date               : 05/02/2011
  * Description        : 
  *                    : 
  *
  *
  * History            : 05/02/2011 M. Davis
  *                    :   Add SCT Copyright header,
  *
  ******************************************************************************
  */

#ifndef __UARTC_H
#define __UARTC_H

#include <arch/gentype.h>
#include "usart.h"

#define UARTC_NAME              USART3
#define UARTC_PORT              GPIOD
#define UARTC_GPIO_AF           GPIO_AF_USART3
#define UARTC_TX_PIN            GPIO_Pin_8
#define UARTC_RX_PIN            GPIO_Pin_9
#define UARTC_TX_PIN_SOURCE     GPIO_PinSource8
#define UARTC_RX_PIN_SOURCE     GPIO_PinSource9

void uartC_init(USARTBAUD baud, bool useFlowControl, bool useInterrupt);
void uartC_changebaudrate(USARTBAUD baud);
#define uartC_tx(databyte)      USART_SendData(UARTC_NAME, databyte);    \
    while(USART_GetFlagStatus(UARTC_NAME, USART_FLAG_TXE) == RESET);
#define uartC_rx()              USART_ReceiveData(UARTC_NAME)
#define uartC_getitstatus()     USART_GetITStatus(UARTC_NAME, USART_IT_RXNE)
#define uartC_getflagstatus(f)  USART_GetFlagStatus(USRTC_NAME, f)

#define uartC_disable_rx_interrupt()    \
    USART_ITConfig(UARTC_NAME, USART_IT_RXNE, DISABLE);
#define uartC_isdatavailable()  USART_GetFlagStatus(UARTC_NAME, USART_FLAG_RXNE) == SET

#endif  //__UARTC_H
