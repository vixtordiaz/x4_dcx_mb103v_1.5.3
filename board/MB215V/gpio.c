/**
  **************** (C) COPYRIGHT 2011 SCT Performance, LLC *********************
  * File Name          : gpio.c
  * Device / System    : SCT products using the "CommonCode" platform
  * Author             : Quyen Leba
  *
  * Version            : 1 
  * Date               : 05/02/2011
  * Description        : 
  *                    : 
  *
  *
  * History            : 05/02/2011 M. Davis
  *                    :   Add SCT Copyright header,
  *
  ******************************************************************************
  */

#include <board/genplatform.h>
#include <board/delays.h>
#include <common/statuscode.h>
#include "gpio.h"

#if BOARDREV_REVB0
//has no effects on normal operations
#define DUMMY_PORT                  GPIOE
#define DUMMY_PIN                   GPIO_Pin_2

//start using since LWTS B.1, other boards have no effects
#define AB_UI_BLOCK_EN_PORT         GPIO?
#define AB_UI_BLOCK_EN_PIN          GPIO_Pin_?

#define SB_DAV_A_BUSY_PORT          GPIOD
#define SB_DAV_A_BUSY_PIN           GPIO_Pin_11
#define SB_DAV_V_BUSY_PORT          GPIOD
#define SB_DAV_V_BUSY_PIN           GPIO_Pin_12
#define SB_DAV_V_BUSY_PORTSOURCE    EXTI_PortSourceGPIOD
#define SB_DAV_V_BUSY_PINSOURCE     GPIO_PinSource12
#define SB_DAV_V_BUSY_EXTI_LINE     EXTI_Line12
#define SB_DAV_V_BUSY_EXTI_CHANNEL  EXTI15_10_IRQn

#define SB_V_RESET_PORT             GPIOE
#define SB_V_RESET_PIN              GPIO_Pin_11
#define SB_A_RESET_N_PORT           GPIOE
#define SB_A_RESET_N_PIN            GPIO_Pin_10
#define SB_BOOTEN_PORT              GPIOE
#define SB_BOOTEN_PIN               GPIO_Pin_1

#define SB_BOARDREV_PORT            GPIOC
#define SB_BOARDREV_0_PIN           GPIO_Pin_13
#define SB_BOARDREV_1_PIN           GPIO_Pin_14
#define SB_BOARDREV_2_PIN           GPIO_Pin_15

//CTS input of uartA
#define COMMLINK_CTS_PORT           GPIOA
#define COMMLINK_CTS_PIN            GPIO_Pin_11

#define CC3000_SW_EN_PORT           GPIOE
#define CC3000_SW_EN_PIN            GPIO_Pin_13

#define USB_MUX_STAT_PORT           GPIOE
#define USB_MUX_STAT_PORTSOURCE     EXTI_PortSourceGPIOE
#define USB_MUX_STAT_PIN            GPIO_Pin_15
#define USB_MUX_STAT_PINSOURCE      GPIO_PinSource15
#define USB_MUX_STAT_EXTI_LINE      EXTI_Line15
#define USB_MUX_STAT_EXTI_CHANNEL   EXTI15_10_IRQn
// 5V Regulator Power Good Pin
#define V5VSW_POWERGOOD_PORT        GPIOB
#define V5VSW_POWERGOOD_PIN         GPIO_Pin_1

#define MCO1_PORT                   GPIOA
#define MCO1_PIN                    GPIO_Pin_8

#define USB_RESET_PORT              GPIOB
#define USB_RESET_PIN               GPIO_Pin_9

#if USE_COMMLINK_BT11_BLUETOOTH //++++++++++++++++++++++++++++++++++++++++++++++
#warning USE_COMMLINK_BT11_BLUETOOTH
#define COMMLINK_HAS_LINK           1
#define COMMLINK_HAS_SESSION        1
#define COMMLINK_HAS_MODE           1
#define COMMLINK_HAS_FORCE115K      1
#define FUNC_HAS_IGN_KEY_DET        0
#define FUNC_HAS_HIGH_PWR_DET       0
//Check AppBoard Link-Status output (SB.DAV_A_n, BT11.GPIO4)
#define COMMLINK_LINK_PORT          GPIOD
#define COMMLINK_LINK_PIN           GPIO_Pin_11
//Check AppBoard Sessions-Status output (SB.GPIO_7, BT11.GPIO14)
#define COMMLINK_SESSION_PORT       GPIOE
#define COMMLINK_SESSION_PIN        GPIO_Pin_9
//Check AppBoard Mode-Status output (SB.GPIO_6, BT11.GPIO15)
#define COMMLINK_MODE_PORT          GPIOE
#define COMMLINK_MODE_PIN           GPIO_Pin_0
//Force AppBoard 115kBaud Control Input (SB.GPIO3.PWM.BACKLIGHT_EN, BT11.GPIO5)
#define COMMLINK_115KBAUD_PORT      GPIOE
#define COMMLINK_115KBAUD_PIN       GPIO_Pin_14
#define COMMLINK_115KBAUD_LEVEL     1

//Control ignition key detection signal (HIGH: keyon)
#define FUNC_IGN_KEY_DET_PORT       DUMMY_PORT
#define FUNC_IGN_KEY_DET_PIN        DUMMY_PIN
//Check high power active input
#define FUNC_HIGH_POWER_PORT        DUMMY_PORT
#define FUNC_HIGH_POWER_PIN         DUMMY_PIN

#elif USE_COMMLINK_KEN257       //++++++++++++++++++++++++++++++++++++++++++++++
#warning USE_COMMLINK_KEN257
#define COMMLINK_HAS_LINK           0
#define COMMLINK_HAS_SESSION        1
#define COMMLINK_HAS_MODE           0
#define COMMLINK_HAS_FORCE115K      0
#define FUNC_HAS_IGN_KEY_DET        1
#define FUNC_HAS_HIGH_PWR_DET       1
//No AppBoard Link-Status output
#define COMMLINK_LINK_PORT          DUMMY_PORT
#define COMMLINK_LINK_PIN           DUMMY_PIN
//Check AppBoard Sessions-Status output (SB.GPIO_8.USARTB_CK, KEN257.GPIO4[5])
#define COMMLINK_SESSION_PORT       GPIOD
#define COMMLINK_SESSION_PIN        GPIO_Pin_7
//No AppBoard Mode-Status output
#define COMMLINK_MODE_PORT          DUMMY_PORT
#define COMMLINK_MODE_PIN           DUMMY_PIN
//No force AppBoard 115kBaud Control Input
#define COMMLINK_115KBAUD_PORT      DUMMY_PORT
#define COMMLINK_115KBAUD_PIN       DUMMY_PIN
#define COMMLINK_115KBAUD_LEVEL     1

//Control ignition key detection signal (HIGH: keyon, SB.GPIO5.DAC, KEN257.GPIO3[1])
#define FUNC_IGN_KEY_DET_PORT       GPIOA
#define FUNC_IGN_KEY_DET_PIN        GPIO_Pin_4
//Check high power active input (HIGH: active, SB.GPIO3.PWM.BACLIGHT_EN, KEN257.GPIO4[8])
#define FUNC_HIGH_POWER_PORT        GPIOE
#define FUNC_HIGH_POWER_PIN         GPIO_Pin_14

#elif USE_COMMLINK_X4           //++++++++++++++++++++++++++++++++++++++++++++++
#warning USE_COMMLINK_KEN257
#define COMMLINK_HAS_LINK           0
#define COMMLINK_HAS_SESSION        1
#define COMMLINK_HAS_MODE           0
#define COMMLINK_HAS_FORCE115K      0
#define FUNC_HAS_IGN_KEY_DET        1
#define FUNC_HAS_HIGH_PWR_DET       1
//No AppBoard Link-Status output
#define COMMLINK_LINK_PORT          DUMMY_PORT
#define COMMLINK_LINK_PIN           DUMMY_PIN
//Check AppBoard Sessions-Status output (SB.GPIO_8.USARTB_CK, KEN257.GPIO4[5])
#define COMMLINK_SESSION_PORT       GPIOD
#define COMMLINK_SESSION_PIN        GPIO_Pin_7
//No AppBoard Mode-Status output
#define COMMLINK_MODE_PORT          DUMMY_PORT
#define COMMLINK_MODE_PIN           DUMMY_PIN
//No force AppBoard 115kBaud Control Input
#define COMMLINK_115KBAUD_PORT      DUMMY_PORT
#define COMMLINK_115KBAUD_PIN       DUMMY_PIN
#define COMMLINK_115KBAUD_LEVEL     1

//Control ignition key detection signal (HIGH: keyon, SB.GPIO5.DAC, KEN257.GPIO3[1])
#define FUNC_IGN_KEY_DET_PORT       GPIOA
#define FUNC_IGN_KEY_DET_PIN        GPIO_Pin_4
//Check high power active input (HIGH: active, SB.GPIO3.PWM.BACLIGHT_EN, KEN257.GPIO4[8])
#define FUNC_HIGH_POWER_PORT        GPIOE
#define FUNC_HIGH_POWER_PIN         GPIO_Pin_14

#else
#error UNKNOWN APPBOARD COMMLINK DEVICE
#endif

#else
#error INVALID_BOARD
#endif

static bool DAV_V_BUSY_STROBED = FALSE;
static u8 mb_board_rev = 0;

void gpio_dav_busy_pin_init();

//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
void gpio_init()
{
    GPIO_InitTypeDef GPIO_InitStructure;

    GPIO_InitStructure.GPIO_Pin = AB_UI_BLOCK_EN_PIN;
    GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
    GPIO_InitStructure.GPIO_Mode = GPIO_Mode_OUT;
    GPIO_InitStructure.GPIO_OType = GPIO_OType_PP;
    GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_NOPULL;
    GPIO_Init(AB_UI_BLOCK_EN_PORT, &GPIO_InitStructure);
    gpio_block_remote_ui(FALSE);

    GPIO_InitStructure.GPIO_Pin = SB_DAV_V_BUSY_PIN;
    GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
    GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IN;
    GPIO_InitStructure.GPIO_OType = GPIO_OType_PP;
    GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_UP;
    GPIO_Init(SB_DAV_V_BUSY_PORT, &GPIO_InitStructure);

#if BOARDREV_REVB0
    GPIO_InitStructure.GPIO_Pin = SB_DAV_A_BUSY_PIN;
    GPIO_Init(SB_DAV_A_BUSY_PORT, &GPIO_InitStructure);
#endif

    //must be pull-up input pin when not used; otherwise, VehicleBoard
    //watchdog reset will not work
    GPIO_InitStructure.GPIO_Pin = SB_V_RESET_PIN;
    GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
    GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IN;
    GPIO_InitStructure.GPIO_OType = GPIO_OType_PP;
    GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_UP;
    GPIO_Init(SB_V_RESET_PORT, &GPIO_InitStructure);
    
    // Vehicle/App Board BOOT_EN default is LOW. It's active HIGH, this will force
    // the VB/AB into bootloader incase there is a problem.
    GPIO_InitStructure.GPIO_Pin = SB_BOOTEN_PIN;
    GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
    GPIO_InitStructure.GPIO_OType = GPIO_OType_PP;
    GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_NOPULL;
    GPIO_Init(SB_BOOTEN_PORT, &GPIO_InitStructure);
    GPIO_ResetBits(SB_BOOTEN_PORT, SB_BOOTEN_PIN); // Set LOW

    // Configure AppBoard reset signal as pull-up input; reconfig whenever use
    GPIO_InitStructure.GPIO_Pin = SB_A_RESET_N_PIN;
    GPIO_Init(SB_A_RESET_N_PORT, &GPIO_InitStructure);

    //configure pin to force AppBoard in 115kbaud
    GPIO_InitStructure.GPIO_Pin = COMMLINK_115KBAUD_PIN;
    GPIO_InitStructure.GPIO_Mode = GPIO_Mode_OUT;
    GPIO_InitStructure.GPIO_OType = GPIO_OType_PP;
    GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_NOPULL;
    GPIO_Init(COMMLINK_115KBAUD_PORT, &GPIO_InitStructure);
#if COMMLINK_115KBAUD_LEVEL
    GPIO_ResetBits(COMMLINK_115KBAUD_PORT, COMMLINK_115KBAUD_PIN);
#else
    GPIO_SetBits(COMMLINK_115KBAUD_PORT, COMMLINK_115KBAUD_PIN);
#endif

    //configure pin to check AppBoard Link-Status
    GPIO_InitStructure.GPIO_Pin = COMMLINK_LINK_PIN;
    GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IN;
    GPIO_InitStructure.GPIO_OType = GPIO_OType_PP;
    GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_DOWN;
    GPIO_Init(COMMLINK_LINK_PORT, &GPIO_InitStructure);
    //configure pin to check AppBoard Session-Status
    GPIO_InitStructure.GPIO_Pin = COMMLINK_SESSION_PIN;
    GPIO_Init(COMMLINK_SESSION_PORT, &GPIO_InitStructure);
    //configure pin to check AppBoard Mode-Status
    GPIO_InitStructure.GPIO_Pin = COMMLINK_MODE_PIN;
    GPIO_Init(COMMLINK_MODE_PORT, &GPIO_InitStructure);

    //configure pin to signal ignition key status
    GPIO_InitStructure.GPIO_Pin = FUNC_IGN_KEY_DET_PIN;
    GPIO_InitStructure.GPIO_Mode = GPIO_Mode_OUT;
    GPIO_InitStructure.GPIO_OType = GPIO_OType_PP;
    GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_NOPULL;
    GPIO_Init(FUNC_IGN_KEY_DET_PORT, &GPIO_InitStructure);
    GPIO_ResetBits(FUNC_IGN_KEY_DET_PORT, FUNC_IGN_KEY_DET_PIN);

    //configure pins to check MainBoard board rev
    GPIO_InitStructure.GPIO_Pin = SB_BOARDREV_0_PIN | SB_BOARDREV_1_PIN | SB_BOARDREV_2_PIN;
    GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IN;
    GPIO_InitStructure.GPIO_OType = GPIO_OType_PP;
    GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_DOWN;
    GPIO_Init(SB_BOARDREV_PORT, &GPIO_InitStructure);
    //get MB board rev
    mb_board_rev = GPIO_ReadInputDataBit(SB_BOARDREV_PORT, SB_BOARDREV_0_PIN);
    mb_board_rev |= GPIO_ReadInputDataBit(SB_BOARDREV_PORT, SB_BOARDREV_1_PIN) << 1;
    mb_board_rev |= GPIO_ReadInputDataBit(SB_BOARDREV_PORT, SB_BOARDREV_2_PIN) << 2;

    // Configure CC3000 Power Pin to default off
    GPIO_InitStructure.GPIO_Pin = CC3000_SW_EN_PIN;
    GPIO_InitStructure.GPIO_Mode = GPIO_Mode_OUT;
    GPIO_InitStructure.GPIO_OType = GPIO_OType_PP;
    GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_DOWN;
    GPIO_Init(CC3000_SW_EN_PORT, &GPIO_InitStructure);
    GPIO_ResetBits(CC3000_SW_EN_PORT, CC3000_SW_EN_PIN);

    //configure pin to reset USB transceiver
    GPIO_InitStructure.GPIO_Pin = USB_RESET_PIN;
    GPIO_InitStructure.GPIO_Mode = GPIO_Mode_OUT;
    GPIO_InitStructure.GPIO_OType = GPIO_OType_PP;
    GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_DOWN;
    GPIO_Init(USB_RESET_PORT, &GPIO_InitStructure);
    GPIO_ResetBits(USB_RESET_PORT, USB_RESET_PIN);
    
    //Output HSE clock on MCO1 pin(PA8)
    GPIO_InitStructure.GPIO_Pin = MCO1_PIN;
    GPIO_InitStructure.GPIO_Speed = GPIO_Speed_100MHz;
    GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AF;
    GPIO_InitStructure.GPIO_OType = GPIO_OType_PP;
    GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_UP;
    GPIO_Init(MCO1_PORT, &GPIO_InitStructure);

    RCC_MCO1Config(RCC_MCO1Source_HSE, RCC_MCO1Div_1);

    //configure pin to check USB Power Mux Status pin
    GPIO_InitStructure.GPIO_Pin = USB_MUX_STAT_PIN;
    GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IN;
    GPIO_InitStructure.GPIO_OType = GPIO_OType_PP;
    GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_UP;
    GPIO_Init(USB_MUX_STAT_PORT, &GPIO_InitStructure);
    
    //configure pin to check 5V Regulator "Power Good" Pin
    GPIO_InitStructure.GPIO_Pin = V5VSW_POWERGOOD_PIN;
    GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IN;
    GPIO_InitStructure.GPIO_OType = GPIO_OType_PP;
    GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_UP;
    GPIO_Init(V5VSW_POWERGOOD_PORT, &GPIO_InitStructure);

#if USE_COMMLINK_X4
    // configure pin to check if high power active (if available)
    GPIO_InitStructure.GPIO_Pin = FUNC_HIGH_POWER_PIN;
    GPIO_InitStructure.GPIO_Mode = GPIO_Mode_OUT;
    GPIO_InitStructure.GPIO_OType = GPIO_OType_PP;
    GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_DOWN;
    GPIO_Init(FUNC_HIGH_POWER_PORT, &GPIO_InitStructure);

    if (gpio_is_fullpower_available())      //check and setup current power mode
    {
        gpio_use_fullpower_mode(TRUE);
    }
    else
    {
        gpio_use_fullpower_mode(FALSE);
    }
#else
    // configure pin to check if high power active (if available)
    GPIO_InitStructure.GPIO_Pin = FUNC_HIGH_POWER_PIN;
    GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IN;
    GPIO_InitStructure.GPIO_OType = GPIO_OType_PP;
    GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_DOWN;
    GPIO_Init(FUNC_HIGH_POWER_PORT, &GPIO_InitStructure);
#endif
}

//------------------------------------------------------------------------------
// Sets the Force 115K Baud Pin on AppBoard
// Return:  u8  status
// Engineer: Patrick Downs
//------------------------------------------------------------------------------
u8 gpio_force115kBaud_app_board(bool enable)
{
#if COMMLINK_HAS_FORCE115K
#if COMMLINK_115KBAUD_LEVEL
    if(enable)
    {
        GPIO_SetBits(COMMLINK_115KBAUD_PORT, COMMLINK_115KBAUD_PIN);
    }
    else
    {
        GPIO_ResetBits(COMMLINK_115KBAUD_PORT, COMMLINK_115KBAUD_PIN);
    }
#else
    if(enable)
    {
        GPIO_ResetBits(COMMLINK_115KBAUD_PORT, COMMLINK_115KBAUD_PIN);
    }
    else
    {
        GPIO_SetBits(COMMLINK_115KBAUD_PORT, COMMLINK_115KBAUD_PIN);
    }
#endif
    return S_SUCCESS;
#else
    return S_NOTSUPPORT;
#endif
}

//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
void gpio_dav_busy_pin_init()
{
    EXTI_InitTypeDef EXTI_InitStructure;

    SYSCFG_EXTILineConfig(SB_DAV_V_BUSY_PORTSOURCE, SB_DAV_V_BUSY_PINSOURCE);
    
    EXTI_InitStructure.EXTI_Line = SB_DAV_V_BUSY_EXTI_LINE;
    EXTI_InitStructure.EXTI_Mode = EXTI_Mode_Interrupt;    
    EXTI_InitStructure.EXTI_Trigger = EXTI_Trigger_Falling;
    EXTI_InitStructure.EXTI_LineCmd = ENABLE;
    EXTI_Init(&EXTI_InitStructure);
}

//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
void gpio_read_dav_v_busy_intr()
{
    if(EXTI_GetITStatus(SB_DAV_V_BUSY_EXTI_LINE) != RESET)
    {
        DAV_V_BUSY_STROBED = TRUE;
        
        // Clear the Button EXTI line pending bit
        EXTI_ClearITPendingBit(SB_DAV_V_BUSY_EXTI_LINE);
    }
}

//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
bool gpio_dav_v_busy_strobed()
{
    if(DAV_V_BUSY_STROBED)
    {
        DAV_V_BUSY_STROBED = FALSE;
        return TRUE;
    }
    else
    {
        return FALSE;
    }
}

//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
GPIO_PIN_STATUS gpio_read_dav_v_busy_pin()
{
    return (GPIO_ReadInputDataBit(SB_DAV_V_BUSY_PORT, SB_DAV_V_BUSY_PIN) == Bit_SET) ? GPIO_PIN_STATUS_HIGH : GPIO_PIN_STATUS_LOW;
}

//------------------------------------------------------------------------------
// Check if 5V switching power is good
// Return:  bool (TRUE: power good, odb2 power source present)
//------------------------------------------------------------------------------
bool gpio_is_5vsw_powergood()
{
    return (bool)GPIO_ReadInputDataBit(V5VSW_POWERGOOD_PORT, V5VSW_POWERGOOD_PIN);
}

//------------------------------------------------------------------------------
// Setup external interrupt pin to trigger on USB Power Status pin change
//------------------------------------------------------------------------------
void gpio_usb_mux_status_pin_init(void)
{
    EXTI_InitTypeDef EXTI_InitStructure;
    
    GPIO_EXTILineConfig(USB_MUX_STAT_PORTSOURCE, USB_MUX_STAT_PINSOURCE);
    
    EXTI_InitStructure.EXTI_Line = USB_MUX_STAT_EXTI_LINE;
    EXTI_InitStructure.EXTI_Mode = EXTI_Mode_Interrupt;
    EXTI_InitStructure.EXTI_Trigger = EXTI_Trigger_Rising_Falling;
    EXTI_InitStructure.EXTI_LineCmd = ENABLE;
    EXTI_Init(&EXTI_InitStructure);
}

//------------------------------------------------------------------------------
// A helper to trigger fullpower mode through a tasktimer
// Engineer: Quyen Leba
//------------------------------------------------------------------------------
void __use_fullpower_mode()
{
    gpio_use_fullpower_mode(TRUE);
}

//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
void gpio_usb_mux_status_pin_intr(void)
{
    if(EXTI_GetITStatus(USB_MUX_STAT_EXTI_LINE) != RESET)
    {
        if (gpio_is_fullpower_available())
        {
            //prevent power flickering. Full power must be stable for 2s first
            tasktimer_setup(2000, TASKTIMER_ONESHOT, TaskTimerTaskType_SignalFullpowerMode,
                            (TimerTaskFunc)&__use_fullpower_mode, NULL);
        }
        else
        {
            tasktimer_setup(0, TASKTIMER_ONESHOT, TaskTimerTaskType_SignalFullpowerMode,
                            NULL, NULL);
            gpio_use_fullpower_mode(FALSE);
        }
        // Clear the EXTI line pending bit
        EXTI_ClearITPendingBit(USB_MUX_STAT_EXTI_LINE);
    }
}

//------------------------------------------------------------------------------
// Test if in FullPower mode
// Return:  bool (TRUE: in FullPower mode)
// Engineer: Quyen Leba
//------------------------------------------------------------------------------
bool gpio_is_fullpower_available()
{
    // USB Mux Stat Pin HIGH = VUSB Source
    if (GPIO_ReadInputDataBit(USB_MUX_STAT_PORT, USB_MUX_STAT_PIN))
    {
        return FALSE;   //powered by USB
    }
    else
    {
        return TRUE;    //powered by OBD2
    }
}

//------------------------------------------------------------------------------
// Control power mode (to use full power or low power)
// Input:   bool yes (TRUE: use full power)
// Engineer: Quyen Leba
//------------------------------------------------------------------------------
void gpio_use_fullpower_mode(bool yes)
{
#if USE_COMMLINK_X4
    //FUNC_HIGH_POWER_PIN low: low power source, high: power source
    if (yes /*&& gpio_is_5vsw_powergood()*/)
    {
        GPIO_SetBits(FUNC_HIGH_POWER_PORT, FUNC_HIGH_POWER_PIN);    //full
    }
    else /*if (!yes && !gpio_is_5vsw_powergood())*/
    {
        GPIO_ResetBits(FUNC_HIGH_POWER_PORT, FUNC_HIGH_POWER_PIN);
    }
    else
    {
        GPIO_ResetBits(FUNC_HIGH_POWER_PORT, FUNC_HIGH_POWER_PIN);
    }
#else
    //do nothing
#endif
}

//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
//TODOQ: for now
//u8 gpio_read_dav_a_busy_pin()
//{
//    return GPIO_ReadInputDataBit(SB_DAV_A_BUSY_PORT, SB_DAV_A_BUSY_PIN);
//}

//------------------------------------------------------------------------------
// Reset AppBoard
// Engineer: Mark Davis
// Note: reset pin is normally configured as pull-up input, re-configure it to
// output push-pull to drive reset pin and re-configure it back to pull-up input
// Important for BT11 module
//------------------------------------------------------------------------------
void gpio_reset_app_board()
{
    GPIO_InitTypeDef GPIO_InitStructure;

    GPIO_InitStructure.GPIO_Pin = SB_A_RESET_N_PIN;
    GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
    GPIO_InitStructure.GPIO_Mode = GPIO_Mode_OUT;
    GPIO_InitStructure.GPIO_OType = GPIO_OType_PP;
    GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_NOPULL;
    GPIO_SetBits(SB_A_RESET_N_PORT,SB_A_RESET_N_PIN);
    GPIO_Init(SB_A_RESET_N_PORT, &GPIO_InitStructure);

    // Hold RESET high for 30mS
    delays_counter(DELAYS_COUNTER_30MS);

    // Set RESET low for 100mS
    GPIO_ResetBits(SB_A_RESET_N_PORT,SB_A_RESET_N_PIN);
    delays_counter(DELAYS_COUNTER_100MS);

    // Set RESET high
    GPIO_SetBits(SB_A_RESET_N_PORT,SB_A_RESET_N_PIN);

    GPIO_InitStructure.GPIO_Pin = SB_A_RESET_N_PIN;
    GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IN;
    GPIO_InitStructure.GPIO_OType = GPIO_OType_PP;
    GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_UP;
    GPIO_Init(SB_A_RESET_N_PORT, &GPIO_InitStructure);

    delays_counter(DELAYS_COUNTER_100MS);
    delays_counter(DELAYS_COUNTER_100MS);
}

//------------------------------------------------------------------------------
// Reset VehicleBoard
// Engineer: Quyen Leba
//------------------------------------------------------------------------------
void gpio_reset_vehicle_board()
{
    GPIO_InitTypeDef GPIO_InitStructure;

    GPIO_InitStructure.GPIO_Pin = SB_V_RESET_PIN;
    GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
    GPIO_InitStructure.GPIO_Mode = GPIO_Mode_OUT;
    GPIO_InitStructure.GPIO_OType = GPIO_OType_PP;
    GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_NOPULL;
    GPIO_Init(SB_V_RESET_PORT, &GPIO_InitStructure);
    GPIO_SetBits(SB_V_RESET_PORT,SB_V_RESET_PIN);
    delays_counter(DELAYS_COUNTER_10MS);

    GPIO_ResetBits(SB_V_RESET_PORT,SB_V_RESET_PIN);
    delays_counter(DELAYS_COUNTER_30MS);

    //must be pull-up input pin when not used; otherwise, VehicleBoard
    //watchdog reset will not work
    GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IN;
    GPIO_InitStructure.GPIO_OType = GPIO_OType_PP;
    GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_UP;
    GPIO_Init(SB_V_RESET_PORT, &GPIO_InitStructure);

    delays_counter(DELAYS_COUNTER_100MS);
}

//------------------------------------------------------------------------------
// Set BOOT Enable Line
// Engineer: Patrick Downs
//------------------------------------------------------------------------------
void gpio_set_booten(bool state)
{
    if(state == TRUE)
        GPIO_SetBits(SB_BOOTEN_PORT,SB_BOOTEN_PIN);
    else
        GPIO_ResetBits(SB_BOOTEN_PORT,SB_BOOTEN_PIN);
}



//------------------------------------------------------------------------------
// Get Board Rev
// Return:  u8  rev
// Engineer: Quyen Leba
//------------------------------------------------------------------------------
u8 gpio_get_board_rev()
{
    //board rev is read in gpio_init()
    return mb_board_rev;
}

//------------------------------------------------------------------------------
// Check connection status from AppBoard
// Return:  u32 connection status flags
//------------------------------------------------------------------------------
u32 gpio_peek_commlink_connection_status()
{
    u32  bt_connection;

    bt_connection = 0;
#if COMMLINK_HAS_LINK
    if (GPIO_ReadInputDataBit(COMMLINK_LINK_PORT, COMMLINK_LINK_PIN) == Bit_SET)
    {
        bt_connection |= (1<<COMMLINK_LINK_BIT_POS);
    }
#else
    bt_connection |= (1<<COMMLINK_LINK_BIT_POS);
#endif

#if COMMLINK_HAS_SESSION
    if (GPIO_ReadInputDataBit(COMMLINK_SESSION_PORT, COMMLINK_SESSION_PIN) == Bit_SET)
    {
        bt_connection |= (1<<COMMLINK_SESSION_BIT_POS);
    }
#else
    bt_connection |= (1<<COMMLINK_SESSION_BIT_POS);
#endif

#if COMMLINK_HAS_MODE
    if (GPIO_ReadInputDataBit(COMMLINK_MODE_PORT, COMMLINK_MODE_PIN) == Bit_SET)
    {
        bt_connection |= (1<<COMMLINK_MODE_BIT_POS);
    }
#else
    bt_connection |= (1<<COMMLINK_MODE_BIT_POS);
#endif
    return bt_connection;
}

//------------------------------------------------------------------------------
// Return the raw value of commlink CTS input (low is asserted)
//------------------------------------------------------------------------------
u8 gpio_peek_commlink_CTS_status()
{
    // Asserted is low
    return (GPIO_ReadInputDataBit(COMMLINK_CTS_PORT, COMMLINK_CTS_PIN));
}

/**
 * @brief   Send signal to AB to block UI
 *
 * @param   [in] yes
 *
 * @author  Quyen Leba
 *
 */
void gpio_block_remote_ui(bool yes)
{
    if (yes)
    {
        GPIO_SetBits(AB_UI_BLOCK_EN_PORT, AB_UI_BLOCK_EN_PIN);
    }
    else
    {
        GPIO_ResetBits(AB_UI_BLOCK_EN_PORT, AB_UI_BLOCK_EN_PIN);
    }
}

//------------------------------------------------------------------------------
// Enable or disable USB UPLI PHY
// Input:   bool enable (TRUE: enable)
// Engineer: Quyen Leba
//------------------------------------------------------------------------------
void gpio_enable_usb(bool enable)
{
    if (enable)
    {
        GPIO_ResetBits(USB_RESET_PORT, USB_RESET_PIN);
    }
    else
    {
        GPIO_SetBits(USB_RESET_PORT, USB_RESET_PIN);
    }
}
