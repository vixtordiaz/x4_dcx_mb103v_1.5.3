/**
  **************** (C) COPYRIGHT 2011 SCT Performance, LLC *********************
  * File Name          : usartB.h
  * Device / System    : SCT products using the "CommonCode" platform
  * Author             : Quyen Leba
  *
  * Version            : 1 
  * Date               : 05/02/2011
  * Description        : 
  *                    : 
  *
  *
  * History            : 05/02/2011 M. Davis
  *                    :   Add SCT Copyright header,
  *
  ******************************************************************************
  */

#ifndef __USARTB_H
#define __USARTB_H

#include <arch/gentype.h>
#include "stm32f2xx_usart.h"
#include "usart.h"

#define USARTB_NAME				USART2
#define USARTB_PORT             GPIOD
#define USARTB_IRQn             USART2_IRQn
#define USARTB_Priority         
#define USARTB_SubPriority      
#define USARTB_GPIO_AF          GPIO_AF_USART2
#define USARTB_TX_PIN           GPIO_Pin_5
#define USARTB_RX_PIN           GPIO_Pin_6
#define USARTB_TX_PIN_SOURCE    GPIO_PinSource9
#define USARTB_RX_PIN_SOURCE    GPIO_PinSource10

void usartB_init(USARTBAUD baud, bool useFlowControl, bool useInterrupt);
void usartB_changebaudrate(USARTBAUD baud);
#define usartB_tx(databyte)     USART_SendData(USARTB_NAME, databyte);    \
    while(USART_GetFlagStatus(USARTB_NAME, USART_FLAG_TXE) == RESET);
#define usartB_rx()             USART_ReceiveData(USARTB_NAME)
#define usartB_getitstatus()    USART_GetITStatus(USARTB_NAME, USART_IT_RXNE)
#define usartB_getflagstatus(f) USART_GetFlagStatus(USARTB_NAME, f)

#define usartB_disable_rx_interrupt()   \
    USART_ITConfig(USARTB_NAME, USART_IT_RXNE, DISABLE);
#define usartB_isdatavailable() USART_GetFlagStatus(USART3, USART_FLAG_RXNE) == SET

#endif  //__USARTB_H
