/**
  **************** (C) COPYRIGHT 2011 SCT Performance, LLC *********************
  * File Name          : accelerometer.c
  * Device / System    : SCT products using the "CommonCode" platform
  * Author             : Quyen Leba
  *
  * Version            : 1 
  * Date               : 05/02/2011
  * Description        : 
  *                    : 
  *
  *
  * History            : 05/02/2011 M. Davis
  *                    :   Add SCT Copyright header,
  *
  ******************************************************************************
  */

#include <arch/gentype.h>
#include <common/statuscode.h>
#include "i2c.h"
#include "accelerometer.h"

#define ACCELEROMETER_I2C_ADDR      (0x1D << 1)
#define ACCEL_XOUTL                 0x00
#define ACCEL_XOUTH                 0x01
#define ACCEL_YOUTL                 0x02
#define ACCEL_YOUTH                 0x03
#define ACCEL_ZOUTL                 0x04
#define ACCEL_ZOUTH                 0x05
#define ACCEL_XOUT8                 0x06
#define ACCEL_YOUT8                 0x07
#define ACCEL_ZOUT8                 0x08
#define ACCEL_STATUS                0x09
#define ACCEL_DETSRC                0x0A    //Detection Source
#define ACCEL_I2CAD                 0x0D
#define ACCEL_WHOAMI                0x0F

#define ACCEL_MCTL                  0x16    //Mode Control
#define ACCEL_INTRST                0x17    //Interrupt Latch Reset
#define ACCEL_CTL1                  0x18    //Control 1
#define ACCEL_CTL2                  0x19    //Control 2
#define ACCEL_LDTH                  0x1A    //Level Detection Threshold
#define ACCEL_PDTH                  0x1B    //Pulse Detection Threshold
#define ACCEL_PW                    0x1C    //Pulse Duration
#define ACCEL_LT                    0x1D    //Latency Time
#define ACCEL_TW                    0x1E    //Time Window for 2nd Pulse

//Mode Control
#define ACCEL_RANGE_MASK            0xF3
#define ACCEL_RANGE_8G              (0x00<<2)
#define ACCEL_RANGE_4G              (0x10<<2)
#define ACCEL_RANGE_2G              (0x01<<2)
#define ACCEL_MODE_MASK             0xFC
#define ACCEL_MODE_STANDBY          (0x00)
#define ACCEL_MODE_MEASUREMENT      (0x01)
#define ACCEL_MODE_LEVEL_DETECT     (0x10)
#define ACCEL_MODE_PULSE_DETECT     (0x11)
#define ACCEL_DRPD                  (1<<6)  //Data Ready to INT1/DRDY pin

//Latch Interrupt
#define ACCEL_CLR_INT2              (1<<1)
#define ACCEL_CLR_INT1              (1<<0)

struct
{
    u8  mctl;
}accel_info;

//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
u8 accelerometer_init()
{
    u8  who;
    u8  status;

    status = accelerometer_get_whoami(&who);
    if (status != S_SUCCESS)
    {
        return S_FAIL;
    }

    status = accelerometer_set_measurement_mode();
    if (status != S_SUCCESS)
    {
        return S_FAIL;
    }

    status = accelerometer_set_sensitivity(2);
    if (status != S_SUCCESS)
    {
        return S_FAIL;
    }
    
    return S_SUCCESS;
}

//------------------------------------------------------------------------------
// Get WhoAmI
// Output:  u8  *whoami
// Return:  u8  status
// Engineer: Quyen Leba
//------------------------------------------------------------------------------
u8 accelerometer_get_whoami(u8 *whoami)
{
    u8  who;
    u8  addr;
    u8  status;

    *whoami = 0;
    status = i2c_readbytes(ACCELEROMETER_I2C_ADDR,ACCEL_I2CAD,
                           &addr,1);
    if (status != S_SUCCESS || addr != (ACCELEROMETER_I2C_ADDR>>1))
    {
        return S_FAIL;
    }

    status = i2c_readbytes(ACCELEROMETER_I2C_ADDR,ACCEL_WHOAMI,
                           &who,1);
    if (status == S_SUCCESS)
    {
        *whoami = who;
        return S_SUCCESS;
    }
    else
    {
        return S_FAIL;
    }
}

//------------------------------------------------------------------------------
// Set Measurement Mode
// Return:  u8  status
// Engineer: Quyen Leba
//------------------------------------------------------------------------------
u8 accelerometer_set_measurement_mode()
{
    u8  modecontrol;
    u8  status;

    status = i2c_readbytes(ACCELEROMETER_I2C_ADDR,ACCEL_MCTL,
                           &modecontrol,1);
    if (status == S_SUCCESS)
    {
        modecontrol &= ACCEL_MODE_MASK;
        modecontrol |= ACCEL_MODE_MEASUREMENT;
        status = i2c_writebytes(ACCELEROMETER_I2C_ADDR,ACCEL_MCTL,
                            &modecontrol,1);
    }
    if (status == S_SUCCESS)
    {
        accel_info.mctl = modecontrol;
    }
    return status;
}

//------------------------------------------------------------------------------
// Set Level Detection Mode
// Return:  u8  status
// Engineer: Quyen Leba
//------------------------------------------------------------------------------
u8 accelerometer_set_leveldectection_mode()
{
    u8  modecontrol;
    u8  status;

    status = i2c_readbytes(ACCELEROMETER_I2C_ADDR,ACCEL_MCTL,
                           &modecontrol,1);
    if (status == S_SUCCESS)
    {
        modecontrol &= ACCEL_MODE_MASK;
        modecontrol |= ACCEL_MODE_LEVEL_DETECT;
        status = i2c_writebytes(ACCELEROMETER_I2C_ADDR,ACCEL_MCTL,
                            &modecontrol,1);
    }
    if (status == S_SUCCESS)
    {
        accel_info.mctl = modecontrol;
    }
    return status;
}

//------------------------------------------------------------------------------
// Set Pulse Detection Mode
// Return:  u8  status
// Engineer: Quyen Leba
//------------------------------------------------------------------------------
u8 accelerometer_set_pulsedectection_mode()
{
    u8  modecontrol;
    u8  status;

    status = i2c_readbytes(ACCELEROMETER_I2C_ADDR,ACCEL_MCTL,
                           &modecontrol,1);
    if (status == S_SUCCESS)
    {
        modecontrol &= ACCEL_MODE_MASK;
        modecontrol |= ACCEL_MODE_PULSE_DETECT;
        status = i2c_writebytes(ACCELEROMETER_I2C_ADDR,ACCEL_MCTL,
                            &modecontrol,1);
    }
    if (status == S_SUCCESS)
    {
        accel_info.mctl = modecontrol;
    }
    return status;
}

//------------------------------------------------------------------------------
// Set sensitivity
// Input:   u8  g (valid value: 2,4,8)
// Return:  u8  status
// Engineer: Quyen Leba
//------------------------------------------------------------------------------
u8 accelerometer_set_sensitivity(u8 g)
{
    u8  modecontrol;
    u8  status;

    status = i2c_readbytes(ACCELEROMETER_I2C_ADDR,ACCEL_MCTL,
                               &modecontrol,1);
    if (status != S_SUCCESS)
    {
        return status;
    }

    modecontrol &= ACCEL_RANGE_MASK;
    switch(g)
    {
    case 2:
        modecontrol |= ACCEL_RANGE_2G;
        break;
    case 4:
        modecontrol |= ACCEL_RANGE_4G;
        break;
    case 8:
        modecontrol |= ACCEL_RANGE_8G;
        break;
    default:
        return S_INPUT;
    }

    status = i2c_writebytes(ACCELEROMETER_I2C_ADDR,ACCEL_MCTL,
                            &modecontrol,1);
    if (status == S_SUCCESS)
    {
        accel_info.mctl = modecontrol;
    }

    return status;
}

//------------------------------------------------------------------------------
// Get XYZ
// Input:   bool use10bit
// Outputs: u16 *x
//          u16 *y
//          u16 *z
// Return:  u8  status
// Engineer: Quyen Leba
//------------------------------------------------------------------------------
u8 accelerometer_get_xyz(u16 *x, u16 *y, u16 *z, bool use10bit)
{
    u8  buffer[6];
    u8  status;

    if (use10bit)
    {
        status = i2c_readbytes(ACCELEROMETER_I2C_ADDR,ACCEL_XOUTL,
                               buffer,6);
        if (status == S_SUCCESS)
        {
            *x = *(u16*)&buffer[0];
            *y = *(u16*)&buffer[2];
            *z = *(u16*)&buffer[4];
        }
    }
    else
    {
        status = i2c_readbytes(ACCELEROMETER_I2C_ADDR,ACCEL_XOUT8,
                               buffer,3);
        if (status == S_SUCCESS)
        {
            *x = buffer[0];
            *y = buffer[1];
            *z = buffer[2];
        }
    }
    return status;
}
