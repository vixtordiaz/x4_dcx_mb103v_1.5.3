/**
  **************** (C) COPYRIGHT 2011 SCT Performance, LLC *********************
  * File Name          : accelerometer.h
  * Device / System    : SCT products using the "CommonCode" platform
  * Author             : Quyen Leba
  *
  * Version            : 1 
  * Date               : 05/02/2011
  * Description        : 
  *                    : 
  *
  *
  * History            : 05/02/2011 M. Davis
  *                    :   Add SCT Copyright header,
  *
  ******************************************************************************
  */

#ifndef __ACCELEROMETER_H
#define __ACCELEROMETER_H

u8 accelerometer_init();
u8 accelerometer_get_whoami(u8 *whoami);
u8 accelerometer_set_measurement_mode();
u8 accelerometer_set_leveldectection_mode();
u8 accelerometer_set_pulsedectection_mode();
u8 accelerometer_set_sensitivity(u8 g);
u8 accelerometer_get_xyz(u16 *x, u16 *y, u16 *z, bool use10bit);

#endif	//__ACCELEROMETER_H
