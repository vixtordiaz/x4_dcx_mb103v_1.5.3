/**
  **************** (C) COPYRIGHT 2011 SCT Performance, LLC *********************
  * File Name          : interrupt_sw.c
  * Device / System    : SCT products using the "CommonCode" platform
  * Author             : Quyen Leba
  *
  * Version            : 1 
  * Date               : 05/02/2011
  * Description        : 
  *                    : 
  *
  *
  * History            : 05/02/2011 M. Davis
  *                    :   Add SCT Copyright header,
  *
  ******************************************************************************
  */

#include <misc.h>
#include <board/genplatform.h>
#include <common/statuscode.h>
#include "interrupt_sw.h"

struct
{
    interrupt_sw_func   func[3];
    bool func_fire_once[3];
}interrupt_sw_info =
{
    .func[0] = NULL,
    .func[1] = NULL,
    .func[2] = NULL,
    .func_fire_once[0] = TRUE,
    .func_fire_once[1] = TRUE,
    .func_fire_once[2] = TRUE,
};

#define SW_INTR_TRIGGER_0_LINE          EXTI_Line0
#define SW_INTR_TRIGGER_0_IRQ           EXTI0_IRQn
#define SW_INTR_TRIGGER_1_LINE          EXTI_Line1
#define SW_INTR_TRIGGER_1_IRQ           EXTI1_IRQn
#define SW_INTR_TRIGGER_2_LINE          EXTI_Line2
#define SW_INTR_TRIGGER_2_IRQ           EXTI2_IRQn

//------------------------------------------------------------------------------
// Init software interrupt
// Engineer: Quyen Leba
//------------------------------------------------------------------------------
void interrupt_sw_init()
{
    EXTI_InitTypeDef EXTI_InitStructure;

    //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    // Configure sw interrupt 0
    //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    EXTI_InitStructure.EXTI_Line = SW_INTR_TRIGGER_0_LINE;
    EXTI_InitStructure.EXTI_Mode = EXTI_Mode_Interrupt;
    EXTI_InitStructure.EXTI_Trigger = EXTI_Trigger_Rising_Falling;
    EXTI_InitStructure.EXTI_LineCmd = ENABLE;
    EXTI_Init(&EXTI_InitStructure);
    
    //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    // Configure sw interrupt 1
    //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    EXTI_InitStructure.EXTI_Line = SW_INTR_TRIGGER_1_LINE;
    EXTI_InitStructure.EXTI_Mode = EXTI_Mode_Interrupt;
    EXTI_InitStructure.EXTI_Trigger = EXTI_Trigger_Rising_Falling;
    EXTI_InitStructure.EXTI_LineCmd = ENABLE;
    EXTI_Init(&EXTI_InitStructure);

    //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    // Configure sw interrupt 2
    //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    EXTI_InitStructure.EXTI_Line = SW_INTR_TRIGGER_2_LINE;
    EXTI_InitStructure.EXTI_Mode = EXTI_Mode_Interrupt;
    EXTI_InitStructure.EXTI_Trigger = EXTI_Trigger_Rising_Falling;
    EXTI_InitStructure.EXTI_LineCmd = ENABLE;
    EXTI_Init(&EXTI_InitStructure);
}

//------------------------------------------------------------------------------
// Trigger a software interrupt
// Input:   SoftwareIntrTrigger trigger
// Return:  u8  status
// Engineer: Quyen Leba
//------------------------------------------------------------------------------
u8 interrupt_sw_trigger(SoftwareIntrTrigger trigger)
{
    switch(trigger)
    {
    case SoftwareIntrTrigger_0:
        EXTI_GenerateSWInterrupt(SW_INTR_TRIGGER_0_LINE);
        break;
    case SoftwareIntrTrigger_1:
        EXTI_GenerateSWInterrupt(SW_INTR_TRIGGER_1_LINE);
        break;
    case SoftwareIntrTrigger_2:
        EXTI_GenerateSWInterrupt(SW_INTR_TRIGGER_2_LINE);
        break;
    default:
        return S_NOTSUPPORT;
    }
    return S_SUCCESS;
}

//------------------------------------------------------------------------------
// Set a function to trigger
// Inputs:  SoftwareIntrTrigger trigger
//          interrupt_sw_func func (trigger function)
//          bool fire_once (TRUE: trigger only one time)
// Return:  u8  status
// Engineer: Quyen Leba
//------------------------------------------------------------------------------
u8 interrupt_sw_trigger_set_func(SoftwareIntrTrigger trigger,
                                 interrupt_sw_func func, bool fire_once)
{
    switch(trigger)
    {
    case SoftwareIntrTrigger_0:
        interrupt_sw_info.func[0] = func;
        interrupt_sw_info.func_fire_once[0] = fire_once;
        break;
    case SoftwareIntrTrigger_1:
        interrupt_sw_info.func[1] = func;
        interrupt_sw_info.func_fire_once[1] = fire_once;
        break;
    case SoftwareIntrTrigger_2:
        interrupt_sw_info.func[2] = func;
        interrupt_sw_info.func_fire_once[2] = fire_once;
        break;
    default:
        return S_NOTSUPPORT;
    }
    return S_SUCCESS;
}

//------------------------------------------------------------------------------
// Set a function to trigger
// Input:   SoftwareIntrTrigger trigger
// Return:  u8  status
// Engineer: Quyen Leba
//------------------------------------------------------------------------------
u8 interrupt_sw_trigger_clear_func(SoftwareIntrTrigger trigger)
{
    switch(trigger)
    {
    case SoftwareIntrTrigger_0:
        interrupt_sw_info.func[0] = NULL;
        break;
    case SoftwareIntrTrigger_1:
        interrupt_sw_info.func[1] = NULL;
        break;
    case SoftwareIntrTrigger_2:
        interrupt_sw_info.func[2] = NULL;
        break;
    default:
        return S_NOTSUPPORT;
    }
    return S_SUCCESS;
}

//------------------------------------------------------------------------------
// Interrupt handler of trigger 0 (ext interrupt line 1)
// Engineer: Quyen Leba
//------------------------------------------------------------------------------
void interrupt_sw_trigger_handler_0()
{
    if (interrupt_sw_info.func[0])
    {
        interrupt_sw_info.func[0]();
    }
    if (interrupt_sw_info.func_fire_once[0])
    {
        interrupt_sw_info.func[0] = NULL;
        EXTI_ClearITPendingBit(SW_INTR_TRIGGER_0_LINE);
    }
}

//------------------------------------------------------------------------------
// Interrupt handler of trigger 1 (ext interrupt line 2)
// Engineer: Quyen Leba
//------------------------------------------------------------------------------
void interrupt_sw_trigger_handler_1()
{
    if (interrupt_sw_info.func[1])
    {
        interrupt_sw_info.func[1]();
    }
    if (interrupt_sw_info.func_fire_once[1])
    {
        interrupt_sw_info.func[1] = NULL;
        EXTI_ClearITPendingBit(SW_INTR_TRIGGER_1_LINE);
    }
}

//------------------------------------------------------------------------------
// Interrupt handler of trigger 2 (ext interrupt line 3)
// Engineer: Quyen Leba
//------------------------------------------------------------------------------
void interrupt_sw_trigger_handler_2()
{
    if (interrupt_sw_info.func[2])
    {
        interrupt_sw_info.func[2]();
    }
    if (interrupt_sw_info.func_fire_once[2])
    {
        interrupt_sw_info.func[2] = NULL;
        EXTI_ClearITPendingBit(SW_INTR_TRIGGER_2_LINE);
    }
}
