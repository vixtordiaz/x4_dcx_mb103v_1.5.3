/**
  **************** (C) COPYRIGHT 2011 SCT Performance, LLC *********************
  * File Name          : clock.c
  * Device / System    : SCT products using the "CommonCode" platform
  * Author             : Quyen Leba
  *
  * Version            : 1 
  * Date               : 05/02/2011
  * Description        : 
  *                    : 
  *
  *
  * History            : 05/02/2011 M. Davis
  *                    :   Add SCT Copyright header,
  *
  ******************************************************************************
  */

#include <board/clock.h>
#include <common/statuscode.h>

//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
u8 clock_init()
{
    if (RCC_GetFlagStatus(RCC_FLAG_WWDGRST) != RESET)
    { 
        RCC_ClearFlag();
    }
    return S_SUCCESS;
}
