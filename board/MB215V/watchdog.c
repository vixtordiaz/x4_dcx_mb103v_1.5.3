/**
  **************** (C) COPYRIGHT 2011 SCT Performance, LLC *********************
  * File Name          : watchdog.c
  * Device / System    : SCT products using the "CommonCode" platform
  * Author             : Quyen Leba
  *
  * Version            : 1 
  * Date               : 05/02/2011
  * Description        : 
  *                    : 
  *
  *
  * History            : 05/02/2011 M. Davis
  *                    :   Add SCT Copyright header,
  *
  ******************************************************************************
  */

#include "watchdog.h"

//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
void watchdog_set()
{
    RCC_APB1PeriphClockCmd(RCC_APB1Periph_WWDG, ENABLE);
    //WWDG clock counter = (PCLK1 (30MHz)/4096)/8 = 915 Hz (~1092 us)
    WWDG_SetPrescaler(WWDG_Prescaler_8);

    //Set Window value to 80; WWDG counter should be refreshed only when the counter
    //is below 80 (and greater than 64) otherwise a reset will be generated
    WWDG_SetWindowValue(80);
    //Enable WWDG and set counter value to 127, WWDG timeout = ~1092 us * 64 = 69.9 ms 
    //In this case the refresh window is: ~1092 * (127-80) = 51.3 ms < refresh window < ~1092 * 64 = 69.9ms
    WWDG_Enable(127);

    //Clear EWI flag
    WWDG_ClearFlag();
    //Enable EW interrupt
    ////WWDG_EnableIT();
}
