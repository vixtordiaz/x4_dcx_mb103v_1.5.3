/**
  **************** (C) COPYRIGHT 2011 SCT Performance, LLC *********************
  * File Name          : usartB.c
  * Device / System    : SCT products using the "CommonCode" platform
  * Author             : Quyen Leba
  *
  * Version            : 1 
  * Date               : 05/02/2011
  * Description        : 
  *                    : 
  *
  *
  * History            : 05/02/2011 M. Davis
  *                    :   Add SCT Copyright header,
  *
  ******************************************************************************
  */

#include "usartB.h"

//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
void usartB_init(USARTBAUD baud, bool useFlowControl, bool useInterrupt)
{
    USART_InitTypeDef USART_InitStructure;
    GPIO_InitTypeDef GPIO_InitStructure;

    // Enable USART2 clocks
    RCC_APB1PeriphClockCmd(RCC_APB1Periph_USART2, ENABLE);
    // Connect USART pins to AF7
    GPIO_PinAFConfig(USARTB_PORT, USARTB_TX_PIN_SOURCE, USARTB_GPIO_AF);
    GPIO_PinAFConfig(USARTB_PORT, USARTB_RX_PIN_SOURCE, USARTB_GPIO_AF);

    // Configure USART Tx and Rx as alternate function push-pull
    GPIO_InitStructure.GPIO_Pin = USARTB_TX_PIN;
    GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AF;
    GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
    GPIO_InitStructure.GPIO_OType = GPIO_OType_PP;
    GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_UP;
    GPIO_Init(USARTB_PORT, &GPIO_InitStructure);

    GPIO_InitStructure.GPIO_Pin = USARTB_RX_PIN;
    GPIO_Init(USARTB_PORT, &GPIO_InitStructure);

    // Enable the USART OverSampling by 8
    //USART_OverSampling8Cmd(USARTA_NAME, ENABLE);  

    /* USART1 configured as follow:
    - BaudRate = 3750000 baud
    - Maximum BaudRate that can be achieved when using the Oversampling by 8
    is: (USART APB Clock / 8) 
    Example: 
    - (USART3 APB1 Clock / 8) = (30 MHz / 8) = 3750000 baud
    - (USART1 APB2 Clock / 8) = (60 MHz / 8) = 7500000 baud
    - Maximum BaudRate that can be achieved when using the Oversampling by 16
    is: (USART APB Clock / 16) 
    Example: (USART3 APB1 Clock / 16) = (30 MHz / 16) = 1875000 baud
    Example: (USART1 APB2 Clock / 16) = (60 MHz / 16) = 3750000 baud
    - Word Length = 8 Bits
    - one Stop Bit
    - No parity
    - Hardware flow control disabled (RTS and CTS signals)
    - Receive and transmit enabled
    */
    USART_InitStructure.USART_BaudRate = (u32)baud;
    USART_InitStructure.USART_WordLength = USART_WordLength_8b;
    USART_InitStructure.USART_StopBits = USART_StopBits_1;
    USART_InitStructure.USART_Parity = USART_Parity_No;
    if (useFlowControl)
    {
        USART_InitStructure.USART_HardwareFlowControl = USART_HardwareFlowControl_RTS_CTS;
    }
    else
    {
        USART_InitStructure.USART_HardwareFlowControl = USART_HardwareFlowControl_None;
    }
    USART_InitStructure.USART_Mode = USART_Mode_Rx | USART_Mode_Tx;
    USART_Init(USARTB_NAME, &USART_InitStructure);

//    // Enable the UARTA Interrupt
//    NVIC_InitStructure.NVIC_IRQChannel = USARTB_IRQn;
//    NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = USARTB_Priority;
//    NVIC_InitStructure.NVIC_IRQChannelSubPriority = USARTB_SubPriority;
//    NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;
//    NVIC_Init(&NVIC_InitStructure);

    if (useInterrupt)
    {
        // Enable usartA Receive interrupt
        USART_ITConfig(USARTB_NAME, USART_IT_RXNE, ENABLE);
    }
    else
    {
        USART_ITConfig(USARTB_NAME, USART_IT_RXNE, DISABLE);
    }

    USART_Cmd(USARTB_NAME, ENABLE);
}

//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
void usartB_changebaudrate(USARTBAUD baud)
{
    u32 tmpreg = 0x00, apbclock = 0x00;
    u32 integerdivider = 0x00;
    u32 fractionaldivider = 0x00;
    RCC_ClocksTypeDef RCC_ClocksStatus;
    
    RCC_GetClocksFreq(&RCC_ClocksStatus);  
    
    apbclock = RCC_ClocksStatus.PCLK1_Frequency;
    
    /* Determine the integer part */
    integerdivider = ((0x19 * apbclock) / (0x04 * ((u32)baud)));
    tmpreg = (integerdivider / 0x64) << 0x04;
    
    /* Determine the fractional part */
    fractionaldivider = integerdivider - (0x64 * (tmpreg >> 0x04));
    tmpreg |= ((((fractionaldivider * 0x10) + 0x32) / 0x64)) & ((u8)0x0F);
    
    while(USART_GetFlagStatus(USARTB_NAME, USART_FLAG_TC) == RESET);
    
    /* Write to USART BRR */
    USARTB_NAME->BRR = (u16)tmpreg;
}
