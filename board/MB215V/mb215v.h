/**
  **************** (C) COPYRIGHT 2011 SCT Performance, LLC *********************
  * File Name          : mb215v.h
  * Device / System    : SCT products using the "CommonCode" platform
  * Author             : Quyen Leba
  *
  * Version            : 1 
  * Date               : 05/02/2011
  * Description        : 
  *                    : 
  *
  *
  * History            : 05/02/2011 M. Davis
  *                    :   Add SCT Copyright header,
  *
  ******************************************************************************
  */

#ifndef __MB215V_H
#define __MB215V_H

#define BOARDREV_REVB0                              1

#define USE_CRC32_HARDWARE                          1
#define USE_UARTTYPE_DATALINK                       1

#include "timer.h"
#include "MB215V/adcmb.h"
#include "MB215V/properties.h"
#include "MB215V/generic.h"
#include "MB215V/init.h"
#include "MB215V/flash.h"
#include "MB215V/crc32_hw.h"
#include "MB215V/watchdog.h"
#include "MB215V/spi.h"
#include "MB215V/gpio.h"
#include "MB215V/led.h"
#include "MB215V/button.h"
#include "MB215V/accelerometer.h"
#include "MB215V/usartB.h"
#include "MB215V/uartC.h"
#include "MB215V/interrupt_sw.h"
#if __COMMLINK_HAL_BLUETOOTH__
#include "MB215V/bluetooth.h"
#endif
#include "usb_conf.h"
#include "usbd_ioreq.h"
#include "MB215V/USB/usb_callback_helper.h"

#if __COMMLINK_HAL_BLUETOOTH__
#include "MB215V/commlink_hal_bluetooth.h"
#elif __COMMLINK_HAL_UARTA__
#include "MB215V/commlink_hal_uartA.h"
#elif __COMMLINK_HAL_KEN__
#include "MB215V/cc3000_hal.h"
#include "MB215V/commlink_hal_ken.h"
#elif __COMMLINK_HAL_X4__
#include "MB215V/cc3000_hal.h"
#include "MB215V/commlink_hal_x4.h"
#else
#error genplatform.h: unknown COMMLINK_HAL
#endif

#define SUPPORT_FILE_XFER

#define DEFAULT_DATALOG_FEATURE_FILENAME            "datalogfeatures.dlx"

#endif	//__MB215V_H
