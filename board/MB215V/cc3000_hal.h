/**
  **************** (C) COPYRIGHT 2011 SCT Performance, LLC *********************
  * File Name          : cc3000_hal.h
  * Device / System    : SCT products using the "CommonCode" platform
  * Author             : Quyen Leba
  *
  * Version            : 1 
  * Date               : 05/02/2011
  * Description        : 
  *                    : 
  *
  *
  * History            : 05/02/2011 M. Davis
  *                    :   Add SCT Copyright header,
  *
  ******************************************************************************
  */

#ifndef __CC3000_HAL_H
#define __CC3000_HAL_H

#include <arch/gentype.h>

void cc3000_hal_init();
void cc3000_hal_enable();
void cc3000_hal_disable();
long cc3000_hal_readinterruptpin();
void cc3000_hal_wlaninterruptenable();
void cc3000_hal_wlaninterruptdisable();
void cc3000_hal_writewlanpin(u8 val);
void cc3000_hal_setabrev(u8 abrev);

void cc3000_hal_cs_init();
void cc3000_hal_cs_deinit();

#endif	//__CC3000_HAL_H
