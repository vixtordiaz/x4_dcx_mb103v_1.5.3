/**
  **************** (C) COPYRIGHT 2011 SCT Performance, LLC *********************
  * File Name          : usartA.c
  * Device / System    : SCT products using the "CommonCode" platform
  * Author             : Quyen Leba
  *
  * Version            : 1 
  * Date               : 05/02/2011
  * Description        : 
  *                    : 
  *
  *
  * History            : 05/02/2011 M. Davis
  *                    :   Add SCT Copyright header,
  *
  ******************************************************************************
  */

#include "usartA.h"

//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
void usartA_init(USARTBAUD baud, bool useFlowControl, bool useInterrupt)
{
    USART_InitTypeDef USART_InitStructure;
    GPIO_InitTypeDef GPIO_InitStructure;

    RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOA, ENABLE);
    RCC_APB2PeriphClockCmd(RCC_APB2Periph_USART1, ENABLE);
    // Connect USART pins to AF7
    GPIO_PinAFConfig(USARTA_PORT, USARTA_TX_PIN_SOURCE, USARTA_GPIO_AF);
    GPIO_PinAFConfig(USARTA_PORT, USARTA_RX_PIN_SOURCE, USARTA_GPIO_AF);
    GPIO_PinAFConfig(USARTA_PORT, USARTA_CTS_PIN_SOURCE, USARTA_GPIO_AF);
    GPIO_PinAFConfig(USARTA_PORT, USARTA_RTS_PIN_SOURCE, USARTA_GPIO_AF);

    // Configure USART Tx and Rx as alternate function push-pull
    GPIO_InitStructure.GPIO_Pin = USARTA_TX_PIN;
    GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AF;
    GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
    GPIO_InitStructure.GPIO_OType = GPIO_OType_PP;
    GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_UP;
    GPIO_Init(USARTA_PORT, &GPIO_InitStructure);

    GPIO_InitStructure.GPIO_Pin = USARTA_RTS_PIN;
    GPIO_Init(USARTA_PORT, &GPIO_InitStructure);

    GPIO_InitStructure.GPIO_Pin = USARTA_RX_PIN;
    //GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IN;
    GPIO_Init(USARTA_PORT, &GPIO_InitStructure);

    GPIO_InitStructure.GPIO_Pin = USARTA_CTS_PIN;
    GPIO_Init(USARTA_PORT, &GPIO_InitStructure);

    if (!useFlowControl)
    {
        //config RTS & CTS as pull-down inputs
        GPIO_InitStructure.GPIO_Pin = USARTA_CTS_PIN;
        GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IN;
        GPIO_InitStructure.GPIO_OType = GPIO_OType_PP;
        GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_DOWN;
        GPIO_Init(USARTA_PORT, &GPIO_InitStructure);

        GPIO_InitStructure.GPIO_Pin = USARTA_RTS_PIN;
        GPIO_InitStructure.GPIO_Mode = GPIO_Mode_OUT;
        GPIO_Init(GPIOD, &GPIO_InitStructure);
        GPIO_ResetBits(USARTA_PORT, USARTA_RTS_PIN);
    }
    
    // Enable the USART OverSampling by 8
    //USART_OverSampling8Cmd(USARTA_NAME, ENABLE);  

    /* USART1 configured as follow:
    - BaudRate = 3750000 baud
    - Maximum BaudRate that can be achieved when using the Oversampling by 8
    is: (USART APB Clock / 8) 
    Example: 
    - (USART3 APB1 Clock / 8) = (30 MHz / 8) = 3750000 baud
    - (USART1 APB2 Clock / 8) = (60 MHz / 8) = 7500000 baud
    - Maximum BaudRate that can be achieved when using the Oversampling by 16
    is: (USART APB Clock / 16) 
    Example: (USART3 APB1 Clock / 16) = (30 MHz / 16) = 1875000 baud
    Example: (USART1 APB2 Clock / 16) = (60 MHz / 16) = 3750000 baud
    - Word Length = 8 Bits
    - one Stop Bit
    - No parity
    - Hardware flow control disabled (RTS and CTS signals)
    - Receive and transmit enabled
    */
    USART_InitStructure.USART_BaudRate = (u32)baud;
    USART_InitStructure.USART_WordLength = USART_WordLength_8b;
    USART_InitStructure.USART_StopBits = USART_StopBits_1;
    USART_InitStructure.USART_Parity = USART_Parity_No;
    if (useFlowControl)
    {
        USART_InitStructure.USART_HardwareFlowControl = USART_HardwareFlowControl_RTS_CTS;
    }
    else
    {
        USART_InitStructure.USART_HardwareFlowControl = USART_HardwareFlowControl_None;
    }
    USART_InitStructure.USART_Mode = USART_Mode_Rx | USART_Mode_Tx;
    USART_Init(USARTA_NAME, &USART_InitStructure);

    if (useInterrupt)
    {
        // Enable usartA Receive interrupt
        USART_ITConfig(USARTA_NAME, USART_IT_RXNE, ENABLE);
    }
    else
    {
        USART_ITConfig(USARTA_NAME, USART_IT_RXNE, DISABLE);
    }
    
    // Enable the usartA
    USART_Cmd(USARTA_NAME, ENABLE);
}

//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
void usartA_disable_interrupt()
{
    USART_ITConfig(USARTA_NAME, USART_IT_RXNE, DISABLE);
}

void usartA_enable_interrupt()
{
    USART_ITConfig(USARTA_NAME, USART_IT_RXNE, ENABLE);
}

//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
void usartA_changebaudrate(USARTBAUD baud)
{
    u32 tmpreg = 0x00, apbclock = 0x00;
    u32 integerdivider = 0x00;
    u32 fractionaldivider = 0x00;
    //  u32 usartxbase = 0;
    RCC_ClocksTypeDef RCC_ClocksStatus;
    
    /* DISABLE usartA Receive and Transmit interrupts */
    //USART_ITConfig(USARTA_NAME, USART_IT_RXNE, DISABLE);
    
    RCC_GetClocksFreq(&RCC_ClocksStatus);  
    
    apbclock = RCC_ClocksStatus.PCLK1_Frequency;
    
    /* Determine the integer part */
    integerdivider = ((0x19 * apbclock) / (0x04 * ((u32)baud)));
    tmpreg = (integerdivider / 0x64) << 0x04;
    
    /* Determine the fractional part */
    fractionaldivider = integerdivider - (0x64 * (tmpreg >> 0x04));
    tmpreg |= ((((fractionaldivider * 0x10) + 0x32) / 0x64)) & ((u8)0x0F);
    
    //  while(USART_GetFlagStatus(USARTA_NAME, USART_FLAG_IDLE) != RESET);
    while(USART_GetFlagStatus(USARTA_NAME, USART_FLAG_TC) == RESET);
    
    /* Write to USART BRR */
    USARTA_NAME->BRR = (u16)tmpreg;
    
    //USART_ClearFlag(USARTA_NAME, USART_FLAG_RXNE);
    //USART_ClearITPendingBit(USARTA_NAME, USART_IT_RXNE);
    
    /* Enable USARTA_NAME Receive and Transmit interrupts */
    //USART_ITConfig(USARTA_NAME, USART_IT_RXNE, ENABLE);
    
    /* Enable the usartA */
    //USART_Cmd(USARTA_NAME, ENABLE);
}

//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
//void usartA_tx(u8 databyte)
//{
//    USART_SendData(USARTA_NAME, databyte);
//    while(USART_GetFlagStatus(USARTA_NAME, USART_FLAG_TXE) == RESET);
//}
