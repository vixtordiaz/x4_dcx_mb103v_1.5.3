/**
  **************** (C) COPYRIGHT 2011 SCT Performance, LLC *********************
  * File Name          : flash.c
  * Device / System    : SCT products using the "CommonCode" platform
  * Author             : Quyen Leba
  *
  * Version            : 1 
  * Date               : 05/02/2011
  * Description        : 
  *                    : 
  *
  *
  * History            : 05/02/2011 M. Davis
  *                    :   Add SCT Copyright header,
  *
  ******************************************************************************
  */

#include <string.h>
#include <board/genplatform.h>
#include <board/indicator.h>
#include <fs/genfs.h>
#include <common/settings.h>
#include <common/statuscode.h>
#include <common/crypto_blowfish.h>
#include <common/crc32.h>
#include "flash.h"

//------------------------------------------------------------------------------
// Inputs:  FlashSettingsArea flashsettingsarea
//          u8 *settingsdata
//          u32 settingsdatalength (must be multiple of 4 bytes)
// Return:  u8  status
// Engineer: Quyen Leba
//------------------------------------------------------------------------------
u8 flash_save_setting(FlashSettingsArea flashsettingsarea,
                      u8 *settingsdata, u32 settingsdatalength)
{
    u32 erase_sector;
    FLASH_Status FlashStatus = FLASH_COMPLETE;
    struct
    {
        u32 address[5];
        u8  *buffer;
        u8  count;
        u8  working_index;
    }backup =
    {
        .buffer = NULL,
    };
    u8  *filename;
    F_FILE *sfptr;
    u32 bytecount;
    u32 *crc32e_wptr;
    u32 i,j;
    u32 *wptr;
    u8  status;

    status = S_SUCCESS;
    sfptr = NULL;
    crc32e_wptr = NULL;
    filename = NULL;

    if (settingsdatalength > FLASHSETTINGS_MAX_SETTINGS_SIZE)
    {
        return S_BADCONTENT;
    }
    backup.buffer = __malloc(5*FLASHSETTINGS_MAX_SETTINGS_SIZE);
    if (backup.buffer == NULL)
    {
        return S_MALLOC;
    }

    memcpy((char*)&backup.buffer[0*FLASHSETTINGS_MAX_SETTINGS_SIZE],
           (char*)FLASHSETTINGS_BOOTSETTINGS_ADDR,
           FLASHSETTINGS_MAX_SETTINGS_SIZE);
    backup.address[0] = FLASHSETTINGS_BOOTSETTINGS_ADDR;

    memcpy((char*)&backup.buffer[1*FLASHSETTINGS_MAX_SETTINGS_SIZE],
           (char*)FLASHSETTINGS_CRITICAL_ADDR,
           FLASHSETTINGS_MAX_SETTINGS_SIZE);
    backup.address[1] = FLASHSETTINGS_CRITICAL_ADDR;

    memcpy((char*)&backup.buffer[2*FLASHSETTINGS_MAX_SETTINGS_SIZE],
           (char*)FLASHSETTINGS_TUNE_ADDR,
           FLASHSETTINGS_MAX_SETTINGS_SIZE);
    backup.address[2] = FLASHSETTINGS_TUNE_ADDR;

    memcpy((char*)&backup.buffer[3*FLASHSETTINGS_MAX_SETTINGS_SIZE],
           (char*)FLASHSETTINGS_FLEET_ADDR,
           FLASHSETTINGS_MAX_SETTINGS_SIZE);
    backup.address[3] = FLASHSETTINGS_FLEET_ADDR;

    memcpy((char*)&backup.buffer[4*FLASHSETTINGS_MAX_SETTINGS_SIZE],
           (char*)FLASHSETTINGS_DATALOGGENERAL_ADDR,
           FLASHSETTINGS_MAX_SETTINGS_SIZE);
    backup.address[4] = FLASHSETTINGS_DATALOGGENERAL_ADDR;

    backup.count = 5;
    erase_sector = FLASH_Sector_3;

    switch(flashsettingsarea)
    {
    case BootArea:
        backup.working_index = 0;
        break;
    case CriticalArea:
        filename = FLASHSETTINGS_CRITICAL_FILENAME;
        crc32e_wptr = &((Settings_Critical*)settingsdata)->crc32e;
        backup.working_index = 1;
        break;
    case TuneArea:
        filename = FLASHSETTINGS_TUNE_FILENAME;
        crc32e_wptr = &((Settings_Tune*)settingsdata)->crc32e;
        backup.working_index = 2;
        break;
    case FleetArea:
        filename = FLASHSETTINGS_FLEET_FILENAME;
        crc32e_wptr = &((Settings_Fleet*)settingsdata)->crc32e;
        backup.working_index = 3;
        break;
    case DatalogGeneralArea:
        filename = FLASHSETTINGS_DATALOGGENERAL_FILENAME;
        crc32e_wptr = &((Settings_DatalogGeneral*)settingsdata)->crc32e;
        backup.working_index = 4;
        break;
    default:
        status = S_INPUT;
        goto flash_save_setting_done;
    }
    if (memcmp((char*)backup.address[backup.working_index],
               (char*)settingsdata,settingsdatalength) == 0)
    {
        status = S_SUCCESS;
        goto flash_save_setting_done;
    }

    memcpy((char*)&backup.buffer[backup.working_index*FLASHSETTINGS_MAX_SETTINGS_SIZE],
           (char*)settingsdata,settingsdatalength);

    if (crc32e_wptr)
    {
        crc32e_reset();
        *crc32e_wptr = 0xFFFFFFFF;
        *crc32e_wptr = crc32e_calculateblock(0xFFFFFFFF,
                                             (u32*)backup.address[backup.working_index],
                                             FLASHSETTINGS_MAX_SETTINGS_SIZE/4);
    }

    status = S_SUCCESS;
    //1st, make sure able to backup settings to a file before doing anything
    //to the settings in flash
    if (filename)
    {
        sfptr = genfs_user_openfile(filename,"w");
        if (sfptr == NULL)
        {
            status = S_OPENFILE;
        }
        else
        {
            bytecount = fwrite(&backup.buffer[backup.working_index*FLASHSETTINGS_MAX_SETTINGS_SIZE],
                               1,FLASHSETTINGS_MAX_SETTINGS_SIZE,sfptr);
            if (bytecount != FLASHSETTINGS_MAX_SETTINGS_SIZE)
            {
                status = S_WRITEFILE;
            }
            genfs_closefile(sfptr);
            sfptr = NULL;
        }
    }

    if (status != S_SUCCESS)
    {
        indicator_set(Indicator_BackupSettingsError);
        goto flash_save_setting_done_no_indicator;
    }

    FLASH_Unlock();

    FLASH_ClearFlag(FLASH_FLAG_EOP | FLASH_FLAG_OPERR | FLASH_FLAG_WRPERR | 
                    FLASH_FLAG_PGAERR | FLASH_FLAG_PGPERR|FLASH_FLAG_PGSERR);

    FlashStatus = FLASH_EraseSector(erase_sector, VoltageRange_3);
    if (FlashStatus != FLASH_COMPLETE)
    {
        FLASH_Lock();
        status = S_FAIL;
        goto flash_save_setting_done;
    }

    // write new settings to flash
    for(i=0;i<backup.count;i++)
    {
        wptr = (u32*)&backup.buffer[i*FLASHSETTINGS_MAX_SETTINGS_SIZE];
        for(j=0;j<FLASHSETTINGS_MAX_SETTINGS_SIZE;j+=4)
        {
            FlashStatus = FLASH_ProgramWord(backup.address[i]+j,*wptr++);
            if (FlashStatus != FLASH_COMPLETE)
            {
                FLASH_Lock();
                status = S_FAIL;
                goto flash_save_setting_done;
            }
        }
    }
    FLASH_Lock();

flash_save_setting_done:    
    if (status != S_SUCCESS)
    {
        indicator_set(Indicator_SettingsError);
    }
flash_save_setting_done_no_indicator:
    if (backup.buffer)
    {
        __free(backup.buffer);
    }
    return status;
}

//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
u8 flash_load_setting(FlashSettingsArea flashsettingsarea,
                      u8 *settingsdata, u32 settingsdatalength)
{
    switch(flashsettingsarea)
    {
    case CriticalArea:
        memcpy((char*)settingsdata,
               (char*)FLASHSETTINGS_CRITICAL_ADDR,settingsdatalength);
        break;
    case TuneArea:
        memcpy((char*)settingsdata,
               (char*)FLASHSETTINGS_TUNE_ADDR,settingsdatalength);
        break;
    case DatalogGeneralArea:
        memcpy((char*)settingsdata,
               (char*)FLASHSETTINGS_DATALOGGENERAL_ADDR,settingsdatalength);
        break;
    default:
        return S_INPUT;
    }

    return S_SUCCESS;
}

//------------------------------------------------------------------------------
// Load bootsettings from flash
// Input:   u32 length
// Output:  u8  *settings
// Return:  u8  status
// Engineer: Quyen Leba
//------------------------------------------------------------------------------
u8 flash_load_bootsettings(u8 *settings, u32 length)
{
    if (length <= FLASHSETTINGS_MAX_SETTINGS_SIZE)
    {
        memcpy((char*)settings,(char*)FLASHSETTINGS_BOOTSETTINGS_ADDR,length);
        return S_SUCCESS;
    }
    return S_INPUT;
}

//------------------------------------------------------------------------------
// Store bootsettings to flash
// Inputs:  u8  *settings
//          u16 length
// Return:  u8  status
// Engineer: Quyen Leba
//------------------------------------------------------------------------------
u8 flash_store_bootsettings(u8 *settings, u16 length)
{
    return flash_save_setting(BootArea,settings,length);
}

//------------------------------------------------------------------------------
// Enables the Write and Read Out Protections if not already enabled
// Inputs:  WP_PAGES Defines which pages to protect
// Return:
// Engineer: Patrick Downs
//------------------------------------------------------------------------------
#define WP_PAGES 0x000001FF
u8 flash_enable_protections(void)
{
//    u16 WRP0_Data = 0xFFFF, WRP1_Data = 0xFFFF, WRP2_Data = 0xFFFF, WRP3_Data = 0xFFFF;
//    FlagStatus WpStatus;
//    FlagStatus RdpStatus;
//    u32 FLASH_Pages;
//    
//    FLASH_Pages = (u32)(~WP_PAGES);
//    WRP0_Data = (u16)(FLASH_Pages & 0x000000FF);
//    WRP1_Data = (u16)((FLASH_Pages & 0x0000FF00) >> 8);
//    WRP2_Data = (u16)((FLASH_Pages & 0x00FF0000) >> 16);
//    WRP3_Data = (u16)((FLASH_Pages & 0xFF000000) >> 24);
//    
//    WpStatus = SET;
//    RdpStatus = SET;
//    
//    u32 WPDATA;
//    
//    WPDATA = FLASH_GetWriteProtectionOptionByte();
//    if(WPDATA != (~WP_PAGES))
//       WpStatus = RESET;
//
//    RdpStatus = FLASH_GetReadOutProtectionStatus();
//    
//    if(!RdpStatus || !WpStatus)
//    {        
//        FLASH_Unlock();        
//        
//        if(FLASH_ReadOutProtection(ENABLE) != FLASH_COMPLETE)
//            return S_FAIL;
//        
//        // Options Byte is erased in FLASH_ReadOutProtection()        
//        if(WRP0_Data != 0xFF)
//        {
//            if(FLASH_ProgramOptionByteData((u32)&(OB->WRP0), WRP0_Data) != FLASH_COMPLETE)
//                return S_FAIL;
//        }
//        if(WRP1_Data != 0xFF)
//        {
//            if(FLASH_ProgramOptionByteData((u32)&(OB->WRP1), WRP1_Data) != FLASH_COMPLETE)
//                return S_FAIL;
//        }
//        if(WRP2_Data != 0xFF)
//        {
//            if(FLASH_ProgramOptionByteData((u32)&(OB->WRP2), WRP2_Data) != FLASH_COMPLETE)
//                return S_FAIL;
//        }
//        if(WRP3_Data != 0xFF)
//        {
//            if(FLASH_ProgramOptionByteData((u32)&(OB->WRP3), WRP3_Data) != FLASH_COMPLETE)
//                return S_FAIL;
//        }
//      
//        NVIC_SystemReset(); // Reset so Opt Bytes will take effect
//        while(1);
//    }
//    
//    return S_SUCCESS;
}
