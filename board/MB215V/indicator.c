/**
  **************** (C) COPYRIGHT 2011 SCT Performance, LLC *********************
  * File Name          : indicator.c
  * Device / System    : SCT products using the "CommonCode" platform
  * Author             : Quyen Leba
  *
  * Version            : 1 
  * Date               : 05/02/2011
  * Description        : 
  *                    : 
  *
  *
  * History            : 05/02/2011 M. Davis
  *                    :   Add SCT Copyright header,
  *
  ******************************************************************************
  */

#include <device_config.h>
#include <common/statuscode.h>
#include <board/delays.h>
#include <board/indicator.h>
#include "bluetooth.h"
#include "led.h"

struct
{
    u8  current_state;
}indicator_info =
{
    .current_state = Indicator_None,
};

//------------------------------------------------------------------------------
// Clear any low-level indications
// Return:  u8  status
// Engineer: Quyen Leba
//------------------------------------------------------------------------------
u8 indicator_clear()
{
    if (led_isnormaloperationmode())
    {
        led_setState(LED_OFF);
    }
    indicator_info.current_state = Indicator_None;
    return S_SUCCESS;
}

//------------------------------------------------------------------------------
// Show link status
// Return:  u8  status
// Engineer: Quyen Leba
//------------------------------------------------------------------------------
u8 indicator_link_status()
{
    bluetooth_status_indicator_update();
    return S_SUCCESS;
}

//------------------------------------------------------------------------------
// Restore to current state
// Return:  u8  status
// Engineer: Quyen Leba
//------------------------------------------------------------------------------
u8 indicator_restore()
{
    return indicator_set((Indicator)indicator_info.current_state);
}

//------------------------------------------------------------------------------
// Set the normal operation mode
// Input:  bool normal
// Engineer: Patrick Downs
//------------------------------------------------------------------------------
void indicator_set_normaloperation(bool normal)
{
    led_setoperationmode(normal);
}

//------------------------------------------------------------------------------
// Get the normal operation mode
// Return:  bool normal
// Engineer: Patrick Downs
//------------------------------------------------------------------------------
bool indicator_get_normaloperation()
{
    return led_isnormaloperationmode();
}

//------------------------------------------------------------------------------
// Set low-level indication for some important task
// Input:   Indicator indicator
// Return:  u8  status
// Engineer: Quyen Leba
//------------------------------------------------------------------------------
u8 indicator_set(Indicator indicator)
{
    indicator_info.current_state = (u8)indicator;

    if (!led_isnormaloperationmode())
    {
        //not allow to change LEDs
        return S_SUCCESS;
    }

    switch(indicator)
    {
    case Indicator_SettingsError:
        break;
    case Indicator_BackupSettingsError:
        break;
    case Indicator_FirmwareError:
        break;
    case Indicator_FilesystemError:
        break;
    case Indicator_FailsafeBoot:
        led_setState(LED_RED); delays(100,'m'); led_setState(LED_OFF); delays(100,'m');
        led_setState(LED_RED); delays(100,'m'); led_setState(LED_OFF); delays(100,'m');
        led_setState(LED_RED); delays(100,'m'); led_setState(LED_OFF);
        break;
    case Indicator_MainBoot:
        led_setState(LED_RED); delays(100,'m'); led_setState(LED_OFF); delays(100,'m');
        led_setState(LED_RED); delays(100,'m'); led_setState(LED_OFF);
        break;
    case Indicator_EnterApplicationLocked:
#if CONFIG_FOR_TEST
        led_setState(LED_RED); delays(300,'m');
        led_setState(LED_GREEN); delays(300,'m');
        led_setState(LED_BLUE); delays(300,'m');
#else
        led_setState(LED_GREEN); delays(100,'m'); led_setState(LED_OFF); delays(100,'m');
        led_setState(LED_GREEN); delays(100,'m'); led_setState(LED_OFF); delays(100,'m');
        led_setState(LED_BLUE); delays(100,'m'); led_setState(LED_OFF);
#endif
        break;
    case Indicator_EnterApplicationUnlocked:
#if CONFIG_FOR_TEST
        led_setState(LED_RED); delays(300,'m');
        led_setState(LED_GREEN); delays(300,'m');
        led_setState(LED_BLUE); delays(300,'m');
#else
        led_setState(LED_GREEN); delays(100,'m'); led_setState(LED_OFF); delays(100,'m');
        led_setState(LED_GREEN); delays(100,'m'); led_setState(LED_OFF); delays(100,'m');
        led_setState(LED_GREEN); delays(100,'m'); led_setState(LED_OFF);
#endif
        break;
    case Indicator_LinkConnected:
#if !CONFIG_FOR_TEST
        led_setState(LED_PURPLE_BLINK_REGULAR);
#endif
        break;
    case Indicator_LinkOpened:
#if !CONFIG_FOR_TEST
        led_setState(LED_PURPLE_BLINK_SLOW);
#endif
        break;
    case Indicator_UploadStock:
        led_setState(LED_RED_BLINK_SLOW);
        break;
    case Indicator_DownloadTune_Erasing:
        led_setState(LED_RED_BLINK_REGULAR);
        break;
    case Indicator_DownloadTune_Programming:
        led_setState(LED_RED_BLINK_FAST);
        break;
    case Indicator_Datalog_Setup:
        led_setState(LED_BLUE_BLINK_REGULAR);
        break;
    case Indicator_Datalog_Active:
        led_setState(LED_BLUE_BLINK_FAST);
        break;
    case Indicator_Datalog_CommLost:
        led_setState(LED_BLUE_BLINK_SLOW);
        break;
    case Indicator_TaskInProgress:
        led_setState(LED_YELLOW_BLINK_REGULAR);
        break;
    case Indicator_TaskSuccess:
        led_setState(LED_GREEN);
        break;
    case Indicator_TaskFail:
        led_setState(LED_RED);
        break;        
    case Indicator_None:
    default:
        indicator_info.current_state = Indicator_None;
        led_setState(LED_OFF);
        break;
    }
    return S_SUCCESS;
}
