/**
  **************** (C) COPYRIGHT 2011 SCT Performance, LLC *********************
  * File Name          : usart.h
  * Device / System    : SCT products using the "CommonCode" platform
  * Author             : Quyen Leba
  *
  * Version            : 1 
  * Date               : 05/02/2011
  * Description        : 
  *                    : 
  *
  *
  * History            : 05/02/2011 M. Davis
  *                    :   Add SCT Copyright header,
  *
  ******************************************************************************
  */

#ifndef __USART_H
#define __USART_H

#include <arch/gentype.h>

typedef enum
{
    BAUD9600    = 9600,
    BAUD115200  = 115200,
    BAUD230400  = 230400,
    BAUD460800  = 460800,
    BAUD921600  = 921600,
}USARTBAUD;

#define UART_USE_FLOWCONTROL            TRUE
#define UART_NO_FLOWCONTROL             FALSE
#define UART_USE_INTERRUPT              TRUE
#define UART_NO_INTERRUPT               FALSE

#endif	//__USART_H
