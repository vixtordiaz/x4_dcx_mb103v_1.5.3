/**
  **************** (C) COPYRIGHT 2011 SCT Performance, LLC *********************
  * File Name          : usartA.h
  * Device / System    : SCT products using the "CommonCode" platform
  * Author             : Quyen Leba
  *
  * Version            : 1 
  * Date               : 05/02/2011
  * Description        : 
  *                    : 
  *
  *
  * History            : 05/02/2011 M. Davis
  *                    :   Add SCT Copyright header,
  *
  ******************************************************************************
  */

#ifndef __USARTA_H
#define __USARTA_H

#include <arch/gentype.h>
#include "usart.h"

#define USARTA_NAME             USART1
#define USARTA_PORT             GPIOA
#define USARTA_GPIO_AF          GPIO_AF_USART1
#define USARTA_TX_PIN           GPIO_Pin_9
#define USARTA_RX_PIN           GPIO_Pin_10
#define USARTA_CTS_PIN          GPIO_Pin_11
#define USARTA_RTS_PIN          GPIO_Pin_12
#define USARTA_TX_PIN_SOURCE    GPIO_PinSource9
#define USARTA_RX_PIN_SOURCE    GPIO_PinSource10
#define USARTA_CTS_PIN_SOURCE   GPIO_PinSource11
#define USARTA_RTS_PIN_SOURCE   GPIO_PinSource12

void usartA_init(USARTBAUD baud, bool useFlowControl, bool useInterrupt);
void usartA_disable_interrupt();
void usartA_enable_interrupt();
void usartA_changebaudrate(USARTBAUD baud);
#define usartA_tx(databyte)     USART_SendData(USARTA_NAME, databyte);    \
    while(USART_GetFlagStatus(USARTA_NAME, USART_FLAG_TXE) == RESET);
#define usartA_txnowait(databyte)   USART_SendData(USARTA_NAME, databyte)
#define usartA_rx()             USART_ReceiveData(USARTA_NAME)
#define usartA_getitstatus()    USART_GetITStatus(USARTA_NAME, USART_IT_RXNE)
#define usartA_hasdata()        (USART_GetFlagStatus(USARTA_NAME, USART_FLAG_RXNE) != RESET)
#define usartA_readytosend()    (USART_GetFlagStatus(USARTA_NAME, USART_FLAG_TXE) != RESET)

#endif	//__USARTA_H
