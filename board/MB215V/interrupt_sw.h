/**
  **************** (C) COPYRIGHT 2011 SCT Performance, LLC *********************
  * File Name          : interrupt_sw.h
  * Device / System    : SCT products using the "CommonCode" platform
  * Author             : Quyen Leba
  *
  * Version            : 1 
  * Date               : 05/02/2011
  * Description        : 
  *                    : 
  *
  *
  * History            : 05/02/2011 M. Davis
  *                    :   Add SCT Copyright header,
  *
  ******************************************************************************
  */

#include <misc.h>
#include <board/genplatform.h>
#include <board/interrupt.h>

#ifndef __INTERRUPT_SW_H
#define __INTERRUPT_SW_H

typedef void(*interrupt_sw_func)(void);

typedef enum
{
    SoftwareIntrTrigger_0           = 0,
    SoftwareIntrTrigger_1           = 1,
    SoftwareIntrTrigger_2           = 2,
}SoftwareIntrTrigger;

void interrupt_sw_init();
u8 interrupt_sw_trigger(SoftwareIntrTrigger trigger);
u8 interrupt_sw_trigger_set_func(SoftwareIntrTrigger trigger,
                                 interrupt_sw_func func, bool fire_once);
u8 interrupt_sw_trigger_clear_func(SoftwareIntrTrigger trigger);
void interrupt_sw_trigger_handler_0();
void interrupt_sw_trigger_handler_1();
void interrupt_sw_trigger_handler_2();

#endif  //__INTERRUPT_SW_H
