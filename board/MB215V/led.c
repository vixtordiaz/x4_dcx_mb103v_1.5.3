/**
  **************** (C) COPYRIGHT 2011 SCT Performance, LLC *********************
  * File Name          : led.c
  * Device / System    : SCT products using the "CommonCode" platform
  * Author             : Quyen Leba
  *
  * Version            : 1 
  * Date               : 05/02/2011
  * Description        : 
  *                    : 
  *
  *
  * History            : 05/02/2011 M. Davis
  *                    :   Add SCT Copyright header,
  *
  ******************************************************************************
  */

#include <board/indicator.h>
#include "timer.h"
#include "led.h"

#define LED_SUPPORT_PWM         1

#define LED_GPIO_CLK            RCC_AHB1Periph_GPIOD
#define LED_GPIO_PORT           GPIOD
#define LED_RED_PIN             GPIO_Pin_13
#define LED_GREEN_PIN           GPIO_Pin_14
#define LED_BLUE_PIN            GPIO_Pin_15
#define LED_ALL_PIN             LED_RED_PIN | LED_GREEN_PIN | LED_BLUE_PIN

#define LED_GPIO_AF             GPIO_AF_TIM4
#define LED_RED_PIN_SOURCE      GPIO_PinSource13
#define LED_GREEN_PIN_SOURCE    GPIO_PinSource14
#define LED_BLUE_PIN_SOURCE     GPIO_PinSource15

#define LED_RED_PWM_CH          2
#define LED_GREEN_PWM_CH        3
#define LED_BLUE_PWM_CH         4
#define LED_ON_PERIOD           5

struct
{
    u8  use_pwm     : 1;
    u8  normal_mode : 1;
    u8  reserved    : 6;
}led_info =
{
    .use_pwm = 0,
    .normal_mode = 1,
};

//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
void led_init(bool enable_pwm)
{
    GPIO_InitTypeDef GPIO_InitStructure;

    led_info.use_pwm = FALSE;
    if (enable_pwm)
    {
        led_info.use_pwm = TRUE;

        timer4_init(TIMER_PRESCALE_REGULAR);
        GPIO_InitStructure.GPIO_Pin =  LED_ALL_PIN;
        GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AF;
        GPIO_InitStructure.GPIO_Speed = GPIO_Speed_100MHz;
        GPIO_InitStructure.GPIO_OType = GPIO_OType_PP;
        GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_UP;
        GPIO_Init(LED_GPIO_PORT, &GPIO_InitStructure);

        GPIO_PinAFConfig(LED_GPIO_PORT, LED_RED_PIN_SOURCE, LED_GPIO_AF);
        GPIO_PinAFConfig(LED_GPIO_PORT, LED_GREEN_PIN_SOURCE, LED_GPIO_AF); 
        GPIO_PinAFConfig(LED_GPIO_PORT, LED_BLUE_PIN_SOURCE, LED_GPIO_AF);
    }
    else
    {
        //config as outputs
        GPIO_InitStructure.GPIO_Pin = LED_ALL_PIN;
        GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
        GPIO_InitStructure.GPIO_Mode = GPIO_Mode_OUT;
        GPIO_InitStructure.GPIO_OType = GPIO_OType_PP;
        GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_UP;
        GPIO_Init(LED_GPIO_PORT, &GPIO_InitStructure);
    }
    led_setState(LED_OFF);
}

//------------------------------------------------------------------------------
// Set LED operation mode
// Input:   bool normal (TRUE: normal status indicator, FALSE: taken over)
// If FALSE, indicator functions can't change LEDs
//------------------------------------------------------------------------------
void led_setoperationmode(bool normal)
{
    led_info.normal_mode = 0;
    if (normal)
    {
        led_info.normal_mode = 1;
    }
}


//------------------------------------------------------------------------------
// Check if LED in normal operation mode
// Return: bool (TRUE: normal)
//------------------------------------------------------------------------------
bool led_isnormaloperationmode()
{
    return (bool)led_info.normal_mode;
}

//------------------------------------------------------------------------------
// Very basic LED to indicate emergency situations such as clock init failed,
// system crashed, etc
// This function never return
//------------------------------------------------------------------------------
void led_emergency_indicator_system_error()
{
    GPIO_InitTypeDef GPIO_InitStructure;
    u32 i;

    RCC_AHB1PeriphClockCmd(LED_GPIO_CLK,ENABLE);

    //config all as GPIO outputs
    GPIO_InitStructure.GPIO_Pin = LED_ALL_PIN;
    GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
    GPIO_InitStructure.GPIO_Mode = GPIO_Mode_OUT;
    GPIO_InitStructure.GPIO_OType = GPIO_OType_PP;
    GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_UP;
    GPIO_Init(LED_GPIO_PORT, &GPIO_InitStructure);

    while(1)
    {
        //alternating RED - WHITE
        GPIO_SetBits(LED_GPIO_PORT, LED_ALL_PIN);
        GPIO_ResetBits(LED_GPIO_PORT, LED_RED_PIN);
        for(i=0;i<0x3FFFFF;i++);
        GPIO_ResetBits(LED_GPIO_PORT, LED_ALL_PIN);
        for(i=0;i<0x3FFFFF;i++);
    }
}

//------------------------------------------------------------------------------
// Very basic LED to indicate error in settings
// This function never return
//------------------------------------------------------------------------------
void led_emergency_indicator_settings_error()
{
    GPIO_InitTypeDef GPIO_InitStructure;
    u32 i;

    RCC_AHB1PeriphClockCmd(LED_GPIO_CLK,ENABLE);

    //config all as GPIO outputs
    GPIO_InitStructure.GPIO_Pin = LED_ALL_PIN;
    GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
    GPIO_InitStructure.GPIO_Mode = GPIO_Mode_OUT;
    GPIO_InitStructure.GPIO_OType = GPIO_OType_PP;
    GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_UP;
    GPIO_Init(LED_GPIO_PORT, &GPIO_InitStructure);

    GPIO_SetBits(LED_GPIO_PORT, LED_ALL_PIN);
    while(1)
    {
        //alternating RED - off - RED - off - WHITE - off
        GPIO_ResetBits(LED_GPIO_PORT, LED_RED_PIN);
        for(i=0;i<0x3FFFFF;i++);
        GPIO_SetBits(LED_GPIO_PORT, LED_ALL_PIN);   //off
        for(i=0;i<0x3FFFFF;i++);
        GPIO_ResetBits(LED_GPIO_PORT, LED_RED_PIN);
        for(i=0;i<0x3FFFFF;i++);
        GPIO_SetBits(LED_GPIO_PORT, LED_ALL_PIN);   //off
        for(i=0;i<0x3FFFFF;i++);
        GPIO_ResetBits(LED_GPIO_PORT, LED_ALL_PIN);
        for(i=0;i<0x3FFFFF;i++);
        GPIO_SetBits(LED_GPIO_PORT, LED_ALL_PIN);   //off
        for(i=0;i<0x3FFFFF;i++);
    }
}

//------------------------------------------------------------------------------
// Very basic LED to indicate error in firmware version
// This function never return
//------------------------------------------------------------------------------
void led_emergency_indicator_version_error()
{
    GPIO_InitTypeDef GPIO_InitStructure;
    u32 i;

    RCC_AHB1PeriphClockCmd(LED_GPIO_CLK,ENABLE);

    //config all as GPIO outputs
    GPIO_InitStructure.GPIO_Pin = LED_ALL_PIN;
    GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
    GPIO_InitStructure.GPIO_Mode = GPIO_Mode_OUT;
    GPIO_InitStructure.GPIO_OType = GPIO_OType_PP;
    GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_UP;
    GPIO_Init(LED_GPIO_PORT, &GPIO_InitStructure);

    GPIO_SetBits(LED_GPIO_PORT, LED_ALL_PIN);
    while(1)
    {
        //alternating WHITE - off - WHITE - off - RED - off
        GPIO_ResetBits(LED_GPIO_PORT, LED_ALL_PIN);
        for(i=0;i<0x3FFFFF;i++);
        GPIO_SetBits(LED_GPIO_PORT, LED_ALL_PIN);   //off
        for(i=0;i<0x3FFFFF;i++);
        
        GPIO_ResetBits(LED_GPIO_PORT, LED_ALL_PIN);
        for(i=0;i<0x3FFFFF;i++);
        GPIO_SetBits(LED_GPIO_PORT, LED_ALL_PIN);   //off
        for(i=0;i<0x3FFFFF;i++);
        
        GPIO_ResetBits(LED_GPIO_PORT, LED_RED_PIN);
        for(i=0;i<0x3FFFFF;i++);
        GPIO_SetBits(LED_GPIO_PORT, LED_ALL_PIN);   //off
        for(i=0;i<0x3FFFFF;i++);
    }
}

//------------------------------------------------------------------------------
// Very basic LED to indicate error in stack (stack overflow)
// This function never return
//------------------------------------------------------------------------------
void led_emergency_indicator_stack_error()
{
    GPIO_InitTypeDef GPIO_InitStructure;
    u32 i;

    RCC_APB2PeriphClockCmd(LED_GPIO_CLK,ENABLE);

    //config all as GPIO outputs
    GPIO_InitStructure.GPIO_Pin = LED_ALL_PIN;
    GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
    GPIO_InitStructure.GPIO_Mode = GPIO_Mode_Out_PP;
    GPIO_Init(LED_GPIO_PORT, &GPIO_InitStructure);

    GPIO_SetBits(LED_GPIO_PORT, LED_ALL_PIN);
    while(1)
    {
        //alternating RED - off - RED - off - BLUE - off - WHITE - off
        GPIO_ResetBits(LED_GPIO_PORT, LED_RED_PIN);
        for(i=0;i<0x3FFFFF;i++);
        GPIO_SetBits(LED_GPIO_PORT, LED_ALL_PIN);   //off
        for(i=0;i<0x3FFFFF;i++);

        GPIO_ResetBits(LED_GPIO_PORT, LED_RED_PIN);
        for(i=0;i<0x3FFFFF;i++);
        GPIO_SetBits(LED_GPIO_PORT, LED_ALL_PIN);   //off
        for(i=0;i<0x3FFFFF;i++);

        GPIO_ResetBits(LED_GPIO_PORT, LED_BLUE_PIN);
        for(i=0;i<0x3FFFFF;i++);
        GPIO_SetBits(LED_GPIO_PORT, LED_ALL_PIN);   //off
        for(i=0;i<0x3FFFFF;i++);

        GPIO_ResetBits(LED_GPIO_PORT, LED_ALL_PIN);
        for(i=0;i<0x3FFFFF;i++);
        GPIO_SetBits(LED_GPIO_PORT, LED_ALL_PIN);   //off
        for(i=0;i<0x3FFFFF;i++);
    }
}

//------------------------------------------------------------------------------
// Very basic LED to indicate error in heap (mem leak, overrun)
// This function never return
//------------------------------------------------------------------------------
void led_emergency_indicator_heap_error()
{
    GPIO_InitTypeDef GPIO_InitStructure;
    u32 i;

    RCC_APB2PeriphClockCmd(LED_GPIO_CLK,ENABLE);

    //config all as GPIO outputs
    GPIO_InitStructure.GPIO_Pin = LED_ALL_PIN;
    GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
    GPIO_InitStructure.GPIO_Mode = GPIO_Mode_Out_PP;
    GPIO_Init(LED_GPIO_PORT, &GPIO_InitStructure);

    GPIO_SetBits(LED_GPIO_PORT, LED_ALL_PIN);
    while(1)
    {
        //alternating RED - off - RED - off - GREEN - off - WHITE - off
        GPIO_ResetBits(LED_GPIO_PORT, LED_RED_PIN);
        for(i=0;i<0x3FFFFF;i++);
        GPIO_SetBits(LED_GPIO_PORT, LED_ALL_PIN);   //off
        for(i=0;i<0x3FFFFF;i++);

        GPIO_ResetBits(LED_GPIO_PORT, LED_RED_PIN);
        for(i=0;i<0x3FFFFF;i++);
        GPIO_SetBits(LED_GPIO_PORT, LED_ALL_PIN);   //off
        for(i=0;i<0x3FFFFF;i++);

        GPIO_ResetBits(LED_GPIO_PORT, LED_GREEN_PIN);
        for(i=0;i<0x3FFFFF;i++);
        GPIO_SetBits(LED_GPIO_PORT, LED_ALL_PIN);   //off
        for(i=0;i<0x3FFFFF;i++);

        GPIO_ResetBits(LED_GPIO_PORT, LED_ALL_PIN);
        for(i=0;i<0x3FFFFF;i++);
        GPIO_SetBits(LED_GPIO_PORT, LED_ALL_PIN);   //off
        for(i=0;i<0x3FFFFF;i++);
    }
}

//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
void led_setState(LED_State state)
{
    if (led_info.use_pwm)
    {
        u8 R=100,G=100,B=100;
        timer4_pwm_duty_cycle(LED_RED_PWM_CH,R);
        timer4_pwm_duty_cycle(LED_GREEN_PWM_CH,G);
        timer4_pwm_duty_cycle(LED_BLUE_PWM_CH,B);

        timer4_init(TIMER_PRESCALE_REGULAR);
        switch(state)
        {
        case LED_OFF:
            timer4_pwm_duty_cycle(LED_RED_PWM_CH,100);
            timer4_pwm_duty_cycle(LED_GREEN_PWM_CH,100);
            timer4_pwm_duty_cycle(LED_BLUE_PWM_CH,100);
            break;
        case LED_RED:
            timer4_pwm_duty_cycle(LED_RED_PWM_CH,0);
            timer4_pwm_duty_cycle(LED_GREEN_PWM_CH,100);
            timer4_pwm_duty_cycle(LED_BLUE_PWM_CH,100);
            break;
        case LED_GREEN:
            timer4_pwm_duty_cycle(LED_RED_PWM_CH,100);
            timer4_pwm_duty_cycle(LED_GREEN_PWM_CH,0);
            timer4_pwm_duty_cycle(LED_BLUE_PWM_CH,100);
            break;
        case LED_BLUE:
            timer4_pwm_duty_cycle(LED_RED_PWM_CH,100);
            timer4_pwm_duty_cycle(LED_GREEN_PWM_CH,100);
            timer4_pwm_duty_cycle(LED_BLUE_PWM_CH,0);
            break;
        case LED_PURPLE:
            timer4_pwm_duty_cycle(LED_RED_PWM_CH,0);
            timer4_pwm_duty_cycle(LED_GREEN_PWM_CH,100);
            timer4_pwm_duty_cycle(LED_BLUE_PWM_CH,0);
            break;
        case LED_YELLOW:
            timer4_pwm_duty_cycle(LED_RED_PWM_CH,0);
            timer4_pwm_duty_cycle(LED_GREEN_PWM_CH,0);
            timer4_pwm_duty_cycle(LED_BLUE_PWM_CH,100);
            break;
        case LED_WHITE:
            timer4_pwm_duty_cycle(LED_RED_PWM_CH,0);
            timer4_pwm_duty_cycle(LED_GREEN_PWM_CH,0);
            timer4_pwm_duty_cycle(LED_BLUE_PWM_CH,0);
            break;
        case LED_BLUERED:
            timer4_pwm_duty_cycle(LED_RED_PWM_CH,0);
            timer4_pwm_duty_cycle(LED_GREEN_PWM_CH,100);
            timer4_pwm_duty_cycle(LED_BLUE_PWM_CH,0);
            break;
        case LED_BLUEGREEN:
            timer4_pwm_duty_cycle(LED_RED_PWM_CH,100);
            timer4_pwm_duty_cycle(LED_GREEN_PWM_CH,0);
            timer4_pwm_duty_cycle(LED_BLUE_PWM_CH,0);
            break;
        case LED_BLUE_BLINK_FAST:
            timer4_init(TIMER_PRESCALE_FAST);
            timer4_pwm_duty_cycle(LED_RED_PWM_CH,100);
            timer4_pwm_duty_cycle(LED_GREEN_PWM_CH,100);
            timer4_pwm_duty_cycle(LED_BLUE_PWM_CH,LED_ON_PERIOD);
            break;
        case LED_BLUE_BLINK_REGULAR:
            timer4_init(TIMER_PRESCALE_REGULAR);
            timer4_pwm_duty_cycle(LED_RED_PWM_CH,100);
            timer4_pwm_duty_cycle(LED_GREEN_PWM_CH,100);
            timer4_pwm_duty_cycle(LED_BLUE_PWM_CH,LED_ON_PERIOD);
            break;
        case LED_BLUE_BLINK_SLOW:
            timer4_init(TIMER_PRESCALE_SLOW);
            timer4_pwm_duty_cycle(LED_RED_PWM_CH,100);
            timer4_pwm_duty_cycle(LED_GREEN_PWM_CH,100);
            timer4_pwm_duty_cycle(LED_BLUE_PWM_CH,LED_ON_PERIOD);
            break;
        case LED_GREEN_BLINK_FAST:
            timer4_init(TIMER_PRESCALE_FAST);
            timer4_pwm_duty_cycle(LED_RED_PWM_CH,100);
            timer4_pwm_duty_cycle(LED_GREEN_PWM_CH,LED_ON_PERIOD);
            timer4_pwm_duty_cycle(LED_BLUE_PWM_CH,100);
            break;
        case LED_GREEN_BLINK_REGULAR:
            timer4_init(TIMER_PRESCALE_REGULAR);
            timer4_pwm_duty_cycle(LED_RED_PWM_CH,100);
            timer4_pwm_duty_cycle(LED_GREEN_PWM_CH,LED_ON_PERIOD);
            timer4_pwm_duty_cycle(LED_BLUE_PWM_CH,100);
            break;
        case LED_GREEN_BLINK_SLOW:
            timer4_init(TIMER_PRESCALE_SLOW);
            timer4_pwm_duty_cycle(LED_RED_PWM_CH,100);
            timer4_pwm_duty_cycle(LED_GREEN_PWM_CH,LED_ON_PERIOD);
            timer4_pwm_duty_cycle(LED_BLUE_PWM_CH,100);
            break;
        case LED_RED_BLINK_FAST:
            timer4_init(TIMER_PRESCALE_FAST);
            timer4_pwm_duty_cycle(LED_RED_PWM_CH,LED_ON_PERIOD);
            timer4_pwm_duty_cycle(LED_GREEN_PWM_CH,100);
            timer4_pwm_duty_cycle(LED_BLUE_PWM_CH,100);
            break;
        case LED_RED_BLINK_REGULAR:
            timer4_init(TIMER_PRESCALE_REGULAR);
            timer4_pwm_duty_cycle(LED_RED_PWM_CH,LED_ON_PERIOD);
            timer4_pwm_duty_cycle(LED_GREEN_PWM_CH,100);
            timer4_pwm_duty_cycle(LED_BLUE_PWM_CH,100);
            break;
        case LED_RED_BLINK_SLOW:
            timer4_init(TIMER_PRESCALE_SLOW);
            timer4_pwm_duty_cycle(LED_RED_PWM_CH,LED_ON_PERIOD);
            timer4_pwm_duty_cycle(LED_GREEN_PWM_CH,100);
            timer4_pwm_duty_cycle(LED_BLUE_PWM_CH,100);
            break;
        case LED_PURPLE_BLINK_FAST:
            timer4_init(TIMER_PRESCALE_FAST);
            timer4_pwm_duty_cycle(LED_RED_PWM_CH,LED_ON_PERIOD);
            timer4_pwm_duty_cycle(LED_GREEN_PWM_CH,100);
            timer4_pwm_duty_cycle(LED_BLUE_PWM_CH,LED_ON_PERIOD);
            break;
        case LED_PURPLE_BLINK_REGULAR:
            timer4_init(TIMER_PRESCALE_REGULAR);
            timer4_pwm_duty_cycle(LED_RED_PWM_CH,LED_ON_PERIOD);
            timer4_pwm_duty_cycle(LED_GREEN_PWM_CH,100);
            timer4_pwm_duty_cycle(LED_BLUE_PWM_CH,LED_ON_PERIOD);
            break;
        case LED_PURPLE_BLINK_SLOW:
            timer4_init(TIMER_PRESCALE_SLOW);
            timer4_pwm_duty_cycle(LED_RED_PWM_CH,LED_ON_PERIOD);
            timer4_pwm_duty_cycle(LED_GREEN_PWM_CH,100);
            timer4_pwm_duty_cycle(LED_BLUE_PWM_CH,LED_ON_PERIOD);
            break;
        case LED_YELLOW_BLINK_FAST:
            timer4_init(TIMER_PRESCALE_FAST);
            timer4_pwm_duty_cycle(LED_RED_PWM_CH,LED_ON_PERIOD);
            timer4_pwm_duty_cycle(LED_GREEN_PWM_CH,LED_ON_PERIOD);
            timer4_pwm_duty_cycle(LED_BLUE_PWM_CH,100);
            break;
        case LED_YELLOW_BLINK_REGULAR:
            timer4_init(TIMER_PRESCALE_REGULAR);
            timer4_pwm_duty_cycle(LED_RED_PWM_CH,LED_ON_PERIOD);
            timer4_pwm_duty_cycle(LED_GREEN_PWM_CH,LED_ON_PERIOD);
            timer4_pwm_duty_cycle(LED_BLUE_PWM_CH,100);
            break;
        case LED_YELLOW_BLINK_SLOW:
            timer4_init(TIMER_PRESCALE_SLOW);
            timer4_pwm_duty_cycle(LED_RED_PWM_CH,LED_ON_PERIOD);
            timer4_pwm_duty_cycle(LED_GREEN_PWM_CH,LED_ON_PERIOD);
            timer4_pwm_duty_cycle(LED_BLUE_PWM_CH,100);
            break;            
        default:
            break;
        }
    }
    else
    {
        switch(state)
        {
        case LED_OFF:
            GPIO_SetBits(LED_GPIO_PORT, LED_ALL_PIN);
            break;
        case LED_RED:
            GPIO_SetBits(LED_GPIO_PORT, LED_ALL_PIN);
            GPIO_ResetBits(LED_GPIO_PORT, LED_RED_PIN);
            break;
        case LED_GREEN:
            GPIO_SetBits(LED_GPIO_PORT, LED_ALL_PIN);
            GPIO_ResetBits(LED_GPIO_PORT, LED_GREEN_PIN);
            break;
        case LED_BLUE:
            GPIO_SetBits(LED_GPIO_PORT, LED_ALL_PIN);
            GPIO_ResetBits(LED_GPIO_PORT, LED_BLUE_PIN);
            break;
        case LED_PURPLE:
            GPIO_SetBits(LED_GPIO_PORT, LED_ALL_PIN);
            GPIO_ResetBits(LED_GPIO_PORT, LED_RED_PIN | LED_BLUE_PIN);
            break;
        case LED_YELLOW:
            GPIO_SetBits(LED_GPIO_PORT, LED_ALL_PIN);
            GPIO_ResetBits(LED_GPIO_PORT, LED_RED_PIN | LED_GREEN_PIN);
            break;
        case LED_WHITE:
            GPIO_ResetBits(LED_GPIO_PORT, LED_ALL_PIN);
            break;
        case LED_BLUERED:
            GPIO_SetBits(LED_GPIO_PORT, LED_ALL_PIN);
            GPIO_ResetBits(LED_GPIO_PORT, LED_BLUE_PIN | LED_RED_PIN);
            break;
        case LED_BLUEGREEN:
            GPIO_SetBits(LED_GPIO_PORT, LED_ALL_PIN);
            GPIO_ResetBits(LED_GPIO_PORT, LED_BLUE_PIN | LED_GREEN_PIN);
            break;
        case LED_BLUE_BLINK_FAST:
        case LED_BLUE_BLINK_REGULAR:
        case LED_BLUE_BLINK_SLOW:
        case LED_GREEN_BLINK_FAST:
        case LED_GREEN_BLINK_REGULAR:
        case LED_GREEN_BLINK_SLOW:
        case LED_RED_BLINK_FAST:
        case LED_RED_BLINK_REGULAR:
        case LED_RED_BLINK_SLOW:
        case LED_PURPLE_BLINK_FAST:
        case LED_PURPLE_BLINK_REGULAR:
        case LED_PURPLE_BLINK_SLOW:
            //treat as LED_OFF
            GPIO_SetBits(LED_GPIO_PORT, LED_ALL_PIN);
            break;
        default:
            break;
        }
    }
}

//------------------------------------------------------------------------------
// Cycle through all LEDs
//------------------------------------------------------------------------------
void led_cycle_test()
{
    GPIO_InitTypeDef GPIO_InitStructure;
    u32 i,j;

    indicator_set_normaloperation(FALSE);
    RCC_APB2PeriphClockCmd(LED_GPIO_CLK,ENABLE);

    //config all as GPIO outputs
    GPIO_InitStructure.GPIO_Pin = LED_ALL_PIN;
    GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
    GPIO_InitStructure.GPIO_Mode = GPIO_Mode_OUT;
    GPIO_InitStructure.GPIO_OType = GPIO_OType_PP;
    GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_UP;
    GPIO_Init(LED_GPIO_PORT, &GPIO_InitStructure);

    GPIO_SetBits(LED_GPIO_PORT, LED_ALL_PIN);
    for(j=0;j<5;j++)
    {
        //alternating WHITE - off - WHITE - off - RED - off
        GPIO_ResetBits(LED_GPIO_PORT, LED_RED_PIN);
        for(i=0;i<0x3FFFFF;i++);
        GPIO_SetBits(LED_GPIO_PORT, LED_ALL_PIN);   //off
        for(i=0;i<0x3FFFFF;i++);
        GPIO_ResetBits(LED_GPIO_PORT, LED_GREEN_PIN);
        for(i=0;i<0x3FFFFF;i++);
        GPIO_SetBits(LED_GPIO_PORT, LED_ALL_PIN);   //off
        for(i=0;i<0x3FFFFF;i++);
        GPIO_ResetBits(LED_GPIO_PORT, LED_BLUE_PIN);
        for(i=0;i<0x3FFFFF;i++);
        GPIO_SetBits(LED_GPIO_PORT, LED_ALL_PIN);   //off
        for(i=0;i<0x3FFFFF;i++);
    }

    indicator_set_normaloperation(TRUE);
    led_init(TRUE);
}
