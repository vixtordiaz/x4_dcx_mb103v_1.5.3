/**
  **************** (C) COPYRIGHT 2011 SCT Performance, LLC *********************
  * File Name          : button.c
  * Device / System    : SCT products using the "CommonCode" platform
  * Author             : Quyen Leba
  *
  * Version            : 1 
  * Date               : 05/02/2011
  * Description        : Initialization and interrupt processing for push-button
  *                    : attached to ext interrupt line 0
  *
  *
  * History            : 05/02/2011 M. Davis
  *                    :   Add SCT Copyright header,
  *
  ******************************************************************************
  */

#include <board/genplatform.h>
#include <board/delays.h>
#include "system.h"
#include "bluetooth.h"
#include "button.h"

#define BUTTON_GPIO_PORT            GPIOB
#define BUTTON_GPIO_CLK             RCC_APB2Periph_GPIOB
#define BUTTON_GPIO_PORTSOURCE      EXTI_PortSourceGPIOB
#define BUTTON_GPIO_PIN             GPIO_Pin_14
#define BUTTON_GPIO_PINSOURCE       EXTI_PinSource14
#define BUTTON_EXTI_LINE            EXTI_Line14
#define BUTTON_EXTI_CHANNEL         EXTI15_10_IRQn

struct
{
    u8  pressed     :1;
    u8  triggered   :1;
    u8  locked      :1;
}button_info;

void button_trigger();

//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
void button_init()
{
    GPIO_InitTypeDef GPIO_InitStructure;
    //Configure PB.0, as input pull up
    GPIO_InitStructure.GPIO_Pin = BUTTON_GPIO_PIN;
    GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
    GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IN;
    GPIO_InitStructure.GPIO_OType = GPIO_OType_PP;
    GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_UP;
    GPIO_Init(BUTTON_GPIO_PORT, &GPIO_InitStructure);
    
    button_info.pressed = 0;
    button_info.triggered = 0;
    button_info.locked = 0;
}

//------------------------------------------------------------------------------
// Active LOW button
//------------------------------------------------------------------------------
u8 button_getState(void)
{
    return GPIO_ReadInputDataBit(BUTTON_GPIO_PORT, BUTTON_GPIO_PIN);
}

//------------------------------------------------------------------------------
// Lock button function
// Input:   bool lock (TRUE: lock, FALSE: unlock)
// Engineer: Quyen Leba
//------------------------------------------------------------------------------
void button_lock(bool lock)
{
    button_info.locked = lock;
}

//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
bool button_isTrigger()
{
    u8 istrigger = button_info.triggered;

    button_info.triggered = 0;
    return (bool)istrigger;
}

//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
void button_intr_init()
{
    GPIO_InitTypeDef GPIO_InitStructure;
    EXTI_InitTypeDef EXTI_InitStructure;

    GPIO_InitStructure.GPIO_Pin = BUTTON_GPIO_PIN;
    GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IN;
    GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
    GPIO_InitStructure.GPIO_OType = GPIO_OType_PP;
    GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_UP;
    GPIO_Init(BUTTON_GPIO_PORT, &GPIO_InitStructure);
    
    // Connect Button EXTI Line to Button GPIO Pin
    SYSCFG_EXTILineConfig(BUTTON_GPIO_PORTSOURCE, BUTTON_GPIO_PINSOURCE);
    
    // Configure Button EXTI line
    EXTI_InitStructure.EXTI_Line = BUTTON_EXTI_LINE;
    EXTI_InitStructure.EXTI_Mode = EXTI_Mode_Interrupt;
    EXTI_InitStructure.EXTI_Trigger = EXTI_Trigger_Rising_Falling;
    EXTI_InitStructure.EXTI_LineCmd = ENABLE;
    EXTI_Init(&EXTI_InitStructure);
    
    button_info.pressed = 0;
    button_info.triggered = 0;
}

//------------------------------------------------------------------------------
// Handle the push-button attached to ext interrupt line 14
//------------------------------------------------------------------------------
void button_intr_handler()
{
    if(EXTI_GetITStatus(BUTTON_EXTI_LINE) != RESET)
    {
        if (!button_info.locked)
        {
            if (button_getState())  //RELEASE
            {
                if (button_info.pressed)
                {
                    button_info.triggered = 1;
                }
                button_info.pressed = 0;
            }
            else                    //PRESSED
            {
                button_info.triggered = 0;
                button_info.pressed = 1;
            }

            if (button_info.triggered)
            {
                interrupt_sw_trigger_set_func(SoftwareIntrTrigger_2,
                                              button_trigger,TRUE);
                interrupt_sw_trigger(SoftwareIntrTrigger_2);
            }
        }
        // Clear the Button EXTI line pending bit
        EXTI_ClearITPendingBit(BUTTON_EXTI_LINE);
    }
}

//------------------------------------------------------------------------------
// Used by a software interrupt trigger to handle button functions
// Engineer: Quyen Leba
//------------------------------------------------------------------------------
void button_trigger()
{
    led_setState(LED_BLUE);
    delays(500,'m');
#if __COMMLINK_HAL_BLUETOOTH__
    bluetooth_unpair();
#elif __COMMLINK_HAL_KEN__
    //do nothing now
#elif __COMMLINK_HAL_X4__
    //do nothing now
#else
#error fix me
#endif

    // Loop back control throuh reset button.
//    if(bluetooth_info.loopback)
//    {
//        bluetooth_loopback_config(FALSE); // Turn off
//    }
//    else
//    {
//        bluetooth_loopback_config(TRUE); // Turn On
//    }

    delays(1000,'m');
    led_setState(LED_OFF);
}
