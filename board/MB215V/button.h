/**
  **************** (C) COPYRIGHT 2011 SCT Performance, LLC *********************
  * File Name          : button.h
  * Device / System    : SCT products using the "CommonCode" platform
  * Author             : Quyen Leba
  *
  * Version            : 1 
  * Date               : 05/02/2011
  * Description        : 
  *                    : 
  *
  *
  * History            : 05/02/2011 M. Davis
  *                    :   Add SCT Copyright header,
  *
  ******************************************************************************
  */

#ifndef __BUTTON_H
#define __BUTTON_H

#include <arch/gentype.h>

void button_init();
u8 button_getState(void);
void button_lock(bool lock);
bool button_isTrigger();
void button_intr_init();

#endif  //__BUTTON_H