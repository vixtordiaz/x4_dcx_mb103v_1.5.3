/**
  **************** (C) COPYRIGHT 2011-2014 SCT Performance, LLC ****************
  * File Name          : cc3000_hal.c
  * Device / System    : SCT products using the "CommonCode" platform
  * Author             : Quyen Leba
  *
  * Version            : 1 
  * Date               : 05/02/2011
  * Description        : 
  *                    : 
  *
  ******************************************************************************
*/

#include <device_config.h>
#include <board/delays.h>
#include "hci.h"
#include "cc3000_common.h"
#include "cc3000_hal.h"
#include "spi.h"

#define CC3000_IRQ_GPIO_PORT                    GPIOA
#define CC3000_IRQ_GPIO_PIN                     GPIO_Pin_4
#define CC3000_IRQ_GPIO_PORTSOURCE              GPIO_PortSourceGPIOA
#define CC3000_IRQ_GPIO_PINSOURCE               GPIO_PinSource4
#define CC3000_IRQ_GPIO_EXTI_LINE               EXTI_Line4
#define CC3000_IRQ_GPIO_EXTI_CHANNEL            EXTI4_IRQn

#define CC3000_SW_EN_PORT                       GPIOE
#define CC3000_SW_EN_PIN                        GPIO_Pin_13

#define CC3000_SPI_MASTER                       SPI1
#define CC3000_SPI_MASTER_DR                    SPI1->DR
#define CC3000_SPI_MASTER_DMA_RX_CHANNEL        DMA1_Channel2
#define CC3000_SPI_MASTER_DMA_TX_CHANNEL        DMA1_Channel3

#define SPI_DMA_RX_TCFLAG                       DMA1_FLAG_TC2
#define SPI_DMA_TX_TCFLAG                       DMA1_FLAG_TC3

#define DMA_WINDOW_SIZE                         1024
#define SPI_WINDOW_SIZE                         DMA_WINDOW_SIZE
#define CC3000_READ                             3
#define CC3000_WRITE                            1

#define SPI_HEADER_SIZE                         (5)
#define HEADERS_SIZE_EVNT                       (SPI_HEADER_SIZE + 5)

#define eSPI_STATE_POWERUP                      (0)
#define eSPI_STATE_INITIALIZED                  (1)
#define eSPI_STATE_IDLE                         (2)
#define eSPI_STATE_WRITE_IRQ                    (3)
#define eSPI_STATE_WRITE_FIRST_PORTION          (4)
#define eSPI_STATE_WRITE_EOT                    (5)
#define eSPI_STATE_READ_IRQ                     (6)
#define eSPI_STATE_READ_FIRST_PORTION           (7)
#define eSPI_STATE_READ_EOT                     (8)

#define HI(value)                               (((value) & 0xFF00) >> 8)
#define LO(value)                               ((value) & 0x00FF)

#define CC3000_HAL_ASSERT_SPI_CS()              spi_master_enable_cs1();
#define CC3000_HAL_DEASSERT_SPI_CS()            spi_master_disable_cs1();

#define CC3000_HAL_ENABLE_SPIDMA_TXCOMPLETE()   \
DMA_ITConfig(CC3000_SPI_MASTER_DMA_TX_CHANNEL, DMA_IT_TC, ENABLE)
#define CC3000_HAL_DISABLE_SPIDMA_TXCOMPLETE()  \
DMA_ITConfig(CC3000_SPI_MASTER_DMA_TX_CHANNEL, DMA_IT_TC, DISABLE)

#define CC3000_HAL_ENABLE_SPIDMA_TXCMD()   \
DMA_Cmd(CC3000_SPI_MASTER_DMA_TX_CHANNEL, ENABLE)
#define CC3000_HAL_DISABLE_SPIDMA_TXCMD()   \
DMA_Cmd(CC3000_SPI_MASTER_DMA_TX_CHANNEL, DISABLE)

#define CC3000_HAL_ENABLE_SPIDMA_RXCMD()    \
DMA_Cmd(CC3000_SPI_MASTER_DMA_RX_CHANNEL, ENABLE)
#define CC3000_HAL_DISABLE_SPIDMA_RXCMD()   \
DMA_Cmd(CC3000_SPI_MASTER_DMA_RX_CHANNEL, DISABLE)

typedef void (*gcSpiHandleRx)(void *p);
typedef void (*gcSpiHandleTx)(void);

typedef struct
{
    u32 ulPioPortAddress;
    u32 ulPioSpiPort;
    u32 ulPioSpiCs;
    u32 ulPortInt;
    u32 ulPioSlEnable;
    
    u32 uluDmaPort;
    u32 uluDmaRxChannel;
    u32 uluDmaTxChannel;
    
    u32 ulSsiPort;
    u32 ulSsiPortAddress;
    u32 ulSsiTx;
    u32 ulSsiRx;
    u32 ulSsiClck;
    u32 ulSsiPortInt;
}tSpiHwConfiguration;

typedef struct
{
    gcSpiHandleRx SPIRxHandler;
    
    u16 usTxPacketLength;
    u16 usRxPacketLength;
    volatile u32  ulSpiState;
    u8  *pTxPacket;
    u8  *pRxPacket;
    tSpiHwConfiguration sHwSettings;
}tSpiInformation;

tSpiInformation sSpiInformation;

// The magic number that resides at the end of the TX/RX buffer (1 byte after 
// the allocated size) for the purpose of detection of the overrun. The location
// of the memory where the magic number  resides shall never be written. 
// In case it is written - the overrun occurred and either receive function or
// send function will stuck forever.
#define CC3000_BUFFER_MAGIC_NUMBER (0xDE)

__no_init u8 wlan_rx_buffer[CC3000_RX_BUFFER_SIZE];
__no_init u8 wlan_tx_buffer[CC3000_TX_BUFFER_SIZE];
//__no_init u8 chBuffer[CC3000_RX_BUFFER_SIZE];
//__no_init static u8 ucDMAChannelControlStructure[DMA_CHANNEL_CONTROL_STRUCTURE_SIZE];

//static buffer for 5 bytes of SPI HEADER
u8  tSpiReadHeader[] = {CC3000_READ, 0, 0, 0, 0};

//void SpiWriteDataSynchronous(const unsigned char *data, unsigned short size);
void cc3000_hal_readdata(u8 *data, u16 datalength);
void cc3000_hal_writedataasync(const u8 *data, u16 size);
void SpiDisableInterrupts(void);
void SpiResumeSpi(void);

static u32 cc3000_hal_isspidmastop(u32 ch);
static void cc3000_hal_waitforspitxdone();

void cc3000_hal_spidma_init();

//------------------------------------------------------------------------------
// Check if DMA is stopped
// Input:   u32 ch (SPI_DMA_TX_TCFLAG, SPI_DMA_RX_TCFLAG)
// Return:  SET, RESET
// Engineer: Quyen Leba
//------------------------------------------------------------------------------
static u32 cc3000_hal_isspidmastop(u32 ch)
{
    return DMA_GetFlagStatus(ch);
}

//------------------------------------------------------------------------------
// Init SPI DMA for CC3000
// Engineer: Quyen Leba
//------------------------------------------------------------------------------
void cc3000_hal_spidma_init()
{    
    spi_master_dma_init(FALSE); // FALSE- CC3000 does not support SPI CRC
}

//------------------------------------------------------------------------------
// Read data via dma with interrupt. Generation interrrupt at the end of the
// TX interrupt
// Input:   u32 datalength (# of byte to read)
// Output:  const u8 *data
// Engineer: Quyen Leba
//------------------------------------------------------------------------------
void cc3000_hal_spireaddmainterrupt(const u8 *data, u32 datalength)
{   
    //delay for at least 50 us at the start of every transfer
    delays_counter(DELAYS_COUNTER_10MS/150);

    spi_master_dma_transfer_data((u8*)tSpiReadHeader, (u8*)data, datalength, 
                                 TRUE);
    
    spi_master_dma_transfer_waitforcomplete(1000);
}

//------------------------------------------------------------------------------
// Write data via dma with interrupt. Generate interrrupt at the end of the
// TX interrupt
// Inputs:  const u8 *data
//          u32 datalength (# of byte to read)
// Engineer: Quyen Leba
//------------------------------------------------------------------------------
void cc3000_hal_spiwritedmainterrupt(const u8 *data, u32 datalength)
{    
    //delay for at least 50 us at the start of every transfer
    delays_counter(DELAYS_COUNTER_10MS/150);
    
    spi_master_dma_transfer_data((u8*)data, (u8*)(u8*)sSpiInformation.pRxPacket, datalength, 
                                 TRUE);
    
    spi_master_dma_transfer_waitforcomplete(1000);
}

//------------------------------------------------------------------------------
// Read data via dma. Don't generate interrrupt at the end of the TX interrupt
// Input:   u32 datalength (# of byte to read)
// Output:  const u8 *data
// Engineer: Quyen Leba
//------------------------------------------------------------------------------
void cc3000_hal_spireaddma(const uint8_t *ptrData, uint32_t ulDataSize)
{    
    //delay for at least 50 us at the start of every transfer
    delays_counter(DELAYS_COUNTER_10MS/150);
    
    spi_master_dma_transfer_data((u8*)tSpiReadHeader, (u8*)ptrData, ulDataSize,
                                 FALSE);
    
    spi_master_dma_transfer_waitforcomplete(1000);
}

//------------------------------------------------------------------------------
// Write data via dma. Don't generate interrrupt at the end of the TX interrupt
// Input:   u32 datalength (# of byte to read)
// Output:  const u8 *data
// Engineer: Quyen Leba
//------------------------------------------------------------------------------
void cc3000_hal_spiwritedma(const uint8_t *ptrData, uint32_t ulDataSize)
{    
    //delay for at least 50 us at the start of every transfer
    delays_counter(DELAYS_COUNTER_10MS/150);
    
    spi_master_dma_transfer_data((u8*)ptrData, (u8*)sSpiInformation.pRxPacket, ulDataSize, 
                                 FALSE);
    
    spi_master_dma_transfer_waitforcomplete(1000);
}

//------------------------------------------------------------------------------
// Engineer: Quyen Leba
//------------------------------------------------------------------------------
void SpiInterruptDisable(void)
{
    CC3000_HAL_DISABLE_SPIDMA_TXCOMPLETE();
}

//------------------------------------------------------------------------------
// Engineer: Quyen Leba
//------------------------------------------------------------------------------
void SpiInterruptEnable()
{ 
    CC3000_HAL_ENABLE_SPIDMA_TXCOMPLETE();
}

//------------------------------------------------------------------------------
// Clear transfer complete flags of DMA TX & RX and interrupt flags of SPI TX &
// RX
// Engineer: Quyen Leba
//------------------------------------------------------------------------------
void cc3000_hal_clear_spidma_interrupt_flags()
{
    DMA_ClearFlag(SPI_DMA_TX_TCFLAG | SPI_DMA_RX_TCFLAG);
    SPI_I2S_ClearFlag(CC3000_SPI_MASTER, SPI_I2S_IT_TXE | SPI_I2S_IT_RXNE);
}

//------------------------------------------------------------------------------
// Helper function to config & enable interrupt
// Engineer: Quyen Leba
// Note: called by wlan API
//------------------------------------------------------------------------------
void cc3000_hal_wlaninterruptenable()
{
    EXTI_InitTypeDef EXTI_InitStructure;
    
    GPIO_EXTILineConfig(CC3000_IRQ_GPIO_PORTSOURCE, CC3000_IRQ_GPIO_PINSOURCE);
    
    EXTI_InitStructure.EXTI_Line = CC3000_IRQ_GPIO_EXTI_LINE;
    EXTI_InitStructure.EXTI_Mode = EXTI_Mode_Interrupt;
    EXTI_InitStructure.EXTI_Trigger = EXTI_Trigger_Falling;
    EXTI_InitStructure.EXTI_LineCmd = ENABLE;
    EXTI_Init(&EXTI_InitStructure);
}

//------------------------------------------------------------------------------
// Helper function to disable interrupt
// Engineer: Quyen Leba
// Note: called by wlan API
//------------------------------------------------------------------------------
void cc3000_hal_wlaninterruptdisable()
{
    EXTI_InitTypeDef EXTI_InitStructure;
    
    EXTI_InitStructure.EXTI_Line = CC3000_IRQ_GPIO_EXTI_LINE;
    EXTI_InitStructure.EXTI_Mode = EXTI_Mode_Interrupt;    
    EXTI_InitStructure.EXTI_Trigger = EXTI_Trigger_Falling;
    EXTI_InitStructure.EXTI_LineCmd = DISABLE;
    EXTI_Init(&EXTI_InitStructure);   
}

//------------------------------------------------------------------------------
// Helper function to control CC3000 VBAT_SW_EN
// Input:   u8  val (WLAN_ENABLE: enable CC3000, WLAN_DISABLE)
// Engineer: Quyen Leba
// Note: called by wlan API
//------------------------------------------------------------------------------
void cc3000_hal_writewlanpin(u8 val)
{
    if (val == WLAN_ENABLE)
    {
        cc3000_hal_enable();
    }
    else
    {
        cc3000_hal_disable();
    }
}

//------------------------------------------------------------------------------
// Init hardware interface to CC3000
// Engineer: Quyen Leba
//------------------------------------------------------------------------------
void cc3000_hal_init()
{
    GPIO_InitTypeDef GPIO_InitStructure;
    EXTI_InitTypeDef EXTI_InitStructure;
    
    //Disble Interrupt on IRQ line
    cc3000_hal_wlaninterruptdisable();
    
    //CC3000 VBAT_SW_EN: output active HIGH
    GPIO_InitStructure.GPIO_Pin = CC3000_SW_EN_PIN;
    GPIO_InitStructure.GPIO_Speed = GPIO_Speed_10MHz;
    GPIO_InitStructure.GPIO_Mode = GPIO_Mode_Out_PP;
    GPIO_Init(CC3000_SW_EN_PORT, &GPIO_InitStructure);
    cc3000_hal_disable();
    delays_counter(TIME_LOOP_COUNT_30MS);
    
    //CC3000 SPI_IRQ
    GPIO_InitStructure.GPIO_Pin = CC3000_IRQ_GPIO_PIN;
    GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IPU;
    GPIO_Init(CC3000_IRQ_GPIO_PORT, &GPIO_InitStructure);
    
    GPIO_EXTILineConfig(CC3000_IRQ_GPIO_PORTSOURCE, CC3000_IRQ_GPIO_PINSOURCE);
    
    EXTI_InitStructure.EXTI_Line = CC3000_IRQ_GPIO_EXTI_LINE;
    EXTI_InitStructure.EXTI_Mode = EXTI_Mode_Interrupt;    
    EXTI_InitStructure.EXTI_Trigger = EXTI_Trigger_Falling;
    EXTI_InitStructure.EXTI_LineCmd = ENABLE;
    EXTI_Init(&EXTI_InitStructure);
    
    EXTI_ClearITPendingBit(CC3000_IRQ_GPIO_EXTI_LINE);  
    EXTI_ClearFlag(CC3000_IRQ_GPIO_EXTI_LINE); 
    
    cc3000_hal_spidma_init();
}

//------------------------------------------------------------------------------
// Enable power to CC3000
// Engineer: Quyen Leba
//------------------------------------------------------------------------------
void cc3000_hal_enable()
{
    GPIO_SetBits(CC3000_SW_EN_PORT, CC3000_SW_EN_PIN);
}

//------------------------------------------------------------------------------
// Disable power to CC3000
// Engineer: Quyen Leba
//------------------------------------------------------------------------------
void cc3000_hal_disable()
{
    GPIO_ResetBits(CC3000_SW_EN_PORT, CC3000_SW_EN_PIN);
}

//------------------------------------------------------------------------------
// 
// Engineer: Quyen Leba
// Note: do not rename. Used by CC3000 API.
//------------------------------------------------------------------------------
void SpiClose(void)
{
    if (sSpiInformation.pRxPacket)
    {
        sSpiInformation.pRxPacket = 0;
    }
    
    //disable CC3000 SPI_IRQ
    tSLInformation.WlanInterruptDisable();
}

//------------------------------------------------------------------------------
// 
// Engineer: Quyen Leba
// Note: do not rename. Used by CC3000 API.
//------------------------------------------------------------------------------
void SpiOpen(gcSpiHandleRx pfRxHandler)
{
    sSpiInformation.ulSpiState = eSPI_STATE_POWERUP;
    
    sSpiInformation.SPIRxHandler = pfRxHandler;
    sSpiInformation.usTxPacketLength = 0;
    sSpiInformation.pTxPacket = NULL;
    sSpiInformation.pRxPacket = wlan_rx_buffer;
    sSpiInformation.usRxPacketLength = 0;
    wlan_rx_buffer[CC3000_RX_BUFFER_SIZE - 1] = CC3000_BUFFER_MAGIC_NUMBER;
    wlan_tx_buffer[CC3000_TX_BUFFER_SIZE - 1] = CC3000_BUFFER_MAGIC_NUMBER;
    
    //enable interrupt on the WLAN IRQ
    tSLInformation.WlanInterruptEnable();
}

//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
long SpiFirstWrite(unsigned char *ucBuf, unsigned short usLength)
{   
    //workaround for first transaction
    CC3000_HAL_ASSERT_SPI_CS();

    ////    SpiSysDelay(FWT_DELAY/4);

    cc3000_hal_spiwritedma(ucBuf, 4);
    ////    SpiSysDelay(FWT_DELAY/4);

    //    SpiWriteDataSynchronous(ucBuf + 4, usLength - 4);
    cc3000_hal_spiwritedma(ucBuf + 4, usLength - 4);

    //from this point on, operate in a regular way
    sSpiInformation.ulSpiState = eSPI_STATE_IDLE;

    CC3000_HAL_DEASSERT_SPI_CS();

    return 0;
}

//------------------------------------------------------------------------------
//
// Note: do not rename. Used by CC3000 API.
//------------------------------------------------------------------------------
long SpiWrite(unsigned char *pUserBuffer, unsigned short usLength)
{
    u8 ucPad = 0;
    
    // Figure out the total length of the packet in order to figure out if there is padding or not
    if(!(usLength & 0x0001))
    {
        ucPad++;
    }

    pUserBuffer[0] = CC3000_WRITE;
    pUserBuffer[1] = HI(usLength + ucPad);
    pUserBuffer[2] = LO(usLength + ucPad);
    pUserBuffer[3] = 0;
    pUserBuffer[4] = 0;

    usLength += (SPI_HEADER_SIZE + ucPad);

    // The magic number that resides at the end of the TX/RX buffer (1 byte after the allocated size)
    // for the purpose of detection of the overrun. If the magic number is overwritten - buffer overrun
    // occurred - and we will stuck here forever!
    if (wlan_tx_buffer[CC3000_TX_BUFFER_SIZE - 1] != CC3000_BUFFER_MAGIC_NUMBER)
    {
        while (1)
        {}
    }

    if (sSpiInformation.ulSpiState == eSPI_STATE_POWERUP)
    {
        while (sSpiInformation.ulSpiState != eSPI_STATE_INITIALIZED);
    }

    if (sSpiInformation.ulSpiState == eSPI_STATE_INITIALIZED)
    {
        // This is time for first TX/RX transactions over SPI: the IRQ is down - so need to send read buffer size command
        SpiFirstWrite(pUserBuffer, usLength);
        
        // Due to the fact that we are currently implementing a blocking situation
        // here we will wait till end of transaction
    }
    else 
    {
        // We need to prevent here race that can occur in case 2 back to back packets are sent to the 
        // device, so the state will move to IDLE and once again to not IDLE due to IRQ

        while (sSpiInformation.ulSpiState != eSPI_STATE_IDLE)
        {}

        sSpiInformation.ulSpiState = eSPI_STATE_WRITE_IRQ;
        sSpiInformation.pTxPacket = pUserBuffer;
        sSpiInformation.usTxPacketLength = usLength;

        // Assert the CS line and wait till SSI IRQ line is active and then initialize write operation     
        CC3000_HAL_ASSERT_SPI_CS();
    }

    // Due to the fact that we are currently implementing a blocking situation
    // here we will wait till end of transaction
    while (eSPI_STATE_IDLE != sSpiInformation.ulSpiState)
    {}

    return 0;
}

//------------------------------------------------------------------------------
// Read module data
// Input:   u16 datalength
// Output:  u8  *data
// Engineer: Quyen Leba
//------------------------------------------------------------------------------
void cc3000_hal_readdata(u8 *data, u16 datalength)
{
    cc3000_hal_spireaddmainterrupt(data, datalength);
}

//------------------------------------------------------------------------------
// Write module data (async)
// Input:   u16 datalength
// Output:  u8  *data
// Engineer: Quyen Leba
//------------------------------------------------------------------------------
void cc3000_hal_writedataasync(const u8 *data, u16 datalength)
{
    cc3000_hal_spiwritedma(data, datalength);
}

//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
static void cc3000_hal_waitforspitxdone()
{
    while(SPI_I2S_GetFlagStatus(CC3000_SPI_MASTER, SPI_I2S_FLAG_BSY) != RESET)
    {
        //TODOQK: timeout
    }
}

//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
////void SpiWriteDataSynchronous(const unsigned char *data, unsigned short size)
////{
////}

//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
void SpiReadHeader(void)
{
    sSpiInformation.ulSpiState = eSPI_STATE_READ_IRQ;
    cc3000_hal_readdata(sSpiInformation.pRxPacket, 10);
}

//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
long SpiReadDataCont(void)
{
    long data_to_recv;
    u8 *evnt_buff, type;
    
    //determine what type of packet we have
    evnt_buff =  sSpiInformation.pRxPacket;
    data_to_recv = 0;
    STREAM_TO_UINT8((char *)(evnt_buff + SPI_HEADER_SIZE), HCI_PACKET_TYPE_OFFSET, type);
    
    switch(type)
    {
    case HCI_TYPE_DATA:
        {
            STREAM_TO_UINT16((char *)(evnt_buff + SPI_HEADER_SIZE), HCI_DATA_LENGTH_OFFSET, data_to_recv);
            
            if (data_to_recv >= SPI_WINDOW_SIZE)
            {
                data_to_recv = eSPI_STATE_READ_FIRST_PORTION;
                cc3000_hal_readdata(evnt_buff + 10, SPI_WINDOW_SIZE);
                sSpiInformation.ulSpiState = eSPI_STATE_READ_FIRST_PORTION;
            }
            else
            {
                // We need to read the rest of data..
                if (!((HEADERS_SIZE_EVNT + data_to_recv) & 1))
                {    
                    data_to_recv++;
                }
                
                if (data_to_recv)
                {
                    cc3000_hal_readdata(evnt_buff + 10, data_to_recv);
                }
                
                sSpiInformation.ulSpiState = eSPI_STATE_READ_EOT;
            }
            break;
        }
    case HCI_TYPE_EVNT:
        {
            
            // Calculate the rest length of the data
            STREAM_TO_UINT8((char *)(evnt_buff + SPI_HEADER_SIZE), HCI_EVENT_LENGTH_OFFSET, data_to_recv);
            data_to_recv -= 1;
            
            // Add padding byte if needed
            if ((HEADERS_SIZE_EVNT + data_to_recv) & 1)
            {
                
                data_to_recv++;
            }
            
            if (data_to_recv)
            {
                cc3000_hal_readdata(evnt_buff + 10, data_to_recv);
            }
            
            sSpiInformation.ulSpiState = eSPI_STATE_READ_EOT;
            break;
        }
    }
    
    return (data_to_recv);
}

//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
////void SpiDisableInterrupts(void)
////{
////}

//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
void SpiResumeSpi(void)
{
    //TODOQK:
}

//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
void SpiTriggerRxProcessing(void)
{
    CC3000_HAL_DEASSERT_SPI_CS();
    
    sSpiInformation.ulSpiState = eSPI_STATE_IDLE;
    sSpiInformation.SPIRxHandler(sSpiInformation.pRxPacket + SPI_HEADER_SIZE);
}

//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
void SSIContReadOperation(void)
{
    //the header was read - continue with  the payload read
    if (!SpiReadDataCont())
    {        
        //all the data was read - finalize handling by switching to the task
        //and calling from task Event Handler
        SpiTriggerRxProcessing();
    }
}

//------------------------------------------------------------------------------
// CC3000 SPI_IRQ interrupt handle. When the CC3000 is ready to interact with
// host CPU it generates an interrupt signal. After that Host CPU has registered
// this interrupt request it set the corresponding /CS in active state.
//------------------------------------------------------------------------------
void cc3000_hal_gpio_interrupt_handler()
{
    if (GPIO_ReadInputDataBit(CC3000_IRQ_GPIO_PORT,CC3000_IRQ_GPIO_PIN) == Bit_RESET)
    {
        if (sSpiInformation.ulSpiState == eSPI_STATE_POWERUP)
        {
            //This means IRQ line was low call a callback of HCI Layer to  inform on event
            sSpiInformation.ulSpiState = eSPI_STATE_INITIALIZED;
        }
        else if (sSpiInformation.ulSpiState == eSPI_STATE_IDLE)
        {
            sSpiInformation.ulSpiState = eSPI_STATE_READ_IRQ;
            
            //IRQ line goes down - we are start reception 
            CC3000_HAL_ASSERT_SPI_CS();
            //Wait for TX/RX Compete which will come as DMA interrupt
            SpiReadHeader();
        }
        else if (sSpiInformation.ulSpiState == eSPI_STATE_WRITE_IRQ)
        {
            // LMS uDMA is capable to transfer data in one shot only of size up to 1024
            // so in case of bigger transfer size will need to perform 2 transfers (scatter-gather uDMA
            // mode also can be used but it will not be very useful since CPU anyway is blocked on SPI
            // write operation and can not do anything till the end
            //
            if (sSpiInformation.usTxPacketLength <= SPI_WINDOW_SIZE)
            {
                // Send the data over SPI and wait for complete interupt
                sSpiInformation.ulSpiState = eSPI_STATE_WRITE_EOT;
                cc3000_hal_spiwritedmainterrupt(sSpiInformation.pTxPacket, sSpiInformation.usTxPacketLength);
                
            }
            else
            {
                // Send the data over SPI and wait for complete interupt to transfer the rest
                sSpiInformation.ulSpiState = eSPI_STATE_WRITE_FIRST_PORTION;
                // Start the dma and change state
                cc3000_hal_writedataasync(sSpiInformation.pTxPacket, SPI_WINDOW_SIZE);
            }
        }
    }
}

//------------------------------------------------------------------------------
// CC3000 SPI interrupt handle to get command/data packet from CC3000 module
//
// probably ok ?!?
//
//------------------------------------------------------------------------------
void cc3000_hal_spi_interrupt_handler()
{
    u32 ucTxFinished, ucRxFinished;
    u16 data_to_recv;
    u8  *evnt_buff;
    
    ucTxFinished = cc3000_hal_isspidmastop(SPI_DMA_TX_TCFLAG); 
    ucRxFinished = cc3000_hal_isspidmastop(SPI_DMA_RX_TCFLAG);
    evnt_buff =  sSpiInformation.pRxPacket;
    data_to_recv = 0;

    if (sSpiInformation.ulSpiState == eSPI_STATE_READ_IRQ)
    {
        // If one of DMA's still did not finished its operation - we need to stay
        // and wait till it will finish
        if (ucTxFinished && ucRxFinished)
        {
            // If SSI Int is pending - this can be second DMA - clean it...
            cc3000_hal_clear_spidma_interrupt_flags();
            SSIContReadOperation();
        }
    }
    else if (sSpiInformation.ulSpiState == eSPI_STATE_READ_FIRST_PORTION)
    {
        if (ucRxFinished)
        {
            // If SSI Int is pending - this can be second DMA - clean it...
            cc3000_hal_clear_spidma_interrupt_flags();

            STREAM_TO_UINT16((char *)(evnt_buff + SPI_HEADER_SIZE), HCI_DATA_LENGTH_OFFSET, data_to_recv);

            // We need to read the rest of data..
            data_to_recv -=SPI_WINDOW_SIZE;

            if (!((HEADERS_SIZE_EVNT + data_to_recv) & 1))
            {    
                data_to_recv++;
            }

            cc3000_hal_readdata(sSpiInformation.pRxPacket + 10 + SPI_WINDOW_SIZE, data_to_recv);
            sSpiInformation.ulSpiState = eSPI_STATE_READ_EOT;
        }
    }
    else if (sSpiInformation.ulSpiState == eSPI_STATE_READ_EOT)
    {
        // All the data was read - finalize handling by switching to the task
        // and calling from task Event Handler
        if (ucRxFinished)
        {
            // If SSI Int is pending - this can be second DMA - clean it...
            cc3000_hal_clear_spidma_interrupt_flags();
            SpiTriggerRxProcessing();
        }
    }
    else if (sSpiInformation.ulSpiState == eSPI_STATE_WRITE_EOT)
    {
        if (ucTxFinished)
        {
            cc3000_hal_waitforspitxdone();
            cc3000_hal_clear_spidma_interrupt_flags();
            CC3000_HAL_DEASSERT_SPI_CS();

            sSpiInformation.ulSpiState = eSPI_STATE_IDLE;
        }
    }
    else if (sSpiInformation.ulSpiState == eSPI_STATE_WRITE_FIRST_PORTION)
    {
        if (ucTxFinished)
        {
            sSpiInformation.ulSpiState = eSPI_STATE_WRITE_EOT;
            cc3000_hal_writedataasync(sSpiInformation.pTxPacket + SPI_WINDOW_SIZE, sSpiInformation.usTxPacketLength - SPI_WINDOW_SIZE);
        }
    }
}

//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
long cc3000_hal_readinterruptpin()
{
    return GPIO_ReadInputDataBit(CC3000_IRQ_GPIO_PORT, CC3000_IRQ_GPIO_PIN);
}
