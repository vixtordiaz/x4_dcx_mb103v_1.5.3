/**
  **************** (C) COPYRIGHT 2011 SCT Performance, LLC *********************
  * File Name          : flash.h
  * Device / System    : SCT products using the "CommonCode" platform
  * Author             : Quyen Leba
  *
  * Version            : 1 
  * Date               : 05/02/2011
  * Description        : 
  *                    : 
  *
  *
  * History            : 05/02/2011 M. Davis
  *                    :   Add SCT Copyright header,
  *
  ******************************************************************************
  */

#ifndef __FLASH_H
#define __FLASH_H

#include <arch/gentype.h>
#include <common/settings.h>
#include <common/version.h>

typedef enum
{
    CriticalArea,
    TuneArea,
    FleetArea,
    DatalogGeneralArea,
    BootArea,
    OldSettingsArea,
}FlashSettingsArea;

u8 flash_save_setting(FlashSettingsArea flashsettingsarea,
                      u8 *settingsdata, u32 settingsdatalength);
u8 flash_load_setting(FlashSettingsArea flashsettingsarea,
                      u8 *settingsdata, u32 settingsdatalength);

//------------------------------------------------------------------------------
// Flash map
// - Failsafe Bootloader    44kB
// - Blowfish S Table       4kB
// - Settings               16kB
// - Main Bootloader        64kB
// - Application            256kB+128kB
//------------------------------------------------------------------------------

#define FLASH_START_ADDRESS                     ((u32)0x08000000)
#define FLASH_SECTOR_SIZE                       (0)

//0x08000000
#define FLASH_FAILSAFE_BOOT_ADDRESS             (FLASH_START_ADDRESS)
#define FLASH_FAILSAFE_BOOT_SIZE                (3*16*1024-4096)
#define FLASH_FAILSAFE_BOOT_FWTAG_ADDRESS       FLASH_FAILSAFE_BOOT_ADDRESS + FLASH_FAILSAFE_BOOT_SIZE - sizeof(FIRMWARE_TAG)

//0x0800B000
#define FLASH_BF_S_ADDRESS                      (FLASH_FAILSAFE_BOOT_ADDRESS + FLASH_FAILSAFE_BOOT_SIZE)
#define FLASH_BF_S_SIZE                         (4096)

//0x0800C000
#define FLASH_SETTINGS_ADDRESS                  (FLASH_BF_S_ADDRESS + FLASH_BF_S_SIZE)
#define FLASH_SETTINGS_SIZE                     (16*1024)

//0x08010000
#define FLASH_MAIN_BOOT_STARTING_SECTOR         4
#define FLASH_MAIN_BOOT_ADDRESS                 (FLASH_SETTINGS_ADDRESS + FLASH_SETTINGS_SIZE)
#define FLASH_MAIN_BOOT_SIZE                    (64*1024)
#define FLASH_MAIN_BOOT_FWTAG_ADDRESS           (FLASH_MAIN_BOOT_ADDRESS + FLASH_MAIN_BOOT_SIZE - sizeof(FIRMWARE_TAG))

//0x08020000
#define FLASH_APPLICATION_STARTING_SECTOR       5
#define FLASH_APPLICATION_SECTOR_SIZE           (128*1024)
#define FLASH_APPLICATION_ADDRESS               (FLASH_MAIN_BOOT_ADDRESS + FLASH_MAIN_BOOT_SIZE)
#define FLASH_APPLICATION_SIZE                  (3*128*1024)
#define FLASH_APPLICATION_FWTAG_ADDRESS         (FLASH_APPLICATION_ADDRESS + FLASH_APPLICATION_SIZE - sizeof(FIRMWARE_TAG))

#define FLASHSETTINGS_ADDR                      (FLASH_START_ADDRESS + 0xC000)
#define FLASHSETTINGS_MAX_SETTINGS_SIZE         2048
#define FLASHSETTINGS_BOOTSETTINGS_ADDR         (FLASHSETTINGS_ADDR + 0*FLASHSETTINGS_MAX_SETTINGS_SIZE)
#define FLASHSETTINGS_CRITICAL_ADDR             (FLASHSETTINGS_ADDR + 1*FLASHSETTINGS_MAX_SETTINGS_SIZE)
#define FLASHSETTINGS_TUNE_ADDR                 (FLASHSETTINGS_ADDR + 2*FLASHSETTINGS_MAX_SETTINGS_SIZE)
#define FLASHSETTINGS_FLEET_ADDR                (FLASHSETTINGS_ADDR + 3*FLASHSETTINGS_MAX_SETTINGS_SIZE)
#define FLASHSETTINGS_DATALOGGENERAL_ADDR       (FLASHSETTINGS_ADDR + 4*FLASHSETTINGS_MAX_SETTINGS_SIZE)

#define FLASHSETTINGS_CRITICAL_FILENAME         "crsb.scf"
#define FLASHSETTINGS_TUNE_FILENAME             "tusb.scf"
#define FLASHSETTINGS_FLEET_FILENAME            "flsb.scf"
#define FLASHSETTINGS_DATALOGGENERAL_FILENAME   "dgsb.scf"

u8 flash_erase_sector(u16 sector);
u8 flash_write_sector(u16 sector, u8 *data, u16 datalength, bool encrypted);
u8 flash_validate_content(u16 sector, u32 length, u32 crc32e);
u8 flash_load_bootsettings(u8 *settings, u32 length);
u8 flash_store_bootsettings(u8 *settings, u16 length);
u8 flash_enable_protections(void);

#endif	//__FLASH_H
