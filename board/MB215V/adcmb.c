/**
  **************** (C) COPYRIGHT 2011 SCT Performance, LLC *********************
  * File Name          : adcmb.c
  * Device / System    : SCT products using the "CommonCode" platform
  * Author             : Quyen Leba
  *
  * Version            : 1 
  * Date               : 05/02/2011
  * Description        : 
  *                    : 
  *
  *
  * History            : 05/02/2011 M. Davis
  *                    :   Add SCT Copyright header,
  *
  ******************************************************************************
  */

#include <device_config.h>
#include <arch/gentype.h>
#include <board/adc.h>
#include <board/datalogfeatures.h>
#include <common/statuscode.h>
#include "adcmb.h"

#define ADC_AIN7_INDEX          0
#define ADC_AIN8_INDEX          1

struct
{
    //channels of this MainBoard
    u16 adc_values[2];
}adc_info;

//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
void adcmb_init()
{
    //TODOQK: later on this
}

//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
u8 adcmb_read(ADC_CHANNEL channel, float *DataValueFloat)
{
    switch(channel)
    {
    case ADC_AIN7:
        *DataValueFloat = datalogfeatures_calc_ain_7(adc_info.adc_values[ADC_AIN7_INDEX]);
        break;
    case ADC_AIN8:
        *DataValueFloat = datalogfeatures_calc_ain_8(adc_info.adc_values[ADC_AIN8_INDEX]);
        break;
    default:
        return S_INPUT;
    }

    return S_SUCCESS;
}
