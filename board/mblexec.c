/**
  **************** (C) COPYRIGHT 2011 SCT Performance, LLC *********************
  * File Name          : mblexec.c
  * Device / System    : SCT products using the "CommonCode" platform
  * Author             : Quyen Leba
  *
  * Version            : 1 
  * Date               : 05/02/2011
  * Description        : 
  *                    : 
  *
  *
  * History            : 05/02/2011 M. Davis
  *                    :   Add SCT Copyright header,
  *
  ******************************************************************************
  */

#include <string.h>
#include <board/genplatform.h>
#include <common/crypto_blowfish.h>
#include <common/tea.h>
#include <common/statuscode.h>
#include "mblif.h"
#include "mblif_dma.h"
#include "mblif_intr.h"
#include "mblexec_bootloader.h"
#include "mblexec.h"
#include <board/delays.h>

#define SECURITY_SEEDKEY_SIZE       32

extern funcptr_mblif_link* mblif_link;              //from mblif.c
u8  seedkey[SECURITY_SEEDKEY_SIZE];

//------------------------------------------------------------------------------
// Private prototypes
//------------------------------------------------------------------------------
u8 mblexec_get_settings(u8 *opcodedata, u16 opcodedatalength,
                        u8 *returndata, u16 *returndatalength);
u8 mblexec_set_settings(u8 *opcodedata, u16 opcodedatalength);
u8 mblexec_erase_sector_to_vehicleboard(u8 *opcodedata, u16 opcodedatalength);
u8 mblexec_write_sector_to_vehicleboard(u8 *opcodedata, u16 opcodedatalength);
u8 mblexec_validate_flash_content(u8 *opcode_data, u16 opcode_datalength);
u8 mblexec_request_security_seed();
u8 mblexec_validate_security_key();

u8 mblexec_adc_read(ADC_CHANNEL ch, float *value);
u8 mblexec_set_vehicle_comm(VehicleCommType comm_type,
                            VehicleCommLevel comm_level,
                            VehicleCommConfig comm_flags,
                            u8 *privdata, u8 privdatalength);
u8 mblexec_send_simple_msg(u8 *data, u16 datalength);
u8 mblexec_receive_simple_msg(u8 *data, u16 datalength,
                              u8 *returndata, u16 *returndatalength);
u8 mblexec_send_receive_simple_msg(u8 *data, u16 datalength,
                              u8 *returndata, u16 *returndatalength);
u8 mblexec_send_complex_msg(u8 *data, u16 datalength);
u8 mblexec_receive_complex_msg(u8 *data, u16 datalength,
                               u8 *returndata, u16 *returndatalength);
u8 mblexec_readblock_helper(u8 *data, u16 datalength,
                            u8 *returndata, u16 *returndatalength);
u8 mblexec_writeblock_helper(u8 *data, u16 datalength,
                             u8 *returndata, u16 *returndatalength);
u8 mblexec_get_hwconfig(MBLEXEC_HWCONFIG *hwconfig);
u8 mblexec_set_hwconfig(MBLEXEC_HWCONFIG *hwconfig);
u8 mblexec_set_mblif_mode(MBLIF_MODE mode);
u8 mblexec_get_mblif_mode(MBLIF_MODE *mode);
u8 mblexec_obd2_voltage_monitor();

//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
void mblexec_init()
{
}

//------------------------------------------------------------------------------
// Sensitive EXEC commands required prior security unlock
// Return:  u8  status
// Engineer: Quyen Leba
//------------------------------------------------------------------------------
u8 mblexec_unlock_security()
{
    u8  status;

    status = mblexec_call(MBLEXEC_OPCODE_REQUEST_SECURITY_SEED,
                          NULL,NULL,NULL,NULL);
    if (status != S_SUCCESS)
    {
        return status;
    }
    status = mblexec_call(MBLEXEC_OPCODE_VALIDATE_SECURITY_KEY,
                          NULL,NULL,NULL,NULL);
    return status;
}

//------------------------------------------------------------------------------
// Lock security to protect sensitive EXEC commands
// Return:  u8  status
// Engineer: Quyen Leba
//------------------------------------------------------------------------------
u8 mblexec_lock_security()
{
    u8  status;

    status = mblexec_call(MBLEXEC_OPCODE_REQUEST_SECURITY_SEED,
                          NULL,NULL,NULL,NULL);
    return status;
}

//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
u8 mblexec_call(MBLEXEC_OPCODE opcode, u8 *opcodedata, u16 opcodedatalength,
                u8 *returndata, u16 *returndatalength)
{
    u8  status;

    switch(opcode)
    {
    //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    //
    //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    case MBLEXEC_OPCODE_SEND_SIMPLE_MSG:
        status = mblexec_send_simple_msg(opcodedata,opcodedatalength);
        break;
    case MBLEXEC_OPCODE_RECEIVE_SIMPLE_MSG:
        status = mblexec_receive_simple_msg(opcodedata,opcodedatalength,
                                            returndata, returndatalength);
        break;
    case MBLEXEC_OPCODE_SEND_RECEIVE_SIMPLE_MSG:
        status = mblexec_send_receive_simple_msg(opcodedata,opcodedatalength,
                                                 returndata, returndatalength);
        break;
    case MBLEXEC_OPCODE_SEND_COMPLEX_MSG:
        status = mblexec_send_complex_msg(opcodedata,opcodedatalength);
        break;
    case MBLEXEC_OPCODE_RECEIVE_COMPLEX_MSG:
        status = mblexec_receive_complex_msg(opcodedata,opcodedatalength,
                                             returndata, returndatalength);
        break;
    //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    // Special/critical functions
    //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    case MBLEXEC_OPCODE_READBLOCK_HELPER:
        status = mblexec_readblock_helper(opcodedata,opcodedatalength,
                                          returndata, returndatalength);
        break;
    case MBLEXEC_OPCODE_WRITEBLOCK_HELPER:
        status = mblexec_writeblock_helper(opcodedata,opcodedatalength,
                                           returndata, returndatalength);
        break;

    //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    // Settings
    //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    case MBLEXEC_OPCODE_GET_SETTINGS:
        status = mblexec_get_settings(opcodedata,opcodedatalength,
                                      returndata,returndatalength);
        break;
    case MBLEXEC_OPCODE_SET_SETTINGS:
        status = mblexec_set_settings(opcodedata,opcodedatalength);
        break;
    case MBLEXEC_OPCODE_SET_MBLIF_MODE:
        status = mblexec_set_mblif_mode((MBLIF_MODE)*opcodedata);
        break;
    case MBLEXEC_OPCODE_GET_MBLIF_MODE:
        status = mblexec_get_mblif_mode((MBLIF_MODE*)returndata);
        break;
        //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    // Bootloader
    //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//    case MBLEXEC_OPCODE_ERASE_SECTOR:
//        status = mblexec_erase_sector_to_vehicleboard(opcodedata,
//                                                      opcodedatalength);
//        break;
//    case MBLEXEC_OPCODE_WRITE_SECTOR:
//        status = mblexec_write_sector_to_vehicleboard(opcodedata,
//                                                      opcodedatalength);
//        break;
//    case MBLEXEC_OPCODE_VALIDATE_FLASH_CONTENT:
//        status = mblexec_validate_flash_content(opcodedata,opcodedatalength);
//        break;
    case MBLEXEC_OPCODE_BOOTLOADER_SETBOOT:
        status = mblexec_bootloader_setboot((BootloaderMode)(*(u16*)opcodedata));
        break;
    case MBLEXEC_OPCODE_BOOTLOADER_INIT:
        if (opcodedatalength == sizeof(FirmwareHeader))
        {
            status = mblexec_bootloader_init((FirmwareHeader*)opcodedata);
        }
        break;
    case MBLEXEC_OPCODE_BOOTLOADER_DO:
        status = mblexec_bootloader_do(opcodedata,opcodedatalength);
        break;
    case MBLEXEC_OPCODE_BOOTLOADER_VALIDATE:
        if (opcodedatalength == 4)
        {
            status = mblexec_bootloader_validate(*(u32*)opcodedata);
        }
        break;
    //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    // Security
    //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    case MBLEXEC_OPCODE_REQUEST_SECURITY_SEED:
        status = mblexec_request_security_seed();
        break;
    case MBLEXEC_OPCODE_VALIDATE_SECURITY_KEY:
        status = mblexec_validate_security_key();
        break;
    //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    //
    //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    case MBLEXEC_OPCODE_GET_AIN:
        //opcodedata[0]: the requested AIN channel
        status = mblexec_adc_read((ADC_CHANNEL)*opcodedata,(float*)returndata);
        *returndatalength = 4;
        break;
    case MBLEXEC_OPCODE_SET_VEHICLE_COMM:
        if (opcodedatalength >= 4)
        {
            VehicleCommConfig comm_flags;
            
            comm_flags.raw.value = *((u16*)(&opcodedata[2]));
            status = mblexec_set_vehicle_comm((VehicleCommType)opcodedata[0],
                                              (VehicleCommLevel)opcodedata[1],
                                              comm_flags,
                                              &opcodedata[4], opcodedatalength-4);
        }
        else
        {
            status = S_BADCONTENT;
        }
        break;
    case MBLEXEC_OPCODE_GET_HWCONFIG:
        status = mblexec_get_hwconfig((MBLEXEC_HWCONFIG*)returndata);
        *returndatalength = sizeof(MBLEXEC_HWCONFIG);
        break;
    case MBLEXEC_OPCODE_SET_HWCONFIG:
        if (opcodedatalength == sizeof(MBLEXEC_HWCONFIG))
        {
            status = mblexec_set_hwconfig((MBLEXEC_HWCONFIG*)opcodedata);
        }
        else
        {
            status = S_BADCONTENT;
        }
        break;
    case MBLEXEC_OPCODE_OBD2_VOLTAGE_MONITOR:
        status = mblexec_obd2_voltage_monitor();
        break;
    //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    //
    //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    default:
        status = S_NOTSUPPORT;
    }
    
    return status;
}

//------------------------------------------------------------------------------
// A helper of mblexec to issue an AIN request
// Input:   ADC_CHANNEL ch
// Output:  float *value
// Return:  u8  status
// Engineer: Quyen Leba
//------------------------------------------------------------------------------
u8 mblexec_adc_read(ADC_CHANNEL ch, float *value)
{
    MBLIF_CONTROL *pcontrol;
    u8  buffer[256];
    u16 length;
    u8  status;

    length = 1;
    status = mblif_link(MBLIF_CMD_EXEC,
                        MBLEXEC_OPCODE_GET_AIN,(u8*)&ch,&length);
    if (status != S_SUCCESS)
    {
        return status;
    }

    app_timeout_set(100);
    while(1)
    {
        status = mblif_link(MBLIF_CMD_GET_STATUS,0,buffer,&length);
        if (status == S_SUCCESS && length == 4)
        {
            pcontrol = (MBLIF_CONTROL*)&buffer;
            if (pcontrol->exec_done)
            {
                if (pcontrol->exec_ok)
                {
                    mblif_link(MBLIF_CMD_READ_BUFFER,0,buffer,&length);
                    if (length == 4)
                    {
                        *value = *((float*)(buffer));
                    }
                    else
                    {
                        *value = 0;
                        status = S_FAIL;
                    }
                }
                else
                {
                    *value = 0;
                    status = S_FAIL;
                }
                break;
            }
        }//if (status ...
        else if (app_timeout())
        {
            status = S_TIMEOUT;
            break;
        }
    }//while(1)...

    return status;
}

//------------------------------------------------------------------------------
// A helper of mblexec to set vehicle comm
// Inputs:  VehicleCommType comm_type
//          VehicleCommLevel comm_level
//          VehicleCommConfig comm_flags
// Return:  u8  status
// Engineer: Quyen Leba
//------------------------------------------------------------------------------
u8 mblexec_set_vehicle_comm(VehicleCommType comm_type,
                            VehicleCommLevel comm_level,
                            VehicleCommConfig comm_flags,
                            u8 *privdata, u8 privdatalength)
{
    MBLIF_CONTROL *pcontrol;
    u8  buffer[16];
    u16 bufferlength;
    u8  status;

    status = S_SUCCESS;
    switch(comm_type)
    {
    case CommType_SCP:
    case CommType_SCP32:
    case CommType_VPW:
    case CommType_CAN:
    case CommType_SCIA:
    case CommType_SCIB:
    case CommType_KLINE:                
    case CommType_KWP2000:                   
        break;
    default:
        return S_NOTSUPPORT;
    }

    bufferlength = 4+privdatalength;
    if (bufferlength > sizeof(buffer))
    {
        return S_INPUT;
    }

    buffer[0] = (u8)comm_type;
    buffer[1] = (u8)comm_level;
    buffer[2] = comm_flags.raw.value & 0xFF;
    buffer[3] = comm_flags.raw.value >> 8;
    memcpy((char*)&buffer[4],(char*)privdata,privdatalength);
    status = mblif_link(MBLIF_CMD_EXEC,
                        MBLEXEC_OPCODE_SET_VEHICLE_COMM,buffer,&bufferlength);
    if (status != S_SUCCESS)
    {
        return status;
    }

    app_timeout_set(200);
    while(1)
    {
        status = mblif_link(MBLIF_CMD_GET_STATUS,0,buffer,&bufferlength);
        if (status == S_SUCCESS && bufferlength == 4)
        {
            pcontrol = (MBLIF_CONTROL*)&buffer;
            if (pcontrol->exec_done)
            {
                if (!pcontrol->exec_ok)
                {
                    status = pcontrol->status;
                }
                break;
            }
        }//if (status ...
        else if (app_timeout())
        {
            status = S_TIMEOUT;
            break;
        }
    }//while(1)...

    return status;
}

//------------------------------------------------------------------------------
// A helper of mblexec to send a simple message
// Inputs:  u8  *data ([2:flags][n-2:msgdata])
//          u16 datalength
// Return:  u8  status
// Engineer: Quyen Leba
//------------------------------------------------------------------------------
u8 mblexec_send_simple_msg(u8 *data, u16 datalength)
{
    MBLIF_CONTROL *pcontrol;
    u8  buffer[256];
    u16 bufferlength;
    u8  status;
    
    status = mblif_link(MBLIF_CMD_EXEC, MBLEXEC_OPCODE_SEND_SIMPLE_MSG,
                        (u8*)data,&datalength);
    if (status != S_SUCCESS)
    {
        return status;
    }

    app_timeout_set(10000);
    while(1)
    {
        status = mblif_link(MBLIF_CMD_GET_STATUS,0,buffer,&bufferlength);
        if (status == S_SUCCESS && bufferlength == 4)
        {
            pcontrol = (MBLIF_CONTROL*)&buffer;
            if (pcontrol->exec_done)
            {
                if (!pcontrol->exec_ok)
                {
                    status = pcontrol->status;
                }
                break;
            }
        }//if (status ...
        else if (app_timeout())
        {
            status = S_TIMEOUT;
            break;
        }
    }
    
    return status;
}

//------------------------------------------------------------------------------
// A helper of mblexec to receive a simple message
// Inputs:  u8  *data ([2:flags][2:timeout])
//          u16 datalength
// Return:  u8  status
// Engineer: Quyen Leba
//------------------------------------------------------------------------------
u8 mblexec_receive_simple_msg(u8 *data, u16 datalength,
                              u8 *returndata, u16 *returndatalength)
{
    MBLIF_CONTROL *pcontrol;
    u8  buffer[256];
    u16 bufferlength;
    u8  status;
    
    status = mblif_link(MBLIF_CMD_EXEC, MBLEXEC_OPCODE_RECEIVE_SIMPLE_MSG,
                        (u8*)data,&datalength);
    if (status != S_SUCCESS)
    {
        return status;
    }

    app_timeout_set(10000);
    while(1)
    {
        status = mblif_link(MBLIF_CMD_GET_STATUS,0,buffer,&bufferlength);
        if (status == S_SUCCESS && bufferlength == 4)
        {
            pcontrol = (MBLIF_CONTROL*)&buffer;
            if (pcontrol->exec_done)
            {
                if(pcontrol->exec_wait)
                {
                    if (app_timeout())
                    {
                        status = S_TIMEOUT;
                        break;
                    }
                    delays(5,'m');
                    continue; // wait untill app_timeout
                }
                else if (pcontrol->exec_ok)
                {
                    status = mblif_link(MBLIF_CMD_READ_BUFFER,0,
                                        returndata,returndatalength);
                    if(status != S_SUCCESS)
                    {
                        status = S_FAIL;
                    }
                }
                else
                {
                    status = pcontrol->status;
                }
                break;
            }
        }//if (status ...
        else if (app_timeout())
        {
            status = S_TIMEOUT;
            break;
        }
    }

    return status;
}

//------------------------------------------------------------------------------
// A helper of mblexec to send and receive a simple message
// Inputs:  u8  *data ([2:send_flags][2:reserved]
//                     [2:receive_flags][2:reserved][n:send_msg])
//          u16 datalength
// Return:  u8  status
// Engineer: Quyen Leba
//------------------------------------------------------------------------------
u8 mblexec_send_receive_simple_msg(u8 *data, u16 datalength,
                              u8 *returndata, u16 *returndatalength)
{
    MBLIF_CONTROL *pcontrol;
    u8  buffer[256];
    u16 bufferlength;
    u8  status;
    
    status = mblif_link(MBLIF_CMD_EXEC, MBLEXEC_OPCODE_SEND_RECEIVE_SIMPLE_MSG,
                        (u8*)data,&datalength);
    if (status != S_SUCCESS)
    {
        return status;
    }

    app_timeout_set(5000);
    while(1)
    {
        status = mblif_link(MBLIF_CMD_GET_STATUS,0,buffer,&bufferlength);
        if (status == S_SUCCESS && bufferlength == 4)
        {
            pcontrol = (MBLIF_CONTROL*)&buffer;
            if (pcontrol->exec_done)
            {
                if (pcontrol->exec_ok)
                {
                    status = mblif_link(MBLIF_CMD_READ_BUFFER,0,
                                        returndata,returndatalength);
                }
                else
                {
                    status = pcontrol->status;
                }
                break;
            }
        }//if (status ...
        else if (app_timeout())
        {
            status = S_TIMEOUT;
            break;
        }
    }

    return status;
}

//------------------------------------------------------------------------------
// A helper of mblexec to send a complex message
// Inputs:  u8  *data ([2:flags])
//          u16 datalength
// Return:  u8  status
// Engineer: Quyen Leba
//------------------------------------------------------------------------------
u8 mblexec_send_complex_msg(u8 *data, u16 datalength)
{
    MBLIF_CONTROL *pcontrol;
    u8  buffer[256];
    u16 bufferlength;
    u8  status;
    
    status = mblif_link(MBLIF_CMD_EXEC, MBLEXEC_OPCODE_SEND_COMPLEX_MSG,
                        (u8*)data,&datalength);
    if (status != S_SUCCESS)
    {
        return status;
    }

    app_timeout_set(10000);
    while(1)
    {
        status = mblif_link(MBLIF_CMD_GET_STATUS,0,buffer,&bufferlength);
        if (status == S_SUCCESS && bufferlength == 4)
        {
            pcontrol = (MBLIF_CONTROL*)&buffer;
            if (pcontrol->exec_done)
            {
                if (pcontrol->exec_ok)
                {
                    status = S_SUCCESS;
                }
                else
                {
                    status = pcontrol->status;
                }
                break;
            }
        }//if (status ...
        else if (app_timeout())
        {
            status = S_TIMEOUT;
            break;
        }
    }

    return status;
}

//------------------------------------------------------------------------------
// A helper of mblexec to receive a simple message
// Inputs:  u8  *data ([2:flags])
//          u16 datalength
// Return:  u8  status
// Engineer: Quyen Leba
//------------------------------------------------------------------------------
u8 mblexec_receive_complex_msg(u8 *data, u16 datalength,
                               u8 *returndata, u16 *returndatalength)
{
    MBLIF_CONTROL *pcontrol;
    u8  buffer[256];
    u16 bufferlength;
    u8  status;
    
    status = mblif_link(MBLIF_CMD_EXEC, MBLEXEC_OPCODE_RECEIVE_COMPLEX_MSG,
                        (u8*)data,&datalength);
    if (status != S_SUCCESS)
    {
        return status;
    }

    app_timeout_set(10000);
    while(1)
    {
        status = mblif_link(MBLIF_CMD_GET_STATUS,0,buffer,&bufferlength);
        if (status == S_SUCCESS && bufferlength == 4)
        {
            pcontrol = (MBLIF_CONTROL*)&buffer;
            if (pcontrol->exec_done)
            {
                if (pcontrol->exec_ok)
                {
                    status = mblif_link(MBLIF_CMD_READ_BUFFER,0,
                                        returndata,returndatalength);
                }
                else
                {
                    status = pcontrol->status;
                }
                break;
            }
        }//if (status ...
        else if (app_timeout())
        {
            status = S_TIMEOUT;
            break;
        }
    }

    return status;
}

//------------------------------------------------------------------------------
// A helper of mblexec to read block from PCM/TCM
// Inputs:  u8  *data (see readblock_info)
//          u16 datalength
// Return:  u8  status
// Engineer: Quyen Leba
//------------------------------------------------------------------------------
u8 mblexec_readblock_helper(u8 *data, u16 datalength,
                            u8 *returndata, u16 *returndatalength)
{
    MBLIF_CONTROL *pcontrol;
    u8  buffer[256];
    u16 bufferlength;
    u8  status;

    status = mblif_link(MBLIF_CMD_EXEC, MBLEXEC_OPCODE_READBLOCK_HELPER,
                        (u8*)data,&datalength);
    if (status != S_SUCCESS)
    {
        return status;
    }

    app_timeout_set(10000);
    while(1)
    {
        status = mblif_link(MBLIF_CMD_GET_STATUS,0,buffer,&bufferlength);
        if (status == S_SUCCESS && bufferlength == 4)
        {
            pcontrol = (MBLIF_CONTROL*)&buffer;
            if (pcontrol->exec_done)
            {
                if (pcontrol->exec_ok)
                {
                    status = mblif_link(MBLIF_CMD_READ_BUFFER,0,
                                        returndata,returndatalength);
                }
                else
                {
                    status = pcontrol->status;
                }
                break;
            }
        }//if (status ...
        else if (app_timeout())
        {
            status = S_TIMEOUT;
            break;
        }
    }

    return status;
}

//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
u8 mblexec_writeblock_helper(u8 *setupdata, u16 setupdatalength,
                            u8 *writedata, u16 *writedatalength)
{
    MBLIF_CONTROL *pcontrol;
    u8  buffer[256];
    u16 bufferlength;
    u8  status;

    status = mblif_link(MBLIF_CMD_WRITE_BUFFER,0,writedata,writedatalength);
    if (status != S_SUCCESS)
    {
        return status;
    }

    status = mblif_link(MBLIF_CMD_EXEC, MBLEXEC_OPCODE_WRITEBLOCK_HELPER,
                        (u8*)setupdata,&setupdatalength);
    if (status != S_SUCCESS)
    {
        return status;
    }

    app_timeout_set(10000);
    while(1)
    {
        status = mblif_link(MBLIF_CMD_GET_STATUS,0,buffer,&bufferlength);
        if (status == S_SUCCESS && bufferlength == 4)
        {
            pcontrol = (MBLIF_CONTROL*)&buffer;
            if (pcontrol->exec_done)
            {
                if (pcontrol->exec_ok)
                {
                    status = S_SUCCESS;
                }
                else
                {
                    status = pcontrol->status;
                }
                break;
            }
        }//if (status ...
        else if (app_timeout())
        {
            status = S_TIMEOUT;
            break;
        }
    }

    return status;
}

//------------------------------------------------------------------------------
// A helper of mblexec to get hardware config
// Output:  MBLEXEC_HWCONFIG *hwconfig
// Return:  u8  status
// Engineer: Quyen Leba
//------------------------------------------------------------------------------
u8 mblexec_get_hwconfig(MBLEXEC_HWCONFIG *hwconfig)
{
    MBLIF_CONTROL *pcontrol;
    u8  buffer[256];
    u16 bufferlength;
    u8  status;
    
    bufferlength = sizeof(MBLEXEC_HWCONFIG);
    status = mblif_link(MBLIF_CMD_EXEC, MBLEXEC_OPCODE_GET_HWCONFIG,
                        (u8*)buffer,&bufferlength);
    if (status != S_SUCCESS)
    {
        return status;
    }

    app_timeout_set(100);
    while(1)
    {
        status = mblif_link(MBLIF_CMD_GET_STATUS,0,buffer,&bufferlength);
        if (status == S_SUCCESS && bufferlength == 4)
        {
            pcontrol = (MBLIF_CONTROL*)&buffer;
            if (pcontrol->exec_done)
            {
                if (pcontrol->exec_ok)
                {
                    status = mblif_link(MBLIF_CMD_READ_BUFFER,0,
                                        buffer,&bufferlength);
                    if (status == S_SUCCESS)
                    {
                        if (bufferlength == sizeof(MBLEXEC_HWCONFIG))
                        {
                            memcpy((char*)hwconfig,
                                   (char*)buffer,sizeof(MBLEXEC_HWCONFIG));
                        }
                        else
                        {
                            status = S_BADCONTENT;
                        }
                    }
                }
                else
                {
                    status = pcontrol->status;
                }
                break;
            }
        }//if (status ...
        else if (app_timeout())
        {
            status = S_TIMEOUT;
            break;
        }
    }
    return status;
}

//------------------------------------------------------------------------------
// A helper of mblexec to set hardware config
// Output:  MBLEXEC_HWCONFIG *hwconfig
// Return:  u8  status
// Engineer: Quyen Leba
//------------------------------------------------------------------------------
u8 mblexec_set_hwconfig(MBLEXEC_HWCONFIG *hwconfig)
{
    MBLIF_CONTROL *pcontrol;
    u8  buffer[256];
    u16 bufferlength;
    u8  status;
    
    bufferlength = sizeof(MBLEXEC_HWCONFIG);
    status = mblif_link(MBLIF_CMD_EXEC, MBLEXEC_OPCODE_SET_HWCONFIG,
                        (u8*)hwconfig,&bufferlength);
    if (status != S_SUCCESS)
    {
        return status;
    }

    app_timeout_set(300);
    while(1)
    {
        status = mblif_link(MBLIF_CMD_GET_STATUS,0,buffer,&bufferlength);
        if (status == S_SUCCESS && bufferlength == 4)
        {
            pcontrol = (MBLIF_CONTROL*)&buffer;
            if (pcontrol->exec_done)
            {
                if (!pcontrol->exec_ok)
                {
                    status = pcontrol->status;
                }
                break;
            }
        }//if (status ...
        else if (app_timeout())
        {
            status = S_TIMEOUT;
            break;
        }
    }

    return status;
}

//------------------------------------------------------------------------------
// Get settings from vehicle board
// Inputs:  u8  *opcodedata [2:setting opcode]
//          u16 opcodedatalength
// Outputs: u8  *returndata
//          u16 *returndatalength
// Return:  u8  status
// Engineer: Quyen Leba
//------------------------------------------------------------------------------
u8 mblexec_get_settings(u8 *opcodedata, u16 opcodedatalength,
                        u8 *returndata, u16 *returndatalength)
{
    MBLIF_CONTROL *pcontrol;
    u8  buffer[256];
    u16 length;
    u8  status;

    status = mblif_link(MBLIF_CMD_EXEC,
                        MBLEXEC_OPCODE_GET_SETTINGS,
                        opcodedata,&opcodedatalength);
    if (status != S_SUCCESS)
    {
        return status;
    }
    
    app_timeout_set(200);
    while(1)
    {
        status = mblif_link(MBLIF_CMD_GET_STATUS,0,buffer,&length);
        if (status == S_SUCCESS && length == 4)
        {
            pcontrol = (MBLIF_CONTROL*)&buffer;
            if (pcontrol->exec_done)
            {
                if (pcontrol->exec_ok)
                {
                    mblif_link(MBLIF_CMD_READ_BUFFER,0,
                               returndata,returndatalength);
                    if (*returndatalength == 0)
                    {
                        status = S_FAIL;
                    }
                }
                else
                {
                    status = pcontrol->status;
                }
                break;
            }
        }//if (status ...
        else if (app_timeout())
        {
            status = S_TIMEOUT;
            break;
        }
    }//while(1)...

    return status;
}

//------------------------------------------------------------------------------
// Get the mblif mode
// Inputs:  u16 mode
//          
// Return:  u8  status
// Engineer: Patrick Downs
//------------------------------------------------------------------------------
u8 mblexec_get_mblif_mode(MBLIF_MODE *mode)
{
    MBLIF_CONTROL *pcontrol;
    u8  buffer[256];
    u16 length;
    u8  status;
    
    length = 0;
    status = mblif_link(MBLIF_CMD_EXEC, MBLEXEC_OPCODE_GET_MBLIF_MODE,
                        NULL,&length);
    if (status != S_SUCCESS)
    {
        return status;
    }
    
    app_timeout_set(200);
    while(1)
    {
        status =    mblif_link(MBLIF_CMD_GET_STATUS,0,buffer,&length);
        if (status == S_SUCCESS && length == 4)
        {
            pcontrol = (MBLIF_CONTROL*)&buffer;
            if (pcontrol->exec_done)
            {
                if (pcontrol->exec_ok)
                {
                    status = mblif_link(MBLIF_CMD_READ_BUFFER, 0, (u8*)buffer, 
                                        &length);
                    if(status == S_SUCCESS && length != 0)
                    {
                        *mode = (MBLIF_MODE)buffer[0];
                    }
                    else
                    {
                        status = S_FAIL;
                    }
                }
                else
                {
                    status = pcontrol->status;
                }
                break;
            }
        }//if (status ...
        else if (app_timeout())
        {
            status = S_TIMEOUT;
            break;
        }
    }//while(1)...
    
    return status;
}

//------------------------------------------------------------------------------
// Set the mblif mode
// Inputs:  u16 mode
//          
// Return:  u8  status
// Engineer: Patrick Downs
//------------------------------------------------------------------------------
u8 mblexec_set_mblif_mode(MBLIF_MODE mode)
{
    MBLIF_CONTROL *pcontrol;
    u8  buffer[256];
    u16 length;
    u8  status;

    switch(mode)
    {
    case MBLIF_MODE_INTERRUPT:
#if (MBLIF_MODE_TYPE == MBLIF_MODE_DMA_WITH_CRC)
    case MBLIF_MODE_DMA_WITH_CRC:
#endif
#if (MBLIF_MODE_TYPE == MBLIF_MODE_DMA_NO_CRC)       
    case MBLIF_MODE_DMA_NO_CRC:
#endif
        break;
    default:
        return S_NOTSUPPORT;
    }

    length = sizeof(MBLIF_MODE);
    status = mblif_link(MBLIF_CMD_EXEC,
                        MBLEXEC_OPCODE_SET_MBLIF_MODE,
                        (u8*)&mode,(u16*)&length);
    if (status != S_SUCCESS)
    {
        return status;
    }
    
    if (status == S_SUCCESS)
    {
        switch(mode)
        {
        case MBLIF_MODE_INTERRUPT:
            mblif_link = &mblif_intr_link;
            mblif_intr_init();
            break;
        case MBLIF_MODE_DMA_WITH_CRC:
            mblif_link = &mblif_dma_link;
            mblif_dma_init(MBLIF_MODE_DMA_WITH_CRC);
            break;
        case MBLIF_MODE_DMA_NO_CRC:
            mblif_link = &mblif_dma_link;
            mblif_dma_init(MBLIF_MODE_DMA_NO_CRC);
            break;
        default:
            return S_NOTSUPPORT;
        }
    }
    
    app_timeout_set(200);
    while(1)
    {
        status = mblif_link(MBLIF_CMD_GET_STATUS,0,buffer,&length);
        if (status == S_SUCCESS && length == 4)
        {
            pcontrol = (MBLIF_CONTROL*)&buffer;
            if (pcontrol->exec_done)
            {
                if (pcontrol->exec_ok)
                {
                    status = S_SUCCESS;
                }
                else
                {
                    status = pcontrol->status;
                }
                break;
            }
        }//if (status ...
        else if (app_timeout())
        {
            status = S_TIMEOUT;
            break;
        }
    }//while(1)...

    return status;
}

//------------------------------------------------------------------------------
// Set settings to vehicle board
// Inputs:  u8  *opcodedata [2:setting opcode][n:settings data]
//          u16 opcodedatalength
// Return:  u8  status
// Engineer: Quyen Leba
//------------------------------------------------------------------------------
u8 mblexec_set_settings(u8 *opcodedata, u16 opcodedatalength)
{
    MBLIF_CONTROL *pcontrol;
    u8  buffer[256];
    u16 length;
    u8  status;

    status = mblif_link(MBLIF_CMD_EXEC,
                        MBLEXEC_OPCODE_SET_SETTINGS,
                        opcodedata,&opcodedatalength);
    if (status != S_SUCCESS)
    {
        return status;
    }
    
    app_timeout_set(200);
    while(1)
    {
        status = mblif_link(MBLIF_CMD_GET_STATUS,0,buffer,&length);
        if (status == S_SUCCESS && length == 4)
        {
            pcontrol = (MBLIF_CONTROL*)&buffer;
            if (pcontrol->exec_done)
            {
                if (pcontrol->exec_ok)
                {
                    status = S_SUCCESS;
                }
                else
                {
                    status = pcontrol->status;
                }
                break;
            }
        }//if (status ...
        else if (app_timeout())
        {
            status = S_TIMEOUT;
            break;
        }
    }//while(1)...

    return status;
}

//------------------------------------------------------------------------------
// Erase a sector of VehicleBoard flash
// Inputs:  u8  *opcodedata [2:sector index]
//          u16 opcodedatalength
// Return:  u8  status
// Engineer: Quyen Leba
//------------------------------------------------------------------------------
//u8 mblexec_erase_sector_to_vehicleboard(u8 *opcodedata, u16 opcodedatalength)
//{
//    MBLIF_CONTROL *pcontrol;
//    u8  buffer[256];
//    u16 length;
//    u8  status;
//
//    status = mblif_link(MBLIF_CMD_EXEC,
//                        MBLEXEC_OPCODE_ERASE_SECTOR,
//                        opcodedata,&opcodedatalength);
//    if (status != S_SUCCESS)
//    {
//        return status;
//    }
//
//    app_timeout_set(300);
//    while(1)
//    {
//        status = mblif_link(MBLIF_CMD_GET_STATUS,0,buffer,&length);
//        if (status == S_SUCCESS && length == 4)
//        {
//            pcontrol = (MBLIF_CONTROL*)&buffer;
//            if (pcontrol->exec_done)
//            {
//                if (pcontrol->exec_ok)
//                {
//                    status = S_SUCCESS;
//                }
//                else
//                {
//                    status = pcontrol->status;
//                }
//                break;
//            }
//        }//if (status ...
//        else if (app_timeout())
//        {
//            status = S_TIMEOUT;
//            break;
//        }
//    }//while(1)...
//
//    return status;
//}

//------------------------------------------------------------------------------
// Write a sector of VehicleBoard flash
// Inputs:  u8  *opcodedata [2:sector index]
//          u16 opcodedatalength
// Return:  u8  status
// Note: must do a MBLIF_CMD_WRITE_BUFFER first before exec this task
// Engineer: Quyen Leba
//------------------------------------------------------------------------------
//u8 mblexec_write_sector_to_vehicleboard(u8 *opcodedata, u16 opcodedatalength)
//{
//    MBLIF_CONTROL *pcontrol;
//    u8  buffer[256];
//    u16 length;
//    u8  status;
//
//    status = mblif_link(MBLIF_CMD_EXEC,
//                        MBLEXEC_OPCODE_WRITE_SECTOR,
//                        opcodedata,&opcodedatalength);
//    if (status != S_SUCCESS)
//    {
//        return status;
//    }
//
//    app_timeout_set(600);
//    while(1)
//    {
//        status = mblif_link(MBLIF_CMD_GET_STATUS,0,buffer,&length);
//        if (status == S_SUCCESS && length == 4)
//        {
//            pcontrol = (MBLIF_CONTROL*)&buffer;
//            if (pcontrol->exec_done)
//            {
//                if (pcontrol->exec_ok)
//                {
//                    status = S_SUCCESS;
//                }
//                else
//                {
//                    status = pcontrol->status;
//                }
//                break;
//            }
//        }//if (status ...
//        else if (app_timeout())
//        {
//            status = S_TIMEOUT;
//            break;
//        }
//    }//while(1)...
//
//    return status;
//}

//------------------------------------------------------------------------------
// A helper of mblexec to issue an AIN request
// Inputs:  u8 *opcode_data
//              [2:sector index][2:reserved][4:length in byte][4:crc32e]
//          u16 opcode_datalength
// Output:  float *value
// Return:  u8  status
// Engineer: Quyen Leba
//------------------------------------------------------------------------------
//u8 mblexec_validate_flash_content(u8 *opcode_data, u16 opcode_datalength)
//{
//    MBLIF_CONTROL *pcontrol;
//    u8  buffer[256];
//    u16 length;
//    u8  status;
//
//    status = mblif_link(MBLIF_CMD_EXEC,
//                        MBLEXEC_OPCODE_VALIDATE_FLASH_CONTENT,
//                        opcode_data,&opcode_datalength);
//    if (status != S_SUCCESS)
//    {
//        return status;
//    }
//
//    app_timeout_set(600);
//    while(1)
//    {
//        status = mblif_link(MBLIF_CMD_GET_STATUS,0,buffer,&length);
//        if (status == S_SUCCESS && length == 4)
//        {
//            pcontrol = (MBLIF_CONTROL*)&buffer;
//            if (pcontrol->exec_done)
//            {
//                if (pcontrol->exec_ok)
//                {
//                    status = S_SUCCESS;
//                }
//                else
//                {
//                    status = pcontrol->status;
//                }
//                break;
//            }
//        }//if (status ...
//        else if (app_timeout())
//        {
//            status = S_TIMEOUT;
//            break;
//        }
//    }//while(1)...
//
//    return status;
//}

//------------------------------------------------------------------------------
// Request security seed
// Return:  u8  status
// Engineer: Quyen Leba
//------------------------------------------------------------------------------
u8 mblexec_request_security_seed()
{
    MBLIF_CONTROL *pcontrol;
    u8  buffer[256];
    u16 length;
    u8  status;

    length = 0;
    status = mblif_link(MBLIF_CMD_EXEC,
                        MBLEXEC_OPCODE_REQUEST_SECURITY_SEED,
                        NULL,&length);
    if (status != S_SUCCESS)
    {
        return status;
    }

    app_timeout_set(100);
    while(1)
    {
        status = mblif_link(MBLIF_CMD_GET_STATUS,0,buffer,&length);
        if (status == S_SUCCESS && length == 4)
        {
            pcontrol = (MBLIF_CONTROL*)&buffer;
            if (pcontrol->exec_done)
            {
                if (pcontrol->exec_ok)
                {
                    mblif_link(MBLIF_CMD_READ_BUFFER,0,
                               buffer,&length);
                    if (length == SECURITY_SEEDKEY_SIZE)
                    {
                        crypto_blowfish_decryptblock_internal_key
                            (buffer,SECURITY_SEEDKEY_SIZE);
                        memcpy(seedkey,buffer,SECURITY_SEEDKEY_SIZE);
                    }
                    else
                    {
                        status = S_FAIL;
                    }
                }
                else
                {
                    status = pcontrol->status;
                }
                break;
            }
        }//if (status ...
        else if (app_timeout())
        {
            status = S_TIMEOUT;
            break;
        }
    }//while(1)...

    return status;
}

//------------------------------------------------------------------------------
// Calculate and send security key
// Return:  u8  status
// Engineer: Quyen Leba
//------------------------------------------------------------------------------
u8 mblexec_validate_security_key()
{
    MBLIF_CONTROL *pcontrol;
    u8  buffer[256];
    u32 tea_key[4];
    u32 *wptr;
    u16 length;
    u32 i;
    u8  status;

    tea_key[0] = 0xA73C1823;
    tea_key[1] = 0xE81F1C05;
    tea_key[2] = 0x10E3A762;
    tea_key[3] = 0x9B64CE81;

    wptr = (u32*)seedkey;
    for(i=0;i<SECURITY_SEEDKEY_SIZE/8;i++)
    {
        tea_encryption(wptr,tea_key);
        wptr+=2;
    }
    crypto_blowfish_encryptblock_internal_key(seedkey,SECURITY_SEEDKEY_SIZE);

    length = SECURITY_SEEDKEY_SIZE;
    status = mblif_link(MBLIF_CMD_EXEC,
                        MBLEXEC_OPCODE_VALIDATE_SECURITY_KEY,
                        seedkey,&length);
    if (status != S_SUCCESS)
    {
        return status;
    }
    
    app_timeout_set(100);
    while(1)
    {
        status = mblif_link(MBLIF_CMD_GET_STATUS,0,buffer,&length);
        if (status == S_SUCCESS && length == 4)
        {
            pcontrol = (MBLIF_CONTROL*)&buffer;
            if (pcontrol->exec_done)
            {
                if (pcontrol->exec_ok)
                {
                    status = S_SUCCESS;
                }
                else
                {
                    status = pcontrol->status;
                }
                break;
            }
        }//if (status ...
        else if (app_timeout())
        {
            status = S_TIMEOUT;
            break;
        }
    }//while(1)...

    return status;
}

/**
 *  @brief OBD2 voltage monitor
 *  
 *  @details This function checks the OBD2 voltage to detect physical vehicle
 *           connection. This is used to determine if obd2_getinfo() needs to
 *           check for commtype and commlevel again.
 */
u8 mblexec_obd2_voltage_monitor()
{
    MBLIF_CONTROL *pcontrol;
    u8  buffer[256];
    u16 length;
    u8  status;
    
    length = 0;
    status = mblif_link(MBLIF_CMD_EXEC, MBLEXEC_OPCODE_OBD2_VOLTAGE_MONITOR,
                        NULL,&length);
    if (status != S_SUCCESS)
    {
        return status;
    }
    
    app_timeout_set(100);
    while(1)
    {
        status = mblif_link(MBLIF_CMD_GET_STATUS,0,buffer,&length);
        if (status == S_SUCCESS && length == 4)
        {
            pcontrol = (MBLIF_CONTROL*)&buffer;
            if (pcontrol->exec_done)
            {
                status = pcontrol->status;
                break;
            }
        }//if (status ...
        else if (app_timeout())
        {
            status = S_TIMEOUT;
            break;
        }
    }//while(1)...
    
    return status;
}