/**
  **************** (C) COPYRIGHT 2011 SCT Performance, LLC *********************
  * File Name          : datalogfeatures.h
  * Device / System    : SCT products using the "CommonCode" platform
  * Author             : Quyen Leba
  *
  * Version            : 1 
  * Date               : 05/02/2011
  * Description        : 
  *                    : 
  *
  *
  * History            : 05/02/2011 M. Davis
  *                    :   Add SCT Copyright header,
  *
  ******************************************************************************
  */

#ifndef __DATALOGFEATURES_H
#define __DATALOGFEATURES_H

#include <common/obd2datalog.h>

enum
{
    VehicleBoard_BoardRev1  = 1,
    VehicleBoard_BoardRev2  = 2,
    VehicleBoard_BoardRev3  = 3,
    VehicleBoard_BoardRev4  = 4,
};

// AIN1
#define AIN1_SHORTNAME      "AIN1"
#define AIN1_NAME           "Analog Input 1 (White Wire)"
#define AIN1_MIN            0
#define AIN1_MAX            5

// AIN2
#define AIN2_SHORTNAME      "AIN2"
#define AIN2_NAME           "Analog Input 2 (Orange Wire)"
#define AIN2_MIN            0
#define AIN2_MAX            5

// AIN3
#define AIN3_SHORTNAME      "AIN3"
#define AIN3_NAME           "Analog Input 3 (Yellow Wire)"
#define AIN3_MIN            0
#define AIN3_MAX            5

// AIN4
#define AIN4_SHORTNAME      "AIN4"
#define AIN4_NAME           "Analog Input 4 (Green Wire)"
#define AIN4_MIN            0
#define AIN4_MAX            5

// AIN5
#define AIN5_SHORTNAME      "AIN5"
#define AIN5_NAME           "Analog Input 5 (Blue Wire)"
#define AIN5_MIN            0
#define AIN5_MAX_VBREV1     12
#define AIN5_MAX            15

// AIN6
#define AIN6_SHORTNAME      "AIN6"
#define AIN6_NAME           "Analog Input 6 (Purple Wire)"
#define AIN6_MIN            0
#define AIN6_MAX_VBREV1     12
#define AIN6_MAX            15

// AIN7
#define AIN7_SHORTNAME      "AIN7"
#define AIN7_NAME           "Analog Input 7 (Red FireWire)"
#define AIN7_MIN            0
#define AIN7_24V_MAX        24
#define AIN7_15V_MAX        15

// AIN8
#define AIN8_SHORTNAME      "AIN8"
#define AIN8_NAME           "Analog Input 8 (Orange FireWire)"
#define AIN8_MIN            0
#define AIN8_24V_MAX        24
#define AIN8_5V_MAX         5

u8 datalogfeatures_getInputCount(u8 vbrev);
float datalogfeatures_calc_ain_7(u32 adccount);
float datalogfeatures_calc_ain_8(u32 adccount);

u8 datalogfeatures_check_file(const u8 *filename, u8 vb_boardrevision);
u8 datalogfeatures_create_file(const u8 *filename, u8 vb_boardrevision);

#endif    //__DATALOGFEATURES_H
