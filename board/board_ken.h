/**
  **************** (C) COPYRIGHT 2011 SCT Performance, LLC *********************
  * File Name          : board_ken.h
  * Device / System    : SCT products using the "CommonCode" platform
  * Author             : Quyen Leba
  *
  * Version            : 1 
  * Date               : 05/02/2011
  * Description        : 
  *                    : 
  *
  *
  * History            : 05/02/2011 M. Davis
  *                    :   Add SCT Copyright header,
  *
  ******************************************************************************
  */

#ifndef __BOARD_KEN_H
#define __BOARD_KEN_H

#include <arch/gentype.h>

u8 board_set_factory_default();

#endif  //__BOARD_KEN_H