/**
 *  ************* Copyright 2013 SCT Performance, LLC ******
 *  @file properties_ab_x4.c
 *  @brief Handles the properties data structure for the X4
 *         app board.
 *  
 *  @authors Patrick Downs
 *  @date 11/06/2013
 *  ********************************************************
 */

#include <board/genplatform.h>
#include <common/version.h>
#include <common/bootsettings.h>
#include <common/crypto_messageblock.h>
#include <common/statuscode.h>
#include <device_version.h>
#include <string.h>
#include "properties_ab_x4.h"
#include "bootloader_ab_x4.h"

PROPERTIES_AB_INFO properties_ab_info;

//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
void properties_ab_init()
{
    CommandResponse pxResponse;
    u8  status;
    
    // X4 Appboard properties can only be read from App board bootloader 
    //IMPORTANT: do not use MainBoot because setboot makes device vulnerable to bootsettings corruption
    //if user keeps cycling power device at startup.
    status = bootloader_ab_start_bootloader(BootloaderMode_AB_FailSafe, FALSE);
    if(status == S_SUCCESS)
    {
        status = commlink_basicCmdAck(CMDIF_CMD_GET_PROPERTIES, NULL, 
                                      NULL, &pxResponse, 1000);
        
        if(pxResponse.datalength == sizeof(properties_ab_info))
        {
            memcpy((u8*)&properties_ab_info, pxResponse.data, pxResponse.datalength);
        }
        else
        {
            status = S_FAIL;
        }
    }
    
    if(status != S_SUCCESS)
    {
        // Clear our properties to clearly indicate that they were never set
        memset((u8*)&properties_ab_info, 0, sizeof(properties_ab_info));
    }

    bootloader_ab_resetandrun();
}

//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
void properties_ab_getpropertiesafterupdate()
{
    CommandResponse pxResponse;
    u8  status;
    
    status = commlink_basicCmdAck(CMDIF_CMD_GET_PROPERTIES, NULL, 
                                  NULL, &pxResponse, 1000);
    
    if(pxResponse.datalength == sizeof(properties_ab_info))
    {
        memcpy((u8*)&properties_ab_info, pxResponse.data, pxResponse.datalength);
    }
    else
    {
        status = S_FAIL;
    }
    
    if(status != S_SUCCESS)
    {
        // Clear our properties to clearly indicate that they were never set
        memset((u8*)&properties_ab_info, 0, sizeof(properties_ab_info));
    }
}

//------------------------------------------------------------------------------
// Get AppBoard properties
// Outputs: u8  *info (vb properties; must be able to store 48 bytes)
//          u32 *info_length
// Return:  u8  status
// Engineer: Quyen Leba
//------------------------------------------------------------------------------
u8 properties_ab_getinfo(u8 *info, u32 *info_length)
{   
    if (!info || !info_length)
    {
        return S_INPUT;
    }
    
    if(properties_ab_info.operating_mode == 0)
    {
        // Properties failed to be initialized
        return S_FAIL;
    }
    
    memcpy((char*)info,(char*)&properties_ab_info,sizeof(properties_ab_info));
    *info_length = sizeof(properties_ab_info);    
    return S_SUCCESS;
}