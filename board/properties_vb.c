/**
 *  ************* Copyright 2013 SCT Performance, LLC ******
 *  @file properties_vb.c
 *  @brief Handles the properties data structure for the 
 *         vehicle board.
 *  
 *  @authors Patrick Downs
 *  @date 11/06/2013
 *  ********************************************************
 */

#include <board/genplatform.h>
#include <common/version.h>
#include <common/bootsettings.h>
#include <common/crypto_messageblock.h>
#include <common/statuscode.h>
#include <device_version.h>
#include <string.h>
#include "properties_vb.h"
#include "mblif.h"
#include "mblif_intr.h"
#include "mblif_dma.h"

//------------------------------------------------------------------------------
// Get VehicleBoard properties
// Outputs: u8  *info (vb properties; must be able to store 48 bytes)
//          u32 *info_length
// Return:  u8  status
// Engineer: Quyen Leba
//------------------------------------------------------------------------------
u8 properties_vb_getinfo(u8 *info, u32 *info_length)
{
    PROPERTIES_VB_INFO properties_vb_info;
    u8  buffer[256];
    u16 bufferlength;
    u8  settingsbuffer[MAX_CRYPTO_MESSAGE_BLOCK_LENGTH];
    u32 settingsbufferlength;
    MBLIF_RDID_INFO rdid_info;
    u16 settings_opcode;
    u8  status;

    if (!info || !info_length)
    {
        return S_INPUT;
    }
    *info_length = 0;
    
    memset((char*)&properties_vb_info,0,sizeof(properties_vb_info));
    status = mblif_link(MBLIF_CMD_RDID,0,
                        buffer,&bufferlength);
    if (status != S_SUCCESS || bufferlength != sizeof(rdid_info))
    {
        status = S_ERROR;
        goto properties_vb_info_getinfo_done;
    }
    memcpy((char*)&rdid_info,buffer,sizeof(MBLIF_RDID_INFO));

    properties_vb_info.failsafeboot_version = 0;
    properties_vb_info.mainboot_version = 0;
    properties_vb_info.app_version = 0;
    properties_vb_info.app_build = 0;
    properties_vb_info.app_signature = 0;
    
    properties_vb_info.operating_mode = rdid_info.id;
    properties_vb_info.code_sector_size = rdid_info.sector_size;
    properties_vb_info.board_rev = rdid_info.rev;
    properties_vb_info.secboot_startsector = rdid_info.secboot_startsector;
    properties_vb_info.secboot_maxcodesize = 12*1024;//rdid_info.secboot_length;
    properties_vb_info.app_startsector = rdid_info.app_startsector;
    properties_vb_info.app_maxcodesize = 98*1024;

    settings_opcode = BootSettingsOpcode_AppVersion;
    status = mblexec_call(MBLEXEC_OPCODE_GET_SETTINGS,(u8*)(&settings_opcode),2,
                          buffer,&bufferlength);
    if (status != S_SUCCESS || bufferlength != MAX_CRYPTO_MESSAGE_BLOCK_LENGTH)
    {
        status = S_ERROR;
        goto properties_vb_info_getinfo_done;
    }
    status = crypto_messageblock_decrypt(buffer,bufferlength,
                                         settingsbuffer,&settingsbufferlength,
                                         FALSE,CRYPTO_USE_INTERNAL_KEY);
    if (status == S_SUCCESS && settingsbufferlength == 4)
    {
        properties_vb_info.app_version = *(u32*)settingsbuffer;
        properties_vb_info.app_version %= 1000000;
    }
    else
    {
        status = S_ERROR;
        goto properties_vb_info_getinfo_done;
    }

    settings_opcode = BootSettingsOpcode_AppSignature;
    status = mblexec_call(MBLEXEC_OPCODE_GET_SETTINGS,(u8*)(&settings_opcode),2,
                          buffer,&bufferlength);
    if (status != S_SUCCESS || bufferlength != MAX_CRYPTO_MESSAGE_BLOCK_LENGTH)
    {
        status = S_ERROR;
        goto properties_vb_info_getinfo_done;
    }
    status = crypto_messageblock_decrypt(buffer,bufferlength,
                                         settingsbuffer,&settingsbufferlength,
                                         FALSE,CRYPTO_USE_INTERNAL_KEY);
    if (status == S_SUCCESS && settingsbufferlength == 4)
    {
        properties_vb_info.app_signature = *(u32*)settingsbuffer;
    }
    else
    {
        status = S_ERROR;
        goto properties_vb_info_getinfo_done;
    }
    
    settings_opcode = BootSettingsOpcode_MainBootVersion;
    status = mblexec_call(MBLEXEC_OPCODE_GET_SETTINGS,(u8*)(&settings_opcode),2,
                          buffer,&bufferlength);
    if (status != S_SUCCESS || bufferlength != MAX_CRYPTO_MESSAGE_BLOCK_LENGTH)
    {
        status = S_ERROR;
        goto properties_vb_info_getinfo_done;
    }
    status = crypto_messageblock_decrypt(buffer,bufferlength,
                                         settingsbuffer,&settingsbufferlength,
                                         FALSE,CRYPTO_USE_INTERNAL_KEY);
    if (status == S_SUCCESS && settingsbufferlength == 4)
    {
        properties_vb_info.mainboot_version = *(u32*)settingsbuffer;
        properties_vb_info.mainboot_version %= 1000000;
    }
    else
    {
        status = S_ERROR;
        goto properties_vb_info_getinfo_done;
    }
    
    settings_opcode = BootSettingsOpcode_FailSafeBootVersion;
    status = mblexec_call(MBLEXEC_OPCODE_GET_SETTINGS,(u8*)(&settings_opcode),2,
                          buffer,&bufferlength);
    if (status != S_SUCCESS || bufferlength != MAX_CRYPTO_MESSAGE_BLOCK_LENGTH)
    {
        status = S_ERROR;
        goto properties_vb_info_getinfo_done;
    }
    status = crypto_messageblock_decrypt(buffer,bufferlength,
                                         settingsbuffer,&settingsbufferlength,
                                         FALSE,CRYPTO_USE_INTERNAL_KEY);
    if (status == S_SUCCESS && settingsbufferlength == 4)
    {
        properties_vb_info.failsafeboot_version = *(u32*)settingsbuffer;
        properties_vb_info.failsafeboot_version %= 1000000;
    }
    else
    {
        status = S_ERROR;
        goto properties_vb_info_getinfo_done;
    }

    switch(rdid_info.id)
    {
    case 0xE71A955B:    //VehicleBoard in FailsafeBoot
        properties_vb_info.failsafeboot_version = rdid_info.version;
        break;
    case 0x36F585C2:    //VehicleBoard in MainBoot
        properties_vb_info.mainboot_version = rdid_info.version;
        break;
    default:
        properties_vb_info.app_version = rdid_info.version;
        properties_vb_info.app_build = rdid_info.build;
        break;
    }
    memcpy((char*)info,(char*)&properties_vb_info,sizeof(properties_vb_info));
    *info_length = sizeof(properties_vb_info);

properties_vb_info_getinfo_done:
    return status;
}

//------------------------------------------------------------------------------
// Check if VB fw meets version & mode requirements
// Return:  u8  status (S_SUCCESS: ok, S_OSUNMATCH: not in app mode,
//                      S_EXPIRED: old version)
// Engineer: Quyen Leba
//------------------------------------------------------------------------------
u8 properties_vb_validate_version_condition()
{
    PROPERTIES_VB_INFO properties_vb_info;
    u32 properties_vb_info_length;
    u32 version;
    u8  status;

    status = properties_vb_getinfo((u8*)&properties_vb_info, &properties_vb_info_length);
    if (status != S_SUCCESS)
    {
        return S_FAIL;
    }

    version = properties_vb_info.app_version * 1000 + properties_vb_info.app_build;
    if (properties_vb_info.operating_mode != VB_APPLICATION_OPERATING_MODE)
    {
        return S_OSUNMATCH;
    }
    else if (version < MIN_VB_VERSION_REQUIRED)
    {
        return S_EXPIRED;
    }

    return S_SUCCESS;
}
