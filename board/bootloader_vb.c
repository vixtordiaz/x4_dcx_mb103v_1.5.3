/**
  **************** (C) COPYRIGHT 2011 SCT Performance, LLC *********************
  * File Name          : bootloader_vb.c
  * Device / System    : SCT products using the "CommonCode" platform
  * Author             : Quyen Leba
  *
  * Version            : 1 
  * Date               : 05/02/2011
  * Description        : 
  *                    : 
  *
  *
  * History            : 05/02/2011 M. Davis
  *                    :   Add SCT Copyright header,
  *
  ******************************************************************************
  */

//------------------------------------------------------------------------------
// Log error point: 0x7400 -> 0x74FF
//------------------------------------------------------------------------------

#include <string.h>
#include <device_config.h>
#include <board/delays.h>
#include <common/statuscode.h>
#include <common/crc32.h>
#include <common/crypto_blowfish.h>
#include <common/tea.h>
#include <common/filecrypto.h>
#include <common/bootsettings.h>
#include "bootloader_vb.h"

extern funcptr_mblif_link* mblif_link;              //from mblif.c
MBLIF_RDID_INFO rdid_info;
struct
{
    struct
    {
        u8  init        :1;
        u8  usefsboot   :1;
        u8  reserved    :6;
    }control;
    u32 fwcrc32e;
}bootloader_vb_info = 
{
    .control.init = 0,
    .fwcrc32e = 0,    
};

u8 __vbupdater_init(FirmwareHeader *fwheader);
u8 __vbupdater_do(u8 *data, u16 datalength);
u8 __vbupdater_validate(u32 fwcrc32e);

//------------------------------------------------------------------------------
// Input:   FirmwareHeader fwheader (encrypted with critcal key)
// Return:  u8  status
// Engineer: Quyen Leba
//------------------------------------------------------------------------------
u8 bootloader_vb_init(FirmwareHeader fwheader)
{
    u32 *wptr;
    u8  status;
    
    if(fwheader.firmware_flags & FIRMWARE_FLAGS_UPDATE_WITH_FAILSAFE)
    {
        bootloader_vb_info.control.usefsboot = 1;
    }
    
    bootloader_vb_info.fwcrc32e = 0xFFFFFFFF;    
    // CHECK THIS: Why is this recalculated?
    wptr = (u32*)&fwheader;
    wptr++;
    crc32e_reset();
    fwheader.hcrc = crc32e_calculateblock(0xFFFFFFFF,
                                          (u32*)wptr,
                                          (sizeof(FirmwareHeader)-4)/4);
    crypto_blowfish_encryptblock_internal_key((u8*)&fwheader,
                                              sizeof(FirmwareHeader));
    status = __vbupdater_init(&fwheader);
    crc32e_sw_reset();
    
    return status;
}

//------------------------------------------------------------------------------
// Inputs:  u8  *data (encrypted with critcal key)
//          u32 datalength (multiple of 8 bytes)
// Return:  u8  status
// Engineer: Quyen Leba
//------------------------------------------------------------------------------
u8 bootloader_vb_do(u8 *data, u32 datalength)
{
    crypto_blowfish_decryptblock_critical(data,datalength);
    // Calculate CRC32 by software for validation
    bootloader_vb_info.fwcrc32e = crc32e_sw_calculateblock
        (bootloader_vb_info.fwcrc32e,(u32*)data,datalength/4);
    crypto_blowfish_encryptblock_internal_key(data,datalength);
    return __vbupdater_do(data,datalength);
}

//------------------------------------------------------------------------------
// Validate VehicleBoard firmware
// Return:  u8  status
// Engineer: Quyen Leba
//------------------------------------------------------------------------------
u8 bootloader_vb_validate()
{
    return __vbupdater_validate(bootloader_vb_info.fwcrc32e);
}

//------------------------------------------------------------------------------
// Init VehicleBoard in bootloader
// Input:   FirmwareHeader *fwheader
// Return:  u8  status
// Engineer: Quyen Leba, Patrick Downs
//------------------------------------------------------------------------------
// log_push_error_point(0x7422); - Previously used in vbupdater_init to detect 
//                                  unlock fail when bootmode is unknown, now abandoned
u8 __vbupdater_init(FirmwareHeader *fwheader)
{
    u8  status;
    
    status = bootloader_vb_setbootmode(BootloaderMode_VB_Main);
    if(status != S_SUCCESS || bootloader_vb_info.control.usefsboot)
    {
        // Try BOOT_EN pin method
        status = bootloader_vb_forceboot_booten_pin();
        if(status != S_SUCCESS)
        {
            goto __vbupdater_init_error;
        }
    }

    status = mblexec_unlock_security();
    if (status != S_SUCCESS)
    {
        log_push_error_point(0x7424);
        goto __vbupdater_init_error;
    }
    
    status = mblexec_call(MBLEXEC_OPCODE_BOOTLOADER_INIT,
                          (u8*)fwheader,sizeof(FirmwareHeader),NULL,NULL);
    if (status != S_SUCCESS)
    {
        log_push_error_point(0x7425);
        goto __vbupdater_init_error;
    }
    mblexec_lock_security();
    bootloader_vb_info.control.init = 1;
    
__vbupdater_init_error:
    return status;
}

/**
 *  __vbupdater_setbootmode
 *  
 *  @brief Set VehicleBoard boot mode
 *  
 *  @param [in] newmode        New boot mode a BootloaderMode enum
 *  @retval                 returns a STATUS_CODE enum
 *  
 *  @details Sets the boot mode for the vehicle board. This will save a new 
 *           bootmode into it's bootsettings and then reset itself.
 *  
 *  @authors Patrick Downs
 *  
 */
u8 bootloader_vb_setbootmode(BootloaderMode newmode)
{
    u8  status;
    u8  buffer[256];
    u16 bufferlength;
    BootloaderMode currentmode;
    
    status = bootloader_vb_getbootmode(&currentmode);
    if(status == S_SUCCESS)
    {
        if(currentmode != newmode)
        {
            status = mblexec_unlock_security();
            if (status == S_SUCCESS)
            {
                bufferlength = 2;
                buffer[0] = newmode & 0xFF;
                buffer[1] = newmode >> 8;
                status = mblexec_call(MBLEXEC_OPCODE_BOOTLOADER_SETBOOT,
                                      buffer,bufferlength,NULL,NULL);
                if (status == S_SUCCESS)
                {
                    delays_counter(DELAYS_COUNTER_100MS); // Give time to reboot
                    mblif_force_interruptmode(); // VB always boots in MBLIF interrupt mode
                    status = bootloader_vb_getbootmode(&currentmode);
                    if(status == S_SUCCESS)
                    {
                        if(currentmode != newmode)
                        {
                            log_push_error_point(0x7421);
                            status = S_FAIL;
                        }
                    }
                }
                else
                {
                    log_push_error_point(0x7423);                    
                }
            }
        }
        //else Already in correct mode
    }
    
    return status;
}

/**
 *  __vbupdater_getbootmode
 *  
 *  @brief Get VehicleBoard boot mode
 *  
 *  @param [out] mode    Current boot mode a BootloaderMode enum
 *  @retval             returns a STATUS_CODE enum
 *  
 *  @details Gets the current boot mode of the vehicle board.
 *  
 *  @authors Patrick Downs
 *  
 */
u8 bootloader_vb_getbootmode(BootloaderMode *mode)
{
    u8  status;
    u8  buffer[256];
    u16 bufferlength;
    
    status = mblif_link(MBLIF_CMD_RDID,0,buffer,&bufferlength);
    if (status == S_SUCCESS && bufferlength == sizeof(rdid_info))
    {
        memcpy((char*)&rdid_info,buffer,sizeof(rdid_info));
        
        if(rdid_info.id == VB_FAILSAFE_BOOTLOADER_OPERATING_MODE)
        {
            *mode = BootloaderMode_VB_FailSafe;
        }
        else if(rdid_info.id == VB_MAIN_BOOTLOADER_OPERATING_MODE)
        {
            *mode = BootloaderMode_VB_Main;
        }
        else if(rdid_info.id == VB_APPLICATION_OPERATING_MODE)
        {
            *mode = BootloaderMode_VB_Application;
        }
        else
        {
            status = S_ERROR;
        }
    }
    else
    {
        log_push_error_point(0x7420);
        status = S_ERROR;
    }
    
    return status;

}

/**
 *  bootloader_vb_forceboot_booten_pin
 *  
 *  @brief Forces the Vehicle Board to reset and boot into failsafe bootloader
 *  
 *  @retval             returns a STATUS_CODE enum
 *  
 *  @details This will set the BOOT_EN pin and reset the Vehicle Board. On start
 *           up the bootloader checks this pin. If it's set it will stay running
 *           a not boot into any other mode.
 *  
 *  @authors Patrick Downs
 *  
 */
u8 bootloader_vb_forceboot_booten_pin()
{
    u8  status;
    BootloaderMode currentmode;
        
    mblif_force_interruptmode();
    
    // Force App Board into bootloader by settings boot enable
    gpio_set_booten(TRUE);
    gpio_reset_vehicle_board();
    gpio_set_booten(FALSE); // Only needed on boot up. Normally keep disabled
    
    status = bootloader_vb_getbootmode(&currentmode);
    if (status == S_SUCCESS)
    {
        if(currentmode != BootloaderMode_VB_FailSafe)
        {
            status = S_FAIL;
        }
    }
    
    return status;
}

/**
 *  bootloader_vb_resetandrun
 *  
 *  @brief      Resets and runs the vehicle board
 *  
 *  @retval             returns a STATUS_CODE enum
 *  
 *  @details    Resets the VB and then reinitalizes MBLIF mode
 *           
 *  
 *  @authors Patrick Downs
 *  
 */
void bootloader_vb_resetandrun()
{
    gpio_reset_vehicle_board();
    
    mblif_init();
}


//------------------------------------------------------------------------------
// Program VehicleBoard firmware data
// Inputs:  u8  *data
//          u16 datalength (multiple of 8 bytes)
// Return:  u8  status
// Engineer: Quyen Leba
//------------------------------------------------------------------------------
u8 __vbupdater_do(u8 *data, u16 datalength)
{
    u16 index;
    u16 length;
    u8  status;

    if (bootloader_vb_info.control.init == 0)
    {
        log_push_error_point(0x7430);
        status = S_ERROR;
        goto __vbupdater_do_done;
    }
    
    status = mblexec_unlock_security();
    if (status != S_SUCCESS)
    {
        log_push_error_point(0x7431);
        goto __vbupdater_do_done;
    }

    index = 0;
    while(index < datalength)
    {
        if ((index + rdid_info.sector_size) <= datalength)
        {
            length = rdid_info.sector_size;
        }
        else
        {
            length = datalength - index;
        }
        
        status = mblif_link(MBLIF_CMD_WRITE_BUFFER, 0,
                            (u8*)&data[index],&length);
        if (status != S_SUCCESS)
        {
            log_push_error_point(0x7432);
            goto __vbupdater_do_done;
        }
        
        status = mblexec_call(MBLEXEC_OPCODE_BOOTLOADER_DO,NULL,NULL,NULL,NULL);
        if (status != S_SUCCESS)
        {
            log_push_error_point(0x7433);
            goto __vbupdater_do_done;
        }
        
        index += length;
    }

    mblexec_lock_security();

__vbupdater_do_done:
    if (status != S_SUCCESS)
    {
        bootloader_vb_info.control.init = 0;
    }
    return status;
}

//------------------------------------------------------------------------------
// Validate VehicleBoard firmware data
// Input:   u32 fwcrc32e
// Return:  u8  status
// Engineer: Quyen Leba
//------------------------------------------------------------------------------
u8 __vbupdater_validate(u32 fwcrc32e)
{
    u8  status;

    if (bootloader_vb_info.control.init == 0)
    {
        log_push_error_point(0x7440);
        status = S_ERROR;
        goto __vbupdater_validate_done;
    }
    bootloader_vb_info.control.init = 0;

    status = mblexec_unlock_security();
    if (status != S_SUCCESS)
    {
        log_push_error_point(0x7441);
        goto __vbupdater_validate_done;
    }
    
    status = mblexec_call(MBLEXEC_OPCODE_BOOTLOADER_VALIDATE,
                          (u8*)&fwcrc32e,4,NULL,NULL);
    if (status != S_SUCCESS)
    {
        log_push_error_point(0x7442);
        goto __vbupdater_validate_done;
    }
    // Wait for watch dog reset & VB to be ready
    delays_counter(DELAYS_COUNTER_100MS);
    delays_counter(DELAYS_COUNTER_100MS);
    delays_counter(DELAYS_COUNTER_100MS);
    delays_counter(DELAYS_COUNTER_100MS);
    delays_counter(DELAYS_COUNTER_100MS);
    mblif_init();

//    mblexec_lock_security();
    
__vbupdater_validate_done:
    return status;
}
