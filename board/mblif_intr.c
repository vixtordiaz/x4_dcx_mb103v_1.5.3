/**
  **************** (C) COPYRIGHT 2013 SCT Performance, LLC *********************
  * File Name          : mblif_intr.c
  * Device / System    : SCT products using the "CommonCode" platform
  * Author             : Quyen Leba, Patrick Downs
  *
  * Version            : 1 
  * Date               : 01/10/2013
  * Description        : 
  *                    : 
  *
  *
  *
  ******************************************************************************
  */

#include <string.h>
#include <board/genplatform.h>
#include <board/delays.h>
#include <common/statuscode.h>
#include "mblif.h"
#include "mblif_intr.h"

#define MAX_READBUFFER_SIZE         4096

//for now
//#define mblif_sendbyte(b)           spi_master_send_byte(b)
//#define mblif_receivebyte()         spi_master_receive_byte()
#define mblif_isbusy()              (gpio_read_dav_v_busy_pin() == GPIO_PIN_STATUS_HIGH)
u8 mblif_transferbyte(u8 txbyte, u8* recvbyte);

//------------------------------------------------------------------------------
// Private prototypes
//------------------------------------------------------------------------------
u8 mblif_get_status(u8 *status, u16 *statuslength);
u8 mblif_rdid(u8 *id, u16 *idlength);
u8 mblif_writebuffer(u8 *buffer, u16 bufferlength);
u8 mblif_appendbuffer(u8 *buffer, u16 bufferlength);
u8 mblif_readbuffer(u8 *buffer, u16 *bufferlength);
u8 mblif_exec(u16 opcode, u8 *data, u8 datalength);

//------------------------------------------------------------------------------
// Inputs:  u8  cmd
//          u16 opcode (only used in certain cmds)
//          u8  *data
//          u8  *datalength
// Return:  u8  status
// Engineer: Quyen Leba
//------------------------------------------------------------------------------
void mblif_intr_init()
{
    spi_master_moduleinitonly(FALSE);
}

//------------------------------------------------------------------------------
// Inputs:  u8  cmd
//          u16 opcode (only used in certain cmds)
//          u8  *data
//          u8  *datalength
// Return:  u8  status
// Engineer: Quyen Leba
//------------------------------------------------------------------------------
u8 mblif_intr_link(u8 cmd, u16 opcode, u8 *data, u16 *datalength)
{
    u8  status;

    board_timeout_set(1000);
    while(mblif_isbusy())
    {
        if (board_timeout())
        {
            return S_TIMEOUT;
        }
    }

    spi_master_enable_cs0();

    switch(cmd)
    {
    case MBLIF_CMD_GET_STATUS:
        status = mblif_get_status(data,datalength);
        break;
    case MBLIF_CMD_RDID:
        status = mblif_rdid(data,datalength);
        break;
    case MBLIF_CMD_WRITE_BUFFER:
        status = mblif_writebuffer(data,*datalength);
        break;
    case MBLIF_CMD_APPEND_BUFFER:
        status = mblif_appendbuffer(data,*datalength);
        break;
    case MBLIF_CMD_READ_BUFFER:
        status = mblif_readbuffer(data,datalength);
        break;
    case MBLIF_CMD_EXEC:
        status = mblif_exec(opcode,data,(u8)*datalength);
        break;
    default:
        status = S_INPUT;
        break;
    }

    spi_master_disable_cs0();

    return status;
}

u8 mblif_transferbyte(u8 txbyte, u8* recvbyte)
{        
    board_timeout_set(1000);
    while(mblif_isbusy())
    {
        if (board_timeout())
            return S_TIMEOUT;
    }
    
    spi_master_send_byte(txbyte);
    *recvbyte = spi_master_receive_byte();
    
    return S_SUCCESS;
}

u8 mblif_sendbyte(u8 byte)
{
    u8 status;
    
    board_timeout_set(1000);
    while(mblif_isbusy())
    {
        if (board_timeout())        
            return S_TIMEOUT;
    }
    
    status = spi_master_send_byte(byte);
    
    return status;    
}

u8 mblif_receivebyte(u8 *recvbyte)
{
    board_timeout_set(1000);
    while(mblif_isbusy())
    {
        if (board_timeout())        
            return S_TIMEOUT;
    }
    *recvbyte = spi_master_receive_byte();
    
    return S_SUCCESS;
}

//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
u8 mblif_get_status(u8 *status, u16 *statuslength)
{
    u8  sequenceresponse;
    u8  i;
    u8  rstatus;
    
    rstatus = mblif_transferbyte((u8)MBLIF_CMD_GET_STATUS, &sequenceresponse);
    if(rstatus != S_SUCCESS)
        return rstatus;
    
    rstatus = mblif_transferbyte((u8)MBLIF_CMD_GET_STATUS_S1, &sequenceresponse);
    if(rstatus != S_SUCCESS)
        return rstatus;

    if (sequenceresponse != MBLIF_CMD_RESPONSE_S1)
        return S_UNMATCH;
    
    for(i=0;i<4;i++)
    {
        rstatus = mblif_transferbyte((u8)MBLIF_CMD_RESPONSE_S4, &status[i]);
        if(rstatus != S_SUCCESS)
            return rstatus;
    }
    
    *statuslength = 4;

    return S_SUCCESS;
}

//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
u8 mblif_rdid(u8 *id, u16 *idlength)
{
    u8  sequenceresponse;
    u8  length;
    u8  i;
    u8  rstatus;
    
    rstatus = mblif_transferbyte((u8)MBLIF_CMD_RDID, &sequenceresponse);
    if(rstatus != S_SUCCESS)
        return rstatus;
    
    rstatus = mblif_transferbyte((u8)MBLIF_CMD_RDID_S1, &sequenceresponse);
    if(rstatus != S_SUCCESS)
        return rstatus;
    
    if (sequenceresponse != MBLIF_CMD_RESPONSE_S1)
        return S_UNMATCH;    
    
    rstatus = mblif_transferbyte((u8)MBLIF_CMD_RDID_S2, &sequenceresponse);
    if(rstatus != S_SUCCESS)
        return rstatus;
    
    if (sequenceresponse != MBLIF_CMD_RESPONSE_S2)
        return S_UNMATCH;

    //time to prepare data
    delays_counter(DELAYS_COUNTER_10MS);
    
    rstatus = mblif_transferbyte((u8)MBLIF_CMD_RDID_S3, &length);
    if(rstatus != S_SUCCESS)
        return rstatus;

    for(i=0;i<length;i++)
    {
        rstatus = mblif_transferbyte((u8)0xFF, &id[i]);
        if(rstatus != S_SUCCESS)
            return rstatus;
    }
    
    *idlength = length;

    return S_SUCCESS;
}

//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
u8 mblif_writebuffer(u8 *buffer, u16 bufferlength)
{
    u8  sequenceresponse;
    u16 i;
    u8 rstatus;

    rstatus = mblif_transferbyte((u8)MBLIF_CMD_WRITE_BUFFER, &sequenceresponse);
    if(rstatus != S_SUCCESS)
        return rstatus;

    rstatus = mblif_transferbyte((u8)MBLIF_CMD_WRITE_BUFFER_S1, &sequenceresponse);
    if(rstatus != S_SUCCESS)
        return rstatus;
    
    if (sequenceresponse != MBLIF_CMD_RESPONSE_S1)
        return S_UNMATCH;
    
    rstatus = mblif_transferbyte((u8)(bufferlength >> 8), &sequenceresponse); //TODOQ: wrong endian
    if(rstatus != S_SUCCESS)
        return rstatus;
    
    if (sequenceresponse != MBLIF_CMD_RESPONSE_S2)
        return S_UNMATCH;
    
    rstatus = mblif_transferbyte((u8)(bufferlength & 0xFF), &sequenceresponse);
    if(rstatus != S_SUCCESS)
        return rstatus;
    
    if (sequenceresponse != MBLIF_CMD_RESPONSE_S3)
        return S_UNMATCH;
    
    for(i=0;i<bufferlength;i++)
    {
        rstatus = mblif_transferbyte(*buffer++, &sequenceresponse);
        if(rstatus != S_SUCCESS)
            return rstatus;
        
        if (sequenceresponse != MBLIF_CMD_RESPONSE_S4)
            return S_UNMATCH;
    }
    return S_SUCCESS;
}

//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
u8 mblif_appendbuffer(u8 *buffer, u16 bufferlength)
{
    u8  sequenceresponse;
    u16 i;
    u8 rstatus;

    rstatus = mblif_transferbyte((u8)MBLIF_CMD_APPEND_BUFFER, &sequenceresponse);
    if(rstatus != S_SUCCESS)
        return rstatus;

    rstatus = mblif_transferbyte((u8)MBLIF_CMD_APPEND_BUFFER_S1, &sequenceresponse);
    if(rstatus != S_SUCCESS)
        return rstatus;
    
    if (sequenceresponse != MBLIF_CMD_RESPONSE_S1)
        return S_UNMATCH;
    
    rstatus = mblif_transferbyte((u8)(bufferlength >> 8), &sequenceresponse); //TODOQ: wrong endian
    if(rstatus != S_SUCCESS)
        return rstatus;
    
    if (sequenceresponse != MBLIF_CMD_RESPONSE_S2)
        return S_UNMATCH;
    
    rstatus = mblif_transferbyte((u8)(bufferlength & 0xFF), &sequenceresponse);
    if(rstatus != S_SUCCESS)
        return rstatus;
    
    if (sequenceresponse != MBLIF_CMD_RESPONSE_S3)
        return S_UNMATCH;
    
    for(i=0;i<bufferlength;i++)
    {
        rstatus = mblif_transferbyte(*buffer++, &sequenceresponse);
        if(rstatus != S_SUCCESS)
            return rstatus;
        
        if (sequenceresponse != MBLIF_CMD_RESPONSE_S4)
            return S_UNMATCH;
    }
    return S_SUCCESS;
}

//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
u8 mblif_readbuffer(u8 *buffer, u16 *bufferlength)
{
    u16 i;
    u16 length;
    u8  sequenceresponse;
    u8  tmp;
    u8 rstatus;
    
    rstatus = mblif_transferbyte((u8)MBLIF_CMD_READ_BUFFER, &sequenceresponse);
    if(rstatus != S_SUCCESS)
        return rstatus;

    rstatus = mblif_transferbyte((u8)MBLIF_CMD_READ_BUFFER_S1, &sequenceresponse);
    if(rstatus != S_SUCCESS)
        return rstatus;

    if (sequenceresponse != MBLIF_CMD_RESPONSE_S1)
        return S_UNMATCH;
    
    rstatus = mblif_transferbyte((u8)MBLIF_CMD_READ_BUFFER_S2, &sequenceresponse);
    if(rstatus != S_SUCCESS)
        return rstatus;
    
    length = (u16)sequenceresponse << 8;
    
    rstatus = mblif_transferbyte((u8)MBLIF_CMD_READ_BUFFER_S3, &sequenceresponse);
    if(rstatus != S_SUCCESS)
        return rstatus;
    
    length |= (u16)sequenceresponse;

    if (length > MAX_READBUFFER_SIZE)
    {
        return S_BADCONTENT;
    }

    for(i=0;i<length;i++)
    {
        rstatus = mblif_transferbyte((u8)MBLIF_CMD_RESPONSE_S4, &tmp);
        if(rstatus != S_SUCCESS)
            return rstatus;
        
        *buffer++ = tmp;
    }
    
    *bufferlength = length;
    
    return S_SUCCESS;
}

//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
u8 mblif_exec(u16 opcode, u8 *data, u8 datalength)
{
    u8  sequenceresponse;
    u16 i;
    u8 rstatus;

    rstatus = mblif_transferbyte((u8)MBLIF_CMD_EXEC, &sequenceresponse);
    if(rstatus != S_SUCCESS)
        return rstatus;
    
    rstatus = mblif_transferbyte((u8)MBLIF_CMD_EXEC_S1, &sequenceresponse);
    if(rstatus != S_SUCCESS)
        return rstatus;
    
    if (sequenceresponse != MBLIF_CMD_RESPONSE_S1)
    {
        return S_UNMATCH;
    }
    
    rstatus = mblif_transferbyte((u8)MBLIF_CMD_EXEC_S2, &sequenceresponse);
    if(rstatus != S_SUCCESS)
        return rstatus;
    
    if (sequenceresponse != MBLIF_CMD_RESPONSE_S2)
    {
        return S_UNMATCH;
    }
    
    rstatus = mblif_transferbyte((u8)(datalength + 2), &sequenceresponse);
    if(rstatus != S_SUCCESS)
        return rstatus;
    
    if (sequenceresponse != MBLIF_CMD_RESPONSE_S3)
    {
        return S_UNMATCH;
    }
    
    rstatus = mblif_transferbyte((u8)(opcode & 0xFF), &sequenceresponse);
    if(rstatus != S_SUCCESS)
        return rstatus;
    
    if (sequenceresponse != MBLIF_CMD_RESPONSE_S4)
    {
        return S_UNMATCH;
    }
    
    rstatus = mblif_transferbyte((u8)(opcode >> 8), &sequenceresponse);
    if(rstatus != S_SUCCESS)
        return rstatus;
    
    if (sequenceresponse != MBLIF_CMD_RESPONSE_S4)
    {
        return S_UNMATCH;
    }
    
    //May need small delay here for bootloader, time to prepare data
    //But found slow down cauese missed frames during vehicle communication
    //Need futher testing...
    //    delays(1, 'm');
    
    for(i=0;i<datalength;i++)
    {
        rstatus = mblif_transferbyte((u8)(*data++), &sequenceresponse);
        if(rstatus != S_SUCCESS)
            return rstatus;
        
        if (sequenceresponse != MBLIF_CMD_RESPONSE_S4)
        {
            return S_UNMATCH;
        }
    }
    
    //Same issue as above...
    //    delays(1, 'm');
    
    return S_SUCCESS;
}
