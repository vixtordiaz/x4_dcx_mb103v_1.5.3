/**
  **************** (C) COPYRIGHT 2011 SCT Performance, LLC *********************
  * File Name          : bootloader_ab_x4.c
  * Device / System    : SCT products using the "CommonCode" platform
  * Author             : Quyen Leba
  *
  * Version            : 1 
  * Date               : 05/02/2011
  * Description        : 
  *                    : 
  *
  *
  * History            : 05/02/2011 M. Davis
  *                    :   Add SCT Copyright header,
  *
  ******************************************************************************
  */

//------------------------------------------------------------------------------
// Log error point: 0x7000 -> 0x70FF
//------------------------------------------------------------------------------

#include <board/genplatform.h>
#include <string.h>
#include <ctype.h>
#include <device_config.h>
#include <common/statuscode.h>
#include <common/crc32.h>
#include <common/crypto_blowfish.h>
#include <common/tea.h>
#include <common/filecrypto.h>
#include <common/bootsettings.h>
#include "bootloader_ab_x4.h"
#include <board/indicator.h>
#include <common/housekeeping.h>
#include "commlink.h"
#include <board/delays.h>

#define APPBOARD_X4_FW_FILENAME                         "X4App.bin"

struct
{
    struct
    {
        u8  init        :1;
        u8  usefsboot   :1;
        u8  reserved    :6;
    }control;
    u32 operating_mode;
    u32 fwcrc32e;
    F_FILE* fptr;
    u32 calccrc32e;
}bootloader_ab_info = 
{
    .control.init = 0,
    .fwcrc32e = 0,
    .fptr = NULL,
};

u8 __abupdater_init(FirmwareHeader *fwheader);
u8 __abupdater_do(u8 *data, u16 datalength);
u8 __abupdater_validate(u32 fwcrc32e);

//------------------------------------------------------------------------------
// Input:   FirmwareHeader fwheader (No Encryption)
// Return:  u8  status
// Engineer: Quyen Leba/Patrick Downs
//------------------------------------------------------------------------------
u8 bootloader_ab_init(FirmwareHeader fwheader)
{
    u8  status;
    
    bootloader_ab_info.control.init = 1;
    bootloader_ab_info.fwcrc32e = fwheader.firmware_crc32e;  
    
    if(fwheader.firmware_flags & FIRMWARE_FLAGS_UPDATE_WITH_FAILSAFE) // Check Firmware Flags for Update With Failsafe flag
    {
        bootloader_ab_info.control.usefsboot = 1;
    }
    
    // Encrypt the firmware header with External Key for App Board because
    // External Key is the only key it can support.
    crypto_blowfish_encryptblock_external_key((u8*)&fwheader,sizeof(FirmwareHeader));
    
    status = __abupdater_init(&fwheader);
    
    crc32e_sw_reset();
    bootloader_ab_info.calccrc32e = 0xFFFFFFFF;
    
    return status;
}

//------------------------------------------------------------------------------
// Input:   FirmwareHeader fwheader (No Encryption)
// Return:  u8  status
// Engineer: Quyen Leba/Patrick Downs
//------------------------------------------------------------------------------
u8 bootloader_ab_init_file(FirmwareHeader fwheader)
{
    if (bootloader_ab_info.fptr)
    {
        genfs_closefile(bootloader_ab_info.fptr);
        bootloader_ab_info.fptr = NULL;
    }
    bootloader_ab_info.fptr = genfs_user_openfile(AB_FWUPDATE_TEMP,"wb");
    if (!bootloader_ab_info.fptr)
    {
        return S_OPENFILE;
    }
    
    bootloader_ab_info.control.init = 1;
    bootloader_ab_info.fwcrc32e = fwheader.firmware_crc32e;
    
    crc32e_sw_reset();
    bootloader_ab_info.calccrc32e = 0xFFFFFFFF;
    
    return S_SUCCESS;
}

/**
 *  bootloader_ab_progressbar
 *  
 *  @brief Display progress bar and message to GUI
 *  
 *  @param [in] percentage Percentage completer 0-100
 *  @param [in] message     Short string to display with progress
 *  @retval                 returns a STATUS_CODE enum
 *  
 *  @details Displays the progress bar and string while in app board main
 *              bootloader.
 *  
 *  @authors Patrick Downs
 *  
 */
u8 bootloader_ab_progressbar(u8 percentage, u8 *message)
{
    CommandResponse pxResponse;
    u8 data[64];
    u32 datalength;
    u8 status;

    if(bootloader_ab_info.operating_mode == BootloaderMode_AB_Main)
    {
        memset(data, 0, sizeof(data));
        
        // Data [1:Pecentage]{n:Message][NULL]
        if(percentage > 100)
        {
            percentage = 100;
        }
        data[0] = percentage;
        datalength = 1;
        
        if(strlen((char*)message) > (sizeof((char*)data)-2)) // Check messgae length less than data length including percentage and NULL
        {
            message[(sizeof(data)-2)] = 0x00; // Truncate with null
        }
        strcpy((char*)(data+1), (char const*)message);
        datalength += strlen((char*)message)+1;
        
        // Progress bar
        status = commlink_basicCmdAck(CMDIF_CMD_PROGRESS_BAR, (const u8*)data, 
                                      datalength, &pxResponse, 1000);
        if(status == S_SUCCESS)
        {
            if(pxResponse.responseCode != CMDIF_ACK_OK)
            {
                status = S_FAIL;
            }
        }
    }
    else
    {
        status = S_NOTSUPPORT;
    }
    
    return status;
}

//------------------------------------------------------------------------------
// Inputs:  u8  *data (encrypted with critcal key)
//          u32 datalength (multiple of 8 bytes)
// Return:  u8  status
// Engineer: Quyen Leba
//------------------------------------------------------------------------------
#define BOOTLOADER_AB_COMMLINK_MAXSIZE 2048
u8 bootloader_ab_do(u8 *data, u32 datalength)
{
    u8 *blockptr;
    u16 blocklen;
    u32 blockindex;
    u8 status;
    
    // Flash data is encrypted with Critcal (Data) Key in the FWU file
    // we have to decrypt it and re-encrypt it with External Key because
    // App board only can use External Key.
    
    crypto_blowfish_decryptblock_critical(data,datalength);
    crypto_blowfish_encryptblock_external_key(data,datalength); 
    
    // Calculate CRC32 by software for validation
    bootloader_ab_info.calccrc32e = crc32e_sw_calculateblock(bootloader_ab_info.calccrc32e,
                                                             (u32*)data,datalength/4);
    
    blocklen = 0;
    blockindex = 0;
    while(datalength-blockindex)
    {
        blockptr = (u8*)(data+blockindex);
        if(datalength > BOOTLOADER_AB_COMMLINK_MAXSIZE)
        {
            blocklen = BOOTLOADER_AB_COMMLINK_MAXSIZE;
        }
        else
        {
            blocklen = datalength;
        }
        
        status = __abupdater_do(blockptr, blocklen);
        if(status != S_SUCCESS)
        {
            break;            
        }
        
        blockindex += blocklen;
    }
    
    return status;
}

//------------------------------------------------------------------------------
// Inputs:  u8  *data (encrypted with critcal key)
//          u32 datalength (multiple of 8 bytes)
// Return:  u8  status
// Engineer: Quyen Leba/Patrick Downs
//------------------------------------------------------------------------------
u8 bootloader_ab_do_file(u8 *data, u32 datalength)
{
    u32 bytecount;
    
    if (bootloader_ab_info.fptr)
    {
        // Calculate CRC32 by software for validation
        bootloader_ab_info.calccrc32e = crc32e_sw_calculateblock(bootloader_ab_info.calccrc32e,
                                                                 (u32*)data,datalength/4);

        bytecount = fwrite(data,1,datalength,bootloader_ab_info.fptr);
        if(bytecount == datalength)
        {
            return S_SUCCESS;
        }
        else
        {
            return S_WRITEFILE;
        }
    }
    else
    {
        return S_OPENFILE;
    }
}

//------------------------------------------------------------------------------
// Validate VehicleBoard firmware
// Return:  u8  status
// Engineer: Quyen Leba
//------------------------------------------------------------------------------
u8 bootloader_ab_validate()
{
    u8  status;

    status = __abupdater_validate(bootloader_ab_info.calccrc32e);
    if(status == S_SUCCESS)
    {
        properties_ab_getpropertiesafterupdate();
        bootloader_ab_resetandrun();
    }
    
    return status;
}

//------------------------------------------------------------------------------
// Validate VehicleBoard firmware
// Return:  u8  status - S_LOADFILE should indicate to calling function that
//              bootloader_ab_updatebyfile() must be called next to auctually load
//              the firmware.
// Engineer: Quyen Leba/Patrick Downs
//------------------------------------------------------------------------------
u8 bootloader_ab_validate_file()
{
    if (bootloader_ab_info.fptr)
    {
        genfs_closefile(bootloader_ab_info.fptr);
        bootloader_ab_info.fptr = NULL;
    }

    if (bootloader_ab_info.fwcrc32e != bootloader_ab_info.calccrc32e)
    {
        return S_CRC32E;
    }
    
    return S_LOADFILE;
}

//------------------------------------------------------------------------------
// Program AppBoard firmware data from a file
// Inputs:  u8  *data
//          u16 datalength (multiple of 8 bytes)
// Return:  u8  status
// Engineer: Quyen Leba
//------------------------------------------------------------------------------
u8 bootloader_ab_updatebyfile()
{
    return S_NOTSUPPORT;
}

/**
 *  bootloader_ab_setcommlinkbaud
 *  
 *  @brief Set commlink baud to a specific baud rate
 *  
 *  @param [in] baud        New commlink baud rate, CommLinkBaudrate enum
 *  @retval                 returns a STATUS_CODE enum
 *  
 *  @details Changes the baud rate of the commlink
 *  
 *  @authors Patrick Downs
 *  
 */
u8 bootloader_ab_setcommlinkbaud(CommLinkBaudrate baud)
{
    u8 status;
    CommandResponse pxResponse;
    u8  cmddata[6];
    u16 cmdlength;
    
    cmddata[0] = HardwareAccess_SetCommLinkSpeed;   //2byte2: opcode
    cmddata[1] = (HardwareAccess_SetCommLinkSpeed >> 8);
    cmddata[2] = baud;                              //2bytes: opcodedata
    cmddata[3] = 0x00;
    cmdlength = 4;
    
    status = commlink_basicCmdAck(CMDIF_CMD_HARDWARE,(const u8*)cmddata,
                                  cmdlength, &pxResponse, 2000);
    if(status == S_SUCCESS)
    {
        if(pxResponse.responseCode == CMDIF_ACK_OK)
        {
            switch(baud)
            {
                // Supported baud rates
            case COMMLINK_115200_BAUD:
            case COMMLINK_230400_BAUD:
            case COMMLINK_460800_BAUD:
            case COMMLINK_921600_BAUD:
                commlink_hal_change_baudrate(baud);
                break;
            default:
                status = S_FAIL;
                break;
            }
        }
    }    
    delays_counter(DELAYS_COUNTER_100MS);
    
    return status;
}

/**
 *  bootloader_ab_setbacklight
 *  
 *  @brief Set LCD backlight brightness for bootloader
 *  
 *  @param [in] brightness  brightness percentage 0-100
 *  @retval                 returns a STATUS_CODE enum
 *  
 *  @details Sets the backlight brightness
 *  
 *  @authors Patrick Downs
 *  
 */
u8 bootloader_ab_setbacklight(u8 brightness)
{
    u8 status;
    CommandResponse pxResponse;
    u8  cmddata[6];
    u16 cmdlength;

    if(brightness > 100)
        brightness = 100;
    
    cmddata[0] = HardwareAccess_SetBacklightBrightness;   //2byte2: opcode
    cmddata[1] = (HardwareAccess_SetBacklightBrightness >> 8);
    cmddata[2] = brightness;                              //1byte: opcodedata
    cmdlength = 3;
    
    status = commlink_basicCmdAck(CMDIF_CMD_HARDWARE,(const u8*)cmddata,
                                  cmdlength, &pxResponse, 2000);
    if(status == S_SUCCESS)
    {
        if(pxResponse.responseCode == CMDIF_ACK_OK)
        {
            status = S_SUCCESS;
        }
        else
        {
            status = S_FAIL;
        }
    }
    
    return status;
}

/**
 *  bootloader_ab_setbootmode
 *  
 *  @brief Set Appboard boot mode
 *  
 *  @param [in] newmode        New boot mode a BootloaderMode enum
 *  @retval                 returns a STATUS_CODE enum
 *  
 *  @details Sets the boot mode for the appboard. This will save a new 
 *           bootmode into it's bootsettings and then reset itself.
 *  
 *  @authors Patrick Downs
 *  
 */
u8 bootloader_ab_setbootmode(BootloaderMode newmode)
{
    u8 status;
    CommandResponse pxResponse;
    u32 mode;
    
    status = commlink_basicCmdAck(CMDIF_CMD_BOOTLOADER_SETBOOT,(const u8*)&newmode,
                                  2, &pxResponse, 1000);
    if(status == S_SUCCESS)
    {
        if(pxResponse.responseCode == CMDIF_ACK_OK)
        {
            gpio_reset_app_board(); // Just reset instead of waiting for watch dog
            
            // Check mode
            if(newmode == BootloaderMode_AB_Application)
            {
                // Can only check operating mode in bootloader
                // Mainboard needs to default commlink role as Responder
                commlink_change_mode(CommLinkInfoMode_Responder);
            }
            else
            {
                status = bootloader_ab_getoperatingmode(&mode);
                if(status == S_SUCCESS)
                {
                    switch(newmode)
                    {
                    case BootloaderMode_AB_FailSafe:
                        if(mode != AB_FAILSAFE_BOOTLOADER_OPERATING_MODE)
                        {
                            status = S_FAIL;
                        }
                        break;
                    case BootloaderMode_AB_Main:
                        if(mode != AB_MAIN_BOOTLOADER_OPERATING_MODE)
                        {
                            status = S_FAIL;
                        }
                        break;
                    case BootloaderMode_AB_Application:
                        if(mode != AB_APPLICATION_OPERATING_MODE)
                        {
                            status = S_FAIL;
                        }
                        break;
                    default:
                        status = S_FAIL;
                        break;
                    }
                }
            }
        }
        else
        {
            status = S_FAIL;
        }
    }
    
    return status;
}

/**
 *  bootloader_ab_getbootmode
 *  
 *  @brief Get App board boot mode
 *  
 *  @param [out] mode    Current boot mode a BootloaderMode enum
 *  @retval             returns a STATUS_CODE enum
 *  
 *  @details Gets the current boot mode of the app board.
 *  
 *  @authors Patrick Downs
 *  
 */
u8 bootloader_ab_getbootmode(BootloaderMode *mode)
{
    u8 status;
    CommandResponse pxResponse;
    
    status = commlink_basicCmdAck(CMDIF_CMD_BOOTLOADER_GETBOOT, NULL, 
                                  NULL, &pxResponse, 1000);
    
    if(pxResponse.datalength == 2)
    {
        memcpy(mode, pxResponse.data, pxResponse.datalength);
    }
    else
    {
        status = S_FAIL;
    }
    
    return status;
}

/**
 *  bootloader_ab_getoperatingmode
 *  
 *  @brief Get App board operating mode
 *  
 *  @param [out] mode    Current operating mode
 *  @retval             returns a STATUS_CODE enum
 *  
 *  @details Gets the current operating mode of the app board.
 *  
 *  @authors Patrick Downs
 *  
 */
u8 bootloader_ab_getoperatingmode(u32 *mode)
{
    u8 status;
    CommandResponse pxResponse;
    u16 opcode;
    
    opcode = HardwareAccess_GetOperatingMode;
    
    status = commlink_basicCmdAck(CMDIF_CMD_HARDWARE, (u8*)&opcode, 
                                  2, &pxResponse, 1000);    
    if(pxResponse.datalength == 4)
    {
        memcpy((u8*)mode, pxResponse.data, pxResponse.datalength);
    }
    else
    {
        status = S_FAIL;
    }
    
    return status;
}

/**
 *  bootloader_ab_forceboot_booten_pin
 *  
 *  @brief Forces the App Board to reset and boot into failsafe bootloader
 *  
 *  @retval             returns a STATUS_CODE enum
 *  
 *  @details This will set the BOOT_EN pin and reset the App Board. On start
 *           up the bootloader checks this pin. If it's set it will stay running
 *           a not boot into any other mode.
 *  
 *  @authors Patrick Downs
 *  
 */
u8 bootloader_ab_forceboot_booten_pin()
{
    u8  status;
    u32 opmode;
    
    // Force App Board into bootloader by settings boot enable
    gpio_set_booten(TRUE);
    gpio_reset_app_board();
    gpio_set_booten(FALSE); // Only needed on boot up. Normally keep disabled
    
    // Mainboard changes commlink role and becomes Commander when appboard in bootloader
    commlink_change_mode(CommLinkInfoMode_Commander);
    
    status = bootloader_ab_getoperatingmode(&opmode);
    if(status == S_SUCCESS)
    {
        if(opmode != AB_FAILSAFE_BOOTLOADER_OPERATING_MODE)
        {
            status = S_FAIL;
        }
    }
    
    return status;
}

/**
 *  bootloader_ab_start_bootloader
 *  
 *  @brief Forces the App Board to reset and boot into main bootloader
 *  
 *  @param [in] mode    New operating mode (IMPORTANT: when mode is BootloaderMode_AB_FailSafe,
 *             must always use boot pin method to get into failsafe bootloader. Must never use setboot.
 *
 *  @retval             returns a STATUS_CODE enum
 *  
 *  @details Set force App board to boot into main bootloader. This is useful for
 *             firmware/tune revision update to report progress to the GUI.
 *  
 *  @authors Patrick Downs
 *  
 */
u8 bootloader_ab_start_bootloader(BootloaderMode mode, bool backlighton)
{
    u8 status;
    
    commlink_init(USE_COMMLINK_BAUD);
    commlink_change_mode(CommLinkInfoMode_Commander);

    //IMPORTANT: when mode is failsafe, must always use boot pin method to get into failsafe bootloader.
    //IMPORTANT: must not use setboot to get into failsafe bootloader
    bootloader_ab_info.operating_mode = BootloaderMode_AB_FailSafe;
    
    status = bootloader_ab_forceboot_booten_pin();
    if(status == S_SUCCESS)
    {
        if(mode == BootloaderMode_AB_Main)
        {
            // Boot into main bootloader and change baud rate
            status = bootloader_ab_setbootmode(BootloaderMode_AB_Main);
            if(status == S_SUCCESS)
            {
                bootloader_ab_info.operating_mode = BootloaderMode_AB_Main;
                status = bootloader_ab_setcommlinkbaud(COMMLINK_921600_BAUD);
            }
            
            if(status != S_SUCCESS)
            {
                // Failed to go to mainboot and/or change commlink baud, go back to failsafe.
                commlink_init(USE_COMMLINK_BAUD);
                commlink_change_mode(CommLinkInfoMode_Commander);
                status = bootloader_ab_forceboot_booten_pin();
            }
        }
    }
    
    if(status == S_SUCCESS)
    {
        if(backlighton)
            bootloader_ab_setbacklight(10); // Set backlight 10%
    }
    
    return status;
}

/**
 *  bootloader_ab_resetandrun
 *  
 *  @brief      Reset and run the app board 
 *  
 *  @retval     none
 *  
 *  @details    Resets the app board and puts commlink back to Responder mode
 *              typically after an update to run App boot mode.
 *           
 *  @authors Patrick Downs
 *  
 */
void bootloader_ab_resetandrun()
{    
    // Mainboard should go back to default commlink role as Responder
    commlink_change_mode(CommLinkInfoMode_Responder);
    // Force app board reset
    gpio_reset_app_board();
}

/**
 *  __abupdater_getproperties
 *  
 *  @brief Get VehicleBoard boot mode
 *  
 *  @param [out] mode    Current boot mode a BootloaderMode enum
 *  @retval             returns a STATUS_CODE enum
 *  
 *  @details Gets the current boot mode of the vehicle board.
 *  
 *  @authors Patrick Downs
 *  
 */
u8 bootloader_ab_getproperties(const u8* properties)
{
    u8 status;
    CommandResponse pxResponse;
        
    status = commlink_basicCmdAck(CMDIF_CMD_GET_PROPERTIES, NULL, 
                                  NULL, &pxResponse, 1000);
    
    if(pxResponse.datalength == sizeof(PROPERTIES_AB_INFO))
    {
        memcpy((u8*)properties, pxResponse.data, pxResponse.datalength);
    }
    else
    {
        status = S_FAIL;
    }
    
    return status;
}

//------------------------------------------------------------------------------
// Init AppBoard in bootloader
// Input:   FirmwareHeader *fwheader
// Return:  u8  status
// Engineer: Quyen Leba
//------------------------------------------------------------------------------
u8 __abupdater_init(FirmwareHeader *fwheader)
{
    u8 status;
    CommandResponse pxResponse;
    
    if(bootloader_ab_info.control.usefsboot)
    {
        status = bootloader_ab_start_bootloader(BootloaderMode_AB_FailSafe, TRUE);
    }
    else
    {
        status = bootloader_ab_start_bootloader(BootloaderMode_AB_Main, TRUE);
    }
    
    if(status == S_SUCCESS)
    {
        status = commlink_basicCmdAck(CMDIF_CMD_BOOTLOADER_INIT, (const u8*)fwheader, 
                                      sizeof(FirmwareHeader), &pxResponse, 10000);
        if(status == S_SUCCESS)
        {
            if(pxResponse.responseCode != CMDIF_ACK_OK)
            {
                status = S_FAIL;
            }
        }
    }

    return status;
}

//------------------------------------------------------------------------------
// Program AppBoard firmware data
// Inputs:  u8  *data
//          u16 datalength (multiple of 8 bytes)
// Return:  u8  status
// Engineer: Quyen Leba
//------------------------------------------------------------------------------
u8 __abupdater_do(u8 *data, u16 datalength)
{
    u8 status;
    
    CommandResponse pxResponse;
    
    if (bootloader_ab_info.control.init == 0)
    {
        //        log_push_error_point();
        return S_ERROR;
    }
    
    status = commlink_basicCmdAck(CMDIF_CMD_BOOTLOADER_DO, (const u8*)data, 
                                  datalength, &pxResponse, 3000);
    if(status == S_SUCCESS)
    {
        if(pxResponse.responseCode != CMDIF_ACK_OK)
        {
            status = S_FAIL;
        }
    }
    
    return status;
}

//------------------------------------------------------------------------------
// Validate AppBoard firmware data
// Input:   u32 fwcrc32e
// Return:  u8  status
// Engineer: Quyen Leba
//------------------------------------------------------------------------------
u8 __abupdater_validate(u32 fwcrc32e)
{
    u8 status;
    CommandResponse pxResponse;
    
    if (bootloader_ab_info.control.init == 0)
    {
//        log_push_error_point(0x7050); // abandoned
        log_push_error_point(0x7020);
        return S_ERROR;
    }
    
    status = commlink_basicCmdAck(CMDIF_CMD_BOOTLOADER_VALIDATE, (u8*)&fwcrc32e, 
                                  4, &pxResponse, 10000);
    if(status == S_SUCCESS)
    {
        if(pxResponse.responseCode != CMDIF_ACK_OK)
        {
            status = S_FAIL;
        }
    }
    
    return S_SUCCESS;
}

//------------------------------------------------------------------------------
// Get AppBoard firmware version
// Output:  u32 *fwversion
// Return:  u8  status
// Engineer: Quyen Leba
//------------------------------------------------------------------------------
u8 bootloader_ab_getversion(u32 *fwversion)
{
    F_FILE *fptr;
    FileCryptoHeaderInfo general_header;
    u8  *bptr;
    u32 bytecount;
    u32 readcount;
    u32 index;
    u32 crc32e_calc;
    u8  buffer[512];

    //if error, force *fwversion to ZERO to accept anything, and AppBoard
    //will decide how/if to use it
    *fwversion = 0;
    fptr = genfs_user_openfile(APPBOARD_X4_FW_FILENAME,"r");
    if (!fptr)
    {
        goto bootloader_ab_getversion_done;
    }
    
    bytecount = fread((char*)buffer,1,sizeof(buffer),fptr);
    if (bytecount != sizeof(buffer))
    {
        //this file can't be less than 512b
        goto bootloader_ab_getversion_done;
    }
    crypto_blowfish_decryptblock_external_key((u8*)buffer,sizeof(buffer));
    memcpy((char*)&general_header,buffer,sizeof(FileCryptoHeaderInfo));
    if (general_header.hversion != FILECRYPTO_GENERAL_HEADER_VERSION_MAX)
    {
        goto bootloader_ab_getversion_done;
    }
    else if (general_header.ftype != FileCryptoFileType_FileBundle ||
             general_header.fsubtype != FileCryptoFileSubType_Firmware)
    {
        goto bootloader_ab_getversion_done;
    }

    bptr = buffer+4;
    crc32e_reset();
    if (general_header.hsize > sizeof(buffer))
    {
        bytecount = sizeof(buffer);
    }
    else
    {
        bytecount = general_header.hsize;
    }
    index = bytecount;
    crc32e_calc = crc32e_calculateblock(0xFFFFFFFF,(u32*)bptr,(bytecount-4)/4);

    while (index < general_header.hsize)
    {
        if ((general_header.hsize - index) > sizeof(buffer))
        {
            readcount = sizeof(buffer);
        }
        else
        {
            readcount = general_header.hsize - index;
        }
        bytecount = fread((char*)buffer,1,readcount,fptr);
        if (bytecount > 0 && (bytecount%4 == 0))
        {
            crypto_blowfish_decryptblock_external_key((u8*)buffer,bytecount);
            crc32e_calc = crc32e_calculateblock(crc32e_calc,
                                                (u32*)buffer,bytecount/4);
        }
        else
        {
            break;
        }
        index += bytecount;
    }

    if (crc32e_calc != general_header.hcrc32e)
    {
        goto bootloader_ab_getversion_done;
    }
    *fwversion = general_header.contentversion;

bootloader_ab_getversion_done:
    if (fptr)
    {
        genfs_closefile(fptr);
    }
    return S_SUCCESS;
}

