/**
  **************** (C) COPYRIGHT 2011 SCT Performance, LLC *********************
  * File Name          : indicator.h
  * Device / System    : SCT products using the "CommonCode" platform
  * Author             : Quyen Leba
  *
  * Version            : 1 
  * Date               : 05/02/2011
  * Description        : 
  *                    : 
  *
  *
  * History            : 05/02/2011 M. Davis
  *                    :   Add SCT Copyright header,
  *
  ******************************************************************************
  */

#ifndef __INDICATOR_H
#define __INDICATOR_H

#include <arch/gentype.h>

typedef enum
{
    Indicator_None,
    Indicator_SettingsError,
    Indicator_BackupSettingsError,
    Indicator_FirmwareError,
    Indicator_FilesystemError,
    Indicator_FailsafeBoot,
    Indicator_MainBoot,
    Indicator_LinkConnected,
    Indicator_LinkOpened,
    Indicator_EnterApplicationLocked,
    Indicator_EnterApplicationUnlocked,
    Indicator_UploadStock,
    Indicator_DownloadTune_Erasing,
    Indicator_DownloadTune_Programming,
    Indicator_Datalog_Setup,
    Indicator_Datalog_Active,
    Indicator_Datalog_CommLost,
    Indicator_TaskInProgress,
    Indicator_TaskSuccess,
    Indicator_TaskFail,
}Indicator;

u8 indicator_clear();
u8 indicator_restore();
u8 indicator_set(Indicator indicator);
u8 indicator_link_status();
void indicator_set_normaloperation(bool normal);
bool indicator_get_normaloperation();

#endif    //__INDICATOR_H
