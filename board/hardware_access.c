/**
  **************** (C) COPYRIGHT 2011 SCT Performance, LLC *********************
  * File Name          : hardware.c
  * Device / System    : SCT products using the "CommonCode" platform
  * Author             : Quyen Leba
  *
  * Version            : 1 
  * Date               : 05/02/2011
  * Description        : 
  *                    : 
  *
  *
  * History            : 05/02/2011 M. Davis
  *                    :   Add SCT Copyright header,
  *
  ******************************************************************************
  */

#include <string.h>
#include <board/genplatform.h>
#include <board/indicator.h>
#include <board/power.h>
#include <common/statuscode.h>
#include <common/obd2.h>
#include "hardware_access.h"

//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
u8 hardware_access_init()
{
    return S_SUCCESS;
}

//------------------------------------------------------------------------------
// Check if opcode is supported
// Inputs:  HardwareAccess opcode
// Return   u8  status
// Engineer: Quyen Leba
//------------------------------------------------------------------------------
u8 hardware_access_checksupport(HardwareAccess opcode)
{
    switch(opcode)
    {
    case HardwareAccess_Unpair:
    case HardwareAccess_GetMacAddress:
    case HardwareAccess_SetMacAddress:
    case HardwareAccess_GetFriendlyName:
    case HardwareAccess_SetFriendlyName:
    case HardwareAccess_GetSensorData:
    case HardwareAccess_Reset:
    case HardwareAccess_SetExt5VConfig:
    case HardwareAccess_GetExt5VConfig:
    case HardwareAccess_SetGPIO1Config:
    case HardwareAccess_GetGPIO1Config:
    case HardwareAccess_GetGPIO2Input:
    case HardwareAccess_CheckIgnitionKey:
    case HardwareAccess_SupportIgnitionKey:
    case HardwareAccess_SetCommLinkSpeed:
    case HardwareAccess_CommLinkSpeedSyncReq:
    case HardwareAccess_CommLinkSpeedSyncCfm:
    case HardwareAccess_Scan:
    case HardwareAccess_Connect:
    case HardwareAccess_Disconect:
    case HardwareAccess_SmartConfig:
        break;
    default:
        return S_NOTSUPPORT;
    }
    return S_SUCCESS;
}

//------------------------------------------------------------------------------
// Access to hardware
// Inputs:  HardwareAccess opcode
//          u8  *opcodedata
// Outputs: u8  *returndata (max 256 bytes)
//          u32 *returndatalength
// Return   u8  status
// Engineer: Quyen Leba
//------------------------------------------------------------------------------
u8 hardware_access(HardwareAccess opcode, u8 *opcodedata,
                   u8 *returndata, u32 *returndatalength)
{
    u8  status;
    bool ok;
    
    ok = FALSE;
    status = S_FAIL;
    switch(opcode)
    {
    //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    case HardwareAccess_Reset:
        status = hardware_reset(*(u16*)opcodedata);
        break;
    //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    case HardwareAccess_Unpair:
#if __COMMLINK_HAL_BLUETOOTH__
        status = bluetooth_unpair();
#else
        status = S_NOTSUPPORT;
#endif
        break;
    //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    case HardwareAccess_GetMacAddress:
        *returndatalength = 0;
#if __COMMLINK_HAL_BLUETOOTH__
        status = bluetooth_getmacaddress(returndata);
#elif USE_WIFI
        status = wifi_hal_getmacaddress(returndata);
#else
        status = S_NOTSUPPORT;
#endif
        if (status == S_SUCCESS)
        {
            *returndatalength = strlen((char*)returndata)+1;
        }
        break;
    //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    case HardwareAccess_SetMacAddress:
        status = S_NOTSUPPORT;
        break;
    //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    case HardwareAccess_GetFriendlyName:
#if __COMMLINK_HAL_BLUETOOTH__
        status = bluetooth_getfriendlyname(returndata);
        *returndatalength = 0;
        if (status == S_SUCCESS)
        {
            *returndatalength = strlen((char*)returndata)+1;
        }
#else
        status = S_NOTSUPPORT;
#endif
        break;
    //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    case HardwareAccess_SetFriendlyName:
#if __COMMLINK_HAL_BLUETOOTH__
        status = bluetooth_setfriendlyname(opcodedata);
#else
        status = S_NOTSUPPORT;
#endif
        break;
    //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    case HardwareAccess_GetSensorData:
        status = hardware_get_sensordata(*(u32*)opcodedata,
                                         returndata,returndatalength);
        break;
    //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    case HardwareAccess_FirmwareVersionStr:
        status = hardware_firmwareversionstr((HardwareSource)(*(u16*)opcodedata),
                                             returndata,returndatalength);
        break;
    //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    case HardwareAccess_LEDSet:
        led_setoperationmode(FALSE);
        led_setState((LED_State)(*(u16*)opcodedata));
        status = S_SUCCESS;
        break;
    //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    case HardwareAccess_LEDRelease:
        led_setoperationmode(TRUE);
        indicator_restore();
        status = S_SUCCESS;
        break;
    //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    case HardwareAccess_SetExt5VConfig:
        if(*opcodedata == EXT5VCONFIG_ON)
            V5VExt_Ctrl(ENABLE);
        else
            V5VExt_Ctrl(DISABLE);
        status = S_SUCCESS;
        break;
    //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    case HardwareAccess_SetGPIO1Config:
        if(*opcodedata == GPIO1CONFIG_ON)
            adc_config_gpio1_out(TRUE);
        else
            adc_config_gpio1_out(FALSE);
        status = S_SUCCESS;
        break;
    //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    case HardwareAccess_GetGPIO2Input:
        returndata[0] = adc_peek_gpio2_in(&ok);
        *returndatalength = 1;
        if(ok)
            status = S_SUCCESS;
        break;
    //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    case HardwareAccess_CheckIgnitionKey:
#if SUPPORT_IGN_KEY_DETECTION
        status = obd2_checkkeyon(CommType_Unknown, FALSE);
        if (status == S_SUCCESS)
        {
            //key on
            returndata[0] = S_SUCCESS;
        }
        else
        {
            //key off
            returndata[0] = S_NOCOMMUNICATION;
        }
#else
        returndata[0] = S_NOTSUPPORT;
#endif
        *returndatalength = 1;
        status = S_SUCCESS;
        break;
    //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    case HardwareAccess_SupportIgnitionKey:
#if SUPPORT_IGN_KEY_DETECTION
        status = S_SUCCESS;
#else
        status = S_FAIL;
#endif
        break;
    //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    case HardwareAccess_SetCommLinkSpeed:
        status = commlink_init((CommLinkBaudrate)(*(u16*)opcodedata));
        break;
    //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    case HardwareAccess_CommLinkSpeedSyncReq:
        *returndatalength = 0;
        status = commlink_speedsyncrequest((u32*)returndata);
        if (status == S_SUCCESS)
        {
            *returndatalength = 4;
        }
        break;
    //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    case HardwareAccess_CommLinkSpeedSyncCfm:
        status = commlink_speedsyncconfirm(*(u32*)opcodedata);
        break;
    //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    case HardwareAccess_Scan:
        //opcodedata: [1:WIFI_HAL_SCAN_OPCODE]
#if (USE_WIFI)
        //CC3000 takes at least 10 seconds (default) to complete scan
        *returndatalength = sizeof(WIFI_HAL_AP_INFO);
        switch(*opcodedata)
        {
        case WIFI_HAL_SCAN_OPCODE_START:
            status = wifi_hal_scan_start((WIFI_HAL_AP_INFO *)returndata);
            break;
        case WIFI_HAL_SCAN_OPCODE_GETSSID:
            status = wifi_hal_scan_next((WIFI_HAL_AP_INFO *)returndata);
            break;
        case WIFI_HAL_SCAN_OPCODE_STOP:
            *returndatalength = 0;
            status = wifi_hal_scan_stop();
            break;
        default:
            status = S_INPUT;
            break;
        }
        if (status != S_SUCCESS)
        {
            *returndatalength = 0;
        }
#else
        status = S_NOTSUPPORT;
#endif
        break;
    //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    case HardwareAccess_Connect:
        //opcodedata: [WIFI_HAL_AP_INFO]
#if (USE_WIFI)
        status = wifi_hal_connect((WIFI_HAL_AP_INFO *)opcodedata);
#else
        status = S_NOTSUPPORT;
#endif
        break;
    //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    case HardwareAccess_Disconect:
#if (USE_WIFI)
        status = wifi_hal_disconnect();
#else
        status = S_NOTSUPPORT;
#endif
        break;
    //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    case HardwareAccess_SmartConfig:
#if (USE_CC3000_WIFI)
        status = cc3000_smartConfig((CC3000_SMARTCONFIG_OPCODE)*opcodedata);
#else
        status = S_NOTSUPPORT;
#endif
        break;
    //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++        
    default:
        status = S_NOTSUPPORT;
        break;
    }
    return status;
}



//------------------------------------------------------------------------------
// Get sensor data
// Inputs:  u32 sensors (each bit HIGH indicates requesting sensor)
// Outputs: u8  *returndata (max 4*32 -> 128 bytes)
//          u32 *returndatalength
// Return   u8  status
// Engineer: Quyen Leba
// IMPORTANT: must process sensors in order; sensor bit0 always first
//------------------------------------------------------------------------------
u8 hardware_get_sensordata(u32 sensors, u8 *returndata, u32 *returndatalength)
{
    u32 dataindex;
    float fval;
    bool usecompactdata;
    u8  status;
    u8  returnstatus;

    memset((char*)returndata,0,4*32);
    *returndatalength = 128;
    usecompactdata = FALSE;
    dataindex = 0;
    returnstatus = S_SUCCESS;
    //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    if (sensors & (HardwareSensor_X_AXIS | HardwareSensor_Y_AXIS | HardwareSensor_Z_AXIS))
    {
        u16 x,y,z;
        status = accelerometer_get_xyz(&x,&y,&z,TRUE);
        if (status == S_SUCCESS)
        {

            if (sensors & HardwareSensor_X_AXIS)
            {
                fval = (float)x;
                memcpy((char*)returndata,(char*)&fval,4);
                dataindex += 4;
            }

            if (sensors & HardwareSensor_Y_AXIS)
            {
                if (!usecompactdata)
                {
                    dataindex = HardwareSensorBit_Y_Axis * 4;
                }
                fval = (float)y;
                memcpy((char*)&returndata[dataindex],(char*)&fval,4);
                dataindex += 4;
            }

            if (sensors & HardwareSensor_Z_AXIS)
            {
                if (!usecompactdata)
                {
                    dataindex = HardwareSensorBit_Z_Axis * 4;
                }
                fval = (float)z;
                memcpy((char*)&returndata[dataindex],(char*)&fval,4);
                dataindex += 4;
            }
        }
        else
        {
            returnstatus = S_FAIL;
        }
    }
    //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    if (sensors & (HardwareSensor_3 | HardwareSensor_4 | HardwareSensor_5))
    {
        if (sensors & HardwareSensorBit_3)
        {
            if (!usecompactdata)
            {
                dataindex = HardwareSensorBit_3 * 4;
            }
            fval = 0;
            memcpy((char*)&returndata[dataindex],(char*)&fval,4);
            dataindex += 4;
        }
        if (sensors & HardwareSensorBit_4)
        {
            if (!usecompactdata)
            {
                dataindex = HardwareSensorBit_4 * 4;
            }
            fval = 0;
            memcpy((char*)&returndata[dataindex],(char*)&fval,4);
            dataindex += 4;
        }
        if (sensors & HardwareSensorBit_5)
        {
            if (!usecompactdata)
            {
                dataindex = HardwareSensorBit_5 * 4;
            }
            fval = 0;
            memcpy((char*)&returndata[dataindex],(char*)&fval,4);
            dataindex += 4;
        }
    }
    //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    if (sensors & HardwareSensor_Temp)
    {
        status = adc_read(ADC_TEMPSENSOR,&fval);
        if (status != S_SUCCESS)
        {
            fval = 0;
            returnstatus = S_FAIL;
        }
        if (!usecompactdata)
        {
            dataindex = HardwareSensorBit_Temp * 4;
        }
        memcpy((char*)&returndata[dataindex],(char*)&fval,4);
        dataindex += 4;
    }
    //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    if (sensors & HardwareSensor_Aux)
    {
        status = adc_read(ADC_VREFINT,&fval);
        if (status != S_SUCCESS)
        {
            fval = 0;
            returnstatus = S_FAIL;
        }
        if (!usecompactdata)
        {
            dataindex = HardwareSensorBit_Aux * 4;
        }
        memcpy((char*)&returndata[dataindex],(char*)&fval,4);
        dataindex += 4;
    }
    //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    if (sensors & HardwareSensor_Analog_1)
    {
        status = adc_read(ADC_AIN1,&fval);
        if (status != S_SUCCESS)
        {
            fval = 0;
            returnstatus = S_FAIL;
        }
        if (!usecompactdata)
        {
            dataindex = HardwareSensorBit_Analog_1 * 4;
        }
        memcpy((char*)&returndata[dataindex],(char*)&fval,4);
        dataindex += 4;
    }
    //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    if (sensors & HardwareSensor_Analog_2)
    {
        status = adc_read(ADC_AIN2,&fval);
        if (status != S_SUCCESS)
        {
            fval = 0;
            returnstatus = S_FAIL;
        }
        if (!usecompactdata)
        {
            dataindex = HardwareSensorBit_Analog_2 * 4;
        }
        memcpy((char*)&returndata[dataindex],(char*)&fval,4);
        dataindex += 4;
    }
    //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    if (sensors & HardwareSensor_Analog_3)
    {
        status = adc_read(ADC_AIN3,&fval);
        if (status != S_SUCCESS)
        {
            fval = 0;
            returnstatus = S_FAIL;
        }
        if (!usecompactdata)
        {
            dataindex = HardwareSensorBit_Analog_3 * 4;
        }
        memcpy((char*)&returndata[dataindex],(char*)&fval,4);
        dataindex += 4;
    }
    //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    if (sensors & HardwareSensor_Analog_4)
    {
        status = adc_read(ADC_AIN4,&fval);
        if (status != S_SUCCESS)
        {
            fval = 0;
            returnstatus = S_FAIL;
        }
        if (!usecompactdata)
        {
            dataindex = HardwareSensorBit_Analog_4 * 4;
        }
        memcpy((char*)&returndata[dataindex],(char*)&fval,4);
        dataindex += 4;
    }
    //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    if (usecompactdata)
    {
        *returndatalength = dataindex;
    }
    
    return returnstatus;
}

//------------------------------------------------------------------------------
// Get hardware specific device info
// Outputs: u8  *name (max 256 bytes)
//          u32 *namelength
// Return   u8  status
// Engineer: Quyen Leba
//------------------------------------------------------------------------------
u8 hardware_get_device_name(u8 *name, u32 *namelength)
{
    u8  buffer[128];
    u8  status;

    status = commlink_get_device_name(buffer);
    if (status == S_SUCCESS)
    {
        strcpy((char*)name,(char*)buffer);
        *namelength = strlen((char*)name);
    }
    else
    {
        *namelength = 0;
        name[0] = NULL;
    }
    return status;
}

//------------------------------------------------------------------------------
// Get hardware specific device info
// Outputs: u8  *info (max 256 bytes)
//          u32 *infolength
// Return   u8  status
// Engineer: Quyen Leba
//------------------------------------------------------------------------------
u8 hardware_get_hardware_specific_device_info(u8 *info, u32 *infolength)
{
//    u8  buffer[128];
//    u32 bufferlength;
//    u8  status;
//
//    strcpy((char*)info,"<HEADER>Bluetooth:</HEADER>");
//    status = hardware_access(HardwareAccess_GetMacAddress,NULL,
//                             buffer,&bufferlength);
//    if (status == S_SUCCESS)
//    {
//        
//        strcat((char*)info,(char*)buffer);
//    }
//    else
//    {
//        strcat((char*)info,"N/A");
//    }
//
//    *infolength = strlen((char*)info);
//    
//    return S_SUCCESS;
    *infolength = 0;
    return S_NOTREQUIRED;
}

//------------------------------------------------------------------------------
// Do reset
// Input:   u16 flags
// Return   u8  status
// Engineer: Quyen Leba
//------------------------------------------------------------------------------
u8 hardware_reset(u16 flags)
{
    if (flags & HardwareResetVehicleBoard)
    {
        gpio_reset_vehicle_board();
        // Need to go reset MBLIF to interrupt mode on reset
        spi_master_init();
        mblif_init();
    }

    if (flags & HardwareResetAppBoardBoard)
    {
        gpio_reset_app_board();
    }

    if (flags & HardwareResetMainBoard)
    {
        watchdog_set();
    }

    return S_SUCCESS;
}

//------------------------------------------------------------------------------
// Get firmware version
// Input:   u16 source
// Return   u8  status
// Engineer: Quyen Leba
//------------------------------------------------------------------------------
u8 hardware_firmwareversionstr(HardwareSource hwsource,
                               u8 *fwver, u32 *fwverlength)
{
    u8  status;

    status = S_FAIL;
    switch(hwsource)
    {
    case HardwareSource_MainBoard:
        status = S_NOTSUPPORT;
        break;
    case HardwareSource_VehicleBoard:
        status = S_NOTSUPPORT;
        break;
    case HardwareSource_AppBoard:
        status = S_NOTSUPPORT;
        break;
    default:
        return S_NOTSUPPORT;
    }

    return status;
}
