/**
  **************** (C) COPYRIGHT 2011 SCT Performance, LLC *********************
  * File Name          : mblexec_bootloader.h
  * Device / System    : SCT products using the "CommonCode" platform
  * Author             : Quyen Leba
  *
  * Version            : 1 
  * Date               : 05/02/2011
  * Description        : 
  *                    : 
  *
  *
  * History            : 05/02/2011 M. Davis
  *                    :   Add SCT Copyright header,
  *
  ******************************************************************************
  */

#ifndef __MBLEXEC_BOOTLOADER_H
#define __MBLEXEC_BOOTLOADER_H

#include <arch/gentype.h>
#include <board/bootloader.h>

u8 mblexec_bootloader_setboot(BootloaderMode bootmode);
u8 mblexec_bootloader_init(FirmwareHeader *firmware_header);
u8 mblexec_bootloader_do(u8 *data, u32 datalength);
u8 mblexec_bootloader_validate();

#endif    //__MBLEXEC_BOOTLOADER_H
