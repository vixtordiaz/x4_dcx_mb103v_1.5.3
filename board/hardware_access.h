/**
  **************** (C) COPYRIGHT 2011 SCT Performance, LLC *********************
  * File Name          : hardware.h
  * Device / System    : SCT products using the "CommonCode" platform
  * Author             : Quyen Leba
  *
  * Version            : 1 
  * Date               : 05/02/2011
  * Description        : 
  *                    : 
  *
  *
  * History            : 05/02/2011 M. Davis
  *                    :   Add SCT Copyright header,
  *
  ******************************************************************************
  */

#ifndef __HARDWARE_ACCESS_H
#define __HARDWARE_ACCESS_H

#include <arch/gentype.h>

typedef enum
{
    HardwareAccess_Reset                    = 0x0005,
    HardwareAccess_FirmwareVersionStr       = 0x0006,
    HardwareAccess_GetBoardRevision         = 0x0007,
    HardwareAccess_GetOperatingMode         = 0x0008,
    HardwareAccess_Unpair                   = 0x0010,
    HardwareAccess_GetMacAddress            = 0x0011,
    HardwareAccess_SetMacAddress            = 0x0012,
    HardwareAccess_GetFriendlyName          = 0x0013,
    HardwareAccess_SetFriendlyName          = 0x0014,
    HardwareAccess_LEDSet                   = 0x0015,
    HardwareAccess_LEDRelease               = 0x0016,
    
    HardwareAccess_Scan                     = 0x0017,
    HardwareAccess_Connect                  = 0x0018,
    HardwareAccess_Disconect                = 0x0019,
    HardwareAccess_SmartConfig              = 0x001A,
    
    HardwareAccess_GetSensorData            = 0x0020,
    HardwareAccess_SetExt5VConfig           = 0x0030,
    HardwareAccess_GetExt5VConfig           = 0x0031,
    HardwareAccess_SetGPIO1Config           = 0x0032,
    HardwareAccess_GetGPIO1Config           = 0x0033,
    HardwareAccess_GetGPIO2Input            = 0x0034,
    HardwareAccess_SetBacklightBrightness   = 0x0040,
    HardwareAccess_CheckIgnitionKey         = 0x0060,
    HardwareAccess_SupportIgnitionKey       = 0x0061,
    HardwareAccess_SetCommLinkSpeed         = 0x0065,
    HardwareAccess_CommLinkSpeedSyncReq     = 0x0066,
    HardwareAccess_CommLinkSpeedSyncCfm     = 0x0067,
}HardwareAccess;

enum
{
    HardwareSensorBit_X_Axis            = 0,
    HardwareSensorBit_Y_Axis            = 1,
    HardwareSensorBit_Z_Axis            = 2,
    HardwareSensorBit_3                 = 3,
    HardwareSensorBit_4                 = 4,
    HardwareSensorBit_5                 = 5,
    HardwareSensorBit_Temp              = 6,
    HardwareSensorBit_Aux               = 7,
    HardwareSensorBit_Analog_1          = 8,
    HardwareSensorBit_Analog_2          = 9,
    HardwareSensorBit_Analog_3          = 10,
    HardwareSensorBit_Analog_4          = 11,
};

enum
{
    HardwareSensor_X_AXIS               = (u32)1<<HardwareSensorBit_X_Axis,
    HardwareSensor_Y_AXIS               = (u32)1<<HardwareSensorBit_Y_Axis,
    HardwareSensor_Z_AXIS               = (u32)1<<HardwareSensorBit_Z_Axis,
    HardwareSensor_3                    = (u32)1<<HardwareSensorBit_3,
    HardwareSensor_4                    = (u32)1<<HardwareSensorBit_4,
    HardwareSensor_5                    = (u32)1<<HardwareSensorBit_5,
    HardwareSensor_Temp                 = (u32)1<<HardwareSensorBit_Temp,
    HardwareSensor_Aux                  = (u32)1<<HardwareSensorBit_Aux,
    HardwareSensor_Analog_1             = (u32)1<<HardwareSensorBit_Analog_1,
    HardwareSensor_Analog_2             = (u32)1<<HardwareSensorBit_Analog_2,
    HardwareSensor_Analog_3             = (u32)1<<HardwareSensorBit_Analog_3,
    HardwareSensor_Analog_4             = (u32)1<<HardwareSensorBit_Analog_4,
};

enum
{
    HardwareResetMainBoard              = (1u<<0),
    HardwareResetVehicleBoard           = (1u<<1),
    HardwareResetAppBoardBoard          = (1u<<2),
};

typedef enum
{
    HardwareSource_MainBoard            = 1,
    HardwareSource_VehicleBoard         = 2,
    HardwareSource_AppBoard             = 3,
}HardwareSource;

u8 hardware_access_init();
u8 hardware_access_checksupport(HardwareAccess opcode);
u8 hardware_access(HardwareAccess opcode, u8 *opcodedata,
                   u8 *returndata, u32 *returndatalength);
u8 hardware_get_sensordata(u32 sensors, u8 *returndata, u32 *returndatalength);
u8 hardware_get_device_name(u8 *name, u32 *namelength);
u8 hardware_get_hardware_specific_device_info(u8 *info, u32 *infolength);
u8 hardware_reset(u16 flags);
u8 hardware_firmwareversionstr(HardwareSource source,
                               u8 *fwver, u32 *fwverlength);

#endif    //__HARDWARE_ACCESS_H
