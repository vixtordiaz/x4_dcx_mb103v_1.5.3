/**
  **************** (C) COPYRIGHT 2011 SCT Performance, LLC *********************
  * File Name          : bootloader.h
  * Device / System    : SCT products using the "CommonCode" platform
  * Author             : Quyen Leba
  *
  * Version            : 1 
  * Date               : 05/02/2011
  * Description        : 
  *                    : 
  *
  *
  * History            : 05/02/2011 M. Davis
  *                    :   Add SCT Copyright header,
  *
  ******************************************************************************
  */

#ifndef __BOOTLOADER_H
#define __BOOTLOADER_H

#include <arch/gentype.h>

//NOTE:
//UpdateFromFile firmware filenames need to conform to short 8.3 filenames
//MainBoot has simple filesystem which does not support long filenames
#define UPDATEFROMFILE_FILE_COUNT   6
#define MB_MAINBOOT_FILENAME        GENFS_USER_FOLDER_PATH"mbmboot.fwu"
#define MB_APP_FILENAME             GENFS_USER_FOLDER_PATH"mbapp.fwu"
#define VB_MAINBOOT_FILENAME        GENFS_USER_FOLDER_PATH"vbmboot.fwu"
#define VB_APP_FILENAME             GENFS_USER_FOLDER_PATH"vbapp.fwu"
#define AB_MAINBOOT_FILENAME        GENFS_USER_FOLDER_PATH"abmboot.fwu"
#define AB_APP_FILENAME             GENFS_USER_FOLDER_PATH"abapp.fwu"

typedef enum
{
    //MainBoard
    BootloaderMode_MB_FailSafe      = 0xA538,
    BootloaderMode_MB_Main          = 0x5593,
    BootloaderMode_MB_Application   = 0xAA72,
    //VehicleBoard
    BootloaderMode_VB_FailSafe      = 0xC8A7,
    BootloaderMode_VB_Main          = 0x3455,
    BootloaderMode_VB_Application   = 0x16AA,
    //X4 App Board
    BootloaderMode_AB_FailSafe      = 0x5AA5,
    BootloaderMode_AB_Main          = 0x4133,
    BootloaderMode_AB_Application   = 0x2845,
}BootloaderMode;

typedef struct
{
    //as defined in FileCrypto
    //general header (24 bytes)
    u32 hcrc;
    u16 hversion;
    u16 ftype;
    u16 hsize;
    u8  fsubtype;
    u8  reserved0;
    u32 firmware_size;
    u32 firmware_crc32e;
    u32 reserved1;
    //specific header ()
    u32 firmware_version;
    u32 hardware;   //firmware_signature
    u32 device_market;
    u32 device_type;
    u32 firmware_flags;     //new
    u32 signature;          //new
    u8  serialnumber[16];
}FirmwareHeader;

typedef struct
{
    u32 firmware_flags;
    u32 firmware_version;
    u32 firmware_size;
    u32 firmware_crc32e;
    u32 bytecount;
    u16 bytecheck;
    u16 current_sector;
    u32 current_address;
    u32 current_fwsize;
    u32 current_version;
    struct
    {
        u8  validated   : 1;
        u8  init        : 1;
        u8  reserved    : 6;
    }status;
}BootloaderInfo;

//FirmwareFlags
//[bit2..0]
#define FIRMWARE_FLAGS_APPLICATION                  (1<<0)
#define FIRMWARE_FLAGS_MAIN_BOOTLOADER              (2<<0)
//[bit3] - only applicable if version/crc32e are the same
#define FIRMWARE_FLAGS_FORCE_UPDATE                 (1<<3)
//[bit4] - Update With Failsafe Bootloader
#define FIRMWARE_FLAGS_UPDATE_WITH_FAILSAFE         (1<<4)
//[bit31..]: reserved

void bootloader_init();
void bootloader_jump_to_failsafe_bootloader();
void bootloader_jump_to_main_bootloader();
void bootloader_jump_to_application();
u8 bootloader_setboot(BootloaderMode bootmode);
u8 bootloader_setup_session(u8 *firmware_header, u16 *sector_size, bool updatebyfile);
u8 bootloader_validate_fwheader(FirmwareHeader fwheader);
u8 bootloader_write_flash(u8 *data, u32 datalength);
u8 bootloader_validate();
u8 bootloader_clearengineeringsignature();
u8 bootloader_force_application_validation();
u8 bootloader_force_mainboot_validation();
u8 bootloader_updatebyfile();
u8 bootloader_validate_file(u8 *filename, bool *usefailsafeboot);

u8 bootloader_updatefromfile_init_ab_boot();
u8 bootloader_updatefromfile_init_ab_app();
u8 bootloader_updatefromfile_init_vb_boot();
u8 bootloader_updatefromfile_init_vb_app();
u8 bootloader_updatefromfile_init_mb_boot();
u8 bootloader_updatefromfile_init_mb_app();

void bootloader_updatefromfile_start();
u8 bootloader_updatefromfile_checkifdone_mb();
void bootloader_updatefromfile_checkifdone();
void bootloader_wifipatch_checkforupdate();
u8 bootloader_comparefwversion_file(u8 *filename);

#endif    //__BOOTLOADER_H
