/**
  **************** (C) COPYRIGHT 2011 SCT Performance, LLC *********************
  * File Name          : mblif.h
  * Device / System    : SCT products using the "CommonCode" platform
  * Author             : Quyen Leba
  *
  * Version            : 1 
  * Date               : 05/02/2011
  * Description        : 
  *                    : 
  *
  *
  * History            : 05/02/2011 M. Davis
  *                    :   Add SCT Copyright header,
  *
  ******************************************************************************
  */

#ifndef __MBLIF_DMA_H
#define __MBLIF_DMA_H

#include <arch/gentype.h>

u8 mblif_dma_init(MBLIF_MODE mode);
u8 mblif_dma_link(u8 cmd, u16 opcode, u8 *data, u16 *datalength);

#endif  //__MBLIF_H
