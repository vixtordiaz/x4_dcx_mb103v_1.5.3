/**
  **************** (C) COPYRIGHT 2011 SCT Performance, LLC *********************
  * File Name          : board_ken.h
  * Device / System    : SCT products using the "CommonCode" platform
  * Author             : Quyen Leba
  *
  * Version            : 1 
  * Date               : 05/02/2011
  * Description        : 
  *                    : 
  *
  *
  * History            : 05/02/2011 M. Davis
  *                    :   Add SCT Copyright header,
  *
  ******************************************************************************
  */

#include <common/statuscode.h>
#include "board_ken.h"

//------------------------------------------------------------------------------
// Reset to factory default
// Return:  u8  status
// Engineer: Quyen Leba
//------------------------------------------------------------------------------
u8 board_set_factory_default()
{
    return S_SUCCESS;
}
