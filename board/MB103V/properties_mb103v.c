/**
  **************** (C) COPYRIGHT 2011 SCT Performance, LLC *********************
  * File Name          : properties.c
  * Device / System    : SCT products using the "CommonCode" platform
  * Author             : Quyen Leba
  *
  * Version            : 1 
  * Date               : 05/02/2011
  * Description        : 
  *                    : 
  *
  *
  * History            : 05/02/2011 M. Davis
  *                    :   Add SCT Copyright header,
  *
  ******************************************************************************
  */

#include <string.h>
#include <device_version.h>
#include <board/genplatform.h>
#include <common/version.h>
#include <common/bootsettings.h>
#include <common/crypto_messageblock.h>
#include <common/statuscode.h>
#include "flash.h"
#include "gpio.h"
#include "properties_mb103v.h"

typedef enum
{
    MediaType_NAND2k,
    MediaType_SD,
    MediaType_SDHC,
    MediaType_MMC,
    MediaType_Unknown,
}MediaType;

extern funcptr_mblif_link* mblif_link;              //from mblif.c
PROPERTIES_MB_INFO properties_info =
{
    .operating_mode = APPLICATION_OPERATING_MODE,
    .failsafeboot_version = 0,
    .mainboot_version = 0,
    .code_sector_size = FLASH_SECTOR_SIZE,
    .secboot_startsector = FLASH_MAIN_BOOT_STARTING_SECTOR,
    .secboot_maxcodesize = FLASH_MAIN_BOOT_SIZE,
    .app_startsector = FLASH_APPLICATION_STARTING_SECTOR,
    .app_maxcodesize = FLASH_APPLICATION_SIZE,

    .media_type = MediaType_Unknown,
    .media_size = 0,
    .media_sector_size = 0,

    .reserved2 = {0,0},
    .reserved3 = {0,0,0,0,0,0,0,0},
};

//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
void properties_mb_init()
{
    properties_info.board_rev = gpio_get_board_rev();
    BOOTSETTINGS_GetAppVersion() = firmware_tag.version;
    properties_info.app_version = BOOTSETTINGS_GetAppVersion();
    properties_info.app_version %= 1000000;
    properties_info.app_build = APPLICATION_VERSION_BUILD;
    properties_info.app_signature = BOOTSETTINGS_GetAppSignature();

    properties_info.mainboot_version = BOOTSETTINGS_GetMainBootVersion();
    properties_info.mainboot_version %= 1000000;
    properties_info.failsafeboot_version = BOOTSETTINGS_GetFailsafeBootVersion();
    properties_info.failsafeboot_version %= 1000000;
}

//------------------------------------------------------------------------------
// Get MainBoard properties
// Outputs: u8  *info (vb properties; must be able to store 48 bytes)
//          u32 *info_length
// Return:  u8  status
// Engineer: Quyen Leba
//------------------------------------------------------------------------------
u8 properties_mb_getinfo(u8 *info, u32 *info_length)
{
    if (!info || !info_length)
    {
        return S_INPUT;
    }
    memcpy((char*)info,(char*)&properties_info,sizeof(properties_info));
    *info_length = sizeof(properties_info);
    return S_SUCCESS;
}

