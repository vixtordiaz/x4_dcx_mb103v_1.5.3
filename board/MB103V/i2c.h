/**
  **************** (C) COPYRIGHT 2011 SCT Performance, LLC *********************
  * File Name          : i2c.h
  * Device / System    : SCT products using the "CommonCode" platform
  * Author             : Quyen Leba
  *
  * Version            : 1 
  * Date               : 05/02/2011
  * Description        : 
  *                    : 
  *
  *
  * History            : 05/02/2011 M. Davis
  *                    :   Add SCT Copyright header,
  *
  ******************************************************************************
  */

#ifndef __I2C_H
#define __I2C_H

u8 i2c_init();
u8 i2c_writebytes(u8 slaveaddr, u8 writeaddr, u8 *data, u32 datalength);
u8 i2c_readbytes(u8 slaveaddr, u8 readaddr, u8 *data, u32 datalength);

#endif    //__I2C_H
