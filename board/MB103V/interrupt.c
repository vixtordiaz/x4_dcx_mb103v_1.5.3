/**
  **************** (C) COPYRIGHT 2011 SCT Performance, LLC *********************
  * File Name          : interrupt.c
  * Device / System    : SCT products using the "CommonCode" platform
  * Author             : Quyen Leba
  *
  * Version            : 1 
  * Date               : 05/02/2011
  * Description        : 
  *                    : 
  *
  *
  * History            : 05/02/2011 M. Davis
  *                    :   Add SCT Copyright header,
  *
  ******************************************************************************
  */

#include <misc.h>
#include <board/genplatform.h>
#include <board/interrupt.h>

//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
void interrupt_init(FunctionalState state)
{
    interrupt_ctrl(state);
    interrupt_sw_init();
}

//------------------------------------------------------------------------------
// Function Name  : interrupt_config
// Description    : Configures the used IRQ Channels and sets their priority.
// Input          : FunctionalState intrCtrl - DISABLE, ENABLE
// Output         : None
// Return         : None
//------------------------------------------------------------------------------
void interrupt_ctrl(FunctionalState intrCtrl)
{
    NVIC_InitTypeDef NVIC_InitStructure;

    NVIC_SetVectorTable(NVIC_VectTab_FLASH, FLASH_APPLICATION_ADDRESS);

    //Group3: PreemptionPriority (0-7), SubPriority (0-1)
    NVIC_PriorityGroupConfig(NVIC_PriorityGroup_3);

    //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    // Interrupt priorities
    // Systick:     0-0
    // UART2:       0-1 (Commlink)
    // EXTI12:      1-0 (VB Busy Pin Stobe / Bluetooth Link Status)
    // TIM1:        1-1 (App & Board timeout timer)
    // DMA1:        2-0 (Wifi SPI DMA TX)
    // EXT4:        2-1 (WiFi Interrupt Pin)
    // USB:         3-0 & 3-1
    // UART3:       4-0 (Debug UART)
    // EXTI1:       4-1 (SW Intr)
    // EXTI9:       5-0 (bluetooth session status)
    // TIM7:        5-1 (task timer)
    // EXTI0:       6-0 (Button)
    // TIM2:        6-1 (Commlink Command timeout)
    // EXTI2:       7-0 (SW Intr)
    // EXTI3:       7-1 (SW Intr)
    //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

     //Configure the SysTick handler priority
    NVIC_SystemHandlerPriorityConfig(SystemHandler_SysTick, 0, 0);
    
    //Enable the USART2 Interrupt - Commlink
    NVIC_InitStructure.NVIC_IRQChannel = USART2_IRQn;
    NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 0;
    NVIC_InitStructure.NVIC_IRQChannelSubPriority = 1;
    NVIC_InitStructure.NVIC_IRQChannelCmd = intrCtrl;
    NVIC_Init(&NVIC_InitStructure);

#if !CONFIG_FOR_TEST   
    // Enable EXTI12
    NVIC_InitStructure.NVIC_IRQChannel = EXTI15_10_IRQn;
    NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 1;
    NVIC_InitStructure.NVIC_IRQChannelSubPriority = 0;
    NVIC_InitStructure.NVIC_IRQChannelCmd = intrCtrl;
    NVIC_Init(&NVIC_InitStructure);
#endif
    
    // Enable the TIM1 gloabal Interrupt
    NVIC_InitStructure.NVIC_IRQChannel = TIM1_UP_IRQn;
    NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 1;
    NVIC_InitStructure.NVIC_IRQChannelSubPriority = 1;
    NVIC_InitStructure.NVIC_IRQChannelCmd = intrCtrl;
    NVIC_Init(&NVIC_InitStructure);
    
    // Enable SPI DMA TX Channel interrupt - WIFI SPI
    NVIC_InitStructure.NVIC_IRQChannel = DMA1_Channel3_IRQn;
    NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 2;
    NVIC_InitStructure.NVIC_IRQChannelSubPriority = 0;
    NVIC_InitStructure.NVIC_IRQChannelCmd = intrCtrl;
    NVIC_Init(&NVIC_InitStructure);
    
    // Enable External Interrupt 4 - WIFI IRQ interrupt pin
    NVIC_InitStructure.NVIC_IRQChannel = EXTI4_IRQn;
    NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 2;
    NVIC_InitStructure.NVIC_IRQChannelSubPriority = 1;
    NVIC_InitStructure.NVIC_IRQChannelCmd = intrCtrl;
    NVIC_Init(&NVIC_InitStructure);
        
    // Enable USB ---------------------------------------------
    NVIC_InitStructure.NVIC_IRQChannel = USB_HP_CAN1_TX_IRQn;
    NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 3;
    NVIC_InitStructure.NVIC_IRQChannelSubPriority = 0;
    NVIC_InitStructure.NVIC_IRQChannelCmd = intrCtrl;
    NVIC_Init(&NVIC_InitStructure);
    
    NVIC_InitStructure.NVIC_IRQChannel = USB_LP_CAN1_RX0_IRQn;
    NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 3;
    NVIC_InitStructure.NVIC_IRQChannelSubPriority = 1;
    NVIC_InitStructure.NVIC_IRQChannelCmd = intrCtrl;
    NVIC_Init(&NVIC_InitStructure);
    // --------------------------------------------------------
    
    //Enable the USART3 Interrupt
    NVIC_InitStructure.NVIC_IRQChannel = USART3_IRQn;
    NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 4;
    NVIC_InitStructure.NVIC_IRQChannelSubPriority = 0;
    NVIC_InitStructure.NVIC_IRQChannelCmd = intrCtrl;
    NVIC_Init(&NVIC_InitStructure);
    
    // Enable EXTI1
    NVIC_InitStructure.NVIC_IRQChannel = EXTI1_IRQn;
    NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 4;
    NVIC_InitStructure.NVIC_IRQChannelSubPriority = 1;
    NVIC_InitStructure.NVIC_IRQChannelCmd = intrCtrl;
    NVIC_Init(&NVIC_InitStructure);
    
#if !CONFIG_FOR_TEST
    // Enable EXTI9
    NVIC_InitStructure.NVIC_IRQChannel = EXTI9_5_IRQn;
    NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 5;
    NVIC_InitStructure.NVIC_IRQChannelSubPriority = 0;
    NVIC_InitStructure.NVIC_IRQChannelCmd = intrCtrl;
    NVIC_Init(&NVIC_InitStructure);
#endif
    
    // Enable the TIM7 gloabal Interrupt 
    NVIC_InitStructure.NVIC_IRQChannel = TIM7_IRQn;
    NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 5;
    NVIC_InitStructure.NVIC_IRQChannelSubPriority = 1;
    NVIC_InitStructure.NVIC_IRQChannelCmd = intrCtrl;
    NVIC_Init(&NVIC_InitStructure);
    
    // Enable EXTI0
    NVIC_InitStructure.NVIC_IRQChannel = EXTI0_IRQn;
    NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 6;
    NVIC_InitStructure.NVIC_IRQChannelSubPriority = 0;
    NVIC_InitStructure.NVIC_IRQChannelCmd = intrCtrl;
    NVIC_Init(&NVIC_InitStructure);
    
    // Enable the TIM2 gloabal Interrupt
    NVIC_InitStructure.NVIC_IRQChannel = TIM2_IRQn;
    NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 6;
    NVIC_InitStructure.NVIC_IRQChannelSubPriority = 1;
    NVIC_InitStructure.NVIC_IRQChannelCmd = intrCtrl;
    NVIC_Init(&NVIC_InitStructure);

    // Enable EXTI2
    NVIC_InitStructure.NVIC_IRQChannel = EXTI2_IRQn;
    NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 7;
    NVIC_InitStructure.NVIC_IRQChannelSubPriority = 0;
    NVIC_InitStructure.NVIC_IRQChannelCmd = intrCtrl;
    NVIC_Init(&NVIC_InitStructure);
    
    // Enable EXTI3
    NVIC_InitStructure.NVIC_IRQChannel = EXTI3_IRQn;
    NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 7;
    NVIC_InitStructure.NVIC_IRQChannelSubPriority = 1;
    NVIC_InitStructure.NVIC_IRQChannelCmd = intrCtrl;
    NVIC_Init(&NVIC_InitStructure);
}
