/**
  **************** (C) COPYRIGHT 2011 SCT Performance, LLC *********************
  * File Name          : usartB.c
  * Device / System    : SCT products using the "CommonCode" platform
  * Author             : Quyen Leba
  *
  * Version            : 1 
  * Date               : 05/02/2011
  * Description        : 
  *                    : 
  *
  *
  * History            : 05/02/2011 M. Davis
  *                    :   Add SCT Copyright header,
  *
  ******************************************************************************
  */

#include "usartB.h"

//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
void usartB_init(USARTBAUD baud, bool useFlowControl, bool useInterrupt)
{
    USART_InitTypeDef USART_InitStructure;
    GPIO_InitTypeDef GPIO_InitStructure;

    // Enable USART3 clocks
    RCC_APB1PeriphClockCmd(RCC_APB1Periph_USART3, ENABLE);
    GPIO_PinRemapConfig(GPIO_FullRemap_USART3, ENABLE);

    // Configure USART3 Tx as alternate function push-pull
    GPIO_InitStructure.GPIO_Pin = GPIO_Pin_8;
    GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
    GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AF_PP;
    GPIO_Init(GPIOD, &GPIO_InitStructure);
    
    // Configure USART1 Rx as input floating
    GPIO_InitStructure.GPIO_Pin = GPIO_Pin_9;
    GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IN_FLOATING;
    GPIO_Init(GPIOD, &GPIO_InitStructure);

    // USART3 configuration
    /* USART3 configured as follow:
    - BaudRate = 115200 baud  
    - Word Length = 8 Bits
    - One Stop Bit
    - No parity
    - Hardware flow control disabled (RTS and CTS signals)
    - Receive and transmit enabled
    */
    USART_DeInit(USARTB_NAME);
    USART_InitStructure.USART_BaudRate = (u32)baud;
    USART_InitStructure.USART_WordLength = USART_WordLength_8b;
    USART_InitStructure.USART_StopBits = USART_StopBits_1;
    USART_InitStructure.USART_Parity = USART_Parity_No;
    if (useFlowControl)
    {
        USART_InitStructure.USART_HardwareFlowControl = USART_HardwareFlowControl_RTS_CTS;
    }
    else
    {
        USART_InitStructure.USART_HardwareFlowControl = USART_HardwareFlowControl_None;
    }
    USART_InitStructure.USART_Mode = USART_Mode_Rx | USART_Mode_Tx;
    
    USART_Init(USARTB_NAME, &USART_InitStructure);

    if (useInterrupt)
    {
        // Enable usartB Receive interrupt
        USART_ITConfig(USARTB_NAME, USART_IT_RXNE, ENABLE);
    }
    else
    {
        USART_ITConfig(USARTB_NAME, USART_IT_RXNE, DISABLE);
    }

    // Enable the usartB
    USART_Cmd(USARTB_NAME, ENABLE);
}

//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
void usartB_changebaudrate(USARTBAUD baud)
{
    u32 tmpreg = 0x00, apbclock = 0x00;
    u32 integerdivider = 0x00;
    u32 fractionaldivider = 0x00;
    RCC_ClocksTypeDef RCC_ClocksStatus;
    
    RCC_GetClocksFreq(&RCC_ClocksStatus);  
    
    apbclock = RCC_ClocksStatus.PCLK1_Frequency;
    
    /* Determine the integer part */
    integerdivider = ((0x19 * apbclock) / (0x04 * ((u32)baud)));
    tmpreg = (integerdivider / 0x64) << 0x04;
    
    /* Determine the fractional part */
    fractionaldivider = integerdivider - (0x64 * (tmpreg >> 0x04));
    tmpreg |= ((((fractionaldivider * 0x10) + 0x32) / 0x64)) & ((u8)0x0F);
    
    while(USART_GetFlagStatus(USARTB_NAME, USART_FLAG_TC) == RESET);
    
    /* Write to USART BRR */
    USARTB_NAME->BRR = (u16)tmpreg;
}
