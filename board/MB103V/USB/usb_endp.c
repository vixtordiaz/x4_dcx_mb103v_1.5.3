/******************** (C) COPYRIGHT 2010 STMicroelectronics ********************
* File Name          : usb_endp.c
* Author             : MCD Application Team
* Version            : V3.2.1
* Date               : 07/05/2010
* Description        : Endpoint routines
********************************************************************************
* THE PRESENT FIRMWARE WHICH IS FOR GUIDANCE ONLY AIMS AT PROVIDING CUSTOMERS
* WITH CODING INFORMATION REGARDING THEIR PRODUCTS IN ORDER FOR THEM TO SAVE TIME.
* AS A RESULT, STMICROELECTRONICS SHALL NOT BE HELD LIABLE FOR ANY DIRECT,
* INDIRECT OR CONSEQUENTIAL DAMAGES WITH RESPECT TO ANY CLAIMS ARISING FROM THE
* CONTENT OF SUCH FIRMWARE AND/OR THE USE MADE BY CUSTOMERS OF THE CODING
* INFORMATION CONTAINED HEREIN IN CONNECTION WITH THEIR PRODUCTS.
*******************************************************************************/

/* Includes ------------------------------------------------------------------*/
#include "usb_lib.h"
#include "usb_istr.h"
#include "usb_pwr.h"
#include <device_config.h>
#include <common/usb_callback.h>

/* Private typedef -----------------------------------------------------------*/
/* Private define ------------------------------------------------------------*/
#define USB_CONSOLE_TX_BUFFER_MAX_LENGTH    1024
/* Private macro -------------------------------------------------------------*/
/* Private variables ---------------------------------------------------------*/
#ifndef __KEN_EMULATOR__
uint8_t usb_console_tx_buffer[USB_CONSOLE_TX_BUFFER_MAX_LENGTH];    //hold data to send to HOST
uint32_t usb_console_tx_input_end_buffer_index = 0;     //track data entered to buffer
uint32_t usb_console_tx_output_end_buffer_index = 0;    //track data moved out of buffer (to HOST)
uint32_t usb_console_tx_length = 0;     //length of buffer to send
uint8_t usb_console_tx_busy = 0;
#endif  //__KEN_EMULATOR__
/* Private function prototypes -----------------------------------------------*/
/* Private functions ---------------------------------------------------------*/
/* Extern variables ----------------------------------------------------------*/
extern __IO uint32_t bDeviceState; /* USB device status */
/* Extern functions ----------------------------------------------------------*/
extern void commlink_process_incoming_command_byte(u8 in);

/*******************************************************************************
* Function Name  : EP1_IN_Callback
* Description    : EP1 IN Callback Routine
* Input          : None.
* Output         : None.
* Return         : None.
*******************************************************************************/
void EP1_IN_Callback(void)
{
    usb_callback_ep1in(NULL,EP1_IN);
}

/*******************************************************************************
* Function Name  : EP2_OUT_Callback.
* Description    : EP2 OUT Callback Routine.
* Input          : None.
* Output         : None.
* Return         : None.
*******************************************************************************/
void EP2_OUT_Callback(void)
{
    usb_callback_ep2out(NULL,EP2_OUT);
}

/*******************************************************************************
* Function Name  : EP3_IN_Callback
* Description    : EP3 IN Callback Routine
* Input          : None.
* Output         : None.
* Return         : None.
*******************************************************************************/
void EP3_IN_Callback(void)
{
    usb_callback_ep3in(NULL,EP3_IN);
}

/*******************************************************************************
* Function Name  : EP5_IN_Callback.
* Description    : EP5 IN Callback Routine.
* Input          : None.
* Output         : None.
* Return         : None.
*******************************************************************************/
void EP5_IN_Callback(void)
{
#if USB_HAS_VIRTUAL_COM_PORT
#ifdef __KEN_EMULATOR__
    usb_callback_ep5in(NULL,EP5_IN);;
#else
  uint16_t tx_buffer_index;
  uint16_t tx_length;

  if (usb_console_tx_busy == 1)
  {
    if (usb_console_tx_length == 0)
    {
      usb_console_tx_busy = 0;
    }
    else
    {
      if (usb_console_tx_length > ENDP5_DATASIZE)
      {
        tx_buffer_index = usb_console_tx_output_end_buffer_index;
        tx_length = ENDP5_DATASIZE;
        
        usb_console_tx_output_end_buffer_index += ENDP5_DATASIZE;
        usb_console_tx_length -= ENDP5_DATASIZE;    
      }
      else 
      {
        tx_buffer_index = usb_console_tx_output_end_buffer_index;
        tx_length = usb_console_tx_length;
        
        usb_console_tx_output_end_buffer_index += usb_console_tx_length;
        usb_console_tx_length = 0;
      }

      USB_SIL_Write(EP5_OUT, &usb_console_tx_buffer[tx_buffer_index], tx_length);
      SetEPTxValid(ENDP5);
    }
  }
#endif
#endif  //USB_HAS_VIRTUAL_COM_PORT
}

/*******************************************************************************
* Function Name  : EP5_IN_AsynchXfer
* Description    : Use this function with USB SOF
* Input          : None.
* Return         : none.
*******************************************************************************/
void EP5_IN_AsynchXfer(void)
{
#if USB_HAS_VIRTUAL_COM_PORT
#ifdef __KEN_EMULATOR__
#else
  uint16_t tx_buffer_index;
  uint16_t tx_length;

  if (usb_console_tx_busy != 1)
  {
    if (usb_console_tx_output_end_buffer_index == USB_CONSOLE_TX_BUFFER_MAX_LENGTH)
    {
      usb_console_tx_output_end_buffer_index = 0;
    }

    if(usb_console_tx_output_end_buffer_index == usb_console_tx_input_end_buffer_index) 
    {
      usb_console_tx_busy = 0; 
      return;
    }

    if (usb_console_tx_output_end_buffer_index > usb_console_tx_input_end_buffer_index) /* rollback */
    { 
      usb_console_tx_length = USB_CONSOLE_TX_BUFFER_MAX_LENGTH - usb_console_tx_output_end_buffer_index;
    }
    else
    {
      usb_console_tx_length = usb_console_tx_input_end_buffer_index - usb_console_tx_output_end_buffer_index;
    }

    if (usb_console_tx_length > ENDP5_DATASIZE)
    {
      tx_buffer_index = usb_console_tx_output_end_buffer_index;
      tx_length = ENDP5_DATASIZE;

      usb_console_tx_output_end_buffer_index += ENDP5_DATASIZE;
      usb_console_tx_length -= ENDP5_DATASIZE;
    }
    else
    {
      tx_buffer_index = usb_console_tx_output_end_buffer_index;
      tx_length = usb_console_tx_length;

      usb_console_tx_output_end_buffer_index += usb_console_tx_length;
      usb_console_tx_length = 0;
    }
    usb_console_tx_busy = 1;

    USB_SIL_Write(EP5_OUT, &usb_console_tx_buffer[tx_buffer_index], tx_length);
    SetEPTxValid(ENDP5);
  }
#endif
#endif  //USB_HAS_VIRTUAL_COM_PORT
}

/*******************************************************************************
* Function Name  : EP3_OUT_Callback
* Description    :
* Input          : None.
* Output         : None.
* Return         : None.
*******************************************************************************/
void EP6_OUT_Callback(void)
{
#if USB_HAS_VIRTUAL_COM_PORT
  uint16_t i;
  uint16_t bytecount;
  uint8_t buffer[64];
  
  //memset((char*)buffer,0,sizeof(buffer));

  /* Get the received data buffer and update the counter */
  bytecount = USB_SIL_Read(EP6_OUT, buffer);
  
  /* USB data will be immediately processed, this allow next USB traffic being 
  NAKed till the end of the USART Xfer */

#ifdef __KEN_EMULATOR__
  for(i=0;i<bytecount;i++)
  {
    commlink_process_incoming_command_byte(buffer[i]);
  }
#else 
  for(i=0;i<bytecount;i++)
  {
      debugif_uart_rx_process_input(buffer[i]);
  }
#endif

  /* Enable the receive of data on EP3 */
  SetEPRxValid(ENDP6);
#endif
}

/*******************************************************************************
* Function Name  : SOF_Callback / INTR_SOFINTR_Callback
* Description    :
* Input          : None.
* Output         : None.
* Return         : None.
*******************************************************************************/
void SOF_Callback(void)
{
  static uint32_t __framecount = 0;

  if(bDeviceState == CONFIGURED)
  {
    if (__framecount++ == 8)    //1 frame = 1ms
    {
      __framecount = 0;
#ifdef __KEN_EMULATOR__
#else
      EP5_IN_AsynchXfer();
#endif
    }
  }
}

/*******************************************************************************
* Function Name  : USB_CONSOLE_OUT
* Description    : push data to console tx buffer (to send to HOST)
* Input          : None.
* Return         : none.
*******************************************************************************/
void USB_CONSOLE_OUT(const char *data, uint32_t datalength)
{
#ifndef __KEN_EMULATOR__
  uint32_t i;
  uint8_t *ptr;

  ptr = (uint8_t*)data;
  for(i=0;i<datalength;i++)
  {
    usb_console_tx_buffer[usb_console_tx_input_end_buffer_index++] = *ptr++;

    //avoid buffer overflow
    if (usb_console_tx_input_end_buffer_index == USB_CONSOLE_TX_BUFFER_MAX_LENGTH)
    {
        usb_console_tx_input_end_buffer_index = 0;
    }
  }
#endif
}

/******************* (C) COPYRIGHT 2010 STMicroelectronics *****END OF FILE****/
