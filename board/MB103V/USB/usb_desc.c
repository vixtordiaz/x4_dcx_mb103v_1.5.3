/******************** (C) COPYRIGHT 2010 STMicroelectronics ********************
* File Name          : usb_desc.c
* Author             : MCD Application Team
* Version            : V3.2.1
* Date               : 07/05/2010
* Description        : Descriptors for SCT USB Device
********************************************************************************
* THE PRESENT FIRMWARE WHICH IS FOR GUIDANCE ONLY AIMS AT PROVIDING CUSTOMERS
* WITH CODING INFORMATION REGARDING THEIR PRODUCTS IN ORDER FOR THEM TO SAVE TIME.
* AS A RESULT, STMICROELECTRONICS SHALL NOT BE HELD LIABLE FOR ANY DIRECT,
* INDIRECT OR CONSEQUENTIAL DAMAGES WITH RESPECT TO ANY CLAIMS ARISING FROM THE
* CONTENT OF SUCH FIRMWARE AND/OR THE USE MADE BY CUSTOMERS OF THE CODING
* INFORMATION CONTAINED HEREIN IN CONNECTION WITH THEIR PRODUCTS.
*******************************************************************************/

/* Includes ------------------------------------------------------------------*/
#include "usb_desc.h"
#include "usb_conf.h"
#include <device_config.h>

#define USB_DEVICE_DESCRIPTOR_TYPE              0x01
#define USB_CONFIGURATION_DESCRIPTOR_TYPE       0x02
#define USB_STRING_DESCRIPTOR_TYPE              0x03
#define USB_INTERFACE_DESCRIPTOR_TYPE           0x04
#define USB_ENDPOINT_DESCRIPTOR_TYPE            0x05

#if USB_HAS_VIRTUAL_COM_PORT
uint8_t SCTUSB_CompositeDeviceDescriptor[SCTUSB_SIZ_DEVICE_DESC] =
  {
    0x12,   /* bLength  */
    0x01,   /* bDescriptorType */
    0x00,   /* bcdUSB, version 2.00 */
    0x02,
    0xEF,   /*0x02,*/               /*0x00,*/   /* bDeviceClass : each interface define the device class */
    0x02,   /*0x00,*/   /* bDeviceSubClass */
    0x01,   /*0x00,*/   /* bDeviceProtocol */
    0x40,   /* bMaxPacketSize0 0x40 = 64 */
    0x59,   /* idVendor */
    0x50,
    DEFAULT_USB_PID & 0xFF, /* idProduct */
    DEFAULT_USB_PID >> 8,
    0x00,   /* bcdDevice 1.00*/
    0x01,
    1,      /* index of string Manufacturer  */
    2,      /* index of string descriptor of product*/
    0,      /* index of serial number string: none */
    0x01    /*bNumConfigurations */
  };

const uint8_t SCTUSB_CompositeConfigDescriptor[SCTUSB_SIZ_CONFIG_DESC_COMPOSITE] =
  {
    0x09,   /* bLength: Configuation Descriptor size */
    0x02,   /* bDescriptorType: Configuration */
    SCTUSB_SIZ_CONFIG_DESC_COMPOSITE,   //SCTUSB_SIZ_CONFIG_DESC,

    0x00,
    0x03,   /* bNumInterfaces: 3 interface */
    0x01,   /* bConfigurationValue: */
    /*      Configuration value */
    0x00,   /* iConfiguration: */
    /*      Index of string descriptor */
    /*      describing the configuration */
    0xC0,   /* bmAttributes: */
    /*      bus powered */
    DEFAULT_USB_POWER,  /* see device_config.h */

    /******************** Descriptor of default interface *********************/
    /* 9 */
    0x09,   /* bLength: Interface Descriptor size */
    0x04,   /* bDescriptorType: */
    /*      Interface descriptor type */
    0x00,   /* bInterfaceNumber: Number of Interface */
    0x00,   /* bAlternateSetting: Alternate setting */
    3,      /* bNumEndpoints*/
    0xFF,   /* bInterfaceClass:  */
    0xFF,   /* bInterfaceSubClass : */
    0xFF,   /* nInterfaceProtocol */
    0,      /* iInterface: no string descriptor describing interface */

    /* 18 */
    0x07,   /*Endpoint descriptor length = 7*/
    0x05,   /*Endpoint descriptor type */
    0x81,   /*Endpoint address (IN, address 1) */
    0x02,   /*Bulk endpoint type */
    0x40,   /*Maximum packet size (64 bytes) */
    0x00,
    0x00,   /*Polling interval in milliseconds */

    /* 25 */
    0x07,   /*Endpoint descriptor length = 7 */
    0x05,   /*Endpoint descriptor type */
    0x02,   /*Endpoint address (OUT, address 2) */
    0x02,   /*Bulk endpoint type */
    0x40,   /*Maximum packet size (64 bytes) */
    0x00,
    0x00,   /*Polling interval in milliseconds*/
//    /* 25 */
//    0x07,   /*Endpoint descriptor length = 7 */
//    0x05,   /*Endpoint descriptor type */
//    0x83,   /*Endpoint address (IN, address 3) */
//    0x05,   /*Isochronous endpoint type, asynchronous */
//    0x20,   /*Maximum packet size (32 bytes) */
//    0x00,
//    0x01,   /*Polling interval in milliseconds*/

    /* 32 */
    0x07,   /*Endpoint descriptor length = 7*/
    0x05,   /*Endpoint descriptor type */
    0x83,   /*Endpoint address (IN, address 1) */
    0x02,   /*Bulk endpoint type */
    0x40,   /*Maximum packet size (64 bytes) */
    0x00,
    0x00,   /*Polling interval in milliseconds */

    /* 39 */
    // IAD
    0x08,   /* bLength */
    0x0B,   /* bDescriptorType */
    0x01,   /* bFirstInterface */
    0x02,   /* bInterfaceCount */
    0x02,   /* bFunctionClass */
    0x02,   /* bFunctionSubClass */
    0x01,   /* bFunctionProtocol */
    0x00,   /* iFunction */

    /******************** Descriptor of VCP interface *********************/
    /* 47 */
    //Control Class Interface Descriptor - USB spec 9.6.5, page 267-269, Table 9-12
    0x09,   /* bLength: Interface Descriptor size */
    0x04,   /* bDescriptorType: */
    /*      Interface descriptor type */
    0x01,   /* bInterfaceNumber: Number of Interface */
    0x00,   /* bAlternateSetting: Alternate setting */
    1,      /* bNumEndpoints*/
    0x02,   /* bInterfaceClass: Communication Interface Class */
    0x02,   /* bInterfaceSubClass: Abstract Control Model */
    0x01,   /* bInterfaceProtocol: Common AT commands */
    0,      /* iInterface: no string descriptor describing interface */

    //CDC Header Functional Descriptor - CDC Spec 5.2.3.1, Table 26
    /* 56 */
    0x05,   /* bLength: Endpoint Descriptor size */
    0x24,   /* bDescriptorType: CS_INTERFACE */
    0x00,   /* bDescriptorSubtype: Header Func Desc */
    0x10,   /* bcdCDC: spec release number */
    0x01,

    //Call Management Functional Descriptor - CDC Spec 5.2.3.2, Table 27
    /* 61 */
    0x05,   /* bFunctionLength */
    0x24,   /* bDescriptorType: CS_INTERFACE */
    0x01,   /* bDescriptorSubtype: Call Management Func Desc */
    0x03,   /* bmCapabilities: */
    0x01,   /* bDataInterface: 1 */

    //Abstract Control Management Functional Descriptor - CDC Spec 5.2.3.3, Table 28
    /* 66 */
    0x04,   /* bFunctionLength */
    0x24,   /* bDescriptorType: CS_INTERFACE */
    0x02,   /* bDescriptorSubtype: Abstract Control Management desc */
    0x06,   /* bmCapabilities */

    //Union Functional Descriptor - CDC Spec 5.2.3.8, Table 33
    /* 70 */
    0x05,   /* bFunctionLength */
    0x24,   /* bDescriptorType: CS_INTERFACE */
    0x06,   /* bDescriptorSubtype: Union func desc */
    0x00,   /* bMasterInterface: Communication class interface */
    0x01,   /* bSlaveInterface0: Data Class Interface */
    
    /*Endpoint 4 Descriptor*/
    /* 75 */
    0x07,   /* bLength: Endpoint Descriptor size */
    USB_ENDPOINT_DESCRIPTOR_TYPE,   /* bDescriptorType: Endpoint */
    0x84,   /* bEndpointAddress: (IN4) */
    0x03,   /* bmAttributes: Interrupt */
    ENDP4_DATASIZE,   /*Maximum packet size: */
    0x00,
    16,     /* bInterval: */

    //Data Class Interface Descriptor - USB spec 9.6.5, page 267-269, Table 9-12
    /* 82 */
    0x09,   /* bLength: Endpoint Descriptor size */
    USB_INTERFACE_DESCRIPTOR_TYPE,  /* bDescriptorType: */
    0x02,   /* bInterfaceNumber: Number of Interface */
    0x00,   /* bAlternateSetting: Alternate setting */
    0x02,   /* bNumEndpoints: Two endpoints used */
    0x0A,   /* bInterfaceClass: CDC */
    0x00,   /* bInterfaceSubClass: */
    0x00,   /* bInterfaceProtocol: */
    0x00,   /* iInterface: */

    /*Endpoint 6 Descriptor*/
    /* 91 */
    0x07,   /* bLength: Endpoint Descriptor size */
    USB_ENDPOINT_DESCRIPTOR_TYPE,   /* bDescriptorType: Endpoint */
    0x06,   /* bEndpointAddress: (OUT6) */
    0x02,   /* bmAttributes: Bulk */
    ENDP6_DATASIZE,   /*Maximum packet size: */
    0x00,
    0x00,   /* bInterval: ignore for Bulk transfer */

    /*Endpoint 5 Descriptor*/
    /* 98 */
    0x07,   /* bLength: Endpoint Descriptor size */
    USB_ENDPOINT_DESCRIPTOR_TYPE,   /* bDescriptorType: Endpoint */
    0x85,   /* bEndpointAddress: (IN5) */
    0x02,   /* bmAttributes: Bulk */
    ENDP5_DATASIZE,   /*Maximum packet size: */
    0x00,
    0x00,    /* bInterval */

    /* 105 */
  };

#else   //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

uint8_t SCTUSB_DeviceDescriptor[SCTUSB_SIZ_DEVICE_DESC] =
  {
    0x12,   /* bLength  */
    0x01,   /* bDescriptorType */
    0x00,   /* bcdUSB, version 2.00 */
    0x02,
    0xEF,   /*0x02,*/               /*0x00,*/   /* bDeviceClass : each interface define the device class */
    0x02,   /*0x00,*/   /* bDeviceSubClass */
    0x01,   /*0x00,*/   /* bDeviceProtocol */
    0x40,   /* bMaxPacketSize0 0x40 = 64 */
    0x59,   /* idVendor */
    0x50,
    DEFAULT_USB_PID & 0xFF, /* idProduct */
    DEFAULT_USB_PID >> 8,
    0x00,   /* bcdDevice 1.00*/
    0x01,
    1,      /* index of string Manufacturer  */
    2,      /* index of string descriptor of product*/
    0,      /* index of serial number string: none */
    0x01    /*bNumConfigurations */
  };

const uint8_t SCTUSB_ConfigDescriptor[SCTUSB_SIZ_CONFIG_DESC] =
  {
    0x09,   /* bLength: Configuation Descriptor size */
    0x02,   /* bDescriptorType: Configuration */
    SCTUSB_SIZ_CONFIG_DESC,

    0x00,
    0x01,   /* bNumInterfaces: 1 interface */
    0x01,   /* bConfigurationValue: */
    /*      Configuration value */
    0x00,   /* iConfiguration: */
    /*      Index of string descriptor */
    /*      describing the configuration */
    0xC0,   /* bmAttributes: */
    /*      bus powered */
    DEFAULT_USB_POWER,  /* see device_config.h */

    /******************** Descriptor of default interface *********************/
    /* 09 */
    0x09,   /* bLength: Interface Descriptor size */
    0x04,   /* bDescriptorType: */
    /*      Interface descriptor type */
    0x00,   /* bInterfaceNumber: Number of Interface */
    0x00,   /* bAlternateSetting: Alternate setting */
    3,      /* bNumEndpoints*/
    0xFF,   /* bInterfaceClass:  */
    0xFF,   /* bInterfaceSubClass : */
    0xFF,   /* nInterfaceProtocol */
    0,      /* iInterface: no string descriptor describing interface */
    /* 18 */
    0x07,   /*Endpoint descriptor length = 7*/
    0x05,   /*Endpoint descriptor type */
    0x81,   /*Endpoint address (IN, address 1) */
    0x02,   /*Bulk endpoint type */
    0x40,   /*Maximum packet size (64 bytes) */
    0x00,
    0x00,   /*Polling interval in milliseconds */
    /* 25 */
    0x07,   /*Endpoint descriptor length = 7 */
    0x05,   /*Endpoint descriptor type */
    0x02,   /*Endpoint address (OUT, address 2) */
    0x02,   /*Bulk endpoint type */
    0x40,   /*Maximum packet size (64 bytes) */
    0x00,
    0x00,   /*Polling interval in milliseconds*/
    /* 32 */
//    0x07,   /*Endpoint descriptor length = 7 */
//    0x05,   /*Endpoint descriptor type */
//    0x83,   /*Endpoint address (IN, address 3) */
//    0x05,   /*Isochronous endpoint type, asynchronous */
//    0x20,   /*Maximum packet size (32 bytes) */
//    0x00,
//    0x01,   /*Polling interval in milliseconds*/
    0x07,   /*Endpoint descriptor length = 7*/
    0x05,   /*Endpoint descriptor type */
    0x83,   /*Endpoint address (IN, address 1) */
    0x02,   /*Bulk endpoint type */
    0x40,   /*Maximum packet size (64 bytes) */
    0x00,
    0x00,   /*Polling interval in milliseconds */
    /* 39 */
  };
#endif

const uint8_t SCTUSB_StringLangID[SCTUSB_SIZ_STRING_LANGID] =
  {
    SCTUSB_SIZ_STRING_LANGID,
    0x03,
    0x09,
    0x04
  }
  ;      /* LangID = 0x0409: U.S. English */
const uint8_t SCTUSB_StringVendor[SCTUSB_SIZ_STRING_VENDOR] =
  {
    SCTUSB_SIZ_STRING_VENDOR, /* Size of manufaturer string */
    0x03,           /* bDescriptorType = String descriptor */
    /* Manufacturer: "SCT LLC" */
    'S', 0, 'C', 0, 'T', 0, ' ', 0, 'L', 0, 'L', 0, 'C', 0
  };

const uint8_t SCTUSB_StringProduct[SCTUSB_SIZ_STRING_PRODUCT] =
  {
    SCTUSB_SIZ_STRING_PRODUCT,
    0x03,           /* bDescriptorType = String descriptor */
    /* Product name: "iTSX" */
    'S', 0, 'C', 0, 'T', 0, ' ', 0, 'M', 0, 'B', 0, '1', 0, '0', 0, '3', 0
  };

uint8_t SCTUSB_StringSerial[SCTUSB_SIZ_STRING_SERIAL] =
  {
    SCTUSB_SIZ_STRING_SERIAL,
    0x03,           /* bDescriptorType = String descriptor */
    /* Serial number*/  //TODOQ: more on this string later
    '0', 0, '0', 0, '0', 0, '0', 0, '0', 0, '0', 0, '0', 0, '0', 0, '0', 0, '0', 0, '0', 0, '0', 0, '0', 0
  };

const uint8_t SCTUSB_StringInterface[SCTUSB_SIZ_STRING_INTERFACE] =
  {
    SCTUSB_SIZ_STRING_INTERFACE,
    0x03,           /* bDescriptorType = String descriptor */
    /* Interface 0: "iTSX IF" */
    'S', 0, 'C', 0, 'T', 0, ' ', 0, 'M', 0, 'B', 0, '1', 0, '0', 0, '3', 0, ' ', 0, 'I', 0, 'F', 0
  };

/******************* (C) COPYRIGHT 2010 STMicroelectronics *****END OF FILE****/
