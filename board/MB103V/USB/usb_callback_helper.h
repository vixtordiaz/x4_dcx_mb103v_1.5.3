/**
  **************** (C) COPYRIGHT 2011 SCT Performance, LLC *********************
  * File Name          : usb_callback_helper.h
  * Device / System    : SCT products using the "CommonCode" platform
  * Author             : Quyen Leba
  *
  * Version            : 1 
  * Date               : 05/02/2011
  * Description        : 
  *                    : 
  *
  *
  * History            : 05/02/2011 M. Davis
  *                    :   Add SCT Copyright header,
  *
  ******************************************************************************
  */

#ifndef __USB_CALLBACK_HELPER_H
#define __USB_CALLBACK_HELPER_H

#include "usb_type.h"
#include "usb_core.h"
#include "usb_lib.h"
#include "usb_hwconfig.h"

#define BULK_MAX_PACKET_SIZE            64
#define ALT_BULK_MAX_PACKET_SIZE        64
#define VIRCOM_BULK_MAX_PACKET_SIZE     8
//#define ALT_ISO_MAX_PACKET_SIZE         ENDP3_BUFSIZE

#define SetReadyEPOUT_DataPending()     SetEPRxValid(ENDP2)
#define SetReadyEPOUT_NoDataPending()   SetEPRxValid(ENDP2)

#define SetReadyEPIN()                  SetEPTxValid(ENDP1)
#define SetReadyEPIN_ALT()              SetEPTxValid(ENDP3)
#define SetReadyEPIN_VirCOM()           SetEPTxValid(ENDP5)

#define SetStallEPIN()                  SetEPTxStatus(ENDP1, EP_TX_STALL)
#define SetStallEPIN_ALT()              SetEPTxStatus(ENDP3, EP_TX_STALL)
#define SetStallEPIN_VirCOM()           SetEPTxStatus(ENDP5, EP_TX_STALL)
#define SetStallEPOUT()                 SetEPRxStatus(ENDP2, EP_RX_STALL)

#if (USB_EP2_DOUBLE_BUFFER)
#define GetEPOUTData()                  \
    FreeUserBuffer(ENDP2, EP_DBUF_OUT); \
    if(GetENDPOINT(ENDP2) & EP_DTOG_TX) \
    { \
        usb_callback_info.bulkbufferlength = GetEPDblBuf1Count(ENDP2); \
        PMAToUserBufferCopy(usb_callback_info.bulkbuffer, ENDP2_BUF1Addr, usb_callback_info.bulkbufferlength); \
    } \
    else \
    { \
        usb_callback_info.bulkbufferlength = GetEPDblBuf0Count(ENDP2); \
        PMAToUserBufferCopy(usb_callback_info.bulkbuffer, ENDP2_BUF0Addr, usb_callback_info.bulkbufferlength); \
    }
#else
#define GetEPOUTData()                  {usb_callback_info.bulkbufferlength = USB_SIL_Read(EP2_OUT,usb_callback_info.bulkbuffer);}
#endif

#define SendDataEPIN(dev,data,len)      USB_SIL_Write(EP1_IN,(u8*)data,len)
#define SendDataEPIN_ALT(dev,data,len)  USB_SIL_Write(EP3_IN,(u8*)data,len)
#define SendDataEPIN_VirCOM(dev,data,len)   USB_SIL_Write(EP5_IN,(u8*)data,len)

#define SendDataEPIN_ALT_ISO(dev,d,len) \
    if (GetENDPOINT(ENDP3) & EP_DTOG_RX)    \
    {   \
        UserToPMABufferCopy(d,ENDP3_BUF0Addr,len);  \
        SetEPDblBuf0Count(ENDP3,EP_DBUF_IN,len);    \
    }   \
    else    \
    {   \
        UserToPMABufferCopy(d,ENDP3_BUF1Addr,len);  \
        SetEPDblBuf1Count(ENDP3,EP_DBUF_IN,len);    \
    }   \
    FreeUserBuffer(ENDP3, EP_DBUF_IN)

void usb_send_data_to_host(u8 *buffer, u32 bufferlength);
void usb_trigger_send_databuffer_to_host();
void usb_set_csw(void *pdev, u16 status, bool send);

#endif    //__USB_CALLBACK_HELPER_H
