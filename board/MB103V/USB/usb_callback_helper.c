/**
  **************** (C) COPYRIGHT 2011 SCT Performance, LLC *********************
  * File Name          : usb_callback_helper.c
  * Device / System    : SCT products using the "CommonCode" platform
  * Author             : Quyen Leba
  *
  * Version            : 1 
  * Date               : 05/02/2011
  * Description        : 
  *                    : 
  *
  *
  * History            : 05/02/2011 M. Davis
  *                    :   Add SCT Copyright header,
  *
  ******************************************************************************
  */

#include "usb_type.h"
#include "usb_core.h"
#include "usb_lib.h"
#include "usb_hwconfig.h"
#include <device_config.h>
#include <common/usb_callback.h>
#include "usb_callback_helper.h"

extern u8  *usb_databuffer;
extern u32 usb_databufferindex;
extern u32 usb_databufferlength;

//------------------------------------------------------------------------------
// Send data to HOST
// Inputs:  u8  *buffer
//          u32 bufferlength (must be less than BULK_MAX_PACKET_SIZE)
// Engineer: Quyen Leba
//------------------------------------------------------------------------------
void usb_send_data_to_host(u8 *buffer, u32 bufferlength)
{
    USB_SIL_Write(EP1_IN,buffer,bufferlength);
    SetEPTxStatus(ENDP1, EP_TX_VALID);
}

//------------------------------------------------------------------------------
// A trigger to send usb_databuffer to HOST
// Engineer: Quyen Leba
//------------------------------------------------------------------------------
void usb_trigger_send_databuffer_to_host()
{
    if (usb_databufferlength > BULK_MAX_PACKET_SIZE)
    {
        SCTUSB_SetFlowControl(CB_FC_DATA_IN);
        usb_databufferindex += BULK_MAX_PACKET_SIZE;
        SCTUSB_StatusResidue -= BULK_MAX_PACKET_SIZE;
        USB_SIL_Write(EP1_IN,usb_databuffer,BULK_MAX_PACKET_SIZE);
    }
    else
    {
        SCTUSB_SetFlowControl(CB_FC_DATA_IN_LAST);
        usb_databufferindex += usb_databufferlength;
        SCTUSB_StatusResidue -= usb_databufferlength;
        USB_SIL_Write(EP1_IN,usb_databuffer,usb_databufferlength);
    }
    SetEPTxStatus(ENDP1, EP_TX_VALID);
}

//------------------------------------------------------------------------------
// Set/Send status to HOST
// Inputs:  void *pdev (not used in MB103V)
//          u8  status
//          bool send
// Engineer: Quyen Leba
//------------------------------------------------------------------------------
void usb_set_csw(void *pdev, u16 status, bool send)
{
    SCTUSB_SetStatusID(CB_CSW_ID);
    usb_callback_info.csw.status = status;

    USB_SIL_Write(EP1_IN,(u8*)&usb_callback_info.csw,CB_CSW_LENGTH);

    SCTUSB_SetFlowControl(CB_FC_ERROR);
    if (send)
    {
        SCTUSB_SetFlowControl(CB_FC_STATUS_SEND);
        SetEPTxStatus(ENDP1, EP_TX_VALID);
    }
}
