/******************** (C) COPYRIGHT 2010 STMicroelectronics ********************
* File Name          : usb_prop.c
* Author             : MCD Application Team
* Version            : V3.2.1
* Date               : 07/05/2010
* Description        : All processing related to Mass Storage Demo
********************************************************************************
* THE PRESENT FIRMWARE WHICH IS FOR GUIDANCE ONLY AIMS AT PROVIDING CUSTOMERS
* WITH CODING INFORMATION REGARDING THEIR PRODUCTS IN ORDER FOR THEM TO SAVE TIME.
* AS A RESULT, STMICROELECTRONICS SHALL NOT BE HELD LIABLE FOR ANY DIRECT,
* INDIRECT OR CONSEQUENTIAL DAMAGES WITH RESPECT TO ANY CLAIMS ARISING FROM THE
* CONTENT OF SUCH FIRMWARE AND/OR THE USE MADE BY CUSTOMERS OF THE CODING
* INFORMATION CONTAINED HEREIN IN CONNECTION WITH THEIR PRODUCTS.
*******************************************************************************/

/* Includes ------------------------------------------------------------------*/
#include "usb_lib.h"
#include "usb_desc.h"
#include "usb_pwr.h"
#include "usb_hwconfig.h"
#include "usb_prop.h"
#include <device_config.h>
#include <common/usb_callback.h>

/* Private typedef -----------------------------------------------------------*/
/* Private define ------------------------------------------------------------*/
/* Private macro -------------------------------------------------------------*/
/* Private variables ---------------------------------------------------------*/
uint8_t Request = 0;
LINE_CODING linecoding =
  {
    115200, /* baud rate*/
    0x00,   /* stop bits-1*/
    0x00,   /* parity - none*/
    0x08    /* no. of bits 8*/
  };

DEVICE Device_Table =
  {
    EP_NUM,
    1
  };

DEVICE_PROP Device_Property =
  {
    SCTUSB_Init,
    SCTUSB_Reset,
    SCTUSB_Status_In,
    SCTUSB_Status_Out,
    SCTUSB_Data_Setup,
    SCTUSB_NoData_Setup,
    SCTUSB_Get_Interface_Setting,
    SCTUSB_GetDeviceDescriptor,
    SCTUSB_GetConfigDescriptor,
    SCTUSB_GetStringDescriptor,
    0,
    0x40 /*MAX PACKET SIZE*/
  };

USER_STANDARD_REQUESTS User_Standard_Requests =
  {
    SCTUSB_GetConfiguration,
    SCTUSB_SetConfiguration,
    SCTUSB_GetInterface,
    SCTUSB_SetInterface,
    SCTUSB_GetStatus,
    SCTUSB_ClearFeature,
    SCTUSB_SetEndPointFeature,
    SCTUSB_SetDeviceFeature,
    SCTUSB_SetDeviceAddress
  };

ONE_DESCRIPTOR Device_Descriptor =
  {
#if USB_HAS_VIRTUAL_COM_PORT
    (uint8_t*)SCTUSB_CompositeDeviceDescriptor,
    SCTUSB_SIZ_DEVICE_DESC
#else
    (uint8_t*)SCTUSB_DeviceDescriptor,
    SCTUSB_SIZ_DEVICE_DESC
#endif
  };

ONE_DESCRIPTOR Config_Descriptor =
  {
#if USB_HAS_VIRTUAL_COM_PORT
    (uint8_t*)SCTUSB_CompositeConfigDescriptor,
    SCTUSB_SIZ_CONFIG_DESC_COMPOSITE
#else
    (uint8_t*)SCTUSB_ConfigDescriptor,
    SCTUSB_SIZ_CONFIG_DESC
#endif
  };

ONE_DESCRIPTOR String_Descriptor[5] =
  {
    {(uint8_t*)SCTUSB_StringLangID, SCTUSB_SIZ_STRING_LANGID},
    {(uint8_t*)SCTUSB_StringVendor, SCTUSB_SIZ_STRING_VENDOR},
    {(uint8_t*)SCTUSB_StringProduct, SCTUSB_SIZ_STRING_PRODUCT},
    {(uint8_t*)SCTUSB_StringSerial, SCTUSB_SIZ_STRING_SERIAL},
    {(uint8_t*)SCTUSB_StringInterface, SCTUSB_SIZ_STRING_INTERFACE},
  };

/* Extern variables ----------------------------------------------------------*/
extern USB_CALLBACK_INFO usb_callback_info;

bool usb_enumeration_detected = FALSE;


/* Private function prototypes -----------------------------------------------*/
/* Extern function prototypes ------------------------------------------------*/
/* Private functions ---------------------------------------------------------*/
/*******************************************************************************
* Function Name  : SCTUSB_Init
* Description    : Mass Storage init routine.
* Input          : None.
* Output         : None.
* Return         : None.
*******************************************************************************/
void SCTUSB_Init()
{
  /* Update the serial number string descriptor with the data from the unique
  ID*/
  Get_SerialNum();

  pInformation->Current_Configuration = 0;

  /* Connect the device */
  PowerOn();

  /* Perform basic device initialization operations */
  USB_SIL_Init();

  bDeviceState = UNCONNECTED;
}

/*******************************************************************************
* Function Name  : SCTUSB_Reset
* Description    : SCTUSB reset routine.
* Input          : None.
* Output         : None.
* Return         : None.
*******************************************************************************/
void SCTUSB_Reset()
{
  /* Set the device as not configured */
  Device_Info.Current_Configuration = 0;

  /* Current Feature initialization */
#if USB_HAS_VIRTUAL_COM_PORT
  pInformation->Current_Feature = SCTUSB_CompositeConfigDescriptor[7];
#else
  pInformation->Current_Feature = SCTUSB_ConfigDescriptor[7];
#endif

#ifdef STM32F10X_CL   
  
  /* EP0 is already configured by USB_SIL_Init() function */

  /* Init EP1 IN as Bulk endpoint */
  OTG_DEV_EP_Init(EP1_IN, OTG_DEV_EP_TYPE_BULK, BULK_MAX_PACKET_SIZE);
  
  /* Init EP2 OUT as Bulk endpoint */
  OTG_DEV_EP_Init(EP2_OUT, OTG_DEV_EP_TYPE_BULK, BULK_MAX_PACKET_SIZE); 
  
#else 

  SetBTABLE(BTABLE_ADDRESS);

  /* Initialize Endpoint 0 */
  SetEPType(ENDP0, EP_CONTROL);
  SetEPTxStatus(ENDP0, EP_TX_NAK);
  SetEPRxAddr(ENDP0, ENDP0_RXADDR);
  SetEPRxCount(ENDP0, Device_Property.MaxPacketSize);
  SetEPTxAddr(ENDP0, ENDP0_TXADDR);
  Clear_Status_Out(ENDP0);
  SetEPRxValid(ENDP0);

  /* Initialize Endpoint 1 */
  SetEPType(ENDP1, EP_BULK);
  SetEPTxAddr(ENDP1, ENDP1_TXADDR);
  SetEPTxStatus(ENDP1, EP_TX_NAK);
  SetEPRxStatus(ENDP1, EP_RX_DIS);

  /* Initialize Endpoint 2 */
#if USB_EP2_DOUBLE_BUFFER
  SetEPType(ENDP2, EP_BULK);
  SetEPDoubleBuff(ENDP2);
  SetEPDblBuffAddr(ENDP2, ENDP2_BUF0Addr, ENDP2_BUF1Addr);
  SetEPDblBuffCount(ENDP2, EP_DBUF_OUT, Device_Property.MaxPacketSize);
  ClearDTOG_RX(ENDP2);
  ClearDTOG_TX(ENDP2);
  ToggleDTOG_TX(ENDP2);
  SetEPRxStatus(ENDP2, EP_RX_VALID);
  SetEPTxStatus(ENDP2, EP_TX_DIS);
#else
  SetEPType(ENDP2, EP_BULK);
  SetEPRxAddr(ENDP2, ENDP2_RXADDR);
  SetEPRxCount(ENDP2, Device_Property.MaxPacketSize);
  SetEPRxStatus(ENDP2, EP_RX_VALID);
  SetEPTxStatus(ENDP2, EP_TX_DIS);
#endif

  /* Initialize Endpoint 3 */
//  SetEPType(ENDP3, EP_BULK);
//  SetEPTxAddr(ENDP3, ENDP3_TXADDR);
//  SetEPTxStatus(ENDP3, EP_TX_NAK);
//  SetEPRxStatus(ENDP3, EP_RX_DIS);

#if 0
  /* Initialize Endpoint 3 */
  SetEPType(ENDP3, EP_ISOCHRONOUS);
  SetEPDblBuffAddr(ENDP3, ENDP3_BUF0Addr, ENDP3_BUF1Addr);
  SetEPDblBuffCount(ENDP3, EP_DBUF_OUT, 32);
  ClearDTOG_TX(ENDP3);
  ClearDTOG_RX(ENDP3);
  ToggleDTOG_RX(ENDP3);
  SetEPTxStatus(ENDP3, EP_TX_NAK);
  SetEPRxStatus(ENDP3, EP_RX_DIS);
#endif
  /* Initialize Endpoint 3 */
  SetEPType(ENDP3, EP_BULK);
  SetEPTxAddr(ENDP3, ENDP3_TXADDR);
  SetEPTxStatus(ENDP3, EP_TX_NAK);
  SetEPRxStatus(ENDP3, EP_RX_DIS);
  
//  /* Initialize Endpoint 4 */
//  SetEPType(ENDP4, EP_ISOCHRONOUS);
//  SetEPDblBuffAddr(ENDP4, ENDP4_BUF0Addr, ENDP4_BUF1Addr);
//  SetEPDblBuffCount(ENDP4, EP_DBUF_OUT, 32);
//  ClearDTOG_RX(ENDP4);
//  ClearDTOG_TX(ENDP4);
//  ToggleDTOG_TX(ENDP4);
//  SetEPRxStatus(ENDP4, EP_RX_VALID);
//  SetEPTxStatus(ENDP4, EP_TX_DIS);

#if USB_HAS_VIRTUAL_COM_PORT
  /* Initialize Endpoint 4 */
  SetEPType(ENDP4, EP_INTERRUPT);
  SetEPTxAddr(ENDP4, ENDP4_TXADDR);
  SetEPRxStatus(ENDP4, EP_RX_DIS);
  SetEPTxStatus(ENDP4, EP_TX_NAK);

  /* Initialize Endpoint 5 */
  SetEPType(ENDP5, EP_BULK);
  SetEPTxAddr(ENDP5, ENDP5_TXADDR);
  SetEPTxStatus(ENDP5, EP_TX_NAK);
  SetEPRxStatus(ENDP5, EP_RX_DIS);

  /* Initialize Endpoint 6 */
  SetEPType(ENDP6, EP_BULK);
  SetEPRxAddr(ENDP6, ENDP6_RXADDR);
  SetEPRxCount(ENDP6, ENDP6_DATASIZE);
  SetEPRxStatus(ENDP6, EP_RX_VALID);
  SetEPTxStatus(ENDP6, EP_TX_DIS);
#endif
  
  SetEPRxCount(ENDP0, Device_Property.MaxPacketSize);
  SetEPRxValid(ENDP0);

  /* Set the device to response on default address */
  SetDeviceAddress(0);
#endif /* STM32F10X_CL */

  bDeviceState = ATTACHED;

  usb_callback_info.cbw.id = CB_CBW_ID;
  SCTUSB_SetFlowControl(CB_FC_IDLE);
}

/*******************************************************************************
* Function Name  : SCTUSB_SetConfiguration
* Description    : Handle the SetConfiguration request.
* Input          : None.
* Output         : None.
* Return         : None.
*******************************************************************************/
void SCTUSB_SetConfiguration(void)
{
  if (pInformation->Current_Configuration != 0)
  {
    /* Device configured */
    bDeviceState = CONFIGURED;

#ifdef STM32F10X_CL 
    /* Init EP1 IN as Bulk endpoint */
    OTG_DEV_EP_Init(EP1_IN, OTG_DEV_EP_TYPE_BULK, BULK_MAX_PACKET_SIZE);
  
    /* Init EP2 OUT as Bulk endpoint */
    OTG_DEV_EP_Init(EP2_OUT, OTG_DEV_EP_TYPE_BULK, BULK_MAX_PACKET_SIZE);     
#else    
    ClearDTOG_TX(ENDP1);
    ClearDTOG_RX(ENDP2);
#endif /* STM32F10X_CL */

    //set the Bot state machine to the IDLE state
    SCTUSB_SetFlowControl(CB_FC_IDLE);
  }
}

/*******************************************************************************
* Function Name  : SCTUSB_ClearFeature
* Description    : Handle the ClearFeature request.
* Input          : None.
* Output         : None.
* Return         : None.
*******************************************************************************/
void SCTUSB_ClearFeature(void)
{
  /* when the host send a CBW with invalid signature or invalid length the two
     Endpoints (IN & OUT) shall stall until receiving a SCTUSB_Reset     */
    if (usb_callback_info.cbw.id != CB_CBW_ID)
    {
        usb_stall_all_ep(NULL);
    }
}

/*******************************************************************************
* Function Name  : SCTUSB_SetConfiguration.
* Description    : Udpade the device state to addressed.
* Input          : None.
* Output         : None.
* Return         : None.
*******************************************************************************/
void SCTUSB_SetDeviceAddress (void)
{
  bDeviceState = ADDRESSED;
}
/*******************************************************************************
* Function Name  : SCTUSB_Status_In
* Description    : Mass Storage Status IN routine.
* Input          : None.
* Output         : None.
* Return         : None.
*******************************************************************************/
void SCTUSB_Status_In(void)
{
#if USB_HAS_VIRTUAL_COM_PORT
  if (Request == SET_LINE_CODING)
  {
    //TODOQ: config uart
    Request = 0;
  }
#endif
  return;
}

/*******************************************************************************
* Function Name  : SCTUSB_Status_Out
* Description    : Mass Storage Status OUT routine.
* Input          : None.
* Output         : None.
* Return         : None.
*******************************************************************************/
void SCTUSB_Status_Out(void)
{
  return;
}

/*******************************************************************************
* Function Name  : SCTUSB_Data_Setup.
* Description    : Handle the data class specific requests..
* Input          : RequestNo.
* Output         : None.
* Return         : RESULT.
*******************************************************************************/
RESULT SCTUSB_Data_Setup(uint8_t RequestNo)
{
#if USB_HAS_VIRTUAL_COM_PORT
  uint8_t    *(*CopyRoutine)(uint16_t);

  CopyRoutine = NULL;

  if (RequestNo == GET_LINE_CODING)
  {
    if (Type_Recipient == (CLASS_REQUEST | INTERFACE_RECIPIENT))
    {
      CopyRoutine = Virtual_Com_Port_GetLineCoding;
    }
  }
  else if (RequestNo == SET_LINE_CODING)
  {
    if (Type_Recipient == (CLASS_REQUEST | INTERFACE_RECIPIENT))
    {
      CopyRoutine = Virtual_Com_Port_SetLineCoding;
    }
    Request = SET_LINE_CODING;
  }
  else
  {
    return USB_UNSUPPORT;
  }

  if (CopyRoutine == NULL)
  {
    return USB_UNSUPPORT;
  }

    //also, we might not need to support this
  pInformation->Ctrl_Info.CopyData = CopyRoutine;
  pInformation->Ctrl_Info.Usb_wOffset = 0;
  (*CopyRoutine)(0);

  return USB_SUCCESS;

#else

  return USB_UNSUPPORT;
#endif
}

/*******************************************************************************
* Function Name  : SCTUSB_NoData_Setup.
* Description    : Handle the no data class specific requests.
* Input          : RequestNo.
* Output         : None.
* Return         : RESULT.
*******************************************************************************/
RESULT SCTUSB_NoData_Setup(uint8_t RequestNo)
{
#if USB_HAS_VIRTUAL_COM_PORT
  if (Type_Recipient == (CLASS_REQUEST | INTERFACE_RECIPIENT))
  {
    if (RequestNo == SET_COMM_FEATURE)
    {
      return USB_SUCCESS;
    }
    else if (RequestNo == SET_CONTROL_LINE_STATE)
    {
      return USB_SUCCESS;
    }
  }
#endif
  return USB_UNSUPPORT;
}

/*******************************************************************************
* Function Name  : SCTUSB_Get_Interface_Setting
* Description    : Test the interface and the alternate setting according to the
*                  supported one.
* Input          : uint8_t Interface, uint8_t AlternateSetting.
* Output         : None.
* Return         : RESULT.
*******************************************************************************/
RESULT SCTUSB_Get_Interface_Setting(uint8_t Interface, uint8_t AlternateSetting)
{
  if (AlternateSetting > 0)
  {
    return USB_UNSUPPORT;/* in this application we don't have AlternateSetting*/
  }
#if USB_HAS_VIRTUAL_COM_PORT
  else if (Interface > 2)
  {
    return USB_UNSUPPORT;/*in this application we have only 1 interfaces*/
  }
#else
  else if (Interface > 0)
  {
    return USB_UNSUPPORT;/*in this application we have only 1 interfaces*/
  }
#endif
  return USB_SUCCESS;
}

/*******************************************************************************
* Function Name  : SCTUSB_GetDeviceDescriptor
* Description    : Get the device descriptor.
* Input          : uint16_t Length.
* Output         : None.
* Return         : None.
*******************************************************************************/
uint8_t *SCTUSB_GetDeviceDescriptor(uint16_t Length)
{
    // USB Enumeration Detected bool used with USB Cable disconnection
    usb_enumeration_detected = TRUE;
    
  return Standard_GetDescriptorData(Length, &Device_Descriptor );
}

/*******************************************************************************
* Function Name  : SCTUSB_GetConfigDescriptor
* Description    : Get the configuration descriptor.
* Input          : uint16_t Length.
* Output         : None.
* Return         : None.
*******************************************************************************/
uint8_t *SCTUSB_GetConfigDescriptor(uint16_t Length)
{
  return Standard_GetDescriptorData(Length, &Config_Descriptor );
}

/*******************************************************************************
* Function Name  : SCTUSB_GetStringDescriptor
* Description    : Get the string descriptors according to the needed index.
* Input          : uint16_t Length.
* Output         : None.
* Return         : None.
*******************************************************************************/
uint8_t *SCTUSB_GetStringDescriptor(uint16_t Length)
{
  uint8_t wValue0 = pInformation->USBwValue0;

  if (wValue0 > 5)
  {
    return NULL;
  }
  else
  {
    return Standard_GetDescriptorData(Length, &String_Descriptor[wValue0]);
  }
}

/*******************************************************************************
* Function Name  : Virtual_Com_Port_GetLineCoding.
* Description    : send the linecoding structure to the PC host.
* Input          : Length.
* Output         : None.
* Return         : Linecoding structure base address.
*******************************************************************************/
uint8_t *Virtual_Com_Port_GetLineCoding(uint16_t Length)
{
  if (Length == 0)
  {
    pInformation->Ctrl_Info.Usb_wLength = sizeof(linecoding);
    return NULL;
  }
  return(uint8_t *)&linecoding;
}

/*******************************************************************************
* Function Name  : Virtual_Com_Port_SetLineCoding.
* Description    : Set the linecoding structure fields.
* Input          : Length.
* Output         : None.
* Return         : Linecoding structure base address.
*******************************************************************************/
uint8_t *Virtual_Com_Port_SetLineCoding(uint16_t Length)
{
  if (Length == 0)
  {
    pInformation->Ctrl_Info.Usb_wLength = sizeof(linecoding);
    return NULL;
  }
  return(uint8_t *)&linecoding;
}

/******************* (C) COPYRIGHT 2010 STMicroelectronics *****END OF FILE****/
