/**
  **************** (C) COPYRIGHT 2011 SCT Performance, LLC *********************
  * File Name          : properties.h
  * Device / System    : SCT products using the "CommonCode" platform
  * Author             : Quyen Leba
  *
  * Version            : 1 
  * Date               : 05/02/2011
  * Description        : 
  *                    : 
  *
  *
  * History            : 05/02/2011 M. Davis
  *                    :   Add SCT Copyright header,
  *
  ******************************************************************************
  */

#ifndef __PROPERTIES_MB103V_H
#define __PROPERTIES_MB103V_H

#include <arch/gentype.h>

typedef struct
{
    u32 operating_mode;
    u32 failsafeboot_version;
    u32 mainboot_version;
    u32 app_signature;
    u32 app_version;
    u16 code_sector_size;
    u8  board_rev;
    u8  secboot_startsector;
    u16 secboot_maxcodesize;
    u8  app_build;   //reserved1;
    u8  app_startsector;
    u32 app_maxcodesize;

    u8  media_type;         //used as MediaType
    u8  media_sector_size;  //in kbyte
    u8  reserved2[2];
    u32 media_size;         //in MByte

    u8  reserved3[8];
}PROPERTIES_MB_INFO;

void properties_mb_init();
u8 properties_mb_getinfo(u8 *info, u32 *info_length);

#endif  //__PROPERTIES_MB103V_H
