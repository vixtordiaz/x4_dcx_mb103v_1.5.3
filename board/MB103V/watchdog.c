/**
  **************** (C) COPYRIGHT 2011 SCT Performance, LLC *********************
  * File Name          : watchdog.c
  * Device / System    : SCT products using the "CommonCode" platform
  * Author             : Quyen Leba
  *
  * Version            : 1 
  * Date               : 05/02/2011
  * Description        : 
  *                    : 
  *
  *
  * History            : 05/02/2011 M. Davis
  *                    :   Add SCT Copyright header,
  *
  ******************************************************************************
  */

#include "watchdog.h"

//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
void watchdog_set()
{
    RCC_APB1PeriphClockCmd(RCC_APB1Periph_WWDG, ENABLE);
    //WWDG clock counter = (PCLK1/4096)/8 = ~1098 Hz (~0.91 ms)
    WWDG_SetPrescaler(WWDG_Prescaler_8);

    //Set Window value to 0x41
    WWDG_SetWindowValue(0x41);
    //Enable WWDG and set counter value to 0x7F, WWDG timeout = ~0.91 ms * 64 = 58 ms
    WWDG_Enable(0x7F);
    //Clear EWI flag
    WWDG_ClearFlag();
    //Enable EW interrupt
    ////WWDG_EnableIT();
}
