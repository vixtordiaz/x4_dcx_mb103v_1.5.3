/**
  **************** (C) COPYRIGHT 2011 SCT Performance, LLC *********************
  * File Name          : flash.c
  * Device / System    : SCT products using the "CommonCode" platform
  * Author             : Quyen Leba
  *
  * Version            : 1 
  * Date               : 05/02/2011
  * Description        : 
  *                    : 
  *
  *
  * History            : 05/02/2011 M. Davis
  *                    :   Add SCT Copyright header,
  *
  ******************************************************************************
  */

#include <string.h>
#include <board/genplatform.h>
#include <board/indicator.h>
#include <fs/genfs.h>
#include <common/settings.h>
#include <common/statuscode.h>
#include <common/crypto_blowfish.h>
#include <common/crc32.h>
#include "flash.h"

//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
#define FLASHSETTINGS_SECTOR_SIZE               FLASH_SECTOR_SIZE
#define FLASHSETTINGS_ADDR                      FLASH_SETTINGS_ADDRESS
#define FLASHSETTINGS_ADDR_S0                   FLASHSETTINGS_ADDR                                  //0x08011800
#define FLASHSETTINGS_ADDR_S1                   FLASHSETTINGS_ADDR_S0 + FLASHSETTINGS_SECTOR_SIZE   //0x08012000
#define FLASHSETTINGS_ADDR_S2                   FLASHSETTINGS_ADDR_S1 + FLASHSETTINGS_SECTOR_SIZE   //0x08012800
#define FLASHSETTINGS_ADDR_S3                   FLASHSETTINGS_ADDR_S2 + FLASHSETTINGS_SECTOR_SIZE   //0x08013000
#define FLASHSETTINGS_ADDR_S4                   FLASHSETTINGS_ADDR_S3 + FLASHSETTINGS_SECTOR_SIZE   //0x08013800
#define FLASHSETTINGS_ADDR_S5                   FLASHSETTINGS_ADDR_S4 + FLASHSETTINGS_SECTOR_SIZE   //0x08014000

#define FLASHSETTINGS_OLDSETTINGS_ADDR          (FLASHSETTINGS_ADDR_S0)
//SETTINGS_ADDR_S1: unused
#define BOOTLOADER_SIGNATURE_ADDR               (FLASHSETTINGS_ADDR_S2)
#define FLASHSETTINGS_CRITICAL_ADDR             (FLASHSETTINGS_ADDR_S3)
#define FLASHSETTINGS_TUNE_ADDR                 (FLASHSETTINGS_ADDR_S4)
#define FLASHSETTINGS_FLEET_ADDR  0 //TODOQ: critical
#define FLASHSETTINGS_DATALOGGENERAL_ADDR       (FLASHSETTINGS_ADDR_S5)

#define FLASHSETTINGS_CRITICAL_FILENAME         "crsb.scf"
#define FLASHSETTINGS_TUNE_FILENAME             "tusb.scf"
#define FLASHSETTINGS_FLEET_FILENAME            "flsb.scf"
#define FLASHSETTINGS_DATALOGGENERAL_FILENAME   "dgsb.scf"
#define FLASHSETTINGS_OLDSETTINGS_FILENAME      "ossb.scf"

//------------------------------------------------------------------------------
// Save bootloader type signature into BOOTLOADER_SIGNATURE_ADDR
// Inputs:  BootloaderType bootloadertype
//          u8  *signature
//          u8  signaturelength (must be multiple of 4 bytes)
// Return:  u8  status
// Engineer: Quyen Leba
//------------------------------------------------------------------------------
u8 flash_save_bootloader_signature(BootloaderType bootloadertype)
{
#define BOOTLOADERTYPE_REGULAR_SIGNATURE    0xAAAAAAAA
#define BOOTLOADERTYPE_RECOVERY_SIGNATURE   0xFFFFFFFF
#define BOOTLOADERTYPE_APP_SIGNATURE        0x00000000
#define BOOTLOADERTYPE_PRODUCTION_SIGNATURE 0xA5A5A5A5
    FLASH_Status FlashStatus;
    u32 signature;
    u32 signature_cmp;
    u8  status;
    
    switch(bootloadertype)
    {
    case BooloaderType_Regular:
        signature = BOOTLOADERTYPE_REGULAR_SIGNATURE;
        break;
    case BooloaderType_Recovery:
        signature = BOOTLOADERTYPE_RECOVERY_SIGNATURE;
        break;
    case BooloaderType_App:
        signature = BOOTLOADERTYPE_APP_SIGNATURE;
        break;
    case BooloaderType_Production:
        signature = BOOTLOADERTYPE_PRODUCTION_SIGNATURE;
        break;
    default:
        return S_INPUT;
    }
    
    status = S_SUCCESS;
    
    FLASH_Unlock();
    FlashStatus = FLASH_ErasePage(BOOTLOADER_SIGNATURE_ADDR);
    if (FlashStatus != FLASH_COMPLETE)
    {
        status = S_FAIL;
        goto flash_save_bootloader_signature_done;
    }
    
    // Erasure will inherently set the word at the beginning of the erased
    // page to 0xFFFFFFFF, which corresponds to RECOVERY_BOOT. If a different
    // boot-load type has been specified in the boot-load argument, force the
    // first word in the page to the corresponding signature.
    if(bootloadertype != BooloaderType_Recovery)
    {
        FlashStatus = FLASH_ProgramWord(BOOTLOADER_SIGNATURE_ADDR,
                                        signature);
        if (FlashStatus != FLASH_COMPLETE)
        {
            status = S_FAIL;
            goto flash_save_bootloader_signature_done;
        }
    }//if(bootloadertype != BooloaderType_Recovery)...
    
    signature_cmp = *(u32*)BOOTLOADER_SIGNATURE_ADDR;
    if(signature_cmp != signature)
    {
        status = S_BADCONTENT;
    }
    
flash_save_bootloader_signature_done:
    FLASH_Lock();
    return status;
}

//------------------------------------------------------------------------------
// Inputs:  FlashSettingsArea flashsettingsarea
//          u8 *settingsdata
//          u32 settingsdatalength (must be multiple of 4 bytes)
// Return:  u8  status
// Engineer: Quyen Leba
//------------------------------------------------------------------------------
u8 flash_save_setting(FlashSettingsArea flashsettingsarea,
                      u8 *settingsdata, u32 settingsdatalength)
{
    u32 flashaddr;
    u32 *settingsdataptr;
    u32 *tmpdataptr;
    u8  *flashbuffer;
    u8  *filename;
    F_FILE *sfptr;
    u32 bytecount;
    u32 i;
    u32 *crc32e_wptr;
    u8  status;
    FLASH_Status FlashStatus = FLASH_COMPLETE;

    status = S_SUCCESS;
    sfptr = NULL;
    
    if (settingsdatalength > FLASH_SECTOR_SIZE)
    {
        return S_BADCONTENT;
    }

    flashbuffer = __malloc(FLASH_SECTOR_SIZE);
    if (flashbuffer == NULL)
    {
        return S_MALLOC;
    }

    crc32e_wptr = NULL;
    switch(flashsettingsarea)
    {
    case CriticalArea:
        flashaddr = FLASHSETTINGS_CRITICAL_ADDR;
        filename = FLASHSETTINGS_CRITICAL_FILENAME;
        crc32e_wptr = &((Settings_Critical*)settingsdata)->crc32e;
        break;
    case TuneArea:
        flashaddr = FLASHSETTINGS_TUNE_ADDR;
        filename = FLASHSETTINGS_TUNE_FILENAME;
        crc32e_wptr = &((Settings_Tune*)settingsdata)->crc32e;
        break;
    case FleetArea:
        flashaddr = FLASHSETTINGS_FLEET_ADDR;
        filename = FLASHSETTINGS_FLEET_FILENAME;
        crc32e_wptr = &((Settings_Fleet*)settingsdata)->crc32e;
        break;
    case DatalogGeneralArea:
        flashaddr = FLASHSETTINGS_DATALOGGENERAL_ADDR;
        filename = FLASHSETTINGS_DATALOGGENERAL_FILENAME;
        crc32e_wptr = &((Settings_DatalogGeneral*)settingsdata)->crc32e;
        break;
    case OldSettingsArea:
        flashaddr = FLASHSETTINGS_OLDSETTINGS_ADDR;
        filename = FLASHSETTINGS_OLDSETTINGS_FILENAME;
        break;
    default:
        status = S_INPUT;
        goto flash_save_setting_done;
    }
    memcpy((char*)flashbuffer,(char*)flashaddr,FLASH_SECTOR_SIZE);
    memcpy((char*)flashbuffer,(char*)settingsdata,settingsdatalength);
    if (crc32e_wptr)
    {
        crc32e_reset();
        *crc32e_wptr = 0xFFFFFFFF;
        *crc32e_wptr = crc32e_calculateblock(0xFFFFFFFF,
                                             (u32*)flashaddr,FLASH_SECTOR_SIZE/4);
    }
    
    status = S_SUCCESS;
    //1st, make sure able to backup settings to a file before doing anything
    //to the settings in flash
    if (filename)
    {
        sfptr = genfs_user_openfile(filename,"w");
        if (sfptr == NULL)
        {
            status = S_OPENFILE;
        }
        else
        {
            bytecount = fwrite(flashbuffer,1,FLASH_SECTOR_SIZE,sfptr);
            if (bytecount != FLASH_SECTOR_SIZE)
            {
                status = S_WRITEFILE;
            }
            genfs_closefile(sfptr);
            sfptr = NULL;
        }
    }
    
    if (status != S_SUCCESS)
    {
        indicator_set(Indicator_BackupSettingsError);
        //TODOQ: push a log here
        goto flash_save_setting_done;
    }

    settingsdataptr = (u32*)flashbuffer;
    // unlock the Flash Program Erase controller 
    FLASH_Unlock();

    FlashStatus = FLASH_ErasePage(flashaddr);
    
    // write new settings to flash
    for(i=0;i<FLASH_SECTOR_SIZE/4;i++)
    {
        FlashStatus = FLASH_ProgramWord(flashaddr+(i*4), settingsdataptr[i]);
        if (FlashStatus != FLASH_COMPLETE)
        {
            status = S_FAIL;
            break;
        }
    }
    FLASH_Lock();

    // double check new settings in flash
    tmpdataptr = (u32*)flashaddr;
    for(i=0;i<FLASH_SECTOR_SIZE/4;i++)
    {
        if (tmpdataptr[i] != settingsdataptr[i])
        {
            status = S_BADCONTENT;
            break;
        }
    }

    if (status != S_SUCCESS)
    {
        indicator_set(Indicator_SettingsError);
    }

flash_save_setting_done:
    if (flashbuffer)
    {
        __free(flashbuffer);
    }
    return status;
}

//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
u8 flash_load_setting(FlashSettingsArea flashsettingsarea,
                      u8 *settingsdata, u32 settingsdatalength)
{
    //TODOQ: old structure settings need conversion
    switch(flashsettingsarea)
    {
    case CriticalArea:
        memcpy((char*)settingsdata,
               (char*)FLASHSETTINGS_CRITICAL_ADDR,settingsdatalength);
        break;
    case TuneArea:
        memcpy((char*)settingsdata,
               (char*)FLASHSETTINGS_TUNE_ADDR,settingsdatalength);
        break;
    case DatalogGeneralArea:
        memcpy((char*)settingsdata,
               (char*)FLASHSETTINGS_DATALOGGENERAL_ADDR,settingsdatalength);
        break;
    case OldSettingsArea:
        memcpy((char*)settingsdata,
               (char*)FLASHSETTINGS_OLDSETTINGS_ADDR,settingsdatalength);
        break;
    default:
        return S_INPUT;
    }

    return S_SUCCESS;
}

//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
typedef struct
{
    u8  Sct[4];
    u8  serial_num[13];
    u32 DeviceType;
    u32 MarketType;
    u32 BluetoothBaud;
    u16 settingsVersion;
    u32 settingsCRC;
}OldSettingsInfo;

typedef struct
{
    u8  Sct[4];
    u8  eec_type;           // indicates which EEC this cal is for
    u8  comm_type;
    u8  married;            // indicates that this device is married
    u8  married_cnt;        // how many times married's been cleared
    u8  married_strategy[40];
    u8  num_of_proc;        // number of processors detected
    u8  Dwnld_Fail;            // Processor erased, but failed download
    u8  EPATS[14];          // holds evhicle EPATS info
    u8  VIN[18];
    u8  VID[256];           // holds larder vehicle ID info block which includes the trans ID block found in CAN processors.
    u32 stockfilesize;
    u8  prev_VIN[18];
    u8  A1_on;              //analog 1 on
    u8  A2_on;              //analog 2 on
    u8  Equate1;            //equation for analog input channel 1 
    u8  Equate2;            //equation for analog input channel 2
    u8  Analog1_Des[19];    //analog input description channel 1
    u8  Analog2_Des[19];    //analog input description channel 2
    u8  A1_Units[11];       //analog units display channel 1
    u8  A2_Units[11];       //analog units display channel 2
    u8  A1_selected_num;    //Number in the selection of PIDs, need to clear this if any changes made to channel
    u8  A2_selected_num;    //Number in the selection of PIDs, need to clear this if any changes made to channel  
    u8  customtune_filename[40];        // Save the file name of the strategy file for the vehicle which the device is married to
    u8  strategytune_filename_pcm0[40]; // Save the file name of the strategy file for the vehicle which the device is married to
    u8  strategytune_filename_pcm1[40]; // Save the file name of the strategy file for the vehicle which the device is married to
    u8  option_filename_pcm0[40];       // Save the file name of the option file for the vehicle which the device is married to
    u8  option_filename_pcm1[40];       // Save the file name of the option file for the vehicle which the device is married to
    u8  strategyfilecount;              // Number of strategy/option files to apply
    u8  TuneDescription[50];
    u8  DLF_filename[20];
    u32 DLfilesize[10];     //filesize of datalogX.dat
    u8  Selected_PIDs[30];  //30 for now
    u8  datalogprecision;
    u8  record_monitor;
    u8  record_file[14];
    CustomEq          AnalogCustomEq[2];
    u8  TuneType;
    FirmwareVersion_T FirmwareVersionSettings;
    u8  TuneRevision[MAX_TUNE_VER_STRING];
    u32 settingsCRC;        // Holding location for settings CRC
}OldSettingsGeneral;

#define INFOSTARTADDR           (0x0807E000 + 0x800)
#define GENERALSTARTADDR        (INFOSTARTADDR + 0x800)

//------------------------------------------------------------------------------
// Check if old settings structure
// Return:  bool isoldsettings (TRUE: old settings found)
// Engineer: Quyen Leba
//------------------------------------------------------------------------------
bool flash_isoldstructure_settings()
{
    u32 settingsgeneraladdress;
    u8  sct[4];
    u8  i;
    
    memcpy(&sct,(u8*)INFOSTARTADDR,4);
    if (strncmp((char*)sct,"SCT0",4) != 0)
    {
        return FALSE;
    }
    
    settingsgeneraladdress = GENERALSTARTADDR+3*1024;
    for(i=0;i<3;i++)
    {
        memcpy((char*)sct,(u8*)(settingsgeneraladdress),4);
        if (strncmp((char*)sct,"OLD1",4) == 0)
        {
            return FALSE;
        }
        else if (strncmp((char*)sct,"SCT1",4) == 0)
        {
            return TRUE;
        }
        //set to next block
        settingsgeneraladdress -= 1024;
    }
    return FALSE;
}

//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
u8 flash_convert_to_mico()
{
    OldSettingsInfo oldsettingsinfo;
    OldSettingsGeneral oldsettingsgeneral;
    F_FILE *fptr;
    u8  buffer[1024];
    u32 settingsgeneraladdress;
    u8  sct[4];
    bool isvalidsettingsgeneralfound;
    u8  status;
    u8  i;
    
    //##########################################################################
    //find and load current(old) settings
    //##########################################################################
    status = S_SUCCESS;
    memcpy(&oldsettingsinfo,(u8*)INFOSTARTADDR,
           sizeof(OldSettingsInfo));
    if (strncmp((char*)oldsettingsinfo.Sct,"SCT0",4) != 0)
    {
        return S_INVALIDSETTINGS;
    }
    
    isvalidsettingsgeneralfound = FALSE;
    //start from the last block
    settingsgeneraladdress = GENERALSTARTADDR+3*1024;
    for(i=0;i<3;i++)
    {
        memcpy((char*)sct,(u8*)(settingsgeneraladdress),4);
        if (strncmp((char*)sct,"OLD1",4) == 0)
        {
            return S_INVALIDSETTINGS;
        }
        else if (strncmp((char*)sct,"SCT1",4) == 0)
        {
            isvalidsettingsgeneralfound = TRUE;
            break;
        }
        //set to next block
        settingsgeneraladdress -= 1024;
    }
    
    if (isvalidsettingsgeneralfound == FALSE)
    {
        return S_INVALIDSETTINGS;
    }
    
    memcpy(&oldsettingsgeneral,(u8*)settingsgeneraladdress,
           sizeof(OldSettingsGeneral));
    
    fptr = genfs_user_openfile("pmsb.bak","wb");
    if (fptr == NULL)
    {
        return S_OPENFILE;
        //flash_convert_to_mico_done
    }
    
    //##########################################################################
    //save a backup of current(old) settings
    //##########################################################################
    memset(buffer,0xFF,sizeof(buffer));
    memcpy(buffer,(char*)&oldsettingsinfo,sizeof(OldSettingsInfo));
    fwrite(buffer,1,sizeof(buffer),fptr);
    
    memset(buffer,0xFF,sizeof(buffer));
    memcpy(buffer,(char*)&oldsettingsgeneral,sizeof(OldSettingsGeneral));
    fwrite(buffer,1,sizeof(buffer),fptr);
    
    genfs_closefile(fptr);
    fptr = NULL;
    
    //##########################################################################
    //IMPORTANT: init settings
    //##########################################################################
    status = settings_setproductiondefault();
    if (status != S_SUCCESS)
    {
        goto flash_convert_to_mico_done;
    }
    
    //##########################################################################
    //convert to new settings structures
    //##########################################################################
    SETTINGS_CRITICAL(devicetype) = oldsettingsinfo.DeviceType;
    SETTINGS_CRITICAL(markettype) = oldsettingsinfo.MarketType;
    SETTINGS_CRITICAL(baudrate) = oldsettingsinfo.BluetoothBaud;
    memset(SETTINGS_CRITICAL(serialnumber),0xFF,
           sizeof(SETTINGS_CRITICAL(serialnumber)));
    memcpy(SETTINGS_CRITICAL(serialnumber),oldsettingsinfo.serial_num,13);
    
    SETTINGS_TUNE(veh_type) = (u16)oldsettingsgeneral.eec_type;
    if (oldsettingsgeneral.married)
    {
        SETTINGS_SetMarried();
        if (oldsettingsgeneral.Dwnld_Fail)
        {
            SETTINGS_SetDownloadFail();
        }
    }
    SETTINGS_TUNE(stockfilesize) = oldsettingsgeneral.stockfilesize;
    
    SETTINGS_TUNE(married_count) = oldsettingsgeneral.married_cnt;
    memcpy(SETTINGS_TUNE(vin),oldsettingsgeneral.VIN,17);
    SETTINGS_TUNE(vin)[17] = NULL;
    memcpy(SETTINGS_TUNE(prev_vin),oldsettingsgeneral.prev_VIN,17);
    SETTINGS_TUNE(prev_vin)[17] = NULL;
    
    memcpy(SETTINGS_TUNE(epats),oldsettingsgeneral.EPATS,14);
    memcpy(SETTINGS_TUNE(vid),oldsettingsgeneral.VID,256);
    
    SETTINGS_TUNE(tuneinfotrack.flashtype) = oldsettingsgeneral.TuneType;
    if (oldsettingsgeneral.TuneType == 0x51)    //CMDIF_ACT_CUSTOM_FLASHER
    {
        memcpy(SETTINGS_TUNE(tuneinfotrack.tunefilenames)[0],
               oldsettingsgeneral.customtune_filename,40);
        memcpy(SETTINGS_TUNE(tuneinfotrack.tunedescriptions)[0],
               oldsettingsgeneral.TuneDescription,39);
        SETTINGS_TUNE(tuneinfotrack.tunedescriptions)[0][39] = NULL;
    }
    else    //assume CMDIF_ACT_PRELOADED_FLASHER
    {
        memcpy(SETTINGS_TUNE(tuneinfotrack.tunefilenames)[0],
               oldsettingsgeneral.strategytune_filename_pcm0,40);
        memcpy(SETTINGS_TUNE(tuneinfotrack.tunefilenames)[1],
               oldsettingsgeneral.strategytune_filename_pcm1,40);
        memcpy(SETTINGS_TUNE(tuneinfotrack.optionfilenames)[0],
               oldsettingsgeneral.option_filename_pcm0,40);
        memcpy(SETTINGS_TUNE(tuneinfotrack.optionfilenames)[1],
               oldsettingsgeneral.option_filename_pcm1,40);
    }
    
    settings_update(SETTINGS_FORCE_UPDATE);
    
flash_convert_to_mico_done:
    if (fptr)
    {
        genfs_closefile(fptr);
    }
    return status;
}

#define FLASH_KEY1                              ((u32)0x45670123)
#define FLASH_KEY2                              ((u32)0xCDEF89AB)

#define CR_PG_Set                               ((u32)0x00000001)
#define CR_PG_Reset                             ((u32)0x00001FFE)
#define CR_PER_Set                              ((u32)0x00000002)
#define CR_PER_Reset                            ((u32)0x00001FFD)
#define CR_STRT_Set                             ((u32)0x00000040)
#define CR_LOCK_Set                             ((u32)0x00000080)

#define EraseTimeout                            ((u32)0x000B0000)
#define ProgramTimeout                          ((u32)0x00002000)


//#define FLASH_WaitForLastOperation(timeout)     \
//    while(FLASH->SR &   \
//          (FLASH_FLAG_BANK1_BSY | FLASH_FLAG_BANK1_PGERR |  \
//           FLASH_FLAG_BANK1_WRPRTERR))  \
//    if (timeout-- == 0) {status = S_TIMEOUT; break;}

//------------------------------------------------------------------------------
// Erase a sector
// Input:   u16 sector
// Return:  u8  status
// Engineer: Quyen Leba
//------------------------------------------------------------------------------
u8 flash_erase_sector(u16 sector)
{
    u32 timeout;
    u8  status;

    if (sector >= FLASH_VALID_SECTOR_MIN && sector <= FLASH_VALID_SECTOR_MAX)
    {
        //unlock flash
        FLASH->KEYR = FLASH_KEY1;
        FLASH->KEYR = FLASH_KEY2;

        status = S_SUCCESS;
        timeout = EraseTimeout;
        FLASH_WaitForLastOperation(timeout);
        if (status == S_SUCCESS)
        {
            FLASH->CR|= CR_PER_Set;
            FLASH->AR = FLASH_START_ADDRESS + sector * FLASH_SECTOR_SIZE;
            FLASH->CR|= CR_STRT_Set;

            timeout = EraseTimeout;
            FLASH_WaitForLastOperation(timeout);

            FLASH->CR &= CR_PER_Reset;
        }

        //lock flash
        FLASH->CR |= CR_LOCK_Set;

        return status;
    }
    else
    {
        return S_INPUT;
    }
}

#define __IO    volatile

//------------------------------------------------------------------------------
// Write sector
// Inputs:  u16 sector (starting sector)
//          u8  *data (encrypted)
//          u16 datalength
//          bool encrypted (TRUE: data is encrypted)
// Return:  u8  status
// Engineer: Quyen Leba
//------------------------------------------------------------------------------
u8 flash_write_sector(u16 sector, u8 *data, u16 datalength, bool encrypted)
{
    u16 *dataptr;
    u32 address;
    u32 timeout;
    u8  status;

    if (sector >= FLASH_VALID_SECTOR_MIN && sector <= FLASH_VALID_SECTOR_MAX)
    {
        //unlock flash
        FLASH->KEYR = FLASH_KEY1;
        FLASH->KEYR = FLASH_KEY2;

        status = S_SUCCESS;
        timeout = ProgramTimeout;
        FLASH_WaitForLastOperation(timeout);
        if (status == S_SUCCESS)
        {
            address = FLASH_START_ADDRESS + sector * FLASH_SECTOR_SIZE;
            if (encrypted)
            {
                crypto_blowfish_decryptblock_critical(data,datalength);
            }
            dataptr = (u16*)data;

            timeout = ProgramTimeout;
            status = S_SUCCESS;
            FLASH_WaitForLastOperation(timeout);
            if (status == S_SUCCESS)
            {
                FLASH->CR |= CR_PG_Set;
                for(address=FLASH_START_ADDRESS+sector*FLASH_SECTOR_SIZE;
                    address<FLASH_START_ADDRESS+sector*FLASH_SECTOR_SIZE + datalength;
                    address+=2)
                {
                    *(__IO u16*)address = *dataptr++;
                    timeout = ProgramTimeout;
                    FLASH_WaitForLastOperation(timeout);
                    if (status != S_SUCCESS)
                    {
                        break;
                    }
                }
                FLASH->CR &= CR_PG_Reset;
            }
        }

        //lock flash
        FLASH->CR |= CR_LOCK_Set;

        return status;
    }
    else
    {
        return S_INPUT;
    }
}

//------------------------------------------------------------------------------
// Validate content
// Inputs:  u16 sector (starting sector)
//          u32 length
//          u32 crc32e
// Return:  u8  status
// Engineer: Quyen Leba
//------------------------------------------------------------------------------
u8 flash_validate_content(u16 sector, u32 length, u32 crc32e)
{
    u32 calc_crc32e;

    if ((sector >= FLASH_VALID_SECTOR_MIN &&
         (sector + length/FLASH_SECTOR_SIZE) <= FLASH_VALID_SECTOR_MAX))
    {
        crc32e_reset();
        calc_crc32e = crc32e_calculateblock
            (0xFFFFFFFF,
             (u32*)(FLASH_START_ADDRESS+sector*FLASH_SECTOR_SIZE),length/4);

        if (calc_crc32e != crc32e)
        {
            return S_CRC32E;
        }
        else
        {
            return S_SUCCESS;
        }
    }
    else
    {
        return S_INPUT;
    }
}

//------------------------------------------------------------------------------
// Load bootsettings from flash
// Input:   u32 length
// Output:  u8  *settings
// Return:  u8  status
// Engineer: Quyen Leba
//------------------------------------------------------------------------------
u8 flash_load_bootsettings(u8 *settings, u32 length)
{
    if (length <= FLASH_BOOTSETTINGS_SIZE)
    {
        memcpy((char*)settings,
           (char*)FLASH_BOOTSETTINGS_ADDRESS,length);
        return S_SUCCESS;
    }
    return S_INPUT;
}

//------------------------------------------------------------------------------
// Store bootsettings to flash
// Inputs:  u8  *settings
//          u16 length
// Return:  u8  status
// Engineer: Quyen Leba
//------------------------------------------------------------------------------
u8 flash_store_bootsettings(u8 *settings, u16 length)
{
    u8  status;

    status = flash_erase_sector(FLASH_BOOTSETTINGS_STARTING_SECTOR);
    if (status == S_SUCCESS)
    {
        status = flash_write_sector(FLASH_BOOTSETTINGS_STARTING_SECTOR,
                                    settings,length,FALSE);
    }
    return status;
}

//------------------------------------------------------------------------------
// Enables the Write and Read Out Protections if not already enabled
// Inputs:  WP_PAGES Defines which pages to protect
// Return:
// Engineer: Patrick Downs
//------------------------------------------------------------------------------
#define WP_PAGES 0x000001FF
u8 flash_enable_protections(void)
{
    u16 WRP0_Data = 0xFFFF, WRP1_Data = 0xFFFF, WRP2_Data = 0xFFFF, WRP3_Data = 0xFFFF;
    FlagStatus WpStatus;
    FlagStatus RdpStatus;
    u32 FLASH_Pages;
    
    FLASH_Pages = (u32)(~WP_PAGES);
    WRP0_Data = (u16)(FLASH_Pages & 0x000000FF);
    WRP1_Data = (u16)((FLASH_Pages & 0x0000FF00) >> 8);
    WRP2_Data = (u16)((FLASH_Pages & 0x00FF0000) >> 16);
    WRP3_Data = (u16)((FLASH_Pages & 0xFF000000) >> 24);
    
    WpStatus = SET;
    RdpStatus = SET;
    
    u32 WPDATA;
    
    WPDATA = FLASH_GetWriteProtectionOptionByte();
    if(WPDATA != (~WP_PAGES))
       WpStatus = RESET;

    RdpStatus = FLASH_GetReadOutProtectionStatus();
    
    if(!RdpStatus || !WpStatus)
    {        
        FLASH_Unlock();        
        
        if(FLASH_ReadOutProtection(ENABLE) != FLASH_COMPLETE)
            return S_FAIL;
        
        // Options Byte is erased in FLASH_ReadOutProtection()        
        if(WRP0_Data != 0xFF)
        {
            if(FLASH_ProgramOptionByteData((u32)&(OB->WRP0), WRP0_Data) != FLASH_COMPLETE)
                return S_FAIL;
        }
        if(WRP1_Data != 0xFF)
        {
            if(FLASH_ProgramOptionByteData((u32)&(OB->WRP1), WRP1_Data) != FLASH_COMPLETE)
                return S_FAIL;
        }
        if(WRP2_Data != 0xFF)
        {
            if(FLASH_ProgramOptionByteData((u32)&(OB->WRP2), WRP2_Data) != FLASH_COMPLETE)
                return S_FAIL;
        }
        if(WRP3_Data != 0xFF)
        {
            if(FLASH_ProgramOptionByteData((u32)&(OB->WRP3), WRP3_Data) != FLASH_COMPLETE)
                return S_FAIL;
        }
      
        NVIC_SystemReset(); // Reset so Opt Bytes will take effect
        while(1);
    }
    
    return S_SUCCESS;
}
