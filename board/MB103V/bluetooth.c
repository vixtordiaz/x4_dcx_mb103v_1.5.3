/**
  **************** (C) COPYRIGHT 2011 SCT Performance, LLC *********************
  * File Name          : bluetooth.c
  * Device / System    : SCT products using the "CommonCode" platform
  * Author             : Quyen Leba
  *
  * Version            : 1 
  * Date               : 05/02/2011
  * Description        : 
  *                    : 
  *
  *
  * History            : 05/02/2011 M. Davis
  *                    :   Add SCT Copyright header,
  *
  ******************************************************************************
  */

#include <stdio.h>
#include <string.h>
#include <device_config.h>
#include <board/delays.h>
#include <board/indicator.h>
#include <board/commlink.h>
#include <fs/genfs.h>
#include <common/cmdif_func_filexfer.h>
#include <common/statuscode.h>
#include <common/crypto_blowfish.h>
#include <common/crypto_messageblock.h>
#include <common/crc32.h>
#include <common/itoa.h>
#include <board/debug.h>
#include "bluetooth.h"
#include "debugif.h"

#define BLUETOOTH_FRIENDLY_NAME_MAX_LENGTH          40
extern CommLinkInfo commlink_info;

struct
{
    u8  rxbackup[256];          //use to store last 255 bytes from BT module
    u16 rxbackupindex_wr;
    u16 rxbackupindex_rd;
    u8  reserved;
}bluetooth_info;

//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
const u8 bt11_endline[3] = {0x0D, 0x0A, 0x00};

//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------

// Link status signal from Amp'ed RF Technology BT11-MU Bluetooth Module
#define BLUETOOTH_LINK_PORT         GPIOD
#define BLUETOOTH_LINK_PORT_SRC     GPIO_PortSourceGPIOD
#define BLUETOOTH_LINK_PIN          GPIO_Pin_11
#define BLUETOOTH_LINK_PIN_SRC      GPIO_PinSource11
#define BLUETOOTH_LINK_EXTI_LINE    EXTI_Line11
#define BLUETOOTH_LINK_EXTI_CH      EXTI15_10_IRQn

// Session status signal from Amp'ed RF Technology BT11-MU Bluetooth Module
#define BLUETOOTH_SESSION_PORT      GPIOE
#define BLUETOOTH_SESSION_PORT_SRC  GPIO_PortSourceGPIOE
#define BLUETOOTH_SESSION_PIN       GPIO_Pin_9
#define BLUETOOTH_SESSION_PIN_SRC   GPIO_PinSource9
#define BLUETOOTH_SESSION_EXTI_LINE EXTI_Line9
#define BLUETOOTH_SESSION_EXTI_CH   EXTI9_5_IRQn

// CMD / Bypass signal from Amp'ed RF Technology BT11-MU Bluetooth Module
//  (CMD / Bypass state is obtained through gpio_peek_commlink_connection_status())
//#define BLUETOOTH_MODE_PORT         GPIOE
//#define BLUETOOTH_MODE_PORT_SRC     GPIO_PortSourceGPIOE
//#define BLUETOOTH_MODE_PIN          GPIO_Pin_0
//#define BLUETOOTH_MODE_PIN_SRC      GPIO_PinSource0
//#define BLUETOOTH_MODE_EXTI_LINE    EXTI_Line0
//#define BLUETOOTH_MODE_EXTI_CH      EXTI0_IRQn

//BT11.IO2
#define BLUETOOTH_BOOT_PORT         GPIOE
#define BLUETOOTH_BOOT_PIN          GPIO_Pin_8

//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
USARTBAUD bluetooth_to_uart_baud_translator(CommLinkBaudrate btbaud);
u8 bluetooth_check_operating_mode(u32 *operating_mode);

//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
void bluetooth_gpio_init()
{
    GPIO_InitTypeDef GPIO_InitStructure;

    GPIO_InitStructure.GPIO_Pin = BLUETOOTH_BOOT_PIN;
    GPIO_InitStructure.GPIO_Mode = GPIO_Mode_Out_PP;
    GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
    GPIO_SetBits(BLUETOOTH_BOOT_PORT,BLUETOOTH_BOOT_PIN);
    GPIO_Init(BLUETOOTH_BOOT_PORT, &GPIO_InitStructure);
}

//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
u8 bluetooth_status_intr_init()
{
    GPIO_InitTypeDef GPIO_InitStructure;
#if !CONFIG_FOR_TEST
    EXTI_InitTypeDef EXTI_InitStructure;
#endif

    //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    // Session
    //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    GPIO_InitStructure.GPIO_Pin = BLUETOOTH_SESSION_PIN;
    GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IPU;
    GPIO_Init(BLUETOOTH_SESSION_PORT, &GPIO_InitStructure);

#if !CONFIG_FOR_TEST
    GPIO_EXTILineConfig(BLUETOOTH_SESSION_PORT_SRC, BLUETOOTH_SESSION_PIN_SRC);
    EXTI_InitStructure.EXTI_Line = BLUETOOTH_SESSION_EXTI_LINE;
    EXTI_InitStructure.EXTI_Mode = EXTI_Mode_Interrupt;
    EXTI_InitStructure.EXTI_Trigger = EXTI_Trigger_Rising_Falling;
    EXTI_InitStructure.EXTI_LineCmd = ENABLE;
    EXTI_Init(&EXTI_InitStructure);
#endif

    //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++    
    // Link
    //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    GPIO_InitStructure.GPIO_Pin = BLUETOOTH_LINK_PIN;
    GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IPU;
    GPIO_Init(BLUETOOTH_LINK_PORT, &GPIO_InitStructure);

#if !CONFIG_FOR_TEST
    GPIO_EXTILineConfig(BLUETOOTH_LINK_PORT_SRC, BLUETOOTH_LINK_PIN_SRC);
    EXTI_InitStructure.EXTI_Line = BLUETOOTH_LINK_EXTI_LINE;
    EXTI_InitStructure.EXTI_Mode = EXTI_Mode_Interrupt;
    EXTI_InitStructure.EXTI_Trigger = EXTI_Trigger_Rising_Falling;
    EXTI_InitStructure.EXTI_LineCmd = ENABLE;
    EXTI_Init(&EXTI_InitStructure);    
#endif

    //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    // Mode... Command / Bypass (accessed through (gpio_peek_commlink_connection_status())
    //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//    GPIO_InitStructure.GPIO_Pin = BLUETOOTH_MODE_PIN;
//    GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IPU;
//    GPIO_Init(BLUETOOTH_MODE_PORT, &GPIO_InitStructure);

    return S_SUCCESS;
}

//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
void bluetooth_status_indicator_update()
{
#if CONFIG_FOR_TEST
    //do nothing
#else
    if (GPIO_ReadInputDataBit(BLUETOOTH_SESSION_PORT, BLUETOOTH_SESSION_PIN))
    {
        //active session
        commlink_update_link_status(CommLinkInfoLink_Opened);
    }
    else
    {
        //no active session
        if (GPIO_ReadInputDataBit(BLUETOOTH_LINK_PORT, BLUETOOTH_LINK_PIN))
        {
            //only bluetooth connection
            commlink_update_link_status(CommLinkInfoLink_Connected);
        }
        else
        {
            //no bluetooth connection
            commlink_update_link_status(CommLinkInfoLink_None);
        }
    }
#endif
}

//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
void bluetooth_session_intr_handler()
{
    if(EXTI_GetITStatus(BLUETOOTH_SESSION_EXTI_LINE) != RESET)
    {
        if (GPIO_ReadInputDataBit(BLUETOOTH_SESSION_PORT, BLUETOOTH_SESSION_PIN))
        {
            //Rising-edge detected
            commlink_update_link_status(CommLinkInfoLink_Opened);
            commlink_clear_halt_sending_ack();
            commlink_set_connection_history_session_open();
        }
        else
        {
            //Falling-edge detected
            if (GPIO_ReadInputDataBit(BLUETOOTH_LINK_PORT, BLUETOOTH_LINK_PIN))
            {
                //Bluetooth still connected
                commlink_update_link_status(CommLinkInfoLink_Connected);
            }
            else
            {
                //Link pin low: no connection
                commlink_update_link_status(CommLinkInfoLink_None);
            }
            commlink_set_connection_history_session_close();
        }
        EXTI_ClearITPendingBit(BLUETOOTH_SESSION_EXTI_LINE);
    }
}

//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
void bluetooth_link_intr_handler()
{
    if(EXTI_GetITStatus(BLUETOOTH_LINK_EXTI_LINE) != RESET)
    {
        if (GPIO_ReadInputDataBit(BLUETOOTH_LINK_PORT, BLUETOOTH_LINK_PIN))
        {
            //Rising-edge detected
            if (GPIO_ReadInputDataBit(BLUETOOTH_SESSION_PORT, BLUETOOTH_SESSION_PIN))
            {
                //Bluetooth connected & session active
                commlink_update_link_status(CommLinkInfoLink_Opened);
            }
            else
            {
                //Session pin low: no active session yet, only Bluetooth connection
                commlink_update_link_status(CommLinkInfoLink_Connected);
            }

            commlink_set_connection_history_session_connect();
        }
        else
        {
            //Falling-edge detected
            commlink_update_link_status(CommLinkInfoLink_None);
            commlink_set_connection_history_session_disconnect();
        }
        EXTI_ClearITPendingBit(BLUETOOTH_LINK_EXTI_LINE);
    }
}

//------------------------------------------------------------------------------
// Input:   BluetoothBaudrate btbaud
// Return:  USARTBAUD bt_uartbaud
// Engineer: Quyen Leba
//------------------------------------------------------------------------------
USARTBAUD bluetooth_to_uart_baud_translator(CommLinkBaudrate btbaud)
{
    USARTBAUD uartbaud;
    switch(btbaud)
    {
    //add other baud here
    case COMMLINK_115200_BAUD:
        uartbaud = BAUD115200;
        break;
    case COMMLINK_230400_BAUD:
        uartbaud = BAUD230400;
        break;
    case COMMLINK_460800_BAUD:
        uartbaud = BAUD460800;
        break;
    case COMMLINK_921600_BAUD:
        uartbaud = BAUD921600;
        break;
    default:
        uartbaud = BAUD115200;
        break;
    }
    return uartbaud;
}

//------------------------------------------------------------------------------
// Initialize Bluetooth
// Input:   BluetoothBaudrate btbaud (a preferred baudrate)
// Return:  u8  status
// Engineer: Quyen Leba
//------------------------------------------------------------------------------
u8 bluetooth_init()
{
//    USARTBAUD bt_uartbaud;
    u32 operating_mode;
    u32  bt_connection;
    u8  status;

    bluetooth_gpio_init();
    bluetooth_hard_reset(FALSE);
    
    bluetooth_hal(init)(BAUD115200,UART_NO_FLOWCONTROL,UART_USE_INTERRUPT);
    
#if CONFIG_FOR_TEST
    //do nothing
#else
    status = bluetooth_check_operating_mode(&operating_mode);
    if (status == S_SUCCESS)
    {
        switch(operating_mode)
        {
        case AB_FAILSAFE_BOOTLOADER_OPERATING_MODE:
        case AB_MAIN_BOOTLOADER_OPERATING_MODE:
            bootloader_ab_setoperatingmode(operating_mode);
            break;
        case AB_APPLICATION_OPERATING_MODE:
        default:
            bluetooth_hal(init)(BAUD115200,UART_USE_FLOWCONTROL,UART_USE_INTERRUPT);            
            bootloader_ab_setoperatingmode(AB_APPLICATION_OPERATING_MODE);
            
            status = bluetooth_to_command_mode();
            if (status == S_SUCCESS)
            {
                bluetooth_enable_autoreconnect(BLUETOOTH_AUTORECONNECT_TIME);
                
                bluetooth_setcommlinkbaudrate();
                
                // we're in command mode... if there is a valid session, return to bypass
                bt_connection = gpio_peek_commlink_connection_status();
                
                if (bt_connection & COMMLINK_SESSION_BIT_MASK)
                    bluetooth_to_bypass_mode();                
            }
            
            break;
        }
    }
    else
    {
        bluetooth_hal(init)(BAUD115200,UART_USE_FLOWCONTROL,UART_USE_INTERRUPT);
        bootloader_ab_setoperatingmode(0);
    }
#endif

    bluetooth_status_intr_init();
    bluetooth_status_indicator_update();

    return status;
}

//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
u8 bluetooth_setcommlinkbaudrate(void)
{
    if(commlink_info.baudrate != COMMLINK_115200_BAUD)
    {
        return bluetooth_changebaud_btmodule(commlink_info.baudrate);
    }
    return S_SUCCESS;
}


//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
void bluetooth_changebaudrate(CommLinkBaudrate commlinkbaud)
{
    USARTBAUD bt_uartbaud;

    if ((u32)commlinkbaud > (u32)sizeof(BluetoothBaudrateTable))
    {
        bt_uartbaud = (USARTBAUD)(BluetoothBaudrateTable[7]);   //115200
    }
    else
    {
        bt_uartbaud = (USARTBAUD)(BluetoothBaudrateTable[(u32)commlinkbaud]);
    }

    bluetooth_hal(changebaudrate)(bt_uartbaud);
}

//------------------------------------------------------------------------------
// Changes the Bluetooth Module Baudrate untill next reset
// Input:   BluetoothBaudrate btbaud
// Return   u8  status
// Engineer: Patrick Downs
//------------------------------------------------------------------------------
u8 bluetooth_changebaud_btmodule(CommLinkBaudrate btbaud)
{
    const u8 bt11_changebaudrate[] = "AT+AB ChangeBaud ";
    const u8 bt11_changebaudrate_response[] = "AT-AB Baudrate Changed";
    char  newbaud[10];
    u8 buffer[256];
    u8  *bptr;
    u8  status;

    memset(newbaud, 0, sizeof(newbaud));
    switch(btbaud)
    {
    case COMMLINK_2400_BAUD:
        strcpy(newbaud, "2400");
        break;
    case COMMLINK_4800_BAUD:
        strcpy(newbaud, "4800");
        break;
    case COMMLINK_7200_BAUD:
        strcpy(newbaud, "7200");
        break;
    case COMMLINK_9600_BAUD:
        strcpy(newbaud, "9600");
        break;
    case COMMLINK_19200_BAUD:
        strcpy(newbaud, "19200");
        break;   
    case COMMLINK_38400_BAUD:
        strcpy(newbaud, "38400");
        break;   
    case COMMLINK_57600_BAUD:
        strcpy(newbaud, "57600");
        break;   
    case COMMLINK_115200_BAUD:
        strcpy(newbaud, "115200");
        break;   
    case COMMLINK_230400_BAUD:
        strcpy(newbaud, "230400");
        break;   
    case COMMLINK_460800_BAUD:
        strcpy(newbaud, "460800");
        break;
    case COMMLINK_921600_BAUD:
        strcpy(newbaud, "921600");
        break;
    default:
        // unsupported baud rate
        return S_FAIL;
    }
    
    bluetooth_reset_rxbackup();
    
    bluetooth_uart_tx((u8*)bt11_changebaudrate,strlen((char*)bt11_changebaudrate),FALSE);
    bluetooth_uart_tx((u8*)newbaud,strlen((char*)newbaud),FALSE);
    bluetooth_uart_tx((u8*)bt11_endline,2,FALSE);
    delays(100,'m');
    bluetooth_get_rxbackup(buffer, sizeof(buffer));
    buffer[255] = NULL;
    bptr = (u8*)strstr((char*)buffer,(char*)bt11_changebaudrate_response);
    if (!bptr)
    {
        status = S_FAIL;
    }
    else
    {
        status = S_SUCCESS;
        bluetooth_changebaudrate(btbaud);
    }
    
    return status;
}

//-----------------------------------------
//DO NOT USE for changing Baud rate PD 07/03/2013
//We do not permantely change the baud rate to prevent any problems
//with BT11 bootloader baud handling
//-----------------------------------------
//------------------------------------------------------------------------------
// Changes the Default Bluetooth Module Baudrate. This requires a module RESET.
// Input:   BluetoothBaudrate btbaud
// Return   u8  status
// Engineer: Patrick Downs
//------------------------------------------------------------------------------
/*
u8 bluetooth_changedefaultbaud_btmodule(CommLinkBaudrate btbaud)
{
    const u8 bt11_changebaudrate[] = "AT+AB ChangeDefaultBaud ";
    const u8 bt11_changebaudrate_response[] = "AT-AB Baudrate Changed";
    char  newbaud[10];
    u8 buffer[256];
    u8  *bptr;
    u8  status;

    memset(newbaud, 0, sizeof(newbaud));
    switch(btbaud)
    {
    case COMMLINK_2400_BAUD:
        strcpy(newbaud, "2400");
        break;
    case COMMLINK_4800_BAUD:
        strcpy(newbaud, "4800");
        break;
    case COMMLINK_7200_BAUD:
        strcpy(newbaud, "7200");
        break;
    case COMMLINK_9600_BAUD:
        strcpy(newbaud, "9600");
        break;
    case COMMLINK_19200_BAUD:
        strcpy(newbaud, "19200");
        break;   
    case COMMLINK_38400_BAUD:
        strcpy(newbaud, "38400");
        break;   
    case COMMLINK_57600_BAUD:
        strcpy(newbaud, "57600");
        break;   
    case COMMLINK_115200_BAUD:
        strcpy(newbaud, "115200");
        break;   
    case COMMLINK_230400_BAUD:
        strcpy(newbaud, "230400");
        break;   
    case COMMLINK_460800_BAUD:
        strcpy(newbaud, "460800");
        break;
    case COMMLINK_921600_BAUD:
        strcpy(newbaud, "921600");
        break;
    default:
        // unsupported baud rate
        return S_FAIL;
    }
    
    bluetooth_reset_rxbackup();
    
    bluetooth_uart_tx((u8*)bt11_changebaudrate,strlen((char*)bt11_changebaudrate),FALSE);
    bluetooth_uart_tx((u8*)newbaud,strlen((char*)newbaud),FALSE);
    bluetooth_uart_tx((u8*)bt11_endline,2,FALSE);
    delays(100,'m');
    bluetooth_get_rxbackup(buffer, sizeof(buffer));
    buffer[255] = NULL;
    bptr = (u8*)strstr((char*)buffer,(char*)bt11_changebaudrate_response);
    if (!bptr)
    {
        status = S_FAIL;
    }
    else
    {
        status = S_SUCCESS;
        
        bluetooth_hard_reset(TRUE);
        bluetooth_changebaudrate(btbaud);
    }
        
    return status;
}
*/

//------------------------------------------------------------------------------
// Changes the Default Bluetooth Module Baudrate. This requires a module RESET.
// Input:   BluetoothBaudrate btbaud
// Return   u8  status
// Engineer: Patrick Downs
//------------------------------------------------------------------------------
u8 bluetooth_force115kBaud_btmodule(void)
{
    u8 status = S_SUCCESS;

    gpio_force115kBaud_app_board(TRUE);
    gpio_reset_app_board();
    bluetooth_changebaudrate(COMMLINK_115200_BAUD);
    return status;
}

//------------------------------------------------------------------------------
// Input:   u8 data
//------------------------------------------------------------------------------
void bluetooth_uart_tx_byte(u8 data)
{
    bluetooth_hal(tx)(data);
}

//------------------------------------------------------------------------------
// Output:  u8  *data
// Return:  u8  status
//------------------------------------------------------------------------------
u8 bluetooth_uart_rx_byte(u8 *data)
{
    u32 i = 0x7FF;
    while(i--)
    {
        if(bluetooth_hal(hasdata)())
        {
            *data = bluetooth_hal(rx)();
            return S_SUCCESS;
        }
    }
    return S_TIMEOUT;
}

//------------------------------------------------------------------------------
// Reset the rxbackup. This buffer stores last 255 bytes of data from Bluetooth
// module
// Engineer: Quyen Leba
//------------------------------------------------------------------------------
void bluetooth_reset_rxbackup()
{
    memset((char*)bluetooth_info.rxbackup,0,sizeof(bluetooth_info.rxbackup));
    bluetooth_info.rxbackupindex_rd = 0;
    bluetooth_info.rxbackupindex_wr = 0;
}

//------------------------------------------------------------------------------
// Get data from rxbackup; last (max)255 bytes of data from Bluetooth module
// Output:  u8  *returndata
// Engineer: Quyen Leba, Patrick Downs
//------------------------------------------------------------------------------
int bluetooth_get_rxbackup(u8 *returndata, u32 returndata_maxsize)
{
    u32 length;
    u16 rindex;
    u16 windex;
    
    if(bluetooth_info.rxbackupindex_rd == bluetooth_info.rxbackupindex_wr)
        return 0; // No new data
    
    rindex = bluetooth_info.rxbackupindex_rd;
    windex = bluetooth_info.rxbackupindex_wr;
    
    if(windex>rindex)
    {
        length = windex - rindex;
        if(length>returndata_maxsize)
            length = returndata_maxsize;
            
        memcpy((char*)returndata,(char*)(bluetooth_info.rxbackup+rindex),length);
        bluetooth_info.rxbackupindex_rd+=length;
    }
    else
    {
        length = sizeof(bluetooth_info.rxbackup) - rindex;
        if(length>returndata_maxsize)
        {
            length = returndata_maxsize;
            memcpy((char*)returndata,(char*)(bluetooth_info.rxbackup+rindex),length);
            bluetooth_info.rxbackupindex_rd += length;
        }
        else
        {
            memcpy((char*)returndata,(char*)(bluetooth_info.rxbackup+rindex),length);
            if(windex>(returndata_maxsize-length))
            {
                memcpy((char*)returndata+length,(char*)(bluetooth_info.rxbackup),(returndata_maxsize-length));
                length += (returndata_maxsize-length);
                bluetooth_info.rxbackupindex_rd = (returndata_maxsize-length);
            }
            else
            {
                memcpy((char*)returndata+length,(char*)(bluetooth_info.rxbackup),windex);
                length += windex;
                bluetooth_info.rxbackupindex_rd = windex;
            }
        }
    }
    
    return length;
}

//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
void bluetooth_uart_rx_irq_handler()
{
    u8  in;
    
    if(bluetooth_hal(getitstatus)() != RESET)
    {
        in = bluetooth_hal(rx)();

        //-------------------------------------
        // Special Bluetooth Modes
        //------------------------------------- 
        if(commlink_info.passthrough)
        {
            debugif_out((u8*)&in, 1);
            return;
        }
        else if(commlink_info.loopback)
        {
            bluetooth_hal(tx)(in);
            return;
        }
        //-------------------------------------

        if (bluetooth_info.rxbackupindex_wr >= sizeof(bluetooth_info.rxbackup))
        {
            bluetooth_info.rxbackupindex_wr = 0;
        }
        bluetooth_info.rxbackup[bluetooth_info.rxbackupindex_wr++] = in;
        
        commlink_process_incoming_command_byte(in);
    }
}

//------------------------------------------------------------------------------
// Put Bluetooth module in command mode
// Return   u8  status
// Engineer: Quyen Leba
//------------------------------------------------------------------------------
u8 bluetooth_to_command_mode()
{
    u32 bt_connection;
    u8  retry;

    bt_connection = gpio_peek_commlink_connection_status();
    if ((bt_connection & 0x08) == 0)
    {
        bluetooth_uart_tx("^#^$^%",6,FALSE);
        retry = 50;
        while(gpio_peek_commlink_connection_status() & 0x08 == 0)
        {
            delays(10,'m');
            if (retry--)
            {
                return S_TIMEOUT;
            }
        }
        delays(1000,'m');
    }
    return S_SUCCESS;
}

//------------------------------------------------------------------------------
// Put Bluetooth module in bypass mode
// Return   u8  status
// Engineer: Quyen Leba
//------------------------------------------------------------------------------
u8 bluetooth_to_bypass_mode()
{
    const u8 bt11_bypass[] = "at+ab bypass";

    bluetooth_uart_tx((u8*)bt11_bypass,strlen((char*)bt11_bypass),FALSE);
    bluetooth_uart_tx((u8*)bt11_endline,2,FALSE);

    return S_SUCCESS;
}

//------------------------------------------------------------------------------
// Enable Bluetooth autoreconnect
// Input:   u8  reconnecttime (in sec)
// Return   u8  status
// Engineer: Quyen Leba
// Note: need to be in command mode first
//------------------------------------------------------------------------------
u8 bluetooth_enable_autoreconnect(u8 reconnecttime)
{
    const u8 bt11_autoconn[] = "at+ab autoreconnectsetup ";
    u8  buffer[32];
    
    itoa(reconnecttime,buffer,10);
    bluetooth_uart_tx((u8*)bt11_autoconn,strlen((char*)bt11_autoconn),FALSE);
    bluetooth_uart_tx((u8*)buffer,strlen((char*)buffer),FALSE);
    bluetooth_uart_tx((u8*)bt11_endline,2,FALSE);
    delays(100,'m');
    
    return S_SUCCESS;
}


//------------------------------------------------------------------------------
// Enable Bluetooth module Flow Control
// Input:   bool enable
// Return   u8  status
// Engineer: Patrick Downs
//------------------------------------------------------------------------------
u8 bluetooth_set_flowcontrol(bool enable)
{   
    u8 status;
    
    if(enable)
    {
        // Enable Flow Control on module, means disable StreamingSerial
        status = bluetooth_set_config_var(VAR_StreamingSerial, "FALSE");
    }
    else
    {
        status = bluetooth_set_config_var(VAR_StreamingSerial, "TRUE");   
    }
    
    return status;
}

//------------------------------------------------------------------------------
// Set BT module CONFIG var#
// Input:   u8 var_number - Varible number ex Var04 = 04
//          u8* inputstring - String to set the variable to
// Return   u8  status
// Engineer: Patrick Downs
//------------------------------------------------------------------------------
u8 bluetooth_set_config_var(u8 var_number, u8* inputstring)
{
    const u8 bt11_config_command[] = "at+ab config var";
    const u8 bt11_config_response[] = "AT-AB ConfigOk";
    u8  buffer[256];
    u8  *bptr;
    u8 status;
    
    if(strlen((char const*)inputstring) > (sizeof(buffer)-(sizeof(bt11_config_command)+
                        sizeof(bt11_config_response)+sizeof(bt11_endline))))
    {
        return S_FAIL;
    }
    
    memset(buffer, 0, sizeof(buffer));
    sprintf((char*)buffer, "%s%02d=%s%s", bt11_config_command, var_number, inputstring, 
            bt11_endline);
    
    bluetooth_reset_rxbackup();
    bluetooth_uart_tx((u8*)buffer,strlen((char*)buffer),FALSE);
    delays(100,'m');
    bluetooth_get_rxbackup(buffer,sizeof(buffer));
    buffer[255] = NULL;
    bptr = (u8*)strstr((char*)buffer,(char*)bt11_config_response);
    if (!bptr)
    {
        status = S_FAIL;
    }
    else
    {
        status = S_SUCCESS;
    }
    
    return status;
}

//------------------------------------------------------------------------------
// Get BT module CONFIG var#
// Input:   u8 var_number - Varible number ex Var04 = 04, 0x00 is dump all
//          u8* outputstring - Buffer to return variable data
//          u32 output_maxlength - Max size the return buffer can hold
// Return   u8  status
// Engineer: Patrick Downs
//------------------------------------------------------------------------------
#define BT_RX_TIMEOUT 72000
u8 bluetooth_get_config_var(u8 var_number, u8* outputstring, u32 output_maxlength)
{
    u8 buffer[256];
    int length;
    int index;
    int timeout;
    u8 status;
    
    index = 0;
    timeout = BT_RX_TIMEOUT;
    memset(buffer, 0, sizeof(buffer));
    
    if(var_number)
    {
        sprintf((char*)buffer, "at+ab config var%02d%s", var_number, bt11_endline);
    }
    else // 0x00 means dump all var data
    {
        sprintf((char*)buffer, "at+ab config%s", bt11_endline);
    }   
    
    bluetooth_reset_rxbackup();
    bluetooth_uart_tx((u8*)buffer,strlen((char*)buffer),FALSE);
    while(timeout)
    {
         length = bluetooth_get_rxbackup((u8*)(outputstring+index), 
                                                   (u32)(output_maxlength-index));
         if(length)
         {
             timeout = BT_RX_TIMEOUT;
             index += length;             
         }
         else
         {
             timeout--;
         }
    }
    
    if((index == 0) && (timeout == 0))
    {
        status = S_FAIL;
    }
    else
    {
        status = S_SUCCESS;
    }
    
    return status;
}

//------------------------------------------------------------------------------
// Set BT module Accessory Name
// Input:   u8* name 
// Return   u8  status
// Engineer: Patrick Downs
//------------------------------------------------------------------------------
u8 bluetooth_set_AccName(u8* name)
{
    u8 status;
    
    status = bluetooth_set_config_var(VAR_AccName, name);
    
    return status;
}

//------------------------------------------------------------------------------
// Set BT module Accessory Manufacturer
// Input:   u8* manufacturer
// Return   u8  status
// Engineer: Patrick Downs
//------------------------------------------------------------------------------
u8 bluetooth_set_AccManufacturer(u8* manufacturer)
{
    u8 status;
    
    status = bluetooth_set_config_var(VAR_AccManufacturer, manufacturer);
    
    return status;
}

//------------------------------------------------------------------------------
// Set BT module Accessory Model Number
// Input:   u8* modelnumber
// Return   u8  status
// Engineer: Patrick Downs
//------------------------------------------------------------------------------
u8 bluetooth_set_AccModelNumber(u8* modelnumber)
{
    u8 status;
    
    status = bluetooth_set_config_var(VAR_AccModelNumber, modelnumber);
    
    return status;
}

//------------------------------------------------------------------------------
// Set BT module Accessory Serial Number
// Input:   bool enable - 
// Return   u8  status
// Engineer: Patrick Downs
//------------------------------------------------------------------------------
u8 bluetooth_set_AccSerialNumber(u8* serialnumber)
{
    u8 status;
    
    status = bluetooth_set_config_var(VAR_AccSerialNumber, serialnumber);
    
    return status;
}

//------------------------------------------------------------------------------
// Disable Bluetooth autoreconnect
// Return   u8  status
// Engineer: Quyen Leba
// Note: need to be in command mode first
//------------------------------------------------------------------------------
u8 bluetooth_disable_autoreconnect()
{
    const u8 bt11_disableautoconn[] = "at+ab disableautoreconnect";

    bluetooth_uart_tx((u8*)bt11_disableautoconn,strlen((char*)bt11_disableautoconn),FALSE);
    bluetooth_uart_tx((u8*)bt11_endline,2,FALSE);
    delays(100,'m');

    return S_SUCCESS;
}

//------------------------------------------------------------------------------
// Erase Bluetooth bondtable
// Return   u8  status
// Engineer: Quyen Leba
// Note: need to be in command mode first
//------------------------------------------------------------------------------
u8 bluetooth_erase_bondtable()
{
    const u8 bt11_erasebondtable[] = "at+ab erasebondtable";

    bluetooth_uart_tx((u8*)bt11_erasebondtable,strlen((char*)bt11_erasebondtable),FALSE);
    bluetooth_uart_tx((u8*)bt11_endline,2,FALSE);
    delays(100,'m');

    return S_SUCCESS;
}

//------------------------------------------------------------------------------
// Soft reset Bluetooth module
// Return   u8  status
// Engineer: Quyen Leba
// Note: need to be in command mode first
//------------------------------------------------------------------------------
//------------------------------------------------------------------------------
// IMPORTANT!!
//  After any reset, you will need to reconfigure the baud rate if it's 
//  something other than 115kbaud.
//------------------------------------------------------------------------------
u8 bluetooth_soft_reset(bool runcheck)
{
    const u8 bt11_reset[] = "at+ab reset";
    const u8 bt11_bda_response[] = "BDAddress";
    u8  buffer[256];
    u32 retry;

    bluetooth_uart_tx((u8*)bt11_reset,strlen((char*)bt11_reset),FALSE);
    bluetooth_uart_tx((u8*)bt11_endline,2,FALSE);
    delays(500,'m');

    if (runcheck)
    {
        retry = 50;     //about 5sec
        while(retry--)
        {
            bluetooth_get_rxbackup(buffer, sizeof(buffer));
            buffer[255] = NULL;
            if (strstr((char*)buffer,(char*)bt11_bda_response))
            {
                return S_SUCCESS;
            }
            else
            {
                delays_counter(DELAYS_COUNTER_100MS);
            }
        }
        return S_TIMEOUT;
    }
    return S_SUCCESS;
}

//------------------------------------------------------------------------------
// Hard reset Bluetooth module
// Return   u8  status
// Engineer: Mark Davis
// toggle BT11MU Bluetooth module RESET input
//------------------------------------------------------------------------------
//------------------------------------------------------------------------------
// IMPORTANT!!
//  After any reset, you will need to reconfigure the baud rate if it's 
//  something other than 115kbaud.
//------------------------------------------------------------------------------
u8 bluetooth_hard_reset(bool runcheck)
{
    const u8 bt11_bda_response[] = "BDAddress";
    u8  buffer[256];
    u32 retry;

    gpio_reset_app_board();

    if (runcheck)
    {
        retry = 50;     //about 5sec
        while(retry--)
        {
            bluetooth_get_rxbackup(buffer, sizeof(buffer));
            buffer[255] = NULL;
            if (strstr((char*)buffer,(char*)bt11_bda_response))
            {
                return S_SUCCESS;
            }
            else
            {
                delays_counter(DELAYS_COUNTER_100MS);
            }
        }
        return S_TIMEOUT;
    }

    return S_SUCCESS;
}

//------------------------------------------------------------------------------
// Enter BT11 bootloader
// Return   u8  status
// Engineer: Quyen Leba
//------------------------------------------------------------------------------
u8 bluetooth_enter_bootloader()
{
    u32 retry;

    bluetooth_reset_rxbackup();

    bluetooth_hal(init)(BAUD115200,UART_NO_FLOWCONTROL,UART_USE_INTERRUPT);

    GPIO_ResetBits(BLUETOOTH_BOOT_PORT,BLUETOOTH_BOOT_PIN);
    bluetooth_hard_reset(FALSE);
    retry = 10;
    while(retry--)
    {
        if (strstr((char*)bluetooth_info.rxbackup,"Waiting for"))   //Waiting for the file to be sent
        {
            break;
        }//Application has not been downloaded
        else if (strstr((char*)bluetooth_info.rxbackup,"Enter: "))
        {
            break;
        }
        delays_counter(DELAYS_COUNTER_100MS);
    }
    
    delays_counter(DELAYS_COUNTER_100MS);
    bluetooth_reset_rxbackup();
    bluetooth_uart_tx("1",1,FALSE);
    delays_counter(DELAYS_COUNTER_100MS);
    delays_counter(DELAYS_COUNTER_100MS);
    delays_counter(DELAYS_COUNTER_100MS);
    delays_counter(DELAYS_COUNTER_100MS);
    delays_counter(DELAYS_COUNTER_100MS);
    bluetooth_hal(disable_interrupt)();
    //Waiting for the file to be sent ... (press 'a' to abort)
    if (strstr((char*)bluetooth_info.rxbackup,"to abort"))
    {
        u8  c;
        u32 retry;
        
        retry = 3000;
        while(retry--)
        {
            if (bluetooth_uart_rx_byte(&c) == S_SUCCESS)
            {
                if (c == 'C')
                {
                    break;
                }
            }
            delays_counter(DELAYS_COUNTER_10MS);
        }
        if (retry == 0)
        {
            return S_TIMEOUT;
        }
    }
    else
    {
        return S_FAIL;
    }

    return S_SUCCESS;
}

//------------------------------------------------------------------------------
// Exit BT11 bootloader
// Return   u8  status
// Engineer: Quyen Leba
//------------------------------------------------------------------------------
u8 bluetooth_exit_bootloader()
{
    bluetooth_hal(init)(BAUD115200,UART_USE_FLOWCONTROL,UART_USE_INTERRUPT);
    GPIO_SetBits(BLUETOOTH_BOOT_PORT,BLUETOOTH_BOOT_PIN);
    bluetooth_hard_reset(TRUE);
    
    //per AmpedRF, one extra reset after reflash is needed
    delays_counter(DELAYS_COUNTER_100MS);
    delays_counter(DELAYS_COUNTER_100MS);
    delays_counter(DELAYS_COUNTER_100MS);
    delays_counter(DELAYS_COUNTER_100MS);
    delays_counter(DELAYS_COUNTER_100MS);
    delays_counter(DELAYS_COUNTER_100MS);
    delays_counter(DELAYS_COUNTER_100MS);
    delays_counter(DELAYS_COUNTER_100MS);
    delays_counter(DELAYS_COUNTER_100MS);
    delays_counter(DELAYS_COUNTER_100MS);
    
    bluetooth_init();
    
    return S_SUCCESS;
}

//------------------------------------------------------------------------------
// Unpair Bluetooth
// Return   u8  status
// Engineer: Quyen Leba
//------------------------------------------------------------------------------
u8 bluetooth_unpair()
{
    const u8 bt11_iapdisconnect[] = "at+ab iapdisconnect";
    u8  status;
                
    status = bluetooth_to_command_mode();
    if (status != S_SUCCESS)
    {
        return status;
    }

    bluetooth_disable_autoreconnect();

    bluetooth_uart_tx((u8*)bt11_iapdisconnect,strlen((char*)bt11_iapdisconnect),FALSE);
    bluetooth_uart_tx((u8*)bt11_endline,2,FALSE);
    delays(100,'m');

    bluetooth_erase_bondtable();
    
    bluetooth_enable_autoreconnect(BLUETOOTH_AUTORECONNECT_TIME);
    
    bluetooth_to_bypass_mode();
    
    return S_SUCCESS;
}

//------------------------------------------------------------------------------
// Get Bluetooth MAC address
// Output:  u8  *MACaddress (in ASCII; e.g. [00:11:22:33:44:55][NULL])
// Return   u8  status
// Engineer: Quyen Leba
//------------------------------------------------------------------------------
u8 bluetooth_getmacaddress(u8 *MACaddress)
{
//    const u8 bt11_getmac[] = "at+ab getbdaddress";
    const u8 bt11_getmac_response[] = "BD_ADDR = ";
    u8  buffer[256];
    u8  *bptr;
    u32 i;
    u8  status;
    
    status = bluetooth_get_config_var(VAR_BD_ADDR, buffer, sizeof(buffer));

    if(status == S_SUCCESS)
    {
        bptr = (u8*)strstr((char*)buffer,(char*)bt11_getmac_response);
        if (bptr && (sizeof(bt11_getmac_response)-1 + 2*6+2 == strlen((char*)buffer)))
        {
            //response data: ["BD_ADDR = 001122334455"][0x0D][0x0A][NULL]
            bptr += sizeof(bt11_getmac_response)-1;
            for(i=0;i<6;i++)
            {
                MACaddress[i*3+0] = *bptr++;
                MACaddress[i*3+1] = *bptr++;
                MACaddress[i*3+2] = ':';
            }
            MACaddress[(i-1)*3+2] = NULL;
            status = S_SUCCESS;
        }
        else
        {
            status = S_FAIL;
        }
    }
    
    return status;
}

//------------------------------------------------------------------------------
// Set Bluetooth MAC address
// Input:   u8  *MACaddress (in ASCII; e.g. [00:11:22:33:44:55][NULL])
// Return   u8  status
// Engineer: Quyen Leba
//------------------------------------------------------------------------------
u8 bluetooth_setmacaddress(u8 *MACaddress)
{
    return S_NOTSUPPORT;
}

//------------------------------------------------------------------------------
// Get Bluetooth friendly name
// Output:  u8  *friendlyname (NULL terminated)
// Return   u8  status
// Engineer: Quyen Leba
//------------------------------------------------------------------------------
u8 bluetooth_getfriendlyname(u8 *friendlyname)
{
    const u8 bt11_changename_response[] = "var04 DeviceName";
    u8  buffer[256];
    u8  *bptr;
    u32 index;
    u8  status;
    
    status = bluetooth_to_command_mode();
    if (status != S_SUCCESS)
    {
        return status;
    }
    
    status = bluetooth_get_config_var(VAR_DeviceName, buffer, sizeof(buffer));
    if(status == S_SUCCESS)
    {
        status = S_FAIL;
        bptr = (u8*)strstr((char*)buffer,(char*)bt11_changename_response);
        if (bptr)
        {
            bptr = (u8*)strstr((char*)buffer,"=");
            if (bptr)
            {
                index = 0;
                
                bptr += 2;  //skip space and '=' char
                while(index < BLUETOOTH_FRIENDLY_NAME_MAX_LENGTH &&
                      *bptr != NULL && *bptr != 0x0A && *bptr != 0x0D)
                {
                    friendlyname[index++] = *bptr++;
                }
                friendlyname[index] = NULL;
                status = S_SUCCESS;
            }
        }
    }
    
    bluetooth_to_bypass_mode();
    
    return status;
}

//------------------------------------------------------------------------------
// Set Bluetooth friendly name
// Input:   u8  *friendlyname (NULL terminated)
// Return   u8  status
// Engineer: Quyen Leba
//------------------------------------------------------------------------------
u8 bluetooth_setfriendlyname(u8 *friendlyname)
{
    u8  status;
    
    status = bluetooth_to_command_mode();
    if (status != S_SUCCESS)
    {
        return status;
    }
    
    status = bluetooth_set_config_var(VAR_DeviceName, friendlyname);

    bluetooth_to_bypass_mode();
    
    return status;
}

//------------------------------------------------------------------------------
// Set bluetooth module to SCT factory default settings
// Return   u8  status
// Engineer: Quyen Leba, Patrick Downs
//------------------------------------------------------------------------------
u8 bluetooth_set_factory_default()
{
    u8  friendlyname[64] = "SCT iTSX ";
    u8  mac[32];
    u8  status;

    status = bluetooth_getmacaddress(mac);
    if (status == S_SUCCESS)
    {
        //mac: 00:11:22:33:44:55
        strcat((char*)friendlyname,(char*)&mac[9]);
        bluetooth_setfriendlyname(friendlyname);
    }

    bluetooth_unpair();
    return S_SUCCESS;
}

//------------------------------------------------------------------------------
// Get Bluetooth module version string
// Output:  u8  *versionstr (NULL terminated)
// Return   u8  status
// Engineer: Quyen Leba
//------------------------------------------------------------------------------
u8 bluetooth_getversionstring(u8 *versionstr)
{
    const u8 bt11_build[] = "at+ab build";
    const u8 bt11_build_response[] = "Build Version";
    u8  buffer[256];
    u8  *bptr;
    u8  status;

    bluetooth_reset_rxbackup();
    status = bluetooth_to_command_mode();
    if (status != S_SUCCESS)
    {
        return status;
    }

    bluetooth_uart_tx((u8*)bt11_build,strlen((char*)bt11_build),FALSE);
    bluetooth_uart_tx((u8*)bt11_endline,2,FALSE);
    delays(100,'m');
    bluetooth_to_bypass_mode();

    bluetooth_get_rxbackup(buffer, sizeof(buffer));
    buffer[255] = NULL;
    status = S_FAIL;
    bptr = (u8*)strstr((char*)buffer,(char*)bt11_build_response);
    if (bptr)
    {
        bptr = (u8*)strstr((char*)buffer,"Version");
        if (bptr)
        {
            u32 index = 0;
            
            bptr += 8;  //skip "Version" and space char
            while(*bptr != ' ' && *bptr != 0 && index < 16)
            {
                versionstr[index++] = *bptr++;
            }
            versionstr[index] = NULL;
            status = S_SUCCESS;
        }
    }

    return status;
}

//------------------------------------------------------------------------------
// Check Bluetooth operating mode. Only accessible right after module reset
// Output:  u32 *mode
// Return   u8  status
// Engineer: Quyen Leba
//------------------------------------------------------------------------------
u8 bluetooth_check_operating_mode(u32 *operating_mode)
{
    u32  bt_connection;
    const u8 bt11_err[] = "QQ";
    const u8 bt11_err_response[] = "ErrFormat";
    const u8 bt11_inv_response[] = "Invalid";
    const u8 bt11_bda_response[] = "BDAddress";
    u8  buffer[256];
    s32 retry;
    u8  status;

    retry = 30;     //about 3sec max
    while(retry--)
    {
        bluetooth_get_rxbackup(buffer, sizeof(buffer));
        buffer[255] = NULL;
        if (strstr((char*)buffer,(char*)bt11_bda_response))
        {
            *operating_mode = AB_APPLICATION_OPERATING_MODE;
            goto bluetooth_check_operating_mode_done;
        }
        else
        {
            delays_counter(DELAYS_COUNTER_100MS);
        }
    }

    bluetooth_reset_rxbackup();
    status = bluetooth_to_command_mode();
    if (status != S_SUCCESS)
    {
        *operating_mode = 0;
        return status;
    }

    retry = 30;     //about 3sec max
    while(--retry)
    {
        bluetooth_uart_tx((u8*)bt11_err,strlen((char*)bt11_err),FALSE);
        bluetooth_uart_tx((u8*)bt11_endline,2,FALSE);
        delays_counter(DELAYS_COUNTER_100MS);

        bluetooth_get_rxbackup(buffer, sizeof(buffer));
        buffer[255] = NULL;

        if (strstr((char*)buffer,(char*)bt11_err_response))
        {
            *operating_mode = AB_APPLICATION_OPERATING_MODE;
            break;
        }
        else if (strstr((char*)buffer,(char*)bt11_inv_response))
        {
            *operating_mode = AB_FAILSAFE_BOOTLOADER_OPERATING_MODE;
            break;
        }
        bluetooth_reset_rxbackup();
    }

    if (retry == 0)
    {
        return S_TIMEOUT;
    }

bluetooth_check_operating_mode_done:
    // if there is a valid session and not already in Bypass mode, return to bypass mode
    bt_connection = gpio_peek_commlink_connection_status();
  
    if ((bt_connection & COMMLINK_SESSION_BIT_MASK) && (bt_connection & COMMLINK_MODE_BIT_MASK))
    {
        bluetooth_to_bypass_mode();
        
        // Allow time for serial output to complete
        delays(5,'m');
        
    }
    return S_SUCCESS;
}
