/**
  **************** (C) COPYRIGHT 2011 SCT Performance, LLC *********************
  * File Name          : usartA.c
  * Device / System    : SCT products using the "CommonCode" platform
  * Author             : Quyen Leba
  *
  * Version            : 1 
  * Date               : 05/02/2011
  * Description        : 
  *                    : 
  *
  *
  * History            : 05/02/2011 M. Davis
  *                    :   Add SCT Copyright header,
  *
  ******************************************************************************
  */

#include "usartA.h"

#define USARTA_TX      GPIO_Pin_5
#define USARTA_RTS     GPIO_Pin_4
#define USARTA_RX      GPIO_Pin_6
#define USARTA_CTS     GPIO_Pin_3

//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
void usartA_init(USARTBAUD baud, bool useFlowControl, bool useInterrupt)
{
    USART_InitTypeDef USART_InitStructure;
    GPIO_InitTypeDef GPIO_InitStructure;

    // Enable usartA clocks
    RCC_APB1PeriphClockCmd(RCC_APB1Periph_USART2, ENABLE);

    GPIO_PinRemapConfig(GPIO_Remap_USART2, ENABLE);
    // Configure usartA RTS and usartA Tx as alternate function push-pull
    GPIO_InitStructure.GPIO_Pin = USARTA_TX | USARTA_RTS;
    GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
    GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AF_PP;
    GPIO_Init(GPIOD, &GPIO_InitStructure);

    // Configure usartA CTS as input pull down
    GPIO_InitStructure.GPIO_Pin = USARTA_CTS;
    GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IPD;
    GPIO_Init(GPIOD, &GPIO_InitStructure);
    // Configure usartA Rx as input floating
    GPIO_InitStructure.GPIO_Pin = USARTA_RX;
    GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IN_FLOATING;
    GPIO_Init(GPIOD, &GPIO_InitStructure);
    
    if (!useFlowControl)
    {
        //config RTS & CTS as pull-down inputs
        GPIO_InitStructure.GPIO_Pin = USARTA_CTS;
        GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IPD;
        GPIO_Init(GPIOD, &GPIO_InitStructure);

        GPIO_InitStructure.GPIO_Pin = USARTA_RTS;
        GPIO_InitStructure.GPIO_Mode = GPIO_Mode_Out_PP;
        GPIO_Init(GPIOD, &GPIO_InitStructure);
        GPIO_ResetBits(GPIOD, USARTA_RTS);
    }

    // usartA configuration
    /* usartA configured as follow:
    - BaudRate = 115200 baud  
    - Word Length = 8 Bits
    - One Stop Bit
    - No parity
    - Hardware flow control enabled (RTS and CTS signals)
    - Receive and transmit enabled
    */
    USART_DeInit(USARTA_NAME);
    USART_InitStructure.USART_BaudRate = (u32)baud;
    USART_InitStructure.USART_WordLength = USART_WordLength_8b;
    USART_InitStructure.USART_StopBits = USART_StopBits_1;
    USART_InitStructure.USART_Parity = USART_Parity_No;
    if (useFlowControl)
    {
        USART_InitStructure.USART_HardwareFlowControl = USART_HardwareFlowControl_RTS_CTS;
    }
    else
    {
        USART_InitStructure.USART_HardwareFlowControl = USART_HardwareFlowControl_None;
    }
    USART_InitStructure.USART_Mode = USART_Mode_Rx | USART_Mode_Tx;

    USART_OverSampling8Cmd(USARTA_NAME, ENABLE);
    
    USART_Init(USARTA_NAME, &USART_InitStructure);

    if (useInterrupt)
    {
        // Enable usartA Receive and Transmit interrupts
        USART_ITConfig(USARTA_NAME, USART_IT_RXNE, ENABLE);
    }
    else
    {
        USART_ITConfig(USARTA_NAME, USART_IT_RXNE, DISABLE);
    }
    
    // Enable the usartA
    USART_Cmd(USARTA_NAME, ENABLE);
}

//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
void usartA_disable_interrupt()
{
    USART_ITConfig(USARTA_NAME, USART_IT_RXNE, DISABLE);
}

void usartA_enable_interrupt()
{
    USART_ITConfig(USARTA_NAME, USART_IT_RXNE, ENABLE);
}

//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
void usartA_changebaudrate(USARTBAUD baud)
{
    u32 tmpreg = 0x00, apbclock = 0x00;
    u32 integerdivider = 0x00;
    u32 fractionaldivider = 0x00;
    //  u32 usartxbase = 0;
    RCC_ClocksTypeDef RCC_ClocksStatus;
    
    /* DISABLE usartA Receive and Transmit interrupts */
    //USART_ITConfig(USARTA_NAME, USART_IT_RXNE, DISABLE);
    
    RCC_GetClocksFreq(&RCC_ClocksStatus);  
    
    apbclock = RCC_ClocksStatus.PCLK1_Frequency;
    
    /* Determine the integer part */
    integerdivider = ((0x19 * apbclock) / (0x04 * ((u32)baud)));
    tmpreg = (integerdivider / 0x64) << 0x04;
    
    /* Determine the fractional part */
    fractionaldivider = integerdivider - (0x64 * (tmpreg >> 0x04));
    tmpreg |= ((((fractionaldivider * 0x10) + 0x32) / 0x64)) & ((u8)0x0F);
    
    //  while(USART_GetFlagStatus(USARTA_NAME, USART_FLAG_IDLE) != RESET);
    while(USART_GetFlagStatus(USARTA_NAME, USART_FLAG_TC) == RESET);
    
    /* Write to USART BRR */
    USARTA_NAME->BRR = (u16)tmpreg;
    
    //USART_ClearFlag(USARTA_NAME, USART_FLAG_RXNE);
    //USART_ClearITPendingBit(USARTA_NAME, USART_IT_RXNE);
    
    /* Enable USARTA_NAME Receive and Transmit interrupts */
    //USART_ITConfig(USARTA_NAME, USART_IT_RXNE, ENABLE);
    
    /* Enable the usartA */
    //USART_Cmd(USARTA_NAME, ENABLE);
}

//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
//void usartA_tx(u8 databyte)
//{
//    USART_SendData(USARTA_NAME, databyte);
//    while(USART_GetFlagStatus(USARTA_NAME, USART_FLAG_TXE) == RESET);
//}
