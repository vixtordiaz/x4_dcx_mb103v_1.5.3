/**
  **************** (C) COPYRIGHT 2011 SCT Performance, LLC *********************
  * File Name          : spi.c
  * Device / System    : SCT products using the "CommonCode" platform
  * Author             : Quyen Leba
  *
  * Version            : 1 
  * Date               : 05/02/2011
  * Description        : 
  *                    : 
  *
  *
  * History            : 05/02/2011 M. Davis
  *                    :   Add SCT Copyright header,
  *
  ******************************************************************************
  */

#include <device_config.h>
#include <board/timer.h>
#include <MB103V/gpio.h>
#include <common/statuscode.h>
#include "spi.h"

#if USE_CC3000_WIFI
#include "cc3000.h"
#endif

#define SPI_MASTER                      SPI1
#define SPI_MASTER_CLK                  RCC_APB2Periph_SPI1
#define SPI_MASTER_GPIO_PORT            GPIOA
#define SPI_MASTER_GPIO_CLK             RCC_APB2Periph_GPIOA
#define SPI_MASTER_PIN_SCK              GPIO_Pin_5
#define SPI_MASTER_PIN_MISO             GPIO_Pin_6
#define SPI_MASTER_PIN_MOSI             GPIO_Pin_7
#define SPI_MASTER_DMA                  DMA1
#define SPI_MASTER_DMA_CLK              RCC_AHBPeriph_DMA1  
#define SPI_MASTER_Rx_DMA_Channel       DMA1_Channel2
#define SPI_MASTER_Rx_DMA_FLAG          DMA1_FLAG_TC2
#define SPI_MASTER_Tx_DMA_Channel       DMA1_Channel3
#define SPI_MASTER_Tx_DMA_FLAG          DMA1_FLAG_TC3  
#define SPI_MASTER_DR_Base              (SPI1_BASE + 0xC)

#define SPI_MASTER_CS0_PORT             GPIOE
#define SPI_MASTER_PIN_CS0              GPIO_Pin_7
#define SPI_MASTER_CS1_PORT             GPIOE
#define SPI_MASTER_PIN_CS1              GPIO_Pin_8

#if USE_CC3000_WIFI
#define spi_master_initfunc_cs0()       cc3000_deinit_cs()
#else
#define spi_master_initfunc_cs0()
#endif

//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
void spi_master_init()
{
    RCC_APB2PeriphClockCmd(SPI_MASTER_CLK, ENABLE);
    RCC_AHBPeriphClockCmd(SPI_MASTER_DMA_CLK, ENABLE);
    
    spi_gpio_init();
    
    // Startup in MBLIF interrupt mode with no CRC checking
    spi_master_polling_init();
}

//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
void spi_gpio_init()
{
    GPIO_InitTypeDef GPIO_InitStructure;

    GPIO_InitStructure.GPIO_Pin = SPI_MASTER_PIN_CS0;
    GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
    GPIO_InitStructure.GPIO_Mode = GPIO_Mode_Out_PP;
    GPIO_Init(SPI_MASTER_CS0_PORT, &GPIO_InitStructure);
    GPIO_SetBits(SPI_MASTER_CS0_PORT, SPI_MASTER_PIN_CS0);

    GPIO_InitStructure.GPIO_Pin = SPI_MASTER_PIN_CS1;
    GPIO_Init(SPI_MASTER_CS1_PORT, &GPIO_InitStructure);
    GPIO_SetBits(SPI_MASTER_CS1_PORT, SPI_MASTER_PIN_CS1);

    GPIO_InitStructure.GPIO_Pin = SPI_MASTER_PIN_SCK | SPI_MASTER_PIN_MOSI;
    GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
    GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AF_PP;
    GPIO_Init(SPI_MASTER_GPIO_PORT, &GPIO_InitStructure);

    GPIO_InitStructure.GPIO_Pin = SPI_MASTER_PIN_MISO;
    GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IN_FLOATING;
    GPIO_Init(SPI_MASTER_GPIO_PORT, &GPIO_InitStructure);
}

SPI_CS_TYPE current_cs = SPI_CS_TYPE_NONE;
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
void spi_master_enable_cs0()
{
    if(current_cs != SPI_CS_TYPE_0)
    {
        spi_master_initfunc_cs0();
        current_cs = SPI_CS_TYPE_0;
    }

    spi_master_disable_cs1();
    GPIO_ResetBits(SPI_MASTER_CS0_PORT, SPI_MASTER_PIN_CS0);
}

void spi_master_disable_cs0()
{
    GPIO_SetBits(SPI_MASTER_CS0_PORT, SPI_MASTER_PIN_CS0);
}

//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
void spi_master_enable_cs1()
{
    if(current_cs != SPI_CS_TYPE_1)
    {
        current_cs = SPI_CS_TYPE_1;
    }

    spi_master_disable_cs0();
    GPIO_ResetBits(SPI_MASTER_CS1_PORT, SPI_MASTER_PIN_CS1);
}

void spi_master_disable_cs1()
{
    GPIO_SetBits(SPI_MASTER_CS1_PORT, SPI_MASTER_PIN_CS1);
}

//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
void spi_master_polling_init()
{
    spi_master_moduleinitonly(FALSE);
}

//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
void spi_master_dma_init(bool useCRC)
{
    spi_master_moduleinitonly(useCRC);
}

//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
void spi_master_moduleinitonly(bool useCRC)
{
    SPI_InitTypeDef  SPI_InitStructure;
    
    DMA_DeInit(SPI_MASTER_Rx_DMA_Channel);
    DMA_DeInit(SPI_MASTER_Tx_DMA_Channel);
    
    SPI_I2S_DeInit(SPI_MASTER);
    
    SPI_InitStructure.SPI_Mode = SPI_Mode_Master;
    SPI_InitStructure.SPI_Direction = SPI_Direction_2Lines_FullDuplex;
    SPI_InitStructure.SPI_DataSize = SPI_DataSize_8b;
    SPI_InitStructure.SPI_CPOL = SPI_CPOL_Low;
    SPI_InitStructure.SPI_CPHA = SPI_CPHA_2Edge;
    SPI_InitStructure.SPI_NSS = SPI_NSS_Soft;
    SPI_InitStructure.SPI_BaudRatePrescaler = SPI_BaudRatePrescaler_8;
    SPI_InitStructure.SPI_FirstBit = SPI_FirstBit_MSB;
    SPI_InitStructure.SPI_CRCPolynomial = 7;
    SPI_Init(SPI_MASTER, &SPI_InitStructure);
    
    if(useCRC)
        SPI_CalculateCRC(SPI1, ENABLE);
    
    SPI_Cmd(SPI_MASTER, ENABLE);
}

//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
void spi_master_dma_transfer_data(u8 *txbuffer, u8 *rxbuffer, u32 buffersize, bool usetxirq)
{
    DMA_InitTypeDef  DMA_InitStructure;
    
    DMA_DeInit(SPI_MASTER_Rx_DMA_Channel);
    DMA_DeInit(SPI_MASTER_Tx_DMA_Channel);

    DMA_ITConfig(SPI_MASTER_Tx_DMA_Channel, DMA_IT_TC, DISABLE);
    
    DMA_InitStructure.DMA_PeripheralBaseAddr = (uint32_t)SPI_MASTER_DR_Base;
    DMA_InitStructure.DMA_BufferSize = buffersize;
    DMA_InitStructure.DMA_PeripheralInc = DMA_PeripheralInc_Disable;
    DMA_InitStructure.DMA_MemoryInc = DMA_MemoryInc_Enable;
    DMA_InitStructure.DMA_PeripheralDataSize = DMA_PeripheralDataSize_Byte;
    DMA_InitStructure.DMA_MemoryDataSize = DMA_MemoryDataSize_Byte;
    DMA_InitStructure.DMA_Mode = DMA_Mode_Normal;
    DMA_InitStructure.DMA_M2M = DMA_M2M_Disable;
    
    // SPI_MASTER_Rx_DMA_Channel configuration
    DMA_InitStructure.DMA_MemoryBaseAddr = (uint32_t)rxbuffer;
    DMA_InitStructure.DMA_DIR = DMA_DIR_PeripheralSRC;
    DMA_InitStructure.DMA_Priority = DMA_Priority_VeryHigh;
    DMA_Init(SPI_MASTER_Rx_DMA_Channel, &DMA_InitStructure);
    
    // SPI_MASTER_Tx_DMA_Channel configuration
    DMA_InitStructure.DMA_MemoryBaseAddr = (uint32_t)txbuffer;
    DMA_InitStructure.DMA_DIR = DMA_DIR_PeripheralDST;
    DMA_InitStructure.DMA_Priority = DMA_Priority_High;
    DMA_Init(SPI_MASTER_Tx_DMA_Channel, &DMA_InitStructure);
        
    // Enable SPI_MASTER DMA Tx request
    SPI_I2S_DMACmd(SPI_MASTER, SPI_I2S_DMAReq_Tx, ENABLE);
    // Enable SPI_MASTER DMA Rx request
    SPI_I2S_DMACmd(SPI_MASTER, SPI_I2S_DMAReq_Rx, ENABLE);
    
    if(usetxirq)
        DMA_ITConfig(SPI_MASTER_Tx_DMA_Channel, DMA_IT_TC, ENABLE);
    
    DMA_Cmd(SPI_MASTER_Rx_DMA_Channel, ENABLE);
    DMA_Cmd(SPI_MASTER_Tx_DMA_Channel, ENABLE);
}

//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
u8 spi_master_dma_transfer_waitforcomplete(u32 timeoutms)
{
    board_timeout_set(timeoutms);
    
    while(board_timeout() == FALSE)
    {
        if(DMA_GetFlagStatus(SPI_MASTER_Rx_DMA_FLAG) && 
           DMA_GetFlagStatus(SPI_MASTER_Tx_DMA_FLAG))
        {
            return S_SUCCESS;
        }
    }
    
    return S_TIMEOUT;
}

//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
u8 spi_master_dma_transfer_checkcrc()
{
    volatile u8 SPI_MASTERCRCValue;
    
    board_timeout_set(500);
    
    // Wait for CRC byte
    if(SPI_I2S_GetFlagStatus(SPI_MASTER, SPI_I2S_FLAG_RXNE) == RESET)
    {
        return S_BUSY;
    }
    
    // Read CRC value
    SPI_MASTERCRCValue = SPI_I2S_ReceiveData(SPI_MASTER);
    // Check CRC flag
    if ((SPI_I2S_GetFlagStatus(SPI_MASTER, SPI_FLAG_CRCERR)) != RESET)
    {
        spi_master_dma_init(TRUE);
        return S_FAIL; // CRC error
    }
    
    return S_SUCCESS;
}

//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
u8 spi_master_send_byte(u8 databyte)
{
    while (SPI_I2S_GetFlagStatus(SPI_MASTER, SPI_I2S_FLAG_TXE) == RESET);
    SPI_I2S_SendData(SPI_MASTER, databyte);
    
    return S_SUCCESS;
}

//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
u8 spi_master_receive_byte()
{
    while (SPI_I2S_GetFlagStatus(SPI_MASTER, SPI_I2S_FLAG_RXNE) == RESET);
    return SPI_I2S_ReceiveData(SPI_MASTER);
}




