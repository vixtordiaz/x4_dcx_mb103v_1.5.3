/**
  **************** (C) COPYRIGHT 2011 SCT Performance, LLC *********************
  * File Name          : generic.c
  * Device / System    : SCT products using the "CommonCode" platform
  * Author             : Quyen Leba
  *
  * Version            : 1 
  * Date               : 05/02/2011
  * Description        : 
  *                    : 
  *
  *
  * History            : 05/02/2011 M. Davis
  *                    :   Add SCT Copyright header,
  *
  ******************************************************************************
  */

#include <arch/gentype.h>
#include "generic.h"

#ifdef __USE_VPORT_MALLOC_
#include "heap_4_mod.c"
extern void vPortFree( void *pv );
extern void *pvPortMalloc( size_t xWantedSize );
extern unsigned int v_memory_start;
extern unsigned int v_memory_end;
#endif

#ifdef __DEBUG_JTAG_

#include <string.h>

extern void led_emergency_indicator_system_error(); /* from led.c */

vmalloc_t memory;   /* vmalloc memory structure */
u32 memory_start;   /* Heap starting address */
u32 memory_end;     /* Heap ending address */

/**
 *  @brief Allocate memory (verbose)
 *  
 *  @param [in] size    This is the size of the memory block, in bytes.
 *  @param [in] file    Filename string of where the malloc was called from.
 *  @param [in] line    Line number of where malloc was called from.
 *
 *  @return This function returns a pointer to the allocated memory, or NULL if
 *          the request fails.
 *  
 *  @details This custom function void *vmalloc(size_t size) allocates the
 *           requested memory and returns a pointer to it. In addition, symbolic
 *           information on the allocated memory is stored in a global table.
 */
void *vmalloc(size_t size, char *file, u32 line)
{
    u8* ptr;
    u32 memsize;
    u32 filename_size;
    u32 i;
    
    memsize = (size + sizeof(memsize));
    memset(&memory.last_allocated, 0, sizeof(vmalloc_item_t));
    memory.last_allocated.size = size;
    memory.last_allocated.line = line;
    filename_size = strlen(file);
    if (filename_size > sizeof(memory.last_allocated.file))
    {
        filename_size = filename_size;
    }
    memcpy(memory.last_allocated.file, file, filename_size);
        
#ifdef __USE_VPORT_MALLOC_
        ptr = pvPortMalloc(memsize);
#else
        ptr = malloc(memsize);
#endif
    
    if (ptr == 0)
    {
        /* CAN'T MALLOC, NOT ENOUGH MEM AVAILABLE!!!! */
        memory.status = S_MEMORY_FULL;
        if (memory.last_allocated.size + memory.free < memory.total)
        {
            /* NOT ENOUGH CONTIGUOUS MEM AVAILABLE FOR THIS MALLOC!!!! */
            memory.status = S_MEMORY_FULL_FRAGMENTED;
        }
        led_emergency_indicator_system_error();
    }
    if (memory.count++ >= MAX_MALLOC_ENTRY_COUNT)
    {
        /* CAN'T MALLOC, TOO MANY ITEMS ALLOCATED!!!! */
        memory.status = S_MEMORY_COUNT_EXCEEDED;
        led_emergency_indicator_system_error();
    }

    memory.used += memsize;
    if (memory.used > memory.peakused)
    {
        memory.peakused = memory.used;
    }
    if (memory.count > memory.peakcount)
    {
       memory.peakcount = memory.count;
    }
    memcpy(ptr, &memsize, sizeof(memsize));
    ptr += sizeof(memsize);    
    
    if (ptr)
    {
        /* Add information to table if memory successfully allocated */
        for (i = 0; i < MAX_MALLOC_ENTRY_COUNT; i++)
        {
            if (memory.allocated[i].address == NULL)
            { 
                memory.last_allocated.address = (u32)ptr;
                memcpy(&memory.allocated[i], &memory.last_allocated, sizeof(vmalloc_item_t));
                break;
            }
        }
        memory.free = memory.total - memory.used;
        vmalloc_fragmentation_check();
    }
    
    return (void*)ptr;
}

/**
 *  @brief Deallocate memory (verbose)
 *  
 *  @param [in] ptr     This is the pointer to a memory block previously 
 *                      allocated with vmalloc to be deallocated.If a null
 *                      pointer is passed as argument, no action occurs.
 *
 *  @return This function does not return any value
 *  
 *  @details This custom function void vfree(void *ptr) deallocates the memory
 *           previously allocated by vmalloc. In addition, symbolic information
 *           on the to be deallocated memory is removed from the global table.
 */
void vfree(void *ptr)
{
    u32 memsize;
    u32 i, j;
    
    if (ptr != NULL)
    {
        /* Remove information from table */
        for (i = 0; i < MAX_MALLOC_ENTRY_COUNT; i++)
        {
            if (memory.allocated[i].address == (u32)ptr)
            {
                memset(&memory.allocated[i], 0, sizeof(vmalloc_item_t));
                
                /* Shift items up */
                for (j = i + 1; j < MAX_MALLOC_ENTRY_COUNT; j++)
                {
                    memcpy(&memory.allocated[j-1], &memory.allocated[j], sizeof(vmalloc_item_t));
                    memset(&memory.allocated[j], 0, sizeof(vmalloc_item_t));
                }
                break;
            }
        }
        
        ptr = (void*)((u8*)ptr - sizeof(memsize));
        memcpy(&memsize, ptr, sizeof(memsize));
        
        memory.count--;
        memory.used -= memsize;
        memory.free = memory.total - memory.used;
        vmalloc_fragmentation_check();
        
#ifdef __USE_VPORT_MALLOC_
        vPortFree(ptr);
#else
        free(ptr);
#endif
        ptr = NULL;
    }
    
}

/**
 *  @brief Fragmentation check for vmalloc
 *
 *  @return This function does not return any value
 *  
 *  @details This function performs a memory heap fragmentation check
 */
void vmalloc_fragmentation_check()
{
    u32 start;
    u32 next, nextsize;
    u32 i, j;
    
    /* Start checking at beginning of heap */
    start = memory_start;
    memory.available = 0;
    
    for (i = 0; i < memory.count; i++)
    {
        /* Get next contiguous memory block */
        next = 0xFFFFFFFF;
        for (j = 0; j < memory.count; j++)
        {
            if (memory.allocated[j].address == NULL)
            {
                continue;
            }
            if (memory.allocated[j].address > start &&
                memory.allocated[j].address < next)
            {
                next = memory.allocated[j].address;
                nextsize = memory.allocated[j].size;
            }
        }
        /* Check free space between blocks */
        if (next - start > memory.available)
        {
            memory.available = next - start;
        }
        start = next + nextsize;
    }
    /* Check free space between last block and end of heap */
    if (memory_end - start > memory.available)
    {
        memory.available = memory_end - start;
    }
}

/**
 *  @brief Initialize vmalloc memory structure
 *
 *  @return This function does not return any value
 *  
 *  @details This function initializes the vmalloc memory structure. This 
 *           function must be called before allocating any memory with vmalloc.
 */
#pragma segment="HEAP"
void vmalloc_init()
{
#ifdef __USE_VPORT_MALLOC_
    memory.total = v_memory_end - v_memory_start;
    memory_start = v_memory_start;
    memory_end = v_memory_end;
#else
    memory.total = __segment_size("HEAP");
    memory_start = (u32)__segment_begin("HEAP");
    memory_end = (u32)__segment_end("HEAP");
#endif    
    memory.free = memory.total;
    memory.available = memory.total;
    memory.used = 0;
    memory.peakused = 0; 
    memory.count = 0;
    memory.peakcount = 0;
    memory.status = S_OKAY;
    memset(&memory.last_allocated, 0, sizeof(vmalloc_item_t));
    memset(memory.allocated, 0, sizeof(memory.allocated));
}

#else

/**
 *  @brief Allocate memory
 *  
 *  @param [in] size    This is the size of the memory block, in bytes.
 *
 *  @return This function returns a pointer to the allocated memory, or NULL if
 *          the request fails.
 *  
 *  @details This custom function void *malloc(size_t size) allocates the
 *           requested memory and returns a pointer to it. 
 */
void *__malloc(size_t s)
{
#ifdef __USE_VPORT_MALLOC_
        return pvPortMalloc(s);
#else
        return malloc(s);
#endif
}

/**
 *  @brief Deallocate memory
 *  
 *  @param [in] ptr     This is the pointer to a memory block previously 
 *                      allocated with malloc to be deallocated. If a null
 *                      pointer is passed as argument, no action occurs.
 *
 *  @return This function does not return any value
 *  
 *  @details This custom function void free(void *ptr) deallocates the memory
 *           previously allocated by malloc.
 */
void __free(void* p)
{
    if (p != NULL)
    {
#ifdef __USE_VPORT_MALLOC_
        vPortFree(p);
#else
        free(p);
#endif
        p = NULL;
    }
}

#endif /* __DEBUG_JTAG_ */

