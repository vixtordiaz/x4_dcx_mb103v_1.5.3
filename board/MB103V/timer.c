/**
  **************** (C) COPYRIGHT 2011 SCT Performance, LLC *********************
  * File Name          : timer.c
  * Device / System    : SCT products using the "CommonCode" platform
  * Author             : Quyen Leba
  *
  * Version            : 1 
  * Date               : 05/02/2011
  * Description        : 
  *                    : 
  *
  *
  * History            : 05/02/2011 M. Davis
  *                    :   Add SCT Copyright header,
  *
  ******************************************************************************
  */

#include <string.h>
#include <arch/gentype.h>
#include <board/timer.h>
#include <common/statuscode.h>

TIMER_INFO timer_info;

/**
 *  timer1_init
 *  
 *  @brief Init Timer 1 for global free running timer used for App and Board 
 *          Level timeouts
 *  
 *  @retval                     none
 *  
 *  @details Sets up timer 1 to interrupt every 1ms and increment global
 *              free running timer structure data for App and Board Level timeouts
 *  
 *  @authors Quyen Leba, Patrick Downs
 *  
 */
void timer1_init()
{
    TIM_TimeBaseInitTypeDef  TIM_TimeBaseStructure;
    
    // Init Board Timeout values
    timer_info.board_timeout = 1;
    timer_info.board_timeout_ms = 0xFFFFFFFF;
    
    // Init App Timeout values
    timer_info.app_timeout = 1;
    timer_info.app_timeout_ms = 0xFFFFFFFF;

    //Enable TIMER 1
    RCC_APB2PeriphClockCmd(RCC_APB2Periph_TIM1 , ENABLE);

    // TIM1 counter clock = 72 MHz
    // Interrupt Frequency = (72MHz/Prescaler)/Period
    // Interrupt Period = (1/Frequency)
    //
    // Period = 1/((72MHz/72)/1000) = 1ms
    // Every 1ms timer1 interrupt will fire
    
    TIM_TimeBaseStructure.TIM_Prescaler = 71; // Prescaler = (value + 1)
    TIM_TimeBaseStructure.TIM_CounterMode = TIM_CounterMode_Up;
    TIM_TimeBaseStructure.TIM_Period = 1000;
    TIM_TimeBaseStructure.TIM_ClockDivision = 0;
    TIM_TimeBaseStructure.TIM_RepetitionCounter = 0;
    TIM_TimeBaseInit(TIM1, &TIM_TimeBaseStructure);
    
    //TIM1 counter enable
    TIM_Cmd(TIM1, ENABLE);

    //TIM IT enable
    TIM_ITConfig(TIM1, TIM_IT_Update, ENABLE);
}

//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
void timer1_deinit()
{
    TIM_DeInit(TIM1);
    // TIM1 clock disable
    RCC_APB1PeriphClockCmd(RCC_APB2Periph_TIM1, DISABLE);
    // TIM1 disable counter
    TIM_Cmd(TIM1, DISABLE);
}

//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
u16 timer1_getcounter()
{
    return TIM_GetCounter(TIM1);
}

//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
void timer1_resetcounter()
{
    TIM_SetCounter(TIM1,0);
}

//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
void timer1_msec_counter_update()
{
    timer_info.msec++; // Increment free running timer milisecond count
    
    if(timer_info.app_timeout == FALSE)
    {
        if(timer_info.app_timeout_ms < timer_info.msec)
        {
            timer_info.app_timeout = TRUE;
        }
    }
    if(timer_info.board_timeout == FALSE)
    {
        if(timer_info.board_timeout_ms < timer_info.msec)
        {
            timer_info.board_timeout = TRUE; // TO reached
        }
    }    
}

/**
 *  timer2_init
 *  
 *  @brief Init Timer 2 for commlink command watch dog
 *  
 *  @retval                     none
 *  
 *  @details Sets up timer 2 to run as a commlink watch dog
 *  
 *  @authors Quyen Leba, Patrick Downs
 *  
 */
void timer2_init()
{
    TIM_TimeBaseInitTypeDef  TIM_TimeBaseStructure;
    
    //TIM2 clock enable
    RCC_APB1PeriphClockCmd(RCC_APB1Periph_TIM2, ENABLE);

    // TIM2 counter clock = 36 MHz
    // Interrupt Frequency = (36MHz/Prescaler)/Period
    // Interrupt Period = (1/Frequency)
    //
    // Period = 1/((36MHz/3600)/30000) = 3s
    // Every 3s timer2 interrupt will fire
    
    TIM_DeInit(TIM2);
    TIM_TimeBaseStructure.TIM_Period = 30000;
    TIM_TimeBaseStructure.TIM_Prescaler = 3599; // Prescaler = (value + 1)
    TIM_TimeBaseStructure.TIM_ClockDivision = 0;
    TIM_TimeBaseStructure.TIM_CounterMode = TIM_CounterMode_Up;
    TIM_TimeBaseInit(TIM2, &TIM_TimeBaseStructure);
    
    //TIM2 enable counter
    TIM_Cmd(TIM2, ENABLE);
    
    //TIM IT enable
    TIM_ITConfig(TIM2, TIM_IT_Update, ENABLE);
}

//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
void timer2_deinit()
{
    TIM_DeInit(TIM2);
    // TIM2 clock disable
    RCC_APB1PeriphClockCmd(RCC_APB1Periph_TIM2, DISABLE);
    // TIM2 disable counter
    TIM_Cmd(TIM2, DISABLE);
}

//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
u16 timer2_getcounter()
{
    return TIM_GetCounter(TIM2);
}

//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
void timer2_resetcounter()
{
    TIM_SetCounter(TIM2,0);
}

//------------------------------------------------------------------------------
// TIM3 NOT CURRENTLY USED
//------------------------------------------------------------------------------
/*
void timer3_init()
{
    TIM_TimeBaseInitTypeDef TIM_TimeBaseStructure;    

    //TIM3 clock enable
    RCC_APB1PeriphClockCmd(RCC_APB1Periph_TIM3, ENABLE);

    // TIM3 counter clock = 36 MHz
    // Interrupt Frequency = (36MHz/Prescaler)/Period
    // Interrupt Period = (1/Frequency)
    //
    // Period = 1/((36MHz/3600)/30000) = 3s
    // Every 3s timer2 interrupt will fire
    
    TIM_DeInit(TIM3);
    TIM_TimeBaseStructure.TIM_Period = 30000;
    TIM_TimeBaseStructure.TIM_Prescaler = 3599; // Prescaler = (value + 1)
    TIM_TimeBaseStructure.TIM_ClockDivision = 0;
    TIM_TimeBaseStructure.TIM_CounterMode = TIM_CounterMode_Up;
    TIM_TimeBaseInit(TIM3, &TIM_TimeBaseStructure);
    
    TIM_ARRPreloadConfig(TIM3, DISABLE);
    TIM_ITConfig(TIM3, TIM_IT_Update, DISABLE);
    TIM_Cmd(TIM3, DISABLE);
}

//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
void timer3_intr_handler()
{    
}
*/

//------------------------------------------------------------------------------
// This timer is used by LEDs
//------------------------------------------------------------------------------
void timer4_init(u8 prescale_type)
{
    TIM_TimeBaseInitTypeDef TIM_TimeBaseStructure;
    TIM_OCInitTypeDef TIM_OCInitStructure;
    u32 prescale;
    
    //TIM4 clock enable
    RCC_APB1PeriphClockCmd(RCC_APB1Periph_TIM4, ENABLE);
    
    TIM_DeInit(TIM4);
    //Time base configuration
    TIM_TimeBaseStructure.TIM_Period = 10000/2 - 1;
    switch(prescale_type)
    {
    case TIMER_PRESCALE_FAST:
        prescale = 1800*2;
        break;
    case TIMER_PRESCALE_REGULAR:
        prescale = 5000*2;
        break;
    case TIMER_PRESCALE_SLOW:
        prescale = 18000*2;
        break;
    default:
        prescale = 5000*2;
        break;
    }
    
    TIM_TimeBaseStructure.TIM_Prescaler = prescale - 1;
    TIM_TimeBaseStructure.TIM_ClockDivision = 0;
    TIM_TimeBaseStructure.TIM_CounterMode = TIM_CounterMode_Up;
    TIM_TimeBaseInit(TIM4, &TIM_TimeBaseStructure);
    
    TIM_OCInitStructure.TIM_OCMode = TIM_OCMode_PWM1;
    TIM_OCInitStructure.TIM_OCPolarity = TIM_OCPolarity_High;
    
    TIM_OCInitStructure.TIM_OutputState = TIM_OutputState_Enable;
    TIM_OCInitStructure.TIM_Pulse = 5000;
    TIM_OC2Init(TIM4, &TIM_OCInitStructure);
    TIM_OC2PreloadConfig(TIM4, TIM_OCPreload_Enable);
    
    TIM_OCInitStructure.TIM_OutputState = TIM_OutputState_Enable;
    TIM_OCInitStructure.TIM_Pulse = 5000;
    TIM_OC3Init(TIM4, &TIM_OCInitStructure);
    TIM_OC3PreloadConfig(TIM4, TIM_OCPreload_Enable);
    
    TIM_OCInitStructure.TIM_OutputState = TIM_OutputState_Enable;
    TIM_OCInitStructure.TIM_Pulse = 5000;
    TIM_OC4Init(TIM4, &TIM_OCInitStructure);
    TIM_OC4PreloadConfig(TIM4, TIM_OCPreload_Enable);

    TIM_ARRPreloadConfig(TIM4, ENABLE);
    //TIM2 enable counter
    TIM_Cmd(TIM4, ENABLE);
    
    //TIM IT enable
    //TIM_ITConfig(TIM4, TIM_IT_Update, ENABLE);
}

void timer4_pwm_duty_cycle(u8 ch, u8 duty_cycle)
{
    TIM_OCInitTypeDef TIM_OCInitStructure;

    TIM_OCInitStructure.TIM_OCMode = TIM_OCMode_PWM1;
    TIM_OCInitStructure.TIM_OCPolarity = TIM_OCPolarity_High;
    TIM_OCInitStructure.TIM_OutputState = TIM_OutputState_Enable;
    TIM_OCInitStructure.TIM_Pulse = 5000 * duty_cycle/100;

    switch(ch)
    {
    case 1:
        //not in used
        //TIM_OC1Init(TIM4, &TIM_OCInitStructure);
        //TIM_OC1PreloadConfig(TIM4, TIM_OCPreload_Enable);
        break;
    case 2:
        TIM_OC2Init(TIM4, &TIM_OCInitStructure);
        TIM_OC2PreloadConfig(TIM4, TIM_OCPreload_Enable);
        break;
    case 3:
        TIM_OC3Init(TIM4, &TIM_OCInitStructure);
        TIM_OC3PreloadConfig(TIM4, TIM_OCPreload_Enable);
        break;
    case 4:
        TIM_OC4Init(TIM4, &TIM_OCInitStructure);
        TIM_OC4PreloadConfig(TIM4, TIM_OCPreload_Enable);
        break;
    default:
        break;
    }
}



/**
 *  timer5_init
 *  
 *  @brief Use timer 5 to do delay
 *
 *  @retval None
 *  
 *  @details 
 *  
 *  @authors Quyen Leba
 *  
 */
void timer5_init()
{
    TIM_TimeBaseInitTypeDef TIM_TimeBaseStructure;
    
    // TIM5 counter clock = 72 MHz
    // Interrupt Frequency = (72MHz/Prescaler)/Period
    // Interrupt Period = (1/Frequency)

    //TIM5 clock enable
    RCC_APB1PeriphClockCmd(RCC_APB1Periph_TIM5, ENABLE);

    TIM_DeInit(TIM5);
    TIM_TimeBaseStructure.TIM_Period = 0xFFFF;
    TIM_TimeBaseStructure.TIM_Prescaler = (72-1);
    TIM_TimeBaseStructure.TIM_ClockDivision = 0;
    TIM_TimeBaseStructure.TIM_CounterMode = TIM_CounterMode_Up;
    TIM_TimeBaseInit(TIM5, &TIM_TimeBaseStructure);
    
    TIM_ARRPreloadConfig(TIM5, DISABLE);
    //TIM_ITConfig(TIM5, TIM_IT_Update, DISABLE);
    TIM_Cmd(TIM5, ENABLE);
}

/**
 *  timer5_wait_ms
 *  
 *  @brief Wait for amount of time in ms using timer 5
 *  
 *  @param [in] time_ms
 *
 *  @retval None
 *  
 *  @details 
 *  
 *  @authors Quyen Leba
 *  
 */
void timer5_wait_ms(u32 time_ms)
{
    while(1)
    {
        TIM_SetCounter(TIM5,0);
        if (time_ms <= 50)
        {
            while(TIM_GetCounter(TIM5) < (time_ms*1000));
            break;
        }
        else
        {
            while(TIM_GetCounter(TIM5) < 50000);
            time_ms -= 50;
        }
    }
}

/**
 *  timer5_wait_us
 *  
 *  @brief Wait for amount of time in us using timer 5
 *  
 *  @param [in] time_us
 *
 *  @retval None
 *  
 *  @details 
 *  
 *  @authors Quyen Leba
 *  
 */
void timer5_wait_us(u32 time_us)
{
    while(1)
    {
        TIM_SetCounter(TIM5,0);
        if (time_us <= 50000)
        {
            while(TIM_GetCounter(TIM5) < time_us);
            break;
        }
        else
        {
            while(TIM_GetCounter(TIM5) < 50000);
            time_us -= 50000;
        }
    }
}

//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
/*
void timer5_intr_handler()
{
}
*/

//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
void timer_task_null_func(void *priv) {}

//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
void timer7_init()
{
    u8  i;  
  
    TIM_TimeBaseInitTypeDef TIM_TimeBaseStructure;
    
    for(i = 0; i < TASKTIMER_TASK_COUNT_MAX; i++)
    {
        timer_info.task[i].timeout_ms = 0;
        timer_info.task[i].current_ms = 0;
        timer_info.task[i].control.abort = 0;
        timer_info.task[i].tasktype = (TaskTimerTaskType)i;
        timer_info.task[i].taskfunc = timer_task_null_func;
        timer_info.task[i].taskpriv = NULL;
    }

    //TIM7 clock enable
    RCC_APB1PeriphClockCmd(RCC_APB1Periph_TIM7, ENABLE);
    
    //TIM7 counter clock = 36 MHz
    TIM_DeInit(TIM7);
    //Time base configuration: 1ms
    TIM_TimeBaseStructure.TIM_Period = 100 - 1;
    TIM_TimeBaseStructure.TIM_Prescaler = (720/2) - 1;
    TIM_TimeBaseStructure.TIM_ClockDivision = 0;
    TIM_TimeBaseStructure.TIM_CounterMode = TIM_CounterMode_Up;
    TIM_TimeBaseInit(TIM7, &TIM_TimeBaseStructure);

    TIM_ARRPreloadConfig(TIM7, DISABLE);
    TIM_ITConfig(TIM7, TIM_IT_Update, DISABLE);
    TIM_Cmd(TIM7, DISABLE);
}

//------------------------------------------------------------------------------
// Enable/Disable passthrough mode. This mode will pass all data
// to debug console
// Inputs:  u32 time_ms (0: stop repeating task, >0: exec task after time_ms)
//          bool oneshot (TRUE: exec once)
//          TaskTimerTaskType tasktype
//          TimerTaskFunc taskfunc (exec func after time_ms)
//          void *taskpriv (optional priv data of taskfunc)
// Engineer: Quyen Leba
//------------------------------------------------------------------------------
u8 timer7_setup(u32 time_ms, bool oneshot, TaskTimerTaskType tasktype,
                TimerTaskFunc taskfunc, void *taskpriv)
{
    u8  i;
    u8  status;

    TIM_Cmd(TIM7, DISABLE);
    TIM_ITConfig(TIM7, TIM_IT_Update, DISABLE);

    status = S_FAIL;
    for(i = 0; i < TASKTIMER_TASK_COUNT_MAX; i++)
    {
        if (timer_info.task[i].tasktype == tasktype)
        {
            if (time_ms == 0)
            { 
                timer_info.task[i].control.abort = 1;
                timer_info.task[i].taskfunc = timer_task_null_func;
                timer_info.task[i].taskpriv = NULL;

                status = S_SUCCESS;
            }
            else if(timer_info.task[i].taskfunc == timer_task_null_func)
            {
                timer_info.task[i].current_ms = 0;
                timer_info.task[i].timeout_ms = time_ms;
                timer_info.task[i].control.abort = 0;
                if (taskfunc)
                { 
                    timer_info.task[i].taskfunc = taskfunc;
                    timer_info.task[i].taskpriv = taskpriv;
                    timer_info.task[i].control.oneshot = 0;
                    if (oneshot)
                    {
                        timer_info.task[tasktype].control.oneshot = 1;
                    }
                    
                    status = S_SUCCESS;
                }
            }
            else
            {
                break;
            }
        }//if (timer_info.task...
    }//for(i...

    TIM_ARRPreloadConfig(TIM7, ENABLE);
    TIM_ITConfig(TIM7, TIM_IT_Update, ENABLE);
    TIM_Cmd(TIM7, ENABLE);
    
    return status;    
}

//------------------------------------------------------------------------------
// Terminate all tasktimer tasks
//------------------------------------------------------------------------------
void timer7_terminate_alltasks()
{
    u8  i;

    TIM_ARRPreloadConfig(TIM7, DISABLE);
    TIM_ITConfig(TIM7, TIM_IT_Update, DISABLE);
    TIM_Cmd(TIM7, DISABLE);

    for(i = 0; i < TASKTIMER_TASK_COUNT_MAX; i++)
    {
        timer_info.task[i].timeout_ms = 0;
        timer_info.task[i].current_ms = 0;
        timer_info.task[i].control.abort = 0;
        timer_info.task[i].tasktype = (TaskTimerTaskType)i;
        timer_info.task[i].taskfunc = timer_task_null_func;
        timer_info.task[i].taskpriv = NULL;
    }
}

//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
void timer7_intr_handler()
{
    u8  cancelled_task_count;
    u8  i;

    cancelled_task_count = 0;
    for(i = 0; i < TASKTIMER_TASK_COUNT_MAX; i++)
    {
        if (timer_info.task[i].taskfunc != timer_task_null_func)
        {
            if (timer_info.task[i].current_ms >= timer_info.task[i].timeout_ms)
            {
                timer_info.task[i].current_ms = 0;
                timer_info.task[i].taskfunc(timer_info.task[i].taskpriv);

                if (timer_info.task[i].control.oneshot ||
                    timer_info.task[i].control.abort)
                {
                    //terminate this task
                    timer_info.task[i].control.abort = 0;
                    timer_info.task[i].taskfunc = timer_task_null_func;
                    cancelled_task_count++;
                }
            }
            else
            {
                if (!timer_info.task[i].control.abort)
                {
                    timer_info.task[i].current_ms++;
                }
                else
                {
                    //abort this task
                    timer_info.task[i].control.abort = 0;
                    timer_info.task[i].taskfunc = timer_task_null_func;
                    cancelled_task_count++;
                }
            }
        }//if(timer_info.task[i].taskfunc...
        else
        {
            //task in this slot is cancelled
            cancelled_task_count++;
        }
    }//for(i...

    if (cancelled_task_count >= TASKTIMER_TASK_COUNT_MAX)
    {
        //no tasks, turn off task timer
        TIM_ARRPreloadConfig(TIM7, DISABLE);
        TIM_ITConfig(TIM7, TIM_IT_Update, DISABLE);
        TIM_Cmd(TIM7, DISABLE);
    }
}
