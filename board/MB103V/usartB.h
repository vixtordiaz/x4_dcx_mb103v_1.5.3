/**
  **************** (C) COPYRIGHT 2011 SCT Performance, LLC *********************
  * File Name          : usartB.h
  * Device / System    : SCT products using the "CommonCode" platform
  * Author             : Quyen Leba
  *
  * Version            : 1 
  * Date               : 05/02/2011
  * Description        : 
  *                    : 
  *
  *
  * History            : 05/02/2011 M. Davis
  *                    :   Add SCT Copyright header,
  *
  ******************************************************************************
  */

#ifndef __USARTB_H
#define __USARTB_H

#include <arch/gentype.h>
#include "usart.h"

#define USARTB_NAME                USART3

void usartB_init(USARTBAUD baud, bool useFlowControl, bool useInterrupt);
void usartB_changebaudrate(USARTBAUD baud);
#define usartB_tx(databyte)     USART_SendData(USARTB_NAME, databyte);    \
    while(USART_GetFlagStatus(USARTB_NAME, USART_FLAG_TXE) == RESET);
#define usartB_rx()             USART_ReceiveData(USARTB_NAME)
#define usartB_getitstatus()    USART_GetITStatus(USARTB_NAME, USART_IT_RXNE)
#define usartB_getflagstatus(f) USART_GetFlagStatus(USARTB_NAME, f)

#define usartB_disable_rx_interrupt()   \
    USART_ITConfig(USARTB_NAME, USART_IT_RXNE, DISABLE);
#define usartB_isdatavailable() USART_GetFlagStatus(USART3, USART_FLAG_RXNE) == SET

#endif  //__USARTB_H
