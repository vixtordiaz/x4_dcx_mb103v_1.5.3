/**
  **************** (C) COPYRIGHT 2011 SCT Performance, LLC *********************
  * File Name          : commlink_hal_ken.c
  * Device / System    : SCT products using the "CommonCode" platform
  * Author             : Quyen Leba
  *
  * Version            : 1 
  * Date               : 05/02/2011
  * Description        : 
  *                    : 
  *
  *
  * History            : 05/02/2011 M. Davis
  *                    :   Add SCT Copyright header,
  *
  ******************************************************************************
  */

#include <device_config.h>
#include <board/commlink.h>
#include <common/statuscode.h>
#ifdef __KEN_EMULATOR__
#include <common/usb_callback.h>
#endif  //__KEN_EMULATOR__
#include "commlink_hal_ken.h"

extern CommLinkInfo commlink_info;

//Session status signal from KenAppBoard
#define COMMLINK_SESSION_PORT           GPIOD
#define COMMLINK_SESSION_PORT_SRC       GPIO_PortSourceGPIOD
#define COMMLINK_SESSION_PIN            GPIO_Pin_10
#define COMMLINK_SESSION_PIN_SRC        GPIO_PinSource10
#define COMMLINK_SESSION_EXTI_LINE      EXTI_Line10
#define COMMLINK_SESSION_EXTI_CH        EXTI15_10_IRQn

//------------------------------------------------------------------------------
// Input:   BluetoothBaudrate btbaud
// Return:  USARTBAUD bt_uartbaud
// Engineer: Quyen Leba
//------------------------------------------------------------------------------
USARTBAUD commlink_hal_ken_to_uart_baud_translator(CommLinkBaudrate btbaud)
{
    USARTBAUD uartbaud;
    switch(btbaud)
    {
    //add other baud here
    case COMMLINK_115200_BAUD:
        uartbaud = BAUD115200;
        break;
    case COMMLINK_230400_BAUD:
        uartbaud = BAUD230400;
        break;
    case COMMLINK_460800_BAUD:
        uartbaud = BAUD460800;
        break;
    case COMMLINK_921600_BAUD:
        uartbaud = BAUD921600;
        break;
    default:
        uartbaud = BAUD115200;
        break;
    }
    return uartbaud;
}

//------------------------------------------------------------------------------
// Setup interrupt to detect commlink status
// Engineer: Quyen Leba
//------------------------------------------------------------------------------
void commlink_hal_status_intr_init()
{
    GPIO_InitTypeDef GPIO_InitStructure;
#if !CONFIG_FOR_TEST
    EXTI_InitTypeDef EXTI_InitStructure;
#endif

    //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    // Session
    //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    GPIO_InitStructure.GPIO_Pin = COMMLINK_SESSION_PIN;
    GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IPU;
    GPIO_Init(COMMLINK_SESSION_PORT, &GPIO_InitStructure);
    
#if !CONFIG_FOR_TEST
    GPIO_EXTILineConfig(COMMLINK_SESSION_PORT_SRC, COMMLINK_SESSION_PIN_SRC);
    EXTI_InitStructure.EXTI_Line = COMMLINK_SESSION_EXTI_LINE;
    EXTI_InitStructure.EXTI_Mode = EXTI_Mode_Interrupt;
    EXTI_InitStructure.EXTI_Trigger = EXTI_Trigger_Rising_Falling;
    EXTI_InitStructure.EXTI_LineCmd = ENABLE;
    EXTI_Init(&EXTI_InitStructure);
#endif
}

//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
void commlink_session_intr_handler()
{
    if(EXTI_GetITStatus(COMMLINK_SESSION_EXTI_LINE) != RESET)
    {
#if FORCE_COMMLINK_STATUS_AS_OPENED
        commlink_update_link_status(CommLinkInfoLink_Opened);
#else
        if (GPIO_ReadInputDataBit(COMMLINK_SESSION_PORT, COMMLINK_SESSION_PIN))
        {
            //Rising-edge detected
            commlink_update_link_status(CommLinkInfoLink_Opened);
            commlink_clear_halt_sending_ack();
            commlink_set_connection_history_session_open();
        }
        else
        {
            commlink_update_link_status(CommLinkInfoLink_None);
            commlink_set_connection_history_session_close();
        }
#endif
        EXTI_ClearITPendingBit(COMMLINK_SESSION_EXTI_LINE);
    }
}

//------------------------------------------------------------------------------
// 
// Return:  u8  status
// Engineer: Quyen Leba
//------------------------------------------------------------------------------
u8 commlink_hal_init()
{
    USARTBAUD uartbaud;

    uartbaud = commlink_hal_ken_to_uart_baud_translator(commlink_info.baudrate);
    commlink_hal_uart(init)(uartbaud,COMMLINK_FLOWCONTROL_SETUP,UART_USE_INTERRUPT);

    commlink_hal_status_intr_init();
    commlink_status_indicator_update();

    return S_SUCCESS;
}

//------------------------------------------------------------------------------
// Update current status to indicator
// Engineer: Quyen Leba
//------------------------------------------------------------------------------
void commlink_hal_status_indicator_update()
{
#if CONFIG_FOR_TEST
    //do nothing
#else
#if FORCE_COMMLINK_STATUS_AS_OPENED
    commlink_update_link_status(CommLinkInfoLink_Opened);
#else
    if (GPIO_ReadInputDataBit(COMMLINK_SESSION_PORT, COMMLINK_SESSION_PIN))
    {
        //active session
        commlink_update_link_status(CommLinkInfoLink_Opened);
    }
    else
    {
        commlink_update_link_status(CommLinkInfoLink_None);
    }
#endif
#endif
}

//------------------------------------------------------------------------------
// Get device name
// Output:   u8 *name
// Return:  u8  status
// Engineer: Quyen Leba
//------------------------------------------------------------------------------
u8 commlink_hal_get_device_name(u8 *name)
{
    //-QK-
	//TODOQ:
    strcpy((char*)name,"LivewireTS");
    return S_SUCCESS;
}

//------------------------------------------------------------------------------
// Input:   CommLinkBaudrate baudrate
// Return:  u8  status
// Engineer: Quyen Leba
//------------------------------------------------------------------------------
u8 commlink_hal_change_baudrate(CommLinkBaudrate baudrate)
{
    USARTBAUD uartbaud;

    uartbaud = commlink_hal_ken_to_uart_baud_translator(commlink_info.baudrate);
    commlink_hal_uart(init)(uartbaud,COMMLINK_FLOWCONTROL_SETUP,UART_USE_INTERRUPT);
    return S_SUCCESS;
}

//------------------------------------------------------------------------------
// Input:   u8 *data
//          u16 length
//          bool session_required (TRUE: only tx if valid session)
// Return:  u8  status
// Engineer: Quyen Leba
//------------------------------------------------------------------------------
u8 commlink_hal_tx(u8 *data, u16 length, bool session_required)
{
#ifdef __KEN_EMULATOR__
    usb_push_vircom_data(data,length);
    return S_SUCCESS;
#else
#define COMMLINK_HAL_INACTIVITY_TIME_RESET      20  //seconds
    u32 cycleCount;
    u16 i = 0;
    
    //-------------------------
    // Android Testing Only
    // session_required = FALSE;
    //-------------------------
    while(i < length)
    {
        if (session_required &&
            commlink_info.link != CommLinkInfoLink_Opened)
        {
            goto commlink_hal_tx_session_dropped;
        }

        cycleCount = 50000; //TODOMD: figure out how many character intervals this is
        
        // Check for the TX data register to become available in a reasonable period of time
        while((!commlink_hal_uart(readytosend)()) && --cycleCount);
        if (cycleCount == 0)
        {
            if ((timeout_counter_gettime_in_sec() - commlink_info.connection_history.LastTransmission) >= COMMLINK_HAL_INACTIVITY_TIME_RESET)
            {
                commlink_set_halt_sending_ack();
                //-Q-
//////                bluetooth_hard_reset(TRUE); //TODOQ: if this failed, reset system
            }
            return S_TIMEOUT;
        }
        else
        {
            if (session_required &&
                commlink_info.link != CommLinkInfoLink_Opened)
            {
                goto commlink_hal_tx_session_dropped;
            }
            commlink_hal_uart(tx)(data[i++]);
            commlink_info.connection_history.LastTransmission = timeout_counter_gettime_in_sec();
        }
    }//while(i < length)...
    return S_SUCCESS;

commlink_hal_tx_session_dropped:  // open session was required, but not available
    // always turn off commlink_info.require_session... the calling routine will set it, if needed
    commlink_info.connection_history.LastTransmission = timeout_counter_gettime_in_sec();

    return S_SESSION_DROPPED;
#endif
}

//------------------------------------------------------------------------------
// Input:   u8 data
//------------------------------------------------------------------------------
void commlink_hal_tx_byte(u8 data)
{
    commlink_hal_uart(tx)(data);
}

//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
void commlink_hal_ken_uart_rx_irq_handler()
{
    u8  in;
    
    if(commlink_hal_uart(getitstatus)() != RESET)
    {
        in = commlink_hal_uart(rx)();

        //-------------------------------------
        // Special commlink Modes
        //------------------------------------- 
        if(commlink_info.passthrough)
        {
            debugif_out((u8*)&in, 1);
            return;
        }
        else if(commlink_info.loopback)
        {
            commlink_hal_uart(tx)(in);
            return;
        }

        //-------------------------------------
#ifndef __KEN_EMULATOR__
        commlink_process_incoming_command_byte(in);
#endif
    }
}
