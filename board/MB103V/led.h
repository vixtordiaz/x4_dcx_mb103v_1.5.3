/**
  **************** (C) COPYRIGHT 2011 SCT Performance, LLC *********************
  * File Name          : led.h
  * Device / System    : SCT products using the "CommonCode" platform
  * Author             : Quyen Leba
  *
  * Version            : 1 
  * Date               : 05/02/2011
  * Description        : 
  *                    : 
  *
  *
  * History            : 05/02/2011 M. Davis
  *                    :   Add SCT Copyright header,
  *
  ******************************************************************************
  */

#ifndef __LED_H
#define __LED_H

typedef enum 
{
    LED_OFF,
    LED_RED,
    LED_GREEN,
    LED_YELLOW,
    LED_WHITE,
    LED_PURPLE,
    
    //special state
    LED_BLUE,
    LED_BLUERED,
    LED_BLUEGREEN,
    
    //pwm only state
    LED_BLUE_BLINK_FAST,
    LED_BLUE_BLINK_REGULAR,
    LED_BLUE_BLINK_SLOW,
    LED_GREEN_BLINK_FAST,
    LED_GREEN_BLINK_REGULAR,
    LED_GREEN_BLINK_SLOW,
    LED_RED_BLINK_FAST,
    LED_RED_BLINK_REGULAR,
    LED_RED_BLINK_SLOW,
    LED_PURPLE_BLINK_SLOW,
    LED_PURPLE_BLINK_REGULAR,
    LED_PURPLE_BLINK_FAST,
    LED_YELLOW_BLINK_FAST,
    LED_YELLOW_BLINK_REGULAR,
    LED_YELLOW_BLINK_SLOW,
}LED_State;

void led_init(bool enable_pwm);
void led_setoperationmode(bool normal);
bool led_isnormaloperationmode();
void led_emergency_indicator_system_error();    //RED - WHITE
void led_emergency_indicator_settings_error();  //RED - RED - WHITE
void led_emergency_indicator_version_error();   //WHITE - WHITE - RED
void led_emergency_indicator_stack_error();     //RED - RED - BLUE - WHITE
void led_emergency_indicator_heap_error();      //RED - RED - GREEN - WHITE
void led_emergency_indicator_filesystem_error(); //RED - RED - YELLOW - WHITE 
void led_setState(LED_State state);
void led_cycle_test();

#endif  //__LED_H