/**
  **************** (C) COPYRIGHT 2011 SCT Performance, LLC *********************
  * File Name          : generic.h
  * Device / System    : SCT products using the "CommonCode" platform
  * Author             : Quyen Leba
  *
  * Version            : 1 
  * Date               : 05/02/2011
  * Description        : 
  *                    : 
  *
  *
  * History            : 05/02/2011 M. Davis
  *                    :   Add SCT Copyright header,
  *
  ******************************************************************************
  */

#ifndef __GENERIC_H
#define __GENERIC_H

#include <stdlib.h>

#define MAX_BUFFER_LENGTH                   4096
#define MAX_BUFFER_MALLOC_LENGTH            MAX_BUFFER_LENGTH
#define MAX_MALLOC_ENTRY_COUNT              84

/* To use the FreeRTOS vPortMalloc() and vFree() functions, uncomment the line
 * below. These functions require heap_4_mod.c to be added to the build and heap
 * size in stm32f10x_flash.icf must be set to 0: 
 *      define symbol __ICFEDIT_size_heap__   = 0;
 */
//#define __USE_VPORT_MALLOC_

void *__malloc(size_t s);
void __free(void* p);
void *vmalloc(size_t size, char *file, u32 line);
void vfree(void *ptr);
void vmalloc_init();
void vmalloc_fragmentation_check();

typedef enum
{
    S_OKAY                      = 0,    /**< Default status; everything is good */
    S_MEMORY_FULL               = 1,    /**< Not enough memory for allocation */
    S_MEMORY_FULL_FRAGMENTED    = 2,    /**< Not enough contiguous memory space for allocation */
    S_MEMORY_COUNT_EXCEEDED     = 3,    /**< Too many allocations */
}MEMORY_STATUS;

typedef struct
{
    u32 address;    /**< Memory address of allocated block */
    u32 size;       /**< Size of memory allocated */
    char file[52];  /**< Filename of source file containing the malloc */
    u32 line;       /**< Line number containing the malloc */
}vmalloc_item_t;
STRUCT_SIZE_CHECK(vmalloc_item_t, 64);

typedef struct
{
    u32 total;              /**< Total memory */
    u32 free;               /**< Free memory */
    u32 available;          /**< Free contiguous memory available for a single allocation */
    u32 used;               /**< Current memory used */
    u32 peakused;           /**< Peak memory used */
    u32 count;              /**< Number of current allocations */
    u32 peakcount;          /**< Peak allocations used */
    MEMORY_STATUS status;   /**< Status; on crash, this gives specific disposition */
    vmalloc_item_t last_allocated;                      /**< Info for last successful or attempted allocated memory block */
    vmalloc_item_t allocated[MAX_MALLOC_ENTRY_COUNT];   /**< Info for all currently allocated memory blocks */
}vmalloc_t;

/* Override malloc definition to use vmalloc. Use __BASE_FILE__ and __LINE__
 * macros to give better information about malloc location.
 *
 * NOTE: Add --no_path_in_file_macros to compiler options to avoid full path
 * for vmalloc filename. These constants consume memory.
 */
#ifdef __DEBUG_JTAG_
#define __malloc(size)            vmalloc(size,__BASE_FILE__,__LINE__)
#define __free                    vfree
#endif

#endif    //__GENERIC_H