/**
  **************** (C) COPYRIGHT 2011 SCT Performance, LLC *********************
  * File Name          : rtc.c
  * Device / System    : SCT products using the "CommonCode" platform
  * Author             : Quyen Leba
  *
  * Version            : 1 
  * Date               : 05/02/2011
  * Description        : 
  *                    : 
  *
  *
  * History            : 05/02/2011 M. Davis
  *                    :   Add SCT Copyright header,
  *
  ******************************************************************************
  */

#include <board/rtc.h>

#define RTC_PRESCALE_VAULE    62
#define RTC_CLK_SOURCE_VALUE  62500
#define RTC_TICK_VALUE        1/(RTC_CLK_SOURCE_VALUE/(RTC_PRESCALE_VAULE+1))

//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
void rtc_init()
{
    // Enable PWR and BKP clocks
    RCC_APB1PeriphClockCmd(RCC_APB1Periph_PWR | RCC_APB1Periph_BKP, ENABLE);

    // Allow access to BKP Domain
    PWR_BackupAccessCmd(ENABLE);

    // Reset Backup Domain
    BKP_DeInit();

    // Select HSE_Div128 as RTC Clock Source
    RCC_RTCCLKConfig(RCC_RTCCLKSource_HSE_Div128);

    // Enable RTC Clock
    RCC_RTCCLKCmd(ENABLE);

    // Wait for RTC registers synchronization
    RTC_WaitForSynchro();

    // Wait until last write operation on RTC registers has finished
    RTC_WaitForLastTask();

    // Enable the RTC Second
    RTC_ITConfig(RTC_IT_SEC, ENABLE);

    // RTC period = RTCCLK/RTC_PR = (62.5 KHz)/(62+1) for 1.008ms tick
    RTC_SetPrescaler(RTC_PRESCALE_VAULE);

    // Wait until last write operation on RTC registers has finished
    RTC_WaitForLastTask();
}

//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
void rtc_reset()
{
    /* Wait until last write operation on RTC registers has finished */
    RTC_WaitForLastTask();
    RTC_SetCounter(0x0);
    RTC_WaitForLastTask();
}

//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
u32 rtc_getvalue()
{
    return RTC_GetCounter();
}
