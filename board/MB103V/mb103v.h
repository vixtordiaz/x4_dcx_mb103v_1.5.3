/**
  **************** (C) COPYRIGHT 2011 SCT Performance, LLC *********************
  * File Name          : mb103v.h
  * Device / System    : SCT products using the "CommonCode" platform
  * Author             : Quyen Leba
  *
  * Version            : 1 
  * Date               : 05/02/2011
  * Description        : 
  *                    : 
  *
  *
  * History            : 05/02/2011 M. Davis
  *                    :   Add SCT Copyright header,
  *
  ******************************************************************************
  */

#ifndef __MB103V_H
#define __MB103V_H

#define BOARDREV_REVA1                              1

#define USE_CRC32_HARDWARE                          1
#define USE_UARTTYPE_DATALINK                       1

#include "timer.h"
#include "MB103V/adcmb.h"
#include "MB103V/properties_mb103v.h"
#include "MB103V/generic.h"
#include "MB103V/init.h"
#include "MB103V/flash.h"
#include "MB103V/crc32_hw.h"
#include "MB103V/watchdog.h"
#include "MB103V/spi.h"
#include "MB103V/gpio.h"
#include "MB103V/led.h"
#include "MB103V/button.h"
#include "MB103V/accelerometer.h"
#include "MB103V/usartB.h"
#include "MB103V/uartC.h"
#include "MB103V/interrupt_sw.h"
#if __COMMLINK_HAL_BLUETOOTH__
#include "MB103V/bluetooth.h"
#endif
#include "usb_type.h"
#include "usb_core.h"
#include "usb_hwconfig.h"
#include "usb_lib.h"
#include "MB103V/USB/usb_hwconfig.h"
#include "MB103V/USB/usb_callback_helper.h"

#if __COMMLINK_HAL_BLUETOOTH__
#include "MB103V/commlink_hal_bluetooth.h"
#elif __COMMLINK_HAL_KEN__
#include "MB103V/cc3000_hal.h"
#include "MB103V/commlink_hal_ken.h"
#elif __COMMLINK_HAL_X4__
#include "MB103V/cc3000_hal.h"
#include "MB103V/commlink_hal_x4.h"
#else
#error genplatform.h: unknown COMMLINK_HAL
#endif

#define SUPPORT_FILE_XFER

#define DEFAULT_DATALOG_FEATURE_FILENAME            "datalogfeatures.dlx"

#endif    //__MB103V_H
