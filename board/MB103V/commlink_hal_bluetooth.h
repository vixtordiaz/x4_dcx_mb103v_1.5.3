/**
  **************** (C) COPYRIGHT 2011 SCT Performance, LLC *********************
  * File Name          : commlink_hal_bluetooth.h
  * Device / System    : SCT products using the "CommonCode" platform
  * Author             : Quyen Leba
  *
  * Version            : 1 
  * Date               : 05/02/2011
  * Description        : 
  *                    : 
  *
  *
  * History            : 05/02/2011 M. Davis
  *                    :   Add SCT Copyright header,
  *
  ******************************************************************************
  */

#ifndef __COMMLINK_HAL_BLUETOOTH_H
#define __COMMLINK_HAL_BLUETOOTH_H

#include <arch/gentype.h>
#include <board/genplatform.h>
#include <board/commlink.h>

#define commlink_reset_command_watchdog()       timer2_resetcounter()
#define commlink_get_incoming_byte()            bluetooth_hal(rx)()

u8 commlink_hal_init();
void commlink_hal_status_indicator_update();
u8 commlink_hal_get_device_name(u8 *name);
u8 commlink_hal_change_baudrate(CommLinkBaudrate baudrate);

u8 commlink_hal_tx(u8 *data, u16 length, bool session_required);
void commlink_hal_tx_byte(u8 data);

#endif	//__COMMLINK_HAL_BLUETOOTH_H
