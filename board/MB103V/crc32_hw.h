/**
  **************** (C) COPYRIGHT 2011 SCT Performance, LLC *********************
  * File Name          : crc32_hw.h
  * Device / System    : SCT products using the "CommonCode" platform
  * Author             : Quyen Leba
  *
  * Version            : 1 
  * Date               : 05/02/2011
  * Description        : 
  *                    : 
  *
  *
  * History            : 05/02/2011 M. Davis
  *                    :   Add SCT Copyright header,
  *
  ******************************************************************************
  */

#ifndef __CRC32_HW_H
#define __CRC32_HW_H

#include <arch/gentype.h>


#define CRC32_USE_HARDWARE

//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
static inline void crc32_reset()
{
    CRC_ResetDR();
}

//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
static inline void crc32e_reset()
{
    CRC_ResetDR();
}

//------------------------------------------------------------------------------
// Inputs:  u32 val (initial value) (UNUSED)
//          u32 pBuffer[]
//          u32 BufferLength (length of pBuffer)
// Return:  u32 calc_value (the crc32e value)
//------------------------------------------------------------------------------
static inline u32 crc32e_calculateblock(u32 val,
                                        u32 pBuffer[], u32 BufferLength)
{
    return CRC_CalcBlockCRC(pBuffer,BufferLength);
}

#endif    //__CRC32_HW_H
