/**
  **************** (C) COPYRIGHT 2011 SCT Performance, LLC *********************
  * File Name          : delays.c
  * Device / System    : SCT products using the "CommonCode" platform
  * Author             : Quyen Leba
  *
  * Version            : 1 
  * Date               : 05/02/2011
  * Description        : 
  *                    : 
  *
  *
  * History            : 05/02/2011 M. Davis
  *                    :   Add SCT Copyright header,
  *
  ******************************************************************************
  */

#include <stm32f10x_systick.h>
#include <board/delays.h>
#include <board/timer.h>

vu32 TimingDelay = 0;

//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
void delays(u32 timeout, u8 prefix)
{
    if (prefix == 'm')
    {
        delay_timer_wait_ms(timeout);
    }
    else if (prefix == 'u')
    {
        delay_timer_wait_us(timeout);
    }
}

//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
void delays_counter(u32 count)
{
    volatile u32 i = 0;
    while(1)
    {
        if (i++ > count)
        {
            break;
        }
    }
}
