/**
  **************** (C) COPYRIGHT 2011 SCT Performance, LLC *********************
  * File Name          : power.c
  * Device / System    : SCT products using the "CommonCode" platform
  * Author             : Quyen Leba
  *
  * Version            : 1 
  * Date               : 05/02/2011
  * Description        : 
  *                    : 
  *
  *
  * History            : 05/02/2011 M. Davis
  *                    :   Add SCT Copyright header,
  *
  ******************************************************************************
  */

#include <string.h>
#include <common/statuscode.h>
#include <board/power.h>
#include <board/delays.h>
#include <board/mblexec.h>
#include <common/settings.h>

#define POWER_STACKBUS_5VSW_EN_PORT     GPIOC
#define POWER_STACKBUS_5VSW_EN_PIN      GPIO_Pin_3
#define POWER_STACKBUS_5VM_EN_PORT      GPIOC
#define POWER_STACKBUS_5VM_EN_PIN       GPIO_Pin_2
#define POWER_STACKBUS_3V3_EN_PORT      GPIOC
#define POWER_STACKBUS_3V3_EN_PIN       GPIO_Pin_0

#define POWER_DEFAULT_5VSW_ON           0
#define POWER_DEFAULT_5VM_ON            1

//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
void power_init()
{
    u8 status;
    u8 returndata;
    u32 returndatalength;
    
    GPIO_InitTypeDef GPIO_InitStructure;

    GPIO_InitStructure.GPIO_Pin = POWER_STACKBUS_5VSW_EN_PIN;
    GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
    GPIO_InitStructure.GPIO_Mode = GPIO_Mode_Out_OD;
#if POWER_DEFAULT_5VSW_ON
    //GPIO_SetBits(POWER_STACKBUS_5VSW_EN_PORT, POWER_STACKBUS_5VSW_EN_PIN);
    //this method reduce stress on the pin because there's no gate resistor
    GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IPU;
#else
    GPIO_ResetBits(POWER_STACKBUS_5VSW_EN_PORT, POWER_STACKBUS_5VSW_EN_PIN);
#endif
    GPIO_Init(POWER_STACKBUS_5VSW_EN_PORT, &GPIO_InitStructure);
    
    GPIO_InitStructure.GPIO_Pin = POWER_STACKBUS_5VM_EN_PIN;
    GPIO_InitStructure.GPIO_Mode = GPIO_Mode_Out_PP;
#if POWER_DEFAULT_5VM_ON
    //GPIO_SetBits(POWER_STACKBUS_5VM_EN_PORT, POWER_STACKBUS_5VM_EN_PIN);
    //this method reduce stress on the pin because there's no gate resistor
    GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IPU;
#else
    GPIO_ResetBits(POWER_STACKBUS_5VM_EN_PORT, POWER_STACKBUS_5VM_EN_PIN);
#endif
    GPIO_Init(POWER_STACKBUS_5VM_EN_PORT, &GPIO_InitStructure);
    
    GPIO_InitStructure.GPIO_Pin = POWER_STACKBUS_3V3_EN_PIN;
    GPIO_InitStructure.GPIO_Mode = GPIO_Mode_Out_OD;
    GPIO_SetBits(POWER_STACKBUS_3V3_EN_PORT, POWER_STACKBUS_3V3_EN_PIN);
    GPIO_Init(POWER_STACKBUS_3V3_EN_PORT, &GPIO_InitStructure);
    
    status = settings_getsettings_byopcode(SETTINGS_OPCODE_ANALOGPORT_EXT5V, 0,
                                           &returndata, &returndatalength);    
    if(status == S_SUCCESS)
    {
        if(returndata == EXT5VCONFIG_ON)
        {
            V5VExt_Ctrl(ENABLE);
        }
        else if(returndata != EXT5VCONFIG_OFF && returndata != EXT5VCONFIG_AUTO)
        {
            returndata = EXT5VCONFIG_AUTO;
            returndatalength = 1;
            status = settings_setsettings_byopcode(SETTINGS_OPCODE_ANALOGPORT_EXT5V, 0,
                                           &returndata, returndatalength);
        }
    }
}

//------------------------------------------------------------------------------
// Turn on/off and route Vpp to ODB2
// Input:   VppState state (Vpp_OFF, Vpp_FEPS, Vpp_SCI_AE_6, Vpp_SCI_BT_9, Vpp_SCI_BE_12, Vpp_SCI_AT_14)
// Engineer: Quyen Leba
// Date: Jan 28, 2008
//------------------------------------------------------------------------------
u8 Vpp_Ctrl(VppState state)
{
    MBLEXEC_HWCONFIG hwconfig;
    u16 bufferlength;
    u8  status;

    switch(state)
    {
    case Vpp_OFF:
    case Vpp_FEPS:
    case Vpp_SCI_AE_6:
    case Vpp_SCI_BT_9:
    case Vpp_SCI_BE_12:
    case Vpp_SCI_AT_14:
        //state is valid
        break;
    default:
        return S_INPUT;
    }
    
    status = mblexec_call(MBLEXEC_OPCODE_GET_HWCONFIG,NULL,0,
                          (u8*)&hwconfig,&bufferlength);
    if (status == S_SUCCESS)
    {
        hwconfig.enable_feps = 0;
        hwconfig.enable_sci_ae_vpp = 0;
        hwconfig.enable_sci_at_vpp = 0;
        hwconfig.enable_sci_be_vpp = 0;
        hwconfig.enable_sci_bt_vpp = 0;
        
        switch(state)
        {
        case Vpp_OFF:
            break;
        case Vpp_FEPS:
            hwconfig.enable_feps = 1;
            break;
        case Vpp_SCI_AE_6:
            hwconfig.enable_sci_ae_vpp = 1;
            break;
        case Vpp_SCI_BT_9:
            hwconfig.enable_sci_bt_vpp = 1;
            break;
        case Vpp_SCI_BE_12:
            hwconfig.enable_sci_be_vpp = 1;
            break;
        case Vpp_SCI_AT_14:
            hwconfig.enable_sci_at_vpp = 1;
            break;
        default:
            return S_INPUT;
        }
        
        delays_counter(DELAYS_COUNTER_100MS);
        status = mblexec_call(MBLEXEC_OPCODE_SET_HWCONFIG,
                              (u8*)&hwconfig,sizeof(MBLEXEC_HWCONFIG),
                              NULL,NULL);
    }

    return status;
}

//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
void V5VExt_Ctrl(FunctionalState State)
{
    MBLEXEC_HWCONFIG hwconfig;
    u16 bufferlength;
    u8  status;

    status = mblexec_call(MBLEXEC_OPCODE_GET_HWCONFIG,NULL,0,
                          (u8*)&hwconfig,&bufferlength);
    if (status == S_SUCCESS)
    {
        if (State == ENABLE && hwconfig.enable_ext5v == 0)
        {
            hwconfig.enable_ext5v = 1;
            status = mblexec_call(MBLEXEC_OPCODE_SET_HWCONFIG,
                                  (u8*)&hwconfig,sizeof(MBLEXEC_HWCONFIG),
                                  NULL,NULL);
        }
        else if (State == DISABLE && hwconfig.enable_ext5v == 1)
        {
            hwconfig.enable_ext5v = 0;
            status = mblexec_call(MBLEXEC_OPCODE_SET_HWCONFIG,
                                  (u8*)&hwconfig,sizeof(MBLEXEC_HWCONFIG),
                                  NULL,NULL);
        }
    }
}
