/**
  **************** (C) COPYRIGHT 2011 SCT Performance, LLC *********************
  * File Name          : peripherals.c
  * Device / System    : SCT products using the "CommonCode" platform
  * Author             : Quyen Leba
  *
  * Version            : 1 
  * Date               : 05/02/2011
  * Description        : 
  *                    : 
  *
  *
  * History            : 05/02/2011 M. Davis
  *                    :   Add SCT Copyright header,
  *
  ******************************************************************************
  */

#include <board/genplatform.h>
#include <board/power.h>
#include <board/delays.h>
#include <board/timer.h>
#include <board/rtc.h>
#include <board/bootloader.h>
#include <board/peripherals.h>
#include <board/commlink.h>
#include <board/hardwarefeatures.h>
#include <board/debugif.h>
#if USE_WIFI
#include <board/CC3000/wifi_hal.h>
#endif
#include <fs/genfs.h>
#include <common/obd2can.h>
#include <common/bootsettings.h>
#include <common/settings.h>
#include <common/cmdif.h>
#include <common/statuscode.h>
#include "button.h"
#include "sdmmc.h"
#include "i2c.h"
#include "accelerometer.h"
#include "led.h"
#include "indicator.h"
#include "usart.h"

//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
extern void USB_Disconnect_Config(void);
extern void Set_USBClock(void);
extern void USB_Interrupts_Config(void);
extern void USB_Init(void);

//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
void peripherals_init()
{
    u8  fsstatus;

    //Enable GPIOA, GPIOB, GPIOC, GPIOD, GPIOE, GPIOF, GPIOG and AFIO clocks
    RCC_APB2PeriphClockCmd
        (RCC_APB2Periph_GPIOA | RCC_APB2Periph_GPIOB |RCC_APB2Periph_GPIOC |
         RCC_APB2Periph_GPIOD | RCC_APB2Periph_GPIOE | RCC_APB2Periph_GPIOF |
         RCC_APB2Periph_GPIOG | RCC_APB2Periph_AFIO, ENABLE);
    
    //Enable CRC clock
    RCC_AHBPeriphClockCmd(RCC_AHBPeriph_CRC, ENABLE);
    
    //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    // Configure a SysTick Base time to 1 ms.
    //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    SysTick_Config(72000000 / 1000);

    delay_timer_init();
    gpio_init();
    power_init();
    USB_Disconnect_Config();
    timeout_counter_init();
    tasktimer_init();
    timer2_init();

    led_init(FALSE);
    led_setState(LED_RED);
    
    i2c_init();
    rtc_init();
    sdmmc_init();
    button_init();

    bootsettings_load();
    settings_load();
    properties_mb_init();
    bootloader_force_application_validation();
    
    button_intr_init();
    gpio_dav_busy_pin_init();
    gpio_usb_mux_status_pin_init();
    interrupt_init(ENABLE);
    
    peripherals_init_vehicleboard();
    Vpp_Ctrl(Vpp_OFF);
    
    accelerometer_init();
    adc_init(ADC_DATALOG);

    fsstatus = genfs_init();
    
    cmdif_init();
    commlink_init(USE_COMMLINK_BAUD);
    
    // Commlink needs to be init before request AB properties
    properties_ab_init(); 
    
#if USE_WIFI
    if (hardwarefeatures_has_wifi())
    {
        // Board Rev from AB properties must be init before CC3000 init
        wifi_hal_init_on_powerup();
    }
#endif
    
    Set_USBClock();
    USB_Interrupts_Config();
    USB_Init();
    
    if(fsstatus != S_SUCCESS)
    {
        // Filesystem error
        // Halt system here after USB initialized and show error on LED       
        led_emergency_indicator_filesystem_error();
    }
    
#ifdef __DEBUG_JTAG_
    debugif_init();
#endif    
    if (SETTINGS_IsMarried())
    {
        indicator_set(Indicator_EnterApplicationLocked);
    }
    else
    {
        indicator_set(Indicator_EnterApplicationUnlocked);
    }
    led_init(TRUE);
    commlink_status_indicator_update();
}

//------------------------------------------------------------------------------
// Init vehicle board (reset, setup SPI & MBLIF)
//------------------------------------------------------------------------------
void peripherals_init_vehicleboard()
{
    gpio_reset_vehicle_board();
    spi_master_init();
    mblif_init();
}
