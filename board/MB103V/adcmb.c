/**
  **************** (C) COPYRIGHT 2011 SCT Performance, LLC *********************
  * File Name          : adcmb.c
  * Device / System    : SCT products using the "CommonCode" platform
  * Author             : Quyen Leba
  *
  * Version            : 1 
  * Date               : 05/02/2011
  * Description        : 
  *                    : 
  *
  *
  * History            : 05/02/2011 M. Davis
  *                    :   Add SCT Copyright header,
  *
  ******************************************************************************
  */

#include <device_config.h>
#include <arch/gentype.h>
#include <board/adc.h>
#include <board/datalogfeatures.h>
#include <common/statuscode.h>
#include "adcmb.h"

#define ADC1_DR_Address         ((uint32_t)0x4001244C)
#define ADC3_DR_Address         ((uint32_t)0x40013C4C)

#define ADC_AIN7_INDEX          0
#define ADC_AIN8_INDEX          1

struct
{
    //channels of this MainBoard
    u16 adc_values[2];
}adc_info;

//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
void adcmb_init()
{
    ADC_InitTypeDef ADC_InitStructure;
    DMA_InitTypeDef DMA_InitStructure;
    GPIO_InitTypeDef GPIO_InitStructure;

    // Enable ADC1 and GPIOC clocks
    RCC_APB2PeriphClockCmd(RCC_APB2Periph_ADC1 |
                           RCC_APB2Periph_GPIOA, ENABLE);
    // Enable DMA1 clock
    RCC_AHBPeriphClockCmd(RCC_AHBPeriph_DMA1, ENABLE);

    //ADC channels 1&2 (PA1, PA2)
    //CH1: AIN0_5V
    //CH2: AIN1_5V
    GPIO_InitStructure.GPIO_Pin = GPIO_Pin_1 | GPIO_Pin_2;
    GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AIN;
    GPIO_Init(GPIOA, &GPIO_InitStructure);
    
    DMA_DeInit(DMA1_Channel1);
    DMA_InitStructure.DMA_PeripheralBaseAddr = (u32)ADC1_DR_Address;
    DMA_InitStructure.DMA_MemoryBaseAddr = (u32)adc_info.adc_values;
    DMA_InitStructure.DMA_DIR = DMA_DIR_PeripheralSRC;
    DMA_InitStructure.DMA_BufferSize = 2;
    DMA_InitStructure.DMA_PeripheralInc = DMA_PeripheralInc_Disable;
    DMA_InitStructure.DMA_MemoryInc = DMA_MemoryInc_Enable;
    DMA_InitStructure.DMA_PeripheralDataSize = DMA_PeripheralDataSize_HalfWord;
    DMA_InitStructure.DMA_MemoryDataSize = DMA_MemoryDataSize_HalfWord;
    DMA_InitStructure.DMA_Mode = DMA_Mode_Circular;
    DMA_InitStructure.DMA_Priority = DMA_Priority_High;
    DMA_InitStructure.DMA_M2M = DMA_M2M_Disable;
    DMA_Init(DMA1_Channel1, &DMA_InitStructure);

    DMA_ITConfig(DMA1_Channel1, DMA_IT_TC, ENABLE);
    DMA_Cmd(DMA1_Channel1, ENABLE);
    
    ADC_InitStructure.ADC_Mode = ADC_Mode_Independent;
    ADC_InitStructure.ADC_ScanConvMode = ENABLE;
    ADC_InitStructure.ADC_ContinuousConvMode = ENABLE;
    ADC_InitStructure.ADC_ExternalTrigConv = ADC_ExternalTrigConv_None;
    ADC_InitStructure.ADC_DataAlign = ADC_DataAlign_Right;
    ADC_InitStructure.ADC_NbrOfChannel = 2;
    ADC_Init(ADC1, &ADC_InitStructure);

    // ADC regular channels configuration
    ADC_RegularChannelConfig(ADC1, ADC_Channel_1, 1, ADC_SampleTime_239Cycles5);
    ADC_RegularChannelConfig(ADC1, ADC_Channel_2, 2, ADC_SampleTime_239Cycles5);
    
    ADC_DMACmd(ADC1, ENABLE);
    ADC_Cmd(ADC1, ENABLE);
    
    // Enable ADC1 reset calibaration register
    ADC_ResetCalibration(ADC1);
    // Check the end of ADC1 reset calibration register
    while(ADC_GetResetCalibrationStatus(ADC1));
    
    // Start ADC1 calibaration
    ADC_StartCalibration(ADC1);
    // Check the end of ADC1 calibration
    while(ADC_GetCalibrationStatus(ADC1));
    
    // Start ADC1 Software Conversion
    ADC_SoftwareStartConvCmd(ADC1, ENABLE);
}

//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
u8 adcmb_read(ADC_CHANNEL channel, float *DataValueFloat)
{
    switch(channel)
    {
    case ADC_AIN7:
        *DataValueFloat = datalogfeatures_calc_ain_7(adc_info.adc_values[ADC_AIN7_INDEX]);
        break;
    case ADC_AIN8:
        *DataValueFloat = datalogfeatures_calc_ain_8(adc_info.adc_values[ADC_AIN8_INDEX]);
        break;
    default:
        return S_INPUT;
    }

    return S_SUCCESS;
}
