/**
  **************** (C) COPYRIGHT 2011 SCT Performance, LLC *********************
  * File Name          : bluetooth.h
  * Device / System    : SCT products using the "CommonCode" platform
  * Author             : Quyen Leba
  *
  * Version            : 1 
  * Date               : 05/02/2011
  * Description        : 
  *                    : 
  *
  *
  * History            : 05/02/2011 M. Davis
  *                    :   Add SCT Copyright header,
  *
  ******************************************************************************
  */

#ifndef __BLUETOOTH_H
#define __BLUETOOTH_H

#include <arch/gentype.h>
#include <board/commlink.h>
#include <common/cmdif.h>
#include "usartA.h"

#define BLUETOOTH_MAX_DEVICE_COUNT          7

// Config Var# definitions
#define VAR_BuildVersion                        1
#define VAR_BD_ADDR                             3
#define VAR_DeviceName                          4
#define VAR_StreamingSerial                     5
#define VAR_PIN                                 6
#define VAR_UartBaudrate                        7
#define VAR_DefaultSecurity                     40
#define VAR_DefaultAuthentication               41
#define VAR_iAPAppID                            44
#define VAR_iAPProtocolStrMain                  45
#define VAR_iAPProtocolStrAlt                   46
#define VAR_AccName                             57
#define VAR_AccManufacturer                     58
#define VAR_AccModelNumber                      59
#define VAR_AccSerialNumber                     60

// redirect which usart port to run bluetooth module
#define bluetooth_hal(func)         usartA_##func

static const u32 BluetoothBaudrateTable[] =
{
    //IMPORTANT: the index follow enum of SbBaudrate
    [0] = 2400,
    [1] = 4800,
    [2] = 7200,
    [3] = 9600,
    [4] = 19200,
    [5] = 38400,
    [6] = 57600,
    [7] = 115200,
    [8] = 230400,
    [9] = 460800,
    [10] = 921600,
};

u8 bluetooth_init();
void bluetooth_status_indicator_update();
void bluetooth_changebaudrate(CommLinkBaudrate btbaud);
u8 bluetooth_setcommlinkbaudrate(void);

void bluetooth_session_intr_handler();
void bluetooth_link_intr_handler();

u8 bluetooth_to_command_mode();
u8 bluetooth_to_bypass_mode();
u8 bluetooth_soft_reset(bool runcheck);
u8 bluetooth_hard_reset(bool runcheck);
u8 bluetooth_force115kBaud_btmodule(void);

u8 bluetooth_enable_autoreconnect(u8 reconnecttime);
u8 bluetooth_disable_autoreconnect();
u8 bluetooth_enter_bootloader();
u8 bluetooth_exit_bootloader();
u8 bluetooth_unpair();
u8 bluetooth_getfriendlyname(u8 *friendlyname);
u8 bluetooth_setfriendlyname(u8 *friendlyname);
u8 bluetooth_set_factory_default();
u8 bluetooth_getmacaddress(u8 *MACaddress);
u8 bluetooth_getversionstring(u8 *versionstr);
u8 bluetooth_changebaud_btmodule(CommLinkBaudrate btbaud);
//u8 bluetooth_changedefaultbaud_btmodule(CommLinkBaudrate btbaud);
u8 bluetooth_set_streamingserial(bool enable);

void bluetooth_uart_tx_byte(u8 data);
u8 bluetooth_uart_rx_byte(u8 *data);
void bluetooth_uart_rx_irq_handler();

void bluetooth_reset_rxbackup();
int bluetooth_get_rxbackup(u8 *returndata, u32 returndata_maxsize);

u8 bluetooth_get_config_var(u8 var_number, u8* outputstring, u32 output_maxlength);
u8 bluetooth_set_config_var(u8 var_number, u8* inputstring);

u8 bluetooth_set_AccName(u8* name);
u8 bluetooth_set_AccManufacturer(u8* manufacturer);
u8 bluetooth_set_AccModelNumber(u8* modelnumber);
u8 bluetooth_set_AccSerialNumber(u8* serialnumber);

#define bluetooth_uart_tx(data,len,session_required)  commlink_tx(data,len,session_required)

#endif    //__BLUETOOTH_H
