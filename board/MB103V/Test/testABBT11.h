/**
  **************** (C) COPYRIGHT 2011 SCT Performance, LLC *********************
  * File Name          : testABBT11.h
  * Device / System    : SCT products using the "CommonCode" platform
  * Author             : Quyen Leba
  *
  * Version            : 1 
  * Date               : 05/02/2011
  * Description        : 
  *                    : 
  *
  *
  * History            : 05/02/2011 M. Davis
  *                    :   Add SCT Copyright header,
  *
  ******************************************************************************
  */

#ifndef __TESTABBT11_H
#define __TESTABBT11_H

#include <arch/gentype.h>

void testABBT11_init();
u8 testABBT11_all();
u8 testABBT11_bluetooth();
u8 testABBT11_accelerometer();
u8 testABBT11_wirecom();

#endif  //__TESTABBT11_H
