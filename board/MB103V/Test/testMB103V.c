/**
  **************** (C) COPYRIGHT 2011 SCT Performance, LLC *********************
  * File Name          : testMB103V.c
  * Device / System    : SCT products using the "CommonCode" platform
  * Author             : Quyen Leba
  *
  * Version            : 1 
  * Date               : 05/02/2011
  * Description        : 
  *                    : 
  *
  *
  * History            : 05/02/2011 M. Davis
  *                    :   Add SCT Copyright header,
  *
  ******************************************************************************
  */

#include <board/genplatform.h>
#include <board/clock.h>
#include <board/interrupt.h>
#include <board/peripherals.h>
#include <common/crypto_blowfish.h>
#include <board/delays.h>
#include <fs/genfs.h>
#include <common/statuscode.h>
#include "test.h"
#include "testMB103V.h"

extern bool usb_enumeration_detected;               //from usb_prop.c

//------------------------------------------------------------------------------
// MainBoard Test Init
//------------------------------------------------------------------------------
void testMB103V_init()
{
    u8 status;
    
    status = clock_init();
    if(status != S_SUCCESS)
    {
        clock_emergency();
        //this function never returns
        led_emergency_indicator_system_error();
    }
    
    interrupt_init(DISABLE);
    crypto_blowfish_init();
    peripherals_init();
}

//------------------------------------------------------------------------------
// Test SDIO and SD card
// Return:  u8  status
//------------------------------------------------------------------------------
u8 testMB103V_SD()
{
    u8  status;

    genfs_init();
    status = genfs_format();
    if (status == S_SUCCESS)
    {
        status = genfs_reinit();
    }
    return status;
}

//------------------------------------------------------------------------------
// Test USB Disconnect Circuit and Enumeration
// Return:  u8  status
//------------------------------------------------------------------------------
u8 testMB103V_USB()
{
    u8  status;

    //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    // Test USB RE-ENUMERATION TODOPD: Move this to it's own test when we 
    // update App.
    //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    while(!button_isTrigger())
    {
        led_setState(LED_WHITE);
        delays(1500,'m');
        led_setState(LED_OFF);
        delays(1000,'m');
    }
    
    // Is USB Cable connected?
    if(usb_enumeration_detected == TRUE)
    {
        // Force disconnect/reconnect
        usb_enumeration_detected = FALSE;
        USB_Cable_Config(DISABLE);
        delays(100,'m');
        USB_Cable_Config(ENABLE);
        delays(1000,'m');
        // Check for re-enumeration
        if(usb_enumeration_detected != TRUE)
        {
            // Fail
            status = S_FAIL;
            test_errorlog("ERROR: Main Board - USB Disconnect HW Fail\r\n");
        }
        else
        {
            status = S_SUCCESS;
        }
    }
    else
    {
        // Fail, need to have USB connected before running test
        status = S_FAIL;
        test_errorlog("ERROR: Main Board - No USB connected\r\n");
    }
    
    return status;
}

//------------------------------------------------------------------------------
// Test LED and push button input, do this last. Requires to push the button
// and observe LEDs go RED->GREEN->BLUE->WHITE
// Input:   u8  current_status
//------------------------------------------------------------------------------
void testMB103V_IO(u8 current_status)
{
    if (current_status == S_SUCCESS)
    {
        led_setState(LED_GREEN);
    }
    else
    {
        led_setState(LED_RED);
    }

    while(1)
    {
        while(!button_isTrigger());

        led_setState(LED_RED);
        delays(800,'m');
        led_setState(LED_GREEN);
        delays(800,'m');
        led_setState(LED_BLUE);
        delays(800,'m');
        led_setState(LED_WHITE);
        delays(1000,'m');
        
        // Go back to solid test result light
        if (current_status == S_SUCCESS)
        {
            led_setState(LED_GREEN);
        }
        else
        {
            led_setState(LED_RED);
            test_printerrorlog();
        }
    }
}

//------------------------------------------------------------------------------
// Return:  u8  status
//------------------------------------------------------------------------------
u8 testMB103V_power()
{
    return S_SUCCESS;
}

//------------------------------------------------------------------------------
// Test ADCs on MainBoard
// Return:  u8  status
//------------------------------------------------------------------------------
u8 testMB103V_adc()
{
    return S_SUCCESS;
}
