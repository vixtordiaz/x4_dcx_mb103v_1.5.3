/**
  **************** (C) COPYRIGHT 2011 SCT Performance, LLC *********************
  * File Name          : testABBT11.c
  * Device / System    : SCT products using the "CommonCode" platform
  * Author             : Quyen Leba
  *
  * Version            : 1 
  * Date               : 05/02/2011
  * Description        : 
  *                    : 
  *
  *
  * History            : 05/02/2011 M. Davis
  *                    :   Add SCT Copyright header,
  *
  ******************************************************************************
  */

#include <string.h>
#include <board/genplatform.h>
#include <board/delays.h>
#include <MB103V/bluetooth.h>
#include <common/statuscode.h>
#include "test.h"
#include "testABBT11.h"

extern CommLinkInfo commlink_info;

//------------------------------------------------------------------------------
// AppBoard Test Init
//------------------------------------------------------------------------------
void testABBT11_init()
{
}

//------------------------------------------------------------------------------
// Test AppBoard
// Return: u8   returnstatus
//------------------------------------------------------------------------------
u8 testABBT11_all()
{
    u8  status;
    u8  returnstatus;

    returnstatus = S_SUCCESS;

    status = testABBT11_bluetooth();
    if (status != S_SUCCESS)
    {
        returnstatus = S_FAIL;
    }

    status = testABBT11_accelerometer();
    if (status != S_SUCCESS)
    {
        returnstatus = S_FAIL;
    }

    status = testABBT11_wirecom();
    if (status != S_SUCCESS)
    {
        returnstatus = S_FAIL;
    }

    return returnstatus;
}

#define BT_ALLOWED_BUILD_COUNT 5
//------------------------------------------------------------------------------
// Test Bluetooth module
// Return: u8   returnstatus
//------------------------------------------------------------------------------
u8 testABBT11_bluetooth()
{
    const u8 bt11_build[] = "at+ab build";
    const u8 bt11_cptest[] = "at+ab cptest";
    const u8 bt11_discovery[] = "at+ab discovery";
    const u8 bt11_buildversionstring[BT_ALLOWED_BUILD_COUNT][45] = 
    {
        "AT-AB abSerial Build Version 130322D IDPS",
        "AT-AB abSerial Build Version 110522B IDPS", 
        "AT-AB abSerial Build Version 110522C IDPS", 
        "AT-AB abSerial Build Version 110522G IDPS",
        "AT-AB abSerial Build Version 110522H IDPS"
    };
    const u8 bt11_cptestresponsestring[] = "CP Device Version: 0x03";
    const u8 bt11_discoverypending[] = "AT-AB DiscoveryPending";
    const u8 endline[2] = {0x0D, 0x0A};
    u32 bt_connection;
    u8  rxdatabuffer[256];
    u32 TO = 15;
    u8  returnstatus;

    returnstatus = S_SUCCESS;
    commlink_info.use_encryption = 1;

    bt_connection = gpio_peek_commlink_connection_status();
    if ((bt_connection & 0x08) == 0)
    {
        bluetooth_uart_tx("^#^$^%",6,FALSE);
        while(gpio_peek_commlink_connection_status() & 0x08 == 0);
    }

    // Check the Build Version
    bluetooth_reset_rxbackup();
    bluetooth_uart_tx((u8*)bt11_build,strlen((char*)bt11_build),FALSE);
    bluetooth_uart_tx((u8*)endline,2,FALSE);
    delays(100,'m');
    bluetooth_get_rxbackup(rxdatabuffer, sizeof(rxdatabuffer));
    
    for(int i=0;i<BT_ALLOWED_BUILD_COUNT;i++)
    {
        if(strncmp((char*)rxdatabuffer,(char*)bt11_buildversionstring[i],
                   strlen((char*)bt11_buildversionstring[i])) != 0)
        {
            returnstatus = S_FAIL;
        }
        else
        {
            returnstatus = S_SUCCESS;
            break;
        }
    }
    if(returnstatus == S_FAIL)
    {
        test_errorlog("ERROR: App Board - Bluetooth Build Mis-match\r\n");
    }

    // Check for Apple Co-Processor Connection
    bluetooth_reset_rxbackup();
    bluetooth_uart_tx((u8*)bt11_cptest,strlen((char*)bt11_cptest),FALSE);
    bluetooth_uart_tx((u8*)endline,2,FALSE);
    delays(100,'m');
    bluetooth_get_rxbackup(rxdatabuffer,sizeof(rxdatabuffer));
    
    if(strncmp((char*)rxdatabuffer,(char*)bt11_cptestresponsestring,
               strlen((char*)bt11_cptestresponsestring)) != 0)
    {
        returnstatus = S_FAIL;
        test_errorlog("ERROR: App Board - No Response from Apple CP\r\n");
    }

     // Check for radio operation, try discovery
    bluetooth_reset_rxbackup();
    bluetooth_uart_tx((u8*)bt11_discovery,strlen((char*)bt11_discovery),FALSE);
    bluetooth_uart_tx((u8*)endline,2,FALSE);
    delays(100,'m');
    while(TO)
    {
        bluetooth_get_rxbackup(rxdatabuffer,sizeof(rxdatabuffer));
        if(strlen((char*)rxdatabuffer) != 0)
        {
            if (strstr((char*)rxdatabuffer,(char*)bt11_discoverypending) != 0)
            {
                // BT Devices found;
                break;
            }
        }
        bluetooth_reset_rxbackup();
        delays(1000,'m');
        TO--;
    }
    if(!TO)
    {
        returnstatus = S_FAIL;
        test_errorlog("ERROR: App Board - Bluetooth Discovery Failed\r\n");
    }

    return returnstatus;
}

//------------------------------------------------------------------------------
// Test Accelerometer
// Return: u8   status
//------------------------------------------------------------------------------
u8 testABBT11_accelerometer()
{
    u8 status;
    u8 whoami;

    status = accelerometer_get_whoami(&whoami);
    if(status != S_SUCCESS)
    {
        test_errorlog("ERROR: App Board - Accelerometer Failed\r\n");
        return S_FAIL;
    }
    return S_SUCCESS;
}

//------------------------------------------------------------------------------
// Test WireCom port
// Return: u8   status
//------------------------------------------------------------------------------
u8 testABBT11_wirecom()
{
    u8  rxtemp[20];
    u32 TO;
    u32 i;
    u8  returnstatus;

    returnstatus = S_SUCCESS;
    //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    // Test USART B
    //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    usartB_init(BAUD115200,UART_NO_FLOWCONTROL,UART_USE_INTERRUPT);
    usartB_disable_rx_interrupt();

    memset(rxtemp, 0, sizeof(rxtemp));
    for(i=0;i<10;i++)
    {
        usartB_tx('0'+i);
        TO = 1000;
        while(!usartB_isdatavailable())
        {
            if(!TO--)
            {
                returnstatus = S_FAIL;
                test_errorlog("ERROR: App Board - UARTB Loop Back Failed\r\n");
                i = 100;
                break;
            }
        }
        rxtemp[i] = usartB_rx(); 
    }
    // PD NOTE: RS232 Transceiver causes bad first character. Workaround is skip it..
    if(strcmp((char*)(rxtemp+1), "123456789") != 0)
    {
        returnstatus = S_FAIL;
        test_errorlog("ERROR: App Board - UARTB Loop Back Failed\r\n");
    }

    //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    // Test USART C
    //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    uartC_init(BAUD115200,UART_NO_FLOWCONTROL,UART_USE_INTERRUPT);
    memset(rxtemp, 0, sizeof(rxtemp));
    for(i=0;i<10;i++)
    {
        uartC_tx('0'+i);
        TO = 1000;
        while(!uartC_isdatavailable())
        {
            if(!TO--)
            {
                returnstatus = S_FAIL;
                test_errorlog("ERROR: App Board - UARTB Loop Back Failed\r\n");
                i = 100;
                break;
            }
        }
        rxtemp[i] = uartC_rx();
    }
    if (strcmp((char*)rxtemp, "0123456789") != 0)
    {
        returnstatus = S_FAIL;
        test_errorlog("ERROR: App Board - UARTB Loop Back Failed\r\n");
    }

    return returnstatus;
}
