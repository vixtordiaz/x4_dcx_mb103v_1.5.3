/**
  **************** (C) COPYRIGHT 2011 SCT Performance, LLC *********************
  * File Name          : test.c
  * Device / System    : SCT products using the "CommonCode" platform
  * Author             : Quyen Leba
  *
  * Version            : 1 
  * Date               : 05/02/2011
  * Description        : 
  *                    : 
  *
  *
  * History            : 05/02/2011 M. Davis
  *                    :   Add SCT Copyright header,
  *
  ******************************************************************************
  */
#include <string.h>
#include <board/genplatform.h>
#include <board/bootloader.h>
#include <board/delays.h>
#include <common/statuscode.h>
#include <common/debug/debug_output.h>
#include "testABBT11.h"
#include "testMB103V.h"
#include "testVB103R.h"
#include "test.h"

#define TESTLOG_MAX_LENGTH          6*1024

struct
{
    u8  *log;
}test_info =
{
    .log = NULL,
};

//------------------------------------------------------------------------------
// Init Production Test
// Engineer: Quyen Leba
//------------------------------------------------------------------------------
void test_init()
{
    if (test_info.log)
    {
        __free(test_info.log);
    }
    test_info.log = __malloc(TESTLOG_MAX_LENGTH);
    if (test_info.log)
    {
        strcpy((char*)test_info.log,"---- Test Error Log ----\r\n");
    }
}

//------------------------------------------------------------------------------
// 
//------------------------------------------------------------------------------
void test_errorlog(u8 *err_str)
{
    if (test_info.log)
    {
        if (strlen((char*)test_info.log) + strlen((char*)err_str) < TESTLOG_MAX_LENGTH)
        {
            strcat((char*)test_info.log,(char*)err_str);
        }
    }
}

//------------------------------------------------------------------------------
// 
//------------------------------------------------------------------------------
void test_printerrorlog()
{
    if (test_info.log)
    {
        debug_out(test_info.log);
    }
}

//------------------------------------------------------------------------------
// Execute Production Test
// Engineer: Quyen Leba
//------------------------------------------------------------------------------
void test_exec()
{
    u8  status;
    u8  returnstatus;

    returnstatus = S_SUCCESS;

    testMB103V_init();

    //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    // Test MainBoard SDIO and SD card
    //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    status = testMB103V_SD();
    if(status != S_SUCCESS)
    {
        returnstatus = S_FAIL;
        test_errorlog("ERROR: Main Board - Filesystem Format Failed\r\n");
    }
    
    //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    // Test VehicleBoard
    //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    testVB103R_init();
    status = testVB103R_all();
    if(status != S_SUCCESS)
    {
        returnstatus = S_FAIL;
        test_errorlog("ERROR: VehicleBoard - Did Not Pass\r\n");
    }

    //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    // Test AppBoard
    //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    testABBT11_init();
    status = testABBT11_all();
    if(status != S_SUCCESS)
    {
        returnstatus = S_FAIL;
        test_errorlog("ERROR: AppBoard - Did Not Pass\r\n");
    }

    //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    // Test MainBoard USB Disconnect and Enumeration
    //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    status = testMB103V_USB();
    if(status != S_SUCCESS)
    {
        returnstatus = S_FAIL;
        test_errorlog("ERROR: Main Board - USB HW Fail\r\n");
    }
    
    //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    // Test MainBoard LEDs and push button
    //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    testMB103V_IO(returnstatus);
    //never return
}
