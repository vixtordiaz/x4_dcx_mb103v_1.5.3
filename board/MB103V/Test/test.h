/**
  **************** (C) COPYRIGHT 2011 SCT Performance, LLC *********************
  * File Name          : test.h
  * Device / System    : SCT products using the "CommonCode" platform
  * Author             : Quyen Leba
  *
  * Version            : 1 
  * Date               : 05/02/2011
  * Description        : 
  *                    : 
  *
  *
  * History            : 05/02/2011 M. Davis
  *                    :   Add SCT Copyright header,
  *
  ******************************************************************************
  */

#ifndef __TEST_H
#define __TEST_H

#include <arch/gentype.h>

void test_init();
void test_errorlog(u8 *err_str);
void test_printerrorlog();
void test_exec();

#endif  //__TEST_H
