/**
  **************** (C) COPYRIGHT 2011 SCT Performance, LLC *********************
  * File Name          : testVB103R.c
  * Device / System    : SCT products using the "CommonCode" platform
  * Author             : Quyen Leba
  *
  * Version            : 1 
  * Date               : 05/02/2011
  * Description        : 
  *                    : 
  *
  *
  * History            : 05/02/2011 M. Davis
  *                    :   Add SCT Copyright header,
  *
  ******************************************************************************
  */

#include <board/genplatform.h>
#include <board/clock.h>
#include <board/interrupt.h>
#include <board/peripherals.h>
#include <board/properties_vb.h>
#include <board/power.h>
#include <board/delays.h>
#include <fs/genfs.h>
#include <common/obd2.h>
#include <common/statuscode.h>
#include "test.h"
#include "testVB103R.h"

//------------------------------------------------------------------------------
// VehicleBoard Test Init
//------------------------------------------------------------------------------
void testVB103R_init()
{
    
}

//------------------------------------------------------------------------------
// Test VehicleBoard
// Return: u8   status
//------------------------------------------------------------------------------
u8 testVB103R_all()
{
    u8  status;
    u8  returnstatus;

    returnstatus = S_SUCCESS;
    
    status = testVB103R_check_boardrevision();
    if (status != S_SUCCESS)
    {
        returnstatus = S_FAIL;
    }

    status = testVB103R_power_adc();
    if (status != S_SUCCESS)
    {
        returnstatus = S_FAIL;
    }

    status = testVB103V_vehiclecomm();
    if (status != S_SUCCESS)
    {
        returnstatus = S_FAIL;
    }

    return returnstatus;
}

//------------------------------------------------------------------------------
// Check Vehicle Board, Board Revision for correct revision
// Checks for Board Revision: 1
// NOTE: THIS MUST BE UPDATED IF THE BOARD REVISION CHANGES!
// Return: u8   status
//------------------------------------------------------------------------------
#define VB_CURRENT_BOARD_REV    0x01
#define VB_PREVIOUS_BOARD_REV   0x00
u8 testVB103R_check_boardrevision()
{
    u8  status;
    u32 properties_vb_info_length;
    
    struct
    {
        u32 operating_mode;
        u32 failsafeboot_version;
        u32 mainboot_version;
        u32 app_signature;
        u32 app_version;
        u16 code_sector_size;
        u8  board_rev;
        u8  secboot_startsector;
        u16 secboot_maxcodesize;
        u8  reserved1;
        u8  app_startsector;
        u32 app_maxcodesize;
        u8  reserved[16];
    }properties_vb_info;
        
    status = properties_vb_getinfo((u8*)&properties_vb_info, &properties_vb_info_length);    
    if((status != S_SUCCESS) && ((properties_vb_info.board_rev != VB_CURRENT_BOARD_REV) 
       || (properties_vb_info.board_rev != VB_PREVIOUS_BOARD_REV)))
    {
        test_errorlog("ERROR: Vehicle Board - Board Revision Fail");
        return S_FAIL;
    }
    
    return S_SUCCESS;
}

//------------------------------------------------------------------------------
// Test power and ADCs on VehicleBoard
// Return: u8   status
//------------------------------------------------------------------------------
u8 testVB103R_power_adc()
{
    float adcvalue;
    bool ok;
    u8  buffer[64];
    u8  state;
    u8  returnstatus;
    u8  status;

    returnstatus = S_SUCCESS;

    // VPP test
    status = Vpp_Ctrl(Vpp_FEPS);         // Set to 18V 
    delays(100,'m'); // Allow time for stabilization
    status = adc_read(ADC_VPP, (float*)&adcvalue);
    if(adcvalue < 17.0 || adcvalue > 19.0 || status != S_SUCCESS)
    {
        returnstatus = S_FAIL;
        test_errorlog("ERROR: Vehicle Board - VPP 18V FAIL >> ");
        if (status == S_SUCCESS)
        {
            sprintf((char*)buffer,"%3.2fV", adcvalue);
            test_errorlog(buffer);
        }
        else
        {
            test_errorlog("Can't read");
        }
        test_errorlog("\r\n");
    }

    status = Vpp_Ctrl(Vpp_OFF);
    delays(100,'m');

    status = Vpp_Ctrl(Vpp_SCI_BE_12);   // Set to 20V 
    delays(100,'m'); // Allow time for stabilization
    status = adc_read(ADC_VPP, (float*)&adcvalue);
    if(adcvalue < 19.0 || adcvalue > 24.0 || status != S_SUCCESS)
    {
        returnstatus = S_FAIL;
        test_errorlog("ERROR: Vehicle Board - VPP 20V FAIL >> ");
        if (status == S_SUCCESS)
        {
            sprintf((char*)buffer,"%3.2fV", adcvalue);
            test_errorlog(buffer);
        }
        else
        {
            test_errorlog("Can't read");
        }
        test_errorlog("\r\n");
    }
    status = Vpp_Ctrl(Vpp_OFF);

    // VBatt test
    status = adc_read(ADC_VBAT, (float*)&adcvalue);
    if(adcvalue < 10 || status != S_SUCCESS)
    {
        returnstatus = S_FAIL;
        test_errorlog("ERROR: Vehicle Board - VBAT FAIL >> ");
        if (status == S_SUCCESS)
        {
            sprintf((char*)buffer,"%3.2fV", adcvalue);
            test_errorlog(buffer);
        }
        else
        {
            test_errorlog("Can't read");
        }
        test_errorlog("\r\n");
    }

    // Test 5VExt and all analog inputs
    V5VExt_Ctrl(ENABLE);
    delays(100,'m'); // Allow time for stabilization
    status = adc_read(ADC_AIN1, (float*)&adcvalue);
    if(adcvalue < 4 || status != S_SUCCESS)
    {
        returnstatus = S_FAIL;
        test_errorlog("ERROR: Vehicle Board - AIN1 FAIL\r\n");
    }
    status = adc_read(ADC_AIN2, (float*)&adcvalue);
    if(adcvalue < 4 || status != S_SUCCESS)
    {
        returnstatus = S_FAIL;
        test_errorlog("ERROR: Vehicle Board - AIN2 FAIL\r\n");
    }
    status = adc_read(ADC_AIN3, (float*)&adcvalue);
    if(adcvalue < 4 || status != S_SUCCESS)
    {
        returnstatus = S_FAIL;
        test_errorlog("ERROR: Vehicle Board - AIN3 FAIL\r\n");
    }
    status = adc_read(ADC_AIN4, (float*)&adcvalue);
    if(adcvalue < 4 || status != S_SUCCESS)
    {
        returnstatus = S_FAIL;
        test_errorlog("ERROR: Vehicle Board - AIN4 FAIL\r\n");
    }
    status = adc_read(ADC_AIN5, (float*)&adcvalue);
    if(adcvalue < 4 || status != S_SUCCESS)
    {
        returnstatus = S_FAIL;
        test_errorlog("ERROR: Vehicle Board - AIN5 FAIL\r\n");
    }
    status = adc_read(ADC_AIN6, (float*)&adcvalue);
    if(adcvalue < 4 || status != S_SUCCESS)
    {
        returnstatus = S_FAIL;
        test_errorlog("ERROR: Vehicle Board - AIN6 FAIL\r\n");
    }
    
    // Test GPIO1_Out_5V and GPIO2_In
    // ON
    status = adc_config_gpio1_out(TRUE);
    state = adc_peek_gpio2_in(&ok);
    if (status != S_SUCCESS || !ok || state != Bit_SET)
    {
        returnstatus = S_FAIL;
        test_errorlog("ERROR: Vehicle Board - GPIO ON FAIL\r\n");
    }
    // OFF
    status = adc_config_gpio1_out(FALSE);
    state = adc_peek_gpio2_in(&ok);
    if (status != S_SUCCESS || !ok || state != Bit_RESET)
    {
        returnstatus = S_FAIL;
        test_errorlog("ERROR: Vehicle Board - GPIO OFF FAIL\r\n");
    }

    V5VExt_Ctrl(DISABLE);
    delays(100,'m'); // Allow time for stabilization
    status = adc_read(ADC_AIN1, (float*)&adcvalue);
    if((adcvalue > 1.0) || status != S_SUCCESS)
    {
        returnstatus = S_FAIL;
        test_errorlog("ERROR: Vehicle Board - AIN1 FAIL\r\n");
    }
    status = adc_read(ADC_AIN2, (float*)&adcvalue);
    if((adcvalue > 1.0) || status != S_SUCCESS)
    {
        returnstatus = S_FAIL;
        test_errorlog("ERROR: Vehicle Board - AIN2 FAIL\r\n");
    }
    status = adc_read(ADC_AIN3, (float*)&adcvalue);
    if((adcvalue > 1.0) || status != S_SUCCESS)
    {
        returnstatus = S_FAIL;
        test_errorlog("ERROR: Vehicle Board - AIN3 FAIL\r\n");
    }
    status = adc_read(ADC_AIN4, (float*)&adcvalue);
    if((adcvalue > 1.0) || status != S_SUCCESS)
    {
        returnstatus = S_FAIL;
        test_errorlog("ERROR: Vehicle Board - AIN4 FAIL\r\n");
    }
    status = adc_read(ADC_AIN5, (float*)&adcvalue);
    if((adcvalue > 1.0) || status != S_SUCCESS)
    {
        returnstatus = S_FAIL;
        test_errorlog("ERROR: Vehicle Board - AIN5 FAIL\r\n");
    }
    status = adc_read(ADC_AIN6, (float*)&adcvalue);
    if((adcvalue > 1.0) || status != S_SUCCESS)
    {
        returnstatus = S_FAIL;
        test_errorlog("ERROR: Vehicle Board - AIN6 FAIL\r\n");
    }

    return returnstatus;
}

//------------------------------------------------------------------------------
// Test vehicle comm on VehicleBoard
// Return: u8   status
//------------------------------------------------------------------------------
u8 testVB103V_vehiclecomm()
{
    vehicle_info vehicleinfo;
    VehicleCommType vehcomm;
    u8  status;
    u8  returnstatus;

    returnstatus = S_SUCCESS;
    // Test CAN communications
    vehcomm = CommType_CAN;
    status = obd2_readecminfo(&vehicleinfo.ecm, &vehcomm);
    if (status != S_SUCCESS)
    {
        returnstatus = S_FAIL;
        test_errorlog("ERROR: Vehicle Board - CAN FAIL\r\n");
    }
    
    // Test SCP communications
    vehcomm = CommType_SCP;
    status = obd2_readecminfo(&vehicleinfo.ecm, &vehcomm);
    if (status != S_SUCCESS)
    {
        returnstatus = S_FAIL;
        test_errorlog("ERROR: Vehicle Board - SCP FAIL\r\n");
    }

    return returnstatus;
}