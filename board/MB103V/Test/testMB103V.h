/**
  **************** (C) COPYRIGHT 2011 SCT Performance, LLC *********************
  * File Name          : testMB103V.h
  * Device / System    : SCT products using the "CommonCode" platform
  * Author             : Quyen Leba
  *
  * Version            : 1 
  * Date               : 05/02/2011
  * Description        : 
  *                    : 
  *
  *
  * History            : 05/02/2011 M. Davis
  *                    :   Add SCT Copyright header,
  *
  ******************************************************************************
  */

#ifndef __TESTMB103V_H
#define __TESTMB103V_H

#include <arch/gentype.h>

void testMB103V_init(void);
u8 testMB103V_SD();
void testMB103V_IO(u8 current_status);
u8 testMB103V_power();
u8 testMB103V_adc();
u8 testMB103V_USB();

#endif  //__TESTMB103V_H
