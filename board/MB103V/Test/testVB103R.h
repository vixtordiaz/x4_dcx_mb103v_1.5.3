/**
  **************** (C) COPYRIGHT 2011 SCT Performance, LLC *********************
  * File Name          : testVB103R.h
  * Device / System    : SCT products using the "CommonCode" platform
  * Author             : Quyen Leba
  *
  * Version            : 1 
  * Date               : 05/02/2011
  * Description        : 
  *                    : 
  *
  *
  * History            : 05/02/2011 M. Davis
  *                    :   Add SCT Copyright header,
  *
  ******************************************************************************
  */

#ifndef __TESTVB103R_H
#define __TESTVB103R_H

#include <arch/gentype.h>

void testVB103R_init();
u8 testVB103R_all();
u8 testVB103R_power_adc();
u8 testVB103V_vehiclecomm();
u8 testVB103R_check_boardrevision();

#endif  //__TESTVB103R_H
