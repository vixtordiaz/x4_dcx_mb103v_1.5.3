/**
  **************** (C) COPYRIGHT 2011 SCT Performance, LLC *********************
  * File Name          : commlink_hal_bluetooth.c
  * Device / System    : SCT products using the "CommonCode" platform
  * Author             : Quyen Leba
  *
  * Version            : 1 
  * Date               : 05/02/2011
  * Description        : 
  *                    : 
  *
  *
  * History            : 05/02/2011 M. Davis
  *                    :   Add SCT Copyright header,
  *
  ******************************************************************************
  */

#include <board/commlink.h>
#include <common/statuscode.h>
#include "bluetooth.h"
#include "commlink_hal_bluetooth.h"

extern CommLinkInfo commlink_info;

//------------------------------------------------------------------------------
// 
// Return:  u8  status
// Engineer: Quyen Leba
//------------------------------------------------------------------------------
u8 commlink_hal_init()
{
    return bluetooth_init();
}

//------------------------------------------------------------------------------
// Update current status to indicator
// Engineer: Quyen Leba
//------------------------------------------------------------------------------
void commlink_hal_status_indicator_update()
{
    bluetooth_status_indicator_update();
}

//------------------------------------------------------------------------------
// Get device name
// Output:   u8 *name
// Return:  u8  status
// Engineer: Quyen Leba
//------------------------------------------------------------------------------
u8 commlink_hal_get_device_name(u8 *name)
{
    return bluetooth_getfriendlyname(name);
}

//------------------------------------------------------------------------------
// Input:   CommLinkBaudrate baudrate
// Return:  u8  status
// Engineer: Quyen Leba
//------------------------------------------------------------------------------
u8 commlink_hal_change_baudrate(CommLinkBaudrate baudrate)
{
    bluetooth_changebaudrate(baudrate);
    bluetooth_reset_rxbackup();
    return S_SUCCESS;
}

//------------------------------------------------------------------------------
// Input:   u8 *data
//          u16 length
//          bool session_required (TRUE: only tx if valid session)
// Return:  u8  status
// Engineer: Quyen Leba
//------------------------------------------------------------------------------
u8 commlink_hal_tx(u8 *data, u16 length, bool session_required)
{
#define COMMLINK_HAL_INACTIVITY_TIME_RESET      20  //seconds
    u16 i = 0;
    u32 timeout;
    
    timeout = timeout_counter_gettime_in_sec();
    timeout += COMMLINK_HAL_INACTIVITY_TIME_RESET;
    
    while(i < length)
    {        
        if (session_required &&
            commlink_info.link != CommLinkInfoLink_Opened)
        {
            goto commlink_hal_tx_session_dropped;
        }
        
        // Wait for UART Ready To Send
        while(!bluetooth_hal(readytosend)())
        {
            if(timeout_counter_gettime_in_sec() > timeout)
            {
                // RTS timeout
                commlink_set_halt_sending_ack();
                bluetooth_init(); // if this failed, reset system
                return S_TIMEOUT;
            }
        }
        
        // Send UART data
        bluetooth_hal(tx)(data[i++]);
        commlink_info.connection_history.LastTransmission = timeout_counter_gettime_in_sec();
    }//while(i < length)...
    return S_SUCCESS;

commlink_hal_tx_session_dropped:  // open session was required, but not available
    commlink_info.connection_history.LastTransmission = timeout_counter_gettime_in_sec();

    return S_SESSION_DROPPED;
}

//------------------------------------------------------------------------------
// Input:   u8 data
//------------------------------------------------------------------------------
void commlink_hal_tx_byte(u8 data)
{
    bluetooth_hal(tx)(data);
}
