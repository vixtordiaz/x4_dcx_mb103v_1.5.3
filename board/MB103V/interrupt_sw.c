/**
  **************** (C) COPYRIGHT 2011 SCT Performance, LLC *********************
  * File Name          : interrupt_sw.c
  * Device / System    : SCT products using the "CommonCode" platform
  * Author             : Quyen Leba
  *
  * Version            : 1 
  * Date               : 05/02/2011
  * Description        : 
  *                    : 
  *
  *
  * History            : 05/02/2011 M. Davis
  *                    :   Add SCT Copyright header,
  *
  ******************************************************************************
  */

#include <misc.h>
#include <board/genplatform.h>
#include <common/statuscode.h>
#include "interrupt_sw.h"

struct
{
    struct
    {
        interrupt_sw_func func;
        u8  fire_once       : 1;
        u8  privdata_length : 7;
        u8  privdata[SW_INTR_PRIVDATA_MAX_LENGTH];
    }item[3];
}interrupt_sw_info =
{
    .item[0].func = NULL, .item[0].fire_once = 1, .item[0].privdata_length = 0,
    .item[1].func = NULL, .item[1].fire_once = 1, .item[1].privdata_length = 0,
    .item[2].func = NULL, .item[2].fire_once = 1, .item[2].privdata_length = 0,
};

#define SW_INTR_TRIGGER_0_LINE          EXTI_Line1
#define SW_INTR_TRIGGER_0_IRQ           EXTI1_IRQn
#define SW_INTR_TRIGGER_1_LINE          EXTI_Line2
#define SW_INTR_TRIGGER_1_IRQ           EXTI2_IRQn
#define SW_INTR_TRIGGER_2_LINE          EXTI_Line3
#define SW_INTR_TRIGGER_2_IRQ           EXTI3_IRQn

u8 __interrupt_sw_trigger_to_index(SoftwareIntrTrigger trigger, bool *isOK);

//------------------------------------------------------------------------------
// Init software interrupt
// Engineer: Quyen Leba
//------------------------------------------------------------------------------
void interrupt_sw_init()
{
    EXTI_InitTypeDef EXTI_InitStructure;

    //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    // Configure sw interrupt 0
    //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    EXTI_InitStructure.EXTI_Line = SW_INTR_TRIGGER_0_LINE;
    EXTI_InitStructure.EXTI_Mode = EXTI_Mode_Interrupt;
    EXTI_InitStructure.EXTI_Trigger = EXTI_Trigger_Rising_Falling;
    EXTI_InitStructure.EXTI_LineCmd = ENABLE;
    EXTI_Init(&EXTI_InitStructure);

    //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    // Configure sw interrupt 1
    //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    EXTI_InitStructure.EXTI_Line = SW_INTR_TRIGGER_1_LINE;
    EXTI_InitStructure.EXTI_Mode = EXTI_Mode_Interrupt;
    EXTI_InitStructure.EXTI_Trigger = EXTI_Trigger_Rising_Falling;
    EXTI_InitStructure.EXTI_LineCmd = ENABLE;
    EXTI_Init(&EXTI_InitStructure);

   
    //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    // Configure sw interrupt 2
    //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    EXTI_InitStructure.EXTI_Line = SW_INTR_TRIGGER_2_LINE;
    EXTI_InitStructure.EXTI_Mode = EXTI_Mode_Interrupt;
    EXTI_InitStructure.EXTI_Trigger = EXTI_Trigger_Rising_Falling;
    EXTI_InitStructure.EXTI_LineCmd = ENABLE;
    EXTI_Init(&EXTI_InitStructure);

    
}

//------------------------------------------------------------------------------
// Translate trigger to its index presentation
// Input:   SoftwareIntrTrigger trigger
// Output:  bool *isOK (NULL allowed)
// Return:  u8  index of trigger
// Engineer: Quyen Leba
//------------------------------------------------------------------------------
u8 __interrupt_sw_trigger_to_index(SoftwareIntrTrigger trigger, bool *isOK)
{
    u8  i;

    if (isOK)
    {
        *isOK = TRUE;
    }
    switch(trigger)
    {
    case SoftwareIntrTrigger_0:
        i = 0;
        break;
    case SoftwareIntrTrigger_1:
        i = 1;
        break;
    case SoftwareIntrTrigger_2:
        i = 2;
        break;
    default:
        i = 0;
        if (isOK)
        {
            *isOK = FALSE;
        }
        break;
    }
    return i;
}

//------------------------------------------------------------------------------
// Trigger a software interrupt
// Input:   SoftwareIntrTrigger trigger
// Return:  u8  status
// Engineer: Quyen Leba
//------------------------------------------------------------------------------
u8 interrupt_sw_trigger(SoftwareIntrTrigger trigger)
{
    switch(trigger)
    {
    case SoftwareIntrTrigger_0:
        EXTI_GenerateSWInterrupt(SW_INTR_TRIGGER_0_LINE);
        break;
    case SoftwareIntrTrigger_1:
        EXTI_GenerateSWInterrupt(SW_INTR_TRIGGER_1_LINE);
        break;
    case SoftwareIntrTrigger_2:
        EXTI_GenerateSWInterrupt(SW_INTR_TRIGGER_2_LINE);
        break;
    default:
        return S_NOTSUPPORT;
    }
    return S_SUCCESS;
}

//------------------------------------------------------------------------------
// Set a function to trigger
// Inputs:  SoftwareIntrTrigger trigger
//          interrupt_sw_func func (trigger function)
//          bool fire_once (TRUE: trigger only one time)
// Return:  u8  status
// Engineer: Quyen Leba
//------------------------------------------------------------------------------
u8 interrupt_sw_trigger_set_func(SoftwareIntrTrigger trigger,
                                 interrupt_sw_func func, bool fire_once)
{
    bool isOK;
    u8  index;

    index = __interrupt_sw_trigger_to_index(trigger, &isOK);
    if (!isOK)
    {
        return S_NOTSUPPORT;
    }

    interrupt_sw_info.item[index].func = func;
    interrupt_sw_info.item[index].fire_once = fire_once;
    return S_SUCCESS;
}

//------------------------------------------------------------------------------
// Set a function to trigger
// Input:   SoftwareIntrTrigger trigger
// Return:  u8  status
// Engineer: Quyen Leba
//------------------------------------------------------------------------------
u8 interrupt_sw_trigger_clear_func(SoftwareIntrTrigger trigger)
{
    bool isOK;
    u8  index;

    index = __interrupt_sw_trigger_to_index(trigger, &isOK);
    if (!isOK)
    {
        return S_NOTSUPPORT;
    }

    interrupt_sw_info.item[index].func = NULL;
    interrupt_sw_info.item[index].privdata_length = 0;
    return S_SUCCESS;
}

/**
 * @brief   Set privdata for software interrupt function
 *
 * @param   [in] trigger
 * @param   [in] data
 * @param   [in] datalength
 *
 * @retval  u8 status
 *
 * @author  Quyen Leba
 *
 * @details Privdata is used by the trigger function
 */
u8 interrupt_sw_trigger_set_privdata(SoftwareIntrTrigger trigger,
                                     u8 *data, u8 datalength)
{
    bool isOK;
    u8  index;

    if (data == NULL || datalength > SW_INTR_PRIVDATA_MAX_LENGTH)
    {
        return S_INPUT;
    }

    index = __interrupt_sw_trigger_to_index(trigger, &isOK);
    if (!isOK)
    {
        return S_NOTSUPPORT;
    }

    memcpy((char*)interrupt_sw_info.item[index].privdata,(char*)data,datalength);
    interrupt_sw_info.item[index].privdata_length = datalength;

    return S_SUCCESS;
}

/**
 * @brief   Get privdata for software interrupt function
 *
 * @param   [in]  trigger
 * @param   [out] data
 * @param   [out] datalength
 *
 * @retval  u8 status
 *
 * @author  Quyen Leba
 *
 * @details Privdata is used by the trigger function
 */
u8 interrupt_sw_trigger_get_privdata(SoftwareIntrTrigger trigger,
                                     u8 *data, u8 *datalength)
{
    bool isOK;
    u8  index;

    if (data == NULL || datalength == NULL)
    {
        return S_INPUT;
    }

    *datalength = 0;
    index = __interrupt_sw_trigger_to_index(trigger, &isOK);
    if (!isOK)
    {
        return S_NOTSUPPORT;
    }

    if (interrupt_sw_info.item[index].privdata_length > SW_INTR_PRIVDATA_MAX_LENGTH)
    {
        return S_BADCONTENT;
    }

    if (interrupt_sw_info.item[index].privdata_length)
    {
        memcpy((char*)data,(char*)interrupt_sw_info.item[index].privdata,
               interrupt_sw_info.item[index].privdata_length);
        *datalength = interrupt_sw_info.item[index].privdata_length;
    }

    return S_SUCCESS;
}

//------------------------------------------------------------------------------
// Interrupt handler of trigger 0 (ext interrupt line 1)
// Engineer: Quyen Leba
//------------------------------------------------------------------------------
void interrupt_sw_trigger_handler_0()
{
    if (interrupt_sw_info.item[0].func)
    {
        interrupt_sw_info.item[0].func();
    }
    if (interrupt_sw_info.item[0].fire_once)
    {
        interrupt_sw_trigger_clear_func(SoftwareIntrTrigger_0);
        EXTI_ClearITPendingBit(SW_INTR_TRIGGER_0_LINE);
    }
}

//------------------------------------------------------------------------------
// Interrupt handler of trigger 1 (ext interrupt line 2)
// Engineer: Quyen Leba
//------------------------------------------------------------------------------
void interrupt_sw_trigger_handler_1()
{
    if (interrupt_sw_info.item[1].func)
    {
        interrupt_sw_info.item[1].func();
    }
    if (interrupt_sw_info.item[1].fire_once)
    {
        interrupt_sw_trigger_clear_func(SoftwareIntrTrigger_1);
        EXTI_ClearITPendingBit(SW_INTR_TRIGGER_1_LINE);
    }
}

//------------------------------------------------------------------------------
// Interrupt handler of trigger 2 (ext interrupt line 3)
// Engineer: Quyen Leba
//------------------------------------------------------------------------------
void interrupt_sw_trigger_handler_2()
{
    if (interrupt_sw_info.item[2].func)
    {
        interrupt_sw_info.item[2].func();
    }
    if (interrupt_sw_info.item[2].fire_once)
    {
        interrupt_sw_trigger_clear_func(SoftwareIntrTrigger_2);
        EXTI_ClearITPendingBit(SW_INTR_TRIGGER_0_LINE);
    }
}
