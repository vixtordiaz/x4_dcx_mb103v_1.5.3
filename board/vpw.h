/**
  **************** (C) COPYRIGHT 2011 SCT Performance, LLC *********************
  * File Name          : vpw.h
  * Device / System    : SCT products using the "CommonCode" platform
  * Author             : Quyen Leba
  *
  * Version            : 1 
  * Date               : 05/02/2011
  * Description        : 
  *                    : 
  *
  *
  * History            : 05/02/2011 M. Davis
  *                    :   Add SCT Copyright header,
  *
  ******************************************************************************
  */

#ifndef __VPW_H__
#define __VPW_H__

#include <arch/gentype.h>

#define VPW_MODE_FUNCTIONAL         1
#define VPW_MODE_NODE               0

#define VPW_FLAGS_FUNCTIONALADDR    (1<<6)

//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
u8 vpw_init(bool boosted, bool highspeed);
u8 vpw_tx(u8 *databuffer, u16 datalength);
u8 vpw_rx(u8 *databuffer, u16 *datalength, u16 timeout, u8 mode, u8 funcaddr);
u8 vpw_txrx_simple(u8 *tx_databuffer, u16 tx_datalength,
                   u8 *rx_databuffer, u16 *rx_datalength);
u8 vpw_send_break();

#endif  //__VPW_H__
