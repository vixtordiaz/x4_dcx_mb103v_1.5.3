/**
  **************** (C) COPYRIGHT 2011 SCT Performance, LLC *********************
  * File Name          : bootloader_ab_ken.h
  * Device / System    : SCT products using the "CommonCode" platform
  * Author             : Quyen Leba
  *
  * Version            : 1 
  * Date               : 05/02/2011
  * Description        : 
  *                    : 
  *
  *
  * History            : 05/02/2011 M. Davis
  *                    :   Add SCT Copyright header,
  *
  ******************************************************************************
  */

#ifndef __BOOTLOADER_AB_KEN_H
#define __BOOTLOADER_AB_KEN_H

#include <arch/gentype.h>
#include <board/bootloader.h>

#define AB_FAILSAFE_BOOTLOADER_OPERATING_MODE   0x6E2A7315
#define AB_MAIN_BOOTLOADER_OPERATING_MODE       0x2D81F35A
#define AB_APPLICATION_OPERATING_MODE           0x377C19B2

#define AB_FWUPDATE_TEMP    "abupdatetemp.bin"

u8 bootloader_ab_init(FirmwareHeader fwheader);
u8 bootloader_ab_init_file(FirmwareHeader fwheader);
u8 bootloader_ab_do(u8 *data, u32 datalength);
u8 bootloader_ab_do_file(u8 *data, u32 datalength);
u8 bootloader_ab_validate();
u8 bootloader_ab_validate_file();
u8 bootloader_ab_getversion(u32 *fwversion);
u8 bootloader_ab_setoperatingmode(u32 mode);
u8 bootloader_ab_getoperatingmode(u32 *mode);
u8 bootloader_ab_updatebyfile();

u8 bootloader_ab_start_bootloader(BootloaderMode mode, bool backlighton);
u8 bootloader_ab_setbootmode(BootloaderMode newmode);
u8 bootloader_ab_progressbar(u8 percentage, u8 *message);
void bootloader_ab_resetandrun();

#endif  //__BOOTLOADER_AB_KEN_H
