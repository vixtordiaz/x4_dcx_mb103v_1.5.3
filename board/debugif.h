/**
  **************** (C) COPYRIGHT 2011 SCT Performance, LLC *********************
  * File Name          : debugif.c
  * Device / System    : SCT products using the "CommonCode" platform
  * Author             : Patrick Downs
  *
  * Version            : 1 
  * Date               : 05/02/2011
  * Description        : 
  *                    : 
  *
  *
  * History            : 05/23/2011 P. Downs
  *                    :   Created
  *
  ******************************************************************************
  */
#ifndef __DEBUGIF_H
#define __DEBUGIF_H

#include <arch/gentype.h>

typedef enum
{
    DEBUGIF_IN_NORMAL           = 0,
    DEBUGIF_IN_PASSTHROUGH      = 1,
}DEBUGIF_IN_MODE;

u8 debugif_init();
void debugif_out(u8* dataout, u32 size);
u8 debugif_in(u8* datain, u32 *size);
void debugif_uart_rx_reset();
void debugif_setmode(DEBUGIF_IN_MODE mode);
void debugif_uart_rx_irq_handler();
void debugif_uart_rx_process_input(u8 inbyte);

#endif //__DEBUGIF_H
