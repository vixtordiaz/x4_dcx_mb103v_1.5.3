/**
  **************** (C) COPYRIGHT 2011 SCT Performance, LLC *********************
  * File Name          : media.c
  * Device / System    : SCT products using the "CommonCode" platform
  * Author             : Quyen Leba
  *
  * Version            : 1 
  * Date               : 05/02/2011
  * Description        : 
  *                    : 
  *
  *
  * History            : 05/02/2011 M. Davis
  *                    :   Add SCT Copyright header,
  *
  ******************************************************************************
  */

#include <fs/genfs.h>
#include <common/statuscode.h>
#include "media.h"

#define MEDIA_SECTOR_SIZE   GENFS_SECTOR_SIZE

struct
{
    u32 total_length;       //in kB
    u32 current_sector;
}media_info = 
{
    .total_length = 0,
    .current_sector = 0,
};

//------------------------------------------------------------------------------
// Init media FTL session
// Inputs:  u16 chunk_index (each chunk is chunk_size)
//          u32 chunk_size
// Return:  u8  status
// Engineer: Quyen Leba
//------------------------------------------------------------------------------
u8 media_ftlinit(u16 chunk_index, u32 chunk_size)
{
    media_info.current_sector = (chunk_index * chunk_size) / MEDIA_SECTOR_SIZE;
    return genfs_storage_init();
}

//------------------------------------------------------------------------------
// Deinit media FTL session
// Engineer: Quyen Leba
//------------------------------------------------------------------------------
u8 media_ftldeinit()
{
    genfs_storage_sync();
    genfs_storage_deinit();
    media_info.current_sector = 0;
    return S_SUCCESS;
}

//------------------------------------------------------------------------------
// FTL read sectors
// Input:   u32 datalength
// Outputs: u8  *data
//          u32 *readlength
// Return:  u8  status
// Engineer: Quyen Leba
//------------------------------------------------------------------------------
u8 media_ftlread(u8 *data, u32 datalength, u32 *readlength)
{
    u32 sector_count;
    u8  status;

    sector_count = datalength / MEDIA_SECTOR_SIZE;
    status = genfs_readsectors(data, media_info.current_sector, sector_count);
    media_info.current_sector += sector_count;
    *readlength = sector_count * MEDIA_SECTOR_SIZE;

    return status;
}

//------------------------------------------------------------------------------
// FTL write sectors
// Inputs:  u8  *data
//          u32 datalength
// Return:  u8  status
// Engineer: Quyen Leba
//------------------------------------------------------------------------------
u8 media_ftlwrite_failsafe(u8 *data, u32 datalength)
{
    u32 sector_count;
    u8  status;

    sector_count = datalength / MEDIA_SECTOR_SIZE;
    status = genfs_writesectors_failsafe(data, media_info.current_sector, sector_count);
    media_info.current_sector += sector_count;

    return status;
}

//------------------------------------------------------------------------------
// FTL write sectors
// Inputs:  u8  *data
//          u32 datalength
// Return:  u8  status
// Engineer: Quyen Leba
//------------------------------------------------------------------------------
u8 media_ftlwrite(u8 *data, u32 datalength)
{
    u32 sector_count;
    u8  status;

    sector_count = datalength / MEDIA_SECTOR_SIZE;
    status = genfs_writesectors(data, media_info.current_sector, sector_count);
    media_info.current_sector += sector_count;

    return status;
}

//------------------------------------------------------------------------------
// Create filesystem journal in media
// Return:  u8  status
// Engineer: Quyen Leba
//------------------------------------------------------------------------------
u8 media_create_fs_journal()
{
    return genfs_create_journal();
}

//------------------------------------------------------------------------------
// Delete filesystem journal in media
// Return:  u8  status
// Engineer: Quyen Leba
//------------------------------------------------------------------------------
u8 media_delete_fs_journal()
{
    return genfs_delete_journal();
}
