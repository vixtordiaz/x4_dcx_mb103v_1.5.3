/**
  **************** (C) COPYRIGHT 2011 SCT Performance, LLC *********************
  * File Name          : delays.h
  * Device / System    : SCT products using the "CommonCode" platform
  * Author             : Quyen Leba
  *
  * Version            : 1 
  * Date               : 05/02/2011
  * Description        : 
  *                    : 
  *
  *
  * History            : 05/02/2011 M. Davis
  *                    :   Add SCT Copyright header,
  *
  ******************************************************************************
  */

#ifndef __DELAYS_H
#define __DELAYS_H

#include <device_config.h>
#include <arch/gentype.h>

#define DELAYS_COUNTER_10MS         TIME_LOOP_COUNT_10MS
#define DELAYS_COUNTER_30MS         TIME_LOOP_COUNT_30MS
#define DELAYS_COUNTER_100MS        TIME_LOOP_COUNT_100MS

void delays(u32 timeout, u8 prefix);
void delays_counter(u32 count);

#endif  //__DELAYS_H
