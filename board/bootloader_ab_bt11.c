/**
  **************** (C) COPYRIGHT 2011 SCT Performance, LLC *********************
  * File Name          : bootloader_ab_bt11.c
  * Device / System    : SCT products using the "CommonCode" platform
  * Author             : Quyen Leba
  *
  * Version            : 1 
  * Date               : 05/02/2011
  * Description        : 
  *                    : 
  *
  *
  * History            : 05/02/2011 M. Davis
  *                    :   Add SCT Copyright header,
  *
  ******************************************************************************
  */

//------------------------------------------------------------------------------
// Log error point: 0x70xx -> 0x73xx
//------------------------------------------------------------------------------

#include <string.h>
#include <ctype.h>
#include <device_config.h>
#include <common/statuscode.h>
#include <common/crc32.h>
#include <common/crypto_blowfish.h>
#include <common/tea.h>
#include <common/filecrypto.h>
#include <common/bootsettings.h>
#include <common/ymodem.h>
#include <board/indicator.h>
#include <common/housekeeping.h>
#include "commlink.h"
#include "bootloader_ab_bt11.h"

struct
{
    struct
    {
        u8  init        :1;
        u8  reserved    :7;
    }control;
    u32 operating_mode;
    u32 fwcrc32e;
}bootloader_ab_info = 
{
    .control.init = 0,
    .fwcrc32e = 0,
};

F_FILE* fptr;
u32 calccrc32e;

u8 __abupdater_init(FirmwareHeader *fwheader);
u8 __abupdater_do(u8 *data, u16 datalength);
u8 __abupdater_validate(u32 fwcrc32e);

//------------------------------------------------------------------------------
// Input:   FirmwareHeader fwheader (No Encryption)
// Return:  u8  status
// Engineer: Quyen Leba/Patrick Downs
//------------------------------------------------------------------------------
u8 bootloader_ab_init(FirmwareHeader fwheader)
{
    u8  status;
    
    status = __abupdater_init(&fwheader);
    
    crc32e_sw_reset();
    calccrc32e = 0xFFFFFFFF;
    
    return status;
}

//------------------------------------------------------------------------------
// Input:   FirmwareHeader fwheader (No Encryption)
// Return:  u8  status
// Engineer: Quyen Leba/Patrick Downs
//------------------------------------------------------------------------------
u8 bootloader_ab_init_file(FirmwareHeader fwheader)
{
    fptr = genfs_user_openfile(AB_FWUPDATE_TEMP,"wb");
    if(!fptr)
        return S_OPENFILE;
    
    bootloader_ab_info.control.init = 1;
    bootloader_ab_info.fwcrc32e = fwheader.firmware_crc32e;
    
    crc32e_sw_reset();
    calccrc32e = 0xFFFFFFFF;
    
    return S_SUCCESS;
}

//------------------------------------------------------------------------------
// Inputs:  u8  *data (encrypted with critcal key)
//          u32 datalength (multiple of 8 bytes)
// Return:  u8  status
// Engineer: Quyen Leba
//------------------------------------------------------------------------------
u8 bootloader_ab_do(u8 *data, u32 datalength)
{
    // Calculate CRC32 by software for validation
    calccrc32e = crc32e_sw_calculateblock(calccrc32e,(u32*)data,datalength/4);
    crypto_blowfish_decryptblock_critical(data,datalength);
    
    return __abupdater_do(data,datalength);
}

//------------------------------------------------------------------------------
// Inputs:  u8  *data (encrypted with critcal key)
//          u32 datalength (multiple of 8 bytes)
// Return:  u8  status
// Engineer: Quyen Leba/Patrick Downs
//------------------------------------------------------------------------------
u8 bootloader_ab_do_file(u8 *data, u32 datalength)
{
    u8 status;
    u32 bytecount;
    
    status = S_FAIL;
    
    // Calculate CRC32 by software for validation
    calccrc32e = crc32e_sw_calculateblock(calccrc32e,(u32*)data,datalength/4);
    
    bytecount = fwrite(data,1,datalength,fptr);
    if(bytecount == datalength)
        status = S_SUCCESS;
    
    return status;
}

//------------------------------------------------------------------------------
// Validate VehicleBoard firmware
// Return:  u8  status
// Engineer: Quyen Leba
//------------------------------------------------------------------------------
u8 bootloader_ab_validate()
{
    u8  status;

    status = __abupdater_validate(bootloader_ab_info.fwcrc32e);
    if (status == S_SUCCESS)
    {
        status = bluetooth_exit_bootloader();
    }
    return status;
}

//------------------------------------------------------------------------------
// Validate VehicleBoard firmware
// Return:  u8  status - S_LOADFILE should indicate to calling function that
//              bootloader_ab_updatebyfile() must be called next to auctually load
//              the firmware.
// Engineer: Quyen Leba/Patrick Downs
//------------------------------------------------------------------------------
u8 bootloader_ab_validate_file()
{
    genfs_closefile(fptr);

    if (bootloader_ab_info.fwcrc32e != calccrc32e)
    {
        return S_CRC32E;
    }
    
    return S_LOADFILE;
}

//------------------------------------------------------------------------------
// Program AppBoard firmware data from a file
// Inputs:  u8  *data
//          u16 datalength (multiple of 8 bytes)
// Return:  u8  status
// Engineer: Quyen Leba
//------------------------------------------------------------------------------
u8 bootloader_ab_updatebyfile()
{
#define FW_BUFFER_SIZE    2048
#define BLUETOOTH_SESSION_EXTI_LINE EXTI_Line9
#define BLUETOOTH_LINK_EXTI_LINE    EXTI_Line11
    u8  status;
    u8 *readbuffer;
    u32 filesize;
    u32 bytecount;
    u8 friendlynamebackup[32];
    u8 friendlystatus;
    
    // Wait for the session to be closed by the app
    do
    {
        // todo timeout;
    }
    while(commlink_info.link == CommLinkInfoLink_Opened);
    
    // Backup friendlyname
    memset(friendlynamebackup, 0, sizeof(friendlynamebackup));
    friendlystatus = bluetooth_getfriendlyname(friendlynamebackup);
    
    indicator_set(Indicator_TaskInProgress);
    indicator_set_normaloperation(FALSE); // Lock indicator for atleast 10 seconds
    
    status = bluetooth_enter_bootloader();
    if (status != S_SUCCESS)
    {
        return status;
    }
    
    fptr = genfs_user_openfile(AB_FWUPDATE_TEMP,"rb");
    if(!fptr)
        return S_OPENFILE;
    
    readbuffer = __malloc(FW_BUFFER_SIZE);
    if (!readbuffer)
        return S_MALLOC;
    
    fseek(fptr,0,SEEK_END);
    filesize = ftell(fptr);
    fseek(fptr,0,SEEK_SET);
    
    status = ymodem_init(bluetooth_uart_tx_byte,bluetooth_uart_rx_byte);
    if (status == S_SUCCESS)
        status = ymodem_set_fileinfo("abfw.bin",filesize);
    
    while(filesize && status == S_SUCCESS)
    {
        if(filesize >= FW_BUFFER_SIZE)
        {
            bytecount = fread(readbuffer,1,FW_BUFFER_SIZE,fptr);
        }
        else
        {
            bytecount = fread(readbuffer,1,filesize,fptr);
        }
        
        filesize -= bytecount;
        
        crypto_blowfish_decryptblock_critical(readbuffer,bytecount);
                
        status = __abupdater_do(readbuffer, bytecount);
        if (status != S_SUCCESS)
        {
            break;
        }
    }
    ymodem_deinit();
    if(status == S_SUCCESS)
    {
        status = bluetooth_exit_bootloader();
        if(status == S_SUCCESS)
        {
            indicator_set_normaloperation(TRUE); 
            indicator_set(Indicator_TaskSuccess);    // Success, go green, normalmode LED
            indicator_set_normaloperation(FALSE); 
        }
    }
    else
    {
        indicator_set_normaloperation(TRUE); 
        indicator_set(Indicator_TaskFail);           // Error, stay RED no normalmode LED
        indicator_set_normaloperation(FALSE); 
    }
    
    genfs_closefile(fptr);
    __free(readbuffer);
    
    // Restore the friendlyname
    if(friendlystatus == S_SUCCESS)
        bluetooth_setfriendlyname(friendlynamebackup);
    
    housekeeping_additem(HouseKeepingType_IndicatorTimeout, 3000);// Lock indicator for atleast 3 seconds
    
    return status;
}

//------------------------------------------------------------------------------
// Init AppBoard in bootloader
// Input:   FirmwareHeader *fwheader
// Return:  u8  status
// Engineer: Quyen Leba
//------------------------------------------------------------------------------
u8 __abupdater_init(FirmwareHeader *fwheader)
{
    u8  status;

    bootloader_ab_info.control.init = 0;
    status = bluetooth_enter_bootloader();
    if (status != S_SUCCESS)
    {
        log_push_error_point(0x7020);
        return status;
    }
    bootloader_ab_info.control.init = 1;
    bootloader_ab_info.fwcrc32e = fwheader->firmware_crc32e;

    status = ymodem_init(bluetooth_uart_tx_byte,bluetooth_uart_rx_byte);
    if (status == S_SUCCESS)
    {
        status = ymodem_set_fileinfo("abfw.bin",fwheader->firmware_size);
    }

    return status;
}

//------------------------------------------------------------------------------
// Program AppBoard firmware data
// Inputs:  u8  *data
//          u16 datalength (multiple of 8 bytes)
// Return:  u8  status
// Engineer: Quyen Leba
//------------------------------------------------------------------------------
u8 __abupdater_do(u8 *data, u16 datalength)
{
    u8  status;

    if (bootloader_ab_info.control.init == 0)
    {
        log_push_error_point(0x7030);
        status = S_ERROR;
        goto __abupdater_do_done;
    }
    
    status = ymodem_send_datablock(data,datalength);
    if (status == S_SUCCESS)
    {
        return S_SUCCESS;
    }

__abupdater_do_done:
    ymodem_deinit();
    return status;
}

//------------------------------------------------------------------------------
// Validate AppBoard firmware data
// Input:   u32 fwcrc32e
// Return:  u8  status
// Engineer: Quyen Leba
//------------------------------------------------------------------------------
u8 __abupdater_validate(u32 fwcrc32e)
{    
    ymodem_deinit();
    if (bootloader_ab_info.control.init == 0)
    {
        log_push_error_point(0x7050);
        return S_ERROR;
    }
    else if (bootloader_ab_info.fwcrc32e != calccrc32e)
    {
        log_push_error_point(0x7051);
        return S_CRC32E;
    }

    return S_SUCCESS;
}

//------------------------------------------------------------------------------
// Get AppBoard firmware version
// Output:  u32 *fwversion
// Return:  u8  status
// Engineer: Quyen Leba
//------------------------------------------------------------------------------
u8 bootloader_ab_getversion(u32 *fwversion)
{
    u8  buffer[32];
    u32 i;
    u32 version;
    u8  status;

    version = 0;
    *fwversion = 0;
    status = bluetooth_getversionstring(buffer);
    if (status == S_SUCCESS)
    {
        //expected version string: YYMMDDB (YYMMDD: date, B: build letter)
        if (strlen((char*)buffer) == 7)
        {
            for(i=0;i<6;i++)
            {
                if (isdigit(buffer[i]))
                {
                    version *= 10;
                    version += buffer[i] - '0';
                }
                else
                {
                    log_push_error_point(0x7060);
                    status = S_BADCONTENT;
                    break;
                }
            }
            version *= 1000;
            if (isalpha(buffer[i]))
            {
                version += buffer[i];
                *fwversion = version;
                status = S_SUCCESS;
            }
            else
            {
                log_push_error_point(0x7061);
                status = S_BADCONTENT;
            }
        }
        else
        {
            log_push_error_point(0x7062);
            status = S_BADCONTENT;
        }
    }
    return status;
}

//------------------------------------------------------------------------------
// set AppBoard operating mode
// Output:  u32 *mode
// Return:  u8  status
// Engineer: Quyen Leba
//------------------------------------------------------------------------------
u8 bootloader_ab_setoperatingmode(u32 mode)
{
    bootloader_ab_info.operating_mode = mode;
    return S_SUCCESS;
}

//------------------------------------------------------------------------------
// Get AppBoard operating mode
// Output:  u32 *mode
// Return:  u8  status
// Engineer: Quyen Leba
//------------------------------------------------------------------------------
u8 bootloader_ab_getoperatingmode(u32 *mode)
{
    *mode = bootloader_ab_info.operating_mode;
    return S_SUCCESS;
}

/**
 *  bootloader_ab_start_bootloader
 *  
 *  @brief Forces the App Board to reset and boot into main bootloader
 *  
 *  @retval 			returns a STATUS_CODE enum
 *  
 *  @details Set force App board to boot into main bootloader. This is useful for
 *             firmware/tune revision update to report progress to the GUI.
 *  
 *  @authors Patrick Downs
 *  
 */
u8 bootloader_ab_start_bootloader(BootloaderMode mode, bool backlighton)
{
    return S_NOTSUPPORT;
}

/**
 *  bootloader_ab_setbootmode
 *  
 *  @brief Set Appboard boot mode
 *  
 *  @param [in] newmode	    New boot mode a BootloaderMode enum
 *  @retval 				returns a STATUS_CODE enum
 *  
 *  @details Sets the boot mode for the appboard. This will save a new 
 *           bootmode into it's bootsettings and then reset itself.
 *  
 *  @authors Patrick Downs
 *  
 */
u8 bootloader_ab_setbootmode(BootloaderMode newmode)
{
    return S_NOTSUPPORT;
}

/**
 *  bootloader_ab_progressbar
 *  
 *  @brief Display progress bar and message to GUI
 *  
 *  @param [in] percentage Percentage completer 0-100
 *  @param [in] message     Short string to display with progress
 *  @retval 				returns a STATUS_CODE enum
 *  
 *  @details Displays the progress bar and string while in app board main
 *              bootloader.
 *  
 *  @authors Patrick Downs
 *  
 */
u8 bootloader_ab_progressbar(u8 percentage, u8 *message)
{
    return S_NOTSUPPORT;
}

/**
 *  bootloader_ab_resetandrun
 *  
 *  @brief      Reset and run the app board 
 *  
 *  @retval     none
 *  
 *  @details    Resets the app board and puts commlink back to Responder mode
 *              typically after an update to run App boot mode.
 *           
 *  @authors Patrick Downs
 *  
 */
void bootloader_ab_resetandrun()
{
    return;
}
