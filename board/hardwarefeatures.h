/**
  **************** (C) COPYRIGHT 2011 SCT Performance, LLC *********************
  * File Name          : hardwarefeatures.h
  * Device / System    : SCT products using the "CommonCode" platform
  * Author             : Quyen Leba
  *
  * Version            : 1 
  * Date               : 05/02/2011
  * Description        : 
  *                    : 
  *
  *
  * History            : 05/02/2011 M. Davis
  *                    :   Add SCT Copyright header,
  *
  ******************************************************************************
  */

#ifndef __HARDWAREFEATURES_H
#define __HARDWAREFEATURES_H

#include <arch/gentype.h>

bool hardwarefeatures_has_wifi();

#endif    //__HARDWAREFEATURES_H
