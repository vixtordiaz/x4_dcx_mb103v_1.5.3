/**
  **************** (C) COPYRIGHT 2013 SCT Performance, LLC *********************
  * File Name          : mblif_dma.c
  * Device / System    : SCT products using the "CommonCode" platform
  * Author             : Patrick Downs
  *
  * Version            : 1 
  * Date               : 01/09/2013
  * Description        : 
  *                    : 
  *
  *
  *
  ******************************************************************************
  */

#include <string.h>
#include <board/genplatform.h>
#include <board/delays.h>
#include <common/statuscode.h>
#include "mblif_dma.h"

#define MAX_READBUFFER_SIZE         4096
#define MBLIF_READ_WRITE_XFER_SIZE  1024
#define MBLIF_EXEC_XFER_SIZE        256
#define MBLIF_CMD_DMA_DATALENGTH    64

#define mblif_isbusy()              (gpio_read_dav_v_busy_pin() == GPIO_PIN_STATUS_HIGH)

MBLIF_MODE mblif_dma_mode;

//------------------------------------------------------------------------------
// Private prototypes
//------------------------------------------------------------------------------
u8 mblif_dma_get_status(u8 *status, u16 *statuslength);
u8 mblif_dma_rdid(u8 *id, u16 *idlength);
u8 mblif_dma_writebuffer(u8 *buffer, u16 bufferlength);
u8 mblif_dma_appendbuffer(u8 *buffer, u16 bufferlength);
u8 mblif_dma_readbuffer(u8 *buffer, u16 *bufferlength);
u8 mblif_dma_exec(u16 opcode, u8 *data, u8 datalength);
u8 mblif_dma_busy_pulse_wait();
u8 mblif_dma_check_header_response(u8* response, u32 length);

//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
u8 mblif_dma_init(MBLIF_MODE mode)
{
    // Change MBLIF to DMA mode
    switch(mode)
    {
    case MBLIF_MODE_DMA_WITH_CRC:
        mblif_dma_mode = mode;
        spi_master_dma_init(TRUE);
        break;
    case MBLIF_MODE_DMA_NO_CRC:
        mblif_dma_mode = mode;
        spi_master_dma_init(FALSE);
        break;
    default:
        return S_NOTSUPPORT;
    }
    
    return S_SUCCESS;
}

u8 mblif_dma_transfer(u8 *txdata, u8 *rxdata, u32 datalength)
{
    u8 status;
        
    gpio_dav_v_busy_strobed(); // clear
    
    spi_master_dma_transfer_data(txdata, rxdata, datalength, FALSE);
    
    board_timeout_set(5000);
    
    while(gpio_dav_v_busy_strobed() == FALSE)   // wait for VB strobe
    {
        if (board_timeout())
            return S_TIMEOUT;
    }
    
    if(mblif_dma_mode == MBLIF_MODE_DMA_WITH_CRC)
    {
        status = spi_master_dma_transfer_checkcrc();
        if(status == S_SUCCESS)
            status = spi_master_dma_transfer_waitforcomplete(1000);      
    }
    else
    {
        status = spi_master_dma_transfer_waitforcomplete(1000);        
    }
              
    return status;
}

//------------------------------------------------------------------------------
// Inputs:  u8  cmd
//          u16 opcode (only used in certain cmds)
//          u8  *data
//          u8  *datalength
// Return:  u8  status
// Engineer: Patrick Downs
//------------------------------------------------------------------------------
u8 mblif_dma_link(u8 cmd, u16 opcode, u8 *data, u16 *datalength)
{
    u8  status;

    board_timeout_set(500);
    while(mblif_isbusy())
    {
        if (board_timeout())
        {
            return S_TIMEOUT;
        }
    }

    spi_master_enable_cs0();

    switch(cmd)
    {
    case MBLIF_CMD_GET_STATUS:
        status = mblif_dma_get_status(data,datalength);
        break;
    case MBLIF_CMD_RDID:
        status = mblif_dma_rdid(data,datalength);
        break;
    case MBLIF_CMD_WRITE_BUFFER:
        status = mblif_dma_writebuffer(data,*datalength);
        break;
    case MBLIF_CMD_APPEND_BUFFER:
        status = mblif_dma_appendbuffer(data,*datalength);
        break;
    case MBLIF_CMD_READ_BUFFER:
        status = mblif_dma_readbuffer(data,datalength);
        break;
    case MBLIF_CMD_EXEC:
        status = mblif_dma_exec(opcode,data,(u8)*datalength);
        break;
    default:
        status = S_INPUT;
        break;
    }

    spi_master_disable_cs0();

    return status;
}

u8 mblif_dma_busy_pulse_wait()
{
    board_timeout_set(2000);
       
    while(mblif_isbusy())
    {
        if (board_timeout())
        {
            return S_TIMEOUT;
        }
    }
    return S_SUCCESS;    
}

u8 mblif_dma_check_header_response(u8* response, u32 length)
{
    if(response[0] != MBLIF_CMD_HDR_S1)
        return S_FAIL;

    if(response[1] != MBLIF_CMD_HDR_S2)
        return S_FAIL;
    
    if(response[2] != MBLIF_CMD_HDR_S3)    
        return S_FAIL;

    if(response[3] != MBLIF_CMD_HDR_S4)
        return S_FAIL;
    
    return S_SUCCESS;
}

//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
u8 mblif_dma_get_status(u8 *status, u16 *statuslength)
{
    u8 cmdbuffer[MBLIF_CMD_DMA_DATALENGTH];
    u8 rxbuffer[MBLIF_CMD_DMA_DATALENGTH];
    u8 transferstatus;
    
    memset(cmdbuffer, 0, sizeof(cmdbuffer));

    cmdbuffer[0] = MBLIF_CMD_GET_STATUS;
    cmdbuffer[1] = MBLIF_CMD_GET_STATUS_S1;
    cmdbuffer[2] = MBLIF_CMD_RESPONSE_S4;
    
    transferstatus = mblif_dma_transfer(cmdbuffer, rxbuffer, sizeof(cmdbuffer));
    if(transferstatus != S_SUCCESS)
        return transferstatus;
    
    transferstatus = mblif_dma_check_header_response(rxbuffer, 
                                                     MBLIF_CMD_DMA_DATALENGTH);
    if(transferstatus != S_SUCCESS)
        return transferstatus;
    
    memset(cmdbuffer, 0, sizeof(cmdbuffer));
    transferstatus = mblif_dma_transfer(cmdbuffer, rxbuffer, sizeof(cmdbuffer));
    if(transferstatus != S_SUCCESS)
        return transferstatus;
    
    // Validate respsonse
    if(rxbuffer[0] != MBLIF_CMD_RESPONSE_S1)
        return S_FAIL;
    if(rxbuffer[5] != MBLIF_CMD_RESPONSE_S4)
        return S_FAIL;
    
    memcpy(status, (u8*)&rxbuffer[1], 4);
    *statuslength = 4;
        
    return S_SUCCESS;
}

//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
u8 mblif_dma_rdid(u8 *id, u16 *idlength)
{
    u8 cmdbuffer[MBLIF_CMD_DMA_DATALENGTH];
    u8 rxbuffer[MBLIF_CMD_DMA_DATALENGTH];
    u8 transferstatus;
    u8  length;
   
    memset(cmdbuffer, 0, sizeof(cmdbuffer));

    cmdbuffer[0] = MBLIF_CMD_RDID;
    cmdbuffer[1] = MBLIF_CMD_RDID_S1;
    cmdbuffer[2] = MBLIF_CMD_RDID_S2;
    cmdbuffer[3] = MBLIF_CMD_RDID_S3;
    
    transferstatus = mblif_dma_transfer(cmdbuffer, rxbuffer, sizeof(cmdbuffer));
    if(transferstatus != S_SUCCESS)
        return transferstatus;
    
    transferstatus = mblif_dma_check_header_response(rxbuffer, 
                                                     MBLIF_CMD_DMA_DATALENGTH);
    if(transferstatus != S_SUCCESS)
        return transferstatus;
    
    memset(cmdbuffer, 0, sizeof(cmdbuffer));
    transferstatus = mblif_dma_transfer(cmdbuffer, rxbuffer, sizeof(cmdbuffer));
    if(transferstatus != S_SUCCESS)
        return transferstatus;
    
    // Validate respsonse
    if(rxbuffer[0] != MBLIF_CMD_RESPONSE_S1)
        return S_FAIL;
        
    length = rxbuffer[1];
    
    if(rxbuffer[2+length] != MBLIF_CMD_RESPONSE_S4)
        return S_FAIL;
    
    memcpy(id, (u8*)&rxbuffer[2], length);        
    
    *idlength = length;

    return S_SUCCESS;
}

//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
u8 mblif_dma_writebuffer(u8 *buffer, u16 bufferlength)
{
    u8 xferbuffer[MBLIF_READ_WRITE_XFER_SIZE];
    u32 xfersize;
    u8 transferstatus;

#if (2*MBLIF_CMD_DMA_DATALENGTH > MBLIF_READ_WRITE_XFER_SIZE)
#error mblif_dma.c: bad MBLIF_CMD_DMA_DATALENGTH
#endif

    memset(xferbuffer, 0, sizeof(xferbuffer));

    // Use Write Buffer for initial data transfer
    if(bufferlength > MBLIF_READ_WRITE_XFER_SIZE)
    {
        xfersize = MBLIF_READ_WRITE_XFER_SIZE;
    }
    else
    {
        xfersize = bufferlength;
    }

    xferbuffer[0] = MBLIF_CMD_WRITE_BUFFER;
    xferbuffer[1] = MBLIF_CMD_WRITE_BUFFER_S1;
    xferbuffer[2] = MBLIF_CMD_WRITE_BUFFER_S2;
    xferbuffer[3] = MBLIF_CMD_WRITE_BUFFER_S3;
    xferbuffer[4] = (u8)(xfersize >> 8);
    xferbuffer[5] = (u8)(xfersize);
    
    transferstatus = mblif_dma_transfer(&xferbuffer[0],
                                        &xferbuffer[MBLIF_CMD_DMA_DATALENGTH], MBLIF_CMD_DMA_DATALENGTH);
    if(transferstatus != S_SUCCESS)
        return transferstatus;
    
    transferstatus = mblif_dma_check_header_response(&xferbuffer[MBLIF_CMD_DMA_DATALENGTH], MBLIF_CMD_DMA_DATALENGTH);
    if(transferstatus != S_SUCCESS)
        return transferstatus;
    
    transferstatus = mblif_dma_transfer(buffer, xferbuffer, xfersize);
    if(transferstatus != S_SUCCESS)
        return transferstatus;
    
    // Validate respsonse
    if(xferbuffer[0] != MBLIF_CMD_RESPONSE_S1 && xfersize >= 1)
        return S_UNMATCH;
    
    if(xferbuffer[1] != MBLIF_CMD_RESPONSE_S2 && xfersize >= 2)
        return S_UNMATCH;
    
    if(xferbuffer[2] != MBLIF_CMD_RESPONSE_S3 && xfersize >= 3)
        return S_UNMATCH;
    
    if(xferbuffer[3] != MBLIF_CMD_RESPONSE_S4 && xfersize >= 4)
        return S_UNMATCH;
    
    // Use Append Buffer to send the rest of the data
    bufferlength -= xfersize;    
    if (bufferlength)
    {
        buffer += xfersize;
        transferstatus = mblif_dma_appendbuffer(buffer, bufferlength);
        if(transferstatus != S_SUCCESS)        
            return transferstatus;
    }
    
    return S_SUCCESS;
}

//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
u8 mblif_dma_appendbuffer(u8 *buffer, u16 bufferlength)
{
    u8 xferbuffer[MBLIF_READ_WRITE_XFER_SIZE];
    u16 xfersize;
    u8 transferstatus;    
    
    if (bufferlength > MAX_READBUFFER_SIZE)
        return S_INPUT;
    
    while (bufferlength)
    {        
        if(bufferlength > MBLIF_READ_WRITE_XFER_SIZE)
            xfersize = MBLIF_READ_WRITE_XFER_SIZE;
        else
            xfersize = bufferlength;
    
        memset(xferbuffer, 0, sizeof(xferbuffer));

        xferbuffer[0] = MBLIF_CMD_APPEND_BUFFER;
        xferbuffer[1] = MBLIF_CMD_APPEND_BUFFER_S1;
        xferbuffer[2] = MBLIF_CMD_APPEND_BUFFER_S2;
        xferbuffer[3] = MBLIF_CMD_APPEND_BUFFER_S3;
        xferbuffer[4] = (u8)(xfersize >> 8);
        xferbuffer[5] = (u8)(xfersize);
            
        transferstatus = mblif_dma_transfer(xferbuffer, xferbuffer, MBLIF_CMD_DMA_DATALENGTH);
        if(transferstatus != S_SUCCESS)
            return transferstatus;
        
        transferstatus = mblif_dma_check_header_response(xferbuffer, MBLIF_CMD_DMA_DATALENGTH);
        if(transferstatus != S_SUCCESS)
            return transferstatus;
        
        transferstatus = mblif_dma_transfer(buffer, xferbuffer, xfersize);
        if(transferstatus != S_SUCCESS)
            return transferstatus;

        // Validate response
        if(xferbuffer[0] != MBLIF_CMD_RESPONSE_S1 && xfersize >= 1)
            return S_UNMATCH;
        
        if(xferbuffer[1] != MBLIF_CMD_RESPONSE_S2 && xfersize >= 2)
            return S_UNMATCH;
        
        if(xferbuffer[2] != MBLIF_CMD_RESPONSE_S3 && xfersize >= 3)
            return S_UNMATCH;
        
        if(xferbuffer[3] != MBLIF_CMD_RESPONSE_S4 && xfersize >= 4)
            return S_UNMATCH;
        
        bufferlength -= xfersize;
        
        if (bufferlength)
            buffer += xfersize;        
    }
        
    return S_SUCCESS;
}


//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
/*
u8 mblif_dma_writebuffer(u8 *buffer, u16 bufferlength)
{
    u8 cmdbuffer[MBLIF_CMD_DMA_DATALENGTH];
    u8 rxbuffer[MAX_READBUFFER_SIZE];
    u8 transferstatus;
        
    memset(cmdbuffer, 0, sizeof(cmdbuffer));

    cmdbuffer[0] = MBLIF_CMD_WRITE_BUFFER;
    cmdbuffer[1] = MBLIF_CMD_WRITE_BUFFER_S1;
    cmdbuffer[2] = MBLIF_CMD_WRITE_BUFFER_S2;
    cmdbuffer[3] = MBLIF_CMD_WRITE_BUFFER_S3;
    cmdbuffer[4] = (u8)(bufferlength >> 8);
    cmdbuffer[5] = (u8)(bufferlength);
        
    transferstatus = mblif_dma_transfer(cmdbuffer, rxbuffer, sizeof(cmdbuffer));
    if(transferstatus != S_SUCCESS)
        return transferstatus;
    
    transferstatus = mblif_dma_check_header_response(rxbuffer, 
                                                     MBLIF_CMD_DMA_DATALENGTH);
    if(transferstatus != S_SUCCESS)
        return transferstatus;
    
    memset(cmdbuffer, 0, sizeof(cmdbuffer));
    transferstatus = mblif_dma_transfer(buffer, rxbuffer, bufferlength);
    if(transferstatus != S_SUCCESS)
        return transferstatus;

    // Validate respsonse
    if(rxbuffer[0] != MBLIF_CMD_RESPONSE_S1 && bufferlength >= 1)
        return S_UNMATCH;
    
    if(rxbuffer[1] != MBLIF_CMD_RESPONSE_S2 && bufferlength >= 2)
        return S_UNMATCH;
    
    if(rxbuffer[2] != MBLIF_CMD_RESPONSE_S3 && bufferlength >= 3)
        return S_UNMATCH;
    
    if(rxbuffer[3] != MBLIF_CMD_RESPONSE_S4 && bufferlength >= 4)
        return S_UNMATCH;
        
    return S_SUCCESS;
}
*/

//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
/*
u8 mblif_dma_appendbuffer(u8 *buffer, u16 bufferlength)
{
    u8 cmdbuffer[MBLIF_CMD_DMA_DATALENGTH];
    u8 rxbuffer[MAX_READBUFFER_SIZE];
    u8 transferstatus;
        
    memset(cmdbuffer, 0, sizeof(cmdbuffer));

    cmdbuffer[0] = MBLIF_CMD_APPEND_BUFFER;
    cmdbuffer[1] = MBLIF_CMD_APPEND_BUFFER_S1;
    cmdbuffer[2] = MBLIF_CMD_APPEND_BUFFER_S2;
    cmdbuffer[3] = MBLIF_CMD_APPEND_BUFFER_S3;
    cmdbuffer[4] = (u8)(bufferlength >> 8);
    cmdbuffer[5] = (u8)(bufferlength);
        
    transferstatus = mblif_dma_transfer(cmdbuffer, rxbuffer, sizeof(cmdbuffer));
    if(transferstatus != S_SUCCESS)
        return transferstatus;
    
    transferstatus = mblif_dma_check_header_response(rxbuffer, 
                                                     MBLIF_CMD_DMA_DATALENGTH);
    if(transferstatus != S_SUCCESS)
        return transferstatus;
    
    memset(cmdbuffer, 0, sizeof(cmdbuffer));
    transferstatus = mblif_dma_transfer(buffer, rxbuffer, bufferlength);
    if(transferstatus != S_SUCCESS)
        return transferstatus;

    // Validate respsonse
    if(rxbuffer[0] != MBLIF_CMD_RESPONSE_S1 && bufferlength >= 1)
        return S_UNMATCH;
    
    if(rxbuffer[1] != MBLIF_CMD_RESPONSE_S2 && bufferlength >= 2)
        return S_UNMATCH;
    
    if(rxbuffer[2] != MBLIF_CMD_RESPONSE_S3 && bufferlength >= 3)
        return S_UNMATCH;
    
    if(rxbuffer[3] != MBLIF_CMD_RESPONSE_S4 && bufferlength >= 4)
        return S_UNMATCH;
        
    return S_SUCCESS;
}
*/

//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
u8 mblif_dma_readbuffer(u8 *buffer, u16 *bufferlength)
{
    u8 cmdbuffer[MBLIF_CMD_DMA_DATALENGTH];
    u8 rxbuffer[MAX_READBUFFER_SIZE];
    u8 transferstatus;
    u16 length;
    
    memset(cmdbuffer, 0, sizeof(cmdbuffer));

    cmdbuffer[0] = MBLIF_CMD_READ_BUFFER;
    cmdbuffer[1] = MBLIF_CMD_READ_BUFFER_S1;
    cmdbuffer[2] = MBLIF_CMD_READ_BUFFER_S2;
    cmdbuffer[3] = MBLIF_CMD_READ_BUFFER_S3;
        
    transferstatus = mblif_dma_transfer(cmdbuffer, rxbuffer, sizeof(cmdbuffer));
    if(transferstatus != S_SUCCESS)
        return transferstatus;
    
    transferstatus = mblif_dma_check_header_response(rxbuffer, 
                                                     MBLIF_CMD_DMA_DATALENGTH);
    if(transferstatus != S_SUCCESS)
        return transferstatus;
    
    cmdbuffer[0] = MBLIF_CMD_READ_BUFFER;
    cmdbuffer[1] = MBLIF_CMD_READ_BUFFER_S3;
    cmdbuffer[2] = MBLIF_CMD_READ_BUFFER_S2;
    cmdbuffer[3] = MBLIF_CMD_READ_BUFFER_S1;
    transferstatus = mblif_dma_transfer(cmdbuffer, rxbuffer, sizeof(cmdbuffer));
    if(transferstatus != S_SUCCESS)
        return transferstatus;
    
    // Validate respsonse
    if(rxbuffer[0] != MBLIF_CMD_RESPONSE_S1)
    {
        return S_UNMATCH;
    }   
    if(rxbuffer[3] != MBLIF_CMD_RESPONSE_S4)
    {
        return S_UNMATCH;
    }
    
    length = (u16)(rxbuffer[1] << 8);
    length |= (u16)rxbuffer[2];
    
    if (length > MAX_READBUFFER_SIZE)
    {
        return S_BADCONTENT;
    }
    
    transferstatus = mblif_dma_transfer(rxbuffer, buffer, length);
    if(transferstatus != S_SUCCESS)
        return transferstatus;
    
    *bufferlength = length;
    
    return S_SUCCESS;
}

//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
u8 mblif_dma_exec(u16 opcode, u8 *data, u8 datalength)
{
    u8 cmdbuffer[MBLIF_EXEC_XFER_SIZE];
    u8 rxbuffer[MBLIF_EXEC_XFER_SIZE];
    u8 transferstatus;
    
    memset(cmdbuffer, 0, sizeof(cmdbuffer));

    cmdbuffer[0] = MBLIF_CMD_EXEC;
    cmdbuffer[1] = MBLIF_CMD_EXEC_S1;
    cmdbuffer[2] = MBLIF_CMD_EXEC_S2;
    cmdbuffer[3] = MBLIF_CMD_EXEC_S3;
    cmdbuffer[4] = datalength + 2;
    
    transferstatus = mblif_dma_transfer(cmdbuffer, rxbuffer, MBLIF_CMD_DMA_DATALENGTH);
    if(transferstatus != S_SUCCESS)
    {
        return transferstatus;
    }
    
    transferstatus = mblif_dma_check_header_response(rxbuffer, 
                                                     MBLIF_CMD_DMA_DATALENGTH);
    if(transferstatus != S_SUCCESS)
    {
        return transferstatus;
    }
    
    cmdbuffer[0] = (u8)opcode;
    cmdbuffer[1] = (u8)(opcode >> 8);
    memcpy((u8*)&cmdbuffer[2], data, datalength);
                
    transferstatus = mblif_dma_transfer(cmdbuffer, rxbuffer, datalength+2);
    if(transferstatus != S_SUCCESS)
    {
        return transferstatus; 
    }
    
    // Validate respsonse
    if(rxbuffer[0] != MBLIF_CMD_RESPONSE_S1)
        return S_UNMATCH;

    if(rxbuffer[1] != MBLIF_CMD_RESPONSE_S2)
        return S_UNMATCH;

    if((datalength+2)>2 && rxbuffer[2] != MBLIF_CMD_RESPONSE_S3)
        return S_UNMATCH;
    
    if((datalength+2)>3 && rxbuffer[3] != MBLIF_CMD_RESPONSE_S4)
        return S_UNMATCH;
    
    return S_SUCCESS;
}
