/**
  **************** (C) COPYRIGHT 2011 SCT Performance, LLC *********************
  * File Name          : updatefailsafe.h
  * Device / System    : SCT products using the "CommonCode" platform
  * Author             : Patrick Downs
  *
  * Version            : 1 
  * Date               : 08/31/2011
  * Description        : 
  *                    : 
  *
  *
  * History            : 08/31/2011 P. Downs
  *                    :   Created
  *
  ******************************************************************************
  */

#ifndef __UPDATEFAILSAFE_H
#define __UPDATEFAILSAFE_H

void updatefailsafe_init();
bool updatefailsafe_check(void);
int  updatefailsafe_update(void);
void updatefailsafe_done();
int updatefailsafe_writeprotect(void);
int updatefailsafe_nowriteprotect(void);

#endif	//__UPDATEFAILSAFE_H
