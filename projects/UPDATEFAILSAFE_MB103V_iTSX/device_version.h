/**
  **************** (C) COPYRIGHT 2011 SCT Performance, LLC *********************
  * File Name          : device_version.h
  * Device / System    : SCT products using the "CommonCode" platform
  * Author             : Quyen Leba
  *
  * Version            : 1 
  * Date               : 05/02/2011
  * Description        : 
  *                    : 
  *
  *
  * History            : 05/02/2011 M. Davis
  *                    :   Add SCT Copyright header,
  *
  ******************************************************************************
  */

#ifndef __DEVICE_VERSION_H
#define __DEVICE_VERSION_H

#include <common/version.h>

#define TUNE_REVISION_REQUIRED          //TODOQ:
//format: 1122233 (major 11, minor 222, build 33): current is 1.3.0
#define MIN_APP_VERSION_REQUIRED        100300
//format: 11222333 (major 11, minor 222, build 333): current is 1.14.10
#define MIN_VB_VERSION_REQUIRED         1014010

#define APPLICATION_VERSION_MAJOR       1
#define APPLICATION_VERSION_MINOR       2
#define APPLICATION_VERSION_BUILD       0       //max 255
#define APPLICATION_VERSION             (APPLICATION_VERSION_MAJOR*1000 + APPLICATION_VERSION_MINOR)

static const firmware_version fwversion = 
{
    .market     = 1,
    .hardware   = 1,
    .major      = APPLICATION_VERSION_MAJOR,
    .minor      = APPLICATION_VERSION_MINOR,
    .build      = APPLICATION_VERSION_BUILD,
};

#endif	//__DEVICE_VERSION_H
