/**
  **************** (C) COPYRIGHT 2011 SCT Performance, LLC *********************
  * File Name          : device_config.h
  * Device / System    : SCT products using the "CommonCode" platform
  * Author             : Quyen Leba
  *
  * Version            : 1 
  * Date               : 05/02/2011
  * Description        : 
  *                    : 
  *
  *
  * History            : 05/02/2011 M. Davis
  *                    :   Add SCT Copyright header,
  *
  ******************************************************************************
  */

#ifndef __DEVICE_CONFIG_H
#define __DEVICE_CONFIG_H

#include <common/devicedef.h>

#define DEFAULT_USB_PID         		0x4040
#define DEFAULT_USB_POWER               0x32        //100mA
#define SUPPORT_DEVICE_MARKET           0x1234      //for now as example
#define USB_HAS_VIRTUAL_COM_PORT        0

#define TEA_KEY_0                       0x0F38409F
#define TEA_KEY_1                       0x03C350A2
#define TEA_KEY_2                       0xD6E59954
#define TEA_KEY_3                       0x43542102

#define ENGINEERING_SIGNATURE           0x5F1C83A0

#define USE_SEGGER_EMFILE               1
#define USE_HCC_EMBEDDED_FS             0

#define USE_COMMLINK_BT11_BLUETOOTH     1
#define USE_COMMLINK_KEN257             0
#define USE_COMMLINK_X4                 0
//not used in normal operations
#define FORCE_COMMLINK_STATUS_AS_OPENED 0
#define SUPPORT_IGN_KEY_DETECTION       0
#define DEFAULT_CRITICALSETTINGS_FLAGS  (SettingsDeviceFlags_None)

#define BLUETOOTH_AUTORECONNECT_TIME    5

#define FS_MAX_SECTOR_SIZE              512

#define SUPPORT_OTF                     0
#define DATALOG_MAX_RECORD_FILE_SIZE    (1*1024*1024)

#define DEVICE_TYPE                     iTSX_FORD
// If DEVICE_TYPE is "BANKS" or "GMBANKS", define "BANKS_ENG" to enable BANKS menus
#if (DEVICE_TYPE == BANKS) || (DEVICE_TYPE == GMBANKS)
    #define BANKS_ENG                   1
#endif

#define MARKET_TYPE                     US_MARKET_TYPE
#define MARKET_TYPE_STRING              US_MARKET_TYPE_STRING

#define SUPPORT_UPLOAD_SPECIAL_STOCK    1
#define HARDWARE_LOOKUP_FILE            "??.txt"
#define OS_LOOKUP_FILE                  "??.txt"

#endif	//__DEVICE_CONFIG_H
