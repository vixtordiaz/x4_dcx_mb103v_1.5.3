/**
  **************** (C) COPYRIGHT 2011 SCT Performance, LLC *********************
  * File Name          : updatefailsafe.h
  * Device / System    : SCT products using the "CommonCode" platform
  * Author             : Patrick Downs
  *
  * Version            : 1 
  * Date               : 08/31/2011
  * Description        : 
  *                    : 
  *
  *
  * History            : 08/31/2011 P. Downs
  *                    :   Created
  *
  ******************************************************************************
  */

#include <stm32f10x_rcc.h>
#include <stm32f10x_gpio.h>
#include <board/power.h>
#include <board/timer.h>
#include <board/rtc.h>
#include <board/interrupt.h>
#include <board/peripherals.h>
#include <fs/genfs.h>
#include <common/obd2can.h>
#include <common/bootsettings.h>
#include <common/cmdif.h>
#include <common/statuscode.h>
#include <MB103V\button.h>
#include <MB103V\sdmmc.h>
#include <MB103V\i2c.h>
#include <MB103V\accelerometer.h>
#include <MB103V\led.h>
#include <board\indicator.h>
#include <MB103V\usartA.h>
#include <MB103V\bluetooth.h>
#include <MB103V\debugif.h>
#include <MB103V\flash.h>
#include <string.h>

extern void USB_Disconnect_Config(void);
extern void Set_USBClock(void);
extern void USB_Interrupts_Config(void);
extern void USB_Init(void);
extern void USB_Cable_Config (FunctionalState NewState);

//0x08056000
#define FLASH_NEWFAILSAFE_STARTING_SECTOR       172
#define FLASH_NEWFAILSAFE_ADDRESS               (FLASH_START_ADDRESS + FLASH_NEWFAILSAFE_STARTING_SECTOR*FLASH_SECTOR_SIZE)
#define FLASH_NEWFAILSAFE_SIZE_MAX              FLASH_FAILSAFE_BOOT_SIZE

//------------------------------------------------------------------------------
// Private prototypes
//------------------------------------------------------------------------------


//------------------------------------------------------------------------------
// Customized init for fail safe bootloader update
// Engineer: Patrick Downs
//------------------------------------------------------------------------------
void updatefailsafe_init()
{
    clock_init();
    interrupt_init(DISABLE);
    crypto_blowfish_init();
    //Enable GPIOA, GPIOB, GPIOC, GPIOD, GPIOE, GPIOF, GPIOG and AFIO clocks
    RCC_APB2PeriphClockCmd
        (RCC_APB2Periph_GPIOA | RCC_APB2Periph_GPIOB |RCC_APB2Periph_GPIOC |
         RCC_APB2Periph_GPIOD | RCC_APB2Periph_GPIOE | RCC_APB2Periph_GPIOF |
         RCC_APB2Periph_GPIOG | RCC_APB2Periph_AFIO,
         ENABLE);  

    RCC_APB2PeriphClockCmd(RCC_APB2Periph_AFIO, ENABLE);
    
    //Enable CRC clock
    RCC_AHBPeriphClockCmd(RCC_AHBPeriph_CRC, ENABLE);
    
    //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    // Configure a SysTick Base time to 1 us.
    //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    #define CTRL_TICKINT_Set      ((u32)0x00000002)
    //SysTick interrupt each 100 Hz with HCLK equal to 72MHz
    SysTick->LOAD = 72000;
    //Configure HCLK clock as SysTick clock source
    //Enable the SysTick Interrupt
    SysTick->CTRL |= (SysTick_CLKSource_HCLK | CTRL_TICKINT_Set);

    gpio_init();
    USB_Disconnect_Config();
    timeout_init();
    board_timeout_init();
    timer1_init();
    timer2_init();
    spi_master_init();
    i2c_init();
    rtc_init();
    sdmmc_init();
    button_init();

    interrupt_init(ENABLE);
    button_intr_init();

    gpio_reset_vehicle_board();

    accelerometer_init();
    adc_init(ADC_DATALOG);

    led_init(FALSE);
    led_setState(LED_WHITE);

    bootsettings_load();
    settings_load();
    
    bootloader_force_application_validation();

    Set_USBClock();
    USB_Interrupts_Config();
    
    power_init();
    debugif_init();
}

void updatefailsafe_done()
{
    u8 status; 
    properties_init();
    USB_Init();
    
    status = genfs_init();
    if (status != S_SUCCESS)
    {
        //TODOQ: save this failure as USB/Bluetooth cmds want to access fs
        indicator_set(Indicator_FilesystemError);
    }
    cmdif_init();
    bluetooth_init(BT_115200_BAUD);
    
    
    
}


//------------------------------------------------------------------------------
// Compares the new firmware binary CRC32 with data currently in flash. Returns
// if update is necessary.
//
// 
// Engineer: Patrick Downs
//------------------------------------------------------------------------------
bool updatefailsafe_check(void)
{
    int failsafecrc32_flash;
    int failsafecrc32_file;
    
    // Calculate CRC32 of Fail Safe Bootloader in flash
    crc32e_reset();
    failsafecrc32_flash = crc32e_calculateblock
        (0xFFFFFFFF,(u32*)FLASH_FAILSAFE_BOOT_ADDRESS,FLASH_FAILSAFE_BOOT_SIZE/4);
    
    // Calculate CRC32 of Fail Safe Bootloader in file
    crc32e_reset();
    failsafecrc32_file = crc32e_calculateblock
        (0xFFFFFFFF,(u32*)FLASH_NEWFAILSAFE_ADDRESS,FLASH_NEWFAILSAFE_SIZE_MAX/4);
    
    if(failsafecrc32_flash != failsafecrc32_file)
    {
        // Flash CRC32 does not match the file CRC32, need update
        return TRUE;
    }
    
    if(bootsettings.failsafeboot_crc32e != failsafecrc32_file)
    {
        // Save new failsafe CRC32 in bootsettings after flash update
        bootsettings.failsafeboot_crc32e = failsafecrc32_file;
        bootsettings_update();
    }
    
    return FALSE;
}

//------------------------------------------------------------------------------
// Updates the failsafe bootloader with new firmware binary.
// 
// Engineer: Patrick Downs
//------------------------------------------------------------------------------
int updatefailsafe_update(void)
{   
    u32 pagecount;
    u32 Address;
    u32 EraseCounter;
    u32* Data;    
    volatile FLASH_Status FLASHStatus = FLASH_COMPLETE;
    
    pagecount = (FLASH_FAILSAFE_BOOT_SIZE / FLASH_SECTOR_SIZE);
    Address = FLASH_FAILSAFE_BOOT_ADDRESS;
    Data = (u32*)FLASH_NEWFAILSAFE_ADDRESS;
    
    
    FLASH_UnlockBank1();
    
    FLASH_ClearFlag(FLASH_FLAG_EOP | FLASH_FLAG_PGERR | FLASH_FLAG_WRPRTERR);
    
    // Erase
    for(EraseCounter = 0; (EraseCounter < pagecount) && (FLASHStatus == FLASH_COMPLETE); EraseCounter++)
    {
        FLASHStatus = FLASH_ErasePage(FLASH_FAILSAFE_BOOT_ADDRESS + (FLASH_SECTOR_SIZE * EraseCounter));
    }
    if(FLASHStatus != FLASH_COMPLETE)
    {
        // Error
        return S_FAIL;
    }
    
    // Program    
    while((Address < (FLASH_FAILSAFE_BOOT_ADDRESS + FLASH_FAILSAFE_BOOT_SIZE)) && (FLASHStatus == FLASH_COMPLETE))
    {
        FLASHStatus = FLASH_ProgramWord(Address, *Data);
        Address += 4;
        Data++;
    }
    if(FLASHStatus != FLASH_COMPLETE)
    {
        // Error
        return S_FAIL;
    }
    FLASH_LockBank1();
    
    // Verify
    if(updatefailsafe_check())
    {
        // Error
        return S_FAIL;
    }
        
    return S_SUCCESS;
}

// Failsafe Bootloader (first 16 pages)
#define WP_PAGES 0x000000FF
int updatefailsafe_writeprotect(void)
{
    u32 WRPR_Value;
    volatile FLASH_Status FLASHStatus = FLASH_COMPLETE;
    
    WRPR_Value = FLASH_GetWriteProtectionOptionByte();  
    if(WRPR_Value != (~WP_PAGES))
    {
        FLASH_Unlock();
        FLASHStatus = FLASH_EraseOptionBytes();
        
        FLASHStatus = FLASH_EnableWriteProtection(WP_PAGES);
        
        NVIC_SystemReset();
    }
    
    return S_SUCCESS;
}

int updatefailsafe_nowriteprotect(void)
{
    u32 WRPR_Value;
    volatile FLASH_Status FLASHStatus = FLASH_COMPLETE;
    
    WRPR_Value = FLASH_GetWriteProtectionOptionByte();
    if(WRPR_Value != 0xFFFFFFFF)
    {
        FLASH_Unlock();
        
        FLASHStatus = FLASH_EraseOptionBytes();
        
        NVIC_SystemReset();
    }  
    
    return S_SUCCESS;
}

//------------------------------------------------------------------------------
// Validate application stored in FLASH
// Return:  u8  status (S_SUCCESS: good)
// Engineer: Quyen Leba
//------------------------------------------------------------------------------
#if 0
u8 bootloader_validate_application()
{
    u32 calc_crc32e;

    if (BOOTSETTINGS_GetAppLength() > BOOTLOADER_APPLICATION_SIZE_MAX)
    {
        return S_BADCONTENT;
    }
    crc32e_reset();
    calc_crc32e = crc32e_calculateblock
        (0xFFFFFFFF,(u32*)BOOTLOADER_APPLICATION_ADDRESS,
         BOOTSETTINGS_GetAppLength()/4);
    if (calc_crc32e != BOOTSETTINGS_GetAppCRC32E())
    {
        return S_FAIL;
    }
    return S_SUCCESS;
}
#endif

//------------------------------------------------------------------------------
// Set boot mode in bootsettings
// Input:   u16 bootmode
// Return:  u8  status
// Engineer: Quyen Leba
//------------------------------------------------------------------------------
#if 0
u8 bootloader_setboot(BootloaderMode bootmode)
{
    u8  status;

    status = S_SUCCESS;
    if (bootmode != BOOTSETTINGS_GetBootMode())
    {
        switch(bootmode)
        {
        case BootloaderMode_MB_FailSafe:
        case BootloaderMode_MB_Main:
        case BootloaderMode_MB_Application:
            BOOTSETTINGS_SetBootMode((u16)bootmode);
            status = bootsettings_update();
            break;
        default:
            log_push_error_point(0xBBA0);
            return S_INPUT;
        }
    }
    return status;
}
#endif

//------------------------------------------------------------------------------
// Write bootloader data to flash
// Inputs:  u8  *data (encrypted)
//          u32 datalength (must be multiple of 8)
// Return:  u8  status
// Engineer: Quyen Leba
//------------------------------------------------------------------------------
#if 0
u8 bootloader_write_flash(u8 *data, u32 datalength)
{
    u32 datacount;
    u8  status;

    if (bootloader_info.status.init == 0)
    {
        log_push_error_point(0xBBC0);
        return S_ERROR;
    }

    if ((bootloader_info.bytecheck > FLASH_SECTOR_SIZE) ||
        (bootloader_info.bytecount + datalength) > bootloader_info.firmware_size)
    {
        log_push_error_point(0xBBC1);
        return S_ERROR;
    }

    datacount = 0;
    while(datacount < datalength)
    {
        u32 byteavailable_currentsector;
        u32 byteremain_toprocess;
        u16 bytetoprogram;

        byteavailable_currentsector = FLASH_SECTOR_SIZE - bootloader_info.bytecheck;
        byteremain_toprocess = datalength - datacount;
        if (byteavailable_currentsector == 0)
        {
            bootloader_info.current_sector++;
            if (flash_erase_sector(bootloader_info.current_sector) != S_SUCCESS)
            {
                log_push_error_point(0xBBC2);
                goto bootloader_write_flash_failed;
            }
            bootloader_info.bytecheck = 0;
            byteavailable_currentsector = FLASH_SECTOR_SIZE;
        }
        else if (byteavailable_currentsector >= 2048)
        {
            if (flash_erase_sector(bootloader_info.current_sector) != S_SUCCESS)
            {
                log_push_error_point(0xBBC3);
                goto bootloader_write_flash_failed;
            }
            bootloader_info.bytecheck = 0;
            byteavailable_currentsector = FLASH_SECTOR_SIZE;
        }

        if (byteremain_toprocess >= byteavailable_currentsector)
        {
            bytetoprogram = byteavailable_currentsector;
        }
        else
        {
            bytetoprogram = byteremain_toprocess;
        }

        status = flash_write_sector_at_offset(bootloader_info.current_sector,
                                              bootloader_info.bytecheck,
                                              &data[datacount],bytetoprogram,TRUE);
        if (status != S_SUCCESS)
        {
            log_push_error_point(0xBBC4);
            goto bootloader_write_flash_failed;
        }

        datacount += bytetoprogram;
        bootloader_info.bytecount += bytetoprogram;
        bootloader_info.bytecheck += bytetoprogram;
    }
    
    return S_SUCCESS;

bootloader_write_flash_failed:
    bootloader_info.status.init = 0;
    return S_FAIL;
}
#endif

//------------------------------------------------------------------------------
// Validate bootloader
// Inputs:  u32 crc32e
// Return:  u8  status
// Engineer: Quyen Leba
//------------------------------------------------------------------------------
#if 0 
u8 bootloader_validate()
{
    u32 dummy_u32;
    u32 calc_crc32e;
    u32 calc_crc32e_decrypted;
    u32 starting_address;
    u32 *target_crc32e;
    u32 *target_length;
    u32 *target_fwversion;
    u32 old_crc32e;
    u32 old_length;
    u32 old_fwversion;
    u8  *flash_bptr;
    BootloaderMode new_bootmode;
    u32 i;

    if (bootloader_info.status.init == 0)
    {
        log_push_error_point(0xBBD0);
        return S_ERROR;
    }
    bootloader_info.status.init = 0;

    switch(bootloader_info.firmware_flags & 0x07)
    {
    case FIRMWARE_FLAGS_APPLICATION:
        old_crc32e = BOOTSETTINGS_GetAppCRC32E();
        old_length = BOOTSETTINGS_GetAppLength();
        old_fwversion = BOOTSETTINGS_GetAppVersion();

        target_crc32e = &BOOTSETTINGS_GetAppCRC32E();
        target_length = &BOOTSETTINGS_GetAppLength();
        target_fwversion = &BOOTSETTINGS_GetAppVersion();
        starting_address = BOOTLOADER_APPLICATION_ADDRESS;
        new_bootmode = BootloaderMode_MB_Application;
        if (bootloader_info.firmware_size <= BOOTLOADER_APPLICATION_SIZE_MAX)
        {
            *target_length = bootloader_info.firmware_size;
        }
        else
        {
            log_push_error_point(0xBBD1);
            return S_BADCONTENT;
        }
        break;
    case FIRMWARE_FLAGS_MAIN_BOOTLOADER:
        old_crc32e = BOOTSETTINGS_GetMainBootCRC32E();
        old_length = BOOTLOADER_MAIN_SIZE;
        old_fwversion = BOOTSETTINGS_GetMainBootVersion();

        target_crc32e = &BOOTSETTINGS_GetMainBootCRC32E();
        target_length = &dummy_u32;
        target_fwversion = &BOOTSETTINGS_GetMainBootVersion();
        starting_address = BOOTLOADER_MAIN_FLASH_ADDRESS;
        new_bootmode = BootloaderMode_MB_Main;
        *target_length = BOOTLOADER_MAIN_SIZE;
        break;
    default:
        log_push_error_point(0xBBD2);
        return S_BADCONTENT;
    }

    flash_bptr = (u8*)starting_address;
    //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    //calculate crc32e on encrypted data
    //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    crc32e_reset();
    calc_crc32e = 0xFFFFFFFF;
    i = 0;
    while(i < *target_length)
    {
        u8  tmpbuffer[FLASHCHECK_BLOCKSIZE];
        u32 length;

        length = *target_length - i;
        if (length > FLASHCHECK_BLOCKSIZE)
        {
            length = FLASHCHECK_BLOCKSIZE;
        }
        memcpy((char*)tmpbuffer,(char*)&flash_bptr[i],length);
        crypto_blowfish_encryptblock_critical(tmpbuffer,length);
        calc_crc32e = crc32e_calculateblock(calc_crc32e,
                                            (u32*)tmpbuffer,length/4);
        i += length;
    }
    //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    //calculate crc32e on decrypted data
    //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    crc32e_reset();
    calc_crc32e_decrypted = 0xFFFFFFFF;
    i = 0;
    while(i < *target_length)
    {
        u8  tmpbuffer[FLASHCHECK_BLOCKSIZE];
        u32 length;

        length = *target_length - i;
        if (length > FLASHCHECK_BLOCKSIZE)
        {
            length = FLASHCHECK_BLOCKSIZE;
        }
        memcpy((char*)tmpbuffer,(char*)&flash_bptr[i],length);
        calc_crc32e_decrypted = crc32e_calculateblock(calc_crc32e_decrypted,
                                                      (u32*)tmpbuffer,length/4);
        i += length;
    }
    
    if (calc_crc32e == bootloader_info.firmware_crc32e)
    {
        u8  status;

        *target_crc32e = calc_crc32e_decrypted;
        *target_fwversion = bootloader_info.firmware_version;
        if (old_crc32e != calc_crc32e_decrypted || old_length != *target_length ||
            old_fwversion != bootloader_info.firmware_version ||
            new_bootmode != BOOTSETTINGS_GetBootMode())
        {
            BOOTSETTINGS_SetBootMode(new_bootmode);
            status = bootsettings_update();
        }
        else
        {
            status = S_SUCCESS;
        }
        
        if (status == S_SUCCESS)
        {
            watchdog_set();
        }
        return status;
    }
    else
    {
        *target_crc32e = old_crc32e;
        *target_length = old_length;
        *target_fwversion = old_fwversion;
        log_push_error_point(0xBBD3);
        return S_CRC32E;
    }
}
#endif