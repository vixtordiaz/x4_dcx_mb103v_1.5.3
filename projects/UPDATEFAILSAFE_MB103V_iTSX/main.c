/**
  **************** (C) COPYRIGHT 2011 SCT Performance, LLC *********************
  * File Name          : main.c
  * Device / System    : SCT products using the "CommonCode" platform
  * Author             : Quyen Leba
  *
  * Version            : 1 
  * Date               : 05/02/2011
  * Description        : 
  *                    : 
  *
  *
  * History            : 05/02/2011 M. Davis
  *                    :   Add SCT Copyright header,
  *
  ******************************************************************************
  */

#include <common/crypto_blowfish.h>
#include <MB103V/init.h>
#include <common/cmdif.h>
#include "stm32f10x_gpio.h"
#include <MB103V/button.h>
#include <common/obd2vpw.h>
#include <common/obd2can.h>
#include <common/settings.h>
#include <common/statuscode.h>
#include <fs/genfs.h>
#include <board/MB103V/disk.h>
#include <board/MB103V/mblif.h>
#include <board/MB103V/mblexec.h>
#include <board/MB103V/led.h>
#include <board/timer.h>

#include <board/power.h>
#include <board/bootloader.h>
#include <board/MB103V/usart.h>
#include <common/vbupdater.h>

#include "stm32f10x_rcc.h"
#include "stm32f10x_i2c.h"
#include "stm32f10x_spi.h"

#include "updatefailsafe.h"
#include <common\debug\debug_output.h>
#include <common/usbcmdhandler.h>

u8  button_pressed = FALSE;
extern BluetoothInfo bluetooth_info;

int main()
{
    int status;
    u8 retry = 2;
    char statusstring[10];

    uch_init();
    updatefailsafe_init();
    debug_init();
    
    if(updatefailsafe_check())
    {
        // Run Update
        if(updatefailsafe_nowriteprotect() != S_SUCCESS)
        {
            // TODO
        }
RETRY_UPDATE:
        status = updatefailsafe_update();
        if(status != S_SUCCESS)
        {
            // Retry?
            if(retry--)
            {
                goto RETRY_UPDATE;
            }
            // Bad news...
            led_setState(LED_RED);
            debug_out("Update Error Status: ");
            itoa(status, statusstring, 10);
            debug_out(statusstring);
            while(1)
            {
                debug_console();
            }
        }
    }
    
    // Done
    updatefailsafe_writeprotect();
    led_setState(LED_GREEN);
    updatefailsafe_done();

    debug_init();
    while(1)
    {
        debug_console();               
        cmdif_handler();
    }//while(1)...
}
