/**
  **************** (C) COPYRIGHT 2011 SCT Performance, LLC *********************
  * File Name          : device_config.h
  * Device / System    : SCT products using the "CommonCode" platform
  * Author             : Quyen Leba
  *
  * Version            : 1 
  * Date               : 05/02/2011
  * Description        : 
  *                    : 
  *
  *
  * History            : 05/02/2011 M. Davis
  *                    :   Add SCT Copyright header,
  *
  ******************************************************************************
  */

#ifndef __DEVICE_CONFIG_H
#define __DEVICE_CONFIG_H

#include <common/devicedef.h>
#include <common/obd2def.h>

#define DEFAULT_USB_PID                 0x4040
#define DEFAULT_USB_POWER               0xFA        //500mA
#define SUPPORT_DEVICE_MARKET           0x1234      //for now as example
#define USB_HAS_VIRTUAL_COM_PORT        0

#define ENGINEERING_SIGNATURE           0x5F1C83A0

#define USE_SEGGER_EMFILE               1
#define USE_HCC_EMBEDDED_FS             0

#define USE_WIFI                        1
#define USE_CC3000_WIFI                 1

#define USE_COMMLINK_BT11_BLUETOOTH     0
#define USE_COMMLINK_KEN257             0
#define USE_COMMLINK_X4                 1
#define USE_COMMLINK_BAUD               COMMLINK_115200_BAUD
//not used in normal operations
#define FORCE_COMMLINK_STATUS_AS_OPENED 0
#define SUPPORT_IGN_KEY_DETECTION       1
#define DEFAULT_CRITICALSETTINGS_FLAGS  (SettingsDeviceFlags_None)

#define FS_MAX_SECTOR_SIZE              512

#define SUPPORT_OTF                     0
#define DATALOG_MAX_RECORD_FILE_SIZE    (10*1024*1024)

#define DATALOG_FEATURES_FLAGS          DATALOG_FEATURES_DTC |              \
                                        DATALOG_FEATURES_OSC |              \
                                        DATALOG_FEATURES_PACKET_PID

#define TIME_LOOP_COUNT_10MS            71000
#define TIME_LOOP_COUNT_30MS            216000
#define TIME_LOOP_COUNT_100MS           724000

// MBLIF_MODE_TYPE: X4 uses NO SPI CRC, CC3000 cannot use SPI CRC and STM32 SPI
// hardware requires constant resync of CRC on both sides when used in mutislave 
// setup, it's not worth the trouble to run SPI CRC over DMA.
#define MBLIF_MODE_TYPE                 MBLIF_MODE_DMA_NO_CRC 

#define OEM_TYPE                        OemType_FORD
#define DEVICE_TYPE                     DeviceType_X4_FORD
// If DEVICE_TYPE is "BANKS" or "GMBANKS", define "BANKS_ENG" to enable BANKS menus
#if (DEVICE_TYPE == BANKS) || (DEVICE_TYPE == GMBANKS)
    #define BANKS_ENG                   1
#endif

#define MARKET_TYPE                     MarketType_Any
#define MARKET_TYPE_STRING              UNKNOWN_MARKET_TYPE_STRING

#define SUPPORT_UPLOAD_SPECIAL_STOCK    1
#define HARDWARE_LOOKUP_FILE            "??.txt"
#define OS_LOOKUP_FILE                  "??.txt"

// ---------------------------------------------------------------------------
// DTC File info
// This is used by the Read DTC function to read DTC descriptions
// Lookup table can be updated as necessary.
// ---------------------------------------------------------------------------
#define DTC_FILE_COUNT                  3
#define DTC_FILENAME_ISO                "DTCDB_ISO14229.txt" 
#define DTC_FILENAME_KWP                "DTCDB_KWP2000.txt"
#define DTC_FILENAME_OBD2               "DTCDB_OBD2.txt"

// ---------------------------------------------------------------------------
// DTC File info
// This is used by the Read DTC function to read DTC descriptions
// Lookup table can be updated as necessary.
// ---------------------------------------------------------------------------
typedef struct
{
    VehicleCommLevel vehiclecommlevel;
    char filename[20];
}DTC_FILE;

const static DTC_FILE dtc_file_lookup[DTC_FILE_COUNT] = 
{
    [0] = 
    {
    .vehiclecommlevel = CommLevel_KWP2000,
    .filename = DTC_FILENAME_KWP,
    },
    [1] = 
    {
    .vehiclecommlevel = CommLevel_ISO14229,
    .filename = DTC_FILENAME_ISO,
    },
    [2] = 
    {
    .vehiclecommlevel = CommLevel_Unknown,
    .filename = DTC_FILENAME_OBD2,
    },
};

// ---------------------------------------------------------------------------
// Production Test Definitions
// ---------------------------------------------------------------------------
#define MB_REV      3
#define VB_REV      3
#define AB_REV      2

#endif    //__DEVICE_CONFIG_H
