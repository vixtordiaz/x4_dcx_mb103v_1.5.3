/**
  ******************************************************************************
  * @file    ADC/ADC3_DMA/stm32f2xx_it.c 
  * @author  MCD Application Team
  * @version V1.0.0
  * @date    18-April-2011
  * @brief   Main Interrupt Service Routines.
  *          This file provides template for all exceptions handler and 
  *          peripherals interrupt service routine.
  ******************************************************************************
  * @attention
  *
  * THE PRESENT FIRMWARE WHICH IS FOR GUIDANCE ONLY AIMS AT PROVIDING CUSTOMERS
  * WITH CODING INFORMATION REGARDING THEIR PRODUCTS IN ORDER FOR THEM TO SAVE
  * TIME. AS A RESULT, STMICROELECTRONICS SHALL NOT BE HELD LIABLE FOR ANY
  * DIRECT, INDIRECT OR CONSEQUENTIAL DAMAGES WITH RESPECT TO ANY CLAIMS ARISING
  * FROM THE CONTENT OF SUCH FIRMWARE AND/OR THE USE MADE BY CUSTOMERS OF THE
  * CODING INFORMATION CONTAINED HEREIN IN CONNECTION WITH THEIR PRODUCTS.
  *
  * <h2><center>&copy; COPYRIGHT 2011 STMicroelectronics</center></h2>
  ******************************************************************************
  */ 

/* Includes ------------------------------------------------------------------*/
#include "stm32f2xx_it.h"
#include "usb_core.h"
#include "usbd_core.h"
#include <board/genplatform.h>

/* Private typedef -----------------------------------------------------------*/
/* Private define ------------------------------------------------------------*/
/* Private macro -------------------------------------------------------------*/
/* Private variables ---------------------------------------------------------*/
extern USB_OTG_CORE_HANDLE  USB_OTG_dev;
/* Private function prototypes -----------------------------------------------*/
extern void USB_1ms_Periodic_Task(void);

extern void led_emergency_indicator_system_error();
extern void bluetooth_uart_rx_irq_handler();
extern void bluetooth_session_intr_handler();
extern void bluetooth_link_intr_handler();
extern void debugif_uart_rx_irq_handler();
extern void I2C1_ErIrqHandler(void);
extern void I2C1_EvIrqHandler(void);
extern void button_intr_handler();
extern void timer3_intr_handler();
extern void timer5_intr_handler();
extern void timer7_intr_handler();
extern void commlink_reset_command_processor();
extern void interrupt_sw_trigger_handler_0();
extern void interrupt_sw_trigger_handler_1();
extern void interrupt_sw_trigger_handler_2();
extern void gpio_read_dav_v_busy_intr();

extern uint32_t USBD_OTG_ISR_Handler (USB_OTG_CORE_HANDLE *pdev);

#ifdef USB_OTG_HS_DEDICATED_EP1_ENABLED 
extern uint32_t USBD_OTG_EP1IN_ISR_Handler (USB_OTG_CORE_HANDLE *pdev);
extern uint32_t USBD_OTG_EP1OUT_ISR_Handler (USB_OTG_CORE_HANDLE *pdev);
#endif

/* Private functions ---------------------------------------------------------*/

/******************************************************************************/
/*            Cortex-M3 Processor Exceptions Handlers                         */
/******************************************************************************/

/**
  * @brief   This function handles NMI exception.
  * @param  None
  * @retval None
  */
void NMI_Handler(void)
{
}

/**
  * @brief  This function handles Hard Fault exception.
  * @param  None
  * @retval None
  */
void HardFault_Handler(void)
{
  /* Go to infinite loop when Hard Fault exception occurs */
  while (1)
  {
      led_emergency_indicator_system_error();
  }
}

/**
  * @brief  This function handles Memory Manage exception.
  * @param  None
  * @retval None
  */
void MemManage_Handler(void)
{
  /* Go to infinite loop when Memory Manage exception occurs */
  while (1)
  {
  }
}

/**
  * @brief  This function handles Bus Fault exception.
  * @param  None
  * @retval None
  */
void BusFault_Handler(void)
{
  /* Go to infinite loop when Bus Fault exception occurs */
  while (1)
  {
  }
}

/**
  * @brief  This function handles Usage Fault exception.
  * @param  None
  * @retval None
  */
void UsageFault_Handler(void)
{
  /* Go to infinite loop when Usage Fault exception occurs */
  while (1)
  {
  }
}

/**
  * @brief  This function handles SVCall exception.
  * @param  None
  * @retval None
  */
void SVC_Handler(void)
{
}

/**
  * @brief  This function handles Debug Monitor exception.
  * @param  None
  * @retval None
  */
void DebugMon_Handler(void)
{
}

/**
  * @brief  This function handles PendSVC exception.
  * @param  None
  * @retval None
  */
void PendSV_Handler(void)
{
}

/**
  * @brief  This function handles SysTick Handler.
  * @param  None
  * @retval None
  */
void SysTick_Handler(void)
{  
}

/**
* @brief  This function handles USART1 interrupt request.
* @param  None
* @retval None
*/
void USART1_IRQHandler(void)
{
    bluetooth_uart_rx_irq_handler();
}

/**
* @brief  This function handles USART3 interrupt request.
* @param  None
* @retval None
*/
void USART3_IRQHandler(void)
{
    debugif_uart_rx_irq_handler();
}

/**
* @brief  This function handles WWDG interrupt request.
* @param  None
* @retval None
*/
void WWDG_IRQHandler(void)
{
    WWDG_SetCounter(0x7F);
    WWDG_ClearFlag();
}

/**
  * @brief  This function handles TIM1 Handler.
  * @param  None
  * @retval None
  */
void TIM1_UP_TIM10_IRQHandler(void)
{
    if ((TIM1->SR & TIM_IT_Update) != (u16)RESET &&
        (TIM1->DIER & TIM_IT_Update) != (u16)RESET)
    {
        timer1_msec_counter_update();
        USB_1ms_Periodic_Task();
        TIM1->SR = (u16)~TIM_IT_Update;
    }
}

/**
  * @brief  This function handles TIM2 Handler.
  * @param  None
  * @retval None
  */
void TIM2_IRQHandler(void)
{
    if ((TIM2->SR & TIM_IT_Update) != (u16)RESET &&
        (TIM2->DIER & TIM_IT_Update) != (u16)RESET)
    {
        commlink_reset_command_processor();
        TIM2->SR = (u16)~TIM_IT_Update;
    }
}

/**
  * @brief  This function handles TIM3 Handler.
  * @param  None
  * @retval None
  */
void TIM3_IRQHandler(void)
{
    if (TIM_GetITStatus(TIM3, TIM_IT_Update) != RESET)
    {
        timer3_intr_handler();
        TIM_ClearITPendingBit(TIM3, TIM_IT_Update);
    }
}

/**
  * @brief  This function handles TIM4 Handler.
  * @param  None
  * @retval None
  */
void TIM4_IRQHandler(void)
{
    if ((TIM4->SR & TIM_IT_Update) != (u16)RESET &&
        (TIM4->DIER & TIM_IT_Update) != (u16)RESET)
    {
        //!?! timer4_intr_handler();
        TIM4->SR = (u16)~TIM_IT_Update;
    }
}

/**
  * @brief  This function handles TIM5 Handler.
  * @param  None
  * @retval None
  */
void TIM5_IRQHandler(void)
{
    if (TIM_GetITStatus(TIM5, TIM_IT_Update) != RESET)
    {
        timer5_intr_handler();
        TIM_ClearITPendingBit(TIM5, TIM_IT_Update);
    }
}

/**
  * @brief  This function handles TIM7 Handler.
  * @param  None
  * @retval None
  */
void TIM7_IRQHandler(void)
{
    if (TIM_GetITStatus(TIM7, TIM_IT_Update) != RESET)
    {
        timer7_intr_handler();
        TIM_ClearITPendingBit(TIM7, TIM_IT_Update);
    }
}

/**
  * @brief  This function handles I2C1_EV Handler.
  * @param  None
  * @retval None
  */
void I2C1_EV_IRQHandler(void)
{
    I2C1_EvIrqHandler();
}

/**
  * @brief  This function handles I2C1_ER Handler.
  * @param  None
  * @retval None
  */
void I2C1_ER_IRQHandler(void)
{
    I2C1_ErIrqHandler();
}

/**
  * @brief  This function handles EXTI0 Handler.
  * @param  None
  * @retval None
  */
void EXTI0_IRQHandler(void)
{
    interrupt_sw_trigger_handler_0();
}

/**
  * @brief  This function handles EXTI1 Handler.
  * @param  None
  * @retval None
  */
void EXTI1_IRQHandler(void)
{
    interrupt_sw_trigger_handler_1();
}

/**
  * @brief  This function handles EXTI2 Handler.
  * @param  None
  * @retval None
  */
void EXTI2_IRQHandler(void)
{
    interrupt_sw_trigger_handler_2();
}

/**
  * @brief  This function handles EXTI3 Handler.
  * @param  None
  * @retval None
  */
void EXTI3_IRQHandler(void)
{
    int x =0;
    x++;
}

/**
  * @brief  This function handles EXTI9_5 Handler.
  * @param  None
  * @retval None
  */
void EXTI9_5_IRQHandler(void)
{
    bluetooth_session_intr_handler();
}

/**
  * @brief  This function handles EXTI15_10 Handler.
  * @param  None
  * @retval None
  */
void EXTI15_10_IRQHandler(void)
{
    gpio_read_dav_v_busy_intr();
    bluetooth_link_intr_handler();
    button_intr_handler();
	gpio_usb_mux_status_pin_intr();
}

/**
  * @brief  This function handles OTG_HS Handler.
  * @param  None
  * @retval None
  */
#ifdef USE_USB_OTG_HS
void OTG_HS_IRQHandler(void)
{
  USBD_OTG_ISR_Handler (&USB_OTG_dev);
}
#endif

#ifdef USE_USB_OTG_FS  
void OTG_FS_IRQHandler(void)
{
  USBD_OTG_ISR_Handler (&USB_OTG_dev);
}
#endif

#ifdef USB_OTG_HS_DEDICATED_EP1_ENABLED 
/**
  * @brief  This function handles EP1_IN Handler.
  * @param  None
  * @retval None
  */
void OTG_HS_EP1_IN_IRQHandler(void)
{
  USBD_OTG_EP1IN_ISR_Handler (&USB_OTG_dev);
}

/**
  * @brief  This function handles EP1_OUT Handler.
  * @param  None
  * @retval None
  */
void OTG_HS_EP1_OUT_IRQHandler(void)
{
  USBD_OTG_EP1OUT_ISR_Handler (&USB_OTG_dev);
}
#endif

/******************************************************************************/
/*                 STM32F2xx Peripherals Interrupt Handlers                   */
/*  Add here the Interrupt Handler for the used peripheral(s) (PPP), for the  */
/*  available peripheral interrupt handler's name please refer to the startup */
/*  file (startup_stm32f2xx.s).                                               */
/******************************************************************************/

/**
  * @brief  This function handles PPP interrupt request.
  * @param  None
  * @retval None
  */
/*void PPP_IRQHandler(void)
{
}*/

/**
  * @}
  */ 


/******************* (C) COPYRIGHT 2011 STMicroelectronics *****END OF FILE****/
