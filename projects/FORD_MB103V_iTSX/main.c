/**
  **************** (C) COPYRIGHT 2011 SCT Performance, LLC *********************
  * File Name          : main.c
  * Device / System    : SCT products using the "CommonCode" platform
  * Author             : Quyen Leba
  *
  * Version            : 1 
  * Date               : 05/02/2011
  * Description        : 
  *                    : 
  *
  *
  * History            : 05/02/2011 M. Davis
  *                    :   Add SCT Copyright header,
  *
  ******************************************************************************
  */

#ifdef __PRODUCTION_TEST__
    #include <board/MB103V/Test/test.h>
#endif

#include <arch/gentype.h>
#include <board/genplatform.h>
#include <common/cmdif.h>
#include <common/debug/debug_output.h>
#include <common/housekeeping.h>
#include <common/usbcmdhandler.h>
#include <common/memcheck.h>

int main()
{
#ifdef __DEBUG_JTAG_
    memcheck_init();
    vmalloc_init();
#endif
    uch_init();
#ifdef __PRODUCTION_TEST__
    test_init();
    test_exec();
#endif
    
    init();
    debug_init();

    while(1)
    {
        debug_console();
        cmdif_handler();
        housekeeping_process();
#ifdef __DEBUG_JTAG_
        memcheck_test();
#endif
    }//while(1)...
}

