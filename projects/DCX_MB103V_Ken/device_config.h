/**
  **************** (C) COPYRIGHT 2011 SCT Performance, LLC *********************
  * File Name          : device_config.h
  * Device / System    : SCT products using the "CommonCode" platform
  * Author             : Quyen Leba
  *
  * Version            : 1 
  * Date               : 05/02/2011
  * Description        : 
  *                    : 
  *
  *
  * History            : 05/02/2011 M. Davis
  *                    :   Add SCT Copyright header,
  *
  ******************************************************************************
  */

#ifndef __DEVICE_CONFIG_H
#define __DEVICE_CONFIG_H

#include <common/devicedef.h>
#include <common/obd2def.h>

#define DEFAULT_USB_PID         		0x4040
#define DEFAULT_USB_POWER               0xFA        //500mA
#define SUPPORT_DEVICE_MARKET           0x1234      //for now as example

// -----------------------------------------------------------------------------
// Only INF files are provided. Assume device driver is already installed. 
// Go to device manager to uninstall current driver then update driver manually
// with "Regular" or "HasVirtualCOM" folder.
//
// Define ONE of the below modes for use with VCP
#undef  __KEN_EMULATOR__    // LWTS Commlink emulation through VCP
#undef  __VCP_DEBUG_OUT__   // Debug Terminal Input/Output through VCP

#if defined (__KEN_EMULATOR__) && defined (__VCP_DEBUG_OUT__)
#error "ERROR: BOTH __KEN_EMULATOR__ AND  __VCP_DEBUG_OUT__ CANNOT BE ENABLED AT THE SAME TIME"
#endif 

#if defined (__KEN_EMULATOR__) || defined (__VCP_DEBUG_OUT__)
#ifdef __DEBUG_JTAG_
#define USB_HAS_VIRTUAL_COM_PORT        1
#else
#error "ERROR: __KEN_EMULATOR__ or __VCP_DEBUG_OUT__ CANNOT BE ENABLED FOR RELEASE"
#endif
#endif

//------------------------------------------------------------------------------

#define ENGINEERING_SIGNATURE           0x5F1C83A0

#define USE_SEGGER_EMFILE               1
#define USE_HCC_EMBEDDED_FS             0

#define USE_WIFI                        1
#define USE_CC3000_WIFI                 1

#define USE_COMMLINK_BT11_BLUETOOTH     0
#define USE_COMMLINK_KEN257             1
#define USE_COMMLINK_X4                 0
#define USE_COMMLINK_BAUD               COMMLINK_115200_BAUD
#define COMMLINK_FLOWCONTROL_SETUP      UART_NO_FLOWCONTROL

//not used in normal operations
#define FORCE_COMMLINK_STATUS_AS_OPENED 0
#define SUPPORT_IGN_KEY_DETECTION       0
#define DEFAULT_CRITICALSETTINGS_FLAGS  (SettingsDeviceFlags_UploadOnlySupport)

#define FS_MAX_SECTOR_SIZE              512

#define SUPPORT_OTF                     0
#define DATALOG_MAX_RECORD_FILE_SIZE    (10*1024*1024)

#define DATALOG_FEATURES_FLAGS          DATALOG_FEATURES_DTC |              \
                                        DATALOG_FEATURES_PACKET_PID

#define TIME_LOOP_COUNT_10MS            71000
#define TIME_LOOP_COUNT_30MS            216000
#define TIME_LOOP_COUNT_100MS           724000

// MBLIF_MODE_TYPE: LWTS uses NO SPI CRC, CC3000 cannot use SPI CRC and STM32 SPI
// hardware requires constant resync of CRC on both sides when used in mutislave 
// setup, it's not worth the trouble to run SPI CRC over DMA.
#define MBLIF_MODE_TYPE                 MBLIF_MODE_DMA_NO_CRC

#define OEM_TYPE                        OemType_DCX
#define DEVICE_TYPE                     DeviceType_Ken_DCX
// If DEVICE_TYPE is "BANKS" or "GMBANKS", define "BANKS_ENG" to enable BANKS menus
#if (DEVICE_TYPE == BANKS) || (DEVICE_TYPE == GMBANKS)
    #define BANKS_ENG                   1
#endif

#define MARKET_TYPE                     MarketType_Any
#define MARKET_TYPE_STRING              UNKNOWN_MARKET_TYPE_STRING

#define SUPPORT_UPLOAD_SPECIAL_STOCK    1
#define HARDWARE_LOOKUP_FILE            "DCX_PartTypeList.txt"
#define OS_LOOKUP_FILE                  "??.txt"

// ---------------------------------------------------------------------------
// DTC File info
// This is used by the Read DTC function to read DTC descriptions
// Lookup table can be updated as necessary.
// ---------------------------------------------------------------------------
#define DTC_FILE_COUNT                  2
#define DTC_FILENAME_KWP                "DTCDB_DCX_KWP2K.txt"
#define DTC_FILENAME_OBD2               "DTCDB_OBD2.txt"

typedef struct
{
    VehicleCommLevel vehiclecommlevel;
    char filename[20];
}DTC_FILE;

const static DTC_FILE dtc_file_lookup[DTC_FILE_COUNT] = 
{
    [0] = 
    {
    .vehiclecommlevel = CommLevel_KWP2000,
    .filename = DTC_FILENAME_KWP,
    },
	[1] = 
    {
    .vehiclecommlevel = CommLevel_Unknown,
    .filename = DTC_FILENAME_OBD2,
    },
};

#endif	//__DEVICE_CONFIG_H
