/*********************************************************************
*                SEGGER MICROCONTROLLER GmbH & Co. KG                *
*        Solutions for real time microcontroller applications        *
**********************************************************************
*                                                                    *
*        (c) 2003-2012     SEGGER Microcontroller GmbH & Co KG       *
*                                                                    *
*        Internet: www.segger.com    Support:  support@segger.com    *
*                                                                    *
**********************************************************************

**** emFile file system for embedded applications ****
emFile is protected by international copyright laws. Knowledge of the
source code may not be used to write a similar product. This file may
only be used in accordance with a license and should not be re-
distributed in any way. We appreciate your understanding and fairness.
----------------------------------------------------------------------
File        : NOR_BM_Drv.c
Purpose     : File system high level NOR flash driver with reduced RAM usage.
---------------------------END-OF-HEADER------------------------------

General info on the inner workings of this high level flash driver:

Layered approach
================
All read, write and erase operations are performed by the low level
flash driver. The low level flash driver is also responsible for
returning information about the sectorization of the flash.
This driver assumes the following:
- The flash is organized in physical sectors
- The phys. sectors are at least 1kb in size
- Erasing a phys. sector fills all bytes with FF
- Writing is permitted in arbitrary units (bytes)
- Writing can change bits from 1 to 0, even if the byte already
  had a value other than FF

Data management
===============
  Data is stored in so called data blocks in the NOR flash. The assignment
  information (which pyhsical sector contains which data) is stored in the
  sector header. Modifications of data are not done in the data blocks directly,
  but using a concept of work blocks. A work block contains modifications of a data block.
  The first physical sector is used to store format information and written only once.
  All other physical sectors are used to store data. This means that in the driver,
  a valid physical sector index is always > 0.

Reading data
============
  So when data is read, the driver checks
  a) Is there a work block which contains this information ? If so, this is recent and used.
  b) Is there a data block which contains this information ? If so, this is recent and used.
  c) Otherwise, the sector has never been written. In this case, the driver delivers 0xFF data bytes.

Writing data to the NOR flash
=============================
  This is a complicated task.

Abbreviations
=============
LBI
    Logical Block Index; Gives the data position of a logical block.
BRSI
    Block Relative Sector Index: Index of logical sector relative to start of logical block.
PSI
    Physical Sector Index: Gives the position of a NOR physical sector.
SRSI
    Sector Relative Sector Index: Index of logical sector relative to start of physical sector.

----------------------------------------------------------------------
Potential improvements

  Improve speed
    - Optionally, keep erase count for every block in RAM (for systems with large amount of RAM) to speed up block search
    - Optionally, check before conversion if all the sectors of a work block are valid

  Reducing RAM size
    - Optionally, pFreeMap can be eliminated (free block is searched each time)
----------------------------------------------------------------------
*/

#include "FS_Int.h"
#include "NOR_Private.h"

/*********************************************************************
*
*       Defines, configurable
*
**********************************************************************
*/
#ifndef   FS_NOR_NUM_UNITS
  #define FS_NOR_NUM_UNITS            4
#endif

#ifndef   FS_NOR_MAX_ERASE_CNT_DIFF
  #define FS_NOR_MAX_ERASE_CNT_DIFF   5000    // Determines Active wear leveling threshold, at which a block containing unchanged data is copied and freed. 0 means no active wearleveling.
#endif

#ifndef   FS_NOR_ENABLE_STATS
  #define FS_NOR_ENABLE_STATS         (FS_DEBUG_LEVEL >= FS_DEBUG_LEVEL_CHECK_ALL)   // Statistics only in debug builds
#endif

#ifndef   FS_NOR_PHY_SECTOR_RESERVE
  #define FS_NOR_PHY_SECTOR_RESERVE   8
#endif

#ifndef   FS_NOR_LOG_SECTOR_RESERVE
  #define FS_NOR_LOG_SECTOR_RESERVE   4
#endif

/*********************************************************************
*
*       Defines, non configurable
*
**********************************************************************
*/
#define LLFORMAT_VERSION    10001u

#if FS_NOR_ENABLE_STATS
  #define IF_STATS(exp) exp
#else
  #define IF_STATS(exp)
#endif

/*********************************************************************
*
*       Special values for "INVALID"
*/
#define BRSI_INVALID        0xFFFF         // Invalid relative sector index
#define ERASE_CNT_INVALID   0xFFFFFFFFUL   // Invalid erase count

/*********************************************************************
*
*       Status of data in a physical sector
*/
#define DATA_STAT_EMPTY     0xFF      // Block is empty
#define DATA_STAT_WORK      0xFE      // Block is used as "work block"
#define DATA_STAT_VALID     0xFC      // Block contains valid data
#define DATA_STAT_INVALID   0x00      // Block contains old, invalid data

/*********************************************************************
*
*       Status of read/write NOR operations
*/
#define NOR_WRITE_ERROR           1   // Error while writing to NOR flash
#define NOR_ERASE_ERROR           2   // Error while erasing a physical sector of the NOR flash
#define NOR_OUT_OF_FREE_SECTORS   3   // Tried to allocate a free physical sector but no more were found

/*********************************************************************
*
*       Const data
*
**********************************************************************
*/
#define INFO_BLOCK_PSI            0

#if FS_SUPPORT_JOURNAL
  #define NUM_WORK_BLOCKS_MIN     4   // For performance reasons we need more work blocks when Journaling is enabled
#else
  #define NUM_WORK_BLOCKS_MIN     3
#endif

#define NUM_WORK_BLOCKS_MAX       10

/****************************************************************************************
*
*       First physical sector in a NOR flash should have these values
*       for the NOR driver to recognize the device as properly formatted.
*/
static const U8 _acInfo[16] = {
  0x53, 0x45, 0x47, 0x47, 0x45, 0x52
};
#define INFO_OFF_LLFORMAT_VERSION         0x10
#define INFO_OFF_BYTES_PER_SECTOR         0x20
#define INFO_OFF_NUM_LOG_BLOCKS           0x30
#define INFO_OFF_NUM_WORK_BLOCKS          0x40

/*********************************************************************
*
*       Type definitions
*
**********************************************************************
*/

/*********************************************************************
*
*       PHY_SECTOR_HEADER
*/
typedef struct {
  U8  DataStat;           // Type of data stored by the physical sector
  U8  DataCnt;            // Number of times this physical sector has been copied
  U16 lbi;                // Index of the logical block stored in this physical sector
  U32 EraseCnt;           // Number of times the sector has been erased
  U8  aReserved[FS_NOR_PHY_SECTOR_RESERVE];
#if FS_NOR_LINE_SIZE > 16
  U8  Padding[FS_NOR_LINE_SIZE - 16];   // Pad to line size
#endif
#if FS_NOR_CAN_REWRITE == 0
  U8  IsWork;
  U8  Padding2[FS_NOR_LINE_SIZE - 1];
  U8  IsValid;
  U8  Padding3[FS_NOR_LINE_SIZE - 1];
  U8  IsInvalid;
  U8  Padding4[FS_NOR_LINE_SIZE - 1];
#endif
} PHY_SECTOR_HEADER;

/*********************************************************************
*
*       LOG_SECTOR_HEADER
*/
typedef struct {
  U8  DataStat;           // Status of data stored in the logical sector (empty, valid, invalid)
  U8  Padding;
  U16 brsi;               // Index of the logical sector relative to begin of logical block
  U8  aReserved[FS_NOR_LOG_SECTOR_RESERVE];
#if FS_NOR_LINE_SIZE > 8
  U8  Padding2[FS_NOR_LINE_SIZE - 8];   // Pad to line size
#endif
#if FS_NOR_CAN_REWRITE == 0
  U8  IsValid;
  U8  Padding3[FS_NOR_LINE_SIZE - 1];
  U8  IsInvalid;
  U8  Padding4[FS_NOR_LINE_SIZE - 1];
#endif
} LOG_SECTOR_HEADER;

/*********************************************************************
*
*       WORK_BLOCK
*
*  Organisation of a work block
*  ============================
*
*  The WORK_BLOCK structure has 6 elements, as can be seen below.
*  The first 2, pNext & pPrev, are used to keep it in a doubly linked list.
*  The next 2 elements are used to associate it with a data block and logical block index.
*  The last 2 elements contain the actual managment data. The are pointers to arrays, allocated during init.
*  paIsWritten is a 1-bit array.
*    There is one bit = one entry for every sector in the block.
*  paAssign is a n bit array.
*    The number of bits is determined by the number of sectors per block.
*    The index is the logical position (brsi, block relative sector index).
*/
typedef struct WORK_BLOCK_DESC {
  struct WORK_BLOCK_DESC * pNext; // Pointer to next work buffer.     NULL if there is no next.
  struct WORK_BLOCK_DESC * pPrev; // Pointer to previous work buffer. NULL if there is no previous.
  unsigned   psi;                 // Physical index of the destination sector which data is written to. 0 means none is selected yet.
  unsigned   lbi;                 // Logical block index of the work block
  U8       * paIsWritten;         // Pointer to IsWritten table, which indicates which sectors have already been transferred into the work buffer. This is a 1-bit array.
  void     * paAssign;            // Pointer to assignment table, containing n bits per block. n depends on number of sectors per block.
} WORK_BLOCK_DESC;

/*********************************************************************
*
*       NOR_BM_INST structure
*
*  This is the central data structure for the entire driver.
*  It contains data items of one instance of the driver.
*/
typedef struct {
  U8    Unit;
  U8    IsInited;               // 1 if the driver has been initialized. Set to 0 when the NOR flash is unmounted.
  U8    IsLLMounted;
  U8    LLMountFailed;
  U8    HasFatalError;
  U8    ErrorType;              // Type of fatal error
  U16   ErrorPSI;               // Index of the physical sector where the fatal error occurred
  const FS_NOR_PHY_TYPE * pPhyType;   // Interface to physical layer
  U8  * pFreeMap;               // Pointer to physical sector usage map. Each bit represents one physical sector. 0: Block is not assigned; 1: Assigned.
                                // Only purpose is to find a free physical sector.
  U8  * pLog2PhyTable;          // Pointer to Log2Phytable, which contains the logical block to physical sector translation (0: Not assigned)
  U32   NumLogSectors;          // Number of logical sectors. This is redundant, but the value is used in a lot of places, so it is worth it!
  U16   NumPhySectors;
  U32   PhySectorSize;          // Size of a physical sector in bytes
  U16   NumLogBlocks;
  U32   EraseCntMax;            // Worst (= highest) erase count of all physical sectors
  U32   EraseCntMin;            // Smallest erase count of all physical sectors. Used for active wear leveling.
  U32   NumBlocksEraseCntMin;   // Number of erase counts with the smallest value
  WORK_BLOCK_DESC * pFirstWorkBlockInUse;   // Pointer to the first work block
  WORK_BLOCK_DESC * pFirstWorkBlockFree;    // Pointer to the first free work block
  WORK_BLOCK_DESC * paWorkBlock;            // WorkBlock management info
  U32   MRUFreeBlock;           // Most recently used free physical sector
  U16   ldBytesPerSector;       // Number of user data bytes in a logical sector (as power of 2)
  U16   LSectorsPerPSector;     // Number of logical sectors that fit in a physical sector
  U8    NumWorkBlocks;          // Number of configured work blocks
  U8    NumBitsPhySectorIndex;
  U8    NumBitsSRSI;
  U8    NumBytesIsWritten;
  U16   FirstPhySector;         // Index of the first physical sector used by the driver
  //
  // Configuration items. 0 per default, which typically means: Use a reasonable default
  //
  U32   MaxEraseCntDiff;        // Threshold for active wear leveling
  U16   BytesPerSectorConf;     // Number of bytes available to file system in a logical sector
  U8    NumWorkBlocksConf;      // Number of work blocks configured by application
  //
  // Additional info for debugging purposes
  //
#if FS_NOR_ENABLE_STATS
  FS_NOR_BM_STAT_COUNTERS   StatCounters;
#endif
} NOR_BM_INST;

/*********************************************************************
*
*       Static data
*
**********************************************************************
*/
static NOR_BM_INST * _apInst[FS_NOR_NUM_UNITS];
static int           _NumUnits;

/*********************************************************************
*
*       Macros, function replacement
*
**********************************************************************
*/

/*********************************************************************
*
*       Assertion macros
*/
#if FS_DEBUG_LEVEL >= FS_DEBUG_LEVEL_CHECK_ALL
  #define ASSERT_UNIT_NO_IS_IN_RANGE(Unit)                            if (Unit >= _NumUnits)                      { FS_DEBUG_ERROROUT((FS_MTYPE_DRIVER,  "NOR_BM: Invalid unit number.\n")); }
  #define ASSERT_PHY_TYPE_IS_SET(pInst)                               if ((const void *)pInst->pPhyType == NULL)  { FS_DEBUG_ERROROUT((FS_MTYPE_DRIVER,  "NOR_BM: PHY type not set.\n")); }
  #define ASSERT_SECTORS_ARE_IN_RANGE(pInst, FirstSector, LastSector) if (((FirstSector) >= pInst->NumLogSectors) || ((LastSector) >= pInst->NumLogSectors)) { FS_DEBUG_ERROROUT((FS_MTYPE_DRIVER,  "NOR_BM: Invalid sector number.\n")); }
#else
  #define ASSERT_UNIT_NO_IS_IN_RANGE(Unit)
  #define ASSERT_PHY_TYPE_IS_SET(pInst)
  #define ASSERT_SECTORS_ARE_IN_RANGE(pInst, FirstSector, LastSector)
#endif

/*********************************************************************
*
*       Local functions
*
**********************************************************************
*/

/*********************************************************************
*
*       _ld
*/
static unsigned _ld(U32 Value) {
  unsigned i;

  for (i = 0; i < 16; i++) {
    if ((1UL << i) == Value) {
      break;
    }
  }
  return i;
}

/*********************************************************************
*
*       _Find0BitInByte
*
*  Function description
*     Returns the position of the first 0-bit in a byte. The function checks only the bits
*     between the offsets FirstBit and LastBit inclusive.
*
*  Parameters
*     FirstBit  Position of the first bit to check (0-based)
*     LastBit   Position of the last bit to check (0-based)
*     Off       Byte offset to be added to the found bit position
*
*  Return value
*    >= 0 On Success, Bit position of first 0
*    -1   On Error, No 0-bit found
*/
static int _Find0BitInByte(U8 Data, unsigned FirstBit, unsigned LastBit, unsigned Off) {
  unsigned i;

  for (i = FirstBit; i <= LastBit; i++) {
    if ((Data & (1 << i)) == 0) {
      return i + (Off << 3);
    }
  }
  return -1;
}

/*********************************************************************
*
*       _Find0BitInArray
*
*  Function description
*    Finds the first 0-bit in a byte array. Bits are numbered lsb first as follows:
*    00000000 11111100
*    76543210 54321098
*    So the first byte contains bits 0..7, second byte contains bits 8..15, third byte contains 16..23
*
*  Return value
*    >= 0 On Success, Bit position of first 0
*    -1   On Error, No 0-bit found
*
*  Examples
*    Min = 1, Max = 13, pData points to 0xff, 0x
*/
static int _Find0BitInArray(const U8 * pData, unsigned FirstBit, unsigned LastBit) {
  unsigned FirstOff;
  unsigned LastOff;
  U8 Data;
  int i;

  FirstOff = FirstBit >> 3;
  LastOff  = LastBit  >> 3;
  pData   += FirstOff;

  //
  // Handle first byte
  //
  Data = *pData++;
  if (FirstOff == LastOff) {      // Special case where first and last byte are the same ?
    i = _Find0BitInByte(Data, FirstBit & 7, LastBit & 7, FirstOff);
    return i;
  }
  i = _Find0BitInByte(Data, FirstBit & 7, 7, FirstOff);
  if (i >= 0) {
    return i + (FirstOff << 3);
  }
  //
  // Handle complete bytes
  //
  for (i = FirstOff + 1; (unsigned)i < LastOff; i++) {
    Data = *pData++;
    if (Data != 0xFF) {
      return _Find0BitInByte(Data, 0, 7, i);
    }
  }
  //
  // Handle last byte
  //
  Data = *pData;
  return _Find0BitInByte(Data, 0, LastBit & 7, i);
}

/*********************************************************************
*
*       _CalcNumWorkBlocksDefault
*
*   Function description
*     Computes the the default number of work blocks.
*     This is a percentage of number of NOR physical sectors.
*/
static U32 _CalcNumWorkBlocksDefault(U32 NumPhyBlocks) {
  U32 NumWorkBlocks;

  //
  // Allocate 10% of NOR capacity for work blocks
  //
  NumWorkBlocks = NumPhyBlocks >> 7;
  //
  // Limit the number of work blocks to reasonable values
  //
  if (NumWorkBlocks > NUM_WORK_BLOCKS_MAX) {
    NumWorkBlocks = NUM_WORK_BLOCKS_MAX;
  }
  if (NumWorkBlocks < NUM_WORK_BLOCKS_MIN) {
    NumWorkBlocks = NUM_WORK_BLOCKS_MIN;
  }
  return NumWorkBlocks;
}

/*********************************************************************
*
*       _CalcNumBlocksToUse
*
*   Function description
*     Computes the number of logical blocks available to file system.
*/
static int _CalcNumBlocksToUse(U32 NumPhyBlocks, U32 NumWorkBlocks) {
  int NumLogBlocks;
  int Reserve;

  //
  // Compute the number of logical blocks. These are the blocks which are
  // actually available to the file system and therefor determines the capacity.
  // We reserve some sectors for driver management: the number of work blocks + 1 info block (first block) + 1 block for copy operations
  //
  NumLogBlocks  = NumPhyBlocks;
  Reserve       = NumWorkBlocks + 2;
  NumLogBlocks -= Reserve;
  return NumLogBlocks;
}

/*********************************************************************
*
*       _FindSectorRangeToUse
*
*   Function description
*     Searches for the longest continuous range of physical sectors with the same size.
*     Typ. called at driver initialization to determine which physical sectors should be used as storage.
*/
static unsigned _FindSectorRangeToUse(NOR_BM_INST * pInst, unsigned * pFirstPhySector, U32 * pPhySectorSize) {
  int      NumPhySectors;
  U32      PhySectorSize;
  U32      NumPhySectorsToUse;
  U32      NumPhySectorsInRange;
  unsigned FirstPhySector;
  U32      PhySectorIndex;
  U32      LenPrev;

  NumPhySectors = pInst->pPhyType->pfGetNumSectors(pInst->Unit);
  if (NumPhySectors <= 0) {
    return 0;
  }
  //
  // Search for the longest sequence of sectors with the same size
  //
  LenPrev              = 0;
  FirstPhySector       = 0;
  PhySectorSize        = 0;
  NumPhySectorsToUse   = 0;
  NumPhySectorsInRange = 0;
  PhySectorIndex       = 0;
  do {
    U32 Off;
    U32 Len;

    pInst->pPhyType->pfGetSectorInfo(pInst->Unit, PhySectorIndex, &Off, &Len);
    if (Len != LenPrev) {       // New sector range ?
      if (NumPhySectorsInRange > NumPhySectorsToUse) {
        FirstPhySector     = PhySectorIndex - NumPhySectorsInRange;
        PhySectorSize      = LenPrev;
        NumPhySectorsToUse = NumPhySectorsInRange;
      }
      NumPhySectorsInRange = 0;
    }
    ++NumPhySectorsInRange;
    ++PhySectorIndex;
    LenPrev = Len;
  } while (--NumPhySectors);
  if (NumPhySectorsToUse == 0) {
    PhySectorSize      = LenPrev;
    NumPhySectorsToUse = NumPhySectorsInRange;
  }
  if (pFirstPhySector) {
    *pFirstPhySector = FirstPhySector;
  }
  if (pPhySectorSize) {
    *pPhySectorSize  = PhySectorSize;
  }
  return NumPhySectorsToUse;
}

/*********************************************************************
*
*       _CalcLSectorsPerPSector
*
*   Function description
*     Computes the number of logical sectors that fit in a physical sector.
*/
static unsigned _CalcLSectorsPerPSector(U32 PhySectorSize, U32 LogSectorSize) {
  return (PhySectorSize - sizeof(PHY_SECTOR_HEADER)) / (sizeof(LOG_SECTOR_HEADER) + LogSectorSize);
}

/*********************************************************************
*
*       _ReadApplyDeviceParas
*
*   Function description
*     Reads the device info and computes the parameters stored in the instance structure
*     such as number of logical blocks, number of logical sectors etc.
*
*   Return value:
*     ==0   O.K.
*     !=0   Error, Could not apply device paras
*/
static int _ReadApplyDeviceParas(NOR_BM_INST * pInst) {
  unsigned NumPhySectors;
  unsigned NumWorkBlocks;
  int      NumLogBlocks;
  U32      BytesPerSector;
  unsigned LSectorsPerPSector;
  U32      PhySectorSize;
  unsigned FirstPhySector;

  //
  // Determine the range of physical sectors to be used by the driver
  //
  NumPhySectors = _FindSectorRangeToUse(pInst, &FirstPhySector, &PhySectorSize);
  if (NumPhySectors == 0) {
    FS_DEBUG_ERROROUT((FS_MTYPE_DRIVER, "NOR_BM: No sectors found for storage.\n"));
    return 1;
  }
  //
  // Compute a default number of work blocks if the application did not configured it yet.
  //
  if (pInst->NumWorkBlocksConf == 0) {
    NumWorkBlocks = _CalcNumWorkBlocksDefault(NumPhySectors);
  } else {
    NumWorkBlocks = pInst->NumWorkBlocksConf;
  }
  //
  // Compute the number of logical blocks available to file system
  //
  NumLogBlocks = _CalcNumBlocksToUse(NumPhySectors, NumWorkBlocks);
  if (NumLogBlocks <= 0) {
    FS_DEBUG_ERROROUT((FS_MTYPE_DRIVER, "NOR_BM: Insufficient physical sectors.\n"));
    return 1;
  }
  //
  // Set a default sector size if the user did not configured one
  //
  if (pInst->BytesPerSectorConf == 0) {
    BytesPerSector = FS_Global.MaxSectorSize;
  } else {
    BytesPerSector = pInst->BytesPerSectorConf;
  }
  LSectorsPerPSector = _CalcLSectorsPerPSector(PhySectorSize, BytesPerSector);
  //
  // Store the values into the driver instance
  //
  pInst->NumPhySectors         = NumPhySectors;
  pInst->PhySectorSize         = PhySectorSize;
  pInst->NumBitsPhySectorIndex = FS_BITFIELD_CalcNumBitsUsed(NumPhySectors);
  pInst->FirstPhySector        = FirstPhySector;
  pInst->NumLogBlocks          = NumLogBlocks;
  pInst->NumWorkBlocks         = NumWorkBlocks;
  pInst->NumLogSectors         = NumLogBlocks * LSectorsPerPSector;
  pInst->LSectorsPerPSector    = LSectorsPerPSector;
  pInst->NumBitsSRSI           = FS_BITFIELD_CalcNumBitsUsed(LSectorsPerPSector);
  pInst->ldBytesPerSector      = _ld(BytesPerSector);
  return 0;                   // O.K., successfully identified
}

/*********************************************************************
*
*       _GetPhySectorInfo
*
*   Function description
*     Returns the size and the offset in bytes of a physical sector by querying the physical layer.
*/
static void _GetPhySectorInfo(NOR_BM_INST * pInst, unsigned PhySectorIndex, U32 * pOff, U32 * pSize) {
   PhySectorIndex += pInst->FirstPhySector;
   pInst->pPhyType->pfGetSectorInfo(pInst->Unit, PhySectorIndex, pOff, pSize);
}

/*********************************************************************
*
*       _ReadOff
*
*   Function description
*     Reads data from the given offset of the NOR flash.
*/
static int _ReadOff(NOR_BM_INST * pInst, void * pData, U32 Off, U32 NumBytes) {
  int r;

  r = pInst->pPhyType->pfReadOff(pInst->Unit, pData, Off, NumBytes);
  if (r) {
    FS_DEBUG_ERROROUT((FS_MTYPE_DRIVER,  "NOR_BM: Read failed @ 0x%x.", Off));
  }
  return r;
}

/*********************************************************************
*
*        _WriteOff
*
*   Function description
*     Writes data to the NOR flash using the low-level flash driver.
*
*   Return value
*     ==0   O.K., data has been written
*     !=0   Error
*/
static int _WriteOff(NOR_BM_INST * pInst, const void * pData, U32 Off, U32 NumBytes) {
  int r;

  r =  pInst->pPhyType->pfWriteOff(pInst->Unit, Off, pData, NumBytes);
  if (r) {
    FS_DEBUG_ERROROUT((FS_MTYPE_DRIVER,  "NOR_BM: Write failed @ 0x%x.", Off));
  }
  //
  // On higher debug levels, check if the write succeeded.
  //
#if FS_DEBUG_LEVEL >= FS_DEBUG_LEVEL_CHECK_ALL
  else {
    U8  DataRead;
    U8  DataExpected;
    U32 i;

    for (i = 0; i < NumBytes; i++) {
      _ReadOff(pInst, &DataRead, Off + i , 1);
      DataExpected = *((const U8 *)pData + i);
      if (DataRead != DataExpected) {
        FS_DEBUG_ERROROUT((FS_MTYPE_DRIVER, "NOR_BM: Data was not programmed correctly @ 0x%x. Expected 0x%x, read 0x%x\n", Off + i, DataExpected, DataRead));
        r = 1;
      }
    }
  }
#endif
  return r;
}

/*********************************************************************
*
*        _ErasePhySector
*
*/
static int _ErasePhySector(NOR_BM_INST * pInst, U32 PhySectorIndex) {
  int r;

  PhySectorIndex += pInst->FirstPhySector;
  r =  pInst->pPhyType->pfEraseSector(pInst->Unit, PhySectorIndex);
  if (r) {
    FS_DEBUG_ERROROUT((FS_MTYPE_DRIVER, "NOR_BM: Erase failed @ sector 0x%x.", PhySectorIndex));
    return r;
  }
  IF_STATS(pInst->StatCounters.EraseCnt++);     // Increment statistics counter if enabled
  //
  // On higher debug levels, check if erase worked.
  //
#if FS_DEBUG_LEVEL >= FS_DEBUG_LEVEL_CHECK_ALL
  {
    U32 Off;
    U32 Size;
    U32 aData[8];
    U32 NumLoops;
    U32 NumBytes;

    pInst->pPhyType->pfGetSectorInfo(pInst->Unit, PhySectorIndex, &Off, &Size);
    NumLoops = Size / sizeof(aData);
    do {
      U32      * pData;
      unsigned   i;

      //
      // Read data from NOR flash.
      //
      NumBytes = sizeof(aData);
      FS_MEMSET(aData, 0, NumBytes);
      _ReadOff(pInst, aData, Off, NumBytes);
      pData = aData;
      //
      // Verify if the sector is blank.
      //
      for (i = 0; i < COUNTOF(aData); ++i) {
        U32 Data32;

        Data32 = *pData++;
        if (Data32 != 0xFFFFFFFF) {
          FS_DEBUG_ERROROUT((FS_MTYPE_DRIVER,  "NOR_BM: Erase verification failed @ 0x%x with 0x%x", Off + i, Data32));
          return 1;
        }
      }
      Off += NumBytes;
    } while (--NumLoops);
  }
#endif
  return 0;
}

/*********************************************************************
*
*       _GetLogSectorOff_Header
*/
static U32 _GetLogSectorOff_Header(NOR_BM_INST * pInst, unsigned PhySectorIndex, unsigned srsi) {
  U32 Off;

  _GetPhySectorInfo(pInst, PhySectorIndex, &Off, NULL);
  Off += sizeof(PHY_SECTOR_HEADER);
  Off += (sizeof(LOG_SECTOR_HEADER) + (1 << pInst->ldBytesPerSector)) * srsi;
  return Off;
}

/*********************************************************************
*
*       _GetLogSectorOff_Data
*/
static U32 _GetLogSectorOff_Data(NOR_BM_INST * pInst, unsigned PhySectorIndex, unsigned srsi) {
  U32 Off;

  Off  = _GetLogSectorOff_Header(pInst, PhySectorIndex, srsi);
  Off += sizeof(LOG_SECTOR_HEADER);
  return Off;
}

/*********************************************************************
*
*       _WriteLogSectorData
*/
static int _WriteLogSectorData(NOR_BM_INST * pInst, unsigned PhySectorIndex, unsigned srsi, const void * pData, unsigned OffData, unsigned NumBytes) {
  int r;
  U32 Off;
#if FS_NOR_LINE_SIZE
  U32 aBuffer[FS_NOR_LINE_SIZE / 4];
#endif

  Off = _GetLogSectorOff_Data(pInst, PhySectorIndex, srsi);
#if FS_NOR_LINE_SIZE
  //
  // Make sure that we write at least one flash line.
  //
  if (NumBytes < FS_NOR_LINE_SIZE) {
    FS_MEMSET(aBuffer, 0xFF, sizeof(aBuffer));      // Leave unchanged the flash locations which are not written.
    FS_MEMCPY(aBuffer, pData, NumBytes);
    pData    = aBuffer;
    NumBytes = sizeof(aBuffer);
  }
#endif
  r   = _WriteOff(pInst, pData, Off + OffData, NumBytes);
  return r;
}

/*********************************************************************
*
*       _ReadLogSectorData
*/
static int _ReadLogSectorData(NOR_BM_INST * pInst, unsigned PhySectorIndex, unsigned srsi, void * pData, unsigned OffData, unsigned NumBytes) {
  int r;
  U32 Off;

  Off = _GetLogSectorOff_Data(pInst, PhySectorIndex, srsi);
  r   = _ReadOff(pInst, pData, Off + OffData, NumBytes);
  return r;
}

/*********************************************************************
*
*       _GetPhySectorDataStat
*/
static unsigned _GetPhySectorDataStat(PHY_SECTOR_HEADER * psh) {
  unsigned DataStat;

#if (FS_NOR_CAN_REWRITE == 0)
  if        (psh->IsInvalid == 0) {   // Reversed logic: 0 means data is invalid
    DataStat = DATA_STAT_INVALID;
  } else if (psh->IsValid == 0) {     // Reversed logic: 0 means data block
    DataStat = DATA_STAT_VALID;
  } else if (psh->IsWork == 0) {      // Reversed logic: 0 means work block
    DataStat = DATA_STAT_WORK;
  } else {
    DataStat = DATA_STAT_EMPTY;
  }
#else
  DataStat   = psh->DataStat;
#endif
  return DataStat;
}

/*********************************************************************
*
*       _SetPhySectorDataStat
*/
static void _SetPhySectorDataStat(PHY_SECTOR_HEADER * psh, unsigned DataStat) {
#if (FS_NOR_CAN_REWRITE == 0)
  if        (DataStat == DATA_STAT_WORK) {
    psh->IsWork    = 0;               // Reversed logic: 0 means work block
  } else if (DataStat == DATA_STAT_VALID) {
    psh->IsValid   = 0;               // Reversed logic: 0 means data block
  } else if (DataStat == DATA_STAT_INVALID) {
    psh->IsInvalid = 0;               // Reversed logic: 0 means data is invalid
  }
#else
  psh->DataStat = DataStat;
#endif
}

/*********************************************************************
*
*       _GetLogSectorDataStat
*/
static unsigned _GetLogSectorDataStat(LOG_SECTOR_HEADER * lsh) {
  unsigned DataStat;

#if (FS_NOR_CAN_REWRITE == 0)
  DataStat = DATA_STAT_VALID;
  if        (lsh->IsInvalid == 0) {     // Reversed logic: 0 means invalid
    DataStat = DATA_STAT_INVALID;
  } else if (lsh->IsValid == 0) {       // Reversed logic: 0 means valid
    DataStat = DATA_STAT_VALID;
  } else {
    DataStat = DATA_STAT_EMPTY;
  }
#else
  DataStat = lsh->DataStat;
#endif
  return DataStat;
}

/*********************************************************************
*
*       _SetLogSectorDataStat
*/
static void _SetLogSectorDataStat(LOG_SECTOR_HEADER * lsh, unsigned DataStat) {
#if (FS_NOR_CAN_REWRITE == 0)
  if        (DataStat == DATA_STAT_VALID) {
    lsh->IsValid = 0;                   // Reversed logic: 0 means valid
  } else if (DataStat == DATA_STAT_INVALID) {
    lsh->IsInvalid = 0;                 // Reversed logic: 0 means invalid
  }
#else
  lsh->DataStat = DataStat;
#endif
}

/*********************************************************************
*
*        _CopyLogSectorData
*
*   Function description
*     Copies the data part of a logical sector. The logical sector header is  not copied.
*
*   Notes
*     (1) The sector is copied in multiple chunks, where a small
*         array on the stack is used as buffer. The reason why the data
*         is copied in multiple chunks is simply to keep the stack load low.
*
*/
static int _CopyLogSectorData(NOR_BM_INST * pInst, unsigned psiSrc, unsigned srsiSrc, unsigned psiDest, unsigned srsiDest) {
  U32      acBuffer[32];
  int      r;
  unsigned NumBytesAtOnce;
  unsigned NumBytes;
  U32      OffSrc;
  U32      OffDest;

  r = 0;
  NumBytes = 1u << pInst->ldBytesPerSector;
  OffSrc   = _GetLogSectorOff_Data(pInst, psiSrc,  srsiSrc);
  OffDest  = _GetLogSectorOff_Data(pInst, psiDest, srsiDest);
  do {
    NumBytesAtOnce = MIN(NumBytes, sizeof(acBuffer));
    _ReadOff(pInst, acBuffer, OffSrc,  NumBytesAtOnce);
    r |= _WriteOff(pInst, acBuffer, OffDest, NumBytesAtOnce);
    NumBytes -= NumBytesAtOnce;
    OffSrc   += NumBytesAtOnce;
    OffDest  += NumBytesAtOnce;
  } while (NumBytes);
  return r;
}

/*********************************************************************
*
*       _IsLogSectorBlank
*
*   Function description
*     It checks if all data bytes of a logical sector are set to 0xFF.
*
*   Return value
*     ==1  Sector is blank
*     ==0  Sector is not blank
*
*/
static int _IsLogSectorBlank(NOR_BM_INST * pInst, unsigned PhySectorIndex, unsigned srsi) {
  U32        acBuffer[32];
  U32        Off;
  unsigned   NumBytes;
  unsigned   NumBytesAtOnce;
  unsigned   NumItems;
  U32      * p;

  NumBytes = 1u << pInst->ldBytesPerSector;
  Off      = _GetLogSectorOff_Data(pInst, PhySectorIndex,  srsi);
  do {
    NumBytesAtOnce = MIN(NumBytes, sizeof(acBuffer));
    _ReadOff(pInst, acBuffer, Off, NumBytesAtOnce);
    NumItems = NumBytesAtOnce >> 2;
    p        = acBuffer;
    do {
      if (*p++ != 0xFFFFFFFFUL) {
        return 0;
      }
    } while (--NumItems);
    NumBytes -= NumBytesAtOnce;
    Off      += NumBytesAtOnce;
  } while (NumBytes);
  return 1;
}

/*********************************************************************
*
*       _WritePSH
*
*   Function description
*     Writes the header of a given physical sector.
*/
static int _WritePSH(NOR_BM_INST * pInst, unsigned PhySectorIndex, const PHY_SECTOR_HEADER * pPSH) {
  U32 Off;
  int r;

  _GetPhySectorInfo(pInst, PhySectorIndex, &Off, NULL);
  r = _WriteOff(pInst, pPSH, Off, sizeof(PHY_SECTOR_HEADER));
  return r;
}

/*********************************************************************
*
*       _ReadPSH
*
*   Function description
*     Reads the header of a physical sector.
*/
static int _ReadPSH(NOR_BM_INST * pInst, unsigned PhySectorIndex, PHY_SECTOR_HEADER * pPSH) {
  U32 Off;
  int r;

  _GetPhySectorInfo(pInst, PhySectorIndex, &Off, NULL);
  r = _ReadOff(pInst, pPSH, Off, sizeof(PHY_SECTOR_HEADER));
  return r;
}

/*********************************************************************
*
*       _WriteLSH
*
*   Function description
*     Reads the header of a logical sector.
*/
static int _WriteLSH(NOR_BM_INST * pInst, unsigned PhySectorIndex, unsigned srsi, const LOG_SECTOR_HEADER * pLSH) {
  U32 Off;
  int r;

  Off = _GetLogSectorOff_Header(pInst, PhySectorIndex, srsi);
  r   = _WriteOff(pInst, pLSH, Off, sizeof(LOG_SECTOR_HEADER));
  return r;
}

/*********************************************************************
*
*       _ReadLSH
*
*   Function description
*     Reads the header of a logical sector.
*/
static int _ReadLSH(NOR_BM_INST * pInst, unsigned PhySectorIndex, unsigned srsi, LOG_SECTOR_HEADER * pLSH) {
  U32 Off;
  int r;

  Off = _GetLogSectorOff_Header(pInst, PhySectorIndex, srsi);
  r   = _ReadOff(pInst, pLSH, Off, sizeof(LOG_SECTOR_HEADER));
  return r;
}

/*********************************************************************
*
*       _LogSectorIndex2LogBlockIndex
*
*   Function description
*     Computes the index of the logical block where a logical sector is stored.
*     The position of the logical sector in the logical block is returned in pBRSI.
*/
static unsigned _LogSectorIndex2LogBlockIndex(NOR_BM_INST * pInst, U32 LogSectorIndex, unsigned * pBRSI) {
  unsigned lbi;
  U32      brsi;

  lbi    = FS__DivModU32(LogSectorIndex, pInst->LSectorsPerPSector, &brsi);
  *pBRSI = brsi;
  return lbi;
}

/*********************************************************************
*
*       _PreErasePhySector
*
*   Function description:
*     Pre-erasing means writing an value into the data status which indicates that
*     the data is invalid and the physical sector needs to be erased.
*/
static int _PreErasePhySector(NOR_BM_INST * pInst, unsigned PhySectorIndex) {
  PHY_SECTOR_HEADER psh;
  int r;

  _ReadPSH(pInst, PhySectorIndex, &psh);
  _SetPhySectorDataStat(&psh, DATA_STAT_INVALID);
  r = _WritePSH(pInst, PhySectorIndex, &psh);
  return r;
}

/*********************************************************************
*
*       _MarkPhySectorAsFree
*
*   Function description
*     Marks physical sector as free in management data.
*/
static void _MarkPhySectorAsFree(NOR_BM_INST * pInst, unsigned PhySectorIndex) {
  U8   Mask;
  U8 * pData;

  if (PhySectorIndex >= pInst->NumPhySectors) {
    return;
  }
  Mask  = 1 << (PhySectorIndex & 7);
  pData = pInst->pFreeMap + (PhySectorIndex >> 3);
#if FS_NOR_ENABLE_STATS
  if ((*pData & Mask) == 0) {
    pInst->StatCounters.NumFreeBlocks++;
  }
#endif
  *pData |= Mask;    // Mark physical sector as free

}

/*********************************************************************
*
*       _MarkPhySectorAsAllocated
*
*   Function description
*     Mark a physical sector as "in use" in management data.
*/
static void _MarkPhySectorAsAllocated(NOR_BM_INST * pInst, unsigned PhySectorIndex) {
  U8   Mask;
  U8 * pData;

  Mask  = 1 << (PhySectorIndex & 7);
  pData = pInst->pFreeMap + (PhySectorIndex >> 3);
#if FS_NOR_ENABLE_STATS
  if (*pData & Mask) {
    pInst->StatCounters.NumFreeBlocks--;
  }
#endif
  *pData &= ~(unsigned)Mask;    // Mark physical sector as allocated
}

/*********************************************************************
*
*       _PhySectorIsFree
*
*   Function description
*     Checks if physical sector is free to use.
*/
static int _PhySectorIsFree(NOR_BM_INST * pInst, unsigned PhySectorIndex) {
  U8   Mask;
  U8 * pData;
  int  IsFree;

  Mask   = 1 << (PhySectorIndex & 7);
  pData  = pInst->pFreeMap + (PhySectorIndex >> 3);
  IsFree = *pData & Mask;
  return IsFree;
}

/*********************************************************************
*
*       _L2P_Read
*
*  Function Description
*    Returns the contents of the given entry in the L2P table (physical sector lookup table)
*
*/
static unsigned _L2P_Read(NOR_BM_INST * pInst, U32 LogIndex) {
  U32 v;

  v = FS_BITFIELD_ReadEntry(pInst->pLog2PhyTable, LogIndex, pInst->NumBitsPhySectorIndex);
  return v;
}

/*********************************************************************
*
*       _L2P_Write
*
*  Function Description
*    Updates the contents of the given entry in the L2P table (physical sector lookup table)
*
*/
static void _L2P_Write(NOR_BM_INST * pInst, U32 LogIndex, unsigned v) {
  FS_BITFIELD_WriteEntry(pInst->pLog2PhyTable, LogIndex, pInst->NumBitsPhySectorIndex, v);
}

/*********************************************************************
*
*       _L2P_GetSize
*
*  Function Description
*    Computes & returns the size of the L2P assignment table of a work block.
*    Is used before allocation to find out how many bytes need to be allocated.
*/
static unsigned _L2P_GetSize(NOR_BM_INST * pInst) {
  unsigned v;

  v = FS_BITFIELD_CalcSize(pInst->NumLogBlocks, pInst->NumBitsPhySectorIndex);
  return v;
}

/*********************************************************************
*
*       _WB_SectorIsWritten
*
*  Function Description
*    Returns if a sector in a work block is used (written) by looking at the 1 bit paIsWritten-array.
*    A set bit indicates that the sector in question has been written.
*
*  Return value
*    0      Sector not written
*  !=0      Sector written
*/
static char _WB_SectorIsWritten(WORK_BLOCK_DESC * pWorkBlock, unsigned brsi) {
  unsigned Data;

  Data =   *(pWorkBlock->paIsWritten + (brsi >> 3));
  Data >>= brsi & 7;
  return Data & 1;
}

/*********************************************************************
*
*       _WB_MarkSectorAsUsed
*
*   Function description
*     Mark sector as used in work block
*/
static void _WB_MarkSectorAsUsed(WORK_BLOCK_DESC * pWorkBlock, unsigned brsi) {
  U8 Mask;
  unsigned Off;

  Off  = brsi >> 3;
  Mask = 1 << (brsi & 7);
  *(pWorkBlock->paIsWritten + Off) |= Mask;
}

/*********************************************************************
*
*       _WB_ReadAssignment
*
*   Function description
*     Reads an entry in the assignment table of a work block.
*     It is necessary to use a subroutine to do the job since the entries are stored in a bitfield.
*     Logically, the code does the following:
*       return pWorkBlock->aAssign[Index];
*/
static unsigned _WB_ReadAssignment(NOR_BM_INST * pInst, WORK_BLOCK_DESC * pWorkBlock, unsigned Index) {
  unsigned r;

  r = FS_BITFIELD_ReadEntry((const U8 *)pWorkBlock->paAssign, Index, pInst->NumBitsSRSI);
  return r;
}

/*********************************************************************
*
*       _WB_WriteAssignment
*
*   Function description
*     Writes an entry in the assignment table of a work block.
*     It is necessary to use a subroutine to do the job since the entries are stored in a bitfield.
*     Logically, the code does the following:
*       pWorkBlock->aAssign[Index] = v;
*/
static void _WB_WriteAssignment(NOR_BM_INST * pInst, WORK_BLOCK_DESC * pWorkBlock, unsigned Index, unsigned v) {
  FS_BITFIELD_WriteEntry((U8 *)pWorkBlock->paAssign, Index, pInst->NumBitsSRSI, v);
}

/*********************************************************************
*
*       _WB_GetAssignmentSize
*
*   Function description
*     Returns the size of the assignment table of a work block.
*/
static unsigned _WB_GetAssignmentSize(NOR_BM_INST * pInst) {
  unsigned v;

  v = FS_BITFIELD_CalcSize(1 << pInst->NumBitsSRSI, pInst->NumBitsSRSI);
  return v;
}

/*********************************************************************
*
*       _FindFreeSectorInWorkBlock
*
*   Function Description
*     Locate a free sector in a work block.
*     If available we try to locate the brsi at the "native" position, meaning physical brsi = logical brsi,
*     because this leaves the option to later convert the work block into a data block without copying the data.
*
*   Returns
*     != BRSI_INVALID  brsi    If free sector has been found
*     == BRSI_INVALID          No free sector
*/
static unsigned _FindFreeSectorInWorkBlock(NOR_BM_INST * pInst, WORK_BLOCK_DESC * pWorkBlock, unsigned brsi) {
  unsigned NumSectors;
  int      i;

  //
  // Preferred position is the real position within the block. So we first check if it is available.
  //
  if (_WB_SectorIsWritten(pWorkBlock, brsi) == 0) {
    return brsi;
  }
  //
  // Preferred position is taken. Let's use first free position.
  //
  NumSectors = pInst->LSectorsPerPSector;
  i = _Find0BitInArray(pWorkBlock->paIsWritten, 1, NumSectors - 1);   // Returns bit position (1 ... NumSectors - 1) if a 0-bit has been found, else -1
  if (i > 0) {
    return i;
  }
  return BRSI_INVALID;     // No free logical sector in this block
}

/*********************************************************************
*
*       _WB_RemoveFromList
*
*  Function Description
*    Removes a given work block from list of work blocks.
*/
static void _WB_RemoveFromList(WORK_BLOCK_DESC * pWorkBlock, WORK_BLOCK_DESC ** ppFirst) {
  //
  // Unlink Front: From head or previous block
  //
  if (pWorkBlock == *ppFirst) {           // This WB first in list ?
    *ppFirst = pWorkBlock->pNext;
  } else {
    pWorkBlock->pPrev->pNext = pWorkBlock->pNext;
  }
  //
  // Unlink next if pNext is valid
  //
  if (pWorkBlock->pNext) {
    pWorkBlock->pNext->pPrev = pWorkBlock->pPrev;
  }
}

/*********************************************************************
*
*       _WB_AddToList
*
*  Function Description
*    Adds a given work block to the beginning of the list of work block descriptors.
*/
static void _WB_AddToList(WORK_BLOCK_DESC * pWorkBlock, WORK_BLOCK_DESC ** ppFirst) {
  WORK_BLOCK_DESC * pPrevFirst;

  pPrevFirst = *ppFirst;
  pWorkBlock->pPrev = NULL;    // First entry
  pWorkBlock->pNext = pPrevFirst;
  if (pPrevFirst) {
    pPrevFirst->pPrev = pWorkBlock;
  }
  *ppFirst = pWorkBlock;
}

/*********************************************************************
*
*       _WB_RemoveFromUsedList
*
*  Function Description
*    Removes a given work block from list of used work blocks.
*/
static void _WB_RemoveFromUsedList(NOR_BM_INST * pInst, WORK_BLOCK_DESC * pWorkBlock) {
  _WB_RemoveFromList(pWorkBlock, &pInst->pFirstWorkBlockInUse);
}

/*********************************************************************
*
*       _WB_AddToUsedList
*
*  Function Description
*    Adds a given work block to the list of used work blocks.
*/
static void _WB_AddToUsedList(NOR_BM_INST * pInst, WORK_BLOCK_DESC * pWorkBlock) {
  _WB_AddToList(pWorkBlock, &pInst->pFirstWorkBlockInUse);
}

/*********************************************************************
*
*       _WB_RemoveFromFreeList
*
*  Function Description
*    Removes a given work block from list of free work blocks.
*/
static void _WB_RemoveFromFreeList(NOR_BM_INST * pInst, WORK_BLOCK_DESC * pWorkBlock) {
  _WB_RemoveFromList(pWorkBlock, &pInst->pFirstWorkBlockFree);
}

/*********************************************************************
*
*       _WB_AddToFreeList
*
*  Function Description
*    Adds a given work block to the list of free work blocks.
*/
static void _WB_AddToFreeList(NOR_BM_INST * pInst, WORK_BLOCK_DESC * pWorkBlock) {
  _WB_AddToList(pWorkBlock, &pInst->pFirstWorkBlockFree);
}

/*********************************************************************
*
*       _brsi2srsi
*
*   Function description
*     Converts a block relative sector index into a sector relative sector index.
*     Typ. called to find a free logical sector in a work block where we can write to.
*
*   Return value
*     brsi            If free sector has been found
*     BRSI_INVALID    No free sector on the work block
*/
static unsigned _brsi2srsi(NOR_BM_INST * pInst, WORK_BLOCK_DESC * pWorkBlock, unsigned brsi) {
  unsigned srsi;
  unsigned DataStat;

  //
  // In case of logical sector index <> 0 we need to check the physical sector index.
  // The physical sector index of such a logical sector index will never be zero,
  // since we do not assign such a value to a logical sector index.
  // (see function _FindFreeSectorInWorkBlock)
  //
  if (brsi) {
    srsi = _WB_ReadAssignment(pInst, pWorkBlock, brsi);
    if (srsi == 0) {
      return BRSI_INVALID;
    }
    return srsi;
  }
  //
  // brsi == 0 (First sector in work block) requires special handling.
  //
  if (_WB_SectorIsWritten(pWorkBlock, 0) == 0) {
    return BRSI_INVALID;
  }
  srsi = _WB_ReadAssignment(pInst, pWorkBlock, 0);
  //
  // srsi == 0 has 2 different meanings:
  //    1. Logical sector 0 is stored on sector 0 of the physical sector
  //    2. Logical sector 0 has been invalidated
  // We have to differentiate between these and return the correct value.
  //
  if (srsi == 0) {
    unsigned          psiWork;
    LOG_SECTOR_HEADER lsh;

    srsi    = BRSI_INVALID;
    psiWork = pWorkBlock->psi;
    _ReadLSH(pInst, psiWork, 0, &lsh);
    DataStat = _GetLogSectorDataStat(&lsh);
    if (DataStat == DATA_STAT_VALID) {
      srsi = 0;
    }
  }
  return srsi;
}

/*********************************************************************
*
*       _AllocWorkBlockDesc
*
*  Function description
*    Allocates a WORK_BLOCK_DESC from the array in the pInst structure.
*/
static WORK_BLOCK_DESC * _AllocWorkBlockDesc(NOR_BM_INST * pInst, unsigned lbi) {
  WORK_BLOCK_DESC * pWorkBlock;

  //
  // Check if a free work block is available.
  //
  pWorkBlock = pInst->pFirstWorkBlockFree;
  if (pWorkBlock) {
    unsigned NumBytesAssignment;
    unsigned NumBytesIsWritten;
    //
    // Initialize work block descriptor,
    // mark it as in use and add it to the list
    //
    NumBytesAssignment = _WB_GetAssignmentSize(pInst);
    NumBytesIsWritten  = pInst->NumBytesIsWritten;
    _WB_RemoveFromFreeList(pInst, pWorkBlock);
    _WB_AddToUsedList(pInst, pWorkBlock);
    pWorkBlock->lbi    = lbi;
    FS_MEMSET(pWorkBlock->paIsWritten, 0, NumBytesIsWritten);   // Mark all entries as unused: Work block does not yet contain any sectors (data)
    FS_MEMSET(pWorkBlock->paAssign,    0, NumBytesAssignment);  // Make sure that no old assignment info from previous descriptor is in the table
  }
  return pWorkBlock;
}

/*********************************************************************
*
*       _MarkPhySector
*
*   Function Description
*     Marks a physical sector as a logical block of given type
*
*   Parameters
*     pInst           [IN]  Driver instance
*                     [OUT] ---
*     PhySectorIndex  Index of physical sector to write
*     lbi             Index of logical block assigned to physical sector
*     EraseCnt        Number of times the physical sector has been erased
*     DataStat        Status of the data in the physical sector
*     DataCnt         Number of times the data of the physical sector has been copied.
*
*   Return values
*     ==0   OK
*     !=0   A write error occurred
*/
static int _MarkPhySector(NOR_BM_INST * pInst, unsigned PhySectorIndex, unsigned lbi, U32 EraseCnt, unsigned DataStat, unsigned DataCnt) {
  PHY_SECTOR_HEADER psh;
  int               r;

  _ReadPSH(pInst, PhySectorIndex, &psh);
  _SetPhySectorDataStat(&psh, DataStat);
  psh.DataCnt  = DataCnt;
  psh.lbi      = lbi;
  psh.EraseCnt = EraseCnt;
  r = _WritePSH(pInst, PhySectorIndex, &psh);
  return r;
}

/*********************************************************************
*
*       _MarkAsWorkBlock
*
*   Function Description
*     Marks a physical sector as work block.
*
*   Parameters
*     pInst           [IN]  Driver instance
*                     [OUT] ---
*     PhySectorIndex  Index of physical sector to write
*     lbi             Index of logical block assigned to physical sector
*     EraseCnt        Number of times the physical sector has been erased
*
*   Return values
*     ==0   OK
*     !=0   A write error occurred (recoverable)
*/
static int _MarkAsWorkBlock(NOR_BM_INST * pInst, unsigned PhySectorIndex, unsigned lbi, U32 EraseCnt) {
  return _MarkPhySector(pInst, PhySectorIndex, lbi, EraseCnt, DATA_STAT_WORK, 0xFF);
}

/*********************************************************************
*
*       _MarkAsDataBlock
*
*   Function Description
*     Marks a physical sector as data block.
*
*   Parameters
*     pInst       [IN]  Driver instance
*                 [OUT] ---
*     psi         Index of physical sector to write
*     lbi         Index of logical block assigned to physical sector
*     EraseCnt    Number of times the physical sector has been erased
*     DataCnt     Number of times the data block was copied. Used to determine which of two data blocks with the same LBI is the most recent one (see _SectorDataIsMoreRecent).
*
*   Return values
*     ==0   OK
*     !=0   A write error occurred (recoverable)
*/
static int _MarkAsDataBlock(NOR_BM_INST * pInst, unsigned PhySectorIndex, unsigned lbi, U32 EraseCnt, unsigned DataCnt) {
  return _MarkPhySector(pInst, PhySectorIndex, lbi, EraseCnt, DATA_STAT_VALID, DataCnt);
}

/*********************************************************************
*
*       _OnFatalError
*
*   Function description:
*     Called when a fatal error occurrs. It switches to read-only mode
*     and sets the error flag.
*
*   Parameters
*     pInst       [IN]  Driver instance
*                 [OUT] ---
*     ErrorType   Identifies the error
*     ErrorPSI    Index of the physical sector where the error occurred
*/
static void _OnFatalError(NOR_BM_INST * pInst, int ErrorType, unsigned ErrorPSI) {
  if (pInst->HasFatalError == 0) {
    pInst->HasFatalError = 1;
    pInst->ErrorType     = (U8)ErrorType;
    pInst->ErrorPSI      = ErrorPSI;
    FS_DEBUG_ERROROUT((FS_MTYPE_DRIVER, "NOR_BM: Fatal error %d occurred on sector %u. Switched to read-only mode.", ErrorType, ErrorPSI));
  }
}

/*********************************************************************
*
*       _MakePhySectorAvailable
*
*   Function description
*     Marks the data of a physical sector as invalid. The physical sector is put in the in the list of free sectors.
*
*   Parameters
*     pInst           [IN]  Driver instance
*                     [OUT] ---
*     PhySectorIndex  Index of the physical sector to be marked as free
*     EraseCnt        Number of times the sector has been erased. This information is used to update the NumBlocksEraseCntMin
*
*   Return value
*     ==0   OK
*     !=0   A write error occurred, recoverable
*/
static int _MakePhySectorAvailable(NOR_BM_INST * pInst, unsigned PhySectorIndex, U32 EraseCnt) {
  int               r;
  PHY_SECTOR_HEADER psh;

  r = 0;
  //
  // Physical sector 0 stores only management information and can never be freed
  //
  if (PhySectorIndex) {
    //
    // Mark physical sector as invalid and put it to the free list.
    //
    _ReadPSH(pInst, PhySectorIndex, &psh);
    _SetPhySectorDataStat(&psh, DATA_STAT_INVALID);
    _WritePSH(pInst, PhySectorIndex, &psh);
    _MarkPhySectorAsFree(pInst, PhySectorIndex);
    if (pInst->NumBlocksEraseCntMin && (pInst->EraseCntMin == EraseCnt)) {
      pInst->NumBlocksEraseCntMin--;
    }
  }
  return r;
}

/*********************************************************************
*
*       _CopyLogSector
*
*   Function description
*     Copies the data of a logical sector into another logical sector.
*
*   Parameters
*     pInst     [IN]  Driver instance
*               [OUT] ---
*     psiSrc    Source physical sector index
*     srsiSrc   Source sector relative sector index
*     psiDest   Destination physical sector index
*     srsiDest  Destination sector relative sector index
*     brsi      Block relative index of the copied sector
*
*   Return value
*     ==0   OK
*     !=0   An error occurred
*/
static int _CopyLogSector(NOR_BM_INST * pInst, unsigned psiSrc, unsigned srsiSrc, unsigned psiDest, unsigned srsiDest, unsigned brsi) {
  int r;
  unsigned DataStat;
  LOG_SECTOR_HEADER lsh;

  _ReadLSH(pInst, psiSrc, srsiSrc, &lsh);
  DataStat = _GetLogSectorDataStat(&lsh);
  if (DataStat != DATA_STAT_VALID) {
    return 0;                     // Nothing to copy. The source sector is empty or has been invalidated.
  }
  //
  // Copy the sector data
  //
  r = _CopyLogSectorData(pInst, psiSrc, srsiSrc, psiDest, srsiDest);
  if (r) {
    return 1;                     // Error
  }
  //
  // The destination sector contains now valid data
  //
  _ReadLSH(pInst, psiDest, srsiDest, &lsh);
  _SetLogSectorDataStat(&lsh, DATA_STAT_VALID);
  //
  // Important when we clean a work block "in place".
  // In case of a power fail the sector is marked as used.
  //
  if (brsi != BRSI_INVALID) {
    lsh.brsi = brsi;
  }
  r = _WriteLSH(pInst, psiDest, srsiDest, &lsh);
  if (r) {
    return 1;                     // Error
  }
  IF_STATS(pInst->StatCounters.CopySectorCnt++);
  return 0;
}

/*********************************************************************
*
*       _CountDataBlocksWithEraseCntMin
*
*   Function description
*     Goes through all blocks and counts the data blocks with the
*     lowest erase cnt.
*
*   Parameters
*     pInst       [IN]  Driver instance
*                 [OUT] ---
*     pEraseCnt   [IN]  ---
*                 [OUT] Minimum erase count
*     pPSI        [IN]  ---
*                 [OUT] Index of the first physical sector with the min erase count
*
*   Return value
*     Number of data sectors found with a min erase count
*/
static U32 _CountDataBlocksWithEraseCntMin(NOR_BM_INST * pInst, U32 * pEraseCnt, unsigned * pPSI) {
  unsigned iSector;
  unsigned psi;
  U32      EraseCntMin;
  U32      EraseCnt;
  U32      NumPhySectors;

  psi           = 0;
  EraseCntMin   = ERASE_CNT_INVALID;
  NumPhySectors = 0;
  //
  // Read and compare the erase count of all data blocks exept the first one
  // which stores only management information
  //
  for (iSector = 1; iSector < pInst->NumPhySectors; ++iSector) {
    U8                DataStat;
    PHY_SECTOR_HEADER psh;

    _ReadPSH(pInst, iSector, &psh);
    DataStat = _GetPhySectorDataStat(&psh);
    //
    // Only physical sectors that store valid data are checked
    //
    if (DataStat == DATA_STAT_VALID) {
      EraseCnt = psh.EraseCnt;
      if ((EraseCntMin == ERASE_CNT_INVALID) || (EraseCnt < EraseCntMin)) {
        psi           = iSector;
        EraseCntMin   = EraseCnt;
        NumPhySectors = 1;
      } else if (EraseCnt == EraseCntMin) {
        ++NumPhySectors;
      }
    }
  }
  *pEraseCnt = EraseCntMin;
  *pPSI      = psi;
  return NumPhySectors;
}

/*********************************************************************
*
*       _FindDataBlockByEraseCnt
*
*   Function description
*     Goes through all data blocks and returns the first one with
*     the given erase count.
*
*   Parameters
*     pInst     [IN]  Driver instance
*               [OUT] ---
*     EraseCnt  Erase count to lookup for
*
*   Return value
*     == 0  No data block found
*     != 0  Physical sector index of the found data block
*/
static unsigned _FindDataBlockByEraseCnt(NOR_BM_INST * pInst, U32 EraseCnt) {
  unsigned iSector;

  //
  // Read and compare the erase count of all data blocks except the first one
  // which stores only management information
  //
  for (iSector = 1; iSector < pInst->NumPhySectors; ++iSector) {
    PHY_SECTOR_HEADER psh;
    U8  DataStat;
    U32 EraseCntData;

    _ReadPSH(pInst, iSector, &psh);
    DataStat = _GetPhySectorDataStat(&psh);
    //
    // Only physical sectors that store valid data are checked
    //
    if (DataStat == DATA_STAT_VALID) {
      EraseCntData = psh.EraseCnt;
      if (EraseCnt == EraseCntData) {
        return iSector;
      }
    }
  }
  return 0;     // No data sector found with the requested erase count
}

/*********************************************************************
*
*       _CheckActiveWearLeveling
*
*   Function description
*     Checks if it is time to perform active wear leveling by
*     comparing the given erase count to the lowest erase count.
*     If so (difference is too big), the index of the data block with the lowest erase count is returned.
*
*   Parameters
*     pInst           [IN]  Driver instance
*                     [OUT] ---
*     EraseCnt        Erase count of physical sector to be erased
*     pDataEraseCnt   [IN]  ---
*                     [OUT] Erase count of the data block
*
*   Return value
*     == 0  No data block found
*     != 0  Physical sector index of the data block found
*/
static unsigned _CheckActiveWearLeveling(NOR_BM_INST * pInst, U32 EraseCnt, U32 * pDataEraseCnt) {
  unsigned pbi;
  I32      EraseCntDiff;
  U32      NumBlocks;
  U32      EraseCntMin;

  //
  // Update pInst->EraseCntMin if necessary
  //
  pbi         = 0;
  NumBlocks   = pInst->NumBlocksEraseCntMin;
  EraseCntMin = pInst->EraseCntMin;
  if (NumBlocks == 0) {
    NumBlocks = _CountDataBlocksWithEraseCntMin(pInst, &EraseCntMin, &pbi);
    if (NumBlocks == 0) {
      return 0;     // We don't have any data block yet, it can happen if the flash is empty
    }
    pInst->EraseCntMin          = EraseCntMin;
    pInst->NumBlocksEraseCntMin = NumBlocks;
  }
  //
  // Check if the treshold for active wear leveling is reached
  //
  EraseCntDiff = EraseCnt -  EraseCntMin;
  if (EraseCntDiff < (I32)pInst->MaxEraseCntDiff) {
    return 0;       // Active wear leveling not necessary, EraseCntDiff is not big enough yes
  }
  if (pbi == 0) {
    pbi = _FindDataBlockByEraseCnt(pInst, EraseCntMin);
  }
  *pDataEraseCnt = EraseCntMin;
  --pInst->NumBlocksEraseCntMin;
  return pbi;
}

/*********************************************************************
*
*       _PerformPassiveWearLeveling
*
*   Function description
*     Searches for the next free physical sector and returns its index.
*     The sector is marked as allocated in the internal list.
*
*   Parameters
*     pInst       [IN]  Driver instance
*                 [OUT] ---
*     pEraseCnt   [IN]  ---
*                 [OUT] Erase count of the allocated physical sector
*
*   Return value
*     == 0  No more free blocks
*     != 0  Physical sector index of the allocated block
*/
static unsigned _PerformPassiveWearLeveling(NOR_BM_INST * pInst, U32 * pEraseCnt) {
  unsigned i;
  unsigned iSector;
  U32      EraseCnt;

  iSector = pInst->MRUFreeBlock;
  for (i = 0; i < pInst->NumPhySectors; i++) {
    if (++iSector >= pInst->NumPhySectors) {
      iSector = 1;        // Physical sector 0 contains only management information, so we skip it
    }
    if (_PhySectorIsFree(pInst, iSector)) {
      PHY_SECTOR_HEADER psh;

      _ReadPSH(pInst, iSector, &psh);
      EraseCnt = psh.EraseCnt;
      if (EraseCnt == ERASE_CNT_INVALID) {
        EraseCnt = pInst->EraseCntMax;
      }
      *pEraseCnt = EraseCnt;
      _MarkPhySectorAsAllocated(pInst, iSector);
      pInst->MRUFreeBlock = iSector;
      return iSector;
    }
  }
  FS_DEBUG_ERROROUT((FS_MTYPE_DRIVER,  "NOR_BM: No more free physical sectors."));
  return 0;               // Error, no more free physical sectors
}

/*********************************************************************
*
*       _MoveDataBlock
*
*   Function description
*     Copies the contents of a data block into another block.
*     The souce data block is marked as free.
*
*   Parameters
*     pInst       [IN]  Driver instance
*                 [OUT] ---
*     psiSrc      Index of the physical sector to be copied
*     psiDest     Index of the physical sector where to copy
*     EraseCnt    Erase count of the source physical sector
*
*   Return value
*     ==0     OK
*     !=0     An error occurred
*/
static int _MoveDataBlock(NOR_BM_INST * pInst, unsigned psiSrc, unsigned psiDest, U32 EraseCnt) {
  unsigned NumSectors;
  unsigned iSector;
  int      r;
  unsigned lbi;
  unsigned psi;
  unsigned DataCnt;
  PHY_SECTOR_HEADER psh;

  DataCnt    = 0;
  NumSectors = pInst->LSectorsPerPSector;
  for (iSector = 0; iSector < NumSectors; ++iSector) {
    r = _CopyLogSector(pInst, psiSrc, iSector, psiDest, iSector, BRSI_INVALID);
    if (r) {
      return 1;
    }
  }
  //
  // Get the logical block index of the copied sector.
  // We do this by searching table that translates a logical block to physical sector.
  //
  for (lbi = 0; lbi < pInst->NumLogBlocks; ++lbi) {
    psi = FS_BITFIELD_ReadEntry(pInst->pLog2PhyTable, lbi, pInst->NumBitsPhySectorIndex);
    if (psi == psiSrc) {
      break;
    }
  }
  _ReadPSH(pInst, psiSrc, &psh);
  DataCnt = psh.DataCnt;
  ++DataCnt;
  r = _MarkAsDataBlock(pInst, psiDest, lbi, EraseCnt, DataCnt);
  if (r) {
    return 1;           // Error
  }
  //
  // Update the mapping of physical sectors to logical blocks
  //
  _L2P_Write(pInst, lbi, psiDest);

  //
  // Fail-safe TP. At this point we have two data blocks with the same LBI
  //

  return _MakePhySectorAvailable(pInst, psiSrc, EraseCnt);
}

/*********************************************************************
*
*       _AllocErasedBlock
*
*   Function description
*     Selects a block to write data into. The physical sector is erased.
*
*   Parameters
*     pInst       [IN]  Driver instance
*                 [OUT] ---
*     pEraseCnt   [IN]  ---
*                 [OUT] Erase count of the selected physical sector
*
*   Return value
*     ==0   An error occurred
*     !=0   Physical sector index
*/
static unsigned _AllocErasedBlock(NOR_BM_INST * pInst, U32 * pEraseCnt) {
  unsigned psiAlloc;
  unsigned psiData;
  U32      EraseCntAlloc;
  U32      EraseCntData;
  int      r;

  //
  // Passive wear leveling. Get the next free physical sector in the row
  //
  psiAlloc = _PerformPassiveWearLeveling(pInst, &EraseCntAlloc);
  if (psiAlloc == 0) {
    _OnFatalError(pInst, NOR_OUT_OF_FREE_SECTORS, 0);
    return 0;                   // Fatal error, out of free physical sectors
  }
  r = _ErasePhySector(pInst, psiAlloc);
  if (r) {
    _OnFatalError(pInst, NOR_ERASE_ERROR, psiAlloc);
    return 0;                   // Fatal error, erasing of physical sector failed
  }
  //
  // OK, we found a free physical sector.
  // Now, let's check if the erase count is too high so we need to use active wear leveling
  //
  psiData = _CheckActiveWearLeveling(pInst, EraseCntAlloc, &EraseCntData);
  ++EraseCntAlloc;
  if (psiData == 0) {
    *pEraseCnt = EraseCntAlloc; // No data physical sector has an erase count low enough, keep the physical sector allocated by passive wear leveling
    return psiAlloc;
  }
  //
  // Perform active wear leveling:
  // A block containing data has a much lower erase count. This block is now moved, giving us a free block with low erase count.
  // This procedure makes sure that blocks which contain data that does not change still take part in the the
  // wear leveling scheme.
  //
  r = _MoveDataBlock(pInst, psiData, psiAlloc, EraseCntAlloc);
  if (r) {
    _OnFatalError(pInst, NOR_WRITE_ERROR, psiAlloc);
    return 0;                 // Fatal error, could not relocate data block
  }
  //
  // The data has been moved and the data block is now free to use
  //
  _MarkPhySectorAsAllocated(pInst, psiData);
  r = _ErasePhySector(pInst, psiData);
  if (r) {
    _OnFatalError(pInst, NOR_ERASE_ERROR, psiAlloc);
    return 0;                 // Fatal error, erasing of physical sector failed
  }
  ++EraseCntData;
  *pEraseCnt = EraseCntData;
  return psiData;
}

/*********************************************************************
*
*       _ConvertWorkBlockViaCopy
*
*   Function description
*     Converts a work block into a data block. The data of the work block
*     is "merged" with the data of the source block in another free block.
*     The merging operation copies sector-wise the data from work block
*     into the free block. If the sector data is invalid in the work block
*     the sector data from the source block is copied instead. The sectors
*     in the work block doesn't have to be on their native positions.
*
*   Parameters
*     pInst       [IN]  Driver instance
*                 [OUT] ---
*     pWorkBlock  [IN]  Work block to convert
*                 [OUT] ---
*
*   Return value
*     ==0   OK
*     !=0   An error occurred
*/
static int _ConvertWorkBlockViaCopy(NOR_BM_INST * pInst, WORK_BLOCK_DESC * pWorkBlock) {
  unsigned iSector;
  unsigned psiSrc;
  unsigned psiWork;
  unsigned psiDest;
  unsigned NumSectors;
  unsigned srsi;
  U8       DataCnt;
  U32      EraseCntDest;
  U32      EraseCntSrc;
  int      r;
  PHY_SECTOR_HEADER pshSrc;


  DataCnt      = 0;
  EraseCntSrc  = ERASE_CNT_INVALID;
  EraseCntDest = ERASE_CNT_INVALID;
  psiWork      = pWorkBlock->psi;
  NumSectors   = pInst->LSectorsPerPSector;
  psiDest      = 0;       // No destination sector, yet
  //
  // We need to allocate a new block to copy data into
  //
  psiDest = _AllocErasedBlock(pInst, &EraseCntDest);
  if (psiDest == 0) {
    return 1;             // Error, no more free physical sectors
  }
  //
  // OK, we have an empty block to copy our data into
  //
  psiSrc  = _L2P_Read(pInst, pWorkBlock->lbi);
  //
  // Copy the data sector by sector
  //
  for (iSector = 0; iSector < NumSectors; iSector++) {
    //
    // If data is not in work block, take it from source block
    //
    srsi = _brsi2srsi(pInst, pWorkBlock, iSector);
    if (srsi != BRSI_INVALID) { // In work block ?
      r = _CopyLogSector(pInst, psiWork, srsi, psiDest, iSector, BRSI_INVALID);
      if (r) {
        _OnFatalError(pInst, NOR_WRITE_ERROR, psiDest);
        return 1;               // Error while copying the data
      }
    } else if (psiSrc) {        // In source block ?
      //
      // Copy if we have a data source.
      // Note that when closing a work block which did not yet have a source data block,
      // it can happen that some sector have no source and stay empty.
      //
      r = _CopyLogSector(pInst, psiSrc, iSector, psiDest, iSector, BRSI_INVALID);
      if (r) {
        _OnFatalError(pInst, NOR_WRITE_ERROR, psiDest);
        return 1;               // Error while copying the data
      }
    }
  }
  if (psiSrc) {
    _ReadPSH(pInst, psiSrc, &pshSrc);
    DataCnt     = pshSrc.DataCnt;
    EraseCntSrc = pshSrc.EraseCnt;
    DataCnt++;
  }
  //
  // Mark the newly allocated block as data block
  //
  _MarkAsDataBlock(pInst, psiDest, pWorkBlock->lbi, EraseCntDest, DataCnt);

  //
  // Fail-safe TP. At this point we have two data blocks with the same LBI
  //

  //
  // Update the mapping of physical sectors to logical blocks
  //
  _L2P_Write(pInst, pWorkBlock->lbi, psiDest);
  //
  // Mark former work block as invalid and put it to the free list if there was no error in the ECC.
  //
  _MakePhySectorAvailable(pInst, psiWork, ERASE_CNT_INVALID);
  //
  // Change data status of block which contained the "old" data
  // as invalid and put it to the free list if there was no error in ECC
  //
  _MakePhySectorAvailable(pInst, psiSrc, EraseCntSrc);
  //
  // Remove the work block from the internal list
  //
  _WB_RemoveFromUsedList(pInst, pWorkBlock);
  _WB_AddToFreeList(pInst, pWorkBlock);
  //
  // If required, update the information used for active wear leveling
  //
  {
    U32 EraseCntMin;
    U32 NumBlocksEraseCntMin;

    EraseCntMin          = pInst->EraseCntMin;
    NumBlocksEraseCntMin = pInst->NumBlocksEraseCntMin;
    if (EraseCntDest < EraseCntMin) {
      EraseCntMin          = EraseCntDest;
      NumBlocksEraseCntMin = 1;
    } else if (EraseCntDest == EraseCntMin) {
      ++NumBlocksEraseCntMin;
    }
    pInst->EraseCntMin          = EraseCntMin;
    pInst->NumBlocksEraseCntMin = NumBlocksEraseCntMin;
  }
  IF_STATS(pInst->StatCounters.ConvertViaCopyCnt++);
  return 0;
}

/*********************************************************************
*
*       _ConvertWorkBlockInPlace
*
*   Function description
*     Converts a work block into a data block. It assumes that
*     the sectors are on their native positions. The missing sectors
*     are copied from the source block into the work block.
*
*   Parameters
*     pInst       [IN]  Driver instance
*                 [OUT] ---
*     pWorkBlock  [IN]  Work block to convert
*                 [OUT] ---
*
*   Return value
*     ==0   OK
*     !=0   An error occurred
*/
static int _ConvertWorkBlockInPlace(NOR_BM_INST * pInst, WORK_BLOCK_DESC * pWorkBlock) {
  unsigned iSector;
  unsigned psiSrc;
  unsigned psiWork;
  unsigned DataCnt;
  unsigned NumSectors;
  int      r;
  unsigned brsi;
  U32      EraseCnt;
  PHY_SECTOR_HEADER psh;

  brsi       = BRSI_INVALID;
  DataCnt    = 0;
  EraseCnt   = ERASE_CNT_INVALID;
  psiSrc     = _L2P_Read(pInst, pWorkBlock->lbi);
  psiWork    = pWorkBlock->psi;
  NumSectors = pInst->LSectorsPerPSector;
  //
  // If there is a source block, then use it to "fill the gaps", reading sectors which are empty in the work block.
  //
  if (psiSrc) {
    for (iSector = 0; iSector < NumSectors; iSector++) {
      if (_WB_SectorIsWritten(pWorkBlock, iSector) == 0) {
        if (iSector) {
          brsi = iSector;
        }
        r = _CopyLogSector(pInst, psiSrc, iSector, psiWork, iSector, brsi);
        if (r) {
          _OnFatalError(pInst, NOR_WRITE_ERROR, psiWork);
          return 1;                     // Error, could not copy logical sector
        }
      }
    }
    _ReadPSH(pInst, psiSrc, &psh);
    DataCnt  = psh.DataCnt;
    EraseCnt = psh.EraseCnt;            // We need this erase count for the management of wear leveling
    DataCnt++;
  }
  //
  // Convert work block into valid data block by changing the data status
  //
  _ReadPSH(pInst, psiWork, &psh);
  psh.DataCnt  = DataCnt;
  _SetPhySectorDataStat(&psh, DATA_STAT_VALID);
  _WritePSH(pInst, psiWork, &psh);

  //
  // Fail-safe TP. At this point we have two data blocks with the same logical block index
  //

  //
  // Converted work block is now data block, update Log2Phy table here.
  //
  _L2P_Write(pInst, pWorkBlock->lbi, pWorkBlock->psi);
  //
  // Change data status of block which contained the "old" data
  // as invalid and put it to the free list
  //
  _MakePhySectorAvailable(pInst, psiSrc, EraseCnt);
  //
  // Remove the work block from the internal list
  //
  _WB_RemoveFromUsedList(pInst, pWorkBlock);
  _WB_AddToFreeList(pInst, pWorkBlock);
  IF_STATS(pInst->StatCounters.ConvertInPlaceCnt++);
  return 0;       // Success
}

/*********************************************************************
*
*       _WorkBlockCanBeConverted
*
*   Function description
*     Checks if a work block can be converted.
*     This is the case if all written sectors are in the right place.
*
*   Return value
*     ==0       Found sectors with valid data not on their native positions.
*     ==1       All the sectors having valid data found on their native positions.
*/
static int _WorkBlockCanBeConverted(NOR_BM_INST * pInst, WORK_BLOCK_DESC * pWorkBlock) {
#if FS_NOR_CAN_REWRITE
  unsigned u;
  unsigned Pos;
  unsigned NumSectors;

  NumSectors = pInst->LSectorsPerPSector;
  for (u = 0; u < NumSectors; u++) {
    if (_WB_SectorIsWritten(pWorkBlock, u)) {
      Pos = _WB_ReadAssignment(pInst, pWorkBlock, u);
      if (Pos != u) {
        return 0;
      }
    }
  }
  return 1;
#else
  FS_USE_PARA(pInst);
  FS_USE_PARA(pWorkBlock);
  return 0;
#endif
}

/*********************************************************************
*
*       _CleanWorkBlock
*
*   Function description
*     Closes the work buffer.
*     - Convert work block into normal data buffer by copy all data into it and marking it as data block
*     - Invalidate and mark as free the block which contained the same logical data area before
*
*   Parameters
*     pInst       [IN]  Driver instance
*                 [OUT] ---
*     pWorkBlock  [IN]  Work block to be cleaned
*                 [OUT] ---
*
*   Return values
*     ==0   OK
*     !=0   An error occurred
*/
static int _CleanWorkBlock(NOR_BM_INST * pInst, WORK_BLOCK_DESC *pWorkBlock) {
  int r;

  r = _WorkBlockCanBeConverted(pInst, pWorkBlock);
  if (r) {                  // Can work block be converted in-place ?
    r = _ConvertWorkBlockInPlace(pInst, pWorkBlock);
    if (r == 0) {
      return 0;             // Block converted, we are done
    }
    return 1;               // Fatal error, no recovery is possible
  }
  //
  // Work block could not be converted in place, try via copy.
  //
  return _ConvertWorkBlockViaCopy(pInst, pWorkBlock);
}

/*********************************************************************
*
*       _CleanLastWorkBlock
*
*  Function Description
*    Removes the least recently used work block from list of work blocks and converts it into data block
*/
static int _CleanLastWorkBlock(NOR_BM_INST * pInst) {
  WORK_BLOCK_DESC * pWorkBlock;
  //
  // Find last work block in list
  //
  pWorkBlock = pInst->pFirstWorkBlockInUse;
  while (pWorkBlock->pNext) {
    pWorkBlock = pWorkBlock->pNext;
  }
  return _CleanWorkBlock(pInst, pWorkBlock);
}

/*********************************************************************
*
*       _CleanAllWorkBlocks
*
*   Function description
*     Closes all work blocks.
*/
static void _CleanAllWorkBlocks(NOR_BM_INST * pInst) {
  while (pInst->pFirstWorkBlockInUse) {
    _CleanWorkBlock(pInst, pInst->pFirstWorkBlockInUse);
  }
}

/*********************************************************************
*
*       _AllocWorkBlock
*
*   Function Description
*    - Allocates a WORK_BLOCK_DESC management entry from the array in the pInst structure
*    - Finds a free block and assigns it to the WORK_BLOCK_DESC
*    - Writes info such as EraseCnt, lbui and the WORK-BLOCK marker to the spare area of the first sector
*
*   Parameters
*     pInst       [IN]  Driver instance
*                 [OUT] ---
*     lbi         Logical block index assigned to work block
*
*   Return values
*     !=NULL  Pointer to allocated work block
*     ==NULL  An error occurred, typ. a fatal error
*/
static WORK_BLOCK_DESC * _AllocWorkBlock(NOR_BM_INST * pInst, unsigned lbi) {
  WORK_BLOCK_DESC * pWorkBlock;
  U32               EraseCnt;
  unsigned          psi;
  int               r;

  pWorkBlock = _AllocWorkBlockDesc(pInst, lbi);
  if (pWorkBlock == NULL) {
    r = _CleanLastWorkBlock(pInst);
    if (r) {
      return NULL;                  // Error: Happens only in case of a fatal error
    }
    pWorkBlock = _AllocWorkBlockDesc(pInst, lbi);
    if (pWorkBlock == NULL) {
      return NULL;                  // Error. This can happen in case of hardware failure only
    }
  }
  //
  // Get an empty block to write on
  //
  psi = _AllocErasedBlock(pInst, &EraseCnt);
  if (psi == 0) {
    return NULL;                    // Error: Happens only in case of a fatal error
  }
  //
  // New workblock allocated
  //
  pWorkBlock->psi = psi;
  _MarkAsWorkBlock(pInst, psi, lbi, EraseCnt);
  return pWorkBlock;
}

/*********************************************************************
*
*       _FindWorkBlock
*
*  Function description
*    Tries to locate a work block for a given logical block.
*/
static WORK_BLOCK_DESC * _FindWorkBlock(NOR_BM_INST * pInst, unsigned lbi) {
  WORK_BLOCK_DESC * pWorkBlock;

  //
  // Iterate over used-list
  //
  pWorkBlock = pInst->pFirstWorkBlockInUse;
  do {
    if (pWorkBlock == NULL) {
      break;                         // No match
    }
    if (pWorkBlock->lbi == lbi) {
      break;                         // Found it
    }
    pWorkBlock = pWorkBlock->pNext;
  } while (1);
  return pWorkBlock;
}

/*********************************************************************
*
*       _MarkWorkBlockAsMRU
*
*   Function description
*     Marks the given work block as most-recently used.
*     This is important so the least recently used one can be "kicked out" if a new one is needed.
*/
static void _MarkWorkBlockAsMRU(NOR_BM_INST * pInst, WORK_BLOCK_DESC * pWorkBlock) {
  if (pWorkBlock != pInst->pFirstWorkBlockInUse) {
    _WB_RemoveFromUsedList(pInst, pWorkBlock);
    _WB_AddToUsedList(pInst, pWorkBlock);
  }
}

/*********************************************************************
*
*       _LoadWorkBlock
*
*   Function description
*     Reads management data of work block.
*     Used during low-level mount only, since at all other times, the work block descriptors are up to date.
*/
static void _LoadWorkBlock(NOR_BM_INST * pInst, WORK_BLOCK_DESC * pWorkBlock) {
  unsigned NumSectors;
  unsigned iSector;
  unsigned brsi;
  unsigned psiWork;
  unsigned DataStat;

  psiWork    = pWorkBlock->psi;
  NumSectors = pInst->LSectorsPerPSector;
  //
  // Iterate over all logical sectors, reading header info in order to find out if sector contains data and if so which data
  //
  for (iSector = 0; iSector < NumSectors; iSector++) {
    LOG_SECTOR_HEADER lsh;

    _ReadLSH(pInst, psiWork, iSector, &lsh);
    DataStat = _GetLogSectorDataStat(&lsh);
    if (DataStat == DATA_STAT_EMPTY) {
      if (_IsLogSectorBlank(pInst, psiWork, iSector) == 0) {
        //
        // Sector is not blank, even though the data status in the header says so.
        // This situation can arise if a sector write operation has been interrupted by
        // a reset; in this case the header is still marked as blank, but a part of the data
        // area is not blank. We mark the sector as containing invalid data so we do not
        // run into it again. It will be re-used when the physical sector is erased.
        //
        _SetLogSectorDataStat(&lsh, DATA_STAT_INVALID);
        _WriteLSH(pInst, psiWork, iSector, &lsh);
        _WB_MarkSectorAsUsed(pWorkBlock, iSector);
      }
      continue;
    }
    //
    // Found a sector which has been written
    //
    brsi = lsh.brsi;
    _WB_MarkSectorAsUsed(pWorkBlock, iSector);
    if ((brsi < pInst->LSectorsPerPSector) && (DataStat == DATA_STAT_VALID)) {
      _WB_WriteAssignment(pInst, pWorkBlock, brsi, iSector);
    }
  }
}

/*********************************************************************
*
*       _SectorDataIsMoreRecent
*
*   Function description
*     Used during low-level mount only.
*/
static int _SectorDataIsMoreRecent(NOR_BM_INST * pInst, PHY_SECTOR_HEADER * pPSH, unsigned psiPrev) {
  PHY_SECTOR_HEADER pshPrev;
  int Data;

  _ReadPSH(pInst, psiPrev, &pshPrev);
  Data = (pshPrev.DataCnt - pPSH->DataCnt) & 0xFF;
  if (Data == 1) {
    return 1;        // Newer!
  }
  return 0;          // Older!
}

/*********************************************************************
*
*       _GetNumValidSectors
*
*   Function description
*     Counts how many sectors in a block contain valid data.
*
*   Parameters
*     pInst   [IN]  Driver instance
*             [OUT] ---
*     lbi     Logical index of the block to process.
*
*   Return value
*     Number of valid sectors.
*
*   Notes
*       (1) A sector in a work block which contains valid data has also a valid BRSI assigned to it
*/
#if FS_NOR_ENABLE_STATS
static U32 _GetNumValidSectors(NOR_BM_INST * pInst, unsigned lbi) {
  unsigned LSectorsPerPSector;
  U32      NumSectors;
  unsigned psiSrc;
  unsigned srsi;
  unsigned brsi;
  unsigned DataStat;
  WORK_BLOCK_DESC   * pWorkBlock;
  LOG_SECTOR_HEADER   lsh;

  psiSrc             = _L2P_Read(pInst, lbi);
  pWorkBlock         = _FindWorkBlock(pInst, lbi);
  LSectorsPerPSector = pInst->LSectorsPerPSector;
  NumSectors         = 0;
  //
  // 1st case: a data block is assigned to logical block
  //
  if (psiSrc && (pWorkBlock == NULL)) {
    srsi = 0;
    do {
      _ReadLSH(pInst, psiSrc, srsi, &lsh);
      DataStat = _GetLogSectorDataStat(&lsh);
      if (DataStat == DATA_STAT_VALID) {
        ++NumSectors;
      }
      ++srsi;
    } while (--LSectorsPerPSector);
  }
  //
  // 2nd case: a work block is assigned to logical block
  //
  if ((psiSrc == 0) && pWorkBlock) {
    brsi = 0;
    do {
      if (_brsi2srsi(pInst, pWorkBlock, brsi) != BRSI_INVALID) {     // Note 1
        ++NumSectors;
      }
      ++brsi;
    } while (--LSectorsPerPSector);
  }
  //
  // 3rd case: a data block and a work block are assigned to logical block
  //
  if (psiSrc && pWorkBlock) {
    srsi = 0;
    do {
      _ReadLSH(pInst, psiSrc, srsi, &lsh);
      DataStat = _GetLogSectorDataStat(&lsh);
      if (DataStat == DATA_STAT_VALID) {
        ++NumSectors;
      } else if (_brsi2srsi(pInst, pWorkBlock, srsi) != BRSI_INVALID) {   // Note 1
        ++NumSectors;
      }
      ++srsi;
    } while (--LSectorsPerPSector);
  }
  return NumSectors;
}
#endif

/*********************************************************************
*
*       _LowLevelMount
*
*   Function description
*     Reads and analyzes management information from NOR flash.
*     If the information makes sense and allows us to read and write from
*     the medium, it can perform read and write operations.
*
*   Return value
*     ==0   O.K., device has been successfully mounted and is accessible
*     !=0   Error
*
*/
static int _LowLevelMount(NOR_BM_INST * pInst) {
  U32      EraseCntMax;               // Highest erase count on any sector
  U32      EraseCnt;
  U32      EraseCntMin;
  U32      NumBlocksEraseCntMin;
  unsigned DataStat;
  unsigned lbi;
  unsigned iSector;
  unsigned psiPrev;
  U8       acInfo[sizeof(_acInfo)];
  U32      Version;
  U32      BytesPerSectorFromDevice;
  U32      NumLogBlocksFromDevice;
  int      NumLogBlocksToUse;
  U16      NumWorkBlocksFromDevice;
  unsigned NumWorkBlocks;
  unsigned NumWorkBlocksToAllocate;
  unsigned LSectorsPerPSector;
  unsigned LSectorsPerPSectorMax;
  unsigned NumPhySectors;
  U32      PhySectorSize;
  WORK_BLOCK_DESC * pWorkBlock;

  //
  // Check info block first (First physical sector on NOR flash)
  //
  _ReadLogSectorData(pInst, INFO_BLOCK_PSI, 0, acInfo,                    0,                         sizeof(acInfo));
  _ReadLogSectorData(pInst, INFO_BLOCK_PSI, 0, &Version,                  INFO_OFF_LLFORMAT_VERSION, sizeof(Version));
  _ReadLogSectorData(pInst, INFO_BLOCK_PSI, 0, &BytesPerSectorFromDevice, INFO_OFF_BYTES_PER_SECTOR, sizeof(BytesPerSectorFromDevice));
  _ReadLogSectorData(pInst, INFO_BLOCK_PSI, 0, &NumLogBlocksFromDevice,   INFO_OFF_NUM_LOG_BLOCKS,   sizeof(NumLogBlocksFromDevice));
  _ReadLogSectorData(pInst, INFO_BLOCK_PSI, 0, &NumWorkBlocksFromDevice,  INFO_OFF_NUM_WORK_BLOCKS,  sizeof(NumWorkBlocksFromDevice));
  if (FS_MEMCMP(_acInfo, acInfo , sizeof(_acInfo))) {
    FS_DEBUG_ERROROUT((FS_MTYPE_DRIVER, "NOR_BM: Invalid low-level signature."));
    return 1;                   // Error
  }
  if (Version != (U32)LLFORMAT_VERSION) {
    FS_DEBUG_ERROROUT((FS_MTYPE_DRIVER, "NOR_BM: Invalid low-level format version."));
    return 1;                   // Error
  }
  if (BytesPerSectorFromDevice > (U32)FS_Global.MaxSectorSize) {
    FS_DEBUG_ERROROUT((FS_MTYPE_DRIVER, "NOR_BM: Sector size specified in drive is higher than the sector size that can be stored by the FS."));
    return 1;                   // Error
  }
  if (NumWorkBlocksFromDevice >= pInst->NumPhySectors) {
    FS_DEBUG_ERROROUT((FS_MTYPE_DRIVER, "NOR_BM: Invalid number of work blocks."));
    return 1;                   // Error
  }
  PhySectorSize             = pInst->PhySectorSize;
  LSectorsPerPSector        = _CalcLSectorsPerPSector(PhySectorSize, BytesPerSectorFromDevice);
  LSectorsPerPSectorMax     = MAX(LSectorsPerPSector, pInst->LSectorsPerPSector);   // Required for the correct allocation of paIsWritten and paAssign arrays
  pInst->LSectorsPerPSector = LSectorsPerPSector;
  pInst->NumBitsSRSI        = FS_BITFIELD_CalcNumBitsUsed(LSectorsPerPSectorMax);
  pInst->ldBytesPerSector   = _ld(BytesPerSectorFromDevice);
  //
  // Find out how many work blocks are required to be allocated.
  // We take the maximum between the number of work blocks read from device
  // and the number of work blocks configured. The reason is to prevent
  // an overflow in the paWorkBlock array when the application increases
  // the number of work blocks and does a low-level format.
  //
  NumWorkBlocks           = pInst->NumWorkBlocks;
  NumWorkBlocksToAllocate = MAX(NumWorkBlocksFromDevice, NumWorkBlocks);
  NumWorkBlocks           = NumWorkBlocksFromDevice;
  //
  // Check if there are enough logical sectors to store the file system.
  //
  NumPhySectors     = pInst->NumPhySectors;
  NumLogBlocksToUse = _CalcNumBlocksToUse(NumPhySectors, NumWorkBlocks);
  if ((NumLogBlocksToUse <= 0) || (NumLogBlocksFromDevice > (U32)NumLogBlocksToUse)) {
    FS_DEBUG_ERROROUT((FS_MTYPE_DRIVER, "NOR_BM: Number of logical blocks has shrunk. Low-level format required.\n"));
    return 1;
  }
  pInst->NumLogBlocks  = NumLogBlocksToUse;
  pInst->NumWorkBlocks = NumWorkBlocks;
  pInst->NumLogSectors = NumLogBlocksToUse * LSectorsPerPSector;
  //
  // Assign reasonable default for config values
  //
  if (pInst->MaxEraseCntDiff == 0) {
    pInst->MaxEraseCntDiff = FS_NOR_MAX_ERASE_CNT_DIFF;
  }
  //
  // Allocate/Zero memory for tables
  //
  {
    unsigned NumBytesL2PTable;
    unsigned NumBytesFreeMap;

    NumBytesL2PTable = _L2P_GetSize(pInst);
    NumBytesFreeMap  = (pInst->NumPhySectors + 7) / 8;
    FS_AllocZeroedPtr((void **)&pInst->pLog2PhyTable, NumBytesL2PTable);
    FS_AllocZeroedPtr((void **)&pInst->pFreeMap,      NumBytesFreeMap);
  }
  //
  //  Init work block descriptors: Alloc memory and add them to free list
  //
  {
    unsigned NumBytesAssignment;
    unsigned NumBytesIsWritten;
    unsigned NumBytesWorkBlocks;

    NumBytesWorkBlocks = sizeof(WORK_BLOCK_DESC) * NumWorkBlocksToAllocate;
    if (pInst->paWorkBlock == NULL) {
      pInst->paWorkBlock = (WORK_BLOCK_DESC *)FS_AllocZeroed(NumBytesWorkBlocks);
      FS_MEMSET(pInst->paWorkBlock, 0, NumBytesWorkBlocks);
    }
    NumBytesAssignment = _WB_GetAssignmentSize(pInst);
    NumBytesIsWritten  = (LSectorsPerPSectorMax + 7) >> 3;
    pWorkBlock         = pInst->paWorkBlock;
    do {
      FS_AllocZeroedPtr((void **)&pWorkBlock->paIsWritten, NumBytesIsWritten);
      FS_AllocZeroedPtr((void **)&pWorkBlock->paAssign,    NumBytesAssignment);
      //
      // Not all the work block descriptors are available if the number of work blocks
      // specified in the device is smaller than the number of work blocks configured.
      //
      if (NumWorkBlocks) {
        _WB_AddToFreeList(pInst, pWorkBlock);
        NumWorkBlocks--;
      }
      pWorkBlock++;
    } while (--NumWorkBlocksToAllocate);
    pInst->NumBytesIsWritten = NumBytesIsWritten;
  }
  //
  // O.K., we read the phyisical sector headers and fill the tables
  //
  EraseCntMax          = 0;
  EraseCntMin          = ERASE_CNT_INVALID;
  NumBlocksEraseCntMin = 0;
  for (iSector = 1; iSector < pInst->NumPhySectors; iSector++) {
    PHY_SECTOR_HEADER psh;

    _ReadPSH(pInst, iSector, &psh);
    DataStat = _GetPhySectorDataStat(&psh);
    lbi      = psh.lbi;
    EraseCnt = psh.EraseCnt;
    //
    // Is this physical sector containing valid data ?
    //
    if (EraseCnt != ERASE_CNT_INVALID) {
      //
      // Has this physical sector been used as work block ?
      //
      if (DataStat == DATA_STAT_WORK) {
        //
        // If the work block is an invalid one do a pre-erase.
        //
        if (lbi >= pInst->NumLogBlocks) {
          _PreErasePhySector(pInst, iSector);
          _MarkPhySectorAsFree(pInst, iSector);
          continue;
        }
        if (pInst->pFirstWorkBlockFree) {
          //
          // Check if we already have a block with this lbi.
          // If we do, then we erase it and add it to the free list.
          //
          pWorkBlock = _FindWorkBlock(pInst, lbi);
          if (pWorkBlock) {
            FS_DEBUG_WARN((FS_MTYPE_DRIVER, "NOR_BM: Found a work block with the same lbi."));
            _PreErasePhySector(pInst, iSector);
            _MarkPhySectorAsFree(pInst, iSector);
            continue;
          }
          pWorkBlock      = _AllocWorkBlockDesc(pInst, lbi);
          pWorkBlock->psi = iSector;
        } else {
          FS_DEBUG_ERROROUT((FS_MTYPE_DRIVER, "NOR_BM: Found more work blocks than can be handled. Configuration changed ?"));
          _PreErasePhySector(pInst, iSector);
          _MarkPhySectorAsFree(pInst, iSector);
        }
        continue;
      }
      //
      // Is this a block containing valid data ?
      //
      if (DataStat == DATA_STAT_VALID) {
        if (lbi >= pInst->NumLogBlocks) {
          _MarkPhySectorAsFree(pInst, iSector);
          continue;
        }
        psiPrev = _L2P_Read(pInst, lbi);
        if (psiPrev == 0) {                                   // Has this lbi already been assigned ?
          _L2P_Write(pInst, lbi, iSector);                    // Add block to the translation table
          if (EraseCnt > EraseCntMax) {
            EraseCntMax = EraseCnt;
          }
          continue;
        }
        if (_SectorDataIsMoreRecent(pInst, &psh, psiPrev)) {
          _MarkPhySectorAsFree(pInst, iSector);
          _PreErasePhySector(pInst, iSector);
        } else {
          _MarkPhySectorAsFree(pInst, psiPrev);
          _PreErasePhySector(pInst, psiPrev);
          _L2P_Write(pInst, lbi, iSector);                    // Add block to the translation table
        }
        if ((EraseCntMin == ERASE_CNT_INVALID) || (EraseCnt < EraseCntMin)) {   // Collect information for the active wear leveling
          EraseCntMin          = EraseCnt;
          NumBlocksEraseCntMin = 1;
        } else if (EraseCnt == EraseCntMin) {
          ++NumBlocksEraseCntMin;
        }
      }
    }
    //
    // Any other physical sectors are interpreted as free.
    //
    _MarkPhySectorAsFree(pInst, iSector);
  }
  pInst->EraseCntMax          = EraseCntMax;
  pInst->EraseCntMin          = EraseCntMin;
  pInst->NumBlocksEraseCntMin = NumBlocksEraseCntMin;
  //
  // Handle the work blocks we found
  //
  pWorkBlock = pInst->pFirstWorkBlockInUse;
  while (pWorkBlock) {
    _LoadWorkBlock(pInst, pWorkBlock);
    pWorkBlock = pWorkBlock->pNext;
  }
  //
  // On debug builds we count here the number of valid sectors
  //
#if FS_NOR_ENABLE_STATS
  {
    U32      NumSectors;
    unsigned iBlock;

    for (iBlock = 0; iBlock < pInst->NumLogBlocks; ++iBlock) {
      NumSectors = _GetNumValidSectors(pInst, iBlock);
      pInst->StatCounters.NumValidSectors += NumSectors;
    }
  }
#endif
  return 0;          // O.K. !
}

/*********************************************************************
*
*       _LowLevelMountIfRequired
*
*  Function description
*    LL-Mounts the device if it is not LLMounted and this has not already been
*    tried in vain.
*
*  Return value
*    0     O.K., device has been successfully mounted and is accessible
*  !=0     Error
*/
static int _LowLevelMountIfRequired(NOR_BM_INST * pInst) {
  int r;

  if (pInst->IsLLMounted) {
    return 0;                   // O.K., is mounted
  }
  if (pInst->LLMountFailed) {
    return 1;                   // Error, we could not mount it and do not want to try again
  }

  r = _LowLevelMount(pInst);
  if (r == 0) {
    pInst->IsLLMounted = 1;
  } else {
    pInst->LLMountFailed = 1;
  }
  return r;
}

/*********************************************************************
*
*       _ReadOneLogSector
*
*   Function description
*     Reads one logical sectors from storage device.
*     There are 3 possibilities:
*     a) Data is in work block
*     b) There is a physical sector assigned to the logical block -> Read from hardware
*     c) There is a no physical block assigned to this logical block. This means data has never been written to storage. Fill data with 0.
*
*   Return value
*     ==0   Data successfully read.
*     !=0   An error has occurred.
*
*/
static int _ReadOneLogSector(NOR_BM_INST * pInst, unsigned LogSectorIndex, void * pData) {
  int      r;
  unsigned lbi;
  unsigned psi;
  unsigned srsi;
  unsigned brsi;
  unsigned BytesPerSector;
  unsigned DataStat;
  WORK_BLOCK_DESC   * pWorkBlock;
  LOG_SECTOR_HEADER   lsh;

  //
  // Physical block index is taken from Log2Phy table or is work block
  //
  lbi        = _LogSectorIndex2LogBlockIndex(pInst, LogSectorIndex, &brsi);
  psi        = _L2P_Read(pInst, lbi);
  srsi       = brsi;
  pWorkBlock = _FindWorkBlock(pInst, lbi);
  if (pWorkBlock) {
    unsigned u;

    u = _brsi2srsi(pInst, pWorkBlock, brsi);
    if (u != BRSI_INVALID) {
      psi  = pWorkBlock->psi;
      srsi = u;
    }
  }
  //
  // Get data from NOR flash
  //
  r = 0;
  BytesPerSector = 1u << pInst->ldBytesPerSector;
  if (psi == 0) {
    //
    // Logical sector not written yet.
    //
    FS_MEMSET(pData, 0, BytesPerSector);      // O.K., we filled the buffer with 0-bytes since this sector has never been written.
  } else {
    //
    // Read from hardware
    //
    _ReadLSH(pInst, psi, srsi, &lsh);
    DataStat = _GetLogSectorDataStat(&lsh);
    if (DataStat == DATA_STAT_VALID) {
      r = _ReadLogSectorData(pInst, psi, srsi, pData, 0, BytesPerSector);
    } else {
      //
      // Data of logical sector has been invalidated.
      //
      FS_MEMSET(pData, 0, BytesPerSector);
    }
  }
  return r;
}

/*********************************************************************
*
*       _WriteOneLogSector
*
*   Function description
*     Writes one logical sector to storage device.
*
*   Return value
*     ==0     Data successfully written.
*     !=0     An error has occurred.
*
*/
static int _WriteOneLogSector(NOR_BM_INST * pInst, U32 LogSectorIndex, const void * pData) {
  int      r;
  unsigned lbi;
  unsigned brsi;
  unsigned srsi;
  unsigned srsiPrev;
  unsigned psiWork;
  unsigned NumBytes;
  WORK_BLOCK_DESC   * pWorkBlock;
  LOG_SECTOR_HEADER   lsh;

  lbi  = _LogSectorIndex2LogBlockIndex(pInst, LogSectorIndex, &brsi);
  srsi = ~0u;
  //
  // Find (or create) a work block and the sector to be used in it.
  //
  pWorkBlock = _FindWorkBlock(pInst, lbi);
  if (pWorkBlock) {
    //
    // Make sure that sector to write is in work area and has not already been written
    //
    srsi = _FindFreeSectorInWorkBlock(pInst, pWorkBlock, brsi);
    if (srsi == BRSI_INVALID) {
      r = _CleanWorkBlock(pInst, pWorkBlock);
      if (r) {
        return 1;
      }
      pWorkBlock = NULL;
    }
  }
  if (pWorkBlock == NULL) {
    pWorkBlock = _AllocWorkBlock(pInst, lbi);
    if (pWorkBlock == NULL) {
      return 1;
    }
    srsi = brsi;          // Preferred position is free, so let's use it.
  }
  //
  // Write data to work block
  //
  psiWork  = pWorkBlock->psi;
  NumBytes = 1u << pInst->ldBytesPerSector;
  r = _WriteLogSectorData(pInst, psiWork, srsi, pData, 0, NumBytes);
  if (r) {
    _OnFatalError(pInst, NOR_WRITE_ERROR, psiWork);
    return 1;
  }
  //
  // Logical sector contains now valid data.
  //
  _ReadLSH(pInst, psiWork, srsi, &lsh);
  _SetLogSectorDataStat(&lsh, DATA_STAT_VALID);
  lsh.brsi     = brsi;
  _WriteLSH(pInst, psiWork, srsi, &lsh);
#if FS_NOR_ENABLE_STATS
  //
  // For debug builds only. Keep the number of valid sectors up to date
  //
  {
    unsigned psiSrc;
    unsigned DataStat;

    //
    // The number of valid sectors is increased only if the sector
    // is written for the first time since the last low-level format
    // or it is re-written after its value has been invalidated.
    //
    psiSrc   = _L2P_Read(pInst, lbi);
    srsiPrev = _brsi2srsi(pInst, pWorkBlock, brsi);
    if (srsiPrev == BRSI_INVALID) {     // Sector not written yet ?
      if (psiSrc) {                     // Sector in data block ?
        _ReadLSH(pInst, psiSrc, brsi, &lsh);
        DataStat = _GetLogSectorDataStat(&lsh);
        if (DataStat != DATA_STAT_VALID) {
          pInst->StatCounters.NumValidSectors++;
        }
      } else {
        pInst->StatCounters.NumValidSectors++;
      }
    }
  }
#endif // FS_NOR_ENABLE_STATS
  //
  // Invalidate data previously used for the same brsi (if necessary)
  //
  srsiPrev = _brsi2srsi(pInst, pWorkBlock, brsi);
  if (srsiPrev != BRSI_INVALID) {       // Sector written ?
    _ReadLSH(pInst, psiWork, srsiPrev, &lsh);
    _SetLogSectorDataStat(&lsh, DATA_STAT_INVALID);
    _WriteLSH(pInst, psiWork, srsiPrev, &lsh);
  }
  //
  // Update work block management info
  //
  _MarkWorkBlockAsMRU(pInst, pWorkBlock);
  _WB_MarkSectorAsUsed(pWorkBlock, srsi);               // Mark sector as used
  _WB_WriteAssignment(pInst, pWorkBlock, brsi, srsi);   // Update look-up-table
  return 0;
}

/*********************************************************************
*
*        _FreeOneSector
*/
static int _FreeOneSector(NOR_BM_INST * pInst, U32 LogSectorIndex) {
  unsigned lbi;
  unsigned psiSrc;
  unsigned psiWork;
  unsigned brsi;
  unsigned srsi;
  unsigned DataStat;
  int      r;
  WORK_BLOCK_DESC   * pWorkBlock;
  LOG_SECTOR_HEADER   lsh;

  r      = 1;    // No sector freed yet
  lbi    = _LogSectorIndex2LogBlockIndex(pInst, LogSectorIndex, &brsi);
  srsi   = brsi;
  psiSrc = _L2P_Read(pInst, lbi);
  //
  // If necessary, mark the sector as free in the data block
  //
  if (psiSrc) {                   // Sector in a data block ?
    //
    // Invalidate only sectors which contain valid data
    //
    _ReadLSH(pInst, psiSrc, srsi, &lsh);
    DataStat = _GetLogSectorDataStat(&lsh);
    if (DataStat == DATA_STAT_VALID) {
      _SetLogSectorDataStat(&lsh, DATA_STAT_INVALID);
      _WriteLSH(pInst, psiSrc, srsi, &lsh);
      r = 0;          // Sector has been freed
    }
  }
  //
  // If necessary, mark the sector as free in the work block
  //
  pWorkBlock = _FindWorkBlock(pInst, lbi);
  if (pWorkBlock) {
    psiWork = pWorkBlock->psi;
    srsi    = _brsi2srsi(pInst, pWorkBlock, brsi);
    if (srsi != BRSI_INVALID) {       // Sector in a work block ?
      //
      // Mark on the medium the sector data as invalid
      //
      _ReadLSH(pInst, psiWork, srsi, &lsh);
      _SetLogSectorDataStat(&lsh, DATA_STAT_INVALID);
      _WriteLSH(pInst, psiWork, srsi, &lsh);
      //
      // Remove the assignment of the logical sector
      //
      _WB_WriteAssignment(pInst, pWorkBlock, brsi, 0);
      r = 0;          // Sector has been freed
    }
  }
  return r;
}

/*********************************************************************
*
*        _FreeSectors
*
*   Function description
*     Marks a logical sector as free. This routine is called from the
*     higher layer file system to help the driver to manage the data:
*     This way sectors which are no longer in use by the higher
*     layer file system do not need to be copied.
*/
static void _FreeSectors(NOR_BM_INST * pInst, U32 LogSectorIndex, U32 NumSectors) {
  ASSERT_SECTORS_ARE_IN_RANGE(pInst, LogSectorIndex, LogSectorIndex + NumSectors - 1);
  do {
    if (_FreeOneSector(pInst, LogSectorIndex) == 0) {
      IF_STATS(pInst->StatCounters.NumValidSectors--);
    }
    LogSectorIndex++;
  } while (--NumSectors);
}

/*********************************************************************
*
*        _GetSectorUsage
*
*   Function description
*     Checks if a logical sector contains valid data.
*
*   Return values
*     ==0     Sector in use (contains valid data)
*     ==1     Sector not in use (was not written nor was invalidated)
*     ==2     Usage unknown
*/
static int _GetSectorUsage(NOR_BM_INST * pInst, U32 LogSectorIndex) {
  unsigned lbi;
  unsigned pbiSrc;
  unsigned brsi;
  unsigned srsi;
  unsigned DataStat;
  int      r;
  WORK_BLOCK_DESC   * pWorkBlock;
  LOG_SECTOR_HEADER   lsh;

  ASSERT_SECTORS_ARE_IN_RANGE(pInst, LogSectorIndex, LogSectorIndex);
  r      = 1;                // Sector not in use
  lbi    = _LogSectorIndex2LogBlockIndex(pInst, LogSectorIndex, &brsi);
  pbiSrc = _L2P_Read(pInst, lbi);
  //
  // First check if the sector is in a data block
  //
  if (pbiSrc) {                     // Sector in a data block ?
    _ReadLSH(pInst, pbiSrc, brsi, &lsh);
    DataStat = _GetLogSectorDataStat(&lsh);
    if (DataStat == DATA_STAT_VALID) {
      r = 0;                        // Sector has valid data
    }
  }
  if (r == 0) {
    pWorkBlock = _FindWorkBlock(pInst, lbi);
    if (pWorkBlock) {
      srsi = _brsi2srsi(pInst, pWorkBlock, brsi);
      if (srsi != BRSI_INVALID) {   // Sector in a work block ?
        r = 0;                      // Sector has valid data
      }
    }
  }
  return r;
}

/*********************************************************************
*
*       _CleanOne
*
*   Function description
*     Executes a single "clean" job. This might be the conversion of a work block into a data block
*     or the erase of a block marked as invalid.
*
*   Return value
*     ==0   Nothing else to "clean"
*     ==1   At least one more "clean" job left to do
*/
static int _CleanOne(NOR_BM_INST * pInst) {
  //
  // Clean work blocks first
  //
  if (pInst->pFirstWorkBlockInUse) {
    _CleanWorkBlock(pInst, pInst->pFirstWorkBlockInUse);
  }
  //
  // Now check if there is more work to do
  //
  if (pInst->pFirstWorkBlockInUse) {
    return 1;     // At least one more work block to clean
  }
  return 0;
}

/*********************************************************************
*
*       _Clean
*
*  Function description
*    Converts all work blocks into data blocks and erases all free blocks.
*/
static void _Clean(NOR_BM_INST * pInst) {
  _CleanAllWorkBlocks(pInst);
}

/*********************************************************************
*
*       _LowLevelFormat
*
*   Function description
*     Erases all blocks and writes the format information to the first one.
*
*   Return value:
*     ==0  O.K.
*     !=0  Error
*/
static int _LowLevelFormat(NOR_BM_INST * pInst) {
  int r;
  U16 NumPhySectors;
  U32 PhySectorIndex;
  U32 Version;
  U32 BytesPerSector;
  U32 NumLogBlocks;
  U16 NumWorkBlocks;

  pInst->LLMountFailed = 0;
  pInst->IsLLMounted   = 0;
  //
  // Erase all physical sectors of NOR flash.
  //
  r = 0;
  NumPhySectors = pInst->NumPhySectors;
  for (PhySectorIndex = 0; PhySectorIndex < NumPhySectors; ++PhySectorIndex) {
    r = _ErasePhySector(pInst, PhySectorIndex);
    if (r) {
      return 1;
    }
  }
  IF_STATS(pInst->StatCounters.NumValidSectors = 0);
  //
  // Write the format information to medium. This information is used by the driver at low-level mount.
  //
  Version        = LLFORMAT_VERSION;
  BytesPerSector = 1u << pInst->ldBytesPerSector;
  NumLogBlocks   = pInst->NumLogBlocks;
  NumWorkBlocks  = pInst->NumWorkBlocks;
  r  = 0;
  r |= _WriteLogSectorData(pInst, INFO_BLOCK_PSI, 0, _acInfo,         0,                         sizeof(_acInfo));
  r |= _WriteLogSectorData(pInst, INFO_BLOCK_PSI, 0, &Version,        INFO_OFF_LLFORMAT_VERSION, sizeof(Version));
  r |= _WriteLogSectorData(pInst, INFO_BLOCK_PSI, 0, &BytesPerSector, INFO_OFF_BYTES_PER_SECTOR, sizeof(BytesPerSector));
  r |= _WriteLogSectorData(pInst, INFO_BLOCK_PSI, 0, &NumLogBlocks,   INFO_OFF_NUM_LOG_BLOCKS,   sizeof(NumLogBlocks));
  r |= _WriteLogSectorData(pInst, INFO_BLOCK_PSI, 0, &NumWorkBlocks,  INFO_OFF_NUM_WORK_BLOCKS,  sizeof(NumWorkBlocks));
  return r;
}

/*********************************************************************
*
*       _InitIfRequired
*
*   Function description
*     Intializes the NOR device if not already done.
*
*/
static int _InitIfRequired(NOR_BM_INST * pInst) {
  if (pInst->IsInited) {
    return 0;                           // Nothing to do, already initialized
  }
  if (_ReadApplyDeviceParas(pInst)) {   // Read parameters from device
    return 1;
  }
  pInst->IsInited = 1;
  return 0;                             // Device is accessible
}

/*********************************************************************
*
*       _AllocInstIfRequired
*
*   Function description
*     Allocate memory for the specified unit if required.
*
*/
static NOR_BM_INST * _AllocInstIfRequired(U8 Unit) {
  NOR_BM_INST * pInst;

  if (Unit >= FS_NOR_NUM_UNITS) {
    FS_DEBUG_ERROROUT((FS_MTYPE_DRIVER, "Invalid unit number %d.", Unit));
    FS_X_Panic(FS_ERROR_UNKNOWN_DEVICE);
    return NULL;
  }
  pInst = _apInst[Unit];
  if (pInst == NULL) {
    FS_AllocZeroedPtr((void**)&_apInst[Unit], sizeof(NOR_BM_INST));
    pInst = _apInst[Unit];
  }
  pInst->Unit = Unit;
  return pInst;
}

/*********************************************************************
*
*       Global functions
*
**********************************************************************
*/

/*********************************************************************
*
*       _NOR_InitMedium
*
*   Function description
*     Initialize and identifies the storage device.
*
*   Return value
*     ==0   Device OK and ready for operation.
*     !=0   An error has occurred.
*/
static int _NOR_InitMedium(U8 Unit) {
  NOR_BM_INST * pInst;

  ASSERT_UNIT_NO_IS_IN_RANGE(Unit);
  pInst = _apInst[Unit];
  ASSERT_PHY_TYPE_IS_SET(pInst);
  return _InitIfRequired(pInst);
}

/*********************************************************************
*
*       _NOR_GetDriverName
*/
static const char * _NOR_GetDriverName(U8 Unit) {
  FS_USE_PARA(Unit);
  return "nor";
}

/*********************************************************************
*
*       _NOR_AddDevice
*/
static int _NOR_AddDevice(void) {
  if (_NumUnits >= FS_NOR_NUM_UNITS) {
    return -1;
  }
  _AllocInstIfRequired((U8)_NumUnits);
  return _NumUnits++;
}

/*********************************************************************
*
*       _NOR_Read
*
*/
static int _NOR_Read(U8 Unit, U32 SectorNo, void * p, U32 NumSectors) {
  U8          * pData;
  NOR_BM_INST * pInst;
  int           r;
  unsigned      BytesPerSector;

  ASSERT_UNIT_NO_IS_IN_RANGE(Unit);
  pInst = _apInst[Unit];
  pData = (U8 *)p;
  //
  // Make sure device is low-level mounted. If it is not, there is nothing we can do.
  //
  r = _LowLevelMountIfRequired(pInst);
  if (r) {
    return r;             // Error
  }
  ASSERT_SECTORS_ARE_IN_RANGE(pInst, SectorNo, SectorNo + NumSectors - 1);
  //
  // Read data one sector at a time
  //
  BytesPerSector = 1u << pInst->ldBytesPerSector;
  do {
    r = _ReadOneLogSector(pInst, SectorNo, pData);
    if (r) {
      FS_DEBUG_ERROROUT((FS_MTYPE_DRIVER,  "NOR_BM: Failed to read logical sector."));
      return r;           // Error
    }
    pData += BytesPerSector;
    ++SectorNo;
    IF_STATS(pInst->StatCounters.ReadSectorCnt++);
  } while (--NumSectors);
  return 0;               // O.K.
}

/*********************************************************************
*
*       _NOR_Write
*
*/
static int _NOR_Write(U8 Unit, U32 SectorNo, const void  * p, U32 NumSectors, U8  RepeatSame) {
  const U8    * pData;
  NOR_BM_INST * pInst;
  int           r;
  unsigned      BytesPerSector;

  ASSERT_UNIT_NO_IS_IN_RANGE(Unit);
  pInst = _apInst[Unit];
  //
  // Mount the NOR flash if not already mounted
  //
  r = _LowLevelMountIfRequired(pInst);
  if (r) {
    return r;
  }
  //
  // Refuse to write if we encountered a fatal error. Low-level format required to make the NOR flash writable.
  //
  if (pInst->HasFatalError) {
    FS_DEBUG_ERROROUT((FS_MTYPE_DRIVER, "NOR_BM: Failed to write. Medium has a fatal error."));
    return 1;
  }
  ASSERT_SECTORS_ARE_IN_RANGE(pInst, SectorNo, SectorNo + NumSectors - 1);
  //
  // Write data one sector at a time
  //
  pData          = (const U8 *)p;
  BytesPerSector = 1u << pInst->ldBytesPerSector;
  do {
    r = _WriteOneLogSector(pInst, SectorNo, pData);
    if (r) {
      return 1;                     // Error, write failed
    }
    IF_STATS(pInst->StatCounters.WriteSectorCnt++);
    if (--NumSectors == 0) {
      break;
    }
    if (RepeatSame == 0) {
      pData += BytesPerSector;
    }
    ++SectorNo;
  } while (1);
  return 0;                                    // O.K.
}

/*********************************************************************
*
*       _NOR_IoCtl
*/
static int _NOR_IoCtl(U8 Unit, I32 Cmd, I32 Aux, void * pBuffer) {
  NOR_BM_INST * pInst;
  FS_DEV_INFO * pDevInfo;
  int           r;

  FS_USE_PARA(Aux);
  ASSERT_UNIT_NO_IS_IN_RANGE(Unit);
  pInst    = _apInst[Unit];
  r        = -1;
  switch (Cmd) {
  case FS_CMD_GET_DEVINFO:
    if (pBuffer) {
      //
      // This low-level mount is required in oder to calculate
      // the correct number of sectors available to the file system
      //
      r = _LowLevelMountIfRequired(pInst);
      if (r == 0) {
        pDevInfo = (FS_DEV_INFO *)pBuffer;
        pDevInfo->NumSectors     = pInst->NumLogSectors;
        pDevInfo->BytesPerSector = 1 << pInst->ldBytesPerSector;
      }
    }
    break;
  case FS_CMD_FORMAT_LOW_LEVEL:
    return _LowLevelFormat(pInst);
  case FS_CMD_REQUIRES_FORMAT:
    return _LowLevelMountIfRequired(pInst);
  case FS_CMD_UNMOUNT:
  case FS_CMD_UNMOUNT_FORCED:
    pInst->IsInited             = 0;
    pInst->IsLLMounted          = 0;
    pInst->MRUFreeBlock         = 0;
    pInst->pFirstWorkBlockFree  = NULL;
    pInst->pFirstWorkBlockInUse = NULL;
    r = 0;
    break;
  case FS_CMD_CLEAN_ONE:
    r = _LowLevelMountIfRequired(pInst);
    if (r == 0) {
      int   More;
      int * pMore;

      More  = _CleanOne(pInst);
      pMore = (int *)pBuffer;
      if (pMore) {
        *pMore = More;
      }
    }
    break;
  case FS_CMD_CLEAN:
    r = _LowLevelMountIfRequired(pInst);
    if (r == 0) {
      _Clean(pInst);
    }
    break;
  case FS_CMD_GET_SECTOR_USAGE:
    if (pBuffer) {
      r = _LowLevelMountIfRequired(pInst);
      if (r == 0) {
        int * pSectorUsage;

        pSectorUsage  = (int *)pBuffer;
        *pSectorUsage = _GetSectorUsage(pInst, (U32)Aux);
      }
    }
    break;
  case FS_CMD_FREE_SECTORS:
    r = _LowLevelMountIfRequired(pInst);
    if (r == 0) {
      U32 SectorIndex;
      U32 NumSectors;

      SectorIndex = (U32)Aux;
      NumSectors  = *(U32 *)pBuffer;
      _FreeSectors(pInst, SectorIndex, NumSectors);
    }
    break;
#if FS_SUPPORT_DEINIT
  case FS_CMD_DEINIT:
    {
      unsigned u;

      FS_FREE(pInst->pLog2PhyTable);
      FS_FREE(pInst->pFreeMap);
      if (pInst->paWorkBlock) {       // The array is allocated only when the volume is mounted.
        for (u = 0; u < pInst->NumWorkBlocks; u++) {
          WORK_BLOCK_DESC * pWorkBlock;

          pWorkBlock = &pInst->paWorkBlock[u];
          FS_FREE(pWorkBlock->paIsWritten);
          FS_FREE(pWorkBlock->paAssign);
        }
        FS_FREE(pInst->paWorkBlock);
      }
      FS_FREE(pInst);
      _apInst[Unit] = 0;
      r = 0;
    }
    break;
#endif
  }
  return r;
}

/*********************************************************************
*
*       _NOR_GetNumUnits
*/
static int _NOR_GetNumUnits(void) {
  return _NumUnits;
}

/*********************************************************************
*
*       _NOR_GetStatus
*/
static int _NOR_GetStatus(U8 Unit) {
  FS_USE_PARA(Unit);
  return FS_MEDIA_IS_PRESENT;
}

/*********************************************************************
*
*       API Table
*
**********************************************************************
*/
const FS_DEVICE_TYPE FS_NOR_BM_Driver = {
  _NOR_GetDriverName,
  _NOR_AddDevice,
  _NOR_Read,
  _NOR_Write,
  _NOR_IoCtl,
  _NOR_InitMedium,
  _NOR_GetStatus,
  _NOR_GetNumUnits
};

/*********************************************************************
*
*       Public code
*
**********************************************************************
*/

/*********************************************************************
*
*       FS_NOR_BM_Configure
*
*   Function description
*     Configures a single instance of the driver
*
*   Parameters
*     Unit        Driver index
*     BaseAddr    Address of the first byte in NOR flash
*     StartAddr   Address of the first byte the driver should use as storage.
*                 It must be greater than BaseAddr
*     NumBytes    Number of bytes from StartAddr available to driver
*/
void FS_NOR_BM_Configure(U8 Unit, U32 BaseAddr, U32 StartAddr, U32 NumBytes) {
  NOR_BM_INST * pInst;

  pInst = _AllocInstIfRequired(Unit);
  ASSERT_PHY_TYPE_IS_SET(pInst);
  pInst->pPhyType->pfConfigure(Unit, BaseAddr, StartAddr, NumBytes);
}

/*********************************************************************
*
*       FS_NOR_BM_GetStatCounters
*/
void FS_NOR_BM_GetStatCounters(U8 Unit, FS_NOR_BM_STAT_COUNTERS * pStat) {
#if FS_NOR_ENABLE_STATS
  NOR_BM_INST * pInst;

  ASSERT_UNIT_NO_IS_IN_RANGE(Unit);
  pInst  = _AllocInstIfRequired(Unit);
  *pStat = pInst->StatCounters;
#else
  FS_USE_PARA(Unit);
  FS_MEMSET(pStat, 0, sizeof(FS_NOR_BM_STAT_COUNTERS));
#endif
}

/*********************************************************************
*
*       FS_NOR_BM_ResetStatCounters
*/
void FS_NOR_BM_ResetStatCounters(U8 Unit) {
#if FS_NOR_ENABLE_STATS
  NOR_BM_INST * pInst;
  FS_NOR_BM_STAT_COUNTERS * pStat;

  ASSERT_UNIT_NO_IS_IN_RANGE(Unit);
  pInst  = _AllocInstIfRequired(Unit);
  pStat = &pInst->StatCounters;
  pStat->ConvertInPlaceCnt = 0;
  pStat->ConvertViaCopyCnt = 0;
  pStat->CopySectorCnt     = 0;
  pStat->EraseCnt          = 0;
  pStat->ReadSectorCnt     = 0;
  pStat->WriteSectorCnt    = 0;
#else
  FS_USE_PARA(Unit);
#endif
}

/*********************************************************************
*
*       FS_NOR_BM_SetPhyType
*/
void FS_NOR_BM_SetPhyType(U8 Unit, const FS_NOR_PHY_TYPE * pPhyType) {
  NOR_BM_INST * pInst;

  ASSERT_UNIT_NO_IS_IN_RANGE(Unit);
  pInst  = _AllocInstIfRequired(Unit);
  pInst->pPhyType = pPhyType;
  pPhyType->pfOnSelectPhy(Unit);
}

/*********************************************************************
*
*       FS_NOR_BM_SetMaxEraseCntDiff
*/
void FS_NOR_BM_SetMaxEraseCntDiff(U8 Unit, U32 EraseCntDiff) {
  NOR_BM_INST * pInst;

  ASSERT_UNIT_NO_IS_IN_RANGE(Unit);
  pInst = _AllocInstIfRequired(Unit);
  pInst->MaxEraseCntDiff = EraseCntDiff;
}

/*********************************************************************
*
*       FS_NOR_BM_SetNumWorkBlocks
*
*   Function description
*     Configures the number of work blocks.
*/
void FS_NOR_BM_SetNumWorkBlocks(U8 Unit, unsigned NumWorkBlocks) {
  NOR_BM_INST * pInst;

  ASSERT_UNIT_NO_IS_IN_RANGE(Unit);
  pInst = _AllocInstIfRequired(Unit);
  pInst->NumWorkBlocksConf = NumWorkBlocks;
}

/*********************************************************************
*
*       FS_NOR_BM_SetSectorSize
*
*   Function description
*     Configures the number of bytes in a logical sector.
*/
void FS_NOR_BM_SetSectorSize(U8 Unit, unsigned SectorSize) {
  NOR_BM_INST * pInst;

  ASSERT_UNIT_NO_IS_IN_RANGE(Unit);
  pInst = _AllocInstIfRequired(Unit);
  pInst->BytesPerSectorConf = (U16)SectorSize;
}

/*********************************************************************
*
*       FS_NOR_BM_GetDiskInfo
*
*   Function description
*     Returns information about the NOR flash.
*     This function is not required for the functionality of the driver and will typically not be linked in in production builds.
*
*/
int FS_NOR_BM_GetDiskInfo(U8 Unit, FS_NOR_BM_DISK_INFO * pDiskInfo) {
  NOR_BM_INST * pInst;
  unsigned      iSector;
  U16           NumPhySectors;
  U16           NumUsedPhySectors;
  U32           EraseCntMax;
  U32           EraseCntMin;
  U32           EraseCntAvg;
  U32           EraseCnt;
  U32           EraseCntTotal;
  U32           NumEraseCnt;
  int           r;

  ASSERT_UNIT_NO_IS_IN_RANGE(Unit);
  //
  // Allocate a driver instance, initialize it and mount the NOR flash.
  //
  pInst = _AllocInstIfRequired(Unit);     // It ever gives a NULL back.
  r     = _InitIfRequired(pInst);
  if (r) {
    return 1;                             // Error: could not initialize the NOR flash.
  }
  r = _LowLevelMountIfRequired(pInst);
  if (r) {
    return 1;                             // Error: could not mount the NOR flash.
  }
  FS_MEMSET(pDiskInfo, 0, sizeof(FS_NOR_BM_DISK_INFO));
  //
  // Retrieve the number of used physical blocks and block status
  //
  NumUsedPhySectors = 0;
  NumPhySectors     = pInst->NumPhySectors;
  EraseCntMax       = 0;
  EraseCntMin       = 0xFFFFFFFF;
  NumEraseCnt       = 0;
  EraseCntTotal     = 0;
  //
  // Iterate over all physical sectors and collect information.
  // about the number of physical sectors in use and about the erase counts.
  //
  for (iSector = 0; iSector < NumPhySectors; iSector++) {
    PHY_SECTOR_HEADER psh;

    //
    // Check if the physical sector is not used for data storage
    //
    if (_PhySectorIsFree(pInst, iSector) == 0) {
      NumUsedPhySectors++;
    }
    _ReadPSH(pInst, iSector, &psh);
    EraseCnt = psh.EraseCnt;
    if (EraseCnt != ERASE_CNT_INVALID) {
      if (EraseCnt > EraseCntMax) {
        EraseCntMax = EraseCnt;
      }
      if (EraseCnt < EraseCntMin) {
        EraseCntMin = EraseCnt;
      }
      EraseCntTotal += EraseCnt;
      ++NumEraseCnt;
    }
  }
  if (NumEraseCnt) {
    EraseCntAvg = EraseCntTotal / NumEraseCnt;
  } else {
    EraseCntAvg = 0;
  }
  pDiskInfo->NumPhySectors      = NumPhySectors;
  pDiskInfo->NumLogBlocks       = pInst->NumLogBlocks;
  pDiskInfo->NumUsedPhySectors  = NumUsedPhySectors;
  pDiskInfo->LSectorsPerPSector = pInst->LSectorsPerPSector;
  pDiskInfo->BytesPerSector     = 1 << pInst->ldBytesPerSector;
  pDiskInfo->EraseCntMax        = EraseCntMax;
  pDiskInfo->EraseCntMin        = EraseCntMin;
  pDiskInfo->EraseCntAvg        = EraseCntAvg;
  pDiskInfo->HasFatalError      = pInst->HasFatalError;
  pDiskInfo->ErrorType          = pInst->ErrorType;
  pDiskInfo->ErrorPSI           = pInst->ErrorPSI;
  return 0;           // OK, information read.
}

/*********************************************************************
*
*       FS_NOR_BM_ReadOff
*
*   Function description
*     Reads bytes from the NOR flash memory.
*     This function is not required for the functionality of the driver and will typically not be linked in in production builds.
*
*/
int FS_NOR_BM_ReadOff(U8 Unit, void * pData, U32 Off, U32 NumBytes) {
  NOR_BM_INST * pInst;
  int           r;

  ASSERT_UNIT_NO_IS_IN_RANGE(Unit);
  pInst = _AllocInstIfRequired(Unit);     // It ever gives a NULL back.
  r     = _InitIfRequired(pInst);
  if (r) {
    return 1;
  }
  _ReadOff(pInst, pData, Off, NumBytes);
  return 0;
}

/*************************** End of file ****************************/
