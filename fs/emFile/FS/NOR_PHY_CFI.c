/*********************************************************************
*                SEGGER MICROCONTROLLER GmbH & Co. KG                *
*        Solutions for real time microcontroller applications        *
**********************************************************************
*                                                                    *
*        (c) 2003-2012     SEGGER Microcontroller GmbH & Co KG       *
*                                                                    *
*        Internet: www.segger.com    Support:  support@segger.com    *
*                                                                    *
**********************************************************************

**** emFile file system for embedded applications ****
emFile is protected by international copyright laws. Knowledge of the
source code may not be used to write a similar product. This file may
only be used in accordance with a license and should not be re-
distributed in any way. We appreciate your understanding and fairness.
----------------------------------------------------------------------
File        : NOR_PHY_CFI.c
Purpose     : Low level flash driver for x16 bit CFI compliant flash chips
----------------------------------------------------------------------

Comments in this file refer to Intel's "Common Flash Interface
(CFI) and Command Sets" document as the "CFI spec."

Supported devices:
Any CFI-compliant flash in 16-bit mode


Literature:
[1] Intel's "Common Flash Interface (CFI) and Command Sets"
    Application Note 646, April 2000

[2] Spansion's "Common Flash Interface Version 1.4 Vendor Specific Extensions"
    Revision A, Amendment 0, March 22 - 2004

------------  END-OF-HEADER  -----------------------------------------
*/

#include "FS_Int.h"
#include "NOR_Private.h"

/*********************************************************************
*
*      Defines
*
**********************************************************************
*/

/*********************************************************************
*
*       Configurable defines
*
**********************************************************************
*/


#ifndef   MAX_SECTOR_BLOCKS
  #define MAX_SECTOR_BLOCKS  6       // Worst (known) case CFI flash has 6 sector blocks. Typical flashes have only 2, such as 63 * 64kB + 8 * 8kB
#endif

#ifndef   NUM_UNITS
  #define NUM_UNITS           2
#endif

/*********************************************************************
*
*       Fixed defines
*
**********************************************************************
*/

/*********************************************************************
*
*       Algo types.
*       These values are defined by hardware. DO NOT CHANGE!
*/
#define ALGO_TYPE_INTEL_EXT       0x0001
#define ALGO_TYPE_AMD_STD         0x0002
#define ALGO_TYPE_INTEL_STD       0x0003
#define ALGO_TYPE_AMD_EXT         0x0004
#define ALGO_TYPE_SST             0x0701

/*********************************************************************
*
*       CFI_OFF_...: Offset of parameters in CFI string
*/
#define CFI_OFF_NUMBLOCKS            0x2c   // Offset to the number of erase block regions
#define CFI_OFF_SECTORINFO           0x2d   // Offset to the first erase block region information
#define CFI_OFF_AMD_REVERSE          0x4f   // Offset to the AMD specific top / bottom sector flag info

/*********************************************************************
*
*      Types
*
**********************************************************************
*/

/*********************************************************************
*
*      SECTOR_BLOCK
*
*  A Sector block defines a number of adjacent sectors of the same size.
*  It therefor contains the size (of one sector) and number of sectors in the block.
*  The entire sectorization can be described with just a few sector blocks.
*/
typedef struct {
  U32 SectorSize;
  U32 NumSectors;
} SECTOR_BLOCK;

typedef struct {
  U32  BaseAddr;
  U32  StartAddrConf;        // Configured start addr
  U32  StartAddrUsed;        // Start addr. actually used (aligned to start of a sector)
  U32  NumBytes;
  SECTOR_BLOCK aSectorUsed[MAX_SECTOR_BLOCKS];
  U16  NumSectorBlocksUsed;
  U16  NumSectorsTotal;
  U8   IsInited;
  U8   IsConfigured;
  U8   NumChips;
  FS_NOR_READ_CFI_FUNC     * pReadCFI;
  const FS_NOR_PROGRAM_HW  * pProgramHW;
  U8   MultiBytesAtOnce;
} PROPS;

/*********************************************************************
*
*      Static data
*
**********************************************************************
*/

static PROPS *  _apProps[NUM_UNITS];

/*********************************************************************
*
*      Static code
*
**********************************************************************
*/

/*********************************************************************
*
*      _CFILoad_U16
*
*  Function description
*    Loads a 16-bit value from a CFI string. The 16 bit value is
*    stored in little endian format.
*/
static U16 _CFILoad_U16(const U8 *pBuffer) {
  U16 r;

  r  = * pBuffer;
  r |= *(pBuffer + 1) << 8;
  return r;
}

/*********************************************************************
*
*      _LoadSectorSize
*
*  Function description
*    Read the sector size from the given location.
*/
static U32 _LoadSectorSize(const U8* pData) {
  U32 Size;

  Size = _CFILoad_U16(pData);
  if (Size == 0) {
    return 128;       /* CFI spec. p. 9 says a value of 0 means 128-byte block size. */
  } else {
    return Size << 8; /* Size != 0 means we have to multiply with 256 */
  }
}

/*********************************************************************
*
*      _Init
*
* Purpose:
*   This routine checks if the device is a valid CFI device.
*   If device is CFI compliant, the routine sets and fills the
*   information required by the erase and write routines.
*   CFI information are dynamically read as nessecary.
*
*   Notes:
*    (1)      Acc. to [2], AMD   compatible flashes have the value 0x40 at off 0x15.
*             Acc. to [1], Intel compatible Flashes have the value 0x50.
*/
static void _Init(U8 Unit, PROPS * pProps) {
  U8   Off;
  U8   NumBlocks;
  int  i;
  U8   ReverseSectorBlocks;
  U16  NumSectors;
  U32  SectorSize;
  U8   aInfo[16];
  unsigned NumBlocksUsed = 0;
  I32  NumBytesToSkip;
  I32  NumBytesRem;
  U32  NumBytesSkipped = 0;
  U16  NumSectorsTotal = 0;
  U32  BaseAddr;
  U8   MultiByteWrite;
  U16  AlgoType;

  ReverseSectorBlocks = 0;
  BaseAddr = pProps->BaseAddr;
  //
  // Read 0x10 - 0x16, containing ID "QRY" @10-12, Primary command set @13-14 and Addr of Primary Extended Table @15-16
  //
  pProps->pReadCFI(Unit, BaseAddr, 0x10, &aInfo[0] , 7);
  //
  // Check if it is a CFI compatible device
  //
  if (   (aInfo[0] == 'Q')
      && (aInfo[1] == 'R')
      && (aInfo[2] == 'Y')) {
    //
    // Read Algo type of used flash
    //
    AlgoType       = _CFILoad_U16(&aInfo[3]);      // 1: INTEL, 2: AMD --> [1],[2]
    //
    // Determine flash algorithm
    //
    if (pProps->NumChips == 1) {
      if (AlgoType == ALGO_TYPE_INTEL_STD) {
        pProps->pProgramHW = &FS_NOR_Program_Intel_1x16;
      } else if (AlgoType == ALGO_TYPE_INTEL_EXT) {
        pProps->pProgramHW = &FS_NOR_Program_IntelFast_1x16;
      } else if (AlgoType == ALGO_TYPE_AMD_STD) {
        pProps->pProgramHW = &FS_NOR_Program_AMD_1x16;
      } else if (AlgoType == ALGO_TYPE_AMD_EXT) {
        pProps->pProgramHW = &FS_NOR_Program_AMD_1x16;
      } else if (AlgoType == ALGO_TYPE_SST) {
       //
       // SST flashes are not fully CFI compliant.
       // The algo type 0x0701 that is returned is not listed in the CFI spec.
       // The algo type would be according to the CFI specification vendor specific.
       // Datasheets of all SST39x say that there are AMD command compliant.
       //
        pProps->pProgramHW = &FS_NOR_Program_AMD_1x16;
      }
    } else if (pProps->NumChips == 2) {
      if ((AlgoType == ALGO_TYPE_INTEL_STD) || (AlgoType == ALGO_TYPE_INTEL_EXT)) {
        pProps->pProgramHW = &FS_NOR_Program_Intel_2x16;
      } else if ((AlgoType == ALGO_TYPE_AMD_STD) || (AlgoType == ALGO_TYPE_AMD_EXT)) {
        pProps->pProgramHW = &FS_NOR_Program_AMD_2x16;
      }
    }
    if (pProps->pProgramHW == NULL) {
      FS_DEBUG_WARN((FS_MTYPE_DRIVER, "NOR_PHY_CFI: Algorithm %d is either not supported or not allowed", AlgoType));
    }
    //
    // AMD specific parameter table ? (Note 1)
    //
    if (_CFILoad_U16(&aInfo[5]) == 0x40) {
      //
      // Reverse blocks if "Boot Block Flag" info tells us to do so.
      //
      pProps->pReadCFI(Unit, BaseAddr, 0x4F, &aInfo[0], 1);
      if (aInfo[0] == 3) {
        ReverseSectorBlocks = 1;
      }
    }
    //
    // Check if write to buffer command is supported
    //
    pProps->pReadCFI(Unit, BaseAddr, 0x2a, &MultiByteWrite, 1);
    if (MultiByteWrite >= 5) {
      if (pProps->pProgramHW == &FS_NOR_Program_AMD_1x16) {
        pProps->pProgramHW = &FS_NOR_Program_AMDFast_1x16;
        pProps->MultiBytesAtOnce = 1 << MultiByteWrite;
      }
    }    
    //
    // Read number of sector blocks
    //
    pProps->pReadCFI(Unit, BaseAddr, CFI_OFF_NUMBLOCKS, &aInfo[0], 1);
    NumBlocks = aInfo[0];
    if (NumBlocks > MAX_SECTOR_BLOCKS) {
      NumBlocks = MAX_SECTOR_BLOCKS;
    }
    //
    // Read physical sector block information and add it to the list of used blocks
    //
    NumBytesToSkip = pProps->StartAddrConf - pProps->BaseAddr;
    NumBytesRem    = pProps->NumBytes;
    for (i = 0; i < NumBlocks; i++) {
      unsigned j;
      // Swap sector blocks if device is a top boot device
      if (ReverseSectorBlocks) {
        j = NumBlocks - i - 1;
      } else {
        j = i;
      }
      Off        = CFI_OFF_SECTORINFO + (j << 2);
      pProps->pReadCFI(Unit, BaseAddr, Off, &aInfo[0] , 4);
      NumSectors = _CFILoad_U16(&aInfo[0]) + 1;
      SectorSize = _LoadSectorSize(&aInfo[2]) * pProps->NumChips;
      //
      // Take care of bytes to skip before data area
      //
      while (NumSectors && (NumBytesToSkip > 0)) {
        NumBytesToSkip  -= SectorSize;
        NumBytesSkipped += SectorSize;
        NumSectors--;
      }
      if (NumSectors) {
        U16 NumSectorsRem;
        NumSectorsRem = (U16) ((U32)NumBytesRem / SectorSize);
        if (NumSectors > NumSectorsRem) {
          NumSectors = NumSectorsRem;
          NumBytesRem = 0;      // No more sectors after this to make sure that sectors are adjacent!
        } else {
          NumBytesRem -= NumSectors * SectorSize;
        }
        //
        // Take care of bytes to skip after data area
        //
        if (NumSectors) {
          if (NumBlocksUsed == 0) {
            pProps->StartAddrUsed = pProps->BaseAddr + NumBytesSkipped;       // Remember addr of first sector used
          }
          pProps->aSectorUsed[NumBlocksUsed].SectorSize = SectorSize;
          pProps->aSectorUsed[NumBlocksUsed].NumSectors = NumSectors;
          NumBlocksUsed++;
          NumSectorsTotal += NumSectors;
        }
      }
    }
    pProps->NumSectorBlocksUsed = NumBlocksUsed;
    pProps->NumSectorsTotal     = NumSectorsTotal;
    pProps->IsInited  = 1;
    //
    // Perform a sanity check
    //
    if (NumBlocksUsed == 0) {
      FS_DEBUG_ERROROUT((FS_MTYPE_DRIVER, "NOR_PHY_CFI: Flash size to small for this configuration. 0 bytes available."));
    }
  }
}

/*********************************************************************
*
*      _GetSectorOff
*/
static U32 _GetSectorOff(PROPS * pProps, unsigned SectorIndex) {
  unsigned i;
  unsigned NumBlocks;
  unsigned NumSectors;
  U32 Off;

  NumBlocks = pProps->NumSectorBlocksUsed;
  Off = 0;
  for (i = 0; i < NumBlocks; i++) {
    NumSectors = pProps->aSectorUsed[i].NumSectors;
    if (SectorIndex < NumSectors) {
      NumSectors = SectorIndex;
    }
    Off += NumSectors * pProps->aSectorUsed[i].SectorSize;
    SectorIndex -= NumSectors;   // Number of remaining sectors
  }
  return Off;
}

/*********************************************************************
*
*      _GetSectorSize
*/
static U32 _GetSectorSize(PROPS * pProps, unsigned SectorIndex) {
  unsigned i;
  unsigned NumBlocks;
  unsigned NumSectors;

  NumBlocks = pProps->NumSectorBlocksUsed;
  for (i = 0; i < NumBlocks; i++) {
    NumSectors = pProps->aSectorUsed[i].NumSectors;
    if (SectorIndex < NumSectors) {
      return pProps->aSectorUsed[i].SectorSize;
    }
    SectorIndex -= NumSectors;   // Number of remaining sectors
  }
  return 0;                      // SectorIndex was out of bounds
}

/*********************************************************************
*
*      _WriteData
*
* Purpose:
*   Handles relocation if necessary before writing data
* Return value:
*   0       O.K., data has been written
*   other   error
*/
static char _WriteData(U8 Unit, PROPS * pProps, U32 SectorAddr, U32 Addr, const void FS_NOR_FAR * pSrc, int NumItems) {
  char r;

  if (pProps->pProgramHW) {
    r = pProps->pProgramHW->pfWrite(Unit, pProps->BaseAddr, SectorAddr, Addr, (const U16 FS_NOR_FAR *)pSrc, NumItems);
  } else {
    FS_DEBUG_ERROROUT((FS_MTYPE_DRIVER, "NOR_PHY_CFI: Unsupported flash algo"));
    return 1;
  }
  return r;
}

/*********************************************************************
*
*      _AllocIfRequired
*
* Purpose:
*   Handles relocation if necessary before writing data
*
*/
static PROPS * _AllocIfRequired(U8 Unit) {
  if (_apProps[Unit] == NULL) {
     _apProps[Unit] = (PROPS *)FS_AllocZeroed(sizeof(PROPS));
  }
  return _apProps[Unit];
}

/*********************************************************************
*
*      Exported code
*
**********************************************************************
*/


/*********************************************************************
*
*       _GetNumSectors
*
* Purpose:
*   Returns the number of flash sectors
*/
static int _GetNumSectors(U8 Unit) {
  int r;

  r = 0;
  if (_apProps[Unit]) {
    r = _apProps[Unit]->NumSectorsTotal;
  }
  return r;
}

/*********************************************************************
*
*       _GetSectorInfo
*
* Purpose:
*   Returns the offset and length of the given sector
*/
static void _GetSectorInfo(U8 Unit, unsigned int SectorIndex, U32 * pOff, U32 * pLen) {
  unsigned NumSectors;
  U32      SectorOff;
  U32      SectorSize;
  PROPS  * pProps;

  SectorOff  = 0;
  SectorSize = 0;
  pProps     = _apProps[Unit];
  if (pProps) {
    //
    // Make sure SectorIndex is in range
    //
    NumSectors = pProps->NumSectorsTotal;
    if (SectorIndex >= NumSectors) {
      return;          // Out of bounds ... Should not really happen!
    }
    //
    // Compute result
    //
    SectorOff  = _GetSectorOff(pProps, SectorIndex);
    SectorSize = _GetSectorSize(pProps, SectorIndex);
  }
  if (pOff) {
    *pOff = SectorOff;
  }
  if (pLen) {
    *pLen = SectorSize;
  }

}

/*********************************************************************
*
*       _Off2SectorIndex
*/
static int _Off2SectorIndex(U8 Unit, U32 Addr) {
  U32 i, NumSectors = _GetNumSectors(Unit);
  for (i = 0; i < NumSectors; i++) {
    U32 SectorAddr, SectorSize;
    _GetSectorInfo(Unit, i, &SectorAddr, &SectorSize);
    if ((Addr >= SectorAddr) && (Addr < (SectorAddr + SectorSize))) {
      return i;
    }
  }
  return -1;
}

/*********************************************************************
*
*      _WriteOff
*
* Purpose:
*   This routine writes data into any section of the flash. It does not
*   check if this section has been previously erased; this is in the
*   responsibility of the user program.
*   Data written into multiple sectors at a time can be handled by this
*   routine.
* Return value:
*   0       O.K., data has been written
*   other   error
*/
static int _WriteOff(U8 Unit, U32 Off, const void * pSrc, U32 NumBytes) {
  unsigned NumItems;
  U32      Addr;
  PROPS *  pProps;

  pProps = _apProps[Unit];
  if (pProps) {
    NumItems = NumBytes >> 1;
    if (NumItems) {
      int SectorIndex;
      U32 SectorAddr;
      U32 SectorSize;

      SectorIndex = _Off2SectorIndex(Unit, Off);
      _GetSectorInfo(Unit, SectorIndex, &SectorAddr, &SectorSize);
      Addr = (pProps->StartAddrUsed + Off);
      SectorAddr += pProps->StartAddrUsed;
      if (_WriteData(Unit, pProps, SectorAddr, Addr, pSrc, NumItems)) {           /* 16 bit units */
        return 1;    /* Error */
      }
    }
    return 0;
  }
  return 1;    /* Error */
}

/*********************************************************************
*
*       _ReadOff
*
* Purpose:
*   Reads data from the given offset of the flash.
*/
static int _ReadOff(U8 Unit, void * pDest, U32 Off, U32 Len) {
  U32     Addr;
  PROPS * pProps;

  pProps = _apProps[Unit];
  if (pProps) {
    Addr = pProps->StartAddrUsed + Off;
    return pProps->pProgramHW->pfRead(Unit, pDest, Addr, Len) ;
  }
  return 1;    /* Error */
}

/*********************************************************************
*
*      _EraseSector
*
* Purpose:
*   Erases one sector
* Return value:
*   0       O.K., sector is ereased
*   other   error, sector may not be erased
*/
static int _EraseSector(U8 Unit, unsigned int SectorIndex) {
  int r;
  unsigned NumSectors;
  PROPS * pProps;
  U32 Off;

  r = -1;
  pProps = _apProps[Unit];
  if (pProps) {
    NumSectors = pProps->NumSectorsTotal;
    if (SectorIndex >= NumSectors) {
      return 1;
    }
    Off = _GetSectorOff(pProps, SectorIndex);
    if (pProps->pProgramHW) {
      r = pProps->pProgramHW->pfEraseSector(Unit, pProps->BaseAddr, (pProps->StartAddrUsed + Off));
    } else {
      FS_DEBUG_ERROROUT((FS_MTYPE_DRIVER, "NOR_PHY_CFI: Unsupported flash algo"));
      return 1;
    }
  }
  return r;
}

/*********************************************************************
*
*       _Configure
*
*  Description:
*    Configures a single instance of the driver
*/
static void _Configure(U8 Unit, U32 BaseAddr, U32 StartAddr, U32 NumBytes) {
  PROPS * pProps;

  _AllocIfRequired(Unit);

// TBD: Add parameter checks in debug build

  pProps = _apProps[Unit];
  if (pProps) {
    pProps->BaseAddr      = BaseAddr;
    pProps->StartAddrConf = StartAddr;
    pProps->NumBytes      = NumBytes;
    _Init(Unit, pProps);
  }
}

/*********************************************************************
*
*       _OnSelectPhy
*
*  Description:
*    Called right after selection of the physical layer
*/
static void _OnSelectPhy(U8 Unit, int NumChips, FS_NOR_READ_CFI_FUNC * pReadCFI) {
  PROPS * pProps;

  pProps = _AllocIfRequired(Unit);
  pProps->NumChips = (U8)NumChips;
  if (pProps->pReadCFI == NULL) {
    pProps->pReadCFI   = pReadCFI;
  }
}

/*********************************************************************
*
*       _OnSelectPhy1x16
*
*  Description:
*    Called right after selection of the single chip, 16-bit mode (1x16) physical layer
*/
static void _OnSelectPhy1x16(U8 Unit) {
  _OnSelectPhy(Unit, 1, FS_NOR_CFI_ReadCFI_1x16);
}

/*********************************************************************
*
*       _OnSelectPhy2x16
*
*  Description:
*    Called right after selection of the dual chip, 16-bit mode (2x16) physical layer
*/
static void _OnSelectPhy2x16(U8 Unit) {
  _OnSelectPhy(Unit, 2, FS_NOR_CFI_ReadCFI_2x16);
}

/*********************************************************************
*
*       _DeInit
*
*  Function description:
*    This function deinitialize or frees up memory resources that
*    are no longer needed.
*
*  Parameters:
*    Unit    - Physical unit.
*  
*/
static void _DeInit(U8 Unit) {
  FS_USE_PARA(Unit);
#if FS_SUPPORT_DEINIT
  FS_FREE(_apProps[Unit]);
  _apProps[Unit] = NULL;
#endif
}

/*********************************************************************
*
*      Public code
*
**********************************************************************
*/

/*********************************************************************
*
*       FS_NOR_CFI_SetReadCFIFunc
*
*  Description:
*    Called right after selection of the physical layer
*/
void FS_NOR_CFI_SetReadCFIFunc(U8 Unit, FS_NOR_READ_CFI_FUNC * pReadCFI) {
  PROPS * pProps;

  pProps = _AllocIfRequired(Unit);
  pProps->pReadCFI   = pReadCFI;
}




/*********************************************************************
*
*       Global data
*
**********************************************************************
*/
// 1 x 16-bit CFI compliant NOR flash
const FS_NOR_PHY_TYPE FS_NOR_PHY_CFI_1x16 = {
  _WriteOff,
  _ReadOff,
  _EraseSector,
  _GetSectorInfo,
  _GetNumSectors,
  _Configure,
  _OnSelectPhy1x16,
  _DeInit
};            

// 2 x 16-bit CFI compliant NOR flash
const FS_NOR_PHY_TYPE FS_NOR_PHY_CFI_2x16 = {
  _WriteOff,
  _ReadOff,
  _EraseSector,
  _GetSectorInfo,
  _GetNumSectors,
  _Configure,
  _OnSelectPhy2x16,
  _DeInit
};            

/*************************** End of file ****************************/
