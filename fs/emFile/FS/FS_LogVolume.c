/*********************************************************************
*                SEGGER MICROCONTROLLER GmbH & Co. KG                *
*        Solutions for real time microcontroller applications        *
**********************************************************************
*                                                                    *
*        (c) 2003-2012     SEGGER Microcontroller GmbH & Co KG       *
*                                                                    *
*        Internet: www.segger.com    Support:  support@segger.com    *
*                                                                    *
**********************************************************************

**** emFile file system for embedded applications ****
emFile is protected by international copyright laws. Knowledge of the
source code may not be used to write a similar product. This file may
only be used in accordance with a license and should not be re-
distributed in any way. We appreciate your understanding and fairness.
----------------------------------------------------------------------
File        : FS_LogVolume.c
Purpose     : Logical volume driver
---------------------------END-OF-HEADER------------------------------
*/

/*********************************************************************
*
*             #include Section
*
**********************************************************************
*/

#include "FS_Int.h"

/*********************************************************************
*
*       defines, configurable
*
**********************************************************************
*/
#ifdef FS_LOGVOL_MAXUNIT
  #define NUM_UNITS   FS_LOGVOL_MAXUNIT
#else
  #define NUM_UNITS   4
#endif

/*********************************************************************
*
*       typedefs
*
**********************************************************************
*/
typedef struct DEV_INFO DEV_INFO;

struct DEV_INFO {
  DEV_INFO             * pNext;
  const FS_DEVICE_TYPE * pDeviceType;
  U8                     DeviceUnit;
  U32                    StartOff;
  U32                    NumSectors;
  U32                    NumSectorsConf;
};

typedef struct {
  const char * sVolName;
  DEV_INFO   * pDevInfo;
  U16          BytesPerSector;
} LOGVOL_INST;

/*********************************************************************
*
*       Static data
*
**********************************************************************
*/
static LOGVOL_INST * _apInst[NUM_UNITS];
static int            _NumUnits;

/*********************************************************************
*
*       Macros, function replacement
*
**********************************************************************
*/

/*********************************************************************
*
*       ASSERT_UNIT_NO_IS_IN_RANGE
*/
#if FS_DEBUG_LEVEL >= FS_DEBUG_LEVEL_CHECK_ALL
  #define ASSERT_UNIT_NO_IS_IN_RANGE(Unit)                                \
  if (Unit >= NUM_UNITS) {                                                  \
    FS_DEBUG_ERROROUT((FS_MTYPE_DRIVER, "LOGVOL: Invalid unit number.\n")); \
    return -1;                                            \
  }
#else
  #define ASSERT_UNIT_NO_IS_IN_RANGE(Unit)
#endif

/*********************************************************************
*
*       Static code
*
**********************************************************************
*/

/*********************************************************************
*
*       _GetNumSectors
*
*/
static U32 _GetNumSectors(LOGVOL_INST * pInst) {
  U32 NumSectors;
  DEV_INFO * pDevInfo;
  
  NumSectors = 0;
  for (pDevInfo = pInst->pDevInfo; pDevInfo; pDevInfo = pDevInfo->pNext) {
    NumSectors += pDevInfo->NumSectors;
  }
  return NumSectors;
}

/*********************************************************************
*
*       _ReadApplyDeviceParas
*
*/
static int _ReadApplyDeviceParas(LOGVOL_INST * pInst) {
  DEV_INFO     * pDevInfo;
  int           r;
  FS_DEV_INFO    DevInfo;
  U32           NumSectorsConf;
  U32           NumSectorsDevice;
  U16           BytesPerSector;
  
  r = 0;                // Set to indicate success.
  BytesPerSector = 0;   // Number of bytes per sector not determined.
  for (pDevInfo = pInst->pDevInfo; pDevInfo; pDevInfo = pDevInfo->pNext) {
    const FS_DEVICE_TYPE * pDeviceType;
    U8 DeviceUnit;

    pDeviceType = pDevInfo->pDeviceType;
    DeviceUnit  = pDevInfo->DeviceUnit;
    //
    // Get info from device.
    //
    if (pDeviceType->pfIoCtl(DeviceUnit, FS_CMD_GET_DEVINFO, 0, (void*)&DevInfo)) {
      FS_DEBUG_ERROROUT((FS_MTYPE_DRIVER, "LOGVOL: Could not retrieve device information from device.\n"));
      r = 1;
      break;
    }
    NumSectorsConf   = pDevInfo->NumSectorsConf;
    NumSectorsDevice = DevInfo.NumSectors;
    //
    // When the number of configured sectors is 0 use the number of sectors reported by the device.
    //
    if (NumSectorsConf == 0) {
      NumSectorsConf = NumSectorsDevice;
    }
    //
    // Additional check to see if more sectors were configured as the number of sectors available. 
    // In this case use the number of sectors reported by the device and issue a warning.
    //
    if (NumSectorsDevice < NumSectorsConf) {
      FS_DEBUG_WARN((FS_MTYPE_DRIVER, "LOGVOL: Device has less sectors than requested. Using the number of sectors reported by device.\n"));
      NumSectorsConf = NumSectorsDevice;
    }
    //
    // For first device, set the number of bytes per sector. All add. devices added need to have the same sector size.
    //
    if (BytesPerSector == 0) {
      BytesPerSector = DevInfo.BytesPerSector;
    }
    if (BytesPerSector != DevInfo.BytesPerSector) {
      FS_DEBUG_ERROROUT((FS_MTYPE_DRIVER, "LOGVOL: Devices with different sector size can not be combined.\n"));
      r = 1;
      break;
    }
    pDevInfo->NumSectors = NumSectorsConf;
  }
  pInst->BytesPerSector = BytesPerSector;
  return r;
}

/*********************************************************************
*
*       _ReadApplyDeviceParasIfRequired
*
*/
static int _ReadApplyDeviceParasIfRequired(LOGVOL_INST * pInst) {
  int r;

  r = 0;      // No error so far.
  if (pInst->BytesPerSector == 0) {
    r = _ReadApplyDeviceParas(pInst);
  }
  return r;
}

/*********************************************************************
*
*       _ReadWrite
*/
static int _ReadWrite(LOGVOL_INST * pInst, U32 FirstSectorReq, const void * pBuffer, U32 NumSectorsReq, U8 IsWrite, U8 RepeatSame) {
  DEV_INFO * pDevInfo;
  int        r;
  U32        NumSectorsAtOnce;
  U32           NumSectorsUsed;

  r = -1;
  //
  // Iterate over devices until we have reached the last device or all data has been read.
  //
  pDevInfo = pInst->pDevInfo;
  while (pDevInfo) {
    NumSectorsUsed = pDevInfo->NumSectors;
    if (FirstSectorReq < NumSectorsUsed) {
      U32 SectorIndex;
      U8  DeviceUnit;
      const FS_DEVICE_TYPE * pDeviceType;

      NumSectorsAtOnce = MIN(NumSectorsUsed - FirstSectorReq, NumSectorsReq);
      pDeviceType = pDevInfo->pDeviceType;
      DeviceUnit  = pDevInfo->DeviceUnit;
      SectorIndex = FirstSectorReq + pDevInfo->StartOff;
      if (IsWrite) {
        r = pDeviceType->pfWrite(DeviceUnit, SectorIndex, pBuffer, NumSectorsAtOnce, RepeatSame);
      } else {
        r = pDeviceType->pfRead (DeviceUnit, SectorIndex, (void *)pBuffer, NumSectorsAtOnce);
      }
      if (r) {
        break;      // Error, read/write operation failed.
      }
      NumSectorsReq   -= NumSectorsAtOnce;
      FirstSectorReq  += NumSectorsAtOnce;
      {
        const U8 * p;

        p        = (const U8 *)pBuffer;
        p       += NumSectorsAtOnce * pInst->BytesPerSector;
        pBuffer  = p;
      }
    }
    FirstSectorReq -= NumSectorsUsed;
    if (NumSectorsReq == 0) {
      r = 0;                    // O.K., all sectors read
      break;
    }
    pDevInfo = pDevInfo->pNext;
  }
  return r;
}

/*********************************************************************
*
*       _FreeSectors
*/
static int _FreeSectors(LOGVOL_INST * pInst, U32 FirstSectorReq, U32 NumSectorsReq) {
  int r;
  U32 NumSectorsAtOnce;
  U32 NumSectorsUsed;
  DEV_INFO * pDevInfo;

  r = 1;      // Set to indicate an error.
  //
  // Iterate over all configured devices and call the "free sectors" function.
  //
  pDevInfo = pInst->pDevInfo;
  while (pDevInfo) {
    NumSectorsUsed = pDevInfo->NumSectors;
    if (FirstSectorReq < NumSectorsUsed) {
      U32 SectorIndex;
      U8  DeviceUnit;
      const FS_DEVICE_TYPE * pDeviceType;

      NumSectorsAtOnce = MIN(NumSectorsUsed - FirstSectorReq, NumSectorsReq);
      pDeviceType = pDevInfo->pDeviceType;
      DeviceUnit  = pDevInfo->DeviceUnit;
      SectorIndex = FirstSectorReq + pDevInfo->StartOff;
      r = pDeviceType->pfIoCtl(DeviceUnit, FS_CMD_FREE_SECTORS, (int)SectorIndex, &NumSectorsAtOnce);
      if (r) {
        break;      // Error, operation failed.
      }
      NumSectorsReq  -= NumSectorsAtOnce;
      FirstSectorReq += NumSectorsAtOnce;
    }
    FirstSectorReq -= NumSectorsUsed;
    if (NumSectorsReq == 0) {
      r = 0;        // OK, all sectors freed.
      break;
    }
    pDevInfo = pDevInfo->pNext;
  }
  return r;
}

/*********************************************************************
*
*       Public code (indirectly thru callback)
*
**********************************************************************
*/

/*********************************************************************
*
*       _LOGVOL_GetDriverName
*/
static const char * _LOGVOL_GetDriverName(U8 Unit) {
  const char * sVolName;
  if (_apInst[Unit]) {
    sVolName = _apInst[Unit]->sVolName;
  } else {
    sVolName = "";
  }
  return sVolName;
}

/*********************************************************************
*
*       _LOGVOL_AddDevice
*
*  Note:
*    No functionality required here.
*/
static int _LOGVOL_AddDevice(void) {
  return _NumUnits;
}

/*********************************************************************
*
*       _LOGVOL_Read
*/
static int _LOGVOL_Read(U8 Unit, U32 SectorIndex, void * pBuffer, U32 NumSectors) {
  int r;
  LOGVOL_INST * pInst;

  ASSERT_UNIT_NO_IS_IN_RANGE(Unit);
  pInst = _apInst[Unit];
  r = _ReadWrite(pInst, SectorIndex, pBuffer, NumSectors, 0, 0);    // Perform a read operation.
  return r;
}

/*********************************************************************
*
*       _LOGVOL_Write
*/
static int _LOGVOL_Write(U8 Unit, U32 SectorIndex, const void * pBuffer, U32 NumSectors, U8 RepeatSame) {
  int r;
  LOGVOL_INST * pInst;

  ASSERT_UNIT_NO_IS_IN_RANGE(Unit);
  pInst = _apInst[Unit];
  r = _ReadWrite(pInst, SectorIndex, (const void *)pBuffer, NumSectors, 1, RepeatSame);   // Perform a write operation.
  return r;
}

/*********************************************************************
*
*       _LOGVOL_IoCtl
*
*/
static int _LOGVOL_IoCtl(U8 Unit, I32 Cmd, I32 Aux, void * pBuffer) {
  LOGVOL_INST * pInst;
  FS_DEV_INFO * pInfo;
  DEV_INFO    * pDevInfo;
  int           r;
  U32           NumSectors; 
  U32           SectorIndex;

  ASSERT_UNIT_NO_IS_IN_RANGE(Unit);
  r     = -1;             // Set to indicate an error,
  pInst = _apInst[Unit];
  switch (Cmd) {
  case FS_CMD_GET_DEVINFO:
    r = _ReadApplyDeviceParasIfRequired(pInst);
    if (pBuffer && (r == 0)) {
    pInfo = (FS_DEV_INFO *)pBuffer;
      NumSectors = _GetNumSectors(pInst);
      if (NumSectors) {
    pInfo->NumSectors      = NumSectors;
        pInfo->BytesPerSector  = pInst->BytesPerSector;
    r                      = 0;
      }
    }
    break;
  case FS_CMD_DEINIT:
    pDevInfo = pInst->pDevInfo;
    while (pDevInfo) {
      DEV_INFO * pDevNext;

      pDevNext = pDevInfo->pNext;
      FS_Free(pDevInfo);
      pDevInfo = pDevNext;
    }
    FS_FREE(pInst);
    _apInst[Unit] = 0;  //-Q-
    _NumUnits--;
    r = 0;
    break;
  case FS_CMD_FREE_SECTORS:
    SectorIndex = (U32)Aux;
    NumSectors  = *(U32 *)pBuffer;
    r = _FreeSectors(pInst, SectorIndex, NumSectors);
    break;
  case FS_CMD_CLEAN_ONE:
    // thru
  case FS_CMD_CLEAN:
    // thru
  case FS_CMD_GET_SECTOR_USAGE:
    // TBD: Add support for these commands.
    break;
  case FS_CMD_UNMOUNT:
    // thru
  case FS_CMD_UNMOUNT_FORCED:
    pInst->BytesPerSector = 0;      // At next mount, force a read/apply of parameters from device.
    // thru
  default:
    pDevInfo = pInst->pDevInfo;
    r        = 0;
    while (pDevInfo) {      
      int DriverReturn;
      U8  DeviceUnit;
      const FS_DEVICE_TYPE * pDeviceType;

      pDeviceType  = pDevInfo->pDeviceType;
      DeviceUnit   = pDevInfo->DeviceUnit;
      DriverReturn = pDeviceType->pfIoCtl(DeviceUnit, Cmd, Aux, pBuffer);
      if (DriverReturn != 0) {
        r = DriverReturn;
      }
      pDevInfo = pDevInfo->pNext;
    }
    break;
  }
  return r;
}

/*********************************************************************
*
*       _LOGVOL_InitMedium
*
*/
static int _LOGVOL_InitMedium(U8 Unit) {
  DEV_INFO    * pDevInfo;
  int           r;
  LOGVOL_INST * pInst;

  ASSERT_UNIT_NO_IS_IN_RANGE(Unit);
  pInst = _apInst[Unit];
  for (pDevInfo = pInst->pDevInfo; pDevInfo; pDevInfo = pDevInfo->pNext) {
    const FS_DEVICE_TYPE * pDeviceType;
    U8 DeviceUnit;

    pDeviceType = pDevInfo->pDeviceType;
    DeviceUnit  = pDevInfo->DeviceUnit;
    if (pDeviceType->pfInitMedium) {
      r = pDeviceType->pfInitMedium(DeviceUnit);
      if (r) {
        return 1;   // Error, failed to initialize the device.
      }
    }
  }
  return 0;
}

/*********************************************************************
*
*       _LOGVOL_GetStatus
*
*/
static int _LOGVOL_GetStatus(U8 Unit) {
  DEV_INFO    * pDevInfo;
  LOGVOL_INST * pInst;
  int           Status;

  ASSERT_UNIT_NO_IS_IN_RANGE(Unit);
  pInst  = _apInst[Unit];
  Status = FS_MEDIA_IS_PRESENT;
  for (pDevInfo = pInst->pDevInfo; pDevInfo; pDevInfo = pDevInfo->pNext) {
    const FS_DEVICE_TYPE * pDeviceType;
    U8  DeviceUnit;
    int DeviceStatus;

    pDeviceType  = pDevInfo->pDeviceType;
    DeviceUnit   = pDevInfo->DeviceUnit;
    DeviceStatus = pDeviceType->pfGetStatus(DeviceUnit);
    if (DeviceStatus == FS_MEDIA_NOT_PRESENT) {
      Status = DeviceStatus;
      break;          // Error, the device is not available.
  }
  }
  return Status;
}

/*********************************************************************
*
*       _LOGVOL_GetNumUnits
*
*/
static int _LOGVOL_GetNumUnits(void) {
  return _NumUnits;
}

/*********************************************************************
*
*       Public code, internal
*
**********************************************************************
*/

/*********************************************************************
*
*       FS_LOGVOL_AddDevice
*
*/
static const FS_DEVICE_TYPE _LOGVOL_Driver = {
  _LOGVOL_GetDriverName,
  _LOGVOL_AddDevice,
  _LOGVOL_Read,
  _LOGVOL_Write,
  _LOGVOL_IoCtl,
  _LOGVOL_InitMedium,
  _LOGVOL_GetStatus,
  _LOGVOL_GetNumUnits
};

/*********************************************************************
*
*       Public code
*
**********************************************************************
*/

/*********************************************************************
*
*       FS_LOGVOL_Create
*
*/
int FS_LOGVOL_Create(const char * sVolName) {
  int r;
  LOGVOL_INST   * pLogVol;
  FS_VOLUME * pVolume;

  FS_LOCK();
  r = -1;
  if (_NumUnits < NUM_UNITS) {
    pVolume = FS__AddDevice(&_LOGVOL_Driver);
    if (pVolume) {
      pLogVol = (LOGVOL_INST *)FS_AllocZeroed(sizeof(LOGVOL_INST));
      if (pLogVol) {
        pLogVol->sVolName = sVolName;
        _apInst[_NumUnits++] = pLogVol;
        r = 0;
      }
    }
  }
  FS_UNLOCK();
  return r;
}

/*********************************************************************
*
*       FS_LOGVOL_AddDevice
*
*/
int FS_LOGVOL_AddDevice(const char * sLogVolName, const FS_DEVICE_TYPE * pDeviceType, U8 DeviceUnit, U32 StartOff, U32 NumSectors) {
  int       i;
  int       r;
  LOGVOL_INST * pInst;

  FS_LOCK();
  r = -1;
  //
  // Find log volume and store the parameters.
  //
  for (i = 0; i < _NumUnits; i++) {
    pInst = _apInst[i];
    if (strcmp(sLogVolName, pInst->sVolName) == 0) {
      DEV_INFO             * pDevInfo;
      DEV_INFO             ** ppPrevNext;
      FS_DEV_INFO            DevInfo;

      FS_MEMSET(&DevInfo, 0, sizeof(DevInfo));
      //
      // A new item is inserted an the end of the list.
      //
      ppPrevNext = &pInst->pDevInfo;
      for (; pDevInfo = *ppPrevNext, pDevInfo; ppPrevNext = &pDevInfo->pNext);
      //
      // Alloc memory for new device, fill it in and add it to the linked list
      //
      pDevInfo    = (DEV_INFO *)FS_AllocZeroed(sizeof(DEV_INFO));
      pDevInfo->NumSectorsConf = NumSectors;
      pDevInfo->StartOff   = StartOff;
      pDevInfo->pDeviceType    = pDeviceType;
      pDevInfo->DeviceUnit     = DeviceUnit;
      *ppPrevNext = pDevInfo;
      r = 0;
      break;
    }
  }
  FS_UNLOCK();
  return r;
}

/*************************** End of file ****************************/

