/*********************************************************************
*                SEGGER MICROCONTROLLER GmbH & Co. KG                *
*        Solutions for real time microcontroller applications        *
**********************************************************************
*                                                                    *
*        (c) 2003-2012     SEGGER Microcontroller GmbH & Co KG       *
*                                                                    *
*        Internet: www.segger.com    Support:  support@segger.com    *
*                                                                    *
**********************************************************************

**** emFile file system for embedded applications ****
emFile is protected by international copyright laws. Knowledge of the
source code may not be used to write a similar product. This file may
only be used in accordance with a license and should not be re-
distributed in any way. We appreciate your understanding and fairness.
----------------------------------------------------------------------
File        : NAND_PHY_DataFlash.c
Purpose     : Atmel DATAFLASH physical layer access
---------------------------END-OF-HEADER------------------------------
*/

/*********************************************************************
*
*             #include Section
*
**********************************************************************
*/
#include "FS_Int.h"
#include "FS_CLib.h"
#include "FS_Debug.h"
#include "FS_DF_X_HW.h"

/*********************************************************************
*
*       defines, configurable
*
**********************************************************************
*/
#ifdef FS_NAND_MAXUNIT
  #define NUM_UNITS   FS_NAND_MAXUNIT
#else
  #define NUM_UNITS   4
#endif

#ifndef   FS_NAND_USE_LARGE_BLOCKS
  #define FS_NAND_USE_LARGE_BLOCKS  1   // If set to 1 the "sector erase" command is used. On Atmel DataFlashes sectors are the biggest erasable units (comparable to NAND-blocks).
#endif

/*********************************************************************
*
*       defines, not configurable
*
**********************************************************************
*/
/*******     Status                        **************************/
#define STATUS_ERROR            0x01    /* 0:Pass,          1:Fail */
#define STATUS_READY            0x40    /* 0:Busy,          1:Ready */
#define STATUS_WRITE_PROTECTED  0x80    /* 0:Protect,       1:Not Protect */

/*******     Commands                      **************************/
#define  READ_STATUS                   0xd7
#define  PAGE_TO_BUFFER                0x53
#define  BUFFER_READ                   0x54
#define  BUFFER_READ_SERIAL            0xD4
#define  WRITE_TO_BUFFER               0x84
#define  BUFFER_TO_PAGE_WITHOUT_ERASE  0x88
#define  BLOCK_ERASE                   0x50
#define  SECTOR_ERASE                  0x7C

/*******     Serial Flash specific data    **************************/
#define COMMAND_LENGTH    0x04

/*******     Serial Flash types            **************************/
#define  FLASH_1MBIT      0x03
#define  FLASH_2MBIT      0x05
#define  FLASH_4MBIT      0x07
#define  FLASH_8MBIT      0x09
#define  FLASH_16MBIT     0x0b
#define  FLASH_32MBIT     0x0d
#define  FLASH_64MBIT     0x0f
#define  FLASH_128MBIT    0x04

/*********************************************************************
*
*       Types
*
**********************************************************************
*/
typedef struct {
  U16  ldBytesPerPage;
  U16  BytesPerPageData;
  U16  BytesPerPageSpare;
  U16  PagesPerBlock;
  char IsInited;
  U8   ReadBufferCmd;
} DF_INST;

/*********************************************************************
*
*       Static data
*
**********************************************************************
*/
static DF_INST _aInst[NUM_UNITS];

/*********************************************************************
*
*       Static code
*
**********************************************************************
*/

/*********************************************************************
*
*       _ld
*/
static U16 _ld(U32 Value) {
  U16 i;

  for (i = 0; i < 16; i++) {
    if ((1UL << i) == Value) {
      break;
    }
  }
  return i;
}

/*********************************************************************
*
*       _SendDummyBytes
*
*  Function description
*/
static void _SendDummyBytes(U8 Unit, int NumBytes) {
  U32 DummyData;

  DummyData = 0xffffffff;
  while (NumBytes >= 4) {
    FS_DF_HW_X_Write(Unit, (U8 *)&DummyData, 4);
    NumBytes -= 4;
  } 
  if (NumBytes) {
    FS_DF_HW_X_Write(Unit, (U8 *)&DummyData, NumBytes);
  }
}

/*********************************************************************
*
*       _SendCommandPara
*
*  Description:
*    Sends a command with an additional parameter to DataFlash.
*
*  Parameters:
*    Unit       - Specifies which DataFlash unit
*    Command    - Command that shall be sent.
*    Para       - Additional parameter to the command.
*    CSHandling - _SendCommand shall take care of CS handling.
*  
*/
static void _SendCommandPara(U8 Unit, U8 Command, U32 Para, int CSHandling) {
  U8 aData[COMMAND_LENGTH];

  aData[0] = Command;
  aData[1] = (U8)((Para >> 16) & 0xff);
  aData[2] = (U8)((Para >>  8) & 0xff);
  aData[3] = (U8)( Para        & 0xff);
  if (CSHandling) {
    FS_DF_HW_X_EnableCS(Unit);
  }
  FS_DF_HW_X_Write(Unit, aData, COMMAND_LENGTH);
  if (CSHandling) {
    FS_DF_HW_X_DisableCS(Unit);
  }
}

/*********************************************************************
*
*       _SendCommand
*
*  Description:
*    Sends a simple command to DataFlash.
*
*  Parameters:
*    Unit       - Specifies which DataFlash unit
*    Command    - Command that shall be sent.
*    CSHandling - _SendCommand shall take care of CS handling.
*  
*  Return value:
*    Answer of the DataFlash to the command.
*
*/
static U8 _SendCommand(U8 Unit, U8 Command, int CSHandling) {
  U8 r;

  if (CSHandling) {
    FS_DF_HW_X_EnableCS(Unit);
  }
  FS_DF_HW_X_Write(Unit, &Command, 1);
  FS_DF_HW_X_Read(Unit,  &r,       1);
  if (CSHandling) {
    FS_DF_HW_X_DisableCS(Unit);
  }
  return r;
}

/*********************************************************************
*
*       _WaitUntilReady
*
*  Description:
*    Waits until the DataFlash unit is ready.
*
*  Parameters:
*    Unit     - Specifies which DataFlash unit
*  
*/
static void _WaitUntilReady(U8 Unit) {
  U8 Status;

  do {
    Status = _SendCommand(Unit, READ_STATUS, 1);
    if ((Status & (1 << 7)) == (1 << 7)) {
      break;
    }
  } while(1);
}

/*********************************************************************
*
*       _ReadFromBuffer
*
*  Description:
*    Read data from the internal buffer of DataFlash.
*
*  Parameters:
*    Unit     - Specifies which DataFlash unit
*    Off      - Off within the buffer
*    p        - Pointer to a buffer to store the data
*    NumBytes - Number of byte to read
*
*/
static void _ReadFromBuffer(U8 Unit, unsigned Off, U8 * p, unsigned NumBytes) {
  FS_DF_HW_X_EnableCS(Unit);

  _SendCommandPara(Unit, _aInst[Unit].ReadBufferCmd, Off, 0);
  _SendDummyBytes(Unit, 1);
  FS_DF_HW_X_Read(Unit, p, NumBytes);
  FS_DF_HW_X_DisableCS(Unit);
  _WaitUntilReady(Unit);
}

/*********************************************************************
*
*       _WriteToBuffer
*
*  Description:
*    Writes data to the internal buffer of DataFlash.
*
*  Parameters:
*    Unit     - Specifies which DataFlash unit
*    Off      - Off within the buffer
*    p        - Pointer to a buffer to store the data
*    NumBytes - Number of byte to read
*
*/
static void _WriteToBuffer(U8 Unit, unsigned Off, const U8 * p, unsigned NumBytes) {
  FS_DF_HW_X_EnableCS(Unit);
  _SendCommandPara(Unit, WRITE_TO_BUFFER, Off, 0);
  FS_DF_HW_X_Write(Unit, p, NumBytes);
  FS_DF_HW_X_DisableCS(Unit);
  _WaitUntilReady(Unit);
}

/*********************************************************************
*
*       _ReadData
*
*  Description:
*    Reads data from DataFlash.
*    Typically this function is called to read either data from the
*    data area or from spare area or both.
*    
*
*  Parameters:
*    Unit          - Specifies which DataFlash unit
*    PageNo        - Page that shall be read
*    pData         - Pointer to a buffer to store the data in - May be NULL if not needed
*    OffData       - Offset with the page to read from
*    NumBytesData  - Number of byte to read from data area
*    pSpare        - Pointer to a buffer to store the spare area data  - May be NULL if not needed
*    OffSpare      - Offset with the page to read from
*    NumBytesSpare - Number of bytes to read from spare area
*
*/
static void _ReadData(U8 Unit, U32 PageNo, void * pData, unsigned OffData, unsigned NumBytesData, void * pSpare, unsigned OffSpare, unsigned NumBytesSpare) {
  U32       Addr;
  U8      * pData8;
  U8      * pSpare8;
  DF_INST * pInst;
  unsigned  NumBytesAtOnce;
  unsigned  Off;
  unsigned  MaxOffData;
  unsigned  MaxOffSpare;


  pInst     = &_aInst[Unit];
  pData8    = (U8 *)pData;
  pSpare8   = (U8 *)pSpare;
  //
  // In case we have a 256 + 8 page device
  // we need to handle this correctly
  //
  if (pInst->ldBytesPerPage == 8) {
    PageNo <<= 1;
    MaxOffData  = 512;
    MaxOffSpare =  16;
  } else {
    MaxOffData  = pInst->BytesPerPageData;
    MaxOffSpare = pInst->BytesPerPageSpare;
  }
  //
  //  Read data from the flash and copy 
  //  the data to the 
  //
  while (1) {
    Addr = PageNo << (pInst->ldBytesPerPage + 1);
    _SendCommandPara(Unit, PAGE_TO_BUFFER, Addr, 1);
    _WaitUntilReady(Unit);
    if (pData) {
      Off = OffData & (MaxOffData - 1);
      if ((Off < pInst->BytesPerPageData) && NumBytesData) {
        NumBytesAtOnce = MIN(NumBytesData, pInst->BytesPerPageData);
        _ReadFromBuffer(Unit, OffData, pData8, NumBytesAtOnce);
        NumBytesData -= NumBytesAtOnce;
        pData8       += NumBytesAtOnce;
      }
    }
    if (pSpare) {
      Off = OffSpare & (MaxOffSpare - 1);
      if ((Off < pInst->BytesPerPageSpare) && NumBytesSpare) {
        if (pInst->ldBytesPerPage == 8) {
          Off = pInst->BytesPerPageData + Off;
        } else {
          Off = OffSpare;
        }
        NumBytesAtOnce = MIN(NumBytesSpare, pInst->BytesPerPageSpare);
        _ReadFromBuffer(Unit, Off, pSpare8, NumBytesAtOnce);
        NumBytesSpare -= NumBytesAtOnce;
        pSpare8       += NumBytesAtOnce;
      }
    }
    PageNo++;
    if ((NumBytesData == 0) && (NumBytesSpare == 0)) {
      break;
    }
  }
  
}

/*********************************************************************
*
*       _WriteData
*
*  Description:
*    Writes data to DataFlash.
*
*  Parameters:
*    Unit          - Specifies which DataFlash unit
*    PageNo        - Page that shall be read
*    pData         - Pointer to a buffer that holds the data area data
*    OffData       - Offset with the page to write to
*    NumBytesData  - Number of byte to write to data area
*    pSpare        - Pointer to a buffer that holds the spare area data 
*    OffSpare      - Offset with the page to write to
*    NumBytesSpare - Number of bytes to write to spare area
*
*/
static void _WriteData(U8 Unit, U32 PageNo, const void * pData, unsigned OffData, unsigned NumBytesData, const void * pSpare, unsigned OffSpare, unsigned NumBytesSpare) {
  U32        Addr;
  const U8 * pData8;
  const U8 * pSpare8;
  DF_INST  * pInst;
  unsigned   NumBytesAtOnce;
  unsigned   Off;
  unsigned   MaxOffData;
  unsigned   MaxOffSpare;

  pInst     = &_aInst[Unit];
  pData8    = (const U8 *)pData;
  pSpare8   = (const U8 *)pSpare;
  //
  // In case we have a 256 + 8 page device
  // we need to handle this correctly
  //
  if (pInst->ldBytesPerPage == 8) {
    PageNo     <<= 1;
    MaxOffData   = 512;
    MaxOffSpare  =  16;
  } else {
    MaxOffData  = pInst->BytesPerPageData;
    MaxOffSpare = pInst->BytesPerPageSpare;
  }
  do {
    Addr = PageNo << (pInst->ldBytesPerPage + 1);
    //
    //  Read the data into the internal buffer of the DataFlash
    //
    _SendCommandPara(Unit, PAGE_TO_BUFFER, Addr, 1);
    _WaitUntilReady(Unit);
    //
    //  Modify the data
    //
    if (pData) {
      Off = OffData & (MaxOffData - 1);
      if ((Off < pInst->BytesPerPageData) && NumBytesData) {
        NumBytesAtOnce = MIN(NumBytesData, pInst->BytesPerPageData);
        _WriteToBuffer(Unit, OffData, pData8, NumBytesAtOnce);
        NumBytesData -= NumBytesAtOnce;
        pData8       += NumBytesAtOnce;
      }
    }
    if (pSpare) {
      Off = OffSpare & (MaxOffSpare - 1);
      if ((Off < pInst->BytesPerPageSpare) && NumBytesSpare) {
        if (pInst->ldBytesPerPage == 8) {
          Off = pInst->BytesPerPageData + Off;
        } else {
          Off = OffSpare;
        }
        NumBytesAtOnce = MIN(NumBytesSpare, pInst->BytesPerPageSpare);
        _WriteToBuffer(Unit, Off, pSpare8, NumBytesAtOnce);
        NumBytesSpare -= NumBytesAtOnce;
        pSpare8       += NumBytesAtOnce;
      }
    }
    //
    //  Write the data back to page
    //
    _SendCommandPara(Unit, BUFFER_TO_PAGE_WITHOUT_ERASE, Addr, 1);
    _WaitUntilReady(Unit);
    PageNo++;
    if ((NumBytesData == 0) && (NumBytesSpare == 0)) {
      break;
    }
  } while (1);
}

/*********************************************************************
*
*       _Read
*
*  Function description
*    Reads data from a complete or a part of a page.
*    This function is used to read either main memory or spare area.
*
*  Return value:
*      0                       - Data successfully transferred.
*    !=0                       - An error has occurred.
*/
static int _Read(U8 Unit, U32 PageNo, void * pBuffer, unsigned Off, unsigned NumBytes) {
  unsigned   OffSpare;
  DF_INST  * pInst;

  pInst     = &_aInst[Unit];
  //
  // Check first what kind of data flash we have,
  // For a small page data (256 bytes per page), the offset for spare is 512, meaning
  // 2 pages combine 1 page for the upper layer.
  // Otherwise the spare area is after data area
  //
  if (pInst->ldBytesPerPage == 8) {
    OffSpare = 512;
  } else {
    OffSpare = pInst->BytesPerPageData;
  }
  if (Off < OffSpare) {
    _ReadData(Unit, PageNo, pBuffer, Off, NumBytes, NULL, 0, 0);
  } else {    
    _ReadData(Unit, PageNo, NULL, 0, 0, pBuffer, Off, NumBytes);
  }
  return 0;
}

/*********************************************************************
*
*       _ReadEx
*
*  Function description
*    Reads data from 2 parts of a page.
*    Typically used to read data and spare area at the same time.
*
*  Return value:
*      0                       - Data successfully transferred.
*    !=0                       - An error has occurred.
*/
static int _ReadEx(U8 Unit, U32 PageNo, void * pBuffer0, unsigned Off0, unsigned NumBytes0, void * pBuffer1, unsigned Off1, unsigned NumBytes1) {
  _ReadData(Unit, PageNo, pBuffer0, Off0, NumBytes0, pBuffer1, Off1, NumBytes1);
  return 0;
}


/*********************************************************************
*
*       _Write
*
*  Function description
*    Writes data into a complete or a part of a page.
*    This code is identical for main memory and spare area; the spare area
*    is located rigth after the main area.
*
*  Return value:
*      0                       - Data occurred transferred.
*    !=0                       - An error has occurred.
*/
static int _Write(U8 Unit, U32 PageNo, const void * pBuffer, unsigned Off, unsigned NumBytes) {
  unsigned   OffSpare;
  DF_INST  * pInst;

  pInst     = &_aInst[Unit];
  //
  // Check first what kind of data flash we have,
  // For a small page data (256 bytes per page), the offset for spare is 512, meaning
  // 2 pages combine 1 page for the upper layer.
  // Otherwise the spare area is after data area
  //
  if (pInst->ldBytesPerPage == 8) {
    OffSpare = 512;
  } else {
    OffSpare = pInst->BytesPerPageData;
  }
  if (Off < OffSpare) {
    _WriteData(Unit, PageNo, pBuffer, Off, NumBytes, NULL, 0, 0);
  } else {
    _WriteData(Unit, PageNo, NULL, 0, 0, pBuffer, Off, NumBytes);
  }

  return 0;
}

/*********************************************************************
*
*       _WriteEx
*
*  Function description
*    Writes data to 2 parts of a page.
*    Typically used to write data and spare area at the same time.
*
*  Return value:
*      0                       - Data successfully transferred.
*    !=0                       - An error has occurred.
*/
static int _WriteEx(U8 Unit, U32 PageNo, const void * pBuffer0, unsigned Off0, unsigned NumBytes0, const void * pBuffer1, unsigned Off1, unsigned NumBytes1) {
  _WriteData(Unit, PageNo, pBuffer0, Off0, NumBytes0, pBuffer1, Off1, NumBytes1);
  return 0;
}

#if FS_NAND_USE_LARGE_BLOCKS
/*********************************************************************
*
*       _EraseBlock
*
*  Function description
*    Erases a single block of the DataFlash.
*    On DataFlash devices there are 3 different erase sizes which can be used:
*    Pagewise   -> One page is erased (The size of one page depends on the device density)
*    Blockwise  -> One block is erased. A block consists of 8 pages
*    Sectorwise -> One sector is erased. A sector consists of multiple blocks (How many blocks build a sector depends on the device density)
*
*    We use the erase-sector command in order to erase a DataFlash sector, since on Atmel DataFlashes cumulative erasing/programming
*    actions within one sector has an influence on the data of other pages within the same sector.
*
*    \\fileserver\techinfo\Company\Atmel\DataFLASH\AT45DB161D_doc3500.pdf
*    11.3 AutoPage Rewrite
*    Each page within a sector must be updated/rewritten at least once
*    within every 10,000 cumulative page erase/program operations in that sector.
*
*  Parameters
*    IndexFirstPage    Index of first page in the block that shall be erased
*
*  Return value:
*      0                       - Data successfully transferred.
*      1                       - An error has occurred.
*/
static int _EraseBlock(U8 Unit, U32 IndexFirstPage) {
  DF_INST * pInst;
  U32 Addr;

  pInst = &_aInst[Unit];
  //
  // Block 0 needs special treatment, since it divided into 2 sectors 0a (8 pages) and 0b (248 pages)
  //
  Addr = IndexFirstPage << (pInst->ldBytesPerPage + 1);
  if (Addr == 0) {
    _SendCommandPara(Unit, SECTOR_ERASE, 0, 1);
    _WaitUntilReady(Unit);
    _SendCommandPara(Unit, SECTOR_ERASE, 1 << (pInst->ldBytesPerPage + 4), 1);
    _WaitUntilReady(Unit);
  } else {
    _SendCommandPara(Unit, SECTOR_ERASE, Addr, 1);
    _WaitUntilReady(Unit);
  }
  return 0;
}

/*********************************************************************
*
*       _InitGetDeviceInfo
*
*  Function description
*    Initializes hardware layer, resets NAND flash and tries to identify the NAND flash.
*    If the NAND flash can be handled, Device Info is filled.
*
*  Return value:
*      0                       - O.K., device can be handled
*      1                       - Error: device can not be handled
*/
static int _InitGetDeviceInfo(U8 Unit, FS_NAND_DEVICE_INFO * pDevInfo) {
  U8        Status = 0;
  U16       NumBlocks;
  U16       BytesPerPage;
  U16       PagesPerBlock;
  DF_INST * pInst;

  pInst = &_aInst[Unit];
  FS_DF_HW_X_Init(Unit);
  Status = _SendCommand(Unit, READ_STATUS, 1);
  //
  // When bit 0 of status register is set to 1,
  // then flash does not have a spare area.
  //
  // In the following the original PagesPerBlock and NumBlocks of Atmel data-flashes
  // have been modified in order to reduce maintenance effort.
  // For example on the 32 MBit devices a block contains of 8 pages and the whole device contains 1024 blocks.
  // Since in emFile block-wise management is done and many small blocks generate much maintenance effort (and needs a lot of RAM to hold management data for each block),
  // we decided to merge 4 real blocks to one, for emFile block-management.
  // The underlying functions such as _EraseBlock() are designed to deal with this merged blocks, so no customer-specific adaption is necessary.
  //
  if (Status & 1) {
    FS_DEBUG_ERROROUT((FS_MTYPE_DRIVER, "Dataflash in \"Power of 2\" mode (with no spare area) is not supported."));
    return 1;
  }
  pInst->ReadBufferCmd = BUFFER_READ;
  Status = (Status >> 2) & 0x0f;
  switch (Status) {
  case FLASH_1MBIT:
    BytesPerPage  = 256;
    PagesPerBlock = 256;      // 64-KB sectors
    NumBlocks     =   2;      // 128-KB Total
    break;
  case FLASH_2MBIT:
    BytesPerPage  = 256;
    PagesPerBlock = 256;      // 64-KB "sectors"
    NumBlocks     =   4;      // 256-KB Total
    break;
  case FLASH_4MBIT:
    BytesPerPage  = 256;
    PagesPerBlock = 256;      // 64-KB "sectors"
    NumBlocks     =   8;      // 512-KB Total
    break;
  case FLASH_8MBIT:
    BytesPerPage  = 256;
    PagesPerBlock = 256;      // 64-KB "sectors"
    NumBlocks     =  16;      // 1-MB Total
    break;
  case FLASH_16MBIT:
    BytesPerPage  = 512;
    PagesPerBlock = 256;      // 128-KB "sectors"
    NumBlocks     =  16;      // 2-MB Total
    break;
  case FLASH_32MBIT:
    BytesPerPage   = 512;
    PagesPerBlock  = 128;     // 64-KB "sectors"
    NumBlocks      =  64;     // 4-MB total
    break;
  case FLASH_64MBIT:
    BytesPerPage   = 1024;
    PagesPerBlock  =  256;    // 256-KB "sectors"
    NumBlocks      =   32;    // 8-MB total
    pInst->ReadBufferCmd = BUFFER_READ_SERIAL;
    break;
  case FLASH_128MBIT:
    BytesPerPage   = 1024;
    PagesPerBlock  =   32;
    NumBlocks      =  512;
    pInst->ReadBufferCmd = BUFFER_READ_SERIAL;
    break;
  default:
    return 1;
  }
  pInst->ldBytesPerPage    = _ld(BytesPerPage);
  pInst->PagesPerBlock     = PagesPerBlock;
  pInst->BytesPerPageData  = BytesPerPage;
  pInst->BytesPerPageSpare = BytesPerPage >> 5;   // Spare area size is always: PageSize in bytes / 32
  if (pDevInfo) {
    pDevInfo->BPP_Shift      = (U8)(BytesPerPage > 512 ? _ld(BytesPerPage) : 9);   // Upper layer (NAND driver) requires a minimum page size of 512. This layer needs to treat 2 256-byte pages as 1 512-byte page for small data flashes
    pDevInfo->NumBlocks      = NumBlocks;
    pDevInfo->PPB_Shift      = (U8)_ld(PagesPerBlock);
  }
  return 0;
}
#else
/*********************************************************************
*
*       _EraseBlock
*
*  Function description
*    Erases a block.
*
*  Return value:
*      0                       - Data successfully transferred.
*      1                       - An error has occurred.
*/
static int _EraseBlock(U8 Unit, U32 Block) {
  unsigned ldBytesPerPage;
  unsigned i;
  DF_INST * pInst;

  pInst = &_aInst[Unit];
  ldBytesPerPage = pInst->ldBytesPerPage;
  if (ldBytesPerPage == 8) {
    Block <<= 1;
  }
  // 
  // As there is no real block erase that we can use.
  //
  for (i = 0; i < (unsigned)(pInst->PagesPerBlock >> 3); i++) {
    U32 Addr;
    
    Addr = (Block + (i << 3)) << (ldBytesPerPage + 1);
    _SendCommandPara(Unit, BLOCK_ERASE, Addr, 1);
    _WaitUntilReady(Unit);
  }
  return 0;
}

/*********************************************************************
*
*       _InitGetDeviceInfo
*
*  Function description
*    Initializes hardware layer, resets NAND flash and tries to identify the NAND flash.
*    If the NAND flash can be handled, Device Info is filled.
*
*  Return value:
*      0                       - O.K., device can be handled
*      1                       - Error: device can not be handled
*/
static int _InitGetDeviceInfo(U8 Unit, FS_NAND_DEVICE_INFO * pDevInfo) {
  U8        Status = 0;
  U16       NumBlocks;
  U16       BytesPerPage;
  U16       BytesPerSector;
  U16       PagesPerBlock;
  DF_INST * pInst;

  pInst = &_aInst[Unit];
  BytesPerSector = 512;
  FS_DF_HW_X_Init(Unit);
  Status = _SendCommand(Unit, READ_STATUS, 1);
  //
  // When bit 0 of status register is set to 1, 
  // then flash does not have a spare area.
  //
  // In the following the original PagesPerBlock and NumBlocks of Atmel data-flashes
  // have been modified in order to reduce maintenance effort.
  // For example on the 32 MBit devices a block contains of 8 pages and the whole device contains 1024 blocks.
  // Since in emFile block-wise management is done and many small blocks generate much maintenance effort (and needs a lot of RAM to hold management data for each block),
  // we decided to merge 4 real blocks to one, for emFile block-management.
  // The underlying functions such as _EraseBlock() are designed to deal with this merged blocks, so no customer-specific adaption is necessary.
  //
  if (Status & 1) {
    FS_DEBUG_ERROROUT((FS_MTYPE_DRIVER, "Dataflash in \"Power of 2\" mode (with no spare area) is not supported."));
    return 1;
  }
  pInst->ReadBufferCmd = BUFFER_READ;
  Status = (Status >> 2) & 0x0f;
  switch (Status) {
  case FLASH_1MBIT:
    BytesPerPage  = 256;
    PagesPerBlock = 64;
    NumBlocks     = 8;  
    break;
  case FLASH_2MBIT:
    BytesPerPage  = 256;
    PagesPerBlock = 64;
    NumBlocks     = 16;  
    break;
  case FLASH_4MBIT:
    BytesPerPage  = 256;
    PagesPerBlock = 64;
    NumBlocks     = 32;  
    break;
  case FLASH_8MBIT:
    BytesPerPage  = 256;
    PagesPerBlock = 64;
    NumBlocks     = 64;  
    break;
  case FLASH_16MBIT:
    BytesPerPage  = 512;
    PagesPerBlock = 32;
    NumBlocks     = 128;  
    break;
  case FLASH_32MBIT:
    BytesPerPage   = 512;
    PagesPerBlock  = 32;
    NumBlocks      = 256;  
    break;
  case FLASH_64MBIT:
    BytesPerPage   = 1024;
    BytesPerSector = 1024;
    PagesPerBlock  = 32;
    NumBlocks      = 256;
    pInst->ReadBufferCmd = BUFFER_READ_SERIAL;
    break; 
  case FLASH_128MBIT:
    BytesPerPage   = 1024;
    BytesPerSector = 1024;
    PagesPerBlock  = 32;
    NumBlocks      = 512;  
    pInst->ReadBufferCmd = BUFFER_READ_SERIAL;
    break;
  default:
    return 1;
  }
  pInst->ldBytesPerPage    = _ld(BytesPerPage);
  pInst->PagesPerBlock     = PagesPerBlock;
  pInst->BytesPerPageData  = BytesPerPage;
  pInst->BytesPerPageSpare = BytesPerPage >> 5;
  if (pDevInfo) {
    pDevInfo->BPP_Shift      = (U8)_ld(BytesPerSector);
    pDevInfo->NumBlocks      = NumBlocks;
    pDevInfo->PPB_Shift      = 5;  // 32 pages per block
  }
  return 0;
}
#endif  // FS_NAND_USE_LARGE_BLOCKS

/*********************************************************************
*
*       _IsWP
*
*  Function description
*    Checks if the device is write protected.
*    This is done by reading bit 7 of the status register.
*    Typical reason for write protection is that either the supply voltage is too low
*    or the /WP-pin is active (low)
*
*  Return value:
*     <0          - Error
*      0          - Not write protected
*     >0          - Write protected
*/
static int _IsWP(U8 Unit) {
  FS_USE_PARA(Unit);
  return 0;
}

/*********************************************************************
*
*       Public const
*
**********************************************************************
*/
const FS_NAND_PHY_TYPE FS_NAND_PHY_DataFlash = {
  _EraseBlock,
  _InitGetDeviceInfo,
  _IsWP,
  _Read,
  _ReadEx,
  _Write,
  _WriteEx
};

/*********************************************************************
*
*       FS_DF_ChipErase
*
*  Description:
*    
*
*  Parameters:
*    Unit          - Specifies which DataFlash unit
*
*/
void FS_DF_ChipErase(U8 Unit) {
  FS_NAND_DEVICE_INFO  DevInfo;
  int                  i;

  if (_InitGetDeviceInfo(Unit, &DevInfo) == 0) {
    for (i = 0; i < DevInfo.NumBlocks; i++) {
      U32 PageIndex;
      PageIndex = i << DevInfo.PPB_Shift;
      _EraseBlock(Unit, PageIndex);
    }
  }
}

/*************************** End of file ****************************/
