/*********************************************************************
*                SEGGER MICROCONTROLLER GmbH & Co. KG                *
*        Solutions for real time microcontroller applications        *
**********************************************************************
*                                                                    *
*        (c) 2003-2012     SEGGER Microcontroller GmbH & Co KG       *
*                                                                    *
*        Internet: www.segger.com    Support:  support@segger.com    *
*                                                                    *
**********************************************************************

**** emFile file system for embedded applications ****
emFile is protected by international copyright laws. Knowledge of the
source code may not be used to write a similar product. This file may
only be used in accordance with a license and should not be re-
distributed in any way. We appreciate your understanding and fairness.
----------------------------------------------------------------------
File        : FS_CacheMan.c
Purpose     : Logical Block Layer, Cache module
              Cache Strategy:
                Pure read cache, caching management sectors only.
                Which sectors are management sectors is determined
                by the File system. In case of FAT, only FAT sectors
                are considered management sectors.
              Limitations:
                None. This cache module can be used on any device with any file system.
---------------------------END-OF-HEADER------------------------------
*/

/*********************************************************************
*
*       #include Section
*
**********************************************************************
*/

#include "FS_ConfDefaults.h"
#include "FS_Int.h"

#if FS_SUPPORT_CACHE

/*********************************************************************
*
*       Defines, non-configurable
*
**********************************************************************
*/

#define SECTOR_INDEX_INVALID   0xFFFFFFFFUL

/*********************************************************************
*
*       Static code
*
**********************************************************************
*/

/*********************************************************************
*
*       _GetHashCode
*
*   Function description
*     Returns the entry in the cache which stores a given sector.
*/
static U32 _GetHashCode(U32 SectorNo, U32 NumSectorIndices) {
  return SectorNo % NumSectorIndices;
}

/*********************************************************************
*
*       _InvalidateCache
*
*   Function description
*    Invalidates all data in cache
*/
static void _InvalidateCache(CACHE_MAN_DATA * pCacheData) {
  U32 i;
  U32 NumSectors;
  U32 SectorSize;
  CACHE_MAN_BLOCK_INFO * pBlockInfo;

  NumSectors = pCacheData->NumSectors;
  SectorSize = pCacheData->SectorSize;
  pBlockInfo = (CACHE_MAN_BLOCK_INFO *)(pCacheData + 1);
  //
  // Init all the cache entries.
  //
  for (i = 0; i < NumSectors; i++) {
    pBlockInfo->SectorNo = SECTOR_INDEX_INVALID;
    pBlockInfo = (CACHE_MAN_BLOCK_INFO*)(((U8*)(pBlockInfo + 1)) + SectorSize);
  }
}

/*********************************************************************
*
*       _ComputeNumSectors
*
*/
static U32 _ComputeNumSectors(FS_DEVICE * pDevice, CACHE_MAN_DATA * pCacheData) {
  U32 NumSectors;
  U16 SectorSize;
  U32 NumBytes;

  if (pCacheData->SectorSize) {
    return pCacheData->NumSectors;
  }
  NumBytes   = pCacheData->NumBytesCache;
  SectorSize = FS_GetSectorSize(pDevice);
  if ((SectorSize == 0) || (NumBytes < sizeof(CACHE_MAN_DATA))) {
    return 0;
  }
  //
  // Compute number of sectors in cache.
  //
  NumSectors = (NumBytes - sizeof(CACHE_MAN_DATA)) / (sizeof(CACHE_MAN_BLOCK_INFO) + SectorSize);
  if (NumSectors > 0) {
    pCacheData->NumSectors    = NumSectors;
    pCacheData->SectorSize    = SectorSize;
    _InvalidateCache(pCacheData);
  }
  return NumSectors;
}

/*********************************************************************
*
*       _WriteIntoCache
*
*   Function description
*     Writes a sector to cache.
*/
static void _WriteIntoCache(CACHE_MAN_BLOCK_INFO  * pBlockInfo, U32 SectorNo, const void * pData, U32 SectorSize) {
  pBlockInfo->SectorNo = SectorNo;
  FS_MEMCPY(pBlockInfo + 1, pData, SectorSize);
}

/*********************************************************************
*
*       Static code (callbacks)
*
**********************************************************************
*/

/*********************************************************************
*
*       _CacheMan_ReadFromCache
*
*   Function description
*    Read sector from cache if possible
*
*   Return value
*     0   Sector found
*     1   Sector not found
*/
static char _CacheMan_ReadFromCache(FS_DEVICE * pDevice, U32 SectorNo, void * pData, U8 SectorType) {
  U32 Off;
  U32 SectorSize;
  CACHE_MAN_DATA       * pCacheData;
  CACHE_MAN_BLOCK_INFO * pBlockInfo;

  FS_USE_PARA(SectorType);
  pCacheData  = (CACHE_MAN_DATA *)pDevice->Data.pCacheData;
  if (_ComputeNumSectors(pDevice, pCacheData) == 0) {
    return 1;             // Device is not available
  }
  SectorSize  = pCacheData->SectorSize;
  Off         = _GetHashCode(SectorNo, pCacheData->NumSectors) * (sizeof(CACHE_MAN_BLOCK_INFO) + SectorSize);
  pBlockInfo  = (CACHE_MAN_BLOCK_INFO *) (((U8 *)(pCacheData + 1)) + Off);
  if (pBlockInfo->SectorNo == SectorNo) {
    FS_MEMCPY(pData, pBlockInfo + 1, SectorSize);
    return 0;             // Sector found
  }
  return 1;               // Sector not found
}

/*********************************************************************
*
*       _CacheMan_WriteCache
*
*   Function description
*     Writes sector to cache.
*
*  Return value
*    0    Not in write cache, the physical write operation still needs to be performed (Since this cache is a pure read-cache).
*/
static char _CacheMan_WriteCache(FS_DEVICE * pDevice, U32 SectorNo, const void * pData, U8 SectorType) {
  U32 Off;
  U32 SectorSize;
  CACHE_MAN_DATA       * pCacheData;
  CACHE_MAN_BLOCK_INFO * pBlockInfo;

  pCacheData  = (CACHE_MAN_DATA *)pDevice->Data.pCacheData;
  if (_ComputeNumSectors(pDevice, pCacheData) == 0) {
    return 0;             // Device is not available 
  }
  if (SectorType == FS_SECTOR_TYPE_MAN) {
    SectorSize  = pCacheData->SectorSize;
    Off         = _GetHashCode(SectorNo, pCacheData->NumSectors) * (sizeof(CACHE_MAN_BLOCK_INFO) + SectorSize);
    pBlockInfo  = (CACHE_MAN_BLOCK_INFO *) (((U8 *)(pCacheData + 1)) + Off);
    _WriteIntoCache(pBlockInfo, SectorNo, pData, SectorSize);
  }
  return 0;
}

/*********************************************************************
*
*       _CacheMan_InvalidateCache
*
*   Function description
*    Invalidates all data in cache
*/
static void _CacheMan_InvalidateCache(void * p) {
  CACHE_MAN_DATA * pCacheData;

  pCacheData = (CACHE_MAN_DATA *)p;
  _InvalidateCache(pCacheData);
  pCacheData->NumSectors = 0;
  pCacheData->SectorSize = 0;
}

/*********************************************************************
*
*       _CacheMan_Command
*
*   Function description
*     Executes a cache command.
*
*   Return value
*     ==0  Command executed
*     !=0  An error occurred
*/
static int _CacheMan_Command(FS_DEVICE * pDevice, int Cmd, void *p) {
  int r;

  FS_USE_PARA(pDevice);
  r  = -1;
  switch (Cmd) {
  case FS_CMD_CACHE_INVALIDATE:
    _CacheMan_InvalidateCache(p);
    r = 0;
    break;
  case FS_CMD_CACHE_GET_TYPE:
    {
      FS_CACHE_TYPE * pCacheType;

      pCacheType  = (FS_CACHE_TYPE *)p;
      if (pCacheType) {
        *pCacheType = FS_CacheMan_Init;
      }
      r = 0;
    }
    break;
  }
  return r;
}

/*********************************************************************
*
*       _CacheManAPI
*
*/
static const FS_CACHE_API _CacheManAPI = {
  _CacheMan_ReadFromCache,
  _CacheMan_WriteCache,
  _CacheMan_InvalidateCache,
  _CacheMan_Command,
  _CacheMan_WriteCache,
};

/*********************************************************************
*
*       Public data
*
**********************************************************************
*/

/*********************************************************************
*
*       FS_CacheMan_Init
*
*   Function description
*     Initializes the cache.
*/
U32 FS_CacheMan_Init(FS_DEVICE * pDevice, void * pData, I32 NumBytes) {
  FS_DEVICE_DATA * pDevData;
  U32              NumSectors;
  CACHE_MAN_DATA * pCacheData;
  U8             * pData8;

  NumSectors = 0;
  pDevData   = &pDevice->Data;
  pData8     = (U8 *)pData;
  //
  // Align pointer to a 32bit boundary
  //
  if ((U32)pData8 & 3) {
    NumBytes  -= (4 - (((U32)pData8) & 3));
    pData8    += (4 - (((U32)pData8) & 3));
  }
  //
  // If less memory is available as we need to hold the
  // management structure, we leave everything as it is.
  // A cache module is then not attached to the device.
  //
  if ((U32)NumBytes < sizeof(CACHE_MAN_DATA)) {
    return 0;
  }
  pCacheData = (CACHE_MAN_DATA *)pData8;
  FS_MEMSET(pCacheData, 0, sizeof(CACHE_MAN_DATA));
  pDevData->pCacheAPI       = &_CacheManAPI;
  pDevData->pCacheData      = pCacheData;
  pCacheData->NumBytesCache = NumBytes;
  NumSectors = _ComputeNumSectors(pDevice, pCacheData);
  return NumSectors;
}
#else 
 void CacheMan_c(void);
 void CacheMan_c(void) {}

#endif // FS_SUPPORT_CACHE

/*************************** End of file ****************************/
