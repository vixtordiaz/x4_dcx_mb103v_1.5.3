/*********************************************************************
*                SEGGER MICROCONTROLLER GmbH & Co. KG                *
*        Solutions for real time microcontroller applications        *
**********************************************************************
*                                                                    *
*        (c) 2003-2012     SEGGER Microcontroller GmbH & Co KG       *
*                                                                    *
*        Internet: www.segger.com    Support:  support@segger.com    *
*                                                                    *
**********************************************************************

**** emFile file system for embedded applications ****
emFile is protected by international copyright laws. Knowledge of the
source code may not be used to write a similar product. This file may
only be used in accordance with a license and should not be re-
distributed in any way. We appreciate your understanding and fairness.
----------------------------------------------------------------------
File        : NAND_PHY_2048x16.c
Purpose     : Large page NAND flashes physical 16-bit access
Literature  : [1] \\fileserver\techinfo\Company\Samsung\NAND_Flash\Device\K9K8G08U0A_2KPageSLC_R11.pdf
              [2] \\Fileserver\techinfo\Company\Samsung\NAND_Flash\Device\K9F1Gxx0M_2KPageSLC_R13.pdf
              [3] \\fileserver\techinfo\Company\Micron\NAND\MT29F2G0_8AAD_16AAD_08ABD_16ABD.pdf
---------------------------END-OF-HEADER------------------------------
*/

/*********************************************************************
*
*             #include Section
*
**********************************************************************
*/

#include "FS_Int.h"
#include "FS_CLib.h"
#include "FS_Debug.h"
#include "NAND_X_HW.h"


/*********************************************************************
*
*             #define constants
*
**********************************************************************
*/
/* Status */
#define STATUS_ERROR            0x01    /* 0:Pass,          1:Fail */
#define STATUS_READY            0x40    /* 0:Busy,          1:Ready */
#define STATUS_WRITE_PROTECTED  0x80    /* 0:Protect,       1:Not Protect */

#define PAGES_PER_BLOCK         64

/*********************************************************************
*
*       NAND commands
*/
#define NAND_CMD_WRITE_1         0x80
#define NAND_CMD_WRITE_2         0x10
#define NAND_CMD_READ_1          0x00
#define NAND_CMD_READ_2          0x30
#define NAND_CMD_RESET_CHIP      0xFF
#define NAND_CMD_ERASE_1         0x60
#define NAND_CMD_ERASE_2         0xD0
#define NAND_CMD_READ_STATUS     0x70
#define NAND_CMD_READ_ID         0x90
#define NAND_CMD_RANDOM_READ_1  0x05
#define NAND_CMD_RANDOM_READ_2  0xE0
#define NAND_CMD_RANDOM_WRITE   0x85

/*********************************************************************
*
*       Static code
*
**********************************************************************
*/

/*********************************************************************
*
*       _WriteCmd
*
*  Function description
*    Writes a single byte command to the NAND flash
*/
static void _WriteCmd(U8 Unit, U16 Cmd) {
  FS_NAND_HW_X_SetCmdMode(Unit);
  FS_NAND_HW_X_Write_x16(Unit, &Cmd, sizeof(Cmd));
  FS_NAND_HW_X_SetDataMode(Unit);      // Switch back to data mode (default)
}

/*********************************************************************
*
*       _StartOperation
*
*  Function description
*    Enables CE and writes a single byte command to the NAND flash
*/
static void _StartOperation(U8 Unit, U8 Cmd) {
  FS_NAND_HW_X_EnableCE(Unit);
  _WriteCmd(Unit, Cmd);
}

/*********************************************************************
*
*       _WriteRowAddr
*
*  Function description
*    Writes the row addr into the NAND flash.
*
*  Parameters
*    RowAddr   Zero-based page index.
*
*  Notes
*    (1) RowAddr
*        This is the zero based page index.
*        A block consists of 64 pages, so that BlockIndex = RowAddr / 64.
*/
static void _WriteRowAddr(U8 Unit, U32 RowAddr) {
  U16 aAddr[3];

  FS_NAND_HW_X_SetAddrMode(Unit);
  aAddr[0] = (U8)RowAddr;
  aAddr[1] = (U8)(RowAddr >> 8);
  aAddr[2] = (U8)(RowAddr >> 16);
  FS_NAND_HW_X_Write_x16(Unit, aAddr, sizeof(aAddr));
}

/*********************************************************************
*
*       _WriteCRAddr
*
*  Function description
*    Writes the column and row addr into the NAND flash.
*
*  Parameters
*    RowAddr   Zero-based page index.
*    ColAddr   Byte-offset within a page
*/
static void _WriteCRAddr(U8 Unit, unsigned ColAddr, U32 RowAddr) {
  U16 aAddr[5];

  ColAddr >>= 1;
  FS_NAND_HW_X_SetAddrMode(Unit);
  aAddr[0] = (U8)ColAddr;
  aAddr[1] = (U8)(ColAddr >> 8);
  aAddr[2] = (U8)RowAddr;
  aAddr[3] = (U8)(RowAddr >> 8);
  aAddr[4] = (U8)(RowAddr >> 16);
  FS_NAND_HW_X_Write_x16(Unit, aAddr, sizeof(aAddr));
  FS_NAND_HW_X_SetDataMode(Unit);
}

/*********************************************************************
*
*       _WriteCAddr
*
*  Function description
*    Writes the column into the NAND flash.
*
*  Parameters
*    ColAddr   Byte-offset within the selected page
*/
static void _WriteCAddr(U8 Unit, unsigned ColAddr) {
  U16 aAddr[2];

  ColAddr >>= 1;
  FS_NAND_HW_X_SetAddrMode(Unit);
  aAddr[0] = (U8)ColAddr;
  aAddr[1] = (U8)(ColAddr >> 8);
  FS_NAND_HW_X_Write_x16(Unit, aAddr, sizeof(aAddr));
  FS_NAND_HW_X_SetDataMode(Unit);
}

/*********************************************************************
*
*       _ReadStatus
*
*  Function description
*    Reads and returns the contents of the status register.
*/
static U8 _ReadStatus(U8 Unit) {
  U16 r;

  _WriteCmd(Unit, NAND_CMD_READ_STATUS);
  FS_NAND_HW_X_Read_x16(Unit, &r, sizeof(r));
  return (U8)r;
}

/*********************************************************************
*
*       _ResetErr
*
*  Function description
*    Resets the NAND flash by command
*/
static void _ResetErr(U8 Unit) {
  U8 Status;

  _StartOperation(Unit, NAND_CMD_RESET_CHIP);
  Status = 0;
  do {
    Status = _ReadStatus(Unit);
  } while (!(Status & STATUS_READY));
  FS_NAND_HW_X_DisableCE(Unit);
}

/*********************************************************************
*
*       _WaitBusy
*
*  Function description
*     Waits for the NAND to complete its last operation.
*
*   Parameters
*     Unit
*
*   Return value
*    ==0  Success
*    !=0  An error has occurred
*/
static int _WaitBusy(U8 Unit) {
  U8 Status;
  Status = 0;
  //
  // Try to use the hardware pin to find out when busy is cleared.
  //
  if (FS_NAND_HW_X_WaitWhileBusy(Unit, 0)) {
    //
    // Busy pin is not connected. We use status read instead.
    //
      do {
        Status = _ReadStatus(Unit);
      } while ((Status & STATUS_READY) == 0);
    } else {
    //
    // Read status to check for error
    //
      Status = _ReadStatus(Unit);
    }
  if (Status & STATUS_ERROR) {
    _ResetErr(Unit);
    return 1;                       // Error
  }
  return 0;                         // Success
}

/*********************************************************************
*
*       _EndOperation
*
*  Function description
*    Checks status register to find out if operation was successful and disables CE.
*
*  Return value:
*      0                       - Data successfully transferred.
*    !=0                       - An error has occurred.
*/
static int _EndOperation(U8 Unit) {
  U8 Status;

  Status = _ReadStatus(Unit);
  if ((Status & (STATUS_ERROR | STATUS_READY)) != STATUS_READY) {
    _ResetErr(Unit);
    return 1;                        // Error
  }
  FS_NAND_HW_X_DisableCE(Unit);
  return 0;                          // O.K.
}

/*********************************************************************
*
*       _WaitEndOperation
*
*  Function description
*    Waits until the current operation is completed (Checking busy)
*    and ends operation, disabling CE.
*
*  Return value:
*      0                       - Data successfully transferred.
*      1                       - An error has occurred.
*/
static int _WaitEndOperation(U8 Unit) {
  if(_WaitBusy(Unit)) {
    return -1;
  }
  return _EndOperation(Unit);
}

/*********************************************************************
*
*       _Read
*
*  Function description
*    Reads data from a complete or a part of a page.
*    This code is identical for main memory and spare area; the spare area
*    is located rigth after the main area.
*
*  Return value:
*      0                       - Data successfully transferred.
*    !=0                       - An error has occurred.
*/
static int _Read(U8 Unit, U32 PageNo, void * pData, unsigned Off, unsigned NumBytes) {
  FS_DEBUG_ASSERT(FS_MTYPE_DRIVER, ((NumBytes | Off | (U32)pData) & 1) == 0);
  _StartOperation(Unit, NAND_CMD_READ_1);
  _WriteCRAddr(Unit, Off, PageNo);
  _WriteCmd(Unit, NAND_CMD_READ_2);
  if (_WaitBusy(Unit)) {
    return 1;                            // Error
  }
  //
  // Restore the command register destroyed by the READ STATUS operation
  //
  _WriteCmd(Unit, NAND_CMD_READ_1);
  FS_NAND_HW_X_Read_x16(Unit, pData, NumBytes);
  return _EndOperation(Unit);
}

/*********************************************************************
*
*       _ReadEx
*
*  Function description
*    Reads data from a 2 parts of a page.
*    Typically used to read data and spare area at the same time.
*
*  Return value:
*      0                       - Data successfully transferred.
*    !=0                       - An error has occurred.
*
*  Notes
*    (1) Literature
*        Procedure taken from [1], Random data output in a Page, p. 30
*/
static int _ReadEx(U8 Unit, U32 PageNo, void * pData, unsigned Off, unsigned NumBytes, void * pSpare, unsigned OffSpare, unsigned NumBytesSpare) {
  FS_DEBUG_ASSERT(FS_MTYPE_DRIVER, ((NumBytes | Off | (U32)pSpare | OffSpare | NumBytesSpare) & 1) == 0);
  _StartOperation(Unit, NAND_CMD_READ_1);
  _WriteCRAddr(Unit, Off, PageNo);
  _WriteCmd(Unit, NAND_CMD_READ_2);
  if (_WaitBusy(Unit)) {
    return 1;                            // Error
  }
  //
  // Restore the command register destroyed by the READ STATUS operation
  //
  _WriteCmd(Unit, NAND_CMD_READ_1);
  FS_NAND_HW_X_Read_x16(Unit, pData, NumBytes);
  _WriteCmd(Unit, NAND_CMD_RANDOM_READ_1);
  _WriteCAddr(Unit, OffSpare);
  _WriteCmd(Unit, NAND_CMD_RANDOM_READ_2);
  FS_NAND_HW_X_Read_x16(Unit, pSpare, NumBytesSpare);
  return _EndOperation(Unit);
}

/*********************************************************************
*
*       _Write
*
*  Function description
*    Writes data into a complete or a part of a page.
*    This code is identical for main memory and spare area; the spare area
*    is located rigth after the main area.
*
*  Return value:
*      0                       - Data successfully transferred.
*    !=0                       - An error has occurred.
*/
static int _Write(U8 Unit, U32 PageNo, const void * pData, unsigned Off, unsigned NumBytes) {
  FS_DEBUG_ASSERT(FS_MTYPE_DRIVER, ((NumBytes | Off | (U32)pData) & 1) == 0);
  _StartOperation(Unit, NAND_CMD_WRITE_1);
  _WriteCRAddr(Unit, Off, PageNo);
  FS_NAND_HW_X_Write_x16(Unit, pData, NumBytes);
  _WriteCmd(Unit, NAND_CMD_WRITE_2);
  FS_DEBUG_LOG((FS_MTYPE_DRIVER, "Write:    Block: 0x%.8x,  Page: 0x%.8x, Off: 0x%.8x, NumBytes: 0x%.8x", (PageNo >> 6), (PageNo & 0x3f),  Off, NumBytes));
  return _WaitEndOperation(Unit);
}

/*********************************************************************
*
*       _WriteEx
*
*  Function description
*    Writes data to 2 parts of a page.
*    Typically used to write data and spare area at the same time.
*
*  Return value:
*      0                       - Data successfully transferred.
*      1                       - An error has occurred.
*/
static int _WriteEx(U8 Unit, U32 PageNo, const void * pData, unsigned Off, unsigned NumBytes, const void * pSpare, unsigned OffSpare, unsigned NumBytesSpare) {
  FS_DEBUG_ASSERT(FS_MTYPE_DRIVER, ((NumBytes | Off | (U32)pData | (U32)pSpare | OffSpare | NumBytesSpare) & 1) == 0);
  _StartOperation(Unit, NAND_CMD_WRITE_1);
  _WriteCRAddr(Unit, Off, PageNo);
  FS_NAND_HW_X_Write_x16(Unit, pData, NumBytes);
  _WriteCmd(Unit, NAND_CMD_RANDOM_WRITE);
  _WriteCAddr(Unit, OffSpare);
  FS_NAND_HW_X_Write_x16(Unit, pSpare, NumBytesSpare);
  _WriteCmd(Unit, NAND_CMD_WRITE_2);
  FS_DEBUG_LOG((FS_MTYPE_DRIVER, "WriteEx:  Block: 0x%.8x,  Page: 0x%.8x, Off: 0x%.8x, NumBytes: 0x%.8x, OffSpare: 0x%.8x, NumBytesSpare: 0x%.8x", PageNo / PAGES_PER_BLOCK, PageNo & (PAGES_PER_BLOCK - 1),  Off, NumBytes, OffSpare, NumBytesSpare));
  return _WaitEndOperation(Unit);
}

/*********************************************************************
*
*       _EraseBlock
*
*  Function description
*    Erases a block.
*
*  Return value:
*      0                       - Data successfully transferred.
*    !=0                       - An error has occurred.
*/
static int _EraseBlock(U8 Unit, U32 BlockNo) {
  _StartOperation(Unit, NAND_CMD_ERASE_1);
  _WriteRowAddr(Unit, BlockNo);
  _WriteCmd(Unit, NAND_CMD_ERASE_2);
  FS_DEBUG_LOG((FS_MTYPE_DRIVER, "Erase:    Block: 0x%.8x", (BlockNo / PAGES_PER_BLOCK)));
  return _WaitEndOperation(Unit);
}

/*********************************************************************
*
*       _InitGetDeviceInfo
*
*  Function description
*    Initializes hardware layer, resets NAND flash and tries to identify the NAND flash.
*    If the NAND flash can be handled, Device Info is filled.
*
*  Return value:
*      0                       - O.K., device can be handled
*      1                       - Error: device can not be handled
*
*  Notes
*       (1) A RESET command must be issued as the first command after power-on (see [3])
*/
static int _InitGetDeviceInfo(U8 Unit, FS_NAND_DEVICE_INFO * pDevInfo) {
  U16  aId[5];
  U8  DeviceCode;
  U16 NumBlocks;
  const U16 Dummy = 0;

  FS_NAND_HW_X_Init_x16(Unit);
  _ResetErr(Unit);         // Note 1
  //
  // Retrieve Id information from NAND device
  //
  _StartOperation(Unit, NAND_CMD_READ_ID);
  FS_NAND_HW_X_SetAddrMode(Unit);
  FS_NAND_HW_X_Write_x16(Unit, &Dummy, sizeof(Dummy));
  FS_NAND_HW_X_SetDataMode(Unit);
  FS_NAND_HW_X_Read_x16(Unit, &aId[0], sizeof(aId));
  if (_EndOperation(Unit)) {
    return 1;                  // Error
  }

  DeviceCode  = (U8)aId[1];
  switch(DeviceCode)   {
  case 0xB1:
  case 0xC1:
    NumBlocks    = 1024;
    break;
  case 0xBA:
  case 0xCA:
    NumBlocks    = 2048;
    break;
  case 0xBC:
  case 0xCC:
    NumBlocks    = 4096;
    break;
  case 0xB3:
  case 0xC3:
    NumBlocks    = 8192;
    break;
  default:
    return 1;                // Error
  }
  pDevInfo->BPP_Shift = 11;  // 2048 bytes/page
  pDevInfo->PPB_Shift =  6;  // Large page NAND flashes have 64 pages per block
  pDevInfo->NumBlocks = NumBlocks;
  return 0;                  // Success
}

/*********************************************************************
*
*       _IsWP
*
*  Function description
*    Checks if the device is write protected.
*    This is done by reading bit 7 of the status register.
*    Typical reason for write protection is that either the supply voltage is too low
*    or the /WP-pin is active (low)
*
*  Return value:
*     <0          - Error
*      0          - Not write protected
*     >0          - Write protected
*/
static int _IsWP(U8 Unit) {
  U8 Status;

  FS_NAND_HW_X_EnableCE(Unit);
  Status = _ReadStatus(Unit);
  if (_EndOperation(Unit)) {
    return -1;                    // Error
  }
  return (Status & STATUS_WRITE_PROTECTED) ? 0 : 1;
}

/*********************************************************************
*
*       Public const
*
**********************************************************************
*/
const FS_NAND_PHY_TYPE FS_NAND_PHY_2048x16 = {
  _EraseBlock,
  _InitGetDeviceInfo,
  _IsWP,
  _Read,
  _ReadEx,
  _Write,
  _WriteEx
};


/*************************** End of file ****************************/
