/*********************************************************************
*                SEGGER MICROCONTROLLER GmbH & Co. KG                *
*        Solutions for real time microcontroller applications        *
**********************************************************************
*                                                                    *
*        (c) 2003-2012     SEGGER Microcontroller GmbH & Co KG       *
*                                                                    *
*        Internet: www.segger.com    Support:  support@segger.com    *
*                                                                    *
**********************************************************************

**** emFile file system for embedded applications ****
emFile is protected by international copyright laws. Knowledge of the
source code may not be used to write a similar product. This file may
only be used in accordance with a license and should not be re-
distributed in any way. We appreciate your understanding and fairness.
----------------------------------------------------------------------
File        : NAND_Drv.c
Purpose     : File system generic NAND driver for Single level cell
              NAND flashes. Also works for ATMEL Dataflashes.
              For more information on supported devices, refer to the
              user manual.
---------------------------END-OF-HEADER------------------------------

Literature:
[1] Samsung data sheet for K9XXG08UXM devices, specifically K9F2G08U0M, K9K4G08U1M. Similar information in other Samsung manuals.
    \\fileserver\techinfo\Company\Samsung\NAND_Flash\Device\K9F2GxxU0M_2KPageSLC_R12.pdf
[2] Micron data sheet for MT29F2G08AAD, MT29F2G16AAD, MT29F2G08ABD, MT29F2G16ABD devices
    \\fileserver\techinfo\Company\Micron\NANDFlash\MT29F2G0_8AAD_16AAD_08ABD_16ABD.pdf
[3] Micron presentations for Flash Memory Summit.
    \\fileserver\techinfo\Company\Micron\NANDFlash\flash_mem_summit_08_khurram_nand.pdf
[4] Micron application notes about bad block management, wear leveling and error correction codes.

General info on the inner workings of this high level flash driver:

ECC and Error correction
========================
  The driver uses an ECC error correction scheme. This error correction scheme allows finding and correcting 1-bit errors
  and detecting 2-bit errors. ECC is performed over blocks of 256 bytes. The reason why we chose this small block size
  (ECC block sizes of 512 or more are more common) is simple: It works more reliably and has a higher propability of actually
  detecting and fixing errors.
  Why are smaller ECC sizes better ?
  Simple. Let's take one example, where we consider 2 * 256 bytes of memory. If an error occurs in both blocks, it can still
  be fixed if every block has a separate ECC, but not if we only have one ECC for the entire 512 bytes.
  So the 256 byte ECC is always at least as good as the 512-byte ECC and better in some cases where we deal with 2 1-bit errors
  (which are correctable) versus a single 2-bit errror, which is not.

  How does ECC work ?
    Fairly simple actually, but you should consult literature.
    Basically, you need 2 * n bits for an ECC of 2^n bits. This means: 256 bytes = 2048 bits = 2 ^ 11 bits => 22 bits required.

Data management
===============
  Data is stored in so called data blocks in the NAND flash. The assignment information (which pyhsical block contains which data)
  is stored in the spare area of the block.
  Modifications of data are not done in the data blocks directly, but using a concept of work blocks. A work block contains
  modifications of a data block.
  The first block is used to store format information and written only once. All other blocks are used to store
  data. This means that in the driver, a valid physical block index is always > 0.

Capacity
========
  Not all available data blocks are used to store data.
  About 3% of the blocks are reserved in order to make sure that there are always blocks available,
  even if some develop bad blocks and can no longer be used.


Reading data
============
  So when data is read, the driver checks
  a) Is there a work block which contains this information ? If so, this is recent and used.
  b) Is there a data block which contains this information ? If so, this is recent and used.
  c) Otherwise, the sector has never been written. In this case, the driver delivers 0xFF data bytes.

Writing data to the NAND flash
==============================
  This is a complicated task.

Max. write accesses to a page without erasing it
================================================
  This depends on the sector size, but in general the following is done by the NAND driver when writing a block:

  Find work block which can be used:
  --> Work block found, but no more sectors are free, clean workblock:
    --> Workblock can be converted:  Up to sectors per page write accesses to one page (worst case: no sector in the page has been modified, all sectors in this page have to be copied from the old block)
                                     1 write access to the spare area of the first page in the block
    --> Workblock can not be converted: Up to sectors per page write accesses to one page
                                        1 write access to spare area of first page in the block, to mark it as valid data block
                                        1 write access to the first page of the workblock to mark it as invalid
                                        1 write access to the first page of the old data block to mark it as invalid
  --> No work block found or work block has been cleaned
    --> Write spare of first page in the work block to mark it as work block
  Write sector:
  --> 1 write access to write sector data + spare data (brsi info)
  --> 1 write access to write spare data in order to mark a previous version of the same sector as invalid

  Worst case:
    - First page in a work block is written in order to mark block as work block (1 WAcc)
    - Sector data + brsi is written into the first page in a work block (Up to sectors per page WAcc)
    - Previos version of the sector is invalidated. If we have 4 sectors in a page and the same sector is written 5 times, we have to invalidate the previous version of the sector 4 times
    - Workblock is converted / copied: Mark as data block / invalid (1 WAcc)

    Worst case 1 + SectorsPerPage + SectorsPerPage + 1 Write accesses to the same page


Spare area usage
================

  Block information.

    Block info is stored in the first spare area of a block.

    Byte 0x00  - Block status: 0xFF means O.K. - Else Error!
    Byte 0x01  - Data Count & status
                 b[3..0]: DataCnt:      Increments every time a block is copied. part of the logical block.
                 b[7..4]: DataStat:     F: Unused (Empty)
                                        E: Work
                                        C: Data
                                        0: Dirty
    Byte 0x02  - EraseCnt b[31..24]
    Byte 0x03  - EraseCnt b[23..16]
    Byte 0x04  - EraseCnt b[15..8]
    Byte 0x05  - EraseCnt b[7..0]         // For older NAND flashes, this byte is used as block info, in which case EraseCnt[7..0] moves to offset 0
    Byte 0x06  - LBI  15:8
    Byte 0x07  - LBI  7:0
    Byte 0x0B  - LBI 15:8 [Copy]
    Byte 0x0C  - LBI  7:0 [Copy]


  ECC information

    Spare are usage, ECC (512 byte pages)
      0x8 - 0xA     ECC for 0x100-0x1FF
      0xD - 0xF     ECC for 0x000-0x0FF

    Spare are usage, ECC (2048 byte pages)
      0x08 - 0x0A     ECC for 0x100-0x1FF
      0x0D - 0x0F     ECC for 0x000-0x0FF
      0x18 - 0x1A     ECC for 0x300-0x3FF
      0x1D - 0x1F     ECC for 0x200-0x2FF
      0x28 - 0x2A     ECC for 0x500-0x5FF
      0x2D - 0x2F     ECC for 0x400-0x4FF
      0x38 - 0x3A     ECC for 0x700-0x7FF
      0x3D - 0x3F     ECC for 0x600-0x6FF

  The sector usage information is only present in work blocks
  Moreover, this information is available for each sector.
  For example, if we have 4 sectors per page (512 byte sectors, 2048 byte page size, 64 bytes spare size),
  the brsi information is available 4 times in the spare area of one page (1 time for each sector)
  info for sector0: 0x06-0x07, 0x0B-0x0C
  info for sector1: 0x16-0x17, 0x1B-0x1C
  info for sector2: 0x26-0x27, 0x2B-0x2C
  info for sector3: 0x36-0x37, 0x3B-0x3C
  
  Sector usage (BRSI, block relative sector information) for sectors in a work block
#if BRSI = 0 // First sector in block
    Byte[0]   - Block status
    Byte[1]   - Data Count & Status
    Byte[5:2] - EraseCnt
#else  // BRSI > 0
    Byte[0]   - Unused (0xFF)
    Byte[1]   - FreeMarker for this sector. 0 if this sector has been freed using TRIM, else 0xFF
    Byte[2]   - FreeMarker for first sector if BRSI = 1, else unused (0xFF)
    Byte[5:3] - Unused (0xFF)
#endif
    Byte 0x06  - BRSI  15:8 ^ 0xff
    Byte 0x07  - BRSI  7:0  ^ 0xff
    Byte 0x0B  - BRSI 15:8  ^ 0xff [Copy]
    Byte 0x0C  - BRSI  7:0  ^ 0xff [Copy]
    Example:
      Let's look at a system with 16 sectors per block and a workblock with BRSIs/LBIs as follows:
      SectorIndex  BRSI/LBI
                0    0x0001 -> Block 1  If ECC is valid, it contains BRSI 0 = Sector 16
                1    0x0000 -> Invalid (Same sector written multiple times into workblock)
                2    0xFFFE -> BSRI  1 -> Sector 17
                3    0xFFFF -> Unused
                4    0xFFFB -> BSRI  4 -> Sector 20
                5    0xFFFF -> Unused
                6    0xFFFF -> Unused
                ...
                15   0xFFFF -> Unused

Passive wear leveling
=====================
  BlockNoNextErase = pInst->BlockNoNextErase
  if (BlockNoNextErase == 0) {
    FindNextFree(pInst);
  }

Active wear leveling
====================
  If (pInst->MaxErase - pInst->MinErase > ACTIVE_WL_THRESHOLD) {
    CopyBlock(BlockNoLowestEraseCnt);
  }

 LBI: Logical block index   : Gives the data position of a block.
BRSI: Block relative sector index  : Index of sector relative to start of block, typ. 0..63 or 0..255

----------------------------------------------------------------------
Potential improvements

  Stability & data integrity
    - Optionally, mark blocks which have 1-bit errors as BAD (Maybe with special marker to show that they have been marked as bad by this driver and for what reason)

  Improve Speed
    - Optionally, keep erase count for every block in RAM (for systems with large amount of RAM) to speed up block search
    - Optionally, check before conversion if all the sectors of a work block are valid

  Reducing RAM size
    - Optionally, pFreeMap can be eliminated (free block is searched each time)

  Misc
  - Currently, at least 7 blocks are reserved, which is a lot for data flashes. This minimum could be eliminated. -> _ReadApplyDeviceParas()
*/

/*********************************************************************
*
*       #include Section
*
**********************************************************************
*/
#include "FS_Int.h"

/*********************************************************************
*
*       Defines, configurable
*
**********************************************************************
*/
#ifdef FS_NAND_MAXUNIT
  #define NUM_UNITS   FS_NAND_MAXUNIT
#else
  #define NUM_UNITS   4
#endif

#ifndef   FS_NAND_MAX_ERASE_CNT_DIFF
  #define FS_NAND_MAX_ERASE_CNT_DIFF            5000      // Determines Active wear leveling threshold, at which a block containing unchanged data is copied and freed. 0 means no active wearleveling.
#endif

#ifndef   FS_NAND_NUM_READ_RETRIES
  #define FS_NAND_NUM_READ_RETRIES              10      // Number of retries in case of a read error (device error or uncorrectable bit error)
#endif

#ifndef   FS_NAND_ENABLE_STATS
  #define FS_NAND_ENABLE_STATS                  (FS_DEBUG_LEVEL >= FS_DEBUG_LEVEL_CHECK_ALL)   // Statistics only in debug builds
#endif

#ifndef   FS_NAND_ENABLE_TRIM
  #define FS_NAND_ENABLE_TRIM                   1       // Enables/disables the TRIM functionality
#endif

#ifndef   FS_NAND_ENABLE_CLEAN
  #define FS_NAND_ENABLE_CLEAN                  1       // Enables/disables the CLEAN functionality
#endif

#ifndef   FS_NAND_RECLAIM_DRIVER_BAD_BLOCKS
  #define FS_NAND_RECLAIM_DRIVER_BAD_BLOCKS     1       // Configures how the driver treats the bad blocks marked by itself when the medium is low level formatted. 1: bad block is turned into a good block.
#endif

#ifndef   FS_NAND_NUM_BLOCKS_RESERVED        
  #define FS_NAND_NUM_BLOCKS_RESERVED           2       // Number of NAND blocks the driver reserves for internal use, in addition to the 3 percent for bad block repacement
#endif

/*********************************************************************
*
*       Defines, non configurable
*
**********************************************************************
*/
#define LLFORMAT_VERSION (20001)

#if FS_NAND_ENABLE_STATS
  #define IF_STATS(exp) exp
#else
  #define IF_STATS(exp)
#endif

/*********************************************************************
*
*       Spare area usage
*
*  For details, see explanation in header
*/
#define SPARE_OFF_DATA_STATUS   0x01
#define SPARE_OFF_ERASE_CNT     0x02
#define SPARE_OFF_ADDR1         0x06
#define SPARE_OFF_ADDR2         0x0B

#define SPARE_OFF_ECC00         0x0D
#define SPARE_OFF_ECC10         0x08

#define SPARE_OFF_SECTOR_FREE   0x01
#define SPARE_OFF_SECTOR0_FREE  0x02

/*********************************************************************
*
*       Special values for "INVALID"
*/
#define BRSI_INVALID       0xFFFF         // Invalid relative sector index
#define ERASE_CNT_INVALID  0xFFFFFFFFUL   // Invalid erase count

/*********************************************************************
*
*       DATA STATUS NIBBLE
*/
#define DATA_STAT_EMPTY    0xF      // Block is empty
#define DATA_STAT_WORK     0xE      // Block is used as "work block"
#define DATA_STAT_VALID    0xC      // Block contains valid data
#define DATA_STAT_INVALID  0x0      // Block contains old, invalid data

/*********************************************************************
*
*       Block status marker
*/
#define BAD_BLOCK_MARKER   0x00
#define GOOD_BLOCK_MARKER  0xFF

/*********************************************************************
*
*       Status of read/write NAND operations
*/
#define NAND_NO_ERROR             0   // Everything OK
#define NAND_1BIT_CORRECTED       1   // 1 bit error in the data corrected
#define NAND_ERROR_IN_ECC         2   // Error in the ECC itself detected, data is OK
#define NAND_UNCORRECTABLE_ERROR  3   // 2 or more bit errors detected, not recoverable
#define NAND_READ_ERROR           4   // Error while reading from NAND, not recoverable
#define NAND_WRITE_ERROR          5   // Error while writing into NAND, recoverable
#define NAND_OUT_OF_FREE_BLOCKS   6   // Tried to allocate a free block but no more were found
#define NAND_ERASE_ERROR          7   // Error while erasing a NAND block, recoverable

/*********************************************************************
*
*       Const data
*
**********************************************************************
*/
#define FORMAT_INFO_SECTOR_INDEX    0
#define ERROR_INFO_SECTOR_INDEX     1

#if FS_SUPPORT_JOURNAL
  #define NUM_WORK_BLOCKS_MIN       4   // For performance reasons we need more work blocks when Journaling is enabled
#else
  #define NUM_WORK_BLOCKS_MIN       3
#endif

#define NUM_WORK_BLOCKS_MAX         10

#ifdef FS_NAND_MAX_WORK_BLOCKS
  #define NUM_WORK_BLOCKS_OLD       FS_NAND_MAX_WORK_BLOCKS
#else
  #define NUM_WORK_BLOCKS_OLD       3
#endif

/****************************************************************************************
*
*       The first sector/block in a NAND flash should have these values so the NAND
*       driver recognize the device as properly formatted.
*/
static const U8 _acInfo[16] = {
  0x53, 0x45, 0x47, 0x47, 0x45, 0x52             // Id (Can be expanded in the future to include format / version information)
};
#define INFO_OFF_LLFORMAT_VERSION         0x10
#define INFO_OFF_SECTOR_SIZE              0x20
#define INFO_OFF_BAD_BLOCK_OFFSET         0x30
#define INFO_OFF_NUM_LOG_BLOCKS           0x40
#define INFO_OFF_NUM_WORK_BLOCKS          0x50

/****************************************************************************************
*
*       The second sector of the first block in a NAND flash stores the fatal error information
*/
#define INFO_OFF_IS_WRITE_PROTECTED       0x00        // Inverted. 0xFFFF -> ~0xFFFF = 0 means normal (not write protected)
#define INFO_OFF_HAS_FATAL_ERROR          0x02        // Inverted. 0xFFFF -> ~0xFFFF = 0 means normal (no error)
#define INFO_OFF_FATAL_ERROR_TYPE         0x04    // Type of fatal error
#define INFO_OFF_FATAL_ERROR_SECTOR_INDEX 0x08    // Index of the sector where the error occurred

/****************************************************************************************
*
*       The data area of the first page stores this info about the reason why the block was marked as bad
*/
#define INFO_OFF_BAD_MARK_ERROR_TYPE      0x00    // Reason why the block was marked as bad (U16, BE)
#define INFO_OFF_BAD_MARK_ERROR_BRSI      0x02    // Index of the sector where the error occurred in the block marked as bad (U16, BE)

/*********************************************************************
*
*       Type definitions
*
**********************************************************************
*/


/*********************************************************************
*
*       WORK_BLOCK
*
*  Organisation of a work block
*  ============================
*
*  The WORK_BLOCK structure has 6 elements, as can be seen below.
*  The first 2, pNext & pPrev, are used to keep it in a doubly linked list.
*  The next 2 elements are used to associate it with a data block and logical block index.
*  The last 2 elements contain the actual managment data. The are pointers to arrays, allocated during init.
*  paIsWritten is a 1-bit array.
*    There is one bit = one entry for every sector in the block.
*  paAssign is a n bit array.
*    The number of bits is determined by the number of sectors per block.
*    The index is the logical position (brsi, block relative sector index).
*/
typedef struct WORK_BLOCK_DESC {
  struct WORK_BLOCK_DESC * pNext;          // Pointer to next work buffer.     NULL if there is no next.
  struct WORK_BLOCK_DESC * pPrev;          // Pointer to previous work buffer. NULL if there is no previous.
  unsigned            pbi;            // Physical Index of the destination block which data is written to. 0 means none is selected yet.
  unsigned            lbi;            // Logical block index of the work block
  U8                * paIsWritten;    // Pointer to IsWritten table, which indicates which sectors have already been transferred into the work buffer. This is a 1-bit array.
  void              * paAssign;       // Pointer to assignment table, containing n bits per block. n depends on number of sectors per block.
} WORK_BLOCK_DESC;

/*********************************************************************
*
*       NAND_INST structure
*
*  This is the central data structure for the entire driver.
*  It contains data items of one instance of the driver.
*/
typedef struct {
  U8         Unit;
  U8         IsLLMounted;
  U8         LLMountFailed;
  U8         IsWriteProtected;
  U8         BadBlockOffset;         // Specifies where to find the bad block information in the spare area.
                                     // Small page NAND flashes (Page size: 512 bytes) normally contain the information at Offset 5 in the spare area.
                                     // Large page NAND flashes (Page size: 2048/4096 bytes)  normally contain the information at Offset 0 in the spare area.
  U8         HasFatalError;
  U8         ErrorType;              // Type of fatal error
  unsigned   ErrorSectorIndex;       // Index of the sector where the fatal error occurred
  const FS_NAND_PHY_TYPE * pPhyType;  // Interface to physical layer
  U8         * pFreeMap;          // Pointer to physical block usage map. Each bit represents one physical block. 0: Block is not assigned; 1: Assigned or bad block.
                                  // Only purpose is to find a free block.
  U8         * pLog2PhyTable;     // Pointer to Log2Phytable, which contains the logical to physical block translation (0xFFFF -> Not assigned)
  U32          NumSectors;        // Number of logical sectors. This is redundant, but the value is used in a lot of places, so it is worth it!
  U32          EraseCntMax;       // Worst (= highest) erase count of all blocks
  U32          NumPhyBlocks;
  U32          NumLogBlocks;
  U32          EraseCntMin;           // Smallest erase count of all blocks. Used for active wear leveling.
  U32          NumBlocksEraseCntMin;  // Number of erase counts with the smallest value
  U32          NumWorkBlocks;         // Number of configured work blocks
  WORK_BLOCK_DESC * pFirstWorkBlockInUse;   // Pointer to the first work block
  WORK_BLOCK_DESC * pFirstWorkBlockFree;    // Pointer to the first free work block
  WORK_BLOCK_DESC * paWorkBlock;            // WorkBlock management info
  U32          MRUFreeBlock;       // Most recently used free block
  U16          BytesPerSector;
  U16          BytesPerPage;
  U8           SPB_Shift;         // Sectors per Block shift: typ. a block contains 64 pages -> 8; in case of 2048 byte physical pages and 512 byte sectors: 256 -> 8
  U8           PPB_Shift;         // Pages per block shift. Typ. 6 for 64 pages for block
  U8           NumBitsPhyBlockIndex;
  //
  // Configuration items. 0 per default, which typically means: Use a reasonable default
  //
  U32       FirstBlock;              // Allows sparing blocks at the beginning of the NAND flash
  U32       MaxNumBlocks;            // Allows sparing blocks at the end of the NAND flash
  U32       MaxEraseCntDiff;         // Threshold for active wear leveling
  U32       NumWorkBlocksConf;        // Number of work blocks configured by application
  //
  // Additional info for debugging purposes
  //
#if FS_NAND_ENABLE_STATS
  FS_NAND_STAT_COUNTERS   StatCounters; 
#endif
} NAND_INST;


/*********************************************************************
*
*       Static data
*
**********************************************************************
*/
static U32        * _pSectorBuffer;         // We need a buffer for one sector for internal operations such as copying a block etc. Typ. 512 or 2048 bytes.
static U8         * _pSpareAreaData;        // Buffer for spare area, Either 16 or 64 bytes.
static NAND_INST  * _apInst[NUM_UNITS];
static int          _NumUnits;
static FS_NAND_ON_FATAL_ERROR_CALLBACK * _pfOnFatalError;

/*********************************************************************
*
*       Macros, function replacement
*
**********************************************************************
*/

/*********************************************************************
*
*       Assertion macros
*/
#if FS_DEBUG_LEVEL >= FS_DEBUG_LEVEL_CHECK_ALL
  #define ASSERT_UNIT_NO_IS_IN_RANGE(Unit)                            if (Unit >= _NumUnits)                     { FS_DEBUG_ERROROUT((FS_MTYPE_DRIVER,  "NAND: Invalid unit number."));   }
  #define ASSERT_PHY_TYPE_IS_SET(pInst)                               if ((const void *)pInst->pPhyType == NULL) { FS_DEBUG_ERROROUT((FS_MTYPE_DRIVER,  "NAND: PHY type not set."));      }
#else
  #define ASSERT_UNIT_NO_IS_IN_RANGE(Unit)
  #define ASSERT_PHY_TYPE_IS_SET(pInst)
#endif

/*********************************************************************
*
*       Local functions
*
**********************************************************************
*/

/*********************************************************************
*
*       _Find0BitInByte
*
*  Function description
*     Returns the position of the first 0-bit in a byte. The function checks only the bits 
*     between the offsets FirstBit and LastBit inclusive.
*
*  Parameters
*     FirstBit  Position of the first bit to check (0-based)
*     LastBit   Position of the last bit to check (0-based)
*     Off       Byte offset to be added to the found bit position
*  
*  Return value
*    >= 0 On Success, Bit position of first 0
*    -1   On Error, No 0-bit found
*/
static int _Find0BitInByte(U8 Data, unsigned FirstBit, unsigned LastBit, unsigned Off) {
  unsigned i;

  for (i = FirstBit; i <= LastBit; i++) {
    if ((Data & (1 << i)) == 0) {
      return i + (Off << 3);
    }
  }
  return -1;
}

/*********************************************************************
*
*       _Find0BitInArray
*
*  Function description
*    Finds the first 0-bit in a byte array. Bits are numbered lsb first as follows:
*    00000000 11111100
*    76543210 54321098
*    So the first byte contains bits 0..7, second byte contains bits 8..15, third byte contains 16..23
*
*  Return value
*    >= 0 On Success, Bit position of first 0
*    -1   On Error, No 0-bit found
*
*  Examples
*    Min = 1, Max = 13, pData points to 0xff, 0x
*/
static int _Find0BitInArray(const U8 * pData, unsigned FirstBit, unsigned LastBit) {
  unsigned FirstOff;
  unsigned LastOff;
  U8 Data;
  int i;

  FirstOff = FirstBit >> 3;
  LastOff  = LastBit  >> 3;
  pData   += FirstOff;

  //
  // Handle first byte
  //
  Data = *pData++;
  if (FirstOff == LastOff) {      // Special case where first and last byte are the same ?
    i = _Find0BitInByte(Data, FirstBit & 7, LastBit & 7, FirstOff);
    return i;
  }
  i = _Find0BitInByte(Data, FirstBit & 7, 7, FirstOff);
  if (i >= 0) {
    return i + (FirstOff << 3);
  }
  //
  // Handle complete bytes
  //
  for (i = FirstOff + 1; (unsigned)i < LastOff; i++) {
    Data = *pData++;
    if (Data != 0xFF) {
      return _Find0BitInByte(Data, 0, 7, i);
    }
  }
  //
  // Handle last byte
  //
  Data = *pData;
  return _Find0BitInByte(Data, 0, LastBit & 7, i);
}

/*********************************************************************
*
*       _CalcNumWorkBlocksDefault
*
*   Function description
*     Computes the the default number of work blocks.
*     This is a percentage of number of NAND blocks.
*/
static U32 _CalcNumWorkBlocksDefault(U32 NumPhyBlocks) {
  U32 NumWorkBlocks;

#ifdef FS_NAND_MAX_WORK_BLOCKS
  FS_USE_PARA(NumPhyBlocks);
  NumWorkBlocks = FS_NAND_MAX_WORK_BLOCKS;
#else
  //
  // Allocate 10% of NAND capacity for work blocks
  //
  NumWorkBlocks = NumPhyBlocks >> 7;
  //
  // Limit the number of work blocks to reasonable values
  //
  if (NumWorkBlocks > NUM_WORK_BLOCKS_MAX) {
    NumWorkBlocks = NUM_WORK_BLOCKS_MAX;
  }
  if (NumWorkBlocks < NUM_WORK_BLOCKS_MIN) {
    NumWorkBlocks = NUM_WORK_BLOCKS_MIN;
  }
#endif
  return NumWorkBlocks;
}

/*********************************************************************
*
*       _CalcNumBlocksToUse
*
*   Function description
*     Computes the number of logical blocks available to file system.
*/
static int _CalcNumBlocksToUse(U32 NumPhyBlocks, U32 NumWorkBlocks) {
  int NumLogBlocks;
  int Reserve;

  //
  // Compute the number of logical blocks. These are the blocks which are
  // actually available to the file system and therefor determines the capacity.
  // We reserve a small percentage (about 3%) for bad blocks
  // plus the number of work blocks + 1 info block (first block) + 1 block for copy operations
  //
  NumLogBlocks  = (NumPhyBlocks * 125) >> 7;    // Reserve some blocks for blocks which are or can turn "bad" = unusable. We need at least one block.
  Reserve       = NumWorkBlocks + FS_NAND_NUM_BLOCKS_RESERVED;
  NumLogBlocks -= Reserve;
  return NumLogBlocks;
}

/*********************************************************************
*
*       _CalcNumBlocksToUseOldFormat
*
*   Function description
*     Computes the number of logical blocks available to file system
*     like in the "old" versions of the driver.
*/
static int _CalcNumBlocksToUseOldFormat(U32 NumPhyBlocks, U32 NumWorkBlocks) {
  int NumLogBlocks;

  NumLogBlocks = _CalcNumBlocksToUse(NumPhyBlocks, NumWorkBlocks);
  NumLogBlocks++;                             // More logical blocks available to file system as in the current version
  return NumLogBlocks;
}

/*********************************************************************
*
*       _ComputeAndStoreECC
*
*  Function description
*    Computes the ECC values and writes them into the 64-byte buffer for the redundant area
*/
static void _ComputeAndStoreECC(NAND_INST * pInst, const U32 * pData, U8 * pSpare) {
  int i;
  U32 ecc;
  int NumLoops;

  NumLoops = pInst->BytesPerSector >> 9;                 // 512 bytes are taken care of in one loop
  for (i = 0; i < NumLoops; i++) {
    ecc = FS__ECC256_Calc(pData);
    FS__ECC256_Store(pSpare + SPARE_OFF_ECC00, ecc);
    ecc = FS__ECC256_Calc(pData + 64);
    FS__ECC256_Store(pSpare + SPARE_OFF_ECC10, ecc);
    pData  += 128;
    pSpare += 16;
  }
}

/*********************************************************************
*
*       _ApplyECC
*
*  Function description:
*    Uses the ECC values to correct the data if necessary
*    Works on an entire 2Kbyte page (which is divided into 8 parts of 256 bytes each)
*
*  Return value:
*   -1    - Data block is empty
*    0    - O.K., data is valid. No error in data
*    1    - 1 bit error in data which has been corrected
*    2    - Error in ECC
*    3    - Uncorrectable error
*
*/
static int _ApplyECC(NAND_INST * pInst, U32 * pData, const U8 * pSpare) {
  unsigned r;
  unsigned Result;
  int i;
  U32 ecc;
  int NumLoops;

  NumLoops = pInst->BytesPerSector >> 9;
  Result = 0;
  for (i = 0; i < NumLoops; i++) {
    ecc = FS__ECC256_Load(pSpare + SPARE_OFF_ECC00);
    if (FS__ECC256_IsValid(ecc) == 0) {
      return -1;       // Data block is empty
    }
    r = FS__ECC256_Apply(pData, ecc);
    if (r > Result) {
      Result = r;
    }
    ecc = FS__ECC256_Load(pSpare + SPARE_OFF_ECC10);
    r = FS__ECC256_Apply(pData + 64, ecc);
    if (r > Result) {
      Result = r;
    }
    pData  += 128;
    pSpare += 16;
  }
  return Result;
}

/*********************************************************************
*
*       _ReadApplyDeviceParas
*
*  Function description
*    Reads the device info and computes the parameters stored in the Instance structure
*    such as Number of blocks, Number of Sectors, Sector Size etc.
*
*  Return value:
*    0    - O.K.
*    1    - Error, Could not apply device paras
*/
static int _ReadApplyDeviceParas(NAND_INST * pInst) {
  unsigned BytesPerSector;
  unsigned BytesPerPage;
  unsigned SPB_Shift;
  unsigned PPB_Shift;
  U32      MaxNumBlocks;
  U32      NumBlocks;
  FS_NAND_DEVICE_INFO DeviceInfo;
  U32      NumWorkBlocks;
  int      NumLogBlocks;

  FS_MEMSET(&DeviceInfo, 0, sizeof(DeviceInfo));
  if (pInst->pPhyType->pfInitGetDeviceInfo(pInst->Unit, &DeviceInfo)) {
    FS_DEBUG_ERROROUT((FS_MTYPE_DRIVER, "NAND: Could not read device info. Fatal."));
    return 1;
  }
  MaxNumBlocks = pInst->MaxNumBlocks;
  NumBlocks = DeviceInfo.NumBlocks;
  if (NumBlocks <= pInst->FirstBlock) {
    return 1;                         // Less blocks than configured
  }
  NumBlocks -= pInst->FirstBlock;
  if (MaxNumBlocks) {                 // Is an upper limit configured ?
    if (NumBlocks > MaxNumBlocks) {
      NumBlocks = MaxNumBlocks;
    }
  }
  //
  // Compute a default number of work blocks if the application did not configured it yet.
  //
  if (pInst->NumWorkBlocksConf == 0) {
    NumWorkBlocks = _CalcNumWorkBlocksDefault(NumBlocks);
  } else {
    NumWorkBlocks = pInst->NumWorkBlocksConf;
  }
  //
  // Compute the number of blocks available to file system
  //
  NumLogBlocks = _CalcNumBlocksToUse(NumBlocks, NumWorkBlocks);
  if (NumLogBlocks <= 0) {
    FS_DEBUG_ERROROUT((FS_MTYPE_DRIVER, "NAND: Insufficient logical blocks."));
    return 1;
  }
  pInst->NumPhyBlocks = NumBlocks;
  pInst->NumBitsPhyBlockIndex = FS_BITFIELD_CalcNumBitsUsed(NumBlocks);
  pInst->NumLogBlocks         = (U32)NumLogBlocks;
  pInst->NumWorkBlocks        = NumWorkBlocks;
  BytesPerPage        = 1 << DeviceInfo.BPP_Shift;
  PPB_Shift           = DeviceInfo.PPB_Shift;
  //
  // Adjust BytesPerSector to be <= max. sector size
  //
  SPB_Shift = PPB_Shift;
  BytesPerSector = BytesPerPage;
  while (BytesPerSector > FS_Global.MaxSectorSize) {
    BytesPerSector >>= 1;
    SPB_Shift++;
  }
  pInst->SPB_Shift      = SPB_Shift;
  pInst->BytesPerSector = BytesPerSector;
  pInst->BytesPerPage   = BytesPerPage;
  pInst->NumSectors     = (U32)pInst->NumLogBlocks << pInst->SPB_Shift;
  pInst->PPB_Shift      = PPB_Shift;
  return 0;                   // O.K., successfully identified
}

/*********************************************************************
*
*       _StoreEraseCnt
*
*  Function description
*    Stores the logical block index in the static spare area buffer.
*/
static void _StoreEraseCnt(NAND_INST * pInst, U32 EraseCnt) {
  U8 * pBuffer;

  pBuffer    = (_pSpareAreaData + SPARE_OFF_ERASE_CNT);
  *pBuffer++ = (U8)(EraseCnt >> 24);
  *pBuffer++ = (U8)(EraseCnt >> 16);
  *pBuffer++ = (U8)(EraseCnt >>  8);
  //
  // Last byte is stored at offset 5 or 0 to avoid conflicts with the BAD block marker
  //
  pBuffer    = _pSpareAreaData + (5 - pInst->BadBlockOffset);
  *pBuffer   = (U8)EraseCnt;
}

/*********************************************************************
*
*       _LoadEraseCnt
*
*  Function description
*    Retrieves the erase count from the static spare area buffer.
*/
static U32 _LoadEraseCnt(NAND_INST * pInst, const U8 * pSpare) {
  U32   r;
  const U8  * pBuffer;


  pBuffer = (pSpare + SPARE_OFF_ERASE_CNT);
  r = *pBuffer++;
  r = (r << 8) | *pBuffer++;
  r = (r << 8) | *pBuffer++;
  //
  // Last byte is stored at offset 5 or 0 to avoid conflicts with the BAD block marker
  //
  pBuffer    = pSpare + (5 - pInst->BadBlockOffset);
  r = (r << 8) | *pBuffer;
  return r;
}

/*********************************************************************
*
*       _StoreLBI
*
*  Function description
*    Stores the logical block index in the static spare area buffer.
*/
static void _StoreLBI(unsigned lbi) {
  FS_StoreU16BE(_pSpareAreaData + SPARE_OFF_ADDR1, lbi);
  FS_StoreU16BE(_pSpareAreaData + SPARE_OFF_ADDR2, lbi);
}

/*********************************************************************
*
*       _LoadLBI
*
*  Function description
*    Retrieves the logical block index from the static spare area buffer.
*
*  Return value
*    Block is not assigned:  pInst->NumLogBlocks
*    else                    LogicalBlockIndex  (>= 0, <= pInst->NumLogBlocks)
*/
static unsigned _LoadLBI(NAND_INST * pInst) {
  unsigned  lbi1;
  unsigned  lbi2;

  lbi1 = FS_LoadU16BE(_pSpareAreaData + SPARE_OFF_ADDR1);
  lbi2 = FS_LoadU16BE(_pSpareAreaData + SPARE_OFF_ADDR2);
  if (lbi1 == lbi2) {
    if (lbi1 < pInst->NumLogBlocks) {
      return lbi1;
    }
  }
  return pInst->NumLogBlocks;
}

/*********************************************************************
*
*       _LoadBRSI
*
*  Function description
*    Retrieves the block relative sector index from the static spare area buffer.
*
*  Return value
*    BRSI_INVALID      Sector not assigned
*    else              BRSI
*/
static unsigned _LoadBRSI(NAND_INST * pInst) {
  unsigned  i1;
  unsigned  i2;

  i1 = FS_LoadU16BE(_pSpareAreaData + SPARE_OFF_ADDR1);
  i2 = FS_LoadU16BE(_pSpareAreaData + SPARE_OFF_ADDR2);
  if (i1 == i2) {
    i1 ^= 0x0FFFF;            // Physical to logical conversion
    if (i1 < (unsigned)(1 << pInst->SPB_Shift)) {
      return i1;
    }
  }
  return BRSI_INVALID;
}

/*********************************************************************
*
*       _StoreBRSI
*
*  Function description
*    Writes the block relative sector index into the static spare area buffer.
*/
static void _StoreBRSI(unsigned lbi) {
  lbi ^= 0x0FFFF;            // Logical to physical conversion
  FS_StoreU16BE(_pSpareAreaData + SPARE_OFF_ADDR1, lbi);
  FS_StoreU16BE(_pSpareAreaData + SPARE_OFF_ADDR2, lbi);
}



/*********************************************************************
*
*       _BlockIndex2SectorIndex
*
*  Function description
*    Returns the sector index of the first sector in a block.
*    With 128KB blocks and 2048 byte sectors, this means multiplying with 64.
*    With 128KB blocks and  512 byte sectors, this means multiplying with 256.
*/
static U32 _BlockIndex2SectorIndex(NAND_INST * pInst, unsigned BlockIndex) {
  U32 SectorIndex;
  SectorIndex = (U32)BlockIndex << pInst->SPB_Shift;
  return SectorIndex;
}

/*********************************************************************
*
*       _EraseBlock
*/
static int _EraseBlock(NAND_INST * pInst, unsigned BlockIndex) {
  U32 PageIndex;

  IF_STATS(pInst->StatCounters.EraseCnt++);     // Increment statistics counter if enabled
  BlockIndex += pInst->FirstBlock;
  PageIndex = (U32)BlockIndex << pInst->PPB_Shift;
  return pInst->pPhyType->pfEraseBlock(pInst->Unit, PageIndex);
}


/*********************************************************************
*
*       _PhySectorIndex2PageIndex_Data
*
*  Function description:
*    Converts Logical pages (which can be 512 / 1024 / 2048 bytes) into
*    physical pages with same or larger page size.
*
*  Notes
*    (1) Mapping of sectors to pages
*        In general, sector data is stored before spare data as follows:
*        !-------------------Phy. Page-------------!---- Phy. Spare area -------------!
*        !SectorData0!  ...        !SectorData<n>  !SectorSpare0! ....  SectorSpare<n>!
*        This typically applies only if a 2K device is used with 512 byte sectors.
*/
static U32 _PhySectorIndex2PageIndex_Data(NAND_INST * pInst, U32 PhySectorIndex, unsigned * pOff) {
  unsigned SPP_Shift;
  U32 PageIndex;
  unsigned Mask;

  SPP_Shift = (pInst->SPB_Shift - pInst->PPB_Shift);
  PageIndex = PhySectorIndex;
  if (SPP_Shift) {
    PageIndex >>= SPP_Shift;

    Mask        =  PhySectorIndex & ((1 << SPP_Shift) - 1);
    *pOff      +=  Mask * pInst->BytesPerSector;
  }
  PageIndex += (U32)pInst->FirstBlock << pInst->PPB_Shift;
  return PageIndex;
}

/*********************************************************************
*
*       _PhySectorIndex2PageIndex_Spare
*
*  Function description:
*    Converts Logical pages (which can be 512 / 1024 / 2048 bytes) into
*    physical pages with same or larger page size.
*/
static U32 _PhySectorIndex2PageIndex_Spare(NAND_INST * pInst, U32 PhySectorIndex, unsigned * pOff) {
  unsigned SPP_Shift;
  unsigned Off;
  U32 PageIndex;

  SPP_Shift = (pInst->SPB_Shift - pInst->PPB_Shift);
  PageIndex = PhySectorIndex;
  Off = *pOff;
  Off += pInst->BytesPerPage;        // Move offset from data to spare area
  if (SPP_Shift) {
    PageIndex >>= SPP_Shift;
    Off += (PhySectorIndex & ((1 << SPP_Shift) - 1)) * (pInst->BytesPerSector >> 5);
  }
  *pOff = Off;
  PageIndex += (U32)pInst->FirstBlock << pInst->PPB_Shift;
  return PageIndex;
}

/*********************************************************************
*
*       _ReadDataSpare
*
*  Function description:
*    Reads (a part or all of) the data area
*    as well as (a part or all of) the spare area
*/
static int _ReadDataSpare(NAND_INST * pInst, U32 SectorIndex, void *pData, unsigned Off, unsigned NumBytes, void *pSpare, unsigned OffSpare, unsigned NumBytesSpare) {
  U32 PageIndex;

  IF_STATS(pInst->StatCounters.ReadDataCnt++);     // Increment statistics counter if enabled
  PageIndex = _PhySectorIndex2PageIndex_Data(pInst, SectorIndex, &Off);
  _PhySectorIndex2PageIndex_Spare(pInst, SectorIndex, &OffSpare);
  return pInst->pPhyType->pfReadEx(pInst->Unit, PageIndex, pData, Off, NumBytes, pSpare, OffSpare, NumBytesSpare);
}

/*********************************************************************
*
*       _ReadSpare
*
*  Function description:
*    Reads (a part or all of) the spare area for the given sector
*
*  Note
*    (1)  Alignment
*         For 16-bit NAND flashes, half-word alignment is required
*         ==> pData needs to be 16-bit aligned
*         ==> Off, NumBytes need to be even
*/
static int _ReadSpare(NAND_INST * pInst, U32 SectorIndex, void *pData, unsigned Off, unsigned NumBytes) {
  U32 PageIndex;

// TBD: Assert (See note (1))
  IF_STATS(pInst->StatCounters.ReadSpareCnt++);     // Increment statistics counter if enabled
  PageIndex = _PhySectorIndex2PageIndex_Spare(pInst, SectorIndex, &Off);
  return pInst->pPhyType->pfRead(pInst->Unit, PageIndex, pData, Off, NumBytes);
}

/*********************************************************************
*
*       _ReadSpareByte
*
*  Function description:
*    Reads 1 byte of the spare area of the given sector
*/
static int _ReadSpareByte(NAND_INST * pInst, U32 SectorIndex, U8 *pData, unsigned Off) {
  U8 ab[2];
  int r;

  r = _ReadSpare(pInst, SectorIndex, ab, Off & 0xFE, 2);
  *pData = ab[Off & 1];
  return r;
}

/*********************************************************************
*
*       _ReadSpareIntoStaticBuffer
*
*  Function description:
*    Reads the entire spare area of the given sector into the static buffer
*/
static int _ReadSpareIntoStaticBuffer(NAND_INST * pInst, U32 SectorIndex) {
  return _ReadSpare(pInst, SectorIndex, _pSpareAreaData, 0, pInst->BytesPerSector >> 5);
}

/*********************************************************************
*
*       _WriteSpare
*
*  Function description:
*     Writes into the spare area of a sector.
*
*   Parameters
*     pInst         [IN]  Driver instance
*                   [OUT] ---
*     SectorIndex   Index of the sector to write 
*     pData         [IN]  Sector data
*                   [OUT] ---  
*     Off           Byte offset inside the spare area
*     NumBytes      Number of bytes to write
*
*   Return value
*     ==0   OK
*     !=0   A write error occurred (recoverable)
*/
static int _WriteSpare(NAND_INST * pInst, U32 SectorIndex, void * pData, unsigned Off, unsigned NumBytes) {
  U32 PageIndex;

  IF_STATS(pInst->StatCounters.WriteSpareCnt++);     // Increment statistics counter if enabled
  PageIndex = _PhySectorIndex2PageIndex_Spare(pInst, SectorIndex, &Off);
  return pInst->pPhyType->pfWrite(pInst->Unit, PageIndex, pData, Off, NumBytes);
}

/*********************************************************************
*
*       _WriteDataSpare
*
*  Function description:
*    Writes (a part or all of) the data area
*    as well as (a part or all of) the spare area
*    The important point here is this function performs both operations with a single call to the physical layer,
*    giving the physical layer a chance to perform the operation as one operation, which saves time.
*/
static int _WriteDataSpare(NAND_INST * pInst, U32 SectorIndex, const void * pData, unsigned Off, unsigned NumBytes, const void * pSpare, unsigned OffSpare, unsigned NumBytesSpare) {
  U32 PageIndex;

  IF_STATS(pInst->StatCounters.WriteDataCnt++);     // Increment statistics counter if enabled
  PageIndex = _PhySectorIndex2PageIndex_Data(pInst, SectorIndex, &Off);
  _PhySectorIndex2PageIndex_Spare(pInst, SectorIndex, &OffSpare);
  return pInst->pPhyType->pfWriteEx(pInst->Unit, PageIndex, pData, Off, NumBytes, pSpare, OffSpare, NumBytesSpare);
}


/*********************************************************************
*
*       _WriteSpareByte
*
*  Function description:
*    Writes 1 byte of the spare area of the given sector
*    Since we need 2 byte alignment for 16-bit NAND flahes, we copy this into a 2 byte buffer.
*/
static int _WriteSpareByte(NAND_INST * pInst, U32 SectorIndex, U8 Data, unsigned Off) {
  U8 ab[2];

  ab[Off       & 1] = Data;      // Write data byte into buffer
  ab[(Off + 1) & 1] = 0xff;      // Fill the other byte with 0xff which means "do not change"
  return _WriteSpare(pInst, SectorIndex, ab, Off & 0xFE, 2);
}

/*********************************************************************
*
*       _WriteSpareAreaFromStaticBuffer
*/
static int _WriteSpareAreaFromStaticBuffer(NAND_INST * pInst, U32 SectorIndex) {
  return _WriteSpare(pInst, SectorIndex, _pSpareAreaData, 0, pInst->BytesPerSector >> 5);
}

/*********************************************************************
*
*       _ReadPhySpare
*
*  Function description:
*    Reads (a part or all of) the spare area
*
*   Parameters
*     pInst       [IN]  Driver instance
*                 [OUT] ---
*     PageIndex   Physical index of the page to read from
*     pData       [IN]  ---
*                 [OUT] Data read from the spare area
*     Off         Byte offset in the spare area to start reading
*     NumBytes    Number of bytes to read
*
*   Return value
*     == 0  OK
*     != 0  An error occurred
*/
static int _ReadPhySpare(NAND_INST * pInst, U32 PageIndex, void * pData, unsigned Off, unsigned NumBytes) {
  IF_STATS(pInst->StatCounters.ReadSpareCnt++);     // Increment statistics counter if enabled
  return pInst->pPhyType->pfRead(pInst->Unit, PageIndex, pData, Off + pInst->BytesPerPage, NumBytes);
}

/*********************************************************************
*
*       _ReadPhySpareByte
*
*  Function description:
*    Reads 1 byte of the spare area of the given sector
*
*  Note
*    (1) Usage of this function
*        This function should only be called for one reason: To read a physical page as defined
*        by the manufacturer.
*        This is required to find out which blocks are marked as bad (DO NOT USE).
*        All other code should use sub routines that read on a per sector basis in order to allow
*        formatting / accessing 2K NAND flashes with 512 byte sectors (saving a lot of RAM)
*/
static int _ReadPhySpareByte(NAND_INST * pInst, U32 PageIndex, U8 * pData, unsigned Off) {
  U8 ab[2];
  int r;

  PageIndex += pInst->FirstBlock << pInst->PPB_Shift;
  r = _ReadPhySpare(pInst, PageIndex, ab, Off & 0xFE, 2);      // make sure we have 2-byte alignment required by 16-bit NAND flashes
  *pData = ab[Off & 1];
  return r;
}

/*********************************************************************
*
*       _PreEraseBlock
*
*  Function description:
*    Pre-erasing means writing an value into the data status which indicates that
*    the data is invalid and the block needs to be erased.
*/
static int _PreEraseBlock(NAND_INST * pInst, unsigned PhyBlockIndex) {
  U32 SectorIndex;

  SectorIndex = _BlockIndex2SectorIndex(pInst, PhyBlockIndex);
  return _WriteSpareByte(pInst, SectorIndex, 0, SPARE_OFF_DATA_STATUS);
}

/*********************************************************************
*
*       _MarkBlockAsFree
*
*  Function description
*    Mark block as free in management data.
*/
static void _MarkBlockAsFree(NAND_INST * pInst, unsigned iBlock) {
  U8 Mask;
  U8 * pData;

  Mask = 1 << (iBlock & 7);
  if(iBlock >= pInst->NumPhyBlocks) {
    return;
  }
  pData = pInst->pFreeMap + (iBlock >> 3);
#if FS_NAND_ENABLE_STATS
  if ((*pData & Mask) == 0) {
    pInst->StatCounters.NumFreeBlocks++;
  }
#endif
  *pData |= Mask;    // Mark block as free

}

/*********************************************************************
*
*       _MarkBlockAsAllocated
*
*  Function description
*    Mark block as allocated in management data.
*/
static void _MarkBlockAsAllocated(NAND_INST * pInst, unsigned iBlock) {
  U8 Mask;
  U8 * pData;

  Mask = 1 << (iBlock & 7);
  pData = pInst->pFreeMap + (iBlock >> 3);
#if FS_NAND_ENABLE_STATS
  if (*pData & Mask) {
    pInst->StatCounters.NumFreeBlocks--;
  }
#endif
  *pData &= ~(unsigned)Mask;    // Mark block as allocated
}

/*********************************************************************
*
*       _BlockIsFree
*
*  Function description
*    Return if block is free.
*/
static char _BlockIsFree(NAND_INST * pInst, unsigned iBlock) {
  U8 Mask;
  U8 * pData;

  Mask = 1 << (iBlock & 7);
  pData = pInst->pFreeMap + (iBlock >> 3);
  return *pData & Mask;
}

/*********************************************************************
*
*       _ClearStaticSpareArea
*
*  Function description
*    Fills the static spare area with 0xFF.
*/
static void _ClearStaticSpareArea(unsigned SpareAreaSize) {
  FS_MEMSET(_pSpareAreaData, 0xFF, SpareAreaSize);
}

/*********************************************************************
*
*       _ClearStaticSpareAreaExceptECC
*
*   Function description
*     Fills the static spare area with 0xFF, except the bytes which store the ECC.
*     Used by the sector copy routine to speed up the write operation.
*/
static void _ClearStaticSpareAreaExceptECC(unsigned SpareAreaSize) {
  int   i;
  int   NumLoops;
  U8  * pSpare;

  NumLoops = SpareAreaSize >> 4;                 // 16 bytes are taken care of in one loop
  pSpare   = _pSpareAreaData;
  for (i = 0; i < NumLoops; i++) {
    FS_MEMSET(pSpare,                   0xFF, SPARE_OFF_ECC10);
    FS_MEMSET(&pSpare[SPARE_OFF_ADDR2], 0xFF, sizeof(U16));
    pSpare += 16;
  }
}

/*********************************************************************
*
*       _WriteSector
*
*  Function description
*    Performs the following:
*    - Computes ECC and stores it into static spare area
*    - Write entire sector & spare area into NAND flash (in one operations if possible)
*
*  Return value
*        0     O.K., page data has been successfully written
*     != 0     Error
*
*  Notes
*    Before the function call, the static spare area needs to contain info for the page (such as lbi, EraseCnt, etc)
*/
static int _WriteSector(NAND_INST * pInst, const U32 * pBuffer, U32 SectorIndex) {
  _ComputeAndStoreECC(pInst, (const U32 *)pBuffer, _pSpareAreaData);
  return _WriteDataSpare(pInst, SectorIndex, pBuffer, 0, pInst->BytesPerSector, _pSpareAreaData, 0, pInst->BytesPerSector >> 5);
}

/*********************************************************************
*
*       _MarkBlockAsBad
*
*   Function description:
*     The first byte of the spare area in each block is used to mark the block as bad.
*     If it is != 0xFF, then this block will not be used any more.
*
*   Parameters
*     pInst           [IN]  Driver instance
*                     [OUT] ---
*     BlockIndex      Index of the block to mark as "bad"
*     ErrorType       Reason why the block is marked as "bad"
*     ErrorBRSI       Index of the physical sector where the error occurred
*/
static int _MarkBlockAsBad(NAND_INST * pInst, unsigned BlockIndex, int ErrorType, unsigned ErrorBRSI) {
  U32        SectorIndex;
  U8       * pSpare;
  const U8 * pId;
  U8       * pData;
  int        i;

  IF_STATS(pInst->StatCounters.NumBadBlocks++);
  SectorIndex = _BlockIndex2SectorIndex(pInst, BlockIndex);
  //
  // We write "SEGGER" on the spare area of the block to be able to distinguish from the other "bad" blocks marked by manufacturer.
  // Additional information about the reason why the block was marked as "bad" is saved on the data area of the first page.
  //
  _EraseBlock(pInst, BlockIndex);
  _ClearStaticSpareArea(pInst->BytesPerSector >> 5);
  pData = (U8 *)_pSectorBuffer;
  FS_MEMSET(pData, 0xFF, pInst->BytesPerSector);
  FS_StoreU16BE(pData + INFO_OFF_BAD_MARK_ERROR_TYPE, ErrorType);
  FS_StoreU16BE(pData + INFO_OFF_BAD_MARK_ERROR_BRSI, ErrorBRSI);
  _WriteSector(pInst, _pSectorBuffer, SectorIndex);
  pSpare = _pSpareAreaData;
  pId    = _acInfo; 
  for (i = 0; i < 7; ++i) {             // 6 bytes for "SEGGER" and one byte for the "bad" block mark
    if (pInst->BadBlockOffset == i) {
      *pSpare++ = BAD_BLOCK_MARKER;
    } else {
      *pSpare++ = *pId++;
    }
  }
  return _WriteSpareAreaFromStaticBuffer(pInst, SectorIndex);
}

/*********************************************************************
*
*       _BlockIsBad
*
*   Function description
*     Checks whether a block can be used to store data.
*     The good/bad status is read from the spare area of the first and second page.
*
*   Return values
*     ==0   Block can be used to store data
*     !=0   Block is defect
*/
static int _BlockIsBad(NAND_INST * pInst, unsigned BlockIndex) {
  U32 PageIndex;
  U8  BlockStatus;
  U32 OffStatus;

  OffStatus = (pInst->BytesPerPage == 512) ? 5 : 0;
  PageIndex = (U32)BlockIndex << pInst->PPB_Shift;
  _ReadPhySpareByte(pInst, PageIndex, &BlockStatus, OffStatus);
  if (BlockStatus == GOOD_BLOCK_MARKER) {
    _ReadPhySpareByte(pInst, PageIndex + 1, &BlockStatus, OffStatus);
    if (BlockStatus == GOOD_BLOCK_MARKER) {
      return 0;       // OK, block can be used to store data.
    }
  }
  return 1;           // Block is defect.
}

/*********************************************************************
*
*       _BlockCanBeErased
*
*   Function description
*     Checks whether the driver is allowed to erase the given block.
*     The blocks marked as bad by the manufacturer are never erased.
*     Erasing of the blocks marked as bad by the driver can be explicitly 
*     enabled/disable via a compile time switch.
*/
static int _BlockCanBeErased(NAND_INST * pInst, unsigned BlockIndex) {
  if (_BlockIsBad(pInst, BlockIndex) == 0) {
    return 1;             // Block is not bad, it can be erased.
  }
#if FS_NAND_RECLAIM_DRIVER_BAD_BLOCKS
  {
    const U8 * pInfo;
    U8       * pSpare;
    U8         aSpare[7]; // 6 bytes for "SEGGER" and one byte for the "bad" block mark
    unsigned   i;
    U32        PageIndex;
    U32        OffStatus;

    //
    // Check whether the block was marked bad by the driver. If so tell the caller it can erase it.
    //
    PageIndex = (U32)BlockIndex << pInst->PPB_Shift;
    OffStatus = (pInst->BytesPerPage == 512) ? 5 : 0;
    _ReadPhySpare(pInst, PageIndex, aSpare, 0, sizeof(aSpare));
    pInfo  = _acInfo;
    pSpare = aSpare;
    for (i = 0; i < sizeof(aSpare); ++i) {
      if (OffStatus != i) {
        if (*pSpare != *pInfo) {
          break;                    // Driver signature for bad block do not match. Block marked as bad by the manufacturer.
        }
        ++pInfo;
      }
      ++pSpare;
    }
    if (i == sizeof(aSpare)) {
      return 1;                     // Driver marked the block as bad. This block can be erased.
    }
  }
#endif
  return 0;
}

/*********************************************************************
*
*       _ReadSectorWithECC
*
*  Function description
*    Performs the following:
*    - Reads an page data into the specified buffer & spare area into local area
*    - Performs error correction on the data
*
*   Parameters
*     pInst         [IN]  Driver instance
*                   [OUT] ---
*     pBuffer       [IN]  ---
*                   [OUT] Contents of the read sector
*     SectorIndex   Index of the sector to read
*
*  Return value
*     -1                        OK, page is blank
*     NAND_NO_ERROR             OK
*     NAND_1BIT_CORRECTED       OK
*     NAND_ERROR_IN_ECC         OK
*     NAND_UNCORRECTABLE_ERROR  Error
*     NAND_READ_ERROR           Error
*
*  Notes
*    Before the function call, the static spare area needs to contain info for the page!
*/
static int _ReadSectorWithECC(NAND_INST * pInst, U32 * pBuffer, U32 SectorIndex) {
  int r;
  int NumRetries;

  NumRetries = FS_NAND_NUM_READ_RETRIES;
  while (1) {
  //
  // Read data and the entire spare area
  //
  r = _ReadDataSpare(pInst, SectorIndex, pBuffer, 0, pInst->BytesPerSector, _pSpareAreaData, 0, pInst->BytesPerSector >> 5);
  if (r) {
      r = NAND_READ_ERROR;          // Error while reading, try again
      goto Retry;
    }
    //
    // Check the ECC of sector data and correct one bit errors
    //
    r = _ApplyECC(pInst, (U32 *)pBuffer, _pSpareAreaData);
    if (r < 0) {
      return r;                     // Sector has no data
    }
    if ((r == NAND_NO_ERROR) || (r == NAND_1BIT_CORRECTED)) {
      return r;                     // Data has no errors
    }
Retry:
    if (NumRetries-- == 0) {
      break;
    }
    IF_STATS(pInst->StatCounters.NumReadRetries++);
  }
  return r;
}


/*********************************************************************
*
*       _L2P_Read
*
*  Function Description
*    Returns the contents of the given entry in the L2P table (physical block lookup table)
*
*/
static unsigned _L2P_Read(NAND_INST * pInst, U32 LogIndex) {
  U32      v;

  v = FS_BITFIELD_ReadEntry(pInst->pLog2PhyTable, LogIndex, pInst->NumBitsPhyBlockIndex);
  return v;
}

/*********************************************************************
*
*       _L2P_Write
*
*  Function Description
*    Updates the contents of the given entry in the L2P table (physical block lookup table)
*
*/
static void _L2P_Write(NAND_INST * pInst, U32 LogIndex, unsigned v) {
  FS_BITFIELD_WriteEntry(pInst->pLog2PhyTable, LogIndex, pInst->NumBitsPhyBlockIndex, v);
}

/*********************************************************************
*
*       _L2P_GetSize
*
*  Function Description
*    Computes & returns the size of the L2P assignment table of a work block.
*    Is used before allocation to find out how many bytes need to be allocated.
*/
static unsigned _L2P_GetSize(NAND_INST * pInst) {
  unsigned v;

  v = FS_BITFIELD_CalcSize(pInst->NumLogBlocks, pInst->NumBitsPhyBlockIndex);
  return v;
}

/*********************************************************************
*
*       _WB_SectorIsWritten
*
*  Function Description
*    Returns if a sector in a work block is used (written) by looking at the 1 bit paIsWritten-array.
*    A set bit indicates that the sector in question has been written.
*
*  Return value
*    0      Sector not written
*  !=0      Sector written
*/
static char _WB_SectorIsWritten(WORK_BLOCK_DESC * pWorkBlock, unsigned brsi) {
  unsigned Data;

  Data =   *(pWorkBlock->paIsWritten + (brsi >> 3));
  Data >>= brsi & 7;
  return Data & 1;
}

/*********************************************************************
*
*       _WB_MarkSectorAsUsed
*
*  Function Description
*    Mark sector as used in work block
*/
static void _WB_MarkSectorAsUsed(WORK_BLOCK_DESC * pWorkBlock, unsigned brsi) {
  U8 Mask;
  unsigned Off;

  Off  = brsi >> 3;
  Mask = 1 << (brsi & 7);
  *(pWorkBlock->paIsWritten + Off) |= Mask;
}

/*********************************************************************
*
*       _WB_ReadAssignment
*
*  Function Description
*    Reads an entry in the assignment table of a work block.
*    It is necessary to use a subroutine to do the job since the entries are stored in a bitfield.
*    Logically, the code does the following:
*      return pWorkBlock->aAssign[Index];
*/
static unsigned _WB_ReadAssignment(NAND_INST * pInst, WORK_BLOCK_DESC * pWorkBlock, unsigned Index) {
  unsigned r;

  r = FS_BITFIELD_ReadEntry((const U8 *)pWorkBlock->paAssign, Index, pInst->SPB_Shift);
  return r;
}

/*********************************************************************
*
*       _WB_WriteAssignment
*
*  Function Description
*    Writes an entry in the assignment table of a work block.
*    It is necessary to use a subroutine to do the job since the entries are stored in a bitfield.
*    Logically, the code does the following:
*      pWorkBlock->aAssign[Index] = v;
*/
static void _WB_WriteAssignment(NAND_INST * pInst, WORK_BLOCK_DESC * pWorkBlock, unsigned Index, unsigned v) {
  FS_BITFIELD_WriteEntry((U8 *)pWorkBlock->paAssign, Index, pInst->SPB_Shift, v);
}

/*********************************************************************
*
*       _WB_GetAssignmentSize
*
*  Function Description
*    Returns the size of the assignment table of a work block.
*/
static unsigned _WB_GetAssignmentSize(NAND_INST * pInst) {
  unsigned v;

  v = FS_BITFIELD_CalcSize(1 << pInst->SPB_Shift, pInst->SPB_Shift);
  return v;
}

/*********************************************************************
*
*       _FindFreeSectorInWorkBlock
*
*  Function Description
*    Locate a free sector in a work block.
*    If available we try to locate the brsi at the "native" position, meaning physical brsi = logical brsi,
*    because this leaves the option to later convert the work block into a data block without copying the data.
*
*  Returns
*    != BRSI_INVALID  brsi    If free sector has been found
*    == BRSI_INVALID          No free sector
*/
static unsigned _FindFreeSectorInWorkBlock(NAND_INST * pInst, WORK_BLOCK_DESC * pWorkBlock, unsigned brsi) {
  unsigned SectorsPerBlock;
  int i;

  //
  // Preferred position is the real position within the block. So we first check if it is available.
  //
  if (_WB_SectorIsWritten(pWorkBlock, brsi) == 0) {
    return brsi;
  }
  //
  // Preferred position is taken. Let's use first free position.
  //
  SectorsPerBlock = 1 << pInst->SPB_Shift;
  i = _Find0BitInArray(pWorkBlock->paIsWritten, 1, SectorsPerBlock - 1);     // Returns bit position (1 ... SectorsPerBlock - 1) if a 0-bit has been found, else -1
  if (i > 0) {
    return i;
  }
  return BRSI_INVALID;     // No free Sector in this block
}

/*********************************************************************
*
*       _WB_RemoveFromList
*
*  Function Description
*    Removes a given work block from list of work blocks.
*/
static void _WB_RemoveFromList(WORK_BLOCK_DESC * pWorkBlock, WORK_BLOCK_DESC ** ppFirst) {
  //
  // Unlink Front: From head or previous block
  //
  if (pWorkBlock == *ppFirst) {           // This WB first in list ?
    *ppFirst = pWorkBlock->pNext;
  } else {
    pWorkBlock->pPrev->pNext = pWorkBlock->pNext;
  }
  //
  // Unlink next if pNext is valid
  //
  if (pWorkBlock->pNext) {
    pWorkBlock->pNext->pPrev = pWorkBlock->pPrev;
  }
}

/*********************************************************************
*
*       _WB_AddToList
*
*  Function Description
*    Adds a given work block to the beginning of the list of work block descriptors.
*/
static void _WB_AddToList(WORK_BLOCK_DESC * pWorkBlock, WORK_BLOCK_DESC ** ppFirst) {
  WORK_BLOCK_DESC * pPrevFirst;

  pPrevFirst = *ppFirst;
  pWorkBlock->pPrev = NULL;    // First entry
  pWorkBlock->pNext = pPrevFirst;
  if (pPrevFirst) {
    pPrevFirst->pPrev = pWorkBlock;
  }
  *ppFirst = pWorkBlock;
}

/*********************************************************************
*
*       _WB_RemoveFromUsedList
*
*  Function Description
*    Removes a given work block from list of used work blocks.
*/
static void _WB_RemoveFromUsedList(NAND_INST * pInst, WORK_BLOCK_DESC * pWorkBlock) {
  _WB_RemoveFromList(pWorkBlock, &pInst->pFirstWorkBlockInUse);
}

/*********************************************************************
*
*       _WB_AddToUsedList
*
*  Function Description
*    Adds a given work block to the list of used work blocks.
*/
static void _WB_AddToUsedList(NAND_INST * pInst, WORK_BLOCK_DESC * pWorkBlock) {
  _WB_AddToList(pWorkBlock, &pInst->pFirstWorkBlockInUse);
}

/*********************************************************************
*
*       _WB_RemoveFromFreeList
*
*  Function Description
*    Removes a given work block from list of free work blocks.
*/
static void _WB_RemoveFromFreeList(NAND_INST * pInst, WORK_BLOCK_DESC * pWorkBlock) {
  _WB_RemoveFromList(pWorkBlock, &pInst->pFirstWorkBlockFree);
}

/*********************************************************************
*
*       _WB_AddToFreeList
*
*  Function Description
*    Adds a given work block to the list of free work blocks.
*/
static void _WB_AddToFreeList(NAND_INST * pInst, WORK_BLOCK_DESC * pWorkBlock) {
  _WB_AddToList(pWorkBlock, &pInst->pFirstWorkBlockFree);
}

/*********************************************************************
*
*       _SectorDataIsWritten
*
*   Function description
*     Checks whether the data of a sector was changed at least one time.
*
*   Parameters
*     pInst         [IN]  Driver instance
*                   [OUT] ---
*     SectorIndex   Index of the sector to be checked
*
*   Return value
*     ==1     Sector has been written
*     ==0     Sector empty
*/
static int _SectorDataIsWritten(NAND_INST * pInst, U32 SectorIndex) {
  U32 ecc;

  //
  // We check the ECC validity of the first 256 bytes to detemine if the sector has been written.
  //
  _ReadSpareIntoStaticBuffer(pInst, SectorIndex);
  ecc = FS__ECC256_Load(_pSpareAreaData + SPARE_OFF_ECC00);
  return FS__ECC256_IsValid(ecc);
}

/*********************************************************************
*
*       _SectorDataIsInvalidated
*
*   Function description
*     Checks whether the data of a sector has been invalidated by a "free sectors" command.
*
*   Parameters
*     pInst         [IN]  Driver instance
*                   [OUT] ---
*     SectorIndex   Index of the sector to be checked
*
*   Return value
*     ==1     Sector data invalidated
*     ==0     Sector data not invalidated
*
*   Notes
*       (1) First sector in the block requires special treatement.
*           The spare area of this sector is used up with block related information.
*           We store the flag which indicates whether the sector data was invalidated
*           or not in the spare area of the second sector at the next available byte offset.
*/
static int _SectorDataIsInvalidated(NAND_INST * pInst, U32 SectorIndex) {
  U32      Mask;
  U8       Data8;
  unsigned Off;

  //
  // First check if the sector data has been invalidated by a "free sectors" command.
  //
  Off  = SPARE_OFF_SECTOR_FREE;
  Mask = (1 << pInst->SPB_Shift) - 1;
  if ((SectorIndex & Mask) == 0) {      // First sector in the block ? (See note 1)
    Off = SPARE_OFF_SECTOR0_FREE;
    ++SectorIndex;
  }
  _ReadSpareByte(pInst, SectorIndex, &Data8, Off);
  return Data8 == 0 ? 1 : 0;            // Reversed logic: ==0 means invalidated
}

/*********************************************************************
*
*       _SectorDataIsInvalidatedFast
*
*   Function description
*     Same as _SectorDataIsInvalidated() above, but assumes that the spare area for the given sector has already been read into the static buffer
*/
static int _SectorDataIsInvalidatedFast(NAND_INST * pInst, U32 SectorIndex) {
  U32 Mask;
  U8  Data8;

  //
  // First check if the sector data has been invalidated by a "free sectors" command.
  //
  Mask = (1 << pInst->SPB_Shift) - 1;
  if ((SectorIndex & Mask) == 0) {      // First sector in the block ?
    _ReadSpareByte(pInst, SectorIndex + 1, &Data8, SPARE_OFF_SECTOR0_FREE);
  } else {
    Data8 = *(_pSpareAreaData + SPARE_OFF_SECTOR_FREE);
  }
  return Data8 == 0 ? 1 : 0;            // Reversed logic: ==0 means invalidated
}

/*********************************************************************
*
*       _InvalidateSectorData
*
*  Function description
*    Sets the flag which indicates that the data of a sector is not valid anymore.
*    Typ. called by the "free sectors" operation.
*
*   Parameters
*     pInst         [IN]  Driver instance
*                   [OUT] ---
*     SectorIndex   Index of the sector to be marked as free
*
*   Return value
*     ==0     OK
*     !=0     An error occurred
*/
#if FS_NAND_ENABLE_TRIM
static void _InvalidateSectorData(NAND_INST * pInst, U32 SectorIndex) {
  U32      Mask;
  U8       Data8;
  unsigned Off;

  //
  // First sector in the block requires special treatement (see Note 1 of _SectorDataIsInvalidated()).
  //
  Off   = SPARE_OFF_SECTOR_FREE;
  Mask  = (1 << pInst->SPB_Shift) - 1;
  Data8 = 0;
  if ((SectorIndex & Mask) == 0) {      // First sector in the block ?
    Off = SPARE_OFF_SECTOR0_FREE;
    ++SectorIndex;
  }
  _WriteSpareByte(pInst, SectorIndex, Data8, Off);
}
#endif // FS_NAND_ENABLE_TRIM

/*********************************************************************
*
*       _InvalidateSectorDataFast
*
*  Function description
*     Same as _InvalidateSectorData() above, but assumes that the caller writes the spare data to medium from static buffer
*/
#if FS_NAND_ENABLE_TRIM
static void _InvalidateSectorDataFast(NAND_INST * pInst, U32 SectorIndex) {
  U32 Mask;
  U8  Data8;

  Mask  = (1 << pInst->SPB_Shift) - 1;
  Data8 = 0;
  if ((SectorIndex & Mask) == 0) {      // First sector in the block ?
    _WriteSpareByte(pInst, SectorIndex + 1, Data8, SPARE_OFF_SECTOR0_FREE);
  } else {
    *(_pSpareAreaData + SPARE_OFF_SECTOR_FREE) = Data8;
  }
}
#endif // FS_NAND_ENABLE_TRIM

/*********************************************************************
*
*       _brsiLog2Phy
*
*  Function Description
*    Converts a logical brsi (block relative sector index) into a physical brsi
*
*  Returns
*    brsi             If free sector has been found
*    BRSI_INVALID     On error: No free sector
*/
static unsigned _brsiLog2Phy(NAND_INST * pInst, WORK_BLOCK_DESC * pWorkBlock, unsigned LogBRSI) {
  unsigned PhyBRSI;

  //
  //  In case of logical sector index <> 0 we need to check the physical sector index.
  //  The physical sector index of such a logical sector index will never be zero,
  //  since we do not assign such a value to a logical sector index.
  //  (see function _FindFreeSectorInWorkBlock)
  //
  if (LogBRSI) {
    PhyBRSI = _WB_ReadAssignment(pInst, pWorkBlock, LogBRSI);
    if (PhyBRSI == 0) {
      return BRSI_INVALID;
    }
    return PhyBRSI;
  }
  //
  // LogBRSI == 0 (First sector in block) requires special handling.
  //
  if (_WB_SectorIsWritten(pWorkBlock, 0) == 0) {
    return BRSI_INVALID;
  }
  PhyBRSI = _WB_ReadAssignment(pInst, pWorkBlock, 0);
#if FS_NAND_ENABLE_TRIM
  //
  // PhyBRSI == 0 has 2 different meanings:
  //    1. Logical sector 0 is stored on the first physical sector
  //    2. Logical sector 0 has been invalidated
  // We have to differentiate between these and return the correct value.
  //
  if (PhyBRSI == 0) {
    U32 PhySectorIndex;

    PhyBRSI        = BRSI_INVALID;
    PhySectorIndex = _BlockIndex2SectorIndex(pInst, pWorkBlock->pbi);
    if (_SectorDataIsInvalidated(pInst, PhySectorIndex) == 0) {
      PhyBRSI = 0;
    }
  }
#endif // FS_NAND_ENABLE_TRIM
  return PhyBRSI;
}

/*********************************************************************
*
*       _AllocWorkBlockDesc
*
*  Function description
*    Allocates a WORK_BLOCK_DESC from the array in the pInst structure.
*/
static WORK_BLOCK_DESC * _AllocWorkBlockDesc(NAND_INST * pInst, unsigned lbi) {
  WORK_BLOCK_DESC * pWorkBlock;

  //
  // Check if a free block is available.
  //
  pWorkBlock = pInst->pFirstWorkBlockFree;
  if (pWorkBlock) {
    unsigned NumBytes;
    //
    // Initialize work block descriptor,
    // mark it as in use and add it to the list
    //
    NumBytes = _WB_GetAssignmentSize(pInst);
    _WB_RemoveFromFreeList(pInst, pWorkBlock);
    _WB_AddToUsedList(pInst, pWorkBlock);
    pWorkBlock->lbi     = lbi;
    FS_MEMSET(pWorkBlock->paIsWritten, 0, 1 << (pInst->SPB_Shift - 3));   // Mark all entries as unused: Work block does not yet contain any sectors (data)
    FS_MEMSET(pWorkBlock->paAssign,    0, NumBytes);                      // Make sure that no old assignment info from previous descriptor is in the table
  }
  return pWorkBlock;
}

/*********************************************************************
*
*       _MarkBlock
*
*  Function Description
*    Marks a block as block of given type
*     We write only the 16 bytes actually required, even though the spare area may be larger (64 bytes for large-page NANDS with 2K sectors)
*
*   Parameters
*     pInst       [IN]  Driver instance
*                 [OUT] ---
*     pbi         Index of physical block to write
*     lbi         Index of logical block assigned to physical block
*     EraseCnt    Number of times the block has been erased
*     DataStat    Status of the data in the block (valid, invalid, bad)
*
*   Return values
*     ==0   OK
*     !=0   A write error occurred (recoverable)
*/
static int _MarkBlock(NAND_INST * pInst, unsigned pbi, unsigned lbi, U32 EraseCnt, U8 DataStat) {
  int NumBytes;

  NumBytes = 16;
  _ClearStaticSpareArea(NumBytes);
  _StoreEraseCnt(pInst, EraseCnt);
  _StoreLBI(lbi);
  *(_pSpareAreaData + SPARE_OFF_DATA_STATUS) = DataStat;
  return _WriteSpare(pInst, _BlockIndex2SectorIndex(pInst, pbi), _pSpareAreaData, 0, NumBytes);
}

/*********************************************************************
*
*       _MarkAsWorkBlock
*
*  Function Description
*    Marks a block as work block.
*
*   Parameters
*     pInst       [IN]  Driver instance
*                 [OUT] ---
*     pbi         Index of physical block to write 
*     lbi         Index of logical block assigned to physical block
*     EraseCnt    Number of times the block has been erased
*
*   Return values
*     ==0   OK
*     !=0   A write error occurred (recoverable)
*/
static int _MarkAsWorkBlock(NAND_INST * pInst, unsigned pbi, unsigned lbi, U32 EraseCnt) {
  return _MarkBlock(pInst, pbi, lbi, EraseCnt, 0xf | (DATA_STAT_WORK << 4));
}

/*********************************************************************
*
*       _MarkAsDataBlock
*
*  Function Description
*    Marks a block as data block.
*
*   Parameters
*     pInst       [IN]  Driver instance
*                 [OUT] ---
*     pbi         Index of physical block to write 
*     lbi         Index of logical block assigned to physical block
*     EraseCnt    Number of times the block has been erased
*     DataCnt     Number of times the data block was copied. Used to determine which of two data blocks with the same LBI is the most recent one (see _BlockDataIsMoreRecent).
*
*   Return values
*     ==0   OK
*     !=0   A write error occurred (recoverable)
*/
static int _MarkAsDataBlock(NAND_INST * pInst, unsigned pbi, unsigned lbi, U32 EraseCnt, U8 DataCnt) {
  return _MarkBlock(pInst, pbi, lbi, EraseCnt, (U8)((DataCnt & 0xf) | (DATA_STAT_VALID << 4)));
}

/*********************************************************************
*
*       _OnFatalError
*
*   Function description:
*     Called when a fatal error occurrs. It switches to read-only mode
*     and sets the error flag.
*
*   Parameters
*     pInst             [IN]  Driver instance
*                       [OUT] ---
*     ErrorType         Identifies the error
*     ErrorSectorIndex  Index of the physical sector where the error occurred
*/
static void _OnFatalError(NAND_INST * pInst, int ErrorType, unsigned ErrorSectorIndex) {
  if (pInst->IsWriteProtected == 0) {
    U8 * pPageBuffer;

    pInst->IsWriteProtected = 1;
    pInst->HasFatalError    = 1;
    pInst->ErrorType        = (U8)ErrorType;
    pInst->ErrorSectorIndex = ErrorSectorIndex;
    FS_DEBUG_ERROROUT((FS_MTYPE_DRIVER, "NAND: FATAL error: Error %d occurred on sector %u.", ErrorType, ErrorSectorIndex));
    if (_pfOnFatalError != NULL) {
      FS_NAND_FATAL_ERROR_INFO FatalErrorInfo;
      int Action;
  
      FatalErrorInfo.Unit = pInst->Unit;
      Action = _pfOnFatalError(&FatalErrorInfo);
      if (Action) {
        return;       // Application chose to ignore the fatal error.s
      }
    }
    FS_DEBUG_ERROROUT((FS_MTYPE_DRIVER, "NAND: Switching permanently to read-only mode."));
    //
    // Save the write protected status and the error information into the first block
    //
    pPageBuffer = (U8 *)_pSectorBuffer;
    FS_MEMSET(pPageBuffer, 0xFF, pInst->BytesPerSector);
    FS_StoreU16BE(pPageBuffer + INFO_OFF_IS_WRITE_PROTECTED, 0);      // Inverted, 0 means write protected
    FS_StoreU16BE(pPageBuffer + INFO_OFF_HAS_FATAL_ERROR,    0);      // Inverted, 0 means has fatal error
    FS_StoreU16BE(pPageBuffer + INFO_OFF_FATAL_ERROR_TYPE,         ErrorType);
    FS_StoreU32BE(pPageBuffer + INFO_OFF_FATAL_ERROR_SECTOR_INDEX, ErrorSectorIndex);
    _ClearStaticSpareArea(pInst->BytesPerSector >> 5);
    _WriteSector(pInst, _pSectorBuffer, ERROR_INFO_SECTOR_INDEX);
  }
}

/*********************************************************************
*
*       _MakeBlockAvailable
*
*   Function description
*     Marks the data of a block as invalid and puts it in the list of free blocks.
*
*   Parameters
*     pInst       [IN]  Driver instance
*                 [OUT] ---
*     pbi         Index of the physical block to be marked as free
*     EraseCnt      Number of times the block has been erased. This information is used to update the NumBlocksEraseCntMin
*
*   Return value
*     ==0   OK
*     !=0   A write error occurred, recoverable
*/
static int _MakeBlockAvailable(NAND_INST * pInst, unsigned pbi, U32 EraseCnt) {
  int r;
  U8  DataStat;
  U32 SectorIndex;

  r = 0;
  //
  // Block 0 stores only management information and can never be freed
  //
  if (pbi) {
    //
    // Mark block as invalid and put it to the free list.
    //
    SectorIndex = _BlockIndex2SectorIndex(pInst, pbi);
    DataStat    = DATA_STAT_INVALID << 4;
    r = _WriteSpareByte(pInst, SectorIndex, DataStat, SPARE_OFF_DATA_STATUS);
    _MarkBlockAsFree(pInst, pbi);
    if (pInst->NumBlocksEraseCntMin && (pInst->EraseCntMin == EraseCnt)) {
      pInst->NumBlocksEraseCntMin--;
    }
  }
  return r;
}

/*********************************************************************
*
*       _CopySectorWithECC
*
*   Function description
*     Copies the data of a sector into another sector. During copy
*     the ECC of the source data is checked.
*
*   Parameters
*     pInst             [IN]  Driver instance
*                       [OUT] ---
*     SectorIndexSrc    Source physical sector index
*     SectorIndexDest   Destination physical sector index
*     brsi              Block relative index of the copied sector
*
*   Return value
*     NAND_NO_ERROR             OK
*     NAND_1BIT_CORRECTED       OK
*     NAND_ERROR_IN_ECC         OK
*     NAND_UNCORRECTABLE_ERROR  Error, fatal
*     NAND_READ_ERROR           Error, fatal
*     NAND_WRITE_ERROR          Error, recoverable
*/
static int _CopySectorWithECC(NAND_INST * pInst, U32 SectorIndexSrc, U32 SectorIndexDest, unsigned brsi) {
  int rRead;
  int rWrite;

  rRead = _ReadSectorWithECC(pInst, _pSectorBuffer, SectorIndexSrc);
  if ((rRead == NAND_NO_ERROR) ||
      (rRead == NAND_1BIT_CORRECTED) ||
      (rRead == NAND_ERROR_IN_ECC)) {
#if FS_NAND_ENABLE_TRIM
    if (_SectorDataIsInvalidatedFast(pInst, SectorIndexSrc)) {
      return NAND_NO_ERROR;       // Data have been invalidated by a "free sectors" command
    }
#endif
    //
    // Leave the ECC in the spare area to avoid computing it again in the write function.
    //
    _ClearStaticSpareAreaExceptECC(pInst->BytesPerSector >> 5);
    //
    // A bit error in ECC is not corrected by the ECC check routine.
    // It must be re-computed to avoid propagating the bit error to destination sector.
    //
    if (rRead == NAND_ERROR_IN_ECC) {
      _ComputeAndStoreECC(pInst, _pSectorBuffer, _pSpareAreaData);
    }
    //
    // Important when we clean a work block "in place". In case of a power fail the sector is marked as used.
    //
    if (brsi != BRSI_INVALID) {
      _StoreBRSI(brsi);
    }
    rWrite = _WriteDataSpare(pInst, SectorIndexDest, _pSectorBuffer, 0, pInst->BytesPerSector, _pSpareAreaData, 0, pInst->BytesPerSector >> 5);
    if (rWrite) {
      return NAND_WRITE_ERROR;
    }
    IF_STATS(pInst->StatCounters.CopySectorCnt++);
    return rRead;
  }
  if (rRead < 0) {
    return NAND_NO_ERROR;   // Sector blank, nothing to copy
  }
  FS_DEBUG_ERROROUT((FS_MTYPE_DRIVER, "NAND: FATAL error: Could not copy sector with ECC."));
  _OnFatalError(pInst, rRead, SectorIndexSrc);
  return rRead;             // Unrecoverable error
}

/*********************************************************************
*
*       _CountDataBlocksWithEraseCntMin
*
*  Function description
*     Goes through all blocks and counts the data blocks with the
*     lowest erase cnt.
*
*   Parameters
*     pInst       [IN]  Driver instance
*                 [OUT] ---
*     pEraseCnt   [IN]  ---
*                 [OUT] Minimum erase count
*     pPBI        [IN]  ---
*                 [OUT] Index of the first data block with the min erase count
*
*   Return value
*     Number of data blocks found with a min erase count
*/
static U32 _CountDataBlocksWithEraseCntMin(NAND_INST * pInst, U32 * pEraseCnt, unsigned * pPBI) {
  unsigned iBlock;
  unsigned pbi;
  U32      EraseCntMin;
  U32      SectorIndex;
  U32      EraseCnt;
  U32      NumBlocks;

  pbi         = 0;
  EraseCntMin = ERASE_CNT_INVALID;
  NumBlocks   = 0;
  //
  // Read and compare the erase count of all data blocks exept the first one
  // which stores only management information
  //
  for (iBlock = 1; iBlock < pInst->NumPhyBlocks; ++iBlock) {
    U8 DataStat;

    if (_BlockIsFree(pInst, iBlock)) {
      continue;       // Block is not used.
    }
    if (_BlockIsBad(pInst, iBlock)) {
      continue;       // Block is marked as defect.
    }
    SectorIndex    = _BlockIndex2SectorIndex(pInst, iBlock);
    _ReadSpareIntoStaticBuffer(pInst, SectorIndex);
    DataStat       = _pSpareAreaData[SPARE_OFF_DATA_STATUS];
    //
    // Compare the erase count of data blocks only.
    //
    if ((DataStat >> 4) == DATA_STAT_VALID) {
      EraseCnt = _LoadEraseCnt(pInst, _pSpareAreaData);
      if ((EraseCntMin == ERASE_CNT_INVALID) || (EraseCnt < EraseCntMin)) {
        pbi         = iBlock;
        EraseCntMin = EraseCnt;
        NumBlocks   = 1;
      } else if (EraseCnt == EraseCntMin) {
        ++NumBlocks;
      }
    }
  }
  *pEraseCnt = EraseCntMin;
  *pPBI      = pbi;
  return NumBlocks;
}

/*********************************************************************
*
*       _FindDataBlockByEraseCnt
*
*  Function description
*     Goes through all data blocks and returns the first one with
*     the given erase count.
*
*   Parameters
*     pInst     [IN]  Driver instance
*               [OUT] ---
*     EraseCnt  Erase count to lookup for
*
*   Return value
*     == 0  No data block found
*     != 0  Index of the found data block
*/
static unsigned _FindDataBlockByEraseCnt(NAND_INST * pInst, U32 EraseCnt) {
  unsigned iBlock;

  //
  // Read and compare the erase count of all data blocks except the first one
  // which stores only management information
  //
  for (iBlock = 1; iBlock < pInst->NumPhyBlocks; ++iBlock) {
    U32 SectorIndex;
  U8  DataStat;
    U32 DataEraseCnt;

    if (_BlockIsFree(pInst, iBlock)) {
      continue;       // Block is not used.
    }
    if (_BlockIsBad(pInst, iBlock)) {
      continue;       // Block is marked as defect.
    }
    SectorIndex = _BlockIndex2SectorIndex(pInst, iBlock);
    _ReadSpareIntoStaticBuffer(pInst, SectorIndex);
    DataStat       = _pSpareAreaData[SPARE_OFF_DATA_STATUS];
  //
    // Search only the data blocks.
  //
    if ((DataStat >> 4) == DATA_STAT_VALID) {
      DataEraseCnt = _LoadEraseCnt(pInst, _pSpareAreaData);
      if (EraseCnt == DataEraseCnt) {
        return iBlock;
      }
    }
  }
  return 0;     // No data block found with the requested erase count
}

/*********************************************************************
*
*       _CheckActiveWearLeveling
*
*   Function description
*     Checks if it is time to perform active wear leveling by
*     comparing the given erase count to the lowest erase count.
*     If so (difference is too big), the index of the data block with the lowest erase count is returned.
*
*   Parameters
*     pInst           [IN]  Driver instance
*                     [OUT] ---
*     EraseCnt              Erase count of block to be erased
*     pDataEraseCnt   [IN]  ---
*                     [OUT] Erase count of the data block
*
*   Return value
*     == 0  No data block found
*     != 0  Physical block index of the data block found
*/
static unsigned _CheckActiveWearLeveling(NAND_INST * pInst, U32 EraseCnt, U32 * pDataEraseCnt) {
  unsigned pbi;
  I32      EraseCntDiff;
  U32      NumBlocks;
  U32      EraseCntMin;

  //
  // Update pInst->EraseCntMin if necessary
  //
  pbi         = 0;
  NumBlocks   = pInst->NumBlocksEraseCntMin;
  EraseCntMin = pInst->EraseCntMin;
  if (NumBlocks == 0) {
    NumBlocks = _CountDataBlocksWithEraseCntMin(pInst, &EraseCntMin, &pbi);
    if (NumBlocks == 0) {
      return 0;     // We don't have any data block yet, it can happen if the flash is empty
    }
    pInst->EraseCntMin          = EraseCntMin;
    pInst->NumBlocksEraseCntMin = NumBlocks;
  }
  //
  // Check if the treshold for active wear leveling is reached
  //
  EraseCntDiff = EraseCnt -  EraseCntMin;
  if (EraseCntDiff < (I32)pInst->MaxEraseCntDiff) {
    return 0;       // Active wear leveling not necessary, EraseCntDiff is not big enough yes
  }
  if (pbi == 0) {
    pbi = _FindDataBlockByEraseCnt(pInst, EraseCntMin);
  }
  *pDataEraseCnt = EraseCntMin;
  --pInst->NumBlocksEraseCntMin;
  return pbi;
}

/*********************************************************************
*
*       _PerformPassiveWearLeveling
*
*   Function description
*     Searches for the next free block and returns its index. The block
*     is marked as allocated in the internal list.
*
*   Parameters
*     pInst       [IN]  Driver instance
*                 [OUT] ---
*     pEraseCnt   [IN]  ---
*                 [OUT] Erase count of the allocated block
*
*   Return value
*     == 0  No more free blocks
*     != 0  Physical block index of the allocated block
*/
static unsigned _PerformPassiveWearLeveling(NAND_INST * pInst, U32 * pEraseCnt) {
  unsigned i;
  unsigned iBlock;
  U8       aSpareData[6];   // The erase count is always within the first 6 bytes
  U32      EraseCnt;

  iBlock = pInst->MRUFreeBlock;
  for (i = 0; i < pInst->NumPhyBlocks; i++) {
    if (++iBlock >= pInst->NumPhyBlocks) {
      iBlock = 1;           // Block 0 contains only management information, so we skip it
    }
    if (_BlockIsFree(pInst, iBlock)) {
      _ReadSpare(pInst, _BlockIndex2SectorIndex(pInst, iBlock), aSpareData, 0, sizeof(aSpareData));
      EraseCnt = _LoadEraseCnt(pInst, aSpareData);
      if (EraseCnt == ERASE_CNT_INVALID) {
        EraseCnt = pInst->EraseCntMax;
      }
      *pEraseCnt = EraseCnt;
      _MarkBlockAsAllocated(pInst, iBlock);
      pInst->MRUFreeBlock = iBlock;
      return iBlock;
    }
  }
  FS_DEBUG_ERROROUT((FS_MTYPE_DRIVER,  "NAND: FATAL Error: No free block."));
  return 0;               // Error, no more free blocks
}

/*********************************************************************
*
*       _MoveDataBlock
*
*   Function description
*     Copies the contents of a data block into another block.
*     The souce data block is marked as free.
*
*   Parameters
*     pInst     [IN]  Driver instance
*               [OUT] ---
*     pbiSrc    Index of the block to be copied
*     pbiDest   Index of the block where to copy
*     EraseCnt  Erase count of the source block
*     pErrorBRSI  [IN]  ---
*                 [OUT] Index of the sector where the error occurred
*
*   Return value
*     NAND_NO_ERROR             OK
*     NAND_ERROR_IN_ECC         OK
*     NAND_UNCORRECTABLE_ERROR  Error, fatal
*     NAND_READ_ERROR           Error, fatal
*     NAND_WRITE_ERROR          Error, recoverable
*/
static int _MoveDataBlock(NAND_INST * pInst, unsigned pbiSrc, unsigned pbiDest, U32 EraseCnt, unsigned * pErrorBRSI) {
  unsigned SectorsPerBlock;
  unsigned iSector;
  U32      SectorIndexSrc;
  U32      SectorIndexDest;
  int      r;
  U8       ErrorInECC;
  unsigned lbi;
  unsigned pbi;
  U8       DataStat;

  DataStat        = 0;
  ErrorInECC      = 0;
  *pErrorBRSI     = BRSI_INVALID;
  SectorIndexSrc  = _BlockIndex2SectorIndex(pInst, pbiSrc);
  SectorIndexDest = _BlockIndex2SectorIndex(pInst, pbiDest);
  SectorsPerBlock = 1 << pInst->SPB_Shift;
  for (iSector = 0; iSector < SectorsPerBlock; ++iSector) {
    r = _CopySectorWithECC(pInst, SectorIndexSrc + iSector, SectorIndexDest + iSector, BRSI_INVALID);
    if ((r == NAND_NO_ERROR) || (r == NAND_1BIT_CORRECTED)) {
      continue;
    }
    if ((r == NAND_UNCORRECTABLE_ERROR) ||
        (r == NAND_READ_ERROR) ||
        (r == NAND_WRITE_ERROR)) {
      *pErrorBRSI = iSector;
      return r;
    }
    if (r == NAND_ERROR_IN_ECC) {
      *pErrorBRSI = iSector;
      ErrorInECC = 1;       // Remember that we had an error in ECC
    }
  }
  //
  // Get the logical block index of the copied block. We do this by searching the logical to physical block translation table.
  //
  for (lbi = 0; lbi < pInst->NumLogBlocks; ++lbi) {
    pbi = FS_BITFIELD_ReadEntry(pInst->pLog2PhyTable, lbi, pInst->NumBitsPhyBlockIndex);
    if (pbi == pbiSrc) {
      break;   
    }
  }
  _ReadSpareByte(pInst, SectorIndexSrc, &DataStat, SPARE_OFF_DATA_STATUS);    // Read the 4-bit data count
  DataStat++;
  _MarkAsDataBlock(pInst, pbiDest, lbi, EraseCnt, DataStat);
  //
  // Update the mapping of physical to logical blocks
  //
  _L2P_Write(pInst, lbi, pbiDest);

  //
  // Fail-safe TP. At this point we have two data blocks with the same LBI
  //

  if (ErrorInECC) {
    r = NAND_ERROR_IN_ECC;
  } else {
    _MakeBlockAvailable(pInst, pbiSrc, EraseCnt);
    r = NAND_NO_ERROR;
          }
  return r;
}

/*********************************************************************
*
*       _AllocErasedBlock
*
*   Function description
*     Selects a block to write data into. The returned block is erased.
*
*   Parameters
*     pInst       [IN]  Driver instance
*                 [OUT] ---
*     pEraseCnt   [IN]  ---
*                 [OUT] Erase count of the selected block
*
*   Return value
*     ==0   An error occurred
*     !=0   Physical block index
*/
static unsigned _AllocErasedBlock(NAND_INST * pInst, U32 * pEraseCnt) {
  unsigned pbiAlloc;
  unsigned pbiData;
  U32      AllocEraseCnt;
  U32      DataEraseCnt;
  int      r;
  unsigned ErrorBRSI;

  AllocEraseCnt = pInst->EraseCntMax;   // Just to avoid compiler warning about variable used before being initialized.
  while (1) {
    //
    // Passive wear leveling. Get the next free block in the row
    //
    pbiAlloc = _PerformPassiveWearLeveling(pInst, &AllocEraseCnt);
    if (pbiAlloc == 0) {
      _OnFatalError(pInst, NAND_OUT_OF_FREE_BLOCKS, 0);
      return 0;       // Fatal error, out of free blocks
        }
    r = _EraseBlock(pInst, pbiAlloc);
    if (r) {
      _MarkBlockAsBad(pInst, pbiAlloc, NAND_ERASE_ERROR, 0);
      continue;       // Error when erasing the block, get a new one
      }
      //
    // OK, we found a free block.
    // Now, let's check if the erase count is too high so we need to use active wear leveling
      //
    pbiData = _CheckActiveWearLeveling(pInst, AllocEraseCnt, &DataEraseCnt);
    ++AllocEraseCnt;
    if (pbiData == 0) {
      *pEraseCnt = AllocEraseCnt; // No data block has an erase count low enough, keep the block allocated by passive wear leveling
      return pbiAlloc;
    }
    //
    // Perform active wear leveling:
    // A block containing data has a much lower erase count. This block is now moved, giving us a free block with low erase count.
    // This procedure makes sure that blocks which contain data that does not change still take part in the the
    // wear leveling scheme.
    //
    r = _MoveDataBlock(pInst, pbiData, pbiAlloc, AllocEraseCnt, &ErrorBRSI);
    if (r == NAND_NO_ERROR) {
    //
      // The data has been moved and the data block is now free to use
    //
      _MarkBlockAsAllocated(pInst, pbiData);
      r = _EraseBlock(pInst, pbiData);
      if (r) {
        _MarkBlockAsBad(pInst, pbiData, NAND_ERASE_ERROR, 0);
        continue;           // Error when erasing the block, get a new one
      }
      ++DataEraseCnt;
      *pEraseCnt = DataEraseCnt;
      return pbiData;
    }
    if ((r == NAND_UNCORRECTABLE_ERROR) || (r == NAND_READ_ERROR)) {
      _MarkBlockAsBad(pInst, pbiData, r, ErrorBRSI);
      return 0;                   // Fatal error, no way to recover
    }
    if (r == NAND_WRITE_ERROR) {
      _MarkBlockAsBad(pInst, pbiAlloc, r, ErrorBRSI);
      continue;                   // Error when writting into the allocated block
    }
    if (r == NAND_ERROR_IN_ECC) {
      _MarkBlockAsBad(pInst, pbiData, r, ErrorBRSI);
      continue;                   // Error in the ECC of the data block, get a new free block and a new data block.
    }
  }
}

/*********************************************************************
*
*       _RecoverDataBlock
*
*   Function Description
*     Copies a data block into a free block. Called typ. when an error
*     is found in the ECC.
*
*   Parameters
*     pInst       [IN]  Driver instance
*                 [OUT] ---
*     pbiData     Index of the block to recover
*
*   Return value:
*     ==0   Data block saved
*     !=0   An error occurred
*
*/
static int _RecoverDataBlock(NAND_INST * pInst, unsigned pbiData) {
  unsigned pbiAlloc;
  U32      EraseCnt;
  int      r;
  unsigned ErrorBRSI;

  while (1) {
    //
    // Need a free block where to move the data of the damaged block
    //
    pbiAlloc = _AllocErasedBlock(pInst, &EraseCnt);
    if (pbiAlloc == 0) {
      return 1;               // Could not allocate an empty block, fatal error
    }
    r = _MoveDataBlock(pInst, pbiData, pbiAlloc, EraseCnt, &ErrorBRSI);
    if (r == NAND_ERROR_IN_ECC) {
      _MarkBlockAsBad(pInst, pbiData, r, ErrorBRSI);
      return 0;               // Data was recovered
    }
    if ((r == NAND_UNCORRECTABLE_ERROR) || (r == NAND_READ_ERROR)) {
      _MarkBlockAsBad(pInst, pbiData, r, ErrorBRSI);
      return 1;               // Fatal error, no way to recover
    }
    if (r == NAND_WRITE_ERROR) {
      _MarkBlockAsBad(pInst, pbiAlloc, r, ErrorBRSI);
      continue;               // Error when writting into the allocated block
    }
    if (r == NAND_NO_ERROR) {
      return 0;               // Data moved into the new block
    }
  }
}

/*********************************************************************
*
*       _ConvertWorkBlockViaCopy
*
*   Function description
*     Converts a work block into a data block. The data of the work block
*     is "merged" with the data of the source block in another free block.
*     The merging operation copies sector-wise the data from work block
*     into the free block. If the sector data is invalid in the work block
*     the sector data from the source block is copied instead. The sectors
*     in the work block doesn't have to be on their native positions.
*
*   Parameters
*     pInst       [IN]  Driver instance
*                 [OUT] ---
*     pWorkBlock  [IN]  Work block to convert
*                 [OUT] ---
*     SkipBRSI    BRSI of the sector to ignore when copying
*
*   Return value
*     ==0   OK
*     !=0   An error occurred
*/
static int _ConvertWorkBlockViaCopy(NAND_INST * pInst, WORK_BLOCK_DESC * pWorkBlock, unsigned SkipBRSI) {
  unsigned iSector;
  U16      pbiSrc;
  U16      pbiWork;
  U32      SectorIndexSrc;
  U32      SectorIndexWork;
  unsigned SectorsPerBlock;
  U32      pbiDest;
  U32      SectorIndexDest;
  U32      EraseCntDest;
  U32      EraseCntSrc;
  int      r;
  unsigned brsiSrc;
  int      ErrorInEccSrc;
  int      ErrorInEccWork;
  unsigned ErrorBRSI;
  U8       DataStat;
  U8       aSpareData[6];

  DataStat        = 0;
  EraseCntSrc     = ERASE_CNT_INVALID;
  EraseCntDest    = ERASE_CNT_INVALID;
  pbiWork         = pWorkBlock->pbi;
  SectorIndexWork = _BlockIndex2SectorIndex(pInst, pbiWork);
  SectorsPerBlock = 1 << pInst->SPB_Shift;
  pbiDest         = 0;  // No destination block, yet

Retry:
  ErrorInEccSrc  = 0;
  ErrorInEccWork = 0;
  ErrorBRSI      = 0;
  //
  // We need to allocate a new block to copy data into
  //
  pbiDest = _AllocErasedBlock(pInst, &EraseCntDest);
  if (pbiDest == 0) {
    return 1;             // Error, no more free blocks, not recoverable
    }
  //
  // OK, we have an empty block to copy our data into
  //
  pbiSrc          = _L2P_Read(pInst, pWorkBlock->lbi);
  SectorIndexSrc  = _BlockIndex2SectorIndex(pInst, pbiSrc);
  SectorIndexDest = _BlockIndex2SectorIndex(pInst, pbiDest);
  //
  // Copy the data sector by sector
  //
    for (iSector = 0; iSector < SectorsPerBlock; iSector++) {
      //
      // If data is not in work block, take it from source block
      //
    brsiSrc = _brsiLog2Phy(pInst, pWorkBlock, iSector);
    if ((brsiSrc != BRSI_INVALID) && (brsiSrc != SkipBRSI)) { // In work block ?
      r = _CopySectorWithECC(pInst, SectorIndexWork + brsiSrc, SectorIndexDest + iSector, BRSI_INVALID);
      if ((r == NAND_NO_ERROR) || (r == NAND_1BIT_CORRECTED)) {
        continue;
      }
      if ((r == NAND_UNCORRECTABLE_ERROR) || (r == NAND_READ_ERROR)) {
        _MarkBlockAsBad(pInst, pbiWork, r, iSector);
        return 1;               // No way to recover out of this
      }
      if (r == NAND_ERROR_IN_ECC) {
        ErrorBRSI      = iSector;
        ErrorInEccWork = 1;     // Remember we had an error in the ECC of the work block. We will mark it as "bad" later.
        continue;
        }
      if (r == NAND_WRITE_ERROR) {
        _MarkBlockAsBad(pInst, pbiDest, r, iSector);
        goto Retry;             // Write error occurred, try to find another empty block
      }
    } else if (SectorIndexSrc) {                              // In source block ?
      //
      // Copy if we have a data source.
      // Note that when closing a work block which did not yet have a source data block,
      // it can happen that some sector have no source and stay empty.
      //
      r = _CopySectorWithECC(pInst, SectorIndexSrc + iSector, SectorIndexDest + iSector, BRSI_INVALID);
      if ((r == NAND_NO_ERROR) || (r == NAND_1BIT_CORRECTED)) {
        continue;
      }
      if ((r == NAND_UNCORRECTABLE_ERROR) || (r == NAND_READ_ERROR)) {
        _MarkBlockAsBad(pInst, pbiSrc, r, iSector);
        return 1;             // No way to recover out of this
      }
      if (r == NAND_ERROR_IN_ECC) {
        ErrorBRSI     = iSector;
        ErrorInEccSrc = 1;    // Remember we had an error in the ECC of the data block. We will mark it as "bad" later.
        continue;
      }
      if (r == NAND_WRITE_ERROR) {
        _MarkBlockAsBad(pInst, pbiDest, r, iSector);
        goto Retry;           // Write error occurred, try to find another empty block
        }
      }
    }
  if (SectorIndexSrc) {
    _ReadSpare(pInst, SectorIndexSrc, aSpareData, 0, sizeof(aSpareData));   // Read the 4-bit data count and the erase count
    DataStat    = aSpareData[SPARE_OFF_DATA_STATUS];
    EraseCntSrc = _LoadEraseCnt(pInst, aSpareData);
    DataStat++;
  }
  //
  // Mark the newly allocated block as data block
  //
  _MarkAsDataBlock(pInst, pbiDest, pWorkBlock->lbi, EraseCntDest, DataStat);

  // 
  // Fail-safe TP. At this point we have two data blocks with the same LBI
  //

    //
  // Update the mapping of physical to logical blocks
    //
  _L2P_Write(pInst, pWorkBlock->lbi, pbiDest);
    //
  // Mark former work block as invalid and put it to the free list if there was no error in the ECC.
    //
  if (ErrorInEccWork) {
    _MarkBlockAsBad(pInst, pbiWork, NAND_ERROR_IN_ECC, ErrorBRSI);
  } else {
    _MakeBlockAvailable(pInst, pbiWork, ERASE_CNT_INVALID);
  }
  //
  // Change data status of block which contained the "old" data
  // as invalid and put it to the free list if there was no error in ECC
  //
  if (ErrorInEccSrc) {
    _MarkBlockAsBad(pInst, pbiSrc, NAND_ERROR_IN_ECC, ErrorBRSI);
  } else {
    _MakeBlockAvailable(pInst, pbiSrc, EraseCntSrc);
  }
  //
  // Remove the work block from the internal list
  //
  _WB_RemoveFromUsedList(pInst, pWorkBlock);
  _WB_AddToFreeList(pInst, pWorkBlock);
  //
  // If required, update the information used for active wear leveling
  //
  {
    U32 EraseCntMin;
    U32 NumBlocksEraseCntMin;

    EraseCntMin          = pInst->EraseCntMin;
    NumBlocksEraseCntMin = pInst->NumBlocksEraseCntMin;
    if (EraseCntDest < EraseCntMin) {
      EraseCntMin          = EraseCntDest;
      NumBlocksEraseCntMin = 1;
    } else if (EraseCntDest == EraseCntMin) {
      ++NumBlocksEraseCntMin;
    }
    pInst->EraseCntMin          = EraseCntMin;
    pInst->NumBlocksEraseCntMin = NumBlocksEraseCntMin;
  }
  IF_STATS(pInst->StatCounters.ConvertViaCopyCnt++);
  return 0;
}

/*********************************************************************
*
*       _ConvertWorkBlockInPlace
*
*   Function description
*     Converts a work block into a data block. It assumes that
*     the sectors are on their native positions. The missing sectors
*     are copied from the source block into the work block.
*
*   Parameters
*     pInst       [IN]  Driver instance
*                 [OUT] ---
*     pWorkBlock  [IN]  Work block to convert
*                 [OUT] ---
*     pErrorBRSI  [IN]  ---
*                 [OUT] Index of the sector where the error occurred. Sectors per block is returned in case of a fatal error
*
*   Return value
*     ==0   OK
*     !=0   An error occurred
*/
static int _ConvertWorkBlockInPlace(NAND_INST * pInst, WORK_BLOCK_DESC * pWorkBlock, unsigned * pErrorBRSI) {
  unsigned iSector;
  U16      pbiSrc;
  U32      SectorIndexSrc;
  U32      SectorIndexWork;
  U8       aSpareData[6];   // The erase count is always within the first 6 bytes
  U8       DataStat;
  unsigned SectorsPerBlock;
  int      r;
  unsigned brsi;
  U32      EraseCnt;

  *pErrorBRSI     = BRSI_INVALID;
  brsi            = BRSI_INVALID;
  DataStat        = 0;
  EraseCnt        = ERASE_CNT_INVALID;
  pbiSrc          = _L2P_Read(pInst, pWorkBlock->lbi);
  SectorIndexSrc  = _BlockIndex2SectorIndex(pInst, pbiSrc);
  SectorIndexWork = _BlockIndex2SectorIndex(pInst, pWorkBlock->pbi);
  SectorsPerBlock = 1 << pInst->SPB_Shift;
  //
  // If there is a source block, then use it to "fill the gaps", reading sectors which are empty in the work block
  //
  if (SectorIndexSrc) {
    for (iSector = 0; iSector < SectorsPerBlock; iSector++) {
      if (_WB_SectorIsWritten(pWorkBlock, iSector) == 0) {
        if (iSector) {
          brsi = iSector;
        }
        r = _CopySectorWithECC(pInst, SectorIndexSrc + iSector, SectorIndexWork + iSector, brsi);
        if ((r == NAND_NO_ERROR) || (r == NAND_1BIT_CORRECTED)) {
          continue;
        }
        if ((r == NAND_UNCORRECTABLE_ERROR) || (r == NAND_READ_ERROR)) {
          *pErrorBRSI = SectorsPerBlock;  // Tell the caller a fatal error occurred  
          _MarkBlockAsBad(pInst, pbiSrc, r, iSector);     
          return 1;                     // No way to recover 
        }
        if ((r == NAND_ERROR_IN_ECC) || (r == NAND_WRITE_ERROR)) {
          *pErrorBRSI = iSector;
          return 1;                 // Stop the conversion here and let the caller recover from error
        }
      }
    }
    //
    // Convert work block into valid data block by changing the data status
    //
    _ReadSpare(pInst, SectorIndexSrc, aSpareData, 0, sizeof(aSpareData));       // Read the 4-bit data count and the erase count
    DataStat = aSpareData[SPARE_OFF_DATA_STATUS];
    EraseCnt = _LoadEraseCnt(pInst, aSpareData);
    DataStat++;
  }
  DataStat = (U8) ((DataStat & 0xF) | (DATA_STAT_VALID << 4));   // Mark data as "valid"
  _WriteSpareByte(pInst, SectorIndexWork, DataStat, SPARE_OFF_DATA_STATUS);

  // 
  // Fail-safe TP. At this point we have two data blocks with the same LBI
  //

  //
  // Converted work block is now data block, update Log2Phy Table
  //
  _L2P_Write(pInst, pWorkBlock->lbi, pWorkBlock->pbi);
  //
  // Change data status of block which contained the "old" data
  // as invalid and put it to the free list
  //
  _MakeBlockAvailable(pInst, pbiSrc, EraseCnt);
  //
  // Remove the work block from the internal list
  //
  _WB_RemoveFromUsedList(pInst, pWorkBlock);
  _WB_AddToFreeList(pInst, pWorkBlock);
  //
  // If required, update the information used for active wear leveling
  //
  {
    U32 EraseCntMin;
    U32 NumBlocksEraseCntMin;
    U32 EraseCntWork;

    EraseCntMin          = pInst->EraseCntMin;
    NumBlocksEraseCntMin = pInst->NumBlocksEraseCntMin;
    _ReadSpare(pInst, SectorIndexWork, aSpareData, 0, sizeof(aSpareData));  // Read the EraseCnt
    EraseCntWork         = _LoadEraseCnt(pInst, aSpareData);
    if (EraseCntWork < EraseCntMin) {
      EraseCntMin          = EraseCntWork;
      NumBlocksEraseCntMin = 1;
    } else if (EraseCntWork == EraseCntMin) {
      ++NumBlocksEraseCntMin;
    }
    pInst->EraseCntMin          = EraseCntMin;
    pInst->NumBlocksEraseCntMin = NumBlocksEraseCntMin;
  }
  IF_STATS(pInst->StatCounters.ConvertInPlaceCnt++);
  return NAND_NO_ERROR;
}

/*********************************************************************
*
*       _WorkBlockCanBeConverted
*
*  Function description
*    Checks if a work block can be converted.
*    This is the case if all written sectors are in the right place.
*
*   Return value
*     ==0       Found sectors with valid data not on their native positions.
*     ==1       All the sectors having valid data found on their native positions.
*/
static int _WorkBlockCanBeConverted(NAND_INST * pInst, WORK_BLOCK_DESC * pWorkBlock) {
  unsigned u;
  unsigned Pos;
  unsigned SectorsPerBlock;

  SectorsPerBlock = 1 << pInst->SPB_Shift;
  for (u = 0; u < SectorsPerBlock; u++) {
    if (_WB_SectorIsWritten(pWorkBlock, u)) {
      Pos = _WB_ReadAssignment(pInst, pWorkBlock, u);
      if (Pos != u) {
        return 0;
      }
    }
  }
  return 1;
}

/*********************************************************************
*
*       _CleanWorkBlock
*
*   Function description
*     Closes the work buffer.
*     - Convert work block into normal data buffer by copy all data into it and marking it as data block
*     - Invalidate and mark as free the block which contained the same logical data area before
*
*   Parameters
*     pInst       [IN]  Driver instance
*                 [OUT] ---
*     pWorkBlock  [IN]  Work block to be cleaned
*                 [OUT] ---
*
*   Return values
*     ==0   OK
*     !=0   An error occurred
*/
static int _CleanWorkBlock(NAND_INST * pInst, WORK_BLOCK_DESC *pWorkBlock) {
  int      r;
  unsigned ErrBRSI;
  unsigned SectorsPerBlock;

  ErrBRSI = BRSI_INVALID;
  r = _WorkBlockCanBeConverted(pInst, pWorkBlock);
  if (r < 0) {              // No valid sectors in the work block ?
    _MakeBlockAvailable(pInst, pWorkBlock->pbi, ERASE_CNT_INVALID);
    //
    // Remove the work block from the internal list
    //
    _WB_RemoveFromUsedList(pInst, pWorkBlock);
    _WB_AddToFreeList(pInst, pWorkBlock);
    return 0;
  }
  if (r) {                  // Can work block be converted in-place ?
    r = _ConvertWorkBlockInPlace(pInst, pWorkBlock, &ErrBRSI);
    if (r == 0) {
      return 0;             // Block converted, we are done
    }
    SectorsPerBlock = 1 << pInst->SPB_Shift;
    if (ErrBRSI == SectorsPerBlock) {
      return 1;             // Fatal error, no recovery is possible
    }
  }
  //
  // Work block could not be converted in place, try via copy
  //
  return _ConvertWorkBlockViaCopy(pInst, pWorkBlock, ErrBRSI);
}

/*********************************************************************
*
*       _CleanLastWorkBlock
*
*  Function Description
*    Removes the least recently used work block from list of work blocks and converts it into data block
*/
static int _CleanLastWorkBlock(NAND_INST * pInst) {
  WORK_BLOCK_DESC * pWorkBlock;
  //
  // Find last work block in list
  //
  pWorkBlock = pInst->pFirstWorkBlockInUse;
  while (pWorkBlock->pNext) {
    pWorkBlock = pWorkBlock->pNext;
  }
  return _CleanWorkBlock(pInst, pWorkBlock);
}

/*********************************************************************
*
*       _CleanAllWorkBlocks
*
*   Function description
*     Closes all work blocks.
*/
#if FS_NAND_ENABLE_CLEAN
static void _CleanAllWorkBlocks(NAND_INST * pInst) {
  while (pInst->pFirstWorkBlockInUse) {
    _CleanWorkBlock(pInst, pInst->pFirstWorkBlockInUse);
  }
}
#endif

/*********************************************************************
*
*       _AllocWorkBlock
*
*  Function Description
*    - Allocates a WORK_BLOCK_DESC management entry from the array in the pInst structure
*    - Finds a free block and assigns it to the WORK_BLOCK_DESC
*    - Writes info such as EraseCnt, lbui and the WORK-BLOCK marker to the spare area of the first sector
*
*   Parameters
*     pInst       [IN]  Driver instance
*                 [OUT] ---
*     lbi         Logical block index assigned to work block
*
*   Return values
*     !=NULL  Pointer to allocated work block
*     ==NULL  An error occurred, typ. a fatal error
*/
static WORK_BLOCK_DESC * _AllocWorkBlock(NAND_INST * pInst, unsigned lbi) {
  WORK_BLOCK_DESC * pWorkBlock;
  U32 EraseCnt;
  unsigned          pbi;
  int               r;

  pWorkBlock = _AllocWorkBlockDesc(pInst, lbi);
  if (pWorkBlock == NULL) {
    r = _CleanLastWorkBlock(pInst);
    if (r) {
      return NULL;                  // Error: Happens only in case of a fatal error
    }
    pWorkBlock = _AllocWorkBlockDesc(pInst, lbi);
    if (pWorkBlock == NULL) {
      return NULL;                  // Error. This can happen in case of hardware failure only
    }
  }
  //
  // Get an empty block to write on
  //
  pbi = _AllocErasedBlock(pInst, &EraseCnt);
  if (pbi == 0) {
    return NULL;                    // Error: Happens only in case of a fatal error
  }
  //
  // New workblock allocated
  //
  pWorkBlock->pbi = pbi;
  _MarkAsWorkBlock(pInst, pWorkBlock->pbi, lbi, EraseCnt);
  return pWorkBlock;
}

/*********************************************************************
*
*       _FindWorkBlock
*
*  Function description
*    Tries to locate a work block for a given logical block.
*/
static WORK_BLOCK_DESC * _FindWorkBlock(NAND_INST * pInst, unsigned lbi) {
  WORK_BLOCK_DESC * pWorkBlock;

  //
  // Iterate over used-list
  //
  pWorkBlock = pInst->pFirstWorkBlockInUse;
  do {
    if (pWorkBlock == NULL) {
      break;                         // No match
    }
    if (pWorkBlock->lbi == lbi) {
      break;                         // Found it
    }
    pWorkBlock = pWorkBlock->pNext;
  } while (1);
  return pWorkBlock;
}


/*********************************************************************
*
*       _MarkWorkBlockAsMRU
*
*  Function description
*    Marks the given work block as most-recently used.
*    This is important so the least recently used one can be "kicked out" if a new one is needed.
*/
static void _MarkWorkBlockAsMRU(NAND_INST * pInst, WORK_BLOCK_DESC * pWorkBlock) {
  if (pWorkBlock != pInst->pFirstWorkBlockInUse) {
    _WB_RemoveFromUsedList(pInst, pWorkBlock);
    _WB_AddToUsedList(pInst, pWorkBlock);
  }
}


/*********************************************************************
*
*       _LoadWorkBlock
*
*  Function description
*    Reads management data of work block.
*    Used during low-level mount only, since at all other times, the work block descriptors are up to date.
*
*  Notes
*    (1) Finding out if page data has been written to work block
*        There are 2 criteria to find out if the sector in a work block has been written:
*        a) LBI entry correct
*        b) ECC written. A valid ECC has bits 16/17 == 0.
*        For the first page, criterion a) can not be used since this info is written even without data.
*/
static void _LoadWorkBlock(NAND_INST * pInst, WORK_BLOCK_DESC * pWorkBlock) {
  unsigned NumSectors;
  unsigned iSector;
  U32      ecc;
  U32      SectorIndex0;
  unsigned brsi;
  U32      pbi;

  pbi          = pWorkBlock->pbi;
  NumSectors   = 1 << pInst->SPB_Shift;
  SectorIndex0 = _BlockIndex2SectorIndex(pInst, pbi);
  //
  // Iterate over all sectors, reading spare info in order to find out if sector contains data and if so which data
  //
  for (iSector = 0; iSector < NumSectors; iSector++) {
    U32 SectorIndexSrc;

    SectorIndexSrc = SectorIndex0 + iSector;
    //
    // Check if this sector of the work block contains valid data.
    //
    _ReadSpareIntoStaticBuffer(pInst, SectorIndexSrc);
    //
    // For first page, we need to check if data has been written. Note (1).
    //
    ecc = FS__ECC256_Load(_pSpareAreaData + SPARE_OFF_ECC00);
    if (FS__ECC256_IsValid(ecc)) {
      if (iSector == 0) {
        brsi = 0;
      } else {
        brsi = _LoadBRSI(pInst);
      }
      _WB_MarkSectorAsUsed(pWorkBlock, iSector);
      if (brsi != BRSI_INVALID) {
        _WB_WriteAssignment(pInst, pWorkBlock, brsi, iSector);
      }
    }
  }
}

/*********************************************************************
*
*       _BlockDataIsMoreRecent
*
*  Function description
*    Used during low-level mount only.
*/
static char _BlockDataIsMoreRecent(NAND_INST * pInst, U32 BlockIndex) {
  int Data;
  U8  Data8;
  U32 SectorIndex;

  SectorIndex = _BlockIndex2SectorIndex(pInst, BlockIndex);
  _ReadSpareByte(pInst, SectorIndex, &Data8, SPARE_OFF_DATA_STATUS);
  Data = (Data8 - *(_pSpareAreaData + SPARE_OFF_DATA_STATUS)) & 0xF;
  if (Data == 1) {
    return 1;        // Newer!
  }
  return 0;          // Older!
}

/*********************************************************************
*
*       _GetNumValidSectors
*
*   Function description
*     Counts how many sectors in a block contain valid data.
*
*   Parameters
*     pInst   [IN]  Driver instance
*             [OUT] ---
*     lbi     Logical index of the block to process.
*
*   Return value
*     Number of valid sectors.
*
*   Notes
*       (1) A sector with valid data has also a valid brsi assigned to it
*/
#if FS_NAND_ENABLE_STATS
static U32 _GetNumValidSectors(NAND_INST * pInst, unsigned lbi) {
  unsigned SectorsPerBlock;
  unsigned iSector;
  U32      SectorIndexSrc;
  U32      NumSectors;
  unsigned pbiSrc;
  WORK_BLOCK_DESC * pWorkBlock;

  pbiSrc          = _L2P_Read(pInst, lbi);
  pWorkBlock      = _FindWorkBlock(pInst, lbi);
  SectorsPerBlock = 1 << pInst->SPB_Shift;
  NumSectors      = 0;
  //
  // 1st case: a data block is assigned to logical block
  //
  if (pbiSrc && (pWorkBlock == NULL)) {
    SectorIndexSrc  = _BlockIndex2SectorIndex(pInst, pbiSrc);
    do {
      if (_SectorDataIsInvalidated(pInst, SectorIndexSrc) == 0) {
        if (_SectorDataIsWritten(pInst, SectorIndexSrc)) {
          ++NumSectors;
        }
      }
      ++SectorIndexSrc;
    } while (--SectorsPerBlock);
  }
  //
  // 2nd case: a work block is assigned to logical block
  //
  if ((pbiSrc == 0) && pWorkBlock) {
    for (iSector = 0; iSector < SectorsPerBlock; ++iSector) {
      if (_brsiLog2Phy(pInst, pWorkBlock, iSector) != BRSI_INVALID) {     // Note 1
        ++NumSectors;
      }
    }
  }
  //
  // 3rd case: a data block and a work block are assigned to logical block
  //
  if (pbiSrc && pWorkBlock) {
    SectorIndexSrc  = _BlockIndex2SectorIndex(pInst, pbiSrc);
    for (iSector = 0; iSector < SectorsPerBlock; ++iSector) {
      if (_SectorDataIsInvalidated(pInst, SectorIndexSrc) == 0) {
        //
        // The sector was not invalidated by a "free sectors" command.
        // Check if it contains valid data. Else check if there is
        // valid data in the work block for this sector.
        //
        if (_SectorDataIsWritten(pInst, SectorIndexSrc)) {
          ++NumSectors;
        } else if (_brsiLog2Phy(pInst, pWorkBlock, iSector) != BRSI_INVALID) {   // Note 1
          ++NumSectors;
        }
      }
      ++SectorIndexSrc;
    }
  }
  return NumSectors;
}
#endif

/*********************************************************************
*
*       _LowLevelMount
*
*  Function description
*    Reads and analyzes management information from NAND flash.
*    If the information makes sense and allows us to read and write from
*    the medium, it can perform read and write operations.
*
*  Return value
*    0     O.K., device has been successfully mounted and is accessible
*  !=0     Error
*
*/
static int _LowLevelMount(NAND_INST * pInst) {
  U16        iBlock;
  U16        lbi;
  U16        PBIPrev;
  U32        EraseCntMax;               // Highest erase count on any sector
  U32        EraseCnt;
  U32        EraseCntMin;
  U32        NumBlocksEraseCntMin;
  const U8 * pPageBuffer;
  unsigned   u;
  U32        BadBlockOff;
  int        r;
  U32        Version;
  U32        SectorSize;
  U32        NumBlocksToFileSystem;
  int        NumBlocksToUse;
  WORK_BLOCK_DESC * pWorkBlock;
  unsigned   NumWorkBlocks;
  unsigned   NumWorkBlocksLLFormat;
  unsigned   NumWorkBlocksToAllocate;
  U32        NumPhyBlocks;

  //
  // Check info block first (First block in the system)
  //
  r = _ReadSectorWithECC(pInst, _pSectorBuffer, FORMAT_INFO_SECTOR_INDEX);
  if ((r != NAND_NO_ERROR) && (r != NAND_1BIT_CORRECTED)) {
    return 1;                   // Error
  }
  pPageBuffer = (const U8 *)_pSectorBuffer;
  if (FS_MEMCMP(_acInfo, pPageBuffer , sizeof(_acInfo))) {
    FS_DEBUG_ERROROUT((FS_MTYPE_DRIVER, "NAND: Invalid low-level signature."));
    return 1;                   // Error
  }
  Version = FS_LoadU32BE(pPageBuffer + INFO_OFF_LLFORMAT_VERSION);
  if (Version != (U32)LLFORMAT_VERSION) {
    FS_DEBUG_ERROROUT((FS_MTYPE_DRIVER, "NAND: Invalid low-level format version."));
    return 1;                   // Error
  }
  SectorSize = FS_LoadU32BE(pPageBuffer + INFO_OFF_SECTOR_SIZE);
  if (SectorSize > (U32)FS_Global.MaxSectorSize) {
    FS_DEBUG_ERROROUT((FS_MTYPE_DRIVER, "NAND: Sector size specified in drive is higher than the sector size that can be stored by the FS."));
    return 1;                   // Error
  }
  //
  // Load the number of work blocks from device.
  //
  NumWorkBlocks         = pInst->NumWorkBlocks;
  NumWorkBlocksLLFormat = FS_LoadU32BE(pPageBuffer + INFO_OFF_NUM_WORK_BLOCKS);
  if (NumWorkBlocksLLFormat == 0xFFFFFFFFuL) {  // "Old" driver versions do not set this field but use FS_NAND_MAX_WORK_BLOCKS.
    NumWorkBlocksLLFormat = NUM_WORK_BLOCKS_OLD;
  }
  //
  // Find out how many work blocks are required to be allocated.
  // We take the maximum between the number of work blocks read from device
  // and the number of work blocks configured. The reason is to prevent
  // an overflow in the paWorkBlock array when the application increases
  // the number of work blocks and does a low-level format.
  // 
  NumWorkBlocksToAllocate = MAX(NumWorkBlocksLLFormat, NumWorkBlocks);
  NumWorkBlocks           = NumWorkBlocksLLFormat;
  //
  // Compute the number of logical blocks available for the file system.
  // We have to take into account that this version of the driver
  // reserves one block more for internal use. To stay compatible we have
  // to use 2 algorithms: one for the "old" version and one for the "new" one.
  // We tell the 2 versions appart by checking the INFO_OFF_NUM_LOG_BLOCKS.
  // The "old" version does not set this entry and its value will always be 0xFFFFFFFF.
  //
  NumPhyBlocks          = pInst->NumPhyBlocks;
  NumBlocksToFileSystem = FS_LoadU32BE(pPageBuffer + INFO_OFF_NUM_LOG_BLOCKS);
  NumBlocksToUse        = _CalcNumBlocksToUse(NumPhyBlocks, NumWorkBlocks);
  if (NumBlocksToFileSystem == 0xFFFFFFFFuL) {            // Old Low-Level Format ?
    NumBlocksToUse        = _CalcNumBlocksToUseOldFormat(NumPhyBlocks, NumWorkBlocks);
    NumBlocksToFileSystem = NumBlocksToUse;
  }
  if ((NumBlocksToUse <= 0) || (NumBlocksToFileSystem > (U32)NumBlocksToUse)) {
    FS_DEBUG_ERROROUT((FS_MTYPE_DRIVER,  "NAND: Number of logical blocks has shrunk. Low-level format required.\n"));
    return 1;
  }
  pInst->NumLogBlocks = NumBlocksToUse;
  pInst->NumSectors   = (U32)pInst->NumLogBlocks << pInst->SPB_Shift;
  pInst->NumWorkBlocks = NumWorkBlocks;
  //
  // 3 different values for the BadBlockOff are permitted:
  // 0:          Used by large page flashes (2KB)
  // 5:          Used by small page flashes (512 bytes)
  // 0xFFFFFFFF: Formatted by older version of the driver, => We need to use 0 to stay compatible.
  //
  BadBlockOff = FS_LoadU32BE(pPageBuffer + INFO_OFF_BAD_BLOCK_OFFSET);
  switch (BadBlockOff) {
  case 0:                   // Large page flash ?
    break;
  case 5:                   // Small page flash ?
    break;
  case 0xFFFFFFFF:          // Unknown type, but LL-formatted with bad block info at offset 0
    BadBlockOff = 0;
    break;
  default:                  // Illegal value!
    FS_DEBUG_ERROROUT((FS_MTYPE_DRIVER, "NAND: Invalid bad block offset %d.\n", BadBlockOff));
    return 1;
  }

  pInst->BadBlockOffset = (U8)BadBlockOff;
  //
  // Load the information stored when a fatal error occurs
  //
  pInst->IsWriteProtected = 0;
  pInst->HasFatalError    = 0;
  pInst->ErrorType        = NAND_NO_ERROR;
  pInst->ErrorSectorIndex = 0;
  r = _ReadSectorWithECC(pInst, _pSectorBuffer, ERROR_INFO_SECTOR_INDEX);
  if ((r == NAND_NO_ERROR) || (r == NAND_1BIT_CORRECTED)) {
    pPageBuffer = (const U8 *)_pSectorBuffer;
    pInst->IsWriteProtected = FS_LoadU16BE(pPageBuffer + INFO_OFF_IS_WRITE_PROTECTED) != 0xFFFF ? 1 : 0;  // Inverted, 0xFFFF is not write protected
    pInst->HasFatalError    = FS_LoadU16BE(pPageBuffer + INFO_OFF_HAS_FATAL_ERROR) != 0xFFFF ? 1 : 0;     // Inverted, 0xFFFF doesn't have fatal error
    if (pInst->HasFatalError) {
      pInst->ErrorType        = (U8)FS_LoadU16BE(pPageBuffer + INFO_OFF_FATAL_ERROR_TYPE);
      pInst->ErrorSectorIndex = FS_LoadU32BE(pPageBuffer + INFO_OFF_FATAL_ERROR_SECTOR_INDEX);
    }
  }
  //
  // Assign reasonable default for config values
  //
  if (pInst->MaxEraseCntDiff == 0) {
    pInst->MaxEraseCntDiff = FS_NAND_MAX_ERASE_CNT_DIFF;
  }
  //
  // Allocate/Zero memory for tables
  //
  FS_AllocZeroedPtr((void **)&pInst->pLog2PhyTable, _L2P_GetSize(pInst));
  FS_AllocZeroedPtr((void **)&pInst->pFreeMap,      (pInst->NumPhyBlocks + 7) / 8);
  //
  //  Init work block descriptors: Alloc memory & add them to free list
  //
  {
    unsigned spb;     // Sectors per block
    unsigned NumBytes;

    NumBytes = sizeof(WORK_BLOCK_DESC) * NumWorkBlocksToAllocate;
    //
    // This is equivalent to FS_AllocZeroedPtr() but it avoids filling the array with 0
    // when the memory block is already allocated.
    //
    if (pInst->paWorkBlock == NULL) {
      pInst->paWorkBlock = (WORK_BLOCK_DESC *)FS_AllocZeroed(NumBytes);
      FS_MEMSET(pInst->paWorkBlock, 0, NumBytes);
    }
    NumBytes = _WB_GetAssignmentSize(pInst);
    spb = 1 << pInst->SPB_Shift;
    pWorkBlock = pInst->paWorkBlock;
    u          = NumWorkBlocksToAllocate;
    do {
      FS_AllocZeroedPtr((void **)&pWorkBlock->paIsWritten, spb >> 3);
      FS_AllocZeroedPtr((void **)&pWorkBlock->paAssign,     NumBytes);
      //
      // Not all the work block descriptors are available if the number of work blocks 
      // specified in the device is smaller than the number of work blocks configured.
      //
      if (NumWorkBlocks) {
      _WB_AddToFreeList(pInst, pWorkBlock);
        NumWorkBlocks--;
      }
      pWorkBlock++;
    } while (--u);
  }
  //
  // O.K., we read the spare areas and fill the tables
  //
  EraseCntMax          = 0;
  EraseCntMin          = ERASE_CNT_INVALID;
  NumBlocksEraseCntMin = 0;
  IF_STATS(pInst->StatCounters.NumBadBlocks = 0);
  for (iBlock = 1; iBlock < pInst->NumPhyBlocks; iBlock++) {
    U8   Data;

    _ReadSpareIntoStaticBuffer(pInst, _BlockIndex2SectorIndex(pInst, iBlock));
    if (*(_pSpareAreaData + pInst->BadBlockOffset) != 0xFF) {
      IF_STATS(pInst->StatCounters.NumBadBlocks++);    
      continue;                                                     // This block is invalid and may not be used for anything !
    }
    Data     = *(_pSpareAreaData + SPARE_OFF_DATA_STATUS);
    lbi      = _LoadLBI(pInst);
    EraseCnt = _LoadEraseCnt(pInst, _pSpareAreaData);
    //
    // Is this a block containing valid data ?
    //
    if (EraseCnt != ERASE_CNT_INVALID) {
      //
      // Has this block been used as work block ?
      //
      if ((Data >> 4) == DATA_STAT_WORK) {
        //
        // If the work block is an invalid one
        // do a pre-erase.
        //
        if (lbi >= pInst->NumLogBlocks) {
          _PreEraseBlock(pInst, iBlock);
          _MarkBlockAsFree(pInst, iBlock);
          goto NextBlock;
        }
        if (pInst->pFirstWorkBlockFree) {
          //
          // Check if we already have a block with this lbi.
          // If we do, then we erase it and add it to the free list.
          //
          pWorkBlock = _FindWorkBlock(pInst, lbi);
          if (pWorkBlock) {
              FS_DEBUG_WARN((FS_MTYPE_DRIVER, "NAND: Found a work block with the same lbi."));
              _PreEraseBlock(pInst, iBlock);
              _MarkBlockAsFree(pInst, iBlock);
              goto NextBlock;
            }
          pWorkBlock      = _AllocWorkBlockDesc(pInst, lbi);
          pWorkBlock->pbi = iBlock;
        } else {
          FS_DEBUG_ERROROUT((FS_MTYPE_DRIVER,  "NAND: Found more work blocks than can be handled. Configuration changed ?"));
          _PreEraseBlock(pInst, iBlock);
          _MarkBlockAsFree(pInst, iBlock);
        }
NextBlock:
        continue;
      }
      //
      // Is this a block containing valid data ?
      //
      if ((Data >> 4) == DATA_STAT_VALID) {
        if (lbi >= pInst->NumLogBlocks) {
         _MarkBlockAsFree(pInst, iBlock);
          continue;
        }
        PBIPrev = _L2P_Read(pInst, lbi);
        if (PBIPrev == 0) {                                   // Has this lbi already been assigned ?
          _L2P_Write(pInst, lbi, iBlock);                     // Add block to the translation table
          if (EraseCnt > EraseCntMax) {
            EraseCntMax = EraseCnt;
          }
          continue;
        }
        if (_BlockDataIsMoreRecent(pInst, PBIPrev)) {
          _MarkBlockAsFree(pInst, iBlock);
          _PreEraseBlock(pInst, iBlock);
        } else {
          _MarkBlockAsFree(pInst, PBIPrev);
          _PreEraseBlock(pInst, PBIPrev);
          _L2P_Write(pInst, lbi, iBlock);                     // Add block to the translation table
        }
        if ((EraseCntMin == ERASE_CNT_INVALID) ||
            (EraseCnt < EraseCntMin)) {                       // Collect information for the active wear leveling
          EraseCntMin          = EraseCnt;
          NumBlocksEraseCntMin = 1;
        } else if (EraseCnt == EraseCntMin) {
          ++NumBlocksEraseCntMin;
        }
      }
    }
    //
    // Any other blocks are interpreted as free blocks.
    //
    _MarkBlockAsFree(pInst, iBlock);
  }
  pInst->EraseCntMax = EraseCntMax;
  pInst->EraseCntMin          = EraseCntMin;
  pInst->NumBlocksEraseCntMin = NumBlocksEraseCntMin;
  //
  // Handle the work blocks we found
  //
  pWorkBlock = pInst->pFirstWorkBlockInUse;
  while (pWorkBlock) {
    _LoadWorkBlock(pInst, pWorkBlock);
    pWorkBlock = pWorkBlock->pNext;
  }
  //
  // On debug builds we count here the number of valid sectors
  //
#if FS_NAND_ENABLE_STATS
  {
    U32 NumSectors;
    U32 NumSectorsInBlock;

    NumSectors = 0;
    for (iBlock = 0; iBlock < pInst->NumLogBlocks; ++iBlock) {
      NumSectorsInBlock  = _GetNumValidSectors(pInst, iBlock);
      NumSectors        += NumSectorsInBlock;
    }
    pInst->StatCounters.NumValidSectors = NumSectors;
  }
#endif
  return 0;          // O.K. !
}

/*********************************************************************
*
*       _LowLevelMountIfRequired
*
*  Function description
*    LL-Mounts the device if it is not LLMounted and this has not already been
*    tried in vain.
*
*  Return value
*    0     O.K., device has been successfully mounted and is accessible
*  !=0     Error
*/
static int _LowLevelMountIfRequired(NAND_INST * pInst) {
  int r;

  if (pInst->IsLLMounted) {
    return 0;                   // O.K., is mounted
  }
  if (pInst->LLMountFailed) {
    return 1;                   // Error, we could not mount it and do not want to try again
  }

  r = _LowLevelMount(pInst);
  if (r == 0) {
    pInst->IsLLMounted = 1;
  } else {
    pInst->LLMountFailed = 1;
  }
  return r;
}

/*********************************************************************
*
*       _ReadSector
*
*  Function Description
*    Reads one logical sectors from storage device.
*    There are 3 possibilities:
*    a) Data is in WorkBlock
*    b) There is a physical block assigned to this logical block -> Read from Hardware
*    c) There is a no physical block assigned to this logical block. This means data has never been written to storage. Fill data with 0.
*
*  Return value:
*      0                       - Data successfully read.
*    !=0                       - An error has occurred.
*
*/
static int _ReadSector(NAND_INST * pInst, U32 LogSectorIndex, U8 * pBuffer) {
  int r;
  unsigned lbi;
  unsigned pbi;
  unsigned Mask;
  unsigned brsiPhy;
  unsigned brsiLog;
  U32 PhySectorIndex;
  WORK_BLOCK_DESC * pWorkBlock;

  lbi = LogSectorIndex >> pInst->SPB_Shift;             // Log.  block index
  Mask = (1 << pInst->SPB_Shift) - 1;
  //
  // Physical block index is taken from Log2Phy table or is work block
  //
  pbi = _L2P_Read(pInst, lbi);          // Phys. block index
  brsiLog = LogSectorIndex & Mask;
  brsiPhy = brsiLog;
  pWorkBlock = _FindWorkBlock(pInst, lbi);
  if (pWorkBlock) {
    unsigned u;
    u = _brsiLog2Phy(pInst, pWorkBlock, brsiLog);
    if (u != BRSI_INVALID) {
      pbi = pWorkBlock->pbi;
      brsiPhy = u;
    }
  }

  //
  // Get data
  //
  if (pbi == 0) {
    //
    // Find physical page and fill buffer if it is not assigned
    //
    FS_MEMSET(pBuffer, 0, pInst->BytesPerSector);
    r = 0;                                          // O.K., we filled the buffer with 0-bytes since this sector has never been written.
  } else {
    //
    // Read from hardware
    //
    PhySectorIndex = _BlockIndex2SectorIndex(pInst, pbi) | brsiPhy;
    r = _ReadSectorWithECC(pInst, (U32*)pBuffer, PhySectorIndex);
    if ((r == NAND_NO_ERROR) || (r == NAND_1BIT_CORRECTED)) {
      r = 0;
    } else if (r < 0) {   // Sector is blank
      FS_MEMSET(pBuffer, 0xFF, pInst->BytesPerSector);     // Data should be blank. This one bit in NAND flash may be incorrect, we fill with 0xFF to be safe
      r = 0;
    } else if (r == NAND_ERROR_IN_ECC) {
      //
      // We found an error in the ECC but the data is OK.
      // Copy the data into another block and mark the source block as bad
      //
      if (pWorkBlock) {
        r = _ConvertWorkBlockViaCopy(pInst, pWorkBlock, BRSI_INVALID);
      } else {
        r = _RecoverDataBlock(pInst, pbi);
      }
    }
  }
  return r;
}

/*********************************************************************
*
*       _WriteLogSector
*
*  Function Description
*    Writes one logical sector to storage device.
*
*  Return value:
*      0                       - Data successfully written.
*    !=0                       - An error has occurred.
*
*/
static int _WriteLogSector(NAND_INST * pInst, U32 LogSectorIndex, const void * pBuffer) {
  U16 lbi;
  U32  Mask;
  WORK_BLOCK_DESC * pWorkBlock;
  unsigned brsiSrc;
  unsigned brsiDest;
  int               r;
  U32               SectorIndex;
  unsigned          brsiPhy;

  Mask = (1 << pInst->SPB_Shift) - 1;
  lbi      = (U16)(LogSectorIndex >> pInst->SPB_Shift);
  brsiSrc  = LogSectorIndex & Mask;
  brsiDest = ~0U;
  while (1) {
  //
    // Find (or create) a work block and the sector to be used in it.
  //
  pWorkBlock = _FindWorkBlock(pInst, lbi);
  if (pWorkBlock) {
    //
    // Make sure that sector to write is in work area and has not already been written
    //
    brsiDest = _FindFreeSectorInWorkBlock(pInst, pWorkBlock, brsiSrc);
    if (brsiDest == BRSI_INVALID) {
        r = _CleanWorkBlock(pInst, pWorkBlock);
        if (r) {
          return 1;
        }
      pWorkBlock = NULL;
    }
  }
  if (pWorkBlock == NULL) {
    pWorkBlock = _AllocWorkBlock(pInst, lbi);
      if (pWorkBlock == NULL) {
        return 1;
      }
    brsiDest = brsiSrc;      // Preferred position is free, so let's use it.
  }
  //
  // Write data into sector of work block
  //
  _ClearStaticSpareArea(pInst->BytesPerSector >> 5);
  if (brsiDest) {                  // BRSI is stored in the same place as LBI info, but only for the first sector in a block
    _StoreBRSI(brsiSrc);
  }
    SectorIndex = _BlockIndex2SectorIndex(pInst, pWorkBlock->pbi) | brsiDest;
    r = _WriteSector(pInst, (const U32 *)pBuffer, SectorIndex);
    if (r == 0) {
      break;                    // Data written
    }
    //
    // Could not write into work block. Save the data of this work block into data block
    // and try to find another work block to write into
    //
    r = _ConvertWorkBlockViaCopy(pInst, pWorkBlock, brsiDest);
    if (r) {
      return 1;
    }
  }
#if FS_NAND_ENABLE_STATS
  //
  // For debug builds only. Keep the number of valid sectors up to date
  //
  {
    unsigned pbiSrc;

    //
    // The number of valid sectors is increased only if the sector
    // is written for the first time since the last low-level format
    // or it is re-written after its value has been invalidated.
    //
    pbiSrc  = _L2P_Read(pInst, lbi);
    brsiPhy = _brsiLog2Phy(pInst, pWorkBlock, brsiSrc);
    if (brsiPhy == BRSI_INVALID) {  // Sector not written yet ?
      if (pbiSrc) {                 // Sector in data block ?
        U32 SectorIndexSrc;

        SectorIndexSrc  = _BlockIndex2SectorIndex(pInst, pbiSrc);
        SectorIndexSrc |= brsiSrc;
        if ((_SectorDataIsWritten(pInst, SectorIndexSrc) == 0) || _SectorDataIsInvalidated(pInst, SectorIndexSrc)) {
          pInst->StatCounters.NumValidSectors++;
        }
      } else {
        pInst->StatCounters.NumValidSectors++;
      }
    }
  }
#endif // FS_NAND_ENABLE_STATS
  //
  // Invalidate data previously used for the same brsi (if necessary)
  //
#if FS_NAND_ENABLE_TRIM
  brsiPhy      = _WB_ReadAssignment(pInst, pWorkBlock, brsiSrc);
  SectorIndex  = _BlockIndex2SectorIndex(pInst, pWorkBlock->pbi);
  SectorIndex |= brsiPhy;
  if (brsiPhy) {
    _ClearStaticSpareArea(pInst->BytesPerSector >> 5);
    _StoreBRSI(BRSI_INVALID);
    _InvalidateSectorDataFast(pInst, SectorIndex);
    _WriteSpareAreaFromStaticBuffer(pInst, SectorIndex);
  } else {
    if (brsiSrc == 0) {
      //
      // It is necessary to invalidate the data of the PhyBRSI 0
      // to be able to tell if the LogBRSI 0 has valid data or not
      //
      if (_WB_SectorIsWritten(pWorkBlock, brsiPhy)) {
        _InvalidateSectorData(pInst, SectorIndex);
      }
    }
  }
#else
  if (brsiSrc) {
    brsiPhy = _WB_ReadAssignment(pInst, pWorkBlock, brsiSrc);
    if (brsiPhy) {
      SectorIndex  = _BlockIndex2SectorIndex(pInst, pWorkBlock->pbi);
      SectorIndex |= brsiPhy;
    _ClearStaticSpareArea(pInst->BytesPerSector >> 5);
    _StoreBRSI(BRSI_INVALID);
    _WriteSpareAreaFromStaticBuffer(pInst, SectorIndex);
  }
  }
#endif // FS_NAND_ENABLE_TRIM
  //
  // Update work block management info
  //
  _MarkWorkBlockAsMRU(pInst, pWorkBlock);
  _WB_MarkSectorAsUsed(pWorkBlock, brsiDest);                 // Mark sector as used
  _WB_WriteAssignment(pInst, pWorkBlock, brsiSrc, brsiDest);  // Update look-up-table
  return 0;
}

/*********************************************************************
*
*        _FreeOneSector
*/
#if FS_NAND_ENABLE_TRIM
static int _FreeOneSector(NAND_INST * pInst, U32 LogSectorIndex) {
  unsigned lbi;
  unsigned pbi;
  U32      PhySectorIndex;
  unsigned brsiLog;
  unsigned brsiPhy;
  U32      Mask;
  WORK_BLOCK_DESC * pWorkBlock;
  int      r;

  r       = 1;    // No sector freed yet
  lbi     = LogSectorIndex >> pInst->SPB_Shift;
  pbi     = _L2P_Read(pInst, lbi);
  Mask    = (1 << pInst->SPB_Shift) - 1;
  brsiLog = LogSectorIndex & Mask;
  //
  // If necessary, mark the sector as free in the data block
  //
  if (pbi) {                          // Sector in a data block ?
    PhySectorIndex  = _BlockIndex2SectorIndex(pInst, pbi);
    PhySectorIndex |= brsiLog;
    //
    // Invalidate only sectors which contain valid data and are not aready invalidated
    //
    if (_SectorDataIsWritten(pInst, PhySectorIndex)) {
      if (_SectorDataIsInvalidated(pInst, PhySectorIndex) == 0) {
        _InvalidateSectorData(pInst, PhySectorIndex);
        r = 0;          // Sector has been freed
      }
    }
  }
  //
  // If necessary, mark the sector as free in the work block
  //
  pWorkBlock = _FindWorkBlock(pInst, lbi);
  if (pWorkBlock) {
    brsiPhy = _brsiLog2Phy(pInst, pWorkBlock, brsiLog);
    if (brsiPhy != BRSI_INVALID) {          // Sector in a work block ?
      PhySectorIndex  = _BlockIndex2SectorIndex(pInst, pWorkBlock->pbi);
      PhySectorIndex |= brsiPhy;
      if (brsiPhy) {
        //
        // Mark on the medium the sector data as invalid
        //
        _ClearStaticSpareArea(pInst->BytesPerSector >> 5);
        _StoreBRSI(BRSI_INVALID);
        _InvalidateSectorDataFast(pInst, PhySectorIndex);
        _WriteSpareAreaFromStaticBuffer(pInst, PhySectorIndex);
        //
        // Remove the assignment of the logical sector
        //
        _WB_WriteAssignment(pInst, pWorkBlock, brsiLog, 0);
        r = 0;          // Sector has been freed
      } else {
        _InvalidateSectorData(pInst, PhySectorIndex);
        r = 0;          // Sector has been freed
      }
    }
  }
  return r;
}
#endif // FS_NAND_ENABLE_TRIM

/*********************************************************************
*
*        _FreeSectors
*
*   Function description
*     Marks a logical sector as free. This routine is called from the
*     higher layer file system to help the driver to manage the data:
*     This way sectors which are no longer in use by the higher
*     layer file system do not need to be copied.
*
*   Parameters
*     pInst           Driver instance
*     LogSectorIndex  Index of the first logical sector to be freed
*     NumSectors      Number of sectors to be freed
*
*   Return value
*     ==0   Sectors have been freed.
*     !=0   An error occurred.
*/
#if FS_NAND_ENABLE_TRIM
static int _FreeSectors(NAND_INST * pInst, U32 LogSectorIndex, U32 NumSectors) {
#if FS_DEBUG_LEVEL >= FS_DEBUG_LEVEL_CHECK_ALL
  //
  // On debug builds check whether the sectors are in range.
  //
  if (NumSectors) {
    U32 FirstSector;
    U32 LastSector;
    U32 NumSectorsTotal;

    FirstSector     = LogSectorIndex;
    LastSector      = LogSectorIndex + NumSectors - 1;
    NumSectorsTotal = pInst->NumSectors;
    if ((FirstSector >= NumSectorsTotal) || (LastSector >= NumSectorsTotal)) {
      FS_DEBUG_ERROROUT((FS_MTYPE_DRIVER,  "NAND: Could not free sectors. Invalid sector range.\n"));
      return 1;       // Error, trying to free sectors which do not exist.
    }
  }
#endif
  do {
    if (_FreeOneSector(pInst, LogSectorIndex) == 0) {
      IF_STATS(pInst->StatCounters.NumValidSectors--);
    }
    LogSectorIndex++;
  } while (--NumSectors);
  return 0;
}
#endif // FS_NAND_ENABLE_TRIM

/*********************************************************************
*
*        _GetSectorUsage
*
*   Function description
*     Checks if a logical sector contains valid data.
*
*   Return values
*     ==0     Sector in use (contains valid data)
*     ==1     Sector not in use (was not written nor was invalidated)
*     ==2     Usage unknown
*/
static int _GetSectorUsage(NAND_INST * pInst, U32 LogSectorIndex) {
  unsigned lbi;
  unsigned pbi;
  U32      PhySectorIndex;
  unsigned brsiLog;
  unsigned brsiPhy;
  U32      Mask;
  WORK_BLOCK_DESC * pWorkBlock;
  int      r;

#if FS_DEBUG_LEVEL >= FS_DEBUG_LEVEL_CHECK_ALL
  {
    U32 NumSectorsTotal;

    NumSectorsTotal = pInst->NumSectors;
    if (LogSectorIndex >= NumSectorsTotal) {
      FS_DEBUG_ERROROUT((FS_MTYPE_DRIVER,  "NAND: Could get sector usage. Invalid sector index.\n"));
      return 2;               // Error, trying to get usage of a non-existent sector.
    }
  }
#endif
  r       = 1;                // Sector not in use
  lbi     = LogSectorIndex >> pInst->SPB_Shift;
  pbi     = _L2P_Read(pInst, lbi);
  Mask    = (1 << pInst->SPB_Shift) - 1;
  brsiLog = LogSectorIndex & Mask;
  //
  // First check if the sector is in a data block
  //
  if (pbi) {                  // Sector in a data block ?
    PhySectorIndex  = _BlockIndex2SectorIndex(pInst, pbi);
    PhySectorIndex |= brsiLog;
    if (_SectorDataIsWritten(pInst, PhySectorIndex) && (_SectorDataIsInvalidated(pInst, PhySectorIndex) == 0)) {
      r = 0;                  // Sector has valid data
    }
  }
  if (r == 0) {
    pWorkBlock = _FindWorkBlock(pInst, lbi);
    if (pWorkBlock) {
      brsiPhy = _brsiLog2Phy(pInst, pWorkBlock, brsiLog);
      if (brsiPhy != BRSI_INVALID) {          // Sector in a work block ?
        r = 0;                // Sector has valid data
      }
    }
  }
  return r;
}

/*********************************************************************
*
*       _CleanOne
*
*   Function description
*     Executes a single "clean" job. This might be the conversion of a work block into a data block
*     or the erase of a block marked as invalid.
*
*   Return value
*     ==0   Nothing else to "clean"
*     ==1   At least one more "clean" job left to do
*/
#if FS_NAND_ENABLE_CLEAN
static int _CleanOne(NAND_INST * pInst) {
  //
  // Clean work blocks first
  //
  if (pInst->pFirstWorkBlockInUse) {
    _CleanWorkBlock(pInst, pInst->pFirstWorkBlockInUse);
  }
  //
  // Now check if there is more work to do
  //
  if (pInst->pFirstWorkBlockInUse) {
    return 1;     // At least one more work block to clean
  }
  return 0;
}
#endif // FS_NAND_ENABLE_CLEAN

/*********************************************************************
*
*       _Clean
*
*  Function description
*    Converts all work blocks into data blocks and erases all free blocks.
*/
#if FS_NAND_ENABLE_CLEAN
static void _Clean(NAND_INST * pInst) {
  _CleanAllWorkBlocks(pInst);
}
#endif // FS_NAND_ENABLE_CLEAN

/*********************************************************************
*
*       _LowLevelFormat
*
*  Function description
*    Erases all blocks and writes the format information to the first one.
*
*  Return value:
*     0  O.K.
*  != 0  Error
*/
static int _LowLevelFormat(NAND_INST * pInst) {
  int        Status;
  U32        NumPhyBlocks;
  unsigned   BlockIndex;
  U8       * pPageBuffer;
  U32        OffStatus; // = 0xffffffff;

  Status                = -1;
  pInst->LLMountFailed  = 0;
  pInst->IsLLMounted    = 0;
  pPageBuffer           = (U8 *)_pSectorBuffer;

  OffStatus = (pInst->BytesPerPage == 512) ? 5 : 0;
  //
  // Erase the first sector/phy. block. This block is guaranteed to be valid.
  //
  Status    = _EraseBlock(pInst, 0);
  if (Status) {
    return 1; // Error
  }
  //
  // Erase NAND flash blocks which are valid.
  // Valid NAND flash blocks are blocks that
  // contain a 0xff in the first byte of the spare area of the first two pages of the block
  //
  NumPhyBlocks   = pInst->NumPhyBlocks;
  for (BlockIndex = 1; BlockIndex < NumPhyBlocks; BlockIndex++) {
    if (_BlockCanBeErased(pInst, BlockIndex)) {
      _EraseBlock(pInst, BlockIndex);
      } else {
       IF_STATS(pInst->StatCounters.NumBadBlocks++);      // Count one bad block
    }
  }
  IF_STATS(pInst->StatCounters.NumValidSectors = 0);
  //
  // Write the Format information to first sector of the first block
  //
  FS_MEMSET(pPageBuffer, 0xFF, pInst->BytesPerSector);
  FS_MEMCPY(pPageBuffer, _acInfo, sizeof(_acInfo));
  FS_StoreU32BE(pPageBuffer + INFO_OFF_LLFORMAT_VERSION, LLFORMAT_VERSION);
  FS_StoreU32BE(pPageBuffer + INFO_OFF_SECTOR_SIZE,      FS_Global.MaxSectorSize);
  FS_StoreU32BE(pPageBuffer + INFO_OFF_BAD_BLOCK_OFFSET, OffStatus);
  FS_StoreU32BE(pPageBuffer + INFO_OFF_NUM_LOG_BLOCKS,   pInst->NumLogBlocks);
  FS_StoreU32BE(pPageBuffer + INFO_OFF_NUM_WORK_BLOCKS,  pInst->NumWorkBlocks);
  _ClearStaticSpareArea(pInst->BytesPerSector >> 5);
  return _WriteSector(pInst, _pSectorBuffer, FORMAT_INFO_SECTOR_INDEX);
}

/*********************************************************************
*
*       Global functions
*
**********************************************************************
*/


/*********************************************************************
*
*       _NAND_GetStatus
*/
static int _NAND_GetStatus(U8 Unit) {
  FS_USE_PARA(Unit);
  return FS_MEDIA_IS_PRESENT;
}


/*********************************************************************
*
*       _NAND_Write
*
*   Function description
*    FS driver function. Writes one or more logical sectors to storage device.
*
*   Parameters
*     Unit          Driver unit number
*     SectorNo      Index of the first logical sector to modify
*     p             [IN]  Data to be written
*                   [OUT] ---
*     NumSectors    Number of sectors to write.
*     RepeateSame   Set to 1 if the same data should be written to all sectors.
*
*   Return value
*     ==0     Data successfully written.
*     !=0     An error has occurred.
*
*/
static int _NAND_Write(U8 Unit, U32 SectorNo, const void  * p, U32 NumSectors, U8  RepeatSame) {
  const U8  * pBuffer;
  NAND_INST * pInst;
  int         r;

  ASSERT_UNIT_NO_IS_IN_RANGE(Unit);
  pInst = _apInst[Unit];
  pBuffer = (const U8 *)p;
  r = _LowLevelMountIfRequired(pInst);
  if (r) {
    return r;
  }
  if (pInst->HasFatalError) {
    FS_DEBUG_ERROROUT((FS_MTYPE_DRIVER, "NAND: Failed to write. Medium has a fatal error."));
    return 1;
  }
  if (pInst->IsWriteProtected) {
    return 1;
  }
#if FS_DEBUG_LEVEL >= FS_DEBUG_LEVEL_CHECK_ALL
  //
  // On debug builds check whether the sectors are in range.
  //
  if (NumSectors) {
    U32 FirstSector;
    U32 LastSector;
    U32 NumSectorsTotal;

    FirstSector     = SectorNo;
    LastSector      = SectorNo + NumSectors - 1;
    NumSectorsTotal = pInst->NumSectors;
    if ((FirstSector >= NumSectorsTotal) || (LastSector >= NumSectorsTotal)) {
      FS_DEBUG_ERROROUT((FS_MTYPE_DRIVER,  "NAND: Could not write sectors. Invalid sector range."));
      return 1;       // Error, trying to write sectors which do not exist.
    }
  }
#endif
  //
  // Write data one sector at a time
  //
  do {
    r = _WriteLogSector(pInst, SectorNo, pBuffer);
    if (r) {
      return 1;       // Error, could not write data.
    }
    if (pInst->HasFatalError) {
      return 1;       // Error, a fatal error occurred while writing data.
    }
    IF_STATS(pInst->StatCounters.WriteSectorCnt++);
    if (--NumSectors == 0) {
      break;
    }
    if (RepeatSame == 0) {
      pBuffer += pInst->BytesPerSector;
    }
    SectorNo++;
  } while (1);
  return 0;           // O.K., sector data written.
}

/*********************************************************************
*
*       _NAND_Read
*
*   Function description
*     FS driver function. Reads one or more logical sectors from storage device.
*
*   Parameters
*     Unit        Driver unit number
*     SectorNo    Index of the first logical sector to be read
*     p           [IN]  ---
*                 [OUT] Sector data read from device.
*     NumSectors  Number of logical sectors to read.
*
*   Return value
*     ==0   Data successfully read
*     !=0   An error has occurred
*
*/
static int _NAND_Read(U8 Unit, U32 SectorNo, void * p, U32 NumSectors) {
  U8        * pBuffer;
  NAND_INST * pInst;
  int         r;

  ASSERT_UNIT_NO_IS_IN_RANGE(Unit);
  pInst   = _apInst[Unit];
  pBuffer = (U8 *)p;
  //
  // Make sure device is low-level mounted. If it is not, there is nothing we can do.
  //
  r = _LowLevelMountIfRequired(pInst);
  if (r) {
    return 1;         // Error, could not mount NAND flash.
  }
#if FS_DEBUG_LEVEL >= FS_DEBUG_LEVEL_CHECK_ALL
  //
  // On debug builds check whether the sectors are in range.
  //
  if (NumSectors) {
    U32 FirstSector;
    U32 LastSector;
    U32 NumSectorsTotal;

    FirstSector     = SectorNo;
    LastSector      = SectorNo + NumSectors - 1;
    NumSectorsTotal = pInst->NumSectors;
    if ((FirstSector >= NumSectorsTotal) || (LastSector >= NumSectorsTotal)) {
      FS_DEBUG_ERROROUT((FS_MTYPE_DRIVER,  "NAND: Could not read sectors. Invalid sector range."));
      return 1;       // Error, trying to read sectors which do not exist.
    }
  }
#endif
  //
  // Read data one sector at a time.
  //
  do {
    r = _ReadSector(pInst, SectorNo, pBuffer);
    if (r) {
      FS_DEBUG_ERROROUT((FS_MTYPE_DRIVER,  "NAND: Failed to read sector."));
      return 1;       // Error, could not read sector data.
    }
    pBuffer += pInst->BytesPerSector;
    SectorNo++;
    IF_STATS(pInst->StatCounters.ReadSectorCnt++);
  } while (--NumSectors);
  return 0;                                    // O.K.
}


/*********************************************************************
*
*       _NAND_IoCtl
*/
static int _NAND_IoCtl(U8 Unit, I32 Cmd, I32 Aux, void * pBuffer) {
  NAND_INST   * pInst;
  FS_DEV_INFO * pDevInfo;
  int           r;

  FS_USE_PARA(Aux);
  ASSERT_UNIT_NO_IS_IN_RANGE(Unit);
  pInst    = _apInst[Unit];
  r        = -1;
  switch (Cmd) {
  case FS_CMD_GET_DEVINFO:
    if (pBuffer) {
      //
      // This low-level mount is required in oder to calculate
      // the correct number of sectors available to the file system
      //
      r = _LowLevelMountIfRequired(pInst);
      if (r == 0) {
        pDevInfo = (FS_DEV_INFO *)pBuffer;
        pDevInfo->NumSectors     = pInst->NumSectors;
        pDevInfo->BytesPerSector = pInst->BytesPerSector;
      }
    }
    break;
  case FS_CMD_FORMAT_LOW_LEVEL:
    return _LowLevelFormat(pInst);
  case FS_CMD_REQUIRES_FORMAT:
    return _LowLevelMountIfRequired(pInst);
  case FS_CMD_UNMOUNT:
  case FS_CMD_UNMOUNT_FORCED:
    pInst->IsLLMounted     = 0;
    pInst->MRUFreeBlock    = 0;
    pInst->pFirstWorkBlockFree  = NULL;
    pInst->pFirstWorkBlockInUse = NULL;
#if FS_NAND_ENABLE_STATS
    FS_MEMSET(&pInst->StatCounters, 0, sizeof(pInst->StatCounters));
#endif
    r = 0;
    break;
#if FS_NAND_ENABLE_CLEAN
  case FS_CMD_CLEAN_ONE:
    r = _LowLevelMountIfRequired(pInst);
    if (r == 0) {
      int   More;
      int * pMore;

      More = _CleanOne(pInst);
      pMore = (int *)pBuffer;
      if (pMore) {
        *pMore = More;
      }
    }
    break;
  case FS_CMD_CLEAN:
    r = _LowLevelMountIfRequired(pInst);
    if (r == 0) {
      _Clean(pInst);
    }
    break;
#endif
  case FS_CMD_GET_SECTOR_USAGE:
    if (pBuffer) {
      r = _LowLevelMountIfRequired(pInst);
      if (r == 0) {
        int * pSectorUsage;

        pSectorUsage = (int *)pBuffer;
        *pSectorUsage = _GetSectorUsage(pInst, (U32)Aux);
      }
    }
    break;
#if FS_NAND_ENABLE_TRIM
  case FS_CMD_FREE_SECTORS:
    r = _LowLevelMountIfRequired(pInst);
    if (r == 0) {
      U32 SectorIndex;
      U32 NumSectors;

      SectorIndex = (U32)Aux;
      NumSectors  = *(U32 *)pBuffer;
      r = _FreeSectors(pInst, SectorIndex, NumSectors);
    }
    break;
#endif
#if FS_SUPPORT_DEINIT
  case FS_CMD_DEINIT:
    {
      unsigned      u;

      FS_FREE(pInst->pLog2PhyTable);
      pInst->pLog2PhyTable = 0; //-Q-
      FS_FREE(pInst->pFreeMap);
      pInst->pFreeMap = 0;  //-Q-
      if (pInst->paWorkBlock) {       // The array is allocated only when the volume is mounted.
        for (u = 0; u < pInst->NumWorkBlocks; u++) {
          WORK_BLOCK_DESC * pWorkBlock;

          pWorkBlock = &pInst->paWorkBlock[u];
          FS_FREE(pWorkBlock->paIsWritten);
          pWorkBlock->paIsWritten = 0;  //-Q-
          FS_FREE(pWorkBlock->paAssign);
          pWorkBlock->paAssign = 0; //-Q-
        }
        FS_FREE(pInst->paWorkBlock);
        pInst->paWorkBlock = 0; //-Q-
      }
      FS_FREE(pInst);
      pInst = 0;    //-Q-
      _apInst[Unit] = 0;
      //
      // If all instances have been removed, remove the sector buffers.
      //
      if (--_NumUnits == 0) {
        FS_FREE(_pSectorBuffer);
        _pSectorBuffer = 0;
        FS_FREE(_pSpareAreaData);
        _pSpareAreaData = 0;
      }
      r = 0;
    }
    break;
#endif
  }
  return r;
}


/*********************************************************************
*
*       _AllocInstIfRequired
*
*  Description:
*    Allocate memory for the specified unit if required.
*
*/
static NAND_INST * _AllocInstIfRequired(U8 Unit) {
  NAND_INST * pInst;

  if (Unit >= NUM_UNITS) {
    FS_DEBUG_ERROROUT((FS_MTYPE_DRIVER, "Invalid Unit number specified! Unit no shall be less than %d. Given %d.", NUM_UNITS, Unit));
    FS_X_Panic(FS_ERROR_UNKNOWN_DEVICE);
    return NULL;
  }
  pInst = _apInst[Unit];
  if (pInst == NULL) {
    FS_AllocZeroedPtr((void**)&_apInst[Unit], sizeof(NAND_INST));
  pInst = _apInst[Unit];
  }
  pInst->Unit    = Unit;
  return pInst;
}


/*********************************************************************
*
*       _NAND_AddDevice
*
*  Description:
*    Initializes the low-level driver object.
*
*  Return value:
*    >= 0                       - Command successfully executed, Unit no.
*    <  0                       - Error, could not add device
*
*/
static int _NAND_AddDevice(void) {
  if (_NumUnits >= NUM_UNITS) {
    return -1;
  }
  _AllocInstIfRequired((U8)_NumUnits);
  return _NumUnits++;
}


/*********************************************************************
*
*       _NAND_InitMedium
*
*  Description:
*    Initialize and identifies the storage device.
*
*  Return value:
*    == 0                       - Device ok and ready for operation.
*    <  0                       - An error has occurred.
*/
static int _NAND_InitMedium(U8 Unit) {
  NAND_INST * pInst;

  ASSERT_UNIT_NO_IS_IN_RANGE(Unit);
  pInst    = _apInst[Unit];
  ASSERT_PHY_TYPE_IS_SET(pInst);
  FS_AllocZeroedPtr((void **)&_pSectorBuffer,   FS_Global.MaxSectorSize);
  FS_AllocZeroedPtr((void **)&_pSpareAreaData , FS_Global.MaxSectorSize >> 5);
  if (_ReadApplyDeviceParas(pInst)) {
    return 1;                      // Failed to identify NAND Flash or unsupported type
  }
  if (pInst->pPhyType->pfIsWP(Unit)) {
    pInst->IsWriteProtected = 1;
  }
  return 0;                        // Device is accessible
}

/*********************************************************************
*
*       _NAND_GetNumUnits
*/
static int _NAND_GetNumUnits(void) {
  return _NumUnits;
}

/*********************************************************************
*
*       _NAND_GetDriverName
*/
static const char * _NAND_GetDriverName(U8 Unit) {
  FS_USE_PARA(Unit);
  return "nand";
}

/*********************************************************************
*
*       API Table
*
**********************************************************************
*/
const FS_DEVICE_TYPE FS_NAND_Driver = {
  _NAND_GetDriverName,
  _NAND_AddDevice,
  _NAND_Read,
  _NAND_Write,
  _NAND_IoCtl,
  _NAND_InitMedium,
  _NAND_GetStatus,
  _NAND_GetNumUnits
};

/*********************************************************************
*
*       Public code
*
**********************************************************************
*/

/*********************************************************************
*
*       FS_NAND_GetStatCounters
*/
void FS_NAND_GetStatCounters(U8 Unit, FS_NAND_STAT_COUNTERS * pStat) {
#if FS_NAND_ENABLE_STATS
  NAND_INST * pInst;

  ASSERT_UNIT_NO_IS_IN_RANGE(Unit);
  pInst  = _AllocInstIfRequired(Unit);
  *pStat = pInst->StatCounters;
#else
  FS_USE_PARA(Unit);
  FS_MEMSET(pStat, 0, sizeof(FS_NAND_STAT_COUNTERS));
#endif
}

/*********************************************************************
*
*       FS_NAND_ResetStatCounters
*/
void FS_NAND_ResetStatCounters(U8 Unit) {
#if FS_NAND_ENABLE_STATS
  NAND_INST * pInst;
  FS_NAND_STAT_COUNTERS * pStat;

  ASSERT_UNIT_NO_IS_IN_RANGE(Unit);
  pInst  = _AllocInstIfRequired(Unit);
  pStat = &pInst->StatCounters;
  pStat->ConvertInPlaceCnt = 0;
  pStat->ConvertViaCopyCnt = 0;
  pStat->CopySectorCnt     = 0;
  pStat->EraseCnt          = 0;
  pStat->NumReadRetries    = 0;
  pStat->ReadDataCnt       = 0;
  pStat->ReadSectorCnt     = 0;
  pStat->ReadSpareCnt      = 0;
  pStat->WriteDataCnt      = 0;
  pStat->WriteSectorCnt    = 0;
  pStat->WriteSpareCnt     = 0;
#else
  FS_USE_PARA(Unit);
#endif
}

/*********************************************************************
*
*       FS_NAND_SetPhyType
*/
void FS_NAND_SetPhyType(U8 Unit, const FS_NAND_PHY_TYPE * pPhyType) {
  NAND_INST * pInst;

  ASSERT_UNIT_NO_IS_IN_RANGE(Unit);
  pInst  = _AllocInstIfRequired(Unit);
  pInst->pPhyType = pPhyType;
}

/*********************************************************************
*
*       FS_NAND_SetBlockRange
*/
void FS_NAND_SetBlockRange(U8 Unit, U16 FirstBlock, U16 MaxNumBlocks) {
  NAND_INST * pInst;

  ASSERT_UNIT_NO_IS_IN_RANGE(Unit);
  pInst  = _AllocInstIfRequired(Unit);
  pInst->FirstBlock   = FirstBlock;
  pInst->MaxNumBlocks = MaxNumBlocks;
}

/*********************************************************************
*
*       FS_NAND_IsLLFormatted
*/
int FS_NAND_IsLLFormatted(U8 Unit) {
  NAND_INST       * pInst;
  int         r;

  ASSERT_UNIT_NO_IS_IN_RANGE(Unit);
  pInst = _AllocInstIfRequired(Unit);
  _NAND_InitMedium(Unit);
  r = _LowLevelMountIfRequired(pInst);
  if (r) {
    return 0;     // NAND flash not formatted.
  }
  return 1;       // NAND flash formatted.
}

/*********************************************************************
*
*       FS_NAND_SetMaxEraseCntDiff
*/
void FS_NAND_SetMaxEraseCntDiff(U8 Unit, U32 EraseCntDiff) {
  NAND_INST * pInst;

  ASSERT_UNIT_NO_IS_IN_RANGE(Unit);
  pInst  = _AllocInstIfRequired(Unit);
  pInst->MaxEraseCntDiff = EraseCntDiff;
}

/*********************************************************************
*
*       FS_NAND_SetNumWorkBlocks
*/
void FS_NAND_SetNumWorkBlocks(U8 Unit, U32 NumWorkBlocks) {
  NAND_INST * pInst;

  ASSERT_UNIT_NO_IS_IN_RANGE(Unit);
  pInst = _AllocInstIfRequired(Unit);
  pInst->NumWorkBlocksConf = NumWorkBlocks;
}

/*********************************************************************
*
*       FS_NAND_FormatLow
*/
int FS_NAND_FormatLow(U8 Unit) {
  NAND_INST * pInst;

  pInst  = _AllocInstIfRequired(Unit);
  _NAND_InitMedium(Unit);
  return _LowLevelFormat(pInst);
}

/*********************************************************************
*
*       FS_NAND_ReadPhySector
*
*  Function description:
*    This function will read a physical sector from NAND flash.
*
*  Parameters:
*    Unit             Unit/Index number of the NAND flash to read from
*    PhySectorIndex   Physical sector index
*    pData            Pointer to a buffer to store read data
*    pNumBytesData    [IN]  Pointer to variable storing the size of the data buffer
*                     [OUT] The number of bytes that were stored in the data buffer
*    pSpare           Pointer to a buffer to store read spare data
*    pNumBytesSpare   [IN]  Pointer to variable storing the size of the spare data buffer
*                     [OUT] The number of bytes that were stored in the spare data buffer
*
*  Return value
*     -1                        OK, page is blank
*     NAND_NO_ERROR             OK
*     NAND_1BIT_CORRECTED       OK
*     NAND_ERROR_IN_ECC         OK
*     NAND_UNCORRECTABLE_ERROR  Error
*     NAND_READ_ERROR           Error
*/
int FS_NAND_ReadPhySector(U8 Unit, U32 PhySectorIndex, void * pData, unsigned * pNumBytesData, void * pSpare, unsigned * pNumBytesSpare) {
  NAND_INST * pInst;
  U32         NumPhySectors;
  int         r = -1;

  //
  // Allocate memory if necessary
  //
  pInst = _AllocInstIfRequired(Unit);
  //
  // Initialize the NAND flash, so that all necessary information
  // of the NAND flash are available.
  //
  _NAND_InitMedium(Unit);
  //
  // Mount the NAND flash if necessary
  //
  _LowLevelMountIfRequired(pInst);
  NumPhySectors = (U32)pInst->NumPhyBlocks * (1 << pInst->SPB_Shift);
  if (PhySectorIndex < NumPhySectors) {
    U32 NumBytes2Copy;

    r = _ReadSectorWithECC(pInst, _pSectorBuffer, PhySectorIndex);
    NumBytes2Copy = MIN(pInst->BytesPerSector, *pNumBytesData);
    *pNumBytesData = NumBytes2Copy;
    FS_MEMCPY(pData, _pSectorBuffer, NumBytes2Copy);
    NumBytes2Copy = MIN((unsigned)pInst->BytesPerSector >> 5, *pNumBytesSpare);
    *pNumBytesSpare = NumBytes2Copy;
    FS_MEMCPY(pSpare, _pSpareAreaData, NumBytes2Copy);
  }
  return r;
}

/*********************************************************************
*
*       FS_NAND_EraseFlash
*
*  Function description:
*    This function will erase the whole NAND flash.
*    Please use this function with care, since it will erase all the blocks of the flash
*    and therefore also erases the bad block information.
*    Can be used without this side effect on flashes that are guaranteed to have no bad blocks,
*    such as most ATMEL Dataflashes
*
*  Parameters:
*    Unit    - Number/Index of the NAND flash to erase
*
*/
void FS_NAND_EraseFlash(U8 Unit) {
  NAND_INST * pInst;
  U32         NumPhyBlocks;
  unsigned    Block;
  int         Status = 0;

  //
  // Allocate memory if necessary
  //
  pInst = _AllocInstIfRequired(Unit);
  //
  // Initialize the NAND flash, so that all necessary information
  // of the NAND flash are available.
  //
  _NAND_InitMedium(Unit);
  //
  // Erase NAND flash blocks which are valid.
  // Valid NAND flash blocks are blocks that
  // contain a 0xff in the first byte of the spare area of the first two pages of the block
  //
  NumPhyBlocks   = pInst->NumPhyBlocks;
  for (Block = 0; Block < NumPhyBlocks; Block++) {
    Status = _EraseBlock(pInst, Block);
    //
    // If we could not erase the block
    //
    if (Status) {
      FS_DEBUG_ERROROUT((FS_MTYPE_DRIVER, "NAND: Failed to erase block %d.", Block));
    }
  }
}

/*********************************************************************
*
*       FS_NAND_GetDiskInfo
*
*  Function description:
*    Returns information about the NAND flash disk
*    This function is not required for the functionality of the driver and will typically not be linked in in production builds.
*
*/
void FS_NAND_GetDiskInfo(U8 Unit, FS_NAND_DISK_INFO * pDiskInfo) {
  NAND_INST  * pInst;

  _NAND_InitMedium(Unit);
  pInst = _apInst[Unit];
  FS_MEMSET(pDiskInfo, 0, sizeof(FS_NAND_DISK_INFO));
  if (pInst) {
    unsigned iBlock;
    U32      NumPhyBlocks;
    U32      NumUsedPhyBlocks;
    U32      NumBadPhyBlocks;
    U32      EraseCntMax = 0;
    U32      EraseCntMin;
    U32      EraseCntAvg;
    U32      EraseCnt;
    U32      EraseCntTotal;
    U32      NumEraseCnt;

    //
    // Low level mount the drive if not already done
    //
    if (_LowLevelMountIfRequired(pInst)) {
      //
      // If the low-level mount failed, do nothing.
      //
      FS_DEBUG_ERROROUT((FS_MTYPE_DRIVER, "NAND device could not be low-level-mounted. The device may not be low-level-formatted."));
      return;
    }
    //
    // Retrieve the number of used physical blocks and block status
    //
    NumUsedPhyBlocks = 0;
    NumBadPhyBlocks  = 0;
    NumPhyBlocks     = pInst->NumPhyBlocks;
    EraseCntMax      = 0;
    EraseCntMin      = 0xFFFFFFFF;
    NumEraseCnt      = 0;
    EraseCntTotal    = 0;
    for (iBlock = 0; iBlock < NumPhyBlocks; iBlock++) {
      U32 PageIndex;
      U8  aSpare[6];   // Large enough to hold the bad block status and the erase count
      //
      // Check if block is free
      //
      if (_BlockIsFree(pInst, iBlock) == 0) {
        NumUsedPhyBlocks++;
      }
      //
      // The sparea are of the first page in a block stores the bad block status
      // information and the erase count.
      //
      PageIndex = (U32)iBlock << pInst->PPB_Shift;
      PageIndex += pInst->FirstBlock << pInst->PPB_Shift;
      if (_BlockIsBad(pInst, iBlock)) {
        NumBadPhyBlocks++;
      } else {
        _ReadPhySpare(pInst, PageIndex, aSpare, 0, sizeof(aSpare));
        EraseCnt = _LoadEraseCnt(pInst, aSpare);
        if (EraseCnt != ERASE_CNT_INVALID) {
          if (EraseCnt > EraseCntMax) {
            EraseCntMax = EraseCnt;
          }
          if (EraseCnt < EraseCntMin) {
            EraseCntMin = EraseCnt;
          }
          EraseCntTotal += EraseCnt;
          ++NumEraseCnt;
        }
      }
    }
    if (NumEraseCnt) {
    EraseCntAvg = EraseCntTotal / NumEraseCnt;
    } else {
      EraseCntAvg = 0;
    }
    pDiskInfo->NumPhyBlocks       = NumPhyBlocks;
    pDiskInfo->NumLogBlocks       = pInst->NumLogBlocks;
    pDiskInfo->NumPagesPerBlock   = 1 << pInst->PPB_Shift;
    pDiskInfo->NumSectorsPerBlock = 1 << pInst->SPB_Shift;
    pDiskInfo->BytesPerPage       = pInst->BytesPerPage;
    pDiskInfo->BytesPerSector     = pInst->BytesPerSector;
    pDiskInfo->NumUsedPhyBlocks   = NumUsedPhyBlocks;
    pDiskInfo->NumBadPhyBlocks    = NumBadPhyBlocks;
    pDiskInfo->EraseCntMax        = EraseCntMax;
    pDiskInfo->EraseCntMin        = EraseCntMin;
    pDiskInfo->EraseCntAvg        = EraseCntAvg;
    pDiskInfo->IsWriteProtected   = pInst->IsWriteProtected;
    pDiskInfo->HasFatalError      = pInst->HasFatalError;
    pDiskInfo->ErrorSectorIndex   = pInst->ErrorSectorIndex;
    pDiskInfo->ErrorType          = pInst->ErrorType;
  }
}

/*********************************************************************
*
*       FS_NAND_GetBlockInfo
*
*   Functiond description
*     Returns information about a particular physical block
*    This function is not required for the functionality of the driver and will typically not be linked in in production builds.
*/
void FS_NAND_GetBlockInfo(U8 Unit, U32 PhyBlockIndex, FS_NAND_BLOCK_INFO * pBlockInfo) {
  NAND_INST  * pInst;

  //
  // Allocate memory if necessary
  //
  pInst = _AllocInstIfRequired(Unit);
  //
  // Initialize the NAND flash, so that all necessary information
  // of the NAND flash are available.
  //
  _NAND_InitMedium(Unit);
  //
  // Low level mount the drive if not already done
  //
  _LowLevelMountIfRequired(pInst);
  FS_MEMSET(pBlockInfo, 0, sizeof(FS_NAND_BLOCK_INFO));
  if (pInst) {
    unsigned     iSector;
    U32          SectorIndexSrc;
    const char * sType;
    U32          EraseCnt;
    U32          lbi;
    U16          NumSectorsBlank;               // Sectors are not used yet.
    U16          NumSectorsValid;               // Sectors contain correct data.
    U16          NumSectorsInvalid;             // Sectors have been invalidated.
    U16          NumSectorsECCError;            // Sectors have incorrect ECC.
    U16          NumSectorsECCCorrectable;      // Sectors have correctable ECC error.
    U16          NumSectorsErrorInECC;
    U16          SectorsPerBlock;
    U8           BlockType;
    int          ErrStat;
    U8           Type;

    _ClearStaticSpareArea(pInst->BytesPerSector >> 5);
    NumSectorsBlank            = 0;
    NumSectorsValid            = 0;
    NumSectorsInvalid          = 0;
    NumSectorsECCError         = 0;
    NumSectorsECCCorrectable   = 0;
    NumSectorsErrorInECC       = 0;
    iSector                    = 0;
    SectorsPerBlock = 1 << pInst->SPB_Shift;
    SectorIndexSrc  = _BlockIndex2SectorIndex(pInst, PhyBlockIndex);
    Type  = NAND_BLOCK_TYPE_UNKNOWN;
    sType = "Unknown";
    if (_BlockIsBad(pInst, PhyBlockIndex)) {
      EraseCnt = 0;
      lbi      = 0;
      sType = "Bad block";
      Type       = NAND_BLOCK_TYPE_BAD;
    } else {
      _ReadSpareIntoStaticBuffer(pInst, SectorIndexSrc);
      BlockType = _pSpareAreaData[SPARE_OFF_DATA_STATUS] >> 4;
      EraseCnt = _LoadEraseCnt(pInst, _pSpareAreaData);
      lbi      = _LoadLBI(pInst);
      switch(BlockType) {
      case DATA_STAT_EMPTY:
        sType = "Empty block";
        Type  = NAND_BLOCK_TYPE_EMPTY;
        break;
      case DATA_STAT_WORK:
        sType = "Work block";
        Type  = NAND_BLOCK_TYPE_WORK;
        break;
      case DATA_STAT_VALID:
        sType = "Data block";
        Type  = NAND_BLOCK_TYPE_DATA;
        break;
      case DATA_STAT_INVALID:
        sType = "Block marked as not used anymore";
        Type  = NAND_BLOCK_TYPE_EMPTY;
        break;
      default:
        break;
      }
      //
      // The validity of the first sector in a work block should be checked differently.
      //
      if (BlockType == DATA_STAT_WORK) {
        WORK_BLOCK_DESC * pWorkBlock;

        //
        // Get the descriptor associated with the work block
        //
        pWorkBlock = _FindWorkBlock(pInst, lbi);
        if (pWorkBlock) {
          //
          // The sector is empty if it has never been written
          //
          if (_WB_SectorIsWritten(pWorkBlock, 0) == 0) {
            NumSectorsBlank++;
          } else {
            //
            // Check if it contains valid data
            //
            if ((_WB_ReadAssignment(pInst, pWorkBlock, 0) != 0) || _SectorDataIsInvalidated(pInst, SectorIndexSrc)) {
              NumSectorsInvalid++;
            } else {
              //
              // Now read the sector and verify the ECC
              //
              ErrStat = _ReadSectorWithECC(pInst, _pSectorBuffer, SectorIndexSrc);
              if (ErrStat == NAND_NO_ERROR) {   // Data is valid
                NumSectorsValid++;
              } else if (ErrStat == NAND_1BIT_CORRECTED) {
                NumSectorsECCCorrectable++;
              } else if (ErrStat == NAND_ERROR_IN_ECC) {
                NumSectorsErrorInECC++;
              } else if ((ErrStat == NAND_UNCORRECTABLE_ERROR) || (ErrStat == NAND_READ_ERROR)) {
                NumSectorsECCError++;
              }
            }
          }
        }
        iSector = 1;
      }
      for (; iSector < SectorsPerBlock; iSector++) {
        //
        // Check sector data against ECC in spare area
        //
        ErrStat = _ReadSectorWithECC(pInst, _pSectorBuffer, iSector + SectorIndexSrc);
        if (ErrStat == NAND_NO_ERROR) {
          if (_SectorDataIsInvalidatedFast(pInst, SectorIndexSrc)) {
            NumSectorsInvalid++;
          } else if (iSector != 0) {      // For sectors other than 0 we check the block relative sector index.
            if (_LoadBRSI(pInst) != BRSI_INVALID) {
              NumSectorsValid++;
            } else {
              NumSectorsInvalid++;
            }
          } else {
            NumSectorsValid++;       // On Data sectors we assume the sector is valid.
          }
        } else if (ErrStat == NAND_1BIT_CORRECTED) {
          NumSectorsECCCorrectable++; // Data have been corrected
        } else if (ErrStat == NAND_ERROR_IN_ECC) {
          NumSectorsErrorInECC++;     // Error in ECC, data still OK
        } else if ((ErrStat == NAND_UNCORRECTABLE_ERROR) || (ErrStat == NAND_READ_ERROR)) {
          NumSectorsECCError++;       // Error not correctable by ECC or NAND read error
        } else if (ErrStat < 0) {
          NumSectorsBlank++;          // Sector not used
        }
      }
    }
    pBlockInfo->sType                    = sType;
    pBlockInfo->Type                     = Type;
    pBlockInfo->EraseCnt                 = EraseCnt;
    pBlockInfo->lbi                      = lbi;
    pBlockInfo->NumSectorsBlank          = NumSectorsBlank;
    pBlockInfo->NumSectorsECCCorrectable = NumSectorsECCCorrectable;
    pBlockInfo->NumSectorsErrorInECC     = NumSectorsErrorInECC;
    pBlockInfo->NumSectorsECCError       = NumSectorsECCError;
    pBlockInfo->NumSectorsInvalid        = NumSectorsInvalid;
    pBlockInfo->NumSectorsValid          = NumSectorsValid;
  }
}

/*********************************************************************
*
*       FS_NAND_SetOnFatalErrorCallback
*
*  Function description:
*    Registers a function to be called by the driver when a fatal error happens.
*
*/
void FS_NAND_SetOnFatalErrorCallback(FS_NAND_ON_FATAL_ERROR_CALLBACK * pfOnFatalError) {
  _pfOnFatalError = pfOnFatalError;
}

/*********************************************************************
*
*       FS_NAND_Validate
*
*  Function description:
*    Performs unit tests on some of the internal routines
*
*/
void FS_NAND_Validate(void) {
  int r;
  // 00000000 11111100 22221111 33222222 33333333 44444444 55555544 66665555 77666666
  // 76543210 54321098 32109876 10987654 98765432 76543210 54321098 32109876 10987654
  //
  // 01010011 01000101 01000111 01000111 01000101 01010010 11111111 11111111 01111111 
  U8  aData[] = {0x53, 0x45, 0x47, 0x47, 0x45, 0x52, 0xFF, 0xFF, 0x7F};   

  r = _Find0BitInByte(0xFF, 0, 7, 0);
  while (!(r == -1));
  r = _Find0BitInByte(0xFE, 0, 0, 0);
  while (!(r == 0));
  r = _Find0BitInByte(0x7F, 7, 7, 0);
  while (!(r == 7));
  r = _Find0BitInByte(0xEF, 2, 4, 0);
  while (!(r == 4));
  r = _Find0BitInByte(0xF7, 3, 4, 0);
  while (!(r == 3));
  r = _Find0BitInByte(0xF1, 0, 1, 0);
  while (!(r == 1));
  r = _Find0BitInByte(0xF3, 1, 6, 0);
  while (!(r == 2));
  r = _Find0BitInByte(0xF7, 3 ,3 ,5);
  while (!(r == (3 + (5 * 8))));

  r = _Find0BitInArray(aData, 3, 3);
  while (!(r == 3));
  r = _Find0BitInArray(aData, 7, 16);
  while (!(r == 7));
  r = _Find0BitInArray(aData, 16, 18);
  while (!(r == -1));
  r = _Find0BitInArray(aData, 44, 47);
  while (!(r == 45));
  r = _Find0BitInArray(aData, 5, 47);
  while (!(r == 5));
  r = _Find0BitInArray(aData, 55, 71);
  while (!(r == 71));
}

/*************************** End of file ****************************/
