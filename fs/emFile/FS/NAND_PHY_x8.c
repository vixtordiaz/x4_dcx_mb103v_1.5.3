/*********************************************************************
*                SEGGER MICROCONTROLLER GmbH & Co. KG                *
*        Solutions for real time microcontroller applications        *
**********************************************************************
*                                                                    *
*        (c) 2003-2012     SEGGER Microcontroller GmbH & Co KG       *
*                                                                    *
*        Internet: www.segger.com    Support:  support@segger.com    *
*                                                                    *
**********************************************************************

**** emFile file system for embedded applications ****
emFile is protected by international copyright laws. Knowledge of the
source code may not be used to write a similar product. This file may
only be used in accordance with a license and should not be re-
distributed in any way. We appreciate your understanding and fairness.
----------------------------------------------------------------------
File        : NAND_PHY_x8.c
Purpose     : General 8-bit access NAND flashes physical
---------------------------END-OF-HEADER------------------------------
*/

/*********************************************************************
*
*             #include Section
*
**********************************************************************
*/

#include "FS_Int.h"
#include "FS_CLib.h"
#include "FS_Debug.h"
#include "NAND_X_HW.h"

/*********************************************************************
*
*       Define configurable
*
**********************************************************************
*/
#ifdef FS_NAND_MAXUNIT
  #define NUM_UNITS   FS_NAND_MAXUNIT
#else
  #define NUM_UNITS   4
#endif

/*********************************************************************
*
*       Defines, not configurable
*
**********************************************************************
*/

/*********************************************************************
*
*       Const data
*
**********************************************************************
*/

/*********************************************************************
*
*       _apPhyType
*
*  This is the list of physical layers which are checked. The first one in the list which works is used.
*/
static const FS_NAND_PHY_TYPE * _apPhyTypeList_x8[] = {
  &FS_NAND_PHY_512x8,
  &FS_NAND_PHY_2048x8,
  &FS_NAND_PHY_4096x8,
  &FS_NAND_PHY_ONFI,
  (const FS_NAND_PHY_TYPE *)NULL
};

static const FS_NAND_PHY_TYPE * _apPhyTypeList_x[] = {
  &FS_NAND_PHY_512x8,
  &FS_NAND_PHY_2048x8,
  &FS_NAND_PHY_2048x16,
  &FS_NAND_PHY_4096x8,
  &FS_NAND_PHY_ONFI,
  (const FS_NAND_PHY_TYPE *)NULL
};

/*********************************************************************
*
*       Static data
*
**********************************************************************
*/
static const FS_NAND_PHY_TYPE  * _apPhyType[NUM_UNITS];

/*********************************************************************
*
*       Static code
*
**********************************************************************
*/

/*********************************************************************
*
*       _Read
*/
static int _Read(U8 Unit, U32 PageNo, void * pData, unsigned Off, unsigned NumBytes) {
  int r;
  const FS_NAND_PHY_TYPE * pPhyType;

  r = -1;     // Error
  pPhyType = _apPhyType[Unit];
  if (pPhyType) {
    r = pPhyType->pfRead(Unit, PageNo, pData, Off, NumBytes);
  }
  return r;
}


/*********************************************************************
*
*       _ReadEx
*/
static int _ReadEx(U8 Unit, U32 PageNo, void * pBuffer0, unsigned Off0, unsigned NumBytes0, void * pBuffer1, unsigned Off1, unsigned NumBytes1) {
  int r;
  const FS_NAND_PHY_TYPE * pPhyType;

  r = -1;     // Error
  pPhyType = _apPhyType[Unit];
  if (pPhyType) {
    r = pPhyType->pfReadEx(Unit, PageNo, pBuffer0, Off0, NumBytes0, pBuffer1, Off1, NumBytes1);
  }
  return r;
}

/*********************************************************************
*
*       _Write
*/
static int _Write(U8 Unit, U32 PageNo, const void * pData, unsigned Off, unsigned NumBytes) {
  int r;
  const FS_NAND_PHY_TYPE * pPhyType;

  r = -1;     // Error
  pPhyType = _apPhyType[Unit];
  if (pPhyType) {
    r = pPhyType->pfWrite(Unit, PageNo, pData, Off, NumBytes);
  }
  return r;
}


/*********************************************************************
*
*       _WriteEx
*/
static int _WriteEx(U8 Unit, U32 PageNo, const void * pBuffer0, unsigned Off0, unsigned NumBytes0, const void * pBuffer1, unsigned Off1, unsigned NumBytes1) {
  int r;
  const FS_NAND_PHY_TYPE * pPhyType;

  r = -1;     // Error
  pPhyType = _apPhyType[Unit];
  if (pPhyType) {
    r = pPhyType->pfWriteEx(Unit, PageNo, pBuffer0, Off0, NumBytes0, pBuffer1, Off1, NumBytes1);
  }
  return r;
}

/*********************************************************************
*
*       _EraseBlock
*/
static int _EraseBlock(U8 Unit, U32 BlockNo) {
  int r;
  const FS_NAND_PHY_TYPE * pPhyType;

  r = -1;     // Error
  pPhyType = _apPhyType[Unit];
  if (pPhyType) {
    r = pPhyType->pfEraseBlock(Unit, BlockNo);
  }
  return r;
}

/*********************************************************************
*
*       _IsWP
*/
static int _IsWP(U8 Unit) {
  int r;
  const FS_NAND_PHY_TYPE * pPhyType;

  r = -1;     // Error
  pPhyType = _apPhyType[Unit];
  if (pPhyType) {
    r = pPhyType->pfIsWP(Unit);
  }
  return r;
}

/*********************************************************************
*
*       _InitGetDeviceInfo
*/
static int _InitGetDeviceInfo(U8 Unit, FS_NAND_DEVICE_INFO * pDevInfo, const FS_NAND_PHY_TYPE ** ppPhyType) {
  const FS_NAND_PHY_TYPE * pPhyType;

  do {
    pPhyType = *ppPhyType++;
    if (pPhyType == NULL) {
      break;           //  Error
    }
    if (pPhyType->pfInitGetDeviceInfo(Unit, pDevInfo) == 0) {
      _apPhyType[Unit] = pPhyType;
      return 0;       // Success! Device is recognized by this physical layer.
    }
  } while (1);
  FS_DEBUG_ERROROUT((FS_MTYPE_DRIVER, "NAND_PHY_x8: Can not identify NAND flash"));
  return 1;                 // Error
}

/*********************************************************************
*
*       _InitGetDeviceInfo_x
*/
static int _InitGetDeviceInfo_x(U8 Unit, FS_NAND_DEVICE_INFO * pDevInfo) {
  return _InitGetDeviceInfo(Unit, pDevInfo, &_apPhyTypeList_x[0]);
}

/*********************************************************************
*
*       _InitGetDeviceInfo_x8
*/
static int _InitGetDeviceInfo_x8(U8 Unit, FS_NAND_DEVICE_INFO * pDevInfo) {
  return _InitGetDeviceInfo(Unit, pDevInfo, &_apPhyTypeList_x8[0]);
}

/*********************************************************************
*
*       Public const
*
**********************************************************************
*/
const FS_NAND_PHY_TYPE FS_NAND_PHY_x8 = {
  _EraseBlock,
  _InitGetDeviceInfo_x8,
  _IsWP,
  _Read,
  _ReadEx,
  _Write,
  _WriteEx
};

const FS_NAND_PHY_TYPE FS_NAND_PHY_x = {
  _EraseBlock,
  _InitGetDeviceInfo_x,
  _IsWP,
  _Read,
  _ReadEx,
  _Write,
  _WriteEx
};


/*************************** End of file ****************************/
