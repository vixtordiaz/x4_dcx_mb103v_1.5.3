/*********************************************************************
*                SEGGER MICROCONTROLLER GmbH & Co. KG                *
*        Solutions for real time microcontroller applications        *
**********************************************************************
*                                                                    *
*        (c) 2003-2012     SEGGER Microcontroller GmbH & Co KG       *
*                                                                    *
*        Internet: www.segger.com    Support:  support@segger.com    *
*                                                                    *
**********************************************************************

**** emFile file system for embedded applications ****
emFile is protected by international copyright laws. Knowledge of the
source code may not be used to write a similar product. This file may
only be used in accordance with a license and should not be re-
distributed in any way. We appreciate your understanding and fairness.
----------------------------------------------------------------------
File        : FS_Journal.c
Purpose     : Implementation of Journal for embedded file system
---------------------------END-OF-HEADER------------------------------

Sector usage of journal:
      -------------------------------------------
      0                          Status
      1 - n                      Copy-list
      n+1 - m-2                  Raw data to be transferred
      m-1                        Info sector

      where n: Number of sectors used for Copy-list
            m: Total number of sectors in journal
      -------------------------------------------

Management organisation
      -------------------------------------------
      0x00 - 0x1F                Header: Number of data sectors etc
      0x20 - ....                Assignment info, 16 bytes per sector
      -------------------------------------------

*/

/*********************************************************************
*
*             #include Section
*
**********************************************************************
*/

#include <stdlib.h>

#include "FS_Int.h"

/*********************************************************************
*
*       Defines, fixed
*
**********************************************************************
*/
#define VERSION                    10000
#define SIZEOF_SECTOR_LIST_ENTRY      16
#define JOURNAL_INDEX_INVALID      (0xFFFFFFFFUL)

#define INFO_SECTOR_TAG            "Journal info\0\0\0"
#define MAN_SECTOR_TAG             "Journal status\0"
#define SIZEOF_INFO_SECTOR_TAG     16
#define SIZEOF_MAN_SECTOR_TAG      16

/*********************************************************************
*
*       Defines, configurable
*
**********************************************************************
*/
/*********************************************************************
*
*       Offsets in info sector
*/
#define OFF_INFO_VERSION             (0x10)
#define OFF_INFO_NUM_TOTAL_SECTORS   (0x20)

/*********************************************************************
*
*       Offsets in management info
*/
#define OFF_MAN_SECTOR_CNT           (0x10)
#define OFF_MAN_SECTOR_LIST          (0x20)

/*********************************************************************
*
*       Type definitions
*
**********************************************************************
*/

/*********************************************************************
*
*       INST structure (and STATUS sub-structure)
*
*  This is the central data structure for the entire driver.
*  It contains data items of one instance of the driver.
*/

typedef struct {
  int OpenCnt;
  U32 NumSectorsTotal;        // Total number of sectors allocated to journal
  U32 NumSectorsData;         // Number of sectors available for data
  U32 BytesPerSector;         // Number of bytes per sector. Typically 512.
  U32 PBIInfoSector;          // Physical sector index of last sector of journal. The contents of this sector never change.
  U32 PBIStatusSector;        // Physical sector index of first sector of journal. Used to store status information.
  U32 PBIStartSectorList;     // Physical sector index of first sector of the sector list.
  U32 PBIFirstDataSector;     // Physical sector index of first sector used to store user data ("payload").
  U32 SectorCnt;              // Count of sectors currently in journal
  U8  IsPresent;
} STATUS;

typedef struct {
  STATUS Status;
  U32    NumEntriesJ2P;       // Number of entries for which memory has been allocated.
  U32 *  apJ2P;               // Journal to physical table. Input: journal index (file system view). Output: Physical index (hardware/driver view)
  FS_VOLUME * pVolume;
} JOURNAL_INST;


/*********************************************************************
*
*       Static data
*
**********************************************************************
*/
static JOURNAL_INST  * _apInstJournal[FS_NUM_VOLUMES];

/*********************************************************************
*
*       Static code
*
**********************************************************************
*/

/*********************************************************************
*
*       _Volume2Inst
*/
static JOURNAL_INST * _Volume2Inst(FS_VOLUME * pVolume) {
  int Unit;
  JOURNAL_INST * pInst;
  FS_VOLUME * pVol;

  pVol = &FS_Global.FirstVolume;
  for (Unit = 0; Unit < FS_Global.NumVolumes; Unit++) {
    if (pVolume == pVol) {
      break;
    }
    pVol = pVol->pNext;
  }
  if (_apInstJournal[Unit] == NULL) {
    _apInstJournal[Unit] = (JOURNAL_INST *)FS_AllocZeroed(sizeof(JOURNAL_INST));
    pInst = _apInstJournal[Unit];
    pInst->pVolume = pVolume;
    pVolume->Partition.Device.Data.JournalUnit = Unit;
  }
  pInst = _apInstJournal[Unit];
  if (pInst && pInst->pVolume == 0) {       //-Q-
    pInst->pVolume = pVolume;
  }
  return pInst;
}

/*********************************************************************
*
*       _Volume2Inst2Free
*
*   Quyen Leba
*/
static void _Volume2Inst2Free(FS_VOLUME * pVolume) {
  int Unit;
  FS_VOLUME * pVol;

  pVol = &FS_Global.FirstVolume;
  for (Unit = 0; Unit < FS_Global.NumVolumes; Unit++) {
    if (pVolume == pVol) {
      break;
    }
    pVol = pVol->pNext;
  }
  if (_apInstJournal[Unit]) {
     if (_apInstJournal[Unit]->apJ2P)
     {
        FS_Free(_apInstJournal[Unit]->apJ2P);
        _apInstJournal[Unit]->apJ2P = 0;
     }
      FS_Free(_apInstJournal[Unit]);
    _apInstJournal[Unit] = 0;
  }
}

/*********************************************************************
*
*       _ReadOneSector
*/
static int _ReadOneSector(JOURNAL_INST * pInst, U32 SectorIndex, U8 * pData) {
  FS_DEVICE * pDevice;
  U8          Unit;

  pDevice      = &pInst->pVolume->Partition.Device;
  Unit         = pDevice->Data.Unit;
  return pDevice->pType->pfRead(Unit, SectorIndex, pData, 1);
}

/*********************************************************************
*
*       _WriteOneSector
*/
static int _WriteOneSector(JOURNAL_INST * pInst, U32 SectorIndex, const U8 * pData) {
  FS_DEVICE * pDevice;
  U8          Unit;

  pDevice = &pInst->pVolume->Partition.Device;
  Unit = pDevice->Data.Unit;
  return pDevice->pType->pfWrite(Unit, SectorIndex, pData, 1, 0);
}

/*********************************************************************
*
*       _InitStatus
*
*  Description:
*    Initialize the status data.
*    This makes sure the all routine that depend on this status
*    are working with correct status information.
*
*/
static void _InitStatus(JOURNAL_INST * pInst) {
  STATUS * pStatus;

  pStatus = &pInst->Status;
  //
  //  Invalidate all information with 0.
  //
  FS_MEMSET(pStatus, 0, sizeof(STATUS));
}

/*********************************************************************
*
*       _InitInst
*/
static void _InitInst(JOURNAL_INST * pInst, U32 FirstSector, U32 NumSectors) {
  U32    NumBytes;
  U32    NumSectorsData;
  U32    NumSectorsManagement;
  U32    FirstSectorAfterJournal;
  U32    BytesPerSector;

  _InitStatus(pInst);
  FirstSectorAfterJournal          = FirstSector + NumSectors;
  pInst->Status.PBIInfoSector      = FirstSectorAfterJournal - 1;               // Info sector. Contents never change
  pInst->Status.PBIStatusSector    = FirstSectorAfterJournal - NumSectors;      // Status sector. First sector in journal.
  pInst->Status.PBIStartSectorList = pInst->Status.PBIStatusSector + 1;                // Start of sector list
  //
  // Compute the number of sectors which can be used to store data
  //
  BytesPerSector            = pInst->pVolume->FSInfo.Info.BytesPerSector;
  NumBytes                  = (NumSectors - 3) * BytesPerSector;                            // Total number of bytes for data & management. 3 sectors subtract for info, status and head of sector list
  NumSectorsData            = NumBytes / (BytesPerSector + SIZEOF_SECTOR_LIST_ENTRY);       // This computation is a bit simplified and may waste one sector in some cases
  NumSectorsManagement      = FS__DivideU32Up(NumSectorsData * SIZEOF_SECTOR_LIST_ENTRY, BytesPerSector);
  //
  // Store information
  //
  pInst->Status.BytesPerSector     = BytesPerSector;
  pInst->Status.NumSectorsData     = NumSectorsData;
  pInst->Status.PBIFirstDataSector = pInst->Status.PBIStartSectorList + NumSectorsManagement;      // Data sectors follow sector list
  //
  // Allocate memory for lookup table if necessary
  //
  NumBytes = NumSectorsData * sizeof(U32);
  //
  //  Initialize the journal assignment (J2P) table
  // 
  if (pInst->NumEntriesJ2P < NumSectorsData) {
    pInst->NumEntriesJ2P = NumSectorsData;
    pInst->apJ2P = (U32 *)FS_AllocZeroed(sizeof(U32) * NumSectorsData);
  }
}

/*********************************************************************
*
*       _FindSector
*
*  Function description
*    Locate a logical sector (as seen by the file system) in the journal.
*
*  Return value
*    If found:   Position in journal
*    else    :   JOURNAL_INDEX_INVALID
*
*/
static U32 _FindSector(JOURNAL_INST * pInst, U32 lsi) {
  U32 u;
  U32 SectorCnt;

  SectorCnt = pInst->Status.SectorCnt;
  for (u = 0; u < SectorCnt; u++) {
    if (*(pInst->apJ2P + u) == lsi) {
      return u;
    }
  }
  return JOURNAL_INDEX_INVALID;
}

/*********************************************************************
*
*       _CopyData
*
*  Function description:
*    Copies data from journal to real position in FS.
*/
static int _CopyData(JOURNAL_INST * pInst, U8 * pData) {
  U32 SectorCnt;
  U32 lsiDest;
  U32 u;
  U32 lsiSrc;
  int r;

  SectorCnt = pInst->Status.SectorCnt;
  for (u = 0; u < SectorCnt; u++) {
    //
    // Read from journal
    //
    lsiSrc = pInst->Status.PBIFirstDataSector + u;
    r = _ReadOneSector(pInst, lsiSrc, pData);
    if (r) {
      FS_DEBUG_ERROROUT((FS_MTYPE_JOURNAL, "FS_Journal: Read error while playing journal.\n"));
      return -1;    // Error
    }
    //
    // Write to device
    //
    lsiDest = *(pInst->apJ2P + u);
    r = _WriteOneSector(pInst, lsiDest, pData);
    if (r) {
      FS_DEBUG_ERROROUT((FS_MTYPE_JOURNAL, "FS_Journal: Error playing journal.\n"));
      return -1;    // Error
    }
  }
  return 0;
}

/*********************************************************************
*
*       _ClearJournal
*
*  Function description:
*    Clear the journal. This means resetting the sector count and updating (= writing to device) management info.
*/
static void _ClearJournal(JOURNAL_INST * pInst, U8 * pData) {
  //
  // Write Status sector
  //
  FS_MEMSET(pData, 0, pInst->Status.BytesPerSector);
  FS_MEMCPY(pData, MAN_SECTOR_TAG, 16);
  _WriteOneSector(pInst, pInst->Status.PBIStatusSector, pData);
  FS_MEMSET(pInst->apJ2P, 0, sizeof(U32) * pInst->Status.SectorCnt);
  pInst->Status.SectorCnt = 0;
}

/*********************************************************************
*
*       _CleanJournal
*
*  Function description:
*    This routines copies the data in the journal to the "real" destination and cleans the journal in the foloowing steps:
*    - Write journal management info
*    - Copy data
*    - Clear journal (rewriting management info)
*/
static int _CleanJournal(JOURNAL_INST * pInst) {
  U32    u;
  U32    SectorCnt;
  U32    lsi;
  U8   * pData;
  FS_SB  sb;
  U32    BytesPerSector;
  U32    Off;
  U32    BPSMask;
  int    r = 0;

  SectorCnt = pInst->Status.SectorCnt;
  BytesPerSector = pInst->Status.BytesPerSector;
  //
  // Write out the journal only if there are any data written to it.
  //
  if (SectorCnt) {
    FS__SB_Create(&sb, &pInst->pVolume->Partition);
    pData = sb.pBuffer;
    FS_MEMSET(pData, 0, BytesPerSector);
    //
    // Write Copy-list
    //
    BPSMask = (BytesPerSector -1);
    for (u = 0; u < SectorCnt; u++) {
      Off = u * SIZEOF_SECTOR_LIST_ENTRY;
      lsi = *(pInst->apJ2P + u);
      FS_StoreU32LE(pData + (Off & BPSMask), lsi);
      //
      // Write sector if it is either last entry of copy list or in last entry of this sector
      //
      if ((u == SectorCnt - 1) || ((Off & BPSMask) + SIZEOF_SECTOR_LIST_ENTRY == BytesPerSector)) {
        U32 SectorIndex;

        SectorIndex = Off / BytesPerSector + pInst->Status.PBIStartSectorList;
        if (SectorIndex == pInst->Status.PBIFirstDataSector) {
          FS_DEBUG_ERROROUT((FS_MTYPE_JOURNAL, "FS_Journal: Fatal error: Writing management information into the data area.\n"));
        }
        _WriteOneSector(pInst, SectorIndex, pData);
        FS_MEMSET(pData, 0, BytesPerSector);
      }
    }
    //
    // Write Status sector
    //
    FS_MEMSET(pData, 0, BytesPerSector);
    FS_MEMCPY(sb.pBuffer, MAN_SECTOR_TAG, SIZEOF_MAN_SECTOR_TAG);
    FS_StoreU32LE(pData + OFF_MAN_SECTOR_CNT, SectorCnt);
    _WriteOneSector(pInst, pInst->Status.PBIStatusSector, pData);
    //
    // Copy data from journal to its real destination
    //
    _CopyData(pInst, pData);
    _ClearJournal(pInst, pData);
    //
    // Cleanup
    //
    FS__SB_Delete(&sb);
  }
  return r;
}

/*********************************************************************
*
*       _Write
*
*  Description:
*    FS driver function. Writes one or more logical sectors to storage device.
*
*  Return value:
*      0                       - Data successfully written.
*    !=0                       - An error has occurred.
*
*/
static int _Write(JOURNAL_INST * pInst, U32 LogSec, const void* pData, U32 NumSectors, U8 RepeatSame) {
  U32 psi;
  U32 JournalIndex;
  int r;

  while (NumSectors) {
    //
    // Try to locate sector in journal
    //
    JournalIndex = _FindSector(pInst, LogSec);
    if (JournalIndex != JOURNAL_INDEX_INVALID) {
      psi = JournalIndex + pInst->Status.PBIFirstDataSector;
    } else {
      if (pInst->Status.SectorCnt == pInst->Status.NumSectorsData) {
        FS_DEBUG_WARN((FS_MTYPE_JOURNAL, "Journal is full. Flushing."));      // Note: Journal can no longer guarantee that operations are atomic!
        _CleanJournal(pInst);
      }
      *(pInst->apJ2P + pInst->Status.SectorCnt) = LogSec;
      psi = pInst->Status.SectorCnt + pInst->Status.PBIFirstDataSector;
      pInst->Status.SectorCnt++;
    }
    //
    // Write to device
    //
    r = _WriteOneSector(pInst, psi, (U8 *)pData);
    if (r) {
      return -1;    // Error
    }
    NumSectors--;
    LogSec++;
    if (RepeatSame == 0) {
      pData = (const void *)(pInst->Status.BytesPerSector + (const U8*)pData);    // pData += BytesPerSector
    }
  }
  return 0;
}

/*********************************************************************
*
*       _Read
*
*  Description:
*    FS driver function. Reads one logical sector to given buffer space
*
*  Parameters:
*    DevIndex                  - Device index number.
*    Logical Sector            - Sector number
*    Buffer                    - Pointer to data buffer
*
*
*  Return value:
*    ==0                       - Device okay and ready for operation.
*    <0                        - An error has occurred.
*
*/
static int _Read(JOURNAL_INST * pInst, U32 LogSec, void * pData, U32 NumSectors) {
  U32 psi;
  U32 JournalIndex;
  int r;

  while (NumSectors) {
    psi = LogSec;
    //
    // Try to locate sector in journal
    //
    JournalIndex = _FindSector(pInst, LogSec);
    if (JournalIndex != JOURNAL_INDEX_INVALID) {
      psi = JournalIndex + pInst->Status.PBIFirstDataSector;
    }
    //
    // Read from device
    //
    r = _ReadOneSector(pInst, psi, (U8 *)pData);
    if (r) {
      return -1;    // Error
    }
    NumSectors--;
    LogSec++;
    pData = (void *)(pInst->Status.BytesPerSector + (U8*)pData);    // pData += BytesPerSector
  }
  return 0;
}

/*********************************************************************
*
*       _Mount
*
*  Function description:
*    Mounts the journal layer, replaying the journal.
*
*  Return value:
*    == 0             - O.K.
*    != 0             - Error
*/
static int _MountEx(FS_VOLUME * pVolume, U32 LastSectorInFS) {
  FS_SB   sb;
  int     r;
  U8    * pData;
  JOURNAL_INST * pInst;
  U32     FirstSector;
  U32     NumSectors;
  U32     SectorCnt;
  U32     lsi;
  U32     u;
  U32     Off;
  U32     BytesPerSector;
  U32     SectorIndex;
  U32     StartSector;

  pInst = _Volume2Inst(pVolume);
  FS__SB_Create(&sb, &pVolume->Partition);
  pData   = sb.pBuffer;
  r       = 1;
  //
  // Compute the physical sector index of the last sector.
  //
  StartSector     = pVolume->Partition.StartSector;
  LastSectorInFS += StartSector;
  //
  // Read info sector
  //
  r = _ReadOneSector(pInst, LastSectorInFS, pData);   // Read the info sector (last sector of partition)
  if (r) {
    FS_DEBUG_WARN((FS_MTYPE_JOURNAL, "Read of journal info sector failed"));
    goto CleanUp;
  }
  //
  // Check sector for validity
  //
  if (FS_MEMCMP(pData, INFO_SECTOR_TAG, SIZEOF_INFO_SECTOR_TAG)) {
    FS_DEBUG_WARN((FS_MTYPE_JOURNAL, "No journal found"));
    goto CleanUp;
  }
  if (FS_LoadU32LE(pData + OFF_INFO_VERSION) != VERSION) {
    FS_DEBUG_WARN((FS_MTYPE_JOURNAL, "Journal file version does not match"));
    goto CleanUp;
  }
  //
  // Retrieve static information from info sector. This info is written when the journal is created and never changes.
  //
  NumSectors  = FS_LoadU32LE(pData + OFF_INFO_NUM_TOTAL_SECTORS);
  FirstSector = LastSectorInFS - NumSectors + 1;
  _InitInst(pInst, FirstSector, NumSectors);
  BytesPerSector = pInst->Status.BytesPerSector;
  //
  //  Read status sector, check for validity
  //
  r = _ReadOneSector(pInst, pInst->Status.PBIStatusSector, pData);      // Read the status sector
  if (r) {
    FS_DEBUG_WARN((FS_MTYPE_JOURNAL, "Read of journal status sector failed"));
    goto CleanUp;
  }
  if (FS_MEMCMP(pData, MAN_SECTOR_TAG, SIZEOF_MAN_SECTOR_TAG)) {
    FS_DEBUG_WARN((FS_MTYPE_JOURNAL, "Management sector invalid"));
    goto CleanUp;
  }
  //
  //  Check if any entries are in the journal
  //
  SectorCnt = FS_LoadU32LE(pData + OFF_MAN_SECTOR_CNT);
  if (SectorCnt) {
    //
    // Read management info
    //
    for (u = 0; u < SectorCnt; u++) {
      Off = u * SIZEOF_SECTOR_LIST_ENTRY;
      if ((Off & (BytesPerSector - 1)) == 0) {
        SectorIndex = Off / BytesPerSector + pInst->Status.PBIStartSectorList;
        r = _ReadOneSector(pInst, SectorIndex, pData);
        if (r) {
          FS_DEBUG_WARN((FS_MTYPE_JOURNAL, "Management sector information could not be read."));
          goto CleanUp;
        }
      }
      Off &= BytesPerSector - 1;
      lsi = FS_LoadU32LE(pData + Off);
      *(pInst->apJ2P + u) = lsi;
    }
    pInst->Status.SectorCnt = SectorCnt;
    //
    // Copy data from journal to its real destination
    //
    _CopyData(pInst, pData);
    _ClearJournal(pInst, pData);
  }
  // Journal mounted successfully
  r = 0;
  pInst->Status.IsPresent = 1;
  pVolume->Partition.Device.Data.JournalIsActive = 1;
CleanUp:
  //
  // Cleanup
  //
  FS__SB_Delete(&sb);
  return r;
}

/*********************************************************************
*
*       Internal Public code
*
**********************************************************************
*/

/*********************************************************************
*
*       FS__JOURNAL_Create
*
*  Function description:
*    Creates the journal
*
*  Return value:
*    == 0             - O.K.
*    != 0             - Error
*/
int FS__JOURNAL_Create(FS_VOLUME * pVolume, U32 FirstSector, U32 NumSectors) {
  FS_SB  sb;
  U8 *   pData;
  JOURNAL_INST * pInst;
  U32    BytesPerSector;
  U32    StartSector;

  pInst = _Volume2Inst(pVolume);
  StartSector  = pVolume->Partition.StartSector;
  FirstSector += StartSector;
  _InitInst(pInst, FirstSector, NumSectors);
  BytesPerSector = pInst->Status.BytesPerSector;
  //
  // Prepare and write Info sector
  //
  FS__SB_Create(&sb, &pVolume->Partition);
  pData = sb.pBuffer;
  FS_MEMSET(pData, 1, BytesPerSector);
  FS_MEMCPY(sb.pBuffer, INFO_SECTOR_TAG, SIZEOF_INFO_SECTOR_TAG);
  FS_StoreU32LE(pData + OFF_INFO_VERSION,           VERSION);
  FS_StoreU32LE(pData + OFF_INFO_NUM_TOTAL_SECTORS, NumSectors);
  _WriteOneSector(pInst, pInst->Status.PBIInfoSector, pData);
  //
  // Clear journal
  //
  _ClearJournal(pInst, pData);
  pInst->Status.IsPresent = 1;
  pVolume->Partition.Device.Data.JournalIsActive = 1;
  //
  // Cleanup
  //
  FS__SB_Delete(&sb);
  return 0;
}

/*********************************************************************
*
*       FS__JOURNAL_Mount
*
*  Function description:
*    Mounts the journal layer, replaying the journal.
*
*  Return value:
*    == 0             - O.K.
*    != 0             - Error
*/
int FS__JOURNAL_Mount(FS_VOLUME * pVolume) {
  int r = 1;
  if (pVolume->IsMounted == FS_MOUNT_RW) {
    if (FS_OPEN_JOURNAL_FILE(pVolume) == 0) {
      U32 LastSector;
      LastSector = FS_GET_INDEX_OF_LAST_SECTOR(pVolume);
      r = _MountEx(pVolume, LastSector);
    }
  }
  return r;
}


/*********************************************************************
*
*       FS__JOURNAL_Begin
*
*  Function description:
*    Opens the journal. This means all relevant data is written to the journal,
*    instead of the "real destination".
*
*  Return value:
*    == 0             - O.K.
*    != 0             - Error
*/
int FS__JOURNAL_Begin(FS_VOLUME * pVolume) {
  JOURNAL_INST * pInst;

  pInst = _Volume2Inst(pVolume);
  return ++pInst->Status.OpenCnt;
}

/*********************************************************************
*
*       FS__JOURNAL_End
*
*  Function description:
*    Closes the journal.
*    This means all relevant data is written to the journal,
*    instead of the "real destination".
*
*  Return value:
*    == 0             - O.K.
*    != 0             - Error
*/
int FS__JOURNAL_End(FS_VOLUME * pVolume) {
  JOURNAL_INST * pInst;

  pInst = _Volume2Inst(pVolume);
  if (--pInst->Status.OpenCnt == 0) {
    if (pInst->Status.IsPresent) {
      // Replay
      _CleanJournal(pInst);
    }
  }
  return pInst->Status.OpenCnt;
}


/*********************************************************************
*
*       FS__JOURNAL_Clean
*
*  Function description:
*    Closes the journal.
*    This means all relevant data is written to the journal,
*    instead of the "real destination".
*
*  Return value:
*    == 0             - O.K.
*    != 0             - Error
*/
int FS__JOURNAL_Clean(FS_VOLUME * pVolume) {
  JOURNAL_INST * pInst;

  pInst = _Volume2Inst(pVolume);
  pInst->Status.OpenCnt = 0;
  if (pInst->Status.IsPresent) {
    // Replay
    _CleanJournal(pInst);
  }
  return pInst->Status.OpenCnt;
}

/*********************************************************************
*
*       FS__JOURNAL_Delete
*
*  Function description:
*    Deletes the journal.
*    Typically called before formatting a medium
*
*  Return value:
*    == 0             - O.K.
*    != 0             - Error
*/
void FS__JOURNAL_Delete(FS_VOLUME * pVolume, U32 LastSectorInFS) {
  FS_SB  sb;
  U8    * pData;
  int     r;
  U32     StartSector;
  JOURNAL_INST * pInst;

  pInst = _Volume2Inst(pVolume);
  FS__SB_Create(&sb, &pVolume->Partition);
  pData = sb.pBuffer;
  //
  // Compute the physical sector index of the last sector.
  //
  StartSector     = pVolume->Partition.StartSector;
  LastSectorInFS += StartSector;
  //
  // Overwrite info sector
  //
  *pData = 0;
  r = _WriteOneSector(pInst, LastSectorInFS, pData);    // Write the info sector (last sector of partition)
  if (r) {
    FS_DEBUG_ERROROUT((FS_MTYPE_JOURNAL, "FS_Journal: Failed to invalidate journal.\n"));
  }
  //
  // Update Inst structure
  //
  pInst->Status.SectorCnt = 0;
  pInst->Status.IsPresent = 0;
  pVolume->Partition.Device.Data.JournalIsActive = 0;
  FS__SB_Delete(&sb);
}

/*********************************************************************
*
*       FS__JOURNAL_Invalidate
*
*  Function description:
*    Invalidates the journal.
*    Typically called when formatting a medium to avoid replaying of the journal
*
*/
void FS__JOURNAL_Invalidate(FS_VOLUME * pVolume) {
  JOURNAL_INST * pInst;

  pInst = _Volume2Inst(pVolume);
  //
  //  Invalidate all status information in the instance structure.
  //
  _InitStatus(pInst);
   pVolume->Partition.Device.Data.JournalIsActive = 0;
}

/*********************************************************************
*
*       FS__JOURNAL_IsPresent
*
*  Function description:
*    Returns if a journal is present and active.
*
*/
int FS__JOURNAL_IsPresent(FS_VOLUME * pVolume) {
  JOURNAL_INST * pInst;

  pInst = _Volume2Inst(pVolume);
  return pInst->Status.IsPresent;
}

/*********************************************************************
*
*       FS__JOURNAL_GetNumFreeSectors
*
*  Function description
*    Returns
*
*/
int FS__JOURNAL_GetNumFreeSectors(FS_VOLUME * pVolume) {
  JOURNAL_INST * pInst;

  pInst = _Volume2Inst(pVolume);
  return pInst->Status.NumSectorsData - pInst->Status.SectorCnt;
}

/*********************************************************************
*
*       FS__JOURNAL_Read
*
*  Function description
*    Reads one or multiple sectors from journal
*
*/
int FS__JOURNAL_Read(const FS_DEVICE * pDevice, U32 SectorNo, void * pBuffer, U32 NumSectors) {
  JOURNAL_INST * pInst;

  pInst = _apInstJournal[pDevice->Data.JournalUnit];
  return _Read(pInst, SectorNo, pBuffer, NumSectors);
}

/*********************************************************************
*
*       FS__JOURNAL_Write
*
*  Function description
*    Write one or multiple sectors to journal
*
*/
int FS__JOURNAL_Write(const FS_DEVICE * pDevice, U32 SectorNo, const void * pBuffer, U32 NumSectors, U8 RepeatSame) {
  JOURNAL_INST * pInst;

  pInst = _apInstJournal[pDevice->Data.JournalUnit];
  return _Write(pInst, SectorNo, pBuffer, NumSectors, RepeatSame);
}

/*********************************************************************
*
*       FS__JOURNAL_GetOpenCnt
*
*   Function description
*     Returns the number of nested calls to FS_JOURNAL_Begin()/FS_JOURNAL_End() pair of functions.
*
*/
int FS__JOURNAL_GetOpenCnt(const FS_DEVICE * pDevice) {
  JOURNAL_INST * pInst;
  U8             Unit;
  int            OpenCnt;

  Unit    = pDevice->Data.JournalUnit;
  pInst   = _apInstJournal[Unit];
  OpenCnt = pInst->Status.OpenCnt;
  return OpenCnt;
}

/*********************************************************************
*
*       Public code
*
**********************************************************************
*/

/*********************************************************************
*
*       FS_JOURNAL_Create
*
*  Function description:
*    Creates the journal file.
*
*  Return value:
*    0    O.K., Successfully created
*    1    Journal file already present
*   <0    Error
*/
int FS_JOURNAL_Create(const char * sVolume, U32 NumBytes) {
  int  r;
  FS_VOLUME * pVolume;
  U32 FirstSector;
  U32 NumSectors;

  r = -1;          // Error - Failed to get volume information
  FS_LOCK();
  if (sVolume) {
    pVolume = FS__FindVolume(sVolume, NULL);
    if (pVolume) {
      if (FS__AutoMount(pVolume) == FS_MOUNT_RW)  {
        r = FS__JOURNAL_IsPresent(pVolume);
        if (r == 0) {
          FS_LOCK_DRIVER(&pVolume->Partition.Device);
          r = FS_CREATE_JOURNAL_FILE(pVolume, NumBytes, &FirstSector, &NumSectors);
          if (r == 0) {
            FS__JOURNAL_Create(pVolume,  FirstSector, NumSectors);
          }
          FS_UNLOCK_DRIVER(&pVolume->Partition.Device);
        }
      }
    }
  }
  FS_UNLOCK();
  return r;
}

/*********************************************************************
*
*       FS_JOURNAL_Delete
*
*  Quyen Leba
*/
void FS_JOURNAL_Delete(const char * sVolume) {
  FS_VOLUME * pVolume;
  U32 LastSector;

  FS_LOCK();
  pVolume = FS__FindVolume(sVolume, NULL);
  if (pVolume) {
    FS_LOCK_DRIVER(&pVolume->Partition.Device);
    LastSector = FS_GET_INDEX_OF_LAST_SECTOR(pVolume);
    if (LastSector < 0x800000) {
      FS_JOURNAL_DELETE(pVolume, LastSector);
    }
  }
  FS_UNLOCK();
}

/*********************************************************************
*
*       FS_JOURNAL_Begin
*/
void FS_JOURNAL_Begin(const char * sVolume) {
  FS_VOLUME  * pVolume;

  FS_LOCK();
  pVolume = FS__FindVolume(sVolume, NULL);
  FS_LOCK_DRIVER(&pVolume->Partition.Device);
  FS_JOURNAL_BEGIN(pVolume);
  FS_UNLOCK_DRIVER(&pVolume->Partition.Device);
  FS_UNLOCK();
}

/*********************************************************************
*
*       FS_JOURNAL_End
*/
void FS_JOURNAL_End(const char * sVolume) {
  FS_VOLUME  * pVolume;

  FS_LOCK();
  pVolume = FS__FindVolume(sVolume, NULL);
  FS_LOCK_DRIVER(&pVolume->Partition.Device);
  FS_JOURNAL_END(pVolume);
  FS_UNLOCK_DRIVER(&pVolume->Partition.Device);
  FS_UNLOCK();
}

#if FS_SUPPORT_DEINIT
/*********************************************************************
*
*       FS_JOURNAL_DeInit
*
*  Function description:
*    Deinitialize the Journal module.
*
*/
void FS__JOURNAL_DeInit(FS_VOLUME * pVolume) {
//  JOURNAL_INST * pInst;
//  pInst = _Volume2Inst(pVolume);  
//  FS_Free(pInst);
  _Volume2Inst2Free(pVolume);   //-Q-
}
#endif

/*************************** End of file ****************************/
