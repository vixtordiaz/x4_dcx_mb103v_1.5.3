/*********************************************************************
*                SEGGER MICROCONTROLLER GmbH & Co. KG                *
*        Solutions for real time microcontroller applications        *
**********************************************************************
*                                                                    *
*        (c) 2003-2012     SEGGER Microcontroller GmbH & Co KG       *
*                                                                    *
*        Internet: www.segger.com    Support:  support@segger.com    *
*                                                                    *
**********************************************************************

**** emFile file system for embedded applications ****
emFile is protected by international copyright laws. Knowledge of the
source code may not be used to write a similar product. This file may
only be used in accordance with a license and should not be re-
distributed in any way. We appreciate your understanding and fairness.
----------------------------------------------------------------------
File        : NAND_UNI_Drv.c
Purpose     : File system generic NAND driver for SLC and MLC NAND flashes.
              For more information on supported devices, refer to the user manual.
Literature  : [1] Samsung data sheet for K9XXG08UXM devices, specifically K9F2G08U0M, K9K4G08U1M. Similar information in other Samsung manuals.
                  \\fileserver\techinfo\Company\Samsung\NAND_Flash\Device\K9F2GxxU0M_2KPageSLC_R12.pdf
              [2] Micron data sheet for MT29F2G08AAD, MT29F2G16AAD, MT29F2G08ABD, MT29F2G16ABD devices
                  \\fileserver\techinfo\Company\Micron\NANDFlash\MT29F2G0_8AAD_16AAD_08ABD_16ABD.pdf
              [3] Micron presentations for Flash Memory Summit.
                  \\fileserver\techinfo\Company\Micron\NANDFlash\flash_mem_summit_08_khurram_nand.pdf
              [4] Micron application notes about bad block management, wear leveling and error correction codes.
              [5] \\fileserver\Techinfo\Company\Samsung\NAND_Flash\ecc_algorithm_for_web_512b.pdf
---------------------------END-OF-HEADER------------------------------

General info on the inner workings of this high level flash driver:

Supported NAND flashes
======================
  All the NAND flashes with a pages size >= 2KB with a sparea area >= 16 byte/512 byte data.

ECC and Error correction
========================
  The driver uses an ECC error correction scheme. This error correction scheme allows finding and correcting 1-bit errors
  and detecting 2-bit errors. ECC is performed over blocks of 256 bytes. The reason why we chose this small block size
  (ECC block sizes of 512 or more are more common) is simple: It works more reliably and has a higher propability of actually
  detecting and fixing errors.
  Why are smaller ECC sizes better ?
  Simple. Let's take one example, where we consider 2 * 256 bytes of memory. If an error occurs in both blocks, it can still
  be fixed if every block has a separate ECC, but not if we only have one ECC for the entire 512 bytes.
  So the 256 byte ECC is always at least as good as the 512-byte ECC and better in some cases where we deal with 2 1-bit errors
  (which are correctable) versus a single 2-bit errror, which is not.

  How does ECC work ?
    Fairly simple actually, but you should consult literature.
    Basically, you need 2 * n bits for an ECC of 2^n bits. This means: 256 bytes = 2048 bits = 2 ^ 11 bits => 22 bits required.

Data management
===============
  Data is stored in so called data blocks in the NAND flash. The assignment information (which pyhsical block contains which data)
  is stored in the spare area of the block.
  Modifications of data are not done in the data blocks directly, but using a concept of work blocks. A work block contains
  modifications of a data block.
  The first block is used to store format information and written only once. All other blocks are used to store
  data. This means that in the driver, a valid physical block index is always > 0.

Capacity
========
  Not all available data blocks are used to store data.
  About 3% of the blocks are reserved in order to make sure that there are always blocks available,
  even if some develop bad blocks and can no longer be used.

Reading data
============
  So when data is read, the driver checks
  a) Is there a work block which contains this information ? If so, this is recent and used.
  b) Is there a data block which contains this information ? If so, this is recent and used.
  c) Otherwise, the sector has never been written. In this case, the driver delivers 0xFF data bytes.

Writing data to the NAND flash
==============================
  TBD

Spare area usage
================
  The spare area stores the management information of the driver and the ECC of user data
  and is subdivided into several equal sized regions. Each region corresponds to 512 bytes of user data.
  Example for a NAND flash with a page size of 2048 bytes and a spare area of 64 bytes each region
  is 64 / (2048 / 512) = 16 bytes large. The ECC protects 512 bytes of user data and 4 bytes (bytes 4 to 7)
  of spare area and is stored at the end of the region starting from offset 8. At least 8 bytes are reseved for the ECC.
  The data layout of the spare area looks like this:

  Byte
  offset Region   Description
  ---------------------------
  0-1    0        Bad block marker. Valid only on the first page of a block. Not protected by ECC.
  2-3    0        Reserved
  4-7    0        Erase count. Valid only on the spare area of the first page in a block. Protected by ECC0.
                    Byte 4 - EraseCnt b[31..24]
                    Byte 5 - EraseCnt b[23..16]
                    Byte 6 - EraseCnt b[15..8]
                    Byte 7 - EraseCnt b[7..0]
  8-15   0        ECC0. Protects the user bytes 0x000-0x1FF and the spare bytes 4-7 of region 0.
  0-3    1        Reserved
  4-5    1        Logical block index. Valid only on the spare area of the second page in a block. Protected by ECC1.
                    Byte 4 - LBI  b[15:8]
                    Byte 5 - LBI  b[7:0]
  6      1        Data type and count. Valid only on the spare area of the second page in a block. Protected by ECC1.
                    Bit 3-0 - BlockCnt:  Increments every time a block is copied.
                    Bit 7-4 - BlockType: 0xF: Unused (Empty)
                                         0xE: Work
                                         0xC: Data
  7      1        Sector status. Valid only on the spare area of the second page in a block. Protected by ECC1.
                    Bit 3-0 - Sector data valid
  8-15   1        ECC1. Protects the user bytes 0x200-0x3FF and the spare bytes 4-7 of region 1.
  0-3    2        Reserved
  4-5    2        Block relative sector index. Valid only for work block. Protected by ECC2.
                    Byte 4 - BRSI Bit 15-8
                    Byte 5 - BRSI Bit 7-0
  6-7    2        Not used. Protected by ECC2.
  8-15   2        ECC2. Protects the user bytes 0x400-0x5FF and the spare bytes 4-7 of region 2.
  0-3    3        Reserved
  4-7    3        Not used. Protected by ECC3.
  8-15   3        ECC3. Protects the user bytes 0x600-0x7FF and the spare bytes 4-7 of region 3.
  ...    ...      ...
  ...    ...      ...
  ...    ...      ...

Passive wear leveling
=====================
  BlockNoNextErase = pInst->BlockNoNextErase
  if (BlockNoNextErase == 0) {
    FindNextFree(pInst);
  }

Active wear leveling
====================
  If (pInst->MaxErase - pInst->MinErase > ACTIVE_WL_THRESHOLD) {
    CopyBlock(BlockNoLowestEraseCnt);
  }

 LBI: Logical block index         : Gives the data position of a block.
BRSI: Block relative sector index : Index of sector relative to start of block, 0..255

----------------------------------------------------------------------
Potential improvements

  Reliability
    - Count bit errors in blocks.
----------------------------------------------------------------------
*/

#include "FS_Int.h"

/*********************************************************************
*
*       Defines, configurable
*
**********************************************************************
*/
#ifdef FS_NAND_MAXUNIT
  #define NUM_UNITS   FS_NAND_MAXUNIT
#else
  #define NUM_UNITS   4
#endif

#ifndef   FS_NAND_MAX_ERASE_CNT_DIFF
  #define FS_NAND_MAX_ERASE_CNT_DIFF  5000    // Determines active wear leveling threshold, at which a block containing unchanged data is copied and freed. 0 means no active wear leveling.
#endif

#ifndef   FS_NAND_NUM_READ_RETRIES
  #define FS_NAND_NUM_READ_RETRIES    10      // Number of retries in case of a read error (device error or uncorrectable bit error)
#endif

#ifndef   FS_NAND_ENABLE_STATS
  #define FS_NAND_ENABLE_STATS        (FS_DEBUG_LEVEL >= FS_DEBUG_LEVEL_CHECK_ALL)   // Statistics only in debug builds
#endif

#ifndef   FS_NAND_ENABLE_TRIM
  #define FS_NAND_ENABLE_TRIM         1       // Enables/disables the TRIM functionality
#endif

#ifndef   FS_NAND_ENABLE_CLEAN
  #define FS_NAND_ENABLE_CLEAN        1       // Enables/disables the CLEAN functionality
#endif

#ifndef   FS_NAND_RECLAIM_DRIVER_BAD_BLOCKS
  #define FS_NAND_RECLAIM_DRIVER_BAD_BLOCKS   1   // Configures how the driver treats the bad blocks marked by itself when the medium is low level formatted. 1: bad block is turned into a good block.
#endif

#ifndef   FS_NAND_MAX_SPARE_AREA_SIZE
  #define FS_NAND_MAX_SPARE_AREA_SIZE         0     // Configures the size of the buffer which stores the data of spare area as a number of bytes.
                                                    // Usually this is the size of the file system sector divided by 32. The Micron MT29F8G08ABABA device (and possibly others)
                                                    // have spare area of 224 bytes for a 4KB page. The default computation will not work in this case ant it is required to use
                                                    // this define to set the size of the spare area.
#endif

#ifndef   FS_NAND_VERIFY_WRITE
  #define FS_NAND_VERIFY_WRITE                0     // Read backs and compares the data of sector to check if the write operation was successful.
#endif

/*********************************************************************
*
*       Defines, non configurable
*
**********************************************************************
*/
#define LLFORMAT_VERSION      40001

#if FS_NAND_ENABLE_STATS
  #define IF_STATS(Exp) Exp
#else
  #define IF_STATS(Exp)
#endif

/*********************************************************************
*
*       Spare area usage
*
*  For details, see the explanation in header.
*/
#define SPARE_OFF_BLOCK_STAT      0x00
#define SPARE_OFF_ERASE_CNT       0x04
#define SPARE_OFF_LBI             0x04
#define SPARE_OFF_BLOCK_TYPE_CNT  0x06
#define SPARE_OFF_SECTOR_STAT     0x07
#define SPARE_OFF_BRSI            0x04

/*********************************************************************
*
*       Special values for "INVALID"
*/
#define ERASE_CNT_INVALID         0xFFFFFFFFUL    // Invalid erase count

/*********************************************************************
*
*       Block data type nibble
*/
#define BLOCK_TYPE_EMPTY          0xF             // Block is empty
#define BLOCK_TYPE_WORK           0xE             // Block is used as "work block"
#define BLOCK_TYPE_DATA           0xC             // Block contains valid data

/*********************************************************************
*
*       Block status marker
*/
#define BLOCK_STAT_BAD            0x00
#define BLOCK_STAT_GOOD           0xFF

/*********************************************************************
*
*       Sector data status
*/
#define SECTOR_STAT_WRITTEN       0x0
#define SECTOR_STAT_EMPTY         0xF

/*********************************************************************
*
*       Status of read/write NAND operations
*/
#define NAND_NO_ERROR             0   // Everything OK
#define NAND_BITS_CORRECTED       1   // One or more bit errors corrected
#define NAND_ERROR_IN_ECC         2   // Error in the ECC itself detected, data is OK
#define NAND_UNCORRECTABLE_ERROR  3   // 2 or more bit errors detected, not recoverable
#define NAND_READ_ERROR           4   // Error while reading from NAND, not recoverable
#define NAND_WRITE_ERROR          5   // Error while writing into NAND, recoverable
#define NAND_OUT_OF_FREE_BLOCKS   6   // Tried to allocate a free block but no more were found
#define NAND_ERASE_ERROR          7   // Error while erasing a NAND block, recoverable

/*********************************************************************
*
*       Const data
*
**********************************************************************
*/
#define FORMAT_INFO_SECTOR_INDEX    0
#define ERROR_INFO_SECTOR_INDEX     1
#define BLOCK_INFO_BRSI             1     // Page position in a block where the the block related information are stored. This is also the first page that stores data.
#define STORAGE_START_BLOCK_INDEX   1     // Index of the first block used for data storage

#if FS_SUPPORT_JOURNAL
  #define NUM_WORK_BLOCKS_MIN       4     // For performance reasons we need more work blocks when Journaling is enabled
#else
  #define NUM_WORK_BLOCKS_MIN       3
#endif

#define NUM_WORK_BLOCKS_MAX         10    // Maximum number of work blocks the driver allocates by default
#define BYTES_PER_PAGE_MIN          2048  // This is the minimum page size required by the driver.
#define BYTES_PER_ECC_BLOCK         512   // Number of bytes a single ECC protects.

/****************************************************************************************
*
*       The first sector/block in a NAND flash should have these values so the NAND
*       driver recognize the device as properly formatted.
*/
static const U8 _acInfo[16] = {
  0x53, 0x45, 0x47, 0x47, 0x45, 0x52             // Id (Can be expanded in the future to include format / version information)
};
#define INFO_OFF_LLFORMAT_VERSION         0x10
#define INFO_OFF_NUM_LOG_BLOCKS           0x20
#define INFO_OFF_NUM_WORK_BLOCKS          0x30

/****************************************************************************************
*
*       The second sector of the first block in a NAND flash stores the fatal error information
*/
#define INFO_OFF_IS_WRITE_PROTECTED       0x00    // Inverted. 0xFFFF -> ~0xFFFF = 0 means normal (not write protected)
#define INFO_OFF_HAS_FATAL_ERROR          0x02    // Inverted. 0xFFFF -> ~0xFFFF = 0 means normal (no error)
#define INFO_OFF_FATAL_ERROR_TYPE         0x04    // Type of fatal error
#define INFO_OFF_FATAL_ERROR_SECTOR_INDEX 0x08    // Index of the sector where the error occurred

/****************************************************************************************
*
*       The data area of the first page stores this info about the reason why the block was marked as bad
*/
#define INFO_OFF_BAD_MARK_ERROR_TYPE      0x00    // Reason why the block was marked as bad (U16, BE)
#define INFO_OFF_BAD_MARK_ERROR_BRSI      0x10    // Index of the sector where the error occurred in the block marked as bad (U16, BE)

/*********************************************************************
*
*       Type definitions
*
**********************************************************************
*/

/*********************************************************************
*
*       WORK_BLOCK
*
*  Organisation of a work block
*  ============================
*
*  The WORK_BLOCK structure has 6 elements, as can be seen below.
*  The first 2, pNext & pPrev, are used to keep it in a doubly linked list.
*  The next 2 elements are used to associate it with a data block and logical block index.
*  The last 2 elements contain the actual managment data. The are pointers to arrays, allocated during init.
*  paAssign is a n bit array.
*    The number of bits is determined by the number of sectors per block.
*    The index is the logical position (brsi, block relative sector index).
*/
typedef struct WORK_BLOCK_DESC {
  struct WORK_BLOCK_DESC * pNext;     // Pointer to next work buffer.     NULL if there is no next.
  struct WORK_BLOCK_DESC * pPrev;     // Pointer to previous work buffer. NULL if there is no previous.
  unsigned   pbi;             // Physical Index of the destination block which data is written to. 0 means none is selected yet.
  unsigned   lbi;             // Logical block index of the work block
  U16        brsiFree;        // Position in block of the first sector we can write to.
  void     * paAssign;        // Pointer to assignment table, containing n bits per block. n depends on number of sectors per block.
} WORK_BLOCK_DESC;

/*********************************************************************
*
*       NAND_INST structure
*
*  This is the main data structure for the entire driver.
*  It contains data items of one instance of the driver.
*/
typedef struct {
  U8    Unit;
  U8    IsLLMounted;
  U8    LLMountFailed;
  U8    IsWriteProtected;
  U8    HasFatalError;
  U8    ErrorType;               // Type of fatal error
  U32   ErrorSectorIndex;        // Index of the sector where the fatal error occurred
  const FS_NAND_PHY_TYPE * pPhyType;  // Interface to physical layer
  const FS_NAND_ECC_HOOK * pECCHook;  // Functions to compute and check ECC
  U8  * pFreeMap;                // Pointer to physical block usage map. Each bit represents one physical block. 0: Block is not assigned; 1: Assigned or bad block.
                                 // Only purpose is to find a free block.
  U8  * pLog2PhyTable;           // Pointer to Log2Phytable, which contains the logical to physical block translation (0xFFFF -> Not assigned)
  U32   NumSectors;              // Number of logical sectors. This is redundant, but the value is used in a lot of places, so it is worth it!
  U32   EraseCntMax;             // Worst (= highest) erase count of all blocks
  U32   NumPhyBlocks;
  U32   NumLogBlocks;
  U32   EraseCntMin;             // Smallest erase count of all blocks. Used for active wear leveling.
  U32   NumBlocksEraseCntMin;    // Number of erase counts with the smallest value
  U32   NumWorkBlocks;           // Number of configured work blocks
  WORK_BLOCK_DESC * pFirstWorkBlockInUse;   // Pointer to the first work block
  WORK_BLOCK_DESC * pFirstWorkBlockFree;    // Pointer to the first free work block
  WORK_BLOCK_DESC * paWorkBlock;            // WorkBlock management info
  U32   MRUFreeBlock;             // Most recently used free block
  U16   BytesPerPage;             // Number of bytes in the main area of a page
  U16   BytesPerSpareArea;        // Number of bytes in the spare area of a page. Usually, this is BytesPerPage/32
  U8    PPB_Shift;                // Pages per block shift. Typ. 6 for 64 pages for block
  U8    NumBitsPhyBlockIndex;
  U8    UseHW_ECC;
  U8    NumBitsCorrectable;       // Number of bit errors the ECC is able to correct. It is used for the page blank check.
  //
  // Position of management data in the spare area
  //
  U8    BlockStatOff;
  U8    EraseCntOff;
  U8    lbiOff;
  U8    BlockTypeCntOff;
  U8    SectorStatOff;
  U8    brsiOff;
  //
  // Configuration items. 0 per default, which typically means: Use a reasonable default
  //
  U32   FirstBlock;               // Allows sparing blocks at the beginning of the NAND flash
  U32   MaxNumBlocks;             // Allows sparing blocks at the end of the NAND flash
  U32   MaxEraseCntDiff;          // Threshold for active wear leveling
  U32   NumWorkBlocksConf;        // Number of work blocks configured by application
  //
  // Additional info for debugging purposes
  //
#if FS_NAND_ENABLE_STATS
  FS_NAND_STAT_COUNTERS   StatCounters;
#endif
} NAND_INST;

/*********************************************************************
*
*       Static data
*
**********************************************************************
*/
static U32        * _pSectorBuffer;         // We need a buffer for one sector for internal operations such as copying a block etc.
static U8         * _pSpareAreaData;        // Buffer for spare area
static NAND_INST  * _apInst[NUM_UNITS];
static int          _NumUnits;
static FS_NAND_ON_FATAL_ERROR_CALLBACK * _pfOnFatalError;
#if FS_NAND_VERIFY_WRITE
  static U32     * _pVerifyBuffer;
#endif

/*********************************************************************
*
*       Macros, function replacement
*
**********************************************************************
*/

/*********************************************************************
*
*       ASSERT_UNIT_NO_IS_IN_RANGE
*/
#if FS_DEBUG_LEVEL >= FS_DEBUG_LEVEL_CHECK_ALL
  #define ASSERT_UNIT_NO_IS_IN_RANGE(Unit)                                        \
    if (Unit >= NUM_UNITS) {                                                      \
      FS_DEBUG_ERROROUT((FS_MTYPE_DRIVER,  "NAND_UNI: Invalid unit number.\n"));  \
    }
#else
  #define ASSERT_UNIT_NO_IS_IN_RANGE(Unit)
#endif

/*********************************************************************
*
*       ASSERT_PHY_TYPE_IS_SET
*/
#if FS_DEBUG_LEVEL >= FS_DEBUG_LEVEL_CHECK_ALL
  #define ASSERT_PHY_TYPE_IS_SET(pInst)                                               \
    if ((const void *)pInst->pPhyType == NULL) {                                      \
      FS_DEBUG_ERROROUT((FS_MTYPE_DRIVER,  "NAND_UNI: Phy. layer type not set.\n"));  \
    }
#else
  #define ASSERT_PHY_TYPE_IS_SET(pInst)
#endif

/*********************************************************************
*
*     ASSERT_SECTORS_ARE_IN_RANGE
*/
#if FS_DEBUG_LEVEL >= FS_DEBUG_LEVEL_CHECK_ALL
  #define ASSERT_SECTORS_ARE_IN_RANGE(pInst, FirstSector, LastSector)                   \
    if (((FirstSector) >= pInst->NumSectors) || ((LastSector) >= pInst->NumSectors)) {  \
      FS_DEBUG_ERROROUT((FS_MTYPE_DRIVER,  "NAND_UNI: Invalid sector index.\n"));       \
    }
#else
  #define ASSERT_SECTORS_ARE_IN_RANGE(pInst, FirstSector, LastSector)
#endif

/*********************************************************************
*
*       Local functions
*
**********************************************************************
*/

/*********************************************************************
*
*       _Count0Bits
*
*   Function description
*     Returns the number of bits set to 0.
*/
static unsigned _Count0Bits(U32 Value) {
  unsigned NumBits;

  NumBits = 0;
  while (Value != 0xFFFFFFFF) {
    Value |= Value + 1;           // Turn on the rightmost 0-bit.
    ++NumBits;
  }
  return NumBits;
}

/*********************************************************************
*
*       _CalcNumWorkBlocksDefault
*
*   Function description
*     Computes the the default number of work blocks.
*     This is a percentage of number of NAND blocks.
*/
static U32 _CalcNumWorkBlocksDefault(U32 NumPhyBlocks) {
  U32 NumWorkBlocks;

#ifdef FS_NAND_MAX_WORK_BLOCKS
  NumWorkBlocks = FS_NAND_MAX_WORK_BLOCKS;
#else
  //
  // Allocate 10% of NAND capacity for work blocks
  //
  NumWorkBlocks = NumPhyBlocks >> 7;
  //
  // Limit the number of work blocks to reasonable values
  //
  if (NumWorkBlocks > NUM_WORK_BLOCKS_MAX) {
    NumWorkBlocks = NUM_WORK_BLOCKS_MAX;
  }
  if (NumWorkBlocks < NUM_WORK_BLOCKS_MIN) {
    NumWorkBlocks = NUM_WORK_BLOCKS_MIN;
  }
#endif
  return NumWorkBlocks;
}

/*********************************************************************
*
*       _CalcNumBlocksToUse
*
*   Function description
*     Computes the number of logical blocks available to file system.
*/
static int _CalcNumBlocksToUse(U32 NumPhyBlocks, U32 NumWorkBlocks) {
  int NumLogBlocks;
  int Reserve;

  //
  // Compute the number of logical blocks. These are the blocks which are
  // actually available to the file system and therefor determines the capacity.
  // We reserve a small percentage (about 3%) for bad blocks
  // plus the number of work blocks + 1 info block (first block) + 1 block for copy operations
  //
  NumLogBlocks  = (NumPhyBlocks * 125) >> 7;    // Reserve some blocks for blocks which are or can turn "bad" = unusable. We need at least one block.
  Reserve       = NumWorkBlocks + 2;
  NumLogBlocks -= Reserve;
  return NumLogBlocks;
}

/*********************************************************************
*
*       _CalcParity32
*
*   Function description
*     Computes the parity of a 32-bit item.
*     Returns 0 for even parity, 1 for odd parity.
*
*   Add. notes:
*     Most compilers will (and should) inline this code in higher optimization levels
*/
FS_OPTIMIZE
static U16 _CalcParity32(U32 Data) {
  Data = (Data >> 16) ^ Data;           // Reduce 32 bits to 16 bits
  Data = (Data >>  8) ^ Data;           // Reduce 16 bits to 8 bits
  Data = (Data >>  4) ^ Data;           // Reduce 8 bits to 4 bits
  Data = (Data >>  2) ^ Data;           // Reduce 4 bits to 2 bits
  Data = (Data >>  1) ^ Data;           // Reduce 2 bits to 1 bit
  return (U16)(Data & 1);
}

/*********************************************************************
*
*       _ECC4_Calc
*
*   Function description
*     Computes a 1-bit ECC over 4 bytes. Typ. used to protect the data from the spare area.
*/
FS_OPTIMIZE
static U16 _ECC4_Calc(const U8 * pData) {
  U16 ecc;
  U16 Necc;
  U32 Data32;

  //
  // Load the data in a 32-bit word.
  //
  Data32 = FS_LoadU32LE(pData);
  //
  // Compute the ECC Pn bits (located at odd bit positions).
  // Bit locations were taken from [5].
  //
  ecc  = _CalcParity32(Data32 & 0xAAAAAAAAuL) << 5;   // p1
  ecc |= _CalcParity32(Data32 & 0xCCCCCCCCuL) << 7;   // p2
  ecc |= _CalcParity32(Data32 & 0xF0F0F0F0uL) << 9;   // p4
  ecc |= _CalcParity32(Data32 & 0xFF00FF00uL) << 1;   // p8
  ecc |= _CalcParity32(Data32 & 0xFFFF0000uL) << 3;   // p16
  //
  // Compute the even bits of the ECC: Pn' = Pn ^ P;
  //
  Necc = ecc >> 1;
  if (_CalcParity32(Data32)) {
    Necc ^= 0x5555u;
  }
  ecc |= Necc;
  ecc ^= 0xFFFFu;
  return ecc;
}

/*********************************************************************
*
*       _ECC4_Apply
*
*   Function description
*     Checks and corrects 4 data bytes using a 1-bit ECC.
*/
static int _ECC4_Apply(U8 * pData, U16 eccRead) {
  U16 eccCalced;
  U16 eccXor;
  int i;
  int NumDiffBits;
  unsigned BitPos;
  U32 Data32;

  eccCalced = _ECC4_Calc(pData);
  eccXor    = eccCalced ^ eccRead;
  if (eccXor == 0) {
    return 0;          // Both ECCs match, data is o.k. without correction
  }
  //
  // Count number of different bits in both ECCs
  //
  NumDiffBits = 0;
  for (i = 0; i < 16; i++) {
    if (eccXor & (U16)(1u << i)) {
      NumDiffBits++;
    }
  }
  //
  // Check if this is a correctable error
  //
  if (NumDiffBits == 1) {
    return 2;         // Error in ECC
  }
  if (NumDiffBits != 8) {
    return 3;         // Uncorrectable error
  }
  //
  // Perform correction
  //
  Data32 = FS_LoadU32LE(pData);
  BitPos =  ((eccXor >> 5) & 1)
         | (((eccXor >> 7) & 1) << 1)
         | (((eccXor >> 9) & 1) << 2)
         | (((eccXor >> 1) & 1) << 3)
         | (((eccXor >> 3) & 1) << 4)
         ;
  Data32 ^= (U32)(1uL << BitPos);
  FS_StoreU32LE(pData, Data32);
  return 1;       // Error has been corrected
}

/*********************************************************************
*
*       _ECC516_Compute
*
*   Function description
*     Computates a 1-bit ECC over 512 data bytes and 4 spare bytes.
*     The computed ECC is stored in the spare area.
*
*   Parameters
*     pData       [IN]     Points to a 128 word (512 byte) data area
*     pSpare      [IN,OUT] Points to the 16 byte spare area. Bytes 4-7 are protected by ECC. Bytes 8-F stores the ECC.
*
*   Notes
*       (1) Bits 17/16 are not used. We set them to 1 to avoid ECC errors on a blank block.
*/
static void _ECC516_Compute(const U32 * pData, U8 * pSpare) {
  U32 eccData0;
  U32 eccData1;
  U16 eccSpare;

  //
  // Compute the ECC of the data area and spare area.
  //
  eccData0  = FS__ECC256_Calc(pData);
  eccData0 |= 0x30000;                    // Note 1
  eccData1  = FS__ECC256_Calc(pData + 64);
  eccData1 |= 0x30000;                    // Note 1
  eccSpare  = _ECC4_Calc(pSpare + 4);
  //
  // Store the computed ECC to spare area.
  //
  FS__ECC256_Store(pSpare + 8,  eccData0);
  FS__ECC256_Store(pSpare + 11, eccData1);
  FS_StoreU16LE   (pSpare + 14, eccSpare);
}

/*********************************************************************
*
*       _ECC516_Apply
*
*   Function description
*     Checks and corrects 512 data bytes and 4 spare bytes using a 1-bit ECC.
*
*   Parameters
*     pData       [IN,OUT]     Points to a 128 word (512 byte) data area
*     pSpare      [IN,OUT]     Points to the 16 byte spare area. Bytes 4-7 are protected by ECC. Bytes 8-F stores the ECC.
*/
static int _ECC516_Apply(U32 * pData, U8 * pSpare) {
  U32 eccData;
  U16 eccSpare;
  int r;
  int Result;

  Result   = 0;
  eccData  = FS__ECC256_Load(pSpare + 8);
  eccData &= ~0x30000uL;                  // Bits 17/16 are not used. The FS__ECC256_Apply routine expects them to be 0.
  r = FS__ECC256_Apply(pData, eccData);
  if (r > Result) {
    Result = r;
  }
  eccData  = FS__ECC256_Load(pSpare + 11);
  eccData &= ~0x30000uL;                  // Bits 17/16 are not used. The FS__ECC256_Apply routine expects them to be 0.
  r = FS__ECC256_Apply(pData + 64, eccData);
  if (r > Result) {
    Result = r;
  }
  eccSpare = FS_LoadU16LE(pSpare + 14);
  r = _ECC4_Apply(pSpare + 4, eccSpare);
  if (r > Result) {
    Result = r;
  }
  return Result;
}

/*********************************************************************
*
*       _ComputeAndStoreECC
*
*   Function description
*     Computes the ECC values and writes them into the 64-byte buffer for the redundant area
*/
static void _ComputeAndStoreECC(const NAND_INST * pInst, const U32 * pData, U8 * pSpare) {
  const FS_NAND_ECC_HOOK * pECCHook;

  pECCHook = pInst->pECCHook;
  if (pECCHook) {
    unsigned NumLoops;

    NumLoops = pInst->BytesPerPage >> 9;      // 512 bytes are taken care of in one loop.
    do {
      pECCHook->pfCompute(pData, pSpare);
      pData  += 128;
      pSpare += 16;
    } while (--NumLoops);
  }
}

/*********************************************************************
*
*       _ApplyECC
*
*   Function description
*     Uses the ECC values to correct the data if necessary
*     Works on an entire 2 Kbyte page (which is divided into 4 parts of 512 bytes each)
*
*   Return value
*     == 0  OK, data is valid. No error in data
*     == 1  Bit error(s) in data which has been corrected
*     == 2  OK, data is valid but there is a bit error in ECC
*     == 3  Uncorrectable error
*/
static int _ApplyECC(const NAND_INST * pInst, U32 * pData, U8 * pSpare) {
  const FS_NAND_ECC_HOOK * pECCHook;
  int      r;
  unsigned Result;

  Result   = 0;
  pECCHook = pInst->pECCHook;
  if (pECCHook) {
    unsigned NumLoops;

    NumLoops = pInst->BytesPerPage >> 9;      // ECC is computed over 512 bytes.
    Result   = 0;
    do {
      r = pECCHook->pfApply(pData, pSpare);
      if ((unsigned)r > Result) {
        Result = (unsigned)r;
      }
      pData  += 128;
      pSpare += 16;
    } while (--NumLoops);
  }
  return (int)Result;
}

/*********************************************************************
*
*       _EnableHW_ECC
*
*   Function description
*     Tells the NAND flash to correct the bit errors using the internal ECC engine.
*
*   Return value
*     0   OK
*     1   An error occurred
*/
static int _EnableHW_ECC(const NAND_INST * pInst) {
  int r;

  r = 0;
  if (pInst->pPhyType->pfEnableECC) {
    r = pInst->pPhyType->pfEnableECC(pInst->Unit);
  }
  return r;
}

/*********************************************************************
*
*       _DisableHW_ECC
*
*   Function description
*     Tells the NAND flash to deliver uncorrected data.
*
*   Return value
*     0   OK
*     1   An error occurred
*/
static int _DisableHW_ECC(const NAND_INST * pInst) {
  int r;

  r = 0;
  if (pInst->pPhyType->pfDisableECC) {
    r = pInst->pPhyType->pfDisableECC(pInst->Unit);
  }
  return r;
}

/*********************************************************************
*
*       _EnableHW_ECCIfRequired
*
*/
static int _EnableHW_ECCIfRequired(const NAND_INST * pInst) {
  int r;

  r = 0;
  if (pInst->UseHW_ECC) {         // Bit error correction performed in the HW?
    r = _EnableHW_ECC(pInst);
  }
  return r;
}

/*********************************************************************
*
*       _DisableHW_ECCIfRequired
*
*/
static int _DisableHW_ECCIfRequired(const NAND_INST * pInst) {
  int r;

  r = 0;
  if (pInst->UseHW_ECC) {       // Bit error correction performed in the HW?
    r = _DisableHW_ECC(pInst);
  }
  return r;
}

/*********************************************************************
*
*       _ConfigureHW_ECC
*
*   Function description
*     Sets the correction level for the HW ECC.
*
*   Return value
*     0   OK, correction level lset
*     1   Correction level not supported
*/
static int _ConfigureHW_ECC(const NAND_INST * pInst, U8 NumBitsCorrectable, U16 BytesPerECCBlock) {
  int r;

  r = 0;
  if (pInst->pPhyType->pfConfigureECC) {
    r = pInst->pPhyType->pfConfigureECC(pInst->Unit, NumBitsCorrectable, BytesPerECCBlock);
  }
  return r;
}

/*********************************************************************
*
*       _GetNextFreeSector
*
*   Function description
*     Returns the BRSI of the sector we can write to.
*
*   Return value
*     !=0    OK
*     ==0    All sectors written
*/
static unsigned _GetNextFreeSector(const NAND_INST * pInst, WORK_BLOCK_DESC * pWorkBlock) {
  unsigned SectorsPerBlock;
  unsigned brsiFree;

  SectorsPerBlock = 1 << pInst->PPB_Shift;
  brsiFree        = pWorkBlock->brsiFree;
  if ((brsiFree < BLOCK_INFO_BRSI) || (brsiFree >= SectorsPerBlock)) {
    return 0;                         // All sectors in the work block were written.
  }
  pWorkBlock->brsiFree++;
  return brsiFree;
}

/*********************************************************************
*
*       _ReadApplyDeviceParas
*
*  Function description
*    Reads the device info and computes the parameters stored in the Instance structure
*    such as Number of blocks, Number of Sectors, Sector Size etc.
*
*  Return value:
*    0    - O.K.
*    1    - Error, Could not apply device paras
*/
static int _ReadApplyDeviceParas(NAND_INST * pInst) {
  unsigned BytesPerPage;
  unsigned PPB_Shift;
  U32      MaxNumBlocks;
  U32      NumBlocks;
  FS_NAND_DEVICE_INFO DeviceInfo;
  U32      NumWorkBlocks;
  int      NumLogBlocks;
  U8       NumBitsCorrectableReq;
  U8       NumBitsCorrectableSup;
  unsigned SectorsPerBlock;
  int      r;
  U16      BytesPerSpareArea;
  U8       UseHW_ECC;
  U8       BlockStatOff;
  U8       EraseCntOff;
  U8       lbiOff;
  U8       BlockTypeCntOff;
  U8       SectorStatOff;
  U8       brsiOff;

  FS_MEMSET(&DeviceInfo, 0, sizeof(DeviceInfo));
  if (pInst->pPhyType->pfInitGetDeviceInfo(pInst->Unit, &DeviceInfo)) {
    FS_DEBUG_ERROROUT((FS_MTYPE_DRIVER, "NAND_UNI: Could not read device info. Fatal.\n"));
    return 1;
  }
  MaxNumBlocks = pInst->MaxNumBlocks;
  NumBlocks    = DeviceInfo.NumBlocks;
  if (NumBlocks <= pInst->FirstBlock) {
    return 1;                         // Less blocks than configured
  }
  NumBlocks -= pInst->FirstBlock;
  if (MaxNumBlocks) {                 // Is an upper limit configured ?
    if (NumBlocks > MaxNumBlocks) {
      NumBlocks = MaxNumBlocks;
    }
  }
  //
  // Compute a default number of work blocks if the application did not configured it yet.
  //
  if (pInst->NumWorkBlocksConf == 0) {
    NumWorkBlocks = _CalcNumWorkBlocksDefault(NumBlocks);
  } else {
    NumWorkBlocks = pInst->NumWorkBlocksConf;
  }
  //
  // Compute the number of blocks available to file system
  //
  NumLogBlocks = _CalcNumBlocksToUse(NumBlocks, NumWorkBlocks);
  if (NumLogBlocks <= 0) {
    FS_DEBUG_ERROROUT((FS_MTYPE_DRIVER, "NAND_UNI: Insufficient logical blocks.\n"));
    return 1;
  }
  BytesPerPage = 1u << DeviceInfo.BPP_Shift;
  if (BytesPerPage < BYTES_PER_PAGE_MIN) {
    FS_DEBUG_ERROROUT((FS_MTYPE_DRIVER, "NAND_UNI: Page size of device is too small. A minimum of %d bytes per page is required.\n", BYTES_PER_PAGE_MIN));
    return 1;                         // Error
  }
  if (BytesPerPage > (U32)FS_Global.MaxSectorSize) {
    FS_DEBUG_ERROROUT((FS_MTYPE_DRIVER, "NAND_UNI: Sector size of device is higher than the sector size that can be stored by the FS.\n"));
    return 1;                         // Error
  }
  //
  // Determine the size of the spare area.
  //
  BytesPerSpareArea = DeviceInfo.BytesPerSpareArea;
  if (BytesPerSpareArea == 0) {
    BytesPerSpareArea = BytesPerPage >> 5;  // If the physical layer does not provide a size for the spare area use the usual size of BytesPerPage / 32.
  }
  //
  // Provide default ECC computation routines if none were configured.
  //
  if (pInst->pECCHook == NULL) {
    pInst->pECCHook = &FS_NAND_ECC_1BIT;
  }
  //
  // Determine whether HW ECC should be used.
  //
  UseHW_ECC = 0;
  if ((pInst->pECCHook->pfApply == NULL) || (pInst->pECCHook->pfCompute == NULL)) {
    UseHW_ECC = 1;
  }
  //
  // Get the number of bit errors requested by the NAND flash and correctable by the ECC algorithm.
  //
  NumBitsCorrectableSup = pInst->pECCHook->NumBitsCorrectable;
  NumBitsCorrectableReq = (int)DeviceInfo.ECC_Info.NumBitsCorrectable;
  if (NumBitsCorrectableReq == 0) {
    NumBitsCorrectableReq = NumBitsCorrectableSup;
  }
  //
  // Check if the correction level of ECC is suitable for the NAND flash.
  //
  if (NumBitsCorrectableReq > NumBitsCorrectableSup) {
    FS_DEBUG_WARN((FS_MTYPE_DRIVER, "NAND_UNI: Correction level of ECC is too low for the NAND flash.\n"));
  }
  //
  // Enable the HW ECC if required.
  //
  if (UseHW_ECC) {
    r = _ConfigureHW_ECC(pInst, NumBitsCorrectableReq, BYTES_PER_ECC_BLOCK);
    if (r) {
      FS_DEBUG_WARN((FS_MTYPE_DRIVER, "NAND_UNI: Error correction level not supported by the HW ECC.\n"));
    }
    //
    // Enable the HW ECC. It depends on the physical layer whether the HW ECC of the NAND flash device or the HW ECC of the NAND flash controller is used.
  //
    r = _EnableHW_ECC(pInst);
    if (r) {
      FS_DEBUG_WARN((FS_MTYPE_DRIVER, "NAND_UNI: Could not enable the HW ECC.\n"));
    }
  }
  //
  // Compute the byte offset of management information in the spare area.
  //
  {
    U8 SpareOff;
    U8 NumBytesSpare;
    U8 ECCBlocksPerPage;

    SpareOff         = 0;
    ECCBlocksPerPage = BytesPerPage / BYTES_PER_ECC_BLOCK;
    NumBytesSpare    = BytesPerSpareArea / ECCBlocksPerPage;
    BlockStatOff     = SpareOff + SPARE_OFF_BLOCK_STAT;
    EraseCntOff      = SpareOff + SPARE_OFF_ERASE_CNT;
    SpareOff        += NumBytesSpare;
    lbiOff           = SpareOff + SPARE_OFF_LBI;
    BlockTypeCntOff  = SpareOff + SPARE_OFF_BLOCK_TYPE_CNT;
    SectorStatOff    = SpareOff + SPARE_OFF_SECTOR_STAT;
    SpareOff        += NumBytesSpare;
    brsiOff          = SpareOff + SPARE_OFF_BRSI;
  }
  //
  // Save the parameters to driver instance.
  //
  PPB_Shift                   = DeviceInfo.PPB_Shift;
  SectorsPerBlock             = (1 << PPB_Shift) - 1;   // First sector is reserved for erase count and bad block status.
  pInst->NumPhyBlocks         = NumBlocks;
  pInst->NumBitsPhyBlockIndex = FS_BITFIELD_CalcNumBitsUsed(NumBlocks);
  pInst->NumLogBlocks         = (U32)NumLogBlocks;
  pInst->NumWorkBlocks        = NumWorkBlocks;
  pInst->BytesPerPage         = BytesPerPage;
  pInst->NumSectors           = pInst->NumLogBlocks * SectorsPerBlock;
  pInst->PPB_Shift            = PPB_Shift;
  pInst->BytesPerSpareArea    = BytesPerSpareArea;
  pInst->UseHW_ECC            = UseHW_ECC;
  pInst->NumBitsCorrectable   = NumBitsCorrectableReq;
  pInst->BlockStatOff         = BlockStatOff;
  pInst->EraseCntOff          = EraseCntOff;
  pInst->lbiOff               = lbiOff;
  pInst->BlockTypeCntOff      = BlockTypeCntOff;
  pInst->SectorStatOff        = SectorStatOff;
  pInst->brsiOff              = brsiOff;
  return 0;                   // O.K., successfully identified
}

/*********************************************************************
*
*       _StoreBlockType
*
*   Function description
*     Writes the block type to static spare area buffer.
*/
static void _StoreBlockType(const NAND_INST * pInst, unsigned BlockType) {
  U8 Data8;
  U8 Off;

  Off = pInst->BlockTypeCntOff;
  Data8  = _pSpareAreaData[Off];
  Data8 &= ~(0xF << 4);
  Data8 |= (BlockType & 0xF) << 4;
  _pSpareAreaData[Off] = Data8;
}

/*********************************************************************
*
*       _LoadBlockType
*
*   Function description
*     Reads the block type from static spare area buffer.
*/
static unsigned _LoadBlockType(const NAND_INST * pInst) {
  unsigned BlockType;
  U8       Off;

  Off = pInst->BlockTypeCntOff;
  BlockType = (_pSpareAreaData[Off] >> 4) & 0xF;
  return BlockType;
}

/*********************************************************************
*
*       _StoreBlockCnt
*
*   Function description
*     Writes the block count to static spare area buffer.
*/
static void _StoreBlockCnt(const NAND_INST * pInst, unsigned BlockCnt) {
  U8 Data8;
  U8 Off;

  Off = pInst->BlockTypeCntOff;
  Data8 = _pSpareAreaData[Off];
  Data8 &= ~0xF;
  Data8 |= BlockCnt & 0xF;
  _pSpareAreaData[Off] = Data8;
}

/*********************************************************************
*
*       _LoadBlockCnt
*
*   Function description
*     Reads the block count from the static spare area buffer.
*/
static unsigned _LoadBlockCnt(const NAND_INST * pInst) {
  unsigned BlockCnt;
  U8       Off;

  Off = pInst->BlockTypeCntOff;
  BlockCnt = _pSpareAreaData[Off] & 0xF;
  return BlockCnt;
}

/*********************************************************************
*
*       _StoreEraseCnt
*
*  Function description
*    Stores the erase count in the static spare area buffer.
*/
static void _StoreEraseCnt(const NAND_INST * pInst, U32 EraseCnt) {
  U8 Off;

  Off = pInst->EraseCntOff;
  FS_StoreU32BE(_pSpareAreaData + Off, EraseCnt);
}

/*********************************************************************
*
*       _LoadEraseCnt
*
*  Function description
*    Reads the erase count from the static spare area buffer.
*/
static U32 _LoadEraseCnt(const NAND_INST * pInst) {
  U32 EraseCnt;
  U8  Off;

  Off = pInst->EraseCntOff;
  EraseCnt = FS_LoadU32BE(_pSpareAreaData + Off);
  return EraseCnt;
}

/*********************************************************************
*
*       _StoreLBI
*
*  Function description
*    Stores the logical block index in the static spare area buffer.
*/
static void _StoreLBI(const NAND_INST * pInst, unsigned lbi) {
  U8 Off;

  Off = pInst->lbiOff;
  FS_StoreU16BE(_pSpareAreaData + Off, lbi);
}

/*********************************************************************
*
*       _LoadLBI
*
*  Function description
*    Reads the logical block index from the static spare area buffer.
*
*  Return value
*    Index of the logical block.
*/
static unsigned _LoadLBI(const NAND_INST * pInst) {
  U8       Off;
  unsigned lbi;

  Off = pInst->lbiOff;
  lbi = FS_LoadU16BE(_pSpareAreaData + Off);
  return lbi;
}

/*********************************************************************
*
*       _StoreBRSI
*
*  Function description
*    Writes the block relative sector index into the static spare area buffer.
*/
static void _StoreBRSI(const NAND_INST * pInst, unsigned brsi) {
  U8 Off;

  Off = pInst->brsiOff;
  FS_StoreU16BE(_pSpareAreaData + Off, brsi);
}

/*********************************************************************
*
*       _LoadBRSI
*
*  Function description
*    Reads the block relative sector index from the static spare area buffer.
*/
static unsigned _LoadBRSI(const NAND_INST * pInst) {
  unsigned brsi;
  U8       Off;

  Off  = pInst->brsiOff;
  brsi = FS_LoadU16BE(_pSpareAreaData + Off);
  return brsi;
}

/*********************************************************************
*
*       _StoreBlockStat
*
*   Function description
*     Stores the block bad status in the static sparea area buffer.
*/
static void _StoreBlockStat(const NAND_INST * pInst, unsigned BlockStat) {
  U8 Off;

  Off = pInst->BlockStatOff;
  _pSpareAreaData[Off] = (U8)BlockStat;
}

/*********************************************************************
*
*       _CorrectBlockStatIfRequired
*
*   Function description
*     Takes the bad/good status of a block and corrects its value if it has been affected by bit errors.
*
*/
static unsigned _CorrectBlockStatIfRequired(unsigned BlockStat) {
  unsigned NumBits;
  unsigned Data;
  unsigned i;

  //
  // If the block stat does not store one of the pre-defined values we will have to count bits to decide whether the block is good or bad.
  //
  if ((BlockStat != BLOCK_STAT_GOOD) && (BlockStat != BLOCK_STAT_BAD)) {
    //
    // Count the number of bits set to 1.
    //
    NumBits = 0;
    Data    = BlockStat;
    for (i = 0; i < 8; ++i) {
      if (Data & 1) {
        ++NumBits;
      }
      Data >>= 1;
    }
    //
    // The block good if more than half of the bits are set to 1.
    //
    if (NumBits > 4) {
      BlockStat = BLOCK_STAT_GOOD;
    } else {
      BlockStat = BLOCK_STAT_BAD;
    }
  }
  return BlockStat;
}

/*********************************************************************
*
*       _StoreSectorStat
*
*   Function description
*     Writes to the static spare area buffer the information whether the sector has been written or not.
*/
static void _StoreSectorStat(const NAND_INST * pInst, unsigned SectorStat) {
  U8 Data8;
  U8 Off;

  Off = pInst->SectorStatOff;
  Data8  = _pSpareAreaData[Off];
  Data8 &= ~0xF;
  Data8 |= SectorStat & 0xF;
  _pSpareAreaData[Off] = Data8;
}

/*********************************************************************
*
*       _LoadSectorStat
*
*   Function description
*     Reads from the static spare area buffer the information whether the sector has been written or not.
*/
static unsigned _LoadSectorStat(const NAND_INST * pInst) {
  unsigned SectorStat;
  U8       Off;

  Off = pInst->SectorStatOff;
  SectorStat = _pSpareAreaData[Off] & 0xF;
  return SectorStat;
}

/*********************************************************************
*
*       _BlockIndex2SectorIndex0
*
*  Function description
*    Returns the sector index of the first sector in a block.
*    With 128KB blocks and 2048 byte sectors, this means multiplying with 64.
*    With 128KB blocks and  512 byte sectors, this means multiplying with 256.
*/
static U32 _BlockIndex2SectorIndex0(const NAND_INST * pInst, unsigned BlockIndex) {
  U32 SectorIndex;
  SectorIndex = (U32)BlockIndex << pInst->PPB_Shift;
  return SectorIndex;
}

/*********************************************************************
*
*       _EraseBlock
*/
static int _EraseBlock(NAND_INST * pInst, unsigned BlockIndex) {
  U32 PageIndex;
  int r;


  IF_STATS(pInst->StatCounters.EraseCnt++);     // Increment statistics counter if enabled
  BlockIndex += pInst->FirstBlock;
  PageIndex   = (U32)BlockIndex << pInst->PPB_Shift;
  r = pInst->pPhyType->pfEraseBlock(pInst->Unit, PageIndex);
  return r;
}

/*********************************************************************
*
*       _PhySectorIndex2PageIndex
*
*   Function description
*     Converts logical sectors (which can be 512 / 1024 / 2048 bytes) into
*     physical pages with same or larger page size.
*/
static U32 _PhySectorIndex2PageIndex(const NAND_INST * pInst, U32 PhySectorIndex, unsigned * pOff) {
  unsigned Off;
  U32      PageIndex;

  PageIndex  = PhySectorIndex;
  Off        = *pOff;
  Off       += pInst->BytesPerPage;        // Move offset from data to spare area
  *pOff      = Off;
  PageIndex += (U32)pInst->FirstBlock << pInst->PPB_Shift;
  return PageIndex;
}

/*********************************************************************
*
*       _LogSectorIndex2LogBlockIndex
*
*   Function description
*     For a given file system sector computes the logical block index and the position in the block.
*/
static unsigned _LogSectorIndex2LogBlockIndex(const NAND_INST * pInst, U32 SectorIndex, unsigned * pBRSI) {
  U32 lbi;
  U32 brsi;
  U32 SectorsPerBlock;

  SectorsPerBlock = 1 << pInst->PPB_Shift;
  --SectorsPerBlock;      // Sector 0 is used only to store the erase count of the block.
  lbi = FS__DivModU32(SectorIndex, SectorsPerBlock, &brsi);
  if (pBRSI) {
    ++brsi;               // One sector more to take into account; the sector 0 is reseved for the erase count.
  }
  if (pBRSI) {
    *pBRSI = brsi;
  }
  return lbi;
}

/*********************************************************************
*
*       _ReadDataSpare
*
*   Function description
*     Reads all of the data area as well as all of the spare area
*/
static int _ReadDataSpare(NAND_INST * pInst, U32 SectorIndex, void * pData, unsigned NumBytes, void * pSpare, unsigned NumBytesSpare) {
  U32      PageIndex;
  unsigned OffSpare;

  IF_STATS(pInst->StatCounters.ReadDataCnt++);     // Increment statistics counter if enabled
  OffSpare  = 0;
  PageIndex = _PhySectorIndex2PageIndex(pInst, SectorIndex, &OffSpare);
  return pInst->pPhyType->pfReadEx(pInst->Unit, PageIndex, pData, 0, NumBytes, pSpare, OffSpare, NumBytesSpare);
}

/*********************************************************************
*
*       _WriteDataSpare
*
*   Function description
*     Writes all of the data area as well as all of the spare area.
*     The important point here is this function performs both operations with a single call to the physical layer,
*     giving the physical layer a chance to perform the operation as one operation, which saves time.
*/
static int _WriteDataSpare(NAND_INST * pInst, U32 SectorIndex, const void * pData, unsigned NumBytes, const void * pSpare, unsigned NumBytesSpare) {
  U32      PageIndex;
  unsigned OffSpare;

  IF_STATS(pInst->StatCounters.WriteDataCnt++);                 // Increment statistics counter if enabled.
  OffSpare  = 0;
  PageIndex = _PhySectorIndex2PageIndex(pInst, SectorIndex, &OffSpare);
  return pInst->pPhyType->pfWriteEx(pInst->Unit, PageIndex, pData, 0, NumBytes, pSpare, OffSpare, NumBytesSpare);
}

/*********************************************************************
*
*       _ReadPhySpare
*
*   Function description
*     Reads (a part or all of) the spare area.
*
*   Parameters
*     pInst       [IN]  Driver instance
*                 [OUT] ---
*     PageIndex   Physical index of the page to read from
*     pData       [IN]  ---
*                 [OUT] Data read from the spare area
*     Off         Byte offset in the spare area to start reading
*     NumBytes    Number of bytes to read
*
*   Return value
*     == 0  OK
*     != 0  An error occurred
*/
static int _ReadPhySpare(NAND_INST * pInst, U32 PageIndex, void * pData, unsigned Off, unsigned NumBytes) {
  IF_STATS(pInst->StatCounters.ReadSpareCnt++);                 // Increment statistics counter if enabled.
  PageIndex += pInst->FirstBlock << pInst->PPB_Shift;
  return pInst->pPhyType->pfRead(pInst->Unit, PageIndex, pData, Off + pInst->BytesPerPage, NumBytes);
}

/*********************************************************************
*
*       _ReadPhySpareByte
*
*   Function description:
*     Reads 1 byte of the spare area of the given page.
*
*/
static int _ReadPhySpareByte(NAND_INST * pInst, U32 PageIndex, U8 * pData, unsigned Off) {
  U8  ab[2];
  int r;

  r = _ReadPhySpare(pInst, PageIndex, ab, Off & 0xFE, 2);       // Make sure we have 2-byte alignment required by 16-bit NAND flashes.
  *pData = ab[Off & 1];
  return r;
}

/*********************************************************************
*
*       _MarkBlockAsFree
*
*  Function description
*    Mark block as free in management data.
*/
static void _MarkBlockAsFree(NAND_INST * pInst, unsigned iBlock) {
  U8 Mask;
  U8 * pData;

  Mask = 1 << (iBlock & 7);
  if(iBlock >= pInst->NumPhyBlocks) {
    return;
  }
  pData = pInst->pFreeMap + (iBlock >> 3);
#if FS_NAND_ENABLE_STATS
  if ((*pData & Mask) == 0) {
    pInst->StatCounters.NumFreeBlocks++;
  }
#endif
  *pData |= Mask;    // Mark block as free
}

/*********************************************************************
*
*       _MarkBlockAsAllocated
*
*  Function description
*    Mark block as allocated in management data.
*/
static void _MarkBlockAsAllocated(NAND_INST * pInst, unsigned iBlock) {
  U8 Mask;
  U8 * pData;

  Mask = 1 << (iBlock & 7);
  pData = pInst->pFreeMap + (iBlock >> 3);
#if FS_NAND_ENABLE_STATS
  if (*pData & Mask) {
    pInst->StatCounters.NumFreeBlocks--;
  }
#endif
  *pData &= ~(unsigned)Mask;    // Mark block as allocated
}

/*********************************************************************
*
*       _BlockIsFree
*
*  Function description
*    Return if block is free.
*/
static char _BlockIsFree(const NAND_INST * pInst, unsigned iBlock) {
  U8 Mask;
  U8 * pData;

  Mask = 1 << (iBlock & 7);
  pData = pInst->pFreeMap + (iBlock >> 3);
  return *pData & Mask;
}

/*********************************************************************
*
*       _ClearStaticSpareArea
*
*  Function description
*    Fills the entire static spare area with 0xFF.
*/
static void _ClearStaticSpareArea(const NAND_INST * pInst) {
  FS_MEMSET(_pSpareAreaData, 0xFF, pInst->BytesPerSpareArea);
}

/*********************************************************************
*
*       _WriteSectorWithECC
*
*  Function description
*    Performs the following:
*    - Computes ECC and stores it into static spare area
*    - Write entire sector & spare area into NAND flash (in one operations if possible)
*
*  Return value
*        0     O.K., page data has been successfully written
*     != 0     Error
*
*  Notes
*    Before the function call, the static spare area needs to contain info for the page (such as lbi, EraseCnt, etc)
*/
static int _WriteSectorWithECC(NAND_INST * pInst, const U32 * pBuffer, U32 SectorIndex) {
  if (pInst->UseHW_ECC == 0) {             // Bit error correction performed in SW by the host?
    _ComputeAndStoreECC(pInst, (const U32 *)pBuffer, _pSpareAreaData);
  }
  return _WriteDataSpare(pInst, SectorIndex, pBuffer, pInst->BytesPerPage, _pSpareAreaData, pInst->BytesPerSpareArea);
}

/*********************************************************************
*
*       _OnFatalError
*
*   Function description:
*     Called when a fatal error occurs. It switches to read-only mode
*     and sets the error flag.
*
*   Parameters
*     pInst             [IN]  Driver instance
*                       [OUT] ---
*     ErrorType         Identifies the error
*     ErrorSectorIndex  Index of the physical sector where the error occurred
*/
static void _OnFatalError(NAND_INST * pInst, int ErrorType, unsigned ErrorSectorIndex) {
  if (pInst->IsWriteProtected == 0) {
    U8 * pPageBuffer;

    pInst->IsWriteProtected = 1;
    pInst->HasFatalError    = 1;
    pInst->ErrorType        = (U8)ErrorType;
    pInst->ErrorSectorIndex = ErrorSectorIndex;
    FS_DEBUG_ERROROUT((FS_MTYPE_DRIVER, "NAND_UNI: FATAL error: Error %d occurred on sector %u.\n", ErrorType, ErrorSectorIndex));
    if (_pfOnFatalError != NULL) {
      FS_NAND_FATAL_ERROR_INFO FatalErrorInfo;
      int Action;

      FatalErrorInfo.Unit = pInst->Unit;
      Action = _pfOnFatalError(&FatalErrorInfo);
      if (Action) {
        return;       // Application chose to ignore the fatal error.
      }
    }
    FS_DEBUG_ERROROUT((FS_MTYPE_DRIVER, "NAND_UNI: Switching permanently to read-only mode.\n"));
    //
    // Save the write protected status and the error information into the first block
    //
    pPageBuffer = (U8 *)_pSectorBuffer;
    FS_MEMSET(pPageBuffer, 0xFF, pInst->BytesPerPage);
    FS_StoreU16BE(pPageBuffer + INFO_OFF_IS_WRITE_PROTECTED, 0);      // Inverted, 0 means write protected
    FS_StoreU16BE(pPageBuffer + INFO_OFF_HAS_FATAL_ERROR,    0);      // Inverted, 0 means has fatal error
    FS_StoreU16BE(pPageBuffer + INFO_OFF_FATAL_ERROR_TYPE,         ErrorType);
    FS_StoreU32BE(pPageBuffer + INFO_OFF_FATAL_ERROR_SECTOR_INDEX, ErrorSectorIndex);
    _ClearStaticSpareArea(pInst);
    _WriteSectorWithECC(pInst, _pSectorBuffer, ERROR_INFO_SECTOR_INDEX);
  }
}

/*********************************************************************
*
*       _ReadSectorWithECC
*
*   Function description
*     Performs the following:
*     - Reads the data of a page into the specified buffer and spare area into the static buffer
*     - Performs error correction on the data
*
*   Parameters
*     pInst         [IN]  Driver instance
*                   [OUT] ---
*     pBuffer       [IN]  ---
*                   [OUT] Contents of the read sector
*     SectorIndex   Index of the sector to read
*
*   Return value
*     NAND_NO_ERROR             OK
*     NAND_BITS_CORRECTED       OK
*     NAND_ERROR_IN_ECC         OK
*     NAND_UNCORRECTABLE_ERROR  Error
*     NAND_READ_ERROR           Error
*
*   Notes
*       Before the function call, the static spare area needs to contain info for the page!
*/
static int _ReadSectorWithECC(NAND_INST * pInst, U32 * pBuffer, U32 SectorIndex) {
  int      r;
  int      NumRetries;
  unsigned BytesPerPage;
  unsigned BytesPerSpareArea;

  NumRetries = FS_NAND_NUM_READ_RETRIES;
  while (1) {
    //
    // Read data and the entire spare area.
    //
    BytesPerPage      = pInst->BytesPerPage;
    BytesPerSpareArea = pInst->BytesPerSpareArea;
    r = _ReadDataSpare(pInst, SectorIndex, pBuffer, BytesPerPage, _pSpareAreaData, BytesPerSpareArea);
    if (r) {
      r = NAND_READ_ERROR;
      goto Retry;                     // Re-read the sector in case of an error.
    }
    //
    // If the NAND flash has an ECC engine and it is active no data correction is required.
    //
    if (pInst->UseHW_ECC) {           // Bit correction performed by the NAND flash ?
      return NAND_NO_ERROR;
    }
    //
    // Check and correct bit errors of data and spare area.
    //
    r = _ApplyECC(pInst, pBuffer, _pSpareAreaData);
    if ((r == NAND_NO_ERROR) || (r == NAND_BITS_CORRECTED)) {
      return r;                       // OK, data has no errors.
    }
Retry:
    if (NumRetries-- == 0) {
      break;                          // No more retries.
    }
    IF_STATS(pInst->StatCounters.NumReadRetries++);
  }
  if ((r == NAND_READ_ERROR) || (r == NAND_UNCORRECTABLE_ERROR)) {
    FS_DEBUG_ERROROUT((FS_MTYPE_DRIVER, "NAND_UNI: FATAL error: Could not read sector with ECC.\n"));
    _OnFatalError(pInst, r, SectorIndex);
  }
  return r;
}

/*********************************************************************
*
*       _ReadEraseCnt
*
*   Function description
*     Reads the erase count of a block from the spare area of the first page.
*
*   Return values
*     ==0   OK
*     !=0   An error occurred
*/
static int _ReadEraseCnt(NAND_INST * pInst, unsigned BlockIndex, U32 * pEraseCnt) {
  int r;
  U32 SectorIndex0;
  U32 EraseCnt;
  int Result;

  //
  // The erase count is stored in the sparea area of the first page in the block.
  //
  Result       = 1;
  EraseCnt     = ERASE_CNT_INVALID;
  SectorIndex0 = _BlockIndex2SectorIndex0(pInst, BlockIndex);
  r = _ReadSectorWithECC(pInst, _pSectorBuffer, SectorIndex0);
  if ((r == NAND_NO_ERROR) || (r == NAND_BITS_CORRECTED) || (r == NAND_ERROR_IN_ECC)) {
    EraseCnt = _LoadEraseCnt(pInst);
    Result = 0;
  }
  *pEraseCnt = EraseCnt;
  return Result;
}

/*********************************************************************
*
*       _WriteEraseCnt
*
*   Function description
*     Writes the erase count of a block to the spare area of the first page.
*
*   Return values
*     ==0   OK
*     !=0   An error occurred
*
*   Note: Overwrites the contents of the stector buffer and of the static spare area.
*/
static int _WriteEraseCnt(NAND_INST * pInst, unsigned BlockIndex, U32 EraseCnt) {
  U32 SectorIndex0;

  SectorIndex0 = _BlockIndex2SectorIndex0(pInst, BlockIndex);
  FS_MEMSET(_pSectorBuffer, 0xFF, pInst->BytesPerPage);
  _ClearStaticSpareArea(pInst);
  _StoreEraseCnt(pInst, EraseCnt);
  return _WriteSectorWithECC(pInst, _pSectorBuffer, SectorIndex0);
}

/*********************************************************************
*
*       _ReadBlockCnt
*
*   Function description
*     Reads the data count of a block from the spare area of the second page.
*
*   Return values
*     ==0   OK
*     !=0   An error occurred
*/
static int _ReadBlockCnt(NAND_INST * pInst, unsigned BlockIndex, unsigned * pBlockCnt) {
  int      r;
  U32      SectorIndex;
  unsigned BlockCnt;
  int      Result;

  Result      = 1;
  BlockCnt    = 0;
  SectorIndex = _BlockIndex2SectorIndex0(pInst, BlockIndex);
  //
  // The block count is stored in the sparea area of the second sector in the block.
  //
  ++SectorIndex;
  r = _ReadSectorWithECC(pInst, _pSectorBuffer, SectorIndex);
  if ((r == NAND_NO_ERROR) || (r == NAND_BITS_CORRECTED) || (r == NAND_ERROR_IN_ECC)) {
    BlockCnt = _LoadBlockCnt(pInst);
    Result   = 0;
  }
  *pBlockCnt = BlockCnt;
  return Result;
}

/*********************************************************************
*
*       _ReadSectorStat
*
*   Function description
*     Reads the status of the data in the sector.
*
*   Return values
*     ==0   OK
*     !=0   An error occurred
*/
static int _ReadSectorStat(NAND_INST * pInst, U32 SectorIndex, unsigned * pSectorStat) {
  int      r;
  unsigned SectorStat;
  int      Result;

  Result      = 1;
  SectorStat  = SECTOR_STAT_EMPTY;
  r = _ReadSectorWithECC(pInst, _pSectorBuffer, SectorIndex);
  if ((r == NAND_NO_ERROR) || (r == NAND_BITS_CORRECTED) || (r == NAND_ERROR_IN_ECC)) {
    SectorStat = _LoadSectorStat(pInst);
    Result     = 0;
  }
  *pSectorStat = SectorStat;
  return Result;
}

/*********************************************************************
*
*       _BlockIsBad
*
*   Function description
*     Checks whether a block can be used to store data.
*     The good/bad status is read from the spare area of the first and second page.
*     If required, the HW ECC is disabled to avoid ECC errors since the block status is not protected by ECC.
*
*   Return values
*     ==0   Block is good
*     !=0   Block is defect
*/
static int _BlockIsBad(NAND_INST * pInst, unsigned BlockIndex) {
  int      r;
  U8       Data8;
  U32      PageIndex;
  unsigned BlockStat0;
  unsigned BlockStat1;

  BlockStat0 = BLOCK_STAT_BAD;
  BlockStat1 = BLOCK_STAT_BAD;
  _DisableHW_ECCIfRequired(pInst);          // Temporarily disable the HW ECC during the data transfer.
  PageIndex = (U32)BlockIndex << pInst->PPB_Shift;
  r = _ReadPhySpareByte(pInst, PageIndex, &Data8, 0);
  if (r == 0) {
    BlockStat0 = _CorrectBlockStatIfRequired(Data8);
  }
  //
  // Samsung marks a block as defective by setting to 0 the first byte/half-word
  // of the spare area of the first or second page.
  //
  if (BlockStat0 == BLOCK_STAT_GOOD) {
    r = _ReadPhySpareByte(pInst, PageIndex + 1, &Data8, 0);
    if (r == 0) {
      BlockStat1 = _CorrectBlockStatIfRequired(Data8);
    }
  }
  _EnableHW_ECCIfRequired(pInst);           // Re-enable HW ECC when needed.
  if ((BlockStat0 == BLOCK_STAT_GOOD) && (BlockStat1 == BLOCK_STAT_GOOD)) {
    return 0;                               // Block can be used to store data.
  }
  return 1;                                 // Block is bad.
}

/*********************************************************************
*
*       _MarkBlockAsBad
*
*   Function description:
*     The first byte of the spare area in each block is used to mark the block as bad.
*     If it is != 0xFF, then this block will not be used any more.
*
*   Parameters
*     pInst           [IN]  Driver instance
*                     [OUT] ---
*     BlockIndex      Index of the block to mark as "bad"
*     ErrorType       Reason why the block is marked as "bad"
*     ErrorBRSI       Index of the physical sector where the error occurred
*/
static int _MarkBlockAsBad(NAND_INST * pInst, unsigned BlockIndex, int ErrorType, unsigned ErrorBRSI) {
  U32        SectorIndex;
  U8       * pSpare;
  U8       * pData;
  unsigned   BytesPerPage;
  unsigned   BytesPerSpareArea;
  int        r;

  IF_STATS(pInst->StatCounters.NumBadBlocks++);
  SectorIndex = _BlockIndex2SectorIndex0(pInst, BlockIndex);
  //
  // We write "SEGGER" on the spare area of the block to be able to distinguish it from the "bad" blocks marked by manufacturer.
  // Additional information about the reason why the block was marked as "bad" is saved on the data area of the first page.
  //
  _EraseBlock(pInst, BlockIndex);
  _ClearStaticSpareArea(pInst);
  pData = (U8 *)_pSectorBuffer;
  FS_MEMSET(pData, 0xFF, pInst->BytesPerPage);
  FS_StoreU16BE(pData + INFO_OFF_BAD_MARK_ERROR_TYPE, ErrorType);
  FS_StoreU16BE(pData + INFO_OFF_BAD_MARK_ERROR_BRSI, ErrorBRSI);
  _StoreBlockStat(pInst, BLOCK_STAT_BAD);
  pSpare  = _pSpareAreaData;
  pSpare += SPARE_OFF_BLOCK_STAT + 1;   // Reserve place for the bad block marking.
  FS_MEMCPY(pSpare, _acInfo, 6);        // Store only the letters without the 0 padding at the end.
  BytesPerPage      = pInst->BytesPerPage;
  BytesPerSpareArea = pInst->BytesPerSpareArea;
  //
  // It is not required not write data with ECC since the information
  // whether a block is bad or not is always read with ECC disabled.
  //
  _DisableHW_ECCIfRequired(pInst);
  r = _WriteDataSpare(pInst, SectorIndex, _pSectorBuffer, BytesPerPage, _pSpareAreaData, BytesPerSpareArea);
  _EnableHW_ECCIfRequired(pInst);
  return r;
}

/*********************************************************************
*
*       _L2P_Read
*
*  Function Description
*    Returns the contents of the given entry in the L2P table (physical block lookup table)
*
*/
static unsigned _L2P_Read(const NAND_INST * pInst, U32 LogIndex) {
  U32 v;

  v = FS_BITFIELD_ReadEntry(pInst->pLog2PhyTable, LogIndex, pInst->NumBitsPhyBlockIndex);
  return v;
}

/*********************************************************************
*
*       _L2P_Write
*
*  Function Description
*    Updates the contents of the given entry in the L2P table (physical block lookup table)
*
*/
static void _L2P_Write(const NAND_INST * pInst, U32 LogIndex, unsigned v) {
  FS_BITFIELD_WriteEntry(pInst->pLog2PhyTable, LogIndex, pInst->NumBitsPhyBlockIndex, v);
}

/*********************************************************************
*
*       _L2P_GetSize
*
*  Function Description
*    Computes & returns the size of the L2P assignment table of a work block.
*    Is used before allocation to find out how many bytes need to be allocated.
*/
static unsigned _L2P_GetSize(const NAND_INST * pInst) {
  unsigned v;

  v = FS_BITFIELD_CalcSize(pInst->NumLogBlocks, pInst->NumBitsPhyBlockIndex);
  return v;
}

/*********************************************************************
*
*       _WB_ReadAssignment
*
*  Function Description
*    Reads an entry in the assignment table of a work block.
*    It is necessary to use a subroutine to do the job since the entries are stored in a bitfield.
*    Logically, the code does the following:
*      return pWorkBlock->aAssign[Index];
*/
static unsigned _WB_ReadAssignment(const NAND_INST * pInst, const WORK_BLOCK_DESC * pWorkBlock, unsigned Index) {
  unsigned r;

  r = FS_BITFIELD_ReadEntry((const U8 *)pWorkBlock->paAssign, Index, pInst->PPB_Shift);
  return r;
}

/*********************************************************************
*
*       _WB_WriteAssignment
*
*   Function description
*     Writes an entry in the assignment table of a work block.
*     It is necessary to use a subroutine to do the job since the entries are stored in a bitfield.
*     Logically, the code does the following:
*       pWorkBlock->aAssign[Index] = v;
*/
static void _WB_WriteAssignment(const NAND_INST * pInst, const WORK_BLOCK_DESC * pWorkBlock, unsigned Index, unsigned v) {
  FS_BITFIELD_WriteEntry((U8 *)pWorkBlock->paAssign, Index, pInst->PPB_Shift, v);
}

/*********************************************************************
*
*       _WB_GetAssignmentSize
*
*  Function Description
*    Returns the size of the assignment table of a work block.
*/
static unsigned _WB_GetAssignmentSize(const NAND_INST * pInst) {
  unsigned v;

  v = FS_BITFIELD_CalcSize(1 << pInst->PPB_Shift, pInst->PPB_Shift);
  return v;
}

/*********************************************************************
*
*       _WB_RemoveFromList
*
*  Function Description
*    Removes a given work block from list of work blocks.
*/
static void _WB_RemoveFromList(const WORK_BLOCK_DESC * pWorkBlock, WORK_BLOCK_DESC ** ppFirst) {
  //
  // Unlink Front: From head or previous block
  //
  if (pWorkBlock == *ppFirst) {           // This WB first in list ?
    *ppFirst = pWorkBlock->pNext;
  } else {
    pWorkBlock->pPrev->pNext = pWorkBlock->pNext;
  }
  //
  // Unlink next if pNext is valid
  //
  if (pWorkBlock->pNext) {
    pWorkBlock->pNext->pPrev = pWorkBlock->pPrev;
  }
}

/*********************************************************************
*
*       _WB_AddToList
*
*  Function Description
*    Adds a given work block to the beginning of the list of work block descriptors.
*/
static void _WB_AddToList(WORK_BLOCK_DESC * pWorkBlock, WORK_BLOCK_DESC ** ppFirst) {
  WORK_BLOCK_DESC * pPrevFirst;

  pPrevFirst = *ppFirst;
  pWorkBlock->pPrev = NULL;    // First entry
  pWorkBlock->pNext = pPrevFirst;
  if (pPrevFirst) {
    pPrevFirst->pPrev = pWorkBlock;
  }
  *ppFirst = pWorkBlock;
}

/*********************************************************************
*
*       _WB_RemoveFromUsedList
*
*  Function Description
*    Removes a given work block from list of used work blocks.
*/
static void _WB_RemoveFromUsedList(NAND_INST * pInst, const WORK_BLOCK_DESC * pWorkBlock) {
  _WB_RemoveFromList(pWorkBlock, &pInst->pFirstWorkBlockInUse);
}

/*********************************************************************
*
*       _WB_AddToUsedList
*
*  Function Description
*    Adds a given work block to the list of used work blocks.
*/
static void _WB_AddToUsedList(NAND_INST * pInst, WORK_BLOCK_DESC * pWorkBlock) {
  _WB_AddToList(pWorkBlock, &pInst->pFirstWorkBlockInUse);
}

/*********************************************************************
*
*       _WB_RemoveFromFreeList
*
*  Function Description
*    Removes a given work block from list of free work blocks.
*/
static void _WB_RemoveFromFreeList(NAND_INST * pInst, const WORK_BLOCK_DESC * pWorkBlock) {
  _WB_RemoveFromList(pWorkBlock, &pInst->pFirstWorkBlockFree);
}

/*********************************************************************
*
*       _WB_AddToFreeList
*
*  Function Description
*    Adds a given work block to the list of free work blocks.
*/
static void _WB_AddToFreeList(NAND_INST * pInst, WORK_BLOCK_DESC * pWorkBlock) {
  _WB_AddToList(pWorkBlock, &pInst->pFirstWorkBlockFree);
}

/*********************************************************************
*
*       _WB_HasValidSectors
*
*   Function description
*     Checks whether the work block contains at least one sector with valid data in it.
*/
static unsigned _WB_HasValidSectors(const NAND_INST * pInst, const WORK_BLOCK_DESC * pWorkBlock) {
  unsigned SectorsPerBlock;
  unsigned iSector;
  unsigned brsiPhy;

  SectorsPerBlock = 1 << pInst->PPB_Shift;
  //
  // Check the sectors in the work block one by one.
  //
  for (iSector = 1; iSector < SectorsPerBlock; ++iSector) {
    //
    // The assigment table tell us if a sector has valid data or not.
    //
    brsiPhy = _WB_ReadAssignment(pInst, pWorkBlock, iSector);
    if (brsiPhy) {
      return 1;     // Found one sector with valid data. Done.
    }
  }
  return 0;         // No sector with valid data found in the work block.
}

/*********************************************************************
*
*       _AllocWorkBlockDesc
*
*  Function description
*    Allocates a WORK_BLOCK_DESC from the array in the pInst structure.
*/
static WORK_BLOCK_DESC * _AllocWorkBlockDesc(NAND_INST * pInst, unsigned lbi) {
  WORK_BLOCK_DESC * pWorkBlock;

  //
  // Check if a free block is available.
  //
  pWorkBlock = pInst->pFirstWorkBlockFree;
  if (pWorkBlock) {
    unsigned NumBytes;
    //
    // Initialize work block descriptor, mark it as in use and add it to the list.
    //
    NumBytes = _WB_GetAssignmentSize(pInst);
    _WB_RemoveFromFreeList(pInst, pWorkBlock);
    _WB_AddToUsedList(pInst, pWorkBlock);
    pWorkBlock->lbi      = lbi;
    pWorkBlock->brsiFree = BLOCK_INFO_BRSI;
    FS_MEMSET(pWorkBlock->paAssign, 0, NumBytes);   // Make sure that no old assignment info from previous descriptor is in the table.
  }
  return pWorkBlock;
}

/*********************************************************************
*
*       _ClearBlock
*
*   Function description
*     Erases a block and writes the erase count to first sector.
*
*   Parameters
*     pInst       [IN]  Driver instance
*                 [OUT] ---
*     BlockIndex  Number of the block to clear
*     EraseCnt    Actual erase count of the block. This value is incremented and written to sector 0.
*
*   Return value
*     ==0   OK, block can be used to write data to it
*     !=0   An error occurred
*/
static int _ClearBlock(NAND_INST * pInst, unsigned BlockIndex, U32 EraseCnt) {
  int r;

  r = _EraseBlock(pInst, BlockIndex);
  if (r) {
    _MarkBlockAsBad(pInst, BlockIndex, NAND_ERASE_ERROR, 0);
    return 1;       // Error, erase operation failed.
  }
  ++EraseCnt;
  r = _WriteEraseCnt(pInst, BlockIndex, EraseCnt);
  if (r) {
    _MarkBlockAsBad(pInst, BlockIndex, NAND_WRITE_ERROR, 0);
    return 1;       // Error, write operation failed.
  }
  _MarkBlockAsFree(pInst, BlockIndex);
  return 0;         // OK, block prepared for write.
}

/*********************************************************************
*
*       _PageIsBlank
*
*   Function description
*     Checks whether all bytes in page including the spare area are set to 0xFF.
*     The data is read without ECC since the read routine will switch the NAND flash
*     to read-only mode if an uncorrectable bit error is encountered.
*     It is possible that bit errors are present on the page (that is bits are set to 0).
*     The routine counts them and if the number is smaller than the number of bit errors
*     the ECC can correct the page is considered blank.
*/
static int _PageIsBlank(NAND_INST * pInst, U32 SectorIndex) {
  int        r;
  unsigned   BytesPerPage;
  unsigned   BytesPerSpareArea;
  U32      * p;
  unsigned   NumItems;
  unsigned   NumBits0;
  U32        Data32;
  U8         NumBitsCorrectable;

  BytesPerPage      = pInst->BytesPerPage;
  BytesPerSpareArea = pInst->BytesPerSpareArea;
  _DisableHW_ECCIfRequired(pInst);           // Temporarily disable the HW ECC during the data transfer.
  r = _ReadDataSpare(pInst, SectorIndex, _pSectorBuffer, BytesPerPage, _pSpareAreaData, BytesPerSpareArea);
  _EnableHW_ECCIfRequired(pInst);
  if (r) {
    return 0;                 // Error, return that the page is not empty.
  }
  //
  // Check if the data area is blank.
  //
  p        = _pSectorBuffer;
  NumItems = BytesPerPage >> 2;
  NumBits0 = 0;
  do {
    Data32 = *p++;
    if (Data32 != 0xFFFFFFFFuL) {
      NumBits0 += _Count0Bits(Data32);
    }
  } while (--NumItems);
  //
  // Check if the spare area is blank.
  //
  p        = (U32 *)_pSpareAreaData;
  NumItems = BytesPerSpareArea >> 2;
  do {
    Data32 = *p++;
    if (Data32 != 0xFFFFFFFFuL) {
      NumBits0 += _Count0Bits(Data32);
    }
  } while (--NumItems);
  NumBitsCorrectable = pInst->NumBitsCorrectable;
  if (NumBits0 > NumBitsCorrectable) {
    return 0;                 // Data or spare area is not blank.
  }
  return 1;                   // Page is empty.
}

/*********************************************************************
*
*       _MakeBlockAvailable
*
*   Function description
*     - Erases a block and stores the erase count to first sector
*     - Marks the block as free in the pFreeMap
*     - If required, updates management info of wear leveling
*
*   Parameters
*     pInst       [IN]  Driver instance
*                 [OUT] ---
*     BlockIndex  Index of the physical block
*     BlockType   Type of the block
*     EraseCnt    Number of times the block has been erased
*/
static void _MakeBlockAvailable(NAND_INST * pInst, unsigned BlockIndex, unsigned BlockType, U32 EraseCnt) {
  int r;

  if (EraseCnt == ERASE_CNT_INVALID) {
    r = _ReadEraseCnt(pInst, BlockIndex, &EraseCnt);
    if (r) {
      EraseCnt = pInst->EraseCntMax;
    }
  }
  _ClearBlock(pInst, BlockIndex, EraseCnt);
  //
  // Wear leveling keeps track of the number of data blocks with a minimum EraseCnt.
  // We update this information here if such a data block has been erased.
  //
  if (BlockType == BLOCK_TYPE_DATA) {
    if (pInst->NumBlocksEraseCntMin && (pInst->EraseCntMin == EraseCnt)) {
      pInst->NumBlocksEraseCntMin--;
    }
  }
}

/*********************************************************************
*
*       _CopySectorWithECC
*
*   Function description
*     Copies the data of a sector into another sector. During copy
*     the ECC of the source data is checked.
*
*   Parameters
*     pInst             [IN]  Driver instance
*                       [OUT] ---
*     SectorIndexSrc    Source physical sector index
*     SectorIndexDest   Destination physical sector index
*
*   Return value
*     NAND_NO_ERROR             OK
*     NAND_BITS_CORRECTED       OK
*     NAND_ERROR_IN_ECC         OK
*     NAND_UNCORRECTABLE_ERROR  Error, fatal
*     NAND_READ_ERROR           Error, fatal
*     NAND_WRITE_ERROR          Error, recoverable
*/
static int _CopySectorWithECC(NAND_INST * pInst, U32 SectorIndexSrc, U32 SectorIndexDest) {
  int      r;
  int      Result;
  unsigned BytesPerPage;
  unsigned BytesPerSpareArea;

  //
  // Read the data and the spare area of the source sector and correct the bit errors.
  //
  r = _ReadSectorWithECC(pInst, _pSectorBuffer, SectorIndexSrc);
  if ((r == NAND_NO_ERROR) || (r == NAND_BITS_CORRECTED) || (r == NAND_ERROR_IN_ECC)) {
    Result = r;
    //
    // Check if the file system marked the data of this sector as invalid. In this case the sector is not copied.
    //
    if (_LoadSectorStat(pInst) == SECTOR_STAT_EMPTY) {
      return NAND_NO_ERROR;       // OK, sector data not valid. Nothing to copy.
    }
    //
    // A bit error in ECC is not corrected by the ECC check routine.
    // It must be re-computed to avoid propagating the bit error to destination sector.
    //
    if (r == NAND_ERROR_IN_ECC) {
      r = _WriteSectorWithECC(pInst, _pSectorBuffer, SectorIndexDest);
    } else {
      BytesPerPage      = pInst->BytesPerPage;
      BytesPerSpareArea = pInst->BytesPerSpareArea;
      r = _WriteDataSpare(pInst, SectorIndexDest, _pSectorBuffer, BytesPerPage, _pSpareAreaData, BytesPerSpareArea);
    }
    if (r) {
      return NAND_WRITE_ERROR;    // Error, data could not be written.
    }
    IF_STATS(pInst->StatCounters.CopySectorCnt++);
    return Result;
  }
  return r;                       // Fatal error, bits could not be corrected
}

/*********************************************************************
*
*       _CountDataBlocksWithEraseCntMin
*
*   Function description
*     Goes through all blocks and counts the data blocks with the
*     lowest erase cnt.
*
*   Parameters
*     pInst       [IN]  Driver instance
*                 [OUT] ---
*     pEraseCnt   [IN]  ---
*                 [OUT] Minimum erase count
*     pPBI        [IN]  ---
*                 [OUT] Index of the first data block with the min erase count
*
*   Return value
*     Number of data blocks found with a min erase count
*/
static U32 _CountDataBlocksWithEraseCntMin(NAND_INST * pInst, U32 * pEraseCnt, unsigned * pPBI) {
  unsigned iBlock;
  unsigned pbi;
  U32      EraseCntMin;
  U32      SectorIndex;
  U32      EraseCnt;
  U32      NumBlocks;
  unsigned BlockType;
  int      r;

  pbi         = 0;
  EraseCntMin = ERASE_CNT_INVALID;
  NumBlocks   = 0;
  //
  // Check all allocated blocks.
  //
  for (iBlock = STORAGE_START_BLOCK_INDEX; iBlock < pInst->NumPhyBlocks; ++iBlock) {
    //
    // Consider only blocks which contain valid data.
    //
    if (_BlockIsFree(pInst, iBlock)) {
      continue;
    }
    //
    // Skip blocks marked as defect.
    //
    if (_BlockIsBad(pInst, iBlock)) {
      continue;
    }
    //
    // Found a block which is in use. Get the erase count and the block type from the second page of the block.
    //
    SectorIndex = _BlockIndex2SectorIndex0(pInst, iBlock);
    ++SectorIndex;
    r = _ReadSectorWithECC(pInst, _pSectorBuffer, SectorIndex);
    if ((r == NAND_NO_ERROR) || (r == NAND_BITS_CORRECTED) || (r == NAND_ERROR_IN_ECC)) {
      BlockType = _LoadBlockType(pInst);
      //
      // OK, block information has been read form the sparea.
      //
      if (BlockType == BLOCK_TYPE_DATA) {
        EraseCnt = _LoadEraseCnt(pInst);
        if ((EraseCntMin == ERASE_CNT_INVALID) || (EraseCnt < EraseCntMin)) {
          pbi         = iBlock;
          EraseCntMin = EraseCnt;
          NumBlocks   = 1;
        } else if (EraseCnt == EraseCntMin) {
          ++NumBlocks;
        }
      }
    }
  }
  *pEraseCnt = EraseCntMin;
  *pPBI      = pbi;
  return NumBlocks;
}

/*********************************************************************
*
*       _FindDataBlockByEraseCnt
*
*   Function description
*     Goes through all data blocks and returns the first one with the given erase count.
*
*   Parameters
*     pInst     [IN]  Driver instance
*               [OUT] ---
*     EraseCnt  Erase count to lookup for
*
*   Return value
*     == 0  No data block found
*     != 0  Index of the found data block
*/
static unsigned _FindDataBlockByEraseCnt(NAND_INST * pInst, U32 EraseCnt) {
  unsigned iBlock;
  U32      SectorIndex;
  unsigned BlockType;
  int      r;
  U32      EraseCntData;

  //
  // Check all allocated blocks.
  //
  for (iBlock = STORAGE_START_BLOCK_INDEX; iBlock < pInst->NumPhyBlocks; ++iBlock) {
    //
    // Consider only blocks which contain valid data.
    //
    if (_BlockIsFree(pInst, iBlock)) {
      continue;
    }
    //
    // Skip blocks marked as defect.
    //
    if (_BlockIsBad(pInst, iBlock)) {
      continue;
    }
    //
    // Found a block which is in use. Get the erase count and the block type from the second page of the block.
    //
    SectorIndex = _BlockIndex2SectorIndex0(pInst, iBlock);
    ++SectorIndex;
    r = _ReadSectorWithECC(pInst, _pSectorBuffer, SectorIndex);
    if ((r == NAND_NO_ERROR) || (r == NAND_BITS_CORRECTED) || (r == NAND_ERROR_IN_ECC)) {
      BlockType = _LoadBlockType(pInst);
      //
      // OK, block information has been read form the sparea.
      //
      if (BlockType == BLOCK_TYPE_DATA) {
        EraseCntData = _LoadEraseCnt(pInst);
        if (EraseCnt == EraseCntData) {
          return iBlock;
        }
      }
    }
  }
  return 0;     // No data block found with the given erase count.
}

/*********************************************************************
*
*       _CheckActiveWearLeveling
*
*   Function description
*     Checks if it is time to perform active wear leveling by
*     comparing the given erase count to the lowest erase count.
*     If so (difference is too big), the index of the data block with the lowest erase count is returned.
*
*   Parameters
*     pInst           [IN]  Driver instance
*                     [OUT] ---
*     EraseCnt        Erase count of block to be erased
*     pEraseCntData   [IN]  ---
*                     [OUT] Erase count of the data block
*
*   Return value
*     ==0   No data block found
*     !=0   Physical block index of the data block found
*/
static unsigned _CheckActiveWearLeveling(NAND_INST * pInst, U32 EraseCnt, U32 * pEraseCntData) {
  unsigned pbi;
  I32      EraseCntDiff;
  U32      NumBlocks;
  U32      EraseCntMin;

  //
  // Update pInst->EraseCntMin if necessary.
  //
  pbi         = 0;
  NumBlocks   = pInst->NumBlocksEraseCntMin;
  EraseCntMin = pInst->EraseCntMin;
  if (NumBlocks == 0) {
    NumBlocks = _CountDataBlocksWithEraseCntMin(pInst, &EraseCntMin, &pbi);
    if (NumBlocks == 0) {
      return 0;     // We don't have any data block yet, it can happen if the flash is empty
    }
    pInst->EraseCntMin          = EraseCntMin;
    pInst->NumBlocksEraseCntMin = NumBlocks;
  }
  //
  // Check if the treshold for active wear leveling is reached
  //
  EraseCntDiff = EraseCnt -  EraseCntMin;
  if (EraseCntDiff < (I32)pInst->MaxEraseCntDiff) {
    return 0;       // Active wear leveling not necessary, EraseCntDiff is not big enough yet.
  }
  if (pbi == 0) {
    pbi = _FindDataBlockByEraseCnt(pInst, EraseCntMin);
  }
  *pEraseCntData = EraseCntMin;
  --pInst->NumBlocksEraseCntMin;
  return pbi;
}

/*********************************************************************
*
*       _PerformPassiveWearLeveling
*
*   Function description
*     Searches for the next free block and returns its index. The block
*     is marked as allocated in the internal list.
*
*   Parameters
*     pInst       [IN]  Driver instance
*                 [OUT] ---
*     pEraseCnt   [IN]  ---
*                 [OUT] Erase count of the allocated block
*
*   Return value
*     == 0  No more free blocks
*     != 0  Physical block index of the allocated block
*/
static unsigned _PerformPassiveWearLeveling(NAND_INST * pInst, U32 * pEraseCnt) {
  unsigned i;
  unsigned iBlock;
  U32      EraseCnt;
  int      r;
  U32      SectorIndex0;

  //
  // Search for a block we can write to.
  //
  iBlock = pInst->MRUFreeBlock;
  for (i = 0; i < pInst->NumPhyBlocks; i++) {
    if (++iBlock >= pInst->NumPhyBlocks) {
      iBlock = STORAGE_START_BLOCK_INDEX;
    }
    if (_BlockIsFree(pInst, iBlock)) {
      r = 0;
      //
      // The function must return the erase count of the block, so we read it here.
      //
      _ReadEraseCnt(pInst, iBlock, &EraseCnt);
      //
      // An invalid erase count indicates that the block information is invalid.
      // There are 2 reasons for this:
      //   - Block is blank
      //   - Bit errors where detected and the ECC could not correct them
      //
      if (EraseCnt == ERASE_CNT_INVALID) {
        EraseCnt = pInst->EraseCntMax;
        //
        // Erase the block only if needed by checking if the first page including spare area is empty.
        //
        SectorIndex0 = _BlockIndex2SectorIndex0(pInst, iBlock);
        if (_PageIsBlank(pInst, SectorIndex0)) {
          //
          // Store the erase count so that the block is recognized as free at low-level mount.
          //
          r = _WriteEraseCnt(pInst, iBlock, EraseCnt);
          if (r) {
            _MarkBlockAsBad(pInst, iBlock, NAND_WRITE_ERROR, 0);
            _MarkBlockAsAllocated(pInst, iBlock);       // This block can not be used for anything.
            continue;
          }
        } else {
          //
          // Erase and store the erase count.
          //
          r = _ClearBlock(pInst, iBlock, EraseCnt);
          if (r) {
            continue;                                   // Error, could not write erase count.
          }
        }
      }
      *pEraseCnt = EraseCnt;
      _MarkBlockAsAllocated(pInst, iBlock);
      pInst->MRUFreeBlock = iBlock;
      return iBlock;
    }
  }
  return 0;               // Error, no more free blocks
}

/*********************************************************************
*
*       _MoveDataBlock
*
*   Function description
*     Copies the contents of a data block into another block.
*     The souce data block is marked as free.
*
*   Parameters
*     pInst         [IN]  Driver instance
*                   [OUT] ---
*     pbiSrc        Index of the block to be copied
*     pbiDest       Index of the block where to copy
*     EraseCntDest  Erase count of the destination block. Will be saved to sector 1 of the block together with the block information.
*     pErrorBRSI    [IN]  ---
*                   [OUT] Block relative index of the sector where the error occurred
*
*   Return value
*     NAND_NO_ERROR             OK
*     NAND_ERROR_IN_ECC         OK
*     NAND_UNCORRECTABLE_ERROR  Error, fatal
*     NAND_READ_ERROR           Error, fatal
*     NAND_WRITE_ERROR          Error, recoverable
*/
static int _MoveDataBlock(NAND_INST * pInst, unsigned pbiSrc, unsigned pbiDest, U32 EraseCntDest, unsigned * pErrorBRSI) {
  unsigned SectorsPerBlock;
  unsigned iSector;
  U32      SectorIndexSrc0;
  U32      SectorIndexDest0;
  int      r;
  unsigned BlockCntSrc;
  unsigned lbi;
  U32      EraseCntSrc;
  unsigned ErrorInECC;

  *pErrorBRSI      = 0;
  ErrorInECC       = 0;
  SectorIndexSrc0  = _BlockIndex2SectorIndex0(pInst, pbiSrc);
  SectorIndexDest0 = _BlockIndex2SectorIndex0(pInst, pbiDest);
  SectorsPerBlock  = 1 << pInst->PPB_Shift;
  //
  // Read the block related info from the second sector of source block.
  // LBI and BlockCnt are required for the destination block.
  //
  iSector = BLOCK_INFO_BRSI;
  r = _ReadSectorWithECC(pInst, _pSectorBuffer, SectorIndexSrc0 + iSector);
  if ((r == NAND_NO_ERROR) || (r == NAND_BITS_CORRECTED) || (r == NAND_ERROR_IN_ECC)) {
    BlockCntSrc  = _LoadBlockCnt(pInst);
    lbi          = _LoadLBI(pInst);
    EraseCntSrc  = _LoadEraseCnt(pInst);
    _StoreBlockCnt(pInst, BlockCntSrc + 1);   // BlockCnt helps low-level mount to detect the most recent version of a duplicated data block.
    _StoreEraseCnt(pInst, EraseCntDest);
    if (r == NAND_ERROR_IN_ECC) {
      *pErrorBRSI = iSector;
      ErrorInECC  = 1;                // Remember that we had an error in ECC.
    }
    //
    // Write data and spare area to second sector.
    //
    r = _WriteSectorWithECC(pInst, _pSectorBuffer, SectorIndexDest0 + iSector);
    if (r) {
      return NAND_WRITE_ERROR;        // Error: data and sparea area could not be written.
    }

    //
    // Fail-safe TP
    //
    // At this point we have 2 data blocks with the same LBI. The copy operation is not complete. 
    // Low-level mount should throw away the latest version of the data block.
    //

    //
    // Now copy the rest of the sectors.
    //
    ++iSector;
    for (; iSector < SectorsPerBlock; ++iSector) {
      r = _CopySectorWithECC(pInst, SectorIndexSrc0 + iSector, SectorIndexDest0 + iSector);
      if ((r == NAND_NO_ERROR) || (r == NAND_BITS_CORRECTED)) {
        continue;
      }
      if ((r == NAND_UNCORRECTABLE_ERROR) || (r == NAND_READ_ERROR) || (r == NAND_WRITE_ERROR)) {
        *pErrorBRSI = iSector;
        return r;
      }
      if (r == NAND_ERROR_IN_ECC) {
        *pErrorBRSI = iSector;
        ErrorInECC  = 1;            // Remember that we had an error in ECC.
      }
    }
    //
    // Update the mapping of logical to physical blocks.
    //
    _L2P_Write(pInst, lbi, pbiDest);
    //
    // Erase the block and put it in the free list if no errors in ECC.
    // In case of an error in ECC the block stays allocated and will be later marked as "bad".
    //
    if (ErrorInECC) {
      return NAND_ERROR_IN_ECC;
    } else {
      _MakeBlockAvailable(pInst, pbiSrc, BLOCK_TYPE_DATA, EraseCntSrc);
      return NAND_NO_ERROR;           // OK, the block has been copied.
    }
  }
  *pErrorBRSI = iSector;
  return r;                         // Error, bit error could not be corrected.
}

/*********************************************************************
*
*       _AllocErasedBlock
*
*   Function description
*     Selects a block to write data into. The returned block is erased.
*
*   Parameters
*     pInst       [IN]  Driver instance
*                 [OUT] ---
*     pEraseCnt   [IN]  ---
*                 [OUT] Erase count of the selected block
*
*   Return value
*     ==0   An error occurred
*     !=0   Physical block index
*/
static unsigned _AllocErasedBlock(NAND_INST * pInst, U32 * pEraseCnt) {
  unsigned pbiAlloc;
  unsigned pbiData;
  U32      EraseCntAlloc;
  U32      EraseCntData;
  int      r;
  unsigned ErrorBRSI;

  while (1) {
    //
    // Passive wear leveling. Get the next free block in the row
    //
    pbiAlloc = _PerformPassiveWearLeveling(pInst, &EraseCntAlloc);
    if (pbiAlloc == 0) {
      FS_DEBUG_ERROROUT((FS_MTYPE_DRIVER, "NAND_UNI: FATAL error: No more free blocks.\n"));
      _OnFatalError(pInst, NAND_OUT_OF_FREE_BLOCKS, 0);
      return 0;                   // Fatal error, out of free blocks
    }
    //
    // OK, we found a free block.
    // Now, let's check if the erase count is too high so we need to use active wear leveling.
    //
    pbiData = _CheckActiveWearLeveling(pInst, EraseCntAlloc, &EraseCntData);
    if (pbiData == 0) {
      *pEraseCnt = EraseCntAlloc; // No data block has an erase count low enough, keep the block allocated by passive wear leveling.
      return pbiAlloc;
    }
    //
    // Perform active wear leveling:
    // A block containing data has a much lower erase count. This block is now moved, giving us a free block with low erase count.
    // This procedure makes sure that blocks which contain data that does not change still take part in the the
    // wear leveling scheme.
    //
    r = _MoveDataBlock(pInst, pbiData, pbiAlloc, EraseCntAlloc, &ErrorBRSI);
    if (r == NAND_NO_ERROR) {
      //
      // The data has been moved and the data block is now free to use.
      // We have to mark the block as allocated here as _MoveDataBlock() made it free.
      //
      _MarkBlockAsAllocated(pInst, pbiData);
      //
      // Data block as been one time erased.
      //
      ++EraseCntData;
      *pEraseCnt = EraseCntData;
      return pbiData;
    }
    if ((r == NAND_UNCORRECTABLE_ERROR) || (r == NAND_READ_ERROR)) {
      _MarkBlockAsBad(pInst, pbiData, r, ErrorBRSI);
      return 0;                   // Fatal error, no way to recover
    }
    if (r == NAND_WRITE_ERROR) {
      _MarkBlockAsBad(pInst, pbiAlloc, r, ErrorBRSI);
      continue;                   // Error when writting into the allocated block
    }
    if (r == NAND_ERROR_IN_ECC) {
      _MarkBlockAsBad(pInst, pbiData, r, ErrorBRSI);
      continue;                   // Error in the ECC of the data block, get a new free block and a new data block.
    }
  }
}

/*********************************************************************
*
*       _RecoverDataBlock
*
*   Function Description
*     Copies a data block into a free block. Called typ. when an error is found in the ECC.
*
*   Parameters
*     pInst       [IN]  Driver instance
*                 [OUT] ---
*     pbiData     Index of the block to recover
*
*   Return value:
*     ==0   Data block saved
*     !=0   An error occurred
*
*/
static int _RecoverDataBlock(NAND_INST * pInst, unsigned pbiData) {
  unsigned pbiAlloc;
  U32      EraseCnt;
  int      r;
  unsigned ErrorBRSI;

  while (1) {
    //
    // Need a free block where to move the data of the damaged block
    //
    pbiAlloc = _AllocErasedBlock(pInst, &EraseCnt);
    if (pbiAlloc == 0) {
      return 1;               // Could not allocate an empty block, fatal error
    }
    r = _MoveDataBlock(pInst, pbiData, pbiAlloc, EraseCnt, &ErrorBRSI);
    if (r == NAND_ERROR_IN_ECC) {
      _MarkBlockAsBad(pInst, pbiData, r, ErrorBRSI);
      return 0;               // Data was recovered
    }
    if ((r == NAND_UNCORRECTABLE_ERROR) || (r == NAND_READ_ERROR)) {
      _MarkBlockAsBad(pInst, pbiData, r, ErrorBRSI);
      return 1;               // Fatal error, no way to recover
    }
    if (r == NAND_WRITE_ERROR) {
      _MarkBlockAsBad(pInst, pbiAlloc, r, ErrorBRSI);
      continue;               // Error when writting into the allocated block
    }
    if (r == NAND_NO_ERROR) {
      return 0;               // Data moved into the new block
    }
  }
}

/*********************************************************************
*
*       _ConvertWorkBlock
*
*   Function description
*     Converts a work block into a data block. The data of the work block
*     is "merged" with the data of the source block in another free block.
*     The merging operation copies sector-wise the data from work block
*     into the free block. If the sector data is invalid in the work block
*     the sector data from the source block is copied instead. The sectors
*     in the work block doesn't have to be on their native positions.
*
*   Parameters
*     pInst       [IN]  Driver instance
*                 [OUT] ---
*     pWorkBlock  [IN]  Work block to convert
*                 [OUT] ---
*     brsiToSkip  Index of the sector to ignore when copying.
*
*   Return value
*     ==0   OK
*     !=0   An error occurred
*/
static int _ConvertWorkBlock(NAND_INST * pInst, WORK_BLOCK_DESC * pWorkBlock, unsigned brsiToSkip) {
  unsigned iSector;
  U16      pbiSrc;
  U16      pbiWork;
  U32      SectorIndexSrc0;
  U32      SectorIndexWork0;
  U32      SectorIndexDest0;
  unsigned SectorsPerBlock;
  U32      pbiDest;
  U32      EraseCntDest;
  U32      EraseCntSrc;
  int      r;
  unsigned brsiSrc;
  unsigned BlockCntSrc;
  unsigned SectorStat;
  int      ErrorInEccSrc;
  int      ErrorInEccWork;
  unsigned ErrorBRSI;

  EraseCntSrc      = ERASE_CNT_INVALID;
  EraseCntDest     = ERASE_CNT_INVALID;
  pbiWork          = pWorkBlock->pbi;
  SectorIndexWork0 = _BlockIndex2SectorIndex0(pInst, pbiWork);
  SectorsPerBlock  = 1 << pInst->PPB_Shift;
  pbiDest          = 0;  // No destination block, yet
  ErrorBRSI        = 0;

Retry:
  ErrorInEccSrc  = 0;
  ErrorInEccWork = 0;
  //
  // Allocate a new empty block to store the data from the work block.
  //
  pbiDest = _AllocErasedBlock(pInst, &EraseCntDest);
  if (pbiDest == 0) {
    return 1;             // Error, no more free blocks, not recoverable.
  }
  //
  // OK, we have an empty block. Compute the sector indexes of the source and destination blocks.
  //
  pbiSrc           = _L2P_Read(pInst, pWorkBlock->lbi);
  SectorIndexSrc0  = _BlockIndex2SectorIndex0(pInst, pbiSrc);
  SectorIndexDest0 = _BlockIndex2SectorIndex0(pInst, pbiDest);
  //
  // The second sector of a block stores block related information which must be written to spare area.
  // This is the reason why it is handled separately here.
  //
  iSector     = BLOCK_INFO_BRSI;
  BlockCntSrc = 0;
  brsiSrc     = _WB_ReadAssignment(pInst, pWorkBlock, iSector);
  //
  // Read data and spare area from the source sector which can be the data block or the work block.
  //
  if (brsiSrc && (brsiSrc != brsiToSkip)) { // In work block ?
    //
    // Read here the block count of the data block.
    // It is required below when we write the data to destination block.
    //
    if (SectorIndexSrc0) {
      r = _ReadSectorWithECC(pInst, _pSectorBuffer, SectorIndexSrc0 + iSector);
      if ((r == NAND_UNCORRECTABLE_ERROR) || (r == NAND_READ_ERROR)) {
        _MarkBlockAsBad(pInst, pbiSrc, r, iSector);
        return 1;               // Error: invalid data read, no way to recover.
      }
      if (r == NAND_ERROR_IN_ECC) {
        ErrorBRSI     = iSector;
        ErrorInEccSrc = 1;      // Remember we had an error in the ECC when reading from the data block.
      }
      BlockCntSrc = _LoadBlockCnt(pInst);
    }
    //
    // Now read the actual data from the work block.
    //
    r = _ReadSectorWithECC(pInst, _pSectorBuffer, SectorIndexWork0 + brsiSrc);
    if ((r == NAND_UNCORRECTABLE_ERROR) || (r == NAND_READ_ERROR)) {
      _MarkBlockAsBad(pInst, pbiWork, r, brsiSrc);
      return 1;                 // Error: invalid data read, no way to recover.
    }
    if (r == NAND_ERROR_IN_ECC) {
      ErrorBRSI      = iSector;
      ErrorInEccWork = 1;       // Remember we had an error in the ECC when reading from the work block.
    }
    SectorStat = _LoadSectorStat(pInst);
  } else if (SectorIndexSrc0) {             // In source block ?
    r = _ReadSectorWithECC(pInst, _pSectorBuffer, SectorIndexSrc0 + iSector);
    if ((r == NAND_UNCORRECTABLE_ERROR) || (r == NAND_READ_ERROR)) {
      _MarkBlockAsBad(pInst, pbiSrc, r, iSector);
      return 1;                 // Error: invalid data read, no way to recover.
    }
    if (r == NAND_ERROR_IN_ECC) {
      ErrorBRSI     = iSector;
      ErrorInEccSrc = 1;        // Remember we had an error in the ECC when reading from the data block.
    }
    BlockCntSrc = _LoadBlockCnt(pInst);
    SectorStat  = _LoadSectorStat(pInst);
  } else {
    FS_MEMSET(_pSectorBuffer, 0xFF, pInst->BytesPerPage);
    SectorStat = SECTOR_STAT_EMPTY;
  }
  //
  // Store the block related information to spare area buffer.
  //
  _ClearStaticSpareArea(pInst);
  _StoreEraseCnt(pInst, EraseCntDest);
  _StoreBlockType(pInst, BLOCK_TYPE_DATA);
  _StoreBlockCnt(pInst, BlockCntSrc + 1);
  _StoreSectorStat(pInst, SectorStat);
  _StoreLBI(pInst, pWorkBlock->lbi);
  //
  // Write data and spare area to destination sector.
  //
  r = _WriteSectorWithECC(pInst, _pSectorBuffer, SectorIndexDest0 + iSector);
  if (r) {
    _MarkBlockAsBad(pInst, pbiDest, r, iSector);
    goto Retry;               // Write error occurred, try to find another empty block.
  }

  //
  // Fail-safe TP
  //
  // At this point we have 2 data blocks with the same LBI. The copy operation is not complete. 
  // Low-level mount should throw away the latest version of the data block.
  //

  //
  // Copy the data of the sectors left.
  //
  ++iSector;
  for (; iSector < SectorsPerBlock; iSector++) {
    //
    // Sector data can be in work block, sector block or invalid.
    //
    brsiSrc = _WB_ReadAssignment(pInst, pWorkBlock, iSector);
    if (brsiSrc && (brsiSrc != brsiToSkip)) { // In work block ?
      r = _CopySectorWithECC(pInst, SectorIndexWork0 + brsiSrc, SectorIndexDest0 + iSector);
      if ((r == NAND_NO_ERROR) || (r == NAND_BITS_CORRECTED)) {
        continue;
      }
      if ((r == NAND_UNCORRECTABLE_ERROR) || (r == NAND_READ_ERROR)) {
        _MarkBlockAsBad(pInst, pbiWork, r, iSector);
        return 1;               // No way to recover out of this
      }
      if (r == NAND_ERROR_IN_ECC) {
        ErrorBRSI      = iSector;
        ErrorInEccWork = 1;     // Remember we had an error in the ECC when reading from the work block.
      }
      if (r == NAND_WRITE_ERROR) {
        _MarkBlockAsBad(pInst, pbiDest, r, iSector);
        goto Retry;             // Write error occurred, try to find another empty block
      }
    } else if (SectorIndexSrc0) {             // In source block ?
      //
      // Copy if we have a data source.
      // Note that when closing a work block which did not yet have a source data block,
      // it can happen that some sector have no source and stay empty.
      //
      r = _CopySectorWithECC(pInst, SectorIndexSrc0 + iSector, SectorIndexDest0 + iSector);
      if ((r == NAND_NO_ERROR) || (r == NAND_BITS_CORRECTED)) {
        continue;
      }
      if ((r == NAND_UNCORRECTABLE_ERROR) || (r == NAND_READ_ERROR)) {
        _MarkBlockAsBad(pInst, pbiSrc, r, iSector);
        return 1;             // No way to recover out of this.
      }
      if (r == NAND_ERROR_IN_ECC) {
        ErrorBRSI     = iSector;
        ErrorInEccSrc = 1;    // Remember we had an error in the ECC when reading from the data block.
        continue;
      }
      if (r == NAND_WRITE_ERROR) {
        _MarkBlockAsBad(pInst, pbiDest, r, iSector);
        goto Retry;           // Write error occurred, try to find another empty block.
      }
    }
  }
  //
  // Update the mapping of physical to logical blocks.
  //
  _L2P_Write(pInst, pWorkBlock->lbi, pbiDest);
  //
  // Free the old data block and the work block. Note that the order of the operations are important.
  // The data block must be freed first to ensure the data is not lost after an unexpected reset.
  //
  if (pbiSrc) {
    if (ErrorInEccSrc) {
      _MarkBlockAsBad(pInst, pbiSrc, NAND_ERROR_IN_ECC, ErrorBRSI);           // There was a bit error in the ECC of one of the sectors in the data block. The block is no more usable.
    } else {
      _MakeBlockAvailable(pInst, pbiSrc,  BLOCK_TYPE_DATA, EraseCntSrc);      // Free old data block if there was one.
    }
  }
  if (ErrorInEccWork) {
    _MarkBlockAsBad(pInst, pbiWork, NAND_ERROR_IN_ECC, ErrorBRSI);            // There was a bit error in the ECC of one of the sectors in the work block. The block is no more usable.
  } else {
    _MakeBlockAvailable(pInst, pbiWork, BLOCK_TYPE_WORK, ERASE_CNT_INVALID);  // Free work block.
  }
  //
  // Remove the work block from the internal list.
  //
  _WB_RemoveFromUsedList(pInst, pWorkBlock);
  _WB_AddToFreeList(pInst, pWorkBlock);
  //
  // If required, update the information used for active wear leveling.
  //
  {
    U32 EraseCntMin;
    U32 NumBlocksEraseCntMin;

    EraseCntMin          = pInst->EraseCntMin;
    NumBlocksEraseCntMin = pInst->NumBlocksEraseCntMin;
    if (EraseCntDest < EraseCntMin) {
      EraseCntMin          = EraseCntDest;
      NumBlocksEraseCntMin = 1;
    } else if (EraseCntDest == EraseCntMin) {
      ++NumBlocksEraseCntMin;
    }
    pInst->EraseCntMin          = EraseCntMin;
    pInst->NumBlocksEraseCntMin = NumBlocksEraseCntMin;
  }
  IF_STATS(pInst->StatCounters.ConvertViaCopyCnt++);
  return 0;
}

/*********************************************************************
*
*       _CleanWorkBlock
*
*   Function description
*     Closes the work buffer.
*     - Convert work block into normal data buffer by copy all data into it and marking it as data block
*     - Invalidate and mark as free the block which contained the same logical data area before
*
*   Parameters
*     pInst       [IN]  Driver instance
*                 [OUT] ---
*     pWorkBlock  [IN]  Work block to be cleaned
*                 [OUT] ---
*
*   Return values
*     ==0   OK
*     !=0   An error occurred
*/
static int _CleanWorkBlock(NAND_INST * pInst, WORK_BLOCK_DESC * pWorkBlock) {
  int r;
  U32 EraseCntWork;

  //
  // The erase count is needed when the block is freed, so read it here.
  //
  r = _ReadEraseCnt(pInst, pWorkBlock->pbi, &EraseCntWork);
  if (r) {
    EraseCntWork = pInst->EraseCntMax;
  }
  if (_WB_HasValidSectors(pInst, pWorkBlock) == 0) {    // No valid sectors in the work block ?
    _MakeBlockAvailable(pInst, pWorkBlock->pbi, BLOCK_TYPE_WORK, ERASE_CNT_INVALID);
    //
    // Remove the work block from the internal list
    //
    _WB_RemoveFromUsedList(pInst, pWorkBlock);
    _WB_AddToFreeList(pInst, pWorkBlock);
    return 0;
  }
  //
  // Convert the work block by merging it with the corresponding source block.
  //
  return _ConvertWorkBlock(pInst, pWorkBlock, 0);
}

/*********************************************************************
*
*       _CleanLastWorkBlock
*
*  Function Description
*    Removes the least recently used work block from list of work blocks and converts it into data block
*/
static int _CleanLastWorkBlock(NAND_INST * pInst) {
  WORK_BLOCK_DESC * pWorkBlock;
  //
  // Find last work block in list
  //
  pWorkBlock = pInst->pFirstWorkBlockInUse;
  while (pWorkBlock->pNext) {
    pWorkBlock = pWorkBlock->pNext;
  }
  return _CleanWorkBlock(pInst, pWorkBlock);
}

/*********************************************************************
*
*       _CleanAllWorkBlocks
*
*   Function description
*     Closes all work blocks.
*/
#if FS_NAND_ENABLE_CLEAN
static void _CleanAllWorkBlocks(NAND_INST * pInst) {
  while (pInst->pFirstWorkBlockInUse) {
    _CleanWorkBlock(pInst, pInst->pFirstWorkBlockInUse);
  }
}
#endif

/*********************************************************************
*
*       _AllocWorkBlock
*
*   Function Description
*    - Allocates a WORK_BLOCK_DESC management entry from the array in the pInst structure
*    - Finds a free block and assigns it to the WORK_BLOCK_DESC
*
*   Parameters
*     pInst       [IN]  Driver instance
*                 [OUT] ---
*     lbi         Logical block index assigned to work block
*     pEraseCnt   [IN]  ---
*                 [OUT] Number of times the allocated block has been erased
*
*   Return values
*     !=NULL  Pointer to allocated work block
*     ==NULL  An error occurred, typ. a fatal error
*/
static WORK_BLOCK_DESC * _AllocWorkBlock(NAND_INST * pInst, unsigned lbi, U32 * pEraseCnt) {
  WORK_BLOCK_DESC * pWorkBlock;
  U32               EraseCnt;
  unsigned          pbi;
  int               r;

  pWorkBlock = _AllocWorkBlockDesc(pInst, lbi);
  if (pWorkBlock == NULL) {
    r = _CleanLastWorkBlock(pInst);
    if (r) {
      return NULL;                  // Error: Happens only in case of a fatal error
    }
    pWorkBlock = _AllocWorkBlockDesc(pInst, lbi);
    if (pWorkBlock == NULL) {
      return NULL;                  // Error: This can happen in case of hardware failure only
    }
  }
  //
  // Get an empty block to write on.
  //
  pbi = _AllocErasedBlock(pInst, &EraseCnt);
  if (pbi == 0) {
    return NULL;                    // Error: Happens only in case of a fatal error
  }
  //
  // OK, new work block allocated.
  //
  pWorkBlock->pbi = pbi;
  *pEraseCnt      = EraseCnt;
  return pWorkBlock;
}

/*********************************************************************
*
*       _FindWorkBlock
*
*  Function description
*    Tries to locate a work block for a given logical block.
*/
static WORK_BLOCK_DESC * _FindWorkBlock(const NAND_INST * pInst, unsigned lbi) {
  WORK_BLOCK_DESC * pWorkBlock;

  //
  // Iterate over used-list
  //
  pWorkBlock = pInst->pFirstWorkBlockInUse;
  do {
    if (pWorkBlock == NULL) {
      break;                          // No match
    }
    if (pWorkBlock->lbi == lbi) {
      break;                          // Found it
    }
    pWorkBlock = pWorkBlock->pNext;
  } while (1);
  return pWorkBlock;
}

/*********************************************************************
*
*       _MarkWorkBlockAsMRU
*
*  Function description
*    Marks the given work block as most-recently used.
*    This is important so the least recently used one can be "kicked out" if a new one is needed.
*/
static void _MarkWorkBlockAsMRU(NAND_INST * pInst, WORK_BLOCK_DESC * pWorkBlock) {
  if (pWorkBlock != pInst->pFirstWorkBlockInUse) {
    _WB_RemoveFromUsedList(pInst, pWorkBlock);
    _WB_AddToUsedList(pInst, pWorkBlock);
  }
}

#if FS_NAND_VERIFY_WRITE

/*********************************************************************
*
*       _VerifySector
*
*  Function description
*    Verifies the data stored to a sector.
*/
static int _VerifySector(NAND_INST * pInst, const U32 * pData, U32 SectorIndex) {
  int r;

  r = _ReadSectorWithECC(pInst, _pVerifyBuffer, SectorIndex);
  if ((r == NAND_NO_ERROR) || (r == NAND_BITS_CORRECTED) || (r == NAND_ERROR_IN_ECC)) {
    U32   NumItems;
    U32 * p;

    NumItems = pInst->BytesPerPage / 4;
    p        = _pVerifyBuffer;
    do {
      if (*p++ != *pData++) {
        FS_DEBUG_ERROROUT((FS_MTYPE_DRIVER, "NAND_UNI: Verify failed at sector %d.\n", SectorIndex));
        return 1;           // Verification failed.
      }
    } while (--NumItems);
    return 0;               // OK
  }
  return 1;                 // Error, could not read sector.
}

#endif

/*********************************************************************
*
*       _LoadWorkBlock
*
*  Function description
*    Reads management data of work block.
*    Used during low-level mount only, since at all other times, the work block descriptors are up to date.
*/
static int _LoadWorkBlock(NAND_INST * pInst, WORK_BLOCK_DESC * pWorkBlock) {
  unsigned SectorsPerBlock;
  unsigned iSector;
  U32      SectorIndex0;
  unsigned brsi;
  U32      pbiWork;
  unsigned brsiFree;
  U32      SectorIndexSrc;

  pbiWork         = pWorkBlock->pbi;
  SectorsPerBlock = 1 << pInst->PPB_Shift;
  SectorIndex0    = _BlockIndex2SectorIndex0(pInst, pbiWork);
  iSector         = 0;
  //
  // Iterate over all sectors, reading spare info in order to find out if sector contains data and if so which data.
  //
  ++iSector;            // First writable sector in an empty work block.
  brsiFree = iSector;
  for (; iSector < SectorsPerBlock; iSector++) {
    int r;

    SectorIndexSrc = SectorIndex0 + iSector;
    //
    // Check if this sector of the work block contains valid data.
    //
    r = _ReadSectorWithECC(pInst, _pSectorBuffer, SectorIndexSrc);
    if ((r == NAND_NO_ERROR) || (r == NAND_BITS_CORRECTED) || (r == NAND_ERROR_IN_ECC)) {
      brsi = _LoadBRSI(pInst);
      //
      // Sector 0 does not store any data and is invalid as BRSI.
      //
      if (brsi == 0) {
        FS_DEBUG_ERROROUT((FS_MTYPE_DRIVER, "NAND_UNI: Invalid sector index 0 found in work block.\n"));
        return 1;             // Error, invalid sector index.
      }
      if (brsi > SectorsPerBlock) {
        if (_PageIsBlank(pInst, SectorIndexSrc)) {
          break;              // OK, found an empty sector. This is the first sector we can write to.
        } else {
          FS_DEBUG_ERROROUT((FS_MTYPE_DRIVER, "NAND_UNI: First free sector in work block is not blank.\n"));
          return 1;           // Error, invalid BRSI.
        }
      }
      ++brsiFree;
      _WB_WriteAssignment(pInst, pWorkBlock, brsi, iSector);
      pWorkBlock->brsiFree = brsiFree;
    } else {
      return 1;               // Error, the data in work block is corrupted.
    }
  }
#if FS_DEBUG_LEVEL >= FS_DEBUG_LEVEL_CHECK_ALL
  //
  // On higher debug levels check whether the free sectors in the work block are blank.
  //
  for (; iSector < SectorsPerBlock; ++iSector) {
    SectorIndexSrc = SectorIndex0 + iSector;
    if (_PageIsBlank(pInst, SectorIndexSrc) == 0) {
      FS_DEBUG_ERROROUT((FS_MTYPE_DRIVER, "NAND_UNI: Found free sector in work block which is not blank.\n"));
      return 1;
    }
  }
#endif
  return 0;                   // OK, work block information loaded to memory.
}

/*********************************************************************
*
*       _BlockDataIsMoreRecent
*
*   Function description
*     Used during low-level mount only to determine which version of a data block is newer.
*
*   Return value
*     ==0   psiPrev is older.
*     ==1   psiPrev is newer.
*
*   Note
*       (1) Overwrites the contents of the static spare area.
*/
static char _BlockDataIsMoreRecent(NAND_INST * pInst, U32 psiPrev) {
  unsigned BlockCnt;
  unsigned BlockCntPrev;
  unsigned BlockCntDiff;
  int      r;

  //
  // Get the data count of the current block from the static spare area.
  //
  BlockCnt = _LoadBlockCnt(pInst);
  //
  // Read the data count of the previous block form storage.
  //
  r = _ReadBlockCnt(pInst, psiPrev, &BlockCntPrev);
  if (r) {
    return 0;       // psiPrev is older.
  }
  BlockCntDiff = (BlockCntPrev - BlockCnt) & 0xF;
  if (BlockCntDiff == 1) {
    return 1;       // psiPrev is newer.
  }
  return 0;         // psiPrev is older.
}

/*********************************************************************
*
*       _LowLevelMount
*
*  Function description
*    Reads and analyzes management information from NAND flash.
*    If the information makes sense and allows us to read and write from
*    the medium, it can perform read and write operations.
*
*  Return value
*    0     O.K., device has been successfully mounted and is accessible
*  !=0     Error
*
*/
static int _LowLevelMount(NAND_INST * pInst) {
  U16        iBlock;
  U16        lbi;
  U16        pbiPrev;
  U32        EraseCntMax;               // Highest erase count on any sector
  U32        EraseCnt;
  U32        EraseCntMin;
  U32        NumBlocksEraseCntMin;
  const U8 * pPageBuffer;
  unsigned   u;
  int        r;
  U32        Version;
  U32        NumBlocksToFileSystem;
  int        NumBlocksToUse;
  WORK_BLOCK_DESC * pWorkBlock;
  unsigned   NumWorkBlocks;
  unsigned   NumWorkBlocksLLFormat;
  unsigned   NumWorkBlocksToAllocate;
  U32        NumPhyBlocks;
  unsigned   SectorsPerBlock;

  //
  // Check info block first (First block in the system)
  //
  r = _ReadSectorWithECC(pInst, _pSectorBuffer, FORMAT_INFO_SECTOR_INDEX);
  if ((r != NAND_NO_ERROR) && (r != NAND_BITS_CORRECTED) && (r != NAND_ERROR_IN_ECC)) {
    return 1;                   // Error
  }
  pPageBuffer = (const U8 *)_pSectorBuffer;
  if (FS_MEMCMP(_acInfo, pPageBuffer , sizeof(_acInfo))) {
    FS_DEBUG_ERROROUT((FS_MTYPE_DRIVER, "NAND_UNI: Invalid low-level signature.\n"));
    return 1;                   // Error
  }
  Version = FS_LoadU32BE(pPageBuffer + INFO_OFF_LLFORMAT_VERSION);
  if (Version != (U32)LLFORMAT_VERSION) {
    FS_DEBUG_ERROROUT((FS_MTYPE_DRIVER, "NAND_UNI: Invalid low-level format version.\n"));
    return 1;                   // Error
  }
  NumWorkBlocksLLFormat = FS_LoadU32BE(pPageBuffer + INFO_OFF_NUM_WORK_BLOCKS);
  if (NumWorkBlocksLLFormat >= pInst->NumPhyBlocks) {
    FS_DEBUG_ERROROUT((FS_MTYPE_DRIVER, "NAND_UNI: Invalid number of work blocks.\n"));
    return 1;                   // Error
  }
  //
  // Find out how many work blocks are required to be allocated.
  // We take the maximum between the number of work blocks read from device
  // and the number of work blocks configured. The reason is to prevent
  // an overflow in the paWorkBlock array when the application increases
  // the number of work blocks and does a low-level format.
  //
  NumWorkBlocks           = pInst->NumWorkBlocks;
  NumWorkBlocksToAllocate = MAX(NumWorkBlocksLLFormat, NumWorkBlocks);
  NumWorkBlocks           = NumWorkBlocksLLFormat;
  //
  // Compute the number of logical blocks available for the file system.
  // We have to take into account that this version of the driver
  // reserves one block more for internal use. To stay compatible we have
  // to use 2 algorithms: one for the "old" version and one for the "new" one.
  // We tell the 2 versions appart by checking the INFO_OFF_NUM_LOG_BLOCKS.
  // The "old" version does not set this entry and its value will always be 0xFFFFFFFF.
  //
  NumPhyBlocks          = pInst->NumPhyBlocks;
  NumBlocksToFileSystem = FS_LoadU32BE(pPageBuffer + INFO_OFF_NUM_LOG_BLOCKS);
  NumBlocksToUse        = _CalcNumBlocksToUse(NumPhyBlocks, NumWorkBlocks);
  if ((NumBlocksToUse <= 0) || (NumBlocksToFileSystem > (U32)NumBlocksToUse)) {
    FS_DEBUG_ERROROUT((FS_MTYPE_DRIVER,  "NAND_UNI: Number of logical blocks has shrunk. Low-level format required.\n"));
    return 1;
  }
  SectorsPerBlock      = (1 << pInst->PPB_Shift) - 1;
  pInst->NumLogBlocks  = NumBlocksToUse;
  pInst->NumSectors    = pInst->NumLogBlocks * SectorsPerBlock;
  pInst->NumWorkBlocks = NumWorkBlocks;
  //
  // Load the information stored when a fatal error occurs
  //
  pInst->IsWriteProtected = 0;
  pInst->HasFatalError    = 0;
  pInst->ErrorType        = NAND_NO_ERROR;
  pInst->ErrorSectorIndex = 0;
  r = _ReadSectorWithECC(pInst, _pSectorBuffer, ERROR_INFO_SECTOR_INDEX);
  if ((r == NAND_NO_ERROR) || (r == NAND_BITS_CORRECTED) || (r == NAND_ERROR_IN_ECC)) {
    pPageBuffer = (const U8 *)_pSectorBuffer;
    pInst->IsWriteProtected = FS_LoadU16BE(pPageBuffer + INFO_OFF_IS_WRITE_PROTECTED) != 0xFFFF ? 1 : 0;  // Inverted, 0xFFFF is not write protected
    pInst->HasFatalError    = FS_LoadU16BE(pPageBuffer + INFO_OFF_HAS_FATAL_ERROR)    != 0xFFFF ? 1 : 0;  // Inverted, 0xFFFF doesn't have fatal error
    if (pInst->HasFatalError) {
      pInst->ErrorType        = (U8)FS_LoadU16BE(pPageBuffer + INFO_OFF_FATAL_ERROR_TYPE);
      pInst->ErrorSectorIndex = FS_LoadU32BE(pPageBuffer + INFO_OFF_FATAL_ERROR_SECTOR_INDEX);
    }
  }
  //
  // Assign reasonable defaults for configurable parameters.
  //
  if (pInst->MaxEraseCntDiff == 0) {
    pInst->MaxEraseCntDiff = FS_NAND_MAX_ERASE_CNT_DIFF;
  }
  //
  // Allocate/Zero memory for tables
  //
  FS_AllocZeroedPtr((void **)&pInst->pLog2PhyTable, _L2P_GetSize(pInst));
  FS_AllocZeroedPtr((void **)&pInst->pFreeMap,      (pInst->NumPhyBlocks + 7) / 8);
  //
  //  Init work block descriptors: Alloc memory and add them to free list
  //
  {
    unsigned NumBytes;

    NumBytes = sizeof(WORK_BLOCK_DESC) * NumWorkBlocksToAllocate;
    //
    // This is equivalent to FS_AllocZeroedPtr() but it avoids filling the array with 0
    // when the memory block is already allocated.
    //
    if (pInst->paWorkBlock == NULL) {
      pInst->paWorkBlock = (WORK_BLOCK_DESC *)FS_AllocZeroed(NumBytes);
      FS_MEMSET(pInst->paWorkBlock, 0, NumBytes);
    }
    NumBytes   = _WB_GetAssignmentSize(pInst);
    pWorkBlock = pInst->paWorkBlock;
    u          = NumWorkBlocksToAllocate;
    do {
      FS_AllocZeroedPtr((void **)&pWorkBlock->paAssign, NumBytes);
      //
      // Not all the work block descriptors are available if the number of work blocks
      // specified in the device is smaller than the number of work blocks configured.
      //
      if (NumWorkBlocks) {
        _WB_AddToFreeList(pInst, pWorkBlock);
        NumWorkBlocks--;
      }
      pWorkBlock++;
    } while (--u);
  }
  //
  // Read the spare areas and fill the tables.
  //
  EraseCntMax          = 0;
  EraseCntMin          = ERASE_CNT_INVALID;
  NumBlocksEraseCntMin = 0;
  IF_STATS(pInst->StatCounters.NumBadBlocks = 0);
  for (iBlock = STORAGE_START_BLOCK_INDEX; iBlock < pInst->NumPhyBlocks; iBlock++) {
    unsigned BlockType;
    U32      SectorIndex;

    if (_BlockIsBad(pInst, iBlock)) {
      IF_STATS(pInst->StatCounters.NumBadBlocks++);
      continue;                               // Ignore bad blocks.
    }
    //
    // Read the block information from the sparea area of the second page.
    //
    SectorIndex = _BlockIndex2SectorIndex0(pInst, iBlock);
    ++SectorIndex;
    r = _ReadSectorWithECC(pInst, _pSectorBuffer, SectorIndex);
    if ((r != NAND_NO_ERROR) && (r != NAND_BITS_CORRECTED) && (r != NAND_ERROR_IN_ECC)) {
      _MarkBlockAsFree(pInst, iBlock);        // Block information is corrupted. Put the block in the free list. It will be erased before use.
      continue;
    }
    //
    // OK, block information has no bit errors or the errors were corrected.
    //
    BlockType = _LoadBlockType(pInst);
    lbi       = _LoadLBI(pInst);
    EraseCnt  = _LoadEraseCnt(pInst);
    //
    // Check if the block information makes sense. If not erase the block and put it in the free list.
    //
    if ((EraseCnt == ERASE_CNT_INVALID) || (lbi >= pInst->NumLogBlocks)) {
      _MarkBlockAsFree(pInst, iBlock);        // Block seems not to be empty. Consider it free to use.
      continue;
    }
    //
    // Handle work blocks.
    //
    if (BlockType == BLOCK_TYPE_WORK) {
      if (pInst->pFirstWorkBlockFree) {
        //
        // Check if we already have a block with this lbi.
        // If we do, then we erase it and add it to the free list.
        //
        pWorkBlock = _FindWorkBlock(pInst, lbi);
        if (pWorkBlock) {
          FS_DEBUG_WARN((FS_MTYPE_DRIVER, "NAND_UNI: Found a work block with the same lbi.\n"));
          _ClearBlock(pInst, iBlock, EraseCnt);
          continue;
        }
        pWorkBlock      = _AllocWorkBlockDesc(pInst, lbi);
        pWorkBlock->pbi = iBlock;
      } else {
        FS_DEBUG_ERROROUT((FS_MTYPE_DRIVER,  "NAND_UNI: Found more work blocks than can be handled. Configuration changed?\n"));
        _ClearBlock(pInst, iBlock, EraseCnt);
      }
    //
    // Handle data blocks.
    //
    } else if (BlockType == BLOCK_TYPE_DATA) {
      pbiPrev = _L2P_Read(pInst, lbi);
      if (pbiPrev == 0) {                                   // Has this lbi already been assigned ?
        _L2P_Write(pInst, lbi, iBlock);                     // Add block to the translation table
        if (EraseCnt > EraseCntMax) {
          EraseCntMax = EraseCnt;
        }
        continue;
      }
      if (_BlockDataIsMoreRecent(pInst, pbiPrev)) {
        _ClearBlock(pInst, pbiPrev, ERASE_CNT_INVALID);
        _L2P_Write(pInst, lbi, iBlock);                     // Add block to the translation table
      } else {
        _ClearBlock(pInst, iBlock, EraseCnt);
      }
      if ((EraseCntMin == ERASE_CNT_INVALID) ||
          (EraseCnt < EraseCntMin)) {                       // Collect information for the active wear leveling
        EraseCntMin          = EraseCnt;
        NumBlocksEraseCntMin = 1;
      } else if (EraseCnt == EraseCntMin) {
        ++NumBlocksEraseCntMin;
      }
    } else {
      //
      // Any other blocks are interpreted as free blocks.
      //
      _MarkBlockAsFree(pInst, iBlock);
    }
  }
  pInst->EraseCntMax          = EraseCntMax;
  pInst->EraseCntMin          = EraseCntMin;
  pInst->NumBlocksEraseCntMin = NumBlocksEraseCntMin;
  //
  // Load information from the work blocks we found.
  //
  pWorkBlock = pInst->pFirstWorkBlockInUse;
  while (pWorkBlock) {
    r = _LoadWorkBlock(pInst, pWorkBlock);
    if (r) {
      return 1;       // Error, failed to load work block.
    }
    pWorkBlock = pWorkBlock->pNext;
  }
  return 0;           // O.K. !
}

/*********************************************************************
*
*        _GetSectorUsage
*
*   Function description
*     Checks if a logical sector contains valid data.
*
*   Return values
*     ==0     Sector in use (contains valid data)
*     ==1     Sector not in use (was not written nor was invalidated)
*     ==2     Usage unknown
*/
static int _GetSectorUsage(NAND_INST * pInst, U32 LogSectorIndex) {
  unsigned lbi;
  unsigned pbiSrc;
  unsigned pbiWork;
  unsigned brsiLog;
  unsigned brsiPhy;
  WORK_BLOCK_DESC * pWorkBlock;
  int      r;
  unsigned SectorStat;
  U32      SectorIndexSrc;
  U32      SectorIndexWork;

  ASSERT_SECTORS_ARE_IN_RANGE(pInst, LogSectorIndex, LogSectorIndex);
  lbi        = _LogSectorIndex2LogBlockIndex(pInst, LogSectorIndex, &brsiLog);
  pbiSrc     = _L2P_Read(pInst, lbi);
  pWorkBlock = _FindWorkBlock(pInst, lbi);
  //
  // Sector data can be in data block or work block.
  //
  if (pWorkBlock) {
    pbiWork = pWorkBlock->pbi;
    brsiPhy = _WB_ReadAssignment(pInst, pWorkBlock, brsiLog);
    if (brsiPhy) {          // Sector in a work block ?
      SectorIndexWork  = _BlockIndex2SectorIndex0(pInst, pbiWork);
      SectorIndexWork |= brsiPhy;
      r = _ReadSectorStat(pInst, SectorIndexWork, &SectorStat);
      if ((r == 0) && (SectorStat == SECTOR_STAT_WRITTEN)) {
        return 0;           // Sector contains valid data.
      } else {
        return 1;           // Sector has been invalidated.
      }
    }
  }
  if (pbiSrc) {             // Sector in a data block ?
    SectorIndexSrc  = _BlockIndex2SectorIndex0(pInst, pbiSrc);
    SectorIndexSrc |= brsiLog;
    r = _ReadSectorStat(pInst, SectorIndexSrc, &SectorStat);
    if ((r == 0) && (SectorStat == SECTOR_STAT_WRITTEN)) {
      return 0;             // Sector contains valid data.
    }
  }
  return 1;                 // Sector contains invalid data.
}

/*********************************************************************
*
*       _LowLevelMountIfRequired
*
*  Function description
*    LL-Mounts the device if it is not LLMounted and this has not already been
*    tried in vain.
*
*  Return value
*    0     O.K., device has been successfully mounted and is accessible
*  !=0     Error
*/
static int _LowLevelMountIfRequired(NAND_INST * pInst) {
  int r;

  if (pInst->IsLLMounted) {
    return 0;                   // O.K., is mounted
  }
  if (pInst->LLMountFailed) {
    return 1;                   // Error, we could not mount it and do not want to try again
  }
  r = _LowLevelMount(pInst);
  if (r == 0) {
    pInst->IsLLMounted = 1;
  } else {
    pInst->LLMountFailed = 1;
  }
  //
  // On debug builds we count here the number of valid sectors.
  //
#if FS_NAND_ENABLE_STATS
  if (pInst->IsLLMounted) {
    U32      iSector;
    U32      NumLogSectors;
    unsigned SectorUsage;
    U32      NumValidSectors;

    NumLogSectors   = pInst->NumSectors;
    NumValidSectors = 0;
    for (iSector = 0; iSector < NumLogSectors; ++iSector) {
      SectorUsage = _GetSectorUsage(pInst, iSector);
      if (SectorUsage == 0) {                       // Sector contains valid data ?
        ++NumValidSectors;
      }
    }
    pInst->StatCounters.NumValidSectors = NumValidSectors;
  }
#endif
  return r;
}

/*********************************************************************
*
*       _ReadOneSector
*
*   Function description
*     Reads one logical sectors from storage device.
*     There are 3 possibilities:
*     a) Data is in WorkBlock
*     b) There is a physical block assigned to this logical block -> Read from Hardware
*     c) There is a no physical block assigned to this logical block. This means data has never been written to storage. Fill data with 0.
*
*   Return value
*     ==0   Data successfully read.
*     !=0   An error has occurred.
*
*/
static int _ReadOneSector(NAND_INST * pInst, U32 LogSectorIndex, U8 * pBuffer) {
  int      r;
  unsigned lbi;
  unsigned pbi;
  unsigned brsiPhy;
  unsigned brsiLog;
  U32      SectorIndex;
  WORK_BLOCK_DESC * pWorkBlock;

  lbi        = _LogSectorIndex2LogBlockIndex(pInst, LogSectorIndex, &brsiLog);
  brsiPhy    = brsiLog;
  //
  // Physical block index is taken from Log2Phy table or is work block.
  //
  pbi        = _L2P_Read(pInst, lbi);
  pWorkBlock = _FindWorkBlock(pInst, lbi);
  if (pWorkBlock) {
    unsigned u;

    u = _WB_ReadAssignment(pInst, pWorkBlock, brsiLog);
    if (u) {
      pbi     = pWorkBlock->pbi;
      brsiPhy = u;
    }
  }
  //
  // Copy data to user buffer.
  //
  if (pbi == 0) {
    //
    // Fill buffer with a known value if the sector has never been written.
    //
    FS_MEMSET(pBuffer, 0xFF, pInst->BytesPerPage);
    r = 0;
  } else {
    //
    // Read from hardware.
    //
    SectorIndex  = _BlockIndex2SectorIndex0(pInst, pbi);
    SectorIndex |= brsiPhy;
    r = _ReadSectorWithECC(pInst, (U32 *)pBuffer, SectorIndex);
    if ((r == NAND_NO_ERROR) || (r == NAND_BITS_CORRECTED)) {
      r = 0;
    } else if (r == NAND_ERROR_IN_ECC) {
      //
      // We found an error in the ECC protecting the sector data.
      // The data is still OK so we copy it into another block.
      // This block is not usable anymore.
      //
      if (pWorkBlock) {
        r = _ConvertWorkBlock(pInst, pWorkBlock, 0);
      } else {
        r = _RecoverDataBlock(pInst, pbi);
      }
    }
  }
  return r;
}

/*********************************************************************
*
*       _WriteOneSector
*
*   Function Description
*     Writes one logical sector to storage device. Can be used to invalidate the data of a sector by setting pBuffer to NULL.
*
*   Return value:
*     ==0   Data successfully written.
*     !=0   An error has occurred.
*/
static int _WriteOneSector(NAND_INST * pInst, U32 LogSectorIndex, const void * pBuffer) {
  U16               lbi;
  WORK_BLOCK_DESC * pWorkBlock;
  unsigned          brsiSrc;
  unsigned          brsiDest;
  int               r;
  U32               SectorIndex;
  unsigned          brsiPhy;
  U32               EraseCnt;

  lbi      = _LogSectorIndex2LogBlockIndex(pInst, LogSectorIndex, &brsiSrc);
  brsiDest = 0;
  //
  // Check if the sector contains valid data before invalidating it.
  //
  if (pBuffer == NULL) {
    unsigned SectorStat;
    unsigned pbiSrc;

    pbiSrc       = _L2P_Read(pInst, lbi);
    SectorIndex  = _BlockIndex2SectorIndex0(pInst, pbiSrc);
    SectorIndex |= brsiSrc;
    r = _ReadSectorStat(pInst, SectorIndex, &SectorStat);
    if ((r == 0) && (SectorStat == SECTOR_STAT_EMPTY)) {
      //
      // Source sector contains no valid data. Check if the block containing the sector has been modified.
      //
      pWorkBlock = _FindWorkBlock(pInst, lbi);
      if (pWorkBlock == NULL) {
        return 0;                       // OK, sector has never been written. Invalidating it makes no sense.
      }
      brsiPhy = _WB_ReadAssignment(pInst, pWorkBlock, brsiSrc);
      if (brsiPhy == 0) {
        return 0;                       // OK, no copy of the sector in this work block. Invalidating it makes no sense.
      }
    }
  }
  while (1) {
    EraseCnt = ERASE_CNT_INVALID;
    //
    // Find (or create) a work block and the sector to be used in it.
    //
    pWorkBlock = _FindWorkBlock(pInst, lbi);
    if (pWorkBlock) {
      //
      // Make sure that sector to write is in work area and has not been written already.
      //
      brsiDest = _GetNextFreeSector(pInst, pWorkBlock);
      if (brsiDest == 0) {
        r = _CleanWorkBlock(pInst, pWorkBlock);
        if (r) {
          return 1;
        }
        pWorkBlock = NULL;
      }
    }
    if (pWorkBlock == NULL) {
      pWorkBlock = _AllocWorkBlock(pInst, lbi, &EraseCnt);
      if (pWorkBlock == NULL) {
        return 1;
      }
      brsiDest = _GetNextFreeSector(pInst, pWorkBlock);
    }
    //
    // Compute the physical sector index sector to write to.
    //
    SectorIndex  = _BlockIndex2SectorIndex0(pInst, pWorkBlock->pbi);
    SectorIndex |= brsiDest;
    //
    // Write data to a free sector in the work block.
    //
    _ClearStaticSpareArea(pInst);
    _StoreBRSI(pInst, brsiSrc);
    if (pBuffer) {                        // Mark the sector data as valid if data is written to it.
      _StoreSectorStat(pInst, SECTOR_STAT_WRITTEN);
    }
    //
    // If the sector stores block related information we store them to static spare buffer.
    //
    if (brsiDest == BLOCK_INFO_BRSI) {
      _StoreEraseCnt(pInst, EraseCnt);
      _StoreLBI(pInst, lbi);
      _StoreBlockType(pInst, BLOCK_TYPE_WORK);   // Mark as work block.
    }
    //
    // In case the write operation invalidates the data of a sector
    // use the internal buffer to write 0xFFs to it.
    //
    if (pBuffer == NULL) {
      FS_MEMSET(_pSectorBuffer, 0xFF, pInst->BytesPerPage);
      pBuffer = _pSectorBuffer;
    }
    r = _WriteSectorWithECC(pInst, (const U32 *)pBuffer, SectorIndex);
#if FS_NAND_VERIFY_WRITE
    if (r == 0) {
      r = _VerifySector(pInst, (const U32 *)pBuffer, SectorIndex);
    }
#endif
    if (r == 0) {
      break;                            // OK, data has been written.
    }
    //
    // Could not write to work block. Save the data of this work block into a data block
    // and allocate another work block to write to.
    //
    r = _ConvertWorkBlock(pInst, pWorkBlock, brsiDest);
    if (r) {
      return 1;
    }
  }
  //
  // Update work block management info.
  //
  _MarkWorkBlockAsMRU(pInst, pWorkBlock);
  _WB_WriteAssignment(pInst, pWorkBlock, brsiSrc, brsiDest);
  return 0;
}

/*********************************************************************
*
*        _FreeSectors
*
*   Function description
*     Marks a logical sector as free. This routine is called from the
*     higher layer file system to help the driver to manage the data.
*     This way sectors which are no longer in use by the higher
*     layer file system do not need to be copied.
*/
#if FS_NAND_ENABLE_TRIM
static void _FreeSectors(NAND_INST * pInst, U32 LogSectorIndex, U32 NumSectors) {
  int r;
  int SectorUsage;

  ASSERT_SECTORS_ARE_IN_RANGE(pInst, LogSectorIndex, LogSectorIndex + NumSectors - 1);
  do {
    //
    // The usage of a sector is useful only for the statistics (debug builds)
    // to update the number of valid sectors.
    //
    SectorUsage = _GetSectorUsage(pInst, LogSectorIndex);
    if (SectorUsage == 0) {     // Sector in use?
    r = _WriteOneSector(pInst, LogSectorIndex, NULL);
      if (r == 0) {
      IF_STATS(pInst->StatCounters.NumValidSectors--);
    }
    }
    LogSectorIndex++;
  } while (--NumSectors);
}
#endif // FS_NAND_ENABLE_TRIM

/*********************************************************************
*
*       _CleanOne
*
*   Function description
*     Executes a single "clean" job. This might be the conversion of a work block into a data block
*     or the erase of a block marked as invalid.
*
*   Return value
*     ==0   Nothing else to "clean"
*     ==1   At least one more "clean" job left to do
*/
#if FS_NAND_ENABLE_CLEAN
static int _CleanOne(NAND_INST * pInst) {
  //
  // Clean work blocks first
  //
  if (pInst->pFirstWorkBlockInUse) {
    _CleanWorkBlock(pInst, pInst->pFirstWorkBlockInUse);
  }
  //
  // Now check if there is more work to do
  //
  if (pInst->pFirstWorkBlockInUse) {
    return 1;     // At least one more work block to clean
  }
  return 0;
}
#endif // FS_NAND_ENABLE_CLEAN

/*********************************************************************
*
*       _Clean
*
*  Function description
*    Converts all work blocks into data blocks and erases all free blocks.
*/
#if FS_NAND_ENABLE_CLEAN
static void _Clean(NAND_INST * pInst) {
  _CleanAllWorkBlocks(pInst);
}
#endif // FS_NAND_ENABLE_CLEAN

/*********************************************************************
*
*       _BlockCanBeErased
*
*   Function description
*     Checks whether the driver is allowed to erase the given block.
*     The blocks marked as defect by the manufacturer are never erased.
*     The erasing of the blocks marked as defect by the driver can be explicitly enabled/disable via a compile time switch.
*/
static int _BlockCanBeErased(NAND_INST * pInst, unsigned BlockIndex) {
  if (_BlockIsBad(pInst, BlockIndex) == 0) {
    return 1;                               // Block can be erased.
  }
#if FS_NAND_RECLAIM_DRIVER_BAD_BLOCKS
  {
    const U8 * pInfo;
    U8       * pSpare;
    U8         aSpare[8]; // 6 bytes for the "SEGGER" string and 2 bytes for the "bad" block mark
    U32        PageIndex;
    unsigned   NumBytesToCompare;
    U8         DoCompare;
    unsigned   NumBytesSpare;

    //
    // Check whether the block was marked as defect by the driver. If so, return that it can be erased.
    //
    PageIndex = (U32)BlockIndex << pInst->PPB_Shift;
    _DisableHW_ECCIfRequired(pInst);        // Temporarily disable the HW ECC during the data transfer to avoid ECC errors.
    _ReadPhySpare(pInst, PageIndex, aSpare, 0, sizeof(aSpare));
    _EnableHW_ECCIfRequired(pInst);
    pInfo  = _acInfo;
    pSpare = aSpare;
    NumBytesToCompare = 6;                  // Compare only the letters.
    NumBytesSpare     = sizeof(aSpare);
    DoCompare         = 0;
    do {
      if (DoCompare == 0) {
        if (*pSpare == *pInfo) {
          ++pInfo;
          --NumBytesToCompare;
          DoCompare = 1;                    // First byte of the signature found.
        }
      } else {
        if (NumBytesToCompare) {
          if (*pSpare != *pInfo) {
        break;                              // Driver signature for bad block do not match. Block marked as bad by the manufacturer.
      }
          ++pInfo;
          --NumBytesToCompare;
        }
    }
      ++pSpare;
    } while (--NumBytesSpare);
    if (NumBytesToCompare == 0) {
      return 1;                             // Driver marked the block as bad. This block can be erased.
    }
  }
#endif
  return 0;
}

/*********************************************************************
*
*       _LowLevelFormat
*
*  Function description
*    Erases all blocks and writes the format information to the first one.
*
*  Return value:
*    ==0    O.K.
*    !=0    Error
*/
static int _LowLevelFormat(NAND_INST * pInst) {
  int        r;
  U32        NumPhyBlocks;
  unsigned   iBlock;
  U8       * pPageBuffer;

  pInst->LLMountFailed  = 0;
  pInst->IsLLMounted    = 0;
  pPageBuffer           = (U8 *)_pSectorBuffer;

  //
  // Erase the first sector/phy. block. This block is guaranteed to be valid.
  //
  r = _EraseBlock(pInst, 0);
  if (r) {
    return 1; // Error
  }
  //
  // Erase NAND flash blocks which are valid.
  // Valid NAND flash blocks are blocks that
  // contain a 0xff in the first byte of the spare area of the first two pages of the block
  //
  NumPhyBlocks = pInst->NumPhyBlocks;
  for (iBlock = 1; iBlock < NumPhyBlocks; iBlock++) {
    if (_BlockCanBeErased(pInst, iBlock)) {
      _EraseBlock(pInst, iBlock);
    } else {
       IF_STATS(pInst->StatCounters.NumBadBlocks++);
    }
  }
  IF_STATS(pInst->StatCounters.NumValidSectors = 0);
  //
  // Write the format information to first sector of the first block.
  //
  FS_MEMSET(pPageBuffer, 0xFF, pInst->BytesPerPage);
  FS_MEMCPY(pPageBuffer, _acInfo, sizeof(_acInfo));
  FS_StoreU32BE(pPageBuffer + INFO_OFF_LLFORMAT_VERSION, LLFORMAT_VERSION);
  FS_StoreU32BE(pPageBuffer + INFO_OFF_NUM_LOG_BLOCKS,   pInst->NumLogBlocks);
  FS_StoreU32BE(pPageBuffer + INFO_OFF_NUM_WORK_BLOCKS,  pInst->NumWorkBlocks);
  _ClearStaticSpareArea(pInst);
  return _WriteSectorWithECC(pInst, _pSectorBuffer, FORMAT_INFO_SECTOR_INDEX);
}

/*********************************************************************
*
*       Global functions
*
**********************************************************************
*/

/*********************************************************************
*
*       _NAND_GetStatus
*/
static int _NAND_GetStatus(U8 Unit) {
  FS_USE_PARA(Unit);
  return FS_MEDIA_IS_PRESENT;
}

/*********************************************************************
*
*       _NAND_Write
*
*   Function description
*     FS driver function. Writes one or more logical sectors to storage device.
*
*   Return value
*     ==0   Data successfully written.
*     !=0   An error has occurred.
*
*/
static int _NAND_Write(U8 Unit, U32 SectorIndex, const void * p, U32 NumSectors, U8 RepeatSame) {
  const U8  * pData8;
  NAND_INST * pInst;
  int         r;

  ASSERT_UNIT_NO_IS_IN_RANGE(Unit);
  pInst = _apInst[Unit];
  pData8 = (const U8 *)p;
  r = _LowLevelMountIfRequired(pInst);
  if (r) {
    return r;                       // Error, could not mount the NAND flash.
  }
  if (pInst->HasFatalError) {
    FS_DEBUG_ERROROUT((FS_MTYPE_DRIVER, "NAND_UNI: Failed to write. Medium has a fatal error.\n"));
    return 1;
  }
  if (pInst->IsWriteProtected) {
    return 1;
  }
  ASSERT_SECTORS_ARE_IN_RANGE(pInst, SectorIndex, SectorIndex + NumSectors - 1);
  //
  // Write the data one sector at a time.
  //
  do {
#if FS_NAND_ENABLE_STATS
    int SectorUsage;
#endif

    //
    // The usage of a sector is useful only in debug builds
    // to update the number of valid sectors.
    //
#if FS_NAND_ENABLE_STATS
    SectorUsage = _GetSectorUsage(pInst, SectorIndex);
#endif
    r = _WriteOneSector(pInst, SectorIndex, pData8);
    if (r) {
      return 1;                     // Error, could not write sector.
    }
    if (pInst->HasFatalError) {
      return 1;
    }
#if FS_NAND_ENABLE_STATS
    //
    // Increment the sector valid count only if the sector is written for the first time.
    //
    if (SectorUsage != 0) {
      pInst->StatCounters.NumValidSectors++;
    }
    pInst->StatCounters.WriteSectorCnt++;
#endif
    if (--NumSectors == 0) {
      break;
    }
    if (RepeatSame == 0) {
      pData8 += pInst->BytesPerPage;
    }
    SectorIndex++;
  } while (1);
  return 0;                         // OK, data written.
}

/*********************************************************************
*
*       _NAND_Read
*
*  Function description
*    Driver callback function.
*    Reads one or more logical sectors from storage device.
*
*  Return value
*      0    Data successfully written.
*    !=0    An error has occurred.
*
*/
static int _NAND_Read(U8 Unit, U32 SectorIndex, void * p, U32 NumSectors) {
  U8        * pData8;
  NAND_INST * pInst;

  ASSERT_UNIT_NO_IS_IN_RANGE(Unit);
  pInst = _apInst[Unit];
  pData8 = (U8 *)p;

  //
  // Make sure device is low-level mounted. If it is not, there is nothing we can do.
  //
  if (_LowLevelMountIfRequired(pInst)) {
    return 1;                                 // Error, could not mount the NAND flash.
  }
  ASSERT_SECTORS_ARE_IN_RANGE(pInst, SectorIndex, SectorIndex + NumSectors - 1);
  //
  // Read the data one sector at a time.
  //
  do {
    if (_ReadOneSector(pInst, SectorIndex, pData8)) {
      FS_DEBUG_ERROROUT((FS_MTYPE_DRIVER,  "NAND_UNI: Failed to read sector.\n"));
      return 1;                               // Error, could not read data.
    }
    pData8 += pInst->BytesPerPage;
    SectorIndex++;
    IF_STATS(pInst->StatCounters.ReadSectorCnt++);
  } while (--NumSectors);
  return 0;                                    // OK, data read.
}


/*********************************************************************
*
*       _NAND_IoCtl
*/
static int _NAND_IoCtl(U8 Unit, I32 Cmd, I32 Aux, void * pBuffer) {
  NAND_INST   * pInst;
  FS_DEV_INFO * pDevInfo;
  int           r;

  FS_USE_PARA(Aux);
  ASSERT_UNIT_NO_IS_IN_RANGE(Unit);
  pInst    = _apInst[Unit];
  r        = -1;
  switch (Cmd) {
  case FS_CMD_GET_DEVINFO:
    if (pBuffer) {
      //
      // This low-level mount is required in oder to calculate
      // the correct number of sectors available to the file system
      //
      r = _LowLevelMountIfRequired(pInst);
      if (r == 0) {
        pDevInfo = (FS_DEV_INFO *)pBuffer;
        pDevInfo->NumSectors     = pInst->NumSectors;
        pDevInfo->BytesPerSector = pInst->BytesPerPage;
      }
    }
    break;
  case FS_CMD_FORMAT_LOW_LEVEL:
    return _LowLevelFormat(pInst);
  case FS_CMD_REQUIRES_FORMAT:
    return _LowLevelMountIfRequired(pInst);
  case FS_CMD_UNMOUNT:
  case FS_CMD_UNMOUNT_FORCED:
    if (pInst) {
    pInst->IsLLMounted          = 0;
    pInst->MRUFreeBlock         = 0;
    pInst->pFirstWorkBlockFree  = NULL;
    pInst->pFirstWorkBlockInUse = NULL;
    r = 0;
    }
    break;
#if FS_NAND_ENABLE_CLEAN
  case FS_CMD_CLEAN_ONE:
    r = _LowLevelMountIfRequired(pInst);
    if (r == 0) {
      int   More;
      int * pMore;

      More = _CleanOne(pInst);
      pMore = (int *)pBuffer;
      if (pMore) {
        *pMore = More;
      }
    }
    break;
  case FS_CMD_CLEAN:
    r = _LowLevelMountIfRequired(pInst);
    if (r == 0) {
      _Clean(pInst);
    }
    break;
#endif
  case FS_CMD_GET_SECTOR_USAGE:
    if (pBuffer) {
      r = _LowLevelMountIfRequired(pInst);
      if (r == 0) {
        int * pSectorUsage;

        pSectorUsage  = (int *)pBuffer;
        *pSectorUsage = _GetSectorUsage(pInst, (U32)Aux);
      }
    }
    break;
#if FS_NAND_ENABLE_TRIM
  case FS_CMD_FREE_SECTORS:
    r = _LowLevelMountIfRequired(pInst);
    if (r == 0) {
      U32 SectorIndex;
      U32 NumSectors;

      SectorIndex = (U32)Aux;
      NumSectors  = *(U32 *)pBuffer;
      _FreeSectors(pInst, SectorIndex, NumSectors);
    }
    break;
#endif
#if FS_SUPPORT_DEINIT
  case FS_CMD_DEINIT:
    if (pInst) {
      unsigned u;

      FS_FREE(pInst->pLog2PhyTable);
      FS_FREE(pInst->pFreeMap);
      if (pInst->paWorkBlock) {       // The array is allocated only when the volume is mounted.
        for (u = 0; u < pInst->NumWorkBlocks; u++) {
          WORK_BLOCK_DESC * pWorkBlock;

          pWorkBlock = &pInst->paWorkBlock[u];
          FS_FREE(pWorkBlock->paAssign);
        }
        FS_FREE(pInst->paWorkBlock);
      }
      FS_FREE(pInst);
      _apInst[Unit] = 0;
      //
      // If all instances have been removed, remove the sector buffers.
      //
      if (--_NumUnits == 0) {
        FS_FREE(_pSectorBuffer);
        _pSectorBuffer = 0;
        FS_FREE(_pSpareAreaData);
        _pSpareAreaData = 0;
#if FS_NAND_VERIFY_WRITE
        FS_FREE(_pVerifyBuffer);
        _pVerifyBuffer = 0;
#endif
      }
      r = 0;
    }
    break;
#endif
  }
  return r;
}

/*********************************************************************
*
*       _AllocInstIfRequired
*
*   Function description
*     Allocate memory for the specified unit if required.
*
*/
static NAND_INST * _AllocInstIfRequired(U8 Unit) {
  NAND_INST * pInst;

  if (Unit >= NUM_UNITS) {
    FS_DEBUG_ERROROUT((FS_MTYPE_DRIVER, "Invalid unit number specified! Unit no shall be less than %d. Given %d.\n", NUM_UNITS, Unit));
    FS_X_Panic(FS_ERROR_UNKNOWN_DEVICE);
    return NULL;
  }
  pInst = _apInst[Unit];
  if (pInst == NULL) {
    FS_AllocZeroedPtr((void**)&_apInst[Unit], sizeof(NAND_INST));
    pInst = _apInst[Unit];
  }
  pInst->Unit = Unit;
  return pInst;
}

/*********************************************************************
*
*       _InitDevice
*
*   Function description
*     Identifies and intializes the NAND flash and stores its parameters in the driver instance.
*
*/
static int _InitDevice(NAND_INST * pInst) {
  int r;
  U32 BytesPerSector;
  U32 BytesPerSpareArea;

  ASSERT_PHY_TYPE_IS_SET(pInst);
  //
  // Allocate the sector buffer and the spare area buffer which are common to all driver instances.
  //
  BytesPerSector    = FS_Global.MaxSectorSize;
#if FS_NAND_MAX_SPARE_AREA_SIZE
  BytesPerSpareArea = FS_NAND_MAX_SPARE_AREA_SIZE;
#else
  BytesPerSpareArea = BytesPerSector >> 5;
#endif
  FS_AllocZeroedPtr((void **)&_pSectorBuffer,   BytesPerSector);         
  FS_AllocZeroedPtr((void **)&_pSpareAreaData, BytesPerSpareArea);
#if FS_NAND_VERIFY_WRITE
  FS_AllocZeroedPtr((void **)&_pVerifyBuffer,  BytesPerSector);
#endif
  r = _ReadApplyDeviceParas(pInst);
  if (r) {
    return 1;                      // Failed to identify NAND Flash or unsupported type
  }
  if (pInst->pPhyType->pfIsWP(pInst->Unit)) {
    pInst->IsWriteProtected = 1;
  }
  return 0;
}

/*********************************************************************
*
*       _PrepareInst
*
*   Function description
*     Called by the API functions to ensure the driver and the NAND flash are initialized properly.
*
*   Return value
*     !=NULL   Driver instance
*     ==NULL   An error occurred
*
*/
static NAND_INST * _PrepareInst(U8 Unit) {
  NAND_INST * pInst;
  int         r;

  pInst = _AllocInstIfRequired(Unit);     // It ever gives a NULL back.
  r     = _InitDevice(pInst);
  if (r) {
    return NULL;
  }
  r = _LowLevelMountIfRequired(pInst);
  if (r) {
    return NULL;
  }
  return pInst;
}

/*********************************************************************
*
*       _NAND_AddDevice
*
*   Function description
*     Allocates memory for a new driver instance.
*
*   Return value
*     >=0   Command successfully executed, Unit no.
*     < 0   Error, could not add device
*
*/
static int _NAND_AddDevice(void) {
  if (_NumUnits >= NUM_UNITS) {
    return -1;
  }
  _AllocInstIfRequired((U8)_NumUnits);
  return _NumUnits++;
}

/*********************************************************************
*
*       _NAND_InitMedium
*
*   Function description
*     Initialize and identifies the storage device.
*
*   Return value:
*     ==0   Device OK and ready for operation.
*     < 0   An error has occurred.
*/
static int _NAND_InitMedium(U8 Unit) {
  NAND_INST * pInst;

  ASSERT_UNIT_NO_IS_IN_RANGE(Unit);
  pInst    = _apInst[Unit];
  return _InitDevice(pInst);
}

/*********************************************************************
*
*       _NAND_GetNumUnits
*/
static int _NAND_GetNumUnits(void) {
  return _NumUnits;
}

/*********************************************************************
*
*       _NAND_GetDriverName
*/
static const char * _NAND_GetDriverName(U8 Unit) {
  FS_USE_PARA(Unit);
  return "nand";
}

/*********************************************************************
*
*       API Table
*
**********************************************************************
*/
const FS_DEVICE_TYPE FS_NAND_UNI_Driver = {
  _NAND_GetDriverName,
  _NAND_AddDevice,
  _NAND_Read,
  _NAND_Write,
  _NAND_IoCtl,
  _NAND_InitMedium,
  _NAND_GetStatus,
  _NAND_GetNumUnits
};

/*********************************************************************
*
*       ECC hooks
*
**********************************************************************
*/
const FS_NAND_ECC_HOOK FS_NAND_ECC_SW_1BIT = {
  _ECC516_Compute,
  _ECC516_Apply,
  1
};

const FS_NAND_ECC_HOOK FS_NAND_ECC_HW_NULL = {
  NULL,
  NULL,
  0
};

const FS_NAND_ECC_HOOK FS_NAND_ECC_HW_4BIT = {
  NULL,
  NULL,
  4
};

/*********************************************************************
*
*       Public code
*
**********************************************************************
*/

/*********************************************************************
*
*       FS_NAND_UNI_GetStatCounters
*/
void FS_NAND_UNI_GetStatCounters(U8 Unit, FS_NAND_STAT_COUNTERS * pStat) {
#if FS_NAND_ENABLE_STATS
  NAND_INST * pInst;

  ASSERT_UNIT_NO_IS_IN_RANGE(Unit);
  pInst  = _AllocInstIfRequired(Unit);
  *pStat = pInst->StatCounters;
#else
  FS_USE_PARA(Unit);
  FS_MEMSET(pStat, 0, sizeof(FS_NAND_STAT_COUNTERS));
#endif
}

/*********************************************************************
*
*       FS_NAND_UNI_ResetStatCounters
*/
void FS_NAND_UNI_ResetStatCounters(U8 Unit) {
#if FS_NAND_ENABLE_STATS
  NAND_INST * pInst;
  FS_NAND_STAT_COUNTERS * pStat;

  ASSERT_UNIT_NO_IS_IN_RANGE(Unit);
  pInst  = _AllocInstIfRequired(Unit);
  pStat = &pInst->StatCounters;
  pStat->ConvertInPlaceCnt = 0;
  pStat->ConvertViaCopyCnt = 0;
  pStat->CopySectorCnt     = 0;
  pStat->EraseCnt          = 0;
  pStat->NumReadRetries    = 0;
  pStat->ReadDataCnt       = 0;
  pStat->ReadSectorCnt     = 0;
  pStat->ReadSpareCnt      = 0;
  pStat->WriteDataCnt      = 0;
  pStat->WriteSectorCnt    = 0;
  pStat->WriteSpareCnt     = 0;
#else
  FS_USE_PARA(Unit);
#endif
}

/*********************************************************************
*
*       FS_NAND_UNI_SetPhyType
*/
void FS_NAND_UNI_SetPhyType(U8 Unit, const FS_NAND_PHY_TYPE * pPhyType) {
  NAND_INST * pInst;

  ASSERT_UNIT_NO_IS_IN_RANGE(Unit);
  pInst  = _AllocInstIfRequired(Unit);
  pInst->pPhyType = pPhyType;
}

/*********************************************************************
*
*       FS_NAND_UNI_SetECCHook
*/
void FS_NAND_UNI_SetECCHook(U8 Unit, const FS_NAND_ECC_HOOK * pECCHook) {
  NAND_INST * pInst;

  ASSERT_UNIT_NO_IS_IN_RANGE(Unit);
  pInst  = _AllocInstIfRequired(Unit);
  pInst->pECCHook = pECCHook;
}

/*********************************************************************
*
*       FS_NAND_UNI_SetBlockRange
*/
void FS_NAND_UNI_SetBlockRange(U8 Unit, U16 FirstBlock, U16 MaxNumBlocks) {
  NAND_INST * pInst;

  ASSERT_UNIT_NO_IS_IN_RANGE(Unit);
  pInst  = _AllocInstIfRequired(Unit);
  pInst->FirstBlock   = FirstBlock;
  pInst->MaxNumBlocks = MaxNumBlocks;
}

/*********************************************************************
*
*       FS_NAND_UNI_SetMaxEraseCntDiff
*/
void FS_NAND_UNI_SetMaxEraseCntDiff(U8 Unit, U32 EraseCntDiff) {
  NAND_INST * pInst;

  ASSERT_UNIT_NO_IS_IN_RANGE(Unit);
  pInst = _AllocInstIfRequired(Unit);
  pInst->MaxEraseCntDiff = EraseCntDiff;
}

/*********************************************************************
*
*       FS_NAND_UNI_SetNumWorkBlocks
*/
void FS_NAND_UNI_SetNumWorkBlocks(U8 Unit, U32 NumWorkBlocks) {
  NAND_INST * pInst;

  ASSERT_UNIT_NO_IS_IN_RANGE(Unit);
  pInst = _AllocInstIfRequired(Unit);
  pInst->NumWorkBlocksConf = NumWorkBlocks;
}

/*********************************************************************
*
*       FS_NAND_UNI_ReadPhySector
*
*  Function description:
*    This function will read a physical sector from NAND flash.
*
*  Parameters:
*    Unit             Unit/Index number of the NAND flash to read from
*    PhySectorIndex   Physical sector index
*    pData            Pointer to a buffer to store read data
*    pNumBytesData    [IN]  Pointer to variable storing the size of the data buffer
*                     [OUT] The number of bytes that were stored in the data buffer
*    pSpare           Pointer to a buffer to store read spare data
*    pNumBytesSpare   [IN]  Pointer to variable storing the size of the spare data buffer
*                     [OUT] The number of bytes that were stored in the spare data buffer
*
*  Return value
*     ==0   OK, sector data read
*     !=0   An error occurred
*/
int FS_NAND_UNI_ReadPhySector(U8 Unit, U32 PhySectorIndex, void * pData, unsigned * pNumBytesData, void * pSpare, unsigned * pNumBytesSpare) {
  NAND_INST * pInst;
  U32         NumPhySectors;
  int         r;
  U32         NumBytes2Copy;

  //
  // Allocate memory for the instance and initialize the NAND flash, if required.
  //
  pInst = _AllocInstIfRequired(Unit);     // It ever gives a NULL back.
  r     = _InitDevice(pInst);
  if (r) {
    return 1;
  }
  if (pInst == NULL) {
    return 1;         // Error, instance could not be initialized or mounted.
  }
  NumPhySectors = (U32)pInst->NumPhyBlocks * (1 << pInst->PPB_Shift);
  if (PhySectorIndex >= NumPhySectors) {
    return 1;         // Error, invalid sector number.
  }
  r = _ReadSectorWithECC(pInst, _pSectorBuffer, PhySectorIndex);
  if ((r == NAND_NO_ERROR) || (r == NAND_BITS_CORRECTED) || (r == NAND_ERROR_IN_ECC)) {
    NumBytes2Copy   = MIN(pInst->BytesPerPage, *pNumBytesData);
    *pNumBytesData  = NumBytes2Copy;
    FS_MEMCPY(pData, _pSectorBuffer, NumBytes2Copy);
    NumBytes2Copy   = MIN((unsigned)pInst->BytesPerPage >> 5, *pNumBytesSpare);
    *pNumBytesSpare = NumBytes2Copy;
    FS_MEMCPY(pSpare, _pSpareAreaData, NumBytes2Copy);
    return 0;         // OK, data read.
  }
  return 1;           // Error, reading from NAND flash failed.
}

/*********************************************************************
*
*       FS_NAND_UNI_EraseFlash
*
*   Function description
*     This function will erase the whole NAND flash.
*     Please use this function with care, since it will erase all the blocks of the flash
*     and therefor also erases the bad block information.
*
*   Parameters
*     Unit    Number/index of the NAND flash to erase
*
*/
int FS_NAND_UNI_EraseFlash(U8 Unit) {
  NAND_INST * pInst;
  U32         NumPhyBlocks;
  unsigned    iBlock;
  int         r;

  //
  // Allocate memory for the instance and initialize the NAND flash, if required.
  //
  pInst = _AllocInstIfRequired(Unit);     // It ever gives a NULL back.
  r     = _InitDevice(pInst);
  if (pInst == NULL) {
    return 1;
  }
  //
  // Erase NAND flash blocks which are valid.
  // Valid NAND flash blocks are blocks that
  // contain a 0xff in the first byte of the spare area of the first two pages of the block
  //
  r            = 0;
  NumPhyBlocks = pInst->NumPhyBlocks;
  for (iBlock = 0; iBlock < NumPhyBlocks; iBlock++) {
    r |= _EraseBlock(pInst, iBlock);
    //
    // If we could not erase the block
    //
    if (r) {
      FS_DEBUG_ERROROUT((FS_MTYPE_DRIVER, "NAND_UNI: Failed to erase block %d.\n", iBlock));
    }
  }
  return r;
}

/*********************************************************************
*
*       FS_NAND_UNI_GetDiskInfo
*
*  Function description:
*    Returns information about the NAND flash disk
*    This function is not required for the functionality of the driver and will typically not be linked in in production builds.
*
*/
int FS_NAND_UNI_GetDiskInfo(U8 Unit, FS_NAND_DISK_INFO * pDiskInfo) {
  NAND_INST * pInst;
  unsigned    iBlock;
  U32         NumPhyBlocks;
  U32         NumUsedPhyBlocks;
  U32         NumBadPhyBlocks;
  U32         EraseCntMax;
  U32         EraseCntMin;
  U32         EraseCntAvg;
  U32         EraseCnt;
  U32         EraseCntTotal;
  U32         NumEraseCnt;

  //
  // Allocate memory for the instance, initialize the NAND flash and do a low-level mount, if required.
  //
  pInst = _PrepareInst(Unit);
  if (pInst == NULL) {
    return 1;       // Error, intialisation or low-level mount failed.
  }
  //
  // Initalize the counters.
  //
  NumUsedPhyBlocks = 0;
  NumBadPhyBlocks  = 0;
  NumPhyBlocks     = pInst->NumPhyBlocks;
  EraseCntMax      = 0;
  EraseCntMin      = 0xFFFFFFFF;
  NumEraseCnt      = 0;
  EraseCntTotal    = 0;
  //
  // Check each block of the NAND flash and collect the information into counters.
  //
  for (iBlock = 0; iBlock < NumPhyBlocks; iBlock++) {
    U32      SectorIndex;
    int      r;

    //
    // Count allocated blocks.
    //
    if (_BlockIsFree(pInst, iBlock) == 0) {
      NumUsedPhyBlocks++;
    }
    //
    // Count bad blocks.
    //
    if (_BlockIsBad(pInst, iBlock)) {
      NumBadPhyBlocks++;
      continue;
    }
    //
    // The sparea are of the first page in a block stores the erase count.
    //
    SectorIndex = _BlockIndex2SectorIndex0(pInst, iBlock);
    r = _ReadSectorWithECC(pInst, _pSectorBuffer, SectorIndex);
    if ((r == NAND_NO_ERROR) || (r == NAND_BITS_CORRECTED) || (r == NAND_ERROR_IN_ECC)) {
      EraseCnt = _LoadEraseCnt(pInst);
        //
        // Update the min and max erase counts.
        //
        if (EraseCnt != ERASE_CNT_INVALID) {
          if (EraseCnt > EraseCntMax) {
            EraseCntMax = EraseCnt;
          }
          if (EraseCnt < EraseCntMin) {
            EraseCntMin = EraseCnt;
          }
          EraseCntTotal += EraseCnt;
          ++NumEraseCnt;
        }
      }
  }
  //
  // Compute the average erase count of all blocks.
  //
  if (NumEraseCnt) {
    EraseCntAvg = EraseCntTotal / NumEraseCnt;
  } else {
    EraseCntAvg = 0;
  }
  //
  // Store the collected information to user structure.
  //
  FS_MEMSET(pDiskInfo, 0, sizeof(*pDiskInfo));
  pDiskInfo->NumPhyBlocks       = NumPhyBlocks;
  pDiskInfo->NumLogBlocks       = pInst->NumLogBlocks;
  pDiskInfo->NumPagesPerBlock   = 1 << pInst->PPB_Shift;
  pDiskInfo->NumSectorsPerBlock = 1 << pInst->PPB_Shift;
  pDiskInfo->BytesPerPage       = pInst->BytesPerPage;
  pDiskInfo->BytesPerSector     = pInst->BytesPerPage;
  pDiskInfo->NumUsedPhyBlocks   = NumUsedPhyBlocks;
  pDiskInfo->NumBadPhyBlocks    = NumBadPhyBlocks;
  pDiskInfo->EraseCntMax        = EraseCntMax;
  pDiskInfo->EraseCntMin        = EraseCntMin;
  pDiskInfo->EraseCntAvg        = EraseCntAvg;
  pDiskInfo->IsWriteProtected   = pInst->IsWriteProtected;
  pDiskInfo->HasFatalError      = pInst->HasFatalError;
  pDiskInfo->ErrorSectorIndex   = pInst->ErrorSectorIndex;
  pDiskInfo->ErrorType          = pInst->ErrorType;
  return 0;
}

/*********************************************************************
*
*       FS_NAND_UNI_GetBlockInfo
*
*   Functiond description
*     Returns information about a particular physical block
*     This function is not required for the functionality of the driver and will typically not be linked in in production builds.
*/
int FS_NAND_UNI_GetBlockInfo(U8 Unit, U32 PhyBlockIndex, FS_NAND_BLOCK_INFO * pBlockInfo) {
  NAND_INST  * pInst;
  unsigned     iSector;
  U32          SectorIndexSrc0;
  U32          EraseCnt;
  U32          lbi;
  U16          NumSectorsBlank;               // Sectors are not used yet.
  U16          NumSectorsValid;               // Sectors contain correct data.
  U16          NumSectorsInvalid;             // Sectors have been invalidated.
  U16          NumSectorsECCError;            // Sectors have incorrect ECC.
  U16          NumSectorsECCCorrectable;      // Sectors have correctable ECC error.
  U16          NumSectorsErrorInECC;
  U16          SectorsPerBlock;
  unsigned     BlockType;
  int          r;
  unsigned     Type;
  unsigned     SectorStat;
  unsigned     brsi;

  //
  // Allocate memory for the instance, initialize the NAND flash and do a low-level mount, if required.
  //
  pInst = _PrepareInst(Unit);
  if (pInst == NULL) {
    return 1;       // Error, intialisation or low-level mount failed.
  }
  NumSectorsBlank          = 0;
  NumSectorsValid          = 0;
  NumSectorsInvalid        = 0;
  NumSectorsECCError       = 0;
  NumSectorsECCCorrectable = 0;
  NumSectorsErrorInECC     = 0;
  lbi                      = 0;
  SectorsPerBlock          = 1 << pInst->PPB_Shift;
  SectorIndexSrc0          = _BlockIndex2SectorIndex0(pInst, PhyBlockIndex);
  EraseCnt                 = 0;
  FS_MEMSET(pBlockInfo, 0, sizeof(FS_NAND_BLOCK_INFO));
  if (_BlockIsBad(pInst, PhyBlockIndex)) {
    Type = NAND_BLOCK_TYPE_BAD;
    goto Done;
  }
  //
  // Read the second sector of the block to get the erase count and the LBI.
  //
  Type = NAND_BLOCK_TYPE_UNKNOWN;
  for (iSector = BLOCK_INFO_BRSI; iSector < SectorsPerBlock; ++iSector) {
    r = _ReadSectorWithECC(pInst, _pSectorBuffer, SectorIndexSrc0 + iSector);
    if ((r == NAND_NO_ERROR) || (r == NAND_BITS_CORRECTED) || (r == NAND_ERROR_IN_ECC)) {
      if (iSector == BLOCK_INFO_BRSI) {
        BlockType = _LoadBlockType(pInst);
        EraseCnt  = _LoadEraseCnt(pInst);
        lbi       = _LoadLBI(pInst);
        switch(BlockType) {
        case BLOCK_TYPE_EMPTY:
         Type = NAND_BLOCK_TYPE_EMPTY;
         break;
        case BLOCK_TYPE_WORK:
         Type = NAND_BLOCK_TYPE_WORK;
         break;
        case BLOCK_TYPE_DATA:
         Type = NAND_BLOCK_TYPE_DATA;
         break;
        default:
         Type = NAND_BLOCK_TYPE_UNKNOWN;
         break;
        }
      }
      SectorStat = _LoadSectorStat(pInst);
      if (SectorStat != SECTOR_STAT_WRITTEN) {
        NumSectorsInvalid++;
      } else {
        brsi = _LoadBRSI(pInst);
        if (brsi == 0) {
          ++NumSectorsInvalid;            // Error: invalid BRSI.
        }
        if (brsi > SectorsPerBlock) {
          if (_PageIsBlank(pInst, SectorIndexSrc0 + iSector)) {
            ++NumSectorsBlank;            // OK, found an empty sector.
          } else {
            ++NumSectorsInvalid;          // Error, invalid BRSI.
          }
        }
        if (r == NAND_BITS_CORRECTED) {
          NumSectorsECCCorrectable++;     // Data have been corrected
        } else if (r == NAND_ERROR_IN_ECC) {
          NumSectorsErrorInECC++;
        }
      }
    } else {
      NumSectorsECCError++;               // Error not correctable by ECC or NAND read error
    }
  }
Done:
  pBlockInfo->Type                     = Type;
  pBlockInfo->EraseCnt                 = EraseCnt;
  pBlockInfo->lbi                      = lbi;
  pBlockInfo->NumSectorsBlank          = NumSectorsBlank;
  pBlockInfo->NumSectorsECCCorrectable = NumSectorsECCCorrectable;
  pBlockInfo->NumSectorsErrorInECC     = NumSectorsErrorInECC;
  pBlockInfo->NumSectorsECCError       = NumSectorsECCError;
  pBlockInfo->NumSectorsInvalid        = NumSectorsInvalid;
  pBlockInfo->NumSectorsValid          = NumSectorsValid;
  return 0;
}

/*********************************************************************
*
*       FS_NAND_UNI_SetOnFatalErrorCallback
*
*  Function description:
*    Registers a function to be called by the driver when a fatal error happens.
*
*/
void FS_NAND_UNI_SetOnFatalErrorCallback(FS_NAND_ON_FATAL_ERROR_CALLBACK * pfOnFatalError) {
  _pfOnFatalError = pfOnFatalError;
}

/*************************** End of file ****************************/
