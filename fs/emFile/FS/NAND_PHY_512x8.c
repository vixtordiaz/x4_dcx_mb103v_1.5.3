/*********************************************************************
*                SEGGER MICROCONTROLLER GmbH & Co. KG                *
*        Solutions for real time microcontroller applications        *
**********************************************************************
*                                                                    *
*        (c) 2003-2012     SEGGER Microcontroller GmbH & Co KG       *
*                                                                    *
*        Internet: www.segger.com    Support:  support@segger.com    *
*                                                                    *
**********************************************************************

**** emFile file system for embedded applications ****
emFile is protected by international copyright laws. Knowledge of the
source code may not be used to write a similar product. This file may
only be used in accordance with a license and should not be re-
distributed in any way. We appreciate your understanding and fairness.
----------------------------------------------------------------------
File        : NAND_PHY_512x8.c
Purpose     : Small page NAND flashes physical 8-bit access
Literature  : [1] \\fileserver\techinfo\Company\Micron\NAND\MT29F2G0_8AAD_16AAD_08ABD_16ABD.pdf
---------------------------END-OF-HEADER------------------------------
*/

/*********************************************************************
*
*             #include Section
*
**********************************************************************
*/
#include "FS_Int.h"
#include "FS_CLib.h"
#include "FS_Debug.h"
#include "NAND_X_HW.h"

/*********************************************************************
*
*       Define configurable
*
**********************************************************************
*/
#ifdef FS_NAND_MAXUNIT
  #define NUM_UNITS   FS_NAND_MAXUNIT
#else
  #define NUM_UNITS   4
#endif

/*********************************************************************
*
*             #define constants
*
**********************************************************************
*/
/* Status */
#define STATUS_ERROR            0x01    /* 0:Pass,          1:Fail */
#define STATUS_READY            0x40    /* 0:Busy,          1:Ready */
#define STATUS_WRITE_PROTECTED  0x80    /* 0:Protect,       1:Not Protect */

/*********************************************************************
*

*       NAND commands
*/
#define NAND_CMD_WRITE_1         0x80
#define NAND_CMD_WRITE_2         0x10
#define NAND_CMD_READ            0x00
#define NAND_CMD_READ_SPARE      0x50
#define NAND_CMD_RESET_CHIP      0xFF
#define NAND_CMD_ERASE_1         0x60
#define NAND_CMD_ERASE_2         0xD0
#define NAND_CMD_READ_STATUS     0x70
#define NAND_CMD_READ_ID         0x90

/*********************************************************************
*
*       Static data
*
**********************************************************************
*/
static U8 _aNeed4AddrCycles[NUM_UNITS];

/*********************************************************************
*
*       Static code
*
**********************************************************************
*/

/*********************************************************************
*
*       _WriteCmd
*
*  Function description
*    Writes a single byte command to the NAND flash
*/
static void _WriteCmd(U8 Unit, U8 Cmd) {
  FS_NAND_HW_X_SetCmdMode(Unit);
  FS_NAND_HW_X_Write_x8(Unit, &Cmd, 1);
  FS_NAND_HW_X_SetDataMode(Unit);
}

/*********************************************************************
*
*       _StartOperation
*
*  Function description
*    Enables CE and writes a single byte command to the NAND flash
*/
static void _StartOperation(U8 Unit, U8 Cmd) {
  FS_NAND_HW_X_EnableCE(Unit);
  _WriteCmd(Unit, Cmd);
}

/*********************************************************************
*
*       _WriteRowAddr
*
*  Function description
*    Writes the row addr into the NAND flash.
*
*  Parameters
*    RowAddr   Zero-based page index.
*
*  Notes
*    (1) RowAddr
*        This is the zero based page index.
*        A block consists of 64 pages, so that BlockIndex = RowAddr / 64.
*/
static void _WriteRowAddr(U8 Unit, unsigned RowAddr) {
  U8 aAddr[3];
  unsigned NumBytes;

  FS_NAND_HW_X_SetAddrMode(Unit);
  FS_StoreU24LE(&aAddr[0], RowAddr);
  NumBytes = _aNeed4AddrCycles[Unit] ? 3 : 2;
  FS_NAND_HW_X_Write_x8(Unit, aAddr, NumBytes);
}

/*********************************************************************
*
*       _WriteCRAddr
*
*  Function description
*    Writes the column and row addr into the NAND flash.
*
*  Parameters
*    RowAddr   Zero-based page index.
*    ColAddr   Byte-offset within a page
*/
static void _WriteCRAddr(U8 Unit, unsigned ColAddr, unsigned RowAddr) {
  U8 aAddr[4];
  unsigned NumBytes;

  FS_NAND_HW_X_SetAddrMode(Unit);
  aAddr[0] = (U8)ColAddr;
  FS_StoreU24LE(&aAddr[1], RowAddr);
  NumBytes = _aNeed4AddrCycles[Unit] ? 4 : 3;
  FS_NAND_HW_X_Write_x8(Unit, aAddr, NumBytes);
  FS_NAND_HW_X_SetDataMode(Unit);
}

/*********************************************************************
*
*       _ReadStatus
*
*  Function description
*    Reads and returns the contents of the status register.
*/
static U8 _ReadStatus(U8 Unit) {
  U8 r;

  _WriteCmd(Unit, NAND_CMD_READ_STATUS);
  FS_NAND_HW_X_Read_x8(Unit, &r, 1);
  return r;
}

/*********************************************************************
*
*       _ResetErr
*
*  Function description
*    Resets the NAND flash by command
*/
static void _ResetErr(U8 Unit) {
  U8 Status;
  _StartOperation(Unit, NAND_CMD_RESET_CHIP);
  do {
    Status = _ReadStatus(Unit);
  } while ((Status & STATUS_READY) == 0);
  FS_NAND_HW_X_DisableCE(Unit);
}

/*********************************************************************
*
*       _WaitBusy
*
*  Function description
*    Waits until the NAND device has completed an operation
*
*  Return value:
*      0                       - Success.
*    !=0                       - An error has occurred.
*/
static int _WaitBusy(U8 Unit) {
  U8 Status;
  Status = 0;
  //
  // Try to use the hardware pin to find out when busy is cleared.
  //
  if (FS_NAND_HW_X_WaitWhileBusy(Unit, 0)) {
    //
    // Busy pin is not connected. We use status read instead.
    //
      do {
        Status = _ReadStatus(Unit);
      } while ((Status & STATUS_READY) == 0);
    } else {
      Status = _ReadStatus(Unit);
    }
  if (Status & STATUS_ERROR) {
    _ResetErr(Unit);
    return 1;                       // Error
  }
  return 0;                         // Success
}

/*********************************************************************
*
*       _EndOperation
*
*  Function description
*    Checks status register to find out if operation was successful and disables CE.
*
*  Return value:
*      0                       - Data successfully transferred.
*    !=0                       - An error has occurred.
*
*  Notes
*    (1) Status check
*        Read may have trigger an other read of the following page;
*        therefore NAND device may report itself to be busy after read.
*        Therefore BUSY-flag should not be checked.
*/
static int _EndOperation(U8 Unit) {
  U8 Status;

  Status = _ReadStatus(Unit);
  if (Status & STATUS_ERROR) {       // Note (1)
    _ResetErr(Unit);
    return 1;                        // Error
  }
  FS_NAND_HW_X_DisableCE(Unit);
  return 0;                          // O.K.
}

/*********************************************************************
*
*       _WaitEndOperation
*
*  Function description
*    Waits until the current operation is completed (Checking busy)
*    and ends operation, disabling CE.
*
*  Return value:
*      0                       - Data successfully transferred.
*      1                       - An error has occurred.
*/
static int _WaitEndOperation(U8 Unit) {
  if(_WaitBusy(Unit)) {
    return -1;
  }
  return _EndOperation(Unit);
}

/*********************************************************************
*
*       _SetOperationPointer
*/
static void _SetOperationPointer(U8 Unit, unsigned Off) {
  U8  Cmd;

  if (Off < 512) {
    Cmd = NAND_CMD_READ;
  } else {
    Cmd = NAND_CMD_READ_SPARE;
  }
  _WriteCmd(Unit, Cmd);
}


/*********************************************************************
*
*       _Read
*
*  Function description
*    Reads data from a complete or a part of a page.
*    This function is used to read either main memory or spare area.
*
*  Return value:
*      0                       - Data successfully transferred.
*    !=0                       - An error has occurred.
*/
static int _Read(U8 Unit, U32 PageNo, void * pBuffer, unsigned Off, unsigned NumBytes) {
  FS_NAND_HW_X_EnableCE(Unit);
  _SetOperationPointer(Unit, Off);
  _WriteCRAddr(Unit, Off, PageNo);
  if (_WaitBusy(Unit)) {
    return 1;                            // Error
  }
  _SetOperationPointer(Unit, Off);      // Restore the read command. It was overwritten by _WaitBusy()
  FS_NAND_HW_X_Read_x8(Unit, pBuffer, NumBytes);
  return _EndOperation(Unit);
}

/*********************************************************************
*
*       _ReadEx
*
*  Function description
*    Reads data from 2 parts of a page.
*    Typically used to read data and spare area at the same time.
*
*  Return value:
*      0                       - Data successfully transferred.
*    !=0                       - An error has occurred.
*/
static int _ReadEx(U8 Unit, U32 PageNo, void * pBuffer0, unsigned Off0, unsigned NumBytes0, void * pBuffer1, unsigned Off1, unsigned NumBytes1) {
  //
  // Perform first read operation: Read data
  //
  FS_NAND_HW_X_EnableCE(Unit);
  _SetOperationPointer(Unit, Off0);
  _WriteCRAddr(Unit, Off0, PageNo);
  if (_WaitBusy(Unit)) {
    return 1;                            // Error
  }
  _SetOperationPointer(Unit, Off0);     // Restore the read command. It was overwritten by _WaitBusy()
  FS_NAND_HW_X_Read_x8(Unit, pBuffer0, NumBytes0);
  //
  // Read dummys if there is a gap between area 0 and area 1
  //
  if (Off1 > (Off0 + NumBytes0)) {
    U8 aDummy[16];
    unsigned NumBytes;
    unsigned NumBytes2Read;

    NumBytes = Off1 - (Off0 + NumBytes0);
    do {
      NumBytes2Read = MIN(sizeof(aDummy), NumBytes);
      FS_NAND_HW_X_Read_x8(Unit, aDummy, NumBytes2Read);
      NumBytes -= NumBytes2Read;
    } while (NumBytes2Read);
  }
  //
  // Read spare area
  //
  FS_NAND_HW_X_Read_x8(Unit, pBuffer1, NumBytes1);     // Read second data (usually spare)
  return _EndOperation(Unit);
}


/*********************************************************************
*
*       _Write
*
*  Function description
*    Writes data into a complete or a part of a page.
*    This code is identical for main memory and spare area; the spare area
*    is located rigth after the main area.
*
*  Return value:
*      0                       - Data successfully transferred.
*    !=0                       - An error has occurred.
*/
static int _Write(U8 Unit, U32 PageNo, const void * pBuffer, unsigned Off, unsigned NumBytes) {
  FS_NAND_HW_X_EnableCE(Unit);
  _SetOperationPointer(Unit, Off);
  _WriteCmd(Unit, NAND_CMD_WRITE_1);
  _WriteCRAddr(Unit, Off, PageNo);
  FS_NAND_HW_X_Write_x8(Unit, pBuffer, NumBytes);
  _WriteCmd(Unit, NAND_CMD_WRITE_2);
  return _WaitEndOperation(Unit);
}

/*********************************************************************
*
*       _WriteEx
*
*  Function description
*    Writes data to 2 parts of a page.
*    Typically used to write data and spare area at the same time.
*
*  Return value:
*      0                       - Data successfully transferred.
*    !=0                       - An error has occurred.
*/
static int _WriteEx(U8 Unit, U32 PageNo, const void * pBuffer0, unsigned Off0, unsigned NumBytes0, const void * pBuffer1, unsigned Off1, unsigned NumBytes1) {
  FS_NAND_HW_X_EnableCE(Unit);
  _SetOperationPointer(Unit, Off0);
  _WriteCmd(Unit, NAND_CMD_WRITE_1);
  _WriteCRAddr(Unit, Off0, PageNo);
  FS_NAND_HW_X_Write_x8(Unit, pBuffer0, NumBytes0);
  if (Off1 > (Off0 + NumBytes0)) {
    U8 aDummy[16];
    unsigned NumBytes;
    unsigned NumBytes2Write;

    FS_MEMSET(&aDummy[0], 0xff, sizeof(aDummy));
    NumBytes = Off1 - (Off0 + NumBytes0);
    do {
      NumBytes2Write = MIN(sizeof(aDummy), NumBytes);
      FS_NAND_HW_X_Write_x8(Unit, aDummy, NumBytes2Write);
      NumBytes -= NumBytes2Write;
    } while (NumBytes2Write);
  }
  FS_NAND_HW_X_Write_x8(Unit, pBuffer1, NumBytes1);
  _WriteCmd(Unit, NAND_CMD_WRITE_2);
  return _WaitEndOperation(Unit);
}

/*********************************************************************
*
*       _EraseBlock
*
*  Function description
*    Erases a block.
*
*  Return value:
*      0                       - Data successfully transferred.
*      1                       - An error has occurred.
*/
static int _EraseBlock(U8 Unit, U32 Block) {
   _StartOperation(Unit, NAND_CMD_ERASE_1);
  _WriteRowAddr(Unit, Block);
  _WriteCmd(Unit, NAND_CMD_ERASE_2);
  return _WaitEndOperation(Unit);
}

/*********************************************************************
*
*       _InitGetDeviceInfo
*
*  Function description
*    Initializes hardware layer, resets NAND flash and tries to identify the NAND flash.
*    If the NAND flash can be handled, Device Info is filled.
*
*  Return value:
*      0                       - O.K., device can be handled
*      1                       - Error: device can not be handled
*
*  Notes
*       (1) A RESET command must be issued as the first command after power-on (see [1])
*/
static int _InitGetDeviceInfo(U8 Unit, FS_NAND_DEVICE_INFO * pDevInfo) {
  U8  aId[5];
  U8  DeviceCode;
  U16 NumBlocks;
  U8  PPB_Shift;
  U8  Need4AddrCycles;
  U32 LastPageNo;
  const U8 Dummy = 0;

  FS_NAND_HW_X_Init_x8(Unit);
  _ResetErr(Unit);         // Note 1
  //
  // Retrieve Id information from NAND device
  //
  FS_NAND_HW_X_EnableCE(Unit);
  _WriteCmd(Unit, NAND_CMD_READ_ID);
  FS_NAND_HW_X_SetAddrMode(Unit);
  FS_NAND_HW_X_Write_x8(Unit, &Dummy, 1);
  FS_NAND_HW_X_SetDataMode(Unit);
  FS_NAND_HW_X_Read_x8(Unit, &aId[0], sizeof(aId));
  FS_NAND_HW_X_DisableCE(Unit);
  DeviceCode              = aId[1];
  PPB_Shift               = 5;     // Small page NAND flashes have normally 32 pages per block
  switch(DeviceCode)   {
  case 0x6B:
  case 0xE3:
  case 0xE5:
    PPB_Shift    = 4;    // 32MBit (4MByte) small page NAND flashes have 16 pages per block
    NumBlocks    = 512;
    break;
  case 0x39:
  case 0xE6:
    PPB_Shift    = 4;    // 64MBit (8MByte) small page NAND flashes have 16 pages per block
    NumBlocks    = 1024;
    break;
  case 0x33:
  case 0x73:
    NumBlocks    = 1024;
    break;
  case 0x35:
  case 0x75:
    NumBlocks    = 2048;
    break;
  case 0x36:
  case 0x76:
    NumBlocks    = 4096;
    break;
  case 0x78:
  case 0x79:
    NumBlocks    = 8192;
    break;
  default:
    return 1;                 // Error
  }
  //
  // Check if we need 3 or 4 address cycles to access page or spare
  //
  LastPageNo = (NumBlocks << PPB_Shift) - 1;
  Need4AddrCycles = 1;
  if (LastPageNo <= 0xFFFF) {
    Need4AddrCycles = 0;
  }
  _aNeed4AddrCycles[Unit] = Need4AddrCycles;
  pDevInfo->BPP_Shift = 9;  // 512 bytes/page
  pDevInfo->PPB_Shift = PPB_Shift;
  pDevInfo->NumBlocks = NumBlocks;
  return 0;
}

/*********************************************************************
*
*       _IsWP
*
*  Function description
*    Checks if the device is write protected.
*    This is done by reading bit 7 of the status register.
*    Typical reason for write protection is that either the supply voltage is too low
*    or the /WP-pin is active (low)
*
*  Return value:
*     <0          - Error
*      0          - Not write protected
*     >0          - Write protected
*/
static int _IsWP(U8 Unit) {
  U8 Status;

  FS_NAND_HW_X_EnableCE(Unit);
  Status = _ReadStatus(Unit);
  if (_EndOperation(Unit)) {
    return -1;                    // Error
  }
  return (Status & STATUS_WRITE_PROTECTED) ? 0 : 1;
}

/*********************************************************************
*
*       Public const
*
**********************************************************************
*/
const FS_NAND_PHY_TYPE FS_NAND_PHY_512x8 = {
  _EraseBlock,
  _InitGetDeviceInfo,
  _IsWP,
  _Read,
  _ReadEx,
  _Write,
  _WriteEx
};


/*************************** End of file ****************************/
