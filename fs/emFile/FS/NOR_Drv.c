/*********************************************************************
*                SEGGER MICROCONTROLLER GmbH & Co. KG                *
*        Solutions for real time microcontroller applications        *
**********************************************************************
*                                                                    *
*        (c) 2003-2012     SEGGER Microcontroller GmbH & Co KG       *
*                                                                    *
*        Internet: www.segger.com    Support:  support@segger.com    *
*                                                                    *
**********************************************************************

**** emFile file system for embedded applications ****
emFile is protected by international copyright laws. Knowledge of the
source code may not be used to write a similar product. This file may
only be used in accordance with a license and should not be re-
distributed in any way. We appreciate your understanding and fairness.
----------------------------------------------------------------------
File        : NOR_Drv.c
Purpose     : File system high level NOR FLASH driver
----------------------------------------------------------------------
General info on the inner workings of this high level flash driver:

Layered approach
================
All read, write and erase operations are performed by the low level
flash driver. The low level flash driver is also responsible for
returning information about the sectorization of the flash.
This driver assumes the following:
- The flash is organized in physical sectors
- The phys. sectors are at least 1kb in size
- Erasing a phys. sector fills all bytes with FF
- Writing is permitted in arbitrary units (bytes)
- Writing can change bits from 1 to 0, even if the byte already
  had a value other than FF

Data storage
============
Data is stored in logical sectors of 512 bytes each. Each logical
sector has a 4 byte "Id".
A logical sector can be in 1 of 3 states:
- Blank
- Valid
- Erasable.
If the sector is blank (all bytes are ff), the Id value is FFFFFFFF.
The sector does not contain any data, but can be used to store data.
If the sector is valid, its Id value contains the logical sector
number and the data area contains valid data.
If the sector is dirty, the sector does not contain any data, but can
not be used to store data without erasing of the physical sector in
which it is contained.

Info sector
===========
The info sector is used when checking integrity of the low level format.
It's format is as follows:
Off  Type Meaning    Explanation
-----------------------------------------
0x00 U32 Signature             0x464c4153
0x04 U32 Version               Rev + (Min << 8) + (Maj << 16)
0x08 U32 NumLogSectors
0x0c U32 NumPhySectors
0x10 U32 BytesPerLogSector
0x14 U32 HasError              (0xFFFFFFFF means Format is o.k, 0xFFFFFFFE means R/O, everything else: no format)

---------------------------END-OF-HEADER------------------------------
*/

#include "FS_Int.h"
#include "NOR_Private.h"

/*********************************************************************
*
*       Configurable defines
*
**********************************************************************
*/

#ifdef    FS_NOR_MAXUNIT
  #define NUM_UNITS  FS_NOR_MAXUNIT
#else
  #define NUM_UNITS  2
#endif
#ifndef   FS_NOR_NUM_FREE_SECTORCACHE
  #define FS_NOR_NUM_FREE_SECTORCACHE    100
#endif

#ifndef   FS_NOR_MAX_ERASE_CNT_DIFF
  #define FS_NOR_MAX_ERASE_CNT_DIFF     5     // Maximum erase count difference. Usually 5% of the guaranteed erase cycles is a good value.
                                              // Low values provide better leveling, but also more overhead for copying.
#endif

#ifndef   FS_NOR_ENABLE_STATS
  #define FS_NOR_ENABLE_STATS           (FS_DEBUG_LEVEL >= FS_DEBUG_LEVEL_CHECK_ALL)   // Statistics only in debug builds
#endif

/*********************************************************************
*
*       Defines, configurable by maintainer of the code
*
**********************************************************************
*/

#define FORMAT_VERSION    1L      // Should be incremented if a format change results in an incompatible format
#define VERSION_MAJOR             FORMAT_VERSION
#define VERSION_MINOR             0x20
#define VERSION_REV               0x1

#define RESERVE           10      // Number of physical sectors to reserve in percents

#if FS_NOR_ENABLE_STATS
  #define IF_STATS(Exp) Exp
#else
  #define IF_STATS(Exp)
#endif

/*********************************************************************
*
*       Defines, fixed
*
**********************************************************************
*/
#define VERSION   ((VERSION_MAJOR<< 16L) | (VERSION_MINOR << 8) | VERSION_REV)
#define PHYS_SECTOR_SIGNATURE       0x50    // Signature value
#define NUM_RESERVED_SECTORS      0x02

#define PHY_SECTOR_TYPE_WORK        0xFF
#define PHY_SECTOR_TYPE_DATA        0x02    // This is the type required to identify a physical sector containing valid data. Everything else is invalid.
#define PHY_SECTOR_TYPE_INVALID     0

#define SECTOR_SIZE_SHIFT           8
#define MAX_SECTOR_SIZE_INDEX       10      // Defines the max. phys. sector size. 10 -> 512kb, 11 -> 1024kb, ...

#define LOG_SECTOR_ID_BLANK         0xFFFFFFFFUL      // Logical sector is blank. It can be used to store data
#define LOG_SECTOR_ID_ERASABLE      0xFFFFFFFEUL      // Logical sector is erasable. The data it contains is obsolete !
#define LOG_SECTOR_ID_INFO          0xFFFF0000UL      // Logical sector is an info sector
#define SIGNATURE                  0x464c4153UL
#define ERASE_CNT_INVALID           0xFFFFFFFFuL

#define INFO_SECTOR_OFF_SIGNATURE             0x00
#define INFO_SECTOR_OFF_VERSION               0x04
#define INFO_SECTOR_OFF_NUM_LOG_SECTORS       0x08
#define INFO_SECTOR_OFF_NUM_PHY_SECTORS       0x0C
#define INFO_SECTOR_OFF_BYTES_PER_LOG_SECTOR  0x10
#define INFO_SECTOR_OFF_HAS_ERROR             0x14

#define NOR_ERROR_STATE_OK                  0xFFFFFFFFUL
#define NOR_ERROR_STATE_READONLY            0xFFFFFFFEUL

#define WORK_SECTOR_INVALID         (-1)

/*********************************************************************
*
*       Macros, function replacement
*
**********************************************************************
*/

/*********************************************************************
*
*       ASSERT_UNIT_NO_IS_IN_RANGE
*/
#if FS_DEBUG_LEVEL >= FS_DEBUG_LEVEL_CHECK_ALL
  #define ASSERT_UNIT_NO_IS_IN_RANGE(Unit)                                  \
    if (Unit >= NUM_UNITS) {                                                \
      FS_DEBUG_ERROROUT((FS_MTYPE_DRIVER,  "NOR: Invalid unit number.\n")); \
      FS_X_Panic(FS_ERROR_INVALID_PARA);                                    \
    }
#else
  #define ASSERT_UNIT_NO_IS_IN_RANGE(Unit)
#endif

/*********************************************************************
*
*       ASSERT_PHY_TYPE_IS_SET
*/
#if FS_DEBUG_LEVEL >= FS_DEBUG_LEVEL_CHECK_ALL
  #define ASSERT_PHY_TYPE_IS_SET(pInst)                                           \
    if ((const void *)pInst->pPhyType == NULL) {                                  \
      FS_DEBUG_ERROROUT((FS_MTYPE_DRIVER,  "NOR: Physical layer is not set.\n")); \
      FS_X_Panic(FS_ERROR_UNKNOWN_DEVICE);                                        \
    }
#else
  #define ASSERT_PHY_TYPE_IS_SET(pInst)
#endif

/*********************************************************************
*
*       Function replacement macros
*
**********************************************************************
*/
/*********************************************************************
*
*       Conversion of logical sector indices to physical sector indices
*
*  The 2 macros convert a physical into a logical sector index and vice versa.
*  This is required since logical sector indices start at 0, but 0 is a reserved
*  value since it is also used to mark the sector as invalid, so the
*  physical and logical values have to be different.
*/
#define LSI2PSI(lsi) (lsi + 0x100000UL)
#define PSI2LSI(psi) (psi - 0x100000UL)

/*********************************************************************
*
*       Data types
*
**********************************************************************
*/

/*********************************************************************
*
*       LOG_SECTOR_HEADER
*
* This header is placed in front of every logical sector. With most
* external flashes, this header consists of only 4 bytes indicating
* the sector number of the data,
* 0xFFFFFFFF for blank or 0 for invalid (obsolete) data.
* For flashes with bigger flash lines and / or flashes
* which can not be rewritten without erase, the header is bigger,
* but still stores the same information.
*/
typedef struct {
  U32 Id;
  U32 Padding;
#if FS_NOR_LINE_SIZE > 8
    U8  Padding[FS_NOR_LINE_SIZE - 8];   // Pad to line size
#endif
#if FS_NOR_CAN_REWRITE == 0
    U8  IsEraseable;
    U8  Padding2[FS_NOR_LINE_SIZE - 1];
#endif
} LOG_SECTOR_HEADER;

/*********************************************************************
*
*       PHY_SECTOR_HEADER
*/
typedef struct {
  U8  Signature;
  U8  FormatVersion;
  U8  SizeIndex;               // Not used anymore but kept for the compatibility of low-level format.
  U8  Type;
  U32 EraseCnt;
  U8  aReserved[8];
#if FS_NOR_LINE_SIZE > 16
    U8  Padding[FS_NOR_LINE_SIZE - 16];
#endif
#if FS_NOR_CAN_REWRITE == 0
    U8  IsWork;
    U8  Padding2[FS_NOR_LINE_SIZE - 1];
  U8  IsValid;
  U8  Padding3[FS_NOR_LINE_SIZE - 1];
#endif
} PHY_SECTOR_HEADER;

/*********************************************************************
*
*       FREE_SECTOR_CACHE
*/
typedef struct {
  U32 RdPos;
  U32 Cnt;
  U32 aData[FS_NOR_NUM_FREE_SECTORCACHE]; // Contains offsets of free sectors
} FREE_SECTOR_CACHE;

/*********************************************************************
*
*       NOR_INST structure (and NOR_STATUS sub-structure)
*
*  This is the central data structure for the entire driver.
*  It contains data items of one instance of the driver.
*/
typedef struct {
  U8                        HasError;
  U8                        LLMountFailed;
  U8                        IsLLMounted;
  U8                        IsInited;
  int                       psiLastCleared;
  I32                       aWorkIndex[MAX_SECTOR_SIZE_INDEX + 1];
  FREE_SECTOR_CACHE         FreeSectorCache;
  U32                       WLSectorSize;                                    // Sector size for which wear leveling needs to be done
  U32                       OffInfoSector;                                   // Offset of info sector. Used as temp during LL-mount.
} NOR_STATUS;

typedef struct {
  NOR_STATUS                Status;
  U8                      * pL2P;                                            // Look-up table for logical to physical translation
  const FS_NOR_PHY_TYPE   * pPhyType;
  U8                        Unit;
  U8                        Reserve;
  U16                       SectorSize;
  U32                       NumLogSectors;                                   // Number of logical sectors (Computed from number and size of physical sectors)
  U32                       NumPhySectors;                                   // Number of physical sectors
  U32                       aNumPhySectorsPerSize[MAX_SECTOR_SIZE_INDEX + 1];
  U32                       NumBitsUsed;
#if FS_NOR_ENABLE_STATS
  FS_NOR_STAT_COUNTERS      StatCounters;
#endif
} NOR_INST;

/*********************************************************************
*
*       Static Data
*
**********************************************************************
*/
static int      _NumUnits;
static NOR_INST  *  _apInst[NUM_UNITS];

#if FS_DEBUG_LEVEL >= FS_DEBUG_LEVEL_CHECK_PARA
  static U32 _FlashStart;
  static U32 _FlashEnd;
#endif

/*********************************************************************
*
*       _SetError
*
*  Description:
*    Sets Error flag. If the error flag is set, write operations are
*    no longer permitted and are ignored.
*
*/
static void _SetError(NOR_INST * pInst) {
  pInst->Status.HasError = 1;
}

/*********************************************************************
*
*       _ReadOff
*
* Purpose:
*   Reads data from the given offset of the flash.
*/
static int _ReadOff(NOR_INST * pInst, void * pDest, U32 Off, U32 Len) {
  return pInst->pPhyType->pfReadOff(pInst->Unit, pDest, Off, Len);
}

/*********************************************************************
*
*       _SectorSize2ShiftCnt
*
*  Description:
*    Converts the size of a physical sector into a shift count.
*
*/
static int _SectorSize2ShiftCnt(U32 SectorSize) {
  int i;
  SectorSize >>= SECTOR_SIZE_SHIFT;
  for (i = 0; i <= MAX_SECTOR_SIZE_INDEX; i++) {
    if (SectorSize == 1) {
      return i;
    }
#if FS_DEBUG_LEVEL >= FS_DEBUG_LEVEL_CHECK_ALL
    if (SectorSize & 1) {
      FS_DEBUG_ERROROUT((FS_MTYPE_DRIVER,  "NOR: _SectorSize2ShiftCnt: Illegal sector size (Not a power of 2)."));
    }
#endif
    SectorSize >>= 1;
  }
  /* Error ... unsupported sector size */
  FS_DEBUG_ERROROUT((FS_MTYPE_DRIVER,  "NOR: _SectorSize2ShiftCnt: Unsupported sector size."));
  return -1;     /* Fatal error ... can not continue */
}

/*********************************************************************
*
*       _SectorShiftCnt2Size
*
*  Function description:
*    Converts the shift count of a physical sector into its size.
*
*/
static U32 _SectorShiftCnt2Size(unsigned SectorSizeIndex) {
  return 1L << (SECTOR_SIZE_SHIFT + SectorSizeIndex);
}


/*********************************************************************
*
*       _GetSectorInfo
*
*  Function description:
*    Returns the size & offset of a physical sector in bytes by querying the physical layer.
*/
static void _GetSectorInfo(NOR_INST * pInst, unsigned int PhySectorIndex, U32 * pOff, U32 * pSize) {
   pInst->pPhyType->pfGetSectorInfo(pInst->Unit, PhySectorIndex, pOff, pSize);
}

/*********************************************************************
*
*       _WriteL2PEntry
*
*  Function description
*    Writes the physical offset for sector into the look-up table
*
*  Parameters
*    LogSectorIndex   The index of the logical sector.
*                     The first sector has Index 0 !
*
*  Return value
*    The former offset is returned.
*/
static U32 _WriteL2PEntry(NOR_INST * pInst, U32 LogSectorIndex, U32 Off) {
  U32   r;

  //
  // Higher debug levels: Check if an other logical index is using the same physical offset, which would be
  // a fatal error.
  //
  // This check has been commented out as it slows down too much the low-level mount operation
  // in case of a high capacity NOR flash.
  //
#if 0  
  if (Off) {
    U32 i;
    U32      L2PEntry;

    for (i = 0; i < pInst->NumLogSectors; i++) {
      if (i != LogSectorIndex) {
        L2PEntry  = FS_BITFIELD_ReadEntry(pInst->pL2P, i, pInst->NumBitsUsed);
        if (L2PEntry == Off) {
          FS_DEBUG_ERROROUT((FS_MTYPE_DRIVER,  "NOR: _WriteL2PEntry: Physical data area identified by \"Off\" is cross-linked to 2 or more logical sectors."));
        }
      }
    }
  }
#endif
  r = FS_BITFIELD_ReadEntry(pInst->pL2P, LogSectorIndex, pInst->NumBitsUsed);
  FS_BITFIELD_WriteEntry(pInst->pL2P, LogSectorIndex, pInst->NumBitsUsed, Off);
  FS_DEBUG_LOG((FS_MTYPE_DRIVER, "_WriteL2PEntry:        LogSectorIndex: 0x%8x, NewOff: 0x%8x, PrevOff:  0x%8x\n", LogSectorIndex, Off, r));
  return r;
}


/*********************************************************************
*
*        _WriteOff
*
*  Description:
*    Writes data into the flash using the low-level flash driver.
*
*  Return value:
*    0        O.K., data has been written
*    other    Error
*/
static int _WriteOff(NOR_INST * pInst, U32 Off, const void * pSrc, U32 Len) {
  int r;
  U8  Unit;

  Unit = pInst->Unit;
#if FS_DEBUG_LEVEL >= FS_DEBUG_LEVEL_CHECK_PARA
  if ((Off < _FlashStart) || (Off > _FlashEnd) || (Off + Len > _FlashEnd)) {
    FS_DEBUG_ERROROUT((FS_MTYPE_DRIVER,  "NOR: WriteOff: Offset error, illegal write operation"));
    return 1;
  }
#endif
  r =  pInst->pPhyType->pfWriteOff(Unit, Off, pSrc, Len);
  if (r) {
    FS_DEBUG_ERROROUT((FS_MTYPE_DRIVER,  "NOR: WriteOff failed"));
  }
#if FS_DEBUG_LEVEL >= FS_DEBUG_LEVEL_CHECK_ALL
  else {
    U8  Data;
    U8  DataSrc;
    U32 i;

    for (i = 0; i < Len; i++) {
      _ReadOff(pInst, &Data, Off + i , 1);
      DataSrc = *((const U8 *)pSrc + i);
      if (Data != DataSrc) {
        FS_DEBUG_ERROROUT((FS_MTYPE_DRIVER,  "NOR: WriteOff: Data @0x%x was not programmed correctly. Expected 0x%x, read 0x%x\n", Off + i, DataSrc, Data));
        r = 1;
      }
    }
  }
#endif
  return r;
}

/*********************************************************************
*
*       _ReadLogSectorData
*
*/
static void _ReadLogSectorData(NOR_INST * pInst, U32 Off, void * p, U32 NumBytes) {
  FS_DEBUG_LOG((FS_MTYPE_DRIVER, "_ReadLogSectorData:                                Off:    0x%8x, NumBytes: 0x%8x\n", Off, NumBytes));
  _ReadOff(pInst, p, Off, NumBytes);   /* write sector data */
}

/*********************************************************************
*
*       _WriteLogSectorData
*
*/
static void _WriteLogSectorData(NOR_INST * pInst, U32 Off, const void * p, U32 NumBytes) {
  FS_DEBUG_LOG((FS_MTYPE_DRIVER, "_WriteLogSectorData:                               Off:    0x%8x, NumBytes: 0x%8x\n", Off, NumBytes));
  _WriteOff(pInst, Off, p, NumBytes);   /* write sector data */
}

/*********************************************************************
*
*        _WriteLogSectorHeader
*/
static int _WriteLogSectorHeader(NOR_INST * pInst, U32 Off, LOG_SECTOR_HEADER * pLSH) {
  FS_DEBUG_LOG((FS_MTYPE_DRIVER, "_WriteLogSectorHeader:                             Off:    0x%8x,                                  LSH: Id:   %8x\n", Off, pLSH->Id));
  return _WriteOff(pInst, Off, pLSH, sizeof(LOG_SECTOR_HEADER)); // write sector id
}

/*********************************************************************
*
*        _WriteLogSectorIndex
*
*  Function description
*    Writes the initial sector index into the logical header
*/
static void _WriteLogSectorInfo(NOR_INST * pInst, U32 Off, U32 LogSectorId) {
  LOG_SECTOR_HEADER lsh;

  FS_MEMSET(&lsh, 0xff, sizeof(lsh));
  //
  //  Write
  //
  if (LogSectorId < pInst->NumLogSectors) {
    lsh.Id = LSI2PSI(LogSectorId);
  } else {
    lsh.Id = LogSectorId;      // Handle special cases such as the info sector
  }
  _WriteLogSectorHeader(pInst, Off, &lsh);
}

/*********************************************************************
*
*        _CopySectorData
*
*  Function description
*    Copies the data part of a sector. The logical sector header is
*    not copied.
*
*  Notes
*    (1) The sector is copied in multiple chunks, where a small
*        array on the stack is used as buffer. The reason why the data
*        is copied in multiple chunks is simply to keep the stack load low.
*
*/
static int _CopySectorData(NOR_INST * pInst, U32 DestAddr, U32 SrcAddr) {
  U32  acBuffer[32];
  int r;
  unsigned NumBytesAtOnce;
  unsigned NumBytes;

  FS_DEBUG_LOG((FS_MTYPE_DRIVER, "_CopySectorData:                                   SrcOff: 0x%8x, DestOff:  0x%8x,                                                      Len: 0x%8x\n", SrcAddr, DestAddr, pInst->SectorSize));
  r = 0;
  NumBytes = pInst->SectorSize;
  do {
    NumBytesAtOnce = MIN(NumBytes, sizeof(acBuffer));
    _ReadOff (pInst, acBuffer, SrcAddr,  NumBytesAtOnce);
    r |= _WriteOff(pInst, DestAddr, acBuffer, NumBytesAtOnce);
    NumBytes -= NumBytesAtOnce;
    SrcAddr  += NumBytesAtOnce;
    DestAddr += NumBytesAtOnce;
  } while (NumBytes);
  IF_STATS(++pInst->StatCounters.CopySectorCnt);
  return r;
}


/*********************************************************************
*
*       _FindLogSector
*
*  Function description
*    Locates the specified logical sector in flash memory.
*
*  Parameters:
*    LogSectorIndex   The index of the logical sector.
*                     The first sector has Index 0 !
*
*  Return value:
*    >0                        - Address offset of found sector
*    ==0                       - Sector not found.
*
*/
static U32 _FindLogSector(NOR_INST * pInst, U32 LogSectorIndex) {
  U32 r;
  //
  // Check parameters (higher debug level only)
  //
  #if FS_DEBUG_LEVEL >= FS_DEBUG_LEVEL_CHECK_PARA
    if (LogSectorIndex >= pInst->NumLogSectors) { /* test if ID within valid range */
      FS_DEBUG_ERROROUT((FS_MTYPE_DRIVER,  "NOR: _FindLogSector: LogSectorIndex out of range."));
      return 0;
    }
  #endif
  r = FS_BITFIELD_ReadEntry(pInst->pL2P, LogSectorIndex, pInst->NumBitsUsed);
  return r;   /* Look-up table for logical to physical translation */
}


/*********************************************************************
*
*       _GetLogSectorIndex
*
*  Function description
*    Returns the Index of a logical sector, specified by its Index.
*    The return value can be a sector number or a special code for special function
*    sectors or types such as "blank"
*
*  Return value:
*    0 .. 0xFFFF0000           - Index of logical sector
*    >=   0xFFFF0000           - not a logical sector (special code)
*/
static U32 _GetLogSectorIndex(NOR_INST * pInst, U32 Off) {
  U32 LogSectorIndex;
  LOG_SECTOR_HEADER lsh;

  _ReadOff(pInst, &lsh, Off, sizeof(LOG_SECTOR_HEADER));
  switch (lsh.Id) {
  case 0xFFFFFFFFUL:
    LogSectorIndex = LOG_SECTOR_ID_BLANK;
    break;
  case LOG_SECTOR_ID_INFO:
    LogSectorIndex = LOG_SECTOR_ID_INFO;
    break;
  default:
#if FS_NOR_CAN_REWRITE
      if (lsh.Id == 0) {
        LogSectorIndex = LOG_SECTOR_ID_ERASABLE;
        break;
      }
#else
      if (lsh.IsEraseable == 0) {
        LogSectorIndex = LOG_SECTOR_ID_ERASABLE;
        break;
      }
#endif
    LogSectorIndex = PSI2LSI(lsh.Id);
    if (LogSectorIndex >= pInst->NumLogSectors) {
      FS_DEBUG_WARN((FS_MTYPE_DRIVER, "NOR: Logical sector index out of bounds."));
      LogSectorIndex = LOG_SECTOR_ID_ERASABLE;
    }
    break;
  }
  FS_DEBUG_LOG((FS_MTYPE_DRIVER, "_GetLogSectorIndex:                                Off:    0x%8x,                                  LSH: Id:   %8x\n", Off, lsh.Id));
  return LogSectorIndex;
}

/*********************************************************************
*
*       _GetEraseCnt
*
*/
static U32 _GetEraseCnt(PHY_SECTOR_HEADER * pPSH) {
  return pPSH->EraseCnt;
}

/*********************************************************************
*
*       _GetPhySectorType
*
*/
static U8 _GetPhySectorType(PHY_SECTOR_HEADER * pPSH) {
#if FS_NOR_CAN_REWRITE
  return pPSH->Type;
#else
  if (pPSH->IsValid == 0) {
    return PHY_SECTOR_TYPE_INVALID;
  }
  if (pPSH->IsWork) {
    return PHY_SECTOR_TYPE_WORK;
  }
  return PHY_SECTOR_TYPE_DATA;
#endif
}

/*********************************************************************
*
*       _ReadPSH
*
*  Function description
*    Read physical sector header
*/
static void _ReadPSH(NOR_INST * pInst, U32 Off, PHY_SECTOR_HEADER * pPSH) {
  _ReadOff(pInst, pPSH, Off, sizeof(PHY_SECTOR_HEADER ));
  FS_DEBUG_LOG((FS_MTYPE_DRIVER, "_ReadPSH:                                          Off:    0x%8x,                                  PSH: Type: %8x, EraseCnt: 0x%8x\n", Off, pPSH->Type, pPSH->EraseCnt));
}

/*********************************************************************
*
*       _WritePSH
*
*  Function description
*    Write physical sector header
*/
static int _WritePSH(NOR_INST * pInst, U32 Off, PHY_SECTOR_HEADER * pPSH) {
  FS_DEBUG_LOG((FS_MTYPE_DRIVER, "_WritePSH:                                         Off:    0x%8x,                                  PSH: Type: %8x, EraseCnt: 0x%8x\n", Off, pPSH->Type, pPSH->EraseCnt));
  return _WriteOff(pInst, Off, pPSH, sizeof(PHY_SECTOR_HEADER));
}

/*********************************************************************
*
*       _GetMaxEraseCnt
*
*  Function description
*    Returns the highest erase countof all physical data sectors of the given size.
*    This can be useful if the erase count of a physical sector has been lost because
*    of a power failure during or right after erase of the sector.
*/
static U32 _GetMaxEraseCnt(NOR_INST * pInst, U32 SectorSize) {
  U32 NumPhySectors;
  U32 MaxCnt;
  U32 Addr;
  U32 Size;
  U32 EraseCnt;
  U32 i;
  PHY_SECTOR_HEADER psh;

  NumPhySectors = pInst->NumPhySectors;
  MaxCnt = 0;
  for (i = 0; i < NumPhySectors; i++) {
    _GetSectorInfo(pInst, i, &Addr, &Size);
    if (Size == SectorSize) {
      _ReadPSH(pInst, Addr, &psh);
      if (_GetPhySectorType(&psh) == PHY_SECTOR_TYPE_DATA) {
        EraseCnt = _GetEraseCnt(&psh);
        if ((EraseCnt > MaxCnt) && (EraseCnt != 0xFFFFFFFFUL)) {
          MaxCnt = EraseCnt;
        }
      }
    }
  }
  return MaxCnt;
}

/*********************************************************************
*
*       _SetPhySectorType
*
*  Function description
*     Modifles in the type of physical sector.
*/
static void _SetPhySectorType(PHY_SECTOR_HEADER * pPSH, U8 SectorType) {
#if FS_NOR_CAN_REWRITE
  pPSH->Type = SectorType;
#else
  if (SectorType == PHY_SECTOR_TYPE_INVALID) {
    pPSH->IsValid = 0;
  } else if (SectorType == PHY_SECTOR_TYPE_DATA) {
    pPSH->IsWork = 0;
  }
#endif
}

/*********************************************************************
*
*       _InvalidatePhySector
*
*  Function description
*    Invalidate the physical sector.
*    This is done typically before the sector is erased, in order to avoid that a
*    partial erase leads to data corruption.
*    This way, if the sector is not completly erased, it will not be mounted in the
*    case the header is still intact (but data is not).
*    We currently only invalidate the physical header, but could also invalidate
*    logical headers (which would eat up some more time)
*/
static void _InvalidatePhySector(NOR_INST * pInst, U32 PhySectorIndex) {
  PHY_SECTOR_HEADER psh;
  U32                SectorOff;

  _GetSectorInfo(pInst, PhySectorIndex, &SectorOff, NULL);
  _ReadOff(pInst, &psh, SectorOff, sizeof(psh));
  _SetPhySectorType(&psh, PHY_SECTOR_TYPE_INVALID);
  _WriteOff(pInst, SectorOff, &psh, sizeof(psh));
  FS_DEBUG_LOG((FS_MTYPE_DRIVER, "_InvalidatePhySector:  PhySectorIndex: 0x%8x, Off:    0x%8x\n", PhySectorIndex, SectorOff));
}

/*********************************************************************
*
*       _ContainsErasable
*
*  Description:
*    Search for a physical flash sector that contains
*    eraseable logical sectors and return its ID.
*
*  Parameters:
*    Sector offset             - Physical address of the flash sector
*    Sector length             - Length of the flash sector
*
*  Return value:
*    1                         - there is at least one free logical sector
*    ==0                       - no free sector within this flash sector
*
*/
static int _ContainsErasable(NOR_INST * pInst, U32 SecOff, U32 SecLen) {
  int                r;
  PHY_SECTOR_HEADER psh;
  unsigned           LogSectorSize;

  r = 0;                       // Set to 0 -> no erasable sector found
  //
  // Make sure this is a data buffer.
  //
  _ReadPSH(pInst, SecOff, &psh);
  if (_GetPhySectorType(&psh) != PHY_SECTOR_TYPE_DATA) {
    goto Done;
  }
  //
  // Try to find an erasable sector
  //
  SecLen  -= sizeof(PHY_SECTOR_HEADER);
  SecOff  += sizeof(PHY_SECTOR_HEADER);
  LogSectorSize = sizeof(LOG_SECTOR_HEADER) + pInst->SectorSize;
  while (SecLen >= LogSectorSize) {
    U32 LogSecIndex;
    LogSecIndex = _GetLogSectorIndex(pInst, SecOff);
    if (LogSecIndex == LOG_SECTOR_ID_ERASABLE) {
      r = 1;               // Erasable sector found !
      break;
    }
    SecLen  -= LogSectorSize;
    SecOff  += LogSectorSize;
  }
Done:
  FS_DEBUG_LOG((FS_MTYPE_DRIVER, "_ContainsErasable:                                        Return:   0x%8x\n", r));
  return r;                      /* No erasable sector found */
}


/*********************************************************************
*
*       _IsPhySectorFree
*
*  Description:
*    Checks if a physical sector is free, so it can be used as work buffer
*
*  Parameters:
*    PhySectorIndex   - Physical sector to be checked
*
*  Return value:
*    ==1              - Physical sector is free
*    ==0              - Physical sector is not free
*
*/
static int _IsPhySectorFree(NOR_INST * pInst, U32 PhySectorIndex) {
  PHY_SECTOR_HEADER psh;
  U32                SectorOff, SectorLen;
  int                r;
  unsigned           LogSectorSize;

  r = 1;            // Default: Physical sector is free
  _GetSectorInfo(pInst, PhySectorIndex, &SectorOff, &SectorLen);
  //
  // If it is not a data sector, this sector is free
  //
  _ReadPSH(pInst, SectorOff, &psh);
  if (_GetPhySectorType(&psh) != PHY_SECTOR_TYPE_DATA) {
    goto Done;
  }
  //
  // Try to find an erasable sector
  //
  SectorLen  -= sizeof(PHY_SECTOR_HEADER);
  SectorOff  += sizeof(PHY_SECTOR_HEADER);
  LogSectorSize = sizeof(LOG_SECTOR_HEADER) + pInst->SectorSize;
  while (SectorLen >= LogSectorSize) {
    U32 LogSecIndex;
    LogSecIndex = _GetLogSectorIndex(pInst, SectorOff);
    if ((LogSecIndex <= pInst->NumLogSectors) ||  (LogSecIndex == LOG_SECTOR_ID_INFO)) {
      r = 0;                  // Physical sector is not free
      break;
    }
    SectorLen  -= LogSectorSize;
    SectorOff  += LogSectorSize;
  }
Done:
  FS_DEBUG_LOG((FS_MTYPE_DRIVER, "_IsPhySectorFree:                                                  Return:   0x%8x\n", r));
  return r;
}


/*********************************************************************
*
*        _MarkLogSectorAsInvalid
*
* Description:
*   Marks a logical sector as invalid.
*   This routine is called from the higher layer file-system to
*   help the driver to manage the data:
*   This way sectors which are no longer in use by the higher
*   layer file system do not need to be copied.
*/
static void _MarkLogSectorAsInvalid(NOR_INST * pInst, U32 Off) {
  LOG_SECTOR_HEADER LogHeader;
  U8                IsUpdateRequired;

  _ReadOff(pInst,&LogHeader, Off, sizeof(LOG_SECTOR_HEADER));
  IsUpdateRequired = 0;
#if FS_NOR_CAN_REWRITE
  if (LogHeader.Id != 0) {
  LogHeader.Id = 0;
     IsUpdateRequired = 1;
   }
#else
   if (LogHeader.IsEraseable != 0) {
  LogHeader.IsEraseable = 0;
     IsUpdateRequired = 1;
   }
#endif
  if (IsUpdateRequired) {
    _WriteLogSectorHeader(pInst, Off, &LogHeader);  // mark sector as eraseable
  }
}


/*********************************************************************
*
*        _FreeSectors
*
* Description:
*   Marks a logical sector as free.
*   This routine is called from the higher layer file-system to
*   help the driver to manage the data:
*   This way sectors which are no longer in use by the higher
*   layer file system do not need to be copied.
*/
static void _FreeSectors(NOR_INST * pInst, U32 LogSectorIndex, U32 NumSectors) {
  /*
   * Check parameters (higher debug level only)
   */
#if FS_DEBUG_LEVEL >= FS_DEBUG_LEVEL_CHECK_PARA
    if (LogSectorIndex >= pInst->NumLogSectors) { /* test if ID within valid range */
      FS_DEBUG_ERROROUT((FS_MTYPE_DRIVER,  "NOR: _FreeSectors: LogSectorIndex out of range."));
      return;
    }
#endif
  do {
    U32 PhysAddr;
    PhysAddr = _FindLogSector(pInst, LogSectorIndex);           /* check if sector does already exist */
    if (PhysAddr) {
      _MarkLogSectorAsInvalid(pInst, PhysAddr);
      _WriteL2PEntry(pInst, LogSectorIndex, 0);
    }
    LogSectorIndex++;
  } while (--NumSectors);
}

/*********************************************************************
*
*        _ErasePhySector
*
*   Function description
*     Sets to 1 all the bits in a physical sector.
*/
static int _ErasePhySector(NOR_INST * pInst, U32 PhySectorIndex) {
  int r;

  r = pInst->pPhyType->pfEraseSector(pInst->Unit, PhySectorIndex);
  if (r) {
    FS_DEBUG_ERROROUT((FS_MTYPE_DRIVER,  "NOR: Erase operation failed.\n"));
    return r;
  }
  IF_STATS(pInst->StatCounters.EraseCnt++);     // Increment statistics counter if enabled
  //
  // On higher debug levels, check if erase worked
  //
#if FS_DEBUG_LEVEL >= FS_DEBUG_LEVEL_CHECK_ALL
  {
    U32 Off;
    U32 Size;
    U8  aData[32];
    _GetSectorInfo(pInst, PhySectorIndex, &Off, &Size);
    FS_DEBUG_LOG((FS_MTYPE_DRIVER, "_EraseSector:        Off: 0x%8x, Len: 0x%8x.\n", Off, Size));
    while (Size -= sizeof(aData)) {
      unsigned i;
      _ReadOff(pInst, &aData[0], Off, sizeof(aData));
      for (i = 0; i < sizeof(aData); i++) {
        if (aData[i] != 0xff) {
          FS_DEBUG_ERROROUT((FS_MTYPE_DRIVER,  "NOR: Erase verification failed @ Off 0x%8x.\n", Off + i));
        }
      }
      Off += sizeof(aData);
    }
  }
#endif
  return r;
}

/*********************************************************************
*
*       _MarkPhySectorAsData
*
*  Function description
*    Marks a physical sector as data sector. The phys. sector header
*    must have been written before.
*/
static void _MarkPhySectorAsData(NOR_INST * pInst, U32 PhySectorIndex) {
  PHY_SECTOR_HEADER psh;
  U32                SectorOff;

  _GetSectorInfo(pInst, PhySectorIndex, &SectorOff, NULL);
  _ReadOff(pInst, &psh, SectorOff, sizeof(PHY_SECTOR_HEADER));
  _SetPhySectorType(&psh, PHY_SECTOR_TYPE_DATA);
  _WriteOff(pInst, SectorOff, &psh, sizeof(PHY_SECTOR_HEADER));
  FS_DEBUG_LOG((FS_MTYPE_DRIVER, "_MarkPhySectorAsData:  PhySectorIndex: 0x%8x, Off: 0x%8x.\n", PhySectorIndex, SectorOff));
}

/*********************************************************************
*
*       _AddFreeSectorToCache
*
*  Function description
*    Adds a free sector to the free sector list.
*    If there is no space in the free sector list, the entry is not added.
*    Not adding the entry is no problem since the list is filled up if it is empty;
*    This will only cost some time, but will work fine.
*
*  Parameters
*    Off       The offset of the free logical sector to add.
*/
static void _AddFreeSectorToCache(NOR_INST * pInst, U32 Off) {
  unsigned WrPos;
  unsigned CountOf;

  CountOf = COUNTOF(pInst->Status.FreeSectorCache.aData);
  if (pInst->Status.FreeSectorCache.Cnt >= CountOf) {
    return;       // Cache is already full
  }
  WrPos = pInst->Status.FreeSectorCache.RdPos + pInst->Status.FreeSectorCache.Cnt;
  if (WrPos >= CountOf) {
    WrPos -= CountOf;
  }
  pInst->Status.FreeSectorCache.aData[WrPos] = Off;
  pInst->Status.FreeSectorCache.Cnt++;
}

/*********************************************************************
*
*       _ChangeWorkSector
*
*  Function description
*    Sets the index of a physical sector to be used as work sector.
*
*  Parameters
*    PhySectorIndex       index of the physical sector that needs to be invalidated.
*
*  Note
*    When a data sector is changed to a work sector,
*    the FreeSectorCache table may contain entries from this physical sector.
*    These entries need to be invalidated in the FreeSectorCache.
*/
static void _ChangeWorkSector(NOR_INST * pInst, U32 PhySectorIndex) {
  U32 StartOff;
  U32 EndOff;
  U32 Len;
  unsigned CountOf;
  unsigned i;
  int SectorShiftCnt;

  _GetSectorInfo(pInst, PhySectorIndex, &StartOff, &Len);
  //
  // Setup the new work sector
  // _SectorSize2ShiftCnt() should always return a valid shift count here, since this errors are already handled in LowLevelMount
  //
  SectorShiftCnt = _SectorSize2ShiftCnt(Len);
  pInst->Status.aWorkIndex[SectorShiftCnt] = PhySectorIndex;
  //
  // Make sure that there are no entries in the free list for this phy. sector
  //
  EndOff  = StartOff + Len - 1;
  CountOf = COUNTOF(pInst->Status.FreeSectorCache.aData);
  for (i = 0; i < CountOf; i++) {
    U32 CacheEntry;

    CacheEntry = pInst->Status.FreeSectorCache.aData[i];
    if ((StartOff <= CacheEntry) && ((CacheEntry < EndOff))) {
       pInst->Status.FreeSectorCache.aData[i] = 0;
    }
  }
}

/*********************************************************************
*
*       _RemoveFreeSectorFromCache
*
*  Function description
*    Removes a free sector from the free sector list.
*
*  Return value
*    Off       The offset of the free logical sector
*/
static U32 _RemoveFreeSectorFromCache(NOR_INST * pInst) {
  U32 Off;
  unsigned RdPos;

  //
  // If the cache is empty, return 0
  //
  if (pInst->Status.FreeSectorCache.Cnt == 0) {
    return 0;
  }
  RdPos = pInst->Status.FreeSectorCache.RdPos;
  Off   = pInst->Status.FreeSectorCache.aData[RdPos];
  RdPos++;
  if (RdPos >= FS_NOR_NUM_FREE_SECTORCACHE) {
    RdPos = 0;
  }
  pInst->Status.FreeSectorCache.RdPos = RdPos;
  pInst->Status.FreeSectorCache.Cnt--;
  //
  // Higher debug levels: Check if this Offset is currently in use
  // which would be a fatal error.
  //
#if FS_DEBUG_LEVEL >= FS_DEBUG_LEVEL_CHECK_ALL
  if (Off) {
    unsigned i;
    for (i = 0; i < pInst->NumLogSectors; i++) {
      U32 L2PEntry;

      L2PEntry  = FS_BITFIELD_ReadEntry(pInst->pL2P, i, pInst->NumBitsUsed);
      if (L2PEntry == Off) {
        FS_DEBUG_ERROROUT((FS_MTYPE_DRIVER,  "NOR: _RemoveFreeSectorFromCache: Free sector is in use"));
      }
    }
  }
#endif
  return Off;
}

/*********************************************************************
*
*       _CopyDataSector
*
*  Function description
*    Copies the data contained in one physical sector into an other physical sector of the same size.
*    All valid logical sectors inside this physical sector are copied; the invalid / unused parts of the old sector
*    are not copied and left blank in the destination sector. The blank parts of the destination sector
*    can later on be used to store other data.
*/
static void _CopyDataSector(NOR_INST * pInst, U32 DstPhySec, U32 SrcPhySec) {
  PHY_SECTOR_HEADER psh;
  U32 DstAddr, DstLen;
  U32 SrcAddr, SrcLen;
  U32 SrcAddrStart;
  U32 DstAddrStart;
  U32 EraseCnt;
  unsigned LogSectorSize;

  _GetSectorInfo(pInst, DstPhySec, &DstAddr, &DstLen);
  _GetSectorInfo(pInst, SrcPhySec, &SrcAddr, &SrcLen);
  DstAddrStart = DstAddr;      // Remember it for later
  SrcAddrStart = SrcAddr;
  //
  // Erase destination sector. Before doing so, we need to read the psh of both Source and destination.
  // psh of the destination is psh of the source with EraseCnt of destination.
  //
  _ReadPSH(pInst, DstAddr, &psh);
  EraseCnt = _GetEraseCnt(&psh);
  _ReadPSH(pInst, SrcAddr, &psh);
  if (EraseCnt == ERASE_CNT_INVALID) {
    EraseCnt = _GetMaxEraseCnt(pInst, SrcLen);
  } else {
    EraseCnt++;
  }
  psh.EraseCnt = EraseCnt;
  psh.Type     = 0xFF;
  _ErasePhySector(pInst, DstPhySec);
  _WritePSH(pInst, DstAddrStart, &psh);
  //
  // Copy all logical sectors containing data
  //
  pInst->Status.WLSectorSize = DstLen;                // Check wear level of all sector of this size on next occasion
  SrcAddr += sizeof(PHY_SECTOR_HEADER);
  DstAddr += sizeof(PHY_SECTOR_HEADER);
  LogSectorSize = sizeof(LOG_SECTOR_HEADER) + pInst->SectorSize;
  while ((SrcAddr + LogSectorSize) <= (SrcAddrStart + SrcLen)) {
    U32 LogSectorIndex = _GetLogSectorIndex(pInst, SrcAddr);
    if ((LogSectorIndex < pInst->NumLogSectors) || (LogSectorIndex == LOG_SECTOR_ID_INFO)) { // Does this sector contain data ? If so, copy it.
      //
      // Copy the data portion of log. sector
      //
      _CopySectorData(pInst, DstAddr + sizeof(LOG_SECTOR_HEADER), SrcAddr + sizeof(LOG_SECTOR_HEADER));
      //
      //  Update the logical sector header
      //
      _WriteLogSectorInfo(pInst, DstAddr, LogSectorIndex);
      //
      //  Update L2P table
      //
      if (LogSectorIndex != LOG_SECTOR_ID_INFO) {
        if (_WriteL2PEntry(pInst, LogSectorIndex, DstAddr) != SrcAddr) {
          FS_DEBUG_ERROROUT((FS_MTYPE_DRIVER,  "NOR: L2P entry is not correct"));
        }
      }
    } else {
      _AddFreeSectorToCache(pInst, DstAddr);
    }
    SrcAddr += LogSectorSize;
    DstAddr += LogSectorSize;
  }
  //
  // Mark target data as valid data sector and source as invalid.
  // Note that this is a critical point for stability: If we have an unexpected RESET
  // after validating the destination, but before invalidating the source, we will have 2 physical sectors
  // with identical logical sectors. This needs to be handled correctly.
  //
  _MarkPhySectorAsData(pInst, DstPhySec);
  _InvalidatePhySector(pInst, SrcPhySec);         // TestPoint: Set break and RESET here.
  _ChangeWorkSector(pInst, SrcPhySec);
}

/*********************************************************************
*
*       _GetWorkSectorIndex
*
*   Function description
*    Returns the index of the physcial work sector for the given sector size.
*/
static I32 _GetWorkSectorIndex(NOR_INST * pInst, U32 SectorSize) {
  I32 WorkSectorIndex;
  int SectorShiftCnt;

  //
  // _SectorSize2ShiftCnt() should always return a valid shift count here, since this errors are already handled in LowLevelMount
  //
  SectorShiftCnt  = _SectorSize2ShiftCnt(SectorSize);
  WorkSectorIndex = pInst->Status.aWorkIndex[SectorShiftCnt];
  //
  // On higher debug levels heck if we have a valid work sector and if no L2P entries are in it.
  //
#if FS_DEBUG_LEVEL >= FS_DEBUG_LEVEL_CHECK_ALL
  if ((U32)WorkSectorIndex >= pInst->NumPhySectors) {
    FS_DEBUG_ERROROUT((FS_MTYPE_DRIVER,  "NOR: Invalid work sector index.\n"));
  } else {
    unsigned i;
    U32 Off;
    U32 StartOff;
    U32 Size;

    _GetSectorInfo(pInst, WorkSectorIndex, &StartOff, &Size);
    for (i = 0; i < pInst->NumLogSectors; i++) {
      Off = FS_BITFIELD_ReadEntry(pInst->pL2P, i, pInst->NumBitsUsed);
      if ((Off != 0) && (Off >= StartOff) && (Off < StartOff + Size)) {
        FS_DEBUG_ERROROUT((FS_MTYPE_DRIVER,  "NOR: Physical work sector still contains valid data.\n"));
      }
    }
  }
#endif
  return WorkSectorIndex;
}

/*********************************************************************
*
*       _SetWorkSectorIndex
*
*   Function description
*     Sets the index of a physical sector for a given sector size.
*/
static void _SetWorkSectorIndex(NOR_INST * pInst, U32 SectorSize, I32 WorkSectorIndex) {
  int SectorShiftCnt;

  //
  // _SectorSize2ShiftCnt() should always return a valid shift count here, since this errors are already handled in LowLevelMount.
  //
  SectorShiftCnt = _SectorSize2ShiftCnt(SectorSize);
  pInst->Status.aWorkIndex[SectorShiftCnt] = WorkSectorIndex;
}

/*********************************************************************
*
*       _WearLevel
*
*  Description:
*    Perform the actual wear leveling.
*
*  Notes
*    (1) The sector index given is the index of the work sector. If
*        this work sector has been erased a lot more often than an other
*        physical sector of the same size, this sector is consider "fitter".
*        In this case, the contents of the fitter sector are copied
*        to the work sector and the fitter sector is used as work sector.
*
*/
static void _WearLevel(NOR_INST * pInst) {
  PHY_SECTOR_HEADER psh;
  U32 Addr;
  U32 MinCnt;
  U32 MinCntSector;
  U32 EraseCnt;
  U32 Size;
  U32 WLSize;
  U32 WLEraseCnt;
  int NumPhySectors;
  int i;
  //
  // Check if wear leveling is required (usualy only after a ERASE)
  //
  if (pInst->Status.WLSectorSize == 0) {
    return;     // Nothing to do
  }
  WLSize = pInst->Status.WLSectorSize;
  MinCntSector = 0;
  //
  // Find sector of the same size with smallest erase count
  //
  NumPhySectors = pInst->NumPhySectors;
  MinCnt = 0xFFFFFFFFUL;
  for (i = 0; i < NumPhySectors; i++) {
    _GetSectorInfo(pInst, i, &Addr, &Size);
    if (Size == WLSize) {
      _ReadPSH(pInst, Addr, &psh);
      if (_GetPhySectorType(&psh) == PHY_SECTOR_TYPE_DATA) {
        EraseCnt = _GetEraseCnt(&psh);
        if (EraseCnt < MinCnt) {
          MinCnt = EraseCnt;
          MinCntSector = i;
        }
      }
    }
  }
  //
  // Copy data if it makes sense, meaning if the erase count difference exceed a certain predefined number.
  //
  i = _GetWorkSectorIndex(pInst, WLSize);
  _GetSectorInfo(pInst, i, &Addr, NULL);
  _ReadOff(pInst, &psh, Addr, sizeof(psh));
  WLEraseCnt = _GetEraseCnt(&psh);
  if (WLEraseCnt > MinCnt + FS_NOR_MAX_ERASE_CNT_DIFF) {
    //
    // Invalidate old Work sector
    //
    pInst->Status.aWorkIndex[_SectorSize2ShiftCnt(WLSize)] = -1; // _SectorSize2ShiftCnt() should always return a valid shift count here, since this errors are already handled in LowLevelMount
    _CopyDataSector(pInst, i, MinCntSector);
  }
  pInst->Status.WLSectorSize = 0;      // Do not perform this again until next erase
}

/*********************************************************************
*
*             _CalcNumLogSectors
*
*  Description:
*    Calculate number of logical sectors.
*
*  Return value:
*    Number of available sectors.
*
*  Notes
*    (1) This function does not need to access the flash. It simply performs this calculation
*        based on the size and number of available physical sectors.
*/
static U32 _CalcNumLogSectors(NOR_INST * pInst) {
  U32 LogSectorCnt;
  U32 LSectorSize;
  U32 NumPhySectors;
  U32 LSectorsPerPSector;
  unsigned i;
  unsigned LogSectorSize;

  LogSectorCnt = 0;
  LogSectorSize = sizeof(LOG_SECTOR_HEADER) + pInst->SectorSize;
  for (i = 0; i < COUNTOF(pInst->Status.aWorkIndex); i++) {        //  Iterate over all sizes
    NumPhySectors = pInst->aNumPhySectorsPerSize[i];
    if (NumPhySectors) {
      LSectorSize = _SectorShiftCnt2Size(i);
      LSectorsPerPSector = (LSectorSize - sizeof(PHY_SECTOR_HEADER)) / LogSectorSize;
      LogSectorCnt += LSectorsPerPSector * (NumPhySectors - 1);
    }
  }
  LogSectorCnt = (LogSectorCnt * (100 - pInst->Reserve)) / 100;
  return LogSectorCnt;
}

/*********************************************************************
*
*       _InitSizeInfo
*
*  Description:
*    Initialize SizeInfo
*
*/
static void _InitSizeInfo(NOR_INST * pInst) {
  U32 SecOff;
  U32 SecLen;
  int    NumPhySectors;
  int    i;
  int    SizeIndex;

  FS_MEMSET(pInst->Status.aWorkIndex, 0, sizeof(pInst->Status.aWorkIndex));  /* Not required, to be safe for re-init */
  NumPhySectors = pInst->NumPhySectors;
  for (i = 0; i < NumPhySectors; i++) {
    _GetSectorInfo(pInst, i, &SecOff, &SecLen);
    SizeIndex = _SectorSize2ShiftCnt(SecLen);
    if (SizeIndex == -1) {
      return;
    }
    pInst->aNumPhySectorsPerSize[SizeIndex]++;
    #if FS_DEBUG_LEVEL >= FS_DEBUG_LEVEL_CHECK_PARA
    if (_FlashStart > SecOff) {
      _FlashStart = SecOff;
    }
    if (_FlashEnd < (SecOff + SecLen)) {
      _FlashEnd = SecOff + SecLen;
    }
    #endif
  }
}

/*********************************************************************
*
*       _InitStatus
*
*  Description:
*    Initialize the status pointer.
*    This makes sure the all routine that depend on this status
*    are working with correct status information.
*
*/
static void _InitStatus(NOR_INST * pInst) {
  NOR_STATUS * pStatus;
  unsigned     i;
  U32          SizeOfBitField;
  U32          Off, Len, LastOff;

  pStatus = &pInst->Status;
  //
  //  Invalidate all information with 0.
  //
  FS_MEMSET(pStatus, 0, sizeof(NOR_STATUS));
  //
  // Mark all work sector as unknown
  //
  for (i = 0; i < COUNTOF(pStatus->aWorkIndex); i++) {
    pInst->Status.aWorkIndex[i] = -1;
  }
  //
  //  Initialize the logical 2 physical table
  //
  pInst->pPhyType->pfGetSectorInfo(pInst->Unit, pInst->NumPhySectors - 1, &Off, &Len);
  LastOff = Off + Len - 1;
  pInst->NumBitsUsed = FS_BITFIELD_CalcNumBitsUsed(LastOff);
  SizeOfBitField = FS_BITFIELD_CalcSize(pInst->NumLogSectors, pInst->NumBitsUsed);
  if (pInst->NumLogSectors) {
    FS_AllocZeroedPtr((void **)&pInst->pL2P, SizeOfBitField);  // If allocation of memory fails, the file system will not return from here and stays in FS_X_Panic()
  }
}

/*********************************************************************
*
*       _CheckInfoSector
*
*  Function description
*    Checks if the information in the info sector matches the current version and settings.
*
*  Return value:
*     0  O.K.
*  != 0  Error
*/
static int _CheckInfoSector(NOR_INST * pInst, U32 Off) {
  U32 aInfo[8];
  U32 DriveState;

  _ReadOff(pInst, aInfo, Off + sizeof(LOG_SECTOR_HEADER), sizeof(aInfo));   /* write sector data */
  if (aInfo[INFO_SECTOR_OFF_SIGNATURE] != SIGNATURE) {
    FS_DEBUG_ERROROUT((FS_MTYPE_DRIVER,  "NOR: Signature mismatch"));
    return 1;
  }
  if ((aInfo[INFO_SECTOR_OFF_VERSION >> 2] >> 16) != VERSION_MAJOR) {
    FS_DEBUG_ERROROUT((FS_MTYPE_DRIVER,  "NOR: Version mismatch"));
    return 1;
  }
  if (aInfo[INFO_SECTOR_OFF_NUM_LOG_SECTORS >> 2] != pInst->NumLogSectors) {
    FS_DEBUG_ERROROUT((FS_MTYPE_DRIVER,  "NOR: Number of logical sectors mismatch"));
    return 1;
  }
  if (aInfo[INFO_SECTOR_OFF_BYTES_PER_LOG_SECTOR >> 2] != pInst->SectorSize) {
    FS_DEBUG_ERROROUT((FS_MTYPE_DRIVER,  "NOR: Logical sector size mismatch"));
    return 1;
  }
  DriveState = aInfo[INFO_SECTOR_OFF_HAS_ERROR >> 2];
  if (DriveState  != NOR_ERROR_STATE_OK) {
    if (DriveState  == NOR_ERROR_STATE_READONLY) {
      FS_DEBUG_ERROROUT((FS_MTYPE_DRIVER,  "NOR: Flash is in readonly mode"));
      _SetError(pInst);
    } else {
      FS_DEBUG_ERROROUT((FS_MTYPE_DRIVER,  "NOR: Unexpected Error"));
      return 1;
    }
  }
  return 0;
}

/*********************************************************************
*
*       _IsLogSectorBlank
*
*  Return value:
*     1  -  Sector is blank
*     0  -  Sector is not blank
*
*/
static char _IsLogSectorBlank(NOR_INST * pInst, U32 Off) {
  U32        acBuffer[32];
  unsigned   NumBytes;
  unsigned   NumBytesAtOnce;
  unsigned   NumItems;
  U32      * p;

  NumBytes = pInst->SectorSize;
  do {
    NumBytesAtOnce = MIN(NumBytes, sizeof(acBuffer));
    _ReadOff(pInst, acBuffer, Off, NumBytesAtOnce);
    NumItems = NumBytesAtOnce >> 2;
    p        = acBuffer;
    do {
      if (*p++ != 0xFFFFFFFFUL) {
      return 0;
    }
    } while (--NumItems);
    NumBytes -= NumBytesAtOnce;
    Off      += NumBytesAtOnce;
  } while (NumBytes);
  return 1;
}

/*********************************************************************
*
*       _AddPhySectorData
*
*  Function description
*    Adds all logical sectors in a physical data sector to the L2P table.
*/
static void _AddPhySectorData(NOR_INST * pInst, U32 Start, U32 SecLen) {
  int Err;
  U32 Off;
  U32 LastPermittedSectorStartAddr;
  U32 LogSectorIndex;
  unsigned LogSectorSize;

  LogSectorSize = sizeof(LOG_SECTOR_HEADER) + pInst->SectorSize;
  LastPermittedSectorStartAddr = Start + SecLen - LogSectorSize;
  Start += sizeof(PHY_SECTOR_HEADER);
  //
  // Now we add the logical sectors one by one.
  //
  for (Off = Start; Off <= LastPermittedSectorStartAddr; Off += LogSectorSize) {
    LogSectorIndex = _GetLogSectorIndex(pInst, Off);
    Err = 0;
    if (LogSectorIndex < pInst->NumLogSectors) {
      U32  PrevOff;

      PrevOff = _WriteL2PEntry(pInst, LogSectorIndex, Off);
      //
      // Check if this entry is already set. If so, this is a copy of an other physical data sector.
      //
      if (PrevOff) {
        _WriteL2PEntry(pInst, LogSectorIndex, PrevOff);   // Restore former value
        Err = 1;
      }
    } else if (LogSectorIndex == LOG_SECTOR_ID_INFO) {
      if (pInst->Status.OffInfoSector) {    // Check if this entry is already set.
        Err = 1;
      } else {
        pInst->Status.OffInfoSector = Off;
      }
    } else if ((LogSectorIndex != LOG_SECTOR_ID_BLANK) && (LogSectorIndex != LOG_SECTOR_ID_ERASABLE)) {
      Err = 1;
    }
    if (Err) {
      _MarkLogSectorAsInvalid(pInst, Off);      // This logical sector should be blank. If it is not, we'll find out during write and use an other one.
    }
  }
}

/*********************************************************************
*
*       _ReadApplyDeviceParas
*
*   Function description
*     Reads the device parameters and computes the parameters stored in the instance structure.
*
*   Return value
*     ==0   OK
*     !=0   An error occurred.
*/
static int _ReadApplyDeviceParas(NOR_INST * pInst) {
  U32    NumLogSectors;

  pInst->NumPhySectors = pInst->pPhyType->pfGetNumSectors(pInst->Unit);
    //
    // Fill the SizeInfostructure
    //
    _InitSizeInfo(pInst);
    //
    // Determine the sector size
    //
    if (pInst->SectorSize == 0) {
      pInst->SectorSize = FS_Global.MaxSectorSize;
    }
    //
    // Calculate the number of logical sectors
    //
    if (pInst->NumLogSectors == 0) {
      NumLogSectors = _CalcNumLogSectors(pInst);
      if (NumLogSectors == 0) {
      FS_DEBUG_ERROROUT((FS_MTYPE_DRIVER,  "NOR: Cannot calculate the number of logical sectors with this configuration.\n"));
      return 1;
      }
      pInst->NumLogSectors = NumLogSectors;
    }
    _InitStatus(pInst);
  return 0;
}

/*********************************************************************
*
*       _LowLevelMount
*
*  Function description
*    Reads and analyzes management information from NAND flash.
*    If the information makes sense and allows us to read and write from
*    the medium, it can perform read and write operations.
*
*  Return value
*    0     O.K., device has been successfully mounted and is accessible
*  !=0     Error
*/
static int _LowLevelMount(NOR_INST * pInst) {
  U32                i;
  U32                NumPhySectors;
  U32                NumPhyDataSectors;
  U32                Start;
  U32                SecLen;
  PHY_SECTOR_HEADER psh;
  int                SizeIndex;
  U8                 SectorType;
  int                r;

  _InitStatus(pInst);
  if (pInst->NumLogSectors == 0) {
    r = _ReadApplyDeviceParas(pInst);
    if (r) {
      return 1;                                 // Error: No logical sectors
    }
  }
  //
  //  Perform a quick check if the medium is LL-formatted
  //
  NumPhySectors = pInst->NumPhySectors;
  NumPhyDataSectors = 0;
  for (i = 0; i < NumPhySectors; i++) {
    _GetSectorInfo(pInst, i, &Start, &SecLen);
    _ReadPSH(pInst, Start, &psh);
    SectorType = _GetPhySectorType(&psh);
    if (SectorType == PHY_SECTOR_TYPE_DATA) {
      NumPhyDataSectors++;
    }
  }
  if (NumPhyDataSectors == 0) {
    FS_DEBUG_WARN((FS_MTYPE_DRIVER, "NOR: No data sectors found. Low level format required."));
    return 1;
  }
  //
  //  Build the L2P table and check physical format at the same time
  //
  for (i = 0; i < NumPhySectors; i++) {
    _GetSectorInfo(pInst, i, &Start, &SecLen);
    _ReadPSH(pInst, Start, &psh);
    SectorType = _GetPhySectorType(&psh);
    if (SectorType == PHY_SECTOR_TYPE_DATA) {
      _AddPhySectorData(pInst, Start, SecLen);
      continue;         // O.K., this physical sector is a valid data sector. Next in line!
    }
    //
    // This sector is not a valid data sector. We'll remember it and will use it as work sector when required.
    //
    SizeIndex = _SectorSize2ShiftCnt(SecLen);
    if (SizeIndex == -1) {
      return -1;                              // Error, should not happen.
    }
    //
    // If we already have a work sector for sectors of this size, let us erase the previous one since we only need one.
    //
    if (pInst->Status.aWorkIndex[SizeIndex] != -1) {
      FS_DEBUG_WARN((FS_MTYPE_DRIVER, "NOR: Found multiple work sectors of identical size. Low level format invalid."));
      return 1;         // Fatal error: Can not have multiple work sectors. Unable to Low-level mount.
    }
    //
    // Make this work sector for its size
    //
    pInst->Status.aWorkIndex[SizeIndex] = i;
  }
  //
  // Verify that a work sector exists for every sector size
  //
  for (i = 0; i < COUNTOF(pInst->Status.aWorkIndex); i++) {
    if (pInst->aNumPhySectorsPerSize[i]) {
      if (pInst->Status.aWorkIndex[i] == -1) {
        U32 iSector;

        //
        // Check if a sector was marked as data, but no data have been written to this sector
        // In this case we can use it as work sector.
        //
        for (iSector = 0; iSector < NumPhySectors; iSector++) {
          if (_IsPhySectorFree(pInst, iSector)) {
            _InvalidatePhySector(pInst, iSector);    // Mark this sector as invalid
            pInst->Status.aWorkIndex[i] = iSector;
            break;

          }
        }
        if (pInst->Status.aWorkIndex[i] == -1) {
          FS_DEBUG_ERROROUT((FS_MTYPE_DRIVER, "NOR: No work sector available for sector index %d, can not low-level mount", i));
          return 1;         // No work sector sector for this size. Fatal error; should not be possible under any circumstances!
        }
      }
    }
  }
  //
  // Make sure we have a valid info sector. If not, refuse to low-level mount.
  //
  if (pInst->Status.OffInfoSector == 0) {
    FS_DEBUG_ERROROUT((FS_MTYPE_DRIVER,  "NOR: No info sector found, can not low-level mount."));
    return 1;                        // No work sector sector for this size. Fatal error; should not be possible under any circumstances!
  }
  if (_CheckInfoSector(pInst, pInst->Status.OffInfoSector)) {
    FS_DEBUG_ERROROUT((FS_MTYPE_DRIVER,  "NOR: Incompatible format acc. to info sector, can not low-level mount."));
    return 1;                        // If the info sector is not o.k., this is a fatal error!
  }
  pInst->Status.IsLLMounted   = 1;
  return 0;
}

/*********************************************************************
*
*       _FillFreeSctorCache
*
*  Description:
*    Fills the free sectors cache.
*    It iterates over all physical sectors, checks if the sector is a data sector
*    and adds all blank logical sectors to the FreeSectorList.
*
*  Return value:
*    FreeSectorCnt    0 means that no free sectors are available and should not happen.
*
*/
static int _FillFreeSectorCache(NOR_INST * pInst) {
  U32 NumPhySectors;
  U32 i;
  U32 Start;
  U32 SecLen;
  U32 Off;
  U32 End;
  PHY_SECTOR_HEADER psh;
  unsigned LogSectorSize;

  LogSectorSize = sizeof(LOG_SECTOR_HEADER) + pInst->SectorSize;
  NumPhySectors = pInst->NumPhySectors;
  for (i = 0; i < NumPhySectors; i++) {
    U8 Type;
    _GetSectorInfo(pInst, i, &Start, &SecLen);
    End = Start + SecLen;
    _ReadOff(pInst, &psh, Start, sizeof(PHY_SECTOR_HEADER));
    Type = _GetPhySectorType(&psh);
    if (Type == PHY_SECTOR_TYPE_DATA) {
      Off = Start + sizeof(PHY_SECTOR_HEADER);
      while (Off < End) {
        U32 LogSectorIndex = _GetLogSectorIndex(pInst, Off);
        if (LogSectorIndex == LOG_SECTOR_ID_BLANK) {
          _AddFreeSectorToCache(pInst, Off);
          //
          // Check if we have enough free sectors found
          //
          if (pInst->Status.FreeSectorCache.Cnt >= FS_NOR_NUM_FREE_SECTORCACHE) {
            return pInst->Status.FreeSectorCache.Cnt;
          }
        }
        Off += LogSectorSize;
        if ((Off + LogSectorSize) > End) {
          break;
        }
      }
    }
  }
  return pInst->Status.FreeSectorCache.Cnt;
}

/*********************************************************************
*
*       _FindClearableSector
*
*   Function description
*    Search a flash sector that contains erasable logical sectors.
*
*   Return value
*     >=0   Valid physical sector number
*     < 0   No clearable sector found
*
*/
static int _FindClearableSector(NOR_INST * pInst) {
  U32 SrcLen;
  U32 SrcAddr;
  int NumPhySectors;
  int i;
  int PhySectorIndex;

  NumPhySectors = pInst->NumPhySectors;
  //
  // Search a physical sector which contains erasable logical sectors.
  //
  for (i = 0; i < NumPhySectors; i++) {
    PhySectorIndex = pInst->Status.psiLastCleared + i;
    if (PhySectorIndex >= NumPhySectors) {
      PhySectorIndex -= NumPhySectors;
    }
    _GetSectorInfo(pInst, PhySectorIndex, &SrcAddr, &SrcLen);
    if (_ContainsErasable(pInst, SrcAddr, SrcLen)) {
      pInst->Status.psiLastCleared = PhySectorIndex + 1;
      return PhySectorIndex;      // Erasable phys. sector found
    }
  }
  return -1;                      // No clearable sector found
}

/*********************************************************************
*
*       _MakeCleanSector
*
*  Description:
*    Copy the data of a physical sector which contains clearable log.sectors
*    to the work sector.
*    This produces one or more free log. sectors.
*    Returns the address of one of the free log. sectors.
*
*/
static void _MakeCleanSector(NOR_INST * pInst) {
  U32      SrcAddr, SrcLen;
  int      SrcPhySec;
  int      DstPhySec;

  SrcPhySec = _FindClearableSector(pInst);  // Find a sector that contains erasable sectors
  if (SrcPhySec == -1) {
    FS_DEBUG_ERROROUT((FS_MTYPE_DRIVER,  "NOR: _MakeCleanSector: No clearable sector left"));
    return;                    // Error
  }
  _GetSectorInfo(pInst, SrcPhySec, &SrcAddr, &SrcLen);
  DstPhySec = _GetWorkSectorIndex(pInst, SrcLen);
  //
  // Invalidate old Work sector
  //
  pInst->Status.aWorkIndex[_SectorSize2ShiftCnt(SrcLen)] = -1; // _SectorSize2ShiftCnt() should always return a valid shift count here, since this errors are already handled in LowLevelMount
  _CopyDataSector(pInst, DstPhySec, SrcPhySec);
}

/*********************************************************************
*
*       _FindFreeLogSector
*
*  Description:
*    Finds a free logical sector.
*
*  Return value:
*    >0                        - Address of a free logical sector.
*    ==0                       - No free sector found.
*
*/
static U32 _FindFreeLogSector(NOR_INST * pInst) {
  U32 Off;     // Offset of free logical sector

  do {
    if (pInst->Status.FreeSectorCache.Cnt == 0) {
      if (_FillFreeSectorCache(pInst) == 0) {
        _MakeCleanSector(pInst);
        if (pInst->Status.FreeSectorCache.Cnt == 0) {
          FS_DEBUG_ERROROUT((FS_MTYPE_DRIVER,  "NOR: Could not find / create a free logical sector."));
          return 0;                // Fatal error!
        }
      }
    }
    //
    // Remove one item from free sector cache and check if it is actually blank
    //
    Off = _RemoveFreeSectorFromCache(pInst);
    if (Off == 0) {
      continue;
    }
    if (_IsLogSectorBlank(pInst, Off)) {
      break;
    }
    //
    // Sector is not blank, even though it is in the free list and should be blank.
    // This situation can arise if a sector write operation has been interrupted by
    // a reset; in this case the header is still marked as blank, but a part of the data
    // area is not blank. We mark the sector as containing invalid data so we do not
    // run into it again. It will be re-used when the physical sector is erased.
    //
    _MarkLogSectorAsInvalid(pInst, Off);      // Not completly blank, so mark it as invalid to make sure we know next time
  } while (1);
  return Off;
}

/*********************************************************************
*
*       _WriteInfoSector
*
*  Description:
*    Write the info sector, which contains all relevant information
*    of the Flash disk.
*
*/
static void _WriteInfoSector(NOR_INST * pInst) {
  U32 aInfo[8];
  U32 Off;

  FS_MEMSET(aInfo, 0x00, sizeof(aInfo));
  aInfo[INFO_SECTOR_OFF_SIGNATURE]                 = SIGNATURE;
  aInfo[INFO_SECTOR_OFF_VERSION >> 2]              = VERSION;
  aInfo[INFO_SECTOR_OFF_NUM_LOG_SECTORS >> 2]      = pInst->NumLogSectors;
  aInfo[INFO_SECTOR_OFF_BYTES_PER_LOG_SECTOR >> 2] = pInst->SectorSize;
  aInfo[INFO_SECTOR_OFF_HAS_ERROR >> 2]            = NOR_ERROR_STATE_OK;
  Off = _FindFreeLogSector(pInst);       // Try to get empty sector

  _WriteLogSectorData(pInst, Off + sizeof(LOG_SECTOR_HEADER), aInfo, sizeof(aInfo));   /* write sector data */
  _WriteLogSectorInfo(pInst, Off, LOG_SECTOR_ID_INFO);
}

/*********************************************************************
*
*       _LowLevelFormat
*
*  Description:
*    Erases all flash memory and format it
*    with working buffer and erase number structure.
*
*  Parameters:
*    None
*
*  Return value:
*     0  O.K.
*  != 0  Error
*/
static int _LowLevelFormat(NOR_INST * pInst) {
  PHY_SECTOR_HEADER psh;
  U32                SecOffs;
  U32                SecLen;
  int                NumSectors;
  int                i;
  int                SizeIndex;
  U8                 SectorType;

  NumSectors = pInst->NumPhySectors;
  FS_MEMSET(&psh, 0xff, sizeof(PHY_SECTOR_HEADER));
  psh.EraseCnt = 1;
  psh.FormatVersion = FORMAT_VERSION;
  psh.Signature     = PHYS_SECTOR_SIGNATURE;
  //
  // Init the sector info: No work sector so far
  //
  _InitStatus(pInst);
  //
  // For each sector:
  // Erase and write physical header at beginning of the sector
  //
  for (i = 0; i < NumSectors; i++) {
    //
    // Erase the sector
    //
    if (_ErasePhySector(pInst, i) != 0) {
      FS_DEBUG_ERROROUT((FS_MTYPE_DRIVER, "NOR: _LowLevelFormat: Failed to erase sector: %d", i));
      return -1;                 /* Error */
    }
    //
    // Retrieve sector info
    //
    _GetSectorInfo(pInst, i, &SecOffs, &SecLen);
    SizeIndex = _SectorSize2ShiftCnt(SecLen);
    if (SizeIndex == -1) {
      return -1;
    }
    if (pInst->Status.aWorkIndex[SizeIndex] == -1) {
      pInst->Status.aWorkIndex[SizeIndex] = i;         // Remember it as work sector
      SectorType     = PHY_SECTOR_TYPE_WORK;                  // Work type, can later be converted to data
      _SetPhySectorType(&psh, SectorType);
      _WritePSH(pInst, SecOffs, &psh);
    } else {
      /* Make it a data buffer */
      SectorType     = PHY_SECTOR_TYPE_DATA;  // Sector will be used for data
      _SetPhySectorType(&psh, SectorType);
      if (_WritePSH(pInst, SecOffs, &psh) != 0) {
        FS_DEBUG_ERROROUT((FS_MTYPE_DRIVER, "NOR: _LowLevelFormat: Failed to write to physical header @: 0x%x", SecOffs));
        return -1;
      }
    }
  }
  //
  // Invalidate all necessary instance information
  //
  _InitStatus(pInst);
  //
  // Write the info sector
  //
  _WriteInfoSector(pInst);
  //
  // Do a low-level mount
  //
  _LowLevelMount(pInst);
  return 0;
}

/*********************************************************************
*
*       _LowLevelMountIfRequired
*
*  Function description
*    LL-Mounts the device if it is not LLMounted and this has not already been
*    tried in vain.
*
*  Return value
*    0     O.K., device has been successfully mounted and is accessible
*  !=0     Error
*/
static int _LowLevelMountIfRequired(NOR_INST * pInst) {
  if (pInst->Status.IsLLMounted) {
    return 0;                   // O.K., is mounted
  }
  if (pInst->Status.LLMountFailed) {
    return 1;                   // Error, we could not mount it and do not want to try again
  }
  _LowLevelMount(pInst);
  if (pInst->Status.IsLLMounted == 0) {
    pInst->Status.LLMountFailed = 1;
  }
  return pInst->Status.IsLLMounted == 0;
}

/*********************************************************************
*
*       _AllocInstIfRequired
*
*  Description:
*    Allocate memory for the specified unit if required.
*
*/
static NOR_INST * _AllocInstIfRequired(U8 Unit) {
  NOR_INST * pInst;

  pInst = _apInst[Unit];
  if (pInst == NULL) {
    pInst = (NOR_INST *)FS_AllocZeroed(sizeof(NOR_INST));   // Alloc memory. This is guaranteed to work by the memory module.
        //
    // Additional error checking if pInst is valid, is not necessary, since FS_AllocZeroed would not return and sty in FS_X_Panic if allocation failed.
        //
    _apInst[Unit]  = pInst;
    pInst->Unit    = Unit;
    pInst->Reserve = RESERVE;
      }
  return pInst;
}

/*********************************************************************
*
*       _WriteOneSector
*
*/
static int _WriteOneSector(NOR_INST * pInst, U32 LogSectorNo, const U8 * pBuffer) {
  U32                Target;
  U32                DstAddr;

  if (pInst->Status.HasError) {
    FS_DEBUG_ERROROUT((FS_MTYPE_DRIVER,  "NOR: _WriteOneSector: Write ignored"));
    return -1;                  /* Error */
  }
#if FS_DEBUG_LEVEL >= FS_DEBUG_LEVEL_CHECK_PARA
  if (LogSectorNo >= pInst->NumLogSectors) { /* test if ID within valid range */
    FS_DEBUG_ERROROUT((FS_MTYPE_DRIVER,  "NOR: _WriteOneSector: LogSector out of range."));
    return -1;                  /* Error */
  }
#endif
  //
  // Find a free log sector first
  //
  Target = _FindFreeLogSector(pInst);
  if (Target == 0) { /* check if valid address before writing */
    FS_DEBUG_ERROROUT((FS_MTYPE_DRIVER, "NOR: _WriteOneSector: Can not write: %d", LogSectorNo));
    return -1;           /* Error */
  }
  DstAddr = _FindLogSector(pInst, LogSectorNo); /* check if sector does already exist */
  //
  // Write sector data
  //
  _WriteLogSectorData(pInst, Target + sizeof(LOG_SECTOR_HEADER), pBuffer, pInst->SectorSize);
  //
  // Write log sector header
  //
  _WriteLogSectorInfo(pInst, Target, LogSectorNo);
  //
  // Clear the old sector
  //
  if (DstAddr != 0) {
    _MarkLogSectorAsInvalid(pInst, DstAddr);   // TestPoint: Set break and RESET here.
  }
  //
  // Update L2P table
  //
  _WriteL2PEntry(pInst, LogSectorNo, Target);
  _WearLevel(pInst);
  return 0;
}

/*********************************************************************
*
*       _CleanPhySector
*
*  Function description
*    Converts the erasable logical sectors by copying the valid logical sectors to work sector. .
*/
static void _CleanPhySector(NOR_INST * pInst, U32 psiSrc) {
  int psiDest;
  U32 SectorLen;

 _GetSectorInfo(pInst, psiSrc, NULL, &SectorLen);
  psiDest = _GetWorkSectorIndex(pInst, SectorLen);
  //
  // Invalidate the old work sector.
  //
  _SetWorkSectorIndex(pInst, SectorLen, WORK_SECTOR_INVALID);
  //
  // Copy the valid logical sector to work sector.
  //
  _CopyDataSector(pInst, (U32)psiDest, psiSrc);
  //
  // Perform the wear leveling.
  //
  _WearLevel(pInst);
}

/*********************************************************************
*
*       _IsPhySectorCleanable
*
*   Function description
*     Checks whether a physical sector can be cleaned which means that
*     the following conditions must be true:
*       - physical sector contains no free logical sectors
*       - physical sector contains at least one erasable sector
*
*   Return value
*     ==0   Physical sector is not cleanable
*     ==1   Physical sector can be cleaned
*/
static int _IsPhySectorCleanable(NOR_INST * pInst, U32 PhySectorIndex) {
  PHY_SECTOR_HEADER psh;
  U32               SectorOff;
  U32               SectorLen;
  int               r;
  unsigned          LogSectorSize;
  U8                PhySectorType;
  int               NumErasableSectors;

  r = 0;      // Physical sector can not be cleaned
  _GetSectorInfo(pInst, PhySectorIndex, &SectorOff, &SectorLen);
  _ReadPSH(pInst, SectorOff, &psh);
  PhySectorType = _GetPhySectorType(&psh);
  if (PhySectorType == PHY_SECTOR_TYPE_DATA) {
    NumErasableSectors = 0;
    SectorLen     -= sizeof(PHY_SECTOR_HEADER);
    SectorOff     += sizeof(PHY_SECTOR_HEADER);
    LogSectorSize  = sizeof(LOG_SECTOR_HEADER) + pInst->SectorSize;
    //
    // Check the index of all the logical sectors.
    //
    while (SectorLen >= LogSectorSize) {
      U32 LogSectorIndex;

      LogSectorIndex = _GetLogSectorIndex(pInst, SectorOff);
      if (LogSectorIndex == LOG_SECTOR_ID_BLANK) {
        break;                      // Free logical sector found. Physical sector can not be cleaned.
      }
      if (LogSectorIndex == LOG_SECTOR_ID_ERASABLE) {
        ++NumErasableSectors;
      }
      SectorLen  -= LogSectorSize;
      SectorOff  += LogSectorSize;
    }
    if (NumErasableSectors) {
      r = 1;                        // At least one erasable logical sector found. Physical sector can be cleaned.
    }
  }
  return r;
}

/*********************************************************************
*
*       _FindPhySectorToClean
*
*   Function description
*     Searches for a physical sector which contains no free sectors.
*
*   Return value
*     >=0   Index of the physical sector found
*     < 0   No physical sector found
*/
static int _FindPhySectorToClean(NOR_INST * pInst) {
  U32 iSector;
  U32 NumPhySectors;
  U8  PhySectorType;
  int r;
  PHY_SECTOR_HEADER psh;

  r = -1;       // No suitable physical sector found yet.
  //
  // Iterate through all data physical sectors and check for free sectors.
  //
  NumPhySectors = pInst->NumPhySectors;
  for (iSector = 0; iSector < NumPhySectors; ++iSector) {
    U32 SectorOff;

    _GetSectorInfo(pInst, iSector, &SectorOff, NULL);
    _ReadPSH(pInst, SectorOff, &psh);
    PhySectorType = _GetPhySectorType(&psh);
    if (PhySectorType == PHY_SECTOR_TYPE_DATA) {
      if (_IsPhySectorCleanable(pInst, iSector)) {
        r = iSector;
        break;
      }
    }
  }
  return r;
}

/*********************************************************************
*
*       _CleanOne
*
*   Function description
*     Performs garbage collection on the medium.
*     Relocates 1 physical sector which contain erasable logical sectors to create writable (blank) logical sectors.
*     The caller is informed whether more operations are required to clean the storage completely.
*
*   Return value
*     ==0   Nothing else to "clean"
*     ==1   At least one more "clean" job left to do
*/
static int _CleanOne(NOR_INST * pInst) {
  int PhySectorIndex;
  int r;

  r = 0;          // Nothing more to clean.
  PhySectorIndex = _FindPhySectorToClean(pInst);
  if (PhySectorIndex >= 0) {
    _CleanPhySector(pInst, PhySectorIndex);
    PhySectorIndex = _FindPhySectorToClean(pInst);
    if (PhySectorIndex >= 0) {
      r = 1;
    }
  }
  return r;
}

/*********************************************************************
*
*       _Clean
*
*   Function description
*     Performs garbage collection on the medium. 
*     All physical sectors which contain erasable logical sectors are relocated to create free (writable) logical sectors.
*/
static void _Clean(NOR_INST * pInst) {
  U32 iSector;
  U32 NumPhySectors;
  U8  PhySectorType;
  PHY_SECTOR_HEADER psh;

  //
  // Iterate through all physical sectors and copy them to work sector.
  //
  NumPhySectors = pInst->NumPhySectors;
  for (iSector = 0; iSector < NumPhySectors; ++iSector) {
    U32 SectorOff;

    _GetSectorInfo(pInst, iSector, &SectorOff, NULL);
    _ReadPSH(pInst, SectorOff, &psh);
    PhySectorType = _GetPhySectorType(&psh);
    if (PhySectorType == PHY_SECTOR_TYPE_DATA) {
      //
      // Only physical sectors which do not contain any free logical sectors are processed.
      //
      if (_IsPhySectorCleanable(pInst, iSector)) {
        _CleanPhySector(pInst, iSector);
      }
    }
  }
}

/*********************************************************************
*
*       Public code (indirectly thru callback)
*
**********************************************************************
*/

/*********************************************************************
*
*       _GetDriverName
*
*   Function description
*     FS driver function. Returns the string which identifies the driver.
*/
static const char * _GetDriverName(U8 Unit) {
  FS_USE_PARA(Unit);
  return "nor";
}

/*********************************************************************
*
*       _AddDevice
*
*   Function description
*     FS driver function. Initializes the low-level driver object.
*
*   Return value
*     >=0     Device allocated
*     < 0     Error, could not add device
*
*/
static int _AddDevice(void) {
  if (_NumUnits >= NUM_UNITS) {
    return -1;
  }
  _AllocInstIfRequired((U8)_NumUnits);
  return _NumUnits++;
}

/*********************************************************************
*
*       _Read
*
*   Function description
*     FS driver function. Reads the contents of one logical sector to given buffer space.
*
*   Parameters
*     Unit            Device number
*     LogSectorIndex  Number of the logical sector to be read
*     pData           [IN]  ---
*                     [OUT] Contents of logical sector
*     NumSectors      Number of sectors to be read.
*
*   Return value:
*     ==0   Sector data read
*     !=0   An error has occurred
*
*/
static int _Read(U8 Unit, U32 LogSectorIndex, void * pData, U32 NumSectors) {
  U32        Addr;
  NOR_INST * pInst;
  U8       * pData8;
  int        r;

  ASSERT_UNIT_NO_IS_IN_RANGE(Unit);
  pInst  = _apInst[Unit];
  pData8 = (U8 *)pData;
  r      = -1;                     // Set to indicate an error.
  if (pInst) {
    if (_LowLevelMountIfRequired(pInst)) {
      return -1;
    }
#if FS_DEBUG_LEVEL >= FS_DEBUG_LEVEL_CHECK_PARA
    if (LogSectorIndex >= pInst->NumLogSectors) {
      FS_DEBUG_ERROROUT((FS_MTYPE_DRIVER,  "NOR: Could not read. Logical sector out of range.\n"));
      return -1;
    }
#endif
    do {
      Addr = _FindLogSector(pInst, LogSectorIndex);
      if (Addr != 0) {
        _ReadLogSectorData(pInst, Addr + sizeof(LOG_SECTOR_HEADER), pData8, pInst->SectorSize);
      } else {
        FS_MEMSET(pData8, 0, pInst->SectorSize);
      }
      IF_STATS(pInst->StatCounters.ReadSectorCnt++);
      pData8 += pInst->SectorSize;
      LogSectorIndex++;
    } while (--NumSectors);
    r = 0;
  }
  return r;
}

/*********************************************************************
*
*       _Write
*
*   Function description
*    FS driver function. Writes one or more logical sectors to storage device.
*
*   Return value
*     ==0   Data successfully written.
*     !=0   An error has occurred.
*
*/
static int _Write(U8 Unit, U32 LogSectorIndex, const void * pData, U32 NumSectors, U8 RepeatSame) {
  int r;
  const U8 * pData8;
  NOR_INST     * pInst;

  pData8 = (const U8 *)pData;
  r      = -1;                        // Set to indicate an error.
  ASSERT_UNIT_NO_IS_IN_RANGE(Unit);
  pInst = _apInst[Unit];
  if (pInst) {
    if (_LowLevelMountIfRequired(pInst)) {
      return -1;
    }
    do {
      r = _WriteOneSector(pInst, LogSectorIndex++, pData8);
      if (r) {
        break;
      }
      IF_STATS(pInst->StatCounters.WriteSectorCnt++);
      if (RepeatSame == 0) {
        pData8 += pInst->SectorSize;
      }
    } while (--NumSectors);
  }
  return r;
}

/*********************************************************************
*
*       _IoCtl
*
*   Function description
*     FS driver function. Device IO control interface function.
*
*   Parameters
*     Unit      Device index number.
*     Cmd       Command to be executed
*     Aux       Parameter for the command
*     pBuffer   Pointer to a buffer holding add. parameters
*
*   Return value
*     ==0   Command successfully executed
*     !=0   An error has occurred.
*
*/
static int _IoCtl(U8 Unit, I32 Cmd, I32 Aux, void * pBuffer) {
  FS_DEV_INFO * pInfo;
  NOR_INST * pInst;
  int           r;

  FS_USE_PARA(Aux);
  ASSERT_UNIT_NO_IS_IN_RANGE(Unit);
  pInst = _apInst[Unit];
  r     = -1;             // Set to indicate an error.
  if (pInst) {
    switch (Cmd) {
    case FS_CMD_GET_DEVINFO:
      if (pBuffer) {
        r = _LowLevelMountIfRequired(pInst);
        if (r == 0) {
          pInfo = (FS_DEV_INFO*)pBuffer;
          pInfo->NumSectors     = pInst->NumLogSectors - NUM_RESERVED_SECTORS;
          pInfo->BytesPerSector = pInst->SectorSize;
      }
      }
      break;
    case FS_CMD_FORMAT_LOW_LEVEL:
      return _LowLevelFormat(pInst);
    case FS_CMD_FREE_SECTORS:
      {
        U32 SectorIndex;
        U32 NumSectors;

        SectorIndex = (U32)Aux;
        NumSectors  = *(U32 *)pBuffer;
        _FreeSectors(pInst, SectorIndex, NumSectors);
        r = 0;
      }
      break;
    case FS_CMD_REQUIRES_FORMAT:
      return _LowLevelMountIfRequired(pInst);
    case FS_CMD_CLEAN_ONE:
      r = _LowLevelMountIfRequired(pInst);
      if (r == 0) {
        int   More;
        int * pMore;

        More  = _CleanOne(pInst);
        pMore = (int *)pBuffer;
        if (pMore) {
          *pMore = More;
        }
      }
      break;
    case FS_CMD_CLEAN:
      r = _LowLevelMountIfRequired(pInst);
      if (r == 0) {
        _Clean(pInst);
      }
      break;
#if FS_SUPPORT_DEINIT
    case FS_CMD_DEINIT:
      //
      //  Deinitialize the driver
      //
      if (pInst->pPhyType->pfDeInit) {
        pInst->pPhyType->pfDeInit(Unit);
      }
      FS_FREE(pInst->pL2P);
      FS_FREE(pInst);
      pInst = 0;    //-Q-
      _apInst[Unit] = 0;
      _NumUnits--;
      r = 0;
      break;
#endif
    }
  }
  return r;
}

/*********************************************************************
*
*       _InitMedium
*
*   Function description
*     FS driver function. Initializes and identifies the storage device.
*
*   Return value
*     ==0   Device okay and ready for operation
*     !=0   An error has occurred.
*/
static int _InitMedium(U8 Unit) {
  int        r;
  NOR_INST * pInst;

  pInst = _apInst[Unit];
  r     = -1;
  if (pInst->pPhyType) {
    pInst->Unit       = Unit;
    r = _ReadApplyDeviceParas(pInst);
  }
  return r;
}

/*********************************************************************
*
*       _GetStatus
*
*   Function description:
*     FS driver function. Checks device status and returns it.
*
*/
static int _GetStatus(U8 Unit) {
  FS_USE_PARA(Unit);
  return FS_MEDIA_IS_PRESENT;
}

/*********************************************************************
*
*       _GetNumUnits
*
*   Function description
*     FS driver function. Returns the number of allocated driver instances.
*/
static int _GetNumUnits(void) {
  return _NumUnits;
}

/*********************************************************************
*
*       Global variables
*
**********************************************************************
*/
const FS_DEVICE_TYPE FS_NOR_Driver = {
  _GetDriverName,
  _AddDevice,
  _Read,
  _Write,
  _IoCtl,
  _InitMedium,
  _GetStatus,
  _GetNumUnits
};


/*********************************************************************
*
*       FS_NOR_Configure
*
*  Description:
*    Configures a single instance of the driver
*/
void FS_NOR_Configure(U8 Unit, U32 BaseAddr, U32 StartAddr, U32 NumBytes) {
  NOR_INST * pInst;

  pInst  = _AllocInstIfRequired(Unit);
  ASSERT_PHY_TYPE_IS_SET(pInst);
  pInst->pPhyType->pfConfigure(Unit, BaseAddr, StartAddr, NumBytes);
}

/*********************************************************************
*
*       FS_NOR_ConfigureReserve
*
*  Description:
*    Configures the percentage of sectors that shall be reserved.
*    Some reserved sectors are required so we always have some
*    free sectors.
*    The free sectors are available at any given time, the slower the
*    driver will operate.
*/
void FS_NOR_ConfigureReserve(U8 Unit, U8 Percentage2Reserve) {
  NOR_INST * pInst;

  pInst  = _AllocInstIfRequired(Unit);
  pInst->Reserve = Percentage2Reserve;
}

/*********************************************************************
*
*       FS_NOR_SetPhyType
*
*   Function description
*     Configures the physical layer which should be used to access the NOR flash.
*/
void FS_NOR_SetPhyType(U8 Unit, const FS_NOR_PHY_TYPE * pPhyType) {
  NOR_INST * pInst;

  ASSERT_UNIT_NO_IS_IN_RANGE(Unit);
  pInst  = _AllocInstIfRequired(Unit);
  pInst->pPhyType = pPhyType;
  pPhyType->pfOnSelectPhy(Unit);
}

/*********************************************************************
*
*       FS_NOR_GetDiskInfo
*
*   Function description
*     Returns information about the flash disk.
*
*/
void FS_NOR_GetDiskInfo(U8 Unit, FS_NOR_DISK_INFO * pDiskInfo) {
  unsigned    i;
  U32         NumUsedSectors;
  NOR_INST  * pInst;

  pInst = _AllocInstIfRequired(Unit);
  if (pInst) {
    NumUsedSectors = 0;
    for (i = 0; i < pInst->NumLogSectors; i++) {
      U32 L2PEntry;

      L2PEntry = FS_BITFIELD_ReadEntry(pInst->pL2P, i, pInst->NumBitsUsed);
      if (L2PEntry) {
        NumUsedSectors++;
      }
    }
    pDiskInfo->NumPhysSectors = pInst->NumPhySectors;
    pDiskInfo->NumLogSectors  = pInst->NumLogSectors - NUM_RESERVED_SECTORS; // One sector is reserved for the internal info sector
    pDiskInfo->NumUsedSectors = NumUsedSectors;
  } else {
    pDiskInfo->NumPhysSectors = 0;
    pDiskInfo->NumLogSectors  = 0;
    pDiskInfo->NumUsedSectors = 0;
  }

}

/*********************************************************************
*
*       FS_NOR_GetSectorInfo()
*
*   Function description
*     Returns information about a particular physical physical sector.
*
*/
void FS_NOR_GetSectorInfo(U8 Unit, U32 PhySectorIndex, FS_NOR_SECTOR_INFO * pSectorInfo) {
  int r;
  U32 u;
  U32 SectorOff;
  U32 SectorSize;
  U16 NumEraseableSectors;
  U16 NumFreeSectors;
  U16 NumUsedSectors;
  U8  PhySectorType;
  U32 EraseCnt;
  unsigned   LogSectorSize;
  NOR_INST * pInst;
  PHY_SECTOR_HEADER psh;

  pInst = _AllocInstIfRequired(Unit);
  r     = _LowLevelMountIfRequired(pInst);
  SectorOff           = 0;
  SectorSize          = 0;
  EraseCnt            = 0;
  NumEraseableSectors = 0;
  NumFreeSectors      = 0;
  NumUsedSectors      = 0;
  if (r == 0) {
    _GetSectorInfo(pInst, PhySectorIndex, &SectorOff, &SectorSize);
    _ReadOff(pInst, &psh, SectorOff, sizeof(PHY_SECTOR_HEADER));
    EraseCnt      = psh.EraseCnt;
    LogSectorSize = sizeof(LOG_SECTOR_HEADER) + pInst->SectorSize;
    PhySectorType = _GetPhySectorType(&psh);
    if (PhySectorType == PHY_SECTOR_TYPE_DATA) {
      for (u = SectorOff + sizeof(PHY_SECTOR_HEADER); u + LogSectorSize < SectorOff + SectorSize; u += LogSectorSize) {
        U32 LogSectorIndex;

        LogSectorIndex = _GetLogSectorIndex(pInst, u);
        switch (LogSectorIndex) {
        case LOG_SECTOR_ID_ERASABLE:
          NumEraseableSectors++;
          break;
        case LOG_SECTOR_ID_BLANK:
          NumFreeSectors++;
          break;
        default:
          NumUsedSectors++;
        }
      }
    }
  }
  pSectorInfo->Off                 = SectorOff;
  pSectorInfo->Size                = SectorSize;
  pSectorInfo->EraseCnt            = EraseCnt;
  pSectorInfo->NumEraseableSectors = NumEraseableSectors;
  pSectorInfo->NumFreeSectors      = NumFreeSectors;
  pSectorInfo->NumUsedSectors      = NumUsedSectors;
}

/*********************************************************************
*
*       FS_NOR_LogSector2PhySectorAddr
*
*   Function description
*     Returns the address in memory of a given logical sector.
*/
const void * FS_NOR_LogSector2PhySectorAddr(U8 Unit, U32 LogSectorNo) {
  NOR_INST       * pInst;
  const void * pSectorData;
  U32          Addr;

  pInst  = _AllocInstIfRequired(Unit);
  if (_LowLevelMountIfRequired(pInst)) {
    return NULL;
  }
  if(LogSectorNo >= pInst->NumLogSectors) {
    return NULL;
  }
  Addr = _FindLogSector(pInst, LogSectorNo);
  pSectorData = (const void *)(Addr + sizeof(LOG_SECTOR_HEADER));
  return pSectorData;
}

/*********************************************************************
*
*       FS_NOR_SetSectorSize
*
*   Function description
*     Configures the size of the logical sector.
*/
void FS_NOR_SetSectorSize(U8 Unit, U16 SectorSize) {
  NOR_INST       * pInst;

  pInst             = _AllocInstIfRequired(Unit);
  pInst->SectorSize = SectorSize;
}

/*********************************************************************
*
*       FS_NOR_IsLLFormatted
*
*   Function description
*     Checks whether the NOR flash is low-level formatted.
*/
int FS_NOR_IsLLFormatted(U8 Unit) {
  NOR_INST       * pInst;

  pInst  = _AllocInstIfRequired(Unit);
  _InitMedium(Unit);
  return _LowLevelMountIfRequired(pInst);
}

/*********************************************************************
*
*       FS_NOR_FormatLow
*
*   Function description
*     Performs a low-level format of NOR flash. All phyiscal sectors are erased and identification information is stored to medium.
*/
int FS_NOR_FormatLow(U8 Unit) {
  NOR_INST       * pInst;

  pInst  = _AllocInstIfRequired(Unit);
  _InitMedium(Unit);
  return _LowLevelFormat(pInst);
}

/*********************************************************************
*
*       FS_NOR_GetStatCounters
*
*   Function description
*     Reads the statistical counters.
*/
void FS_NOR_GetStatCounters(U8 Unit, FS_NOR_STAT_COUNTERS * pStat) {
#if FS_NOR_ENABLE_STATS
  NOR_INST * pInst;

  ASSERT_UNIT_NO_IS_IN_RANGE(Unit);
  pInst  = _AllocInstIfRequired(Unit);
  *pStat = pInst->StatCounters;
#else
  FS_USE_PARA(Unit);
  FS_MEMSET(pStat, 0, sizeof(FS_NOR_STAT_COUNTERS));
#endif
}

/*********************************************************************
*
*       FS_NOR_ResetStatCounters
*
*   Function description
*     Sets to 0 the statistical counters.
*/
void FS_NOR_ResetStatCounters(U8 Unit) {
#if FS_NOR_ENABLE_STATS
  NOR_INST * pInst;
  FS_NOR_STAT_COUNTERS * pStat;

  ASSERT_UNIT_NO_IS_IN_RANGE(Unit);
  pInst = _AllocInstIfRequired(Unit);
  pStat = &pInst->StatCounters;
  pStat->CopySectorCnt     = 0;
  pStat->EraseCnt          = 0;
  pStat->ReadSectorCnt     = 0;
  pStat->WriteSectorCnt    = 0;
#else
  FS_USE_PARA(Unit);
#endif
}

/*************************** End of file ****************************/
