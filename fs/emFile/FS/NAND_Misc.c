/*********************************************************************
*                SEGGER MICROCONTROLLER GmbH & Co. KG                *
*        Solutions for real time microcontroller applications        *
**********************************************************************
*                                                                    *
*        (c) 2003-2012     SEGGER Microcontroller GmbH & Co KG       *
*                                                                    *
*        Internet: www.segger.com    Support:  support@segger.com    *
*                                                                    *
**********************************************************************

**** emFile file system for embedded applications ****
emFile is protected by international copyright laws. Knowledge of the
source code may not be used to write a similar product. This file may
only be used in accordance with a license and should not be re-
distributed in any way. We appreciate your understanding and fairness.
----------------------------------------------------------------------
File        : NAND_Misc.c
Purpose     : Misc. functions related to NAND flash.
---------------------------END-OF-HEADER------------------------------
*/

#include "FS.h"
#include "FS_Int.h"
#include "NAND_Private.h"
#include "NAND_X_HW.h"

/*********************************************************************
*
*       Defines, not configurable
*
**********************************************************************
*/

/*********************************************************************
*
*       NAND flash commands
*/
#define CMD_READ_1          0x00
#define CMD_READ_ID       0x90
#define CMD_READ_STATUS   0x70
#define CMD_READ_PARA_PAGE  0xEC
#define CMD_RESET         0xFF

/*********************************************************************
*
*       ONFI parameters
*/
#define PARA_PAGE_SIZE      256
#define PARA_CRC_POLY       0x8005
#define PARA_CRC_INIT       0x4F4E
#define NUM_PARA_PAGES      3

#define STATUS_READY      (1u << 6)

/*********************************************************************
*
*       Static functions
*
**********************************************************************
*/

/*********************************************************************
*
*       _WriteCmd
*
*   Function description
*     Sends a command to NAND flash.
*/
static void _WriteCmd(U8 Unit, U8 Cmd) {
  FS_NAND_HW_X_SetCmdMode(Unit);
  FS_NAND_HW_X_Write_x8(Unit, &Cmd, 1);
}

/*********************************************************************
*
*       _WriteAddrByte
*
*   Function description
*     Sends an address byte to NAND flash.
*/
static void _WriteAddrByte(U8 Unit, U8 Addr) {
  FS_NAND_HW_X_SetAddrMode(Unit);
  FS_NAND_HW_X_Write_x8(Unit, &Addr, 1);
}

/*********************************************************************
*
*       _ReadData8
*
*   Function description
*     Reads data bytes from NAND flash.
*/
static void _ReadData8(U8 Unit, void * pData, unsigned NumBytes) {
  FS_NAND_HW_X_SetDataMode(Unit);
  FS_NAND_HW_X_Read_x8(Unit, pData, NumBytes);
}

/*********************************************************************
*
*       _ReadStatus
*
*   Function description
*     Executes the the READ STATUS command.
*/
static U8 _ReadStatus(U8 Unit) {
  U8 Status;

  _WriteCmd(Unit, CMD_READ_STATUS);
  _ReadData8(Unit, &Status, 1);
  return Status;
}

/*********************************************************************
*
*       _Reset
*
*   Function description
*     Executes the the RESET command.
*
*/
static void _Reset(U8 Unit) {
  _WriteCmd(Unit, CMD_RESET);
}

/*********************************************************************
*
*       _ReadId
*
*   Function description
*     Executes the the READ ID command.
*
*/
static void _ReadId(U8 Unit, U8 * pId, U32 NumBytes) {
  _WriteCmd(Unit, CMD_READ_ID);
  _WriteAddrByte(Unit, 0);    // Identification information is stored at address 0.
  _ReadData8(Unit, pId, NumBytes);
}

/*********************************************************************
*
*       _WaitForReady
*
*   Function description
*     Wait for NAND device to become ready.
*/
static void _WaitForReady(U8 Unit) {
  U8 Status;

  do {
    Status = _ReadStatus(Unit);
  } while ((Status & STATUS_READY) == 0);
}

/*********************************************************************
*
*       _IsONFISupported
*
*   Function description
*     Checks if the device supports ONFI.
*     An ONFI compatible device returns "ONFI" ASCII string
*     when executing a READ ID operation from address 0x20
*
*   Return value
*     ==0    ONFI not supported
*     !=0    ONFI supported
*/
static int _IsONFISupported(U8 Unit) {
  int r;
  U8  aId[4];

  r = 0;    // asumming ONFI is not supported
  _WriteCmd(Unit, CMD_READ_ID);
  _WriteAddrByte(Unit, 0x20);
  _ReadData8(Unit, &aId[0], sizeof(aId));
  if ((aId[0] == 'O') &&
      (aId[1] == 'N') &&
      (aId[2] == 'F') &&
      (aId[3] == 'I')) {
    r = 1;
  }
  return r;
}

/*********************************************************************
*
*       _ReadONFIPara
*
*   Function description
*     Reads the ONFI parameter page.
*     A page has 256 bytes. The integrity of information is checked using CRC.
*
*   Parameters
*     Unit    Device unit number
*     pPara   [IN]  ---
*             [OUT] Information read from the parameter page. Must be at least 256 bytes long.
*
*   Return value
*     ==0    ONFI parameters read
*     !=0    An error occurred
*/
static int _ReadONFIPara(U8 Unit, void * pPara) {
  int   r;
  U16   crcRead;
  U16   crcCalc;
  U8  * p;
  U32   NumBytesPara;
  int   i;

  r = 1;        // No parameter page found, yet.
  _WriteCmd(Unit, CMD_READ_PARA_PAGE);
  _WriteAddrByte(Unit, 0);
  _WaitForReady(Unit);
  _WriteCmd(Unit, CMD_READ_1);    // Switch back to read mode. _WaitForReady() function changed it to status mode.
  p = (U8 *)pPara;
  //
  // Several identical parameter pages are stored in a device.
  // Read from the first one which stores valid information.
  //
  for (i = 0; i < NUM_PARA_PAGES; ++i) {
    _ReadData8(Unit, p, PARA_PAGE_SIZE);
    NumBytesPara = PARA_PAGE_SIZE - sizeof(crcRead);    // CRC is stored on the last 2 bytes of the parameter page.
    //
    // Validate the parameters by checking the CRC.
    //
    crcRead = FS_LoadU16LE(&p[NumBytesPara]);
    crcCalc = FS_CRC16_CalcBitByBit(p, NumBytesPara, PARA_CRC_INIT, PARA_CRC_POLY);
    if (crcRead != crcCalc) {
      continue;
    }
    //
    // CRC is valid, now check the signature.
    //
    if ((p[0] == 'O') &&
        (p[1] == 'N') &&
        (p[2] == 'F') &&
        (p[3] == 'I')) {
      r = 0;
      break;                      // Found a valid parameter page.
    }
  }
  return r;
}

/*********************************************************************
*
*       Private functions
*
**********************************************************************
*/

/*********************************************************************
*
*       FS_NAND__IsONFISupported
*
*   Function description
*     Checks whether the NAND flash supports ONFI.
*
*   Parameters
*     Unit      Index of HW layer connected to NAND flash
*
*   Return value
*     ==0   ONFI not supported
*     !=0   NAND flash supports ONFI
*
*/
int FS__NAND_IsONFISupported(U8 Unit) {
  int r;

  r = _IsONFISupported(Unit);
  return r;
}

/*********************************************************************
*
*       FS_NAND__ReadONFIPara
*
*   Function description
*     Reads the ONFI parameter page from a NAND flash.
*
*   Parameters
*     Unit      Index of HW layer connected to NAND flash
*     pPara     [IN]  ---
*               [OUT] Data of ONFI parameter page read from NAND flash.
*                     The size of the buffer must be at least 256 bytes.
*
*   Return value
*     ==0   ONFI parameters read
*     !=0   ONFI is not supported by the NAND flash
*
*/
int FS__NAND_ReadONFIPara(U8 Unit, void * pPara) {
  int r;

  r = _ReadONFIPara(Unit, pPara);
  return r;
}

/*********************************************************************
*
*       Public functions
*
**********************************************************************
*/

/*********************************************************************
*
*       FS_NAND_PHY_ReadDeviceId
*
*   Function description
*     Executes the READ ID command to get information about the NAND flash.
*     Typ. used by an application to determine which kind of NAND driver to configure.
*
*   Parameters
*     Unit      Index of HW layer connected to NAND flash
*     pId       [IN]  ---
*               [OUT] Identification data read from NAND flash
*     NumBytes  Number of bytes to read.
*
*/
void FS_NAND_PHY_ReadDeviceId(U8 Unit, U8 * pId, U32 NumBytes) {
  //
  // Initialize the HW layer.
  //
  FS_NAND_HW_X_Init_x8(Unit);
  //
  // Enable device.
  //
  FS_NAND_HW_X_EnableCE(Unit);
  //
  // NAND device must be reset before we can communicate with it.
  //
  _Reset(Unit);
  _WaitForReady(Unit);
  _ReadId(Unit, pId, NumBytes);
  //
  // Disable device.
  //
  FS_NAND_HW_X_DisableCE(Unit);
}

/*********************************************************************
*
*       FS_NAND_PHY_ReadONFIPara
*
*   Function description
*     Reads the ONFI parameter page from a NAND flash.
*
*   Parameters
*     Unit      Index of HW layer connected to NAND flash
*     pPara     [IN]  ---
*               [OUT] Data of ONFI parameter page read from NAND flash.
*                     The size of the buffer must be at least 256 bytes.
*                     This parameter can be 0.
*
*   Return value
*     ==0   ONFI parameters read
*     !=0   ONFI is not supported by the NAND flash
*
*/
int FS_NAND_PHY_ReadONFIPara(U8 Unit, void * pPara) {
  int r;

  r = 1;
  //
  // Initialize the HW layer.
  //
  FS_NAND_HW_X_Init_x8(Unit);
  //
  // Enable device.
  //
  FS_NAND_HW_X_EnableCE(Unit);
  //
  // NAND device must be reset before we can communicate with it.
  //
  _Reset(Unit);
  _WaitForReady(Unit);
  if (FS__NAND_IsONFISupported(Unit)) {
    r = 0;
    if (pPara) {
      //
      // Call the private function to do the reading.
      //
      r = FS__NAND_ReadONFIPara(Unit, pPara);
    }
  }
  //
  // Disable device.
  //
  FS_NAND_HW_X_DisableCE(Unit);
  return r;
}

/*************************** End of file ****************************/
