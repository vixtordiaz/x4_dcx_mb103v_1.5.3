/*********************************************************************
*                SEGGER MICROCONTROLLER GmbH & Co. KG                *
*        Solutions for real time microcontroller applications        *
**********************************************************************
*                                                                    *
*        (c) 2003-2012     SEGGER Microcontroller GmbH & Co KG       *
*                                                                    *
*        Internet: www.segger.com    Support:  support@segger.com    *
*                                                                    *
**********************************************************************

**** emFile file system for embedded applications ****
emFile is protected by international copyright laws. Knowledge of the
source code may not be used to write a similar product. This file may
only be used in accordance with a license and should not be re-
distributed in any way. We appreciate your understanding and fairness.
----------------------------------------------------------------------
File        : NOR_HW_CFI_2x16.c
Purpose     : Low level flash layer handling CFI compliant flash chips
----------------------------------------------------------------------

Comments in this file refer to Intel's "Common Flash Interface
(CFI) and Command Sets" document as the "CFI spec."

Supported devices:
Any CFI-compliant flash in 16-bit mode



Literature:
[1] Intel's "Common Flash Interface (CFI) and Command Sets"
    Application Note 646, April 2000

[2] Spansion's "Common Flash Interface Version 1.4 Vendor Specific Extensions"
    Revision A, Amendment 0, March 22 - 2004

------------  END-OF-HEADER  -----------------------------------------
*/

#include "FS_Int.h"
#include "NOR_Private.h"

/*********************************************************************
*
*      Defines
*
**********************************************************************
*/



/*********************************************************************
*
*      Flash command definitions (Intel Algo)
*/
#define INTEL_PROGRAM(BaseAddr, Addr, Data)           \
  *(volatile U32 FS_NOR_FAR*)(BaseAddr)  = 0x400040UL;  \
  *(volatile U32 FS_NOR_FAR*)(Addr)      = Data

#define INTEL_READSTATUS(BaseAddr, Stat)              \
  *(volatile U32 FS_NOR_FAR*)(BaseAddr)  = 0x700070UL;  \
  Stat = *(volatile U32 FS_NOR_FAR*)(BaseAddr)

#define INTEL_ERASEBLOCK(Addr)                        \
  *(volatile U32 FS_NOR_FAR*)(Addr)            = 0x200020UL; \
  *(volatile U32 FS_NOR_FAR*)(Addr)            = 0xD000D0UL

#define INTEL_CLEARSTATUS(BaseAddr)                   \
  *(volatile U32 FS_NOR_FAR*)(BaseAddr)  = 0x500050UL

#define INTEL_RESET(BaseAddr)                         \
  *(volatile U32 FS_NOR_FAR*)(BaseAddr)  = 0xFF00FFUL


/*********************************************************************
*
*      Flash command definitions (AMD Algo)
*/
#define AMD_WRITECODE(BaseAddr)                                       \
  *(volatile U32 FS_NOR_FAR*)(BaseAddr + (0x555 << 2)) = 0xAA00AAUL;         \
  *(volatile U32 FS_NOR_FAR*)(BaseAddr + (0x2AA << 2)) = 0x550055UL

#define AMD_PROGRAM(BaseAddr)                                         \
  AMD_WRITECODE(BaseAddr);                                            \
  *(volatile U32 FS_NOR_FAR*)(BaseAddr + (0x555 << 2)) = 0xA000A0UL

#define AMD_BLOCKERASE(BaseAddr)                                      \
  AMD_WRITECODE(BaseAddr);                                            \
  *(volatile U32 FS_NOR_FAR*)(BaseAddr + (0x555 << 2)) = 0x800080UL;    \
  AMD_WRITECODE(BaseAddr)

#define AMD_RESET(BaseAddr)                                           \
  *(volatile U32 FS_NOR_FAR*)(BaseAddr + (0x000 << 2)) = 0xF000F0UL

#define AMD_AUTOSELECT(BaseAddr)                                      \
  AMD_WRITECODE(BaseAddr);                                            \
  *(volatile U32 FS_NOR_FAR*)(BaseAddr + (0x555 << 2)) = 0x900090UL



/*********************************************************************
*
*      Flash command definitions (CFI)
*
*    Notes:
*      1   To determine if a device is CFI capable, we have to write 0x98
*          (CFI query) at address 0x55 (CFI spec, page 4).
*/
#define CFI_READCONFIG(BaseAddr)                                                \
  *(volatile U32 FS_NOR_FAR*)(BaseAddr + (0x55 << 2))  = 0x980098UL

// Writing 0xFF puts the device into read array mode (CFI spec, page 15).
#define CFI_RESET(BaseAddr)                         \
  *(volatile U32 FS_NOR_FAR*)(BaseAddr)  = 0xFF00FFUL;     \
  *(volatile U32 FS_NOR_FAR*)(BaseAddr)  = 0xF000F0UL


/*********************************************************************
*
*       Fixed defines
*
**********************************************************************
*/
// None

/*********************************************************************
*
*      Types
*
**********************************************************************
*/
// None

/*********************************************************************
*
*      Static data
*
**********************************************************************
*/
// None

/*********************************************************************
*
*      Static code
*
**********************************************************************
*/
// None

/*********************************************************************
*
*      Write/Erase routines for Intel comaptible devices
*
**********************************************************************
*/
/*********************************************************************
*
*      _EraseSectorINTEL
*
* Purpose:
*   obvious
* Return-value:
*   0       O.K., sector is erased
*   other   error, sector may not be erased
*/
static int _EraseSectorINTEL(U8 Unit, U32 BaseAddr, U32 SectorAddr) {
  U32 Status;
  volatile U32 FS_NOR_FAR* pB;

  FS_USE_PARA(Unit);
  pB = (volatile U32 FS_NOR_FAR*)SectorAddr;
  FS_NOR_DI();  /* Disable interrupts, to be on the safe side  */
  INTEL_ERASEBLOCK(pB);
  do {
    INTEL_READSTATUS(BaseAddr, Status);
    FS_NOR_DELAY();
  } while ((Status & 0x800080UL) == 0);  /* Wait till flash is no longer busy */
  INTEL_RESET(BaseAddr);
  INTEL_CLEARSTATUS(BaseAddr);
  FS_NOR_EI();  /* Enable interrupts */
  if (Status & 0x3A003AUL) {
    return 1;
  }
  return 0;
}

/*********************************************************************
*
*      _WriteINTEL
*/
static char _WriteINTEL(U8 Unit, U32 BaseAddr, U32 SectorAddr, U32 DestAddr, const U16 FS_NOR_FAR * pSrc0, int NumItems) {
  U32 Status;
  volatile U32 FS_NOR_FAR * pDest;
  U32 FS_NOR_FAR * pSrc;
  U32 Data32;

  FS_USE_PARA(Unit);
  FS_USE_PARA(SectorAddr);
  pSrc  = (U32 FS_NOR_FAR *)pSrc0;
  pDest = (volatile U32 FS_NOR_FAR *)DestAddr;
  NumItems >>= 1;      // Convert into  number of 32-bit units
  do {
    Data32 =  *pSrc;
    FS_NOR_DI();  /* Disable interrupts, to be on the safe side  */
    INTEL_PROGRAM(BaseAddr, pDest, Data32);
    do {
      INTEL_READSTATUS(BaseAddr, Status);
      FS_NOR_DELAY();
    } while ((Status & 0x800080UL) == 0);  /* Wait till flash is no longer busy */
    INTEL_READSTATUS(BaseAddr, Status);
    INTEL_RESET(BaseAddr);
    INTEL_CLEARSTATUS(BaseAddr);
    INTEL_RESET(BaseAddr);
    FS_NOR_EI();  /* Enable interrupts */
    if (Status & 0x1a001aUL) {
      return 1;
    }
    if (*pDest != Data32) {
      return 1;
    }
    ++pDest;
    ++pSrc;
  } while (--NumItems);
  return 0;
}

/*********************************************************************
*
*      Write/Erase routines for AMD comaptible devices
*
**********************************************************************
*/
/*********************************************************************
*
*      _EraseSectorAMD
*
* Purpose:
*   Erases one sector
* Return value:
*   0       O.K., sector is ereased
*   other   error, sector may not be erased
*/
static int _EraseSectorAMD(U8 Unit, U32 BaseAddr, U32 SectorAddr) {
  volatile U32 FS_NOR_FAR * pB;
  volatile U32 FS_NOR_FAR * pStatus;
  U32  Status;

  FS_USE_PARA(Unit);
  pB       = (volatile U32 FS_NOR_FAR*)SectorAddr;
  pStatus  = (volatile U32 FS_NOR_FAR*)SectorAddr;
  FS_NOR_DI();  /* Disable interrupts, to be on the safe side  */
  AMD_BLOCKERASE(BaseAddr);
  *pB = 0x300030UL;
  while (Status = *pStatus, (Status ^ *pStatus) & 0x400040UL) {      /* Check toggle bit */
    FS_NOR_DELAY();
  }
  if (*pB != 0xffffffffUL) {
    AMD_RESET(BaseAddr);
    FS_NOR_EI();  /* Enable interrupts */
    return 1;
  }
  FS_NOR_EI();  /* Enable interrupts */
  return 0;
}

/*********************************************************************
*
*      _WriteAMD
*
* Purpose:
*   Writes data into flash
* Return value:
*   0       O.K., data has been written
*   other   error
*/
static char _WriteAMD(U8 Unit, U32 BaseAddr, U32 SectorAddr, U32 DestAddr, const U16 FS_NOR_FAR * pSrc0, int NumItems) {
  int i;
  U32          FS_NOR_FAR * pSrc;
  volatile U32 FS_NOR_FAR * pDest;
  U32 a;
  U32 Data32;

  FS_USE_PARA(Unit);
  FS_USE_PARA(SectorAddr);
  pSrc  = (U32 FS_NOR_FAR *)pSrc0;
  pDest = (volatile U32 FS_NOR_FAR *)DestAddr;
  FS_NOR_DI();  /* Disable interrupts, to be on the safe side  */
  AMD_RESET(BaseAddr);
  FS_NOR_EI();  /* Enable interrupts */

  NumItems >>= 1;      // Convert into  number of 32-bit units
  do {
    i = 0;
    Data32 =  *pSrc;
    FS_NOR_DI();  /* Disable interrupts, to be on the safe side  */
    AMD_PROGRAM(BaseAddr);
    *pDest = Data32;
    /* Wait for operation to finish */
    while (a = *pDest, (a ^ *pDest) & 0x400040UL) {     /* Check toggle bit */
      if (++i > 10000) {                              /* Check timeout */
        AMD_RESET(BaseAddr);
        FS_NOR_EI();  /* Enable interrupts */
        return 1;
      }
      FS_NOR_DELAY();
    }
    /* Verify */
    if (*pDest != Data32) {
      AMD_RESET(BaseAddr);
      FS_NOR_EI();  /* Enable interrupts */
      return 1;
    }
    FS_NOR_EI();  /* Enable interrupts */
    ++pDest;
    ++pSrc;
  } while (--NumItems);
  return 0;
}

/*********************************************************************
*
*       _Read
*
* Purpose:
*   Reads data from the given address of the flash.
*/
static int _Read(U8 Unit, void * pDest, U32 Addr, U32 NumBytes) {
  FS_USE_PARA(Unit);
  FS_MEMCPY(pDest, (const void*)Addr, NumBytes);
  return 0;
}

/*********************************************************************
*
*      Public code
*
**********************************************************************
*/
/*********************************************************************
*
*      FS_NOR_CFI_ReadCFI_2x16
*
*  Function description
*    Reads CFI data from hardware into buffer.
*    Note that every 16-bit value from flash contains just a single byte.
*
*  Note
*    (1) Dual flash
*        We consider the CFI info of the flash at the even addr (A1 == 0) only, assuming that the 2 flashes are identical.
*/
void FS_NOR_CFI_ReadCFI_2x16(U8 Unit, U32 BaseAddr, U32 Off, U8 * pData, unsigned NumItems) {
  volatile U32 FS_NOR_FAR * pAddr;

  FS_USE_PARA(Unit);
  pAddr = (volatile U32 FS_NOR_FAR *)(BaseAddr + (Off << 2));
  FS_NOR_DI();
  CFI_READCONFIG(BaseAddr);
  do {
    *pData++ = (U8)*pAddr++;  // Only the low byte of the CFI data is relevant
  } while(--NumItems);
  CFI_RESET(BaseAddr);
  FS_NOR_EI();
}


/*********************************************************************
*
*      Public data
*
**********************************************************************
*/
const FS_NOR_PROGRAM_HW FS_NOR_Program_Intel_2x16 = {
  _Read,
  _EraseSectorINTEL,
  _WriteINTEL,
};

const FS_NOR_PROGRAM_HW FS_NOR_Program_AMD_2x16 = {
  _Read,
  _EraseSectorAMD,
  _WriteAMD,
};



/*************************** End of file ****************************/
