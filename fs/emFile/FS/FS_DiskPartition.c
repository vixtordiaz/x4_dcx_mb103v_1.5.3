/*********************************************************************
*                SEGGER MICROCONTROLLER GmbH & Co. KG                *
*        Solutions for real time microcontroller applications        *
**********************************************************************
*                                                                    *
*        (c) 2003-2012     SEGGER Microcontroller GmbH & Co KG       *
*                                                                    *
*        Internet: www.segger.com    Support:  support@segger.com    *
*                                                                    *
**********************************************************************

**** emFile file system for embedded applications ****
emFile is protected by international copyright laws. Knowledge of the
source code may not be used to write a similar product. This file may
only be used in accordance with a license and should not be re-
distributed in any way. We appreciate your understanding and fairness.
----------------------------------------------------------------------
File        : FS_DiskPartition.c
Purpose     : Logical volume driver
---------------------------END-OF-HEADER------------------------------
*/

/*********************************************************************
*
*             #include Section
*
**********************************************************************
*/
#include "FS_Int.h"

/*********************************************************************
*
*       defines, configurable
*
**********************************************************************
*/
#ifdef FS_DISKPART_MAXUNIT
  #define NUM_UNITS   FS_DISKPART_MAXUNIT
#else
  #define NUM_UNITS   4
#endif

/*********************************************************************
*
*       typedefs
*
**********************************************************************
*/
typedef struct {
  U8  Unit;
  U8  DeviceUnit;
  U8  PartIndex;
  U8  HasError;
  const FS_DEVICE_TYPE * pDeviceType;
  U32 StartSector;
  U32 NumSectors;
  U16 BytesPerSector;
} DISKPART_INST;

/*********************************************************************
*
*       Static data
*
**********************************************************************
*/
static DISKPART_INST * _apInst[NUM_UNITS];
static int             _NumUnits;

/*********************************************************************
*
*       Macros, function replacement
*
**********************************************************************
*/

/*********************************************************************
*
*       ASSERT_UNIT_NO_IS_IN_RANGE
*/
#if FS_DEBUG_LEVEL >= FS_DEBUG_LEVEL_CHECK_ALL
  #define ASSERT_UNIT_NO_IS_IN_RANGE(Unit)                                          \
    if (Unit >= NUM_UNITS) {                                                        \
      FS_DEBUG_ERROROUT((FS_MTYPE_DRIVER, "DISKPART: Invalid unit number.\n"));     \
      FS_X_Panic(FS_ERROR_UNKNOWN_DEVICE);                                          \
    }
#else
  #define ASSERT_UNIT_NO_IS_IN_RANGE(Unit)
#endif

/*********************************************************************
*
*       ASSERT_PART_INDEX_IS_IN_RANGE
*/
#if FS_DEBUG_LEVEL >= FS_DEBUG_LEVEL_CHECK_ALL
  #define ASSERT_PART_INDEX_IS_IN_RANGE(PartIndex)                                  \
  if (PartIndex >= 4) {                                                             \
      FS_DEBUG_ERROROUT((FS_MTYPE_DRIVER, "DISKPART: Invalid partition index.\n")); \
      FS_X_Panic(FS_ERROR_INVALID_PARA);                                            \
    }
#else
  #define ASSERT_PART_INDEX_IS_IN_RANGE(PartIndex)
#endif

/*********************************************************************
*
*       ASSERT_SECTORS_ARE_IN_RANGE
*/
#if FS_DEBUG_LEVEL >= FS_DEBUG_LEVEL_CHECK_ALL
  #define ASSERT_SECTORS_ARE_IN_RANGE(pInst, SectorIndex, NumSectors)               \
  if ((SectorIndex >= pInst->NumSectors) ||                                         \
      ((SectorIndex + NumSectors) > pInst->NumSectors)) {                           \
      FS_DEBUG_ERROROUT((FS_MTYPE_DRIVER, "DISKPART: Invalid partition index.\n")); \
      FS_X_Panic(FS_ERROR_INVALID_PARA);                                            \
    }
#else
  #define ASSERT_SECTORS_ARE_IN_RANGE(pInst, SectorIndex, NumSectors)
#endif

/*********************************************************************
*
*       ASSERT_DEVICE_IS_SET
*/
#if FS_DEBUG_LEVEL >= FS_DEBUG_LEVEL_CHECK_ALL
  #define ASSERT_DEVICE_IS_SET(pInst)                                               \
    if (pInst->pDeviceType == NULL) {                                                   \
      FS_DEBUG_ERROROUT((FS_MTYPE_DRIVER, "DISKPART: Device not set.\n"));          \
      FS_X_Panic(FS_ERROR_UNKNOWN_DEVICE);                                          \
    }
#else
  #define ASSERT_DEVICE_IS_SET(Unit)
#endif

/*********************************************************************
*
*       Static code
*
**********************************************************************
*/

/*********************************************************************
*
*       _InitMedium
*
*   Function description
*     Initializes the underlying driver.
*/
static int _InitMedium(DISKPART_INST * pInst) {
  int r;
  U8  DeviceUnit;
  const FS_DEVICE_TYPE * pDeviceType;

  ASSERT_DEVICE_IS_SET(pInst);
  pDeviceType = pInst->pDeviceType;
  DeviceUnit = pInst->DeviceUnit;
  r = pDeviceType->pfInitMedium(DeviceUnit);
  if (r) {
    FS_DEBUG_ERROROUT((FS_MTYPE_DRIVER, "DISKPART: Could not initialize storage medium.\n"));
  }
  return r;
}

/*********************************************************************
*
*       _GetDeviceInfo
*
*   Function description
*     Reads device information from the underlying driver.
*/
static int _GetDeviceInfo(DISKPART_INST * pInst, FS_DEV_INFO * pDevInfo) {
  int r;
  U8  DeviceUnit;
  const FS_DEVICE_TYPE * pDeviceType;

  pDeviceType = pInst->pDeviceType;
  DeviceUnit  = pInst->DeviceUnit;
  r = pDeviceType->pfIoCtl(DeviceUnit, FS_CMD_GET_DEVINFO, 0, (void *)pDevInfo);
  if (r) {
    FS_DEBUG_ERROROUT((FS_MTYPE_DRIVER, "DISKPART: Could not get storage info.\n"));
  }
  return r;
}

/*********************************************************************
*
*       _ReadMBR
*
*   Function description
*     Reads the contents of Master Boot Record (MBR) and checks if the signature is valid.
*/
static int _ReadMBR(DISKPART_INST * pInst, U8 * pBuffer) {
  int   r;
  U16   Signature;
  U8  * p;
  U8    DeviceUnit;
  const FS_DEVICE_TYPE * pDeviceType;

  pDeviceType = pInst->pDeviceType;
  DeviceUnit  = pInst->DeviceUnit;
  //
  // Read the MBR from storage medium. This is always the first sector.
  //
  r = pDeviceType->pfRead(DeviceUnit, 0, pBuffer, 1);
  if (r) {
    FS_DEBUG_ERROROUT((FS_MTYPE_DRIVER, "DISKPART: Could not read MBR.\n"));
    return 1;           // Error, failed to read MBR.
  }
  //
  // MBR read, check whether the signature is correct.
  //
  r = 0;
  p = pBuffer + MBR_OFF_SIGNATURE;
  Signature = FS_LoadU16LE(p);
  if (Signature != MBR_SIGNATURE) {
    FS_DEBUG_ERROROUT((FS_MTYPE_DRIVER, "DISKPART: Invalid MBR signature.\n"));
    r = 1;              // Error, invalid signature.
  }
  return r;
}

/*********************************************************************
*
*       _ReadSectors
*
*   Function description
*     Loads a number of sectors from the storage medium.
*/
static int _ReadSectors(DISKPART_INST * pInst, U32 SectorIndex, void * pBuffer, U32 NumSectors) {
  int   r;
  U32   StartSector;
  U8    DeviceUnit;
  const FS_DEVICE_TYPE * pDeviceType;

  if (pInst->HasError) {
    return 1;
  }
  ASSERT_SECTORS_ARE_IN_RANGE(pInst, SectorIndex, NumSectors);
  ASSERT_DEVICE_IS_SET(pInst);
  pDeviceType  = pInst->pDeviceType;
  DeviceUnit   = pInst->DeviceUnit;
  StartSector  = pInst->StartSector;
  SectorIndex += StartSector;
  r = pDeviceType->pfRead(DeviceUnit, SectorIndex, pBuffer, NumSectors);
  if (r) {
    FS_DEBUG_ERROROUT((FS_MTYPE_DRIVER, "DISKPART: Could not read sectors.\n"));
  }
  return r;
}

/*********************************************************************
*
*       _ReadPartInfo
*
*   Function description
*     Reads information about the location of the partition from MBR.
*/
static int _ReadPartInfo(DISKPART_INST * pInst) {
  int           r;
  U8            PartIndex;
  FS_PART_INFO  PartInfo;
  U32           StartSector;
  U32           NumSectors;
  FS_DEV_INFO   DevInfo;
  U16           BytesPerSector;
  U8            HasError;
  U32           NumSectorsDevice;
  U8          * pBuffer;

  PartIndex      = pInst->PartIndex;
  StartSector    = 0;
  NumSectors     = 0;
  BytesPerSector = 0;
  HasError       = 1;                // Set to indicate error until the instance is initialized successfully.
  r              = 1;               // Set to indicate error.  
  pBuffer = FS__AllocSectorBuffer();
  if (pBuffer) {
    //
    // Read information form the partition table of MBR.
    //
    r = _ReadMBR(pInst, pBuffer);
    if (r == 0) {
      //
      // The contents of MBR is now in buffer. Decode the configured partition table entry.
      //
      FS__LoadPartitionInfo(PartIndex, &PartInfo, pBuffer);
      if (PartInfo.IsValid) {
        StartSector = PartInfo.PartStartSector;
        NumSectors  = PartInfo.NumSectors4Part;
        //
        // Get the sector size from device.
        //
        r = _GetDeviceInfo(pInst, &DevInfo);
        if (r == 0) {
          BytesPerSector   = DevInfo.BytesPerSector;
          NumSectorsDevice = DevInfo.NumSectors;
          //
          // Validity checks:
          //   - the number of sectors must be valid
          //   - the partition should fit on the storage medium
          //
          if        (NumSectors == 0) {
            FS_DEBUG_ERROROUT((FS_MTYPE_DRIVER, "DISKPART: Invalid number of sectors.\n"));
          } else if ((StartSector >= NumSectorsDevice) ||
                     ((StartSector + NumSectors) > NumSectorsDevice)) {
            FS_DEBUG_ERROROUT((FS_MTYPE_DRIVER, "DISKPART: Partition exceeds device size.\n"));
          } else {
            FS_DEBUG_LOG((FS_MTYPE_DRIVER, "DISKPART: Found PartIndex: %d, StartSector: %u, NumSectors: %u.\n", PartIndex, StartSector, NumSectors));
            HasError = 0;
          }
        }
      }
    }
    FS__FreeSectorBuffer(pBuffer);
  }
  pInst->HasError       = HasError;
  pInst->StartSector    = StartSector;
  pInst->NumSectors     = NumSectors;
  pInst->BytesPerSector = BytesPerSector;
  return r;
}

/*********************************************************************
*
*       _ReadPartInfoIfRequired
*
*   Function description
*     Reads information about the location of the partition from MBR only if it is not already present.
*/
static int _ReadPartInfoIfRequired(DISKPART_INST * pInst) {
  int r;

  if (pInst->HasError) {
    return 1;               // Error
  }
  if (pInst->NumSectors) {
    return 0;               // OK, information already read;
  }
  r = _ReadPartInfo(pInst);
  return r;
}

/*********************************************************************
*
*       _GetStatus
*
*   Function description
*     Retuns whether the storage medium is present or not.
*/
static int _GetStatus(DISKPART_INST * pInst) {
  int Status;
  U8  DeviceUnit;
  const FS_DEVICE_TYPE * pDeviceType;

  Status = FS_MEDIA_NOT_PRESENT;
  if ((pInst->HasError == 0) && (pInst->pDeviceType)) {
    pDeviceType = pInst->pDeviceType;
    DeviceUnit  = pInst->DeviceUnit;
    Status      = pDeviceType->pfGetStatus(DeviceUnit);
  }
  return Status;
}

/*********************************************************************
*
*       _WriteSectors
*
*   Function description
*     Stores a number of sectors to storage medium.
*/
static int _WriteSectors(DISKPART_INST * pInst, U32 SectorIndex, const void * pBuffer, U32 NumSectors, U8 RepeatSame) {
  int   r;
  U32   StartSector;
  U8    DeviceUnit;
  const FS_DEVICE_TYPE * pDeviceType;

  if (pInst->HasError) {
    return 1;
  }
  ASSERT_SECTORS_ARE_IN_RANGE(pInst, SectorIndex, NumSectors);
  ASSERT_DEVICE_IS_SET(pInst);
  pDeviceType  = pInst->pDeviceType;
  DeviceUnit   = pInst->DeviceUnit;
  StartSector  = pInst->StartSector;
  SectorIndex += StartSector;
  r = pDeviceType->pfWrite(DeviceUnit, SectorIndex, pBuffer, NumSectors, RepeatSame);
  if (r) {
    FS_DEBUG_ERROROUT((FS_MTYPE_DRIVER, "DISKPART: Could not write sectors.\n"));
  }
  return r;
}

/*********************************************************************
*
*       Global functions
*
**********************************************************************
*/

/*********************************************************************
*
*       _DISKPART_GetDriverName
*
*   Function description
*     FS driver function. Returns the driver name.
*/
static const char * _DISKPART_GetDriverName(U8 Unit) {
  FS_USE_PARA(Unit);
  return "diskpart";
}

/*********************************************************************
*
*       _DISKPART_AddDevice
*
*   Function description
*     FS driver function. Creates a driver instance.
*/
static int _DISKPART_AddDevice(void) {
  DISKPART_INST * pInst;
  U8              Unit;

  if (_NumUnits >= NUM_UNITS) {
    FS_DEBUG_ERROROUT((FS_MTYPE_DRIVER, "DISKPART: Could not add device. Too many instances.\n"));
    return -1;                      // Error, too many instances defined.
  }
  Unit  = _NumUnits;
  pInst = _apInst[Unit];
  if (pInst == NULL) {
    FS_AllocZeroedPtr((void**)&pInst, sizeof(DISKPART_INST));
    _apInst[Unit] = pInst;
    pInst->Unit = Unit;
    ++_NumUnits;
  }
  return Unit;                      // OK, instance created.
}

/*********************************************************************
*
*       _DISKPART_Read
*
*   Function description
*     FS driver function. Reads a number of sectors from storage medium.
*/
static int _DISKPART_Read(U8 Unit, U32 SectorIndex, void * pBuffer, U32 NumSectors) {
  DISKPART_INST * pInst;
  int             r;

  ASSERT_UNIT_NO_IS_IN_RANGE(Unit);
  r     = 1;                // Set to indicate an error.
  pInst = _apInst[Unit];
  if (pInst) {
    r = _ReadSectors(pInst, SectorIndex, pBuffer, NumSectors);
  }
  return r;
}

/*********************************************************************
*
*       _DISKPART_Write
*
*   Function description
*     FS driver function. Writes a number of sectors to storage medium.
*/
static int _DISKPART_Write(U8 Unit, U32 SectorIndex, const void * pBuffer, U32 NumSectors, U8 RepeatSame) {
  DISKPART_INST * pInst;
  int             r;

  ASSERT_UNIT_NO_IS_IN_RANGE(Unit);
  r     = 1;                // Set to indicate an error.
  pInst = _apInst[Unit];
  if (pInst) {
    r = _WriteSectors(pInst, SectorIndex, pBuffer, NumSectors, RepeatSame);
  }
  return r;
}

/*********************************************************************
*
*       _DISKPART_IoCtl
*
*   Function description
*     FS driver function. Executes an I/O control command.
*/
static int _DISKPART_IoCtl(U8 Unit, I32 Cmd, I32 Aux, void * pBuffer) {
  DISKPART_INST * pInst;
  int             r;
  U8              DeviceUnit;
  FS_DEV_INFO   * pDevInfo;
  int             RelayCmd;
  U32             SectorIndex;
  const FS_DEVICE_TYPE * pDeviceType;

  ASSERT_UNIT_NO_IS_IN_RANGE(Unit);
  pInst = _apInst[Unit];
  FS_USE_PARA(Aux);
  r        = -1;              // Set to indicate an error.
  RelayCmd = 1;               // By default, pass the commands to the underlying driver
  DeviceUnit   = 0;
  pDeviceType  = NULL;
  if (pInst) {
    DeviceUnit  = pInst->DeviceUnit;
    pDeviceType = pInst->pDeviceType;
  }
  switch (Cmd) {
  case FS_CMD_GET_DEVINFO:
    if (pBuffer && pInst) {
      r = _ReadPartInfoIfRequired(pInst);
      if (r == 0) {
        pDevInfo = (FS_DEV_INFO *)pBuffer;
        pDevInfo->NumSectors     = pInst->NumSectors;
        pDevInfo->BytesPerSector = pInst->BytesPerSector;
      }
    }
    RelayCmd = 0;             // Command is handled at this level.
    break;
  case FS_CMD_DEINIT:
    if (pInst) {
      FS_FREE(pInst);
      pInst = NULL;
      _apInst[Unit] = NULL;
      _NumUnits--;
    }
    break;
  case FS_CMD_UNMOUNT:
    // thru
  case FS_CMD_UNMOUNT_FORCED:
    if (pInst) {
      pInst->HasError       = 0;
      pInst->NumSectors     = 0;
      pInst->StartSector    = 0;
      pInst->BytesPerSector = 0;
    }
    break;
  case FS_CMD_FREE_SECTORS:
    //
    // SectorIndex is relative to the begining of partition but the driver 
    // expects the absolute logical sector index. The command is relayed.
    //
    if (pInst) {
      SectorIndex  = (U32)Aux;
      SectorIndex += pInst->StartSector;
      Aux          = SectorIndex;
    }
    break;
  default:
    //
    // All other commands are relayed to the underlying driver(s).
    //
    break;
  }
  if (RelayCmd) {
    if (pDeviceType) {
      r = pDeviceType->pfIoCtl(DeviceUnit, Cmd, Aux, pBuffer);
    }
  }
  return r;
}

/*********************************************************************
*
*       _DISKPART_InitMedium
*
*   Function description
*     FS driver function. Initalizes the storage medium.
*/
static int _DISKPART_InitMedium(U8 Unit) {
  int             r;
  DISKPART_INST * pInst;

  ASSERT_UNIT_NO_IS_IN_RANGE(Unit);
  r     = 1;                // Set to indicate an error.
  pInst = _apInst[Unit];
  if (pInst) {
    r = _InitMedium(pInst);
  }
  return r;
}

/*********************************************************************
*
*       _DISKPART_GetStatus
*
*   Function description
*     FS driver function. Returns whether the storage media is present or not.
*/
static int _DISKPART_GetStatus(U8 Unit) {
  DISKPART_INST * pInst;
  int             Status;

  ASSERT_UNIT_NO_IS_IN_RANGE(Unit);
  Status = FS_MEDIA_NOT_PRESENT;    // Set to indicate an error.
  pInst  = _apInst[Unit];
  if (pInst) {
    Status = _GetStatus(pInst);
  }
  return Status;
}

/*********************************************************************
*
*       _DISKPART_GetNumUnits
*
*   Function description
*     FS driver function. Returns the number of driver instances.
*/
static int _DISKPART_GetNumUnits(void) {
  return _NumUnits;
}

/*********************************************************************
*
*       FS_DISKPART_Driver
*
*/
const FS_DEVICE_TYPE FS_DISKPART_Driver = {
  _DISKPART_GetDriverName,
  _DISKPART_AddDevice,
  _DISKPART_Read,
  _DISKPART_Write,
  _DISKPART_IoCtl,
  _DISKPART_InitMedium,
  _DISKPART_GetStatus,
  _DISKPART_GetNumUnits
};

/*********************************************************************
*
*       Public code
*
**********************************************************************
*/

/*********************************************************************
*
*       FS_DISKPART_Configure
*
*   Function description
*     Sets the parameters which allows the driver instance to access the partition table stored in Master Boot Record (MBR).
*/
void FS_DISKPART_Configure(U8 Unit, const FS_DEVICE_TYPE * pDeviceType, U8 DeviceUnit, U8 PartIndex) {
  DISKPART_INST * pInst;

  ASSERT_UNIT_NO_IS_IN_RANGE(Unit);
  ASSERT_PART_INDEX_IS_IN_RANGE(PartIndex);
  pInst              = _apInst[Unit];
  pInst->pDeviceType = pDeviceType;
  pInst->DeviceUnit  = DeviceUnit;
  pInst->PartIndex   = PartIndex;
}

/*************************** End of file ****************************/
