/*********************************************************************
*                SEGGER MICROCONTROLLER GmbH & Co. KG                *
*        Solutions for real time microcontroller applications        *
**********************************************************************
*                                                                    *
*        (c) 2003-2012     SEGGER Microcontroller GmbH & Co KG       *
*                                                                    *
*        Internet: www.segger.com    Support:  support@segger.com    *
*                                                                    *
**********************************************************************

**** emFile file system for embedded applications ****
emFile is protected by international copyright laws. Knowledge of the
source code may not be used to write a similar product. This file may
only be used in accordance with a license and should not be re-
distributed in any way. We appreciate your understanding and fairness.
----------------------------------------------------------------------
File        : NAND_PHY_2048x8.c
Purpose     : Large page NAND flashes physical 8-bit access
Literature  : [1] \\fileserver\techinfo\Company\Samsung\NAND_Flash\Device\K9K8G08U0A_2KPageSLC_R11.pdf
              [2] \\fileserver\techinfo\Company\Micron\NAND\MT29F2G0_8AAD_16AAD_08ABD_16ABD.pdf
---------------------------END-OF-HEADER------------------------------
*/

/*********************************************************************
*
*             #include Section
*
**********************************************************************
*/
#include "FS_Int.h"
#include "FS_CLib.h"
#include "FS_Debug.h"
#include "NAND_X_HW.h"

/*********************************************************************
*
*       Defines, configurable
*
**********************************************************************
*/
#ifdef FS_NAND_MAXUNIT
  #define NUM_UNITS   FS_NAND_MAXUNIT
#else
  #define NUM_UNITS   4
#endif

/*********************************************************************
*
*       Defines, not configurable
*
**********************************************************************
*/
/* Status */
#define STATUS_ERROR            0x01    /* 0:Pass,          1:Fail */
#define STATUS_READY            0x40    /* 0:Busy,          1:Ready */
#define STATUS_WRITE_PROTECTED  0x80    /* 0:Protect,       1:Not Protect */

/*********************************************************************
*
*       NAND commands
*/
#define NAND_CMD_READ_1          0x00     // Start read access. Followed by 2-byte Col, 3 byte Row, then 0x30
#define NAND_CMD_READ_2          0x30     
#define NAND_CMD_READ_RANDOM_0   0x05     // Modifies Col-Addr. Followed by 2-byte ColAddr and 0xe0
#define NAND_CMD_READ_RANDOM_1   0xE0

#define NAND_CMD_WRITE_1         0x80
#define NAND_CMD_WRITE_RANDOM    0x85     // Modifies Col-Addr. Followed by 2-byte ColAddr, then data
#define NAND_CMD_PROGRAM         0x10

#define NAND_CMD_ERASE_1         0x60
#define NAND_CMD_READ_STATUS     0x70
#define NAND_CMD_READ_ID         0x90
#define NAND_CMD_ERASE_2         0xD0
#define NAND_CMD_RESET_CHIP      0xFF

#define PAGE_NO_INVALID          0xFFFFFFFF

/*********************************************************************
*
*       typedefs
*
**********************************************************************
*/
typedef struct {
  U32 PageNoRequested;    // Number of the last page read from NAND flash
} PHY_INST;

/*********************************************************************
*
*       Static data
*
**********************************************************************
*/
static PHY_INST _aInst[NUM_UNITS];

/*********************************************************************
*
*       Static code
*
**********************************************************************
*/

/*********************************************************************
*
*       _WriteCmd
*
*  Function description
*    Writes a single byte command to the NAND flash
*/
static void _WriteCmd(U8 Unit, U8 Cmd) {
  FS_NAND_HW_X_SetCmdMode(Unit);
  FS_NAND_HW_X_Write_x8(Unit, &Cmd, 1);
  FS_NAND_HW_X_SetDataMode(Unit);      // Switch back to data mode (default)
}

/*********************************************************************
*
*       _StartOperation
*
*  Function description
*    Enables CE and writes a single byte command to the NAND flash
*/
static void _StartOperation(U8 Unit, U8 Cmd) {
  FS_NAND_HW_X_EnableCE(Unit);
  _WriteCmd(Unit, Cmd);
}

/*********************************************************************
*
*       _WriteRowAddr
*
*  Function description
*    Writes the row addr into the NAND flash.
*
*  Parameters
*    RowAddr   Zero-based page index.
*
*  Notes
*    (1) RowAddr
*        This is the zero based page index.
*        A block consists of 64 pages, so that BlockIndex = RowAddr / 64.
*/
static void _WriteRowAddr(U8 Unit, unsigned RowAddr) {
  U8 aAddr[3];

  FS_NAND_HW_X_SetAddrMode(Unit);
  FS_StoreU24LE(&aAddr[0], RowAddr);
  FS_NAND_HW_X_Write_x8(Unit, aAddr, sizeof(aAddr));
}

/*********************************************************************
*
*       _WriteCRAddr
*
*  Function description
*    Writes the column and row addr into the NAND flash.
*
*  Parameters
*    RowAddr   Zero-based page index.
*    ColAddr   Byte-offset within a page
*/
static void _WriteCRAddr(U8 Unit, unsigned ColAddr, unsigned RowAddr) {
  U8 aAddr[5];

  FS_NAND_HW_X_SetAddrMode(Unit);
  FS_StoreU16LE(&aAddr[0], ColAddr);
  FS_StoreU24LE(&aAddr[2], RowAddr);
  FS_NAND_HW_X_Write_x8(Unit, aAddr, sizeof(aAddr));
  FS_NAND_HW_X_SetDataMode(Unit);
}

/*********************************************************************
*
*       _WriteCAddr
*
*  Function description
*    Writes the column into the NAND flash.
*
*  Parameters
*    ColAddr   Byte-offset within the selected page
*/
static void _WriteCAddr(U8 Unit, unsigned ColAddr) {
  U8 aAddr[2];

  FS_NAND_HW_X_SetAddrMode(Unit);
  FS_StoreU16LE(&aAddr[0], ColAddr);
  FS_NAND_HW_X_Write_x8(Unit, aAddr, sizeof(aAddr));
  FS_NAND_HW_X_SetDataMode(Unit);
}

/*********************************************************************
*
*       _ReadStatus
*
*  Function description
*    Reads and returns the contents of the status register.
*/
static U8 _ReadStatus(U8 Unit) {
  U8 r;

  _WriteCmd(Unit, NAND_CMD_READ_STATUS);
  FS_NAND_HW_X_Read_x8(Unit, &r, 1);
  return r;
}

/*********************************************************************
*
*       _ResetErr
*
*  Function description
*    Resets the NAND flash by command
*/
static void _ResetErr(U8 Unit) {
  U8 Status;
  PHY_INST * pInst;

  pInst   = &_aInst[Unit];
  pInst->PageNoRequested = PAGE_NO_INVALID;
  _StartOperation(Unit, NAND_CMD_RESET_CHIP);
  do {
    Status = _ReadStatus(Unit);
  } while ((Status & STATUS_READY) == 0);
  FS_NAND_HW_X_DisableCE(Unit);
}

/*********************************************************************
*
*       _WaitBusy
*
*  Function description
*    Waits until the NAND device has completed an operation
*
*  Return value:
*      0                       - Success.
*    !=0                       - An error has occurred.
*/
static int _WaitBusy(U8 Unit) {
  U8 Status;
  Status = 0;
  //
  // Try to use the hardware pin to find out when busy is cleared.
  //
  if (FS_NAND_HW_X_WaitWhileBusy(Unit, 0)) {
    //
    // Busy pin is not connected. We use status read instead.
    //
      do {
        Status = _ReadStatus(Unit);
      } while ((Status & STATUS_READY) == 0);
    } else {
      Status = _ReadStatus(Unit);
    }
  if (Status & STATUS_ERROR) {
    _ResetErr(Unit);
    return 1;                       // Error
  }
  return 0;                         // Success
}

/*********************************************************************
*
*       _EndOperation
*
*  Function description
*    Checks status register to find out if operation was successful and disables CE.
*
*  Return value:
*      0                       - Data successfully transferred.
*    !=0                       - An error has occurred.
*/
static int _EndOperation(U8 Unit) {
  U8 Status;

  Status = _ReadStatus(Unit);
  if ((Status & (STATUS_ERROR | STATUS_READY)) != STATUS_READY) {
    _ResetErr(Unit);
    return 1;                        // Error
  }
  FS_NAND_HW_X_DisableCE(Unit);
  return 0;                          // O.K.
}

/*********************************************************************
*
*       _WaitEndOperation
*
*  Function description
*    Waits until the current operation is completed (Checking busy)
*    and ends operation, disabling CE.
*
*  Return value:
*      0                       - Data successfully transferred.
*      1                       - An error has occurred.
*/
static int _WaitEndOperation(U8 Unit) {
  if(_WaitBusy(Unit)) {
    return -1;
  }
  return _EndOperation(Unit);
}

/*********************************************************************
*
*       _Read
*
*  Function description
*    Reads data from a complete or a part of a page.
*    This code is identical for main memory and spare area; the spare area
*    is located right after the main area.
*
*  Return value:
*      0                       - Data successfully transferred.
*    !=0                       - An error has occurred.
*/
static int _Read(U8 Unit, U32 PageNo, void * pBuffer, unsigned Off, unsigned NumBytes) {
  PHY_INST * pInst;

  pInst = &_aInst[Unit];
  if (PageNo != pInst->PageNoRequested) {
    _StartOperation(Unit, NAND_CMD_READ_1);
    _WriteCRAddr(Unit, Off, PageNo);
    _WriteCmd(Unit, NAND_CMD_READ_2);
    if (_WaitBusy(Unit)) {
      return 1;                            // Error
    }
    _StartOperation(Unit, NAND_CMD_READ_1); // Restore the read command overwritten by _WaitBusy()
    FS_NAND_HW_X_Read_x8(Unit, pBuffer, NumBytes);
    pInst->PageNoRequested = PageNo;
  } else {
    _StartOperation(Unit, NAND_CMD_READ_1); // Restore the read command overwritten by _WaitBusy()
    _WriteCmd(Unit, NAND_CMD_READ_RANDOM_0);      // 0x05
    _WriteCAddr(Unit, Off);
    _WriteCmd(Unit, NAND_CMD_READ_RANDOM_1);      // 0xE0
    FS_NAND_HW_X_Read_x8(Unit, pBuffer, NumBytes);     // Read second data (usually spare)
  }
  return _EndOperation(Unit);
}

/*********************************************************************
*
*       _ReadEx
*
*  Function description
*    Reads data from 2 parts of a page.
*    Typically used to read data and spare area at the same time.
*
*  Return value:
*      0                       - Data successfully transferred.
*    !=0                       - An error has occurred.
*
*  Notes
*    (1) Literature
*        Procedure taken from [1], Random data output in a Page, p. 30
*/
static int _ReadEx(U8 Unit, U32 PageNo, void * pBuffer0, unsigned Off0, unsigned NumBytes0, void * pBuffer1, unsigned Off1, unsigned NumBytes1) {
  PHY_INST * pInst;

  //
  // Copy Page into RAM buffer (Read)
  //
  pInst = &_aInst[Unit];
  if (PageNo != pInst->PageNoRequested) {
    _StartOperation(Unit, NAND_CMD_READ_1);       // 0x00
    _WriteCRAddr(Unit, Off0, PageNo);
    _WriteCmd(Unit, NAND_CMD_READ_2);             // 0x30
    if (_WaitBusy(Unit)) {
      return 1;                            // Error
    }
    _StartOperation(Unit, NAND_CMD_READ_1); // Restore the read command overwritten by _WaitBusy()
    //
    // Read up data from RAM buffer
    //
    FS_NAND_HW_X_Read_x8(Unit, pBuffer0, NumBytes0);     // Read first data
    pInst->PageNoRequested = PageNo;
  } else {
    _StartOperation(Unit, NAND_CMD_READ_1);
    _WriteCmd(Unit, NAND_CMD_READ_RANDOM_0);      // 0x05
    _WriteCAddr(Unit, Off0);
    _WriteCmd(Unit, NAND_CMD_READ_RANDOM_1);      // 0xE0
    FS_NAND_HW_X_Read_x8(Unit, pBuffer0, NumBytes0);     // Read second data (usually spare)
  }
  //
  // Read second data area (typically the spare area) from RAM buffer
  //
  if (NumBytes1) {
    _WriteCmd(Unit, NAND_CMD_READ_RANDOM_0);      // 0x05
  _WriteCAddr(Unit, Off1);
    _WriteCmd(Unit, NAND_CMD_READ_RANDOM_1);      // 0xE0
  FS_NAND_HW_X_Read_x8(Unit, pBuffer1, NumBytes1);     // Read second data (usually spare)
  }
  return _EndOperation(Unit);
}

/*********************************************************************
*
*       _Write
*
*  Function description
*    Writes data into a complete or a part of a page.
*    This code is identical for main memory and spare area; the spare area
*    is located rigth after the main area.
*
*  Return value:
*      0                       - Data successfully transferred.
*    !=0                       - An error has occurred.
*/
static int _Write(U8 Unit, U32 PageNo, const void * pBuffer, unsigned Off, unsigned NumBytes) {
  PHY_INST * pInst;

  pInst = &_aInst[Unit];
  pInst->PageNoRequested = PAGE_NO_INVALID;
  _StartOperation(Unit, NAND_CMD_WRITE_1);
  _WriteCRAddr(Unit, Off, PageNo);
  FS_NAND_HW_X_Write_x8(Unit, pBuffer, NumBytes);
  _WriteCmd(Unit, NAND_CMD_PROGRAM);
  return _WaitEndOperation(Unit);
}

/*********************************************************************
*
*       _WriteEx
*
*  Function description
*    Writes data to 2 parts of a page.
*    Typically used to write data and spare area at the same time.
*
*  Return value:
*      0                       - Data successfully transferred.
*      1                       - An error has occurred.
*/
static int _WriteEx(U8 Unit, U32 PageNo, const void * pBuffer0, unsigned Off0, unsigned NumBytes0, const void * pBuffer1, unsigned Off1, unsigned NumBytes1) {
  PHY_INST * pInst;

  pInst = &_aInst[Unit];
  pInst->PageNoRequested = PAGE_NO_INVALID;
  _StartOperation(Unit, NAND_CMD_WRITE_1);
  _WriteCRAddr(Unit, Off0, PageNo);
  FS_NAND_HW_X_Write_x8(Unit, pBuffer0, NumBytes0);
  _WriteCmd(Unit, 0x85);
  _WriteCAddr(Unit, Off1);
  FS_NAND_HW_X_Write_x8(Unit, pBuffer1, NumBytes1);
  _WriteCmd(Unit, NAND_CMD_PROGRAM);
  return _WaitEndOperation(Unit);
}

/*********************************************************************
*
*       _EraseBlock
*
*  Function description
*    Erases a block.
*
*  Parameters
*    PageIndex      Index of the first page in the block to be erased.
*                   If the device has 64 pages per block, then the following values are permitted:
*                   0   ->  block 0
*                   64  ->  block 1
*                   128 ->  block 2
*                   etc.
*
*  Return value:
*      0                       - Data successfully transferred.
*      1                       - An error has occurred.
*/
static int _EraseBlock(U8 Unit, U32 PageIndex) {
  PHY_INST * pInst;

  pInst = &_aInst[Unit];
  pInst->PageNoRequested = PAGE_NO_INVALID;
  _StartOperation(Unit, NAND_CMD_ERASE_1);
  _WriteRowAddr(Unit, PageIndex);
  _WriteCmd(Unit, NAND_CMD_ERASE_2);
  return _WaitEndOperation(Unit);
}

/*********************************************************************
*
*       _InitGetDeviceInfo
*
*  Function description
*    Initializes hardware layer, resets NAND flash and tries to identify the NAND flash.
*    If the NAND flash can be handled, Device Info is filled.
*
*  Return value:
*      0                       - O.K., device can be handled
*      1                       - Error: device can not be handled
*
*  Notes
*       (1) The first command after power-on must be RESET (see [2])
*/
static int _InitGetDeviceInfo(U8 Unit, FS_NAND_DEVICE_INFO * pDevInfo) {
  U8  aId[5];
  U8  DeviceCode;
  U16 NumBlocks;
  const U8 Dummy = 0;

  FS_NAND_HW_X_Init_x8(Unit);
  _ResetErr(Unit);         // Note 1
  //
  // Retrieve Id information from NAND device
  //
  _StartOperation(Unit, NAND_CMD_READ_ID);
  FS_NAND_HW_X_SetAddrMode(Unit);
  FS_NAND_HW_X_Write_x8(Unit, &Dummy, 1);
  FS_NAND_HW_X_SetDataMode(Unit);
  FS_NAND_HW_X_Read_x8(Unit, &aId[0], sizeof(aId));
  if (_EndOperation(Unit)) {
    return 1;                    // Error
  }

  DeviceCode    = aId[1];
  switch(DeviceCode)   {
  case 0xA2:
  case 0xF2:
    NumBlocks    = 512;
    break;
  case 0xA1:
  case 0xF1:
  case 0x11:
    NumBlocks    = 1024;
    break;
  case 0xAA:
  case 0xDA:
    NumBlocks    = 2048;
    break;
  case 0xAC:
  case 0xDC:
    NumBlocks    = 4096;
    break;
  case 0xA3:
  case 0xD3:
    NumBlocks    = 8192;
    break;
  default:
    return 1;                 // Error
  }
  pDevInfo->BPP_Shift = 11;  // 2048 bytes/page
  pDevInfo->PPB_Shift =  6;  // Large page NAND flashes have 64 pages per block
  pDevInfo->NumBlocks = NumBlocks;
  return 0;
}

/*********************************************************************
*
*       _IsWP
*
*  Function description
*    Checks if the device is write protected.
*    This is done by reading bit 7 of the status register.
*    Typical reason for write protection is that either the supply voltage is too low
*    or the /WP-pin is active (low)
*
*  Return value:
*     <0          - Error
*      0          - Not write protected
*     >0          - Write protected
*/
static int _IsWP(U8 Unit) {
  U8 Status;

  FS_NAND_HW_X_EnableCE(Unit);
  Status = _ReadStatus(Unit);
  if (_EndOperation(Unit)) {
    return -1;                    // Error
  }
  return (Status & STATUS_WRITE_PROTECTED) ? 0 : 1;
}

/*********************************************************************
*
*       Public const
*
**********************************************************************
*/
const FS_NAND_PHY_TYPE FS_NAND_PHY_2048x8 = {
  _EraseBlock,
  _InitGetDeviceInfo,
  _IsWP,
  _Read,
  _ReadEx,
  _Write,
  _WriteEx
};


/*************************** End of file ****************************/
