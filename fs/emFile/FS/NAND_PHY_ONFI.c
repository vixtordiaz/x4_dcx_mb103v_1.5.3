/*********************************************************************
*                SEGGER MICROCONTROLLER GmbH & Co. KG                *
*        Solutions for real time microcontroller applications        *
**********************************************************************
*                                                                    *
*        (c) 2003-2012     SEGGER Microcontroller GmbH & Co KG       *
*                                                                    *
*        Internet: www.segger.com    Support:  support@segger.com    *
*                                                                    *
**********************************************************************

**** emFile file system for embedded applications ****
emFile is protected by international copyright laws. Knowledge of the
source code may not be used to write a similar product. This file may
only be used in accordance with a license and should not be re-
distributed in any way. We appreciate your understanding and fairness.
----------------------------------------------------------------------
File        : NAND_PHY_ONFI.c
Purpose     : Physical Layer for the NAND driver that uses ONFI
Literature  : [1] \\fileserver\techinfo\Subject\NANDFlash\ONFI\ONFI_23.pdf
              [2] \\fileserver\techinfo\Company\Micron\NAND\MT29F2G0_8AAD_16AAD_08ABD_16ABD.pdf
---------------------------END-OF-HEADER------------------------------
*/

#include "FS_Int.h"
#include "NAND_Private.h"
#include "NAND_X_HW.h"

/*********************************************************************
*
*       Define configurable
*
**********************************************************************
*/
#ifdef FS_NAND_MAXUNIT
  #define NUM_UNITS   FS_NAND_MAXUNIT
#else
  #define NUM_UNITS   4
#endif

/*********************************************************************
*
*       Defines, not configurable
*
**********************************************************************
*/

/*********************************************************************
*
*       Execution status
*/
#define STATUS_ERROR            0x01    // 0:Pass,    1:Fail
#define STATUS_READY            0x40    // 0:Busy,    1:Ready
#define STATUS_WRITE_PROTECTED  0x80    // 0:Protect, 1:Not Protect

/*********************************************************************
*
*       NAND commands
*/
#define CMD_READ_1          0x00
#define CMD_RANDOM_READ_1   0x05
#define CMD_WRITE_2         0x10
#define CMD_READ_2          0x30
#define CMD_ERASE_1         0x60
#define CMD_ERASE_2         0xD0
#define CMD_READ_STATUS     0x70
#define CMD_WRITE_1         0x80
#define CMD_RANDOM_WRITE    0x85
#define CMD_READ_ID         0x90
#define CMD_RANDOM_READ_2   0xE0
#define CMD_GET_FEATURES    0xEE
#define CMD_SET_FEATURES    0xEF
#define CMD_RESET           0xFF

/*********************************************************************
*
*       Device features
*/
#define NUM_FEATURE_PARA    4
#define MICRON_ECC_FEATURE_ADDR   0x90
#define MICRON_ECC_FEATURE_MASK   0x08

/*********************************************************************
*
*       Assert macros
*/
#if FS_DEBUG_LEVEL >= FS_DEBUG_LEVEL_CHECK_ALL
  #define ASSERT_PARA_IS_ALIGNED(pInst, Para)   if ((pInst)->DataBusWidth == 16) { FS_DEBUG_ASSERT(FS_MTYPE_DRIVER, ((Para) & 1) == 0); }
#else
  #define ASSERT_PARA_IS_ALIGNED(pInst, Para)
#endif

/*********************************************************************
*
*       typedefs
*
**********************************************************************
*/
typedef struct {
  U8 DataBusWidth;      // Widht of the data bus in bits (16 or 8)
  U8 NumColAddrBytes;   // Number of bytes in a column address
  U8 NumRowAddrBytes;   // Number of bytes in a row address
} PHY_INST;

/*********************************************************************
*
*       Static data
*
**********************************************************************
*/
static PHY_INST _aInst[NUM_UNITS];

/*********************************************************************
*
*       Static code
*
**********************************************************************
*/

/*********************************************************************
*
*       _ld
*/
static U16 _ld(U32 Value) {
  U16 i;
  for (i = 0; i < 16; i++) {
    if ((1UL << i) == Value) {
      break;
    }
  }
  return i;
}

/*********************************************************************
*
*       _WriteCmd
*
*   Function description
*     Writes a single byte command to the NAND flash
*/
static void _WriteCmd(U8 Unit, U8 Cmd) {
  FS_NAND_HW_X_SetCmdMode(Unit);
  FS_NAND_HW_X_Write_x8(Unit, &Cmd, sizeof(Cmd));
}

/*********************************************************************
*
*       _WriteAddrRow
*
*   Function description
*     Selects the address of the page to be accessed.
*/
static void _WriteAddrRow(U8 Unit, U32 RowAddr, unsigned NumRowAddrBytes) {
  U8         aAddr[sizeof(RowAddr)];
  U8       * p;
  unsigned   NumBytes;

  FS_NAND_HW_X_SetAddrMode(Unit);
  p        = aAddr;
  NumBytes = NumRowAddrBytes;
  do {
    *p++      = (U8)RowAddr;
    RowAddr >>= 8;
  } while (--NumBytes);
  FS_NAND_HW_X_Write_x8(Unit, aAddr, NumRowAddrBytes);
}

/*********************************************************************
*
*       _WriteAddrCol
*
*   Function description
*     Selects the address of the byte to be accessed.
*/
static void _WriteAddrCol(U8 Unit, U32 ColAddr, unsigned NumColAddrBytes, U8 DataBusWidth) {
  U8         aAddr[sizeof(ColAddr)];
  U8       * p;
  unsigned   NumBytes;

  FS_NAND_HW_X_SetAddrMode(Unit);
  if (DataBusWidth == 16) {
    ColAddr >>= 1;        // Convert to a 16-bit word address
  }
  p        = aAddr;
  NumBytes = NumColAddrBytes;
  do {
    *p++      = (U8)ColAddr;
    ColAddr >>= 8;
  } while (--NumBytes);
  FS_NAND_HW_X_Write_x8(Unit, aAddr, NumColAddrBytes);
}

/*********************************************************************
*
*       _WriteAddrColRow
*
*   Function description
*     Selects the byte and the page address to be accessed
*/
static void _WriteAddrColRow(U8 Unit, U32 ColAddr, U32 NumColAddrBytes, U32 RowAddr, U32 NumRowAddrBytes, U8 DataBusWidth) {
  U8         aAddr[sizeof(ColAddr) + sizeof(RowAddr)];
  U8       * p;
  unsigned   NumBytes;

  FS_NAND_HW_X_SetAddrMode(Unit);
  if (DataBusWidth == 16) {
    ColAddr >>= 1;        // Convert to a 16-bit word address
  }
  p        = aAddr;
  NumBytes = NumColAddrBytes;
  do {
    *p++      = (U8)ColAddr;
    ColAddr >>= 8;
  } while (--NumBytes);
  NumBytes = NumRowAddrBytes;
  do {
    *p++      = (U8)RowAddr;
    RowAddr >>= 8;
  } while (--NumBytes);
  FS_NAND_HW_X_Write_x8(Unit, aAddr, NumColAddrBytes + NumRowAddrBytes);
}

/*********************************************************************
*
*       _WriteAddrByte
*
*   Function description
*     Writes the byte address of the parameter to read from.
*/
static void _WriteAddrByte(U8 Unit, U8 ByteAddr) {
  FS_NAND_HW_X_SetAddrMode(Unit);
  FS_NAND_HW_X_Write_x8(Unit, &ByteAddr, sizeof(ByteAddr));
}

/*********************************************************************
*
*       _ReadData
*
*   Function description
*     Transfers data from device to host CPU.
*/
static void _ReadData(U8 Unit, void * pData, unsigned NumBytes, U8 DataBusWidth) {
  FS_NAND_HW_X_SetDataMode(Unit);
  if (DataBusWidth == 16) {
    FS_NAND_HW_X_Read_x16(Unit, pData, NumBytes);
  } else {
    FS_NAND_HW_X_Read_x8(Unit, pData, NumBytes);
  }
}

/*********************************************************************
*
*       _WriteData
*
*   Function description
*     Transfers data from host CPU to device.
*/
static void _WriteData(U8 Unit, const void * pData, unsigned NumBytes, U8 DataBusWidth) {
  FS_NAND_HW_X_SetDataMode(Unit);
  if (DataBusWidth == 16) {
    FS_NAND_HW_X_Write_x16(Unit, pData, NumBytes);
  } else {
    FS_NAND_HW_X_Write_x8(Unit, pData, NumBytes);
  }
}

/*********************************************************************
*
*       _ReadStatus
*
*   Function description
*     Reads and returns the contents of the status register.
*/
static U8 _ReadStatus(U8 Unit) {
  U8 Status;

  _WriteCmd(Unit, CMD_READ_STATUS);
  _ReadData(Unit, &Status, sizeof(Status), 8);
  return Status;
}

/*********************************************************************
*
*       _WaitBusy
*
*   Function description
*     Waits for the NAND to complete its last operation.
*
*   Parameters
*     Unit    Number of NAND unit
*
*   Return value
*    ==0  Success
*    !=0  An error has occurred
*/
static int _WaitBusy(U8 Unit) {
  U8 Status;

  Status = 0;
  //
  // Try to use the hardware pin to find out when busy is cleared.
  //
  FS_NAND_HW_X_WaitWhileBusy(Unit, 0);
  do {
    Status = _ReadStatus(Unit);
  } while ((Status & STATUS_READY) == 0);
  return (Status & STATUS_ERROR) ? 1 : 0;
}

/*********************************************************************
*
*       _Reset
*
*   Function description
*     Resets the NAND flash by command
*/
static void _Reset(U8 Unit) {
  U8 Status;

  FS_NAND_HW_X_EnableCE(Unit);
  _WriteCmd(Unit, CMD_RESET);
  Status = 0;
  do {
    Status = _ReadStatus(Unit);
  } while (!(Status & STATUS_READY));
  FS_NAND_HW_X_DisableCE(Unit);
}

/*********************************************************************
*
*       _GetFeatures
*
*   Function description
*     Reads the device settings.
*
*   Parameters
*     Unit    Device unit number
*     Addr    Feature address (see device datasheet)
*     pData   [IN]  ---
*             [OUT] Read device settings. Must be at least 4 bytes long.
*
*   Return value
*     ==0    Settings read
*     !=0    An error occurred
*/
static int _GetFeatures(U8 Unit, U8 Addr, U8 * pData) {
  PHY_INST * pInst;
  int r;

  pInst = &_aInst[Unit];
  FS_NAND_HW_X_EnableCE(Unit);
  _WriteCmd(Unit, CMD_GET_FEATURES);
  _WriteAddrByte(Unit, Addr);
  r = _WaitBusy(Unit);
  if (r == 0) {
    _WriteCmd(Unit, CMD_READ_1);     // Revert to read mode. _WaitBusy() change it to status mode.
    _ReadData(Unit, pData, NUM_FEATURE_PARA, pInst->DataBusWidth);
  }
  FS_NAND_HW_X_DisableCE(Unit);
  if (r) {
    _Reset(Unit);
  }
  return r;
}

/*********************************************************************
*
*       _SetFeatures
*
*   Function description
*     Modifies the device settings.
*
*   Parameters
*     Unit    Device unit number
*     Addr    Feature address (see device datasheet)
*     pData   [IN]  New device settings.  Must be at least 4 bytes long.
*             [OUT] ---
*
*   Return value
*     ==0    Settings written
*     !=0    An error occurred
*/
static int _SetFeatures(U8 Unit, U8 Addr, const U8 * pData) {
  PHY_INST * pInst;
  int r;

  pInst = &_aInst[Unit];
  FS_NAND_HW_X_EnableCE(Unit);
  _WriteCmd(Unit, CMD_SET_FEATURES);
  _WriteAddrByte(Unit, Addr);
  _WriteData(Unit, pData, NUM_FEATURE_PARA, pInst->DataBusWidth);
  r = _WaitBusy(Unit);
  FS_NAND_HW_X_DisableCE(Unit);
  if (r) {
    _Reset(Unit);
  }
  return r;
}

/*********************************************************************
*
*       _Read
*
*   Function description
*     Reads data from a complete or a part of a page.
*     This code is identical for main memory and spare area; the spare area
*     is located rigth after the main area.
*
*   Return value:
*     ==0     Data successfully transferred.
*     !=0     An error has occurred.
*/
static int _Read(U8 Unit, U32 PageNo, void * pData, unsigned Off, unsigned NumBytes) {
  PHY_INST * pInst;
  int r;

  pInst = &_aInst[Unit];
  ASSERT_PARA_IS_ALIGNED(pInst, NumBytes | Off | (U32)pData);
  FS_NAND_HW_X_EnableCE(Unit);
  //
  // Select the start address to read from
  //
  _WriteCmd(Unit, CMD_READ_1);
  _WriteAddrColRow(Unit, Off, pInst->NumColAddrBytes, PageNo, pInst->NumRowAddrBytes, pInst->DataBusWidth);
  //
  // Start the execution of read command and wait for it to finish
  //
  _WriteCmd(Unit, CMD_READ_2);
  r = _WaitBusy(Unit);
  if (r == 0) {
    //
    // The data to read is now in the data register of device; copy it to host memory
    //
    _WriteCmd(Unit, CMD_READ_1);     // Revert to read mode. _WaitBusy() change it to status mode
    _ReadData(Unit, pData, NumBytes, pInst->DataBusWidth);
  }
  FS_NAND_HW_X_DisableCE(Unit);
  if (r) {
    _Reset(Unit);
  }
  return r;
}

/*********************************************************************
*
*       _ReadEx
*
*   Function description
*     Reads data from two locations on a page.
*     Typically used to read data and spare area at once.
*
*   Return value:
*     ==0     Data successfully transferred.
*     !=0     An error has occurred.
*/
static int _ReadEx(U8 Unit, U32 PageNo, void * pData, unsigned Off, unsigned NumBytes, void * pSpare, unsigned OffSpare, unsigned NumBytesSpare) {
  PHY_INST * pInst;
  int r;

  pInst = &_aInst[Unit];
  ASSERT_PARA_IS_ALIGNED(pInst, NumBytes | Off | (U32)pSpare | OffSpare | NumBytesSpare);
  FS_NAND_HW_X_EnableCE(Unit);
  //
  // Select the start address of the first location to read from
  //
  _WriteCmd(Unit, CMD_READ_1);
  _WriteAddrColRow(Unit, Off, pInst->NumColAddrBytes, PageNo, pInst->NumRowAddrBytes, pInst->DataBusWidth);
  //
  // Start the execution of read command and wait for it to finish
  //
  _WriteCmd(Unit, CMD_READ_2);
  r = _WaitBusy(Unit);
  if (r == 0) {
    //
    // The data to read is now in the data register of device.
    // Copy the data from the first location to host memory
    //
    _WriteCmd(Unit, CMD_READ_1);     // Revert to read mode. _WaitBusy() change it to status mode
    _ReadData(Unit, pData, NumBytes, pInst->DataBusWidth);
    //
    // Select the start address of the second location to read from
    //
    _WriteCmd(Unit, CMD_RANDOM_READ_1);
    _WriteAddrCol(Unit, OffSpare, pInst->NumColAddrBytes, pInst->DataBusWidth);
    _WriteCmd(Unit, CMD_RANDOM_READ_2);
    //
    // Copy the data from the second location to host memory
    //
    _ReadData(Unit, pSpare, NumBytesSpare, pInst->DataBusWidth);
  }
  FS_NAND_HW_X_DisableCE(Unit);
  if (r) {
    _Reset(Unit);
  }
  return r;
}

/*********************************************************************
*
*       _Write
*
*   Function description
*     Writes data into a complete or a part of a page.
*     This code is identical for main memory and spare area; the spare area
*     is located rigth after the main area.
*
*   Return value:
*     ==0     Data successfully transferred.
*     !=0     An error has occurred.
*/
static int _Write(U8 Unit, U32 PageNo, const void * pData, unsigned Off, unsigned NumBytes) {
  PHY_INST * pInst;
  int r;

  pInst = &_aInst[Unit];
  ASSERT_PARA_IS_ALIGNED(pInst, NumBytes | Off | (U32)pData);
  FS_NAND_HW_X_EnableCE(Unit);
  //
  // Select the start address of the location to write to
  //
  _WriteCmd(Unit, CMD_WRITE_1);
  _WriteAddrColRow(Unit, Off, pInst->NumColAddrBytes, PageNo, pInst->NumRowAddrBytes, pInst->DataBusWidth);
  //
  // Load the data register of device with the data to write
  //
  _WriteData(Unit, pData, NumBytes, pInst->DataBusWidth);
  //
  // Execute the write command and wait for it to finish
  //
  _WriteCmd(Unit, CMD_WRITE_2);
  r = _WaitBusy(Unit);
  FS_NAND_HW_X_DisableCE(Unit);
  if (r) {
    _Reset(Unit);
  }
  return r;
}

/*********************************************************************
*
*       _WriteEx
*
*   Function description
*     Writes data to 2 parts of a page.
*     Typically used to write data and spare area at the same time.
*
*  Return value:
*     ==0     Data successfully transferred.
*     !=0     An error has occurred.
*/
static int _WriteEx(U8 Unit, U32 PageNo, const void * pData, unsigned Off, unsigned NumBytes, const void * pSpare, unsigned OffSpare, unsigned NumBytesSpare) {
  PHY_INST * pInst;
  int r;

  pInst = &_aInst[Unit];
  ASSERT_PARA_IS_ALIGNED(pInst, NumBytes | Off | (U32)pData | (U32)pSpare | OffSpare | NumBytesSpare);
  FS_NAND_HW_X_EnableCE(Unit);
  //
  // Select the start address of the first location to write to
  //
  _WriteCmd(Unit, CMD_WRITE_1);
  _WriteAddrColRow(Unit, Off, pInst->NumColAddrBytes, PageNo, pInst->NumRowAddrBytes, pInst->DataBusWidth);
  //
  // Load the data register of device with the first data to write
  //
  _WriteData(Unit, pData, NumBytes, pInst->DataBusWidth);
  //
  // Select the start address of the second location to write to
  //
  _WriteCmd(Unit, CMD_RANDOM_WRITE);
  _WriteAddrCol(Unit, OffSpare, pInst->NumColAddrBytes, pInst->DataBusWidth);
  //
  // Load the data register of device with the second data to write
  //
  _WriteData(Unit, pSpare, NumBytesSpare, pInst->DataBusWidth);
  //
  // Execute the write command and wait for it to finish
  //
  _WriteCmd(Unit, CMD_WRITE_2);
  r = _WaitBusy(Unit);
  FS_NAND_HW_X_DisableCE(Unit);
  if (r) {
    _Reset(Unit);
  }
  return r;
}

/*********************************************************************
*
*       _EraseBlock
*
*   Function description
*     Sets all the bytes in a block to 0xFF
*
*   Return value:
*     ==0     Data successfully transferred.
*     !=0     An error has occurred.
*/
static int _EraseBlock(U8 Unit, U32 BlockNo) {
  PHY_INST * pInst;
  int r;

  pInst = &_aInst[Unit];
  FS_NAND_HW_X_EnableCE(Unit);
  _WriteCmd(Unit, CMD_ERASE_1);
  _WriteAddrRow(Unit, BlockNo, pInst->NumRowAddrBytes);
  _WriteCmd(Unit, CMD_ERASE_2);
  r = _WaitBusy(Unit);
  FS_NAND_HW_X_DisableCE(Unit);
  if (r) {
    _Reset(Unit);
  }
  return r;
}

/*********************************************************************
*
*       _InitGetDeviceInfo
*
*  Function description
*    Initializes hardware layer, resets NAND flash and tries to identify the NAND flash.
*    If the NAND flash can be handled, Device Info is filled.
*
*  Return value:
*      0    O.K., device can be handled
*      1    Error: device can not be handled
*
*  Notes
*       (1) The first command to be issued after power-on is RESET (see [2])
*/
static int _InitGetDeviceInfo(U8 Unit, FS_NAND_DEVICE_INFO * pDevInfo) {
  U8 * pBuffer;
  int  r;

  r = 1;
  //
  // Initialize hardware and reset the device
  //
  FS_NAND_HW_X_Init_x8(Unit);
  _Reset(Unit);                   // Note 1
  FS_NAND_HW_X_EnableCE(Unit);
  if (FS__NAND_IsONFISupported(Unit)) {
    //
    // Device supports ONFI, go ahead and read parameters.
    // We use a temporarily sector buffer for reading.
    // A sector buffer is minimum 512 bytes, so large enough to hold all the parameters.
    //
    pBuffer = FS__AllocSectorBuffer();
    r       = FS__NAND_ReadONFIPara(Unit, pBuffer);
    if (r == 0) {
      U32 BytesPerPage;
      U32 PagesPerBlock;
      U32 NumBlocks;
      U16 Features;
      U8  NumAddrBytes;
      PHY_INST * pInst;

      //
      // Decode the ONFI parameters required by the driver.
      //
      Features      = FS_LoadU16LE(&pBuffer[6]);
      BytesPerPage  = FS_LoadU32LE(&pBuffer[80]);
      PagesPerBlock = FS_LoadU32LE(&pBuffer[92]);
      NumBlocks     = FS_LoadU32LE(&pBuffer[96]);
      NumAddrBytes  = pBuffer[101];
      //
      // Store the information needed by the physical layer at runtime.
      //
      pInst = &_aInst[Unit];
      if (Features & 1) {
        pInst->DataBusWidth = 16;
      } else {
        pInst->DataBusWidth = 8;
      }
      pInst->NumColAddrBytes = NumAddrBytes >> 4;
      pInst->NumRowAddrBytes = NumAddrBytes & 0x0F;
      //
      // Fill in the info needed by the NAND driver.
      //
      pDevInfo->BPP_Shift = (U8)_ld(BytesPerPage);
      pDevInfo->PPB_Shift = (U8)_ld(PagesPerBlock);
      pDevInfo->NumBlocks = (U16)NumBlocks;
    }
    FS__FreeSectorBuffer(pBuffer);
  }
  FS_NAND_HW_X_DisableCE(Unit);
  return r;
}

/*********************************************************************
*
*       _IsWP
*
*   Function description
*     Checks if the device is write protected.
*     This is done by reading bit 7 of the status register.
*     Typical reason for write protection is that either the supply voltage is too low
*     or the /WP-pin is active (low)
*
*   Return value:
*    ==0    Not write protected
*     >0    Write protected
*/
static int _IsWP(U8 Unit) {
  U8 Status;

  FS_NAND_HW_X_EnableCE(Unit);
  Status = _ReadStatus(Unit);
  FS_NAND_HW_X_DisableCE(Unit);
  return (Status & STATUS_WRITE_PROTECTED) ? 0 : 1;
}

/*********************************************************************
*
*       _EnableECC
*
*   Function description
*     Activates the internal ECC engine of NAND flash.
*
*   Return value
*     ==0     ECC compuation activated
*     !=0     An error occurred
*
*   Note
*       (1) A read-modify-write operation is required since more than one feature is stored in a parameter.
*/
static int _EnableECC(U8 Unit) {
  U8  aPara[4];
  int r;

  r = _GetFeatures(Unit, MICRON_ECC_FEATURE_ADDR, aPara);    // Note 1
  if (r == 0) {
    aPara[0] |= MICRON_ECC_FEATURE_MASK;
    r = _SetFeatures(Unit, MICRON_ECC_FEATURE_ADDR, aPara);
  }
  if (r) {
    _Reset(Unit);
  }
  return r;
}

/*********************************************************************
*
*       _DisableECC
*
*   Function description
*     Deactivates the internal ECC engine of NAND flash.
*
*   Return value
*     ==0     ECC compuation deactivated
*     !=0     An error occurred
*
*   Note
*       (1) A read-modify-write operation is required since more than one feature is stored in a parameter.
*/
static int _DisableECC(U8 Unit) {
  U8  aPara[4];
  int r;

  r = _GetFeatures(Unit, MICRON_ECC_FEATURE_ADDR, aPara);    // Note 1
  if (r == 0) {
    aPara[0] &= ~MICRON_ECC_FEATURE_MASK;
    r = _SetFeatures(Unit, MICRON_ECC_FEATURE_ADDR, aPara);
  }
  if (r) {
    _Reset(Unit);
  }
  return r;
}

/*********************************************************************
*
*       Public const
*
**********************************************************************
*/
const FS_NAND_PHY_TYPE FS_NAND_PHY_ONFI = {
  _EraseBlock,
  _InitGetDeviceInfo,
  _IsWP,
  _Read,
  _ReadEx,
  _Write,
  _WriteEx,
  _EnableECC,
  _DisableECC
};

/*************************** End of file ****************************/
