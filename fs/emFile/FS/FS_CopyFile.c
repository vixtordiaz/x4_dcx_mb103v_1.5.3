/*********************************************************************
*                SEGGER MICROCONTROLLER GmbH & Co. KG                *
*        Solutions for real time microcontroller applications        *
**********************************************************************
*                                                                    *
*        (c) 2003-2012     SEGGER Microcontroller GmbH & Co KG       *
*                                                                    *
*        Internet: www.segger.com    Support:  support@segger.com    *
*                                                                    *
**********************************************************************

**** emFile file system for embedded applications ****
emFile is protected by international copyright laws. Knowledge of the
source code may not be used to write a similar product. This file may
only be used in accordance with a license and should not be re-
distributed in any way. We appreciate your understanding and fairness.
----------------------------------------------------------------------
File        : FS_CopyFile.c
Purpose     : Implementation of FS_CopyFile
---------------------------END-OF-HEADER------------------------------
*/

/*********************************************************************
*
*             #include Section
*
**********************************************************************
*/

#include "FS_Int.h"

/*********************************************************************
*
*       Public code, internal
*
**********************************************************************
*/

/*********************************************************************
*
*       FS__CopyFileEx
*
*   Function description
*     Internal version of FS_CopyFileEx. Copies a file.
*
*  Parameters:
*     sSource     Name of the source file (fully qualified).
*     sDest       Name of the destination file (fully qualified).
*     pBuffer     Buffer to temporary store the copied data.
*     NumBytes    Size of the buffer size in bytes.
*
*   Return value
*     ==0     File has been copied.
*     ==-1    An error has occurred.
*/
int FS__CopyFileEx(const char * sSource, const char * sDest, void * pBuffer, U32 NumBytes) {
  int           r;
  FS_FILE     * pFileSrc;
  FS_FILE     * pFileDest;
  U32       NumBytesInFile;
  U32        TimeStamp;

  // 
  // Open source file.
  //
  pFileSrc  = FS__FOpenEx(sSource, FS_FILE_ACCESS_FLAG_R, 0, 0, 1);
  if ((void *)pFileSrc == NULL) {
    FS_DEBUG_ERROROUT((FS_MTYPE_API, "FS__CopyFile: Source file could not be opened.\n"));
    return -1;
  }
  // 
  // Open destination file.
  // 
  pFileDest = FS__FOpenEx(sDest, FS_FILE_ACCESS_FLAGS_CW, 1, 1, 0);
  if ((void *)pFileDest == NULL) {
    FS_DEBUG_ERROROUT((FS_MTYPE_API, "FS__CopyFile: Destination file could not be created.\n"));
    FS__FClose(pFileSrc);
    return -1;
  }
  r = -1;     // Set to indicate an error.
  //
  // Preallocate destination file to optimize copy.
  // 
  NumBytesInFile = FS__GetFileSize(pFileSrc);
  if (NumBytesInFile == 0) {
    r = 0;                    // OK, the source file is empty.
  } else {
    FS__FSeek(pFileDest, NumBytesInFile, FS_FILE_BEGIN);
    FS__SetEndOfFile(pFileDest);
    FS__FSeek(pFileDest, 0, FS_FILE_BEGIN);
    //
    // Now copy the data to the destination file.
    // 
    do {
      U32 NumBytesRead;
      U32 NumBytesWritten;

      NumBytesRead = FS__Read(pFileSrc, pBuffer, NumBytes);
      if (NumBytesRead == 0) {
        r = -1;               // Error, could not read from source file.
        break;
      }
      NumBytesWritten  = FS__Write(pFileDest, pBuffer, NumBytesRead);
      NumBytesInFile  -= NumBytesRead;
      if (NumBytesWritten != NumBytesRead) {
        r = -1;   // Not all bytes have been written, maybe the volume is full.
        break;
      }
      if (NumBytesInFile == 0) {
        r = 0;
        break;
      }
    } while (1);
  }
  //
  //  Close source and destination file
  // and update the directory entry for destination file.
  // 
  FS__FClose(pFileSrc);
  FS__FClose(pFileDest);
  //
  // Since we have copied the file, we need to set the attributes 
  // and timestamp of destination file to the same as source file.
  // 
  if (r == 0) {
    U8 Attrib;

    FS__GetFileTimeEx(sSource, &TimeStamp, FS_FILETIME_CREATE);
    FS__SetFileTimeEx(sDest,   TimeStamp, FS_FILETIME_CREATE);
    Attrib = FS__GetFileAttributes(sSource);
    FS__SetFileAttributes(sDest, Attrib);
  } else {
    //
    // Error occurred, delete the destination file.
    // 
    FS__Remove(sDest);
  }
  return r;
}

/*********************************************************************
*
*       FS__CopyFile
*
*   Function description:
*     Internal version of FS_CopyFile. Copies a file. It uses an internal temporary buffer.
*
*   Parameters:
*     sSource     Name of the source file (fully qualified).
*     sDest       Name of the destination file (fully qualified).
*
*   Return value
*     ==0     File has been copied.
*     ==-1    An error has occurred.
*/
int FS__CopyFile(const char * sSource, const char * sDest) {
  U8     aBuffer[512];
  void * pBuffer;
  I32    NumBytes;

  pBuffer  = aBuffer;
  NumBytes = sizeof(aBuffer);
  return FS__CopyFileEx(sSource, sDest, pBuffer, NumBytes);
}

/*********************************************************************
*
*       Public code
*
**********************************************************************
*/

/*********************************************************************
*
*       FS_CopyFileEx
*
*   Function description
*     Copies a file using a temporary buffer provided by the application.
*
*   Parameters
*     sSource     Name of the source file (fully qualified).
*     sDest       Name of the destination file (fully qualified).
*     pBuffer     Buffer to temporary store the copied data.
*     NumBytes    Size of the buffer size in bytes.
*
*  Return value:
*     ==0     File has been copied.
*     ==-1    An error has occurred.
*/
int FS_CopyFileEx(const char * sSource, const char * sDest, void * pBuffer, U32 NumBytes) {
  int r;

  FS_LOCK();
  r = FS__CopyFileEx(sSource, sDest, pBuffer, NumBytes);
  FS_UNLOCK();
  return r;
}


/*********************************************************************
*
*       FS_CopyFile
*
*   Function description
*     Copies a file using an internal temporary buffer.
*
*   Parameters
*     sSource     Name of the source file (fully qualified).
*     sDest       Name of the destination file (fully qualified).
*
*  Return value:
*     ==0     File has been copied.
*     ==-1    An error has occurred.
*/
int FS_CopyFile(const char * sSource, const char * sDest) {
  int r;

  FS_LOCK();
  r = FS__CopyFile(sSource, sDest);
  FS_UNLOCK();
  return r;
}

/*************************** End of file ****************************/
