/*********************************************************************
*                SEGGER MICROCONTROLLER GmbH & Co. KG                *
*        Solutions for real time microcontroller applications        *
**********************************************************************
*                                                                    *
*        (c) 2003-2012     SEGGER Microcontroller GmbH & Co KG       *
*                                                                    *
*        Internet: www.segger.com    Support:  support@segger.com    *
*                                                                    *
**********************************************************************

**** emFile file system for embedded applications ****
emFile is protected by international copyright laws. Knowledge of the
source code may not be used to write a similar product. This file may
only be used in accordance with a license and should not be re-
distributed in any way. We appreciate your understanding and fairness.
----------------------------------------------------------------------
File        : API_Dev.c
Purpose     : Device and media tools
---------------------------END-OF-HEADER------------------------------
*/

/*********************************************************************
*
*       #include Section
*
**********************************************************************
*/
#include "FS_ConfDefaults.h"        // File system configuration
#include "FS_CLib.h"
#include "FS_Lbl.h"
#include "FS_Int.h"

/*********************************************************************
*
*       Static code
*
**********************************************************************
*/

/*********************************************************************
*
*       _GetPartitionNumSectors
*
*  Description:
*     Function to get the number of sectors of the partition
*
*  Parameters:
*     PartIndex          - The partition index. Valid range is 0..3.
*                          Since this is an internal function, this parameter
*                          is not checked for validity
*     pBuffer            - Data buffer
*
*  Return value:
*     <  0xFFFFFFFF      - The number of sectors
*     == 0xFFFFFFFF      - Invalid partition specified
*/
static U32 _GetPartitionNumSectors(U8 PartIndex, U8 *pBuffer) {
  int Off;
  Off  = MBR_OFF_PARTITION0 + (PartIndex * PART_ENTRY_SIZE);
  Off += PART_ENTRY_OFF_NUM_SECTORS;
  return FS_LoadU32LE(&pBuffer[Off]);
}

/*********************************************************************
*
*       _GetPartitionStartSector
*
*  Description:
*     Function to retrieve the start sector of a given partition.
*
*  Parameters:
*     PartIndex          - The partition index. Valid range is 0..3.
*                          Since this is an internal function, this parameter
*                          is not checked for validity
*     pBuffer            - Data buffer
*
*  Return value:
*     <  0xFFFFFFFF      - The value of the start sector
*     == 0xFFFFFFFF      - Invalid partition specified
*/
static U32 _GetPartitionStartSector(U8 PartIndex, U8 *pBuffer) {
  int Off;
  Off  = MBR_OFF_PARTITION0 + (PartIndex * PART_ENTRY_SIZE);
  Off += PART_ENTRY_OFF_START_SECTOR;
  return FS_LoadU32LE(&pBuffer[Off]);
}

/*********************************************************************
*
*       _HasSignature
*
*/
static char _HasSignature(const U8 *pBuffer) {
  U16 Data;

  Data = FS_LoadU16LE(pBuffer + MBR_OFF_SIGNATURE);
  if (Data == 0xAA55) {
    return 1;
  }
  return 0;
}

/*********************************************************************
*
*       _IsBPB
*
*/
static char _IsBPB(const U8 *pBuffer) {
  /*
   *  Check if there is a x86 unconditional jmp instruction,
   *  which indicates that there is a BootParameterBlock
   */
  /* Check for the 1-byte relative jump with opcode 0xe9 */
  if (pBuffer[0] == 0xe9) {
    return 1;
  }
  /* Check for the 2-byte relative jump with opcode 0xeb */
  if ((pBuffer[0] == 0xeb) && (pBuffer[2] == 0x90)) {
    return 1;
  }
  return 0;
}

/*********************************************************************
*
*       _LocatePartition
*
*   Function description
*     Static helper for the FS__LocatePartition() function.
*
*   Return value
*     ==0   O.K.
*     !=0   An error occurred
*/
static signed char _LocatePartition(FS_VOLUME * pVolume, U8* pBuffer) {
  U32  StartSector;
  U32 NumSectors;

  NumSectors = 0;
  //
  // Calculate start sector of the first partition
  //
  StartSector = FS__GetMediaStartSecEx(pVolume, &NumSectors, pBuffer);
  if (StartSector == 0xFFFFFFFFUL) {  // Is MBR/BPB invalid ?
    return -1;                        // Error: Invalid MBR/BPB
  }
  pVolume->Partition.StartSector = StartSector;
  pVolume->Partition.NumSectors  = NumSectors;
  return 0;
}

/*********************************************************************
*
*       Public code, internal
*
**********************************************************************
*/

/*********************************************************************
*
*       FS__GetMediaStartSecEx
*
*  Description:
*    Get logical start sector from master boot record
*    or partition table.
*
*  Parameters:
*    pVolume         - pointer to volume.
*    pBuffer         - A pointer to a buffer containing the first
*                      sector of the media. This should contain the
*                      master boot record (MBR) or Bios Parameter Block
*                      (BPB) if the device has no partition table.
*
*  Return value:
*    < 0xFFFFFFF     - Number of the first sector of the medium.
*    ==0xFFFFFFF     - No valid MBR/BPB found.
*/
U32 FS__GetMediaStartSecEx(FS_VOLUME * pVolume, U32 * pNumSectors, U8 * pBuffer) {
  U32      StartSector;
  U32      NumSectors;
  FS_DEVICE * pDevice;

  if ((pVolume == NULL) || (pBuffer == NULL)) {
    return 0xFFFFFFFFUL;
  }
  NumSectors = 0;
  pDevice    = &pVolume->Partition.Device;
  if (FS_LB_ReadDevice(pDevice, 0, pBuffer, FS_SECTOR_TYPE_DATA) < 0) {
    return 0xFFFFFFFFUL;     /* Error: Sector read failed */
  }

  if (_HasSignature(pBuffer) == 0) {
    return 0xFFFFFFFFUL;     /* Error: This sector is neither MBR nor BPB. */
  }
  if (_IsBPB(pBuffer) == 0) {
    FS_DEV_INFO DevInfo;
    U32         NumTotalSectorsOfPartition;
    /* Seems to not be a valid BPB.
     * We now assume that it is a boot sector which contains a valid partition
     * table.
     */

    StartSector = _GetPartitionStartSector(0, pBuffer);
    NumSectors  = _GetPartitionNumSectors(0, pBuffer);
    FS_LB_GetDeviceInfo(pDevice, &DevInfo);
    if ((NumSectors == 0) || (StartSector == 0)) {
      return 0xFFFFFFFFUL;  /* Error, partition table entry 0 is not valid */
    }
    //
    // Allow a tolerance of 0.4% in order of having a larger partition
    // than are reported by device.
    //
    NumTotalSectorsOfPartition = ((StartSector + NumSectors) >> 8) / 255;
    if (NumTotalSectorsOfPartition > DevInfo.NumSectors) {
      FS_DEBUG_WARN((FS_MTYPE_API, "Warning: Partition 0's size is greater than the reported size by the volume"));
      return 0xFFFFFFFFUL;  /* Error, partition table entry 0 is out of bounds */
    }
  } else {
    StartSector = 0;
  }
  if (pNumSectors) {
    *pNumSectors = NumSectors;
  }
  return StartSector;
}

/*********************************************************************
*
*       FS__LocatePartition
*
*  Return value
*     ==0   O.K.
*    !=0  Error
*/
signed char FS__LocatePartition(FS_VOLUME * pVolume) {
  signed char r;
  U8 * pBuffer;
  U16  BytesPerSector;

  BytesPerSector = FS_GetSectorSize(&pVolume->Partition.Device);
  pBuffer = FS__AllocSectorBuffer();
  //
  // Check if the a sector fits into the sector buffer
  //
  if ((BytesPerSector > FS_Global.MaxSectorSize) || (BytesPerSector == 0)) {
    FS_DEBUG_ERROROUT((FS_MTYPE_API, "FS_LocatePartition: Invalid BytesPerSector value: %d.\n", BytesPerSector));
    r = -1;
  } else {
    r = _LocatePartition(pVolume, pBuffer);
  }
  FS__FreeSectorBuffer(pBuffer);
  return r;
}

/*********************************************************************
*
*       FS__LoadPartitionInfo
*
*   Function description
*     Returns information about a partition. Data is read from the pBuffer which stores the MBR.
*/
void FS__LoadPartitionInfo(U8 PartIndex, FS_PART_INFO * pPartInfo, const U8 * pBuffer) {
  unsigned   Off;
  const U8 * p;
  int        IsActive;
  int        IsValid;
  U8         Status;

  Off    = MBR_OFF_PARTITION0 + (PartIndex * PART_ENTRY_SIZE);
  p      = pBuffer + Off;
  Status = *p++;
  if        (Status == PART_ENTRY_STATUS_ACTIVE) {        // Bootable?
    IsActive = 1;
    IsValid  = 1;
  } else if (Status == PART_ENTRY_STATUS_INACTIVE) {      // Non-bootable?
    IsActive = 0;
    IsValid  = 1;
  } else {
    IsActive = 0;
    IsValid  = 0;
  }
  pPartInfo->IsValid          = (U8)IsValid;
  pPartInfo->IsActive         = (U8)IsActive;
  pPartInfo->StartHead        = (U8)*p++;
  pPartInfo->StartSector      = (U8)(*p & 0x3F);
  pPartInfo->StartCylinder    = (U16)((*p++ & 0xC0) << 2);
  pPartInfo->StartCylinder   += (U16)*p++;
  pPartInfo->FSType           = (U8)*p++;
  pPartInfo->EndHead          = (U8)*p++;
  pPartInfo->EndSector        = (U8)(*p & 0x3F);
  pPartInfo->EndCylinder      = (U16)((*p++ & 0xC0) << 2);
  pPartInfo->EndCylinder     += (U16)*p++;
  pPartInfo->PartStartSector  = FS_LoadU32LE(p);
  p += 4;
  pPartInfo->NumSectors4Part  = FS_LoadU32LE(p);
}

/*********************************************************************
*
*       FS__StorePartitionInfo
*
*   Function description
*     Stores a partition entry to provided buffer.
*/
void FS__StorePartitionInfo(U8 PartIndex, FS_PART_INFO * pPartInfo, U8 * pBuffer) {
  unsigned   Off;
  U8       * p;
  U8         Status;

  Off  = MBR_OFF_PARTITION0 + (PartIndex * PART_ENTRY_SIZE);
  p    = pBuffer + Off;
  if        (pPartInfo->IsValid == 0) {
    Status = PART_ENTRY_STATUS_INVALID;
  } else if (pPartInfo->IsActive) {
    Status = PART_ENTRY_STATUS_ACTIVE;
  } else {
    Status = PART_ENTRY_STATUS_INACTIVE;
  }
  *p++ = Status;
  *p++ = pPartInfo->StartHead;
  *p++ =  (pPartInfo->StartSector   & 0x003F)
       | ((pPartInfo->StartCylinder & 0x0300) >> 2);
  *p++ = pPartInfo->StartCylinder & 0x00FF;
  *p++ = pPartInfo->FSType;
  *p++ = pPartInfo->EndHead;
  *p++ =  (pPartInfo->EndSector   & 0x003f)
       | ((pPartInfo->EndCylinder & 0x0300) >> 2);
  *p++ = pPartInfo->EndCylinder & 0x00FF;
  FS_StoreU32LE(p, pPartInfo->PartStartSector);
  p += 4;
  FS_StoreU32LE(p, pPartInfo->NumSectors4Part);
}

/*********************************************************************
*
*       FS__CreateMBR
*
*   Function description
*     Helper for the FS_CreateMBR() function.
*
*   Return value
*     ==0   O.K.
*     !=0   An error occurred
*/
int FS__CreateMBR(FS_VOLUME * pVolume, FS_PART_INFO * pPartInfo, int NumPartitions) {
  FS_DEVICE * pDevice;
  U8        * pBuffer;
  int         iPart;
  int         r;
  U8        * p;

  r       = 1;            // Set to indicate an error.
  pBuffer = FS__AllocSectorBuffer();
  if (pBuffer) {
    FS_MEMSET(pBuffer, 0, FS_Global.MaxSectorSize);
    //
    // Store the partition entries.
    //
    for (iPart = 0; iPart < NumPartitions; ++iPart) {
      FS__StorePartitionInfo((U8)iPart, pPartInfo, pBuffer);
      ++pPartInfo;
    }
    //
    // Store the signature.
    //
    p = pBuffer + MBR_OFF_SIGNATURE;
    FS_StoreU16LE(p, MBR_SIGNATURE);
    //
    // Write the MBR sector to storage medium.
    //
    pDevice = &pVolume->Partition.Device;
    r = FS_LB_WriteDevice(pDevice, 0, pBuffer, FS_SECTOR_TYPE_DATA, 0);
    FS__FreeSectorBuffer(pBuffer);
  }
  return r;
}

/*********************************************************************
*
*       Public API
*
**********************************************************************
*/

/*********************************************************************
*
*       FS_CreateMBR
*
*   Function description
*     Writes the Master Boot Record to sector 0 of the storage medium.
*/
int FS_CreateMBR(const char * sVolumeName, FS_PART_INFO * pPartInfo, int NumPartitions) {
  int         r;
  FS_VOLUME * pVolume;

  FS_LOCK();
  r = 1;      // Set to indicate an error.
  pVolume = FS__FindVolume(sVolumeName, NULL);
  if (pVolume) {
    r = FS__CreateMBR(pVolume, pPartInfo, NumPartitions);
  }
  FS_UNLOCK();
  return r;
}

/*************************** End of file ****************************/

