/*********************************************************************
*                SEGGER MICROCONTROLLER GmbH & Co. KG                *
*        Solutions for real time microcontroller applications        *
**********************************************************************
*                                                                    *
*        (c) 2003-2012     SEGGER Microcontroller GmbH & Co KG       *
*                                                                    *
*        Internet: www.segger.com    Support:  support@segger.com    *
*                                                                    *
**********************************************************************

**** emFile file system for embedded applications ****
emFile is protected by international copyright laws. Knowledge of the
source code may not be used to write a similar product. This file may
only be used in accordance with a license and should not be re-
distributed in any way. We appreciate your understanding and fairness.
----------------------------------------------------------------------
File        : FS_CacheMultiWay.c
Purpose     : Cache module for the Logical Block Layer

Strategy:     Read/write cache, caching all sectors (management, directory and data) equally.
Asociativity: Multi-way, configurable.
Limitations:  None. This cache module can be used on any device with any file system.

The cache is subdivided in so called sets. Each set can store N sectors
where N is the configured associativity level. The asociativity level
is a power of 2 value (2, 4, 8...) The set number where a sector
must be stored is calculated using the formula:

  SetNo = SectorNo % NumSets

The repacement policy is base on a LRU (Least Recently Used) algorithm.
Each cache block stores an access count. The access count is set to 0 each time the corresponding sector is read/updated.
At the same time, the access counts of the other cache blocks in the set are incremented.
The cache block in a set with the greatest access count will be replaced.

---------------------------END-OF-HEADER------------------------------
*/

/*********************************************************************
*
*       #include Section
*
**********************************************************************
*/

#include "FS_ConfDefaults.h"
#include "FS_Int.h"

#if FS_SUPPORT_CACHE

#ifndef   FS_CACHE_MULTY_WAY_ASSOC_LEVEL
  #define FS_CACHE_MULTY_WAY_ASSOC_LEVEL  2     // Default associativity level. Runtime configurable.
#endif

/*********************************************************************
*
*       Defines, non-configurable
*
**********************************************************************
*/
#define SECTOR_NO_INVALID   0xFFFFFFFFuL
#define ACCESS_CNT_MAX      0xFFFF

/*********************************************************************
*
*       Static code
*
**********************************************************************
*/

/*********************************************************************
*
*       _ld
*/
static U16 _ld(U32 Value) {
  U16 i;
  for (i = 0; i < 16; i++) {
    if ((1UL << i) == Value) {
      break;
    }
  }
  return i;
}

/*********************************************************************
*
*       _SectorNoToSetNo
*
*   Function description
*     Computes the number of the set where a sector should be stored.
*/
static U32 _SectorNoToSetNo(CACHE_MULTI_WAY_DATA * pCacheData, U32 SectorNo) {
  U32 NumSets;

  NumSets = pCacheData->NumSets;
  return SectorNo % NumSets;
}

/*********************************************************************
*
*       _GetNumSectors
*
*   Function description
*     Computes the number of sectors which can be stored in the cache.
*/
static U32 _GetNumSectors(CACHE_MULTI_WAY_DATA * pCacheData) {
  U32 NumSets;
  U32 ldAssocLevel;
  U32 NumSectors;

  NumSets      = pCacheData->NumSets;
  ldAssocLevel = pCacheData->ldAssocLevel;
  NumSectors   = NumSets << ldAssocLevel;
  return NumSectors;
}

/*********************************************************************
*
*       _InvalidateCache
*
*   Function description
*     Marks als invalid all sectors in the cache.
*/
static void _InvalidateCache(CACHE_MULTI_WAY_DATA * pCacheData) {
  U32 SectorSize;
  U32 NumSectors;
  CACHE_MULTI_WAY_BLOCK_INFO * pBlockInfo;

  SectorSize   = pCacheData->SectorSize;
  NumSectors   = _GetNumSectors(pCacheData);
  //
  // Visit each cache block and invalidate the data.
  //
  if (NumSectors) {
    pBlockInfo = (CACHE_MULTI_WAY_BLOCK_INFO *)(pCacheData + 1);
    do {
      pBlockInfo->SectorNo  = SECTOR_NO_INVALID;
      pBlockInfo->AccessCnt = 0;
      pBlockInfo->IsDirty   = 0;
      pBlockInfo            = (CACHE_MULTI_WAY_BLOCK_INFO *)(((U8 *)(pBlockInfo + 1)) + SectorSize);
    } while (--NumSectors);
  }
}

/*********************************************************************
*
*       _UpdateNumSets
*
*   Function description
*     Computes the maximum number of sets which can be stored in the cache.
*     The value is saved to cache management data.
*
*   Return value
*     !=0     Number of sets which can be stored to cache
*     ==0     An error occurred
*/
static U32 _UpdateNumSets(FS_DEVICE * pDevice) {
  U32 NumSets;
  U32 NumSectors;
  U32 SectorSize;
  U32 NumBytes;
  U32 ldAssocLevel;
  U32 SizeofCacheData;
  U32 SizeofBlockInfo;
  CACHE_MULTI_WAY_DATA * pCacheData;

  pCacheData = (CACHE_MULTI_WAY_DATA *)pDevice->Data.pCacheData;
  //
  // First, check if we already computed the number of sets.
  //
  SectorSize = pCacheData->SectorSize;
  if (SectorSize) {
    return pCacheData->NumSets;   // OK, Number of sets already computed.
  }
  //
  // Ask the driver for the number of bytes in a sector.
  //
  SectorSize = FS_GetSectorSize(pDevice);
  if (SectorSize == 0) {
    return 0;                     // Error, medium removed.
  }
  //
  // Sanity check. The cache size should be great enough to store the cache management data.
  //
  NumBytes = pCacheData->NumBytesCache;
  if (NumBytes < sizeof(CACHE_MULTI_WAY_DATA)) {
    return 0;                     // Error, cache to small.
  }
  //
  // Compute number of sets the cache is able to store.
  //
  ldAssocLevel    = pCacheData->ldAssocLevel;
  SizeofCacheData = sizeof(CACHE_MULTI_WAY_DATA);
  SizeofBlockInfo = sizeof(CACHE_MULTI_WAY_BLOCK_INFO);
  NumSectors      = (NumBytes - SizeofCacheData) / (SizeofBlockInfo + SectorSize);
  NumSets         = NumSectors >> ldAssocLevel;
  if (NumSets > 0) {
    pCacheData->NumSets    = NumSets;
    pCacheData->SectorSize = SectorSize;
    _InvalidateCache(pCacheData);
  }
  return NumSets;
}

/*********************************************************************
*
*       _WriteIntoBlock
*
*   Function description
*     Modifies a cache block. Stores the sector data and the sector index.
*/
static void _WriteIntoBlock(CACHE_MULTI_WAY_BLOCK_INFO * pBlockInfo, U32 SectorNo, const void * pData, U32 SectorSize, U8 IsDirty) {
  pBlockInfo->IsDirty  = (U16)IsDirty;
  pBlockInfo->SectorNo = SectorNo;
  FS_MEMCPY(pBlockInfo + 1, pData, SectorSize);
}

/*********************************************************************
*
*       _CleanBlock
*
*   Function description
*     Writes the sector data of a cache block to medium.
*/
static int _CleanBlock(FS_DEVICE * pDevice, CACHE_MULTI_WAY_BLOCK_INFO * pBlockInfo) {
  int r;
  U32 SectorNo;

  SectorNo = pBlockInfo->SectorNo;
  FS_DEBUG_LOG((FS_MTYPE_CACHE, "Cleaning %s:%d: SectorNo: 0x%8x.\n", pDevice->pType->pfGetName(pDevice->Data.Unit), pDevice->Data.Unit, SectorNo));
  r = FS_LB_WriteBack(pDevice, SectorNo, pBlockInfo + 1);
  return r;
}

/*********************************************************************
*
*       _CleanBlockIfRequired
*
*   Function description
*     Writes the sector data of a cache block to medium if it is marked as dirty.
*/
static int _CleanBlockIfRequired(FS_DEVICE * pDevice, CACHE_MULTI_WAY_BLOCK_INFO * pBlockInfo) {
  int r;

  r = 0;
  if ((pBlockInfo->SectorNo != SECTOR_NO_INVALID) && pBlockInfo->IsDirty) {
    r = _CleanBlock(pDevice, pBlockInfo);
    if (r == 0) {
      pBlockInfo->IsDirty  = 0;
      pBlockInfo->SectorNo = SECTOR_NO_INVALID;
    }
  }
  return r;
}

/*********************************************************************
*
*       _FindBlockBySectorNo
*
*   Function description
*     Returns the cache block in a set where a given sector is stored.
*
*   Parameters
*     pCacheData    [IN]  Cache management data.
*                   [OUT] ---
*     SetNo         Number of the set where the sector is stored.
*     SectorNo      Number of the sector to look up for.
*
*   Return value
*     !=0     Pointer to the cache block where the sector is stored.
*     ==0     Sector is not stored in the set.
*/
static CACHE_MULTI_WAY_BLOCK_INFO * _FindBlockBySectorNo(CACHE_MULTI_WAY_DATA * pCacheData, U32 SetNo, U32 SectorNo) {
  U32 SizeofSet;
  U32 SectorSize;
  U32 NumWays;
  U32 ldAssocLevel;
  CACHE_MULTI_WAY_BLOCK_INFO * pBlockInfo;

  ldAssocLevel = pCacheData->ldAssocLevel;
  SectorSize   = pCacheData->SectorSize;
  //
  // Compute the position of the set in the cache.
  //
  SizeofSet    = (sizeof(CACHE_MULTI_WAY_BLOCK_INFO) + SectorSize) << ldAssocLevel;
  pBlockInfo   = (CACHE_MULTI_WAY_BLOCK_INFO *)((U8 *)(pCacheData + 1) + (SizeofSet * SetNo));
  //
  // Search for the block containing the given sector number.
  //
  NumWays = 1uL << ldAssocLevel;
  do {
    if (pBlockInfo->SectorNo == SectorNo) {
      return pBlockInfo;
    }
    pBlockInfo = (CACHE_MULTI_WAY_BLOCK_INFO *)((U8 *)(pBlockInfo + 1) + SectorSize);
  } while (--NumWays);
  return NULL;
}

/*********************************************************************
*
*       _GetBlockToDiscard
*
*   Function description
*     Returns the cache block which can be discarded from a set.
*
*   Parameters
*     pCacheData    [IN]  Cache management data.
*                   [OUT] ---
*     SetNo         Number of the set to search.
*
*   Return value
*     Pointer to the cache block to discard.
*/
static CACHE_MULTI_WAY_BLOCK_INFO * _GetBlockToDiscard(CACHE_MULTI_WAY_DATA * pCacheData, U32 SetNo) {
  U32 SizeofSet;
  U32 SectorSize;
  U32 NumWays;
  U32 ldAssocLevel;
  U16 AccessCntMax;
  U16 AccessCnt;
  U32 SectorNo;
  CACHE_MULTI_WAY_BLOCK_INFO * pBlockInfo;
  CACHE_MULTI_WAY_BLOCK_INFO * pBlockInfoLRU;

  ldAssocLevel = pCacheData->ldAssocLevel;
  SectorSize   = pCacheData->SectorSize;
  //
  // Compute the position of the set in the cache.
  //
  SizeofSet    = (sizeof(CACHE_MULTI_WAY_BLOCK_INFO) + SectorSize) << ldAssocLevel;
  pBlockInfo   = (CACHE_MULTI_WAY_BLOCK_INFO *)((U8 *)(pCacheData + 1) + (SizeofSet * SetNo));
  //
  // Search for the block containing the least recently used sector. The access count is used for this purpose.
  // The block with the highest access count stores the LRU sector.
  //
  NumWays       = 1uL << ldAssocLevel;
  pBlockInfoLRU = pBlockInfo;
  AccessCntMax  = 0;
  do {
    SectorNo  = pBlockInfo->SectorNo;
    if (SectorNo == SECTOR_NO_INVALID) {
      pBlockInfoLRU = pBlockInfo;
      break;
    }
    AccessCnt = pBlockInfo->AccessCnt;
    if (AccessCnt > AccessCntMax) {
      AccessCntMax  = AccessCnt;
      pBlockInfoLRU = pBlockInfo;
    }
    pBlockInfo = (CACHE_MULTI_WAY_BLOCK_INFO *)((U8 *)(pBlockInfo + 1) + SectorSize);
  } while (--NumWays);
  return pBlockInfoLRU;
}

/*********************************************************************
*
*       _UpdateBlockAccessCnt
*
*   Function description
*     It modifies the access count of all blocks in a set.
*     First, it sets the access count of the block storing the given sector to 0.
*     Then the access counts of other blocks in the set are incremented.
*     The LRU sector will have the highest access count.
*
*   Parameters
*     pCacheData    [IN]  Cache management data.
*                   [OUT] ---
*     SetNo         Number of the set to search.
*     SectorNoLRU   Number of LRU sector.
*/
static void _UpdateBlockAccessCnt(CACHE_MULTI_WAY_DATA * pCacheData, U32 SetNo, U32 SectorNoLRU) {
  U32 SizeofSet;
  U32 SectorSize;
  U32 NumWays;
  U32 ldAssocLevel;
  U16 AccessCnt;
  U32 SectorNo;
  CACHE_MULTI_WAY_BLOCK_INFO * pBlockInfo;

  ldAssocLevel = pCacheData->ldAssocLevel;
  SectorSize   = pCacheData->SectorSize;
  //
  // Compute the position of the set in the cache.
  //
  SizeofSet    = (sizeof(CACHE_MULTI_WAY_BLOCK_INFO) + SectorSize) << ldAssocLevel;
  pBlockInfo   = (CACHE_MULTI_WAY_BLOCK_INFO *)((U8 *)(pCacheData + 1) + (SizeofSet * SetNo));
  //
  // Search for the block containing the least recently used sector. The access count is used for this purpose.
  // The block with the highest access count stores the LRU sector.
  //
  NumWays = 1uL << ldAssocLevel;
  do {
    SectorNo  = pBlockInfo->SectorNo;
    if (SectorNo != SECTOR_NO_INVALID) {
      AccessCnt = pBlockInfo->AccessCnt;
      if (SectorNo == SectorNoLRU) {
        AccessCnt = 0;
      } else if (AccessCnt < ACCESS_CNT_MAX) {
        ++AccessCnt;
      }
      pBlockInfo->AccessCnt = AccessCnt;
    }
    pBlockInfo = (CACHE_MULTI_WAY_BLOCK_INFO *)((U8 *)(pBlockInfo + 1) + SectorSize);
  } while (--NumWays);
}

/*********************************************************************
*
*       Static code (callbacks)
*
**********************************************************************
*/

/*********************************************************************
*
*       _CacheMultiWay_ReadFromCache
*
*   Function description
*     Reads one sector from cache. If not found the function returns an error.
*
*   Return value
*     !=0     Sector not found
*     ==0     Sector found
*/
static char _CacheMultiWay_ReadFromCache(FS_DEVICE * pDevice, U32 SectorNo, void * pData, U8 SectorType) {
  U32 SectorSize;
  U32 NumSets;
  U32 SetNo;
  CACHE_MULTI_WAY_DATA       * pCacheData;
  CACHE_MULTI_WAY_BLOCK_INFO * pBlockInfo;

  FS_USE_PARA(SectorType);
  pCacheData  = (CACHE_MULTI_WAY_DATA *)pDevice->Data.pCacheData;
  NumSets     = _UpdateNumSets(pDevice);
  if (NumSets == 0) {
    return 1;                          // Error, storage device is not available.
  }
  SectorSize  = pCacheData->SectorSize;
  SetNo       = _SectorNoToSetNo(pCacheData, SectorNo);
  pBlockInfo  = _FindBlockBySectorNo(pCacheData, SetNo, SectorNo);
  if (pBlockInfo) {
    _UpdateBlockAccessCnt(pCacheData, SetNo, SectorNo);
    FS_MEMCPY(pData, pBlockInfo + 1, SectorSize);
    return 0;                         // OK, sector found.
  }
  return 1;                           // Error, sector not found.
}

/*********************************************************************
*
*       _CacheMultiWay_UpdateCache
*
*   Function description
*     Updates a sector in cache. Called after a READ operation to update the cache.
*
*   Return value
*     ==0     Sector updated
*     !=0     An error occurred
*/
static char _CacheMultiWay_UpdateCache(FS_DEVICE * pDevice, U32 SectorNo, const void * pData, U8 SectorType) {
  U32 SectorSize;
  int CacheMode;
  U32 NumSets;
  U32 SetNo;
  CACHE_MULTI_WAY_DATA       * pCacheData;
  CACHE_MULTI_WAY_BLOCK_INFO * pBlockInfo;

  pCacheData  = (CACHE_MULTI_WAY_DATA *)pDevice->Data.pCacheData;
  CacheMode   = pCacheData->aCacheMode[SectorType];
  NumSets     = _UpdateNumSets(pDevice);
  if (NumSets == 0) {
    return 1;                           // Error, device is not available.
  }
  if (CacheMode & FS_CACHE_MODE_R) {    // Is read cache active for this type of sector ?
    SectorSize  = pCacheData->SectorSize;
    SetNo       = _SectorNoToSetNo(pCacheData, SectorNo);
    pBlockInfo  = _FindBlockBySectorNo(pCacheData, SetNo, SectorNo);
    if (pBlockInfo == NULL) {
      //
      // If the sector is not in cache, find a block in the corresponding set where we can store it.
      //
      pBlockInfo = _GetBlockToDiscard(pCacheData, SetNo);
      //
      // If we replace an other, dirty sector, we need to write it out first.
      //
      if (pBlockInfo->SectorNo != SectorNo) {
        _CleanBlockIfRequired(pDevice, pBlockInfo);
      }
    }
    _WriteIntoBlock(pBlockInfo, SectorNo, pData, SectorSize, 0);
    _UpdateBlockAccessCnt(pCacheData, SetNo, SectorNo);
  }
  return 0;
}

/*********************************************************************
*
*       _CacheMultiWay_WriteIntoCache
*
*   Function description
*     Writes a sector into cache.
*
*   Return value
*     0    Not  in write cache, the physical write operation still needs to be performed.
*     1    Data in write cache, the physical write operation does not need to be performed.
*/
static char _CacheMultiWay_WriteIntoCache(FS_DEVICE * pDevice, U32 SectorNo, const void * pData, U8 SectorType) {
  U32  SectorSize;
  U32  NumSets;
  U32  SetNo;
  int  CacheMode;
  U8  WriteRequired;
  U8  IsDirty;
  CACHE_MULTI_WAY_DATA       * pCacheData;
  CACHE_MULTI_WAY_BLOCK_INFO * pBlockInfo;

  pCacheData = (CACHE_MULTI_WAY_DATA *)pDevice->Data.pCacheData;
  NumSets    = _UpdateNumSets(pDevice);
  if (NumSets == 0) {
    return 0;                               // Error, device is not available.
  }
  CacheMode     = pCacheData->aCacheMode[SectorType];
  SectorSize    = pCacheData->SectorSize;
  SetNo         = _SectorNoToSetNo(pCacheData, SectorNo);
  pBlockInfo    = _FindBlockBySectorNo(pCacheData, SetNo, SectorNo);
  IsDirty       = 0;
  WriteRequired = 0;
  if (CacheMode & FS_CACHE_MODE_W) {        // Write cache active for this type of sector ?
    WriteRequired = 1;
  } else if (pBlockInfo) {                  // Sector already in cache ?
    WriteRequired = 1;
  }
  if (WriteRequired) {
    if (pBlockInfo == NULL) {
      //
      // If the sector is not in cache, find a block in the corresponding set where we can store it.
      //
      pBlockInfo = _GetBlockToDiscard(pCacheData, SetNo);
      //
      // If we replace an other, dirty sector, we need to write it out.
      //
      if (pBlockInfo->SectorNo != SectorNo) {
        _CleanBlockIfRequired(pDevice, pBlockInfo);
      }
    }
    if (CacheMode & FS_CACHE_MODE_D) {      // Delayed write allowed cache on for this type of sector ?
      IsDirty = 1;
    }
    _WriteIntoBlock(pBlockInfo, SectorNo, pData, SectorSize, IsDirty);
    _UpdateBlockAccessCnt(pCacheData, SetNo, SectorNo);
  }
  if (IsDirty) {
    return 1;                               // Write is delayed (data in cache) and does not need to be performed.
  } else {
    return 0;                               // Write still needs to be performed.
  }
}

/*********************************************************************
*
*       _CacheMultiWay_InvalidateCache
*
*   Function description
*     Invalidates all the sectors in the cache.
*/
static void _CacheMultiWay_InvalidateCache(void * p) {
  CACHE_MULTI_WAY_DATA * pCacheData;

  pCacheData = (CACHE_MULTI_WAY_DATA *)p;
  _InvalidateCache(pCacheData);
  pCacheData->NumSets = 0;
  pCacheData->SectorSize = 0;
}

/*********************************************************************
*
*       _SetMode
*
*   Function description
*     Sets the cache strategy for the given type of sectors.
*/
static void _SetMode(FS_DEVICE * pDevice, CACHE_MODE * pCacheMode) {
  int i;
  CACHE_MULTI_WAY_DATA * pCacheData;

  pCacheData = (CACHE_MULTI_WAY_DATA *)pDevice->Data.pCacheData;
  for (i = 0; i < FS_SECTOR_TYPE_COUNT; i++) {
    int TypeMask;

    TypeMask = 1 << i;
    if (TypeMask & pCacheMode->TypeMask) {
      pCacheData->aCacheMode[i] = pCacheMode->ModeMask;
    }
  }
}

/*********************************************************************
*
*       _Clean
*
*   Function description
*     Writes out all dirty sectors from cache.
*/
static int _Clean(FS_DEVICE * pDevice) {
  U32 NumSectors;
  U32 SectorSize;
  CACHE_MULTI_WAY_DATA       * pCacheData;
  CACHE_MULTI_WAY_BLOCK_INFO * pBlockInfo;

  pCacheData = (CACHE_MULTI_WAY_DATA *)pDevice->Data.pCacheData;
  SectorSize = pCacheData->SectorSize;
  NumSectors = _GetNumSectors(pCacheData);
  if (NumSectors) {
    pBlockInfo = (CACHE_MULTI_WAY_BLOCK_INFO *)((U8 *)(pCacheData + 1));
    do {
      _CleanBlockIfRequired(pDevice, pBlockInfo);
      pBlockInfo = (CACHE_MULTI_WAY_BLOCK_INFO *)((U8 *)(pBlockInfo + 1) + SectorSize);
    } while (--NumSectors);
  }
  return 0;
}

/*********************************************************************
*
*       _SetAssocLevel
*
*   Function description
*     Modifies the associativity level of the cache.
*/
static int _SetAssocLevel(FS_DEVICE * pDevice, U32 AssocLevel) {
  int r;
  U32 NumSets;
  CACHE_MULTI_WAY_DATA * pCacheData;

  r = 0;
  pCacheData = (CACHE_MULTI_WAY_DATA *)pDevice->Data.pCacheData;
  pCacheData->ldAssocLevel = _ld(AssocLevel);
  pCacheData->SectorSize   = 0;   // Force the update of the number of sets.
  //
  // Update the number of sets in the cache management data.
  //
  NumSets = _UpdateNumSets(pDevice);
  if (NumSets == 0) {
    r = 1;                        // Error, the cache should be large enough to hold at least one set.
  }
  return r;
}

/*********************************************************************
*
*       _RemoveFromCache
*
*   Function description
*     Invalidates the data of a range of sectors in the cache.
*     This function does not write dirty data to medium. Data of dirty entries is discared.
*     Typ. called when files and directories are removed.
*/
static void _RemoveFromCache(FS_DEVICE * pDevice, U32 FirstSector, U32 NumSectors) {
  U32 NumSectorsInCache;
  U32 SectorNo;
  U32 SetNo;
  U32 LastSector;
  U32 SectorSize;
  CACHE_MULTI_WAY_DATA       * pCacheData;
  CACHE_MULTI_WAY_BLOCK_INFO * pBlockInfo;

  pCacheData        = (CACHE_MULTI_WAY_DATA *)pDevice->Data.pCacheData;
  NumSectorsInCache = _GetNumSectors(pCacheData);
  LastSector        = FirstSector + NumSectors - 1;
  SectorSize        = pCacheData->SectorSize;
  if (NumSectorsInCache) {
    //
    // Use the most efficient way to search for sectors in the cache.
    //
    if (NumSectors > NumSectorsInCache) {
      //
      // Loop through all sectors in the cache and remove the ones included in the given range.
      //
      pBlockInfo = (CACHE_MULTI_WAY_BLOCK_INFO *)(pCacheData + 1);
      do {
        SectorNo = pBlockInfo->SectorNo;
        if (SectorNo != SECTOR_NO_INVALID) {
          if ((SectorNo >= FirstSector) && (SectorNo <= LastSector)) {
            pBlockInfo->SectorNo  = SECTOR_NO_INVALID;
            pBlockInfo->AccessCnt = 0;
            pBlockInfo->IsDirty   = 0;
            FS_DEBUG_LOG((FS_MTYPE_CACHE, "Removing %s:%d: SectorNo: 0x%8x.\n", pDevice->pType->pfGetName(pDevice->Data.Unit), pDevice->Data.Unit, pBlockInfo->SectorNo));
          }
        }
        pBlockInfo = (CACHE_MULTI_WAY_BLOCK_INFO *)(((U8 *)(pBlockInfo + 1)) + SectorSize);
      } while (--NumSectorsInCache);
    } else {
      //
      // Take each sector form the range of sectors to be removed.
      //
      for (SectorNo = FirstSector; SectorNo <= LastSector; ++SectorNo) {
        SetNo      = _SectorNoToSetNo(pCacheData, SectorNo);
        pBlockInfo = _FindBlockBySectorNo(pCacheData, SetNo, SectorNo);
        if (pBlockInfo) {
          pBlockInfo->SectorNo  = SECTOR_NO_INVALID;
          pBlockInfo->IsDirty   = 0;
          pBlockInfo->AccessCnt = 0;
        }
      }
    }
  }
}

/*********************************************************************
*
*       _CacheMultiWay_Command
*
*   Function description
*     Executes a command on the cache.
*
*   Return value
*     ==0     Command executed
*     !=0     An error occurred
*/
static int _CacheMultiWay_Command(FS_DEVICE * pDevice, int Cmd, void * p) {
  int r;

  r = -1;
  switch (Cmd) {
  case FS_CMD_CACHE_CLEAN:
    r = _Clean(pDevice);
    break;
  case FS_CMD_CACHE_SET_MODE:
    _SetMode(pDevice, (CACHE_MODE *)p);
    r = 0;
    break;
  case FS_CMD_CACHE_INVALIDATE:
    _CacheMultiWay_InvalidateCache(p);
    r = 0;
    break;
  case FS_CMD_CACHE_SET_ASSOC_LEVEL:
    {
      U32 AssocLevel;

      AssocLevel = *(int *)p;
      r = _SetAssocLevel(pDevice, AssocLevel);
    }
    break;
  case FS_CMD_CACHE_GET_NUM_SECTORS:
    {
      U32   NumSectors;
      U32 * pNumSectors;
      CACHE_MULTI_WAY_DATA * pCacheData;

      pCacheData  = (CACHE_MULTI_WAY_DATA *)pDevice->Data.pCacheData;
      pNumSectors = (U32 *)p;
      if (pNumSectors) {
        NumSectors   = _GetNumSectors(pCacheData);
        *pNumSectors = NumSectors;
      }
      r = 0;
    }
    break;
  case FS_CMD_CACHE_FREE_SECTORS:
    {
      CACHE_FREE * pCacheFree;
      U32          FirstSector;
      U32          NumSectors;

      pCacheFree  = (CACHE_FREE *)p;
      FirstSector = pCacheFree->FirstSector;
      NumSectors  = pCacheFree->NumSectors;
      _RemoveFromCache(pDevice, FirstSector, NumSectors);
      r = 0;
    }
    break;
  case FS_CMD_CACHE_GET_TYPE:
    {
      FS_CACHE_TYPE * pCacheType;

      pCacheType  = (FS_CACHE_TYPE *)p;
      if (pCacheType) {
        *pCacheType = FS_CacheMultiWay_Init;
      }
      r = 0;
    }
    break;
  }
  return r;
}

/*********************************************************************
*
*       _CacheMultiWayAPI
*
*/
static const FS_CACHE_API _CacheMultiWayAPI = {
  _CacheMultiWay_ReadFromCache,
  _CacheMultiWay_UpdateCache,
  _CacheMultiWay_InvalidateCache,
  _CacheMultiWay_Command,
  _CacheMultiWay_WriteIntoCache
};

/*********************************************************************
*
*       _CacheMultiWay_Init
*
*   Function descriptionS
*     Initializes the cache.
*
*   Return value
*     Returns the number of cache blocks (Number of sectors that can be cached)
*/
U32 FS_CacheMultiWay_Init(FS_DEVICE * pDevice, void * pData, I32 NumBytes) {
  U32   NumSectors;
  U32   NumSets;
  U8  * pData8;
  U16   ldAssocLevel;
  FS_DEVICE_DATA        * pDevData;
  CACHE_MULTI_WAY_DATA  * pCacheData;

  NumSectors = 0;
  pDevData   = &pDevice->Data;
  pData8     = (U8 *)pData;
  //
  // Align pointer to a 32bit boundary
  //
  if ((U32)pData8 & 3) {
    NumBytes  -= (4 - (((U32)pData8) & 3));
    pData8    += (4 - (((U32)pData8) & 3));
  }
  //
  // If less memory is available as we need to hold the management structure, we leave everything as it is.
  // A cache module is then not attached to the device.
  //
  if ((U32)NumBytes < sizeof(CACHE_MULTI_WAY_DATA)) {
    return 0;               // Error, not enough memory to store the cache management data.
  }
  pCacheData = (CACHE_MULTI_WAY_DATA *)pData8;
  FS_MEMSET(pCacheData, 0, sizeof(CACHE_MULTI_WAY_DATA));
  ldAssocLevel              = _ld(FS_CACHE_MULTY_WAY_ASSOC_LEVEL);
  pDevData->pCacheAPI       = &_CacheMultiWayAPI;
  pDevData->pCacheData      = pCacheData;
  pCacheData->NumBytesCache = NumBytes;
  pCacheData->ldAssocLevel  = ldAssocLevel;
  NumSets                   = _UpdateNumSets(pDevice);
  NumSectors                = NumSets << ldAssocLevel;
  return NumSectors;
}
#else

void CacheMultiWay_c(void);
void CacheMultiWay_c(void) {}

#endif /* FS_SUPPORT_CACHE */

/*************************** End of file ****************************/
