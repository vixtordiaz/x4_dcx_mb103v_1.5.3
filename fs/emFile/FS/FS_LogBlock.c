/*********************************************************************
*                SEGGER MICROCONTROLLER GmbH & Co. KG                *
*        Solutions for real time microcontroller applications        *
**********************************************************************
*                                                                    *
*        (c) 2003-2012     SEGGER Microcontroller GmbH & Co KG       *
*                                                                    *
*        Internet: www.segger.com    Support:  support@segger.com    *
*                                                                    *
**********************************************************************

**** emFile file system for embedded applications ****
emFile is protected by international copyright laws. Knowledge of the
source code may not be used to write a similar product. This file may
only be used in accordance with a license and should not be re-
distributed in any way. We appreciate your understanding and fairness.
----------------------------------------------------------------------
File        : FS_LogBlock.c
Purpose     : Logical Block Layer
---------------------------END-OF-HEADER------------------------------
*/

/*********************************************************************
*
*       #include Section
*
**********************************************************************
*/

#include "FS_ConfDefaults.h"        /* FS Configuration */
#include "FS_Int.h"

/*********************************************************************
*
*       Macros
*
**********************************************************************
*/

#if FS_DEBUG_LEVEL >= FS_DEBUG_LEVEL_CHECK_PARA
  #define CALL_ONDEVICE_HOOK(pDevice, Operation, StartSector, NumSectors, Sectortype)                        \
    {                                                                                                        \
      if (pDevice->Data.pfOnDeviceActivityHook) {                                                            \
        (pDevice->Data.pfOnDeviceActivityHook)(pDevice, Operation, StartSector, NumSectors, Sectortype);     \
      }                                                                                                      \
    }
#else
  #define CALL_ONDEVICE_HOOK(pDevice, Operation, StartSector, NumSectors, Sectortype)
#endif

#if FS_DEBUG_LEVEL >= FS_DEBUG_LEVEL_CHECK_PARA
  #define INC_READ_SECTOR_CNT(NumSectors, Type)              \
    {                                                        \
      FS_STORAGE_Counters.ReadOperationCnt++;                \
      FS_STORAGE_Counters.ReadSectorCnt += NumSectors;       \
      if (Type == FS_SECTOR_TYPE_MAN) {                      \
        FS_STORAGE_Counters.ReadSectorCntMan += NumSectors;  \
      }                                                      \
      if (Type == FS_SECTOR_TYPE_DIR) {                      \
        FS_STORAGE_Counters.ReadSectorCntDir += NumSectors;  \
      }                                                      \
    }
  #define INC_WRITE_SECTOR_CNT(NumSectors, Type)             \
    {                                                        \
      FS_STORAGE_Counters.WriteOperationCnt++;               \
      FS_STORAGE_Counters.WriteSectorCnt += NumSectors;      \
      if (Type == FS_SECTOR_TYPE_MAN) {                      \
        FS_STORAGE_Counters.WriteSectorCntMan += NumSectors; \
      }                                                      \
      if (Type == FS_SECTOR_TYPE_DIR) {                      \
        FS_STORAGE_Counters.WriteSectorCntDir += NumSectors; \
      }                                                      \
    }
  #define INC_READ_CACHE_HIT_CNT()                  {FS_STORAGE_Counters.ReadSectorCachedCnt++;}
  #define INC_WRITE_CACHE_CLEAN_CNT()               {FS_STORAGE_Counters.WriteSectorCntCleaned++;}
#else
  #define INC_READ_SECTOR_CNT(NumSectors, Type)
  #define INC_WRITE_SECTOR_CNT(NumSectors, Type)
  #define INC_READ_CACHE_HIT_CNT()
  #define INC_WRITE_CACHE_CLEAN_CNT()
#endif

/*********************************************************************
*
*       Code & data for debug builds
*
**********************************************************************
*/
#if FS_DEBUG_LEVEL >= FS_DEBUG_LEVEL_LOG_ALL

typedef struct {
  int          Type;
  const char * s;
} TYPE_DESC;

static const TYPE_DESC _aDesc[] = {
  { FS_SECTOR_TYPE_DATA, "DATA" },
  { FS_SECTOR_TYPE_MAN,  "MAN " },
  { FS_SECTOR_TYPE_DIR,  "DIR " },
};

/*********************************************************************
*
*       _Type2Name
*
*/
// static   -- Code is static, but not declared as such in order to avoid compiler warnings if this function is not referenced (lower debug levels)
const char * _Type2Name(int Type);    // Avoid "No prototype" warning
const char * _Type2Name(int Type) {
  unsigned i;
  for (i = 0; i < COUNTOF(_aDesc); i++) {
    if (_aDesc[i].Type == Type) {
      return _aDesc[i].s;
    }
  }
  return "Unknown Type";
}

#else

#endif

/*********************************************************************
*
*       Static code
*
**********************************************************************
*/

#if FS_SUPPORT_BUSY_LED
  #define CLR_BUSY_LED(pDevice) _ClrBusyLED(pDevice)
  #define SET_BUSY_LED(pDevice) _SetBusyLED(pDevice)


/*********************************************************************
*
*       _ClrBusyLED
*
*  Function description
*    Calls the user supplied callback (hook) to switch off the BUSY-LED.
*/
static void _ClrBusyLED(FS_DEVICE * pDevice) {
  if (pDevice->Data.pfSetBusyLED) {
    pDevice->Data.pfSetBusyLED(0);
  }
}

/*********************************************************************
*
*       _SetBusyLED
*
*  Function description
*    Calls the user supplied callback (hook) to switch on the BUSY-LED.
*/
static void _SetBusyLED(FS_DEVICE * pDevice) {
  if (pDevice->Data.pfSetBusyLED) {
    pDevice->Data.pfSetBusyLED(1);
  }

}

#else
  #define CLR_BUSY_LED(pDevice)
  #define SET_BUSY_LED(pDevice)
#endif

/*********************************************************************
*
*       _ReadFromStorage
*
*  Function description
*     Helper function which reads sectors from storage medium. If the support for journaling is enabled the data is read from journal.
*
*  Return value
*     ==0     All sectors have been read sucessfully.
*     < 0     An error has occurred.
*/
static int _ReadFromStorage(FS_DEVICE * pDevice, U32 SectorNo, void * pBuffer, U32 NumSectors) {
  int r;
  U8  Unit;
  const FS_DEVICE_TYPE * pDeviceType;
  pDeviceType = pDevice->pType;
  Unit        = pDevice->Data.Unit;
#if FS_SUPPORT_JOURNAL
  if (pDevice->Data.JournalIsActive) {
    r = FS__JOURNAL_Read(pDevice, SectorNo, pBuffer, NumSectors);
  } else
#endif
  {
    r = (pDeviceType->pfRead)(Unit, SectorNo, pBuffer, NumSectors);
  }
  if (r) {
    FS_DEBUG_ERROROUT((FS_MTYPE_STORAGE, "LOGBLOCK: Failed to read sector(s): 0x%8x-0x%8x from \"%s:%d:\".\n", SectorNo, SectorNo + NumSectors - 1, pDevice->pType->pfGetName(Unit), Unit));
  }
  return r;
}

#if FS_VERIFY_WRITE

/*********************************************************************
*
*       _Verify
*
*   Function description
*     Reads the contents of the given sectors and compares them against the buffer.
*
*   Return value
*     ==0     OK, contents match.
*     !=0     Error, content mismatch.
*
*/
static int _Verify(FS_DEVICE * pDevice, U32 SectorNo, const void * pBuffer, U32 NumSectors, U8 RepeatSame) {
  U16 SectorSize;
  int         r;
  const U8  * p;
  static U8 * _pVerifyBuffer;
      
  //
  // If required, allocate memory to store one sector.
  //
  SectorSize = FS_GetSectorSize(pDevice);
  FS_AllocZeroedPtr((void **)&_pVerifyBuffer, SectorSize);
  //
  // Take one sector at a time and check its contents.
  //
  p = (U8 *)pBuffer;
  do {
    //
    // Read one sector.
    //
    r = _ReadFromStorage(pDevice, SectorNo, _pVerifyBuffer, 1);
    if (r) {
      return 1;               // Error, read failed.
    }
    r = FS_MEMCMP(p, (const void *)_pVerifyBuffer, SectorSize);
    if (r) {
      FS_DEBUG_ERROROUT((FS_MTYPE_STORAGE, "LOGBLOCK: Verify failed at sector 0x%8x on \"%s:%d:\".\n", SectorNo, pDevice->pType->pfGetName(pDevice->Data.Unit), pDevice->Data.Unit));
      return 1;               // Error, content of sectors differs.
    }
    if (RepeatSame == 0) {
      p += SectorSize;
    }
    ++SectorNo;
  } while (--NumSectors);
  return 0;
}

#endif // FS_VERIFY_WRITE

/*********************************************************************
*
*       _WriteToStorage
*
*  Function description
*     Write sectors to storage medium. If support for journaling is enabled the data is written to journal first.
*
*   Parameters
*     pDevice         [IN]  Device to write to
*                     [OUT] ---
*     SectorNo        Index of the sector to write to
*     pBuffer         [IN]  Data to write
*                     [OUT] ---  
*     NumSectors      Number of sectors to write
*     RepeatSame      Set to 1 if same data should be written in all sectors
*     WriteToJournal  Set to 1 if data should go through journal
*
*  Return value
*     ==0     All sectors have been written successfully.
*     < 0     An error has occurred.
*/
static int _WriteToStorage(FS_DEVICE * pDevice, U32 SectorNo, const void * pBuffer, U32 NumSectors, U8 RepeatSame, U8 WriteToJournal) {
  int r;
  U8  Unit;
  const FS_DEVICE_TYPE * pDeviceType;

  FS_USE_PARA(WriteToJournal);
  pDeviceType = pDevice->pType;
  Unit        = pDevice->Data.Unit;
#if FS_SUPPORT_JOURNAL
  if (pDevice->Data.JournalIsActive && (WriteToJournal == 0)) {
    int OpenCnt;

    //
    // The optimization which avoids writing to journal should be disabled if the application calls
    // FS_JOURNAL_Begin()/FS_JOURNAL_End() directly. This is required in order to avoid the corruption of the storage.
    // Corruption can take place if a file is deleted and another one is created by appending data to it.
    // If the optimization is active the data of the deleted file might be overwritten by the created file.
    // An unexpected reset that occurs before the journal is replayed, will restore the deleted file
    // leaving the file system in an inconsistent state.
    //
    OpenCnt = FS__JOURNAL_GetOpenCnt(pDevice);
    if (OpenCnt > 1) {          // Application started a journaling operation?
      WriteToJournal = 1;
    }
  }
  if (pDevice->Data.JournalIsActive && WriteToJournal) {
    r = FS__JOURNAL_Write(pDevice, SectorNo, pBuffer, NumSectors, RepeatSame);
  } else
#endif
  {
    r = (pDeviceType->pfWrite)(Unit, SectorNo, pBuffer, NumSectors, RepeatSame);
  }
  if (r) {
    FS_DEBUG_ERROROUT((FS_MTYPE_STORAGE, "LOGBLOCK: Failed to write sector(s): 0x%8x-0x%8x to \"%s:%d:\".\n", SectorNo, SectorNo + NumSectors - 1, pDevice->pType->pfGetName(Unit), Unit));
  }
#if FS_VERIFY_WRITE
  if (r == 0) {
    r = _Verify(pDevice, SectorNo, pBuffer, NumSectors, RepeatSame);
  }
#endif
  return r;
}

#if FS_SUPPORT_CACHE

/*********************************************************************
*
*       _UpdateCache
*
*   Function description
*     Writes the contents of the sectors to cache.
*/
static void _UpdateCache(FS_DEVICE * pDevice, U32 SectorNo, const void * pBuffer, U32 NumSectors, U8 RepeatSame, U8 Type) {
  int        r;
  const U8 * p;
  U16        SectorSize;
  const FS_CACHE_API * pCacheAPI;

  pCacheAPI = pDevice->Data.pCacheAPI;
  if (pCacheAPI) {
    SectorSize = FS_GetSectorSize(pDevice);
    p          = (U8 *)pBuffer;
    do {
      r = pCacheAPI->pfUpdateCache(pDevice, SectorNo, p, Type);
      if (r) {
        FS_DEBUG_WARN((FS_MTYPE_STORAGE, "Could not update sector 0x%8x in cache.\n", SectorNo));
      }
      SectorNo++;
      if (RepeatSame == 0) {
        p += SectorSize;
      }
    } while (--NumSectors);
  }
}

/*********************************************************************
*
*       _ReadThroughCache
*
*   Function description
*     Reads from storage when the sectors are not in cache. The cache is updated with the new data.
*/
static int _ReadThroughCache(FS_DEVICE * pDevice, U32 SectorNo, void * pBuffer, U32 NumSectors, U8 Type) {
  int r;
  const FS_CACHE_API * pCacheAPI;
  U32                  NumSectorsToRead;
  U32                  FirstSector;
  U16                  SectorSize;
  int                  NeedReadBurst;
  void               * pReadBuffer;
  U8                 * p;

  NeedReadBurst    = 0;
  NumSectorsToRead = 0;
  FirstSector      = 0;
  pReadBuffer      = NULL;
  pCacheAPI        = pDevice->Data.pCacheAPI;
  SectorSize       = FS_GetSectorSize(pDevice);
  if (pCacheAPI) {
    p = (U8 *)pBuffer;
    do {
      r = (pCacheAPI->pfReadFromCache)(pDevice, SectorNo, p, Type);
      if (r) {
        //
        // Cache miss. We need to read from hardware. Since we try to use burst mode, we do not read immediately.
        //
        if (NeedReadBurst) {
          NumSectorsToRead++;
        } else {
          FirstSector      = SectorNo;
          pReadBuffer      = p;
          NumSectorsToRead = 1;
          NeedReadBurst    = 1;
        }
      } else {
        INC_READ_CACHE_HIT_CNT();       // For statistics / debugging only
        if (NeedReadBurst) {
          NeedReadBurst = 0;
          r = _ReadFromStorage(pDevice, FirstSector, pReadBuffer, NumSectorsToRead);
          if (r) {
            break;                      // Error, read failure. We end the operation here.
          }
          _UpdateCache(pDevice, FirstSector, pReadBuffer, NumSectorsToRead, 0, Type);
  }
      }
      p += SectorSize;
      SectorNo++;
    } while(--NumSectors);
    //
    // End of read routine reached. There may be a hardware "read burst" operation pending, which needs to be executed in this case.
    //
    if (NeedReadBurst) {
      r = _ReadFromStorage(pDevice, FirstSector, pReadBuffer, NumSectorsToRead);
      if (r == 0) {
        _UpdateCache(pDevice, FirstSector, pReadBuffer, NumSectorsToRead, 0, Type);
      }
    }
  } else {
    r = _ReadFromStorage(pDevice, SectorNo, pBuffer, NumSectors);
  }
  return r;
}

/*********************************************************************
*
*       _WriteThroughCache
*
*   Function description
*     Write sectors to cache and to storage medium if required.
*/
static int _WriteThroughCache(FS_DEVICE * pDevice, U32 SectorNo, const void * pBuffer, U32 NumSectors, U8 RepeatSame, U8 Type, U8 WriteToJournal) {
  int r;
  const FS_CACHE_API * pCacheAPI;
  int                  WriteRequired;
  U16                  SectorSize;
  U8                 * p;
  char                 IsWritten;
  U32                  NumSectorsToCache;
  U32                  SectorNoToCache;

  r             = 0;
  WriteRequired = 1;
  pCacheAPI     = pDevice->Data.pCacheAPI;
  SectorSize    = FS_GetSectorSize(pDevice);
  pCacheAPI     = pDevice->Data.pCacheAPI;
  if (pCacheAPI) {        // Cache is configured ?
    p                 = (U8 *)pBuffer;
    NumSectorsToCache = NumSectors;
    SectorNoToCache   = SectorNo;
    do {
      IsWritten = (pCacheAPI->pfWriteIntoCache)(pDevice, SectorNoToCache, p, Type);
      if (IsWritten) {
        WriteRequired = 0;
      }
      if (RepeatSame == 0) {
        p += SectorSize;
      }
      ++SectorNoToCache;
    } while (--NumSectorsToCache);
  }
  //
  // Write to storage medium if required.
  //
  if (WriteRequired) {
    r = _WriteToStorage(pDevice, SectorNo, pBuffer, NumSectors, RepeatSame, WriteToJournal);
  }
  return r;
}

#endif // FS_SUPPORT_CACHE

/*********************************************************************
*
*       Public code
*
*  These functions should not be called by user application!
*
**********************************************************************
*/

/*********************************************************************
*
*       FS_LB_GetStatus
*
*  Function description
*    FS internal function. Get status of a device.
*
*  Parameters
*    pDevice     - Pointer to a device driver structure.
*
*  Return value
*    FS_MEDIA_STATE_UNKNOWN  if the state of the media is unknown.
*    FS_MEDIA_NOT_PRESENT    if media is not present.
*    FS_MEDIA_IS_PRESENT     if media is     present.
*
*/
int FS_LB_GetStatus(FS_DEVICE * pDevice) {
  int r;
  const FS_DEVICE_TYPE * pDeviceType;

  pDeviceType = pDevice->pType;
  r = (pDeviceType->pfGetStatus)(pDevice->Data.Unit);
  return r;
}

/*********************************************************************
*
*       FS_LB_InitMedium
*
*  Function description
*    This function calls the initialize routine of the driver, if one exists.
*    If there if no initialization routine available, we assume the driver is
*    handling this automatically.
*
*  Parameters
*    pDevice     - Pointer to a device driver structure.
*
*  Return value
*    1           - Device/medium has been initialized.
*    0           - Error, device/medium could not be initialized.
*/
int FS_LB_InitMedium(FS_DEVICE * pDevice) {
  int IsInited;
  const FS_DEVICE_TYPE * pDeviceType;

  IsInited = 0;
  pDeviceType = pDevice->pType;
  if (pDeviceType->pfInitMedium) {
    if ((pDeviceType->pfInitMedium)(pDevice->Data.Unit) == 0) {
      IsInited = 1;
    }
  } else {
    IsInited = 1;
  }
  pDevice->Data.IsInited = (U8)IsInited;
  return IsInited;
}

/*********************************************************************
*
*       FS_LB_InitMediumIfRequired
*
*  Function description
*    Initialize medium if it has not already been initialized.
*
*  Parameters
*    pDevice     - Pointer to a device driver structure.
*
*/
int FS_LB_InitMediumIfRequired(FS_DEVICE * pDevice) {
  if (pDevice->Data.IsInited == 0) {
    FS_LB_InitMedium(pDevice);
  }
  return pDevice->Data.IsInited;
}


/*********************************************************************
*
*       FS_LB_ReadDevice
*
*  Function description
*    Read sector from device. It also checks whether the sector can be
*    read from the cache if available.
*
*  Parameters
*    pDriver     - Pointer to a device driver structure.
*    SectorNo     Index of the logical sector to be read from the device
*    pBuffer     - Pointer to buffer for storing the data.
*    Type        - The type of sector that shall be read.
*
*  Return value
*    ==0         - Sector has been read and copied to pBuffer.
*    < 0         - An error has occurred.
*/
int FS_LB_ReadDevice(FS_DEVICE *pDevice, U32 SectorNo, void * pBuffer, U8 Type) {
  int r;

  FS_LB_InitMediumIfRequired(pDevice);
  INC_READ_SECTOR_CNT(1, Type);             // For statistics / debugging only
  FS_DEBUG_LOG((FS_MTYPE_STORAGE, "Read          %s:%d: %s SectorNo: 0x%8x.\n", pDevice->pType->pfGetName(pDevice->Data.Unit), pDevice->Data.Unit, _Type2Name(Type), SectorNo));
  CALL_ONDEVICE_HOOK(pDevice, FS_OPERATION_READ, SectorNo, 1, Type);
  SET_BUSY_LED(pDevice);
#if FS_SUPPORT_CACHE
  r = _ReadThroughCache(pDevice, SectorNo, pBuffer, 1, Type);
#else
  FS_USE_PARA(Type);
  r = _ReadFromStorage(pDevice, SectorNo, pBuffer, 1);
#endif
  CLR_BUSY_LED(pDevice);
  return r;
}

/*********************************************************************
*
*       FS_LB_ReadPart
*
*  Function description
*    Read sector from volume.
*
*  Parameters
*    pDriver     - Pointer to a device driver structure.
*    Sector      - Physical sector to be read from the partition.
*                  The partition start sector is added.
*    pBuffer     - Pointer to buffer for storing the data.
*    Type        - The type of sector that shall be read.
*
*  Return value
*    ==0         - Sector has been read and copied to pBuffer.
*    <0          - An error has occurred.
*/
int FS_LB_ReadPart(FS_PARTITION *pPart, U32 Sector, void *pBuffer, U8 Type) {
  return FS_LB_ReadDevice(&pPart->Device, pPart->StartSector + Sector, pBuffer, Type);
}

/*********************************************************************
*
*       FS_LB_ReadBurst
*
*  Function description
*    Read multiple sectors from device.
*
*  Parameters
*    pDriver     - Pointer to a device driver structure.
*    Unit        - Unit number.
*    SectorNo    - First sector to be read from the device.
*    NumSectors  - Number of sectors to be read from the device.
*    pBuffer     - Pointer to buffer for storing the data.
*    Type        - The type of sector that shall be read.
*
*  Return value
*    ==0         - Sectors have been read and copied to pBuffer.
*    <0          - An error has occurred.
*/
int FS_LB_ReadBurst(FS_DEVICE * pDevice, U32 SectorNo, U32 NumSectors, void * pBuffer, U8 Type) {
  int r;

  r = 0;
  FS_LB_InitMediumIfRequired(pDevice);
  INC_READ_SECTOR_CNT(NumSectors, Type);             // For statistics / debugging only
  FS_DEBUG_LOG((FS_MTYPE_STORAGE, "ReadBurst     %s:%d: %s SectorNo: 0x%8x, NumSectors: %d.\n", pDevice->pType->pfGetName(pDevice->Data.Unit), pDevice->Data.Unit, _Type2Name(Type), SectorNo, NumSectors));
  SET_BUSY_LED(pDevice);
#if FS_SUPPORT_CACHE
  r = _ReadThroughCache(pDevice, SectorNo, pBuffer, NumSectors, Type);
#else
  FS_USE_PARA(Type);
  r = _ReadFromStorage(pDevice, SectorNo, pBuffer, NumSectors);
#endif
  CLR_BUSY_LED(pDevice);
  return r;
}

/*********************************************************************
*
*       FS_LB_ReadBurstPart
*
*  Function description
*    Read multiple sectors from device.
*
*  Parameters
*    pPart       - Pointer to a partition structure.
*    SectorNo    - First sector to be read from the device.
*    NumSectors  - Number of sectors to be read from the device.
*    pBuffer     - Pointer to buffer for storing the data.
*    Type        - The type of sector that shall be read.
*
*  Return value
*    ==0         - Sectors have been read and copied to pBuffer.
*    <0          - An error has occurred.
*/
int FS_LB_ReadBurstPart(FS_PARTITION *pPart, U32 SectorNo, U32 NumSectors, void *pBuffer, U8 Type) {
  FS_DEVICE      * pDevice;

  pDevice     = &pPart->Device;
  SectorNo   += pPart->StartSector;
  return  FS_LB_ReadBurst(pDevice, SectorNo, NumSectors, pBuffer, Type);
}

/*********************************************************************
*
*       FS_LB_WriteBurst
*
*  Function description
*    Write multiple sectors to device.
*
*  Parameters
*     pDevice         [IN]  Pointer to a device driver structure.
*                     [OUT] ---
*     SectorNo        First sector to be written to the device.
*     NumSectors      Number of sectors to be written.
*     pBuffer         [IN]  Pointer to buffer for holding the data.
*                     [OUT] ---
*     Type            The type of sector that shall be written (management, data).
*     WriteToJournal  Set to 1 if data should go through journal
*
*  Return value
*     ==0       Sectors have been read and copied to pBuffer.
*     <0        An error has occurred.
*/
int FS_LB_WriteBurst(FS_DEVICE * pDevice, U32 SectorNo, U32 NumSectors, const void *pBuffer, U8 Type, U8 WriteToJournal) {
  int r;

  FS_USE_PARA(Type);
  FS_LB_InitMediumIfRequired(pDevice);
  INC_WRITE_SECTOR_CNT(NumSectors, Type);       // For statistics / debugging only
  CALL_ONDEVICE_HOOK(pDevice, FS_OPERATION_WRITE, SectorNo, NumSectors, Type);
  FS_DEBUG_LOG((FS_MTYPE_STORAGE, "WriteBurst    %s:%d: %s SectorNo: 0x%8x, NumSectors: %d.\n", pDevice->pType->pfGetName(pDevice->Data.Unit), pDevice->Data.Unit, _Type2Name(Type), SectorNo, NumSectors));
  SET_BUSY_LED(pDevice);
#if FS_SUPPORT_CACHE
  r = _WriteThroughCache(pDevice, SectorNo, pBuffer, NumSectors, 0, Type, WriteToJournal);
#else
  r = _WriteToStorage(pDevice, SectorNo, pBuffer, NumSectors, 0, WriteToJournal);
#endif
  CLR_BUSY_LED(pDevice);
  return r;
}

/*********************************************************************
*
*       FS_LB_WriteBurstPart
*
*  Function description
*    Write multiple sectors to device.
*
*  Parameters
*     pPart           [IN]  Pointer to the partition structure.
*                     [OUT] ---
*     SectorNo        First sector to be written to the device.
*     NumSectors      Number of sectors to be written.
*     pBuffer         [IN]  Pointer to buffer for holding the data.
*                     [OUT] ---
*     Type            The type of sector that shall be written.
*     WriteToJournal  Set to 1 if data should go through journal
*
*  Return value
*     ==0     Sectors have been read and copied to pBuffer.
*     <0      An error has occurred.
*/
int FS_LB_WriteBurstPart(FS_PARTITION * pPart, U32 SectorNo, U32 NumSectors, const void * pBuffer, U8 Type, U8 WriteToJournal) {
  FS_DEVICE * pDevice;

  FS_USE_PARA(Type);
  pDevice     = &pPart->Device;
  SectorNo   += pPart->StartSector;
  return  FS_LB_WriteBurst(pDevice, SectorNo, NumSectors, pBuffer, Type, WriteToJournal);
}

/*********************************************************************
*
*       FS_LB_WriteMultiple
*
*  Function description
*    Write multiple sectors to device.
*
*  Parameters
*     pDevice         [IN]  Pointer to a device driver structure.
*                     [OUT] ---
*     SectorNo        First sector to be written to the device.
*     NumSectors      Number of sectors to be written.
*     pBuffer         [IN]  Pointer to buffer for holding the data.
*                     [OUT] ---
*     Type            The type of sector that shall be written (management, data).
*     WriteToJournal  Set to 1 if data should go through journal
*
*  Return value
*     ==0     Sectors have been read and copied to pBuffer.
*     <0      An error has occurred.
*/
int FS_LB_WriteMultiple(FS_DEVICE * pDevice, U32 SectorNo, U32 NumSectors, const void *pBuffer, U8 Type, U8 WriteToJournal) {
  int r;

  FS_USE_PARA(Type);
  FS_LB_InitMediumIfRequired(pDevice);
  INC_WRITE_SECTOR_CNT(NumSectors, Type);       // For statistics / debugging only
  FS_DEBUG_LOG((FS_MTYPE_STORAGE, "WriteMultiple %s:%d: %s SectorNo: 0x%8x, NumSectors: %d.\n", pDevice->pType->pfGetName(pDevice->Data.Unit), pDevice->Data.Unit, _Type2Name(Type), SectorNo, NumSectors));
  CALL_ONDEVICE_HOOK(pDevice, FS_OPERATION_WRITE, SectorNo, NumSectors, Type);
  SET_BUSY_LED(pDevice);
#if FS_SUPPORT_CACHE
  r = _WriteThroughCache(pDevice, SectorNo, pBuffer, NumSectors, 1, Type, WriteToJournal);
#else
  r = _WriteToStorage(pDevice, SectorNo, pBuffer, NumSectors, 1, WriteToJournal);
#endif
  CLR_BUSY_LED(pDevice);
  return r;
}

/*********************************************************************
*
*       FS_LB_WriteMultiplePart
*
*  Function description
*     Write more than one sector to a file system partition.
*
*  Parameters
*     pPart           [IN]  Partition to write to
*                     [OUT] ---
*     SectorNo        First sector to be written to partition. 
*     NumSectors      Number of sectors to be written.
*     pBuffer         [IN]  Buffer for holding the sector data.
*                     [OUT] ---
*     Type            The type of sector that shall be written.
*     WriteToJournal  Set to 1 if data should go through journal
*
*  Return value
*     ==0     Sectors have been read and copied to pBuffer.
*     <0      An error has occurred.
*/
int FS_LB_WriteMultiplePart(FS_PARTITION * pPart, U32 SectorNo, U32 NumSectors, const void * pBuffer, U8 Type, U8 WriteToJournal) {
  FS_DEVICE * pDevice;

  FS_USE_PARA(Type);
  pDevice     = &pPart->Device;
  SectorNo   += pPart->StartSector;
  return  FS_LB_WriteMultiple(pDevice, SectorNo, NumSectors, pBuffer, Type, WriteToJournal);
}

/*********************************************************************
*
*       FS_LB_WriteDevice
*
*  Function description
*    FS internal function. Write sector to device.
*
*  Parameters
*     pDriver         [IN]  Pointer to a device driver structure.
*                     [OUT] ---
*     SectorNo        First sector to be written to the device.
*     pBuffer         [IN]  Pointer to buffer for holding the data.
*                     [OUT] ---
*     Type            The type of sector that shall be written (management, data).
*     WriteToJournal  Set to 1 if data should go through journal
*
*  Return value
*     == 0      Sector has been written to the device.
*     <  0      An error has occurred.
*/
int FS_LB_WriteDevice(FS_DEVICE * pDevice, U32 SectorNo, const void * pBuffer, U8 Type, U8 WriteToJournal) {
  int r;

  FS_USE_PARA(Type);
  FS_LB_InitMediumIfRequired(pDevice);
  INC_WRITE_SECTOR_CNT(1, Type);             // For statistics / debugging only
  FS_DEBUG_LOG((FS_MTYPE_STORAGE, "WriteDevice   %s:%d: %s SectorNo: 0x%8x.\n", pDevice->pType->pfGetName(pDevice->Data.Unit), pDevice->Data.Unit, _Type2Name(Type), SectorNo));
  CALL_ONDEVICE_HOOK(pDevice, FS_OPERATION_WRITE, SectorNo, 1, Type);
  SET_BUSY_LED(pDevice);
#if FS_SUPPORT_CACHE
  r = _WriteThroughCache(pDevice, SectorNo, pBuffer, 1, 0, Type, WriteToJournal);
#else
  r = _WriteToStorage(pDevice, SectorNo, pBuffer, 1, 0, WriteToJournal);
#endif
  CLR_BUSY_LED(pDevice);
  return r;
}

/*********************************************************************
*
*         FS_LB_WritePart
*
*  Function description
*     Writes a sector to a file system partition
*
*  Parameters
*     pPart           [IN]  Partition to write to
*                     [OUT] ---
*     Sector          Physical sector to be written to partition.
*     pBuffer         [IN]  Sector data to store 
*                     [OUT] ---
*     Type            The type of sector that shall be written.
*     WriteToJournal  Set to 1 if data should go through journal
*
*  Return value
*     ==0     Sector has been read and copied to pBuffer.
*     <0      An error has occurred.
*/
int FS_LB_WritePart(FS_PARTITION * pPart, U32 Sector, const void * pBuffer, U8 Type, U8 WriteToJournal) {
  return FS_LB_WriteDevice(&pPart->Device, pPart->StartSector + Sector, pBuffer, Type, WriteToJournal);
}

/*********************************************************************
*
*       FS_LB_Ioctl
*
*  Function description
*    Executes device command.
*
*  Parameters
*    pDriver     - Pointer to a device driver structure.
*    Cmd         - Command to be executed.
*    Aux         - Parameter depending on command.
*    pBuffer     - Pointer to a buffer used for the command.
*
*  Return value
*    Command specific. In general a negative value means an error.
*/
int FS_LB_Ioctl(FS_DEVICE * pDevice, I32 Cmd, I32 Aux, void * pBuffer) {
  int r;
  int IsInited;
  const FS_DEVICE_TYPE * pDeviceType;

  r = 0;                    // No error so far.
  pDeviceType = pDevice->pType;
  switch (Cmd) {
  case FS_CMD_UNMOUNT:
    // thru
  case FS_CMD_UNMOUNT_FORCED:
    // thru
  case FS_CMD_DEINIT:
    break;
  default:
    IsInited = FS_LB_InitMediumIfRequired(pDevice);
    if (IsInited == 0) {
      r = 1;              // Error: driver could not be initailized.
    }
    break;
  }
  if (r == 0) {
    r = (pDeviceType->pfIoCtl)(pDevice->Data.Unit, Cmd, Aux, pBuffer);
  }
  return r;
}

/*********************************************************************
*
*       FS_GetSectorSize
*
*  Function description
*    Returns the sector size of a device.
*
*/
U16 FS_GetSectorSize(FS_DEVICE * pDevice) {
  U16 r = 0;
  FS_DEV_INFO DevInfo;

  FS_LB_InitMediumIfRequired(pDevice);
  if (pDevice->pType->pfIoCtl(pDevice->Data.Unit, FS_CMD_GET_DEVINFO, 0, &DevInfo) == 0) {
    r = DevInfo.BytesPerSector;
  }
  return r;
}

/*********************************************************************
*
*       FS_LB_GetDeviceInfo
*/
int FS_LB_GetDeviceInfo(FS_DEVICE * pDevice, FS_DEV_INFO * pDevInfo) {
  int r;

  r = 0;
  FS_LB_InitMediumIfRequired(pDevice);
  if (pDevice->pType->pfIoCtl(pDevice->Data.Unit, FS_CMD_GET_DEVINFO, 0, (void *)pDevInfo)) {
    r = -1;
  }
  return r;
}

/*********************************************************************
*
*       FS_LB_FreeSectorsPart
*
*  Function description
*    Frees unused sectors (from cache and devices) of a partition
*/
void FS_LB_FreeSectorsPart(FS_PARTITION * pPart, U32 SectorNo, U32 NumSectors) {
  FS_DEVICE * pDevice;

  pDevice   = &pPart->Device;
  SectorNo += pPart->StartSector;      // Convert into device sector index
  FS_USE_PARA(pDevice);
  FS_DEBUG_LOG((FS_MTYPE_STORAGE, "FreeSectors   %s:%d:      SectorNo: 0x%8x, NumSectors: %u.\n", pDevice->pType->pfGetName(pDevice->Data.Unit), pDevice->Data.Unit, SectorNo, NumSectors));
#if FS_SUPPORT_JOURNAL
  if (pDevice->Data.JournalIsActive) {
//    r = FS_JOURNAL_FreeSector(pDevice, SectorNo, pBuffer, NumSectors, RepeatSame);
// TBD: This can be optimized if the journal can also manage "freed" sectors.
  } else
#endif
  {
    FS_LB_Ioctl(pDevice, FS_CMD_FREE_SECTORS, SectorNo, &NumSectors);
  }
#if FS_SUPPORT_CACHE
  {
    CACHE_FREE CacheFree;

    CacheFree.FirstSector = SectorNo;
    CacheFree.NumSectors  = NumSectors;
    FS__CACHE_CommandDeviceNL(pDevice, FS_CMD_CACHE_FREE_SECTORS, &CacheFree);
  }
#endif
}

/*********************************************************************
*
*       FS_LB_WriteBack
*
*   Function description
*     Writes a sector to storage. Typ. called by the cache modules when a dirty block must be freed (write back).
*/
int FS_LB_WriteBack(FS_DEVICE * pDevice, U32 SectorNo, const void * pBuffer) {
  int r;

  INC_WRITE_CACHE_CLEAN_CNT();
  r = _WriteToStorage(pDevice, SectorNo, pBuffer, 1, 0, 1);
  return r;
}

/*************************** End of file ****************************/
