/*********************************************************************
*                SEGGER MICROCONTROLLER GmbH & Co. KG                *
*        Solutions for real time microcontroller applications        *
**********************************************************************
*                                                                    *
*        (c) 2003-2012     SEGGER Microcontroller GmbH & Co KG       *
*                                                                    *
*        Internet: www.segger.com    Support:  support@segger.com    *
*                                                                    *
**********************************************************************

**** emFile file system for embedded applications ****
emFile is protected by international copyright laws. Knowledge of the
source code may not be used to write a similar product. This file may
only be used in accordance with a license and should not be re-
distributed in any way. We appreciate your understanding and fairness.
----------------------------------------------------------------------
File        : IDE_Drv.c
Purpose     : File system generic IDE driver
---------------------------END-OF-HEADER------------------------------
*/

/*********************************************************************
*
*       #include Section
*
**********************************************************************
*/

#include "FS_ConfDefaults.h"        /* FS Configuration */

#include "FS_Int.h"
#include "IDE_X_HW.h"
#include "FS_CLib.h"

/*********************************************************************
*
*       #define constants
*
**********************************************************************
*/
#define   IDE_SECTOR_SIZE     512

#ifdef FS_IDE_MAXUNIT
  #define NUM_UNITS          FS_IDE_MAXUNIT
#else
  #define NUM_UNITS          4
#endif


/*********************************************************************
*
*       IDE commands
*/
#define CMD_READ_SECTORS          0x20
#define CMD_IDENTIFY              0xEC
#define CMD_SET_FEATURES          0xEF
#define CMD_WRITE_SECTORS         0x30

/*********************************************************************
*
*       IDE status
*/
#define STAT_BUSY                 (1 << 7)
#define STAT_READY                (1 << 6)
#define STAT_WRITE_FAIL           (1 << 5)
#define STAT_DISC_SEEK_COMPLETE   (1 << 4)
#define STAT_DATA_REQUEST         (1 << 3)
#define STAT_CORRECTABLE          (1 << 2)
#define STAT_ERROR                (1 << 0)

/*********************************************************************
*
*       Drive/Head register
*/
#define DH_REG_LBA                (7 << 5)
#define DH_REG_DRIVE0             (0 << 4)
#define DH_REG_DRIVE1             (1 << 4)

/*********************************************************************
*
*       IDE feature commands
*/
#define FEATURE_ENABLE_WRITE_CACHE      (0x02)
#define FEATURE_SET_PIO_MODE            (0x03)
#define FEATURE_ENABLE_READ_LOOK_AHEAD  (0xAA)

/*********************************************************************
*
*       IDE register offsets
*/
#define IDE_ADDR_OFF_SECTOR             0x02
#define IDE_ADDR_OFF_CYLINDER           0x04
#define IDE_ADDR_OFF_DH_CMD             0x06
#define IDE_ADDR_OFF_DATA               0x08
#define IDE_ADDR_OFF_FEAT_ERROR         0x0C
#define IDE_ADDR_OFF_DEVICE_CONTROL     0x0E

/*********************************************************************
*
*       Device control register
*
* Notes:
*   (1) CF spec changed
*       CF 2.0 specifies that only bit 1 and 2 are used.
*       All other bits should be zero.
*       (CF spec 1.4 said bit 4 should be set to 1).
*
**********************************************************************
*/
#define DC_REG_INT_ENABLE         (1 << 1)
#define DC_REG_SW_RESET           (1 << 2)


/*********************************************************************
*
*       #define macros
*
**********************************************************************
*/

#define   NUM_SECTORS_AT_ONCE       255

/*********************************************************************
*
*       Type definitions
*
**********************************************************************
*/
typedef struct {
  U8 aSerialNo[20];
} IDE_SERIAL_NO;

typedef struct {
  U8            IsInited;
  U8            Unit;
  IDE_SERIAL_NO SerialNo;
  U16           NumHeads;
  U16           SectorsPerTrack;
  U32           NumSectors;
  U16           BytesPerSector;
  U8            MaxPioMode;
  U8            IsSlave;
} IDE_INST;

/*********************************************************************
*
*       Static data
*
**********************************************************************
*/
static IDE_INST * _apInst[NUM_UNITS];
static int        _NumUnits;
static U8         _HeadRegister[NUM_UNITS];

/*********************************************************************
*
*       Static code
*
**********************************************************************
*/

#if 0
/*********************************************************************
*
*       _GetCylHigh()
*
  Description:
  FS driver hardware layer function. Read the CYLINDER HIGH register.

  Parameters:
  Unit        - Unit number.
 
  Return value:
  Value of the CYLINDER HIGH register.
*/
static U8 _GetCylHigh(U8 Unit) {
  U16 Data;

  Data = FS_IDE_HW_ReadReg(Unit, IDE_ADDR_OFF_CYLINDER);
  return (U8)(Data >> 8);
}

/*********************************************************************
*
*       _GetCylLow()
*
  Description:
  FS driver hardware layer function. Read the CYLINDER LOW register.

  Parameters:
  Unit        - Unit number.
 
  Return value:
  Value of the CYLINDER LOW register.
*/
static U8 _GetCylLow(U8 Unit) {
  U16 Data;

  Data = FS_IDE_HW_ReadReg(Unit, IDE_ADDR_OFF_CYLINDER);
  return (U8)(Data & 0xff);
}

/*********************************************************************
*
*       _GetDevice()
*
  Description:
  FS driver hardware layer function. Read the DEVICE/HEAD register.

  Parameters:
  Unit        - Unit number.
 
  Return value:
  Value of the DEVICE/HEAD register.
*/
static U8 _GetDevice(U8 Unit) {
  return (U8)FS_IDE_HW_ReadReg(Unit, IDE_ADDR_OFF_DH_CMD);
}

/*********************************************************************
*
*       _GetSectorNo()
*
  Description:
  FS driver hardware layer function. Read the SECTOR NUMBER register.

  Parameters:
  Unit        - Unit number.
 
  Return value:
  Value of the SECTOR NUMBER register.
*/
static U8 _GetSectorNo(U8 Unit) {
  U16 Data;

  Data = FS_IDE_HW_ReadReg(Unit, IDE_ADDR_OFF_SECTOR);
  return (U8)(Data >> 8);
}

/*********************************************************************
*
*       _GetSectorCount()
*
  Description:
  FS driver hardware layer function. Read the SECTOR COUNT register.

  Parameters:
  Unit        - Unit number.
 
  Return value:
  Value of the SECTOR COUNT register.
*/
static U8 _GetSectorCount(U8 Unit) {
  U16 Data;

  Data = FS_IDE_HW_ReadReg(Unit, IDE_ADDR_OFF_SECTOR);
  return (U8)(Data & 0xff);
}

#endif

/*********************************************************************
*
*       _SetFeatures()
*
  Description:
  FS driver hardware layer function. Set the FEATURES register.

  Parameters:
  Unit        - Unit number.
  Data        - Value to write to the FEATURES register.
 
  Return value:
  None.
*/

static void _SetFeatures(U8 Unit, U8 Data) {
  FS_IDE_HW_WriteReg(Unit, IDE_ADDR_OFF_FEAT_ERROR, (U16)(Data << 8));
}

/*********************************************************************
*
*       _GetError()
*
  Description:
  FS driver hardware layer function. Read the ERROR register.

  Parameters:
  Unit        - Unit number.
 
  Return value:
  Value of the ERROR register.
*/
static U8 _GetError(U8 Unit) {
  U16 Data;

  Data = FS_IDE_HW_ReadReg(Unit, IDE_ADDR_OFF_FEAT_ERROR);
  return (U8)(Data >> 8);
}

/*********************************************************************
*
*       _GetAltStatus()
*
  Description:
  FS driver hardware layer function. Read the ALTERNATE STATUS register.

  Parameters:
  Unit        - Unit number.
 
  Return value:
  Value of the ALTERNATE STATUS register.
*/
static U8 _GetAltStatus(U8 Unit) {
  U16 Data;

  Data = FS_IDE_HW_ReadReg(Unit, IDE_ADDR_OFF_DEVICE_CONTROL);
  return (U8)Data & 0xff;
}
/*********************************************************************
*
*       _GetStatus()
*
  Description:
  FS driver hardware layer function. Read the STATUS register.

  Parameters:
  Unit        - Unit number.
 
  Return value:
  Value of the STATUS register.
*/
static U8 _GetStatus(U8 Unit) {
  U16 Data;

  Data = FS_IDE_HW_ReadReg(Unit, IDE_ADDR_OFF_DH_CMD);
  return (U8)(Data >> 8);
}

/*********************************************************************
*
*       _WaitWhileBusy
*
*  Description:
*    FS driver internal function. Wait for a maximum of N * access-time
*    (400ns), that the device is not busy.
*
*  Parameters:
*    Unit        - Unit number.
*
*  Return value:
*    ==0         - Device is not busy.
*    ==1         - Timeout, device is still busy.
*/

static U8 _WaitWhileBusy(IDE_INST * pInst) {
  U8 Status;
  U8  Unit;
  U32 N = 8000000UL;
    
  Unit = pInst->Unit;
  do {
    FS_IDE_HW_Delay400ns(Unit);
    Status = _GetAltStatus(Unit);
    if (--N == 0) {
      FS_DEBUG_ERROROUT((FS_MTYPE_DRIVER, "IDE: _WaitWhileBusy: time out.\n"));
      return 1;
    }
  } while ((Status & STAT_BUSY) != 0);
  FS_IDE_HW_Delay400ns(Unit);
  Status = _GetAltStatus(Unit);
  if (Status & 0x01)   {
    if (_GetError(Unit) == (1 << 2)) {
      //
      //  Clear error
      //
      Status = _GetStatus(Unit);
    } else {
      FS_DEBUG_ERROROUT((FS_MTYPE_DRIVER, "IDE: Drive reported error.\n"));
      return 1;
    }
  }
  if ((Status & (STAT_DISC_SEEK_COMPLETE | STAT_READY)) != (STAT_DISC_SEEK_COMPLETE | STAT_READY)) {
    FS_DEBUG_ERROROUT((FS_MTYPE_DRIVER, "IDE: Drive not ready.\n"));
    return 1;
  }
  return 0;
}


/*********************************************************************
*
*       _SetDevice()
*
  Description:
  FS driver hardware layer function. Set the DEVICE/HEAD register.

  Parameters:
  Unit        - Unit number.
  Data        - Value to write to the DEVICE/HEAD register.

  Problems:   When 16 bit memory access is used, this register can only be
              written together with command register.
  Return value:
  None.
*/
static void _SetDevice(U8 Unit, U8 Data) {
  U16 Data16;
  _HeadRegister[Unit] = Data;  /* Save new Device/Head settings for command writes    */
  Data16 = (U16)(0x00 << 8) | (Data);  // Upper byte = 0x97 => idle 
  FS_IDE_HW_WriteReg(Unit, IDE_ADDR_OFF_DH_CMD, Data16);  
}

/*********************************************************************
*
*       _SetCommand()
*
  Description:
  FS driver hardware layer function. Set the COMMAND register.

  Parameters:
  Unit        - Unit number.
  Data        - Value to write to the COMMAND register.

  Problems:   When 16 bit memory access is used, this register can only be
              written together with Select card / head register.

  Return value:
  None.
*/
static void _SetCommand(U8 Unit, U8 Data) {
  FS_IDE_HW_WriteReg(Unit, IDE_ADDR_OFF_DH_CMD, (U16)(_HeadRegister[Unit] | (Data << 8)));
}

/*********************************************************************
*
*       _SetDevControl()
*
  Description:
  FS driver hardware layer function. Set the DEVICE CONTROL register.

  Parameters:
  Unit        - Unit number.
  Data        - Value to write to the DEVICE CONTROL register.
 
  Return value:
  None.
*/
static void _SetDevControl(U8 Unit, U8 Data) {
  FS_IDE_HW_WriteReg(Unit, IDE_ADDR_OFF_DEVICE_CONTROL, Data);
}

/*********************************************************************
*
*       _SetSectorReg()
*
  Description:
  FS driver hardware layer function. Set the DEVICE CONTROL register.

  Parameters:
  Unit        - Unit number.
  Data        - Value to write to the DEVICE CONTROL register.
 
  Return value:
  None.
*/
static void _SetSectorReg(U8 Unit, U16 Data) {
  FS_IDE_HW_WriteReg(Unit, IDE_ADDR_OFF_SECTOR, Data);
}

/*********************************************************************
*
*       _SetCylReg()
*
  Description:
  FS driver hardware layer function. Set the DEVICE CONTROL register.

  Parameters:
  Unit        - Unit number.
  Data        - Value to write to the DEVICE CONTROL register.
 
  Return value:
  None.
*/
static void _SetCylReg(U8 Unit, U16 Data) {
  FS_IDE_HW_WriteReg(Unit, IDE_ADDR_OFF_CYLINDER, Data);
}

/*********************************************************************
*
*       _SetDCReg()
*
  Description:
  FS driver hardware layer function. Set the DEVICE CONTROL register.

  Parameters:
  Unit        - Unit number.
  Data        - Value to write to the DEVICE CONTROL register.
 
  Return value:
  None.
*/
static void _SetDCReg(U8 Unit, U16 Data) {
  FS_IDE_HW_WriteReg(Unit, IDE_ADDR_OFF_DH_CMD, Data);
}

/*********************************************************************
*
*       _SelectDev
*
*  Description:
*    FS driver internal function. Select a device.
*
*  Parameters:
*    Unit        - Unit number.
*
*  Return value:
*    ==1         - Device has been selected.
*    ==0         - Could not select the device.
*/
static U8 _SelectDev(IDE_INST * pInst) {
  int TimeOut;
  U8  Unit;
  U8  Status;
  U16 DeviceReg;
  int r;

  Unit = pInst->Unit;
  DeviceReg  = DH_REG_LBA;
  DeviceReg |= (pInst->IsSlave == 0) ? DH_REG_DRIVE0 : DH_REG_DRIVE1;
  _SetDCReg(Unit, (U16)DeviceReg);
  _WaitWhileBusy(pInst);
  r = 0;
  /* Wait until BUSY=0 / RDY=1 / DSC=1 */
  TimeOut = 200;
  do {
    FS_IDE_HW_Delay400ns(Unit);
    Status = _GetStatus(Unit);
    if (Status & 0x01)   {
        if (_GetError(Unit) == (1 << 2)) {
          goto CheckForReady;
        } else {
          FS_DEBUG_ERROROUT((FS_MTYPE_DRIVER,  "IDE: Drive reported error.\n"));
          break;
        }
    }
CheckForReady:    
    if ((Status & (STAT_DISC_SEEK_COMPLETE | STAT_READY)) == (STAT_DISC_SEEK_COMPLETE | STAT_READY)) {
      r = 1;
      break;
    }
    if (--TimeOut) {
      FS_DEBUG_ERROROUT((FS_MTYPE_DRIVER,  "IDE: Selectinh the drive timed out.\n"));
    }
  } while (1); /* Until device is selected */
  return r;
}


/*********************************************************************
*
*       _WriteFeature
*
*/
static void _WriteFeature(IDE_INST * pInst, U8 Cmd, U16 Para) {
  U8       Unit;
  unsigned Status;
  U16      DeviceReg;

  Unit = pInst->Unit;
  //
  //  Select device
  //
  Status = _SelectDev(pInst);
  if (Status == 0) {
    FS_DEBUG_ERROROUT((FS_MTYPE_DRIVER,  "IDE: _WriteFeature: SelectDev failed"));
    return;
  }
  _SetFeatures(Unit, Cmd);
  _SetSectorReg(Unit, Para);
  DeviceReg  = DH_REG_LBA;
  DeviceReg |= (pInst->IsSlave == 0) ? DH_REG_DRIVE0 : DH_REG_DRIVE1;
  DeviceReg |= CMD_SET_FEATURES << 8;
  _SetDCReg(Unit, DeviceReg); /* Start command */
  _WaitWhileBusy(pInst);
  Status = _GetStatus(Unit);
  if ((Status & (STAT_ERROR)) || (Status & STAT_BUSY)) {
    FS_DEBUG_ERROROUT((FS_MTYPE_DRIVER,  "IDE: _WriteFeature: Command not supported.\n"));
  }
}


/*********************************************************************
*
*       _WriteSectors
*/
static int _WriteSectors(IDE_INST * pInst , U32 SectorNo, const U8 *pBuffer, U8 NumSectors, int RepeatSame) {
  int i;
  U8 Status;
  U8 Unit;
  U16 Data16;

  /* Wait until not busy; should never be the case */
  i = _WaitWhileBusy(pInst);
  if (i) {
    FS_DEBUG_ERROROUT((FS_MTYPE_DRIVER,  "IDE: _WriteSectors: Busy on entry.\n"));
    return -1;
  }
  /* Select device */
  i = _SelectDev(pInst);
  if (i == 0) {
    FS_DEBUG_ERROROUT((FS_MTYPE_DRIVER,  "IDE: _WriteSectors: SelectDev failed.\n"));
    return -1;
  }
  Unit = pInst->Unit;
  //
  // Setup all necessary registers for command
  //
  _SetDevControl(Unit, DC_REG_INT_ENABLE);
  Data16 = (U16)(SectorNo << 8) | NumSectors;
  _SetSectorReg (Unit, Data16);
  Data16 = (U16)((SectorNo >>  8) & 0xffff);
  _SetCylReg    (Unit, Data16);
  Data16  = (DH_REG_LBA | ((U16)(SectorNo >> 24) & 0x0f));
  Data16 |= (pInst->IsSlave == 0) ? DH_REG_DRIVE0 : DH_REG_DRIVE1;
  Data16 |= CMD_WRITE_SECTORS << 8;
  _SetDCReg(Unit, Data16); /* Start command */
  /* Wait maximum of 8Mio * 400ns = 32s for command to complete */
  _WaitWhileBusy(pInst);
  /* Write sector */
  Status = _GetStatus(Unit);
  if ((Status & (STAT_BUSY | STAT_DATA_REQUEST)) != STAT_DATA_REQUEST) {
    FS_DEBUG_ERROROUT((FS_MTYPE_DRIVER,  "IDE: _WriteSectors: Not ready to write.\n"));
    return -1;
  }
  do {
    FS_IDE_HW_WriteData(Unit, pBuffer, IDE_SECTOR_SIZE);
    if (RepeatSame == 0) {
      pBuffer += IDE_SECTOR_SIZE;
    }
    Status = _WaitWhileBusy(pInst);
  } while (--NumSectors > 0);
  /* Wait maximum of 8Mio * 400ns = 32s for command to complete */
  i = _WaitWhileBusy(pInst);
  if (i) {
    FS_DEBUG_ERROROUT((FS_MTYPE_DRIVER,  "IDE: _WriteSectors: Time out after write.\n"));
    return -1;
  }
  /* Check for error */
  Status = _GetStatus(Unit);
  if (Status & (STAT_CORRECTABLE | STAT_WRITE_FAIL | STAT_BUSY)) {
    FS_DEBUG_ERROROUT((FS_MTYPE_DRIVER,  "IDE: _WriteSectors: Drive reported error.\n"));
    return -1;
  }
  return 0;
}

/*********************************************************************
*
*       _ReadSectors
*/
static int _ReadSectors(IDE_INST * pInst, U32 SectorNo, U8 NumSectors, U8 *pBuffer) {
  int i;
  U8  Status;
  U8  Unit;
  U16 Data16;

  /* Wait until not busy; should never be the case */
  i = _WaitWhileBusy(pInst);
  if (i) {
    FS_DEBUG_ERROROUT((FS_MTYPE_DRIVER,  "IDE: _ReadSectors: Busy on entry.\n"));
    return -1;
  }
  i = !_SelectDev(pInst);  /* Select device */
  if (i) {
    return -1;
  }
  Unit = pInst->Unit;
  //
  // Setup all necessary registers for command
  //
  _SetDevControl(Unit, DC_REG_INT_ENABLE);
  Data16 = ((U16)SectorNo << 8) | NumSectors;
  _SetSectorReg (Unit, Data16);
  Data16 = (U16)((SectorNo >>  8) & 0xffff);
  _SetCylReg    (Unit, Data16);
  Data16   = (DH_REG_LBA | (U16)((SectorNo >> 24) & 0x0f));
  Data16  |= (pInst->IsSlave == 0) ? DH_REG_DRIVE0 : DH_REG_DRIVE1;
  Data16  |=  CMD_READ_SECTORS << 8;
  _SetDCReg(Unit, Data16); /* Start command */
  //
  // Wait maximum of 8Mio * 400ns = 32s for command to complete
  //
  i = _WaitWhileBusy(pInst);
  if (i) {
    return -1;
  }
  /* Read sector */
  Status = _GetStatus(Unit);
  if ((Status & (STAT_BUSY | STAT_DATA_REQUEST)) == STAT_DATA_REQUEST) {
    do {
      FS_IDE_HW_ReadData(Unit, pBuffer, IDE_SECTOR_SIZE);
      Status = _WaitWhileBusy(pInst);
      pBuffer += IDE_SECTOR_SIZE;
    } while (--NumSectors > 0);
  }
  /* Check for error */
  Status = _GetStatus(Unit);
  if (Status & (STAT_CORRECTABLE | STAT_WRITE_FAIL) || (Status & STAT_BUSY)) {
    FS_DEBUG_ERROROUT((FS_MTYPE_DRIVER, "_ReadSectors: Drive reported error. Device Status = 0x%x.\n", Status));
    return -1;
  }
  return 0;
}

/*********************************************************************
*
*       _IDE_ReadSectors
*
*  Description:
*    Read a sector from the media.
*
*  Parameters:
*    Unit        - Unit number.
*    Sector      - Sector to be read from the device.
*    pBuffer     - Pointer to buffer for storing the data.
*
*  Return value:
*    ==0         - Sector has been read and copied to pBuffer.
*    < 0         - An error has occurred.
*/
static int _IDE_ReadSectors(IDE_INST * pInst, U32 Sector, U8 *pBuffer, U32 NumSectors) {
  U32 i;
  U32 NumLoops;
  int    Status = 0;
  NumLoops = (NumSectors + NUM_SECTORS_AT_ONCE - 1) / NUM_SECTORS_AT_ONCE;
  for (i = 0; (i < NumLoops) && (Status == 0); i++) {
    U8 NumSectors2Read;
    if (NumSectors > NUM_SECTORS_AT_ONCE) {
      NumSectors2Read =  (U8)(NUM_SECTORS_AT_ONCE);
    } else {
     NumSectors2Read =  (U8)(NumSectors & 0xff);
    }
    Status      = _ReadSectors(pInst, Sector, NumSectors2Read, pBuffer);
    NumSectors -= NumSectors2Read;
    pBuffer    += NumSectors2Read * IDE_SECTOR_SIZE;
    Sector     += NumSectors2Read;
  }
  return Status;
}

/*********************************************************************
*
*       _IDE_WriteSectors
*
*  Description:
*    Write sector to the media.
*
*  Parameters:
*    Unit        - Unit number.
*    SectorNo    - Sector number to be written to the device.
*    NumSectors  - Number of sectors
*    pBuffer     - Pointer to data to be stored.
*
*  Return value:
*    ==0         - Sector has been written to the device.
*    < 0         - An error has occurred.
*/
static int _IDE_WriteSectors(IDE_INST * pInst, U32 SectorNo, const U8 *pBuffer, U32 NumSectors, int RepeatSame) {
  U32 i;
  U32 NumLoops;
  int    Status = 0;
  NumLoops = (NumSectors + NUM_SECTORS_AT_ONCE - 1) / NUM_SECTORS_AT_ONCE;
  for (i = 0; (i < NumLoops) && (Status == 0); i++) {
    U8 NumSectors2Write;
    if (NumSectors > NUM_SECTORS_AT_ONCE) {
      NumSectors2Write = (U8)NUM_SECTORS_AT_ONCE;
    } else {
      NumSectors2Write = (U8)(NumSectors & 0xff);
    }
    Status      = _WriteSectors(pInst, SectorNo, pBuffer, NumSectors2Write, RepeatSame);
    NumSectors -= NumSectors2Write;
    if (RepeatSame == 0) {
      pBuffer    += NumSectors2Write * IDE_SECTOR_SIZE;
    }
    SectorNo   += NumSectors2Write;
  }
  return Status;
}


/*********************************************************************
*
*       _IDE_ReadDriveInfo
*
*  Description:
*    Reads and Identifies Drive Information from the media.
*
*  Parameters:
*    Unit        - Unit number.
*    pBuffer     - Pointer to buffer for storing the data.
*
*  Return value:
*    ==0         - Info has been read and copied to pBuffer.
*    < 0         - An error has occurred.
*/

static int _IDE_ReadDriveInfo(IDE_INST * pInst, U16 *pBuffer) {
  U8 Status;
  U8 Unit;

  /* Wait until not busy; should never be the case */
  Status = _WaitWhileBusy(pInst);
  if (Status) {
    return -1;
  }
  Status = _SelectDev(pInst);  /* Select device */
  if (Status == 0) {
    return -1;  /* Select failed */
  }
  Unit = pInst->Unit;
  /* Setup command parameters */
  FS_IDE_HW_Delay400ns(Unit);
  _SetCommand(Unit, 0xEC);  /* Start command */
  /* Wait maximum of 8Mio * 400ns = 32s for command to complete */
  Status = _WaitWhileBusy(pInst);
  if (Status) {
    return -1;
  }
  /* Read Info */
  Status = _GetStatus(Unit);
  if ((Status & (STAT_BUSY | STAT_DATA_REQUEST )) == STAT_DATA_REQUEST) {
    FS_IDE_HW_ReadData(Unit, (U8 *)pBuffer, IDE_SECTOR_SIZE);
    Status = _WaitWhileBusy(pInst);
  }
  /* Check for error */
  Status = _GetStatus(Unit);
  if ((Status & (STAT_BUSY | STAT_WRITE_FAIL | STAT_CORRECTABLE)) || (Status & STAT_BUSY)) {
    return -1;
  }
  return 0;
}

/*********************************************************************
*
*       _GetDeviceInfo
*
*  Description:
*  Retrieves the device information of the card/HDD and stores them locally.
*
* Parameters:
*   Unit        - Unit number.
*
*  Return value:
*   ==0         - Device has been reset.
*   < 0         - An error has occurred.
*/
static int _GetDeviceInfo(IDE_INST * pInst) {
  U16  aIDEInfoBuffer[256];
  unsigned Data;
  int  r;

  r = 0;
  r = _IDE_ReadDriveInfo(pInst, &aIDEInfoBuffer[0]);
  if (r != 0) {
    return -1; /* read failed, card invalid */
  }
  pInst->NumHeads        = FS_LoadU16LE((const U8 *)&aIDEInfoBuffer[3]);  /* heads */
  pInst->SectorsPerTrack = FS_LoadU16LE((const U8 *)&aIDEInfoBuffer[6]);  /* sec per track */
  pInst->NumSectors      = FS_LoadU32LE((const U8 *)&aIDEInfoBuffer[60]); /* Number of sectors */
  pInst->BytesPerSector  = IDE_SECTOR_SIZE;                 /* IDE/CF media are always working with 512 Bytes per sector */
  Data = FS_LoadU16LE((const U8 *)&aIDEInfoBuffer[53]);
  if (Data & (1 << 1)) {
    U8 MaxPioMode = 2;
    Data = FS_LoadU16LE((const U8 *)&aIDEInfoBuffer[64]) & 0xff;
    do {
      if (Data & 1) {
        MaxPioMode++;
      }
      Data >>= 1;
    } while (Data);
    pInst->MaxPioMode = MaxPioMode;
  } else {
    pInst->MaxPioMode = FS_LoadU16LE((const U8 *)&aIDEInfoBuffer[51]) >> 8;  /* Max PIO mode */
  }
  FS_MEMCPY(&pInst->SerialNo, &aIDEInfoBuffer[10], 20);
  return r;
}

/*********************************************************************
*
*       _IDE_Init
*
*  Description:
*  Resets/Initializes the device
*
* Parameters:
*   Unit        - Unit number.
*
*  Return value:
*   ==0         - Device has been reset.
*   < 0         - An error has occurred.
*/
static int _IDE_Init(IDE_INST * pInst) {
  int i;
  U8  Unit;

  Unit = pInst->Unit;
  FS_IDE_HW_Reset(Unit);
  //
  //  Do a soft reset of IDE device
  //
  _SetDevControl(Unit, DC_REG_SW_RESET | DC_REG_INT_ENABLE);
  //
  //  Wait at least 80ms before releasing the soft reset.
  //
  for (i = 0; i < 200; i++) {
    FS_IDE_HW_Delay400ns(Unit);
  }
  //
  //  Release soft reset
  //
  _SetDevControl(Unit, DC_REG_INT_ENABLE);
  if (pInst->IsSlave == 0) {
    _SetDevice(Unit, DH_REG_LBA | DH_REG_DRIVE0);
  } else {
    _SetDevice(Unit, DH_REG_LBA | DH_REG_DRIVE1);
  }
  /* Wait maximum of 8Mio * 400ns = 32s for device to get ready */
  i = _WaitWhileBusy(pInst);
  if (i) {
    return -1;
  }
  if (_GetDeviceInfo(pInst)) {
    return -1;
  }
  //
  // Setup drive for better performance
  //
  _WriteFeature(pInst, FEATURE_ENABLE_WRITE_CACHE, 0);
  _WriteFeature(pInst, FEATURE_ENABLE_READ_LOOK_AHEAD, 0);
//  _WriteFeature(pInst, FEATURE_SET_PIO_MODE, pInst->MaxPioMode);
  return 0;
}

/*********************************************************************
*
*       _IDE_Unmount
*
*  Description:
*    FS driver function. Unmounts the volume.
*
*  Parameters:
*    Unit        - Unit number.
*
*/
static void _IDE_Unmount(U8 Unit) {
  if (_apInst[Unit]->IsInited == 1) {
    FS_MEMSET(_apInst[Unit], 0, sizeof(IDE_INST));
  }
}



/*********************************************************************
*
*       _GetDevInfo
*
*  Description:
*    Returns device info
*/
static int _GetDevInfo(IDE_INST * pInst, FS_DEV_INFO * pInfo) {
  int        r;

  r        = 0;
  if (pInfo) {
    if (pInst->IsInited != 1) {
      _GetDeviceInfo(pInst);
    }
    pInfo->NumHeads        = pInst->NumHeads;         /* heads */
    pInfo->SectorsPerTrack = pInst->SectorsPerTrack;  /* sec per track */
    pInfo->NumSectors      = pInst->NumSectors;       /* Number of sectors */
    pInfo->BytesPerSector  = pInst->BytesPerSector;   /* IDE/CF media are always working with 512 Bytes per sector */
  }
  return r;
}

/*********************************************************************
*
*       Public code (indirectly thru callback)
*
**********************************************************************
*/

/*********************************************************************
*
*       _IDE_GetStatus
*
*  Description:
*    FS driver function. Get status of the media.
*
*  Parameters:
*    Unit                  - Unit number.
*
*  Return value:
*    FS_MEDIA_STATE_UNKNOWN if the state of the media is unknown.
*    FS_MEDIA_NOT_PRESENT   if no card is present.
*    FS_MEDIA_IS_PRESENT    if a card is present.
*/

static int _IDE_GetStatus(U8 Unit) {
  return FS_IDE_HW_IsPresent(Unit);  /* Check if a card is present */
}

/*********************************************************************
*
*       _IDE_Read
*
*  Function Description
*    Driver callback function.
*    Reads one or more logical sectors from storage device.
*
*  Return value:
*      0                       - Data successfully written.
*    !=0                       - An error has occurred.
*
*/
static int _IDE_Read(U8 Unit, U32 SectorNo, void * p, U32 NumSectors) {
  IDE_INST * pInst;
  
  pInst = _apInst[Unit];
  return _IDE_ReadSectors(pInst, SectorNo, (U8 *)p, NumSectors);
}

/*********************************************************************
*
*       _IDE_Write
*
*  Description:
*    FS driver function. Writes one or more logical sectors to storage device.
*
*  Return value:
*      0                       - Data successfully written.
*    !=0                       - An error has occurred.
*
*/
static int _IDE_Write(U8 Unit, U32 SectorNo, const void  * p, U32 NumSectors, U8  RepeatSame) {
  IDE_INST * pInst;
  
  pInst = _apInst[Unit];
 return _IDE_WriteSectors(pInst, SectorNo, (const U8 *)p, NumSectors, RepeatSame);
}

/*********************************************************************
*
*             _IDE_DevIoCtl
*
*  Description:
*    FS driver function. Execute device command.
*
*  Parameters:
*    Unit    - Unit number.
*    Cmd         - Command to be executed.
*    Aux         - Parameter depending on command.
*    pBuffer     - Pointer to a buffer used for the command.
*
*  Return value:
*    Command specific. In general a negative value means an error.
*/
static int _IDE_IoCtl(U8 Unit, I32 Cmd, I32 Aux, void *pBuffer) {
  IDE_INST * pInst;

  pInst = _apInst[Unit];
  FS_USE_PARA(Aux);
  switch (Cmd) {
  case FS_CMD_UNMOUNT:
  case FS_CMD_UNMOUNT_FORCED:
    _IDE_Unmount(Unit);
    break;
  case FS_CMD_GET_DEVINFO:
    return _GetDevInfo(pInst, (FS_DEV_INFO *)pBuffer);
#if FS_SUPPORT_DEINIT
  case FS_CMD_DEINIT:
    FS_FREE(pInst);
    _apInst[Unit] = 0;  //-Q-: pInst
    _NumUnits--;
    return 0;
#endif
  default:
    break;
  }
  return 0;
}

/*********************************************************************
*
*       _IDE_InitMedium
*
*  Description:
*    Initialize the specified medium.
*
*  Parameters:
*    Unit    - Unit number.
*
*  Return value:
*/
static int _IDE_InitMedium(U8 Unit) {
  int i;
  IDE_INST * pInst;

  pInst = _apInst[Unit];
  if (pInst->IsInited == 0) {
    i = _IDE_Init(pInst);
    if (i < 0) { /* init failed, no valid card in slot */
      FS_DEBUG_WARN((FS_MTYPE_DRIVER, "IDE/CF: Init failure, no valid card found"));
      return -1;
    } else {
      pInst->IsInited = 1;
    }
  }
  return 0;
}

/*********************************************************************
*
*      _AllocIfRequired
*
* Purpose:
*   Handles relocation if necessary before writing data
*
*/
static IDE_INST * _AllocIfRequired(U8 Unit) {
  if (_apInst[Unit] == NULL) {
     _apInst[Unit] = (IDE_INST *)FS_AllocZeroed(sizeof(IDE_INST));
     _apInst[Unit]->IsSlave = Unit & 1;
  }
  return _apInst[Unit];
}
  



/*********************************************************************
*
*       _IDE_AddDevice
*
*  Description:
*    Initializes the low-level driver object.
*
*  Return value:
*    >= 0                       - Command succesfully executed, Unit no.
*    <  0                       - Error, could not add device
*
*/
static int _IDE_AddDevice(void) {
  U8         Unit;
  IDE_INST * pInst;

  if (_NumUnits >= NUM_UNITS) {
    return -1;
  }
  Unit = _NumUnits++;
  pInst = _AllocIfRequired(Unit);   // Alloc memory. This is guaranteed to work by the memory module.
  pInst->Unit   = Unit;
  return Unit;
}

/*********************************************************************
*
*       _IDE_GetNumUnits
*/
static int _IDE_GetNumUnits(void) {
  return _NumUnits;
}

/*********************************************************************
*
*       _IDE_GetDriverName
*/
static const char * _IDE_GetDriverName(U8 Unit) {
  FS_USE_PARA(Unit);
  return "ide";
}

/*********************************************************************
*
*       Public data
*
**********************************************************************
*/
const FS_DEVICE_TYPE FS_IDE_Driver = {
  _IDE_GetDriverName,
  _IDE_AddDevice,
  _IDE_Read,
  _IDE_Write,
  _IDE_IoCtl,
  _IDE_InitMedium,
  _IDE_GetStatus,
  _IDE_GetNumUnits
};

/*********************************************************************
*
*       Public code
*
**********************************************************************
*/
void FS_IDE_Configure(U8 Unit, U8 IsSlave) {
  IDE_INST * pInst;
  
  pInst = _AllocIfRequired(Unit);
  pInst->IsSlave = IsSlave;  
}

/*************************** End of file ****************************/
