/*********************************************************************
*                SEGGER MICROCONTROLLER GmbH & Co. KG                *
*        Solutions for real time microcontroller applications        *
**********************************************************************
*                                                                    *
*        (c) 2003-2012     SEGGER Microcontroller GmbH & Co KG       *
*                                                                    *
*        Internet: www.segger.com    Support:  support@segger.com    *
*                                                                    *
**********************************************************************

**** emFile file system for embedded applications ****
emFile is protected by international copyright laws. Knowledge of the
source code may not be used to write a similar product. This file may
only be used in accordance with a license and should not be re-
distributed in any way. We appreciate your understanding and fairness.
----------------------------------------------------------------------
File        : NOR_PHY_ST_M25.c
Purpose     : Low level flash driver for NOR SPI flash (ST/Numonyx M25 series)
Literature  : [1] \\fileserver\Techinfo\Company\ST\Flash\SerialFlash\M25P10.pdf
              [2] \\fileserver\Techinfo\Company\SST\Flash\SST25VF016B.pdf
------------  END-OF-HEADER  -----------------------------------------
*/

#include "FS_Int.h"
#include "NOR_Private.h"
#include "NOR_HW_SPI_X.h"

/*********************************************************************
*
*      Defines, configurable
*
**********************************************************************
*/
#ifdef    FS_NOR_MAXUNIT
  #define NUM_UNITS  FS_NOR_MAXUNIT
#else
  #define NUM_UNITS  2
#endif

/*********************************************************************
*
*       Defines, non-configurable
*
**********************************************************************
*/
#define CMD_WRSR        0x01    // Write the status register
#define CMD_PP          0x02    // Page Program
#define CMD_RDSR        0x05    // Read Status Register
#define CMD_WREN        0x06    // Write Enable
#define CMD_FAST_READ   0x0B    // Read Data Bytes at Higher Speed
#define CMD_RDID        0x9F    // Read Identification
#define CMD_RES         0xAB    // Release from deep power-down
#define CMD_SE          0xD8    // Sector Erase

#define TIMEOUT_SECTOR_ERASE  3000  // Time-outs here are given in ms
#define TIMEOUT_PAGE_PROGRAM  5     // Time-outs here are given in ms
#define BLOCK_PROTECT_MASK    0x3C  // Bitmask of the write protection flags

/*********************************************************************
*
*       ASSERT_SECTOR_INDEX_IS_IN_RANGE
*/
#if FS_DEBUG_LEVEL >= FS_DEBUG_LEVEL_CHECK_ALL
  #define ASSERT_SECTOR_INDEX_IS_IN_RANGE(pInst, SectorIndex)                   \
    if (SectorIndex >= pInst->NumSectors) {                                     \
      FS_DEBUG_ERROROUT((FS_MTYPE_DRIVER, "NOR_SPI: Invalid sector index.\n")); \
      FS_X_Panic(FS_ERROR_INVALID_PARA);                                        \
    }
#else
  #define ASSERT_SECTOR_INDEX_IS_IN_RANGE(pInst, SectorIndex)
#endif

/*********************************************************************
*
*       ASSERT_DEVICE_IS_SET
*/
#if FS_DEBUG_LEVEL >= FS_DEBUG_LEVEL_CHECK_ALL
  #define ASSERT_DEVICE_IS_SET(pInst)                                           \
    if (pInst->pDevice == NULL) {                                               \
      FS_DEBUG_ERROROUT((FS_MTYPE_DRIVER, "NOR_SPI: Device not set.\n"));       \
      FS_X_Panic(FS_ERROR_UNKNOWN_DEVICE);                                      \
    }
#else
  #define ASSERT_DEVICE_IS_SET(pInst)
#endif

/*********************************************************************
*
*       ASSERT_UNIT_NO_IS_IN_RANGE
*/
#if FS_DEBUG_LEVEL >= FS_DEBUG_LEVEL_CHECK_ALL
  #define ASSERT_UNIT_NO_IS_IN_RANGE(Unit)                                      \
    if (Unit >= NUM_UNITS) {                                                    \
      FS_DEBUG_ERROROUT((FS_MTYPE_DRIVER, "NOR_SPI: Invalid unit number.\n"));  \
      FS_X_Panic(FS_ERROR_INVALID_PARA);                                        \
    }
#else
  #define ASSERT_UNIT_NO_IS_IN_RANGE(Unit)
#endif

/*********************************************************************
*
*      Types
*
**********************************************************************
*/
typedef struct {
  U8  DeviceID;
  U32 SectorSize;
  U16 NumSectors;
  U16 BytesPerPage;
} FLASH_DEVICE;

typedef struct {
  U32 BaseAddr;
  U32 StartAddrConf;            // Configured start address
  U32 StartAddrUsed;            // Start addr. actually used (aligned to start of a sector)
  U32 NumBytes;
  U32 NtimeoutErase;            // Number of cycles to wait for sector erase completion till timeout
  U32 NtimeoutWritePage;        // Number of cycles to wait for page program completion till timeout
  const FLASH_DEVICE * pDevice;
  U16 NumSectors;               // Total number of physical sectors
  U8  IsInited;                 // This variable is used for 2 different things:
                                // 1. The device is identified and ready to be used. [Bit 0]
                                // 2. The device is set manually. [Bit 1]
  U8  Unit;
} NOR_SPI_INST;

/*********************************************************************
*
*      Static const
*
**********************************************************************
*/
static const FLASH_DEVICE _aDevice[] = {
  {0x11, 0x08000UL,   4, 256},   //   1MBit Device
  {0x12, 0x10000UL,   4, 256},   //   2MBit Device
  {0x13, 0x10000UL,   8, 256},   //   4MBit Device
  {0x14, 0x10000UL,  16, 256},   //   8MBit Device
  {0x15, 0x10000UL,  32, 256},   //  16MBit Device
  {0x16, 0x10000UL,  64, 256},   //  32MBit Device
  {0x17, 0x10000UL, 128, 256},   //  64MBit Device
  {0x18, 0x40000UL,  64, 256},   // 128MBit Device
  {0x00, 0x00000UL,   0, 0}
};

/*********************************************************************
*
*      Static data
*
**********************************************************************
*/
static NOR_SPI_INST * _apInst[NUM_UNITS];

/*********************************************************************
*
*      Static code
*
**********************************************************************
*/

/*********************************************************************
*
*      _EnableWrite
*
*   Function description
*     Sets the write enable bit in the SPI flash.
*
*/
static void _EnableWrite(U8 Unit) {
  U8 Cmd;

  Cmd = CMD_WREN;
  FS_NOR_SPI_HW_X_EnableCS(Unit);
  FS_NOR_SPI_HW_X_Write(Unit, &Cmd, sizeof(Cmd));
  FS_NOR_SPI_HW_X_DisableCS(Unit);
}

/*********************************************************************
*
*      _WaitForEndOfOperation
*
*   Function description
*     Waits for flash to be ready for next command.
*/
static int _WaitForEndOfOperation(U8 Unit, U32 TimeOut) {
  int r;
  U8  Cmd;
  U8  Status;

  r = 1;              // Set to indicate an error.
  FS_NOR_SPI_HW_X_EnableCS(Unit);
  Cmd = CMD_RDSR;
  FS_NOR_SPI_HW_X_Write(Unit, &Cmd, 1);
  do {
    FS_NOR_SPI_HW_X_Read(Unit, &Status, 1);
    if ((Status & 0x01) == 0) {
      r = 0;          // OK, SPI flash is ready for a new operation.
      break;
    }
  } while (--TimeOut);
  FS_NOR_SPI_HW_X_DisableCS(Unit);
  return r;
}

/*********************************************************************
*
*      _ReadStatusRegister
*
*   Function description
*     Returns the contents of the status register.
*/
static U8 _ReadStatusRegister(U8 Unit) {
  U8  Cmd;
  U8  Status;

  Cmd = CMD_RDSR;
  FS_NOR_SPI_HW_X_EnableCS(Unit);
  FS_NOR_SPI_HW_X_Write(Unit, &Cmd, 1);
  FS_NOR_SPI_HW_X_Read(Unit, &Status, 1);
  FS_NOR_SPI_HW_X_DisableCS(Unit);
  return Status;
}

/*********************************************************************
*
*      _WriteStatusRegister
*
*   Function description
*     Writes a value to status register. Typ. called to remove the write protection of physical blocks.
*
*/
static void _WriteStatusRegister(U8 Unit, U8 Value) {
  U8 aCmd[2];

  _EnableWrite(Unit);
  aCmd[0] = CMD_WRSR;
  aCmd[1] = Value;
  FS_NOR_SPI_HW_X_EnableCS(Unit);
  FS_NOR_SPI_HW_X_Write(Unit, aCmd, sizeof(aCmd));
  FS_NOR_SPI_HW_X_DisableCS(Unit);
}

/*********************************************************************
*
*      _RemoveWriteProtection
*
*   Function description
*     Makes all physical sectors writable.
*
*/
static int _RemoveWriteProtection(const NOR_SPI_INST * pInst) {
  U8  Unit;
  U8  Status;
  int r;

  r      = 0;                     // No error so far.
  Unit = pInst->Unit;
  Status = _ReadStatusRegister(Unit);
  if (Status & BLOCK_PROTECT_MASK) {
  _WriteStatusRegister(Unit, 0);
  r = _WaitForEndOfOperation(Unit, pInst->NtimeoutWritePage);
  }
  return r;
}

/*********************************************************************
*
*      _Init
*
*   Function description
*     Initializes the HW layer and auto-detects the SPI flash if not configured manually.
*
*/
static void _Init(NOR_SPI_INST * pInst) {
  U8 Cmd;
  U8 aData[3];
  const FLASH_DEVICE * pDevice;
  U32 Frequency;
  U16 NumSectors;
  U32 SectorSize;
  U32 BaseAddr;
  U32 StartAddrUsed;
  I32  NumBytesToSkip;
  I32  NumBytesRem;
  U32 NumBytesSkipped;
  U8  Unit;

  Unit = pInst->Unit;
  //
  // Initialize SPI
  //
  Frequency = FS_NOR_SPI_HW_X_Init(Unit);
  FS_NOR_SPI_HW_X_DisableCS(Unit);
  //
  // Release device from an possible deep power-down mode (Mode for PE devices and newer P -devices, which do not need or accept dummy bytes).
  //
  Cmd = CMD_RES;
  FS_NOR_SPI_HW_X_EnableCS(Unit);
  FS_NOR_SPI_HW_X_Write(Unit, &Cmd, sizeof(Cmd));
  FS_NOR_SPI_HW_X_DisableCS(Unit);
  //
  // Release device from an possible Deep power mode with dummy bytes.
  //
  FS_NOR_SPI_HW_X_EnableCS(Unit);
  FS_NOR_SPI_HW_X_Write(Unit, &Cmd, sizeof(Cmd));
  FS_MEMSET(aData, 0, sizeof(aData));
  FS_NOR_SPI_HW_X_Write(Unit, aData, sizeof(aData));
  FS_NOR_SPI_HW_X_DisableCS(Unit);
  if (pInst->pDevice == NULL) {
    //
    // Send ID command
    //
    Cmd = CMD_RDID;
    FS_NOR_SPI_HW_X_EnableCS(Unit);
    FS_NOR_SPI_HW_X_Write(Unit, &Cmd, sizeof(Cmd));
    FS_NOR_SPI_HW_X_Read (Unit, aData, sizeof(aData));
    FS_NOR_SPI_HW_X_DisableCS(Unit);
    //
    // Check Id code and 
    // verify and write sector information to pInst.
    //
    //
    FS_DEBUG_LOG((FS_MTYPE_DRIVER, "Found Serial NOR Flash with following ids: ManufacturerId 0x%2x MemoryTypeId 0x%2x MemoryCapacityId 0x%2x.\n", aData[0], aData[1], aData[2]));
    pDevice = &_aDevice[0];
    do {
      if (pDevice->DeviceID == aData[2]) {
        pInst->pDevice = pDevice;
        break;
      }
      pDevice++;
      if (pDevice->DeviceID == 0) {
        FS_DEBUG_ERROROUT((FS_MTYPE_DRIVER, "SPI NOR FLASH driver: Could not identify device\n"));
        FS_X_Panic(FS_ERROR_UNKNOWN_DEVICE);
      }
    } while (pDevice);
  } else {
    FS_DEBUG_LOG((FS_MTYPE_DRIVER, "Device is selected manually.\n"));   // Note: This is typically done for older devices, which do not support RDID command
  }
  //
  // Remove the write protection of all physical sectors.
  //
  _RemoveWriteProtection(pInst);
  //
  // OK, the device is identified. Determine which phyiscal sectors are used as storage.
  //
  NumBytesSkipped = 0;
  BaseAddr        = pInst->BaseAddr;
  NumBytesToSkip  = pInst->StartAddrConf - BaseAddr;
  NumBytesRem     = pInst->NumBytes;
  SectorSize      = pInst->pDevice->SectorSize;
  NumSectors      = pInst->pDevice->NumSectors;
  //
  // Take care of bytes to skip before storage area.
  //
  while (NumSectors && (NumBytesToSkip > 0)) {
    NumBytesToSkip  -= SectorSize;
    NumBytesSkipped += SectorSize;
    NumSectors--;
  }
  StartAddrUsed = BaseAddr + NumBytesSkipped;
  if (NumSectors) {
    U16 NumSectorsRem;

    NumSectorsRem = (U16)((U32)NumBytesRem / SectorSize);
    if (NumSectors > NumSectorsRem) {
      NumSectors = NumSectorsRem;
      NumBytesRem = 0;      // No more sectors after this to make sure that sectors are adjacent!
    } else {
      NumBytesRem -= NumSectors * SectorSize;
    }
      }
  if (NumSectors == 0) {
    FS_DEBUG_ERROROUT((FS_MTYPE_DRIVER, "NOR_SPI: Flash size to small for this configuration. 0 bytes available.\n"));
    }
  pInst->StartAddrUsed      = StartAddrUsed;
  pInst->NumSectors         = NumSectors;
  pInst->NtimeoutErase      = Frequency * TIMEOUT_SECTOR_ERASE;
  pInst->NtimeoutWritePage  = Frequency * TIMEOUT_PAGE_PROGRAM;
}

/*********************************************************************
*
*      _InitIfRequired
*
*/
static void _InitIfRequired(NOR_SPI_INST * pInst) {
  if ((pInst->IsInited & 1) == 0) {
    _Init(pInst);
    pInst->IsInited |= 1;
  }
}

/*********************************************************************
*
*      _WritePage
*
*   Function description
*     Writes data to a page of SPI flash.
*
*   Return value
*     ==0   O.K., data has been written
*     !=0   An error occurred
*/
static int _WritePage(const NOR_SPI_INST * pInst, U32 Addr, const U8 * pSrc, int NumBytes) {
  U8 aCmd[4];
  U8 Unit;

  Unit = pInst->Unit;
  _EnableWrite(Unit);
  //
  //  Send write page command to serial flash
  //
  aCmd[0] = CMD_PP;
  aCmd[1] = (U8)(Addr >> 16);
  aCmd[2] = (U8)(Addr >>  8);
  aCmd[3] = (U8)Addr;
  FS_NOR_SPI_HW_X_EnableCS(Unit);
  FS_NOR_SPI_HW_X_Write(Unit, aCmd, sizeof(aCmd));
  FS_NOR_SPI_HW_X_Write(Unit, pSrc, NumBytes);
  FS_NOR_SPI_HW_X_DisableCS(Unit);
  return _WaitForEndOfOperation(Unit, pInst->NtimeoutWritePage);
}

/*********************************************************************
*
*      _WriteData
*
*   Function description
*     Writes data to SPI flash and handles relocation if necessary.
*
*   Return value
*     ==0   O.K., data has been written
*     !=0   An error occurred
*/
static int _WriteData(NOR_SPI_INST * pInst, U32 Addr, const U8 * pData, int NumBytes) {
  int NumBytes2Write;
  int r;
  U32 BytesPerPage;

  BytesPerPage = pInst->pDevice->BytesPerPage;
  if (Addr & (BytesPerPage - 1)) {
    NumBytes2Write = BytesPerPage - (Addr & (BytesPerPage - 1));
    NumBytes2Write = MIN (NumBytes2Write, NumBytes);
    r = _WritePage(pInst, Addr, pData, NumBytes2Write);
    if (r) {
      return 1;           // Error, write failed.
    }
    pData    += NumBytes2Write;
    NumBytes -=  NumBytes2Write;
    Addr     += NumBytes2Write;
  }
  if (NumBytes > 0) {
    do {
      NumBytes2Write = MIN((U32)NumBytes, BytesPerPage);
      r = _WritePage(pInst, Addr, pData, NumBytes2Write);
      if (r) {
        return 1;         // Error, write failed.
      }
      pData    += NumBytes2Write;
      NumBytes -= NumBytes2Write;
      Addr     += NumBytes2Write;
    } while(NumBytes);
  }
  return 0;               // OK, data written successfully.
}

/*********************************************************************
*
*      _AllocIfRequired
*
*   Function description
*     Allocates memory for the instance of a physical layer.
*
*/
static NOR_SPI_INST * _AllocIfRequired(U8 Unit) {
  NOR_SPI_INST * pInst;

  ASSERT_UNIT_NO_IS_IN_RANGE(Unit);
  pInst = _apInst[Unit];
  if (pInst == NULL) {
     pInst = (NOR_SPI_INST *)FS_AllocZeroed(sizeof(NOR_SPI_INST));
     pInst->Unit  = Unit;
    _apInst[Unit] = pInst;
  }
  return pInst;
}

/*********************************************************************
*
*      Exported code
*
**********************************************************************
*/

/*********************************************************************
*
*      _WriteOff
*
*   Function description
*     Physical layer function.
*   This routine writes data into any section of the flash. It does not
*     check if this section has been previously erased; this is in the responsibility of the user program.
*     Data written into multiple sectors at a time can be handled by this routine.
*
*   Return value
*     ==0   O.K., data has been written
*     !=0   An error occurred
*/
static int _WriteOff(U8 Unit, U32 Off, const void * pSrc, U32 NumBytes) {
  U32 Addr;
  int r;
  NOR_SPI_INST * pInst;

  r = 1;                  // Set to indicate an error.
  ASSERT_UNIT_NO_IS_IN_RANGE(Unit);
  pInst = _apInst[Unit];
  if (pInst) {
    _InitIfRequired(pInst);
    if (NumBytes) {
      Addr = pInst->StartAddrUsed + Off;
      r = _WriteData(pInst, Addr, (U8 *)pSrc, NumBytes);
      }
    }
  return r;
}

/*********************************************************************
*
*       _ReadOff
*
*   Function description
*     Physical layer function. Reads data from the given offset of the flash.
*/
static int _ReadOff(U8 Unit, void * pDest, U32 Off, U32 Len) {
  U32 Addr;
  int r;
  NOR_SPI_INST * pInst;

  r = 1;                  // Set to indicate an error.
  ASSERT_UNIT_NO_IS_IN_RANGE(Unit);
  pInst = _apInst[Unit];
  if (pInst) {
    U8 aCmd[5];

    _InitIfRequired(pInst);
    Addr = pInst->StartAddrUsed + Off;
    aCmd[0] = CMD_FAST_READ;
    aCmd[1] = (U8)(Addr >> 16);
    aCmd[2] = (U8)(Addr >>  8);
    aCmd[3] = (U8)Addr;
    aCmd[4] = 0xff;
    FS_NOR_SPI_HW_X_EnableCS(Unit);
    FS_NOR_SPI_HW_X_Write(Unit, aCmd, sizeof(aCmd));
    FS_NOR_SPI_HW_X_Read (Unit, (U8 *)pDest, Len);
    FS_NOR_SPI_HW_X_DisableCS(Unit);
    r = 0;
  }
  return r;
}

/*********************************************************************
*
*      _EraseSector
*
*   Function description
*     Physical layer function. Erases one physical sector.
*
*   Return value
*     ==0   O.K., sector has been erased
*     !=0   An error occurred
*/
static int _EraseSector(U8 Unit, unsigned int SectorIndex) {
  int r;
  U32 SectorSize;
  U32 Addr;
  NOR_SPI_INST * pInst;

  r = 1;                    // Set to indicate an error.
  ASSERT_UNIT_NO_IS_IN_RANGE(Unit);
  pInst = _apInst[Unit];
  if (pInst) {
    U8 aCmd[4];

    ASSERT_SECTOR_INDEX_IS_IN_RANGE(pInst, SectorIndex);
    ASSERT_DEVICE_IS_SET(pInst);
    _InitIfRequired(pInst);
    //
    // Send Sector erase command to flash
    //
    SectorSize  = pInst->pDevice->SectorSize;
    Addr        = SectorSize * SectorIndex;
    Addr       += pInst->StartAddrUsed;
    aCmd[0] = CMD_SE;
    aCmd[1] = (U8)(Addr >> 16);
    aCmd[2] = (U8)(Addr >>  8);
    aCmd[3] = (U8)Addr;
    _EnableWrite(Unit);
    FS_NOR_SPI_HW_X_EnableCS(Unit);
    FS_NOR_SPI_HW_X_Write(Unit, aCmd, sizeof(aCmd));
    FS_NOR_SPI_HW_X_DisableCS(Unit);
    r = _WaitForEndOfOperation(Unit, pInst->NtimeoutErase);
  }
  return r;
}

/*********************************************************************
*
*       _GetSectorInfo
*
*   Function description
*     Physical layer function. Returns the offset and length of the given physical sector.
*/
static void _GetSectorInfo(U8 Unit, unsigned int SectorIndex, U32 * pOff, U32 * pLen) {
  U32 SectorOff;
  U32 SectorSize;
  NOR_SPI_INST * pInst;

  SectorOff  = 0;
  SectorSize = 0;
  ASSERT_UNIT_NO_IS_IN_RANGE(Unit);
  pInst = _apInst[Unit];
  if (pInst) {
    ASSERT_SECTOR_INDEX_IS_IN_RANGE(pInst, SectorIndex);
    ASSERT_DEVICE_IS_SET(pInst);
    _InitIfRequired(pInst);
    //
    // Compute result
    //
    SectorSize = pInst->pDevice->SectorSize;
    SectorOff  = SectorSize * SectorIndex;
  }
  if (pOff) {
    *pOff = SectorOff;
  }
  if (pLen) {
    *pLen = SectorSize;
  }
}

/*********************************************************************
*
*       _GetNumSectors
*
*   Function description
*     Physical layer function. Returns the number total number of physical sectors in the SPI flash.
*/
static int _GetNumSectors(U8 Unit) {
  int NumSectors;
  NOR_SPI_INST * pInst;

  NumSectors = 0;               // Set to indicate an error.
  ASSERT_UNIT_NO_IS_IN_RANGE(Unit);
  pInst = _apInst[Unit];
  if (pInst) {
    _InitIfRequired(pInst);
    NumSectors = pInst->NumSectors;
  }
  return NumSectors;
}

/*********************************************************************
*
*       _Configure
*
*   Function description
*     Physical layer function. Configures a single instance of the driver.
*
*/
static void _Configure(U8 Unit, U32 BaseAddr, U32 StartAddr, U32 NumBytes) {
  NOR_SPI_INST * pInst;

  FS_DEBUG_ASSERT(FS_MTYPE_DRIVER, StartAddr >= BaseAddr);
  pInst = _AllocIfRequired(Unit);
  if (pInst) {
    pInst->BaseAddr      = BaseAddr;
    pInst->StartAddrConf = StartAddr;
    pInst->NumBytes      = NumBytes;
    pInst->IsInited      &= ~1;           // The layer needs to be re-initialized.
  }
}

/*********************************************************************
*
*       _OnSelectPhy
*
*   Function description
*     Physical layer function. Called right after selection of the physical layer.
*/
static void _OnSelectPhy(U8 Unit) {
  _AllocIfRequired(Unit);
}

/*********************************************************************
*
*       _DeInit
*
*   Function description
*     Physical layer function.
*     This function frees up memory resources allocated for the instance of a physical layer.
*
*   Parameters
*     Unit  Physical layer number
*  
*/
static void _DeInit(U8 Unit) {
#if FS_SUPPORT_DEINIT
  NOR_SPI_INST * pInst;

  ASSERT_UNIT_NO_IS_IN_RANGE(Unit);
  pInst = _apInst[Unit];
  if (pInst) {
    if ((pInst->IsInited & (1 << 1))) {       // Has been manually configured?
      FS_Free((void *)((FLASH_DEVICE *)pInst->pDevice));
    }
    FS_Free((void *)pInst);
    _apInst[Unit] = 0;  //-Q-: pInst
  }
  _apInst[Unit] = NULL;
#else
  FS_USE_PARA(Unit);
#endif
}

/*********************************************************************
*
*      Public code
*
**********************************************************************
*/

/*********************************************************************
*
*       FS_NOR_SPI_Configure
*
*   Function description
*    This function can be used to manually configure the SPI NOR flash.
*    In some cases the identification on older ST M25 flashes may fail,
*    so that this function can be used to enable such a flash.
*    SPI NOR (M25 series) flashes have uniform sectors, which means
*    only one sector size is used for the whole flash.
*
*   Parameters
*     Unit          Physical layer number
*     SectorSize    The size of a sector given in bytes
*     NumSectors    The number of physical sectors available
*  
*/
void FS_NOR_SPI_Configure(U8 Unit, U32  SectorSize, U16  NumSectors) {
  NOR_SPI_INST * pInst;
  FLASH_DEVICE * pDevice;

  pInst = _AllocIfRequired(Unit);
  pDevice = (FLASH_DEVICE *)pInst->pDevice;
  if (pDevice == NULL) {
  pDevice = (FLASH_DEVICE *)FS_AllocZeroed(sizeof(FLASH_DEVICE));
  } else {
    if ((pInst->IsInited & (1 << 1)) == 0) {
      FS_DEBUG_ERROROUT((FS_MTYPE_DRIVER, "NOR_SPI: Could not configure SPI flash. Device has been auto-detected.\n"));
      return;
    }
  }
  pDevice->NumSectors = NumSectors;
  pDevice->SectorSize = SectorSize;
  pDevice->BytesPerPage  = 256;
  pInst->pDevice         = (const FLASH_DEVICE *)pDevice;
  pInst->IsInited       |= (1 << 1);      // Remember that we configured the device manually.
}

/*********************************************************************
*
*       FS_NOR_SPI_SetPageSize
*
*   Function description
*     Specifies the number of bytes in page. Typ. called to configure some SST SPI flashs (see [2]) which can write only 1 byte at a time.
*
*   Parameters
*     Unit            Physical layer number
*     BytesPerPage    The number of bytes to write with one page program command
*
*/
void FS_NOR_SPI_SetPageSize(U8 Unit, U16 BytesPerPage) {
  NOR_SPI_INST * pInst;
  FLASH_DEVICE * pDevice;

  pInst = _AllocIfRequired(Unit);
  pDevice = (FLASH_DEVICE *)pInst->pDevice;
  if (pDevice == NULL) {
    pDevice = (FLASH_DEVICE *)FS_AllocZeroed(sizeof(FLASH_DEVICE));
  } else {
    if ((pInst->IsInited & (1 << 1)) == 0) {
      FS_DEBUG_ERROROUT((FS_MTYPE_DRIVER, "NOR_SPI: Could not set page size. Device has been auto-detected.\n"));
      return;
    }
  }
  pDevice->BytesPerPage  = BytesPerPage;
  pInst->pDevice         = (const FLASH_DEVICE *)pDevice;
  pInst->IsInited       |= (1 << 1);      // Remember that we configured the device manually.
}

/*********************************************************************
*
*       Global data
*
**********************************************************************
*/
// ST M25P compliant Serial NOR flash
const FS_NOR_PHY_TYPE FS_NOR_PHY_ST_M25 = {
  _WriteOff,
  _ReadOff,
  _EraseSector,
  _GetSectorInfo,
  _GetNumSectors,
  _Configure,
  _OnSelectPhy,
  _DeInit
};            

/*************************** End of file ****************************/
