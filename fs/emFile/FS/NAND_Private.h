/*********************************************************************
*                SEGGER MICROCONTROLLER GmbH & Co. KG                *
*        Solutions for real time microcontroller applications        *
**********************************************************************
*                                                                    *
*        (c) 2003-2012     SEGGER Microcontroller GmbH & Co KG       *
*                                                                    *
*        Internet: www.segger.com    Support:  support@segger.com    *
*                                                                    *
**********************************************************************

**** emFile file system for embedded applications ****
emFile is protected by international copyright laws. Knowledge of the
source code may not be used to write a similar product. This file may
only be used in accordance with a license and should not be re-
distributed in any way. We appreciate your understanding and fairness.
----------------------------------------------------------------------
File        : NAND_Private.h
Purpose     : Private header file for the NAND flash driver.
---------------------------END-OF-HEADER------------------------------
*/
#ifndef _NAND_PRIVATE_H_            // Avoid multiple/recursive inclusion
#define _NAND_PRIVATE_H_

/*********************************************************************
*
*       Public code
*
**********************************************************************
*/
int FS__NAND_IsONFISupported(U8 Unit);
int FS__NAND_ReadONFIPara(U8 Unit, void * pPara);

#endif                               // Avoid multiple/recursive inclusion

/*************************** End of file ****************************/
