/*********************************************************************
*                SEGGER MICROCONTROLLER GmbH & Co. KG                *
*        Solutions for real time microcontroller applications        *
**********************************************************************
*                                                                    *
*        (c) 2003-2012     SEGGER Microcontroller GmbH & Co KG       *
*                                                                    *
*        Internet: www.segger.com    Support:  support@segger.com    *
*                                                                    *
**********************************************************************

**** emFile file system for embedded applications ****
emFile is protected by international copyright laws. Knowledge of the
source code may not be used to write a similar product. This file may
only be used in accordance with a license and should not be re-
distributed in any way. We appreciate your understanding and fairness.
----------------------------------------------------------------------
File        : NOR_HW_CFI_1x16.c
Purpose     : Low level flash layer handling CFI compliant flash chips
----------------------------------------------------------------------

Comments in this file refer to Intel's "Common Flash Interface
(CFI) and Command Sets" document as the "CFI spec."

Supported devices:
Any CFI-compliant flash in 16-bit mode



Literature:
[1] Intel's "Common Flash Interface (CFI) and Command Sets"
    Application Note 646, April 2000

[2] Spansion's "Common Flash Interface Version 1.4 Vendor Specific Extensions"
    Revision A, Amendment 0, March 22 - 2004

------------  END-OF-HEADER  -----------------------------------------
*/

#include "FS_Int.h"
#include "NOR_Private.h"

/*********************************************************************
*
*      Defines
*
**********************************************************************
*/

#define AMD_BYTES_PER_BUFFER                   0x20
#define INTEL_BYTES_PER_BUFFER                 0x20

/*********************************************************************
*
*      Flash command definitions (Intel Algo)
*/
#define INTEL_PROGRAM(BaseAddr, Addr, Data)           \
  *(volatile U16 FS_NOR_FAR *)((U32)BaseAddr)  = 0x40  ;     \
  *(volatile U16 FS_NOR_FAR *)((U32)Addr)      = Data

#define INTEL_READSTATUS(BaseAddr, Stat)              \
  *(volatile U16 FS_NOR_FAR *)((U32)BaseAddr)  = 0x70;       \
  Stat = *(volatile U16 FS_NOR_FAR *)((U32)BaseAddr)

#define INTEL_ERASEBLOCK(Addr)                        \
  *(volatile U16 FS_NOR_FAR *)((U32)Addr)            = 0x20; \
  *(volatile U16 FS_NOR_FAR *)((U32)Addr)            = 0xD0

#define INTEL_CLEARSTATUS(BaseAddr)                   \
  *(volatile U16 FS_NOR_FAR *)((U32)BaseAddr)  = 0x50

#define INTEL_RESET(BaseAddr)                         \
  *(volatile U16 FS_NOR_FAR*)((U32)BaseAddr)  = 0xFF

#define INTEL_WAIT_UNTIL_FINISHED(pDest, Status)      \
  {                                                   \
    Status = *pDest;      /* 2 reads are required. */ \
    do {                                              \
      Status = *pDest;                                \
      FS_NOR_DELAY();                             \
    } while ((Status & 0x80) == 0);                   \
  }

#define INTEL_UNLOCK(pDest, Status)                   \
  *pDest = 0x60;                                      \
  *pDest = 0xD0;                                      \
  INTEL_WAIT_UNTIL_FINISHED(pDest, Status);           \
  *pDest = 0xFF;      /* Back to "Read array" mode */


/*********************************************************************
*
*      Flash command definitions (AMD Algo)
*/
#define AMD_WRITECODE(BaseAddr)                                          \
  *(volatile U16 FS_NOR_FAR *)((U32)(BaseAddr + (0x5555UL << 1))) = 0xAA;           \
  *(volatile U16 FS_NOR_FAR *)((U32)(BaseAddr + (0x2AAAUL << 1))) = 0x55

#define AMD_PROGRAM(BaseAddr)                                            \
  AMD_WRITECODE(BaseAddr);                                               \
  *(volatile U16 FS_NOR_FAR *)((U32)(BaseAddr + (0x5555UL << 1))) = 0xA0

#define AMD_BLOCKERASE(BaseAddr)                                         \
  AMD_WRITECODE(BaseAddr);                                               \
  *(volatile U16 FS_NOR_FAR *)((U32)(BaseAddr + (0x5555UL << 1))) = 0x80;           \
  AMD_WRITECODE(BaseAddr)

#define AMD_RESET(BaseAddr)                                              \
  *(volatile U16 FS_NOR_FAR *)((U32)(BaseAddr + (0x0000UL << 1))) = 0xF0

#define AMD_BUFFER_LOAD(SectorAddr, NumItems)                            \
  *(volatile U16 FS_NOR_FAR *)((U32)(SectorAddr + (0x0000UL << 1))) = 0x25;         \
  *(volatile U16 FS_NOR_FAR *)((U32)(SectorAddr + (0x0000UL << 1))) = NumItems - 1  \


#define AMD_WRITE_BUFFER_TO_FLASH(SectorAddr)                            \
  *(volatile U16 FS_NOR_FAR *)((U32)(SectorAddr + (0x0000UL << 1))) = 0x29

#define AMD_AUTOSELECT(BaseAddr)                                         \
  AMD_WRITECODE(BaseAddr);                                               \
  *(volatile U16 FS_NOR_FAR *)((U32)(BaseAddr + (0x5555UL << 1))) = 0x90



/*********************************************************************
*
*      Flash command definitions (CFI)
*
*    Notes:
*      1   To determine if a device is CFI capable, we have to write 0x98
*          (CFI query) at address 0x55 (CFI spec, page 4).
*/
#define CFI_READCONFIG(BaseAddr)                                         \
  *(volatile U16 FS_NOR_FAR *)((U32)(BaseAddr + (0x55UL << 1)))  = 0x98

// Writing 0xFF puts the device into read array mode (CFI spec, page 15).
#define CFI_RESET(BaseAddr)                         \
  *(volatile U16 FS_NOR_FAR *)(BaseAddr)  = 0xFF;     \
  *(volatile U16 FS_NOR_FAR *)(BaseAddr)  = 0xF0

// Some flashes do not comply to the CFI specification reg. the command sequence,
// but contain CFI information. Such as SST SST39xxxx NOR flash series.
#define CFI_READCONFIG_NON_CONFORM(BaseAddr)                                         \
  AMD_WRITECODE(BaseAddr);                                               \
  *(volatile U16 FS_NOR_FAR *)((U32)(BaseAddr + (0x5555UL << 1)))  = 0x98


/*********************************************************************
*
*       Fixed defines
*
**********************************************************************
*/


/*********************************************************************
*
*      Types
*
**********************************************************************
*/

/*********************************************************************
*
*      Static data
*
**********************************************************************
*/
static U8 _IsCFICompliant;
static U8 _IsInited;
/*********************************************************************
*
*      Static code
*
**********************************************************************
*/

/*********************************************************************
*
*      Write/Erase routines for Intel compatible devices
*
**********************************************************************
*/


static int _INTEL_WritePage(U8 Unit, U32 BaseAddr, U32 SectorAddr, U32 DestAddr, const U16 FS_NOR_FAR * pSrc0, int NumItems) {
  U8  IsUnprotected = 0;
  U16 Status;
  volatile U16 FS_NOR_FAR * pDest;
  int i;

  FS_USE_PARA(Unit);
  FS_USE_PARA(BaseAddr);
  FS_USE_PARA(SectorAddr);
  pDest = (volatile U16 FS_NOR_FAR *)DestAddr;
RetryProgram:
  *pDest = 0xE8;                        // Write to buffer command
  INTEL_WAIT_UNTIL_FINISHED(pDest, Status);
  //
  // Write 16 items to buffer
  //
  *pDest = NumItems - 1;
  for (i = 0; i < NumItems; i++) {
    *(pDest + i) = *pSrc0++;
  }
  //
  // Start programming
  //
  *pDest = 0xD0;
  INTEL_WAIT_UNTIL_FINISHED(pDest, Status);
  //
  // Check if error occurred
  //
  Status &= 0x7E;
  if (Status) {
    *pDest = 0x50;                      // Clear status register
    *pDest = 0xFF;                      // Back to "Read array" mode
    if (Status & (1 << 1)) {
      if (IsUnprotected == 0) {
        INTEL_UNLOCK(pDest, Status);
        IsUnprotected = 1;
        goto RetryProgram;
      }
    }
    return -(int)Status;
  }
  *pDest = 0xFF;                        // Back to "Read array" mode
  pDest += 16;
  return 0;
}

/*********************************************************************
*
*      _INTEL_EraseSector
*
* Purpose:
*   obvious
* Return-value:
*   0       O.K., sector is erased
*   other   error, sector may not be erased
*/
static int _INTEL_EraseSector(U8 Unit, U32 BaseAddr, U32 SectorAddr) {
  volatile U16 FS_NOR_FAR * pDest;
  U16 Status;
  U8  IsUnprotected = 0;

  FS_USE_PARA(Unit);
  pDest= (volatile U16 FS_NOR_FAR *)SectorAddr;
  //
  // Start erase operation
  //
RetryErase:
  FS_NOR_DI();  /* Disable interrupts, to be on the safe side  */
  INTEL_ERASEBLOCK(pDest);
  INTEL_WAIT_UNTIL_FINISHED(pDest, Status);
  FS_NOR_EI();  /* Enable interrupts */
  //
  // Check if error occurred
  //
  Status &= 0x7E;
  if (Status) {
    *pDest = 0x50;                      // Clear status register
    *pDest = 0xFF;                      // Back to "Read array" mode
    if (Status & (1 << 1)) {
      if (IsUnprotected == 0) {
        FS_NOR_DI();  /* Disable interrupts, to be on the safe side  */
        INTEL_UNLOCK(pDest, Status);
        FS_NOR_EI();  /* Enable interrupts */
        IsUnprotected = 1;
        goto RetryErase;
      }
    }
    return -(int)Status;
  }
  FS_NOR_DI();  /* Disable interrupts, to be on the safe side  */
  INTEL_RESET(BaseAddr);                     // Back to "Read array" mode
  FS_NOR_EI();  /* Enable interrupts */
  return 0;
}

/*********************************************************************
*
*      _INTEL_Write
*/
static char _INTEL_Write(U8 Unit, U32 BaseAddr, U32 SectorAddr, U32 DestAddr, const U16 FS_NOR_FAR * pSrc0, int NumItems) {
  int Status;
  volatile U16 FS_NOR_FAR * pDest;
  U16          FS_NOR_FAR * pSrc;
  U16                       Data16;

  FS_USE_PARA(Unit);
  FS_USE_PARA(SectorAddr);
  pSrc  = (U16 FS_NOR_FAR *)pSrc0;
  pDest = (volatile U16 FS_NOR_FAR *)DestAddr;
  while(NumItems > 0) {
    Data16 =  *pSrc;
    FS_NOR_DI();  /* Disable interrupts, to be on the safe side  */
    INTEL_UNLOCK(pDest, Status);
    INTEL_PROGRAM(BaseAddr, pDest, Data16);
    do {
      INTEL_READSTATUS(BaseAddr, Status);
    } while ((Status & (1<<7))==0);  /* Wait till flash is no longer busy */
    INTEL_READSTATUS(BaseAddr, Status);
    INTEL_RESET(BaseAddr);
    INTEL_CLEARSTATUS(BaseAddr);
    INTEL_RESET(BaseAddr);
    FS_NOR_EI();  /* Enable interrupts */
    if (Status & 0x1a) {
      return 1;
    }
    if (*pDest != Data16) {
      return 1;
    }
    NumItems--;
    ++pDest;
    ++pSrc;

  }
  return 0;
}

/*********************************************************************
*
*      _INTEL_WriteFast
*/
static char _INTEL_WriteFast(U8 Unit, U32 BaseAddr, U32 SectorAddr, U32 DestAddr, const U16 FS_NOR_FAR * pSrc0, int NumItems) {
  const U16 FS_NOR_FAR * pSrc;
  int                    NumItems2Write;

  FS_USE_PARA(Unit);
  pSrc  = (const U16 FS_NOR_FAR *)pSrc0;
  while(NumItems > 0) {
    if (DestAddr & (INTEL_BYTES_PER_BUFFER - 1)) {
      NumItems2Write = (INTEL_BYTES_PER_BUFFER - (DestAddr & (INTEL_BYTES_PER_BUFFER - 1))) >> 1;
      NumItems2Write = MIN(NumItems2Write, NumItems);
      FS_NOR_DI();  /* Disable interrupts, to be on the safe side  */
      if (_INTEL_WritePage(Unit, BaseAddr, SectorAddr, DestAddr, pSrc, NumItems2Write)) {
        FS_NOR_EI();  /* Enable interrupts */
        return 1;  
      }
      FS_NOR_EI();  /* Enable interrupts */
      pSrc     += NumItems2Write;
      NumItems -= NumItems2Write;
      DestAddr += (NumItems2Write << 1);
    }
    if (NumItems) {
      do {
        NumItems2Write = MIN(NumItems, (INTEL_BYTES_PER_BUFFER >> 1));
        FS_NOR_DI();  /* Disable interrupts */
        if (_INTEL_WritePage(Unit, BaseAddr, SectorAddr, DestAddr, pSrc, NumItems2Write)) {
          FS_NOR_EI();  /* Enable interrupts */
          return 1;
        }
        FS_NOR_EI();  /* Enable interrupts */
        pSrc     += NumItems2Write;
        NumItems -= NumItems2Write;
        DestAddr += (NumItems2Write << 1);
      } while(NumItems);
    }
  }
  return 0;
}


/*********************************************************************
*
*      Write/Erase routines for AMD compatible devices
*
**********************************************************************
*/

/*********************************************************************
*
*      _AMD_WritePage
*
* Purpose:
*   Writes a complete page in one program cycle a page is in general 32 byte wide.
*
* Return value:
*   0       O.K., sector is ereased
*   other   error, sector may not be written correctly.
*/
static int _AMD_WritePage(U8 Unit, U32 BaseAddr, U32 SectorAddr, U32 DestAddr, const U16 FS_NOR_FAR * pSrc0, int NumItems) {
  volatile U16 FS_NOR_FAR * pSrc;
  volatile U16 FS_NOR_FAR * pDest;
  volatile U16 FS_NOR_FAR * pStatus;
  U16           d0, d1;
  int i;

  FS_USE_PARA(Unit);
  FS_USE_PARA(SectorAddr);
  pSrc        = (volatile U16 FS_NOR_FAR *)pSrc0;
  pDest       = (volatile U16 FS_NOR_FAR *)DestAddr;
  //
  // Write 16 items to buffer
  //
  AMD_WRITECODE(BaseAddr);
  AMD_BUFFER_LOAD(SectorAddr, NumItems);           // "Write to Buffer" command
  for (i = 0; i < NumItems; i++) {
    *(pDest + i) = *(pSrc + i);
  }
  //
  // Program buffer
  //
  AMD_WRITE_BUFFER_TO_FLASH(SectorAddr);          // "Program Buffer to Flash" command
  //
  // Wait for flash operation to finish by reading status and testing toggle bit
  //
  pStatus = pDest + (NumItems - 1);                         // Monitor last address to be programmed
  do {
    d0 = *pStatus;
    d1 = *pStatus;
    if (d0 == d1) {
      break;
    }
    //
    // Check for error
    //
    if (d0 & (1 << 5)) {
      d0 = *pStatus;
      if (d0 == d1) {
        break;
      }
      AMD_RESET(BaseAddr);                // Reset flash to read array mode
      FS_NOR_EI();                        // Enable interrupts
      return 4;                           // Program error
    }
    FS_NOR_DELAY();
  } while (1);
  return 0;
}

/*********************************************************************
*
*      _AMD_EraseSector
*
* Purpose:
*   Erases one sector
* Return value:
*   0       O.K., sector is ereased
*   other   error, sector may not be erased
*/
static int _AMD_EraseSector(U8 Unit, U32 BaseAddr, U32 SectorAddr) {
  volatile U16 FS_NOR_FAR * pB;
  volatile U16 FS_NOR_FAR * pStatus;
  U16  Status;

  FS_USE_PARA(Unit);
  pB       = (volatile U16 FS_NOR_FAR *)SectorAddr;
  pStatus  = (volatile U16 FS_NOR_FAR *)SectorAddr;
  FS_NOR_DI();  /* Disable interrupts, to be on the safe side  */
  AMD_BLOCKERASE(BaseAddr);
  *pB = 0x30;
  while (Status = *pStatus, (Status ^ *pStatus) & (1 << 6)) {      /* Check toggle bit */
    FS_NOR_DELAY();
  }
  if (*pB != 0xffff) {
    AMD_RESET(BaseAddr);
    FS_NOR_EI();  /* Enable interrupts */
    return 1;
  }
  FS_NOR_EI();  /* Enable interrupts */
  return 0;
}

/*********************************************************************
*
*      _AMD_Write
*
* Purpose:
*   Writes data into flash
* Return value:
*   0       O.K., data has been written
*   other   error
*/
static char _AMD_Write(U8 Unit, U32 BaseAddr, U32 SectorAddr, U32 DestAddr, const U16 FS_NOR_FAR * pSrc0, int NumItems) {
  int i;
  U16          FS_NOR_FAR * pSrc;
  volatile U16 FS_NOR_FAR * pDest;
  U16 a;
  U16 Data16;

  FS_USE_PARA(Unit);
  FS_USE_PARA(SectorAddr);
  pSrc  = (U16 FS_NOR_FAR *)pSrc0;
  pDest = (volatile U16 FS_NOR_FAR *)DestAddr;
  FS_NOR_DI();  /* Disable interrupts, to be on the safe side  */
  AMD_RESET(BaseAddr);
  FS_NOR_EI();  /* Enable interrupts */
  while(NumItems > 0) {
    i = 0;
    Data16 =  *pSrc;
    FS_NOR_DI();  /* Disable interrupts, to be on the safe side  */
    AMD_PROGRAM(BaseAddr);
    *pDest = Data16;
    /* Wait for operation to finish */
    while (a = *pDest, (a ^ *pDest) & (1 << 6)) {     /* Check toggle bit */
      if (++i > 10000) {                              /* Check timeout */
        AMD_RESET(BaseAddr);
        FS_NOR_EI();  /* Enable interrupts */
        return 1;
      }
      FS_NOR_DELAY();
    }
    /* Verify */
    if (*pDest != Data16) {
      AMD_RESET(BaseAddr);
      FS_NOR_EI();  /* Enable interrupts */
      return 1;
    }
    FS_NOR_EI();  /* Enable interrupts */
    NumItems--;
    ++pDest;
    ++pSrc;
  }
  return 0;
}

/*********************************************************************
*
*      _AMD_WriteFast
*
* Purpose:
*   Writes data into flash
* Return value:
*   0       O.K., data has been written
*   other   error
*/
static char _AMD_WriteFast(U8 Unit, U32 BaseAddr, U32 SectorAddr, U32 DestAddr, const U16 FS_NOR_FAR* pSrc0, int NumItems) {
  const U16 FS_NOR_FAR * pSrc;
  int                    NumItems2Write;

  FS_USE_PARA(Unit);
  pSrc  = (const U16 FS_NOR_FAR *)pSrc0;
  FS_NOR_DI();  /* Disable interrupts, to be on the safe side  */
  AMD_RESET(BaseAddr);
  FS_NOR_EI();  /* Enable interrupts */
  while(NumItems > 0) {
    if (DestAddr & (AMD_BYTES_PER_BUFFER - 1)) {
      NumItems2Write = (AMD_BYTES_PER_BUFFER - (DestAddr & (AMD_BYTES_PER_BUFFER - 1))) >> 1;
      NumItems2Write = MIN(NumItems2Write, NumItems);
      FS_NOR_DI();  /* Disable interrupts, to be on the safe side  */
      if (_AMD_WritePage(Unit, BaseAddr, SectorAddr, DestAddr, pSrc, NumItems2Write)) {
        FS_NOR_EI();  /* Enable interrupts */
        return 1;  
      }
      FS_NOR_EI();  /* Enable interrupts */
      pSrc     += NumItems2Write;
      NumItems -= NumItems2Write;
      DestAddr += (NumItems2Write << 1);
    }
    if (NumItems) {
      do {
        NumItems2Write = MIN(NumItems, (AMD_BYTES_PER_BUFFER >> 1));
       FS_NOR_DI();  /* Disable interrupts */
       if (_AMD_WritePage(Unit, BaseAddr, SectorAddr, DestAddr, pSrc, NumItems2Write)) {
          FS_NOR_EI();  /* Enable interrupts */
          return 1;
        }
        FS_NOR_EI();  /* Enable interrupts */
        pSrc     += NumItems2Write;
        NumItems -= NumItems2Write;
        DestAddr += (NumItems2Write << 1);
      } while(NumItems);
    }
  }
  return 0;
}

/*********************************************************************
*
*       _Read
*
* Purpose:
*   Reads data from the given address of the flash.
*/
static int _Read(U8 Unit, void * pDest, U32 Addr, U32 NumBytes) {
  FS_USE_PARA(Unit);
  FS_MEMCPY(pDest, (const void *)Addr, NumBytes);
  return 0;
}



/*********************************************************************
*
*      FS_NOR_CFI_ReadCFI_1x16
*
*  Function description
*    Reads CFI data from hardware into buffer.
*    Note that every 16-bit value from flash contains just a single byte.
*/
void FS_NOR_CFI_ReadCFI_1x16(U8 Unit, U32 BaseAddr, U32 Off, U8 * pData, unsigned NumItems) {
  volatile U16 FS_NOR_FAR * pAddr;
  
  FS_USE_PARA(Unit);
  //
  // We initially need to check whether the flash is fully CFI compliant.
  //
  if (_IsInited == 0) {
    U8 aData[3];
    pAddr = (volatile U16 FS_NOR_FAR *)(BaseAddr + (0x10 << 1));   // at Offset 0x10 (x16) we retrieve the 'QRY' information.
    FS_NOR_DI();
    CFI_READCONFIG(BaseAddr);
    aData[0] = (U8)*pAddr++;
    aData[1] = (U8)*pAddr++;
    aData[2] = (U8)*pAddr++;
    CFI_RESET(BaseAddr);
    FS_NOR_EI();
    if (   (aData[0] == 'Q')
        && (aData[1] == 'R')
        && (aData[2] == 'Y')) {
        _IsCFICompliant = 1;
    }
    _IsInited = 1;
  }
  pAddr = (volatile U16 FS_NOR_FAR *)(BaseAddr + (Off << 1));
  FS_NOR_DI();
  //
  // Write the correct CFI-query sequence
  //
  if (_IsCFICompliant) {
    CFI_READCONFIG(BaseAddr);
  } else {
    CFI_READCONFIG_NON_CONFORM(BaseAddr);
  }
  //
  // Read the data
  //
  do {
    *pData++ = (U8)*pAddr++;  // Only the low byte of the CFI data is relevant
  } while(--NumItems);
  //
  // Perform a reset, which means, return from CFI mode to normal mode
  // 
  CFI_RESET(BaseAddr);
  FS_NOR_EI();
}


/*********************************************************************
*
*      Public data
*
**********************************************************************
*/
const FS_NOR_PROGRAM_HW FS_NOR_Program_Intel_1x16 = {
  _Read,  
  _INTEL_EraseSector,
  _INTEL_Write
};

const FS_NOR_PROGRAM_HW FS_NOR_Program_AMD_1x16 = {
  _Read,  
  _AMD_EraseSector,
  _AMD_Write
};

const FS_NOR_PROGRAM_HW FS_NOR_Program_IntelFast_1x16 = {
  _Read,  
  _INTEL_EraseSector,
  _INTEL_WriteFast
};

const FS_NOR_PROGRAM_HW FS_NOR_Program_AMDFast_1x16 = {
  _Read,  
  _AMD_EraseSector,
  _AMD_WriteFast
};

/*************************** End of file ****************************/
