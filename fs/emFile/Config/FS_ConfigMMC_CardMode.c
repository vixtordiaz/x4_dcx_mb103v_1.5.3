/*********************************************************************
*                SEGGER MICROCONTROLLER GmbH & Co. KG                *
*        Solutions for real time microcontroller applications        *
**********************************************************************
*                                                                    *
*        (c) 2003-2012     SEGGER Microcontroller GmbH & Co KG       *
*                                                                    *
*        Internet: www.segger.com    Support:  support@segger.com    *
*                                                                    *
**********************************************************************

**** emFile file system for embedded applications ****
emFile is protected by international copyright laws. Knowledge of the
source code may not be used to write a similar product. This file may
only be used in accordance with a license and should not be re-
distributed in any way. We appreciate your understanding and fairness.
----------------------------------------------------------------------
File        : FS_ConfigMMC_CardMode.c
Purpose     : Configuration functions for FS with MMC/SD card mode driver
---------------------------END-OF-HEADER------------------------------
*/

#include "FS.h"

/*********************************************************************
*
*       Defines, configurable
*
*       This section is the only section which requires changes 
*       using the MMC/SD card mode disk driver as a single device.
*
**********************************************************************
*/
// Size constants, from emFile User Guide v3.26 Rev 3 Pg. 417 Section 10.1.6 Dynamic RAM requirements
// and from stepping through FS_Init while watching "FS_NumBytesAllocated"
#define FILE_SIZE                   16          // FS_FILE structure size
#define OBJ_SIZE                    44          // FS_FILE_OBJ structure size
#define SECTOR_BUFFER_SIZE          1560        // 3 * (8 + sector size(512))
#define VOLUME_SIZE                 28
#define VOLUME_FORCE_UNOUNT         52
#define JOURNAL_MOUNT               236
//***************************************************************************

#define MAX_FILE_COUNT              10          // Configure for max simultaneous open files          
#define FS_INIT_SIZE                (VOLUME_SIZE+VOLUME_FORCE_UNOUNT+JOURNAL_MOUNT+SECTOR_BUFFER_SIZE)
#define FS_FILE_SIZE                (MAX_FILE_COUNT*(FILE_SIZE+OBJ_SIZE))
#define FS_RESERVE_SIZE             596         // Reserved bytes for saftey buffer

#define ALLOC_SIZE                  (FS_INIT_SIZE+FS_FILE_SIZE+FS_RESERVE_SIZE) // 3072 total ALLOC_SIZE in bytes = 2476 + 596

/*********************************************************************
*
*       Static data
*
**********************************************************************
*/

//
// Memory pool used for semi-dynamic allocation.
//
#ifdef __ICCARM__
  #pragma location="FS_RAM"
  static __no_init U32 _aMemBlock[ALLOC_SIZE / 4];
#endif
#ifdef __CC_ARM
  U32 static _aMemBlock[ALLOC_SIZE / 4] __attribute__ ((section ("FS_RAM"), zero_init));
#endif
#if (!defined(__ICCARM__) && !defined(__CC_ARM))
  static U32 _aMemBlock[ALLOC_SIZE / 4];
#endif

/*********************************************************************
*
*       Public code
*
*       This section does not require modifications in most systems.
*
**********************************************************************
*/

/*********************************************************************
*
*       FS_X_AddDevices
*
*  Function description
*    This function is called by the FS during FS_Init().
*    It is supposed to add all devices, using primarily FS_AddDevice().
*    
*  Note
*    (1) Other API functions
*        Other API functions may NOT be called, since this function is called
*        during initialization. The devices are not yet ready at this point.
*/
void FS_X_AddDevices(void) {
  FS_AssignMemory(&_aMemBlock[0], sizeof(_aMemBlock));
  FS_AddDevice(&FS_MMC_CardMode_Driver);
  FS_MMC_CM_Allow4bitMode(0, 1);
  FS_SetFileWriteMode(FS_WRITEMODE_FAST);
}

/*********************************************************************
*
*       FS_X_GetTimeDate
*
*  Description:
*    Current time and date in a format suitable for the file system.
*
*    Bit 0-4:   2-second count (0-29)
*    Bit 5-10:  Minutes (0-59)
*    Bit 11-15: Hours (0-23)
*    Bit 16-20: Day of month (1-31)
*    Bit 21-24: Month of year (1-12)
*    Bit 25-31: Count of years from 1980 (0-127)
*
*/
U32 FS_X_GetTimeDate(void) {
  U32 r;
  U16 Sec, Min, Hour;
  U16 Day, Month, Year;

  Sec   = 0;        // 0 based.  Valid range: 0..59
  Min   = 0;        // 0 based.  Valid range: 0..59
  Hour  = 0;        // 0 based.  Valid range: 0..23
  Day   = 1;        // 1 based.    Means that 1 is 1. Valid range is 1..31 (depending on month)
  Month = 1;        // 1 based.    Means that January is 1. Valid range is 1..12.
  Year  = 0;        // 1980 based. Means that 2007 would be 27.
  r   = Sec / 2 + (Min << 5) + (Hour  << 11);
  r  |= (U32)(Day + (Month << 5) + (Year  << 9)) << 16;
  return r;
}

/*************************** End of file ****************************/
