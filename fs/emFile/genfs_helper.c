/**
  **************** (C) COPYRIGHT 2011 SCT Performance, LLC *********************
  * File Name          : genfs_helper.c
  * Device / System    : SCT products using the "CommonCode" platform
  * Author             : Quyen Leba
  *
  * Version            : 1 
  * Date               : 05/02/2011
  * Description        : 
  *                    : 
  *
  *
  * History            : 05/02/2011 M. Davis
  *                    :   Add SCT Copyright header,
  *
  ******************************************************************************
  */

#include <arch/gentype.h>
#include <common/statuscode.h>
#include <common/crc32.h>

#define LISTING_FOLDER_MAX_LENGTH       (64+1)
#define FS_JOURNAL_SIZE                 (20*1024)

enum
{
    FOLDER_INDEX_DEFAULT    = 0,
    FOLDER_INDEX_USER       = 1,
    FOLDER_INDEX_MAX        = GENFS_TOTAL_FOLDER,
};

const u8 *folder_list[GENFS_TOTAL_FOLDER] =
{
    [FOLDER_INDEX_DEFAULT] = GENFS_DEFAULT_FOLDER_PATH,
    [FOLDER_INDEX_USER] = GENFS_USER_FOLDER_PATH,
};

struct
{
    FS_FIND_DATA f_find_info;
    u8  *folder_name;
    u8  entry_buffer[GENFS_ENTRYNAMELENGTH_MAX+1];
    u32 total_entry_found;
    struct
    {
        u8  is_find_init    : 1;
        u8  is_overflow     : 1;
        u8  listallfolders  : 1;
        u8  reserved        : 5;
    }status;
    u8  current_listing_folder_index;
}genfs_helper_info = 
{
    .folder_name = NULL,
    .total_entry_found = 0,
    .status.is_find_init = 0,
    .status.is_overflow = 0,
    .status.listallfolders = 1,
    .current_listing_folder_index = FOLDER_INDEX_DEFAULT,
};

//------------------------------------------------------------------------------
// Make correction to a filename (accept default/user path)
// If no path, use default
// Inputs:  u8  *source (filename)
//          u32 maxnamelength (max *correctname can hold)
// Output:  u8  *correctname
// Return:  u8  status
// Engineer: Quyen Leba
//------------------------------------------------------------------------------
u8 genfs_generalpathcorrection(u8 *source, u32 maxnamelength, u8 *correctname)
{
    if(genfs_checkfilehaspath(source) == S_SUCCESS)
    {
        //already has correct path
        if (strlen((char*)source) >= maxnamelength)
        {
            return S_BADCONTENT;
        }
        strcpy((char*)correctname,(char*)source);
    }
    else
    {
        //no path, insert default path
        if (strlen((char*)source) +
            strlen((char*)GENFS_DEFAULT_FOLDER_PATH) >= maxnamelength)
        {
            return S_BADCONTENT;
        }
        strcpy((char*)correctname,GENFS_DEFAULT_FOLDER_PATH);
        if (source[0] == '/' || source[0] == '\\')
        {
            strcat((char*)correctname,(char*)&source[1]);
        }
        else
        {
            strcat((char*)correctname,(char*)source);
        }
    }

    return S_SUCCESS;
}

//------------------------------------------------------------------------------
// Make correction to a filename (accept default/user path)
// If no path, use default
// Inputs:  u8  *filename
//          
// Return:  u8  status
// Engineer: Patrick Downs
//------------------------------------------------------------------------------
u8 genfs_checkfilehaspath(u8 *filename)
{
    if (strstr((char*)filename,GENFS_DEFAULT_FOLDER_PATH) ||
        strstr((char*)filename,GENFS_USER_FOLDER_PATH))
    {
        return S_SUCCESS; //already has correct path
    }

    return S_FAIL;
}

//------------------------------------------------------------------------------
// Make correction to a filename to default folder
// Inputs:  u8  *source (filename)
//          u32 maxnamelength (max *correctname can hold)
// Output:  u8  *correctname
// Return:  u8  status
// Engineer: Quyen Leba
//------------------------------------------------------------------------------
u8 genfs_defaultpathcorrection(u8 *source, u32 maxnamelength, u8 *correctname)
{
    if(!isgraph(source[0]))
        return S_INPUT;
    
    if (strstr((char*)source,GENFS_DEFAULT_FOLDER_PATH))
    {
        //already has correct path
        if (strlen((char*)source) >= maxnamelength)
        {
            return S_BADCONTENT;
        }
        strcpy((char*)correctname,(char*)source);
    }
    else if (strstr((char*)source,GENFS_USER_FOLDER_PATH))
    {
        //already have a different path
        return S_ERROR;
    }
    else
    {
        //no path, insert default path
        if (strlen((char*)source) +
            strlen((char*)GENFS_DEFAULT_FOLDER_PATH) >= maxnamelength)
        {
            return S_BADCONTENT;
        }
        strcpy((char*)correctname,GENFS_DEFAULT_FOLDER_PATH);
        if (source[0] == '/' || source[0] == '\\')
        {
            strcat((char*)correctname,(char*)&source[1]);
        }
        else
        {
            strcat((char*)correctname,(char*)source);
        }
    }

    return S_SUCCESS;
}

//------------------------------------------------------------------------------
// Make correction to a filename to user folder
// Inputs:  u8  *source (filename)
//          u32 maxnamelength (max *correctname can hold)
// Output:  u8  *correctname
// Return:  u8  status
// Engineer: Quyen Leba
//------------------------------------------------------------------------------
u8 genfs_userpathcorrection(u8 *source, u32 maxnamelength, u8 *correctname)
{
    if (strstr((char*)source,GENFS_USER_FOLDER_PATH))
    {
        //already has correct path
        if (strlen((char*)source) >= maxnamelength)
        {
            return S_BADCONTENT;
        }
        strcpy((char*)correctname,(char*)source);
    }
    else if (strstr((char*)source,GENFS_DEFAULT_FOLDER_PATH))
    {
        //already have a different path
        return S_ERROR;
    }
    else
    {
        //no path, insert user path
        if (strlen((char*)source) +
            strlen((char*)GENFS_USER_FOLDER_PATH) >= maxnamelength)
        {
            return S_BADCONTENT;
        }
        strcpy((char*)correctname,GENFS_USER_FOLDER_PATH);
        if (source[0] == '/' || source[0] == '\\')
        {
            strcat((char*)correctname,(char*)&source[1]);
        }
        else
        {
            strcat((char*)correctname,(char*)source);
        }
    }

    return S_SUCCESS;
}

//------------------------------------------------------------------------------
// Init filesystem
// Return:  u8  status
// Engineer: Quyen Leba
// TODOQ: add callback when file system is invalid
//------------------------------------------------------------------------------
u8 genfs_init()
{
    u8  doformat = 0;

    if (genfs_reinit() != S_SUCCESS)
    {
        if (doformat)
        {
            doformat = 0;
            genfs_format();
            if (genfs_reinit() != S_SUCCESS)
            {
                return S_FAIL;
            }
        }
        else
        {
            return S_FAIL;
        }
    }
    
    if (doformat)
    {
        doformat = 0;
        genfs_format();
        if (genfs_reinit() != S_SUCCESS)
        {
            return S_FAIL;
        }
    }
    return S_SUCCESS;
}

//------------------------------------------------------------------------------
// Reinit filesystem
// Return:  u8  status
// Engineer: Quyen Leba
//------------------------------------------------------------------------------
u8 genfs_reinit()
{
    u32 volumesize;
    u8  status;

    FS_Init();
    FS_FAT_SupportLFN();    
    
    if (FS_IsHLFormatted(GENFS_VOLUME_NAME) != 1)
    {
        return S_FAIL;
    }

    if (FS_IsVolumeMounted(GENFS_VOLUME_NAME) != 1)
    {
        return S_FAIL;
    }
    
    // Below configurations are required for Journaling
    FS_ConfigUpdateDirOnWrite(1);
    
    volumesize = FS_GetVolumeSize(GENFS_VOLUME_NAME);
    if (volumesize == 0)
    {
        return S_FAIL;
    }

    if (genfs_isfolderexist("\\"GENFS_DEFAULT_FOLDER_NAME) != S_SUCCESS)
    {
        //create default folder if not exist
        status = genfs_createdir(GENFS_DEFAULT_FOLDER_NAME);
        if (status != S_SUCCESS)
        {
            return status;
        }
    }
    if (genfs_isfolderexist("\\"GENFS_USER_FOLDER_NAME) != S_SUCCESS)
    {
        //create user folder if not exist
        status = genfs_createdir(GENFS_USER_FOLDER_NAME);
        if (status != S_SUCCESS)
        {
            return status;
        }
    }
    
    return S_SUCCESS;
}

//------------------------------------------------------------------------------
// Deinit filesystem
// Return:  u8  status
// Engineer: Quyen Leba
//------------------------------------------------------------------------------
u8 genfs_deinit()
{
    FS_DeInit();
    return S_SUCCESS;
}

//------------------------------------------------------------------------------
// Create journal for file system
// Return:  u8  status
// Engineer: Quyen Leba
//------------------------------------------------------------------------------
u8 genfs_create_journal()
{
    u32 response;

    response = FS_JOURNAL_Create(GENFS_VOLUME_NAME, FS_JOURNAL_SIZE);
    if (response != 0 && response != 1) //0: created, 1: existed
    {
        return S_FAIL;
    }
    return S_SUCCESS;
}

//------------------------------------------------------------------------------
// Delete file system journal
// Return:  u8  status
// Engineer: Quyen Leba
//------------------------------------------------------------------------------
u8 genfs_delete_journal()
{
    FS_JOURNAL_Delete(GENFS_VOLUME_NAME);
    return S_SUCCESS;
}

//------------------------------------------------------------------------------
// Format filesystem
// Return:  u8  status
// Engineer: Quyen Leba
//------------------------------------------------------------------------------
u8 genfs_format()
{
    u32 freespace;
    u32 response;
    u8  status;

    status = S_FAIL;
    freespace = 0;

    genfs_delete_journal();

    response = FS_FormatSD(GENFS_VOLUME_NAME);
    if (response != 0)
    {
        goto genfs_format_done;
    }

    if (genfs_create_journal() != S_SUCCESS)
    {
        goto genfs_format_done;
    }

    FS_FAT_SupportLFN();
    freespace = FS_GetVolumeFreeSpace(GENFS_VOLUME_NAME);
    if (freespace == 0)
    {
        status = S_FAIL;
        goto genfs_format_done;
    }

    status = genfs_createdir(GENFS_DEFAULT_FOLDER_NAME);
    if (status != S_SUCCESS)
    {
        goto genfs_format_done;
    }
    status = genfs_createdir(GENFS_USER_FOLDER_NAME);
    if (status != S_SUCCESS)
    {
        goto genfs_format_done;
    }

    status = S_SUCCESS;

genfs_format_done:

    return status;
}

//------------------------------------------------------------------------------
// Get filesystem free space
// Return:  u32 freespace (in kBytes)
// Engineer: Quyen Leba
//------------------------------------------------------------------------------
u32 genfs_getfreespace()
{
    u32 freespace;

    freespace = FS_GetVolumeFreeSpaceKB(GENFS_VOLUME_NAME);

    return freespace;
}

//------------------------------------------------------------------------------
// Get filesystem capacity
// Return:  u32 capacity
// Engineer: Quyen Leba
//------------------------------------------------------------------------------
u32 genfs_getcapacity()
{
    u32 capacity;

    capacity = FS_GetVolumeSizeKB(GENFS_VOLUME_NAME);

    return capacity;
}

//------------------------------------------------------------------------------
// Open a file
// Inputs:  const u8 *filename
//          const u8 *attrib ("r", "b", etc)
// Return:  F_FILE *fptr
// Engineer: Quyen Leba
//------------------------------------------------------------------------------
F_FILE * genfs_general_openfile(const u8 *filename, const u8 *attrib)
{
    u8  correctedfilename[512];

    if (genfs_generalpathcorrection((u8*)filename,sizeof(correctedfilename),
                                    correctedfilename) == S_SUCCESS)
    {
        return fopen((char*)correctedfilename,(char*)attrib);
    }
    return NULL;
}

//------------------------------------------------------------------------------
// Open a file
// Inputs:  const u8 *filename
//          const u8 *attrib ("r", "b", etc)
// Return:  F_FILE *fptr
// Engineer: Quyen Leba
//------------------------------------------------------------------------------
F_FILE * genfs_default_openfile(const u8 *filename, const u8 *attrib)
{
    u8  correctedfilename[512];

    if (genfs_defaultpathcorrection((u8*)filename,sizeof(correctedfilename),
                                    correctedfilename) == S_SUCCESS)
    {
        return fopen((char*)correctedfilename,(char*)attrib);
    }
    return NULL;
}

//------------------------------------------------------------------------------
// Open a file in user folder
// Inputs:  const u8 *filename
//          const u8 *attrib ("r", "b", etc)
// Return:  F_FILE *fptr
// Engineer: Quyen Leba
//------------------------------------------------------------------------------
F_FILE * genfs_user_openfile(const u8 *filename, const u8 *attrib)
{
    u8  correctedfilename[512];

    if (genfs_userpathcorrection((u8*)filename,sizeof(correctedfilename),
                                 correctedfilename) == S_SUCCESS)
    {
        return fopen((char*)correctedfilename,(char*)attrib);
    }
    return NULL;
}

//------------------------------------------------------------------------------
// Close a file
// Input:   F_FILE *fptr
// Return:  none
// Engineer: Quyen Leba
//------------------------------------------------------------------------------
void genfs_closefile(F_FILE *fptr)
{
    if (fptr)
    {
        fclose(fptr);
    }
}

//------------------------------------------------------------------------------
// Check if folder exist
// Input:   u8  *folder_name
// Return:  u8  status
// Engineer: Quyen Leba
//------------------------------------------------------------------------------
u8 genfs_isfolderexist(const u8 *folder_name)
{
    FS_DIR *dir;

    dir = FS_OpenDir((char*)folder_name);
    if (dir)
    {
        FS_CloseDir(dir);
        return S_SUCCESS;
    }
    return S_FAIL;
}

//------------------------------------------------------------------------------
// Delete a file
// Input:   u8  *filename
// Return:  u8  status
// Engineer: Quyen Leba
//------------------------------------------------------------------------------
u8 genfs_deletefile(u8 *filename)
{
    u8  correctedfilename[512];

    if (genfs_generalpathcorrection(filename,sizeof(correctedfilename),
                                    correctedfilename) == S_SUCCESS)
    {
        if (fdelete((char*)correctedfilename) == 0)
        {
            return S_SUCCESS;
        }
    }
    
    return S_FAIL;
}

//------------------------------------------------------------------------------
// Rename a file
// Inputs:  u8  *existingfilename
//          u8  *newfilename
// Return:  u8  status
// Engineer: Quyen Leba
//------------------------------------------------------------------------------
u8 genfs_renamefile(u8 *existingfilename, u8 *newfilename)
{
    u8  correctedsrcfilename[512];
    u8  path[512];

    if (genfs_generalpathcorrection(existingfilename,sizeof(correctedsrcfilename),
                                    correctedsrcfilename) == S_SUCCESS)
    {
        genfs_getpathfromfilename(correctedsrcfilename, path);
        strcat((char*)path, (char*)newfilename);
        genfs_deletefile(path);
        
        if (frename((char*)correctedsrcfilename,
                    (char*)newfilename) == 0)
        {
            return S_SUCCESS;
        }
    }
    
    return S_FAIL;
}

//------------------------------------------------------------------------------
// Move a file
// Inputs:  u8  *srcfilename
//          u8  *dstfilename
// Return:  u8  status
// Engineer: Quyen Leba
//------------------------------------------------------------------------------
u8 genfs_movefile(u8 *srcfilename, u8 *dstfilename)
{
    u8  correctedsrcfilename[512];
    u8  correcteddstfilename[512];

    if (genfs_generalpathcorrection(srcfilename,sizeof(correctedsrcfilename),
                             correctedsrcfilename) == S_SUCCESS)
    {
        if (genfs_generalpathcorrection(dstfilename,sizeof(correcteddstfilename),
                                 correcteddstfilename) == S_SUCCESS)
        {
            if (fmove((char*)correctedsrcfilename,
                      (char*)correcteddstfilename) == 0)
            {
                return S_SUCCESS;
            }
        }
    }
    
    return S_FAIL;
}

//------------------------------------------------------------------------------
// Truncate a file
// Inputs:  u8  *filename (any valid path)
//          u32 newsize
// Return:  u8  status
// Engineer: Quyen Leba
//------------------------------------------------------------------------------
u8 genfs_truncatefile(u8 *filename, u32 newsize)
{
    F_FILE *fptr;
    u32 currentsize;

    fptr = genfs_general_openfile(filename,"r+");
    if (fptr)
    {
        currentsize = fgetfilesize(fptr);
        
        if (currentsize < newsize)
        {
            return S_NOTFIT;
        }
        else if (currentsize == newsize)
        {
            return S_SUCCESS;
        }
        else if (ftruncate(fptr,newsize) == 0)
        {
            return S_SUCCESS;
        }
    }
    else
    {
        return S_OPENFILE;
    }
    return S_FAIL;
}

//------------------------------------------------------------------------------
// Copy a file
// Inputs:  u8  *srcfilename
//          u8  *dstfilename
// Return:  u8  status
// Engineer: Quyen Leba
//------------------------------------------------------------------------------
u8 genfs_copyfile(u8 *srcfilename, u8 *dstfilename)
{
    u8  correctedsrcfilename[512];
    u8  correcteddstfilename[512];

    if (genfs_generalpathcorrection(srcfilename,sizeof(correctedsrcfilename),
                             correctedsrcfilename) == S_SUCCESS)
    {
        if (genfs_generalpathcorrection(dstfilename,sizeof(correcteddstfilename),
                                 correcteddstfilename) == S_SUCCESS)
        {
            if (fcopyfile((char*)correctedsrcfilename,
                          (char*)correcteddstfilename) == 0)
            {
                return S_SUCCESS;
            }
        }
    }

    return S_FAIL;
}

//------------------------------------------------------------------------------
// Verify a file
// Input:   u8  *filename (any valid path)
// Output:  u32 *filesize
// Return:  u8  status
// Engineer: Quyen Leba
//------------------------------------------------------------------------------
u8 genfs_verifyfile(u8 *filename, u32 filesize, u32 filecrc32e)
{
#define GENFS_VERIFY_BUFFER_SIZE    2048
    F_FILE *fptr;
    u8  *bufferptr;
    u32 bufferlength;
    u32 calc_crc32e;
    u8  status;

    status = S_SUCCESS;
    fptr = genfs_general_openfile(filename,"r");
    if (fptr)
    {
        if (fgetfilesize(fptr) != filesize)
        {
            status = S_UNMATCH;
        }
        else
        {
            bufferptr = __malloc(GENFS_VERIFY_BUFFER_SIZE);
            if (bufferptr)
            {
                u8  tmpbuff[4];
                u32 remaining;
    
                memset((char*)tmpbuff,0xFF,sizeof(tmpbuff));
    
                crc32e_reset();
                calc_crc32e = 0xFFFFFFFF;
                while(!feof(fptr))
                {
                    bufferlength = fread((char*)bufferptr,
                                         1,GENFS_VERIFY_BUFFER_SIZE,fptr);
                    remaining = bufferlength - (bufferlength/4)*4;
                    calc_crc32e = crc32e_calculateblock(calc_crc32e,
                                                        (u32*)bufferptr,
                                                        bufferlength/4);
                    calc_crc32e = crc32e_calculateblock(calc_crc32e,
                                                        (u32*)tmpbuff,
                                                        remaining/4);
                }
                __free(bufferptr);

                if (calc_crc32e != filecrc32e)
                {
                    status = S_CRC32E;
                }
            }
            else
            {
                status = S_MALLOC;
            }
        }
        genfs_closefile(fptr);
    }
    else
    {
        status = S_OPENFILE;
    }
    return status;
}

//------------------------------------------------------------------------------
// Create a dir
// Input:   u8  *fulldirname
// Return:  u8  status
// Engineer: Quyen Leba
//------------------------------------------------------------------------------
u8 genfs_createdir(const u8 *fulldirname)
{
    if (FS_MkDir((char*)fulldirname) != 0)
    {
        return S_FAIL;
    }
    return S_SUCCESS;
}

//------------------------------------------------------------------------------
// Remove a dir
// Input:   u8  *fulldirname
// Return:  u8  status
// Engineer: Quyen Leba
//------------------------------------------------------------------------------
u8 genfs_removedir(const u8 *fulldirname)
{
    if (FS_RmDir((char*)fulldirname) != 0)
    {
        return S_FAIL;
    }
    return S_SUCCESS;
}

//------------------------------------------------------------------------------
// Change dir
// Input:   u8  *fulldirname
// Return:  u8  status
// Engineer: Quyen Leba
//------------------------------------------------------------------------------
u8 genfs_changedir(const u8 *fulldirname)
{
    return S_NOTSUPPORT;
}

//------------------------------------------------------------------------------
// Get file size
// Input:   u8  *filename (any valid path)
// Output:  u32 *filesize
// Return:  u8  status
// Engineer: Quyen Leba
//------------------------------------------------------------------------------
u8 genfs_getfilesize(u8 *filename, u32 *filesize)
{
    F_FILE *fptr;
    u8  correctedfilename[512];

    *filesize = 0;
    if (genfs_generalpathcorrection(filename,sizeof(correctedfilename),
                             correctedfilename) == S_SUCCESS)
    {
        fptr = genfs_general_openfile(correctedfilename,"r");
        if (fptr)
        {
            *filesize = fgetfilesize(fptr);
            genfs_closefile(fptr);
            if (*filesize == 0xFFFFFFFF)
            {
                *filesize = 0;
                return S_FAIL;
            }
        }
        else
        {
            return S_OPENFILE;
        }
        return S_SUCCESS;
    }
    return S_FAIL;
}

//------------------------------------------------------------------------------
// Get file size
// Input:   F_FILE  *fptr
// Output:  u32 *filesize
// Return:  u8  status
// Engineer: Quyen Leba
//------------------------------------------------------------------------------
u8 genfs_getfilesize_fptr(F_FILE *fptr, u32 *filesize)
{
    if (!fptr || !filesize)
    {
        return S_INPUT;
    }
    *filesize = fgetfilesize(fptr);
    if (*filesize == 0xFFFFFFFF)
    {
        *filesize = 0;
        return S_FAIL;
    }
    return S_SUCCESS;
}

//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
u8 genfs_storage_init()
{
    genfs_deinit();
    FS_STORAGE_Init();
    return S_SUCCESS;
}

//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
u8 genfs_storage_deinit()
{
    FS_STORAGE_DeInit();
    return S_SUCCESS;
}

//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
u8 genfs_storage_sync()
{
    FS_STORAGE_Sync(GENFS_VOLUME_NAME);
    return S_SUCCESS;
}

//------------------------------------------------------------------------------
// Read filesystem sectors
// Inputs:  u32 sector_index
//          u32 sector_count
// Output:  u8  *buffer
// Return:  u8  status
// Engineer: Quyen Leba
//------------------------------------------------------------------------------
u8 genfs_readsectors(u8 *buffer, u32 sector_index, u32 sector_count)
{
    u32 response;

    response = FS_STORAGE_ReadSectors(GENFS_VOLUME_NAME,buffer,
                                      sector_index,sector_count);
    if (response == 0)
    {
        return S_SUCCESS;
    }
    return S_FAIL;
}

//------------------------------------------------------------------------------
// Write filesystem sectors
// Inputs:  u8  *buffer
//          u32 sector_index
//          u32 sector_count
// Return:  u8  status
// Engineer: Quyen Leba
//------------------------------------------------------------------------------
u8 genfs_writesectors_failsafe(u8 *buffer, u32 sector_index, u32 sector_count)
{
    u32 response;

    response = FS_STORAGE_WriteSectors(GENFS_VOLUME_NAME,buffer,
                                       sector_index,sector_count);
    if (response == 0)
    {
        return S_SUCCESS;
    }
    return S_FAIL;
}

//------------------------------------------------------------------------------
// Write filesystem sectors without using Journaling
// Inputs:  u8  *buffer
//          u32 sector_index
//          u32 sector_count
// Return:  u8  status
// Engineer: Quyen Leba
// Only use this in safe condition such as DFS where filesytem content is
// reloaded.
//------------------------------------------------------------------------------
u8 genfs_writesectors(u8 *buffer, u32 sector_index, u32 sector_count)
{
    u32 response;

    response = FS_STORAGE_WriteSectors_BypassJournal(GENFS_VOLUME_NAME,buffer,
                                                     sector_index,sector_count);
    if (response == 0)
    {
        return S_SUCCESS;
    }
    return S_FAIL;
}

//------------------------------------------------------------------------------
// Set folder for listing
// Input:   u8  *folder_name (NULL terminated)
// Return:  u8  status
// Engineer: Quyen Leba
//------------------------------------------------------------------------------
u8 genfs_listing_set_folder(u8 *folder_name)
{
    u32 length;

    length = strlen((char*)folder_name);
    if (genfs_isfolderexist(folder_name) != S_SUCCESS)
    {
        return S_OUTOFRANGE;
    }
    else if (length >= LISTING_FOLDER_MAX_LENGTH || length < 2)
    {
        return S_INPUT;
    }
    if (genfs_helper_info.folder_name == NULL)
    {
        genfs_helper_info.folder_name = __malloc(LISTING_FOLDER_MAX_LENGTH);
        if (genfs_helper_info.folder_name == NULL)
        {
            return S_MALLOC;
        }
    }
    strcpy((char*)genfs_helper_info.folder_name,(char*)folder_name);
    if (folder_name[length-1] != '\\')
    {
        strcat((char*)genfs_helper_info.folder_name,"\\");
    }
    return S_SUCCESS;
}

//------------------------------------------------------------------------------
// Reset/clean folder name used listing
// Engineer: Quyen Leba
//------------------------------------------------------------------------------
void genfs_listing_reset_folder()
{
    if (genfs_helper_info.folder_name)
    {
        __free(genfs_helper_info.folder_name);
        genfs_helper_info.folder_name = NULL;
    }
}

//------------------------------------------------------------------------------
// Init entry listing
// Input:   u8  folder_selection (0: all, 1: 1st, 2: 2nd)
// Return:  u8  status
// Engineer: Quyen Leba
//------------------------------------------------------------------------------
u8 genfs_listing_init(u16 folder_selection)
{
    if (genfs_helper_info.status.is_find_init)
    {
        FS_FindClose(&genfs_helper_info.f_find_info);
    }
    genfs_helper_info.total_entry_found = 0;
    genfs_helper_info.status.is_find_init = 0;
    genfs_helper_info.status.is_overflow = 0;
    genfs_helper_info.status.listallfolders = 0;
    
    switch(folder_selection)
    {
    case 0:
        genfs_helper_info.current_listing_folder_index = FOLDER_INDEX_DEFAULT;
        if (genfs_helper_info.folder_name == NULL)
        {
            genfs_helper_info.status.listallfolders = 1;
        }
        break;
    case 1:
        genfs_helper_info.current_listing_folder_index = FOLDER_INDEX_DEFAULT;
        break;
    case 2:
        genfs_helper_info.current_listing_folder_index = FOLDER_INDEX_USER;
        break;
    default:
        return S_INPUT;
    }
    return S_SUCCESS;
}

//------------------------------------------------------------------------------
// Get current entry
// Outputs: u8  *entry
//          u32 *entry_size
// Return:  u8  status
// Engineer: Quyen Leba
//------------------------------------------------------------------------------
u8 genfs_listing_get_entry(u8 *entry, u32 *entry_size)
{
    u32 response;
    u8  status;

    status = S_FAIL;

    if (genfs_helper_info.total_entry_found != 0)
    {
        response = FS_FindNextFile(&genfs_helper_info.f_find_info);
        if (response == 1)
        {
            *entry_size = genfs_helper_info.f_find_info.FileSize;
            genfs_helper_info.total_entry_found++;
            status = S_SUCCESS;
        }
        else
        {
            memset((char*)genfs_helper_info.entry_buffer,
                   0,sizeof(genfs_helper_info.entry_buffer));
            FS_FindClose(&genfs_helper_info.f_find_info);
            
            if (genfs_helper_info.current_listing_folder_index != FOLDER_INDEX_MAX)
            {
                genfs_helper_info.current_listing_folder_index++;
                
                if (genfs_helper_info.folder_name == NULL)
                {
                    response = FS_FindFirstFile(&genfs_helper_info.f_find_info,
                                                (char*)folder_list[genfs_helper_info.current_listing_folder_index],
                                                (char*)genfs_helper_info.entry_buffer,
                                                sizeof(genfs_helper_info.entry_buffer));
                }
                else
                {
                    //should never happen
                    response = FS_FindFirstFile(&genfs_helper_info.f_find_info,
                                                (char*)genfs_helper_info.folder_name,
                                                (char*)genfs_helper_info.entry_buffer,
                                                sizeof(genfs_helper_info.entry_buffer));
                    genfs_helper_info.current_listing_folder_index = FOLDER_INDEX_MAX;
                }
                if (response == 0)
                {
                    genfs_helper_info.status.is_find_init = 1;
                    genfs_helper_info.total_entry_found++;
                    *entry_size = genfs_helper_info.f_find_info.FileSize;
                    status = S_SUCCESS;
                }
                else
                {
                    memset((char*)genfs_helper_info.entry_buffer,
                           0,sizeof(genfs_helper_info.entry_buffer));
                    FS_FindClose(&genfs_helper_info.f_find_info);
                    genfs_helper_info.status.is_find_init = 0;
                }
                if (genfs_helper_info.status.listallfolders == 0)
                {
                    genfs_helper_info.current_listing_folder_index = FOLDER_INDEX_MAX;
                }
            }
            else
            {
                genfs_helper_info.status.is_find_init = 0;
                genfs_helper_info.total_entry_found = 0;
            }
        }
    }//if (genfs_helper_info.total_entry_found != 0)...
    else
    {
        if (genfs_helper_info.folder_name == NULL)
        {
            response = FS_FindFirstFile(&genfs_helper_info.f_find_info,
                                        (char*)folder_list[genfs_helper_info.current_listing_folder_index],
                                        (char*)genfs_helper_info.entry_buffer,
                                        sizeof(genfs_helper_info.entry_buffer));
        }
        else
        {
            response = FS_FindFirstFile(&genfs_helper_info.f_find_info,
                                        (char*)genfs_helper_info.folder_name,
                                        (char*)genfs_helper_info.entry_buffer,
                                        sizeof(genfs_helper_info.entry_buffer));
            genfs_helper_info.current_listing_folder_index = FOLDER_INDEX_MAX;
        }

        if (response == 0)
        {
            genfs_helper_info.status.is_find_init = 1;
            genfs_helper_info.total_entry_found++;
            *entry_size = genfs_helper_info.f_find_info.FileSize;
            status = S_SUCCESS;
        }
        else
        {
            memset((char*)genfs_helper_info.entry_buffer,
                   0,sizeof(genfs_helper_info.entry_buffer));
            FS_FindClose(&genfs_helper_info.f_find_info);
            genfs_helper_info.status.is_find_init = 0;
            genfs_helper_info.total_entry_found = 0;
        }
    }

    if (status == S_SUCCESS)
    {
        if (genfs_helper_info.folder_name == NULL)
        {
            strcpy((char*)entry,(char*)folder_list[genfs_helper_info.current_listing_folder_index]);
        }
        else
        {
            strcpy((char*)entry,(char*)genfs_helper_info.folder_name);
        }
        strcat((char*)entry,(char*)genfs_helper_info.entry_buffer);
    }
    else
    {
        entry[0] = NULL;
        *entry_size = 0;
    }
    return status;
}

//------------------------------------------------------------------------------
// Get as many entries from current entry as *data can hold
// Inputs:  u16 marker (0: restart, else continue)
//          u16 folder_selection (0: all folders, 1: 1st, 2: 2nd)
//          u8  *extension (NULL: don't care)
//          u32 maxdatalength
// Outputs: u8  *data (data[maxdatalength])
//          u32 *datalength (actual content length of *data)
// Return:  u8  status (last successful status: S_TERMINATED)
// Engineer: Quyen Leba
// Note: each entry has [n:entryname][4:entrysize][4:"|<>|"]
// Note: last entry ending in [4:"|><|"]
//------------------------------------------------------------------------------
u8 genfs_get_listing_bulk(u16 marker, u16 folder_selection, u8 *extension,
                          u32 maxdatalength,
                          u8 *data, u32 *datalength)
{
    u8  entryname[GENFS_ENTRYNAMELENGTH_MAX+1];
    u32 entrynamelength;
    u32 entrysize;
    u32 bytecount;
    u8  status;

    bytecount = 0;
    *datalength = 0;

    if (marker == 0 || genfs_helper_info.status.is_find_init == 0)
    {
        status = genfs_listing_init(folder_selection);
        if (status != S_SUCCESS)
        {
            goto genfs_get_listing_bulk_done;
        }
    }

    if (genfs_helper_info.status.is_overflow && genfs_helper_info.status.is_find_init)
    {
        genfs_helper_info.status.is_overflow = 0;
        entrysize = genfs_helper_info.f_find_info.FileSize;

        if (genfs_helper_info.folder_name == NULL)
        {
            strcpy((char*)entryname,
                   (char*)folder_list[genfs_helper_info.current_listing_folder_index]);
        }
        else
        {
            strcpy((char*)entryname,(char*)genfs_helper_info.folder_name);
        }
        strcat((char*)entryname,genfs_helper_info.f_find_info.sFileName);
        
        entrynamelength = strlen((char*)entryname);
        //Note: +1 to make room for [NULL] end the end of data
        //There's no [NULL] after each entry
        if ((bytecount + entrynamelength + 8+1) < maxdatalength)    //8: [4:size][4:"|<>|"]
        {
            if (extension == NULL)
            {
                memcpy((char*)&data[bytecount],(char*)entryname,entrynamelength);
                bytecount += entrynamelength;
                memcpy((char*)&data[bytecount],(char*)&entrysize,4);
                bytecount += 4;
                data[bytecount++] = '|';
                data[bytecount++] = '<';
                data[bytecount++] = '>';
                data[bytecount++] = '|';
            }
            else if (entrynamelength > 4 && strstr((char*)&entryname[entrynamelength-3],(char*)extension))
            {
                memcpy((char*)&data[bytecount],(char*)entryname,entrynamelength);
                bytecount += entrynamelength;
                memcpy((char*)&data[bytecount],(char*)&entrysize,4);
                bytecount += 4;
                data[bytecount++] = '|';
                data[bytecount++] = '<';
                data[bytecount++] = '>';
                data[bytecount++] = '|';
            }
        }
        else
        {
            status = S_ERROR;
            goto genfs_get_listing_bulk_done;
        }
    }
    
    while(1)
    {
        status = genfs_listing_get_entry(entryname,&entrysize);
        if (status == S_SUCCESS)
        {
            entrynamelength = strlen((char*)entryname);
            //Note: +1 to make room for [NULL] end the end of data
            //There's no [NULL] after each entry
            if ((bytecount + entrynamelength + 8+1) < maxdatalength)    //8: [4:size][4:"|<>|"]
            {
                if (extension == NULL)
                {
                    memcpy((char*)&data[bytecount],
                           (char*)entryname,entrynamelength);
                    bytecount += entrynamelength;
                    memcpy((char*)&data[bytecount],
                           (char*)&entrysize,4);
                    bytecount += 4;
                    data[bytecount++] = '|';
                    data[bytecount++] = '<';
                    data[bytecount++] = '>';
                    data[bytecount++] = '|';
                }
                else if (entrynamelength > 4 && strstr((char*)&entryname[entrynamelength-3],(char*)extension))
                {
                    memcpy((char*)&data[bytecount],
                           (char*)entryname,entrynamelength);
                    bytecount += entrynamelength;
                    memcpy((char*)&data[bytecount],
                           (char*)&entrysize,4);
                    bytecount += 4;
                    data[bytecount++] = '|';
                    data[bytecount++] = '<';
                    data[bytecount++] = '>';
                    data[bytecount++] = '|';
                }
            }
            else if (bytecount > 0)
            {
                genfs_helper_info.status.is_overflow = 1;
                break;
            }
            else
            {
                status = S_ERROR;
                goto genfs_get_listing_bulk_done;
            }
        }
        else
        {
            //probably no more entries
            genfs_listing_init(0);
            genfs_listing_reset_folder();
            status = S_TERMINATED;
            break;
        }
    }

    if (status == S_SUCCESS || status == S_TERMINATED)
    {
        if (bytecount > 4)
        {
            //the last entry has different termination
            data[bytecount-4] = '|';
            data[bytecount-3] = '>';
            data[bytecount-2] = '<';
            data[bytecount-1] = '|';
            data[bytecount] = NULL;
        }
        else if (bytecount == 0)
        {
            data[0] = '|';
            data[1] = '>';
            data[2] = '<';
            data[3] = '|';
            data[4] = NULL;
            bytecount = 4;
        }
        else
        {
            status = S_FAIL;
        }
    }
    *datalength = bytecount;

genfs_get_listing_bulk_done:
    return status;
}

/**
 *  @brief Strip filename from path
 *
 *  @param [in]     fullfilename    Full filename including path
 *  @param [out]     path            Path
 *
 *  @return Nothing to return
 */
void genfs_getpathfromfilename(u8 *fullfilename, u8 *path)
{
    u8 i;
    
    path[0] = NULL_BYTE;
    
    for (i = strlen((char*)fullfilename); i > 0; i--)
    {
        if (fullfilename[i] == '\\' || fullfilename[i] == '/')
        {
            memcpy((char*)path,(char*)fullfilename,i+1);
            path[i+1] = NULL_BYTE;
            break;
        }
    }
}

/**
 *  @brief Strip path from filename
 *
 *  @param [in]     fullfilename    Full filename including path
 *  @param [out]     filename        Filename
 *
 *  @return Nothing to return
 */
void genfs_getfilenamefrompath(u8 *fullfilename, u8 *filename)
{
    u8 i;
    
    filename[0] = NULL_BYTE;
    
    for (i = strlen((char*)fullfilename); i > 0; i--)
    {
        if (fullfilename[i] == '\\' || fullfilename[i] == '/')
        {
            strcpy((char*)filename, (char const*)((fullfilename+i)+1));
            break;
        }
    }
    
    if(i == 0) // No path, just return filename
    {
        strcpy((char*)filename, (const char*)fullfilename);
    }
}

