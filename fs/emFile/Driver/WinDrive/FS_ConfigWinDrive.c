/*********************************************************************
*                SEGGER MICROCONTROLLER GmbH & Co. KG                *
*        Solutions for real time microcontroller applications        *
**********************************************************************
*                                                                    *
*        (c) 2003-2012     SEGGER Microcontroller GmbH & Co KG       *
*                                                                    *
*        Internet: www.segger.com    Support:  support@segger.com    *
*                                                                    *
**********************************************************************

**** emFile file system for embedded applications ****
emFile is protected by international copyright laws. Knowledge of the
source code may not be used to write a similar product. This file may
only be used in accordance with a license and should not be re-
distributed in any way. We appreciate your understanding and fairness.
----------------------------------------------------------------------
File        : FS_ConfigWinDrive.c
Purpose     : Configuration file  for Win driver.
---------------------------END-OF-HEADER------------------------------
*/


#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include "FS.h"
#include "FS_Debug.h"

/*********************************************************************
*
*      defines for Memory allocation
*
**********************************************************************
*/
#ifndef   FS_ALLOC_SIZE
  #define FS_ALLOC_SIZE 0x1000000
#endif

#define MEM_UNITS (FS_ALLOC_SIZE + sizeof(U32) - 1) / sizeof(U32)

/*********************************************************************
*
*       Static data
*
**********************************************************************
*/
static U32   _aMemBlock[MEM_UNITS];           // Memory pool used for semi-dynamic allocation in FS_X_Alloc().

/*********************************************************************
*
*       Public code
*
**********************************************************************
*/

/*********************************************************************
*
*       FS_X_Panic
*
*  Function description
*    Referred in debug builds of the file system only and
*    called only in case of fatal, unrecoverable errors.
*/
void FS_X_Panic(int ErrorCode) {
  char ac[255];

  sprintf(ac, "The following FS_PANIC occurred: %d", ErrorCode);
  MessageBox(NULL, ac, "FS_X_Panic", MB_ICONERROR | MB_OK);
  while (1);
}

/*********************************************************************
*
*       FS_X_AddDevices
*
*  Function description
*    This function is called by the FS during FS_Init().
*    It is supposed to add all devices, using primarily FS_AddDevice().
*    
*  Note
*    (1) Other API functions
*        Other API functions may NOT be called, since this function is called
*        during initialization. The devices are not yet ready at this point.
*/
void FS_X_AddDevices(void) {
  U8 Unit;

  FS_AssignMemory(&_aMemBlock[0], sizeof(_aMemBlock));
  FS_AddDevice(&FS_WINDRIVE_Driver);
  WINDRIVE_Configure(0, NULL);
}

/*********************************************************************
*
*       FS_X_GetTimeDate
*
*  Function description:
*    Current time and date in a format suitable for the file system.
*
*    Bit 0-4:   2-second count (0-29)
*    Bit 5-10:  Minutes (0-59)
*    Bit 11-15: Hours (0-23)
*    Bit 16-20: Day of month (1-31)
*    Bit 21-24: Month of year (1-12)
*    Bit 25-31: Count of years from 1980 (0-127)
*
*/
U32 FS_X_GetTimeDate(void) {
  U16         rDate;
  U16         rTime;
  time_t      Time;
  struct tm * pLocalTime;

  time(&Time);
  pLocalTime = localtime(&Time);
  rDate      = (U16) (pLocalTime->tm_mday);
  rDate     |= (U16)((pLocalTime->tm_mon  +  1) << 5);
  rDate     |= (U16)((pLocalTime->tm_year - 80) << 9);
  rTime      = (U16)(pLocalTime->tm_sec   /  2);
  rTime     |= (U16)(pLocalTime->tm_min  <<  5);
  rTime     |= (U16)(pLocalTime->tm_hour << 11);
  return rDate << 16 | rTime;
}

/*********************************************************************
*
*      Logging: Target dependent
*
*  Note:
*    Logging is used in higher debug levels only. The typical target
*     build does not use logging and does therefore not require any of
*     the logging routines below. For a release build without logging
*     the routines below may be eliminated to save some space.
*     (If the linker is not function aware and eliminates unreferenced
*     functions automatically)
*
*/
void FS_X_Log(const char *s) {
  printf(s);
}

void FS_X_Warn(const char *s) {
  printf("FS warning: %s", s);
}

void FS_X_ErrorOut(const char *s) {
  MessageBox(NULL, s, "FS ErrorOut", MB_ICONERROR | MB_OK);
}

/*************************** End of file ****************************/
