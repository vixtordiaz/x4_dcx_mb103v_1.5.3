/*********************************************************************
*                SEGGER MICROCONTROLLER GmbH & Co. KG                *
*        Solutions for real time microcontroller applications        *
**********************************************************************
*                                                                    *
*        (c) 2003-2012     SEGGER Microcontroller GmbH & Co KG       *
*                                                                    *
*        Internet: www.segger.com    Support:  support@segger.com    *
*                                                                    *
**********************************************************************

**** emFile file system for embedded applications ****
emFile is protected by international copyright laws. Knowledge of the
source code may not be used to write a similar product. This file may
only be used in accordance with a license and should not be re-
distributed in any way. We appreciate your understanding and fairness.
----------------------------------------------------------------------
----------------------------------------------------------------------
File        : Driver_Template.c
Purpose     : Template for a FS driver.
---------------------------END-OF-HEADER------------------------------
*/

/*********************************************************************
*
*       #include Section
*
**********************************************************************
*/

#include "FS_ConfDefaults.h"        /* FS Configuration */

#include "FS_Int.h"
#include "FS_CLib.h"

/*********************************************************************
*
*       #define constants
*
**********************************************************************
*/
#define   SECTOR_SIZE     512

#ifndef NUM_UNITS
  #define NUM_UNITS         4
#endif

/*********************************************************************
*
*       Type definitions
*
**********************************************************************
*/
typedef struct {
  U8            IsInited;
  U8            Unit;
  U16           NumHeads;
  U16           SectorsPerTrack;
  U32           NumSectors;
  U16           BytesPerSector;
} DRIVER_INST;

/*********************************************************************
*
*       Static data
*
**********************************************************************
*/
static DRIVER_INST  * _apInst[NUM_UNITS];
static int         _NumUnits;

/*********************************************************************
*
*       Static code
*
**********************************************************************
*/


/*********************************************************************
*
*       _Init
*
*  Description:
*  Resets/Initializes the device
*
* Parameters:
*   Unit        - Unit number.
*
*  Return value:
*   ==0         - Device has been reset and initialized.
*   < 0         - An error has occurred.
*/
static int _Init(DRIVER_INST * pInst) {
  return 0;
}

/*********************************************************************
*
*       _Unmount
*
*  Description:
*    Unmounts the volume.
*
*  Parameters:
*    Unit        - Unit number.
*
*/
static void _Unmount(U8 Unit) {
  if (_apInst[Unit]->IsInited == 1) {
    FS_MEMSET(_apInst[Unit], 0, sizeof(DRIVER_INST));
  }
}

/*********************************************************************
*
*       Public code (indirectly thru callback)
*
**********************************************************************
*/

/*********************************************************************
*
*       _GetStatus
*
*  Description:
*    FS driver function. Get status of the media.
*
*  Parameters:
*    Unit                  - Unit number.
*
*  Return value:
*    FS_MEDIA_STATE_UNKNOWN - if the state of the media is unknown.
*    FS_MEDIA_NOT_PRESENT   - if no card is present.
*    FS_MEDIA_IS_PRESENT    - if a card is present.
*/
static int _GetStatus(U8 Unit) {
  return FS_MEDIA_IS_PRESENT;
}

/*********************************************************************
*
*       _Read
*
*  Function Description
*    Driver callback function.
*    Reads one or more logical sectors from storage device.
*
*  Parameters:
*    Unit        - Unit number.
*    Sector      - Sector to be read from the device.
*    pBuffer     - Pointer to buffer for storing the data.
*    NumSectors  - Number of sectors to read
*
*  Return value:
*      0                       - Data successfully written.
*    !=0                       - An error has occurred.
*
*/
static int _Read(U8 Unit, U32 SectorNo, void * p, U32 NumSectors) {
  DRIVER_INST * pInst;
  
  pInst = _apInst[Unit];
  return pInst->IsInited ? 0 : -1;
}

/*********************************************************************
*
*       _Write
*
*  Description:
*    Driver callback function.
*    Writes one or more logical sectors to storage device.
*
*  Parameters:
*    Unit        - Unit number.
*    SectorNo    - Sector number to be written to the device.
*    pBuffer     - Pointer to buffer conating the data to write
*    NumSectors  - Number of sectors to store.
*    RepeatSame  - Repeat the same data to sectors.
*
*  Return value:
*      0                       - Data successfully written.
*    !=0                       - An error has occurred.
*
*/
static int _Write(U8 Unit, U32 SectorNo, const void  * p, U32 NumSectors, U8  RepeatSame) {
  DRIVER_INST * pInst;
  
  pInst = _apInst[Unit];
  return pInst->IsInited ? 0 : -1;
}

/*********************************************************************
*
*       _DevIoCtl
*
*  Description:
*    FS driver function. Execute device command.
*
*  Parameters:
*    Unit    - Unit number.
*    Cmd         - Command to be executed.
*    Aux         - Parameter depending on command.
*    pBuffer     - Pointer to a buffer used for the command.
*
*  Return value:
*    Command specific. In general a negative value means an error.
*/
static int _IoCtl(U8 Unit, I32 Cmd, I32 Aux, void *pBuffer) {
  DRIVER_INST * pInst;
  FS_DEV_INFO * pInfo;
  int           r;

  pInst = _apInst[Unit];
  r     = -1;
  FS_USE_PARA(Aux);
  switch (Cmd) {
  case FS_CMD_GET_DEVINFO:
    pInfo = (FS_DEV_INFO *)pBuffer;
    pInfo->NumHeads        = pInst->NumHeads;         /* heads */
    pInfo->SectorsPerTrack = pInst->SectorsPerTrack;  /* sec per track */
    pInfo->NumSectors      = pInst->NumSectors;       /* Number of sectors */
    pInfo->BytesPerSector  = pInst->BytesPerSector;   /* IDE/CF media are always working with 512 Bytes per sector */
    r = 0;
    break;
  case FS_CMD_UNMOUNT:  
    //
    // (Optional)
    // Device shall be unmounted - sync all operations and mark it as unmounted
    //
    
    _Unmount(Unit);
    r = 0;
    break;
  case FS_CMD_UNMOUNT_FORCED:
    //
    // (Optional)
    // Device shall be unmounted - mark it as unmounted without syncing any pending operations 
    //
    
    // ToDo: Call the function
    break;
  case FS_CMD_SYNC:
    //
    // (Optional)
    // Sync/flush any pending operations 
    //

    // ToDo: Call the function
    break;
  default:
    break;
  }
  return r;
}

/*********************************************************************
*
*       _InitMedium
*
*  Description:
*    Initialize the specified medium.
*
*  Parameters:
*    Unit    - Unit number.
*
*  Return value:
*/
static int _InitMedium(U8 Unit) {
  int i;
  DRIVER_INST * pInst;

  pInst = _apInst[Unit];
  if (pInst->IsInited == 0) {
    i = _Init(pInst);
    if (i < 0) { /* init failed, no valid card in slot */
      FS_DEBUG_WARN("IDE/CF: Init failure, no valid card found");
      return -1;
    } else {
      pInst->IsInited = 1;
    }
  }
  return 0;
}

/*********************************************************************
*
*       _AddDevice
*
*  Function Description:
*    Initializes the low-level driver object.
*
*  Return value:
*    >= 0                       - Command succesfully executed, Unit no.
*    <  0                       - Error, could not add device
*
*/
static int _AddDevice(void) {
  U8            Unit;
  DRIVER_INST * pInst;

  if (_NumUnits >= NUM_UNITS) {
    return -1;
  }
  Unit = _NumUnits++;
  pInst = (DRIVER_INST *)FS_AllocZeroed(sizeof(DRIVER_INST));   // Alloc memory. This is guaranteed to work by the memory module.
  _apInst[Unit] = pInst;
  return Unit;
}

/*********************************************************************
*
*       _GetNumUnits
*
*  Function description:
*    
*  Return value:
*    >= 0                       - Command succesfully executed, Unit no.
*    <  0                       - Error, could not add device
*/
static int _GetNumUnits(void) {
  return _NumUnits;
}

/*********************************************************************
*
*       _GetDriverName
*/
static const char * _GetDriverName(U8 Unit) {
  return "sample";
}

/*********************************************************************
*
*       Public data
*
**********************************************************************
*/
const FS_DEVICE_TYPE FS_TEMPLATE_Driver = {
  _GetDriverName,
  _AddDevice,
  _Read,
  _Write,
  _IoCtl,
  _InitMedium,
  _GetStatus,
  _GetNumUnits
};

/*************************** End of file ****************************/
