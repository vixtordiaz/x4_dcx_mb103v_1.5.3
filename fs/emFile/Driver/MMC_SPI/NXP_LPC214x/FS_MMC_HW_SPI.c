/*********************************************************************
*                SEGGER MICROCONTROLLER GmbH & Co. KG                *
*        Solutions for real time microcontroller applications        *
**********************************************************************
*                                                                    *
*        (c) 2003-2012     SEGGER Microcontroller GmbH & Co KG       *
*                                                                    *
*        Internet: www.segger.com    Support:  support@segger.com    *
*                                                                    *
**********************************************************************

**** emFile file system for embedded applications ****
emFile is protected by international copyright laws. Knowledge of the
source code may not be used to write a similar product. This file may
only be used in accordance with a license and should not be re-
distributed in any way. We appreciate your understanding and fairness.
----------------------------------------------------------------------
File        : MMC_HW_SPI.c
Purpose     : Sample MMC hardware layer for NXP LPC21xx/22xx series
---------------------------END-OF-HEADER------------------------------
*/

#include "FS.h"
#include "MMC_X_HW.h"


#define _CRYSTAL_CLOCK          12000000

/*********************************************************************
*
*       Defines
*
**********************************************************************
*/
/* SPI0 (Serial Peripheral Interface 0) */
#define SSPCR0        *(volatile U32 *)(0xE0068000)
#define SSPCR1        *(volatile U32 *)(0xE0068004)
#define SSPDR         *(volatile U32 *)(0xE0068008)
#define SSPSR         *(volatile U32 *)(0xE006800C)
#define SSPCPSR       *(volatile U32 *)(0xE0068010)
#define SSPIMSC       *(volatile U32 *)(0xE0068014)
#define SSPRIS        *(volatile U32 *)(0xE0068018)
#define SSPMIS        *(volatile U32 *)(0xE006801C)
#define SSPICR        *(volatile U32 *)(0xE0068020)

/* General Purpose Input/Output (GPIO) */
#define IOPIN0         *(volatile U32 *)(0xE0028000)
#define IOSET0         *(volatile U32 *)(0xE0028004)
#define IODIR0         *(volatile U32 *)(0xE0028008)
#define IOCLR0         *(volatile U32 *)(0xE002800C)
#define IOPIN1         *(volatile U32 *)(0xE0028010)
#define IOSET1         *(volatile U32 *)(0xE0028014)
#define IODIR1         *(volatile U32 *)(0xE0028018)
#define IOCLR1         *(volatile U32 *)(0xE002801C)

/* Pin Connect Block */
#define PINSEL0        *(volatile U32 *)(0xE002C000)
#define PINSEL1        *(volatile U32 *)(0xE002C004)
#define PINSEL2        *(volatile U32 *)(0xE002C014)
#define PCONP          *(volatile U32 *)(0xE01FC0C4)
#define VPBDIV         *(volatile U32 *)(0xE01FC100)
#define PLLSTAT        *(volatile U32 *)(0xE01FC088)

#define MMC_DEFAULTSUPPLYVOLTAGE  3300 /* in mV, example means 3.3V */

/*********************************************************************
*
*       Static data
*
**********************************************************************
*/
static char _IsInited;
/*********************************************************************
*
*       Static code
*
**********************************************************************
*/

/*********************************************************************
*
*       _Init
*
*/
static void _Init(void) {
  if (_IsInited == 0) {
    U8  i, Dummy;
    
    
    PCONP      |= (1 << 10);                 // Enable clock for SPI1
    SSPCR1      = 0x00;                      // SSP master (off) in normal mode
    // Configure PIN connect block
    PINSEL1    |= (2 << 2)                   //Set Port 0.17 to SCK1
               |  (2 << 4)                   //Set Port 0.18 to MISO1
               |  (2 << 6)                   //Set Port 0.19 to MOSI1
               ;
    IODIR0      = 1 << 20;                   // CS is output
    IOSET0      = 1 << 20;                   // Set CS to high
    SSPCR0      = 0x0007;                    // Set data to 8-bit, Frame format SPI, CPOL = 0, CPHA = 0 and SCR is 0
    SSPCPSR     = 0x2;                       // SSPCPSR clock prescale register, master mode, minimum divisor is 0x02
    //
    // Device select as master, SSP Enabled, normal operational mode
    //
    SSPCR1 = 0x02;
    for ( i = 0; i < 8; i++ ) {
      Dummy = SSPDR;                         // Flush RxFIFO
      FS_USE_PARA(Dummy);
    }
    _IsInited = 1;
  }
}

/*********************************************************************
*
*       FS_MMC_HW_X_EnableCS
*
*/
void FS_MMC_HW_X_EnableCS(U8 Unit) {
  IOCLR0 = (1 << 20);
}


/*********************************************************************
*
*       FS_MMC_HW_X_DisableCS
*
*/
void FS_MMC_HW_X_DisableCS(U8 Unit) {
  IOSET0 = (1 << 20);
}


/*********************************************************************
*
*       FS_MMC_HW_X_IsPresent
*
*/
int FS_MMC_HW_X_IsPresent(U8 Unit) {
  _Init();
  return FS_MEDIA_IS_PRESENT;
}

/*********************************************************************
*
*       FS_MMC_HW_X_IsWriteProtected
*
*/
int FS_MMC_HW_X_IsWriteProtected(U8 Unit) {
  return 0;
}


/*********************************************************************
*
*       FS_MMC_HW_X_SetMaxSpeed
*
*/
U16 FS_MMC_HW_X_SetMaxSpeed(U8 Unit, U16 MaxFreq) {
  unsigned Prescaler;    
  U32      CPUClock;
  U32      PeriphalClock;
  U32      PLLStatus;
  _Init();
  
  CPUClock  = _CRYSTAL_CLOCK;
  PLLStatus = PLLSTAT;
  // PLL enabled ?
  if (PLLStatus & (3 << 8)) {
    CPUClock *= ((PLLStatus & 0x0f) + 1);
  }
  switch (VPBDIV) {
  case 0:
    PeriphalClock = CPUClock >> 2;
    break;
  case 1:
    PeriphalClock = CPUClock;
    break;
  case 2:
    PeriphalClock = CPUClock >> 1;
    break;
  }
  // Calc prescaler value.
  Prescaler = ((PeriphalClock / 1000) + MaxFreq - 1) / MaxFreq;
  Prescaler = ((Prescaler + 1) >> 1) << 1;
  if (Prescaler < 2) {
    Prescaler = 2;
  }
  SSPCPSR  = Prescaler;
  return (PeriphalClock / 1000) / Prescaler;
}

/*********************************************************************
*
*       FS_MMC_HW_X_SetVoltage
*
*/
int FS_MMC_HW_X_SetVoltage(U8 Unit, U16 Vmin, U16 Vmax) {
  return 1;
}


/*********************************************************************
*
*       FS_MMC_HW_X_Read
*
*/
void FS_MMC_HW_X_Read(U8 Unit, U8 * pData, int NumBytes) {
  int i;
  
  for (i = 0; i < NumBytes; i++) {
    // Wrtie dummy byte out to generate clock 
    // to read data from MISO
    SSPDR = 0xFF;
    // Wait until the Busy bit is cleared
    while ( SSPSR & (1 << 4));
    *pData++ = SSPDR;
  }
}

/*********************************************************************
*
*       FS_MMC_HW_X_Write
*
*/
void FS_MMC_HW_X_Write(U8 Unit, const U8 * pData, int NumBytes) {
  U8 Dummy;
  
  do {
    //
    // As long as TNF bit is set, 
    // TxFIFO is not full -> writing to the FIFO allowed
    //
    while ((SSPSR & (1 << 1)) == 0);
    SSPDR = *pData;
    // Wait until the Busy bit is cleared
    while ((SSPSR & (1 << 0x04)));
    Dummy = SSPDR; /* Flush the RxFIFO */
    FS_USE_PARA(Dummy);
    pData++;
  } while (--NumBytes);
}
/*************************** End of file ****************************/
