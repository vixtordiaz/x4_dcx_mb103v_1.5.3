/*********************************************************************
*                SEGGER MICROCONTROLLER GmbH & Co. KG                *
*        Solutions for real time microcontroller applications        *
**********************************************************************
*                                                                    *
*        (c) 2003-2012     SEGGER Microcontroller GmbH & Co KG       *
*                                                                    *
*        Internet: www.segger.com    Support:  support@segger.com    *
*                                                                    *
**********************************************************************

**** emFile file system for embedded applications ****
emFile is protected by international copyright laws. Knowledge of the
source code may not be used to write a similar product. This file may
only be used in accordance with a license and should not be re-
distributed in any way. We appreciate your understanding and fairness.
----------------------------------------------------------------------
----------------------------------------------------------------------
File        : MMC_HW_SPI.c
Purpose     : Sample MMC hardware layer for Atmel AT91SAM9260
---------------------------END-OF-HEADER------------------------------
*/

/*********************************************************************
*
*             #include Section
*
**********************************************************************
*/

#include "FS_Int.h"
#include "MMC_X_HW.h"
#include "FS_OS.h"
#include "RTOS.h"

/*********************************************************************
*
*       Defines, Configurable
*
**********************************************************************
*/
#define USE_OS    0     // 0: Polled mode, 1 means the task is suspended during DMA transfer, reducing CPU load and allowing the CPU to execute an other task


#ifndef USE_PDC
  #define USE_PDC 1    // 0: PDC shall not be used. When PDC is used to transfer data from/to internal memory, wrong data are received. 1 : PDC shall be used.
#endif

/*********************************************************************
*
*       Defines, sfr
*
**********************************************************************
*/
#define PIOA_BASE 0xfffff400
#define PIOA_PDR  *(volatile U32*) (PIOA_BASE + 0x04) /* PIOA disable register               */
#define PIOA_ASR  *(volatile U32*) (PIOA_BASE + 0x70) /* PIOA "A" peripheral select register */
#define PIOA_BSR  *(volatile U32*) (PIOA_BASE + 0x74) /* PIOA "B" peripheral select register */
#define PIOA_PER  *(volatile U32*) (PIOA_BASE + 0x00) // Enable register, enables PIO function
#define PIOA_OER  *(volatile U32*) (PIOA_BASE + 0x10) // Output enable register, sets to output mode
#define PIOA_ODR  *(volatile U32*) (PIOA_BASE + 0x14) // Output enable register, sets to output mode
#define PIOA_SODR *(volatile U32*) (PIOA_BASE + 0x30) // Set output data
#define PIOA_CODR *(volatile U32*) (PIOA_BASE + 0x34) // Clear output data register
#define PIOA_ODSR *(volatile U32*) (PIOA_BASE + 0x38) // output data status register
#define PIOA_PDSR *(volatile U32*) (PIOA_BASE + 0x3c) // pin data status register
#define PIOA_OWER *(volatile U32*) (PIOA_BASE + 0xA0) // Output write enable register
#define PIOA_OWDR *(volatile U32*) (PIOA_BASE + 0xA4) // Output write disable register

#define PIOA_ID          2

/*      SPI0 */
#define SPI_BASE  0xFFFC8000
#define SPI_CR    *(volatile U32*) (SPI_BASE + 0x00)
#define SPI_MR    *(volatile U32*) (SPI_BASE + 0x04)
#define SPI_RDR   *(volatile U32*) (SPI_BASE + 0x08)
#define SPI_TDR   *(volatile U32*) (SPI_BASE + 0x0C)
#define SPI_SR    *(volatile U32*) (SPI_BASE + 0x10)
#define SPI_IER   *(volatile U32*) (SPI_BASE + 0x14)
#define SPI_IDR   *(volatile U32*) (SPI_BASE + 0x18)
#define SPI_IMR   *(volatile U32*) (SPI_BASE + 0x1c)
#define SPI_CSR0  *(volatile U32*) (SPI_BASE + 0x30)
#define SPI_CSR1  *(volatile U32*) (SPI_BASE + 0x34)
#define SPI_CSR2  *(volatile U32*) (SPI_BASE + 0x38)
#define SPI_CSR3  *(volatile U32*) (SPI_BASE + 0x3c)

/*      SPI PDC */
#define SPI_PDC_RPR  *(volatile U32*) (SPI_BASE + 0x100)
#define SPI_PDC_RCR  *(volatile U32*) (SPI_BASE + 0x104)
#define SPI_PDC_TPR  *(volatile U32*) (SPI_BASE + 0x108)
#define SPI_PDC_TCR  *(volatile U32*) (SPI_BASE + 0x10c)
#define SPI_PDC_RNPR *(volatile U32*) (SPI_BASE + 0x110)
#define SPI_PDC_RNCR *(volatile U32*) (SPI_BASE + 0x114)
#define SPI_PDC_TNPR *(volatile U32*) (SPI_BASE + 0x118)
#define SPI_PDC_TNCR *(volatile U32*) (SPI_BASE + 0x11c)
#define SPI_PDC_PTCR *(volatile U32*) (SPI_BASE + 0x120)
#define SPI_PDC_PTSR *(volatile U32*) (SPI_BASE + 0x124)

#define SPI_RXBUFF    ((unsigned int) 0x1 <<  6)    // (SPI) RXBUFF Interrupt
#define SPI_TXBUFE    ((unsigned int) 0x1 <<  7)    // (SPI) TXBUFE Interrupt

#define PDC_RXTEN     ((unsigned int) 0x1 <<  0)    // (PDC) Receiver Transfer Enable
#define PDC_RXTDIS    ((unsigned int) 0x1 <<  1)    // (PDC) Receiver Transfer Disable
#define PDC_TXTEN     ((unsigned int) 0x1 <<  8)    // (PDC) Transmitter Transfer Enable
#define PDC_TXTDIS    ((unsigned int) 0x1 <<  9)    // (PDC) Transmitter Transfer Disable


#define SPI_ID          12

/*      Power management controller */
#define PMC_BASE  0xFFFFFC00
#define PMC_SCER  *(volatile U32*) (PMC_BASE + 0x00) // System Clock Enable Register
#define PMC_SCDR  *(volatile U32*) (PMC_BASE + 0x04) // System Clock Disable Register
#define PMC_SCSR  *(volatile U32*) (PMC_BASE + 0x08) // System Clock Status Register

#define PMC_PCER  *(volatile U32*) (PMC_BASE + 0x10)  /* Peripheral clock enable register */

/*********************************************************************
*
*       #define Macros
*
**********************************************************************
*/
#define SD_CS_PIN         3
#define SD_MISO_PIN       0
#define SD_MOSI_PIN       1
#define SD_CLK_PIN        2
#define SPI_CSR           SPI_CSR0


#define MMC_DEFAULTSUPPLYVOLTAGE  3300 /* in mV, example means 3.3V */
#define MCLK                     (79257600uL)  // may depend on PLL
#define MCLK_SPICLOCK             (MCLK)         // defaults is SD clock = MCLK


#define BUFFER_SIZE                   0x200
#define CACHE_LINE_SIZE               32

/*********************************************************************
*
*       Static data
*
**********************************************************************
*/
static char         _IsInited;
static U8           _sbcr = MCLK_SPICLOCK / 4000000UL;
static U32          _aBuffer[(BUFFER_SIZE + CACHE_LINE_SIZE) / 4];   // Used to transfer data from or to DMA
static const U8     _aFF[512] = {
  0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 
  0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 
  0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 
  0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 
  0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 
  0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 
  0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 
  0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 
  0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 
  0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 
  0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 
  0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 
  0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 
  0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 
  0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 
  0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 
  0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 
  0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 
  0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 
  0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 
  0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 
  0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 
  0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 
  0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 
  0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 
  0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 
  0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 
  0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 
  0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 
  0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 
  0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 
  0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff
};

/*********************************************************************
*
*       Static code
*
**********************************************************************
*/

/*********************************************************************
*
*       _PDC_SetTx
*/
static void _PDC_SetTx(const U8 * pAddress, unsigned NumBytes) {
  SPI_PDC_TPR = (unsigned int)pAddress;
  SPI_PDC_TCR = NumBytes;
}

/*********************************************************************
*
*       _PDC_SetRx
*/
static void _PDC_SetRx(U8 * pAddress, unsigned Bytes) {
  SPI_PDC_RPR = (unsigned int)pAddress;
  SPI_PDC_RCR = Bytes;
}

/*********************************************************************
*
*       _PDC_EnableTx
*/
static void _PDC_EnableTx(void) {
  SPI_PDC_PTCR = PDC_TXTEN;
}

/*********************************************************************
*
*       _PDC_EnableRx
*/
static void _PDC_EnableRx(void) {
  SPI_PDC_PTCR = PDC_RXTEN;
}

#if USE_OS
/**********************************************************
*
*       _IrqHandler
*/
static void _IrqHandler(void) {
  U32 v;

  //
  // Disable further interrupts
  //
  v  = SPI_IMR;
  SPI_IDR =  v;
  //
  // Signal (wake) the task waiting
  //
  FS_OS_SIGNAL();
}
#endif
/**********************************************************
*
*       _GetBufferAddr
*/
static U8 * _GetBufferAddr(void) {
  U8 * p;

  p = (U8 *)(((U32)&_aBuffer[0] + 0x1f) & ~0x1f);
  return p;
}

/*********************************************************************
*
*       _Init
*/
static void _Init(void) {
  //
  // Enable Power for PIOA and SPI block
  //
  PMC_PCER = (1 << SPI_ID)
           | (1 << PIOA_ID);
  //
  // Setup Pins
  //
  PIOA_PER =  0
            | (1 << SD_CS_PIN)
            | (1 << SD_MISO_PIN)
            | (1 << SD_MOSI_PIN)
            | (1 << SD_CLK_PIN)
            ;
  PIOA_SODR = 0
            | (1 << SD_CS_PIN)
            | (1 << SD_MOSI_PIN)
            | (1 << SD_CLK_PIN)
            ;
  PIOA_OER  = 0
            | (1 << SD_CS_PIN)
            | (1 << SD_MOSI_PIN)
            | (1 << SD_CLK_PIN)
            ;
  PIOA_ODR  = 0
            | (1 << SD_MOSI_PIN)
            ;
  PIOA_PDR =  0
            | (1 << SD_MISO_PIN)       // SPI-MISO
            | (1 << SD_MOSI_PIN)       // SPI-MOSI
            | (1 << SD_CLK_PIN)       // SPI-Clock
            ;
  PIOA_ASR  = 0
            |(1 << SD_MISO_PIN)       // SPI-MISO
            |(1 << SD_MOSI_PIN)       // SPI-MOSI
            |(1 << SD_CLK_PIN)       // SPI-Clock
            ;
  //
  // SPI
  //
  SPI_CR    = (1 << 7);      // Software reset
  SPI_MR    = 0
            |(1 << 0)         // 1 : Master mode
            |(0 << 1)         // 0 : Fixed chip select
            |(0 << 2)         // Chip select
            |(0 << 3)         // 0: Use MCLK as clock
            |(1 << 4)         // 1: Fault detection disable
            |(0 << 7)         // 1: Loopback
            |(0 << 16)        // 0000b: Use CS0
            ;
  SPI_CSR0  = 0
            |(1 << 0)         // 1 : Clock polarity of idle is high
            |(0 << 1)         // Clock hase sel
            |(1 << 3)         // Leave CS0 stay low
            |(0 << 4)         // 0000b: 8 bits per transfer
            |(_sbcr<< 8)         // 8..15: SCBR: Baud rate divider
            |(0x100000);
              ;
  SPI_CR    = (1 << 0);       // Enable SPI
#if USE_OS
  //
  // HW Init done
  // Setup IRQ handler
  //
  SPI_IDR = 0xFFFFFFFF;                                     // Disable all interrupts
  *(U32*)(0xFFFFF080 + 4 * SPI_ID)   = (U32)_IrqHandler;    // Set interrupt vector
  *(U32*)(0xFFFFF128)                = (1 << SPI_ID);       // Clear pending interrupt
  *(U32*)(0xFFFFF120)                = (1 << SPI_ID);       // Enable Interrupt
#endif
  _IsInited =1;
}


/*********************************************************************
*
*       Public code
*
**********************************************************************
*/

/*********************************************************************
*
*             FS_MMC_HW_X_EnableCS
*
*  Description:
*    FS low level function. Sets the card slot active using the
*    chip select (CS) line.
*
*  Parameters:
*    Unit      - Device Index
*
*  Return value:
*    void
*/

void FS_MMC_HW_X_EnableCS(U8 Unit) {
  PIOA_CODR  = (1 <<  SD_CS_PIN);       // CS0 on eval board
}

/*********************************************************************
*
*             FS_MMC_HW_X_EnableCS
*
*  Description:
*    FS low level function. Sets the card slot inactive using the
*    chip select (CS) line.
*
*  Parameters:
*    Unit      - Device Index
*
*  Return value:
*    void
*/

void FS_MMC_HW_X_DisableCS(U8 Unit) {
  PIOA_SODR  = (1 <<  SD_CS_PIN);       // CS0 on eval board
}

/*********************************************************************
*
*             FS_MMC_HW_X_IsWriteProtected
*
*  Description:
*    FS low level function. Returns the state of the physical write
*    protection of the SD cards.
*
*  Parameters:
*    Unit      - Device Index
*
*  Return value:
*    1                - the card is write protected
*    ==0              - the card is not write protected
*/

int FS_MMC_HW_X_IsWriteProtected(U8 Unit) {
  return 0;
}

/*********************************************************************
*
*             FS_MMC_HW_X_SetMaxSpeed
*
*  Description:
*    FS low level function. Sets the SPI interface to a maximum frequency.
*    Make sure that you set the frequency lower or equal but never higher
*    than the given value. Recommended startup frequency is 100kHz - 400kHz.
*
*  Parameters:
*    Unit       - Device Index
*    MaxFreq           - SPI clock frequency in kHz
*
*  Return value:
*    max. frequency    - the maximum frequency set in kHz
*    ==0               - the frequency could not be set
*/

U16 FS_MMC_HW_X_SetMaxSpeed(U8 Unit, U16 MaxFreq) {
  U32 InFreq;
  U32 SPIFreq;

  if (MaxFreq < 400) {
    MaxFreq = 400;
  }
  SPIFreq = 1000 * MaxFreq;
  if (SPIFreq >= 200000) {
    InFreq = MCLK_SPICLOCK;
  }
  _sbcr = (InFreq + SPIFreq - 1) / SPIFreq;
  //
  // Acc. to errata of chip, if the divider value is odd, 
  // then the SPI does not work properly with PDC.
  //
  if (_sbcr & 1) {
    _sbcr++;
  }
  _Init();
  return MaxFreq;    /* We are not faster than this */
}

/*********************************************************************
*
*             FS_MMC_HW_X_SetVoltage
*
*  Description:
*    FS low level function. Be sure that your card slot si within the given
*    voltage range. Return 1 if your slot can support the required voltage,
*    and if not, return 0;
*
*  Parameters:
*    Unit      - Device Index
*    MaxFreq          - SPI clock frequency in kHz
*
*  Return value:
*    1                - the card slot supports the voltage range
*    ==0              - the card slot does not support the voltage range
*/

int FS_MMC_HW_X_SetVoltage(U8 Unit, U16 Vmin, U16 Vmax) {
  /* voltage range check */
  char r;
  if((Vmin <= MMC_DEFAULTSUPPLYVOLTAGE) && (Vmax >= MMC_DEFAULTSUPPLYVOLTAGE)) {
    r = 1;
  } else {
    r = 0;
  }
  return r;
}

/*********************************************************************
*
*             FS_MMC_HW_X_IsPresent
*
*  Description:
*    Returns the state of the media. If you do not know the state, return
*    FS_MEDIA_STATE_UNKNOWN and the higher layer will try to figure out if
*    a media is present.
*
*  Parameters:
*    Unit                 - Device Index
*
*  Return value:
*    FS_MEDIA_STATE_UNKNOWN      - the state of the media is unkown
*    FS_MEDIA_NOT_PRESENT        - no card is present
*    FS_MEDIA_IS_PRESENT         - a card is present
*/

int FS_MMC_HW_X_IsPresent(U8 Unit) {
  if (_IsInited == 0) {
    _Init();
  }
  return FS_MEDIA_IS_PRESENT;
}

/*********************************************************************
*
*             FS_MMC_HW_X_Read
*
*  Description:
*    FS low level function. Reads a specified number of bytes from MMC
*    card to buffer.
*
*  Parameters:
*    Unit      - Device Index
*    pData            - Pointer to a data buffer
*    NumBytes         - Number of bytes
*
*  Return value:
*    void
*/
void FS_MMC_HW_X_Read (U8 Unit, U8 * pData, int NumBytes) {
  U8 * p;
  volatile int v;
  
  v = SPI_RDR;  // Do a dummy read to clear RDRF flag
  if (USE_PDC) {
    p = _GetBufferAddr();
    _PDC_SetRx(p, NumBytes);
    _PDC_SetTx(_aFF, NumBytes);
    _PDC_EnableRx();
    _PDC_EnableTx();
    //
    // For larger blocks of data, block task to save CPU time (if an OS is present)
    //
    if (USE_OS && (NumBytes >= 512)) {
      SPI_IER = SPI_RXBUFF;
      FS_OS_WAIT(1000);
    }
    //
    // Make sure we have received all bytes
    //
    while (SPI_PDC_RCR);
    OS_ARM_DCACHE_InvalidateRange(p, (((NumBytes + 0x1f) >> 5) << 5));
    FS_MEMCPY(pData, p , NumBytes);
  } else {
    do {
      SPI_TDR = 0xff;
      while ((SPI_SR & (1 << 0)) == 0);
      *pData++ = SPI_RDR;
    } while (--NumBytes);
  }
}

/*********************************************************************
*
*             FS_MMC_HW_X_Write
*
*  Description:
*    FS low level function. Writes a specified number of bytes from
*    data buffer to the MMC/SD card.
*
*  Parameters:
*    Unit      - Device Index
*    pData            - Pointer to a data buffer
*    NumBytes         - Number of bytes
*
*  Return value:
*    void
*/

void FS_MMC_HW_X_Write(U8 Unit, const U8 * pData, int NumBytes) {
  U8 * p;
  
  if (USE_PDC) {
    p = _GetBufferAddr();
    FS_MEMCPY(p, pData, NumBytes);
    OS_ARM_DCACHE_CleanRange(p, (((NumBytes + 0x1f) >> 5) << 5));
    //
    // Start transmission using the PDC
    //
    _PDC_SetTx(p, NumBytes);
    _PDC_EnableTx();
    //
    // For larger blocks of data, block task to save CPU time (if an OS is present)
    //
    if (USE_OS && (NumBytes >= 512)) {
      SPI_IER = SPI_TXBUFE;
      FS_OS_WAIT(1000);
    }
    //
    // Check PDC counter to make sure all bytes are transferred
    //
    while (SPI_PDC_TCR);
  } else {
    do {
      SPI_TDR = *pData++;
      while ((SPI_SR & (1 << 1)) == 0); // Check TDRE
    } while (--NumBytes);

  }
  //
  // Check TXEMPTY-flag to make sure the last byte has left the serializer (has been sent)
  //
  while ((SPI_SR & (1 << 9)) == 0);   // Check TXEMPTY for sureness
}

/*************************** End of file ****************************/
