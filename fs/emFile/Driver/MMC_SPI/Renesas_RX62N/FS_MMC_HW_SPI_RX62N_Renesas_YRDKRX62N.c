/*********************************************************************
*                SEGGER MICROCONTROLLER GmbH & Co. KG                *
*        Solutions for real time microcontroller applications        *
**********************************************************************
*                                                                    *
*        (c) 2003-2012     SEGGER Microcontroller GmbH & Co KG       *
*                                                                    *
*        Internet: www.segger.com    Support:  support@segger.com    *
*                                                                    *
**********************************************************************

**** emFile file system for embedded applications ****
emFile is protected by international copyright laws. Knowledge of the
source code may not be used to write a similar product. This file may
only be used in accordance with a license and should not be re-
distributed in any way. We appreciate your understanding and fairness.
----------------------------------------------------------------------
----------------------------------------------------------------------
File        : FS_MMC_HW_SPI_RX62N_Renesas_YRDKRX62N.c
Purpose     : Sample MMC SPI hardware layer for Renesas RX62N
Literature  : [1] \\fileserver\Techinfo\Company\Renesas\MCU\RX\RX62N_RX621\RX62N_UserManual_Rev1.00_100907.pdf
              [2] \\fileserver\techinfo\Company\Future\YRDKRX62N_4_SCH.pdf
---------------------------END-OF-HEADER------------------------------
*/

/*********************************************************************
*
*             #include Section
*
**********************************************************************
*/

#include "FS_Int.h"
#include "MMC_X_HW.h"

/*********************************************************************
*
*       Defines, Configurable
*
**********************************************************************
*/
#define PERIPH_CLOCK            48000000uL  // Input clock of RSPI in Hz
#define DEFAULT_SUPPLY_VOLTAGE  3300        // in mV, example: 3300 means 3.3V

/*********************************************************************
*
*       Defines, sfr
*
**********************************************************************
*/
#if defined __RX
  #define EVENACCESS  __evenaccess
#else
  #define EVENACCESS
#endif

//
// I/O Ports
//
#define PC_BASE_ADDR      0x0008C00C
#define PC_DDR            (*(volatile EVENACCESS U8 *)(PC_BASE_ADDR + 0x00))      // Data direction register
#define PC_DR             (*(volatile EVENACCESS U8 *)(PC_BASE_ADDR + 0x20))      // Data register
#define PC_ICR            (*(volatile EVENACCESS U8 *)(PC_BASE_ADDR + 0x60))      // Input buffer control register

//
// Serial Peripheral Interface (RSPI)
//
#define RSPI0_BASE_ADDR   0x00088300
#define RSPI0_SPCR        (*(volatile EVENACCESS U8 *) (RSPI0_BASE_ADDR + 0x80))  // Control register
#define RSPI0_SPSR        (*(volatile EVENACCESS U8 *) (RSPI0_BASE_ADDR + 0x83))  // Status register
#define RSPI0_SPDR        (*(volatile EVENACCESS U32 *)(RSPI0_BASE_ADDR + 0x84))  // Data register (32-bit)
#define RSPI0_SPSCR       (*(volatile EVENACCESS U8 *) (RSPI0_BASE_ADDR + 0x88))  // Sequence control register
#define RSPI0_SPBR        (*(volatile EVENACCESS U8 *) (RSPI0_BASE_ADDR + 0x8A))  // Bit rate register
#define RSPI0_SPDCR       (*(volatile EVENACCESS U8 *) (RSPI0_BASE_ADDR + 0x8B))  // Data control register
#define RSPI0_SPCR2       (*(volatile EVENACCESS U8 *) (RSPI0_BASE_ADDR + 0x8F))  // Control register 2
#define RSPI0_SPCMD0      (*(volatile EVENACCESS U16 *)(RSPI0_BASE_ADDR + 0x90))  // Command register 0

//
// Misc. registers
//
#define MSTPCRB           (*(volatile EVENACCESS U32 *)0x00080014)                // Module stop control register B
#define PFGSPI            (*(volatile EVENACCESS U8  *)0x0008C110)                // Port function control register G

//
// RSPI port function control bits
//
#define PFGSPI_RSPCKE   1
#define PFGSPI_MOSIE    2
#define PFGSPI_MISOE    3

//
// RSPI control register bits
//
#define SPCR_SPMS       0
#define SPCR_MODFEN     2
#define SPCR_MSTR       3
#define SPCR_SPE        6

//
// RSPI status register bits
//
#define SPSR_OVRF       0
#define SPSR_ILDNF      1
#define SPSR_MODF       2
#define SPSR_PERF       3
#define SPSR_SPTEF      5
#define SPSR_SPRF       7

//
// RSPI data control register bits
//
#define SPDCR_SPLW      5

//
// RSPI command register bits
//
#define SPCMD_CHPA      0
#define SPCMD_CPOL      1
#define SPCMD_SPB       8

//
// Misc. bit defines
//
#define SD_CS_PIN       4   // Chip select (PC)
#define SD_MISO_PIN     7   // Master in slave out (PC)
#define MSTPCRB_RSPI0   17  // Start/Stop RSPI0

/*********************************************************************
*
*       Static data
*
**********************************************************************
*/
static U8 _IsInited;

/*********************************************************************
*
*       Static code
*
**********************************************************************
*/

/*********************************************************************
*
*       _Init
*/
static void _Init(void) {
  //
  // Configure the chip select signal as output and enable the input buffer of MISO signal.
  //
  PC_DDR |= (1 << SD_CS_PIN);
  PC_ICR |= (1 << SD_MISO_PIN);
  //
  // The SD card is connected on port C. The chip select signal is controlled by the driver.
  //
  PFGSPI = (1 << PFGSPI_RSPCKE)
         | (1 << PFGSPI_MOSIE)
         | (1 << PFGSPI_MISOE)
         ;

  //
  // Enable the RSPI peripheral
  //
  MSTPCRB    &= ~(1u << MSTPCRB_RSPI0);
  //
  // Reset the RSPI peripheral.
  //
  RSPI0_SPCR  = 0x0;
  //
  // Clear RSPI error flags.
  //
  RSPI0_SPSR &= ~((1 << SPSR_OVRF) |
                  (1 << SPSR_MODF) |
                  (1 << SPSR_PERF));
  //
  // Single sequence using command 0 register.
  //
  RSPI0_SPSCR  = 0;
  RSPI0_SPDCR  = 1 << SPDCR_SPLW;     // 32-bit access to data register
  RSPI0_SPCMD0 = 0
               | (1 << SPCMD_CHPA)
               | (1 << SPCMD_CPOL)
               | (7 << SPCMD_SPB)     // 8 bit transfers
               ;
  RSPI0_SPCR2 = 0;
  //
  // Configure the RSSPI in master mode.
  //
  RSPI0_SPCR  = (1 << SPCR_SPMS)
              | (1 << SPCR_MSTR)
              | (1 << SPCR_SPE)
              ;
  //
  // Wait for RSPI to become ready.
  //
  while (RSPI0_SPSR & (1 << SPSR_ILDNF)) {
    ;
  }
  _IsInited = 1;
}

/*********************************************************************
*
*       _ClearErrors
*
*   Function description
*     Performs error recovery. By now only the receiver overrun error is handled.
*/
static void _ClearErrors(void) {
  volatile U32 Data;

  //
  // Clear overrun error if required. Use the same sequence as the one described in section "32.3.14 Error Handling" of [1]
  //
  if (RSPI0_SPSR & (1 << SPSR_OVRF)) {
    RSPI0_SPSR &= ~(1 << SPSR_OVRF);
    Data        = RSPI0_SPDR;
    if (RSPI0_SPSR & (1 << SPSR_SPTEF)) {    // Transmit buffer not empty ?
      RSPI0_SPCR &= ~(1 << SPCR_SPE);
      RSPI0_SPCR |=  (1 << SPCR_SPE);
    }
  }
}

/*********************************************************************
*
*       _WaitForTxBufferEmpty
*
*   Function description
*     Waits for RSPI to move the data from the transmit buffer to shift register.
*/
static void _WaitForTxBufferEmpty(void) {
  while ((RSPI0_SPSR & (1 << SPSR_SPTEF)) == 0) {   // Wait for data to be transmitted.
    ;
  }
}

/*********************************************************************
*
*       _WaitForRxBufferReady
*
*   Function description
*     Waits for RSPI to fill the receive buffer with data.
*/
static void _WaitForRxBufferReady(void) {
  while ((RSPI0_SPSR & (1 << SPSR_SPRF)) == 0) {    // Wait for data to be received.
    ;
  }
}

/*********************************************************************
*
*       _Read8Bits
*
*   Function description
*     Receives 8 bits from SD card.
*/
static void _Read8Bits(U8 * pData) {
  _WaitForTxBufferEmpty();
  //
  // Transfer 8 bits.
  //
  RSPI0_SPCMD0 &= ~(0xF << SPCMD_SPB);
  RSPI0_SPCMD0 |=   0x7 << SPCMD_SPB;
  //
  // Perform 1 transfer.
  //
  RSPI0_SPDCR  &= ~0x3;
  //
  // Start the data transfer.
  //
  RSPI0_SPDR    = 0xFF;
  _WaitForRxBufferReady();
  //
  // Store data to receive buffer.
  //
  *pData = (U8)RSPI0_SPDR;
}

/*********************************************************************
*
*       _Read32Bits
*
*   Function description
*     Receives 32 bits from SD card.
*/
static void _Read32Bits(U32 * pData) {
  U32 Data32;

  _WaitForTxBufferEmpty();
  //
  // Transfer 32 bits.
  //
  RSPI0_SPCMD0 &= ~(0xF << SPCMD_SPB);
  RSPI0_SPCMD0 |=   0x3 << SPCMD_SPB;
  //
  // Perform 1 transfer.
  //
  RSPI0_SPDCR  &= ~0x3;
  //
  // Start the data transfer.
  //
  RSPI0_SPDR = 0xFFFFFFFF;
  _WaitForRxBufferReady();
  Data32   = RSPI0_SPDR;
  *pData++ = FS_LoadU32BE((U8 *)&Data32);
}

/*********************************************************************
*
*       _Read128Bits
*
*   Function description
*     Receives 128 bits from SD card.
*/
static void _Read128Bits(U32 * pData) {
  U32 Data32;

  _WaitForTxBufferEmpty();
  //
  // Transfer 32 bits.
  //
  RSPI0_SPCMD0 &= ~(0xF << SPCMD_SPB);
  RSPI0_SPCMD0 |=   0x3 << SPCMD_SPB;
  //
  // Perform 4 transfers (128 bits in total).
  //
  RSPI0_SPDCR  |= 0x3;
  //
  // Data transfer starts when the last word is stored to buffer.
  //
  RSPI0_SPDR = 0xFFFFFFFF;
  RSPI0_SPDR = 0xFFFFFFFF;
  RSPI0_SPDR = 0xFFFFFFFF;
  RSPI0_SPDR = 0xFFFFFFFF;
  _WaitForRxBufferReady();
  Data32   = RSPI0_SPDR;
  *pData++ = FS_LoadU32BE((U8 *)&Data32);
  Data32   = RSPI0_SPDR;
  *pData++ = FS_LoadU32BE((U8 *)&Data32);
  Data32   = RSPI0_SPDR;
  *pData++ = FS_LoadU32BE((U8 *)&Data32);
  Data32   = RSPI0_SPDR;
  *pData++ = FS_LoadU32BE((U8 *)&Data32);
}

/*********************************************************************
*
*       _Write8Bits
*
*   Function description
*     Sends 8 bits over SPI to SD card.
*/
static void _Write8Bits(U8 Data) {
  volatile U32 Dummy32;

  _WaitForTxBufferEmpty();
  //
  // Transfer 8 bits.
  //
  RSPI0_SPCMD0 &= ~(0xF << SPCMD_SPB);
  RSPI0_SPCMD0 |=   0x7 << SPCMD_SPB;
  //
  // Perform 1 transfer.
  //
  RSPI0_SPDCR  &= ~0x3;
  //
  // Start the data transfer.
  //
  RSPI0_SPDR    = (U32)Data;
  _WaitForRxBufferReady();
  //
  // Read data from buffer to prevent a receive overrun error.
  //
  Dummy32 = RSPI0_SPDR;
}

/*********************************************************************
*
*       _Write32Bits
*
*   Function description
*     Sends 32 bits over SPI to SD card.
*/
static void _Write32Bits(U32 Data) {
  volatile U32 Dummy32;

  _WaitForTxBufferEmpty();
  //
  // Transfer 32 bits.
  //
  RSPI0_SPCMD0 &= ~(0xF << SPCMD_SPB);
  RSPI0_SPCMD0 |=   0x3 << SPCMD_SPB;
  //
  // Perform 1 transfer.
  //
  RSPI0_SPDCR  &= ~0x3;
  //
  // Start the data transfer.
  //
  RSPI0_SPDR = FS_LoadU32BE((U8 *)&Data);
  _WaitForRxBufferReady();
  Dummy32    = RSPI0_SPDR;
}

/*********************************************************************
*
*       _Write128Bits
*
*   Function description
*     Sends 32 bits over SPI to SD card.
*/
static void _Write128Bits(U32 * pData) {
  volatile U32 Dummy32;

  _WaitForTxBufferEmpty();
  //
  // Transfer 32 bits.
  //
  RSPI0_SPCMD0 &= ~(0xF << SPCMD_SPB);
  RSPI0_SPCMD0 |=   0x3 << SPCMD_SPB;
  //
  // Perform 4 transfers (128 bits in total).
  //
  RSPI0_SPDCR  |= 0x3;
  //
  // Data transfer starts when the last word is stored to buffer.
  //
  RSPI0_SPDR = FS_LoadU32BE((U8 *)pData++);
  RSPI0_SPDR = FS_LoadU32BE((U8 *)pData++);
  RSPI0_SPDR = FS_LoadU32BE((U8 *)pData++);
  RSPI0_SPDR = FS_LoadU32BE((U8 *)pData++);
  _WaitForRxBufferReady();
  Dummy32 = RSPI0_SPDR;
  Dummy32 = RSPI0_SPDR;
  Dummy32 = RSPI0_SPDR;
  Dummy32 = RSPI0_SPDR;
}

/*********************************************************************
*
*       Public code
*
**********************************************************************
*/

/*********************************************************************
*
*       FS_MMC_HW_X_EnableCS
*
*   Description
*     FS low level function. Sets the card slot active using the
*     chip select (CS) line.
*
*   Parameters
*     Unit    Device index
*/
void FS_MMC_HW_X_EnableCS(U8 Unit) {
  FS_USE_PARA(Unit);
  PC_DR &= ~(1 << SD_CS_PIN);           // Active low
}

/*********************************************************************
*
*       FS_MMC_HW_X_EnableCS
*
*   Description
*     FS low level function. Sets the card slot inactive using the
*     chip select (CS) line.
*
*   Parameters
*     Unit    Device index
*/
void FS_MMC_HW_X_DisableCS(U8 Unit) {
  FS_USE_PARA(Unit);
  PC_DR |= 1 << SD_CS_PIN;              // Active low
}

/*********************************************************************
*
*       FS_MMC_HW_X_IsWriteProtected
*
*   Description
*     FS low level function. Returns the state of the physical write
*     protection of the SD cards.
*
*   Parameters
*     Unit    Device index
*
*   Return value
*     ==1     The card is write protected
*     ==0     The card is writable
*/
int FS_MMC_HW_X_IsWriteProtected(U8 Unit) {
  FS_USE_PARA(Unit);
  return 0;           // Micro SD cards do not have a write protect switch.
}

/*********************************************************************
*
*       FS_MMC_HW_X_SetMaxSpeed
*
*   Description
*     FS low level function. Sets the SPI interface to a maximum frequency.
*     Make sure that you set the frequency lower or equal but never higher
*     than the given value. Recommended startup frequency is 100kHz - 400kHz.
*
*   Parameters
*     Unit      Device Index
*     MaxFreq   SPI clock frequency in kHz
*
*   Return value
*     max. frequency   The maximum frequency set in kHz
*     ==0              The frequency could not be set
*/

U16 FS_MMC_HW_X_SetMaxSpeed(U8 Unit, U16 MaxFreq) {
  U32 SPIFreq;
  U8  RegValue;

  FS_USE_PARA(Unit);
  SPIFreq   = 1000 * MaxFreq;
  SPIFreq  *= 2;
  RegValue  = (U8)((PERIPH_CLOCK + SPIFreq - 1) / SPIFreq);
  if (RegValue) {
    --RegValue;
  }
  MaxFreq   = PERIPH_CLOCK / (2 * (RegValue + 1)) / 1000;
  //
  // RSPI must be disabled when the bit rate register is modified (see the description of SPBR in [1])
  //
  RSPI0_SPCR &= ~(1 << SPCR_SPE);
  RSPI0_SPBR  = RegValue;
  RSPI0_SPCR |=  (1 << SPCR_SPE);
  return MaxFreq;
}

/*********************************************************************
*
*       FS_MMC_HW_X_SetVoltage
*
*   Description
*     FS low level function. Be sure that your card slot si within the given
*     voltage range. Return 1 if your slot can support the required voltage,
*     and if not, return 0;
*
*   Parameters
*     Unit        Device index
*     MaxFreq     SPI clock frequency in kHz
*
*  Return value
*     ==1     The card slot supports the voltage range
*     ==0     The card slot does not support the voltage range
*/
int FS_MMC_HW_X_SetVoltage(U8 Unit, U16 Vmin, U16 Vmax) {
  char r;

  FS_USE_PARA(Unit);
  r = 0;
  if ((Vmin <= DEFAULT_SUPPLY_VOLTAGE) && (Vmax >= DEFAULT_SUPPLY_VOLTAGE)) {
    r = 1;
  }
  return r;
}

/*********************************************************************
*
*       FS_MMC_HW_X_IsPresent
*
*   Description
*     Returns the state of the media. If you do not know the state, return
*     FS_MEDIA_STATE_UNKNOWN and the higher layer will try to figure out if
*     a media is present.
*
*   Parameters
*     Unit      Device Index
*
*   Return value
*     FS_MEDIA_STATE_UNKNOWN    The state of the media is unkown
*     FS_MEDIA_NOT_PRESENT      No card is present
*     FS_MEDIA_IS_PRESENT       A card is present
*/

int FS_MMC_HW_X_IsPresent(U8 Unit) {
  FS_USE_PARA(Unit);
  if (_IsInited == 0) {
    _Init();
  }
  return FS_MEDIA_IS_PRESENT;   // The micro SD card slot does not have a "card inserted" contact.
}

/*********************************************************************
*
*       FS_MMC_HW_X_Read
*
*   Description
*     FS low level function. Reads a specified number of bytes from MMC card to buffer.
*
*   Parameters
*     Unit      Device index
*     pData     [IN]  ---
*               [OUT] Data read from card
*     NumBytes  Number of bytes to read
*/
void FS_MMC_HW_X_Read(U8 Unit, U8 * pData, int NumBytes) {
  U32 NumWords;
  U32 NumBlocks;

  FS_USE_PARA(Unit);
  _ClearErrors();
  //
  // Transfer single bytes until the data pointer is 32-bit aligned.
  //
  while (1) {
    if (NumBytes == 0) {
      break;
    }
    if (((U32)pData & 0x3) == 0) {
      break;
    }
    _Read8Bits(pData);
    ++pData;
    --NumBytes;
  }
  //
  // OK, data pointer is 32-bit aligned. Try to read 128-bit blocks at a time as this is the most efficient way to transfer data.
  //
  if (NumBytes) {
    NumBlocks = NumBytes >> 4;
    if (NumBlocks) {
      NumBytes -= NumBlocks << 4;
      do {
        _Read128Bits((U32 *)pData);
        pData += 16;
      } while (--NumBlocks);
    }
  }
  //
  // Try to read the rest of the data as words.
  //
  if (NumBytes) {
    NumWords = NumBytes >> 2;
    if (NumWords) {
      NumBytes -= NumWords << 2;
      do {
        _Read32Bits((U32 *)pData);
        pData += 4;
      } while (--NumWords);
    }
  }
  //
  // Read the remaining data as single bytes.
  //
  if (NumBytes) {
    do {
      _Read8Bits(pData);
      ++pData;
    } while (--NumBytes);
  }
}

/*********************************************************************
*
*       FS_MMC_HW_X_Write
*
*   Description
*     FS low level function. Writes a specified number of bytes from
*     data buffer to the MMC/SD card.
*
*   Parameters
*     Unit        Device index
*     pData       [IN]  Data to be sent
*                 [OUT] ---
*     NumBytes    Number of bytes sent
*/

void FS_MMC_HW_X_Write(U8 Unit, const U8 * pData, int NumBytes) {
  U32 NumWords;
  U32 NumBlocks;

  FS_USE_PARA(Unit);
  _ClearErrors();
  //
  // Transfer single bytes until the data pointer is 32-bit aligned.
  //
  while (1) {
    if (NumBytes == 0) {
      break;
    }
    if (((U32)pData & 0x3) == 0) {
      break;
    }
    _Write8Bits(*pData);
    ++pData;
    --NumBytes;
  }
  //
  // OK, data pointer is 32-bit aligned. Try to read 128-bit blocks at a time as this is the most efficient way to transfer data.
  //
  if (NumBytes) {
    NumBlocks = NumBytes >> 4;
    if (NumBlocks) {
      NumBytes -= NumBlocks << 4;
      do {
        _Write128Bits((U32 *)pData);
        pData += 16;
      } while (--NumBlocks);
    }
  }
  //
  // Try to write the rest of the data as words.
  //
  if (NumBytes) {
    NumWords = NumBytes >> 2;
    if (NumWords) {
      NumBytes -= NumWords << 2;
      do {
        _Write32Bits(*(U32 *)pData);
        pData += 4;
      } while (--NumWords);
    }
  }
  //
  // Write the remaining data as single bytes.
  //
  if (NumBytes) {
    do {
      _Write8Bits(*pData);
      ++pData;
    } while (--NumBytes);
  }
}

/*************************** End of file ****************************/
