/*********************************************************************
*                SEGGER MICROCONTROLLER GmbH & Co. KG                *
*        Solutions for real time microcontroller applications        *
**********************************************************************
*                                                                    *
*        (c) 2003-2012     SEGGER Microcontroller GmbH & Co KG       *
*                                                                    *
*        Internet: www.segger.com    Support:  support@segger.com    *
*                                                                    *
**********************************************************************

**** emFile file system for embedded applications ****
emFile is protected by international copyright laws. Knowledge of the
source code may not be used to write a similar product. This file may
only be used in accordance with a license and should not be re-
distributed in any way. We appreciate your understanding and fairness.
----------------------------------------------------------------------
----------------------------------------------------------------------
File        : MMC_HW_TemplatePort.c
Purpose     : Sample MMC hardware layer for accessing MMC/SD 
              via port banging.
---------------------------END-OF-HEADER------------------------------
*/

#include "FS.h"
#include "MMC_X_HW.h"

/*********************************************************************
*
*       Configurable macros
*
*   Please setup these macros according your hardware
*
**********************************************************************
*/

#define MAX_CLK_FREQ        // TBD: Maximum frequency of the CLK signal in kHz. Depends on CPU speed.
#define DELAY_AT_100KHZ     // TBD: Number software loops needed to generate a 100kHz CLK signal. Depends on CPU speed.

#define MIN_SUPPLY_VOLTAGE  // TBD: Minimum supply voltage in mV. 3300 means 3.3V
#define MAX_SUPPLY_VOLTAGE  // TBD: Maximum supply voltage in mV. 3300 means 3.3V

#define CLR_CS()            // TBD: Sets CS pin to 0
#define SET_CS()            // TBD: Sets CS pin to 1
#define CLR_CLK()           // TBD: Sets CLK pin to 0
#define SET_CLK()           // TBD: Sets CLK pin to 1
#define CLR_MOSI()          // TBD: Sets MOSI pin to 0
#define SET_MOSI()          // TBD: Sets MOSI pin to 1
#define GET_MISO()          // TBD: Reads the level of MISO pin
#define SETUP_PINS()        // TBD: Configures the port pins: CS, CLK, MOSI as outputs, MISO as input

/*********************************************************************
*
*       Defines, not modifiable
*
**********************************************************************
*/

#define DELAY()                     do { int i_ = _Delay; while(i_-- != 0); } while(0)
#define RX_BIT(Bit, Byte)           CLR_CLK(); if (GET_MISO()) { Byte |= 1 << (Bit); } SET_CLK()
#define RX_BIT_THROTTLED(Bit, Byte) CLR_CLK(); DELAY(); if (GET_MISO()) { Byte |= 1 << (Bit); } SET_CLK(); DELAY()
#define TX_BIT(Bit, Byte)           if (Byte & (1 << Bit)) { SET_MOSI(); } else { CLR_MOSI(); } CLR_CLK(); SET_CLK()
#define TX_BIT_THROTTLED(Bit, Byte) if (Byte & (1 << Bit)) { SET_MOSI(); } else { CLR_MOSI(); } CLR_CLK(); DELAY(); SET_CLK(); DELAY()

/*********************************************************************
*
*       Static data
*
**********************************************************************
*/
static int _Delay;

/*********************************************************************
*
*             Local functions
*
**********************************************************************
*/

/*********************************************************************
*
*             _Init
*
*  Description:
*    Sets up the port pins and initializes the local variables.
*
*/

static void _Init(void) {
  _Delay = DELAY_AT_100KHZ;
  SETUP_PINS();
}

/*********************************************************************
*
*             _RxByte
*
*  Description:
*    Receives a single byte as fast as the CPU can drive the port pins.
*
*/

static U8 _RxByte(void) {
  U8 Data;

  Data = 0;
  RX_BIT(7, Data);
  RX_BIT(6, Data);
  RX_BIT(5, Data);
  RX_BIT(4, Data);
  RX_BIT(3, Data);
  RX_BIT(2, Data);
  RX_BIT(1, Data);
  RX_BIT(0, Data);
  return Data;
}

/*********************************************************************
*
*             _RxByteThrottled
*
*  Description:
*    Receives a single byte. Uses busy loops to keep the clock speed
*    within the configured frequency.
*
*/

static U8 _RxByteThrottled(void) {
  U8 Data;

  Data = 0;
  RX_BIT_THROTTLED(7, Data);
  RX_BIT_THROTTLED(6, Data);
  RX_BIT_THROTTLED(5, Data);
  RX_BIT_THROTTLED(4, Data);
  RX_BIT_THROTTLED(3, Data);
  RX_BIT_THROTTLED(2, Data);
  RX_BIT_THROTTLED(1, Data);
  RX_BIT_THROTTLED(0, Data);
  return Data;
}

/*********************************************************************
*
*             _TxByte
*
*  Description:
*    Sends a single byte as fast as the CPU can drive the port pins.
*
*/

static void _TxByte(U8 Data) {
  TX_BIT(7, Data);
  TX_BIT(6, Data);
  TX_BIT(5, Data);
  TX_BIT(4, Data);
  TX_BIT(3, Data);
  TX_BIT(2, Data);
  TX_BIT(1, Data);
  TX_BIT(0, Data);
}

/*********************************************************************
*
*             _TxByteThrottled
*
*  Description:
*    Sends a single byte. Uses busy loops to keep the clock speed
*    within the configured frequency.
*
*/

static void _TxByteThrottled(U8 Data) {
  TX_BIT_THROTTLED(7, Data);
  TX_BIT_THROTTLED(6, Data);
  TX_BIT_THROTTLED(5, Data);
  TX_BIT_THROTTLED(4, Data);
  TX_BIT_THROTTLED(3, Data);
  TX_BIT_THROTTLED(2, Data);
  TX_BIT_THROTTLED(1, Data);
  TX_BIT_THROTTLED(0, Data);
}

/*********************************************************************
*
*             FS_MMC_HW_X_EnableCS
*
*  Description:
*    FS low level function. Sets the card slot active using the
*    chip select (CS) line.
*
*  Parameters:
*    Unit      - Device Index
*
*  Return value:
*    void
*/

void FS_MMC_HW_X_EnableCS(U8 Unit) {
  CLR_CS();
}

/*********************************************************************
*
*             FS_MMC_HW_X_EnableCS
*
*  Description:
*    FS low level function. Sets the card slot inactive using the
*    chip select (CS) line.
*
*  Parameters:
*    Unit      - Device Index
*
*  Return value:
*    void
*/

void FS_MMC_HW_X_DisableCS(U8 Unit) {
  SET_CS();
}

/*********************************************************************
*
*             FS_MMC_HW_X_IsWriteProtected
*
*  Description:
*    FS low level function. Returns the state of the physical write
*    protection of the SD cards.
*
*  Parameters:
*    Unit      - Device Index
*
*  Return value:
*    1                - the card is write protected
*    ==0              - the card is not write protected
*/

int FS_MMC_HW_X_IsWriteProtected(U8 Unit) {
  // If the card slot has no write switch detector, return 0
  return 0;
}

/*********************************************************************
*
*             FS_MMC_HW_X_SetMaxSpeed
*
*  Description:
*    FS low level function. Sets the SPI interface to a maximum frequency.
*    Make sure that you set the frequency lower or equal but never higher
*    than the given value. Recommended startup frequency is 100kHz - 400kHz.
*
*  Parameters:
*    Unit       - Device Index
*    MaxFreq           - SPI clock frequency in kHz
*
*  Return value:
*    max. frequency    - the maximum frequency set in kHz
*    ==0               - the frequency could not be set
*/

U16 FS_MMC_HW_X_SetMaxSpeed(U8 Unit, U16 MaxFreq) {
  _Init();
  if (MaxFreq >= MAX_CLK_FREQ) {
    _Delay = 0;
    return MAX_CLK_FREQ;
  }
  _Delay = MaxFreq * DELAY_AT_100KHZ / (MAX_CLK_FREQ - 100);
  return MaxFreq;
}

/*********************************************************************
*
*             FS_MMC_HW_X_SetVoltage
*
*  Description:
*    FS low level function. Be sure that your card slot si within the given
*    voltage range. Return 1 if your slot can support the required voltage,
*    and if not, return 0;
*
*  Parameters:
*    Unit      - Device Index
*    MaxFreq          - SPI clock frequency in kHz
*
*  Return value:
*    1                - the card slot supports the voltage range
*    ==0              - the card slot does not support the voltage range
*/

int FS_MMC_HW_X_SetVoltage(U8 Unit, U16 Vmin, U16 Vmax) {
  int r;

  //
  // voltage range check
  //
  if((Vmin <= MIN_SUPPLY_VOLTAGE) && (Vmax >= MAX_SUPPLY_VOLTAGE)) {
    r = 1;
  } else {
    r = 0;
  }
  return r;
}

/*********************************************************************
*
*             FS_MMC_HW_X_IsPresent
*
*  Description:
*    Returns the state of the media. If you do not know the state, return
*    FS_MEDIA_STATE_UNKNOWN and the higher layer will try to figure out if
*    a media is present.
*
*  Parameters:
*    Unit                 - Device Index
*
*  Return value:
*    FS_MEDIA_STATE_UNKNOWN      - the state of the media is unkown
*    FS_MEDIA_NOT_PRESENT        - no card is present
*    FS_MEDIA_IS_PRESENT         - a card is present
*/

int FS_MMC_HW_X_IsPresent(U8 Unit) {
  return FS_MEDIA_STATE_UNKNOWN;
}

/*********************************************************************
*
*             FS_MMC_HW_X_Read
*
*  Description:
*    FS low level function. Reads a specified number of bytes from MMC
*    card to buffer.
*
*  Parameters:
*    Unit      - Device Index
*    pData            - Pointer to a data buffer
*    NumBytes         - Number of bytes
*
*  Return value:
*    void
*/

void FS_MMC_HW_X_Read (U8 Unit, U8 * pData, int NumBytes) {
  SET_MOSI();
  if (_Delay) {
    do {
      *pData++ = _RxByteThrottled();
    } while (--NumBytes);
  } else {
    do {
      *pData++ = _RxByte();
    } while (--NumBytes);
  }
}

/*********************************************************************
*
*             FS_MMC_HW_X_Write
*
*  Description:
*    FS low level function. Writes a specified number of bytes from
*    data buffer to the MMC/SD card.
*
*  Parameters:
*    Unit      - Device Index
*    pData            - Pointer to a data buffer
*    NumBytes         - Number of bytes
*
*  Return value:
*    void
*/

void FS_MMC_HW_X_Write(U8 Unit, const U8 * pData, int NumBytes) {
  if (_Delay) {
    do {
      _TxByteThrottled(*pData++);
    } while (--NumBytes);
      } else {
    do {
      _TxByte(*pData++);
    } while (--NumBytes);
      }
  //
  // Default state of output data line is high
  //
  SET_MOSI();
}

/*************************** End of file ****************************/
