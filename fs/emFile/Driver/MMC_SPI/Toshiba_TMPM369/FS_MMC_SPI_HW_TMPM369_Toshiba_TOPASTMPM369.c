/*********************************************************************
*                SEGGER MICROCONTROLLER GmbH & Co. KG                *
*        Solutions for real time microcontroller applications        *
**********************************************************************
*                                                                    *
*        (c) 2003-2012     SEGGER Microcontroller GmbH & Co KG       *
*                                                                    *
*        Internet: www.segger.com    Support:  support@segger.com    *
*                                                                    *
**********************************************************************

**** emFile file system for embedded applications ****
emFile is protected by international copyright laws. Knowledge of the
source code may not be used to write a similar product. This file may
only be used in accordance with a license and should not be re-
distributed in any way. We appreciate your understanding and fairness.
----------------------------------------------------------------------
----------------------------------------------------------------------
File        : FS_MMC_SPI_HW_TMPM369_Toshiba_TOPASTMPM369.c
Purpose     : Sample MMC SPI hardware layer for Toshiba TMPM369
Literature  : [1] \\fileserver\Techinfo\Company\Toshiba\MCU\Devices\CortexM3based\TMPM369\EvalBoard\Toshiba_BMSKTOPASM369\110914_topas369_schematics.pdf
              [2] \\fileserver\Techinfo\Company\Toshiba\MCU\Devices\CortexM3based\TMPM369\E-TMPM369FDFG_FYFG_20111114.pdf
---------------------------END-OF-HEADER------------------------------
*/

#include "FS_Int.h"
#include "MMC_X_HW.h"

/*********************************************************************
*
*       Defines, configurable
*
**********************************************************************
*/
#define USE_DMA                 1
#define PERIPH_CLOCK            80000000  // Input clock of SSP in Hz
#define DEFAULT_SUPPLY_VOLTAGE  3300      // in mV, example: 3300 means 3.3V

/*********************************************************************
*
*       Defines, sfr
*
**********************************************************************
*/
//
// Port B registers
//
#define PORTB_BASE_ADDR   0x400C0100
#define PBDATA            (*(volatile U32 *)(PORTB_BASE_ADDR + 0x00))   // Port B data register
#define PBCR              (*(volatile U32 *)(PORTB_BASE_ADDR + 0x04))   // Port B output control register
#define PBFR1             (*(volatile U32 *)(PORTB_BASE_ADDR + 0x08))   // Port B function register 1
#define PBFR2             (*(volatile U32 *)(PORTB_BASE_ADDR + 0x0C))   // Port B function register 2
#define PBFR3             (*(volatile U32 *)(PORTB_BASE_ADDR + 0x10))   // Port B function register 3
#define PBFR4             (*(volatile U32 *)(PORTB_BASE_ADDR + 0x14))   // Port B function register 4
#define PBOD              (*(volatile U32 *)(PORTB_BASE_ADDR + 0x28))   // Port B open-drain control register
#define PBPUP             (*(volatile U32 *)(PORTB_BASE_ADDR + 0x2C))   // Port B pull-up control register
#define PBPDN             (*(volatile U32 *)(PORTB_BASE_ADDR + 0x30))   // Port B pull-down control register
#define PBIE              (*(volatile U32 *)(PORTB_BASE_ADDR + 0x38))   // Port B input control register

//
// Port E registers
//
#define PORTE_BASE_ADDR   0x400C0400
#define PEDATA            (*(volatile U32 *)(PORTE_BASE_ADDR + 0x00))   // Port E data register
#define PECR              (*(volatile U32 *)(PORTE_BASE_ADDR + 0x04))   // Port E output control register
#define PEFR1             (*(volatile U32 *)(PORTE_BASE_ADDR + 0x08))   // Port E function register 1
#define PEFR2             (*(volatile U32 *)(PORTE_BASE_ADDR + 0x0C))   // Port E function register 2
#define PEFR3             (*(volatile U32 *)(PORTE_BASE_ADDR + 0x10))   // Port E function register 3
#define PEFR4             (*(volatile U32 *)(PORTE_BASE_ADDR + 0x14))   // Port E function register 4
#define PEOD              (*(volatile U32 *)(PORTE_BASE_ADDR + 0x28))   // Port E open-drain control register
#define PEPUP             (*(volatile U32 *)(PORTE_BASE_ADDR + 0x2C))   // Port E pull-up control register
#define PEPDN             (*(volatile U32 *)(PORTE_BASE_ADDR + 0x30))   // Port E pull-down control register
#define PEIE              (*(volatile U32 *)(PORTE_BASE_ADDR + 0x38))   // Port E input control register

//
// Synchronous Serial Port (SPP)
//
#define SPP2_BASE_ADDR    0x40042000
#define SSP2CR0           (*(volatile U32 *)(SPP2_BASE_ADDR + 0x00))    // Control register 0
#define SSP2CR1           (*(volatile U32 *)(SPP2_BASE_ADDR + 0x04))    // Control register 1
#define SSP2DR            (*(volatile U32 *)(SPP2_BASE_ADDR + 0x08))    // Receive FIFO (read) and transmit FIFO (write) data register
#define SSP2SR            (*(volatile U32 *)(SPP2_BASE_ADDR + 0x0C))    // Status register
#define SSP2CPSR          (*(volatile U32 *)(SPP2_BASE_ADDR + 0x10))    // Clock prescale register
#define SSP2IMSC          (*(volatile U32 *)(SPP2_BASE_ADDR + 0x14))    // Interrupt enable/disable register
#define SSP2RIS           (*(volatile U32 *)(SPP2_BASE_ADDR + 0x18))    // Pre-enable interrupt status register
#define SSP2MIS           (*(volatile U32 *)(SPP2_BASE_ADDR + 0x1C))    // Post-enable interrupt status register
#define SSP2ICR           (*(volatile U32 *)(SPP2_BASE_ADDR + 0x20))    // Interrupt clear register
#define SSP2DMACR         (*(volatile U32 *)(SPP2_BASE_ADDR + 0x24))    // DMA control register

//
// DMA
//
#define DMA2_BASE_ADDR          0x4004D000uL
#define DMA2_CFG                (*(volatile U32*)(DMA2_BASE_ADDR + 0x0004))   // DMA Configuration Register
#define DMA2_CTRL_BASE_PTR      (*(volatile U32*)(DMA2_BASE_ADDR + 0x0008))   // Channel Control Data Base Pointer Register
#define DMA2_CHNL_SW_REQUEST    (*(volatile U32*)(DMA2_BASE_ADDR + 0x0014))   // Channel software request ststus register
#define DMA2_CHNL_REQ_MASK_CLR  (*(volatile U32*)(DMA2_BASE_ADDR + 0x0024))   // Channel Request Mask Clear Register
#define DMA2_CHNL_ENABLE_SET    (*(volatile U32*)(DMA2_BASE_ADDR + 0x0028))   // Channel Enable Set Register
#define DMA2_CHNL_PRI_ALT_CLR   (*(volatile U32*)(DMA2_BASE_ADDR + 0x0034))   // Channel Alternate Clear Register
#define DMA2_ERR_CLR            (*(volatile U32*)(DMA2_BASE_ADDR + 0x004C))   // Bus Error Clear Register

//
// Assignment of DMA channels.
//
#define DMA_CHNL_SPP2_RX        8
#define DMA_CHNL_SPP2_TX        9

//
// Pin assignments of SD card signals
//
#define SD_CD_PIN     7   // Card detect    (PORT E)
#define SD_CS_PIN     5   // Chip select    (PORT D)
#define SD_MISO_PIN   4   // Data from card (PORT D)
#define SD_MOSI_PIN   3   // Data to card   (PORT D)
#define SD_CLK_PIN    2   // Clock          (PORT D)

//
// SSP control register flags
//
#define SSPxCR0_DSS   0
#define SSPxCR0_SPO   6
#define SSPxCR0_SPH   7
#define SSPxCR1_SSE   1

#define SSPxSR_BSY    4

//
// SSP DMA control flags
//
#define SSPxDMACR_RXDMAE        0
#define SSPxDMACR_TXDMAE        1

//
// Limits for the clock prescaler
//
#define DIVISOR_MIN_VALUE       8
#define DIVISOR_MAX_VALUE       254

//
// DMA global config flags
//
#define DMA2_CFG_EN             0

//
// DMA descriptor config flags
//
#define CFG_DST_INC_NONE        (3uL << 30)
#define CFG_SRC_INC_NONE        (3uL << 26)
#define CFG_N_MINUS_1_SHIFT     4
#define CFG_CYCLE_CTRL_BASIC    1uL
#define CFG_CYCLE_CTRL_MASK     0x7uL

#define NUM_DMA_CHANNELS        (DMA_CHNL_SPP2_TX + 1)

/*********************************************************************
*
*       Macros
*
**********************************************************************
*/

#if USE_DMA
  #ifdef __CC_ARM  // KEIL
    #define DELAY_1NS()  { __nop(); __nop(); __nop(); __nop(); __nop(); __nop(); __nop(); __nop(); __nop();  __nop(); \
                           __nop(); __nop(); __nop(); __nop(); __nop(); __nop(); __nop(); __nop(); __nop();  __nop(); \
                           __nop(); __nop(); __nop(); __nop(); __nop(); __nop(); __nop(); __nop(); __nop();  __nop(); \
                           __nop(); __nop(); __nop(); __nop(); __nop(); __nop(); __nop(); __nop(); __nop();  __nop(); \
                           __nop(); __nop(); __nop(); __nop(); __nop(); __nop(); __nop(); __nop(); __nop();  __nop(); \
                           __nop(); __nop(); __nop(); __nop(); __nop(); __nop(); __nop(); __nop(); __nop();  __nop(); \
                           __nop(); __nop(); __nop(); __nop(); __nop(); __nop(); __nop(); __nop(); __nop();  __nop(); \
                           __nop(); __nop(); __nop(); __nop(); __nop(); __nop(); __nop(); __nop(); __nop();  __nop(); }
  #endif
#endif

/*********************************************************************
*
*       Typedefs
*
**********************************************************************
*/
typedef struct {
  volatile U32 SrcEndAddr;   // Last address DMA reads from
  volatile U32 DestEndAddr;  // Last address DMA writes to
  volatile U32 Config;        // Transfer configuration
  volatile U32 Padding;
} DMA_DESC;

/*********************************************************************
*
*       Static data
*
**********************************************************************
*/
static U8 _IsInited;
#if USE_DMA
  static U32 _Dummy;
  //
  // The DMA descriptor table requires 4K byte alignment.
  // Only the primary descriptors are used
  //
  #ifdef __ICCARM__  // IAR
    #pragma data_alignment=4096
    static DMA_DESC _aDMADesc[NUM_DMA_CHANNELS];
  #else
    #ifdef __CC_ARM  // KEIL
      __align(4096) static DMA_DESC _aDMADesc[NUM_DMA_CHANNELS];
    #else
      #ifdef __GNUC__    // GCC
        static DMA_DESC _aDMADesc[NUM_DMA_CHANNELS] __attribute__ ((aligned (4096)));
  #else
    #error Data alignment not defined!
  #endif
    #endif
  #endif
#endif

/*********************************************************************
*
*       Static code
*
**********************************************************************
*/

/*********************************************************************
*
*       _Init
*/
static void _Init(void) {
  //
  // Configure the I/O pins driven by SPP.
  //
  PBCR  &=  ~(1uL << SD_MISO_PIN);
  PBCR  |=   (1uL << SD_MOSI_PIN)
        |    (1uL << SD_CLK_PIN)
        ;
  PBFR1 &= ~((1uL << SD_MOSI_PIN) |
             (1uL << SD_MISO_PIN) |
             (1uL << SD_CLK_PIN));
  PBFR2 |=   (1uL << SD_MOSI_PIN)
        |    (1uL << SD_MISO_PIN)
        |    (1uL << SD_CLK_PIN)
        ;
  PBFR3 &= ~((1uL << SD_MOSI_PIN) |
             (1uL << SD_MISO_PIN) |
             (1uL << SD_CLK_PIN));
  PBFR4 &= ~((1uL << SD_MOSI_PIN) |
             (1uL << SD_MISO_PIN) |
             (1uL << SD_CLK_PIN));
  PBOD  &= ~((1uL << SD_MOSI_PIN) |
             (1uL << SD_MISO_PIN) |
             (1uL << SD_CLK_PIN));
  PBPUP &= ~((1uL << SD_MOSI_PIN) |
             (1uL << SD_MISO_PIN) |
             (1uL << SD_CLK_PIN));
  PBPDN &= ~((1uL << SD_MOSI_PIN) |
             (1uL << SD_MISO_PIN) |
             (1uL << SD_CLK_PIN));
  PBIE  &= ~((1uL << SD_MOSI_PIN) |
             (1uL << SD_CLK_PIN));
  PBIE  |=    1uL << SD_MISO_PIN;
  //
  // Configure the chip select pin as output.
  //
  PBCR  |=   1uL << SD_CS_PIN;
  PBFR1 &= ~(1uL << SD_CS_PIN);
  PBFR2 &= ~(1uL << SD_CS_PIN);
  PBFR3 &= ~(1uL << SD_CS_PIN);
  PBFR4 &= ~(1uL << SD_CS_PIN);
  PBOD  &= ~(1uL << SD_CS_PIN);
  PBPUP &= ~(1uL << SD_CS_PIN);
  PBPDN &= ~(1uL << SD_CS_PIN);
  PBIE  &= ~(1uL << SD_CS_PIN);
  //
  // Configure the card detect pin as input.
  //
  PECR  &= ~(1uL << SD_CD_PIN);
  PEFR1 &= ~(1uL << SD_CD_PIN);
  PEFR2 &= ~(1uL << SD_CD_PIN);
  PEFR3 &= ~(1uL << SD_CD_PIN);
  PEFR4 &= ~(1uL << SD_CD_PIN);
  PEOD  &= ~(1uL << SD_CD_PIN);
  PEPUP &= ~(1uL << SD_CD_PIN);
  PEPDN &= ~(1uL << SD_CD_PIN);
  PEIE  |=   1uL << SD_CD_PIN;
  //
  // Configure SSP.
  //
  SSP2CR1   = 0;      // Disable the module
  SSP2CR0   = (1uL << SSPxCR0_SPO)
            | (1uL << SSPxCR0_SPH)
            | (7uL << SSPxCR0_DSS)      // 8-bit frame
            ;
  SSP2IMSC  = 0;      // Polling mode, no interrupts are required.
  SSP2ICR   = 0x3;    // Clear interrupts.
#if USE_DMA
  SSP2DMACR = (1uL << SSPxDMACR_RXDMAE)
            | (1uL << SSPxDMACR_TXDMAE)
            ;
  //
  // Initialize DMA.
  //
  DMA2_CTRL_BASE_PTR  = (U32)&_aDMADesc;
  DMA2_CFG           |= (1uL << DMA2_CFG_EN);   // Enable DMA.
#endif
  SSP2CR1  |= 1uL << SSPxCR1_SSE;               // Enable the SSP module.
  _IsInited = 1;
}

/*********************************************************************
*
*       Public code
*
**********************************************************************
*/

/*********************************************************************
*
*       _StartDMAWrite
*
*   Function description
*     Sends data to SD-Card over SPI using DMA.
*
*   Parameters
*     pData     [IN]  Data to send
*               [OUT] ---
*     NumBytes  Number of bytes to send
*/
#if USE_DMA
static void _StartDMAWrite(const U8 * pData, int NumBytes) {
  DMA_DESC * pDMADescTx;
  DMA_DESC * pDMADescRx;
  int        NumItemsMinus1;

  NumItemsMinus1 = NumBytes - 1;
  //
  // Configure the DMA channel that sends data
  //
  pDMADescTx              = &_aDMADesc[DMA_CHNL_SPP2_TX];
  pDMADescTx->SrcEndAddr  = (U32)(pData + NumItemsMinus1);
  pDMADescTx->DestEndAddr = (U32)&SSP2DR;
  pDMADescTx->Config      = CFG_DST_INC_NONE
                          | (NumItemsMinus1 << CFG_N_MINUS_1_SHIFT)
                          | CFG_CYCLE_CTRL_BASIC
                          ;
  //
  // Configure the DMA channel that receives data. All data is discarded
  //
  pDMADescRx              = &_aDMADesc[DMA_CHNL_SPP2_RX];
  pDMADescRx->SrcEndAddr  = (U32)&SSP2DR;
  pDMADescRx->DestEndAddr = (U32)&_Dummy;
  pDMADescRx->Config      = CFG_DST_INC_NONE
                          | CFG_SRC_INC_NONE
                          | (NumItemsMinus1 << CFG_N_MINUS_1_SHIFT)
                          | CFG_CYCLE_CTRL_BASIC
                          ;
  //
  // Start the transfer on both DMA channels
  //
  DMA2_CHNL_REQ_MASK_CLR = (1uL << DMA_CHNL_SPP2_TX)   // Enable the requests from peripheral
                         | (1uL << DMA_CHNL_SPP2_RX)
                         ;
  DMA2_CHNL_PRI_ALT_CLR  = (1uL << DMA_CHNL_SPP2_TX)   // Use the primary channel (disable the alternate one)
                         | (1uL << DMA_CHNL_SPP2_RX)
                         ;
  DMA2_CHNL_ENABLE_SET   = (1uL << DMA_CHNL_SPP2_TX)   // Start the data transfer.
                         | (1uL << DMA_CHNL_SPP2_RX)
                         ;
  //
  // The SSP module must be disabled and re-enabled again to really start the data transfer.
  //
  SSP2CR1 &= ~(1uL << SSPxCR1_SSE);
  SSP2CR1 |=  (1uL << SSPxCR1_SSE);
}
#endif

/*********************************************************************
*
*       _StartDMARead
*
*   Function description
*     Receives data from SD-Card over SPI using DMA.
*
*   Parameters
*     pData     [IN] ---
*               [OUT] Rata received from SD-Card
*     NumBytes  Number of bytes to receive
*/
#if USE_DMA
static void _StartDMARead(U8 * pData, int NumBytes) {
  DMA_DESC * pDMADescRx;
  DMA_DESC * pDMADescTx;
  int        NumItemsMinus1;

  NumItemsMinus1 = NumBytes - 1;
  _Dummy         = 0xFFFFFFFF;
  //
  // Configure the DMA channel that receives data
  //
  pDMADescRx              = &_aDMADesc[DMA_CHNL_SPP2_RX];
  pDMADescRx->SrcEndAddr  = (U32)&SSP2DR;
  pDMADescRx->DestEndAddr = (U32)(pData + NumItemsMinus1);
  pDMADescRx->Config      = CFG_SRC_INC_NONE
                          | (NumItemsMinus1 << CFG_N_MINUS_1_SHIFT)
                          | CFG_CYCLE_CTRL_BASIC
                          ;
  //
  // Configure the DMA channel that sends data. We need to send something in order to receive.
  //
  pDMADescTx              = &_aDMADesc[DMA_CHNL_SPP2_TX];
  pDMADescTx->SrcEndAddr  = (U32)&_Dummy;
  pDMADescTx->DestEndAddr = (U32)&SSP2DR;
  pDMADescTx->Config      = CFG_DST_INC_NONE
                          | CFG_SRC_INC_NONE
                          | (NumItemsMinus1 << CFG_N_MINUS_1_SHIFT)
                          | CFG_CYCLE_CTRL_BASIC
                          ;
  //
  // Start the transfer on both DMA channels
  //
  DMA2_CHNL_REQ_MASK_CLR = (1uL << DMA_CHNL_SPP2_TX)   // Enable the requests from peripheral
                         | (1uL << DMA_CHNL_SPP2_RX)
                         ;
  DMA2_CHNL_PRI_ALT_CLR  = (1uL << DMA_CHNL_SPP2_TX)   // Use the primary channel (disable the alternate one)
                         | (1uL << DMA_CHNL_SPP2_RX)
                         ;
  DMA2_CHNL_ENABLE_SET   = (1uL << DMA_CHNL_SPP2_TX)   // Enable the channels
                         | (1uL << DMA_CHNL_SPP2_RX)
                         ;
  //
  // The SSP module must be disabled and re-enabled again to really start the data transfer.
  //
  SSP2CR1 &= ~(1uL << SSPxCR1_SSE);
  SSP2CR1 |=  (1uL << SSPxCR1_SSE);
}
#endif

/*********************************************************************
*
*       _WaitForDMATransferEnd
*
*   Function description
*     Waits for the current DMA transfer to end.
*/
#if USE_DMA
static void _WaitForDMATransferEnd(void) {
  U32        Status;
  DMA_DESC * pDMADesc;

  do {
    //
    // Check for error first
    //
    Status = DMA2_ERR_CLR;
    if (Status) {
      DMA2_ERR_CLR = 1;     // Clear the error condition and exit
      break;
    }
    //
    // Check for the end of data transfer
    //
    pDMADesc  = &_aDMADesc[DMA_CHNL_SPP2_RX];
    Status    = pDMADesc->Config;
    pDMADesc  = &_aDMADesc[DMA_CHNL_SPP2_TX];
    Status   |= pDMADesc->Config;
    if ((Status & CFG_CYCLE_CTRL_MASK) == 0) {    // DMA controller sets the cycle control to 0 when all items have been transferred
      break;
    }
  } while (1);
}
#endif

/*********************************************************************
*
*       FS_MMC_HW_X_EnableCS
*
*   Description
*     FS low level function. Sets the card slot active using the
*     chip select (CS) line.
*
*   Parameters
*     Unit    Device index
*/
void FS_MMC_HW_X_EnableCS(U8 Unit) {
  FS_USE_PARA(Unit);
  PBDATA &= ~(1uL << SD_CS_PIN);    // Active low
}

/*********************************************************************
*
*       FS_MMC_HW_X_EnableCS
*
*   Description
*     FS low level function. Sets the card slot inactive using the
*     chip select (CS) line.
*
*   Parameters
*     Unit    Device index
*/
void FS_MMC_HW_X_DisableCS(U8 Unit) {
  FS_USE_PARA(Unit);
  PBDATA |= (1uL << SD_CS_PIN);     // Active low
}

/*********************************************************************
*
*       FS_MMC_HW_X_IsWriteProtected
*
*   Description
*     FS low level function. Returns the state of the physical write
*     protection of the SD cards.
*
*   Parameters
*     Unit    Device index
*
*   Return value
*     ==1     The card is write protected
*     ==0     The card is writable
*/
int FS_MMC_HW_X_IsWriteProtected(U8 Unit) {
  FS_USE_PARA(Unit);
  return 0;   // Micro SD cards do not have a write protected switch.
}

/*********************************************************************
*
*       FS_MMC_HW_X_SetMaxSpeed
*
*   Description
*     FS low level function. Sets the SPI interface to a maximum frequency.
*     Make sure that you set the frequency lower or equal but never higher
*     than the given value. Recommended startup frequency is 100kHz - 400kHz.
*
*   Parameters
*     Unit      Device Index
*     MaxFreq   SPI clock frequency in kHz
*
*   Return value
*     max. frequency   The maximum frequency set in kHz
*     ==0              The frequency could not be set
*/

U16 FS_MMC_HW_X_SetMaxSpeed(U8 Unit, U16 MaxFreq) {
  U32 SPIFreq;
  U32 Divisor;

  FS_USE_PARA(Unit);

  SPIFreq = 1000 * MaxFreq;
  Divisor = (PERIPH_CLOCK + SPIFreq - 1) / SPIFreq;
  //
  // Limit the clock prescaler according to [2] p. 498.
  //
  if        (Divisor < DIVISOR_MIN_VALUE) {
    Divisor  = DIVISOR_MIN_VALUE;
  } else if (Divisor > DIVISOR_MAX_VALUE) {
    Divisor  = DIVISOR_MAX_VALUE;
  } else {
    Divisor &= ~1uL;    // Divisor must be a multiple of 2
  }
  MaxFreq  = PERIPH_CLOCK / Divisor / 1000;
  SSP2CPSR = Divisor;
  return MaxFreq;       // We are not faster than this.
}

/*********************************************************************
*
*       FS_MMC_HW_X_SetVoltage
*
*   Description
*     FS low level function. Be sure that your card slot si within the given
*     voltage range. Return 1 if your slot can support the required voltage,
*     and if not, return 0;
*
*   Parameters
*     Unit        Device index
*     MaxFreq     SPI clock frequency in kHz
*
*  Return value
*     ==1     The card slot supports the voltage range
*     ==0     The card slot does not support the voltage range
*/
int FS_MMC_HW_X_SetVoltage(U8 Unit, U16 Vmin, U16 Vmax) {
  char r;

  FS_USE_PARA(Unit);
  if ((Vmin <= DEFAULT_SUPPLY_VOLTAGE) && (Vmax >= DEFAULT_SUPPLY_VOLTAGE)) {
    r = 1;
  } else {
    r = 0;
  }
  return r;
}

/*********************************************************************
*
*       FS_MMC_HW_X_IsPresent
*
*   Description
*     Returns the state of the media. If you do not know the state, return
*     FS_MEDIA_STATE_UNKNOWN and the higher layer will try to figure out if
*     a media is present.
*
*   Parameters
*     Unit      Device Index
*
*   Return value
*     FS_MEDIA_STATE_UNKNOWN    The state of the media is unkown
*     FS_MEDIA_NOT_PRESENT      No card is present
*     FS_MEDIA_IS_PRESENT       A card is present
*/

int FS_MMC_HW_X_IsPresent(U8 Unit) {
  FS_USE_PARA(Unit);
  if (_IsInited == 0) {
    _Init();
  }
#if 0   // Card detection does not work properly. The CD signal is high even when the card is inserted.
  return (PEDATA & (1 << SD_CD_PIN)) ? FS_MEDIA_NOT_PRESENT : FS_MEDIA_IS_PRESENT;
#else
  return FS_MEDIA_IS_PRESENT;
#endif
}

/*********************************************************************
*
*       FS_MMC_HW_X_Read
*
*   Description
*     FS low level function. Reads a specified number of bytes from MMC
*     card to buffer.
*
*   Parameters
*     Unit      Device index
*     pData     [IN]  ---
*               [OUT] Data read from card
*     NumBytes  Number of bytes to read
*/
void FS_MMC_HW_X_Read (U8 Unit, U8 * pData, int NumBytes) {
#if USE_DMA
  FS_USE_PARA(Unit);
  _StartDMARead(pData, NumBytes);
  #ifdef __CC_ARM  // KEIL
    //
    // There seems to be a problem with KEIL toolchain when using DMA read for 16 bytes.
    // The problem could not be located but seems to be fixed when giving the DMA 1ns
    // before accessing its registers again.
    //
    if (NumBytes == 0x10) {
      DELAY_1NS();
    }
  #endif
  _WaitForDMATransferEnd();
#else
  volatile U8 Data;

  FS_USE_PARA(Unit);
  do {
    SSP2DR = 0xFF;                          // Start the transfer of one byte
    while (SSP2SR & (1uL << SSPxSR_BSY)) {  // Wait for the byte to be received
      ;
    }
    *pData++ = SSP2DR;
  } while (--NumBytes);
#endif
}

/*********************************************************************
*
*       FS_MMC_HW_X_Write
*
*   Description
*     FS low level function. Writes a specified number of bytes from
*     data buffer to the MMC/SD card.
*
*   Parameters
*     Unit        Device index
*     pData       [IN]  Data to be sent
*                 [OUT] ---
*     NumBytes    Number of bytes sent
*/

void FS_MMC_HW_X_Write(U8 Unit, const U8 * pData, int NumBytes) {
#if USE_DMA
  FS_USE_PARA(Unit);
  _StartDMAWrite(pData, NumBytes);
  _WaitForDMATransferEnd();
#else
  volatile U8 Data;

  FS_USE_PARA(Unit);
  do {
    SSP2DR = *pData++;                      // Start the transfer of one byte
    while (SSP2SR & (1uL << SSPxSR_BSY)) {  // Wait for the byte to be received
      ;
    }
    Data = SSP2DR;
  } while (--NumBytes);
#endif
}

/*************************** End of file ****************************/
