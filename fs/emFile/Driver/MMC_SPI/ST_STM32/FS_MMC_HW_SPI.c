/*********************************************************************
*                SEGGER MICROCONTROLLER GmbH & Co. KG                *
*        Solutions for real time microcontroller applications        *
**********************************************************************
*                                                                    *
*        (c) 2003-2012     SEGGER Microcontroller GmbH & Co KG       *
*                                                                    *
*        Internet: www.segger.com    Support:  support@segger.com    *
*                                                                    *
**********************************************************************

**** emFile file system for embedded applications ****
emFile is protected by international copyright laws. Knowledge of the
source code may not be used to write a similar product. This file may
only be used in accordance with a license and should not be re-
distributed in any way. We appreciate your understanding and fairness.
----------------------------------------------------------------------
----------------------------------------------------------------------
File        : MMC_HW_Template.c
Purpose     : MMC hardware layer template.
---------------------------END-OF-HEADER------------------------------
*/

/*********************************************************************
*
*             #include Section
*
**********************************************************************
*/
#include "FS.h"
#include "MMC_X_HW.h"

/*********************************************************************
*
*             #define Macros
*
**********************************************************************
*/
#define MMC_DEFAULTSUPPLYVOLTAGE  3300   // in mV, example means 3.3V
#define PCLK                      36000  // Peripheral clock given in kHz

/***  sfrs ******/
#define _RCC_BASE      0x40021000L
#define _RCC_CFGR      *(volatile unsigned int*)(_RCC_BASE + 0x04)
#define _RCC_APB2RSTR  *(volatile unsigned int*)(_RCC_BASE + 0x0C)
#define _RCC_APB1RSTR  *(volatile unsigned int*)(_RCC_BASE + 0x10)
#define _RCC_AHBENR    *(volatile unsigned int*)(_RCC_BASE + 0x14)
#define _RCC_APB2ENR   *(volatile unsigned int*)(_RCC_BASE + 0x18)
#define _RCC_APB1ENR   *(volatile unsigned int*)(_RCC_BASE + 0x1C)

#define _GPIO_PB_BASE_ADDR        (0x40010C00)
#define _GPIO_PC_BASE_ADDR        (0x40011000)
#define _GPIOB_CRL          *(volatile unsigned *)(_GPIO_PB_BASE_ADDR + 0x00)
#define _GPIOB_CRH          *(volatile unsigned *)(_GPIO_PB_BASE_ADDR + 0x04)
#define _GPIOB_ODR          *(volatile unsigned *)(_GPIO_PB_BASE_ADDR + 0x0C)
#define _GPIOC_CRL          *(volatile unsigned *)(_GPIO_PC_BASE_ADDR + 0x00)
#define _GPIOC_IDR          *(volatile unsigned *)(_GPIO_PC_BASE_ADDR + 0x08)

#define _SPI1_BASE_ADDR        (0x40013000)
#define _SPI2_BASE_ADDR        (0x40003800)

#define _SPI1_CR1    *(volatile unsigned *)(_SPI1_BASE_ADDR + 0x00)
#define _SPI1_CR2    *(volatile unsigned *)(_SPI1_BASE_ADDR + 0x04)
#define _SPI1_SR     *(volatile unsigned *)(_SPI1_BASE_ADDR + 0x08)
#define _SPI1_DR     *(volatile unsigned *)(_SPI1_BASE_ADDR + 0x0C)
#define _SPI1_CRCPR  *(volatile unsigned *)(_SPI1_BASE_ADDR + 0x10)
#define _SPI1_RXCRCR *(volatile unsigned *)(_SPI1_BASE_ADDR + 0x14)
#define _SPI1_TXCRCR *(volatile unsigned *)(_SPI1_BASE_ADDR + 0x18)

#define _SPI2_CR1    *(volatile unsigned *)(_SPI2_BASE_ADDR + 0x00)
#define _SPI2_CR2    *(volatile unsigned *)(_SPI2_BASE_ADDR + 0x04)
#define _SPI2_SR     *(volatile unsigned *)(_SPI2_BASE_ADDR + 0x08)
#define _SPI2_DR     *(volatile unsigned *)(_SPI2_BASE_ADDR + 0x0C)
#define _SPI2_CRCPR  *(volatile unsigned *)(_SPI2_BASE_ADDR + 0x10)
#define _SPI2_RXCRCR *(volatile unsigned *)(_SPI2_BASE_ADDR + 0x14)
#define _SPI2_TXCRCR *(volatile unsigned *)(_SPI2_BASE_ADDR + 0x18)

/*********************************************************************
*
*       Static data
*
**********************************************************************
*/
static char _IsInited;
/*********************************************************************
*
*       Static code
*
**********************************************************************
*/

/*********************************************************************
*
*       _Init
*/
static void _Init(void) {
  if (_IsInited == 0) {
    _IsInited = 1;
    _RCC_APB2RSTR &= ~( (1 << 3)       // Remove RESET from PortB
                       |(1 << 4))      // Remove RESET from PortC
                   ;
    _RCC_APB2ENR  |= (1 << 3)          // Enable clock for PortB
                   | (1 << 4)          // Enable clock for PortC
                   ;
    _RCC_APB1RSTR &= ~(1 << 14)        // Remove RESET from SPI2
                   ;
    _RCC_APB1ENR  |=  (1 << 14)        // Enable clock for SPI2
                   ;
    //
    // Reset all port pins to reset state
    //
    _GPIOB_CRH &= ~(  (3 << 16)
                    | (3 << 18)
                    | (3 << 20)
                    | (3 << 22)
                    | (3 << 24)
                    | (3 << 26)
                    | (3 << 28)
                    | (3 << 30))
                    ;
    _GPIOB_CRH |= (3 << 16)    // PortB12 -> GPIO output, using PP (used for CS)
               |  (3 << 20)    // PortB13 -> Output mode, max 50MHz
               |  (2 << 22)    // PortB13 -> SPI_CLK
               |  (3 << 24)    // PortB14 -> Output mode, max 50MHz
               |  (2 << 26)    // PortB14 -> SPI_MOSI
               |  (3 << 28)    // PortB15 -> Output mode, max 50MHz
               |  (2 << 30)    // PortB15 -> SPI_MISO
               ;

    //
    // Reset all port pins to reset state
    //
    _GPIOC_CRL &= ~(  (3 << 24)
                    | (3 << 26)
                    | (3 << 28)
                    | (3 << 30))
                    ;
    _GPIOC_CRL |= (1 << 26)       //  Port C6 - floating input mode used as WP
               |  (1 << 30)       //  Port C7 - floating input mode used as CD
                ;
    //
    // Initialize SPI
    //
    _SPI2_CR1  = (0 << 0)         // Second clock transition is the first data capture edge
               | (0 << 1)         // Clock polarity is high when idle
               | (1 << 2)         // SPI is master
               | (7 << 3)         // Clock = pclk (72MHz) / 256
               | (1 << 8)  
               | (1 << 9)
               ;
    _SPI2_CR2  = 0;               // Neither interrupts nor DMA is used
    _SPI2_CR1 |= (1 << 6)         // Enable SPI
               ;
    
  }
}

/*********************************************************************
*
*       _ReadWriteSPI
*
*/
static U8 _ReadWriteSPI(U8 Data) {
  //Send Data
  _SPI2_DR = Data;
  //wait until all bits are shifted
  while (0 == (_SPI2_SR & (1 << 0)));
  //Read data
  return (U8)_SPI2_DR;
}


/*********************************************************************
*
*       Public code
*
**********************************************************************
*/
/*********************************************************************
*
*       FS_MMC_HW_X_EnableCS
*
*  Description:
*    FS low level function. Sets the card slot active using the
*    chip select (CS) line.
*
*  Parameters:
*    Unit      - Device Index
*
*  Return value:
*    void
*/
void FS_MMC_HW_X_EnableCS(U8 Unit) {
  _GPIOB_ODR &= ~(1 << 12);
}

/*********************************************************************
*
*       FS_MMC_HW_X_DisableCS
*
*  Description:
*    FS low level function. Clears the card slot inactive using the
*    chip select (CS) line.
*
*  Parameters:
*    Unit      - Device Index
*
*  Return value:
*    void
*/
void FS_MMC_HW_X_DisableCS(U8 Unit) {
  _GPIOB_ODR |= (1 << 12);
}

/*********************************************************************
*
*       FS_MMC_HW_X_IsWriteProtected
*
*  Description:
*    FS low level function. Returns the state of the physical write
*    protection of the SD cards.
*
*  Parameters:
*    Unit      - Device Index
*
*  Return value:
*    1                - the card is write protected
*    ==0              - the card is not write protected
*/
int FS_MMC_HW_X_IsWriteProtected(U8 Unit) {
  return (_GPIOC_IDR & (1 << 6)) ? 0 : 1;
}

/*********************************************************************
*
*       FS_MMC_HW_X_SetMaxSpeed
*
*  Description:
*    FS low level function. Sets the SPI interface to a maximum frequency.
*    Make sure that you set the frequency lower or equal but never higher
*    than the given value. Recommended startup frequency is 100kHz - 400kHz.
*
*  Parameters:
*    Unit       - Device Index
*    MaxFreq           - SPI clock frequency in kHz
*
*  Return value:
*    max. frequency    - the maximum frequency set in kHz
*    ==0               - the frequency could not be set
*/

U16 FS_MMC_HW_X_SetMaxSpeed(U8 Unit, U16 MaxFreq) {
  U32  Div;
  unsigned DivVal;
  
  Div    = 2;
  DivVal = 0;
  while ((MaxFreq * Div) < PCLK) {
    Div <<= 1;
    if (++DivVal == 7) {
      break;
    }
  }
  //
  // Disable the SPI intially before setting the new divider
  //
  // 
  _SPI2_CR1  &=  ~(1 << 6);
  //
  // Set the divider
  //
  _SPI2_CR1  &=  ~(7 << 3);
  _SPI2_CR1  |=  (DivVal << 3);
  //
  // Enable SPI
  //
  _SPI2_CR1  |=  (1 << 6);
  return PCLK / Div;    /* We are not faster than this */
}

/*********************************************************************
*
*       FS_MMC_HW_X_SetVoltage
*
*  Description:
*    FS low level function. Be sure that your card slot si within the given
*    voltage range. Return 1 if your slot can support the required voltage,
*    and if not, return 0;
*
*  Parameters:
*    Unit      - Device Index
*    MaxFreq          - SPI clock frequency in kHz
*
*  Return value:
*    1                - the card slot supports the voltage range
*    ==0              - the card slot does not support the voltage range
*/

int FS_MMC_HW_X_SetVoltage(U8 Unit, U16 Vmin, U16 Vmax) {
  return (char)MMC_DEFAULTSUPPLYVOLTAGE;
}

/*********************************************************************
*
*       FS_MMC_HW_X_IsPresent
*
*  Description:
*    Returns the state of the media. If you do not know the state, return
*    FS_MEDIA_STATE_UNKNOWN and the higher layer will try to figure out if
*    a media is present.
*
*  Parameters:
*    Unit                 - Device Index
*
*  Return value:
*    FS_MEDIA_STATE_UNKNOWN    - Media state is unknown
*    FS_MEDIA_NOT_PRESENT      - Media is not present
*    FS_MEDIA_IS_PRESENT       - Media is present
*/
int FS_MMC_HW_X_IsPresent(U8 Unit) {
  _Init();
  return (_GPIOC_IDR & (1 << 7)) ? FS_MEDIA_IS_PRESENT : FS_MEDIA_NOT_PRESENT;
}

/*********************************************************************
*
*       FS_MMC_HW_X_Read
*
*  Description:
*    FS low level function. Reads a specified number of bytes from MMC
*    card to buffer.
*
*  Parameters:
*    Unit      - Device Index
*    pData            - Pointer to a data buffer
*    NumBytes         - Number of bytes
*
*  Return value:
*    void
*/
void FS_MMC_HW_X_Read (U8 Unit, U8 * pData, int NumBytes) {
  do {
    *pData++ = _ReadWriteSPI(0xff);
  } while (--NumBytes);

}

/*********************************************************************
*
*       FS_MMC_HW_X_Write
*
*  Description:
*    FS low level function. Writes a specified number of bytes from
*    data buffer to the MMC/SD card.
*
*  Parameters:
*    Unit      - Device Index
*    pData            - Pointer to a data buffer
*    NumBytes         - Number of bytes
*
*  Return value:
*    void
*/
void FS_MMC_HW_X_Write(U8 Unit, const U8 * pData, int NumBytes) {
  do {
    _ReadWriteSPI(*pData++);
  } while (--NumBytes);
}

/*************************** End of file ****************************/
