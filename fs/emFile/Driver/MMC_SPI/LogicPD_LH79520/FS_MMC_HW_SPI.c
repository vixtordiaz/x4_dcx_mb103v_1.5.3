/*********************************************************************
*                SEGGER MICROCONTROLLER GmbH & Co. KG                *
*        Solutions for real time microcontroller applications        *
**********************************************************************
*                                                                    *
*        (c) 2003-2012     SEGGER Microcontroller GmbH & Co KG       *
*                                                                    *
*        Internet: www.segger.com    Support:  support@segger.com    *
*                                                                    *
**********************************************************************

**** emFile file system for embedded applications ****
emFile is protected by international copyright laws. Knowledge of the
source code may not be used to write a similar product. This file may
only be used in accordance with a license and should not be re-
distributed in any way. We appreciate your understanding and fairness.
----------------------------------------------------------------------
----------------------------------------------------------------------
File        : MMC_HW_SPI.c
Purpose     : Sample MMC hardware layer for LogicPD
---------------------------END-OF-HEADER------------------------------
*/

/*********************************************************************
*
*             #include Section
*
**********************************************************************
*/

#include "FS.h"
#include "MMC_X_HW.h"

/*********************************************************************
*
*             #define constants
*
**********************************************************************
*/

#define HW_USE_SPI 1

#define FS__MMC_DEFAULTSUPPLYVOLTAGE  3300 /* in mV, example means 3.3V */
#define SIO_FREQ                     12600 /* KHz, SysFreq/4            */

/*
 *  SPI implemetation for Sharp LH79520
 */
#define IOCONBASE 0xFFFE5000
#define LCDMUX    *(volatile U32*)(IOCONBASE + 0x004)
#define IOSSPMUX  *(volatile U32*)(IOCONBASE + 0x014)
#define UARTMUX   *(volatile U32*)(IOCONBASE + 0x010)

#define RCPC_BASE  0xFFFE2000
#define RCPC_CTRL     *(volatile U32*)(RCPC_BASE + 0x000)
#define RCPC_CLK_CTL2 *(volatile U32*)(RCPC_BASE + 0x028)
#define RCPC_CLK_SEL2 *(volatile U32*)(RCPC_BASE + 0x034)

/* LH79520 SPI register definitions */
#define SSPBASE   0xFFFC6000
#define SSPCR0  *(volatile U32*)(0x000 + SSPBASE)
#define SSPCR1  *(volatile U32*)(0x004 + SSPBASE)
#define SSPDR   *(volatile U8 *)(0x008 + SSPBASE)
#define SSPSR   *(volatile U32*)(0x00C + SSPBASE)
#define SSCPSR  *(volatile U32*)(0x010 + SSPBASE)
#define SSPIIR  *(volatile U32*)(0x014 + SSPBASE)
#define SSPICR  *(volatile U32*)(0x014 + SSPBASE)
#define SSPRXTO *(volatile U32*)(0x018 + SSPBASE)

#define RCPCBASE 0xFFFE2000
#define PERIPHCLKCTRL  *(volatile U32*)(RCPCBASE + 0x0024)
#define PERIPHCLKCTRL2 *(volatile U32*)(RCPCBASE + 0x0028)
#define SSPCLKPRESCALE *(volatile U32*)(RCPCBASE + 0x0044)

#define GPIOBASE0 0xFFFDF000
#define GPIOPADR  *(volatile U32*)(GPIOBASE0 + 0x0000)
#define GPIOPBDR  *(volatile U32*)(GPIOBASE0 + 0x0004)
#define GPIOPADDR *(volatile U32*)(GPIOBASE0 + 0x0008)
#define GPIOPBDDR *(volatile U32*)(GPIOBASE0 + 0x000C)

#define GPIOBASE1 0xFFFDE000
#define GPIOPCDR  *(volatile U32*)(GPIOBASE1 + 0x0000)
#define GPIOPDDR  *(volatile U32*)(GPIOBASE1 + 0x0004)
#define GPIOPCDDR *(volatile U32*)(GPIOBASE1 + 0x0008)
#define GPIOPDDDR *(volatile U32*)(GPIOBASE1 + 0x000C)

#define GPIOBASE2 0xFFFDD000
#define GPIOPEDR  *(volatile U32*)(GPIOBASE2 + 0x0000)
#define GPIOPFDR  *(volatile U32*)(GPIOBASE2 + 0x0004)
#define GPIOPEDDR *(volatile U32*)(GPIOBASE2 + 0x0008)
#define GPIOPFDDR *(volatile U32*)(GPIOBASE2 + 0x000C)

#define GPIOBASE3 0xFFFDC000
#define GPIOPGDR  *(volatile U32*)(GPIOBASE3 + 0x0000)
#define GPIOPHDR  *(volatile U32*)(GPIOBASE3 + 0x0004)
#define GPIOPGDDR *(volatile U32*)(GPIOBASE3 + 0x0008)
#define GPIOPHDDR *(volatile U32*)(GPIOBASE3 + 0x000C)

/*********************************************************************
*
*             #define Macros
*
**********************************************************************
*/
#define SPI_CLR_CS()          GPIOPADR &= ~(1 << 2)
#define SPI_SET_CS()          GPIOPADR |=  (1 << 2)

/*********************************************************************
*
*       Static data
*
**********************************************************************
*/
static volatile char _Dummy;

/*********************************************************************
*
*             Local functions
*
**********************************************************************
*/


/*********************************************************************
*
*       _Init
*
*/
static void _Init(void) {
  IOSSPMUX       |= 0x8;            // Set pins as SPI
  GPIOPADDR = (1 << 2)  /* PA2 output */
            | (1 << 1); /* PA1 output */
  PERIPHCLKCTRL2 &=  ~(1<<1);        // enable clock for SPI
  SSPCLKPRESCALE  =   1;             // prescaler /2 for SPI 
  SSCPSR          =   2;             // again prescaler /2 for SPI 
  SSPCR0          =  (1 << 8)       // Clock divisor
                    |(1  << 7)       // Clock phase   ... 1: SSPFRM = 0
                    |(1  << 6)       // Polarity
                    |(0  << 5)       // Frame format
                    |(0  << 4)       // Frame format
                    |(7  << 0);      // 8 bit data
  SSPCR1          =  (1  << 4)       // enable SPI
                    |(0  << 3)       // loopback mode
                    |(0  << 2)       // disable receive FIFO overrun interrupt
                    |(0  << 1)       // disable Transmit FIFO interrupt
                    |(0  << 1);      // disable Receive  FIFO interrupt
    _Dummy = SSPDR;
    _Dummy = SSPDR;
    _Dummy = SSPDR;
    _Dummy = SSPDR;
    _Dummy = SSPDR;
}

/*********************************************************************
*
*       FS_MMC_HW_X_EnableCS
*
*  Description:
*    FS low level function. Sets the card slot active using the
*    chip select (CS) line.
*
*  Parameters:
*    Unit      - Unit number
*
*  Return value:
*    void
*/
void FS_MMC_HW_X_EnableCS   (U8 Unit) {
  SPI_CLR_CS();
}

/*********************************************************************
*
*       FS_MMC_HW_X_EnableCS
*
*  Description:
*    FS low level function. Sets the card slot inactive using the
*    chip select (CS) line.
*
*  Parameters:
*    Unit      - Unit number
*
*  Return value:
*    void
*/

void FS_MMC_HW_X_DisableCS(U8 Unit) {
  SPI_SET_CS();
}

/*********************************************************************
*
*       FS_MMC_HW_X_IsWriteProtected
*
*  Description:
*    FS low level function. Returns the state of the physical write
*    protection of the SD cards.
*
*  Parameters:
*    Unit      - Unit number
*
*  Return value:
*    1                - the card is write protected
*    ==0              - the card is not write protected
*/
int FS_MMC_HW_X_IsWriteProtected(U8 Unit) {
  /* If the card slot has no write switch detector, return 0 */
  return 0;
}

/*********************************************************************
*
*       FS_MMC_HW_X_SetMaxSpeed
*
*  Description:
*    FS low level function. Sets the SPI interface to a maximum frequency.
*    Make sure that you set the frequency lower or equal but never higher
*    than the given value. Recommended startup frequency is 100kHz - 400kHz.
*
*  Parameters:
*    Unit       - Unit number
*    MaxFreq    - SPI clock frequency in kHz
*
*  Return value:
*    max. frequency    - the maximum frequency set in kHz
*    ==0               - the frequency could not be set
*/
U16 FS_MMC_HW_X_SetMaxSpeed(U8 Unit, U16 MaxFreq) {
  int Divider;
  _Init();
  SSPCR1  &= ~(1 << 4);      // Disable SPI
  Divider = SIO_FREQ / MaxFreq;
  if (Divider > 255) {
    Divider = 255;
  }
  SSPCR0  = (SSPCR0 & 0xff) | (Divider << 8);
  SSPCR1  |= (1 << 4);      // Enable SPI
  return SIO_FREQ / (Divider + 1);    /* We are not faster than this */
}

/*********************************************************************
*
*             FS_MMC_HW_X_SetVoltage
*
*  Description:
*    FS low level function. Be sure that your card slot si within the given
*    voltage range. Return 1 if your slot can support the required voltage,
*    and if not, return 0;
*
*  Parameters:
*    Unit      - Unit number
*    MaxFreq          - SPI clock frequency in kHz
*
*  Return value:
*    1                - the card slot supports the voltage range
*    ==0              - the card slot does not support the voltage range
*/

int FS_MMC_HW_X_SetVoltage(U8 Unit, U16 Vmin, U16 Vmax) {
  char r;
  /* voltage range check */
  if((Vmin <= FS__MMC_DEFAULTSUPPLYVOLTAGE) && (Vmax >= FS__MMC_DEFAULTSUPPLYVOLTAGE)) {
    r = 1;
  } else {
    r = 0;
  }
  return r;
}

/*********************************************************************
*
*       FS_MMC_HW_X_IsPresent
*
*  Description:
*    Returns the state of the media. If you do not know the state, return
*    FS_MEDIA_STATE_UNKNOWN and the higher layer will try to figure out if
*    a media is present.
*
*  Parameters:
*    Unit                 - Unit number
*
*  Return value:
*    FS_MEDIA_STATE_UNKNOWN      - the state of the media is unkown
*    FS_MEDIA_NOT_PRESENT        - no card is present
*    FS_MEDIA_IS_PRESENT         - a card is present
*/
int FS_MMC_HW_X_IsPresent(U8 Unit) {
  return FS_MEDIA_STATE_UNKNOWN;
}

/*********************************************************************
*
*       FS_MMC_HW_X_Read
*
*  Description:
*    FS low level function. Reads a specified number of bytes from MMC
*    card to buffer.
*
*  Parameters:
*    Unit        - Unit number
*    pData       - Pointer to a data buffer
*    NumBytes    - Number of bytes
*
*  Return value:
*    void
*/
void FS_MMC_HW_X_Read(U8 Unit, U8 * pData, int NumBytes) {
  SSPDR = 0xff;
  if (--NumBytes) {
    do {
      SSPDR = 0xff;
      while ((SSPSR & (1 << 2)) == 0);  /* Wait until we received a byte */
      *pData++ = SSPDR;
    } while (--NumBytes);
  } 
  while ((SSPSR & (1 << 2)) == 0);  /* Wait until we received a byte */
  *pData = SSPDR;
}

/*********************************************************************
*
*       FS_MMC_HW_X_Write
*
*  Description:
*    FS low level function. Writes a specified number of bytes from
*    data buffer to the MMC/SD card.
*
*  Parameters:
*    Unit        - Unit number
*    pData       - Pointer to a data buffer
*    NumBytes    - Number of bytes
*
*  Return value:
*    void
*/
void FS_MMC_HW_X_Write(U8 Unit, const U8 * pData, int NumBytes) {
  while ((SSPSR & (1 << 4)) != 0);  /* Busy ? */
  do {
    SSPDR = *pData++;
    while ((SSPSR & (1 << 2)) == 0);  /* Wait until we received a byte */
    _Dummy = SSPDR;
  } while (--NumBytes);
}

/****** EOF *********************************************************/
