/*********************************************************************
*                SEGGER MICROCONTROLLER GmbH & Co. KG                *
*        Solutions for real time microcontroller applications        *
**********************************************************************
*                                                                    *
*        (c) 2003-2012     SEGGER Microcontroller GmbH & Co KG       *
*                                                                    *
*        Internet: www.segger.com    Support:  support@segger.com    *
*                                                                    *
**********************************************************************

**** emFile file system for embedded applications ****
emFile is protected by international copyright laws. Knowledge of the
source code may not be used to write a similar product. This file may
only be used in accordance with a license and should not be re-
distributed in any way. We appreciate your understanding and fairness.
----------------------------------------------------------------------
----------------------------------------------------------------------
File        : FS_MMC_HW_SPI_LPC1768_KEIL_MCB1700.c
Purpose     : Sample MMC hardware layer for NXP LPC17xx series
---------------------------END-OF-HEADER------------------------------
*/

#include "FS.h"
#include "MMC_X_HW.h"


#define _CRYSTAL_CLOCK          12000000

/*********************************************************************
*
*       Defines
*
**********************************************************************
*/
/* SPI0 (Serial Peripheral Interface 0) */
#define SSPCR0        *(volatile U32 *)(0x40088000)
#define SSPCR1        *(volatile U32 *)(0x40088004)
#define SSPDR         *(volatile U32 *)(0x40088008)
#define SSPSR         *(volatile U32 *)(0x4008800C)
#define SSPCPSR       *(volatile U32 *)(0x40088010)
#define SSPIMSC       *(volatile U32 *)(0x40088014)
#define SSPRIS        *(volatile U32 *)(0x40088018)
#define SSPMIS        *(volatile U32 *)(0x4008801C)
#define SSPICR        *(volatile U32 *)(0x40088020)

/* General Purpose Input/Output (GPIO) */
#define FIO0SET       *(volatile U32 *)(0x2009C018)
#define FIO0DIR       *(volatile U32 *)(0x2009C000)
#define FIO0CLR       *(volatile U32 *)(0x2009C01C)
#define FIO4PIN       *(volatile U32 *)(0x2009C094)

/* Pin Connect Block */
#define PINSEL0       *(volatile U32 *)(0x4002C000)
#define PINSEL1       *(volatile U32 *)(0x4002C004)
#define PCONP         *(volatile U32 *)(0x400FC0C4)
#define PLLSTAT       *(volatile U32 *)(0x400FC088)

/* Port bits */
#define PCONP_SSP0_BIT    (21)
#define PINSEL0_SCK_BIT   (30)
#define PINSEL1_MISO_BIT   (2)
#define PINSEL1_MOSI_BIT   (4)
#define FIO_CS_BIT        (16)
#define FIO_CD_BIT        (29)

#define MMC_DEFAULTSUPPLYVOLTAGE  3300 /* in mV, example means 3.3V */

/*********************************************************************
*
*       Static data
*
**********************************************************************
*/
static char _IsInited;

/*********************************************************************
*
*       Static code
*
**********************************************************************
*/

/*********************************************************************
*
*       _Init
*
*/
static void _Init(void) {
  if (_IsInited == 0) {
    U8  i, Dummy;


    PCONP   |= (1 << PCONP_SSP0_BIT);     // Enable clock for SPI
    SSPCR1   = 0x00;                      // SSP master (off) in normal mode
    // Configure PIN connect block
    PINSEL0 |= (2UL << PINSEL0_SCK_BIT);  //Set Port 0.15 to SCK0
    PINSEL1 |= (2UL << PINSEL1_MISO_BIT)  //Set Port 0.17 to MISO0
            |  (2UL << PINSEL1_MOSI_BIT)  //Set Port 0.18 to MOSI0
            ;
    FIO0DIR  = 1 << FIO_CS_BIT;           // CS is output
    FIO0SET  = 1 << FIO_CS_BIT;           // Set CS to high
    SSPCR0   = 0x0007;                    // Set data to 8-bit, Frame format SPI, CPOL = 0, CPHA = 0 and SCR is 0
    SSPCPSR  = 0x2;                       // SSPCPSR clock prescale register, master mode, minimum divisor is 0x02
    //
    // Device select as master, SSP Enabled, normal operational mode
    //
    SSPCR1 = 0x02;
    for ( i = 0; i < 8; i++ ) {
      Dummy = SSPDR;                         // Flush RxFIFO
      FS_USE_PARA(Dummy);
    }
    _IsInited = 1;
  }
}

/*********************************************************************
*
*       FS_MMC_HW_X_EnableCS
*
*/
void FS_MMC_HW_X_EnableCS(U8 Unit) {
  FIO0CLR = (1 << FIO_CS_BIT);
}


/*********************************************************************
*
*       FS_MMC_HW_X_DisableCS
*
*/
void FS_MMC_HW_X_DisableCS(U8 Unit) {
  FIO0SET = (1 << FIO_CS_BIT);
}


/*********************************************************************
*
*       FS_MMC_HW_X_IsPresent
*
*/
int FS_MMC_HW_X_IsPresent(U8 Unit) {
  _Init();
  if (FIO4PIN & (1 << FIO_CD_BIT)) {
    return FS_MEDIA_NOT_PRESENT;
  } else {
    return FS_MEDIA_IS_PRESENT;
  }
}

/*********************************************************************
*
*       FS_MMC_HW_X_IsWriteProtected
*
*/
int FS_MMC_HW_X_IsWriteProtected(U8 Unit) {
  return 0;
}


/*********************************************************************
*
*       FS_MMC_HW_X_SetMaxSpeed
*
*/
U16 FS_MMC_HW_X_SetMaxSpeed(U8 Unit, U16 MaxFreq) {
  unsigned Prescaler;
  U32      PeriphalClock;
  U32      PLLStatus;
  _Init();

  PeriphalClock = _CRYSTAL_CLOCK;
  PLLStatus = PLLSTAT;
  // PLL enabled ?
  if (PLLStatus & (3 << 8)) {
    PeriphalClock *= ((PLLStatus & 0x0f) + 1);
  }
  // Calc prescaler value.
  Prescaler = ((PeriphalClock / 1000) + MaxFreq - 1) / MaxFreq;
  Prescaler = ((Prescaler + 1) >> 1) << 1;
  if (Prescaler < 2) {
    Prescaler = 2;
  }
  SSPCPSR  = Prescaler;
  return (PeriphalClock / 1000) / Prescaler;
}

/*********************************************************************
*
*       FS_MMC_HW_X_SetVoltage
*
*/
int FS_MMC_HW_X_SetVoltage(U8 Unit, U16 Vmin, U16 Vmax) {
  return 1;
}


/*********************************************************************
*
*       FS_MMC_HW_X_Read
*
*/
void FS_MMC_HW_X_Read(U8 Unit, U8 * pData, int NumBytes) {
  int i;

  for (i = 0; i < NumBytes; i++) {
    // Wrtie dummy byte out to generate clock
    // to read data from MISO
    SSPDR = 0xFF;
    // Wait until the Busy bit is cleared
    while ( SSPSR & (1 << 4));
    *pData++ = SSPDR;
  }
}

/*********************************************************************
*
*       FS_MMC_HW_X_Write
*
*/
void FS_MMC_HW_X_Write(U8 Unit, const U8 * pData, int NumBytes) {
  U8 Dummy;

  do {
    //
    // As long as TNF bit is set,
    // TxFIFO is not full -> writing to the FIFO allowed
    //
    while ((SSPSR & (1 << 1)) == 0);
    SSPDR = *pData;
    // Wait until the Busy bit is cleared
    while ((SSPSR & (1 << 0x04)));
    Dummy = SSPDR; /* Flush the RxFIFO */
    FS_USE_PARA(Dummy);
    pData++;
  } while (--NumBytes);
}
/*************************** End of file ****************************/
