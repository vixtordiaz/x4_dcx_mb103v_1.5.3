/*********************************************************************
*                SEGGER MICROCONTROLLER GmbH & Co. KG                *
*        Solutions for real time microcontroller applications        *
**********************************************************************
*                                                                    *
*        (c) 2003-2012     SEGGER Microcontroller GmbH & Co KG       *
*                                                                    *
*        Internet: www.segger.com    Support:  support@segger.com    *
*                                                                    *
**********************************************************************

**** emFile file system for embedded applications ****
emFile is protected by international copyright laws. Knowledge of the
source code may not be used to write a similar product. This file may
only be used in accordance with a license and should not be re-
distributed in any way. We appreciate your understanding and fairness.
----------------------------------------------------------------------
----------------------------------------------------------------------
File        : MMC_HW_Template.c
Purpose     : MMC hardware layer template.
---------------------------END-OF-HEADER------------------------------
*/

/*********************************************************************
*
*             #include Section
*
**********************************************************************
*/
#include "FS.h"
#include "MMC_X_HW.h"

/*********************************************************************
*
*             #define Macros
*
**********************************************************************
*/
#define MMC_DEFAULTSUPPLYVOLTAGE  3300 /* in mV, example means 3.3V */

/*********************************************************************
*
*       Public code
*
**********************************************************************
*/
/*********************************************************************
*
*       FS_MMC_HW_X_EnableCS
*
*  Description:
*    FS low level function. Sets the card slot active using the
*    chip select (CS) line.
*
*  Parameters:
*    Unit      - Device Index
*
*  Return value:
*    void
*/
void FS_MMC_HW_X_EnableCS(U8 Unit) {
}

/*********************************************************************
*
*       FS_MMC_HW_X_DisableCS
*
*  Description:
*    FS low level function. Clears the card slot inactive using the
*    chip select (CS) line.
*
*  Parameters:
*    Unit      - Device Index
*
*  Return value:
*    void
*/
void FS_MMC_HW_X_DisableCS(U8 Unit) {
}

/*********************************************************************
*
*       FS_MMC_HW_X_IsWriteProtected
*
*  Description:
*    FS low level function. Returns the state of the physical write
*    protection of the SD cards.
*
*  Parameters:
*    Unit      - Device Index
*
*  Return value:
*    1                - the card is write protected
*    ==0              - the card is not write protected
*/
int FS_MMC_HW_X_IsWriteProtected(U8 Unit) {
  return 0;
}

/*********************************************************************
*
*       FS_MMC_HW_X_SetMaxSpeed
*
*  Description:
*    FS low level function. Sets the SPI interface to a maximum frequency.
*    Make sure that you set the frequency lower or equal but never higher
*    than the given value. Recommended startup frequency is 100kHz - 400kHz.
*
*  Parameters:
*    Unit       - Device Index
*    MaxFreq           - SPI clock frequency in kHz
*
*  Return value:
*    max. frequency    - the maximum frequency set in kHz
*    ==0               - the frequency could not be set
*/

U16 FS_MMC_HW_X_SetMaxSpeed(U8 Unit, U16 MaxFreq) {
  return MaxFreq;    /* We are not faster than this */
}

/*********************************************************************
*
*       FS_MMC_HW_X_SetVoltage
*
*  Description:
*    FS low level function. Be sure that your card slot si within the given
*    voltage range. Return 1 if your slot can support the required voltage,
*    and if not, return 0;
*
*  Parameters:
*    Unit      - Device Index
*    MaxFreq          - SPI clock frequency in kHz
*
*  Return value:
*    1                - the card slot supports the voltage range
*    ==0              - the card slot does not support the voltage range
*/

int FS_MMC_HW_X_SetVoltage(U8 Unit, U16 Vmin, U16 Vmax) {
  return (char)MMC_DEFAULTSUPPLYVOLTAGE;
}

/*********************************************************************
*
*       FS_MMC_HW_X_IsPresent
*
*  Description:
*    Returns the state of the media. If you do not know the state, return
*    FS_MEDIA_STATE_UNKNOWN and the higher layer will try to figure out if
*    a media is present.
*
*  Parameters:
*    Unit                 - Device Index
*
*  Return value:
*    FS_MEDIA_STATE_UNKNOWN    - Media state is unknown
*    FS_MEDIA_NOT_PRESENT      - Media is not present
*    FS_MEDIA_IS_PRESENT       - Media is present
*/
int FS_MMC_HW_X_IsPresent(U8 Unit) {
  return FS_MEDIA_IS_PRESENT;
}

/*********************************************************************
*
*       FS_MMC_HW_X_Read
*
*  Description:
*    FS low level function. Reads a specified number of bytes from MMC
*    card to buffer.
*
*  Parameters:
*    Unit      - Device Index
*    pData            - Pointer to a data buffer
*    NumBytes         - Number of bytes
*
*  Return value:
*    void
*/
void FS_MMC_HW_X_Read (U8 Unit, U8 * pData, int NumBytes) {
}

/*********************************************************************
*
*       FS_MMC_HW_X_Write
*
*  Description:
*    FS low level function. Writes a specified number of bytes from
*    data buffer to the MMC/SD card.
*
*  Parameters:
*    Unit      - Device Index
*    pData            - Pointer to a data buffer
*    NumBytes         - Number of bytes
*
*  Return value:
*    void
*/
void FS_MMC_HW_X_Write(U8 Unit, const U8 * pData, int NumBytes) {
}

/*************************** End of file ****************************/
