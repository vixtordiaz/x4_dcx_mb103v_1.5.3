/*********************************************************************
*                SEGGER MICROCONTROLLER GmbH & Co. KG                *
*        Solutions for real time microcontroller applications        *
**********************************************************************
*                                                                    *
*        (c) 2003-2012     SEGGER Microcontroller GmbH & Co KG       *
*                                                                    *
*        Internet: www.segger.com    Support:  support@segger.com    *
*                                                                    *
**********************************************************************

**** emFile file system for embedded applications ****
emFile is protected by international copyright laws. Knowledge of the
source code may not be used to write a similar product. This file may
only be used in accordance with a license and should not be re-
distributed in any way. We appreciate your understanding and fairness.
----------------------------------------------------------------------
----------------------------------------------------------------------
File        : MMC_X_SPI.c
Purpose     : Sample MMC hardware layer for ST STR912 using the SPI 
              controller.
---------------------------END-OF-HEADER------------------------------
*/

/*********************************************************************
*
*             #include Section
*
**********************************************************************
*/

#include "FS.h"
#include "MMC_X_HW.h"

/*********************************************************************
*
*             #define Macros
*
**********************************************************************
*/

#define _APB0_BASE    0x58000000

#define _GPIO_BASE     (_APB0_BASE + 0x6000)
#define _GPIO_DIR_OFFS 0x400
#define _GPIO_SEL_OFFS 0x41C

#define _GPIO0_BASE   (_APB0_BASE + 0x6000)
#define _GPIO1_BASE   (_APB0_BASE + 0x7000)
#define _GPIO2_BASE   (_APB0_BASE + 0x8000)
#define _GPIO3_BASE   (_APB0_BASE + 0x9000)
#define _GPIO4_BASE   (_APB0_BASE + 0xA000)
#define _GPIO5_BASE   (_APB0_BASE + 0xB000)
#define _GPIO6_BASE   (_APB0_BASE + 0xC000)
#define _GPIO7_BASE   (_APB0_BASE + 0xD000)
#define _GPIO8_BASE   (_APB0_BASE + 0xE000)
#define _GPIO9_BASE   (_APB0_BASE + 0xF000)

#define _APB1_BASE     0x5C000000
#define _SCU_BASE      (_APB1_BASE + 0x2000)
#define _SCU_PCGR1    *(volatile unsigned int*)(_SCU_BASE + 0x18)
#define _SCU_PRR1     *(volatile unsigned int*)(_SCU_BASE + 0x20)

#define _SSP0_BASE    (_APB1_BASE + 0x7000)
#define _SSP0_CR0     *(volatile unsigned int*)(_SSP0_BASE + 0x00)
#define _SSP0_CR1     *(volatile unsigned int*)(_SSP0_BASE + 0x04)
#define _SSP0_DR      *(volatile unsigned int*)(_SSP0_BASE + 0x08)
#define _SSP0_SR      *(volatile unsigned int*)(_SSP0_BASE + 0x0C)
#define _SSP0_PR      *(volatile unsigned int*)(_SSP0_BASE + 0x10)
#define _SSP0_IMSCR   *(volatile unsigned int*)(_SSP0_BASE + 0x14)
#define _SSP0_RISR    *(volatile unsigned int*)(_SSP0_BASE + 0x18)
#define _SSP0_MISR    *(volatile unsigned int*)(_SSP0_BASE + 0x1C)
#define _SSP0_ICR     *(volatile unsigned int*)(_SSP0_BASE + 0x20)
#define _SSP0_DMACR   *(volatile unsigned int*)(_SSP0_BASE + 0x24)

#define _SSP1_BASE    (_APB1_BASE + 0x8000)
#define _SSP1_CR0     *(volatile unsigned int*)(_SSP0_BASE + 0x00)
#define _SSP1_CR1     *(volatile unsigned int*)(_SSP0_BASE + 0x04)
#define _SSP1_DR      *(volatile unsigned int*)(_SSP0_BASE + 0x08)
#define _SSP1_SR      *(volatile unsigned int*)(_SSP0_BASE + 0x0C)
#define _SSP1_PR      *(volatile unsigned int*)(_SSP0_BASE + 0x10)
#define _SSP1_IMSCR   *(volatile unsigned int*)(_SSP0_BASE + 0x14)
#define _SSP1_RISR    *(volatile unsigned int*)(_SSP0_BASE + 0x18)
#define _SSP1_MISR    *(volatile unsigned int*)(_SSP0_BASE + 0x1C)
#define _SSP1_ICR     *(volatile unsigned int*)(_SSP0_BASE + 0x20)
#define _SSP1_DMACR   *(volatile unsigned int*)(_SSP0_BASE + 0x24)


#define _SCU_GPIOOUT0      *(volatile unsigned int*)(_SCU_BASE + 0x44)
#define _SCU_GPIOOUT1      *(volatile unsigned int*)(_SCU_BASE + 0x48)
#define _SCU_GPIOOUT2      *(volatile unsigned int*)(_SCU_BASE + 0x4C)
#define _SCU_GPIOOUT3      *(volatile unsigned int*)(_SCU_BASE + 0x50)
#define _SCU_GPIOOUT4      *(volatile unsigned int*)(_SCU_BASE + 0x54)
#define _SCU_GPIOOUT5      *(volatile unsigned int*)(_SCU_BASE + 0x58)
#define _SCU_GPIOOUT6      *(volatile unsigned int*)(_SCU_BASE + 0x5C)
#define _SCU_GPIOOUT7      *(volatile unsigned int*)(_SCU_BASE + 0x60)

#define _SCU_GPIOIN0      *(volatile unsigned int*)(_SCU_BASE + 0x64)
#define _SCU_GPIOIN1      *(volatile unsigned int*)(_SCU_BASE + 0x68)
#define _SCU_GPIOIN2      *(volatile unsigned int*)(_SCU_BASE + 0x6C)
#define _SCU_GPIOIN3      *(volatile unsigned int*)(_SCU_BASE + 0x70)
#define _SCU_GPIOIN4      *(volatile unsigned int*)(_SCU_BASE + 0x74)
#define _SCU_GPIOIN5      *(volatile unsigned int*)(_SCU_BASE + 0x78)
#define _SCU_GPIOIN6      *(volatile unsigned int*)(_SCU_BASE + 0x7C)
#define _SCU_GPIOIN7      *(volatile unsigned int*)(_SCU_BASE + 0x80)

#define _SCU_GPIOTYPE0      *(volatile unsigned int*)(_SCU_BASE + 0x84)
#define _SCU_GPIOTYPE1      *(volatile unsigned int*)(_SCU_BASE + 0x88)
#define _SCU_GPIOTYPE2      *(volatile unsigned int*)(_SCU_BASE + 0x8C)
#define _SCU_GPIOTYPE3      *(volatile unsigned int*)(_SCU_BASE + 0x90)
#define _SCU_GPIOTYPE4      *(volatile unsigned int*)(_SCU_BASE + 0x94)
#define _SCU_GPIOTYPE5      *(volatile unsigned int*)(_SCU_BASE + 0x98)
#define _SCU_GPIOTYPE6      *(volatile unsigned int*)(_SCU_BASE + 0x9C)
#define _SCU_GPIOTYPE7      *(volatile unsigned int*)(_SCU_BASE + 0xA0)
#define _SCU_GPIOTYPE8      *(volatile unsigned int*)(_SCU_BASE + 0xA4)
#define _SCU_GPIOTYPE9      *(volatile unsigned int*)(_SCU_BASE + 0xA8)

#define _DIR_OFFS     0x400
#define _SEL_OFFS     0x420

#define _GPIO0_CLK_BIT  14
#define _GPIO1_CLK_BIT  15
#define _GPIO2_CLK_BIT  16
#define _GPIO3_CLK_BIT  17
#define _GPIO4_CLK_BIT  18
#define _GPIO5_CLK_BIT  19
#define _GPIO7_CLK_BIT  20
#define _GPIO8_CLK_BIT  21
#define _GPIO9_CLK_BIT  22


#define _MMC_CS_PORT_BASE     _GPIO2_BASE
#define _MMC_CS_PORT_CLK_BIT  _GPIO2_CLK_BIT
#define _MMC_CS_BIT             7
#define _MMC_CS_SCU_GPIOOUT  _SCU_GPIOOUT2
#define _MMC_CS_PORT_DIR     *(volatile unsigned char*)(_MMC_CS_PORT_BASE + _DIR_OFFS)
#define _MMC_CS_PORT_SEL     *(volatile unsigned char*)(_MMC_CS_PORT_BASE + _SEL_OFFS)
#define _MMC_CS_PORT_DATA    *(volatile unsigned char*)(_MMC_CS_PORT_BASE + ((1 << _MMC_CS_BIT) << 2))

#define _MMC_CLK_PORT_BASE     _GPIO2_BASE
#define _MMC_CLK_PORT_CLK_BIT  _GPIO2_CLK_BIT
#define _MMC_CLK_BIT            4
#define _MMC_CLK_SCU_GPIOOUT  _SCU_GPIOOUT2
#define _MMC_CLK_PORT_DIR     *(volatile unsigned char*)(_MMC_CLK_PORT_BASE + _DIR_OFFS)
#define _MMC_CLK_PORT_SEL     *(volatile unsigned char*)(_MMC_CLK_PORT_BASE + _SEL_OFFS)

#define _MMC_SDO_PORT_BASE     _GPIO2_BASE
#define _MMC_SDO_PORT_CLK_BIT  _GPIO2_CLK_BIT
#define _MMC_SDO_BIT           5
#define _MMC_SD0_SCU_GPIOOUT  _SCU_GPIOOUT2
#define _MMC_SDO_PORT_DIR     *(volatile unsigned char*)(_MMC_SDO_PORT_BASE + _DIR_OFFS)
#define _MMC_SDO_PORT_SEL     *(volatile unsigned char*)(_MMC_SDO_PORT_BASE + _SEL_OFFS)

#define _MMC_SDI_PORT_BASE     _GPIO2_BASE
#define _MMC_SDI_PORT_CLK_BIT  _GPIO2_CLK_BIT
#define _MMC_SDI_BIT           6
#define _MMC_SDI_SCU_GPIOOUT   _SCU_GPIOOUT2
#define _MMC_SDI_SCU_GPIOIN    _SCU_GPIOIN2
#define _MMC_SDI_PORT_DIR      *(volatile unsigned char*)(_MMC_SDI_PORT_BASE + _DIR_OFFS)
#define _MMC_SDI_PORT_SEL      *(volatile unsigned char*)(_MMC_SDI_PORT_BASE + _SEL_OFFS)

#define SPI_CLR_CS()          _MMC_CS_PORT_DATA = 0x00
#define SPI_SET_CS()          _MMC_CS_PORT_DATA = 0xFF


#define  _SSP_CR0            _SSP0_CR0
#define  _SSP_CR1            _SSP0_CR1
#define  _SSP_PR             _SSP0_PR
#define  _SSP_DR             _SSP0_DR
#define  _SSP_SR             _SSP0_SR




#define MMC_DEFAULTSUPPLYVOLTAGE  3300 /* in mV, example means 3.3V */

/*********************************************************************
*
*       Static data
*
**********************************************************************
*/
static volatile U8  _Dummy;

/*********************************************************************
*
*             Local functions
*
**********************************************************************
*/
/*********************************************************************
*
*       _Init
*/
static void _Init(void) {
  //
  // Confiure PINS
  //
  //
  // SD/MMC CS
  //
  _SCU_PCGR1           |= (1 << _MMC_CS_PORT_CLK_BIT);
  _SCU_PRR1            |= (1 << _MMC_CS_PORT_CLK_BIT);
  _MMC_CS_SCU_GPIOOUT  |= (1 << (_MMC_CS_BIT << 1));
  _MMC_CS_PORT_DIR     |=  (1 << _MMC_CS_BIT);
  _MMC_CS_PORT_SEL     &=  ~(1 << _MMC_CS_BIT);
  //
  // SD/MMC  CLK
  //
  _SCU_PCGR1            |= (1 << _MMC_CLK_PORT_CLK_BIT);
  _SCU_PRR1             |= (1 << _MMC_CLK_PORT_CLK_BIT);
  _MMC_CLK_SCU_GPIOOUT  |= (2 << (_MMC_CLK_BIT << 1));
  _MMC_CLK_PORT_DIR     |= (1 << _MMC_CLK_BIT);
  _MMC_CLK_PORT_SEL     &= ~(1 << _MMC_CLK_BIT);
  //
  // SD/MMC  MOSI
  //
  _SCU_PCGR1           |= (1 << _MMC_SDO_PORT_CLK_BIT);
  _SCU_PRR1            |= (1 << _MMC_SDO_PORT_CLK_BIT);
  _MMC_SD0_SCU_GPIOOUT |= (2 << (_MMC_SDO_BIT << 1));
  _MMC_SDO_PORT_DIR    |= (1 << _MMC_SDO_BIT);
  _MMC_SDO_PORT_SEL    &= ~(1 << _MMC_SDO_BIT);
  //
  // SD/MMC  MISO
  //
  _SCU_PCGR1           |= (1 << _MMC_SDI_PORT_CLK_BIT);
  _SCU_PRR1            |= (1 << _MMC_SDI_PORT_CLK_BIT);
  _MMC_SDI_SCU_GPIOOUT &= ~(3 << (_MMC_SDI_BIT << 1));
  _MMC_SDI_SCU_GPIOIN  |=   (1 << _MMC_SDI_BIT);
  _MMC_SDI_PORT_DIR    &=  ~(1 << _MMC_SDI_BIT);
  _MMC_SDI_PORT_SEL    &=  ~(1 << _MMC_SDI_BIT);
  //
  // Enable and clear reset both SSPs
  //
  _SCU_PCGR1  |= (1 << 8); // Enable clock for SSP0
  _SCU_PRR1   |= (1 << 8); // Clear reset for SSP0
  //
  // Configure SSP0
  //
  _SSP_CR1 = 0x0000;   // Disable SSP0
  _SSP_CR0 = 0x0AC7;
  _SSP_PR  = 0x000C;
  _SSP_CR1 = 0x0002;  // Enable SSP0
}
/*********************************************************************
*
*       FS_MMC_HW_X_EnableCS
*
*  Description:
*    FS low level function. Sets the card slot active using the
*    chip select (CS) line.
*
*  Parameters:
*    Unit      - Device Index
*
*  Return value:
*    void
*/
void FS_MMC_HW_X_EnableCS(U8 Unit) {
  SPI_CLR_CS();
}

/*********************************************************************
*
*       FS_MMC_HW_X_EnableCS
*
*  Description:
*    FS low level function. Sets the card slot inactive using the
*    chip select (CS) line.
*
*  Parameters:
*    Unit      - Device Index
*
*  Return value:
*    void
*/
void FS_MMC_HW_X_DisableCS(U8 Unit) {
  SPI_SET_CS();
}

/*********************************************************************
*
*       FS_MMC_HW_X_IsWriteProtected
*
*  Description:
*    FS low level function. Returns the state of the physical write
*    protection of the SD cards.
*
*  Parameters:
*    Unit      - Device Index
*
*  Return value:
*    1                - the card is write protected
*    ==0              - the card is not write protected
*/
int FS_MMC_HW_X_IsWriteProtected(U8 Unit) {
  /* If the card slot has no write switch detector, return 0 */
  return 0;
}

/*********************************************************************
*
*       FS_MMC_HW_X_SetMaxSpeed
*
*  Description:
*    FS low level function. Sets the SPI interface to a maximum frequency.
*    Make sure that you set the frequency lower or equal but never higher
*    than the given value. Recommended startup frequency is 100kHz - 400kHz.
*
*  Parameters:
*    Unit       - Device Index
*    MaxFreq           - SPI clock frequency in kHz
*
*  Return value:
*    max. frequency    - the maximum frequency set in kHz
*    ==0               - the frequency could not be set
*/

U16 FS_MMC_HW_X_SetMaxSpeed(U8 Unit, U16 MaxFreq) {
  _Init();
  if (MaxFreq > 400) {
    _SSP_CR1 = 0x0000;
    _SSP_CR0 = 0x00C7;
    _SSP_PR  = 0x0004;
    _SSP_CR1 = 0x0002;
  }
  return 400;    /* We are not faster than this */
}

/*********************************************************************
*
*       FS_MMC_HW_X_SetVoltage
*
*  Description:
*    FS low level function. Be sure that your card slot is within the given
*    voltage range. Return 1 if your slot can support the required voltage,
*    and if not, return 0;
*
*  Parameters:
*    Unit      - Device Index
*    MaxFreq          - SPI clock frequency in kHz
*
*  Return value:
*    1                - the card slot supports the voltage range
*    ==0              - the card slot does not support the voltage range
*/
int FS_MMC_HW_X_SetVoltage(U8 Unit, U16 Vmin, U16 Vmax) {
  /* voltage range check */
  char r;
  if((Vmin <= MMC_DEFAULTSUPPLYVOLTAGE) && (Vmax >= MMC_DEFAULTSUPPLYVOLTAGE)) {
    r = 1;
  } else {
    r = 0;
  }
  return r;
}

/*********************************************************************
*
*       FS_MMC_HW_X_IsPresent
*
*  Description:
*    Returns the state of the media. If you do not know the state, return
*    FS_MEDIA_STATEUNKNOWN and the higher layer will try to figure out if
*    a media is present.
*
*  Parameters:
*    Unit                 - Device Index
*
*  Return value:
*    FS_MEDIA_STATE_UNKNOWN    - Media state is unknown
*    FS_MEDIA_NOT_PRESENT      - Media is not present
*    FS_MEDIA_IS_PRESENT       - Media is present
*/
int FS_MMC_HW_X_IsPresent(U8 Unit) {
  return FS_MEDIA_STATE_UNKNOWN;
}

/*********************************************************************
*
*       FS_MMC_HW_X_Read
*
*  Description:
*    FS low level function. Reads a specified number of bytes from MMC
*    card to buffer.
*
*  Parameters:
*    Unit      - Device Index
*    pData            - Pointer to a data buffer
*    NumBytes         - Number of bytes
*
*  Return value:
*    void
*/
void FS_MMC_HW_X_Read (U8 Unit, U8 * pData, int NumBytes) {
  _SSP_DR = 0xff;
  if (--NumBytes) {
    do {
      _SSP0_DR = 0xff;
      while ((_SSP_SR & (1 << 2)) == 0);  /* Wait until we received a byte */
      *pData++ = _SSP_DR;
    } while (--NumBytes);
  }
  while ((_SSP_SR & (1 << 2)) == 0);  /* Wait until we received a byte */
  *pData = _SSP_DR;
}

/*********************************************************************
*
*             FS_MMC_HW_X_Write
*
*  Description:
*    FS low level function. Writes a specified number of bytes from
*    data buffer to the MMC/SD card.
*
*  Parameters:
*    Unit      - Device Index
*    pData            - Pointer to a data buffer
*    NumBytes         - Number of bytes
*
*  Return value:
*    void
*/
void FS_MMC_HW_X_Write(U8 Unit, const U8 * pData, int NumBytes) {
  while ((_SSP_SR & (1 << 4)) != 0);  /* Busy ? */
  do {
    _SSP_DR = *pData++;
    while ((_SSP0_SR & (1 << 2)) == 0);  /* Wait until we received a byte */
    _Dummy = _SSP0_DR;
  } while (--NumBytes);
}
/*************************** End of file ****************************/
