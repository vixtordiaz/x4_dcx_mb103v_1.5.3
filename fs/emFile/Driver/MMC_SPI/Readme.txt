Readme.txt for SD card driver

The following hardware samples for SD cards are available:

MMC_HW_Template.c              Generic file containing only blank functions.
                               Typically used as starting point when writing new hardware routines.
MMC_HW_TemplatePort.c          Generic file containing functions to access cards connected via port pins.
                               Typically used as starting point when writing new hardware routines for
                               cards connected via port pins.


