/*********************************************************************
*                SEGGER MICROCONTROLLER GmbH & Co. KG                *
*        Solutions for real time microcontroller applications        *
**********************************************************************
*                                                                    *
*        (c) 2003-2012     SEGGER Microcontroller GmbH & Co KG       *
*                                                                    *
*        Internet: www.segger.com    Support:  support@segger.com    *
*                                                                    *
**********************************************************************

**** emFile file system for embedded applications ****
emFile is protected by international copyright laws. Knowledge of the
source code may not be used to write a similar product. This file may
only be used in accordance with a license and should not be re-
distributed in any way. We appreciate your understanding and fairness.
----------------------------------------------------------------------
----------------------------------------------------------------------
File        : MMC_X_HW.c
Purpose     : MMC hardware layer for accessing MMC/SD via SPI for M16C
---------------------------END-OF-HEADER------------------------------
*/

/*********************************************************************
*
*             #include Section
*
**********************************************************************
*/

#include "FS.h"
#include "FS_Conf.h"
#include "MMC_X_HW.h"

/*********************************************************************
*
*             #define Macros
*
**********************************************************************
*/
/* Port P3 register */
#define P3    (*(volatile U8 *)(0x03e5))
/* Port P3 direction register */
#define PD3   (*(volatile U8 *)(0x03e7))
/* Port P9 register */
#define P9    (*(volatile U8 *)(0x03f1))
/* Port P9 direction register */
#define PD9   (*(volatile U8 *)(0x03f3))
/* Protect register */
#define PRCR  (*(volatile U8 *)(0x000A))

/* SI/O3 interrupt control register */
#define S3IC  (*(volatile U8 *)(0x0049))
/* SI/O3i transmit/receive register (i=3,4)*/
#define S3TRR (*(volatile U8 *)(0x0360))
/* SI/O3 control register */
#define S3C   (*(volatile U8 *)(0x0362))
/* SI/O3 bit rate generator (Use "MOV" instruction when writing to these registers)*/
#define S3BRG (*(volatile U8 *)(0x0363))


/*********************************************************************
*
*       Configurable macros
*
*   Please setup these macros according your hardware
*
*/
#define SPI_CS_PORT            P3
#define SPI_CLK_PORT           P9
#define SPI_DATAOUT_PORT       P9
#define SPI_DATAIN_PORT        P9

#define SPI_CS_PIN            5
#define SPI_CLK_PIN           0
#define SPI_DATAOUT_PIN       2
#define SPI_DATAIN_PIN        1
#define SPI_DELAY()          //{ int i=10L; while(i-- != 0); }
#define SPI_SETUP_PINS()     { PD3   = (1 << SPI_CS_PIN); \
                               PRCR  = (1 << 2); \
                               PD9  |= (1 << SPI_CLK_PIN) | (1 << SPI_DATAOUT_PIN); }

#define SPI_TX_REGISTER      S3TRR
#define SPI_RX_REGISTER      S3TRR
#define SPI_RX_INT_REGISTER  S3IC
#define SPI_TX_INT_REGISTER  S3IC

#ifndef PCLK_SPI
  #define PCLK_SPI             14745600L    /* peripheral clock for SPI unit */
#endif

/*********************************************************************
*
*       #define Macros
*
*/
#define SPI_CLR_CS()          SPI_CS_PORT      &= ~(1 << SPI_CS_PIN)
#define SPI_SET_CS()          SPI_CS_PORT      |=  (1 << SPI_CS_PIN)

#define SPI_CLR_CLK()         SPI_CLK_PORT     &= ~(1 << SPI_CLK_PIN)
#define SPI_SET_CLK()         SPI_CLK_PORT     |=  (1 << SPI_CLK_PIN)
#define SPI_CLR_DATAOUT()     SPI_DATAOUT_PORT &= ~(1 << SPI_DATAOUT_PIN)
#define SPI_SET_DATAOUT()     SPI_DATAOUT_PORT |=  (1 << SPI_DATAOUT_PIN)

#define SPI_DATAIN()          ( S3TRR )

#define SPI_CLR_RX_INT()     ( SPI_RX_INT_REGISTER &= ~(1 << 3) )
#define SPI_CLR_TX_INT()     ( SPI_RX_INT_REGISTER &= ~(1 << 3) )

#define SPI_DATAOUT(data)    ( S3TRR = data )

#define SPI_RX_BUSY()        ( (SPI_RX_INT_REGISTER & (1 << 3)) == 0 )
#define SPI_TX_BUSY()        ( (SPI_TX_INT_REGISTER & (1 << 3)) == 0 )

#define MMC_DEFAULTSUPPLYVOLTAGE  3300 /* in mV, example means 3.3V */

/*********************************************************************
*
*       Static data
*
**********************************************************************
*/

/*********************************************************************
*
*             Local functions
*
**********************************************************************
*/

static void _Init(U16 Frequency) {
  #define SPI_FREQUENCY (PCLK_SPI/1000)
  int BrgValue;
  SPI_SETUP_PINS();
  SPI_SET_CS();
  SPI_SET_CLK();

  /* Initialize SIO3 in SPI mode  */
  PRCR |= (1 << 2);
  S3C =  0x00             /* Use f1 as clock source */
       | (0 << 2)         /* SOUT enabled           */
       | (1 << 3)         /* CLK output enabled     */
       | (0 << 4)         /* always 0               */
       | (1 << 5)         /* MSB first              */
       | (1 << 6)         /* internal clock         */
       | (1 << 7);        /* CLK initially high     */
  /* Set baud rate */
  BrgValue = (SPI_FREQUENCY/Frequency/2 - 1);
  BrgValue++;
  S3BRG = BrgValue;
}

/*********************************************************************
*
*       FS_MMC_HW_X_EnableCS
*
*  Description:
*    Sets the card slot active using the
*    chip select (CS) line.
*
*  Parameters:
*    Unit      - Device Index
*
*  Return value:
*    void
*/
void FS_MMC_HW_X_EnableCS(U8 Unit) {
  SPI_CLR_CS();
}

/*********************************************************************
*
*       FS_MMC_HW_X_DisableCS
*
*  Description:
*    Clears the card slot inactive using the
*    chip select (CS) line.
*
*  Parameters:
*    Unit      - Device Index
*
*  Return value:
*    void
*/
void FS_MMC_HW_X_DisableCS(U8 Unit) {
  SPI_SET_CS();
}

/*********************************************************************
*
*       FS_MMC_HW_X_IsWriteProtected
*
*  Description:
*    Returns the state of the physical write
*    protection of the SD cards.
*
*  Parameters:
*    Unit      - Device Index
*
*  Return value:
*    1                - the card is write protected
*    ==0              - the card is not write protected
*/

int FS_MMC_HW_X_IsWriteProtected(U8 Unit) {
  return 0;
}

/*********************************************************************
*
*       FS_MMC_HW_X_SetMaxSpeed
*
*  Description:
*    Sets the SPI interface to a maximum frequency.
*    Make sure that you set the frequency lower or equal but never higher
*    than the given value. Recommended startup frequency is 100kHz - 400kHz.
*
*  Parameters:
*    Unit       - Device Index
*    MaxFreq           - SPI clock frequency in kHz
*
*  Return value:
*    max. frequency    - the maximum frequency set in kHz
*    ==0               - the frequency could not be set
*/

U16 FS_MMC_HW_X_SetMaxSpeed(U8 Unit, U16 MaxFreq) {
  _Init(MaxFreq);
  return MaxFreq;         /* We are not faster than this */
}

/*********************************************************************
*
*       FS_MMC_HW_X_SetVoltage
*
*  Description:
*    Be sure that your card slot si within the given
*    voltage range. Return 1 if your slot can support the required voltage,
*    and if not, return 0;
*
*  Parameters:
*    Unit      - Device Index
*    MaxFreq          - SPI clock frequency in kHz
*
*  Return value:
*    1                - the card slot supports the voltage range
*    ==0              - the card slot does not support the voltage range
*/

int  FS_MMC_HW_X_SetVoltage(U8 Unit, U16 Vmin, U16 Vmax) {
  /* voltage range check */
  char r;
  if((Vmin <= MMC_DEFAULTSUPPLYVOLTAGE) && (Vmax >= MMC_DEFAULTSUPPLYVOLTAGE)) {
    r = 1;
  } else {
    r = 0;
  }
  return r;
}

/*********************************************************************
*
*       FS_MMC_HW_X_IsPresent
*
*  Description:
*    Returns the state of the media. If you do not know the state, return
*    FS_MEDIA_STATEUNKNOWN and the higher layer will try to figure out if
*    a media is present.
*
*  Parameters:
*    Unit                 - Device Index
*
*  Return value:
*    FS_MEDIA_STATE_UNKNOWN    - Media state is unknown
*    FS_MEDIA_NOT_PRESENT      - Media is not present
*    FS_MEDIA_IS_PRESENT       - Media is present
*/

int FS_MMC_HW_X_IsPresent(U8 Unit) {
  return FS_MEDIA_STATE_UNKNOWN;
}

/*********************************************************************
*
*       FS_MMC_HW_X_Read
*
*  Description:
*    Reads a specified number of bytes from MMC
*    card to buffer.
*
*  Parameters:
*    Unit      - Device Index
*    pData     - Pointer to a data buffer
*    NumBytes  - Number of bytes
*
*  Return value:
*    void
*/

void FS_MMC_HW_X_Read (U8 Unit, U8 * pData, int NumBytes) {
  U8 c;
  do {
    SPI_CLR_RX_INT();
    c = 0;
    SPI_DATAOUT(0xFF);         /* start reception by sending 0xFF */
    do {
    } while (SPI_RX_BUSY());   /* Wait until data is available    */
    c = SPI_DATAIN();
    *pData++ = c;
  } while (--NumBytes);
}

/*********************************************************************
*
*       FS_MMC_HW_X_Write
*
*  Description:
*    Writes a specified number of bytes from
*    data buffer to the MMC/SD card.
*
*  Parameters:
*    Unit      - Device Index
*    pData            - Pointer to a data buffer
*    NumBytes         - Number of bytes
*
*  Return value:
*    void
*/

void FS_MMC_HW_X_Write(U8 Unit, const U8 * pData, int NumBytes) {
  int i;
  U8 data;
  for (i = 0; i < NumBytes; i++) {
    SPI_CLR_TX_INT();
    data = pData[i];
    SPI_DATAOUT(data);
    do {
    } while (SPI_TX_BUSY());
  }
}

/*************************** End of file ****************************/
