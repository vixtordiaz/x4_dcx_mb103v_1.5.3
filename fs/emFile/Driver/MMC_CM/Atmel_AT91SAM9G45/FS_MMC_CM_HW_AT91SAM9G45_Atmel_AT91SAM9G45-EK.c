/*********************************************************************
*                SEGGER MICROCONTROLLER GmbH & Co. KG                *
*        Solutions for real time microcontroller applications        *
**********************************************************************
*                                                                    *
*        (c) 2003-2012     SEGGER Microcontroller GmbH & Co KG       *
*                                                                    *
*        Internet: www.segger.com    Support:  support@segger.com    *
*                                                                    *
**********************************************************************

**** emFile file system for embedded applications ****
emFile is protected by international copyright laws. Knowledge of the
source code may not be used to write a similar product. This file may
only be used in accordance with a license and should not be re-
distributed in any way. We appreciate your understanding and fairness.
----------------------------------------------------------------------
----------------------------------------------------------------------
File        : FS_MMC_CM_HW_AT91SAM9G45_Atmel_AT91SAM9G45-EK.c
Purpose     : Hardware layer the Atmel AT91SAM9G45 (card mode)
Literature  : [1] \\fileserver\techinfo\Company\Atmel\MCU\ARM\AT91SAM9G45\AT91SAM9G45_RM_0910_6438.pdf
              [2] \\fileserver\techinfo\Company\Atmel\MCU\ARM\AT91SAM9G45\AT91SAM9G45-EKES_0906_doc6481.pdf
---------------------------END-OF-HEADER------------------------------
*/

/*********************************************************************
*
*       Includes
*
**********************************************************************
*/
#include "FS.h"
#include "FS_Int.h"
#include "MMC_SD_CardMode_X_HW.h"
#include "RTOS.h"
#include "BSP.h"

/*********************************************************************
*
*       Defines configurable
*
**********************************************************************
*/
#define USE_DMA                 0
#define MCK                     (384000000uL / 3) // Peripheral clock speed in Hz
#define DEFAULT_SPEED           400         // Default communication speed (kHz)
#define DEFAULT_TIMEOUT         0x7FuL      // Card cycles (see the description of Data Timeout Register)
#if USE_DMA
  #define HSMCI0_DMA_CHANNEL    0           // DMA channel index of the first HSMCI controller
  #define HSMCI1_DMA_CHANNEL    1           // DMA channel index of the second HSMCI controller
#else
  #define NUM_BLOCKS_AT_ONCE    65535       // Maximum number of blocks in a data transfer
#endif // USE_DMA

/*********************************************************************
*
*       Defines non configurable
*
**********************************************************************
*/

/*********************************************************************
*
*       High Speed MultiMedia Card Interface
*/
#define HSMCI_BASE_ADDR     0xFFF80000
#define HSMCI_SIZEOF_FIFO   (0x4000 - 0x200)      // Size of the FIFO in bytes

/*********************************************************************
*
*       DMA Controller
*/
#define DMAC_BASE_ADDR      0xFFFFEC00
#define DMAC_EN             (*(volatile U32 *)(DMAC_BASE_ADDR + 0x04))          // DMAC Enable Register
#define DMAC_EBCISR         (*(volatile U32 *)(DMAC_BASE_ADDR + 0x24))          // DMAC Error, Chained Buffer transfer completed and Buffer transfer completed Status Register
#define DMAC_CHER           (*(volatile U32 *)(DMAC_BASE_ADDR + 0x28))          // Channel Handler Enable Register
#define DMAC_CHDR           (*(volatile U32 *)(DMAC_BASE_ADDR + 0x2C))          // Channel Handler Disable Register
#define DMAC_CHSR           (*(volatile U32 *)(DMAC_BASE_ADDR + 0x30))          // Channel Handler Status Register

/*********************************************************************
*
*       Parallel Input/Output (The output pins of HSMCI are mapped on Port A)
*/
#define PIOA_BASE_ADDR      0xFFFFF200
#define PIOA_PER            (*(volatile U32 *)(PIOA_BASE_ADDR + 0x00))    // Port A Enable Register
#define PIOA_PDR            (*(volatile U32 *)(PIOA_BASE_ADDR + 0x04))    // Port A Disable Register
#define PIOA_PDSR           (*(volatile U32 *)(PIOA_BASE_ADDR + 0x3C))    // Controller Pin Data Status Register
#define PIOA_PUDR           (*(volatile U32 *)(PIOA_BASE_ADDR + 0x60))    // Pull-up Disable Register
#define PIOA_ABSR           (*(volatile U32 *)(PIOA_BASE_ADDR + 0x70))    // Peripheral AB Select Register

/*********************************************************************
*
*       Parallel Input/Output (The output pins of HSMCI are mapped also on Port C)
*/
#define PIOD_BASE_ADDR      0xFFFFF800
#define PIOD_PER            (*(volatile U32 *)(PIOD_BASE_ADDR + 0x00))    // Port C Enable Register
#define PIOD_PDR            (*(volatile U32 *)(PIOD_BASE_ADDR + 0x04))    // Port C Disable Register
#define PIOD_PDSR           (*(volatile U32 *)(PIOD_BASE_ADDR + 0x3C))    // Controller Pin Data Status Register
#define PIOD_PUDR           (*(volatile U32 *)(PIOD_BASE_ADDR + 0x60))    // Pull-up Disable Register
#define PIOD_ABSR           (*(volatile U32 *)(PIOD_BASE_ADDR + 0x70))    // Peripheral AB Select Register

/*********************************************************************
*
*       Power Management Controller
*/
#define PMC_BASE_ADDR       0xFFFFFC00
#define PMC_PCER            (*(volatile U32 *)(PMC_BASE_ADDR + 0x10))     // Peripheral Clock Enable Register

/*********************************************************************
*
*       HSMCI Control Register
*/
#define CR_MCIEN            (1uL << 0)    // Multi-Media Interface Enable
#define CR_MCIDIS           (1uL << 1)    // Multi-Media Interface Disable
#define CR_PWSDIS           (1uL << 3)    // Disable power down mode
#define CR_SWRST            (1uL << 7)    // Software Reset

/*********************************************************************
*
*       HSMCI Mode Register
*/
#define MR_RDPROOF          (1uL << 11)   // Read Proof Enable
#define MR_WRPROOF          (1uL << 12)   // Write Proof Enable
#define MR_FBYTE            (1uL << 13)   // Force Byte Transfer

/*********************************************************************
*
*       HSMCI Command Register
*/
#define CMDR_CMDNB_MASK     0x3F          // Command number
#define CMDR_RSPTYP_NONE    (0uL << 6)    // No response type
#define CMDR_RSPTYP_48BIT   (1uL << 6)    // 48-bit response type
#define CMDR_RSPTYP_136BIT  (2uL << 6)    // 136-bit response type
#define CMDR_SPCMD_INIT     (1uL << 8)    // Initialisation command
#define CMDR_SPCMD_SYNC     (2uL << 8)    // Wait for the end of last command
#define CMDR_OPCMD          (1uL << 11)   // Open drain/push pull
#define CMDR_MAXLAT         (1uL << 12)   // Max Latency for Command to Response
#define CMDR_TRCMD_MASK     (3uL << 16)   // Type of transfer command
#define CMDR_TRCMD_NONE     (0uL << 16)   // No transfer command
#define CMDR_TRCMD_START    (1uL << 16)   // Start transfer
#define CMDR_TRCMD_STOP     (2uL << 16)   // Stop transfer
#define CMDR_TRDIR          (1uL << 18)   // Transfer direction
#define CMDR_TRTYP_MULTI    (1uL << 19)   // SD Multiple block transfer

/*********************************************************************
*
*       HSMCI Status Register
*/
#define SR_CMDRDY           (1uL << 0)    // Command Ready
#define SR_RXRDY            (1uL << 1)    // Receiver Ready
#define SR_TXRDY            (1uL << 2)    // Transmitter Ready
#define SR_DTIP             (1uL << 4)    // Data transfer in progress
#define SR_NOTBUSY          (1uL << 5)    // Not busy
#define SR_RINDE            (1uL << 16)   // Response Index Error
#define SR_RDIRE            (1uL << 17)   // Response Direction Error
#define SR_RCRCE            (1uL << 18)   // Response CRC Error
#define SR_RENDE            (1uL << 19)   // Response End Bit Error
#define SR_RTOE             (1uL << 20)   // Response Timeout Error
#define SR_DCRCE            (1uL << 21)   // Data CRC Error
#define SR_DTOE             (1uL << 22)   // Data Time-out Error
#define SR_CSTOE            (1uL << 23)   // Completion Signal Time-out Error
#define SR_BLKOVRE          (1uL << 24)   // DMA Block Overrun Error
#define SR_DMADONE          (1uL << 25)   // DMA Transfer Done
#define SR_FIFOEMPTY        (1uL << 26)   // Fifo empty flag
#define SR_XFRDONE          (1uL << 27)   // Transfer Done Flag
#define SR_OVRE             (1uL << 30)   // Overrun
#define SR_UNRE             (1uL << 31)   // Underrun

/*********************************************************************
*
*       HSMCI SDCard/SDIO Register
*/
#define SDCR_SDCBUS_MASK    (3uL << 6)
#define SDCR_SDCBUS_1BIT    (0uL << 6)
#define SDCR_SDCBUS_4BIT    (2uL << 6)
#define SDCR_SDCBUS_8BIT    (3uL << 6)

/*********************************************************************
*
*       HSMCI DMA Configuration Register
*/
#define DMA_CHKSIZE         (1uL << 4)    // Channel Read and Write Chunk Size
#define DMA_DMAEN           (1uL << 8)    // DMA Hardware Hanshaking Enable
#define DMA_ROPT            (1uL << 12)   // Read Optimisation with Padding

/*********************************************************************
*
*       HSMCI Configuration Register
*/
#define CFG_FIFOMODE        (1uL << 0)    // Internal FIFO Control Mode
#define CFG_FERRCTRL        (1uL << 4)    // Flow Error Flag Reset Control Mode
#define CFG_HSMODE          (1uL << 8)    // High Speed Mode

/*********************************************************************
*
*       HSMCI0 port pins
*/
#define PIOA_MCCK0          (1uL << 0)    // Clock
#define PIOA_MCCD0          (1uL << 1)    // Command
#define PIOA_MCD00          (1uL << 2)    // D0
#define PIOA_MCD01          (1uL << 3)    // D1
#define PIOA_MCD02          (1uL << 4)    // D2
#define PIOA_MCD03          (1uL << 5)    // D3
#define PIOA_MCD04          (1uL << 6)    // D4
#define PIOA_MCD05          (1uL << 7)    // D5
#define PIOA_MCD06          (1uL << 8)    // D6
#define PIOA_MCD07          (1uL << 9)    // D7
#define PIOD_MCI0_CD        (1uL << 10)   // Card detect

/*********************************************************************
*
*       HSMCI1 port pins
*/
#define PIOA_MCCK1          (1uL << 31)   // Clock
#define PIOA_MCCD1          (1uL << 22)   // Command
#define PIOA_MCD10          (1uL << 23)   // D0
#define PIOA_MCD11          (1uL << 24)   // D1
#define PIOA_MCD12          (1uL << 25)   // D2
#define PIOA_MCD13          (1uL << 26)   // D3
#define PIOA_MCD14          (1uL << 27)   // D4
#define PIOA_MCD15          (1uL << 28)   // D5
#define PIOA_MCD16          (1uL << 29)   // D6
#define PIOA_MCD17          (1uL << 30)   // D7
#define PIOD_MCI1_CD        (1uL << 11)   // Card detect
#define PIOD_MCI1_WP        (1uL << 29)   // Write protect

/*********************************************************************
*
*       DMAC Control A Register
*/
#define CTRLA_BCSIZE_MASK     0xFFFuL
#define CTRLA_SCSIZE          (1uL << 16)
#define CTRLA_DCSIZE          (1uL << 20)
#define CTRLA_DST_WITDH_WORD  (2uL << 28)
#define CTRLA_SRC_WITDH_WORD  (2uL << 24)

/*********************************************************************
*
*       DMAC Control B Register
*/
#define CTRLB_SRC_DSCR        (1uL << 16)
#define CTRLB_DST_DSCR        (1uL << 20)
#define CTRLB_FC_M2P          (5uL << 21)
#define CTRLB_FC_P2M          (4uL << 21)

/*********************************************************************
*
*       DMAC Configuration Register
*/
#define CFG_SRC_H2SEL         (1uL << 9)
#define CFG_DST_H2SEL         (1uL << 13)
#define CFG_SRC_PER_HSMCI0    0
#define CFG_DST_PER_HSMCI0    0
#define CFG_SRC_PER_HSMCI1    13uL
#define CFG_DST_PER_HSMCI1    (13uL << 4)

/*********************************************************************
*
*       Peripheral identifiers
*/
#define PIOA_ID               2
#define PIOD_ID               5
#define HSMCI0_ID             11
#define HSMCI1_ID             29
#define DMAC_ID               21

#define NUM_HSMCI_UNITS               2
#define GET_HSMCI_REGS(Unit)          ((HSMCI_REGS *)(HSMCI_BASE_ADDR + (0x50000 * Unit)))
#define GET_DMA_CHANNEL_INDEX(Unit)   (Unit ? HSMCI1_DMA_CHANNEL : HSMCI0_DMA_CHANNEL)
#define GET_DMA_CHANNEL_REGS(Unit)    ((DMAC_CHANNEL_REGS *)(DMAC_BASE_ADDR + 0x3C + (GET_DMA_CHANNEL_INDEX(Unit) * 0x28)))
#define GET_DMA_SRC_PER(Unit)         (Unit ? CFG_SRC_PER_HSMCI1 : CFG_SRC_PER_HSMCI0)
#define GET_DMA_DST_PER(Unit)         (Unit ? CFG_DST_PER_HSMCI1 : CFG_DST_PER_HSMCI0)

#define BYTES_PER_SECTOR      512

//
// We need this define to be compatible with the old SD card mode drivers
//
#ifndef   FS_MMC_CMD_FLAG_USE_MMC8MODE
  #define FS_MMC_CMD_FLAG_USE_MMC8MODE  0
#endif

/*********************************************************************
*
*       Type definitions
*
**********************************************************************
*/

//
// Describes the registers of a HSMCI peripheral
//
typedef struct HSMCI_REGS {
  volatile U32 HSMCI_CR;            // Control Register
  volatile U32 HSMCI_MR;            // Mode Register
  volatile U32 HSMCI_DTOR;          // Data Timeout Register
  volatile U32 HSMCI_SDCR;          // SD/SDIO Card Register
  volatile U32 HSMCI_ARGR;          // Argument Register
  volatile U32 HSMCI_CMDR;          // Command Register
  volatile U32 HSMCI_BLKR;          // Block Register
  volatile U32 HSMCI_CSTOR;         // Completion Signal Timeout Register
  volatile U32 HSMCI_RSPR[4];       // Response Register
  volatile U32 HSMCI_RDR;           // Receive Data Register
  volatile U32 HSMCI_TDR;           // Transmit Data Register
  volatile U32 Reserved1[(0x3C - 0x38) / 4 + 1];
  volatile U32 HSMCI_SR;            // Status Register
  volatile U32 HSMCI_IER;           // Interrupt Enable Register
  volatile U32 HSMCI_IDR;           // Interrupt Disable Register
  volatile U32 HSMCI_IMR;           // Interrupt Mask Register
  volatile U32 HSMCI_DMA;           // DMA Configuration Register
  volatile U32 HSMCI_CFG;           // Configuration Register
  volatile U32 Reserved2[(0xE0 - 0x58) / 4 + 1];
  volatile U32 HSMCI_WPMR;          // Write Protection Mode Register
  volatile U32 HSMCI_WPSR;          // Write Protection Status Register
  volatile U32 Reserved3[(0x1FC - 0xEC) / 4 + 1];
  volatile U32 HSMCI_FIFO[(0x3FFC - 0x200) / 4 + 1];   // FIFO Memory Aperture
} HSMCI_REGS;

#if USE_DMA
//
// Describes the registers of a DMA channel
//
typedef struct DMA_CHANNEL_REGS {
  volatile U32 DMAC_SADDR;          // Channel Source Address Register
  volatile U32 DMAC_DADDR;          // Channel Destination Address Register
  volatile U32 DMAC_DSCR;           // Channel Description Address Register
  volatile U32 DMAC_CTRLA;          // Channel Control A Register
  volatile U32 DMAC_CTRLB;          // Channel Control B Register
  volatile U32 DMAC_CFG;            // Channel Configuration Register
  volatile U32 DMAC_SPIP;           // Channel Source Picture in Picture Configuration Register
  volatile U32 DMAC_DPIP;           // Channel Destination Picture in Picture Configuration Register
  volatile U32 Reserved[2];         // Reserved
} DMAC_CHANNEL_REGS;
#endif // USE_DMA

//
// Describes the instance of a driver.
//
typedef struct DRIVER_INST {
  U8           IgnoreCRC;         // Remembers that we should ignore CRC errors in case of an R3 response
  U16          BlockSize;         // Size of the block to transfer as set by the upper layer
  U16          NumBlocks;         // Number of blocks to transfer as set by the upper layer
#if USE_DMA
  U32          BufferVAddr;       // Virtual address of the memory buffer used by DMA
  U32          BufferPAddr;       // Physical address of the memory buffer used by DMA
  U32          NumBytesInBuffer;  // DMA buffer size in bytes
#endif // USE_DMA
} DRIVER_INST;

/*********************************************************************
*
*       Static data
*
**********************************************************************
*/
static DRIVER_INST _aInst[NUM_HSMCI_UNITS];   // List of driver instances

/*********************************************************************
*
*       Static code
*
**********************************************************************
*/

/*********************************************************************
*
*       _ComputeClockDivisor
*
*   Function description
*     Computes the divisor value to get the given SD clock frequency
*     from the given peripheral clock.
*
*   Parameters:
*     PClk  The peripheral frequency in kHz
*     SDClk The SD card frequency in kHz
*
*   Return value
*     The computed divisor value
*/
static U32 _ComputeClockDivisor(U32 PClk, U32 SDClk) {
  U32 ClkDiv;

  if (PClk < SDClk) {
    return 0;
  } else {
    //
    // The peripheral clock is divided by 2 * (ClkDiv + 1)
    //
    // Example:
    //
    // Peripheral clock 96MHz
    // ClkDiv = 0   Speed = 96 / 2 * (0 + 1) = 48MHz
    // ClkDiv = 5   Speed = 96 / 2 * (5 + 1) = 9.6MHz
    //
    ClkDiv = (PClk + SDClk) / (SDClk * 2);
    if (ClkDiv > 0) {
      --ClkDiv;
    }
    return ClkDiv;
  }
}

/*********************************************************************
*
*       _WaitForResponse
*
*   Function description
*     Waits for a command response to arrive.
*
*   Return values
*     FS_MMC_CARD_NO_ERROR                Success
*     FS_MMC_CARD_RESPONSE_CRC_ERROR      CRC error in response
*     FS_MMC_CARD_RESPONSE_TIMEOUT        No response received
*     FS_MMC_CARD_RESPONSE_GENERIC_ERROR  Any other error
*/
static int _WaitForResponse(U8 Unit) {
  U32           Status;
  HSMCI_REGS  * pHSMCIRegs;

  //
  // Wait for command to finish
  //
  pHSMCIRegs = GET_HSMCI_REGS(Unit);
  do {
    Status = pHSMCIRegs->HSMCI_SR;
  } while (!(Status & SR_CMDRDY));
  if ((Status & SR_RCRCE) && !_aInst[Unit].IgnoreCRC) {   // CRC error in response
    return FS_MMC_CARD_RESPONSE_CRC_ERROR;
  } else {
    if (Status & SR_RINDE) {                  // Command index mismatch
      return FS_MMC_CARD_RESPONSE_GENERIC_ERROR;
    }
    if (Status & SR_RDIRE) {                  // Transmission bit not cleared
      return FS_MMC_CARD_RESPONSE_GENERIC_ERROR;
    }
  }
  if (Status & SR_RENDE) {                    // End bit is cleared
    return FS_MMC_CARD_RESPONSE_GENERIC_ERROR;
  }
  if (Status & SR_RTOE) {                     // No response received
    return FS_MMC_CARD_RESPONSE_TIMEOUT;
  }
  return FS_MMC_CARD_NO_ERROR;
}

#if USE_DMA == 0

/*********************************************************************
*
*       _WaitForRxReady
*
*   Function description
*     Waits until new data is received. The function also returns
*     in case of a receiving error.
*
*   Return values
*     FS_MMC_CARD_NO_ERROR              Success
*     FS_MMC_CARD_READ_CRC_ERROR        CRC error in received data
*     FS_MMC_CARD_READ_TIMEOUT          No data received
*     FS_MMC_CARD_READ_GENERIC_ERROR    Any other error
*/
static int _WaitForRxReady(U8 Unit) {
  U32          Status;
  HSMCI_REGS * pHSMCIRegs;

  pHSMCIRegs = GET_HSMCI_REGS(Unit);
  while (1) {
    Status = pHSMCIRegs->HSMCI_SR;
    if (Status & SR_DCRCE) {
      return FS_MMC_CARD_READ_CRC_ERROR;
    }
    if (Status & SR_OVRE) {
      return FS_MMC_CARD_READ_GENERIC_ERROR;
    }
    if (Status & SR_DTOE) {
      return FS_MMC_CARD_READ_TIMEOUT;
    }
    if (Status & SR_CSTOE) {
      return FS_MMC_CARD_READ_TIMEOUT;
    }
    if (Status & SR_RXRDY) {
      return FS_MMC_CARD_NO_ERROR;
    }
  }
}

/*********************************************************************
*
*       _WaitForTxReady
*
*   Function description
*     Waits until the transmitter is ready to send new data.
*     The function retuns in case of an underrun condition.
*     This can happen when we are not able to deliver the data fast enough.
*
*   Return values
*     FS_MMC_CARD_NO_ERROR              Success
*     FS_MMC_CARD_WRITE_GENERIC_ERROR   Any other error
*/
static int _WaitForTxReady(U8 Unit) {
  U32          Status;
  HSMCI_REGS * pHSMCIRegs;

  pHSMCIRegs = GET_HSMCI_REGS(Unit);
  while (1) {
    Status = pHSMCIRegs->HSMCI_SR;
    if (Status & SR_UNRE) {
      return FS_MMC_CARD_WRITE_GENERIC_ERROR;
    }
    if (Status & SR_DCRCE) {
      return FS_MMC_CARD_READ_CRC_ERROR;
    }
    if (Status & SR_TXRDY) {
      return FS_MMC_CARD_NO_ERROR;
    }
  }
}
#endif // USE_DMA == 0

/*********************************************************************
*
*       _WaitForCmdReady
*
*   Function description
*     Waits until for the last command to finish.
*/
static void _WaitForCmdReady(U8 Unit) {
  HSMCI_REGS * pHSMCIRegs;

  pHSMCIRegs = GET_HSMCI_REGS(Unit);
  //
  // The check for the busy flag is here to ensure
  // that a previous write transfer command has finished
  //
  while ((pHSMCIRegs->HSMCI_SR & (SR_CMDRDY | SR_NOTBUSY)) != (SR_CMDRDY | SR_NOTBUSY)) {
    ;
  }
}

/*********************************************************************
*
*       _Delay1ms
*
*   Function description
*     Busy loops for about 1 millisecond.
*/
static void _Delay1ms(void) {
  volatile int i;

  //
  // This loop takes about 1ms at 96MHz
  //
  for (i = 0; i < 0x1FFF; ++i) {
    ;
  }
}

#if USE_DMA

/*********************************************************************
*
*       _EnableDMAChannel
*
*   Function description
*     Enables the DMA channel used for the data transfer.
*/
static void _EnableDMAChannel(U8 Unit) {
  U32 Mask;
  U32 ChannelIndex;

  ChannelIndex = GET_DMA_CHANNEL_INDEX(Unit);
  Mask         = 1 << ChannelIndex;
  DMAC_CHER    = Mask;
}

/*********************************************************************
*
*       _DisableDMAChannel
*
*   Function description
*     Disables the DMA channel used for the data transfer.
*/
static void _DisableDMAChannel(U8 Unit) {
  U32 Mask;
  U32 ChannelIndex;

  ChannelIndex = GET_DMA_CHANNEL_INDEX(Unit);
  Mask         = 1 << ChannelIndex;
  DMAC_CHDR    = Mask;
  while (DMAC_CHSR & Mask) {
    ;
  }
}

/*********************************************************************
*
*       _DMAChannelIsEnabled
*
*   Function description
*     Checks the status of the DMA channel. Typ. used to determine if a data transfer has been completed.
*/
static int _DMAChannelIsEnabled(U8 Unit) {
  U32 Mask;
  U32 ChannelIndex;
  U32 IsEnabled;

  ChannelIndex = GET_DMA_CHANNEL_INDEX(Unit);
  Mask         = 1 << ChannelIndex;
  IsEnabled    = 0;
  if (DMAC_CHSR & Mask) {
    IsEnabled  = 1;
  }
  return IsEnabled;
}

/*********************************************************************
*
*       _WaitForEndOfDMAWrite
*
*   Function description
*     Waits for DMA to move data from memory to HSMCI.
*
*   Return values
*     FS_MMC_CARD_NO_ERROR      Success
*     FS_MMC_CARD_READ_TIMEOUT  No data received
*/
static int _WaitForEndOfDMAWrite(U8 Unit) {
  U32          Status;
  HSMCI_REGS * pHSMCIRegs;

  pHSMCIRegs = GET_HSMCI_REGS(Unit);
  while (1) {
    Status = pHSMCIRegs->HSMCI_SR;
    if (Status & SR_DTOE) {
      return FS_MMC_CARD_READ_TIMEOUT;
    }
    if (Status & SR_XFRDONE) {
      return FS_MMC_CARD_NO_ERROR;
    }
  }
}

/*********************************************************************
*
*       _WaitForEndOfDMARead
*
*   Function description
*     Waits for the DMA to move data from HSMCI into memory.
*
*   Return values
*     FS_MMC_CARD_NO_ERROR            Success
*     FS_MMC_CARD_READ_CRC_ERROR      CRC error in received data
*     FS_MMC_CARD_READ_TIMEOUT        No data received
*     FS_MMC_CARD_READ_GENERIC_ERROR  Any other error
*/
static int _WaitForEndOfDMARead(U8 Unit) {
  U32          Status;
  HSMCI_REGS * pHSMCIRegs;

  pHSMCIRegs = GET_HSMCI_REGS(Unit);
  while (1) {
    Status = pHSMCIRegs->HSMCI_SR;
    if (Status & SR_DCRCE) {
      return FS_MMC_CARD_READ_CRC_ERROR;
    }
    if (Status & SR_BLKOVRE) {
      return FS_MMC_CARD_READ_GENERIC_ERROR;
    }
    if (Status & SR_DTOE) {
      return FS_MMC_CARD_READ_TIMEOUT;
    }
    if (_DMAChannelIsEnabled(Unit) == 0) {
      return FS_MMC_CARD_NO_ERROR;
    }
  }
}

/*********************************************************************
*
*       _StartDMARead
*
*   Function description
*     Starts a DMA transfer to move the data from the HSMCI into
*     the read buffer. This function follows the steps described
*     in [1] for setting up a DMA transfer.
*/
static void _StartDMARead(U8 Unit) {
  volatile U32        Dummy;
  U32                 NumBytes;
  U32                 CtrlA;
  U32                 CtrlB;
  U32                 Cfg;
  DRIVER_INST       * pInst;
  DMAC_CHANNEL_REGS * pDMAChannelRegs;
  HSMCI_REGS        * pHSMCIRegs;

  _DisableDMAChannel(Unit);
  Dummy      = DMAC_EBCISR;      // Discard any pending interrupts.
  FS_USE_PARA(Dummy);
  pInst      = &_aInst[Unit];
  NumBytes   = (U32)pInst->NumBlocks * (U32)pInst->BlockSize;
  pHSMCIRegs = GET_HSMCI_REGS(Unit);
  CtrlA = (CTRLA_BCSIZE_MASK & (NumBytes / 4))
        | CTRLA_SCSIZE          // Same as DMA_CHKSIZE
        | CTRLA_DST_WITDH_WORD
        | CTRLA_SRC_WITDH_WORD
        ;
  CtrlB = CTRLB_DST_DSCR        // Destination address is continuous
        | CTRLB_SRC_DSCR
        | CTRLB_FC_P2M          // Peripheral to memory transfer
        ;
  Cfg   = CFG_SRC_H2SEL         // Pheripheral triggers the transfer
        | GET_DMA_SRC_PER(Unit)
        ;
  pDMAChannelRegs             = GET_DMA_CHANNEL_REGS(Unit);
  pDMAChannelRegs->DMAC_DSCR  = 0;
  pDMAChannelRegs->DMAC_SADDR = (U32)&pHSMCIRegs->HSMCI_FIFO;
  pDMAChannelRegs->DMAC_DADDR = (U32)pInst->BufferPAddr;
  pDMAChannelRegs->DMAC_CTRLA = CtrlA;
  pDMAChannelRegs->DMAC_CTRLB = CtrlB;
  pDMAChannelRegs->DMAC_CFG   = Cfg;
  DMAC_EN                     = 1;                  // Global DMA enable
  _EnableDMAChannel(Unit);
}

/*********************************************************************
*
*       _StartDMAWrite
*
*   Function description
*     Starts a DMA transfer to move the data from the write buffer
*     into HSMCI.
*/
static void _StartDMAWrite(U8 Unit) {
  volatile U32        Dummy;
  U32                 NumBytes;
  U32                 CtrlA;
  U32                 CtrlB;
  U32                 Cfg;
  DRIVER_INST       * pInst;
  DMAC_CHANNEL_REGS * pDMAChannelRegs;
  HSMCI_REGS        * pHSMCIRegs;

  _DisableDMAChannel(Unit);
  Dummy      = DMAC_EBCISR;     // Discard any pending interrupts.
  FS_USE_PARA(Dummy);
  pInst      = &_aInst[Unit];
  NumBytes   = (U32)pInst->NumBlocks * (U32)pInst->BlockSize;
  pHSMCIRegs = GET_HSMCI_REGS(Unit);
  CtrlA = (CTRLA_BCSIZE_MASK & (NumBytes / 4))
        | CTRLA_DCSIZE          // Same as DMA_CHKSIZE
        | CTRLA_DST_WITDH_WORD
        | CTRLA_SRC_WITDH_WORD
        ;
  CtrlB = CTRLB_FC_M2P          // Memory to peripheral transfer
        | CTRLB_DST_DSCR
        | CTRLB_SRC_DSCR
        ;
  Cfg   = CFG_DST_H2SEL         // Pheripheral triggers the transfer
        | GET_DMA_DST_PER(Unit)
        ;
  pDMAChannelRegs = GET_DMA_CHANNEL_REGS(Unit);
  pDMAChannelRegs->DMAC_DSCR  = 0;
  pDMAChannelRegs->DMAC_SADDR = (U32)pInst->BufferPAddr;
  pDMAChannelRegs->DMAC_DADDR = (U32)&pHSMCIRegs->HSMCI_FIFO;
  pDMAChannelRegs->DMAC_CTRLA = CtrlA;
  pDMAChannelRegs->DMAC_CTRLB = CtrlB;
  pDMAChannelRegs->DMAC_CFG   = Cfg;
  DMAC_EN                     = 1;                  // Global DMA enable
  _EnableDMAChannel(Unit);
}

/*********************************************************************
*
*       _ReadDMAData
*
*   Function description
*     Reads the contents of the DMA buffer
*/
static void _ReadDMAData(U8 Unit, void * pBuffer, unsigned NumBytes) {
  DRIVER_INST * pInst;

  pInst = &_aInst[Unit];
  NumBytes = MIN(NumBytes, pInst->NumBytesInBuffer);
  FS_MEMCPY(pBuffer, (U8 *)pInst->BufferVAddr, NumBytes);
}

/*********************************************************************
*
*       _WriteDMAData
*
*   Function description
*     Stores data into the DMA buffer
*/
static void _WriteDMAData(U8 Unit, const void * pBuffer, unsigned NumBytes) {
  DRIVER_INST * pInst;

  pInst = &_aInst[Unit];
  NumBytes = MIN(NumBytes, pInst->NumBytesInBuffer);
  FS_MEMCPY((U8 *)pInst->BufferVAddr, pBuffer, NumBytes);
}

/*********************************************************************
*
*     _GetTransferMemIfRequired
*
*/
static U32 _GetTransferMemIfRequired(U8 Unit) {
  DRIVER_INST * pInst;
  U32           NumBytes;

  pInst = &_aInst[Unit];
  NumBytes = pInst->NumBytesInBuffer;
  if (NumBytes == 0) {
    U32 BufferPAddr;
    U32 BufferVAddr;

    NumBytes = BSP_SD_GetTransferMem(&BufferPAddr, &BufferVAddr);
    pInst->NumBytesInBuffer = NumBytes;
    pInst->BufferPAddr      = BufferPAddr;
    pInst->BufferVAddr      = BufferVAddr;
  }
  return NumBytes;
}
#endif // USE_DMA

/*********************************************************************
*
*       _SetupHSMCI0
*
*   Function description
*     Configures the first HSMCI controller.
*/
static void _SetupHSMCI0(void) {
  //
  // Enable the clock of HSMCI and PIO
  //
  PMC_PCER = (1 << HSMCI0_ID)
           | (1 << PIOA_ID)
           | (1 << PIOD_ID)
           ;
  //
  // Give control of external port pins to HSMCI
  //
  PIOA_PDR |= PIOA_MCCK0
           |  PIOA_MCCD0
           |  PIOA_MCD00
           |  PIOA_MCD01
           |  PIOA_MCD02
           |  PIOA_MCD03
           |  PIOA_MCD04
           |  PIOA_MCD05
           |  PIOA_MCD06
           |  PIOA_MCD07
           ;
  // TBD This register is readonly
  PIOA_ABSR &= ~(PIOA_MCCK0 |
                 PIOA_MCCD0 |
                 PIOA_MCD00 |
                 PIOA_MCD01 |
                 PIOA_MCD02 |
                 PIOA_MCD03 |
                 PIOA_MCD04 |
                 PIOA_MCD05 |
                 PIOA_MCD06 |
                 PIOA_MCD07);
  PIOA_PUDR |= PIOA_MCCK0
            |  PIOA_MCCD0
            |  PIOA_MCD00
            |  PIOA_MCD01
            |  PIOA_MCD02
            |  PIOA_MCD03
            |  PIOA_MCD04
            |  PIOA_MCD05
            |  PIOA_MCD06
            |  PIOA_MCD07
            ;
}

/*********************************************************************
*
*       _SetupHSMCI1
*
*   Function description
*     Configures the second HSMCI controller.
*/
static void _SetupHSMCI1(void) {
  //
  // Enable the clock of HSMCI and PIO
  //
  PMC_PCER = (1 << HSMCI1_ID)
           | (1 << PIOA_ID)
           | (1 << PIOD_ID)
           ;
  //
  // Give control of external port pins to HSMCI
  //
  PIOA_PDR |= PIOA_MCCK1
           |  PIOA_MCCD1
           |  PIOA_MCD10
           |  PIOA_MCD11
           |  PIOA_MCD12
           |  PIOA_MCD13
           |  PIOA_MCD14
           |  PIOA_MCD15
           |  PIOA_MCD16
           |  PIOA_MCD17
           ;
  // TBD This register is readonly
  PIOA_ABSR &= ~(PIOA_MCCK1 |
                 PIOA_MCCD1 |
                 PIOA_MCD10 |
                 PIOA_MCD11 |
                 PIOA_MCD12 |
                 PIOA_MCD13 |
                 PIOA_MCD14 |
                 PIOA_MCD15 |
                 PIOA_MCD16 |
                 PIOA_MCD17);
  PIOA_PUDR |= PIOA_MCCK1
            |  PIOA_MCCD1
            |  PIOA_MCD10
            |  PIOA_MCD11
            |  PIOA_MCD12
            |  PIOA_MCD13
            |  PIOA_MCD14
            |  PIOA_MCD15
            |  PIOA_MCD16
            |  PIOA_MCD17
            ;
}

/*********************************************************************
*
*     _IsCardDetectedOnHSMCI0
*
*   Function description
*     Checks if there is a card inserted on the socket of the HSMCI0 controller
*
*   Return values
*     !=0   Card inserted
*     ==0   No card detected
*/
static int _IsCardDetectedOnHSMCI0(void) {
  PIOD_PER = PIOD_MCI0_CD;
  if (PIOD_PDSR & PIOD_MCI0_CD) {
    return 0;
  } else {
    return 1;
  }
}

/*********************************************************************
*
*     _IsCardDetectedOnHSMCI1
*
*   Function description
*     Checks if there is a card inserted on the socket of the HSMCI1 controller
*
*   Return values
*     !=0   Card inserted
*     ==0   No card detected
*/
static int _IsCardDetectedOnHSMCI1(void) {
  PIOD_PER = PIOD_MCI1_CD;
  if (PIOD_PDSR & PIOD_MCI1_CD) {
    return 0;
  } else {
    return 1;
  }
}

/*********************************************************************
*
*     _IsHSMCI1WriteProtected
*
*   Function description
*     Checks if the card inserted on the socket of HSMCI1 is write protected
*
*   Return values
*     !=0   Write protected
*     ==0   Writeable
*/
static int _IsHSMCI1WriteProtected(void) {
  PIOD_PER = PIOD_MCI1_WP;
  if (PIOD_PDSR & PIOD_MCI1_WP) {
    return 1;
  } else {
    return 0;
  }
}

/*********************************************************************
*
*       Public code
*
**********************************************************************
*/

/*********************************************************************
*
*       FS_MMC_HW_X_SetHWNumBlocks
*
*  Function description
*    Sets the number of block (sectors) to be transferred.
*
*/
void FS_MMC_HW_X_SetHWNumBlocks(U8 Unit, U16 NumBlocks) {
  _aInst[Unit].NumBlocks = NumBlocks;
}

/*********************************************************************
*
*       FS_MMC_HW_X_SetHWBlockLen
*
*   Function description
*    Sets the block size (sector size) that shall be transferred.
*
*   Note
     Block size *not* multiple of 4 bytes are allowed only
*    if FBYTE in the HSMCI_MR is set.
*/
void FS_MMC_HW_X_SetHWBlockLen(U8 Unit, U16 BlockSize) {
  _aInst[Unit].BlockSize = BlockSize;
}

/*********************************************************************
*
*       FS_MMC_HW_X_SetMaxSpeed
*
*  Function description
*    Sets the frequency of the MMC/SD card controller.
*    The frequency is given in kHz.
*    It is called 2 times:
*     1. During card initialization
*        Initialize the frequency to not more than 400kHz.
*
*     2. After card initialization
*        The CSD register of card is read and the max frequency
*        the card can operate is determined.
*        [In most cases: MMC cards 20MHz, SD cards 25MHz]
*
*/
U16 FS_MMC_HW_X_SetMaxSpeed(U8 Unit, U16 Freq) {
  U32          PClk;
  U32          ClkDiv;
  HSMCI_REGS * pHSMCIRegs;

  pHSMCIRegs = GET_HSMCI_REGS(Unit);
  PClk = MCK / 1000;   // Convert to kHz
  if (PClk < Freq) {
    Freq = PClk;
  } else {
    ClkDiv = _ComputeClockDivisor(PClk, Freq);
    pHSMCIRegs->HSMCI_CR = CR_MCIDIS;
    pHSMCIRegs->HSMCI_MR &= ~0xFFuL;
    pHSMCIRegs->HSMCI_MR |= ClkDiv & 0xFF;
    pHSMCIRegs->HSMCI_CR = CR_MCIEN;
  }
  //
  // In the high-speed mode the clock frequency is always bigger than 25MHz
  //
  if (Freq > 25000) {
    pHSMCIRegs->HSMCI_CFG |= CFG_HSMODE;
  } else {
    pHSMCIRegs->HSMCI_CFG &= ~CFG_HSMODE;
  }
  return Freq;
}

/*********************************************************************
*
*       FS_MMC_HW_X_IsPresent
*
*    Returns the state of the media. If you do not know the state, return
*    FS_MEDIA_STATE_UNKNOWN and the higher layer will try to figure out if
*    a media is present.
*
*  Parameters:
*    Unit                 - Device Index
*
*  Return value:
*    FS_MEDIA_STATE_UNKNOWN      - the state of the media is unkown
*    FS_MEDIA_NOT_PRESENT        - no card is present
*    FS_MEDIA_IS_PRESENT         - a card is present
*/
int FS_MMC_HW_X_IsPresent(U8 Unit) {
  int CardDetected;

  //
  // Specific to AT91SAM9G45-EKES board
  //
  if (Unit == 0) {
    CardDetected = _IsCardDetectedOnHSMCI0();
  } else {
    CardDetected = _IsCardDetectedOnHSMCI1();
  }
  if (CardDetected) {
    return FS_MEDIA_IS_PRESENT;
  } else {
    return FS_MEDIA_NOT_PRESENT;
  }
}

/*********************************************************************
*
*       FS_MMC_HW_X_IsWriteProtected
*
*  Function description
*    Returns wheter card is write protected or not.
*/
int FS_MMC_HW_X_IsWriteProtected(U8 Unit) {
  int WriteProtected;

  //
  // Specific to AT91SAM9G45-EKES board
  // Only the HSMCI1 controller has a write protected signal
  //
  WriteProtected = 0;
  if (Unit != 0) {
    WriteProtected = _IsHSMCI1WriteProtected();
  }
  return WriteProtected;
}

/*********************************************************************
*
*       FS_MMC_HW_X_SetResponseTimeOut
*
*  Function description
*    Sets the reponse time out value given in MMC/SD card cycles.
*
*/
void FS_MMC_HW_X_SetResponseTimeOut(U8 Unit, U32 Value) {
  FS_USE_PARA(Unit);
  FS_USE_PARA(Value);
  //
  // The hardware has only a short and a long timeout.
  // We always set the long timeout before we send a command.
  //
}

/*********************************************************************
*
*       FS_MMC_HW_X_SetReadDataTimeOut
*
*  Function description
*    Sets the read data time out value given in MMC/SD card cycles.
*/
void FS_MMC_HW_X_SetReadDataTimeOut(U8 Unit, U32 Value) {
  FS_USE_PARA(Unit);
  FS_USE_PARA(Value);
  //
  // We always use the maximum timeout here
  //
}

/*********************************************************************
*
*       FS_MMC_HW_X_SendCmd
*
*   Function description
*     Sends a command to the MMC/SD card.
*
*   Parameters
*     Unit          Number of physical card controller
*     Cmd           Command number according to [2]
*     CmdFlags      Additional information about the command to execute
*     ResponseType  Type of response as defined in [2]
*     Arg           Command parameter
*/
void FS_MMC_HW_X_SendCmd(U8 Unit, unsigned Cmd, unsigned CmdFlags, unsigned ResponseType, U32 Arg) {
  unsigned      CmdRegValue;
  HSMCI_REGS  * pHSMCIRegs;
  DRIVER_INST * pInst;

  pHSMCIRegs = GET_HSMCI_REGS(Unit);
  _WaitForCmdReady(Unit);
  CmdRegValue = (Cmd & CMDR_CMDNB_MASK)   // Limit the command to the valid range
              | CMDR_MAXLAT               // Max latency 64-cycles
              ;
  switch (ResponseType) {
  //
  //  No response is expected
  //
  case FS_MMC_RESPONSE_FORMAT_NONE:
  default:
    CmdRegValue |= CMDR_RSPTYP_NONE;
    break;
  //
  //  Short response is expected (48bit)
  //
  case FS_MMC_RESPONSE_FORMAT_R1:
  case FS_MMC_RESPONSE_FORMAT_R3:
    CmdRegValue |= CMDR_RSPTYP_48BIT;
    break;
  //
  //  Long response is expected (136bit)
  //
  case FS_MMC_RESPONSE_FORMAT_R2:
    CmdRegValue |= CMDR_RSPTYP_136BIT;
    break;
  }
  pInst = &_aInst[Unit];
  //
  // According to specs the R3 response has no CRC.
  // The hardware sets the CRC error bit when it receives such a response
  // so we have to ignore it.
  //
  if (ResponseType == FS_MMC_RESPONSE_FORMAT_R3) {
    pInst->IgnoreCRC = 1;
  } else {
    pInst->IgnoreCRC = 0;
  }
  pHSMCIRegs->HSMCI_BLKR = ((U32)pInst->BlockSize << 16) | (U32)pInst->NumBlocks;
  if (CmdFlags & FS_MMC_CMD_FLAG_DATATRANSFER) {
    CmdRegValue &= ~CMDR_TRCMD_MASK;
    CmdRegValue |= CMDR_TRCMD_START;
    //
    // Start a multiple block transfer if the high-level driver
    // wants to send more than 1 block at a time
    //
    if (pInst->NumBlocks > 1) {
      CmdRegValue |= CMDR_TRTYP_MULTI;
    }
  }
  if (CmdFlags & FS_MMC_CMD_FLAG_STOP_TRANS) {        // Stop the current transfer
    CmdRegValue &= ~CMDR_TRCMD_MASK;
    CmdRegValue |= CMDR_TRCMD_STOP;
  }
  if (!(CmdFlags & FS_MMC_CMD_FLAG_WRITETRANSFER)) {  // Read transfer?
    CmdRegValue |= CMDR_TRDIR;
  }
#if USE_DMA
  if ((CmdFlags & FS_MMC_CMD_FLAG_DATATRANSFER) &&
      !(CmdFlags & FS_MMC_CMD_FLAG_WRITETRANSFER)) {
    _StartDMARead(Unit);
  } else {
    _DisableDMAChannel(Unit);
  }
#endif // USE_DMA
  pHSMCIRegs->HSMCI_SDCR &= ~SDCR_SDCBUS_MASK;
  if (CmdFlags & FS_MMC_CMD_FLAG_USE_MMC8MODE) {        // use 8 data lines?
    pHSMCIRegs->HSMCI_SDCR |= SDCR_SDCBUS_8BIT;
  } else if (CmdFlags & FS_MMC_CMD_FLAG_USE_SD4MODE) {  // use 4 data lines?
    pHSMCIRegs->HSMCI_SDCR |= SDCR_SDCBUS_4BIT;
  } else {
    pHSMCIRegs->HSMCI_SDCR |= SDCR_SDCBUS_1BIT;
  }
  pHSMCIRegs->HSMCI_ARGR = Arg;
  pHSMCIRegs->HSMCI_CMDR = CmdRegValue;
}

/*********************************************************************
*
*       FS_MMC_HW_X_GetResponse
*
*  Function description
*    Receives the reponses that was sent by the card after
*    a command was sent to the card.
*
*  Parameters
*    Unit     Number of physical card controller
*    pBuffer  User allocated buffer where the response is stored.
*    Size     Size of the buffer in bytes
*
*  Return values
*     FS_MMC_CARD_NO_ERROR                Success
*     FS_MMC_CARD_RESPONSE_CRC_ERROR      CRC error in response
*     FS_MMC_CARD_RESPONSE_TIMEOUT        No response received
*     FS_MMC_CARD_RESPONSE_GENERIC_ERROR  Any other error
*
*  Notes
*     (1) The driver expects the whole response as it is received form the SD card
*         even though only the payload is used.
*         In other words: if the card controller delivers only the reponse payload
*         it should be stored to buffer starting at byte index 1.
*         Byte index 0 is the start bit, transmission bit  and the 6 stuff bits (see [2] chapter 4.9).
*
*/
int FS_MMC_HW_X_GetResponse(U8 Unit, void * pBuffer, U32 Size) {
  U8         * p;
  U32          NumWords;
  U32          Value;
  U32          i;
  int          r;
  HSMCI_REGS * pHSMCIRegs;

  pHSMCIRegs = GET_HSMCI_REGS(Unit);
  r = _WaitForResponse(Unit);
  if (r != FS_MMC_CARD_NO_ERROR) {
    return r;
  }
  //
  // The size we get from the upper driver is total response size in bytes.
  // We compute here the number of read accesses to the 32-bit register which
  // holds the response. Take into account that the first byte of the response
  // is not delivered by the hardware
  //
  NumWords = (Size - 1) / 4;
  p        = (U8 *)pBuffer;
  ++p;    // See note (1)
  for (i = 0; i < NumWords; ++i) {
    Value = pHSMCIRegs->HSMCI_RSPR[0];
    *p = (U8)(Value >> 24);
    ++p;
    *p = (U8)(Value >> 16);
    ++p;
    *p = (U8)(Value >> 8);
    ++p;
    *p = (U8)Value;
    ++p;
  }
  return FS_MMC_CARD_NO_ERROR;
}

/*********************************************************************
*
*       FS_MMC_HW_X_ReadData
*
*   Function description
*     Reads data from MMC/SD card through the MMC/SD card controller.
*
*   Parameters
*     Unit
*     pBuffer
*     NumBytes
*     NumBlocks
*
*   Return values
*     FS_MMC_CARD_NO_ERROR            Success
*     FS_MMC_CARD_READ_CRC_ERROR      CRC error in received data
*     FS_MMC_CARD_READ_TIMEOUT        No data received
*     FS_MMC_CARD_READ_GENERIC_ERROR  Any other error
*
*/
int FS_MMC_HW_X_ReadData(U8 Unit, void * pBuffer, unsigned NumBytes, unsigned NumBlocks) {
#if USE_DMA
  int r;

  FS_USE_PARA(pBuffer);
  FS_USE_PARA(NumBytes);
  FS_USE_PARA(NumBlocks);
  r = _WaitForEndOfDMARead(Unit);
  if (r == 0) {
    _ReadDMAData(Unit, pBuffer, NumBytes * NumBlocks);
  }
  return r;
#else
  unsigned     NumWords;
  unsigned     i;
  U8         * p;
  U32          Value;
  int          r;
  HSMCI_REGS * pHSMCIRegs;

  pHSMCIRegs = GET_HSMCI_REGS(Unit);
  p          = (U8 *)pBuffer;
  NumWords   = (NumBytes * NumBlocks) / 4;
  for (i = 0; i < NumWords; ++i) {
    //
    // Wait for data to arrive or an for error to occur
    //
    r = _WaitForRxReady(Unit);
    if (r != FS_MMC_CARD_NO_ERROR) {
      return r;
    }
    Value = pHSMCIRegs->HSMCI_RDR;
    *p = (U8)Value;
    ++p;
    *p = (U8)(Value >> 8);
    ++p;
    *p = (U8)(Value >> 16);
    ++p;
    *p = (U8)(Value >> 24);
    ++p;
  }
  return FS_MMC_CARD_NO_ERROR;
#endif // USE_DMA
}

/*********************************************************************
*
*       FS_MMC_HW_X_WriteData
*
*  Function description
*    Writes the data to MMC/SD card through the MMC/SD card controller.
*
*   Parameters
*     Unit
*     pBuffer
*     NumBytes
*     NumBlocks
*
*   Return values
*     FS_MMC_CARD_NO_ERROR      Success
*     FS_MMC_CARD_READ_TIMEOUT  No data received
*/
int FS_MMC_HW_X_WriteData(U8 Unit, const void * pBuffer, unsigned NumBytes, unsigned NumBlocks) {
#if USE_DMA
  FS_USE_PARA(pBuffer);
  FS_USE_PARA(NumBytes);
  FS_USE_PARA(NumBlocks);
  _WriteDMAData(Unit, pBuffer, NumBytes * NumBlocks);
  _StartDMAWrite(Unit);
  return _WaitForEndOfDMAWrite(Unit);
#else
  unsigned     NumWords;
  unsigned     i;
  const U8   * p;
  U32          Value;
  int          r;
  HSMCI_REGS * pHSMCIRegs;

  pHSMCIRegs = GET_HSMCI_REGS(Unit);
  p          = (U8 *)pBuffer;
  NumWords   = (NumBytes * NumBlocks) / 4;
  for (i = 0; i < NumWords; ++i) {
    //
    // Wait for transmitter ready
    //
    r = _WaitForTxReady(Unit);
    if (r != FS_MMC_CARD_NO_ERROR) {
      return r;
    }
    Value = (U32)(*p);
    ++p;
    Value |= (U32)(*p) << 8;
    ++p;
    Value |= (U32)(*p) << 16;
    ++p;
    Value |= (U32)(*p) << 24;
    ++p;
    pHSMCIRegs->HSMCI_TDR = Value;
  }
  return FS_MMC_CARD_NO_ERROR;
#endif // USE_DMA
}

/*********************************************************************
*
*       FS_MMC_HW_X_Delay
*
*   Function description
*     Waits for a certain time given by the parameter.
*
*   Parameters
*     ms
*/
void FS_MMC_HW_X_Delay(int ms) {
  int i;

  for (i = 0; i < ms; ++i) {
    _Delay1ms();
  }
}

/*********************************************************************
*
*       FS_MMC_HW_X_InitHW
*
*   Function description
*     Initialize the MMC/SD card controller.
*
*   Parameters
*     Unit
*
*/
void FS_MMC_HW_X_InitHW(U8 Unit) {
  HSMCI_REGS  * pHSMCIRegs;

  if (Unit == 0) {
    _SetupHSMCI0();
  } else {
    _SetupHSMCI1();
  }
  pHSMCIRegs              = GET_HSMCI_REGS(Unit);
  pHSMCIRegs->HSMCI_CR    = CR_SWRST          // Reset the HSMCI module
                          | CR_MCIDIS         // Disable the HSMCI module
                          | CR_PWSDIS         // Disable the power down mode
                          ;
  pHSMCIRegs->HSMCI_IDR   = ~0uL;             // We work in polling mode, no need of interrupts
  pHSMCIRegs->HSMCI_SDCR  = SDCR_SDCBUS_1BIT; // Start in 1-bit mode, slot A
  pHSMCIRegs->HSMCI_MR    = _ComputeClockDivisor(MCK / 1000, DEFAULT_SPEED)
                          | MR_WRPROOF
                          | MR_RDPROOF
                          ;
  pHSMCIRegs->HSMCI_CFG   = CFG_FIFOMODE      // Start transfer as soon as data is written
                          | CFG_FERRCTRL      // A FIFO error is reset when the Control Register is read
                          ;
#if USE_DMA
  pHSMCIRegs->HSMCI_DMA   = DMA_DMAEN         // Enable the DMA handshake
                          | DMA_CHKSIZE       // Number of data available over DMA is always 4 bytes
                          ;
  PMC_PCER                = (1 << DMAC_ID);   // Enable the clock of DMA
  _GetTransferMemIfRequired(Unit);
#endif // USE_DMA
  pHSMCIRegs->HSMCI_DTOR  = DEFAULT_TIMEOUT;
  pHSMCIRegs->HSMCI_CSTOR = DEFAULT_TIMEOUT;
  pHSMCIRegs->HSMCI_CR    = CR_MCIEN;         // Enable the operation of HSMCI
}

/*********************************************************************
*
*       FS_MMC_HW_X_GetMaxReadBurst
*
*  Function description
*    Returns the number of block (sectors)
*    that can be read at once with a single
*    READ_MULTIPLE_SECTORS.
*
*/
U16 FS_MMC_HW_X_GetMaxReadBurst(U8 Unit) {
#if USE_DMA
  U16 NumSectors;
  U32 NumBytes;

  NumBytes   = _GetTransferMemIfRequired(Unit);
  NumSectors = NumBytes / BYTES_PER_SECTOR;
  return NumSectors;
#else
  FS_USE_PARA(Unit);
  return NUM_BLOCKS_AT_ONCE;
#endif
}

/*********************************************************************
*
*       FS_MMC_HW_X_GetMaxWriteBurst
*
*  Function description
*    Returns the number of block (sectors)
*    that can be written at once with a single
*    WRITE_MULTIPLE_SECTORS.
*/
U16 FS_MMC_HW_X_GetMaxWriteBurst(U8 Unit) {
#if USE_DMA
  U16 NumSectors;
  U32 NumBytes;

  NumBytes   = _GetTransferMemIfRequired(Unit);
  NumSectors = NumBytes / BYTES_PER_SECTOR;
  return NumSectors;
#else
  FS_USE_PARA(Unit);
  return NUM_BLOCKS_AT_ONCE;
#endif
}

/*********************************************************************
*
*       FS_MMC_HW_X_SetDataPointer
*
*  Function description
*    Tells the hardware layer where to read data from
*    or write data to. This may be necessary for some controller,
*    before sending the command to the card, eg. programming the DMA.
*    In most cases this function can be left empty.
*
*  Parameters:
*    Unit          - SD card controller no, in case there are more than one.
*
*/
void FS_MMC_HW_X_SetDataPointer(U8 Unit, const void * p) {
  FS_USE_PARA(Unit);
  FS_USE_PARA(p);
}
