/*********************************************************************
*                SEGGER MICROCONTROLLER GmbH & Co. KG                *
*        Solutions for real time microcontroller applications        *
**********************************************************************
*                                                                    *
*        (c) 2003-2012     SEGGER Microcontroller GmbH & Co KG       *
*                                                                    *
*        Internet: www.segger.com    Support:  support@segger.com    *
*                                                                    *
**********************************************************************

**** emFile file system for embedded applications ****
emFile is protected by international copyright laws. Knowledge of the
source code may not be used to write a similar product. This file may
only be used in accordance with a license and should not be re-
distributed in any way. We appreciate your understanding and fairness.
----------------------------------------------------------------------
----------------------------------------------------------------------
File        : MMC_CM_HW.c
Purpose     : Low level MMC/SD driver for the Sharp Toshiba TMP910A
---------------------------END-OF-HEADER------------------------------
*/

/*********************************************************************
*
*       Includes
*
**********************************************************************
*/
#include "FS_Int.h"
#include "MMC_SD_CardMode_X_HW.h"

/*********************************************************************
*
*       Defines non configurable
*
**********************************************************************
*/

/*********************************************************************
*
*       Local defines (sfrs)
*
**********************************************************************
*/
#define  CPU_HCLK           96000UL   // 96 MHz is set as HCLK given in kHz


#define _GPIOG_BASE_ADDR     0xF0806000
#define _GPIOG_FR1_OFFSET    0x424
#define _GPIOG_FR1           (*(volatile U32*) (_GPIOG_BASE_ADDR + _GPIOG_FR1_OFFSET))

#define SD_CARD_BASE 0xF2030000
#define SD_CMD            *(volatile U16 *)(SD_CARD_BASE + 0x0000) // SD command setting
#define SD_PORTSEL        *(volatile U16 *)(SD_CARD_BASE + 0x0004) // SD card port select
#define SD_ARG0           *(volatile U16 *)(SD_CARD_BASE + 0x0008) // SD command argument [15:0]
#define SD_ARG1           *(volatile U16 *)(SD_CARD_BASE + 0x000C) // SD command argument [31:16]
#define SD_STOP           *(volatile U16 *)(SD_CARD_BASE + 0x0010) // Stop internal action
#define SD_SECCNT         *(volatile U16 *)(SD_CARD_BASE + 0x0014) // Sector transfer count
#define SD_RSP0           *(volatile U16 *)(SD_CARD_BASE + 0x0018) // Response data (23:8)
#define SD_RSP1           *(volatile U16 *)(SD_CARD_BASE + 0x001C) // Response data (39:24)
#define SD_RSP2           *(volatile U16 *)(SD_CARD_BASE + 0x0020) // Response data (55:40)
#define SD_RSP3           *(volatile U16 *)(SD_CARD_BASE + 0x0024) // Response data (71:56)
#define SD_RSP4           *(volatile U16 *)(SD_CARD_BASE + 0x0028) // Response data (87:72)
#define SD_RSP5           *(volatile U16 *)(SD_CARD_BASE + 0x002C) // Response data (103:88)
#define SD_RSP6           *(volatile U16 *)(SD_CARD_BASE + 0x0030) // Response data (119:104)
#define SD_RSP7           *(volatile U16 *)(SD_CARD_BASE + 0x0034) // Response data (127:120)
#define SD_INFO1          *(volatile U16 *)(SD_CARD_BASE + 0x0038) // Card status (Detect, Write protect etc.)
#define SD_INFO2          *(volatile U16 *)(SD_CARD_BASE + 0x003C) // SD Buffer control and Error
#define SD_INFO1_MASK     *(volatile U16 *)(SD_CARD_BASE + 0x0040) // SD_INFO1 interrupt MASK
#define SD_INFO2_MASK     *(volatile U16 *)(SD_CARD_BASE + 0x0044) // SD_INFO2 interrupt MASK
#define SD_CLK_CTRL       *(volatile U16 *)(SD_CARD_BASE + 0x0048) // SD memory card clock control
#define SD_SIZE           *(volatile U16 *)(SD_CARD_BASE + 0x004C) // SD memory card data transfer size
#define SD_OPTION         *(volatile U16 *)(SD_CARD_BASE + 0x0050) // Setup options for SD memory card
#define SD_ERR_STS1       *(volatile U16 *)(SD_CARD_BASE + 0x0058) // CMD, CRC, END error status
#define SD_ERR_STS2       *(volatile U16 *)(SD_CARD_BASE + 0x005C) // Timeout error status
#define SD_BUF            *(volatile U16 *)(SD_CARD_BASE + 0x0060) // Data buffer for SD memory card
#define SDIO_MODE         *(volatile U16 *)(SD_CARD_BASE + 0x0068) // SDIO mode control
#define SDIO_INFO1        *(volatile U16 *)(SD_CARD_BASE + 0x006C) // SDIO information
#define SDIO_INFO1_MASK   *(volatile U16 *)(SD_CARD_BASE + 0x0070) // SDIO information MASK
// Reserved 0x0080 - 0x01AC
#define CC_EXT_MODE       *(volatile U16 *)(SD_CARD_BASE + 0x01B0) // Buffer , Status mode enable
 // Reserved 0x01B4
#define SOFT_RESET        *(volatile U16 *)(SD_CARD_BASE + 0x01C0) // Soft reset for Internal circuit
 // Reserved 0x01D4 - 0x01DC
#define EXT_SDIO          *(volatile U16 *)(SD_CARD_BASE + 0x01E8) // Expansion port SDIO INFO
#define EXT_WP            *(volatile U16 *)(SD_CARD_BASE + 0x01EC) // Expansion write protect
#define EXT_CD            *(volatile U16 *)(SD_CARD_BASE + 0x01F0) // Expansion card detection
#define EXT_CD_DAT3       *(volatile U16 *)(SD_CARD_BASE + 0x01F4) // Expansion card detection (DAT3)
#define EXT_CD_MASK       *(volatile U16 *)(SD_CARD_BASE + 0x01F8) // Expansion card detection MASK
#define EXT_CD_DAT3_MASK  *(volatile U16 *)(SD_CARD_BASE + 0x01FC) // Expansion card detection (DAT3)

#define SD_RESPONSE_BASE  (SD_CARD_BASE + 0x0018)

/*********************************************************************
*
*       Public code
*
**********************************************************************
*/


/*********************************************************************
*
*       FS_MMC_HW_X_SetHWNumBlocks
*
*  Function description
*    Sets the number of block (sectors) to be transferred.
*
*/
void FS_MMC_HW_X_SetHWNumBlocks(U8 Unit, U16 NumBlocks) {
   SD_SECCNT = NumBlocks;
}

/*********************************************************************
*
*       FS_MMC_HW_X_SetHWBlockLen
*
*  Function description
*    Sets the block size (sector size) that shall be transferred.
*/
void FS_MMC_HW_X_SetHWBlockLen(U8 Unit, U16 BlockSize) {
  SD_SIZE = BlockSize;
}


/*********************************************************************
*
*       FS_MMC_HW_X_SetMaxSpeed
*
*  Function description
*    Sets the frequency of the MMC/SD card controller.
*    The frequency is given in kHz.
*    It is called 2 times:
*     1. During card initialization
*        Initialize the frequency to not more than 400kHz.
*        
*     2. After card initialization
*        The CSD register of card is read and the max frequency 
*        the card can operate is determined.
*        [In most cases: MMC cards 20MHz, SD cards 25MHz]
*
*/
U16 FS_MMC_HW_X_SetMaxSpeed(U8 Unit, U16 Freq) {
  unsigned Shift;
  U32      SDFreq;

  Shift = 1;
  do {
    SDFreq = CPU_HCLK >> Shift;
    if (SDFreq <= Freq) {
      break;
    }
    Shift++;
  } while (Shift < 10);
  SD_CLK_CTRL = (1 << (Shift - 2))
              | (1 << 8)
              ;
  return SDFreq;
}

/*********************************************************************
*
*       FS_MMC_HW_X_IsPresent
*
*    Returns the state of the media. If you do not know the state, return
*    FS_MEDIA_STATE_UNKNOWN and the higher layer will try to figure out if
*    a media is present.
*
*  Parameters:
*    Unit                 - Device Index
*
*  Return value:
*    FS_MEDIA_STATE_UNKNOWN      - the state of the media is unknown
*    FS_MEDIA_NOT_PRESENT        - no card is present
*    FS_MEDIA_IS_PRESENT         - a card is present
*/
int FS_MMC_HW_X_IsPresent(U8 Unit) {
  return (SD_INFO1 & (1 << 4)) ? FS_MEDIA_IS_PRESENT : FS_MEDIA_NOT_PRESENT;
}

/*********************************************************************
*
*       FS_MMC_HW_X_IsWriteProtected
*
*  Function description
*    Returns wheter card is write protected or not.
*/
int    FS_MMC_HW_X_IsWriteProtected  (U8 Unit) {
  return 0;
}

/*********************************************************************
*
*       FS_MMC_HW_X_SetResponseTimeOut
*
*  Function description
*    Sets the reponse time out value given in MMC/SD card cycles.
*
*/
void FS_MMC_HW_X_SetResponseTimeOut(U8 Unit, U32 Value) {
}

/*********************************************************************
*
*       FS_MMC_HW_X_SetReadDataTimeOut
*
*  Function description
*    Sets the read data time out value given in MMC/SD card cycles.
*/
void FS_MMC_HW_X_SetReadDataTimeOut(U8 Unit, U32 Value) {
}
static U8 _IsACMD;
/*********************************************************************
*
*       FS_MMC_HW_X_SendCmd
*
*  Function description
*    Sends a command to the MMC/SD card.
*/
void FS_MMC_HW_X_SendCmd(U8 Unit, unsigned Cmd, unsigned CmdFlags, unsigned ResponseType, U32 Arg) {
  U16 CmdReg = 0;  
  U16 Response;
  U16 Mode = 0;
  U16 rw   = 0;
  U16 IsMultiTransfer   = 0;
  
  SD_INFO1 &= ~(1 << 0);
  switch (ResponseType) {
  //
  //  No response is expected
  //
  case FS_MMC_RESPONSE_FORMAT_NONE:
  default:
    Response = 3;
    break;
  //
  //  Short response is expected (48bit)
  //
  case FS_MMC_RESPONSE_FORMAT_R1:
    if (CmdFlags & FS_MMC_CMD_FLAG_SETBUSY) {
      Response = 5;
    } else {
      Response = 4;
    }
    break;
  case FS_MMC_RESPONSE_FORMAT_R3:
    Response = 7;
    break;
  //
  //  Long response is expected (136bit)
  //
  case FS_MMC_RESPONSE_FORMAT_R2:
    Response = 6;
    break;
  }
  //
  // Handle the flags;
  //
  if (CmdFlags & FS_MMC_CMD_FLAG_DATATRANSFER) { /* If data transfer */
    rw = 0;
    Mode = 1;
  }
  if (CmdFlags & FS_MMC_CMD_FLAG_WRITETRANSFER) {   /* Write transfer ? */
    // Set WRITE bit
    rw = 1;
    Mode = 1;
  }
  if (CmdFlags & FS_MMC_CMD_FLAG_USE_SD4MODE) {   /* 4 bit mode ? */
   SD_OPTION |= (1 << 15) | (1 << 14);
  } else {
     // Clear WIDE bit
     SD_OPTION &= ~(1 << 15);
  }
  
  if (Cmd == 55) {
    _IsACMD = 1;
  } else if (Cmd == 18 || Cmd == 25) {
    IsMultiTransfer = 1;
    SD_STOP = 1 << 8;
  } else if (_IsACMD) {
    CmdReg  |= (1 << 6);
    _IsACMD  = 0;
  }
  CmdReg |= Cmd & 0x3f | (Response << 8) | (Mode << 11) | (rw << 12) | (IsMultiTransfer << 13);
  SD_ARG0 = (U16)(Arg & 0xffff);
  SD_ARG1 = (U16)(Arg >> 16);
  SD_CMD  = CmdReg;
}

/*********************************************************************
*
*       FS_MMC_HW_X_GetResponse
*
*  Function description
*    Receives the reponses that was sent by the card after
*    a command was sent to the card.
*/
int FS_MMC_HW_X_GetResponse(U8 Unit, void *pBuffer, U32 Size) {
  U16   Data16;
  U8  * pResponse;
  volatile U16 * pResponseReg;
  unsigned NumItems;
  
  while((SD_INFO1 & (1 << 0)) != (1 << 0));
  SD_INFO1 &= ~(1 << 0);
  if (SD_INFO2 & (1 << 6)) {
    SD_INFO2 &= ~(1 << 6);
    return FS_MMC_CARD_RESPONSE_TIMEOUT;
  }
  if (SD_INFO2 & (1 << 1)) {
    SD_INFO2 &= ~(1 << 1);
    return FS_MMC_CARD_RESPONSE_CRC_ERROR;
  }
  Size         -= 2;
  NumItems      = Size >> 1;
  pResponse     = (U8 *)pBuffer + Size - 1;
  pResponseReg  = (U16 *)(SD_RESPONSE_BASE);
  do {
    Data16 = FS_LoadU16BE((const U8 *)pResponseReg);
    FS_StoreU16LE(pResponse, Data16);
    pResponse    -= 2;
    pResponseReg += 2;
  } while (--NumItems);
  return 0;
}

/*********************************************************************
*
*       FS_MMC_HW_X_ReadData
*
*  Function description
*    Reads data from MMC/SD card through the MMC/SD card controller.
*
*/
int FS_MMC_HW_X_ReadData (U8 Unit, void * pBuffer, unsigned NumBytes, unsigned NumBlocks) {
  unsigned   i;
  U16      * pBuf = (U16 *)pBuffer;

  do {
#if 0    
    i = 0;
    // Wait until transfer is complete
    while ((SD_INFO2 & (1 << 8)) == 0) {
      if (SD_INFO2 & (1 << 3)) {
        return FS_MMC_CARD_READ_TIMEOUT;
      }
      if (SD_INFO2 & (1 << 1)) {
        return FS_MMC_CARD_READ_CRC_ERROR;
      }
    }
    // Continue reading data until FIFO is empty
    while (((SD_INFO2 & (1 << 8))) && (i < (NumBytes >> 1))) {
      // Any data in the FIFO
      if ((SD_INFO2 & (1 << 8)) == 0) {
        *pBuf++ = (U16)SD_BUF;
        i++;
      }
    }
#else
    for (i = 0; i < (NumBytes >> 1); i++) {
      *pBuf++ = (U16)SD_BUF;
    }
#endif
  } while (--NumBlocks); 
  return 0;

}

/*********************************************************************
*
*       FS_MMC_HW_X_WriteData
*
*  Function description
*    Writes the data to MMC/SD card through the MMC/SD card controller.
*
*         
*/
int FS_MMC_HW_X_WriteData(U8 Unit, const void * pBuffer, unsigned NumBytes, unsigned NumBlocks) {
  return 0;
}

/*********************************************************************
*
*       FS_MMC_HW_X_Delay
*
*  Function description
*    Waits for a certain time given by the parameter.
*
*/
void FS_MMC_HW_X_Delay(int ms) {
  volatile unsigned Time;

  Time = ms * 50 * 1000;
  do {} while(--Time);
}

/*********************************************************************
*
*       FS_MMC_HW_X_InitHW
*
*  Function description
*    Initialize the MMC/SD card controller.
*/
void FS_MMC_HW_X_InitHW(U8 Unit) {
  //
  // Set GPIO G pin to alternate functions -> SD card pins
  //
  _GPIOG_FR1 = 0xff;
  //
  // Reset the SD memory card controller
  //
  SOFT_RESET = 0;
  FS_MMC_HW_X_Delay(250);
  SOFT_RESET = 1;
  //
  // Disable all interrupts
  //
  SD_INFO1_MASK  = 0;
  SD_INFO2_MASK  = 0;
}

/*********************************************************************
*
*       FS_MMC_HW_X_GetMaxReadBurst
*
*  Function description
*    Returns the number of block (sectors)
*    that can be read at once with a single
*    READ_MULTIPLE_SECTORS.
*
*/
U16 FS_MMC_HW_X_GetMaxReadBurst(U8 Unit) {
  return 0xffff;
}

/*********************************************************************
*
*       FS_MMC_HW_X_GetMaxWriteBurst
*
*  Function description
*    Returns the number of block (sectors)
*    that can be written at once with a single
*    WRITE_MULTIPLE_SECTORS.
*/
U16 FS_MMC_HW_X_GetMaxWriteBurst(U8 Unit) {
  return 0xffff;
}
