/*********************************************************************
*                SEGGER MICROCONTROLLER GmbH & Co. KG                *
*        Solutions for real time microcontroller applications        *
**********************************************************************
*                                                                    *
*        (c) 2003-2012     SEGGER Microcontroller GmbH & Co KG       *
*                                                                    *
*        Internet: www.segger.com    Support:  support@segger.com    *
*                                                                    *
**********************************************************************

**** emFile file system for embedded applications ****
emFile is protected by international copyright laws. Knowledge of the
source code may not be used to write a similar product. This file may
only be used in accordance with a license and should not be re-
distributed in any way. We appreciate your understanding and fairness.
----------------------------------------------------------------------
----------------------------------------------------------------------
File        : FS_Driver_TMPA910CRAXBG.c
Purpose     : SD card driver for the use with the Toshiba TMPA910.
---------------------------END-OF-HEADER------------------------------
*/

/*********************************************************************
*
*       #include Section
*
**********************************************************************
*/

#include "FS_ConfDefaults.h"        /* FS Configuration */

#include "FS_Int.h"
#include "FS_CLib.h"
#include "RTOS.h"

#define USE_OS    1     // 0: Polled mode, 1 means the task is suspended during DMA transfer, reducing CPU load and allowing the CPU to execute an other task


/*********************************************************************
*
*       #define constants
*
**********************************************************************
*/
#define SECTOR_SIZE           512
#define WAIT_POWER_UP_TIME    100
#define CPU_HCLK            96000UL   // 96 MHz is set as HCLK given in kHz

#ifndef NUM_UNITS
  #define NUM_UNITS         1
#endif


#define PID_SD              22
#define TRUE                 1
#define FALSE                0


#define SDD_STATUS_INACTIVE  0
#define SDD_STATUS_ACTIVE    1
#define E_SDD_SDMEM          1
#define E_SDD_OK             0
#define E_SDD_NO_CARD       -1
#define E_SDD_INACTIVE      -2
#define E_SDD_PARAMETER     -3
#define E_SDD_PROTECT       -5
#define E_SDD_DEVICE_TYPE   -7
#define E_SDD_ACTIVE        -8
#define E_SDD_SD_VER        -9    /* 070904 */

#define E_SDD_CMD_OK         0     /* internal only */
#define E_SDD_CMD_ACCEPT     0 /* internal only */
#define E_SDD_CMD_ERR      -11
#define E_SDD_CMD_TIMEOUT  -13
#define E_SDD_CMD_CRC_ERR  -14
#define E_SDD_CMD_WR_ERR   -15
#define E_SDD_CMD_IDLE     -16 /* internal only */
#define E_SDD_CMD_CARD_ERR -20

#define SDD_CRC_ENABLE  1
#define SDD_CRC_DISABLE 0

/*********************************************************************
*
*       #define constants
*
**********************************************************************
*/

#define GPIO_G_BASE       (0xF0806000)
#define GPIOGFR1          *((volatile U32 *)(GPIO_G_BASE+0x424))

#define SDHC_BASE           (0xF2030000)
#define SD_CMD            *((volatile U16 *)(SDHC_BASE+0x0000))
#define SD_PORTSEL        *((volatile U16 *)(SDHC_BASE+0x0004))
#define SD_ARG0           *((volatile U16 *)(SDHC_BASE+0x0008))
#define SD_ARG1           *((volatile U16 *)(SDHC_BASE+0x000c))
#define SD_STOP           *((volatile U16 *)(SDHC_BASE+0x0010))
#define SD_SECCNT         *((volatile U16 *)(SDHC_BASE+0x0014))
#define SD_RSP0           *((volatile U16 *)(SDHC_BASE+0x0018))
#define SD_RSP1           *((volatile U16 *)(SDHC_BASE+0x001c))
#define SD_RSP2           *((volatile U16 *)(SDHC_BASE+0x0020))
#define SD_RSP3           *((volatile U16 *)(SDHC_BASE+0x0024))
#define SD_RSP4           *((volatile U16 *)(SDHC_BASE+0x0028))
#define SD_RSP5           *((volatile U16 *)(SDHC_BASE+0x002c))
#define SD_RSP6           *((volatile U16 *)(SDHC_BASE+0x0030))
#define SD_RSP7           *((volatile U16 *)(SDHC_BASE+0x0034))
#define SD_INFO1          *((volatile U16 *)(SDHC_BASE+0x0038))
#define SD_INFO2          *((volatile U16 *)(SDHC_BASE+0x003c))
#define SD_INFO1_MASK     *((volatile U16 *)(SDHC_BASE+0x0040))
#define SD_INFO2_MASK     *((volatile U16 *)(SDHC_BASE+0x0044))
#define SD_CLK_CTRL       *((volatile U16 *)(SDHC_BASE+0x0048))
#define SD_SIZE           *((volatile U16 *)(SDHC_BASE+0x004c))
#define SD_OPTION         *((volatile U16 *)(SDHC_BASE+0x0050))
#define SD_ERR_STS1       *((volatile U16 *)(SDHC_BASE+0x0058))
#define SD_ERR_STS2       *((volatile U16 *)(SDHC_BASE+0x005c))
#define SD_BUF            *((volatile U16 *)(SDHC_BASE+0x0060))
#define SDIO_MODE         *((volatile U16 *)(SDHC_BASE+0x0068))
#define SDIO_INFO         *((volatile U16 *)(SDHC_BASE+0x006c))
#define SDIO_INFO1_MASK   *((volatile U16 *)(SDHC_BASE+0x0070))
#define CC_STATUS         *((volatile U16 *)(SDHC_BASE+0x0080))
#define CC_STS_MASK       *((volatile U16 *)(SDHC_BASE+0x0084))
#define CC_COLUMN         *((volatile U16 *)(SDHC_BASE+0x0088))
#define CC_MKB            *((volatile U16 *)(SDHC_BASE+0x008c))
#define CC_CDATA0         *((volatile U16 *)(SDHC_BASE+0x00a0))
#define CC_CDATA1         *((volatile U16 *)(SDHC_BASE+0x00a4))
#define CC_CDATA2         *((volatile U16 *)(SDHC_BASE+0x00a8))
#define CC_CDATA3         *((volatile U16 *)(SDHC_BASE+0x00ac))
#define CC_OSR0           *((volatile U16 *)(SDHC_BASE+0x00b0))
#define CC_OSR1           *((volatile U16 *)(SDHC_BASE+0x00b4))
#define CC_OSR2           *((volatile U16 *)(SDHC_BASE+0x00b8))
#define CC_OSR3           *((volatile U16 *)(SDHC_BASE+0x00bc))
#define CC_SUI0           *((volatile U16 *)(SDHC_BASE+0x00c0))
#define CC_SUI1           *((volatile U16 *)(SDHC_BASE+0x00c4))
#define CC_SUI2           *((volatile U16 *)(SDHC_BASE+0x00c8))
#define CC_SUI3           *((volatile U16 *)(SDHC_BASE+0x00cc))
#define CC_SDKEY0         *((volatile U16 *)(SDHC_BASE+0x0100))
#define CC_SDKEY1         *((volatile U16 *)(SDHC_BASE+0x0104))
#define CC_SDKEY2         *((volatile U16 *)(SDHC_BASE+0x0108))
#define CC_SDKEY3         *((volatile U16 *)(SDHC_BASE+0x010c))
#define CC_IDAT           *((volatile U16 *)(SDHC_BASE+0x0120))
#define CC_ODAT           *((volatile U16 *)(SDHC_BASE+0x0140))
#define CC_O2DAT          *((volatile U16 *)(SDHC_BASE+0x0148))
#define CC_VAR_LF1        *((volatile U16 *)(SDHC_BASE+0x0154))
#define CC_VAR_LF0        *((volatile U16 *)(SDHC_BASE+0x0158))
#define CC_VAR_LH         *((volatile U16 *)(SDHC_BASE+0x015c))
#define CC_INFO           *((volatile U16 *)(SDHC_BASE+0x0160))
#define CC_INFO_MASK      *((volatile U16 *)(SDHC_BASE+0x0164))
#define CC_SDSD           *((volatile U16 *)(SDHC_BASE+0x0168))
#define CC_SWITCH         *((volatile U16 *)(SDHC_BASE+0x016c))
#define CC_CLKDIV         *((volatile U16 *)(SDHC_BASE+0x0170))
#define CC_AC_CLK_CTRL    *((volatile U16 *)(SDHC_BASE+0x0174))
#define CC_CON_CTRL       *((volatile U16 *)(SDHC_BASE+0x0178))
#define CC_ADDR_CTRL      *((volatile U16 *)(SDHC_BASE+0x017c))
#define CC_IBUF_CADDR     *((volatile U16 *)(SDHC_BASE+0x0190))
#define CC_OBUF_CADDR     *((volatile U16 *)(SDHC_BASE+0x01a4))
#define CC_IBUF_HADDR     *((volatile U16 *)(SDHC_BASE+0x01a8))
#define CC_OBUF_HADDR     *((volatile U16 *)(SDHC_BASE+0x01ac))
#define CC_EXT_MODE       *((volatile U16 *)(SDHC_BASE+0x01b0))
#define CC_EDKSET         *((volatile U16 *)(SDHC_BASE+0x01b4))
#define SOFT_RESET        *((volatile U16 *)(SDHC_BASE+0x01c0))
//#define VERSION           *((volatile U16 *)(SDHC_BASE+0x01c4))
#define CC_PROC           *((volatile U16 *)(SDHC_BASE+0x01d4))
#define CC_SSEC           *((volatile U16 *)(SDHC_BASE+0x01d8))
#define CC_SECCNT         *((volatile U16 *)(SDHC_BASE+0x01dc))
#define SD_PWR            *((volatile U16 *)(SDHC_BASE+0x01e4))
#define EXT_SDIO          *((volatile U16 *)(SDHC_BASE+0x01e8))
#define EXT_WP            *((volatile U16 *)(SDHC_BASE+0x01ec))
#define EXT_CD            *((volatile U16 *)(SDHC_BASE+0x01f0))
#define EXT_CD_DAT3       *((volatile U16 *)(SDHC_BASE+0x01f4))
#define EXT_CD_MASK       *((volatile U16 *)(SDHC_BASE+0x01f8))
#define EXT_CD_DAT3_MASK  *((volatile U16 *)(SDHC_BASE+0x01fc))

#define BMSK1             (0x0200)
#define BMSK0             (0x0100)
#define OCR_HCS           (0x40000000)
#define MASK_UPPER_16BIT  (0x0000ffff)
#define MASK_UPPER_24BIT  (0x000000ff)
#define CBSY              (0x00004000)
#define INFO0             (0x00000001)
#define INFO2             (0x00000004)

#define RW_COMPLETE       (0x0004)
/* SD_INFO2 bit */
#define BWE (0x0200)
#define BRE (0x0100)

#define EVENT_BRE         (0x01)
#define EVENT_BWE         (0x02)
#define EVENT_RW_COMPLETE (0x04)

#define CMD0  0
#define CMD1  1
#define CMD2  2
#define CMD3  3
#define CMD5  5
#define CMD6  0x1c06 /* CMD6 is not supported as automatic sequence */
#define CMD7  7
#define CMD9  9
#define CMD8  8
#define CMD10 10
#define CMD13 13
#define CMD16 16
#define CMD17_READ_SINGLE_BLOCK 17
#define CMD18 18
#define CMD24 24
#define CMD25 25
#define CMD52 52
#define CMD55 55
#define CMD58 58
#define CMD59 59
#define ACMD6   (6 | 0x40)
#define ACMD41  (41| 0x40)
#define ACMD51  (51| 0x40)

#define TIME_NCR        10000   /* �b��l */
#define TIME_WR_BUSY  ((U32)WR_BUSY_MAX * 1000 * 1000 / CFG_SCLK_NSTIME / 8)
#define TIME_RD_WAIT  ((U32)RD_WAIT_MAX * 1000 * 1000 / CFG_SCLK_NSTIME / 8)

#define CSD_SIZE  17
#define CID_SIZE  16
#define OCR_SIZE  4

#define SDD_SCLK_NSTIME         800 /* 800ns(=1.25MHz) */
#define RETRY_MAX                 3 /* retry count for CRC error */

/*********************************************************************
*
*       Type definitions
*
**********************************************************************
*/

typedef struct {           /* CSD register structure */
  U8 Dummy;
  U8 aData[16];            /* Size is 128 bit */
} CSD;

typedef struct {
  U8    IsInited;
  U8    Unit;
  U8    IsActive;
  U32   NumSectors;
  U16   aCmdResponseBuf[8];
  U32   OCRReg;
  U16   RCA;
  CSD   CSDreg;
  U32   SDVer;
  U8    IsSDHCCard;
} DRIVER_INST;

/*********************************************************************
*
*       CSD register access macros
*/
#define CSD_STRUCTURE(pCSD)            _GetFromCSD(pCSD, 126, 127)
#define CSD_WRITE_PROTECT(pCSD)        _GetFromCSD(pCSD, 12, 13)
#define CSD_FILE_FORMAT_GRP(pCSD)      _GetFromCSD(pCSD, 15, 15)
#define CSD_WRITE_BL_LEN(pCSD)         _GetFromCSD(pCSD, 22, 25)
#define CSD_R2W_FACTOR(pCSD)           _GetFromCSD(pCSD, 26, 28)
#define CSD_C_SIZE_MULT(pCSD)          _GetFromCSD(pCSD, 47, 49)
#define CSD_C_SIZE(pCSD)               _GetFromCSD(pCSD, 62, 73)
#define CSD_READ_BL_LEN(pCSD)          _GetFromCSD(pCSD, 80, 83)
#define CSD_TRAN_SPEED(pCSD)           (pCSD->aData[3])   // Same as, but more efficient than: _GetFromCSD(pCSD,  96, 103)
#define CSD_NSAC(pCSD)                 (pCSD->aData[2])   // Same as, but more efficient than: _GetFromCSD(pCSD, 104, 111)
#define CSD_TAAC(pCSD)                 (pCSD->aData[1])   // Same as, but more efficient than: _GetFromCSD(pCSD, 112, 119)
#define CSD_C_SIZE_V2(pCSD)            _GetFromCSD(pCSD,  48, 69)


/*********************************************************************
*
*       Static const
*
**********************************************************************
*/
static const U8 _aFactor[16] = {
  0,    /* 0: reserved - not supported */
  10,   /* 1 */
  12,   /* 2 */
  13,   /* 3 */
  15,   /* 4 */
  20,   /* 5 */
  25,   /* 6 */
  30,   /* 7 */
  35,   /* 8 */
  40,   /* 9 */
  45,   /* 10 */
  50,   /* 11 */
  55,   /* 12 */
  60,   /* 13 */
  65,   /* 14 */
  80    /* 15 */
};

static const U32 _aUnit[8] = {
  10000000UL,
  1000000UL,
  100000UL,
  10000UL,
  1000UL,
  100UL,
  10UL,
  1UL,
};


/*********************************************************************
*
*       Static data
*
**********************************************************************
*/
static DRIVER_INST  * _apInst[NUM_UNITS];
static int            _NumUnits;
static U16            _aSDBuffer[256];                    /* 071002 test for CMD6 response  */
#if USE_OS
  static OS_TASK      * _pTask;
#else
  static volatile char _Event;
#endif

/*********************************************************************
*
*       Static forward declaration
*
**********************************************************************
*/
static void _SetMaxSpeed      (U16 Freq);
static int  _IsCardPresent    (void);
static int  _IsCardWrProtected(void);

/*********************************************************************
*
*       Static code
*
**********************************************************************
*/

/*********************************************************************
*
*       _Wait
*
*  Function description:
*    
*
*/
static void _Wait(int Cnt) {
  volatile int i;

  for (i = 0; i < Cnt; i++) {
    __asm("nop");
  }
}

/*********************************************************************
*
*       _Wait4Powerup
*
*  Function description:
*    Waits for 
*
*/
static void _Wait4Powerup(void) {
  int i = WAIT_POWER_UP_TIME;

  while (i > 0) {
    i--;
  }
}

/*********************************************************************
*
*       _WaitEvent
*
*  Function description:
*    
*
*  Parameters:
*    EventMask    - 
*  
*  Return value:
*    char       - 
*
*/
static char _WaitEvent(char EventMask) {
#if USE_OS
  char EventOccured;
  _pTask = OS_GetpCurrentTask();
  EventOccured = OS_WaitEventTimed(EventMask, 1000);
  if (EventOccured & EventMask) {    
    return 1;
  }
  return 0;
#else
  U32 TimeOut = 0;
  while (1) {
    if ((_Event & EventMask) == EventMask) {
      _Event &= ~EventMask;
      return 1;
    }
    if (TimeOut++ > 0x100000) {
      break;
    }
  }
  return 0;
#endif
}

/*********************************************************************
*
*       _Wait4End
*
*/
static void _Wait4End(void) {
  _WaitEvent(EVENT_RW_COMPLETE);
}

static void _Wait4BRE(void) {
  _WaitEvent(EVENT_BRE);
}

static void _Wait4BWE(void) {
  _WaitEvent(EVENT_BWE);
}

/*********************************************************************
*
*       _IsrHandler
*
*  Function description:
*    
*/
static void _IsrHandler(void) {
  U16  SDInfo1, SDInfo2;
  char Event = 0;

  SDInfo2 = SD_INFO2;
  SDInfo1 = SD_INFO1;
  /* Check read enable flag (BRE) */
  if (SDInfo2 & BRE) {
    Event |= EVENT_BRE;
  }
  /* Check write enable flag (BWE)  */
  if (SDInfo2 & BWE) {
    Event |= EVENT_BWE;
  }
  /* Check write enable flag (BWE)  */
  if (SDInfo1 & RW_COMPLETE) {
    Event |= EVENT_RW_COMPLETE;
  }
#if (USE_OS)
  OS_SignalEvent(Event, _pTask);
#else 
  _Event = Event;
#endif
  SD_INFO2 = 0x0000;
  SD_INFO1 &= ~RW_COMPLETE;
}


/*********************************************************************
*
*       _GetFromCSD
*
*  Function description
*    Returns a value from the CSD field. These values are stored in
*    a 128 bit array; the bit-indices documented in [1]: 5.3 CSD register, page 69
*    can be used as parameters when calling the function
*/
static unsigned _GetFromCSD(const CSD * pCSD, unsigned FirstBit, unsigned LastBit) {
  unsigned Off;
  unsigned OffLast;
  unsigned NumBytes;
  U32 Data;

  Off      = FirstBit / 8;
  OffLast  = LastBit / 8;
  NumBytes = OffLast - Off + 1;
  Off      = 15 - OffLast;                      // Bytes are reversed in CSD
  Data = 0;
  //
  // Read data into 32 bits
  //
  do {
    Data <<= 8;
    Data |= pCSD->aData[Off++];
  } while (--NumBytes);
  //
  // Shift and mask result
  //
  Data >>= (FirstBit & 7);
  Data &= (2 << (LastBit - FirstBit)) - 1;                // Mask out bits that are outside of given bit range
  return Data;
}

/*********************************************************************
*
*       _WaitResponse
*
*  Function description:
*    
*
*  Parameters:
*    TimeOut    - 
*  
*  Return value:
*    int       - 
*
*/
static int _WaitResponse(U32 TimeOut) {
  U32 i;
  U16 RegData;

  for (i = 0; i < TimeOut; i++) {
    RegData = SD_INFO1;
    RegData &= INFO0;
    if (RegData == INFO0) {
      RegData = SD_INFO1;
      SD_INFO1 = RegData & (U16) (~INFO0);
      RegData = SD_INFO2; /* 071002 test for CMD6 response  */
      return E_SDD_CMD_OK;
    }
  }
  return E_SDD_CMD_TIMEOUT;
}

/*********************************************************************
*
*       _WriteCmd
*
*  Function description:
*    
*
*  Parameters:
*    idx    - 
*    arg    - 
*  
*  Return value:
*    int       - 
*
*/
static int _WriteCmd(DRIVER_INST * pInst, U16 CommandIndex, U32 Arg) {
  volatile U16 RegData;

  SD_INFO1 = 0x0000;
  SD_INFO2 = 0x0000;
  /* Waits for end of command transaction */
  do {
    RegData = SD_INFO2;
    RegData &= 0x407f;
  } while (RegData != 0x0000);
  SD_ARG0 = (U16)(Arg & MASK_UPPER_16BIT);
  SD_ARG1 = (U16)(Arg >> 16);
  SD_CMD = CommandIndex;
  return E_SDD_CMD_OK;
}

/*********************************************************************
*
*       _ReadCmdResponse
*
*  Function description:
*    
*
*  Parameters:
*        - 
*  
*  Return value:
*    int       - 
*
*/
static int _ReadCmdResponse(DRIVER_INST * pInst) {
  int r;

  r = _WaitResponse(TIME_NCR);
  if (r < 0) {
    return E_SDD_CMD_TIMEOUT;
  }
  /* Reads all response */
  pInst->aCmdResponseBuf[0] = SD_RSP0;
  pInst->aCmdResponseBuf[1] = SD_RSP1;
  pInst->aCmdResponseBuf[2] = SD_RSP2;
  pInst->aCmdResponseBuf[3] = SD_RSP3;
  pInst->aCmdResponseBuf[4] = SD_RSP4;
  pInst->aCmdResponseBuf[5] = SD_RSP5;
  pInst->aCmdResponseBuf[6] = SD_RSP6;
  pInst->aCmdResponseBuf[7] = SD_RSP7;
  return E_SDD_CMD_OK;
}

/*********************************************************************
*
*       _ResetSDInfoRegs
*
*  Function description:
*    Set registers for single read
*
*/
static void _ResetSDInfoRegs(void) {
  /* Clear information registers  */
  SD_INFO1    = 0x0000;
  SD_INFO2    = 0x0000;
  EXT_CD      = 0x0000;
  EXT_CD_DAT3 = 0x0000;
  /* Set control registers  */
  SD_SIZE     = SECTOR_SIZE;
  SD_STOP     = 0;
  SD_SECCNT   = 0;
}

/*********************************************************************
*
*       _SendCmdGoIdleState
*
*  Function description:
*    
*
*  Parameters:
*        - 
*  
*  Return value:
*    int       - 
*
*/
static int _SendCmdGoIdleState(DRIVER_INST * pInst) {
  int r;

  r = _WriteCmd(pInst, CMD0, 0);
  pInst->RCA = 0;
  return r;
}

/*********************************************************************
*
*       _SendCmdAllSendCID
*
*  Function description:
*    
*
*  Parameters:
*        - 
*  
*  Return value:
*    int       - 
*
*/
static int _SendCmdAllSendCID(DRIVER_INST * pInst) {
  int r;

  _WriteCmd(pInst, CMD2, 0);
  r = _ReadCmdResponse(pInst);
  if (r < 0) {
    return E_SDD_CMD_TIMEOUT;
  }
  return E_SDD_CMD_OK;
}

/*********************************************************************
*
*       _SendCmdSendRelativeAddr
*
*  Function description:
*    
*
*  Parameters:
*        - 
*  
*  Return value:
*    int       - 
*
*/
static int _SendCmdSendRelativeAddr(DRIVER_INST * pInst) {
  int r;

  _WriteCmd(pInst, CMD3, 0);
  r = _ReadCmdResponse(pInst);
  if (r < 0) {
    return E_SDD_CMD_TIMEOUT;
  }
  pInst->RCA = pInst->aCmdResponseBuf[1];
  return E_SDD_CMD_OK;
}

/*********************************************************************
*
*       _SendCmdSwitchFunc
*
*  Function description:
*    
*
*  Parameters:
*        - 
*  
*  Return value:
*    int       - 
*
*/
static int _SendCmdSwitchFunc(DRIVER_INST * pInst, U32 Arg) {
  int      r;
  
  _ResetSDInfoRegs();
  SD_SIZE = 64;
  _WriteCmd(pInst, CMD6, Arg);
  r = _ReadCmdResponse(pInst);
  _Wait4BRE();
  SD_SIZE = SECTOR_SIZE;
  if (r < 0) {
    return E_SDD_CMD_TIMEOUT;
  }
  return E_SDD_CMD_OK;
}

/*********************************************************************
*
*       _SendCmdSelectCard
*
*  Function description:
*    
*
*  Parameters:
*        - 
*  
*  Return value:
*    int       - 
*
*/
static int _SendCmdSelectCard(DRIVER_INST * pInst) {
  int r;

  _WriteCmd(pInst, CMD7, pInst->RCA << 16);
  r = _ReadCmdResponse(pInst);
  if (r < 0) {
    return E_SDD_CMD_TIMEOUT;
  }
  return E_SDD_CMD_OK;
}

/*********************************************************************
*
*       _SendCmdSendIFCond
*
*  Function description:
*    
*
*  Parameters:
*    arg    - 
*  
*  Return value:
*    int       - 
*
*/
static int _SendCmdSendIFCond(DRIVER_INST * pInst, U32 arg) {
  int r;

  _WriteCmd(pInst, CMD8, arg);
  r = _ReadCmdResponse(pInst);
  if (r < 0) {
    return E_SDD_CMD_TIMEOUT;
  }
  return E_SDD_CMD_OK;
}

/*********************************************************************
*
*       _SendCmdSendCSD
*
*  Function description:
*    
*
*  Parameters:
*    csd    - 
*  
*  Return value:
*    int       - 
*
*/
static int _SendCmdSendCSD(DRIVER_INST * pInst, U8 * pCSD) {
  int r;
  int i;
  
  _WriteCmd(pInst, CMD9, pInst->RCA << 16);
  r = _ReadCmdResponse(pInst);
  if (r < 0) {
    return E_SDD_CMD_TIMEOUT;
  }
  for (i = 0; i < CSD_SIZE >> 1; i++) {
    U16 Data;
    Data = pInst->aCmdResponseBuf[7 - i];
    *pCSD++ = Data >> 8;
    *pCSD++ = Data & 0xff;
  }
  return E_SDD_CMD_OK;
}

/*********************************************************************
*
*       _SendCmdSendStatus
*
*  Function description:
*    
*
*  Parameters:
*        - 
*  
*  Return value:
*    int       - 
*
*/
static int _SendCmdSendStatus(DRIVER_INST * pInst) {
  int r;

  _WriteCmd(pInst, CMD13, pInst->RCA << 16);
  r = _ReadCmdResponse(pInst);
  if (r < 0) {
    return E_SDD_CMD_TIMEOUT;
  }
  return E_SDD_CMD_OK;
}

/*********************************************************************
*
*       _SendCmdSetBlockLen
*
*  Function description:
*    
*
*  Parameters:
*        - 
*  
*  Return value:
*    int       - 
*
*/
static int _SendCmdSetBlockLen(DRIVER_INST * pInst) {
  int r;

  _WriteCmd(pInst, CMD16, SECTOR_SIZE);
  r = _ReadCmdResponse(pInst);
  if (r < 0) {
    return E_SDD_CMD_TIMEOUT;
  }
  return E_SDD_CMD_OK;
}

/*********************************************************************
*
*       _SendCmdReadSingleBlock
*
*  Function description:
*    
*
*  Parameters:
*    addr    - 
*    RegData    - 
*  
*  Return value:
*    int       - 
*
*/
static int _SendCmdReadSingleBlock(DRIVER_INST * pInst, U32 Addr, U16 * pData) {
  int r;
  U32 i;

  _ResetSDInfoRegs();
  _WriteCmd(pInst, CMD17_READ_SINGLE_BLOCK, Addr);
  r = _ReadCmdResponse(pInst);
  if (r < 0) {
    return E_SDD_CMD_TIMEOUT;
  }
  /* Check read enable  */
  _Wait4BRE();
  /* Read data  */
  for (i = 0; i < (SECTOR_SIZE / sizeof(U16)); i++) {
    *pData++ = SD_BUF;
  }
  /* Check end of all data response */
  _Wait4End();
  SD_INFO1 = 0x0000;
  return E_SDD_CMD_OK;
}

/*********************************************************************
*
*       _SendCmdReadSingleBlock
*
*  Function description:
*    
*
*  Parameters:
*    addr    - 
*    RegData    - 
*  
*  Return value:
*    int       - 
*
*/
static int _SendCmdReadMultipleBlock(DRIVER_INST * pInst, U32 Addr, U16 * pData, U32 NumSectors) {
  int r;
  U32 i;

  _ResetSDInfoRegs();
  SD_SECCNT = NumSectors;
  SD_STOP   = (1 << 8);
  _WriteCmd(pInst, CMD18, Addr);
  r = _ReadCmdResponse(pInst);
  if (r < 0) {
    return E_SDD_CMD_TIMEOUT;
  }
  /* Check read enable  */
  do {
    _Wait4BRE();
    //
    // Read data
    //
    for (i = 0; i < (SECTOR_SIZE / sizeof(U16)); i++) {
      *pData++ = SD_BUF;
    }
  } while (--NumSectors);
  /* Check end of all data response */
  _Wait4End();
  SD_INFO1 = 0x0000;
  return E_SDD_CMD_OK;
}


/*********************************************************************
*
*       _SendCmdWriteBlock
*
*  Function description:
*    
*
*  Parameters:
*    addr    - 
*    RegData    - 
*  
*  Return value:
*    int       - 
*
*/
static int _SendCmdWriteBlock(DRIVER_INST * pInst, U32 Addr, const U16 * pData) {
  int r;
//  volatile U16 SDInfoRegData;
  U32 i;

  _ResetSDInfoRegs();
  _WriteCmd(pInst, CMD24, Addr);
  r = _ReadCmdResponse(pInst);
  if (r < 0) {
    return E_SDD_CMD_TIMEOUT;
  }
  /* Check write enable */
  _Wait4BWE();
  /* Write data */
  for (i = 0; i < (SECTOR_SIZE / sizeof(U16)); i++) {
    SD_BUF = *pData++;
  }
  /* Check end of all data response */
  _Wait4End();
  return E_SDD_CMD_OK;
}

/*********************************************************************
*
*       _SendCmdMultipleWriteBlock
*
*  Function description:
*    
*
*  Parameters:
*    addr    - 
*    RegData    - 
*  
*  Return value:
*    int       - 
*
*/
static int _SendCmdMultipleWriteBlock(DRIVER_INST * pInst, U32 Addr, const U16 * pData, U32 NumSectors) {
  int r;
  U32 i;

  _ResetSDInfoRegs();
  SD_SECCNT = NumSectors;
  SD_STOP   = (1 << 8);
  _WriteCmd(pInst, CMD25, Addr);
  r = _ReadCmdResponse(pInst);
  if (r < 0) {
    return E_SDD_CMD_TIMEOUT;
  }
  do {
    /* Check write enable */
    _Wait4BWE();
    /* Write data */
    for (i = 0; i < (SECTOR_SIZE / sizeof(U16)); i++) {
      SD_BUF = *pData++;
    }
  } while (--NumSectors);
  /* Check end of all data response */
  _Wait(50);
  _Wait4End();
  return E_SDD_CMD_OK;
}


/*********************************************************************
*
*       _SendCmdAPPCmd
*
*  Function description:
*    
*
*  Parameters:
*    arg    - 
*  
*  Return value:
*    int       - 
*
*/
static int _SendCmdAPPCmd(DRIVER_INST * pInst, U32 Arg) {
  int r;

  //
  // 
  _WriteCmd(pInst, CMD55, Arg);
  r = _ReadCmdResponse(pInst);
  return r;
}

/*********************************************************************
*
*       _SendACmdSetBusWidth
*
*  Function description:
*    
*
*  Parameters:
*        - 
*  
*  Return value:
*    int       - 
*
*/
static int _SendACmdSetBusWidth(DRIVER_INST * pInst) {
  int r;

  r = _SendCmdAPPCmd(pInst, pInst->RCA << 16);
  if (r < 0) {
    return r;                        /* time out */
  }
  _WriteCmd(pInst, ACMD6, 0x00000002);
  r = _ReadCmdResponse(pInst);
  if (r < 0) {
    return E_SDD_CMD_TIMEOUT;
  }
  SD_OPTION = 0x40ee;         /* Bus width = 4bit   */
  return E_SDD_CMD_OK;        /* A910  */
}

/*********************************************************************
*
*       _SendACmdSendOPCode
*
*  Function description:
*    
*
*  Parameters:
*        - 
*  
*  Return value:
*    int       - 
*
*/
static int _SendACmdSendOPCode(DRIVER_INST * pInst) {
  int r;

  r = _SendCmdAPPCmd(pInst, 0x00000000);
  if (r < 0) {
    return r; /* time out */
  }
  _WriteCmd(pInst, ACMD41, pInst->OCRReg | OCR_HCS);
  r = _ReadCmdResponse(pInst);
  pInst->OCRReg = pInst->aCmdResponseBuf[0] + (pInst->aCmdResponseBuf[1] << 16);
  if (r < 0) {
    return E_SDD_CMD_TIMEOUT;
  }
  return E_SDD_CMD_OK; /* A910  */
}

/*********************************************************************
*
*       _SendACmdSendSCR
*
*  Function description:
*    
*
*  Parameters:
*        - 
*  
*  Return value:
*    int       - 
*
*/
static int _SendACmdSendSCR(DRIVER_INST * pInst) {
  int r;
  U16 aBuffer[4];

  r = _SendCmdAPPCmd(pInst, pInst->RCA << 16);
  if (r < 0) {
    return r; /* time out */
  }
  _ResetSDInfoRegs(); /* Read setting */
  SD_SIZE = 8;
  _WriteCmd(pInst, ACMD51, 0x00000000);
  OS_IncDI();
  r = _ReadCmdResponse(pInst);
  /* Check read enable  */
  _Wait4BRE();
  OS_DecRI();
  /* Read data  */
  aBuffer[0] = SD_BUF; /* Read [15:0] */
  aBuffer[1] = SD_BUF; /* Read [31:16]  */
  aBuffer[2] = SD_BUF; /* Read [47:32]  */
  aBuffer[3] = SD_BUF; /* Read [63:48]  */
  pInst->SDVer = aBuffer[0] & 0x000f;
  /* Check end of all data response */
  _Wait4End();
  SD_SIZE = SECTOR_SIZE;
  if (r < 0) {
    return E_SDD_CMD_TIMEOUT;
  }
  return E_SDD_CMD_OK;
}

/*********************************************************************
*
*      _ApplyCSD
*
*  Description:
*    MMC/SD driver internal function.
*    Read the card's CSD (card specific data) registers and check
*    its contents.
*
*  Parameters:
*    Unit      - Device index number
*
*  Return value:
*    ==0           - CSD has been read and all parameters are valid.
*    <0            - An error has occurred.
*
*  Notes
*    (1) SectorSize
*        Newer SD card (4 GByte-card) return a block size larger than 512.
*        Sector Size used however is always 512 bytes.
*/
static int _ApplyCSD(DRIVER_INST * pInst, const CSD * pCSD) {
  U32      Factor;
  U32      CardSize;
  int      tmp;
  U32      Freq;
  unsigned c;
  unsigned CSDVersion;
  unsigned ccs;

  ccs = pInst->OCRReg & OCR_HCS;
  //
  // Get the CSD version of the SD card.
  // 
  CSDVersion = CSD_STRUCTURE(pCSD);
  /* Card parameter interpretation */
  if (CSDVersion == 0) {
    //
    //  Calc number of sectors available on the medium
    //
    Factor     = (U16)(1 << CSD_READ_BL_LEN(pCSD)) / SECTOR_SIZE;
    Factor     *= 1 << (CSD_C_SIZE_MULT(pCSD) + 2);
    CardSize   = CSD_C_SIZE(pCSD) + 1;
    CardSize   *= Factor;
    //
    //  Store calculated values into medium's instance structure
    //
    pInst->NumSectors       = CardSize;
  } else if (CSDVersion == 1){  // Newer SD V2 cards.
    //
    //  Calculate number of sectors available on the medium
    //
    CardSize   = CSD_C_SIZE_V2(pCSD) << 10;
    //
    //  Store calculated values into medium's instance structure
    //
    pInst->NumSectors       = CardSize;
    if (ccs) {
      pInst->IsSDHCCard         = 1;
    }
  } else {
    FS_DEBUG_ERROROUT((FS_MTYPE_DRIVER,  "MMC: _ApplyCSD: Unsupported CSD version.\n"));
    return 1;
  }
  /* Calculate maximum communication speed according to card specification */
  tmp = CSD_TRAN_SPEED(pCSD);
  c = 6 - (tmp & 0x03);
  Freq = _aUnit[c];
  c = ((tmp & 0x78) >> 3); /* filter frequency bits */
  Freq *= _aFactor[c];
  _SetMaxSpeed((U16)Freq);

  return 0;  
}


/*********************************************************************
*
*       SDMemCardInit
*
*  Function description:
*    
*
*  Parameters:
*        - 
*  
*  Return value:
*    int       - 
*
*/
static int SDMemCardInit(DRIVER_INST * pInst) {
  int r;

  r = _SendCmdGoIdleState(pInst);
  if (r < 0) {
    r = E_SDD_NO_CARD;
    goto OnError;
  }
  /* SDHC */
  r = _SendCmdSendIFCond(pInst, 0x000001aa);
  OS_Delay(10);
  do {
    r = _SendACmdSendOPCode(pInst);
    if (r == E_SDD_CMD_TIMEOUT) {
      /* Multimedia Card Check (not support now) */
      return E_SDD_DEVICE_TYPE;
    } else if (r != E_SDD_CMD_OK) {
      r = E_SDD_NO_CARD;
      goto OnError;
    }
    OS_Delay(50);
  } while ((pInst->aCmdResponseBuf[1] & 0x8000) != 0x8000);
  /* CMD2 */
  r = _SendCmdAllSendCID(pInst);
  if (r < 0) {
    goto OnError;
  }
  /* CMD3 */
  r = _SendCmdSendRelativeAddr(pInst);
  if (r < 0) {
    goto OnError;
  }
  r = _SendCmdSendCSD(pInst, (U8 *)&pInst->CSDreg);
  if (r < 0) {
    goto OnError;
  }
  /* CMD7 */
  r = _SendCmdSelectCard(pInst);
  if (r < 0) {
    goto OnError;
  }
  r = _SendCmdSetBlockLen(pInst);
  if (r < 0) {
    goto OnError;
  }
  /* ACMD6  */
  r = _SendACmdSetBusWidth(pInst);
  if (r < 0) {
    goto OnError;
  }
  /* number of sectors */
  _ApplyCSD(pInst, &pInst->CSDreg);
  pInst->IsActive = SDD_STATUS_ACTIVE;
  return E_SDD_SDMEM;
OnError:
  return r;
}

/*********************************************************************
*
*       _OpenCard
*
*  Function description:
*    
*
*  Parameters:
*        - 
*  
*  Return value:
*    int       - 
*
*/
static int _OpenCard(DRIVER_INST * pInst) {
  int r;

  if (pInst->IsActive == SDD_STATUS_ACTIVE) {
    return E_SDD_ACTIVE;
  }

  if (_IsCardPresent() == FALSE) {
    return E_SDD_NO_CARD;
  }
  _Wait4Powerup();
  /* Initialize SD memory card  */
  r = SDMemCardInit(pInst);
  if (r < 0) {
    return r;
  } else {
    pInst->IsActive = SDD_STATUS_ACTIVE;
    return E_SDD_SDMEM;
  }
}

/*********************************************************************
*
*       _SwitchToHighSpeed
*
*  Function description:
*    
*
*/
static int _SwitchToHighSpeed(DRIVER_INST * pInst) {
  I32 r;
  U16 buf_16_0;
  U16 buf_16_1;
  unsigned tmp;
  unsigned c;
  unsigned Freq;

  OS_Delay(10);
  r = _SendACmdSendSCR(pInst);
  if (r < 0) {
    return r;
  }
  if (pInst->SDVer != 0) {
    U32 i;

    r = _SendCmdSwitchFunc(pInst, 0x00000001);
    if (r < 0) {
      return r;
    }
    for (i = 0; i < 256; i++) {
      _aSDBuffer[i] = SD_BUF;
    }
    buf_16_0 = _aSDBuffer[6] & 0x0200;
    buf_16_1 = _aSDBuffer[8] & 0x0001;
    if ((buf_16_0 == 0) || (buf_16_1 == 0)) {
      return E_SDD_SD_VER;
    }
    r = _SendCmdSwitchFunc(pInst, 0x80000001);
    if (r < 0) {
      return r;
    }
    for (i = 0; i < 256; i++) {
      _aSDBuffer[i] = SD_BUF;
    }

    //
    // Calculate maximum communication speed according to card specification
    //
    tmp = CSD_TRAN_SPEED((&(pInst->CSDreg)));
    c = 6 - (tmp & 0x03);
    Freq = _aUnit[c];
    c = ((tmp & 0x78) >> 3); /* filter frequency bits */
    Freq *= _aFactor[c];
    _SetMaxSpeed((U16)Freq);
    /* Set SD_CLK as high speed mode  */
    SD_CLK_CTRL = 0x0000;
    _Wait(5);
    SD_CLK_CTRL = 0x0100;
    /* Wait for stable clock  */
    OS_Delay(10);
  } else {
    r = E_SDD_SD_VER;
  }
  return r;
}


/*********************************************************************
*
*       _Init
*
*  Description:
*  Resets/Initializes the device
*
* Parameters:
*   Unit        - Unit number.
*
*  Return value:
*   ==0         - Device has been reset and initialized.
*   < 0         - An error has occurred.
*/
static int _Init(DRIVER_INST * pInst) {
  int CardType;

  GPIOGFR1    = 0x000000ff;  // Set SD host controller ports
  //
  // Set SD host controller registers
  //
  SOFT_RESET    = 0x0000;    // Set Software Reset
  SOFT_RESET    = 0x0003;    // Set Software Reset
  SD_OPTION     = 0xc0ee;    // Bus width = 1bit
  SD_INFO1      = 0x0000;    // Clear status
  SD_INFO2      = 0x0000;    // Clear status
  SD_INFO1_MASK = 0xFFFB;    // Enable RW Complete
  SD_INFO2_MASK = 0xfcff;    // Enable BRE and BWE
  _SetMaxSpeed(400);
  SD_STOP       = 0x0000;
  pInst->IsActive = SDD_STATUS_INACTIVE;
  OS_IncDI();
  OS_ARM_InstallISRHandler(PID_SD, _IsrHandler);
  OS_ARM_ISRSetPrio(PID_SD, 1);
  OS_ARM_EnableISR(PID_SD);
  OS_DecRI();
  CardType = _OpenCard(pInst);
  if (CardType == E_SDD_SDMEM) {    
    _SwitchToHighSpeed(pInst);    // SD memory card was detected
    pInst->IsInited = 1;
    return 0;
  }
  return -1;
}

/*********************************************************************
*
*       _WriteSectors
*
*  Description:
*  
*
* Parameters:
*   Unit        - Unit number.
*
*  Return value:
*   ==0         - Device has been reset and initialized.
*   < 0         - An error has occurred.
*/
static int _WriteSectors(DRIVER_INST * pInst, U32 StartSector, U32 NumSectors, const U16 * pBuffer, U8  RepeatSame) {
  U32      Addr;
  int      r; 
  unsigned i, j;

  if (pInst->IsActive == SDD_STATUS_INACTIVE) {
    return E_SDD_INACTIVE;
  }

  if (_IsCardPresent() == FALSE) {
    return E_SDD_NO_CARD;
  }

  if (_IsCardWrProtected() == TRUE) {
    return E_SDD_PROTECT;
  }

  if ((pInst->NumSectors <= StartSector) || (pInst->NumSectors < StartSector + NumSectors) || (NumSectors == 0)) {
    return E_SDD_PARAMETER;
  }
  if ((NumSectors == 1) || (RepeatSame == 1)) {
    for (i = 0; i < NumSectors; i++) {
      Addr = StartSector;
      if (pInst->IsSDHCCard == 0) {
        Addr *= SECTOR_SIZE;
      }
      for (j = 0; j < RETRY_MAX; j++) {
        _Wait(1); /* for wait from last command */
        r = _SendCmdWriteBlock(pInst, Addr, pBuffer);
        if (r == E_SDD_CMD_OK) {
          break;
        } else if (r != E_SDD_CMD_CRC_ERR) {
          return r;
        }
      }
      if (j == RETRY_MAX) {
        return r;
      }
      _Wait(1);
      r = _SendCmdSendStatus(pInst);
      if (r < 0) {
        return r;
      }
      StartSector++;
      if (RepeatSame == 0) {
        pBuffer += (SECTOR_SIZE / sizeof(U16));
      }
    }
  } else {
    Addr = StartSector;
    if (pInst->IsSDHCCard == 0) {
      Addr *= SECTOR_SIZE;
    }
    for (j = 0; j < RETRY_MAX; j++) {
      _Wait(1); /* for wait from last command */
      r = _SendCmdMultipleWriteBlock(pInst, Addr, pBuffer, NumSectors);
      if (r == E_SDD_CMD_OK) {
        break;
      } else if (r != E_SDD_CMD_CRC_ERR) {
        return r;
      }
    }
    
    if (j == RETRY_MAX) {
      return r;
    }
    _Wait(1);
    r = _SendCmdSendStatus(pInst);
    if (r < 0) {
      return r;
    }
  }
  return E_SDD_OK;
}


/*********************************************************************
*
*       _ReadSectors
*
*  Function description:
*    
*
*  Parameters:
*    start    - 
*    count    - 
*    RegData    - 
*  
*  Return value:
*    int       - 
*
*/
static int _ReadSectors(DRIVER_INST * pInst, U32 StartSector, U32 NumSectors, U16 * pBuffer) {
  U32      Addr;
  int      r;
  unsigned j;

  if (pInst->IsActive == SDD_STATUS_INACTIVE) {
    return E_SDD_INACTIVE;
  }
  if (_IsCardPresent() == FALSE) {
    return E_SDD_NO_CARD;
  }
  if ((pInst->NumSectors <= StartSector) || (pInst->NumSectors < StartSector + NumSectors) || (NumSectors== 0)) {
    return E_SDD_PARAMETER;
  }
  Addr = StartSector;
  if (pInst->IsSDHCCard == 0) {
    Addr *= SECTOR_SIZE;
  }
  if (NumSectors == 1) {
    for (j = 0; j < RETRY_MAX; j++) {
      _Wait(1); /* for wait from last command */
      r = _SendCmdReadSingleBlock(pInst, Addr, pBuffer);
      if (r == E_SDD_CMD_OK) {
        break;
      } else if (r != E_SDD_CMD_CRC_ERR) {
        return r;
      }
    }
    if (j == RETRY_MAX) {
      return r;
    }
  } else {
    for (j = 0; j < RETRY_MAX; j++) {
      _Wait(1); /* for wait from last command */
      r = _SendCmdReadMultipleBlock(pInst, Addr, pBuffer, NumSectors);
      if (r == E_SDD_CMD_OK) {
        break;
      } else if (r != E_SDD_CMD_CRC_ERR) {
        return r;
      }
    }
    if (j == RETRY_MAX) {
      return r;
    }
  }
  return E_SDD_OK;
}



/*********************************************************************
*
*       _Unmount
*
*  Description:
*    Unmounts the volume.
*
*  Parameters:
*    Unit        - Unit number.
*
*/
static void _Unmount(U8 Unit) {
  if (_apInst[Unit]->IsInited == 1) {
    FS_MEMSET(_apInst[Unit], 0, sizeof(DRIVER_INST));
  }
}

/*********************************************************************
*
*       Public code (indirectly thru callback)
*
**********************************************************************
*/

/*********************************************************************
*
*       _GetStatus
*
*  Description:
*    FS driver function. Get status of the media.
*
*  Parameters:
*    Unit                  - Unit number.
*
*  Return value:
*    FS_MEDIA_STATE_UNKNOWN - if the state of the media is unknown.
*    FS_MEDIA_NOT_PRESENT   - if no card is present.
*    FS_MEDIA_IS_PRESENT    - if a card is present.
*/
static int _GetStatus(U8 Unit) {
  return FS_MEDIA_IS_PRESENT;
}

/*********************************************************************
*
*       _Read
*
*  Function Description
*    Driver callback function.
*    Reads one or more logical sectors from storage device.
*
*  Parameters:
*    Unit        - Unit number.
*    Sector      - Sector to be read from the device.
*    pBuffer     - Pointer to buffer for storing the data.
*    NumSectors  - Number of sectors to read
*
*  Return value:
*      0                       - Data successfully written.
*    !=0                       - An error has occurred.
*
*/
static int _Read(U8 Unit, U32 SectorNo, void * p, U32 NumSectors) {
  DRIVER_INST * pInst;
  int           r;
  
  pInst = _apInst[Unit];
  r = _ReadSectors(pInst, SectorNo, NumSectors, (U16 *)p);
  return r;
}

/*********************************************************************
*
*       _Write
*
*  Description:
*    Driver callback function.
*    Writes one or more logical sectors to storage device.
*
*  Parameters:
*    Unit        - Unit number.
*    SectorNo    - Sector number to be written to the device.
*    pBuffer     - Pointer to buffer containing the data to write
*    NumSectors  - Number of sectors to store.
*    RepeatSame  - Repeat the same data to sectors.
*
*  Return value:
*      0                       - Data successfully written.
*    !=0                       - An error has occurred.
*
*/
static int _Write(U8 Unit, U32 SectorNo, const void  * p, U32 NumSectors, U8  RepeatSame) {
  DRIVER_INST * pInst;
  int           r;
  
  pInst = _apInst[Unit];
  r = _WriteSectors(pInst, SectorNo, NumSectors, (const U16 *)p, RepeatSame);
  return r;
}

/*********************************************************************
*
*       _DevIoCtl
*
*  Description:
*    FS driver function. Execute device command.
*
*  Parameters:
*    Unit    - Unit number.
*    Cmd         - Command to be executed.
*    Aux         - Parameter depending on command.
*    pBuffer     - Pointer to a buffer used for the command.
*
*  Return value:
*    Command specific. In general a negative value means an error.
*/
static int _IoCtl(U8 Unit, I32 Cmd, I32 Aux, void *pBuffer) {
  DRIVER_INST * pInst;
  FS_DEV_INFO * pInfo;
  int           r;

  pInst = _apInst[Unit];
  r     = -1;
  FS_USE_PARA(Aux);
  switch (Cmd) {
  case FS_CMD_GET_DEVINFO:
    pInfo = (FS_DEV_INFO *)pBuffer;
    pInfo->NumHeads        = 0;                       /* heads */
    pInfo->SectorsPerTrack = 0;                       /* sec per track */
    pInfo->NumSectors      = pInst->NumSectors;       /* Number of sectors */
    pInfo->BytesPerSector  = SECTOR_SIZE;             /* Number of byte per sector*/
    r = 0;
    break;
  case FS_CMD_UNMOUNT:  
    //
    // (Optional)
    // Device shall be unmounted - sync all operations and mark it as unmounted
    //
    
    _Unmount(Unit);
    r = 0;
    break;
  case FS_CMD_UNMOUNT_FORCED:
    //
    // (Optional)
    // Device shall be unmounted - mark it as unmounted without syncing any pending operations 
    //
    
    // ToDo: Call the function
    break;
  case FS_CMD_SYNC:
    //
    // (Optional)
    // Sync/flush any pending operations 
    //

    // ToDo: Call the function
    break;
  default:
    break;
  }
  return r;
}

/*********************************************************************
*
*       _InitMedium
*
*  Description:
*    Initialize the specified medium.
*
*  Parameters:
*    Unit    - Unit number.
*
*  Return value:
*/
static int _InitMedium(U8 Unit) {
  int i;
  DRIVER_INST * pInst;

  pInst = _apInst[Unit];
  if (pInst->IsInited == 0) {
    i = _Init(pInst);
    if (i < 0) { /* init failed, no valid card in slot */
      FS_DEBUG_WARN((FS_MTYPE_DRIVER, "MMC: Init failure, no valid card found"));
      return -1;
    } else {
      pInst->IsInited = 1;
    }
  }
  return 0;
}

/*********************************************************************
*
*       _AddDevice
*
*  Function Description:
*    Initializes the low-level driver object.
*
*  Return value:
*    >= 0                       - Command successfully executed, Unit no.
*    <  0                       - Error, could not add device
*
*/
static int _AddDevice(void) {
  U8            Unit;
  DRIVER_INST * pInst;

  if (_NumUnits >= NUM_UNITS) {
    return -1;
  }
  Unit = _NumUnits++;
  pInst = (DRIVER_INST *)FS_AllocZeroed(sizeof(DRIVER_INST));   // Alloc memory. This is guaranteed to work by the memory module.
  _apInst[Unit] = pInst;
  return Unit;
}

/*********************************************************************
*
*       _GetNumUnits
*
*  Function description:
*    
*  Return value:
*    >= 0                       - Command successfully executed, Unit no.
*    <  0                       - Error, could not add device
*/
static int _GetNumUnits(void) {
  return _NumUnits;
}

/*********************************************************************
*
*       _GetDriverName
*/
static const char * _GetDriverName(U8 Unit) {
  return "mmc";
}

/*********************************************************************
*
*       Public data
*
**********************************************************************
*/
const FS_DEVICE_TYPE FS_Driver_TMPA910CRAXBG = {
  _GetDriverName,
  _AddDevice,
  _Read,
  _Write,
  _IoCtl,
  _InitMedium,
  _GetStatus,
  _GetNumUnits
};

/*********************************************************************
*
*       Static code that can be modified by user
*
**********************************************************************
*/

/*********************************************************************
*
*       _SetMaxSpeed
*
*  Function description
*    Sets the frequency of the MMC/SD card controller.
*    The frequency is given in kHz.
*    It is called 2 times:
*     1. During card initialization
*        Initialize the frequency to not more than 400kHz.
*        
*     2. After card initialization
*        The CSD register of card is read and the max frequency 
*        the card can operate is determined.
*        [In most cases: MMC cards 20MHz, SD cards 25MHz]
*
*/
static void _SetMaxSpeed(U16 Freq) {
  unsigned Shift;
  U32      SDFreq;

  Shift = 1;
  do {
    SDFreq = CPU_HCLK >> Shift;
    if (SDFreq <= Freq) {
      break;
    }
    Shift++;
  } while (Shift < 10);
  //
  // Set new divider value
  //
  SD_CLK_CTRL = (1 << (Shift - 2));
  OS_Delay(5);
  //
  //  Enable clock
  //
  SD_CLK_CTRL |= (1 << 8);
}

/*********************************************************************
*
*       _IsCardPresent
*
*  Function description:
*    Check whether the card in the card slot or not.
*
*  Return value:
*    1       - Card is present
*    0       - Card is not present
*
*/
static int _IsCardPresent(void) {
  return TRUE;
}

/*********************************************************************
*
*       _IsCardWrProtected
*
*  Function description:
*    Returns whether the write protect switch is set or not.
*/
static int _IsCardWrProtected(void) {
  return FALSE;
}

/*************************** End of file ****************************/
