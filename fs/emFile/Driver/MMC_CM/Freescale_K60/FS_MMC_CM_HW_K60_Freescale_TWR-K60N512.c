/*********************************************************************
*                SEGGER MICROCONTROLLER GmbH & Co. KG                *
*        Solutions for real time microcontroller applications        *
**********************************************************************
*                                                                    *
*        (c) 2003-2012     SEGGER Microcontroller GmbH & Co KG       *
*                                                                    *
*        Internet: www.segger.com    Support:  support@segger.com    *
*                                                                    *
**********************************************************************

**** emFile file system for embedded applications ****
emFile is protected by international copyright laws. Knowledge of the
source code may not be used to write a similar product. This file may
only be used in accordance with a license and should not be re-
distributed in any way. We appreciate your understanding and fairness.
----------------------------------------------------------------------
File        : FS_MMC_HW_CM_K90_Freescale_TWR-K60N512.c
Purpose     : Hardware layer the Freescale Kinetis K60 (card mode)
Literature  : [1] \\fileserver\Techinfo\Company\Freescale\MCU\CortexM4_Kinetis\EvalBoard\TWR-K60N512-KIT\TWR-K60N512-UM.pdf
              [2] \\fileserver\Techinfo\Company\Freescale\MCU\CortexM4_Kinetis\K60P144M150SF3RM.pdf
              [3] \\fileserver\Techinfo\Company\Freescale\MCU\CortexM4_Kinetis\EvalBoard\KinetisK60_SCH26548Rev.C\TWR-K60N512-SCH_C.pdf
              [4] \\fileserver\Techinfo\Company\SDCard_org\SDCardPhysicalLayerSpec_V200.pdf
---------------------------END-OF-HEADER------------------------------
*/

/*********************************************************************
*
*       Includes
*
**********************************************************************
*/
#include "FS.h"
#include "FS_Int.h"
#include "MMC_SD_CardMode_X_HW.h"

/*********************************************************************
*
*       Defines configurable
*
**********************************************************************
*/
#define USE_DMA               1           // Turns the DMA support on and off
#define PERIPH_CLOCK          96000000uL  // Peripheral clock speed in Hz
#define DEFAULT_SPEED         400         // Default communication speed (kHz)
#define MAX_SPEED             50000       // Limits the maximum communication speed to this value (kHz)
#define DEFAULT_TIMEOUT       0x7FuL      // Card cycles (see the description of Data Timeout Register)
#if USE_DMA
  #define NUM_BLOCKS_AT_ONCE  256         // Maximum number of blocks in a data transfer
#else
  #define NUM_BLOCKS_AT_ONCE  65535       // Maximum number of blocks in a data transfer
#endif // USE_DMA

/*********************************************************************
*
*       Defines non configurable
*
**********************************************************************
*/
/*********************************************************************
*
*       Local defines (sfrs)
*
**********************************************************************
*/
//
// System integration module
//
#define SIM_BASE        0x40047000
#define SIM_SCGC3       (*(volatile U32*)(SIM_BASE + 0x1030))   // System Clock Gating Control Register 3
#define SIM_SCGC5       (*(volatile U32*)(SIM_BASE + 0x1038))   // System Clock Gating Control Register 5

//
// PORT E
//
#define PORTE_BASE      0x4004D000
#define PORTE_PCR0      (*(volatile U32*)(PORTE_BASE + 0x00))   // Pin control register 0
#define PORTE_PCR1      (*(volatile U32*)(PORTE_BASE + 0x04))   // Pin control register 1
#define PORTE_PCR2      (*(volatile U32*)(PORTE_BASE + 0x08))   // Pin control register 2
#define PORTE_PCR3      (*(volatile U32*)(PORTE_BASE + 0x0C))   // Pin control register 3
#define PORTE_PCR4      (*(volatile U32*)(PORTE_BASE + 0x10))   // Pin control register 4
#define PORTE_PCR5      (*(volatile U32*)(PORTE_BASE + 0x14))   // Pin control register 5
#define PORTE_PCR27     (*(volatile U32*)(PORTE_BASE + 0x6C))   // Pin control register 27
#define PORTE_PCR28     (*(volatile U32*)(PORTE_BASE + 0x70))   // Pin control register 28
#define PORTE_DFER      (*(volatile U32*)(PORTE_BASE + 0xC0))   // Digial filter enable register

//
// GPIO E
//
#define GPIOE_BASE      0x400FF000
#define GPIOE_PDOR      (*(volatile U32*)(GPIOE_BASE + 0x100))  // Port Data Output Register
#define GPIOE_PDIR      (*(volatile U32*)(GPIOE_BASE + 0x110))  // Port Data Input Register
#define GPIOE_PDDR      (*(volatile U32*)(GPIOE_BASE + 0x114))  // Port Data Direction Register

//
// SDHC
//
#define SDHC_BASE       0x400B1000
#define SDHC_BLKATTR    (*(volatile U32*)(SDHC_BASE + 0x04))   // Block Attributes Register
#define SDHC_CMDARG     (*(volatile U32*)(SDHC_BASE + 0x08))   // Command Argument Register
#define SDHC_XFERTYP    (*(volatile U32*)(SDHC_BASE + 0x0C))   // Transfer Type Register
#define SDHC_CMDRSP0    (*(volatile U32*)(SDHC_BASE + 0x10))   // Command Response 0
#define SDHC_DATPORT    (*(volatile U32*)(SDHC_BASE + 0x20))   // Buffer Data Port Register
#define SDHC_PRSSTAT    (*(volatile U32*)(SDHC_BASE + 0x24))   // Present State Register
#define SDHC_PROCTL     (*(volatile U32*)(SDHC_BASE + 0x28))   // Protocol Control Register
#define SDHC_SYSCTL     (*(volatile U32*)(SDHC_BASE + 0x2C))   // System Control Register
#define SDHC_IRQSTAT    (*(volatile U32*)(SDHC_BASE + 0x30))   // Interrupt Status Register
#define SDHC_IRQSTATEN  (*(volatile U32*)(SDHC_BASE + 0x34))   // Interrupt Status Enable Register
#define SDHC_WML        (*(volatile U32*)(SDHC_BASE + 0x44))   // Watermark Level Register
#define SDHC_ADSADDR    (*(volatile U32*)(SDHC_BASE + 0x58))   // ADMA System Address Register
#define SDHC_VENDOR     (*(volatile U32*)(SDHC_BASE + 0xC0))   // Vendor Specific Register

//
// MPU
//
#define MPU_BASE        0x4000D000
#define MPU_RGDAAC0     (*(volatile U32*)(MPU_BASE + 0x800))   // Region Descriptor Alternate Access Control 0

//
// Pin assignments of signals
//
#define SD_CMD_PIN      3   // Command         (GPIO E)
#define SD_CLK_PIN      2   // Clock           (GPIO E)
#define SD_D0_PIN       1   // Data 0          (GPIO E)
#define SD_D1_PIN       0   // Data 1          (GPIO E)
#define SD_D2_PIN       5   // Data 2          (GPIO E)
#define SD_D3_PIN       4   // Data 3          (GPIO E)
#define SD_CD_PIN       28  // Card detect     (GPIO E)
#define SD_WP_PIN       27  // Write protected (GPIO E)

//
// Port control register bis
//
#define PCR_MUX         8   // Alternate port setting
#define PCR_MUX_GPIO    1   // Controlled directly by the HW layer
#define PCR_MUX_SDHC    4   // Controlled by SDHC
#define PCR_DSE         6   // Driver strength enable
#define PCR_PE          1   // Pull enable
#define PCR_PS          0   // Pull-up enable

//
// Clock enable bits
//
#define SCGC3_SDHC      17    // SDHC
#define SCGC5_PORTE     13    // GPIO E

//
// SDHC interrupt request bits
//
#define IRQSTAT_CC      0   // Command complete
#define IRQSTAT_TC      1   // Transfer complete
#define IRQSTAT_DINT    3   // DMA interrupt
#define IRQSTAT_BWR     4   // Buffer write ready
#define IRQSTAT_BRR     5   // Buffer read ready
#define IRQSTAT_CTOE    16  // Command timeout error
#define IRQSTAT_CCE     17  // Command CRC error
#define IRQSTAT_CEBE    18  // Command end bit error
#define IRQSTAT_CIE     19  // Command index error
#define IRQSTAT_DTOE    20  // Data timeout error
#define IRQSTAT_DCE     21  // Data CRC error
#define IRQSTAT_DEBE    22  // Data end bit error
#define IRQSTAT_DMAE    28  // DMA error

//
// SDHC present status bits
//
#define PRSSTAT_CIHB    0   // Command inhibit (CMD)
#define PRSSTAT_CDIHB   1   // Command inhibit (DAT)
#define PRSSTAT_SDSTB   3   // Set to 1 if the clock is stable
#define PRSSTAT_SDOFF   7
#define PRESSTAT_BWEN   10

//
// SDHC system control bits
//
#define SYSCTL_IPGEN            0
#define SYSCTL_HCKEN            1
#define SYSCTL_PEREN            2
#define SYSCTL_SDCLKEN          3
#define SYSCTL_DVS              4
#define SYSCTL_DVS_MASK         0xFuL
#define SYSCTL_DVS_MAX          (SYSCTL_DVS_MASK + 1)
#define SYSCTL_SDCLKFS          8
#define SYSCTL_SDCLKFS_MASK     0xFFuL
#define SYSCTL_DTOCV            16
#define SYSCTL_DTOCV_MASK       0xFuL
#define SYSCTL_DTOCV_MAX        (SYSCTL_DTOCV_MASK - 1)
#define SYSCTL_RSTA             24
#define SYSCTL_RSTC             25
#define SYSCTL_RSTD             26
#define SYSCTL_INITA            27

//
// SDHC transfer type bits
//
#define XFERTYP_DMAEN           0     // DMA enable
#define XFERTYP_BCEN            1
#define XFERTYP_DTDSEL          4
#define XFERTYP_MSBSEL          5
#define XFERTYP_RSPTYP          16
#define XFERTYP_RSPTYP_NONE     0uL
#define XFERTYP_RSPTYP_136BIT   1uL
#define XFERTYP_RSPTYP_48BIT    2uL
#define XFERTYP_CCCEN           19
#define XFERTYP_CICEN           20
#define XFERTYP_DPSEL           21
#define XFERTYP_CMDIDX          24
#define XFERTYP_CMDIDX_MASK     0x3FuL

//
// SDHC protocol control bits
//
#define PROCTL_DTW              1
#define PROCTL_DTW_MASK         0x3uL
#define PROCTL_DTW_4BIT         0x1uL
#define PROCTL_EMODE            4
#define PROCTL_EMODE_LE         2uL
#define PROCTL_DMAS             8
#define PROCTL_DMAS_MASK        0x3uL
#define PROCTL_DMAS_DMA2        2uL

//
// SDHC block attributes bits
//
#define BLKATTR_BLKCNT          16

//
// SDHC Watermark level
//
#define WML_RDWML               0
#define WML_RDWML_MASK          0xFFuL
#define WML_WRWML               16
#define WML_WRWML_MASK          0xFFuL

//
// DMA attributes
//
#define DMA_ATTR_VALID          0
#define DMA_ATTR_END            1
#define DMA_ATTR_INT            2
#define DMA_ATTR_ACT            4
#define DMA_ATTR_ACT_TRAN       2uL

//
// MPU region descriptor bits
//
#define RGD_M5RE    27
#define RGD_M5WE    26

//
// Number of DMA descriptors
//
#define MAX_BLOCK_SIZE        512
#define MAX_DMA_LEN           65535
#define NUM_DMA_DESC          ((NUM_BLOCKS_AT_ONCE * MAX_BLOCK_SIZE + (MAX_DMA_LEN - 1)) / MAX_DMA_LEN)  // Number of DMA descriptors required.

/*********************************************************************
*
*       typedefs
*
**********************************************************************
*/

//
// Memory layout of a ADMA2 descriptor.
//
#if USE_DMA
typedef struct {
  U16 Attr;       // Descriptor attributes
  U16 NumBytes;   // Number of bytes to transfer
  U32 Addr;       // Destination/source memory address (32-bit aligned)
} DMA_DESC;
#endif

/*********************************************************************
*
*       Static data
*
**********************************************************************
*/
static U16          _BlockSize;   // Size of the block to transfer as set by the upper layer
static U16          _NumBlocks;   // Number of blocks to transfer as set by the upper layer
#if USE_DMA
  static DMA_DESC   _aDMADesc[NUM_DMA_DESC];    // List of DMA descriptors
  static void     * _pData;       // Destination/source buffer for the DMA transfers
#endif

/*********************************************************************
*
*       Static code
*
**********************************************************************
*/

/*********************************************************************
*
*       _WaitForResponse
*
*   Function description
*     Waits for a command response to arrive.
*
*   Return values
*     FS_MMC_CARD_NO_ERROR                Success
*     FS_MMC_CARD_RESPONSE_CRC_ERROR      CRC error in response
*     FS_MMC_CARD_RESPONSE_TIMEOUT        No response received
*     FS_MMC_CARD_RESPONSE_GENERIC_ERROR  Any other error
*/
static int _WaitForResponse(void) {
  U32 Status;

  //
  // Wait for command to finish
  //
  while (1) {
    Status = SDHC_IRQSTAT;
    if (Status & (1uL << IRQSTAT_CTOE)) {
      return FS_MMC_CARD_RESPONSE_TIMEOUT;        // Error, no response received.
    }
    if (Status & (1uL << IRQSTAT_CCE)) {
      return FS_MMC_CARD_RESPONSE_CRC_ERROR;      // Error, CRC error detected in response.
    }
    if (Status & (1uL << IRQSTAT_CEBE)) {
      return FS_MMC_CARD_RESPONSE_GENERIC_ERROR;  // Error, end bit of response is 0.
    }
    if (Status & (1uL << IRQSTAT_CIE)) {
      return FS_MMC_CARD_RESPONSE_GENERIC_ERROR;  // Error, command index do not match.
    }
    if (Status & (1uL << IRQSTAT_CC)) {
      return FS_MMC_CARD_NO_ERROR;                // OK, valid response received.
    }
  }
}

/*********************************************************************
*
*       _WaitForRxReady
*
*   Function description
*     Waits until new data is received. The function returns in case of a receiving error.
*
*   Return values
*     FS_MMC_CARD_NO_ERROR              Success
*     FS_MMC_CARD_READ_CRC_ERROR        CRC error in received data
*     FS_MMC_CARD_READ_TIMEOUT          No data received
*     FS_MMC_CARD_READ_GENERIC_ERROR    Any other error
*/
#if (USE_DMA == 0)
static int _WaitForRxReady(void) {
  U32 Status;

  while (1) {
    Status = SDHC_IRQSTAT;
    if (Status & (1uL << IRQSTAT_DEBE)) {
      return FS_MMC_CARD_READ_GENERIC_ERROR;      // Error, 0 detected on the end bit.
    }
    if (Status & (1uL << IRQSTAT_DCE)) {
      return FS_MMC_CARD_READ_CRC_ERROR;          // Error, CRC check failed.
    }
    if (Status & (1uL << IRQSTAT_DTOE)) {
      return FS_MMC_CARD_READ_TIMEOUT;            // Error, data timeout.
    }
    if (Status & (1uL << IRQSTAT_BRR)) {
      return FS_MMC_CARD_NO_ERROR;                // OK, data can be read from queue.
    }
  }
}
#endif

/*********************************************************************
*
*       _WaitForTxReady
*
*   Function description
*     Waits until the transmitter is ready to send new data.
*     The function retuns in case of an underrun condition.
*     This can happen when we are not able to deliver the data fast enough.
*
*   Return values
*     FS_MMC_CARD_NO_ERROR              Success
*     FS_MMC_CARD_WRITE_GENERIC_ERROR   Any other error
*/
#if (USE_DMA == 0)
static int _WaitForTxReady(void) {
  U32 Status;

  while (1) {
    Status = SDHC_IRQSTAT;
    if (Status & (1uL << IRQSTAT_DTOE)) {
      return FS_MMC_CARD_WRITE_GENERIC_ERROR;     // Error, data timeout.
    }
    if (Status & (1uL << IRQSTAT_DCE)) {
      return FS_MMC_CARD_WRITE_CRC_ERROR;         // Error, CRC check failed.
    }
    if (Status & (1uL << IRQSTAT_BWR)) {
      return FS_MMC_CARD_NO_ERROR;                // OK, data can be written to queue.
    }
  }
}
#endif

/*********************************************************************
*
*       _WaitForCmdReady
*
*   Function description
*     Waits until for the last command to finish.
*/
static void _WaitForCmdReady(void) {
  U32 Status;

  while (1) {
    Status = SDHC_PRSSTAT;
    if ((Status & (1uL << PRSSTAT_CIHB)) == 0) {
      if ((Status & (1uL << PRSSTAT_CDIHB)) == 0) {
        break;
      }
    }
  }
}

/*********************************************************************
*
*       _Delay1ms
*
*   Function description
*     Busy loops for about 1 millisecond.
*/
static void _Delay1ms(void) {
  volatile int i;

  //
  // This loop takes about 1ms at 25MHz.
  //
  for (i = 0; i < 0x7FF; ++i) {
    ;
  }
}

/*********************************************************************
*
*       _Reset
*
*   Function description
*     Resets the command and data state machines. Typ. called in case of an error.
*/
static void _Reset(void) {
  //
  // Reset command path.
  //
  SDHC_SYSCTL |=   1uL << SYSCTL_RSTC;
  SDHC_SYSCTL &= ~(1uL << SYSCTL_RSTC);
  //
  // Reset data path.
  //
  SDHC_SYSCTL |=   1uL << SYSCTL_RSTD;
  SDHC_SYSCTL &= ~(1uL << SYSCTL_RSTD);
}

/*********************************************************************
*
*       _SetWMLRead
*
*   Function description
*     Sets the watermark level for read operations.
*/
static void _SetWMLRead(U32 NumBytes) {
  U32 NumWords;

  NumWords  = NumBytes / 4;
  NumWords /= 3;
  if (NumWords == 0) {
    NumWords = 1;
  }
  SDHC_WML &= ~(WML_RDWML_MASK << WML_RDWML);
  SDHC_WML |= ~(WML_RDWML_MASK << NumWords);
}

/*********************************************************************
*
*       _GetWMLRead
*/
#if (USE_DMA == 0)
static U32 _GetWMLRead(void) {
  U32 NumWords;

  NumWords = (SDHC_WML >> WML_RDWML) & WML_RDWML_MASK;
  return NumWords;
}
#endif

/*********************************************************************
*
*       _SetWMLlWrite
*
*   Function description
*     Sets the watermark level for write operations.
*/
static void _SetWMLWrite(U32 NumBytes) {
  U32 NumWords;

  NumWords  = NumBytes / 4;
  NumWords  = NumWords * 2 / 3;
  if (NumWords == 0) {
    NumWords = 1;
  }
  SDHC_WML &= ~(WML_WRWML_MASK << WML_WRWML);
  SDHC_WML |= ~(WML_WRWML_MASK << NumWords);
}

/*********************************************************************
*
*       _GetWMLWrite
*/
#if (USE_DMA == 0)
static U32 _GetWMLWrite(void) {
  U32 NumWords;

  NumWords = (SDHC_WML >> WML_WRWML) & WML_WRWML_MASK;
  return NumWords;
}
#endif

/*********************************************************************
*
*       _StartDMATransfer
*
*   Function description
*     Starts a DMA data transfer.
*
*/
#if USE_DMA
static void _StartDMATransfer(void) {
  U32        NumBytes;
  DMA_DESC * pDesc;
  int        i;
  U8       * pAddr;
  U32        NumBytesAtOnce;

  pDesc    = _aDMADesc;
  pAddr    = (U8 *)_pData;
  NumBytes = _NumBlocks * _BlockSize;
  for (i = 0; i < NUM_DMA_DESC; ++i) {
    NumBytesAtOnce   = MIN(NumBytes, MAX_DMA_LEN);
    pDesc->Addr      = (U32)pAddr;
    pDesc->NumBytes  = NumBytesAtOnce;
    pDesc->Attr      = (DMA_ATTR_ACT_TRAN << DMA_ATTR_ACT)
                     | (1uL               << DMA_ATTR_VALID)
                     ;
    pAddr           += NumBytesAtOnce;
    NumBytes        -= NumBytesAtOnce;
    if (NumBytes == 0) {
      pDesc->Attr   |= (1uL << DMA_ATTR_INT)    // Set the DMA interrupt flag at the end of transfer.
                    |  (1uL << DMA_ATTR_END)
                    ;
      break;
    }
    ++pDesc;
  }
  SDHC_ADSADDR  = (U32)_aDMADesc;
  SDHC_PROCTL  &= ~(PROCTL_DMAS_MASK << PROCTL_DMAS);
  SDHC_PROCTL  |=  (PROCTL_DMAS_DMA2 << PROCTL_DMAS);
}
#endif // USE_DMA

/*********************************************************************
*
*       _WaitForEndOfDMARead
*/
#if USE_DMA
static int _WaitForEndOfDMARead(void) {
  U32 Status;

  while (1) {
    Status = SDHC_IRQSTAT;
    if (Status & (1uL << IRQSTAT_DTOE)) {
      return FS_MMC_CARD_READ_GENERIC_ERROR;      // Error, data timeout.
    }
    if (Status & (1uL << IRQSTAT_DCE)) {
      return FS_MMC_CARD_READ_CRC_ERROR;          // Error, CRC check failed.
    }
    if (Status & (1uL << IRQSTAT_DMAE)) {
      return FS_MMC_CARD_READ_GENERIC_ERROR;      // Error, DMA transfer failed.
    }
    if (Status & (1uL << IRQSTAT_DINT)) {
      return FS_MMC_CARD_NO_ERROR;                // OK, DMA transfer complete.
    }
  }
}
#endif // USE_DMA

/*********************************************************************
*
*       _WaitForEndOfDMAWrite
*/
#if USE_DMA
static int _WaitForEndOfDMAWrite(void) {
  U32 Status;

  while (1) {
    Status = SDHC_IRQSTAT;
    if (Status & (1uL << IRQSTAT_DTOE)) {
      return FS_MMC_CARD_WRITE_GENERIC_ERROR;     // Error, data timeout.
    }
    if (Status & (1uL << IRQSTAT_DCE)) {
      return FS_MMC_CARD_WRITE_CRC_ERROR;         // Error, CRC check failed.
    }
    if (Status & (1uL << IRQSTAT_DMAE)) {
      return FS_MMC_CARD_WRITE_GENERIC_ERROR;     // Error, DMA transfer failed.
    }
    if (Status & (1uL << IRQSTAT_DINT)) {
      return FS_MMC_CARD_NO_ERROR;                // OK, DMA transfer complete.
    }
  }
}
#endif // USE_DMA

/*********************************************************************
*
*       Public code
*
**********************************************************************
*/

/*********************************************************************
*
*       FS_MMC_HW_X_SetHWNumBlocks
*
*   Function description
*     Sets the number of block (sectors) to be transferred.
*
*/
void FS_MMC_HW_X_SetHWNumBlocks(U8 Unit, U16 NumBlocks) {
  FS_USE_PARA(Unit);
  _NumBlocks = NumBlocks;
}

/*********************************************************************
*
*       FS_MMC_HW_X_SetHWBlockLen
*
*   Function description
*     Sets the block size (sector size) that shall be transferred.
*/
void FS_MMC_HW_X_SetHWBlockLen(U8 Unit, U16 BlockSize) {
  FS_USE_PARA(Unit);
  _BlockSize = BlockSize;
}

/*********************************************************************
*
*       FS_MMC_HW_X_SetMaxSpeed
*
*  Function description
*    Sets the frequency of the MMC/SD card controller.
*    The frequency is given in kHz.
*    It is called 2 times:
*     1. During card initialization
*        Initialize the frequency to not more than 400kHz.
*
*     2. After card initialization
*        The CSD register of card is read and the max frequency
*        the card can operate is determined.
*        [In most cases: MMC cards 20MHz, SD cards 25MHz]
*
*/
U16 FS_MMC_HW_X_SetMaxSpeed(U8 Unit, U16 Freq) {
  U32 Prescaler;
  U32 Divisor;
  U32 Factor;
  U32 SDClock;

  FS_USE_PARA(Unit);
  //
  // Limit the communication speed.
  //
  if (Freq > MAX_SPEED) {
    Freq = MAX_SPEED;
  }
  SDClock = Freq * 1000;              // Convert to Hz
  Factor  = (PERIPH_CLOCK + SDClock - 1) / SDClock;
  //
  // Determine the prescaler and the divisor values.
  //
  if (Factor == 0) {
    Factor = 1;
  }
  if (Factor <= SYSCTL_DVS_MAX) {
    Prescaler  = 0;
    Divisor    = Factor - 1;
  } else {
    Prescaler = 1;
    while (1) {
      //
      // Compensate for integer division errors. We must generate a clock frequeny <= Freq.
      //
      if (Factor & 1) {
        ++Factor;
      }
      Factor >>= 1;
      if (Factor <= SYSCTL_DVS_MAX) {
        Divisor = Factor - 1;
        break;
      }
      Prescaler <<= 1;
    }
  }
  //
  // Stop the clock to be able to chang its frequency.
  //
  SDHC_SYSCTL &= ~(1uL << SYSCTL_SDCLKEN);
  //
  // Set the prescaler and the divisor.
  //
  SDHC_SYSCTL &= ~((SYSCTL_SDCLKFS_MASK << SYSCTL_SDCLKFS) |
                   (SYSCTL_DVS_MASK     << SYSCTL_DVS));
  SDHC_SYSCTL |= ((Prescaler & SYSCTL_SDCLKFS_MASK) << SYSCTL_SDCLKFS)
              |  ((Divisor & SYSCTL_DVS_MASK)       << SYSCTL_DVS)
              ;
  //
  // Enable the clock to SD card.
  //
  SDHC_SYSCTL |= (1uL << SYSCTL_SDCLKEN)
              |  (1uL << SYSCTL_PEREN)
              |  (1uL << SYSCTL_HCKEN)
              |  (1uL << SYSCTL_IPGEN)
              ;
  //
  // Wait for the clock to stabilize.
  //
  while ((SDHC_PRSSTAT & (1uL << PRSSTAT_SDSTB)) == 0) {
    ;
  }
  //
  // Return the actual clock frequency.
  //
  Factor = 1;
  if (Prescaler) {
    Factor *= Prescaler << 1;
  }
  Factor *= Divisor + 1;
  Freq = PERIPH_CLOCK / 1000 / Factor;
  return Freq;
}

/*********************************************************************
*
*       FS_MMC_HW_X_IsPresent
*
*    Returns the state of the media. If you do not know the state, return
*    FS_MEDIA_STATE_UNKNOWN and the higher layer will try to figure out if
*    a media is present.
*
*  Parameters:
*    Unit                 - Device Index
*
*  Return value:
*    FS_MEDIA_STATE_UNKNOWN      - the state of the media is unkown
*    FS_MEDIA_NOT_PRESENT        - no card is present
*    FS_MEDIA_IS_PRESENT         - a card is present
*/
int FS_MMC_HW_X_IsPresent(U8 Unit) {
  int r;

  FS_USE_PARA(Unit);
  r = (GPIOE_PDIR & (1uL << SD_CD_PIN)) ? FS_MEDIA_NOT_PRESENT : FS_MEDIA_IS_PRESENT;
  return r;
}

/*********************************************************************
*
*       FS_MMC_HW_X_IsWriteProtected
*
*  Function description
*    Returns wheter card is write protected or not.
*/
int FS_MMC_HW_X_IsWriteProtected(U8 Unit) {
  int r;

  FS_USE_PARA(Unit);
  r = (GPIOE_PDIR & (1 << SD_WP_PIN)) ? 1 : 0;
  return r;
}

/*********************************************************************
*
*       FS_MMC_HW_X_SetResponseTimeOut
*
*  Function description
*    Sets the reponse time out value given in MMC/SD card cycles.
*
*/
void FS_MMC_HW_X_SetResponseTimeOut(U8 Unit, U32 Value) {
  FS_USE_PARA(Unit);
  FS_USE_PARA(Value);
  //
  // The hardware has only a short and a long timeout.
  // We always set the long timeout before we send a command.
  //
}

/*********************************************************************
*
*       FS_MMC_HW_X_SetReadDataTimeOut
*
*  Function description
*    Sets the read data time out value given in MMC/SD card cycles.
*/
void FS_MMC_HW_X_SetReadDataTimeOut(U8 Unit, U32 Value) {
  FS_USE_PARA(Unit);

  if (Value > SYSCTL_DTOCV_MAX) {
    Value = SYSCTL_DTOCV_MAX;
  }
  SDHC_SYSCTL &= ~(SYSCTL_DTOCV_MASK << SYSCTL_DTOCV);
  SDHC_SYSCTL |= Value << SYSCTL_DTOCV;
}

/*********************************************************************
*
*       FS_MMC_HW_X_SendCmd
*
*   Function description
*     Sends a command to the MMC/SD card.
*
*   Parameters
*     Unit          Number of physical card controller
*     Cmd           Command number according to [4]
*     CmdFlags      Additional information about the command to execute
*     ResponseType  Type of response as defined in [4]
*     Arg           Command parameter
*/
void FS_MMC_HW_X_SendCmd(U8 Unit, unsigned Cmd, unsigned CmdFlags, unsigned ResponseType, U32 Arg) {
  U32 RegValue;

  FS_USE_PARA(Unit);
  _WaitForCmdReady();
  _Reset();
  RegValue = (Cmd & XFERTYP_CMDIDX_MASK) << XFERTYP_CMDIDX;   // Set the command index.
  switch (ResponseType) {
  //
  // No response is expected
  //
  case FS_MMC_RESPONSE_FORMAT_NONE:
  default:
    RegValue |= XFERTYP_RSPTYP_NONE << XFERTYP_RSPTYP;
    break;
  //
  // Short response is expected (48bit)
  //
  case FS_MMC_RESPONSE_FORMAT_R3:
    RegValue |= XFERTYP_RSPTYP_48BIT << XFERTYP_RSPTYP;
    break;

  case FS_MMC_RESPONSE_FORMAT_R1:
    RegValue |= (XFERTYP_RSPTYP_48BIT << XFERTYP_RSPTYP)
             |  (1uL << XFERTYP_CICEN)      // Check the received command index.
             |  (1uL << XFERTYP_CCCEN)      // Check the CRC of response.
             ;
    break;
  //
  // Long response is expected (136bit)
  //
  case FS_MMC_RESPONSE_FORMAT_R2:
    RegValue |= (XFERTYP_RSPTYP_136BIT << XFERTYP_RSPTYP)
             |  (1uL << XFERTYP_CCCEN)      // Check the CRC of response.
             ;
    break;
  }
  //
  // If required, Setup the transfer over data lines.
  //
  if (CmdFlags & FS_MMC_CMD_FLAG_DATATRANSFER) {
    RegValue |= (1uL << XFERTYP_DPSEL)
             ;
    if (_NumBlocks > 1) {
      RegValue |= (1uL << XFERTYP_MSBSEL)
               |  (1uL << XFERTYP_BCEN)
               ;
    }
    if (CmdFlags & FS_MMC_CMD_FLAG_WRITETRANSFER) {
      _SetWMLWrite(_BlockSize);
    } else {
      _SetWMLRead(_BlockSize);
      RegValue |= (1uL << XFERTYP_DTDSEL);  // Read data from SD card.
    }
    //
    // Configure the size and the number of blocks to transfer
    //
    SDHC_BLKATTR = (_NumBlocks << BLKATTR_BLKCNT) | _BlockSize;
    //
    // Configure the number of data lines to use for the transfer.
    //
    SDHC_PROCTL &= ~(PROCTL_DTW_MASK << PROCTL_DTW);
    if (CmdFlags & FS_MMC_CMD_FLAG_USE_SD4MODE) {
      SDHC_PROCTL |= (PROCTL_DTW_4BIT << PROCTL_DTW);
    }
#if USE_DMA
    RegValue |= (1uL << XFERTYP_DMAEN);
    _StartDMATransfer();
#endif // USE_DMA
  }
#if USE_DMA
  //
  // Enable the status flags we handle in DMA mode.
  //
  SDHC_IRQSTATEN = (1uL << IRQSTAT_CC)
                 | (1uL << IRQSTAT_TC)
                 | (1uL << IRQSTAT_CTOE)
                 | (1uL << IRQSTAT_CCE)
                 | (1uL << IRQSTAT_CEBE)
                 | (1uL << IRQSTAT_CIE)
                 | (1uL << IRQSTAT_DTOE)
                 | (1uL << IRQSTAT_DCE)
                 | (1uL << IRQSTAT_DEBE)
                 | (1uL << IRQSTAT_DINT)
                 | (1uL << IRQSTAT_DMAE)
                 ;
#else
  //
  // Enable the status flags we handle in polling mode.
  //
  SDHC_IRQSTATEN = (1uL << IRQSTAT_CC)
                 | (1uL << IRQSTAT_TC)
                 | (1uL << IRQSTAT_BWR)
                 | (1uL << IRQSTAT_BRR)
                 | (1uL << IRQSTAT_CTOE)
                 | (1uL << IRQSTAT_CCE)
                 | (1uL << IRQSTAT_CEBE)
                 | (1uL << IRQSTAT_CIE)
                 | (1uL << IRQSTAT_DTOE)
                 | (1uL << IRQSTAT_DCE)
                 | (1uL << IRQSTAT_DEBE)
                 ;
#endif
  //
  // This is the initialization delay defined by the SD card specification as:
  // maximum of 1 msec, 74 clock cycles and supply ramp up time.
  // The sequence below sends 80 clock cycles which is enough for
  // the power supply of SD card to raise to Vdd min.
  //
  if (CmdFlags & FS_MMC_CMD_FLAG_INITIALIZE) {
    SDHC_SYSCTL |= 1uL << SYSCTL_INITA;
    while (SDHC_SYSCTL & (1uL << SYSCTL_INITA)) {
      ;
    }
  }
  SDHC_IRQSTAT = ~0uL;                      // Reset all status flags.
  SDHC_CMDARG  = Arg;
  SDHC_XFERTYP = RegValue;                  // Send the command.
}

/*********************************************************************
*
*       FS_MMC_HW_X_GetResponse
*
*  Function description
*    Receives the reponses that was sent by the card after
*    a command was sent to the card.
*
*  Parameters
*    Unit     Number of physical card controller
*    pBuffer  User allocated buffer where the response is stored.
*    Size     Size of the buffer in bytes
*
*  Return values
*     FS_MMC_CARD_NO_ERROR                Success
*     FS_MMC_CARD_RESPONSE_CRC_ERROR      CRC error in response
*     FS_MMC_CARD_RESPONSE_TIMEOUT        No response received
*     FS_MMC_CARD_RESPONSE_GENERIC_ERROR  Any other error
*
*  Notes
*     (1) The SD car driver expects the whole response as it comes over
*         the wire even though only the payload is used.
*         In other words: if the card controller delivers only the
*         reponse payload it should be stored starting at byte
*         index 1. Byte index 0 is the start bit, transmission bit
*         and the 6 stuff bits (see [2] chapter 4.9).
*
*/
int FS_MMC_HW_X_GetResponse(U8 Unit, void * pBuffer, U32 Size) {
  U8           * pData;
  volatile U32 * pReg;
  U32 NumWords;
  U32 Value;
  U32 i;
  int r;

  FS_USE_PARA(Unit);
  r = _WaitForResponse();
  if (r != FS_MMC_CARD_NO_ERROR) {
    _Reset();
    return r;                     // Error
  }
  //
  // The size we get from the upper driver is total response size in bytes.
  // We compute here the number of read accesses to the 32-bit register which
  // holds the response. Take into account that the first byte of the response
  // is not delivered by the hardware
  //
  NumWords = (Size - 1) / 4;
  pData    = pBuffer;
  pReg     = &SDHC_CMDRSP0 + (NumWords - 1);
  //
  // In case of a 136 bit response SDHC does not deliver the checksum byte.
  // By not incrementing the data pointer the bytes are saved at expected positions.
  //
  if (NumWords != 4) {
    ++pData;                      // See note (1)
  }
  for (i = 0; i < NumWords; ++i) {
    Value = *pReg;
    *pData++ = (U8)(Value >> 24);
    *pData++ = (U8)(Value >> 16);
    *pData++ = (U8)(Value >> 8);
    *pData++ = (U8)Value;
    --pReg;
  }
  return FS_MMC_CARD_NO_ERROR;  // OK, valid command received.
}

/*********************************************************************
*
*       FS_MMC_HW_X_ReadData
*
*   Function description
*     Reads data from MMC/SD card through the MMC/SD card controller.
*
*   Return values
*     FS_MMC_CARD_NO_ERROR            Success
*     FS_MMC_CARD_READ_CRC_ERROR      CRC error in received data
*     FS_MMC_CARD_READ_TIMEOUT        No data received
*     FS_MMC_CARD_READ_GENERIC_ERROR  Any other error
*
*/
int FS_MMC_HW_X_ReadData(U8 Unit, void * pBuffer, unsigned NumBytes, unsigned NumBlocks) {
#if USE_DMA
  int r;

  FS_USE_PARA(Unit);
  FS_USE_PARA(pBuffer);
  FS_USE_PARA(NumBytes);
  FS_USE_PARA(NumBlocks);
  r = _WaitForEndOfDMARead();
  return r;
#else
  U32   NumWords;
  U32 * pData32;
  U32   NumWordsAtOnce;
  U32   WordsPerBlock;
  U32   WMLRead;
  int   r;

  FS_USE_PARA(Unit);
  pData32       = (U32 *)pBuffer;
  WordsPerBlock = NumBytes / 4;
  WMLRead       = _GetWMLRead();
  do {
    NumWords = WordsPerBlock;
    do {
      r = _WaitForRxReady();
      if (r != FS_MMC_CARD_NO_ERROR) {
        _Reset();
        return r;                             // Error
      }
      NumWordsAtOnce  = MIN(NumWords, WMLRead);
      NumWords       -= NumWordsAtOnce;
      do {
        *pData32++ = SDHC_DATPORT;
      } while (--NumWordsAtOnce);
      SDHC_IRQSTAT = 1uL << IRQSTAT_BRR;      // This flag is only set by SDHC. We have to clear it here to get the next notification.
    } while (NumWords);
  } while (--NumBlocks);
  return FS_MMC_CARD_NO_ERROR;                // OK, data received.
#endif
}

/*********************************************************************
*
*       FS_MMC_HW_X_WriteData
*
*  Function description
*    Writes the data to MMC/SD card through the MMC/SD card controller.
*
*   Return values
*     FS_MMC_CARD_NO_ERROR      Success
*     FS_MMC_CARD_READ_TIMEOUT  No data received
*/
int FS_MMC_HW_X_WriteData(U8 Unit, const void * pBuffer, unsigned NumBytes, unsigned NumBlocks) {
#if USE_DMA
  int r;

  FS_USE_PARA(Unit);
  FS_USE_PARA(pBuffer);
  FS_USE_PARA(NumBytes);
  FS_USE_PARA(NumBlocks);
  r = _WaitForEndOfDMAWrite();
  return r;
#else
  U32         NumWords;
  const U32 * pData32;
  int         r;
  U32         NumWordsAtOnce;
  U32         WordsPerBlock;
  U32         WMLWrite;

  FS_USE_PARA(Unit);
  pData32       = (U32 *)pBuffer;
  WordsPerBlock = NumBytes / 4;
  WMLWrite      = _GetWMLWrite();
  do {
    NumWords    = WordsPerBlock;
    do {
      r = _WaitForTxReady();
      if (r != FS_MMC_CARD_NO_ERROR) {
        _Reset();
        return r;                             // Error
      }
      NumWordsAtOnce  = MIN(NumWords, WMLWrite);
      NumWords       -= NumWordsAtOnce;
      do {
        SDHC_DATPORT = *pData32++;
      } while (--NumWordsAtOnce);
      SDHC_IRQSTAT = 1uL << IRQSTAT_BWR;      // This flag is only set by SDHC. We have to clear it here to get the next notification.
    } while (NumWords);
  } while (--NumBlocks);
  return FS_MMC_CARD_NO_ERROR;                // OK, data sent.
#endif
}

/*********************************************************************
*
*       FS_MMC_HW_X_Delay
*
*   Function description
*     Waits for a certain time given by the parameter.
*
*   Parameters
*     ms
*/
void FS_MMC_HW_X_Delay(int ms) {
  int i;

  for (i = 0; i < ms; ++i) {
    _Delay1ms();
  }
}

/*********************************************************************
*
*       FS_MMC_HW_X_InitHW
*
*   Function description
*     Initialize the MMC/SD card controller.
*
*   Parameters
*     Unit
*
*/
void FS_MMC_HW_X_InitHW(U8 Unit) {
  FS_USE_PARA(Unit);

  //
  // Enable the clock of GPIOs and of SDHC
  //
  SIM_SCGC3 |= 1uL << SCGC3_SDHC;
  SIM_SCGC5 |= 1uL << SCGC5_PORTE;
  //
  // Clock line
  //
  PORTE_PCR2  = (PCR_MUX_SDHC << PCR_MUX)
              | (1uL          << PCR_PE)
              | (1uL          << PCR_PS)
              | (1uL          << PCR_DSE)
              ;
  //
  // Command line
  //
  PORTE_PCR3  = (PCR_MUX_SDHC << PCR_MUX)
              | (1uL          << PCR_PE)
              | (1uL          << PCR_PS)
              | (1uL          << PCR_DSE)
              ;
  //
  // Data 0 line
  //
  PORTE_PCR1  = (PCR_MUX_SDHC << PCR_MUX)
              | (1uL          << PCR_PE)
              | (1uL          << PCR_PS)
              | (1uL          << PCR_DSE)
              ;
  //
  // Data 1 line
  //
  PORTE_PCR0  = (PCR_MUX_SDHC << PCR_MUX)
              | (1uL          << PCR_PE)
              | (1uL          << PCR_PS)
              | (1uL          << PCR_DSE)
              ;
  //
  // Data 2 line
  //
  PORTE_PCR5  = (PCR_MUX_SDHC << PCR_MUX)
              | (1uL          << PCR_PE)
              | (1uL          << PCR_PS)
              | (1uL          << PCR_DSE)
              ;
  //
  // Data 3 line
  //
  PORTE_PCR4  = (PCR_MUX_SDHC << PCR_MUX)
              | (1uL          << PCR_PE)
              | (1uL          << PCR_PS)
              | (1uL          << PCR_DSE)
              ;
  //
  // Write protect is controlled by the HW layer.
  //
  PORTE_PCR27 = (PCR_MUX_GPIO << PCR_MUX)
              | (1uL          << PCR_PE)
              | (1uL          << PCR_PS)
              ;
  //
  // Card detect is controlled by the HW layer
  //
  PORTE_PCR28 = (PCR_MUX_GPIO << PCR_MUX)
              | (1uL          << PCR_PE)
              | (1uL          << PCR_PS)
              ;
  //
  // Disable the digial filtering on all port pins assigned to SDHC.
  //
  PORTE_DFER &= ~((1uL << SD_CD_PIN)  |
                  (1uL << SD_WP_PIN)  |
                  (1uL << SD_CLK_PIN) |
                  (1uL << SD_CMD_PIN) |
                  (1uL << SD_D0_PIN)  |
                  (1uL << SD_D1_PIN)  |
                  (1uL << SD_D2_PIN)  |
                  (1uL << SD_D3_PIN));
#if USE_DMA
  //
  // Give DMA access to system memory.
  //
  MPU_RGDAAC0 |= (1uL << RGD_M5RE)
              |  (1uL << RGD_M5WE)
              ;
#endif
  //
  // Configure SDHC.
  //
  SDHC_SYSCTL = SYSCTL_RSTA;    // Reset peripheral.
  SDHC_SYSCTL = 0;              // Start peripheral.
  SDHC_PROCTL = PROCTL_EMODE_LE  << PROCTL_EMODE;
  SDHC_VENDOR = 0;              // No external DMA requests.
}

/*********************************************************************
*
*       FS_MMC_HW_X_GetMaxReadBurst
*
*  Function description
*    Returns the number of block (sectors)
*    that can be read at once with a single
*    READ_MULTIPLE_SECTORS.
*
*/
U16 FS_MMC_HW_X_GetMaxReadBurst(U8 Unit) {
  FS_USE_PARA(Unit);
  return NUM_BLOCKS_AT_ONCE;
}

/*********************************************************************
*
*       FS_MMC_HW_X_GetMaxWriteBurst
*
*  Function description
*    Returns the number of block (sectors)
*    that can be written at once with a single
*    WRITE_MULTIPLE_SECTORS.
*/
U16 FS_MMC_HW_X_GetMaxWriteBurst(U8 Unit) {
  FS_USE_PARA(Unit);
  return NUM_BLOCKS_AT_ONCE;
}

/*********************************************************************
*
*       FS_MMC_HW_X_SetDataPointer
*
*  Function description
*    Tells the hardware layer where to read data from
*    or write data to. This may be necessary for some controller,
*    before sending the command to the card, eg. programming the DMA.
*    In most cases this function can be left empty.
*
*  Parameters:
*    Unit          - SD card controller no, in case there are more than one.
*
*/
void FS_MMC_HW_X_SetDataPointer(U8 Unit, const void * p) {
  FS_USE_PARA(Unit);
#if USE_DMA
  _pData = (void *)p;     // Cast const away.
#else
  FS_USE_PARA(p);
#endif
}
