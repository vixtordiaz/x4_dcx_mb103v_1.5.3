/*********************************************************************
*                SEGGER MICROCONTROLLER GmbH & Co. KG                *
*        Solutions for real time microcontroller applications        *
**********************************************************************
*                                                                    *
*        (c) 2003-2012     SEGGER Microcontroller GmbH & Co KG       *
*                                                                    *
*        Internet: www.segger.com    Support:  support@segger.com    *
*                                                                    *
**********************************************************************

**** emFile file system for embedded applications ****
emFile is protected by international copyright laws. Knowledge of the
source code may not be used to write a similar product. This file may
only be used in accordance with a license and should not be re-
distributed in any way. We appreciate your understanding and fairness.
----------------------------------------------------------------------
File        : FS_MMC_CM_HW_LPC3250_Phytec.c
Purpose     : Low level MMC/SD driver for the NXP LPC3250
Literature  : [1] \\fileserver\Techinfo\Company\NXP\MCU\LPC32xx\LPC32x0_UM_080529.pdf
              [2] \\fileserver\Techinfo\Company\Phytec\LPC3250\Phytec_LPC3250_schematic.pdf
              [3] \\fileserver\Techinfo\Company\Phytec\LPC3250\LPC3250_HM.pdf
---------------------------END-OF-HEADER------------------------------
*/

/*********************************************************************
*
*       Includes
*
**********************************************************************
*/
#include "FS_Int.h"
#include "MMC_SD_CardMode_X_HW.h"
#include "RTOS.h"

/*********************************************************************
*
*       Defines configurable
*
**********************************************************************
*/
#define PCLK          (208000000uL / 4)   // Clock speed of MCI peripheral in Hz
#define DMA_CHANNEL   1                   // Index of the DMA channel used for the data transfer
#define MAX_SPEED     50000               // Maximum clock speed of the SD card in KHz

/*********************************************************************
*
*       Defines non configurable
*
**********************************************************************
*/

/*********************************************************************
*
*       Multimedia Card Interface registers
*/
#define MCI_BASE_ADDR		0x20098000
#define MCI_POWER      (*(volatile U32 *)(MCI_BASE_ADDR + 0x00))
#define MCI_CLOCK      (*(volatile U32 *)(MCI_BASE_ADDR + 0x04))
#define MCI_ARGUMENT   (*(volatile U32 *)(MCI_BASE_ADDR + 0x08))
#define MCI_COMMAND    (*(volatile U32 *)(MCI_BASE_ADDR + 0x0C))
#define MCI_RESP_CMD   (*(volatile U32 *)(MCI_BASE_ADDR + 0x10))
#define MCI_RESP0      (*(volatile U32 *)(MCI_BASE_ADDR + 0x14))
#define MCI_RESP1      (*(volatile U32 *)(MCI_BASE_ADDR + 0x18))
#define MCI_RESP2      (*(volatile U32 *)(MCI_BASE_ADDR + 0x1C))
#define MCI_RESP3      (*(volatile U32 *)(MCI_BASE_ADDR + 0x20))
#define MCI_DATA_TMR   (*(volatile U32 *)(MCI_BASE_ADDR + 0x24))
#define MCI_DATA_LEN   (*(volatile U32 *)(MCI_BASE_ADDR + 0x28))
#define MCI_DATA_CTRL  (*(volatile U32 *)(MCI_BASE_ADDR + 0x2C))
#define MCI_DATA_CNT   (*(volatile U32 *)(MCI_BASE_ADDR + 0x30))
#define MCI_STATUS     (*(volatile U32 *)(MCI_BASE_ADDR + 0x34))
#define MCI_CLEAR      (*(volatile U32 *)(MCI_BASE_ADDR + 0x38))
#define MCI_MASK0      (*(volatile U32 *)(MCI_BASE_ADDR + 0x3C))
#define MCI_MASK1      (*(volatile U32 *)(MCI_BASE_ADDR + 0x40))
#define MCI_FIFO_CNT   (*(volatile U32 *)(MCI_BASE_ADDR + 0x48))
#define MCI_FIFO       (*(volatile U32 *)(MCI_BASE_ADDR + 0x80))
#define MCI_FIFO_ADDR  ( (volatile U32 *)(MCI_BASE_ADDR + 0x80))

/*********************************************************************
*
*       Clocking and power control
*/
#define MS_CTRL        (*(volatile U32 *)(0x40004080))
#define DMACLK_CTRL    (*(volatile U32 *)(0x400040E8))

/*********************************************************************
*
*       General Purpose Input/Output (GPIO)
*/
#define GPIO_BASE_ADDR		0x40028000
#define PIO_INP_STATE  (*(volatile U32 *)(GPIO_BASE_ADDR + 0x00))

/*********************************************************************
*
*       General-purpose DMA Controller
*/
#define GPDMA_BASE_ADDR   0x31000000
#define GPDMA_INT_STAT         (*(volatile U32 *)(GPDMA_BASE_ADDR + 0x000))
#define GPDMA_INT_TCSTAT       (*(volatile U32 *)(GPDMA_BASE_ADDR + 0x004))
#define GPDMA_INT_TCCLR        (*(volatile U32 *)(GPDMA_BASE_ADDR + 0x008))
#define GPDMA_INT_ERR_STAT     (*(volatile U32 *)(GPDMA_BASE_ADDR + 0x00C))
#define GPDMA_INT_ERR_CLR      (*(volatile U32 *)(GPDMA_BASE_ADDR + 0x010))
#define GPDMA_RAW_INT_TCSTAT   (*(volatile U32 *)(GPDMA_BASE_ADDR + 0x014))
#define GPDMA_RAW_INT_ERR_STAT (*(volatile U32 *)(GPDMA_BASE_ADDR + 0x018))
#define GPDMA_ENABLED_CHNS     (*(volatile U32 *)(GPDMA_BASE_ADDR + 0x01C))
#define GPDMA_SOFT_BREQ        (*(volatile U32 *)(GPDMA_BASE_ADDR + 0x020))
#define GPDMA_SOFT_SREQ        (*(volatile U32 *)(GPDMA_BASE_ADDR + 0x024))
#define GPDMA_SOFT_LBREQ       (*(volatile U32 *)(GPDMA_BASE_ADDR + 0x028))
#define GPDMA_SOFT_LSREQ       (*(volatile U32 *)(GPDMA_BASE_ADDR + 0x02C))
#define GPDMA_CONFIG           (*(volatile U32 *)(GPDMA_BASE_ADDR + 0x030))
#define GPDMA_SYNC             (*(volatile U32 *)(GPDMA_BASE_ADDR + 0x034))
#define GPDMA_CH_SRC            (*(volatile U32 *)(GPDMA_BASE_ADDR + (DMA_CHANNEL * 0x20) + 0x100))
#define GPDMA_CH_DEST           (*(volatile U32 *)(GPDMA_BASE_ADDR + (DMA_CHANNEL * 0x20) + 0x104))
#define GPDMA_CH_LLI            (*(volatile U32 *)(GPDMA_BASE_ADDR + (DMA_CHANNEL * 0x20) + 0x108))
#define GPDMA_CH_CTRL           (*(volatile U32 *)(GPDMA_BASE_ADDR + (DMA_CHANNEL * 0x20) + 0x10C))
#define GPDMA_CH_CFG            (*(volatile U32 *)(GPDMA_BASE_ADDR + (DMA_CHANNEL * 0x20) + 0x110))

/*********************************************************************
*
*       MCI status register
*/
#define STATUS_CMD_CRC_FAIL         0
#define STATUS_DATA_CRC_FAIL        1
#define STATUS_CMD_TIMEOUT          2
#define STATUS_DATA_TIMEOUT         3
#define STATUS_TX_UNDERRUN          4
#define STATUS_RX_OVERRUN           5
#define STATUS_CMD_RESP_END         6
#define STATUS_CMD_SENT             7
#define STATUS_DATA_END             8
#define STATUS_START_BIT_ERROR      9
#define STATUS_CMD_ACTIVE           11
#define STATUS_TX_FIFO_HALF_EMPTY   14
#define STATUS_RX_FIFO_HALF_FULL    15
#define STATUS_TX_FIFO_FULL         16
#define STATUS_RX_FIFO_FULL         17
#define STATUS_TX_FIFO_EMPTY        18
#define STATUS_TX_DATA_AVAIL        20
#define STATUS_RX_DATA_AVAIL        21

/*********************************************************************
*
*       MCI clock register
*/
#define CLOCK_ENABLE            8
#define CLOCK_PWR_SAVE          9
#define CLOCK_WIDE_BUS          11

/*********************************************************************
*
*       MCI command register
*/
#define COMMAND_RESPONSE        6
#define COMMAND_ENABLE          10

/*********************************************************************
*
*       MCI data control register
*/
#define DATA_CTRL_ENABLE        0
#define DATA_CTRL_DIRECTION     1
#define DATA_CTRL_DMA_ENABLE    3

/*********************************************************************
*
*       DMA channel configuration register
*/
#define CH_CFG_ENABLE           0
#define CH_CFG_SRC_PERIPH       1
#define CH_CFG_DEST_PERIPH      6
#define CH_CFG_FLOW_CTRL        11
#define CH_CFG_ACTIVE           17
#define CH_CFG_LOCKED           16
#define CH_CFG_HALT             18

/*********************************************************************
*
*       DMA channel control register
*/
#define CH_CTRL_SBSIZE          12
#define CH_CTRL_DBSIZE          15
#define CH_CTRL_SWIDTH          18
#define CH_CTRL_DWIDTH          21
#define CH_CTRL_SMASTER         24
#define CH_CTRL_SINC            26
#define CH_CTRL_DINC            27
#define CH_CTRL_TC_EN           31

/*********************************************************************
*
*       DMA configuration
*/
#define CONFIG_EN               0

/*********************************************************************
*
*       Data buffer defines
*/
#define NUM_SECTORS_AT_ONCE     16
#define SECTOR_SIZE             512
#define CACHE_LINE_SIZE         32uL

/*********************************************************************
*
*       Static data
*
**********************************************************************
*/
static U32  _BlockSize;
static U8     _IgnoreCRC;
static U32  _DataControlReg;
static U32  _NumBlocks;
static U32    _aSectorBuffer[((NUM_SECTORS_AT_ONCE) * SECTOR_SIZE + CACHE_LINE_SIZE) / 4];   // Buffer used for the data transfer over DMA
static U32  * _pSectorBuffer;       // Cached aligned buffer for DMA transfers.
static U32  * _pReadBuffer;
static void * _pBuffer;
static U32    _NumBytesToRead;

/*********************************************************************
*
*       Static code
*
**********************************************************************
*/

/*********************************************************************
*
*       _BufferIsAligned
*/
static int _BufferIsAligned(const void * pBuffer, U32 NumBytes) {
  int r;
  
  r = 1;
  if (((U32)pBuffer & (CACHE_LINE_SIZE - 1)) || (NumBytes & (CACHE_LINE_SIZE - 1))) {
    r = 0;
  }
  return r;
}


/*********************************************************************
*
*       _Delay1ms
*
*   Function description
*     Blocks the program execution for about 1ms.
*     The number of loops must be ajusted according to the CPU speed.
*/
static void _Delay1ms(void) {
  volatile unsigned NumLoops;

  NumLoops = 2500;
  do {
    ;
  } while(--NumLoops);
}

/*********************************************************************
*
*       _ld
*/
static U16 _ld(U32 Value) {
  U16 i;
  for (i = 0; i < 16; i++) {
    if ((1uL << i) == Value) {
      break;
    }
  }
  return i;
}

/*********************************************************************
*
*       _StartDataTransfer
*
*  Function description
*    This fucntion writes the MCI data control registers,
*    and initiates thus the data transfer.
*    The information how to set the register is done in FS_MMC_HW_X_SendCmd().
*/
static void _StartDataTransfer(U32 NumBytes) {
  MCI_DATA_LEN  = NumBytes;
  MCI_DATA_CTRL = _DataControlReg;
}
                      
/*********************************************************************
*
*       _DMAInit
*
*  Function description
*    Initialize the DMA.
*    This is necessary since data tranfer thru polling does not work correctly.
*/
static void _DMAInit(void) {
  //
  // Enable DMA clock.
  //
  DMACLK_CTRL = 1;
  //
  // Clear interrupts on DMA channel.
  //
  GPDMA_INT_TCCLR   = 1uL << DMA_CHANNEL;
  GPDMA_INT_ERR_CLR = 1uL << DMA_CHANNEL;
  //
  //  Enable DMA controller, AHB Master is little endian
  //
  GPDMA_CONFIG  = 1uL << CONFIG_EN;
  while ((GPDMA_CONFIG & (1uL << CONFIG_EN)) == 0) {
    ;
  }
}

/*********************************************************************
*
*       _StartDMARead
*
*  Function description
*    Before starting any data tranfers from or to the card
*    we need to program the DMA which will do the work.
*/
static void _StartDMARead(void * pBuffer, U32 NumBytes) {
  //
  // Disable the DMA channel if required
  //
  if (GPDMA_ENABLED_CHNS & (1uL << DMA_CHANNEL)) {
    GPDMA_CH_CFG = 1uL << CH_CFG_HALT;
    while (GPDMA_CH_CFG & (1uL << CH_CFG_ACTIVE)) {
      ;
    }
    GPDMA_CH_CFG &= ~(1uL << CH_CFG_ENABLE);
  }
  GPDMA_INT_TCCLR   = 1uL << DMA_CHANNEL;
  GPDMA_INT_ERR_CLR = 1uL << DMA_CHANNEL;
  GPDMA_CH_SRC      = (U32)MCI_FIFO_ADDR;
  GPDMA_CH_DEST     = (U32)pBuffer;
  GPDMA_CH_CTRL     = 0
                    | ((NumBytes >> 2) & 0x0FFF)    // Transfer words
                    | (2uL << CH_CTRL_SBSIZE)
                    | (2uL << CH_CTRL_DBSIZE)
                    | (2uL << CH_CTRL_SWIDTH)
                    | (2uL << CH_CTRL_DWIDTH)
                    | (1uL << CH_CTRL_DINC)
                    | (1uL << CH_CTRL_TC_EN)
                    | (1uL << CH_CTRL_SMASTER)      // Select the AHB master
                    ;
  GPDMA_CH_CFG      = 0
                    | (1uL << CH_CFG_ENABLE)
                    | (1uL << CH_CFG_LOCKED)
                    | (4uL << CH_CFG_SRC_PERIPH)    // SD/MMC is the source peripheral
                    | (6uL << CH_CFG_FLOW_CTRL)     // SD/MMC controls the transfer to memory
                    ;
}

/*********************************************************************
*
*       _StartDMAWrite
*
*  Function description
*    Before starting any data tranfers from or to the card
*    we need to program the DMA which will do the work.
*/
static void _StartDMAWrite(const void * pBuffer, U32 NumBytes) {
  //
  // Disable the DMA channel if required
  //
  if (GPDMA_ENABLED_CHNS & (1uL << DMA_CHANNEL)) {
    GPDMA_CH_CFG = 1uL << CH_CFG_HALT;
    while (GPDMA_CH_CFG & (1uL << CH_CFG_ACTIVE)) {
                  ;
    }
    GPDMA_CH_CFG &= ~(1uL << CH_CFG_ENABLE);
  }
  GPDMA_INT_TCCLR   = 1uL << DMA_CHANNEL;
  GPDMA_INT_ERR_CLR = 1uL << DMA_CHANNEL;
  GPDMA_CH_SRC      = (U32)pBuffer;
  GPDMA_CH_DEST     = (U32)MCI_FIFO_ADDR;
  GPDMA_CH_CTRL     = 0
                    | ((NumBytes >> 2) & 0x0FFF)    // Transfer words
                    | (2uL << CH_CTRL_SBSIZE)
                    | (2uL << CH_CTRL_DBSIZE)
                    | (2uL << CH_CTRL_SWIDTH)
                    | (2uL << CH_CTRL_DWIDTH)
                    | (1uL << CH_CTRL_SINC)
                    | (1uL << CH_CTRL_TC_EN)
                    | (1uL << CH_CTRL_SMASTER)      // Select the AHB master
                    ;
  GPDMA_CH_CFG      = 0
                    | (1uL << CH_CFG_ENABLE)
                    | (1uL << CH_CFG_LOCKED)
                    | (4uL << CH_CFG_DEST_PERIPH)   // SD/MMC is the destination peripheral
                    | (5uL << CH_CFG_FLOW_CTRL)     // SD/MMC controls the transfer to card
                  ;
}

/*********************************************************************
*
*       _WaitForEndOfDMARead
*
*/
static int _WaitForEndOfDMARead(void) {
  U32 Status;

  do {
    Status = MCI_STATUS;
    if (Status & (1uL << STATUS_DATA_CRC_FAIL)) {
      return FS_MMC_CARD_READ_CRC_ERROR;
    }
    if (Status & (1uL << STATUS_DATA_TIMEOUT)) {
      return FS_MMC_CARD_READ_TIMEOUT;
    }
    if (Status & (1uL << STATUS_RX_OVERRUN)) {
      return FS_MMC_CARD_READ_GENERIC_ERROR;
    }
    if (Status & (1uL << STATUS_START_BIT_ERROR)) {
      return FS_MMC_CARD_READ_GENERIC_ERROR;
  }
    if (GPDMA_RAW_INT_ERR_STAT & (1uL << DMA_CHANNEL)) {
      return FS_MMC_CARD_READ_GENERIC_ERROR;
    }
    if (GPDMA_RAW_INT_TCSTAT & (1uL << DMA_CHANNEL)) {
      return FS_MMC_CARD_NO_ERROR;  // OK, data received
    }
  } while (1);
}

/*********************************************************************
*
*       _WaitForEndOfDMAWrite
*
*/
static int _WaitForEndOfDMAWrite(void) {
  U32 Status;

  do {
    Status = MCI_STATUS;
    if (Status & (1uL << STATUS_DATA_CRC_FAIL)) {
      return FS_MMC_CARD_WRITE_CRC_ERROR;
    }
    if (Status & (1uL << STATUS_TX_UNDERRUN)) {
      return FS_MMC_CARD_WRITE_GENERIC_ERROR;
    }
    if (GPDMA_RAW_INT_ERR_STAT & (1uL << DMA_CHANNEL)) {
      return FS_MMC_CARD_WRITE_GENERIC_ERROR;
    }
    if (Status & (1uL << STATUS_DATA_END)) {
      return FS_MMC_CARD_NO_ERROR;        // OK, data sent.
    }
  } while (1);
}

/*********************************************************************
*
*       Public code
*
**********************************************************************
*/

/*********************************************************************
*
*       FS_MMC_HW_X_SetHWNumBlocks
*
*  Function description
*    Sets the number of block (sectors) to be transferred.
*
*/
void FS_MMC_HW_X_SetHWNumBlocks(U8 Unit, U16 NumBlocks) {
  FS_USE_PARA(Unit);
  _NumBlocks = NumBlocks;
}

/*********************************************************************
*
*       FS_MMC_HW_X_SetHWBlockLen
*
*  Function description
*    Sets the block size (sector size) that shall be transferred.
*/
void FS_MMC_HW_X_SetHWBlockLen(U8 Unit, U16 BlockSize) {
  FS_USE_PARA(Unit);
  _BlockSize = BlockSize;
}

/*********************************************************************
*
*       FS_MMC_HW_X_SetMaxSpeed
*
*  Function description
*    Sets the frequency of the MMC/SD card controller.
*    The frequency is given in kHz.
*    It is called 2 times:
*     1. During card initialization
*        Initialize the frequency to not more than 400kHz.
*        
*     2. After card initialization
*        The CSD register of card is read and the max frequency 
*        the card can operate is determined.
*        [In most cases: MMC cards 20MHz, SD cards 25MHz]
*
*/
U16 FS_MMC_HW_X_SetMaxSpeed(U8 Unit, U16 Freq) {
  U32 SDClock;
  int Div;
  
  FS_USE_PARA(Unit);
  //
  // Limit the clock speed if necessary.
  //
  if (Freq > MAX_SPEED) {
    Freq = MAX_SPEED;
  }
  //
  // Formula: MCLCLK frequency = PCLK / (2 * (ClkDiv+1))
  //
  SDClock = PCLK / 1000;
  Div     = (SDClock + ((2 * Freq) - 1)) / (2 * Freq) - 1;
  if (Div < 0) {
    Div = 0;
  }
  MCI_CLOCK  = (1uL << CLOCK_ENABLE)
             | (1uL << CLOCK_PWR_SAVE)
             | Div
             ;
  Freq = SDClock / (2 * (Div + 1));
  return Freq;
}

/*********************************************************************
*
*       FS_MMC_HW_X_IsPresent
*
*    Returns the state of the media. If you do not know the state, return
*    FS_MEDIA_STATE_UNKNOWN and the higher layer will try to figure out if
*    a media is present.
*
*  Parameters:
*    Unit                 - Device Index
*
*  Return value:
*    FS_MEDIA_STATE_UNKNOWN      - the state of the media is unkown
*    FS_MEDIA_NOT_PRESENT        - no card is present
*    FS_MEDIA_IS_PRESENT         - a card is present
*/
int FS_MMC_HW_X_IsPresent(U8 Unit) {
  FS_USE_PARA(Unit);
// TBD
//  return (PIO_INP_STATE & (1 << 5)) == 0 ? FS_MEDIA_IS_PRESENT : FS_MEDIA_NOT_PRESENT;
  return FS_MEDIA_IS_PRESENT;
}

/*********************************************************************
*
*       FS_MMC_HW_X_IsWriteProtected
*
*  Function description
*    Returns wheter card is write protected or not.
*/
int FS_MMC_HW_X_IsWriteProtected(U8 Unit) {
  FS_USE_PARA(Unit);
// TBD
  return 0;
}

/*********************************************************************
*
*       FS_MMC_HW_X_SetResponseTimeOut
*
*  Function description
*    Sets the reponse time out value given in MMC/SD card cycles.
*
*/
void FS_MMC_HW_X_SetResponseTimeOut(U8 Unit, U32 Value) {
  FS_USE_PARA(Unit);
  FS_USE_PARA(Value);
  // Not configurable.
}

/*********************************************************************
*
*       FS_MMC_HW_X_SetReadDataTimeOut
*
*  Function description
*    Sets the read data time out value given in MMC/SD card cycles.
*/
void FS_MMC_HW_X_SetReadDataTimeOut(U8 Unit, U32 Value) {
  FS_USE_PARA(Unit);
  MCI_DATA_TMR = Value;
}

/*********************************************************************
*
*       FS_MMC_HW_X_SendCmd
*
*  Function description
*    Sends a command to the MMC/SD card.
*/
void FS_MMC_HW_X_SendCmd(U8 Unit, unsigned Cmd, unsigned CmdFlags, unsigned ResponseType, U32 Arg) {
  unsigned ResponseFormat;
  unsigned ldBlockLength;
  U32      ClockReg;

  FS_USE_PARA(Unit);
  switch (ResponseType) {
  //
  //  No response is expected
  //
  case FS_MMC_RESPONSE_FORMAT_NONE:
  default:
    ResponseFormat = 0;
    break;
  //
  //  Short response is expected (48bit)
  //
  case FS_MMC_RESPONSE_FORMAT_R1:
  case FS_MMC_RESPONSE_FORMAT_R3:
     ResponseFormat = 1;
    break;
  //
  //  Long response is expected (136bit)
  //
  case FS_MMC_RESPONSE_FORMAT_R2:
    ResponseFormat = 3;
    break;
  }
  if (ResponseType == FS_MMC_RESPONSE_FORMAT_R3) {
    _IgnoreCRC = 1;
  } else {
    _IgnoreCRC = 0;
  }
  ldBlockLength   = _ld(_BlockSize);
  _DataControlReg = (ldBlockLength << 4);
  //
  // Handle the command flags.
  //
  if (CmdFlags & FS_MMC_CMD_FLAG_DATATRANSFER) {            // Data transfer?
    _DataControlReg |= (1uL << DATA_CTRL_ENABLE)
                    |  (1uL << DATA_CTRL_DMA_ENABLE)
                    ;
    if ((CmdFlags & FS_MMC_CMD_FLAG_WRITETRANSFER) == 0) {  // Read transfer?
      U32   NumBytesToRead;
      U32 * p;

      _DataControlReg |= 1uL << DATA_CTRL_DIRECTION;
      NumBytesToRead  = _BlockSize * _NumBlocks;
      //
      // The data path must be ready before the read command is sent
      // since the SD card is allowed to send the data at the same time
      // it sends the response to command.
      //
      p = (U32 *)_pBuffer;
      if (_BufferIsAligned(_pBuffer, NumBytesToRead) == 0) {
        p = _pSectorBuffer;
      }
      _StartDMARead(p, NumBytesToRead);
      _StartDataTransfer(NumBytesToRead);
      _NumBytesToRead = NumBytesToRead;
      _pReadBuffer = p;
    }
  }
  ClockReg = MCI_CLOCK;
  if (CmdFlags & FS_MMC_CMD_FLAG_USE_SD4MODE) {
    ClockReg |=  (1uL << CLOCK_WIDE_BUS);                   // Use 4 data lines.
  } else {
    ClockReg &= ~(1uL << CLOCK_WIDE_BUS);                   // Use 1 data line.
  }
  if (CmdFlags & FS_MMC_CMD_FLAG_INITIALIZE) {
    //
    // Let the clock run to allow the SD card to send the last response.
    //
    if (MCI_CLOCK & (1uL << CLOCK_PWR_SAVE)) {
      ClockReg |= (1uL << CLOCK_PWR_SAVE);
  }
    MCI_CLOCK &= ~(1uL << CLOCK_PWR_SAVE);
    _Delay1ms();                                            // The clock register can be re-written after 5 PCLK clock periods.
  }
  MCI_CLOCK    = ClockReg;
  MCI_CLEAR    = 0x7FF;
  MCI_ARGUMENT  = Arg;
  MCI_COMMAND  = (Cmd & 0x3F)
               | (ResponseFormat << COMMAND_RESPONSE)
               | (1uL << COMMAND_ENABLE)
                ;
  //
  // For commands which do not expect a response, wait here for the command to be sent.
  //
  if (ResponseType == FS_MMC_RESPONSE_FORMAT_NONE) {
    do {
      U32 Status;

      Status = MCI_STATUS;
      if (Status & (1uL << STATUS_CMD_SENT)) {
        break;
      }
    } while (1);
  }
}

/*********************************************************************
*
*       FS_MMC_HW_X_GetResponse
*
*  Function description
*    Receives the reponses that was sent by the card after
*    a command was sent to the card.
*/
int FS_MMC_HW_X_GetResponse(U8 Unit, void *pBuffer, U32 Size) {
  unsigned  Status;
  U8      * p;
  int       r;
  int       NumBytes;
 
  FS_USE_PARA(Unit);
  p        = (U8 *)pBuffer;
  NumBytes = Size;
  r        = 0;    // No error so far
  //
  // Wait until command processing is finished
  //
  while (MCI_STATUS & (1uL << STATUS_CMD_ACTIVE)) {
    ;
  }
  do {
    Status = MCI_STATUS;
    if (Status & (1uL << STATUS_CMD_CRC_FAIL) && (_IgnoreCRC == 0)) {
      r = FS_MMC_CARD_RESPONSE_CRC_ERROR;
      break;
    }
    if (Status & (1uL << STATUS_CMD_TIMEOUT)) {
      r = FS_MMC_CARD_RESPONSE_TIMEOUT;
      break;
    }
    if (Status & (1uL << STATUS_CMD_RESP_END) || _IgnoreCRC) {
      volatile U32 * pReg;

      *p++ = MCI_RESP_CMD;
      NumBytes--;
      pReg = (volatile U32 *)(&MCI_RESP0);
      do {
        U32 Data32;

        Data32 = FS_LoadU32BE((const U8 *)pReg);
        FS_StoreU32LE(p, Data32);
        NumBytes -= 4;
        pReg++;
        p += 4;
      } while (NumBytes >= 4);
      break;
    }
  } while (1);
  return r;
}

/*********************************************************************
*
*       FS_MMC_HW_X_ReadData
*
*  Function description
*    Reads data from MMC/SD card via MMC/SD card controller.
*/
int FS_MMC_HW_X_ReadData(U8 Unit, void * pBuffer, unsigned NumBytes, unsigned NumBlocks) {
  int   r; 

  FS_USE_PARA(Unit);
  FS_USE_PARA(NumBytes);
  FS_USE_PARA(NumBlocks);
  r = _WaitForEndOfDMARead();
  if (r == FS_MMC_CARD_NO_ERROR) {
    U32   NumBytesToRead;
  U32 * p;  

    NumBytesToRead = _NumBytesToRead;
    p              = _pReadBuffer;
    OS_ARM_DCACHE_InvalidateRange(p, (NumBytesToRead + (CACHE_LINE_SIZE - 1)) & ~(CACHE_LINE_SIZE - 1));
    if (p != pBuffer) {
      FS_MEMCPY(pBuffer, p, NumBytesToRead);
    }
  }
  return r;
}

/*********************************************************************
*
*       FS_MMC_HW_X_WriteData
*
*  Function description
*    Writes the data to MMC/SD card via MMC/SD card controller.
*         
*/
int FS_MMC_HW_X_WriteData(U8 Unit, const void * pBuffer, unsigned NumBytes, unsigned NumBlocks) {
  U32   NumBytesToWrite;
  int   r; 
  U32 * p;

  FS_USE_PARA(Unit);
  FS_USE_PARA(NumBytes);
  FS_USE_PARA(NumBlocks);
  NumBytesToWrite = _BlockSize * _NumBlocks;
  p = (U32 *)pBuffer;
  if (_BufferIsAligned(pBuffer, NumBytesToWrite) == 0) {
    p = _pSectorBuffer;
    FS_MEMCPY(p, pBuffer, NumBytesToWrite);
  }
  OS_ARM_DCACHE_CleanRange(p, (NumBytesToWrite + (CACHE_LINE_SIZE - 1)) & ~(CACHE_LINE_SIZE - 1));
  _StartDMAWrite(p, NumBytesToWrite);
  _StartDataTransfer(NumBytesToWrite);
  r = _WaitForEndOfDMAWrite();
  return r;
}

/*********************************************************************
*
*       FS_MMC_HW_X_Delay
*
*  Function description
*    Waits for a certain time given by the parameter.
*
*/
void FS_MMC_HW_X_Delay(int ms) {
  do {
    _Delay1ms();
  } while (--ms);
}

/*********************************************************************
*
*       FS_MMC_HW_X_InitHW
*
*  Function description
*    Initialize the MMC/SD card controller.
*/
void FS_MMC_HW_X_InitHW(U8 Unit) {
  U32 NumLoops;
    
  FS_USE_PARA(Unit);
  MCI_POWER  = 0;             // Turn off the power of SD card.
  MCI_CLOCK  = 0;             // Disable the MCI module.
  MS_CTRL = (4 << 0)           // Set SD input clock diver to 4
          | (1 << 5)           // Enable SD clock 
          | (0 << 6)           // Enable pull-up on SD DAT0
          | (0 << 7)           // Enable pull-up on SD DAT1
          | (0 << 8)           // Enable pull-up on SD DAT2
          | (1 << 9)           // Enable pull-ups on SD DAT pins
          ;
  //
  // Initialize the MCI module.
  //
  MCI_MASK0 = 0;
  MCI_MASK1 = 0;
  MCI_CLEAR = 0x7FF;    // Clear all pending interrupts
  MCI_COMMAND = 0;
  MCI_DATA_CTRL = 0;      
  MCI_POWER     = 0x02;     // Power up
  //
  // Wait or the SD supply voltage to stabilize.
  //
  NumLoops = 20;
  do {
    _Delay1ms();
  } while (--NumLoops);
  MCI_POWER = 0x03;     // Power on
  _DMAInit();
  _pSectorBuffer = (U32 *)(((U32)_aSectorBuffer + (CACHE_LINE_SIZE - 1)) & ~(CACHE_LINE_SIZE - 1));
}

/*********************************************************************
*
*       FS_MMC_HW_X_GetMaxReadBurst
*
*  Function description
*    Returns the number of block (sectors)
*    that can be read at once with a single
*    READ_MULTIPLE_SECTORS.
*
*/
U16 FS_MMC_HW_X_GetMaxReadBurst(U8 Unit) {
  FS_USE_PARA(Unit);
  return NUM_SECTORS_AT_ONCE;
}

/*********************************************************************
*
*       FS_MMC_HW_X_GetMaxWriteBurst
*
*  Function description
*    Returns the number of block (sectors)
*    that can be written at once with a single
*    WRITE_MULTIPLE_SECTORS.
*/
U16 FS_MMC_HW_X_GetMaxWriteBurst(U8 Unit) {
  FS_USE_PARA(Unit);
  return NUM_SECTORS_AT_ONCE;
}

/*********************************************************************
*
*       FS_MMC_HW_X_SetDataPointer
*
*  Function description
*    Tells the hardware layer where to read data from 
*    or write data to. This may be necessary for some controller,
*    before sending the command to the card, eg. programming the DMA.
*    In most cases this function can be left empty.
*
*  Parameters:
*    Unit          - SD card controller no, in case there are more than one.
*
*/
void FS_MMC_HW_X_SetDataPointer(U8 Unit, const void * p) {
  FS_USE_PARA(Unit);
  _pBuffer = (void *)p;
}

/*************************** End of file ****************************/
