/*********************************************************************
*                SEGGER MICROCONTROLLER GmbH & Co. KG                *
*        Solutions for real time microcontroller applications        *
**********************************************************************
*                                                                    *
*        (c) 2003-2012     SEGGER Microcontroller GmbH & Co KG       *
*                                                                    *
*        Internet: www.segger.com    Support:  support@segger.com    *
*                                                                    *
**********************************************************************

**** emFile file system for embedded applications ****
emFile is protected by international copyright laws. Knowledge of the
source code may not be used to write a similar product. This file may
only be used in accordance with a license and should not be re-
distributed in any way. We appreciate your understanding and fairness.
----------------------------------------------------------------------
----------------------------------------------------------------------
File        : FS_MMC_HW_CM_AT91SAM3U.c
Purpose     : Low level MMC/SD driver for the Atmel AT91SAM3U (card mode)
Literature  : [1] "\\fileserver\techinfo\Company\Atmel\MCU\ARM\AT91SAM3U\SAM3U Datasheet\SAM3U_Manual_090901.pdf"
              [2] \\fileserver\techinfo\Company\Atmel\MCU\ARM\AT91SAM3U\SAM3U-EKs
---------------------------END-OF-HEADER------------------------------
*/

/*********************************************************************
*
*       Includes
*
**********************************************************************
*/
#include "FS.h"
#include "FS_Int.h"
#include "MMC_SD_CardMode_X_HW.h"

/*********************************************************************
*
*       Defines configurable
*
**********************************************************************
*/
#define USE_DMA               1
#define FSYS                  96000000uL  // CPU speed in Hz
#define DEFAULT_SPEED         400         // Default communication speed (kHz)
#define DEFAULT_TIMEOUT       0x7FuL      // Card cycles (see the description of Data Timeout Register)
#if USE_DMA
#define NUM_BUFFERS           8           // Maximum number of DMA buffer descriptors
#define CHANNEL_INDEX         0           // Index of DMA channel to use for data transfer (0-3)
#define MAX_SECTOR_SIZE       512
#else
#define NUM_BLOCKS_AT_ONCE    65535       // Maximum number of blocks in a data transfer
#endif // USE_DMA

/*********************************************************************
*
*       Defines non configurable
*
**********************************************************************
*/
/*********************************************************************
*
*       Local defines (sfrs)
*
**********************************************************************
*/
//
// High Speed MultiMedia Card Interface
//
#define HSMCI_BASE_ADDR     0x40000000
#define HSMCI_CR            (*(volatile U32 *)(HSMCI_BASE_ADDR + 0x00))   // Control Register
#define HSMCI_MR            (*(volatile U32 *)(HSMCI_BASE_ADDR + 0x04))   // Mode Register
#define HSMCI_DTOR          (*(volatile U32 *)(HSMCI_BASE_ADDR + 0x08))   // Data Timeout Register
#define HSMCI_SDCR          (*(volatile U32 *)(HSMCI_BASE_ADDR + 0x0C))   // SD/SDIO Card Register
#define HSMCI_ARGR          (*(volatile U32 *)(HSMCI_BASE_ADDR + 0x10))   // Argument Register
#define HSMCI_CMDR          (*(volatile U32 *)(HSMCI_BASE_ADDR + 0x14))   // Command Register
#define HSMCI_BLKR          (*(volatile U32 *)(HSMCI_BASE_ADDR + 0x18))   // Block Register
#define HSMCI_CSTOR         (*(volatile U32 *)(HSMCI_BASE_ADDR + 0x1C))   // Completion Signal Timeout Register
#define HSMCI_RSPR          (*(volatile U32 *)(HSMCI_BASE_ADDR + 0x20))   // Response Register
#define HSMCI_RDR           (*(volatile U32 *)(HSMCI_BASE_ADDR + 0x30))   // Receive Data Register
#define HSMCI_TDR           (*(volatile U32 *)(HSMCI_BASE_ADDR + 0x34))   // Transmit Data Register
#define HSMCI_SR            (*(volatile U32 *)(HSMCI_BASE_ADDR + 0x40))   // Status Register
#define HSMCI_IER           (*(volatile U32 *)(HSMCI_BASE_ADDR + 0x44))   // Interrupt Enable Register
#define HSMCI_IDR           (*(volatile U32 *)(HSMCI_BASE_ADDR + 0x48))   // Interrupt Disable Register
#define HSMCI_IMR           (*(volatile U32 *)(HSMCI_BASE_ADDR + 0x4C))   // Interrupt Mask Register
#define HSMCI_DMA           (*(volatile U32 *)(HSMCI_BASE_ADDR + 0x50))   // DMA Configuration Register
#define HSMCI_CFG           (*(volatile U32 *)(HSMCI_BASE_ADDR + 0x54))   // Configuration Register
#define HSMCI_WPMR          (*(volatile U32 *)(HSMCI_BASE_ADDR + 0xE4))   // Write Protection Mode Register
#define HSMCI_WPSR          (*(volatile U32 *)(HSMCI_BASE_ADDR + 0xE8))   // Write Protection Status Register
#define HSMCI_FIFO          (*(volatile U32 *)(HSMCI_BASE_ADDR + 0x200))  // FIFO Memory Aperture
#define HSMCI_SIZEOF_FIFO   (0x4000 - 0x200)                              // Size of the FIFO in bytes

//
// DMA Controller
//
#define DMAC_BASE_ADDR      0x400B0000
#define DMAC_EN             (*(volatile U32 *)(DMAC_BASE_ADDR + 0x04))          // DMAC Enable Register
#define DMAC_EBCISR         (*(volatile U32 *)(DMAC_BASE_ADDR + 0x24))          // DMAC Error, Chained Buffer transfer completed and Buffer transfer completed Status Register
#define DMAC_CHER           (*(volatile U32 *)(DMAC_BASE_ADDR + 0x28))          // Channel Handler Enable Register
#define DMAC_CHDR           (*(volatile U32 *)(DMAC_BASE_ADDR + 0x2C))          // Channel Handler Disable Register
#define DMAC_CHSR           (*(volatile U32 *)(DMAC_BASE_ADDR + 0x30))          // Channel Handler Status Register
#define DMAC_CHANNEL_BASE_ADDR  (DMAC_BASE_ADDR + 0x3C + (CHANNEL_INDEX * 0x28))
#define DMAC_SADDRx         (*(volatile U32 *)(DMAC_CHANNEL_BASE_ADDR + 0x00))  // DMAC Channel Source Address Register
#define DMAC_DADDRx         (*(volatile U32 *)(DMAC_CHANNEL_BASE_ADDR + 0x04))  // DMAC Channel Destination Address Register
#define DMAC_DSCRx          (*(volatile U32 *)(DMAC_CHANNEL_BASE_ADDR + 0x08))  // DMAC Channel Destination Address Register
#define DMAC_CTRLAx         (*(volatile U32 *)(DMAC_CHANNEL_BASE_ADDR + 0x0C))  // DMAC Channel Control A Register
#define DMAC_CTRLBx         (*(volatile U32 *)(DMAC_CHANNEL_BASE_ADDR + 0x10))  // DMAC Channel Control B Register
#define DMAC_CFGx           (*(volatile U32 *)(DMAC_CHANNEL_BASE_ADDR + 0x14))  // DMAC Channel Configuration Register

//
// Parallel Input/Output (The output pins of HSMCI are mapped on Port A)
//
#define PIOA_BASE_ADDR      0x400E0C00
#define PIOA_PER            (*(volatile U32 *)(PIOA_BASE_ADDR + 0x00))    // Port A Enable Register
#define PIOA_PDR            (*(volatile U32 *)(PIOA_BASE_ADDR + 0x04))    // Port A Disable Register
#define PIOA_PDSR           (*(volatile U32 *)(PIOA_BASE_ADDR + 0x3C))    // Controller Pin Data Status Register
#define PIOA_PUDR           (*(volatile U32 *)(PIOA_BASE_ADDR + 0x60))    // Pull-up Disable Register
#define PIOA_ABSR           (*(volatile U32 *)(PIOA_BASE_ADDR + 0x70))    // Peripheral AB Select Register

//
// Parallel Input/Output (The output pins of HSMCI are mapped also on Port C)
//
#define PIOC_BASE_ADDR      0x400E1000
#define PIOC_PER            (*(volatile U32 *)(PIOC_BASE_ADDR + 0x00))    // Port C Enable Register
#define PIOC_PDR            (*(volatile U32 *)(PIOC_BASE_ADDR + 0x04))    // Port C Disable Register
#define PIOC_PDSR           (*(volatile U32 *)(PIOC_BASE_ADDR + 0x3C))    // Controller Pin Data Status Register
#define PIOC_PUDR           (*(volatile U32 *)(PIOC_BASE_ADDR + 0x60))    // Pull-up Disable Register
#define PIOC_ABSR           (*(volatile U32 *)(PIOC_BASE_ADDR + 0x70))    // Peripheral AB Select Register

//
// Power Management Controller
//
#define PMC_BASE_ADDR       0x400E0400
#define PMC_PCER            (*(volatile U32 *)(PMC_BASE_ADDR + 0x10))     // Peripheral Clock Enable Register

//
// HSMCI Control Register
//
#define CR_MCIEN            (1uL << 0)    // Multi-Media Interface Enable
#define CR_MCIDIS           (1uL << 1)    // Multi-Media Interface Disable
#define CR_PWSDIS           (1uL << 3)    // Disable power down mode
#define CR_SWRST            (1uL << 7)    // Software Reset

//
// HSMCI Mode Register
//
#define MR_RDPROOF          (1uL << 11)   // Read Proof Enable
#define MR_WRPROOF          (1uL << 12)   // Write Proof Enable
#define MR_FBYTE            (1uL << 13)   // Force Byte Transfer

//
// HSMCI Command Register
//
#define CMDR_CMDNB_MASK     0x3F          // Command number
#define CMDR_RSPTYP_NONE    (0uL << 6)    // No response type
#define CMDR_RSPTYP_48BIT   (1uL << 6)    // 48-bit response type
#define CMDR_RSPTYP_136BIT  (2uL << 6)    // 136-bit response type
#define CMDR_SPCMD_INIT     (1uL << 8)    // Initialisation command
#define CMDR_SPCMD_SYNC     (2uL << 8)    // Wait for the end of last command
#define CMDR_OPCMD          (1uL << 11)   // Open drain/push pull
#define CMDR_MAXLAT         (1uL << 12)   // Max Latency for Command to Response
#define CMDR_TRCMD_MASK     (3uL << 16)   // Type of transfer command
#define CMDR_TRCMD_NONE     (0uL << 16)   // No transfer command
#define CMDR_TRCMD_START    (1uL << 16)   // Start transfer
#define CMDR_TRCMD_STOP     (2uL << 16)   // Stop transfer
#define CMDR_TRDIR          (1uL << 18)   // Transfer direction
#define CMDR_TRTYP_MULTI    (1uL << 19)   // SD Multiple block transfer

//
// HSMCI Status Register
//
#define SR_CMDRDY           (1uL << 0)    // Command Ready
#define SR_RXRDY            (1uL << 1)    // Receiver Ready
#define SR_TXRDY            (1uL << 2)    // Transmitter Ready
#define SR_DTIP             (1uL << 4)    // Data transfer in progress
#define SR_NOTBUSY          (1uL << 5)    // Not busy
#define SR_RINDE            (1uL << 16)   // Response Index Error
#define SR_RDIRE            (1uL << 17)   // Response Direction Error
#define SR_RCRCE            (1uL << 18)   // Response CRC Error
#define SR_RENDE            (1uL << 19)   // Response End Bit Error
#define SR_RTOE             (1uL << 20)   // Response Timeout Error
#define SR_DCRCE            (1uL << 21)   // Data CRC Error
#define SR_DTOE             (1uL << 22)   // Data Time-out Error
#define SR_CSTOE            (1uL << 23)   // Completion Signal Time-out Error
#define SR_BLKOVRE          (1uL << 24)   // DMA Block Overrun Error
#define SR_DMADONE          (1uL << 25)   // DMA Transfer Done
#define SR_FIFOEMPTY        (1uL << 26)   // Fifo empty flag
#define SR_XFRDONE          (1uL << 27)   // Transfer Done Flag
#define SR_OVRE             (1uL << 30)   // Overrun
#define SR_UNRE             (1uL << 31)   // Underrun

//
// HSMCI SDCard/SDIO Register
//
#define SDCR_SDCBUS_MASK    (3uL << 6)
#define SDCR_SDCBUS_1BIT    (0uL << 6)
#define SDCR_SDCBUS_4BIT    (2uL << 6)
#define SDCR_SDCBUS_8BIT    (3uL << 6)

//
// HSMCI DMA Configuration Register
//
#define DMA_CHKSIZE         (1uL << 4)    // Channel Read and Write Chunk Size
#define DMA_DMAEN           (1uL << 8)    // DMA Hardware Hanshaking Enable
#define DMA_ROPT            (1uL << 12)   // Read Optimisation with Padding

//
// HSMCI Configuration Register
//
#define CFG_FIFOMODE        (1uL << 0)    // Internal FIFO Control Mode
#define CFG_FERRCTRL        (1uL << 4)    // Flow Error Flag Reset Control Mode
#define CFG_HSMODE          (1uL << 8)    // High Speed Mode

//
// HSMCI port pins
//
#define PIOA_MCCK           (1uL << 3)    // Clock
#define PIOA_MCCDA          (1uL << 4)    // Command
#define PIOA_MCDA0          (1uL << 5)    // D0
#define PIOA_MCDA1          (1uL << 6)    // D1
#define PIOA_MCDA2          (1uL << 7)    // D2
#define PIOA_MCDA3          (1uL << 8)    // D3
#define PIOA_CD             (1uL << 25)   // Card detect
#define PIOC_MCDA4          (1uL << 28)   // D4
#define PIOC_MCDA5          (1uL << 29)   // D5
#define PIOC_MCDA6          (1uL << 30)   // D6
#define PIOC_MCDA7          (1uL << 31)   // D7

//
// DMAC Control A Register
//
#define CTRLA_BCSIZE_MASK     0xFFFuL
#define CTRLA_SCSIZE          (1uL << 16)
#define CTRLA_DCSIZE          (1uL << 20)
#define CTRLA_DST_WITDH_WORD  (2uL << 28)
#define CTRLA_SRC_WITDH_WORD  (2uL << 24)

//
// DMAC Control B Register
//
#define CTRLB_SRC_DSCR        (1uL << 16)
#define CTRLB_DST_DSCR        (1uL << 20)
#define CTRLB_FC_M2P          (1uL << 21)
#define CTRLB_FC_P2M          (2uL << 21)

//
// DMAC Configuration Register
//
#define CFG_SRC_H2SEL         (1uL << 9)
#define CFG_DST_H2SEL         (1uL << 13)

//
// Peripheral identifiers
//
#define PIOA_ID               10
#define PIOC_ID               12
#define HSMCI_ID              17
#define DMAC_ID               28

#if USE_DMA
//
// Multiple DMA tansfers use a linked list of this structures
// to configure a single transfer
//
typedef struct DMA_LLI {
  U32 SrcAddr;
  U32 DestAddr;
  U32 CtrlA;          // Value loaded in the register DMAC_CTRLA(x)
  U32 CtrlB;          // Value loaded in the register DMAC_CTRLB(x)
  U32 Descriptor;     // A zero is stored here to mark the last transfer
} DMA_LLI;
#endif // USE_DMA

/*********************************************************************
*
*       Static data
*
**********************************************************************
*/
static int        _IgnoreCRC;   // Remembers that we should ignore CRC errors in case of an R3 response
static U16        _BlockSize;   // Size of the block to transfer as set by the upper layer
static U16        _NumBlocks;   // Number of blocks to transfer as set by the upper layer
#if USE_DMA
static const U8 * _pBuffer;     // Points to the memory buffer a DMA transfer should use
static DMA_LLI    _aLLI[NUM_BUFFERS];   // Linked list of DMA buffer descriptors
#endif // USE_DMA

/*********************************************************************
*
*       Static code
*
**********************************************************************
*/

/*********************************************************************
*
*       _ComputeClockDivisor
*
*   Function description
*     Computes the divisor value to get the given SD clock frequency
*     from the given peripheral clock.
*
*   Parameters:
*     PClk  The peripheral frequency in kHz
*     SDClk The SD card frequency in kHz
*
*   Return value
*     The computed divisor value
*/
static U32 _ComputeClockDivisor(U32 PClk, U32 SDClk) {
  U32 ClkDiv;

  if (PClk < SDClk) {
    return 0;
  } else {
    //
    // The peripheral clock is divided by 2 * (ClkDiv + 1)
    //
    // Example:
    //
    // Peripheral clock 96MHz
    // ClkDiv = 0   Speed = 96 / 2 * (0 + 1) = 48MHz
    // ClkDiv = 5   Speed = 96 / 2 * (5 + 1) = 9.6MHz
    //
    ClkDiv = (PClk + SDClk) / (SDClk * 2);
    if (ClkDiv > 0) {
      --ClkDiv;
    }
    return ClkDiv;
  }
}

/*********************************************************************
*
*       _WaitForResponse
*
*   Function description
*     Waits for a command response to arrive.
*
*   Return values
*     FS_MMC_CARD_NO_ERROR                Success
*     FS_MMC_CARD_RESPONSE_CRC_ERROR      CRC error in response
*     FS_MMC_CARD_RESPONSE_TIMEOUT        No response received
*     FS_MMC_CARD_RESPONSE_GENERIC_ERROR  Any other error
*/
static int _WaitForResponse(void) {
  U32  Status;

  //
  // Wait for command to finish
  //
  do {
    Status = HSMCI_SR;
  } while (!(Status & SR_CMDRDY));
  if ((Status & SR_RCRCE) && !_IgnoreCRC) {   // CRC error in response
    return FS_MMC_CARD_RESPONSE_CRC_ERROR;
  } else {
    if (Status & SR_RINDE) {                  // Command index mismatch
      return FS_MMC_CARD_RESPONSE_GENERIC_ERROR;
    }
    if (Status & SR_RDIRE) {                  // Transmission bit not cleared
      return FS_MMC_CARD_RESPONSE_GENERIC_ERROR;
    }
  }
  if (Status & SR_RENDE) {                    // End bit is cleared
    return FS_MMC_CARD_RESPONSE_GENERIC_ERROR;
  }
  if (Status & SR_RTOE) {                     // No response received
    return FS_MMC_CARD_RESPONSE_TIMEOUT;
  }
  return FS_MMC_CARD_NO_ERROR;
}

/*********************************************************************
*
*       _WaitForRxReady
*
*   Function description
*     Waits until new data is received. The function also returns
*     in case of a receiving error.
*
*   Return values
*     FS_MMC_CARD_NO_ERROR              Success
*     FS_MMC_CARD_WRITE_GENERIC_ERROR   Any other error
*/
#if !USE_DMA
static int _WaitForRxReady(void) {
  U32 Status;

  while (1) {
    Status = HSMCI_SR;
    if (Status & SR_DCRCE) {
      return FS_MMC_CARD_READ_CRC_ERROR;
    }
    if (Status & SR_OVRE) {
      return FS_MMC_CARD_READ_GENERIC_ERROR;
    }
    if (Status & SR_DTOE) {
      return FS_MMC_CARD_READ_TIMEOUT;
    }
    if (Status & SR_CSTOE) {
      return FS_MMC_CARD_READ_TIMEOUT;
    }
    if (Status & SR_RXRDY) {
      return FS_MMC_CARD_NO_ERROR;
    }
  }
}
#endif // !USE_DMA

/*********************************************************************
*
*       _WaitForTxReady
*
*   Function description
*     Waits until the transmitter is ready to send new data.
*     The function retuns in case of an underrun condition.
*     This can happen when we are not able to deliver the data fast enough.
*
*   Return values
*     FS_MMC_CARD_NO_ERROR              Success
*     FS_MMC_CARD_WRITE_GENERIC_ERROR   Any other error
*/
#if !USE_DMA
static int _WaitForTxReady(void) {
  U32 Status;

  while (1) {
    Status = HSMCI_SR;
    if (Status & SR_UNRE) {
      return FS_MMC_CARD_WRITE_GENERIC_ERROR;
    }
    if (Status & SR_DCRCE) {
      return FS_MMC_CARD_READ_CRC_ERROR;
    }
    if (Status & SR_TXRDY) {
      return FS_MMC_CARD_NO_ERROR;
    }
  }
}
#endif // !USE_DMA

/*********************************************************************
*
*       _WaitForCmdReady
*
*   Function description
*     Waits until for the last command to finish.
*/
static void _WaitForCmdReady(void) {
  //
  // The check for the busy flag is here to ensure
  // that a previous write transfer command has finished
  //
  while ((HSMCI_SR & (SR_CMDRDY | SR_NOTBUSY)) != (SR_CMDRDY | SR_NOTBUSY)) {
    ;
  }
}

/*********************************************************************
*
*       _Delay1ms
*
*   Function description
*     Busy loops for about 1 millisecond.
*/
static void _Delay1ms(void) {
  volatile int i;

  //
  // This loop takes about 1ms at 96MHz
  //
  for (i = 0; i < 0x1FFF; ++i) {
    ;
  }
}

/*********************************************************************
*
*       _EnableDMAChannel
*
*   Function description
*     Enables the DMA channel used for the data transfer.
*
*/
#if USE_DMA
static void _EnableDMAChannel(void) {
  U32 Mask;

  Mask = 1 << CHANNEL_INDEX;
  DMAC_CHER = Mask;
}
#endif // USE_DMA

/*********************************************************************
*
*       _DisableDMAChannel
*
*   Function description
*     Disables the DMA channel used for the data transfer.
*
*/
#if USE_DMA
static void _DisableDMAChannel(void) {
  U32 Mask;

  Mask = 1 << CHANNEL_INDEX;
  DMAC_CHDR = Mask;
  while (DMAC_CHSR & Mask) {
    ;
  }
}
#endif // USE_DMA

/*********************************************************************
*
*       _WaitForEndOfDMAWrite
*
*   Function description
*     Waits for DMA to move data from memory to HSMCI.
*
*   Return values
*     FS_MMC_CARD_NO_ERROR      Success
*     FS_MMC_CARD_READ_TIMEOUT  No data received
*/
#if USE_DMA
static int _WaitForEndOfDMAWrite(void) {
  U32 Status;

  while (1) {
    Status = HSMCI_SR;
    if (Status & SR_DTOE) {
      return FS_MMC_CARD_READ_TIMEOUT;
    }
    if (Status & SR_XFRDONE) {
      return FS_MMC_CARD_NO_ERROR;
    }
  }
}
#endif

/*********************************************************************
*
*       _WaitForEndOfDMARead
*
*   Function description
*     Waits for the DMA to move data from HSMCI into memory.
*
*   Return values
*     FS_MMC_CARD_NO_ERROR            Success
*     FS_MMC_CARD_READ_CRC_ERROR      CRC error in received data
*     FS_MMC_CARD_READ_TIMEOUT        No data received
*     FS_MMC_CARD_READ_GENERIC_ERROR  Any other error
*/
#if USE_DMA
static int _WaitForEndOfDMARead(void) {
  U32 Status;

  while (1) {
    Status = HSMCI_SR;
    if (Status & SR_DCRCE) {
      return FS_MMC_CARD_READ_CRC_ERROR;
    }
    if (Status & SR_BLKOVRE) {
      return FS_MMC_CARD_READ_GENERIC_ERROR;
    }
    if (Status & SR_DTOE) {
      return FS_MMC_CARD_READ_TIMEOUT;
    }
    if (Status & SR_XFRDONE) {
      return FS_MMC_CARD_NO_ERROR;
    }
  }
}
#endif

/*********************************************************************
*
*       _StartDMARead
*
*   Function description
*     Starts a DMA transfer to move the data from the HSMCI into
*     the read buffer. This function follows the steps described
*     in [1] for setting up a DMA transfer.
*
*/
#if USE_DMA
static void _StartDMARead(void) {
  volatile U32   Dummy;
  U32            NumBytes;
  U32            NumBuffers;
  U32            NumBytesToRead;
  const U8     * p;
  DMA_LLI      * pLLI;

  _DisableDMAChannel();
  Dummy      = DMAC_EBCISR;      // Discard any pending interrupts
  FS_USE_PARA(Dummy);
  NumBytes   = (U32)_NumBlocks * (U32)_BlockSize;
  NumBuffers = (NumBytes + HSMCI_SIZEOF_FIFO - 1) / HSMCI_SIZEOF_FIFO;
  NumBuffers = MIN(NumBuffers, COUNTOF(_aLLI));
  pLLI       = _aLLI;
  p          = _pBuffer;
  //
  // Each transfered block uses a buffer descriptor from the list
  //
  do {
    if (NumBytes < HSMCI_SIZEOF_FIFO) {
      NumBytesToRead = NumBytes;
    } else {
      NumBytesToRead = HSMCI_SIZEOF_FIFO;
    }
    pLLI->SrcAddr  = (U32)&HSMCI_FIFO;
    pLLI->DestAddr = (U32)p;
    pLLI->CtrlA    = (CTRLA_BCSIZE_MASK & (NumBytesToRead / 4))
                   | CTRLA_SCSIZE         // Same as DMA_CHKSIZE
                   | CTRLA_DST_WITDH_WORD
                   | CTRLA_SRC_WITDH_WORD
                   ;
    pLLI->CtrlB    = CTRLB_DST_DSCR       // Destination address is continuous
                   | CTRLB_FC_P2M         // Peripheral to memory transfer
                   ;
    if (NumBuffers == 1) {
      pLLI->CtrlB      |= CTRLB_SRC_DSCR; // Do not update the source address on descriptor fetch
      pLLI->Descriptor  = 0;              // End of list
    } else {
      pLLI->Descriptor  = (U32)(pLLI + 1);
    }
    NumBytes -= NumBytesToRead;
    p        += NumBytesToRead;
    ++pLLI;
  } while (--NumBuffers);
  DMAC_DSCRx  = (U32)_aLLI;
  DMAC_CTRLAx = 0;
  DMAC_CTRLBx = CTRLB_FC_P2M;
  DMAC_CFGx   = CFG_SRC_H2SEL;            // Pheripheral triggers the transfer
  DMAC_EN     = 1;                        // Global DMA enable
  _EnableDMAChannel();
}
#endif // USE_DMA

/*********************************************************************
*
*       _StartDMAWrite
*
*   Function description
*     Starts a DMA transfer to move the data from the write buffer
*     into HSMCI.
*/
#if USE_DMA
static void _StartDMAWrite(void) {
  volatile U32   Dummy;
  U32            NumBytes;
  U32            NumBuffers;
  U32            NumBytesToWrite;
  const U8     * p;
  DMA_LLI      * pLLI;

  _DisableDMAChannel();
  Dummy      = DMAC_EBCISR;      // Discard any pending interrupts
  FS_USE_PARA(Dummy);
  NumBytes   = (U32)_NumBlocks * (U32)_BlockSize;
  NumBuffers = (NumBytes + HSMCI_SIZEOF_FIFO - 1) / HSMCI_SIZEOF_FIFO;
  NumBuffers = MIN(NumBuffers, COUNTOF(_aLLI));
  pLLI       = _aLLI;
  p          = _pBuffer;
  //
  // Each transfered block uses a buffer descriptor from the list
  //
  do {
    if (NumBytes < HSMCI_SIZEOF_FIFO) {
      NumBytesToWrite = NumBytes;
    } else {
      NumBytesToWrite = HSMCI_SIZEOF_FIFO;
    }
    pLLI->SrcAddr  = (U32)p;
    pLLI->DestAddr = (U32)&HSMCI_FIFO;
    pLLI->CtrlA    = (CTRLA_BCSIZE_MASK & (NumBytesToWrite / 4))
                   | CTRLA_DCSIZE         // Same as DMA_CHKSIZE
                   | CTRLA_DST_WITDH_WORD
                   | CTRLA_SRC_WITDH_WORD
                   ;
    pLLI->CtrlB    = CTRLB_SRC_DSCR       // Source address is continuous
                   | CTRLB_FC_M2P         // Memory to peripheral transfer
                   ;
    if (NumBuffers == 1) {
      pLLI->CtrlB      |= CTRLB_DST_DSCR; // Do not update the destination address on descriptor fetch
      pLLI->Descriptor  = 0;              // End of list
    } else {
      pLLI->Descriptor  = (U32)(pLLI + 1);
    }
    NumBytes -= NumBytesToWrite;
    p        += NumBytesToWrite;
    ++pLLI;
  } while (--NumBuffers);
  DMAC_DSCRx  = (U32)_aLLI;
  DMAC_CTRLAx = 0;
  DMAC_CTRLBx = CTRLB_FC_M2P;
  DMAC_CFGx   = CFG_DST_H2SEL;            // Pheripheral triggers the transfer
  DMAC_EN     = 1;                        // Global DMA enable
  _EnableDMAChannel();
}
#endif // USE_DMA

/*********************************************************************
*
*       Public code
*
**********************************************************************
*/

/*********************************************************************
*
*       FS_MMC_HW_X_SetHWNumBlocks
*
*  Function description
*    Sets the number of block (sectors) to be transferred.
*
*/
void FS_MMC_HW_X_SetHWNumBlocks(U8 Unit, U16 NumBlocks) {
  FS_USE_PARA(Unit);
  _NumBlocks = NumBlocks;
}

/*********************************************************************
*
*       FS_MMC_HW_X_SetHWBlockLen
*
*   Function description
*    Sets the block size (sector size) that shall be transferred.
*
*   Note
     Block size *not* multiple of 4 bytes are allowed only
*    if FBYTE in the HSMCI_MR is set.
*/
void FS_MMC_HW_X_SetHWBlockLen(U8 Unit, U16 BlockSize) {
  FS_USE_PARA(Unit);
  _BlockSize = BlockSize;
}

/*********************************************************************
*
*       FS_MMC_HW_X_SetMaxSpeed
*
*  Function description
*    Sets the frequency of the MMC/SD card controller.
*    The frequency is given in kHz.
*    It is called 2 times:
*     1. During card initialization
*        Initialize the frequency to not more than 400kHz.
*
*     2. After card initialization
*        The CSD register of card is read and the max frequency
*        the card can operate is determined.
*        [In most cases: MMC cards 20MHz, SD cards 25MHz]
*
*/
U16 FS_MMC_HW_X_SetMaxSpeed(U8 Unit, U16 Freq) {
  U32 PClk;
  U32 ClkDiv;

  FS_USE_PARA(Unit);
  PClk = FSYS / 1000;   // Convert to kHz
  if (PClk < Freq) {
    Freq = PClk;
  } else {
    ClkDiv = _ComputeClockDivisor(PClk, Freq);
    HSMCI_CR = CR_MCIDIS;
    HSMCI_MR &= ~0xFF;
    HSMCI_MR |= ClkDiv & 0xFF;
    HSMCI_CR = CR_MCIEN;
  }
  //
  // In the high-speed mode the clock frequency is always bigger than 25MHz
  //
  if (Freq > 25000) {
    HSMCI_CFG |= CFG_HSMODE;
  } else {
    HSMCI_CFG &= ~CFG_HSMODE;
  }
  return Freq;
}

/*********************************************************************
*
*       FS_MMC_HW_X_IsPresent
*
*    Returns the state of the media. If you do not know the state, return
*    FS_MEDIA_STATE_UNKNOWN and the higher layer will try to figure out if
*    a media is present.
*
*  Parameters:
*    Unit                 - Device Index
*
*  Return value:
*    FS_MEDIA_STATE_UNKNOWN      - the state of the media is unkown
*    FS_MEDIA_NOT_PRESENT        - no card is present
*    FS_MEDIA_IS_PRESENT         - a card is present
*/
int FS_MMC_HW_X_IsPresent(U8 Unit) {
  FS_USE_PARA(Unit);
  //
  // Specific to SAM3U-EK board
  //
  PIOA_PER = PIOA_CD;
  if (PIOA_PDSR & PIOA_CD) {
    return FS_MEDIA_NOT_PRESENT;
  } else {
    return FS_MEDIA_IS_PRESENT;
  }
}

/*********************************************************************
*
*       FS_MMC_HW_X_IsWriteProtected
*
*  Function description
*    Returns wheter card is write protected or not.
*/
int FS_MMC_HW_X_IsWriteProtected(U8 Unit) {
  FS_USE_PARA(Unit);
  //
  // It looks like the SD_WP pin of the SD Card Socket (see [3]) is
  // not connected to any MCU pin.
  //
  return 0;
}

/*********************************************************************
*
*       FS_MMC_HW_X_SetResponseTimeOut
*
*  Function description
*    Sets the reponse time out value given in MMC/SD card cycles.
*
*/
void FS_MMC_HW_X_SetResponseTimeOut(U8 Unit, U32 Value) {
  FS_USE_PARA(Unit);
  FS_USE_PARA(Value);
  //
  // The hardware has only a short and a long timeout.
  // We always set the long timeout before we send a command.
  //
}

/*********************************************************************
*
*       FS_MMC_HW_X_SetReadDataTimeOut
*
*  Function description
*    Sets the read data time out value given in MMC/SD card cycles.
*/
void FS_MMC_HW_X_SetReadDataTimeOut(U8 Unit, U32 Value) {
  FS_USE_PARA(Unit);
  FS_USE_PARA(Value);
  //
  // We always use the maximum timeout here
  //
}

/*********************************************************************
*
*       FS_MMC_HW_X_SendCmd
*
*   Function description
*     Sends a command to the MMC/SD card.
*
*   Parameters
*     Unit          Number of physical card controller
*     Cmd           Command number according to [2]
*     CmdFlags      Additional information about the command to execute
*     ResponseType  Type of response as defined in [2]
*     Arg           Command parameter
*/
void FS_MMC_HW_X_SendCmd(U8 Unit, unsigned Cmd, unsigned CmdFlags, unsigned ResponseType, U32 Arg) {
  unsigned CmdRegValue;

  FS_USE_PARA(Unit);
  _WaitForCmdReady();
  CmdRegValue = (Cmd & CMDR_CMDNB_MASK)   // Limit the command to the valid range
              | CMDR_MAXLAT               // Max latency 64-cycles
              ;
  switch (ResponseType) {
  //
  //  No response is expected
  //
  case FS_MMC_RESPONSE_FORMAT_NONE:
  default:
    CmdRegValue |= CMDR_RSPTYP_NONE;
    break;
  //
  //  Short response is expected (48bit)
  //
  case FS_MMC_RESPONSE_FORMAT_R1:
  case FS_MMC_RESPONSE_FORMAT_R3:
    CmdRegValue |= CMDR_RSPTYP_48BIT;
    break;
  //
  //  Long response is expected (136bit)
  //
  case FS_MMC_RESPONSE_FORMAT_R2:
    CmdRegValue |= CMDR_RSPTYP_136BIT;
    break;
  }
  //
  // According to specs the R3 response has no CRC.
  // The hardware sets the CRC error bit when it receives such a response
  // so we have to ignore it.
  //
  if (ResponseType == FS_MMC_RESPONSE_FORMAT_R3) {
    _IgnoreCRC = 1;
  } else {
    _IgnoreCRC = 0;
  }
  HSMCI_BLKR = ((U32)_BlockSize << 16) | (U32)_NumBlocks;
  if (CmdFlags & FS_MMC_CMD_FLAG_DATATRANSFER) {
    CmdRegValue &= ~CMDR_TRCMD_MASK;
    CmdRegValue |= CMDR_TRCMD_START;
    //
    // Start a multiple block transfer if the high-level driver
    // wants to send more than 1 block at a time
    //
    if (_NumBlocks > 1) {
      CmdRegValue |= CMDR_TRTYP_MULTI;
    }
  }
  if (CmdFlags & FS_MMC_CMD_FLAG_STOP_TRANS) {        // Stop the current transfer
    CmdRegValue &= ~CMDR_TRCMD_MASK;
    CmdRegValue |= CMDR_TRCMD_STOP;
  }
  if (!(CmdFlags & FS_MMC_CMD_FLAG_WRITETRANSFER)) {  // Read transfer?
    CmdRegValue |= CMDR_TRDIR;
  }
#if USE_DMA
  if ((CmdFlags & FS_MMC_CMD_FLAG_DATATRANSFER) &&
      !(CmdFlags & FS_MMC_CMD_FLAG_WRITETRANSFER)) {
    _StartDMARead();
  } else {
    _DisableDMAChannel();
  }
#endif // USE_DMA
  HSMCI_SDCR &= ~SDCR_SDCBUS_MASK;
  if (CmdFlags & FS_MMC_CMD_FLAG_USE_MMC8MODE) {        // use 8 data lines?
    HSMCI_SDCR |= SDCR_SDCBUS_8BIT;
  } else if (CmdFlags & FS_MMC_CMD_FLAG_USE_SD4MODE) {  // use 4 data lines?
    HSMCI_SDCR |= SDCR_SDCBUS_4BIT;
  } else {
    HSMCI_SDCR |= SDCR_SDCBUS_1BIT;
  }
  HSMCI_ARGR = Arg;
  HSMCI_CMDR = CmdRegValue;
}

/*********************************************************************
*
*       FS_MMC_HW_X_GetResponse
*
*  Function description
*    Receives the reponses that was sent by the card after
*    a command was sent to the card.
*
*  Parameters
*    Unit     Number of physical card controller
*    pBuffer  User allocated buffer where the response is stored.
*    Size     Size of the buffer in bytes
*
*  Notes
*     (1) The caller expects the whole response as it comes over
*         the wire even though only the payload is used.
*         In other words: if the card controller delivers only the
*         reponse payload you should store this value starting at byte
*         index 1. Byte index 0 is the start bit, transmission bit
*         and the 6 stuff bits (see [2] chapter 4.9).
*
*/
int FS_MMC_HW_X_GetResponse(U8 Unit, void * pBuffer, U32 Size) {
  U8 * p;
  U32  NumWords;
  U32  Value;
  U32  i;
  int  r;

  FS_USE_PARA(Unit);
  r = _WaitForResponse();
  if (r != FS_MMC_CARD_NO_ERROR) {
    return r;
  }
  //
  // The size we get from the upper driver is total response size in bytes.
  // We compute here the number of read accesses to the 32-bit register which
  // holds the response. Take into account that the first byte of the response
  // is not delivered by the hardware
  //
  NumWords = (Size - 1) / 4;
  p        = pBuffer;
  ++p;    // See note (1)
  for (i = 0; i < NumWords; ++i) {
    Value = HSMCI_RSPR;
    *p = (U8)(Value >> 24);
    ++p;
    *p = (U8)(Value >> 16);
    ++p;
    *p = (U8)(Value >> 8);
    ++p;
    *p = (U8)Value;
    ++p;
  }
  return FS_MMC_CARD_NO_ERROR;
}

/*********************************************************************
*
*       FS_MMC_HW_X_ReadData
*
*   Function description
*     Reads data from MMC/SD card through the MMC/SD card controller.
*
*   Parameters
*     Unit
*     pBuffer
*     NumBytes
*     NumBlocks
*
*   Return values
*     FS_MMC_CARD_NO_ERROR            Success
*     FS_MMC_CARD_READ_CRC_ERROR      CRC error in received data
*     FS_MMC_CARD_READ_TIMEOUT        No data received
*     FS_MMC_CARD_READ_GENERIC_ERROR  Any other error
*
*/
int FS_MMC_HW_X_ReadData(U8 Unit, void * pBuffer, unsigned NumBytes, unsigned NumBlocks) {
#if USE_DMA
  FS_USE_PARA(Unit);
  FS_USE_PARA(pBuffer);
  FS_USE_PARA(NumBytes);
  FS_USE_PARA(NumBlocks);
  return _WaitForEndOfDMARead();
#else
  unsigned   NumWords;
  unsigned   i;
  U8       * p;
  U32        Value;
  int        r;

  FS_USE_PARA(Unit);
  p        = pBuffer;
  NumWords = (NumBytes * NumBlocks) / 4;
  for (i = 0; i < NumWords; ++i) {
    //
    // Wait for data to arrive or an for error to occur
    //
    r = _WaitForRxReady();
    if (r != FS_MMC_CARD_NO_ERROR) {
      return r;
    }
    Value = HSMCI_RDR;
    *p = (U8)Value;
    ++p;
    *p = (U8)(Value >> 8);
    ++p;
    *p = (U8)(Value >> 16);
    ++p;
    *p = (U8)(Value >> 24);
    ++p;
  }
  return FS_MMC_CARD_NO_ERROR;
#endif // USE_DMA
}

/*********************************************************************
*
*       FS_MMC_HW_X_WriteData
*
*   Function description
*     Writes the data to MMC/SD card through the MMC/SD card controller.
*
*   Parameters
*     Unit
*     pBuffer
*     NumBytes
*     NumBlocks
*
*   Return values
*     FS_MMC_CARD_NO_ERROR      Success
*     FS_MMC_CARD_READ_TIMEOUT  No data received
*/
int FS_MMC_HW_X_WriteData(U8 Unit, const void * pBuffer, unsigned NumBytes, unsigned NumBlocks) {
#if USE_DMA
  FS_USE_PARA(Unit);
  FS_USE_PARA(pBuffer);
  FS_USE_PARA(NumBytes);
  FS_USE_PARA(NumBlocks);
  _StartDMAWrite();
  return _WaitForEndOfDMAWrite();
#else
  unsigned   NumWords;
  unsigned   i;
  const U8 * p;
  U32        Value;
  int        r;

  FS_USE_PARA(Unit);
  p        = pBuffer;
  NumWords = (NumBytes * NumBlocks) / 4;
  for (i = 0; i < NumWords; ++i) {
    //
    // Wait for transmitter ready
    //
    r = _WaitForTxReady();
    if (r != FS_MMC_CARD_NO_ERROR) {
      return r;
    }
    Value = (U32)(*p);
    ++p;
    Value |= (U32)(*p) << 8;
    ++p;
    Value |= (U32)(*p) << 16;
    ++p;
    Value |= (U32)(*p) << 24;
    ++p;
    HSMCI_TDR = Value;
  }
  return FS_MMC_CARD_NO_ERROR;
#endif // USE_DMA
}

/*********************************************************************
*
*       FS_MMC_HW_X_Delay
*
*  Function description
*    Waits for a certain time given by the parameter.
*
*   Parameters
*     ms
*/
void FS_MMC_HW_X_Delay(int ms) {
  int i;

  for (i = 0; i < ms; ++i) {
    _Delay1ms();
  }
}

/*********************************************************************
*
*       FS_MMC_HW_X_InitHW
*
*   Function description
*     Initialize the MMC/SD card controller.
*
*   Parameters
*     Unit
*/
void FS_MMC_HW_X_InitHW(U8 Unit) {
  FS_USE_PARA(Unit);
  //
  // Enable the clock of HSMCI and PIO
  //
  PMC_PCER = (1 << HSMCI_ID)
           | (1 << PIOA_ID)
           | (1 << PIOC_ID)
#if USE_DMA
           |  (1 << DMAC_ID)
#endif // USE_DMA
           ;
  //
  // Give control of external port pins to HSMCI
  //
  PIOA_PDR |= PIOA_MCCK
           |  PIOA_MCCDA
           |  PIOA_MCDA0
           |  PIOA_MCDA1
           |  PIOA_MCDA2
           |  PIOA_MCDA3
           ;
  PIOA_ABSR &= ~(PIOA_MCCK  |
                 PIOA_MCCDA |
                 PIOA_MCDA0 |
                 PIOA_MCDA1 |
                 PIOA_MCDA2 |
                 PIOA_MCDA3);
  PIOA_PUDR |= PIOA_MCCK
            |  PIOA_MCCDA
            |  PIOA_MCDA0
            |  PIOA_MCDA1
            |  PIOA_MCDA2
            |  PIOA_MCDA3
            ;
  PIOC_PDR |= PIOC_MCDA4
           |  PIOC_MCDA5
           |  PIOC_MCDA6
           |  PIOC_MCDA7
           ;
  PIOC_ABSR |= PIOC_MCDA4
            |  PIOC_MCDA5
            |  PIOC_MCDA6
            |  PIOC_MCDA7
            ;
  PIOC_PUDR |= PIOC_MCDA4
            |  PIOC_MCDA5
            |  PIOC_MCDA6
            |  PIOC_MCDA7
            ;
  HSMCI_CR = CR_SWRST     // Reset the HSMCI module
           | CR_MCIDIS    // Disable the HSMCI module
           | CR_PWSDIS    // Disable the power down mode
           ;
  HSMCI_IDR   = ~0uL;     // We work in polling mode, no need of interrupts
  HSMCI_SDCR  = SDCR_SDCBUS_1BIT; // Start in 1-bit mode, slot A
  HSMCI_MR    = _ComputeClockDivisor(FSYS / 1000, DEFAULT_SPEED)
              | MR_WRPROOF
              | MR_RDPROOF
              ;
  HSMCI_CFG   = CFG_FIFOMODE      // Start transfer as soon as data is written
              | CFG_FERRCTRL      // A FIFO error is reset when the Control Register is read
              ;
#if USE_DMA
  HSMCI_DMA   = DMA_DMAEN         // Enable the DMA handshake
              | DMA_CHKSIZE       // Number of data available over DMA is always 4 bytes
              ;
#endif // USE_DMA
  HSMCI_DTOR  = DEFAULT_TIMEOUT;
  HSMCI_CSTOR = DEFAULT_TIMEOUT;
  HSMCI_CR    = CR_MCIEN;         // Enable the operation of HSMCI
}

/*********************************************************************
*
*       FS_MMC_HW_X_GetMaxReadBurst
*
*  Function description
*    Returns the number of block (sectors)
*    that can be read at once with a single
*    READ_MULTIPLE_SECTORS.
*
*/
U16 FS_MMC_HW_X_GetMaxReadBurst(U8 Unit) {
  FS_USE_PARA(Unit);
#if USE_DMA
  return NUM_BUFFERS * HSMCI_SIZEOF_FIFO / MAX_SECTOR_SIZE;
#else
  return NUM_BLOCKS_AT_ONCE;
#endif // USE_DMA
}

/*********************************************************************
*
*       FS_MMC_HW_X_GetMaxWriteBurst
*
*  Function description
*    Returns the number of block (sectors)
*    that can be written at once with a single
*    WRITE_MULTIPLE_SECTORS.
*/
U16 FS_MMC_HW_X_GetMaxWriteBurst(U8 Unit) {
  FS_USE_PARA(Unit);
#if USE_DMA
  return NUM_BUFFERS * HSMCI_SIZEOF_FIFO / MAX_SECTOR_SIZE;
#else
  return NUM_BLOCKS_AT_ONCE;
#endif // USE_DMA
}

/*********************************************************************
*
*       FS_MMC_HW_X_SetDataPointer
*
*  Function description
*    Tells the hardware layer where to read data from
*    or write data to. This may be necessary for some controller,
*    before sending the command to the card, eg. programming the DMA.
*    In most cases this function can be left empty.
*
*  Parameters:
*    Unit          - SD card controller no, in case there are more than one.
*
*/
void FS_MMC_HW_X_SetDataPointer(U8 Unit, const void * p) {
  FS_USE_PARA(Unit);
#if USE_DMA
  _pBuffer = p;
#else
  FS_USE_PARA(p);
#endif // USE_DMA
}
