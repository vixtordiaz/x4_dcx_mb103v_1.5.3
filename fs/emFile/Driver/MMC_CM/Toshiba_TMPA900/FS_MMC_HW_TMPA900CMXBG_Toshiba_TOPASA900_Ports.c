/*********************************************************************
*                SEGGER MICROCONTROLLER GmbH & Co. KG                *
*        Solutions for real time microcontroller applications        *
**********************************************************************
*                                                                    *
*        (c) 2003-2012     SEGGER Microcontroller GmbH & Co KG       *
*                                                                    *
*        Internet: www.segger.com    Support:  support@segger.com    *
*                                                                    *
**********************************************************************

**** emFile file system for embedded applications ****
emFile is protected by international copyright laws. Knowledge of the
source code may not be used to write a similar product. This file may
only be used in accordance with a license and should not be re-
distributed in any way. We appreciate your understanding and fairness.
----------------------------------------------------------------------
File        : FS_MMC_HW_TMPA900CMXBG_Toshiba_TOPASA900_Ports.c
Purpose     : Sample MMC hardware layer for accessing MMC/SD 
              via port banging.
---------------------------END-OF-HEADER------------------------------
*/

/*********************************************************************
*
*             #include Section
*
**********************************************************************
*/

#include "FS.h"
#include "MMC_X_HW.h"

/*********************************************************************
*
*             #define Macros
*
**********************************************************************
*/
#define _GPIOG_BASE_ADDR     0xF0806000
#define _GPIOG_DATA (*(volatile U32*) (_GPIOG_BASE_ADDR + 0x03FC)) // PortG Data Regsiter
#define _GPIOG_DIR  (*(volatile U32*) (_GPIOG_BASE_ADDR + 0x0400)) // PortG Data Direction Register
#define _GPIOG_FR1  (*(volatile U32*) (_GPIOG_BASE_ADDR + 0x0424)) // PortG Function Register1

/*********************************************************************
*
*       Configurable macros
*
*   Please setup these macros according your hardware
*
*/

#define SPI_CS_PORT            _GPIOG_DATA
#define SPI_CLK_PORT           _GPIOG_DATA
#define SPI_MISO_PORT          _GPIOG_DATA
#define SPI_MOSI_PORT          _GPIOG_DATA
#define SPI_CD_PORT            _GPIOG_DATA
#define SPI_WP_PORT            _GPIOG_DATA

#define SPI_CS_PIN            3 
#define SPI_CLK_PIN           7
#define SPI_MISO_PIN          0
#define SPI_MOSI_PIN          4

#define SPI_CD_PIN            6
#define SPI_WP_PIN            5

/*********************************************************************
*
*             #define Macros
*
*/
#define SPI_CLR_CS()          SPI_CS_PORT      &= ~(1 << SPI_CS_PIN)
#define SPI_SET_CS()          SPI_CS_PORT      |=  (1 << SPI_CS_PIN)
#define SPI_CLR_CLK()         SPI_CLK_PORT     &= ~(1 << SPI_CLK_PIN)
#define SPI_SET_CLK()         SPI_CLK_PORT     |=  (1 << SPI_CLK_PIN)
#define SPI_CLR_DATAOUT()     SPI_MOSI_PORT    &= ~(1 << SPI_MOSI_PIN)
#define SPI_SET_DATAOUT()     SPI_MOSI_PORT    |=  (1 << SPI_MOSI_PIN)
#define SPI_DATAIN()          (SPI_MISO_PORT &   (1 << SPI_MISO_PIN))
#define SPI_DELAY()           { volatile int i=_Delay ; while(i-- != 0); }
#define SPI_SETUP_PINS()      {                                                   \
                                _GPIOG_FR1 = 0; /* Setup all pins as port pins */ \
                                _GPIOG_DIR = (1 << SPI_CLK_PIN)                   \
                                           | (1 << SPI_MOSI_PIN)                  \
                                           | (1 << SPI_CS_PIN)                    \
                                           ;                                      \
                               }

#define SPI_GET_CD()         (SPI_CD_PORT &   (1 << SPI_CD_PIN))
#define SPI_GET_WP()         (SPI_WP_PORT &   (1 << SPI_WP_PIN))

#define MMC_DEFAULTSUPPLYVOLTAGE  3300 /* in mV, example means 3.3V */

/*********************************************************************
*
*       Static data
*
**********************************************************************
*/
static volatile int _Delay;


/*********************************************************************
*
*             Local functions
*
**********************************************************************
*/


static void _Init(void) {
  SPI_SETUP_PINS();
}

/*********************************************************************
*
*             FS_MMC_HW_X_EnableCS
*
*  Description:
*    FS low level function. Sets the card slot active using the
*    chip select (CS) line.
*
*  Parameters:
*    Unit      - Device Index
*
*  Return value:
*    void
*/

void FS_MMC_HW_X_EnableCS   (U8 Unit) {
  SPI_CLR_CS();
}

/*********************************************************************
*
*             FS_MMC_HW_X_EnableCS
*
*  Description:
*    FS low level function. Sets the card slot inactive using the
*    chip select (CS) line.
*
*  Parameters:
*    Unit      - Device Index
*
*  Return value:
*    void
*/

void FS_MMC_HW_X_DisableCS(U8 Unit) {
  SPI_SET_CS();
}

/*********************************************************************
*
*             FS_MMC_HW_X_SetMaxSpeed
*
*  Description:
*    FS low level function. Sets the SPI interface to a maximum frequency.
*    Make sure that you set the frequency lower or equal but never higher
*    than the given value. Recommended startup frequency is 100kHz - 400kHz.
*
*  Parameters:
*    Unit       - Device Index
*    MaxFreq           - SPI clock frequency in kHz
*
*  Return value:
*    max. frequency    - the maximum frequency set in kHz
*    ==0               - the frequency could not be set
*/

U16 FS_MMC_HW_X_SetMaxSpeed(U8 Unit, U16 MaxFreq) {
  _Init();
  if (MaxFreq <= 400) {
    _Delay = 4000;
  } else {
    _Delay = 1;
    MaxFreq = 2000;
  }
  return MaxFreq;    /* We are not faster than this */
}

/*********************************************************************
*
*             FS_MMC_HW_X_SetVoltage
*
*  Description:
*    FS low level function. Be sure that your card slot si within the given
*    voltage range. Return 1 if your slot can support the required voltage,
*    and if not, return 0;
*
*  Parameters:
*    Unit      - Device Index
*    MaxFreq          - SPI clock frequency in kHz
*
*  Return value:
*    1                - the card slot supports the voltage range
*    ==0              - the card slot does not support the voltage range
*/

int FS_MMC_HW_X_SetVoltage(U8 Unit, U16 Vmin, U16 Vmax) {
  int r;

  //
  // voltage range check
  //
  if((Vmin <= MMC_DEFAULTSUPPLYVOLTAGE) && (Vmax >= MMC_DEFAULTSUPPLYVOLTAGE)) {
    r = 1;
  } else {
    r = 0;
  }
  return r;
}

/*********************************************************************
*
*             FS_MMC_HW_X_IsPresent
*
*  Description:
*    Returns the state of the media. If you do not know the state, return
*    FS_MEDIA_STATE_UNKNOWN and the higher layer will try to figure out if
*    a media is present.
*
*  Parameters:
*    Unit                 - Device Index
*
*  Return value:
*    FS_MEDIA_STATE_UNKNOWN      - the state of the media is unkown
*    FS_MEDIA_NOT_PRESENT        - no card is present
*    FS_MEDIA_IS_PRESENT         - a card is present
*/

int FS_MMC_HW_X_IsPresent(U8 Unit) {
  U8 State;
  
  State = SPI_GET_CD();
  if (State) {
    return FS_MEDIA_NOT_PRESENT;
  }
  return FS_MEDIA_IS_PRESENT;
}

/*********************************************************************
*
*             FS_MMC_HW_X_IsWriteProtected
*
*  Description:
*    FS low level function. Returns the state of the physical write
*    protection of the SD cards.
*
*  Parameters:
*    Unit      - Device Index
*
*  Return value:
*    1                - the card is write protected
*    ==0              - the card is not write protected
*/

int FS_MMC_HW_X_IsWriteProtected(U8 Unit) {
  U8 State;
  
  State = SPI_GET_WP();
  if (State) {
    return 1;
  }
  return 0;
}


/*********************************************************************
*
*             FS_MMC_HW_X_Read
*
*  Description:
*    FS low level function. Reads a specified number of bytes from MMC
*    card to buffer.
*
*  Parameters:
*    Unit      - Device Index
*    pData            - Pointer to a data buffer
*    NumBytes         - Number of bytes
*
*  Return value:
*    void
*/

void FS_MMC_HW_X_Read (U8 Unit, U8 * pData, int NumBytes) {
  U8 bpos;
  U8 c;
  SPI_SET_DATAOUT();
  do {
    c = 0;
    bpos = 8; /* get 8 bits */
    do {
      SPI_CLR_CLK();
      SPI_DELAY();
      c <<= 1;
      if (SPI_DATAIN()) {
        c |= 1;
      }
      SPI_SET_CLK();
      SPI_DELAY();
    } while (--bpos);
    *pData++ = c;
  } while (--NumBytes);
}

/*********************************************************************
*
*             FS_MMC_HW_X_Write
*
*  Description:
*    FS low level function. Writes a specified number of bytes from
*    data buffer to the MMC/SD card.
*
*  Parameters:
*    Unit      - Device Index
*    pData            - Pointer to a data buffer
*    NumBytes         - Number of bytes
*
*  Return value:
*    void
*/

void FS_MMC_HW_X_Write(U8 Unit, const U8 * pData, int NumBytes) {
  int i;
  U8 mask;
  U8 data;
  for (i = 0; i < NumBytes; i++) {
    data = pData[i];
    mask = 0x80;
    while (mask) {
      if (data & mask) {
        SPI_SET_DATAOUT();
      } else {
        SPI_CLR_DATAOUT();
      }
      SPI_CLR_CLK();
      SPI_DELAY();
      SPI_SET_CLK();
      SPI_DELAY();
      mask >>= 1;
    }
  }
  SPI_SET_DATAOUT(); /* default state of data line is high */
}

/*************************** End of file ****************************/
