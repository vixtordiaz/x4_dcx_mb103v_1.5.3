/*********************************************************************
*                SEGGER MICROCONTROLLER GmbH & Co. KG                *
*        Solutions for real time microcontroller applications        *
**********************************************************************
*                                                                    *
*        (c) 2003-2012     SEGGER Microcontroller GmbH & Co KG       *
*                                                                    *
*        Internet: www.segger.com    Support:  support@segger.com    *
*                                                                    *
**********************************************************************

**** emFile file system for embedded applications ****
emFile is protected by international copyright laws. Knowledge of the
source code may not be used to write a similar product. This file may
only be used in accordance with a license and should not be re-
distributed in any way. We appreciate your understanding and fairness.
----------------------------------------------------------------------
File        : FS_MMC_CM_HW_STM32F207IGT6_ST_MB786.c
Purpose     : Hardware layer for MMC/SD driver in CardMode, for the ST MB785 Eval board (STM3220F-EVAL)
Literature  : [1] \\fileserver\techinfo\Company\ST\MCU\STM32\STM32_20xxx\EvalBoard\MB786_RevA\STM3220G-EVAL.pdf
              [2] \\fileserver\techinfo\Company\ST\MCU\STM32\STM32_20xxx\STM32F2xx_RM_rev2.pdf
              [3] \\fileserver\techinfo\Company\ST\MCU\STM32\STM32_20xxx\ErrataSheet_STM32F2xxRevY.pdf
---------------------------END-OF-HEADER------------------------------
*/

/*********************************************************************
*
*             #include section
*
**********************************************************************
*/
#include "FS_Int.h"
#include "MMC_SD_CardMode_X_HW.h"

/*********************************************************************
*
*       Defines non-configurable
*
**********************************************************************
*/
#define SDIO_CLK          48000L  // Clock of MMC module in kHz
#define MAX_BLOCK_SIZE    512     // Maximun number of bytes in a transfered data block
#define MAX_SD_CLK        48000   // Maximum transfer speed
#define USE_OS            0       // Selects the operating mode: 1 event-driven, 0 polling

/*********************************************************************
*
*       #include section, conditional
*
**********************************************************************
*/
#if USE_OS
  #include "RTOS.h"
  #include "stm32f2xx.h"
#endif

/*********************************************************************
*
*       Register macros & defines for register addresses
*
**********************************************************************
*/

/*********************************************************************
*
*       SDIO interface registers
*
*/
#define SDIO_BASE_ADDR    0x40012C00uL
#define SDIO_POWER        (*(volatile U32 *)(SDIO_BASE_ADDR + 0x00))
#define SDIO_CLKCR        (*(volatile U32 *)(SDIO_BASE_ADDR + 0x04))
#define SDIO_ARG          (*(volatile U32 *)(SDIO_BASE_ADDR + 0x08))
#define SDIO_CMD          (*(volatile U32 *)(SDIO_BASE_ADDR + 0x0C))
#define SDIO_RESPCMD      (*(volatile U32 *)(SDIO_BASE_ADDR + 0x10))
#define SDIO_RESP1        (*(volatile U32 *)(SDIO_BASE_ADDR + 0x14))
#define SDIO_RESP2        (*(volatile U32 *)(SDIO_BASE_ADDR + 0x18))
#define SDIO_RESP3        (*(volatile U32 *)(SDIO_BASE_ADDR + 0x1C))
#define SDIO_RESP4        (*(volatile U32 *)(SDIO_BASE_ADDR + 0x20))
#define SDIO_DTIMER       (*(volatile U32 *)(SDIO_BASE_ADDR + 0x24))
#define SDIO_DLEN         (*(volatile U32 *)(SDIO_BASE_ADDR + 0x28))
#define SDIO_DCTRL        (*(volatile U32 *)(SDIO_BASE_ADDR + 0x2C))
#define SDIO_DCOUNT       (*(volatile U32 *)(SDIO_BASE_ADDR + 0x30))
#define SDIO_STA          (*(volatile U32 *)(SDIO_BASE_ADDR + 0x34))
#define SDIO_ICR          (*(volatile U32 *)(SDIO_BASE_ADDR + 0x38))
#define SDIO_MASK         (*(volatile U32 *)(SDIO_BASE_ADDR + 0x3C))
#define SDIO_FIFOCNT      (*(volatile U32 *)(SDIO_BASE_ADDR + 0x48))
#define SDIO_FIFO         (*(volatile U32 *)(SDIO_BASE_ADDR + 0x80))

/*********************************************************************
*
*       Reset and clock control registers
*
*/
#define RCC_BASE_ADDR     0x40023800uL
#define RCC_AHB1RSTR      (*(volatile U32 *)(RCC_BASE_ADDR + 0x10))
#define RCC_APB2RSTR      (*(volatile U32 *)(RCC_BASE_ADDR + 0x24))
#define RCC_AHB1ENR       (*(volatile U32 *)(RCC_BASE_ADDR + 0x30))
#define RCC_APB2ENR       (*(volatile U32 *)(RCC_BASE_ADDR + 0x44))

/*********************************************************************
*
*       Port C registers
*
*/
#define GPIOC_BASE_ADDR   0x40020800uL
#define GPIOC_MODER       (*(volatile U32 *)(GPIOC_BASE_ADDR + 0x00))
#define GPIOC_OSPEEDR     (*(volatile U32 *)(GPIOC_BASE_ADDR + 0x08))
#define GPIOC_PUPDR       (*(volatile U32 *)(GPIOC_BASE_ADDR + 0x0C))
#define GPIOC_AFRH        (*(volatile U32 *)(GPIOC_BASE_ADDR + 0x24))

/*********************************************************************
*
*       Port D registers
*
*/
#define GPIOD_BASE_ADDR   0x40020C00uL
#define GPIOD_MODER       (*(volatile U32 *)(GPIOD_BASE_ADDR + 0x00))
#define GPIOD_OSPEEDR     (*(volatile U32 *)(GPIOD_BASE_ADDR + 0x08))
#define GPIOD_PUPDR       (*(volatile U32 *)(GPIOD_BASE_ADDR + 0x0C))
#define GPIOD_AFRL        (*(volatile U32 *)(GPIOD_BASE_ADDR + 0x20))

/*********************************************************************
*
*       Port H registers
*
*/
#define GPIOH_BASE_ADDR   0x40021C00uL
#define GPIOH_MODER       (*(volatile U32 *)(GPIOH_BASE_ADDR + 0x00))
#define GPIOH_PUPDR       (*(volatile U32 *)(GPIOH_BASE_ADDR + 0x0C))
#define GPIOH_IDR         (*(volatile U32 *)(GPIOH_BASE_ADDR + 0x10))

/*********************************************************************
*
*       DMA 2 registers
*
*/
#define DMA2_BASE_ADDR    0x40026400uL
#define DMA2_LISR         (*(volatile U32 *)(DMA2_BASE_ADDR + 0))
#define DMA2_HISR         (*(volatile U32 *)(DMA2_BASE_ADDR + 4))
#define DMA2_LIFCR        (*(volatile U32 *)(DMA2_BASE_ADDR + 8))
#define DMA2_HIFCR        (*(volatile U32 *)(DMA2_BASE_ADDR + 12))
#define DMA2_S3CR         (*(volatile U32 *)(DMA2_BASE_ADDR + 24 * 3 + 16))
#define DMA2_S3NDTR       (*(volatile U32 *)(DMA2_BASE_ADDR + 24 * 3 + 20))
#define DMA2_S3PAR        (*(volatile U32 *)(DMA2_BASE_ADDR + 24 * 3 + 24))
#define DMA2_S3M0AR       (*(volatile U32 *)(DMA2_BASE_ADDR + 24 * 3 + 28))
#define DMA2_S3FCR        (*(volatile U32 *)(DMA2_BASE_ADDR + 24 * 3 + 36))

/*********************************************************************
*
*       Reset and clock bits for the peripherals used by the driver
*
*/
#define AHB1ENR_DMA2EN    (1uL << 22)
#define AHB1ENR_PORTCEN   (1uL << 2)
#define AHB1ENR_PORTDEN   (1uL << 3)
#define AHB1ENR_PORTHEN   (1uL << 7)
#define APB2ENR_SDIOEN    (1uL << 11)
#define AHB1RSTR_DMA2RST  (1uL << 22)
#define AHB1RSTR_PORTCRST (1uL << 2)
#define AHB1RSTR_PORTDRST (1uL << 3)
#define AHB1RSTR_PORTHRST (1uL << 7)
#define APB2RSTR_SDIORST  (1uL << 11)

/*********************************************************************
*
*       SDIO status register bits
*
*/
#define STA_CCRCFAIL      (1uL << 0)
#define STA_DCRCFAIL      (1uL << 1)
#define STA_CTIMEOUT      (1uL << 2)
#define STA_DTIMEOUT      (1uL << 3)
#define STA_TXUNDERR      (1uL << 4)
#define STA_RXOVERR       (1uL << 5)
#define STA_CMDREND       (1uL << 6)
#define STA_CMDSENT       (1uL << 7)
#define STA_DATAEND       (1uL << 8)
#define STA_STBITERR      (1uL << 9)
#define STA_DBCKEND       (1uL << 10)

#define DCTRL_DTEN              (1uL << 0)
#define DCTRL_DTDIR             (1uL << 1)
#define DCTRL_DTMODE            (1uL << 2)
#define DCTRL_DMAEN             (1uL << 3)
#define DCTRL_DBLOCKSIZE_SHIFT  4uL

#define CLKCR_CLKEN             (1uL   << 8)
#define CLKCR_CLK_PWRSAV        (0x1ul <<  9)
#define CLKCR_CLK_BYPASS        (0x1ul << 10)
#define CLKCR_WIDBUS_MASK       (0x3uL << 11)
#define CLKCR_WIDBUS_4BIT       (0x1uL << 11)
#define CLKCR_HWFC_EN           (1UL   << 14)

#define CMD_CMD_MASK            (0x3FuL)
#define CMD_WAITRESP_SHORT      (1uL << 6)
#define CMD_WAITRESP_LONG       (3uL << 6)
#define CMD_CPSMEN              (1uL << 10)
#define CMD_ENDCMDCMPLT         (1uL << 12)
#define CMD_NIEN                (1uL << 13)
#define CMD_WAITPEND            (1ul <<  9)

#define ICR_CCRCFAIL            (1uL << 0)
#define ICR_DCRCFAIL            (1uL << 1)
#define ICR_CTIMEOUT            (1uL << 2)
#define ICR_DTIMEOUT            (1uL << 3)
#define ICR_TXUNDERR            (1uL << 4)
#define ICR_RXOVERR             (1uL << 5)
#define ICR_CMDREND             (1uL << 6)
#define ICR_CMDSENT             (1uL << 7)
#define ICR_DATAEND             (1uL << 8)
#define ICR_STBITERR            (1uL << 9)
#define ICR_DBCKEND             (1uL << 10)
#define ICR_SDIOIT              (1uL << 22)
#define ICR_CEATAEND            (1uL << 23)
#define ICR_ALL                 (ICR_CCRCFAIL | \
                                 ICR_DCRCFAIL | \
                                 ICR_CTIMEOUT | \
                                 ICR_DTIMEOUT | \
                                 ICR_TXUNDERR | \
                                 ICR_RXOVERR  | \
                                 ICR_CMDREND  | \
                                 ICR_CMDSENT  | \
                                 ICR_DATAEND  | \
                                 ICR_STBITERR | \
                                 ICR_DBCKEND  | \
                                 ICR_SDIOIT   | \
                                 ICR_CEATAEND)

#define MASK_CCRCFAILIE         (1uL << 0)
#define MASK_DCRCFAILIE         (1uL << 1)
#define MASK_CTIMEOUTIE         (1uL << 2)
#define MASK_DTIMEOUTIE         (1uL << 3)
#define MASK_TXUNDERRIE         (1uL << 4)
#define MASK_RXOVERRIE          (1uL << 5)
#define MASK_CMDRENDIE          (1uL << 6)
#define MASK_CMDSENTIE          (1uL << 7)
#define MASK_DATAENDIE          (1uL << 8)
#define MASK_STBITERRIE         (1uL << 9)
#define MASK_DBCKENDIE          (1uL <<10)
#define MASK_ALL                 (MASK_CCRCFAILIE | \
                                  MASK_DCRCFAILIE | \
                                  MASK_CTIMEOUTIE | \
                                  MASK_DTIMEOUTIE | \
                                  MASK_TXUNDERRIE | \
                                  MASK_RXOVERRIE  | \
                                  MASK_CMDRENDIE  | \
                                  MASK_CMDSENTIE  | \
                                  MASK_DATAENDIE  | \
                                  MASK_STBITERRIE | \
                                  MASK_DBCKENDIE)

#define DLEN_DATALENGTH_MASK    (0x03FFFFFFuL)

/*********************************************************************
*
*       GPIO bit positions of SD card lines
*
*/
#define SD_CD_BIT     13
#define SD_D0_BIT     8
#define SD_D1_BIT     9
#define SD_D2_BIT     10
#define SD_D3_BIT     11
#define SD_CLK_BIT    12
#define SD_CMD_BIT    2

/*********************************************************************
*
*       DMA2 related defines
*
*/
#define LIFCR_CDMEIF3     (1uL    << 24)
#define LIFCR_CTCIF3      (1uL    << 25)
#define LIFCR_CHTIF3      (1uL    << 26)
#define LIFCR_CTEIF3      (1uL    << 27)
#define LISR_TEIF3        (1uL << 25)
#define LISR_TCIF3        (1uL << 27)
#define S3CR_EN           (1uL << 0)
#define S3CR_TEIE         (1uL << 2)
#define S3CR_TCIE         (1uL << 4)
#define S3CR_DIR_MASK     (3uL << 6)
#define S3CR_DIR_M2P      (1uL << 6)
#define S3CR_MINC         (1uL << 10)
#define S3CR_PSIZE_32BIT  (2uL << 11)
#define S3CR_MSIZE_32BIT  (2uL << 13)
#define S3CR_PRIO_HIGH    (3uL << 16)
#define S3CR_CHSEL_CH4    (4uL << 25)
#define S3CR_PFCTRL       (1uL << 5)
#define S3CR_PBURST_INCR4 (1uL << 21)
#define S3CR_MBURST_INCR4 (1uL << 23)
#define S3FCR_DMDIS       (1uL << 2)

#define PERIPHERAL_TO_MEMORY        0
#define MEMORY_TO_PERIPHERAL        1

#define MAX_NUM_BLOCKS              ((DLEN_DATALENGTH_MASK / MAX_BLOCK_SIZE) - 1)   // Number of blocks to transfer at once

#define WAIT_TIMEOUT_MAX            0x7FFFFFFFL
#define SDIO_PRIO                   15
#define DMA_PRIO                    15

/*********************************************************************
*
*       Static data
*
**********************************************************************
*/
static U8     _IgnoreCRC;
static U16    _BlockSize;
static U16    _NumBlocks;
static U32    _DataBlockSize;
static void * _pBuffer;
#if USE_OS
  static volatile U32 _StatusSDIO;
  static volatile U32 _StatusDMA;
#endif

/*********************************************************************
*
*       Static code
*
**********************************************************************
*/

/*********************************************************************
*
*       _ld
*/
static U16 _ld(U32 Value) {
  U16 i;

  for (i = 0; i < 16; i++) {
    if ((1uL << i) == Value) {
      break;
    }
  }
  return i;
}

/*********************************************************************
*
*       _DMAStart
*/
static void _DMAStart(U32 * pMemory, U32 * pPripheral, U8 Direction) {
  DMA2_S3CR   &= ~S3CR_EN;          // Stop the data transfer
  while (DMA2_S3CR & S3CR_EN) {     // Wait for the stream to switch off
    ;
  }
  DMA2_S3PAR   = (U32)pPripheral;   // Periphery data register address
  DMA2_S3M0AR  = (U32)pMemory;      // Memory buffer address
  DMA2_S3NDTR  = 0;
  DMA2_LIFCR  = LIFCR_CDMEIF3       // Clear any pending interrupts.
              | LIFCR_CTEIF3
              | LIFCR_CHTIF3
              | LIFCR_CTCIF3
              ;
  DMA2_S3CR   &= ~S3CR_DIR_MASK;
#if USE_OS
  DMA2_S3CR   |= S3CR_TEIE
              |  S3CR_TCIE
              ;
#endif
  if (Direction == MEMORY_TO_PERIPHERAL) {
    DMA2_S3CR |= S3CR_DIR_M2P;
  }
  DMA2_S3CR   |= S3CR_EN;           // Start the data transfer
}

#if USE_OS

/**********************************************************
*
*       SDIO_IRQHandler
*
*   Function description
*     Handles the SDIO interrupt.
*/
void SDIO_IRQHandler(void);
void SDIO_IRQHandler(void) {
  OS_EnterInterrupt();          // Inform embOS that interrupt code is running.
  _StatusSDIO = SDIO_STA;       // Save the status to a static variable and check it in the task.
  SDIO_ICR    = ICR_ALL;        // Clear the flags to prevent further interrupts.
  FS_X_OS_Signal();             // Wake up the task.
  OS_LeaveInterrupt();          // Inform embOS that interrupt code is left.
}

/**********************************************************
*
*       DMA2_Channel4_IRQHandler
*
*   Function description
*     Handles the DMA interrupt.
*/
void DMA2_Channel4_IRQHandler(void);
void DMA2_Channel4_IRQHandler(void) {
  OS_EnterInterrupt();          // Inform embOS that interrupt code is running.
  _StatusDMA = DMA2_LISR;       // Save the status to a static variable and check it in the task.
  //
  // Clear pending interrupt flags.
  //
  DMA2_LIFCR  = LIFCR_CDMEIF3
              | LIFCR_CTEIF3
              | LIFCR_CHTIF3
              | LIFCR_CTCIF3
              ;
  FS_X_OS_Signal();             // Wake up the task.
  OS_LeaveInterrupt();          // Inform embOS that interrupt code is left.
}

#endif

/*********************************************************************
*
*       Public code
*
**********************************************************************
*/

/*********************************************************************
*
*       FS_MMC_HW_X_SetHWNumBlocks
*
*  Function description
*    Sets the number of block (sectors) to be transferred.
*
*/
void FS_MMC_HW_X_SetHWNumBlocks(U8 Unit, U16 NumBlocks) {
  FS_USE_PARA(Unit);
  _NumBlocks = NumBlocks;
}

/*********************************************************************
*
*       FS_MMC_HW_X_SetHWBlockLen
*
*  Function description
*    Sets the block size (sector size) that shall be transferred.
*/
void FS_MMC_HW_X_SetHWBlockLen(U8 Unit, U16 BlockSize) {
  FS_USE_PARA(Unit);
  _BlockSize     = BlockSize;
  _DataBlockSize = _ld(BlockSize) << DCTRL_DBLOCKSIZE_SHIFT;
}

/*********************************************************************
*
*       FS_MMC_HW_X_SetMaxSpeed
*
*  Function description
*    Sets the frequency of the MMC/SD card controller.
*    The frequency is given in kHz.
*    It is called 2 times:
*     1. During card initialization
*        Initialize the frequency to not more than 400kHz.
*
*     2. After card initialization
*        The CSD register of card is read and the max frequency
*        the card can operate is determined.
*        [In most cases: MMC cards 20MHz, SD cards 25MHz]
*
*/
U16 FS_MMC_HW_X_SetMaxSpeed(U8 Unit, U16 Freq) {
  U32      Fact;
  unsigned Div;

  FS_USE_PARA(Unit);

  if (Freq > MAX_SD_CLK) {
    Freq = MAX_SD_CLK;
  }
  SDIO_CLKCR &=  ~(CLKCR_CLKEN | 0xFFuL | CLKCR_CLK_BYPASS);
  Fact         = 2;
  Div          = 0;
  while ((Freq * Fact) < SDIO_CLK) {
    ++Fact;
    if (0xFF == ++Div) {
      break;
    }
  }
  SDIO_CLKCR |=  Div | CLKCR_CLKEN | CLKCR_CLK_PWRSAV;
  return SDIO_CLK / (Div + 2);
}

/*********************************************************************
*
*       FS_MMC_HW_X_IsPresent
*
*    Returns the state of the media. If you do not know the state, return
*    FS_MEDIA_STATE_UNKNOWN and the higher layer will try to figure out if
*    a media is present.
*
*  Parameters:
*    Unit                 - Device Index
*
*  Return value:
*    FS_MEDIA_STATE_UNKNOWN      - the state of the media is unkown
*    FS_MEDIA_NOT_PRESENT        - no card is present
*    FS_MEDIA_IS_PRESENT         - a card is present
*/
int FS_MMC_HW_X_IsPresent(U8 Unit) {

  FS_USE_PARA(Unit);
  //return !(GPIOH_IDR & (1uL << SD_CD_BIT)) ? FS_MEDIA_IS_PRESENT : FS_MEDIA_NOT_PRESENT;
  //-Q-
  return FS_MEDIA_IS_PRESENT;
}

/*********************************************************************
*
*       FS_MMC_HW_X_IsWriteProtected
*
*  Function description
*    Returns whether card is write protected or not.
*/
int FS_MMC_HW_X_IsWriteProtected  (U8 Unit) {
  FS_USE_PARA(Unit);
  return 0;
}

/*********************************************************************
*
*       FS_MMC_HW_X_SetResponseTimeOut
*
*  Function description
*    Sets the response time out value given in MMC/SD card cycles.
*
*/
void FS_MMC_HW_X_SetResponseTimeOut(U8 Unit, U32 Value) {
  FS_USE_PARA(Unit);
  FS_USE_PARA(Value);
  //
  // The response timeout is fixed in hardware
  //
  ;
}

/*********************************************************************
*
*       FS_MMC_HW_X_SetReadDataTimeOut
*
*  Function description
*    Sets the read data time out value given in MMC/SD card cycles.
*/
void FS_MMC_HW_X_SetReadDataTimeOut(U8 Unit, U32 Value) {
  FS_USE_PARA(Unit);
  SDIO_DTIMER = Value;
}

/*********************************************************************
*
*       FS_MMC_HW_X_SendCmd
*
*  Function description
*    Sends a command to the MMC/SD card.
*/
void FS_MMC_HW_X_SendCmd(U8 Unit, unsigned Cmd, unsigned CmdFlags, unsigned ResponseType, U32 Arg) {
  U32 CmdCfg;
  U8  Direction;
  U32 NumBytes;
  U32 Status;

  FS_USE_PARA(Unit);
  CmdCfg = CMD_CPSMEN
         | CMD_ENDCMDCMPLT
         | CMD_NIEN
         ;
  _IgnoreCRC = 0;
  switch (ResponseType) {
  case FS_MMC_RESPONSE_FORMAT_R3:
    _IgnoreCRC = 1;
  case FS_MMC_RESPONSE_FORMAT_R1:
    CmdCfg |= CMD_WAITRESP_SHORT;
    break;
  case FS_MMC_RESPONSE_FORMAT_R2:
    CmdCfg |= CMD_WAITRESP_LONG;
    break;
  }
  if (CmdCfg & FS_MMC_CMD_FLAG_SETBUSY) {
    CmdCfg |= CMD_WAITPEND;
  }
  if (CmdFlags & FS_MMC_CMD_FLAG_USE_SD4MODE) {   // 4 bit mode?
    SDIO_CLKCR |= CLKCR_WIDBUS_4BIT;
  } else {
    SDIO_CLKCR &=~CLKCR_WIDBUS_MASK;
  }
  SDIO_DCTRL = 0;
  if (CmdFlags & FS_MMC_CMD_FLAG_DATATRANSFER) {
    SDIO_ICR = ICR_DCRCFAIL
             | ICR_DTIMEOUT
             | ICR_TXUNDERR
             | ICR_DATAEND
             | ICR_DBCKEND
             | ICR_STBITERR
             | ICR_RXOVERR
             ;
    NumBytes = _BlockSize * _NumBlocks;
    SDIO_DLEN = NumBytes;
    if (CmdFlags & FS_MMC_CMD_FLAG_WRITETRANSFER) {
      Direction = MEMORY_TO_PERIPHERAL;
    } else {
      Direction = PERIPHERAL_TO_MEMORY;
    }
    _DMAStart((U32 *)_pBuffer, (U32 *)&SDIO_FIFO, Direction);
  }
  //
  // Clear pending status flags
  //
  SDIO_ICR = ICR_CCRCFAIL
           | ICR_CTIMEOUT
           | ICR_CMDREND
           | ICR_CMDSENT
           ;
#if USE_OS
  _StatusSDIO = 0;
  _StatusDMA  = 0;
#endif
  SDIO_ARG = Arg;
  SDIO_CMD = CmdCfg | (Cmd & CMD_CMD_MASK);
  if (CmdFlags & FS_MMC_CMD_FLAG_INITIALIZE)  {
    while (1) {
#if USE_OS
      FS_X_OS_Wait(WAIT_TIMEOUT_MAX);
      Status = _StatusSDIO;
#else
      Status = SDIO_STA;
#endif
      if (Status & (STA_CMDSENT | STA_CMDREND | STA_CCRCFAIL |  STA_CTIMEOUT)) {
        break;
      }
    }
  }
}

/*********************************************************************
*
*       FS_MMC_HW_X_GetResponse
*
*  Function description
*    Receives the responses that was sent by the card after
*    a command was sent to the card.
*/
int FS_MMC_HW_X_GetResponse(U8 Unit, void *pBuffer, U32 Size) {
  U8           * p;
  int            NumBytes;
  volatile U32 * pReg;
  U32            Data32;
  U32            Status;

  FS_USE_PARA(Unit);
  p        = (U8 *)pBuffer;
  NumBytes = Size;
  while (1) {
#if USE_OS
    FS_X_OS_Wait(WAIT_TIMEOUT_MAX);
    Status = _StatusSDIO;
#else
    Status = SDIO_STA;
#endif
    if (Status & (STA_CMDSENT | STA_CMDREND | STA_CCRCFAIL | STA_CTIMEOUT)) {
      break;
    }
  }
  if (STA_CTIMEOUT & Status) {
    return FS_MMC_CARD_RESPONSE_TIMEOUT;
  }
  if ((STA_CCRCFAIL & Status) && !_IgnoreCRC) {
    return FS_MMC_CARD_READ_CRC_ERROR;
  }
  if ((STA_CMDREND & Status) || _IgnoreCRC) {
    *p++ = SDIO_RESPCMD;
    NumBytes--;
    pReg = (volatile U32 *)(&SDIO_RESP1);
    do {
      Data32 = FS_LoadU32BE((const U8 *)pReg);
      FS_StoreU32LE(p, Data32);
      NumBytes -= 4;
      pReg++;
      p += 4;
    } while (NumBytes >= 4);
    SDIO_ICR = STA_CMDREND;
  }
  return FS_MMC_CARD_NO_ERROR;
}

/*********************************************************************
*
*       FS_MMC_HW_X_ReadData
*
*  Function description
*    Reads data from MMC/SD card try the MMC/SD card controller.
*
*/
int FS_MMC_HW_X_ReadData(U8 Unit, void * pBuffer, unsigned NumBytes, unsigned NumBlocks) {
  U32 StatusSDIO;
  U32 StatusDMA;

  FS_USE_PARA(Unit);
  FS_USE_PARA(pBuffer);
  FS_USE_PARA(NumBytes);
  FS_USE_PARA(NumBlocks);
  //
  // Data transfer starts when the data control register is updated
  //
  SDIO_DCTRL = _DataBlockSize
              | DCTRL_DTDIR
              | DCTRL_DMAEN
              | DCTRL_DTEN
              ;
  while (1) {
#if USE_OS
    FS_X_OS_Wait(WAIT_TIMEOUT_MAX);
    StatusSDIO = _StatusSDIO;
    StatusDMA  = _StatusDMA;
#else
    StatusSDIO = SDIO_STA;
    StatusDMA = DMA2_LISR;
#endif
    if (StatusDMA & LISR_TEIF3) {
      return FS_MMC_CARD_READ_CRC_ERROR;
    }
    if (StatusSDIO & STA_DCRCFAIL) {
      return FS_MMC_CARD_READ_CRC_ERROR;
    }
    if (StatusSDIO & STA_DTIMEOUT) {
      return FS_MMC_CARD_READ_TIMEOUT;
    }
    if (StatusSDIO & STA_RXOVERR) {
      return FS_MMC_CARD_READ_CRC_ERROR;
    }
    if (StatusSDIO & STA_STBITERR) {
      return FS_MMC_CARD_READ_CRC_ERROR;
    }
    if (StatusDMA & LISR_TCIF3) {
      return FS_MMC_CARD_NO_ERROR;
    }
  }
}

/*********************************************************************
*
*       FS_MMC_HW_X_WriteData
*
*  Function description
*    Writes the data to MMC/SD card try the MMC/SD card controller.
*
*/
int FS_MMC_HW_X_WriteData(U8 Unit, const void * pBuffer, unsigned NumBytes, unsigned NumBlocks) {
  U32 StatusSDIO;
  U32 StatusDMA;

  FS_USE_PARA(Unit);
  FS_USE_PARA(pBuffer);
  FS_USE_PARA(NumBytes);
  FS_USE_PARA(NumBlocks);
  //
  // Data transfer starts when the data control register is updated
  //
  SDIO_DCTRL = _DataBlockSize
             | DCTRL_DTEN
             | DCTRL_DMAEN
             ;
  while (1) {
#if USE_OS
    FS_X_OS_Wait(WAIT_TIMEOUT_MAX);
    StatusSDIO = _StatusSDIO;
    StatusDMA  = _StatusDMA;
#else
    StatusSDIO = SDIO_STA;
    StatusDMA = DMA2_LISR;
#endif
    if (StatusDMA & LISR_TEIF3) {
      return FS_MMC_CARD_READ_CRC_ERROR;
    }
    if (StatusSDIO & STA_DCRCFAIL) {
      return FS_MMC_CARD_WRITE_CRC_ERROR;
    }
    if (StatusSDIO & STA_DTIMEOUT) {
      return FS_MMC_CARD_WRITE_CRC_ERROR;
    }
    if (StatusSDIO & STA_TXUNDERR) {
      return FS_MMC_CARD_WRITE_CRC_ERROR;
    }
    if (StatusSDIO & STA_STBITERR) {
      return FS_MMC_CARD_WRITE_CRC_ERROR;
    }
    if (StatusSDIO & STA_DATAEND) {
      return FS_MMC_CARD_NO_ERROR;
    }
  }
}

/*********************************************************************
*
*       FS_MMC_HW_X_Delay
*
*  Function description
*    Waits for a certain time given by the parameter.
*
*/
void FS_MMC_HW_X_Delay(int ms) {
  while (ms--) {
    for(volatile U32 i = 1000 * 10; i; i--) {
      ;
    }
  }
}

/*********************************************************************
*
*       FS_MMC_HW_X_InitHW
*
*  Function description
*    Initialize the MMC/SD card controller.
*
*  Notes
*     (1) Flow control should not be enabled as it causes glitches on the clock line (see [3])
*/
void FS_MMC_HW_X_InitHW(U8 Unit) {
  FS_USE_PARA(Unit);

  //
  // Reset SDIO
  //
  RCC_APB2RSTR |= APB2RSTR_SDIORST;
  RCC_APB2RSTR &= ~APB2RSTR_SDIORST;

  //
  // Enable GPIOs, DMA and SDIO
  //
  RCC_AHB1ENR |= AHB1ENR_DMA2EN
              |  AHB1ENR_PORTCEN
              |  AHB1ENR_PORTDEN
              |  AHB1ENR_PORTHEN
              ;
  RCC_APB2ENR |= APB2ENR_SDIOEN
              ;
  //
  // Card detect (CD) line is input with pull-up
  //
/* -Q- don't need this
  GPIOH_MODER &= ~(3uL << (SD_CD_BIT << 1));  // Input
  GPIOH_PUPDR &= ~(3uL << (SD_CD_BIT << 1));  // Pull-up
  GPIOH_PUPDR |=  (1uL << (SD_CD_BIT << 1));
  //
  // D0, D1, D2, D3, CLK lines are controlled by SDIO
  //
  GPIOC_MODER &= ~((3uL << (SD_D0_BIT  << 1)) |
                   (3uL << (SD_D1_BIT  << 1)) |
                   (3uL << (SD_D2_BIT  << 1)) |
                   (3uL << (SD_D3_BIT  << 1)) |
                   (3uL << (SD_CLK_BIT << 1)));
  GPIOC_MODER |= (2uL << (SD_D0_BIT  << 1))
              |  (2uL << (SD_D1_BIT  << 1))
              |  (2uL << (SD_D2_BIT  << 1))
              |  (2uL << (SD_D3_BIT  << 1))
              |  (2uL << (SD_CLK_BIT << 1))
              ;
  GPIOC_PUPDR &= ~((3uL << (SD_D0_BIT  << 1)) |
                   (3uL << (SD_D1_BIT  << 1)) |
                   (3uL << (SD_D2_BIT  << 1)) |
                   (3uL << (SD_D3_BIT  << 1)) |
                   (3uL << (SD_CLK_BIT << 1)));
  GPIOC_AFRH  &= ~((0xFuL << ((SD_D0_BIT - 8)  << 2)) |
                   (0xFuL << ((SD_D1_BIT - 8) << 2))  |
                   (0xFuL << ((SD_D2_BIT - 8)  << 2)) |
                   (0xFuL << ((SD_D3_BIT - 8)  << 2)) |
                   (0xFuL << ((SD_CLK_BIT - 8) << 2)));
  GPIOC_AFRH  |= (12uL << ((SD_D0_BIT - 8)  << 2))
              |  (12uL << ((SD_D1_BIT - 8)  << 2))
              |  (12uL << ((SD_D2_BIT - 8)  << 2))
              |  (12uL << ((SD_D3_BIT - 8)  << 2))
              |  (12uL << ((SD_CLK_BIT - 8) << 2));
  GPIOC_OSPEEDR &= ~((3uL << (SD_D0_BIT  << 1)) |
                    (3uL << (SD_D1_BIT  << 1))  |
                    (3uL << (SD_D2_BIT  << 1))  |
                    (3uL << (SD_D3_BIT  << 1))  |
                    (3uL << (SD_CLK_BIT << 1)));
  GPIOC_OSPEEDR |= (3uL << (SD_D0_BIT  << 1)) |
                   (3uL << (SD_D1_BIT  << 1)) |
                   (3uL << (SD_D2_BIT  << 1)) |
                   (3uL << (SD_D3_BIT  << 1)) |
                   (3uL << (SD_CLK_BIT << 1));    // High speed ports
  //
  // CMD line is also controlled by SDIO
  //
  GPIOD_MODER   &= ~(3uL << (SD_CMD_BIT  << 1));
  GPIOD_MODER   |= (2uL << (SD_CMD_BIT  << 1));
  GPIOD_PUPDR   &= ~(3uL << (SD_CMD_BIT << 1));
  GPIOD_AFRL    &= ~(0xFuL << (SD_CMD_BIT << 2));
  GPIOD_AFRL    |=  (12uL  << (SD_CMD_BIT << 2));
  GPIOD_OSPEEDR &= ~(3uL << (SD_CMD_BIT  << 1));
  GPIOD_OSPEEDR |=  (2uL   << (SD_CMD_BIT << 1));
*/
  //
  // SDIO uses the stream 3, channel 4 of DMA2
  //
  DMA2_LIFCR  |= LIFCR_CDMEIF3      // Clear any pending interrupts.
              |  LIFCR_CTEIF3
              |  LIFCR_CHTIF3
              |  LIFCR_CTCIF3
              ;
  DMA2_S3CR   = 0
              | S3CR_PSIZE_32BIT    // Peripheral bus width
              | S3CR_MSIZE_32BIT    // Memory bus width
              | S3CR_MINC           // Memory increment enable
              | S3CR_PRIO_HIGH      // Set priority to high
              | S3CR_CHSEL_CH4      // Channel connected to SDIO
              | S3CR_PFCTRL         // Peripheral controls the data transfer
              | S3CR_PBURST_INCR4   // Burst transfer on peripheral side
              | S3CR_MBURST_INCR4   // Burst transfer on memory side
              ;
  DMA2_S3FCR  = (3 << 0)            // Full fifo
              | S3FCR_DMDIS         // Disable direct mode (the only way the DMA transfer works together with SDIO)
              ;

  //
  // Initialize SDIO
  //
  SDIO_POWER  = 0;    // Power off
  SDIO_CLKCR  = 0;    // Disable the clock (Note 1)
  SDIO_ARG    = 0;
  SDIO_CMD    = 0;
  SDIO_DTIMER = 0;
  SDIO_DLEN   = 0;
  SDIO_DCTRL  = 0;
  SDIO_ICR    = 0x00C007FF;       // Clear interrupts
  SDIO_MASK   = 0;
  SDIO_POWER  = 3;    // Power On
#if USE_OS
  //
  // Unmask the interrupt sources.
  //
  SDIO_MASK   = MASK_ALL;
  //
  // Set the priority and enable the interrupts.
  //
  NVIC_SetPriority(DMA2_Stream3_IRQn, DMA_PRIO);
  NVIC_SetPriority(SDIO_IRQn, SDIO_PRIO);
  NVIC_EnableIRQ(DMA2_Stream3_IRQn);
  NVIC_EnableIRQ(SDIO_IRQn);
#endif
}

/*********************************************************************
*
*       FS_MMC_HW_X_GetMaxReadBurst
*
*  Function description
*    Returns the number of block (sectors)
*    that can be read at once with a single
*    READ_MULTIPLE_SECTORS.
*
*/
U16 FS_MMC_HW_X_GetMaxReadBurst (U8 Unit) {
  FS_USE_PARA(Unit);
  return (U16)MAX_NUM_BLOCKS;
}

/*********************************************************************
*
*       FS_MMC_HW_X_GetMaxWriteBurst
*
*  Function description
*    Returns the number of block (sectors)
*    that can be written at once with a single
*    WRITE_MULTIPLE_SECTORS.
*/
U16 FS_MMC_HW_X_GetMaxWriteBurst (U8 Unit) {
  FS_USE_PARA(Unit);
  return (U16)MAX_NUM_BLOCKS;
}

/*********************************************************************
*
*       FS_MMC_HW_X_SetDataPointer
*
*  Function description
*    Tells the hardware layer where to read data from
*    or write data to. This may be necessary for some controller,
*    before sending the command to the card, eg. programming the DMA.
*    In most cases this function can be left empty.
*
*  Parameters:
*    Unit          - SD card controller no, in case there are more than one.
*
*/
void FS_MMC_HW_X_SetDataPointer(U8 Unit, const void * p) {
  FS_USE_PARA(Unit);
  _pBuffer = (void *)p; // cast const away as this buffer is used also for storing the data from card
}

/*************************** End of file ****************************/

