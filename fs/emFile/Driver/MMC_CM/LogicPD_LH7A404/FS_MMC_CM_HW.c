/*********************************************************************
*                SEGGER MICROCONTROLLER GmbH & Co. KG                *
*        Solutions for real time microcontroller applications        *
**********************************************************************
*                                                                    *
*        (c) 2003-2012     SEGGER Microcontroller GmbH & Co KG       *
*                                                                    *
*        Internet: www.segger.com    Support:  support@segger.com    *
*                                                                    *
**********************************************************************

**** emFile file system for embedded applications ****
emFile is protected by international copyright laws. Knowledge of the
source code may not be used to write a similar product. This file may
only be used in accordance with a license and should not be re-
distributed in any way. We appreciate your understanding and fairness.
----------------------------------------------------------------------
----------------------------------------------------------------------
File        : MMC_CM_HW_LogicPD_LH7A404.c
Purpose     : Low level MMC/SD driver for the Sharp LH7A404
---------------------------END-OF-HEADER------------------------------
*/

/*********************************************************************
*
*       Includes
*
**********************************************************************
*/
#include "FS.h"
#include "MMC_SD_CardMode_X_HW.h"


/*********************************************************************
*
*       Defines non configurable
*
**********************************************************************
*/
#define  TIME_OUT_VALUE     (5000 * 50)  // Set an approx. time out of 5 sec.
#define  CPU_HCLK           99500000UL   // 100 MHz is set as HCLK

/*********************************************************************
*
*       Defines non configurable
*
**********************************************************************
*/

/* SDMMC clock rate register divide by 1 load value */
#define MMC_CLOCK_DIV1                   0
/* SDMMC clock rate register divide by 2 load value */
#define MMC_CLOCK_DIV2                   1
/* SDMMC clock rate register divide by 4 load value */
#define MMC_CLOCK_DIV4                   2
/* SDMMC clock rate register divide by 8 load value */
#define MMC_CLOCK_DIV8                   3
/* SDMMC clock rate register divide by 16 load value */
#define MMC_CLOCK_DIV16                  4
/* SDMMC clock rate register divide by 32 load value */
#define MMC_CLOCK_DIV32                  5
/* SDMMC clock rate register divide by 64 load value */
#define MMC_CLOCK_DIV64                  6


/*********************************************************************
*
*       Local defines (sfrs)
*
**********************************************************************
*/
#define __GPIOBASE  0x80000E00
#define __GPIO_PAD           *(volatile U32*)(__GPIOBASE + 0x0000)
#define __GPIO_PBD           *(volatile U32*)(__GPIOBASE + 0x0004)
#define __GPIO_PCD           *(volatile U32*)(__GPIOBASE + 0x0008)
#define __GPIO_PDD           *(volatile U32*)(__GPIOBASE + 0x000C)
#define __GPIO_PADD          *(volatile U32*)(__GPIOBASE + 0x0010)
#define __GPIO_PBDD          *(volatile U32*)(__GPIOBASE + 0x0014)
#define __GPIO_PCDD          *(volatile U32*)(__GPIOBASE + 0x0018)
#define __GPIO_PDDD          *(volatile U32*)(__GPIOBASE + 0x001C)
#define __GPIO_PED           *(volatile U32*)(__GPIOBASE + 0x0020)
#define __GPIO_PEDD          *(volatile U32*)(__GPIOBASE + 0x0024)
#define __GPIO_KBDCTL        *(volatile U32*)(__GPIOBASE + 0x0028)
#define __GPIO_PINMUX        *(volatile U32*)(__GPIOBASE + 0x002C)
#define __GPIO_PFD           *(volatile U32*)(__GPIOBASE + 0x0030)
#define __GPIO_PFDD          *(volatile U32*)(__GPIOBASE + 0x0034)
#define __GPIO_PGD           *(volatile U32*)(__GPIOBASE + 0x0038)
#define __GPIO_PGDD          *(volatile U32*)(__GPIOBASE + 0x003C)
#define __GPIO_PHD           *(volatile U32*)(__GPIOBASE + 0x0040)
#define __GPIO_PHDD          *(volatile U32*)(__GPIOBASE + 0x0044)
#define __GPIO_INTTYPE1      *(volatile U32*)(__GPIOBASE + 0x004C)
#define __GPIO_INTTYPE2      *(volatile U32*)(__GPIOBASE + 0x0050)
#define __GPIO_GPIOFEOI      *(volatile U32*)(__GPIOBASE + 0x0054)
#define __GPIO_GPIOINTEN     *(volatile U32*)(__GPIOBASE + 0x0058)
#define __GPIO_INTSTATUS     *(volatile U32*)(__GPIOBASE + 0x005C)
#define __GPIO_RAWINTSTATUS  *(volatile U32*)(__GPIOBASE + 0x0060)
#define __GPIO_DEBOUNCE      *(volatile U32*)(__GPIOBASE + 0x0064)
#define __GPIO_PAPD          *(volatile U32*)(__GPIOBASE + 0x0068)
#define __GPIO_PBPD          *(volatile U32*)(__GPIOBASE + 0x006C)
#define __GPIO_PCPD          *(volatile U32*)(__GPIOBASE + 0x0070)
#define __GPIO_PDPD          *(volatile U32*)(__GPIOBASE + 0x0074)
#define __GPIO_PEPD          *(volatile U32*)(__GPIOBASE + 0x0078)
#define __GPIO_PFPD          *(volatile U32*)(__GPIOBASE + 0x007C)
#define __GPIO_PGPD          *(volatile U32*)(__GPIOBASE + 0x0080)
#define __GPIO_PHPD          *(volatile U32*)(__GPIOBASE + 0x0084)


#define __SDMMC_BASE         (0x80000100)
#define __SDMMC_CLKC           *(volatile U32*)(__SDMMC_BASE + 0x0000)
#define __SDMMC_STATUS         *(volatile U32*)(__SDMMC_BASE + 0x0004)
#define __SDMMC_RATE           *(volatile U32*)(__SDMMC_BASE + 0x0008)
#define __SDMMC_PREDIV         *(volatile U32*)(__SDMMC_BASE + 0x000C)
#define __SDMMC_CMDCON         *(volatile U32*)(__SDMMC_BASE + 0x0014)
#define __SDMMC_RES_TO         *(volatile U32*)(__SDMMC_BASE + 0x0018)
#define __SDMMC_READ_TO        *(volatile U32*)(__SDMMC_BASE + 0x001C)
#define __SDMMC_BLK_LEN        *(volatile U32*)(__SDMMC_BASE + 0x0020)
#define __SDMMC_NUM_BLK        *(volatile U32*)(__SDMMC_BASE + 0x0024)
#define __SDMMC_INT_STATUS     *(volatile U32*)(__SDMMC_BASE + 0x0028)
#define __SDMMC_EOI            *(volatile U32*)(__SDMMC_BASE + 0x002C)
#define __SDMMC_INT_MASK       *(volatile U32*)(__SDMMC_BASE + 0x0034)
#define __SDMMC_CMD            *(volatile U32*)(__SDMMC_BASE + 0x0038)
#define __SDMMC_ARGUMENT       *(volatile U32*)(__SDMMC_BASE + 0x003C)
#define __SDMMC_RES_FIFO       *(volatile U32*)(__SDMMC_BASE + 0x0040)
#define __SDMMC_DATA_FIFO      *(volatile U32*)(__SDMMC_BASE + 0x0048)

#define MMC_STATUS_END_COMMAND_RESPONSE    (1 << 13)
#define MMC_STATUS_FIFO_EMPTY              (1 <<  6)
#define MMC_STATUS_FIFO_FULL               (1 <<  7)
#define MMC_STATUS_DATA_TRANFER_DONE       (1 << 11)
#define MMC_STATUS_DATA_PROGRAM_DONE       (1 << 12)

#define MMC_STATUS_END_COMMAND_RESPONSE    (1 << 13)
#define MMC_STATUS_RESPONSE_TIMEOUT        (1 <<  1)
#define MMC_STATUS_READDATA_TIMEOUT        (1 <<  0)
#define MMC_STATUS_CLOCK_DISABLED          (1 <<  8)


/*********************************************************************
*
*       Static code
*
**********************************************************************
*/

/**********************************************************
*
*       _StopMMCClock
*/
static void _StopMMCClock(U8 Unit) {
  __SDMMC_CLKC      = (1 << 0);       // Stop the MMC/SD clock
  while ((__SDMMC_STATUS & MMC_STATUS_CLOCK_DISABLED) == 0);
}

/**********************************************************
*
*       _StartMMCClock
*/
static void _StartMMCClock(U8 Unit) {
  __SDMMC_CLKC      = (1 << 1);       // Start the MMC/SD clock
}

/**********************************************************
*
*       _Delayus
*/
static void _Delayus(unsigned us) {
  volatile unsigned Time;

  Time = us * 50;
  do {} while(--Time);
}

/*********************************************************************
*
*       Public code
*
**********************************************************************
*/


/*********************************************************************
*
*       FS_MMC_HW_X_SetHWNumBlocks
*
*  Function description
*    Sets the number of block (sectors) to be transferred.
*
*/

void FS_MMC_HW_X_SetHWNumBlocks(U8 Unit, U16 NumBlocks) {
  __SDMMC_NUM_BLK  = NumBlocks;
}

/*********************************************************************
*
*       FS_MMC_HW_X_SetHWBlockLen
*
*  Function description
*    Sets the block size (sector size) that shall be transferred.
*/void FS_MMC_HW_X_SetHWBlockLen(U8 Unit, U16 BlockSize) {
  __SDMMC_BLK_LEN  = BlockSize;
}

/*********************************************************************
*
*       FS_MMC_HW_X_SetMaxSpeed
*
*  Function description
*    Sets the frequency of the MMC/SD card controller.
*    The frequency is given in kHz.
*    It is called 2 times:
*     1. During card initialization
*        Initialize the frequency to not more than 400kHz.
*        
*     2. After card initialization
*        The CSD register of card is read and the max frequency 
*        the card can operate is determined.
*        [In most cases: MMC cards 20MHz, SD cards 25MHz]
*
*/
U16 FS_MMC_HW_X_SetMaxSpeed(U8 Unit, U16 Freq) {
  U32 saved_prediv;
  U32 saved_div;

  if (Freq == 400) {
    saved_prediv =  8;
    saved_div    =  5;
  } else {
    saved_prediv =  5;
    saved_div    =  0;
  }
  __SDMMC_PREDIV    = (1 << 5)        // Use Poll mode instead of DMA
                    | (1 << 4)        // Enable the Controller
                    | (saved_prediv & 0x0f);
  __SDMMC_RATE      = saved_div;
  Freq = ((CPU_HCLK / saved_prediv) / (1 << saved_div)) /1000 ;
  return Freq;
}

/*********************************************************************
*
*       FS_MMC_HW_X_IsPresent
*
*    Returns the state of the media. If you do not know the state, return
*    FS_MEDIA_STATE_UNKNOWN and the higher layer will try to figure out if
*    a media is present.
*
*  Parameters:
*    Unit                 - Device Index
*
*  Return value:
*    FS_MEDIA_STATE_UNKNOWN      - the state of the media is unkown
*    FS_MEDIA_NOT_PRESENT        - no card is present
*    FS_MEDIA_IS_PRESENT         - a card is present
*/int FS_MMC_HW_X_IsPresent(U8 Unit) {
  __GPIO_PFDD &=  ~(1 << 5);  // Set PE.5 as input for card detect signal
  return ((__GPIO_PFD >> 5) & 1) ? FS_MEDIA_NOT_PRESENT : FS_MEDIA_IS_PRESENT;
}

/*********************************************************************
*
*       FS_MMC_HW_X_IsWriteProtected
*
*  Function description
*    Returns wheter card is write protected or not.
*/
int    FS_MMC_HW_X_IsWriteProtected  (U8 Unit) {
  return 0;
}

/*********************************************************************
*
*       FS_MMC_HW_X_SetResponseTimeOut
*
*  Function description
*    Sets the reponse time out value given in MMC/SD card cycles.
*
*/
void FS_MMC_HW_X_SetResponseTimeOut(U8 Unit, U32 Value) {
  __SDMMC_RES_TO    = Value;           // Set a high timeout for Card Response
}

/*********************************************************************
*
*       FS_MMC_HW_X_SetReadDataTimeOut
*
*  Function description
*    Sets the read data time out value given in MMC/SD card cycles.
*/void FS_MMC_HW_X_SetReadDataTimeOut(U8 Unit, U32 Value) {
  __SDMMC_READ_TO   = Value;          // Same to read timeout
}

/*********************************************************************
*
*       FS_MMC_HW_X_SendCmd
*
*  Function description
*    Sends a command to the MMC/SD card.
*/
void FS_MMC_HW_X_SendCmd(U8 Unit, unsigned Cmd, unsigned CmdFlags, unsigned ResponseType, U32 Arg) {
  U32 CmdCon;

  CmdCon = ResponseType;
  if (CmdFlags & FS_MMC_CMD_FLAG_DATATRANSFER) { /* If data transfer */
    CmdCon |= (1 << 8)    // Set big endian flag for data transfers since this is how the data is in the 16-bit fifo
           |  (1 << 2);   // DATA_EN
  }
  if (CmdFlags & FS_MMC_CMD_FLAG_WRITETRANSFER) {   /* Abort transfer ? */
    CmdCon |= (1 << 3);   // Set WRITE bit
  }
  if (CmdFlags & FS_MMC_CMD_FLAG_SETBUSY) {        /* Set busy ? */
    CmdCon |= (1 << 5);   // Set ABORT bit
  }
  if (CmdFlags & FS_MMC_CMD_FLAG_INITIALIZE) {   /* Init ? */
    CmdCon |= (1 << 6);   // Set ABORT bit
  }
  if (CmdFlags & FS_MMC_CMD_FLAG_USE_SD4MODE) {   /* 4 bit mode ? */
    CmdCon |= (1 << 7);   // Set WIDE bit
  }
  if (CmdFlags & FS_MMC_CMD_FLAG_STOP_TRANS) {   /* Abort transfer ? */
    CmdCon |= (1 << 13);   // Set ABORT bit
  }
  _StopMMCClock(Unit);
  __SDMMC_CMD      = Cmd;
  __SDMMC_CMDCON   = CmdCon;
  __SDMMC_ARGUMENT = Arg;
  _StartMMCClock(Unit);
}

/*********************************************************************
*
*       FS_MMC_HW_X_GetResponse
*
*  Function description
*    Receives the responses that was sent by the card after
*    a command was sent to the card.
*/
int FS_MMC_HW_X_GetResponse(U8 Unit, void *pBuffer, U32 Size) {
  U16      * pResponse;
  unsigned   Index;

  pResponse = (U16 *) pBuffer;
  // Wait for response
  while (1) {
    //
    //  Re enable the clock, since hardware disables clock without having an
    //  chance to retrieve the response from card.
    //
    while (((__SDMMC_STATUS & MMC_STATUS_CLOCK_DISABLED) == MMC_STATUS_CLOCK_DISABLED)) {
      _StartMMCClock(Unit);
      _Delayus(200);
    }
    if ((__SDMMC_STATUS & MMC_STATUS_END_COMMAND_RESPONSE) == MMC_STATUS_END_COMMAND_RESPONSE) {
      break;
    }
    if (__SDMMC_STATUS & MMC_STATUS_RESPONSE_TIMEOUT) {
      return 1;
    }
  }
  // Read the necessary number of response words from the response
  // FIFO
  for (Index = 0; Index < (Size >> 1); Index++) {
    *pResponse++ = (U16)__SDMMC_RES_FIFO;
  }
  return 0;
}

/*********************************************************************
*
*       FS_MMC_HW_X_ReadData
*
*  Function description
*    Reads data from MMC/SD card thru the MMC/SD card controller.
*
*/
int FS_MMC_HW_X_ReadData (U8 Unit, void * pBuffer, unsigned NumBytes, unsigned NumBlocks) {
  unsigned   i;
  U16      * pBuf = (U16 *)pBuffer;
  int        TimeOut;
  do {
    i = 0;
    //
    // It might be necessary to enable the clock.
    // Hardware disables the clock although not all data have not been read.
    //
    if (((__SDMMC_STATUS & MMC_STATUS_CLOCK_DISABLED) == MMC_STATUS_CLOCK_DISABLED)) {
      _StartMMCClock(Unit);
    }
    TimeOut = TIME_OUT_VALUE;
    // Wait until transfer is complete
    while ((__SDMMC_STATUS & MMC_STATUS_FIFO_FULL) == 0) {
      if (__SDMMC_STATUS & (1 << 3)) {
        return FS_MMC_CARD_READ_CRC_ERROR;
      }
      if (__SDMMC_STATUS & (1 << 0)) {
        return FS_MMC_CARD_READ_TIMEOUT;
      }
      if (((__SDMMC_STATUS & MMC_STATUS_CLOCK_DISABLED) == MMC_STATUS_CLOCK_DISABLED)) {
        break; 
      }
      if (--TimeOut == 0) {
        return FS_MMC_CARD_READ_TIMEOUT;
      }
    }
    TimeOut = TIME_OUT_VALUE;
    // Continue reading data until FIFO is empty
    while (((__SDMMC_STATUS & MMC_STATUS_FIFO_EMPTY) == 0) && (i < (NumBytes >> 1))) {
      // Any data in the FIFO
      if ((__SDMMC_STATUS & MMC_STATUS_FIFO_EMPTY) == 0) {
        *pBuf++ = (U16)__SDMMC_DATA_FIFO;
        i++;
      }
      if (--TimeOut == 0) {
        return FS_MMC_CARD_READ_TIMEOUT;
      }
    }
  } while (--NumBlocks); 
  return 0;
}

/*********************************************************************
*
*       FS_MMC_HW_X_WriteData
*
*  Function description
*    Writes the data to MMC/SD card thru the MMC/SD card controller.
*
*         
*/
int FS_MMC_HW_X_WriteData(U8 Unit, const void * pBuffer, unsigned NumBytes, unsigned NumBlocks) {
  unsigned    i;
  const U16 * pBuf;
  int         TimeOut;

  pBuf = (const U16 *)pBuffer;
  do {
    while((__SDMMC_STATUS & MMC_STATUS_FIFO_EMPTY) == 0);
    for (i = 0; i < (NumBytes >> 1); i++) {
      __SDMMC_DATA_FIFO =  *pBuf++;
    }
    _StartMMCClock(Unit);
    if (__SDMMC_STATUS & (1 << 2)) {
      return FS_MMC_CARD_WRITE_CRC_ERROR;
    }
  } while (--NumBlocks);
  //
  // Set an approx. Timeout of 5 sec.
  //
  TimeOut = TIME_OUT_VALUE;
  while (1) {
    if (__SDMMC_STATUS & MMC_STATUS_DATA_TRANFER_DONE) {
      break;
    }
    if (--TimeOut < 0) {
      return FS_MMC_CARD_READ_TIMEOUT;
    }
    
  }
  TimeOut = TIME_OUT_VALUE;
  // Wait until write operation has ended
  while (1) {
    if (__SDMMC_STATUS & MMC_STATUS_DATA_PROGRAM_DONE) {
      break;
    }
    if (--TimeOut < 0) {
      return FS_MMC_CARD_READ_TIMEOUT;
    }
  }
  return 0;
}

/*********************************************************************
*
*       FS_MMC_HW_X_Delay
*
*  Function description
*    Waits for a certain time given by the parameter.
*
*/
void   FS_MMC_HW_X_Delay(int Delay) {
  _Delayus(Delay * 1000);
}

/*********************************************************************
*
*       FS_MMC_HW_X_InitHW
*
*  Function description
*    Initialize the MMC/SD card controller.
*/
void   FS_MMC_HW_X_InitHW(U8 Unit) {
  __SDMMC_CLKC     = 1;
  __SDMMC_RATE     = 0;
  __SDMMC_PREDIV   = 0;
  __SDMMC_CMDCON   = 0;
  __SDMMC_RES_TO   = 0x7F;
  __SDMMC_READ_TO  = 0xFFFF;
  __SDMMC_BLK_LEN  = 0;
  __SDMMC_NUM_BLK  = 0;
  __SDMMC_CMD      = 0;
  __SDMMC_ARGUMENT = 0;
}

/*********************************************************************
*
*       FS_MMC_HW_X_GetMaxReadBurst
*
*  Function description
*    Returns the number of block (sectors)
*    that can be read at once with a single
*    READ_MULTIPLE_SECTORS.
*
*/
U16 FS_MMC_HW_X_GetMaxReadBurst(U8 Unit) {
  return 0xffff;
}

/*********************************************************************
*
*       FS_MMC_HW_X_GetMaxWriteBurst
*
*  Function description
*    Returns the number of block (sectors)
*    that can be written at once with a single
*    WRITE_MULTIPLE_SECTORS.
*/
U16 FS_MMC_HW_X_GetMaxWriteBurst(U8 Unit) {
  return 0xffff;
}

/*********************************************************************
*
*       FS_MMC_HW_X_SetDataPointer
*
*  Function description
*    Tells the hardware layer where to read data from 
*    or write data to. This may be necessary for some controller,
*    before sending the command to the card, eg. programming the DMA.
*    In most cases this function can be left empty.
*
*  Parameters:
*    Unit          - SD card controller no, in case there are more than one.
*
*/
void FS_MMC_HW_X_SetDataPointer(U8 Unit, const void * p) {
  FS_USE_PARA(Unit);
  FS_USE_PARA(p);
}

/*************************** End of file ****************************/
