/*********************************************************************
*                SEGGER MICROCONTROLLER GmbH & Co. KG                *
*        Solutions for real time microcontroller applications        *
**********************************************************************
*                                                                    *
*        (c) 2003-2012     SEGGER Microcontroller GmbH & Co KG       *
*                                                                    *
*        Internet: www.segger.com    Support:  support@segger.com    *
*                                                                    *
**********************************************************************

**** emFile file system for embedded applications ****
emFile is protected by international copyright laws. Knowledge of the
source code may not be used to write a similar product. This file may
only be used in accordance with a license and should not be re-
distributed in any way. We appreciate your understanding and fairness.
----------------------------------------------------------------------
----------------------------------------------------------------------
File        : FS_Driver_LPC3131_EA_LPC31xx.c
Purpose     : SD/MMC driver for Embedded Artists LPC31xx using NXP CDL.
---------------------------END-OF-HEADER------------------------------
*/

/*********************************************************************
*
*       #include Section
*
**********************************************************************
*/

#include "FS_ConfDefaults.h"        /* FS Configuration */

#include "FS_Int.h"
#include "FS_CLib.h"

#include "lpc313x_mci_driver.h"
#include "lpc313x_ioconf_driver.h"

/*********************************************************************
*
*       Defines, configurable
*
*       This section is the only section which requires changes for typical embedded systems
*       using the generic driver with a single device.
*
**********************************************************************
*/
#define ALLOC_SIZE         0x100000       // Size of memory dedicated to the file system. This value should be fine tuned according for your system.

/*********************************************************************
*
*       Static data.
*
*       This section does not require modifications in most systems.
*
**********************************************************************
*/
static U32   _aMemBlock[ALLOC_SIZE / 4];    // Memory pool used for semi-dynamic allocation.
static int   _MCIDev;

/*********************************************************************
*
*       #define constants
*
**********************************************************************
*/
#define   SECTOR_SIZE     512

#ifndef NUM_UNITS
  #define NUM_UNITS         1
#endif

/*********************************************************************
*
*       Type definitions
*
**********************************************************************
*/
typedef struct {
  U8            IsInited;
  U8            Unit;
  U16           NumHeads;
  U16           SectorsPerTrack;
  U32           NumSectors;
  U16           BytesPerSector;
} DRIVER_INST;

/*********************************************************************
*
*       Static data
*
**********************************************************************
*/
static DRIVER_INST  * _apInst[NUM_UNITS];
static int            _NumUnits;

/*********************************************************************
*
*       Static code
*
**********************************************************************
*/

/*********************************************************************
*
*       _Delayms
*/
static void _Delayms(unsigned ms) {
  volatile unsigned Time;

  Time = ms * 10000;
  do {} while(--Time);
}

/*********************************************************************
*
*       _Init
*
*  Description:
*  Resets/Initializes the device
*
* Parameters:
*   Unit        - Unit number.
*
*  Return value:
*   ==0         - Device has been reset and initialized.
*   < 0         - An error has occurred.
*/
static int _Init(DRIVER_INST * pInst) {
  MCI_CARD_INFO_T* pSDCard;

  //
  // Enable power to SD card slot by setting I2STX_DATA0 low
  //
  gpio_set_outpin_low(IOCONF_MUX1, 5);
  //
  // Wait atleast 10msec for card to stablize
  //
  _Delayms(10);
  //
  // Open MCI device
  //
  _MCIDev = mci_open((void*)SD_MMC_BASE, 0);
  pSDCard = (MCI_CARD_INFO_T*)_MCIDev;
  //
  // Check if card is acquired
  //
  if (pSDCard->cid[0] != 0) {
    return 0;   // Card found
  } else {
    return -1;  // Error
  }
}

/*********************************************************************
*
*       _Unmount
*
*  Description:
*    Unmounts the volume.
*
*  Parameters:
*    Unit        - Unit number.
*
*/
static void _Unmount(U8 Unit) {
  if (_apInst[Unit]->IsInited == 1) {
    FS_MEMSET(_apInst[Unit], 0, sizeof(DRIVER_INST));
    //
    // Disable power to SD card slot by setting I2STX_DATA0 high
    //
    gpio_set_outpin_high(IOCONF_MUX1, 5);
  }
}

/*********************************************************************
*
*       Public code (indirectly thru callback)
*
**********************************************************************
*/

/*********************************************************************
*
*       _GetStatus
*
*  Description:
*    FS driver function. Get status of the media.
*
*  Parameters:
*    Unit                  - Unit number.
*
*  Return value:
*    FS_MEDIA_STATE_UNKNOWN - if the state of the media is unknown.
*    FS_MEDIA_NOT_PRESENT   - if no card is present.
*    FS_MEDIA_IS_PRESENT    - if a card is present.
*/
static int _GetStatus(U8 Unit) {
  return FS_MEDIA_IS_PRESENT;
}

/*********************************************************************
*
*       _Read
*
*  Function Description
*    Driver callback function.
*    Reads one or more logical sectors from storage device.
*
*  Parameters:
*    Unit        - Unit number.
*    Sector      - Sector to be read from the device.
*    pBuffer     - Pointer to buffer for storing the data.
*    NumSectors  - Number of sectors to read
*
*  Return value:
*      0                       - Data successfully written.
*    !=0                       - An error has occurred.
*
*/
static int _Read(U8 Unit, U32 SectorNo, void * p, U32 NumSectors) {
  int NumBytesRead;
  int NumRetry = 0;

Retry:
  NumBytesRead = mci_read_blocks(_MCIDev, p, SectorNo, (SectorNo + NumSectors) - 1);
  if (NumBytesRead == (NumSectors * SECTOR_SIZE)) {
    return 0;
  } else {
    if (NumRetry++ > 5) {
      return -1;
    }
    goto Retry;
  }
}

/*********************************************************************
*
*       _Write
*
*  Description:
*    Driver callback function.
*    Writes one or more logical sectors to storage device.
*
*  Parameters:
*    Unit        - Unit number.
*    SectorNo    - Sector number to be written to the device.
*    pBuffer     - Pointer to buffer conating the data to write
*    NumSectors  - Number of sectors to store.
*    RepeatSame  - Repeat the same data to sectors.
*
*  Return value:
*      0                       - Data successfully written.
*    !=0                       - An error has occurred.
*
*/
static int _Write(U8 Unit, U32 SectorNo, const void  * p, U32 NumSectors, U8  RepeatSame) {
  int NumBytesWritten;
  int i;
  U8 * pData;

  pData = (U8*)p;
  for (i = 0; i < NumSectors; i++) {
    NumBytesWritten = mci_write_blocks(_MCIDev, (void*)pData, SectorNo, SectorNo);
    if (NumBytesWritten != SECTOR_SIZE) {
      return -1;
    }
    SectorNo++;
    if (RepeatSame == 0) {
      pData += SECTOR_SIZE;
    }
  }
  return 0;
}

/*********************************************************************
*
*       _DevIoCtl
*
*  Description:
*    FS driver function. Execute device command.
*
*  Parameters:
*    Unit    - Unit number.
*    Cmd         - Command to be executed.
*    Aux         - Parameter depending on command.
*    pBuffer     - Pointer to a buffer used for the command.
*
*  Return value:
*    Command specific. In general a negative value means an error.
*/
static int _IoCtl(U8 Unit, I32 Cmd, I32 Aux, void *pBuffer) {
  DRIVER_INST * pInst;
  FS_DEV_INFO * pInfo;
  int           r;

  pInst = _apInst[Unit];
  r     = -1;
  FS_USE_PARA(Aux);
  switch (Cmd) {
  case FS_CMD_GET_DEVINFO:
    pInfo = (FS_DEV_INFO *)pBuffer;
    pInfo->NumHeads        = pInst->NumHeads;         /* heads */
    pInfo->SectorsPerTrack = pInst->SectorsPerTrack;  /* sec per track */
    pInfo->NumSectors      = pInst->NumSectors;       /* Number of sectors */
    pInfo->BytesPerSector  = SECTOR_SIZE;             /* SD/MMC media are always working with 512 Bytes per sector */
    r = 0;
    break;
  case FS_CMD_UNMOUNT:  
    //
    // (Optional)
    // Device shall be unmounted - sync all operations and mark it as unmounted
    //
    
    _Unmount(Unit);
    r = 0;
    break;
  case FS_CMD_UNMOUNT_FORCED:
    //
    // (Optional)
    // Device shall be unmounted - mark it as unmounted without syncing any pending operations 
    //
    
    // ToDo: Call the function
    break;
  case FS_CMD_SYNC:
    //
    // (Optional)
    // Sync/flush any pending operations 
    //

    // ToDo: Call the function
    break;
  default:
    break;
  }
  return r;
}

/*********************************************************************
*
*       _InitMedium
*
*  Description:
*    Initialize the specified medium.
*
*  Parameters:
*    Unit    - Unit number.
*
*  Return value:
*/
static int _InitMedium(U8 Unit) {
  int i;
  DRIVER_INST * pInst;
  MCI_CARD_INFO_T* pSDCard;

  pInst = _apInst[Unit];
  if (pInst->IsInited == 0) {
    i = _Init(pInst);
    if (i < 0) { /* init failed, no valid card in slot */
      FS_DEBUG_WARN((FS_MTYPE_DRIVER, "SD/MMC: Init failure, no valid card found"));
      return -1;
    } else {
      pSDCard = (MCI_CARD_INFO_T*)_MCIDev;
      pInst->NumSectors = pSDCard->device_size / SECTOR_SIZE;
      pInst->BytesPerSector = SECTOR_SIZE;
      pInst->IsInited = 1;
    }
  }
  return 0;
}

/*********************************************************************
*
*       _AddDevice
*
*  Function Description:
*    Initializes the low-level driver object.
*
*  Return value:
*    >= 0                       - Command succesfully executed, Unit no.
*    <  0                       - Error, could not add device
*
*/
static int _AddDevice(void) {
  U8            Unit;
  DRIVER_INST * pInst;

  if (_NumUnits >= NUM_UNITS) {
    return -1;
  }
  Unit = _NumUnits++;
  pInst = (DRIVER_INST *)FS_AllocZeroed(sizeof(DRIVER_INST));   // Alloc memory. This is guaranteed to work by the memory module.
  _apInst[Unit] = pInst;
  return Unit;
}

/*********************************************************************
*
*       _GetNumUnits
*
*  Function description:
*    
*  Return value:
*    >= 0                       - Command succesfully executed, Unit no.
*    <  0                       - Error, could not add device
*/
static int _GetNumUnits(void) {
  return _NumUnits;
}

/*********************************************************************
*
*       _GetDriverName
*/
static const char * _GetDriverName(U8 Unit) {
  return "FS_LPC31XX_MMC_CM_Driver";
}

/*********************************************************************
*
*       Public data
*
**********************************************************************
*/
const FS_DEVICE_TYPE FS_LPC31XX_MMC_CM_Driver = {
  _GetDriverName,
  _AddDevice,
  _Read,
  _Write,
  _IoCtl,
  _InitMedium,
  _GetStatus,
  _GetNumUnits
};

/*********************************************************************
*
*       Public code
*
*       This section does not require modifications in most systems.
*
**********************************************************************
*/

/*********************************************************************
*
*       FS_X_AddDevices
*
*  Function description
*    This function is called by the FS during FS_Init().
*    It is supposed to add all devices, using primarily FS_AddDevice().
*
*  Note
*    (1) Other API functions
*        Other API functions may NOT be called, since this function is called
*        during initialisation. The devices are not yet ready at this point.
*/
void FS_X_AddDevices(void) {
  FS_AssignMemory(&_aMemBlock[0], sizeof(_aMemBlock));
  //
  //  Add driver
  //
  FS_AddDevice(&FS_LPC31XX_MMC_CM_Driver);
}

/*********************************************************************
*
*       FS_X_GetTimeDate
*
*  Description:
*    Current time and date in a format suitable for the file system.
*
*    Bit 0-4:   2-second count (0-29)
*    Bit 5-10:  Minutes (0-59)
*    Bit 11-15: Hours (0-23)
*    Bit 16-20: Day of month (1-31)
*    Bit 21-24: Month of year (1-12)
*    Bit 25-31: Count of years from 1980 (0-127)
*
*/
U32 FS_X_GetTimeDate(void) {
  U32 r;
  U16 Sec, Min, Hour;
  U16 Day, Month, Year;

  Sec   = 0;        // 0 based.  Valid range: 0..59
  Min   = 0;        // 0 based.  Valid range: 0..59
  Hour  = 0;        // 0 based.  Valid range: 0..23
  Day   = 1;        // 1 based.    Means that 1 is 1. Valid range is 1..31 (depending on month)
  Month = 1;        // 1 based.    Means that January is 1. Valid range is 1..12.
  Year  = 0;        // 1980 based. Means that 2007 would be 27.
  r   = Day + (Month << 5) + (Year  << 9);
  r  |= (U32)(Sec / 2 + (Min << 5) + (Hour  << 11)) << 16;
  return r;
}

/*************************** End of file ****************************/
