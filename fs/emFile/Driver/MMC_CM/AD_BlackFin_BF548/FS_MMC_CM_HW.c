/*********************************************************************
*
*       #include Section
*
**********************************************************************
*/
//#include <cyg/kernel/kapi.h>
//#include <cyg/infra/cyg_trac.h>
//#include <cyg/hal/hal_io.h>
//#include <cyg/hal/hal_cache.h>
#include <cdefBF548.h>
//#include "../../../common/hardware/gpio_setup.h"
#include "FS_Int.h"
#include "MMC_SD_CardMode_X_HW.h"

/*********************************************************************
*
*       #define constants
*
**********************************************************************
*/

#define SCLK                  133000UL        // SCLK given in kHz

/*
 * DMA
 */
#define RESTART                 0x0020  /* DMA Buffer Clear */

/* Bit masks for SDH_COMMAND */
#define                   CMD_IDX  0x3f       /* Command Index */
#define                   CMD_RSP  0x40       /* Response */
#define                 CMD_L_RSP  0x80       /* Long Response */
#define                 CMD_INT_E  0x100      /* Command Interrupt */
#define                CMD_PEND_E  0x200      /* Command Pending */
#define                     CMD_E  0x400      /* Command Enable */

/* Bit masks for SDH_PWR_CTL */
#define                    PWR_ON  0x3        /* Power On */
#define                 SD_CMD_OD  0x40       /* Open Drain Output */
#define                   ROD_CTL  0x80       /* Rod Control */

/* Bit masks for SDH_CLK_CTL */
#define                    CLKDIV  0xff       /* MC_CLK Divisor */
#define                     CLK_E  0x100      /* MC_CLK Bus Clock Enable */
#define                  PWR_SV_E  0x200      /* Power Save Enable */
#define             CLKDIV_BYPASS  0x400      /* Bypass Divisor */
#define                  WIDE_BUS  0x800      /* Wide Bus Mode Enable */

/* Bit masks for SDH_RESP_CMD */
#define                  RESP_CMD  0x3f       /* Response Command */

/* Bit masks for SDH_DATA_CTL */
#define                     DTX_E  0x1        /* Data Transfer Enable */
#define                   DTX_DIR  0x2        /* Data Transfer Direction */
#define                  DTX_MODE  0x4        /* Data Transfer Mode */
#define                 DTX_DMA_E  0x8        /* Data Transfer DMA Enable */
#define              DTX_BLK_LGTH  0xf0       /* Data Transfer Block Length */

/* Bit masks for SDH_STATUS */
#define              CMD_CRC_FAIL  0x1        /* CMD CRC Fail */
#define              DAT_CRC_FAIL  0x2        /* Data CRC Fail */
#define              CMD_TIME_OUT  0x4        /* CMD Time Out */
#define              DAT_TIME_OUT  0x8        /* Data Time Out */
#define               TX_UNDERRUN  0x10       /* Transmit Underrun */
#define                RX_OVERRUN  0x20       /* Receive Overrun */
#define              CMD_RESP_END  0x40       /* CMD Response End */
#define                  CMD_SENT  0x80       /* CMD Sent */
#define                   DAT_END  0x100      /* Data End */
#define             START_BIT_ERR  0x200      /* Start Bit Error */
#define               DAT_BLK_END  0x400      /* Data Block End */
#define                   CMD_ACT  0x800      /* CMD Active */
#define                    TX_ACT  0x1000     /* Transmit Active */
#define                    RX_ACT  0x2000     /* Receive Active */
#define              TX_FIFO_STAT  0x4000     /* Transmit FIFO Status */
#define              RX_FIFO_STAT  0x8000     /* Receive FIFO Status */
#define              TX_FIFO_FULL  0x10000    /* Transmit FIFO Full */
#define              RX_FIFO_FULL  0x20000    /* Receive FIFO Full */
#define              TX_FIFO_ZERO  0x40000    /* Transmit FIFO Empty */
#define               RX_DAT_ZERO  0x80000    /* Receive FIFO Empty */
#define                TX_DAT_RDY  0x100000   /* Transmit Data Available */
#define               RX_FIFO_RDY  0x200000   /* Receive Data Available */

/* Bit masks for SDH_STATUS_CLR */
#define         CMD_CRC_FAIL_STAT  0x1        /* CMD CRC Fail Status */
#define         DAT_CRC_FAIL_STAT  0x2        /* Data CRC Fail Status */
#define          CMD_TIMEOUT_STAT  0x4        /* CMD Time Out Status */
#define          DAT_TIMEOUT_STAT  0x8        /* Data Time Out status */
#define          TX_UNDERRUN_STAT  0x10       /* Transmit Underrun Status */
#define           RX_OVERRUN_STAT  0x20       /* Receive Overrun Status */
#define         CMD_RESP_END_STAT  0x40       /* CMD Response End Status */
#define             CMD_SENT_STAT  0x80       /* CMD Sent Status */
#define              DAT_END_STAT  0x100      /* Data End Status */
#define        START_BIT_ERR_STAT  0x200      /* Start Bit Error Status */
#define          DAT_BLK_END_STAT  0x400      /* Data Block End Status */

/* Bit masks for SDH_MASK0 */
#define         CMD_CRC_FAIL_MASK  0x1        /* CMD CRC Fail Mask */
#define         DAT_CRC_FAIL_MASK  0x2        /* Data CRC Fail Mask */
#define          CMD_TIMEOUT_MASK  0x4        /* CMD Time Out Mask */
#define          DAT_TIMEOUT_MASK  0x8        /* Data Time Out Mask */
#define          TX_UNDERRUN_MASK  0x10       /* Transmit Underrun Mask */
#define           RX_OVERRUN_MASK  0x20       /* Receive Overrun Mask */
#define         CMD_RESP_END_MASK  0x40       /* CMD Response End Mask */
#define             CMD_SENT_MASK  0x80       /* CMD Sent Mask */
#define              DAT_END_MASK  0x100      /* Data End Mask */
#define        START_BIT_ERR_MASK  0x200      /* Start Bit Error Mask */
#define          DAT_BLK_END_MASK  0x400      /* Data Block End Mask */
#define              CMD_ACT_MASK  0x800      /* CMD Active Mask */
#define               TX_ACT_MASK  0x1000     /* Transmit Active Mask */
#define               RX_ACT_MASK  0x2000     /* Receive Active Mask */
#define         TX_FIFO_STAT_MASK  0x4000     /* Transmit FIFO Status Mask */
#define         RX_FIFO_STAT_MASK  0x8000     /* Receive FIFO Status Mask */
#define         TX_FIFO_FULL_MASK  0x10000    /* Transmit FIFO Full Mask */
#define         RX_FIFO_FULL_MASK  0x20000    /* Receive FIFO Full Mask */
#define         TX_FIFO_ZERO_MASK  0x40000    /* Transmit FIFO Empty Mask */
#define          RX_DAT_ZERO_MASK  0x80000    /* Receive FIFO Empty Mask */
#define           TX_DAT_RDY_MASK  0x100000   /* Transmit Data Available Mask */
#define          RX_FIFO_RDY_MASK  0x200000   /* Receive Data Available Mask */

/* Bit masks for SDH_FIFO_CNT */
#define                FIFO_COUNT  0x7fff     /* FIFO Count */

/* Bit masks for SDH_E_STATUS */
#define              SDIO_INT_DET  0x2        /* SDIO Int Detected */
#define               SD_CARD_DET  0x10       /* SD Card Detect */

/* Bit masks for SDH_E_MASK */
#define                  SDIO_MSK  0x2        /* Mask SDIO Int Detected */
#define                   SCD_MSK  0x40       /* Mask Card Detect */

/* Bit masks for SDH_CFG */
#define                   CLKS_EN  0x1        /* Clocks Enable */
#define                      SD4E  0x4        /* SDIO 4-Bit Enable */
#define                       MWE  0x8        /* Moving Window Enable */
#define                    SD_RST  0x10       /* SDMMC Reset */
#define                 PUP_SDDAT  0x20       /* Pull-up SD_DAT */
#define                PUP_SDDAT3  0x40       /* Pull-up SD_DAT3 */
#define                 PD_SDDAT3  0x80       /* Pull-down SD_DAT3 */

/* Bit masks for SDH_RD_WAIT_EN */
#define                       RWR  0x1        /* Read Wait Request */


#define HAL_WRITE_UINT16( _register_, _value_ )         \
                        __asm __volatile( "P0 = %1;"    \
                                          "R0 = %0;"    \
                                          "W[P0] = R0;" \
                                          "SSYNC;" : : "r"(_value_), "r"(_register_) : "P0", "R0");
#define HAL_WRITE_UINT32( _register_, _value_ )         \
                        __asm __volatile( "P0 = %1;"    \
                                          "R0 = %0;"    \
                                          "[P0] = R0;"  \
                                          "SSYNC;" : : "r"(_value_), "r"(_register_) : "P0", "R0");
#define HAL_READ_UINT16( _register_, _value_ )          \
        __asm __volatile(   "P0 = %1;"                  \
                            "R0 = W[P0];"               \
                            "%0 = R0;": "=r"(_value_) : "p"(_register_) : "P0", "R0")

#define HAL_READ_UINT32( _register_, _value_ )          \
        ((_value_) = *((volatile U32 *)(_register_)))

#define GEN_ACCESS_FUNCTION(type,ADR)                   \
        static U##type bfin_read_##ADR(void)                   \
          {                                             \
          U##type result;                               \
          HAL_READ_UINT##type(ADR,result);              \
          return result;                                \
          }                                             \
          \
        static U##type bfin_write_##ADR(U##type value) \
          {                                       \
					HAL_WRITE_UINT##type(ADR,value);\
          return value;                           \
          }

GEN_ACCESS_FUNCTION(16, SDH_E_STATUS)
GEN_ACCESS_FUNCTION(16, SDH_E_MASK)
GEN_ACCESS_FUNCTION(16, SDH_CLK_CTL)
GEN_ACCESS_FUNCTION(16, SDH_COMMAND)
GEN_ACCESS_FUNCTION(16, SDH_RESP_CMD)
GEN_ACCESS_FUNCTION(32, SDH_RESPONSE0)
GEN_ACCESS_FUNCTION(32, SDH_RESPONSE1)
GEN_ACCESS_FUNCTION(32, SDH_RESPONSE2)
GEN_ACCESS_FUNCTION(32, SDH_RESPONSE3)
GEN_ACCESS_FUNCTION(32, SDH_DATA_TIMER)
GEN_ACCESS_FUNCTION(16, SDH_DATA_LGTH)
GEN_ACCESS_FUNCTION(16, SDH_DATA_CTL)
GEN_ACCESS_FUNCTION(16, SDH_DATA_CNT)
GEN_ACCESS_FUNCTION(32, SDH_STATUS)
GEN_ACCESS_FUNCTION(16, SDH_STATUS_CLR)
GEN_ACCESS_FUNCTION(16, SDH_CFG)
GEN_ACCESS_FUNCTION(32, SDH_ARGUMENT)
GEN_ACCESS_FUNCTION(16, SDH_PWR_CTL)

GEN_ACCESS_FUNCTION(32, DMA22_START_ADDR)
GEN_ACCESS_FUNCTION(16, DMA22_X_COUNT)
GEN_ACCESS_FUNCTION(16, DMA22_Y_COUNT)
GEN_ACCESS_FUNCTION(16, DMA22_X_MODIFY)
GEN_ACCESS_FUNCTION(16, DMA22_Y_MODIFY)
GEN_ACCESS_FUNCTION(16, DMA22_CONFIG)

GEN_ACCESS_FUNCTION(16, DMAC1_PERIMUX);

GEN_ACCESS_FUNCTION(16, PORTC_FER);
GEN_ACCESS_FUNCTION(32, PORTC_MUX);

#define bfin_write_DMA_START_ADDR      bfin_write_DMA22_START_ADDR
#define bfin_write_DMA_X_COUNT         bfin_write_DMA22_X_COUNT
#define bfin_write_DMA_X_MODIFY        bfin_write_DMA22_X_MODIFY
#define bfin_write_DMA_CONFIG          bfin_write_DMA22_CONFIG


#define GET_BITS(aResp, StartBitPos, NumBits)                                          \
        ({                                                                   \
                const int __size = NumBits;                                     \
                const uint32_t __mask = (__size < 32 ? 1 << __size : 0) - 1; \
                const int32_t __off = 3 - ((StartBitPos) / 32);                    \
                const int32_t __shft = (StartBitPos) & 31;                         \
                uint32_t __res;                                              \
                \
                __res = aResp[__off] >> __shft;                          \
                if (__size + __shft > 32)                               \
                        __res |= aResp[__off-1] << ((32 - __shft) % 32); \
                __res & __mask;                                         \
        })

        
/*********************************************************************
*
*       Static data
*
**********************************************************************
*/
static U32      _aResponse[4];
static unsigned _Status;
static unsigned _IgnoreCRC;

/*********************************************************************
*
*       Static code
*
**********************************************************************
*/
/*********************************************************************
*
*       _ld
*/
static U16 _ld(U32 Value) {
  U16 i;

  for (i = 0; i < 16; i++) {
    if ((1UL << i) == Value) {
      break;
    }
  }
  return i;
}

/*********************************************************************
*
*       __delay
*
*/
static __inline__ void __delay(volatile U32 loops) {
  do { 
    __asm __volatile("nop;");
  } while (--loops);
}

/*********************************************************************
*
*       udelay
*
*  Function description:
*   Use only for very small delays ( < 1 msec).  Should probably use a
*   lookup table, really, as the multiplications take much too long with
*   short delays.  This is a "reasonable" implementation, though (and the
*   first constant multiplications gets optimized away if the delay is
*   a constant)
*
*
*/
static void udelay(U32 usecs) {
  __delay(usecs * 40);
}

/*********************************************************************
*
*       Public code
*
**********************************************************************
*/

/*********************************************************************
*
*       FS_MMC_HW_X_ReadData
*
*/
/*********************************************************************
*
*       FS_MMC_HW_X_ReadData
*
*  Function description:
*    Reads data from MMC/SD card through the MMC/SD card controller.
*
*  Parameters:
*    Unit          - SD card controller no, in case there are more than one.
*    pBuffer       - Pointer to the buffer to store the read data
*    NumBytes      - Number of bytes to be read for each block/sector
*    NumBlocks     - Number of block/sectors to read.
*  
*  Return value:
*    FS_MMC_CARD_NO_ERROR        - Data have been successfully transferred and were written by card.
*    FS_MMC_CARD_READ_CRC_ERROR  - A CRC error occurred during data transfer.
*
*/
int FS_MMC_HW_X_ReadData(U8 Unit, void * pBuffer, unsigned NumBytes, unsigned NumBlocks) {
  U16 DataCtl = 0;
  U16 DMACfg = 0;
  U16 Status;
  unsigned i;

  FS_USE_PARA(Unit);
  //debug("MMC_ReadSectors: DevId %d, StartSector %d, NumSectors %d, (_MMCBlockDev.blksz=%d)\n", DevId, StartSector, NumSectors,_MMCBlockDev.blksz);
  //
  // Setup the data control register.
  //
  DataCtl |= _ld(NumBytes) << 4;
  DataCtl |= DTX_DIR;
  bfin_write_SDH_DATA_CTL(DataCtl);
  DMACfg |= WDSIZE_32 | RESTART | WNR | DMAEN;
  //
  // Initialize/Prepare the DMA for read transfer
  //
//  HAL_DCACHE_INVALIDATE_ALL();
  bfin_write_DMA_START_ADDR((unsigned int)(pBuffer));
  bfin_write_DMA_X_COUNT(NumBytes * NumBlocks / 4);
  bfin_write_DMA_X_MODIFY(4);
  bfin_write_DMA_CONFIG(DMACfg);
  bfin_write_SDH_DATA_LGTH(NumBytes * NumBlocks);
  bfin_write_SDH_DATA_CTL(bfin_read_SDH_DATA_CTL() | DTX_DMA_E | DTX_E);
  //
  // For each block, we need to check, whether the block was successfully transferred by the DMA.
  // If this is not confirmed, the next block/sector is not transferred from the card.
  //
  for (i= 0; i < NumBlocks; i++) {
    //
    //  Check that the data end of a block/sector was received or whether an error occurred.
    //
    do {
      udelay(1);
      Status = bfin_read_SDH_STATUS();
    } while (!(Status & (DAT_END | DAT_TIME_OUT | DAT_CRC_FAIL | RX_OVERRUN)));
    //
    // In case of an error, reset the status register and return to 
    // the upper layer, that an error occurred.
    //
    if (Status & (DAT_TIME_OUT | DAT_CRC_FAIL | RX_OVERRUN)) {
      bfin_write_SDH_STATUS_CLR(DAT_TIMEOUT_STAT | DAT_CRC_FAIL_STAT | RX_OVERRUN_STAT);
      if ((Status & DAT_TIME_OUT) || (Status && RX_OVERRUN)) {
        return FS_MMC_CARD_READ_TIMEOUT;
      } else {
        return FS_MMC_CARD_READ_CRC_ERROR;
      }
    } else {
      bfin_write_SDH_STATUS_CLR(DAT_BLK_END_STAT | DAT_END_STAT);
    }
  }
  return FS_MMC_CARD_NO_ERROR;
}

/*********************************************************************
*
*       FS_MMC_HW_X_WriteData
*
*  Function description:
*    Writes the data to MMC/SD card through the MMC/SD card controller.
*
*  Parameters:
*    Unit          - SD card controller no, in case there are more than one.
*    pBuffer       - Pointer to the buffer containing the data to send
*    NumBytes      - Number of bytes to be sent for each block/sector
*    NumBlocks     - Number of block/sectors to send.
*  
*  Return value:
*    FS_MMC_CARD_NO_ERROR        - Data have been successfully transferred and were written by card.
*    FS_MMC_CARD_WRITE_CRC_ERROR - A CRC error occurred during data transfer.
*
*/
int FS_MMC_HW_X_WriteData(U8 Unit, const void * pBuffer, unsigned NumBytes, unsigned NumBlocks) {
  U16 DataCtl = 0;
  U16 DMACfg = 0;
  U16 Status;
  unsigned i;

  FS_USE_PARA(Unit);
  //
  // Setup the data control register.
  //
  DataCtl |= _ld(NumBytes) << 4;
  DataCtl &= ~DTX_DIR;
  bfin_write_SDH_DATA_CTL(DataCtl);
  DMACfg |= WDSIZE_32 | RESTART | DMAEN;
//  HAL_DCACHE_FLUSH(pBuffer /*,NumBytes * NumBlocks*/);
  //
  // Initialize/Prepare the DMA for write transfer
  //
  bfin_write_DMA_START_ADDR((unsigned int)(pBuffer));
  bfin_write_DMA_X_COUNT(NumBytes * NumBlocks / 4);
  bfin_write_DMA_X_MODIFY(4);
  bfin_write_DMA_CONFIG(DMACfg);
  bfin_write_SDH_DATA_LGTH(NumBytes * NumBlocks);
  udelay(100);
  bfin_write_SDH_DATA_CTL(bfin_read_SDH_DATA_CTL() | DTX_DMA_E | DTX_E);
  //
  // For each block, we need to check, whether the block was successfully transferred by the DMA.
  // If this is not confirmed, the next block/sector is not transferred to the card.
  //
  for (i= 0; i < NumBlocks; i++) {
    //
    //  Check that data end of a block/sector was confirmed or whether an error occurred.
    //
    do {
      udelay(1);
      Status = bfin_read_SDH_STATUS();
    } while (!(Status & (DAT_END | DAT_TIME_OUT | DAT_CRC_FAIL | TX_UNDERRUN)));
    //
    // In case of an error, reset the status register and return to 
    // the upper layer, that an error occurred.
    //
    if (Status & (DAT_TIME_OUT | DAT_CRC_FAIL | TX_UNDERRUN)) {
      bfin_write_SDH_STATUS_CLR(DAT_TIMEOUT_STAT | DAT_CRC_FAIL_STAT | TX_UNDERRUN_STAT);
      return FS_MMC_CARD_WRITE_CRC_ERROR;

    } else {
      bfin_write_SDH_STATUS_CLR(DAT_BLK_END_STAT | DAT_END_STAT);
    }
  }
  return FS_MMC_CARD_NO_ERROR;
}

/*********************************************************************
*
*       FS_MMC_HW_X_GetResponse
*
*  Function description:
*    Receives the responses that was sent by the card after
*    a command was sent to the card.
*
*  Parameters:
*    Unit          - SD card controller no, in case there are more than one.
*    pBuffer       - Pointer to the buffer to store the received response.
*    NumBytes      - Number of bytes to be read from the card.
*  
*  Return value:
*    FS_MMC_CARD_NO_ERROR           - Response successfully received.
*    FS_MMC_CARD_RESPONSE_TIMEOUT   - Card did not answer, a time-out occurred.
*    FS_MMC_CARD_RESPONSE_CRC_ERROR - A CRC error occurred during receiving the response.
*
*/
int FS_MMC_HW_X_GetResponse(U8 Unit, void * pBuffer, U32 NumBytes) {
  U8   * p;
  U32  * pResp;
  
  //
  // The status was received in the FS_MMC_HW_X_SendCmd() function.
  // We simply check whether we have received an error.
  //
  if (_Status & CMD_TIMEOUT) {
    return FS_MMC_CARD_RESPONSE_TIMEOUT;
  } else if ((_Status & CMD_CRC_FAIL) && (_IgnoreCRC == 0)) {
    return FS_MMC_CARD_RESPONSE_CRC_ERROR;
  }
  //
  // The response was received in the FS_MMC_HW_X_SendCmd() function
  // the response is here copied to the buffer that is supplied with this function.
  // 
  p = (U8 *)pBuffer;
  *p++ = bfin_read_SDH_RESP_CMD();
  NumBytes--;
  pResp = (U32 *)(&_aResponse[0]);
  do {
    U32 Data32;

    Data32 = FS_LoadU32BE((const U8 *)pResp);
    FS_StoreU32LE(p, Data32);
    NumBytes -= 4;
    pResp++;
    p += 4;
  } while (NumBytes >= 4);
  return FS_MMC_CARD_NO_ERROR;
}

/*********************************************************************
*
*       FS_MMC_HW_X_SendCmd
*
*  Function description:
*    Sends a command to the MMC/SD card.
*
*  Parameters:
*    Unit          - SD card controller no, in case there are more than one.
*    Cmd           - The command that shall be send to the card.
*    CmdFlags      - Command flags what may be necessary to be set by the card controller.
*                     Available flags:
*                       FS_MMC_CMD_FLAG_DATATRANSFER  : This command includes a data transfer
*                       FS_MMC_CMD_FLAG_WRITETRANSFER : This command includes a write data transfer
*                       FS_MMC_CMD_FLAG_SETBUSY       : The response from cards also includes a busy wait
*                       FS_MMC_CMD_FLAG_INITIALIZE    : The initialize clock sequence needs to be done with this command.
*                       FS_MMC_CMD_FLAG_USE_SD4MODE   : Data transfer is done in 4-bit mode.
*                       FS_MMC_CMD_FLAG_STOP_TRANS    : This command is a stop transfer command.
*    ResponseType  -  The expected response from the card:
*                     Available response types:
*                       FS_MMC_RESPONSE_FORMAT_NONE   : There is no response expected.
*                       FS_MMC_RESPONSE_FORMAT_R1     : A 48  bit response is expected: [ 47..0].
*                       FS_MMC_RESPONSE_FORMAT_R2     : A 136 bit response is expected: [135..0].
*                       FS_MMC_RESPONSE_FORMAT_R3     : Same as FS_MMC_RESPONSE_FORMAT_R1, but the CRC shall be ignored.
*                       FS_MMC_RESPONSE_FORMAT_R6     : Same as FS_MMC_RESPONSE_FORMAT_R1.
*    Arg           -  The argument that shall be passed with the command.
*  
*/
void FS_MMC_HW_X_SendCmd(U8 Unit, unsigned Cmd, unsigned CmdFlags, unsigned ResponseType, U32 Arg) {
  unsigned int SDHCmd;
  int          Timeout = 0xF00000;

  FS_USE_PARA(Unit);
  //
  // Reset all relevant variables that are used with SendCmd/GetResponse
  //
  _Status    = 0;
  _IgnoreCRC = 0;
  //
  // Reset SDH status register
  //
  bfin_write_SDH_STATUS_CLR(CMD_SENT_STAT | CMD_RESP_END_STAT | CMD_TIMEOUT_STAT | CMD_CRC_FAIL_STAT);
  //
  // Prepare the command by evaluating the flags and response types
  //
  SDHCmd  = 0;
  SDHCmd |= Cmd;
  if (ResponseType != FS_MMC_RESPONSE_FORMAT_NONE) {
    SDHCmd |= CMD_RSP;
  } 
  if (ResponseType == FS_MMC_RESPONSE_FORMAT_R2) {
    SDHCmd |= CMD_L_RSP;
  }
  if (ResponseType == FS_MMC_RESPONSE_FORMAT_R3) {
    _IgnoreCRC = 1;
  }
  //
  // When 4-bit shall be used, we set this here
  //
  if (CmdFlags & FS_MMC_CMD_FLAG_USE_SD4MODE) {
    bfin_write_SDH_CLK_CTL(bfin_read_SDH_CLK_CTL() | WIDE_BUS);
  } else {
    bfin_write_SDH_CLK_CTL(bfin_read_SDH_CLK_CTL() & ~WIDE_BUS);
  } 
  //
  //  Write the argument/command
  //
  bfin_write_SDH_ARGUMENT(Arg);
  bfin_write_SDH_COMMAND(SDHCmd | CMD_E);
  //
  //  Now wait till we receive an answer
  //
  do {
    udelay(1);
    _Status = bfin_read_SDH_STATUS();
    Timeout--;
    if (Timeout==0 ){
//    CYG_TRACE1(1,"Status = 0x%8.8x",(int)Status);
      break;
    }
  } while (!(_Status &(CMD_SENT | CMD_RESP_END | CMD_TIME_OUT | CMD_CRC_FAIL)));
  //
  //  When this commands expects an answer, we store it locally and will send it when 
  //  the function FS_MMC_HW_X_GetResponse() is called.
  //
  if (ResponseType != FS_MMC_RESPONSE_FORMAT_NONE) {
    _aResponse[0] = bfin_read_SDH_RESPONSE0();

    if (ResponseType == FS_MMC_RESPONSE_FORMAT_R2) {
      _aResponse[1] = bfin_read_SDH_RESPONSE1();
      _aResponse[2] = bfin_read_SDH_RESPONSE2();
      _aResponse[3] = bfin_read_SDH_RESPONSE3();
    }
  }
  bfin_write_SDH_STATUS_CLR(CMD_SENT_STAT | CMD_RESP_END_STAT | CMD_TIMEOUT_STAT | CMD_CRC_FAIL_STAT);
}

/*********************************************************************
*
*       FS_MMC_HW_X_SetMaxSpeed
*
*  Function description
*    Sets the frequency of the MMC/SD card controller.
*    The frequency is given in kHz.
*    It is called 2 times:
*     1. During card initialization
*        Initialize the frequency to not more than 400kHz.
*        
*     2. After card initialization
*        The CSD register of card is read and the max frequency 
*        the card can operate is determined.
*        [In most cases: MMC cards 20MHz, SD cards 25MHz]
*
*/
U16 FS_MMC_HW_X_SetMaxSpeed(U8 Unit, U16 Freq) {
  U32 SysClk;
  U32 ClkDiv;
  U16 ClkCtl = 0;
  
  FS_USE_PARA(Unit);
  //
  // setting SD_CLK
  //
  SysClk = SCLK; //get_sclk();
  bfin_write_SDH_CLK_CTL(0);
  if (SysClk % (2 * Freq) == 0) {
    ClkDiv = SysClk / (2 * Freq) - 1;
  } else {
    ClkDiv = SysClk / (2 * Freq);
  }
  if (ClkDiv > 0xff) {
    ClkDiv = 0xff;
  }
  ClkCtl |= (ClkDiv & 0xff);
  ClkCtl |= CLK_E;
  bfin_write_SDH_CLK_CTL(ClkCtl);
  return SysClk / (2 * (ClkDiv + 1));
}

/*********************************************************************
*
*       FS_MMC_HW_X_InitHW
*
*/
void FS_MMC_HW_X_InitHW(U8 Unit) {
  //		GPIO_SET( GPIO_SDEN ,1);
  U16 pwr_ctl = 0;
  int r;
  U32 Factor;
  
  FS_USE_PARA(Unit);
  /* Initialize SDH controller */
  bfin_write_DMAC1_PERIMUX(bfin_read_DMAC1_PERIMUX() | 0x1);
  bfin_write_PORTC_FER(bfin_read_PORTC_FER() | 0x3F00);
  bfin_write_PORTC_MUX(bfin_read_PORTC_MUX() & ~0xFFF0000);

  bfin_write_SDH_E_MASK(0x0);
  bfin_write_SDH_E_STATUS(SD_CARD_DET | SDIO_INT_DET);
  //		GPIO_SET_DIR( GPIO_SDEN ,1);
  //		GPIO_SET( GPIO_SDEN ,0);
  // Soft reset SDH
  *(volatile short * )SDH_CFG = SD_RST;
  asm volatile("ssync;");

  // pull down on DATA3, pull up on DATA2-0, SDH clock enabled
  *(volatile short * )SDH_CFG |= (CLKS_EN | PUP_SDDAT | PD_SDDAT3);
  asm volatile ("ssync;");
  pwr_ctl |= PWR_ON;
  bfin_write_SDH_PWR_CTL(pwr_ctl);
  bfin_write_SDH_E_MASK(bfin_read_SDH_E_MASK() | (SCD_MSK | SDIO_MSK));
  udelay(10000);
}

/*********************************************************************
*
*       FS_MMC_HW_X_GetMaxReadBurst
*
*  Function description
*    Returns the number of block (sectors)
*    that can be read at once with a single
*    READ_MULTIPLE_SECTORS command.
*
*/
U16 FS_MMC_HW_X_GetMaxReadBurst(U8 Unit) {
  FS_USE_PARA(Unit);
  //
  // Since the DMA can transfer a max of 65535 (0xffff) bytes at once,
  // we simply say that 127 sectors (127 * 512 bytes = 65024 bytes)
  // can be transferred at once.
  //
  return 127;
}

/*********************************************************************
*
*       FS_MMC_HW_X_GetMaxWriteBurst
*
*  Function description
*    Returns the number of block (sectors)
*    that can be written at once with a single
*    WRITE_MULTIPLE_SECTORS command.
*/
U16 FS_MMC_HW_X_GetMaxWriteBurst(U8 Unit) {
  FS_USE_PARA(Unit);
  //
  // Since the DMA can transfer a max of 65535 (0xffff) bytes at once,
  // we simply say that 127 sectors (127 * 512 bytes = 65024 bytes)
  // can be transferred at once.
  //
  return 127;
}

/*********************************************************************
*
*       FS_MMC_HW_X_SetResponseTimeOut
*
*  Function description
*    Sets the response time out value given in MMC/SD card cycles.
*
*/
void FS_MMC_HW_X_SetResponseTimeOut(U8 Unit, U32 Value) {
  FS_USE_PARA(Unit);
  //
  //  Not available on the SD controller
  //
}

/*********************************************************************
*
*       FS_MMC_HW_X_SetReadDataTimeOut
*
*  Function description
*    Sets the read data time out value given in MMC/SD card cycles.
*/
void FS_MMC_HW_X_SetReadDataTimeOut(U8 Unit, U32 Value) {
  FS_USE_PARA(Unit);
  bfin_write_SDH_DATA_TIMER(Value);
}

/*********************************************************************
*
*       FS_MMC_HW_X_SetHWBlockLen
*
*  Function description
*    Sets the block size (sector size) that shall be transferred.
*/
void FS_MMC_HW_X_SetHWBlockLen(U8 Unit, U16 BlockSize) {
  FS_USE_PARA(Unit);
  //
  //  Not necessary with the SD controller
  //
}

/*********************************************************************
*
*       FS_MMC_HW_X_SetHWNumBlocks
*
*  Function description
*    Sets the number of block (sectors) to be transferred.
*
*/
void FS_MMC_HW_X_SetHWNumBlocks(U8 Unit, U16 NumBlocks) {
  FS_USE_PARA(Unit);
  //
  //  Not necessary with the SD controller
  //
}

/*********************************************************************
*
*       FS_MMC_HW_X_Delay
*
*  Function description
*    Waits for a certain time given by the parameter.
*
*  Parameters:
*    ms            - Number of milli seconds to wait.
*
*/
void FS_MMC_HW_X_Delay(int ms) {
  udelay(1000*ms);
}

/*********************************************************************
*
*       FS_MMC_HW_X_IsPresent
*
*  Function description
*    Returns the state of the media. If you do not know the state, return
*    FS_MEDIA_STATE_UNKNOWN and the higher layer will try to figure out if
*    a media is present.
*
*  Parameters:
*    Unit          - SD card controller no, in case there are more than one.
*
*  Return value:
*    FS_MEDIA_STATE_UNKNOWN      - the state of the media is unknown
*    FS_MEDIA_NOT_PRESENT        - no card is present
*    FS_MEDIA_IS_PRESENT         - a card is present
*/
int FS_MMC_HW_X_IsPresent(U8 Unit) {
  FS_USE_PARA(Unit);
  return FS_MEDIA_IS_PRESENT;
}

/*********************************************************************
*
*       FS_MMC_HW_X_IsWriteProtected
*
*  Function description
*    Returns whether card is write protected or not.
*
*  Parameters:
*    Unit          - SD card controller no, in case there are more than one.
*
*  Return value:
*    0             - Card is not write protected.
*    1             - Card is     write protected.
*/
int FS_MMC_HW_X_IsWriteProtected(U8 Unit) {
  FS_USE_PARA(Unit);
  return 0;
}

/*********************************************************************
*
*       FS_MMC_HW_X_SetDataPointer
*
*  Function description
*    Tells the hardware layer where to read data from 
*    or write data to. This may be necessary for some controller,
*    before sending the command to the card, eg. programming the DMA.
*    In most cases this function can be left empty.
*
*  Parameters:
*    Unit          - SD card controller no, in case there are more than one.
*
*/
void FS_MMC_HW_X_SetDataPointer(U8 Unit, const void * p) {
  FS_USE_PARA(Unit);
  FS_USE_PARA(p);
}

/*************************** End of file ****************************/
