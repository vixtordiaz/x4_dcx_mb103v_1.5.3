/*********************************************************************
*                SEGGER MICROCONTROLLER GmbH & Co. KG                *
*        Solutions for real time microcontroller applications        *
**********************************************************************
*                                                                    *
*        (c) 2003-2012     SEGGER Microcontroller GmbH & Co KG       *
*                                                                    *
*        Internet: www.segger.com    Support:  support@segger.com    *
*                                                                    *
**********************************************************************

**** emFile file system for embedded applications ****
emFile is protected by international copyright laws. Knowledge of the
source code may not be used to write a similar product. This file may
only be used in accordance with a license and should not be re-
distributed in any way. We appreciate your understanding and fairness.
----------------------------------------------------------------------
----------------------------------------------------------------------
File        : MMC_X_hw.c
Purpose     : Low level MMC/SD driver for the Sharp LH7A404
---------------------------END-OF-HEADER------------------------------
*/

/*********************************************************************
*
*       Includes
*
**********************************************************************
*/
#include "FS.h"
#include "MMC_SD_CardMode_X_HW.h"

/*********************************************************************
*
*       Defines non configurable
*
**********************************************************************
*/

/*********************************************************************
*
*       Local defines (sfrs)
*
**********************************************************************
*/

/*********************************************************************
*
*       Public code
*
**********************************************************************
*/


/*********************************************************************
*
*       FS_MMC_HW_X_SetHWNumBlocks
*
*  Function description
*    Sets the number of block (sectors) to be transferred.
*
*/
void FS_MMC_HW_X_SetHWNumBlocks(U8 Unit, U16 NumBlocks) {
}

/*********************************************************************
*
*       FS_MMC_HW_X_SetHWBlockLen
*
*  Function description
*    Sets the block size (sector size) that shall be transferred.
*/
void FS_MMC_HW_X_SetHWBlockLen(U8 Unit, U16 BlockSize) {
}


/*********************************************************************
*
*       FS_MMC_HW_X_SetMaxSpeed
*
*  Function description
*    Sets the frequency of the MMC/SD card controller.
*    The frequency is given in kHz.
*    It is called 2 times:
*     1. During card initialization
*        Initialize the frequency to not more than 400kHz.
*        
*     2. After card initialization
*        The CSD register of card is read and the max frequency 
*        the card can operate is determined.
*        [In most cases: MMC cards 20MHz, SD cards 25MHz]
*
*/
U16 FS_MMC_HW_X_SetMaxSpeed(U8 Unit, U16 Freq) {
  return Freq;
}

/*********************************************************************
*
*       FS_MMC_HW_X_IsPresent
*
*    Returns the state of the media. If you do not know the state, return
*    FS_MEDIA_STATE_UNKNOWN and the higher layer will try to figure out if
*    a media is present.
*
*  Parameters:
*    Unit                 - Device Index
*
*  Return value:
*    FS_MEDIA_STATE_UNKNOWN      - the state of the media is unkown
*    FS_MEDIA_NOT_PRESENT        - no card is present
*    FS_MEDIA_IS_PRESENT         - a card is present
*/
int FS_MMC_HW_X_IsPresent(U8 Unit) {
  return FS_MEDIA_IS_PRESENT;
}

/*********************************************************************
*
*       FS_MMC_HW_X_IsWriteProtected
*
*  Function description
*    Returns wheter card is write protected or not.
*/
int    FS_MMC_HW_X_IsWriteProtected  (U8 Unit) {
  return 0;
}

/*********************************************************************
*
*       FS_MMC_HW_X_SetResponseTimeOut
*
*  Function description
*    Sets the reponse time out value given in MMC/SD card cycles.
*
*/
void FS_MMC_HW_X_SetResponseTimeOut(U8 Unit, U32 Value) {
}

/*********************************************************************
*
*       FS_MMC_HW_X_SetReadDataTimeOut
*
*  Function description
*    Sets the read data time out value given in MMC/SD card cycles.
*/
void FS_MMC_HW_X_SetReadDataTimeOut(U8 Unit, U32 Value) {
}

/*********************************************************************
*
*       FS_MMC_HW_X_SendCmd
*
*  Function description
*    Sends a command to the MMC/SD card.
*/
void FS_MMC_HW_X_SendCmd(U8 Unit, unsigned Cmd, unsigned CmdFlags, unsigned ResponseType, U32 Arg) {
}

/*********************************************************************
*
*       FS_MMC_HW_X_GetResponse
*
*  Function description
*    Receives the reponses that was sent by the card after
*    a command was sent to the card.
*/
int FS_MMC_HW_X_GetResponse(U8 Unit, void *pBuffer, U32 Size) {
  return 0;
}

/*********************************************************************
*
*       FS_MMC_HW_X_ReadData
*
*  Function description
*    Reads data from MMC/SD card through the MMC/SD card controller.
*
*/
int FS_MMC_HW_X_ReadData (U8 Unit, void * pBuffer, unsigned NumBytes, unsigned NumBlocks) {
  return 0;
}

/*********************************************************************
*
*       FS_MMC_HW_X_WriteData
*
*  Function description
*    Writes the data to MMC/SD card through the MMC/SD card controller.
*
*         
*/
int FS_MMC_HW_X_WriteData(U8 Unit, const void * pBuffer, unsigned NumBytes, unsigned NumBlocks) {
  return 0;
}

/*********************************************************************
*
*       FS_MMC_HW_X_Delay
*
*  Function description
*    Waits for a certain time given by the parameter.
*
*/
void FS_MMC_HW_X_Delay(int ms) {
}

/*********************************************************************
*
*       FS_MMC_HW_X_InitHW
*
*  Function description
*    Initialize the MMC/SD card controller.
*/
void FS_MMC_HW_X_InitHW(U8 Unit) {
}

/*********************************************************************
*
*       FS_MMC_HW_X_GetMaxReadBurst
*
*  Function description
*    Returns the number of block (sectors)
*    that can be read at once with a single
*    READ_MULTIPLE_SECTORS.
*
*/
U16 FS_MMC_HW_X_GetMaxReadBurst(U8 Unit) {
  return 1;
}

/*********************************************************************
*
*       FS_MMC_HW_X_GetMaxWriteBurst
*
*  Function description
*    Returns the number of block (sectors)
*    that can be written at once with a single
*    WRITE_MULTIPLE_SECTORS.
*/
U16 FS_MMC_HW_X_GetMaxWriteBurst(U8 Unit) {
  return 1;
}

/*********************************************************************
*
*       FS_MMC_HW_X_SetDataPointer
*
*  Function description
*    Tells the hardware layer where to read data from 
*    or write data to. This may be necessary for some controller,
*    before sending the command to the card, eg. programming the DMA.
*    In most cases this function can be left empty.
*
*  Parameters:
*    Unit          - SD card controller no, in case there are more than one.
*
*/
void FS_MMC_HW_X_SetDataPointer(U8 Unit, const void * p) {
  FS_USE_PARA(Unit);
  FS_USE_PARA(p);
}
