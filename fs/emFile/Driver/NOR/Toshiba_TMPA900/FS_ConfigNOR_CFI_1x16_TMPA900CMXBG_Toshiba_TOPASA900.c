/*********************************************************************
*                SEGGER MICROCONTROLLER GmbH & Co. KG                *
*        Solutions for real time microcontroller applications        *
**********************************************************************
*                                                                    *
*        (c) 2003-2012     SEGGER Microcontroller GmbH & Co KG       *
*                                                                    *
*        Internet: www.segger.com    Support:  support@segger.com    *
*                                                                    *
**********************************************************************

**** emFile file system for embedded applications ****
emFile is protected by international copyright laws. Knowledge of the
source code may not be used to write a similar product. This file may
only be used in accordance with a license and should not be re-
distributed in any way. We appreciate your understanding and fairness.
----------------------------------------------------------------------
File        : FS_ConfigNOR_CFI_1x16_TMPA900CMXBG_Toshiba_TOPASA900.c
Purpose     : Configuration file for FS with 1 * 16bit CFI compliant NOR flash
---------------------------END-OF-HEADER------------------------------
*/


#include <stdio.h>
#include "FS.h"

/*********************************************************************
*
*       Defines, configurable
*
*       This section is the only section which requires changes for typical embedded systems
*       using the NOR flash driver with a single device.
*
**********************************************************************
*/
#define ALLOC_SIZE         0x100000       // Size of memory dedicated to the file system. This value should be fine tuned according for your system.
#define FLASH0_BASE_ADDR   0x80000000     // Base addr of the NOR flash device to be used as storage
#define FLASH0_START_ADDR  0x81000000     // Start addr of the first sector be used as storage. If the entire chip is used for file system, it is identical to the base addr.
#define FLASH0_SIZE        0x00400000     // Number of bytes to be used for storage

/*********************************************************************
*
*       Static data.
*
*       This section does not require modifications in most systems.
*
**********************************************************************
*/
static U32   _aMemBlock[ALLOC_SIZE / 4];    // Memory pool used for semi-dynamic allocation.

/*********************************************************************
*
*       Public code
*
*       This section does not require modifications in most systems.
*
**********************************************************************
*/

/*********************************************************************
*
*       FS_X_AddDevices
*
*  Function description
*    This function is called by the FS during FS_Init().
*    It is supposed to add all devices, using primarily FS_AddDevice().
*
*  Note
*    (1) Other API functions
*        Other API functions may NOT be called, since this function is called
*        during initialisation. The devices are not yet ready at this point.
*/
void FS_X_AddDevices(void) {
  FS_AssignMemory(&_aMemBlock[0], sizeof(_aMemBlock));
  //
  //  Add driver
  //
  FS_AddDevice(&FS_NOR_Driver);
  //
  //  Confgure the NOR flash interface
  //
  FS_NOR_SetPhyType(0, &FS_NOR_PHY_CFI_1x16);
  FS_NOR_Configure(0, FLASH0_BASE_ADDR, FLASH0_START_ADDR, FLASH0_SIZE);
}

/*********************************************************************
*
*       FS_X_GetTimeDate
*
*  Description:
*    Current time and date in a format suitable for the file system.
*
*    Bit 0-4:   2-second count (0-29)
*    Bit 5-10:  Minutes (0-59)
*    Bit 11-15: Hours (0-23)
*    Bit 16-20: Day of month (1-31)
*    Bit 21-24: Month of year (1-12)
*    Bit 25-31: Count of years from 1980 (0-127)
*
*/
U32 FS_X_GetTimeDate(void) {
  U32 r;
  U16 Sec, Min, Hour;
  U16 Day, Month, Year;

  Sec   = 0;        // 0 based.  Valid range: 0..59
  Min   = 0;        // 0 based.  Valid range: 0..59
  Hour  = 0;        // 0 based.  Valid range: 0..23
  Day   = 1;        // 1 based.    Means that 1 is 1. Valid range is 1..31 (depending on month)
  Month = 1;        // 1 based.    Means that January is 1. Valid range is 1..12.
  Year  = 0;        // 1980 based. Means that 2007 would be 27.
  r   = Day + (Month << 5) + (Year  << 9);
  r  |= (U32)(Sec / 2 + (Min << 5) + (Hour  << 11)) << 16;
  return r;
}

/*************************** End of file ****************************/
