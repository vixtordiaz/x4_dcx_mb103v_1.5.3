/*********************************************************************
*                SEGGER MICROCONTROLLER GmbH & Co. KG                *
*        Solutions for real time microcontroller applications        *
**********************************************************************
*                                                                    *
*        (c) 2003-2012     SEGGER Microcontroller GmbH & Co KG       *
*                                                                    *
*        Internet: www.segger.com    Support:  support@segger.com    *
*                                                                    *
**********************************************************************

**** emFile file system for embedded applications ****
emFile is protected by international copyright laws. Knowledge of the
source code may not be used to write a similar product. This file may
only be used in accordance with a license and should not be re-
distributed in any way. We appreciate your understanding and fairness.
----------------------------------------------------------------------
File        : FS_NOR_HW_SPI_STR912.c
Purpose     : NOR SPI hardware driver for STR912
Literature  :
  [1] \\fileserver\techinfo\Company\ST\MCU\STR9STR91xF_ReferenceManual_Rev1.pdf
  [2] \\fileserver\techinfo\Company\Winbond\W25Q32BV.pdf
---------------------------END-OF-HEADER------------------------------
*/

#include "FS.h"
#include "NOR_HW_SPI_X.h"

/*********************************************************************
*
*     Defines, configurable
*
**********************************************************************
*/
#define USE_DMA         1       // Set this to 1 to activate the use of DMA for data transfers
#define PCLK            48000   // Speed of peripheral clock in kHz.
#define PRESCALER       2       // Factor to divide PCLK. Must be a even value between 2 and 254
#if USE_DMA
#define TX_CHANNEL      0       // Index of the DMA channel to use for sending data
#define RX_CHANNEL      1       // Index of the DMA channel to use for receiving data
#endif // USE_DMA

/*********************************************************************
*
*     Defines, not configurable
*
**********************************************************************
*/
#define APB0_BASE       0x58000000
#define APB1_BASE       0x5C000000

// Synchronous serial peripheral 0
#define SSP0_BASE       (APB1_BASE + 0x7000)
#define SSP0_CR0        (*(volatile U32 *)(SSP0_BASE + 0x00))   // Control register 0
#define SSP0_CR1        (*(volatile U32 *)(SSP0_BASE + 0x04))   // Control register 1
#define SSP0_DR         (*(volatile U32 *)(SSP0_BASE + 0x08))   // Data register
#define SSP0_SR         (*(volatile U32 *)(SSP0_BASE + 0x0C))   // Status register
#define SSP0_PR         (*(volatile U32 *)(SSP0_BASE + 0x10))   // Prescaler register
#define SSP0_DMACR      (*(volatile U32 *)(SSP0_BASE + 0x24))   // DMA control register

// System control unit
#define SCU_BASE        (APB1_BASE + 0x2000)
#define SCU_PCGR0       (*(volatile U32 *)(SCU_BASE + 0x14))    // Peripheral clock gating register 0
#define SCU_PCGR1       (*(volatile U32 *)(SCU_BASE + 0x18))    // Peripheral clock gating register 1
#define SCU_PRR0        (*(volatile U32 *)(SCU_BASE + 0x1C))    // Peripheral reset register 0
#define SCU_PRR1        (*(volatile U32 *)(SCU_BASE + 0x20))    // Peripheral reset register 1
#define SCU_GPIOOUT5    (*(volatile U32 *)(SCU_BASE + 0x58))    // Output configuration register
#define SCU_GPIOIN5     (*(volatile U32 *)(SCU_BASE + 0x78))    // Input configuration register

// General purpose I/O port 5
#define GPIO5_BASE      (APB0_BASE + 0xB000)
#define GPIO5_DATA      (*(volatile U8 *)(GPIO5_BASE + 0x000))  // Data register
#define GPIO5_DIR       (*(volatile U8 *)(GPIO5_BASE + 0x400))  // Direction register
#define GPIO5_SEL       (*(volatile U8 *)(GPIO5_BASE + 0x420))  // Selection register

// DMA controller
#define DMAC_BASE       (0x78000000)
#define DMAC_TCICR      (*(volatile U32 *)(DMAC_BASE + 0x08))   // Terminal count interrupt clear register
#define DMAC_EICR       (*(volatile U32 *)(DMAC_BASE + 0x10))   // Error interrupt clear register
#define DMAC_TCRISR     (*(volatile U32 *)(DMAC_BASE + 0x14))   // Terminal raw count interrupt status register
#define DMAC_ERISR      (*(volatile U32 *)(DMAC_BASE + 0x18))   // Error raw interrupt status register
#define DMAC_ENCSR      (*(volatile U32 *)(DMAC_BASE + 0x1C))   // Enable channel status
#define DMAC_CNFR       (*(volatile U32 *)(DMAC_BASE + 0x30))   // Global configuration register
#define DMAC_TX_CHANNEL_BASE  (DMAC_BASE + 0x100 + (TX_CHANNEL * 0x20))
#define DMAC_RX_CHANNEL_BASE  (DMAC_BASE + 0x100 + (RX_CHANNEL * 0x20))
#define DMAC_SRC_TX     (*(volatile U32 *)(DMAC_TX_CHANNEL_BASE + 0x00))   // Channel source address register
#define DMAC_DEST_TX    (*(volatile U32 *)(DMAC_TX_CHANNEL_BASE + 0x04))   // Channel destination address register
#define DMAC_LLI_TX     (*(volatile U32 *)(DMAC_TX_CHANNEL_BASE + 0x08))   // Channel linked list item
#define DMAC_CC_TX      (*(volatile U32 *)(DMAC_TX_CHANNEL_BASE + 0x0C))   // Channel control register
#define DMAC_CCNF_TX    (*(volatile U32 *)(DMAC_TX_CHANNEL_BASE + 0x10))   // Channel configuration register
#define DMAC_SRC_RX     (*(volatile U32 *)(DMAC_RX_CHANNEL_BASE + 0x00))   // Channel source address register
#define DMAC_DEST_RX    (*(volatile U32 *)(DMAC_RX_CHANNEL_BASE + 0x04))   // Channel destination address register
#define DMAC_LLI_RX     (*(volatile U32 *)(DMAC_RX_CHANNEL_BASE + 0x08))   // Channel linked list item
#define DMAC_CC_RX      (*(volatile U32 *)(DMAC_RX_CHANNEL_BASE + 0x0C))   // Channel control register
#define DMAC_CCNF_RX    (*(volatile U32 *)(DMAC_RX_CHANNEL_BASE + 0x10))   // Channel configuration register

// SSP status register
#define SR_RNE          (1uL << 2)  // Receiver FIFO not empty
#define SR_BYS          (1uL << 4)  // SSP busy

// SSP control register 0
#define CR0_DSS_8BIT    0x07uL      // Transfer 8-bit values
#define CR0_CPOL        (1uL << 6)  // Serial clock polarity
#define CR0_CPHA        (1uL << 7)  // Serial clock phase

// SSP control register 1
#define CR1_SSE         (1uL << 1)  // SPP enable/disable

// SSP DMA control register
#define DMACR_RXDMAE    (1uL << 0)  // Receive DMA enable
#define DMACR_TXDMAE    (1uL << 1)  // Transmit DMA enable

// Bit positions of the I/O lines
#define CLK_BIT         4
#define MOSI_BIT        5
#define MISO_BIT        6
#define CS_BIT          7

// Masks for the of I/O ports
#define GPIO5_CLK       (1uL << CLK_BIT)  // Clock
#define GPIO5_MOSI      (1uL << MOSI_BIT) // Master out, slave in
#define GPIO5_MISO      (1uL << MISO_BIT) // Master in, slave out
#define GPIO5_CS        (1uL << CS_BIT)   // Chip select

// Configuration of I/O ports
#define GPIOOUT_MASK    3uL
#define GPIOOUT_ALTOUT1 1uL
#define GPIOOUT_ALTOUT2 2uL

// Masks for the peripheral reset flags
#define PRR0_DMA        (1uL << 8) // Reset bit of DMA
#define PRR1_SSP0       (1uL << 8)  // Reset bit of SSP0
#define PRR1_GPIO5      (1uL << 19) // Reset bit of GPIO5

// Masks for the peripheral clocks
#define PCGR0_DMA       (1uL << 8)  // Clock enable/disable bit of DMA
#define PCGR1_SSP0      (1uL << 8)  // Clock enable/disable bit of SSP0
#define PCGR1_GPIO5     (1uL << 19) // Clock enable/disable bit of GPIO5

// Data register of CS signal
#define CS_DATA         (*(volatile U8 *)(GPIO5_BASE + ((1 << CS_BIT) << 2)))

// DMA channel control register
#define CC_TRFSIZE_MASK 0x0FFF        // Transfer size
#define CC_SI           (1uL << 26)   // Source address increment
#define CC_DI           (1uL << 27)   // Destination address increment

// DMA channel configuration register
#define CCNF_FC_M2P     (1ul << 11)   // Flow control: memory to peripheral
#define CCNF_FC_P2M     (2ul << 11)   // Flow control: peripheral to memory
#define CCNF_DP_SSP0    (13ul << 6)   // Destination is the SSP peripheral
#define CCNF_SP_SSP0    (12ul << 1)   // Source is the SSP peripheral
#define CCNF_E          (1ul << 0)    // Transfer enable


/*********************************************************************
*
*       Static data
*
**********************************************************************
*/
static volatile U8  _Dummy;

/*********************************************************************
*
*       Static code
*
**********************************************************************
*/

/*********************************************************************
*
*       _StartDMAWrite
*
*  Description:
*    Begins a DMA transfer which sends data to device.
*
*  Parameters:
*    pData
*    NumBytes
*
*/
#if USE_DMA
static void _StartDMAWrite(const U8 * pData, int NumBytes) {
  //
  // We use this DMA channel only to empty the receive FIFO of SSP
  //
  DMAC_SRC_RX  = (U32)&SSP0_DR;
  DMAC_DEST_RX = (U32)&_Dummy;
  DMAC_LLI_RX  = 0;
  DMAC_CC_RX   = CC_TRFSIZE_MASK & NumBytes;
  DMAC_CCNF_RX = CCNF_FC_P2M        // Move data from peripheral to memory
               | CCNF_SP_SSP0       // Source is the SSP peripheral
               ;
  //
  // This DMA channel moves the data from memory to SSP
  //
  DMAC_SRC_TX  = (U32)pData;
  DMAC_DEST_TX = (U32)&SSP0_DR;
  DMAC_LLI_TX  = 0;
  DMAC_CC_TX   = CC_TRFSIZE_MASK & NumBytes
               | CC_SI              // We read from consecutive memory locations
               ;
  DMAC_CCNF_TX = CCNF_FC_M2P        // Move data from memory to peripheral
               | CCNF_DP_SSP0       // Destination is the SSP peripheral
               ;
  //
  // Start the transfer
  //
  DMAC_CCNF_RX |= CCNF_E;
  DMAC_CCNF_TX |= CCNF_E;
}
#endif // USE_DMA

/*********************************************************************
*
*       _StartDMARead
*
*  Description:
*    Begins a DMA transfer which receives data from device.
*
*  Parameters:
*    pData
*    NumBytes
*
*/
#if USE_DMA
static void _StartDMARead(U8 * pData, int NumBytes) {
  //
  // We need to setup a DMA channel that sends dummy data
  // in order to be able to receive something from the device
  //
  _Dummy       = 0xFF;
  DMAC_SRC_TX  = (U32)&_Dummy;
  DMAC_DEST_TX = (U32)&SSP0_DR;
  DMAC_LLI_TX  = 0;
  DMAC_CC_TX   = CC_TRFSIZE_MASK & NumBytes;
  DMAC_CCNF_TX = CCNF_FC_M2P        // Move dummy data from memory to peripheral
               | CCNF_DP_SSP0       // Destination is the SSP peripheral
               ;
  //
  // This is the DMA channel that get the bytes receives from device
  // over SSP and saves them into memory
  //
  DMAC_SRC_RX  = (U32)&SSP0_DR;
  DMAC_DEST_RX = (U32)pData;
  DMAC_LLI_RX  = 0;
  DMAC_CC_RX   = CC_TRFSIZE_MASK & NumBytes
               | CC_DI              // We write into consecutive memory locations
               ;
  DMAC_CCNF_RX = CCNF_FC_P2M        // Move data from peripheral to memory
               | CCNF_SP_SSP0       // Source is the SSP peripheral
               ;
  //
  // Start the transfer
  //
  DMAC_CCNF_RX |= CCNF_E;
  DMAC_CCNF_TX |= CCNF_E;
}
#endif

/*********************************************************************
*
*       _WaitForEndOfTransfer
*
*  Description:
*    Waits for a DMA transfer to end.
*
*/
#if USE_DMA
static void _WaitForEndOfTransfer(void) {
  U32 Status;

  do {
    Status = DMAC_ERISR;
    if (Status & ((1 << RX_CHANNEL) | (1 << TX_CHANNEL))) {
      break;    // Exit in case of an error
    }
    Status = DMAC_ENCSR;
    if (!(Status & ((1 << RX_CHANNEL) | (1 << TX_CHANNEL)))) {
      break;    // Channels are disabled at the end of transfer
    }
  } while (1);
}
#endif

/*********************************************************************
*
*       Public code
*
**********************************************************************
*/

/*********************************************************************
*
*       FS_NOR_SPI_HW_X_Init
*
*  Description:
*    Initialize the SPI for use with the flash
*
*  Parameters:
*    Unit      - Device Index
*
*  Return value:
*    SPI frequency that is set - given in kHz.
*
*/
int FS_NOR_SPI_HW_X_Init(U8 Unit) {
  SCU_PCGR1 |= PCGR1_GPIO5 | PCGR1_SSP0;    // Enable clocks of GPIO and SSP
  SCU_PRR1  |= PCGR1_GPIO5 | PCGR1_SSP0;    // Get GPIO and SSP out of reset
#if USE_DMA
  SCU_PCGR0 |= PCGR0_DMA;                   // Enable clock of DMA
  SCU_PRR0  |= PCGR0_DMA;                   // Get DMA controller out of reset
  DMAC_CNFR  = 1;                           // Enable the DMA controller
#endif // USE_DMA
  GPIO5_SEL &= ~(GPIO5_CLK | GPIO5_MOSI | GPIO5_MISO | GPIO5_CS);
  GPIO5_DIR &= ~GPIO5_MISO;                 // This pin is input for master
  GPIO5_DIR |= GPIO5_CLK
            |  GPIO5_MOSI
            |  GPIO5_CS
            ;
  SCU_GPIOOUT5 |= (GPIOOUT_ALTOUT1 << (CS_BIT * 2))   // The upper layer drives the chip select pin
               |  (GPIOOUT_ALTOUT2 << (CLK_BIT * 2))
               |  (GPIOOUT_ALTOUT2 << (MOSI_BIT * 2))
               ;
  SCU_GPIOOUT5 &= ~(GPIOOUT_MASK << (MISO_BIT * 2));  // This pin is input for master
  SCU_GPIOIN5  |= 1 << MISO_BIT;                      // SSP is connected to this input
  SSP0_CR1      = 0;          // Disable peripheral and configure master mode
  SSP0_CR0      = CR0_DSS_8BIT | CR0_CPOL | CR0_CPHA;
  SSP0_PR       = PRESCALER;
#if USE_DMA
  SSP0_DMACR    = DMACR_TXDMAE | DMACR_RXDMAE;
#endif // USE_DMA
  SSP0_CR1     |= CR1_SSE;    // Enable SSP peripheral
  return PCLK / PRESCALER;
}

/*********************************************************************
*
*       FS_NOR_SPI_HW_X_EnableCS
*
*  Description:
*    Activates chip select signal (CS) of the flash chip.
*
*  Parameters:
*    Unit      - Device Index
*
*  Return value:
*    void
*/
void FS_NOR_SPI_HW_X_EnableCS(U8 Unit) {
  CS_DATA = 0;          // Active low
}

/*********************************************************************
*
*       FS_MMC_HW_X_DisableCS
*
*  Description:
*    Deactivates chip select signal (CS) of the flash chip.
*
*  Parameters:
*    Unit      - Device Index
*
*  Return value:
*    void
*/
void FS_NOR_SPI_HW_X_DisableCS(U8 Unit) {
  CS_DATA = GPIO5_CS;   // Active low
}

/*********************************************************************
*
*       FS_NOR_SPI_HW_X_Write
*
*  Description:
*    Reads a specified number of bytes from flash to buffer.
*
*  Parameters:
*    Unit      - Device Index
*    pData     - Pointer to a data buffer
*    NumBytes  - Number of bytes
*
*  Return value:
*    void
*/
void FS_NOR_SPI_HW_X_Read(U8 Unit, U8 * pData, int NumBytes) {
#if USE_DMA
  _StartDMARead(pData, NumBytes);
  _WaitForEndOfTransfer();
#else
  SSP0_DR = 0xFF;
  if (--NumBytes) {
    do {
      SSP0_DR = 0xFF;
      while ((SSP0_SR & SR_RNE) == 0) { // Wait until we received a byte
        ;
      }
      *pData++ = SSP0_DR;
    } while (--NumBytes);
  }
  while ((SSP0_SR & SR_RNE) == 0) {     // Wait until we received a byte
    ;
  }
  *pData = SSP0_DR;
#endif
}

/*********************************************************************
*
*       FS_NOR_SPI_HW_X_Write
*
*  Description:
*    Writes a specified number of bytes from data buffer to flash.
*
*  Parameters:
*    Unit      - Device Index
*    pData     - Pointer to a data buffer
*    NumBytes  - Number of bytes
*
*  Return value:
*    void
*/
void FS_NOR_SPI_HW_X_Write(U8 Unit, const U8 * pData, int NumBytes) {
#if USE_DMA
  _StartDMAWrite(pData, NumBytes);
  _WaitForEndOfTransfer();
#else
  while ((SSP0_SR & SR_BYS) != 0) {     // Busy ?
    ;
  }
  do {
    SSP0_DR = *pData++;
    while ((SSP0_SR & SR_RNE) == 0) {   // Wait until we received a byte
      ;
    }
    _Dummy = SSP0_DR;
  } while (--NumBytes);
#endif // USE_DMA
}

/*************************** End of file ****************************/
