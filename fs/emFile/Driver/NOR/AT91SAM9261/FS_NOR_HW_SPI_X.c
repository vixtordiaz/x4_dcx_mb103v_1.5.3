/*********************************************************************
*                SEGGER MICROCONTROLLER GmbH & Co. KG                *
*        Solutions for real time microcontroller applications        *
**********************************************************************
*                                                                    *
*        (c) 2003-2012     SEGGER Microcontroller GmbH & Co KG       *
*                                                                    *
*        Internet: www.segger.com    Support:  support@segger.com    *
*                                                                    *
**********************************************************************

**** emFile file system for embedded applications ****
emFile is protected by international copyright laws. Knowledge of the
source code may not be used to write a similar product. This file may
only be used in accordance with a license and should not be re-
distributed in any way. We appreciate your understanding and fairness.
----------------------------------------------------------------------
----------------------------------------------------------------------
File        : MMC_HW_SPI.c
Purpose     : NOR SPI hardware layer for Atmel AT91SAM9261.
---------------------------END-OF-HEADER------------------------------
*/

/*********************************************************************
*
*             #include Section
*
**********************************************************************
*/

#include "FS.h"
#include "NOR_HW_SPI_X.h"

/*********************************************************************
*
*       Defines, sfr
*
**********************************************************************
*/
#define PIOA_BASE 0xfffff400
#define PIOA_PDR  *(volatile U32*) (PIOA_BASE + 0x04) /* PIOA disable register               */
#define PIOA_ASR  *(volatile U32*) (PIOA_BASE + 0x70) /* PIOA "A" peripheral select register */
#define PIOA_BSR  *(volatile U32*) (PIOA_BASE + 0x74) /* PIOA "B" peripheral select register */
#define PIOA_PER  *(volatile U32*) (PIOA_BASE + 0x00) // Enable register, enables PIO function
#define PIOA_OER  *(volatile U32*) (PIOA_BASE + 0x10) // Output enable register, sets to output mode
#define PIOA_ODR  *(volatile U32*) (PIOA_BASE + 0x14) // Output enable register, sets to output mode
#define PIOA_SODR *(volatile U32*) (PIOA_BASE + 0x30) // Set output data
#define PIOA_CODR *(volatile U32*) (PIOA_BASE + 0x34) // Clear output data register
#define PIOA_ODSR *(volatile U32*) (PIOA_BASE + 0x38) // output data status register
#define PIOA_PDSR *(volatile U32*) (PIOA_BASE + 0x3c) // pin data status register
#define PIOA_OWER *(volatile U32*) (PIOA_BASE + 0xA0) // Output write enable register
#define PIOA_OWDR *(volatile U32*) (PIOA_BASE + 0xA4) // Output write disable register

#define PIOA_ID          2

/*      SPI0 */
#define SPI_BASE  0xFFFC8000
#define SPI_CR    *(volatile U32*) (SPI_BASE + 0x00)
#define SPI_MR    *(volatile U32*) (SPI_BASE + 0x04)
#define SPI_RDR   *(volatile U32*) (SPI_BASE + 0x08)
#define SPI_TDR   *(volatile U32*) (SPI_BASE + 0x0C)
#define SPI_SR    *(volatile U32*) (SPI_BASE + 0x10)
#define SPI_IER   *(volatile U32*) (SPI_BASE + 0x14)
#define SPI_IDR   *(volatile U32*) (SPI_BASE + 0x18)
#define SPI_IMR   *(volatile U32*) (SPI_BASE + 0x1c)
#define SPI_CSR0  *(volatile U32*) (SPI_BASE + 0x30)
#define SPI_CSR1  *(volatile U32*) (SPI_BASE + 0x34)
#define SPI_CSR2  *(volatile U32*) (SPI_BASE + 0x38)
#define SPI_CSR3  *(volatile U32*) (SPI_BASE + 0x3c)

/*      SPI PDC */
#define SPI_PDC_RPR  *(volatile U32*) (SPI_BASE + 0x100)
#define SPI_PDC_RCR  *(volatile U32*) (SPI_BASE + 0x104)
#define SPI_PDC_TPR  *(volatile U32*) (SPI_BASE + 0x108)
#define SPI_PDC_TCR  *(volatile U32*) (SPI_BASE + 0x10c)
#define SPI_PDC_RNPR *(volatile U32*) (SPI_BASE + 0x110)
#define SPI_PDC_RNCR *(volatile U32*) (SPI_BASE + 0x114)
#define SPI_PDC_TNPR *(volatile U32*) (SPI_BASE + 0x118)
#define SPI_PDC_TNCR *(volatile U32*) (SPI_BASE + 0x11c)
#define SPI_PDC_PTCR *(volatile U32*) (SPI_BASE + 0x120)
#define SPI_PDC_PTSR *(volatile U32*) (SPI_BASE + 0x124)

#define SPI_ID          12

/*      Power management controller */
#define PMC_BASE  0xFFFFFC00
#define PMC_SCER  *(volatile U32*) (PMC_BASE + 0x00) // System Clock Enable Register
#define PMC_SCDR  *(volatile U32*) (PMC_BASE + 0x04) // System Clock Disable Register
#define PMC_SCSR  *(volatile U32*) (PMC_BASE + 0x08) // System Clock Status Register

#define PMC_PCER  *(volatile U32*) (PMC_BASE + 0x10)  /* Peripheral clock enable register */

/*********************************************************************
*
*       #define Macros
*
**********************************************************************
*/
//#define SD_WP_PIN        16
//#define SD_CD_PIN        20
#define SD_CS_PIN         6
#define SD_MISO_PIN       0
#define SD_MOSI_PIN       1
#define SD_CLK_PIN        2
#define SPI_CSR           SPI_CSR0


#define MMC_DEFAULTSUPPLYVOLTAGE  3300 /* in mV, example means 3.3V */
#define MCLK                     (100045440uL)  // may depend on PLL
#define MCLK_SPICLOCK             (MCLK)         // defaults is SD clock = MCLK

/*********************************************************************
*
*       Static data
*
**********************************************************************
*/
static U8           _sbcr = MCLK_SPICLOCK / 4000000UL;

/*********************************************************************
*
*       Public code
*
**********************************************************************
*/



/*********************************************************************
*
*       _SetMaxSpeed
*
*/
static U16 _SetMaxSpeed(U8 Unit, U16 MaxFreq) {
  U32 InFreq;
  U32 SPIFreq;

  if (MaxFreq < 400) {
    MaxFreq = 400;
  }
  SPIFreq = 1000 * MaxFreq;
  if (SPIFreq >= 200000) {
    InFreq = MCLK_SPICLOCK;
  }
  _sbcr = (InFreq + SPIFreq - 1) / SPIFreq;
  return MaxFreq;    /* We are not faster than this */
}

/*********************************************************************
*
*       FS_NOR_SPI_HW_X_Init
*
*  Description:
*    Initialize the SPI for use with the flash
*
*  Parameters:
*    Unit      - Device Index
*  
*  Return value:
*    SPI frequency that is set - given in kHz.
*
*/
int FS_NOR_SPI_HW_X_Init(U8 Unit) {
  int MaxSpeed;

  MaxSpeed = _SetMaxSpeed(Unit, 50000);
  //
  // Enable Power for PIOA and SPI block
  //
  PMC_PCER = (1 << SPI_ID)
           | (1 << PIOA_ID);
  //
  // Setup Pins
  //
  PIOA_PER =  0
            | (1 << SD_CS_PIN)
            | (1 << SD_MISO_PIN)
            | (1 << SD_MOSI_PIN)
            | (1 << SD_CLK_PIN)

            ;
  PIOA_SODR = 0
            | (1 << SD_CS_PIN)
            | (1 << SD_MOSI_PIN)
            | (1 << SD_CLK_PIN)
            ;
  PIOA_OER  = 0
            | (1 << SD_CS_PIN)
            | (1 << SD_MOSI_PIN)
            | (1 << SD_CLK_PIN)
            ;
  PIOA_ODR  = 0
            | (1 << SD_MOSI_PIN)
            ;
  PIOA_PDR =  0
            | (1 << SD_MISO_PIN)       // SPI-MISO
            | (1 << SD_MOSI_PIN)       // SPI-MOSI
            | (1 << SD_CLK_PIN)        // SPI-Clock
            ;
  PIOA_ASR  = 0
            |(1 << SD_MISO_PIN)       // SPI-MISO
            |(1 << SD_MOSI_PIN)       // SPI-MOSI
            |(1 << SD_CLK_PIN)       // SPI-Clock
            ;
  //
  // SPI
  //
  SPI_CR    = (1 << 7);      // Software reset
  SPI_MR    = 0
            |(1 << 0)         // 1 : Master mode
            |(0 << 1)         // 0 : Fixed chip select
            |(0 << 2)         // Chip select
            |(0 << 3)         // 0: Use MCLK as clock
            |(1 << 4)         // 1: Fault detection disable
            |(0 << 7)         // 1: Loopback
            |(0 << 16)        // 0000b: Use CS0
            ;
  SPI_CSR   = 0
            |(1 << 0)         // 1 : Clock polarity of idle is high
            |(0 << 1)         // Clock hase sel
            |(1 << 3)         // Leave CS0 stay low
            |(0 << 4)         // 0000b: 8 bits per transfer
            |(_sbcr<< 8)      // 8..15: SCBR: Baud rate divider
            |(0x100000);
              ;
  SPI_CR    = (1 << 0);       // Enable SPI
  return MaxSpeed;
}

/*********************************************************************
*
*       FS_NOR_SPI_HW_X_EnableCS
*
*  Description:
*    Sets the card slot active using the chip select (CS) line.
*
*  Parameters:
*    Unit      - Device Index
*
*  Return value:
*    void
*/
void FS_NOR_SPI_HW_X_EnableCS(U8 Unit) {
  PIOA_CODR  = (1 <<  SD_CS_PIN);       // CS0 on eval board
}

/*********************************************************************
*
*       FS_MMC_HW_X_DisableCS
*
*  Description:
*    Clears the card slot inactive using the chip select (CS) line.
*
*  Parameters:
*    Unit      - Device Index
*
*  Return value:
*    void
*/
void FS_NOR_SPI_HW_X_DisableCS(U8 Unit) {
  PIOA_SODR  = (1 <<  SD_CS_PIN);       // CS0 on eval board
}

/*********************************************************************
*
*       FS_NOR_SPI_HW_X_Write
*
*  Description:
*    Reads a specified number of bytes from flash to buffer.
*
*  Parameters:
*    Unit      - Device Index
*    pData     - Pointer to a data buffer
*    NumBytes  - Number of bytes
*
*  Return value:
*    void
*/
void FS_NOR_SPI_HW_X_Read(U8 Unit, U8 * pData, int NumBytes) {
  do {
    SPI_TDR = 0xff;
    while ((SPI_SR & (1 << 9)) == 0);
    while ((SPI_SR & (1 << 0)) == 0);
    *pData++ = SPI_RDR;
  } while (--NumBytes);
}

/*********************************************************************
*
*       FS_NOR_SPI_HW_X_Write
*
*  Description:
*    Writes a specified number of bytes from data buffer to flash.
*
*  Parameters:
*    Unit      - Device Index
*    pData     - Pointer to a data buffer
*    NumBytes  - Number of bytes
*
*  Return value:
*    void
*/
void FS_NOR_SPI_HW_X_Write(U8 Unit, const U8 * pData, int NumBytes) {
  do {
    SPI_TDR = *pData++;
    while ((SPI_SR & (1 << 9)) == 0);
  } while (--NumBytes);
}

/*************************** End of file ****************************/
