/*********************************************************************
*                SEGGER MICROCONTROLLER GmbH & Co. KG                *
*        Solutions for real time microcontroller applications        *
**********************************************************************
*                                                                    *
*        (c) 2003-2012     SEGGER Microcontroller GmbH & Co KG       *
*                                                                    *
*        Internet: www.segger.com    Support:  support@segger.com    *
*                                                                    *
**********************************************************************

**** emFile file system for embedded applications ****
emFile is protected by international copyright laws. Knowledge of the
source code may not be used to write a similar product. This file may
only be used in accordance with a license and should not be re-
distributed in any way. We appreciate your understanding and fairness.
----------------------------------------------------------------------
----------------------------------------------------------------------
File        : NOR_PHY_Template.c
Purpose     : Low level flash driver template
----------------------------------------------------------------------
------------  END-OF-HEADER  -----------------------------------------
*/

#include "FS_Int.h"
#include "NOR_Private.h"

/*********************************************************************
*
*      Defines
*
**********************************************************************
*/

/*********************************************************************
*
*       Configurable defines
*
**********************************************************************
*/

#define NUM_SECTORS      64

/*********************************************************************
*
*       Fixed defines
*
**********************************************************************
*/


/*********************************************************************
*
*      Types
*
**********************************************************************
*/


/*********************************************************************
*
*      Static data
*
**********************************************************************
*/

/*********************************************************************
*
*      Static code
*
**********************************************************************
*/



/*********************************************************************
*
*      Exported code thru call backs
*
**********************************************************************
*/

/*********************************************************************
*
*      _WriteOff
*
* Function description
*   This routine writes data into any section of the flash. It does not
*   check if this section has been previously erased; this is in the
*   responsibility of the user program.
*   Data written into multiple sectors at a time can be handled by this
*   routine.
*
* Return value:
*   0       O.K., data has been written
*   other   error
*/
static int _WriteOff(U8 Unit, U32 Off, const void * pSrc, U32 NumBytes) {
  return 0;
}

/*********************************************************************
*
*       _ReadOff
*
* Function description
*   Reads data from the given offset of the flash.
*
* Return value:
*   0       O.K., data has been read from flash
*   other   error
*/
static int _ReadOff(U8 Unit, void * pDest, U32 Off, U32 NumBytes) {
  //
  //  Implement here the read of the flash
  //
  return 0;
}

/*********************************************************************
*
*      _EraseSector
*
* Function description
*   Erases one sector.
*
*
* Return value:
*   0       O.K., sector is ereased
*   other   error, sector may not be erased
*
*/
static int _EraseSector(U8 Unit, unsigned int SectorIndex) {
  //
  //  Implement here the erase of a sector
  //

  return 0;
}

/*********************************************************************
*
*       _GetSectorInfo
*
* Function description
*   Returns the offset and length of the given sector
*/
static void _GetSectorInfo(U8 Unit, unsigned int SectorIndex, U32 * pOff, U32 * pLen) {
  if (pOff) {
    *pOff = 0;
  }
  if (pLen) {
    *pLen = 0x10000;
  }

}

/*********************************************************************
*
*       _GetNumSectors
*
* Function description
*   Returns the number of flash sectors
*/
static int _GetNumSectors(U8 Unit) {
  return NUM_SECTORS;
}

/*********************************************************************
*
*       _Configure
*
*  Function description
*    Configures a single instance of the driver
*/
static void _Configure(U8 Unit, U32 BaseAddr, U32 StartAddr, U32 NumBytes) {
}

/*********************************************************************
*
*       _OnSelectPhy
*
*  Function description
*    Called right after selection of the physical layer
*    This may be neccessary to retrieve the information from flash.
*/
static void _OnSelectPhy(U8 Unit) {
}


/*********************************************************************
*
*      Public code
*
**********************************************************************
*/
/*********************************************************************
*
*       Global data
*
**********************************************************************
*/
// NOR flash physical driver structure
const FS_NOR_PHY_TYPE FS_NOR_PHY_Template = {
  _WriteOff,
  _ReadOff,
  _EraseSector,
  _GetSectorInfo,
  _GetNumSectors,
  _Configure,
  _OnSelectPhy
};            


/*************************** End of file ****************************/
