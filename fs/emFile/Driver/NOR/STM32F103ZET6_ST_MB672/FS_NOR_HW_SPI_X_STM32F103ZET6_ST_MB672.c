/*********************************************************************
*                SEGGER MICROCONTROLLER GmbH & Co. KG                *
*        Solutions for real time microcontroller applications        *
**********************************************************************
*                                                                    *
*        (c) 2003-2011     SEGGER Microcontroller GmbH & Co. KG      *
*                                                                    *
*        Internet: www.segger.com    Support:  support@segger.com    *
*                                                                    *
**********************************************************************

**** emFile file system for embedded applications ****
emFile is protected by international copyright laws. Knowledge of the
source code may not be used to write a similar product. This file may
only be used in accordance with a license and should not be re-
distributed in any way. We appreciate your understanding and fairness.
----------------------------------------------------------------------
----------------------------------------------------------------------
File        : NOR_HW_SPI_X.c
Purpose     : NOR SPI hardware layer for STM32F103ZET6 and ST MB672
              eval board.
---------------------------END-OF-HEADER------------------------------
*/

/*********************************************************************
*
*             #include Section
*
**********************************************************************
*/
#include "FS.h"
#include "NOR_HW_SPI_X.h"

/*********************************************************************
*
*             #defines, SFRs needed for configuration
*
**********************************************************************
*/
#define SPI1_BASE_ADDR        (0x40013000)
#define SPI2_BASE_ADDR        (0x40003800)

/*********************************************************************
*
*             #defines, configuration
*
**********************************************************************
*/
#define SPI_BASE_ADDR  SPI1_BASE_ADDR

/*********************************************************************
*
*             #defines, macros, SFRs
*
**********************************************************************
*/
#define MMC_DEFAULTSUPPLYVOLTAGE  3300   // in mV, example means 3.3V
#define PCLK                      36000  // Peripheral clock given in kHz

/***  sfrs ******/
#define RCC_BASE           0x40021000L
#define RCC_CFGR           *(volatile unsigned long*)(RCC_BASE + 0x04)
#define RCC_APB2RSTR       *(volatile unsigned long*)(RCC_BASE + 0x0C)
#define RCC_APB1RSTR       *(volatile unsigned long*)(RCC_BASE + 0x10)
#define RCC_AHBENR         *(volatile unsigned long*)(RCC_BASE + 0x14)
#define RCC_APB2ENR        *(volatile unsigned long*)(RCC_BASE + 0x18)
#define RCC_APB1ENR        *(volatile unsigned long*)(RCC_BASE + 0x1C)

#define GPIO_PA_BASE_ADDR  0x40010800
#define GPIO_PB_BASE_ADDR  0x40010C00

#define GPIOA_CRL          *(volatile unsigned long*)(GPIO_PA_BASE_ADDR + 0x00)
#define GPIOA_ODR          *(volatile unsigned long*)(GPIO_PA_BASE_ADDR + 0x0C)

#define GPIOB_CRL          *(volatile unsigned long*)(GPIO_PB_BASE_ADDR + 0x00)
#define GPIOB_ODR          *(volatile unsigned long*)(GPIO_PB_BASE_ADDR + 0x0C)

#define SPI_CR1            *(volatile unsigned long*)(SPI_BASE_ADDR + 0x00)
#define SPI_CR2            *(volatile unsigned long*)(SPI_BASE_ADDR + 0x04)
#define SPI_SR             *(volatile unsigned long*)(SPI_BASE_ADDR + 0x08)
#define SPI_DR             *(volatile unsigned long*)(SPI_BASE_ADDR + 0x0C)
#define SPI_CRCPR          *(volatile unsigned long*)(SPI_BASE_ADDR + 0x10)
#define SPI_RXCRCR         *(volatile unsigned long*)(SPI_BASE_ADDR + 0x14)
#define SPI_TXCRCR         *(volatile unsigned long*)(SPI_BASE_ADDR + 0x18)

/*********************************************************************
*
*       Static data
*
**********************************************************************
*/
static char _IsInited;
/*********************************************************************
*
*       Static code
*
**********************************************************************
*/


/*********************************************************************
*
*       _SetMaxSpeed
*/
static U16 _SetMaxSpeed(U8 Unit, U16 MaxFreq) {
  U32  Div;
  unsigned DivVal;
  
  Div    = 2;
  DivVal = 0;
  while ((MaxFreq * Div) < PCLK) {
    Div <<= 1;
    if (++DivVal == 7) {
      break;
    }
  }
  //
  // Disable the SPI intially before setting the new divider
  //
  // 
  SPI_CR1 &= ~(1 << 6);
  //
  // Set the divider
  //
  SPI_CR1 &= ~(7 << 3);
  SPI_CR1 |= (DivVal << 3);
  //
  // Enable SPI
  //
  SPI_CR1 |= (1 << 6);
  return PCLK / Div;    /* We are not faster than this */
}

/*********************************************************************
*
*       _ReadWriteSPI
*
*/
static U8 _ReadWriteSPI(U8 Data) {
  //Send Data
  SPI_DR = Data;
  //wait until all bits are shifted
  while (0 == (SPI_SR & (1 << 0)));
  //Read data
  return (U8)SPI_DR;
}


/*********************************************************************
*
*       Public code
*
**********************************************************************
*/


/*********************************************************************
*
*       FS_NOR_SPI_HW_X_Init
*
*  Description:
*    Initialize the SPI for use with the flash
*
*  Parameters:
*    Unit      - Device Index
*  
*  Return value:
*    SPI frequency that is set - given in kHz.
*
*/
int FS_NOR_SPI_HW_X_Init(U8 Unit) {
  U32 v;

  if (_IsInited == 0) {
    _IsInited = 1;
    RCC_APB2RSTR &= ~((1 << 2) |  // Remove RESET from PortA
                      (1 << 3));  // Remove RESET from PortB
    RCC_APB2ENR  |=  ((1 << 2) |  // Enable clock for PortA
                      (1 << 3));  // Enable clock for PortB
    RCC_APB2RSTR &=  ~(1 << 12);  // Remove RESET from SPI1
    RCC_APB2ENR  |=   (1 << 12);  // Enable clock for SPI1
    //
    // Setup port pins
    //
    v  = GPIOA_CRL;
    v &= ~0xFFF00000;
    v |= (3 << 20)    // PortA05 -> GPIO output, max 50MHz
      |  (2 << 22)    // PortA05 -> Output mode, using PP (used for SPI_CLK)
      |  (3 << 24)    // PortA06 -> GPIO input
      |  (3 << 26)    // PortA06 -> Input mode, floating (used for SPI_MISO)
      |  (3 << 28)    // PortA07 -> GPIO output, max 50MHz
      |  (2 << 30)    // PortA07 -> Output mode, using PP (used for SPI_MOSI)
      ;
    GPIOA_CRL = v;
    v  = GPIOB_CRL;
    v &= ~0x00000F00;
    v |= (3 <<  8)    // PortB02 -> GPIO output, max 50MHz
      |  (0 << 10)    // PortB02 -> Output mode, using PP (used for CS)
      ;
    GPIOB_CRL = v;
    //
    // Initialize SPI
    //
    SPI_CR1  = (0 << 0)         // Second clock transition is the first data capture edge
             | (0 << 1)         // Clock polarity is high when idle
             | (1 << 2)         // SPI is master
             | (7 << 3)         // Clock = pclk (72MHz) / 256
             | (1 << 8)  
             | (1 << 9)
             ;
    SPI_CR2  = 0;               // Neither interrupts nor DMA is used
    SPI_CR1 |= (1 << 6)         // Enable SPI
             ;
  }
  return _SetMaxSpeed(Unit, 25000);
}

/*********************************************************************
*
*       FS_NOR_SPI_HW_X_EnableCS
*
*  Description:
*    Sets the card slot active using the chip select (CS) line.
*
*  Parameters:
*    Unit      - Device Index
*
*  Return value:
*    void
*/
void FS_NOR_SPI_HW_X_EnableCS(U8 Unit) {
  GPIOB_ODR &= ~(1 << 2);
}

/*********************************************************************
*
*       FS_MMC_HW_X_DisableCS
*
*  Description:
*    Clears the card slot inactive using the chip select (CS) line.
*
*  Parameters:
*    Unit      - Device Index
*
*  Return value:
*    void
*/
void FS_NOR_SPI_HW_X_DisableCS(U8 Unit) {
  GPIOB_ODR |= (1 << 2);
}

/*********************************************************************
*
*       FS_NOR_SPI_HW_X_Write
*
*  Description:
*    Reads a specified number of bytes from flash to buffer.
*
*  Parameters:
*    Unit      - Device Index
*    pData     - Pointer to a data buffer
*    NumBytes  - Number of bytes
*
*  Return value:
*    void
*/
void FS_NOR_SPI_HW_X_Read(U8 Unit, U8 * pData, int NumBytes) {
  do {
    *pData++ = _ReadWriteSPI(0xff);
  } while (--NumBytes);

}

/*********************************************************************
*
*       FS_NOR_SPI_HW_X_Write
*
*  Description:
*    Writes a specified number of bytes from data buffer to flash.
*
*  Parameters:
*    Unit      - Device Index
*    pData     - Pointer to a data buffer
*    NumBytes  - Number of bytes
*
*  Return value:
*    void
*/
void FS_NOR_SPI_HW_X_Write(U8 Unit, const U8 * pData, int NumBytes) {
  do {
    _ReadWriteSPI(*pData++);
  } while (--NumBytes);
}

/*************************** End of file ****************************/
