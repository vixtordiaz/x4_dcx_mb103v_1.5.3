/*********************************************************************
*                SEGGER MICROCONTROLLER GmbH & Co. KG                *
*        Solutions for real time microcontroller applications        *
**********************************************************************
*                                                                    *
*        (c) 2003-2012     SEGGER Microcontroller GmbH & Co KG       *
*                                                                    *
*        Internet: www.segger.com    Support:  support@segger.com    *
*                                                                    *
**********************************************************************

**** emFile file system for embedded applications ****
emFile is protected by international copyright laws. Knowledge of the
source code may not be used to write a similar product. This file may
only be used in accordance with a license and should not be re-
distributed in any way. We appreciate your understanding and fairness.
----------------------------------------------------------------------
File        : NOR_SPI_Template.c
Purpose     : NOR SPI hardware layer template
---------------------------END-OF-HEADER------------------------------
*/

/*********************************************************************
*
*             #include Section
*
**********************************************************************
*/
#include "FS.h"
#include "NOR_HW_SPI_X.h"

/*********************************************************************
*
*             #define Macros
*
**********************************************************************
*/

/*********************************************************************
*
*       Static data
*
**********************************************************************
*/
static char _IsInited;

/*********************************************************************
*
*       Static code
*
**********************************************************************
*/

/*********************************************************************
*
*       Public code
*
**********************************************************************
*/


/*********************************************************************
*
*       FS_NOR_SPI_HW_X_Init
*
*  Description:
*    Initialize the SPI for use with the flash
*
*  Parameters:
*    Unit      - Device Index
*  
*  Return value:
*    SPI frequency that is set - given in kHz.
*
*/
int FS_NOR_SPI_HW_X_Init(U8 Unit) {
  U32 SPIFreq;
  if (_IsInited == 0) {
    _IsInited = 1;
    //
    // Insert initialization here
    //
  }
  return SPIFreq;
}

/*********************************************************************
*
*       FS_NOR_SPI_HW_X_EnableCS
*
*  Description:
*    Activates chip select signal (CS) of the flash chip.
*
*  Parameters:
*    Unit      - Device Index
*
*  Return value:
*    void
*/
void FS_NOR_SPI_HW_X_EnableCS(U8 Unit) {
}

/*********************************************************************
*
*       FS_MMC_HW_X_DisableCS
*
*  Description:
*    Deactivates chip select signal (CS) of the flash chip.
*
*  Parameters:
*    Unit      - Device Index
*
*  Return value:
*    void
*/
void FS_NOR_SPI_HW_X_DisableCS(U8 Unit) {
}

/*********************************************************************
*
*       FS_NOR_SPI_HW_X_Write
*
*  Description:
*    Reads a specified number of bytes from flash to buffer.
*
*  Parameters:
*    Unit      - Device Index
*    pData     - Pointer to a data buffer
*    NumBytes  - Number of bytes
*
*  Return value:
*    void
*/
void FS_NOR_SPI_HW_X_Read(U8 Unit, U8 * pData, int NumBytes) {
}

/*********************************************************************
*
*       FS_NOR_SPI_HW_X_Write
*
*  Description:
*    Writes a specified number of bytes from data buffer to flash.
*
*  Parameters:
*    Unit      - Device Index
*    pData     - Pointer to a data buffer
*    NumBytes  - Number of bytes
*
*  Return value:
*    void
*/
void FS_NOR_SPI_HW_X_Write(U8 Unit, const U8 * pData, int NumBytes) {
}

/*************************** End of file ****************************/
