/*********************************************************************
*                SEGGER MICROCONTROLLER GmbH & Co. KG                *
*        Solutions for real time microcontroller applications        *
**********************************************************************
*                                                                    *
*        (c) 2003-2012     SEGGER Microcontroller GmbH & Co KG       *
*                                                                    *
*        Internet: www.segger.com    Support:  support@segger.com    *
*                                                                    *
**********************************************************************

**** emFile file system for embedded applications ****
emFile is protected by international copyright laws. Knowledge of the
source code may not be used to write a similar product. This file may
only be used in accordance with a license and should not be re-
distributed in any way. We appreciate your understanding and fairness.
----------------------------------------------------------------------
File        : NOR_PHY_ST_STM32F207.c
Purpose     : Physical layer for the internal flash of ST STM32F207 MCU.
Literature  : [1] \\fileserver\Techinfo\Company\ST\MCU\STM32\STM32_20xxx\STM32F2xx_RM_rev4_1112.pdf
              [2] \\fileserver\Techinfo\Company\ST\MCU\STM32\STM32_20xxx\STM32F2xx_FlashProgramming_Rev4_1105.pdf
Note:
      Make sure that the FS_NOR_LINE_SIZE defined is set according to the supply of your target HW
      as described in the following table taken from [2]:

      Supply voltage   FS_NOR_LINE_SIZE
      (V)              (bytes)
      ---------------------------------
      2.7 - 3.6        4
      2.4 - 2.7        2
      2.1 - 2.4        2
      1.8 - 2.1        1

------------  END-OF-HEADER  -----------------------------------------
*/

/*********************************************************************
*
*       #include section
*
**********************************************************************
*/
#include "FS_Int.h"
#include "NOR_Private.h"

/*********************************************************************
*
*       Defines, configurable
*
**********************************************************************
*/
#define USE_OS    0   // 0: polling mode, 1: event driven

/*********************************************************************
*
*       #include section, conditional
*
**********************************************************************
*/
#if USE_OS
  #include "RTOS.h"
  #include "stm32f2xx.h"
#endif

/*********************************************************************
*
*       Defines, non-configurable
*
**********************************************************************
*/

/*********************************************************************
*
*       Flash interface registers
*/
#define FLASH_BASE_ADDR   0x40023C00uL
#define FLASH_ACR         (*(volatile U32 *)(FLASH_BASE_ADDR + 0x00))
#define FLASH_KEYR        (*(volatile U32 *)(FLASH_BASE_ADDR + 0x04))
#define FLASH_OPTKEYR     (*(volatile U32 *)(FLASH_BASE_ADDR + 0x08))
#define FLASH_SR          (*(volatile U32 *)(FLASH_BASE_ADDR + 0x0C))
#define FLASH_CR          (*(volatile U32 *)(FLASH_BASE_ADDR + 0x10))
#define FLASH_OPTCR       (*(volatile U32 *)(FLASH_BASE_ADDR + 0x14))

/*********************************************************************
*
*       Flash keys
*/
#define FLASH_KEY1        0x45670123
#define FLASH_KEY2        0xCDEF89AB

/*********************************************************************
*
*       Status bits
*/
#define SR_EOP        0
#define SR_OPERR      1
#define SR_WRPERR     4
#define SR_PGAERR     5
#define SR_PGPERR     6
#define SR_PGSERR     7
#define SR_BUSY       16

/*********************************************************************
*
*       Control bits
*/
#define CR_PG         0     // Start programming
#define CR_SER        1
#define CR_SNB        3
#define CR_SNB_MAX    0xF
#define CR_PSIZE      8
#define CR_PSIZE_8    0uL
#define CR_PSIZE_16   1uL
#define CR_PSIZE_32   2uL
#define CR_STRT       16
#define CR_EOPIE      24
#define CR_ERRIE      25
#define CR_LOCK       31    // Register access protection

/*********************************************************************
*
*       Flash acceleration bits
*/
#define ACR_DCEN      10
#define ACR_DCRST     12

/*********************************************************************
*
*       Misc. defines
*/
#define MAX_SECTOR_BLOCKS   3   // Number of different sector sizes. The internal flash of STM32F2 is divided into 4 * 16KB, 1 * 64KB, 7 * 128KB sectors.
#define WAIT_TIMEOUT_MAX    0x7FFFFFFFL
#define FLASH_PRIO          15

/*********************************************************************
*
*       ASSERT_SECTOR_INDEX_IS_IN_RANGE
*/
#if FS_DEBUG_LEVEL >= FS_DEBUG_LEVEL_CHECK_ALL
  #define ASSERT_SECTOR_INDEX_IS_IN_RANGE(SectorIndex)                          \
    if (SectorIndex >= _pInst->NumSectorsUsed) {                                \
      FS_DEBUG_ERROROUT((FS_MTYPE_DRIVER, "NOR_K90: Invalid sector index.\n")); \
      FS_X_Panic(FS_ERROR_INVALID_PARA);                                        \
    }
#else
  #define ASSERT_SECTOR_INDEX_IS_IN_RANGE(SectorIndex)
#endif

/*********************************************************************
*
*       ASSERT_DATA_IS_ALIGNED...
*/
#if FS_DEBUG_LEVEL >= FS_DEBUG_LEVEL_CHECK_ALL
  #if FS_NOR_LINE_SIZE == 1
    #define ASSERT_DATA_IS_ALIGNED(Data)
  #endif
  #if FS_NOR_LINE_SIZE == 2
    #define ASSERT_DATA_IS_ALIGNED(Data)                                        \
      if ((Off | NumBytes) & 1) {                                               \
        FS_DEBUG_ERROROUT((FS_MTYPE_DRIVER, "NOR_K90: Data not aligned.\n"));   \
        FS_X_Panic(FS_ERROR_INVALID_PARA);                                      \
      }
  #endif
  #if FS_NOR_LINE_SIZE == 4
    #define ASSERT_DATA_IS_ALIGNED(Data)                                        \
      if ((Off | NumBytes) & 3) {                                               \
        FS_DEBUG_ERROROUT((FS_MTYPE_DRIVER, "NOR_K90: Data not aligned.\n"));   \
        FS_X_Panic(FS_ERROR_INVALID_PARA);                                      \
      }
  #endif
#else
  #define ASSERT_DATA_IS_ALIGNED(Data)
#endif

/*********************************************************************
*
*      Types
*
**********************************************************************
*/

/*********************************************************************
*
*      SECTOR_BLOCK
*
*  A sector block defines a number of adjacent sectors of the same size.
*  It therefor contains the size (of one sector) and number of sectors in the block.
*  The entire sectorization can be described with just a few sector blocks.
*/
typedef struct {
  U32 SectorSize;
  U32 NumSectors;
} SECTOR_BLOCK;

typedef struct {
  U32 BaseAddr;               // Address of the first byte of flash        (configured)
  U32 StartAddrConf;          // Address of the first byte used as storage (configured)
  U32 NumBytes;               // Number of bytes to be used as storage     (configured)
  U32 StartAddrUsed;          // Start address actually used (aligned to start of a sector)
  SECTOR_BLOCK aSectorBlockUsed[MAX_SECTOR_BLOCKS];
  U8  NumSectorBlocksUsed;    // Number of sector blocks used as storage.
  U8  FirstSectorIndex;       // Index of the first physical sector used for storage.
  U8  NumSectorsUsed;         // Number of physical sectors used as storage.
} NOR_STM32F2_INST;

/*********************************************************************
*
*      Static const
*
**********************************************************************
*/
static const SECTOR_BLOCK _aSectorBlockDevice[MAX_SECTOR_BLOCKS] = {   // Organization of internal flash
  {16  * 1024, 4},
  {64  * 1024, 1},
  {128 * 1024, 7}
};

/*********************************************************************
*
*      Static data
*
**********************************************************************
*/
static NOR_STM32F2_INST * _pInst;
#if USE_OS
  static volatile U32 _Status;
#endif

/*********************************************************************
*
*      Static code
*
**********************************************************************
*/

#if USE_OS

/**********************************************************
*
*       FLASH_IRQHandler
*
*   Function description
*     Handles the flash operation complete interrupt.
*/
void FLASH_IRQHandler(void);
void FLASH_IRQHandler(void) {
  OS_EnterInterrupt();          // Inform embOS that interrupt code is running.
  _Status  = FLASH_SR;          // Save the status to a static variable and check it in the task.
  FLASH_SR = 0                  // Clear the flags to prevent further interrupts.
           | (1uL << SR_EOP)
           | (1uL << SR_OPERR)
           | (1uL << SR_WRPERR)
           | (1uL << SR_PGAERR)
           | (1uL << SR_PGPERR)
           | (1uL << SR_PGSERR)
           ;
  FS_X_OS_Signal();             // Wake up the task.
  OS_LeaveInterrupt();          // Inform embOS that interrupt code is left.
}

#endif

/*********************************************************************
*
*      _Lock
*
*   Function description
*     Locks access to flash control register.
*
*/
static void _Lock(void) {
  FLASH_CR = (1uL << CR_LOCK);
}

/*********************************************************************
*
*      _Unlock
*
*   Function description
*     Unlocks access to flash control register.
*
*/
static void _Unlock(void) {
  FLASH_KEYR = FLASH_KEY1;
  FLASH_KEYR = FLASH_KEY2;
}

/*********************************************************************
*
*      _InvalidateCache
*
*/
static void _InvalidateCache(void) {
  if (FLASH_ACR & (1uL << ACR_DCEN)) {    // Data cache enabled?
    FLASH_ACR &= ~(1uL << ACR_DCEN);      // Disable data cache in order to invalidate it.
    FLASH_ACR |=   1uL << ACR_DCRST;
    FLASH_ACR &= ~(1uL << ACR_DCRST);
    FLASH_ACR |=   1uL << ACR_DCEN;       // Re-enable data cache.
  }
}

/*********************************************************************
*
*      _EnableProgram
*
*   Function description
*     Executes a programming operation.
*
*/
static void _EnableProgram(int LineSize) {
  U32 AccessWidth;

#if USE_OS
  _Status = 0;
#endif
  //
  // Clear all error flags.
  //
  FLASH_SR = 0
           | (1uL << SR_WRPERR)
           | (1uL << SR_PGAERR)
           | (1uL << SR_PGPERR)
           | (1uL << SR_PGSERR)
           ;
  if (LineSize == 4) {
    AccessWidth = CR_PSIZE_32;
  } else if (LineSize == 2) {
    AccessWidth = CR_PSIZE_16;
  } else {
    AccessWidth = CR_PSIZE_8;
  }
  FLASH_CR |= (AccessWidth << CR_PSIZE)   // Enable programming with the specified access width.
           |  (1uL << CR_PG)
#if USE_OS
           | (1uL << CR_ERRIE)            // Enable interrupts.
           | (1uL << CR_EOPIE)
#endif
           ;
}

/*********************************************************************
*
*      _StartErase
*
*   Function description
*     Executes an erase operation.
*
*/
static void _StartErase(void) {
#if USE_OS
  _Status = 0;
#endif
  //
  // Clear all error flags.
  //
  FLASH_SR = 0
           | (1uL << SR_WRPERR)
           | (1uL << SR_PGAERR)
           | (1uL << SR_PGPERR)
           | (1uL << SR_PGSERR)
           ;
  //
  // Start erasing.
  //
  FLASH_CR |= 0
           | (1uL << CR_STRT)
#if USE_OS
           | (1uL << CR_ERRIE)            // Enable interrupts.
           | (1uL << CR_EOPIE)
#endif
           ;
}

/*********************************************************************
*
*      _WaitForEndOfOperation
*
*   Function description
*     Waits until a flash operation completes.
*
*/
static int _WaitForEndOfOperation(void) {
  U32  Status;
  int r;

  r = 1;    // Set to indicate an error.
  //
  // Wait for the operation to complete
  //
  do {
#if USE_OS
    FS_X_OS_Wait(WAIT_TIMEOUT_MAX);
    Status = _Status;
#else
    Status = FLASH_SR;
#endif
  } while (Status & (1uL << SR_BUSY));
  //
  // OK, operation completed. Check the result.
  //
  if        (Status & (1uL << SR_WRPERR)) {
    FS_DEBUG_ERROROUT((FS_MTYPE_DRIVER, "NOR_STM32F2: Flash operation failed. Flash is write-protected.\n"));
  } else if (Status & (1uL << SR_PGAERR)) {
    FS_DEBUG_ERROROUT((FS_MTYPE_DRIVER, "NOR_STM32F2: Flash operation failed. Access not aligned.\n"));
  } else if (Status & (1uL << SR_PGPERR)) {
    FS_DEBUG_ERROROUT((FS_MTYPE_DRIVER, "NOR_STM32F2: Flash operation failed. Invalid number of bytes.\n"));
  } else if (Status & (1uL << SR_PGSERR)) {
    FS_DEBUG_ERROROUT((FS_MTYPE_DRIVER, "NOR_STM32F2: Flash operation failed. Invalid configuration.\n"));
  } else {
    r = 0;
  }
  return r;
}

#if FS_NOR_LINE_SIZE == 1
/*********************************************************************
*
*      _WriteOff8
*/
static int _WriteOff8(U32 Off, const void * pData, U32 NumBytes) {
  U8  * pDest;
  U8  * pSrc;
  U32   StartAddr;
  int   r;

  ASSERT_DATA_IS_ALIGNED(Off | NumBytes);
  r = 0;              // OK, data written to flash.
  StartAddr = _pInst->StartAddrUsed;
  pDest     = (U8 *)StartAddr + Off;
  pSrc      = (U8 *)pData;
  _Unlock();
  _EnableProgram(FS_NOR_LINE_SIZE);
  do {
    *pDest++ = *pSrc++;
    r = _WaitForEndOfOperation();
    if (r) {
      r = 1;       // Error, operation failed.
      break;
    }
  } while (--NumBytes);
  _Lock();
  return r;
}
#endif

#if FS_NOR_LINE_SIZE == 2
/*********************************************************************
*
*      _WriteOff16
*/
static int _WriteOff16(U32 Off, const void * pData, U32 NumBytes) {
  U16 * pDest;
  U16 * pSrc;
  U32   StartAddr;
  int   r;
  U32   NumItems;

  ASSERT_DATA_IS_ALIGNED(Off | NumBytes);
  r = 0;              // OK, data written to flash.
  NumItems   = NumBytes >> 1;
  Off      >>= 1;
  StartAddr = _pInst->StartAddrUsed;
  pDest     = (U16 *)StartAddr + Off;
  pSrc      = (U16 *)pData;
  _Unlock();
  _EnableProgram(FS_NOR_LINE_SIZE);
  do {
    *pDest++ = *pSrc++;
    r = _WaitForEndOfOperation();
    if (r) {
      r = 1;       // Error, operation failed.
      break;
    }
  } while (--NumItems);
  _Lock();
  return r;
}
#endif

#if FS_NOR_LINE_SIZE == 4
/*********************************************************************
*
*      _WriteOff32
*/
static int _WriteOff32(U32 Off, const void * pData, U32 NumBytes) {
  U32 * pDest;
  U32 * pSrc;
  U32   StartAddr;
  int   r;
  U32   NumItems;

  ASSERT_DATA_IS_ALIGNED(Off | NumBytes);
  r = 0;              // OK, data written to flash.
  NumItems   = NumBytes >> 2;
  Off      >>= 2;
  StartAddr = _pInst->StartAddrUsed;
  pDest     = (U32 *)StartAddr + Off;
  pSrc      = (U32 *)pData;
  _Unlock();
  _EnableProgram(FS_NOR_LINE_SIZE);
  do {
    *pDest++ = *pSrc++;
    r = _WaitForEndOfOperation();
    if (r) {
      r = 1;       // Error, operation failed.
      break;
    }
  } while (--NumItems);
  _Lock();
  return r;
}
#endif

/*********************************************************************
*
*      _InitHW
*
*/
static void _InitHW(void) {
#if USE_OS
  NVIC_SetPriority(FLASH_IRQn, FLASH_PRIO);
  NVIC_EnableIRQ(FLASH_IRQn);
#endif
}

/*********************************************************************
*
*      _Init
*
*   Function description
*     Identifies the MCU and computes the start address and the number of sectors used as storage.
*/
static void _Init(void) {
  U32 NumSectorBlocks;
  U8  NumSectorBlocksUsed;
  U32 NumBytesToSkip;
  U32 NumBytesRem;
  U32 NumSectorsUsed;
  U32 NumBytesSkipped;
  U32 i;
  U32 BaseAddr;
  U32 StartAddrUsed;
  U32 SectorIndex;
  U8  FirstSectorIndex;
  SECTOR_BLOCK * pSectorBlockUsed;

  //
  // Read physical sector block information and add it to the list of used blocks
  //
  NumSectorBlocks     = COUNTOF(_aSectorBlockDevice);
  NumSectorBlocksUsed = 0;
  NumSectorsUsed      = 0;
  NumBytesSkipped     = 0;
  BaseAddr            = _pInst->BaseAddr;
  NumBytesToSkip      = _pInst->StartAddrConf - BaseAddr;
  NumBytesRem         = _pInst->NumBytes;
  StartAddrUsed       = 0;
  pSectorBlockUsed    = _pInst->aSectorBlockUsed;
  SectorIndex         = 0;
  FirstSectorIndex    = 0;
  for (i = 0; i < NumSectorBlocks; i++) {
    U32 NumSectors;
    U32 SectorSize;

    NumSectors = _aSectorBlockDevice[i].NumSectors;
    SectorSize = _aSectorBlockDevice[i].SectorSize;
    //
    // Take care of bytes to skip before data area.
    //
    while (NumSectors && (NumBytesToSkip > 0)) {
      if (NumBytesToSkip > SectorSize) {
        NumBytesToSkip  -= SectorSize;
      } else {
        NumBytesToSkip   = 0;
      }
      NumBytesSkipped += SectorSize;
      NumSectors--;
      SectorIndex++;
    }
    if (NumSectors) {
      U32 NumSectorsRem;

      NumSectorsRem = NumBytesRem / SectorSize;
      if (NumSectors > NumSectorsRem) {
        NumSectors  = NumSectorsRem;
        NumBytesRem = 0;      // No more sectors after this to make sure that the sectors are adjacent!
      } else {
        NumBytesRem -= NumSectors * SectorSize;
      }
      //
      // Take care of bytes to skip after data area.
      //
      if (NumSectors) {
        if (NumSectorBlocksUsed == 0) {
          StartAddrUsed    = BaseAddr + NumBytesSkipped;      // Remember address of first sector used.
          FirstSectorIndex = SectorIndex;
        }
        pSectorBlockUsed->SectorSize = SectorSize;
        pSectorBlockUsed->NumSectors = NumSectors;
        ++NumSectorBlocksUsed;
        ++pSectorBlockUsed;
        NumSectorsUsed += NumSectors;
      }
    }
  }
  _pInst->NumSectorBlocksUsed = NumSectorBlocksUsed;
  _pInst->NumSectorsUsed      = NumSectorsUsed;
  _pInst->StartAddrUsed       = StartAddrUsed;
  _pInst->FirstSectorIndex    = FirstSectorIndex;
  //
  // Perform a sanity check.
  //
  if (NumSectorBlocksUsed == 0) {
    FS_DEBUG_ERROROUT((FS_MTYPE_DRIVER, "NOR_STM32F2: Flash size to small for this configuration. 0 bytes available."));
  }
  _InitHW();
}

/*********************************************************************
*
*      _GetSectorOff
*/
static U32 _GetSectorOff(unsigned SectorIndex) {
  unsigned i;
  unsigned NumBlocks;
  unsigned NumSectors;
  U32      SectorSize;
  U32      Off;

  NumBlocks = _pInst->NumSectorBlocksUsed;
  Off = 0;
  for (i = 0; i < NumBlocks; i++) {
    NumSectors = _pInst->aSectorBlockUsed[i].NumSectors;
    SectorSize = _pInst->aSectorBlockUsed[i].SectorSize;
    if (SectorIndex < NumSectors) {
      NumSectors = SectorIndex;
    }
    Off += NumSectors * SectorSize;
    SectorIndex -= NumSectors;   // Number of remaining sectors
  }
  return Off;
}

/*********************************************************************
*
*      _GetSectorSize
*/
static U32 _GetSectorSize(unsigned SectorIndex) {
  unsigned i;
  unsigned NumBlocks;
  unsigned NumSectors;
  U32      SectorSize;

  NumBlocks = _pInst->NumSectorBlocksUsed;
  for (i = 0; i < NumBlocks; i++) {
    NumSectors = _pInst->aSectorBlockUsed[i].NumSectors;
    SectorSize = _pInst->aSectorBlockUsed[i].SectorSize;
    if (SectorIndex < NumSectors) {
      return SectorSize;
    }
    SectorIndex -= NumSectors;   // Number of remaining sectors
  }
  return 0;                      // Invalid sector index. Should not happen.
}


/*********************************************************************
*
*      _AllocIfRequired
*
*   Function description
*     Allocates an instance of the physical layer if necessary.
*
*/
static NOR_STM32F2_INST * _AllocIfRequired(void) {
  if (_pInst == NULL) {
     _pInst = (NOR_STM32F2_INST *)FS_AllocZeroed(sizeof(NOR_STM32F2_INST));
  }
  return _pInst;
}

/*********************************************************************
*
*      Global functions
*
**********************************************************************
*/

/*********************************************************************
*
*      _WriteOff
*
*   Function description
*     This routine writes data into any section of the flash. It does not
*     check if this section has been previously erased; this is in the
*     responsibility of the user program.
*     Data written into multiple sectors at a time can be handled by this
*     routine.
*
*   Parameters
*     Unit      Unit number of physical layer
*     Off       Byte offset to read from
*     pData     [IN]  Data to be written to flash
*               [OUT] ---
*     NumBytes  Number of bytes to write
*
*   Return value
*     ==0   OK, data has been written
*     !=    An error occurred
*/
static int _WriteOff(U8 Unit, U32 Off, const void * pData, U32 NumBytes) {
  int   r;

  FS_USE_PARA(Unit);
  r = 1;                // Set to indicate an error.
  if (_pInst) {
    r = 0;              // OK, data written to flash.
    if (NumBytes) {
#if FS_NOR_LINE_SIZE == 1
      r = _WriteOff8(Off, pData, NumBytes);
#endif
#if FS_NOR_LINE_SIZE == 2
      r = _WriteOff16(Off, pData, NumBytes);
#endif
#if FS_NOR_LINE_SIZE == 4
      r = _WriteOff32(Off, pData, NumBytes);
#endif
    }
  }
  return r;
}

/*********************************************************************
*
*       _ReadOff
*
*   Function description
*     Physical layer function. Reads data from the given byte offset of the flash.
*
*   Parameters
*     Unit      Unit number of physical layer
*     pData     [IN]  ---
*               [OUT] Data read from flash
*     Off       Byte offset to read from
*     NumBytes  Number of bytes to read
*
*   Return value
*     ==0   OK, data read from flash
*     !=0   An error occurred
*/
static int _ReadOff(U8 Unit, void * pData, U32 Off, U32 NumBytes) {
  U32 ReadAddr;
  U32 StartAddr;
  int r;

  FS_USE_PARA(Unit);
  r = 1;        // Set to indicate an error.
  if (_pInst) {
    StartAddr = _pInst->StartAddrUsed;
    ReadAddr  = StartAddr + Off;
    FS_MEMCPY(pData, (U8 *)ReadAddr, NumBytes);
    r = 0;      // OK, data read from flash.
  }
  return r;
}

/*********************************************************************
*
*      _EraseSector
*
*   Function description
*     Physical layer function. Erases one physical sector.
*
*   Parameters
*     Unit          Unit number of physical layer
*     SectorIndex   Index of physical sector to erase
*
*   Return value
*     ==0   OK, sector is erased
*     !=0   An error occurred
*/
static int _EraseSector(U8 Unit, unsigned int SectorIndex) {
  int r;

  FS_USE_PARA(Unit);
  r = 1;
  if (_pInst) {
    ASSERT_SECTOR_INDEX_IS_IN_RANGE(SectorIndex);
    SectorIndex += _pInst->FirstSectorIndex;
    _Unlock();
    //
    // Set flash controller to sector erase mode and select sector to be erased.
    //
    FLASH_CR = (1uL << CR_SER)
             | ((SectorIndex & CR_SNB_MAX) << CR_SNB)
             ;
    _StartErase();
    r = _WaitForEndOfOperation();
    //
    // The cache must be invalidate after an erase operation to make sure the data is consistent (See [2] page 13).
    //
    _InvalidateCache();
    _Lock();
  }
  return r;
}

/*********************************************************************
*
*       _GetSectorInfo
*
*   Function description
*     Physical layer function. Returns the byte offset and length in bytes of
*     the physical sector with the given index.
*
*   Parameters
*     Unit          Unit number of physical layer
*     SectorIndex   Index of physical sector to query. The index is relative to start of storage.
*     pOff          [IN]  ---
*                   [OUT] Byte offset of physical sector
*     pLen          [IN]  ---
*                   [OUT] Lengt of physical sector in bytes
*/
static void _GetSectorInfo(U8 Unit, unsigned int SectorIndex, U32 * pOff, U32 * pLen) {
  U32 SectorOff;
  U32 SectorSize;

  FS_USE_PARA(Unit);
  SectorOff  = 0;
  SectorSize = 0;
  if (_pInst) {
    //
    // Fail if the SectorIndex is out of bounds or the device parameters are not set.
    //
    ASSERT_SECTOR_INDEX_IS_IN_RANGE(SectorIndex);
    //
    // Compute result.
    //
    SectorSize = _GetSectorSize(SectorIndex);
    SectorOff  = _GetSectorOff(SectorIndex);
  }
  if (pOff) {
    *pOff = SectorOff;
  }
  if (pLen) {
    *pLen = SectorSize;
  }
}

/*********************************************************************
*
*       _GetNumSectors
*
*   Function description
*     Physical layer function. Returns the number of logical sectors which can be stored to flash.
*
*   Parameters
*     Unit    Unit number of physical layer
*
*   Return value
*     <=0     An error occurred
*     > 0     Number of logical sectors which can be stored to NOR flash
*/
static int _GetNumSectors(U8 Unit) {
  int r;

  FS_USE_PARA(Unit);
  r = 0;
  if (_pInst) {
    r =_pInst->NumSectorsUsed;
  }
  return r;
}

/*********************************************************************
*
*       _Configure
*
*   Function description
*     Physical layer function. Configures the instance of the physical layer.
*
*   Parameters
*     Unit        Unit number of physical layer
*     BaseAddr    Address of the first byte in flash
*     StartAddr   Address of the first byte to be used as storage by the physical layer
*     NumBytes    Number of bytes to use as storage
*/
static void _Configure(U8 Unit, U32 BaseAddr, U32 StartAddr, U32 NumBytes) {
  FS_USE_PARA(Unit);
  _AllocIfRequired();
  if (_pInst) {
    _pInst->BaseAddr      = BaseAddr;
    _pInst->StartAddrConf = StartAddr;
    _pInst->NumBytes      = NumBytes;
    _Init();
  }
}

/*********************************************************************
*
*       _OnSelectPhy
*
*   Function description
*     Physical layer function. Called right after selection of the physical layer.
*
*   Parameters
*     Unit    Unit number of physical layer
*/
static void _OnSelectPhy(U8 Unit) {
  FS_USE_PARA(Unit);
  _AllocIfRequired();
}

/*********************************************************************
*
*       _DeInit
*
*   Function description
*     Physical layer function. Frees up the memory allocated for the instance.
*
*   Parameters
*     Unit    Unit number of physical layer
*/
static void _DeInit(U8 Unit) {
  FS_USE_PARA(Unit);
#if FS_SUPPORT_DEINIT
  FS_Free((void *)_pInst);
  _pInst = NULL;
#endif
}

/*********************************************************************
*
*      Public code
*
**********************************************************************
*/

/*********************************************************************
*
*       Global data
*
**********************************************************************
*/
const FS_NOR_PHY_TYPE FS_NOR_PHY_ST_STM32F207 = {
  _WriteOff,
  _ReadOff,
  _EraseSector,
  _GetSectorInfo,
  _GetNumSectors,
  _Configure,
  _OnSelectPhy,
  _DeInit
};

/*************************** End of file ****************************/
