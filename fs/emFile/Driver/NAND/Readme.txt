
The following hardware samples for NAND  are available:

NAND_HW_Template.c             Generic file containing only blank functions.                                
                               Typically used as starting point when flash is connected to address/data bus.
NAND_HW_TemplatePort.c         Generic file containing functions to access flash connected via port pins.
                               Typically used as starting point when writing new hardware routines for
                               flash connected via port pins.


