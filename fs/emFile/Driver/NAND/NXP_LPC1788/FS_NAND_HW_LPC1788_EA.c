/*********************************************************************
*                SEGGER MICROCONTROLLER GmbH & Co. KG                *
*        Solutions for real time microcontroller applications        *
**********************************************************************
*                                                                    *
*        (c) 2003-2012     SEGGER Microcontroller GmbH & Co KG       *
*                                                                    *
*        Internet: www.segger.com    Support:  support@segger.com    *
*                                                                    *
**********************************************************************

**** emFile file system for embedded applications ****
emFile is protected by international copyright laws. Knowledge of the
source code may not be used to write a similar product. This file may
only be used in accordance with a license and should not be re-
distributed in any way. We appreciate your understanding and fairness.
----------------------------------------------------------------------
File        : FS_NAND_HW_LPC1788_EA.c.c
Purpose     : NAND flash HW layer for Embedded Artists LPC1788
---------------------------END-OF-HEADER------------------------------
*/

#include "NAND_X_HW.h"
#include "FS_Int.h"       // For FS_MEMCPY
#include <string.h>

/*********************************************************************
*
*       defines, non-configurable
*
**********************************************************************
*/
#define NAND_BASE_ADDR          0x90000000
#define NAND_DATA               (U16 *)(NAND_BASE_ADDR + 0x00000000)
#define NAND_ADDR               (U16 *)(NAND_BASE_ADDR + 0x00080000)
#define NAND_CMD                (U16 *)(NAND_BASE_ADDR + 0x00100000)

/*********************************************************************
*
*       External memory controller registers
*/
#define EMC_BASE_ADDR           0x2009C000
#define EMC_CONTROL             (*(volatile U32 *)(EMC_BASE_ADDR + 0x0000))
#define EMC_CONFIG              (*(volatile U32 *)(EMC_BASE_ADDR + 0x0008))
#define EMC_STATIC_CONFIG1      (*(volatile U32 *)(EMC_BASE_ADDR + 0x0220))
#define EMC_STATIC_WAITWEN1     (*(volatile U32 *)(EMC_BASE_ADDR + 0x0224))
#define EMC_STATIC_WAITOEN1     (*(volatile U32 *)(EMC_BASE_ADDR + 0x0228))
#define EMC_STATIC_WAITRD1      (*(volatile U32 *)(EMC_BASE_ADDR + 0x022C))
#define EMC_STATIC_WAITPAGE1    (*(volatile U32 *)(EMC_BASE_ADDR + 0x0230))
#define EMC_STATIC_WAITWR1      (*(volatile U32 *)(EMC_BASE_ADDR + 0x0234))
#define EMC_STATIC_WAITTURN1    (*(volatile U32 *)(EMC_BASE_ADDR + 0x0238))

/*********************************************************************
*
*       Clock and power control registers
*/
#define PCONP                   (*(volatile U32 *)0x400FC0C4)

/*********************************************************************
*
*       I/O configuration registers
*/
#define IOCON_BASE_ADDR         0x4002C000
#define IOCON_P2_21             (*(volatile U32 *)(IOCON_BASE_ADDR + 0x0154))
#define IOCON_P3_00             (*(volatile U32 *)(IOCON_BASE_ADDR + 0x0180))
#define IOCON_P3_01             (*(volatile U32 *)(IOCON_BASE_ADDR + 0x0184))
#define IOCON_P3_02             (*(volatile U32 *)(IOCON_BASE_ADDR + 0x0188))
#define IOCON_P3_03             (*(volatile U32 *)(IOCON_BASE_ADDR + 0x018C))
#define IOCON_P3_04             (*(volatile U32 *)(IOCON_BASE_ADDR + 0x0190))
#define IOCON_P3_05             (*(volatile U32 *)(IOCON_BASE_ADDR + 0x0194))
#define IOCON_P3_06             (*(volatile U32 *)(IOCON_BASE_ADDR + 0x0198))
#define IOCON_P3_07             (*(volatile U32 *)(IOCON_BASE_ADDR + 0x019C))
#define IOCON_P4_00             (*(volatile U32 *)(IOCON_BASE_ADDR + 0x0200))
#define IOCON_P4_01             (*(volatile U32 *)(IOCON_BASE_ADDR + 0x0204))
#define IOCON_P4_02             (*(volatile U32 *)(IOCON_BASE_ADDR + 0x0208))
#define IOCON_P4_03             (*(volatile U32 *)(IOCON_BASE_ADDR + 0x020C))
#define IOCON_P4_04             (*(volatile U32 *)(IOCON_BASE_ADDR + 0x0210))
#define IOCON_P4_05             (*(volatile U32 *)(IOCON_BASE_ADDR + 0x0214))
#define IOCON_P4_06             (*(volatile U32 *)(IOCON_BASE_ADDR + 0x0218))
#define IOCON_P4_07             (*(volatile U32 *)(IOCON_BASE_ADDR + 0x021C))
#define IOCON_P4_08             (*(volatile U32 *)(IOCON_BASE_ADDR + 0x0220))
#define IOCON_P4_09             (*(volatile U32 *)(IOCON_BASE_ADDR + 0x0224))
#define IOCON_P4_10             (*(volatile U32 *)(IOCON_BASE_ADDR + 0x0228))
#define IOCON_P4_11             (*(volatile U32 *)(IOCON_BASE_ADDR + 0x022C))
#define IOCON_P4_12             (*(volatile U32 *)(IOCON_BASE_ADDR + 0x0230))
#define IOCON_P4_13             (*(volatile U32 *)(IOCON_BASE_ADDR + 0x0234))
#define IOCON_P4_14             (*(volatile U32 *)(IOCON_BASE_ADDR + 0x0238))
#define IOCON_P4_15             (*(volatile U32 *)(IOCON_BASE_ADDR + 0x023C))
#define IOCON_P4_16             (*(volatile U32 *)(IOCON_BASE_ADDR + 0x0240))
#define IOCON_P4_17             (*(volatile U32 *)(IOCON_BASE_ADDR + 0x0244))
#define IOCON_P4_18             (*(volatile U32 *)(IOCON_BASE_ADDR + 0x0248))
#define IOCON_P4_19             (*(volatile U32 *)(IOCON_BASE_ADDR + 0x024C))
#define IOCON_P4_20             (*(volatile U32 *)(IOCON_BASE_ADDR + 0x0250))
#define IOCON_P4_21             (*(volatile U32 *)(IOCON_BASE_ADDR + 0x0254))
#define IOCON_P4_22             (*(volatile U32 *)(IOCON_BASE_ADDR + 0x0258))
#define IOCON_P4_23             (*(volatile U32 *)(IOCON_BASE_ADDR + 0x025C))
#define IOCON_P4_24             (*(volatile U32 *)(IOCON_BASE_ADDR + 0x0260))
#define IOCON_P4_25             (*(volatile U32 *)(IOCON_BASE_ADDR + 0x0264))
#define IOCON_P4_26             (*(volatile U32 *)(IOCON_BASE_ADDR + 0x0268))
#define IOCON_P4_27             (*(volatile U32 *)(IOCON_BASE_ADDR + 0x026C))
#define IOCON_P4_28             (*(volatile U32 *)(IOCON_BASE_ADDR + 0x0270))
#define IOCON_P4_29             (*(volatile U32 *)(IOCON_BASE_ADDR + 0x0274))
#define IOCON_P4_30             (*(volatile U32 *)(IOCON_BASE_ADDR + 0x0278))
#define IOCON_P4_31             (*(volatile U32 *)(IOCON_BASE_ADDR + 0x027C))

/*********************************************************************
*
*       GPIO registers
*/
#define GPIO_BASE_ADDR          0x20098000
#define GPIO_FIO2PIN            (*(volatile U32 *)(GPIO_BASE_ADDR + 0x0054))

/*********************************************************************
*
*       Peripheral power control
*/
#define PCONP_PCEMC             11
#define PCONP_PCGPIO            15

/*********************************************************************
*
*       Static data
*
**********************************************************************
*/
static U16 * _pCurrentNANDAddr;

/*********************************************************************
*
*       Static code
*
**********************************************************************
*/

/*********************************************************************
*
*       _CopyData8
*/
static void _CopyData8(void * pDest, const void * pSrc, unsigned NumBytes) {
  char * pd;
  const char * ps;
  pd = (char*)pDest;
  ps = (const char*)pSrc;
  //
  // Copy bytes, one at a time.
  //
  if (NumBytes) {
    do {
      *(char*)pd++ = *(const char*)ps++;
    } while (--NumBytes);
  }
}

/*********************************************************************
*
*       _CopyData16
*/
static void _CopyData16(void * pDest, const void * pSrc, unsigned NumBytes) {
  char * pd;
  const char * ps;
  pd = (char*)pDest;
  ps = (const char*)pSrc;

  if ((((U32)ps & 1) == 0) && (((U32)pd & 1) == 0)) {
    unsigned NumHWords = NumBytes >> 1;
    if (NumHWords) {
      do {
        *(short *)pd = *(const short *)ps;
        pd += 2;
        ps += 2;
      } while (--NumHWords);
    }
    NumBytes &= 1;
  }
  //
  // Copy bytes, one at a time.
  //
  if (NumBytes) {
    do {
      *(char*)pd++ = *(const char*)ps++;
    } while (--NumBytes);
  }
}

/*********************************************************************
*
*       Public code
*
**********************************************************************
*/

/*********************************************************************
*
*       FS_NAND_HW_X_EnableCE
*/
void FS_NAND_HW_X_EnableCE(U8 Unit) {
  FS_USE_PARA(Unit);
}

/*********************************************************************
*
*       FS_NAND_HW_X_DisableCE
*/
void FS_NAND_HW_X_DisableCE(U8 Unit) {
  FS_USE_PARA(Unit);
}

/*********************************************************************
*
*       FS_NAND_HW_X_SetData
*/
void FS_NAND_HW_X_SetDataMode(U8 Unit) {
  FS_USE_PARA(Unit);
  _pCurrentNANDAddr = NAND_DATA;    // CLE low, ALE low
}

/*********************************************************************
*
*       FS_NAND_HW_X_SetCmd
*/
void FS_NAND_HW_X_SetCmdMode(U8 Unit) {
  FS_USE_PARA(Unit);
  _pCurrentNANDAddr = NAND_CMD;     // CLE high, ALE low
}

/*********************************************************************
*
*       FS_NAND_HW_X_SetAddr
*/
void FS_NAND_HW_X_SetAddrMode(U8 Unit) {
  FS_USE_PARA(Unit);
  _pCurrentNANDAddr = NAND_ADDR;    // CLE low, ALE high
}

/*********************************************************************
*
*       FS_NAND_HW_X_Read_x8
*/
void FS_NAND_HW_X_Read_x8(U8 Unit, void * pData, unsigned NumBytes) {
  FS_USE_PARA(Unit);
  _CopyData8(pData, _pCurrentNANDAddr, NumBytes);
}

/*********************************************************************
*
*       FS_NAND_HW_X_Read_x16
*/
void FS_NAND_HW_X_Read_x16(U8 Unit, void * pData, unsigned NumBytes) {
  FS_USE_PARA(Unit);
  _CopyData16(pData, _pCurrentNANDAddr, NumBytes);
}

/*********************************************************************
*
*       FS_NAND_HW_X_Write_x8
*/
void FS_NAND_HW_X_Write_x8(U8 Unit, const void * pData, unsigned NumBytes) {
  FS_USE_PARA(Unit);
  _CopyData8(_pCurrentNANDAddr, pData, NumBytes);
}

/*********************************************************************
*
*       FS_NAND_HW_X_Write_x16
*/
void FS_NAND_HW_X_Write_x16(U8 Unit, const void * pData, unsigned NumBytes) {
  FS_USE_PARA(Unit);
  _CopyData16(_pCurrentNANDAddr, pData, NumBytes);
}

/*********************************************************************
*
*       FS_NAND_HW_X_Init_x8
*/
void FS_NAND_HW_X_Init_x8(U8 Unit) {
  FS_USE_PARA(Unit);
  //
  // Turn on the clock of EMC and GPIO.
  //
  PCONP |= (1uL << PCONP_PCEMC)
        |  (1uL << PCONP_PCGPIO)
        ;
  //
  // Setup pin to be used as address/data/control bus pins
  //
  IOCON_P3_00 |= 1; // D0
  IOCON_P3_01 |= 1; // D1
  IOCON_P3_02 |= 1; // D2
  IOCON_P3_03 |= 1; // D3
  IOCON_P3_04 |= 1; // D4
  IOCON_P3_05 |= 1; // D5
  IOCON_P3_06 |= 1; // D6
  IOCON_P3_07 |= 1; // D7

  IOCON_P4_00 |= 1; // A0
  IOCON_P4_01 |= 1; // A1
  IOCON_P4_02 |= 1; // A2
  IOCON_P4_03 |= 1; // A3
  IOCON_P4_04 |= 1; // A4
  IOCON_P4_05 |= 1; // A5
  IOCON_P4_06 |= 1; // A6
  IOCON_P4_07 |= 1; // A7
  IOCON_P4_08 |= 1; // A8
  IOCON_P4_09 |= 1; // A9
  IOCON_P4_10 |= 1; // A10
  IOCON_P4_11 |= 1; // A11
  IOCON_P4_12 |= 1; // A12
  IOCON_P4_13 |= 1; // A13
  IOCON_P4_14 |= 1; // A14
  IOCON_P4_15 |= 1; // A15
  IOCON_P4_16 |= 1; // A16
  IOCON_P4_17 |= 1; // A17
  IOCON_P4_18 |= 1; // A18
  IOCON_P4_19 |= 1; // A19
  IOCON_P4_20 |= 1; // A20
  IOCON_P4_21 |= 1; // A21
  IOCON_P4_22 |= 1; // A22
  IOCON_P4_23 |= 1; // A23
  IOCON_P4_24 |= 1; // OEN
  IOCON_P4_25 |= 1; // WEN
  IOCON_P4_26 |= 1; // BLSN[0]
  IOCON_P4_27 |= 1; // BLSN[1]
  IOCON_P4_30 |= 1; // CSN[0]
  IOCON_P4_31 |= 1; // CSN[1]

  //
  // Configure the external memory controller.
  //
  EMC_CONTROL = (1uL << 0);             // Enable the external memory controller.
  EMC_CONFIG  = 0;
  //
  // Configure CS1 which interface to NAND flash.
  //
  EMC_STATIC_WAITWEN1  = 0x00000002;    // n + 1 CCLK cycle delay between assertion of chip select and write enable
  EMC_STATIC_WAITOEN1  = 0x00000002;    // n cycle delay.
  EMC_STATIC_WAITRD1   = 0x00000008;    // n + 1 CCLK cycle delay
  EMC_STATIC_WAITPAGE1 = 0x0000001f;    // 32 CCLK cycle read access time
  EMC_STATIC_WAITWR1   = 0x00000008;    // n + 2 CCLK cycle write access time
  EMC_STATIC_WAITTURN1 = 0x0000000f;    // 16 CCLK turnaround cycles
  EMC_STATIC_CONFIG1   = 0x00000080;    // Bit lane state
}

/*********************************************************************
*
*       FS_NAND_HW_X_Init_x16
*/
void FS_NAND_HW_X_Init_x16(U8 Unit) {
  FS_NAND_HW_X_Init_x8(Unit);
}

/*********************************************************************
*
*       FS_NAND_HW_X_WaitWhileBusy
*
*   Function description
*     This function is called to check the busy pin of
*     the NAND flash.
*     When no busy pin is available this function should
*     return 1, so the upper layer can check if NAND
*     flash is busy.
*/
int FS_NAND_HW_X_WaitWhileBusy(U8 Unit, unsigned us) {
  FS_USE_PARA(Unit);
  FS_USE_PARA(us);
  //
  // If pin is used for an alternate function, we need to return 1
  // in order to do a soft-wait
  //
  if (IOCON_P2_21 & 0x7uL) {
    return 1;
  }
  //
  // Check the pin and as soon as it is high, NAND chip is ready for the next command.
  //
  while((GPIO_FIO2PIN & (1 << 21)) == 0) {
    ;
  }
  return 0;
}

/**************************** end of file ***************************/

