/*********************************************************************
*                SEGGER MICROCONTROLLER GmbH & Co. KG                *
*        Solutions for real time microcontroller applications        *
**********************************************************************
*                                                                    *
*        (c) 2003-2012     SEGGER Microcontroller GmbH & Co KG       *
*                                                                    *
*        Internet: www.segger.com    Support:  support@segger.com    *
*                                                                    *
**********************************************************************

**** emFile file system for embedded applications ****
emFile is protected by international copyright laws. Knowledge of the
source code may not be used to write a similar product. This file may
only be used in accordance with a license and should not be re-
distributed in any way. We appreciate your understanding and fairness.
----------------------------------------------------------------------
----------------------------------------------------------------------
File        : NAND_HW_Template.c
Purpose     : Generic NAND flash hardware layer
----------------------------------------------------------------------
Known problems or limitations with current version
----------------------------------------------------------------------
None.
---------------------------END-OF-HEADER------------------------------
*/

/*********************************************************************
*
*       #include Section
*
**********************************************************************
*/
#include "FS_ConfDefaults.h"

#include "NAND_X_HW.h"
#include "FS_Int.h"       // For FS_MEMCPY
#include <string.h>

/*********************************************************************
*
*       #define Macros
*
**********************************************************************
*/
#define NAND_BASE_ADDR          0x40000000
#define NAND_DATA               (void *)(NAND_BASE_ADDR + 0x000000)
#define NAND_ADDR               (void *)(NAND_BASE_ADDR + 0x200000)
#define NAND_CMD                (void *)(NAND_BASE_ADDR + 0x400000)


#define NAND_SET_CE()
#define NAND_CLR_CE()
#define NAND_GET_BUSY()         1

/*********************************************************************
*
*       #define sfrs
*
**********************************************************************
*/

/*********************************************************************
*
*       Static data
*
**********************************************************************
*/

static volatile void * _pCurrentNANDAddr;

/*********************************************************************
*
*       Static code
*
**********************************************************************
*/

/*********************************************************************
*
*       FS_NAND_HW_X_EnableCE
*/
void FS_NAND_HW_X_EnableCE(U8 Unit) {
  NAND_CLR_CE(); // Enable NAND CE
}

/*********************************************************************
*
*       FS_NAND_HW_X_DisableCE
*/
void FS_NAND_HW_X_DisableCE(U8 Unit) {
  NAND_SET_CE(); // Disable NAND CE
}


/*********************************************************************
*
*       FS_NAND_HW_X_SetData
*/
void FS_NAND_HW_X_SetDataMode(U8 Unit) {
  FS_USE_PARA(Unit);
  // CLE low, ALE low
  _pCurrentNANDAddr = NAND_DATA;
}


/*********************************************************************
*
*       FS_NAND_HW_X_SetCmd
*/
void FS_NAND_HW_X_SetCmdMode(U8 Unit) {
  FS_USE_PARA(Unit);
  //CLE high, ALE low
  _pCurrentNANDAddr = NAND_CMD;
}

/*********************************************************************
*
*       FS_NAND_HW_X_SetAddr
*/
void FS_NAND_HW_X_SetAddrMode(U8 Unit) {
  FS_USE_PARA(Unit);
  // CLE low, ALE high
  _pCurrentNANDAddr = NAND_ADDR;
}

/*********************************************************************
*
*       FS_NAND_HW_X_Read_x8
*/
void FS_NAND_HW_X_Read_x8(U8 Unit, void * pData, unsigned NumBytes) {
  FS_MEMCPY(pData, (void *)_pCurrentNANDAddr, NumBytes);
}


/*********************************************************************
*
*       FS_NAND_HW_X_Write_x8
*/
void FS_NAND_HW_X_Write_x8(U8 Unit, const void * pData, unsigned NumBytes) {
  FS_MEMCPY((void *)_pCurrentNANDAddr, pData, NumBytes);
}

/*********************************************************************
*
*       FS_NAND_HW_X_Init_x8
*/
void FS_NAND_HW_X_Init_x8(U8 Unit) {
  //
  // Add here the initialization of your NAND hardware in 8bit mode.
  //
}

/*********************************************************************
*
*       FS_NAND_HW_X_Read_x16
*/
void FS_NAND_HW_X_Read_x16(U8 Unit, void * pData, unsigned NumBytes) {
  FS_MEMCPY(pData, (void *)_pCurrentNANDAddr, NumBytes);
}


/*********************************************************************
*
*       FS_NAND_HW_X_Write_x16
*/
void FS_NAND_HW_X_Write_x16(U8 Unit, const void * pData, unsigned NumBytes) {
  FS_MEMCPY((void *)_pCurrentNANDAddr, pData, NumBytes);
}

/*********************************************************************
*
*       FS_NAND_HW_X_Init_x16
*/
void FS_NAND_HW_X_Init_x16(U8 Unit) {
  //
  // Add here the initialization of your NAND hardware in 16bit,
  // when a 16 bit NAND flash is used.
  //
  
}

/*********************************************************************
*
*             FS_NAND_HW_X_WaitWhileBusy
*/
int FS_NAND_HW_X_WaitWhileBusy(U8 Unit, unsigned us) {
  //
  //  This function is called to check the busy pin of
  //  the NAND flash.
  //  When no busy pin is available this function should 
  //  return 1, so the upper layer can check if NAND
  //  flash is busy.
  //
  while (NAND_GET_BUSY() == 0) {
  }
  return 0;
}

/**************************** end of file ***************************/

