/*********************************************************************
*                SEGGER MICROCONTROLLER GmbH & Co. KG                *
*        Solutions for real time microcontroller applications        *
**********************************************************************
*                                                                    *
*        (c) 2003-2012     SEGGER Microcontroller GmbH & Co KG       *
*                                                                    *
*        Internet: www.segger.com    Support:  support@segger.com    *
*                                                                    *
**********************************************************************

**** emFile file system for embedded applications ****
emFile is protected by international copyright laws. Knowledge of the
source code may not be used to write a similar product. This file may
only be used in accordance with a license and should not be re-
distributed in any way. We appreciate your understanding and fairness.
----------------------------------------------------------------------
----------------------------------------------------------------------
File        : NAND_DF_HW_Template.c
Purpose     : Data Flash hardware layer template
---------------------------END-OF-HEADER------------------------------
*/

#include "FS.h"
#include "FS_DF_X_HW.h"


/*********************************************************************
*
*             #define Macros
*
**********************************************************************
*/

/*********************************************************************
*
*       Configurable macros
*
*   Please setup these macros according your hardware
*
*/
#define SPI_CS_PORT            P1
#define SPI_CLK_PORT           P2
#define SPI_DATAOUT_PORT       P2
#define SPI_DATAIN_PORT        P4

#define SPI_CS_PIN            1
#define SPI_CLK_PIN           3
#define SPI_DATAOUT_PIN       7
#define SPI_DATAIN_PIN        2



/*********************************************************************
*
*             #define Macros
*
*/
#define SPI_CLR_CS()          SPI_CS_PORT      &= ~(1 << SPI_CS_PIN)
#define SPI_SET_CS()          SPI_CS_PORT      |=  (1 << SPI_CS_PIN)
#define SPI_CLR_CLK()         SPI_CLK_PORT     &= ~(1 << SPI_CLK_PIN)
#define SPI_SET_CLK()         SPI_CLK_PORT     |=  (1 << SPI_CLK_PIN)
#define SPI_CLR_DATAOUT()     SPI_DATAOUT_PORT &= ~(1 << SPI_DATAOUT_PIN)
#define SPI_SET_DATAOUT()     SPI_DATAOUT_PORT |=  (1 << SPI_DATAOUT_PIN)
#define SPI_DATAIN()          (SPI_DATAIN_PORT &   (1 << SPI_DATAIN_PIN))
#define SPI_DELAY()           { int i=10L; while(i-- != 0); }
#define SPI_SETUP_PINS()

/*********************************************************************
*
*       Public code
*
**********************************************************************
*/

/*********************************************************************
*
*       FS_DF_HW_X_Init
*
*  Description:
*    Initialize the hardware before accessing the device.
*
*  Parameters:
*    Unit      - Device Index
*
*  Return Value:
*    == 0      - Hardware successfully intialized
*    != 0      - Error
*/
int FS_DF_HW_X_Init(U8 Unit) {
  return 0;
}


/*********************************************************************
*
*       FS_DF_HW_X_EnableCS
*
*  Description:
*    Sets the device active using the chip select (CS) line.
*
*  Parameters:
*    Unit      - Device Index
*
*/
void FS_DF_HW_X_EnableCS(U8 Unit) {
  SPI_CLR_CS();
}

/*********************************************************************
*
*       FS_DF_HW_X_DisableCS
*
*  Description:
*    Sets the device inactive using the chip select (CS) line.
*
*  Parameters:
*    Unit      - Device Index
*
*/
void FS_DF_HW_X_DisableCS(U8 Unit) {
  SPI_SET_CS();
}

/*********************************************************************
*
*       FS_DF_HW_X_Read
*
*  Description:
*    Reads a specified number of bytes from device.
*
*  Parameters:
*    Unit             - Device Index
*    pData            - Pointer to a data buffer
*    NumBytes         - Number of bytes
*
*/
void FS_DF_HW_X_Read(U8 Unit, U8 * pData, int NumBytes) {
  U8 bpos;
  U8 c;
  SPI_SET_DATAOUT();
  do {
    c = 0;
    bpos = 8; /* get 8 bits */
    do {
      SPI_CLR_CLK();
      c <<= 1;
      if (SPI_DATAIN()) {
        c |= 1;
      }
      SPI_SET_CLK();
    } while (--bpos);
    *pData++ = c;
  } while (--NumBytes);
}

/*********************************************************************
*
*       FS_DF_HW_X_Write
*
*  Description:
*    Writes a specified number of bytes from data buffer to device.
*
*  Parameters:
*    Unit             - Device Index
*    pData            - Pointer to a data buffer
*    NumBytes         - Number of bytes
*
*/
void FS_DF_HW_X_Write(U8 Unit, const U8 * pData, int NumBytes) {
  int i;
  U8 mask;
  U8 data;
  for (i = 0; i < NumBytes; i++) {
    data = pData[i];
    mask = 0x80;
    while (mask) {
      if (data & mask) {
        SPI_SET_DATAOUT();
      } else {
        SPI_CLR_DATAOUT();
      }
      SPI_CLR_CLK();
      SPI_DELAY();
      SPI_SET_CLK();
      SPI_DELAY();
      mask >>= 1;
    }
  }
  SPI_SET_DATAOUT(); // Default state of data line is high
}

/*************************** End of file ****************************/
