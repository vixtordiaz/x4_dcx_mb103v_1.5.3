/*********************************************************************
*                SEGGER MICROCONTROLLER GmbH & Co. KG                *
*        Solutions for real time microcontroller applications        *
**********************************************************************
*                                                                    *
*        (c) 2003-2012     SEGGER Microcontroller GmbH & Co KG       *
*                                                                    *
*        Internet: www.segger.com    Support:  support@segger.com    *
*                                                                    *
**********************************************************************

**** emFile file system for embedded applications ****
emFile is protected by international copyright laws. Knowledge of the
source code may not be used to write a similar product. This file may
only be used in accordance with a license and should not be re-
distributed in any way. We appreciate your understanding and fairness.
----------------------------------------------------------------------
----------------------------------------------------------------------
File        : NAND_HW.c
Purpose     : NAND flash hardware layer for Atmel AT91SAM7SExxx
----------------------------------------------------------------------
Known problems or limitations with current version
----------------------------------------------------------------------
None.
---------------------------END-OF-HEADER------------------------------
*/

/*********************************************************************
*
*       #include Section
*
**********************************************************************
*/
#include "NAND_X_HW.h"
#include "FS_Int.h"       // For FS_MEMCPY
#include <string.h>

/*********************************************************************
*
*       #define Macros
*
**********************************************************************
*/
#ifdef __ICCARM__
  #define OPTIMIZE        __ramfunc
#else
  #define OPTIMIZE
#endif


/*********************************************************************
*
*       #define Macros
*
**********************************************************************
*/
#define NAND_BASE_ADDR          0x40000000
#define NAND_DATA               (U8 *)(NAND_BASE_ADDR + 0x000000)
#define NAND_ADDR               (U8 *)(NAND_BASE_ADDR + 0x200000)
#define NAND_CMD                (U8 *)(NAND_BASE_ADDR + 0x400000)

/*********************************************************************
*
*       #define sfrs
*
**********************************************************************
*/

#define EBI_CSA         (*(volatile U32 *)(0xFFFFFF80))

#define SMC_BASE        0xFFFFFF90
#define SMC_CSR0        (*(volatile U32 *)(0xFFFFFF90 + 0x00))
#define SMC_CSR1        (*(volatile U32 *)(0xFFFFFF90 + 0x04))
#define SMC_CSR2        (*(volatile U32 *)(0xFFFFFF90 + 0x08))
#define SMC_CSR3        (*(volatile U32 *)(0xFFFFFF90 + 0x0C))
#define SMC_CSR4        (*(volatile U32 *)(0xFFFFFF90 + 0x10))
#define SMC_CSR5        (*(volatile U32 *)(0xFFFFFF90 + 0x14))
#define SMC_CSR6        (*(volatile U32 *)(0xFFFFFF90 + 0x18))
#define SMC_CSR7        (*(volatile U32 *)(0xFFFFFF90 + 0x1C))


#define PMC_BASE        0xFFFFFC00
#define PMC_SCER        (*(volatile U32 *)(PMC_BASE + 0x00)) // (PMC) System Clock Enable Register
#define PMC_SCDR        (*(volatile U32 *)(PMC_BASE + 0x04)) // (PMC) System Clock Disable Register
#define PMC_SCSR        (*(volatile U32 *)(PMC_BASE + 0x08)) // (PMC) System Clock Status Register
#define PMC_PCER        (*(volatile U32 *)(PMC_BASE + 0x10)) // (PMC) Peripheral Clock Enable Register
#define PMC_PCDR        (*(volatile U32 *)(PMC_BASE + 0x14)) // (PMC) Peripheral Clock Disable Register
#define PMC_PCSR        (*(volatile U32 *)(PMC_BASE + 0x18)) // (PMC) Peripheral Clock Status Register
#define PMC_MOR         (*(volatile U32 *)(PMC_BASE + 0x20)) // (PMC) Main Oscillator Register
#define PMC_MCFR        (*(volatile U32 *)(PMC_BASE + 0x24)) // (PMC) Main Clock  Frequency Register
#define PMC_PLLR        (*(volatile U32 *)(PMC_BASE + 0x2C)) // (PMC) PLL Register
#define PMC_MCKR        (*(volatile U32 *)(PMC_BASE + 0x30)) // (PMC) Master Clock Register
#define PMC_PCKR        (*(volatile U32 *)(PMC_BASE + 0x40)) // (PMC) Programmable Clock Register
#define PMC_IER         (*(volatile U32 *)(PMC_BASE + 0x60)) // (PMC) Interrupt Enable Register
#define PMC_IDR         (*(volatile U32 *)(PMC_BASE + 0x64)) // (PMC) Interrupt Disable Register
#define PMC_SR          (*(volatile U32 *)(PMC_BASE + 0x68)) // (PMC) Status Register
#define PMC_IMR         (*(volatile U32 *)(PMC_BASE + 0x6C)) // (PMC) Interrupt Mask Register

// ========== Register definition for PIOA peripheral ==========
#define PIOA_BASE       0xFFFFF400


#define PIOA_PER        (*(volatile U32 *)(PIOA_BASE + 0x00)) // (PIOA) PIO Enable Register
#define PIOA_PDR        (*(volatile U32 *)(PIOA_BASE + 0x04)) // (PIOA) PIO Disable Register
#define PIOA_PSR        (*(volatile U32 *)(PIOA_BASE + 0x08)) // (PIOA) PIO Status Register
#define PIOA_OER        (*(volatile U32 *)(PIOA_BASE + 0x10)) // (PIOA) Output Enable Register
#define PIOA_ODR        (*(volatile U32 *)(PIOA_BASE + 0x14)) // (PIOA) Output Disable Registerr
#define PIOA_OSR        (*(volatile U32 *)(PIOA_BASE + 0x18)) // (PIOA) Output Status Register
#define PIOA_IFER       (*(volatile U32 *)(PIOA_BASE + 0x20)) // (PIOA) Input Filter Enable Register
#define PIOA_IFDR       (*(volatile U32 *)(PIOA_BASE + 0x24)) // (PIOA) Input Filter Disable Register
#define PIOA_IFSR       (*(volatile U32 *)(PIOA_BASE + 0x28)) // (PIOA) Input Filter Status Register
#define PIOA_SODR       (*(volatile U32 *)(PIOA_BASE + 0x30)) // (PIOA) Set Output Data Register
#define PIOA_CODR       (*(volatile U32 *)(PIOA_BASE + 0x34)) // (PIOA) Clear Output Data Register
#define PIOA_ODSR       (*(volatile U32 *)(PIOA_BASE + 0x38)) // (PIOA) Output Data Status Register
#define PIOA_PDSR       (*(volatile U32 *)(PIOA_BASE + 0x3C)) // (PIOA) Pin Data Status Register
#define PIOA_IER        (*(volatile U32 *)(PIOA_BASE + 0x40)) // (PIOA) Interrupt Enable Register
#define PIOA_IDR        (*(volatile U32 *)(PIOA_BASE + 0x44)) // (PIOA) Interrupt Disable Register
#define PIOA_IMR        (*(volatile U32 *)(PIOA_BASE + 0x48)) // (PIOA) Interrupt Mask Register
#define PIOA_ISR        (*(volatile U32 *)(PIOA_BASE + 0x4C)) // (PIOA) Interrupt Status Register
#define PIOA_MDER       (*(volatile U32 *)(PIOA_BASE + 0x50)) // (PIOA) Multi-driver Enable Register
#define PIOA_MDDR       (*(volatile U32 *)(PIOA_BASE + 0x54)) // (PIOA) Multi-driver Disable Register
#define PIOA_MDSR       (*(volatile U32 *)(PIOA_BASE + 0x58)) // (PIOA) Multi-driver Status Register
#define PIOA_PPUDR      (*(volatile U32 *)(PIOA_BASE + 0x60)) // (PIOA) Pull-up Disable Register
#define PIOA_PPUER      (*(volatile U32 *)(PIOA_BASE + 0x64)) // (PIOA) Pull-up Enable Register
#define PIOA_PPUSR      (*(volatile U32 *)(PIOA_BASE + 0x68)) // (PIOA) Pull-up Status Register
#define PIOA_ASR        (*(volatile U32 *)(PIOA_BASE + 0x70)) // (PIOA) Select A Register
#define PIOA_BSR        (*(volatile U32 *)(PIOA_BASE + 0x74)) // (PIOA) Select B Register
#define PIOA_ABSR       (*(volatile U32 *)(PIOA_BASE + 0x78)) // (PIOA) AB Select Status Register
#define PIOA_OWER       (*(volatile U32 *)(PIOA_BASE + 0xA0)) // (PIOA) Output Write Enable Register
#define PIOA_OWDR       (*(volatile U32 *)(PIOA_BASE + 0xA4)) // (PIOA) Output Write Disable Register
#define PIOA_OWSR       (*(volatile U32 *)(PIOA_BASE + 0xA8)) // (PIOA) Output Write Status Register

// ========== Register definition for PIOB peripheral ==========
#define PIOB_BASE       0xFFFFF600

#define PIOB_PER        (*(volatile U32 *)(PIOB_BASE + 0x00)) // (PIOB) PIO Enable Register
#define PIOB_PDR        (*(volatile U32 *)(PIOB_BASE + 0x04)) // (PIOB) PIO Disable Register
#define PIOB_PSR        (*(volatile U32 *)(PIOB_BASE + 0x08)) // (PIOB) PIO Status Register
#define PIOB_OER        (*(volatile U32 *)(PIOB_BASE + 0x10)) // (PIOB) Output Enable Register
#define PIOB_ODR        (*(volatile U32 *)(PIOB_BASE + 0x14)) // (PIOB) Output Disable Registerr
#define PIOB_OSR        (*(volatile U32 *)(PIOB_BASE + 0x18)) // (PIOB) Output Status Register
#define PIOB_IFER       (*(volatile U32 *)(PIOB_BASE + 0x20)) // (PIOB) Input Filter Enable Register
#define PIOB_IFDR       (*(volatile U32 *)(PIOB_BASE + 0x24)) // (PIOB) Input Filter Disable Register
#define PIOB_IFSR       (*(volatile U32 *)(PIOB_BASE + 0x28)) // (PIOB) Input Filter Status Register
#define PIOB_SODR       (*(volatile U32 *)(PIOB_BASE + 0x30)) // (PIOB) Set Output Data Register
#define PIOB_CODR       (*(volatile U32 *)(PIOB_BASE + 0x34)) // (PIOB) Clear Output Data Register
#define PIOB_ODSR       (*(volatile U32 *)(PIOB_BASE + 0x38)) // (PIOB) Output Data Status Register
#define PIOB_PDSR       (*(volatile U32 *)(PIOB_BASE + 0x3C)) // (PIOB) Pin Data Status Register
#define PIOB_IER        (*(volatile U32 *)(PIOB_BASE + 0x40)) // (PIOB) Interrupt Enable Register
#define PIOB_IDR        (*(volatile U32 *)(PIOB_BASE + 0x44)) // (PIOB) Interrupt Disable Register
#define PIOB_IMR        (*(volatile U32 *)(PIOB_BASE + 0x48)) // (PIOB) Interrupt Mask Register
#define PIOB_ISR        (*(volatile U32 *)(PIOB_BASE + 0x4C)) // (PIOB) Interrupt Status Register
#define PIOB_MDER       (*(volatile U32 *)(PIOB_BASE + 0x50)) // (PIOB) Multi-driver Enable Register
#define PIOB_MDDR       (*(volatile U32 *)(PIOB_BASE + 0x54)) // (PIOB) Multi-driver Disable Register
#define PIOB_MDSR       (*(volatile U32 *)(PIOB_BASE + 0x58)) // (PIOB) Multi-driver Status Register
#define PIOB_PPUDR      (*(volatile U32 *)(PIOB_BASE + 0x60)) // (PIOB) Pull-up Disable Register
#define PIOB_PPUER      (*(volatile U32 *)(PIOB_BASE + 0x64)) // (PIOB) Pull-up Enable Register
#define PIOB_PPUSR      (*(volatile U32 *)(PIOB_BASE + 0x68)) // (PIOB) Pull-up Status Register
#define PIOB_ASR        (*(volatile U32 *)(PIOB_BASE + 0x70)) // (PIOB) Select A Register
#define PIOB_BSR        (*(volatile U32 *)(PIOB_BASE + 0x74)) // (PIOB) Select B Register
#define PIOB_ABSR       (*(volatile U32 *)(PIOB_BASE + 0x78)) // (PIOB) AB Select Status Register
#define PIOB_OWER       (*(volatile U32 *)(PIOB_BASE + 0xA0)) // (PIOB) Output Write Enable Register
#define PIOB_OWDR       (*(volatile U32 *)(PIOB_BASE + 0xA4)) // (PIOB) Output Write Disable Register
#define PIOB_OWSR       (*(volatile U32 *)(PIOB_BASE + 0xA8)) // (PIOB) Output Write Status Register

// ========== Register definition for PIOC peripheral ==========
#define PIOC_BASE       0xFFFFF800
#define PIOC_PER        (*(volatile U32 *)(PIOC_BASE + 0x00)) // (PIOC) PIO Enable Register
#define PIOC_PDR        (*(volatile U32 *)(PIOC_BASE + 0x04)) // (PIOC) PIO Disable Register
#define PIOC_PSR        (*(volatile U32 *)(PIOC_BASE + 0x08)) // (PIOC) PIO Status Register
#define PIOC_OER        (*(volatile U32 *)(PIOC_BASE + 0x10)) // (PIOC) Output Enable Register
#define PIOC_ODR        (*(volatile U32 *)(PIOC_BASE + 0x14)) // (PIOC) Output Disable Registerr
#define PIOC_OSR        (*(volatile U32 *)(PIOC_BASE + 0x18)) // (PIOC) Output Status Register
#define PIOC_IFER       (*(volatile U32 *)(PIOC_BASE + 0x20)) // (PIOC) Input Filter Enable Register
#define PIOC_IFDR       (*(volatile U32 *)(PIOC_BASE + 0x24)) // (PIOC) Input Filter Disable Register
#define PIOC_IFSR       (*(volatile U32 *)(PIOC_BASE + 0x28)) // (PIOC) Input Filter Status Register
#define PIOC_SODR       (*(volatile U32 *)(PIOC_BASE + 0x30)) // (PIOC) Set Output Data Register
#define PIOC_CODR       (*(volatile U32 *)(PIOC_BASE + 0x34)) // (PIOC) Clear Output Data Register
#define PIOC_ODSR       (*(volatile U32 *)(PIOC_BASE + 0x38)) // (PIOC) Output Data Status Register
#define PIOC_PDSR       (*(volatile U32 *)(PIOC_BASE + 0x3C)) // (PIOC) Pin Data Status Register
#define PIOC_IER        (*(volatile U32 *)(PIOC_BASE + 0x40)) // (PIOC) Interrupt Enable Register
#define PIOC_IDR        (*(volatile U32 *)(PIOC_BASE + 0x44)) // (PIOC) Interrupt Disable Register
#define PIOC_IMR        (*(volatile U32 *)(PIOC_BASE + 0x48)) // (PIOC) Interrupt Mask Register
#define PIOC_ISR        (*(volatile U32 *)(PIOC_BASE + 0x4C)) // (PIOC) Interrupt Status Register
#define PIOC_MDER       (*(volatile U32 *)(PIOC_BASE + 0x50)) // (PIOC) Multi-driver Enable Register
#define PIOC_MDDR       (*(volatile U32 *)(PIOC_BASE + 0x54)) // (PIOC) Multi-driver Disable Register
#define PIOC_MDSR       (*(volatile U32 *)(PIOC_BASE + 0x58)) // (PIOC) Multi-driver Status Register
#define PIOC_PPUDR      (*(volatile U32 *)(PIOC_BASE + 0x60)) // (PIOC) Pull-up Disable Register
#define PIOC_PPUER      (*(volatile U32 *)(PIOC_BASE + 0x64)) // (PIOC) Pull-up Enable Register
#define PIOC_PPUSR      (*(volatile U32 *)(PIOC_BASE + 0x68)) // (PIOC) Pull-up Status Register
#define PIOC_ASR        (*(volatile U32 *)(PIOC_BASE + 0x70)) // (PIOC) Select A Register
#define PIOC_BSR        (*(volatile U32 *)(PIOC_BASE + 0x74)) // (PIOC) Select B Register
#define PIOC_ABSR       (*(volatile U32 *)(PIOC_BASE + 0x78)) // (PIOC) AB Select Status Register
#define PIOC_OWER       (*(volatile U32 *)(PIOC_BASE + 0xA0)) // (PIOC) Output Write Enable Register
#define PIOC_OWDR       (*(volatile U32 *)(PIOC_BASE + 0xA4)) // (PIOC) Output Write Disable Register
#define PIOC_OWSR       (*(volatile U32 *)(PIOC_BASE + 0xA8)) // (PIOC) Output Write Status Register


#define PERIPHAL_ID_FIQ         (0)  // Advanced Interrupt Controller (FIQ)
#define PERIPHAL_ID_SYS         (1)  // System Peripheral
#define PERIPHAL_ID_PIOA        (2)  // Parallel IO Controller A
#define PERIPHAL_ID_PIOB        (3)  // Parallel IO Controller B
#define PERIPHAL_ID_PIOC        (4)  // Parallel IO Controller C
#define PERIPHAL_ID_SPI         (5)  // Serial Peripheral Interface 0
#define PERIPHAL_ID_US0         (6)  // USART 0
#define PERIPHAL_ID_US1         (7)  // USART 1
#define PERIPHAL_ID_SSC         (8)  // Serial Synchronous Controller
#define PERIPHAL_ID_TWI         (9)  // Two-Wire Interface
#define PERIPHAL_ID_PWMC        (10) // PWM Controller
#define PERIPHAL_ID_UDP         (11) // USB Device Port
#define PERIPHAL_ID_TC0         (12) // Timer Counter 0
#define PERIPHAL_ID_TC1         (13) // Timer Counter 1
#define PERIPHAL_ID_TC2         (14) // Timer Counter 2
#define PERIPHAL_ID_ADC         (15) // Analog-to-Digital Converter
#define PERIPHAL_ID_IRQ0        (29) // Advanced Interrupt Controller (IRQ0)
#define PERIPHAL_ID_IRQ1        (30) // Advanced Interrupt Controller (IRQ1)


/*********************************************************************
*
*       Static data
*
**********************************************************************
*/
static U8 * _pCurrentNANDAddr;
/*********************************************************************
*
*       Static code
*
**********************************************************************
*/


/*********************************************************************
*
*       Public code
*
**********************************************************************
*/


/*********************************************************************
*
*       FS_NAND_HW_X_EnableCE
*/
OPTIMIZE void FS_NAND_HW_X_EnableCE(U8 Unit) {
  PIOB_CODR = (1 << 18); // Enable NAND CE
}

/*********************************************************************
*
*       FS_NAND_HW_X_DisableCE
*/
OPTIMIZE void FS_NAND_HW_X_DisableCE(U8 Unit) {
  PIOB_SODR = (1 << 18); // Disable NAND CE
}


/*********************************************************************
*
*       FS_NAND_HW_X_SetData
*/
OPTIMIZE void FS_NAND_HW_X_SetDataMode(U8 Unit) {
  FS_USE_PARA(Unit);
  // CLE low, ALE low
  _pCurrentNANDAddr = NAND_DATA;
}


/*********************************************************************
*
*       FS_NAND_HW_X_SetCmd
*/
OPTIMIZE void FS_NAND_HW_X_SetCmdMode(U8 Unit) {
  FS_USE_PARA(Unit);
  //CLE high, ALE low
  _pCurrentNANDAddr = NAND_CMD;
}

/*********************************************************************
*
*       FS_NAND_HW_X_SetAddr
*/
OPTIMIZE void FS_NAND_HW_X_SetAddrMode(U8 Unit) {
  FS_USE_PARA(Unit);
  // CLE low, ALE high
  _pCurrentNANDAddr = NAND_ADDR;
}

/*********************************************************************
*
*       FS_NAND_HW_X_Read_x8
*/
void FS_NAND_HW_X_Read_x8(U8 Unit, void * pData, unsigned NumBytes) {
  FS_MEMCPY(pData, _pCurrentNANDAddr, NumBytes);
}


/*********************************************************************
*
*       FS_NAND_HW_X_Write_x8
*/
void FS_NAND_HW_X_Write_x8(U8 Unit, const void * pData, unsigned NumBytes) {
  FS_MEMCPY(_pCurrentNANDAddr, pData, NumBytes);
}

/*********************************************************************
*
*       FS_NAND_HW_X_Init_x8
*/
void FS_NAND_HW_X_Init_x8(U8 Unit) {
  //
  // Enable clocks for PIOA, PIOB, PIOC
  //
  PMC_PCER = (1 << PERIPHAL_ID_PIOA)
           | (1 << PERIPHAL_ID_PIOB)
           | (1 << PERIPHAL_ID_PIOC)
           ;

  //
  //  Set PIOB pin 18 as port pin (output), for use as NAND CS.
  //
  PIOB_PER  = 1 << 18;    // Set pin as port pin
  PIOB_OER  = 1 << 18;    // Configure as output
  PIOB_SODR = 1 << 18;    // set pin high

  //
  //  Set PIOB pin 19 as port pin, for use as NAND Ready/Busy line.
  //
  PIOB_PER  = 1 << 19;    // Set pin as port pin
  PIOB_ODR  = 1 << 19;    // Configure in intput

  //
  //  Set different port pins to alternate function
  //
  //  Note (quote from chip errata):
  //    The PIO PC19 is erroneously used twice. USB_CNX (VBUS detect) and A21/ALE
  //    (NAND Flash Address Latch Enable) uses this PIO. There is no effect when PC19
  //    is configured as A21 for the NAND Flash usage, but USB_CNX state (VBUS) cannot
  //    be read at the same time. The user has to swap PC19 to input mode to detect
  //    the VBUS state, but the NANDFlash cannot be accessed in this configuration.
  //

  PIOC_ASR  = (1 <<  0)            // PIOC.pin0  -> D0
           |  (1 <<  1)            // PIOC.pin1  -> D1
           |  (1 <<  2)            // PIOC.pin2  -> D2
           |  (1 <<  3)            // PIOC.pin3  -> D3
           |  (1 <<  4)            // PIOC.pin4  -> D4
           |  (1 <<  5)            // PIOC.pin5  -> D5
           |  (1 <<  6)            // PIOC.pin6  -> D6
           |  (1 <<  7)            // PIOC.pin7  -> D7
           |  (1 << 19)            // PIOC.pin19 -> A21
           |  (1 << 20)            // PIOC.pin20 -> A22
           ;
  PIOC_BSR  = (1 << 17)            // PIOC.pin17 -> NANDOE
           |  (1 << 18)            // PIOC.pin18 -> NANDWE
           ;
  PIOC_PDR =  (1 <<  0)            // PIOC.pin0  -> D0
           |  (1 <<  1)            // PIOC.pin1  -> D1
           |  (1 <<  2)            // PIOC.pin2  -> D2
           |  (1 <<  3)            // PIOC.pin3  -> D3
           |  (1 <<  4)            // PIOC.pin4  -> D4
           |  (1 <<  5)            // PIOC.pin5  -> D5
           |  (1 <<  6)            // PIOC.pin6  -> D6
           |  (1 <<  7)            // PIOC.pin7  -> D7
           |  (1 << 19)            // PIOC.pin19 -> A21
           |  (1 << 20)            // PIOC.pin20 -> A22
           |  (1 << 17)            // PIOC.pin17 -> NANDOE
           |  (1 << 18)            // PIOC.pin18 -> NANDWE
           ;
  //
  // Update external bus interface, static memory controller
  //
  EBI_CSA  |= (1 <<  3);   // Assign CS3 for use with NAND flash
  SMC_CSR3  = (1 <<  7);   // Enable Wait states
  SMC_CSR3 |= (2 <<  0)    // 2n Wait states
           |  (2 <<  8)    // Use 2 Data float time cycles for NAND flash
           |  (2 << 13)    // 8-bit data bus width
           |  (0 << 24)    // Set Read and Write Signal Setup Time to n + 1/2 cycles.
           |  (1 << 28)    // Set Read and Write Signal Hold Time  to n cycles.
          ;
}

/*********************************************************************
*
*       FS_NAND_HW_X_WaitWhileBusy
*/

OPTIMIZE int FS_NAND_HW_X_WaitWhileBusy(U8 Unit, unsigned us) {
  while ((PIOB_PDSR & (1 << 19)) == 0);
  return 0;
}

/*********************************************************************
*
*       Dummy function for 16-bit access
*
**********************************************************************
*/

/*********************************************************************
*
*       FS_NAND_HW_X_Read_x16
*/
void FS_NAND_HW_X_Read_x16(U8 Unit, void * pData, unsigned NumBytes) {
}

/*********************************************************************
*
*       FS_NAND_HW_X_Write_x16
*/
void FS_NAND_HW_X_Write_x16(U8 Unit, const void * pData, unsigned NumBytes) {
}

/*********************************************************************
*
*       FS_NAND_HW_X_Init_x16
*/
void FS_NAND_HW_X_Init_x16(U8 Unit) {
}


/**************************** end of file ***************************/

