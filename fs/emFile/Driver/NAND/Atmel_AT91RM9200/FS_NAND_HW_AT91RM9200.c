/*********************************************************************
*                SEGGER MICROCONTROLLER GmbH & Co. KG                *
*        Solutions for real time microcontroller applications        *
**********************************************************************
*                                                                    *
*        (c) 2003-2012     SEGGER Microcontroller GmbH & Co KG       *
*                                                                    *
*        Internet: www.segger.com    Support:  support@segger.com    *
*                                                                    *
**********************************************************************

**** emFile file system for embedded applications ****
emFile is protected by international copyright laws. Knowledge of the
source code may not be used to write a similar product. This file may
only be used in accordance with a license and should not be re-
distributed in any way. We appreciate your understanding and fairness.
----------------------------------------------------------------------
----------------------------------------------------------------------
File        : FS_NAND_HW_AT91RM9200.c
Purpose     : NAND flash hardware layer for Atmel AT91RM9200
----------------------------------------------------------------------
Known problems or limitations with current version
----------------------------------------------------------------------
None.
---------------------------END-OF-HEADER------------------------------
*/

/*********************************************************************
*
*       #include Section
*
**********************************************************************
*/
#include "NAND_X_HW.h"
#include "FS_Int.h"       // For FS_MEMCPY
#include <string.h>
/*********************************************************************
*
*       #include Section
*
**********************************************************************
*/

/*********************************************************************
*
*       Defines configurable
*
**********************************************************************
*/
#define NAND_BASE_ADDR          0x40000000
#define ALE_ADDR_LINE           22
#define CLE_ADDR_LINE           21

#define CE_PIO_ID               PERIPHAL_ID_PIOC
#define CE_PIO_BASE             PIOC_BASE
#define CE_PIN                  28

#define BUSY_PIO_ID             PERIPHAL_ID_PIOC
#define BUSY_PIO_BASE           PIOC_BASE
#define BUSY_PIN                29

#define CONFIGURE_OE_WE          1

#if CONFIGURE_OE_WE
  #define OE_PIN                1
  #define OE_PIO_BASE           PIOC_BASE
  #define OE_PIO_PERIPHAL_OFF   PIO_OFF_ASR
  #define OE_PIO_ID             PERIPHAL_ID_PIOC


  #define WE_PIN                3
  #define WE_PIO_BASE           PIOC_BASE
  #define WE_PIO_PERIPHAL_OFF   PIO_OFF_ASR
  #define WE_PIO_ID             PERIPHAL_ID_PIOC
#endif

/*********************************************************************
*
*       #define sfrs
*
**********************************************************************
*/

#define MC_BASE_ADDR      0xFFFFFF60
#define SMC_CSR3         (*(volatile U32*)(MC_BASE_ADDR + 0x10 + 3 * 0x04))  // SMC CS3 Mode Register
#define MC_EBICSA        (*(volatile U32*)(0xFFFFFF60))                      // EBI Chip Select Assignment register

#define PMC_BASE_ADDR    0xFFFFFC00
#define PMC_PCER         (*(volatile U32 *)(PMC_BASE_ADDR + 0x10)) // (PMC) Peripheral Clock Enable Register
#define PMC_PCDR         (*(volatile U32 *)(PMC_BASE_ADDR + 0x14)) // (PMC) Peripheral Clock Disable Register

// ========== Register definition for PIOx peripheral ==========
#define PIOA_BASE        0xFFFFF400
#define PIOB_BASE        0xFFFFF600
#define PIOC_BASE        0xFFFFF800
#define PIOD_BASE        0xFFFFFA00

#define PIO_OFF_PER         (0x00) // PIO Enable Register
#define PIO_OFF_PDR         (0x04) // PIO Disable Register
#define PIO_OFF_PSR         (0x08) // PIO Status Register
#define PIO_OFF_OER         (0x10) // Output Enable Register
#define PIO_OFF_ODR         (0x14) // Output Disable Register
#define PIO_OFF_OSR         (0x18) // Output Status Register
#define PIO_OFF_IFER        (0x20) // Input Filter Enable Register
#define PIO_OFF_IFDR        (0x24) // Input Filter Disable Register
#define PIO_OFF_IFSR        (0x28) // Input Filter Status Register
#define PIO_OFF_SODR        (0x30) // Set Output Data Register
#define PIO_OFF_CODR        (0x34) // Clear Output Data Register
#define PIO_OFF_ODSR        (0x38) // Output Data Status Register
#define PIO_OFF_PDSR        (0x3C) // Pin Data Status Register
#define PIO_OFF_IER         (0x40) // Interrupt Enable Register
#define PIO_OFF_IDR         (0x44) // Interrupt Disable Register
#define PIO_OFF_IMR         (0x48) // Interrupt Mask Register
#define PIO_OFF_ISR         (0x4C) // Interrupt Status Register
#define PIO_OFF_MDER        (0x50) // Multi-driver Enable Register
#define PIO_OFF_MDDR        (0x54) // Multi-driver Disable Register
#define PIO_OFF_MDSR        (0x58) // Multi-driver Status Register
#define PIO_OFF_PPUDR       (0x60) // Pull-up Disable Register
#define PIO_OFF_PPUER       (0x64) // Pull-up Enable Register
#define PIO_OFF_PPUSR       (0x68) // Pull-up Status Register
#define PIO_OFF_ASR         (0x70) // Select A Register
#define PIO_OFF_BSR         (0x74) // Select B Register
#define PIO_OFF_ABSR        (0x78) // AB Select Status Register
#define PIO_OFF_OWER        (0xA0) // Output Write Enable Register
#define PIO_OFF_OWDR        (0xA4) // Output Write Disable Register
#define PIO_OFF_OWSR        (0xA8) // Output Write Status Register

#define PERIPHAL_ID_PIOA        (2)  // Parallel IO Controller A
#define PERIPHAL_ID_PIOB        (3)  // Parallel IO Controller B
#define PERIPHAL_ID_PIOC        (4)  // Parallel IO Controller C
#define PERIPHAL_ID_PIOD        (5)  // Parallel IO Controller D


#define NAND_DATA               (U8 *)(NAND_BASE_ADDR + 0x000000)
#define NAND_ADDR               (U8 *)(NAND_BASE_ADDR + (1UL << ALE_ADDR_LINE))
#define NAND_CMD                (U8 *)(NAND_BASE_ADDR + (1UL << CLE_ADDR_LINE))

/*********************************************************************
*
*       Static data
*
**********************************************************************
*/
static U8 * _pCurrentNANDAddr;

/*********************************************************************
*
*       Static code
*
**********************************************************************
*/

/*********************************************************************
*
*       _WritePIO
*/
static void _WritePIO(U32 PIOOff, U32 Pin) {
  volatile U32 * p;

  p = (volatile U32 *)(PIOOff);
  *p = (1UL << Pin);
}


/*********************************************************************
*
*       _ReadPIO
*/
static U32 _ReadPIO(U32 PIOOff, U32 Pin) {
  volatile U32 * p;
  U32            r;

  p = (volatile U32 *)(PIOOff);
  r = *p & (1UL << Pin);
  return r;
}

/*********************************************************************
*
*       _CopyData8
*/
static void _CopyData8(void * pDest, const void * pSrc, unsigned NumBytes) {
  char * pd;
  const char * ps;
  pd = (char*)pDest;
  ps = (const char*)pSrc;
  //
  // Copy bytes, one at a time
  //
  if (NumBytes) {
    do {
      *(char*)pd++ = *(const char*)ps++;
    } while (--NumBytes);
  }
}

/*********************************************************************
*
*       _CopyData16
*/
static void _CopyData16(void * pDest, const void * pSrc, unsigned NumBytes) {
  char * pd;
  const char * ps;
  pd = (char*)pDest;
  ps = (const char*)pSrc;
  
  if ((((U32)ps & 1) == 0) && (((U32)pd & 1) == 0)) {
    unsigned NumHWords = NumBytes >> 1;
    if (NumHWords) {
      do {
        *(short *)pd = *(const short *)ps;
        pd += 2;
        ps += 2;
      } while (--NumHWords);
    }
    NumBytes &= 1;
  }
  //
  // Copy bytes, one at a time
  //
  if (NumBytes) {
    do {
      *(char*)pd++ = *(const char*)ps++;
    } while (--NumBytes);
  }
}

/*********************************************************************
*
*       Public code
*
**********************************************************************
*/


/*********************************************************************
*
*       FS_NAND_HW_X_EnableCE
*/
void FS_NAND_HW_X_EnableCE(U8 Unit) {

  _WritePIO(CE_PIO_BASE + PIO_OFF_CODR, CE_PIN); // Enable NAND CE
}

/*********************************************************************
*
*       FS_NAND_HW_X_DisableCE
*/
void FS_NAND_HW_X_DisableCE(U8 Unit) {
  _WritePIO(CE_PIO_BASE + PIO_OFF_SODR, CE_PIN); // Disable NAND CE
}


/*********************************************************************
*
*       FS_NAND_HW_X_SetData
*/
void FS_NAND_HW_X_SetDataMode(U8 Unit) {
  FS_USE_PARA(Unit);
  // CLE low, ALE low
  _pCurrentNANDAddr = NAND_DATA;
}


/*********************************************************************
*
*       FS_NAND_HW_X_SetCmd
*/
void FS_NAND_HW_X_SetCmdMode(U8 Unit) {
  FS_USE_PARA(Unit);
  //CLE high, ALE low
  _pCurrentNANDAddr = NAND_CMD;
}

/*********************************************************************
*
*       FS_NAND_HW_X_SetAddr
*/
void FS_NAND_HW_X_SetAddrMode(U8 Unit) {
  FS_USE_PARA(Unit);
  // CLE low, ALE high
  _pCurrentNANDAddr = NAND_ADDR;
}

/*********************************************************************
*
*       FS_NAND_HW_X_Read_x8
*/
void FS_NAND_HW_X_Read_x8(U8 Unit, void * pData, unsigned NumBytes) {
  _CopyData8(pData, _pCurrentNANDAddr, NumBytes);
}

/*********************************************************************
*
*       FS_NAND_HW_X_Write_x8
*/
void FS_NAND_HW_X_Write_x8(U8 Unit, const void * pData, unsigned NumBytes) {
  _CopyData8(_pCurrentNANDAddr, pData, NumBytes);
}

/*********************************************************************
*
*       FS_NAND_HW_X_Init_x8
*/
void FS_NAND_HW_X_Init_x8(U8 Unit) {
  //
  // Update external bus interface, static memory controller
  //
  MC_EBICSA |= (1 << 3); // Assign CS3 for use with NAND flash
  SMC_CSR3   = (0 << 28)     // Data hold time     set 1  cycle
             | (0 << 24)     // Data setup up time set 0 - 1/2 cycle
             | (0 << 16)     // Standard access is used
             | (2 << 13)     // DBW: 2: 8-bit NAND, 1: 16-bit NAND
             | (1 <<  8)     // 1 cycle as data float time
             | (1 <<  7)     // Enable wait states
             | (5 <<  0)     // 5 cycles as wait pulse length
             ;
  //
  // Enable clocks for specific PIOs
  //
  PMC_PCER = (1 << CE_PIO_ID)
           | (1 << BUSY_PIO_ID)
           ;
  //
  //  Set PIO pin x as port pin, for use as NAND Ready/Busy line.
  //
  _WritePIO(BUSY_PIO_BASE + PIO_OFF_ODR, BUSY_PIN);  // Configure in intput
  _WritePIO(BUSY_PIO_BASE + PIO_OFF_PER, BUSY_PIN);  // Set pin as port pin
  //
  //  Set PIOC pin x as port pin (output), for use as NAND CS.
  //
  _WritePIO(CE_PIO_BASE + PIO_OFF_PER,   CE_PIN);  // Set pin as port pin
  _WritePIO(CE_PIO_BASE + PIO_OFF_OER  , CE_PIN);  // Configure as output
  _WritePIO(CE_PIO_BASE + PIO_OFF_SODR , CE_PIN);  // set pin high
  //
  //  Set different port pins to alternate function, if necessary
  //
#if CONFIGURE_OE_WE
  PMC_PCER  = (OE_PIO_ID)
            | (WE_PIO_ID)
            ;
  _WritePIO(OE_PIO_BASE + OE_PIO_PERIPHAL_OFF, OE_PIN);
  _WritePIO(OE_PIO_BASE + PIO_OFF_PDR, OE_PIN);
  _WritePIO(WE_PIO_BASE + WE_PIO_PERIPHAL_OFF, WE_PIN);
  _WritePIO(WE_PIO_BASE + PIO_OFF_PDR, WE_PIN);
#endif
}

/*********************************************************************
*
*             FS_NAND_HW_X_WaitWhileBusy
*/
int FS_NAND_HW_X_WaitWhileBusy(U8 Unit, unsigned us) {
  while ((_ReadPIO(BUSY_PIO_BASE + PIO_OFF_PDSR, BUSY_PIN)) == 0);
  return 0;
}

/**************************** end of file ***************************/

