/*********************************************************************
*                SEGGER MICROCONTROLLER GmbH & Co. KG                *
*        Solutions for real time microcontroller applications        *
**********************************************************************
*                                                                    *
*        (c) 2003-2012     SEGGER Microcontroller GmbH & Co KG       *
*                                                                    *
*        Internet: www.segger.com    Support:  support@segger.com    *
*                                                                    *
**********************************************************************

**** emFile file system for embedded applications ****
emFile is protected by international copyright laws. Knowledge of the
source code may not be used to write a similar product. This file may
only be used in accordance with a license and should not be re-
distributed in any way. We appreciate your understanding and fairness.
----------------------------------------------------------------------
File        : FS_NAND_HW_STM32F103ZET6_ST_MB672.c
Purpose     : NAND flash hardware layer for the STM3210E-EVAL board (Rev. D).
Literature  : [1] \\fileserver\Techinfo\Company\ST\MCU\STM32\STM32_10xxx\STM32F10_RM.pdf
              [2] \\fileserver\Techinfo\Company\ST\MCU\STM32\STM32_10xxx\EvalBoard\MB672_STM32F103\STM3210E-EVAL_UM0488_Rev1.pdf
---------------------------END-OF-HEADER------------------------------
*/

#include <string.h>

#include "FS_Int.h"       // For FS_MEMCPY
#include "NAND_X_HW.h"
#include "stm32f10x.h"

/*********************************************************************
*
*       defines, configurable
*
**********************************************************************
*/
#define USE_OS        0   // Selects the operating mode: 1 event-driven, 0 polling

/*********************************************************************
*
*       Include files, compile-time dependant
*
**********************************************************************
*/
#if USE_OS
  #include "RTOS.h"
#endif

/*********************************************************************
*
*       defines, non-configurable
*
**********************************************************************
*/

/*********************************************************************
*
*       NAND flash address
*/
#define NAND_BASE_ADDR          0x70000000
#define NAND_DATA               (void *)(NAND_BASE_ADDR + 0x00000)
#define NAND_CMD                (void *)(NAND_BASE_ADDR + 0x10000)
#define NAND_ADDR               (void *)(NAND_BASE_ADDR + 0x20000)

/*********************************************************************
*
*       Flexible static memory controller
*/
#define FSMC_BASE_ADDR          0xA0000000uL
#define FSMC_PCR2               (*(volatile U32*)(FSMC_BASE_ADDR + 0x40 + 0x20 * (2 - 1)))
#define FSMC_PMEM2              (*(volatile U32*)(FSMC_BASE_ADDR + 0x48 + 0x20 * (2 - 1)))
#define FSMC_PATT2              (*(volatile U32*)(FSMC_BASE_ADDR + 0x4C + 0x20 * (2 - 1)))

/*********************************************************************
*
*       Reset and clock control
*/
#define RCC_BASE_ADDR           0x40021000uL
#define RCC_APB2ENR             (*(volatile U32*)(RCC_BASE_ADDR + 0x18))

/*********************************************************************
*
*       Port D registers
*/
#define GPIOD_BASE_ADDR         0x40011400uL
#define GPIOD_CRL               (*(volatile U32*)(GPIOD_BASE_ADDR + 0x00))
#define GPIOD_CRH               (*(volatile U32*)(GPIOD_BASE_ADDR + 0x04))
#define GPIOD_IDR               (*(volatile U32*)(GPIOD_BASE_ADDR + 0x08))

/*********************************************************************
*
*       Port E registers
*/
#define GPIOE_BASE_ADDR         0x40011800uL
#define GPIOE_CRL               (*(volatile U32*)(GPIOE_BASE_ADDR + 0x00))
#define GPIOE_CRH               (*(volatile U32*)(GPIOE_BASE_ADDR + 0x04))

/*********************************************************************
*
*       External interrupt controller
*/
#define EXTI_BASE_ADDR          0x40010400
#define EXTI_IMR                (*(volatile U32*)(EXTI_BASE_ADDR + 0x00))
#define EXTI_RTSR               (*(volatile U32*)(EXTI_BASE_ADDR + 0x08))
#define EXTI_FTSR               (*(volatile U32*)(EXTI_BASE_ADDR + 0x0C))
#define EXTI_PR                 (*(volatile U32*)(EXTI_BASE_ADDR + 0x14))

/*********************************************************************
*
*       Alternate function I/Os
*/
#define AFIO_BASE_ADDR          0x40010000
#define AFIO_EXTICR2            (*(volatile U32*)(AFIO_BASE_ADDR + 0x0C))

/*********************************************************************
*
*       GPIO bit positions of NAND flash signals
*/
#define NAND_CLE      11  // Port D
#define NAND_ALE      12  // Port D
#define NAND_D0       14  // Port D
#define NAND_D1       15  // Port D
#define NAND_D2       0   // Port D
#define NAND_D3       1   // Port D
#define NAND_D4       7   // Port E
#define NAND_D5       8   // Port E
#define NAND_D6       9   // Port E
#define NAND_D7       10  // Port E
#define NAND_NOE      4   // Port D
#define NAND_NWE      5   // Port D
#define NAND_NWAIT    6   // Port D
#define NAND_NCE2     7   // Port D

/*********************************************************************
*
*       Masks for the I/O port enable bits
*/
#define APB2ENR_AFIOEN    0
#define APB2ENR_IOPDEN    5
#define APB2ENR_IOPEEN    6

/*********************************************************************
*
*       AFIO external interrupt configuration
*/
#define EXTICR2_EXTI6     8
#define EXTICR2_MASK      0xFuL
#define EXTICR2_PD        0x3uL

/*********************************************************************
*
*       Defines for I/O port configuration
*/
#define GPIO_MODE_AF_PP   ((2uL << 2) | 3uL)
#define GPIO_MODE_IN_PU   ((2uL << 2) | 0uL)

#define EXT_INT_PRIO      15

/*********************************************************************
*
*       Static data
*
**********************************************************************
*/
static volatile void * _pCurrentNANDAddr;

/*********************************************************************
*
*       Static code
*
**********************************************************************
*/

#if USE_OS
/**********************************************************
*
*       EXTI9_5_IRQHandler
*
*   Function description
*     Handles the external interrupt generated by the NAND busy pin.
*/
void EXTI9_5_IRQHandler(void);
void EXTI9_5_IRQHandler(void) {
  OS_EnterInterrupt(); // Inform embOS that interrupt code is running
  //
  // Trigger the OS event when the busy signal goes from low to high.
  //
  if (EXTI_PR & (1uL << NAND_NWAIT)) {
    EXTI_PR |= 1uL << NAND_NWAIT;
    FS_X_OS_Signal();
  }
  OS_LeaveInterrupt(); // Inform embOS that interrupt code is left
}
#endif

/*********************************************************************
*
*       Public code
*
**********************************************************************
*/

/*********************************************************************
*
*       FS_NAND_HW_X_EnableCE
*/
void FS_NAND_HW_X_EnableCE(U8 Unit) {
  FS_USE_PARA(Unit);
  //
  // The CS signal is driven by FSMC.
  //
}

/*********************************************************************
*
*       FS_NAND_HW_X_DisableCE
*/
void FS_NAND_HW_X_DisableCE(U8 Unit) {
  FS_USE_PARA(Unit);
  //
  // The CS signal is driven by FSMC.
  //
}


/*********************************************************************
*
*       FS_NAND_HW_X_SetData
*/
void FS_NAND_HW_X_SetDataMode(U8 Unit) {
  FS_USE_PARA(Unit);
  //
  // CLE low, ALE low
  //
  _pCurrentNANDAddr = NAND_DATA;
}


/*********************************************************************
*
*       FS_NAND_HW_X_SetCmd
*/
void FS_NAND_HW_X_SetCmdMode(U8 Unit) {
  FS_USE_PARA(Unit);
  //
  // CLE high, ALE low
  //
  _pCurrentNANDAddr = NAND_CMD;
}

/*********************************************************************
*
*       FS_NAND_HW_X_SetAddr
*/
void FS_NAND_HW_X_SetAddrMode(U8 Unit) {
  FS_USE_PARA(Unit);
  //
  // CLE low, ALE high
  //
  _pCurrentNANDAddr = NAND_ADDR;
}

/*********************************************************************
*
*       FS_NAND_HW_X_Read_x8
*/
void FS_NAND_HW_X_Read_x8(U8 Unit, void * pData, unsigned NumBytes) {
  FS_USE_PARA(Unit);
  FS_MEMCPY(pData, (void *)_pCurrentNANDAddr, NumBytes);
}


/*********************************************************************
*
*       FS_NAND_HW_X_Write_x8
*/
void FS_NAND_HW_X_Write_x8(U8 Unit, const void * pData, unsigned NumBytes) {
  FS_USE_PARA(Unit);
  FS_MEMCPY((void *)_pCurrentNANDAddr, pData, NumBytes);
}

/*********************************************************************
*
*       FS_NAND_HW_X_Init_x8
*/
void FS_NAND_HW_X_Init_x8(U8 Unit) {
  U32 Timings;

  FS_USE_PARA(Unit);
  //
  // Enable the clocks for the GPIOs
  //
  RCC_APB2ENR |= (1uL << APB2ENR_IOPDEN)
              |  (1uL << APB2ENR_IOPEEN)
              ;
#if USE_OS
  RCC_APB2ENR |=  1uL << APB2ENR_AFIOEN;
#endif
  //
  // Configure the pins which drive the NAND flash signals.
  //
  GPIOD_CRH &= ~((0xFuL << ((NAND_CLE - 8) << 2)) |
                 (0xFuL << ((NAND_ALE - 8) << 2)) |
                 (0xFuL << ((NAND_D0 - 8)  << 2)) |
                 (0xFuL << ((NAND_D1 - 8)  << 2)));
  GPIOD_CRH |= (GPIO_MODE_AF_PP << ((NAND_CLE - 8) << 2))
            |  (GPIO_MODE_AF_PP << ((NAND_ALE - 8) << 2))
            |  (GPIO_MODE_AF_PP << ((NAND_D0 - 8)  << 2))
            |  (GPIO_MODE_AF_PP << ((NAND_D1 - 8)  << 2))
            ;
  GPIOD_CRL &= ~((0xFuL << (NAND_D2   << 2)) |
                 (0xFuL << (NAND_D3   << 2)) |
                 (0xFuL << (NAND_NOE  << 2)) |
                 (0xFuL << (NAND_NWE  << 2)) |
                 (0xFuL << (NAND_NCE2 << 2)));
  GPIOD_CRL |= (GPIO_MODE_AF_PP << (NAND_D2   << 2))
            |  (GPIO_MODE_AF_PP << (NAND_D3   << 2))
            |  (GPIO_MODE_AF_PP << (NAND_NOE  << 2))
            |  (GPIO_MODE_AF_PP << (NAND_NWE  << 2))
            |  (GPIO_MODE_AF_PP << (NAND_NCE2 << 2))
            ;
  GPIOE_CRH &= ~((0xFuL << ((NAND_D5 - 8) << 2)) |
                 (0xFuL << ((NAND_D6 - 8) << 2)) |
                 (0xFuL << ((NAND_D7 - 8) << 2)));
  GPIOE_CRH |= (GPIO_MODE_AF_PP << ((NAND_D5 - 8) << 2))
            |  (GPIO_MODE_AF_PP << ((NAND_D6 - 8) << 2))
            |  (GPIO_MODE_AF_PP << ((NAND_D7 - 8) << 2))
            ;
  GPIOE_CRL &= ~((0xFuL << (NAND_D4 << 2)));
  GPIOE_CRL |= (GPIO_MODE_AF_PP << (NAND_D4 << 2));
  //
  // The busy signal is an input with pull-up.
  //
  GPIOD_CRL &= ~((0xFuL << (NAND_NWAIT << 2)));
  GPIOD_CRL |= (GPIO_MODE_IN_PU << (NAND_NWAIT << 2));

#if USE_OS
  //
  // The busy signal is connected on port D pin 6.
  //

  AFIO_EXTICR2 &= ~(EXTICR2_MASK << EXTICR2_EXTI6);
  AFIO_EXTICR2 |=   EXTICR2_PD   << EXTICR2_EXTI6;
  //
  // The rising edge of the busy signal will generate an interrupt.
  //
  EXTI_RTSR |=   1uL << NAND_NWAIT;
  EXTI_FTSR &= ~(1uL << NAND_NWAIT);
  //
  // Unmask the external interrupt.
  //
  EXTI_IMR |= 1uL << NAND_NWAIT;
  //
  // Set the priority and enable the NVIC interrupt.
  //
  NVIC_SetPriority(EXTI9_5_IRQn, EXT_INT_PRIO);
  NVIC_EnableIRQ(EXTI9_5_IRQn);
#endif
  //
  // Configure the memory controller
  //
  Timings    = 0
             | (1uL << 24)  // HiZ time
             | (2uL << 16)  // HOLD time
             | (3uL << 8)   // WAIT time
             | (1uL << 0)   // SET time
             ;
  FSMC_PMEM2 = Timings;
  FSMC_PATT2 = Timings;
  FSMC_PCR2  = 0
             | (1uL << 3)   // Memory type: NAND flash
             | (1uL << 2)   // Enable memory bank
             ;
}

/*********************************************************************
*
*       FS_NAND_HW_X_Read_x16
*/
void FS_NAND_HW_X_Read_x16(U8 Unit, void * pData, unsigned NumBytes) {
  FS_USE_PARA(Unit);
  FS_MEMCPY(pData, (void *)_pCurrentNANDAddr, NumBytes);
}

/*********************************************************************
*
*       FS_NAND_HW_X_Write_x16
*/
void FS_NAND_HW_X_Write_x16(U8 Unit, const void * pData, unsigned NumBytes) {
  FS_USE_PARA(Unit);
  FS_MEMCPY((void *)_pCurrentNANDAddr, pData, NumBytes);
}

/*********************************************************************
*
*       FS_NAND_HW_X_Init_x16
*/
void FS_NAND_HW_X_Init_x16(U8 Unit) {
  FS_USE_PARA(Unit);
  FS_NAND_HW_X_Init_x8(Unit);
  FSMC_PCR2 |= (1uL << 4)   // 16-bit wide bus
            ;
}

/*********************************************************************
*
*             FS_NAND_HW_X_WaitWhileBusy
*/
int FS_NAND_HW_X_WaitWhileBusy(U8 Unit, unsigned us) {
  U32 Status;

  FS_USE_PARA(Unit);
  FS_USE_PARA(us);
  while (1) {
    Status = GPIOD_IDR;
    if (Status & (1uL << NAND_NWAIT)) {
      break;
    }
#if USE_OS
    FS_X_OS_Wait(1);
#endif
  }
  return 0;
}

/**************************** end of file ***************************/

