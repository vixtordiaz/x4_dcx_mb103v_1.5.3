/*********************************************************************
*                SEGGER MICROCONTROLLER GmbH & Co. KG                *
*        Solutions for real time microcontroller applications        *
**********************************************************************
*                                                                    *
*        (c) 2003-2012     SEGGER Microcontroller GmbH & Co KG       *
*                                                                    *
*        Internet: www.segger.com    Support:  support@segger.com    *
*                                                                    *
**********************************************************************

**** emFile file system for embedded applications ****
emFile is protected by international copyright laws. Knowledge of the
source code may not be used to write a similar product. This file may
only be used in accordance with a license and should not be re-
distributed in any way. We appreciate your understanding and fairness.
----------------------------------------------------------------------
----------------------------------------------------------------------
File        : NAND_HW.c
Purpose     : NAND flash hardware layer for Atmel AT91SAM9261
----------------------------------------------------------------------
Known problems or limitations with current version
----------------------------------------------------------------------
None.
---------------------------END-OF-HEADER------------------------------
*/

/*********************************************************************
*
*       #include Section
*
**********************************************************************
*/
#include "NAND_X_HW.h"
#include "FS_Int.h"       // For FS_MEMCPY
#include <string.h>
/*********************************************************************
*
*       #include Section
*
**********************************************************************
*/

/*********************************************************************
*
*       #define Macros
*
**********************************************************************
*/
#define NAND_BASE_ADDR          0x40000000
#define NAND_DATA               (U16 *)(NAND_BASE_ADDR + 0x000000)
#define NAND_ADDR               (U16 *)(NAND_BASE_ADDR + 0x400000)
#define NAND_CMD                (U16 *)(NAND_BASE_ADDR + 0x200000)

/*********************************************************************
*
*       #define sfrs
*
**********************************************************************
*/

#define SMC_BASE_ADDR    0xFFFFEC00
#define SMC_SETUP3       (*(volatile U32*)(SMC_BASE_ADDR + 3 * 0x10 + 0x00))  // SMC CS3 Setup Register
#define SMC_PULSE3       (*(volatile U32*)(SMC_BASE_ADDR + 3 * 0x10 + 0x04))  // SMC CS3 Pulse Register
#define SMC_CYCLE3       (*(volatile U32*)(SMC_BASE_ADDR + 3 * 0x10 + 0x08))  // SMC CS3 Cycle Register
#define SMC_CTRL3        (*(volatile U32*)(SMC_BASE_ADDR + 3 * 0x10 + 0x0C))  // SMC CS3 Mode Register
/*      MATRIX + EBI interface */
#define MATRIX_BASE_ADDR (0xFFFFEE00)                                // MATRIX Base Address
#define MATRIX_MCFG      (*(volatile U32*)(MATRIX_BASE_ADDR + 0x00)) // MATRIX Master configuration register
#define MATRIX_EBICSA    (*(volatile U32*)(MATRIX_BASE_ADDR + 0x30)) // MATRIX EBI Chip Select Assignment register

#define PMC_BASE_ADDR    0xFFFFFC00
#define PMC_PCER         (*(volatile U32 *)(PMC_BASE_ADDR + 0x10)) // (PMC) Peripheral Clock Enable Register
#define PMC_PCDR         (*(volatile U32 *)(PMC_BASE_ADDR + 0x14)) // (PMC) Peripheral Clock Disable Register

// ========== Register definition for PIOC peripheral ==========
#define PIOC_BASE        0xFFFFF800
#define PIOC_PER         (*(volatile U32 *)(PIOC_BASE + 0x00)) // (PIOC) PIO Enable Register
#define PIOC_PDR         (*(volatile U32 *)(PIOC_BASE + 0x04)) // (PIOC) PIO Disable Register
#define PIOC_PSR         (*(volatile U32 *)(PIOC_BASE + 0x08)) // (PIOC) PIO Status Register
#define PIOC_OER         (*(volatile U32 *)(PIOC_BASE + 0x10)) // (PIOC) Output Enable Register
#define PIOC_ODR         (*(volatile U32 *)(PIOC_BASE + 0x14)) // (PIOC) Output Disable Registerr
#define PIOC_OSR         (*(volatile U32 *)(PIOC_BASE + 0x18)) // (PIOC) Output Status Register
#define PIOC_IFER        (*(volatile U32 *)(PIOC_BASE + 0x20)) // (PIOC) Input Filter Enable Register
#define PIOC_IFDR        (*(volatile U32 *)(PIOC_BASE + 0x24)) // (PIOC) Input Filter Disable Register
#define PIOC_IFSR        (*(volatile U32 *)(PIOC_BASE + 0x28)) // (PIOC) Input Filter Status Register
#define PIOC_SODR        (*(volatile U32 *)(PIOC_BASE + 0x30)) // (PIOC) Set Output Data Register
#define PIOC_CODR        (*(volatile U32 *)(PIOC_BASE + 0x34)) // (PIOC) Clear Output Data Register
#define PIOC_ODSR        (*(volatile U32 *)(PIOC_BASE + 0x38)) // (PIOC) Output Data Status Register
#define PIOC_PDSR        (*(volatile U32 *)(PIOC_BASE + 0x3C)) // (PIOC) Pin Data Status Register
#define PIOC_IER         (*(volatile U32 *)(PIOC_BASE + 0x40)) // (PIOC) Interrupt Enable Register
#define PIOC_IDR         (*(volatile U32 *)(PIOC_BASE + 0x44)) // (PIOC) Interrupt Disable Register
#define PIOC_IMR         (*(volatile U32 *)(PIOC_BASE + 0x48)) // (PIOC) Interrupt Mask Register
#define PIOC_ISR         (*(volatile U32 *)(PIOC_BASE + 0x4C)) // (PIOC) Interrupt Status Register
#define PIOC_MDER        (*(volatile U32 *)(PIOC_BASE + 0x50)) // (PIOC) Multi-driver Enable Register
#define PIOC_MDDR        (*(volatile U32 *)(PIOC_BASE + 0x54)) // (PIOC) Multi-driver Disable Register
#define PIOC_MDSR        (*(volatile U32 *)(PIOC_BASE + 0x58)) // (PIOC) Multi-driver Status Register
#define PIOC_PPUDR       (*(volatile U32 *)(PIOC_BASE + 0x60)) // (PIOC) Pull-up Disable Register
#define PIOC_PPUER       (*(volatile U32 *)(PIOC_BASE + 0x64)) // (PIOC) Pull-up Enable Register
#define PIOC_PPUSR       (*(volatile U32 *)(PIOC_BASE + 0x68)) // (PIOC) Pull-up Status Register
#define PIOC_ASR         (*(volatile U32 *)(PIOC_BASE + 0x70)) // (PIOC) Select A Register
#define PIOC_BSR         (*(volatile U32 *)(PIOC_BASE + 0x74)) // (PIOC) Select B Register
#define PIOC_ABSR        (*(volatile U32 *)(PIOC_BASE + 0x78)) // (PIOC) AB Select Status Register
#define PIOC_OWER        (*(volatile U32 *)(PIOC_BASE + 0xA0)) // (PIOC) Output Write Enable Register
#define PIOC_OWDR        (*(volatile U32 *)(PIOC_BASE + 0xA4)) // (PIOC) Output Write Disable Register
#define PIOC_OWSR        (*(volatile U32 *)(PIOC_BASE + 0xA8)) // (PIOC) Output Write Status Register

#define PERIPHAL_ID_PIOC        (4)  // Parallel IO Controller C

/*********************************************************************
*
*       Static data
*
**********************************************************************
*/
static U16 * _pCurrentNANDAddr;
/*********************************************************************
*
*       Static code
*
**********************************************************************
*/
/*********************************************************************
*
*       _CopyData16
*/
static void _CopyData16(void * pDest, const void * pSrc, unsigned NumBytes) {
  char * pd;
  const char * ps;
  pd = (char*)pDest;
  ps = (const char*)pSrc;
  
  if ((((U32)ps & 1) == 0) && (((U32)pd & 1) == 0)) {
    unsigned NumHWords = NumBytes >> 1;
    if (NumHWords) {
      do {
        *(short *)pd = *(const short *)ps;
        pd += 2;
        ps += 2;
      } while (--NumHWords);
    }
    NumBytes &= 1;
  }
  //
  // Copy bytes, one at a time
  //
  if (NumBytes) {
    do {
      *(char*)pd++ = *(const char*)ps++;
    } while (--NumBytes);
  }
}
/*********************************************************************
*
*       Public code
*
**********************************************************************
*/

/*********************************************************************
*
*       FS_NAND_HW_X_EnableCE
*/
void FS_NAND_HW_X_EnableCE(U8 Unit) {
  PIOC_CODR = (1 << 14); // Enable NAND CE
}

/*********************************************************************
*
*       FS_NAND_HW_X_DisableCE
*/
void FS_NAND_HW_X_DisableCE(U8 Unit) {
  PIOC_SODR = (1 << 14); // Disable NAND CE
}


/*********************************************************************
*
*       FS_NAND_HW_X_SetData
*/
void FS_NAND_HW_X_SetDataMode(U8 Unit) {
  FS_USE_PARA(Unit);
  // CLE low, ALE low
  _pCurrentNANDAddr = NAND_DATA;
}


/*********************************************************************
*
*       FS_NAND_HW_X_SetCmd
*/
void FS_NAND_HW_X_SetCmdMode(U8 Unit) {
  FS_USE_PARA(Unit);
  //CLE high, ALE low
  _pCurrentNANDAddr = NAND_CMD;
}

/*********************************************************************
*
*       FS_NAND_HW_X_SetAddr
*/
void FS_NAND_HW_X_SetAddrMode(U8 Unit) {
  FS_USE_PARA(Unit);
  // CLE low, ALE high
  _pCurrentNANDAddr = NAND_ADDR;
}

/*********************************************************************
*
*       FS_NAND_HW_X_Read_x8
*/
void FS_NAND_HW_X_Read_x8(U8 Unit, void * pData, unsigned NumBytes) {
  FS_MEMCPY(pData, _pCurrentNANDAddr, NumBytes);
}

/*********************************************************************
*
*       FS_NAND_HW_X_Read_x16
*/
void FS_NAND_HW_X_Read_x16(U8 Unit, void * pData, unsigned NumBytes) {
  _CopyData16(pData, _pCurrentNANDAddr, NumBytes);
}

/*********************************************************************
*
*       FS_NAND_HW_X_Write_x8
*/
void FS_NAND_HW_X_Write_x8(U8 Unit, const void * pData, unsigned NumBytes) {
  FS_MEMCPY(_pCurrentNANDAddr, pData, NumBytes);
}

/*********************************************************************
*
*       FS_NAND_HW_X_Write_x16
*/
void FS_NAND_HW_X_Write_x16(U8 Unit, const void * pData, unsigned NumBytes) {
  _CopyData16(_pCurrentNANDAddr, pData, NumBytes);
}


/*********************************************************************
*
*       FS_NAND_HW_X_Init_x8
*/
void FS_NAND_HW_X_Init_x8(U8 Unit) {
  //
  // Update external bus interface, static memory controller
  //
  MATRIX_EBICSA |= (1 << 3); // Assign CS3 for use with NAND flash
  SMC_SETUP3 = (0x01 <<  0)  // 1 cycle nWE setup length
             | (0x00 <<  8)  // 1 cycle nCSWE setup length for write access
             | (0x01 << 16)  // 1 cycle nRE setup length
             | (0x00 << 24)  // 1 cycle nCSRD setup length for read access
             ;   
  SMC_PULSE3 = (0x03 <<  0)  // 3 cycles nWR pulse length
             | (0x03 <<  8)  // 3 cycles nCS pulse length in write access
             | (0x03 << 16)  // 3 cycles nRD pulse length
             | (0x03 << 24)  // 3 cycles nCS pulse length in read access
             ;
  SMC_CYCLE3 = (0x05 <<  0)  // 5 cycles for complete write length
             | (0x05 << 16)  // 5 cycles for complete read length
             ;
  SMC_CTRL3  =  (1 <<  0)    // read operation is controlled by nRD
             |  (1 <<  1)    // write operation is controlled by nWE
             |  (0 <<  4)    // nWait is not used
             |  (0 <<  8)    // bytes access is realized by the nBSx signals
             |  (0 << 12)    // DBW: 0: 8-bit NAND, 1: 16-bit NAND
             |  (2 << 16)    // Add 2 cycles for data float time
             |  (0 << 20)    // data float time is not optimized
             |  (0 << 24)    // Page mode is not enabled
             ;
  //
  // Enable clocks for PIOA, PIOB, PIOC
  //
  PMC_PCER = (1 << PERIPHAL_ID_PIOC)
           ;
  //
  //  Set PIOC pin 15 as port pin, for use as NAND Ready/Busy line.
  //
  PIOC_ODR  = 1 << 15;    // Configure in intput
  PIOC_PER  = 1 << 15;    // Set pin as port pin
  //
  //  Set PIOC pin 14 as port pin (output), for use as NAND CS.
  //
  PIOC_PER  = 1 << 14;    // Set pin as port pin
  PIOC_OER  = 1 << 14;    // Configure as output
  PIOC_SODR = 1 << 14;    // set pin high
  //
  //  Set different port pins to alternate function
  //
  PIOC_ASR  = (1 <<  0)            // PIOC.pin0  -> NAND_OE
           |  (1 <<  1)            // PIOC.pin1  -> NAND_WE
           ;
  PIOC_PDR =  (1 <<  0)            // PIOC.pin0  -> NAND_OE
           |  (1 <<  1)            // PIOC.pin1  -> NAND_WE
           ;
}

/*********************************************************************
*
*       FS_NAND_HW_X_Init_x16
*/
void FS_NAND_HW_X_Init_x16(U8 Unit) {
  FS_NAND_HW_X_Init_x8(Unit);
  SMC_CTRL3  = (2 << 16)     // 0 cycles data float time
             | (1 << 12)     // DBW: 0: 8-bit NAND, 1: 16-bit NAND
             | (3 <<  0);    // Use NRD & NWE signals for read / write
}

/*********************************************************************
*
*             FS_NAND_HW_X_WaitWhileBusy
*/
int FS_NAND_HW_X_WaitWhileBusy(U8 Unit, unsigned us) {
  while ((PIOC_PDSR & (1 << 15)) == 0);
  return 0;
}

/**************************** end of file ***************************/

