#include "NAND_X_HW.h"
#include "FS_Int.h"       // For FS_MEMCPY
#include <string.h>
/*********************************************************************
*
*       #include Section
*
**********************************************************************
*/
#ifdef __ICCARM__
  #define OPTIMIZE        /* __arm __ramfunc */
#else
  #define OPTIMIZE
#endif


/*********************************************************************
*
*       #define Macros
*
**********************************************************************
*/
#define NAND_DATA               (U16 *)(0x3D000000)
#define NAND_ADDR               (U16 *)(0x3D400000)
#define NAND_CMD                (U16 *)(0x3D200000)

/*********************************************************************
*
*       #define sfrs
*
**********************************************************************
*/

/*********************************************************************
*
*       Static data
*
**********************************************************************
*/
static U16 * _pCurrentNANDAddr;
/*********************************************************************
*
*       Static code
*
**********************************************************************
*/


/*********************************************************************
*
*       Public code
*
**********************************************************************
*/


/*********************************************************************
*
*       FS_NAND_HW_X_EnableCE
*/
OPTIMIZE void FS_NAND_HW_X_EnableCE(U8 Unit) {
    //nothing
}

/*********************************************************************
*
*       FS_NAND_HW_X_DisableCE
*/
OPTIMIZE void FS_NAND_HW_X_DisableCE(U8 Unit) {
    //nothing
}


/*********************************************************************
*
*       FS_NAND_HW_X_SetData
*/
OPTIMIZE void FS_NAND_HW_X_SetDataMode(U8 Unit) {
  FS_USE_PARA(Unit);
  // CLE low, ALE low
  _pCurrentNANDAddr = NAND_DATA;
}


/*********************************************************************
*
*       FS_NAND_HW_X_SetCmd
*/
OPTIMIZE void FS_NAND_HW_X_SetCmdMode(U8 Unit) {
  FS_USE_PARA(Unit);
  //CLE high, ALE low
  _pCurrentNANDAddr = NAND_CMD;
}

/*********************************************************************
*
*       FS_NAND_HW_X_SetAddr
*/
OPTIMIZE void FS_NAND_HW_X_SetAddrMode(U8 Unit) {
  FS_USE_PARA(Unit);
  // CLE low, ALE high
  _pCurrentNANDAddr = NAND_ADDR;
}

/*********************************************************************
*
*       FS_NAND_HW_X_Read_x8
*/
void FS_NAND_HW_X_Read_x8(U8 Unit, void * pData, unsigned NumBytes) {
#if 0   //!?! -Q-
  FS_memcpy(pData, _pCurrentNANDAddr, NumBytes);
#else
  U8 * p;
  p = (U8 *) pData;
  while (NumBytes--) {
    // *(U8 *)pData = *_pCurrentNANDAddr;
    *(U8 *)p = *_pCurrentNANDAddr;
    p++;
  }
#endif
}

/*********************************************************************
*
*       FS_NAND_HW_X_Read_x16
*/
void FS_NAND_HW_X_Read_x16(U8 Unit, void * pData, unsigned NumBytes) {
#if 1
  FS_memcpy(pData, _pCurrentNANDAddr, NumBytes);
#else
  NumBytes >>= 1;
  while (NumBytes--) {
    *(U16 *)pData = *_pCurrentNANDAddr;
    pData += 2;
  }
#endif
}

/*********************************************************************
*
*       FS_NAND_HW_X_Write_x8
*/
void FS_NAND_HW_X_Write_x8(U8 Unit, const void * pData, unsigned NumBytes) {
#if 0   //!?! -Q-
  FS_memcpy(_pCurrentNANDAddr, pData, NumBytes);
#else
  U8 * p;
  p = (U8 *) pData;

  while (NumBytes--) {
    *_pCurrentNANDAddr = *(U8 *)p;
    p++;
  }
#endif
}

/*********************************************************************
*
*       FS_NAND_HW_X_Write_x16
*/
void FS_NAND_HW_X_Write_x16(U8 Unit, const void * pData, unsigned NumBytes) {
#if 1
  FS_memcpy(_pCurrentNANDAddr, pData, NumBytes);
#else
  NumBytes >>= 1;
  while (NumBytes--) {
    *_pCurrentNANDAddr = *(U16 *)pData;
    pData += 2;
  }
#endif
}


/*********************************************************************
*
*       FS_NAND_HW_X_Init_x8
*/
void FS_NAND_HW_X_Init_x8(U8 Unit) {
}

/*********************************************************************
*
*       FS_NAND_HW_X_Init_x16
*/
void FS_NAND_HW_X_Init_x16(U8 Unit) {
}

/*********************************************************************
*
*             FS_NAND_HW_X_WaitWhileBusy
*/
#define NCMD_READST    0x70
#define NCMD_READ      0x00

#define ST_ERROR 0x01
#define ST_READY 0x40
#define ST_WPROT 0x80
OPTIMIZE int FS_NAND_HW_X_WaitWhileBusy(U8 Unit, unsigned us) {
//    for (;;)
//	{
//		U32 state;
//		*NAND_CMD = NCMD_READST;
//		state=*NAND_DATA;
//		if (state & ST_READY)
//		{
//		   	*NAND_CMD = NCMD_READ;
//			return 0;
//		}
//	}
    return 1;   //use software status
}

/**************************** end of file ***************************/

