/*********************************************************************
*                SEGGER MICROCONTROLLER GmbH & Co. KG                *
*        Solutions for real time microcontroller applications        *
**********************************************************************
*                                                                    *
*        (c) 2003-2012     SEGGER Microcontroller GmbH & Co KG       *
*                                                                    *
*        Internet: www.segger.com    Support:  support@segger.com    *
*                                                                    *
**********************************************************************

**** emFile file system for embedded applications ****
emFile is protected by international copyright laws. Knowledge of the
source code may not be used to write a similar product. This file may
only be used in accordance with a license and should not be re-
distributed in any way. We appreciate your understanding and fairness.
----------------------------------------------------------------------
----------------------------------------------------------------------
File        : FS_NAND_HW_AT91SAM9G45.c
Purpose     : NAND flash hardware layer for Atmel AT91SAM9G45
----------------------------------------------------------------------
Known problems or limitations with current version
----------------------------------------------------------------------
None.
---------------------------END-OF-HEADER------------------------------
*/

/*********************************************************************
*
*       #include Section
*
**********************************************************************
*/
#include "NAND_X_HW.h"
#include "FS_Int.h"       // For FS_MEMCPY
#include <string.h>

#include "BSP.h"

/*********************************************************************
*
*       #define Macros
*
**********************************************************************
*/
#define NAND_BASE_ADDR    0x40000000uL
#define NAND_DATA         (U16 *)(NAND_BASE_ADDR + 0)
#define NAND_ADDR         (U16 *)(NAND_BASE_ADDR + (1uL << 21))   // A21 NANDALE
#define NAND_CMD          (U16 *)(NAND_BASE_ADDR + (1uL << 22))   // A22 NANDCLE

/*********************************************************************
*
*       #define sfrs
*
**********************************************************************
*/

//
// Static Memory Controller
//
#define SMC_BASE_ADDR     0xFFFFE800uL
#define SMC_SETUP3        (*(volatile U32 *)(SMC_BASE_ADDR + 3 * 0x10 + 0x00))   // SMC CS3 Setup Register
#define SMC_PULSE3        (*(volatile U32 *)(SMC_BASE_ADDR + 3 * 0x10 + 0x04))   // SMC CS3 Pulse Register
#define SMC_CYCLE3        (*(volatile U32 *)(SMC_BASE_ADDR + 3 * 0x10 + 0x08))   // SMC CS3 Cycle Register
#define SMC_CTRL3         (*(volatile U32 *)(SMC_BASE_ADDR + 3 * 0x10 + 0x0C))   // SMC CS3 Mode Register

//
// MATRIX + EBI interface
//
#define MATRIX_BASE_ADDR  0xFFFFEA00uL                                  // MATRIX Base Address
#define CCFG_EBICSA       (*(volatile U32*)(MATRIX_BASE_ADDR + 0x128))  // MATRIX EBI Chip Select Assignment register

//
// Power Management Controller
//
#define PMC_BASE_ADDR     0xFFFFFC00uL
#define PMC_PCER          (*(volatile U32 *)(PMC_BASE_ADDR + 0x10))     // (PMC) Peripheral Clock Enable Register

// Port C Controller
#define PIOC_BASE         0xFFFFF600uL
#define PIOC_PER          (*(volatile U32 *)(PIOC_BASE + 0x00)) // (PIOC) PIO Enable Register
#define PIOC_OER          (*(volatile U32 *)(PIOC_BASE + 0x10)) // (PIOC) Output Enable Register
#define PIOC_ODR          (*(volatile U32 *)(PIOC_BASE + 0x14)) // (PIOC) Output Disable Registerr
#define PIOC_SODR         (*(volatile U32 *)(PIOC_BASE + 0x30)) // (PIOC) Set Output Data Register
#define PIOC_CODR         (*(volatile U32 *)(PIOC_BASE + 0x34)) // (PIOC) Clear Output Data Register
#define PIOC_PDSR         (*(volatile U32 *)(PIOC_BASE + 0x3C)) // (PIOC) Pin Data Status Register

#define PIOC_ID       4

#define PIOC_RDY_BSY  8   // Ready/busy port pin
#define PIOC_CS       14  // Chip select port pin

/*********************************************************************
*
*       Static data
*
**********************************************************************
*/
static U16 * _pCurrentNANDAddr;

/*********************************************************************
*
*       Static code
*
**********************************************************************
*/

/*********************************************************************
*
*       Public code
*
**********************************************************************
*/

/*********************************************************************
*
*       FS_NAND_HW_X_EnableCE
*
*   Function description
*
*
*/
void FS_NAND_HW_X_EnableCE(U8 Unit) {
  FS_USE_PARA(Unit);
  PIOC_CODR = (1 << PIOC_CS);       // Enable NAND CE
}

/*********************************************************************
*
*       FS_NAND_HW_X_DisableCE
*/
void FS_NAND_HW_X_DisableCE(U8 Unit) {
  FS_USE_PARA(Unit);
  PIOC_SODR = (1 << PIOC_CS);       // Disable NAND CE
}

/*********************************************************************
*
*       FS_NAND_HW_X_SetData
*/
void FS_NAND_HW_X_SetDataMode(U8 Unit) {
  FS_USE_PARA(Unit);
  _pCurrentNANDAddr = NAND_DATA;    // CLE low, ALE low
}

/*********************************************************************
*
*       FS_NAND_HW_X_SetCmd
*/
void FS_NAND_HW_X_SetCmdMode(U8 Unit) {
  FS_USE_PARA(Unit);
  _pCurrentNANDAddr = NAND_CMD;     // CLE high, ALE low
}

/*********************************************************************
*
*       FS_NAND_HW_X_SetAddr
*/
void FS_NAND_HW_X_SetAddrMode(U8 Unit) {
  FS_USE_PARA(Unit);
  _pCurrentNANDAddr = NAND_ADDR;    // CLE low, ALE high
}

/*********************************************************************
*
*       FS_NAND_HW_X_Read_x8
*/
void FS_NAND_HW_X_Read_x8(U8 Unit, void * pData, unsigned NumBytes) {
  FS_USE_PARA(Unit);
  FS_MEMCPY(pData, _pCurrentNANDAddr, NumBytes);
}

/*********************************************************************
*
*       FS_NAND_HW_X_Read_x16
*/
void FS_NAND_HW_X_Read_x16(U8 Unit, void * pData, unsigned NumBytes) {
  FS_USE_PARA(Unit);
  FS_MEMCPY(pData, _pCurrentNANDAddr, NumBytes);
}

/*********************************************************************
*
*     FS_NAND_HW_X_Write_x8
*/
void FS_NAND_HW_X_Write_x8(U8 Unit, const void * pData, unsigned NumBytes) {
  FS_USE_PARA(Unit);
  FS_MEMCPY(_pCurrentNANDAddr, pData, NumBytes);
}

/*********************************************************************
*
*     FS_NAND_HW_X_Write_x16
*/
void FS_NAND_HW_X_Write_x16(U8 Unit, const void * pData, unsigned NumBytes) {
  FS_USE_PARA(Unit);
  FS_MEMCPY(_pCurrentNANDAddr, pData, NumBytes);
}

/*********************************************************************
*
*     FS_NAND_HW_X_Init_x8
*
*   Function description
*     Initializes the memory controller and the port pins
*     used by the interface to NAND device.
*
*   Parameters
*     Unit
*
*   Notes
*       (1) The waveform of the signals assumes that the SMC runs at
*           a frequency of 384MHz (2.6ns).
*/
void FS_NAND_HW_X_Init_x8(U8 Unit) {
  FS_USE_PARA(Unit);
  //
  // Configure the external bus interface, static memory controller
  //
  CCFG_EBICSA |= (1 <<  3);   // Assign CS3 for use with NAND flash
  SMC_SETUP3   = (2 <<  0)    // nWE setup length
               | (2 << 16)    // nRE setup length
               ;
  SMC_PULSE3   = (4 <<  0)    // nWR pulse length
               | (4 <<  8)    // nCS pulse length in write access
               | (4 << 16)    // nRD pulse length
               | (4 << 24)    // nCS pulse length in read access
               ;
  SMC_CYCLE3   = (7 <<  0)    // complete write length
               | (7 << 16)    // for complete read length
               ;
  SMC_CTRL3    = (1 <<  0)    // read operation is controlled by nRD
               | (1 <<  1)    // write operation is controlled by nWE
               | (3 << 16)    // Cycles for data float time
               | (0 << 20)    // data float time is not optimized
               | (0 << 24)    // Page mode is not enabled
               ;
  PMC_PCER  = (1 << PIOC_ID);     // Enable clock of the I/O port
  //
  // Set the port pin to be used as NAND Ready/Busy line.
  //
  PIOC_ODR  = 1 << PIOC_RDY_BSY;  // Configure input
  PIOC_PER  = 1 << PIOC_RDY_BSY;  // Set pin as port pin
  //
  // Set port pin for use as chip select for NAND device
  //
  PIOC_SODR = 1 << PIOC_CS;       // Set pin high
  PIOC_OER  = 1 << PIOC_CS;       // Configure as output
  PIOC_PER  = 1 << PIOC_CS;       // Set pin as port pin
}

/*********************************************************************
*
*     FS_NAND_HW_X_Init_x16
*
*   Function description
*     Initializes the NAND controller when the NAND device
*     has 16 I/O data lines.
*
*   Parameters
*     Unit
*/
void FS_NAND_HW_X_Init_x16(U8 Unit) {
  FS_NAND_HW_X_Init_x8(Unit);
  SMC_CTRL3 = (1 << 12)     // DBW: 0: 8-bit NAND, 1: 16-bit NAND
            | (3 << 0)      // Use NRD & NWE signals for read / write
            ;
}

/*********************************************************************
*
*     FS_NAND_HW_X_WaitWhileBusy
*
*   Function description
*     Waits with an optional timeout for the NAND device to become ready.
*
*   Parameters
*     Unit
*     us
*
*   Return values
*
*/
int FS_NAND_HW_X_WaitWhileBusy(U8 Unit, unsigned us) {
  FS_USE_PARA(Unit);
  FS_USE_PARA(us);
  while ((PIOC_PDSR & (1 << PIOC_RDY_BSY)) == 0) {
    ;
  }
  return 0;
}

/**************************** end of file ***************************/

