/*********************************************************************
*                SEGGER MICROCONTROLLER GmbH & Co. KG                *
*        Solutions for real time microcontroller applications        *
**********************************************************************
*                                                                    *
*        (c) 2003-2012     SEGGER Microcontroller GmbH & Co KG       *
*                                                                    *
*        Internet: www.segger.com    Support:  support@segger.com    *
*                                                                    *
**********************************************************************

**** emFile file system for embedded applications ****
emFile is protected by international copyright laws. Knowledge of the
source code may not be used to write a similar product. This file may
only be used in accordance with a license and should not be re-
distributed in any way. We appreciate your understanding and fairness.
----------------------------------------------------------------------
File        : NAND_PHY_Freescale_K60.c
Purpose     : Physical Layer for the NAND driver which uses the NAND flash controller of Freescale Kinetis K60 CPU.
Literature  : [1] \\fileserver\Techinfo\Company\Freescale\MCU\CortexM4_Kinetis\K60P144M150SF3RM_Rev2.pdf
              [2] \\fileserver\Techinfo\Company\Freescale\MCU\CortexM4_Kinetis\EvalBoard\TWR-K60F120M\TWR-K60F120M-SCH.pdf
---------------------------END-OF-HEADER------------------------------
*/

#include "FS_Int.h"
#include "NAND_Private.h"

/*********************************************************************
*
*       Defines, configurable
*
**********************************************************************
*/
#ifdef    FS_NAND_MAXUNIT
  #define NUM_UNITS   FS_NAND_MAXUNIT
#else
  #define NUM_UNITS   4
#endif

/*********************************************************************
*
*       Defines, not configurable
*
**********************************************************************
*/

/*********************************************************************
*
*       NAND flash controller
*/
#define NFC_BASE_ADDR     0x400A8000uL
#define NFC_DATA          (*(volatile U32 *)(NFC_BASE_ADDR + 0x0000))     // Data buffer
#define NFC_CMD1          (*(volatile U32 *)(NFC_BASE_ADDR + 0x3F00))     // Flash command register 1
#define NFC_CMD2          (*(volatile U32 *)(NFC_BASE_ADDR + 0x3F04))     // Flash command register 2
#define NFC_CAR           (*(volatile U32 *)(NFC_BASE_ADDR + 0x3F08))     // Column address register
#define NFC_RAR           (*(volatile U32 *)(NFC_BASE_ADDR + 0x3F0C))     // Row address register
#define NFC_RAI           (*(volatile U32 *)(NFC_BASE_ADDR + 0x3F14))     // Row address increment register
#define NFC_SR1           (*(volatile U32 *)(NFC_BASE_ADDR + 0x3F18))     // Flash status register 1
#define NFC_SR2           (*(volatile U32 *)(NFC_BASE_ADDR + 0x3F1C))     // Flash status register 2
#define NFC_DMA1          (*(volatile U32 *)(NFC_BASE_ADDR + 0x3F20))     // DMA channel 1 address register
#define NFC_DMACFG        (*(volatile U32 *)(NFC_BASE_ADDR + 0x3F24))     // DMA configuration register
#define NFC_SWAP          (*(volatile U32 *)(NFC_BASE_ADDR + 0x3F28))     // Catch swap register
#define NFC_SECSZ         (*(volatile U32 *)(NFC_BASE_ADDR + 0x3F2C))     // Sector size register
#define NFC_CFG           (*(volatile U32 *)(NFC_BASE_ADDR + 0x3F30))     // Flash configuration register
#define NFC_ISR           (*(volatile U32 *)(NFC_BASE_ADDR + 0x3F38))     // Interrupt status register

/*********************************************************************
*
*       System integration module
*/
#define SIM_BASE_ADDR     0x40047000uL
#define SIM_SCGC3         (*(volatile U32 *)(SIM_BASE_ADDR + 0x1030))   // System clock gating register 1
#define SIM_SCGC5         (*(volatile U32 *)(SIM_BASE_ADDR + 0x1038))   // System clock gating register 5
#define SIM_CLKDIV4       (*(volatile U32 *)(SIM_BASE_ADDR + 0x1068))   // System clock divider register 4

/*********************************************************************
*
*      Port B
*/
#define PORTB_BASE_ADDR   0x4004A000uL
#define PORTB_PCR20        (*(volatile U32 *)(PORTB_BASE_ADDR + 0x50))
#define PORTB_PCR21        (*(volatile U32 *)(PORTB_BASE_ADDR + 0x54))
#define PORTB_PCR22        (*(volatile U32 *)(PORTB_BASE_ADDR + 0x58))
#define PORTB_PCR23        (*(volatile U32 *)(PORTB_BASE_ADDR + 0x5C))

/*********************************************************************
*
*      Port C
*/
#define PORTC_BASE_ADDR   0x4004B000uL
#define PORTC_PCR0        (*(volatile U32 *)(PORTC_BASE_ADDR + 0x00))
#define PORTC_PCR1        (*(volatile U32 *)(PORTC_BASE_ADDR + 0x04))
#define PORTC_PCR2        (*(volatile U32 *)(PORTC_BASE_ADDR + 0x08))
#define PORTC_PCR3        (*(volatile U32 *)(PORTC_BASE_ADDR + 0x0C))
#define PORTC_PCR4        (*(volatile U32 *)(PORTC_BASE_ADDR + 0x10))
#define PORTC_PCR5        (*(volatile U32 *)(PORTC_BASE_ADDR + 0x14))
#define PORTC_PCR6        (*(volatile U32 *)(PORTC_BASE_ADDR + 0x18))
#define PORTC_PCR7        (*(volatile U32 *)(PORTC_BASE_ADDR + 0x1C))
#define PORTC_PCR8        (*(volatile U32 *)(PORTC_BASE_ADDR + 0x20))
#define PORTC_PCR9        (*(volatile U32 *)(PORTC_BASE_ADDR + 0x24))
#define PORTC_PCR10       (*(volatile U32 *)(PORTC_BASE_ADDR + 0x28))
#define PORTC_PCR11       (*(volatile U32 *)(PORTC_BASE_ADDR + 0x2C))
#define PORTC_PCR16       (*(volatile U32 *)(PORTC_BASE_ADDR + 0x40))
#define PORTC_PCR17       (*(volatile U32 *)(PORTC_BASE_ADDR + 0x44))
#define PORTC_PCR18       (*(volatile U32 *)(PORTC_BASE_ADDR + 0x48))

/*********************************************************************
*
*      Port D
*/
#define PORTD_BASE_ADDR   0x4004C000uL
#define PORTD_PCR4        (*(volatile U32 *)(PORTD_BASE_ADDR + 0x10))
#define PORTD_PCR5        (*(volatile U32 *)(PORTD_BASE_ADDR + 0x14))
#define PORTD_PCR8        (*(volatile U32 *)(PORTD_BASE_ADDR + 0x20))
#define PORTD_PCR9        (*(volatile U32 *)(PORTD_BASE_ADDR + 0x24))
#define PORTD_PCR10       (*(volatile U32 *)(PORTD_BASE_ADDR + 0x28))

/*********************************************************************
*
*      Memory protection unit
*/
#define MPU_BASE        0x4000D000
#define MPU_RGDAAC0     (*(volatile U32*)(MPU_BASE + 0x800))   // Region Descriptor Alternate Access Control 0

/*********************************************************************
*
*      Flash configuration register
*/
#define CFG_PAGECNT           0
#define CFG_PAGECNT_MAX       15uL
#define CFG_AIBN              4
#define CFG_AIAD              5
#define CFG_BITWIDTH          7
#define CFG_TIMEOUT           8
#define CFG_IDCNT             13
#define CFG_IDCNT_MAX         7uL
#define CFG_ECC_MODE          17
#define CFG_ECC_MODE_NONE     0uL
#define CFG_ECC_MODE_4BIT     1uL
#define CFG_ECC_MODE_6BIT     2uL
#define CFG_ECC_MODE_8BIT     3uL
#define CFG_ECC_MODE_12BIT    4uL
#define CFG_ECC_MODE_16BIT    5uL
#define CFG_ECC_MODE_24BIT    6uL
#define CFG_ECC_MODE_32BIT    7uL
#define CFG_ECC_MODE_MAX      7uL
#define CFG_DMAREQ            20
#define CFG_ECCSRAM           21
#define CFG_ECCAD             22
#define CFG_STOPWERR          31

/*********************************************************************
*
*      Flash command register 2
*/
#define CMD2_BUSY_START       0
#define CMD2_BUFNO            1
#define CMD2_CODE             8
#define CMD2_BYTE1            24

/*********************************************************************
*
*      Flash command register 1
*/
#define CMD1_BYTE2            24

/*********************************************************************
*
*      Command sequencer codes
*/
#define CODE_END_OF_CMD       1
#define CODE_READ_ID          2
#define CODE_READ_STATUS      3
#define CODE_READ_DATA        5
#define CODE_WAIT_WHILE_BUSY  6
#define CODE_SEND_CMD_BYTE2   7
#define CODE_WRITE_DATA       8
#define CODE_SEND_ROW_ADDR3   9
#define CODE_SEND_ROW_ADDR2   10
#define CODE_SEND_ROW_ADDR1   11
#define CODE_SEND_COL_ADDR2   12
#define CODE_SEND_COL_ADDR1   13
#define CODE_SEND_CMD_BYTE1   14

/*********************************************************************
*
*      Row address register
*/
#define RAR_BYTE1             0
#define RAR_BYTE2             8
#define RAR_BYTE3             16
#define RAR_RB0               24
#define RAR_RB1               25
#define RAR_CS0               28
#define RAR_CS1               29

/*********************************************************************
*
*      Column address register
*/
#define CAR_BYTE1             0
#define CAR_BYTE2             8

/*********************************************************************
*
*      Status register 1
*/
#define SR1_ID4               0
#define SR1_ID3               8
#define SR1_ID2               16
#define SR1_ID1               24

/*********************************************************************
*
*      Status register 2
*/
#define SR2_ID5               24

/*********************************************************************
*
*      Interrupt status register
*/
#define ISR_IDLECLR           17
#define ISR_DONECLR           18
#define ISR_WERRCLR           19
#define ISR_IDLE              29
#define ISR_DONE              30

/*********************************************************************
*
*      System clock gating register
*/
#define SCGC3_NFC             8
#define SCGC5_PORTB           10
#define SCGC5_PORTC           11
#define SCGC5_PORTD           12

/*********************************************************************
*
*      System clock divider register 4
*/
#define CLKDIV4_NFCDIV        27
#define CLKDIV4_NFCFRAC       24
//
// Divider for the NFC clock. The result of (NFCDIV_VALUE + 1) / (NFCFRAC_VALUE + 1) must be a multiple of 0.5
//
#define NFCDIV_VALUE          14uL
#define NFCFRAC_VALUE         1uL

/*********************************************************************
*
*      Pin control register
*/
#define PCR_DSE               6
#define PCR_MUX               8

/*********************************************************************
*
*       Execution status
*/
#define STATUS_ERROR            0x01    // 0:Pass,    1:Fail
#define STATUS_READY            0x40    // 0:Busy,    1:Ready
#define STATUS_WRITE_PROTECTED  0x80    // 0:Protect, 1:Not Protect

/*********************************************************************
*
*       NAND commands
*/
#define CMD_READ_1            0x00
#define CMD_RANDOM_READ_1     0x05
#define CMD_WRITE_2           0x10
#define CMD_READ_2            0x30
#define CMD_ERASE_1           0x60
#define CMD_ERASE_2           0xD0
#define CMD_READ_STATUS       0x70
#define CMD_WRITE_1           0x80
#define CMD_RANDOM_WRITE      0x85
#define CMD_READ_ID           0x90
#define CMD_RANDOM_READ_2     0xE0
#define CMD_READ_PARA_PAGE    0xEC
#define CMD_GET_FEATURES      0xEE
#define CMD_SET_FEATURES      0xEF
#define CMD_RESET             0xFF

/*********************************************************************
*
*       DMA configuration register
*/
#define DMACFG_COUNT1         20
#define DMACFG_ACT1           1

/*********************************************************************
*
*       MPU region descriptor bits
*/
#define RGD_M3UM    18
#define RGD_M3SM    21

/*********************************************************************
*
*       Device features
*/
#define NUM_FEATURE_PARA          4
#define MICRON_ECC_FEATURE_ADDR   0x90
#define MICRON_ECC_FEATURE_MASK   0x08

/*********************************************************************
*
*       ONFI parameters
*/
#define PARA_PAGE_SIZE        256
#define PARA_CRC_POLY         0x8005
#define PARA_CRC_INIT         0x4F4E
#define NUM_PARA_PAGES        3
#define READ_ID_ONFI          0x20

/*********************************************************************
*
*       ECC status
*/
#define ECC_STATUS_OFF        (0x8F7 + 5)       // ECC status is written to last byte of the first buffer page
#define ECC_STATUS            (*(volatile U8 *)(NFC_BASE_ADDR + ECC_STATUS_OFF))
#define ECC_STATUS_CORFAIL    7
#define ECCAD_VALUE           (ECC_STATUS_OFF / 8)

/*********************************************************************
*
*       Misc. defines
*/
#define BUFFER_SIZE           0x900             // Size of the internal NFC buffer in bytes
#define SPARE_OFF_BLOCK_STAT  0x00
#define NUM_BYTES_TO_SWAP     2

/*********************************************************************
*
*       ASSERT_PARA_IS_ALIGNED
*/
#if FS_DEBUG_LEVEL >= FS_DEBUG_LEVEL_CHECK_ALL
  #define ASSERT_PARA_IS_ALIGNED(pInst, Para)               \
    if ((pInst)->DataBusWidth == 16) {                      \
      if ((Para) & 1) {                                                                 \
        FS_DEBUG_ERROROUT((FS_MTYPE_DRIVER, "K60_NAND_PHY: Parameter not aligned.\n")); \
        FS_X_Panic(0);                                                                  \
      }                                                                                 \
    }
#else
  #define ASSERT_PARA_IS_ALIGNED(pInst, Para)
#endif

/*********************************************************************
*
*       ASSERT_WRITE_IS_COMPLETE
*/
#if FS_DEBUG_LEVEL >= FS_DEBUG_LEVEL_CHECK_ALL
  #define ASSERT_WRITE_IS_COMPLETE(pInst, Off, NumBytes, OffSpare, NumBytesSpare)                     \
    if ((Off != 0) || (NumBytes != OffSpare) || (NumBytesSpare != (pInst)->BytesPerSpareArea)) {      \
      FS_DEBUG_ERROROUT((FS_MTYPE_DRIVER, "K60_NAND_PHY: Partial page writes are not supported.\n")); \
      FS_X_Panic(0);                                                                                  \
    }
#else
  #define ASSERT_WRITE_IS_COMPLETE(pInst, Off, NumBytes, OffSpare, NumBytesSpare)
#endif

/*********************************************************************
*
*       ASSERT_READ_IS_COMPLETE
*/
#if FS_DEBUG_LEVEL >= FS_DEBUG_LEVEL_CHECK_ALL
  #define ASSERT_READ_IS_COMPLETE(pInst, Off, NumBytes, OffSpare, NumBytesSpare)                      \
    if ((Off != 0) || (NumBytes != OffSpare) || (NumBytesSpare != (pInst)->BytesPerSpareArea)) {      \
      FS_DEBUG_ERROROUT((FS_MTYPE_DRIVER, "K60_NAND_PHY: Partial page reads are not supported.\n"));  \
      FS_X_Panic(0);                                                                                  \
    }
#else
  #define ASSERT_READ_IS_COMPLETE(pInst, Off, NumBytes, OffSpare, NumBytesSpare)
#endif

/*********************************************************************
*
*       typedefs
*
**********************************************************************
*/
typedef struct {
  U8 Unit;
  U8 DataBusWidth;      // Width of the data bus in bits (16 or 8)
  U8 NumBytesColAddr;   // Number of bytes in a column address
  U8 NumBytesRowAddr;   // Number of bytes in a row address
  U8  ECCIsEnabled;         // 1 if the ECC of NFC is active
  U8  ECCMode;              // Number of bits the ECC of the NAND flash controller should correct.
  U8  NumBitsCorrectable;   // Number of error bits required to be corrected by ECC. 0 means that the HW ECC of the NAND flash device should be used.
  U16 ldBytesPerVPage;
  U16 BytesPerVSpareArea;
  U16 BytesPerSpareArea;
  U16 ldBytesPerPage;
  U16 SwapVPageOff;
  U16 SwapVSpareAreaOff;
  U16 SwapPageOff;
  U16 SwapSpareAreaOff;
} PHY_INST;

/*********************************************************************
*
*       Static data
*
**********************************************************************
*/
static PHY_INST _aInst[NUM_UNITS];

/*********************************************************************
*
*       Static code
*
**********************************************************************
*/

/*********************************************************************
*
*       _ld
*/
static U16 _ld(U32 Value) {
  U16 i;
  for (i = 0; i < 16; i++) {
    if ((1UL << i) == Value) {
      break;
    }
  }
  return i;
}

/*********************************************************************
*
*       _InitDataBus16
*
*   Function description
*     Configures I/O pins and NFC for a 16-bit data bus.
*/
static void _InitDataBus16(void) {
  NFC_CFG     |= 1uL << CFG_BITWIDTH;
  PORTB_PCR20 |= (5uL << PCR_MUX)
              |  (1uL << PCR_DSE)
              ;                       // NFC_DATA15
  PORTB_PCR21 |= (5uL << PCR_MUX)
              |  (1uL << PCR_DSE)
              ;                       // NFC_DATA14
  PORTB_PCR22 |= (5uL << PCR_MUX)
              |  (1uL << PCR_DSE)
              ;                       // NFC_DATA13
  PORTB_PCR23 |= (5uL << PCR_MUX)
              |  (1uL << PCR_DSE)
              ;                       // NFC_DATA12
  PORTC_PCR0  |= (5uL << PCR_MUX)
              |  (1uL << PCR_DSE)
              ;                       // NFC_DATA11
  PORTC_PCR1  |= (5uL << PCR_MUX)
              |  (1uL << PCR_DSE)
              ;                       // NFC_DATA10
  PORTC_PCR2  |= (5uL << PCR_MUX)
              |  (1uL << PCR_DSE)
              ;                       // NFC_DATA9
  PORTC_PCR4  |= (5uL << PCR_MUX)
              |  (1uL << PCR_DSE)
              ;                       // NFC_DATA8
  PORTC_PCR5  = (5uL << PCR_MUX)
              | (1uL << PCR_DSE)
              ;                       // NFC_DATA7
  PORTC_PCR6  = (5uL << PCR_MUX)
              | (1uL << PCR_DSE)
              ;                       // NFC_DATA6
  PORTC_PCR7  = (5uL << PCR_MUX)
              | (1uL << PCR_DSE)
              ;                       // NFC_DATA5
  PORTC_PCR8  = (5uL << PCR_MUX)
              | (1uL << PCR_DSE)
              ;                       // NFC_DATA4
  PORTC_PCR9  = (5uL << PCR_MUX)
              | (1uL << PCR_DSE)
              ;                       // NFC_DATA3
  PORTC_PCR10 = (5uL << PCR_MUX)
              | (1uL << PCR_DSE)
              ;                       // NFC_DATA2
  PORTD_PCR4  = (5uL << PCR_MUX)
              | (1uL << PCR_DSE)
              ;                       // NFC_DATA1
  PORTD_PCR5  = (5uL << PCR_MUX)
              | (1uL << PCR_DSE)
              ;                       // NFC_DATA0
}

/*********************************************************************
*
*       _InitDataBus8
*
*   Function description
*     Configures I/O pins and NFC for an 8-bit data bus.
*/
static void _InitDataBus8(void) {
  NFC_CFG &= ~(1uL << CFG_BITWIDTH);
  PORTC_PCR5  = (5uL << PCR_MUX)
              | (1uL << PCR_DSE)
              ;                       // NFC_DATA7
  PORTC_PCR6  = (5uL << PCR_MUX)
              | (1uL << PCR_DSE)
              ;                       // NFC_DATA6
  PORTC_PCR7  = (5uL << PCR_MUX)
              | (1uL << PCR_DSE)
              ;                       // NFC_DATA5
  PORTC_PCR8  = (5uL << PCR_MUX)
              | (1uL << PCR_DSE)
              ;                       // NFC_DATA4
  PORTC_PCR9  = (5uL << PCR_MUX)
              | (1uL << PCR_DSE)
              ;                       // NFC_DATA3
  PORTC_PCR10 = (5uL << PCR_MUX)
              | (1uL << PCR_DSE)
              ;                       // NFC_DATA2
  PORTD_PCR4  = (5uL << PCR_MUX)
              | (1uL << PCR_DSE)
              ;                       // NFC_DATA1
  PORTD_PCR5  = (5uL << PCR_MUX)
              | (1uL << PCR_DSE)
              ;                       // NFC_DATA0
}

/*********************************************************************
*
*       _InitNFC
*
*   Function description
*     Initializes the NAND flash controller.
*/
static void _InitNFC(PHY_INST * pInst) {
  U8 Unit;

  Unit = pInst->Unit;
  //
  // Reset NFC peripheral.
  //
  SIM_SCGC3 &= ~(1uL << SCGC3_NFC); 	
  SIM_SCGC3 |=   1uL << SCGC3_NFC; 	
  //
  // Enable the port C and D.
  //
  SIM_SCGC5 |= (1uL << SCGC5_PORTB)
            |  (1uL << SCGC5_PORTC)
            |  (1uL << SCGC5_PORTD)
            ;
  //
  // Set the clock divisor.
  //
  SIM_CLKDIV4 |= (NFCDIV_VALUE  << CLKDIV4_NFCDIV)
              |  (NFCFRAC_VALUE << CLKDIV4_NFCFRAC)
              ;
  //
  // Enable the ready/busy signals.
  //
  if (Unit == 0) {
    PORTC_PCR17 = (6uL << PCR_MUX)
                | (1uL << PCR_DSE)    // NFC_CS0
                ;
  } else {
    PORTC_PCR18 = (6uL << PCR_MUX)
                | (1uL << PCR_DSE)
                ;                     // NFC_CS1
  }
  //
  // Enable the control signals.
  //
  PORTC_PCR16 = (6uL << PCR_MUX)
              | (1uL << PCR_DSE)
              ;                       // NFC_RB
  PORTD_PCR8  = (6uL << PCR_MUX)
              | (1uL << PCR_DSE)
              ;                       // NFC_CLE
  PORTD_PCR9  = (6uL << PCR_MUX)
              | (1uL << PCR_DSE)
              ;                       // NFC_ALE
  PORTD_PCR10 = (6uL << PCR_MUX)
              | (1uL << PCR_DSE)
              ;                       // NFC_RE
  PORTC_PCR11 = (5uL << PCR_MUX)
              | (1uL << PCR_DSE)
              ;                       // NFC_WE
  //
  // Enable the data signals. Start with an 8-bit bus.
  // Extend it later to 16-bit if required.
  //
  _InitDataBus8();
  //
  // Stop on write error.
  //
  NFC_CFG = (1uL << CFG_STOPWERR)
          | (1uL << CFG_PAGECNT)
          | (6uL << CFG_TIMEOUT)
          | (1uL << CFG_ECCSRAM)            // ECC correction status is written to internal buffer
          | (ECCAD_VALUE << CFG_ECCAD)
          ;
  //
  // Disable row address increment.
  //
  NFC_RAI  = 0;
  //
  // Disable address swap.
  //
  NFC_SWAP = 0;
  //
  // Clear the command registers.
  //
  NFC_CMD2 = 0;
  NFC_CMD1 = 0;
}

/*********************************************************************
*
*       _SelectDevice
*/
static void _SelectDevice(PHY_INST * pInst) {
  U8 Unit;

  Unit = pInst->Unit;
  if (Unit == 0) {
    NFC_RAR |= (1uL << RAR_CS0)
            |  (1uL << RAR_RB0)
            ;
    NFC_RAR &= ~((1uL << RAR_CS1) |
                 (1uL << RAR_RB1));
  } else {
    NFC_RAR |= (1uL << RAR_CS1)
            |  (1uL << RAR_RB1)
            ;
    NFC_RAR &= ~((1uL << RAR_CS0) |
                 (1uL << RAR_RB0));
  }
}

/*********************************************************************
*
*       _SetECCMode
*/
static void _SetECCMode(U8 Value) {
  NFC_CFG &= ~(CFG_ECC_MODE_MAX << CFG_ECC_MODE);
  NFC_CFG |= Value << CFG_ECC_MODE;
}

/*********************************************************************
*
*       _SetCmdByte1
*/
static void _SetCmdByte1(U8 Value, U8 BufferIndex) {
  U16 Code;

  Code      = 1uL << CODE_SEND_CMD_BYTE1;
  NFC_CMD2 |= (Value << CMD2_BYTE1)
           |  (Code  << CMD2_CODE)
           |  (BufferIndex  << CMD2_BUFNO)
           ;
}

/*********************************************************************
*
*       _SetCmdByte2
*/
static void _SetCmdByte2(U8 Value, U8 BufferIndex) {
  U16 Code;

  Code      = 1uL << CODE_SEND_CMD_BYTE2;
  NFC_CMD2 |= Code          << CMD2_CODE
           |  (BufferIndex  << CMD2_BUFNO)
           ;
  NFC_CMD1 |= Value << CMD1_BYTE2;
}

/*********************************************************************
*
*       _SetRowAddr1
*/
static void _SetRowAddr1(U8 Value) {
  U16 Code;

  Code      = 1uL << CODE_SEND_ROW_ADDR1;
  NFC_CMD2 |= Code << CMD2_CODE;
  NFC_RAR  &= ~(0xFFuL << RAR_BYTE1);
  NFC_RAR  |= (U32)Value << RAR_BYTE1;
}

/*********************************************************************
*
*       _SetRowAddr2
*/
static void _SetRowAddr2(U8 Value) {
  U16 Code;

  Code      = 1uL << CODE_SEND_ROW_ADDR2;
  NFC_CMD2 |= Code << CMD2_CODE;
  NFC_RAR  &= ~(0xFFuL << RAR_BYTE2);
  NFC_RAR  |= (U32)Value << RAR_BYTE2;
}

/*********************************************************************
*
*       _SetRowAddr3
*/
static void _SetRowAddr3(U8 Value) {
  U16 Code;

  Code      = 1uL << CODE_SEND_ROW_ADDR3;
  NFC_CMD2 |= Code << CMD2_CODE;
  NFC_RAR  &= ~(0xFFuL << RAR_BYTE3);
  NFC_RAR  |= (U32)Value << RAR_BYTE3;
}

/*********************************************************************
*
*       _SetColAddr1
*/
static void _SetColAddr1(U8 Value) {
  U16 Code;

  Code      = 1uL << CODE_SEND_COL_ADDR1;
  NFC_CMD2 |= Code << CMD2_CODE;
  NFC_CAR  &= ~(0xFFuL << CAR_BYTE1);
  NFC_CAR  |= (U32)Value << CAR_BYTE1;
}

/*********************************************************************
*
*       _SetColAddr2
*/
static void _SetColAddr2(U8 Value) {
  U16 Code;

  Code      = 1uL << CODE_SEND_COL_ADDR2;
  NFC_CMD2 |= Code << CMD2_CODE;
  NFC_CAR  &= ~(0xFFuL << CAR_BYTE2);
  NFC_CAR  |= (U32)Value << CAR_BYTE2;
}

/*********************************************************************
*
*       _SetStatusRead
*/
static void _SetStatusRead(void) {
  U16 Code;

  Code      = 1uL  << CODE_READ_STATUS;
  NFC_CMD2 |= Code << CMD2_CODE;
}

/*********************************************************************
*
*       _SetBusyWait
*/
static void _SetBusyWait(void) {
  U16 Code;

  Code      = 1uL  << CODE_WAIT_WHILE_BUSY;
  NFC_CMD2 |= Code << CMD2_CODE;
}

/*********************************************************************
*
*       _SetIdRead
*/
static void _SetIdRead(U32 NumBytes) {
  U16 Code;

  Code      = 1uL  << CODE_READ_ID;
  NFC_CMD2 |= Code << CMD2_CODE;
  NFC_CFG  &= ~(CFG_IDCNT_MAX << CFG_IDCNT);
  NFC_CFG  |= NumBytes << CFG_IDCNT;
}

/*********************************************************************
*
*       _SetDataRead
*/
static void _SetDataRead(U32 NumBytes) {
  U16 Code;

  Code       = 1uL  << CODE_READ_DATA;
  NFC_CMD2  |= Code << CMD2_CODE;
  //
  // On a 16-bit data bus the number of bytes transferred must be odd.
  // The controller will read NFC_SECSZ - 1 bytes.
  //
  if (NFC_CFG & (1uL << CFG_BITWIDTH)) {
    ++NumBytes;
  }
  NFC_SECSZ  = NumBytes;
}

/*********************************************************************
*
*       _SetDataWrite
*/
static void _SetDataWrite(U32 NumBytes) {
  U16 Code;

  Code       = 1uL  << CODE_WRITE_DATA;
  NFC_CMD2  |= Code << CMD2_CODE;
  //
  // On a 16-bit data bus the number of bytes transferred must be odd.
  // The controller will write NFC_SECSZ - 1 bytes.
  //
  if (NFC_CFG & (1uL << CFG_BITWIDTH)) {
    ++NumBytes;
  }
  NFC_SECSZ  = NumBytes;
}

/*********************************************************************
*
*       _LoadSwapData8
*
*   Function description
*     Reads data from NFC buffer when the NAND flash is interfaced over a 8-bit bus.
*/
static void _LoadSwapData8(U8 * pData, U32 Off, U32 NumBytes) {
  volatile U32 * pDataSrc;
  U32            NumWords;

  pDataSrc  = &NFC_DATA;
  pDataSrc += Off / 4;
  //
  // Copy 4 bytes at a time if the destination buffer is aligned.
  //
  if ((((U32)pData & 3) == 0) && ((Off & 3) == 0)) {
    U32 * pData32;

    pData32  = (U32 *)pData;
    NumWords = NumBytes >> 2;
    if (NumWords) {
      do {
        //
        // NFC packs together the received bytes as 4 bytes words. The first byte is stored to LSB of word.
        // For example the sequence of received bytes 0x1, 0x2, 0x3, 0x4 is stored as 0x04030201 to NFC buffer.
        // We have to reverse the byte order here as the driver expects an array of bytes.
        //
        *pData32++  = FS_LoadU32BE((U8 *)pDataSrc++);
        NumBytes -= 4;
        pData    += 4;
      } while (--NumWords);
    }
  }
  //
  // Copy the rest of the data byte-wise.
  //
  if (NumBytes) {
    U32 Data32;
    U32 NumLoops;

    Off &= 3;
    do {
      Data32   = FS_LoadU32BE((U8 *)pDataSrc++);
      NumLoops = MIN(NumBytes, 4);
      //
      // Skip bytes if the offset is not aligned.
      //
      if (Off) {
        do {
          Data32 >>= 8;
        } while (--Off);
      }
      do {
        *pData++   = (U8)Data32;
        Data32 >>= 8;
        --NumBytes;
      } while (--NumLoops);
    } while (NumBytes);
  }
}

/*********************************************************************
*
*       _LoadSwapData16
*
*   Function description
*     Reads data from NFC buffer when the NAND flash is interfaced over a 16-bit bus.
*/
static void _LoadSwapData16(U16 * pData, U32 Off, U32 NumBytes) {
  volatile U16 * pDataNFC;
  U32            NumWords;

  pDataNFC = (volatile U16 *)&NFC_DATA;
  pDataNFC += Off / 2;
  NumWords = NumBytes >> 2;
  if (NumWords) {
    do {
      *pData++  = *(pDataNFC + 1);
      *pData++  = *pDataNFC;
      pDataNFC += 2;
      NumBytes -= 4;
    } while (--NumWords);
  }
  if (NumBytes) {
    *pData = *(pDataNFC + 1);
  }
}

/*********************************************************************
*
*       _LoadSwapData
*
*   Function description
*     Reads data from NFC buffer.
*/
static void _LoadSwapData(PHY_INST * pInst, U16 * pData, U32 Off, U32 NumBytes) {
  U8 DataBusWidth;

  DataBusWidth = pInst->DataBusWidth;
  if (DataBusWidth == 16) {
    _LoadSwapData16(pData, Off, NumBytes);
  } else {
    _LoadSwapData8((U8 *)pData, Off, NumBytes);
  }
}

/*********************************************************************
*
*       _StoreSwapData8
*
*   Function description
*     Writes data to NFC buffer when the NAND flash is interfaced over an 8-bit bus.
*/
static void _StoreSwapData8(const U8 * pData, U32 Off, U32 NumBytes) {
  volatile U32 * pDataNFC;

  pDataNFC = &NFC_DATA;
  pDataNFC += Off / 4;
  //
  // Copy 4 bytes at a time if the source buffer is aligned.
  //
  if ((((U32)pData & 3) == 0)  && ((Off & 3) == 0)) {
    const U32 * pData32;
    U32         NumWords;

    pData32  = (U32 *)pData;
    NumWords = NumBytes >> 2;
    if (NumWords) {
      do {
        FS_StoreU32BE((U8 *)pDataNFC++, *pData32++);
        NumBytes -= 4;
        pData    += 4;
      } while (--NumWords);
    }
  }
  //
  // Copy the rest of the data byte-wise.
  //
  if (NumBytes) {
    U32 NewData;
    U32 OldData;
    U32 Mask;
    U32 NumLoops;

    Off &= 3;
    do {
      NumLoops = MIN(NumBytes, 4);
      Mask     = 0xFFFFFFFF;
      NewData  = 0;
      do {
        NewData <<= 8;
        NewData  |= (U32)(*pData);
        --NumBytes;
        ++pData;
        Mask    <<= 8;
      } while (--NumLoops);
      //
      // Move the written bytes to the right position the offset is not aligned.
      //
      if (Off) {
        do {
          NewData <<= 8;
          Mask    <<= 8;
          Mask     |= (U32)0xFF;
        } while (--Off);
      }
      OldData  = FS_LoadU32BE((U8 *)pDataNFC);
      OldData &= Mask;
      NewData |= OldData;
      FS_StoreU32BE((U8 *)pDataNFC++, NewData);
    } while (NumBytes);
  }
}

/*********************************************************************
*
*       _StoreSwapData16
*
*   Function description
*     Writes data to NFC buffer when the NAND flash is interfaced over a 16-bit bus.
*/
static void _StoreSwapData16(const U16 * pData, U32 Off, U32 NumBytes) {
  volatile U16 * pDataNFC;
  U32            NumWords;

  pDataNFC = (volatile U16 *)&NFC_DATA;
  pDataNFC += Off / 2;
  NumWords = NumBytes >> 2;
  if (NumWords) {
    do {
      *pDataNFC++ = *(pData + 1);
      *pDataNFC++ = *pData;
      pData    += 2;
      NumBytes -= 4;
    } while (--NumWords);
  }
  if (NumBytes) {
    ++pDataNFC;
    *pDataNFC++ = *pData;
  }
}

/*********************************************************************
*
*       _StoreSwapData
*
*   Function description
*     Writes data to NFC buffer.
*/
static void _StoreSwapData(PHY_INST * pInst, const U16 * pData, U32 Off, U32 NumBytes) {
  U8 DataBusWidth;

  DataBusWidth = pInst->DataBusWidth;
  if (DataBusWidth == 16) {
    _StoreSwapData16(pData, Off, NumBytes);
  } else {
    _StoreSwapData8((U8 *)pData, Off, NumBytes);
  }
}

/*********************************************************************
*
*       _LoadData
*
*   Function description
*     Reads data from NFC buffer.
*/
static void _LoadData(void * pData, U32 Off, U32 NumBytes) {
  U8 * p;

  p  = (U8 *)&NFC_DATA;
  p += Off;
  FS_MEMCPY(pData, p, NumBytes);
}

/*********************************************************************
*
*       _StoreData
*
*   Function description
*     Writes data to NFC buffer in little-endian format.
*/
static void _StoreData(const void * pData, U32 Off, U32 NumBytes) {
  U8 * p;

  p  = (U8 *)&NFC_DATA;
  p += Off;
  FS_MEMCPY(p, pData, NumBytes);
}

/*********************************************************************
*
*       _StoreU16
*
*   Function description
*     Writes a 16-bit value to NFC buffer at a specified offset.
*     The offset must be word aligned.
*/
static void _StoreU16(const PHY_INST * pInst, U16 Data, U16 Off) {
  U8 DataBusWidth;
  U8 * p;

  p  = (U8 *)&NFC_DATA;
  p += Off;
  DataBusWidth = pInst->DataBusWidth;
  if (DataBusWidth == 16) {
    U16 * pData16;

    pData16  = (U16 *)p;
    *pData16 = Data;
  } else {
    FS_StoreU16BE(p, Data);
  }
}

/*********************************************************************
*
*       _DataIsBlank
*
*   Function description
*     Checks whether a specified number of bytes from the NFC buffer are set to 0xFF.
*/
static int _DataIsBlank(U32 NumBytes) {
  int r;
  volatile U32 * pDataNFC;
  U32            NumWords;

  r = 1;    // Data is blank.
  pDataNFC = (volatile U32 *)&NFC_DATA;
  NumWords = NumBytes >> 2;
  if (NumWords) {
    do {
      if (*pDataNFC++ != 0xFFFFFFFFuL) {
        r = 0;
        break;     // Data area not blank.
      }
    } while (--NumWords);
  }
  return r;
}

/*********************************************************************
*
*       _ExecCmd
*/
static void _ExecCmd(void) {
  U16 Code;

  NFC_ISR  |= (1uL << ISR_IDLECLR)
           |  (1uL << ISR_DONECLR)
           |  (1uL << ISR_WERRCLR)
           ;
  Code      = 1uL << CODE_END_OF_CMD;
  NFC_CMD2 |= (Code << CMD2_CODE)
           |  (1uL  << CMD2_BUSY_START)
           ;
  while ((NFC_ISR & (1uL << ISR_IDLE)) == 0) {
    ;
  }
  NFC_CMD1 = 0;
  NFC_CMD2 = 0;
}

/*********************************************************************
*
*       _SetRowAddr
*/
static void _SetRowAddr(PHY_INST * pInst, U32 RowAddr) {
  U8 NumBytes;

  NumBytes = pInst->NumBytesRowAddr;
  _SetRowAddr1(RowAddr);
  if (NumBytes > 1) {
    RowAddr >>= 8;
    _SetRowAddr2(RowAddr);
  }
  if (NumBytes > 2) {
    RowAddr >>= 8;
    _SetRowAddr3(RowAddr);
  }
}

/*********************************************************************
*
*       _SetColAddr
*/
static void _SetColAddr(PHY_INST * pInst, U32 ColAddr) {
  U8 NumBytes;
  U8 DataBusWidth;

  DataBusWidth = pInst->DataBusWidth;
  if (DataBusWidth == 16) {
    ColAddr >>= 1;        // Convert to a 16-bit word address.
  }
  NumBytes = pInst->NumBytesColAddr;
  _SetColAddr1(ColAddr);
  if (NumBytes > 1) {
    ColAddr >>= 8;
    _SetColAddr2(ColAddr);
  }
}

/*********************************************************************
*
*       _SetRowColAddr
*/
static void _SetRowColAddr(PHY_INST * pInst, U32 RowAddr, U32 ColAddr) {
  _SetRowAddr(pInst, RowAddr);
  _SetColAddr(pInst, ColAddr);
}

/*********************************************************************
*
*       _ReadStatus
*
*   Function description
*     Reads and returns the contents of the status register.
*/
static U8 _ReadStatus(void) {
  U8 Status;

  _SetCmdByte1(CMD_READ_STATUS, 0);
  _SetStatusRead();
  _ExecCmd();
  Status = (U8)NFC_SR2;
  return Status;
}

/*********************************************************************
*
*       _WaitForDeviceReady
*
*   Function description
*     Waits for the NAND to complete its last operation.
*
*   Parameters
*     Unit    Number of NAND unit
*
*   Return value
*    ==0  Success
*    !=0  An error has occurred
*/
static int _WaitForDeviceReady(void) {
  U8 Status;

  _SetBusyWait();
  _ExecCmd();
  do {
    Status = _ReadStatus();
  } while ((Status & STATUS_READY) == 0);
  return (Status & STATUS_ERROR) ? 1 : 0;
}

/*********************************************************************
*
*       _Reset
*
*   Function description
*     Resets the NAND flash by command
*/
static void _Reset(void) {
  _SetCmdByte1(CMD_RESET, 0);
  _ExecCmd();
  _WaitForDeviceReady();
}

/*********************************************************************
*
*       _ReadId
*
*   Function description
*     Reads a specified number of Id bytes.
*/
static void _ReadId(U8 Addr, U8 * pData, U32 NumBytes) {
  _SetCmdByte1(CMD_READ_ID, 0);
  _SetRowAddr1(Addr);
  _SetIdRead(NumBytes);
  _ExecCmd();
  *pData++ = (U8)(NFC_SR1 >> SR1_ID1);
  if (NumBytes > 1) {
    *pData++ = (U8)(NFC_SR1 >> SR1_ID2);
  }
  if (NumBytes > 2) {
    *pData++ = (U8)(NFC_SR1 >> SR1_ID3);
  }
  if (NumBytes > 3) {
    *pData++ = (U8)(NFC_SR1 >> SR1_ID4);
  }
  if (NumBytes > 4) {
    *pData++ = (U8)(NFC_SR2 >> SR2_ID5);
  }
}

/*********************************************************************
*
*       _IsONFISupported
*
*   Function description
*     Checks if the device supports ONFI.
*     An ONFI compatible device returns "ONFI" ASCII string
*     when executing a READ ID operation from address 0x20
*
*   Return value
*     ==0    ONFI not supported
*     !=0    ONFI supported
*/
static int _IsONFISupported(void) {
  int r;
  U8  aId[4];

  r = 0;              // ONFI is not supported so far
  FS_MEMSET(aId, 0, sizeof(aId));
  _ReadId(READ_ID_ONFI, aId, sizeof(aId));
  if ((aId[0] == 'O') &&
      (aId[1] == 'N') &&
      (aId[2] == 'F') &&
      (aId[3] == 'I')) {
    r = 1;
  }
  return r;
}

/*********************************************************************
*
*       _ReadONFIPara
*
*   Function description
*     Reads the ONFI parameter page.
*     A page has 256 bytes. The integrity of information is checked using CRC.
*
*   Parameters
*     pInst   [IN]  Instance of physical layer
*             [OUT] ---
*     pPara   [IN]  ---
*             [OUT] Information read from the parameter page. Must be at least 256 bytes long.
*
*   Return value
*     ==0    ONFI parameters read
*     !=0    An error occurred
*/
static int _ReadONFIPara(PHY_INST * pInst, void * pPara) {
  int   r;
  U16   crcRead;
  U16   crcCalc;
  U8  * p;
  U32   NumBytesPara;
  int   i;

  r = 1;        // No parameter page found, yet.
  _SetCmdByte1(CMD_READ_PARA_PAGE, 0);
  _SetRowAddr1(0);
  _ExecCmd();
  r = _WaitForDeviceReady();
  if (r == 0) {
    _SetCmdByte1(CMD_READ_1, 0);
    _ExecCmd();                   // Switch back to read mode. _WaitForReady() function changed it to status mode.
  }
  if (r == 0) {
    p = (U8 *)pPara;
    //
    // Several identical parameter pages are stored in a device.
    // Read from the first one which stores valid information.
    //
    for (i = 0; i < NUM_PARA_PAGES; ++i) {
      _SetDataRead(PARA_PAGE_SIZE);
      _ExecCmd();
      _LoadSwapData(pInst, (U16 *)pPara, 0, PARA_PAGE_SIZE);
      NumBytesPara = PARA_PAGE_SIZE - sizeof(crcRead);    // CRC is stored on the last 2 bytes of the parameter page.
      //
      // Validate the parameters by checking the CRC.
      //
      crcRead = FS_LoadU16LE(&p[NumBytesPara]);
      crcCalc = FS_CRC16_CalcBitByBit(p, NumBytesPara, PARA_CRC_INIT, PARA_CRC_POLY);
      if (crcRead != crcCalc) {
        continue;
      }
      //
      // CRC is valid, now check the signature.
      //
      if ((p[0] == 'O') &&
          (p[1] == 'N') &&
          (p[2] == 'F') &&
          (p[3] == 'I')) {
        r = 0;
        break;                      // Found a valid parameter page.
      }
    }
  }
  return r;
}

/*********************************************************************
*
*       _GetFeatures
*
*   Function description
*     Reads the device settings.
*
*   Parameters
*     Unit    Device unit number
*     Addr    Feature address (see device datasheet)
*     pData   [IN]  ---
*             [OUT] Read device settings. Must be at least 4 bytes large.
*
*   Return value
*     ==0    Settings read
*     !=0    An error occurred
*/
static int _GetFeatures(PHY_INST * pInst, U8 Addr, U8 * pData) {
  int r;
  U16 aBuffer[NUM_FEATURE_PARA];
  U32 NumBytes;
  U8  DataBusWidth;

  DataBusWidth = pInst->DataBusWidth;
  NumBytes     = NUM_FEATURE_PARA;
  //
  // 4 read cycles must be executed to get the feature parameters.
  // On a 16-bit bus the data is returned on the low-order bits.
  // 2 times more bytes are required to be read in this case.
  //
  if (DataBusWidth == 16) {
    NumBytes <<= 1;
  }
  _SetCmdByte1(CMD_GET_FEATURES, 0);
  _SetRowAddr1(Addr);
  _ExecCmd();
  r = _WaitForDeviceReady();
  if (r == 0) {
    U8  * pData8;
    U16 * pData16;
    U32   NumLoops;

    //
    // Revert to read mode. _WaitForDeviceReady() change it to status mode.
    //
    _SetCmdByte1(CMD_READ_1, 0);
    _SetDataRead(NumBytes);
    _ExecCmd();

    _LoadSwapData(pInst, aBuffer, 0, NumBytes);
    pData8   = (U8 *)aBuffer;
    pData16  = aBuffer;
    NumLoops = NumBytes;
    if (DataBusWidth == 16) {
      NumLoops >>= 1;
    }
    do {
      if (DataBusWidth == 16) {
        *pData++  = (U8)*pData16++;
      } else {
        *pData++ = *pData8++;
      }
    } while (--NumLoops);
  }
  if (r) {
    _Reset();
  }
  return r;
}

/*********************************************************************
*
*       _SetFeatures
*
*   Function description
*     Modifies the device settings.
*
*   Parameters
*     Addr    Feature address (see device datasheet)
*     pData   [IN]  New device settings.  Must be at least 4 bytes long.
*             [OUT] ---
*
*   Return value
*     ==0    Settings written
*     !=0    An error occurred
*/
static int _SetFeatures(PHY_INST * pInst, U8 Addr, const U8 * pData) {
  int   r;
  U16   aBuffer[NUM_FEATURE_PARA];
  U32   NumBytes;
  U8    DataBusWidth;
  U8  * pData8;
  U16 * pData16;
  U32   NumLoops;

  DataBusWidth = pInst->DataBusWidth;
  NumBytes     = NUM_FEATURE_PARA;
  //
  // 4 write cycles must be executed to set the feature parameters.
  // On a 16-bit bus the data is sent on the low-order bits.
  // 2 times more bytes are required to be written in this case.
  //
  if (DataBusWidth == 16) {
    NumBytes <<= 1;
  }
  //
  // Copy data to NFC buffer.
  //
  pData8   = (U8 *)aBuffer;
  pData16  = aBuffer;
  NumLoops = NumBytes;
  if (DataBusWidth == 16) {
    NumLoops >>= 1;
  }
  do {
    if (DataBusWidth == 16) {
      *pData16++  = (U16)*pData++;
    } else {
      *pData8++   = *pData++;
    }
  } while (--NumLoops);
  //
  // Prepare the command and execute it.
  //
  _SetCmdByte1(CMD_SET_FEATURES, 0);
  _SetRowAddr1(Addr);
  _SetDataWrite(NumBytes);
  _StoreSwapData(pInst, aBuffer, 0, NumBytes);
  _ExecCmd();
  //
  // Wait for NAND flash to store the parameters.
  //
  r = _WaitForDeviceReady();
  if (r) {
    _Reset();
  }
  return r;
}

/*********************************************************************
*
*       _GetNumBytesECC
*
*   Function description
*     Returns the size of ECC in bytes.
*
*/
static U32 _GetNumBytesECC(PHY_INST * pInst) {
  U32 NumBytes;
  U8  ECCMode;

  NumBytes = 0;
  ECCMode  = pInst->ECCMode;
  switch (ECCMode) {
  default:
  case CFG_ECC_MODE_NONE:
    NumBytes = 0;
    break;
  case CFG_ECC_MODE_4BIT:
    NumBytes = 8;
    break;
  case CFG_ECC_MODE_6BIT:
    NumBytes = 12;
    break;
  case CFG_ECC_MODE_8BIT:
    NumBytes = 15;
    break;
  case CFG_ECC_MODE_12BIT:
    NumBytes = 23;
    break;
  case CFG_ECC_MODE_16BIT:
    NumBytes = 30;
    break;
  case CFG_ECC_MODE_24BIT:
    NumBytes = 45;
    break;
  case CFG_ECC_MODE_32BIT:
    NumBytes = 60;
    break;
  }
  return NumBytes;
}

/*********************************************************************
*
*       Public functions
*
**********************************************************************
*/

/*********************************************************************
*
*       _Read
*
*   Function description
*     Reads data from a complete or a part of a page.
*     This code is identical for main memory and spare area; the spare area
*     is located rigth after the main area.
*
*   Return value:
*     ==0     Data successfully transferred.
*     !=0     An error has occurred.
*/
static int _Read(U8 Unit, U32 PageNo, void * pData, unsigned Off, unsigned NumBytes) {
  PHY_INST * pInst;
  int r;

  pInst = &_aInst[Unit];
  ASSERT_PARA_IS_ALIGNED(pInst, NumBytes | Off | (U32)pData);
  _SelectDevice(pInst);
  _SetCmdByte1(CMD_READ_1, 0);
  _SetRowColAddr(pInst, PageNo, Off);
  _SetCmdByte2(CMD_READ_2, 0);
  _ExecCmd();
  r = _WaitForDeviceReady();
  if (r == 0) {
    _SetCmdByte1(CMD_READ_1, 0);
    _SetDataRead(NumBytes);
    _ExecCmd();
    _LoadSwapData(pInst, (U16 *)pData, 0, NumBytes);
  }
  if (r) {
    _Reset();
  }
  return r;
}

/*********************************************************************
*
*       _ReadEx
*
*   Function description
*     Reads data from two locations on a page.
*     Typically used to read data and spare area at once.
*
*   Return value:
*     ==0     Data successfully transferred.
*     !=0     An error has occurred.
*/
static int _ReadEx(U8 Unit, U32 PageNo, void * pData, unsigned Off, unsigned NumBytes, void * pSpare, unsigned OffSpare, unsigned NumBytesSpare) {
  PHY_INST * pInst;
  int        r;
  U8       * pData8;
  U8       * pSpare8;

  pInst = &_aInst[Unit];
  ASSERT_PARA_IS_ALIGNED(pInst, NumBytes | Off | (U32)pSpare | OffSpare | NumBytesSpare);
  _SelectDevice(pInst);
  //
  // Read the requested page from the memory array to data register of NAND flash.
  //
  _SetCmdByte1(CMD_READ_1, 0);
  _SetRowColAddr(pInst, PageNo, Off);
  _SetCmdByte2(CMD_READ_2, 0);
  _ExecCmd();
  r = _WaitForDeviceReady();
  //
  // OK, page data (main and spare area) is in the data register of NAND flash.
  //
  if (r == 0) {
    if (pInst->ECCIsEnabled) {
      U32   NumBytesToRead;
      U32   BytesPerVPage;
      U32   BytesPerVSpareArea;
      U32   NumVPages;
      U32   VPageOff;
      U8    ECCStatus;
      U32   iVPage;
      U16   Data16;
      U16   Spare16;
      U16 * pData16;
      U16 * pSpare16;

      ASSERT_READ_IS_COMPLETE(pInst, Off, NumBytes, OffSpare, NumBytesSpare);
      NumVPages          = 1uL << (pInst->ldBytesPerPage - pInst->ldBytesPerVPage);
      BytesPerVPage      = 1uL << pInst->ldBytesPerVPage;
      BytesPerVSpareArea = pInst->BytesPerVSpareArea;
      NumBytesToRead     = BytesPerVPage + BytesPerVSpareArea;
      pData8             = (U8 *)pData;
      pSpare8            = (U8 *)pSpare;
      //
      // Read one virtual page at a time. NFC performs the bit correction using ECC.
      //
      for (iVPage = 0; iVPage < NumVPages; ++iVPage) {
        VPageOff = 0;
        //
        // Copy the virtual page from the data register of NAND flash to host.
        //
        _SetCmdByte1(CMD_READ_1, 0);
        _SetDataRead(NumBytesToRead);
        _ExecCmd();
        ECCStatus = ECC_STATUS;
        if (ECCStatus & (1uL << ECC_STATUS_CORFAIL)) {
          U32 NumBytesToCheck;
          U32 NumBytesECC;


          NumBytesECC     = _GetNumBytesECC(pInst);
          NumBytesToCheck = NumBytesToRead - NumBytesECC;
          //
          // It seems that the ECC of a blank page is not equal to all 0xFFs amd this leads to ECC errors when reading a blank page.
          //
          if (_DataIsBlank(NumBytesToCheck)) {
            //
            // Do not read from NFC buffer. Fill the buffers for the main and sparea are with 0xFFs.
            //
            FS_MEMSET(pData,  0xFF, NumBytes);
            FS_MEMSET(pSpare, 0xFF, NumBytesSpare);
          } else {
            r = 1;        // Error, uncorrectable bit error occurred.
          }
          break;
        }
        //
        // Copy the main data from the NFC buffer to driver buffer.
        //
        _LoadData(pData8, VPageOff, BytesPerVPage);
        VPageOff += BytesPerVPage;
        pData8   += BytesPerVPage;
        //
        // Copy the spare data from the NFC buffer to driver buffer.
        //
        _LoadData(pSpare8, VPageOff, BytesPerVSpareArea);
        pSpare8 += BytesPerVSpareArea;
      }
      //
      // Move the bad block status from the data area to the spare area.
      //
      pSpare8   = (U8 *)pSpare;
      pSpare8  += pInst->SwapSpareAreaOff;
      pData8    = (U8 *)pData;
      pData8   += pInst->SwapPageOff;
      Spare16   = FS_LoadU16BE(pSpare8);
      Data16    = FS_LoadU16BE(pData8);
      pSpare16  = (U16 *)pSpare8;
      *pSpare16 = Data16;
      pData16   = (U16 *)pData8;
      *pData16  = Spare16;
    } else {
      U32 NumBytesAtOnce;

    //
      // Copy the data of main area from the data register of NAND flash to host.
    //
      _SetCmdByte1(CMD_READ_1, 0);
      pData8 = (U8 *)pData;
      do {
        NumBytesAtOnce = MIN(NumBytes, BUFFER_SIZE);
        _SetDataRead(NumBytesAtOnce);
    _ExecCmd();
        _LoadData((U16 *)pData8, 0, NumBytesAtOnce);
        NumBytes -= NumBytesAtOnce;
        pData8   += NumBytesAtOnce;
      } while (NumBytes);
    //
      // Copy the data of spare area from the data register of NAND flash to host.
    //
      _SetCmdByte1(CMD_READ_1, 0);
    _SetColAddr(pInst, OffSpare);
      _SetCmdByte2(CMD_READ_2, 0);
    _SetDataRead(NumBytesSpare);
    _ExecCmd();
      _LoadData(pSpare, 0, NumBytesSpare);
    }
  }
  if (r) {
    _Reset();
  }
  return r;
}

/*********************************************************************
*
*       _Write
*
*   Function description
*     Writes data into a complete or a part of a page.
*     This code is identical for main memory and spare area; the spare area
*     is located rigth after the main area.
*
*   Return value:
*     ==0     Data successfully transferred.
*     !=0     An error has occurred.
*/
static int _Write(U8 Unit, U32 PageNo, const void * pData, unsigned Off, unsigned NumBytes) {
  PHY_INST * pInst;
  int        r;

  pInst = &_aInst[Unit];
  ASSERT_PARA_IS_ALIGNED(pInst, NumBytes | Off | (U32)pData);
  _SelectDevice(pInst);
  //
  // Set the address of the first byte to write.
  //
  _SetCmdByte1(CMD_WRITE_1, 0);
  _SetRowColAddr(pInst, PageNo, Off);
  //
  // Store to NFC buffer the data to write.
  //
  _SetDataWrite(NumBytes);
  _StoreSwapData(pInst, (U16 *)pData, 0, NumBytes);
  _SetCmdByte2(CMD_WRITE_2, 0);
  //
  // Write the data to NAND flash and wait for completion.
  //
  _ExecCmd();
  r = _WaitForDeviceReady();
  if (r) {
    _Reset();
  }
  return r;
}

/*********************************************************************
*
*       _WriteEx
*
*   Function description
*     Writes data to 2 parts of a page.
*     Typically used to write data and spare area at the same time.
*
*  Return value:
*     ==0     Data successfully transferred.
*     !=0     An error has occurred.
*/
static int _WriteEx(U8 Unit, U32 PageNo, const void * pData, unsigned Off, unsigned NumBytes, const void * pSpare, unsigned OffSpare, unsigned NumBytesSpare) {
  PHY_INST * pInst;
  int        r;

  pInst = &_aInst[Unit];
  ASSERT_PARA_IS_ALIGNED(pInst, NumBytes | Off | (U32)pData);
  _SelectDevice(pInst);
  if (pInst->ECCIsEnabled) {
    U32   NumVPages;
    U32   NumBytesVPage;
    U32   NumBytesVSpareArea;
    U32   VPageOff;
    U32   NumBytesToWrite;
    U8  * pData8;
    U8  * pSpare8;
    U32   iVPage;
    U16   Data16;
    U8  * pSwapSrc;
    U32   SwapDestOff;

    ASSERT_WRITE_IS_COMPLETE(pInst, Off, NumBytes, OffSpare, NumBytesSpare);
    NumVPages          = 1uL << (pInst->ldBytesPerPage - pInst->ldBytesPerVPage);
    NumBytesVPage      = 1uL << pInst->ldBytesPerVPage;
    NumBytesVSpareArea = pInst->BytesPerVSpareArea;
    NumBytesToWrite    = NumBytesVPage + NumBytesVSpareArea;
    VPageOff           = 0;
    pData8             = (U8 *)pData;
    pSpare8            = (U8 *)pSpare;
    //
    // Set the address of the first byte in the main area to write.
    //
    _SetCmdByte1(CMD_WRITE_1, 0);
    _SetRowColAddr(pInst, PageNo, VPageOff);
    //
    // Write one virtual page at a time. A virtual page contains main data followed by spare data.
    // NFC stores the ECC to the last bytes of the spare area.
    //
    for (iVPage = 0; iVPage < NumVPages; ++iVPage) {
      VPageOff = 0;
      //
      // Store to NFC buffer the data of the main area.
      //
      _StoreData(pData8, VPageOff, NumBytesVPage);
      //
      // Store 2 bytes of bad block status to data area of the last virtual page
      // which corresponds to the offset of bad block status in the "real" spare area.
      //
      if (iVPage == (NumVPages - 1)) {
        pSwapSrc     = (U8 *)pSpare;
        pSwapSrc    += pInst->SwapSpareAreaOff;
        SwapDestOff  = pInst->SwapVPageOff;
        Data16       = *(U16 *)pSwapSrc;
        _StoreU16(pInst, Data16, SwapDestOff);          // Write to main area in the NFC buffer.
      }
      VPageOff += NumBytesVPage;
      pData8   += NumBytesVPage;
      //
      // Store to NFC buffer the data of the spare area.
      //
      _StoreData(pSpare8, VPageOff, NumBytesVSpareArea);
      //
      // Store 2 bytes of data area from the last virtual page to the place
      // in the spare area where the bad block status is stored.
      //
      if (iVPage == 0) {
        pSwapSrc     = (U8 *)pData;
        pSwapSrc    += pInst->SwapPageOff;
        Data16       = *(U16 *)pSwapSrc;
        SwapDestOff  = pInst->SwapVSpareAreaOff;
        _StoreU16(pInst, Data16, SwapDestOff);          // Write to spare area in the NFC buffer.
      }
      pSpare8  += NumBytesVSpareArea;
      //
      // Transfer data to data register of NAND flash.
      //
      _SetDataWrite(NumBytesToWrite);
      _ExecCmd();
    }
    _SetCmdByte2(CMD_WRITE_2, 0);
    //
    // Ask the NAND flash to program the data from the data register to memory array.
    //
    _ExecCmd();
    r = _WaitForDeviceReady();
  } else {
  //
  // Set the address of the first byte in the main area to write.
  //
    _SetCmdByte1(CMD_WRITE_1, 0);
  _SetRowColAddr(pInst, PageNo, Off);
  //
  // Store to NFC buffer the data to be written to main area of the page.
  //
  _SetDataWrite(NumBytes);
    _StoreSwapData(pInst, (U16 *)pData, 0, NumBytes);
  //
  // Transfer data to data register of NAND flash.
  //
  _ExecCmd();
  //
    // OK, main area data stored to NAND flash data register. Set the address of the first byte in the spare area to write.
  //
    _SetCmdByte1(CMD_RANDOM_WRITE, 0);
  _SetColAddr(pInst, OffSpare);
  //
  // Store to NFC buffer the data to be written to spare area of the page.
  //
  _SetDataWrite(NumBytesSpare);
    _StoreSwapData(pInst, (U16 *)pSpare, 0, NumBytesSpare);
    _SetCmdByte2(CMD_WRITE_2, 0);
  //
  // Write the data to spare area and wait for completion.
  //
  _ExecCmd();
  r = _WaitForDeviceReady();
  }
  if (r) {
    _Reset();
  }
  return r;
}

/*********************************************************************
*
*       _EraseBlock
*
*   Function description
*     Sets all the bytes in a block to 0xFF
*
*   Return value:
*     ==0     Data successfully transferred.
*     !=0     An error has occurred.
*/
static int _EraseBlock(U8 Unit, U32 BlockNo) {
  PHY_INST * pInst;
  int r;

  pInst = &_aInst[Unit];
  _SelectDevice(pInst);
  _SetCmdByte1(CMD_ERASE_1, 0);
  _SetRowAddr(pInst, BlockNo);
  _SetCmdByte2(CMD_ERASE_2, 0);
  _ExecCmd();
  r = _WaitForDeviceReady();
  if (r) {
    _Reset();
  }
  return r;
}

/*********************************************************************
*
*       _InitGetDeviceInfo
*
*  Function description
*    Initializes hardware layer, resets NAND flash and tries to identify the NAND flash.
*    If the NAND flash can be handled, Device Info is filled.
*
*  Return value:
*      0    O.K., device can be handled
*      1    Error: device can not be handled
*
*  Notes
*       (1) The first command to be issued after power-on is RESET (see [2])
*/
static int _InitGetDeviceInfo(U8 Unit, FS_NAND_DEVICE_INFO * pDevInfo) {
  U8 * pBuffer;
  int  r;
  PHY_INST * pInst;

  r = 1;
  //
  // Initialize hardware and reset the device
  //
   pInst = &_aInst[Unit];
   pInst->Unit         = Unit;
   pInst->DataBusWidth = 8;
  _InitNFC(pInst);
  _SelectDevice(pInst);
  _Reset();                   // Note 1
  if (_IsONFISupported()) {
    //
    // Device supports ONFI, go ahead and read parameters.
    // We use a temporarily sector buffer for reading.
    // A sector buffer is minimum 512 bytes, so large enough to hold all the parameters.
    //
    pBuffer = FS__AllocSectorBuffer();
    r = _ReadONFIPara(pInst, pBuffer);
    if (r == 0) {
      U32 BytesPerPage;
      U32 PagesPerBlock;
      U32 NumBlocks;
      U16 Features;
      U8  NumAddrBytes;
      U16 BytesPerSpareArea;

      //
      // Decode the ONFI parameters required by the driver.
      //
      Features      = FS_LoadU16LE(&pBuffer[6]);
      BytesPerPage  = FS_LoadU32LE(&pBuffer[80]);
      BytesPerSpareArea = FS_LoadU16LE(&pBuffer[84]);
      PagesPerBlock = FS_LoadU32LE(&pBuffer[92]);
      NumBlocks     = FS_LoadU32LE(&pBuffer[96]);
      NumAddrBytes  = pBuffer[101];
      //
      // Store the information needed by the physical layer at runtime.
      //
      if (Features & 1) {
        pInst->DataBusWidth = 16;
        _InitDataBus16();
      }
      pInst->NumBytesColAddr = NumAddrBytes >> 4;
      pInst->NumBytesRowAddr = NumAddrBytes & 0x0F;
      //
      // Fill in the info needed by the NAND driver.
      //
      pDevInfo->BPP_Shift    = (U8)_ld(BytesPerPage);
      pDevInfo->PPB_Shift    = (U8)_ld(PagesPerBlock);
      pDevInfo->NumBlocks    = (U16)NumBlocks;
      pDevInfo->BytesPerSpareArea = BytesPerSpareArea;
      //
      // Store locally the required information.
      //
      pInst->ldBytesPerPage    = _ld(BytesPerPage);
      pInst->BytesPerSpareArea = BytesPerSpareArea;
    }
    FS__FreeSectorBuffer(pBuffer);
  }
  return r;
}

/*********************************************************************
*
*       _IsWP
*
*   Function description
*     Checks if the device is write protected.
*     This is done by reading bit 7 of the status register.
*     Typical reason for write protection is that either the supply voltage is too low
*     or the /WP-pin is active (low)
*
*   Return value:
*    ==0    Not write protected
*     >0    Write protected
*/
static int _IsWP(U8 Unit) {
  U8 Status;
  PHY_INST * pInst;

  pInst = &_aInst[Unit];
  _SelectDevice(pInst);
  Status = _ReadStatus();
  return (Status & STATUS_WRITE_PROTECTED) ? 0 : 1;
}

/*********************************************************************
*
*       _EnableECC
*
*   Function description
*     Activates the internal ECC engine of NAND flash.
*
*   Return value
*     ==0     ECC compuation activated
*     !=0     An error occurred
*/
static int _EnableECC(U8 Unit) {
  int        r;
  PHY_INST * pInst;

  r     = 0;
  pInst = &_aInst[Unit];
  if (pInst->NumBitsCorrectable == 0) {     // HW ECC of NAND flash device
  U8         aPara[4];

  _SelectDevice(pInst);
  r = _GetFeatures(pInst, MICRON_ECC_FEATURE_ADDR, aPara);     // Note 1
  if (r == 0) {
    aPara[0] |= MICRON_ECC_FEATURE_MASK;
    r = _SetFeatures(pInst, MICRON_ECC_FEATURE_ADDR, aPara);
  }
  if (r) {
    _Reset();
  }
  } else {
    _SetECCMode(pInst->ECCMode);
    NFC_CFG |= 1uL << CFG_ECCSRAM;
    pInst->ECCIsEnabled = 1;
  }
  return r;
}

/*********************************************************************
*
*       _DisableECC
*
*   Function description
*     Deactivates the internal ECC engine of NAND flash.
*
*   Return value
*     ==0     ECC computation deactivated
*     !=0     An error occurred
*
*   Note
*       (1) A read-modify-write operation is required since more than one feature is stored in a parameter.
*/
static int _DisableECC(U8 Unit) {
  int        r;
  PHY_INST * pInst;

  r     = 0;
  pInst = &_aInst[Unit];
  if (pInst->NumBitsCorrectable == 0) {     // HW ECC of NAND flash device
  U8         aPara[4];

  _SelectDevice(pInst);
  r = _GetFeatures(pInst, MICRON_ECC_FEATURE_ADDR, aPara);    // Note 1
  if (r == 0) {
    aPara[0] &= ~MICRON_ECC_FEATURE_MASK;
    r = _SetFeatures(pInst, MICRON_ECC_FEATURE_ADDR, aPara);
  }
  if (r) {
    _Reset();
  }
  } else {
    FS_USE_PARA(Unit);
    _SetECCMode(CFG_ECC_MODE_NONE);
    NFC_CFG &= ~(1uL << CFG_ECCSRAM);
    pInst->ECCIsEnabled = 0;
  }
  return r;
}

/*********************************************************************
*
*       _ConfigureECC
*
*   Function description
*     Sets the number of bits the HW ECC should correct.
*
*   Parameters
*     Unit                Index of the physical layer
*     NumBitsCorrectable  Number of bits the ECC should be able to correct
*     BytesPerECCBlock    Number of bytes in the page covered by a single ECC
*
*   Return value
*     ==0     Number of bits set
*     !=0     Number of correctable bits not supported
*
*/
static int _ConfigureECC(U8 Unit, U8 NumBitsCorrectable, U16 BytesPerECCBlock) {
  int        r;
  U32        ECCMode;
  PHY_INST * pInst;

  r       = 0;
  pInst   = &_aInst[Unit];
  //
  // Select an ECC mode according to the number of bits which must be corrected.
  //
  ECCMode = CFG_ECC_MODE_NONE;
  switch (NumBitsCorrectable) {
  case 0:
    ECCMode = CFG_ECC_MODE_NONE;
    break;
  case 4:
    ECCMode = CFG_ECC_MODE_4BIT;
    break;
  case 6:
    ECCMode = CFG_ECC_MODE_6BIT;
    break;
  case 8:
    ECCMode = CFG_ECC_MODE_8BIT;
    break;
  case 12:
    ECCMode = CFG_ECC_MODE_12BIT;
    break;
  case 16:
    ECCMode = CFG_ECC_MODE_16BIT;
    break;
  case 24:
    ECCMode = CFG_ECC_MODE_24BIT;
    break;
  case 32:
    ECCMode = CFG_ECC_MODE_32BIT;
    break;
  default:
    r = 1;              // ECC correction level not supported.
    break;
  }
  if (r == 0) {
    U16 BytesPerVSpareArea;
    U16 ECCBlocksPerPage;
    U16 BytesPerSpareArea;
    U16 ldBytesPerVPage;
    U16 SwapPageOff;
    U16 SwapSpareAreaOff;
    U16 SwapVPageOff;
    U16 SwapVSpareAreaOff;

    _SetECCMode(ECCMode);
    //
    // Compute the number of bytes in the spare area of a virtual page.
    //
    ldBytesPerVPage    = _ld(BytesPerECCBlock);
    BytesPerSpareArea  = pInst->BytesPerSpareArea;
    ECCBlocksPerPage   = 1u << (pInst->ldBytesPerPage - ldBytesPerVPage);
    BytesPerVSpareArea = BytesPerSpareArea / ECCBlocksPerPage;
    //
    // Compute the byte offset in the last virtual page of the bad block marker as specified by ONFI.
    //
    SwapVPageOff       = (BytesPerECCBlock + BytesPerVSpareArea) - BytesPerSpareArea;
    //
    // Compute the byte offset in virtual page of the bad block marker.
    //
    SwapVSpareAreaOff  = BytesPerECCBlock + SPARE_OFF_BLOCK_STAT;
    //
    // Compute the byte offset in the driver buffer of the word replaced by the bad block marker in the NFC buffer.
    // The value to be swapped is located in the last ECC block.
    //
    SwapPageOff        = BytesPerECCBlock * (ECCBlocksPerPage - 1) + SwapVPageOff;
    SwapSpareAreaOff   = SPARE_OFF_BLOCK_STAT;
    //
    // Take into account that the NFC reads/writes 32-bit values from/to internal buffer and the values are in big-endian format.
    // This means that the actual position in the NFC buffer must be corrected with the number of bytes to be swapped.
    // The same correction must be applied to the offsets in the driver buffer.
    //
    SwapVPageOff       += NUM_BYTES_TO_SWAP;
    SwapVSpareAreaOff  += NUM_BYTES_TO_SWAP;
    SwapPageOff        += NUM_BYTES_TO_SWAP;
    SwapSpareAreaOff   += NUM_BYTES_TO_SWAP;
    //
    // Save the computed parameters to the physical layer instance.
    //
    pInst->ECCMode            = (U8)ECCMode;
    pInst->NumBitsCorrectable = NumBitsCorrectable;
    pInst->ldBytesPerVPage    = ldBytesPerVPage;
    pInst->BytesPerVSpareArea = BytesPerVSpareArea;
    pInst->SwapVPageOff       = SwapVPageOff;
    pInst->SwapVSpareAreaOff  = SwapVSpareAreaOff;
    pInst->SwapPageOff        = SwapPageOff;
    pInst->SwapSpareAreaOff   = SwapSpareAreaOff;
  }
  return r;
}

/*********************************************************************
*
*       Public const
*
**********************************************************************
*/
const FS_NAND_PHY_TYPE FS_NAND_PHY_Freescale_K60 = {
  _EraseBlock,
  _InitGetDeviceInfo,
  _IsWP,
  _Read,
  _ReadEx,
  _Write,
  _WriteEx,
  _EnableECC,
  _DisableECC,
  _ConfigureECC
};

/*************************** End of file ****************************/
