/*********************************************************************
*                SEGGER MICROCONTROLLER GmbH & Co. KG                *
*        Solutions for real time microcontroller applications        *
**********************************************************************
*                                                                    *
*        (c) 2003-2012     SEGGER Microcontroller GmbH & Co KG       *
*                                                                    *
*        Internet: www.segger.com    Support:  support@segger.com    *
*                                                                    *
**********************************************************************

**** emFile file system for embedded applications ****
emFile is protected by international copyright laws. Knowledge of the
source code may not be used to write a similar product. This file may
only be used in accordance with a license and should not be re-
distributed in any way. We appreciate your understanding and fairness.
----------------------------------------------------------------------
----------------------------------------------------------------------
File        : NAND_HW_TemplatePort.c
Purpose     : Generic NAND flash hardware layer 
----------------------------------------------------------------------
Known problems or limitations with current version
----------------------------------------------------------------------
None.
---------------------------END-OF-HEADER------------------------------
*/

/*********************************************************************
*
*             #include Section
*
**********************************************************************
*/
#include "FS_ConfDefaults.h"
#include "NAND_X_HW.h"

/*********************************************************************
*
*        #define Macros
*
**********************************************************************
*/

/*********************************************************************
*
*        Macros for setting the pin to configure card
*
*    Please define here the sfr (special function register)
*    of your processor.
*
*
**********************************************************************
*/
/* Port direction registers */
#define SET_DATA2INPUT()          
#define SET_DATA2OUTPUT()         


/*********************************************************************
*
*        defines
*
**********************************************************************
*/
#define NAND_GET_DATA(Data)       
#define NAND_SET_DATA(Data)       

#define NAND_SET_ALE()            
#define NAND_CLR_ALE()            

#define NAND_SET_CLE()            
#define NAND_CLR_CLE()            

#define NAND_SET_CE()             
#define NAND_CLR_CE()             

#define NAND_SET_RE()             
#define NAND_CLR_RE()             

#define NAND_SET_WE()             
#define NAND_CLR_WE()             

#define NAND_GET_BUSY()            1

/*********************************************************************
*
*        Public code
*
**********************************************************************
*/

/*********************************************************************
*
*       FS_NAND_HW_X_EnableCE
*/
void FS_NAND_HW_X_EnableCE(U8 Unit) {
  NAND_CLR_CE();
}


/*********************************************************************
*
*       FS_NAND_HW_X_DisableCE
*/
void FS_NAND_HW_X_DisableCE(U8 Unit) {
  NAND_SET_CE();
}

/*********************************************************************
*
*       FS_NAND_HW_X_SetData
*/
void FS_NAND_HW_X_SetDataMode(U8 Unit) {
  FS_USE_PARA(Unit);
  /* nCE low, CLE low, ALE low  */
  NAND_CLR_CLE();
  NAND_CLR_ALE();
}


/*********************************************************************
*
*       FS_NAND_HW_X_SetCmd
*/
void FS_NAND_HW_X_SetCmdMode(U8 Unit) {
  FS_USE_PARA(Unit);
  /* nCE low, CLE high, ALE low  */
  NAND_SET_CLE();
  NAND_CLR_ALE();
}

/*********************************************************************
*
*       FS_NAND_HW_X_SetAddr
*/
void FS_NAND_HW_X_SetAddrMode(U8 Unit) {
  FS_USE_PARA(Unit);
  /* nCE low, CLE low, ALE high */
  NAND_CLR_CLE();
  NAND_SET_ALE();
}

/*********************************************************************
*
*       FS_NAND_HW_X_Read_x8
*/
void FS_NAND_HW_X_Read_x8(U8 Unit, void * p, unsigned NumBytes) {
  U8 * pData;

  pData = (U8 *)p;
  SET_DATA2INPUT();
  do {
    NAND_CLR_RE();     /*  RE is active low */
    NAND_GET_DATA(*pData);
    pData++;
    NAND_SET_RE();    /* disable RE */
  } while (--NumBytes);
}


/*********************************************************************
*
*       FS_NAND_HW_X_Write_x8
*/
void FS_NAND_HW_X_Write_x8(U8 Unit, const void * p, unsigned NumBytes) {
  const U8 * pData;

  pData = (const U8 *)p;
  SET_DATA2OUTPUT();
  do {
    NAND_CLR_WE();     /*  WE is active low */
    NAND_SET_DATA(*pData);
    pData++;
    NAND_SET_WE();    /* disable WE */
  } while (--NumBytes);
}

/*********************************************************************
*
*       FS_NAND_HW_X_Init
*/
void FS_NAND_HW_X_Init_x8(U8 Unit) {
  //
  // Add here the initialization of your NAND hardware
  //
}

/*********************************************************************
*
*       FS_NAND_HW_X_WaitWhileBusy
*/

int FS_NAND_HW_X_WaitWhileBusy(U8 Unit, unsigned us) {
  //
  //  This function is called to check the busy pin of
  //  the NAND flash.
  //  When no busy pin is available this function should 
  //  return 1, so the upper layer can check if NAND
  //  flash is busy.
  //
  while (NAND_GET_BUSY() == 0) {
  }
  return 0;
}

/**************************** end of file ***************************/

