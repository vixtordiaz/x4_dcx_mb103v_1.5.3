/*********************************************************************
*                SEGGER MICROCONTROLLER GmbH & Co. KG                *
*        Solutions for real time microcontroller applications        *
**********************************************************************
*                                                                    *
*        (c) 2003-2012     SEGGER Microcontroller GmbH & Co KG       *
*                                                                    *
*        Internet: www.segger.com    Support:  support@segger.com    *
*                                                                    *
**********************************************************************

**** emFile file system for embedded applications ****
emFile is protected by international copyright laws. Knowledge of the
source code may not be used to write a similar product. This file may
only be used in accordance with a license and should not be re-
distributed in any way. We appreciate your understanding and fairness.
----------------------------------------------------------------------
File        : NAND_PHY_Freescale_K70.c
Purpose     : Physical Layer for the NAND driver which uses the NAND flash controller of Freescale Kinetis K70 CPU.
Literature  : [1] \\fileserver\Techinfo\Company\Freescale\MCU\CortexM4_Kinetis\K70P256M150SF3RM_Rev2.pdf
              [2] \\fileserver\Techinfo\Company\Freescale\MCU\CortexM4_Kinetis\EvalBoard\TWR-K70FN1M\TWR-K70FN1M_Schematics.pdf
---------------------------END-OF-HEADER------------------------------
*/

#include "FS_Int.h"
#include "NAND_Private.h"

/*********************************************************************
*
*       Define configurable
*
**********************************************************************
*/
#ifdef    FS_NAND_MAXUNIT
  #define NUM_UNITS   FS_NAND_MAXUNIT
#else
  #define NUM_UNITS   4
#endif

/*********************************************************************
*
*       Defines, not configurable
*
**********************************************************************
*/

/*********************************************************************
*
*       NAND flash controller
*/
#define NFC_BASE_ADDR     0x400A8000uL
#define NFC_DATA          (*(volatile U32 *)(NFC_BASE_ADDR + 0x0000))     // Data buffer
#define NFC_CMD1          (*(volatile U32 *)(NFC_BASE_ADDR + 0x3F00))     // Flash command register 1
#define NFC_CMD2          (*(volatile U32 *)(NFC_BASE_ADDR + 0x3F04))     // Flash command register 2
#define NFC_CAR           (*(volatile U32 *)(NFC_BASE_ADDR + 0x3F08))     // Column address register
#define NFC_RAR           (*(volatile U32 *)(NFC_BASE_ADDR + 0x3F0C))     // Row address register
#define NFC_RAI           (*(volatile U32 *)(NFC_BASE_ADDR + 0x3F14))     // Row address increment register
#define NFC_SR1           (*(volatile U32 *)(NFC_BASE_ADDR + 0x3F18))     // Flash status register 1
#define NFC_SR2           (*(volatile U32 *)(NFC_BASE_ADDR + 0x3F1C))     // Flash status register 2
#define NFC_DMA1          (*(volatile U32 *)(NFC_BASE_ADDR + 0x3F20))     // DMA channel 1 address register
#define NFC_DMACFG        (*(volatile U32 *)(NFC_BASE_ADDR + 0x3F24))     // DMA configuration register
#define NFC_SWAP          (*(volatile U32 *)(NFC_BASE_ADDR + 0x3F28))     // Catch swap register
#define NFC_SECSZ         (*(volatile U32 *)(NFC_BASE_ADDR + 0x3F2C))     // Sector size register
#define NFC_CFG           (*(volatile U32 *)(NFC_BASE_ADDR + 0x3F30))     // Flash configuration register
#define NFC_ISR           (*(volatile U32 *)(NFC_BASE_ADDR + 0x3F38))     // Interrupt status register

/*********************************************************************
*
*       System integration module
*/
#define SIM_BASE_ADDR     0x40047000uL
#define SIM_SCGC3         (*(volatile U32 *)(SIM_BASE_ADDR + 0x1030))   // System clock gating register 1
#define SIM_SCGC5         (*(volatile U32 *)(SIM_BASE_ADDR + 0x1038))   // System clock gating register 5
#define SIM_CLKDIV4       (*(volatile U32 *)(SIM_BASE_ADDR + 0x1068))   // System clock divider register 4

/*********************************************************************
*
*      Port B
*/
#define PORTB_BASE_ADDR   0x4004A000uL
#define PORTB_PCR20        (*(volatile U32 *)(PORTB_BASE_ADDR + 0x50))
#define PORTB_PCR21        (*(volatile U32 *)(PORTB_BASE_ADDR + 0x54))
#define PORTB_PCR22        (*(volatile U32 *)(PORTB_BASE_ADDR + 0x58))
#define PORTB_PCR23        (*(volatile U32 *)(PORTB_BASE_ADDR + 0x5C))

/*********************************************************************
*
*      Port C
*/
#define PORTC_BASE_ADDR   0x4004B000uL
#define PORTC_PCR0        (*(volatile U32 *)(PORTC_BASE_ADDR + 0x00))
#define PORTC_PCR1        (*(volatile U32 *)(PORTC_BASE_ADDR + 0x04))
#define PORTC_PCR2        (*(volatile U32 *)(PORTC_BASE_ADDR + 0x08))
#define PORTC_PCR3        (*(volatile U32 *)(PORTC_BASE_ADDR + 0x0C))
#define PORTC_PCR4        (*(volatile U32 *)(PORTC_BASE_ADDR + 0x10))
#define PORTC_PCR5        (*(volatile U32 *)(PORTC_BASE_ADDR + 0x14))
#define PORTC_PCR6        (*(volatile U32 *)(PORTC_BASE_ADDR + 0x18))
#define PORTC_PCR7        (*(volatile U32 *)(PORTC_BASE_ADDR + 0x1C))
#define PORTC_PCR8        (*(volatile U32 *)(PORTC_BASE_ADDR + 0x20))
#define PORTC_PCR9        (*(volatile U32 *)(PORTC_BASE_ADDR + 0x24))
#define PORTC_PCR10       (*(volatile U32 *)(PORTC_BASE_ADDR + 0x28))
#define PORTC_PCR11       (*(volatile U32 *)(PORTC_BASE_ADDR + 0x2C))
#define PORTC_PCR16       (*(volatile U32 *)(PORTC_BASE_ADDR + 0x40))
#define PORTC_PCR17       (*(volatile U32 *)(PORTC_BASE_ADDR + 0x44))
#define PORTC_PCR18       (*(volatile U32 *)(PORTC_BASE_ADDR + 0x48))

/*********************************************************************
*
*      Port D
*/
#define PORTD_BASE_ADDR   0x4004C000uL
#define PORTD_PCR4        (*(volatile U32 *)(PORTD_BASE_ADDR + 0x10))
#define PORTD_PCR5        (*(volatile U32 *)(PORTD_BASE_ADDR + 0x14))
#define PORTD_PCR8        (*(volatile U32 *)(PORTD_BASE_ADDR + 0x20))
#define PORTD_PCR9        (*(volatile U32 *)(PORTD_BASE_ADDR + 0x24))
#define PORTD_PCR10       (*(volatile U32 *)(PORTD_BASE_ADDR + 0x28))

/*********************************************************************
*
*      Flash configuration register
*/
#define CFG_PAGECNT           0
#define CFG_AIBN              4
#define CFG_AIAD              5
#define CFG_BITWIDTH          7
#define CFG_TIMEOUT           8
#define CFG_IDCNT             13
#define CFG_IDCNT_MAX         7uL
#define CFG_DMAREQ            20
#define CFG_STOPWERR          31

/*********************************************************************
*
*      Flash command register 2
*/
#define CMD2_BYTE1            24
#define CMD2_CODE             8
#define CMD2_BUSY_START       0

/*********************************************************************
*
*      Flash command register 1
*/
#define CMD1_BYTE2            24

/*********************************************************************
*
*      Command sequencer codes
*/
#define CODE_END_OF_CMD       1
#define CODE_READ_ID          2
#define CODE_READ_STATUS      3
#define CODE_READ_DATA        5
#define CODE_WAIT_WHILE_BUSY  6
#define CODE_SEND_CMD_BYTE2   7
#define CODE_WRITE_DATA       8
#define CODE_SEND_ROW_ADDR3   9
#define CODE_SEND_ROW_ADDR2   10
#define CODE_SEND_ROW_ADDR1   11
#define CODE_SEND_COL_ADDR2   12
#define CODE_SEND_COL_ADDR1   13
#define CODE_SEND_CMD_BYTE1   14

/*********************************************************************
*
*      Row address register
*/
#define RAR_BYTE1             0
#define RAR_BYTE2             8
#define RAR_BYTE3             16
#define RAR_RB0               24
#define RAR_RB1               25
#define RAR_CS0               28
#define RAR_CS1               29

/*********************************************************************
*
*      Column address register
*/
#define CAR_BYTE1             0
#define CAR_BYTE2             8

/*********************************************************************
*
*      Status register 1
*/
#define SR1_ID4               0
#define SR1_ID3               8
#define SR1_ID2               16
#define SR1_ID1               24

/*********************************************************************
*
*      Status register 2
*/
#define SR2_ID5               24

/*********************************************************************
*
*      Interrupt status register
*/
#define ISR_IDLECLR           17
#define ISR_DONECLR           18
#define ISR_WERRCLR           19
#define ISR_DONE              30

/*********************************************************************
*
*      System clock gating register
*/
#define SCGC3_NFC             8
#define SCGC5_PORTB           10
#define SCGC5_PORTC           11
#define SCGC5_PORTD           12

/*********************************************************************
*
*      System clock divider register 4
*/
#define CLKDIV4_NFCDIV        27
#define CLKDIV4_NFCFRAC       24
//
// Divider for the NFC clock. The result of (NFCDIV_VALUE + 1) / (NFCFRAC_VALUE + 1) must be a multiple of 0.5
//
#define NFCDIV_VALUE          14uL
#define NFCFRAC_VALUE         3uL

/*********************************************************************
*
*      Pin control register
*/
#define PCR_DSE               6
#define PCR_MUX               8

/*********************************************************************
*
*       Execution status
*/
#define STATUS_ERROR            0x01    // 0:Pass,    1:Fail
#define STATUS_READY            0x40    // 0:Busy,    1:Ready
#define STATUS_WRITE_PROTECTED  0x80    // 0:Protect, 1:Not Protect

/*********************************************************************
*
*       NAND commands
*/
#define CMD_READ_1            0x00
#define CMD_RANDOM_READ_1     0x05
#define CMD_WRITE_2           0x10
#define CMD_READ_2            0x30
#define CMD_ERASE_1           0x60
#define CMD_ERASE_2           0xD0
#define CMD_READ_STATUS       0x70
#define CMD_WRITE_1           0x80
#define CMD_RANDOM_WRITE      0x85
#define CMD_READ_ID           0x90
#define CMD_RANDOM_READ_2     0xE0
#define CMD_READ_PARA_PAGE    0xEC
#define CMD_GET_FEATURES      0xEE
#define CMD_SET_FEATURES      0xEF
#define CMD_RESET             0xFF

/*********************************************************************
*
*       Device features
*/
#define NUM_FEATURE_PARA          4
#define MICRON_ECC_FEATURE_ADDR   0x90
#define MICRON_ECC_FEATURE_MASK   0x08

/*********************************************************************
*
*       ONFI parameters
*/
#define PARA_PAGE_SIZE        256
#define PARA_CRC_POLY         0x8005
#define PARA_CRC_INIT         0x4F4E
#define NUM_PARA_PAGES        3
#define READ_ID_ONFI          0x20

/*********************************************************************
*
*       Assert macros
*/
#if FS_DEBUG_LEVEL >= FS_DEBUG_LEVEL_CHECK_ALL
  #define ASSERT_PARA_IS_ALIGNED(pInst, Para)               \
    if ((pInst)->DataBusWidth == 16) {                      \
      FS_DEBUG_ASSERT(FS_MTYPE_DRIVER, ((Para) & 1) == 0);  \
    }
#else
  #define ASSERT_PARA_IS_ALIGNED(pInst, Para)
#endif

/*********************************************************************
*
*       typedefs
*
**********************************************************************
*/
typedef struct {
  U8 Unit;
  U8 DataBusWidth;      // Width of the data bus in bits (16 or 8)
  U8 NumBytesColAddr;   // Number of bytes in a column address
  U8 NumBytesRowAddr;   // Number of bytes in a row address
} PHY_INST;

/*********************************************************************
*
*       Static data
*
**********************************************************************
*/
static PHY_INST _aInst[NUM_UNITS];

/*********************************************************************
*
*       Static code
*
**********************************************************************
*/

/*********************************************************************
*
*       _ld
*/
static U16 _ld(U32 Value) {
  U16 i;
  for (i = 0; i < 16; i++) {
    if ((1UL << i) == Value) {
      break;
    }
  }
  return i;
}

/*********************************************************************
*
*       _InitDataBus16
*
*   Function description
*     Configures I/O pins and NFC for a 16-bit data bus.
*/
static void _InitDataBus16(void) {
  NFC_CFG     |= 1uL << CFG_BITWIDTH;
  PORTB_PCR20 |= (5uL << PCR_MUX)
              |  (1uL << PCR_DSE)
              ;                       // NFC_DATA15
  PORTB_PCR21 |= (5uL << PCR_MUX)
              |  (1uL << PCR_DSE)
              ;                       // NFC_DATA14
  PORTB_PCR22 |= (5uL << PCR_MUX)
              |  (1uL << PCR_DSE)
              ;                       // NFC_DATA13
  PORTB_PCR23 |= (5uL << PCR_MUX)
              |  (1uL << PCR_DSE)
              ;                       // NFC_DATA12
  PORTC_PCR0  |= (5uL << PCR_MUX)
              |  (1uL << PCR_DSE)
              ;                       // NFC_DATA11
  PORTC_PCR1  |= (5uL << PCR_MUX)
              |  (1uL << PCR_DSE)
              ;                       // NFC_DATA10
  PORTC_PCR2  |= (5uL << PCR_MUX)
              |  (1uL << PCR_DSE)
              ;                       // NFC_DATA9
  PORTC_PCR4  |= (5uL << PCR_MUX)
              |  (1uL << PCR_DSE)
              ;                       // NFC_DATA8
  PORTC_PCR5  = (5uL << PCR_MUX)
              | (1uL << PCR_DSE)
              ;                       // NFC_DATA7
  PORTC_PCR6  = (5uL << PCR_MUX)
              | (1uL << PCR_DSE)
              ;                       // NFC_DATA6
  PORTC_PCR7  = (5uL << PCR_MUX)
              | (1uL << PCR_DSE)
              ;                       // NFC_DATA5
  PORTC_PCR8  = (5uL << PCR_MUX)
              | (1uL << PCR_DSE)
              ;                       // NFC_DATA4
  PORTC_PCR9  = (5uL << PCR_MUX)
              | (1uL << PCR_DSE)
              ;                       // NFC_DATA3
  PORTC_PCR10 = (5uL << PCR_MUX)
              | (1uL << PCR_DSE)
              ;                       // NFC_DATA2
  PORTD_PCR4  = (5uL << PCR_MUX)
              | (1uL << PCR_DSE)
              ;                       // NFC_DATA1
  PORTD_PCR5  = (5uL << PCR_MUX)
              | (1uL << PCR_DSE)
              ;                       // NFC_DATA0
}

/*********************************************************************
*
*       _InitDataBus8
*
*   Function description
*     Configures I/O pins and NFC for an 8-bit data bus.
*/
static void _InitDataBus8(void) {
  NFC_CFG &= ~(1uL << CFG_BITWIDTH);
  PORTC_PCR5  = (5uL << PCR_MUX)
              | (1uL << PCR_DSE)
              ;                       // NFC_DATA7
  PORTC_PCR6  = (5uL << PCR_MUX)
              | (1uL << PCR_DSE)
              ;                       // NFC_DATA6
  PORTC_PCR7  = (5uL << PCR_MUX)
              | (1uL << PCR_DSE)
              ;                       // NFC_DATA5
  PORTC_PCR8  = (5uL << PCR_MUX)
              | (1uL << PCR_DSE)
              ;                       // NFC_DATA4
  PORTC_PCR9  = (5uL << PCR_MUX)
              | (1uL << PCR_DSE)
              ;                       // NFC_DATA3
  PORTC_PCR10 = (5uL << PCR_MUX)
              | (1uL << PCR_DSE)
              ;                       // NFC_DATA2
  PORTD_PCR4  = (5uL << PCR_MUX)
              | (1uL << PCR_DSE)
              ;                       // NFC_DATA1
  PORTD_PCR5  = (5uL << PCR_MUX)
              | (1uL << PCR_DSE)
              ;                       // NFC_DATA0
}

/*********************************************************************
*
*       _InitNFC
*
*   Function description
*     Initializes the NAND flash controller.
*/
static void _InitNFC(PHY_INST * pInst) {
  U8 Unit;

  Unit = pInst->Unit;
  //
  // Reset NFC peripheral.
  //
  SIM_SCGC3 &= ~(1uL << SCGC3_NFC); 	
  SIM_SCGC3 |=   1uL << SCGC3_NFC; 	
  //
  // Enable the port C and D.
  //
  SIM_SCGC5 |= (1uL << SCGC5_PORTB)
            |  (1uL << SCGC5_PORTC)
            |  (1uL << SCGC5_PORTD)
            ;
  //
  // Set the clock divisor.
  //
  SIM_CLKDIV4 |= (NFCDIV_VALUE  << CLKDIV4_NFCDIV)
              |  (NFCFRAC_VALUE << CLKDIV4_NFCFRAC)
              ;
  //
  // Enable the ready/busy signals.
  //
  if (Unit == 0) {
    PORTC_PCR17 = (6uL << PCR_MUX)
                | (1uL << PCR_DSE)    // NFC_CS0
                ;
  } else {
    PORTC_PCR18 = (6uL << PCR_MUX)
                | (1uL << PCR_DSE)
                ;                     // NFC_CS1
  }
  //
  // Enable the control signals.
  //
  PORTC_PCR16 = (6uL << PCR_MUX)
              | (1uL << PCR_DSE)
              ;                       // NFC_RB
  PORTD_PCR8  = (6uL << PCR_MUX)
              | (1uL << PCR_DSE)
              ;                       // NFC_CLE
  PORTD_PCR9  = (6uL << PCR_MUX)
              | (1uL << PCR_DSE)
              ;                       // NFC_ALE
  PORTD_PCR10 = (6uL << PCR_MUX)
              | (1uL << PCR_DSE)
              ;                       // NFC_RE
  PORTC_PCR11 = (5uL << PCR_MUX)
              | (1uL << PCR_DSE)
              ;                       // NFC_WE
  //
  // Enable the data signals. Start with an 8-bit bus.
  // Extend it later to 16-bit if required.
  //
  _InitDataBus8();
  //
  // Stop on write error.
  //
  NFC_CFG = (1uL << CFG_STOPWERR)
          | (1uL << CFG_PAGECNT)
          | (6uL << CFG_TIMEOUT)
          ;
  //
  // Disable row address increment.
  //
  NFC_RAI  = 0;
  //
  // Disable address swap.
  //
  NFC_SWAP = 0;
  //
  // Clear the command registers.
  //
  NFC_CMD2 = 0;
  NFC_CMD1 = 0;
}

/*********************************************************************
*
*       _SelectDevice
*/
static void _SelectDevice(PHY_INST * pInst) {
  U8 Unit;

  Unit = pInst->Unit;
  if (Unit == 0) {
    NFC_RAR |= (1uL << RAR_CS0)
            |  (1uL << RAR_RB0)
            ;
    NFC_RAR &= ~((1uL << RAR_CS1) |
                 (1uL << RAR_RB1));
  } else {
    NFC_RAR |= (1uL << RAR_CS1)
            |  (1uL << RAR_RB1)
            ;
    NFC_RAR &= ~((1uL << RAR_CS0) |
                 (1uL << RAR_RB0));
  }
}

/*********************************************************************
*
*       _SetCmdByte1
*/
static void _SetCmdByte1(U8 Value) {
  U16 Code;

  Code      = 1uL << CODE_SEND_CMD_BYTE1;
  NFC_CMD2 |= (Value << CMD2_BYTE1)
           |  (Code  << CMD2_CODE)
           ;
}

/*********************************************************************
*
*       _SetCmdByte2
*/
static void _SetCmdByte2(U8 Value) {
  U16 Code;

  Code      = 1uL << CODE_SEND_CMD_BYTE2;
  NFC_CMD2 |= Code  << CMD2_CODE;
  NFC_CMD1 |= Value << CMD1_BYTE2;
}

/*********************************************************************
*
*       _SetRowAddr1
*/
static void _SetRowAddr1(U8 Value) {
  U16 Code;

  Code      = 1uL << CODE_SEND_ROW_ADDR1;
  NFC_CMD2 |= Code << CMD2_CODE;
  NFC_RAR  &= ~(0xFFuL << RAR_BYTE1);
  NFC_RAR  |= (U32)Value << RAR_BYTE1;
}

/*********************************************************************
*
*       _SetRowAddr2
*/
static void _SetRowAddr2(U8 Value) {
  U16 Code;

  Code      = 1uL << CODE_SEND_ROW_ADDR2;
  NFC_CMD2 |= Code << CMD2_CODE;
  NFC_RAR  &= ~(0xFFuL << RAR_BYTE2);
  NFC_RAR  |= (U32)Value << RAR_BYTE2;
}

/*********************************************************************
*
*       _SetRowAddr3
*/
static void _SetRowAddr3(U8 Value) {
  U16 Code;

  Code      = 1uL << CODE_SEND_ROW_ADDR3;
  NFC_CMD2 |= Code << CMD2_CODE;
  NFC_RAR  &= ~(0xFFuL << RAR_BYTE3);
  NFC_RAR  |= (U32)Value << RAR_BYTE3;
}

/*********************************************************************
*
*       _SetColAddr1
*/
static void _SetColAddr1(U8 Value) {
  U16 Code;

  Code      = 1uL << CODE_SEND_COL_ADDR1;
  NFC_CMD2 |= Code << CMD2_CODE;
  NFC_CAR  &= ~(0xFFuL << CAR_BYTE1);
  NFC_CAR  |= (U32)Value << CAR_BYTE1;
}

/*********************************************************************
*
*       _SetColAddr2
*/
static void _SetColAddr2(U8 Value) {
  U16 Code;

  Code      = 1uL << CODE_SEND_COL_ADDR2;
  NFC_CMD2 |= Code << CMD2_CODE;
  NFC_CAR  &= ~(0xFFuL << CAR_BYTE2);
  NFC_CAR  |= (U32)Value << CAR_BYTE2;
}

/*********************************************************************
*
*       _SetStatusRead
*/
static void _SetStatusRead(void) {
  U16 Code;

  Code      = 1uL  << CODE_READ_STATUS;
  NFC_CMD2 |= Code << CMD2_CODE;
}

/*********************************************************************
*
*       _SetBusyWait
*/
static void _SetBusyWait(void) {
  U16 Code;

  Code      = 1uL  << CODE_WAIT_WHILE_BUSY;
  NFC_CMD2 |= Code << CMD2_CODE;
}

/*********************************************************************
*
*       _SetIdRead
*/
static void _SetIdRead(U32 NumBytes) {
  U16 Code;

  Code      = 1uL  << CODE_READ_ID;
  NFC_CMD2 |= Code << CMD2_CODE;
  NFC_CFG  &= ~(CFG_IDCNT_MAX << CFG_IDCNT);
  NFC_CFG  |= NumBytes << CFG_IDCNT;
}

/*********************************************************************
*
*       _SetDataRead
*/
static void _SetDataRead(U32 NumBytes) {
  U16 Code;

  Code       = 1uL  << CODE_READ_DATA;
  NFC_CMD2  |= Code << CMD2_CODE;
  //
  // On a 16-bit data bus the number of bytes transferred must be odd.
  // The controller will read NFC_SECSZ - 1 bytes.
  //
  if (NFC_CFG & CFG_BITWIDTH) {
    ++NumBytes;
  }
  NFC_SECSZ  = NumBytes;
}

/*********************************************************************
*
*       _SetDataWrite
*/
static void _SetDataWrite(U32 NumBytes) {
  U16 Code;

  Code       = 1uL  << CODE_WRITE_DATA;
  NFC_CMD2  |= Code << CMD2_CODE;
  //
  // On a 16-bit data bus the number of bytes transferred must be odd.
  // The controller will write NFC_SECSZ - 1 bytes.
  //
  if (NFC_CFG & CFG_BITWIDTH) {
    ++NumBytes;
  }
  NFC_SECSZ  = NumBytes;
}

/*********************************************************************
*
*       _LoadData8
*
*   Function description
*     Reads data from NFC buffer when the NAND flash is interfaced over a 8-bit bus.
*/
static void _LoadData8(U8 * pData, U32 NumBytes) {
  volatile U32 * pDataNFC;

  pDataNFC = &NFC_DATA;
  //
  // Copy 4 bytes at a time if the destination buffer is aligned.
  //
  if (((U32)pData & 3) == 0) {
    U32 * pData32;
    U32   NumWords;

    pData32  = (U32 *)pData;
    NumWords = NumBytes >> 2;
    if (NumWords) {
      do {
        //
        // NFC packs together 4 bytes in a word starting from LSB.
        // For example the sequence of bytes 0x1, 0x2, 0x3, 0x4 is stored as 0x04030201.
        // We have to reverse the byte order here as the driver expects an array of bytes.
        //
        *pData32++ = FS_LoadU32BE((U8 *)pDataNFC++);
        NumBytes -= 4;
        pData    += 4;
      } while (--NumWords);
    }
  }
  //
  // Copy the rest of the data byte-wise.
  //
  if (NumBytes) {
    U32 Data32;
    U32 NumLoops;

    do {
      Data32   = FS_LoadU32BE((U8 *)pDataNFC++);
      NumLoops = MIN(NumBytes, 4);
      do {
        *pData   = (U8)Data32;
        Data32 >>= 8;
        ++pData;
        --NumBytes;
      } while (--NumLoops);
    } while (NumBytes);
  }
}

/*********************************************************************
*
*       _LoadData16
*
*   Function description
*     Reads data from NFC buffer when the NAND flash is interfaced over a 16-bit bus.
*/
static void _LoadData16(U16 * pData, U32 NumBytes) {
  volatile U16 * pDataNFC;
  U32            NumWords;

  pDataNFC = (volatile U16 *)&NFC_DATA;
  NumWords = NumBytes >> 2;
  if (NumWords) {
    do {
      *pData++  = *(pDataNFC + 1);
      *pData++  = *pDataNFC;
      pDataNFC += 2;
      NumBytes -= 4;
    } while (--NumWords);
  }
  if (NumBytes) {
    *pData = *(pDataNFC + 1);
  }
}

/*********************************************************************
*
*       _LoadData
*
*   Function description
*     Reads data from NFC buffer.
*/
static void _LoadData(PHY_INST * pInst, U16 * pData, U32 NumBytes) {
  U8 DataBusWidth;

  DataBusWidth = pInst->DataBusWidth;
  if (DataBusWidth == 16) {
    _LoadData16(pData, NumBytes);
  } else {
    _LoadData8((U8 *)pData, NumBytes);
  }
}

/*********************************************************************
*
*       _StoreData8
*
*   Function description
*     Writes data to NFC buffer when the NAND flash is interfaced over an 8-bit bus.
*/
static void _StoreData8(const U8 * pData, U32 NumBytes) {
  volatile U32 * pDataNFC;

  pDataNFC = &NFC_DATA;
  //
  // Copy 4 bytes at a time if the source buffer is aligned.
  //
  if (((U32)pData & 3) == 0) {
    const U32 * pData32;
    U32         NumWords;

    pData32  = (U32 *)pData;
    NumWords = NumBytes >> 2;
    if (NumWords) {
      do {
        FS_StoreU32BE((U8 *)pDataNFC++, *pData32++);
        NumBytes -= 4;
        pData    += 4;
      } while (--NumWords);
    }
  }
  //
  // Copy the rest of the data byte-wise.
  //
  if (NumBytes) {
    U32 Data32;
    U32 NumLoops;

    do {
      NumLoops = MIN(NumBytes, 4);
      Data32   = 0xFFFFFFFF;
      do {
        Data32 <<= 8;
        Data32  |= (U32)(*pData);
        --NumBytes;
        ++pData;
      } while (--NumLoops);
      FS_StoreU32BE((U8 *)pDataNFC++, Data32);
    } while (NumBytes);
  }
}

/*********************************************************************
*
*       _StoreData16
*
*   Function description
*     Writes data to NFC buffer when the NAND flash is interfaced over a 16-bit bus.
*/
static void _StoreData16(const U16 * pData, U32 NumBytes) {
  volatile U16 * pDataNFC;
  U32            NumWords;

  pDataNFC = (volatile U16 *)&NFC_DATA;
  NumWords = NumBytes >> 2;
  if (NumWords) {
    do {
      *pDataNFC++ = *(pData + 1);
      *pDataNFC++ = *pData;
      pData    += 2;
      NumBytes -= 4;
    } while (--NumWords);
  }
  if (NumBytes) {
    *pDataNFC++ = 0xFFFF;
    *pDataNFC++ = *pData;
  }
}

/*********************************************************************
*
*       _StoreData
*
*   Function description
*     Writes data to NFC buffer.
*/
static void _StoreData(PHY_INST * pInst, const U16 * pData, U32 NumBytes) {
  U8 DataBusWidth;

  DataBusWidth = pInst->DataBusWidth;
  if (DataBusWidth == 16) {
    _StoreData16(pData, NumBytes);
  } else {
    _StoreData8((U8 *)pData, NumBytes);
  }
}

/*********************************************************************
*
*       _ExecCmd
*/
static void _ExecCmd(void) {
  U16 Code;

  NFC_ISR  |= (1uL << ISR_IDLECLR)
           |  (1uL << ISR_DONECLR)
           |  (1uL << ISR_WERRCLR)
           ;
  Code      = 1uL << CODE_END_OF_CMD;
  NFC_CMD2 |= (Code << CMD2_CODE)
           |  (1uL  << CMD2_BUSY_START)
           ;
  while ((NFC_ISR & (1uL << ISR_DONE)) == 0) {
    ;
  }
  NFC_CMD1 = 0;
  NFC_CMD2 = 0;
}

/*********************************************************************
*
*       _SetRowAddr
*/
static void _SetRowAddr(PHY_INST * pInst, U32 RowAddr) {
  U8 NumBytes;

  NumBytes = pInst->NumBytesRowAddr;
  _SetRowAddr1(RowAddr);
  if (NumBytes > 1) {
    RowAddr >>= 8;
    _SetRowAddr2(RowAddr);
  }
  if (NumBytes > 2) {
    RowAddr >>= 8;
    _SetRowAddr3(RowAddr);
  }
}

/*********************************************************************
*
*       _SetColAddr
*/
static void _SetColAddr(PHY_INST * pInst, U32 ColAddr) {
  U8 NumBytes;
  U8 DataBusWidth;

  DataBusWidth = pInst->DataBusWidth;
  if (DataBusWidth == 16) {
    ColAddr >>= 1;        // Convert to a 16-bit word address.
  }
  NumBytes = pInst->NumBytesColAddr;
  _SetColAddr1(ColAddr);
  if (NumBytes > 1) {
    ColAddr >>= 8;
    _SetColAddr2(ColAddr);
  }
}

/*********************************************************************
*
*       _SetRowColAddr
*/
static void _SetRowColAddr(PHY_INST * pInst, U32 RowAddr, U32 ColAddr) {
  _SetRowAddr(pInst, RowAddr);
  _SetColAddr(pInst, ColAddr);
}

/*********************************************************************
*
*       _ReadStatus
*
*   Function description
*     Reads and returns the contents of the status register.
*/
static U8 _ReadStatus(void) {
  U8 Status;

  _SetCmdByte1(CMD_READ_STATUS);
  _SetStatusRead();
  _ExecCmd();
  Status = (U8)NFC_SR2;
  return Status;
}

/*********************************************************************
*
*       _WaitForDeviceReady
*
*   Function description
*     Waits for the NAND to complete its last operation.
*
*   Parameters
*     Unit    Number of NAND unit
*
*   Return value
*    ==0  Success
*    !=0  An error has occurred
*/
static int _WaitForDeviceReady(void) {
  U8 Status;

  _SetBusyWait();
  _ExecCmd();
  do {
    Status = _ReadStatus();
  } while ((Status & STATUS_READY) == 0);
  return (Status & STATUS_ERROR) ? 1 : 0;
}

/*********************************************************************
*
*       _Reset
*
*   Function description
*     Resets the NAND flash by command
*/
static void _Reset(void) {
  _SetCmdByte1(CMD_RESET);
  _ExecCmd();
  _WaitForDeviceReady();
}

/*********************************************************************
*
*       _ReadId
*
*   Function description
*     Reads a specified number of Id bytes.
*/
static void _ReadId(U8 Addr, U8 * pData, U32 NumBytes) {
  _SetCmdByte1(CMD_READ_ID);
  _SetRowAddr1(Addr);
  _SetIdRead(NumBytes);
  _ExecCmd();
  *pData++ = (U8)(NFC_SR1 >> SR1_ID1);
  if (NumBytes > 1) {
    *pData++ = (U8)(NFC_SR1 >> SR1_ID2);
  }
  if (NumBytes > 2) {
    *pData++ = (U8)(NFC_SR1 >> SR1_ID3);
  }
  if (NumBytes > 3) {
    *pData++ = (U8)(NFC_SR1 >> SR1_ID4);
  }
  if (NumBytes > 4) {
    *pData++ = (U8)(NFC_SR2 >> SR2_ID5);
  }
}

/*********************************************************************
*
*       _IsONFISupported
*
*   Function description
*     Checks if the device supports ONFI.
*     An ONFI compatible device returns "ONFI" ASCII string
*     when executing a READ ID operation from address 0x20
*
*   Return value
*     ==0    ONFI not supported
*     !=0    ONFI supported
*/
static int _IsONFISupported(void) {
  int r;
  U8  aId[4];

  r = 0;              // ONFI is not supported so far
  FS_MEMSET(aId, 0, sizeof(aId));
  _ReadId(READ_ID_ONFI, aId, sizeof(aId));
  if ((aId[0] == 'O') &&
      (aId[1] == 'N') &&
      (aId[2] == 'F') &&
      (aId[3] == 'I')) {
    r = 1;
  }
  return r;
}

/*********************************************************************
*
*       _ReadONFIPara
*
*   Function description
*     Reads the ONFI parameter page.
*     A page has 256 bytes. The integrity of information is checked using CRC.
*
*   Parameters
*     pInst   [IN]  Instance of physical layer
*             [OUT] ---
*     pPara   [IN]  ---
*             [OUT] Information read from the parameter page. Must be at least 256 bytes long.
*
*   Return value
*     ==0    ONFI parameters read
*     !=0    An error occurred
*/
static int _ReadONFIPara(PHY_INST * pInst, void * pPara) {
  int   r;
  U16   crcRead;
  U16   crcCalc;
  U8  * p;
  U32   NumBytesPara;
  int   i;

  r = 1;        // No parameter page found, yet.
  _SetCmdByte1(CMD_READ_PARA_PAGE);
  _SetRowAddr1(0);
  _ExecCmd();
  r = _WaitForDeviceReady();
  if (r == 0) {
    _SetCmdByte1(CMD_READ_1);
    _ExecCmd();                   // Switch back to read mode. _WaitForReady() function changed it to status mode.
  }
  if (r == 0) {
    p = (U8 *)pPara;
    //
    // Several identical parameter pages are stored in a device.
    // Read from the first one which stores valid information.
    //
    for (i = 0; i < NUM_PARA_PAGES; ++i) {
      _SetDataRead(PARA_PAGE_SIZE);
      _ExecCmd();
      _LoadData(pInst, (U16 *)pPara, PARA_PAGE_SIZE);
      NumBytesPara = PARA_PAGE_SIZE - sizeof(crcRead);    // CRC is stored on the last 2 bytes of the parameter page.
      //
      // Validate the parameters by checking the CRC.
      //
      crcRead = FS_LoadU16LE(&p[NumBytesPara]);
      crcCalc = FS_CRC16_CalcBitByBit(p, NumBytesPara, PARA_CRC_INIT, PARA_CRC_POLY);
      if (crcRead != crcCalc) {
        continue;
      }
      //
      // CRC is valid, now check the signature.
      //
      if ((p[0] == 'O') &&
          (p[1] == 'N') &&
          (p[2] == 'F') &&
          (p[3] == 'I')) {
        r = 0;
        break;                      // Found a valid parameter page.
      }
    }
  }
  return r;
}

/*********************************************************************
*
*       _GetFeatures
*
*   Function description
*     Reads the device settings.
*
*   Parameters
*     Unit    Device unit number
*     Addr    Feature address (see device datasheet)
*     pData   [IN]  ---
*             [OUT] Read device settings. Must be at least 4 bytes large.
*
*   Return value
*     ==0    Settings read
*     !=0    An error occurred
*/
static int _GetFeatures(PHY_INST * pInst, U8 Addr, U8 * pData) {
  int r;
  U16 aBuffer[NUM_FEATURE_PARA];
  U32 NumBytes;
  U8  DataBusWidth;

  DataBusWidth = pInst->DataBusWidth;
  NumBytes     = NUM_FEATURE_PARA;
  //
  // 4 read cycles must be executed to get the feature parameters.
  // On a 16-bit bus the data is returned on the low-order bits.
  // 2 times more bytes are required to be read in this case.
  //
  if (DataBusWidth == 16) {
    NumBytes <<= 1;
  }
  _SetCmdByte1(CMD_GET_FEATURES);
  _SetRowAddr1(Addr);
  _ExecCmd();
  r = _WaitForDeviceReady();
  if (r == 0) {
    U8  * pData8;
    U16 * pData16;
    U32   NumLoops;

    //
    // Revert to read mode. _WaitForDeviceReady() change it to status mode.
    //
    _SetCmdByte1(CMD_READ_1);
    _SetDataRead(NumBytes);
    _ExecCmd();

    _LoadData(pInst, aBuffer, NumBytes);
    pData8   = (U8 *)aBuffer;
    pData16  = aBuffer;
    NumLoops = NumBytes;
    if (DataBusWidth == 16) {
      NumLoops >>= 1;
    }
    do {
      if (DataBusWidth == 16) {
        *pData++  = (U8)*pData16++;
      } else {
        *pData++ = *pData8++;
      }
    } while (--NumLoops);
  }
  if (r) {
    _Reset();
  }
  return r;
}

/*********************************************************************
*
*       _SetFeatures
*
*   Function description
*     Modifies the device settings.
*
*   Parameters
*     Addr    Feature address (see device datasheet)
*     pData   [IN]  New device settings.  Must be at least 4 bytes long.
*             [OUT] ---
*
*   Return value
*     ==0    Settings written
*     !=0    An error occurred
*/
static int _SetFeatures(PHY_INST * pInst, U8 Addr, const U8 * pData) {
  int   r;
  U16   aBuffer[NUM_FEATURE_PARA];
  U32   NumBytes;
  U8    DataBusWidth;
  U8  * pData8;
  U16 * pData16;
  U32   NumLoops;

  DataBusWidth = pInst->DataBusWidth;
  NumBytes     = NUM_FEATURE_PARA;
  //
  // 4 write cycles must be executed to set the feature parameters.
  // On a 16-bit bus the data is sent on the low-order bits.
  // 2 times more bytes are required to be written in this case.
  //
  if (DataBusWidth == 16) {
    NumBytes <<= 1;
  }
  //
  // Copy data to NFC buffer.
  //
  pData8   = (U8 *)aBuffer;
  pData16  = aBuffer;
  NumLoops = NumBytes;
  if (DataBusWidth == 16) {
    NumLoops >>= 1;
  }
  do {
    if (DataBusWidth == 16) {
      *pData16++  = (U16)*pData++;
    } else {
      *pData8++   = *pData++;
    }
  } while (--NumLoops);
  //
  // Prepare the command and execute it.
  //
  _SetCmdByte1(CMD_SET_FEATURES);
  _SetRowAddr1(Addr);
  _SetDataWrite(NumBytes);
  _StoreData(pInst, aBuffer, NumBytes);
  _ExecCmd();
  //
  // Wait for NAND flash to store the parameters.
  //
  r = _WaitForDeviceReady();
  if (r) {
    _Reset();
  }
  return r;
}

/*********************************************************************
*
*       _Read
*
*   Function description
*     Reads data from a complete or a part of a page.
*     This code is identical for main memory and spare area; the spare area
*     is located rigth after the main area.
*
*   Return value:
*     ==0     Data successfully transferred.
*     !=0     An error has occurred.
*/
static int _Read(U8 Unit, U32 PageNo, void * pData, unsigned Off, unsigned NumBytes) {
  PHY_INST * pInst;
  int r;

  pInst = &_aInst[Unit];
  ASSERT_PARA_IS_ALIGNED(pInst, NumBytes | Off | (U32)pData);
  _SelectDevice(pInst);
  _SetCmdByte1(CMD_READ_1);
  _SetRowColAddr(pInst, PageNo, Off);
  _SetCmdByte2(CMD_READ_2);
  _ExecCmd();
  r = _WaitForDeviceReady();
  if (r == 0) {
    _SetCmdByte1(CMD_READ_1);
    _SetDataRead(NumBytes);
    _ExecCmd();
    _LoadData(pInst, (U16 *)pData, NumBytes);
  }
  if (r) {
    _Reset();
  }
  return r;
}

/*********************************************************************
*
*       _ReadEx
*
*   Function description
*     Reads data from two locations on a page.
*     Typically used to read data and spare area at once.
*
*   Return value:
*     ==0     Data successfully transferred.
*     !=0     An error has occurred.
*/
static int _ReadEx(U8 Unit, U32 PageNo, void * pData, unsigned Off, unsigned NumBytes, void * pSpare, unsigned OffSpare, unsigned NumBytesSpare) {
  PHY_INST * pInst;
  int        r;

  pInst = &_aInst[Unit];
  ASSERT_PARA_IS_ALIGNED(pInst, NumBytes | Off | (U32)pSpare | OffSpare | NumBytesSpare);
  _SelectDevice(pInst);
  //
  // Read the requested page from the memory array to data register.
  //
  _SetCmdByte1(CMD_READ_1);
  _SetRowColAddr(pInst, PageNo, Off);
  _SetCmdByte2(CMD_READ_2);
  _ExecCmd();
  r = _WaitForDeviceReady();
  //
  // OK, page data, main and spare area, is in data register now.
  //
  if (r == 0) {
    //
    // Copy the data of main area to host.
    //
    _SetCmdByte1(CMD_READ_1);
    _SetDataRead(NumBytes);
    _ExecCmd();
    _LoadData(pInst, (U16 *)pData, NumBytes);
    //
    // Copy the data of spare area to host.
    //
    _SetCmdByte1(CMD_READ_1);
    _SetColAddr(pInst, OffSpare);
    _SetCmdByte2(CMD_READ_2);
    _SetDataRead(NumBytesSpare);
    _ExecCmd();
    _LoadData(pInst, (U16 *)pSpare, NumBytesSpare);
  }
  if (r) {
    _Reset();
  }
  return r;
}

/*********************************************************************
*
*       _Write
*
*   Function description
*     Writes data into a complete or a part of a page.
*     This code is identical for main memory and spare area; the spare area
*     is located rigth after the main area.
*
*   Return value:
*     ==0     Data successfully transferred.
*     !=0     An error has occurred.
*/
static int _Write(U8 Unit, U32 PageNo, const void * pData, unsigned Off, unsigned NumBytes) {
  PHY_INST * pInst;
  int        r;

  pInst = &_aInst[Unit];
  ASSERT_PARA_IS_ALIGNED(pInst, NumBytes | Off | (U32)pData);
  //
  // Set the address of the first byte to write.
  //
  _SetCmdByte1(CMD_WRITE_1);
  _SetRowColAddr(pInst, PageNo, Off);
  //
  // Store to NFC buffer the data to write.
  //
  _SetDataWrite(NumBytes);
  _StoreData(pInst, (U16 *)pData, NumBytes);
  _SetCmdByte2(CMD_WRITE_2);
  //
  // Write the data to NAND flash and wait for completion.
  //
  _ExecCmd();
  r = _WaitForDeviceReady();
  if (r) {
    _Reset();
  }
  return r;
}

/*********************************************************************
*
*       _WriteEx
*
*   Function description
*     Writes data to 2 parts of a page.
*     Typically used to write data and spare area at the same time.
*
*  Return value:
*     ==0     Data successfully transferred.
*     !=0     An error has occurred.
*/
static int _WriteEx(U8 Unit, U32 PageNo, const void * pData, unsigned Off, unsigned NumBytes, const void * pSpare, unsigned OffSpare, unsigned NumBytesSpare) {
  PHY_INST * pInst;
  int        r;

  pInst = &_aInst[Unit];
  ASSERT_PARA_IS_ALIGNED(pInst, NumBytes | Off | (U32)pData);
  //
  // Set the address of the first byte in the main area to write.
  //
  _SetCmdByte1(CMD_WRITE_1);
  _SetRowColAddr(pInst, PageNo, Off);
  //
  // Store to NFC buffer the data to be written to main area of the page.
  //
  _SetDataWrite(NumBytes);
  _StoreData(pInst, (U16 *)pData, NumBytes);
  //
  // Transfer data to data register of NAND flash.
  //
  _ExecCmd();
  //
  // OK, main area data stored to NAND flash data register.
  //
  //
  // Set the address of the first byte in the spare area to write.
  //
  _SetCmdByte1(CMD_RANDOM_WRITE);
  _SetColAddr(pInst, OffSpare);
  //
  // Store to NFC buffer the data to be written to spare area of the page.
  //
  _SetDataWrite(NumBytesSpare);
  _StoreData(pInst, (U16 *)pSpare, NumBytesSpare);
  _SetCmdByte2(CMD_WRITE_2);
  //
  // Write the data to spare area and wait for completion.
  //
  _ExecCmd();
  r = _WaitForDeviceReady();
  if (r) {
    _Reset();
  }
  return r;
}

/*********************************************************************
*
*       _EraseBlock
*
*   Function description
*     Sets all the bytes in a block to 0xFF
*
*   Return value:
*     ==0     Data successfully transferred.
*     !=0     An error has occurred.
*/
static int _EraseBlock(U8 Unit, U32 BlockNo) {
  PHY_INST * pInst;
  int r;

  pInst = &_aInst[Unit];
  _SelectDevice(pInst);
  _SetCmdByte1(CMD_ERASE_1);
  _SetRowAddr(pInst, BlockNo);
  _SetCmdByte2(CMD_ERASE_2);
  _ExecCmd();
  r = _WaitForDeviceReady();
  if (r) {
    _Reset();
  }
  return r;
}

/*********************************************************************
*
*       _InitGetDeviceInfo
*
*  Function description
*    Initializes hardware layer, resets NAND flash and tries to identify the NAND flash.
*    If the NAND flash can be handled, Device Info is filled.
*
*  Return value:
*      0    O.K., device can be handled
*      1    Error: device can not be handled
*
*  Notes
*       (1) The first command to be issued after power-on is RESET (see [2])
*/
static int _InitGetDeviceInfo(U8 Unit, FS_NAND_DEVICE_INFO * pDevInfo) {
  U8 * pBuffer;
  int  r;
  PHY_INST * pInst;

  r = 1;
  //
  // Initialize hardware and reset the device
  //
   pInst = &_aInst[Unit];
   pInst->Unit         = Unit;
   pInst->DataBusWidth = 8;
  _InitNFC(pInst);
  _SelectDevice(pInst);
  _Reset();                   // Note 1
  if (_IsONFISupported()) {
    //
    // Device supports ONFI, go ahead and read parameters.
    // We use a temporarily sector buffer for reading.
    // A sector buffer is minimum 512 bytes, so large enough to hold all the parameters.
    //
    pBuffer = FS__AllocSectorBuffer();
    r = _ReadONFIPara(pInst, pBuffer);
    if (r == 0) {
      U32 BytesPerPage;
      U32 PagesPerBlock;
      U32 NumBlocks;
      U16 Features;
      U8  NumAddrBytes;

      //
      // Decode the ONFI parameters required by the driver.
      //
      Features      = FS_LoadU16LE(&pBuffer[6]);
      BytesPerPage  = FS_LoadU32LE(&pBuffer[80]);
      PagesPerBlock = FS_LoadU32LE(&pBuffer[92]);
      NumBlocks     = FS_LoadU32LE(&pBuffer[96]);
      NumAddrBytes  = pBuffer[101];
      //
      // Store the information needed by the physical layer at runtime.
      //
      if (Features & 1) {
        pInst->DataBusWidth = 16;
        _InitDataBus16();
      }
      pInst->NumBytesColAddr = NumAddrBytes >> 4;
      pInst->NumBytesRowAddr = NumAddrBytes & 0x0F;
      //
      // Fill in the info needed by the NAND driver.
      //
      pDevInfo->BPP_Shift    = (U8)_ld(BytesPerPage);
      pDevInfo->PPB_Shift    = (U8)_ld(PagesPerBlock);
      pDevInfo->NumBlocks    = (U16)NumBlocks;
    }
    FS__FreeSectorBuffer(pBuffer);
  }
  return r;
}

/*********************************************************************
*
*       _IsWP
*
*   Function description
*     Checks if the device is write protected.
*     This is done by reading bit 7 of the status register.
*     Typical reason for write protection is that either the supply voltage is too low
*     or the /WP-pin is active (low)
*
*   Return value:
*    ==0    Not write protected
*     >0    Write protected
*/
static int _IsWP(U8 Unit) {
  U8 Status;
  PHY_INST * pInst;

  pInst = &_aInst[Unit];
  _SelectDevice(pInst);
  Status = _ReadStatus();
  return (Status & STATUS_WRITE_PROTECTED) ? 0 : 1;
}

/*********************************************************************
*
*       _EnableECC
*
*   Function description
*     Activates the internal ECC engine of NAND flash.
*
*   Return value
*     ==0     ECC compuation activated
*     !=0     An error occurred
*
*   Note
*       (1) A read-modify-write operation is required since more than one feature is stored in a parameter.
*/
static int _EnableECC(U8 Unit) {
  PHY_INST * pInst;
  U8         aPara[4];
  int        r;

  pInst = &_aInst[Unit];
  _SelectDevice(pInst);
  r = _GetFeatures(pInst, MICRON_ECC_FEATURE_ADDR, aPara);     // Note 1
  if (r == 0) {
    aPara[0] |= MICRON_ECC_FEATURE_MASK;
    r = _SetFeatures(pInst, MICRON_ECC_FEATURE_ADDR, aPara);
  }
  if (r) {
    _Reset();
  }
  return r;
}

/*********************************************************************
*
*       _DisableECC
*
*   Function description
*     Deactivates the internal ECC engine of NAND flash.
*
*   Return value
*     ==0     ECC compuation deactivated
*     !=0     An error occurred
*
*   Note
*       (1) A read-modify-write operation is required since more than one feature is stored in a parameter.
*/
static int _DisableECC(U8 Unit) {
  PHY_INST * pInst;
  U8         aPara[4];
  int        r;

  pInst = &_aInst[Unit];
  _SelectDevice(pInst);
  r = _GetFeatures(pInst, MICRON_ECC_FEATURE_ADDR, aPara);    // Note 1
  if (r == 0) {
    aPara[0] &= ~MICRON_ECC_FEATURE_MASK;
    r = _SetFeatures(pInst, MICRON_ECC_FEATURE_ADDR, aPara);
  }
  if (r) {
    _Reset();
  }
  return r;
}

/*********************************************************************
*
*       Public const
*
**********************************************************************
*/
const FS_NAND_PHY_TYPE FS_NAND_PHY_Freescale_K70 = {
  _EraseBlock,
  _InitGetDeviceInfo,
  _IsWP,
  _Read,
  _ReadEx,
  _Write,
  _WriteEx,
  _EnableECC,
  _DisableECC
};


/*************************** End of file ****************************/
