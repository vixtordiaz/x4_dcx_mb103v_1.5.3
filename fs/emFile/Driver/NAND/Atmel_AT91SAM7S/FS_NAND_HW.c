/*********************************************************************
*                SEGGER MICROCONTROLLER GmbH & Co. KG                *
*        Solutions for real time microcontroller applications        *
**********************************************************************
*                                                                    *
*        (c) 2003-2012     SEGGER Microcontroller GmbH & Co KG       *
*                                                                    *
*        Internet: www.segger.com    Support:  support@segger.com    *
*                                                                    *
**********************************************************************

**** emFile file system for embedded applications ****
emFile is protected by international copyright laws. Knowledge of the
source code may not be used to write a similar product. This file may
only be used in accordance with a license and should not be re-
distributed in any way. We appreciate your understanding and fairness.
----------------------------------------------------------------------
----------------------------------------------------------------------
File        : NAND_HW.c
Purpose     : NAND flash hardware layer for Atmel AT91SAM7S
----------------------------------------------------------------------
Known problems or limitations with current version
----------------------------------------------------------------------
None.
---------------------------END-OF-HEADER------------------------------
*/

/*********************************************************************
*
*             #include Section
*
**********************************************************************
*/
#include "FS.h"

#ifdef __ICCARM__
#define OPTIMIZE         /*__ramfunc __interwork */
#else
#define OPTIMIZE
#endif

#include "NAND_X_HW.h"

/*********************************************************************
*
*       #define sfrs
*
**********************************************************************
*/
#define PIOA_BASE       0xFFFFF400
#define PIOA_PER        (*(volatile U32 *)(PIOA_BASE + 0x00)) // (PIOA) PIO Enable Register
#define PIOA_PDR        (*(volatile U32 *)(PIOA_BASE + 0x04)) // (PIOA) PIO Disable Register
#define PIOA_PSR        (*(volatile U32 *)(PIOA_BASE + 0x08)) // (PIOA) PIO Status Register
#define PIOA_OER        (*(volatile U32 *)(PIOA_BASE + 0x10)) // (PIOA) Output Enable Register
#define PIOA_ODR        (*(volatile U32 *)(PIOA_BASE + 0x14)) // (PIOA) Output Disable Registerr
#define PIOA_OSR        (*(volatile U32 *)(PIOA_BASE + 0x18)) // (PIOA) Output Status Register
#define PIOA_IFER       (*(volatile U32 *)(PIOA_BASE + 0x20)) // (PIOA) Input Filter Enable Register
#define PIOA_IFDR       (*(volatile U32 *)(PIOA_BASE + 0x24)) // (PIOA) Input Filter Disable Register
#define PIOA_IFSR       (*(volatile U32 *)(PIOA_BASE + 0x28)) // (PIOA) Input Filter Status Register
#define PIOA_SODR       (*(volatile U32 *)(PIOA_BASE + 0x30)) // (PIOA) Set Output Data Register
#define PIOA_CODR       (*(volatile U32 *)(PIOA_BASE + 0x34)) // (PIOA) Clear Output Data Register
#define PIOA_ODSR       (*(volatile U32 *)(PIOA_BASE + 0x38)) // (PIOA) Output Data Status Register
#define PIOA_PDSR       (*(volatile U32 *)(PIOA_BASE + 0x3C)) // (PIOA) Pin Data Status Register
#define PIOA_IER        (*(volatile U32 *)(PIOA_BASE + 0x40)) // (PIOA) Interrupt Enable Register
#define PIOA_IDR        (*(volatile U32 *)(PIOA_BASE + 0x44)) // (PIOA) Interrupt Disable Register
#define PIOA_IMR        (*(volatile U32 *)(PIOA_BASE + 0x48)) // (PIOA) Interrupt Mask Register
#define PIOA_ISR        (*(volatile U32 *)(PIOA_BASE + 0x4C)) // (PIOA) Interrupt Status Register
#define PIOA_MDER       (*(volatile U32 *)(PIOA_BASE + 0x50)) // (PIOA) Multi-driver Enable Register
#define PIOA_MDDR       (*(volatile U32 *)(PIOA_BASE + 0x54)) // (PIOA) Multi-driver Disable Register
#define PIOA_MDSR       (*(volatile U32 *)(PIOA_BASE + 0x58)) // (PIOA) Multi-driver Status Register
#define PIOA_PPUDR      (*(volatile U32 *)(PIOA_BASE + 0x60)) // (PIOA) Pull-up Disable Register
#define PIOA_PPUER      (*(volatile U32 *)(PIOA_BASE + 0x64)) // (PIOA) Pull-up Enable Register
#define PIOA_PPUSR      (*(volatile U32 *)(PIOA_BASE + 0x68)) // (PIOA) Pull-up Status Register
#define PIOA_ASR        (*(volatile U32 *)(PIOA_BASE + 0x70)) // (PIOA) Select A Register
#define PIOA_BSR        (*(volatile U32 *)(PIOA_BASE + 0x74)) // (PIOA) Select B Register
#define PIOA_ABSR       (*(volatile U32 *)(PIOA_BASE + 0x78)) // (PIOA) AB Select Status Register
#define PIOA_OWER       (*(volatile U32 *)(PIOA_BASE + 0xA0)) // (PIOA) Output Write Enable Register
#define PIOA_OWDR       (*(volatile U32 *)(PIOA_BASE + 0xA4)) // (PIOA) Output Write Disable Register
#define PIOA_OWSR       (*(volatile U32 *)(PIOA_BASE + 0xA8)) // (PIOA) Output Write Status Register

#define PMC_BASE        0xFFFFFC00
#define PMC_SCER        (*(volatile U32 *)(PMC_BASE + 0x00)) // (PMC) System Clock Enable Register
#define PMC_SCDR        (*(volatile U32 *)(PMC_BASE + 0x04)) // (PMC) System Clock Disable Register
#define PMC_SCSR        (*(volatile U32 *)(PMC_BASE + 0x08)) // (PMC) System Clock Status Register
#define PMC_PCER        (*(volatile U32 *)(PMC_BASE + 0x10)) // (PMC) Peripheral Clock Enable Register
#define PMC_PCDR        (*(volatile U32 *)(PMC_BASE + 0x14)) // (PMC) Peripheral Clock Disable Register
#define PMC_PCSR        (*(volatile U32 *)(PMC_BASE + 0x18)) // (PMC) Peripheral Clock Status Register
#define PMC_MOR         (*(volatile U32 *)(PMC_BASE + 0x20)) // (PMC) Main Oscillator Register
#define PMC_MCFR        (*(volatile U32 *)(PMC_BASE + 0x24)) // (PMC) Main Clock  Frequency Register
#define PMC_PLLR        (*(volatile U32 *)(PMC_BASE + 0x2C)) // (PMC) PLL Register
#define PMC_MCKR        (*(volatile U32 *)(PMC_BASE + 0x30)) // (PMC) Master Clock Register
#define PMC_PCKR        (*(volatile U32 *)(PMC_BASE + 0x40)) // (PMC) Programmable Clock Register
#define PMC_IER         (*(volatile U32 *)(PMC_BASE + 0x60)) // (PMC) Interrupt Enable Register
#define PMC_IDR         (*(volatile U32 *)(PMC_BASE + 0x64)) // (PMC) Interrupt Disable Register
#define PMC_SR          (*(volatile U32 *)(PMC_BASE + 0x68)) // (PMC) Status Register
#define PMC_IMR         (*(volatile U32 *)(PMC_BASE + 0x6C)) // (PMC) Interrupt Mask Register


#define PERIPHAL_ID_PIOA        (2)  // Parallel IO Controller A


/*********************************************************************
*
*        #define Macros
*
**********************************************************************
*/

/*********************************************************************
*
*        Macros for setting the pin to configure card
*
*    Please define here the sfr (special function register)
*    of your processor.
*
*
**********************************************************************
*/
/* Port direction registers */
#define SET_DATA2INPUT()     PIOA_ODR  = (1 << 24) | (1 << 25) | (1 << 26) | (1 << 27) | (1 << 28) | (1 << 29)  | (1 << 30) | (1 << 31)
#define SET_DATA2OUTPUT()    PIOA_OER  = (1 << 24) | (1 << 25) | (1 << 26) | (1 << 27) | (1 << 28) | (1 << 29)  | (1 << 30) | (1 << 31)


/*********************************************************************
*
*        defines
*
**********************************************************************
*/
#define NAND_GET_DATA(Data)       { __asm("nop"); Data  = (U8)(PIOA_PDSR >> 24); }
#define NAND_SET_DATA(Data)       (PIOA_ODSR = (((U32)Data) << 24))

#define NAND_SET_ALE()            PIOA_SODR = (1 << 17)
#define NAND_CLR_ALE()            PIOA_CODR = (1 << 17)

#define NAND_SET_CLE()            PIOA_SODR = (1 << 16)
#define NAND_CLR_CLE()            PIOA_CODR = (1 << 16)

#define NAND_SET_CE()             PIOA_SODR = (1 << 20)
#define NAND_CLR_CE()             PIOA_CODR = (1 << 20)

#define NAND_SET_RE()             PIOA_SODR = (1 << 19)
#define NAND_CLR_RE()             PIOA_CODR = (1 << 19)

#define NAND_SET_WE()             PIOA_SODR = (1 << 18)
#define NAND_CLR_WE()             PIOA_CODR = (1 << 18)

/*********************************************************************
*
*       _FlashInit
*/
static void _FlashInit(void) {
  
  //
  //  Enable clock for PIOA
  //
  PMC_PCER = (1 << PERIPHAL_ID_PIOA);
  
  //
  // Setup control pins to output, initially high
  //
  PIOA_SODR = (1 << 18)
            | (1 << 19)
            | (1 << 20);
  //
  //  Set ALE, CLE to low
  //
  PIOA_CODR = (1 << 16)
            | (1 << 17);
  //
  // Set pins to PIO mode
  //
  PIOA_PER  = (1 << 16)
            | (1 << 17)
            | (1 << 18)
            | (1 << 19)
            | (1 << 20);
  //
  // Configure pins as output
  //
  PIOA_OER  = (1 << 16)
            | (1 << 17)
            | (1 << 18)
            | (1 << 19)
            | (1 << 20);
  //
  // disable the pullup resistors
  //
  PIOA_PPUDR = (1 << 16)
             | (1 << 17)
             | (1 << 18)
             | (1 << 19)
             | (1 << 20);
	//
  // Setup busy pin
  //
  PIOA_ODR  = (1 << 15);
	PIOA_PER  = (1 << 15);
  //
  // Setup the data pins as initially all inputs
  //
	PIOA_ODR  = (1 << 24)
           | (1 << 25)
           | (1 << 26)
           | (1 << 27)
           | (1 << 28)
           | (1 << 29)
           | (1 << 30)
           | (1 << 31);

	PIOA_PER  = (1 << 24)
           | (1 << 25)
           | (1 << 26)
           | (1 << 27)
           | (1 << 28)
           | (1 << 29)
           | (1 << 30)
           | (1 << 31);
  //
  // enable the pullup resistors
  //
  PIOA_PPUER = (1 << 24)
           | (1 << 25)
           | (1 << 26)
           | (1 << 27)
           | (1 << 28)
           | (1 << 29)
           | (1 << 30)
           | (1 << 31);
  //
	// Configure the Direct Drive
  //
	PIOA_OWDR  = ~((1 << 24)
           | (1 << 25)
           | (1 << 26)
           | (1 << 27)
           | (1 << 28)
           | (1 << 29)
           | (1 << 30)
           | (1 << 31));
	//
  // Setup so we can write to the data pins simultaneously
  //
  PIOA_OWER  = (1 << 24)
           | (1 << 25)
           | (1 << 26)
           | (1 << 27)
           | (1 << 28)
           | (1 << 29)
           | (1 << 30)
           | (1 << 31);
}


/*********************************************************************
*
*       FS_NAND_HW_X_EnableCE
*/
OPTIMIZE void FS_NAND_HW_X_EnableCE(U8 Unit) {
  NAND_CLR_CE();
}


/*********************************************************************
*
*       FS_NAND_HW_X_DisableCE
*/
OPTIMIZE void FS_NAND_HW_X_DisableCE(U8 Unit) {
  NAND_SET_CE();
}

/*********************************************************************
*
*       FS_NAND_HW_X_SetData
*/
OPTIMIZE void FS_NAND_HW_X_SetDataMode(U8 Unit) {
  FS_USE_PARA(Unit);
  /* nCE low, CLE low, ALE low  */
  NAND_CLR_CLE();
  NAND_CLR_ALE();
}


/*********************************************************************
*
*       FS_NAND_HW_X_SetCmd
*/
OPTIMIZE void FS_NAND_HW_X_SetCmdMode(U8 Unit) {
  FS_USE_PARA(Unit);
  /* nCE low, CLE high, ALE low  */
  NAND_SET_CLE();
  NAND_CLR_ALE();
}

/*********************************************************************
*
*       FS_NAND_HW_X_SetAddr
*/
OPTIMIZE void FS_NAND_HW_X_SetAddrMode(U8 Unit) {
  FS_USE_PARA(Unit);
  /* nCE low, CLE low, ALE high */
  NAND_CLR_CLE();
  NAND_SET_ALE();
}

/*********************************************************************
*
*       FS_NAND_HW_X_Read_x8
*/
OPTIMIZE void FS_NAND_HW_X_Read_x8(U8 Unit, void * p, unsigned NumBytes) {
  U8 * pData;

  pData = (U8 *)p;
  SET_DATA2INPUT();
  do {
    NAND_CLR_RE();     /*  RE is active low */
    NAND_GET_DATA(*pData);
    pData++;
    NAND_SET_RE();    /* disable RE */
  } while (--NumBytes);
}


/*********************************************************************
*
*       FS_NAND_HW_X_Write_x8
*/
OPTIMIZE void FS_NAND_HW_X_Write_x8(U8 Unit, const void * p, unsigned NumBytes) {
  const U8 * pData;

  pData = (const U8 *)p;
  SET_DATA2OUTPUT();
  do {
    NAND_CLR_WE();     /*  WE is active low */
    NAND_SET_DATA(*pData);
    pData++;
    NAND_SET_WE();    /* disable WE */
  } while (--NumBytes);
}

/*********************************************************************
*
*       FS_NAND_HW_X_Init_x8
*/
void FS_NAND_HW_X_Init_x8(U8 Unit) {
 _FlashInit();
}

/*********************************************************************
*
*       FS_NAND_HW_X_WaitWhileBusy
*/

OPTIMIZE int FS_NAND_HW_X_WaitWhileBusy(U8 Unit, unsigned us) {
  while ((PIOA_PDSR & (1 << 15)) == 0);
  return 0;
}


/*********************************************************************
*
*       Dummy function for 16-bit access
*
**********************************************************************
*/

/*********************************************************************
*
*       FS_NAND_HW_X_Read_x16
*/
void FS_NAND_HW_X_Read_x16(U8 Unit, void * pData, unsigned NumBytes) {
}

/*********************************************************************
*
*       FS_NAND_HW_X_Write_x16
*/
void FS_NAND_HW_X_Write_x16(U8 Unit, const void * pData, unsigned NumBytes) {
}

/*********************************************************************
*
*       FS_NAND_HW_X_Init_x16
*/
void FS_NAND_HW_X_Init_x16(U8 Unit) {
}

/**************************** end of file ***************************/

