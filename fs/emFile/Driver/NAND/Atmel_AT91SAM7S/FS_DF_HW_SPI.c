/*********************************************************************
*                SEGGER MICROCONTROLLER GmbH & Co. KG                *
*        Solutions for real time microcontroller applications        *
**********************************************************************
*                                                                    *
*        (c) 2003-2012     SEGGER Microcontroller GmbH & Co KG       *
*                                                                    *
*        Internet: www.segger.com    Support:  support@segger.com    *
*                                                                    *
**********************************************************************

**** emFile file system for embedded applications ****
emFile is protected by international copyright laws. Knowledge of the
source code may not be used to write a similar product. This file may
only be used in accordance with a license and should not be re-
distributed in any way. We appreciate your understanding and fairness.
----------------------------------------------------------------------
----------------------------------------------------------------------
File        : DF_HW_SPI.c
Purpose     : Sample Dataflash hardware layer for Atmel AT91SAM7Sxxx
---------------------------END-OF-HEADER------------------------------
*/

/*********************************************************************
*
*             #include Section
*
**********************************************************************
*/

#include "FS.h"
#include "FS_DF_X_HW.h"

/*********************************************************************
*
*       Defines, sfr
*
**********************************************************************
*/
#define PIOA_BASE 0xfffff400
#define PIOA_PDR  *(volatile U32*) (PIOA_BASE + 0x04) /* PIOA disable register               */
#define PIOA_ASR  *(volatile U32*) (PIOA_BASE + 0x70) /* PIOA "A" peripheral select register */
#define PIOA_BSR  *(volatile U32*) (PIOA_BASE + 0x74) /* PIOA "B" peripheral select register */
#define PIOA_PER  *(volatile U32*) (PIOA_BASE + 0x00) // Enable register, enables PIO function
#define PIOA_OER  *(volatile U32*) (PIOA_BASE + 0x10) // Output enable register, sets to output mode
#define PIOA_ODR  *(volatile U32*) (PIOA_BASE + 0x14) // Output enable register, sets to output mode
#define PIOA_SODR *(volatile U32*) (PIOA_BASE + 0x30) // Set output data
#define PIOA_CODR *(volatile U32*) (PIOA_BASE + 0x34) // Clear output data register
#define PIOA_ODSR *(volatile U32*) (PIOA_BASE + 0x38) // output data status register
#define PIOA_PDSR *(volatile U32*) (PIOA_BASE + 0x3c) // pin data status register
#define PIOA_OWER *(volatile U32*) (PIOA_BASE + 0xA0) // Output write enable register
#define PIOA_OWDR *(volatile U32*) (PIOA_BASE + 0xA4) // Output write disable register

#define PIOA_ID          2

/*      SPI */
#define SPI_BASE  0xFFFE0000
#define SPI_CR    *(volatile U32*) (SPI_BASE + 0x00)
#define SPI_MR    *(volatile U32*) (SPI_BASE + 0x04)
#define SPI_RDR   *(volatile U32*) (SPI_BASE + 0x08)
#define SPI_TDR   *(volatile U32*) (SPI_BASE + 0x0C)
#define SPI_SR    *(volatile U32*) (SPI_BASE + 0x10)
#define SPI_IER   *(volatile U32*) (SPI_BASE + 0x14)
#define SPI_IDR   *(volatile U32*) (SPI_BASE + 0x18)
#define SPI_IMR   *(volatile U32*) (SPI_BASE + 0x1c)
#define SPI_CSR0  *(volatile U32*) (SPI_BASE + 0x30)
#define SPI_CSR1  *(volatile U32*) (SPI_BASE + 0x34)
#define SPI_CSR2  *(volatile U32*) (SPI_BASE + 0x38)
#define SPI_CSR3  *(volatile U32*) (SPI_BASE + 0x3c)

/*      SPI PDC */
#define SPI_PDC_RPR  *(volatile U32*) (SPI_BASE + 0x100)
#define SPI_PDC_RCR  *(volatile U32*) (SPI_BASE + 0x104)
#define SPI_PDC_TPR  *(volatile U32*) (SPI_BASE + 0x108)
#define SPI_PDC_TCR  *(volatile U32*) (SPI_BASE + 0x10c)
#define SPI_PDC_RNPR *(volatile U32*) (SPI_BASE + 0x110)
#define SPI_PDC_RNCR *(volatile U32*) (SPI_BASE + 0x114)
#define SPI_PDC_TNPR *(volatile U32*) (SPI_BASE + 0x118)
#define SPI_PDC_TNCR *(volatile U32*) (SPI_BASE + 0x11c)
#define SPI_PDC_PTCR *(volatile U32*) (SPI_BASE + 0x120)
#define SPI_PDC_PTSR *(volatile U32*) (SPI_BASE + 0x124)

#define SPI_ID          5

/*      Power management controller */
#define PMC_BASE  0xFFFFFC00
#define PMC_SCER  *(volatile U32*) (PMC_BASE + 0x00) // System Clock Enable Register
#define PMC_SCDR  *(volatile U32*) (PMC_BASE + 0x04) // System Clock Disable Register
#define PMC_SCSR  *(volatile U32*) (PMC_BASE + 0x08) // System Clock Status Register

#define PMC_PCER  *(volatile U32*) (PMC_BASE + 0x10)  /* Peripheral clock enable register */
#define PMC_MOR   *(volatile U32*) (PMC_BASE + 0x20)  /* main oscillator register */
#define PMC_PLLR  *(volatile U32*) (PMC_BASE + 0x2c)  /* PLL register */
#define PMC_MCKR  *(volatile U32*) (PMC_BASE + 0x30)  /* Master clock register */
#define PMC_SR    *(volatile U32*) (PMC_BASE + 0x68)  /* status register */
#define PMC_IMR   *(volatile U32*) (PMC_BASE + 0x6C)  /* interrupt mask register */
#define PMC_PCKRDY2 (1 << 10)
#define PMC_PCKRDY1 (1 <<  9)
#define PMC_PCKRDY0 (1 <<  8)
#define PMC_MCKRDY  (1 <<  3)
#define PMC_LOCK    (1 <<  2)
#define PMC_MOSCS   (1 <<  0)
#define PMC_MASK_ALL (PMC_PCKRDY2 | PMC_PCKRDY1 | PMC_PCKRDY0 | \
                      PMC_MCKRDY  | PMC_LOCK    | PMC_MOSCS)

/*********************************************************************
*
*       #define Macros
*
**********************************************************************
*/
#define DF_CS_PIN        11
#define DF_MISO_PIN      12
#define DF_MOSI_PIN      13
#define DF_CLK_PIN       14

/*********************************************************************
*
*       Static data
*
**********************************************************************
*/

/*********************************************************************
*
*       Public code
*
**********************************************************************
*/

/*********************************************************************
*
*       FS_DF_HW_X_Init
*/
int FS_DF_HW_X_Init(U8 Unit) {
  int  SPIFreq = 12000;
  U8   sbcr    = 0x78;
  U32  InFreq;

  SPIFreq = 1000 * SPIFreq;
  if (SPIFreq >= 200000) {
    InFreq = 48000000;
  }
  sbcr = (InFreq + SPIFreq - 1) / SPIFreq;
  //
  // Enable Power for PIOA and SPI block
  //
  PMC_PCER = (1 << SPI_ID)
           | (1 << PIOA_ID);
  //
  // Setup Pins
  //
  PIOA_PER =  0
            | (1 << DF_CS_PIN)
            | (1 << DF_MISO_PIN)
            | (1 << DF_MOSI_PIN)
            | (1 << DF_CLK_PIN)
            ;
  PIOA_SODR = 0
            | (1 << DF_CS_PIN)
            | (1 << DF_MOSI_PIN)
            | (1 << DF_CLK_PIN)
            ;
  PIOA_OER  = 0
            | (1 << DF_CS_PIN)
            | (1 << DF_MOSI_PIN)
            | (1 << DF_CLK_PIN)
            ;
  PIOA_ODR  = 0
            | (1 << DF_MOSI_PIN)
            ;
  PIOA_PDR =  0
            | (1 << DF_MISO_PIN)       // SPI-MISO
            | (1 << DF_MOSI_PIN)       // SPI-MOSI
            | (1 << DF_CLK_PIN)       // SPI-Clock
            ;
  PIOA_ASR  = 0
            |(1 << DF_MISO_PIN)       // SPI-MISO
            |(1 << DF_MOSI_PIN)       // SPI-MOSI
            |(1 << DF_CLK_PIN)       // SPI-Clock
            ;
  //
  // SPI
  //
  SPI_CR    = (1 << 7);      // Software reset
  SPI_MR    = 0
            |(1 << 0)         // 1 : Master mode
            |(0 << 1)         // 0 : Fixed chip select
            |(0 << 2)         // Chip select
            |(0 << 3)         // 0: Use MCLK as clock
            |(1 << 4)         // 1: Fault detection disable
            |(0 << 7)         // 1: Loopback
            |(0 << 16)        // 0000b: Use CS0
            ;
  SPI_CSR0  = 0
            |(1 << 0)         // 1 : Clock polarity of idle is high
            |(0 << 1)         // Clock hase sel
            |(1 << 3)         // Leave CS0 stay low
            |(0 << 4)         // 0000b: 8 bits per transfer
            |(sbcr<< 8)         // 8..15: SCBR: Baud rate divider
            |(0x100000);
              ;
  SPI_CR    = (1 << 0);       // Enable SPI
  return 1;
}

/*********************************************************************
*
*       FS_DF_HW_X_EnableCS
*
*  Description:
*    FS low level function. Sets the card slot active using the
*    chip select (CS) line.
*
*  Parameters:
*    Unit      - Device Index
*
*  Return value:
*    void
*/

void FS_DF_HW_X_EnableCS(U8 Unit) {
  PIOA_CODR  = (1 <<  DF_CS_PIN);       // CS0 on eval board
}

/*********************************************************************
*
*       FS_DF_HW_X_DisableCS
*
*  Description:
*    FS low level function. Sets the card slot inactive using the
*    chip select (CS) line.
*
*  Parameters:
*    Unit      - Device Index
*
*  Return value:
*    void
*/

void FS_DF_HW_X_DisableCS(U8 Unit) {
  PIOA_SODR  = (1 <<  DF_CS_PIN);       // CS0 on eval board
}

/*********************************************************************
*
*       FS_DF_HW_X_Read
*
*  Description:
*    FS low level function. Reads a specified number of bytes from MMC
*    card to buffer.
*
*  Parameters:
*    Unit      - Device Index
*    pData            - Pointer to a data buffer
*    NumBytes         - Number of bytes
*
*  Return value:
*    void
*/

void FS_DF_HW_X_Read(U8 Unit, U8 * pData, int NumBytes) {
  do {
    SPI_TDR = 0xff;
    while ((SPI_SR & (1 << 9)) == 0);
    while ((SPI_SR & (1 << 0)) == 0);
    *pData++ = SPI_RDR;
  } while (--NumBytes);
}

/*********************************************************************
*
*       FS_DF_HW_X_Write
*
*  Description:
*    FS low level function. Writes a specified number of bytes from
*    data buffer to the MMC/SD card.
*
*  Parameters:
*    Unit      - Device Index
*    pData            - Pointer to a data buffer
*    NumBytes         - Number of bytes
*
*  Return value:
*    void
*/

void FS_DF_HW_X_Write(U8 Unit, const U8 * pData, int NumBytes) {
  do {
    SPI_TDR = *pData++;
    while ((SPI_SR & (1 << 9)) == 0);
  } while (--NumBytes);
}

/*************************** End of file ****************************/
