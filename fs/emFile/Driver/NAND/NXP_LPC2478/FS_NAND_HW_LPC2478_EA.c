/*********************************************************************
*                SEGGER MICROCONTROLLER GmbH & Co. KG                *
*        Solutions for real time microcontroller applications        *
**********************************************************************
*                                                                    *
*        (c) 2003-2012     SEGGER Microcontroller GmbH & Co KG       *
*                                                                    *
*        Internet: www.segger.com    Support:  support@segger.com    *
*                                                                    *
**********************************************************************

**** emFile file system for embedded applications ****
emFile is protected by international copyright laws. Knowledge of the
source code may not be used to write a similar product. This file may
only be used in accordance with a license and should not be re-
distributed in any way. We appreciate your understanding and fairness.
----------------------------------------------------------------------
----------------------------------------------------------------------
File        : NAND_HW.c
Purpose     : NAND flash hardware layer for Embedded Artists LPC2478
----------------------------------------------------------------------
Known problems or limitations with current version
----------------------------------------------------------------------
None.
---------------------------END-OF-HEADER------------------------------
*/

/*********************************************************************
*
*       #include Section
*
**********************************************************************
*/
#include "NAND_X_HW.h"
#include "FS_Int.h"       // For FS_MEMCPY
#include <string.h>

/*********************************************************************
*
*       #define Macros
*
**********************************************************************
*/
#define NAND_BASE_ADDR          0x81000000
#define NAND_DATA               (U16 *)(NAND_BASE_ADDR + 0x00000000)
#define NAND_ADDR               (U16 *)(NAND_BASE_ADDR + 0x00080000)
#define NAND_CMD                (U16 *)(NAND_BASE_ADDR + 0x00100000)

/*********************************************************************
*
*       #define sfrs
*
**********************************************************************
*/
#define _PINSEL_BASE_ADDR (0xE002C000)
#define _PINSEL0          *(volatile U32*)(_PINSEL_BASE_ADDR + 0x00)
#define _PINSEL4          *(volatile U32*)(_PINSEL_BASE_ADDR + 0x10)
#define _PINSEL5          *(volatile U32*)(_PINSEL_BASE_ADDR + 0x14)
#define _PINSEL6          *(volatile U32*)(_PINSEL_BASE_ADDR + 0x18)
#define _PINSEL7          *(volatile U32*)(_PINSEL_BASE_ADDR + 0x1C)
#define _PINSEL8          *(volatile U32*)(_PINSEL_BASE_ADDR + 0x20)
#define _PINSEL9          *(volatile U32*)(_PINSEL_BASE_ADDR + 0x24)

#define _FP2PIN           *(volatile U32*)(0x3FFFC054)

/****** External memory controller **********************************/
#define _EMC_BASE_ADDR          (0xFFE08000)
#define _EMCCONTROL             *(volatile U32 *)(_EMC_BASE_ADDR + 0x0000)
#define _EMCSTATIC_CONFIG1      *(volatile U32 *)(_EMC_BASE_ADDR + 0x0220)
#define _EMCSTATIC_WAITWEN1     *(volatile U32 *)(_EMC_BASE_ADDR + 0x0224)
#define _EMCSTATIC_WAITOEN1     *(volatile U32 *)(_EMC_BASE_ADDR + 0x0228)
#define _EMCSTATIC_WAITRD1      *(volatile U32 *)(_EMC_BASE_ADDR + 0x022C)
#define _EMCSTATIC_WAITPAGE1    *(volatile U32 *)(_EMC_BASE_ADDR + 0x0230)
#define _EMCSTATIC_WAITWR1      *(volatile U32 *)(_EMC_BASE_ADDR + 0x0234)
#define _EMCSTATIC_WAITTURN1    *(volatile U32 *)(_EMC_BASE_ADDR + 0x0238)

/****** Power, reset clock control unit register ********************/

#define _PLLCON       *(volatile U32*)0xE01FC080
#define _PLLCFG       *(volatile U32*)0xE01FC084
#define _PLLSTAT      *(volatile U32*)0xE01FC088
#define _PLLFEED      *(volatile U32*)0xE01FC08C
#define _PCON         *(volatile U32*)0xE01FC0C0
#define _PCONP        *(volatile U32*)0xE01FC0C4

/*********************************************************************
*
*       Static data
*
**********************************************************************
*/
static U16 * _pCurrentNANDAddr;

/*********************************************************************
*
*       Static code
*
**********************************************************************
*/


/*********************************************************************
*
*       _CopyData8
*/
FS_OPTIMIZE
static void _CopyData8(void * pDest, const void * pSrc, unsigned NumBytes) {
  char * pd;
  const char * ps;
  pd = (char*)pDest;
  ps = (const char*)pSrc;
  //
  // Copy bytes, one at a time
  //
  if (NumBytes) {
    do {
      *(char*)pd++ = *(const char*)ps++;
    } while (--NumBytes);
  }
}

/*********************************************************************
*
*       _CopyData16
*/
FS_OPTIMIZE
static void _CopyData16(void * pDest, const void * pSrc, unsigned NumBytes) {
  char * pd;
  const char * ps;
  pd = (char*)pDest;
  ps = (const char*)pSrc;

  if ((((U32)ps & 1) == 0) && (((U32)pd & 1) == 0)) {
    unsigned NumHWords = NumBytes >> 1;
    if (NumHWords) {
      do {
        *(short *)pd = *(const short *)ps;
        pd += 2;
        ps += 2;
      } while (--NumHWords);
    }
    NumBytes &= 1;
  }
  //
  // Copy bytes, one at a time
  //
  if (NumBytes) {
    do {
      *(char*)pd++ = *(const char*)ps++;
    } while (--NumBytes);
  }
}

/*********************************************************************
*
*       Public code
*
**********************************************************************
*/


/*********************************************************************
*
*       FS_NAND_HW_X_EnableCE
*/
void FS_NAND_HW_X_EnableCE(U8 Unit) {
  FS_USE_PARA(Unit);
  // The CE signal is driven by the externam memory controller.
}

/*********************************************************************
*
*       FS_NAND_HW_X_DisableCE
*/
void FS_NAND_HW_X_DisableCE(U8 Unit) {
  FS_USE_PARA(Unit);
  // The CE signal is driven by the externam memory controller.
}


/*********************************************************************
*
*       FS_NAND_HW_X_SetData
*/
void FS_NAND_HW_X_SetDataMode(U8 Unit) {
  FS_USE_PARA(Unit);
  // CLE low, ALE low
  _pCurrentNANDAddr = NAND_DATA;
}


/*********************************************************************
*
*       FS_NAND_HW_X_SetCmd
*/
void FS_NAND_HW_X_SetCmdMode(U8 Unit) {
  FS_USE_PARA(Unit);
  //CLE high, ALE low
  _pCurrentNANDAddr = NAND_CMD;
}

/*********************************************************************
*
*       FS_NAND_HW_X_SetAddr
*/
void FS_NAND_HW_X_SetAddrMode(U8 Unit) {
  FS_USE_PARA(Unit);
  // CLE low, ALE high
  _pCurrentNANDAddr = NAND_ADDR;
}

/*********************************************************************
*
*       FS_NAND_HW_X_Read_x8
*/
FS_OPTIMIZE
void FS_NAND_HW_X_Read_x8(U8 Unit, void * pData, unsigned NumBytes) {
  FS_USE_PARA(Unit);
  _CopyData8(pData, _pCurrentNANDAddr, NumBytes);
}

/*********************************************************************
*
*       FS_NAND_HW_X_Read_x16
*/
FS_OPTIMIZE
void FS_NAND_HW_X_Read_x16(U8 Unit, void * pData, unsigned NumBytes) {
  FS_USE_PARA(Unit);
  _CopyData16(pData, _pCurrentNANDAddr, NumBytes);
}

/*********************************************************************
*
*       FS_NAND_HW_X_Write_x8
*/
FS_OPTIMIZE
void FS_NAND_HW_X_Write_x8(U8 Unit, const void * pData, unsigned NumBytes) {
  FS_USE_PARA(Unit);
  _CopyData8(_pCurrentNANDAddr, pData, NumBytes);
}

/*********************************************************************
*
*       FS_NAND_HW_X_Write_x16
*/
FS_OPTIMIZE
void FS_NAND_HW_X_Write_x16(U8 Unit, const void * pData, unsigned NumBytes) {
  FS_USE_PARA(Unit);
  _CopyData16(_pCurrentNANDAddr, pData, NumBytes);
}

/*********************************************************************
*
*       FS_NAND_HW_X_Init_x8
*/
void FS_NAND_HW_X_Init_x8(U8 Unit) {
  volatile U32 i;
  U32 Data;

  FS_USE_PARA(Unit);
  _EMCCONTROL  = (1 << 0);    // EMC enable
  _PCONP      |= (1 << 11);   // Turn on EMC peripheral clock
  Data = _PINSEL6;
  //
  //  Remove any previous pin selection for PINSEL6
  //
  Data &= ~(  (3uL <<  0)
            | (3uL <<  2)
            | (3uL <<  4)
            | (3uL <<  6)
            | (3uL <<  8)
            | (3uL << 10)
            | (3uL << 12)
            | (3uL << 14)
            )
            ;
  //
  //  Setup pin to be uased as address/data/control bus pins
  //
  Data |= (1uL <<  0)  // D0
       |  (1uL <<  2)  // D1
       |  (1uL <<  4)  // D2
       |  (1uL <<  6)  // D3
       |  (1uL <<  8)  // D4
       |  (1uL << 10)  // D5
       |  (1uL << 12)  // D6
       |  (1uL << 14)  // D7
       ;
  _PINSEL6 = Data;
  Data = _PINSEL8;
  //
  //  Remove any previous pin selection for PINSEL8
  //
  Data &=  ~(  (3uL <<  0)  // A0
             | (3uL <<  2)  // A1
             | (3uL <<  4)  // A2
             | (3uL <<  6)  // A3
             | (3uL <<  8)  // A4
             | (3uL << 10)  // A5
             | (3uL << 12)  // A6
             | (3uL << 14)  // A7
             | (3uL << 16)  // A8
             | (3uL << 18)  // A9
             | (3uL << 20)  // A10
             | (3uL << 22)  // A11
             | (3uL << 24)  // A12
             | (3uL << 26)  // A13
             | (3uL << 28)  // A14
             | (3uL << 30)  // A15
             )
             ;
  //
  //  Setup pin to be uased as address/data/control bus pins
  //
  Data |=  (1uL <<  0)  // A0
       | (1uL <<  2)    // A1
       | (1uL <<  4)    // A2
       | (1uL <<  6)    // A3
       | (1uL <<  8)    // A4
       | (1uL << 10)    // A5
       | (1uL << 12)    // A6
       | (1uL << 14)    // A7
       | (1uL << 16)    // A8
       | (1uL << 18)    // A9
       | (1uL << 20)    // A10
       | (1uL << 22)    // A11
       | (1uL << 24)    // A12
       | (1uL << 26)    // A13
       | (1uL << 28)    // A14
       | (1uL << 30)    // A15
       ;
  _PINSEL8 = Data;
  Data = _PINSEL9;
  //
  //  Remove any previous pin selection for PINSEL9
  //
  Data   &=  ~(  (3uL <<  0)  // A16
               | (3uL <<  2)  // A17
               | (3uL <<  4)  // A18
               | (3uL <<  6)  // A19
               | (3uL <<  8)  // A20
               | (3uL << 10)  // A21
               | (3uL << 12)  // A22
               | (3uL << 14)  // A23
               | (3uL << 16)  // OE
               | (3uL << 18)  // WE
               | (3uL << 20)  // BLS0
               | (3uL << 22)  // BLS1
               | (3uL << 28)  // CS0
               | (3uL << 30)  // P4.31 (NAND CE)
               )
               ;
  //
  //  Setup pin to be uased as address/data/control bus pins
  //
  Data  |=  (1uL <<  0)  // A16
        |   (1uL <<  2)  // A17
        |   (1uL <<  4)  // A18
        |   (1uL <<  6)  // A19
        |   (1uL <<  8)  // A20
        |   (1uL << 10)  // A21
        |   (1uL << 12)  // A22
        |   (1uL << 14)  // A23
        |   (1uL << 16)  // OE
        |   (1uL << 18)  // WE
        |   (1uL << 20)  // BLS0
        |   (1uL << 22)  // BLS1
        |   (1uL << 28)  // CS0
        |   (1uL << 30)  // CS1 -> NAND CE
        ;
  _PINSEL9 = Data;
  //
  // Configure CS1 bus timing
  //
  _EMCSTATIC_WAITWEN1  = 0x02;       // n + 1 CCLK cycle delay between assertion of chip select and write enable
  _EMCSTATIC_WAITOEN1  = 0x02;       // n cycle delay.
  _EMCSTATIC_WAITRD1   = 0x08;       // n + 1 CCLK cycle delay
  _EMCSTATIC_WAITPAGE1 = 0x1F;       // 32 CCLK cycle read access time
  _EMCSTATIC_WAITWR1   = 0x08;       // n + 2 CCLK cycle write access time
  _EMCSTATIC_WAITTURN1 = 0x0F;       // 16 CCLK turnaround cycles
  _EMCSTATIC_CONFIG1   = (1 << 7);   // Bit lane state
  //
  // Delay, at least 10 us
  //
  for (i = 0; i < (50 * 10); i++) {  // Delay
  }
#if 0 // Normally not needed, since default state of pin is GPIO mode
  //
  //  Setup NAND Busy pin
  //
  _PINSEL5 &= ~(3 << 10);
#endif
}

/*********************************************************************
*
*       FS_NAND_HW_X_Init_x16
*/
void FS_NAND_HW_X_Init_x16(U8 Unit) {
  FS_NAND_HW_X_Init_x8(Unit);
}

/*********************************************************************
*
*       FS_NAND_HW_X_WaitWhileBusy
*
*   Function description
*     This function is called to check the busy pin of
*     the NAND flash.
*     When no busy pin is available this function should
*     return 1, so the upper layer can check if NAND
*     flash is busy.
*/
int FS_NAND_HW_X_WaitWhileBusy(U8 Unit, unsigned us) {
  FS_USE_PARA(Unit);
  FS_USE_PARA(us);
  //
  // If pin is used for an alternate function, we need to return 1
  // in order to do a soft-wait
  //
  if (_PINSEL5 & (3 << 10)) {
    return 1;
  }
  //
  // Check the pin and as soon as it is high, NAND chip is
  while((_FP2PIN & (1 << 21)) == 0);
  return 0;
}

/**************************** end of file ***************************/

