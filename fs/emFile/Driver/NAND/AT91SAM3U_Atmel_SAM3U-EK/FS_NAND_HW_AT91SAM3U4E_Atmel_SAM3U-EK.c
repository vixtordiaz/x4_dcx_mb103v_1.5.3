/*********************************************************************
*                SEGGER MICROCONTROLLER GmbH & Co. KG                *
*        Solutions for real time microcontroller applications        *
**********************************************************************
*                                                                    *
*        (c) 2003-2012     SEGGER Microcontroller GmbH & Co KG       *
*                                                                    *
*        Internet: www.segger.com    Support:  support@segger.com    *
*                                                                    *
**********************************************************************

**** emFile file system for embedded applications ****
emFile is protected by international copyright laws. Knowledge of the
source code may not be used to write a similar product. This file may
only be used in accordance with a license and should not be re-
distributed in any way. We appreciate your understanding and fairness.
----------------------------------------------------------------------
----------------------------------------------------------------------
File        : NAND_HW.c
Purpose     : NAND flash hardware layer for Atmel AT91SAM9263
----------------------------------------------------------------------
Known problems or limitations with current version
----------------------------------------------------------------------
None.
---------------------------END-OF-HEADER------------------------------
*/

/*********************************************************************
*
*       #include Section
*
**********************************************************************
*/
#include "NAND_X_HW.h"
#include "FS_Int.h"       // For FS_MEMCPY
#include <string.h>
/*********************************************************************
*
*       #include Section
*
**********************************************************************
*/
#ifdef __ICCARM__
  #define OPTIMIZE         __ramfunc
#else
  #define OPTIMIZE
#endif


/*********************************************************************
*
*       #define Macros
*
**********************************************************************
*/
#define NAND_BASE_ADDR          0x60000000
#define NAND_DATA               (U16 *)(NAND_BASE_ADDR + 0x1000000)
#define NAND_ADDR               (U16 *)(NAND_BASE_ADDR + 0x1200000)
#define NAND_CMD                (U16 *)(NAND_BASE_ADDR + 0x1400000)

#define NAND_CE_PIN             12
#define NAND_RB_PIN             24
/*********************************************************************
*
*       #define sfrs
*
**********************************************************************
*/

#define SMC_BASE_ADDR    0x400E0000
#define SMC_SETUP_CS1       (*(volatile U32*)(SMC_BASE_ADDR + 1 * 0x14 + 0x70 + 0x00))  // SMC CS1 Setup Register
#define SMC_PULSE_CS1       (*(volatile U32*)(SMC_BASE_ADDR + 1 * 0x14 + 0x70 + 0x04))  // SMC CS1 Pulse Register
#define SMC_CYCLE_CS1       (*(volatile U32*)(SMC_BASE_ADDR + 1 * 0x14 + 0x70 + 0x08))  // SMC CS1 Cycle Register
#define SMC_TIMINGS_CS1     (*(volatile U32*)(SMC_BASE_ADDR + 1 * 0x14 + 0x70 + 0x0C))  // SMC CS1 Timing Register
#define SMC_MODE_CS1        (*(volatile U32*)(SMC_BASE_ADDR + 1 * 0x14 + 0x70 + 0x10))  // SMC CS1 Mode  Register

#define PMC_BASE_ADDR    0x400E0400
#define PMC_PCER         (*(volatile U32 *)(PMC_BASE_ADDR + 0x10)) // (PMC) Peripheral Clock Enable Register
#define PMC_PCDR         (*(volatile U32 *)(PMC_BASE_ADDR + 0x14)) // (PMC) Peripheral Clock Disable Register

// ========== Register definition for PIOB peripheral ==========
#define PIOB_BASE        0x400E0E00
#define PIOB_PER         (*(volatile U32 *)(PIOB_BASE + 0x00)) // (PIOB) PIO Enable Register
#define PIOB_PDR         (*(volatile U32 *)(PIOB_BASE + 0x04)) // (PIOB) PIO Disable Register
#define PIOB_PSR         (*(volatile U32 *)(PIOB_BASE + 0x08)) // (PIOB) PIO Status Register
#define PIOB_OER         (*(volatile U32 *)(PIOB_BASE + 0x10)) // (PIOB) Output Enable Register
#define PIOB_ODR         (*(volatile U32 *)(PIOB_BASE + 0x14)) // (PIOB) Output Disable Registerr
#define PIOB_OSR         (*(volatile U32 *)(PIOB_BASE + 0x18)) // (PIOB) Output Status Register
#define PIOB_IFER        (*(volatile U32 *)(PIOB_BASE + 0x20)) // (PIOB) Input Filter Enable Register
#define PIOB_IFDR        (*(volatile U32 *)(PIOB_BASE + 0x24)) // (PIOB) Input Filter Disable Register
#define PIOB_IFSR        (*(volatile U32 *)(PIOB_BASE + 0x28)) // (PIOB) Input Filter Status Register
#define PIOB_SODR        (*(volatile U32 *)(PIOB_BASE + 0x30)) // (PIOB) Set Output Data Register
#define PIOB_CODR        (*(volatile U32 *)(PIOB_BASE + 0x34)) // (PIOB) Clear Output Data Register
#define PIOB_ODSR        (*(volatile U32 *)(PIOB_BASE + 0x38)) // (PIOB) Output Data Status Register
#define PIOB_PDSR        (*(volatile U32 *)(PIOB_BASE + 0x3C)) // (PIOB) Pin Data Status Register
#define PIOB_IER         (*(volatile U32 *)(PIOB_BASE + 0x40)) // (PIOB) Interrupt Enable Register
#define PIOB_IDR         (*(volatile U32 *)(PIOB_BASE + 0x44)) // (PIOB) Interrupt Disable Register
#define PIOB_IMR         (*(volatile U32 *)(PIOB_BASE + 0x48)) // (PIOB) Interrupt Mask Register
#define PIOB_ISR         (*(volatile U32 *)(PIOB_BASE + 0x4C)) // (PIOB) Interrupt Status Register
#define PIOB_MDER        (*(volatile U32 *)(PIOB_BASE + 0x50)) // (PIOB) Multi-driver Enable Register
#define PIOB_MDDR        (*(volatile U32 *)(PIOB_BASE + 0x54)) // (PIOB) Multi-driver Disable Register
#define PIOB_MDSR        (*(volatile U32 *)(PIOB_BASE + 0x58)) // (PIOB) Multi-driver Status Register
#define PIOB_PPUDR       (*(volatile U32 *)(PIOB_BASE + 0x60)) // (PIOB) Pull-up Disable Register
#define PIOB_PPUER       (*(volatile U32 *)(PIOB_BASE + 0x64)) // (PIOB) Pull-up Enable Register
#define PIOB_PPUSR       (*(volatile U32 *)(PIOB_BASE + 0x68)) // (PIOB) Pull-up Status Register
#define PIOB_ABSR        (*(volatile U32 *)(PIOB_BASE + 0x70)) // (PIOB) AB Select Status Register
#define PIOB_OWER        (*(volatile U32 *)(PIOB_BASE + 0xA0)) // (PIOB) Output Write Enable Register
#define PIOB_OWDR        (*(volatile U32 *)(PIOB_BASE + 0xA4)) // (PIOB) Output Write Disable Register
#define PIOB_OWSR        (*(volatile U32 *)(PIOB_BASE + 0xA8)) // (PIOB) Output Write Status Register


// ========== Register definition for PIOC peripheral ==========
#define PIOC_BASE        0x400E1000
#define PIOC_PER         (*(volatile U32 *)(PIOC_BASE + 0x00)) // (PIOC) PIO Enable Register
#define PIOC_PDR         (*(volatile U32 *)(PIOC_BASE + 0x04)) // (PIOC) PIO Disable Register
#define PIOC_PSR         (*(volatile U32 *)(PIOC_BASE + 0x08)) // (PIOC) PIO Status Register
#define PIOC_OER         (*(volatile U32 *)(PIOC_BASE + 0x10)) // (PIOC) Output Enable Register
#define PIOC_ODR         (*(volatile U32 *)(PIOC_BASE + 0x14)) // (PIOC) Output Disable Registerr
#define PIOC_OSR         (*(volatile U32 *)(PIOC_BASE + 0x18)) // (PIOC) Output Status Register
#define PIOC_IFER        (*(volatile U32 *)(PIOC_BASE + 0x20)) // (PIOC) Input Filter Enable Register
#define PIOC_IFDR        (*(volatile U32 *)(PIOC_BASE + 0x24)) // (PIOC) Input Filter Disable Register
#define PIOC_IFSR        (*(volatile U32 *)(PIOC_BASE + 0x28)) // (PIOC) Input Filter Status Register
#define PIOC_SODR        (*(volatile U32 *)(PIOC_BASE + 0x30)) // (PIOC) Set Output Data Register
#define PIOC_CODR        (*(volatile U32 *)(PIOC_BASE + 0x34)) // (PIOC) Clear Output Data Register
#define PIOC_ODSR        (*(volatile U32 *)(PIOC_BASE + 0x38)) // (PIOC) Output Data Status Register
#define PIOC_PDSR        (*(volatile U32 *)(PIOC_BASE + 0x3C)) // (PIOC) Pin Data Status Register
#define PIOC_IER         (*(volatile U32 *)(PIOC_BASE + 0x40)) // (PIOC) Interrupt Enable Register
#define PIOC_IDR         (*(volatile U32 *)(PIOC_BASE + 0x44)) // (PIOC) Interrupt Disable Register
#define PIOC_IMR         (*(volatile U32 *)(PIOC_BASE + 0x48)) // (PIOC) Interrupt Mask Register
#define PIOC_ISR         (*(volatile U32 *)(PIOC_BASE + 0x4C)) // (PIOC) Interrupt Status Register
#define PIOC_MDER        (*(volatile U32 *)(PIOC_BASE + 0x50)) // (PIOC) Multi-driver Enable Register
#define PIOC_MDDR        (*(volatile U32 *)(PIOC_BASE + 0x54)) // (PIOC) Multi-driver Disable Register
#define PIOC_MDSR        (*(volatile U32 *)(PIOC_BASE + 0x58)) // (PIOC) Multi-driver Status Register
#define PIOC_PPUDR       (*(volatile U32 *)(PIOC_BASE + 0x60)) // (PIOC) Pull-up Disable Register
#define PIOC_PPUER       (*(volatile U32 *)(PIOC_BASE + 0x64)) // (PIOC) Pull-up Enable Register
#define PIOC_PPUSR       (*(volatile U32 *)(PIOC_BASE + 0x68)) // (PIOC) Pull-up Status Register
#define PIOC_ABSR        (*(volatile U32 *)(PIOC_BASE + 0x70)) // (PIOC) AB Select Status Register
#define PIOC_OWER        (*(volatile U32 *)(PIOC_BASE + 0xA0)) // (PIOC) Output Write Enable Register
#define PIOC_OWDR        (*(volatile U32 *)(PIOC_BASE + 0xA4)) // (PIOC) Output Write Disable Register
#define PIOC_OWSR        (*(volatile U32 *)(PIOC_BASE + 0xA8)) // (PIOC) Output Write Status Register

#define PERIPHAL_ID_SMC         ( 9)  // Static memory controller
#define PERIPHAL_ID_PIOA        (10)  // Parallel IO Controller A
#define PERIPHAL_ID_PIOB        (11)  // Parallel IO Controller B
#define PERIPHAL_ID_PIOC        (12)  // Parallel IO Controller C

/*********************************************************************
*
*       Static data
*
**********************************************************************
*/
static U16 * _pCurrentNANDAddr;
/*********************************************************************
*
*       Static code
*
**********************************************************************
*/


/*********************************************************************
*
*       Public code
*
**********************************************************************
*/


/*********************************************************************
*
*       FS_NAND_HW_X_EnableCE
*/
OPTIMIZE void FS_NAND_HW_X_EnableCE(U8 Unit) {
  PIOC_CODR = (1 << NAND_CE_PIN); // Enable NAND CE
}

/*********************************************************************
*
*       FS_NAND_HW_X_DisableCE
*/
OPTIMIZE void FS_NAND_HW_X_DisableCE(U8 Unit) {
  PIOC_SODR = (1 << NAND_CE_PIN); // Disable NAND CE
}


/*********************************************************************
*
*       FS_NAND_HW_X_SetData
*/
OPTIMIZE void FS_NAND_HW_X_SetDataMode(U8 Unit) {
  FS_USE_PARA(Unit);
  // CLE low, ALE low
  _pCurrentNANDAddr = NAND_DATA;
}


/*********************************************************************
*
*       FS_NAND_HW_X_SetCmd
*/
OPTIMIZE void FS_NAND_HW_X_SetCmdMode(U8 Unit) {
  FS_USE_PARA(Unit);
  //CLE high, ALE low
  _pCurrentNANDAddr = NAND_CMD;
}

/*********************************************************************
*
*       FS_NAND_HW_X_SetAddr
*/
OPTIMIZE void FS_NAND_HW_X_SetAddrMode(U8 Unit) {
  FS_USE_PARA(Unit);
  // CLE low, ALE high
  _pCurrentNANDAddr = NAND_ADDR;
}

/*********************************************************************
*
*       FS_NAND_HW_X_Read_x8
*/
void FS_NAND_HW_X_Read_x8(U8 Unit, void * pData, unsigned NumBytes) {
  FS_MEMCPY(pData, _pCurrentNANDAddr, NumBytes);
}

/*********************************************************************
*
*       FS_NAND_HW_X_Read_x16
*/
void FS_NAND_HW_X_Read_x16(U8 Unit, void * pData, unsigned NumBytes) {
  FS_MEMCPY(pData, _pCurrentNANDAddr, NumBytes);
}

/*********************************************************************
*
*       FS_NAND_HW_X_Write_x8
*/
void FS_NAND_HW_X_Write_x8(U8 Unit, const void * pData, unsigned NumBytes) {
  FS_MEMCPY(_pCurrentNANDAddr, pData, NumBytes);
}

/*********************************************************************
*
*       FS_NAND_HW_X_Write_x16
*/
void FS_NAND_HW_X_Write_x16(U8 Unit, const void * pData, unsigned NumBytes) {
  FS_MEMCPY(_pCurrentNANDAddr, pData, NumBytes);
}


/*********************************************************************
*
*       FS_NAND_HW_X_Init_x8
*/
void FS_NAND_HW_X_Init_x8(U8 Unit) {
  //
  // Enable clock for SMC
  //
  PMC_PCER        = (1 << PERIPHAL_ID_SMC);
  SMC_SETUP_CS1   = 0x00010001;
  SMC_PULSE_CS1   = 0x04030302;
  SMC_CYCLE_CS1   = 0x00070005;
  SMC_TIMINGS_CS1 = (1 <<  0)                      // CLE to REN
                  | (2 <<  4)                      // ALE to Data
                  | (1 <<  8)                      // ALE to REN
                  | (1 << 16)                      // Ready to REN
                  | (2 << 24)                      // WEN to REN
                  | (7 << 28)                      // Ready/Busy Line Selection
                  | (1 << 31)                      // Use Nand Flash Timing
                  ;
  SMC_MODE_CS1    =  (0 << 12)     // DBW: 0: 8-bit NAND, 1: 16-bit NAND
                  |  (3 <<  0);    // Use NRD & NWE signals for read / write
  //
  // Enable clocks for PIOA, PIOD
  //
  PMC_PCER = ((1 << PERIPHAL_ID_PIOB) |  (1 << PERIPHAL_ID_PIOC));
  //
  //  Set PIO pin x as port pin, for use as NAND Ready/Busy line.
  //
  PIOB_ODR  = 1 << NAND_RB_PIN;    // Configure input
  PIOB_PER  = 1 << NAND_RB_PIN;    // Set pin as port pin
  //
  //  Set PIO pin x as port pin (output), for use as NAND CS.
  //
  PIOC_SODR = 1 << NAND_CE_PIN;    // Set pin high
  PIOC_OER  = 1 << NAND_CE_PIN;    // Configure as output
  PIOC_PER  = 1 << NAND_CE_PIN;    // Set pin as port pin
  //
  //  Set different port pins to alternate function
  //
  PIOB_ABSR &= ~(  (1 << 17)            // PIOx.pin17  -> NAND_OE
                |  (1 << 18)            // PIOx.pin18  -> NAND_WE
                |  (1 << 21)            // PIOx.pin21  -> NAND_ALE
                |  (1 << 22)            // PIOx.pin22  -> NAND_CLE
                )
            ;
  PIOB_PDR =  (1 << 17)            // PIOC.pin17  -> NAND_OE
           |  (1 << 18)            // PIOx.pin18  -> NAND_WE
           |  (1 << 21)            // PIOx.pin21  -> NAND_ALE
           |  (1 << 22)            // PIOx.pin22  -> NAND_CLE
           ;
  PIOB_ABSR &= ~0xfe01fe00;           // Set pins to alternate function A -> databus
  PIOB_ABSR = (1 << 6);             // Set pins to alternate function B -> databus
  PIOB_PDR =  0xfe01fe00           // Disable port pin function
           |  (1 <<  6)            // Disable port pin function
           ;
  
}

/*********************************************************************
*
*       FS_NAND_HW_X_Init_x16
*/
void FS_NAND_HW_X_Init_x16(U8 Unit) {
  FS_NAND_HW_X_Init_x8(Unit);
  SMC_MODE_CS1  = (1 << 12)     // DBW: 0: 8-bit NAND, 1: 16-bit NAND
                | (3 << 0);     // Use NRD & NWE signals for read / write
}

/*********************************************************************
*
*             FS_NAND_HW_X_WaitWhileBusy
*/
OPTIMIZE int FS_NAND_HW_X_WaitWhileBusy(U8 Unit, unsigned us) {
  while ((PIOB_PDSR & (1 << NAND_RB_PIN)) == 0);
  return 0;
}

/**************************** end of file ***************************/

