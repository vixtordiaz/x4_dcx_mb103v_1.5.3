/*********************************************************************
*                SEGGER MICROCONTROLLER GmbH & Co. KG                *
*        Solutions for real time microcontroller applications        *
**********************************************************************
*                                                                    *
*        (c) 2003-2012     SEGGER Microcontroller GmbH & Co KG       *
*                                                                    *
*        Internet: www.segger.com    Support:  support@segger.com    *
*                                                                    *
**********************************************************************

**** emFile file system for embedded applications ****
emFile is protected by international copyright laws. Knowledge of the
source code may not be used to write a similar product. This file may
only be used in accordance with a license and should not be re-
distributed in any way. We appreciate your understanding and fairness.
----------------------------------------------------------------------
----------------------------------------------------------------------
File   : ide_X_hw.c
Purpose: IDE hardware layer for LH79520 with 16bit memory mapped layout
         for Compact Flash
----------------------------------------------------------------------
Known problems or limitations with current version
----------------------------------------------------------------------
None.
-------- END-OF-HEADER -----------------------------------------------
*/

/*********************************************************************
*
*       #include Section
*
**********************************************************************
*/

#include "IDE_X_HW.h"
#include "FS.h" 

/*********************************************************************
*
*       defines, configurable
*
**********************************************************************
*/
#define IDE_BASEADDRESS  0x50201000

/*********************************************************************
*
*       Static code
*
**********************************************************************
*/

/*********************************************************************
*
*       _Getp
*/
static volatile U16 * _Getp(unsigned Off) {
  volatile U16 * p;

  p = (volatile U16 *)(IDE_BASEADDRESS + Off);
  return p;
}

/*********************************************************************
*
*       Public code
*
**********************************************************************
*/
/*********************************************************************
*
*       FS_IDE_HW_ReadReg
*
*   Description:
*     Reads an IDE register. Data from the IDE register are read 16-bit wide.
* 
*   Parameters:
*     Unit       - Unit number.
*     AddrOff    - Address offset that specifies which IDE register should be read
*  
*   Return value:
*     Data read from the IDE register.
*/
U16  FS_IDE_HW_ReadReg(U8 Unit, unsigned AddrOff) {
  volatile U16 * pIdeReg;
  
  FS_USE_PARA(Unit);
  pIdeReg = _Getp(AddrOff);
  return *pIdeReg;
}

/*********************************************************************
*
*       FS_IDE_HW_WriteReg
*
*   Description:
*     Write an IDE register. Data to the IDE register are written 16-bit wide.
* 
*   Parameters:
*     Unit       - Unit number.
*     AddrOff    - Address offset that specifies which IDE register should be written
*  
*/
void FS_IDE_HW_WriteReg(U8 Unit, unsigned AddrOff, U16 Data) {
  volatile U16 * pIdeReg;
  
  FS_USE_PARA(Unit);
  pIdeReg = _Getp(AddrOff);
  *pIdeReg = Data;
}

/*********************************************************************
*
*       FS_IDE_HW_ReadData
*
*   Description:
*     Reads data from the IDE data register.
* 
*   Parameters:
*     Unit       - Unit number.
*     AddrOff    - Address offset that specifies which IDE register should be read
*     pData      - Pointer to a read buffer
*     NumBytes   - Number of bytes that should be read.
*  
*/
void FS_IDE_HW_ReadData(U8 Unit, U8 * pData, unsigned NumBytes) {
  unsigned       NumItems;
  volatile U16 * pIdeReg;
  U16          * pData16;
  
  FS_USE_PARA(Unit);
  pIdeReg = _Getp(0x08);
  NumItems = NumBytes >> 1;
  pData16 = (U16 *)pData;
  do {
    *pData16++ = *pIdeReg;
  } while (--NumItems);
}

/*********************************************************************
*
*       FS_IDE_HW_WriteData
*
*   Description:
*     Writes data to the IDE data register.
* 
*   Parameters:
*     AddrOff    - Address offset that specifies which IDE register should be read
*     pData      - Pointer to a read buffer
*     NumBytes   - Number of bytes that should be read.
*  
*/
void FS_IDE_HW_WriteData(U8 Unit, const U8 * pData, unsigned NumBytes) {
  unsigned       NumItems;
  volatile U16 * pIdeReg;
  U16          * pData16;
  
  FS_USE_PARA(Unit);
  pIdeReg = _Getp(0x08);
  NumItems = NumBytes >> 1;
  pData16 = (U16 *)pData;
  do {
    *pIdeReg = *pData16++;
  } while (--NumItems);
}

/*********************************************************************
*
*       FS_IDE_HW_IsPresent
*
*   Description:
*     FS driver hardware layer function. Check if the device is present.
* 
*   Parameters:
*     Unit        - Unit number.
*  
*   Return value:
*    FS_MEDIA_STATE_UNKNOWN if the state of the media is unknown.
*    FS_MEDIA_NOT_PRESENT   if no card is present.
*    FS_MEDIA_IS_PRESENT    if a card is present.
*/
int FS_IDE_HW_IsPresent(U8 Unit) {
  FS_USE_PARA(Unit);
  return FS_MEDIA_IS_PRESENT;
}


/*********************************************************************
*
*       FS_IDE_HW_Reset
*
*   Description:
*     FS driver hardware layer function. This function is called, when 
*     the driver detects a new media is present. For ATA HD drives, there 
*     is no action required and this function can be empty.
* 
*   Parameters:
*     Unit        - Unit number.
*   Return value:
*     None.
*/
void FS_IDE_HW_Reset(U8 Unit) {
  FS_USE_PARA(Unit);
}

/*********************************************************************
*
*       FS_IDE_HW_X_Delay400ns
*
*  Description:
*    This function is called whenever a delay of 400 ns is required.
*    After sending a command or sending the parameter to the integrated
*    controller on the IDE/CF drive. Slow cards need a delay of 400ns.
*    New drives are quite fast enough, so that a delay may not be
*    required. So this function can be empty.
*
*  Parameters:
*  Unit        - Unit number.
* 
*  Return value:
*  None.
*/
void FS_IDE_HW_Delay400ns(U8 Unit) {
  FS_USE_PARA(Unit);
}


/****** EOF *********************************************************/

