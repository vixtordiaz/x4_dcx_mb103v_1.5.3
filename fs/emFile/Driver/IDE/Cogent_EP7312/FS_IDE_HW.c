/*********************************************************************
*                SEGGER MICROCONTROLLER GmbH & Co. KG                *
*        Solutions for real time microcontroller applications        *
**********************************************************************
*                                                                    *
*        (c) 2003-2012     SEGGER Microcontroller GmbH & Co KG       *
*                                                                    *
*        Internet: www.segger.com    Support:  support@segger.com    *
*                                                                    *
**********************************************************************

**** emFile file system for embedded applications ****
emFile is protected by international copyright laws. Knowledge of the
source code may not be used to write a similar product. This file may
only be used in accordance with a license and should not be re-
distributed in any way. We appreciate your understanding and fairness.
----------------------------------------------------------------------
----------------------------------------------------------------------
File   : IDE_HW.c
Purpose: IDE hardware layer for Cogent EP7312 True IDE controller
----------------------------------------------------------------------
Known problems or limitations with current version
----------------------------------------------------------------------
None.
-------- END-OF-HEADER -----------------------------------------------
*/

/*********************************************************************
*
*       #include Section
*
**********************************************************************
*/

#include "IDE_X_HW.h"
#include "FS.h" 

/*********************************************************************
*
*       defines, configurable
*
**********************************************************************
*/
#define IDE_BASEADDRESS  0x50000000
/* SFR definition of EP7312 */
#define __PBDR                  *(volatile unsigned char*)0x80000001
#define __PDDR                  *(volatile unsigned char*)0x80000003
#define __PBDDR                 *(volatile unsigned char*)0x80000041
#define __PDDDR                 *(volatile unsigned char*)0x80000043
#define __SYSCON1               *(volatile unsigned int*)0x80000100
#define __SYSFLG1               *(volatile unsigned int*)0x80000140
#define __MEMCFG2               *(volatile unsigned int*)0x800001C0
#define __INTSR1                *(volatile unsigned int*)0x80000240
#define __INTMR1                *(volatile unsigned int*)0x80000280
#define __TC1D                  *(volatile unsigned short*)0x80000300
#define __TC2D                  *(volatile unsigned short*)0x80000340
#define __UARTDR1               *(volatile unsigned short*)0x80000480
#define __UBRLCR1               *(volatile unsigned int*)0x800004C0
#define __TC1EOI                *(volatile unsigned int*)0x800006C0
#define __TC2EOI                *(volatile unsigned int*)0x80000700
#define __SYSFLG2               *(volatile unsigned int*)0x80001140
#define __INTMR2                *(volatile unsigned int*)0x80001280
#define __SYSCON3               *(volatile unsigned int*)0x80002200
#define __PLLW                  *(volatile unsigned int*)0x80002610

/* CSB238 IDE-Bus */
#define __IDE_DATA              *(volatile unsigned short*)0x50000000
#define __IDE_FC                *(volatile unsigned char*)0x50000001
#define __IDE_SC                *(volatile unsigned char*)0x50000002
#define __IDE_SN                *(volatile unsigned char*)0x50000003
#define __IDE_CL                *(volatile unsigned char*)0x50000004
#define __IDE_CH                *(volatile unsigned char*)0x50000005
#define __IDE_DH                *(volatile unsigned char*)0x50000006
#define __IDE_CMD               *(volatile unsigned char*)0x50000007
#define __IDE_DC                *(volatile unsigned char*)0x5000000e
#define __IDE_PIO3              *(volatile unsigned char*)0x50010000

/*********************************************************************
*
*       Static code
*
**********************************************************************
*/

/*********************************************************************
*
*       _Getp
*/
static volatile void * _Getp(unsigned Off) {
  volatile void * p;
  
  if (Off == 0x0C) {
    Off = 0;
  }
  p = (volatile void *)(IDE_BASEADDRESS + Off);
  return p;
}

/*********************************************************************
*
*       Public code
*
**********************************************************************
*/
/*********************************************************************
*
*       FS_IDE_HW_ReadReg
*
*   Description:
*     Reads an IDE register. Data from the IDE register are read 16-bit wide.
* 
*   Parameters:
*     Unit       - Unit number.
*     AddrOff    - Address offset that specifies which IDE register should be read
*  
*   Return value:
*     Data read from the IDE register.
*/
U16  FS_IDE_HW_ReadReg(U8 Unit, unsigned AddrOff) {
  volatile U8 * pIdeReg;
  U16 Data;
  FS_USE_PARA(Unit);

  __MEMCFG2   = 0x1313;     /* CS5 8 bit */
  pIdeReg = _Getp(AddrOff);
  Data = *pIdeReg | (*(pIdeReg + 1) << 8);
  return Data;
}

/*********************************************************************
*
*       FS_IDE_HW_WriteReg
*
*   Description:
*     Write an IDE register. Data to the IDE register are written 16-bit wide.
* 
*   Parameters:
*     Unit       - Unit number.
*     AddrOff    - Address offset that specifies which IDE register should be written
*  
*/
void FS_IDE_HW_WriteReg(U8 Unit, unsigned AddrOff, U16 Data) {
  volatile U8 * pIdeReg;
  
  FS_USE_PARA(Unit);
  __MEMCFG2   = 0x1313;     /* CS5 8 bit */
  pIdeReg     = _Getp(AddrOff);
  *pIdeReg++  = (U8)(Data & 0xff);
  *pIdeReg    = (U8)(Data >> 8);
}

/*********************************************************************
*
*       FS_IDE_HW_ReadData
*
*   Description:
*     Reads data from the IDE data register.
* 
*   Parameters:
*     Unit       - Unit number.
*     AddrOff    - Address offset that specifies which IDE register should be read
*     pData      - Pointer to a read buffer
*     NumBytes   - Number of bytes that should be read.
*  
*/
void FS_IDE_HW_ReadData(U8 Unit, U8 * pData, unsigned NumBytes) {
  unsigned       NumItems;
  volatile U16 * pIdeReg;
  U16          * pData16;
  
  FS_USE_PARA(Unit);
  __MEMCFG2   = 0x1013;     /* CS5 16 bit */
  pIdeReg = _Getp(0);
  NumItems = NumBytes >> 1;
  pData16 = (U16 *)pData;
  do {
    *pData16++ = *pIdeReg;
  } while (--NumItems);
}

/*********************************************************************
*
*       FS_IDE_HW_WriteData
*
*   Description:
*     Writes data to the IDE data register.
* 
*   Parameters:
*     AddrOff    - Address offset that specifies which IDE register should be read
*     pData      - Pointer to a read buffer
*     NumBytes   - Number of bytes that should be read.
*  
*/
void FS_IDE_HW_WriteData(U8 Unit, const U8 * pData, unsigned NumBytes) {
  unsigned       NumItems;
  volatile U16 * pIdeReg;
  U16          * pData16;
  
  FS_USE_PARA(Unit);
  __MEMCFG2   = 0x1013;     /* CS5 16 bit */
  pIdeReg = _Getp(0);
  NumItems = NumBytes >> 1;
  pData16 = (U16 *)pData;
  do {
    *pIdeReg = *pData16++;
  } while (--NumItems);
}

/*********************************************************************
*
*       FS_IDE_HW_IsPresent
*
*   Description:
*     FS driver hardware layer function. Check if the device is present.
* 
*   Parameters:
*     Unit        - Unit number.
*  
*   Return value:
*    FS_MEDIA_STATE_UNKNOWN if the state of the media is unknown.
*    FS_MEDIA_NOT_PRESENT   if no card is present.
*    FS_MEDIA_IS_PRESENT    if a card is present.
*/
int FS_IDE_HW_IsPresent(U8 Unit) {
  FS_USE_PARA(Unit);
  __MEMCFG2  = 0x1f13;        /* CS5 8 bit */
  __SYSCON1 |= 0x40000ul;     /* enable expansion clock */

  return FS_MEDIA_IS_PRESENT;
}


/*********************************************************************
*
*       FS_IDE_HW_Reset
*
*   Description:
*     FS driver hardware layer function. This function is called, when 
*     the driver detects a new media is present. For ATA HD drives, there 
*     is no action required and this function can be empty.
* 
*   Parameters:
*     Unit        - Unit number.
*   Return value:
*     None.
*/
void FS_IDE_HW_Reset(U8 Unit) {
  FS_USE_PARA(Unit);
}

/*********************************************************************
*
*       FS_IDE_HW_X_Delay400ns
*
*  Description:
*    This function is called whenever a delay of 400 ns is required.
*    After sending a command or sending the parameter to the integrated
*    controller on the IDE/CF drive. Slow cards need a delay of 400ns.
*    New drives are quite fast enough, so that a delay may not be
*    required. So this function can be empty.
*
*  Parameters:
*  Unit        - Unit number.
* 
*  Return value:
*  None.
*/
void FS_IDE_HW_Delay400ns(U8 Unit) {
  FS_USE_PARA(Unit);
/*
  volatile U32 Dummy;
  Dummy = 70;
  do {
  } while (--Dummy);
*/
}


/****** EOF *********************************************************/

