/**
  **************** (C) COPYRIGHT 2011 SCT Performance, LLC *********************
  * File Name          : genfs.h
  * Device / System    : SCT products using the "CommonCode" platform
  * Author             : Quyen Leba
  *
  * Version            : 1 
  * Date               : 05/02/2011
  * Description        : 
  *                    : 
  *
  *
  * History            : 05/02/2011 M. Davis
  *                    :   Add SCT Copyright header,
  *
  ******************************************************************************
  */

#ifndef __GENFS_H
#define __GENFS_H

#include <stdlib.h>
#include <stdio.h>
#include <arch/gentype.h>
#include <device_config.h>

#define GENFS_VOLUME_NAME           ""
#define GENFS_DEFAULT_FOLDER_NAME   "STRATEGY"
#define GENFS_USER_FOLDER_NAME      "USER"
#define GENFS_DEFAULT_FOLDER_PATH   "\\"GENFS_DEFAULT_FOLDER_NAME"\\"
#define GENFS_USER_FOLDER_PATH      "\\"GENFS_USER_FOLDER_NAME"\\"
#define GENFS_ENTRYNAMELENGTH_MAX   260
#define GENFS_TOTAL_FOLDER          2

//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
// SEGGER File System
//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
#if USE_SEGGER_EMFILE

#include "FS.h"
#include "FS_Int.h"

#define GENFS_SECTOR_SIZE   FS_MAX_SECTOR_SIZE

#define F_FILE              FS_FILE
#define fopen               FS_FOpen
#define fclose              FS_FClose
#define fwrite              FS_FWrite
#define fread               FS_FRead
#define feof                FS_FEof
#define fseek               FS_FSeek
#define fdelete             FS_Remove
#define frename             FS_Rename
#define fmove               FS_Move
#define ftruncate           FS_Truncate
#define fcopyfile           FS_CopyFile
#define ftell               FS_FTell
#define fsetfileposition    FS_SetFilePosition
#define fseteof             FS_SetEndOfFile
#define fgetfilesize        FS_GetFileSize
#define fwritesectors       FS_WriteSector(GENFS_VOLUME_NAME,
#define freadsectors        FS_ReadSector(GENFS_VOLUME_NAME,

#define F_SEEK_SET          FS_SEEK_SET
#define F_SEEK_CUR          FS_SEEK_CUR
#define F_SEEK_END          FS_SEEK_END

//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
// HCC-Embedded File System
//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
#elif USE_HCC_EMBEDDED_FS
#include "api_f.h"
#include "fat.h"

//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
// Unknown
//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
#else
#error INVALID FILE SYSTEM
#endif


//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
u8 genfs_init();
u8 genfs_reinit();
u8 genfs_deinit();
u8 genfs_create_journal();
u8 genfs_delete_journal();
u8 genfs_format();
u32 genfs_getfreespace();
u32 genfs_getcapacity();
u8 genfs_checkfilehaspath(u8 *filename);
u8 genfs_generalpathcorrection(u8 *source, u32 maxnamelength, u8 *correctname);
u8 genfs_defaultpathcorrection(u8 *source, u32 maxnamelength, u8 *correctname);
u8 genfs_userpathcorrection(u8 *source, u32 maxnamelength, u8 *correctname);
F_FILE * genfs_general_openfile(const u8 *filename, const u8 *attrib);
F_FILE * genfs_default_openfile(const u8 *filename, const u8 *attrib);
F_FILE * genfs_user_openfile(const u8 *filename, const u8 *attrib);
void genfs_closefile(F_FILE *fptr);
u8 genfs_isfolderexist(const u8 *folder_name);
u8 genfs_deletefile(u8 *filename);
u8 genfs_renamefile(u8 *existingfilename, u8 *newfilename);
u8 genfs_movefile(u8 *srcfilename, u8 *dstfilename);
u8 genfs_truncatefile(u8 *filename, u32 newsize);
u8 genfs_copyfile(u8 *srcfilename, u8 *dstfilename);
u8 genfs_verifyfile(u8 *filename, u32 filesize, u32 filecrc32e);
u8 genfs_createdir(const u8 *fulldirname);
u8 genfs_removedir(const u8 *fulldirname);
u8 genfs_changedir(const u8 *fulldirname);
u8 genfs_getfilesize(u8 *filename, u32 *filesize);
u8 genfs_getfilesize_fptr(F_FILE *fptr, u32 *filesize);
u8 genfs_storage_init();
u8 genfs_storage_deinit();
u8 genfs_storage_sync();
u8 genfs_readsectors(u8 *buffer, u32 sector_index, u32 sector_count);
u8 genfs_writesectors(u8 *buffer, u32 sector_index, u32 sector_count);
u8 genfs_writesectors_failsafe(u8 *buffer, u32 sector_index, u32 sector_count);
u8 genfs_listing_set_folder(u8 *folder_name);
u8 genfs_listing_init(u16 folder_selection);
u8 genfs_listing_get_entry(u8 *entry, u32 *entry_size);
u8 genfs_get_listing_bulk(u16 marker, u16 folder_selection, u8 *extension,
                          u32 maxdatalength,
                          u8 *data, u32 *datalength);
void genfs_getpathfromfilename(u8 *fullfilename, u8 *path);
void genfs_getfilenamefrompath(u8 *fullfilename, u8 *filename);

#endif  //__GENFS_H
