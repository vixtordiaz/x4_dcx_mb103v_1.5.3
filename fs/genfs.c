/**
  **************** (C) COPYRIGHT 2011 SCT Performance, LLC *********************
  * File Name          : genfs.c
  * Device / System    : SCT products using the "CommonCode" platform
  * Author             : Quyen Leba
  *
  * Version            : 1 
  * Date               : 05/02/2011
  * Description        : 
  *                    : 
  *
  *
  * History            : 05/02/2011 M. Davis
  *                    :   Add SCT Copyright header,
  *
  ******************************************************************************
  */

#include "genfs.h"

#if USE_SEGGER_EMFILE
#include "emFile/genfs_helper.c"
#elif USE_HCC_EMBEDDED_FS
#else
#endif

#ifdef DEBUG_FS
s16 __fopen_count       = 0;
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
FN_FILE * __debug_fopen(const char *filename,const char *mode)
{
    FN_FILE *fptr;
    fptr = f_open(filename,mode);
    if (fptr)
    {
        __fopen_count++;
    }
    return fptr;
}

void __debug_fclose(FN_FILE *filehandle)
{
    __fopen_count--;
    f_close(filehandle);
}

#endif