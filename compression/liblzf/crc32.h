#ifndef LIBLZF_CRC32_H
#define LIBLZF_CRC32_H

/* crc32 0xdebb20e3 table and supplementary functions.  */
extern const u32 crc32_table[256];

#define crc32(crc,byte) (crc32_table[(u8)(crc) ^ (u8)(byte)] ^ ((crc) >> 8))

#endif

