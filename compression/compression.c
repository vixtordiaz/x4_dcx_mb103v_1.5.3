/**
  **************** (C) COPYRIGHT 2011 SCT Performance, LLC *********************
  * File Name          : compression.c
  * Device / System    : SCT products using the "CommonCode" platform
  * Author             : Quyen Leba
  *
  * Version            : 1 
  * Date               : 05/02/2011
  * Description        : 
  *                    : 
  *
  *
  * History            : 05/02/2011 M. Davis
  *                    :   Add SCT Copyright header,
  *
  ******************************************************************************
  */

//------------------------------------------------------------------------------
// Log error point: 0x4700 -> 0x47FF
//------------------------------------------------------------------------------

#include <string.h>
#include <board/genplatform.h>
#include <fs/genfs.h>
#include <common/statuscode.h>
#include "liblzf/lzfP.h"
#include "compression.h"

extern unsigned int
lzf_compress (const void * in_data, unsigned int in_len,
              u8 *out_data, unsigned int out_len
#if LZF_STATE_ARG
              , LZF_STATE htab
#endif
);
unsigned int 
lzf_decompress (const void *const in_data,  unsigned int in_len,
                void *out_data, unsigned int out_len);

#define BLOCKSIZE           (1024 * 1 - 1)
#define MAX_BLOCKSIZE       BLOCKSIZE

#define TYPE0_HDR_SIZE      5
#define TYPE1_HDR_SIZE      7
#define MAX_HDR_SIZE        7
#define MIN_HDR_SIZE        5

/**
 *  lzf_compress_file
 *  
 *  @brief Compress a file using lzf compression
 *
 *  @param[in] source_filename
 *  @param[in] target_filename
 *
 *  @retval status
 */
u8 lzf_compress_file(const u8 *source_filename, const u8 *target_filename)
{
    F_FILE *srcfile;
    F_FILE *tarfile;
    u32 us, cs, len;
    u8  *buf1;
    u8  *buf2;
    LZF_STATE *htab;
    u32 blocksize = BLOCKSIZE;
    u8  *header;
    u8  status;

    buf1 = NULL;
    buf2 = NULL;
    htab = NULL;
    srcfile = genfs_general_openfile(source_filename,"r");
    if (!srcfile)
    {
        return S_OPENFILE;
    }
    tarfile = genfs_general_openfile(target_filename,"w");
    if (!tarfile)
    {
        status = S_OPENFILE;
        goto lzf_compress_file_done;
    }

    buf1 = __malloc(MAX_HDR_SIZE + MAX_BLOCKSIZE + 16);
    if (!buf1)
    {
        status = S_MALLOC;
        goto lzf_compress_file_done;
    }
    buf2 = __malloc(MAX_HDR_SIZE + MAX_BLOCKSIZE + 16);
    if (!buf2)
    {
        status = S_MALLOC;
        goto lzf_compress_file_done;
    }
    htab = (LZF_STATE *)__malloc(sizeof(LZF_STATE));
    if (!htab)
    {
        status = S_MALLOC;
        goto lzf_compress_file_done;
    }

    while ((us = fread((char*)&buf1[MAX_HDR_SIZE],1,blocksize,srcfile)) > 0)
    {
        cs = lzf_compress(&buf1[MAX_HDR_SIZE], us, &buf2[MAX_HDR_SIZE], us > 4 ? us - 4 : us, *htab);
        if (cs)
        {
            // write compressed data
            header = &buf2[MAX_HDR_SIZE - TYPE1_HDR_SIZE];
            header[0] = 'Z';
            header[1] = 'V';
            header[2] = 1;
            header[3] = cs >> 8;
            header[4] = cs & 0xff;
            header[5] = us >> 8;
            header[6] = us & 0xff;
            len = cs + TYPE1_HDR_SIZE;
        }
        else
        {
            // write uncompressed data
            header = &buf1[MAX_HDR_SIZE - TYPE0_HDR_SIZE];
            header[0] = 'Z';
            header[1] = 'V';
            header[2] = 0;
            header[3] = us >> 8;
            header[4] = us & 0xff;
            len = us + TYPE0_HDR_SIZE;
        }

        if (fwrite((char*)header,1,len,tarfile) != len)
        {
            status = S_WRITEFILE;
            goto lzf_compress_file_done;
        }
        status = S_SUCCESS;
    }

lzf_compress_file_done:
    if (srcfile)
    {
        genfs_closefile(srcfile);
    }
    if (tarfile)
    {
        genfs_closefile(tarfile);
    }
    if (htab)
    {
        __free(htab);
    }
    if (buf1)
    {
        __free(buf1);
    }
    if (buf2)
    {
        __free(buf2);
    }
    return status;
}

/**
 *  lzf_decompress_file
 *  
 *  @brief Decompress a file using lzf compression
 *
 *  @param[in] source_filename
 *  @param[in] target_filename
 *
 *  @retval status
 */
u8 lzf_decompress_file(const u8 *source_filename, const u8 *target_filename)
{
    F_FILE *srcfile;
    F_FILE *tarfile;
    u8  header[MAX_HDR_SIZE];
    u8  *buf1;
    u8  *buf2;
    u8  *p;
    s32 l, rd;
    s16 rc, cs, us, bytes, over = 0;
    
    u8  status;

    buf1 = NULL;
    buf2 = NULL;
    srcfile = genfs_general_openfile(source_filename,"r");
    if (!srcfile)
    {
        return S_OPENFILE;
    }
    tarfile = genfs_general_openfile(target_filename,"w");
    if (!tarfile)
    {
        status = S_OPENFILE;
        goto lzf_decompress_file_done;
    }

    buf1 = __malloc(MAX_HDR_SIZE + MAX_BLOCKSIZE + 16);
    if (!buf1)
    {
        status = S_MALLOC;
        goto lzf_decompress_file_done;
    }
    buf2 = __malloc(MAX_HDR_SIZE + MAX_BLOCKSIZE + 16);
    if (!buf2)
    {
        status = S_MALLOC;
        goto lzf_decompress_file_done;
    }

    while (1)
    {
        rc = fread((char*)header + over,1,MAX_HDR_SIZE - over,srcfile);
        if (rc == 0)
        {
            status = S_SUCCESS;
            goto lzf_decompress_file_done;
        }
        rc += over;
        over = 0;
        if (!rc || header[0] == 0)
        {
            status = S_SUCCESS;
            goto lzf_decompress_file_done;
        }
        
        if (rc < MIN_HDR_SIZE || header[0] != 'Z' || header[1] != 'V')
        {
            status = S_BADCONTENT;
            goto lzf_decompress_file_done;
        }

        switch (header[2])
        {
        case 0:
            cs = -1;
            us = (header[3] << 8) | header[4];
            p = &header[TYPE0_HDR_SIZE];
            break;
        case 1:
            if (rc < TYPE1_HDR_SIZE)
            {
                status = S_ERROR;
                goto lzf_decompress_file_done;
            }
            cs = (header[3] << 8) | header[4];
            us = (header[5] << 8) | header[6];
            p = &header[TYPE1_HDR_SIZE];
            break;
        default:
            status = S_BADCONTENT;
            goto lzf_decompress_file_done;
        }

        bytes = cs == -1 ? us : cs;
        l = &header[rc] - p;
        
        if (l > 0)
        {
            memcpy (buf1, p, l);
        }

        if (l > bytes)
        {
            over = l - bytes;
            memmove (header, &p[bytes], over);
        }

        p = &buf1[l];
        rd = bytes - l;

        if (rd > 0)
        {
            rc = fread((char*)p,1,rd,srcfile);
            if (rc != rd)
            {
                status = S_READFILE;
                goto lzf_decompress_file_done;
            }
        }
      
        if (cs == -1)
        {
            if (fwrite((char*)buf1,1,us,tarfile) != us)
            {
                status = S_WRITEFILE;
                goto lzf_decompress_file_done;
            }
        }
        else
        {
            if (lzf_decompress(buf1, cs, buf2, us) != us)
            {
                status = S_ERROR;
                goto lzf_decompress_file_done;
            }

            if (fwrite((char*)buf2,1,us,tarfile) != us)
            {
                status = S_WRITEFILE;
                goto lzf_decompress_file_done;
            }
        }
    }//while(1)...

lzf_decompress_file_done:
    if (srcfile)
    {
        genfs_closefile(srcfile);
    }
    if (tarfile)
    {
        genfs_closefile(tarfile);
    }
    if (buf1)
    {
        __free(buf1);
    }
    if (buf2)
    {
        __free(buf2);
    }
    return status;
}
