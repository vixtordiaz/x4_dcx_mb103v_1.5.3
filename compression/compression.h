/**
  **************** (C) COPYRIGHT 2011 SCT Performance, LLC *********************
  * File Name          : compression.h
  * Device / System    : SCT products using the "CommonCode" platform
  * Author             : Quyen Leba
  *
  * Version            : 1 
  * Date               : 05/02/2011
  * Description        : 
  *                    : 
  *
  *
  * History            : 05/02/2011 M. Davis
  *                    :   Add SCT Copyright header,
  *
  ******************************************************************************
  */

#ifndef __COMPRESSION_H
#define __COMPRESSION_H

#include <arch/gentype.h>

u8 lzf_compress_file(const u8 *source_filename, const u8 *target_filename);
u8 lzf_decompress_file(const u8 *source_filename, const u8 *target_filename);

#endif  //__COMPRESSION_H
