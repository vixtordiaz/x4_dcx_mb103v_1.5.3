/**
  **************** (C) COPYRIGHT 2011 SCT Performance, LLC *********************
  * File Name          : gentype.h
  * Device / System    : SCT products using the "CommonCode" platform
  * Author             : Quyen Leba
  *
  * Version            : 1 
  * Date               : 05/02/2011
  * Description        : 
  *                    : 
  *
  *
  * History            : 05/02/2011 M. Davis
  *                    :   Add SCT Copyright header,
  *
  ******************************************************************************
  */

#ifndef __GENTYPE_H
#define __GENTYPE_H

#include "bit.h"

#ifdef __STR91x_ARCH__
#include "STR91x/include/91x_type.h"
#elif __STM32F10x_ARCH__
#include <stm32f10x.h>
typedef unsigned long long  u64;
typedef signed long long    s64;
typedef enum {FALSE, TRUE} bool;
#elif __STM32F2xx_ARCH__
#include <stm32f2xx.h>
typedef unsigned long long  u64;
typedef signed long long    s64;
typedef enum {FALSE, TRUE} bool;
#else
#error "gentype.h: Invalid ARCH"
#endif

typedef union
{
    u8 c[4];
    u32 i;
}u32ext;

#define NULL_BYTE   '\0'

//for sanity check of struct
#define CCASSERT(name,predicate)                    _x_CCASSERT_LINE(name, predicate, __LINE__)
#define _x_CCASSERT_LINE(a,b,c)                     _x_CCASSERT_LINE_2(a,b,c)
#define _x_CCASSERT_LINE_2(name, predicate, line)   typedef char constraint_violated_at_##name##line[2*((predicate)!=0)-1];

#define STRUCT_SIZE_CHECK(name,size)                CCASSERT(name,sizeof(name) == size)
#define STRUCT_MAX_SIZE_CHECK(name,size)            CCASSERT(name,sizeof(name) <= size)

#endif    //__GENTYPE_H
