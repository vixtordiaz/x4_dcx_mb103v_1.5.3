/**
  **************** (C) COPYRIGHT 2011 SCT Performance, LLC *********************
  * File Name          : clock.h
  * Device / System    : SCT products using the "CommonCode" platform
  * Author             : Quyen Leba
  *
  * Version            : 1 
  * Date               : 05/02/2011
  * Description        : 
  *                    : 
  *
  *
  * History            : 05/02/2011 M. Davis
  *                    :   Add SCT Copyright header,
  *
  ******************************************************************************
  */

#ifndef __CLOCK_H
#define __CLOCK_H

#include <arch/gentype.h>

void clock_init();

#endif  //__CLOCK_H
