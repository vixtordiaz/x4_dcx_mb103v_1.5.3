/**
  **************** (C) COPYRIGHT 2011 SCT Performance, LLC *********************
  * File Name          : timer.h
  * Device / System    : SCT products using the "CommonCode" platform
  * Author             : Quyen Leba
  *
  * Version            : 1 
  * Date               : 05/02/2011
  * Description        : 
  *                    : 
  *
  *
  * History            : 05/02/2011 M. Davis
  *                    :   Add SCT Copyright header,
  *
  ******************************************************************************
  */

#ifndef __TIMER_H
#define __TIMER_H

#include <arch/gentype.h>

typedef struct
{
    u8  app_timeout     : 1;
    u8  board_timeout   : 1;
    u8  reserved        : 6;
    u32 app_current_ms;
    u32 app_timeout_ms;
    u32 board_current_ms;
    u32 board_timeout_ms;
}TIMER_INFO;
extern TIMER_INFO timer_info;

void timer2_init();
void timer2_set_timeout(u32 ms);
void timer2_reset_timeout();
void timer2_cancel_timeout();
void timer2_intr_handler();

void timer4_init();
void timer4_set_timeout(u32 ms);
void timer4_reset_timeout();
void timer4_intr_handler();

//this is app timeout and used in app level
#define timeout_init()          timer4_init()
#define timeout_set(ms)         timer4_set_timeout(ms)
#define timeout_reset()         timer4_reset_timeout(ms)
#define timeout()               (timer_info.app_timeout == 1)

//board timeout only used in board level to control hardware timeout
#define board_timeout_init()    timer2_init()
#define board_timeout_set(ms)   timer2_set_timeout(ms)
#define board_timeout_reset()   timer2_reset_timeout(ms)
#define board_timeout_cancel()  timer2_cancel_timeout()
#define board_timeout()         (timer_info.board_timeout == 1)

#define get_systick_count()     SysTick->VAL

#endif  //__TIMER_H
