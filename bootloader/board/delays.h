/**
  **************** (C) COPYRIGHT 2011 SCT Performance, LLC *********************
  * File Name          : delays.h
  * Device / System    : SCT products using the "CommonCode" platform
  * Author             : Quyen Leba
  *
  * Version            : 1 
  * Date               : 05/02/2011
  * Description        : 
  *                    : 
  *
  *
  * History            : 05/02/2011 M. Davis
  *                    :   Add SCT Copyright header,
  *
  ******************************************************************************
  */

#ifndef __DELAYS_H
#define __DELAYS_H

#include <arch/gentype.h>

void delays(u32 timeout, u8 prefix);

#endif  //__DELAYS_H
