/**
  **************** (C) COPYRIGHT 2011 SCT Performance, LLC *********************
  * File Name          : interrupt.c
  * Device / System    : SCT products using the "CommonCode" platform
  * Author             : Quyen Leba
  *
  * Version            : 1 
  * Date               : 05/02/2011
  * Description        : 
  *                    : 
  *
  *
  * History            : 05/02/2011 M. Davis
  *                    :   Add SCT Copyright header,
  *
  ******************************************************************************
  */

#include <misc.h>
#include <board/interrupt.h>

#define AIRCR_VECTKEY_MASK    ((uint32_t)0x05FA0000)

//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
void interrupt_init()
{
#ifdef __FAILSAFE_BOOTLOADER__
#define NVIC_VECTOR_TABLE_ADDRESS       0x0000
    SCB->VTOR = NVIC_VectTab_FLASH |
        (NVIC_VECTOR_TABLE_ADDRESS & (u32)0x1FFFFF80);
#else
#define NVIC_VECTOR_TABLE_ADDRESS       0x8000
    SCB->VTOR = NVIC_VectTab_RAM |
        (NVIC_VECTOR_TABLE_ADDRESS & (u32)0x1FFFFF80);
#endif

    SCB->AIRCR = AIRCR_VECTKEY_MASK | NVIC_PriorityGroup_3;
    //NVIC_PriorityGroupConfig(NVIC_PriorityGroup_3);

    //Configure the SysTick handler priority
    NVIC_SystemHandlerPriorityConfig(SystemHandler_SysTick, 0, 0);

//    NVIC_InitTypeDef NVIC_InitStructure;
//
//    NVIC_InitStructure.NVIC_IRQChannel = USB_LP_CAN1_RX0_IRQn;
//    NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 0;
//    NVIC_InitStructure.NVIC_IRQChannelSubPriority = 1;
//    NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;
//    NVIC_Init(&NVIC_InitStructure);
//
//    NVIC_InitStructure.NVIC_IRQChannel = USB_HP_CAN1_TX_IRQn;
//    NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 0;
//    NVIC_InitStructure.NVIC_IRQChannelSubPriority = 0;
//    NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;
//    NVIC_Init(&NVIC_InitStructure);
    
    //USB_LP
    NVIC->IP[USB_LP_CAN1_RX0_IRQn] = 0x16;
    NVIC->ISER[USB_LP_CAN1_RX0_IRQn >> 0x05] = (u32)0x01 << (USB_LP_CAN1_RX0_IRQn & (u8)0x1F);
    //USB_HP
    NVIC->IP[USB_LP_CAN1_RX0_IRQn] = 0x00;
    NVIC->ISER[USB_LP_CAN1_RX0_IRQn >> 0x05] = (u32)0x01 << (USB_HP_CAN1_TX_IRQn & (u8)0x1F);

    //SPI1
    NVIC->IP[SPI1_IRQn] = 0x20;
    NVIC->ISER[SPI1_IRQn >> 0x05] = (u32)0x01 << (SPI1_IRQn & (u8)0x1F);

    //TIM2
    NVIC->IP[TIM2_IRQn] = 0x40;
    NVIC->ISER[TIM2_IRQn >> 0x05] = (u32)0x01 << (TIM2_IRQn & (u8)0x1F);

    //TIM4
    NVIC->IP[TIM4_IRQn] = 0x50;
    NVIC->ISER[TIM4_IRQn >> 0x05] = (u32)0x01 << (TIM4_IRQn & (u8)0x1F);
    
    //DMA1_CH1
    NVIC->IP[DMA1_Channel1_IRQn] = 0x80;
    NVIC->ISER[DMA1_Channel1_IRQn >> 0x05] = (u32)0x01 << (DMA1_Channel1_IRQn & (u8)0x1F);

    //UART2
    NVIC->IP[USART2_IRQn] = 0x20;
    NVIC->ISER[USART2_IRQn >> 0x05] = (u32)0x01 << (USART2_IRQn & (u8)0x1F);

    //WWDG
    NVIC->IP[WWDG_IRQn] = 0x20;
    NVIC->ISER[WWDG_IRQn >> 0x05] = (u32)0x01 << (WWDG_IRQn & (u8)0x1F);
}
