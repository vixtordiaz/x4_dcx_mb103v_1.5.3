/**
  **************** (C) COPYRIGHT 2011 SCT Performance, LLC *********************
  * File Name          : sdmmc.h
  * Device / System    : SCT products using the "CommonCode" platform
  * Author             : Quyen Leba
  *
  * Version            : 1 
  * Date               : 05/02/2011
  * Description        : 
  *                    : 
  *
  *
  * History            : 05/02/2011 M. Davis
  *                    :   Add SCT Copyright header,
  *
  ******************************************************************************
  */

#ifndef __SDMMC_H
#define __SDMMC_H

#include <arch/gentype.h>
#include <arm_comm.h>
#include "disk.h"
#include "stm32f10x_sdio.h"

// Hardware depends definitions
#define IdentificationModeClock     400KHZ
#define PC_PCSPI1                   (1<<10)
#define PC_PCSPI0                   (1<<8)

#define SD_DEF_BLK_SIZE             512

#define OcrReg                      0x40FF8000
#define OcrReg_HC                   0x40000000
#define Cmd8Reg                     0x000001AA

// SD Maximum Block Rad Access Time
#define RD_TIME_OUT                 100LL   // ms
// SD Maximum Block Write Access Time
#define WR_TIME_OUT                 250LL   // ms

typedef enum _SdState_t
{
    SdOk = 0, SdNoPresent, SdNoResponse, SdCardError, SdMiscompare, SdDmaError
} SdState_t;

typedef enum _SdCmdInd_t
{
    CMD0 = 0, // Resets the MultiMediaCard
    CMD1,     // Activates the card�s initialization process
    CMD2,     // Asks all cards to send its card identification (CID)
    CMD3,     // Set relative address
    CMD7,     // Select/deselect card the card is selected by its own relative
    // address and deselected by any other address; address 0 deselects all.
    CMD8,     // Send Interface Condition Command
    // for HC cards only
    CMD9,     // Asks the selected card to send its card-specific data (CSD)
    CMD10,    // Asks the selected card to send its card identification (CID)
    CMD12,    // Stop transmission on multiple block read
    CMD13,    // Asks the selected card to send its status register
    CMD16,    // Selects a block length (in bytes) for all following block commands (read and write)
    CMD17,    // Reads a block of the size selected by the SET_BLOCKLEN command
    CMD18,    // Continuously transfers data blocks from card to host
    // until interrupted by a Stop command or the requested number of data blocks transmitted
    CMD24,    // Writes a block of the size selected by the SET_BLOCKLEN command
    CMD25,    // Continuously writes blocks of data until a �Stop Tran�
    // Token or the requested number of blocks received
    CMD27,    // Programming of the programmable bits of the CSD
    CMD28,    // If the card has write protection features, this
    // command sets the write protection bit of the
    // addressed group. The properties of write protection
    // are coded in the card specific data (WP_GRP_SIZE).
    CMD29,    // If the card has write protection features, this
    // command clears the write protection bit of the addressed group
    CMD30,    // If the card has write protection features, this
    // command asks the card to send the status of the write protection bits
    CMD32,    // Sets the address of the first sector of the erase group
    CMD33,    // Sets the address of the last sector in a continuous
    // range within the selected erase group, or the address
    // of a single sector to be selected for erase
    CMD34,    // Removes one previously selected sector from the erase selection
    CMD35,    // Sets the address of the first erase group within a range to be selected for erase
    CMD36,    // Sets the address of the last erase group within a
    // continuous range to be selected for erase
    CMD37,    // Removes one previously selected erase group from the erase selection.
    CMD38,    // Erases all previously selected sectors
    CMD42,    // Used to set/reset the password or lock/unlock the
    // card. The size of the Data Block is defined by the SET_BLOCK_LEN command
    CMD55,    // Notifies the card that the next command is an
    // application specific command rather than a standard command.
    CMD56,    // Used either to transfer a Data Block to the card or
    // to get a Data Block from the card for general
    // purpose/application specific commands. The size
    // of the Data Block is defined with SET_BLOCK_LEN command
    ACMD6,    // Defines the data bus width (�00�=1bit or�10�=4 bits bus)
    // (Only for SD)
    ACMD41,   // Activates the card�s initialization process (Only for SD)
    CMD_END   // End of commands index
} SdCmdInd_t;

// Card Status register
#define READY_FOR_DATA              (  1UL <<  8)
#define CURRENT_STATE               (0xFUL <<  9)
#define ERASE_RESET                 (  1UL << 13)
#define CARD_ECC_DISABLED           (  1UL << 14)
#define WP_ERASE_SKIP               (  1UL << 15)
#define CID_CSD_OVERWRITE           (  1UL << 16)
#define ERROR                       (  1UL << 17)
#define ILLEGAL_COMMAND             (  1UL << 22)
#define COM_CRC_ERROR               (  1UL << 23)
#define WP_VIOLATION                (  1UL << 26)
#define ERASE_PARAM                 (  1UL << 27)
#define ERASE_SEQ_ERROR             (  1UL << 28)
#define BLOCK_LEN_ERROR             (  1UL << 29)
#define ADDRESS_ERROR               (  1UL << 30)
#define OUT_OF_RANGE                (  1UL << 31)
// Card states
#define CARD_IDLE                   (  0UL << 9)
#define CARD_READY                  (  1UL << 9)
#define CARD_IDENT                  (  2UL << 9)
#define CARD_STBY                   (  3UL << 9)
#define CARD_TRAN                   (  4UL << 9)
#define CARD_DATA                   (  5UL << 9)
#define CARD_RCV                    (  6UL << 9)
#define CARD_PRG                    (  7UL << 9)
#define CARD_DIS                    (  8UL << 9)

typedef enum _SdAgmType_t
{
    SdNoArg = 0, SdBlockLen, SdDataAdd, SdRelAddr, SdOcr,
} SdAgmType_t;

typedef enum _MmcRespType_t
{
    SdNoResp = 0,SdR1,SdR1b,SdR2,SdR3,SdR7
} SdRespType_t;

typedef struct _SdCommads_t
{
    u8         TxData;
    SdAgmType_t  Arg;
    SdRespType_t Resp;
} SdCommads_t;

typedef union _DataCtrl_t
{
    u32 Data;
    struct
    {
        u32 ENABLE     : 1;
        u32 DIRECTION  : 1;
        u32 MODE       : 1;
        u32 DMAENABLE  : 1;
        u32 BLOCKSIZE  : 4;
    u32            :24;
    };
} DataCtrl_t, *pDataCtrl_t;

void Dly100us(void *arg);
#define SdDly_1ms(Delay) Dly100us((void*)(10 * Delay))

void sdmmc_init(void);
//DiskStatusCode_t sdmmc_diskio (u8 * pData,u32 BlockStart,
//                               u32 BlockNum, DiskIoRequest_t IoRequest);

SdState_t sdmmc_blockread(u8 *pData, u32 Add, u32 Length);

#endif  //__SDMMC_H
