/**
  **************** (C) COPYRIGHT 2011 SCT Performance, LLC *********************
  * File Name          : led.h
  * Device / System    : SCT products using the "CommonCode" platform
  * Author             : Quyen Leba
  *
  * Version            : 1 
  * Date               : 05/02/2011
  * Description        : 
  *                    : 
  *
  *
  * History            : 05/02/2011 M. Davis
  *                    :   Add SCT Copyright header,
  *
  ******************************************************************************
  */

#ifndef __LED_H
#define __LED_H

typedef enum 
{
    LED_OFF,
    LED_RED,
    LED_GREEN,
    LED_YELLOW,
    //special state
    LED_BLUE,
    LED_BLUERED,
    LED_BLUEGREEN,
}LED_State;

void led_init();
void led_setState(LED_State state);
void led_enter_bootloader();

#endif  //__LED_H