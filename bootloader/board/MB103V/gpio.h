/**
  **************** (C) COPYRIGHT 2011 SCT Performance, LLC *********************
  * File Name          : gpio.h
  * Device / System    : SCT products using the "CommonCode" platform
  * Author             : Quyen Leba
  *
  * Version            : 1 
  * Date               : 05/02/2011
  * Description        : 
  *                    : 
  *
  *
  * History            : 05/02/2011 M. Davis
  *                    :   Add SCT Copyright header,
  *
  ******************************************************************************
  */

#ifndef __GPIO_H
#define __GPIO_H

#include <arch/gentype.h>
#include "button.h"

void gpio_init();
void gpio_set_dav_v_busy_pin();
void gpio_clear_dav_v_busy_pin();
u8 gpio_read_dav_v_busy_pin();
void gpio_reset_vehicle_board();
u8 gpio_get_board_rev();
#define gpio_is_boot_en_pin_active()    (button_getState() == Bit_RESET)
void gpio_reset_Bluetooth_module();
void gpio_config_Bluetooth_Force115KBaud(bool enable);


#endif	//__GPIO_H
