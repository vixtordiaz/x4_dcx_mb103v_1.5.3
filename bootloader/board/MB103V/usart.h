/**
  **************** (C) COPYRIGHT 2011 SCT Performance, LLC *********************
  * File Name          : usart.h
  * Device / System    : SCT products using the "CommonCode" platform
  * Author             : Quyen Leba
  *
  * Version            : 1 
  * Date               : 05/02/2011
  * Description        : 
  *                    : 
  *
  *
  * History            : 05/02/2011 M. Davis
  *                    :   Add SCT Copyright header,
  *
  ******************************************************************************
  */

#ifndef __USART_H
#define __USART_H

#include "stm32f10x_usart.h"

typedef enum
{
    BAUD9600    = 9600,
    BAUD115200  = 115200,
    BAUD230400  = 230400,
    BAUD460800  = 460800,
    BAUD921600  = 921600,
}USARTBAUD;

void usart2_init(USARTBAUD baud);
void usart2_changebaudrate(USARTBAUD baud);
#define usart2_tx(databyte)     USART_SendData(USART2, databyte);    \
    while(USART_GetFlagStatus(USART2, USART_FLAG_TXE) == RESET);
#define usart2_rx()             USART_ReceiveData(USART2)
#define usart2_getitstatus()    USART_GetITStatus(USART2, USART_IT_RXNE)

#endif	//__USART_H
