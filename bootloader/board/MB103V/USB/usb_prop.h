/******************** (C) COPYRIGHT 2010 STMicroelectronics ********************
* File Name          : usb_prop.h
* Author             : MCD Application Team
* Version            : V3.2.1
* Date               : 07/05/2010
* Description        : All processing related to Mass Storage Demo (Endpoint 0)
********************************************************************************
* THE PRESENT FIRMWARE WHICH IS FOR GUIDANCE ONLY AIMS AT PROVIDING CUSTOMERS
* WITH CODING INFORMATION REGARDING THEIR PRODUCTS IN ORDER FOR THEM TO SAVE TIME.
* AS A RESULT, STMICROELECTRONICS SHALL NOT BE HELD LIABLE FOR ANY DIRECT,
* INDIRECT OR CONSEQUENTIAL DAMAGES WITH RESPECT TO ANY CLAIMS ARISING FROM THE
* CONTENT OF SUCH FIRMWARE AND/OR THE USE MADE BY CUSTOMERS OF THE CODING
* INFORMATION CONTAINED HEREIN IN CONNECTION WITH THEIR PRODUCTS.
*******************************************************************************/

/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __usb_prop_H
#define __usb_prop_H
/* Includes ------------------------------------------------------------------*/
/* Exported types ------------------------------------------------------------*/

/* Exported constants --------------------------------------------------------*/
#define SCTUSB_GetConfiguration           NOP_Process
/* #define SCTUSB_SetConfiguration        NOP_Process*/
#define SCTUSB_GetInterface               NOP_Process
#define SCTUSB_SetInterface               NOP_Process
#define SCTUSB_GetStatus                  NOP_Process
/* #define SCTUSB_ClearFeature            NOP_Process*/
#define SCTUSB_SetEndPointFeature         NOP_Process
#define SCTUSB_SetDeviceFeature           NOP_Process
/*#define SCTUSB_SetDeviceAddress         NOP_Process*/

/* MASS Storage Requests*/
#define GET_MAX_LUN                             0xFE
#define SCTUSB_STORAGE_RESET                    0xFF
#define LUN_DATA_LENGTH                         1

/* Exported macro ------------------------------------------------------------*/
/* Exported functions ------------------------------------------------------- */
void SCTUSB_Init(void);
void SCTUSB_Reset(void);
void SCTUSB_SetConfiguration(void);
void SCTUSB_ClearFeature(void);
void SCTUSB_SetDeviceAddress (void);
void SCTUSB_Status_In (void);
void SCTUSB_Status_Out (void);
RESULT SCTUSB_Data_Setup(uint8_t);
RESULT SCTUSB_NoData_Setup(uint8_t);
RESULT SCTUSB_Get_Interface_Setting(uint8_t Interface, uint8_t AlternateSetting);
uint8_t *SCTUSB_GetDeviceDescriptor(uint16_t );
uint8_t *SCTUSB_GetConfigDescriptor(uint16_t);
uint8_t *SCTUSB_GetStringDescriptor(uint16_t);
uint8_t *Get_Max_Lun(uint16_t Length);

#endif /* __usb_prop_H */

/******************* (C) COPYRIGHT 2010 STMicroelectronics *****END OF FILE****/

