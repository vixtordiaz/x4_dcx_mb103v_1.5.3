/******************** (C) COPYRIGHT 2010 STMicroelectronics ********************
* File Name          : usb_desc.c
* Author             : MCD Application Team
* Version            : V3.2.1
* Date               : 07/05/2010
* Description        : Descriptors for SCT USB Device
********************************************************************************
* THE PRESENT FIRMWARE WHICH IS FOR GUIDANCE ONLY AIMS AT PROVIDING CUSTOMERS
* WITH CODING INFORMATION REGARDING THEIR PRODUCTS IN ORDER FOR THEM TO SAVE TIME.
* AS A RESULT, STMICROELECTRONICS SHALL NOT BE HELD LIABLE FOR ANY DIRECT,
* INDIRECT OR CONSEQUENTIAL DAMAGES WITH RESPECT TO ANY CLAIMS ARISING FROM THE
* CONTENT OF SUCH FIRMWARE AND/OR THE USE MADE BY CUSTOMERS OF THE CODING
* INFORMATION CONTAINED HEREIN IN CONNECTION WITH THEIR PRODUCTS.
*******************************************************************************/

/* Includes ------------------------------------------------------------------*/
#include "usb_desc.h"
#include "device_config.h"

uint8_t SCTUSB_DeviceDescriptor[SCTUSB_SIZ_DEVICE_DESC] =
  {
    0x12,   /* bLength  */
    0x01,   /* bDescriptorType */
    0x00,   /* bcdUSB, version 2.00 */
    0x02,
    0x00,   /* bDeviceClass : each interface define the device class */
    0x00,   /* bDeviceSubClass */
    0x00,   /* bDeviceProtocol */
    0x40,   /* bMaxPacketSize0 0x40 = 64 */
    0x59,   /* idVendor     (0483) */
    0x50,
    DEFAULT_USB_PID & 0xFF, /* idProduct */
    DEFAULT_USB_PID >> 8,
    0x00,   /* bcdDevice 2.00*/
    0x02,
    1,              /* index of string Manufacturer  */
    /**/
    2,              /* index of string descriptor of product*/
    /* */
    3,              /* */
    /* */
    /* */
    0x01    /*bNumConfigurations */
  };
const uint8_t SCTUSB_ConfigDescriptor[SCTUSB_SIZ_CONFIG_DESC] =
  {

    0x09,   /* bLength: Configuation Descriptor size */
    0x02,   /* bDescriptorType: Configuration */
    SCTUSB_SIZ_CONFIG_DESC,

    0x00,
    0x01,   /* bNumInterfaces: 1 interface */
    0x01,   /* bConfigurationValue: */
    /*      Configuration value */
    0x00,   /* iConfiguration: */
    /*      Index of string descriptor */
    /*      describing the configuration */
    0xC0,   /* bmAttributes: */
    /*      bus powered */
    0x32,   /* MaxPower 100 mA */

    /******************** Descriptor of Mass Storage interface ********************/
    /* 09 */
    0x09,   /* bLength: Interface Descriptor size */
    0x04,   /* bDescriptorType: */
    /*      Interface descriptor type */
    0x00,   /* bInterfaceNumber: Number of Interface */
    0x00,   /* bAlternateSetting: Alternate setting */
    0x02,   /* bNumEndpoints*/
    0xFF,   /* bInterfaceClass:  */
    0xFF,   /* bInterfaceSubClass : */
    0xFF,   /* nInterfaceProtocol */
    4,          /* iInterface: */
    /* 18 */
    0x07,   /*Endpoint descriptor length = 7*/
    0x05,   /*Endpoint descriptor type */
    0x81,   /*Endpoint address (IN, address 1) */
    0x02,   /*Bulk endpoint type */
    0x40,   /*Maximum packet size (64 bytes) */
    0x00,
    0x00,   /*Polling interval in milliseconds */
    /* 25 */
    0x07,   /*Endpoint descriptor length = 7 */
    0x05,   /*Endpoint descriptor type */
    0x02,   /*Endpoint address (OUT, address 2) */
    0x02,   /*Bulk endpoint type */
    0x40,   /*Maximum packet size (64 bytes) */
    0x00,
    0x00     /*Polling interval in milliseconds*/
    /*32*/
  };
const uint8_t SCTUSB_StringLangID[SCTUSB_SIZ_STRING_LANGID] =
  {
    SCTUSB_SIZ_STRING_LANGID,
    0x03,
    0x09,
    0x04
  }
  ;      /* LangID = 0x0409: U.S. English */
const uint8_t SCTUSB_StringVendor[SCTUSB_SIZ_STRING_VENDOR] =
  {
    SCTUSB_SIZ_STRING_VENDOR, /* Size of manufaturer string */
    0x03,           /* bDescriptorType = String descriptor */
    /* Manufacturer: "SCT LLC" */
    'S', 0, 'C', 0, 'T', 0, ' ', 0, 'L', 0, 'L', 0, 'C', 0
  };
const uint8_t SCTUSB_StringProduct[SCTUSB_SIZ_STRING_PRODUCT] =
  {
    SCTUSB_SIZ_STRING_PRODUCT,
    0x03,
    /* Product name: "iTSX" */
    'i', 0, 'T', 0, 'S', 0, 'X', 0
  };

uint8_t SCTUSB_StringSerial[SCTUSB_SIZ_STRING_SERIAL] =
  {
    SCTUSB_SIZ_STRING_SERIAL,
    0x03,
    /* Serial number*/  //TODOQ: more on this string later
    'i', 0, 'T', 0, 'S', 0, 'X', 0, '0', 0, '0', 0, '0', 0
  };
const uint8_t SCTUSB_StringInterface[SCTUSB_SIZ_STRING_INTERFACE] =
  {
    SCTUSB_SIZ_STRING_INTERFACE,
    0x03,
    /* Interface 0: "iTSX IF" */
    'i', 0, 'T', 0, 'S', 0, 'X', 0, ' ', 0, 'I', 0, 'F', 0
  };

/******************* (C) COPYRIGHT 2010 STMicroelectronics *****END OF FILE****/
