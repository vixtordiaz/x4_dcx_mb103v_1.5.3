/**
  **************** (C) COPYRIGHT 2011 SCT Performance, LLC *********************
  * File Name          : gpio.c
  * Device / System    : SCT products using the "CommonCode" platform
  * Author             : Quyen Leba
  *
  * Version            : 1 
  * Date               : 05/02/2011
  * Description        : 
  *                    : 
  *
  *
  * History            : 05/02/2011 M. Davis
  *                    :   Add SCT Copyright header,
  *
  ******************************************************************************
  */

#include <board/genplatform.h>
#include <board/delays.h>
#include <common/statuscode.h>
#include "gpio.h"

#if BOARDREV_REVB0
#define SB_DAV_A_BUSY_PORT      GPIOD
#define SB_DAV_A_BUSY_PIN       GPIO_Pin_11
#define SB_DAV_V_BUSY_PORT      GPIOD
#define SB_DAV_V_BUSY_PIN       GPIO_Pin_12
#define RESET_GPIO_PORT         GPIOE
#define RESET_GPIO_PIN          GPIO_Pin_11
#elif BOARDREV_SCREAMINGCIRCUIT_REV10
#define SB_DAV_V_BUSY_PORT      GPIOE
#define SB_DAV_V_BUSY_PIN       GPIO_Pin_11
#define RESET_GPIO_PORT         GPIOD
#define RESET_GPIO_PIN          GPIO_Pin_12
#else
#error INVALID_BOARD
#endif

#define SB_BOARDREV_PORT        GPIOB
#define SB_BOARDREV_0_PIN       GPIO_Pin_5
#define SB_BOARDREV_1_PIN       GPIO_Pin_10
#define SB_BOARDREV_2_PIN       GPIO_Pin_11

// Port E.10 connected to BT11 RESET input
#define BT11_HWRESET_PORT       GPIOE
#define BT11_HWRESET_PIN        GPIO_Pin_10

// Port E.14 connected to BT11 Force 115kBaud Control Input (GPIO [5])
#define BT11_115KBAUD_PORT      GPIOE
#define BT11_115KBAUD_PIN       GPIO_Pin_14

//TODOQ: more on reset later

//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
void gpio_init()
{
    GPIO_InitTypeDef GPIO_InitStructure;

    GPIO_InitStructure.GPIO_Pin = SB_DAV_V_BUSY_PIN;
    GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
    GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IPU;
    GPIO_Init(SB_DAV_V_BUSY_PORT, &GPIO_InitStructure);

#if BOARDREV_REVB0
    GPIO_InitStructure.GPIO_Pin = SB_DAV_A_BUSY_PIN;
    GPIO_Init(SB_DAV_A_BUSY_PORT, &GPIO_InitStructure);
#endif

    //must be pull-up input pin when not used; otherwise, VehicleBoard
    //watchdog reset will not work
    GPIO_InitStructure.GPIO_Pin = RESET_GPIO_PIN;
    GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
    GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IPU;
    GPIO_Init(RESET_GPIO_PORT, &GPIO_InitStructure);

    // Configure the BT-11 Bluetooth module RESET signal as pull-up input
    GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IPU;
    GPIO_InitStructure.GPIO_Pin = BT11_HWRESET_PIN;
    GPIO_Init(BT11_HWRESET_PORT, &GPIO_InitStructure);
    
    GPIO_InitStructure.GPIO_Pin = BT11_115KBAUD_PIN;
    GPIO_InitStructure.GPIO_Mode = GPIO_Mode_Out_PP;
    GPIO_Init(BT11_115KBAUD_PORT, &GPIO_InitStructure);
    GPIO_ResetBits(BT11_115KBAUD_PORT, BT11_115KBAUD_PIN);

    GPIO_InitStructure.GPIO_Pin = SB_BOARDREV_0_PIN | SB_BOARDREV_1_PIN | SB_BOARDREV_2_PIN;
    GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IPU;
    GPIO_Init(SB_BOARDREV_PORT, &GPIO_InitStructure);
}

//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
u8 gpio_read_dav_v_busy_pin()
{
    return GPIO_ReadInputDataBit(SB_DAV_V_BUSY_PORT, SB_DAV_V_BUSY_PIN);
}

//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
u8 gpio_read_dav_a_busy_pin()
{
    return GPIO_ReadInputDataBit(SB_DAV_A_BUSY_PORT, SB_DAV_A_BUSY_PIN);
}

//------------------------------------------------------------------------------
// Reset VehicleBoard
// Engineer: Quyen Leba
//------------------------------------------------------------------------------
void gpio_reset_vehicle_board()
{
    GPIO_InitTypeDef GPIO_InitStructure;

    GPIO_InitStructure.GPIO_Pin = RESET_GPIO_PIN;
    GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
    GPIO_InitStructure.GPIO_Mode = GPIO_Mode_Out_PP;
    GPIO_Init(RESET_GPIO_PORT, &GPIO_InitStructure);
    GPIO_SetBits(RESET_GPIO_PORT,RESET_GPIO_PIN);
    delays(10,'m');

    GPIO_ResetBits(RESET_GPIO_PORT,RESET_GPIO_PIN);
    delays(20,'m');

    //must be pull-up input pin when not used; otherwise, VehicleBoard
    //watchdog reset will not work
    GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IPU;
    GPIO_Init(RESET_GPIO_PORT, &GPIO_InitStructure);
}

//------------------------------------------------------------------------------
// Reset BT11 Bluetooth module, on AppBoard
// Engineer: Mark Davis
// Note: reset pin is normally configured as pull-up input, re-configure it to
// output push-pull to drive reset pin and re-configure it back to pull-up input
//------------------------------------------------------------------------------
void gpio_reset_Bluetooth_module()
{
    GPIO_InitTypeDef GPIO_InitStructure;

    GPIO_InitStructure.GPIO_Mode = GPIO_Mode_Out_PP;
    GPIO_InitStructure.GPIO_Pin = BT11_HWRESET_PIN;
    GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
    GPIO_SetBits(BT11_HWRESET_PORT,BT11_HWRESET_PIN);
    GPIO_Init(BT11_HWRESET_PORT, &GPIO_InitStructure);

    // Hold RESET high for 30mS
    delays(30, 'm');

    // Set RESET low for 100mS
    GPIO_ResetBits(BT11_HWRESET_PORT,BT11_HWRESET_PIN);
    delays(100, 'm');
    delays(100, 'm');
    delays(100, 'm');

    // Set RESET high
    GPIO_SetBits(BT11_HWRESET_PORT,BT11_HWRESET_PIN);

    GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IPU;
    GPIO_InitStructure.GPIO_Pin = BT11_HWRESET_PIN;
    GPIO_Init(BT11_HWRESET_PORT, &GPIO_InitStructure);
}

//------------------------------------------------------------------------------
// Get Board Rev
// Return:  u8  rev
// Engineer: Quyen Leba
//------------------------------------------------------------------------------
u8 gpio_get_board_rev()
{
    u8  rev;
    rev = GPIO_ReadInputDataBit(SB_BOARDREV_PORT, SB_BOARDREV_0_PIN);
    rev |= GPIO_ReadInputDataBit(SB_BOARDREV_PORT, SB_BOARDREV_1_PIN) << 1;
    rev |= GPIO_ReadInputDataBit(SB_BOARDREV_PORT, SB_BOARDREV_2_PIN) << 2;
    return rev;
}

//------------------------------------------------------------------------------
// Sets the Force 115K Baud Pin on the BT11
// Engineer: Patrick Downs
//------------------------------------------------------------------------------
void gpio_config_Bluetooth_Force115KBaud(bool enable)
{
    if(enable)
    {
        GPIO_SetBits(BT11_115KBAUD_PORT, BT11_115KBAUD_PIN);
    }
    else
    {
        GPIO_ResetBits(BT11_115KBAUD_PORT, BT11_115KBAUD_PIN);
    }
}