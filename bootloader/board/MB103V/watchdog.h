/**
  **************** (C) COPYRIGHT 2011 SCT Performance, LLC *********************
  * File Name          : watchdog.h
  * Device / System    : SCT products using the "CommonCode" platform
  * Author             : Quyen Leba
  *
  * Version            : 1 
  * Date               : 05/02/2011
  * Description        : 
  *                    : 
  *
  *
  * History            : 05/02/2011 M. Davis
  *                    :   Add SCT Copyright header,
  *
  ******************************************************************************
  */

#ifndef __WATCHDOG_H
#define __WATCHDOG_H

#include "stm32f10x_wwdg.h"

void watchdog_set();

#endif	//__WATCHDOG_H
