/**
  **************** (C) COPYRIGHT 2011 SCT Performance, LLC *********************
  * File Name          : disk.h
  * Device / System    : SCT products using the "CommonCode" platform
  * Author             : Quyen Leba
  *
  * Version            : 1 
  * Date               : 05/02/2011
  * Description        : 
  *                    : 
  *
  *
  * History            : 05/02/2011 M. Davis
  *                    :   Add SCT Copyright header,
  *
  ******************************************************************************
  */

#include "arm_comm.h"

#ifndef __DISK_H
#define	__DISK_H

typedef enum _DiskStatusCode_t
{
  DiskCommandPass = 0, DiskNotReady, DiskNotPresent,
  DiskParametersError, DiskMiscompareError, DiskChanged,
  DiskUknowError,
} DiskStatusCode_t;

typedef enum _DiskIoRequest_t
{
  DiskWrite = 0, DiskRead, DiskVerify,
} DiskIoRequest_t;

typedef enum _DiskInfoType_t
{
  DiskInquiry = 0, DiskFormatCapacity,
} DiskInfoType_t;

typedef enum _DiskType_t
{
  DiskMMC, DiskSD_Spec1_x, DiskSD_Spec2_0, DiskUnknow,
} DiskType_t;

typedef struct _DiskCtrlBlk_t
{
  Int32U  BlockNumb;
  Int32U  BlockSize;
  DiskStatusCode_t DiskStatus;
  DiskType_t DiskType;
  Boolean WriteProtect;
  Boolean MediaChanged;
} DiskCtrlBlk_t, *pDiskCtrlBlk_t;

typedef void            (* DiskInitFpnt_t)(void);
typedef Int32U          (* DiskInfoFpnt_t)(pInt8U,DiskInfoType_t);
typedef pDiskCtrlBlk_t  (* DiskStatusFpnt_t)(void);
typedef DiskStatusCode_t(* DiskIoFpnt_t)(pInt8U,Int32U,Int32U,DiskIoRequest_t);


#endif //__DISK_H
