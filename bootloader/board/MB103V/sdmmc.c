/**
  **************** (C) COPYRIGHT 2011 SCT Performance, LLC *********************
  * File Name          : sdmmc.c
  * Device / System    : SCT products using the "CommonCode" platform
  * Author             : Quyen Leba
  *
  * Version            : 1 
  * Date               : 05/02/2011
  * Description        : 
  *                    : 
  *
  *
  * History            : 05/02/2011 M. Davis
  *                    :   Add SCT Copyright header,
  *
  ******************************************************************************
  */

#include "sdmmc.h"
#include "delays.h"

#define SDIO_STATIC_FLAGS               (0x000005FFUL)
#define SDIO_CMD0TIMEOUT                (0x00002710UL)
#define SDIO_FIFO_Address               (0x40018080UL)

#define _CSD_GET_TRAN_SPEED_EXP()       (  _MmcSdCsd[0] & 0x07)
#define _CSD_GET_TRAN_SPEED_MANT()      (( _MmcSdCsd[0] & 0xF8) >> 3)
#define _CSD_GET_NSAC()                 (  _MmcSdCsd[1])
#define _CSD_GET_TAAC_EXP()             (  _MmcSdCsd[2] & 0x7)
#define _CSD_GET_TAAC_MANT()            (( _MmcSdCsd[2] & 0xF8) >> 3)
#define _CSD_GET_R2W_FACTOR()           (( _MmcSdCsd[15] & 0x1C) >> 2)
#define _CSD_GET_READ_BL_LEN()          (  _MmcSdCsd[6] & 0x0F)
#define _CSD_GET_C_SIZE()               (((_MmcSdCsd[5] & 0x03) << 10) + (_MmcSdCsd[4] << 2) + ((_MmcSdCsd[11] & 0xc0) >> 6))
#define _CSD_GET_C_SIZE_MULT()          (((_MmcSdCsd[10] & 0x03) << 1 ) + ((_MmcSdCsd[9] & 0x80) >> 7))
#define _CSD_GET_PERM_WRITE_PROTECT()   (( _MmcSdCsd[13] & 0x20) >> 5 )
#define _CSD_GET_TMP_WRITE_PROTECT()    (( _MmcSdCsd[13] & 0x10) >> 4 )
#define _CSD_2_0_GET_C_SIZE()           (((_MmcSdCsd[4] & 0x0F) << 16) + (_MmcSdCsd[11] << 8) + _MmcSdCsd[10])

const u32 _SdTransfExp[] =
{
    10000UL,
    100000UL,
    1000000UL,
    10000000UL,
    0UL,
    0UL,
    0UL,
    0UL,
};

const u32 _SdAccessTime [] =
{
    1UL,
    10UL,
    100UL,
    1000UL,
    10000UL,
    100000UL,
    1000000UL,
    10000000UL,
};

const u32 _SdCsdMant[] =
{
    0UL,10UL,12UL,13UL,15UL,
    20UL,25UL,
    30UL,35UL,
    40UL,45UL,
    50UL,55UL,
    60UL,
    70UL,
    80UL,
};

const SdCommads_t _SdCmd[CMD_END] =
{
    // CMD0
    {0x00,SdNoArg     ,SdNoResp },
    // CMD1
    {0x01,SdOcr       ,SdR3 },
    // CMD2
    {0x02,SdNoArg     ,SdR2 },
    // CMD3
    {0x03,SdRelAddr   ,SdR1 },
    // CMD7
    {0x07,SdRelAddr   ,SdR1 },
    // CMD8
    {0x08,SdDataAdd   ,SdR7 },
    // CMD9
    {0x09,SdRelAddr   ,SdR2 },
    // CMD10
    {0x0A,SdRelAddr   ,SdR2 },
    // CMD12
    {0x0C,SdNoArg     ,SdR1b},
    // CMD13
    {0x0D,SdRelAddr   ,SdR1 },
    // CMD16
    {0x10,SdBlockLen  ,SdR1 },
    // CMD17
    {0x11,SdDataAdd   ,SdR1 },
    // CMD18
    {0x12,SdDataAdd   ,SdR1 },
    // CMD24
    {0x18,SdDataAdd   ,SdR1 },
    // CMD25
    {0x19,SdDataAdd   ,SdR1 },
    // CMD27
    {0x1B,SdOcr       ,SdR1 },
    // CMD28
    {0x1C,SdDataAdd   ,SdR1b},
    // CMD29
    {0x1D,SdDataAdd   ,SdR1b},
    // CMD30
    {0x1E,SdDataAdd   ,SdR1 },
    // CMD32
    {0x20,SdDataAdd   ,SdR1 },
    // CMD33
    {0x21,SdDataAdd   ,SdR1 },
    // CMD34
    {0x22,SdDataAdd   ,SdR1 },
    // CMD35
    {0x23,SdDataAdd   ,SdR1 },
    // CMD36
    {0x24,SdDataAdd   ,SdR1 },
    // CMD37
    {0x25,SdDataAdd   ,SdR1 },
    // CMD38
    {0x26,SdNoArg     ,SdR1b},
    // CMD42
    {0x2A,SdNoArg     ,SdR1b},
    // CMD55
    {0x37,SdRelAddr   ,SdR1 },
    // CMD56
    {0x38,SdNoArg     ,SdR1 },
    // ACMD46
    {0x06,SdDataAdd   ,SdR1 },
    // ACMD41
    {0x29,SdDataAdd   ,SdR3 },
};

DiskCtrlBlk_t _SdDskCtrlBlk;
u32 _SdLastError,_Tnac,_Twr;
Boolean _bSdChanged;
#pragma data_alignment = 4
u8 _MmcSdCsd[16];

static u32 _CardRCA;
static Boolean _bSdPermWriteProtect;
static Boolean _bHC;

u32 Data1;
u32 StatusReg;

//------------------------------------------------------------------------------
// Prototypes
//------------------------------------------------------------------------------
void sdmmc_csd_implement(void);

//------------------------------------------------------------------------------
// Delay 100us x Dly
// Input:   u32 Dly
//------------------------------------------------------------------------------
void Dly100us(void *arg)
{
#define LOOP_DLY_100US  450
    u32 Dly = (u32)arg;
    while(Dly--)
    {
        for(volatile s32 i = LOOP_DLY_100US; i; i--);
    }
}

//------------------------------------------------------------------------------
// Set SD/MMC clock frequency
// Input:   u32 Frequency
// Return:  u32 actual frequency
//------------------------------------------------------------------------------
static
u32 sdmmc_setclkfreq(u32 Frequency)
{
    RCC_ClocksTypeDef RCC_Clocks;
    SDIO_InitTypeDef SDIO_InitStructure;
    u32 Div;
    
    RCC_GetClocksFreq(&RCC_Clocks);
    
    for(Div = 2; Div <= 256; ++Div)
    {
        if ((Frequency * Div) > RCC_Clocks.HCLK_Frequency)
        {
            break;
        }
    }
    
    SDIO_InitStructure.SDIO_ClockDiv = Div;
    SDIO_InitStructure.SDIO_ClockEdge = SDIO_ClockEdge_Rising;
    SDIO_InitStructure.SDIO_ClockBypass = SDIO_ClockBypass_Disable;
    SDIO_InitStructure.SDIO_ClockPowerSave = SDIO_ClockPowerSave_Enable;
    SDIO_InitStructure.SDIO_BusWide = SDIO_BusWide_1b;
    SDIO_InitStructure.SDIO_HardwareFlowControl = SDIO_HardwareFlowControl_Disable;
    SDIO_Init(&SDIO_InitStructure);
    
    // Return real frequency
    return (RCC_Clocks.HCLK_Frequency/Div);
}

//------------------------------------------------------------------------------
// Init SD/MMC port
//------------------------------------------------------------------------------
static
void sdmmc_initport(void)
{
    GPIO_InitTypeDef  GPIO_InitStructure;

    // Configure PC.08, PC.09, PC.10, PC.11, PC.12 pin: D0, D1, D2, D3, CLK pin
    GPIO_InitStructure.GPIO_Pin = GPIO_Pin_8 | GPIO_Pin_9 | GPIO_Pin_10 | GPIO_Pin_11 | GPIO_Pin_12;
    GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
    GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AF_PP;
    GPIO_Init(GPIOC, &GPIO_InitStructure);
        
    // Configure PD.02 CMD line
    GPIO_InitStructure.GPIO_Pin = GPIO_Pin_2;
    GPIO_Init(GPIOD, &GPIO_InitStructure);    
    
    /* Enable the SDIO AHB Clock */
    RCC_AHBPeriphClockCmd(RCC_AHBPeriph_SDIO, ENABLE);
    
    // Enable the DMA2 Clock
    RCC_AHBPeriphClockCmd(RCC_AHBPeriph_DMA2, ENABLE);
    
    SDIO_DeInit();    
}

//------------------------------------------------------------------------------
// Set power down state
//------------------------------------------------------------------------------
static
void sdmmc_powerdown(void)
{
    SDIO_SetPowerState(SDIO_PowerState_OFF);
    // Clock Freq. Identification Mode < 400kHz
    sdmmc_setclkfreq(IdentificationModeClock);
}

//------------------------------------------------------------------------------
// SD/MMC cmd implement
// Input:   SdCmdInd_t ComdInd
//          u32 *pArg
// Return:  SdState_t
//------------------------------------------------------------------------------
static
SdState_t sdmmc_sendcmd(SdCmdInd_t ComdInd, u32 * pArg)
{
    SDIO_CmdInitTypeDef SDIO_CmdInitStructure;
    u32 Status;
    u32 timeout;
    
    // Send Command
    SDIO_CmdInitStructure.SDIO_CmdIndex = _SdCmd[ComdInd].TxData;
    SDIO_CmdInitStructure.SDIO_Response = SDIO_Response_No;
    SDIO_CmdInitStructure.SDIO_Wait     = SDIO_Wait_No;
    SDIO_CmdInitStructure.SDIO_CPSM     = SDIO_CPSM_Enable;
    
    if (pArg != NULL)
    {
        switch(_SdCmd[ComdInd].Resp)
        {
        case SdR2:
            SDIO_CmdInitStructure.SDIO_Response = SDIO_Response_Long;
            break;
        case SdR1:
        case SdR1b:
        case SdR3:
        case SdR7:
            SDIO_CmdInitStructure.SDIO_Response = SDIO_Response_Short;
        }
    }
    
    // Send command's arguments
    if (_SdCmd[ComdInd].Arg != SdNoArg)
    {
        SDIO_CmdInitStructure.SDIO_Argument = *pArg;
    }
    else
    {
        SDIO_CmdInitStructure.SDIO_Argument = 0;
    }
    
    // Clear all the static flags
    SDIO_ClearFlag(SDIO_STATIC_FLAGS);
    
    SDIO_SendCommand(&SDIO_CmdInitStructure);
    
    // Wait command respond
    if (CMD0 == ComdInd)
    {
        timeout = SDIO_CMD0TIMEOUT;
        do
        {
            if (0 == --timeout)
            {
                // Clear all the static flags
                return (SdNoResponse);
            }
            Status = SDIO->STA;
        }
        while (0 == (Status & SDIO_FLAG_CMDSENT));
    }
    else
    {
        do
        {
            Status = SDIO->STA;
        }
        while(!(Status & (SDIO_FLAG_CCRCFAIL | SDIO_FLAG_CMDREND | SDIO_FLAG_CTIMEOUT)));
    }
    
    SDIO_ClearFlag(SDIO_FLAG_CMDSENT);
    
    if (Status & SDIO_FLAG_CTIMEOUT)
    {
        SDIO_ClearFlag(SDIO_FLAG_CTIMEOUT);
        return (SdNoResponse);
    }
    
    if (Status & SDIO_FLAG_CCRCFAIL)
    {
        SDIO_ClearFlag(SDIO_FLAG_CCRCFAIL);
        switch(_SdCmd[ComdInd].TxData)
        {
            // Ignore CRC Error
        case  1: // CMD1
        case 41: // ACMD42
        case 12: // CMD12
            break;
        default:
            return (SdCardError);
        }
    }
    
    if (pArg != NULL)
    {
        switch (_SdCmd[ComdInd].Resp)
        {
        case SdNoResp:
            break;
        case SdR3:
        case SdR7:
            *pArg = SDIO_GetResponse(SDIO_RESP1);
            break;
        case SdR2:
            *pArg++ = SDIO_GetResponse(SDIO_RESP1);
            *pArg++ = SDIO_GetResponse(SDIO_RESP2);
            *pArg++ = SDIO_GetResponse(SDIO_RESP3);
            *pArg++ = SDIO_GetResponse(SDIO_RESP4);
            break;
        default:
            // Check response received is of desired command
            if (SDIO_GetCommandResponse() != _SdCmd[ComdInd].TxData)
            {
                return (SdCardError);
            }
            *pArg = SDIO_GetResponse(SDIO_RESP1);
        }
    }
    SDIO_ClearFlag(SDIO_FLAG_CMDREND);
    return (SdOk);
}

//------------------------------------------------------------------------------
// Init SD/MMC media
// Return:  SdState_t
//------------------------------------------------------------------------------
static
SdState_t sdmmc_initmedia(void)
{
    u32 i,res;
    volatile u32 Dly;
    u8 MmcSdCid[16];
    
    _Tnac = 1;
    
    sdmmc_powerdown();
    SdDly_1ms(100);
    
    // Set Power State to ON
    SDIO_SetPowerState(SDIO_PowerState_ON);
    SdDly_1ms(100);
    
    // Enable SDIO Clock
    SDIO_ClockCmd(ENABLE);
    
    if (sdmmc_sendcmd(CMD0,NULL) != SdOk)
    {
        return (SdNoResponse);
    }
    
    // Determinate Card type SD or MMC
    _SdDskCtrlBlk.DiskType = DiskSD_Spec2_0;
    res = Cmd8Reg;
    if (SdOk == sdmmc_sendcmd(CMD8,&res))
    {
        _bHC = TRUE;
    }
    else
    {
        _SdDskCtrlBlk.DiskType = DiskSD_Spec1_x;
    }
    for(i=100;i;--i)
    {
        res = 0;
        if ((sdmmc_sendcmd(CMD55,&res) == SdOk) &&
            (res & 0x100))
        {
            res = _bHC?OcrReg | OcrReg_HC:OcrReg;
            if ((sdmmc_sendcmd(ACMD41,&res) == SdOk) &&
                (res & 0x80000000))
            {
                // SD card is find
                _bHC = 0 != (res & OcrReg_HC);
                break;
            }
        }
        else
        {
            _SdDskCtrlBlk.DiskType = DiskMMC;
            // CMD1 for MMC Init sequence
            // will be complete within 500ms
            res = OcrReg;
            if (sdmmc_sendcmd(CMD1,&res) ==  SdOk && (res & 0x80000000))
            {
                // MMC card is find
                _bHC = FALSE;
                break;
            }
        }
        
        SdDly_1ms(10);
    }
    
    if (i == 0)
    {
        return (SdNoResponse);
    }
    
    // Read CID
    if (sdmmc_sendcmd(CMD2,(u32 *)MmcSdCid) != SdOk)
    {
        return (SdNoResponse);
    }
    
    // Set address
    _CardRCA = (_SdDskCtrlBlk.DiskType == DiskMMC)?0x00010000:0x00000000;
    if (sdmmc_sendcmd(CMD3,&_CardRCA) != SdOk)
    {
        return (SdNoResponse);
    }
    if (DiskMMC != _SdDskCtrlBlk.DiskType)
    {
        _CardRCA &= 0xFFFF0000;
    }
    else
    {
        _CardRCA  = 0x00010000;
    }
    
    // Read CSD
    _MmcSdCsd[0] = 0;
    _MmcSdCsd[1] = 0;
    _MmcSdCsd[2] = _CardRCA >> 16;
    _MmcSdCsd[3] = _CardRCA >> 24;
    if (sdmmc_sendcmd(CMD9,(u32 *)_MmcSdCsd) != SdOk)
    {
        return (SdNoResponse);
    }
    
    // Implement CSD data
    sdmmc_csd_implement();
    
    // Enter in TRAN state
    res = _CardRCA;
    if (sdmmc_sendcmd(CMD7,&res) != SdOk)
    {
        return (SdNoResponse);
    }
    res = _CardRCA;
    if (SdOk != sdmmc_sendcmd(CMD13,&res))
    {
        return (SdNoResponse);
    }
    else if (!(res & READY_FOR_DATA) ||
             ((res & CURRENT_STATE) != CARD_TRAN))
    {
        return (SdCardError);
    }
    
    // Set Block size
    res = _SdDskCtrlBlk.BlockSize;
    if (sdmmc_sendcmd(CMD16,&res))
    {
        return (SdNoResponse);
    }
    
    if (DiskMMC != _SdDskCtrlBlk.DiskType)
    {
        // Use wide bus for SD
        res = _CardRCA;
        if ((sdmmc_sendcmd(CMD55,&res) != SdOk) ||
            !(res & 0x100))
        {
            return (SdCardError);
        }
        // Set bus width 4bits
        res = 2;
        if (sdmmc_sendcmd(ACMD6,&res) != SdOk)
        {
            return (SdCardError);
        }
        SDIO->CLKCR |= 1UL << 11;
    }
    return (SdOk);
}

//------------------------------------------------------------------------------
// Implement data from CSD
//------------------------------------------------------------------------------
static
void sdmmc_csd_implement(void)
{
    u32 Freq;
    Int64U Tmp;
    
    // Calculate SPI max clock
    Freq = _SdTransfExp[_CSD_GET_TRAN_SPEED_EXP()] * _SdCsdMant[_CSD_GET_TRAN_SPEED_MANT()];
    Freq = sdmmc_setclkfreq(Freq);
    if (DiskMMC == _SdDskCtrlBlk.DiskType)
    {
        // Calculate Time outs for MMC cards
        Tmp = _SdAccessTime[_CSD_GET_TAAC_EXP()] * _SdCsdMant[_CSD_GET_TAAC_MANT()];
        Tmp /= 10000; // us
        // Freq [Hz], Tmp[1 us], *10
        Tmp = (Freq*Tmp)/100000LL;
        // NSAC*100*10
        Tmp += 1000*_CSD_GET_NSAC();
        // Max time out
        _Tnac = Tmp;
        _Twr  = Tmp * (1<<_CSD_GET_R2W_FACTOR());
    }
    else
    {
        // Calculate Time outs for SD cards
        // Freq [Hz], RD_TIME_OUT[ms]
        Tmp = Freq/1000;
        _Tnac = Tmp * RD_TIME_OUT;
        _Twr  = Tmp * WR_TIME_OUT;
    }
    // Calculate Block size and Block Number
    _SdDskCtrlBlk.BlockSize = 1<<_CSD_GET_READ_BL_LEN();
    if (_bHC)
    {
        _SdDskCtrlBlk.BlockNumb = (_CSD_2_0_GET_C_SIZE()+1) * 1024;
    }
    else
    {
        _SdDskCtrlBlk.BlockNumb = (_CSD_GET_C_SIZE()+1)*(4<<_CSD_GET_C_SIZE_MULT());
    }
    if (_SdDskCtrlBlk.BlockSize != SD_DEF_BLK_SIZE)
    {
        // because Windows support only 512 bytes block
        _SdDskCtrlBlk.BlockNumb *= (_SdDskCtrlBlk.BlockSize>>9);
        _SdDskCtrlBlk.BlockSize  = SD_DEF_BLK_SIZE;
    }
    // Set Write Protect
    _bSdPermWriteProtect = _CSD_GET_PERM_WRITE_PROTECT() |
        _CSD_GET_TMP_WRITE_PROTECT();
    _SdDskCtrlBlk.WriteProtect = _bSdPermWriteProtect;
}

//------------------------------------------------------------------------------
// SD/MMC block read
// Input:   u8  *pData
//          u32 Add
//          u32 Length (block count)
// Return:  SdState_t
//------------------------------------------------------------------------------
//static inline
SdState_t sdmmc_blockread(u8 *pData, u32 Add, u32 Length)
{
    DMA_InitTypeDef DMA_InitStructure;
    SDIO_DataInitTypeDef SDIO_DataInitStructure;
    SdState_t Status = SdOk;
    u32 Data;
    u32 StatusReg;
    
    if (Length == 0)
    {
        return (Status);
    }
    
    // Init data path
    SDIO_DataInitStructure.SDIO_DataTimeOut   = _Tnac;
    SDIO_DataInitStructure.SDIO_DataLength    = _SdDskCtrlBlk.BlockSize;
    SDIO_DataInitStructure.SDIO_DataBlockSize = SDIO_DataBlockSize_512b;
    SDIO_DataInitStructure.SDIO_TransferDir   = SDIO_TransferDir_ToSDIO;
    SDIO_DataInitStructure.SDIO_TransferMode  = SDIO_TransferMode_Block;
    SDIO_DataInitStructure.SDIO_DPSM          = SDIO_DPSM_Enable;
    SDIO_DataConfig(&SDIO_DataInitStructure);
    
    // DMA init
    DMA_InitStructure.DMA_PeripheralBaseAddr  = (u32)SDIO_FIFO_Address;
    DMA_InitStructure.DMA_DIR                 = DMA_DIR_PeripheralSRC;
    DMA_InitStructure.DMA_BufferSize          = _SdDskCtrlBlk.BlockSize / 4;
    DMA_InitStructure.DMA_PeripheralInc       = DMA_PeripheralInc_Disable;
    DMA_InitStructure.DMA_MemoryInc           = DMA_MemoryInc_Enable;
    DMA_InitStructure.DMA_PeripheralDataSize  = DMA_PeripheralDataSize_Word;
    DMA_InitStructure.DMA_MemoryDataSize      = DMA_MemoryDataSize_Word;
    DMA_InitStructure.DMA_Mode                = DMA_Mode_Normal;
    DMA_InitStructure.DMA_Priority            = DMA_Priority_High;
    DMA_InitStructure.DMA_M2M                 = DMA_M2M_Disable;
    
    while(Length)
    {
        // Set data size for one block
        Data = Add;
        Status = sdmmc_sendcmd(CMD17,&Data);
        if (SdOk != Status)
        {
            return (Status);
        }
        
        if (!(Data & READY_FOR_DATA) ||
            ((Data & CURRENT_STATE) != CARD_TRAN))
        {
            sdmmc_sendcmd(CMD12,NULL);
            return (SdCardError);
        }
        
        // DMA2 Channel4 Config
        DMA_InitStructure.DMA_MemoryBaseAddr = (u32)pData;
        
        // DMA2 Channel4 disable
        DMA_Cmd(DMA2_Channel4, DISABLE);
        
        // Clear DMA flags
        DMA_ClearFlag(DMA2_FLAG_TC4 | DMA2_FLAG_TE4 |
                      DMA2_FLAG_HT4 | DMA2_FLAG_GL4);
        
        // Clear all SDIO related flags
        SDIO_ClearFlag(SDIO_IT_DTIMEOUT | SDIO_IT_STBITERR | SDIO_IT_DCRCFAIL |
                       SDIO_IT_RXOVERR | SDIO_IT_DATAEND | SDIO_IT_DBCKEND);
        
        // DMA2 Channel4 Config
        DMA_Init(DMA2_Channel4, &DMA_InitStructure);
        
        // DMA2 Channel4 enable
        DMA_Cmd(DMA2_Channel4, ENABLE);
        
        SDIO_DMACmd(ENABLE);
        
        // wait receive complete
        while (DMA_GetFlagStatus(DMA2_FLAG_GL4) == RESET)
        {
            StatusReg = SDIO->STA;
            if (StatusReg & (SDIO_IT_DTIMEOUT | SDIO_IT_STBITERR |
                             SDIO_IT_DCRCFAIL | SDIO_IT_RXOVERR))
            {
                break;
            }
        }
        
        if (DMA_GetFlagStatus(DMA2_FLAG_TE4))
        {
            return (SdDmaError);
        }
        
        StatusReg = SDIO->STA;
        
        // data timeout
        if (StatusReg & SDIO_IT_DTIMEOUT)
        {
            SDIO_ClearFlag(SDIO_STATIC_FLAGS);
            return (SdNoResponse);
        }
        
        // Start bit not detected on all data signals in wide bus mode.
        if (StatusReg & SDIO_IT_STBITERR)
        {
            SDIO_ClearFlag(SDIO_STATIC_FLAGS);
            return (SdCardError);
        }
        
        // CRC error
        if (StatusReg & SDIO_IT_DCRCFAIL)
        {
            // clear error flag
            SDIO_ClearFlag(SDIO_STATIC_FLAGS);
            return (SdCardError);
        }
        
        // rx overrun
        if (StatusReg & SDIO_IT_RXOVERR)
        {
            SDIO_ClearFlag(SDIO_STATIC_FLAGS);
            return (SdCardError);
        }
        
        if (_bHC)
        {
            ++Add;
        }
        else
        {
            Add += _SdDskCtrlBlk.BlockSize;
        }
        --Length;
        pData += _SdDskCtrlBlk.BlockSize;
        
        do
        {
            Data1 = _CardRCA;
            if (sdmmc_sendcmd(CMD13,&Data1) != SdOk)
            {
                continue;
            }
            Data1 >>= 9;
            Data1 &= 0xF;
        }
        while(5 == Data1);  //expect 4
        
        // data end
        if (!Length)
        {
            return (Status);
        }
        // loop read next block
    }
    
    return (Status);
}

//------------------------------------------------------------------------------
// SD/MMC block write
// Input:   const u8 *pData
//          u32 Add
//          u32 Length (block count)
// Return:  SdState_t
//------------------------------------------------------------------------------
static inline
SdState_t sdmmd_blockwrite(const u8 * pData, u32 Add, u32 Length)
{
    SdState_t Status = SdOk;
    u32 Data;
    DMA_InitTypeDef DMA_InitStructure;
    SDIO_DataInitTypeDef SDIO_DataInitStructure;
    
    //Data1 = 0xDEDDABBA;
    
    if (!Length)
    {
        return (Status);
    }
    
    // Init data path
    SDIO_DataInitStructure.SDIO_DataTimeOut   = _Twr;
    SDIO_DataInitStructure.SDIO_DataLength    = _SdDskCtrlBlk.BlockSize;
    SDIO_DataInitStructure.SDIO_DataBlockSize = SDIO_DataBlockSize_512b;
    SDIO_DataInitStructure.SDIO_TransferDir   = SDIO_TransferDir_ToCard;
    SDIO_DataInitStructure.SDIO_TransferMode  = SDIO_TransferMode_Block;
    SDIO_DataInitStructure.SDIO_DPSM          = SDIO_DPSM_Enable;
    SDIO_DataConfig(&SDIO_DataInitStructure);
    
    // DMA init
    DMA_InitStructure.DMA_PeripheralBaseAddr  = (u32)SDIO_FIFO_Address;
    DMA_InitStructure.DMA_DIR                 = DMA_DIR_PeripheralDST;
    DMA_InitStructure.DMA_BufferSize          = _SdDskCtrlBlk.BlockSize / 4;
    DMA_InitStructure.DMA_PeripheralInc       = DMA_PeripheralInc_Disable;
    DMA_InitStructure.DMA_MemoryInc           = DMA_MemoryInc_Enable;
    DMA_InitStructure.DMA_PeripheralDataSize  = DMA_PeripheralDataSize_Word;
    DMA_InitStructure.DMA_MemoryDataSize      = DMA_MemoryDataSize_Word;
    DMA_InitStructure.DMA_Mode                = DMA_Mode_Normal;
    DMA_InitStructure.DMA_Priority            = DMA_Priority_High;
    DMA_InitStructure.DMA_M2M                 = DMA_M2M_Disable;
    
    while(Length)
    {
        Data = Add;
        Status = sdmmc_sendcmd(CMD24,&Data);
        if (SdOk != Status)
        {
            return (Status);
        }
        
        if (!(Data & READY_FOR_DATA) ||
            ((Data & CURRENT_STATE) != CARD_TRAN))
        {
            sdmmc_sendcmd(CMD12,NULL);
            return (SdCardError);
        }
        
        // set destination address
        DMA_InitStructure.DMA_MemoryBaseAddr = (u32)pData;
        
        // DMA2 Channel4 disable
        DMA_Cmd(DMA2_Channel4, DISABLE);
        
        // Clear DMA flags
        DMA_ClearFlag(DMA2_FLAG_TC4 | DMA2_FLAG_TE4 |
                      DMA2_FLAG_HT4 | DMA2_FLAG_GL4);
        
        // Clear all SDIO related flags
        SDIO_ClearFlag(SDIO_IT_DTIMEOUT | SDIO_IT_STBITERR | SDIO_IT_DCRCFAIL |
                       SDIO_IT_TXUNDERR | SDIO_IT_DATAEND | SDIO_IT_DBCKEND);
        
        // DMA2 Channel4 Config
        DMA_Init(DMA2_Channel4, &DMA_InitStructure);
        
        // DMA2 Channel4 enable
        DMA_Cmd(DMA2_Channel4, ENABLE);
        
        SDIO_DMACmd(ENABLE);
        
        // wait transfer complete
        do
        {
            StatusReg = SDIO->STA;
        }
        while(
              ((DMA_GetFlagStatus(DMA2_FLAG_GL4) == RESET) || 
               (0 == (StatusReg & SDIO_IT_DBCKEND))) && 
                  (0 == (StatusReg & (SDIO_IT_DTIMEOUT | SDIO_IT_STBITERR |
                                      SDIO_IT_DCRCFAIL | SDIO_IT_TXUNDERR))));
        
        
        if (DMA_GetFlagStatus(DMA2_FLAG_TE4))
        {
            return (SdDmaError);
        }
        
        StatusReg = SDIO->STA;
        // data timeout
        if (StatusReg & SDIO_IT_DTIMEOUT)
        {
            SDIO_ClearFlag(SDIO_STATIC_FLAGS);
            return (SdNoResponse);
        }
        
        // Start bit not detected on all data signals in wide bus mode.
        if (StatusReg & SDIO_IT_STBITERR)
        {
            SDIO_ClearFlag(SDIO_STATIC_FLAGS);
            return (SdCardError);
        }
        
        // CRC error
        if (StatusReg & SDIO_IT_DCRCFAIL)
        {
            // clear error flag
            SDIO_ClearFlag(SDIO_STATIC_FLAGS);
            return (SdCardError);
        }
        
        // tx underrun
        if (StatusReg & SDIO_IT_TXUNDERR)
        {
            SDIO_ClearFlag(SDIO_STATIC_FLAGS);
            return (SdCardError);
        }
        
        if (_bHC)
        {
            ++Add;
        }
        else
        {
            Add += _SdDskCtrlBlk.BlockSize;
        }
        --Length;
        pData += _SdDskCtrlBlk.BlockSize;
        
        // wait write until write is done
        do
        {
            Data1 = _CardRCA;
            if (sdmmc_sendcmd(CMD13,&Data1) != SdOk)
            {
                continue;
            }
            Data1 >>= 9;
            Data1 &= 0xF;
        }
        while((7 == Data1) || (6 == Data1));
        
        
        // data end
        if (!Length)
        {
            return (Status);
        }
        // loop write next block
    }
    
    return (Status);
}

//------------------------------------------------------------------------------
// SD/MMC block verify
// Input:   const u8 *pData
//          u32 Add
//          u32 Length (block count)
// Return:  SdState_t
//------------------------------------------------------------------------------
static inline
SdState_t sdmmc_blockverify(const u8 * pData, u32 Add, u32 Length)
{
#pragma segment="USB_DMA_RAM"
#pragma location="USB_DMA_RAM"
    __no_init static u8 TempBuffer[2048];  // maximum block length is 2048
    SdState_t Status = SdOk;
    
    while(Length)
    {
        Status = sdmmc_blockread(TempBuffer,Add,_SdDskCtrlBlk.BlockSize);
        if (Status != SdOk)
        {
            break;
        }
        for(u32 i = 0; i < _SdDskCtrlBlk.BlockSize; ++i)
        {
            if (TempBuffer[i] != *pData++)
            {
                return (SdMiscompare);
            }
        }
        if (_bHC)
        {
            --Length;
            ++Add;
        }
        else
        {
            Length -= _SdDskCtrlBlk.BlockSize;
            Add += _SdDskCtrlBlk.BlockSize;
        }
        pData += _SdDskCtrlBlk.BlockSize;
    }
    return (Status);
}

//------------------------------------------------------------------------------
// Get last error
// Return: u32 last error
//------------------------------------------------------------------------------
u32 sdmmc_getlasterror(void)
{
    return (_SdLastError);
}

//------------------------------------------------------------------------------
// Update status of SD/MMC
//------------------------------------------------------------------------------
void sdmmc_statusupdate(void)
{
    u32 Data = _CardRCA;
    
    // Update WP state
    _SdDskCtrlBlk.WriteProtect = _bSdPermWriteProtect;
    
    if ((_SdDskCtrlBlk.DiskStatus != DiskCommandPass))
    {
        switch (sdmmc_initmedia())
        {
        case SdOk:
            _SdDskCtrlBlk.DiskStatus = DiskCommandPass;
            _SdDskCtrlBlk.MediaChanged = TRUE;
            break;
        case SdCardError:
            _SdDskCtrlBlk.DiskStatus = DiskNotReady;
            break;
        default:
            _SdDskCtrlBlk.DiskStatus = DiskNotPresent;
            break;
        }
    }
    else if (sdmmc_sendcmd(CMD13,&Data) != SdOk)
    {
        _SdDskCtrlBlk.DiskStatus = DiskNotReady;
    }
    else if (!(Data & READY_FOR_DATA))
    {
        _SdDskCtrlBlk.DiskStatus = DiskNotReady;
    }
}

//------------------------------------------------------------------------------
// Init SD/MMC drive
//------------------------------------------------------------------------------
void sdmmc_init(void)
{
    _SdDskCtrlBlk.BlockNumb = 0;
    _SdDskCtrlBlk.BlockSize = 0;
    _SdLastError            = 0;
    
    sdmmc_initport();
    switch(sdmmc_initmedia())
    {
    case SdOk:
        _SdDskCtrlBlk.DiskStatus = DiskCommandPass;
        _SdDskCtrlBlk.MediaChanged = TRUE;
        break;
    case SdCardError:
        _SdDskCtrlBlk.DiskStatus = DiskNotReady;
        break;
    default:
        _SdDskCtrlBlk.DiskStatus = DiskNotPresent;
        break;
    }
}

//------------------------------------------------------------------------------
// Return pointer to control block structure of the disk
// Return:  pDiskCtrlBlk_t
//------------------------------------------------------------------------------
pDiskCtrlBlk_t sdmmc_getdiskcontrolblock(void)
{
    return (&_SdDskCtrlBlk);
}

//------------------------------------------------------------------------------
// SD/MMC IO
// Input:   u8  *pData
//          u32 BlockStart,
//          u32 BlockNum
//          DiskIoRequest_t IoRequest
// Return:  DiskStatusCode_t
//------------------------------------------------------------------------------

DiskStatusCode_t sdmmc_diskio(u8 * pData,u32 BlockStart,
                              u32 BlockNum, DiskIoRequest_t IoRequest)
{
    if ((pData == NULL) || (BlockStart+BlockNum > _SdDskCtrlBlk.BlockNumb))
    {
        return (DiskParametersError);
    }
    if (_SdDskCtrlBlk.DiskStatus != DiskCommandPass)
    {
        return (_SdDskCtrlBlk.DiskStatus);
    }
    
    switch(IoRequest)
    {
    case DiskWrite:
        if (_SdDskCtrlBlk.WriteProtect)
        {
            return (DiskParametersError);
        }
        switch(sdmmd_blockwrite(pData,
                        _bHC?BlockStart:BlockStart*_SdDskCtrlBlk.BlockSize,
                        BlockNum))
        {
        case SdOk:
            break;
        case SdCardError:
            _SdDskCtrlBlk.DiskStatus = DiskNotReady;
            break;
        default:
            _SdDskCtrlBlk.DiskStatus = DiskNotPresent;
            break;
        }
        break;
    case DiskRead:
        switch(sdmmc_blockread(pData,
                       _bHC?BlockStart:BlockStart*_SdDskCtrlBlk.BlockSize,
                       BlockNum))
        {
        case SdOk:
            break;
        case SdCardError:
            _SdDskCtrlBlk.DiskStatus = DiskNotReady;
            break;
        default:
            _SdDskCtrlBlk.DiskStatus = DiskNotPresent;
            break;
        }
        break;
    case DiskVerify:
        switch(sdmmc_blockverify(pData,
                         _bHC?BlockStart:BlockStart*_SdDskCtrlBlk.BlockSize,
                         BlockNum))
        {
        case SdOk:
            break;
        case SdMiscompare:
            return (DiskMiscompareError);
        case SdCardError:
            _SdDskCtrlBlk.DiskStatus = DiskNotReady;
            break;
        default:
            _SdDskCtrlBlk.DiskStatus = DiskNotPresent;
            break;
        }
        break;
    default:
        return (DiskParametersError);
    }
    return (_SdDskCtrlBlk.DiskStatus);
}

