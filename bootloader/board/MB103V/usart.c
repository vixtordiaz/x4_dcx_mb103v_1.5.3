/**
  **************** (C) COPYRIGHT 2011 SCT Performance, LLC *********************
  * File Name          : usart.c
  * Device / System    : SCT products using the "CommonCode" platform
  * Author             : Quyen Leba
  *
  * Version            : 1 
  * Date               : 05/02/2011
  * Description        : 
  *                    : 
  *
  *
  * History            : 05/02/2011 M. Davis
  *                    :   Add SCT Copyright header,
  *
  ******************************************************************************
  */

#include "stm32f10x_gpio.h"
#include "stm32f10x_rcc.h"
#include "usart.h"

//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
void usart2_init(USARTBAUD baud)
{
    USART_InitTypeDef USART_InitStructure;
    GPIO_InitTypeDef GPIO_InitStructure;

    /* Enable USART2 clocks */
    RCC_APB1PeriphClockCmd(RCC_APB1Periph_USART2, ENABLE);

    /* Configure USART2 RTS and USART2 Tx as alternate function push-pull */
    //GPIO_InitStructure.GPIO_Pin = GPIO_Pin_1 | GPIO_Pin_2;
    GPIO_InitStructure.GPIO_Pin = GPIO_Pin_4 | GPIO_Pin_5;
    GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
    GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AF_PP;
    GPIO_Init(GPIOD, &GPIO_InitStructure);
    //GPIO_Init(GPIOA, &GPIO_InitStructure);
    
    /* Configure USART2 CTS and USART2 Rx as input floating */
    //GPIO_InitStructure.GPIO_Pin = GPIO_Pin_3 | GPIO_Pin_0;
    GPIO_InitStructure.GPIO_Pin = GPIO_Pin_3 | GPIO_Pin_6;
    GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IN_FLOATING;
    //GPIO_Init(GPIOA, &GPIO_InitStructure);
    GPIO_Init(GPIOD, &GPIO_InitStructure);
    GPIO_PinRemapConfig(GPIO_Remap_USART2, ENABLE);
        
    /* USART2 configuration ------------------------------------------------------*/
    /* USART2 configured as follow:
    - BaudRate = 115200 baud  
    - Word Length = 8 Bits
    - One Stop Bit
    - No parity
    - Hardware flow control enabled (RTS and CTS signals)
    - Receive and transmit enabled
    */
    USART_DeInit(USART2);
    USART_InitStructure.USART_BaudRate = (u32)baud;
    USART_InitStructure.USART_WordLength = USART_WordLength_8b;
    USART_InitStructure.USART_StopBits = USART_StopBits_1;
    USART_InitStructure.USART_Parity = USART_Parity_No;
    USART_InitStructure.USART_HardwareFlowControl = USART_HardwareFlowControl_RTS_CTS;
    //USART_InitStructure.USART_HardwareFlowControl = USART_HardwareFlowControl_None;
    USART_InitStructure.USART_Mode = USART_Mode_Rx | USART_Mode_Tx;
    
    USART_Init(USART2, &USART_InitStructure);
    
    // Enable USART2 Receive and Transmit interrupts 
    USART_ITConfig(USART2, USART_IT_RXNE, ENABLE);
    //USART_ITConfig(USART2, USART_IT_TXE, ENABLE);
    
    /* Enable the USART2 */
    USART_Cmd(USART2, ENABLE);
}

//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
void usart2_changebaudrate(USARTBAUD baud)
{
    u32 tmpreg = 0x00, apbclock = 0x00;
    u32 integerdivider = 0x00;
    u32 fractionaldivider = 0x00;
    //  u32 usartxbase = 0;
    RCC_ClocksTypeDef RCC_ClocksStatus;
    
    /* DISABLE USART2 Receive and Transmit interrupts */
    //USART_ITConfig(USART2, USART_IT_RXNE, DISABLE);
    
    RCC_GetClocksFreq(&RCC_ClocksStatus);  
    
    apbclock = RCC_ClocksStatus.PCLK1_Frequency;
    
    /* Determine the integer part */
    integerdivider = ((0x19 * apbclock) / (0x04 * ((u32)baud)));
    tmpreg = (integerdivider / 0x64) << 0x04;
    
    /* Determine the fractional part */
    fractionaldivider = integerdivider - (0x64 * (tmpreg >> 0x04));
    tmpreg |= ((((fractionaldivider * 0x10) + 0x32) / 0x64)) & ((u8)0x0F);
    
    //  while(USART_GetFlagStatus(USART2, USART_FLAG_IDLE) != RESET);
    while(USART_GetFlagStatus(USART2, USART_FLAG_TC) == RESET);
    
    /* Write to USART BRR */
    USART2->BRR = (u16)tmpreg;
    
    //USART_ClearFlag(USART2, USART_FLAG_RXNE);
    //USART_ClearITPendingBit(USART2, USART_IT_RXNE);
    
    /* Enable USART2 Receive and Transmit interrupts */
    //USART_ITConfig(USART2, USART_IT_RXNE, ENABLE);
    
    /* Enable the USART2 */
    //USART_Cmd(USART2, ENABLE);
}

//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
//void usart2_tx(u8 databyte)
//{
//    USART_SendData(USART2, databyte);
//    while(USART_GetFlagStatus(USART2, USART_FLAG_TXE) == RESET);
//}
