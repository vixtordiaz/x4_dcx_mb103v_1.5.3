/**
  **************** (C) COPYRIGHT 2011 SCT Performance, LLC *********************
  * File Name          : init.h
  * Device / System    : SCT products using the "CommonCode" platform
  * Author             : Quyen Leba
  *
  * Version            : 1 
  * Date               : 05/02/2011
  * Description        : 
  *                    : 
  *
  *
  * History            : 05/02/2011 M. Davis
  *                    :   Add SCT Copyright header,
  *
  ******************************************************************************
  */

#ifndef __INIT_H
#define __INIT_H

void init();

#endif	//__INIT_H
