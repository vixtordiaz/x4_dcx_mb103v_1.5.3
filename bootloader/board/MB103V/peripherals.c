/**
  **************** (C) COPYRIGHT 2011 SCT Performance, LLC *********************
  * File Name          : peripherals.c
  * Device / System    : SCT products using the "CommonCode" platform
  * Author             : Quyen Leba
  *
  * Version            : 1 
  * Date               : 05/02/2011
  * Description        : 
  *                    : 
  *
  *
  * History            : 05/02/2011 M. Davis
  *                    :   Add SCT Copyright header,
  *
  ******************************************************************************
  */

#include <stm32f10x_systick.h>
#include <stm32f10x_rcc.h>
#include <board/genplatform.h>
#include <board/power.h>
#include <board/timer.h>
#include <board/rtc.h>
#include <board/bootloader.h>
#include <board/peripherals.h>
#include <common/bootsettings.h>
#include <common/settings.h>
#include <common/cmdif.h>
#include "pff.h"
#include "gpio.h"
#include "led.h"
#include "spi.h"
#include "usart.h"


//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
extern void USB_Disconnect_Config(void);
extern void Set_USBClock(void);
extern void USB_Interrupts_Config(void);
extern void USB_Init(void);


//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
void peripherals_init()
{
    //Enable GPIOA, GPIOB, GPIOC, GPIOD, GPIOE, GPIOF, GPIOG clocks
    //Enable AFIO and SPI1 clocks
    RCC->APB2ENR |=  (RCC_APB2Periph_GPIOA | RCC_APB2Periph_GPIOB |
                      RCC_APB2Periph_GPIOC | RCC_APB2Periph_GPIOD |
                      RCC_APB2Periph_GPIOE | RCC_APB2Periph_GPIOF |
                      RCC_APB2Periph_GPIOG | RCC_APB2Periph_AFIO |
                      RCC_APB2Periph_SPI1);
    //Enable TIM2 clock
    RCC->APB1ENR |= (RCC_APB1Periph_TIM2 | RCC_APB1Periph_TIM4);
    //Enable CRC clock
    RCC->AHBENR |= RCC_AHBPeriph_CRC;
    
    //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    // Configure a SysTick Base time to 1 us.
    //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    #define CTRL_TICKINT_Set      ((u32)0x00000002)
    //SysTick interrupt each 100 Hz with HCLK equal to 72MHz
    SysTick->LOAD = 72000;
    //Configure HCLK clock as SysTick clock source
    //Enable the SysTick Interrupt
    SysTick->CTRL |= (SysTick_CLKSource_HCLK | CTRL_TICKINT_Set);

    gpio_init();
    USB_Disconnect_Config();
    timeout_init();
    board_timeout_init();
    spi_master_init();

    led_init();
    button_init();

    usart2_init(BAUD115200);

    bootsettings_load();
    settings_load();
    bootloader_init();
    properties_init();

    led_enter_bootloader();
    
    USB_Disconnect_Config();    //TODOQ: clean me
    Set_USBClock();
    USB_Interrupts_Config();
    USB_Init();
    
    cmdif_init();
    
    gpio_config_Bluetooth_Force115KBaud(TRUE);
    bluetooth_init(BT_115200_BAUD); //TODO: for now of iTSX
}
