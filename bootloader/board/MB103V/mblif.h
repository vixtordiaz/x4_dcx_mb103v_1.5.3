/**
  **************** (C) COPYRIGHT 2011 SCT Performance, LLC *********************
  * File Name          : mblif.h
  * Device / System    : SCT products using the "CommonCode" platform
  * Author             : Quyen Leba
  *
  * Version            : 1 
  * Date               : 05/02/2011
  * Description        : 
  *                    : 
  *
  *
  * History            : 05/02/2011 M. Davis
  *                    :   Add SCT Copyright header,
  *
  ******************************************************************************
  */

#ifndef __MBLIF_H
#define __MBLIF_H

#include <arch/gentype.h>

typedef enum
{
    MBLIF_CMD_IDLE                          = 0x00,
    MBLIF_CMD_RDID                          = 0x01,
    MBLIF_CMD_DEBUG1                        = 0x02,
    MBLIF_CMD_DEBUG2                        = 0x03,
    MBLIF_CMD_WRITE_BUFFER                  = 0x04,
    MBLIF_CMD_READ_BUFFER                   = 0x05,
    MBLIF_CMD_EXEC                          = 0x06,
    MBLIF_CMD_GET_STATUS                    = 0x07,

    MBLIF_CMD_DUMMY                         = 0xFF,
}mblif_cmds;

#define MBLIF_CMD_RESPONSE_S1               0xF1
#define MBLIF_CMD_RESPONSE_S2               0xF2
#define MBLIF_CMD_RESPONSE_S3               0xF3
#define MBLIF_CMD_RESPONSE_S4               0xF4

#define MBLIF_CMD_RDID_S1                   0xAE
#define MBLIF_CMD_RDID_S2                   0x55
#define MBLIF_CMD_RDID_S3                   0x0F

#define MBLIF_CMD_WRITE_BUFFER_S1           0x54
#define MBLIF_CMD_WRITE_BUFFER_S2           0x39
#define MBLIF_CMD_WRITE_BUFFER_S3           0xA2

#define MBLIF_CMD_READ_BUFFER_S1            0xC3
#define MBLIF_CMD_READ_BUFFER_S2            0x16
#define MBLIF_CMD_READ_BUFFER_S3            0x47

#define MBLIF_CMD_EXEC_S1                   0x42
#define MBLIF_CMD_EXEC_S2                   0xE4
#define MBLIF_CMD_EXEC_S3                   0x83

#define MBLIF_CMD_GET_STATUS_S1             0x52

typedef struct
{
    u8  read        :1;
    u8  write       :1;
    u8  exec_avail  :1;     // an execution request is available
    u8  exec_busy   :1;     // 1: execution is in progress
    u8  exec_done   :1;
    u8  exec_ok     :1;
    u8  reserved    :2;
    u8  x1;
    u8  x2;
    u8  status;
}MBLIF_CONTROL;

void mblif_init();
u8 mblif_link(u8 cmd, u16 opcode, u8 *data, u16 *datalength);

#endif  //__MBLIF_H
