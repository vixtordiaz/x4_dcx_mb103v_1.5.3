/**
  **************** (C) COPYRIGHT 2011 SCT Performance, LLC *********************
  * File Name          : mblif.c
  * Device / System    : SCT products using the "CommonCode" platform
  * Author             : Quyen Leba
  *
  * Version            : 1 
  * Date               : 05/02/2011
  * Description        : 
  *                    : 
  *
  *
  * History            : 05/02/2011 M. Davis
  *                    :   Add SCT Copyright header,
  *
  ******************************************************************************
  */

#include <string.h>
#include <board/genplatform.h>
#include <board/delays.h>
#include <common/statuscode.h>
#include "mblif.h"

//for now
#define mblif_sendbyte(b)           spi_master_send_byte(b)
#define mblif_receivebyte()         spi_master_receive_byte()
#define mblif_isbusy()              (gpio_read_dav_v_busy_pin())

//------------------------------------------------------------------------------
// Private prototypes
//------------------------------------------------------------------------------
u8 mblif_get_status(u8 *status, u16 *statuslength);
u8 mblif_rdid(u8 *id, u16 *idlength);
u8 mblif_debug1(u8 *id, u16 *idlength);
u8 mblif_writebuffer(u8 *buffer, u16 bufferlength);
u8 mblif_readbuffer(u8 *buffer, u16 *bufferlength);
u8 mblif_exec(u16 opcode, u8 *data, u8 datalength);

//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
void mblif_init()
{
    
}

//------------------------------------------------------------------------------
// Inputs:  u8  cmd
//          u16 opcode (only used in certain cmds)
//          u8  *data
//          u8  *datalength
// Return:  u8  status
// Engineer: Quyen Leba
//------------------------------------------------------------------------------
u8 mblif_link(u8 cmd, u16 opcode, u8 *data, u16 *datalength)
{
    u8  status;

    board_timeout_set(100);
    while(mblif_isbusy())
    {
        if (board_timeout())
        {
            return S_TIMEOUT;
        }
    }

    switch(cmd)
    {
    case MBLIF_CMD_GET_STATUS:
        status = mblif_get_status(data,datalength);
        break;
    case MBLIF_CMD_RDID:
        status = mblif_rdid(data,datalength);
        break;
    case MBLIF_CMD_WRITE_BUFFER:
        status = mblif_writebuffer(data,*datalength);
        break;
    case MBLIF_CMD_READ_BUFFER:
        status = mblif_readbuffer(data,datalength);
        break;
    case MBLIF_CMD_EXEC:
        status = mblif_exec(opcode,data,(u8)*datalength);
        break;
    default:
        status = S_INPUT;
        break;
    }
    
    return status;
}

//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
u8 mblif_get_status(u8 *status, u16 *statuslength)
{
    u8  sequenceresponse;
    u8  i;

    mblif_sendbyte((u8)MBLIF_CMD_GET_STATUS);
    mblif_receivebyte();        //dummy

    mblif_sendbyte((u8)MBLIF_CMD_GET_STATUS_S1);
    sequenceresponse = mblif_receivebyte();
    if (sequenceresponse != MBLIF_CMD_RESPONSE_S1)
    {
        return S_UNMATCH;
    }

    for(i=0;i<4;i++)
    {
        mblif_sendbyte(MBLIF_CMD_RESPONSE_S4);
        status[i] = mblif_receivebyte();
    }
    
    *statuslength = 4;

    return S_SUCCESS;
}

//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
u8 mblif_rdid(u8 *id, u16 *idlength)
{
    u8  sequenceresponse;
    u8  length;
    u8  i;

    mblif_sendbyte((u8)MBLIF_CMD_RDID);
    mblif_receivebyte();        //dummy

    mblif_sendbyte((u8)MBLIF_CMD_RDID_S1);
    sequenceresponse = mblif_receivebyte();
    if (sequenceresponse != MBLIF_CMD_RESPONSE_S1)
    {
        return S_UNMATCH;
    }

    mblif_sendbyte((u8)MBLIF_CMD_RDID_S2);
    sequenceresponse = mblif_receivebyte();
    if (sequenceresponse != MBLIF_CMD_RESPONSE_S2)
    {
        return S_UNMATCH;
    }

    //time to prepare data
    delays(10,'m');

    mblif_sendbyte((u8)MBLIF_CMD_RDID_S3);
    length = mblif_receivebyte();

    for(i=0;i<length;i++)
    {
        mblif_sendbyte(0xFF);
        id[i] = mblif_receivebyte();
    }
    
    *idlength = length;

    return S_SUCCESS;
}

//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
u8 mblif_debug1(u8 *id, u16 *idlength)
{
    u8  i;

    mblif_sendbyte((u8)MBLIF_CMD_DEBUG1);
    mblif_receivebyte();    //dummy

    for(i=0;i<16;i++)
    {
        mblif_sendbyte(0xFF);
        id[i] = mblif_receivebyte();
    }
    
    *idlength = 16;

    return S_SUCCESS;
}

//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
u8 mblif_writebuffer(u8 *buffer, u16 bufferlength)
{
    u8  sequenceresponse;
    u16 i;

    mblif_sendbyte((u8)MBLIF_CMD_WRITE_BUFFER);
    mblif_receivebyte();        //dummy

    mblif_sendbyte(MBLIF_CMD_WRITE_BUFFER_S1);
    sequenceresponse = mblif_receivebyte();
    if (sequenceresponse != MBLIF_CMD_RESPONSE_S1)
    {
        return S_UNMATCH;
    }
    
    mblif_sendbyte(bufferlength >> 8);          //TODOQ: wrong endian
    sequenceresponse = mblif_receivebyte();
    if (sequenceresponse != MBLIF_CMD_RESPONSE_S2)
    {
        return S_UNMATCH;
    }
    
    mblif_sendbyte(bufferlength & 0xFF);
    sequenceresponse = mblif_receivebyte();
    if (sequenceresponse != MBLIF_CMD_RESPONSE_S3)
    {
        return S_UNMATCH;
    }
    
    for(i=0;i<bufferlength;i++)
    {
        mblif_sendbyte(*buffer++);
        sequenceresponse = mblif_receivebyte();
        if (sequenceresponse != MBLIF_CMD_RESPONSE_S4)
        {
            return S_UNMATCH;
        }
    }
    return S_SUCCESS;
}

//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
u8 mblif_readbuffer(u8 *buffer, u16 *bufferlength)
{
    u16 i;
    u16 length;
    u8  sequenceresponse;
    u8  tmp;

    mblif_sendbyte((u8)MBLIF_CMD_READ_BUFFER);
    mblif_receivebyte();        //dummy

    mblif_sendbyte(MBLIF_CMD_READ_BUFFER_S1);
    sequenceresponse = mblif_receivebyte();
    if (sequenceresponse != MBLIF_CMD_RESPONSE_S1)
    {
        return S_UNMATCH;
    }
    
    mblif_sendbyte(MBLIF_CMD_READ_BUFFER_S2);
    length = (u16)mblif_receivebyte() << 8;
    
    mblif_sendbyte(MBLIF_CMD_READ_BUFFER_S3);
    length |= (u16)mblif_receivebyte();

    for(i=0;i<length;i++)
    {
        mblif_sendbyte(MBLIF_CMD_RESPONSE_S4);
        tmp = mblif_receivebyte();
        *buffer++ = tmp;
    }
    
    *bufferlength = length;
    
    return S_SUCCESS;
}

//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
u8 mblif_exec(u16 opcode, u8 *data, u8 datalength)
{
    u8  sequenceresponse;
    u16 i;

    mblif_sendbyte((u8)MBLIF_CMD_EXEC);
    mblif_receivebyte();        //dummy

    mblif_sendbyte(MBLIF_CMD_EXEC_S1);
    sequenceresponse = mblif_receivebyte();
    if (sequenceresponse != MBLIF_CMD_RESPONSE_S1)
    {
        return S_UNMATCH;
    }
    
    mblif_sendbyte(MBLIF_CMD_EXEC_S2);
    sequenceresponse = mblif_receivebyte();
    if (sequenceresponse != MBLIF_CMD_RESPONSE_S2)
    {
        return S_UNMATCH;
    }
    
    mblif_sendbyte(datalength + 2);
    sequenceresponse = mblif_receivebyte();
    if (sequenceresponse != MBLIF_CMD_RESPONSE_S3)
    {
        return S_UNMATCH;
    }
    
    mblif_sendbyte(opcode & 0xFF);
    sequenceresponse = mblif_receivebyte();
    if (sequenceresponse != MBLIF_CMD_RESPONSE_S4)
    {
        return S_UNMATCH;
    }
    
    mblif_sendbyte(opcode >> 8);
    sequenceresponse = mblif_receivebyte();
    if (sequenceresponse != MBLIF_CMD_RESPONSE_S4)
    {
        return S_UNMATCH;
    }
    
    for(i=0;i<datalength;i++)
    {
        mblif_sendbyte(*data++);
        sequenceresponse = mblif_receivebyte();
        if (sequenceresponse != MBLIF_CMD_RESPONSE_S4)
        {
            return S_UNMATCH;
        }
    }
    return S_SUCCESS;
}
