/**
  **************** (C) COPYRIGHT 2011 SCT Performance, LLC *********************
  * File Name          : flash.h
  * Device / System    : SCT products using the "CommonCode" platform
  * Author             : Quyen Leba
  *
  * Version            : 1 
  * Date               : 05/02/2011
  * Description        : 
  *                    : 
  *
  *
  * History            : 05/02/2011 M. Davis
  *                    :   Add SCT Copyright header,
  *
  ******************************************************************************
  */

#ifndef __FLASH_H
#define __FLASH_H

#include <arch/gentype.h>
#include <common/settings.h>

typedef enum
{
    CriticalArea,
    TuneArea,
    FleetArea,
    DatalogGeneralArea,
    OldSettingsArea,
}FlashSettingsArea;

u8 flash_load_setting(FlashSettingsArea flashsettingsarea,
                      u8 *settingsdata, u32 settingsdatalength);

//------------------------------------------------------------------------------
// Flash map
// - Failsafe Bootloader    32kB
// - Blowfish S Table       4kB
// - Main Bootloader        32kB
// - BootSettings           2kB
// - AppSettings            18kB
// - Application            256kB
//------------------------------------------------------------------------------

#define FLASH_START_ADDRESS                     ((u32)0x08000000)
#define FLASH_SECTOR_SIZE                       2048

//0x08000000
#define FLASH_FAILSAFE_BOOT_STARTING_SECTOR     0
#define FLASH_FAILSAFE_BOOT_ADDRESS             (FLASH_START_ADDRESS + FLASH_FAILSAFE_BOOT_STARTING_SECTOR*FLASH_SECTOR_SIZE)
#define FLASH_FAILSAFE_BOOT_SIZE                16*FLASH_SECTOR_SIZE

//0x08008000
#define FLASH_BF_S_STARTING_SECTOR              16
#define FLASH_BF_S_ADDRESS                      (FLASH_START_ADDRESS + FLASH_BF_S_STARTING_SECTOR*FLASH_SECTOR_SIZE)
#define FLASH_BF_S_SIZE                         2*FLASH_SECTOR_SIZE

//0x08009000
#define FLASH_MAIN_BOOT_STARTING_SECTOR         18
#define FLASH_MAIN_BOOT_ADDRESS                 (FLASH_START_ADDRESS + FLASH_MAIN_BOOT_STARTING_SECTOR*FLASH_SECTOR_SIZE)
#define FLASH_MAIN_BOOT_RAM_ADDRESS             (0x20008000)
#define FLASH_MAIN_BOOT_SIZE                    16*FLASH_SECTOR_SIZE

//0x08011000
#define FLASH_BOOTSETTINGS_STARTING_SECTOR      34
#define FLASH_BOOTSETTINGS_ADDRESS              (FLASH_START_ADDRESS + FLASH_BOOTSETTINGS_STARTING_SECTOR*FLASH_SECTOR_SIZE)
#define FLASH_BOOTSETTINGS_SIZE                 1*FLASH_SECTOR_SIZE

//0x08011800
#define FLASH_SETTINGS_STARTING_SECTOR          35
#define FLASH_SETTINGS_ADDRESS                  (FLASH_START_ADDRESS + FLASH_SETTINGS_STARTING_SECTOR*FLASH_SECTOR_SIZE)
#define FLASH_SETTINGS_SIZE                     9*FLASH_SECTOR_SIZE

//0x08016000
#define FLASH_APPLICATION_STARTING_SECTOR       44
#define FLASH_APPLICATION_ADDRESS               (FLASH_START_ADDRESS + FLASH_APPLICATION_STARTING_SECTOR*FLASH_SECTOR_SIZE)
#define FLASH_APPLICATION_SIZE_MAX              (255-44)*FLASH_SECTOR_SIZE

#define FLASH_VALID_SECTOR_MIN                  18
#define FLASH_VALID_SECTOR_MAX                  255
#define FLASH_VALID_ADDRESS_MIN                 FLASH_MAIN_BOOT_ADDRESS
#define FLASH_VALID_ADDRESS_MAX                 (FLASH_APPLICATION_ADDRESS + FLASH_APPLICATION_SIZE_MAX - 1)

#define FLASHSETTINGS_MAX_SETTINGS_SIZE         2048

u8 flash_erase_sector(u16 sector);
u8 flash_write_data(u32 starting_address,
                    u8 *data, u16 datalength, bool encrypted);
u8 flash_write_sector_at_offset(u16 sector, u16 offset,
                                u8 *data, u16 datalength, bool encrypted);
u8 flash_validate_content(u16 sector, u32 length, u32 crc32e);
u8 flash_load_bootsettings(u8 *settings, u32 length);
u8 flash_store_bootsettings(u8 *settings, u16 length);

#endif	//__FLASH_H
