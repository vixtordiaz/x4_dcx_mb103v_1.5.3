/**
  **************** (C) COPYRIGHT 2011 SCT Performance, LLC *********************
  * File Name          : mblexec.h
  * Device / System    : SCT products using the "CommonCode" platform
  * Author             : Quyen Leba
  *
  * Version            : 1 
  * Date               : 05/02/2011
  * Description        : 
  *                    : 
  *
  *
  * History            : 05/02/2011 M. Davis
  *                    :   Add SCT Copyright header,
  *
  ******************************************************************************
  */

#ifndef __MBLCMD_H
#define __MBLCMD_H

#include <arch/gentype.h>

//------------------------------------------------------------------------------
// Format of an execution packet: [2: opcode] [n-2:data]
//------------------------------------------------------------------------------

typedef enum
{
    //[2:setting opcode]
    MBLEXEC_OPCODE_GET_SETTINGS                         = 0x02,
    //[2:setting opcode][64:encrypted message block of settings]
    MBLEXEC_OPCODE_SET_SETTINGS                         = 0x03,
    //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    // Booloader
    //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    //[2:sector index]
    MBLEXEC_OPCODE_ERASE_SECTOR                         = 0x11,
    //[2:sector index]
    MBLEXEC_OPCODE_WRITE_SECTOR                         = 0x12,
    //[2:sector index][2:reserved][4:length in byte][4:crc32e]
    MBLEXEC_OPCODE_VALIDATE_FLASH_CONTENT               = 0x13,
    //[no data]
    MBLEXEC_OPCODE_RUN_FAILSAFE_BOOTLOADER              = 0x14,
    MBLEXEC_OPCODE_RUN_MAIN_BOOTLOADER                  = 0x15,
    MBLEXEC_OPCODE_RUN_APPLICATION                      = 0x16,
    //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    // Application
    //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    //[no data]
    MBLEXEC_OPCODE_PING_VEHICLE_COMM                    = 0x20,
    //[1:comm_type][1:comm_level][2:flags]
    MBLEXEC_OPCODE_SET_VEHICLE_COMM                     = 0x21,
    //[1:ain ch as defined in ADC_CHANNEL]
    MBLEXEC_OPCODE_GET_AIN                              = 0x22,
    //[4:MBLEXEC_HWCONFIG]
    MBLEXEC_OPCODE_SET_HWCONFIG                         = 0x23,
    //[no data]
    MBLEXEC_OPCODE_GET_HWCONFIG                         = 0x24,
    //[4:flags][]
    MBLEXEC_OPCODE_CONFIG_PERIODIC_MSG                  = 0x25,
    //
    MBLEXEC_OPCODE_FIRE_ONCE_PERIODIC_MSG               = 0x26,
    //
    MBLEXEC_OPCODE_SEND_SIMPLE_MSG                      = 0x27,
    //
    MBLEXEC_OPCODE_RECEIVE_SIMPLE_MSG                   = 0x28,
    //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    // Security
    //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    //[no data]
    MBLEXEC_OPCODE_REQUEST_SECURITY_SEED                = 0xC1,
    //[32:key]
    MBLEXEC_OPCODE_VALIDATE_SECURITY_KEY                = 0xC3,
}MBLEXEC_OPCODE;

typedef struct
{
    u8  enable_ext5v            : 1;    //rw
    u8  enable_feps             : 1;    //rw
    u8  data_avail              : 1;    //rc
    u8  enable_sci_ae_vpp       : 1;    //rw
    u8  enable_sci_at_vpp       : 1;    //rw
    u8  enable_sci_be_vpp       : 1;    //rw
    u8  enable_sci_bt_vpp       : 1;    //rw
    u8  reserved                : 1;
    u8  x[3];
}MBLEXEC_HWCONFIG;

typedef struct
{
    u8  newmsg                  : 1;    //w     a msg in write buffer available
    u8  reserved                : 7;
    u8  x[3];
}MBLEXEC_PERIODIC_MSG_CONFIG;

void mblexec_init();
u8 mblexec_unlock_security();
u8 mblexec_lock_security();
u8 mblexec_call(MBLEXEC_OPCODE opcode, u8 *opcodedata, u8 opcodedatalength,
                u8 *returndata, u16 *returndatalength);

#endif	//__MBLCMD_H
