/**
  **************** (C) COPYRIGHT 2011 SCT Performance, LLC *********************
  * File Name          : button.c
  * Device / System    : SCT products using the "CommonCode" platform
  * Author             : Quyen Leba
  *
  * Version            : 1 
  * Date               : 05/02/2011
  * Description        : 
  *                    : 
  *
  *
  * History            : 05/02/2011 M. Davis
  *                    :   Add SCT Copyright header,
  *
  ******************************************************************************
  */

#include "stm32f10x_rcc.h"
#include "stm32f10x_gpio.h"
#include "stm32f10x_exti.h"
#include "misc.h"
#include "system.h"
#include "button.h"

#define BUTTON_GPIO_PORT            GPIOB
#define BUTTON_GPIO_CLK             RCC_APB2Periph_GPIOB
#define BUTTON_GPIO_PORTSOURCE      GPIO_PortSourceGPIOB
#define BUTTON_GPIO_PIN             GPIO_Pin_0
#define BUTTON_GPIO_PINSOURCE       GPIO_PinSource0
#define BUTTON_EXTI_LINE            EXTI_Line0
#define BUTTON_EXTI_CHANNEL         EXTI0_IRQn

struct
{
    u8  pressed     :1;
    u8  triggered   :1;
}button_info;

//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
void button_init()
{
    GPIO_InitTypeDef GPIO_InitStructure;
    //Configure PB.0, as input pull up
    GPIO_InitStructure.GPIO_Pin = BUTTON_GPIO_PIN;
    GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
    GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IPU;
    GPIO_Init(BUTTON_GPIO_PORT, &GPIO_InitStructure);
    
    button_info.pressed = 0;
    button_info.triggered = 0;
}

//------------------------------------------------------------------------------
// Active LOW button
//------------------------------------------------------------------------------
u8 button_getState(void)
{
    return GPIO_ReadInputDataBit(BUTTON_GPIO_PORT, BUTTON_GPIO_PIN);
}

/*
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
void button_intr_init()
{
    GPIO_InitTypeDef GPIO_InitStructure;
    EXTI_InitTypeDef EXTI_InitStructure;
    NVIC_InitTypeDef NVIC_InitStructure;
    
    RCC_APB2PeriphClockCmd(BUTTON_GPIO_CLK | RCC_APB2Periph_AFIO, ENABLE);

    GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IPU;
    GPIO_InitStructure.GPIO_Pin = BUTTON_GPIO_PIN;
    GPIO_Init(BUTTON_GPIO_PORT, &GPIO_InitStructure);
    
    // Connect Button EXTI Line to Button GPIO Pin
    GPIO_EXTILineConfig(BUTTON_GPIO_PORTSOURCE, BUTTON_GPIO_PINSOURCE);
    
    // Configure Button EXTI line
    EXTI_InitStructure.EXTI_Line = BUTTON_EXTI_LINE;
    EXTI_InitStructure.EXTI_Mode = EXTI_Mode_Interrupt;
    
    EXTI_InitStructure.EXTI_Trigger = EXTI_Trigger_Rising_Falling;//EXTI_Trigger_Falling;
    EXTI_InitStructure.EXTI_LineCmd = ENABLE;
    EXTI_Init(&EXTI_InitStructure);
    
    // Enable and set Button EXTI Interrupt to the lowest priority
    NVIC_InitStructure.NVIC_IRQChannel = BUTTON_EXTI_CHANNEL;
    NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 0x0F;
    NVIC_InitStructure.NVIC_IRQChannelSubPriority = 0x0F;
    NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;

    NVIC_Init(&NVIC_InitStructure);
    
    button_info.pressed = 0;
    button_info.triggered = 0;
}

//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
void button_intr_handler()
{
    if(EXTI_GetITStatus(BUTTON_EXTI_LINE) != RESET)
    {
        if (button_getState())  //RELEASE
        {
            if (button_info.pressed)
            {
                button_info.triggered = 1;
            }
            button_info.pressed = 0;
        }
        else                    //PRESSED
        {
            button_info.triggered = 0;
            button_info.pressed = 1;
        }

        // Clear the Button EXTI line pending bit
        EXTI_ClearITPendingBit(BUTTON_EXTI_LINE);
    }
}
*/