/**
  **************** (C) COPYRIGHT 2011 SCT Performance, LLC *********************
  * File Name          : spi.c
  * Device / System    : SCT products using the "CommonCode" platform
  * Author             : Quyen Leba
  *
  * Version            : 1 
  * Date               : 05/02/2011
  * Description        : 
  *                    : 
  *
  *
  * History            : 05/02/2011 M. Davis
  *                    :   Add SCT Copyright header,
  *
  ******************************************************************************
  */

#include "stm32f10x_gpio.h"
#include "stm32f10x_rcc.h"
#include "stm32f10x_spi.h"
#include <common/statuscode.h>
#include "spi.h"

#define SPI_MASTER              SPI1
#define SPI_MASTER_CLK          RCC_APB2Periph_SPI1
#define SPI_MASTER_GPIO_PORT    GPIOA
#define SPI_MASTER_GPIO_CLK     RCC_APB2Periph_GPIOA
//#define SPI_MASTER_PIN_NSS      GPIO_Pin_
#define SPI_MASTER_PIN_SCK      GPIO_Pin_5
#define SPI_MASTER_PIN_MISO     GPIO_Pin_6
#define SPI_MASTER_PIN_MOSI     GPIO_Pin_7

//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
void spi_master_init()
{
    // Pins: SPI_MASTER_PIN_SCK | SPI_MASTER_PIN_MOSI
    // Speed: GPIO_Speed_50MHz
    // Mode: GPIO_Mode_AF_PP
    GPIOA->CRL = 0xB4B44444;

    // Pin: SPI_MASTER_PIN_MISO;
    // Mode: GPIO_Mode_AF_PP;
    GPIOA->CRL = 0xBBB44444;

    /*
    GPIO_InitStructure.GPIO_Pin = SPI_MASTER_PIN_NSS;
    GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
    GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AF_PP;
    GPIO_Init(SPI_MASTER_GPIO_PORT, &GPIO_InitStructure);
    */

#define SPI_Mode_Select      ((u16)0xF7FF)
#define CR1_SPE_Set          ((u16)0x0040)
#define CR2_SSOE_Set         ((u16)0x0004)

    /*
    SPI_InitStructure.SPI_Mode = SPI_Mode_Master;
    SPI_InitStructure.SPI_Direction = SPI_Direction_2Lines_FullDuplex;
    SPI_InitStructure.SPI_DataSize = SPI_DataSize_8b;
    SPI_InitStructure.SPI_CPOL = SPI_CPOL_Low;
    SPI_InitStructure.SPI_CPHA = SPI_CPHA_2Edge;
    SPI_InitStructure.SPI_NSS = SPI_NSS_Soft;
    SPI_InitStructure.SPI_BaudRatePrescaler = SPI_BaudRatePrescaler_4;
    SPI_InitStructure.SPI_FirstBit = SPI_FirstBit_MSB;
    SPI_InitStructure.SPI_CRCPolynomial = 7;
    SPI_Init(SPI_MASTER, &SPI_InitStructure);
    */
    SPI_MASTER->CR1 = 0x030D;
    // Activate the SPI mode (Reset I2SMOD bit in I2SCFGR register)
    SPI_MASTER->I2SCFGR &= SPI_Mode_Select;
    // Write to SPIx CRCPOLY
    SPI_MASTER->CRCPR = 7;

    // Enable NSS output
    //SPI_MASTER->CR2 |= CR2_SSOE_Set;

    // Enable SPI_MASTER
    SPI_MASTER->CR1 |= CR1_SPE_Set;
}

//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
u8 spi_master_send_byte(u8 databyte)
{
    volatile u32 i;

    for(i=0;i<30;i++);
    while((SPI_MASTER->SR & SPI_I2S_FLAG_TXE) == (u16)RESET);
    SPI_MASTER->DR = databyte;
    
    return S_SUCCESS;
}

//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
u8 spi_master_receive_byte()
{
    while((SPI_MASTER->SR & SPI_I2S_FLAG_RXNE) == (u16)RESET);
    return SPI_MASTER->DR;
}
