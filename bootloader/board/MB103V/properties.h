/**
  **************** (C) COPYRIGHT 2011 SCT Performance, LLC *********************
  * File Name          : properties.h
  * Device / System    : SCT products using the "CommonCode" platform
  * Author             : Quyen Leba
  *
  * Version            : 1 
  * Date               : 05/02/2011
  * Description        : 
  *                    : 
  *
  *
  * History            : 05/02/2011 M. Davis
  *                    :   Add SCT Copyright header,
  *
  ******************************************************************************
  */

#ifndef __PROPERTIES_H
#define __PROPERTIES_H

#include <arch/gentype.h>

void properties_init();
u8 properties_getinfo(u8 *info, u32 *info_length);

#endif  //__PROPERTIES_H
