/**
  **************** (C) COPYRIGHT 2011 SCT Performance, LLC *********************
  * File Name          : flash.c
  * Device / System    : SCT products using the "CommonCode" platform
  * Author             : Quyen Leba
  *
  * Version            : 1 
  * Date               : 05/02/2011
  * Description        : 
  *                    : 
  *
  *
  * History            : 05/02/2011 M. Davis
  *                    :   Add SCT Copyright header,
  *
  ******************************************************************************
  */

#include <string.h>
#include <stm32f10x_flash.h>
#include <board/genplatform.h>
#include <common/statuscode.h>
#include <common/crypto_blowfish.h>
#include <common/crc32.h>
#include "flash.h"

//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
#define FLASHSETTINGS_SECTOR_SIZE               FLASH_SECTOR_SIZE
#define FLASHSETTINGS_ADDR                      FLASH_SETTINGS_ADDRESS
#define FLASHSETTINGS_ADDR_S0                   FLASHSETTINGS_ADDR                                  //0x08011800
#define FLASHSETTINGS_ADDR_S1                   FLASHSETTINGS_ADDR_S0 + FLASHSETTINGS_SECTOR_SIZE   //0x08012000
#define FLASHSETTINGS_ADDR_S2                   FLASHSETTINGS_ADDR_S1 + FLASHSETTINGS_SECTOR_SIZE   //0x08012800
#define FLASHSETTINGS_ADDR_S3                   FLASHSETTINGS_ADDR_S2 + FLASHSETTINGS_SECTOR_SIZE   //0x08013000
#define FLASHSETTINGS_ADDR_S4                   FLASHSETTINGS_ADDR_S3 + FLASHSETTINGS_SECTOR_SIZE   //0x08013800
#define FLASHSETTINGS_ADDR_S5                   FLASHSETTINGS_ADDR_S4 + FLASHSETTINGS_SECTOR_SIZE   //0x08014000

#define FLASHSETTINGS_OLDSETTINGS_ADDR          (FLASHSETTINGS_ADDR_S0)
//SETTINGS_ADDR_S1: unused
#define BOOTLOADER_SIGNATURE_ADDR               (FLASHSETTINGS_ADDR_S2)
#define FLASHSETTINGS_CRITICAL_ADDR             (FLASHSETTINGS_ADDR_S3)
#define FLASHSETTINGS_TUNE_ADDR                 (FLASHSETTINGS_ADDR_S4)
#define FLASHSETTINGS_FLEET_ADDR  0 //TODOQ: critical
#define FLASHSETTINGS_DATALOGGENERAL_ADDR       (FLASHSETTINGS_ADDR_S5)

#define FLASH_KEY1                              ((u32)0x45670123)
#define FLASH_KEY2                              ((u32)0xCDEF89AB)

#define CR_PG_Set                               ((u32)0x00000001)
#define CR_PG_Reset                             ((u32)0x00001FFE)
#define CR_PER_Set                              ((u32)0x00000002)
#define CR_PER_Reset                            ((u32)0x00001FFD)
#define CR_STRT_Set                             ((u32)0x00000040)
#define CR_LOCK_Set                             ((u32)0x00000080)

#define EraseTimeout                            ((u32)0x000B0000)
#define ProgramTimeout                          ((u32)0x00002000)


#define FLASH_WaitForLastOperation(timeout)     \
    while(FLASH->SR &   \
          (FLASH_FLAG_BANK1_BSY | FLASH_FLAG_BANK1_PGERR |  \
           FLASH_FLAG_BANK1_WRPRTERR))  \
    if (timeout-- == 0) {status = S_TIMEOUT; break;}

//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
u8 flash_load_setting(FlashSettingsArea flashsettingsarea,
                      u8 *settingsdata, u32 settingsdatalength)
{
    //TODOQ: old structure settings need conversion
    switch(flashsettingsarea)
    {
    case CriticalArea:
        memcpy((char*)settingsdata,
               (char*)FLASHSETTINGS_CRITICAL_ADDR,settingsdatalength);
        break;
    case TuneArea:
        memcpy((char*)settingsdata,
               (char*)FLASHSETTINGS_TUNE_ADDR,settingsdatalength);
        break;
    case DatalogGeneralArea:
        memcpy((char*)settingsdata,
               (char*)FLASHSETTINGS_DATALOGGENERAL_ADDR,settingsdatalength);
        break;
    case OldSettingsArea:
        memcpy((char*)settingsdata,
               (char*)FLASHSETTINGS_OLDSETTINGS_ADDR,settingsdatalength);
        break;
    default:
        return S_INPUT;
    }

    return S_SUCCESS;
}

//------------------------------------------------------------------------------
// Erase a sector
// Input:   u16 sector
// Return:  u8  status
// Engineer: Quyen Leba
//------------------------------------------------------------------------------
u8 flash_erase_sector(u16 sector)
{
    u32 timeout;
    u8  status;

    if (sector >= FLASH_VALID_SECTOR_MIN && sector <= FLASH_VALID_SECTOR_MAX)
    {
        //unlock flash
        FLASH->KEYR = FLASH_KEY1;
        FLASH->KEYR = FLASH_KEY2;

        status = S_SUCCESS;
        timeout = EraseTimeout;
        FLASH_WaitForLastOperation(timeout);
        if (status == S_SUCCESS)
        {
            FLASH->CR|= CR_PER_Set;
            FLASH->AR = FLASH_START_ADDRESS + sector * FLASH_SECTOR_SIZE;
            FLASH->CR|= CR_STRT_Set;

            timeout = EraseTimeout;
            FLASH_WaitForLastOperation(timeout);

            FLASH->CR &= CR_PER_Reset;
        }

        //lock flash
        FLASH->CR |= CR_LOCK_Set;

        return status;
    }
    else
    {
        return S_INPUT;
    }
}

#define __IO    volatile

//------------------------------------------------------------------------------
// Write to a flash sector
// Inputs:  u16 sector
//          u16 offset
//          u8  *data (encrypted)
//          u16 datalength (must be multiple of 8)
//          bool encrypted (TRUE: data is encrypted)
// Return:  u8  status
// Engineer: Quyen Leba
//------------------------------------------------------------------------------
u8 flash_write_sector_at_offset(u16 sector, u16 offset,
                                u8 *data, u16 datalength, bool encrypted)
{
    u16 *dataptr;
    u32 address;
    u32 timeout;
    u8  status;

    if (offset+datalength > FLASH_SECTOR_SIZE)
    {
        return S_INPUT;
    }
    else if (sector >= FLASH_VALID_SECTOR_MIN && sector <= FLASH_VALID_SECTOR_MAX)
    {
        //unlock flash
        FLASH->KEYR = FLASH_KEY1;
        FLASH->KEYR = FLASH_KEY2;

        status = S_SUCCESS;
        timeout = ProgramTimeout;
        FLASH_WaitForLastOperation(timeout);
        if (status == S_SUCCESS)
        {
            if (encrypted)
            {
                crypto_blowfish_decryptblock_critical(data,datalength);
            }
            dataptr = (u16*)data;

            timeout = ProgramTimeout;
            status = S_SUCCESS;
            FLASH_WaitForLastOperation(timeout);
            if (status == S_SUCCESS)
            {
                FLASH->CR |= CR_PG_Set;
                for(address=FLASH_START_ADDRESS+sector*FLASH_SECTOR_SIZE + offset;
                    address<FLASH_START_ADDRESS+sector*FLASH_SECTOR_SIZE + offset + datalength;
                    address+=2)
                {
                    *(__IO u16*)address = *dataptr++;
                    timeout = ProgramTimeout;
                    FLASH_WaitForLastOperation(timeout);
                    if (status != S_SUCCESS)
                    {
                        break;
                    }
                }
                FLASH->CR &= CR_PG_Reset;
            }
        }

        //lock flash
        FLASH->CR |= CR_LOCK_Set;

        return status;
    }
    else
    {
        return S_INPUT;
    }
}

//------------------------------------------------------------------------------
// Validate content
// Inputs:  u16 sector (starting sector)
//          u32 length
//          u32 crc32e
// Return:  u8  status
// Engineer: Quyen Leba
//------------------------------------------------------------------------------
u8 flash_validate_content(u16 sector, u32 length, u32 crc32e)
{
    u32 calc_crc32e;

    if ((sector >= FLASH_VALID_SECTOR_MIN &&
         (sector + length/FLASH_SECTOR_SIZE) <= FLASH_VALID_SECTOR_MAX))
    {
        crc32e_reset();
        calc_crc32e = crc32e_calculateblock
            (0xFFFFFFFF,
             (u32*)(FLASH_START_ADDRESS+sector*FLASH_SECTOR_SIZE),length/4);

        if (calc_crc32e != crc32e)
        {
            return S_CRC32E;
        }
        else
        {
            return S_SUCCESS;
        }
    }
    else
    {
        return S_INPUT;
    }
}

//------------------------------------------------------------------------------
// Load bootsettings from flash
// Input:   u32 length
// Output:  u8  *settings
// Return:  u8  status
// Engineer: Quyen Leba
//------------------------------------------------------------------------------
u8 flash_load_bootsettings(u8 *settings, u32 length)
{
    if (length <= FLASH_BOOTSETTINGS_SIZE)
    {
        memcpy((char*)settings,
           (char*)FLASH_BOOTSETTINGS_ADDRESS,length);
        return S_SUCCESS;
    }
    return S_INPUT;
}

//------------------------------------------------------------------------------
// Store bootsettings to flash
// Inputs:  u8  *settings
//          u16 length
// Return:  u8  status
// Engineer: Quyen Leba
//------------------------------------------------------------------------------
u8 flash_store_bootsettings(u8 *settings, u16 length)
{
    u8  status;

    status = flash_erase_sector(FLASH_BOOTSETTINGS_STARTING_SECTOR);
    if (status == S_SUCCESS)
    {
        status = flash_write_sector_at_offset(FLASH_BOOTSETTINGS_STARTING_SECTOR,
                                              0,settings,length,FALSE);
    }
    return status;
}
