/**
  **************** (C) COPYRIGHT 2011 SCT Performance, LLC *********************
  * File Name          : power.h
  * Device / System    : SCT products using the "CommonCode" platform
  * Author             : Quyen Leba
  *
  * Version            : 1 
  * Date               : 05/02/2011
  * Description        : 
  *                    : 
  *
  *
  * History            : 05/02/2011 M. Davis
  *                    :   Add SCT Copyright header,
  *
  ******************************************************************************
  */

#ifndef __POWER_H
#define __POWER_H

#include <arch/gentype.h>

typedef enum
{
    Vpp_OFF         = 0,
    Vpp_FEPS        = 1,
    Vpp_SCI_AE_6    = 2,
    Vpp_SCI_BT_9    = 3,
    Vpp_SCI_BE_12   = 4,
    Vpp_SCI_AT_14   = 5,
}VppState;

void power_init();
u8 power_vpp_ctrl(VppState state);
#define power_setvpp(state)     power_vpp_ctrl(state)
void power_ext5v_ctrl(FunctionalState state);

void power_vj_init();
void power_vj_enable(bool boost);
void power_vj_disable();

#endif  //__POWER_H
