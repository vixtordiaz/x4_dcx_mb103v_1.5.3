/**
  **************** (C) COPYRIGHT 2011 SCT Performance, LLC *********************
  * File Name          : flash.h
  * Device / System    : SCT products using the "CommonCode" platform
  * Author             : Quyen Leba
  *
  * Version            : 1 
  * Date               : 05/02/2011
  * Description        : 
  *                    : 
  *
  *
  * History            : 05/02/2011 M. Davis
  *                    :   Add SCT Copyright header,
  *
  ******************************************************************************
  */

#ifndef __FLASH_H
#define __FLASH_H

#include <arch/gentype.h>
#include <common/settings.h>

typedef enum
{
    CriticalArea,
    TuneArea,
    FleetArea,
    DatalogGeneralArea,
    OldSettingsArea,
}FlashSettingsArea;

u8 flash_save_bootloader_signature(BootloaderType bootloadertype);
u8 flash_save_bootloader_signature(BootloaderType bootloadertype);
u8 flash_save_setting(FlashSettingsArea flashsettingsarea,
                      u8 *settingsdata, u32 settingsdatalength);
u8 flash_load_setting(FlashSettingsArea flashsettingsarea,
                      u8 *settingsdata, u32 settingsdatalength);
bool flash_isoldstructure_settings();
u8 flash_convert_to_mico();

#endif	//__FLASH_H
