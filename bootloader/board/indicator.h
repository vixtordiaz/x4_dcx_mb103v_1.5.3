/**
  **************** (C) COPYRIGHT 2011 SCT Performance, LLC *********************
  * File Name          : indicator.h
  * Device / System    : SCT products using the "CommonCode" platform
  * Author             : Quyen Leba
  *
  * Version            : 1 
  * Date               : 05/02/2011
  * Description        : 
  *                    : 
  *
  *
  * History            : 05/02/2011 M. Davis
  *                    :   Add SCT Copyright header,
  *
  ******************************************************************************
  */

#ifndef __INDICATOR_H
#define __INDICATOR_H

#include <arch/gentype.h>

typedef enum
{
    USB_SYNC,
    SETTINGS_BACKUP_FAILED,
    SETTINGS_CORRUPTED,
    ECM_UPLOADING,
    ECM_ERASED,
    ECM_DOWNLOADING,
    DATALOG_SCAN,
    DATALOG_START,
}Indicator;

u8 indicator_clear();
u8 indicator_set(Indicator indicator);

#endif	//__INDICATOR_H
