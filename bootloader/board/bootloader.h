/**
  **************** (C) COPYRIGHT 2011 SCT Performance, LLC *********************
  * File Name          : bootloader.h
  * Device / System    : SCT products using the "CommonCode" platform
  * Author             : Quyen Leba
  *
  * Version            : 1 
  * Date               : 05/02/2011
  * Description        : 
  *                    : 
  *
  *
  * History            : 05/02/2011 M. Davis
  *                    :   Add SCT Copyright header,
  *
  ******************************************************************************
  */

#ifndef __BOOTLOADER_H
#define __BOOTLOADER_H

#include <arch/gentype.h>
#include <board/genplatform.h>

typedef enum
{
    //MainBoard
    BootloaderMode_MB_FailSafe      = 0xA538,
    BootloaderMode_MB_Main          = 0x5593,
    BootloaderMode_MB_Application   = 0xAA72,
    //VehicleBoard
    BootloaderMode_VB_FailSafe      = 0xC8A7,
    BootloaderMode_VB_Main          = 0x3455,
    BootloaderMode_VB_Application   = 0x16AA,
}BootloaderMode;

typedef struct
{
    //as defined in FileCrypto
    //general header (24 bytes)
    u32 hcrc;
    u16 hversion;
    u16 ftype;
    u16 hsize;
    u8  fsubtype;
    u8  reserved0;
    u32 firmware_size;
    u32 firmware_crc32e;
    u32 reserved1;
    //specific header ()
    u32 firmware_version;
    u32 hardware;   //firmware_signature
    u32 device_market;
    u32 device_type;
    u32 firmware_flags;     //new
    u32 signature;          //new
    u8  serialnumber[16];
}FirmwareHeader;

typedef struct
{
    u32 firmware_flags;
    u32 firmware_version;
    u32 firmware_size;
    u32 firmware_crc32e;
    u32 bytecount;
    u32 bytecheck;
    u16 current_sector;
    u32 current_address;
    u32 current_fwsize;
    u32 current_version;
    u32 erase_sector_size;
    struct
    {
        u8  validated   : 1;
        u8  init        : 1;
        u8  reserved    : 5;
    }status;
}BootloaderInfo;

//FirmwareFlags
//[bit2..0]
#define FIRMWARE_FLAGS_APPLICATION                  (1<<0)
#define FIRMWARE_FLAGS_MAIN_BOOTLOADER              (2<<0)
//[bit3] - only applicable if version/crc32e are the same
#define FIRMWARE_FLAGS_FORCE_UPDATE                 (1<<3)
//[bit4] - Update With Failsafe Bootloader
#define FIRMWARE_FLAGS_UPDATE_WITH_FAILSAFE         (1<<4)
//[bit31..4]: reserved

#define BOOTLOADER_FAILSAFE_ADDRESS         (FLASH_FAILSAFE_BOOT_ADDRESS)
#define BOOTLOADER_MAIN_RAM_ADDRESS         (FLASH_MAIN_BOOT_RAM_ADDRESS)
#define BOOTLOADER_MAIN_FLASH_ADDRESS       (FLASH_MAIN_BOOT_ADDRESS)
#define BOOTLOADER_MAIN_SIZE                (FLASH_MAIN_BOOT_SIZE)
#define BOOTLOADER_APPLICATION_ADDRESS      (FLASH_APPLICATION_ADDRESS)
#define BOOTLOADER_APPLICATION_SIZE_MAX     (FLASH_APPLICATION_SIZE_MAX)

void bootloader_init();
void bootloader_jump_to_failsafe_bootloader();
void bootloader_jump_to_main_bootloader();
void bootloader_jump_to_application();
u8 bootloader_setboot(BootloaderMode bootmode);
u8 bootloader_setup_session(u8 *firmware_header, u16 *sector_size);
u8 bootloader_write_flash(u8 *data, u32 datalength);
u8 bootloader_validate();
u8 bootloader_clearengineeringsignature();
u8 bootloader_force_application_validation();
u8 bootloader_force_mainboot_validation();
u8 bootloader_updatefromfile_check();

#endif	//__BOOTLOADER_H
