/**
  **************** (C) COPYRIGHT 2011 SCT Performance, LLC *********************
  * File Name          : genplatform.h
  * Device / System    : SCT products using the "CommonCode" platform
  * Author             : Quyen Leba
  *
  * Version            : 1 
  * Date               : 05/02/2011
  * Description        : 
  *                    : 
  *
  *
  * History            : 05/02/2011 M. Davis
  *                    :   Add SCT Copyright header,
  *
  ******************************************************************************
  */

#ifndef __GENPLATFORM_H
#define __GENPLATFORM_H

//------------------------------------------------------------------------------
//------------------------------------------------------------------------------
#ifdef __MB103V_PLATFORM__
//------------------------------------------------------------------------------
//------------------------------------------------------------------------------

#define BOARDREV_REVB0                              1

#define USE_CRC32_HARDWARE                          1
#define USE_BT11_BLUETOOTH                          1
#define USE_RN41_BLUETOOTH                          0

#define USE_UARTTYPE_DATALINK                       1

#include "timer.h"
#include "MB103V/properties.h"
#include "MB103V/init.h"
#include "MB103V/spi.h"
#include "MB103V/gpio.h"
#include "MB103V/led.h"
#include "MB103V/flash.h"
#include "MB103V/crc32_hw.h"
#include "MB103V/watchdog.h"
#include "MB103V/mblexec.h"
#include "MB103V/mblif.h"
#include "usb_type.h"
#include "usb_core.h"
#include "usb_hwconfig.h"
#include "usb_lib.h"
#include "MB103V/USB/usb_hwconfig.h"
#include "MB103V/USB/usb_callback_helper.h"

#include <stdlib.h>
#define __malloc            malloc
#define __free              free

//------------------------------------------------------------------------------
//------------------------------------------------------------------------------
#elif __MB215V_PLATFORM__
//------------------------------------------------------------------------------
//------------------------------------------------------------------------------

#define BOARDREV_REVB0                              1

#define USE_CRC32_HARDWARE                          1
#define USE_BT11_BLUETOOTH                          1
#define USE_RN41_BLUETOOTH                          0

#define USE_UARTTYPE_DATALINK                       1

#include "timer.h"
#include "MB215V/properties.h"
#include "MB215V/init.h"
#include "MB215V/spi.h"
#include "MB215V/gpio.h"
#include "MB215V/led.h"
#include "MB215V/flash.h"
#include "MB215V/crc32_hw.h"
#include "MB215V/watchdog.h"
#include "MB215V/mblexec.h"
#include "MB215V/mblif.h"
#include "usb_conf.h"
#include "usbd_ioreq.h"
#include "MB215V/USB/usb_callback_helper.h"

#include <stdlib.h>
#define __malloc            malloc
#define __free              free

//------------------------------------------------------------------------------
//------------------------------------------------------------------------------
#else
//------------------------------------------------------------------------------
//------------------------------------------------------------------------------

#error "gentype.h: Invalid PLATFORM"

#endif

#endif	//__GENPLATFORM_H
