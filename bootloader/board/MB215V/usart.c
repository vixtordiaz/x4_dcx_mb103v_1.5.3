/**
  **************** (C) COPYRIGHT 2011 SCT Performance, LLC *********************
  * File Name          : usart.c
  * Device / System    : SCT products using the "CommonCode" platform
  * Author             : Quyen Leba
  *
  * Version            : 1 
  * Date               : 05/02/2011
  * Description        : 
  *                    : 
  *
  *
  * History            : 05/02/2011 M. Davis
  *                    :   Add SCT Copyright header,
  *
  ******************************************************************************
  */

#include "stm32f2xx_gpio.h"
#include "stm32f2xx_rcc.h"
#include "usart.h"

//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
void uartA_init(USARTBAUD baud)
{
    USART_InitTypeDef USART_InitStructure;
    NVIC_InitTypeDef NVIC_InitStructure;
    GPIO_InitTypeDef GPIO_InitStructure;

    //RCC_APB2PeriphClockCmd(RCC_APB2Periph_USART1, ENABLE);
    RCC->APB2ENR |= RCC_APB2Periph_USART1;

    // Connect USART pins to AF7
    GPIO_PinAFConfig(UARTA_PORT, UARTA_TX_PIN_SOURCE, UARTA_GPIO_AF);
    GPIO_PinAFConfig(UARTA_PORT, UARTA_RX_PIN_SOURCE, UARTA_GPIO_AF);
    GPIO_PinAFConfig(UARTA_PORT, UARTA_CTS_PIN_SOURCE, UARTA_GPIO_AF);
    GPIO_PinAFConfig(UARTA_PORT, UARTA_RTS_PIN_SOURCE, UARTA_GPIO_AF);

    // Configure USART Tx and Rx as alternate function push-pull
    GPIO_InitStructure.GPIO_Pin = UARTA_TX_PIN;
    GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AF;
    GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
    GPIO_InitStructure.GPIO_OType = GPIO_OType_PP;
    GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_UP;
    GPIO_Init(UARTA_PORT, &GPIO_InitStructure);

    GPIO_InitStructure.GPIO_Pin = UARTA_RTS_PIN;
    GPIO_Init(UARTA_PORT, &GPIO_InitStructure);

    GPIO_InitStructure.GPIO_Pin = UARTA_RX_PIN;
    //GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IN;
    GPIO_Init(UARTA_PORT, &GPIO_InitStructure);

    GPIO_InitStructure.GPIO_Pin = UARTA_CTS_PIN;
    GPIO_Init(UARTA_PORT, &GPIO_InitStructure);

    // Enable the USART OverSampling by 8
    //USART_OverSampling8Cmd(UARTA_NAME, ENABLE);  

    /* USART1 configured as follow:
    - BaudRate = 3750000 baud
    - Maximum BaudRate that can be achieved when using the Oversampling by 8
    is: (USART APB Clock / 8) 
    Example: 
    - (USART3 APB1 Clock / 8) = (30 MHz / 8) = 3750000 baud
    - (USART1 APB2 Clock / 8) = (60 MHz / 8) = 7500000 baud
    - Maximum BaudRate that can be achieved when using the Oversampling by 16
    is: (USART APB Clock / 16) 
    Example: (USART3 APB1 Clock / 16) = (30 MHz / 16) = 1875000 baud
    Example: (USART1 APB2 Clock / 16) = (60 MHz / 16) = 3750000 baud
    - Word Length = 8 Bits
    - one Stop Bit
    - No parity
    - Hardware flow control disabled (RTS and CTS signals)
    - Receive and transmit enabled
    */
    USART_InitStructure.USART_BaudRate = (u32)baud;
    USART_InitStructure.USART_WordLength = USART_WordLength_8b;
    USART_InitStructure.USART_StopBits = USART_StopBits_1;
    USART_InitStructure.USART_Parity = USART_Parity_No;
    USART_InitStructure.USART_HardwareFlowControl = USART_HardwareFlowControl_RTS_CTS;
    USART_InitStructure.USART_Mode = USART_Mode_Rx | USART_Mode_Tx;
    USART_Init(UARTA_NAME, &USART_InitStructure);

    // Enable the UARTA Interrupt
    NVIC_InitStructure.NVIC_IRQChannel = UARTA_IRQn;
    NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = UARTA_Priority;
    NVIC_InitStructure.NVIC_IRQChannelSubPriority = UARTA_SubPriority;
    NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;
    NVIC_Init(&NVIC_InitStructure);

    USART_ITConfig(UARTA_NAME, USART_IT_RXNE, ENABLE);

    //USART_Cmd(UARTA_NAME, ENABLE);
    UARTA_NAME->CR1 |= USART_CR1_UE;
}

//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
void uartA_changebaudrate(USARTBAUD baud)
{
//////    u32 tmpreg = 0x00, apbclock = 0x00;
//////    u32 integerdivider = 0x00;
//////    u32 fractionaldivider = 0x00;
//////    //  u32 usartxbase = 0;
//////    RCC_ClocksTypeDef RCC_ClocksStatus;
//////    
//////    /* DISABLE USART2 Receive and Transmit interrupts */
//////    //USART_ITConfig(USART2, USART_IT_RXNE, DISABLE);
//////    
//////    RCC_GetClocksFreq(&RCC_ClocksStatus);  
//////    
//////    apbclock = RCC_ClocksStatus.PCLK1_Frequency;
//////    
//////    /* Determine the integer part */
//////    integerdivider = ((0x19 * apbclock) / (0x04 * ((u32)baud)));
//////    tmpreg = (integerdivider / 0x64) << 0x04;
//////    
//////    /* Determine the fractional part */
//////    fractionaldivider = integerdivider - (0x64 * (tmpreg >> 0x04));
//////    tmpreg |= ((((fractionaldivider * 0x10) + 0x32) / 0x64)) & ((u8)0x0F);
//////    
//////    //  while(USART_GetFlagStatus(USART2, USART_FLAG_IDLE) != RESET);
//////    while(USART_GetFlagStatus(USART2, USART_FLAG_TC) == RESET);
//////    
//////    /* Write to USART BRR */
//////    USART2->BRR = (u16)tmpreg;
//////    
//////    //USART_ClearFlag(USART2, USART_FLAG_RXNE);
//////    //USART_ClearITPendingBit(USART2, USART_IT_RXNE);
//////    
//////    /* Enable USART2 Receive and Transmit interrupts */
//////    //USART_ITConfig(USART2, USART_IT_RXNE, ENABLE);
//////    
//////    /* Enable the USART2 */
//////    //USART_Cmd(USART2, ENABLE);
}

//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
void uartC_init(USARTBAUD baud)
{
    USART_InitTypeDef USART_InitStructure;
    GPIO_InitTypeDef GPIO_InitStructure;

    RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOD, ENABLE);
    RCC_APB1PeriphClockCmd(RCC_APB1Periph_USART3, ENABLE);

    // Connect USART pins to AF7
    GPIO_PinAFConfig(UARTC_PORT, UARTC_TX_PIN_SOURCE, UARTC_GPIO_AF);
    GPIO_PinAFConfig(UARTC_PORT, UARTC_RX_PIN_SOURCE, UARTC_GPIO_AF);

    // Configure USART Tx and Rx as alternate function push-pull
    GPIO_InitStructure.GPIO_Pin = UARTC_TX_PIN;
    GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AF;
    GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
    GPIO_InitStructure.GPIO_OType = GPIO_OType_PP;
    GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_UP;
    GPIO_Init(UARTC_PORT, &GPIO_InitStructure);

    GPIO_InitStructure.GPIO_Pin = UARTC_RX_PIN;
    //GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IN;
    GPIO_Init(UARTC_PORT, &GPIO_InitStructure);

    // Enable the USART OverSampling by 8
    //USART_OverSampling8Cmd(UARTC_NAME, ENABLE);  

    /* USART3 configured as follow:
    - BaudRate = 3750000 baud
    - Maximum BaudRate that can be achieved when using the Oversampling by 8
    is: (USART APB Clock / 8) 
    Example: 
    - (USART3 APB1 Clock / 8) = (30 MHz / 8) = 3750000 baud
    - (USART1 APB2 Clock / 8) = (60 MHz / 8) = 7500000 baud
    - Maximum BaudRate that can be achieved when using the Oversampling by 16
    is: (USART APB Clock / 16) 
    Example: (USART3 APB1 Clock / 16) = (30 MHz / 16) = 1875000 baud
    Example: (USART1 APB2 Clock / 16) = (60 MHz / 16) = 3750000 baud
    - Word Length = 8 Bits
    - one Stop Bit
    - No parity
    - Hardware flow control disabled (RTS and CTS signals)
    - Receive and transmit enabled
    */
    USART_DeInit(UARTC_NAME);
    USART_InitStructure.USART_BaudRate = (u32)baud;
    USART_InitStructure.USART_WordLength = USART_WordLength_8b;
    USART_InitStructure.USART_StopBits = USART_StopBits_1;
    USART_InitStructure.USART_Parity = USART_Parity_No;
    USART_InitStructure.USART_HardwareFlowControl = USART_HardwareFlowControl_None;//USART_HardwareFlowControl_RTS_CTS;
    USART_InitStructure.USART_Mode = USART_Mode_Rx | USART_Mode_Tx;
    USART_Init(UARTC_NAME, &USART_InitStructure);

    USART_Cmd(UARTC_NAME, ENABLE);
}
