/**
  **************** (C) COPYRIGHT 2011 SCT Performance, LLC *********************
  * File Name          : usb_callback_helper.c
  * Device / System    : SCT products using the "CommonCode" platform
  * Author             : Quyen Leba
  *
  * Version            : 1 
  * Date               : 05/02/2011
  * Description        : 
  *                    : 
  *
  *
  * History            : 05/02/2011 M. Davis
  *                    :   Add SCT Copyright header,
  *
  ******************************************************************************
  */

#include "usb_conf.h"
#include "usbd_ioreq.h"
#include <common/usb_callback.h>
#include "usb_callback_helper.h"

extern u8  usb_databuffer[4096];
extern u32 usb_databufferindex;
extern u32 usb_databufferlength;

//------------------------------------------------------------------------------
// Send data to HOST
// Inputs:  u8  *buffer
//          u32 bufferlength (must be less than BULK_MAX_PACKET_SIZE)
// Engineer: Quyen Leba
//------------------------------------------------------------------------------
void usb_send_data_to_host(u8 *buffer, u32 bufferlength)
{
    DCD_EP_Tx(usb_callback_info.pdev, SCTD_IN_EP, buffer, bufferlength);
}

//------------------------------------------------------------------------------
// A trigger to send usb_databuffer to HOST
// Engineer: Quyen Leba
//------------------------------------------------------------------------------
void usb_trigger_send_databuffer_to_host()
{
    if (usb_databufferlength > BULK_MAX_PACKET_SIZE)
    {
        SCTUSB_SetFlowControl(CB_FC_DATA_IN);
        usb_databufferindex += BULK_MAX_PACKET_SIZE;
        SCTUSB_StatusResidue -= BULK_MAX_PACKET_SIZE;
//        USB_SIL_Write(EP1_IN,usb_databuffer,BULK_MAX_PACKET_SIZE);
        DCD_EP_Tx(usb_callback_info.pdev, SCTD_IN_EP, usb_databuffer, BULK_MAX_PACKET_SIZE);
    }
    else
    {
        SCTUSB_SetFlowControl(CB_FC_DATA_IN_LAST);
        usb_databufferindex += usb_databufferlength;
        SCTUSB_StatusResidue -= usb_databufferlength;
//        USB_SIL_Write(EP1_IN,usb_databuffer,usb_databufferlength);
        DCD_EP_Tx(usb_callback_info.pdev, SCTD_IN_EP, usb_databuffer, usb_databufferlength);
    }
//    SetEPTxStatus(ENDP1, EP_TX_VALID);
}

//------------------------------------------------------------------------------
// Set/Send status to HOST
// Inputs:  void *pdev
//          u8  status
//          bool send
// Engineer: Quyen Leba
//------------------------------------------------------------------------------
void usb_set_csw(void *pdev, u16 status, bool send)
{
    SCTUSB_SetStatusID(CB_CSW_ID);
    usb_callback_info.csw.status = status;

    SCTUSB_SetFlowControl(CB_FC_ERROR);
    if (send)
    {
        SCTUSB_SetFlowControl(CB_FC_STATUS_SEND);
        //SetEPTxStatus(ENDP1, EP_TX_VALID);
        DCD_EP_Tx(pdev,SCTD_IN_EP,(u8 *)&usb_callback_info.csw,CB_CSW_LENGTH);
    }
    // Prapare EP to Receive next Cmd
    DCD_EP_PrepareRx(pdev,SCTD_OUT_EP,(u8*)usb_callback_info.bulkbuffer,CB_CSW_LENGTH);
}
