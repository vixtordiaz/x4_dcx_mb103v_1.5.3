/**
  **************** (C) COPYRIGHT 2011 SCT Performance, LLC *********************
  * File Name          : usb_callback_helper.h
  * Device / System    : SCT products using the "CommonCode" platform
  * Author             : Quyen Leba
  *
  * Version            : 1 
  * Date               : 05/02/2011
  * Description        : 
  *                    : 
  *
  *
  * History            : 05/02/2011 M. Davis
  *                    :   Add SCT Copyright header,
  *
  ******************************************************************************
  */

#ifndef __USB_CALLBACK_HELPER_H
#define __USB_CALLBACK_HELPER_H

#include <arch/gentype.h>
#include "usb_core.h"

#define BULK_MAX_PACKET_SIZE            512

#define SetReadyEPOUT_DataPending()     DCD_EP_PrepareRx(pdev,SCTD_OUT_EP,(u8*)usb_callback_info.bulkbuffer,BULK_MAX_PACKET_SIZE)
#define SetReadyEPOUT_NoDataPending()   DCD_EP_PrepareRx(pdev,SCTD_OUT_EP,(u8*)usb_callback_info.bulkbuffer,CB_CSW_LENGTH)

#define SetReadyEPIN()                  //do nothing

#define SetStallEPIN()                  DCD_EP_Stall(pdev,SCTD_IN_EP)
#define SetStallEPOUT()                 DCD_EP_Stall(pdev,SCTD_OUT_EP)

#define GetEPOUTData()                  {usb_callback_info.bulkbufferlength = USBD_GetRxCount(pdev,SCTD_OUT_EP);}

#define SendDataEPIN(dev,data,len)      DCD_EP_Tx(dev,SCTD_IN_EP,data,len)

void usb_send_data_to_host(u8 *buffer, u32 bufferlength);
void usb_trigger_send_databuffer_to_host();
void usb_set_csw(void *pdev, u16 status, bool send);

#endif	//__USB_CALLBACK_HELPER_H
