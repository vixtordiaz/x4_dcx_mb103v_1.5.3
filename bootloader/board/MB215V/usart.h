/**
  **************** (C) COPYRIGHT 2011 SCT Performance, LLC *********************
  * File Name          : usart.h
  * Device / System    : SCT products using the "CommonCode" platform
  * Author             : Quyen Leba
  *
  * Version            : 1 
  * Date               : 05/02/2011
  * Description        : 
  *                    : 
  *
  *
  * History            : 05/02/2011 M. Davis
  *                    :   Add SCT Copyright header,
  *
  ******************************************************************************
  */

#ifndef __USART_H
#define __USART_H

#include "stm32f2xx_usart.h"

typedef enum
{
    BAUD9600    = 9600,
    BAUD115200  = 115200,
    BAUD230400  = 230400,
    BAUD460800  = 460800,
    BAUD921600  = 921600,
}USARTBAUD;

#define UARTA_NAME              USART1
#define UARTA_PORT              GPIOA
#define UARTA_IRQn              USART1_IRQn
#define UARTA_Priority          1
#define UARTA_SubPriority       0
#define UARTA_GPIO_AF           GPIO_AF_USART1
#define UARTA_TX_PIN            GPIO_Pin_9
#define UARTA_RX_PIN            GPIO_Pin_10
#define UARTA_CTS_PIN           GPIO_Pin_11
#define UARTA_RTS_PIN           GPIO_Pin_12
#define UARTA_TX_PIN_SOURCE     GPIO_PinSource9
#define UARTA_RX_PIN_SOURCE     GPIO_PinSource10
#define UARTA_CTS_PIN_SOURCE    GPIO_PinSource11
#define UARTA_RTS_PIN_SOURCE    GPIO_PinSource12

void uartA_init(USARTBAUD baud);
void uartA_changebaudrate(USARTBAUD baud);
#define uartA_tx(databyte)      USART_SendData(UARTA_NAME, databyte);    \
    while(USART_GetFlagStatus(UARTA_NAME, USART_FLAG_TXE) == RESET);
#define uartA_rx()              USART_ReceiveData(UARTA_NAME)
#define uartA_getitstatus()     USART_GetITStatus(UARTA_NAME, USART_IT_RXNE)


#define UARTC_NAME              USART3
#define UARTC_PORT              GPIOD
#define UARTC_GPIO_AF           GPIO_AF_USART3
#define UARTC_TX_PIN            GPIO_Pin_8
#define UARTC_RX_PIN            GPIO_Pin_9
#define UARTC_TX_PIN_SOURCE     GPIO_PinSource8
#define UARTC_RX_PIN_SOURCE     GPIO_PinSource9

void uartC_init(USARTBAUD baud);

#endif	//__USART_H
