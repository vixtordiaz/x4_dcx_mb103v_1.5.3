/**
  **************** (C) COPYRIGHT 2011 SCT Performance, LLC *********************
  * File Name          : rtc.c
  * Device / System    : SCT products using the "CommonCode" platform
  * Author             : Quyen Leba
  *
  * Version            : 1 
  * Date               : 05/02/2011
  * Description        : 
  *                    : 
  *
  *
  * History            : 05/02/2011 M. Davis
  *                    :   Add SCT Copyright header,
  *
  ******************************************************************************
  */

#include <stm32f10x_rcc.h>
#include <stm32f10x_pwr.h>
#include <stm32f10x_bkp.h>
#include <stm32f10x_rtc.h>
#include <board/rtc.h>

#define RTC_PRESCALE_VAULE          (125-1)

//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
void rtc_init()
{
    // Enable PWR and BKP clocks
    RCC_APB1PeriphClockCmd(RCC_APB1Periph_PWR | RCC_APB1Periph_BKP, ENABLE);
    // Allow access to BKP Domain
    PWR_BackupAccessCmd(ENABLE);
    // Reset Backup Domain
    BKP_DeInit();

    // Select HSE_Div128 as RTC Clock Source
    RCC_RTCCLKConfig(RCC_RTCCLKSource_HSE_Div128);
    // Enable RTC Clock
    RCC_RTCCLKCmd(ENABLE);
    
    // Wait for RTC registers synchronization
    RTC_WaitForSynchro();
    // Wait until last write operation on RTC registers has finished
    RTC_WaitForLastTask();

    //HSE = 16MHz --> (16MHz / 128)/125 = 1ms tick
    RTC_SetPrescaler(RTC_PRESCALE_VAULE);

    // Wait until last write operation on RTC registers has finished
    RTC_WaitForLastTask();
}

//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
void rtc_reset()
{
    // Wait until last write operation on RTC registers has finished
    RTC_WaitForLastTask();
    RTC_SetCounter(0x0);
    RTC_WaitForLastTask();
}

//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
u32 rtc_getvalue()
{
    return RTC_GetCounter();
}
