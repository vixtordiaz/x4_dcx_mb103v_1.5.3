/**
  **************** (C) COPYRIGHT 2011 SCT Performance, LLC *********************
  * File Name          : peripherals.c
  * Device / System    : SCT products using the "CommonCode" platform
  * Author             : Quyen Leba
  *
  * Version            : 1 
  * Date               : 05/02/2011
  * Description        : 
  *                    : 
  *
  *
  * History            : 05/02/2011 M. Davis
  *                    :   Add SCT Copyright header,
  *
  ******************************************************************************
  */

#include <stm32f2xx_rcc.h>
#include <board/genplatform.h>
#include <board/power.h>
#include <board/delays.h>
#include <board/timer.h>
#include <board/rtc.h>
#include <board/bootloader.h>
#include <board/peripherals.h>
#include <common/bootsettings.h>
#include <common/settings.h>
#include <common/cmdif.h>
#include "gpio.h"
#include "led.h"
#include "spi.h"
#include "usart.h"

//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
extern void USB_Disconnect_Config(void);    
extern void Set_USBClock(void);
extern void USB_Interrupts_Config(void);
extern void USB_Init(void);

//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
void peripherals_init()
{
    //Enable GPIOA, GPIOB, GPIOC, GPIOD, GPIOE, GPIOF, GPIOG clocks
    //Enable AFIO and SPI1 clocks
    RCC->AHB1ENR |= (RCC_AHB1Periph_GPIOA | RCC_AHB1Periph_GPIOB |
                     RCC_AHB1Periph_GPIOC | RCC_AHB1Periph_GPIOD |
                     RCC_AHB1Periph_GPIOE | RCC_AHB1Periph_GPIOF |
                     RCC_AHB1Periph_GPIOG | RCC_AHB1Periph_GPIOH |
                     RCC_AHB1Periph_GPIOI);
    //Enable TIM2 & TIM4 clock
    RCC->APB1ENR |= (RCC_APB1Periph_TIM2 | RCC_APB1Periph_TIM4);
    //Enable CRC clock
    RCC->AHB1ENR |= RCC_AHB1Periph_CRC;

    //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    // Configure a SysTick Base time to 1 us.
    //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    SysTick_Config(SystemCoreClock/1000);   //1ms

    gpio_init();

    timeout_init();
    board_timeout_init();
    spi_master_init();

    led_init();
    button_init();

    uartA_init(BAUD115200);

    bootsettings_load();
    settings_load();
    bootloader_init();
    properties_init();

    USB_Init();
    led_enter_bootloader();
    
    cmdif_init();
    
    gpio_config_Bluetooth_Force115KBaud(TRUE);
    bluetooth_init(BT_115200_BAUD); //TODO: for now of iTSX
}
