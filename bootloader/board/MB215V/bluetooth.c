/**
  **************** (C) COPYRIGHT 2011 SCT Performance, LLC *********************
  * File Name          : bluetooth.c
  * Device / System    : SCT products using the "CommonCode" platform
  * Author             : Quyen Leba
  *
  * Version            : 1 
  * Date               : 05/02/2011
  * Description        : 
  *                    : 
  *
  *
  * History            : 05/02/2011 M. Davis
  *                    :   Add SCT Copyright header,
  *
  ******************************************************************************
  */

#include <stdio.h>
#include <string.h>
//#include <stm32f10x_gpio.h>
//#include <stm32f10x_rcc.h>
#include <common/statuscode.h>
#include <common/crypto_blowfish.h>
#include <common/crypto_messageblock.h>
#include <common/crc32.h>
#include <board/debug.h>
#include "usart.h"
#include "bluetooth.h"
#include "led.h"

// redirect which usart port to run bluetooth module
#define bluetooth_hal(func)         uartA_##func

#define RAWSPP_SEND_DATA_SIZE       256


typedef enum
{
    RAWSPP_READY,
    RAWSPP_WAITING,
}RawSppTransferStatus;

typedef struct
{
    u16 len;
    u32 databytesent;
    //u8  buffer[340];
    u8  buffer[(2048+16)];
    bool filexfermode;  //FileTransferMode
    RawSppTransferStatus xfer_status;   //SPP_Data_Status
}rawspp_xfer_info;

rawspp_xfer_info rawsppxferinfo;             //from cmdif_func_filexfer.c

//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
SbDeviceInfoType    SbDevInfo;          //local device context information
SBEvent_T           SbEvent;            //Current result of Simply Blue response
BtDeviceInfo        DeviceInfoList[BLUETOOTH_MAX_DEVICE_COUNT];
SBLinkDataStatus    SbLinkDataStatus;   //SPP_Link_Status
u32 DevPtr = 0;
BLUETOOTH_INFO bluetooth_info;

//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
USARTBAUD bluetooth_translatebaud(SbBaudrate btbaud);
void bluetooth_uart_tx(u8 *data, u16 length);
void SbProcessFrame();
u8 SbProcessFrame_SimpleWrapper();

//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
SBStatus_T SbApiInit()
{
    /* Create a mailbox for inter-task communication */
	SbDevInfo.SbCmdStatus = SBSTATUS_OK;
    
    SbDevInfo.PortsToOpen = 0;
    SbDevInfo.SbPacketState = SBPACKET_SPW_START;
    SbDevInfo.frame_avail = FALSE;

    SbEvent.wPayloadOffset = 0;

    rawsppxferinfo.xfer_status = RAWSPP_WAITING;
    SbLinkDataStatus = RELEASED;
    
    bluetooth_info.use_encryption = 1;

    return SBSTATUS_OK;
}

//------------------------------------------------------------------------------
// Initialize Bluetooth
// Input:   SbBaudrate btbaud (a preferred baudrate)
// Return:  u8  status
// Engineer: Quyen Leba
//------------------------------------------------------------------------------
u8 bluetooth_init(SbBaudrate btbaud)
{
//    SbBaudrate detected_btbaud;
//    SBStatus_T btstatus;
    
    SbApiInit();

    /* !?! for now -Q- //TODO
    //detect the current bluetooth module baudrate
    btstatus = bluetooth_autodetectmodulebaud(btbaud,&detected_btbaud);
    if (btstatus == SBSTATUS_OK)
    {
        if (detected_btbaud != btbaud)
        {
            //config the module to the preferred baudrate
            btstatus = SbChangeNvsUartSpeed(btbaud);
            if (btstatus == SBSTATUS_OK)
            {
                //double check
                btstatus = bluetooth_autodetectmodulebaud(btbaud,
                                                          &detected_btbaud);
                if (detected_btbaud != btbaud)
                {
                    btstatus = SBSTATUS_ERROR;
                }
            }
        }
    }
    
    if (btstatus != SBSTATUS_OK)
    {
        return S_FAIL;
    }
    */
    return S_SUCCESS;
}

//------------------------------------------------------------------------------
// Auto detect bluetooth module baudrate
// Input:   SbBaudrate  preferred_btbaud
// Output:  SbBaudrate  *detected_btbaud
// Return:  SBStatus_T  btstatus
// Engineer: Quyen Leba
//------------------------------------------------------------------------------
//SBStatus_T bluetooth_autodetectmodulebaud(SbBaudrate preferred_btbaud,
//                                          SbBaudrate *detected_btbaud)
//{
//    SbBaudrate baudlist[3] = {BT_115200_BAUD,BT_9600_BAUD,BT_230400_BAUD};
//    BtNameType Name;
//    u8  MaxNameLength;
//    u32 i;
//    SBStatus_T btstatus;
//    
//    bluetooth_hal(init)(bluetooth_translatebaud(preferred_btbaud));
//    btstatus = SbNvsReadDeviceName(Name,&MaxNameLength);
//    if (btstatus == SBSTATUS_OK)
//    {
//        *detected_btbaud = preferred_btbaud;
//        return SBSTATUS_OK;
//    }
//    
//    for(i=0;i<3;i++)
//    {
//        if (baudlist[i] != preferred_btbaud)
//        {
//            bluetooth_changebaudrate(baudlist[i]);
//            btstatus = SbNvsReadDeviceName(Name,&MaxNameLength);
//            if (btstatus == SBSTATUS_OK)
//            {
//                *detected_btbaud = baudlist[i];
//                break;
//            }
//        }
//    }
//    return btstatus;
//}

//------------------------------------------------------------------------------
// Translate to actual baud rate
// Return:  u32 baudrate
//------------------------------------------------------------------------------
USARTBAUD bluetooth_translatebaud(SbBaudrate btbaud)
{
    if ((u32)btbaud > (u32)sizeof(SbTranslatedBaudrate))
    {
        return (USARTBAUD)0;
    }
    else
    {
        return (USARTBAUD)SbTranslatedBaudrate[(u32)btbaud];
    }
}

//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
void bluetooth_changebaudrate(SbBaudrate btbaud)
{
    bluetooth_hal(changebaudrate)(bluetooth_translatebaud(btbaud));
}

//------------------------------------------------------------------------------
// Changes the Default Bluetooth Module Baudrate. This requires a module RESET.
// Input:   BluetoothBaudrate btbaud
// Return   u8  status
// Engineer: Patrick Downs
//------------------------------------------------------------------------------
u8 bluetooth_force115kBaud_btmodule(void)
{
    u8 status = S_SUCCESS;

    gpio_config_Bluetooth_Force115KBaud(TRUE);
    gpio_reset_Bluetooth_module();
    bluetooth_changebaudrate(BT_115200_BAUD);
    return status;
}

//------------------------------------------------------------------------------
// Input:   u8 *data
//          u16 length
//------------------------------------------------------------------------------
void bluetooth_uart_tx(u8 *data, u16 length)
{
    u16 i = 0;
    while(i < length)
    {
        bluetooth_hal(tx)(data[i++]);
    }
}

u8  bbb[64];
u8  bbbl = 0;
bool bluetooth_in_debug = FALSE;

//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
void bluetooth_uart_rx_irq_handler()
{
    u8  in;
    u16 DataLen;
    
    if(bluetooth_hal(getitstatus)() != RESET)
    {
        if (bluetooth_in_debug)
        {
            in = bluetooth_hal(rx)();
            bluetooth_hal(tx)(in);
            return;
        }

        in = bluetooth_hal(rx)();
        if (bbbl >= sizeof(bbb))
        {
            bbbl = 0;
        }
        bbb[bbbl++] = in;

        //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
        //[START][OK][CmdCode][2:payload_len][4:crc32e][n:payload][STOP]
        //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
        switch(SbDevInfo.SbPacketState)
        {
        case SBPACKET_SPW_START:
            if (in == STX_SPW)
            {
                SbDevInfo.SbPacketState = SBPACKET_SPW_STATUS;
            }
            else 
            {
                SbDevInfo.SbPacketState = SBPACKET_SPW_ERR;
            }
            break;
        case SBPACKET_SPW_STATUS:
            SbEvent.bStatus = in;
            SbDevInfo.SbPacketState = SBPACKET_SPW_CMDCODE;
            break;
        case SBPACKET_SPW_CMDCODE:
            SbEvent.bCmdCode = in;
            SbDevInfo.SbPacketState = SBPACKET_SPW_PAYLOADLEN0;
            break;
        case SBPACKET_SPW_PAYLOADLEN0:
            SbEvent.wPayloadOffset = 0;
            SbEvent.wPayloadlen = in;
            SbDevInfo.SbPacketState = SBPACKET_SPW_PAYLOADLEN1;
            break;
        case SBPACKET_SPW_PAYLOADLEN1:
            DataLen = in;
            SbEvent.wPayloadlen |= DataLen << 8; 
            SbDevInfo.SbPacketState = SBPACKET_SPW_CRC32E0;
            break;
        case SBPACKET_SPW_CRC32E0:
            SbEvent.dwCRC32E = in;
            SbDevInfo.SbPacketState = SBPACKET_SPW_CRC32E1;
            break;
        case SBPACKET_SPW_CRC32E1:
            SbEvent.dwCRC32E |= ((u32)in) << 8;
            SbDevInfo.SbPacketState = SBPACKET_SPW_CRC32E2;
            break;
        case SBPACKET_SPW_CRC32E2:
            SbEvent.dwCRC32E |= ((u32)in) << 16;
            SbDevInfo.SbPacketState = SBPACKET_SPW_CRC32E3;
            break;
        case SBPACKET_SPW_CRC32E3:
            SbEvent.dwCRC32E |= ((u32)in) << 24;
            if (SbEvent.wPayloadlen == 0)
            {
                SbDevInfo.SbPacketState = SBPACKET_SPW_END;
            }
            else if (SbEvent.wPayloadlen > (2048+16))    //TODOQ: more needed for scrable
            {
                SbDevInfo.SbPacketState = SBPACKET_SPW_ERR;
            }
            else
            {
                SbDevInfo.SbPacketState = SBPACKET_SPW_DATA;
            }
            break;
        case SBPACKET_SPW_DATA:
            SbEvent.pPayload[SbEvent.wPayloadOffset] = in;
            if(SbEvent.wPayloadOffset < (SbEvent.wPayloadlen-1))
            {   
                SbEvent.wPayloadOffset++;
            }
            else
            {
                SbDevInfo.SbPacketState = SBPACKET_SPW_END;
                SbEvent.wPayloadOffset = 0;
            }

            if(SbEvent.wPayloadOffset > (2048+16))
            {
                SbEvent.wPayloadOffset = 0;
                SbDevInfo.SbPacketState = SBPACKET_SPW_ERR;
            }
            break;
        case SBPACKET_SPW_END:
            SbEvent.bEnd = in;
            if (SbEvent.bEnd == ETX_SPW)
            {
                SbDevInfo.frame_avail = TRUE;
                SbDevInfo.SbPacketState = SBPACKET_SPW_START;
//                if (SbProcessFrame_SimpleWrapper() == S_SUCCESS)
//                {
//                    SbDevInfo.SbPacketState = SBPACKET_SPW_START;
//                }
//                else
//                {
//                    SbDevInfo.SbPacketState = SBPACKET_SPW_ERR;
//                }
            }
            else
            {
                SbDevInfo.SbPacketState = SBPACKET_SPW_ERR;
            }
            break;
        default:
            in = bluetooth_hal(rx)();
            SbDevInfo.SbPacketState = SBPACKET_SPW_ERR;
            break;
        }

        if(SbDevInfo.SbPacketState == SBPACKET_SPW_ERR)
        {
            //u8 outputbuf[5];

            SbDevInfo.SbPacketState = SBPACKET_SPW_START;
            //sprintf((char*)outputbuf, "%X", in);
            //debug("PE!");
            //debug(outputbuf);
            return;
        }
    }
}

//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
u8 SbProcessFrame_SimpleWrapper()
{
    u8  buffer[8];
    u32 paddingcount;
    u32 crc32e;

    SbDevInfo.SbCmdStatus = SBSTATUS_OK;

    //[START][OK][CmdCode][2:payload_len][4:crc32e][n:payload][STOP]
    memset(buffer,0xFF,8);
    buffer[0] = SbEvent.bStatus;
    buffer[1] = SbEvent.bCmdCode;
    buffer[2] = SbEvent.wPayloadlen & 0xFF;
    buffer[3] = SbEvent.wPayloadlen >> 8;
    crc32e_reset();
    crc32e = crc32e_calculateblock(0xFFFFFFFF,(u32*)buffer,8/4);
    paddingcount = SbEvent.wPayloadlen % 4;
    if (paddingcount)
    {
        paddingcount = 4 - paddingcount;
        memset((char*)&SbEvent.pPayload[SbEvent.wPayloadlen],0xFF,paddingcount);
        crc32e = crc32e_calculateblock(crc32e,(u32*)SbEvent.pPayload,
                                       1+SbEvent.wPayloadlen/4);
    }
    else
    {
        crc32e = crc32e_calculateblock(crc32e,(u32*)SbEvent.pPayload,
                                       SbEvent.wPayloadlen/4);
    }

    if (crc32e == SbEvent.dwCRC32E)
    {
        memcpy(rawsppxferinfo.buffer, SbEvent.pPayload, SbEvent.wPayloadlen);
        rawsppxferinfo.len = SbEvent.wPayloadlen;
        rawsppxferinfo.xfer_status = RAWSPP_READY;
        return S_SUCCESS;
    }
    else
    {
        return S_CRC32E;
    }
}

//------------------------------------------------------------------------------
// Output:  u8  *data
//          u16 *len (length of data)
// Return:  u8  status
//------------------------------------------------------------------------------
u8 bluetooth_receive_command(u8 *data, u16 *len)
{
    u8  tmpbuffer[(2048+16)];
    u32 decrypted_datalength;
    u16 count;
    u8  status;

    if (SbDevInfo.frame_avail == TRUE)
    {
        SbDevInfo.frame_avail = FALSE;
        if (SbProcessFrame_SimpleWrapper() != S_SUCCESS)
        {
            return S_FAIL;
        }
    }
    
    //[START][OK][CmdCode][2:payload_len][4:crc32e][n:payload][STOP]

    if (rawsppxferinfo.xfer_status == RAWSPP_READY)
    {
        status = S_SUCCESS;
        data[0] = SbEvent.bStatus;
        data[1] = SbEvent.bCmdCode;
        data[2] = SbEvent.wPayloadlen & 0xFF;;
        data[3] = SbEvent.wPayloadlen >> 8;
        count = 4;
        
        rawsppxferinfo.xfer_status = RAWSPP_WAITING;
        if (SbEvent.wPayloadlen > 0)
        {
            status = 
                crypto_messageblock_decrypt(SbEvent.pPayload,
                                            SbEvent.wPayloadlen,
                                            tmpbuffer,
                                            &decrypted_datalength,
                                            TRUE,CRYPTO_USE_EXTERNAL_KEY);
            if (status == S_SUCCESS)
            {
                memcpy(&data[4],tmpbuffer,decrypted_datalength);
                count += decrypted_datalength;
            }
            else
            {
                count = 0;
            }
        }
        *len = count;
        return status;
    }
    else
    {
        return S_FAIL;
    }
}

//-----------------------------------------------------------------------------
//
//-----------------------------------------------------------------------------
/*
u32 bluetooth_send_command(CMDIF_COMMAND cmd, u8 *data, u16 len)
{
    u8 buffer[330];
    u32 checksum;
    u32 i;
    u32 status;
    
    if (len > (330 - (4+4)))
    {
        return S_FAIL;
    }
    
    memcpy(buffer+8, data, len);
    len += 4 + 4;                 // Add bytes for length(CMD)+(CHECKSUM)+(Data)
    buffer[0] = ERROR_OK;
    buffer[1] = cmd;
    buffer[2] = len;
    buffer[3] = (len >> 8);
    checksum = buffer[0] + buffer[1] + buffer[2] + buffer[3];

    for(i=8;i<len;i++)
    {
        checksum += buffer[i];
    }
    buffer[4] = checksum;
    buffer[5] = checksum>>8;
    buffer[6] = checksum>>16;
    buffer[7] = checksum>>24;

    status = SbSPPSendData(0x01, (void*)buffer, len);
    
    return status;
}
*/

//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
u8 bluetooth_send_ack(CMDIF_COMMAND cmd, CMDIF_ACK resp, u8 *data, u16 datalen)
{
    u8  buffer[12];
    u8  SbCommand[16];
    u32 crc32e;
    u32 paddingcount;
    u8 msgblk[2048+16];
    u32 msgblklen;
    u8  status;

    memset(buffer,0xFF,sizeof(buffer));
    buffer[0] = ERROR_OK;           // Status
    buffer[1] = CMDIF_CMD_ACK;      // Command
    buffer[2] = datalen & 0xFF;     // Length 0
    buffer[3] = datalen >> 8;       // Length 1
    buffer[8] = resp;               // Resp code
    buffer[9] = cmd;                // Resp cmd

    if (bluetooth_info.use_encryption)
    {
        msgblklen = 0;
        if (datalen)
        {
            status = crypto_messageblock_encrypt(data,(u32)datalen,
                                                 msgblk,&msgblklen,
                                                 TRUE,CRYPTO_USE_EXTERNAL_KEY);
            if (status != S_SUCCESS)
            {
                return status;
            }
        }

        buffer[2] = msgblklen & 0xFF;   // Length 0
        buffer[3] = msgblklen >> 8;     // Length 1
        
        crc32e_reset();
        crc32e = crc32e_calculateblock(0xFFFFFFFF,(u32*)buffer,sizeof(buffer)/4);
        crc32e = crc32e_calculateblock(crc32e,(u32*)msgblk,msgblklen/4);

        buffer[4] = crc32e & 0xFF;      // CRC32E
        buffer[5] = crc32e >> 8;
        buffer[6] = crc32e >> 16;
        buffer[7] = crc32e >> 24;

        SbCommand[0] = STX_SPW;
        bluetooth_uart_tx(SbCommand, 1);
        bluetooth_uart_tx(buffer, 10);
        bluetooth_uart_tx(msgblk, msgblklen);
        SbCommand[0] = ETX_SPW;
        bluetooth_uart_tx(SbCommand, 1);
    }
    else
    {
        crc32e_reset();
        crc32e = crc32e_calculateblock(0xFFFFFFFF,(u32*)buffer,sizeof(buffer)/4);
        
        paddingcount = datalen % 4;
        if (paddingcount)
        {
            u8  tmpbuff[4];
            paddingcount = 4 - paddingcount;
            crc32e = crc32e_calculateblock(crc32e,(u32*)data,datalen/4);
            memset(tmpbuff,0xFF,sizeof(tmpbuff));
            memcpy(tmpbuff,&data[(datalen/4)*4],4-paddingcount);
            crc32e = crc32e_calculateblock(crc32e,(u32*)tmpbuff,1);
        }
        else
        {
            crc32e = crc32e_calculateblock(crc32e,(u32*)data,datalen/4);
        }
        
        buffer[4] = crc32e & 0xFF;      // CRC32E
        buffer[5] = crc32e >> 8;
        buffer[6] = crc32e >> 16;
        buffer[7] = crc32e >> 24;
        
        SbCommand[0] = STX_SPW;
        bluetooth_uart_tx(SbCommand, 1);
        bluetooth_uart_tx(buffer, 10);
        bluetooth_uart_tx(data, datalen);
        SbCommand[0] = ETX_SPW;
        bluetooth_uart_tx(SbCommand, 1);
    }

    return S_SUCCESS;
}

//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
u8 bluetooth_send_ack_old_national(CMDIF_COMMAND cmd, CMDIF_ACK resp, u8 *data, u16 datalen)
{
    u8  buffer[10];
    u8  SbCommand[16];
    u16 payload;
    u16 framecontentlen;
    u32 checksum;
    u32 i;
//    u32 status;
    
    buffer[0] = ERROR_OK;           // Status
    buffer[1] = CMDIF_CMD_ACK;      // Command
    buffer[2] = datalen;            // Length 0
    buffer[3] = datalen>>8;         // Length 1    
    buffer[8] = resp;               // Resp code
    buffer[9] = cmd;                // Resp cmd        

    checksum = buffer[0] + buffer[1] + buffer[2] + buffer[3] + 
        buffer[8] + buffer[9];
    for(i=0;i<datalen;i++)              // Calculate checksum
    {
        checksum += data[i];
    }
    buffer[4] = checksum;           // Save checksum in command
    buffer[5] = checksum>>8;
    buffer[6] = checksum>>16;
    buffer[7] = checksum>>24;

    payload = datalen + 10 + 3;
    framecontentlen = payload - 3;
    SbCommand[0] = STX;
    SbCommand[1] = REQ;
    SbCommand[2] = SPP_SEND_DATA;
    SbCommand[3] = (payload & 0x00FF); // payload size is stored 
    SbCommand[4] = (payload >> 8);     // in little endian fashion
    checksum     = SbCommand[1] + SbCommand[2] + SbCommand[3] + SbCommand[4];
    SbCommand[5] = checksum % 256;
    SbCommand[6] = 1;               //LocalPortNo;
    SbCommand[7] = (framecontentlen & 0x00FF);  // payload size is stored 
    SbCommand[8] = (framecontentlen >> 8);      // in little endian fashion
    
    bluetooth_uart_tx(SbCommand, 9);
    bluetooth_uart_tx(buffer, 10);
    bluetooth_uart_tx(data, datalen);
    SbCommand[0] = ETX;
    bluetooth_uart_tx(SbCommand, 1);
    
    return S_SUCCESS;
}

//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
/*
u8 bluetooth_send_ack_nowait(CMDIF_COMMAND cmd, CMDIF_ACK resp, u8 *data, u16 len)
{
    u8  buffer[330];
    u32 checksum;
    u32 i;
    u32 status;
    
    memcpy(buffer+10, data, len);   // Copy in data
    len += 6 + 4;                   // Add bytes for length(CMD)+(CHECKSUM)+(Data)
    buffer[0] = ERROR_OK;           // Status
    buffer[1] = CMDIF_CMD_ACK;      // Command
    buffer[2] = len;                // Length 0
    buffer[3] = len>>8;             // Length 1    
    buffer[8] = resp;               // Resp code
    buffer[9] = cmd;                // Resp cmd        

    checksum = buffer[0] + buffer[1] + buffer[2] + buffer[3] + 
        buffer[8] + buffer[9];
    for(i=10;i<len;i++)             // Calculate checksum
    {
        checksum += buffer[i];
    }
    buffer[4] = checksum;           // Save checksum in command
    buffer[5] = checksum>>8;
    buffer[6] = checksum>>16;
    buffer[7] = checksum>>24;

    status = SbSPPSendDataNoWait(0x01, (void*)buffer, len);
    return status;
}
*/

//-----------------------------------------------------------------------------
//
//-----------------------------------------------------------------------------
/*
u8 bluetooth_send_rawspp(u8 *txbuffer, u16 length)
{
    u16 bufferindex;
    SBStatus_T status;
    u16 txlen;
    
    bufferindex = 0;
    
    while(bufferindex < length)
    {
        txlen = length - bufferindex;
        if (txlen > RAWSPP_SEND_DATA_SIZE)
        {
            status = SbSPPSendData(0x01,
                                   txbuffer+bufferindex, RAWSPP_SEND_DATA_SIZE);
        }
        else
        {
            status = SbSPPSendData(0x01,
                                   txbuffer+bufferindex, txlen);
        }
        
        if (status != SBSTATUS_OK)
        {
            debug("\nError Tx Block!");
        }

        bufferindex += RAWSPP_SEND_DATA_SIZE;
    }
    
    return S_SUCCESS;
}
*/

//------------------------------------------------------------------------------
// Checks the local bluetooth port for a connection status
// Input:   SbPortNumber btport
// Return:  u8 status
//------------------------------------------------------------------------------
/*
u8 bluetooth_checkconnection(SbPortNumber btport)
{
    u8 response_data[3];
    
    if(SbGetPortStatus(btport, response_data) == SBSTATUS_OK)
    {
        if(response_data[0] &= 0x80)
        {
            return S_SUCCESS;   // DLC Availble
        }
    }
    return S_FAIL;  // No DLC availble or error
}
*/
