/**
  **************** (C) COPYRIGHT 2011 SCT Performance, LLC *********************
  * File Name          : init.c
  * Device / System    : SCT products using the "CommonCode" platform
  * Author             : Quyen Leba
  *
  * Version            : 1 
  * Date               : 05/02/2011
  * Description        : 
  *                    : 
  *
  *
  * History            : 05/02/2011 M. Davis
  *                    :   Add SCT Copyright header,
  *
  ******************************************************************************
  */

#include <board/interrupt.h>
#include <board/peripherals.h>
#include <common/crypto_blowfish.h>
#include <common/statuscode.h>
#include "system.h"
#include "clock.h"
#include "init.h"

//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
void init()
{
    clock_init();
    interrupt_init();
    crypto_blowfish_init();
    peripherals_init();
}
