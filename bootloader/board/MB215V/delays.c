/**
  **************** (C) COPYRIGHT 2011 SCT Performance, LLC *********************
  * File Name          : delays.c
  * Device / System    : SCT products using the "CommonCode" platform
  * Author             : Quyen Leba
  *
  * Version            : 1 
  * Date               : 05/02/2011
  * Description        : 
  *                    : 
  *
  *
  * History            : 05/02/2011 M. Davis
  *                    :   Add SCT Copyright header,
  *
  ******************************************************************************
  */

//#include <stm32f2xx_systick.h>
#include <board/delays.h>

vu32 TimingDelay = 0;

//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
void delays(u32 timeout, u8 prefix)
{
    if(prefix == 'm')
    {
        SysTick_Config(SystemCoreClock / 1000); //TODOQ2:
//        SysTick->LOAD = 72000;
    }
    else
    {
//        SysTick->LOAD = 72;
        SysTick_Config(SystemCoreClock / 1000000);
    }

    TimingDelay = timeout;
//    SysTick->CTRL |= SysTick_Counter_Enable;
    while(TimingDelay);

    // Disable the SysTick Counter
//    SysTick->CTRL &= SysTick_Counter_Disable;
    
    // Clear the SysTick Counter
//    SysTick->VAL = SysTick_Counter_Clear;
}
