/**
  **************** (C) COPYRIGHT 2011 SCT Performance, LLC *********************
  * File Name          : button.c
  * Device / System    : SCT products using the "CommonCode" platform
  * Author             : Quyen Leba
  *
  * Version            : 1 
  * Date               : 05/02/2011
  * Description        : 
  *                    : 
  *
  *
  * History            : 05/02/2011 M. Davis
  *                    :   Add SCT Copyright header,
  *
  ******************************************************************************
  */

#include "stm32f2xx_rcc.h"
#include "stm32f2xx_gpio.h"
#include "stm32f2xx_exti.h"
#include "misc.h"
#include "system.h"
#include "button.h"

#define BUTTON_GPIO_PORT            GPIOB
#define BUTTON_GPIO_CLK             RCC_APB2Periph_GPIOB
#define BUTTON_GPIO_PORTSOURCE      GPIO_PortSourceGPIOB
#define BUTTON_GPIO_PIN             GPIO_Pin_14
#define BUTTON_GPIO_PINSOURCE       GPIO_PinSource14
#define BUTTON_EXTI_LINE            EXTI_Line14
#define BUTTON_EXTI_CHANNEL         EXTI15_10_IRQn

struct
{
    u8  pressed     :1;
    u8  triggered   :1;
}button_info;

//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
void button_init()
{
    GPIO_InitTypeDef GPIO_InitStructure;
    //Configure PB.0, as input pull up
    GPIO_InitStructure.GPIO_Pin = BUTTON_GPIO_PIN;
    GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
    GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IN;
    GPIO_InitStructure.GPIO_OType = GPIO_OType_PP;
    GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_UP;
    GPIO_Init(BUTTON_GPIO_PORT, &GPIO_InitStructure);
    
    button_info.pressed = 0;
    button_info.triggered = 0;
}

//------------------------------------------------------------------------------
// Active LOW button
//------------------------------------------------------------------------------
u8 button_getState(void)
{
    return GPIO_ReadInputDataBit(BUTTON_GPIO_PORT, BUTTON_GPIO_PIN);
}
