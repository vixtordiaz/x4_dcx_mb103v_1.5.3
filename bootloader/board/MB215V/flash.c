/**
  **************** (C) COPYRIGHT 2011 SCT Performance, LLC *********************
  * File Name          : flash.c
  * Device / System    : SCT products using the "CommonCode" platform
  * Author             : Quyen Leba
  *
  * Version            : 1 
  * Date               : 05/02/2011
  * Description        : 
  *                    : 
  *
  *
  * History            : 05/02/2011 M. Davis
  *                    :   Add SCT Copyright header,
  *
  ******************************************************************************
  */

#include <string.h>
#include <stm32f2xx_flash.h>
#include <board/genplatform.h>
#include <common/statuscode.h>
#include <common/crypto_blowfish.h>
#include <common/crc32.h>
#include "flash.h"

#define FLASH_TOTAL_SECTOR          9

const u32 erase_sector_size_lookup[FLASH_TOTAL_SECTOR] = 
{
    [0] = 16*1024,  [1] = 16*1024,  [2] = 16*1024,  [3] = 16*1024,
    [4] = 64*1024,
    [5] = 128*1024, [6] = 128*1024, [7] = 128*1024, [8] = 128*1024
};

const u32 erase_sector_number_lookup[FLASH_TOTAL_SECTOR] = 
{
    [0] = FLASH_Sector_0,
    [1] = FLASH_Sector_1,
    [2] = FLASH_Sector_2,
    [3] = FLASH_Sector_3,
    [4] = FLASH_Sector_4,
    [5] = FLASH_Sector_5,
    [6] = FLASH_Sector_6,
    [7] = FLASH_Sector_7,
    [8] = FLASH_Sector_8,
};

//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
u8 flash_load_setting(FlashSettingsArea flashsettingsarea,
                      u8 *settingsdata, u32 settingsdatalength)
{
    switch(flashsettingsarea)
    {
    case CriticalArea:
        memcpy((char*)settingsdata,
               (char*)FLASHSETTINGS_CRITICAL_ADDR,settingsdatalength);
        break;
    case TuneArea:
        memcpy((char*)settingsdata,
               (char*)FLASHSETTINGS_TUNE_ADDR,settingsdatalength);
        break;
    case DatalogGeneralArea:
        memcpy((char*)settingsdata,
               (char*)FLASHSETTINGS_DATALOGGENERAL_ADDR,settingsdatalength);
        break;
    default:
        return S_INPUT;
    }

    return S_SUCCESS;
}

//------------------------------------------------------------------------------
// Erase a sector
// Input:   u16 sector (0..11)
// Return:  u8  status
// Engineer: Quyen Leba
// Note: always use with VoltageRange_3
// Sectors: 0-3 (16kB), 4 (64kB), 6-11 (128kB)
//------------------------------------------------------------------------------
u8 flash_erase_sector(u16 sector)
{
    FLASH_Status FlashStatus;
    u32 erase_sector;

    if (sector >= FLASH_TOTAL_SECTOR)
    {
        return S_INPUT;
    }

    erase_sector = erase_sector_number_lookup[sector];

    FLASH_Unlock();
    FLASH_ClearFlag(FLASH_FLAG_EOP | FLASH_FLAG_OPERR | FLASH_FLAG_WRPERR | 
                    FLASH_FLAG_PGAERR | FLASH_FLAG_PGPERR|FLASH_FLAG_PGSERR);
    FlashStatus = FLASH_EraseSector(erase_sector, VoltageRange_3);
    FLASH_Lock();

    if (FlashStatus != FLASH_COMPLETE)
    {
        return S_FAIL;
    }
    return S_SUCCESS;
}

//------------------------------------------------------------------------------
// Write to a flash sector
// Inputs:  u16 sector
//          u16 offset
//          u32 erase_sector_size
//          u8  *data (encrypted)
//          u16 datalength (must be multiple of 8)
//          bool encrypted (TRUE: data is encrypted)
// Return:  u8  status
// Engineer: Quyen Leba
//------------------------------------------------------------------------------
u8 flash_write_sector_at_offset(u16 sector, u32 offset, u32 erase_sector_size,
                                u8 *data, u16 datalength, bool encrypted)
{
    u32 start_address;
    u32 address,i;
    u32 *wptr;
    FLASH_Status FlashStatus;

    //TODOQ: check sector and length
    start_address = FLASH_START_ADDRESS;
    for(i=0;i<sector;i++)
    {
        start_address += erase_sector_size_lookup[i];
    }

    if (offset+datalength > erase_sector_size)
    {
        return S_INPUT;
    }

    FLASH_Unlock();
    FLASH_ClearFlag(FLASH_FLAG_EOP | FLASH_FLAG_OPERR | FLASH_FLAG_WRPERR | 
                    FLASH_FLAG_PGAERR | FLASH_FLAG_PGPERR|FLASH_FLAG_PGSERR);
    if (encrypted)
    {
        crypto_blowfish_decryptblock_critical(data,datalength);
    }
    wptr = (u32*)data;
    for(address=start_address+offset;
        address<start_address+offset+datalength;address+=4)
    {
        FlashStatus = FLASH_ProgramWord(address,*wptr++);
        if (FlashStatus != FLASH_COMPLETE)
        {
            FLASH_Lock();
            return S_FAIL;
        }
    }
    FLASH_Lock();

    return S_SUCCESS;
}

//------------------------------------------------------------------------------
// Validate content
// Inputs:  u16 sector (starting sector)
//          u32 length
//          u32 crc32e
// Return:  u8  status
// Engineer: Quyen Leba
//------------------------------------------------------------------------------
u8 flash_validate_content(u16 sector, u32 length, u32 crc32e)
{
    u32 start_address;
    u32 calc_crc32e;
    u8  i;

    //TODOQ: check sector and length
    start_address = FLASH_START_ADDRESS;
    for(i=0;i<sector;i++)
    {
        start_address += erase_sector_size_lookup[i];
    }

    crc32e_reset();
    calc_crc32e = crc32e_calculateblock(0xFFFFFFFF,(u32*)start_address,length/4);
    if (calc_crc32e != crc32e)
    {
        return S_CRC32E;
    }
    else
    {
        return S_SUCCESS;
    }
}

//------------------------------------------------------------------------------
// Load bootsettings from flash
// Input:   u32 length
// Output:  u8  *settings
// Return:  u8  status
// Engineer: Quyen Leba
//------------------------------------------------------------------------------
u8 flash_load_bootsettings(u8 *settings, u32 length)
{
    if (length <= FLASHSETTINGS_MAX_SETTINGS_SIZE)
    {
        memcpy((char*)settings,(char*)FLASHSETTINGS_BOOTSETTINGS_ADDR,length);
        return S_SUCCESS;
    }
    return S_INPUT;
}

//------------------------------------------------------------------------------
// Store bootsettings to flash
// Inputs:  u8  *settings
//          u16 length
// Return:  u8  status
// Engineer: Quyen Leba
//------------------------------------------------------------------------------
u8 flash_store_bootsettings(u8 *settings, u16 length)
{
    u32 erase_sector;
    FLASH_Status FlashStatus = FLASH_COMPLETE;
    struct
    {
        u32 address[5];
        u8  *buffer;
        u8  count;
        u8  working_index;
    }backup =
    {
        .buffer = NULL,
    };
    u32 i,j;
    u32 *wptr;
    u8  status;

    if (length > FLASHSETTINGS_MAX_SETTINGS_SIZE)
    {
        return S_BADCONTENT;
    }
    else if (memcmp((char*)FLASHSETTINGS_BOOTSETTINGS_ADDR,(char*)settings,length) == 0)
    {
        return S_SUCCESS;
    }
    backup.buffer = __malloc(5*FLASHSETTINGS_MAX_SETTINGS_SIZE);
    if (backup.buffer == NULL)
    {
        return S_MALLOC;
    }

    memcpy((char*)&backup.buffer[0*FLASHSETTINGS_MAX_SETTINGS_SIZE],
           (char*)FLASHSETTINGS_BOOTSETTINGS_ADDR,
           FLASHSETTINGS_MAX_SETTINGS_SIZE);
    backup.address[0] = FLASHSETTINGS_BOOTSETTINGS_ADDR;
    memcpy((char*)&backup.buffer[0*FLASHSETTINGS_MAX_SETTINGS_SIZE],
           (char*)settings,length);

    memcpy((char*)&backup.buffer[1*FLASHSETTINGS_MAX_SETTINGS_SIZE],
           (char*)FLASHSETTINGS_CRITICAL_ADDR,
           FLASHSETTINGS_MAX_SETTINGS_SIZE);
    backup.address[1] = FLASHSETTINGS_CRITICAL_ADDR;

    memcpy((char*)&backup.buffer[2*FLASHSETTINGS_MAX_SETTINGS_SIZE],
           (char*)FLASHSETTINGS_TUNE_ADDR,
           FLASHSETTINGS_MAX_SETTINGS_SIZE);
    backup.address[2] = FLASHSETTINGS_TUNE_ADDR;

    memcpy((char*)&backup.buffer[3*FLASHSETTINGS_MAX_SETTINGS_SIZE],
           (char*)FLASHSETTINGS_FLEET_ADDR,
           FLASHSETTINGS_MAX_SETTINGS_SIZE);
    backup.address[3] = FLASHSETTINGS_FLEET_ADDR;

    memcpy((char*)&backup.buffer[4*FLASHSETTINGS_MAX_SETTINGS_SIZE],
           (char*)FLASHSETTINGS_DATALOGGENERAL_ADDR,
           FLASHSETTINGS_MAX_SETTINGS_SIZE);
    backup.address[4] = FLASHSETTINGS_DATALOGGENERAL_ADDR;

    backup.count = 5;
    backup.working_index = 0;
    erase_sector = FLASH_Sector_3;

    FLASH_Unlock();

    FLASH_ClearFlag(FLASH_FLAG_EOP | FLASH_FLAG_OPERR | FLASH_FLAG_WRPERR | 
                    FLASH_FLAG_PGAERR | FLASH_FLAG_PGPERR|FLASH_FLAG_PGSERR);

    FlashStatus = FLASH_EraseSector(erase_sector, VoltageRange_3);
    if (FlashStatus != FLASH_COMPLETE)
    {
        FLASH_Lock();
        status = S_FAIL;
        goto flash_store_bootsettings_done;
    }

    // write new settings to flash
    for(i=0;i<backup.count;i++)
    {
        wptr = (u32*)&backup.buffer[i*FLASHSETTINGS_MAX_SETTINGS_SIZE];
        for(j=0;j<FLASHSETTINGS_MAX_SETTINGS_SIZE;j+=4)
        {
            FlashStatus = FLASH_ProgramWord(backup.address[i]+j,*wptr++);
            if (FlashStatus != FLASH_COMPLETE)
            {
                FLASH_Lock();
                status = S_FAIL;
                goto flash_store_bootsettings_done;
            }
        }
    }
    FLASH_Lock();

flash_store_bootsettings_done:
if (backup.buffer)
    {
        __free(backup.buffer);
    }
    return status;
}
