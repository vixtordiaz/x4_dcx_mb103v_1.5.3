/**
  **************** (C) COPYRIGHT 2011 SCT Performance, LLC *********************
  * File Name          : spi.h
  * Device / System    : SCT products using the "CommonCode" platform
  * Author             : Quyen Leba
  *
  * Version            : 1 
  * Date               : 05/02/2011
  * Description        : 
  *                    : 
  *
  *
  * History            : 05/02/2011 M. Davis
  *                    :   Add SCT Copyright header,
  *
  ******************************************************************************
  */

#ifndef __SPI_H
#define __SPI_H

#include <arch/gentype.h>

void spi_master_init();
u8 spi_master_send_byte(u8 databyte);
u8 spi_master_receive_byte();

#endif	//__SPI_H
