/**
  **************** (C) COPYRIGHT 2011 SCT Performance, LLC *********************
  * File Name          : led.c
  * Device / System    : SCT products using the "CommonCode" platform
  * Author             : Quyen Leba
  *
  * Version            : 1 
  * Date               : 05/02/2011
  * Description        : 
  *                    : 
  *
  *
  * History            : 05/02/2011 M. Davis
  *                    :   Add SCT Copyright header,
  *
  ******************************************************************************
  */

#include <arch/gentype.h>
#include <board/delays.h>
#include "timer.h"
#include "led.h"

#define LED_SUPPORT_PWM         0

#define LED_GPIO_PORT           GPIOD
#define LED_RED_PIN             GPIO_Pin_13
#define LED_GREEN_PIN           GPIO_Pin_14
#define LED_BLUE_PIN            GPIO_Pin_15
#define LED_ALL_PIN             LED_RED_PIN | LED_GREEN_PIN | LED_BLUE_PIN

#define LED_RED_PWM_CH          2
#define LED_GREEN_PWM_CH        3
#define LED_BLUE_PWM_CH         4

//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
void led_init()
{
    GPIO_InitTypeDef GPIO_InitStructure;

#if LED_SUPPORT_PWM
    timer4_init();

    GPIO_InitStructure.GPIO_Pin =  LED_ALL_PIN;
    GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AF_PP;
    GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
    GPIO_Init(LED_GPIO_PORT, &GPIO_InitStructure);
    GPIO_PinRemapConfig(GPIO_Remap_TIM4, ENABLE);
#else
    //config as outputs
    GPIO_InitStructure.GPIO_Pin = LED_ALL_PIN;
    GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
    GPIO_InitStructure.GPIO_Mode = GPIO_Mode_OUT;
    GPIO_InitStructure.GPIO_OType = GPIO_OType_PP;
    GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_NOPULL;
    GPIO_Init(LED_GPIO_PORT, &GPIO_InitStructure);
#endif
    led_setState(LED_OFF);
}

//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
void led_setState(LED_State state)
{
#if LED_SUPPORT_PWM
    u8 R=100,G=100,B=100;
    timer4_pwm_duty_cycle(LED_RED_PWM_CH,R);
    timer4_pwm_duty_cycle(LED_GREEN_PWM_CH,G);
    timer4_pwm_duty_cycle(LED_BLUE_PWM_CH,B);
    
    switch(state)
    {
    case LED_OFF:
        timer4_pwm_duty_cycle(LED_RED_PWM_CH,100);
        timer4_pwm_duty_cycle(LED_GREEN_PWM_CH,100);
        timer4_pwm_duty_cycle(LED_BLUE_PWM_CH,100);
        break;
    case LED_RED:
        timer4_pwm_duty_cycle(LED_RED_PWM_CH,0);
        timer4_pwm_duty_cycle(LED_GREEN_PWM_CH,100);
        timer4_pwm_duty_cycle(LED_BLUE_PWM_CH,100);
        break;
    case LED_GREEN:
        timer4_pwm_duty_cycle(LED_RED_PWM_CH,100);
        timer4_pwm_duty_cycle(LED_GREEN_PWM_CH,0);
        timer4_pwm_duty_cycle(LED_BLUE_PWM_CH,100);
        break;
    case LED_BLUE:
        timer4_pwm_duty_cycle(LED_RED_PWM_CH,100);
        timer4_pwm_duty_cycle(LED_GREEN_PWM_CH,100);
        timer4_pwm_duty_cycle(LED_BLUE_PWM_CH,0);
        break;
    case LED_BLUERED:
        timer4_pwm_duty_cycle(LED_RED_PWM_CH,0);
        timer4_pwm_duty_cycle(LED_GREEN_PWM_CH,100);
        timer4_pwm_duty_cycle(LED_BLUE_PWM_CH,0);
        break;
    case LED_BLUEGREEN:
        timer4_pwm_duty_cycle(LED_RED_PWM_CH,100);
        timer4_pwm_duty_cycle(LED_GREEN_PWM_CH,0);
        timer4_pwm_duty_cycle(LED_BLUE_PWM_CH,0);
        break;
    case LED_YELLOW:
        timer4_pwm_duty_cycle(LED_RED_PWM_CH,0);
        timer4_pwm_duty_cycle(LED_GREEN_PWM_CH,20);
        timer4_pwm_duty_cycle(LED_BLUE_PWM_CH,100);
        break;
    default:
        break;
    }
#else
    switch(state)
    {
    case LED_OFF:
        GPIO_SetBits(LED_GPIO_PORT, LED_ALL_PIN);
        break;
    case LED_RED:
        GPIO_ResetBits(LED_GPIO_PORT, LED_RED_PIN);
        GPIO_SetBits(LED_GPIO_PORT, LED_GREEN_PIN | LED_BLUE_PIN);
        break;
    case LED_GREEN:
        GPIO_SetBits(LED_GPIO_PORT, LED_RED_PIN | LED_BLUE_PIN);
        GPIO_ResetBits(LED_GPIO_PORT, LED_GREEN_PIN);
        break;
    case LED_BLUE:
        GPIO_SetBits(LED_GPIO_PORT, LED_RED_PIN | LED_GREEN_PIN);
        GPIO_ResetBits(LED_GPIO_PORT, LED_BLUE_PIN);
        break;
    case LED_BLUERED:
        GPIO_SetBits(LED_GPIO_PORT, LED_GREEN_PIN);
        GPIO_ResetBits(LED_GPIO_PORT, LED_BLUE_PIN | LED_RED_PIN);
        break;
    case LED_BLUEGREEN:
        GPIO_SetBits(LED_GPIO_PORT, LED_RED_PIN);
        GPIO_ResetBits(LED_GPIO_PORT, LED_BLUE_PIN | LED_GREEN_PIN);
        break;
    case LED_YELLOW:
        GPIO_ResetBits(LED_GPIO_PORT, LED_RED_PIN | LED_GREEN_PIN);
        GPIO_SetBits(LED_GPIO_PORT, LED_BLUE_PIN);
        break;
    default:
        break;
    }
#endif
}

//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
void led_enter_bootloader()
{
#if __FAILSAFE_BOOTLOADER__
    led_setState(LED_RED); delays(100,'m'); led_setState(LED_OFF); delays(100,'m');
    led_setState(LED_RED); delays(100,'m'); led_setState(LED_OFF); delays(100,'m');
    led_setState(LED_RED); delays(100,'m'); led_setState(LED_OFF);
#else
    led_setState(LED_RED); delays(100,'m'); led_setState(LED_OFF); delays(100,'m');
    led_setState(LED_RED); delays(100,'m'); led_setState(LED_OFF);
#endif
}
