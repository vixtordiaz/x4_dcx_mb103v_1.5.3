/**
  **************** (C) COPYRIGHT 2011 SCT Performance, LLC *********************
  * File Name          : bootloader.c
  * Device / System    : SCT products using the "CommonCode" platform
  * Author             : Quyen Leba
  *
  * Version            : 1 
  * Date               : 05/02/2011
  * Description        : 
  *                    : 
  *
  *
  * History            : 05/02/2011 M. Davis
  *                    :   Add SCT Copyright header,
  *
  ******************************************************************************
  */

#include <string.h>
#include <device_config.h>
#include <board/bootloader.h>
#include <board/delays.h>
#include <common/crc32.h>
#include <common/bootsettings.h>
#include <common/settings.h>
#include <common/crypto_blowfish.h>
#include <common/tea.h>
#include <common/filecrypto.h>
#include <common/statuscode.h>

#define FLASHCHECK_BLOCKSIZE    1024

BootloaderInfo bootloader_info =
{
    .firmware_version = 0,
    .firmware_size = 0,
    .firmware_crc32e = 0,
    .bytecount = 0,
    .bytecheck = 0,
    .erase_sector_size = 0,
    .status.validated = 0,
    .status.init = 0,
};

typedef void (*pFunction)(void);

//------------------------------------------------------------------------------
// Private prototypes
//------------------------------------------------------------------------------
u8 bootloader_validate_main_bootloader(u32 cmp_crc32e);
u8 bootloader_validate_application();

//------------------------------------------------------------------------------
// Init bootloader. It handles starting device in boot or application
// Engineer: Quyen Leba
//------------------------------------------------------------------------------
void bootloader_init()
{
    delays(30,'m');     //need this before sampling button pin  //for now
    
    if(gpio_is_boot_en_pin_active())   //force to go to bootloader
    {
        //do nothing; i.e. stay in failsafe/mainboot bootloader
        return;
    }

    if (BOOTSETTINGS_GetBootMode() == BootloaderMode_MB_Main)
    {
#ifdef __MAIN_BOOTLOADER__
        //do nothing; i.e. stay in main bootloader
#else
        bootloader_jump_to_main_bootloader();
#endif
    }
    else if (BOOTSETTINGS_GetBootMode() == BootloaderMode_MB_FailSafe)
    {
#ifdef __FAILSAFE_BOOTLOADER__
        //do nothing; i.e. stay in failsafe bootloader
#else
        bootloader_jump_to_failsafe_bootloader();
#endif
    }
    else    //BootloaderMode_MB_Application
    {
        bootloader_jump_to_application();
    }
}

//------------------------------------------------------------------------------
// Jump failsafe bootloader
// Engineer: Quyen Leba
//------------------------------------------------------------------------------
void bootloader_jump_to_failsafe_bootloader()
{
    u32 JumpAddress;
    pFunction Jump_To_Bootloader;

    if (BOOTSETTINGS_GetBootMode() != BootloaderMode_MB_FailSafe)
    {
        BOOTSETTINGS_SetBootMode(BootloaderMode_MB_FailSafe);
        bootsettings_update();
    }
    
    JumpAddress = *(vu32*)(BOOTLOADER_FAILSAFE_ADDRESS + 4);
    Jump_To_Bootloader = (pFunction)JumpAddress;
    // Initialize user application's Stack Pointer 
    __set_MSP(*(vu32*) BOOTLOADER_FAILSAFE_ADDRESS);
    Jump_To_Bootloader();
    while(1);
}

//------------------------------------------------------------------------------
// Jump to main bootloader. Because main bootloader can be modified,
// validate it before use. Main bootloader is run from RAM, and must
// be loaded from FLASH first.
// Engineer: Quyen Leba
//------------------------------------------------------------------------------
void bootloader_jump_to_main_bootloader()
{
    u32 JumpAddress;
    pFunction Jump_To_Bootloader;

    if (bootloader_validate_main_bootloader
        (BOOTSETTINGS_GetMainBootCRC32E()) == S_SUCCESS)
    {
        memcpy((char*)BOOTLOADER_MAIN_RAM_ADDRESS,
               (char*)BOOTLOADER_MAIN_FLASH_ADDRESS,BOOTLOADER_MAIN_SIZE);

        if (BOOTSETTINGS_GetBootMode() != BootloaderMode_MB_Main)
        {
            BOOTSETTINGS_SetBootMode(BootloaderMode_MB_Main);
            bootsettings_update();
        }
        
        JumpAddress = *(vu32*)(BOOTLOADER_MAIN_RAM_ADDRESS + 4);
        Jump_To_Bootloader = (pFunction)JumpAddress;
        // Initialize user application's Stack Pointer 
        __set_MSP(*(vu32*) BOOTLOADER_MAIN_RAM_ADDRESS);
        Jump_To_Bootloader();
        while(1);
    }
}

//------------------------------------------------------------------------------
// Jump to application.
// Engineer: Quyen Leba
//------------------------------------------------------------------------------
void bootloader_jump_to_application()
{
    u32 JumpAddress;
    pFunction Jump_To_Application;

    if (bootloader_validate_application() == S_SUCCESS)
    {
        if (BOOTSETTINGS_GetBootMode() != BootloaderMode_MB_Application)
        {
            BOOTSETTINGS_SetBootMode(BootloaderMode_MB_Application);
            bootsettings_update();
        }
        
        JumpAddress = *(vu32*)(BOOTLOADER_APPLICATION_ADDRESS + 4);
        Jump_To_Application = (pFunction)JumpAddress;
        // Initialize user application's Stack Pointer 
        __set_MSP(*(vu32*) BOOTLOADER_APPLICATION_ADDRESS);
        Jump_To_Application();
        while(1);
    }
}

//------------------------------------------------------------------------------
// Validate main bootloader
// Input:   u32 cmp_crc32e
// Return:  u8  status (S_SUCCESS: good)
// Engineer: Quyen Leba
//------------------------------------------------------------------------------
u8 bootloader_validate_main_bootloader(u32 cmp_crc32e)
{
    u32 calc_crc32e;

    crc32e_reset();
    calc_crc32e = crc32e_calculateblock(0xFFFFFFFF,
                                        (u32*)BOOTLOADER_MAIN_FLASH_ADDRESS,
                                        BOOTLOADER_MAIN_SIZE/4);
    if (calc_crc32e != cmp_crc32e)
    {
        return S_FAIL;
    }
    return S_SUCCESS;
}

//------------------------------------------------------------------------------
// Validate application stored in FLASH
// Return:  u8  status (S_SUCCESS: good)
// Engineer: Quyen Leba
//------------------------------------------------------------------------------
u8 bootloader_validate_application()
{
    u32 calc_crc32e;

    if (BOOTSETTINGS_GetAppLength() > BOOTLOADER_APPLICATION_SIZE_MAX)
    {
        return S_BADCONTENT;
    }
    crc32e_reset();
    calc_crc32e = crc32e_calculateblock
        (0xFFFFFFFF,(u32*)BOOTLOADER_APPLICATION_ADDRESS,
         BOOTSETTINGS_GetAppLength()/4);
    if (calc_crc32e != BOOTSETTINGS_GetAppCRC32E())
    {
        return S_FAIL;
    }
    return S_SUCCESS;
}

//------------------------------------------------------------------------------
// Set boot mode in bootsettings
// Input:   u16 bootmode
// Return:  u8  status
// Engineer: Quyen Leba
//------------------------------------------------------------------------------
u8 bootloader_setboot(BootloaderMode bootmode)
{
    u8  status;

    status = S_SUCCESS;
    if (bootmode != BOOTSETTINGS_GetBootMode())
    {
        switch(bootmode)
        {
        case BootloaderMode_MB_FailSafe:
        case BootloaderMode_MB_Main:
        case BootloaderMode_MB_Application:
            BOOTSETTINGS_SetBootMode((u16)bootmode);
            status = bootsettings_update();
            break;
        default:
            log_push_error_point(0xBBA0);
            return S_INPUT;
        }
    }
    return status;
}

//------------------------------------------------------------------------------
// Setup a bootloader session
// Input:   u8  *firmware_header
// Output:  u16 *sector_size
// Return:  u8  status (S_NO_UPDATE: skip, S_SUCCESS: do the update)
// Engineer: Quyen Leba
//------------------------------------------------------------------------------
u8 bootloader_setup_session(u8 *firmware_header, u16 *sector_size)
{
    FirmwareHeader fwheader;
    u8  tmpbuffer[FLASHCHECK_BLOCKSIZE];
    u32 length;
    u32 *wptr;
    u32 max_size;
    u32 calc_crc32e;
    u32 calc_signature;
    u32 bytecount;
    u32 tea_key[4];
    BootloaderMode new_bootmode;
    bool is_backdating_allowed;
    bool is_content_different;
    bool is_update_required;
    bool is_reboot;
    u32 i;

    bootloader_info.status.init = 0;
    bootloader_info.firmware_size = 0;
    bootloader_info.firmware_crc32e = 0;

    is_backdating_allowed = FALSE;
    is_content_different = FALSE;
    is_update_required = FALSE;
    is_reboot = FALSE;
    
    *sector_size = 0;
    bootloader_info.bytecount = 0;
    bootloader_info.bytecheck = 0;
    memcpy((char*)&fwheader,(char*)firmware_header,sizeof(FirmwareHeader));
    crypto_blowfish_decryptblock_critical((u8*)&fwheader,sizeof(FirmwareHeader));

    //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    // Validate firmware header
    //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    wptr = (u32*)&fwheader;
    wptr++;
    crc32e_reset();
    calc_crc32e = crc32e_calculateblock(0xFFFFFFFF,
                                        (u32*)wptr,(sizeof(FirmwareHeader)-4)/4);
    if (calc_crc32e != fwheader.hcrc)
    {
        log_push_error_point(0xBBB0);
        return S_CRC32E;
    }
    if (fwheader.hversion > FILECRYPTO_GENERAL_HEADER_VERSION_MAX)
    {
        log_push_error_point(0xBBB1);
        return S_NOTSUPPORT;
    }
    if (fwheader.ftype != FILETYPE_FIRMWARE)
    {
        log_push_error_point(0xBBB2);
        return S_INVALIDTYPE;
    }
    else if (fwheader.hardware != HARDWARE_MainBoard_RevA)
    {
        log_push_error_point(0xBBB3);
        return S_WRONG_HARDWARE;
    }
    
    //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    // Check Serial Number
    //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    if (memcmp((char*)SETTINGS_CRITICAL(serialnumber),
               (char*)fwheader.serialnumber,sizeof(fwheader.serialnumber)) != 0)
    {
        u32 sum1,sum2;

        for (i=0,sum1=0,sum2=0;i<sizeof(fwheader.serialnumber);i++)
        {
            sum1 += fwheader.serialnumber[i];   //all ZERO
            sum2 += fwheader.serialnumber[i];   //all 0xFF
        }
        if (sum1 != 0 && sum2 != (16*0xFF))
        {
            log_push_error_point(0xBBB4);
            return S_SERIALUNMATCH;
        }
    }
    
    //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    // Check Backdating
    //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    tea_key[0] = TEA_KEY_0;
    tea_key[1] = TEA_KEY_1;
    tea_key[2] = TEA_KEY_2;
    tea_key[3] = TEA_KEY_3;
    wptr = (u32*)fwheader.serialnumber;
    for(i=0;i<sizeof(fwheader.serialnumber)/8;i++)
    {
        tea_encryption(wptr,tea_key);
        wptr+=2;
    }
    
    crc32e_reset();
    calc_signature = crc32e_calculateblock(0xFFFFFFFF,
                                           (u32*)fwheader.serialnumber,
                                           sizeof(fwheader.serialnumber)/4);
    if (calc_signature == fwheader.signature ||
        BOOTSETTINGS_GetEngSign() == ENGINEERING_SIGNATURE)
    {
        is_backdating_allowed = TRUE;
    }

    //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    //
    //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    switch(fwheader.firmware_flags & 0x07)
    {
    case FIRMWARE_FLAGS_APPLICATION:
        bootloader_info.current_sector = FLASH_APPLICATION_STARTING_SECTOR;
        bootloader_info.current_address = BOOTLOADER_APPLICATION_ADDRESS;
        bootloader_info.current_fwsize = BOOTSETTINGS_GetAppLength();
        bootloader_info.current_version = BOOTSETTINGS_GetAppVersion();
        bootloader_info.erase_sector_size = FLASH_APPLICATION_SECTOR_SIZE;
        max_size = FLASH_APPLICATION_SIZE_MAX;
        new_bootmode = BootloaderMode_MB_Application;
        is_reboot = TRUE;
        break;
    case FIRMWARE_FLAGS_MAIN_BOOTLOADER:
        bootloader_info.current_sector = FLASH_MAIN_BOOT_STARTING_SECTOR;
        bootloader_info.current_address = BOOTLOADER_MAIN_FLASH_ADDRESS;
        bootloader_info.current_fwsize = BOOTLOADER_MAIN_SIZE;
        bootloader_info.current_version = BOOTSETTINGS_GetMainBootVersion();
        bootloader_info.erase_sector_size = FLASH_MAIN_BOOT_SECTOR_SIZE;
        max_size = BOOTLOADER_MAIN_SIZE;
        new_bootmode = BootloaderMode_MB_Main;
#if __FAILSAFE_BOOTLOADER__
        //if in failsafe attempting to update mainboot, however, mainboot is
        //not needed to update, will need a reboot
        is_reboot = TRUE;
#endif
        break;
    default:
        max_size = 0;
        log_push_error_point(0xBBB5);
        return S_ERROR;
    }
    *sector_size = bootloader_info.erase_sector_size;

    if (fwheader.firmware_size > max_size || fwheader.firmware_size == 0 ||
        (fwheader.firmware_size % 8))
    {
        return S_NOTFIT;
    }

    //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    // Check flash content
    //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    crc32e_reset();
    calc_crc32e = 0xFFFFFFFF;
    bytecount = 0;
    while(bytecount < fwheader.firmware_size)
    {
        if (bytecount + FLASHCHECK_BLOCKSIZE <= fwheader.firmware_size)
        {
            length = FLASHCHECK_BLOCKSIZE;
        }
        else
        {
            length = fwheader.firmware_size - bytecount;
        }
        memcpy((char*)tmpbuffer,
               (char*)bootloader_info.current_address,length);
        crypto_blowfish_encryptblock_critical(tmpbuffer,length);
        calc_crc32e = crc32e_calculateblock(calc_crc32e,
                                            (u32*)tmpbuffer,length/4);
        bootloader_info.current_address += length;
        bytecount += length;
    }
    if (calc_crc32e != fwheader.firmware_crc32e ||
        bootloader_info.current_fwsize != fwheader.firmware_size)
    {
        is_content_different = TRUE;
    }

    //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    // Check version
    //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    //version is in format: 11.22.333.444
    //11:market, 22:hardware, 333:main_version, 444:build_version
    //only use 333.444 for firmware version comparison
    if ((bootloader_info.current_version % 1000000) > (fwheader.firmware_version  % 1000000))
    {
        //attempt to backdate firmware
        if (is_backdating_allowed == FALSE &&
            bootloader_info.current_version != 0xFFFFFFFF)
        {
            log_push_error_point(0xBBB6);
            return S_INVALID_VERSION;
        }
        is_update_required = TRUE;
    }
    else if ((bootloader_info.current_version % 1000000) == (fwheader.firmware_version  % 1000000))
    {
        //device has the same firmware version
        if (fwheader.firmware_flags & FIRMWARE_FLAGS_FORCE_UPDATE ||
            is_content_different == TRUE)
        {
            is_update_required = TRUE;
        }
    }
    else
    {
        is_update_required = TRUE;
    }
    
    if (is_update_required)
    {
        bootloader_info.firmware_flags = fwheader.firmware_flags;
        bootloader_info.firmware_version = fwheader.firmware_version;
        bootloader_info.firmware_size = fwheader.firmware_size;
        bootloader_info.firmware_crc32e = fwheader.firmware_crc32e;
        bootloader_info.status.init = 1;

        return S_SUCCESS;
    }
    else
    {
        u8  status;

        if (is_reboot)
        {
            status = S_SUCCESS;
            if (new_bootmode != BOOTSETTINGS_GetBootMode())
            {
                BOOTSETTINGS_SetBootMode(new_bootmode);
                status = bootsettings_update();
            }
            if (status == S_SUCCESS)
            {
                watchdog_set();
                return S_NO_UPDATE; //not a failure, already has latest
            }
            return status;
        }
        else
        {
            return S_NO_UPDATE;     //not a failure, already has latest
        }
    }
}

//------------------------------------------------------------------------------
// Write bootloader data to flash
// Inputs:  u8  *data (encrypted)
//          u32 datalength (must be multiple of 8)
// Return:  u8  status
// Engineer: Quyen Leba
//------------------------------------------------------------------------------
u8 bootloader_write_flash(u8 *data, u32 datalength)
{
    u32 datacount;
    u8  status;

    if (bootloader_info.status.init == 0)
    {
        log_push_error_point(0xBBC0);
        return S_ERROR;
    }

    if ((bootloader_info.bytecheck > bootloader_info.erase_sector_size) ||
        (bootloader_info.bytecount + datalength) > bootloader_info.firmware_size)
    {
        log_push_error_point(0xBBC1);
        return S_ERROR;
    }

    datacount = 0;
    while(datacount < datalength)
    {
        u32 byteavailable_currentsector;
        u32 byteremain_toprocess;
        u16 bytetoprogram;

        byteavailable_currentsector = bootloader_info.erase_sector_size - bootloader_info.bytecheck;
        byteremain_toprocess = datalength - datacount;
        if (byteavailable_currentsector == 0)
        {
            bootloader_info.current_sector++;
            if (flash_erase_sector(bootloader_info.current_sector) != S_SUCCESS)
            {
                log_push_error_point(0xBBC2);
                goto bootloader_write_flash_failed;
            }
            bootloader_info.bytecheck = 0;
            byteavailable_currentsector = bootloader_info.erase_sector_size;
        }
        else if (byteavailable_currentsector >= bootloader_info.erase_sector_size)
        {
            if (flash_erase_sector(bootloader_info.current_sector) != S_SUCCESS)
            {
                log_push_error_point(0xBBC3);
                goto bootloader_write_flash_failed;
            }
            bootloader_info.bytecheck = 0;
            byteavailable_currentsector = bootloader_info.erase_sector_size;
        }

        if (byteremain_toprocess >= byteavailable_currentsector)
        {
            bytetoprogram = byteavailable_currentsector;
        }
        else
        {
            bytetoprogram = byteremain_toprocess;
        }

        status = flash_write_sector_at_offset(bootloader_info.current_sector,
                                              bootloader_info.bytecheck,
                                              bootloader_info.erase_sector_size,
                                              &data[datacount],bytetoprogram,TRUE);
        if (status != S_SUCCESS)
        {
            log_push_error_point(0xBBC4);
            goto bootloader_write_flash_failed;
        }

        datacount += bytetoprogram;
        bootloader_info.bytecount += bytetoprogram;
        bootloader_info.bytecheck += bytetoprogram;
    }
    
    return S_SUCCESS;

bootloader_write_flash_failed:
    bootloader_info.status.init = 0;
    return S_FAIL;
}

//------------------------------------------------------------------------------
// Validate bootloader
// Inputs:  u32 crc32e
// Return:  u8  status
// Engineer: Quyen Leba
//------------------------------------------------------------------------------
u8 bootloader_validate()
{
    u32 dummy_u32;
    u32 calc_crc32e;
    u32 calc_crc32e_decrypted;
    u32 starting_address;
    u32 *target_crc32e;
    u32 *target_length;
    u32 *target_fwversion;
    u32 old_crc32e;
    u32 old_length;
    u32 old_fwversion;
    u8  *flash_bptr;
    BootloaderMode new_bootmode;
    u8  tmpbuffer[FLASHCHECK_BLOCKSIZE];
    u32 length;
    u32 i;

    if (bootloader_info.status.init == 0)
    {
        log_push_error_point(0xBBD0);
        return S_ERROR;
    }
    bootloader_info.status.init = 0;

    switch(bootloader_info.firmware_flags & 0x07)
    {
    case FIRMWARE_FLAGS_APPLICATION:
        old_crc32e = BOOTSETTINGS_GetAppCRC32E();
        old_length = BOOTSETTINGS_GetAppLength();
        old_fwversion = BOOTSETTINGS_GetAppVersion();

        target_crc32e = &BOOTSETTINGS_GetAppCRC32E();
        target_length = &BOOTSETTINGS_GetAppLength();
        target_fwversion = &BOOTSETTINGS_GetAppVersion();
        starting_address = BOOTLOADER_APPLICATION_ADDRESS;
        new_bootmode = BootloaderMode_MB_Application;
        if (bootloader_info.firmware_size <= BOOTLOADER_APPLICATION_SIZE_MAX)
        {
            *target_length = bootloader_info.firmware_size;
        }
        else
        {
            log_push_error_point(0xBBD1);
            return S_BADCONTENT;
        }
        break;
    case FIRMWARE_FLAGS_MAIN_BOOTLOADER:
        old_crc32e = BOOTSETTINGS_GetMainBootCRC32E();
        old_length = BOOTLOADER_MAIN_SIZE;
        old_fwversion = BOOTSETTINGS_GetMainBootVersion();

        target_crc32e = &BOOTSETTINGS_GetMainBootCRC32E();
        target_length = &dummy_u32;
        target_fwversion = &BOOTSETTINGS_GetMainBootVersion();
        starting_address = BOOTLOADER_MAIN_FLASH_ADDRESS;
        new_bootmode = BootloaderMode_MB_Main;
        *target_length = BOOTLOADER_MAIN_SIZE;
        break;
    default:
        log_push_error_point(0xBBD2);
        return S_BADCONTENT;
    }

    flash_bptr = (u8*)starting_address;
    //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    //calculate crc32e on encrypted data
    //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    crc32e_reset();
    calc_crc32e = 0xFFFFFFFF;
    i = 0;
    while(i < *target_length)
    {
        length = *target_length - i;
        if (length > FLASHCHECK_BLOCKSIZE)
        {
            length = FLASHCHECK_BLOCKSIZE;
        }
        memcpy((char*)tmpbuffer,(char*)&flash_bptr[i],length);
        crypto_blowfish_encryptblock_critical(tmpbuffer,length);
        calc_crc32e = crc32e_calculateblock(calc_crc32e,
                                            (u32*)tmpbuffer,length/4);
        i += length;
    }
    //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    //calculate crc32e on decrypted data
    //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    crc32e_reset();
    calc_crc32e_decrypted = 0xFFFFFFFF;
    i = 0;
    while(i < *target_length)
    {
        length = *target_length - i;
        if (length > FLASHCHECK_BLOCKSIZE)
        {
            length = FLASHCHECK_BLOCKSIZE;
        }
        memcpy((char*)tmpbuffer,(char*)&flash_bptr[i],length);
        calc_crc32e_decrypted = crc32e_calculateblock(calc_crc32e_decrypted,
                                                      (u32*)tmpbuffer,length/4);
        i += length;
    }
    
    if (calc_crc32e == bootloader_info.firmware_crc32e)
    {
        u8  status;

        *target_crc32e = calc_crc32e_decrypted;
        *target_fwversion = bootloader_info.firmware_version;
        if (old_crc32e != calc_crc32e_decrypted || old_length != *target_length ||
            old_fwversion != bootloader_info.firmware_version ||
            new_bootmode != BOOTSETTINGS_GetBootMode())
        {
            BOOTSETTINGS_SetBootMode(new_bootmode);
            status = bootsettings_update();
        }
        else
        {
            status = S_SUCCESS;
        }

        return status;
    }
    else
    {
        *target_crc32e = old_crc32e;
        *target_length = old_length;
        *target_fwversion = old_fwversion;
        log_push_error_point(0xBBD3);
        return S_CRC32E;
    }
}

//------------------------------------------------------------------------------
// Clear engineering signature
// Return:  u8  status
// Engineer: Quyen Leba
// Note: just in case this signature is somehow set
//------------------------------------------------------------------------------
u8 bootloader_clearengineeringsignature()
{
    BOOTSETTINGS_ClearEngSign();
    return bootsettings_update();
}

//------------------------------------------------------------------------------
// Force a validation of application
// Return:  u8  status
// Engineer: Quyen Leba
//------------------------------------------------------------------------------
u8 bootloader_force_application_validation()
{
    u32 calc_crc32e;
    
    crc32e_reset();
    calc_crc32e = crc32e_calculateblock(0xFFFFFFFF,
                                        (u32*)BOOTLOADER_APPLICATION_ADDRESS,
                                        BOOTLOADER_APPLICATION_SIZE_MAX/4);
    if (BOOTSETTINGS_GetBootMode() != BootloaderMode_MB_Application ||
        BOOTSETTINGS_GetAppCRC32E() != calc_crc32e ||
        BOOTSETTINGS_GetAppLength() != BOOTLOADER_APPLICATION_SIZE_MAX)
    {
        BOOTSETTINGS_SetBootMode(BootloaderMode_MB_Application);
        BOOTSETTINGS_SetAppCRC32E(calc_crc32e);
        BOOTSETTINGS_SetAppLength(BOOTLOADER_APPLICATION_SIZE_MAX);
        return bootsettings_update();
    }
    return S_SUCCESS;
}

//------------------------------------------------------------------------------
// Force a validation of mainboot
// Return:  u8  status
// Engineer: Quyen Leba
//------------------------------------------------------------------------------
u8 bootloader_force_mainboot_validation()
{
    u32 calc_crc32e;
    
    crc32e_reset();
    calc_crc32e = crc32e_calculateblock(0xFFFFFFFF,
                                        (u32*)BOOTLOADER_MAIN_FLASH_ADDRESS,
                                        BOOTLOADER_MAIN_SIZE/4);
    if (BOOTSETTINGS_GetBootMode() != BootloaderMode_MB_Main ||
        BOOTSETTINGS_GetMainBootCRC32E() != calc_crc32e)
    {
        BOOTSETTINGS_SetBootMode(BootloaderMode_MB_Main);
        BOOTSETTINGS_SetMainBootCRC32E(calc_crc32e);
        return bootsettings_update();
    }
    return S_SUCCESS;
}
