/**
  **************** (C) COPYRIGHT 2011 SCT Performance, LLC *********************
  * File Name          : interrupt.c
  * Device / System    : SCT products using the "CommonCode" platform
  * Author             : Quyen Leba
  *
  * Version            : 1 
  * Date               : 05/02/2011
  * Description        : 
  *                    : 
  *
  *
  * History            : 05/02/2011 M. Davis
  *                    :   Add SCT Copyright header,
  *
  ******************************************************************************
  */

#include <misc.h>
#include <board/interrupt.h>
#include <board/bootloader.h>

#define AIRCR_VECTKEY_MASK    ((uint32_t)0x05FA0000)

//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
void interrupt_init()
{
#ifdef __FAILSAFE_BOOTLOADER__
#define NVIC_VECTOR_TABLE_ADDRESS       BOOTLOADER_FAILSAFE_ADDRESS
    SCB->VTOR = NVIC_VectTab_FLASH | (NVIC_VECTOR_TABLE_ADDRESS & (u32)0x1FFFFF80);
#else
#define NVIC_VECTOR_TABLE_ADDRESS       BOOTLOADER_MAIN_RAM_ADDRESS
    SCB->VTOR = NVIC_VectTab_RAM | (NVIC_VECTOR_TABLE_ADDRESS & (u32)0x1FFFFF80);
#endif

    //Group3: PreemptionPriority (0-7), SubPriority (0-1)
    SCB->AIRCR = AIRCR_VECTKEY_MASK | NVIC_PriorityGroup_3;

    //Configure the SysTick handler priority
    NVIC_SetPriority(SysTick_IRQn,0x0);

    //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    // Interrupt priorities
    // Systick:     0-0
    // UART1:       1-0
    // USB:         1-1, 2-0 & 2-1
    // UART3:       3-0
    // TIM2:        6-1
    // TIM3:        3-1
    // TIM5:        4-0
    // EXTI1:       4-1
    // Button:      6-0
    // EXTI2:       7-0
    // EXTI3:       7-1
    //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
}
