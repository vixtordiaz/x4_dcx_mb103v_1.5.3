/**
  **************** (C) COPYRIGHT 2011 SCT Performance, LLC *********************
  * File Name          : mblexec.c
  * Device / System    : SCT products using the "CommonCode" platform
  * Author             : Quyen Leba
  *
  * Version            : 1 
  * Date               : 05/02/2011
  * Description        : 
  *                    : 
  *
  *
  * History            : 05/02/2011 M. Davis
  *                    :   Add SCT Copyright header,
  *
  ******************************************************************************
  */

#include <string.h>
#include <board/genplatform.h>
#include <common/crypto_blowfish.h>
#include <common/tea.h>
#include <common/statuscode.h>
#include "mblif.h"
#include "mblexec.h"

#define SECURITY_SEEDKEY_SIZE       32

u8  seedkey[SECURITY_SEEDKEY_SIZE];

//------------------------------------------------------------------------------
// Private prototypes
//------------------------------------------------------------------------------
u8 mblexec_get_settings(u8 *opcodedata, u16 opcodedatalength,
                        u8 *returndata, u16 *returndatalength);
u8 mblexec_set_settings(u8 *opcodedata, u16 opcodedatalength);
u8 mblexec_erase_sector_to_vehicleboard(u8 *opcodedata, u16 opcodedatalength);
u8 mblexec_write_sector_to_vehicleboard(u8 *opcodedata, u16 opcodedatalength);
u8 mblexec_validate_flash_content(u8 *opcode_data, u16 opcode_datalength);
u8 mblexec_request_security_seed();
u8 mblexec_validate_security_key();

//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
void mblexec_init()
{
}

//------------------------------------------------------------------------------
// Sensitive EXEC commands required prior security unlock
// Return:  u8  status
// Engineer: Quyen Leba
//------------------------------------------------------------------------------
u8 mblexec_unlock_security()
{
    u8  status;

    status = mblexec_call(MBLEXEC_OPCODE_REQUEST_SECURITY_SEED,
                          NULL,NULL,NULL,NULL);
    if (status != S_SUCCESS)
    {
        return status;
    }
    status = mblexec_call(MBLEXEC_OPCODE_VALIDATE_SECURITY_KEY,
                          NULL,NULL,NULL,NULL);
    return status;
}

//------------------------------------------------------------------------------
// Lock security to protect sensitive EXEC commands
// Return:  u8  status
// Engineer: Quyen Leba
//------------------------------------------------------------------------------
u8 mblexec_lock_security()
{
    u8  status;

    status = mblexec_call(MBLEXEC_OPCODE_REQUEST_SECURITY_SEED,
                          NULL,NULL,NULL,NULL);
    return status;
}

//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
u8 mblexec_call(MBLEXEC_OPCODE opcode, u8 *opcodedata, u8 opcodedatalength,
                u8 *returndata, u16 *returndatalength)
{
    u8  status;

    switch(opcode)
    {
    //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    //
    //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    case MBLEXEC_OPCODE_GET_SETTINGS:
        status = mblexec_get_settings(opcodedata,opcodedatalength,
                                      returndata,returndatalength);
        break;
    case MBLEXEC_OPCODE_SET_SETTINGS:
        status = mblexec_set_settings(opcodedata,opcodedatalength);
        break;
    case MBLEXEC_OPCODE_ERASE_SECTOR:
        status = mblexec_erase_sector_to_vehicleboard(opcodedata,
                                                      opcodedatalength);
        break;
    case MBLEXEC_OPCODE_WRITE_SECTOR:
        status = mblexec_write_sector_to_vehicleboard(opcodedata,
                                                      opcodedatalength);
        break;
    case MBLEXEC_OPCODE_VALIDATE_FLASH_CONTENT:
        status = mblexec_validate_flash_content(opcodedata,opcodedatalength);
        break;
    //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    //
    //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    case MBLEXEC_OPCODE_REQUEST_SECURITY_SEED:
        status = mblexec_request_security_seed();
        break;
    case MBLEXEC_OPCODE_VALIDATE_SECURITY_KEY:
        status = mblexec_validate_security_key();
        break;
    default:
        return S_NOTSUPPORT;
    }
    
    return status;
}


//------------------------------------------------------------------------------
// Get settings from vehicle board
// Inputs:  u8  *opcodedata [2:setting opcode]
//          u16 opcodedatalength
// Outputs: u8  *returndata
//          u16 *returndatalength
// Return:  u8  status
// Engineer: Quyen Leba
//------------------------------------------------------------------------------
u8 mblexec_get_settings(u8 *opcodedata, u16 opcodedatalength,
                        u8 *returndata, u16 *returndatalength)
{
    MBLIF_CONTROL *pcontrol;
    u8  buffer[256];
    u16 length;
    u8  status;

    status = mblif_link(MBLIF_CMD_EXEC,
                        MBLEXEC_OPCODE_GET_SETTINGS,
                        opcodedata,&opcodedatalength);
    if (status != S_SUCCESS)
    {
        return status;
    }
    
    timeout_set(100);
    while(1)
    {
        status = mblif_link(MBLIF_CMD_GET_STATUS,0,buffer,&length);
        if (status == S_SUCCESS && length == 4)
        {
            pcontrol = (MBLIF_CONTROL*)&buffer;
            if (pcontrol->exec_done)
            {
                if (pcontrol->exec_ok)
                {
                    mblif_link(MBLIF_CMD_READ_BUFFER,0,
                               returndata,returndatalength);
                    if (*returndatalength == 0)
                    {
                        status = S_FAIL;
                    }
                }
                else
                {
                    status = pcontrol->status;
                }
                break;
            }
        }//if (status ...
        else if (timeout())
        {
            status = S_TIMEOUT;
            break;
        }
    }//while(1)...

    return status;
}

//------------------------------------------------------------------------------
// Set settings to vehicle board
// Inputs:  u8  *opcodedata [2:setting opcode][n:settings data]
//          u16 opcodedatalength
// Return:  u8  status
// Engineer: Quyen Leba
//------------------------------------------------------------------------------
u8 mblexec_set_settings(u8 *opcodedata, u16 opcodedatalength)
{
    MBLIF_CONTROL *pcontrol;
    u8  buffer[256];
    u16 length;
    u8  status;

    status = mblif_link(MBLIF_CMD_EXEC,
                        MBLEXEC_OPCODE_SET_SETTINGS,
                        opcodedata,&opcodedatalength);
    if (status != S_SUCCESS)
    {
        return status;
    }
    
    timeout_set(100);
    while(1)
    {
        status = mblif_link(MBLIF_CMD_GET_STATUS,0,buffer,&length);
        if (status == S_SUCCESS && length == 4)
        {
            pcontrol = (MBLIF_CONTROL*)&buffer;
            if (pcontrol->exec_done)
            {
                if (pcontrol->exec_ok)
                {
                    status = S_SUCCESS;
                }
                else
                {
                    status = pcontrol->status;
                }
                break;
            }
        }//if (status ...
        else if (timeout())
        {
            status = S_TIMEOUT;
            break;
        }
    }//while(1)...

    return status;
}

//------------------------------------------------------------------------------
// Erase a sector of VehicleBoard flash
// Inputs:  u8  *opcodedata [2:sector index]
//          u16 opcodedatalength
// Return:  u8  status
// Engineer: Quyen Leba
//------------------------------------------------------------------------------
u8 mblexec_erase_sector_to_vehicleboard(u8 *opcodedata, u16 opcodedatalength)
{
    MBLIF_CONTROL *pcontrol;
    u8  buffer[256];
    u16 length;
    u8  status;

    status = mblif_link(MBLIF_CMD_EXEC,
                        MBLEXEC_OPCODE_ERASE_SECTOR,
                        opcodedata,&opcodedatalength);
    if (status != S_SUCCESS)
    {
        return status;
    }

    timeout_set(300);
    while(1)
    {
        status = mblif_link(MBLIF_CMD_GET_STATUS,0,buffer,&length);
        if (status == S_SUCCESS && length == 4)
        {
            pcontrol = (MBLIF_CONTROL*)&buffer;
            if (pcontrol->exec_done)
            {
                if (pcontrol->exec_ok)
                {
                    status = S_SUCCESS;
                }
                else
                {
                    status = pcontrol->status;
                }
                break;
            }
        }//if (status ...
        else if (timeout())
        {
            status = S_TIMEOUT;
            break;
        }
    }//while(1)...

    return status;
}

//------------------------------------------------------------------------------
// Write a sector of VehicleBoard flash
// Inputs:  u8  *opcodedata [2:sector index]
//          u16 opcodedatalength
// Return:  u8  status
// Note: must do a MBLIF_CMD_WRITE_BUFFER first before exec this task
// Engineer: Quyen Leba
//------------------------------------------------------------------------------
u8 mblexec_write_sector_to_vehicleboard(u8 *opcodedata, u16 opcodedatalength)
{
    MBLIF_CONTROL *pcontrol;
    u8  buffer[256];
    u16 length;
    u8  status;

    status = mblif_link(MBLIF_CMD_EXEC,
                        MBLEXEC_OPCODE_WRITE_SECTOR,
                        opcodedata,&opcodedatalength);
    if (status != S_SUCCESS)
    {
        return status;
    }

    timeout_set(300);
    while(1)
    {
        status = mblif_link(MBLIF_CMD_GET_STATUS,0,buffer,&length);
        if (status == S_SUCCESS && length == 4)
        {
            pcontrol = (MBLIF_CONTROL*)&buffer;
            if (pcontrol->exec_done)
            {
                if (pcontrol->exec_ok)
                {
                    status = S_SUCCESS;
                }
                else
                {
                    status = pcontrol->status;
                }
                break;
            }
        }//if (status ...
        else if (timeout())
        {
            status = S_TIMEOUT;
            break;
        }
    }//while(1)...

    return status;
}

//------------------------------------------------------------------------------
// A helper of mblexec to issue an AIN request
// Inputs:  u8 *opcode_data
//              [2:sector index][2:reserved][4:length in byte][4:crc32e]
//          u16 opcode_datalength
// Output:  float *value
// Return:  u8  status
// Engineer: Quyen Leba
//------------------------------------------------------------------------------
u8 mblexec_validate_flash_content(u8 *opcode_data, u16 opcode_datalength)
{
    MBLIF_CONTROL *pcontrol;
    u8  buffer[256];
    u16 length;
    u8  status;

    status = mblif_link(MBLIF_CMD_EXEC,
                        MBLEXEC_OPCODE_VALIDATE_FLASH_CONTENT,
                        opcode_data,&opcode_datalength);
    if (status != S_SUCCESS)
    {
        return status;
    }

    timeout_set(300);
    while(1)
    {
        status = mblif_link(MBLIF_CMD_GET_STATUS,0,buffer,&length);
        if (status == S_SUCCESS && length == 4)
        {
            pcontrol = (MBLIF_CONTROL*)&buffer;
            if (pcontrol->exec_done)
            {
                if (pcontrol->exec_ok)
                {
                    status = S_SUCCESS;
                }
                else
                {
                    status = pcontrol->status;
                }
                break;
            }
        }//if (status ...
        else if (timeout())
        {
            status = S_TIMEOUT;
            break;
        }
    }//while(1)...

    return status;
}

//------------------------------------------------------------------------------
// 
// Return:  u8  status
// Engineer: Quyen Leba
//------------------------------------------------------------------------------
u8 mblexec_request_security_seed()
{
    MBLIF_CONTROL *pcontrol;
    u8  buffer[256];
    u16 length;
    u8  status;

    length = 0;
    status = mblif_link(MBLIF_CMD_EXEC,
                        MBLEXEC_OPCODE_REQUEST_SECURITY_SEED,
                        NULL,&length);
    if (status != S_SUCCESS)
    {
        return status;
    }

    timeout_set(100);
    while(1)
    {
        status = mblif_link(MBLIF_CMD_GET_STATUS,0,buffer,&length);
        if (status == S_SUCCESS && length == 4)
        {
            pcontrol = (MBLIF_CONTROL*)&buffer;
            if (pcontrol->exec_done)
            {
                if (pcontrol->exec_ok)
                {
                    mblif_link(MBLIF_CMD_READ_BUFFER,0,
                               buffer,&length);
                    if (length == SECURITY_SEEDKEY_SIZE)
                    {
                        crypto_blowfish_decryptblock_internal_key
                            (buffer,SECURITY_SEEDKEY_SIZE);
                        memcpy(seedkey,buffer,SECURITY_SEEDKEY_SIZE);
                    }
                    else
                    {
                        status = S_FAIL;
                    }
                }
                else
                {
                    status = pcontrol->status;
                }
                break;
            }
        }//if (status ...
        else if (timeout())
        {
            status = S_TIMEOUT;
            break;
        }
    }//while(1)...

    return status;
}

//------------------------------------------------------------------------------
// 
// Return:  u8  status
// Engineer: Quyen Leba
//------------------------------------------------------------------------------
u8 mblexec_validate_security_key()
{
    MBLIF_CONTROL *pcontrol;
    u8  buffer[256];
    u32 tea_key[4];
    u32 *wptr;
    u16 length;
    u32 i;
    u8  status;

    tea_key[0] = 0xA73C1823;
    tea_key[1] = 0xE81F1C05;
    tea_key[2] = 0x10E3A762;
    tea_key[3] = 0x9B64CE81;

    wptr = (u32*)seedkey;
    for(i=0;i<SECURITY_SEEDKEY_SIZE/8;i++)
    {
        tea_encryption(wptr,tea_key);
        wptr+=2;
    }
    crypto_blowfish_encryptblock_internal_key(seedkey,SECURITY_SEEDKEY_SIZE);

    length = SECURITY_SEEDKEY_SIZE;
    status = mblif_link(MBLIF_CMD_EXEC,
                        MBLEXEC_OPCODE_VALIDATE_SECURITY_KEY,
                        seedkey,&length);
    if (status != S_SUCCESS)
    {
        return status;
    }
    
    timeout_set(100);
    while(1)
    {
        status = mblif_link(MBLIF_CMD_GET_STATUS,0,buffer,&length);
        if (status == S_SUCCESS && length == 4)
        {
            pcontrol = (MBLIF_CONTROL*)&buffer;
            if (pcontrol->exec_done)
            {
                if (pcontrol->exec_ok)
                {
                    status = S_SUCCESS;
                }
                else
                {
                    status = pcontrol->status;
                }
                break;
            }
        }//if (status ...
        else if (timeout())
        {
            status = S_TIMEOUT;
            break;
        }
    }//while(1)...

    return status;
}
