/**
  **************** (C) COPYRIGHT 2011 SCT Performance, LLC *********************
  * File Name          : timer.c
  * Device / System    : SCT products using the "CommonCode" platform
  * Author             : Quyen Leba
  *
  * Version            : 1 
  * Date               : 05/02/2011
  * Description        : 
  *                    : 
  *
  *
  * History            : 05/02/2011 M. Davis
  *                    :   Add SCT Copyright header,
  *
  ******************************************************************************
  */

#include <arch/gentype.h>
#include <stm32f2xx_rcc.h>
#include <stm32f2xx_tim.h>
#include <board/timer.h>

TIMER_INFO timer_info;

//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
void timer2_init()
{
    timer_info.board_timeout = 0;
    timer_info.board_current_ms = 0;
    timer_info.board_timeout_ms = 0xFFFFFFFF;

    RCC->APB1RSTR |= RCC_APB1Periph_TIM2;
    RCC->APB1RSTR &= ~RCC_APB1Periph_TIM2;

    //ClockDiv:0, CounterMode:Up; Time base configuration
    TIM2->CR1 = 0;
    // Set the Autoreload value
    TIM2->ARR = (100 - 1);
    // Set the Prescaler value
    TIM2->PSC = (720/2) - 1;
    // Generate an update event to reload the Prescaler & the Repetition counter
    // values immediately
    TIM2->EGR = TIM_PSCReloadMode_Immediate;

    //Disable Preload register
    TIM2->CR1 &= (uint16_t)~((uint16_t)TIM_CR1_ARPE);
    //Disable TIM_IT_Update
    TIM2->DIER &= (uint16_t)~TIM_IT_Update;
    //Disable timer
    TIM2->CR1 &= (uint16_t)(~((uint16_t)TIM_CR1_CEN));
}

//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
void timer2_set_timeout(u32 ms)
{
    timer_info.board_timeout = 0;
    timer_info.board_current_ms = 0;
    timer_info.board_timeout_ms = ms;

    // Enable Preload register
    TIM2->CR1 |= TIM_CR1_ARPE;
    // Enable TIM_IT_Update
    TIM2->DIER |= TIM_IT_Update;
    //Enable timer
    TIM2->CR1 |= TIM_CR1_CEN;
}

//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
void timer2_reset_timeout()
{
    timer_info.board_current_ms = 0;
    if (timer_info.board_timeout)
    {
        timer_info.board_timeout = 0;
        // Enable Preload register
        TIM2->CR1 |= TIM_CR1_ARPE;
        // Enable TIM_IT_Update
        TIM2->DIER |= TIM_IT_Update;
        //Enable timer
        TIM2->CR1 |= TIM_CR1_CEN;
    }
}

//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
void timer2_cancel_timeout()
{
    //Disable Preload register
    TIM2->CR1 &= (uint16_t)~((uint16_t)TIM_CR1_ARPE);
    //Disable TIM_IT_Update
    TIM2->DIER &= (uint16_t)~TIM_IT_Update;
    //Disable timer
    TIM2->CR1 &= (uint16_t)(~((uint16_t)TIM_CR1_CEN));
}

//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
void timer2_intr_handler()
{
    if ((timer_info.board_current_ms >= timer_info.board_timeout_ms) &&
        (timer_info.board_timeout == 0))
    {
        timer_info.board_timeout = 1;

        //Disable Preload register
        TIM2->CR1 &= (uint16_t)~((uint16_t)TIM_CR1_ARPE);
        //Disable TIM_IT_Update
        TIM2->DIER &= (uint16_t)~TIM_IT_Update;
        //Disable timer
        TIM2->CR1 &= (uint16_t)(~((uint16_t)TIM_CR1_CEN));
    }
    else
    {
        timer_info.board_current_ms++;
    }
}

//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
void timer4_init()
{
    timer_info.app_timeout = 0;
    timer_info.app_current_ms = 0;
    timer_info.app_timeout_ms = 0xFFFFFFFF;
    
    RCC->APB1RSTR |= RCC_APB1Periph_TIM4;
    RCC->APB1RSTR &= ~RCC_APB1Periph_TIM4;

    //ClockDiv:0, CounterMode:Up; Time base configuration
    TIM4->CR1 = 0;
    // Set the Autoreload value
    TIM4->ARR = (100 - 1);
    // Set the Prescaler value
    TIM4->PSC = (720/2) - 1;
    // Generate an update event to reload the Prescaler & the Repetition counter
    // values immediately
    TIM4->EGR = TIM_PSCReloadMode_Immediate;
    
    //Disable Preload register
    TIM4->CR1 &= (uint16_t)~((uint16_t)TIM_CR1_ARPE);
    //Disable TIM_IT_Update
    TIM4->DIER &= (uint16_t)~TIM_IT_Update;
    //Disable timer
    TIM4->CR1 &= (uint16_t)(~((uint16_t)TIM_CR1_CEN));
}

//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
void timer4_set_timeout(u32 ms)
{
    timer_info.app_timeout = 0;
    timer_info.app_current_ms = 0;
    timer_info.app_timeout_ms = ms;

    // Enable Preload register
    TIM4->CR1 |= TIM_CR1_ARPE;
    // Enable TIM_IT_Update
    TIM4->DIER |= TIM_IT_Update;
    //Enable timer
    TIM4->CR1 |= TIM_CR1_CEN;
}

//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
void timer4_reset_timeout()
{
    //timer4_set_timeout(timer_info.app_timeout_ms);
    timer_info.app_current_ms = 0;
    if (timer_info.app_timeout)
    {
        timer_info.app_timeout = 0;
        // Enable Preload register
        TIM4->CR1 |= TIM_CR1_ARPE;
        // Enable TIM_IT_Update
        TIM4->DIER |= TIM_IT_Update;
        //Enable timer
        TIM4->CR1 |= TIM_CR1_CEN;
    }
}

//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
void timer4_intr_handler()
{
    if ((timer_info.app_current_ms >= timer_info.app_timeout_ms) &&
        (timer_info.app_timeout == 0))
    {
        timer_info.app_timeout = 1;
        
        //Disable Preload register
        TIM4->CR1 &= (uint16_t)~((uint16_t)TIM_CR1_ARPE);
        //Disable TIM_IT_Update
        TIM4->DIER &= (uint16_t)~TIM_IT_Update;
        //Disable timer
        TIM4->CR1 &= (uint16_t)(~((uint16_t)TIM_CR1_CEN));
    }
    else
    {
        timer_info.app_current_ms++;
    }
}
