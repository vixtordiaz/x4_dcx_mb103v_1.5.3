/**
  **************** (C) COPYRIGHT 2011 SCT Performance, LLC *********************
  * File Name          : properties.c
  * Device / System    : SCT products using the "CommonCode" platform
  * Author             : Quyen Leba
  *
  * Version            : 1 
  * Date               : 05/02/2011
  * Description        : 
  *                    : 
  *
  *
  * History            : 05/02/2011 M. Davis
  *                    :   Add SCT Copyright header,
  *
  ******************************************************************************
  */

#include <string.h>
#include <common/version.h>
#include <common/bootsettings.h>
#include <common/statuscode.h>
#include "flash.h"
#include "gpio.h"
#include "properties.h"

typedef enum
{
    MediaType_NAND2k,
    MediaType_SD,
    MediaType_SDHC,
    MediaType_MMC,
    MediaType_Unknown,
}MediaType;

struct
{
    u32 operating_mode;
    u32 failsafeboot_version;
    u32 mainboot_version;
    u32 app_signature;
    u32 app_version;
    u16 code_sector_size;
    u8  board_rev;
    u8  secboot_startsector;
    u16 secboot_maxcodesize;
    u8  reserved1;
    u8  app_startsector;
    u32 app_maxcodesize;

    u8  media_type;         //used as MediaType
    u8  media_sector_size;  //in kbyte
    u8  reserved2[2];
    u32 media_size;         //in MByte

    u8  reserved3[8];
}properties_info = {
#if __FAILSAFE_BOOTLOADER__
    .operating_mode = FAILSAFE_BOOTLOADER_OPERATING_MODE,
    .failsafeboot_version = BOOTLOADER_VERSION,
    .mainboot_version = 0,
#elif __MAIN_BOOTLOADER__
    .operating_mode = MAIN_BOOTLOADER_OPERATING_MODE,
    .failsafeboot_version = 0,
    .mainboot_version = BOOTLOADER_VERSION,
#else
#error INVALID_BOOTLOADER   @ properties.c
#endif
    .code_sector_size = (FLASH_APPLICATION_SECTOR_SIZE/1024),
    .secboot_startsector = FLASH_MAIN_BOOT_STARTING_SECTOR,
    .secboot_maxcodesize = (FLASH_MAIN_BOOT_SIZE/1024),
    .app_startsector = FLASH_APPLICATION_STARTING_SECTOR,
    .app_maxcodesize = FLASH_APPLICATION_SIZE,
    
    .media_type = MediaType_Unknown,
    .media_size = 0,
    .media_sector_size = 0,
    
    .reserved1 = 0,
    .reserved2 = {0,0},
    .reserved3 = {0,0,0,0,0,0,0,0},
};

//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
void properties_init()
{
    properties_info.board_rev = gpio_get_board_rev();
    properties_info.app_version = BOOTSETTINGS_GetAppVersion();
    properties_info.app_signature = BOOTSETTINGS_GetAppSignature();

#if __FAILSAFE_BOOTLOADER__
    BOOTSETTINGS_GetMainBootVersion() = firmware_tag.version;
    properties_info.mainboot_version = BOOTSETTINGS_GetMainBootVersion();
#elif __MAIN_BOOTLOADER__
    BOOTSETTINGS_GetFailsafeBootVersion() = firmware_tag.version;
    properties_info.failsafeboot_version = BOOTSETTINGS_GetFailsafeBootVersion();
#endif    
}

//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
u8 properties_getinfo(u8 *info, u32 *info_length)
{
    memcpy((char*)info,(char*)&properties_info,sizeof(properties_info));
    *info_length = sizeof(properties_info);
    return S_SUCCESS;
}
