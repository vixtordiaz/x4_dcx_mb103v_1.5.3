/*-----------------------------------------------------------------------*/
/* Low level disk I/O module skeleton for Petit FatFs (C)ChaN, 2009      */
/*-----------------------------------------------------------------------*/

#include <board/MB103V/sdmmc.h>
#include "diskio.h"
#include "stdio.h"
#include "string.h"

/*-----------------------------------------------------------------------*/
/* Initialize Disk Drive                                                 */
/*-----------------------------------------------------------------------*/

DSTATUS disk_initialize (void)
{
	DSTATUS stat;

    sdmmc_init();
    
    stat = 0; // Success
    
//    pDiskCtrlBlk_t diskcontrolblock;
    
//    diskcontrolblock = (pDiskCtrlBlk_t)sdmmc_getdiskcontrolblock();
    
//    if(diskcontrolblock->DiskStatus == DiskCommandPass)
//        stat = 0;
//    else
//        stat = STA_NOINIT;

	return stat;
}



/*-----------------------------------------------------------------------*/
/* Read Partial Sector                                                   */
/*-----------------------------------------------------------------------*/

DRESULT disk_readp (
	BYTE* dest,			/* Pointer to the destination object */
	DWORD sector,		/* Sector number (LBA) */
	WORD sofs,			/* Offset in the sector */
	WORD count			/* Byte count (bit15:destination) */
)
{
//	DRESULT res;
    SdState_t status;
    BYTE sectordata[512];
    
    if((sofs+count) > sizeof(sectordata))
        return RES_PARERR;                
    
    status = sdmmc_blockread(sectordata, sector, 1);
    if(status == SdOk)
    {
        memcpy(dest, sectordata+sofs, count);
        return RES_OK;
    }

	return RES_ERROR;
}



/*-----------------------------------------------------------------------*/
/* Write Partial Sector                                                  */
/*-----------------------------------------------------------------------*/

DRESULT disk_writep (
	const BYTE* buff,		/* Pointer to the data to be written, NULL:Initiate/Finalize write operation */
	DWORD sc		/* Sector number (LBA) or Number of bytes to send */
)
{
/*
    static u32 sector;
	DRESULT res;
    DiskStatusCode_t status;

	if (!buff) 
    {
		if (sc) 
        {
            // Initiate write process
            sector = sc;
            res = RES_OK;
		}
        else 
        {
			// Finalize write process
            res = RES_OK;
		}
	} 
    else 
    {
		// Send data to the disk
        status = sdmmc_diskio((u8*)buff, sector, 1, DiskWrite);
        if(status == DiskCommandPass)
        {
            res = RES_OK;
        }
        else
        {
            res = RES_ERROR;            
        }
	}

	return res;
*/
    return RES_OK;
}

