/**
  **************** (C) COPYRIGHT 2011 SCT Performance, LLC *********************
  * File Name          : vbupdater.h
  * Device / System    : SCT products using the "CommonCode" platform
  * Author             : Quyen Leba
  *
  * Version            : 1 
  * Date               : 05/02/2011
  * Description        : 
  *                    : 
  *
  *
  * History            : 05/02/2011 M. Davis
  *                    :   Add SCT Copyright header,
  *
  ******************************************************************************
  */

#ifndef __VBUPDATER_H
#define __VBUPDATER_H

#include <arch/gentype.h>
#include <board/genplatform.h>

u8 vbupdater_init();
u8 vbupdater_reflash(u16 sector, u8 *data, u16 datalength);
u8 vbupdater_terminate(u32 mainboot_crc32e,
                       u32 application_crc32e, u16 application_length);

#endif //__VBUPDATER_H
