/**
  **************** (C) COPYRIGHT 2011 SCT Performance, LLC *********************
  * File Name          : usbcmdhandler.h
  * Device / System    : SCT products using the "CommonCode" platform
  * Author             : Quyen Leba
  *
  * Version            : 1 
  * Date               : 05/02/2011
  * Description        : 
  *                    : 
  *
  *
  * History            : 05/02/2011 M. Davis
  *                    :   Add SCT Copyright header,
  *
  ******************************************************************************
  */

#ifndef __USBCMDHANDLER_H
#define __USBCMDHANDLER_H

#include <arch/gentype.h>

#define SUPPORT_USBCMD_FILETRANSFER             0
#define SUPPORT_USBCMD_FULL_GETSETTINGS         0
#define SUPPORT_USBCMD_FULL_SETSETTINGS         0
#define SUPPORT_USBCMD_DATALOG                  0
#define SUPPORT_USBCMD_HARDWARE                 0

typedef enum
{
    UCH_USBCommand_Security                     = 0x10,
    UCH_USBCommand_Hardware                     = 0x11,

    UCH_USBCommand_OpenFile                     = 0x30,
    UCH_USBCommand_WriteFile                    = 0x31,
    UCH_USBCommand_ReadFile                     = 0x32,
    UCH_USBCommand_CloseFile                    = 0x33,

    UCH_USBCommand_Datalog                      = 0x50,
    UCH_USBCommand_GetSettings                  = 0x60,
    UCH_USBCommand_SetSettings                  = 0x61,

    UCH_USBCommand_Bootloader                   = 0xB3,
}UCH_USBCommand;

typedef enum
{
    USBCmdSecurity_Seed                         = 0x0032,
    USBCmdSecurity_Key                          = 0x00C2,
    USBCmdSecurity_Lock                         = 0x0082,
}USBCmdSecurityFlags;

typedef enum
{
    USBCmdBootloader_SetBoot                    = 0x0030,
    USBCmdBootloader_Init                       = 0x0031,
    USBCmdBootloader_Do                         = 0x0032,
    USBCmdBootloader_Validate                   = 0x0033,
    USBCmdBootloader_ClearEngSign               = 0x0034,
}USBCmdBootloaderFlags;

typedef enum
{
    //UCH_USBCommand_GetSettings & UCH_USBCommand_SetSettings
    USBCmdSettings_BootMode                     = 0x0040,
    USBCmdSettings_FailsafeBootVersion          = 0x0050,
    USBCmdSettings_MainBootVersion              = 0x0051,
    USBCmdSettings_AppVersion                   = 0x0052,
    USBCmdSettings_AppSignature                 = 0x0053,
    USBCmdSettings_SerialNumber                 = 0x0054,
    USBCmdSettings_FailsafeBootCRC32E           = 0x0055,
    USBCmdSettings_MainBootCRC32E               = 0x0056,
    USBCmdSettings_AppCRC32E                    = 0x0057,
    USBCmdSettings_CustomPID                    = 0x0058,
    USBCmdSettings_Flags                        = 0x0059,
    
    USBCmdSettings_DevicePartNumberString       = 0x0060,
    USBCmdSettings_DeviceType                   = 0x0061,
    USBCmdSettings_MarketType                   = 0x0062,
    USBCmdSettings_ProgrammerInfoP1             = 0x0063,
    USBCmdSettings_ProgrammerInfoP2             = 0x0064,
    USBCmdSettings_ProgrammerInfoP3             = 0x0065,
    USBCmdSettings_ProgrammerInfoP4             = 0x0066,
    
    USBCmdSettings_VIN                          = 0x0080,
    USBCmdSettings_PreviousVIN                  = 0x0081,
    USBCmdSettings_VID                          = 0x0082,
    USBCmdSettings_EPATS                        = 0x0083,
    USBCmdSettings_StockFilesize                = 0x0084,
    USBCmdSettings_VehicleCodes                 = 0x0085,
    USBCmdSettings_VehicleSerialNumber          = 0x0086,
    USBCmdSettings_TuneInfoTrack                = 0x0087,
    USBCmdSettings_TuneHistoryInfo              = 0x0088,
    
    //UCH_USBCommand_GetSettings only
    USBCmdSettings_Properties                   = 0x005A,
    USBCmdSettings_TuneRevision                 = 0x005B,
    USBCmdSettings_DeviceInfoString             = 0x005C,
    USBCmdSettings_ErrorPool                    = 0x005D,
    
    //UCH_USBCommand_SetSettings only
    USBCmdSettings_ResetFactoryDefault          = 0x00A1,
    USBCmdSettings_ReInit                       = 0x00A2,
    USBCmdSettings_EnableEngSign                = 0x00A3,
    USBCmdSettings_DisableEngSign               = 0x00A4,
}USBCmdSettingsFlags;

typedef enum
{
    USBCmdHardware_GetDeviceInfoString          = 0x0050,

    USBCmdHardware_GetInfo                      = 0x0070,
    USBCmdHardware_FTLInit                      = 0x0071,
    USBCmdHardware_FTLRead                      = 0x0072,
    USBCmdHardware_FTLWrite                     = 0x0073,
    USBCmdHardware_FTLDeinit                    = 0x0074,
    USBCmdHardware_FilesystemReinit             = 0x0075,
    USBCmdHardware_FormatMedia                  = 0x0076,
    USBCmdHardware_Listing                      = 0x0077,
    USBCmdHardware_GetFreeSpace                 = 0x0078,
    USBCmdHardware_GetCapacity                  = 0x0079,
    USBCmdHardware_GetFileSize                  = 0x007A,
    USBCmdHardware_DeleteFile                   = 0x007B,
    USBCmdHardware_RenameFile                   = 0x007C,
    USBCmdHardware_MoveFile                     = 0x007D,
    USBCmdHardware_TruncateFile                 = 0x007E,
    USBCmdHardware_CopyFile                     = 0x007F,
    USBCmdHardware_VerifyFile                   = 0x0080,

    USBCmdHardware_ShowMessage                  = 0x0090,
    USBCmdHardware_GetAnalogData                = 0x0091,
    USBCmdHardware_SetBaudrate                  = 0x0092,
    USBCmdHardware_SetVolume                    = 0x0093,
    USBCmdHardware_SetBrightness                = 0x0094,
    USBCmdHardware_Unpair                       = 0x0095,
    USBCmdHardware_GetFriendlyName              = 0x0096,
    USBCmdHardware_SetFriendlyName              = 0x0097,
    USBCmdHardware_GetMacAddress                = 0x0098,
    USBCmdHardware_SetMacAddress                = 0x0099,
}USBCmdHardwareFlags;

typedef enum
{
    USBCmdFile_None                             = 0x0000,
    USBCmdFile_Read                             = 0x0010,
    USBCmdFile_Write                            = 0x0011,
    USBCmdFile_Append                           = 0x0012,
}USBCmdFileFlags;

//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
void uch_security_seed();
void uch_security_key();
void uch_bootloader();
void uch_settings_get();
void uch_settings_set();
void uch_hardware();
u8 uch_openfile();
u8 uch_readfile();
u8 uch_writefile();
u8 uch_closefile();

#endif 	//__USBCMDHANDLER_H
