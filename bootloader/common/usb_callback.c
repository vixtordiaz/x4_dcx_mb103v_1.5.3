/**
  **************** (C) COPYRIGHT 2011 SCT Performance, LLC *********************
  * File Name          : usb_callback.c
  * Device / System    : SCT products using the "CommonCode" platform
  * Author             : Quyen Leba
  *
  * Version            : 1 
  * Date               : 05/02/2011
  * Description        : 
  *                    : 
  *
  *
  * History            : 05/02/2011 M. Davis
  *                    :   Add SCT Copyright header,
  *
  ******************************************************************************
  */

#include <string.h>
#include <stdlib.h>
#include <device_config.h>
#include <board/genplatform.h>
#include <board/timer.h>
#include <common/statuscode.h>
#include <common/usbcmdhandler.h>
#include "usb_callback.h"

#define USB_SECURITY_ENABLE         1

u8  usb_databuffer[4096];
u32 usb_databufferindex = 0;
u32 usb_databufferlength = 0;

USB_CALLBACK_INFO usb_callback_info =
{
    .control =
    {
        .unlocked       = 0,
        .validseed      = 0,
    },
    .flowcontrol        = CB_FC_IDLE,
    .bulkbufferlength   = 0,
    .cbw.datalength     = 0,
    .pdev               = NULL,
};

//------------------------------------------------------------------------------
// Private prototypes
//------------------------------------------------------------------------------
void usb_cbw_decode(void *pdev);
void usb_outbuffercomplete_action();

//------------------------------------------------------------------------------
// Input:   void *pdev (not used in MB103V)
//------------------------------------------------------------------------------
void usb_stall_all_ep(void *pdev)
{
    SetStallEPIN();
    SetStallEPOUT();
}

//------------------------------------------------------------------------------
// Input:   void *pdev (not used in MB103V)
//------------------------------------------------------------------------------
void usb_stall_out_ep(void *pdev)
{
    SetStallEPOUT();
}

//------------------------------------------------------------------------------
// Input:   void *pdev (not used in MB103V)
//------------------------------------------------------------------------------
void usb_stall_in_ep(void *pdev)
{
    SetStallEPIN();
}

//------------------------------------------------------------------------------
// Handle callback of EP1 IN
// Inputs:  void *pdev (not used in MB103V)
//          u8  epnum (not used in MB103V)
// Engineer: Quyen Leba
//------------------------------------------------------------------------------
void usb_callback_ep1in(void *pdev, u8 epnum)
{
    switch(usb_callback_info.flowcontrol)
    {
    case CB_FC_DATA_IN:
        if (usb_databufferlength <= USB_DATABUFFER_MAXSIZE &&
            usb_databufferlength != 0)
        {
            if ((usb_databufferindex + BULK_MAX_PACKET_SIZE) < usb_databufferlength)
            {
                SendDataEPIN(pdev,&usb_databuffer[usb_databufferindex],BULK_MAX_PACKET_SIZE);
                usb_databufferindex += BULK_MAX_PACKET_SIZE;
                SCTUSB_StatusResidue -= BULK_MAX_PACKET_SIZE;
                SetReadyEPIN();
            }
            else if (usb_databufferindex < usb_databufferlength)
            {
                u32 remaininglength;
                
                remaininglength = usb_databufferlength - usb_databufferindex;
                //send the last chunk of data
                SendDataEPIN(pdev,&usb_databuffer[usb_databufferindex],remaininglength);
                usb_databufferindex = 0;
                SCTUSB_StatusResidue -= remaininglength;
                SCTUSB_SetFlowControl(CB_FC_DATA_IN_LAST);
                SetReadyEPIN();
            }
            else
            {
                //this is error
                SCTUSB_SetFlowControl(CB_FC_IDLE);
                //enable the Endpoint to recive the next cmd
                SetReadyEPOUT_NoDataPending();
            }
        }
        else
        {
            //this is error
            SCTUSB_SetFlowControl(CB_FC_IDLE);
            //enable the Endpoint to recive the next cmd
            SetReadyEPOUT_NoDataPending();
        }
        break;
    case CB_FC_STATUS_SEND:
    case CB_FC_ERROR:
        SCTUSB_SetFlowControl(CB_FC_IDLE);
        //enable the Endpoint to recive the next cmd
        SetReadyEPOUT_NoDataPending();
        break;
    case CB_FC_DATA_IN_LAST:
        usb_set_csw(pdev,SCTUSB_GetCmdStatus(),TRUE);
        SetReadyEPOUT_NoDataPending();
        break;
    default:
        break;
    }
}

//------------------------------------------------------------------------------
// Handle callback of EP2 OUT
// Inputs:  void *pdev (not used in MB103V)
//          u8  epnum (not used in MB103V)
// Engineer: Quyen Leba
//------------------------------------------------------------------------------
void usb_callback_ep2out(void *pdev, u8 epnum)
{
    GetEPOUTData();

    switch(SCTUSB_GetFlowControl())
    {
    case CB_FC_IDLE:
        usb_cbw_decode(pdev);
        break;
    case CB_FC_DATA_OUT:
        if (usb_databufferindex + usb_callback_info.bulkbufferlength > USB_DATABUFFER_MAXSIZE)
        {
            usb_stall_all_ep(pdev);
            usb_set_csw(pdev,S_USB_FLOWCONTROL,FALSE);
        }
        else if ((usb_databufferindex + usb_callback_info.bulkbufferlength) < usb_callback_info.cbw.datalength)
        {
            memcpy((char*)&usb_databuffer[usb_databufferindex],
                   (char*)usb_callback_info.bulkbuffer,
                   usb_callback_info.bulkbufferlength);
            usb_databufferindex += usb_callback_info.bulkbufferlength;
            SCTUSB_StatusResidue -= usb_callback_info.bulkbufferlength;
            //TODOQ: check me
            SetReadyEPOUT_DataPending();
//$$$            SetEPRxValid(ENDP2);
        }
        else if ((usb_databufferindex + usb_callback_info.bulkbufferlength) >= usb_callback_info.cbw.datalength)
        {
            //last chunk of data
            memcpy((char*)&usb_databuffer[usb_databufferindex],
                   (char*)usb_callback_info.bulkbuffer,
                   usb_callback_info.bulkbufferlength);
            usb_databufferindex += usb_callback_info.bulkbufferlength;
            SCTUSB_StatusResidue -= usb_callback_info.bulkbufferlength;

            usb_databufferlength = usb_databufferindex;
            //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
            //all data received, now process it
            usb_outbuffercomplete_action();
            //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

            //TODOQ: check me
            SetReadyEPOUT_NoDataPending();
//$$$            SetEPRxValid(ENDP2);
        }
        else
        {
            //never happen
        }
        break;
    default:
        usb_stall_all_ep(pdev);
        usb_set_csw(pdev,S_USB_FLOWCONTROL,FALSE);
        break;
    }
}

//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
void usb_cbw_decode(void *pdev)
{
    u8  status;

    status = S_USB_UNKNOWN;
    SCTUSB_SetCmdStatus(status);
    usb_databufferindex = 0;
    usb_databufferlength = 0;

    if (usb_callback_info.bulkbufferlength != CB_CBW_LENGTH)
    {
        usb_stall_all_ep(pdev);
        // reset the id to disable the clear feature until
        // receiving a SCTUSB_reset
        usb_callback_info.cbw.id = 0;
        usb_set_csw(pdev,S_USB_CBW_SIZE,FALSE);
        return;
    }

    memcpy((char*)&usb_callback_info.cbw,
           (char*)usb_callback_info.bulkbuffer,CB_CBW_LENGTH);
    SCTUSB_SetStatusTag(usb_callback_info.cbw.tag);
    SCTUSB_SetStatusPrivData(0);
    SCTUSB_SetStatusResidue(usb_callback_info.cbw.datalength);
    usb_callback_info.pdev = pdev;

    if (usb_callback_info.cbw.id == CB_CBW_ID)
    {
#if USB_SECURITY_ENABLE
        if (SCTUSB_isSecurityLocked())
        {
            bool isAllowed = FALSE;

            switch((UCH_USBCommand)usb_callback_info.cbw.cmd)
            {
            case UCH_USBCommand_Security:
                isAllowed = TRUE;
                break;
            case UCH_USBCommand_GetSettings:
                switch(usb_callback_info.cbw.flags)
                {
                case USBCmdSettings_FailsafeBootVersion:
                case USBCmdSettings_MainBootVersion:
                case USBCmdSettings_AppVersion:
                case USBCmdSettings_SerialNumber:
                case USBCmdSettings_DevicePartNumberString:
                case USBCmdSettings_DeviceType:
                case USBCmdSettings_MarketType:
                case USBCmdSettings_Properties:
                    isAllowed = TRUE;
                    break;
                default:
                    //do nothing
                    break;
                }
                break;
            //other commands are always blocked
            default:
                //do nothing
                break;
            }
            if (!isAllowed)
            {
                usb_set_csw(pdev,S_USB_SECURITY,TRUE);
                return;
            }
        }
#endif

        if (usb_callback_info.cbw.datalength > USB_DATABUFFER_MAXSIZE)
        {
            usb_set_csw(pdev,S_USB_CBW_DATALENGTH,TRUE);
            return;
        }
        
        switch((UCH_USBCommand)usb_callback_info.cbw.cmd)
        {
        //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
        // File transfer
        //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
#if SUPPORT_USBCMD_FILETRANSFER
        case UCH_USBCommand_ReadFile:
            uch_readfile();
            break;
        case UCH_USBCommand_WriteFile:
        case UCH_USBCommand_OpenFile:
            SCTUSB_SetFlowControl(CB_FC_DATA_OUT);
            SetReadyEPOUT_DataPending();
            //handled later; when received all data
            break;
        case UCH_USBCommand_CloseFile:
            status = uch_closefile();
            usb_set_csw(pdev,status,TRUE);
            SetReadyEPOUT_NoDataPending();
            break;
#endif  //SUPPORT_USBCMD_FILETRANSFER
        //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
        // Security
        //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
        case UCH_USBCommand_Security:
            SCTUSB_LockSecurity();
            switch((USBCmdSecurityFlags)usb_callback_info.cbw.flags)
            {
            case USBCmdSecurity_Seed:
                uch_security_seed();
                break;
            case USBCmdSecurity_Key:
                SCTUSB_SetFlowControl(CB_FC_DATA_OUT);
                SetReadyEPOUT_DataPending();
                //handled later; when received all data
                break;
            case USBCmdSecurity_Lock:
                usb_set_csw(pdev,S_SUCCESS,TRUE);
                SetReadyEPOUT_NoDataPending();
                break;
            default:
                usb_set_csw(pdev,S_USB_UNKNOWN_FLAGS,TRUE);
                SetReadyEPOUT_NoDataPending();
                break;
            }
            break;
        //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
        // Hardware
        //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
#if SUPPORT_USBCMD_HARDWARE
        case UCH_USBCommand_Hardware:
            switch((USBCmdHardwareFlags)usb_callback_info.cbw.flags)
            {
            case USBCmdHardware_GetDeviceInfoString:
            case USBCmdHardware_GetInfo:
            case USBCmdHardware_FTLInit:
            case USBCmdHardware_FTLRead:
            case USBCmdHardware_FTLDeinit:
            case USBCmdHardware_FilesystemReinit:
            case USBCmdHardware_FormatMedia:
            case USBCmdHardware_Listing:
            case USBCmdHardware_GetFreeSpace:
            case USBCmdHardware_GetCapacity:
            case USBCmdHardware_GetAnalogData:
            case USBCmdHardware_SetBaudrate:
            case USBCmdHardware_SetVolume:
            case USBCmdHardware_SetBrightness:
                //commands without incoming data
                uch_hardware();
                break;
            case USBCmdHardware_FTLWrite:
            case USBCmdHardware_GetFileSize:
            case USBCmdHardware_DeleteFile:
            case USBCmdHardware_RenameFile:
            case USBCmdHardware_MoveFile:
            case USBCmdHardware_TruncateFile:
            case USBCmdHardware_CopyFile:
            case USBCmdHardware_VerifyFile:
            case USBCmdHardware_ShowMessage:
                //commands have incoming data
                SCTUSB_SetFlowControl(CB_FC_DATA_OUT);
                SetReadyEPOUT_DataPending();
                //handled later; when received all data
                break;
            default:
                usb_set_csw(pdev,S_USB_UNKNOWN_FLAGS,TRUE);
                SetReadyEPOUT_NoDataPending();
                break;
            }
            break;
#endif  //SUPPORT_USBCMD_HARDWARE
        //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
        //
        //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
        case UCH_USBCommand_Datalog:
            usb_set_csw(pdev,S_USB_UNSUPPORTED_CMD,TRUE);
            SetReadyEPOUT_NoDataPending();
            break;
        //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
        // Settings
        //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
        case UCH_USBCommand_GetSettings:
            uch_settings_get();
            break;
        case UCH_USBCommand_SetSettings:
            SCTUSB_SetFlowControl(CB_FC_DATA_OUT);
            SetReadyEPOUT_DataPending();
            //handled later; when received all data
            break;
        //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
        // Bootloader
        //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
        case UCH_USBCommand_Bootloader:
            switch((USBCmdBootloaderFlags)usb_callback_info.cbw.flags)
            {
            case USBCmdBootloader_SetBoot:
            case USBCmdBootloader_Init:
            case USBCmdBootloader_Do:
            case 0x3F:
                SCTUSB_SetFlowControl(CB_FC_DATA_OUT);
                SetReadyEPOUT_DataPending();
                //handled later; when received all data
                break;
            case USBCmdBootloader_Validate:
                uch_bootloader();
                break;
            default:
                usb_set_csw(pdev,S_USB_UNKNOWN_FLAGS,TRUE);
                SetReadyEPOUT_NoDataPending();
                break;
            }
            break;
        //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
        //
        //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
        default:
            usb_set_csw(pdev,S_USB_UNKNOWN_CMD,TRUE);
            SetReadyEPOUT_NoDataPending();
            break;
        }//switch((UCH_USBCommand)...
    }
    else
    {
        // invalid CBW
        usb_stall_all_ep(pdev);
        usb_set_csw(pdev,S_USB_CSW_ID,FALSE);
    }
}

//------------------------------------------------------------------------------
// Action when OUT buffer completely received
// Engineer: Quyen Leba
//------------------------------------------------------------------------------
void usb_outbuffercomplete_action()
{
    switch((UCH_USBCommand)SCTUSB_GetRequestedCmd())
    {
#if SUPPORT_USBCMD_FILETRANSFER
    case UCH_USBCommand_WriteFile:
        uch_writefile();
        break;
    case UCH_USBCommand_OpenFile:
        uch_openfile();
        break;
#endif  //SUPPORT_USBCMD_FILETRANSFER
    case UCH_USBCommand_Security:
        uch_security_key();
        break;
#if SUPPORT_USBCMD_HARDWARE
    case UCH_USBCommand_Hardware:
        uch_hardware();
        break;
#endif  //SUPPORT_USBCMD_HARDWARE
#if SUPPORT_USBCMD_DATALOG
    case UCH_USBCommand_Datalog:
        break;
#endif  //SUPPORT_USBCMD_DATALOG
    case UCH_USBCommand_SetSettings:
        uch_settings_set();
        break;
    case UCH_USBCommand_Bootloader:
        uch_bootloader();
        break;
    }
}
