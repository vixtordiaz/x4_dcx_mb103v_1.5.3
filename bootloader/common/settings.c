/**
  **************** (C) COPYRIGHT 2011 SCT Performance, LLC *********************
  * File Name          : settings.c
  * Device / System    : SCT products using the "CommonCode" platform
  * Author             : Quyen Leba
  *
  * Version            : 1 
  * Date               : 05/02/2011
  * Description        : 
  *                    : 
  *
  *
  * History            : 05/02/2011 M. Davis
  *                    :   Add SCT Copyright header,
  *
  ******************************************************************************
  */

#include <ctype.h>
#include <string.h>
#include <board/genplatform.h>
#include <common/statuscode.h>
#include "settings.h"

Settings_Critical settingscritical;

//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
u8 settings_load()
{
    flash_load_setting(CriticalArea,
                       (u8*)&settingscritical, sizeof(settingscritical));
    return S_SUCCESS;
}

//-----------------------------------------------------------------------------
// Get settings by opcode
// Input:   SETTINGS_OPCODE opcode
//          u16 privdata (specific to opcode)
// Outputs: u8  *settings_data
//          u32 settings_len
// Return:  u8  status
// Engineer: Quyen Leba
//-----------------------------------------------------------------------------
u8 settings_getsettings_byopcode(SETTINGS_OPCODE opcode, u16 privdata,
                                  u8 *settings_data, u32 *settings_len)
{
    u32 status;
    
    status = S_SUCCESS;
    *settings_len = 0;
    
    switch(opcode)
    {
    case SETTINGS_OPCODE_VERSION:
//        version_getfirmwareversionstring(settings_data);
//        *settings_len = strlen((void*)settings_data)+1;
        status = S_SERVICENOTSUPPORTED;
        break;
    case SETTINGS_OPCODE_SERIAL:
        strncpy((char*)settings_data,
                (char*)SETTINGS_CRITICAL(serialnumber),13+3);
        settings_data[16] = NULL;
        *settings_len = 13+3+1;
        break;
    case SETTINGS_OPCODE_DEVICE_INFO:
        memcpy((char*)&settings_data[0],
               (char*)&SETTINGS_CRITICAL(firmware_version),4);
        memcpy((char*)&settings_data[4],
               (char*)&SETTINGS_CRITICAL(devicetype),4);
        memcpy((char*)&settings_data[8],
               (char*)&SETTINGS_CRITICAL(markettype),4);
        memcpy((char*)&settings_data[12],
               (char*)&SETTINGS_CRITICAL(baudrate),4);
        *settings_len = 16;
        break;
    case SETTINGS_OPCODE_DEVICE_PARTNUMBER_STRING:
        strcpy((char*)settings_data,"N/A");
        if (isgraph(SETTINGS_CRITICAL(devicepartnumberstring)[0]))
        {
            if (strlen((char*)SETTINGS_CRITICAL(devicepartnumberstring)) <
                sizeof(SETTINGS_CRITICAL(devicepartnumberstring)))
            {
                strcpy((char*)settings_data,
                       (char*)SETTINGS_CRITICAL(devicepartnumberstring));
            }
        }
        *settings_len = strlen((char*)settings_data)+1;
        break;
    case SETTINGS_OPCODE_DEVICE_PROPERTIES:
        status = properties_getinfo(settings_data,settings_len);
        break;
    default:
        status = S_SERVICENOTSUPPORTED;
        break;
    }
    
    return status;
}

//------------------------------------------------------------------------------
// Get Device Serial Number
// Output:  u8  *serialnumber
//              (must be able to store DEVICE_SERIAL_NUMBER_LENGTH+1 bytes)
// Return   u8  status
// Engineer: Patrick Downs, Quyen Leba
//------------------------------------------------------------------------------
u8 settings_get_deviceserialnumber(u8 *serialnumber)
{
    u8  i;
    
    for(i=0;i<DEVICE_SERIAL_NUMBER_LENGTH;i++)
    {
        //isalnum
        if ((SETTINGS_CRITICAL(serialnumber)[i] >= '0' &&
             SETTINGS_CRITICAL(serialnumber)[i] <= '9') ||
            (SETTINGS_CRITICAL(serialnumber)[i] >= 'A' &&
             SETTINGS_CRITICAL(serialnumber)[i] <= 'Z'))
        {
            //do nothing
        }
        else
        {
            strcpy((char*)serialnumber,"N/A");
            return S_BADCONTENT;
        }
    }

    memcpy((char*)serialnumber,(char*)SETTINGS_CRITICAL(serialnumber),
           DEVICE_SERIAL_NUMBER_LENGTH);
    serialnumber[DEVICE_SERIAL_NUMBER_LENGTH] = NULL;
    return S_SUCCESS;
}
