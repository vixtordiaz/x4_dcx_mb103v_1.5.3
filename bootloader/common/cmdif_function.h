/**
  **************** (C) COPYRIGHT 2011 SCT Performance, LLC *********************
  * File Name          : cmdif_function.h
  * Device / System    : SCT products using the "CommonCode" platform
  * Author             : Quyen Leba
  *
  * Version            : 1 
  * Date               : 05/02/2011
  * Description        : 
  *                    : 
  *
  *
  * History            : 05/02/2011 M. Davis
  *                    :   Add SCT Copyright header,
  *
  ******************************************************************************
  */

#ifndef __CMDIF_FUNCTION_H
#define __CMDIF_FUNCTION_H

#include <arch/gentype.h>

u8 cmdif_func_get_device_info();
u8 cmdif_func_get_device_data(u8 dataopcode);
u8 cmdif_func_get_dongle_settings(u8 settings_opcode);
u8 cmdif_func_enter_bootloader(u8 boottype);
u8 cmdif_func_ping_response();
u8 cmdif_func_unsupported_command();
u8 cmdif_func_bootloader_setboot(BootloaderMode bootmode);
u8 cmdif_func_bootloader_init(u8 *encryptedheader);
u8 cmdif_func_bootloader_do(u8 *encrypteddata, u32 length);
u8 cmdif_func_bootloader_validate();
u8 cmdif_func_bootloader_setboot(u16 boot_type);

#endif	//__CMDIF_FUNCTION_H
