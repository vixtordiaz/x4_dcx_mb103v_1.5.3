/**
  **************** (C) COPYRIGHT 2011 SCT Performance, LLC *********************
  * File Name          : bootsettings.c
  * Device / System    : SCT products using the "CommonCode" platform
  * Author             : Quyen Leba
  *
  * Version            : 1 
  * Date               : 05/02/2011
  * Description        : 
  *                    : 
  *
  *
  * History            : 05/02/2011 M. Davis
  *                    :   Add SCT Copyright header,
  *
  ******************************************************************************
  */

#include <string.h>
#include <arch/gentype.h>
#include <board/genplatform.h>
#include <board/bootloader.h>
#include <common/crc32.h>
#include <common/statuscode.h>
#include "crypto_messageblock.h"
#include "bootsettings.h"
#include <common/version.h>

#define BOOTSETTINGS_USE_ENCRYPTED_MESSAGE_BLOCK                1

BootSettings bootsettings;

//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
u8 bootsettings_load()
{
    flash_load_bootsettings((u8*)&bootsettings,sizeof(bootsettings));
    
#if __FAILSAFE_BOOTLOADER__
    if(BOOTSETTINGS_GetFailsafeBootVersion() != BOOTLOADER_VERSION)
    {
        BOOTSETTINGS_SetFailsafeBootVersion(firmware_tag.version);
    }
#elif __MAIN_BOOTLOADER__
    if(BOOTSETTINGS_GetMainBootVersion() != BOOTLOADER_VERSION)
    {
        BOOTSETTINGS_SetMainBootVersion(firmware_tag.version);
    }
#endif
        
    return S_SUCCESS;
}

//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
u8 bootsettings_update()
{
    u8  flashbuffer[FLASHSETTINGS_MAX_SETTINGS_SIZE];
    u8  status;

    flash_load_bootsettings((u8*)flashbuffer,FLASHSETTINGS_MAX_SETTINGS_SIZE);
    memcpy((char*)flashbuffer,(char*)&bootsettings,sizeof(bootsettings));

    crc32e_reset();
    bootsettings.crc32e = 0xFFFFFFFF;
    bootsettings.crc32e = crc32e_calculateblock(0xFFFFFFFF,
                                                (u32*)flashbuffer,
                                                FLASHSETTINGS_MAX_SETTINGS_SIZE/4);
    memcpy((char*)flashbuffer,(char*)&bootsettings,sizeof(bootsettings));
    status = flash_store_bootsettings((u8*)flashbuffer, FLASHSETTINGS_MAX_SETTINGS_SIZE);
    return status;
}

//------------------------------------------------------------------------------
// Get bootsettings by opcode and pack it in encrypted message
// Input:   u16 opcode
// Outputs: u8  *settingsdata
//          u32 *settingsdata_length (max 56 bytes)
// Return:  u8  status
// Engineer: Quyen Leba
//------------------------------------------------------------------------------
u8 bootsettings_get(u16 opcode, u8 *settingsdata, u32 *settingsdata_length)
{
    u8  buffer[16];
    u8  *inputdata_ptr;
    u32 inputdata_length;
    u8  status;

    status = S_SUCCESS;
    memset(buffer,0,sizeof(buffer));

    switch((BootSettingsOpcode)opcode)
    {
    case BootSettingsOpcode_BootMode:
        inputdata_ptr = (u8*)&bootsettings.bootmode;
        inputdata_length = sizeof(bootsettings.bootmode);
        break;
    case BootSettingsOpcode_FailSafeBootVersion:
        inputdata_ptr = (u8*)&bootsettings.failsafeboot_version;
        inputdata_length = sizeof(bootsettings.failsafeboot_version);
        break;
    case BootSettingsOpcode_MainBootVersion:
        inputdata_ptr = (u8*)&bootsettings.mainboot_version;
        inputdata_length = sizeof(bootsettings.mainboot_version);
        break;
    case BootSettingsOpcode_AppVersion:
        inputdata_ptr = (u8*)&bootsettings.app_version;
        inputdata_length = sizeof(bootsettings.app_version);
        break;
    case BootSettingsOpcode_AppSignature:
        inputdata_ptr = (u8*)&bootsettings.app_signature;
        inputdata_length = sizeof(bootsettings.app_signature);
        break;
    case BootSettingsOpcode_FailSafeBootCRC32E:
        memcpy((char*)buffer,(char*)&bootsettings.failsafeboot_crc32e,4);
        inputdata_length = FLASH_FAILSAFE_BOOT_SIZE;
        memcpy((char*)&buffer[4],(char*)&inputdata_length,4);
        inputdata_ptr = buffer;
        inputdata_length = 8;
        break;
    case BootSettingsOpcode_MainBootCRC32E:
        memcpy((char*)buffer,(char*)&bootsettings.mainboot_crc32e,4);
        inputdata_length = FLASH_MAIN_BOOT_SIZE;
        memcpy((char*)&buffer[4],(char*)&inputdata_length,4);
        inputdata_ptr = buffer;
        inputdata_length = 8;
        break;
    case BootSettingsOpcode_AppCRC32E:
        memcpy((char*)buffer,(char*)&bootsettings.app_crc32e,4);
        inputdata_length = bootsettings.app_length;
        memcpy((char*)&buffer[4],(char*)&inputdata_length,4);
        inputdata_ptr = buffer;
        inputdata_length = 8;
        break;
    case BootSettingsOpcode_CustomUSBPID:
        memcpy((char*)buffer,(char*)&bootsettings.customUSBPIDsignature,4);
        memcpy((char*)&buffer[4],(char*)&bootsettings.customUSBPID,4);
        inputdata_ptr = buffer;
        inputdata_length = 8;
        break;
    default:
        *settingsdata_length = 0;
        return S_NOTSUPPORT;
    }

    memcpy(settingsdata,inputdata_ptr,inputdata_length);
    *settingsdata_length = inputdata_length;

    return status;
}

//------------------------------------------------------------------------------
// Set bootsettings by opcode from an encrypted message
// Inputs:  u16 opcode
//          u8  *settingsdata
//          u32 settingsdata_length (max 56 bytes)
// Return:  u8  status
// Engineer: Quyen Leba
//------------------------------------------------------------------------------
u8 bootsettings_set(u16 opcode, u8 *settingsdata, u32 settingsdata_length)
{
    u8  status;

    status = S_SUCCESS;
    if (settingsdata_length == 0 || settingsdata_length > 56)
    {
        return S_INPUT;
    }

    //TODOQ: check for length each opcode
    switch((BootSettingsOpcode)opcode)
    {
    case BootSettingsOpcode_FailSafeBootVersion:
        memcpy((char*)&bootsettings.failsafeboot_version, settingsdata,
               sizeof(bootsettings.failsafeboot_version));
        break;
    case BootSettingsOpcode_MainBootVersion:
        memcpy((char*)&bootsettings.mainboot_version, settingsdata,
               sizeof(bootsettings.mainboot_version));
        break;
    case BootSettingsOpcode_AppVersion:
        memcpy((char*)&bootsettings.app_version, settingsdata,
               sizeof(bootsettings.app_version));
        break;
    case BootSettingsOpcode_AppSignature:
        memcpy((char*)&bootsettings.app_signature, settingsdata,
               sizeof(bootsettings.app_signature));
        break;
    case BootSettingsOpcode_BootMode:
        memcpy((char*)&bootsettings.bootmode, settingsdata,
               sizeof(bootsettings.bootmode));
        break;
    case BootSettingsOpcode_FailSafeBootCRC32E:
        memcpy((char*)&bootsettings.failsafeboot_crc32e, settingsdata,
               sizeof(bootsettings.failsafeboot_crc32e));
        break;
    case BootSettingsOpcode_MainBootCRC32E:
        memcpy((char*)&bootsettings.mainboot_crc32e, settingsdata,
               sizeof(bootsettings.mainboot_crc32e));
        break;
    case BootSettingsOpcode_AppCRC32E:
        memcpy((char*)&bootsettings.app_crc32e, settingsdata,
               sizeof(bootsettings.app_crc32e));
        memcpy((char*)&bootsettings.app_length, (char*)&settingsdata[4],
               sizeof(bootsettings.app_length));
        break;
    case BootSettingsOpcode_CustomUSBPID:
        memcpy((char*)&bootsettings.customUSBPIDsignature, settingsdata,
               sizeof(bootsettings.customUSBPIDsignature));
        memcpy((char*)&bootsettings.customUSBPID, (char*)&settingsdata[4],
               sizeof(bootsettings.customUSBPID));
        break;
    default:
        return S_NOTSUPPORT;
    }

    if (status == S_SUCCESS)
    {
        status = bootsettings_update();
    }
    return status;
}
