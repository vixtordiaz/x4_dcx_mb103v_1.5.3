/**
  **************** (C) COPYRIGHT 2011 SCT Performance, LLC *********************
  * File Name          : itoa.h
  * Device / System    : SCT products using the "CommonCode" platform
  * Author             : Quyen Leba
  *
  * Version            : 1 
  * Date               : 05/02/2011
  * Description        : 
  *                    : 
  *
  *
  * History            : 05/02/2011 M. Davis
  *                    :   Add SCT Copyright header,
  *
  ******************************************************************************
  */

#ifndef __ITOA_H
#define __ITOA_H

#include <arch/gentype.h>

u8* itoa(s32 value, u8* string, u32 radix);

#endif  //__ITOA_H
