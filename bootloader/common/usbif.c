/**
  **************** (C) COPYRIGHT 2011 SCT Performance, LLC *********************
  * File Name          : usbif.c
  * Device / System    : SCT products using the "CommonCode" platform
  * Author             : Quyen Leba
  *
  * Version            : 1 
  * Date               : 05/02/2011
  * Description        : 
  *                    : 
  *
  *
  * History            : 05/02/2011 M. Davis
  *                    :   Add SCT Copyright header,
  *
  ******************************************************************************
  */

//#include "usbif.h"
//
////------------------------------------------------------------------------------
//// Initialize USB interface
//// Engineer: Quyen Leba
////------------------------------------------------------------------------------
//void usbif_init()
//{
//}
//
////------------------------------------------------------------------------------
//// 
//// Engineer: Quyen Leba
////------------------------------------------------------------------------------
//void usbif_bulkout_callback()
//{
//    u8  buffer[128];
//    u32 bytecount;
//    u16 rxstatus;
//    u16 txstatus;
//
//    rxstatus = GetEPRxStatus(ENDP1);
//    if (rxstatus == EP_RX_NAK)
//    {
//        bytecount = GetEPRxCount(ENDP1);
//        PMAToUserBufferCopy((void*)buffer, ENDP1_RXADDR, bytecount);
//        SetEPRxValid(ENDP1);
//        
//        //----
//        int i;
//        for(i=0;i<2;i++)
//        {
//            UserToPMABufferCopy(((u8 *)buffer), ENDP2_TXADDR, bytecount);
//            SetEPTxCount(ENDP2, bytecount);
//            SetEPTxStatus(ENDP2, EP_TX_VALID);
//            while(1)
//            {
//                txstatus = GetEPTxStatus(ENDP2);
//                if (txstatus == EP_TX_NAK)
//                {
//                    //data sent successfully
//                    break;
//                }
//                else if (txstatus == EP_TX_STALL)
//                {
//                    //returnstatus = S_STALL;
//                    //count = length;
//                    break;
//                }
//                //anoher case is EP_TX_DIS but should never happen
//            }
//        }
//    }
//    else if (rxstatus == EP_RX_STALL)
//    {
//        bytecount = 0;
//    }
//}
