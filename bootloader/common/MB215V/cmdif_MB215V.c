/**
  **************** (C) COPYRIGHT 2011 SCT Performance, LLC *********************
  * File Name          : cmdif_tsxd.c
  * Device / System    : SCT products using the "CommonCode" platform
  * Author             : Quyen Leba
  *
  * Version            : 1 
  * Date               : 05/02/2011
  * Description        : 
  *                    : 
  *
  *
  * History            : 05/02/2011 M. Davis
  *                    :   Add SCT Copyright header,
  *
  ******************************************************************************
  */

#include <arch/gentype.h>
#include <MB103V/bluetooth.h>
#include <common/statuscode.h>
#include <common/cmdif.h>
#include "cmdif_mb215v.h"

extern cmdif_datainfo cmdif_datainfo_responsedata;

//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
u8 cmdif_handler()
{
    u8 cmddata[2048+16];
    u16 cmdlength;
    u8 cmdstatus;
    u8 status;

    cmdstatus = bluetooth_receive_command(cmddata,&cmdlength);
    if (cmdstatus == S_SUCCESS)
    {
        status = cmdif_command(cmddata[1], &cmddata[4], cmdlength-4);
        //Note: status is for debug; any error should be reported/handle
        //internally

        if (cmddata[1] != CMDIF_CMD_READ_TO_MONITOR &&
            cmddata[1] != CMDIF_CMD_DO_UPLOAD &&
            cmddata[1] != CMDIF_CMD_DO_DOWNLOAD &&
            cmddata[1] != CMDIF_CMD_DO_RETURN_STOCK)
        {
            cmdif_response_ack(cmdif_datainfo_responsedata.commandcode,
                               cmdif_datainfo_responsedata.responsecode,
                               cmdif_datainfo_responsedata.dataptr,
                               cmdif_datainfo_responsedata.datalength);
        }
    }
    return status;
}

