/**
  **************** (C) COPYRIGHT 2011 SCT Performance, LLC *********************
  * File Name          : tea.c
  * Device / System    : SCT products using the "CommonCode" platform
  * Author             : Quyen Leba
  *
  * Version            : 1 
  * Date               : 05/02/2011
  * Description        : 
  *                    : 
  *
  *
  * History            : 05/02/2011 M. Davis
  *                    :   Add SCT Copyright header,
  *
  ******************************************************************************
  */

#include "tea.h"

//------------------------------------------------------------------------------
// Simple encryption of 8-byte value with 16-bit key
// Inputs:  u32 *value (val[2])
//          u32 *key (key[4])
//------------------------------------------------------------------------------
void tea_encryption(u32 *value, u32 *key)
{
    u32 y=value[0], z=value[1], sum=0;  //set up
    u32 delta=0x9e3779b9;               //a key schedule constant
    u32 n=32;

    while (n-- > 0)
    {
        sum += delta;
        y += ((z<<4)+key[0]) ^ (z+sum) ^ ((z>>5)+key[1]) ;
        z += ((y<<4)+key[2]) ^ (y+sum) ^ ((y>>5)+key[3]) ;
    }
    value[0] = y;
    value[1] = z;
}
