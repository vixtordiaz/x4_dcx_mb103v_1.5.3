/**
  **************** (C) COPYRIGHT 2011 SCT Performance, LLC *********************
  * File Name          : obd2def.h
  * Device / System    : SCT products using the "CommonCode" platform
  * Author             : Quyen Leba
  *
  * Version            : 1 
  * Date               : 05/02/2011
  * Description        : 
  *                    : 
  *
  *
  * History            : 05/02/2011 M. Davis
  *                    :   Add SCT Copyright header,
  *
  ******************************************************************************
  */

#ifndef __OBD2DEF_H
#define __OBD2DEF_H

#include <arch/gentype.h>
//#include <common/veh_defs.h>
//#include <common/genmanuf.h>

#define VIN_LENGTH                  17
#define MAX_TUNECODE_LENTH          16
#define MIN_TUNECODE_LENTH          4

#define MAX_TUNE_CODE_LIST_COUNT    5
#define TUNECODE_SEPARATOR          '*'
#define TUNECODE_SEPARATOR_STRING   "*"
#define TUNECODE_TERMINATOR         '_'

typedef enum
{
    WriteDataBlock                  = 0x3B,
    ReadDataBlock                   = 0x3C,
    TesterPresent                   = 0x3F,
    GeneralResponseMessage          = 0x7F,
}Obd2FunctionCode;

typedef enum
{
    //TODOQ: right now, X3P has different values, merge that, IMPORTANT: remember adjust settings
    SCP_COMM                        = 0,
    SCP32_COMM                      = 1,
    VPW_COMM                        = 2,
    CAN_COMM                        = 3,
    MSCAN_COMM                      = 4,
    SCI_COMM                        = 5,
    ISO9141_COMM                    = 6,
    ISO14230_COMM                   = 7,
    UNKNOWN_COMM                    = 0xFF,
}VehicleCommType;

typedef enum
{
    CommLevel_Regular               = 0,
    CommLevel_KWP2000               = CommLevel_Regular,
    CommLevel_ISO14229              = 1,
    CommLevel_Unknown               = 0xFF,
}VehicleCommLevel;

typedef union
{
    struct
    {
        u8  mode        : 4;
        u8  id29bit     : 1;
        u8  reserved    : 1;
        u8  timeout_idx : 2;
        u8  x;
    }can;
    struct
    {
        u8  highspeed   : 1;
        u8  reserved    : 7;
        u8  x;
    }scp;
    struct
    {
        u16 value;
    }raw;
}VehicleCommConfig;

typedef enum
{
    Diesel                          = 0,
    Gasoline                        = 1,
    UnknownFuelType                 = 0xFF,
}FuelType;

typedef enum
{
    Address_24_Bits,
    Address_32_Bits,
}MemoryAddressType;

typedef enum
{
    Request_3_Byte,
    Request_4_Byte,
}RequestLengthType;

typedef struct
{
#define DTC_MAX_COUNT               128
    u8 count;
    u16 codes[DTC_MAX_COUNT];
}dtc_info;

/*
typedef struct
{
    u8 ecm_count;
    u8 codecount[ECM_MAX_COUNT];
    u8 codeids[ECM_MAX_COUNT][ECM_MAX_PART_COUNT];
    u8 codes[ECM_MAX_COUNT][ECM_MAX_PART_COUNT][MAX_TUNECODE_LENTH+1];
    u8 second_codes[ECM_MAX_COUNT][ECM_MAX_PART_COUNT][MAX_TUNECODE_LENTH+1];
    // must be the same size as Settings_Tune.vehicle_serial_number
    u8 vehicle_serial_number[32];
    u8 bbc[4+1];
    u8 vin[VIN_LENGTH+1];       //only used within ecm_info
    VehicleCommType commtype;
}ecm_info;
*/

/*
typedef struct
{
    ecm_info ecm;
    u8 vin[VIN_LENGTH+1];
    VehicleCommType commtype;
}vehicle_info;
*/

#endif	//__OBD2DEF_H
