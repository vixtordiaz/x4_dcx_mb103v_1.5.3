/**
  **************** (C) COPYRIGHT 2011 SCT Performance, LLC *********************
  * File Name          : itoa.c
  * Device / System    : SCT products using the "CommonCode" platform
  * Author             : Quyen Leba
  *
  * Version            : 1 
  * Date               : 05/02/2011
  * Description        : 
  *                    : 
  *
  *
  * History            : 05/02/2011 M. Davis
  *                    :   Add SCT Copyright header,
  *
  ******************************************************************************
  */

#include "itoa.h"

//------------------------------------------------------------------------------
// Convert an int (s32) to a 0-terminated string, using specified radix
// Inputs:  s32 value (int value to convert)
//          u8  *string (pointer to resulting string)
//          u32 radix (base to use... 2 - 10, or 16... others undefined)
// Output:  u8  *string (the "string" location is modified,
//                       allow at least 12 chars for base 10)
// Return:  u8  *string
//------------------------------------------------------------------------------
u8* itoa(s32 value, u8* string, u32 radix)
{
    u8 buffer[32];
    bool negative;
    u32 unsignedNumber;
    u8  *buffer_bptr;
    
    buffer_bptr = buffer;
    unsignedNumber = value;
    negative = FALSE;
    if(value < 0 && radix == 10)
    {
        negative = TRUE;
        unsignedNumber = -value;
    }
    
    do
    {
        u32 temp = unsignedNumber % radix;
        *(buffer_bptr++) = temp + ((temp < 10) ? '0' : ('A' - 10));
    }
    while((unsignedNumber /= radix) != 0);
    
    if(negative)
    {
        *(buffer_bptr++) = '-';
    }
    
    do
    {
        *(string++) = *(--buffer_bptr);
    }
    while(buffer_bptr > buffer);
    
    *string = 0;
    
    return string;
}