/**
  **************** (C) COPYRIGHT 2011 SCT Performance, LLC *********************
  * File Name          : cmdif_function.c
  * Device / System    : SCT products using the "CommonCode" platform
  * Author             : Quyen Leba
  *
  * Version            : 1 
  * Date               : 05/02/2011
  * Description        : 
  *                    : 
  *
  *
  * History            : 05/02/2011 M. Davis
  *                    :   Add SCT Copyright header,
  *
  ******************************************************************************
  */

#include <string.h>
#include <ctype.h>
#include <stdio.h>
#include <board/genplatform.h>
#include <board/bootloader.h>
#include <common/statuscode.h>
#include <common/crypto_messageblock.h>
#include <common/version.h>
#include <common/itoa.h>
#include "cmdif.h"
#include "cmdif_function.h"

extern cmdif_datainfo cmdif_datainfo_responsedata;  //from cmdif.c

//------------------------------------------------------------------------------
// Get device info
// Return:  u8  status
// Engineer: Patrick Downs, Quyen Leba
//------------------------------------------------------------------------------
u8 cmdif_func_get_device_info()
{
    u8  buffer[128];
    u32 bufferlength;
    u8  *ptr;
    u32 infototallength;
    u8  status;

    cmdif_datainfo_responsedata.responsecode = CMDIF_ACK_FAILED;
    status = S_SUCCESS;

    ptr = cmdif_datainfo_responsedata.data;

    cmdif_datainfo_responsedata.datalength = 0;
    ptr[0] = NULL;

    strcat((char*)ptr,"\n<HEADER>Notice:</HEADER> ");
    strcat((char*)ptr,(char*)"Finish update procedure to put device back to regular operations");
    
    strcat((char*)ptr,"\n<HEADER>Device Part#:</HEADER> ");
    settings_getsettings_byopcode(SETTINGS_OPCODE_DEVICE_PARTNUMBER_STRING,0,
                                  buffer,&bufferlength);
    strcat((char*)ptr,(char*)buffer);
    
    settings_get_deviceserialnumber(buffer);
    strcat((char*)ptr,"\n<HEADER>Serial Number:</HEADER> ");
    strcat((char*)ptr,(char*)buffer);
    
    strcat((char*)ptr,"\n<HEADER>Mode:</HEADER> ");
#if __FAILSAFE_BOOTLOADER__
    strcat((char*)ptr,(char*)"Failsafe Bootloader");
#else
    strcat((char*)ptr,(char*)"Main Bootloader");
#endif

    strcat((char*)ptr,"\n<HEADER>Bootloader Version:</HEADER> ");
    itoa(BOOTLOADER_VERSION,buffer,10);
    strcat((char*)ptr,(char*)buffer);

    infototallength = strlen((char*)ptr)+1;

    cmdif_datainfo_responsedata.datalength = infototallength;
    cmdif_datainfo_responsedata.responsecode = CMDIF_ACK_OK;
    cmdif_datainfo_responsedata.flowcommandcode = CMDIF_CMD_GET_DONGLE_INFO;

    return status;
}

//------------------------------------------------------------------------------
// Get device data (settings, etc)
// Input:   u8  dataopcode (what data is requesting)
// Return   u8  status
// Engineer: Patrick Downs, Quyen Leba
//------------------------------------------------------------------------------
u8 cmdif_func_get_device_data(u8 dataopcode)
{
    CMDIF_GETDEVICEDATA_OPCODE opcode = (CMDIF_GETDEVICEDATA_OPCODE)dataopcode;
    u32 length;

    cmdif_datainfo_responsedata.datalength = 0;
    cmdif_datainfo_responsedata.responsecode = CMDIF_ACK_OK;
    
    switch(opcode)
    {
    case CMDIF_GETDEVICEDATA_MODESTATUS:
        cmdif_datainfo_responsedata.data[0] = CMDIF_ACK_BOOTLOADER_MODE;
        cmdif_datainfo_responsedata.datalength = 1;
        break;
    case CMDIF_GETDEVICEDATA_PROPERTIES:
        properties_getinfo(cmdif_datainfo_responsedata.data,&length);
        cmdif_datainfo_responsedata.datalength = length;
        break;
    case CMDIF_GETDEVICEDATA_SERIAL:
        strncpy((char*)cmdif_datainfo_responsedata.data, (char*)SETTINGS_CRITICAL(serialnumber),
                sizeof(SETTINGS_CRITICAL(serialnumber)));
        cmdif_datainfo_responsedata.datalength = sizeof(SETTINGS_CRITICAL(serialnumber));
        break;
    case CMDIF_GETDEVICEDATA_FIRMWARE:
        //TODOQ:
        cmdif_datainfo_responsedata.responsecode = CMDIF_ACK_UNSUPPORTED;
        break;
    case CMDIF_GETDEVICEDATA_DEVICETYPE:
        cmdif_datainfo_responsedata.data[0] = SETTINGS_CRITICAL(devicetype);
        cmdif_datainfo_responsedata.data[1] = SETTINGS_CRITICAL(devicetype) >> 8;
        cmdif_datainfo_responsedata.data[2] = SETTINGS_CRITICAL(devicetype) >> 16;
        cmdif_datainfo_responsedata.data[3] = SETTINGS_CRITICAL(devicetype) >> 24;
        cmdif_datainfo_responsedata.datalength = 4;
        break;
    case CMDIF_GETDEVICEDATA_MARKETTYPE:
        cmdif_datainfo_responsedata.data[0] = SETTINGS_CRITICAL(markettype);
        cmdif_datainfo_responsedata.data[1] = SETTINGS_CRITICAL(markettype) >> 8;
        cmdif_datainfo_responsedata.data[2] = SETTINGS_CRITICAL(markettype) >> 16;
        cmdif_datainfo_responsedata.data[3] = SETTINGS_CRITICAL(markettype) >> 24;
        cmdif_datainfo_responsedata.datalength = 4;
        break;
    default:
        cmdif_datainfo_responsedata.responsecode = CMDIF_ACK_UNSUPPORTED;
        break;
    }

    return S_SUCCESS;
}

//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
u8 cmdif_func_ping_response()
{
    cmdif_datainfo_responsedata.responsecode = CMDIF_ACK_OK;
    return S_SUCCESS;
}

//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
u8 cmdif_func_unsupported_command()
{
    cmdif_datainfo_responsedata.responsecode = CMDIF_ACK_NOT_SUPPORTED;
    return S_SUCCESS;
}

//------------------------------------------------------------------------------
// Get dongle settings
// Input:   u8  settings_opcode
// Return   u8  status
// Engineer: Patrick Downs, Quyen Leba
//------------------------------------------------------------------------------
u8 cmdif_func_get_dongle_settings(u8 settings_opcode)
{
    u8  settings[128];
    u8  *encrypted_buffer_bptr;
    u32 length;
    u8  status;

    cmdif_datainfo_responsedata.responsecode = CMDIF_ACK_FAILED;
    status = settings_getsettings_byopcode((SETTINGS_OPCODE)settings_opcode,0,
                                           settings,&length);
    if (status == S_SUCCESS)
    {
        encrypted_buffer_bptr = cmdif_datainfo_responsedata.data;
        status = crypto_messageblock_encrypt(settings,length,
                                             encrypted_buffer_bptr,
                                             NULL, FALSE, CRYPTO_USE_EXTERNAL_KEY);
        
        if (status == S_SUCCESS)
        {
            cmdif_datainfo_responsedata.datalength = MAX_CRYPTO_MESSAGE_BLOCK_LENGTH;
            cmdif_datainfo_responsedata.responsecode = CMDIF_ACK_OK;
        }
    }
    else
    {
        if (status == S_SERVICENOTSUPPORTED)
        {
            cmdif_datainfo_responsedata.responsecode = CMDIF_ACK_UNSUPPORTED;
        }
        else
        {
            cmdif_datainfo_responsedata.responsecode = CMDIF_ACK_FAILED;
        }
    }

    return S_SUCCESS;
}

//------------------------------------------------------------------------------
// Set bootloader mode
// Input:   BootloaderMode bootmode
// Return   u8  status
// Engineer: Quyen Leba
//------------------------------------------------------------------------------
u8 cmdif_func_bootloader_setboot(BootloaderMode bootmode)
{
    u8  status;

    cmdif_datainfo_responsedata.responsecode = CMDIF_ACK_FAILED;
    status = bootloader_setboot(bootmode);
    if (status == S_SUCCESS)
    {
        cmdif_datainfo_responsedata.responsecode = CMDIF_ACK_OK;
        watchdog_set();
    }
    return S_SUCCESS;
}

//------------------------------------------------------------------------------
// Init bootloader session
// Input:   u8  *firmware_header
// Return:  u8  status
// Engineer: Quyen Leba
// Note: run cmdif_func_bootloader_init once and repeat cmdif_func_bootloader_do
// until firmware fully loaded.
//------------------------------------------------------------------------------
u8 cmdif_func_bootloader_init(u8 *firmware_header)
{
    u16 sector_size;
    u8  status;

    cmdif_datainfo_responsedata.responsecode = CMDIF_ACK_FAILED;

    status = bootloader_setup_session(firmware_header,&sector_size);
    if (status == S_SUCCESS)
    {
        memcpy((char*)cmdif_datainfo_responsedata.data,(char*)&sector_size,sizeof(sector_size));

        cmdif_datainfo_responsedata.datalength = sizeof(sector_size);
        cmdif_datainfo_responsedata.responsecode = CMDIF_ACK_OK;
    }
    else if(status == S_NO_UPDATE)
    {
        cmdif_datainfo_responsedata.responsecode = CMDIF_ACK_DONE;
    }
    else if(status == S_INVALID_VERSION)
    {
        cmdif_datainfo_responsedata.responsecode = CMDIF_ACK_BACKDATE;
    }
    else if (status == S_INPUT)
    {
        cmdif_datainfo_responsedata.responsecode = CMDIF_ACK_UNSUPPORTED;
    }

    return S_SUCCESS;
}

//------------------------------------------------------------------------------
// Program firmware to flash
// Inputs:  u8  *encrypteddata (firmware content)
//          u32 length
// Return   u8  status
// Engineer: Quyen Leba
//------------------------------------------------------------------------------
u8 cmdif_func_bootloader_do(u8 *encrypteddata, u32 length)
{
    u8  status;

    cmdif_datainfo_responsedata.responsecode = CMDIF_ACK_FAILED;
    status = bootloader_write_flash(encrypteddata,length);
    if (status == S_SUCCESS)
    {
        cmdif_datainfo_responsedata.responsecode = CMDIF_ACK_OK;
    }
    return S_SUCCESS;
}

//------------------------------------------------------------------------------
// Validate firmware content from flash
// Return   u8  status
// Engineer: Quyen Leba
//------------------------------------------------------------------------------
u8 cmdif_func_bootloader_validate()
{
    u8  status;

    cmdif_datainfo_responsedata.responsecode = CMDIF_ACK_FAILED;
    status = bootloader_validate();
    if (status == S_SUCCESS)
    {
        cmdif_datainfo_responsedata.responsecode = CMDIF_ACK_OK;
    }
    return S_SUCCESS;
}
