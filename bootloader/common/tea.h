/**
  **************** (C) COPYRIGHT 2011 SCT Performance, LLC *********************
  * File Name          : tea.h
  * Device / System    : SCT products using the "CommonCode" platform
  * Author             : Quyen Leba
  *
  * Version            : 1 
  * Date               : 05/02/2011
  * Description        : 
  *                    : 
  *
  *
  * History            : 05/02/2011 M. Davis
  *                    :   Add SCT Copyright header,
  *
  ******************************************************************************
  */

#ifndef __TEA_H
#define __TEA_H

#include <arch/gentype.h>

void tea_encryption(u32 *value, u32 *key);

#endif  //__TEA_H
