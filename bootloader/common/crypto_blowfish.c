/**
  **************** (C) COPYRIGHT 2011 SCT Performance, LLC *********************
  * File Name          : crypto_blowfish.c
  * Device / System    : SCT products using the "CommonCode" platform
  * Author             : Quyen Leba
  *
  * Version            : 1 
  * Date               : 05/02/2011
  * Description        : 
  *                    : 
  *
  *
  * History            : 05/02/2011 M. Davis
  *                    :   Add SCT Copyright header,
  *
  ******************************************************************************
  */

#include <string.h>
#include <board/genplatform.h>
#include <common/statuscode.h>
#include "crypto_blowfish_keys.h"
#include "crypto_blowfish.h"

blowfish_context_t      m_allkeys_ctx;

//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
const u32 ORIG_P[18] =
{
    0x243F6A88, 0x85A308D3, 0x13198A2E, 0x03707344,
    0xA4093822, 0x299F31D0, 0x082EFA98, 0xEC4E6C89,
    0x452821E6, 0x38D01377, 0xBE5466CF, 0x34E90C6C,
    0xC0AC29B7, 0xC97C50DD, 0x3F84D5B5, 0xB5470917,
    0x9216D5D9, 0x8979FB1B
};

BF_KEY_TYPE current_key_type;

#define ORIG_S_ADDRESS                      FLASH_BF_S_ADDRESS

//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
#define BLOWFISH_F(x) \
    (((ctx->sbox[0][x >> 24] + ctx->sbox[1][(x >> 16) & 0xFF]) \
    ^ ctx->sbox[2][(x >> 8) & 0xFF]) + ctx->sbox[3][x & 0xFF])

void blowfish_encryptblock(blowfish_context_t *ctx, u32 *hi, u32 *lo);
void blowfish_decryptblock(blowfish_context_t *ctx, u32 *hi, u32 *lo);
void blowfish_initiate(blowfish_context_t *ctx, const void *keyparam, u32 keybytes);
void blowfish_clean(blowfish_context_t *ctx);

u8 crypto_blowfish_init_key_type(BF_KEY_TYPE type);
u8 crypto_blowfish_encryptblock_full(u8 *buf, u32 len);
u8 crypto_blowfish_decryptblock_full(u8 *buf, u32 len);


//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
void blowfish_encryptblock(blowfish_context_t *ctx, u32 *hi, u32 *lo)
{
    u32 i;
    u32 temp;

    for(i = 0; i < 16; i++)
    {
        *hi ^= ctx->pbox[i];
        *lo ^= BLOWFISH_F(*hi);
        temp = *hi, *hi = *lo, *lo = temp;
    }
    temp = *hi, *hi = *lo, *lo = temp;

    *lo ^= ctx->pbox[16];
    *hi ^= ctx->pbox[17];
}

//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
void blowfish_decryptblock(blowfish_context_t *ctx, u32 *hi, u32 *lo)
{
    u32 i;
    u32 temp;

    for(i = 17; i > 1; i--)
    {
        *hi ^= ctx->pbox[i];
        *lo ^= BLOWFISH_F(*hi);
        temp = *hi, *hi = *lo, *lo = temp;
    }
    temp = *hi, *hi = *lo, *lo = temp;

    *lo ^= ctx->pbox[1];
    *hi ^= ctx->pbox[0];
}

//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
void blowfish_initiate(blowfish_context_t *ctx, const void *keyparam,
                       u32 keybytes)
{
    keybytes %= 57;
    u8 *key = (u8*)keyparam;
    u32 i, j, k;
    u32 calc;
    u32 hi = 0, lo = 0;

    memcpy(ctx->sbox,(char*)ORIG_S_ADDRESS,sizeof(ctx->sbox));
    memcpy(ctx->pbox, ORIG_P, sizeof(ctx->pbox));

    if(keybytes)
    {
        for(i = 0, j = 0; i < 18; i++)
        {
            for(k = 0, calc = 0; k < 4; k++)
            {
                calc <<= 8, calc |= key[j++];
                if(j == keybytes)
                {
                    j = 0;
                }
            }
            ctx->pbox[i] ^= calc;
        }
    }

    for(i = 0; i < 18; i += 2)
    {
        blowfish_encryptblock(ctx, &hi, &lo);
        ctx->pbox[i] = hi;
        ctx->pbox[i + 1] = lo;
    }

    for(i = 0; i < 4; i++)
    {
        for(j = 0; j < 256; j += 2)
        {
            blowfish_encryptblock(ctx, &hi, &lo);
            ctx->sbox[i][j] = hi;
            ctx->sbox[i][j + 1] = lo;
        }
    }
}

//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
void blowfish_clean(blowfish_context_t *ctx)
{
    memset(ctx, 0, sizeof(blowfish_context_t));
}

//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
u8 crypto_blowfish_init_key_type(BF_KEY_TYPE type)
{
    if(current_key_type != type)
    {
        switch(type)    // Re-calculate table
        {
        case BF_KEY_INTERNAL:            
            blowfish_initiate(&m_allkeys_ctx,"Hv\\J|BI'c9?Yq,<@",16);
            current_key_type = BF_KEY_INTERNAL;
            break;
        case BF_KEY_DATA:
            blowfish_initiate(&m_allkeys_ctx,"_lzbQJm*t<+,?1E&",16);
            current_key_type = BF_KEY_DATA;
            break;
        case BF_KEY_EXTERNAL:
            blowfish_initiate(&m_allkeys_ctx,"~.a]7I!sp2{hkF)8",16);
            current_key_type = BF_KEY_EXTERNAL;
            break;
        default:
            return S_FAIL;            
        }
    }
    
    return S_SUCCESS;
}

//-----------------------------------------------------------------------------
// Inputs:  blowfish_context_t *ctx
//          u8  *buf
//          u32 len (must be multiple of 8)
// Return:  u8  status
//-----------------------------------------------------------------------------
u8 crypto_blowfish_encryptblock_full_key_type(BF_KEY_TYPE type, u8 *buf, u32 len)
{
    u8 status;
    
    status = crypto_blowfish_init_key_type(type);
    if(status == S_SUCCESS)
    {
        status = crypto_blowfish_encryptblock_full(buf, len);
    }
    
    return status;
}

//-----------------------------------------------------------------------------
// Inputs:  blowfish_context_t *ctx
//          u8  *buf
//          u32 len (must be multiple of 8)
// Return:  u8  status
//-----------------------------------------------------------------------------
u8 crypto_blowfish_decryptblock_full_key_type(BF_KEY_TYPE type, u8 *buf, u32 len)
{
    u8 status;
    
    status = crypto_blowfish_init_key_type(type);
    if(status == S_SUCCESS)
    {
        status = crypto_blowfish_decryptblock_full(buf, len);
    }
    
    return status;
}

//-----------------------------------------------------------------------------
// Inputs:  blowfish_context_t *ctx
//          u8  *buf
//          u32 len (must be multiple of 8)
// Return:  u8  status
//-----------------------------------------------------------------------------
u8 crypto_blowfish_encryptblock_full(u8 *buf, u32 len)
{
    u32 i;
    u32 *glbufptr;

    glbufptr = (u32*)buf;

    for(i=0;i<len/4;i+=2)
    {
        blowfish_encryptblock(&m_allkeys_ctx,&glbufptr[i],&glbufptr[i+1]);
    }
    
    return S_SUCCESS;
}

//-----------------------------------------------------------------------------
// Inputs:  blowfish_context_t *ctx
//          u8  *buf
//          u32 len (must be multiple of 8)
// Return:  u8  status
//-----------------------------------------------------------------------------
u8 crypto_blowfish_decryptblock_full(u8 *buf, u32 len)
{
    u32 i;
    u32 *glbufptr;

    glbufptr = (u32*)buf;
    
    for(i=0;i<len/4;i+=2)
    {
        blowfish_decryptblock(&m_allkeys_ctx,&glbufptr[i],&glbufptr[i+1]);
    }
    
    return S_SUCCESS;
}
