/**
  **************** (C) COPYRIGHT 2011 SCT Performance, LLC *********************
  * File Name          : settings.h
  * Device / System    : SCT products using the "CommonCode" platform
  * Author             : Quyen Leba
  *
  * Version            : 1 
  * Date               : 05/02/2011
  * Description        : 
  *                    : 
  *
  *
  * History            : 05/02/2011 M. Davis
  *                    :   Add SCT Copyright header,
  *
  ******************************************************************************
  */

#ifndef __SETTINGS_H
#define __SETTINGS_H

#include <arch/gentype.h>

#define DEVICE_SERIAL_NUMBER_LENGTH             13

// -----------------------------------------------------------------------------
//  IMPORTANT!!
//  Opcodes are matched to TSX application code(k_code_settings.h) and PC Admin 
//  Any changes made to opcodes need to be updated in above applications also!
// -----------------------------------------------------------------------------
typedef enum
{
    SETTINGS_OPCODE_FLAGS                       = 20,
    SETTINGS_OPCODE_VERSION                     = 30,
    SETTINGS_OPCODE_SERIAL                      = 31,
    SETTINGS_OPCODE_HARDWARE_ID                 = 32,
    SETTINGS_OPCODE_DEVICE_INFO                 = 33,
    SETTINGS_OPCODE_PROGRAMMER_INFO_P1          = 34,   //married, etc
    SETTINGS_OPCODE_PROGRAMMER_INFO_P2          = 35,   //
    SETTINGS_OPCODE_PROGRAMMER_INFO_P3          = 36,   //reserved
    SETTINGS_OPCODE_PROGRAMMER_INFO_P4          = 37,   //reserved
    SETTINGS_OPCODE_MAC_ADDRESS                 = 38,
    SETTINGS_OPCODE_KERNEL_ID                   = 39,   //reserved
    SETTINGS_OPCODE_MODE                        = 40,
    SETTINGS_OPCODE_STATUS                      = 41,
    SETTINGS_OPCODE_CAPACITY                    = 42,
    SETTINGS_OPCODE_FREESPACE                   = 43,
    SETTINGS_OPCODE_INIT_SETTINGS               = 44,
    SETTINGS_OPCODE_RESET_SETTINGS              = 45,
    SETTINGS_OPCODE_BRIGHTNESS                  = 46,
    SETTINGS_OPCODE_VOLUME                      = 47,
    SETTINGS_OPCODE_MICO_SIGNATURE              = 48,
    SETTINGS_OPCODE_TUNE_REVISION               = 49,
    SETTINGS_OPCODE_DEVICE_PARTNUMBER_STRING    = 50,
    SETTINGS_OPCODE_DEVICE_PROPERTIES           = 51,
    SETTINGS_OPCODE_ERRORPOOL                   = 52,
    SETTINGS_OPCODE_DEVICE_TYPE                 = 53,
    SETTINGS_OPCODE_MARKET_TYPE                 = 54,
    
    SETTINGS_OPCODE_STOCKFILESIZE               = 80,
    SETTINGS_OPCODE_VID                         = 81,
    SETTINGS_OPCODE_EPATS                       = 82,
    SETTINGS_OPCODE_VEHICLE_CODES               = 83,
    SETTINGS_OPCODE_VEHICLE_SERIAL_NUMBER       = 84,
    SETTINGS_OPCODE_VIN                         = 85,
    SETTINGS_OPCODE_PREV_VIN                    = 86,
    SETTINGS_OPCODE_TUNEINFOTRACK               = 87,
    SETTINGS_OPCODE_TUNEHISTORYINFO             = 88,
    
    SETTINGS_OPCODE_TUNE_HISTORY_INFO,
    SETTINGS_OPCODE_SPECIAL_STOCK_UPLOAD_INFO,
    
    //-------- Dongle Specific Opcodes -------
    SETTINGS_OPCODE_ENABLE_MASS_STORAGE         = 200,
}SETTINGS_OPCODE;

typedef enum
{
    SettingsRegion_Critical                     = 0,
    SettingsRegion_Tune                         = 1,
    SettingsRegion_Fleet                        = 2,
    SettingsRegion_DatalogGeneral               = 3,
}SettingsRegion;

typedef struct
{
    u8  version;
    u8  sct[3];
    u32 signature;
    u32 crc32e;
    u32 flags;
    u32 normaloperation_signature;
    //----------------
    u32 firmware_version;
    u32 devicetype;
    u32 markettype;
    u32 baudrate;           //only applicable to devices support Bluetooth
    u8  serialnumber[16];   //only use 13 bytes now
    u8  devicepartnumberstring[32];
}Settings_Critical;

extern Settings_Critical settingscritical;

#define SETTINGS_CRITICAL(def)          settingscritical.##def

u8 settings_load();
u8 settings_getsettings_byopcode(SETTINGS_OPCODE opcode, u16 privdata,
                                  u8 *settings_data, u32 *settings_len);
u8 settings_get_deviceserialnumber(u8 *serialnumber);

#endif  //__SETTINGS_H
