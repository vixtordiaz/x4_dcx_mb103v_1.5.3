/**
  **************** (C) COPYRIGHT 2011 SCT Performance, LLC *********************
  * File Name          : log.c
  * Device / System    : SCT products using the "CommonCode" platform
  * Author             : Quyen Leba
  *
  * Version            : 1 
  * Date               : 05/02/2011
  * Description        : 
  *                    : 
  *
  *
  * History            : 05/02/2011 M. Davis
  *                    :   Add SCT Copyright header,
  *
  ******************************************************************************
  */

#include <string.h>
#include <common/bootsettings.h>
#include <common/statuscode.h>
#include <common/log.h>

error_point_pool *booterrorpointpool = &BOOTSETTINGS(errorpointpool);

//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
void log_reset()
{
    memset(booterrorpointpool->error_point,
           0,sizeof(booterrorpointpool->error_point));
    booterrorpointpool->position = 0;
    booterrorpointpool->looped = FALSE;
}

//------------------------------------------------------------------------------
// Log an error point; a trail of error points can be helpful to track down
// an error and possibly its cause.
// Inputs:  u16 error_point (a unique error_point number)
// Return:  none
// Engineer: Quyen Leba
//------------------------------------------------------------------------------
void log_push_error_point(u16 error_point)
{
    if (booterrorpointpool->position >= LOG_MAX_ERROR_POINT_COUNT)
    {
        booterrorpointpool->position = 0;
        booterrorpointpool->looped = TRUE;
    }
    booterrorpointpool->error_point[booterrorpointpool->position] = error_point;
    booterrorpointpool->position++;
    bootsettings_update();
}
