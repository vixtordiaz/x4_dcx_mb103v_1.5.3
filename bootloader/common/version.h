/**
  **************** (C) COPYRIGHT 2011 SCT Performance, LLC *********************
  * File Name          : version.h
  * Device / System    : SCT products using the "CommonCode" platform
  * Author             : Quyen Leba
  *
  * Version            : 1 
  * Date               : 05/02/2011
  * Description        : 
  *                    : 
  *
  *
  * History            : 05/02/2011 M. Davis
  *                    :   Add SCT Copyright header,
  *
  ******************************************************************************
  */

#ifndef __VERSION_H
#define __VERSION_H

#include <arch/gentype.h>

#define FAILSAFE_BOOTLOADER_OPERATING_MODE  0xE71A955B
#define MAIN_BOOTLOADER_OPERATING_MODE      0x36F585C2
#define APPLICATION_OPERATING_MODE          0x6318DF5A
#define BOOTLOADER_VERSION_MAJOR            1
#define BOOTLOADER_VERSION_MINOR            11
#define BOOTLOADER_VERSION                  (BOOTLOADER_VERSION_MAJOR*1000 + BOOTLOADER_VERSION_MINOR)

typedef struct
{
    u32 signature;
    u32 version;
    u32 device_type;
    u32 market_type;
    u8  build;
    u8  reserved[15];
}FIRMWARE_TAG;

extern const FIRMWARE_TAG firmware_tag;

#endif  //__VERSION_H
