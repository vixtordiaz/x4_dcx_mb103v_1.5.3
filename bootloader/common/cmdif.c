/**
  **************** (C) COPYRIGHT 2011 SCT Performance, LLC *********************
  * File Name          : cmdif.c
  * Device / System    : SCT products using the "CommonCode" platform
  * Author             : Quyen Leba
  *
  * Version            : 1 
  * Date               : 05/02/2011
  * Description        : 
  *                    : 
  *
  *
  * History            : 05/02/2011 M. Davis
  *                    :   Add SCT Copyright header,
  *
  ******************************************************************************
  */

#include <string.h>
#include <stdio.h>
#include <board/genplatform.h>
#include <board/bootloader.h>
#include <common/statuscode.h>
#include "cmdif_function.h"
#include "cmdif.h"

cmdif_datainfo cmdif_datainfo_responsedata;

#ifdef SUPPORT_FILE_XFER
extern filexfer_info filexferinfo;                  //from cmdif_func_filexfer.c
extern rawspp_xfer_info rawsppxferinfo;             //from cmdif_func_filexfer.c
#endif

//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
u8 cmdif_init()
{
    cmdif_datainfo_responsedata.datalength = 0;
    cmdif_datainfo_responsedata.responsetype = NormalAck;

    return S_SUCCESS;
}

//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
u8 cmdif_command(u16 command, u8 *data, u16 length)
{
    u8 status;

    //TODOQ: IMPORTANT: we don't check for length
    
    status = S_FAIL;
    cmdif_datainfo_responsedata.responsecode = CMDIF_ACK_FAILED;
    cmdif_datainfo_responsedata.commandcode = (CMDIF_COMMAND)command;
    cmdif_datainfo_responsedata.responsetype = NormalAck;
    cmdif_datainfo_responsedata.datalength = 0;

    switch(command)
    {
    //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    case CMDIF_CMD_GET_DONGLE_INFO:
        status = cmdif_func_get_device_info();
        break;
    case CMDIF_CMD_GET_DONGLE_DATA:
        //data: [1:opcode]
        if (length == 1)
        {
            status = cmdif_func_get_device_data(data[0]);
        }
        break;
    case CMDIF_CMD_GET_DONGLE_SETTINGS:
        //data: [1:opcode]
        if (length == 1)
        {
            status = cmdif_func_get_dongle_settings(data[0]);
        }
        break;
    case CMDIF_CMD_PING_RESPONSE:
        status = cmdif_func_ping_response();
        break;
    //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    case CMDIF_CMD_BOOTLOADER_SETBOOT:
        //data: [2:boot type]
        if (length == 2)
        {
            status = cmdif_func_bootloader_setboot((BootloaderMode)*(u16*)data);
        }
        break;
    case CMDIF_CMD_BOOTLOADER_INIT:
        //data: [64:firmware encrypted header]
        if (length == 64)
        {
            status = cmdif_func_bootloader_init(data);
        }
        break;
    case CMDIF_CMD_BOOTLOADER_DO:
        status = cmdif_func_bootloader_do(data,length);
        break;
    case CMDIF_CMD_BOOTLOADER_VALIDATE:
        status = cmdif_func_bootloader_validate();
        break;
    case CMDIF_CMD_BOOTLOADER_CLEARENGSIGN:
        status = bootloader_clearengineeringsignature();
        break;
    //++++//
    case CMDIF_CMD_DEBUG:
        //data:[4:debug task]
        if (length == 4)
        {
            //status = cmdif_func_debug(*((u32*)data));
        }
        break;
    //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    default:
        status = cmdif_func_unsupported_command();
        break;
    }

    return status;
}

//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
u8 cmdif_getResponseData(cmdif_datainfo **datainfo)
{
    *datainfo = &cmdif_datainfo_responsedata;
    return S_SUCCESS;
}
