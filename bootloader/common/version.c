/**
  **************** (C) COPYRIGHT 2011 SCT Performance, LLC *********************
  * File Name          : version.c
  * Device / System    : SCT products using the "CommonCode" platform
  * Author             : Quyen Leba
  *
  * Version            : 1 
  * Date               : 05/02/2011
  * Description        : 
  *                    : 
  *
  *
  * History            : 05/02/2011 M. Davis
  *                    :   Add SCT Copyright header,
  *
  ******************************************************************************
  */

#include "version.h"
#include "device_config.h"

#pragma location = ".firmware_tag"
const FIRMWARE_TAG firmware_tag =
{
#if __FAILSAFE_BOOTLOADER__
#warning BUILDING_FAILSAFEBOOT_FIRMWARE
    .signature  = FAILSAFE_BOOTLOADER_OPERATING_MODE,
#elif __MAIN_BOOTLOADER__
#warning BUILDING_MAINBOOT_FIRMWARE
    .signature  = MAIN_BOOTLOADER_OPERATING_MODE,
#else
#error INVALID_BOOTLOADER   @ version.c
#endif
    .version        = BOOTLOADER_VERSION,    
    .device_type    = DEVICE_TYPE,
    .market_type    = MARKET_TYPE,
    .build          = 0x00,
    .reserved = {0,0,0,0,0,0,0,0,0,0,0,0,0,0,0},
};
