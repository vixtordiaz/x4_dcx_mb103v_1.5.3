/**
  **************** (C) COPYRIGHT 2011 SCT Performance, LLC *********************
  * File Name          : crypto_blowfish.h
  * Device / System    : SCT products using the "CommonCode" platform
  * Author             : Quyen Leba
  *
  * Version            : 1 
  * Date               : 05/02/2011
  * Description        : 
  *                    : 
  *
  *
  * History            : 05/02/2011 M. Davis
  *                    :   Add SCT Copyright header,
  *
  ******************************************************************************
  */

#ifndef __CRYPTO_BLOWFISH_H
#define __CRYPTO_BLOWFISH_H

#include <arch/gentype.h>

typedef enum
{
    BF_KEY_INVALID0     = 0x00,
        
    BF_KEY_INTERNAL     = 0x01,     //for USB, VehicleBoard
    BF_KEY_DATA         = 0x02,     //for important/private data
    BF_KEY_EXTERNAL     = 0x03,     //for other comm such as Bluetooth
    
    BF_KEY_INVALID1     = 0xFF,
}BF_KEY_TYPE;


typedef struct
{
    u32 pbox[18];
    u32 sbox[4][256];
}blowfish_context_t;

u8 crypto_blowfish_encryptblock_full_key_type(BF_KEY_TYPE type, u8 *buf, u32 len);
u8 crypto_blowfish_decryptblock_full_key_type(BF_KEY_TYPE type, u8 *buf, u32 len);

#define crypto_blowfish_encryptblock_external_key(b,l)   \
    crypto_blowfish_encryptblock_full_key_type(BF_KEY_EXTERNAL,b,l);
#define crypto_blowfish_decryptblock_external_key(b,l)   \
    crypto_blowfish_decryptblock_full_key_type(BF_KEY_EXTERNAL,b,l);

#define crypto_blowfish_encryptblock_internal_key(b,l)   \
    crypto_blowfish_encryptblock_full_key_type(BF_KEY_INTERNAL,b,l);
#define crypto_blowfish_decryptblock_internal_key(b,l)   \
    crypto_blowfish_decryptblock_full_key_type(BF_KEY_INTERNAL,b,l);

#define crypto_blowfish_encryptblock_critical(b,l)   \
    crypto_blowfish_encryptblock_full_key_type(BF_KEY_DATA,b,l);
#define crypto_blowfish_decryptblock_critical(b,l)   \
    crypto_blowfish_decryptblock_full_key_type(BF_KEY_DATA,b,l);

#endif  //__CRYPTO_BLOWFISH_H
