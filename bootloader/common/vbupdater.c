/**
  **************** (C) COPYRIGHT 2011 SCT Performance, LLC *********************
  * File Name          : vbupdater.c
  * Device / System    : SCT products using the "CommonCode" platform
  * Author             : Quyen Leba
  *
  * Version            : 1 
  * Date               : 05/02/2011
  * Description        : 
  *                    : 
  *
  *
  * History            : 05/02/2011 M. Davis
  *                    :   Add SCT Copyright header,
  *
  ******************************************************************************
  */

#include <string.h>
#include <board/genplatform.h>
#include <board/bootloader.h>
#include <board/delays.h>
#include <common/bootsettings.h>
#include <common/statuscode.h>
#include "vbupdater.h"

struct
{
    u32 id;
    u32 bootversion;
    u16 sector_size;
    u8  rev;
    u8  secboot_startsector;
    u16 secboot_length;
    u8  app_startsector;
    u8  reserved[17];
}rdid_info;

//------------------------------------------------------------------------------
// Init VehicleBoard in bootloader
// Return:  u8  status
// Engineer: Quyen Leba
//------------------------------------------------------------------------------
u8 vbupdater_init()
{
    u8  buffer[256];
    u16 bufferlength;
    bool is_retried;
    u8  status;

    rdid_info.id = 0;
    is_retried = FALSE;
vbupdater_init_retry_rdid:
    status = mblif_link(MBLIF_CMD_RDID,0,
                        buffer,&bufferlength);
    if (status != S_SUCCESS || bufferlength != sizeof(rdid_info))
    {
        status = S_ERROR;
        goto vbupdater_init_error;
    }
    memcpy((char*)&rdid_info,buffer,sizeof(rdid_info));

    switch(rdid_info.id)
    {
    case 0xE71A955B:    //VehicleBoard in FailsafeBoot
    case 0x36F585C2:    //VehicleBoard in MainBoot
        break;
    default:
        if (is_retried)
        {
            goto vbupdater_init_error;
        }
        buffer[0] = BootSettingsOpcode_BootMode & 0xFF;
        buffer[1] = BootSettingsOpcode_BootMode >> 8;
        buffer[2] = BootloaderMode_MB_FailSafe & 0xFF;
        buffer[3] = BootloaderMode_MB_FailSafe >> 8;
        status = mblexec_call(MBLEXEC_OPCODE_SET_SETTINGS,
                              buffer,4,NULL,NULL);
        if (status != S_SUCCESS)
        {
            goto vbupdater_init_error;
        }
        status = mblexec_call(MBLEXEC_OPCODE_RUN_FAILSAFE_BOOTLOADER,   //TODOQ: use MAIN_BOOTLOADER
                              NULL,NULL,NULL,NULL);
        if (status != S_SUCCESS)
        {
            goto vbupdater_init_error;
        }
        delays(100,'m');
        is_retried = TRUE;  //retry to make sure it's in bootloader
        goto vbupdater_init_retry_rdid;
        //break;
    }
    
vbupdater_init_error:
    return status;
}

//------------------------------------------------------------------------------
// Reflash FLASH of VehicleBoard
// Inputs:  u16 sector (starting sector)
//          u8  *data
//          u16 datalength
// Return:  u8  status
// Engineer: Quyen Leba
//------------------------------------------------------------------------------
u8 vbupdater_reflash(u16 sector, u8 *data, u16 datalength)
{
    u8  opcode_data[2];
    u16 opcode_datalength;
    u32 currlength;
    u8  status;

    for(currlength=0;currlength<datalength;currlength+=rdid_info.sector_size)
    {
        opcode_data[0] = sector & 0xFF;
        opcode_data[1] = sector >> 8;
        opcode_datalength = 2;
        status = mblexec_call(MBLEXEC_OPCODE_ERASE_SECTOR,
                              opcode_data,opcode_datalength,NULL,NULL);
        if (status != S_SUCCESS)
        {
            goto vbupdater_write_error;
        }

        opcode_datalength = datalength - currlength;
        if (opcode_datalength > rdid_info.sector_size)
        {
            opcode_datalength = rdid_info.sector_size;
        }
        status = mblif_link(MBLIF_CMD_WRITE_BUFFER,0,
                            &data[currlength],&opcode_datalength);
        if (status != S_SUCCESS)
        {
            goto vbupdater_write_error;
        }

        opcode_datalength = 2;
        status = mblexec_call(MBLEXEC_OPCODE_WRITE_SECTOR,
                              opcode_data,opcode_datalength,NULL,NULL);
        if (status != S_SUCCESS)
        {
            goto vbupdater_write_error;
        }

        sector++;
    }

vbupdater_write_error:
    return status;
}

//------------------------------------------------------------------------------
// 
// Inputs:  u32 mainboot_crc32e
//          u32 application_crc32e
//          u16 application_length
// Return:  u8  status
// Engineer: Quyen Leba
//------------------------------------------------------------------------------
u8 vbupdater_terminate(u32 mainboot_crc32e,
                       u32 application_crc32e, u16 application_length)
{
    u16 opcode;
    u8  buffer[256];
    u16 bufferlength;
    __packed struct
    {
        u16 sector;
        u32 length;
        u32 crc32e;
    }validator_info;
    struct
    {
        u32 crc32e_value;
        u32 length;
    }crc32e_info;
    u8  status;

    if (mainboot_crc32e != 0)
    {
        opcode = BootSettingsOpcode_MainBootCRC32E;
        status = mblexec_call(MBLEXEC_OPCODE_GET_SETTINGS,(u8*)&opcode,2,
                              buffer,&bufferlength);
        if (status != S_SUCCESS || bufferlength != sizeof(crc32e_info))
        {
            goto vbupdater_terminate_error;
        }
        memcpy((char*)&crc32e_info,buffer,sizeof(crc32e_info));

        validator_info.sector = rdid_info.secboot_startsector;
        validator_info.length = crc32e_info.length;
        validator_info.crc32e = mainboot_crc32e;
        status = mblexec_call(MBLEXEC_OPCODE_VALIDATE_FLASH_CONTENT,
                              (u8*)&validator_info,sizeof(validator_info),
                              NULL,NULL);
        if (status != S_SUCCESS)
        {
            goto vbupdater_terminate_error;
        }
        
        buffer[0] = BootSettingsOpcode_MainBootCRC32E & 0xFF;
        buffer[1] = BootSettingsOpcode_MainBootCRC32E >> 8;
        buffer[2] = mainboot_crc32e & 0xFF;
        buffer[3] = mainboot_crc32e >> 8;
        buffer[4] = mainboot_crc32e >> 16;
        buffer[5] = mainboot_crc32e >> 24;
        status = mblexec_call(MBLEXEC_OPCODE_SET_SETTINGS,buffer,6,NULL,NULL);
        if (status != S_SUCCESS)
        {
            goto vbupdater_terminate_error;
        }
    }

    if (application_crc32e != 0)
    {
        validator_info.sector = rdid_info.app_startsector;
        validator_info.length = application_length;
        validator_info.crc32e = application_crc32e;
        status = mblexec_call(MBLEXEC_OPCODE_VALIDATE_FLASH_CONTENT,
                              (u8*)&validator_info,sizeof(validator_info),
                              NULL,NULL);
        if (status != S_SUCCESS)
        {
            goto vbupdater_terminate_error;
        }

        buffer[0] = BootSettingsOpcode_AppCRC32E & 0xFF;
        buffer[1] = BootSettingsOpcode_AppCRC32E >> 8;
        buffer[2] = application_crc32e & 0xFF;
        buffer[3] = application_crc32e >> 8;
        buffer[4] = application_crc32e >> 16;
        buffer[5] = application_crc32e >> 24;
        buffer[6] = application_length & 0xFF;
        buffer[7] = application_length >> 8;
        buffer[8] = application_length >> 16;
        buffer[9] = application_length >> 24;
        status = mblexec_call(MBLEXEC_OPCODE_SET_SETTINGS,buffer,10,NULL,NULL);
        if (status != S_SUCCESS)
        {
            goto vbupdater_terminate_error;
        }
    }
    
    buffer[0] = BootSettingsOpcode_BootMode & 0xFF;
    buffer[1] = BootSettingsOpcode_BootMode >> 8;
    buffer[2] = BootloaderMode_MB_Application & 0xFF;
    buffer[3] = BootloaderMode_MB_Application >> 8;
    status = mblexec_call(MBLEXEC_OPCODE_SET_SETTINGS,buffer,4,NULL,NULL);
    if (status != S_SUCCESS)
    {
        goto vbupdater_terminate_error;
    }

vbupdater_terminate_error:
    return status;
}
