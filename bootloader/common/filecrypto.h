/**
  **************** (C) COPYRIGHT 2011 SCT Performance, LLC *********************
  * File Name          : filecrypto.h
  * Device / System    : SCT products using the "CommonCode" platform
  * Author             : Quyen Leba
  *
  * Version            : 1 
  * Date               : 05/02/2011
  * Description        : 
  *                    : 
  *
  *
  * History            : 05/02/2011 M. Davis
  *                    :   Add SCT Copyright header,
  *
  ******************************************************************************
  */

#ifndef __FILECRYPTO_H
#define __FILECRYPTO_H

#include <arch/gentype.h>

#define FILECRYPTO_GENERAL_HEADER_VERSION_MAX       1

typedef enum
{
    HARDWARE_LW                             = 0,
    HARDWARE_TSX_D                          = 1,    //Dongle
    HARDWARE_TSX_M                          = 2,    //Kii (Monitor)
    HARDWARE_X3                             = 3,
    HARDWARE_X3P                            = 4,
    HARDWARE_iTSX                           = 10,
    HARDWARE_MainBoard_RevA                 = 11,
    HARDWARE_VehicleBoard_RevB              = 12,
    HARDWARE_AppBoardiTSX_RevA              = 13,
    
    HARDWARE_UNKNOWN                        = 0xA0,
    HARDWARE_ANY                            = 0xFF,
}FileCrypto_Hardware;

typedef enum
{
    FILETYPE_FIRMWARE                       = 0,
    FILETYPE_CUSTOM_TUNE                    = 1,
    FILETYPE_TUNE_OPTION                    = 2,
    FILETYPE_STOCK_RECOVERY                 = 3,
    FILETYPE_FILE_BUNDLE                    = 4,
    FILETYPE_SYSTEM_RECOVERY_IMAGE          = 5,
    FILETYPE_SYSTEM_RECOVERY_FILE_BUNDLE    = 6,
    FILETYPE_UNKNOWN                        = 0xFF,
}FileCrypto_FileType;

typedef enum
{
    FILESUBTYPE_NONE                        = 0,
    FILESUBTYPE_SYSTEM_RECOVERY             = 1,
    FILESUBTYPE_TUNE_REVISION               = 2,
    FILESUBTYPE_FIRMWARE                    = 3,
    FILESUBTYPE_UNKNOWN                     = 0xFF,
}FileCrypto_FileSubType;

#endif	//__FILECRYPTO_H
