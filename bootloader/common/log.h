/**
  **************** (C) COPYRIGHT 2011 SCT Performance, LLC *********************
  * File Name          : log.h
  * Device / System    : SCT products using the "CommonCode" platform
  * Author             : Quyen Leba
  *
  * Version            : 1 
  * Date               : 05/02/2011
  * Description        : 
  *                    : 
  *
  *
  * History            : 05/02/2011 M. Davis
  *                    :   Add SCT Copyright header,
  *
  ******************************************************************************
  */

#ifndef __LOG_H
#define __LOG_H

#include <arch/gentype.h>

#define LOG_MAX_ERROR_POINT_COUNT       40
typedef struct
{
    u16 error_point[LOG_MAX_ERROR_POINT_COUNT];
    u16 flags;
    u8  position;
    bool looped;
}error_point_pool;

typedef enum
{
    ErrorPointPoolSource_Application    = 0,
    ErrorPointPoolSource_Boot           = 1,
}ErrorPointPoolSource;

void log_reset();
void log_push_error_point(u16 errorpoint);

#endif  //__LOG_H
