/**
  **************** (C) COPYRIGHT 2011 SCT Performance, LLC *********************
  * File Name          : usb_callback.h
  * Device / System    : SCT products using the "CommonCode" platform
  * Author             : Quyen Leba
  *
  * Version            : 1 
  * Date               : 05/02/2011
  * Description        : 
  *                    : 
  *
  *
  * History            : 05/02/2011 M. Davis
  *                    :   Add SCT Copyright header,
  *
  ******************************************************************************
  */

#ifndef __USB_CALLBACK_H
#define __USB_CALLBACK_H

#include <arch/gentype.h>
#include <board/genplatform.h>

#define CB_CBW_ID                       0xAA1655C7
#define CB_CSW_ID                       0x825517AA
#define CB_CBW_LENGTH                   sizeof(CB_CBW)
#define CB_CSW_LENGTH                   sizeof(CB_CSW)
#define SCTUSB_SECURITY_SEEDKEY_SIZE    32
#define USB_DATABUFFER_MAXSIZE          sizeof(usb_databuffer)

typedef struct
{
    u32 id;
    u16 tag;
    u16 cmd;            //used as CB_USBCommand
    u16 flags;
    u16 privatedata[3];
    u32 datalength;
}CB_CBW;

typedef struct
{
    u32 id;
    u16 tag;
    u16 status;
    u32 privatedata;
    u32 dataresidue;
}CB_CSW;

typedef enum
{
    CB_FC_IDLE,
    CB_FC_DATA_OUT,
    CB_FC_DATA_IN,
    CB_FC_DATA_IN_LAST,
    CB_FC_STATUS_SEND,
    CB_FC_ERROR,
}CB_FlowControl;

typedef struct
{
    u8              bulkbuffer[BULK_MAX_PACKET_SIZE];
    u32             bulkbufferlength;
    u8              securitybuffer[SCTUSB_SECURITY_SEEDKEY_SIZE];
    void            *pdev;
    struct
    {
        u8          validseed   : 1;
        u8          unlocked    : 1;
        u8          reserved    : 6;
    }control;
    CB_FlowControl  flowcontrol;
    u16             status;
    CB_CBW          cbw;
    CB_CSW          csw;
}USB_CALLBACK_INFO;

extern USB_CALLBACK_INFO usb_callback_info;
#define SCTUSB_LockSecurity()       usb_callback_info.control.unlocked = 0
#define SCTUSB_UnlockSecurity()     usb_callback_info.control.unlocked = 1
#define SCTUSB_isSecurityLocked()   (usb_callback_info.control.unlocked == 0)
#define SCTUSB_ValidSeed()          usb_callback_info.control.validseed = 1
#define SCTUSB_InvalidSeed()        usb_callback_info.control.validseed = 0
#define SCTUSB_IsSeedValid()        (usb_callback_info.control.validseed == 1)
#define SCTUSB_GetCmdStatus()       usb_callback_info.status
#define SCTUSB_SetCmdStatus(s)      usb_callback_info.status = s
#define SCTUSB_GetFlowControl()     usb_callback_info.flowcontrol
#define SCTUSB_SetFlowControl(f)    usb_callback_info.flowcontrol = f
#define SCTUSB_GetRequestedCmd()    usb_callback_info.cbw.cmd
#define SCTUSB_GetCmdFlags()        usb_callback_info.cbw.flags
#define SCTUSB_SetStatusID(i)       usb_callback_info.csw.id = i
#define SCTUSB_SetStatusTag(t)      usb_callback_info.csw.tag = t
#define SCTUSB_SetStatusPrivData(d) usb_callback_info.csw.privatedata = d
#define SCTUSB_StatusPrivData       usb_callback_info.csw.privatedata
#define SCTUSB_SetStatusResidue(r)  usb_callback_info.csw.dataresidue = r
#define SCTUSB_StatusResidue        usb_callback_info.csw.dataresidue

void usb_stall_all_ep(void *pdev);
void usb_stall_out_ep(void *pdev);
void usb_stall_in_ep(void *pdev);
void usb_send_data_to_host(u8 *buffer, u32 bufferlength);
void usb_trigger_send_databuffer_to_host();
void usb_set_csw(void *pdev, u16 status, bool send);

void usb_callback_ep1in(void  *pdev, u8 epnum);
void usb_callback_ep2out(void  *pdev, u8 epnum);

#endif	//__USB_CALLBACK_H

