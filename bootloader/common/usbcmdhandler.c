/**
  **************** (C) COPYRIGHT 2011 SCT Performance, LLC *********************
  * File Name          : usbcmdhandler.c
  * Device / System    : SCT products using the "CommonCode" platform
  * Author             : Quyen Leba
  *
  * Version            : 1 
  * Date               : 05/02/2011
  * Description        : 
  *                    : 
  *
  *
  * History            : 05/02/2011 M. Davis
  *                    :   Add SCT Copyright header,
  *
  ******************************************************************************
  */

//------------------------------------------------------------------------------
// Log error point: 0xBB00 -> 0xBBFF
//------------------------------------------------------------------------------

#include <string.h>
#include <ctype.h>
#include <device_config.h>
#include <board/genplatform.h>
#include <board/bootloader.h>
#include <common/statuscode.h>
#include <common/crypto_blowfish.h>
#include <common/crypto_messageblock.h>
#include <common/bootsettings.h>
#include <common/settings.h>
#include <common/tea.h>
#include <common/log.h>
#include <common/usb_callback.h>
#include "usbcmdhandler.h"

//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
extern u8  usb_databuffer[4096];        //from usb_callback.c
extern u32 usb_databufferindex;
extern u32 usb_databufferlength;

#if SUPPORT_USBCMD_FILETRANSFER
#include <fs/genfs.h>
#include <common/file.h>
struct
{
    F_FILE  *fileptr;
    u32 file_position;
    u32 file_length;
}usbcmdhandler_info =
{
    .fileptr = NULL,
};
#endif

//------------------------------------------------------------------------------
// Handle security seed setup command
// Engineer: Quyen Leba
//------------------------------------------------------------------------------
void uch_security_seed()
{
    u16 market;
    u8  seed_block[SCTUSB_SECURITY_SEEDKEY_SIZE];
    u8  encrypted_seed[MAX_CRYPTO_MESSAGE_BLOCK_LENGTH];
    u32 encrypted_seed_length;
    
    SCTUSB_InvalidSeed();
    
    market = usb_callback_info.cbw.privatedata[0];
    memset((char*)seed_block,0,sizeof(seed_block));
    if (usb_callback_info.cbw.datalength != MAX_CRYPTO_MESSAGE_BLOCK_LENGTH)
    {
        log_push_error_point(0xBB00);
        SCTUSB_SetCmdStatus(S_BADCONTENT);
    }
    else if (market == SUPPORT_DEVICE_MARKET)
    {
        u32 i;
        u32 *wptr;
        u32 tea_key[4];
        
        srand(get_systick_count());
        wptr = (u32*)seed_block;
        for(i=0;i<SCTUSB_SECURITY_SEEDKEY_SIZE/4;i++)
        {
            *wptr++ = rand();
        }
        
        tea_key[0] = TEA_KEY_0;
        tea_key[1] = TEA_KEY_1;
        tea_key[2] = TEA_KEY_2;
        tea_key[3] = TEA_KEY_3;
        memcpy(usb_callback_info.securitybuffer,
               seed_block,SCTUSB_SECURITY_SEEDKEY_SIZE);
        wptr = (u32*)usb_callback_info.securitybuffer;
        for(i=0;i<SCTUSB_SECURITY_SEEDKEY_SIZE/8;i++)
        {
            tea_encryption(wptr,tea_key);
            wptr+=2;
        }
        
        SCTUSB_SetCmdStatus(S_SUCCESS);
        SCTUSB_ValidSeed();
    }
    else
    {
        log_push_error_point(0xBB01);
        SCTUSB_SetCmdStatus(S_ERROR);
    }
    
    crypto_messageblock_encrypt(seed_block,sizeof(seed_block),
                                encrypted_seed,&encrypted_seed_length,FALSE,
                                CRYPTO_USE_INTERNAL_KEY);
    SCTUSB_SetStatusResidue(usb_callback_info.cbw.datalength - encrypted_seed_length);
    SCTUSB_SetFlowControl(CB_FC_DATA_IN_LAST);
    usb_send_data_to_host(encrypted_seed,encrypted_seed_length);
}

//------------------------------------------------------------------------------
// Handle security key validation command
// Engineer: Quyen Leba
//------------------------------------------------------------------------------
void uch_security_key()
{
    u8  decrypted_key[MAX_CRYPTO_MESSAGE_BLOCK_LENGTH];
    u32 decrypted_key_length;
    u16 status;
    
    SCTUSB_LockSecurity();
    if (SCTUSB_IsSeedValid())
    {
        if (usb_databufferlength == MAX_CRYPTO_MESSAGE_BLOCK_LENGTH)
        {
            status = crypto_messageblock_decrypt(usb_databuffer,usb_databufferlength,
                                                 decrypted_key,&decrypted_key_length,
                                                 FALSE,CRYPTO_USE_INTERNAL_KEY);
            if (status == S_SUCCESS &&
                decrypted_key_length == SCTUSB_SECURITY_SEEDKEY_SIZE)
            {
                if (memcmp((char*)decrypted_key,
                           (char*)usb_callback_info.securitybuffer,
                           SCTUSB_SECURITY_SEEDKEY_SIZE) == 0)
                {
                    SCTUSB_UnlockSecurity();
                    status = S_SUCCESS;
                }
                else
                {
                    log_push_error_point(0xBB02);
                    status = S_UNMATCH;
                    
                }
            }
            else
            {
                log_push_error_point(0xBB03);
                status = S_BADCONTENT;
            }
        }
        else
        {
            log_push_error_point(0xBB04);
            status = S_BADCONTENT;
        }
    }
    else
    {
        log_push_error_point(0xBB05);
        status = S_NOSEEDFOUND;
    }
    
    usb_set_csw(usb_callback_info.pdev,status,TRUE);
}

//------------------------------------------------------------------------------
// Handle bootloader commands
// Engineer: Quyen Leba
//------------------------------------------------------------------------------
void uch_bootloader()
{
    bool reboot;
    u8  status;
    
    reboot = FALSE;
    status = S_INPUT;
    
    switch((USBCmdBootloaderFlags)(usb_callback_info.cbw.flags))
    {
    case USBCmdBootloader_SetBoot:
        //data: [2: bootloadermode]
        if (usb_databufferlength == 2)
        {
            status = bootloader_setboot((BootloaderMode)(*(u16*)usb_databuffer));
            if (status == S_SUCCESS)
            {
                reboot = TRUE;
            }
        }
        break;
    case USBCmdBootloader_Init:
        //data: [64: firmware encrypted header]
        if (usb_databufferlength == 64)
        {
            u16 sector_size;
            status = bootloader_setup_session(usb_databuffer,&sector_size);
        }
        break;
    case USBCmdBootloader_Do:
        //data: [n: firmware encrypted block]
        status = bootloader_write_flash(usb_databuffer,usb_databufferlength);
        break;
    case USBCmdBootloader_Validate:
        status = bootloader_validate();
        if (status == S_SUCCESS)
        {
            reboot = TRUE;
        }
        break;
    case USBCmdBootloader_ClearEngSign:
        status = bootloader_clearengineeringsignature();
        break;
    case 0x3F:
        status = S_USER_EXIT;
        break;
    default:
        log_push_error_point(0xBB07);
        status = S_NOTSUPPORT;
        break;
    }
    
    usb_set_csw(usb_callback_info.pdev,status,TRUE);
    if (reboot)
    {
        watchdog_set();
    }
}

//------------------------------------------------------------------------------
// Handle get settings commands
// Engineer: Quyen Leba
//------------------------------------------------------------------------------
void uch_settings_get()
{
    u8  settingsbuffer[MAX_CRYPTO_MESSAGE_BLOCK_LENGTH];
    u32 settingsbufferlength;
    u8  settingsencryptedblock[MAX_CRYPTO_MESSAGE_BLOCK_LENGTH];
    u32 settingsencryptedblocklength;
    u8  status;
    
    settingsbufferlength = 0;
    settingsencryptedblocklength = 0;

    memset((char*)settingsencryptedblock,0,sizeof(settingsencryptedblocklength));
    status = S_SUCCESS;
    switch((USBCmdSettingsFlags)usb_callback_info.cbw.flags)
    {
    case USBCmdSettings_BootMode:
        status = bootsettings_get(BootSettingsOpcode_BootMode,
                                  settingsbuffer,&settingsbufferlength);
        break;
    case USBCmdSettings_FailsafeBootVersion:
        status = bootsettings_get(BootSettingsOpcode_FailSafeBootVersion,
                                  settingsbuffer,&settingsbufferlength);
        break;
    case USBCmdSettings_MainBootVersion:
        status = bootsettings_get(BootSettingsOpcode_MainBootVersion,
                                  settingsbuffer,&settingsbufferlength);
        break;
    case USBCmdSettings_AppVersion:
        status = bootsettings_get(BootSettingsOpcode_AppVersion,
                                  settingsbuffer,&settingsbufferlength);
        break;
    case USBCmdSettings_AppSignature:
        status = bootsettings_get(BootSettingsOpcode_AppSignature,
                                  settingsbuffer,&settingsbufferlength);
        break;
    case USBCmdSettings_SerialNumber:
        status = settings_getsettings_byopcode(SETTINGS_OPCODE_SERIAL,0,
                                               settingsbuffer,
                                               &settingsbufferlength);
        break;
    case USBCmdSettings_FailsafeBootCRC32E:
        status = bootsettings_get(BootSettingsOpcode_FailSafeBootCRC32E,
                                  settingsbuffer,&settingsbufferlength);
        break;
    case USBCmdSettings_MainBootCRC32E:
        status = bootsettings_get(BootSettingsOpcode_MainBootCRC32E,
                                  settingsbuffer,&settingsbufferlength);
        break;
    case USBCmdSettings_AppCRC32E:
        status = bootsettings_get(BootSettingsOpcode_AppCRC32E,
                                  settingsbuffer,&settingsbufferlength);
        break;
    case USBCmdSettings_CustomPID:
        status = bootsettings_get(BootSettingsOpcode_CustomUSBPID,
                                  settingsbuffer,&settingsbufferlength);
        break;
#if SUPPORT_USBCMD_FULL_GETSETTINGS
    case USBCmdSettings_Flags:
        status = settings_getsettings_byopcode(SETTINGS_OPCODE_FLAGS,
                                               usb_callback_info.cbw.privatedata[0],
                                               settingsbuffer,
                                               &settingsbufferlength);
        break;
#endif
    //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    case USBCmdSettings_DevicePartNumberString:
        status = settings_getsettings_byopcode(SETTINGS_OPCODE_DEVICE_PARTNUMBER_STRING,0,
                                               settingsbuffer,
                                               &settingsbufferlength);
        break;
    case USBCmdSettings_DeviceType:
        status = settings_getsettings_byopcode(SETTINGS_OPCODE_DEVICE_TYPE,0,
                                               settingsbuffer,
                                               &settingsbufferlength);
        break;
    case USBCmdSettings_MarketType:
        status = settings_getsettings_byopcode(SETTINGS_OPCODE_MARKET_TYPE,0,
                                               settingsbuffer,
                                               &settingsbufferlength);
        break;
#if SUPPORT_USBCMD_FULL_GETSETTINGS
    case USBCmdSettings_ProgrammerInfoP1:
        status = settings_getsettings_byopcode(SETTINGS_OPCODE_PROGRAMMER_INFO_P1,0,
                                               settingsbuffer,
                                               &settingsbufferlength);
        break;
    case USBCmdSettings_ProgrammerInfoP2:
        status = settings_getsettings_byopcode(SETTINGS_OPCODE_PROGRAMMER_INFO_P2,0,
                                               settingsbuffer,
                                               &settingsbufferlength);
        break;
    case USBCmdSettings_ProgrammerInfoP3:
        status = settings_getsettings_byopcode(SETTINGS_OPCODE_PROGRAMMER_INFO_P3,0,
                                               settingsbuffer,
                                               &settingsbufferlength);
        break;
    case USBCmdSettings_ProgrammerInfoP4:
        status = settings_getsettings_byopcode(SETTINGS_OPCODE_PROGRAMMER_INFO_P4,0,
                                               settingsbuffer,
                                               &settingsbufferlength);
        break;
    case USBCmdSettings_TuneRevision:
        status = settings_getsettings_byopcode(SETTINGS_OPCODE_TUNE_REVISION,0,
                                               settingsbuffer,
                                               &settingsbufferlength);
        break;
    //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    case USBCmdSettings_VIN:
        status = settings_getsettings_byopcode(SETTINGS_OPCODE_VIN,0,
                                               settingsbuffer,
                                               &settingsbufferlength);
        break;
    case USBCmdSettings_PreviousVIN:
        status = settings_getsettings_byopcode(SETTINGS_OPCODE_PREV_VIN,0,
                                               settingsbuffer,
                                               &settingsbufferlength);
        break;
    case USBCmdSettings_VID:
        status = settings_getsettings_byopcode(SETTINGS_OPCODE_VID,
                                               usb_callback_info.cbw.privatedata[0],
                                               settingsbuffer,
                                               &settingsbufferlength);
        break;
    case USBCmdSettings_EPATS:
        status = settings_getsettings_byopcode(SETTINGS_OPCODE_EPATS,0,
                                               settingsbuffer,
                                               &settingsbufferlength);
        break;
    case USBCmdSettings_VehicleCodes:
        status = settings_getsettings_byopcode(SETTINGS_OPCODE_VEHICLE_CODES,
                                               usb_callback_info.cbw.privatedata[0],
                                               settingsbuffer,
                                               &settingsbufferlength);
        break;
    case USBCmdSettings_VehicleSerialNumber:
        status = settings_getsettings_byopcode(SETTINGS_OPCODE_VEHICLE_SERIAL_NUMBER,0,
                                               settingsbuffer,
                                               &settingsbufferlength);
        break;
    case USBCmdSettings_StockFilesize:
        status = settings_getsettings_byopcode(SETTINGS_OPCODE_STOCKFILESIZE,0,
                                               settingsbuffer,
                                               &settingsbufferlength);
        break;
    case USBCmdSettings_TuneInfoTrack:
        status = settings_getsettings_byopcode(SETTINGS_OPCODE_TUNEINFOTRACK,
                                               usb_callback_info.cbw.privatedata[0],
                                               settingsbuffer,
                                               &settingsbufferlength);
        break;
    case USBCmdSettings_TuneHistoryInfo:
        status = settings_getsettings_byopcode(SETTINGS_OPCODE_TUNEHISTORYINFO,0,
                                               settingsbuffer,
                                               &settingsbufferlength);
        break;
    //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    case USBCmdSettings_ErrorPool:
        status = settings_getsettings_byopcode(SETTINGS_OPCODE_ERRORPOOL,
                                               usb_callback_info.cbw.privatedata[0],
                                               settingsbuffer,
                                               &settingsbufferlength);
        break;
    //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
#endif
    case USBCmdSettings_Properties:
        if(usb_callback_info.cbw.privatedata[0] == 0) // Supports Mainboard Only
        {
            status = properties_getinfo(settingsbuffer,&settingsbufferlength);
        }
        else
            status = S_NOTSUPPORT;
        break;
    default:
        log_push_error_point(0xBB10);
        status = S_NOTSUPPORT;
        break;
    }
    
    if (settingsbufferlength == 0 || settingsbufferlength > 56)
    {
        log_push_error_point(0xBB06);
        status = S_ERROR;
    }
    else
    {
        status = crypto_messageblock_encrypt(settingsbuffer,settingsbufferlength,
                                             settingsencryptedblock,
                                             &settingsencryptedblocklength,
                                             FALSE,CRYPTO_USE_INTERNAL_KEY);
    }
    
    if (status == S_SUCCESS &&
        (settingsencryptedblocklength == 0 || 
         settingsencryptedblocklength > MAX_CRYPTO_MESSAGE_BLOCK_LENGTH))
    {
        log_push_error_point(0xBB12);
        status = S_ERROR;
    }
    if (status != S_SUCCESS)
    {
        memset((char*)settingsencryptedblock,0,sizeof(settingsencryptedblock));
        usb_set_csw(usb_callback_info.pdev,status,TRUE);
        return;
    }
    SCTUSB_SetCmdStatus(status);
    
    SCTUSB_SetFlowControl(CB_FC_DATA_IN_LAST);
    usb_send_data_to_host(settingsencryptedblock,sizeof(settingsencryptedblock));
}

//------------------------------------------------------------------------------
// Handle set settings commands
// Engineer: Quyen Leba
//------------------------------------------------------------------------------
void uch_settings_set()
{
    u8  settingsbuffer[64];
    u32 settingsbufferlength;
    u8  *settingsencryptedblock;
    u32 settingsencryptedblocklength;
    u8  status;
    
    settingsencryptedblock = usb_databuffer;
    settingsencryptedblocklength = usb_databufferlength;
    status = S_SUCCESS;
    
    status = crypto_messageblock_decrypt(settingsencryptedblock,
                                         settingsencryptedblocklength,
                                         settingsbuffer,&settingsbufferlength,
                                         FALSE,CRYPTO_USE_INTERNAL_KEY);
    if (status != S_SUCCESS)
    {
        log_push_error_point(0xBB08);
        goto uch_settings_set_done;
    }

    switch((USBCmdSettingsFlags)usb_callback_info.cbw.flags)
    {
    case USBCmdSettings_BootMode:
        status = bootsettings_set(BootSettingsOpcode_BootMode,
                                  settingsbuffer,settingsbufferlength);
        break;
    case USBCmdSettings_FailsafeBootVersion:
        status = bootsettings_set(BootSettingsOpcode_FailSafeBootVersion,
                                  settingsbuffer,settingsbufferlength);
        break;
    case USBCmdSettings_MainBootVersion:
        status = bootsettings_set(BootSettingsOpcode_MainBootVersion,
                                  settingsbuffer,settingsbufferlength);
        break;
    case USBCmdSettings_AppVersion:
        status = bootsettings_set(BootSettingsOpcode_AppVersion,
                                  settingsbuffer,settingsbufferlength);
        break;
    case USBCmdSettings_AppSignature:
        status = bootsettings_set(BootSettingsOpcode_AppSignature,
                                  settingsbuffer,settingsbufferlength);
        break;
#if SUPPORT_USBCMD_FULL_SETSETTINGS
    case USBCmdSettings_SerialNumber:
        status = settings_setsettings_byopcode(SETTINGS_OPCODE_SERIAL,0,
                                               settingsbuffer,
                                               settingsbufferlength);
        break;
#endif  //SUPPORT_USBCMD_FULL_SETSETTINGS
    case USBCmdSettings_FailsafeBootCRC32E:
        status = bootsettings_set(BootSettingsOpcode_FailSafeBootCRC32E,
                                  settingsbuffer,settingsbufferlength);
        break;
    case USBCmdSettings_MainBootCRC32E:
        status = bootsettings_set(BootSettingsOpcode_MainBootCRC32E,
                                  settingsbuffer,settingsbufferlength);
        break;
    case USBCmdSettings_AppCRC32E:
        status = bootsettings_set(BootSettingsOpcode_AppCRC32E,
                                  settingsbuffer,settingsbufferlength);
        break;
    case USBCmdSettings_CustomPID:
        status = bootsettings_set(BootSettingsOpcode_CustomUSBPID,
                                  settingsbuffer,settingsbufferlength);
        break;
#if SUPPORT_USBCMD_FULL_SETSETTINGS
    case USBCmdSettings_Flags:
        status = settings_setsettings_byopcode(SETTINGS_OPCODE_FLAGS,
                                               usb_callback_info.cbw.privatedata[0],
                                               settingsbuffer,
                                               settingsbufferlength);
        break;
    case USBCmdSettings_DevicePartNumberString:
        status = settings_setsettings_byopcode(SETTINGS_OPCODE_DEVICE_PARTNUMBER_STRING,0,
                                               settingsbuffer,
                                               settingsbufferlength);
        break;
    case USBCmdSettings_DeviceType:
        status = settings_setsettings_byopcode(SETTINGS_OPCODE_DEVICE_TYPE,0,
                                               settingsbuffer,
                                               settingsbufferlength);
        break;
    case USBCmdSettings_MarketType:
        status = settings_setsettings_byopcode(SETTINGS_OPCODE_MARKET_TYPE,0,
                                               settingsbuffer,
                                               settingsbufferlength);
        break;
    //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    case USBCmdSettings_ProgrammerInfoP1:
        status = settings_setsettings_byopcode(SETTINGS_OPCODE_PROGRAMMER_INFO_P1,0,
                                               settingsbuffer,
                                               settingsbufferlength);
        break;
    case USBCmdSettings_ProgrammerInfoP2:
        status = settings_setsettings_byopcode(SETTINGS_OPCODE_PROGRAMMER_INFO_P2,0,
                                               settingsbuffer,
                                               settingsbufferlength);
        break;
    case USBCmdSettings_ProgrammerInfoP3:
        status = settings_setsettings_byopcode(SETTINGS_OPCODE_PROGRAMMER_INFO_P3,0,
                                               settingsbuffer,
                                               settingsbufferlength);
        break;
    case USBCmdSettings_ProgrammerInfoP4:
        status = settings_setsettings_byopcode(SETTINGS_OPCODE_PROGRAMMER_INFO_P4,0,
                                               settingsbuffer,
                                               settingsbufferlength);
        break;
    //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    case USBCmdSettings_VIN:
        status = settings_setsettings_byopcode(SETTINGS_OPCODE_VIN,0,
                                               settingsbuffer,
                                               settingsbufferlength);
        break;
    case USBCmdSettings_PreviousVIN:
        status = settings_setsettings_byopcode(SETTINGS_OPCODE_PREV_VIN,0,
                                               settingsbuffer,
                                               settingsbufferlength);
        break;
    case USBCmdSettings_VID:
        status = settings_setsettings_byopcode(SETTINGS_OPCODE_VID,
                                               usb_callback_info.cbw.privatedata[0],
                                               settingsbuffer,
                                               settingsbufferlength);
        break;
    case USBCmdSettings_EPATS:
        status = settings_setsettings_byopcode(SETTINGS_OPCODE_EPATS,0,
                                               settingsbuffer,
                                               settingsbufferlength);
        break;
    case USBCmdSettings_VehicleCodes:
        status = settings_setsettings_byopcode(SETTINGS_OPCODE_VEHICLE_CODES,
                                               usb_callback_info.cbw.privatedata[0],
                                               settingsbuffer,
                                               settingsbufferlength);
        break;
    case USBCmdSettings_VehicleSerialNumber:
        status = settings_setsettings_byopcode(SETTINGS_OPCODE_VEHICLE_SERIAL_NUMBER,0,
                                               settingsbuffer,
                                               settingsbufferlength);
        break;
    case USBCmdSettings_StockFilesize:
        status = settings_setsettings_byopcode(SETTINGS_OPCODE_STOCKFILESIZE,0,
                                               settingsbuffer,
                                               settingsbufferlength);
        break;
    case USBCmdSettings_TuneInfoTrack:
        status = settings_setsettings_byopcode(SETTINGS_OPCODE_TUNEINFOTRACK,
                                               usb_callback_info.cbw.privatedata[0],
                                               settingsbuffer,
                                               settingsbufferlength);
        break;
    case USBCmdSettings_TuneHistoryInfo:
        status = settings_setsettings_byopcode(SETTINGS_OPCODE_TUNEHISTORYINFO,0,
                                               settingsbuffer,
                                               settingsbufferlength);
        break;
    //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    case USBCmdSettings_ResetFactoryDefault:
        status = settings_setsettings_byopcode(SETTINGS_OPCODE_INIT_SETTINGS,0,
                                               settingsbuffer,
                                               settingsbufferlength);
        if (status == S_SUCCESS)
        {
            status = init_factorydefault();
        }
        break;
    case USBCmdSettings_ReInit:
        status = settings_setsettings_byopcode(SETTINGS_OPCODE_RESET_SETTINGS,0,
                                               settingsbuffer,
                                               settingsbufferlength);
        break;
#endif
    default:
        log_push_error_point(0xBB20);
        status = S_NOTSUPPORT;
        break;
    }
    if (status != S_SUCCESS)
    {
        log_push_error_point(0xBB21);
    }

uch_settings_set_done:
    usb_set_csw(usb_callback_info.pdev,status,TRUE);
}

//------------------------------------------------------------------------------
// Handle hardware commands
// Engineer: Quyen Leba
//------------------------------------------------------------------------------
#if SUPPORT_USBCMD_HARDWARE
void uch_hardware()
{
    u8  *bptrS;
    u8  *bptrD;
    bool isSetStatus;
    u8  status;

    isSetStatus = FALSE;
    switch((USBCmdHardwareFlags)usb_callback_info.cbw.flags)
    {
    case USBCmdHardware_GetDeviceInfoString:
        //input data: none
        //TODOQ:
        break;
    case USBCmdHardware_GetInfo:
        //input data: none
        //TODOQ:
        break;
    case USBCmdHardware_FTLInit:
        //input data: none
        //privatedata[0] is chunk_index; each chunk is 4096 bytes
        status = media_ftlinit(usb_callback_info.cbw.privatedata[0]);
        SCTUSB_SetStatusPrivData(MEDIA_TRAILING_DATA);
        usb_set_csw(usb_callback_info.pdev,status,TRUE);
        break;
    case USBCmdHardware_FTLRead:
        //input data: none
        status = media_ftlread(usb_databuffer,usb_callback_info.cbw.datalength,
                               &usb_databufferlength);
        SCTUSB_SetCmdStatus(status);
        if (status == S_SUCCESS)
        {
            if (usb_databufferlength == usb_callback_info.cbw.datalength &&
                usb_databufferlength > 0)
            {
                usb_trigger_send_databuffer_to_host();
            }
            else
            {
                usb_set_csw(usb_callback_info.pdev,S_FAIL,TRUE);
            }
        }
        else
        {
            usb_set_csw(usb_callback_info.pdev,status,TRUE);
        }
        break;
    case USBCmdHardware_FTLWrite:
        //input data: [4096: ftl block]
        status = media_ftlwrite(usb_databuffer,usb_callback_info.cbw.datalength);
        usb_set_csw(usb_callback_info.pdev,status,TRUE);
        break;
    case USBCmdHardware_FTLDeinit:
        //input data: none
        status = media_ftldeinit();
        usb_set_csw(usb_callback_info.pdev,status,TRUE);
        break;
    case USBCmdHardware_FilesystemReinit:
        //input data: none
        status = genfs_reinit();
        usb_set_csw(usb_callback_info.pdev,status,TRUE);
        break;
    case USBCmdHardware_FormatMedia:
        //input data: none
        status = genfs_format();
        usb_set_csw(usb_callback_info.pdev,status,TRUE);
        break;
    case USBCmdHardware_Listing:
        //input data: none
        //privatedata[0] is marker; zero to start from 1st entry, otherwise
        //from current entry
        //privatedata[1] is folder selection
        memset((char*)usb_databuffer,0,sizeof(usb_databuffer));
        status = genfs_get_listing_bulk(usb_callback_info.cbw.privatedata[0],
                                        usb_callback_info.cbw.privatedata[1],
                                        usb_callback_info.cbw.datalength,
                                        usb_databuffer, &usb_databufferlength);
        SCTUSB_SetCmdStatus(status);
        if (status == S_SUCCESS || status == S_TERMINATED)
        {
            if (usb_databufferlength > 0)
            {
                SCTUSB_SetStatusPrivData(usb_databufferlength);
                usb_databufferlength = usb_callback_info.cbw.datalength;
                usb_trigger_send_databuffer_to_host();
            }
            else
            {
                usb_set_csw(S_FAIL,TRUE);
            }
        }
        else
        {
            usb_set_csw(status,TRUE);
        }
        break;
    case USBCmdHardware_GetFreeSpace:
        //input data: none
        SCTUSB_SetStatusPrivData(genfs_getfreespace());
        usb_set_csw(usb_callback_info.pdev,S_SUCCESS,TRUE);
        break;
    case USBCmdHardware_GetCapacity:
        //input data: none
        SCTUSB_SetStatusPrivData(genfs_getcapacity());
        usb_set_csw(usb_callback_info.pdev,S_SUCCESS,TRUE);
        break;
    case USBCmdHardware_GetFileSize:
        //input data: [n:filename][NULL]
        status = S_BADCONTENT;
        if (usb_databufferlength < USB_DATABUFFER_MAXSIZE)
        {
            usb_databuffer[usb_databufferlength] = NULL;
            status = genfs_getfilesize(usb_databuffer, &SCTUSB_StatusPrivData);
        }
        usb_set_csw(usb_callback_info.pdev,status,TRUE);
        break;
    case USBCmdHardware_DeleteFile:
        //input data: [n:filename][NULL]
        status = S_BADCONTENT;
        if (usb_databufferlength < USB_DATABUFFER_MAXSIZE)
        {
            usb_databuffer[usb_databufferlength] = NULL;
            status = genfs_deletefile(usb_databuffer);
        }
        usb_set_csw(usb_callback_info.pdev,status,TRUE);
        break;
    case USBCmdHardware_RenameFile:
        //input data: [n:existing filename][,][m:new filename][NULL]
        status = S_BADCONTENT;
        if (usb_databufferlength < USB_DATABUFFER_MAXSIZE)
        {
            usb_databuffer[usb_databufferlength] = NULL;
            bptrD = (u8*)strstr((char*)usb_databuffer,",");
            if (bptrD != NULL && (bptrD - usb_databuffer < usb_databufferlength))
            {
                *bptrD++ = NULL;
                bptrS = usb_databuffer;
                if (strlen((char*)bptrS)+strlen((char*)bptrD)+2 <= usb_databufferlength)
                {
                    status = genfs_renamefile(bptrS,bptrD);
                }
            }
        }
        usb_set_csw(usb_callback_info.pdev,status,TRUE);
        break;
    case USBCmdHardware_MoveFile:
        //input data: [n:src filename][,][m:dest filename][NULL]
        status = S_BADCONTENT;
        if (usb_databufferlength < USB_DATABUFFER_MAXSIZE)
        {
            usb_databuffer[usb_databufferlength] = NULL;
            bptrD = (u8*)strstr((char*)usb_databuffer,",");
            if (bptrD != NULL && (bptrD - usb_databuffer < usb_databufferlength))
            {
                *bptrD++ = NULL;
                bptrS = usb_databuffer;
                if (strlen((char*)bptrS)+strlen((char*)bptrD)+2 <= usb_databufferlength)
                {
                    status = genfs_movefile(bptrS,bptrD);
                }
            }
        }
        usb_set_csw(usb_callback_info.pdev,status,TRUE);
        break;
    case USBCmdHardware_TruncateFile:
        //input data: [4:newfilesize][n:filename][NULL]
        status = S_BADCONTENT;
        if (usb_databufferlength < USB_DATABUFFER_MAXSIZE &&
            usb_databufferlength >= 10)
        {
            usb_databuffer[usb_databufferlength] = NULL;
            status = genfs_truncatefile(&usb_databuffer[4],
                                        *(u32*)usb_databuffer);
        }
        usb_set_csw(usb_callback_info.pdev,status,TRUE);
        break;
    case USBCmdHardware_CopyFile:
        //input data: [n:src filename][,][m:dest filename][NULL]
        status = S_BADCONTENT;
        if (usb_databufferlength < USB_DATABUFFER_MAXSIZE)
        {
            usb_databuffer[usb_databufferlength] = NULL;
            bptrD = (u8*)strstr((char*)usb_databuffer,",");
            if (bptrD != NULL && (bptrD - usb_databuffer < usb_databufferlength))
            {
                *bptrD++ = NULL;
                bptrS = usb_databuffer;
                if (strlen((char*)bptrS)+strlen((char*)bptrD)+2 <= usb_databufferlength)
                {
                    status = genfs_copyfile(bptrS,bptrD);
                }
            }
        }
        usb_set_csw(usb_callback_info.pdev,status,TRUE);
        break;
    case USBCmdHardware_VerifyFile:
        //input data: [4:filesize][4:file crc32e][n:filename][NULL]
        status = S_BADCONTENT;
        if (usb_databufferlength < USB_DATABUFFER_MAXSIZE &&
            usb_databufferlength >= 14)
        {
            usb_databuffer[usb_databufferlength] = NULL;
            status = genfs_verifyfile(&usb_databuffer[8],
                                      *(u32*)&usb_databuffer[0],
                                      *(u32*)&usb_databuffer[4]);
        }
        usb_set_csw(usb_callback_info.pdev,status,TRUE);
        break;
    case USBCmdHardware_ShowMessage:
        //input data: [n:message][NULL]
        status = S_BADCONTENT;
        if (usb_databufferlength < USB_DATABUFFER_MAXSIZE)
        {
            usb_databuffer[usb_databufferlength] = NULL;
            //TODOQ: display message
            status = S_SUCCESS;
        }
        usb_set_csw(usb_callback_info.pdev,status,TRUE);
        break;
    case USBCmdHardware_GetAnalogData:
        usb_set_csw(usb_callback_info.pdev,S_NOTSUPPORT,TRUE);
        break;
    case USBCmdHardware_SetBaudrate:
        usb_set_csw(usb_callback_info.pdev,S_NOTSUPPORT,TRUE);
        break;
    case USBCmdHardware_SetVolume:
        usb_set_csw(usb_callback_info.pdev,S_NOTSUPPORT,TRUE);
        break;
    case USBCmdHardware_SetBrightness:
        usb_set_csw(usb_callback_info.pdev,S_NOTSUPPORT,TRUE);
        break;
    //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    case USBCmdHardware_Unpair:
        status = hardware_access(HardwareAccess_Unpair,NULL,NULL,NULL);
        usb_set_csw(status,TRUE);
        break;
    case USBCmdHardware_GetFriendlyName:
        memset((char*)usb_databuffer,0,32);
        status = hardware_access(HardwareAccess_GetFriendlyName,NULL,
                                 usb_databuffer,&usb_databufferlength);
        SCTUSB_SetCmdStatus(status);
        if (status == S_SUCCESS && usb_databufferlength > 0)
        {
            //friendly name max length is 24+NULL, however, always report
            //32 bytes back to HOST
            usb_databuffer[31] = NULL;
            usb_databufferlength = 32;
            usb_trigger_send_databuffer_to_host();
        }
        else
        {
            usb_set_csw(S_FAIL,TRUE);
        }
        break;
    case USBCmdHardware_SetFriendlyName:
        status = hardware_access(HardwareAccess_SetFriendlyName,usb_databuffer,
                                 NULL,NULL);
        usb_set_csw(status,TRUE);
        break;
    case USBCmdHardware_GetMacAddress:
        memset((char*)usb_databuffer,0,32);
        status = hardware_access(HardwareAccess_GetMacAddress,NULL,
                                 usb_databuffer,&usb_databufferlength);
        SCTUSB_SetCmdStatus(status);
        if (status == S_SUCCESS && usb_databufferlength > 0)
        {
            //MAC address is 00:11:22:33:44:55, however, always report
            //32 bytes back to HOST
            usb_databuffer[17] = NULL;
            usb_databufferlength = 32;
            usb_trigger_send_databuffer_to_host();
        }
        else
        {
            usb_set_csw(S_FAIL,TRUE);
        }
        break;
    case USBCmdHardware_SetMacAddress:
        status = hardware_access(HardwareAccess_SetMacAddress,usb_databuffer,
                                 NULL,NULL);
        usb_set_csw(status,TRUE);
        break;
    //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    default:
        status = S_INPUT;
        isSetStatus = TRUE;
        break;
    }

    if (isSetStatus)
    {
        usb_set_csw(usb_callback_info.pdev,status,TRUE);
    }
}
#endif  //SUPPORT_USBCMD_HARDWARE

#if SUPPORT_USBCMD_FILETRANSFER
//------------------------------------------------------------------------------
// Open file to start a file transfer session
// Return:  u8  status
// Engineer: Quyen Leba
//------------------------------------------------------------------------------
u8 uch_openfile()
{
    u32 totalbytetransfer;
    u8  status;

    status = S_SUCCESS;
    uch_closefile();
    //privatedata[0..1] is total bytes to transfer (to write or append)
    totalbytetransfer = *((u32*)usb_callback_info.cbw.privatedata);

    switch((USBCmdFileFlags)SCTUSB_GetCmdFlags())
    {
    case USBCmdFile_Read:
        usbcmdhandler_info.fileptr = genfs_openfile(usb_databuffer,"r");
        usbcmdhandler_info.file_length = file_getsize_byfptr(usbcmdhandler_info.fileptr);
        SCTUSB_SetStatusPrivData(usbcmdhandler_info.file_length);
        break;
    case USBCmdFile_Write:
        usbcmdhandler_info.fileptr = genfs_openfile(usb_databuffer,"w+");
        usbcmdhandler_info.file_length = totalbytetransfer;
        break;
    case USBCmdFile_Append:
        usbcmdhandler_info.fileptr = genfs_openfile(usb_databuffer,"a+");
        usbcmdhandler_info.file_position = file_getsize_byfptr(usbcmdhandler_info.fileptr);
        usbcmdhandler_info.file_length = usbcmdhandler_info.file_position + totalbytetransfer;
        SCTUSB_SetStatusPrivData(usbcmdhandler_info.file_position);
        break;
    default:
        status = S_BADCONTENT;
        break;
    }
    if (status == S_SUCCESS)
    {
        if (usbcmdhandler_info.fileptr == NULL)
        {
            uch_closefile();
            status = S_OPENFILE;
        }
    }

    usb_set_csw(status,TRUE);
    return status;
}

//------------------------------------------------------------------------------
// Read and prepare block data from file to transfer to HOST
// Engineer: Quyen Leba
//------------------------------------------------------------------------------
u8 uch_readfile()
{
    if (usbcmdhandler_info.fileptr)
    {
        if (usb_callback_info.cbw.datalength > USB_DATABUFFER_MAXSIZE ||
            usb_callback_info.cbw.datalength == 0)
        {
            usb_set_csw(S_BADCONTENT,TRUE);
            return S_BADCONTENT;
        }

        usb_databufferindex = 0;
        usb_databufferlength = fread(usb_databuffer,
                                     1,USB_DATABUFFER_MAXSIZE,
                                     usbcmdhandler_info.fileptr);
        usbcmdhandler_info.file_position += usb_databufferlength;
        
        if (usbcmdhandler_info.file_position >= usbcmdhandler_info.file_length)
        {
            uch_closefile();
        }
        else if (usb_databufferlength == 0)
        {
            uch_closefile();
            usb_set_csw(S_READFILE,TRUE);
            return S_READFILE;
        }

        SCTUSB_SetCmdStatus(S_SUCCESS);
        usb_trigger_send_databuffer_to_host();
    }
    else
    {
        usb_set_csw(S_OPENFILE,TRUE);
        return S_OPENFILE;
    }
    return S_SUCCESS;
}

//------------------------------------------------------------------------------
// Process data received from HOST and write it to file
// Engineer: Quyen Leba
//------------------------------------------------------------------------------
u8 uch_writefile()
{
    u8  status;

    if (usbcmdhandler_info.fileptr)
    {
        u32 bytecount;

        status = S_SUCCESS;
        bytecount = fwrite(usb_databuffer,
                           1,usb_databufferlength,usbcmdhandler_info.fileptr);
        if (bytecount != usb_databufferlength)
        {
            status = S_WRITEFILE;
        }
        else
        {
            usbcmdhandler_info.file_position += bytecount;
            if (usbcmdhandler_info.file_position >= usbcmdhandler_info.file_length)
            {
                status = uch_closefile();
            }
        }
    }
    else
    {
        status = S_OPENFILE;
    }

    usb_set_csw(status,TRUE);
    return status;
}

//------------------------------------------------------------------------------
// Close a file transfer session; if any
// Return:  u8  status
// Engineer: Quyen Leba
//------------------------------------------------------------------------------
u8 uch_closefile()
{
    if (usbcmdhandler_info.fileptr)
    {
        genfs_closefile(usbcmdhandler_info.fileptr);
        usbcmdhandler_info.fileptr = NULL;
    }
    usbcmdhandler_info.file_position = 0;
    usbcmdhandler_info.file_length = 0;
    
    return S_SUCCESS;
}
#endif  //SUPPORT_USBCMD_FILETRANSFER
