/**
  **************** (C) COPYRIGHT 2011 SCT Performance, LLC *********************
  * File Name          : device_config.h
  * Device / System    : SCT products using the "CommonCode" platform
  * Author             : Quyen Leba
  *
  * Version            : 1 
  * Date               : 05/02/2011
  * Description        : 
  *                    : 
  *
  *
  * History            : 05/02/2011 M. Davis
  *                    :   Add SCT Copyright header,
  *
  ******************************************************************************
  */

#ifndef __DEVICE_CONFIG_H
#define __DEVICE_CONFIG_H

#define DEFAULT_USB_PID                 0x4040
#define SUPPORT_DEVICE_MARKET           0x1234      //for now as example
#define TEA_KEY_0                       0x0F38409F
#define TEA_KEY_1                       0x03C350A2
#define TEA_KEY_2                       0xD6E59954
#define TEA_KEY_3                       0x43542102

#define ENGINEERING_SIGNATURE           0x5F1C83A0

#endif	//__DEVICE_CONFIG_H
