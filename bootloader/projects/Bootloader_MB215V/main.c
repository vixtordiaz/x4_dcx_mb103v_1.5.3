/**
  **************** (C) COPYRIGHT 2011 SCT Performance, LLC *********************
  * File Name          : main.c
  * Device / System    : SCT products using the "CommonCode" platform
  * Author             : Quyen Leba
  *
  * Version            : 1 
  * Date               : 05/02/2011
  * Description        : 
  *                    : 
  *
  *
  * History            : 05/02/2011 M. Davis
  *                    :   Add SCT Copyright header,
  *
  ******************************************************************************
  */

#include <board/genplatform.h>
#include <board/bootloader.h>
#include <common/cmdif.h>

#include <board/MB215V/usart.h>

u32 CriticalSecCntr;

//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
int main(void)
{
    init();

    u32 i = 0;
    if (i == 1)
    {
        bootloader_force_application_validation();
    }
    if (i == 2)
    {
        bootloader_force_mainboot_validation();
    }
main_loop:
    cmdif_handler();
    goto main_loop;
}
