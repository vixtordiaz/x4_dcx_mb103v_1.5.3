/**
  **************** (C) COPYRIGHT 2011 SCT Performance, LLC *********************
  * File Name          : gentype.h
  * Device / System    : SCT products using the "CommonCode" platform
  * Author             : Quyen Leba
  *
  * Version            : 1 
  * Date               : 05/02/2011
  * Description        : 
  *                    : 
  *
  *
  * History            : 05/02/2011 M. Davis
  *                    :   Add SCT Copyright header,
  *
  ******************************************************************************
  */

#ifndef __GENTYPE_H
#define __GENTYPE_H

#include "bit.h"

#ifdef __STR91x_ARCH__
#include "STR91x/include/91x_type.h"
#elif __STM32F10x_ARCH__
#include <stm32f10x.h>
typedef enum {FALSE, TRUE} bool;
#elif __STM32F2xx_ARCH__
#include <stm32f2xx.h>
typedef enum {FALSE, TRUE} bool;
#else
#error "gentype.h: Invalid ARCH"
#endif

typedef union
{
    u8 c[4];
	u32 i;
}u32ext;

#endif	//__GENTYPE_H
