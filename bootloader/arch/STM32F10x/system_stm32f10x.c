/**
  **************** (C) COPYRIGHT 2011 SCT Performance, LLC *********************
  * File Name          : system_stm32f10x.c
  * Device / System    : SCT products using the "CommonCode" platform
  * Author             : Quyen Leba
  *
  * Version            : 1 
  * Date               : 05/02/2011
  * Description        : 
  *                    : 
  *
  *
  * History            : 05/02/2011 M. Davis
  *                    :   Add SCT Copyright header,
  *
  ******************************************************************************
  */

#include <board/clock.h>
#include <board/interrupt.h>

//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
void SystemInit (void)
{
    clock_init();
    interrupt_init();
}
