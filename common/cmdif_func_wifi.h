/**
  **************** (C) COPYRIGHT 2011 SCT Performance, LLC *********************
  * File Name          : cmdif_func_wifi.h
  * Device / System    : SCT products using the "CommonCode" platform
  * Author             : Quyen Leba
  *
  * Version            : 1 
  * Date               : 05/02/2011
  * Description        : 
  *                    : 
  *
  *
  * History            : 05/02/2011 M. Davis
  *                    :   Add SCT Copyright header,
  *
  ******************************************************************************
  */

#ifndef __CMDIF_FUNC_WIFI_H
#define __CMDIF_FUNC_WIFI_H

#include <arch/gentype.h>

#define SERVER_REQUEST_INFO_FILENAME        GENFS_USER_FOLDER_PATH"info.json"
#define SERVER_RESPONSE_INFO_FILENAME       GENFS_USER_FOLDER_PATH"resp.json"
#define TR_UPDATEFILELIST_FILENAME          GENFS_USER_FOLDER_PATH"trupdate.json"
#define TR_DOWNLOADFILELIST_FILENAME        GENFS_USER_FOLDER_PATH"trdownloadlist.bin"
#define SERVER_TUNEFILELIST_FILENAME        GENFS_USER_FOLDER_PATH"ctdownloadlist.bin"
#define CTB_DIF_FILENAME                    GENFS_USER_FOLDER_PATH"ctbdiflist.dfl"

typedef enum
{
    CMDIF_WIFI_STARTSCAN        = 0x01,
    CMDIF_WIFI_GETAPINFO        = 0x02,
    CMDIF_WIFI_STOPSCAN         = 0x03,
    CMDIF_WIFI_ADDPROFILE       = 0x04,
    CMDIF_WIFI_DELETEPROFILE    = 0x05,
    CMDIF_WIFI_RESETANDCONNECT  = 0x06,
    CMDIF_WIFI_CONNECT          = 0x07,
    CMDIF_WIFI_DISCONNECT       = 0x08,
}CMDIF_WIFI_OPCODE;

typedef struct
{
    u32 version;
    u32 fwsize;
    u32 fwcrc32e;
    u8  url[128];
}SERVER_FW;

typedef struct
{
  SERVER_FW mboot;
  SERVER_FW app;
}SERVER_FW_INFO;

typedef struct
{
    u8  tunedescription[32];
    u32 marketID;
    u8  datecreated[16];
    u8  filename[48];
    u32 storedfilesize;
    u32 crc32e;
    u32 filetypeID;
    u8  rowguid[36];
    u32 encrpytiontypeID;
}SERVER_TUNEFILE_INFO;

typedef struct
{
    u32 mb_fboot_version;
    u32 mb_mboot_version;
    u32 mb_app_version;
    u32 vb_fboot_version;
    u32 vb_mboot_version;
    u32 vb_app_version;
    u32 ab_fboot_version;
    u32 ab_mboot_version;
    u32 ab_app_version;
}FW_VERSIONS;

typedef struct
{
    u32 version;
    u8 tuneversion[32];
    u8 partnumber[16];
    u32 filecount;
}TRUPDATE_HEADER;

typedef struct
{
    u8 filename[40];
    u32 filesize;
    u32 filecrc32e;
    u8 rowguid[36];
}TRUPDATE_BLOCK;

typedef enum    //must match ServerAccessType (LWTS) & tkBaseServerAccessType (X4)
{
    CMDIF_ServerAccessType_Firmware             = 0,
    CMDIF_ServerAccessType_TuneRevision         = 1,
    CMDIF_ServerAccessType_CustomTunes          = 2,
    CMDIF_ServerAccessType_SPF                  = 3,
}CMDIF_ServerAccessType;

// Diff File List Format ".DIF"
#define DIF_VERSION    0
typedef struct
{
    u32 version;
    u32 filecount;
    u32 headercrc32e;
    u32 contentcrc32e;
    u8  reservered[16];
}DIFF_FILE_HEADER; // 32 bytes total
STRUCT_SIZE_CHECK(DIFF_FILE_HEADER,32);

typedef struct
{
    u8  description[32];
    u8  filename[48];
    u32 filesize;
    u32 filecrc32e;
    u8  rowguid[36];
    u8  reserved[4];
}DIFF_FILE_BLOCK; // 128 bytes total
STRUCT_SIZE_CHECK(DIFF_FILE_BLOCK,128);

u8 cmdif_func_wifi_check_update(CMDIF_ServerAccessType update_request);
u8 cmdif_func_wifi_download_update(CMDIF_ServerAccessType update_request);
u8 cmdif_func_wifi_apply_update(CMDIF_ServerAccessType update_request);

void cmdif_func_wifi_get_fwversions(FW_VERSIONS *versions);
u8 cmdif_func_wifi_handleconnect(CMDIF_WIFI_OPCODE opcode, u8 *datain);
u8 cmdif_func_wifi_status(void);

u8 cmdif_func_wifi_tunes_check_update(void);
u8 cmdif_func_wifi_tunes_process_tunelist(void);
u8 cmdif_func_wifi_process_tunelist(void);
u8 cmdif_func_wifi_tunes_check_tune(SERVER_TUNEFILE_INFO tuneinfo);
u8 cmdif_func_wifi_download_files_by_access_string(CMDIF_ServerAccessType type,
                                                   u8 *access_string, u8* dest_filename);
u8 cmdif_func_wifi_get_cloudtune_list(void);
u8 cmdif_func_wifi_check_limit();
u8 cmdif_func_wifi_download_cloud_file(u16 index);
u8 cmdif_func_wifi_deactivate();

#endif    //__CMDIF_FUNC_WIFI_H
