/**
  **************** (C) COPYRIGHT 2011 SCT Performance, LLC *********************
  * File Name          : bootsettings.h
  * Device / System    : SCT products using the "CommonCode" platform
  * Author             : Quyen Leba
  *
  * Version            : 1 
  * Date               : 05/02/2011
  * Description        : 
  *                    : 
  *
  *
  * History            : 05/02/2011 M. Davis
  *                    :   Add SCT Copyright header,
  *
  ******************************************************************************
  */

#ifndef __BOOTSETTINGS_H
#define __BOOTSETTINGS_H

#include <arch/gentype.h>
#include <common/log.h>

#define SETTINGS_SIGNATURE      0x2C1B5A74

typedef struct
{
    u32 signature;
    u32 crc32e;
    u32 engineering_signature;
    u32 failsafeboot_version;
    u32 mainboot_version;
    u32 app_version;
    u32 app_signature;
    u16 bootmode;               //defined by BOOTMODE
    
    // --------------------------------------------------------------------------
    // bootflags and bootstatus are used during "Firmware Update By File" process 
    // --------------------------------------------------------------------------    
    struct
    {
        // bootflags - Indicates if particular a Firmare Update needs to be completed 
        //              use by both bootloader and app firmware
        u8 mbBootUpdateFromFileDone : 1; // When Update From File Complete clear these flags
        u8 mbAppUpdateFromFileDone  : 1; // 
        u8 vbBootUpdateFromFileDone : 1; // 
        u8 vbAppUpdateFromFileDone  : 1; // 
        u8 abBootUpdateFromFileDone : 1; //
        u8 abAppUpdateFromFileDone  : 1; //
        u8 reserved1     : 1;
        u8 reserved2     : 1;
    }bootflags;    
    u8 reserved;    
        
    u16 customUSBPIDsignature;
    u16 customUSBPID;
    u32 failsafeboot_crc32e;
    u32 mainboot_crc32e;
    u32 app_crc32e;
    u32 app_length;

    error_point_pool errorpointpool;
    
    u32 wifipatchversioncheck;
    
    struct
    {
        // bootstatus - Indicates SUCCESS or ERROR during bootload status
        u8 mbBootUpdateStatus;
        u8 mbAppUpdateStatus;
        u8 reserved[6];
    }bootstatus;    
}BootSettings;

typedef enum
{
    BootSettingsOpcode_Signature            = 0,
    BootSettingsOpcode_FailSafeBootVersion  = 1,
    BootSettingsOpcode_MainBootVersion      = 2,
    BootSettingsOpcode_AppVersion           = 3,
    BootSettingsOpcode_AppSignature         = 4,
    BootSettingsOpcode_BootMode             = 5,
    BootSettingsOpcode_FailSafeBootCRC32E   = 6,
    BootSettingsOpcode_MainBootCRC32E       = 7,
    BootSettingsOpcode_AppCRC32E            = 8,
    BootSettingsOpcode_AppLength            = 9,
    BootSettingsOpcode_CustomUSBPID         = 10,
}BootSettingsOpcode;

extern BootSettings bootsettings;
#define BOOTSETTINGS(v)                         bootsettings.##v
#define BOOTSETTINGS_GetBootMode()              bootsettings.bootmode
#define BOOTSETTINGS_SetBootMode(m)             bootsettings.bootmode = m

#define BOOTSETTINGS_GetMbBootUpdateFromFileDone()      bootsettings.bootflags.mbBootUpdateFromFileDone
#define BOOTSETTINGS_SetMbBootUpdateFromFileDone(m)     bootsettings.bootflags.mbBootUpdateFromFileDone = m
#define BOOTSETTINGS_GetMbAppUpdateFromFileDone()       bootsettings.bootflags.mbAppUpdateFromFileDone
#define BOOTSETTINGS_SetMbAppUpdateFromFileDone(m)      bootsettings.bootflags.mbAppUpdateFromFileDone = m

#define BOOTSETTINGS_GetMbBootUpdateStatus()            bootsettings.bootstatus.mbBootUpdateStatus
#define BOOTSETTINGS_SetMbBootUpdateStatus(m)           bootsettings.bootstatus.mbBootUpdateStatus = m
#define BOOTSETTINGS_GetMbAppUpdateStatus()             bootsettings.bootstatus.mbAppUpdateStatus
#define BOOTSETTINGS_SetMbAppUpdateStatus(m)            bootsettings.bootstatus.mbAppUpdateStatus = m

#define BOOTSETTINGS_GetVbBootUpdateFromFileDone()      bootsettings.bootflags.vbBootUpdateFromFileDone
#define BOOTSETTINGS_SetVbBootUpdateFromFileDone(m)     bootsettings.bootflags.vbBootUpdateFromFileDone = m
#define BOOTSETTINGS_GetVbAppUpdateFromFileDone()       bootsettings.bootflags.vbAppUpdateFromFileDone
#define BOOTSETTINGS_SetVbAppUpdateFromFileDone(m)      bootsettings.bootflags.vbAppUpdateFromFileDone = m

#define BOOTSETTINGS_GetAbBootUpdateFromFileDone()      bootsettings.bootflags.abBootUpdateFromFileDone
#define BOOTSETTINGS_SetAbBootUpdateFromFileDone(m)     bootsettings.bootflags.abBootUpdateFromFileDone = m
#define BOOTSETTINGS_GetAbAppUpdateFromFileDone()       bootsettings.bootflags.abAppUpdateFromFileDone
#define BOOTSETTINGS_SetAbAppUpdateFromFileDone(m)      bootsettings.bootflags.abAppUpdateFromFileDone = m


#define BOOTSETTINGS_GetFailsafeBootVersion()   bootsettings.failsafeboot_version
#define BOOTSETTINGS_SetFailsafeBootVersion(v)  bootsettings.failsafeboot_version = v
#define BOOTSETTINGS_GetMainBootVersion()       bootsettings.mainboot_version
#define BOOTSETTINGS_GetMainBootCRC32E()        bootsettings.mainboot_crc32e
#define BOOTSETTINGS_SetMainBootCRC32E(c)       bootsettings.mainboot_crc32e = c
#define BOOTSETTINGS_GetAppCRC32E()             bootsettings.app_crc32e
#define BOOTSETTINGS_SetAppCRC32E(c)            bootsettings.app_crc32e = c
#define BOOTSETTINGS_GetAppVersion()            bootsettings.app_version
#define BOOTSETTINGS_SetAppVersion(v)           bootsettings.app_version = v
#define BOOTSETTINGS_GetAppSignature()          bootsettings.app_signature
#define BOOTSETTINGS_GetAppLength()             bootsettings.app_length
#define BOOTSETTINGS_SetAppLength(l)            bootsettings.app_length = l
#define BOOTSETTINGS_GetEngSign()               bootsettings.engineering_signature
#define BOOTSETTINGS_ClearEngSign()             bootsettings.engineering_signature = 0
#define BOOTSETTINGS_GetCustomUSBPIDSignature() bootsettings.customUSBPIDsignature
#define BOOTSETTINGS_GetCustomUSBPID()          bootsettings.customUSBPID
#define BOOTSETTINGS_GetWifiPatchVersion()      bootsettings.wifipatchversioncheck
#define BOOTSETTINGS_SetWifiPatchVersion(v)     bootsettings.wifipatchversioncheck = v

u8 bootsettings_load();
u8 bootsettings_update();
u8 bootsettings_retoredefault();
u8 bootsettings_get(u16 opcode, u8 *settingsdata, u32 *settingsdata_length);
u8 bootsettings_set(u16 opcode, u8 *settingsdata, u32 settingsdata_length);
u8 bootsettings_verifycorrect();

#endif    //__BOOTSETTINGS_H
