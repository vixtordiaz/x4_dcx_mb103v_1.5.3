/*
 *  Common Code Platform
 *
 *  Copyright 2014 SCT Performance, LLC
 *
 */

/**
 * @file    rsa.h
 * @brief   Header for RSA functions
 * @date    8/2/2014 
 * @author  Tristen Pierson
 */

#ifndef RSA_H_
#define RSAS_H_

#define MOD_SIZE    32  /* Modulus size in DWORD's */

u8 rsa_calc_signature_me9uk(u32 start, u32 end, u8 *modulus, u32 signature_address);
u8 rsa_decrypt(u8 *modulus, u8 *public_exponent, u8 *signature, u8 *result);

#endif
