#include "cloud_lib.h"
#include "statuscode.h"
#include <board/genplatform.h>
#include <fs/genfs.h>
#include <board/CC3000/cc3000.h>
#include "crypto_blowfish.h"
#include <protobuf-c/protobuf_lib.h>
#include <protobuf-c/protobuf-c.h>
#include <protobuf-c/cloud.pb-c.h>
#include "settings.h"
#include "file.h"

#define CLOUD_HOST                      "api-stage.derivesystems.com"
#define CLOUD_CREDENTIALS_FILE          GENFS_USER_FOLDER_PATH"cred.gsf"
#define CLOUD_TOKEN_DATA_FILE           GENFS_USER_FOLDER_PATH"tokendata.gsf"
#define CLOUD_TOKEN_EXPIRATION_FILE     GENFS_USER_FOLDER_PATH"tokenexp.gsf"
#define CLOUD_SEARCH_FILE_LIST          GENFS_USER_FOLDER_PATH"search.gsf"

#define GUID_STRING_LENGTH  20


#define CLOUDHTTPPOSTDATA_INIT \
 { CLOUD_HOST, NULL, "application/x-protobuf", NULL, 0, NULL, 0, NULL, 0 }

typedef struct 
{
  u8* host;
  u8* resource;
  u8* contenttype;
  u8* content;
  u32 contentlength;
  u8* token;
  u32 tokenlength;
  u8 messagebuffer[0x400];
  u32 messagelength;
}CloudHTTPPostData;

typedef struct
{
    u8 password[128];
    u8 username[128];
    u8 domainname[128];
    u8 pin[8];
}CloudCredentials; // Keep struc size multiple of 8 because it's used with encryption

typedef struct
{
    u8* data;
    u32 length;
}CloudToken;

typedef struct
{
    u8 id[40];
    u8 filename[64];
    u32 length;
    u32 crc32e;
}CloudDownloadItem;

typedef struct
{
    u8 serialnumber[16];
    u8 filename[64];
    u8** extentions;
    u32 n_extentions;
    u32 skip;
    u32 take;
}CloudSearch;

typedef struct 
{
    u8 version;    
    u8 reserved[3];
    u32 count;
}CloudSearchResultsHeader;

typedef struct
{
    u8 ctb_description[64];
    CloudDownloadItem ctb;
    CloudDownloadItem supportfiles[3];
    u8 supportfile_count;
    u8 download;
    u8 reserved[6];
}CloudSearchResultsItem;

static u8 cloud_file_save(u8 *filename, u8 *data, u32 length);
static u8 cloud_file_load(u8 *filename, u8 *data, u32 length);
static u8 cloud_file_contentlength(u8 *filename, u32 *length);
static u8 cloud_get_file_list(u8 *serialnumber, u8** extensions, u32 n_extensions);
static u8 cloud_load_token(CloudToken *token);
static u8 cloud_authenticate();
static u8 cloud_search(CloudSearch search, SearchResponse** responseMsg);
static u8 cloud_download(CloudDownloadItem file, PROGRESS_REPORT_FUNC_SIMPLE progress_report_func);

static u8 cloud_http_get(CloudHTTPPostData* httppostdata, u8 *filename, PROGRESS_REPORT_FUNC_SIMPLE progress_report_func);
static u8 cloud_http_post(CloudHTTPPostData* httppostdata, PROGRESS_REPORT_FUNC_SIMPLE progress_report_func);


u32 cloud_currenttime = 0;    

u8 test_func()
{
    CloudCheckLimit limitdata;
    u8 serialnumber[14];
    u8 status;
    
    bool activate = FALSE;
    bool authenticate = FALSE;
    bool deactivate = FALSE;
    bool search = TRUE;
    bool checkupdate_tunes = FALSE;
    bool download = FALSE;
    bool checklimit = FALSE;
    
    do
    {
        settings_get_deviceserialnumber(serialnumber);
        
        if(activate)
        {
            status = cloud_activate(serialnumber);
            if (status != S_SUCCESS && status != S_NOTREQUIRED) break;
        }
        
        if(authenticate)
        {
            status = cloud_authenticate();
            if (status != S_SUCCESS) break;
        }
        
        if(checkupdate_tunes)
        {
            u8 *tunelist;
            u32 tunelength;
            
            status = cloud_update_tunelist(serialnumber);
            if(status == S_SUCCESS)
            {
                status = cloud_get_tunelist(&tunelist, &tunelength);
                if(status == S_SUCCESS)
                {
                    __free(tunelist);
                }
            }
        }
        
        if(search)
        {
            //SearchResponse* responseMsg;
            u8* extensions[] = {"ctb"};
            
            status = cloud_get_file_list(serialnumber, extensions, 1);
            if(status != S_SUCCESS)
            {
                
            }
        }
        
        if(checklimit)
        {
            status = cloud_checklimit(serialnumber, &limitdata);
            if (status != S_SUCCESS) break;
        }
        
        if(download)
        {
            //u8 *filedata;
            //u32 length;
            //CloudSearchResultsHeader *header;
            
            status = cloud_download_file_by_searchlist_index(0, NULL);
            if(status != S_SUCCESS)
            {
                
            }
            
//            status = cloud_file_contentlength(CLOUD_SEARCH_FILE_LIST, &length);
//            if(status == S_SUCCESS)                
//            {
//                filedata = __malloc(length);
//                if(filedata)
//                {
//                    status = cloud_file_load(CLOUD_SEARCH_FILE_LIST, filedata, length);
//                    
//                    header = (CloudSearchResultsHeader*) filedata;
//                    item = (CloudSearchResultsItem*) (filedata+sizeof(CloudSearchResultsHeader));
//                    
//                    for(int i = 0; i < header->count; i++)
//                    {
//                        status = cloud_download(item[i], NULL);
//                        if (status != S_SUCCESS) break;
//                    }
//                    
//                    __free(filedata);
//                }
//            }
        }
        
        if(deactivate)
        {
            settings_get_deviceserialnumber(serialnumber);
            status = cloud_deactivate(serialnumber);
            if(status != S_SUCCESS) break;
        }
        
    } while(0);
   
    
    return S_SUCCESS; //TODO: change this back to returning status when done.
}

u8 cloud_update_tunelist(u8 *serialnumber)
{
    u8* extensions[] = {"ctb"};
    u8 status;
    u8 count;
    u8 *filedata;
    u32 length;
    CloudSearchResultsHeader *header;
    CloudSearchResultsItem *item;
    int i, j;
    
    status = cloud_get_file_list(serialnumber, extensions, 1);
    if(status == S_SUCCESS)
    {
        // Compare files and mark files not in filesystem for download        
        status = cloud_file_contentlength(CLOUD_SEARCH_FILE_LIST, &length);
        if(status == S_SUCCESS)
        {
            filedata = __malloc(length);
            if(filedata)
            {
                status = cloud_file_load(CLOUD_SEARCH_FILE_LIST, filedata, length);
                if(status == S_SUCCESS)
                {
                    header = (CloudSearchResultsHeader*) filedata;
                    item = (CloudSearchResultsItem*) (filedata+sizeof(CloudSearchResultsHeader));
                    
                    count = header->count;
                    
                    for(i = 0; i < count; i++)
                    {
                        status = genfs_verifyfile(item[i].ctb.filename, item[i].ctb.length, item[i].ctb.crc32e); // Check if file already downloaded
                        if(status == S_MALLOC)
                        {
                            log_push_error_point(0x4E0E);
                            break;
                        }
                        else if(status == S_SUCCESS)
                        {
                            // Check if all supportfiles downloaded
                            for(j = 0; j < item[i].supportfile_count; j++)
                            {
                                status = genfs_verifyfile(item[i].supportfiles[j].filename, item[i].supportfiles[j].length, item[i].supportfiles[j].crc32e);
                                if(status != S_SUCCESS)
                                {
                                    item[i].download = TRUE;
                                    break;
                                }
                            }
                        }
                        else
                        {
                            item[i].download = TRUE;
                        }
                    }
                    if(status != S_MALLOC)
                    {
                        status = cloud_file_save(CLOUD_SEARCH_FILE_LIST, filedata, length);
                    }
                }
                __free(filedata);
            }
            else
            {
                log_push_error_point(0x4E12);
                status = S_MALLOC;
            }
        }
    }
    
    return status;
}


u8 cloud_download_file_by_searchlist_index(u16 index, PROGRESS_REPORT_FUNC_SIMPLE progress_report_func)
{
    u8 status;
    u8 *filedata;
    u32 length;
    u16 localindex;
    CloudSearchResultsHeader *header;
    CloudSearchResultsItem *item;
    CloudDownloadItem file;
    u32 i, j;
    
    status = cloud_file_contentlength(CLOUD_SEARCH_FILE_LIST, &length);
    if(status == S_SUCCESS)
    {
        filedata = __malloc(length);
        if(filedata)
        {
            status = cloud_file_load(CLOUD_SEARCH_FILE_LIST, filedata, length);
            if(status == S_SUCCESS)
            {
                header = (CloudSearchResultsHeader*) filedata;
                item = (CloudSearchResultsItem*) (filedata+sizeof(CloudSearchResultsHeader));
                
                if(index < header->count)
                {
                    localindex = 0;
                    for(i=0;i<header->count;i++)
                    {
                        if(item[i].download)
                        {
                            if(localindex == index)
                            {
                                // Download tune 
                                strcpy((char*)file.filename, (char const*)item[i].ctb.filename);
                                strcpy((char*)file.id, (char const*)item[i].ctb.id);
                                file.length = item[i].ctb.length;
                                file.crc32e = item[i].ctb.crc32e;
                                
                                status = cloud_download(file, progress_report_func);
                                if(status == S_SUCCESS)
                                {
                                    // Download Supporting files, SPF, DLX, etc                                    
                                    for(j = 0; j < item[i].supportfile_count; j++)
                                    {
                                        // Check if file already exists on device
                                        status = genfs_verifyfile(item[i].supportfiles[j].filename, item[i].supportfiles[j].length, item[i].supportfiles[j].crc32e);
                                        if(status != S_SUCCESS)
                                        {
                                            strcpy((char*)file.filename, (char const*)item[i].supportfiles[j].filename);
                                            strcpy((char*)file.id, (char const*)item[i].supportfiles[j].id);
                                            file.length = item[i].supportfiles[j].length;
                                            file.crc32e = item[i].supportfiles[j].crc32e;
                                            
                                            status = cloud_download(file, progress_report_func);
                                            if(status != S_SUCCESS)
                                            {
                                                log_push_error_point(0x4E24); // Failed to download support file
                                                break;
                                            }
                                        }
                                    }
                                }
                                else
                                {
                                    log_push_error_point(0x4E25);   //Failed to download file
                                }
                                break;
                            }
                            localindex++;
                        }
                    }
                }
            }
            __free(filedata);
        }
    }
    
    return status;
}
    

u8 cloud_handle_activation_authentication(u8 *serialnumber)
{
    u8 status;
    u32 retry;
    u64 expiration;
    
    retry = 2;
    do
    {
        status = cloud_activate(serialnumber);
        if(status == S_SUCCESS || status == S_NOTREQUIRED)
        {
            wifi_hal_getNtpTime(&cloud_currenttime);
            
            // Check if token is expired    
            status = cloud_file_load(CLOUD_TOKEN_EXPIRATION_FILE, (u8*)&expiration, sizeof(expiration)); // Load Token data
            if(status == S_SUCCESS)
            {
                // compare time
                status = S_EXPIRED;
                if(cloud_currenttime && (cloud_currenttime < (u32)expiration))
                {
                    status = S_SUCCESS;
                }
                else
                {
                    log_push_error_point(0x4E44); // track token expired, not an error
                }
            }
            else
            {
                log_push_error_point(0x4E45); // track token expired, not an error
            }
            
            if(status == S_SUCCESS)
            {
                break;
            }
            else
            {
                // Expired get new token
                status = cloud_authenticate();
                if(status == S_SUCCESS)
                {
                    break; // Done
                }
                else
                {
                    log_push_error_point(0x4E26);
                    // If authentication fails credentials file probably old, delete it and try again
                    genfs_deletefile(CLOUD_CREDENTIALS_FILE);
                }
            }
        }
        else
        {
            log_push_error_point(0x4E27); // Failed to activate
            break;
        }
        retry--;
    }while(retry);
    
    return status;
}
    
u8 cloud_get_tunelist(u8 **list, u32 *list_length)
{
    u8 status;
    u16 count;
    u8 *filedata;
    u32 length;
    CloudSearchResultsHeader *header;
    CloudSearchResultsItem *item;    
    u8* tuneliststring;
    u8* tunelistptr;
    u16 tunecount;
    
    status = cloud_file_contentlength(CLOUD_SEARCH_FILE_LIST, &length);
    if(status == S_SUCCESS)
    {
        filedata = __malloc(length);
        if(filedata)
        {
            status = cloud_file_load(CLOUD_SEARCH_FILE_LIST, filedata, length);
            if(status == S_SUCCESS)
            {
                header = (CloudSearchResultsHeader*) filedata;
                item = (CloudSearchResultsItem*) (filedata+sizeof(CloudSearchResultsHeader));
                
                count = (u16)header->count;
                
                // Get the Tune List byte length
                tunecount = 0;
                length = 2; // First 2 bytes are the count of tunes in list
                for(int i = 0; i < count; i++)
                {
                    if(item[i].download)
                    {
                        length += strlen((const char*)item[i].ctb_description)+1; // Tune Desciption length + NULL
                        tunecount++;
                    }
                }
                
                tuneliststring = __malloc(length);
                if(tuneliststring)
                {
                    memcpy(tuneliststring, (u8*)&tunecount, 2);
                    
                    tunelistptr = tuneliststring + 2;
                    for(u16 i = 0; i < count; i++)
                    {
                        if(item[i].download)
                        {
                            strcpy((char*)tunelistptr, (const char*)item[i].ctb_description);
                            tunelistptr += strlen((const char*)item[i].ctb_description)+1;
                        }
                    }
                    *list = tuneliststring;
                    *list_length = length;
                }
                else
                {
                    log_push_error_point(0x4E2A);
                    status = S_MALLOC;
                }
            }
            __free(filedata);
        }
    }
    
    return status;
}

u8 cloud_http_post(CloudHTTPPostData* httppostdata, PROGRESS_REPORT_FUNC_SIMPLE progress_report_func)
{
    u32 headerlen;
    u8 status = S_SUCCESS;
    u8* buf = NULL;
    u8* authdata = NULL;
    const u8* authstring = "Authorization: %s\r\n";
    
    //Encrypt with blowfish
    crypto_blowfish_encryptblock_external_key(httppostdata->content,
                                               httppostdata->contentlength);
    
    //Send message
    do {
        if (httppostdata->token)
        {
            //allocate buffer
            authdata = __malloc(httppostdata->tokenlength + strlen((const char*)authstring) + 1);
            
            if (authdata)
            { 
                sprintf((char*) authdata,
                        (const char*) authstring, // "Authorization: [token]\r\n"
                        httppostdata->token);
                
                buf = __malloc(httppostdata->tokenlength + 0x400);
            }
            else
            {
                status = S_MALLOC;
            }
            
        }
        else
        {
            authdata = __malloc(sizeof(u8));
            
            if (authdata)
            {
                authdata[0] = 0x00;  // No authorization token needed
                
                buf = __malloc(0x400);
            }
            else
            {
                status = S_MALLOC;
            }
        }
    } while (0);
    
    if ( buf && authdata)
    {
        //"POST /test/interface.php HTTP/1.1\r\n
        //Host: hostname.com\r\n
        //Content-Length: 23\r\n
        //Content-Type: application/x-www-form-urlencoded;\r\n\r\n
        //&function=xxxxxxxxxxxxx";
      
        headerlen = sprintf((char*) buf, 
                                "POST %s HTTP/1.1\r\n" 
                                "Host: %s\r\nContent-Length: %d\r\n"
                                "Content-Type: %s\r\n"
                                "%s" //Authorization: [TOKEN]\r\n -- if needed
                                "\r\n",
                                httppostdata->resource,
                                httppostdata->host,
                                httppostdata->contentlength,
                                httppostdata->contenttype,
                                authdata);
        
        memcpy((char*) &buf[headerlen],
                  (char*) httppostdata->content, 
                  httppostdata->contentlength);
        
        status = wifi_hal_handle_http_request(httppostdata->host,
                                          buf, 
                                          headerlen + httppostdata->contentlength, 
                                          GENFS_USER_FOLDER_PATH"resp.prot", 
                                          progress_report_func);
        if(status == S_SUCCESS)
        {
        //Recieve message
            do {
                F_FILE *f;
                
                f = genfs_user_openfile(GENFS_USER_FOLDER_PATH"resp.prot","rb");
                
                if (f)
                {
                   httppostdata->messagelength = fread(httppostdata->messagebuffer,
                                          1,
                                          sizeof(httppostdata->messagebuffer),
                                          f);
                }
                else
                {
                  status = S_ERROR; //TODO: Error
                }
                genfs_closefile(f);           
            } while(0);
            
            // Check for valid data
            if (httppostdata->messagebuffer[0] == 'I'  &&
                  httppostdata->messagebuffer[1] == 'n' &&
                  httppostdata->messagebuffer[2] == 'v')  //if it says Invalid
            {
                status = S_ERROR;
            }
            
            // Check for valid data
            if (httppostdata->messagebuffer[0] == 'T'  &&
                  httppostdata->messagebuffer[1] == 'h' &&
                  httppostdata->messagebuffer[2] == 'e')  //If it says "The server has..."
            {
                status = S_ERROR;
            }
            
            //Decrypt message
            if (status == S_SUCCESS)
            {
                crypto_blowfish_decryptblock_external_key(httppostdata->messagebuffer,
                                                            httppostdata->messagelength);
                
                httppostdata->messagebuffer[httppostdata->messagelength] = 0x00;
            }
        }
    }
    else
    {
        status = S_MALLOC;
    }
    
    // cleanup
    if (authdata) __free(authdata);
    if (buf) __free(buf);
    
    return status;
}

u8 cloud_http_get(CloudHTTPPostData* httppostdata, u8 *filename, PROGRESS_REPORT_FUNC_SIMPLE progress_report_func)
{
    u32 headerlen;
    u8 status = S_SUCCESS;
    u8* buf = NULL;
    u8* authdata = NULL;
    const u8* authstring = "Authorization: %s\r\n";
    
    //Encrypt with blowfish
    crypto_blowfish_encryptblock_external_key(httppostdata->content,
                                               httppostdata->contentlength);
    
    //Send message
    do {
    if (httppostdata->token)
    {
        //allocate buffer
        authdata = __malloc(httppostdata->tokenlength + strlen((const char*)authstring) + 1);
        
        if (authdata)
        { 
            sprintf((char*) authdata,
                    (const char*) authstring, // "Authorization: [token]\r\n"
                    httppostdata->token);
            
            buf = __malloc(httppostdata->tokenlength + 0x400);
        }
        else
        {
            status = S_MALLOC;
        }
        
    }
    else
    {
        authdata = __malloc(sizeof(u8));
        
        if (authdata)
        {
            authdata[0] = 0x00;  // No authorization token needed
            
            buf = __malloc(0x400);
        }
        else
        {
            status = S_MALLOC;
        }
    }
    } while (0);
    
    if ( buf && authdata)
    {
        //"POST /test/interface.php HTTP/1.1\r\n
        //Host: hostname.com\r\n
        //Content-Length: 23\r\n
        //Content-Type: application/x-www-form-urlencoded;\r\n\r\n
        //&function=xxxxxxxxxxxxx";
      
        headerlen = sprintf((char*) buf, 
                                "POST %s HTTP/1.1\r\n" 
                                "Host: %s\r\nContent-Length: %d\r\n"
                                "Content-Type: %s\r\n"
                                "%s" //Authorization: [TOKEN]\r\n -- if needed
                                "\r\n",
                                httppostdata->resource,
                                httppostdata->host,
                                httppostdata->contentlength,
                                httppostdata->contenttype,
                                authdata);
        
        memcpy((char*) &buf[headerlen],
                  (char*) httppostdata->content, 
                  httppostdata->contentlength);
        
        // Send request and handle retrys
        u32 retry = 3;
        do
        {
            status = wifi_hal_handle_http_request(httppostdata->host,
                                                  buf, 
                                                  headerlen + httppostdata->contentlength, 
                                                  GENFS_USER_FOLDER_PATH"resp.prot", 
                                                  progress_report_func);
            if(status == S_SUCCESS)
            {
                // File is saved with transport layer encrypytion, 
                // need to decrypt and copy to correct filename
                file_blockcrypto bf;        
                bf.bfcrypto.type = file_blockcrypto_type_bf;
                bf.bfcrypto.ctx = (blowfish_context_ptr*)&m_external_ctx;
                bf.bfcrypto.crypto = &crypto_blowfish_decryptblock_full;
                
                status = file_copy(GENFS_USER_FOLDER_PATH"resp.prot", filename, &bf, NULL, NULL);
                break;
            }
            retry--;
        }while(retry);
        
        if(!retry)
        {
            log_push_error_point(0x4E2B); // Failed to post
        }
    }
    else
    {
        status = S_MALLOC;
    }
    
    // cleanup
    if (authdata) __free(authdata);
    if (buf) __free(buf);
    
    return status;
}

typedef struct
{
    u8  version;
    u8  reserved[3];
    u32 header_crc32e;
    u32 content_crc32e;
    u32 content_length;
}GsfHeader; // Generic Storage File

u8 cloud_file_save(u8 *filename, u8 *data, u32 length)
{
    F_FILE* f;
    u32 len;
    u8 status;
    GsfHeader header;
        
    f = genfs_user_openfile(filename,"wb");
    if (f)
    {
        // Populate Header data
        header.version = 0;
        header.header_crc32e = 0xFFFFFFFF;
        header.content_length = length;
        
        // File content is encrypted with blowfish data
        crypto_blowfish_encryptblock_external_key((u8*)data, length);
        
        // Caluclate content CRC32E on encrypted data
        crc32_reset();
        header.content_crc32e = crc32e_calculateblock(header.content_crc32e, 
                                                      (u32*)data, (length/4));
        
        // Header now fully populated, calculate header CRC32E
        crc32_reset();
        header.header_crc32e = crc32e_calculateblock(header.header_crc32e, 
                                                     (u32*)&header, (sizeof(header)/4));
        
        crypto_blowfish_encryptblock_external_key((u8*)&header, sizeof(header));
       
        status = S_WRITEFILE;
        len = fwrite((char*)&header,1,sizeof(header),f); // Write header
        if(len == sizeof(header))
        {
            len = fwrite((char*)data,1,length,f); // Write content
            if(len == length)
            {
                status = S_SUCCESS;
            }
        }
        genfs_closefile(f);
    }
    else
    {
        log_push_error_point(0x4E2C);
        status = S_OPENFILE;
    }
    
    return status;
}

u8 cloud_file_contentlength(u8 *filename, u32 *length)
{
    GsfHeader header;
    F_FILE* f;
    u32 crc32e_file;
    u8 status;
    
    f = genfs_user_openfile(filename,"rb");
    if (f)
    {
        if(fread((char*)&header,1,sizeof(header),f) == sizeof(header)) // Read Header data
        {
            // Validate Header data
            crypto_blowfish_decryptblock_external_key((u8*)&header, sizeof(header));
            crc32e_file = header.header_crc32e;            
            crc32_reset();
            header.header_crc32e = 0xFFFFFFFF;
            header.header_crc32e = crc32e_calculateblock(header.header_crc32e, 
                                                         (u32*)&header, (sizeof(header)/4));
            
            if(crc32e_file == header.header_crc32e)
            {
                *length = header.content_length;
                status = S_SUCCESS;
            }
        }
        else
        {
            status = S_READFILE;
        }
        genfs_closefile(f);
    }
    else
    {
        status = S_OPENFILE;
    }
    
    return status;
}

u8 cloud_file_load(u8 *filename, u8 *data, u32 length)
{
    GsfHeader header;
    F_FILE* f;
    u32 crc32e_file;
    u8 status;
        
    f = genfs_user_openfile(filename,"rb");
    if (f)
    {
        status = S_READFILE;
        
        if(fread((char*)&header,1,sizeof(header),f) == sizeof(header)) // Read Header data
        {
            // Validate Header data
            crypto_blowfish_decryptblock_external_key((u8*)&header, sizeof(header));
            crc32e_file = header.header_crc32e;            
            crc32_reset();
            header.header_crc32e = 0xFFFFFFFF;
            header.header_crc32e = crc32e_calculateblock(header.header_crc32e, 
                                                         (u32*)&header, (sizeof(header)/4));
            if(crc32e_file == header.header_crc32e)
            {
                if(header.content_length <= length)
                {
                    if(fread((char*)data,1,header.content_length,f) == header.content_length) // Read content data
                    {
                        // Validate Content Data
                        crc32e_file = header.content_crc32e;                    
                        crc32_reset();
                        header.content_crc32e = 0xFFFFFFFF;
                        header.content_crc32e = crc32e_calculateblock(header.content_crc32e, 
                                                                      (u32*)data, (header.content_length/4));
                        if(crc32e_file == header.content_crc32e)
                        {
                            status = S_SUCCESS;
                            crypto_blowfish_decryptblock_external_key((u8*)data, header.content_length); //Decrypt content
                        }
                        else
                        {
                            status = S_CRC32E;
                        }
                    }
                }
                else
                {
                    status = S_INPUT;
                }
            }
            else
            {
                status = S_CRC32E;
            }
        }
        genfs_closefile(f);
    }
    else
    {
        status = S_OPENFILE;
    }
    
    return status;
}

u8 cloud_activate(u8 *serialnumber) //Activate function
{
    CloudHTTPPostData httppostdata = CLOUDHTTPPOSTDATA_INIT;
    ActivateRequest msg = ACTIVATE_REQUEST__INIT;
    ActivateResponse* responseMsg;
    void *buf;                     // Buffer to store serialized data
    u32 len;                  // Length of serialized data
    u32 bodyLen;
    u8 status = S_SUCCESS;
    CloudCredentials info;

    httppostdata.resource = "/ActivateDevice";
    
    status = cloud_file_load(CLOUD_CREDENTIALS_FILE, (u8*)&info, sizeof(info));
    if(status == S_SUCCESS)
    {
        status = S_NOTREQUIRED; // Already activated - Use deactivate to delete credentials file to activate again
    }
    else
    {
        msg.serialnumber = (char*)serialnumber;
        
        len = activate_request__get_packed_size(&msg);
        bodyLen = len + (8 -(len % 8)); // buffer padded to 8 bytes for crypto
        
        buf =(void*) __malloc(bodyLen);
        if (buf)
        {
            memset(buf, 0, bodyLen);
            
            activate_request__pack(&msg,buf);   // Serialize using protobuf
            
            httppostdata.content =(u8*) buf;
            httppostdata.contentlength = bodyLen;
            
            status = cloud_http_post(&httppostdata, NULL); //Send Request
            if (status == S_SUCCESS)
            {
                responseMsg = activate_response__unpack(NULL,
                                                        httppostdata.messagelength, 
                                                        httppostdata.messagebuffer); //Deserialize
                if (responseMsg)
                {
                    // The device is activated once and stays active until it's 
                    // Deactivated. Save activation info for whenever we need to 
                    // request a new token.
                    status = S_INPUT;
                    do
                    {
                        // Check response data lengths and copy to CloudActivateFile structure
                        if(strlen((char*)responseMsg->domainname) < sizeof(info.domainname)) // Domain Name                    
                        {
                            strcpy((char*)info.domainname,(char*)responseMsg->domainname); 
                        }
                        else
                        {
                            log_push_error_point(0x4E2D);
                            break;
                        }                    
                        if(strlen((char*)responseMsg->username) < sizeof(info.username)) // User Name
                        {
                            strcpy((char*)info.username,(char*)responseMsg->username); 
                        }
                        else
                        {
                            log_push_error_point(0x4E2E);
                            break;
                        }
                        if(strlen((char*)responseMsg->password) < sizeof(info.password)) // Password
                        {
                            strcpy((char*)info.password,(char*)responseMsg->password); 
                        }
                        else
                        {
                            log_push_error_point(0x4E2F);
                            break;
                        }
                        if(strlen((char*)responseMsg->pin) < sizeof(info.pin)) // PIN
                        {
                            strcpy((char*)info.pin,(char*)responseMsg->pin);
                        }
                        else
                        {
                            log_push_error_point(0x4E30);
                            break;                        
                        }
                        
                        status = cloud_file_save(CLOUD_CREDENTIALS_FILE, (u8*)&info, sizeof(info));
                    }
                    while(0);
                    
                    // Free memory
                    activate_response__free_unpacked(responseMsg,NULL);
                }
                else
                {
                    log_push_error_point(0x4E31);
                    status = S_MALLOC;
                }
            }
            
            __free(buf); // Free the allocated serialized buffer
        }
        else
        {
            log_push_error_point(0x4E32);
            status = S_MALLOC;
        }
    }

    return status;
}

u8 cloud_deactivate(u8 *serialnumber) //Deactivate function
{ 
    CloudHTTPPostData httppostdata = CLOUDHTTPPOSTDATA_INIT;
    DeactivateRequest msg = DEACTIVATE_REQUEST__INIT;
    DeactivateResponse* responseMsg;
    void *buf;                     // Buffer to store serialized data
    u32 len;                  // Length of serialized data
    u32 bodyLen;
    CloudToken token;
    u8 status;

    status = cloud_load_token(&token); // NOTE: This function malloc's token.data, remember to free
    if(status == S_SUCCESS)
    {
        httppostdata.token = token.data;
        httppostdata.tokenlength = token.length;
        
        // Pack Message
        msg.serialnumber = (char*)serialnumber;
        httppostdata.resource = "/DeactivateDevice";
        
        len = deactivate_request__get_packed_size(&msg);
        bodyLen = len + (8 -(len % 8)); // buffer padded to 8 bytes for crypto
        
        buf =(void*) __malloc(bodyLen);
        if (buf)
        {
            memset(buf, 0, bodyLen);
            
            deactivate_request__pack(&msg,buf);
            
            httppostdata.content =(u8*) buf;
            httppostdata.contentlength = bodyLen;
            
            //Send Request
            status = cloud_http_post(&httppostdata, NULL);
            if (status == S_SUCCESS)
            {
                //Deserialize
                responseMsg = deactivate_response__unpack(NULL,
                                                          httppostdata.messagelength, 
                                                          httppostdata.messagebuffer);
                if (responseMsg)
                {
                    //Process message               
                    if(responseMsg->deactivated)
                    {
                        status = genfs_deletefile(CLOUD_CREDENTIALS_FILE); // Delete credentials file upon successful deactivation
                    }
                    else
                    {
                        status = S_FAIL;
                    }
                    
                    // Free memory
                    deactivate_response__free_unpacked(responseMsg,NULL);
                }
                else
                {
                    status = S_MALLOC;
                }
            }
            
            __free(buf); // Free the allocated serialized buffer
        }
        else
        {
            status = S_MALLOC;
        }
        if(token.data)
        {
            __free(token.data);
        }
    }

    return status;
}

u8 cloud_getpin(u8* pin)
{
    u8 status;
    CloudCredentials info;
    
    status = cloud_file_load(CLOUD_CREDENTIALS_FILE, (u8*)&info, sizeof(info));
    if(status == S_SUCCESS)
    {
        strcpy((char*)pin, (const char*)info.pin);
    }
    
    return status;    
}

u8 cloud_authenticate()
{
    CloudHTTPPostData httppostdata = CLOUDHTTPPOSTDATA_INIT;
    AuthenticateRequest msg = AUTHENTICATE_REQUEST__INIT;
    AuthenticateResponse* responseMsg;
    void *buf;                      // Buffer to store serialized data
    u32 len;                        // Length of serialized data
    u32 bodyLen;
    CloudCredentials info;
    
    u8 status = S_SUCCESS;
    httppostdata.resource = "/Authenticate";
    
    status = cloud_file_load(CLOUD_CREDENTIALS_FILE, (u8*)&info, sizeof(info));
    if(status == S_SUCCESS)
    {
        msg.domainname = (char*) info.domainname;
        msg.username = (char*) info.username;
        msg.password = (char*) info.password;
        
        len = authenticate_request__get_packed_size(&msg);
        bodyLen = len + (8 -(len % 8)); // buffer padded to 8 bytes for crypto
        
        buf =(void*) __malloc(bodyLen);
        if (buf)
        {
            memset(buf, 0, bodyLen);
            
            authenticate_request__pack(&msg,buf);
            
            httppostdata.content =(u8*) buf;
            httppostdata.contentlength = bodyLen;
            
            status = cloud_http_post(&httppostdata, NULL); //Send Request
            if (status == S_SUCCESS)
            {
                //Deserialize
                responseMsg = authenticate_response__unpack(NULL,
                                                            httppostdata.messagelength, 
                                                            httppostdata.messagebuffer);
                if (responseMsg)
                {                    
                    // Save Token Data to file, 
                    // NOTE: There is no max token length and the size will vary
                    u8 *data = (u8*)responseMsg->token;
                    u32 length = strlen(responseMsg->token)+1; // +1 NULL
                    
                    status = cloud_file_save(CLOUD_TOKEN_DATA_FILE, (u8*)data, length);
                    if(status == S_SUCCESS)
                    {
                        // Save Token Expiration to file
                        u64 expiration;
                        expiration = responseMsg->expireson;
                        status = cloud_file_save(CLOUD_TOKEN_EXPIRATION_FILE, (u8*)&expiration, sizeof(expiration));
                    }
                    
                    authenticate_response__free_unpacked(responseMsg,NULL); // Free memory
                }
                else
                {
                    log_push_error_point(0x4E33);
                    status = S_MALLOC;
                }
            }
            
            __free(buf); // Free the allocated serialized buffer
        }
        else
        {
            status = S_MALLOC;
        }
    }
    
    return status;
}

u8 cloud_load_token(CloudToken *token)
{
    u8 status;
    u32 length;
    u64 expiration;
    
    // Check if token is expired    
    status = cloud_file_load(CLOUD_TOKEN_EXPIRATION_FILE, (u8*)&expiration, sizeof(expiration)); // Load Token data
    if(status == S_SUCCESS)
    {
        // compare time
        if(cloud_currenttime && (cloud_currenttime < (u32)expiration))
        {
            status = cloud_file_contentlength(CLOUD_TOKEN_DATA_FILE, &length);
            if(status == S_SUCCESS)
            {
                token->data  = __malloc(length);
                if(token->data)
                {
                    token->length = length;
                    
                    status = cloud_file_load(CLOUD_TOKEN_DATA_FILE, (u8*)token->data, length); // Load Token data
                }
            }
        }
        else
        {
            status = S_EXPIRED; // Expired
        }
    }
    
    return status;
}


u8 cloud_checklimit(u8 *serialnumber, CloudCheckLimit *limitdata)
{
    CloudHTTPPostData httppostdata = CLOUDHTTPPOSTDATA_INIT;
    CheckLimitRequest msg = CHECK_LIMIT_REQUEST__INIT;
    CheckLimitResponse* responseMsg;
    void *buf;                     // Buffer to store serialized data
    u32 len;                  // Length of serialized data
    u32 bodyLen;
    CloudToken token;
    u8 status = S_SUCCESS;
    
    // Pack Message
    msg.serialnumber = (char*)serialnumber;
    
    len = check_limit_request__get_packed_size(&msg);
    bodyLen = len + (8 -(len % 8)); // buffer padded to 8 bytes for crypto
    
    buf =(void*) __malloc(bodyLen);
    if (buf)
    {
        memset(buf, 0, bodyLen);
        
        check_limit_request__pack(&msg,buf);
        
        status = cloud_load_token(&token); // NOTE: This function malloc's token.data, remember to free
        if(status == S_SUCCESS)
        {
            httppostdata.resource = "/CheckLimit";
            httppostdata.content =(u8*) buf;
            httppostdata.contentlength = bodyLen;
            httppostdata.token = token.data;
            httppostdata.tokenlength = token.length;
            
            //Send Request
            status = cloud_http_post(&httppostdata, NULL);
            if (status == S_SUCCESS)
            {
                //Deserialize
                responseMsg = check_limit_response__unpack(NULL,
                                                           httppostdata.messagelength, 
                                                           httppostdata.messagebuffer);
                if (responseMsg)
                {
                    limitdata->accountrequired = (bool)responseMsg->accountrequired;
                    limitdata->limit = responseMsg->limit;
                    len = strlen(responseMsg->message);
                    if(len)
                    {
                        limitdata->message = __malloc(len);
                        if(limitdata->message)
                        {
                            strcpy((char*)limitdata->message, responseMsg->message);
                        }
                    }
                    
                    status = S_SUCCESS;
                    
                    // Free memory
                    check_limit_response__free_unpacked(responseMsg,NULL);
                }
                else
                {
                    status = S_MALLOC;
                }
            }
            if(token.data)
            {
                __free(token.data);
            }
        }
        __free(buf); // Free the allocated serialized buffer
    }
    else
    {
        status = S_MALLOC;
    }

    return status;
}

#define SEARCH_MAX_TAKE 5
u8 cloud_get_file_list(u8 *serialnumber, u8** extensions, u32 n_extensions)
{
    CloudSearch search;
    CloudSearch subsearch;
    CloudSearchResultsHeader *header;                        
    CloudSearchResultsItem *item;
    SearchResponse* responseMsg;
    u32 totalfiles;
    u32 itemindex;
    u32 supportindex;
    u8 status;
    u8 *filedata;
    int i, j, k;
    
    strcpy((char*)search.serialnumber, (const char*)serialnumber);
    memset(search.filename, 0, sizeof(search.filename)); 
    search.extentions = extensions;
    search.n_extentions = n_extensions;
    search.skip = 0;
    search.take = 0;
    
    // Search without taking to get total files
    status = cloud_search(search, &responseMsg);
    if(status == S_SUCCESS)
    {
        totalfiles = responseMsg->totalrecords; // Total File Count
                
        search_response__free_unpacked(responseMsg,NULL); // Free memory
        
        filedata = __malloc(sizeof(CloudSearchResultsHeader) + sizeof(CloudSearchResultsItem)*totalfiles);
        if(filedata)
        {
            header = (CloudSearchResultsHeader*)filedata;
            item = (CloudSearchResultsItem*)(filedata+sizeof(CloudSearchResultsHeader));
            
            memset(header, 0, sizeof(CloudSearchResultsHeader));
            
            // Search for remaining files
            itemindex = 0;
            supportindex = 0;
            while(totalfiles)
            {
                if(totalfiles > SEARCH_MAX_TAKE)
                {
                    search.take = SEARCH_MAX_TAKE;
                }
                else
                {
                    search.take = totalfiles;
                }
                
                status = cloud_search(search, &responseMsg);
                if(status == S_SUCCESS)
                {
                    // Loop through search results and data needed to download files
                  for(i=0; i < search.take; i++) 
                  {
                      memset(&item[itemindex], 0, sizeof(CloudSearchResultsItem));
                      item[itemindex].ctb.length = responseMsg->pageresults[i]->length;   // Length
                      
                      // DocumentId (GUID)
                      if(strlen(responseMsg->pageresults[i]->id) < sizeof(item[itemindex].ctb.id)) 
                      {
                          strcpy((char*)item[itemindex].ctb.id, (const char*)responseMsg->pageresults[i]->id);
                      }
                      else
                      {
                          log_push_error_point(0x4E34); // GUID too long
                          continue; // Skip file
                      }
                      
                      // Filename
                      if(strlen(responseMsg->pageresults[i]->name)+strlen(GENFS_USER_FOLDER_PATH) < sizeof(item->ctb.filename))
                      {
                          if(genfs_userpathcorrection((u8*)responseMsg->pageresults[i]->name, 
                                                      sizeof(item[itemindex].ctb.filename), item[itemindex].ctb.filename) != S_SUCCESS)
                          {
                              log_push_error_point(0x4E35); 
                          }
                      }
                      else
                      {
                          log_push_error_point(0x4E36); // Filename too long
                          continue; // Skip file
                      }
                      
                      // Title/Description
                      if(strlen(responseMsg->pageresults[i]->title) < sizeof(item[itemindex].ctb_description))
                      {
                          strcpy((char*)item[itemindex].ctb_description, (const char*)responseMsg->pageresults[i]->title);
                      }
                      else
                      {
                          log_push_error_point(0x4E37); // Title too long
                          continue; // Skip file
                      }
                      
                      if(responseMsg->pageresults[i]->n_properties)
                      {
                          for(j=0;j<responseMsg->pageresults[i]->n_properties;j++)
                          {
                              if(strstr(responseMsg->pageresults[i]->properties[j]->name, "CRC32"))  // File CRC32E - Required
                              {
                                  item[itemindex].ctb.crc32e = atoi(responseMsg->pageresults[i]->properties[j]->value);
                              }
                              else if(strstr(responseMsg->pageresults[i]->properties[j]->name, "DLXFileName")) // DLX File - Optional
                              {
                                  responseMsg->pageresults[i]->properties[j]->value[strlen(responseMsg->pageresults[i]->properties[j]->value)-1] = 0; // Remove ending quote
                                  if(strlen(responseMsg->pageresults[i]->properties[j]->value)+strlen(GENFS_USER_FOLDER_PATH) < sizeof(item[itemindex].supportfiles[item[itemindex].supportfile_count].filename))
                                  {
                                      if(genfs_userpathcorrection((u8*)responseMsg->pageresults[i]->properties[j]->value+1, 
                                                                  sizeof(item[itemindex].supportfiles[item[itemindex].supportfile_count].filename), item[itemindex].supportfiles[item[itemindex].supportfile_count].filename) == S_SUCCESS)
                                      {
                                          item[itemindex].supportfile_count++;
                                      }
                                      else
                                      {
                                          log_push_error_point(0x4E38); 
                                      }
                                  }
                                  else
                                  {
                                      log_push_error_point(0x4E39); // Filename too long
                                  }
                              }
                              else if(strstr(responseMsg->pageresults[i]->name, ".ctb"))  // If CTB file, check for SPF properties
                              {
                                  if(strstr(responseMsg->pageresults[i]->properties[j]->name, "ECM_SPF_Filename"))    // ECM SPF
                                  {
                                      responseMsg->pageresults[i]->properties[j]->value[strlen(responseMsg->pageresults[i]->properties[j]->value)-1] = 0; // Remove ending quote
                                      if(strlen(responseMsg->pageresults[i]->properties[j]->value)+strlen(GENFS_USER_FOLDER_PATH) < sizeof(item[itemindex].supportfiles[item[itemindex].supportfile_count].filename))
                                      {
                                          if(genfs_userpathcorrection((u8*)responseMsg->pageresults[i]->properties[j]->value+1, 
                                                                      sizeof(item[itemindex].supportfiles[item[itemindex].supportfile_count].filename), item[itemindex].supportfiles[item[itemindex].supportfile_count].filename) == S_SUCCESS)
                                          {
                                              item[itemindex].supportfile_count++;
                                          }
                                          else
                                          {
                                              log_push_error_point(0x4E3A); 
                                          }
                                      }
                                      else
                                      {
                                          log_push_error_point(0x4E3B); // Filename too long
                                      }
                                  }
                                  else if(strstr(responseMsg->pageresults[i]->properties[j]->name, "TCM_SPF_Filename"))   // TCM SPF
                                  {
                                      responseMsg->pageresults[i]->properties[j]->value[strlen(responseMsg->pageresults[i]->properties[j]->value)-1] = 0; // Remove ending quote
                                      if(strlen(responseMsg->pageresults[i]->properties[j]->value)+strlen(GENFS_USER_FOLDER_PATH) < sizeof(item[itemindex].supportfiles[item[itemindex].supportfile_count].filename))
                                      {
                                          if(genfs_userpathcorrection((u8*)responseMsg->pageresults[i]->properties[j]->value+1, 
                                                                      sizeof(item[itemindex].supportfiles[item[itemindex].supportfile_count].filename), item[itemindex].supportfiles[item[itemindex].supportfile_count].filename) == S_SUCCESS) // Skip the begining quote
                                          {
                                              item[itemindex].supportfile_count++;
                                          }
                                          else
                                          {
                                              log_push_error_point(0x4E3C);
                                          }
                                      }
                                      else
                                      {
                                          log_push_error_point(0x4E3D); // Filename too long
                                      }
                                  }
                              }
                          }
                      }
                      else
                      {
                          log_push_error_point(0x4E3E); // No properties found
                          continue; // Skip file
                      }
                      
                      itemindex++;
                  }
                  
                  // Free memory
                  search_response__free_unpacked(responseMsg,NULL);
                  
                  // Retrieve supporting file information if necessary
                  for(i=supportindex;i<itemindex;i++)
                  {
                      for(j = 0; j < item[i].supportfile_count; j++)
                      {
                          memset(&subsearch, 0, sizeof(subsearch));
                          subsearch.take = 1;
                          genfs_getfilenamefrompath(item[i].supportfiles[j].filename, subsearch.filename);
                          
                          status = cloud_search(subsearch, &responseMsg);
                          if(status == S_SUCCESS)
                          {
                              if(responseMsg->n_pageresults == 1)
                              {
                                  strcpy(item[i].supportfiles[j].id , responseMsg->pageresults[0]->id);
                                  item[i].supportfiles[j].length = responseMsg->pageresults[0]->length;
                                  
                                  for(k = 0; k < responseMsg->pageresults[0]->n_properties; k++)
                                  {
                                      if(strstr(responseMsg->pageresults[0]->properties[k]->name, "CRC32"))
                                      {
                                          item[i].supportfiles[j].crc32e = atoi(responseMsg->pageresults[0]->properties[k]->value);
                                          break;
                                      }
                                  }
                              }
                              
                              search_response__free_unpacked(responseMsg,NULL); // Free memory
                          }
                          else
                          {
                              log_push_error_point(0x4E3F); // Failed to get file search results
                              break;
                          }
                      }
                  }
                  supportindex = itemindex;
                }
                else
                {
                    break;
                }
                search.skip += search.take;
                totalfiles -= search.take;
            }
            
            if(status == S_SUCCESS)
            {
                // Save file list to filesystem
                memset(header, 0, sizeof(CloudSearchResultsHeader));
                header->count = itemindex;
                
                status = cloud_file_save(CLOUD_SEARCH_FILE_LIST, filedata, sizeof(CloudSearchResultsHeader)+(sizeof(CloudSearchResultsItem)*header->count));                
            }
            __free(filedata);
        }
        else
        {
            log_push_error_point(0x4E40);
            status = S_MALLOC;
        }
    }
    
    return status;
}

u8 cloud_search(CloudSearch search, SearchResponse** responseMsg)
{
    CloudHTTPPostData httppostdata = CLOUDHTTPPOSTDATA_INIT;
    SearchRequest msg = SEARCH_REQUEST__INIT;
    ApiDocumentProperty subMsg = API_DOCUMENT_PROPERTY__INIT;
    ApiDocumentProperty* subMsgArray[1];
    ApiDocumentProperty** subMsgs = subMsgArray;
    CloudToken token;
    void *buf;                // Buffer to store serialized data
    u8 properties_serial[16];
    
    u32 len;                  // Length of serialized data  
    u32 bodyLen;
    u8 status = S_SUCCESS;
    
    if(strlen((char*)search.serialnumber) > (sizeof(properties_serial)-3))
    {
        status = S_INPUT;
    }
    else
    {        
        status = cloud_load_token(&token); // NOTE: This function malloc's token.data, remember to free
        if(status == S_SUCCESS)
        {
            httppostdata.resource = "/Search";
            httppostdata.token = token.data;
            httppostdata.tokenlength = token.length;
            
            subMsgs[0] = &subMsg;
            
            // Filename
            if(strlen(search.filename))
            {
              msg.name = (char*)search.filename;
            }
            
            // File extentions
            if(search.n_extentions)
            {
              msg.extensions =(char **)search.extentions;
              msg.n_extensions = search.n_extentions;
            }
            
            // SubProperties
            if(strlen(search.serialnumber))
            {
              sprintf((char*)properties_serial, "\"%s\"", search.serialnumber);
              subMsgs[0]->name = "SerialNumber";
              subMsgs[0]->value = (char*)properties_serial;            
              
              msg.properties = subMsgs;
              msg.n_properties = 1;
            }
            
            // Handle list navigation
            msg.skip = search.skip;   // Number of pages to skip
            msg.take = search.take;   // Number of results to return
            
            u8* ReturnedProps[] = {"CRC32", "ECM_SPF_Filename", "TCM_SPF_Filename", "DLXFileName"}; // Only care about these properties for now
            msg.returnedproperties = (char**)ReturnedProps;
            msg.n_returnedproperties = 4;
            
            len = search_request__get_packed_size(&msg);
            bodyLen = len + (8 -(len % 8)); // buffer padded to 8 bytes for crypto
            
            buf =(void*) __malloc(bodyLen);
            if (buf)
            {
                memset(buf, 0, bodyLen);
                search_request__pack(&msg,buf);
                
                httppostdata.content =(u8*) buf;
                httppostdata.contentlength = bodyLen;
                
                //Send Request
                status = cloud_http_post(&httppostdata, NULL);
                if(status == S_SUCCESS)
                {
                    //Deserialize
                    *responseMsg = search_response__unpack(NULL,
                                                          httppostdata.messagelength, 
                                                          httppostdata.messagebuffer);
                    if (responseMsg)
                    {
                        status = S_SUCCESS;
                    }
                    else
                    {
                        log_push_error_point(0x4E41);
                        status = S_MALLOC;
                    }
                }
                
                __free(buf); // Free the allocated serialized buffer
            }
            if(token.data)
            {
                __free(token.data);
            }
        }
    }
    return status;
}

u8 cloud_download(CloudDownloadItem file, PROGRESS_REPORT_FUNC_SIMPLE progress_report_func)
{
    CloudHTTPPostData httppostdata = CLOUDHTTPPOSTDATA_INIT;
    CloudToken token;
    DownloadRequest msg = DOWNLOAD_REQUEST__INIT;
    void *buf;                // Buffer to store serialized data
    u32 len;                  // Length of serialized data
    u32 bodyLen;
    u8 status = S_FAIL;
    
    httppostdata.resource = "/Download";
    
    status = cloud_load_token(&token); // NOTE: This function malloc's token.data, remember to free
    if(status == S_SUCCESS)
    {
        httppostdata.token = token.data;
        httppostdata.tokenlength = token.length;
        
        // Pack Message
        msg.documentid = (char*)file.id;
        
        len = download_request__get_packed_size(&msg);
        bodyLen = len + (8 -(len % 8)); // buffer padded to 8 bytes for crypto
        
        buf =(void*) __malloc(bodyLen);
        if (buf)
        {
            memset(buf, 0, bodyLen);
            
            download_request__pack(&msg,buf);
            
            httppostdata.content =(u8*) buf;
            httppostdata.contentlength = bodyLen;
            
            //Send Request
            status = cloud_http_get(&httppostdata, file.filename, progress_report_func);
            if (status == S_SUCCESS)
            {
                // Validate file               
                status = genfs_verifyfile(file.filename, file.length, file.crc32e);
                if(status != S_SUCCESS)
                {
                    log_push_error_point(0x4E42); // downloaded file fails validation
                }
            }
            
            __free(buf); // Free the allocated serialized buffer
        }
        else
        {
            log_push_error_point(0x4E43);
            status = S_MALLOC;
        }
        if(token.data)
        {
            __free(token.data);
        }
    }
    
    return status;
}






