/**
  **************** (C) COPYRIGHT 2011 SCT Performance, LLC *********************
  * File Name          : memcheck.c
  * Device / System    : SCT products using the "CommonCode" platform
  * Author             : Quyen Leba
  *
  * Version            : 1 
  * Date               : 05/02/2011
  * Description        : 
  *                    : 
  *
  *
  * History            : 05/02/2011 M. Davis
  *                    :   Add SCT Copyright header,
  *
  ******************************************************************************
  */

#include <board/genplatform.h>
#include <common/crc32.h>
#include "memcheck.h"

void memcheck_initstackprotection();
void memcheck_initheapprotection();

//------------------------------------------------------------------------------
// Init for mem check (stack & heap)
// Engineer: Quyen Leba
//------------------------------------------------------------------------------
void memcheck_init()
{
    memcheck_initstackprotection();
    memcheck_initheapprotection();
}

//------------------------------------------------------------------------------
// Test if stack and heap are healthy
// Engineer: Quyen Leba
//------------------------------------------------------------------------------
void memcheck_test()
{
    if (memcheck_isstackoverflow())
    {
        led_emergency_indicator_stack_error();
    }
    if (memcheck_isheapoverflow())
    {
        led_emergency_indicator_heap_error();
    }
}

//------------------------------------------------------------------------------
// Assign a 16byte signature to CSTACK_E segment to detect stack overflow
// Engineer: Quyen Leba
//------------------------------------------------------------------------------
#pragma segment="CSTACK_E"
void memcheck_initstackprotection()
{
    u32 *ptr;

    ptr = __segment_begin("CSTACK_E");
    if (__segment_size("CSTACK_E") != 16)
    {
        while(1);   //strap for debug
    }
    ptr[0] = 0x77ACAC8E;
    ptr[1] = 0xAB08289D;
    ptr[2] = 0x9D0EF75A;
    crc32e_sw_reset();
    ptr[3] = crc32_sw_calculateblock(0xFFFFFFFF,ptr,3);
}

//------------------------------------------------------------------------------
// Check if stack overflow
// Return:  bool (TRUE: overflow)
// Engineer: Quyen Leba
//------------------------------------------------------------------------------
#pragma segment="CSTACK_E"
bool memcheck_isstackoverflow()
{
    u32 *ptr;

    ptr = __segment_begin("CSTACK_E");
    if (crc32_sw_calculateblock(0xFFFFFFFF,ptr,3) != ptr[3])
    {
        return TRUE;
    }
    return FALSE;
}

//------------------------------------------------------------------------------
// Assign a 16byte signature to HEAP_E segment to detect heap overflow
// Engineer: Quyen Leba
//------------------------------------------------------------------------------
#pragma segment="HEAP_E"
void memcheck_initheapprotection()
{
    u32 *ptr;

    ptr = __segment_begin("HEAP_E");
    if (__segment_size("HEAP_E") != 16)
    {
        while(1);   //strap for debug
    }
    ptr[0] = 0x730D9694;
    ptr[1] = 0x950A8959;
    ptr[2] = 0xD509070C;
    crc32e_sw_reset();
    ptr[3] = crc32_sw_calculateblock(0xFFFFFFFF,ptr,3);
}

//------------------------------------------------------------------------------
// Check if heap overflow
// Return:  bool (TRUE: overflow)
// Engineer: Quyen Leba
// Note: this does not detect heap corruption
//------------------------------------------------------------------------------
#pragma segment="HEAP_E"
bool memcheck_isheapoverflow()
{
    u32 *ptr;

    ptr = __segment_begin("HEAP_E");
    if (crc32_sw_calculateblock(0xFFFFFFFF,ptr,3) != ptr[3])
    {
        return TRUE;
    }
    return FALSE;
}
