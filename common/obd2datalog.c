/**
  **************** (C) COPYRIGHT 2011 SCT Performance, LLC *********************
  * File Name          : obd2datalog.c
  * Device / System    : SCT products using the "CommonCode" platform
  * Author             : Quyen Leba
  *
  * Version            : 1 
  * Date               : 05/02/2011
  * Description        : 
  *                    : 
  *
  *
  * History            : 05/02/2011 M. Davis
  *                    :   Add SCT Copyright header,
  *
  ******************************************************************************
  */

//------------------------------------------------------------------------------
// Log error point: 0x4900 -> 0x4AFF
//------------------------------------------------------------------------------

#include <fs/genfs.h>
#include <board/genplatform.h>
#include <board/properties_vb.h>
#include <board/datalogfeatures.h>
#include <board/delays.h>
#include <board/power.h>
#include <board/rtc.h>
#include <common/genmanuf_overload.h>
#include <common/obd2iso.h>
#include <common/obd2vpw.h>
#include <common/obd2scp.h>
#include <common/crypto_blowfish.h>
#include <common/crc32.h>
#include <common/statuscode.h>
#include <common/file.h>
#include "obd2datalog.h"

//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
Datalog_Info *dataloginfo = NULL;
Datalog_Mode gDatalogmode;

extern obd2_info gObd2info;                             /* from obd2.c */

//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
u8 obd2datalog_resetdatalogsignals();

//------------------------------------------------------------------------------
// Input:   u32 ecm_id_masked (0: 1st ecm, any: 2nd ecm)
//          u8  packetid (if applicable, otherwise make 0)
//          u8  *data
//          VehicleCommLevel commlevel
// Return:  u8  status
// Engineer: Quyen Leba/Patrick Downs/Tristen Pierson
//------------------------------------------------------------------------------
u8 obd2datalog_parserapidpacketdata(u8 ecm_index, u16 packetid, u8 *data)
{
    int i;
    
    for(i=0;i<dataloginfo->datalogsignalcount;i++)
    {
        obd2datalog_parserdata(ecm_index, i, packetid, data);
    }
    
    return S_SUCCESS;
}

//------------------------------------------------------------------------------
// Input:   u32 ecm_id_masked (0: 1st ecm, any: 2nd ecm)
//          u8  signal_index
//          u16 packetid (if applicable, otherwise make 0)
//          u8  *data
//          VehicleCommLevel commlevel
// Return:  void
// Engineer: Quyen Leba/Patrick Downs/Tristen Pierson
//------------------------------------------------------------------------------
void obd2datalog_parserdata(u8 ecm_index, u16 signal_index, u16 packetid, u8 *data)
{
    float old_value;
    u8  value8;
    u16 value16;
    u8  size;
    int i = signal_index;
    union
    {
        float fvalue;
        u32 uivalue;
        s32 sivalue;
        u8 hex[4];
    }fvalue;    

    // Determine data byte size if size is in bits
    if (dataloginfo->datalogsignals[i].Flags & DATALOG_PIDFLAGS_IS_BITS)
    {
        size = (dataloginfo->datalogsignals[i].Size >> 3) + 1;
        // 3-byte concatenated to 4-byte
        if (size == 3)
        {
            size = 4;
        }
    }
    else
    {
        size = dataloginfo->datalogsignals[i].Size;
    }
    
    if (dataloginfo->datalogsignals[i].EcmIndex == ecm_index &&
        dataloginfo->datalogsignals[i].SignalType.Generic.PacketNumber == packetid)
    {
        old_value = dataloginfo->datalogsignals[i].Value;
        if (dataloginfo->datalogsignals[i].Flags & DATALOG_PIDFLAGS_DATATYPE_BIT)
        {
            // Handle Bitmapped Data Type
            fvalue.uivalue = 0;
            if (size == 1)
            {
                fvalue.hex[0] = (data[dataloginfo->datalogsignals[i].SignalType.Generic.Position]);
            }
            else if (size == 2)
            {
                if (dataloginfo->datalogsignals[i].Flags & DATALOG_PIDFLAGS_BIGENDIAN)
                {
                    fvalue.hex[0] = data[dataloginfo->datalogsignals[i].SignalType.Generic.Position+1];
                    fvalue.hex[1] = data[dataloginfo->datalogsignals[i].SignalType.Generic.Position];
                }
                else // little endian
                {
                    fvalue.hex[0] = data[dataloginfo->datalogsignals[i].SignalType.Generic.Position];
                    fvalue.hex[1] = data[dataloginfo->datalogsignals[i].SignalType.Generic.Position+1];
                }
            }
            else if (size == 4)
            {
                if (dataloginfo->datalogsignals[i].Flags & DATALOG_PIDFLAGS_BIGENDIAN)
                {
                    fvalue.hex[0] = data[dataloginfo->datalogsignals[i].SignalType.Generic.Position+3];
                    fvalue.hex[1] = data[dataloginfo->datalogsignals[i].SignalType.Generic.Position+2];
                    fvalue.hex[2] = data[dataloginfo->datalogsignals[i].SignalType.Generic.Position+1];
                    fvalue.hex[3] = data[dataloginfo->datalogsignals[i].SignalType.Generic.Position];
                }
                else // little endian
                {
                    fvalue.hex[0] = data[dataloginfo->datalogsignals[i].SignalType.Generic.Position];
                    fvalue.hex[1] = data[dataloginfo->datalogsignals[i].SignalType.Generic.Position+1];
                    fvalue.hex[2] = data[dataloginfo->datalogsignals[i].SignalType.Generic.Position+2];
                    fvalue.hex[3] = data[dataloginfo->datalogsignals[i].SignalType.Generic.Position+3];
                }
            }
            if(fvalue.uivalue & (u32)(1 << dataloginfo->datalogsignals[i].SignalType.Bitmapped.Bit))
                dataloginfo->datalogsignals[i].Data = 1;
            else
                dataloginfo->datalogsignals[i].Data = 0;
            
            dataloginfo->datalogsignals[i].Value = dataloginfo->datalogsignals[i].Data;
        }
        else
        {
            // Handle all other data types
            if (size == 1)
            {
                value8 = data[dataloginfo->datalogsignals[i].SignalType.Generic.Position];
                if (dataloginfo->datalogsignals[i].Flags & DATALOG_PIDFLAGS_SIGNED)
                {
                    dataloginfo->datalogsignals[i].Data = (float)((s8)value8);
                }
                else
                {
                    dataloginfo->datalogsignals[i].Data = (float)((u8)value8);
                }
            }
            else if (size == 2)
            {
                if (dataloginfo->datalogsignals[i].Flags & DATALOG_PIDFLAGS_BIGENDIAN)
                {
                    value16 = 
                        (data[dataloginfo->datalogsignals[i].SignalType.Generic.Position] << 8) |
                            (data[dataloginfo->datalogsignals[i].SignalType.Generic.Position+1]);
                }
                else
                {
                    value16 = 
                        (data[dataloginfo->datalogsignals[i].SignalType.Generic.Position+1] << 8) |
                            (data[dataloginfo->datalogsignals[i].SignalType.Generic.Position]);
                }
                if (dataloginfo->datalogsignals[i].Flags & DATALOG_PIDFLAGS_SIGNED)
                {
                    dataloginfo->datalogsignals[i].Data = (float)((s16)value16);
                }
                else
                {
                    dataloginfo->datalogsignals[i].Data = (float)((u16)value16);
                }
            }
            else if (size == 4)
            {
                if (dataloginfo->datalogsignals[i].Flags & DATALOG_PIDFLAGS_BIGENDIAN)
                {
                    if (dataloginfo->datalogsignals[i].Flags & DATALOG_PIDFLAGS_DATATYPE_INT)
                    {
                        fvalue.hex[0] = data[dataloginfo->datalogsignals[i].SignalType.Generic.Position+3];
                        fvalue.hex[1] = data[dataloginfo->datalogsignals[i].SignalType.Generic.Position+2];
                        fvalue.hex[2] = data[dataloginfo->datalogsignals[i].SignalType.Generic.Position+1];
                        fvalue.hex[3] = data[dataloginfo->datalogsignals[i].SignalType.Generic.Position];
                        if (dataloginfo->datalogsignals[i].Flags & DATALOG_PIDFLAGS_SIGNED)
                        {
                            dataloginfo->datalogsignals[i].Data = (float)fvalue.sivalue;
                        }
                        else
                        {
                            dataloginfo->datalogsignals[i].Data = (float)fvalue.uivalue;
                        }
                    }
                    else // float
                    {
                        fvalue.hex[0] = data[dataloginfo->datalogsignals[i].SignalType.Generic.Position+3];
                        fvalue.hex[1] = data[dataloginfo->datalogsignals[i].SignalType.Generic.Position+2];
                        fvalue.hex[2] = data[dataloginfo->datalogsignals[i].SignalType.Generic.Position+1];
                        fvalue.hex[3] = data[dataloginfo->datalogsignals[i].SignalType.Generic.Position];
                        dataloginfo->datalogsignals[i].Data = fvalue.fvalue;
                    }
                }
                else // little endian
                {
                    if (dataloginfo->datalogsignals[i].Flags & DATALOG_PIDFLAGS_DATATYPE_INT)
                    {
                        fvalue.hex[0] = data[dataloginfo->datalogsignals[i].SignalType.Generic.Position];
                        fvalue.hex[1] = data[dataloginfo->datalogsignals[i].SignalType.Generic.Position+1];
                        fvalue.hex[2] = data[dataloginfo->datalogsignals[i].SignalType.Generic.Position+2];
                        fvalue.hex[3] = data[dataloginfo->datalogsignals[i].SignalType.Generic.Position+3];
                        if (dataloginfo->datalogsignals[i].Flags & DATALOG_PIDFLAGS_SIGNED)
                        {
                            dataloginfo->datalogsignals[i].Data = (float)fvalue.sivalue;
                        }
                        else
                        {
                            dataloginfo->datalogsignals[i].Data = (float)fvalue.uivalue;
                        }
                    }
                    else // float
                    {
                        fvalue.hex[0] = data[dataloginfo->datalogsignals[i].SignalType.Generic.Position];
                        fvalue.hex[1] = data[dataloginfo->datalogsignals[i].SignalType.Generic.Position+1];
                        fvalue.hex[2] = data[dataloginfo->datalogsignals[i].SignalType.Generic.Position+2];
                        fvalue.hex[3] = data[dataloginfo->datalogsignals[i].SignalType.Generic.Position+3];
                        dataloginfo->datalogsignals[i].Data = fvalue.fvalue;
                    }
                }
            }
            // "BP"(Ford Only), "Multiplier", and "Offset"
            switch (dataloginfo->datalogsignals[i].StructType)
            {
                case StructTypeBitPtr:
                    dataloginfo->datalogsignals[i].Value = 
                        dataloginfo->datalogsignals[i].Data / 
                        obd2datalog_calculate_bp(&dataloginfo->datalogsignals[i]);
                    dataloginfo->datalogsignals[i].Value = 
                        (dataloginfo->datalogsignals[i].Value * 
                         dataloginfo->datalogsignals[i].SignalType.MultiplierOffset.Multiplier) + 
                         dataloginfo->datalogsignals[i].SignalType.MultiplierOffset.Offset;
                    break;
                case StructTypeMultiplierOffset:                    
                    dataloginfo->datalogsignals[i].Value = 
                        (dataloginfo->datalogsignals[i].Data * 
                         dataloginfo->datalogsignals[i].SignalType.MultiplierOffset.Multiplier) + 
                         dataloginfo->datalogsignals[i].SignalType.MultiplierOffset.Offset;
                    break;
            }
        }
        
        if (dataloginfo->datalogsignals[i].Value != old_value ||
            dataloginfo->control.newdata_filtered == 0)
        {
            dataloginfo->datalogsignals[i].Control |= DATALOG_CONTROL_NEWDATA_SET;
        }
    }//if (dataloginfo->...
    
    return;
}

//------------------------------------------------------------------------------
// Input:   u8  signal_index
//          float data
// Description: This function composes data into a machine readable format. 
//              The float data input is composed per the dlx record scaling
//              parameters
// Return:  float fdata (composed data)
// Engineer: Tristen Pierson
// Note: This function does not handle bitmapped data
//------------------------------------------------------------------------------
float obd2datalog_composedata(u16 signal_index, float data)
{
    float fvalue;
    int i = signal_index;
    u8  hex[4];

    // Remove multipliers/offsets
    if (dataloginfo->datalogsignals[i].StructType != StructTypeOSC)
    {
        // Do nothing
        return 0;
    }
    
    // multiplier/offset
    fvalue = (data * dataloginfo->datalogsignals[i].SignalType.MultiplierOffset.Multiplier) +
            dataloginfo->datalogsignals[i].SignalType.MultiplierOffset.Offset; 
    
    // Handle data types
    if (dataloginfo->datalogsignals[i].Size == 2)
    {
        if (dataloginfo->datalogsignals[i].Flags & DATALOG_PIDFLAGS_BIGENDIAN)
        {
            hex[0] = (u8)(((u32)fvalue) >> 8);
            hex[1] = (u8)fvalue;
            fvalue = (float)(hex[0] + (u16)(hex[1] << 8));
        }
    }
    else if (dataloginfo->datalogsignals[i].Size == 4)
    {
        if (dataloginfo->datalogsignals[i].Flags & DATALOG_PIDFLAGS_BIGENDIAN)
        {
            hex[0] = (u8)(((u32)fvalue) >> 24);
            hex[1] = (u8)(((u32)fvalue) >> 16);
            hex[2] = (u8)(((u32)fvalue) >> 8);
            hex[3] = (u8)fvalue;
            fvalue = (float)(hex[0] + (u32)(hex[1] << 8) + (u32)(hex[2] << 16) + (u32)(hex[3] << 24));
        }
    }
    
    return fvalue;
}

//------------------------------------------------------------------------------
// Input:   DatalogSignal *datalogsignal
// Return:  float value
// Engineer: Quyen Leba
//------------------------------------------------------------------------------
float obd2datalog_calculate_bp(DatalogSignal *datalogsignal)
{
    float value, exp;
    u32 i;
    
    exp = 1;
    
    for(i=1;i<=abs(datalogsignal->SignalType.BitPtr.Bp);i++)
    {
        exp *= 2;
    }
    
    if(datalogsignal->SignalType.BitPtr.Bp < 0)
    {
        value = 1.0f / exp;
    }
    else
    {
        value = exp;
    }
    
    return value;
}

//------------------------------------------------------------------------------
// Handle Analog Port Configureation
// Return:  u8  status
// Engineer: Patrick Downs
//------------------------------------------------------------------------------
void obd2datalog_analogport_init()
{
    u8 status;
    u8 returndata;
    u32 returndatalength;
    
    status = settings_getsettings_byopcode(SETTINGS_OPCODE_ANALOGPORT_EXT5V, 0,
                                           &returndata, &returndatalength);    
    if(status == S_SUCCESS && returndata == EXT5VCONFIG_AUTO)
    {
        V5VExt_Ctrl(ENABLE);
    }

    // TODO: THIS    
//    status = settings_getsettings_byopcode(SETTINGS_OPCODE_ANALOGPORT_GPIO1, 0,
//                                           &returndata, &returndatalength);    
//    if(status == S_SUCCESS && returndata == GPIO1CONFIG_AUTO)
//    {
//        adc_config_gpio1_out(TRUE);
//    }
}

//------------------------------------------------------------------------------
// Handle Analog Port Configuration
// Return:  u8  status
// Engineer: Patrick Downs
//------------------------------------------------------------------------------
void obd2datalog_analogport_deinit()
{
    u8 status;
    u8 returndata;
    u32 returndatalength;
    
    status = settings_getsettings_byopcode(SETTINGS_OPCODE_ANALOGPORT_EXT5V, 0,
                                           &returndata, &returndatalength);    
    if(status == S_SUCCESS && returndata == EXT5VCONFIG_AUTO)
    {
        V5VExt_Ctrl(DISABLE);
    }
    
    // TODO: THIS
//    status = settings_getsettings_byopcode(SETTINGS_OPCODE_ANALOGPORT_GPIO1, 0,
//                                           &returndata, &returndatalength);    
//    if(status == S_SUCCESS && returndata == GPIO1CONFIG_AUTO)
//    {
//        adc_config_gpio1_out(FALSE);
//    }
}

//------------------------------------------------------------------------------
// Initialize structure of dataloginfo
// Return:  u8  status
// Engineer: Quyen Leba
//------------------------------------------------------------------------------
u8 obd2datalog_dataloginfo_init()
{
    PROPERTIES_VB_INFO properties_vb_info;
    u32 properties_vb_info_length;
    u8  status;

    obd2_garbagecollector();
    
    if (dataloginfo == NULL)
    {
        //TODOQ: more on this, dataloginfo is tricky
        //TODOQ: use an init func for this
        dataloginfo = __malloc(sizeof(Datalog_Info));//////////////////////////////
        if (dataloginfo == NULL)
        {
            log_push_error_point(0x4900);
            return S_MALLOC;
        }
        memset((char*)dataloginfo,0,sizeof(Datalog_Info));
        dataloginfo->dlxfilename[0] = NULL;
        dataloginfo->dlxheader = NULL;
        dataloginfo->dlxblocks = NULL;
        dataloginfo->getdatafunc = NULL;
        dataloginfo->datareport_interval = DATALOG_DEFAULT_DATAREPORT_INTERVAL;
        dataloginfo->getanalogdatafunc = NULL;
        dataloginfo->sendtesterpresent = NULL;
        dataloginfo->scheduled_tasks = 0;
        dataloginfo->record.fptr = NULL;
        dataloginfo->record.totallength = 0;
        dataloginfo->record.control.start_pending = 0;
        dataloginfo->record.control.stop_pending = 0;
        dataloginfo->record.control.state = DATALOG_RECORD_STATE_IDLE;
        dataloginfo->control.newdata_filtered = 1;
        dataloginfo->control.ecm_stream = 0;
        dataloginfo->control.tcm_stream = 0;
        dataloginfo->control.hasotf = 0;
        dataloginfo->control.ecm_laststreamdata = 0;
        dataloginfo->control.tcm_laststreamdata = 0;
        dataloginfo->pending = FALSE;
        dataloginfo->osc_pending = FALSE;
        dataloginfo->record.bufferlength = 0;

        // Get the vehicle board properties and check the board revision
        status = properties_vb_getinfo((u8*)&properties_vb_info, &properties_vb_info_length);
        if (status != S_SUCCESS)
        {
            log_push_error_point(0x495C);
            return status;
        }
        dataloginfo->analogsignalmaxavailable = datalogfeatures_getInputCount(properties_vb_info.board_rev);
        if (dataloginfo->analogsignalmaxavailable > DATALOG_ANALOG_INPUT_COUNT_LIMIT)
        {
            log_push_error_point(0x495D);
            return S_ERROR;
        }
    }
    return S_SUCCESS;
}

//------------------------------------------------------------------------------
// Initialize datalogmode structure
// Return:  u8  status
// Engineer: Tristen Pierson
//------------------------------------------------------------------------------
u8 obd2datalog_datalogmode_init()
{
    gDatalogmode.packetmode = RapidRate;        
    gDatalogmode.packetCount = 0;
    gDatalogmode.packetSize = 0;
    gDatalogmode.addressType = Address_32_Bits;
    gDatalogmode.ecmBroadcast = FALSE;
    gDatalogmode.obdPids = FALSE;
    gDatalogmode.unlockRequired = FALSE;
    return S_SUCCESS;
}

/**
 *  @brief Get datalogmode defaults
 *
 *  @param [in]    commtype     Vehicle comm type
 *  @param [in]    commlevel    Vehicle comm level
 *
 *  @return Status
 */
void obd2datalog_datalogmode_get(VehicleCommType commtype, VehicleCommLevel commlevel)
{
    /* Set defaults */
    gDatalogmode.packetCount = 0;
    gDatalogmode.packetSize = 0;

    /* Check commtype */
    switch (commtype)
    {
    case CommType_CAN:
        /* Check commlevel */
        switch (commlevel)
        {
        case CommLevel_KWP2000:
            /* Check OEM type (Ford and DCX share KWP2000) */
            switch (gObd2info.oemtype)
            {
            case OemType_FORD:
                gDatalogmode.packetCount = FORD_KWP_CAN_PACKETID_END - FORD_KWP_CAN_PACKETID_START;
                gDatalogmode.packetSize = FORD_KWP_CAN_MAX_DATABYTE_PER_PACKET;
                break;
            case OemType_DCX:
                gDatalogmode.packetCount = DCX_KWP_CAN_PACKETID_END - DCX_KWP_CAN_PACKETID_START;
                gDatalogmode.packetSize = DCX_KWP_CAN_MAX_DATABYTE_PER_PACKET;
                break;
            }
            break;
        case CommLevel_ISO14229:
            /* Check OEM type (in the future, more ISO14229 OEMs are expected) */
            switch (gObd2info.oemtype)
            {
            case OemType_FORD:
                gDatalogmode.packetCount = FORD_UDS_CAN_PACKETID_END - FORD_UDS_CAN_PACKETID_START;
                gDatalogmode.packetSize = FORD_UDS_CAN_MAX_DATABYTE_PER_PACKET;
                break;
            }
            break;
        case CommLevel_GMLAN:
            if (gObd2info.oemtype == OemType_GM)
            {
                gDatalogmode.packetCount = GM_GMLAN_CAN_PACKETID_START - GM_GMLAN_CAN_PACKETID_END;
                gDatalogmode.packetSize = GM_GMLAN_CAN_MAX_DATABYTE_PER_PACKET;
            }
            break;
        }
        break;
    case CommType_SCP:
    case CommType_SCP32:
        /* Check OEM type; should always be Ford but do this just in case */
        if (gObd2info.oemtype == OemType_FORD)
        {
            gDatalogmode.packetCount = FORD_KWP_SCP_PACKETID_END - FORD_KWP_SCP_PACKETID_START;
            gDatalogmode.packetSize = FORD_KWP_SCP_MAX_DATABYTE_PER_PACKET;
        }
        break;
    case CommType_VPW:
        /* Check OEM type (VPW enhanced diagnostics not supported for non GM OEMs) */
        if (gObd2info.oemtype == OemType_GM)
        {
            gDatalogmode.packetCount = GM_GMLAN_VPW_PACKETID_START - GM_GMLAN_VPW_PACKETID_END;
            gDatalogmode.packetSize = GM_GMLAN_VPW_MAX_DATABYTE_PER_PACKET;
        }
        break;
    } /* switch (commtype) */
    return;
}


//------------------------------------------------------------------------------
// Clean up when done datalogging
// Return:  u8  status
// Engineer: Quyen Leba
//------------------------------------------------------------------------------
u8 obd2datalog_garbagecollector()
{
    if (dataloginfo != NULL)
    {
        if (dataloginfo->dlxheader)
        {
            __free(dataloginfo->dlxheader);
            dataloginfo->dlxheader = NULL;
        }
        if (dataloginfo->dlxblocks)
        {
            __free(dataloginfo->dlxblocks);
            dataloginfo->dlxblocks = NULL;
        }
        dataloginfo->getdatafunc = NULL;
        dataloginfo->getanalogdatafunc = NULL;
        dataloginfo->sendtesterpresent = NULL;

        if (dataloginfo->record.fptr)
        {
            genfs_closefile(dataloginfo->record.fptr);
            dataloginfo->record.fptr = NULL;
        }

        __free(dataloginfo);
        dataloginfo = NULL;        
    }
    return S_SUCCESS;
}

//------------------------------------------------------------------------------
// Set data report interval
// Input:   u8  interval (in ms)
//------------------------------------------------------------------------------
u8 obd2datalog_setdatareportinterval(u8 interval)
{
    dataloginfo->datareport_interval = interval;
    return S_SUCCESS;
}

/**
 *  @brief Initialize datalog session
 *
 *  @param [out]    *commtype   Vehicle comm types
 *  @param [out]    *commlevel  Vehicle comm levels
 *
 *  @return Status
 */
u8 obd2datalog_init(VehicleCommType *commtype, VehicleCommLevel *commlevel)
{
    VehicleCommType temp;
    u8 i;
    u8 status;
    
    if (SETTINGS_IsDemoMode())
    {
        for(i = 0; i < ECM_MAX_COUNT; i++)
        {
            commtype[i] = demo_vehicle.ecm.commtype[i];
            commlevel[i] = demo_vehicle.ecm.commlevel[i];
        }
        return S_SUCCESS;
    }
    
    obd2_open();

    obd2_findvehiclecommtypecommlevel(commtype, commlevel);
    
    obd2datalog_setcomm(commtype, commlevel);
    
    switch (gObd2info.oemtype)
    {
    case OemType_DCX:
        if (commtype[0] == CommType_CAN)
        {
            /* Enter extended diagnostics mode (might be conditional). */
            for (i = 0; i < ECM_MAX_COUNT; i++)
            {
                if (gObd2info.ecu_block[i].commtype == CommType_CAN)
                {
#ifdef __DCX_MANUF__                    
                    status = obd2can_dcx_initiate_diagnostic_operation(FALSE, 
                                          gObd2info.ecu_block[i].ecu_id,
                                          gObd2info.ecu_block[i].ecu_response_id,
                                          diagnosticRequestChrysler_92);
#else
                    status = S_SUCCESS;
#endif
                }
            }
        }
        if (commtype[1] != CommType_Unknown)
        {
            /* If second commtype exists, initialize it here */
            switch (commtype[1])
            {
            case CommType_CAN:
                obd2can_init(Obd2canInitProgram,0);
                status = S_SUCCESS; 
                break; 
            case CommType_KLINE:
            case CommType_KWP2000:
                status = iso_init(&temp);
                break;
            case CommType_VPW:
                status = obd2vpw_init(TRUE, FALSE);
                break;
            default:
                status = S_SUCCESS;
                break;
            }
        }
        break;
#ifdef __FORD_MANUF__
    case OemType_FORD:
        if (commtype[0] == CommType_CAN)
        {
            status = obd2can_ford_datalog_change_diagnostic_mode(commlevel);
        }
        else
        {
            status = S_SUCCESS;
        }
        break;
#endif
#ifdef __GM_MANUF__
    case OemType_GM:
        status = S_SUCCESS;
        break;
#endif
    default:
        //log_push_error_point(0x4987); Abandoned
        //log_push_error_point(0x49A6); Abandoned
        /* Unsupported OEMs are okay */
        switch (commtype[0])
        {
        case CommType_KLINE:
        case CommType_KWP2000:
            status = iso_init(&temp);
            break;
        default:
            status = S_SUCCESS;
            break;
        }
        break;
    }
    return status;    
}

/**
 *  @brief Pre-PID validation task
 *
 *  @param [in]     ecm_index  ECM Index
 *  @param [in]     commtype   Vehicle comm type
 *  @param [in]     commlevel  Vehicle comm level
 *
 *  @return Status
 */
u8 obd2datalog_prevalidate_task(u16 ecm_index, VehicleCommType commtype, 
                                VehicleCommLevel commlevel)
{
    u8 status;
    
    if (SETTINGS_IsDemoMode())
    {
        return S_SUCCESS;
    }

    obd2datalog_datalogmode_get(commtype, commlevel);
    
    obd2_open();

    switch (commtype)
    {
    case CommType_CAN:
        switch (gObd2info.oemtype)
        {
#ifdef __DCX_MANUF__        
        case OemType_DCX:
            status = obd2datalog_dcx_prevalidate_task(ecm_index, 
                                                      commtype, commlevel);
            break;
#endif
#ifdef __FORD_MANUF__
        case OemType_FORD:
            status = obd2datalog_ford_prevalidate_task(ecm_index, 
                                                       commtype, commlevel);
            break;
#endif
#ifdef __GM_MANUF__
        case OemType_GM:
            status = obd2datalog_gm_prevalidate_task(ecm_index, 
                                                     commtype, commlevel);
            break;
#endif
        default:
            status = S_SUCCESS;
            break;
        }
        break;
    case CommType_VPW:
        break;
    case CommType_SCP32:
        obd2scp_init(Obd2scpType_24Bit);
        status = S_SUCCESS;
        break;    
    case CommType_KLINE:
    case CommType_KWP2000:
        status = iso_init(&commtype);
        break;
    default:
        status = S_NOTSUPPORT;
        break;
    }
    if (status == S_SUCCESS)
    {
        obd2datalog_datalogmode_init();
    }
    return status;    
}

/**
 *  @brief Set comm for datalog session
 *
 *  @param [in]     *commtype  Vehicle comm type
 *  @param [in]     *commlevel Vehicle comm level
 *
 *  @return Status
 */
u8 obd2datalog_setcomm(VehicleCommType *commtype, VehicleCommLevel *commlevel)
{
    VehicleCommType temp;
    u8 status = S_SUCCESS;
    
    switch(commtype[0])
    {
    case CommType_CAN:
        if (!SETTINGS_IsDemoMode())
        {
            obd2can_init(Obd2canInitNormal,0);
        }
        break;
    case CommType_SCP:
    case CommType_SCP32:
        if (!SETTINGS_IsDemoMode())
        {
            obd2scp_init(Obd2scpType_24Bit);
        }
        break;
    case CommType_VPW:
        if (!SETTINGS_IsDemoMode())
        {
            obd2vpw_init(TRUE, FALSE);
        }
        break;
    case CommType_KLINE:
    case CommType_KWP2000:
        if (!SETTINGS_IsDemoMode())
        {
            status = iso_init(&temp);
        }
        break;
    default:
        //log_push_error_point(0x4988); Abandoned
        //log_push_error_point(0x49A7); Abandoned
        status = S_COMMTYPE;
    }
    return status;
}

/**
 *  @brief Cleanup datalog packets
 *
 *  @param [in]     *commtype  Vehicle comm type
 *  @param [in]     *commlevel Vehicle comm level
 *
 *  @return Status
 */
u8 obd2datalog_cleanup_packets(VehicleCommType *commtype,
                               VehicleCommLevel *commlevel)
{
    u8 status = S_SUCCESS;

    obd2_open();

    switch(commtype[0])
    {
    case CommType_CAN:
        if (!SETTINGS_IsDemoMode())
        {
#ifdef __FORD_MANUF__            
            if (gObd2info.oemtype == OemType_FORD)
            {
                obd2can_ford_datalog_cleanup_packets(commlevel);
            }
#endif
        }
        break;
    case CommType_SCP:
    case CommType_SCP32:
        if (!SETTINGS_IsDemoMode())
        {
            if (obd2scp_datalog_cleanup_packets() != S_SUCCESS &&
                commtype[0] == CommType_SCP32)
            {
                // Some SCP32's will fail packet cleanup and require another init.
                obd2scp_init(Obd2scpType_24Bit);
            }
        }
        break;
    case CommType_VPW:
    case CommType_KLINE:
    case CommType_KWP2000:
        break;
    default:
        //log_push_error_point(0x49A8); Abandoned
        //log_push_error_point(0x4989); Abandoned
        status = S_COMMTYPE;
    }
    return status;
}

/**
 *  @brief Validate PID for datalog session
 *
 *  @param [in]     ecm_id      ECM ID (CAN only)
 *  @param [in]     *dlxblock   DLX data block to validate
 *  @param [in]     commtype    Vehicle comm type
 *  @param [in]     commlevel   Vehicle comm level
 *
 *  @return Status
 */
u8 obd2datalog_validatepid(u32 ecm_id, DlxBlock *dlxblock,
                           VehicleCommType commtype, VehicleCommLevel commlevel)
{
    u8 status = S_SUCCESS;

    switch (commtype)
    {
    case CommType_CAN:
        if (!SETTINGS_IsDemoMode())
        {
            status = obd2can_validatepid(ecm_id, dlxblock, commlevel);
        }
        break;
    case CommType_VPW:
        if (!SETTINGS_IsDemoMode())
        {
            status = obd2vpw_validatepid(dlxblock, commlevel);
        }
        break;
    case CommType_SCP:
    case CommType_SCP32:
        if (!SETTINGS_IsDemoMode())
        { 
            status = obd2scp_validatepid(dlxblock, commlevel);
        }
        break;
    case CommType_KLINE:
    case CommType_KWP2000:
        if (!SETTINGS_IsDemoMode())
        {
            /* K-LINE and KWP2000 do not support enhanced diagnostics */
            status = S_FAIL;
        }
        break;
    default:
        //log_push_error_point(0x498A); Abandoned
        status = S_COMMTYPE;
        break;
    }

    return status; 
}

/**
 *  @brief Validate DMR for datalog session
 *
 *  @param [in]     ecm_id      ECM ID (CAN only)
 *  @param [in]     *dlxblock   DLX data block to validate
 *  @param [in]     commtype    Vehicle comm type
 *  @param [in]     commlevel   Vehicle comm level
 *
 *  @return Status
 */
u8 obd2datalog_validatedmr(u32 ecm_id, DlxBlock *dlxblock,
                           VehicleCommType commtype, VehicleCommLevel commlevel)
{
    u8 status = S_SUCCESS;

    switch (commtype)
    {
    case CommType_CAN:
        if (!SETTINGS_IsDemoMode())
        {
            status = obd2can_validatedmr(ecm_id, dlxblock, commlevel);
        }
        break;
    case CommType_VPW:
        if (!SETTINGS_IsDemoMode())
        {
            status = obd2vpw_validatedmr(dlxblock, commlevel);
        }
        break;
    case CommType_SCP:
    case CommType_SCP32:
        if (!SETTINGS_IsDemoMode())
        { 
            status = obd2scp_validatedmr(dlxblock, commtype, commlevel);
        }
        break; 
    case CommType_KLINE:
    case CommType_KWP2000:
        if (!SETTINGS_IsDemoMode())
        {
            /* No DMR's available */
            status = S_FAIL;
        }
        break;
    default:
        //log_push_error_point(0x498B); Abandoned
        //log_push_error_point(0x49BA); Abandoned
        status = S_COMMTYPE;
        break;
    }
    return status; 
}

/**
 *  @brief Validate DMR for datalog session
 *
 *  @param [in]     ecm_id      ECM ID (CAN only)
 *  @param [in]     *dlxblock   DLX data block to validate
 *  @param [in]     commtype    Vehicle comm type
 *  @param [in]     commlevel   Vehicle comm level
 *
 *  @return Status
 */
u8 obd2datalog_validateByRapidPacketSetup(u32 ecm_id, DlxBlock *dlxblock,
                                          VehicleCommType commtype, 
                                          VehicleCommLevel commlevel)
{
    u8 status = S_SUCCESS;

    switch (commtype)
    {
    case CommType_CAN:
        if (!SETTINGS_IsDemoMode())
        {
            status = obd2can_validateByRapidPacketSetup(ecm_id, dlxblock, commlevel);
        }
        break;
    case CommType_VPW:
        if (!SETTINGS_IsDemoMode())
        {
            status = obd2vpw_validateByRapidPacketSetup(dlxblock, commlevel);
        }
        break;
    case CommType_SCP:
    case CommType_SCP32:
        if (!SETTINGS_IsDemoMode())
        { 
            status = obd2scp_validateByRapidPacketSetup(dlxblock, commtype, 
                                                        commlevel);
        }
        break;  
    case CommType_KLINE:
    case CommType_KWP2000:
        if (!SETTINGS_IsDemoMode())
        {
            /* No Packets available */
            status = S_FAIL;
        }
        break;
    default:
        status = S_COMMTYPE;
        break;
    }       
    return status; 
}

/**
 *  @brief Validate OSC PID for datalog session
 *
 *  @param [in]     ecm_id      ECM ID (CAN only)
 *  @param [in]     *dlxblock   DLX data block to validate
 *  @param [in]     commtype    Vehicle comm type
 *  @param [in]     commlevel   Vehicle comm level
 *
 *  @return Status
 */
u8 obd2datalog_validate_oscpid(u32 ecm_id, DlxBlock *dlxblock,
                               VehicleCommType commtype,
                               VehicleCommLevel commlevel)
{
    u8 status = S_SUCCESS;

    switch (commtype)
    {
    case CommType_CAN:
        if (!SETTINGS_IsDemoMode())
        {
            status = obd2can_validate_oscpid(ecm_id, dlxblock, commlevel);
        }
        break;
    default:
        status = S_NOTSUPPORT;
        break;
    }
    return status;
}

/**
 *  @brief Evaluate PID packet fit
 *
 *  @param [in]     commtype            Vehicle comm type
 *  @param [in]     commlevel           Vehicle comm level
 *  @param [in]     valid_index_list    Valid item list
 *  @param [in]     valid_index_count   Valid items
 *
 *  @return Status
 */
u8 obd2datalog_evaluate_fit(VehicleCommType *commtype, VehicleCommLevel *commlevel,
                            u16 *valid_index_list, u16 *valid_index_count)
{
    u8  i;
    u16 count;
    u8  status;

    obd2_open();

    switch (gObd2info.oemtype)
    {
#ifdef __DCX_MANUF__ 
    case OemType_DCX:
        status = obd2datalog_dcx_evaluate_fit(commtype, commlevel, 
                                              valid_index_list, valid_index_count);
        break;
#endif
#ifdef __FORD_MANUF__ 
    case OemType_FORD:
        status = obd2datalog_ford_evaluate_fit(commtype, commlevel, 
                                               valid_index_list, valid_index_count);
        break;
#endif
#ifdef __GM_MANUF__ 
    case OemType_GM:
        status = obd2datalog_gm_evaluate_fit(commtype, commlevel, 
                                             valid_index_list, valid_index_count);
        break;
#endif
    default:
        /* Generic datalog does not support packets */
        status = S_NOTFIT;
        break;
    }
    
    /* Evaluate non-packet signals */
    if (status == S_NOTFIT || status == S_COMMTYPE)
    {
        status = obd2_datalog_evaluatesignalsetup(commlevel);
        if (status == S_SUCCESS)
        {
            count = 0;
            for(i=0;i<dataloginfo->datalogsignalcount;i++)
            {
                if ((dataloginfo->datalogsignals[i].Control & DATALOG_CONTROL_SKIP) == 0)
                {
                    switch (dataloginfo->datalogsignals[i].PidType)
                    {
                        case PidTypeAnalog:
                        case PidTypeMode1:
                            valid_index_list[count++] = i;
                            break;
                    }
                }
                if (count >= DATALOG_MAX_VALID_PER_SESSION)
                {
                    break;
                }
            }
            *valid_index_count = count;
            if (count > 0)
            {
                status = S_SUCCESS;
            }
        }
    }
    return status;
}
//------------------------------------------------------------------------------
// Reads the header and block data from a datalog file
// Input:   const u8 *datalogfilename (must be in user folder)
// Output:  DlxHeader *dlxheader
//          DlxBlock *dlxblock
// Return:  u8  status
// July 12, 2009
// Engineer: Patrick Downs, Quyen Leba
//------------------------------------------------------------------------------
u8 obd2datalog_getdatalogfileinfo(const u8 *datalogfilename,
                                  DlxHeader *dlxheader, DlxBlock *dlxblock)
{
    F_FILE *fptr;
    u32 bytesread;
    u8  count;
    u8  status;

    status = S_SUCCESS;
    
    if (dlxheader == NULL || dlxblock == NULL)
    {
        log_push_error_point(0x4901);
        return S_INPUT;
    }

    status = obd2datalog_validate_dlxfile(datalogfilename);
    if (status != S_SUCCESS)
    {
        log_push_error_point(0x493A);
        return status;
    }
    
    fptr = genfs_user_openfile(datalogfilename,"r");
    if(!fptr)
    {
        log_push_error_point(0x4902);
        return S_OPENFILE;
    }
    
    // Save the DLX filename
    strcpy((char*)dataloginfo->dlxfilename, (char*)datalogfilename);
        
    // Read in DLX header info
    bytesread = fread((char*)dlxheader, 1, sizeof(DlxHeader), fptr);
    if(bytesread != sizeof(DlxHeader))
    {
        log_push_error_point(0x4903);
        status = S_READFILE;
        goto obd2datalog_getdatalogfileinfo_done;
    }
#if DATALOG_USE_ENCRYPTION
    crypto_blowfish_decryptblock_external_key((u8*)dlxheader,sizeof(DlxHeader));
#endif
    
    if (dlxheader->count == 0)
    {
        log_push_error_point(0x4904);
        status = S_BADCONTENT;
        goto obd2datalog_getdatalogfileinfo_done;
    }
    else if (dlxheader->count > MAX_DATALOG_DLXBLOCK_COUNT)
    {
        log_push_error_point(0x4905);
        status = S_NOTFIT;
        goto obd2datalog_getdatalogfileinfo_done;
    }

    count = 0;
    while(!(feof(fptr)))
    {
        if (count > dlxheader->count)
        {
            break;
        }
        bytesread = fread((char*)&dlxblock[count],1,sizeof(DlxBlock),fptr);
        if (bytesread != sizeof(DlxBlock))
        {
            log_push_error_point(0x4906);
            status = S_READFILE;
            goto obd2datalog_getdatalogfileinfo_done;
        }
#if DATALOG_USE_ENCRYPTION
        crypto_blowfish_decryptblock_external_key((u8*)&dlxblock[count],sizeof(DlxBlock));
#endif
        count++;
    }
    if (count != dlxheader->count)
    {
        log_push_error_point(0x4907);
        status = S_BADCONTENT;
        goto obd2datalog_getdatalogfileinfo_done;
    }
    
obd2datalog_getdatalogfileinfo_done:
    if (fptr)
    {
        genfs_closefile(fptr);
    }

    return status;
}

//------------------------------------------------------------------------------
// Inputs:  const u8 *datalogfilename (must be in user folder)
//          const u8 *datalogfeaturefilename (must be in user folder)
//          const u8 *validpidfilename (must be in user folder)
//          const u8 *validpidlist
//          u16 validpidcount
// Return:  u8  status
// Engineer: Quyen Leba
// IMPORTANT: when validpidlist changed to u16[], correct it in this function
// Note: for now (Feb102010), only X3P uses this function
//------------------------------------------------------------------------------
u8 obd2datalog_generatevalidpidfile(const u8 *datalogfilename,
                                    const u8 *datalogfeaturefilename,
                                    const u8 *validpidfilename,
                                    const u8 *validpidlist, u16 validpidcount)
{
    F_FILE  *datalogfptr;
    F_FILE  *featurefptr;
    F_FILE  *validfptr;
    DlxHeader dlxheader_datalog;
    DlxHeader dlxheader_feature;
    DlxBlock dlxblock;
    u8  status;
    u32 i;
    u32 bytecount;
    u16 count;
    u16 totalpidcount;

    status = obd2datalog_validate_dlxfile(datalogfilename);
    if (status != S_SUCCESS)
    {
        log_push_error_point(0x493B);
        return status;
    }
    status = obd2datalog_validate_dlxfile(datalogfeaturefilename);
    if (status != S_SUCCESS)
    {
        log_push_error_point(0x493C);
        return status;
    }

    status = S_SUCCESS;
    datalogfptr = featurefptr = validfptr = NULL;
    datalogfptr = genfs_user_openfile(datalogfilename,"r");
    if (datalogfptr == NULL)
    {
        log_push_error_point(0x4908);
        status = S_OPENFILE;
        goto obd2datalog_generatevalidpidfile_done;
    }
    
    featurefptr = genfs_user_openfile(datalogfeaturefilename,"r");
    if (featurefptr == NULL)
    {
        log_push_error_point(0x4909);
        status = S_OPENFILE;
        goto obd2datalog_generatevalidpidfile_done;
    }
    
    validfptr = genfs_user_openfile(validpidfilename,"w");
    if (validfptr == NULL)
    {
        log_push_error_point(0x490A);
        status = S_OPENFILE;
        goto obd2datalog_generatevalidpidfile_done;
    }
    
    //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    // Handle header data
    //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    bytecount = fread((char*)&dlxheader_datalog,1,sizeof(DlxHeader),datalogfptr);
    if (bytecount != sizeof(DlxHeader))
    {
        log_push_error_point(0x490B);
        status = S_READFILE;
        goto obd2datalog_generatevalidpidfile_done;
    }
#if DATALOG_USE_ENCRYPTION
    crypto_blowfish_decryptblock_external_key((u8*)&dlxheader_datalog,sizeof(DlxHeader));
#endif

    bytecount = fread((char*)&dlxheader_feature,1,sizeof(DlxHeader),featurefptr);
    if (bytecount != sizeof(DlxHeader))
    {
        log_push_error_point(0x490C);
        status = S_READFILE;
        goto obd2datalog_generatevalidpidfile_done;
    }
#if DATALOG_USE_ENCRYPTION
    crypto_blowfish_decryptblock_external_key((u8*)&dlxheader_feature,sizeof(DlxHeader));
#endif

    totalpidcount = dlxheader_datalog.count;
    dlxheader_datalog.count = validpidcount + dlxheader_feature.count;
    //TODOQ: also modify description (strcat 'validated' or something),
    //commtype, etc - it's optional

    dlxheader_datalog.header_crc32e = 0xFFFFFFFF;
    crc32e_reset();
    dlxheader_datalog.header_crc32e = crc32e_calculateblock
        (0xFFFFFFFF,(u32*)&dlxheader_datalog,sizeof(DlxHeader)/4);

#if DATALOG_USE_ENCRYPTION
    crypto_blowfish_encryptblock_external_key((u8*)&dlxheader_datalog,sizeof(DlxHeader));
#endif
    bytecount = fwrite((char*)&dlxheader_datalog,1,sizeof(DlxHeader),validfptr);
    if (bytecount != sizeof(DlxHeader))
    {
        log_push_error_point(0x490D);
        status = S_WRITEFILE;
        goto obd2datalog_generatevalidpidfile_done;
    }
    
    //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    // Handle block data of datalog file
    //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    count = 0;
    while(!(feof(datalogfptr)))
    {
        if (count > totalpidcount)
        {
            log_push_error_point(0x490E);
            status = S_BADCONTENT;
            goto obd2datalog_generatevalidpidfile_done;
        }
        bytecount = fread((char*)&dlxblock,1,sizeof(DlxBlock),datalogfptr);
        if (bytecount != sizeof(DlxBlock))
        {
            log_push_error_point(0x490F);
            status = S_READFILE;
            goto obd2datalog_generatevalidpidfile_done;
        }
#if DATALOG_USE_ENCRYPTION
        crypto_blowfish_decryptblock_external_key((u8*)&dlxblock,sizeof(DlxBlock));
#endif

        for(i=0;i<validpidcount;i++)
        {
            if (validpidlist[i] == count)
            {
#if DATALOG_USE_ENCRYPTION
                crypto_blowfish_encryptblock_external_key((u8*)&dlxblock,sizeof(DlxBlock));
#endif
                bytecount = fwrite((char*)&dlxblock,1,sizeof(DlxBlock),validfptr);
                if (bytecount != sizeof(DlxBlock))
                {
                    log_push_error_point(0x4910);
                    status = S_WRITEFILE;
                    goto obd2datalog_generatevalidpidfile_done;
                }
                break;
            }
        }
        count++;
    }
    
    //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    // Handle block data of feature file
    //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    count = 0;
    while(!(feof(featurefptr)))
    {
        if (count > dlxheader_feature.count)
        {
            log_push_error_point(0x4911);
            status = S_BADCONTENT;
            goto obd2datalog_generatevalidpidfile_done;
        }
        bytecount = fread((char*)&dlxblock,1,sizeof(DlxBlock),featurefptr);
        if (bytecount != sizeof(DlxBlock))
        {
            log_push_error_point(0x4912);
            status = S_READFILE;
            goto obd2datalog_generatevalidpidfile_done;
        }
#if DATALOG_USE_ENCRYPTION
        crypto_blowfish_decryptblock_external_key((u8*)&dlxblock,sizeof(DlxBlock));
#endif
        //----//
#if DATALOG_USE_ENCRYPTION
        crypto_blowfish_encryptblock_external_key((u8*)&dlxblock,sizeof(DlxBlock));
#endif
        bytecount = fwrite((char*)&dlxblock,1,sizeof(DlxBlock),validfptr);
        if (bytecount != sizeof(DlxBlock))
        {
            log_push_error_point(0x4913);
            status = S_WRITEFILE;
            goto obd2datalog_generatevalidpidfile_done;
        }
        count++;
    }
    
obd2datalog_generatevalidpidfile_done:
    if (datalogfptr)
    {
        genfs_closefile(datalogfptr);
    }
    if (featurefptr)
    {
        genfs_closefile(featurefptr);
    }
    if (validfptr)
    {
        genfs_closefile(validfptr);
    }
    return status;
}

//------------------------------------------------------------------------------
// Create a Datalog Features DLX with the analog input information to be 
// retrieved via filetransfer by the TSX.
// Input:   const u8 *filename (must be in user folder)
// Return:  u8  status
// Engineer: Patrick Downs, Quyen Leba
//------------------------------------------------------------------------------
u8 obd2datalog_createdatalogfeaturefile(const u8 *filename)
{
    PROPERTIES_VB_INFO properties_vb_info;
    u32 properties_vb_info_length;
    u8  status;
        
    // Get the vehicle board properties and check the board revision
    status = properties_vb_getinfo((u8*)&properties_vb_info, &properties_vb_info_length);
    if(status != S_SUCCESS)
    {
        // Can't get vehicle board info
        log_push_error_point(0x492B);
        return S_FAIL;
    }
    
    if(datalogfeatures_check_file(filename, properties_vb_info.board_rev) != S_SUCCESS)
    {
        // Doesn't exist or wrong contents are incorrect
        if(datalogfeatures_create_file(filename, properties_vb_info.board_rev) != S_SUCCESS)
        {
            log_push_error_point(0x492C);
            return S_FAIL; // Failed to create the file
        }
    }
    
    return S_SUCCESS;
}

//------------------------------------------------------------------------------
// Inputs:  DlxBlock *dlxblock
//          VehicleCommType vehiclecommtype
//          VehicleCommLevel vehiclecommlevel
// Return:  u8  status
// Engineer: Quyen Leba
//------------------------------------------------------------------------------
u8 obd2datalog_validate_by_dlx_block(DlxBlock *dlxblock,
                                     VehicleCommType vehiclecommtype,
                                     VehicleCommLevel vehiclecommlevel)
{
    u32 ecm_id;
    u8  status;

    // Validate the correct PID OEM type
    if (dlxblock->pidOemType != OEM_TYPE && 
        dlxblock->pidOemType != OemType_Unknown)
    {
        return S_FAIL;
    }
    
    // Validate the Pid Comm Type
    if (dlxblock->pidCommType != vehiclecommtype &&
            dlxblock->pidCommType != CommType_Unknown)
    {
        return S_COMMTYPE;
    }
    
    // Validate the Pid Comm Level
    if (dlxblock->pidCommLevel != vehiclecommlevel &&
            dlxblock->pidCommLevel != CommLevel_Unknown)
    {
        return S_COMMLEVEL;        
    }

    // This is really only necessary for CAN
    // SCP & VPW are hardcoded to use 0x10.
    // But when we add additional communications types which support more
    // addresses this will need to be updated.
    if (vehiclecommtype == CommType_CAN)
    {
        /* Only second ECU supports generic diagnostics */
        if (dlxblock->pidModuleType == 0 &&
            gObd2info.ecu_block[1].ecutype == EcuType_ECM)
        {
            ecm_id = gObd2info.ecu_block[1].ecu_id;
        }
        /* ECM */
        else if (dlxblock->pidModuleType == 0)
        {
            ecm_id = gObd2info.ecu_block[0].ecu_id;
        }
        /* TCM */
        else if (dlxblock->pidModuleType == 1)
        {
            ecm_id = gObd2info.ecu_block[1].ecu_id;
        }
        else
        {
            log_push_error_point(0x492D);
            return S_NOTSUPPORT;
        }
    }
    else
    {
        // SCP / VPW can only datalog PCM
        if (dlxblock->pidModuleType != 0)
            return S_NOTSUPPORT;
            
        ecm_id = 0x10;
        //do nothing for now (i.e. SCP & VPW)
    }
       
    switch (dlxblock->pidType)
    {
        case PidTypeRegular:
            status = obd2datalog_validatepid(ecm_id,dlxblock,vehiclecommtype,
                                             vehiclecommlevel); 
            break;
        case PidTypeDMR:
            status = obd2datalog_validatedmr(ecm_id,dlxblock,vehiclecommtype,
                                             vehiclecommlevel);
            break;
        case PidTypeAnalog:
            status = S_SUCCESS;
            break;
        case PidTypeOSC:
            status = obd2datalog_validate_oscpid(ecm_id,dlxblock,vehiclecommtype,
                                                 vehiclecommlevel);
            break;
        case PidTypeMode1:
            status = obd2datalog_validatepid_mode1(ecm_id,dlxblock,vehiclecommtype);
            break;
        default:
            // Unknown pid type, ignore (invalid) for now
            log_push_error_point(0x492F);
            status = S_NOTSUPPORT;
            break;
    }
    
    return status;
}

//------------------------------------------------------------------------------
// Validate an sae pid
// Inputs:  u32 ecm_id
//          DlxBlock *dlxblock
//          VehicleCommType commtype
// Return:  u8  status
// Engineer: Tristen Pierson
// Description: This function checks if a SAE PID is supported by reading a PID
//              containing supported PIDs. This is neccessary because if an 
//              invalid PID is read, there is no negative response which means
//              there will be a longer wait before polling for the next PID.
// Reference: See J1979-DA Appendix A
//------------------------------------------------------------------------------
u8 obd2datalog_validatepid_mode1(u32 ecm_id, DlxBlock *dlxblock,
                                 VehicleCommType vehiclecommtype)
{    
    u8  status = S_FAIL;
    u8  data[DATALOG_MAX_PID_SIZE];
    u8  pidGroup;
    u8  testPid;
    u32 groupMask;    
    u8  pid = (u8)dlxblock->pidAddr;
   
    if (SETTINGS_IsDemoMode())
    {
        return S_SUCCESS;
    }
    
    pidGroup = (pid - 1) & 0xE0;
    testPid = 0x00;
    
    // Only validate PCM OBD PIDs if broadcast mode (req = 0x7DF)
    if (vehiclecommtype == CommType_CAN &&
        gDatalogmode.ecmBroadcast == TRUE && 
        ecm_id != Obd2CanEcuId_7E0)
    {
        return S_FAIL;
    }
    
    // Poll for supported PID groups
    while (testPid <= pidGroup)
    {        
        status = obd2_readpid_mode1(gDatalogmode.ecmBroadcast, ecm_id, 
                                    testPid, 4, data, vehiclecommtype);        
        if (status != S_SUCCESS && gDatalogmode.ecmBroadcast == FALSE)
        {
            gDatalogmode.ecmBroadcast = TRUE;
            
            status = obd2_readpid_mode1(gDatalogmode.ecmBroadcast, ecm_id, 
                                        testPid, 4, data, vehiclecommtype);
            if (status != S_SUCCESS)
            {                
                // If first PID ($00) read fails here, J1979 is probably not supported
                gDatalogmode.ecmBroadcast = FALSE;
                break;
            }
        }
        
        // Conform mask for J1979 bit evaluation
        memcpy(&groupMask, &data[0], sizeof(data));
        groupMask =  __REV(groupMask);        
        
        // Check if PID is in this group
        if (testPid == pidGroup)
        {
            // Check if PID is supported
            if ((groupMask & (1 << (32-(pid & 0x1F)))) > 0)
            {
                status = S_SUCCESS;
            }
            else
            {
                status = S_FAIL;
            }
        }        
        // Check if next PID group is supported
        else if ((groupMask & 0x1) > 0)
        {
            testPid += 0x20;
            continue;
        }
        // No PIDs are supported
        else
        {            
            status = S_FAIL;            
        }
        break;
    } // while (testPid...
    
    // Validate to make sure PID is really supported
    if (status == S_SUCCESS)
    {   
        status = obd2_readpid_mode1(gDatalogmode.ecmBroadcast, ecm_id, pid, 
                                    dlxblock->pidSize, data, vehiclecommtype);
    }        
    
    return status;   
}

//------------------------------------------------------------------------------
// Validate/Check voltage PID
// Inputs:  u8  mode (0 = validate, 1 = test)
// Return:  u8  status
// Engineer: Tristen Pierson
// Description: This function is used to validate and test control module
//              voltage PID ($42). This can be used to detect key off in some
//              vehicles.
// Reference: See J1979-DA
//------------------------------------------------------------------------------
u8 obd2datalog_check_voltage_mode1(u8 mode)
{
    DlxBlock testPid;
    u16 data;
    u8  status; 
    u32 ecm_id;

    testPid.pidAddr = 0x42;         // Control module voltage = $42
    testPid.pidSize = 2;            // 2 data bytes
    
    switch (dataloginfo->commtype[0])
    {
    case CommType_CAN:
        ecm_id = 0x7E0;
        break;
    case CommType_SCP:
    case CommType_SCP32:
    case CommType_VPW:
        ecm_id = NULL;
        break;
    default:
        return S_FAIL;
    }       
        
    switch (mode)
    {    
    case 0: // Verify voltage PID and Validate Data
        status = obd2datalog_validatepid_mode1(ecm_id, &testPid, 
                                               dataloginfo->commtype[0]);
        if (status != S_SUCCESS)
        {
            return S_FAIL;
        }
    case 1: // Validate Data (Control Module Voltage > 0V)
        status = obd2_readpid_mode1(gDatalogmode.ecmBroadcast, ecm_id, 
                                    testPid.pidAddr, testPid.pidSize, 
                                    (u8*)&data, dataloginfo->commtype[0]);
        if ((status == S_SUCCESS) && (data == 0))
        {
            status = S_FAIL;         
        }
        break;
    default:
        status = S_FAIL;
        break;
    }
    if (status != S_SUCCESS)
    {
        status = S_FAIL;
    }
    
    return status;        
}
//------------------------------------------------------------------------------
// Get datalog data (PIDs, EPIDs, and SAE PIDs)
// Input:   VehicleCommLevel commlevel[ECM_MAX_COUNT]
// Return:  u8  status
// Engineer: Tristen Pierson
// Description: This function gets datalog data by non-rapid packet polling. 
//              The function executes until either the entire signal table has
//              been polled, or the fixed timing interval expires. If the
//              interval expires, the state is retained so the next time this  
//              function is called, datalogging resumes.
//------------------------------------------------------------------------------
u8 obd2datalog_getdata_singlerate(VehicleCommLevel *vehiclecommlevel)
{
    static u8 i;            // Retains index count (do not remove static qualifier)   
    static u8 errorcount;   // Retains error count (do not remove static qualifier)
    u8  pidcount;           // Counts number of PIDs read
    u8  pidData[4];
    u8  data[DATALOG_MAX_PID_SIZE];
    u16 datalength;
    u32 rtcvalue;
    u32 ecm_id; 
    u8  ecm_index;    
    bool skip;
    u8  j;
    u8  status;    
    
    pidcount = 0;
    rtcvalue = rtc_getvalue();

    while (1)
    {
        // Polling cannot exceed fixed interval
        if ((rtc_getvalue() - rtcvalue) >= DATALOG_POLL_INTERVAL)
        {
            break;
        }
        // Signals are polled only once
        if (pidcount++ > dataloginfo->datalogsignalcount)
        {
            break;
        }        
        // Enforce signal loop bound        
        if (++i >= dataloginfo->datalogsignalcount)
        {
            i = 0;
        }        
        // Skip items already handled by rapid packet
        if (dataloginfo->datalogsignals[i].SignalType.Generic.PacketNumber != DATALOG_SKIPPED_PACKETID)
        {
            continue;
        } 
        // Skip analog pids
        if (dataloginfo->datalogsignals[i].PidType == PidTypeAnalog)
        {
            continue;
        }
        // Skip other Packet PIDs if covered by previous Packet PID
        if (dataloginfo->datalogsignals[i].Flags & DATALOG_PIDFLAGS_IS_PACKET_PID)
        {
            skip = FALSE;
            for (j = 0; j < i; j++)
            {
                if (dataloginfo->datalogsignals[j].Flags & DATALOG_PIDFLAGS_IS_PACKET_PID &&
                    dataloginfo->datalogsignals[j].Address == dataloginfo->datalogsignals[i].Address)
                {
                    skip = TRUE;
                    break;
                }
            }
            if (skip == TRUE)
            {
                continue;   // Next iteration
            }
        }
        // Skip other bitmapped PIDs if covered by previous poll
        if (dataloginfo->datalogsignals[i].Flags & DATALOG_PIDFLAGS_DATATYPE_BIT)
        {
            skip = FALSE;
            for (j = 0; j < i; j++)
            {
                if (dataloginfo->datalogsignals[j].Flags & DATALOG_PIDFLAGS_DATATYPE_BIT &&
                    dataloginfo->datalogsignals[j].Address == dataloginfo->datalogsignals[i].Address)
                {
                    skip = TRUE;
                    break;
                }
            }
            if (skip == TRUE)
            {
                continue;   // Next iteration
            }
        }

        /* Only second ECU supports generic diagnostics */
        if(dataloginfo->datalogsignals[i].EcmIndex == 0 &&
           gObd2info.ecu_block[1].ecutype == EcuType_ECM)
        {
            ecm_index = 1;
        }
        else
        {
            ecm_index = dataloginfo->datalogsignals[i].EcmIndex;
        }
        
        // Clear data buffer
        memset(data,0,sizeof(data));
        
        // Clear status
        status = S_EMPTY;
        
        // Check comtype and poll pid/address
        switch (dataloginfo->commtype[ecm_index])
        {
            case CommType_CAN:
                ecm_id = gObd2info.ecu_block[ecm_index].ecu_id;
                // Check signal type and read pid/address
                switch (dataloginfo->datalogsignals[i].PidType)
                {
                    case PidTypeRegular:
                        status = obd2can_read_data_bypid(ecm_id,
                                    (u16)dataloginfo->datalogsignals[i].Address,
                                    data, (u8*)&datalength,TRUE);
                        break;
                    case PidTypeDMR:
                        status = obd2can_read_memory_address(ecm_id,
                                    dataloginfo->datalogsignals[i].Address,
                                    (u16)dataloginfo->datalogsignals[i].Size,
                                    gDatalogmode.addressType,DEFAULT_CMD_MEMORY_SIZE,
                                    data,&datalength,FALSE,
                                    vehiclecommlevel[ecm_index]);
                        break;
                    case PidTypeMode1:                        
                        status = obd2can_read_data_bypid_mode1(
                                   gDatalogmode.ecmBroadcast,ecm_id,
                                   (u8)dataloginfo->datalogsignals[i].Address,
                                   data, (u8*)&datalength, TRUE);            
                        break;
                    default:
                        break;
                }
                break;
            case CommType_VPW:
                // Check signal type and read pid/address
                switch (dataloginfo->datalogsignals[i].PidType)
                {
                    case PidTypeRegular:
                        status = obd2vpw_read_data_bypid(
                                    (u16)dataloginfo->datalogsignals[i].Address, 
                                    dataloginfo->datalogsignals[i].Size, data);
                        break;
                    case PidTypeDMR:
                        status = obd2vpw_read_memory_byaddress(
                                    dataloginfo->datalogsignals[i].Address,data);                           
                        break;
                    case PidTypeMode1:
                        status = obd2vpw_read_data_bypid_mode1(
                                    (u8)dataloginfo->datalogsignals[i].Address,
                                    dataloginfo->datalogsignals[i].Size,
                                    data);           
                        break;
                    default:
                        break;
                }
                break;
            case CommType_SCP:
            case CommType_SCP32:            
                 // Check signal type and read pid/address
                switch (dataloginfo->datalogsignals[i].PidType)
                {
                    case PidTypeRegular:
                        status = obd2scp_read_data_bypid(
                                    (u16)dataloginfo->datalogsignals[i].Address, 
                                    dataloginfo->datalogsignals[i].Size, data);
                        break;
                    case PidTypeDMR:
                        // SCP32
                        if (dataloginfo->commtype[ecm_index] == CommType_SCP32)
                        {
                            status = obd2scp_read_memory_address(
                                    dataloginfo->datalogsignals[i].Address,
                                    dataloginfo->datalogsignals[i].Size,
                                    Address_32_Bits, data, &datalength, FALSE);
                        }
                        // SCP
                        else
                        {
                            status = obd2scp_read_memory_address(
                                    dataloginfo->datalogsignals[i].Address,
                                    dataloginfo->datalogsignals[i].Size,
                                    Address_24_Bits, data, &datalength, FALSE);
                        }
                        break;
                    case PidTypeMode1:
                        status = obd2scp_read_data_bypid_mode1(
                                    (u8)dataloginfo->datalogsignals[i].Address,
                                    dataloginfo->datalogsignals[i].Size,
                                    data);         
                        break;
                    default:
                        break;
                }                    
                break;
            case CommType_KLINE:
            case CommType_KWP2000:
                switch (dataloginfo->datalogsignals[i].PidType)
                {
                    case PidTypeMode1:
                        status = obd2iso_read_data_bypid_mode1(
                                    (u8)dataloginfo->datalogsignals[i].Address,
                                    dataloginfo->datalogsignals[i].Size,
                                    data,dataloginfo->commtype[ecm_index]);         
                        break;
                    default:
                        break;
                }
                break;
            default:
                break;
        } // switch
       
        if (status == S_TIMEOUT || status == S_FAIL)
        {
            errorcount++;
        }
        else 
        {
            // Update timestamp and parse data
            errorcount = 0;
            dataloginfo->lastdatapoint_timestamp = rtc_getvalue();

            // Handle packet OBD PIDs - single rate only
            if (dataloginfo->datalogsignals[i].Flags & DATALOG_PIDFLAGS_IS_PACKET_PID)
            {
                // Skip other Packet PIDs if covered by previous Packet PID
                skip = FALSE;
                for (j = 0; j < i; j++)
                {
                    if (dataloginfo->datalogsignals[j].Flags & DATALOG_PIDFLAGS_IS_PACKET_PID &&
                        dataloginfo->datalogsignals[j].Address == dataloginfo->datalogsignals[i].Address)
                    {
                        skip = TRUE;
                        break;
                    }
                }
                if (skip == TRUE)
                {
                    continue;   // Next iteration
                }
                
                for (j = 0; j < dataloginfo->datalogsignalcount; j++)
                {
                    if (dataloginfo->datalogsignals[j].Flags & DATALOG_PIDFLAGS_IS_PACKET_PID &&
                        dataloginfo->datalogsignals[j].Address == dataloginfo->datalogsignals[i].Address)
                    {
                        obd2_parse_packet_pid(&data[0], (u8)datalength, 
                            dataloginfo->datalogsignals[j].packetStartPos,
                            &pidData[0], 
                            dataloginfo->datalogsignals[j].Size,
                            (dataloginfo->datalogsignals[i].Flags & DATALOG_PIDFLAGS_IS_BITS ? 1 : 0));
                        
                        obd2datalog_parserdata(dataloginfo->datalogsignals[j].EcmIndex, 
                                               j, 0, &pidData[0]);
                    }
                }
            }
            else
            {
                memcpy(&pidData, &data, dataloginfo->datalogsignals[i].Size);
                // Parse all bitmapped data within PID
                if (dataloginfo->datalogsignals[i].Flags & DATALOG_PIDFLAGS_DATATYPE_BIT)
                {
                    for (j = 0; j < dataloginfo->datalogsignalcount; j++)
                    {
                        if (dataloginfo->datalogsignals[j].Flags & DATALOG_PIDFLAGS_DATATYPE_BIT &&
                            dataloginfo->datalogsignals[j].Address == dataloginfo->datalogsignals[i].Address)
                        {                                
                            obd2datalog_parserdata(dataloginfo->datalogsignals[i].EcmIndex, 
                                                   j, 0, &pidData[0]);
                        }
                    }
                }
                // Or parse single PID data
                else
                {
                    obd2datalog_parserdata(dataloginfo->datalogsignals[i].EcmIndex, 
                                           i, 0, &pidData[0]);
                }
            }
        }
    } // while(1)

    // Check for timeout
    if (errorcount > DATALOG_MAX_TIMEOUTS)
    {
        return S_COMMLOST;
    }

    else
    {
        return S_SUCCESS;
    }
}

//------------------------------------------------------------------------------
// Inputs:  u8  *datalogfilename (a dlx filename; must be in user folder)
//          VehicleCommType vehiclecommtype[ECM_MAX_COUNT]
//          VehicleCommLevel vehiclecommlevel[ECM_MAX_COUNT]
//          u16 maxvalidpidallow
//          u8  *validpidlist (TODOQ: convert to u16* later)
//          u16 *validpidcount
//          funcptr_v_u8 reportprogressfunc
// Return:  u8  status
// Engineer: Patrick Downs, Quyen Leba
//TODOQ: validpidlist may become u16[]
//------------------------------------------------------------------------------
u8 obd2datalog_getvalidpidlist_fromfile(u8 *datalogfilename,
                                        VehicleCommType *vehiclecommtype,
                                        VehicleCommLevel *vehiclecommlevel,
                                        u16 maxvalidpidallow,
                                        u8 *validpidlist, u16 *validpidcount,
                                        funcptr_v_u8 reportprogressfunc)
{
    DlxHeader dlxheader;
    DlxBlock dlxblock;
    F_FILE *fptr;
    u32 bytesread;
    u16 currentvalidpidcount;
    u16 pidindex;
    u8  percentage;
    u8  ecm_index;
    u8  status;

    status = S_SUCCESS;
    currentvalidpidcount = 0;
    *validpidcount = 0;
        
    status = obd2datalog_validate_dlxfile(datalogfilename);
    if (status != S_SUCCESS)
    {
        log_push_error_point(0x493D);
        return status;
    }
    
    fptr = genfs_user_openfile(datalogfilename,"r");
    if(!fptr)
    {
        log_push_error_point(0x4917);
        return S_OPENFILE;
    }

    // Read in DLX header info
    bytesread = fread((char*)&dlxheader, 1, sizeof(DlxHeader), fptr);
    if(bytesread != sizeof(DlxHeader))
    {
        log_push_error_point(0x4918);
        status = S_READFILE;
        goto obd2datalog_getvalidpidlist_fromfile_done;
    }
#if DATALOG_USE_ENCRYPTION
    crypto_blowfish_decryptblock_external_key((u8*)&dlxheader,sizeof(DlxHeader));
#endif
    
    if (dlxheader.count == 0)
    {
        log_push_error_point(0x4919);
        status = S_BADCONTENT;
        goto obd2datalog_getvalidpidlist_fromfile_done;
    }
    else if (dlxheader.count > maxvalidpidallow)
    {
        log_push_error_point(0x491A);
        status = S_BADCONTENT;
        goto obd2datalog_getvalidpidlist_fromfile_done;
    }
    
    pidindex = 0;
    while(!(feof(fptr)))
    {
        //######################################################################
        // Get PID info
        //######################################################################
        if (pidindex > dlxheader.count)
        {
            break;
        }
        bytesread = fread((char*)&dlxblock,1,sizeof(DlxBlock),fptr);
        if (bytesread != sizeof(DlxBlock))
        {
            log_push_error_point(0x491B);
            status = S_READFILE;
            goto obd2datalog_getvalidpidlist_fromfile_done;
        }
#if DATALOG_USE_ENCRYPTION
        crypto_blowfish_decryptblock_external_key((u8*)&dlxblock,sizeof(DlxBlock));
#endif
        /* Only second ECU supports generic diagnostics */
        if (dlxblock.pidModuleType == 0 &&
            gObd2info.ecu_block[1].ecutype == EcuType_ECM)
        {
            ecm_index = 1;
        }
        /* ECM */
        else if (dlxblock.pidModuleType == 0)
        {
            ecm_index = 0;
            
        }
        /* TCM */
        else if (dlxblock.pidModuleType == 1)
        {
            ecm_index = 1;
        }
        else
        {
            //unknown module type; assume invalid
            continue;
        }
        //######################################################################
        // Validate this PID
        //######################################################################
        status = obd2datalog_validate_by_dlx_block(&dlxblock,
                                                   vehiclecommtype[ecm_index],
                                                   vehiclecommlevel[ecm_index]);
        if (status == S_SUCCESS)
        {
            validpidlist[currentvalidpidcount++] = pidindex;
        }

        if (reportprogressfunc)
        {
            percentage = (u8)(pidindex * 100 / dlxheader.count);
            //a helper function to process progress report
            reportprogressfunc(percentage);
        }
        
        pidindex++;
    }
    if (pidindex != dlxheader.count)
    {
        log_push_error_point(0x491C);
        status = S_BADCONTENT;
        goto obd2datalog_getvalidpidlist_fromfile_done;
    }
    
    status = S_SUCCESS;
    *validpidcount = currentvalidpidcount;
    
obd2datalog_getvalidpidlist_fromfile_done:
    if (fptr)
    {
        genfs_closefile(fptr);
    }

    return status;
}

//------------------------------------------------------------------------------
// Inputs:  DlxHeader *dlxheader
//          DlxBlock *dlxblocks
//          VehicleCommType vehiclecommtype[ECM_MAX_COUNT]
//          VehicleCommLevel vehiclecommlevel[ECM_MAX_COUNT]
//          u8  *validpidlist (TODOQ: convert to u16* later)
//          u16 *validpidcount
//          funcptr_v_u8 reportprogressfunc
// Return:  u8  status
// Engineer: Patrick Downs, Quyen Leba
//TODOQ: validpidlist may become u16[]
//------------------------------------------------------------------------------
u8 obd2datalog_getvalidpidlist(DlxHeader *dlxheader,DlxBlock *dlxblocks,
                               VehicleCommType *vehiclecommtype,
                               VehicleCommLevel *vehiclecommlevel,
                               u8 *validpidlist, u16 *validpidcount,
                               funcptr_v_u8 reportprogressfunc)
{
    u8  ecm_index;
    u32 ecm_id;
    u16 pidindex;
    u16 analogcount;
    u16 count;
    u8  status;
    u8  percentage;

    *validpidcount = 0;    
   
    status = obd2datalog_validate_dlxheader(dlxheader,NULL);
    if (status != S_SUCCESS)
    {
        log_push_error_point(0x4930);
        return status;
    }
    
    // Run pre-validate task
    for (u8 i = 0; i < ECM_MAX_COUNT; i++)
    {
        if (vehiclecommtype[i] != CommType_Unknown)
        {
            obd2datalog_prevalidate_task(i,vehiclecommtype[i],vehiclecommlevel[i]);
        }
    }
    
    // Check for SAE PIDs
    for(pidindex=0;pidindex<dlxheader->count;pidindex++)
    {
        if (dlxblocks[pidindex].pidType == PidTypeMode1 &&
            gDatalogmode.ecmBroadcast == TRUE)
        {
            gDatalogmode.obdPids = TRUE;
            break;
        }
    }
    
    count = 0;
    analogcount = 0;
    for(pidindex=0;pidindex<dlxheader->count;pidindex++)
    {
        /* Only second ECU supports generic diagnostics */
        if (dlxblocks[pidindex].pidModuleType == 0 &&
            gObd2info.ecu_block[1].ecutype == EcuType_ECM)
        {
            ecm_index = 1;
        }
        /* ECM */
        else if (dlxblocks[pidindex].pidModuleType == 0)
        {
            ecm_index = 0;
            
        }
        /* TCM */
        else if (dlxblocks[pidindex].pidModuleType == 1)
        {
            ecm_index = 1;
        }
        else
        {
            //unknown module type; assume invalid
            continue;
        }
        
        // This is really only necessary for CAN
        // SCP & VPW are hardcoded to use 0x10.
        // But when we add additional communications types which support more
        // addresses this will need to be updated.
        if (vehiclecommtype[ecm_index] == CommType_Unknown)
        {
            //comm of this ecm is unknown; assume invalid
            continue;
        }
        else if (vehiclecommtype[ecm_index] == CommType_CAN)
        {
            ecm_id = gObd2info.ecu_block[ecm_index].ecu_id;
        }
        else
        {
            ecm_id = 0x10;
            //do nothing for now (i.e. SCP & VPW)
        }
        
        switch (dlxblocks[pidindex].pidType)
        {
            case PidTypeMode1:
                status = obd2datalog_validatepid_mode1(ecm_id, &dlxblocks[pidindex],
                                                       vehiclecommtype[ecm_index]);
                if (status == S_SUCCESS)
                {
                    gDatalogmode.obdPids = TRUE;
                }
                break;    
            case PidTypeRegular:
                status = obd2datalog_validatepid(ecm_id, &dlxblocks[pidindex],
                                                 vehiclecommtype[ecm_index],
                                                 vehiclecommlevel[ecm_index]);                
                break;
            case PidTypeDMR:
                status = obd2datalog_validatedmr(ecm_id, &dlxblocks[pidindex],
                                                 vehiclecommtype[ecm_index],
                                                 vehiclecommlevel[ecm_index]);
                break;
            case PidTypeAnalog:
                // Assume it's valid
                status = S_SUCCESS;
                analogcount++;
                break;
            case PidTypeOSC:
                status = obd2datalog_validate_oscpid(ecm_id,&dlxblocks[pidindex],
                                                     vehiclecommtype[ecm_index],
                                                     vehiclecommlevel[ecm_index]);
                break;
            default:        
                //unknown pid type, ignore (invalid) for now
                break;
        }
        
        if (status == S_SUCCESS)
        {
            validpidlist[count++] = pidindex;
        }
        else if(status == S_TIMEOUT)
        {
            break;
        }

        if (reportprogressfunc)
        {
            percentage = (u8)(pidindex * 100 / dlxheader->count);
            reportprogressfunc(percentage);
        }
    }//for(pidindex=0;pidindex<...

    *validpidcount = count;
    
    if((count-analogcount) > 0)
    {
        return S_SUCCESS; 
    }
    else
    {
        return S_FAIL; // No valid pids found
    }
}

//------------------------------------------------------------------------------
// Set skip to invalid signals
// Inputs:  u8  *validpidlist (TODOQ: convert to u16* later)
//          u16 validpidcount
// Return:  u8  status
// Engineer: Quyen Leba
//TODOQ: change validpidlist to u16[]
//------------------------------------------------------------------------------
u8 obd2data_set_skip_invalid_signals(u8 *validpidlist, u16 validpidcount)
{
    u32 i,j;
    bool found;

    if (dataloginfo == NULL)
    {
        log_push_error_point(0x495B);
        return S_FAIL;
    }

    for(i=0;i<dataloginfo->datalogsignalcount;i++)
    {
        found = FALSE;
        for(j=0;j<validpidcount;j++)
        {
            if (i == validpidlist[j])
            {
                found = TRUE;
                break;
            }
        }
        if (found == FALSE)
        {
            dataloginfo->datalogsignals[i].Control |= DATALOG_CONTROL_SKIP;
        }
    }
    return S_SUCCESS;
}

//------------------------------------------------------------------------------
// Generate a dlx with only valid pids from a scan
// Inputs:  u8  sourcedlxfilename (must be in user folder)
//          u8  *validpidlist
//          u16 *validpidcount
// Return:  u8  status
// Engineer: Quyen Leba
//------------------------------------------------------------------------------
u8 obd2datalog_generatevalidpidlistfile(u8 *sourcedlxfilename,
                                        u8 *validpidlist, u16 validpidcount)
{
    DlxHeader dlxheader;
    DlxBlock dlxblock;
    F_FILE  *source_fptr;
    F_FILE  *dest_fptr;
    u16 totalsourcepidcount;
    u16 totalvalidpidfound;
    u32 bytecount;
    u16 pidindex;
    u32 crc32e_calc;
    u32 i;
    u8  status;

    status = obd2datalog_validate_dlxfile(sourcedlxfilename);
    if (status != S_SUCCESS)
    {
        log_push_error_point(0x493E);
        return status;
    }

    totalvalidpidfound = 0;
    status = S_FAIL;
    source_fptr = NULL;
    dest_fptr = NULL;
    source_fptr = genfs_user_openfile(sourcedlxfilename,"r");
    if(!source_fptr)
    {
        log_push_error_point(0x491D);
        status = S_OPENFILE;
        goto obd2datalog_generatevalidpidlistfile_done;
    }
    dest_fptr = genfs_user_openfile(DEFAULT_DATALOG_VALID_PID_FILENAME,"w");
    if(!dest_fptr)
    {
        log_push_error_point(0x491E);
        status = S_OPENFILE;
        goto obd2datalog_generatevalidpidlistfile_done;
    }
    
    // Read in DLX header info
    bytecount = fread((char*)&dlxheader, 1, sizeof(DlxHeader), source_fptr);
    if(bytecount != sizeof(DlxHeader))
    {
        log_push_error_point(0x491F);
        status = S_READFILE;
        goto obd2datalog_generatevalidpidlistfile_done;
    }
#if DATALOG_USE_ENCRYPTION
    crypto_blowfish_decryptblock_external_key((u8*)&dlxheader,sizeof(DlxHeader));
#endif

    if (dlxheader.count == 0 || dlxheader.count < validpidcount)
    {
        log_push_error_point(0x4920);
        status = S_BADCONTENT;
        goto obd2datalog_generatevalidpidlistfile_done;
    }
    totalsourcepidcount = dlxheader.count;
    dlxheader.count = validpidcount;
    dlxheader.content_crc32e = 0;
    memset(&dlxheader.features,0,sizeof(dlxheader.features));
    memset(dlxheader.reserved2,0,sizeof(dlxheader.reserved2));
    status = obd2datalog_validate_dlxheader(&dlxheader,&crc32e_calc);
    if (status != S_SUCCESS && status != S_CRC32E)
    {
        log_push_error_point(0x4932);
        status = S_FAIL;
        goto obd2datalog_generatevalidpidlistfile_done;
    }
    else
    {
        dlxheader.header_crc32e = crc32e_calc;
        status = S_SUCCESS;
    }
#if DATALOG_USE_ENCRYPTION
    crypto_blowfish_encryptblock_external_key((u8*)&dlxheader,sizeof(DlxHeader));
#endif
    bytecount = fwrite((char*)&dlxheader, 1, sizeof(DlxHeader), dest_fptr);
    if(bytecount != sizeof(DlxHeader))
    {
        log_push_error_point(0x4921);
        status = S_WRITEFILE;
        goto obd2datalog_generatevalidpidlistfile_done;
    }

    pidindex = 0;
    while(!(feof(source_fptr)))
    {
        if (pidindex >= totalsourcepidcount)
        {
            break;
        }
        bytecount = fread((char*)&dlxblock,1,sizeof(DlxBlock),source_fptr);
        if (bytecount != sizeof(DlxBlock))
        {
            log_push_error_point(0x4922);
            status = S_READFILE;
            goto obd2datalog_generatevalidpidlistfile_done;
        }
#if DATALOG_USE_ENCRYPTION
        crypto_blowfish_decryptblock_external_key((u8*)&dlxblock,sizeof(DlxBlock));
#endif
        
        for(i=0;i<validpidcount;i++)
        {
            if (validpidlist[i] == pidindex)
            {
#if DATALOG_USE_ENCRYPTION
                crypto_blowfish_encryptblock_external_key((u8*)&dlxblock,sizeof(DlxBlock));
#endif
                bytecount = fwrite((char*)&dlxblock,1,sizeof(DlxBlock),dest_fptr);
                if (bytecount != sizeof(DlxBlock))
                {
                    log_push_error_point(0x4923);
                    status = S_WRITEFILE;
                    goto obd2datalog_generatevalidpidlistfile_done;
                }
                totalvalidpidfound++;
                break;
            }
        }
        
        pidindex++;
    }//while(!...
    if (totalvalidpidfound != validpidcount)
    {
        log_push_error_point(0x4924);
        status = S_BADCONTENT;
        goto obd2datalog_generatevalidpidlistfile_done;
    }
    status = S_SUCCESS;
    
obd2datalog_generatevalidpidlistfile_done:
    if (source_fptr)
    {
        genfs_closefile(source_fptr);
    }
    if (dest_fptr)
    {
        genfs_closefile(dest_fptr);
    }
    return status;
}

//------------------------------------------------------------------------------
// Return:  u8  status
// Engineer: Quyen Leba
// Note: dataloginfo->dlxheader and dataloginfo->dlxblocks must already be
// setup by obd2datalog_parsedatalogfile(...)
//------------------------------------------------------------------------------
u8 obd2datalog_resetdatalogsignals()
{
    u16 i;

    if (dataloginfo == NULL)
    {
        log_push_error_point(0x4943);
        return S_BADCONTENT;
    }
    else if (dataloginfo->dlxheader == NULL || dataloginfo->dlxblocks == NULL)
    {
        log_push_error_point(0x4944);
        return S_BADCONTENT;
    }
    else if (dataloginfo->dlxheader->count > MAX_DATALOG_DLXBLOCK_COUNT)
    {
        log_push_error_point(0x4945);
        return S_BADCONTENT;
    }
    
    for(i=0;i<dataloginfo->dlxheader->count;i++)
    {
        // *** Address
        dataloginfo->datalogsignals[i].Address = dataloginfo->dlxblocks[i].pidAddr;
        
        // *** EcmIndex
        //we only datalog 2 ECMs for now (Feb122010)
        if (dataloginfo->dlxblocks[i].pidModuleType == 0)
        {
            dataloginfo->datalogsignals[i].EcmIndex = 0;
        }
        else
        {
            dataloginfo->datalogsignals[i].EcmIndex = 1;
        }
        
        // *** PidType
        dataloginfo->datalogsignals[i].PidType = dataloginfo->dlxblocks[i].pidType;
        
        // *** StructType
        if (dataloginfo->dlxblocks[i].pidType == PidTypeOSC)
        {
            dataloginfo->datalogsignals[i].StructType = StructTypeOSC;
        }
        else if (dataloginfo->dlxblocks[i].pidBitMapped == TRUE)
        {
            dataloginfo->datalogsignals[i].StructType = StructTypeBitmapped;
        }
        else if (dataloginfo->dlxblocks[i].pidBP != 0)
        {
            dataloginfo->datalogsignals[i].StructType = StructTypeBitPtr;
        }
        else
        {
            dataloginfo->datalogsignals[i].StructType = StructTypeMultiplierOffset;
        }
        
        // *** Size (Pid Size)
        dataloginfo->datalogsignals[i].Size = dataloginfo->dlxblocks[i].pidSize;
        
        // *** DLXIndex
        dataloginfo->datalogsignals[i].DlxIndex = i;
        
        // *** Flags
        //TODOQ: check MIN MAX version
        if (dataloginfo->dlxheader->dlxVersion >= 0x0003)
        {
            dataloginfo->datalogsignals[i].Flags = dataloginfo->dlxblocks[i].pidFlags;
        }
        else if (dataloginfo->dlxheader->dlxVersion == 0x0002)
        {
            //dataloginfo->datalogsignals[i].Flags = (1<<4) | (1<<3);  //signed, BIGendian, float
            dataloginfo->datalogsignals[i].Flags = DATALOG_PIDFLAGS_BIGENDIAN | DATALOG_PIDFLAGS_DATATYPE_FLOAT;    //unsigned, BIGendian, float
        }
        else
        {
            //bug in DLX, we missed it in the structure
            dataloginfo->datalogsignals[i].Flags = DATALOG_PIDFLAGS_BIGENDIAN | DATALOG_PIDFLAGS_DATATYPE_FLOAT;    //unsigned, BIGendian, float
        }
        
        // *** Control
        dataloginfo->datalogsignals[i].Control = DATALOG_CONTROL_NEWDATA_SET;
        
        // *** packetStartPos
        dataloginfo->datalogsignals[i].packetStartPos = dataloginfo->dlxblocks[i].packetStartPos;
        
        // *** Data
        dataloginfo->datalogsignals[i].Data = 0;
        
        // *** Value
        dataloginfo->datalogsignals[i].Value = 0;
        
        // *** Struct->
        switch (dataloginfo->datalogsignals[i].StructType)
        {
            case StructTypeMultiplierOffset:
                dataloginfo->datalogsignals[i].SignalType.MultiplierOffset.PacketNumber = DATALOG_INVALID_PACKETID;
                dataloginfo->datalogsignals[i].SignalType.MultiplierOffset.Position = 0;
                dataloginfo->datalogsignals[i].SignalType.MultiplierOffset.Multiplier = (float)dataloginfo->dlxblocks[i].pidMultiplier;
                dataloginfo->datalogsignals[i].SignalType.MultiplierOffset.Offset = (float)dataloginfo->dlxblocks[i].pidOffset;
                break;
            case StructTypeBitPtr:
                dataloginfo->datalogsignals[i].SignalType.BitPtr.PacketNumber = DATALOG_INVALID_PACKETID;
                dataloginfo->datalogsignals[i].SignalType.BitPtr.Position = 0;
                dataloginfo->datalogsignals[i].SignalType.MultiplierOffset.Multiplier = (float)dataloginfo->dlxblocks[i].pidMultiplier;
                dataloginfo->datalogsignals[i].SignalType.MultiplierOffset.Offset = (float)dataloginfo->dlxblocks[i].pidOffset;
                dataloginfo->datalogsignals[i].SignalType.BitPtr.Bp = dataloginfo->dlxblocks[i].pidBP;
                break;
            case StructTypeBitmapped:
                dataloginfo->datalogsignals[i].SignalType.Bitmapped.PacketNumber = DATALOG_INVALID_PACKETID;
                dataloginfo->datalogsignals[i].SignalType.Bitmapped.Position = 0;
                dataloginfo->datalogsignals[i].SignalType.Bitmapped.Bit = dataloginfo->dlxblocks[i].pidBit;
                break;
            case StructTypeOSC:
                DatalogControl_SetOSC(dataloginfo->datalogsignals[i].Control,OSC_Default);
                dataloginfo->datalogsignals[i].SignalType.OSC.PacketNumber = DATALOG_INVALID_PACKETID;
                dataloginfo->datalogsignals[i].SignalType.OSC.Position = 0;
                dataloginfo->datalogsignals[i].SignalType.OSC.Multiplier = (float)dataloginfo->dlxblocks[i].pidMultiplier;
                dataloginfo->datalogsignals[i].SignalType.OSC.Offset = (float)dataloginfo->dlxblocks[i].pidOffset;
                dataloginfo->datalogsignals[i].SignalType.OSC.DataBytePosition = dataloginfo->dlxblocks[i].oscDataBytePos;
                dataloginfo->datalogsignals[i].SignalType.OSC.ControlBytePosition = dataloginfo->dlxblocks[i].oscControlBytePos;
                dataloginfo->datalogsignals[i].SignalType.OSC.ControlByteMask = dataloginfo->dlxblocks[i].oscControlByteMask;
                dataloginfo->datalogsignals[i].SignalType.OSC.UiPidGroup = dataloginfo->dlxblocks[i].oscUiPidGroup;
                break;
            default:
                return S_BADCONTENT;
        }
    }//for(i=0;i<dataloginfo...
    dataloginfo->datalogsignalcount = dataloginfo->dlxheader->count;

    return S_SUCCESS;
}

//------------------------------------------------------------------------------
// Parse a datalog file into datalogsignals in dataloginfo
// datalogsignals is then used in datalog session
// Input:   const u8 *datalogfilename (valid pid datalog file)
// Return:  u8  status
// Engineer: Quyen Leba
//------------------------------------------------------------------------------
u8 obd2datalog_parsedatalogfile(const u8 *datalogfilename)
{
    u8  status;
    
    if (dataloginfo == NULL)
    {
        log_push_error_point(0x4946);
        return S_BADCONTENT;
    }
    
    if (dataloginfo->dlxheader != NULL)
    {
        __free(dataloginfo->dlxheader);
        dataloginfo->dlxheader = NULL;
    }
    if (dataloginfo->dlxblocks != NULL)
    {
        __free(dataloginfo->dlxblocks);
        dataloginfo->dlxblocks = NULL;
    }
    
    dataloginfo->dlxheader = __malloc(sizeof(DlxHeader));
    if (dataloginfo->dlxheader == NULL)
    {
        log_push_error_point(0x4925);
        status = S_MALLOC;
        goto obd2datalog_parsedatalogfile_done;
    }
    dataloginfo->dlxblocks = __malloc(MAX_DATALOG_DLXBLOCK_COUNT*sizeof(DlxBlock));
    if (dataloginfo->dlxblocks == NULL)
    {
        log_push_error_point(0x4926);
        status = S_MALLOC;
        goto obd2datalog_parsedatalogfile_done;
    }
    
    status = obd2datalog_getdatalogfileinfo(datalogfilename,
                                            dataloginfo->dlxheader,
                                            dataloginfo->dlxblocks);
    if (status != S_SUCCESS)
    {
        log_push_error_point(0x4927);
        goto obd2datalog_parsedatalogfile_done;
    }
    
    if (dataloginfo->dlxheader->count > MAX_DATALOG_DLXBLOCK_COUNT)
    {
        log_push_error_point(0x4928);
        status = S_BADCONTENT;
        goto obd2datalog_parsedatalogfile_done;
    }
    
    status = obd2datalog_resetdatalogsignals();
//    if (status != S_SUCCESS)
//    {
//        log_push_error_point(0x4929);
//        goto obd2datalog_parsedatalogfile_done;
//    }
    
obd2datalog_parsedatalogfile_done:
    if (status != S_SUCCESS)
    {
        if (dataloginfo->dlxheader != NULL)
        {
            __free(dataloginfo->dlxheader);
            dataloginfo->dlxheader = NULL;
        }
        if (dataloginfo->dlxblocks != NULL)
        {
            __free(dataloginfo->dlxblocks);
            dataloginfo->dlxblocks = NULL;
        }
    }
    return status;
}

//------------------------------------------------------------------------------
// Trim Datalog Signals
// Used before setup packets to remove invalid signals; i.e. only valid signals
// are allowed in datalog session
// Input:   u8  *validpidlist
//          u16 validpidcount
// Return:  u8  status
// Engineer: Quyen Leba
//------------------------------------------------------------------------------
u8 obd2datalog_trimdatalogsignals(u8 *validpidlist, u16 validpidcount)
{
    u16 i;
    u16 currentsignalindex;

    if (dataloginfo == NULL)
    {
        log_push_error_point(0x4947);
        return S_BADCONTENT;
    }
    if (validpidcount > dataloginfo->datalogsignalcount)
    {
        log_push_error_point(0x492A);
        return S_INPUT;
    }

    currentsignalindex = 0;
    for(i=0;i<validpidcount;i++)
    {
        if (currentsignalindex == validpidlist[i])
        {
            currentsignalindex++;
        }
        else
        {
            memcpy((char*)&dataloginfo->datalogsignals[currentsignalindex],
                   (char*)&dataloginfo->datalogsignals[validpidlist[i]],
                   sizeof(DatalogSignal));
            currentsignalindex++;
        }
    }
    dataloginfo->datalogsignalcount = currentsignalindex;
    return S_SUCCESS;
}

/**
 *  @brief Setup datalog session
 *
 *  @param [in]     commtype            Vehicle comm type
 *  @param [in]     commlevel           Vehicle comm level
 *
 *  @return Status
 */
u8 obd2datalog_setupdatalogsession(VehicleCommType *commtype, VehicleCommLevel *commlevel)
{
    u8 status;
    
    obd2_open();

    switch (gObd2info.oemtype)
    {
#ifdef __DCX_MANUF__ 
    case OemType_DCX:
        status = obd2datalog_dcx_setupdatalogsession(commtype, commlevel);
        break;
#endif
#ifdef __FORD_MANUF__ 
    case OemType_FORD:
        status = obd2datalog_ford_setupdatalogsession(commtype, commlevel);
        break;
#endif
#ifdef __GM_MANUF__ 
    case OemType_GM:
        status = obd2datalog_gm_setupdatalogsession(commtype, commlevel);
        break;
#endif
    default:
        if (SETTINGS_IsDemoMode())
        {
            dataloginfo->getdatafunc = obd2datalog_demo_getdata;
            dataloginfo->getanalogdatafunc = obd2datalog_getdata_analog;
            dataloginfo->sendtesterpresent = obd2datalog_demo_sendtesterpresent;
            dataloginfo->scheduled_tasks |= DATALOG_TASK_GET_DATA
                                         |  DATALOG_TASK_ANALOG;
            status = S_SUCCESS;
        }
        else if (commtype[0] != CommType_Unknown || commtype[1] != CommType_Unknown)
        {
            /* Allow OEMs to datalog using generic PIDs */
            status = obd2_datalog_evaluatesignalsetup(commlevel);
            if (status == S_SUCCESS || status == S_NOTFIT)
            {
                gDatalogmode.packetmode = SingleRate;
                dataloginfo->getanalogdatafunc = obd2datalog_getdata_analog;
                dataloginfo->getdatafunc = obd2datalog_getdata_singlerate;
                dataloginfo->scheduled_tasks |= DATALOG_TASK_GET_DATA_SINGLE_RATE
                                             |  DATALOG_TASK_ANALOG;
            }
        }
        break;
    }
    return status;
}

/**
 *  @brief Start datalog session
 *
 *  @param [in]     commtype            Vehicle comm type
 *  @param [in]     commlevel           Vehicle comm level
 *
 *  @return Status
 */
u8 obd2datalog_startdatalogsession(VehicleCommType *commtype, VehicleCommLevel *commlevel)
{
    u8 status;
    
    obd2_open();
   
    switch (gObd2info.oemtype)
    {
#ifdef __DCX_MANUF__ 
    case OemType_DCX:
        status = obd2datalog_dcx_startdatalogsession(commtype, commlevel);
        break;
#endif
#ifdef __FORD_MANUF__ 
    case OemType_FORD:
        status = obd2datalog_ford_startdatalogsession(commtype, commlevel);
        break;
#endif
#ifdef __GM_MANUF__ 
    case OemType_GM:
        status = obd2datalog_gm_startdatalogsession(commtype, commlevel);
        break;
#endif
    default:
        /* Generic PIDs do not require starting a session */
        status = S_SUCCESS;
        break;
    }
    return status;
}

/**
 *  @brief Stop datalog session
 *
 *  @param [in]     commtype            Vehicle comm type
 *  @param [in]     commlevel           Vehicle comm level
 *
 *  @return Status
 */
u8 obd2datalog_stopdatalogsession(VehicleCommType *commtype, VehicleCommLevel *commlevel)
{
    u8 status;

    obd2_open();

    switch (gObd2info.oemtype)
    {
#ifdef __DCX_MANUF__ 
    case OemType_DCX:
        status = obd2datalog_dcx_stopdatalogsession(commtype, commlevel);
        break;
#endif
#ifdef __FORD_MANUF__ 
    case OemType_FORD:
        status = obd2datalog_ford_stopdatalogsession(commtype, commlevel);
        break;
#endif
#ifdef __GM_MANUF__ 
    case OemType_GM:
        status = obd2datalog_gm_stopdatalogsession(commtype, commlevel);
        break;
#endif
    default:
        /* Generic PIDs do not require stopping a session */
        status = S_SUCCESS;
        break;
    }
    return status;
}

/**
 * @brief   Unset all OSC Signals
 *
 * @param   [in] VehicleCommType
 * @param   [in] VehicleCommLevel
 *
 * @retval  u8 status
 *
 * @author  Tristen Pierson
 *
 * @details This function resets all OSC items to their defaults and
 *          returns control to the ECU.
 */
u8 obd2datalog_unset_all_osc(VehicleCommType *commtype,
                             VehicleCommLevel *commlevel)
{
    u8 status;

    obd2_open();

    switch (gObd2info.oemtype)
    {
#ifdef __FORD_MANUF__ 
    case OemType_FORD:
        status = obd2datalog_ford_unset_all_osc(commtype, commlevel);
        break;
#endif
#ifdef __GM_MANUF__ 
    case OemType_GM:
        status = obd2datalog_gm_unset_all_osc(commtype, commlevel);
        break;
#endif
    default:
        status = S_FAIL;
        break;
    }
    return status;
}

//------------------------------------------------------------------------------
// Start datalog stream
// Inputs:  VehicleCommType vehiclecommtype[ECM_MAX_COUNT]
//          VehicleCommLevel vehiclecommlevel[ECM_MAX_COUNT]
// Return:  u8  status
// Engineer: Quyen Leba
//------------------------------------------------------------------------------
u8 obd2datalog_datastream_start(VehicleCommType *vehiclecommtype,
                                VehicleCommLevel *vehiclecommlevel)
{
    u8  status;

    status = obd2datalog_setupdatalogsession(vehiclecommtype,vehiclecommlevel);
    if (status != S_SUCCESS)
    {
        log_push_error_point(0x4948);
        return status;
    }

    if (SETTINGS_IsDemoMode())
    {
        return S_SUCCESS;
    }

    status = obd2datalog_startdatalogsession(vehiclecommtype,vehiclecommlevel);
    if (status != S_SUCCESS)
    {
        log_push_error_point(0x4949);
    }
    return status;
}

//------------------------------------------------------------------------------
// Stop datalog stream
// Inputs:  VehicleCommType vehiclecommtype[ECM_MAX_COUNT]
//          VehicleCommLevel vehiclecommlevel[ECM_MAX_COUNT]
// Return:  u8  status
// Engineer: Quyen Leba
//------------------------------------------------------------------------------
u8 obd2datalog_datastream_stop(VehicleCommType *vehiclecommtype,
                               VehicleCommLevel *vehiclecommlevel)
{
    u8  status;

    if (SETTINGS_IsDemoMode())
    {
        return S_SUCCESS;
    }

    status = obd2datalog_stopdatalogsession(vehiclecommtype,vehiclecommlevel);
    if (status != S_SUCCESS)
    {
        log_push_error_point(0x494A);
    }
    return status;
}

//------------------------------------------------------------------------------
// Start up a datalog
// Inputs:  VehicleCommType vehiclecommtype[ECM_MAX_COUNT]
//          VehicleCommLevel vehiclecommlevel[ECM_MAX_COUNT]
// Return:  u8  status
// Engineer: Quyen Leba
//------------------------------------------------------------------------------
u8 obd2datalog_startup(VehicleCommType *vehiclecommtype,
                       VehicleCommLevel *vehiclecommlevel)
{
    u8  validpidlist[MAX_DATALOG_DLXBLOCK_COUNT];
    u16 validpidcount;
    u8  status;

    if (dataloginfo == NULL)
    {
        log_push_error_point(0x494B);
        return S_BADCONTENT;
    }
    
    status = obd2datalog_resetdatalogsignals();

    if (status != S_SUCCESS)
    {
        log_push_error_point(0x494C);
        goto obd2datalog_startup_done;
    }
    
    delays(100,'m');
    status = obd2datalog_getvalidpidlist
            (dataloginfo->dlxheader,dataloginfo->dlxblocks,
             vehiclecommtype,vehiclecommlevel,
             validpidlist,&validpidcount,NULL);
    if (status != S_SUCCESS)
    {
        log_push_error_point(0x494D);
        goto obd2datalog_startup_done;
    }
    
    status = obd2datalog_trimdatalogsignals(validpidlist,validpidcount);
    if (status != S_SUCCESS)
    {
        log_push_error_point(0x494E);
        goto obd2datalog_startup_done;
    }
    
    status = obd2datalog_datastream_start(vehiclecommtype,vehiclecommlevel);
    if (status != S_SUCCESS)
    {
        log_push_error_point(0x494F);
        goto obd2datalog_startup_done;
    }
    
    // Free dlxblocks here; malloc in obd2datalog_parsedatalogfile()
    if (dataloginfo->dlxheader != NULL)
    {
        __free(dataloginfo->dlxheader);
        dataloginfo->dlxheader = NULL;
    }
    if (dataloginfo->dlxblocks != NULL)
    {
        __free(dataloginfo->dlxblocks);
        dataloginfo->dlxblocks = NULL;
    }

obd2datalog_startup_done:
    return status;
}

//------------------------------------------------------------------------------
// Get analog data (accumulative average to form analog value)
// Return:  u8  status
// Engineer: Quyen Leba
//------------------------------------------------------------------------------
u8 obd2datalog_getdata_analog_long_average_sampling()
{
#define GETDATA_ANALOG_LONGTERM_SAMPLECOUNT     50
    u32 i;
    u32 signalindex;
    float fvalue;
    float favg;
    u8  status;

    //assume: dataloginfo->analogsignalcount <= dataloginfo->analogsignalmaxavailable
    for(i=0;i<dataloginfo->analogsignalcount;i++)
    {
        signalindex = dataloginfo->analogsignalindex[i];
        if ((signalindex < MAX_DATALOG_DLXBLOCK_COUNT) &&
            (dataloginfo->datalogsignals[signalindex].PidType == PidTypeAnalog))
        {
            status = adc_read
                ((ADC_CHANNEL)dataloginfo->datalogsignals[signalindex].Address,
                 &fvalue);
            if (status == S_SUCCESS)
            {
                favg = dataloginfo->datalogsignals[signalindex].Value * dataloginfo->analogsignalsamplecount[i];
                dataloginfo->analogsignalsamplecount[i]++;
                if (dataloginfo->analogsignalsamplecount[i] > GETDATA_ANALOG_LONGTERM_SAMPLECOUNT)
                {
                    dataloginfo->analogsignalsamplecount[i] = GETDATA_ANALOG_LONGTERM_SAMPLECOUNT;
                    favg += fvalue;
                    favg /= (float)(GETDATA_ANALOG_LONGTERM_SAMPLECOUNT+1);
                }
                else
                {
                    favg += fvalue;
                    favg /= (float)dataloginfo->analogsignalsamplecount[i];
                }

                if (dataloginfo->datalogsignals[signalindex].Value != favg)
                {
                    //dataloginfo->datalogsignals[signalindex].Data = fvalue;
                    dataloginfo->datalogsignals[signalindex].Value = favg;
                    dataloginfo->datalogsignals[signalindex].Control |= DATALOG_CONTROL_NEWDATA_SET;
                }
            }
        }
    }//for(i=...
    
    return S_SUCCESS;
}

//------------------------------------------------------------------------------
// Get analog data (average small set of samples to form analog value & reject
// if min/max of the set is outside of +/-5% of average value)
// Return:  u8  status
// Engineer: Quyen Leba
//------------------------------------------------------------------------------
u8 obd2datalog_getdata_analog_short_average_sampling()
{
    static u16 currentindex = 0;
    u32 i;
    u32 signalindex;
#define GETDATA_ANALOG_SAMPLECOUNT      10
    float sample;
    float fvalue_avg;
    float fvalue_min;
    float fvalue_max;
    u8  status;

    fvalue_min = 99999999;
    fvalue_max = -99999999;
    status = S_SUCCESS;     //must be zero
    signalindex = dataloginfo->analogsignalindex[currentindex];
    if ((signalindex < MAX_DATALOG_DLXBLOCK_COUNT) &&
        (dataloginfo->datalogsignals[signalindex].PidType == PidTypeAnalog))
    {
        for(i=0;i<GETDATA_ANALOG_SAMPLECOUNT;i++)
        {
            status |= adc_read((ADC_CHANNEL)dataloginfo->datalogsignals[signalindex].Address,
                               &sample);
            delays(50,'u');
            if (status == S_SUCCESS)
            {
                if (sample > fvalue_max)
                {
                    fvalue_max = sample;
                }
                if (sample < fvalue_min)
                {
                    fvalue_min = sample;
                }
                fvalue_avg += sample;
            }
            else
            {
                goto obd2datalog_getdata_analog_done;
            }
        }
        fvalue_avg /= GETDATA_ANALOG_SAMPLECOUNT;
        if (((fvalue_min >= (0.95*fvalue_avg) && fvalue_max <= (1.05*fvalue_avg)) ||
             (fvalue_avg < 0.2)) && fvalue_avg >= 0.0 &&
            fvalue_avg != dataloginfo->datalogsignals[signalindex].Value)
        {
            //dataloginfo->datalogsignals[signalindex].Data = fvalue;
            dataloginfo->datalogsignals[signalindex].Value = fvalue_avg;
            dataloginfo->datalogsignals[signalindex].Control |= DATALOG_CONTROL_NEWDATA_SET;
        }
    }

    //prepare for next analog input
    currentindex++;
    if (currentindex >= dataloginfo->analogsignalcount)
    {
        currentindex = 0;
    }

obd2datalog_getdata_analog_done:
    return S_SUCCESS;
}

//------------------------------------------------------------------------------
// Get analog data 
// 
// Return:  u8  status
// Engineer: Quyen Leba, Patrick Downs
//------------------------------------------------------------------------------
u8 obd2datalog_getdata_analog_no_average_sampling()
{
    static u16 currentindex = 0;
    u32 i;
    u32 signalindex;
    float sample;
    u8  status;

    status = S_SUCCESS;
    signalindex = dataloginfo->analogsignalindex[currentindex];
    if ((signalindex < MAX_DATALOG_DLXBLOCK_COUNT) &&
        (dataloginfo->datalogsignals[signalindex].PidType == PidTypeAnalog))
    {
        for(i=0;i<GETDATA_ANALOG_SAMPLECOUNT;i++)
        {
            status = adc_read((ADC_CHANNEL)dataloginfo->datalogsignals[signalindex].Address,
                               &sample);
            if(status == S_SUCCESS)
            {   
                dataloginfo->datalogsignals[signalindex].Value = sample;
                dataloginfo->datalogsignals[signalindex].Control |= DATALOG_CONTROL_NEWDATA_SET;                
            }
            else
            {
                goto obd2datalog_getdata_analog_done;
            }
        }
    }

    //prepare for next analog input
    currentindex++;
    if (currentindex >= dataloginfo->analogsignalcount)
    {
        currentindex = 0;
    }

obd2datalog_getdata_analog_done:
    return S_SUCCESS;
}

//------------------------------------------------------------------------------
// Form a data report to be sent
// Outputs: u8  *report
//              ([4:timestamp][2:signalcount]
//               repeat of [2:dlx_index][4:float value] for # of signalcount)
//              timestamp is in miliseconds
//          u16 *reportlength (4+2+signalcount*6)
// Return:  u8  status
// Engineer: Quyen Leba
//------------------------------------------------------------------------------
u8 obd2datalog_getdatareport(u8 *report, u16 *reportlength)
{
    u16 i;
    u16 index;
    u16 reportcount;
    u32 timestamp;

    *reportlength = 0;
    reportcount = 0;
    timestamp = (u32)rtc_getvalue();

    index = 6;
    for(i=0;i<dataloginfo->datalogsignalcount;i++)
    {
        if (dataloginfo->datalogsignals[i].Control & DATALOG_CONTROL_NEWDATA_REPORT_SET)
        {
            dataloginfo->datalogsignals[i].Control &= DATALOG_CONTROL_NEWDATA_REPORT_CLR;

            memcpy((char*)&report[index],
                   (char*)&dataloginfo->datalogsignals[i].DlxIndex,2);
            index += 2;
            memcpy((char*)&report[index],
                   (char*)&dataloginfo->datalogsignals[i].Value,4);
            index += 4;
            reportcount++;
        }
    }
    if (index > 6)
    {
        // Timestamp
        memcpy((char*)report,&timestamp,4);
        // Pid Count
        memcpy((char*)&report[4],&reportcount,2);
        // Recording Status - This includes record state and record space percentage used (percentageusedspace)
        memcpy((char*)&report[index],&dataloginfo->record.reportstatus,2);
        *reportlength = index+2;
        return S_SUCCESS;
    }
    return S_FAIL;
}

//------------------------------------------------------------------------------
// Validate DlxHeader
// Input:   DlxHeader *dlxheader
// Outputs: u32 *crc32e (NULL allowed)
// Return:  u8  status
// Engineer: Quyen Leba
//------------------------------------------------------------------------------
u8 obd2datalog_validate_dlxheader(DlxHeader *dlxheader, u32 *crc32e)
{
    u32 crc32e_calc;
    u32 crc32e_cmp;
    u8  status;

    crc32e_calc = 0;
    if (!dlxheader)
    {
        log_push_error_point(0x4933);
        status = S_INPUT;
        goto obd2datalog_validate_dlxheader_done;
    }
    
    crc32e_reset();
    crc32e_cmp = dlxheader->header_crc32e;
    dlxheader->header_crc32e = 0xFFFFFFFF;
    crc32e_calc = crc32e_calculateblock(0xFFFFFFFF,
                                        (u32*)dlxheader,sizeof(DlxHeader)/4);
    dlxheader->header_crc32e = crc32e_cmp;
    if (crc32e_cmp != 0 && crc32e_calc != crc32e_cmp)
    {
        //log_push_error_point(0x4934);
        crc32e_calc = 0;
        status = S_CRC32E;
        goto obd2datalog_validate_dlxheader_done;
    }
    status = S_SUCCESS;
    
obd2datalog_validate_dlxheader_done:
    if (crc32e)
    {
        *crc32e = crc32e_calc;
    }
    return status;
}

//------------------------------------------------------------------------------
// Validate dlx file
// Input:   const u8 *filename (must be in user folder)
// Return:  u8  status
// Engineer: Quyen Leba
//------------------------------------------------------------------------------
u8 obd2datalog_validate_dlxfile(const u8 *filename)
{
    DlxHeader dlxheader;
    F_FILE *fptr;
    u8  buffer[256];
    u32 bytecount;
    u32 crc32e_calc;
    u8  status;

    fptr = NULL;
    status = S_SUCCESS;

    fptr = genfs_user_openfile((u8*)filename,"r");
    if (fptr == NULL)
    {
        log_push_error_point(0x4936);
        status = S_OPENFILE;
        goto obd2datalog_validate_dlxfile_done;
    }
    bytecount = fread((char*)&dlxheader,1,sizeof(DlxHeader),fptr);
    if (bytecount != sizeof(DlxHeader))
    {
        log_push_error_point(0x4937);
        status = S_READFILE;
        goto obd2datalog_validate_dlxfile_done;
    }
#if DATALOG_USE_ENCRYPTION
    crypto_blowfish_decryptblock_external_key((u8*)&dlxheader,sizeof(DlxHeader));
#endif
    status = obd2datalog_validate_dlxheader(&dlxheader,NULL);
    if (status != S_SUCCESS)
    {
        log_push_error_point(0x4938);
        status = S_READFILE;
        goto obd2datalog_validate_dlxfile_done;
    }
    crc32e_reset();
    crc32e_calc = 0xFFFFFFFF;
    while(!feof(fptr))
    {
        bytecount = fread((char*)buffer,1,sizeof(buffer),fptr);
        crc32e_calc = crc32e_calculateblock(crc32e_calc,(u32*)buffer,bytecount/4);    
    }
    if (dlxheader.content_crc32e != 0 && dlxheader.content_crc32e != crc32e_calc)
    {
        log_push_error_point(0x4939);
        status = S_CRC32E;
        goto obd2datalog_validate_dlxfile_done;
    }
    
obd2datalog_validate_dlxfile_done:
    if (fptr)
    {
        genfs_closefile(fptr);
    }
    return status;
}

//------------------------------------------------------------------------------
// Merge 2 dlx files
// Inputs:  const u8 *newdlxfilename
//          const u8 *dlxfilename1 (max 50 items in this file)
//          const u8 *dlxfilename2
// Return:  u8  status
// Engineer: Quyen Leba
//------------------------------------------------------------------------------
u8 obd2datalog_merge_dlxfile(const u8 *newdlxfilename,
                             const u8 *dlxfilename1, const u8 *dlxfilename2)
{
    F_FILE *fptrR;
    F_FILE *fptrW;
    DlxHeader dlxheader1;
    DlxHeader dlxheader2;
    DlxBlock dlxblock;
#define MERGE_DLXFILE1_DLXBLOCK_CRC32E_MAX_COUNT    MAX_DATALOG_DLXBLOCK_COUNT
    u32 dlxblock_crc32e[MERGE_DLXFILE1_DLXBLOCK_CRC32E_MAX_COUNT];
    u32 calc_crc32e;
    u32 bytecount;
    u32 newfile_crc32e;
    u16 newfile_count;
    u16 i,j;
    u8  status;

    fptrR = NULL;
    fptrW = NULL;
    status = obd2datalog_validate_dlxfile(dlxfilename1);
    if (status != S_SUCCESS)
    {
        log_push_error_point(0x4960);
        goto obd2datalog_merge_dlxfile_done;
    }
    status = obd2datalog_validate_dlxfile(dlxfilename2);
    if (status != S_SUCCESS)
    {
        log_push_error_point(0x4961);
        goto obd2datalog_merge_dlxfile_done;
    }

    fptrR = genfs_general_openfile(dlxfilename1,"r");
    if (!fptrR)
    {
        log_push_error_point(0x4962);
        status = S_OPENFILE;
        goto obd2datalog_merge_dlxfile_done;
    }
    fptrW = genfs_general_openfile(newdlxfilename,"w");
    if (!fptrR)
    {
        log_push_error_point(0x4963);
        status = S_OPENFILE;
        goto obd2datalog_merge_dlxfile_done;
    }

    bytecount = fread((char*)&dlxheader1, 1, sizeof(DlxHeader), fptrR);
    if (bytecount != sizeof(DlxHeader))
    {
        log_push_error_point(0x4964);
        status = S_READFILE;
        goto obd2datalog_merge_dlxfile_done;
    }
    crypto_blowfish_decryptblock_external_key((u8*)&dlxheader1,sizeof(DlxHeader));
    if (dlxheader1.count > MERGE_DLXFILE1_DLXBLOCK_CRC32E_MAX_COUNT)
    {
        //also prevent dlxblock_crc32e[] from being overrun
        log_push_error_point(0x4965);
        status = S_BADCONTENT;
        goto obd2datalog_merge_dlxfile_done;
    }
    //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    // copy dlxfilename1 content to newdlxfilename
    //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    bytecount = fwrite((char*)&dlxheader1, 1, sizeof(DlxHeader), fptrW);
    if (bytecount != sizeof(DlxHeader))
    {
        log_push_error_point(0x4966);
        status = S_WRITEFILE;
        goto obd2datalog_merge_dlxfile_done;
    }
    //just write a dummy header block right now, will update it at the end

    crc32e_sw_reset();
    newfile_crc32e = 0xFFFFFFFF;
    for(i=0;i<dlxheader1.count;i++)
    {
        bytecount = fread((char*)&dlxblock, 1, sizeof(DlxBlock), fptrR);
        if (bytecount != sizeof(DlxBlock))
        {
            log_push_error_point(0x4967);
            status = S_READFILE;
            goto obd2datalog_merge_dlxfile_done;
        }
        newfile_crc32e = crc32e_sw_calculateblock(newfile_crc32e,
                                                  (u32*)&dlxblock,sizeof(DlxBlock)/4);
        bytecount = fwrite((char*)&dlxblock, 1, sizeof(DlxBlock), fptrW);
        if (bytecount != sizeof(DlxBlock))
        {
            log_push_error_point(0x4968);
            status = S_WRITEFILE;
            goto obd2datalog_merge_dlxfile_done;
        }

        //calc crc32e on each block to later compare blocks in dlxfilename2
        //and skip any duplications.
        crc32e_reset();
        dlxblock_crc32e[i] = crc32e_calculateblock
            (0xFFFFFFFF,(u32*)&dlxblock,sizeof(DlxBlock)/4);
    }
    newfile_count = dlxheader1.count;
    genfs_closefile(fptrR);
    //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    // append dlxfilename2 content to newdlxfilename
    //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    fptrR = genfs_general_openfile(dlxfilename2,"r");
    if (!fptrR)
    {
        log_push_error_point(0x4969);
        status = S_OPENFILE;
        goto obd2datalog_merge_dlxfile_done;
    }
    bytecount = fread((char*)&dlxheader2, 1, sizeof(DlxHeader), fptrR);
    if (bytecount != sizeof(DlxHeader))
    {
        log_push_error_point(0x496A);
        status = S_READFILE;
        goto obd2datalog_merge_dlxfile_done;
    }
    crypto_blowfish_decryptblock_external_key((u8*)&dlxheader2,sizeof(DlxHeader));
    for(i=0;i<dlxheader2.count;i++)
    {
        bytecount = fread((char*)&dlxblock, 1, sizeof(DlxBlock), fptrR);
        if (bytecount != sizeof(DlxBlock))
        {
            log_push_error_point(0x496B);
            status = S_READFILE;
            goto obd2datalog_merge_dlxfile_done;
        }
        crc32e_reset();
        calc_crc32e = crc32e_calculateblock
            (0xFFFFFFFF,(u32*)&dlxblock,sizeof(DlxBlock)/4);
        for(j=0;j<dlxheader1.count;j++)
        {
            if (calc_crc32e == dlxblock_crc32e[j])
            {
                break;
            }
        }
        if (j >= dlxheader1.count)
        {
            //not found, so add it
            bytecount = fwrite((char*)&dlxblock, 1, sizeof(DlxBlock), fptrW);
            if (bytecount != sizeof(DlxBlock))
            {
                log_push_error_point(0x496C);
                status = S_WRITEFILE;
                goto obd2datalog_merge_dlxfile_done;
            }
            newfile_crc32e = crc32e_sw_calculateblock(newfile_crc32e,
                                                      (u32*)&dlxblock,sizeof(DlxBlock)/4);
            newfile_count++;
        }
    }
    //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    // update header with correct content
    //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    dlxheader1.count = newfile_count;
    dlxheader1.content_crc32e = newfile_crc32e;
    dlxheader1.header_crc32e = 0xFFFFFFFF;
    crc32e_reset();
    dlxheader1.header_crc32e = crc32e_calculateblock
        (0xFFFFFFFF,(u32*)&dlxheader1,sizeof(DlxHeader)/4);
    crypto_blowfish_encryptblock_external_key((u8*)&dlxheader1,sizeof(DlxHeader));

    if (fseek(fptrW, 0, F_SEEK_SET) != 0)
    {
        log_push_error_point(0x496D);
        status = S_SEEKFILE;
        goto obd2datalog_merge_dlxfile_done;
    }
    bytecount = fwrite((char*)&dlxheader1, 1, sizeof(DlxHeader), fptrW);
    if (bytecount != sizeof(DlxHeader))
    {
        log_push_error_point(0x496E);
        status = S_WRITEFILE;
        goto obd2datalog_merge_dlxfile_done;
    }
    status = S_SUCCESS;

obd2datalog_merge_dlxfile_done:
    if (fptrR)
    {
        genfs_closefile(fptrR);
    }
    if (fptrW)
    {
        genfs_closefile(fptrW);
    }
    return status;
}

//------------------------------------------------------------------------------
// Get total signal count
// Return:  u8  status
// Engineer: Quyen Leba
//------------------------------------------------------------------------------
u8 obd2datalog_get_total_signal(u32 *total)
{
    *total = 0;
    if (dataloginfo == NULL)
    {
        log_push_error_point(0x4950);
        return S_BADCONTENT;
    }
    else if (dataloginfo->dlxheader == NULL)
    {
        log_push_error_point(0x4951);
        return S_BADCONTENT;
    }
    *total = dataloginfo->dlxheader->count;
    return S_SUCCESS;
}

//------------------------------------------------------------------------------
// Enable/Disable new data filter
// Input:   bool enabled (TRUE: use filter)
// Return:  u8  status
// Engineer: Quyen Leba
//------------------------------------------------------------------------------
u8 obd2datalog_newdata_filter(bool enabled)
{
    if (dataloginfo == NULL)
    {
        log_push_error_point(0x4952);
        return S_BADCONTENT;
    }

    dataloginfo->control.newdata_filtered = 0;
    if (enabled)
    {
        dataloginfo->control.newdata_filtered = 1;
    }
    return S_SUCCESS;
}

//------------------------------------------------------------------------------
// Start datalog record
// Return:  u8  status
// Engineer: Quyen Leba
//------------------------------------------------------------------------------
u8 obd2datalog_start_record()
{
    if (dataloginfo == NULL)
    {
        log_push_error_point(0x4953);
        return S_BADCONTENT;
    }
    if (dataloginfo->record.control.start_pending == 0)
    {
        dataloginfo->record.control.start_pending = 1;
    }
    return S_SUCCESS;
}

//------------------------------------------------------------------------------
// Stop datalog record
// Return:  u8  status
// Engineer: Quyen Leba
//------------------------------------------------------------------------------
u8 obd2datalog_stop_record()
{
    if (dataloginfo == NULL)
    {
        log_push_error_point(0x4954);
        return S_BADCONTENT;
    }
    if (dataloginfo->record.control.stop_pending == 0)
    {
        dataloginfo->record.control.stop_pending = 1;
    }
    return S_SUCCESS;
}

//------------------------------------------------------------------------------
// Datalog record handler
// Return:  u8  status
// Engineer: Quyen Leba
// Datalog record file has 2 parts: <dlx file> <data points>
// Each data point:
//  [4:packet_id 0xAA5572zz][4:timestamp][2:count]
//  [2:item index][4:item value]...[2:item index][4:item value]
//  
//  [2:item index] = AAAAxxxx xxxxxxxx, where AAAA = Datalog Signal Attribute
//------------------------------------------------------------------------------
u8 obd2datalog_record()
{
    u8  *bptr1;
    u8  *bptr2;
    u16 count;
    u16 currentlength;
    u32 bytecount;
    u32 timestamp;
    u8  i;
    u8  status;

    if (dataloginfo->record.control.state == DATALOG_RECORD_STATE_STARTED)
    {
        if (dataloginfo->record.control.stop_pending == 0)
        {
            if (dataloginfo->record.bufferlength + (12+4+4) < sizeof(dataloginfo->record.buffer))
            {
                //buffer has room to store 2+ items
                bptr1 = &dataloginfo->record.buffer[dataloginfo->record.bufferlength];
                bptr2 = bptr1+10;
                count = 0;
                currentlength = dataloginfo->record.bufferlength + 10;
                timestamp = (u32)rtc_getvalue();
                for(i=0;i<dataloginfo->datalogsignalcount;i++)
                {
                    if (dataloginfo->datalogsignals[i].Control & DATALOG_CONTROL_NEWDATA_RECORD_SET)
                    {
                        if (dataloginfo->datalogsignals[i].PidType != PidTypeOSC &&
                            (currentlength + 6) < sizeof(dataloginfo->record.buffer))                        
                        {
                            *(u16*)bptr2 = dataloginfo->datalogsignals[i].DlxIndex + (SignalAttrib_Data << 12);
                            bptr2 += 2;
                            memcpy((char*)bptr2,(char*)&dataloginfo->datalogsignals[i].Value,4);
                            bptr2 += 4;
                            currentlength += 6;

                            count++;
                            dataloginfo->datalogsignals[i].Control &= DATALOG_CONTROL_NEWDATA_RECORD_CLR;
                        }
                        // OSC PIDs report data and status in two records with the same index:
                        //[2:OSC item index][4:OSC item status]...[2:OSC item index][4:OSC item set value]
                        else if (dataloginfo->datalogsignals[i].PidType == PidTypeOSC && 
                                 (currentlength + 12) < sizeof(dataloginfo->record.buffer))
                        {
                            // OSC Item Status
                            *(u16*)bptr2 = dataloginfo->datalogsignals[i].DlxIndex + (SignalAttrib_Status << 12);
                            bptr2 += 2;
                            memcpy((char*)bptr2,(char*)&dataloginfo->datalogsignals[i].Value,4);
                            bptr2 += 4;
                            currentlength += 6;
                            // OSC Item Set Value
                            *(u16*)bptr2 = dataloginfo->datalogsignals[i].DlxIndex + (SignalAttrib_Data << 12);
                            bptr2 += 2;
                            memcpy((char*)bptr2,(char*)&dataloginfo->datalogsignals[i].Data,4);
                            bptr2 += 4;
                            currentlength += 6;
                            
                            count+=2;
                            dataloginfo->datalogsignals[i].Control &= DATALOG_CONTROL_NEWDATA_RECORD_CLR;
                        }
                        else
                        {
                            break;
                        }
                    }
                }//for(i=0;i<dataloginfo->datalogsignalcount;i++)...
                if (count > 0)
                {
                    *(u32*)bptr1 = 0xAA557271;
                    *(u32*)(bptr1+4) = timestamp;
                    *(u16*)(bptr1+8) = count;
                    dataloginfo->record.bufferlength = currentlength;
                }
            }
            else
            {
                bytecount = fwrite(dataloginfo->record.buffer,
                                   1,dataloginfo->record.bufferlength,
                                   dataloginfo->record.fptr);
                if (bytecount != dataloginfo->record.bufferlength)
                {
                    status = S_WRITEFILE;
                    goto obd2datalog_record_error;
                }
                dataloginfo->record.bufferlength = 0;
                dataloginfo->record.totallength += bytecount;
                if (dataloginfo->record.totallength > DATALOG_MAX_RECORD_FILE_SIZE)
                {
                    genfs_closefile(dataloginfo->record.fptr);
                    dataloginfo->record.fptr = NULL;
                    dataloginfo->record.control.start_pending = 0;
                    dataloginfo->record.control.stop_pending = 0;
                    dataloginfo->record.control.state = DATALOG_RECORD_STATE_FULL;
                }
            }
        }//if (dataloginfo->record.control.stop_pending == 0)...
        else
        {
            dataloginfo->record.control.stop_pending = 0;
            if (dataloginfo->record.fptr)
            {
                genfs_closefile(dataloginfo->record.fptr);
                dataloginfo->record.fptr = NULL;
            }
            dataloginfo->record.control.state = DATALOG_RECORD_STATE_STOPPED;
            dataloginfo->record.reportstatus.recording = 0;
        }
    }//if (dataloginfo->record.control.state == DATALOG_RECORD_STATE_STARTED)...
    else if (dataloginfo->record.control.state == DATALOG_RECORD_STATE_STOPPED ||
             dataloginfo->record.control.state == DATALOG_RECORD_STATE_IDLE)
    {
        if (dataloginfo->record.control.start_pending)
        {
            if (dataloginfo->record.fptr)
            {
                genfs_closefile(dataloginfo->record.fptr);
                dataloginfo->record.fptr = NULL;
            }
            if (dataloginfo->record.control.state == DATALOG_RECORD_STATE_IDLE)
            {
                status = file_copy(dataloginfo->dlxfilename, 
                                   DEFAULT_DATALOG_RECORD_FILENAME,
                                   NULL,NULL,NULL);
                if (status != S_SUCCESS)
                {
                    status = S_OPENFILE;
                    goto obd2datalog_record_error;
                }

                dataloginfo->record.fptr = genfs_user_openfile(DEFAULT_DATALOG_RECORD_FILENAME,"ab");
                if (dataloginfo->record.fptr == NULL)
                {
                    status = S_OPENFILE;
                    goto obd2datalog_record_error;
                }
            }
            else if (dataloginfo->record.control.state == DATALOG_RECORD_STATE_STOPPED)
            {
                dataloginfo->record.fptr = genfs_user_openfile(DEFAULT_DATALOG_RECORD_FILENAME,"a");
                if (dataloginfo->record.fptr == NULL)
                {
                    status = S_OPENFILE;
                    goto obd2datalog_record_error;
                }
            }
            else
            {
                status = S_ERROR;
                goto obd2datalog_record_error;
            }
            dataloginfo->record.control.state = DATALOG_RECORD_STATE_STARTED;
            dataloginfo->record.control.start_pending = 0;
            dataloginfo->record.reportstatus.recording = 1;
        }//if (dataloginfo->record.control.start_pending)...
        else if (dataloginfo->record.control.stop_pending)
        {
            if (dataloginfo->record.fptr)
            {
                genfs_closefile(dataloginfo->record.fptr);
                dataloginfo->record.fptr = NULL;
            }
            dataloginfo->record.control.stop_pending = 0;
            dataloginfo->record.reportstatus.recording = 0;
        }
    }//(DATALOG_RECORD_STATE_STOPPED || DATALOG_RECORD_STATE_IDLE)...
    else if (dataloginfo->record.control.state == DATALOG_RECORD_STATE_FULL)
    {
        if (dataloginfo->record.control.start_pending)
        {
            dataloginfo->record.control.start_pending = 0;
            dataloginfo->record.reportstatus.recording = 1;
        }
        else if (dataloginfo->record.control.stop_pending)
        {
            dataloginfo->record.control.stop_pending = 0;
            dataloginfo->record.reportstatus.recording = 0;
        }
    }

    // Calculate percentage of available space used
    dataloginfo->record.reportstatus.percentusedspace = (u8)((float)dataloginfo->record.totallength / (float)DATALOG_MAX_RECORD_FILE_SIZE * 100.0);

    return S_SUCCESS;
obd2datalog_record_error:
    dataloginfo->record.control.state = DATALOG_RECORD_STATE_ERROR;
    dataloginfo->record.control.start_pending = 0;
    dataloginfo->record.control.stop_pending = 0;
    if (dataloginfo->record.fptr)
    {
        genfs_closefile(dataloginfo->record.fptr);
        dataloginfo->record.fptr = NULL;
    }
    return status;
}

//------------------------------------------------------------------------------
// Evaluate dlx file for list of items can datalog in a session
// Input:   const u8 *dlx_filename
// Outputs: u16 *index_list (in order of dlx file)
//          u16 *index_count
// Return:  u8  status
// Engineer: Quyen Leba
// Note: correct commtype & commlevel already inited
//------------------------------------------------------------------------------
u8 obd2datalog_evaluate_dlx_file_for_datalog_session(const u8 *dlx_filename,
                                                     u16 *index_list,
                                                     u16 *index_count)
{
    VehicleCommType commtype[ECM_MAX_COUNT];
    VehicleCommLevel commlevel[ECM_MAX_COUNT];
    u8  validpidlist[MAX_DATALOG_DLXBLOCK_COUNT];
    u16 validpidcount;
    u16 fitpidlist[MAX_DATALOG_DLXBLOCK_COUNT];
    u16 fitpidcount;
    u32 i,j;
    u8  status;

    status = obd2datalog_init(commtype,commlevel);
    if (status != S_SUCCESS)
    {
        log_push_error_point(0x4956);
        goto obd2datalog_evaluate_dlx_file_for_datalog_session_done;
    }

    status = obd2datalog_dataloginfo_init();
    if (status != S_SUCCESS)
    {
        log_push_error_point(0x493F);
        goto obd2datalog_evaluate_dlx_file_for_datalog_session_done;
    }
    
    status = obd2datalog_parsedatalogfile(dlx_filename);
    if (status != S_SUCCESS)
    {
        log_push_error_point(0x4940);
        goto obd2datalog_evaluate_dlx_file_for_datalog_session_done;
    }    

    delays(100,'m');
    status = obd2datalog_getvalidpidlist
            (dataloginfo->dlxheader,dataloginfo->dlxblocks,
             commtype,commlevel,
             validpidlist,&validpidcount,NULL);
    if (status != S_SUCCESS)
    {
        log_push_error_point(0x4942);
        goto obd2datalog_evaluate_dlx_file_for_datalog_session_done;
    }

    status = obd2data_set_skip_invalid_signals(validpidlist,validpidcount);
    if (status != S_SUCCESS)
    {
        log_push_error_point(0x495A);
        goto obd2datalog_evaluate_dlx_file_for_datalog_session_done;
    }

    status = obd2datalog_evaluate_fit(commtype,commlevel,
                                      fitpidlist,&fitpidcount);
    if (status != S_SUCCESS)
    {
        log_push_error_point(0x4955);
        goto obd2datalog_evaluate_dlx_file_for_datalog_session_done;
    }

    //obd2data_set_skip_invalid_signals(...) already take care of this part
    *index_count = 0;
    for(i=0;i<fitpidcount;i++)
    {
        for(j=0;j<validpidcount;j++)
        {
            if (fitpidlist[i] == validpidlist[j])
            {
                index_list[*index_count] = fitpidlist[i];
                (*index_count)++;
                break;
            }
        }
    }

obd2datalog_evaluate_dlx_file_for_datalog_session_done:
    return status;
}

//------------------------------------------------------------------------------
// Demo function to send tester present
// Engineer: Quyen Leba
//------------------------------------------------------------------------------
void obd2datalog_demo_sendtesterpresent()
{
    //do nothing
}

//------------------------------------------------------------------------------
// Demo function to simulate get data from rapid packets
// Input:   VehicleCommLevel commlevel[ECM_MAX_COUNT]
// Return:  u8  status
// Engineer: Quyen Leba
//------------------------------------------------------------------------------
u8 obd2datalog_demo_getdata(VehicleCommLevel *vehiclecommlevel)
{
    u8  i,j;

    for(j=0;j<dataloginfo->datalogsignalcount;j++)
    {
        dataloginfo->datalogsignals[j].Control |= DATALOG_CONTROL_NEWDATA_SET;

        for(i=0;i<demo_datalog.count;i++)
        {
            if (dataloginfo->datalogsignals[j].Address == demo_datalog.items[i].address)
            {
                switch(demo_datalog.items[i].dummyvaluetype)
                {
                case DUMMYVALUETYPE_RANDOM:
                    dataloginfo->datalogsignals[j].Value = ((rand() % (int)(demo_datalog.items[i].hi - demo_datalog.items[i].lo)*1000) + demo_datalog.items[i].lo*1000) / 1000.0;
                    break;
                case DUMMYVALUETYPE_SWEEP:
                    dataloginfo->datalogsignals[j].Value += demo_datalog.items[i].dummyattrvalue;
                    if (dataloginfo->datalogsignals[j].Value > demo_datalog.items[i].hi)
                    {
                        dataloginfo->datalogsignals[j].Value = demo_datalog.items[i].lo;
                    }
                    break;
                default:
                    break;
                }
            }
        }
    }

    return S_SUCCESS;
}

//------------------------------------------------------------------------------
// Check if all signals fit (for single rate datalogging)
// Input:   VehicleCommLevel commlevel[ECM_MAX_COUNT]
// Return:  u8  status
// Engineer: Tristen Pierson
//------------------------------------------------------------------------------
u8 obd2_datalog_evaluatesignalsetup(VehicleCommLevel *vehiclecommlevel)
{
    u16 i;
    
    if (dataloginfo == NULL)
    {
        log_push_error_point(0x49BB);
        return S_BADCONTENT;
    }

    dataloginfo->analogsignalcount = 0;
    dataloginfo->pidsignalcount = 0;
    dataloginfo->dmrsignalcount = 0;
    dataloginfo->oscsignalcount = 0;
    
    for(i=0;i<dataloginfo->datalogsignalcount;i++)
    {
        // Remove packet information; all signals are single rate
        dataloginfo->datalogsignals[i].SignalType.Generic.PacketNumber = DATALOG_INVALID_PACKETID;
        dataloginfo->datalogsignals[i].SignalType.Generic.Position = 0;
        if ((dataloginfo->datalogsignals[i].Control & DATALOG_CONTROL_SKIP) == 0)
        {
            switch (dataloginfo->datalogsignals[i].PidType)
            {
                case PidTypeAnalog:
                    if (dataloginfo->analogsignalcount < dataloginfo->analogsignalmaxavailable)
                    {
                        dataloginfo->analogsignalindex[dataloginfo->analogsignalcount++] = i;
                    }
                   break;
                case PidTypeMode1:
                case PidTypeRegular:
                    dataloginfo->pidsignalcount++;
                    break;
                case PidTypeDMR:
                    dataloginfo->dmrsignalcount++;
                    break;
                case PidTypeOSC:
                    dataloginfo->oscsignalcount++;
                    break;
            }
        }
        if (dataloginfo->pidsignalcount + dataloginfo->dmrsignalcount + 
            dataloginfo->analogsignalcount + dataloginfo->oscsignalcount >= 
            dataloginfo->datalogsignalcount)
        {
            break;
        }
    }    
    if (dataloginfo->pidsignalcount + dataloginfo->dmrsignalcount + 
        dataloginfo->analogsignalcount + dataloginfo->oscsignalcount < 
        dataloginfo->datalogsignalcount)
    {
        // Cannot fit all signals
        log_push_error_point(0x49BC);
        return S_NOTFIT;
    }    
    return S_SUCCESS;
}

/**
 * @brief   Update OSC Signal
 *
 * @param   [in] *data
 * @param   [in] count
 *
 * @retval  u8 status
 *
 * @author  Tristen Pierson
 *
 * @details This function updates an OSC signal item. Data format is as follows:
 *
 *          [2:index 1][4:data 1][1:status 1]...[2:index n][4:data n][1:status n]
 */
u8 obd2_datalog_update_signal_osc(u8 *data, u16 count)
{
    u16 i, index;    
    float signaldata;

    for (i = 0; i < count; i+=7)
    {
        index = *(u16*)&data[i];
        signaldata = *(float*)&data[i+2];
        if (index > MAX_DATALOG_DLXBLOCK_COUNT)
        {
            return S_FAIL;
        }
        if (dataloginfo->datalogsignals[index].PidType == PidTypeOSC)
        {
            dataloginfo->datalogsignals[index].Data = signaldata;
            DatalogControl_SetOSC(dataloginfo->datalogsignals[index].Control,data[i+6]);
        }
        else
        {
            return S_FAIL;
        }
    }
    return S_SUCCESS;
}
