/**
  **************** (C) COPYRIGHT 2011 SCT Performance, LLC *********************
  * File Name          : atoi.h
  * Device / System    : SCT products using the "CommonCode" platform
  * Author             : Quyen Leba
  *
  * Version            : 1 
  * Date               : 05/02/2011
  * Description        : 
  *                    : 
  *
  *
  * History            : 05/02/2011 M. Davis
  *                    :   Add SCT Copyright header,
  *
  ******************************************************************************
  */

#ifndef __ATOI_H
#define __ATOI_H

#include <arch/gentype.h>

u8 atoi(const u8 *inputstr, s32 *intnum);

#endif  //__ATOI_H
