/**
  **************** (C) COPYRIGHT 2011 SCT Performance, LLC *********************
  * File Name          : checksum_ipc.h
  * Device / System    : SCT products using the "CommonCode" platform
  * Author             : Quyen Leba
  *
  * Version            : 1 
  * Date               : 05/02/2011
  * Description        : 
  *                    : 
  *
  *
  * History            : 05/02/2011 M. Davis
  *                    :   Add SCT Copyright header,
  *
  ******************************************************************************
  */

#ifndef __CHECKSUM_IPC_H
#define __CHECKSUM_IPC_H

#include <arch/gentype.h>
#include <fs/genfs.h>

u8 checksum_ipc_apply_to_tunefile(F_FILE *tune_fptr, u8 *ipc_filename,
                                  u16 ecm_type, u32 offset);

#endif    //__CHECKSUM_IPC_H
