/**
  **************** (C) COPYRIGHT 2011 SCT Performance, LLC *********************
  * File Name          : cmdif_func_vehiclefunction.c
  * Device / System    : SCT products using the "CommonCode" platform
  * Author             : Quyen Leba
  *
  * Version            : 1 
  * Date               : 05/02/2011
  * Description        : 
  *                    : 
  *
  *
  * History            : 05/02/2011 M. Davis
  *                    :   Add SCT Copyright header,
  *
  ******************************************************************************
  */

#include <string.h>
#include <stdio.h>
#include <fs/genfs.h>
#include <board/delays.h>
#include <board/genplatform.h>
#include <common/statuscode.h>
#include <common/obd2.h>
#include <common/crypto_messageblock.h>
#include <common/housekeeping.h>
#include <common/itoa.h>
#include <common/cmdif.h>
#include <common/cmdif_remotegui.h>
#include <common/genmanuf_overload.h>
#include "cmdif_func_vehiclefunction.h"

//------------------------------------------------------------------------------
// Log error point: 
//------------------------------------------------------------------------------

extern cmdif_datainfo cmdif_datainfo_responsedata;  //from cmdif.c

/**
 * @brief   Vehicle special functions
 *  
 * @param   [in] cmd
 *
 * @retval  u8  status
 *  
 * @details This function checks commtype and commlevel before calling the
 *          OEM specific special functions menu.
 *  
 * @authors Quyen Leba, Tristen Pierson
 */
u8 cmdif_func_vehiclefunction(CMDIF_COMMAND cmd)
{
    VehicleCommType vehiclecommtype[ECM_MAX_COUNT];
    VehicleCommLevel vehiclecommlevel[ECM_MAX_COUNT];
    CMDIF_REMOTEGUI_RESPONSETYPE responsetype;
    u16 responsechoice;
    u8  status;
    
    cmdif_remotegui_messagebox(cmd,
            "VEHICLE FUNCTIONS", CMDIF_REMOTEGUI_ICON_NONE,
            CMDIF_REMOTEGUI_BUTTONFACE_None, CMDIF_REMOTEGUI_BUTTONFACE_None, CMDIF_REMOTEGUI_BUTTONFACE_None,
            CMDIF_REMOTEGUI_FOCUS_None,
            "<HEADER>PROCESSING DATA</HEADER>\nPlease wait...");
            
    // Check commtype and commlevel; we need this to determine which special
    // functions are available.
    status = obd2_findvehiclecommtypecommlevel(vehiclecommtype, vehiclecommlevel);
    // No comm detected
    if (status != S_SUCCESS)
    {        
        cmdif_remotegui_messagebox(cmd,
                                   "VEHICLE FUNCTIONS", CMDIF_REMOTEGUI_ICON_NONE,
                                   CMDIF_REMOTEGUI_BUTTONFACE_Exit, CMDIF_REMOTEGUI_BUTTONFACE_None, CMDIF_REMOTEGUI_BUTTONFACE_None,
                                   CMDIF_REMOTEGUI_FOCUS_None,
                                   "<HEADER>ERROR</HEADER>\nUnable to communicate with vehicle. Please check connections.");
        cmdif_remotegui_wait_user_response(&responsetype, &responsechoice, NULL);
    }
    // Commlevel is supported, proceed with OEM vehicle function menu
    else if (vehiclecommlevel[0] != CommLevel_Unknown && !SETTINGS_IsDemoMode())
    {        
        obd2_vehiclefunction(vehiclecommtype, vehiclecommlevel);
    }
    // Commlevel is unsupported; OEM type is not supported by the device.
    else
    {        
        cmdif_remotegui_messagebox(cmd,
                                   "SPECIAL FUNCTIONS", CMDIF_REMOTEGUI_ICON_NONE,
                                   CMDIF_REMOTEGUI_BUTTONFACE_Exit, CMDIF_REMOTEGUI_BUTTONFACE_None, CMDIF_REMOTEGUI_BUTTONFACE_None,
                                   CMDIF_REMOTEGUI_FOCUS_None,
                                   "No special functions are available for this vehicle.");
        cmdif_remotegui_wait_user_response(&responsetype, &responsechoice, NULL);
    }
    
    cmdif_datainfo_responsedata.responsecode = CMDIF_ACK_FAILED;
    return S_USERABORT;
}
