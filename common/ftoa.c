/**
  **************** (C) COPYRIGHT 2011 SCT Performance, LLC *********************
  * File Name          : ftoa.c
  * Device / System    : SCT products using the "CommonCode" platform
  * Author             : Quyen Leba
  *
  * Version            : 1 
  * Date               : 05/02/2011
  * Description        : 
  *                    : 
  *
  *
  * History            : 05/02/2011 M. Davis
  *                    :   Add SCT Copyright header,
  *
  ******************************************************************************
  */

#include "ftoa.h"

//------------------------------------------------------------------------------
// Convert an float to a 0-terminated string
// Inputs:  float value (to convert)
//          char *string (pointer to resulting string)
//          unsigned char precision (max 4)
// Output:  char *string (the "string" location is modified,
//                       allow at least 12 chars)
// Return:  char *string
// Engineer: Quyen Leba
//------------------------------------------------------------------------------
char* ftoa(float value, unsigned char precision, char* string)
{
    char buffer[32];
    unsigned char negative;     //0: positive, 1: negative
    unsigned int unsignedNumber;
    unsigned int unsignedPrecision;
    unsigned char total_precision_count;
    char *buffer_bptr;
    char *prec_bptr;
    unsigned int temp;

    total_precision_count = 5;
    buffer_bptr = buffer;
    negative = 0;
    if(value < 0)
    {
        negative = 1;
        unsignedNumber = (unsigned int)(-value);
        unsignedPrecision = (unsigned int)(((-value) - (float)unsignedNumber)*100000);
    }
    else
    {
        unsignedNumber = (unsigned int)value;
        unsignedPrecision = (unsigned int)((value - (float)unsignedNumber)*100000);
    }

    do
    {
        temp = unsignedNumber % 10;
        *(buffer_bptr++) = temp + '0';
    }
    while((unsignedNumber /= 10) != 0);
    
    if(negative)
    {
        *(buffer_bptr++) = '-';
    }
    
    do
    {
        *(string++) = *(--buffer_bptr);
    }
    while(buffer_bptr > buffer);

    if (precision > 0)
    {
        prec_bptr = string;
        *(string++) = '.';

        buffer_bptr = buffer;
        do
        {
            temp = unsignedPrecision % 10;
            *(buffer_bptr++) = temp + '0';
            unsignedPrecision /= 10;
        }
        while(--total_precision_count != 0);

        do
        {
            *(string++) = *(--buffer_bptr);
        }
        while(buffer_bptr > buffer);

        if (precision > 4)
        {
            precision = 4;
        }
        prec_bptr[precision+1] = 0;
    }

    *string = 0;
    
    return string;
}
