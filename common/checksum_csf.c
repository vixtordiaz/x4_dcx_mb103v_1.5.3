/**
  **************** (C) COPYRIGHT 2011 SCT Performance, LLC *********************
  * File Name          : checksum_csf.c
  * Device / System    : SCT products using the "CommonCode" platform
  * Author             : Ricardo Cigarroa
  *
  * Version            : 1 
  * Date               : 03/14/2014
  * Description        : 
  *                    : 
  *
  *
  * History            : 03/14/2014 R. Cigarroa
  *                    :   Add SCT Copyright header,
  *
  ******************************************************************************
  */


#include <string.h>
#include <board/genplatform.h>
#include <common/genmanuf_overload.h>
#include <common/obd2tune.h>
#include <common/crypto_blowfish.h>
#include <common/statuscode.h>
#include <common/filestock.h>
#include <common/file.h>
#include <common/crc32.h>
#include "checksum_csf.h"

//------------------------------------------------------------------------------
// Log error point: 0x5050 -> 5080
//------------------------------------------------------------------------------
extern flasher_info *flasherinfo;                   //from obd2tune.c

//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
const short crc_hh_nibble_table[] =
{
            0x0000, 0xE003, 0x4003, 0xA000,
            0x8006, 0x6005, 0xC005, 0x2006,
            0x8009, 0x600A, 0xC00A, 0x2009,
            0x000F, 0xE00C, 0x400C, 0xA00F
};

const short crc_hl_nibble_table[] =
{
            0x0000, 0x8603, 0x8C03, 0x0A00,
            0x9803, 0x1E00, 0x1400, 0x9203,
            0xB003, 0x3600, 0x3C00, 0xBA03,
            0x2800, 0xAE03, 0xA403, 0x2200
};

const short crc_lh_nibble_table[] =
{
            0x0000, 0x8063, 0x80C3, 0x00A0,
            0x8183, 0x01E0, 0x0140, 0x8123,
            0x8303, 0x0360, 0x03C0, 0x83A3,
            0x0280, 0x82E3, 0x8243, 0x0220,
};


const short crc_ll_nibble_table[] =
{
            0x0000, 0x8005, 0x800F, 0x000A,
            0x801B, 0x001E, 0x0014, 0x8011,
            0x8033, 0x0036, 0x003C, 0x8039,
            0x0028, 0x802D, 0x8027, 0x0022,
};


//------------------------------------------------------------------------------
// Apply CSF checksum to a file
// Inputs:  F_FILE  *tune_fptr (file to apply the checksum to. It could be
//                              either stock.bin or flash.bin. This is also
//                              the same function used to apply checksum to 
//                              the stock.bin after doing an upload. )
//          u8  *csf_filename (file contain cfs checksum info)
//          u16 ecm_type
//          bool ischeckonly
// Return:  u8  status
// Engineer: Ricardo Cigarroa
//------------------------------------------------------------------------------
u8 checksum_csf_apply_to_tunefile(F_FILE *tune_fptr, u8 *csf_filename, u16 ecm_type, 
                                  bool ischeckonly)
{
    //F_FILE  *fptr;
    u8  status;

    //fptr = NULL;
    status = S_SUCCESS;
    
    //Check for .csf file extension
    if (strstr((char*)csf_filename, ".csf") == 0)
    {
        log_push_error_point(0x5050);
        status = S_INPUT;
        goto checksum_csf_apply_to_tunefile_done;
    }
      
    //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    // Validate .CSF file and process blocks
    //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    status = checksum_csf_getcsffileinfo(csf_filename,ischeckonly,
                                         ecm_type, tune_fptr);
    if (status != S_SUCCESS)
    {
        log_push_error_point(0x5051);
        status = S_FAIL;
        goto checksum_csf_apply_to_tunefile_done;
    }
    
    //copy modified .csf into temp .csf. The temp .csf is used for the download. 
    status = file_copy(csf_filename,CSFTEMPFILE,NULL,NULL,NULL); 
    
checksum_csf_apply_to_tunefile_done:
/*    if (fptr)
    {
        genfs_closefile(fptr);
    }
*/
    return status;
}

//------------------------------------------------------------------------------
// Validate CSF file
// Inputs:    u8 *filename (preloaded csf from default folder 
//                          & custom csf from user folder)
// Return:    u8  status
// Description: This function is used in multiple places during the flashing 
//             process. When first processing the .csf file, the file can be in
//             any of the two directories (user or strategy), based on the type 
//             of tune. After processing the file, it is then stored in 
//             "\\USER\\checksumfiletemp.csf". Therefore, we attempt to open the
//             file from both directories. 
// Engineer: Ricardo Cigarroa
//------------------------------------------------------------------------------
u8 checksum_csf_validate_file(u8 *csf_filename, u16 *checksumcount, u8 *response)
{
   Csf_Header *csfheader;
   F_FILE *fptr;
   u8  buffer[128];
   u32 bytecount;
   u32 crc32e_calc;
   u8 status; 
  
   csfheader = NULL; 
   fptr = NULL;
   status = S_SUCCESS;
      
   fptr = genfs_user_openfile((u8*)csf_filename,"r"); 
   if (fptr == NULL)
   {
       fptr = genfs_default_openfile((u8*)csf_filename,"r");  
       if (fptr == NULL)
       {
           status = S_OPENFILE;
           *response = CMDIF_ACK_CSF_FILE_NOT_FOUND;
           goto checksum_csf_validate_file_done;
       }
   }
       
   csfheader = __malloc(sizeof(Csf_Header));
   if (csfheader == NULL)
   {
       status = S_MALLOC;
       goto checksum_csf_validate_file_done;
   }
   
   memset((void*)buffer,0,sizeof(buffer)); 
   memset((void*)csfheader,0,sizeof(Csf_Header));
   
   bytecount = fread((char*)csfheader,1,sizeof(Csf_Header),fptr);
   if (bytecount != sizeof(Csf_Header))
   {
       status = S_READFILE;
       goto checksum_csf_validate_file_done;
   }
   
#if CSF_CHECKSUM_ENCRYPTION_ENABLED
    crypto_blowfish_decryptblock_critical((u8*)csfheader,sizeof(Csf_Header));
#endif  

   //check for version before checking for header and content crc32.
   if (csfheader->version != CSF_VERSION)
   {
       log_push_error_point(0x5052);
      // status = S_INVALID_VERSION;
       //goto checksum_csf_validate_file_done; 
   }
   
   status = checksum_csf_process_header(csfheader,NULL);
   if (status != S_SUCCESS)
   {
       log_push_error_point(0x5053);
       //goto checksum_csf_validate_file_done;
   } 
   
   *checksumcount = csfheader->checksumcount;

   crc32e_reset();
   crc32e_calc = 0xFFFFFFFF;
   while (!feof(fptr))
   {
       bytecount = fread((char*)buffer,1,sizeof(buffer),fptr);
       if (bytecount)
       {
           crc32e_calc = crc32e_calculateblock(crc32e_calc,(u32*)buffer,bytecount/4);    
       }
   }
   
   if (csfheader->content_crc32e != 0 && csfheader->content_crc32e != crc32e_calc)
   {
       log_push_error_point(0x5054);
       //status = S_CRC32E;
       goto checksum_csf_validate_file_done;
   }


checksum_csf_validate_file_done:
   if (fptr)
   {
       genfs_closefile(fptr);
   }
   if (csfheader)
   {
       __free(csfheader);
   }
   if (status == S_CRC32E)
   {
       *response = CMDIF_ACK_CSF_FILE_CORRUPTED;
   }
    
   return status; 
}

//------------------------------------------------------------------------------
// Process checksum csf header
// Input:   CSF_Heaader *csfheader
// Output:  u32 *crc32e (NULL allowed)
// Return:  u8  status
// Engineer: Ricardo Cigarroa
//------------------------------------------------------------------------------
u8 checksum_csf_process_header(Csf_Header *csfheader, u32 *crc32e)
{
    u32 crc32e_calc;
    u32 crc32e_cmp;
    u8  status;
    
    crc32e_calc = 0;
    if (!csfheader)
    {
       status = S_INPUT;
       goto checksum_csf_process_header_done;   
    }
    
    crc32e_reset();
    crc32e_cmp = csfheader->header_crc32e;
    csfheader->header_crc32e = 0xFFFFFFFF;
    crc32e_calc = crc32e_calculateblock(0xFFFFFFFF,
                                        (u32*)csfheader,sizeof(Csf_Header)/4);
    csfheader->header_crc32e = crc32e_cmp;
    if (crc32e_cmp != 0 && crc32e_calc != crc32e_cmp)
    {
       crc32e_calc = 0;
       //status = S_CRC32E;
       //goto checksum_csf_process_header_done;  
    }
    
    status = S_SUCCESS;
    
checksum_csf_process_header_done:
    if (crc32e)
    {
       *crc32e = crc32e_calc;
    }
    
    return status;
}

//------------------------------------------------------------------------------
// Get and store .CSF file information 
// Inputs:  u8  *csf_filename  (csf file),
//          bool ischeckonly,
//          u16 ecm_type,
//          F_FILE *tune_fptr,
// Return:  u8  status
// Engineer: Ricardo Cigarroa
//------------------------------------------------------------------------------
u8 checksum_csf_getcsffileinfo(u8 *csf_filename, bool ischeckonly, u16 ecm_type, 
                               F_FILE *tune_fptr)
{
    u8  count;
    u8  status;
    u8  response; 
    u16 block_type;
    u16 checksumcount; 
    u32 bytesread;
    s32 offset;
    u32 file_position;
    F_FILE *fptr = NULL;  
    Csf_Block_Info *block_info = NULL;
    bool is_rsadata_found = FALSE;
    
    status = S_SUCCESS;

    //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    // Validate .CSF file
    //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    checksumcount = 0; 
    status = checksum_csf_validate_file(csf_filename,&checksumcount,&response);
    if (status != S_SUCCESS)
    {
        log_push_error_point(0x5055);
        goto checksum_getcsffileinfo_done;
    }

    if (flasherinfo->flashtype == CMDIF_ACT_CUSTOM_FLASHER)
    {
        fptr = genfs_user_openfile((u8*)csf_filename,"r+"); 
    }
    else
    {
        //when flashing with a preloaded tune, .csf file should be in default path.
        fptr = genfs_default_openfile((u8*)csf_filename,"r+");
    }

    if (!fptr)
    {
        log_push_error_point(0x5056);
        return S_OPENFILE;
    }
       
    if (checksumcount == 0)
    {
        log_push_error_point(0x5057);
        status = S_BADCONTENT;
        goto checksum_getcsffileinfo_done;
    }
    else if (checksumcount > MAX_CSF_BLOCK_COUNT)
    {
        log_push_error_point(0x5058);
        status = S_NOTFIT;
        goto checksum_getcsffileinfo_done;
    }
    
    block_info = __malloc(sizeof(Csf_Block_Info));
    if (block_info == NULL)
    {
        log_push_error_point(0x5059);
        status = S_MALLOC; 
        goto checksum_getcsffileinfo_done;
    }
    
    //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    // Read and process .CSF blocks
    //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    count = 0;
    bytesread = 0;
    
    //skip header
    if (fseek(fptr,sizeof(Csf_Header),SEEK_SET) != 0)
    {
        log_push_error_point(0x505A);
        status = S_FAIL; 
        goto checksum_getcsffileinfo_done; 
    }
    
    while (!(feof(fptr)))
    {
        if (count >= checksumcount)
        {
            break;
        }
        
        // Read generic block info
        bytesread = fread((char*)block_info,1,sizeof(Csf_Block_Info),fptr);
        if (bytesread != sizeof(Csf_Block_Info))
        {
            log_push_error_point(0x505B);
            status = S_READFILE;
            goto checksum_getcsffileinfo_done;
        }

#if CSF_CHECKSUM_ENCRYPTION_ENABLED
        crypto_blowfish_decryptblock_critical((u8*)block_info,sizeof(Csf_Block_Info));
#endif

        block_type = block_info->type;
        
        // Handle blocks by checksum type
        switch (block_type)
        {
        case CRC32:
        case Hemicrc16_Block:
        case Level2_Sum_Block:
        case Equalizer_Sum_Block:
        case IPC:                       // Generic blocks
        case ETC:
            // Offset to data
            if (fseek(fptr,-sizeof(Csf_Block_Info),SEEK_CUR) != 0)
            {
                log_push_error_point(0x505C);
                status = S_FAIL; 
                goto checksum_getcsffileinfo_done; 
            }
            offset = sizeof(Csf_Block_Generic);
            break;
        case RSA_Signature_Block:       // RSA block
            offset = sizeof(Csf_Block_BlockReplacement_496) - 
                     sizeof(Csf_Block_Info);
            is_rsadata_found = TRUE;
            break;
        case Trans_Info:                // Trans-Info block
            offset = sizeof(Csf_Block_BlockReplacement_5632) - 
                     sizeof(Csf_Block_Info);
            break;
        default:                        // Invalid block type
            log_push_error_point(0x505D); 
            status = S_FAIL; 
            goto checksum_getcsffileinfo_done; 
            break;
        }

        //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
        // Process .CSF blocks
        //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++  
        
        // Save file offset
        file_position = ftell(fptr);
        status = checksum_csf_process_blocks(fptr, tune_fptr, ecm_type, ischeckonly,
                                             block_type, file_position, *block_info); 
        if (status != S_SUCCESS)
        {
            log_push_error_point(0x505E);
            status = S_BADCRCFAIL;
            goto checksum_getcsffileinfo_done;
        }
        // Restore file offset and offset for data block size
        if (fseek(fptr,file_position+offset,SEEK_SET) != 0)
        {
            log_push_error_point(0x505F);
            status = S_FAIL; 
            goto checksum_getcsffileinfo_done; 
        }
        count++;
    }
    
    if (count != checksumcount)
    {
        log_push_error_point(0x5060);
        status = S_BADCONTENT;
        goto checksum_getcsffileinfo_done;
    }
    if(ECM_IsRSAsignatureRequired(ecm_type))
    {
        if (IsVariantId_DCX_GPEC2 && (is_rsadata_found == FALSE))
        {
            log_push_error_point(0x506B);
            status = S_FAIL;
        }
    }
    
checksum_getcsffileinfo_done:
    if (fptr)
    {
        genfs_closefile(fptr);
    }
    if (block_info)
    {
        __free(block_info);
    }

    return status;  
}

//------------------------------------------------------------------------------
// Processes a block from a .CSF file 
// Inputs:  F_FILE *csf_fptr  (csf file)
//          F_FILE *file_fptr (stock.bin or flash.bin)
//          u16 ecm_type
// Return:  u8  status
// Engineer: Ricardo Cigarroa
//------------------------------------------------------------------------------
u8 checksum_csf_process_blocks(F_FILE *csf_fptr, F_FILE *file_fptr, u16 ecm_type, 
                               bool ischeckonly, u16 block_type, u32 address, 
                               Csf_Block_Info block_info)
{
    u8  status = S_SUCCESS;
       
    switch (block_type)
    {
    case Hemicrc16_Block:
        //TODO: handle bigendian
        status = checksum_csf_process_hemicrc16_block(block_info, file_fptr, 
                                                      ischeckonly);
        break;
    case Level2_Sum_Block:
        status = checksum_csf_process_level2_sum_block(block_info, file_fptr, 
                                                       ischeckonly);
        break;
    case Equalizer_Sum_Block:
        status = checksum_csf_process_equalizer_sum_block(block_info, file_fptr, 
                                                          ecm_type, ischeckonly);
        break;
    case ETC:
        status = checksum_csf_process_etc_block(block_info, file_fptr,
                                                ecm_type, ischeckonly);
        break;
    case RSA_Signature_Block:
        if (ischeckonly)
        {
            status = S_SUCCESS;
        }
        else
        {
            status = checksum_csf_process_rsa_signature_block(file_fptr, csf_fptr, 
                                                              ecm_type, ischeckonly, 
                                                              address);
        }
        break;
    case CRC32:
    case IPC:    
    case Trans_Info:
        //do nothing.
        status = S_SUCCESS; 
        break;
    default: 
        return S_FAIL;
     }
      
      if (status != S_SUCCESS)
      {
          log_push_error_point(0x5061);
          return S_BADCRCFAIL;   
      }
                                            
      return status; 
}

//------------------------------------------------------------------------------
// Process csf block: type (hemicrc16)
// Inputs:  Csf_Block *csfblock
//          F_FILE *file_fptr (stock.bin or flash.bin)
//          u8 flashtype (Ex. CMDIF_ACT_STOCK_FLASHER)
// Return:  u8  S_SUCCESS
// TODO:    handle this with goto...
// Engineer: Ricardo Cigarroa
//------------------------------------------------------------------------------
u8 checksum_csf_process_hemicrc16_block(Csf_Block_Info block_info, F_FILE *file_fptr, bool ischeckonly)
{
   u8  i;
   u8  status; 
   u8  temp_buf[8];
   u16 crc;
   u16 result_value;
   u32 number_of_segments;
   u32 temp_buflength;
   Segment_Info *segment_info;
   bool bigendian = FALSE; //TODO;
   
   filestock_set_position(block_info.starting_address);
   status = filestock_read(temp_buf,2,&temp_buflength);
   
   if (status != S_SUCCESS || temp_buflength != 2)
   {
       return S_READFILE;
   }
   
   crc = ((temp_buf[0]<<8) + (temp_buf[1]));
   
   filestock_set_position(block_info.starting_address + temp_buflength);
   status = filestock_read(temp_buf,8,&temp_buflength);
   
   if (status != S_SUCCESS || temp_buflength != 8)
   {
       return S_READFILE;
   }
   
   number_of_segments = ((temp_buf[4]<<24) + (temp_buf[5]<<16) + (temp_buf[6]<<8) + temp_buf[7]);
   
   segment_info = __malloc(number_of_segments * 8);
   if (segment_info == NULL)
   {
       return S_MALLOC;
   }
   
   for (i=0; i<number_of_segments; i++)
   {
       status = filestock_read(temp_buf,8,&temp_buflength);
       if (status != S_SUCCESS || temp_buflength != 8)
       {
           return S_READFILE;
       }
       segment_info[i].segment_block_start = ((temp_buf[0]<<24) + (temp_buf[1]<<16) + 
                                              (temp_buf[2]<<8) + temp_buf[3]);
      
       segment_info[i].segment_block_end = ((temp_buf[4]<<24) + (temp_buf[5]<<16) + 
                                             (temp_buf[6]<<8) + temp_buf[7]);   
   }
   
   for (i=0; i<number_of_segments; i++)
   {
       status = checksum_csf_calcualte_crc16(&crc, segment_info[i].segment_block_start, 
                                            segment_info[i].segment_block_end, bigendian);
       if (status != S_SUCCESS)
       {
           __free(segment_info);
           return S_FAIL;
       }
   }
   
   if (segment_info)
   {
       __free(segment_info);
   }

   filestock_set_position(block_info.location);
   status = filestock_read(temp_buf,8,&temp_buflength);
   
   if (status != S_SUCCESS || temp_buflength != 8)
   {
       return S_READFILE;
   }
            
   result_value = ((temp_buf[0]<<8) + temp_buf[1]);
   if (result_value != crc) 
   {
       if (!ischeckonly)
       {
          temp_buf[0] = ((crc>>8) & 0xFF);
          temp_buf[1] = (crc & 0xFF);
         
          status = filestock_updateflashfile(block_info.location,temp_buf,
                                             temp_buflength);
          if (status != S_SUCCESS)
          {
              return S_FAIL;
          }
      }
      else
      {
          return S_FAIL;
      }
   }

   return status;
}

//------------------------------------------------------------------------------
// Process csf block: type (level2_sum)
// Inputs:  Csf_Block *csfblock
//          F_FILE *file_fptr (stock.bin or flash.bin)
//          u8 flashtype (Ex. CMDIF_ACT_STOCK_FLASHER)
// Return:  u8  S_SUCCESS
// Engineer: Ricardo Cigarroa
//------------------------------------------------------------------------------
u8 checksum_csf_process_level2_sum_block(Csf_Block_Info block_info, 
                                         F_FILE *file_fptr, bool ischeckonly)
{ 
    u8 status;
    u8  temp_buf[8];
    u32 bytecount;
    u32 level2_sum;
    int level2_calc_cksum;
   
    checksum_csf_calcualte_level2_sum_block(&level2_calc_cksum, block_info, file_fptr);//TODO: big endian 
    
    filestock_set_position(block_info.location);
    status = filestock_read(temp_buf,4,&bytecount);
    if (status != S_SUCCESS || bytecount != 4)
    { 
        return S_READFILE;
    }
    
    level2_sum = (((temp_buf[0]<<24) | (temp_buf[1]<<16) | (temp_buf[2]<<8) | (temp_buf[3])) & 0xFFFFFFFF);
    
    if (level2_sum != level2_calc_cksum)
    {
        if (!ischeckonly)
        {
           //TODO: big endian
            temp_buf[0] = ((level2_calc_cksum>>24) & 0xFF);
            temp_buf[1] = ((level2_calc_cksum>>16) & 0xFF);
            temp_buf[2] = ((level2_calc_cksum>>8) & 0xFF);
            temp_buf[3] = (level2_calc_cksum & 0xFF);
          
            status = filestock_updateflashfile(block_info.location,temp_buf,bytecount);
            if (status != S_SUCCESS)
            {
                return S_FAIL;
            }
        }
        else
        {
            return S_FAIL; 
        }
    }
   
    return S_SUCCESS;
}

//------------------------------------------------------------------------------
// Process csf block: type (equalizer_sum)
// Inputs:  Csf_Block *csfblock
//          F_FILE *file_fptr (stock.bin or flash.bin)
//          u8 flashtype (Ex. CMDIF_ACT_STOCK_FLASHER)
// Return:  u8  S_SUCCESS
// Engineer: Ricardo Cigarroa
//------------------------------------------------------------------------------
u8 checksum_csf_process_equalizer_sum_block(Csf_Block_Info block_info, F_FILE *file_fptr, 
                                            u16 ecm_type, bool ischeckonly)
{ 
    u8 status; 
    u32 start_address;
    u32 end_address;
    
    if (!ischeckonly)
    {
        start_address = ECM_GetCalBlockAddr(ecm_type, 0);
        end_address = ECM_GetCalBlockSize(ecm_type, 0);
       
        status = checksum_csf_calculate_equalizer_sum(start_address, end_address, file_fptr, 
                                                      block_info);
        if (status != S_SUCCESS)
        {
            return S_FAIL; 
        }
    }
   
    return S_SUCCESS;
}

//------------------------------------------------------------------------------
// Calculate CRC16
// Inputs:  u16 *crc
//          u32 segment_start
//          u32 segment_end
//          bool bigendian
// Return:  u8  S_SUCCESS;
// Engineer: Ricardo Cigarroa
//------------------------------------------------------------------------------
u8 checksum_csf_calcualte_crc16(u16 *crc, u32 segment_start, u32 segment_end, 
                                bool bigendian)
{
    u8  *data;
    u8  flag = 0;
    u32 bytecount = 0;
    u16 crc_copy = 0;
    u16 new_value = 0;
    u16 data_word = 0;
    u32 read_length = 0;
    signed int length = 0,i;
    
    filestock_set_position(segment_start);
    
    length = segment_end - segment_start;
    crc_copy = *crc;
    
    data = __malloc(MAX_CSF_BLOCK_SIZE);
    if (data == NULL)
    {
        return S_MALLOC;
    }
    while (length > 0)
    {
        if (length >= MAX_CSF_BLOCK_SIZE)
        {
            read_length = MAX_CSF_BLOCK_SIZE;
        }
        else
        {
            read_length = length + 1;
            flag = 1;
        }
       
        memset((void*)data,0,MAX_CSF_BLOCK_SIZE);
        filestock_read(data,read_length,&bytecount);
        if (bytecount != read_length)
        {
            break;
        }
       
        if (flag == 1)
        {
            read_length--;
        }
       
        for (i=0; i<read_length; i=i+2)
        {
            data_word = (((data[i]<<8)|data[i+1]) & 0xFFFF);
            new_value =(u16) (data_word ^ crc_copy);

            crc_copy = (u16 )(crc_hh_nibble_table[((new_value>>12)&0x0F)]^
                              crc_hl_nibble_table[((new_value>>8)&0x0F)]^
                              crc_lh_nibble_table[((new_value>>4)&0x0F)]^
                              crc_ll_nibble_table[((new_value>>0) & 0x0F)]);   
        }
       
        length = length - read_length;    
    }
    
    *crc = crc_copy;
    
    if( data)
    {
        __free(data);
    }
    
    return S_SUCCESS;
}

/**
 *  @Process ETC additive checksum
 *  
 *  @param [out] checksum       Calculated checksum value
 *  @param [in] segment_start   Starting file offset (stock.bin or flash.bin)
 *  @param [in] length          Length of segment to checksum
 *
 *  @return status
 */
u8 checksum_csf_calcualte_etc_checksum(u32 *checksum, u32 segment_start, 
                                       u32 length)
{
    u8  *data;
    u32 bytecount;
    u32 read_length;
    u32 i;
    u8  status;

    if (checksum == NULL || length % 2)
    {
        return S_INPUT;
    }
    
    data = __malloc(MAX_CSF_BLOCK_SIZE);
    if(data == NULL)
    {
        return S_MALLOC;
    }
    
    *checksum = 0;    
    filestock_set_position(segment_start);
    
    while(length > 0)
    {
        if(length > MAX_CSF_BLOCK_SIZE)
        {
            read_length = MAX_CSF_BLOCK_SIZE;
        }
        else
        {
            read_length = length;
        }
       
        bytecount = 0;
        status = filestock_read(data, read_length, &bytecount);
        if(status != S_SUCCESS || bytecount != read_length)
        {
            status = S_READFILE;
            break;
        }
       
        for(i = 0; i < read_length; i += 2)
        {
            *checksum += ((data[i] << 8) | data[i+1]) & 0xFFFF;
        }
       
        length -= read_length;
    }
    
    if (data)
    {
        __free(data);
    }
    
    return status;
}

//------------------------------------------------------------------------------
// Calculate level 2 sum block
// Inputs:  int *level2_calc_cksum
//          Csf_Block *csfblock
//          F_FILE *file_fptr (stock.bin or flash.bin)
// Return:  u8  S_SUCCESS
// Engineer: Ricardo Cigarroa
//------------------------------------------------------------------------------
u8 checksum_csf_calcualte_level2_sum_block(int *level2_calc_cksum, 
                                           Csf_Block_Info block_info, 
                                           F_FILE *file_fptr)
{ 
    u8  *databuf;
    u8  *file_data;
    u16  data_word = 0;
    u32  block_end;
    u32  bytecount = 0;
    u32  read_length = 0;
    u32  length = 0,i;
    int  temp_cksum = 0; 
    
    block_end = block_info.starting_address + block_info.length; 
    length = (block_end - block_info.starting_address) + 1; 
    
    databuf= __malloc(MAX_CSF_BLOCK_SIZE);
    if (databuf == NULL)
    {
        return S_MALLOC;
    }
    
    filestock_set_position(block_info.starting_address);
    
    while (length > 0)
    {
        if (length > MAX_CSF_BLOCK_SIZE)
        {
            read_length = MAX_CSF_BLOCK_SIZE;
        }
        else
        {
            read_length = length;  
        }
       
        memset((void*)databuf,0,MAX_CSF_BLOCK_SIZE);
        filestock_read(databuf,read_length,&bytecount);
        if (bytecount <= 0)
        {
            break;
        }
       
        file_data = databuf;
        for (i=0; i< read_length; i=i+2)
        { 
            data_word = (((file_data[i]<<8)|file_data[i+1]) & 0xFFFF);
            temp_cksum += data_word;
        } 
       
        length = length - bytecount;
    }
    
    *level2_calc_cksum = temp_cksum;
    if (databuf)
    {
        __free(databuf);
    }
    
    return S_SUCCESS;       
}

//------------------------------------------------------------------------------
// Calculate equalizer sum block 
// Inputs:  u32 start_address
//          u32 end_address
//          F_FILE *file_fptr (stock.bin or flash.bin)
//          Csf_Block *csfblock
// Return:  u8  S_SUCCESS
// Engineer: Ricardo Cigarroa
//------------------------------------------------------------------------------
u8 checksum_csf_calculate_equalizer_sum(u32 start_address, u32 end_address, 
                                        F_FILE *file_fptr, Csf_Block_Info block_info)
{ 
    u8    status; 
    u8    *databuf;
    u8    *ptr;
    u8    cal_cksm = 0;
    u8    temp_buf[4] = {0,0,0,0};
    u32   x;
    u32   bytecount = 0;
    u32   temp_length = 0;
    u32   read_length = 0;
    int   tuned_cksum = 0;
    int   stock_cksum = 0;
    u8    actul_cksum = 0;
    F_FILE  *stkfptr = NULL;
    
    databuf = __malloc(MAX_CSF_BLOCK_SIZE);
    if (databuf == NULL)
    {
        return S_MALLOC;  
    }
    
    temp_length = end_address;
    filestock_set_position(start_address);
    
    while (temp_length > 0)
    {
        if (temp_length > MAX_CSF_BLOCK_SIZE)
        {
           read_length = MAX_CSF_BLOCK_SIZE;
        }
        else
        { 
           read_length = temp_length;
        }

        memset((void*)databuf,0,MAX_CSF_BLOCK_SIZE);
        filestock_read(databuf,read_length,&bytecount);
        ptr = databuf;

        for (x=0; x<bytecount; x++)
        {
           tuned_cksum += *ptr++;
        }

        temp_length = temp_length - read_length;  
    }
    
    if (file_fptr)
    {
        fclose(file_fptr);
    }
    
    status = filestock_openstockfile("r",FALSE);
    if (status != S_SUCCESS)
    {
        if (databuf)
        {
            __free(databuf);
        }
        return S_OPENFILE;
    }
   
    stkfptr = filestock_getflashfile_fptr();
    filestock_set_position(start_address);
    
    temp_length = end_address;
    
    while (temp_length > 0)
    {
        if (temp_length > MAX_CSF_BLOCK_SIZE)
        {
           read_length = MAX_CSF_BLOCK_SIZE;
        }
        else
        {
           read_length = temp_length;
        }

        memset((void*)databuf,0,MAX_CSF_BLOCK_SIZE);
        filestock_read(databuf,read_length,&bytecount);
        ptr = databuf;
       
        for (x=0; x<bytecount; x++)
        {
           stock_cksum += *ptr++;
        }

        temp_length = temp_length - read_length;
    }
    
    if (stkfptr)
    {
        fclose(stkfptr);
    }
    
    status = filestock_openflashfile("r+");
    if (status != S_SUCCESS)
    {
        if (databuf)
        {
            __free(databuf);
        }
        return S_OPENFILE;   
    }
    
    file_fptr = filestock_getflashfile_fptr();
    
    if (tuned_cksum <= stock_cksum)
    {
        cal_cksm = (u8)((stock_cksum - tuned_cksum) & 0xFF);
    }
    else
    {
        cal_cksm = (u8)((tuned_cksum - stock_cksum) & 0xFF);
    }

    if (cal_cksm != 0)
    {
        filestock_set_position(block_info.location);
        filestock_read(temp_buf,4,&bytecount);
        block_info.location = ((temp_buf[0]<<24) + (temp_buf[1]<<16) + (temp_buf[2]<<8) + temp_buf[3]);
        block_info.location++;

        filestock_set_position(block_info.location);
        filestock_read(temp_buf,1,&bytecount);
        actul_cksum = temp_buf[0];

        if(tuned_cksum <= stock_cksum)
        {
            actul_cksum = actul_cksum + cal_cksm;
        }
        else
        {
            actul_cksum = actul_cksum - cal_cksm;
        }
       
        temp_buf[0] = (actul_cksum & 0xFF);
        status = filestock_updateflashfile(block_info.location,temp_buf,1);
        if(status != S_SUCCESS)
        {
            __free(databuf);
            return S_FAIL;
        }    
    }

    /*file_fptr is to remain open at this point. It will be closed in a 
    higher level function*/
    if (databuf)
    {
        __free(databuf);
    }
    
    return S_SUCCESS;   
}

//------------------------------------------------------------------------------
// Process csf block: type (RSA signature block)
// Calculate the required bytes to modify the RSA signature block
// Inputs:  csf_Block *csfblock
//          F_FILE *file_fptr (stock.bin or flash.bin)
//          u16 ecm_type
//          bool ischeckonly
//          u32 current_address
// Return:  u8 S_SUCCESS 
// Engineer: Ricardo Cigarroa
//------------------------------------------------------------------------------
u8 checksum_csf_process_rsa_signature_block(F_FILE *file_fptr, F_FILE *csf_fptr, 
                                            u16 ecm_type, bool ischeckonly, 
                                            u32 address)
{
    u8 status = S_FAIL;
          
    status = checksum_csf_calculate_rsa_modified_bytes(file_fptr, ecm_type, 
                                                       csf_fptr, ischeckonly, 
                                                       address);
    return status; 
}

//------------------------------------------------------------------------------
// Calculate the required bytes to modify the RSA signature block
// Inputs:   csf_Block *csfblock
//           F_FILE *file_fptr (stock.bin or flash.bin)
//           u16 ecm_type
//           bool ischeckonly
// Return:   u8 S_SUCCESS 
// Engineer: Ricardo Cigarroa
//------------------------------------------------------------------------------
u8 checksum_csf_calculate_rsa_modified_bytes(F_FILE *file_fptr, u16 ecm_type, 
                                             F_FILE *csf_fptr, bool ischeckonly, 
                                             u32 address)
{
    u8  status = S_FAIL; 
    u8  rsa_modified_buffer[4];
    u8  buffer[256]; 
    u32 start_address = 0;
    u32 end_address = 0;
    u32 crc32e_calc = 0;
    u32 bytecount = 0; 
     
    F_FILE *stockfile_fptr = NULL; 
    Csf_Header *csfheader = NULL;
    
    
    csfheader = __malloc(sizeof(Csf_Header)); 
    if (csfheader == NULL)
    {
        log_push_error_point(0x5062);
        status = S_MALLOC;
        goto checksum_csf_calculate_rsa_modified_bytes_done; 
    }
    
    memset((void*)buffer,0,sizeof(buffer));
    memset((void*)rsa_modified_buffer,0,sizeof(rsa_modified_buffer));
    memset((void*)csfheader,0,sizeof(Csf_Header));

    //**************************************************************************
    //Calculate bytes to modify RSA block. 
    //************************************************************************** Rhonda here meme test only
    stockfile_fptr = genfs_user_openfile(STOCK_FILENAME,"r+");
    if (stockfile_fptr == NULL)
    {
        status = S_FAIL; 
        log_push_error_point(0x5063);
        goto checksum_csf_calculate_rsa_modified_bytes_done; 
    }
    start_address = ECM_GetOSBlockAddr(ecm_type,0);
    end_address = ((start_address)+ (ECM_GetOSBlockSize(ecm_type,0))); 
    
    //use stock file
    status = checksum_csf_calculate_ee2_crc32(stockfile_fptr, start_address, 
                                              end_address, rsa_modified_buffer,
                                              TRUE);
    if (status != S_SUCCESS)
    {
        log_push_error_point(0x5064);
        status = S_FAIL; 
        goto checksum_csf_calculate_rsa_modified_bytes_done;
    }
   
    status = checksum_csf_updatefile(csf_fptr,(address+2),
                                     rsa_modified_buffer,
                                     sizeof(rsa_modified_buffer));
    if (status != S_SUCCESS)
    {
        log_push_error_point(0x5065);
        status = S_FAIL; 
        goto checksum_csf_calculate_rsa_modified_bytes_done;
    }
    
    //recalculate content crc32 
    crc32e_reset();
    crc32e_calc = 0xFFFFFFFF; 
    bytecount = 0; 
    
    //skips the header and begins with the first csf block. 
    if (fseek(csf_fptr,sizeof(Csf_Header),SEEK_SET) != 0)
    {
        log_push_error_point(0x5066);
        status = S_FAIL;
        goto checksum_csf_calculate_rsa_modified_bytes_done;
    }
    
    while (!feof(csf_fptr))
    {
        bytecount = fread((char*)buffer,1,sizeof(buffer),csf_fptr);
        if (bytecount)
        {
            crc32e_calc = crc32e_calculateblock(crc32e_calc,(u32*)buffer,bytecount/4);
        }
    }
    
    /*Point to the beginning of the file of the .csf file.*/
    if (fseek(csf_fptr,0,SEEK_SET) != 0)
    {
        log_push_error_point(0x5067);
        status = S_FAIL;
        goto checksum_csf_calculate_rsa_modified_bytes_done;
    }
    
    /*Read the header to then recalculate the header and content crc32*/
    bytecount = fread((char*)csfheader,1,sizeof(Csf_Header),csf_fptr);
    if (bytecount)
    {       
        //decrypt csf header
#if CSF_CHECKSUM_ENCRYPTION_ENABLED
        crypto_blowfish_decryptblock_critical((u8*)csfheader,sizeof(Csf_Header));
#endif  
        //update csf header info (content crc32)
        csfheader->content_crc32e = crc32e_calc;
            
        //recalculate new header crc32  
        crc32e_calc = 0;
        crc32e_reset();
        csfheader->header_crc32e = 0xFFFFFFFF;
        
        crc32e_calc = crc32e_calculateblock(0xFFFFFFFF,(u32*)csfheader,
                                            sizeof(Csf_Header)/4);
        
        //update csf header info (header crc32)
        csfheader->header_crc32e = crc32e_calc;
        
        //write new header to csf file.     
        status = checksum_csf_updatefile(csf_fptr,0,
                                         (u8*)csfheader,sizeof(Csf_Header));
        if (status != S_SUCCESS)
        {
            log_push_error_point(0x5068);
            status = S_FAIL;
            goto checksum_csf_calculate_rsa_modified_bytes_done;
        }  
        if (stockfile_fptr)
        {
            genfs_closefile(stockfile_fptr);
        }
    }
        
    //**************************************************************************
    //Update flash file with crc
    //**************************************************************************
    start_address = ECM_GetCalBlockAddr(ecm_type,0);
    end_address = ((start_address)+ (ECM_GetCalBlockSize(ecm_type,0))); 
    
    //use flash file. 
    status = checksum_csf_calculate_ee2_crc32(file_fptr, start_address, 
                                              end_address, rsa_modified_buffer,
                                              FALSE);
    if(status != S_SUCCESS)
    {
        log_push_error_point(0x5069);
        status = S_FAIL;
        goto checksum_csf_calculate_rsa_modified_bytes_done;
    }
    
    status = filestock_updateflashfile(0x1C00A, rsa_modified_buffer, sizeof(rsa_modified_buffer));
    if(status != S_SUCCESS)
    {
        log_push_error_point(0x506A);
        status = S_FAIL; 
        goto checksum_csf_calculate_rsa_modified_bytes_done;
    }
    
  
checksum_csf_calculate_rsa_modified_bytes_done:
    if (csfheader)
    {
        __free(csfheader);
    }
    if (stockfile_fptr)
    {
        genfs_closefile(stockfile_fptr);
    }
    
    return status;
}

//------------------------------------------------------------------------------
// This function does the actual calculations for the modified rsa signature block. 
// Inputs:   F_FILE *file_fptr (stock.bin or flash.bin)
//           u32 start_address
//           u32 end_address
//           u8 *rsa_buffer
// Return:   u8 S_SUCCESS 
// Engineer: Ricardo Cigarroa
//------------------------------------------------------------------------------
u8 checksum_csf_calculate_ee2_crc32(F_FILE *file_fptr, u32 start_address, u32 end_address, 
                                    u8 *rsa_buffer, bool is_stock_key)
{
    u8  *databuffer;
    u32 filesize;
    u32 length;
    u32 bytecount; 
    u32 crc;
    
    if (file_fptr == NULL)
    {
        return S_FAIL;
    }
    
    databuffer = __malloc(0x800);
    if(databuffer == NULL)
    {
        //log point error
        return S_MALLOC;
    }   

    filesize = end_address - start_address; 
    if(is_stock_key == TRUE)
    {
        if (fseek(file_fptr,start_address,SEEK_SET) != 0)
        {
            //log point error
            __free(databuffer);
            return S_SEEKFILE; 
        }
    }
    else
    {
        filestock_set_position(start_address);
    }
    
    crc = 0xFFFFFFFF;
    
    while(filesize)
    {
        if(filesize < 0x800)
        {
            length = filesize; 
        }
        else
        {
            length = 0x800;
        }
        
        if(is_stock_key == TRUE)
        {
            file_read(databuffer,length,&bytecount,file_fptr,is_stock_key);
        }
        else
        {
            filestock_read(databuffer,length,&bytecount);
        }
        if(bytecount != length)
        {
            //log point error;
            __free(databuffer);
            return S_FAIL; 
        }
        crc = crc32_sw_calculateblock(crc, databuffer, length);
        filesize -= length; 
    }
    
    crc = crc ^ 0xFFFFFFFF;
    
    rsa_buffer[0] = (u8)((crc >> 24) & 0xFF);
    rsa_buffer[1] = (u8)((crc >> 16) & 0xFF);
    rsa_buffer[2] = (u8)((crc >> 8) & 0xFF);
    rsa_buffer[3] = (u8)(crc & 0xFF);
    
    if(databuffer)
    {
        __free(databuffer);
    }
    
    return S_SUCCESS;
}

//------------------------------------------------------------------------------
// Apply changes to CSF File
// Inputs:  F_FILE *csf_fptr
//          u32 position        (byte position)
//          u32 *data
//          u32 length          # of bytes to change
// Return:  u8  status
// Engineer: Ricardo Cigarroa
//------------------------------------------------------------------------------
u8 checksum_csf_updatefile(F_FILE *csf_fptr, u32 position, u8 *pdata, u32 length)
{
    u32 total;
    u32 count;
    u32 slotposition;
    u8  buffer[8];
    u32 bufferlength;
    u8 status;
    u8 data[128]; 

    if (csf_fptr == NULL)
    {
        return S_BADCONTENT;
    }
    
    if(length > 128) 
    {
        return S_INPUT;     // datalength too big
    }
    
    memcpy(data,pdata,length);
    
    total = 0;
    slotposition = (position/8)*8;  //closest left position in multiple of 8

    if (fseek(csf_fptr,slotposition,SEEK_SET) != 0)
    {
        status = S_SEEKFILE;
        goto checksum_csf_updatefile_done;
    }
    if (slotposition == position)   //position is in alignment
    {
        u32 slotlength;

        slotlength = (length/8)*8;
#if CSF_CHECKSUM_ENCRYPTION_ENABLED
        crypto_blowfish_encryptblock_critical(data,slotlength);
#endif
        total = fwrite(data,1,slotlength,csf_fptr);
        if (total != slotlength)
        {
            status = S_WRITEFILE;
            goto checksum_csf_updatefile_done;
        }
        if (total == length)
        {
            //do nothing
        }
        else if (total < length)    //tail is out of aligment
        {
            bufferlength = fread(buffer,1,sizeof(buffer),csf_fptr);
            if (bufferlength)
            {
                crypto_blowfish_decryptblock_critical(buffer,bufferlength);
                memcpy(buffer,&data[total],length-total);

                if (fseek(csf_fptr,position+total,SEEK_SET) != 0)
                {
                    status = S_SEEKFILE;
                    goto checksum_csf_updatefile_done;
                }
#if CSF_CHECKSUM_ENCRYPTION_ENABLED
                crypto_blowfish_encryptblock_critical(buffer,bufferlength);
#endif
                count = fwrite(buffer,1,bufferlength,csf_fptr);
                if (count != bufferlength)
                {
                    status = S_WRITEFILE;
                    goto checksum_csf_updatefile_done;
                }
                total = length;

                if (fseek(csf_fptr,position+length,SEEK_SET) != 0)
                {
                    status = S_SEEKFILE;
                    goto checksum_csf_updatefile_done;
                }
            }
            else
            {
                //only allow to update existing data
                status = S_READFILE;
                goto checksum_csf_updatefile_done;
            }
        }//elseif (slotlength < length)...
        else
        {
            //should never happen
            status = S_ERROR;
            goto checksum_csf_updatefile_done;
        }
    }//if (slotposition == position)...
    else if (slotposition < position)
    {
        u32 startoffset;
        u32 midlength;
        u32 tmpu32;

        //head is out of alignment
        startoffset = position - slotposition;
        bufferlength = fread(buffer,1,sizeof(buffer),csf_fptr);
        crypto_blowfish_decryptblock_critical(buffer,bufferlength);
        tmpu32 = bufferlength-startoffset;
        if (length <= tmpu32)
        {
            //update data is very small
            tmpu32 = length;
        }
        memcpy(&buffer[startoffset],data,tmpu32);   //update new data
        
        if (fseek(csf_fptr,slotposition,SEEK_SET) != 0)
        {
            status = S_SEEKFILE;
            goto checksum_csf_updatefile_done;
        }
#if CSF_CHECKSUM_ENCRYPTION_ENABLED
        crypto_blowfish_encryptblock_critical(buffer,bufferlength);
#endif
        count = fwrite(buffer,1,bufferlength,csf_fptr);
        if (count != bufferlength)
        {
            status = S_WRITEFILE;
            goto checksum_csf_updatefile_done;
        }
        total += tmpu32;

        midlength = ((length-total)/8)*8;
#if CSF_CHECKSUM_ENCRYPTION_ENABLED
        crypto_blowfish_encryptblock_critical(&data[total],midlength);
#endif
        count = fwrite(&data[total],1,midlength,csf_fptr);
        if (count != midlength)
        {
            status = S_WRITEFILE;
            goto checksum_csf_updatefile_done;
        }
        total += midlength;

        if (total == length)
        {
            //do nothing
        }
        else if (total < length)   //tail is out of alignment
        {
            bufferlength = fread(buffer,1,sizeof(buffer),csf_fptr);
            crypto_blowfish_decryptblock_critical(buffer,bufferlength);
            memcpy(buffer,&data[total],length-total);

            if (fseek(csf_fptr,position+total,SEEK_SET) != 0)
            {
                status = S_SEEKFILE;
                goto checksum_csf_updatefile_done;
            }
#if CSF_CHECKSUM_ENCRYPTION_ENABLED
            crypto_blowfish_encryptblock_critical(buffer,bufferlength);
#endif
            count = fwrite(buffer,1,bufferlength,csf_fptr);
            if (count != bufferlength)
            {
                status = S_WRITEFILE;
                goto checksum_csf_updatefile_done;
            }
            if (fseek(csf_fptr,position+length,SEEK_SET) != 0)
            {
                status = S_SEEKFILE;
                goto checksum_csf_updatefile_done;
            }
            total = length;
        }
        else
        {
            //should never happen
            status = S_ERROR;
            goto checksum_csf_updatefile_done;
        }
    }//else if (slotposition < position)...
    else
    {
        //should never happen
        status = S_ERROR;
        goto checksum_csf_updatefile_done;
    }
    if (total == length)
    {
        status = S_SUCCESS;
    }

checksum_csf_updatefile_done:
    return status;
}

/**
 *  @Process ETC block
 *  
 *  @param [in] csfblock_generic    CSF data block
 *  @param [in] file_fptr           Pointer to stock.bin or flash.bin
 *  @param [in] ischeckonly         Check only?
 *
 *  @return Status
 */
u8 checksum_csf_process_etc_block(Csf_Block_Info block_info, F_FILE *file_fptr, 
                                  u16 ecm_type, bool ischeckonly)
{
    u32 calc_checksum;
    u32 checksum;
    u32 bytecount;
    u8 buffer[4];
    u8 status;
    
    filestock_set_position(block_info.location);
    status = filestock_read(buffer,4,&bytecount);
    if (status != S_SUCCESS || bytecount != 4)
    {
        status = S_READFILE;
    }
    else
    {
        checksum = (buffer[0] << 24) | (buffer[1] << 16) | (buffer[2] << 8) | buffer[3];
    }
    
    if (status == S_SUCCESS)
    {
        status = checksum_csf_calcualte_etc_checksum(&calc_checksum, 
                                                     block_info.starting_address, 
                                                     block_info.length);
        if (status == S_SUCCESS)
        {
            /* ETC additive checksum is stored as 2's complement + 1 */
            calc_checksum = ~calc_checksum + 1;
        }
    }
    
    /* Check / Update Checksum */
    if (status == S_SUCCESS && checksum != calc_checksum)
    {
        if (ischeckonly == FALSE)
        {
            if (ECM_IsLittleEndian(ecm_type))
            {
                buffer[3] = calc_checksum >> 24;
                buffer[2] = calc_checksum >> 16;
                buffer[1] = calc_checksum >> 8;
                buffer[0] = calc_checksum;
            }
            else
            {
                /*Big endian*/
                buffer[0] = calc_checksum >> 24;
                buffer[1] = calc_checksum >> 16;
                buffer[2] = calc_checksum >> 8;
                buffer[3] = calc_checksum;
            }
            
            status = filestock_updateflashfile(block_info.location,buffer,4);
        }
        else
        {
            status = S_FAIL;
        }
    }
    
    if (status != S_SUCCESS)
    {
        status = S_FAIL;
    }

   return status;
}
