/**
  **************** (C) COPYRIGHT 2011 SCT Performance, LLC *********************
  * File Name          : cmdif_func_flasher.c
  * Device / System    : SCT products using the "CommonCode" platform
  * Author             : Quyen Leba
  *
  * Version            : 1 
  * Date               : 05/02/2011
  * Description        : 
  *                    : 
  *
  *
  * History            : 05/02/2011 M. Davis
  *                    :   Add SCT Copyright header,
  *
  ******************************************************************************
  */

//------------------------------------------------------------------------------
// Log error point: 0x48xx -> 0x48FF
//------------------------------------------------------------------------------

#include <device_config.h>
#include <fs/genfs.h>
#include <common/statuscode.h>
#include <common/settings.h>
#include <common/version.h>
#include <common/checksum.h>
#include <common/crypto_blowfish.h>
#include <common/file.h>
#include <common/filectb.h>
#include <common/filestock.h>
#include <common/filestock_option.h>
#include <common/genmanuf_overload.h>
#include <common/log.h>
#include <common/obd2tune.h>
#include <common/cmdif.h>
#include <common/obd2tune_upload.h>
#include <common/vehicleinfolog.h>
#include "cmdif_func_flasher.h"

//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
extern u8 cmdif_scratchpad[1024];                   /* from cmdif.c */
extern u16 cmdif_scratchpad_length;                 /* from cmdif.c */
extern cmdif_datainfo cmdif_datainfo_responsedata;  /* from cmdif.c */
extern flasher_info *flasherinfo;                   /* from obd2tune.c */

//------------------------------------------------------------------------------
// Check flash status (DownloadFailed, married, stock exists, etc)
// Input:   u8  flash_type (from CMDIF_ACTION)
//          u8  *tunefileinfo ("stock.bin" or "recvstock.bin")
//              (only valid for flash_type CMDIF_ACT_STOCK_FLASHER)
// Return:  u8  status
// Engineer: Quyen Leba
//------------------------------------------------------------------------------
u8 cmdif_func_flasher_init_check(u8 flash_type, u8 *tunefileinfo)
{
    u8 *bptr;
    u8 status;
    F_FILE  *stockfile = NULL;
    
    status = obd2tune_flasherinfo_init();
    if (status != S_SUCCESS)
    {
        log_push_error_point(0x4800);
        cmdif_datainfo_responsedata.responsecode = CMDIF_ACK_FAILED;
        return status;
    }
    
    status = vehdef_check_vdf();
    if (status != S_SUCCESS)
    {
        log_push_error_point(0x4817);
        cmdif_datainfo_responsedata.responsecode = CMDIF_ACK_TUNEREVISION_OUTOFDATE;
        return status;
    }

    flasherinfo->flashtype = flash_type;

    if (flash_type == CMDIF_ACT_PRELOADED_FLASHER ||
        flash_type == CMDIF_ACT_CUSTOM_FLASHER)
    {
        bool istunerevisionold = obd2tune_is_plfdatecode_old();

        if (SETTINGS_IsNoPreloadedDevice() && flash_type == CMDIF_ACT_PRELOADED_FLASHER)
        {
            //not support preloaded tune
            log_push_error_point(0x4854);
            cmdif_datainfo_responsedata.responsecode = CMDIF_ACK_NOT_SUPPORTED;
        }
        else if (SETTINGS_IsPreloadedTuneDisabled() && flash_type == CMDIF_ACT_PRELOADED_FLASHER)
        {
            //preloaded tune temporarily disabled
            log_push_error_point(0x4864);
            cmdif_datainfo_responsedata.responsecode = CMDIF_ACK_NOT_SUPPORTED;
        }
        else if (SETTINGS_IsNoCustomDevice() && flash_type == CMDIF_ACT_CUSTOM_FLASHER)
        {
            //not support custom tune
            log_push_error_point(0x4855);   //ok
            cmdif_datainfo_responsedata.responsecode = CMDIF_ACK_NOT_SUPPORTED;
        }
        else if (SETTINGS_Is50StateLegalDevice() && flash_type == CMDIF_ACT_CUSTOM_FLASHER)
        {
            //not support custom tune
            log_push_error_point(0x4855);   //ok
            cmdif_datainfo_responsedata.responsecode = CMDIF_ACK_NOT_SUPPORTED;
        }
        else if (istunerevisionold && flash_type == CMDIF_ACT_PRELOADED_FLASHER)
        {
            //tune revision is old
            log_push_error_point(0x4801);
            cmdif_datainfo_responsedata.responsecode = CMDIF_ACK_TUNEREVISION_OUTOFDATE;
        }
        else if (SETTINGS_IsMarried())
        {
            if(SETTINGS_IsDemoMode())
            {
                status = S_SUCCESS;
                cmdif_datainfo_responsedata.responsecode = CMDIF_ACK_OK;
            }
            else
            {
                stockfile = genfs_user_openfile(STOCK_FILENAME,"r");
                if (obd2_validate_ecmbinarytype(SETTINGS_TUNE(veh_type)) != S_SUCCESS)
                {
                    log_push_error_point(0x4852);
                    cmdif_datainfo_responsedata.responsecode = CMDIF_ACK_INVALID_BINARYTYPE;
                }
                else
                {
                    if(stockfile != NULL)
                    {
                        genfs_closefile(stockfile);
                        if (SETTINGS_IsDownloadFail())
                        {
                            //there was a download failure
                            if (SETTINGS_IsRecoveryStockSet())
                            {
                                cmdif_datainfo_responsedata.responsecode = CMDIF_ACK_RECOVERY_STOCK;
                            }
                            else
                            {
                                log_push_error_point(0x4802);
                                cmdif_datainfo_responsedata.responsecode = CMDIF_ACK_DOWNLOAD_FAILED;
                            }
                        }
                        else
                        {
                            cmdif_datainfo_responsedata.responsecode = CMDIF_ACK_OK;
                        }
                    }
                    else
                    {
                        //device is married, but there's no stock
                        log_push_error_point(0x4803);
                        cmdif_datainfo_responsedata.responsecode = CMDIF_ACK_STOCK_NOT_FOUND;
                    }
                }
            }
        }
        else
        {
            flasherinfo->uploadstock_required = TRUE;
            cmdif_datainfo_responsedata.responsecode = CMDIF_ACK_OK;
        }
    }
    else if (flash_type == CMDIF_ACT_STOCK_FLASHER)
    {
        if (SETTINGS_IsMarried())
        {
            if (SETTINGS_IsDemoMode())
            {
                status = S_SUCCESS;
                cmdif_datainfo_responsedata.responsecode = CMDIF_ACK_OK;
            }
            else
            {
                stockfile = genfs_user_openfile(STOCK_FILENAME,"r");
                if(stockfile != NULL)
                {
                    genfs_closefile(stockfile);
                    if (obd2_validate_ecmbinarytype(SETTINGS_TUNE(veh_type)) != S_SUCCESS)
                    {
                        log_push_error_point(0x4853);
                        cmdif_datainfo_responsedata.responsecode = CMDIF_ACK_INVALID_BINARYTYPE;
                    }
                    else
                    {
                        if (SETTINGS_IsStockRecovery())
                        {
                            flasherinfo->flags |= FLASHERFLAG_RETURNSTOCK_RECOVERY;
                        }

                        cmdif_datainfo_responsedata.responsecode = CMDIF_ACK_FAILED;

                        status = filestock_validate(FILESTOCK_USE_STOCKFILE,NULL); 
                        if (status == S_SUCCESS)
                        {
                            status = filestock_option_validate(NULL);
                            if(status != S_SUCCESS)
                            {
                                cmdif_datainfo_responsedata.responsecode = CMDIF_ACK_STOCK_SPEC_OPTS_BAD;
                                log_push_error_point(0x489C); // Stock Special Options file is corrupt or missing
                                //removed duplicate error point
                                //log_push_error_point(0x485F);
                            }
                        }
                        else
                        {
                            cmdif_datainfo_responsedata.responsecode = CMDIF_ACK_STOCK_CORRUPTED;
                            log_push_error_point(0x485C);
                        }

                        if (status == S_SUCCESS)
                        {
                            cmdif_datainfo_responsedata.responsecode = CMDIF_ACK_OK;
                        }
                        else
                        {
                            log_push_error_point(0x485D);
                        }
                    }
                }
                else
                {
                    //device is married, but there's no stock
                    log_push_error_point(0x4804);
                    cmdif_datainfo_responsedata.responsecode = CMDIF_ACK_STOCK_NOT_FOUND;
                }
            }
        }
        else
        {
            //already returned to stock
            cmdif_datainfo_responsedata.responsecode = CMDIF_ACK_NOT_MARRIED;
        }
    }
    else if (flash_type == CMDIF_ACT_UPLOAD_STOCK)
    {
        if (SUPPORT_UPLOAD_SPECIAL_STOCK)
        {
            if (SETTINGS_IsMarried())
            {
                //cannot use this function if device is married
                log_push_error_point(0x4805);
                cmdif_datainfo_responsedata.responsecode = CMDIF_ACK_FAILED;
            }
            else
            {
                flasherinfo->uploadstock_required = TRUE;
                flasherinfo->isupload_only = TRUE;
                cmdif_datainfo_responsedata.responsecode = CMDIF_ACK_OK;
            }
        }
        else
        {
            //does not support this feature
            log_push_error_point(0x4806);
            cmdif_datainfo_responsedata.responsecode = CMDIF_ACK_NOT_SUPPORTED;
        }
    }
    else
    {
        flasherinfo->flashtype = CMDIF_ACT_INVALID;
        log_push_error_point(0x4807);
        cmdif_datainfo_responsedata.responsecode = CMDIF_ACK_NOT_SUPPORTED;
    }
    
    if (cmdif_datainfo_responsedata.responsecode == CMDIF_ACK_OK)
    {
        bptr = __malloc(8);
        if (bptr == NULL)
        {
            log_push_error_point(0x4808);
            cmdif_datainfo_responsedata.responsecode = CMDIF_ACK_FAILED;
            return S_MALLOC;
        }
        
        //data:
        //[2:reserved][2:Mico core version]
        //[2:min_veh_type][2:max_veh_type]
        cmdif_datainfo_responsedata.dataptr = bptr;
        cmdif_datainfo_responsedata.datalength = 8;
        bptr[0] = 0x00;
        bptr[1] = 0x00;
        bptr[2] = (u8)(MICO_CORE_VERSION & 0xFF);
        bptr[3] = (u8)(MICO_CORE_VERSION >> 8);
        bptr[4] = (u8)(VEH_GetMinVehType() & 0xFF);
        bptr[5] = (u8)(VEH_GetMinVehType() >> 8);
        bptr[6] = (u8)(VEH_GetMaxVehType() & 0xFF);
        bptr[7] = (u8)(VEH_GetMaxVehType() >> 8);
    }
    else
    {   
        status = obd2tune_flasherinfo_deinit();
    }

    return S_SUCCESS;
}

//------------------------------------------------------------------------------
// Return:  u8  status
//------------------------------------------------------------------------------
u8 cmdif_func_flasher_deinit_check()
{
    return obd2tune_flasherinfo_deinit();
}

//------------------------------------------------------------------------------
// Return:  u8  status
//------------------------------------------------------------------------------
u8 cmdif_func_flasher_get_strategy()
{
    u8 *bptr;
    u8 status;
    ecm_info ecm;
    VehicleCommType vehiclecommtype;
    u8 codes[40];
    u8 length;
    u8 i;

    cmdif_datainfo_responsedata.responsecode = CMDIF_ACK_FAILED;

    codes[0] = NULL;
    ecm.vin[0] = NULL;
    vehiclecommtype = CommType_Unknown;
    status = obd2_readecminfo(&ecm, &vehiclecommtype);
    if (status == S_SUCCESS && ecm.ecm_count > 0)
    {
#if SUPPORT_COMBINED_STRATEGY_FOR_6_0L
        if (ecm.ecm_count == 2 &&
            ecm.codes[0][0][0] == 'V' && ecm.codes[0][0][1] == 'X')
        {
            // 6.0L combine strategy function
            // Load strategies in buffer like this:
            // First load digits 2-4 of eng strategy ex. VX-AMO-12
            // Then load digits 2-4 of trans strategy ex. TQ-AQ3-12
            // End up with six digit strat like "AM0AQ3*" AM0(eng) AQ3(tran)
            memcpy((char*)&codes[0],(char*)&ecm.codes[0][0][2],3);
            memcpy((char*)&codes[3],(char*)&ecm.codes[1][0][2],3);
            codes[6] = TUNECODE_SEPARATOR;
            codes[7] = NULL;
        }
        else
        {
            for(i=0;i<ecm.ecm_count;i++)
            {
                if (ecm.codecount[i] > 0)
                {
                    strcat((char*)codes,(char*)ecm.codes[i][0]);
                    strcat((char*)codes,(char*)TUNECODE_SEPARATOR_STRING);
                }
            }
        }
#else
        for(i=0;i<ecm.ecm_count;i++)
        {
            if (ecm.codecount[i] > 0)
            {
                strcat((char*)codes,(char*)ecm.codes[i][0]);
                strcat((char*)codes,(char*)TUNECODE_SEPARATOR_STRING);
            }
        }
#endif
        if (codes[0] == NULL)
        {
            log_push_error_point(0x480D);
            cmdif_datainfo_responsedata.responsecode = CMDIF_ACK_READ_STRATEGY_FAILED;
            goto cmdif_func_flasher_get_strategy_done;
        }

        length = strlen((char*)codes);
        
        bptr = __malloc(length+1);
        if (bptr)
        {
            status = S_SUCCESS;
            
            strcpy((char*)bptr,(char*)codes);
            cmdif_datainfo_responsedata.dataptr = bptr;
            cmdif_datainfo_responsedata.datalength = length + 1;
            cmdif_datainfo_responsedata.responsecode = CMDIF_ACK_OK;
        }
        else
        {
            log_push_error_point(0x480E);
            status = S_MALLOC;
            cmdif_datainfo_responsedata.responsecode = CMDIF_ACK_FAILED;
        }
    }
    else
    {
        cmdif_datainfo_responsedata.responsecode = CMDIF_ACK_READ_STRATEGY_FAILED;
    }

cmdif_func_flasher_get_strategy_done:
    return status;
}

//------------------------------------------------------------------------------
// Return:  u8  status
//------------------------------------------------------------------------------
u8 cmdif_func_flasher_get_strategy_flasher()
{
    u8 *bptr;
    u8 status;
    ecm_info ecm;
    VehicleCommType vehiclecommtype;
    u8 codes[128];
    u8 length;
    u8 i;


    cmdif_datainfo_responsedata.responsecode = CMDIF_ACK_FAILED;
    if (flasherinfo == NULL)
    {
        log_push_error_point(0x4809);
        return S_BADCONTENT;
    }

    codes[0] = NULL;
    ecm.vin[0] = NULL;
    vehiclecommtype = CommType_Unknown;
    
    status = obd2_readecminfo(&ecm, &vehiclecommtype);
    if (status == S_SUCCESS && ecm.ecm_count > 0)
    {
#if SUPPORT_COMBINED_STRATEGY_FOR_6_0L
        if (ecm.ecm_count == 2 &&
            ecm.codes[0][0][0] == 'V' && ecm.codes[0][0][1] == 'X')
        {
            // 6.0L combine strategy function
            // Load strategies in buffer like this:
            // First load digits 2-4 of eng strategy ex. VX-AMO-12
            // Then load digits 2-4 of trans strategy ex. TQ-AQ3-12
            // End up with six digit strat like "AM0AQ3*" AM0(eng) AQ3(tran)
            memcpy((char*)&codes[0],(char*)&ecm.codes[0][0][2],3);
            memcpy((char*)&codes[3],(char*)&ecm.codes[1][0][2],3);
            codes[6] = TUNECODE_SEPARATOR;
            codes[7] = NULL;
        }
        else
        {
            for(i=0;i<ecm.ecm_count;i++)
            {
                if (ecm.codecount[i] > 0)
                {
                    strcat((char*)codes,(char*)ecm.codes[i][0]);
                    strcat((char*)codes,(char*)TUNECODE_SEPARATOR_STRING);
                }
            }
        }
#else
        for(i=0;i<ecm.ecm_count;i++)
        {
            if (ecm.codecount[i] > 0)
            {
                strcat((char*)codes,(char*)ecm.codes[i][0]);
                strcat((char*)codes,(char*)TUNECODE_SEPARATOR_STRING);
            }
        }
#endif

        //if married, use vehiclecodes saved in settings
        if (SETTINGS_IsMarried())
        {
            if (!SETTINGS_IsDemoMode())
            {
                //veh_type unknown at this point so just use settings, married check
                //will be tested again right before proramming. This check is just
                //an early error detection.
                flasherinfo->vehicle_type = SETTINGS_TUNE(veh_type);    //this is required
                status = obd2tune_checkmarried(flasherinfo->vehicle_type,
                                               ecm.vin,
                                               ecm.vehicle_serial_number);
                if (status != S_SUCCESS)
                {
                    log_push_error_point(0x480F);
                    cmdif_datainfo_responsedata.responsecode = CMDIF_ACK_MARRIED_FAIL;
                    goto cmdif_func_flasher_get_strategy_flasher_done;
                }

            }
            //Note: if saved vehiclecodes is invalid, use codes just read from
            //vehicle. This also allows devices w/ older fw & didn't save this
            //info to work
            //Note: custom tune don't care about this info, so always overwrite
            //with preloaded tune vehiclecodes
            if (obd2tune_validate_vehiclecodes(SETTINGS_TUNE(vehiclecodes)) == S_SUCCESS)
            {
                memcpy((char*)codes,
                       (char*)SETTINGS_TUNE(vehiclecodes),
                       sizeof(SETTINGS_TUNE(vehiclecodes)));
            }

        }

        if (codes[0] == NULL)
        {
            log_push_error_point(0x480A);
            cmdif_datainfo_responsedata.responsecode = CMDIF_ACK_READ_STRATEGY_FAILED;
            goto cmdif_func_flasher_get_strategy_flasher_done;
        }

        length = strlen((char*)codes);
        
        bptr = __malloc(length+1);
        if (bptr)
        {
            status = S_SUCCESS;
            
            strcpy((char*)bptr,(char*)codes);
            cmdif_datainfo_responsedata.dataptr = bptr;
            cmdif_datainfo_responsedata.datalength = length + 1;
            cmdif_datainfo_responsedata.responsecode = CMDIF_ACK_OK;
            
            if (flasherinfo)
            {
                memcpy((char*)flasherinfo->vin,(char*)ecm.vin,VIN_LENGTH);
                flasherinfo->vin[VIN_LENGTH] = NULL;
                
                memcpy((char*)flasherinfo->vehicle_serial_number,
                       (char*)ecm.vehicle_serial_number,
                       sizeof(flasherinfo->vehicle_serial_number));
            }
        }
        else
        {
            log_push_error_point(0x480B);
            status = S_MALLOC;
            cmdif_datainfo_responsedata.responsecode = CMDIF_ACK_FAILED;
        }
    }
    else
    {
        cmdif_datainfo_responsedata.responsecode = CMDIF_ACK_READ_STRATEGY_FAILED;
    }

cmdif_func_flasher_get_strategy_flasher_done:
    if (status == S_SUCCESS && flasherinfo)
    {
        flasherinfo->flags |= FLASHERFLAG_VALID_VEHICLE_INFO;
        memcpy((char*)&flasherinfo->ecminfo,(char*)&ecm,sizeof(ecm_info));
        vehicleinfolog_add_block(&flasherinfo->ecminfo);
    }
    return status;
}

//------------------------------------------------------------------------------
// Input:   u8  *tunecode (must be NULL terminated)
// Return:  u8  status
// Engineer: Patrick Downs, Quyen Leba
//------------------------------------------------------------------------------
u8 cmdif_func_flasher_get_tune_list_info(u8 *tunecode)
{
    u8  final_tunecode_count;
    u8  *bptr;
    u8  i;
    u16 id;
    u32 length;
    u32 count;
    u8  status;
    u32 checksum;

    cmdif_datainfo_responsedata.responsecode = CMDIF_ACK_FAILED;
    if (flasherinfo == NULL)
    {
        log_push_error_point(0x481D);
        return S_BADCONTENT;
    }

    cmdif_scratchpad_length = 0;
    length = strlen((char*)tunecode);
    if (length < MIN_TUNECODE_LENTH || length >= MAX_TUNECODE_LENTH)
    {
        cmdif_datainfo_responsedata.responsecode = CMDIF_ACK_FAILED;
    }
    else
    {
        status = obd2tune_gettunecodelistinfo(tunecode,flasherinfo->vin,
                                              &flasherinfo->tunecodeinfolist);
        if (status == S_NOLOOKUP)
        {
            log_push_error_point(0x4810);
            cmdif_datainfo_responsedata.responsecode = CMDIF_ACK_NO_LOOKUP;
        }
        else if (status == S_NOTSUPPORT)
        {
            log_push_error_point(0x4811);
            cmdif_datainfo_responsedata.responsecode = CMDIF_ACK_NOT_SUPPORTED;
        }
        else if (status == S_SERIALUNMATCH)
        {
            log_push_error_point(0x4812);
            cmdif_datainfo_responsedata.responsecode = CMDIF_ACK_SERIAL_UNMATCH;
        }
        else if (status == S_UNMATCH)
        {
            log_push_error_point(0x4813);
            cmdif_datainfo_responsedata.responsecode = CMDIF_ACK_FILE_NOT_MATCH;
        }
        else if (status == S_FILENOTFOUND)
        {
            log_push_error_point(0x485E);
            cmdif_datainfo_responsedata.responsecode = CMDIF_ACK_TUNE_NOT_FOUND;
        }
        else if (status == S_STRATNOTFOUND ||
                 flasherinfo->tunecodeinfolist.count == 0)
        {
            if (flasherinfo->ecm_count > 0)
            {
                u16 prev_veh_index;

                prev_veh_index = flasherinfo->selected_tunelistinfo.tuneindex[flasherinfo->ecm_count-1];
                if (VEH_IsAllowSkipUnsupportedSubECM(prev_veh_index))
                {
                    cmdif_datainfo_responsedata.responsecode = CMDIF_ACK_SKIP;
                }
                else
                {
                    log_push_error_point(0x4814);
                    cmdif_datainfo_responsedata.responsecode = CMDIF_ACK_VEHICLE_UNSUPPORTED;
                }
            }
            else
            {
                log_push_error_point(0x4815);
                cmdif_datainfo_responsedata.responsecode = CMDIF_ACK_VEHICLE_UNSUPPORTED;
            }
        }
        else if (status != S_SUCCESS)
        {
            log_push_error_point(0x4816);
            cmdif_datainfo_responsedata.responsecode = CMDIF_ACK_FAILED;
        }
        else
        {
            id = TUNENAMEID_START;
            length = 0;
            final_tunecode_count = 0;
            for(i=0;i<flasherinfo->tunecodeinfolist.count;i++)
            {
                count = strlen((char*)flasherinfo->tunecodeinfolist.description[i]) + 1;
                if ((length + count + 2) > sizeof(cmdif_scratchpad))
                {
                    //scratch buffer cannot get any more data
                    break;
                }
                cmdif_scratchpad[length++] = id;
                cmdif_scratchpad[length++] = id >> 8;
                strcpy((char*)&cmdif_scratchpad[length],
                       (char*)flasherinfo->tunecodeinfolist.description[i]);
                length += count;
                id++;
                final_tunecode_count++;
            }
            
            checksum = checksum_simpleadditive(cmdif_scratchpad,length);
            bptr = __malloc(9);
            if (bptr)
            {
                bptr[0] = final_tunecode_count;
                bptr[1] = length;
                bptr[2] = length>>8;
                bptr[3] = length>>16;
                bptr[4] = length>>24;
                bptr[5] = checksum;
                bptr[6] = checksum>>8;
                bptr[7] = checksum>>16;
                bptr[8] = checksum>>24;
                cmdif_datainfo_responsedata.dataptr = bptr;
                cmdif_datainfo_responsedata.datalength = 9;
                cmdif_datainfo_responsedata.responsecode = CMDIF_ACK_OK;
                cmdif_scratchpad_length = length;
            }
            else
            {
                log_push_error_point(0x481E);
                cmdif_datainfo_responsedata.responsecode = CMDIF_ACK_FAILED;
                return S_MALLOC;
            }
        }
    }

    return S_SUCCESS;
}

//------------------------------------------------------------------------------
// Input:   u8  *tunecode (must be NULL terminated)
// Return:  u8  status
// Engineer: Patrick Downs, Quyen Leba
// Note: new method
//------------------------------------------------------------------------------
u8 cmdif_func_flasher_get_tune_list_info_complete(u8 *tunecode)
{
    u8  final_tunecode_count;
    u8  *bptr;
    u8  i;
    u16 id;
    u32 length;
    u32 count;
    u8  status;

    cmdif_datainfo_responsedata.responsecode = CMDIF_ACK_FAILED;
    if (flasherinfo == NULL)
    {
        log_push_error_point(0x481F);
        return S_BADCONTENT;
    }

    length = strlen((char*)tunecode);
    if (length < MIN_TUNECODE_LENTH || length >= MAX_TUNECODE_LENTH)
    {
        log_push_error_point(0x4820);
        cmdif_datainfo_responsedata.responsecode = CMDIF_ACK_FAILED;
    }
    else
    {
        status = obd2tune_gettunecodelistinfo(tunecode,flasherinfo->vin,
                                              &flasherinfo->tunecodeinfolist);
        if (status == S_NOLOOKUP)
        {
            log_push_error_point(0x4821);
            cmdif_datainfo_responsedata.responsecode = CMDIF_ACK_NO_LOOKUP;
        }
        else if (status == S_NOTSUPPORT)
        {
            log_push_error_point(0x4851);
            cmdif_datainfo_responsedata.responsecode = CMDIF_ACK_NOT_SUPPORTED;
        }
        else if (status == S_SERIALUNMATCH)
        {
            log_push_error_point(0x4858);
            cmdif_datainfo_responsedata.responsecode = CMDIF_ACK_SERIAL_UNMATCH;
        }
        else if (status == S_UNMATCH)
        {
            log_push_error_point(0x4859);
            cmdif_datainfo_responsedata.responsecode = CMDIF_ACK_FILE_NOT_MATCH;
        }
        else if (status == S_FILENOTFOUND)
        {
            log_push_error_point(0x485F);
            cmdif_datainfo_responsedata.responsecode = CMDIF_ACK_TUNE_NOT_FOUND;
        }
        else if (status == S_STRATNOTFOUND ||
                 flasherinfo->tunecodeinfolist.count == 0)
        {
            if (flasherinfo->ecm_count > 0)
            {
                u16 prev_veh_index;

                prev_veh_index = flasherinfo->selected_tunelistinfo.tuneindex[flasherinfo->ecm_count-1];
                if (VEH_IsAllowSkipUnsupportedSubECM(prev_veh_index))
                {
                    cmdif_datainfo_responsedata.responsecode = CMDIF_ACK_SKIP;
                }
                else
                {
                    log_push_error_point(0x4822);
                    cmdif_datainfo_responsedata.responsecode = CMDIF_ACK_VEHICLE_UNSUPPORTED;
                }
            }
            else
            {
                log_push_error_point(0x4823);
                cmdif_datainfo_responsedata.responsecode = CMDIF_ACK_VEHICLE_UNSUPPORTED;
            }
        }
        else if (status != S_SUCCESS)
        {
            log_push_error_point(0x4824);
            cmdif_datainfo_responsedata.responsecode = CMDIF_ACK_FAILED;
        }
        else
        {
            id = TUNENAMEID_START;
            length = 1;         //1st byte is tune count
            final_tunecode_count = 0;
#define MAX_LIST_INFO_COMPLETE_CONTENT_LENGTH   2048
            bptr = __malloc(MAX_LIST_INFO_COMPLETE_CONTENT_LENGTH);
            if (bptr == NULL)
            {
                log_push_error_point(0x4825);
                cmdif_datainfo_responsedata.responsecode = CMDIF_ACK_FAILED;
                goto cmdif_func_flasher_get_tune_list_info_complete_done;
            }

            for(i=0;i<flasherinfo->tunecodeinfolist.count;i++)
            {
                count = strlen((char*)flasherinfo->tunecodeinfolist.description[i]) + 1;
                if ((length + count + 3) > MAX_LIST_INFO_COMPLETE_CONTENT_LENGTH)
                {
                    //cannot accept any more data
                    break;
                }
                bptr[length++] = id;
                bptr[length++] = id >> 8;
                strcpy((char*)&bptr[length],
                       (char*)flasherinfo->tunecodeinfolist.description[i]);
                length += count;
                id++;
                final_tunecode_count++;
            }
            bptr[0] = final_tunecode_count;

            if (final_tunecode_count > 0)
            {
                cmdif_datainfo_responsedata.dataptr = bptr;
                cmdif_datainfo_responsedata.datalength = length;
                cmdif_datainfo_responsedata.responsecode = CMDIF_ACK_OK;
            }
            else
            {
                log_push_error_point(0x4826);
                __free(bptr);
                cmdif_datainfo_responsedata.responsecode = CMDIF_ACK_FAILED;
            }
        }
    }

cmdif_func_flasher_get_tune_list_info_complete_done:
    return S_SUCCESS;
}

//------------------------------------------------------------------------------
// Inputs:  u8  ecm_index (current ecm index working on)
//          u8  selected_tune_index (user selected tune to use for this ecm)
// Return   u8  status
// Note: only used in preloaded tune
//------------------------------------------------------------------------------
u8 cmdif_func_flasher_set_selected_tune_info(u8 ecm_index,
                                             u8 selected_tune_index)
{
    u8  status;

    cmdif_datainfo_responsedata.responsecode = CMDIF_ACK_FAILED;
    if (flasherinfo == NULL)
    {
        log_push_error_point(0x482B);
        return S_BADCONTENT;
    }

    flasherinfo->selected_tunelistinfo.description[ecm_index][0] = NULL;
    flasherinfo->selected_tunelistinfo.tunefilename[ecm_index][0] = NULL;
    flasherinfo->selected_tunelistinfo.optionfilename[ecm_index][0] = NULL;
    //TODOQ: check ecm_index too
    if ((selected_tune_index >= flasherinfo->tunecodeinfolist.count) ||
        (ecm_index >= ECM_MAX_COUNT))
    {
        log_push_error_point(0x482C);
        return S_INPUT;
    }

    strcpy((char*)flasherinfo->selected_tunelistinfo.description[ecm_index],
           (char*)flasherinfo->tunecodeinfolist.description[selected_tune_index]);
    status = file_default_getfilename(flasherinfo->tunecodeinfolist.tunefilename[selected_tune_index],
                                      flasherinfo->selected_tunelistinfo.tunefilename[ecm_index]);
    
    return status;
}

//------------------------------------------------------------------------------
// Input:   u8  *tune_selection_data ([2:tune_selected_id][1:vehiclecode_index])
// Return:  u8  status
// Engineer: Patrick Downs, Quyen Leba
// Note: always process vehicle index in ascending order (0,1,2...)
// Response data:
//  [n:option filename][NULL][NULL][4:flags]
//  [n:option filename][NULL][m:second option filename][NULL][4:flags]
//  [NULL][m:second option filename][NULL][4:flags]
//------------------------------------------------------------------------------
u8 cmdif_func_flasher_select_tune_preloaded(u8 *tune_selection_data)
{
    u8  mergeoptfilename[128];
    u16 tune_id;
    u16 tune_index;
    u8  code_index;
    //u16  ecm_type;
    u16 preloadedtuneindex;
    u16 length;
    u32 tmpu32;
    u32 flagsfromgenericlookup_specialoptionfile;
    u8  *bptr;
    sthFileHeader *sthheader_ptr;
    bool isRegOptionsValid;
    bool isSpecOptionsFromTuneValid;
    bool isSpecOptionsFromLookupValid;
    u8  status;
    
    isRegOptionsValid = FALSE;
    isSpecOptionsFromTuneValid = FALSE;
    isSpecOptionsFromLookupValid = FALSE;
    sthheader_ptr = NULL;

    cmdif_datainfo_responsedata.responsecode = CMDIF_ACK_FAILED;
    if (flasherinfo == NULL)
    {
        log_push_error_point(0x482D);
        goto cmdif_func_flasher_select_tune_preloaded_done;
    }
    else if (SETTINGS_IsNoPreloadedDevice())
    {
        log_push_error_point(0x4856);
        goto cmdif_func_flasher_select_tune_preloaded_done;
    }

    tune_id = tune_selection_data[0];
    tune_id |= ((u16)tune_selection_data[1]) << 8;
    code_index = tune_selection_data[2];
    if (tune_id < TUNENAMEID_START)
    {
        log_push_error_point(0x482E);
        goto cmdif_func_flasher_select_tune_preloaded_done;
    }
    tune_index = tune_id - TUNENAMEID_START;

    if (code_index >= ECM_MAX_COUNT)
    {
        //assume: 0-PCM, 1:TCM
        log_push_error_point(0x482F);
        cmdif_datainfo_responsedata.responsecode = CMDIF_ACK_INVALID_PCMNUMBER;
        goto cmdif_func_flasher_select_tune_preloaded_done;
    }

    status = cmdif_func_flasher_set_selected_tune_info(code_index,
                                                       tune_index);
    if (status != S_SUCCESS)
    {
        log_push_error_point(0x4830);
        goto cmdif_func_flasher_select_tune_preloaded_done;
    }
    flasherinfo->ecm_count++;

    //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    // Validate this tune file
    //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    sthheader_ptr = (sthFileHeader*)__malloc(sizeof(sthFileHeader));
    if (!sthheader_ptr)
    {
        log_push_error_point(0x489D);
        cmdif_datainfo_responsedata.responsecode = CMDIF_ACK_FAILED;
        goto cmdif_func_flasher_select_tune_preloaded_done;
    }
    status = obd2tune_validate_sth_file(flasherinfo->selected_tunelistinfo.tunefilename[code_index],
                                        sthheader_ptr);
    if (status == S_OPENFILE)
    {
        log_push_error_point(0x4831);
        cmdif_datainfo_responsedata.responsecode = CMDIF_ACK_TUNE_NOT_FOUND;
        goto cmdif_func_flasher_select_tune_preloaded_done;
    }
    else if (status == S_CRC32E)
    {
        log_push_error_point(0x485A);
        cmdif_datainfo_responsedata.responsecode = CMDIF_ACK_FILE_NOT_MATCH;
        goto cmdif_func_flasher_select_tune_preloaded_done;
    }
    else if (status != S_SUCCESS)
    {
        log_push_error_point(0x485B);
        cmdif_datainfo_responsedata.responsecode = CMDIF_ACK_FAILED;
        goto cmdif_func_flasher_select_tune_preloaded_done;
    }

    if (sthFileHeaderFlags_isForceFullFlash(sthheader_ptr->generic.flags))
    {
        flasherinfo->isfullreflash[code_index] = TRUE;
    }
    
    //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    // Check if this tuneos available and save partnumber to flasher info
    //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    if (sthheader_ptr->generic.file_type == CURRENT_PRELOADED_TUNE_FILETYPE_14 &&
        isgraph(sthheader_ptr->type14.tuneosfilename[0]))
    {
        if (strstr((char*)sthheader_ptr->type14.tuneosfilename,".tos"))        //if tuneos#.tos
        {
            u32 i;
            customtune_tuneosdata tuneosdata;
            u8 tuneosfilepath[64];
            genfs_defaultpathcorrection(sthheader_ptr->type14.tuneosfilename, 
                                        sizeof(tuneosfilepath), tuneosfilepath);
            status = obd2tune_extracttuneosdata_tos(tuneosfilepath, &tuneosdata);
            if (status != S_SUCCESS)
            {
                log_push_error_point(0x48C4);
                goto cmdif_func_flasher_select_tune_preloaded_done;
            }
            if (strlen((char*)tuneosdata.basestockfilename) >=
                sizeof(flasherinfo->basestockfilename))
            {
                log_push_error_point(0x48C5);
                goto cmdif_func_flasher_select_tune_preloaded_done;
            }
            else if (strlen((char*)tuneosdata.codes) >=
                     sizeof(flasherinfo->vehiclecodes))
            {
                log_push_error_point(0x48C6);
                goto cmdif_func_flasher_select_tune_preloaded_done;
            }
            else if (strlen((char*)tuneosdata.secondcodes) >=
                     sizeof(flasherinfo->secondvehiclecodes))
            {
                log_push_error_point(0x48C7);
                goto cmdif_func_flasher_select_tune_preloaded_done;
            }
            for(i=0; i < tuneosdata.ecm_count; i++)
            {
                /* the SPF is in default folder */
                strcpy((char*)flasherinfo->basestockfilename[i], GENFS_DEFAULT_FOLDER_PATH);
                strcat((char*)flasherinfo->basestockfilename[i],
                       (char*)tuneosdata.basestockfilename[i]);
                flasherinfo->basestockcrc32e[i] = tuneosdata.basestockcrc32e[i];
            }
            strcpy((char*)flasherinfo->vehiclecodes,(char*)tuneosdata.codes);
            strcpy((char*)flasherinfo->secondvehiclecodes,(char*)tuneosdata.secondcodes);
        }
        else
        {
            log_push_error_point(0x48C8);
            status = S_NOTSUPPORT;
            goto cmdif_func_flasher_select_tune_preloaded_done;
        }
    }

    //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    // Check if this tune requires SPF (stock preloaded file)
    //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    if (sthFileHeaderFlags_isSPFSrouce(sthheader_ptr->generic.flags))
    {
        //use mergeoptfilename as temp buffer to store SPF file name
        bptr = (u8*)mergeoptfilename;

        strcpy((char*)bptr,GENFS_DEFAULT_FOLDER_PATH);
        // Check for partnumber first to open a stock file
        if (flasherinfo->ecminfo.second_codes[code_index][0][0] != NULL)
        {
            strcat((char*)bptr,
                   (char*)flasherinfo->ecminfo.second_codes[code_index][0]);
            strcat((char*)bptr,".spf");        
        }
        else if(flasherinfo->ecminfo.codes[code_index][0][0] != NULL)   // Then strategy
        {
            strcat((char*)bptr,
                   (char*)flasherinfo->ecminfo.codes[code_index][0]);
            strcat((char*)bptr,".spf");        
        }
        else
        {
            log_push_error_point(0x489E);
            cmdif_datainfo_responsedata.responsecode = CMDIF_ACK_FAILED;    //TODOQ: better ACK
            goto cmdif_func_flasher_select_tune_preloaded_done;
        }
        if (!file_isexist(bptr))    //this only check default folder
        {
            log_push_error_point(0x489F);
            cmdif_datainfo_responsedata.responsecode = CMDIF_ACK_TUNE_NOT_FOUND;    //TODOQ: better ACK
            goto cmdif_func_flasher_select_tune_preloaded_done;
        }
        if (strlen((char*)bptr) >= sizeof(flasherinfo->basestockfilename[code_index]))
        {
            log_push_error_point(0x48A0);
            cmdif_datainfo_responsedata.responsecode = CMDIF_ACK_FAILED;
            goto cmdif_func_flasher_select_tune_preloaded_done;
        }
        flasherinfo->basestockcrc32e[code_index] = sthheader_ptr->generic.basestock_crc32e;
        if (strlen((char*)bptr) >= sizeof(flasherinfo->basestockfilename[code_index]))
        {
            log_push_error_point(0x48A1);
            cmdif_datainfo_responsedata.responsecode = CMDIF_ACK_FAILED;
            goto cmdif_func_flasher_select_tune_preloaded_done;
        }
        strcpy((char*)flasherinfo->basestockfilename[code_index],(char*)bptr);
    }
    //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    // Check if this tune has OTF capability
    //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    if (sthFileHeaderFlags_isOnTheFlyEnabled(sthheader_ptr->generic.flags))
    {
        switch(code_index)
        {
        case 0:
            flasherinfo->otfinfo.pcm = sthFileHeaderFlags_getOnTheFlyMode(sthheader_ptr->generic.flags);
            break;
        case 1:
            flasherinfo->otfinfo.tcm = sthFileHeaderFlags_getOnTheFlyMode(sthheader_ptr->generic.flags);
            break;
        default:
            //should not happen
            break;
        }//switch(code_index)...
    }
    __free(sthheader_ptr);
    sthheader_ptr = NULL;

    //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    // Get checksum filename from stategy in sth filename
    //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    obd2tune_preloadedcsumfilename(flasherinfo->selected_tunelistinfo.tunefilename[code_index], 
                                       flasherinfo->selected_tunelistinfo.checksumfilename[code_index]);

    //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    // Get option file name from this tune
    //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    status = obd2tune_getoptionfilename
        (flasherinfo->selected_tunelistinfo.tunefilename[code_index],
         flasherinfo->selected_tunelistinfo.optionfilename[code_index]);
    if(status == S_SUCCESS)
    {
        //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
        // Validate this option file
        //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
        status = obd2tune_validate_oph_file(flasherinfo->selected_tunelistinfo.optionfilename[code_index]);
        if (status == S_OPENFILE)
        {
            log_push_error_point(0x4861);
            cmdif_datainfo_responsedata.responsecode = CMDIF_ACK_FILE_NOT_FOUND;
            goto cmdif_func_flasher_select_tune_preloaded_done;
        }
        else if (status == S_CRC32E)
        {
            log_push_error_point(0x4862);
            cmdif_datainfo_responsedata.responsecode = CMDIF_ACK_FILE_NOT_MATCH;
            goto cmdif_func_flasher_select_tune_preloaded_done;
        }
        else if (status != S_SUCCESS)
        {
            log_push_error_point(0x4863);
            cmdif_datainfo_responsedata.responsecode = CMDIF_ACK_FAILED;
            goto cmdif_func_flasher_select_tune_preloaded_done;
        }        
        isRegOptionsValid = TRUE; // Regular options should be processed
    }
    else if (status != S_SUCCESS && (status != S_NOTREQUIRED)) // S_NOTREQUIRED means no options
    {
        log_push_error_point(0x4885);
        cmdif_datainfo_responsedata.responsecode = CMDIF_ACK_FAILED;
        goto cmdif_func_flasher_select_tune_preloaded_done;
    }
    
    //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    // Get special option file name from this tune
    //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    status = obd2tune_getspecialoptionfilename
        (flasherinfo->selected_tunelistinfo.tunefilename[code_index],
         flasherinfo->selected_tunelistinfo.specialoptionfilename[code_index]);
    if(status == S_SUCCESS)
    {
        //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
        // Validate this option file
        //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
        status = obd2tune_validate_oph_file(flasherinfo->selected_tunelistinfo.specialoptionfilename[code_index]);
        if (status == S_OPENFILE)
        {
            log_push_error_point(0x4880);
            cmdif_datainfo_responsedata.responsecode = CMDIF_ACK_FILE_NOT_FOUND;
            goto cmdif_func_flasher_select_tune_preloaded_done;
        }
        else if (status == S_CRC32E)
        {
            log_push_error_point(0x4881);
            cmdif_datainfo_responsedata.responsecode = CMDIF_ACK_FILE_NOT_MATCH;
            goto cmdif_func_flasher_select_tune_preloaded_done;
        }
        else if (status != S_SUCCESS)
        {
            log_push_error_point(0x4882);
            cmdif_datainfo_responsedata.responsecode = CMDIF_ACK_FAILED;
            goto cmdif_func_flasher_select_tune_preloaded_done;
        }
        isSpecOptionsFromTuneValid = TRUE;  // Special options should be processed
    }
    else if (status != S_SUCCESS && (status != S_NOTREQUIRED)) // S_NOTREQUIRED means no options
    {
        log_push_error_point(0x4883);
        cmdif_datainfo_responsedata.responsecode = CMDIF_ACK_FAILED;
        goto cmdif_func_flasher_select_tune_preloaded_done;
    }

    //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    // Handle merging if necessary and passing back correct option filename
    //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    if(isRegOptionsValid && isSpecOptionsFromTuneValid)
    {
        // Merge and save new option filename
        // Send ACK WAIT
        strcpy((char*)mergeoptfilename,MERGED_OPTS_PRELOAD_FILENAME_PARTA);
        bptr = (u8*)strstr((char*)mergeoptfilename,"#");
        if (!bptr)
        {
            log_push_error_point(0x4884);
            cmdif_datainfo_responsedata.responsecode = CMDIF_ACK_FAILED;
            goto cmdif_func_flasher_select_tune_preloaded_done;   
        }
        bptr[0] = '0'+code_index;

        cmdif_internalresponse_ack(CMDIF_CMD_SELECT_TUNE_STRATEGY,
                                   CMDIF_ACK_WAIT,NULL,0);
        //TODOQK: check if this cause timeout problems on AppBoard
        status = obd2tune_mergeoptionfile(flasherinfo->selected_tunelistinfo.optionfilename[code_index],
                                          flasherinfo->selected_tunelistinfo.specialoptionfilename[code_index],
                                          mergeoptfilename);
        if(status != S_SUCCESS)
        {
            log_push_error_point(0x4886);
            cmdif_datainfo_responsedata.responsecode = CMDIF_ACK_FAILED;
            goto cmdif_func_flasher_select_tune_preloaded_done;   
        }
        cmdif_internalresponse_ack(CMDIF_CMD_SELECT_TUNE_STRATEGY,
                                   CMDIF_ACK_WAIT,NULL,0);

        strcpy((char*)flasherinfo->selected_tunelistinfo.optionfilename[code_index],
               (char*)mergeoptfilename);
        //already merged, no longer need this filename
        flasherinfo->selected_tunelistinfo.specialoptionfilename[code_index][0] = NULL;
    }
    else if(isSpecOptionsFromTuneValid)
    {
        // Only Special options valid, save as option filename
        strcpy((char*)flasherinfo->selected_tunelistinfo.optionfilename[code_index],
               (const char*)flasherinfo->selected_tunelistinfo.specialoptionfilename[code_index]);
        //use as regular optionfile, no longer need this filename
        flasherinfo->selected_tunelistinfo.specialoptionfilename[code_index][0] = NULL;
    }

    //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    // Get special option file from lookup (glf)
    //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    status = obd2tune_search_glf_for_special_option_file(DEFAULT_OPTION_LOOKUP_FILE,
                                                         flasherinfo->vin,
                                                         flasherinfo->ecminfo.codes[code_index][0],
                                                         GeneralFileLookupFileProcessorCodeType_Regular,
                                                         flasherinfo->selected_tunelistinfo.specialoptionfilename[code_index],
                                                         &flagsfromgenericlookup_specialoptionfile);
    isSpecOptionsFromLookupValid = TRUE;
    if (status == S_NOTREQUIRED || status == S_FILENOTFOUND)
    {
        isSpecOptionsFromLookupValid = FALSE;
    }
    else if (status != S_SUCCESS)
    {
        log_push_error_point(0x488E);
        cmdif_datainfo_responsedata.responsecode = CMDIF_ACK_OPTION_LOOKUP_SEARCH;
        goto cmdif_func_flasher_select_tune_preloaded_done;
    }

    //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    // Note: 4 byte flags appended to cmdif_datainfo_responsedata.dataptr
    // after this if statement
    //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    if (isRegOptionsValid && isSpecOptionsFromLookupValid)
    {
        //check if 2 option files need to merge
        if (isGeneralFileLookupSpecialOptionFileFlags_ListMergeFormat(flagsfromgenericlookup_specialoptionfile))
        {
            strcpy((char*)mergeoptfilename,MERGED_OPTS_PRELOAD_FILENAME_PARTB);
            bptr = (u8*)strstr((char*)mergeoptfilename,"#");
            if (!bptr)
            {
                log_push_error_point(0x4893);
                cmdif_datainfo_responsedata.responsecode = CMDIF_ACK_FAILED;
                goto cmdif_func_flasher_select_tune_preloaded_done;   
            }
            bptr[0] = '0'+code_index;

            cmdif_internalresponse_ack(CMDIF_CMD_SELECT_TUNE_STRATEGY,
                                       CMDIF_ACK_WAIT,NULL,0);
            //TODOQK: check if this cause timeout problems on AppBoard
            status = obd2tune_mergeoptionfile(flasherinfo->selected_tunelistinfo.optionfilename[code_index],
                                              flasherinfo->selected_tunelistinfo.specialoptionfilename[code_index],
                                              mergeoptfilename);
            if (status != S_SUCCESS)
            {
                log_push_error_point(0x4894);
                goto cmdif_func_flasher_select_tune_preloaded_done;
            }
            cmdif_internalresponse_ack(CMDIF_CMD_SELECT_TUNE_STRATEGY,
                                       CMDIF_ACK_WAIT,NULL,0);

            //data: [n:merged option filename][NULL][NULL][4:flags]
            cmdif_datainfo_responsedata.datalength = strlen((char*)mergeoptfilename) + 1 + 1;
            cmdif_datainfo_responsedata.dataptr = __malloc(cmdif_datainfo_responsedata.datalength+4);
            if (cmdif_datainfo_responsedata.dataptr == NULL)
            {
                log_push_error_point(0x4895);
                return S_MALLOC;
            }
            memset((char*)cmdif_datainfo_responsedata.dataptr, 0, (cmdif_datainfo_responsedata.datalength + 4));
            strcpy((char*)cmdif_datainfo_responsedata.dataptr,
                   (char*)mergeoptfilename);
            //already merged, no longer need this filename
            flasherinfo->selected_tunelistinfo.specialoptionfilename[code_index][0] = NULL;
        }
        else
        {
            //data: [n:option filename][NULL][m:special option filename][NULL][4:flags]
            length = strlen((char*)flasherinfo->selected_tunelistinfo.optionfilename[code_index]) + 1;
            cmdif_datainfo_responsedata.datalength = length + strlen((char*)flasherinfo->selected_tunelistinfo.specialoptionfilename[code_index]) + 1;
            cmdif_datainfo_responsedata.dataptr = __malloc(cmdif_datainfo_responsedata.datalength+4);
            if (cmdif_datainfo_responsedata.dataptr == NULL)
            {
                log_push_error_point(0x4896);
                return S_MALLOC;
            }
            memset((char*)cmdif_datainfo_responsedata.dataptr, 0, (cmdif_datainfo_responsedata.datalength + 4));
            strcpy((char*)cmdif_datainfo_responsedata.dataptr,
                   (char*)flasherinfo->selected_tunelistinfo.optionfilename[code_index]);
            strcpy((char*)&cmdif_datainfo_responsedata.dataptr[length],
                   (char*)flasherinfo->selected_tunelistinfo.specialoptionfilename[code_index]);
        }
    }
    else if (isRegOptionsValid)
    {
        //data: [n:option filename][NULL][NULL][4:flags]
        cmdif_datainfo_responsedata.datalength = strlen((char*)flasherinfo->selected_tunelistinfo.optionfilename[code_index]) + 1 + 1;
        cmdif_datainfo_responsedata.dataptr = __malloc(cmdif_datainfo_responsedata.datalength+4);
        if (cmdif_datainfo_responsedata.dataptr == NULL)
        {
            log_push_error_point(0x4835);
            return S_MALLOC;
        }
        memset((char*)cmdif_datainfo_responsedata.dataptr, 0, (cmdif_datainfo_responsedata.datalength + 4));
        strcpy((char*)cmdif_datainfo_responsedata.dataptr,
               (char*)flasherinfo->selected_tunelistinfo.optionfilename[code_index]);
    }
    else if (isSpecOptionsFromLookupValid)
    {
        //data: [NULL][m:special option filename][NULL][4:flags]
        cmdif_datainfo_responsedata.datalength = 1 + strlen((char*)flasherinfo->selected_tunelistinfo.specialoptionfilename[code_index]) + 1;
        cmdif_datainfo_responsedata.dataptr = __malloc(cmdif_datainfo_responsedata.datalength+4);
        if (cmdif_datainfo_responsedata.dataptr == NULL)
        {
            log_push_error_point(0x4890);
            return S_MALLOC;
        }
        memset((char*)cmdif_datainfo_responsedata.dataptr, 0, (cmdif_datainfo_responsedata.datalength + 4));
        cmdif_datainfo_responsedata.dataptr[0] = NULL;
        strcpy((char*)&cmdif_datainfo_responsedata.dataptr[1],
               (char*)flasherinfo->selected_tunelistinfo.specialoptionfilename[code_index]);
    }
    else // No options
    {
        //data: [NULL][NULL][4:flags]
        cmdif_datainfo_responsedata.datalength = 1 + 1;
        cmdif_datainfo_responsedata.dataptr = __malloc(cmdif_datainfo_responsedata.datalength+4);
        if (cmdif_datainfo_responsedata.dataptr == NULL)
        {
            log_push_error_point(0x489B);
            return S_MALLOC;
        }
        memset((char*)cmdif_datainfo_responsedata.dataptr, 0, (cmdif_datainfo_responsedata.datalength + 4));
    }    
    //add 4byte flags to response data
#define OPTIONFLAGS_NONE                                    0
//Bit[1..0]: presentation format
#define OPTIONFLAGS_LIST_ALL_SPECIALOPTIONS                 (1)
#define OPTIONFLAGS_ASK_INDIVIDUALLY_SPECIALOPTIONS         (2)
//Bit[31..2]: reserved
    tmpu32 = OPTIONFLAGS_NONE;
    if (isGeneralFileLookupSpecialOptionFileFlags_ListSeparateFormat(flagsfromgenericlookup_specialoptionfile))
    {
        tmpu32 = OPTIONFLAGS_LIST_ALL_SPECIALOPTIONS;
    }
    else if (isGeneralFileLookupSpecialOptionFileFlags_IndividualFormat(flagsfromgenericlookup_specialoptionfile))
    {
        tmpu32 = OPTIONFLAGS_ASK_INDIVIDUALLY_SPECIALOPTIONS;
    }

    memcpy((char*)&cmdif_datainfo_responsedata.dataptr[cmdif_datainfo_responsedata.datalength],
           (char*)&tmpu32,4);
    cmdif_datainfo_responsedata.datalength += 4; // 4 bytes Flags

    //abandon error code 0x4832: log_push_error_point(0x4832)

    status = obd2tune_read_preloadedtune_index_from_preloadedtunefile
        (flasherinfo->selected_tunelistinfo.tunefilename[code_index],
         &preloadedtuneindex);
    if (status != S_SUCCESS)
    {
        //TODOQ: use more accurate ACK code
        log_push_error_point(0x4833);
        cmdif_datainfo_responsedata.responsecode = CMDIF_ACK_INVALID_BINARYTYPE;
        goto cmdif_func_flasher_select_tune_preloaded_done;
    }
    status = obd2_validate_ecmbinarytype(preloadedtuneindex);
    if (status == S_SUCCESS)
    {
        //flasherinfo->vehicle_type_from_preloadedtune[code_index] = preloadedtuneindex;
        //TODOQ: more on this, simplify it
        flasherinfo->selected_tunelistinfo.tuneindex[code_index] = preloadedtuneindex;
    }
    else
    {
        log_push_error_point(0x4834);
        cmdif_datainfo_responsedata.responsecode = CMDIF_ACK_INVALID_BINARYTYPE;
        goto cmdif_func_flasher_select_tune_preloaded_done;
    }
    
    //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    // Check if SPF is required
    //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    if(ECM_IsSpfRequired(VEH_GetMainEcmType(preloadedtuneindex)))
    {
        // SPF is required for this ECM
        if(obd2tune_check_ecm_stock(VEH_GetMainEcmType(preloadedtuneindex),
                                    GenerateStockSource_Use_DEFAULT_Folder_First, code_index) != S_SUCCESS)
        {
            // Required SPF not found
            log_push_error_point(0x4887);
            cmdif_datainfo_responsedata.responsecode = CMDIF_ACK_SPF_NOT_FOUND;
            goto cmdif_func_flasher_select_tune_preloaded_done;
        }
    }

    flasherinfo->selected_tunelistinfo.supported[code_index] = TRUE;
    cmdif_datainfo_responsedata.responsecode = CMDIF_ACK_OK;

cmdif_func_flasher_select_tune_preloaded_done:
    if (sthheader_ptr)
    {
        __free(sthheader_ptr);
    }
    return S_SUCCESS;
}

//------------------------------------------------------------------------------
// Input:   u8  *extra_file_data
//              [n:tuneosfilename][1:NULL][m:specialoptionfilename][1:NULL]
// Return:  u8  status
// Engineer: Quyen Leba
// Note: only used in custom tune
//------------------------------------------------------------------------------
u8 cmdif_func_set_extra_custom_file_g1(u8 *extra_file_data)
{
    u8  specialoptionfilename[MAX_FILENAME_LENGTH+1];
    u8  tuneosfilename[MAX_FILENAME_LENGTH+1];
    u8  tunematch_masked;
    u32 index;
    u8  status;

    tuneosfilename[0] = NULL;
    specialoptionfilename[0] = NULL;
    index = 0;
    
    cmdif_datainfo_responsedata.responsecode = CMDIF_ACK_FAILED;
    if (flasherinfo == NULL)
    {
        log_push_error_point(0x4836);
        return S_BADCONTENT;
    }
    
    //get tuneosfilename
    if (extra_file_data[index+1] != NULL)
    {
        if (file_isfilenamevalid
            (&extra_file_data[index],MAX_TUNE_NAME_LENGTH) == FALSE)
        {
            log_push_error_point(0x4837);
            //return S_BADCONTENT;
            goto cmdif_func_set_extra_custom_file_g1_done;
        }
        status = file_user_getfilename(&extra_file_data[index],tuneosfilename);
        if (status != S_SUCCESS)
        {
            log_push_error_point(0x4838);
            //return status;
            goto cmdif_func_set_extra_custom_file_g1_done;
        }
    }
    index += (strlen((char*)tuneosfilename) + 1);
    //get specialoptionfilename
    if (extra_file_data[index+1] != NULL)
    {
        if (file_isfilenamevalid
            (&extra_file_data[index],MAX_TUNE_NAME_LENGTH) == FALSE)
        {
            log_push_error_point(0x4839);
            //return S_BADCONTENT;
            goto cmdif_func_set_extra_custom_file_g1_done;
        }
        status = file_user_getfilename(&extra_file_data[index],
                                       specialoptionfilename);
        if (status != S_SUCCESS)
        {
            log_push_error_point(0x483A);
            //return status;
            goto cmdif_func_set_extra_custom_file_g1_done;
        }
    }
    //index += (strlen((char*)specialoptionfilename) + 1);
    status = file_user_getfilename(specialoptionfilename,
                                   flasherinfo->selected_tunelistinfo.specialoptionfilename[0]);
    if (status != S_SUCCESS)
    {
        log_push_error_point(0x4865);
        goto cmdif_func_set_extra_custom_file_g1_done;
    }
    
    if (tuneosfilename[0] != NULL)
    {
        //check if tune is supported in this vehicle
        status = obd2tune_check_customtune_support
            (flasherinfo->vehicle_type,flasherinfo->vin,tuneosfilename,
             &flasherinfo->ecminfo,&tunematch_masked,&flasherinfo->isvinmasked);
        if (status == S_NOTREQUIRED)
        {
            cmdif_datainfo_responsedata.responsecode = CMDIF_ACK_OK;
            goto cmdif_func_set_extra_custom_file_g1_done;
        }
        
        if (status == S_SUCCESS && flasherinfo->isvinmasked)
        {
            flasherinfo->tunecheck_status = S_TUNEMATCH_VINMASK;
            cmdif_datainfo_responsedata.responsecode = CMDIF_ACK_OK;
            goto cmdif_func_set_extra_custom_file_g1_done;
        }
        else if (status == S_TUNEUNMATCH && flasherinfo->ecm_count > 1 && 
                 (tunematch_masked & (1<<1)) == 0)
        {
            /* TCM must match */
            log_push_error_point(0x48C2);
            cmdif_datainfo_responsedata.responsecode = CMDIF_ACK_FAILED;
            goto cmdif_func_set_extra_custom_file_g1_done;
        }
        else if (status == S_VINUNMATCH || status == S_TUNEUNMATCH)
        {
            flasherinfo->tunecheck_status = status;
            cmdif_datainfo_responsedata.responsecode = CMDIF_ACK_OK;
            goto cmdif_func_set_extra_custom_file_g1_done;
        }
        else if (status != S_SUCCESS)
        {
            log_push_error_point(0x483B);
            cmdif_datainfo_responsedata.responsecode = CMDIF_ACK_FAILED;
            goto cmdif_func_set_extra_custom_file_g1_done;
        }
    }
    cmdif_datainfo_responsedata.responsecode = CMDIF_ACK_OK;
    
cmdif_func_set_extra_custom_file_g1_done:
    return S_SUCCESS;
}

//------------------------------------------------------------------------------
// Input:   u8  *extra_file_data
//              [n:tuneosfilename][1:NULL][m:specialoptionfilename][1:NULL]
// Return:  u8  status
// Engineer: Quyen Leba
// Note: only used in custom tune
//------------------------------------------------------------------------------
u8 cmdif_func_set_extra_custom_file_g2(u8 *extra_file_data)
{
    u8  checksumfilename[MAX_FILENAME_LENGTH+1];
    u32 index;
    u8  status;

    checksumfilename[0] = NULL;
    index = 0;

    cmdif_datainfo_responsedata.responsecode = CMDIF_ACK_FAILED;
    if (flasherinfo == NULL)
    {
        log_push_error_point(0x483C);
        return S_BADCONTENT;
    }

    //get checksumfilename
    if (extra_file_data[index+1] != NULL)
    {
        if (file_isfilenamevalid
            (&extra_file_data[index],MAX_TUNE_NAME_LENGTH) == FALSE)
        {
            log_push_error_point(0x483D);
            goto cmdif_func_set_extra_custom_file_g2_done;
        }
        status = file_user_getfilename(&extra_file_data[index],
                                  checksumfilename);
        if (status != S_SUCCESS)
        {
            log_push_error_point(0x483E);
            goto cmdif_func_set_extra_custom_file_g2_done;
        }
    }

    status = file_user_getfilename(checksumfilename,
                                   flasherinfo->selected_tunelistinfo.checksumfilename[0]);
    if (status != S_SUCCESS)
    {
        log_push_error_point(0x4866);
        goto cmdif_func_set_extra_custom_file_g2_done;
    }
    
    cmdif_datainfo_responsedata.responsecode = CMDIF_ACK_OK;
    
cmdif_func_set_extra_custom_file_g2_done:
    return S_SUCCESS;
}

//------------------------------------------------------------------------------
// Input:   u8  *tune_selection_data
//              old -> [1:reserved][1:vehicle type defined for veh_defs]
//              [2:vehicle type defined for veh_defs]
//              [2:reserved][4:flags]
//              [n:tunefilename][1:NULL][m:tunedescription][1:NULL]
// Return:  u8  status
// Engineer: Patrick Downs, Quyen Leba
// Note: only used in custom tune
//------------------------------------------------------------------------------
u8 cmdif_func_flasher_select_tune_custom(u8 *tune_selection_data)
{
    u16 veh_binarytype;
    u32 veh_flags;
    u8  tunefilename[MAX_FILENAME_LENGTH+1];
    u32 index;
    u8  status;

    cmdif_datainfo_responsedata.responsecode = CMDIF_ACK_FAILED;
    if (flasherinfo == NULL)
    {
        log_push_error_point(0x483F);
        return S_BADCONTENT;
    }
    else if (SETTINGS_IsNoCustomDevice() || SETTINGS_Is50StateLegalDevice())
    {
        log_push_error_point(0x4857);
        return S_NOTSUPPORT;
    }

    veh_binarytype =
        ((u16)tune_selection_data[0]) |
        ((u16)tune_selection_data[1]) << 8;
    if (obd2_validate_ecmbinarytype(veh_binarytype) != S_SUCCESS)
    {
        log_push_error_point(0x4840);
        cmdif_datainfo_responsedata.responsecode = CMDIF_ACK_INVALID_BINARYTYPE;
        goto cmdif_func_flasher_select_tune_custom_done;
    }

    veh_flags = 
        ((u32)tune_selection_data[4]) |
        ((u32)tune_selection_data[5]) << 8 |
        ((u32)tune_selection_data[6]) << 16 |
        ((u32)tune_selection_data[7]) << 24;

    if (SETTINGS_IsMarried() && (veh_binarytype != SETTINGS_TUNE(veh_type)))
    {
        log_push_error_point(0x4841);
        cmdif_datainfo_responsedata.responsecode = CMDIF_ACK_MARRIED_FAIL;  //not a very good ACK
        goto cmdif_func_flasher_select_tune_custom_done;
    }

    //##########################################################################
    //extract tune file name, tune description
    //##########################################################################
    if (file_isfilenamevalid
        (&tune_selection_data[8],MAX_TUNE_NAME_LENGTH) == FALSE)
    {
        log_push_error_point(0x4842);
        return S_INPUT;
    }

    status = file_user_getfilename(&tune_selection_data[8],tunefilename);
    if (status != S_SUCCESS)
    {
        log_push_error_point(0x4843);
        return status;
    }

    if (file_isexist(tunefilename) == FALSE)
    {
        log_push_error_point(0x4844);
        cmdif_datainfo_responsedata.responsecode = CMDIF_ACK_TUNE_NOT_FOUND;
        goto cmdif_func_flasher_select_tune_custom_done;
    }

    //TODOQ: extract tune description (not used, so not important)
    index = (2+2+4) + strlen((char*)&tune_selection_data[8]) + 1;
    if (strlen((char*)&tune_selection_data[index]) > MAX_DESCRIPTION_LENGTH)
    {
        log_push_error_point(0x4845);
        return S_INPUT;
    }
    
    //save tune file name for later usage
    strcpy((char*)flasherinfo->selected_tunelistinfo.tunefilename[0],
           (char*)tunefilename);
    strcpy((char*)flasherinfo->selected_tunelistinfo.description[0],
           (char*)&tune_selection_data[index]);
    flasherinfo->selected_tunelistinfo.tuneindex[0] = veh_binarytype;
    flasherinfo->vehicle_type = veh_binarytype;
    flasherinfo->ecm_count = VEH_GetEcmCount(veh_binarytype);
    flasherinfo->selected_tunelistinfo.flags[0] = veh_flags;
    index += (strlen((char*)flasherinfo->selected_tunelistinfo.description[0]) + 1);
    
    //##########################################################################
    // check tune flags
    //##########################################################################
    //Note: flasherinfo->tunecheck_status is set when validating tuneos file
    //from cmdif_func_set_extra_custom_file_g1
    if (flasherinfo->tunecheck_status == S_VINUNMATCH)
    {
        log_push_error_point(0x4846);
        cmdif_datainfo_responsedata.responsecode = CMDIF_ACK_VIN_UNMATCH;
        goto cmdif_func_flasher_select_tune_custom_done;
    }
    
    if (obd2tune_tuneflags_isforcefull(flasherinfo->selected_tunelistinfo.flags[0]))
    {
        cmdif_datainfo_responsedata.responsecode = CMDIF_ACK_FORCE_FULL_FLASH;
        goto cmdif_func_flasher_select_tune_custom_done;
    }
    
    //TODOQ: check off-road flag
    //TODOQ: support masked VIN case & segment check
    
    if (flasherinfo->tunecheck_status == S_TUNEMATCH_VINMASK)
    {
        //NOTE: this is a special case, not really a failure
        //In this case, the vehicle tune is matched but VIN is masked, and full
        //reflash is required, therefore, user's confirmation is required
        //NOTE: only do full reflash PCM for now, assume 1st ECM is PCM
        flasherinfo->isfullreflash[0] = TRUE;

        if (ECM_IsDownloadAll(VEH_GetEcmType(flasherinfo->vehicle_type,0)))
        {
            cmdif_datainfo_responsedata.responsecode = CMDIF_ACK_FULL_FLASH;
            log_push_error_point(0x4847);
        }
        else
        {
            cmdif_datainfo_responsedata.responsecode = CMDIF_ACK_FULL_FLASH_NOT_ALLOWED;
            log_push_error_point(0x488B);
        }
        goto cmdif_func_flasher_select_tune_custom_done;
    }
    else if (flasherinfo->tunecheck_status == S_TUNEUNMATCH)
    {
        if (obd2tune_tuneflags_iscalonly(flasherinfo->selected_tunelistinfo.flags[0]))
        {
            log_push_error_point(0x4848);
            cmdif_datainfo_responsedata.responsecode = CMDIF_ACK_TUNE_UNMATCH;
            goto cmdif_func_flasher_select_tune_custom_done;
        }
        else    //obd2tune_tuneflags_isallowfull(...)
        {
            //NOTE: this is a special case, not really a failure
            //In this case, the vehicle is not in a supported list, and a full
            //reflash is required, therefore, user's confirmation is required
            //NOTE: VIN must match
            //NOTE: only do full reflash PCM for now, assume 1st ECM is PCM
            flasherinfo->isfullreflash[0] = TRUE;

            if (ECM_IsDownloadAll(VEH_GetEcmType(flasherinfo->vehicle_type,0)))
            {
                cmdif_datainfo_responsedata.responsecode = CMDIF_ACK_FULL_FLASH;
                log_push_error_point(0x4849);
            }
            else
            {
                cmdif_datainfo_responsedata.responsecode = CMDIF_ACK_FULL_FLASH_NOT_ALLOWED;
                log_push_error_point(0x488D);
            }
            goto cmdif_func_flasher_select_tune_custom_done;
        }
    }
    
    cmdif_datainfo_responsedata.responsecode = CMDIF_ACK_OK;
    
cmdif_func_flasher_select_tune_custom_done:
    return S_SUCCESS;
}

/**
 *  cmdif_func_flasher_select_tune_custom_ctb
 *  
 *  @brief Select a CTB custom tune. Only used in custom tune.
 *
 *  @param[in]  tune_selection_data     [4:reserved][4:flags][n:tunefilename][1:NULL][m:tunedescription][1:NULL]
 *
 *  @retval u8 status
 *  
 *  @authors Quyen Leba
 */
u8 cmdif_func_flasher_select_tune_custom_ctb(u8 *tune_selection_data)
{
    customtune_block tuneblock;
    u8  *ctb_tunefilename;
#define CUSTOM_TUNE_CTB_SELECT_CMD_FLAGS_USE_OPTION     (1<<0)
#define CUSTOM_TUNE_CTB_SELECT_CMD_FLAGS_FIND_OPTION    (1<<1)
    typedef struct
    {
        u8  preloadedtune_disabled          : 1;
        u8  reserved0                       : 7;
        u8  reserved1[3];
    }SelectCustomTuneResponseAttrib;
    SelectCustomTuneResponseAttrib response_attrib;
    union
    {
        struct
        {
            u8  regular_optionfile[32];
            u8  special_optionfile[2][32];
            u32 special_optionflags[2];
        }filenames;
        u8  buffer[128];
    }customtune_optionfile_report;
    u8  cmd_flags;
    bool isUseOption;
    bool hasRegularOptionFile;
    u8  tunematch_masked;
    u8  i;
    u8  status;
    
    cmdif_datainfo_responsedata.responsecode = CMDIF_ACK_FAILED;
    if (flasherinfo == NULL)
    {
        log_push_error_point(0x48A2);
        return S_BADCONTENT;
    }
    else if (SETTINGS_IsNoCustomDevice() || SETTINGS_Is50StateLegalDevice())
    {
        log_push_error_point(0x48A3);
        return S_NOTSUPPORT;
    }

    cmd_flags = *(u32*)&tune_selection_data[4];
    ctb_tunefilename = &tune_selection_data[8];

    cmdif_internalresponse_ack(CMDIF_CMD_SELECT_TUNE_CUSTOM,
                               CMDIF_ACK_WAIT,NULL,0);

    status = filectb_openfile(ctb_tunefilename);
    if (status != S_SUCCESS)
    {
        log_push_error_point(0x48A6);
        goto cmdif_func_flasher_select_tune_custom_ctb_done;
    }

    cmdif_progressbar_setup(cmdif_tracker_get_current_command(),"");

    status = filectb_extractAllFiles(cmdif_progressbar_report_semi);
    if (status == S_SPFNOTFOUND)
    {
        log_push_error_point(0x48C3);
        cmdif_datainfo_responsedata.responsecode = CMDIF_ACK_SPF_NOT_FOUND;
        goto cmdif_func_flasher_select_tune_custom_ctb_done;
    }
    else if (status != S_SUCCESS)
    {
        log_push_error_point(0x48A7);
        goto cmdif_func_flasher_select_tune_custom_ctb_done;
    }
    
    status = filectb_generateTuneBinary(cmdif_progressbar_report_semi);
    if (status != S_SUCCESS)
    {
        log_push_error_point(0x48A8);
        goto cmdif_func_flasher_select_tune_custom_ctb_done;
    }

    status = filectb_parseCustomTuneBlock(&tuneblock);
    if (status != S_SUCCESS)
    {
        log_push_error_point(0x48A9);
        goto cmdif_func_flasher_select_tune_custom_ctb_done;
    }
    if (strlen(FILECTB_BASETUNE_FILENAME) >= sizeof(tuneblock.tunefilename))
    {
        log_push_error_point(0x48AA);
        goto cmdif_func_flasher_select_tune_custom_ctb_done;
    }

    status = obd2tune_validate_custom_tune_block(&tuneblock,TRUE);  //signature already checked when open CTB
    switch(status)
    {
    case S_SUCCESS:
        break;
    case S_SERIALUNMATCH:
        log_push_error_point(0x48AC);
        cmdif_datainfo_responsedata.responsecode = CMDIF_ACK_SERIAL_UNMATCH;
        goto cmdif_func_flasher_select_tune_custom_ctb_done;
    case S_UNMATCH:
        //bad signature
        log_push_error_point(0x48AD);
        cmdif_datainfo_responsedata.responsecode = CMDIF_ACK_TUNE_UNMATCH;
        goto cmdif_func_flasher_select_tune_custom_ctb_done;
    case S_BADCONTENT:
        log_push_error_point(0x48AE);
        goto cmdif_func_flasher_select_tune_custom_ctb_done;
    case S_FILENOTFOUND:
        log_push_error_point(0x48AF);
        cmdif_datainfo_responsedata.responsecode = CMDIF_ACK_FILE_NOT_FOUND;
        goto cmdif_func_flasher_select_tune_custom_ctb_done;
    case S_CRC32E:
        log_push_error_point(0x48B0);
        goto cmdif_func_flasher_select_tune_custom_ctb_done;
    default:
        log_push_error_point(0x48B1);
        goto cmdif_func_flasher_select_tune_custom_ctb_done;
    }

    strcpy((char*)tuneblock.tunefilename,FILECTB_BASETUNE_FILENAME);
    isUseOption = FALSE;
    if (cmd_flags & CUSTOM_TUNE_CTB_SELECT_CMD_FLAGS_USE_OPTION)
    {
        isUseOption = TRUE;
    }
    status = obd2tune_process_custom_tune_block(&tuneblock,isUseOption);
    if (status != S_SUCCESS)
    {
        log_push_error_point(0x48AB);
        goto cmdif_func_flasher_select_tune_custom_ctb_done;
    }

    if (obd2_validate_ecmbinarytype(flasherinfo->vehicle_type) != S_SUCCESS)
    {
        log_push_error_point(0x48A4);
        cmdif_datainfo_responsedata.responsecode = CMDIF_ACK_INVALID_BINARYTYPE;
        goto cmdif_func_flasher_select_tune_custom_ctb_done;
    }

    if (SETTINGS_IsMarried() && (flasherinfo->vehicle_type != SETTINGS_TUNE(veh_type)))
    {
        log_push_error_point(0x48A5);
        cmdif_datainfo_responsedata.responsecode = CMDIF_ACK_MARRIED_FAIL;  //not a very good ACK
        goto cmdif_func_flasher_select_tune_custom_ctb_done;
    }

    //prevent using this custom tune on wrong/uncompatible vehicle
    status = obd2tune_attempt_to_validate_vehicle_type_against_tunecode(flasherinfo->vehicle_type,flasherinfo->ecminfo.codes[0][0],
                                                                        flasherinfo->ecminfo.vin,NULL);
    if (status == S_UNCOMPATIBLE)
    {
        log_push_error_point(0x48B2);
        cmdif_datainfo_responsedata.responsecode = CMDIF_ACK_UNCOMPATIBLE;
        goto cmdif_func_flasher_select_tune_custom_ctb_done;
    }
    status = S_SUCCESS;

    //##########################################################################
    // check tune flags
    //##########################################################################
    //Note: flasherinfo->tunecheck_status is set when validating tuneos file
    //from cmdif_func_set_extra_custom_file_g1
    if (flasherinfo->tunecheck_status == S_VINUNMATCH)
    {
        log_push_error_point(0x48B3);
        cmdif_datainfo_responsedata.responsecode = CMDIF_ACK_VIN_UNMATCH;
        goto cmdif_func_flasher_select_tune_custom_ctb_done;
    }

    if (obd2tune_tuneflags_isforcefull(flasherinfo->selected_tunelistinfo.flags[0]))
    {
        cmdif_datainfo_responsedata.responsecode = CMDIF_ACK_FORCE_FULL_FLASH;
        goto cmdif_func_flasher_select_tune_custom_ctb_done;
    }

    //prevent app timeout
    cmdif_internalresponse_ack(CMDIF_CMD_SELECT_TUNE_CUSTOM,
                               CMDIF_ACK_WAIT,NULL,0);

    //TODOQ: check off-road flag
    //TODOQ: support masked VIN case & segment check
    //##########################################################################
    //
    //##########################################################################
    //check if tune is supported in this vehicle
    status = obd2tune_check_customtune_support
        (flasherinfo->vehicle_type,flasherinfo->vin,tuneblock.tuneosfilename,
         &flasherinfo->ecminfo,&tunematch_masked,&flasherinfo->isvinmasked);
    if (status == S_NOTREQUIRED)
    {
        //do nothing
        flasherinfo->tunecheck_status = S_NOTREQUIRED;
        status = S_SUCCESS;
    }
    else if (status == S_VINUNMATCH)
    {
        log_push_error_point(0x48B4);
        cmdif_datainfo_responsedata.responsecode = CMDIF_ACK_VIN_UNMATCH;
        goto cmdif_func_flasher_select_tune_custom_ctb_done;
    }
    else if ((status == S_SUCCESS || status == S_TUNEUNMATCH) && flasherinfo->isvinmasked)
    {
        flasherinfo->tunecheck_status = S_TUNEMATCH_VINMASK;
        status = S_SUCCESS;
    }
    else if (status == S_TUNEUNMATCH && flasherinfo->ecm_count > 1 && 
             (tunematch_masked & (1<<1)) == 0)
    {
        /* TCM must match */
        log_push_error_point(0x488C);
        cmdif_datainfo_responsedata.responsecode = CMDIF_ACK_FAILED;
        goto cmdif_func_flasher_select_tune_custom_ctb_done;
    }
    else if (status == S_TUNEUNMATCH)
    {
        flasherinfo->tunecheck_status = status;
        status = S_SUCCESS;
    }
    else if (status != S_SUCCESS)
    {
        log_push_error_point(0x48B5);
        cmdif_datainfo_responsedata.responsecode = CMDIF_ACK_FAILED;
        goto cmdif_func_flasher_select_tune_custom_ctb_done;
    }
    
    //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    // Check if SPF is required
    //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    for(i=0;i<VEH_GetEcmCount(flasherinfo->vehicle_type);i++)
    {
        if(ECM_IsSpfRequired(VEH_GetEcmType(flasherinfo->vehicle_type,i)))
        {
            // SPF is required for this ECM
            if(obd2tune_check_ecm_stock(VEH_GetEcmType(flasherinfo->vehicle_type,i),
                                        GenerateStockSource_Use_USER_Folder_First, i) != S_SUCCESS)
            {
                // Required SPF not found
                log_push_error_point(0x48B6);
                cmdif_datainfo_responsedata.responsecode = CMDIF_ACK_SPF_NOT_FOUND;
                goto cmdif_func_flasher_select_tune_custom_ctb_done;
            }
        }//if(ECM_IsSpfRequired...
    }//for(i=0...

    if (cmd_flags & CUSTOM_TUNE_CTB_SELECT_CMD_FLAGS_FIND_OPTION)
    {
        hasRegularOptionFile = FALSE;
        //regular option files
        if (isgraph(tuneblock.optionfilename[0]))
        {
            if (genfs_userpathcorrection((u8*)tuneblock.optionfilename,
                                         sizeof(customtune_optionfile_report.filenames.regular_optionfile),
                                         customtune_optionfile_report.filenames.regular_optionfile) != S_SUCCESS)
            {
                //too long
                log_push_error_point(0x48B7);
                cmdif_datainfo_responsedata.responsecode = CMDIF_ACK_FAILED;
                goto cmdif_func_flasher_select_tune_custom_ctb_done;
            }
            hasRegularOptionFile = TRUE;
        }
        
        //prevent app timeout
        cmdif_internalresponse_ack(CMDIF_CMD_SELECT_TUNE_CUSTOM,
                                   CMDIF_ACK_WAIT,NULL,0);
        
        //special option files
        status = obd2tune_search_customtune_specialoptionfile_with_tuneos(flasherinfo->vehicle_type,
                                                                          tuneblock.tuneosfilename,
                                                                          flasherinfo->vin,
                                                                          customtune_optionfile_report.filenames.special_optionfile[0],
                                                                          &customtune_optionfile_report.filenames.special_optionflags[0],
                                                                          customtune_optionfile_report.filenames.special_optionfile[1],
                                                                          &customtune_optionfile_report.filenames.special_optionflags[1]);
        if (status == S_SUCCESS)
        {
            if (isgraph(customtune_optionfile_report.filenames.special_optionfile[0][0]))
            {
                //pcm has special option file; check if need to merge to regular option file
                if (isGeneralFileLookupSpecialOptionFileFlags_ListMergeFormat(customtune_optionfile_report.filenames.special_optionflags[0]))
                {
                    if (hasRegularOptionFile)
                    {
                        //prevent app timeout
                        cmdif_internalresponse_ack(CMDIF_CMD_SELECT_TUNE_CUSTOM,
                                                   CMDIF_ACK_WAIT,NULL,0);
                        
                        //TODOQK: check if this cause timeout problems on AppBoard
                        status = obd2tune_mergeoptionfile(customtune_optionfile_report.filenames.regular_optionfile,
                                                          customtune_optionfile_report.filenames.special_optionfile[0],
                                                          MERGED_OPTS_CUSTOM_FILENAME_PARTB);
                        if (status != S_SUCCESS)
                        {
                            log_push_error_point(0x48B8);
                            cmdif_datainfo_responsedata.responsecode = CMDIF_ACK_FAILED;
                            goto cmdif_func_flasher_select_tune_custom_ctb_done;
                        }
                        strcpy((char*)customtune_optionfile_report.filenames.regular_optionfile,
                               MERGED_OPTS_CUSTOM_FILENAME_PARTB);
                        memset((char*)customtune_optionfile_report.filenames.special_optionfile[0],
                               0,sizeof(customtune_optionfile_report.filenames.special_optionfile[0]));
                    }
                }//if (isGeneralFileLookupSpecialOptionFileFlags_ListMergeFormat...
            }
            
            if (isgraph(customtune_optionfile_report.filenames.special_optionfile[1][0]))
            {
                //do nothing
            }
        }
        else if(status == S_UNMATCH || status == S_NOTREQUIRED || status == S_INPUT)
        {
            log_push_error_point(0x48B9); // Special Options don't apply
        }
        else
        {
            log_push_error_point(0x48BA);
            cmdif_datainfo_responsedata.responsecode = CMDIF_ACK_FAILED;
            goto cmdif_func_flasher_select_tune_custom_ctb_done;
        }
    }//if (cmd_flags & CUSTOM_TUNE_SELECT_CMD_FLAGS_FIND_OPTION)...

    //prevent app timeout
    cmdif_internalresponse_ack(CMDIF_CMD_SELECT_TUNE_CUSTOM,
                               CMDIF_ACK_WAIT,NULL,0);

    if (flasherinfo->tunecheck_status == S_TUNEMATCH_VINMASK)
    {
        //NOTE: this is a special case, not really a failure
        //In this case, the vehicle tune is matched but VIN is masked, and full
        //reflash is required, therefore, user's confirmation is required
        //NOTE: only do full reflash PCM for now, assume 1st ECM is PCM
        flasherinfo->isfullreflash[0] = TRUE;

        if (ECM_IsDownloadAll(VEH_GetEcmType(flasherinfo->vehicle_type,0)))
        {
            cmdif_datainfo_responsedata.responsecode = CMDIF_ACK_FULL_FLASH;
            log_push_error_point(0x48BB);
        }
        else
        {
            cmdif_datainfo_responsedata.responsecode = CMDIF_ACK_FULL_FLASH_NOT_ALLOWED;
            log_push_error_point(0x48BC);
        }
        goto cmdif_func_flasher_select_tune_custom_ctb_done;
    }
    else if (flasherinfo->tunecheck_status == S_TUNEUNMATCH && 
             VEH_IsSuppressFullFlashMessage(flasherinfo->vehicle_type) == 0)
    {
        if (obd2tune_tuneflags_iscalonly(flasherinfo->selected_tunelistinfo.flags[0]))
        {
            log_push_error_point(0x48BD);
            cmdif_datainfo_responsedata.responsecode = CMDIF_ACK_TUNE_UNMATCH;
            goto cmdif_func_flasher_select_tune_custom_ctb_done;
        }
        else    //obd2tune_tuneflags_isallowfull(...)
        {
            //NOTE: this is a special case, not really a failure
            //In this case, the vehicle is not in a supported list, and a full
            //reflash is required, therefore, user's confirmation is required
            //NOTE: VIN must match
            //NOTE: only do full reflash PCM for now, assume 1st ECM is PCM
            flasherinfo->isfullreflash[0] = TRUE;

            if (ECM_IsDownloadAll(VEH_GetEcmType(flasherinfo->vehicle_type,0)))
            {   
                cmdif_datainfo_responsedata.responsecode = CMDIF_ACK_FULL_FLASH;
                log_push_error_point(0x48BE);
            }
            else
            {
                cmdif_datainfo_responsedata.responsecode = CMDIF_ACK_FULL_FLASH_NOT_ALLOWED;
                log_push_error_point(0x48BF);
            }
            goto cmdif_func_flasher_select_tune_custom_ctb_done;
        }
    }

    cmdif_datainfo_responsedata.responsecode = CMDIF_ACK_OK;

cmdif_func_flasher_select_tune_custom_ctb_done:
    if ((flasherinfo->flags & FLASHERFLAG_DISABLE_PRELOADED_TUNE) ||
        (flasherinfo->flags & FLASHERFLAG_DISABLE_PRELOADED_TUNE_REMOVE))
    {
        u32 currentrestriction;
        obd2tune_preloaded_tune_get_restriction(&currentrestriction);
        if(!currentrestriction)
        {
            response_attrib.preloadedtune_disabled = 1;
        }
    }

    cmdif_datainfo_responsedata.dataptr = __malloc(sizeof(response_attrib)+sizeof(customtune_optionfile_report));
    if (cmdif_datainfo_responsedata.dataptr == NULL)
    {
        cmdif_datainfo_responsedata.responsecode = CMDIF_ACK_FAILED;
        status = S_MALLOC;
        log_push_error_point(0x48C0);
    }
    else
    {
        cmdif_datainfo_responsedata.datalength = sizeof(response_attrib);
        memcpy((char*)cmdif_datainfo_responsedata.dataptr,
               (char*)&response_attrib,sizeof(response_attrib));
        if (cmd_flags & CUSTOM_TUNE_CTB_SELECT_CMD_FLAGS_FIND_OPTION)
        {
            cmdif_datainfo_responsedata.datalength = sizeof(response_attrib)+sizeof(customtune_optionfile_report);
            memcpy((char*)&cmdif_datainfo_responsedata.dataptr[sizeof(response_attrib)],
                   (char*)&customtune_optionfile_report,sizeof(customtune_optionfile_report));
        }
    }
    return status;
}

//------------------------------------------------------------------------------
// Select a custom tune by slot from indexfile.ctf on DEVICE
// Inputs:  u32 cmd_flags
//              - Bit0: 1 (use option), 0 (no option)
//              - Bit31..1: reserved (should be ZERO)
//          u16 cmd_slot (slot of indexfile.ctf; 1st slot is one)
// Engineer: Quyen Leba
// Note: only used in custom tune
// Note: obsolete this function when Advantage is ready for CTB
//------------------------------------------------------------------------------
u8 cmdif_func_flasher_select_tune_custom_advance(u32 cmd_flags, u16 cmd_slot)
{
    typedef struct
    {
        u8  preloadedtune_disabled          : 1;
        u8  reserved0                       : 7;
        u8  reserved1[3];
    }SelectCustomTuneResponseAttrib;
    SelectCustomTuneResponseAttrib response_attrib;
#define CUSTOM_TUNE_SELECT_CMD_FLAGS_USE_OPTION     (1<<0)
#define CUSTOM_TUNE_SELECT_CMD_FLAGS_FIND_OPTION    (1<<1)
    customtune_block tuneblock;
    union
    {
        struct
        {
            u8  regular_optionfile[32];
            u8  special_optionfile[2][32];
            u32 special_optionflags[2];
        }filenames;
        u8  buffer[128];
    }customtune_optionfile_report;  //for CUSTOM_TUNE_SELECT_CMD_FLAGS_FIND_OPTION
    bool isUseOption;
    bool hasRegularOptionFile;
    u32 i;
    u16 selected_slot_index;
    u8  tunematch_masked;
    u8  status;

    memset((char*)&customtune_optionfile_report,0,sizeof(customtune_optionfile_report));
    memset((char*)&response_attrib,0,sizeof(response_attrib));
    status = S_SUCCESS;
    cmdif_datainfo_responsedata.responsecode = CMDIF_ACK_FAILED;
    if (flasherinfo == NULL)
    {
        log_push_error_point(0x4867);
        return S_BADCONTENT;
    }
    else if (SETTINGS_IsNoCustomDevice() || SETTINGS_Is50StateLegalDevice())
    {
        log_push_error_point(0x4868);
        return S_NOTSUPPORT;
    }
    else if (cmd_slot == 0)
    {
        log_push_error_point(0x487D);
        return S_INPUT;
    }
    selected_slot_index = cmd_slot - 1;   //because 1st slot is one

    //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    // Validate indexfile.ctf
    //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    status = obd2tune_validate_ctf_file(DEFAULT_CUSTOM_TUNE_INDEX_FILENAME);
    if (status == S_OPENFILE)
    {
        log_push_error_point(0x4879);
        cmdif_datainfo_responsedata.responsecode = CMDIF_ACK_FILE_NOT_FOUND;
        goto cmdif_func_flasher_select_tune_custom_advance_done;
    }
    else if (status == S_NOTSUPPORT)
    {
        log_push_error_point(0x487A);
        cmdif_datainfo_responsedata.responsecode = CMDIF_ACK_UNSUPPORTED;
        goto cmdif_func_flasher_select_tune_custom_advance_done;
    }
    else if (status == S_CRC32E)
    {
        log_push_error_point(0x487B);
        goto cmdif_func_flasher_select_tune_custom_advance_done;
    }
    else if (status != S_SUCCESS)
    {
        log_push_error_point(0x487C);
        goto cmdif_func_flasher_select_tune_custom_advance_done;
    }
    //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    // Get tune block
    //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    status = obd2tune_get_custom_tune_block(selected_slot_index,&tuneblock);
    if (status == S_CRC32E)
    {
        log_push_error_point(0x4869);
        goto cmdif_func_flasher_select_tune_custom_advance_done;
    }
    else if (status == S_NOTSUPPORT)
    {
        log_push_error_point(0x486A);
        cmdif_datainfo_responsedata.responsecode = CMDIF_ACK_NOT_SUPPORTED;
        goto cmdif_func_flasher_select_tune_custom_advance_done;
    }
    else if (status != S_SUCCESS)
    {
        log_push_error_point(0x486B);
        goto cmdif_func_flasher_select_tune_custom_advance_done;
    }

    //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    // Validate tune block
    //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

    // Validation can take a awhile, send Wait ack
    cmdif_internalresponse_ack(CMDIF_CMD_SELECT_TUNE_CUSTOM_ADVANCE,
                               CMDIF_ACK_WAIT,NULL,0);

    status = obd2tune_validate_custom_tune_block(&tuneblock,FALSE); //must check signature
    switch(status)
    {
    case S_SUCCESS:
        break;
    case S_SERIALUNMATCH:
        log_push_error_point(0x486C);
        cmdif_datainfo_responsedata.responsecode = CMDIF_ACK_SERIAL_UNMATCH;
        goto cmdif_func_flasher_select_tune_custom_advance_done;
    case S_UNMATCH:
        //bad signature
        log_push_error_point(0x486D);
        cmdif_datainfo_responsedata.responsecode = CMDIF_ACK_TUNE_UNMATCH;
        goto cmdif_func_flasher_select_tune_custom_advance_done;
    case S_BADCONTENT:
        log_push_error_point(0x486E);
        goto cmdif_func_flasher_select_tune_custom_advance_done;
    case S_FILENOTFOUND:
        log_push_error_point(0x486F);
        cmdif_datainfo_responsedata.responsecode = CMDIF_ACK_FILE_NOT_FOUND;
        goto cmdif_func_flasher_select_tune_custom_advance_done;
    case S_CRC32E:
        log_push_error_point(0x4870);
        goto cmdif_func_flasher_select_tune_custom_advance_done;
    default:
        log_push_error_point(0x4871);
        goto cmdif_func_flasher_select_tune_custom_advance_done;
    }

    //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    // Process tune block
    //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    isUseOption = FALSE;
    if (cmd_flags & CUSTOM_TUNE_SELECT_CMD_FLAGS_USE_OPTION)
    {
        isUseOption = TRUE;
    }
    status = obd2tune_process_custom_tune_block(&tuneblock,isUseOption);
    if (status != S_SUCCESS)
    {
        log_push_error_point(0x4872);
        goto cmdif_func_flasher_select_tune_custom_advance_done;
    }

    if (obd2_validate_ecmbinarytype(flasherinfo->vehicle_type) != S_SUCCESS)
    {
        log_push_error_point(0x4873);
        cmdif_datainfo_responsedata.responsecode = CMDIF_ACK_INVALID_BINARYTYPE;
        goto cmdif_func_flasher_select_tune_custom_advance_done;
    }
    if (SETTINGS_IsMarried() && (flasherinfo->vehicle_type != SETTINGS_TUNE(veh_type)))
    {
        log_push_error_point(0x4874);
        cmdif_datainfo_responsedata.responsecode = CMDIF_ACK_MARRIED_FAIL;  //not a very good ACK
        goto cmdif_func_flasher_select_tune_custom_advance_done;
    }

    //prevent using this custom tune on wrong/uncompatible vehicle
    status = obd2tune_attempt_to_validate_vehicle_type_against_tunecode(flasherinfo->vehicle_type,flasherinfo->ecminfo.codes[0][0],
                                                                        flasherinfo->ecminfo.vin,NULL);
    if (status == S_UNCOMPATIBLE)
    {
        log_push_error_point(0x4892);
        cmdif_datainfo_responsedata.responsecode = CMDIF_ACK_UNCOMPATIBLE;
        goto cmdif_func_flasher_select_tune_custom_advance_done;
    }
    status = S_SUCCESS;

    //##########################################################################
    // check tune flags
    //##########################################################################
    //Note: flasherinfo->tunecheck_status is set when validating tuneos file
    //from cmdif_func_set_extra_custom_file_g1
    if (flasherinfo->tunecheck_status == S_VINUNMATCH)
    {
        log_push_error_point(0x4875);
        cmdif_datainfo_responsedata.responsecode = CMDIF_ACK_VIN_UNMATCH;
        goto cmdif_func_flasher_select_tune_custom_advance_done;
    }

    if (obd2tune_tuneflags_isforcefull(flasherinfo->selected_tunelistinfo.flags[0]))
    {
        cmdif_datainfo_responsedata.responsecode = CMDIF_ACK_FORCE_FULL_FLASH;
        goto cmdif_func_flasher_select_tune_custom_advance_done;
    }

    //prevent app timeout
    cmdif_internalresponse_ack(CMDIF_CMD_SELECT_TUNE_CUSTOM_ADVANCE,
                               CMDIF_ACK_WAIT,NULL,0);

    //TODOQ: check off-road flag
    //TODOQ: support masked VIN case & segment check
    //##########################################################################
    //
    //##########################################################################
    //check if tune is supported in this vehicle
    status = obd2tune_check_customtune_support
        (flasherinfo->vehicle_type,flasherinfo->vin,tuneblock.tuneosfilename,
         &flasherinfo->ecminfo,&tunematch_masked,&flasherinfo->isvinmasked);
    if (status == S_NOTREQUIRED)
    {
        //do nothing
        flasherinfo->tunecheck_status = S_NOTREQUIRED;
        status = S_SUCCESS;
    }
    else if (status == S_VINUNMATCH)
    {
        log_push_error_point(0x487F);
        cmdif_datainfo_responsedata.responsecode = CMDIF_ACK_VIN_UNMATCH;
        goto cmdif_func_flasher_select_tune_custom_advance_done;
    }
    else if ((status == S_SUCCESS || status == S_TUNEUNMATCH) && flasherinfo->isvinmasked)  //TODOQ: confirm this
    {
        flasherinfo->tunecheck_status = S_TUNEMATCH_VINMASK;
        status = S_SUCCESS;
    }
    else if (status == S_TUNEUNMATCH && flasherinfo->ecm_count > 1 && 
             (tunematch_masked & (1<<1)) == 0 && 
             obd2_get_oemtype() != OemType_FORD)
    {
        /* TCM Backdate Check
         * If TCM or Tune OS does not match and vehicle is not Ford, do not
         * allow TCM backdate tuning. We do however allow Ford TCM backdating.
         */
        log_push_error_point(0x48C1);
        cmdif_datainfo_responsedata.responsecode = CMDIF_ACK_FAILED;
        goto cmdif_func_flasher_select_tune_custom_advance_done;
    }
    else if (status == S_TUNEUNMATCH)
    {
        flasherinfo->tunecheck_status = status;
        status = S_SUCCESS;
    }
    else if (status != S_SUCCESS)
    {
        log_push_error_point(0x487E);
        cmdif_datainfo_responsedata.responsecode = CMDIF_ACK_FAILED;
        goto cmdif_func_flasher_select_tune_custom_advance_done;
    }
    
    //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    // Check if SPF is required
    //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    for(i=0;i<VEH_GetEcmCount(flasherinfo->vehicle_type);i++)
    {
        if(ECM_IsSpfRequired(VEH_GetEcmType(flasherinfo->vehicle_type,i)))
        {
            // SPF is required for this ECM
            if(obd2tune_check_ecm_stock(VEH_GetEcmType(flasherinfo->vehicle_type,i),
                                        GenerateStockSource_Use_USER_Folder_First, i) != S_SUCCESS)
            {
                // Required SPF not found
                log_push_error_point(0x4888);
                cmdif_datainfo_responsedata.responsecode = CMDIF_ACK_SPF_NOT_FOUND;
                goto cmdif_func_flasher_select_tune_custom_advance_done;
            }
        }//if(ECM_IsSpfRequired...
    }//for(i=0...

    if (cmd_flags & CUSTOM_TUNE_SELECT_CMD_FLAGS_FIND_OPTION)
    {
        hasRegularOptionFile = FALSE;
        //regular option files
        if (isgraph(tuneblock.optionfilename[0]))
        {
            if (genfs_userpathcorrection((u8*)tuneblock.optionfilename,
                                         sizeof(customtune_optionfile_report.filenames.regular_optionfile),
                                         customtune_optionfile_report.filenames.regular_optionfile) != S_SUCCESS)
            {
                //too long
                log_push_error_point(0x4897);
                cmdif_datainfo_responsedata.responsecode = CMDIF_ACK_FAILED;
                goto cmdif_func_flasher_select_tune_custom_advance_done;
            }
            hasRegularOptionFile = TRUE;
        }
        
        //prevent app timeout
        cmdif_internalresponse_ack(CMDIF_CMD_SELECT_TUNE_CUSTOM_ADVANCE,
                                   CMDIF_ACK_WAIT,NULL,0);
        
        //special option files
        status = obd2tune_search_customtune_specialoptionfile_with_tuneos(flasherinfo->vehicle_type,
                                                                          tuneblock.tuneosfilename,
                                                                          flasherinfo->vin,
                                                                          customtune_optionfile_report.filenames.special_optionfile[0],
                                                                          &customtune_optionfile_report.filenames.special_optionflags[0],
                                                                          customtune_optionfile_report.filenames.special_optionfile[1],
                                                                          &customtune_optionfile_report.filenames.special_optionflags[1]);
        if (status == S_SUCCESS)
        {
            if (isgraph(customtune_optionfile_report.filenames.special_optionfile[0][0]))
            {
                //pcm has special option file; check if need to merge to regular option file
                if (isGeneralFileLookupSpecialOptionFileFlags_ListMergeFormat(customtune_optionfile_report.filenames.special_optionflags[0]))
                {
                    if (hasRegularOptionFile)
                    {
                        //prevent app timeout
                        cmdif_internalresponse_ack(CMDIF_CMD_SELECT_TUNE_CUSTOM_ADVANCE,
                                                   CMDIF_ACK_WAIT,NULL,0);
                        
                        //TODOQK: check if this cause timeout problems on AppBoard
                        status = obd2tune_mergeoptionfile(customtune_optionfile_report.filenames.regular_optionfile,
                                                          customtune_optionfile_report.filenames.special_optionfile[0],
                                                          MERGED_OPTS_CUSTOM_FILENAME_PARTB);
                        if (status != S_SUCCESS)
                        {
                            log_push_error_point(0x4899);
                            cmdif_datainfo_responsedata.responsecode = CMDIF_ACK_FAILED;
                            goto cmdif_func_flasher_select_tune_custom_advance_done;
                        }
                        strcpy((char*)customtune_optionfile_report.filenames.regular_optionfile,
                               MERGED_OPTS_CUSTOM_FILENAME_PARTB);
                        memset((char*)customtune_optionfile_report.filenames.special_optionfile[0],
                               0,sizeof(customtune_optionfile_report.filenames.special_optionfile[0]));
                    }
                }//if (isGeneralFileLookupSpecialOptionFileFlags_ListMergeFormat...
            }
            
            if (isgraph(customtune_optionfile_report.filenames.special_optionfile[1][0]))
            {
                //do nothing
            }
        }
        else if(status == S_UNMATCH || status == S_NOTREQUIRED || status == S_INPUT)
        {
            log_push_error_point(0x489A); // Special Options don't apply
        }
        else
        {
            log_push_error_point(0x4898);
            cmdif_datainfo_responsedata.responsecode = CMDIF_ACK_FAILED;
            goto cmdif_func_flasher_select_tune_custom_advance_done;
        }
    }//if (cmd_flags & CUSTOM_TUNE_SELECT_CMD_FLAGS_FIND_OPTION)...

    //prevent app timeout
    cmdif_internalresponse_ack(CMDIF_CMD_SELECT_TUNE_CUSTOM_ADVANCE,
                               CMDIF_ACK_WAIT,NULL,0);

    if (flasherinfo->tunecheck_status == S_TUNEMATCH_VINMASK)
    {
        //NOTE: this is a special case, not really a failure
        //In this case, the vehicle tune is matched but VIN is masked, and full
        //reflash is required, therefore, user's confirmation is required
        //NOTE: only do full reflash PCM for now, assume 1st ECM is PCM
        flasherinfo->isfullreflash[0] = TRUE;

        if (ECM_IsDownloadAll(VEH_GetEcmType(flasherinfo->vehicle_type,0)))
        {
            cmdif_datainfo_responsedata.responsecode = CMDIF_ACK_FULL_FLASH;
            log_push_error_point(0x4876);
        }
        else
        {
            cmdif_datainfo_responsedata.responsecode = CMDIF_ACK_FULL_FLASH_NOT_ALLOWED;
            log_push_error_point(0x4889);
        }
        goto cmdif_func_flasher_select_tune_custom_advance_done;
    }
    else if (flasherinfo->tunecheck_status == S_TUNEUNMATCH && 
             VEH_IsSuppressFullFlashMessage(flasherinfo->vehicle_type) == 0)
    {
#ifdef __DCX_MANUF__    // DCX does not allow downloading of unmatched tunes

        log_push_error_point(0x4877);
        cmdif_datainfo_responsedata.responsecode = CMDIF_ACK_TUNE_UNMATCH;
        goto cmdif_func_flasher_select_tune_custom_advance_done;     

#else
        if (obd2tune_tuneflags_iscalonly(flasherinfo->selected_tunelistinfo.flags[0]))
        {
            log_push_error_point(0x4877);
            cmdif_datainfo_responsedata.responsecode = CMDIF_ACK_TUNE_UNMATCH;
            goto cmdif_func_flasher_select_tune_custom_advance_done;
        }
        else    //obd2tune_tuneflags_isallowfull(...)
        {
            //NOTE: this is a special case, not really a failure
            //In this case, the vehicle is not in a supported list, and a full
            //reflash is required, therefore, user's confirmation is required
            //NOTE: VIN must match
            //NOTE: only do full reflash PCM for now, assume 1st ECM is PCM
            flasherinfo->isfullreflash[0] = TRUE;

            if (ECM_IsDownloadAll(VEH_GetEcmType(flasherinfo->vehicle_type,0)))
            {   
                cmdif_datainfo_responsedata.responsecode = CMDIF_ACK_FULL_FLASH;
                log_push_error_point(0x4878);
            }
            else
            {
                cmdif_datainfo_responsedata.responsecode = CMDIF_ACK_FULL_FLASH_NOT_ALLOWED;
                log_push_error_point(0x488A);
            }
            goto cmdif_func_flasher_select_tune_custom_advance_done;
        }
#endif
    }

    cmdif_datainfo_responsedata.responsecode = CMDIF_ACK_OK;

cmdif_func_flasher_select_tune_custom_advance_done:
    if ((flasherinfo->flags & FLASHERFLAG_DISABLE_PRELOADED_TUNE) ||
        (flasherinfo->flags & FLASHERFLAG_DISABLE_PRELOADED_TUNE_REMOVE))
    {
        u32 currentrestriction;
        obd2tune_preloaded_tune_get_restriction(&currentrestriction);
        if(!currentrestriction)
        {
            response_attrib.preloadedtune_disabled = 1;
        }
    }

    cmdif_datainfo_responsedata.dataptr = __malloc(sizeof(response_attrib)+sizeof(customtune_optionfile_report));
    if (cmdif_datainfo_responsedata.dataptr == NULL)
    {
        cmdif_datainfo_responsedata.responsecode = CMDIF_ACK_FAILED;
        status = S_MALLOC;
        log_push_error_point(0x4891);
    }
    else
    {
        cmdif_datainfo_responsedata.datalength = sizeof(response_attrib);
        memcpy((char*)cmdif_datainfo_responsedata.dataptr,
               (char*)&response_attrib,sizeof(response_attrib));
        if (cmd_flags & CUSTOM_TUNE_SELECT_CMD_FLAGS_FIND_OPTION)
        {
            cmdif_datainfo_responsedata.datalength = sizeof(response_attrib)+sizeof(customtune_optionfile_report);
            memcpy((char*)&cmdif_datainfo_responsedata.dataptr[sizeof(response_attrib)],
                   (char*)&customtune_optionfile_report,sizeof(customtune_optionfile_report));
        }
    }
    return status;
}

//------------------------------------------------------------------------------
// Get file from 
// Inputs:  u32 cmd_flags
//          GeneralFileLookupItemType item_type
//          u8  code_index (processor code where 0:ecm & 1:tcm)
// Engineer: Quyen Leba
// Note: only used in custom tune
// IMPORTANT: rely on flasherinfo for search parameters
//------------------------------------------------------------------------------
u8 cmdif_func_flasher_get_file_from_option_lookup(u32 cmd_flags,
                                                  GeneralFileLookupItemType item_type,
                                                  u8 code_index)
{
    u8  *bptr;
    u32 optionflags;
    u8  status;

    cmdif_datainfo_responsedata.responsecode = CMDIF_ACK_FAILED;
    cmdif_datainfo_responsedata.dataptr = NULL;
    bptr = __malloc(40);    //4+4+32
    if (bptr == NULL)
    {
        log_push_error_point(0x488F);
        cmdif_datainfo_responsedata.responsecode = CMDIF_ACK_FAILED;
        return S_MALLOC;
    }
    memset((char*)bptr,0,40);

    if (item_type == GeneralFileLookupItemType_SpecialOptionFile)
    {
        status = obd2tune_search_glf_for_special_option_file(DEFAULT_OPTION_LOOKUP_FILE,
                                                             flasherinfo->vin,
                                                             flasherinfo->ecminfo.codes[code_index][0],
                                                             GeneralFileLookupFileProcessorCodeType_Regular,
                                                             &bptr[4],&optionflags);
        if (status == S_SUCCESS)
        {
            cmdif_datainfo_responsedata.dataptr = bptr;
            cmdif_datainfo_responsedata.datalength = 4+strlen((char*)bptr)+1;
            cmdif_datainfo_responsedata.responsecode = CMDIF_ACK_OK;
        }
        else if (status == S_NOTREQUIRED)
        {
            cmdif_datainfo_responsedata.responsecode = CMDIF_ACK_OK;
        }
        else if (isgraph(flasherinfo->ecminfo.second_codes[code_index][0][0]))
        {
            status = obd2tune_search_glf_for_special_option_file(DEFAULT_OPTION_LOOKUP_FILE,
                                                                 flasherinfo->vin,
                                                                 flasherinfo->ecminfo.second_codes[code_index][0],
                                                                 GeneralFileLookupFileProcessorCodeType_SecondCode,
                                                                 &bptr[4],&optionflags);
            if (status == S_SUCCESS)
            {
                cmdif_datainfo_responsedata.dataptr = bptr;
                cmdif_datainfo_responsedata.datalength = 4+strlen((char*)bptr)+1;
                cmdif_datainfo_responsedata.responsecode = CMDIF_ACK_OK;
            }
        }
    }
    else
    {
        cmdif_datainfo_responsedata.responsecode = CMDIF_ACK_UNSUPPORTED;
        status = S_NOTSUPPORT;
    }

    if (cmdif_datainfo_responsedata.dataptr == NULL && bptr != NULL)
    {
        __free(bptr);
    }

    return status;
}

//------------------------------------------------------------------------------
// This is flexible enough to be used a multi-purpose general response command
// but it is currently used exclusively for 6.4L device-only options. 
// TODO: needs to be reworked when being applied to other non-6.4L functions
//
// Input:   u8  ecm_id
// Return:  u8  status
// Engineer: Patrick Downs, Quyen Leba
//------------------------------------------------------------------------------
u8 cmdif_func_flasher_general_request(u8 ecm_id)
{
//    if (flasherinfo == NULL)
//    {
//        log_push_error_point(0x484A);
//        cmdif_datainfo_responsedata.responsecode = CMDIF_ACK_FAILED;
//        return S_BADCONTENT;
//    }

    //TODOQ: plan for a more general request
    cmdif_datainfo_responsedata.responsecode = CMDIF_ACK_DONE;
    return S_SUCCESS;
}

//------------------------------------------------------------------------------
// Inputs:  u8  *optiondata
//          [1:ecm_id][n:optionfilename][1:NULL]
//          OptionSource optionsource
// Return:  u8  status
// Engineer: Patrick Downs, Quyen Leba
//------------------------------------------------------------------------------
u8 cmdif_func_flasher_use_option(u8 *optiondata, OptionSource optionsource)
{
    u8  status;
    u8  ecm_id;
    u8  optionfilename[MAX_FILENAME_LENGTH+1];

    cmdif_datainfo_responsedata.responsecode = CMDIF_ACK_FAILED;
    if (flasherinfo == NULL)
    {
        log_push_error_point(0x484B);
        return S_BADCONTENT;
    }
    else if (optiondata == NULL)
    {
        log_push_error_point(0x4860);
        return S_INPUT;
    }

    ecm_id = optiondata[0];
    if (optionsource == OptionSoure_SpecialOption)
    {
        flasherinfo->selected_tunelistinfo.useoption[ecm_id] |= TUNEUSEOPTION_SPECIAL;
        flasherinfo->selected_tunelistinfo.specialoptionfilename[ecm_id][0] = NULL;
    }
    else    //assume OptionSoure_Regular
    {
        //always process regular options first
        flasherinfo->selected_tunelistinfo.useoption[ecm_id] = TUNEUSEOPTION_NONE;
        flasherinfo->selected_tunelistinfo.optionfilename[ecm_id][0] = NULL;
    }

    if (file_isfilenamevalid(&optiondata[1],MAX_TUNE_NAME_LENGTH) == FALSE)
    {
        log_push_error_point(0x484C);
        return S_INPUT;
    }
    else if (ecm_id >= ECM_MAX_COUNT)
    {
        log_push_error_point(0x484D);
        cmdif_datainfo_responsedata.responsecode = CMDIF_ACK_INVALID_PCMNUMBER;
        return S_ERROR;
    }

    if (flasherinfo->flashtype == CMDIF_ACT_PRELOADED_FLASHER)
    {
        status = file_general_getfilename(&optiondata[1],optionfilename);
    }
    else if (flasherinfo->flashtype == CMDIF_ACT_CUSTOM_FLASHER)
    {
        status = file_general_getfilename(&optiondata[1],optionfilename);
    }
    else
    {
        status = S_ERROR;
    }
    if (status != S_SUCCESS)
    {
        log_push_error_point(0x484E);
        return status;
    }

    //Note: option file already checked
    cmdif_datainfo_responsedata.responsecode = CMDIF_ACK_OK;
    if (optionsource == OptionSoure_SpecialOption)
    {
        flasherinfo->selected_tunelistinfo.useoption[ecm_id] |= TUNEUSEOPTION_SPECIAL;
        strcpy((char*)flasherinfo->selected_tunelistinfo.specialoptionfilename[ecm_id],
               (char*)optionfilename);
    }
    else    //assume OptionSoure_Regular
    {
        flasherinfo->selected_tunelistinfo.useoption[ecm_id] |= TUNEUSEOPTION_REGULAR;
        strcpy((char*)flasherinfo->selected_tunelistinfo.optionfilename[ecm_id],
               (char*)optionfilename);
    }
    return S_SUCCESS;
}
