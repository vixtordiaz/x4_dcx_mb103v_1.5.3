/**
  **************** (C) COPYRIGHT 2011 SCT Performance, LLC *********************
  * File Name          : obd2scp.h
  * Device / System    : SCT products using the "CommonCode" platform
  * Author             : Quyen Leba
  *
  * Version            : 1 
  * Date               : 05/02/2011
  * Description        : 
  *                    : 
  *
  *
  * History            : 05/02/2011 M. Davis
  *                    :   Add SCT Copyright header,
  *
  ******************************************************************************
  */

#ifndef __OBD2SCP_H
#define __OBD2SCP_H

#include <arch/gentype.h>
#include <board/genplatform.h>
#include <common/genmanuf_overload.h>
#include "obd2.h"

#define VID_LIST_SCP            0x09FF80, 0x01FF80, 0x019F80, 0x1C8080
#define VID_LIST_SCP_COUNT      4
#define VID_LIST_SCP32          0x0100C0, 0x002E80
#define VID_LIST_SCP32_COUNT    2

typedef enum
{
    Obd2scpType_24Bit,
    Obd2scpType_32Bit,
}Obd2scpType;

typedef enum
{
    TransferExitFromUpload,
    TransferExitFromDownload,
}TransferExitMode;

enum
{
    Obd2scpHelper_ReadblockByCmd35single36single37      = 0,
};

//IMPORTANT: MB & VB share this struct, changes to this struct must be made in 
//              both and versions must be incremented.
#define OBD2SCP_RXINFO_VERSION      2
typedef struct 
{
    u8 version;
    u32 max_retry_count;
    bool raw_receive;           //TRUE: don't process received frame
    bool response_expected;     //there's no receive frame
    u8  *rxbuffer;
    u16 rxlength;
    u8  cmd;                    //same as u8 service of txinfo;
    u8  response_cmd_offset;    //usually is 0x40, to calculate response_cmd
    u8  first_frame_data_offset;
    u8  errorcode;
    u8  transfermode;           //SCP_MODE_NODE or SCP_MODE_FUNCTIONAL
    u8  ecmaddress;             //0x10
    u8  tester_frame_address;   //0xF0, 0x05 (key on)
    u8  tester_ifr_address;
    u32 rx_timeout_ms;          /* Low level driver COM timeout in miliseconds */
}obd2scp_rxinfo;

//IMPORTANT: MB & VB share this struct, changes to this struct must be made in 
//              both and versions must be incremented.
#define OBD2SCP_SERVICEDATA_VERSION     2
typedef struct 
{
    u8 version;
    u8  ecm_id;
    u8  devicenode_id;
    bool broadcast;
    bool response_expected;
    u8  service;
    u8  subservice;
    u8  service_controldatabuffer[8];
    u8  service_controldatalength;
    u8  *txdata;
    u16 txdatalength;
    u8  *rxdata;
    u16 rxdatalength;
    u8  errorcode;
    u8  transfermode;
    u8  txretry;
    u8  rxtimeout_extended;
    u32 rx_timeout_ms;          /* Low level driver COM timeout in miliseconds */
}Obd2scp_ServiceData;

//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
void obd2scp_init(Obd2scpType type);
void obd2scp_datalogmode();
void obd2scp_normalmode();
void obd2scp_rxinfo_init(obd2scp_rxinfo *rxinfo);
void obd2scp_servicedata_init(Obd2scp_ServiceData *servicedata);
u8 obd2scp_set_controldata(Obd2scp_ServiceData *servicedata,
                           u8 *controldata, u8 controldatalength);
u8 obd2scp_tx(u8 *txbuffer, u16 txlength, u8 speedmode);
u8 obd2scp_rx(obd2scp_rxinfo *rxinfo);
u8 obd2scp_rxraw(u8 *data, u8 *datalength);
u8 scp_txrx(Obd2scp_ServiceData *servicedata);
u8 obd2scp_txrx_simple(Obd2scp_ServiceData *servicedata);

//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
u8 obd2scp_ping();
u8 obd2scp_read_data_bypid_mode1(u8 pid, u8 pidsize, u8 *output);       //$01
u8 obd2scp_return_normal_mode();                                        //$20
u8 obd2scp_read_data_bypid(u16 pid, u8 pidsize, u8 *output);            //$22
u8 obd2scp_read_memory_address(u32 address, u16 length,                 //$23
                               MemoryAddressType addrtype,
                               u8 *data, u16 *datalength, bool isHighspeed);
u8 obd2scp_stop_transmitting_requested_data();                          //$25
u8 obd2scp_request_diagnostic_data_packet(u8 datarate,                  //$2A
                                          u8 *packetidlist, u8 packetidcount);
u8 obd2scp_dynamicallydefinediagnosticdatapacket(u8 packetid, PidType type,
                                                 u8 size, u8 position,
                                                 u32 address,
                                                 MemoryAddressType addrtype);
u8 obd2scp_start_diagnostic_routine_bytestnumber(u8 testnumber,         //$31
                                                 u8 *testdata,
                                                 u8 testdatalength);
u8 obd2scp_stop_diagnostic_routine_bytestnumber(u8 testnumber);         //$32
u8 obd2scp_request_download(u32 address, u16 length,                    //$34
                            MemoryAddressType addrtype);
u8 obd2scp_request_upload(u32 address, u16 length,                      //$35
                          MemoryAddressType addrtype);
u8 obd2scp_transfer_data_upload(u8 *data, u16 length);                  //$36
u8 obd2scp_transfer_data_download(u8 *data, u16 length);                //$36
u8 obd2scp_request_transfer_exit(MemoryAddressType addrtype,            //$37
                                 TransferExitMode exitmode);

//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
u8 obd2scp_readblock_bycmd35single36single37(u32 address,
                                             MemoryAddressType addrtype,
                                             u16 length, u8 *buffer);

u8 obd2scp_getvidaddress(u32 *vidaddress);
u8 obd2scp_readvid_by_vehicletype(u8 *vid, u16 ford_veh_type);
u8 obd2scp_readpats_by_vehicletype(u8 *pats, u16 ford_veh_type);
u8 obd2scp_readsecuritybyte_by_vehicletype(u8 *securitybyte, u16 ford_veh_type);

u8 obd2scp_readvin(u8 *vin, VehicleCommType *vehiclecommtype);
u8 obd2scp_readstrategy(u8 *strategy, VehicleCommType *commtype);
u8 obd2scp_readecminfo(ecm_info *ecm);
u8 obd2scp_cleardtc();
u8 obd2scp_readdtc(dtc_info *info);

u16 obd2scp_generatekey(u8 *seed, u8 l_seed, u8 *key);
u8 obd2scp_wakeup(u8 veh_type, u8 ecm_index);
u8 obd2scp_unlockecm(u8 ecm_type, bool seed_peek, u8 *l_seed);
u8 obd2scp_generatebaudkey(u8 *seed, u8 l_seed, u8 *key);
u8 obd2scp_requesthighspeed(VehicleCommType commtype, u8 l_seed);
u8 obd2scp_erase_ecm(u8 ecm_type, bool is_cal_only);
u8 obd2scp_reset_ecm(u8 ecm_type);

u8 obd2scp_clear_kam(void);

u8 obd2scp_validatepid(DlxBlock *dlxblock,
                       VehicleCommLevel commlevel);
u8 obd2scp_validatedmr(DlxBlock *dlxblock,
                       VehicleCommType commtype,
                       VehicleCommLevel commlevel);
u8 obd2scp_validateByRapidPacketSetup(DlxBlock *dlxblock,
                       VehicleCommType commtype,
                       VehicleCommLevel commlevel);
u8 obd2scp_datalog_evaluatesignalsetup(VehicleCommLevel vehiclecommlevel);
u8 obd2scp_datalog_getdata(VehicleCommLevel *vehiclecommlevel);
void obd2scp_datalog_sendtesterpresent();
u8 obd2scp_datalog_cleanup_packets();
u8 obd2scp_datalog_initiatepackets(VehicleCommLevel *vehiclecommlevel);
u8 obd2scp_download_test_key_on(u16 ecm_type);
u8 obd2scp_getinfo(obd2_info *obd2info);

#endif  //__OBD2SCP_H
