/**
  **************** (C) COPYRIGHT 2011 SCT Performance, LLC *********************
  * File Name          : file.c
  * Device / System    : SCT products using the "CommonCode" platform
  * Author             : Quyen Leba
  *
  * Version            : 1 
  * Date               : 05/02/2011
  * Description        : 
  *                    : 
  *
  *
  * History            : 05/02/2011 M. Davis
  *                    :   Add SCT Copyright header,
  *
  ******************************************************************************
  */

#include <common/statuscode.h>
#include <fs/genfs.h>
#include <common/cmdif.h>
#include <common/crc32.h>
#include <common/tea.h>
#include "file.h"

extern const u32 stockfile_tea_key[4];              //from filestock.c

//------------------------------------------------------------------------------
// Check if a file exists
// Input:   u8  *filename
// Return:  bool status
//------------------------------------------------------------------------------
bool file_isexist(u8 *filename)
{
    F_FILE  *fptr;
    
    fptr = genfs_general_openfile(filename,"rb");
    if (fptr)
    {
        genfs_closefile(fptr);
        return TRUE;
    }
    else
    {
        return FALSE;
    }
}

//------------------------------------------------------------------------------
// Check if a filename is valid
// Inputs:  u8  *filename
//          u32 maxnamelength
// Return:  bool status
//------------------------------------------------------------------------------
bool file_isfilenamevalid(u8 *filename, u32 maxnamelength)
{
    u32 namelength;
    
    namelength = strlen((char*)filename);
    if ((namelength > maxnamelength) || (namelength <= 4))
    {
        return FALSE;
    }
    else
    {
        return TRUE;
    }
}

//------------------------------------------------------------------------------
// Get file name from path
// Input:   u8  *input_string
// Output:  u8  *output_filename
// Return:  u8  status
//------------------------------------------------------------------------------
u8 file_getfilename_from_path(u8 *path, u8 *filename)
{
    u8  *bptr;
    u8  *bptr_prev;
    u32 length;

    if (path == NULL || filename == NULL)
    {
        return S_INPUT;
    }

    bptr_prev = path;
    while(1)
    {
        bptr = (u8*)strstr((char*)bptr_prev,"\\");
        if (bptr == NULL)
        {
            break;
        }
        bptr_prev = bptr + 1;
    }
    length = strlen((char*)bptr_prev);
    if (length > 0 && strlen((char*)bptr_prev) < MAX_FILENAME_LENGTH)
    {
        strcpy((char*)filename,(char*)bptr_prev);
    }
    else
    {
        filename[0] = NULL;
        return S_FAIL;
    }
    return S_SUCCESS;
}

//------------------------------------------------------------------------------
// Input:   u8  *input_string
// Output:  u8  *output_filename
// Return:  u8  status
//------------------------------------------------------------------------------
u8 file_general_getfilename(u8 *input_string, u8 *output_filename)
{
    if (input_string == NULL || output_filename == NULL)
    {
        return S_INPUT;
    }

    return genfs_generalpathcorrection(input_string,MAX_FILENAME_LENGTH,
                                       output_filename);
}

//------------------------------------------------------------------------------
// Get file name with default folder
// Input:   u8  *input_string
// Output:  u8  *output_filename
// Return:  u8  status
//------------------------------------------------------------------------------
u8 file_default_getfilename(u8 *input_string, u8 *output_filename)
{
    if (input_string == NULL || output_filename == NULL)
    {
        return S_INPUT;
    }

    return genfs_defaultpathcorrection(input_string,MAX_FILENAME_LENGTH,
                                       output_filename);
}

//------------------------------------------------------------------------------
// Get file name with user folder
// Input:   u8  *input_string
// Output:  u8  *output_filename
// Return:  u8  status
//------------------------------------------------------------------------------
u8 file_user_getfilename(u8 *input_string, u8 *output_filename)
{
    if (input_string == NULL || output_filename == NULL)
    {
        return S_INPUT;
    }

    return genfs_userpathcorrection(input_string,MAX_FILENAME_LENGTH,
                                    output_filename);
}

//------------------------------------------------------------------------------
// Get file size
// Input:   F_FILE *fptr
// Output:  u32 *filesize
// Return:  u8  status
// Engineer: Quyen Leba
//------------------------------------------------------------------------------
u8 file_getfilesize(F_FILE *fptr, u32 *filesize)
{
    u32 currentposition;

    if (fptr == NULL || filesize == NULL)
    {
        return S_INPUT;
    }
    
    *filesize = 0;
    currentposition = ftell(fptr);
    if (fseek(fptr,0,SEEK_END) != 0)
    {
        return S_SEEKFILE;
    }
    
    *filesize = ftell(fptr);
    
    if (fseek(fptr,currentposition,SEEK_SET) != 0)
    {
        return S_SEEKFILE;
    }
    
    return S_SUCCESS;
}

//------------------------------------------------------------------------------
// Read a line from from at current file position
// Input:   F_FILE *fptr
// Output:  u8  *linedata
//          u32 *linedatalength (if NULL, not used)
// Return:  u8  status
// Engineer: Quyen Leba
// Note: work with Windows and Linux end line and return
//------------------------------------------------------------------------------
u8 file_readline(F_FILE *fptr, u8 *linedata, u32 *linedatalength)
{
#define READLINE_BUFFERSIZE     512
    u8  buffer[READLINE_BUFFERSIZE+2];
    u32 bufferlength;
    u32 fileposition;
    u32 linedata_index;
    bool isendline;
    u32 i;
    
    linedata[0] = NULL;
    if (linedatalength)
    {
        *linedatalength = 0;
    }
    linedata_index = 0;
    isendline = FALSE;
    if (fptr == NULL)
    {
        return S_INPUT;
    }
    fileposition = ftell(fptr);
    
    do
    {
        bufferlength = fread(buffer,1,sizeof(buffer),fptr);
        if (bufferlength == 0)
        {
            if (linedatalength)
            {
                *linedatalength = linedata_index;
            }
            linedata[linedata_index++] = NULL;
            if (linedata_index == 1)
            {
                return S_END;
            }
            return S_SUCCESS;
        }
        else if (bufferlength > READLINE_BUFFERSIZE)
        {
            bufferlength = READLINE_BUFFERSIZE;
        }
        
        for(i=0;i<bufferlength;i++)
        {
            if (buffer[i] != 0x0A && buffer[i] != 0x0D)
            {
                linedata[linedata_index++] = buffer[i];
            }
            else
            {
                if (linedatalength)
                {
                    *linedatalength = linedata_index;
                }
                linedata[linedata_index++] = NULL;
                isendline = TRUE;
                
                if (buffer[i+1] == 0x0A || buffer[i+1] == 0x0D)
                {
                    linedata_index++;
                }
                
                break;
            }
        }
        
        if (fseek(fptr,(fileposition+linedata_index),SEEK_SET) != 0)
        {
            return S_SEEKFILE;
        }
        
    }while(isendline == FALSE);
    
    return S_SUCCESS;
}

//------------------------------------------------------------------------------
// Inputs:  u8  *source_filename (any valid path)
//          u8  *dest_filename (any valid path)
// Return:  u8  status
// Engineer: Quyen Leba
//------------------------------------------------------------------------------
u8 file_handle_blockcrypto(file_blockcrypto crypt,  u8 *databuffer, u32 databufferlength)
{
    switch(crypt.simplecrypto.type)
    {
    case file_blockcrypto_type_tea:
        crypt.simplecrypto.crypto(databuffer,databufferlength,crypt.simplecrypto.key);   
        break;
    case file_blockcrypto_type_bf:
        crypt.bfcrypto.crypto((blowfish_context_ptr*)crypt.bfcrypto.ctx,databuffer,databufferlength);
        break;
    default:
        return S_FAIL;
    }
    
    return S_SUCCESS;
}

//------------------------------------------------------------------------------
// Inputs:  u8  *source_filename (any valid path)
//          u8  *dest_filename (any valid path)
// Return:  u8  status
// Engineer: Quyen Leba
//------------------------------------------------------------------------------
u8 file_copy(const u8 *source_filename, const u8 *dest_filename,
             file_blockcrypto *source_decryption,
             file_blockcrypto *dest_encryption,
             file_copy_progressreport progressreport_funcptr)
{
    F_FILE  *srcfptr = NULL;
    F_FILE  *dstfptr = NULL;
    u8 databuffer[2048];
    u32 databufferlength;
    u32 bytecount;
    u32 filelength;
    u32 bytecopiedcount;
    u8  percentage;
    u8  status;

    status = S_SUCCESS;

    srcfptr = genfs_general_openfile(source_filename,"rb");
    if (srcfptr == NULL)
    {
        status = S_OPENFILE;
        goto file_copy_done;
    }
    
    if (fseek(srcfptr,0,SEEK_END) != 0)
    {
        status = S_SEEKFILE;
        goto file_copy_done;
    }
    filelength = ftell(srcfptr);
    if (fseek(srcfptr,0,SEEK_SET) != 0)
    {
        status = S_SEEKFILE;
        goto file_copy_done;
    }
    
    dstfptr = genfs_general_openfile(dest_filename,"wb");
    if (dstfptr == NULL)
    {
        status = S_OPENFILE;
        goto file_copy_done;
    }

    bytecopiedcount = 0;
    percentage = 0;

    if (progressreport_funcptr)
    {
        progressreport_funcptr(percentage,"Copying File");
    }
    while(!feof(srcfptr))
    {
        databufferlength = fread(databuffer,1,2048,srcfptr);
        if (databufferlength > 0 && databufferlength <= 2048)
        {
            if (source_decryption)
            {
                file_handle_blockcrypto(*source_decryption, databuffer, databufferlength);
//                source_decryption->crypto(databuffer,databufferlength,
//                                          source_decryption->key);
            }
            if (dest_encryption)
            {
                file_handle_blockcrypto(*dest_encryption, databuffer, databufferlength);
//                dest_encryption->crypto(databuffer,databufferlength,
//                                        dest_encryption->key);                    
            }
            bytecount = fwrite(databuffer,1,databufferlength,dstfptr);
            if (bytecount != databufferlength)
            {
                status = S_WRITEFILE;
                break;
            }
        }
        else
        {
            status = S_READFILE;
            break;
        }
        
        bytecopiedcount += bytecount;
        if (progressreport_funcptr)
        {
            u8  value;
            value = bytecopiedcount*100/filelength;
            if (((value%10) == 0) && (value > percentage))
            {
                percentage = value;
                //TODOQ: assign message const i.e. "Copying File"
                progressreport_funcptr(percentage,"Copying File");
            }
        }
    }
    
    if (bytecopiedcount != filelength)
    {
        status = S_FAIL;
    }
    
file_copy_done:
    if (srcfptr)
    {
        genfs_closefile(srcfptr);
    }
    if (dstfptr)
    {
        genfs_closefile(dstfptr);
    }
    return status;
}

/**
 *  file_append
 *  
 *  @brief Append whole/portion of one file to another
 *  
 *  @param [in] source_filename (any valid path)
 *  @param [in] dest_filename (any valid path)
 *  @param [in] source_offset
 *  @param [in] source_bytetocopy
 *  @param [in] source_decryption
 *  @param [in] dest_encryption
 *  @param [in] progressreport_funcptr
 *
 *  @retval u8  status
 *  
 *  @details source_offset & source_bytetocopy are ZERO, append whole source file
 *  
 *  @authors Quyen Leba
 */
u8 file_append(const u8 *source_filename, const u8 *dest_filename,
               u32 source_offset, u32 source_bytetocopy,
               file_blockcrypto *source_decryption,
               file_blockcrypto *dest_encryption,
               file_copy_progressreport progressreport_funcptr)
{
    F_FILE  *srcfptr = NULL;
    F_FILE  *dstfptr = NULL;
    u8 databuffer[2048];
    u32 databufferlength;
    u32 bytecount;
    u32 filelength;
    u32 bytecopiedcount;
    u32 bytetocopy;
    u32 bytetoread;
    u8  percentage;
    u8  status;

    status = S_SUCCESS;

    srcfptr = genfs_general_openfile(source_filename,"rb");
    if (srcfptr == NULL)
    {
        status = S_OPENFILE;
        goto file_append_done;
    }

    if (fseek(srcfptr,0,SEEK_END) != 0)
    {
        status = S_SEEKFILE;
        goto file_append_done;
    }
    filelength = ftell(srcfptr);
    if (source_decryption || dest_encryption)
    {
        //to use encryption, must be multiple of 8
        if (source_offset % 8 != 0)
        {
            status = S_INPUT;
            goto file_append_done;
        }
        else if (filelength % 8 != 0)
        {
            status = S_BADCONTENT;
            goto file_append_done;
        }
    }

    if ((source_offset + source_bytetocopy) > filelength)
    {
        status = S_INPUT;       //too strict?
        goto file_append_done;
    }
    else if (source_bytetocopy == 0)
    {
        bytetocopy = filelength - source_offset;
    }
    else
    {
        bytetocopy = source_bytetocopy;
    }

    if (fseek(srcfptr,source_offset,SEEK_SET) != 0)
    {
        status = S_SEEKFILE;
        goto file_append_done;
    }
    
    dstfptr = genfs_general_openfile(dest_filename,"a");
    if (dstfptr == NULL)
    {
        status = S_OPENFILE;
        goto file_append_done;
    }

    bytecopiedcount = 0;
    percentage = 0;

    if (progressreport_funcptr)
    {
        progressreport_funcptr(percentage,"Copying File");
    }
    while(bytecopiedcount < bytetocopy)
    {
        bytetoread = bytetocopy - bytecopiedcount;
        if (bytetoread > sizeof(databuffer))
        {
            bytetoread = sizeof(databuffer);
        }
        else if (bytetoread == 0)
        {
            break;
        }
        databufferlength = fread(databuffer,1,bytetoread,srcfptr);
        if (databufferlength == bytetoread)
        {
            if (source_decryption)
            {
                file_handle_blockcrypto(*source_decryption, databuffer, databufferlength);
            }
            if (dest_encryption)
            {
                file_handle_blockcrypto(*dest_encryption, databuffer, databufferlength);
            }
            bytecount = fwrite(databuffer,1,databufferlength,dstfptr);
            if (bytecount != databufferlength)
            {
                status = S_WRITEFILE;
                break;
            }
        }
        else
        {
            status = S_READFILE;
            break;
        }
        
        bytecopiedcount += bytecount;
        if (progressreport_funcptr)
        {
            u8  value;
            value = bytecopiedcount*100/bytetocopy;
            if (((value%10) == 0) && (value > percentage))
            {
                percentage = value;
                //TODOQ: assign message const i.e. "Copying File"
                progressreport_funcptr(percentage,"Copying File");
            }
        }
    }
    
    if (bytecopiedcount != bytetocopy)
    {
        status = S_FAIL;
    }

file_append_done:
    if (srcfptr)
    {
        genfs_closefile(srcfptr);
    }
    if (dstfptr)
    {
        genfs_closefile(dstfptr);
    }
    return status;
}

//------------------------------------------------------------------------------
// Get file size by name
// Inputs:  u8  *filename (any valid path)
// Return:  u32 size
// Engineer: Quyen Leba
//------------------------------------------------------------------------------
u32 file_getsize_byname(u8 *filename)
{
    F_FILE *fptr;
    u32 size;
    
    fptr = genfs_general_openfile(filename,"r");
    if (fptr)
    {
        size = fgetfilesize(fptr);
        genfs_closefile(fptr);
    }
    else
    {
        size = 0;
    }
    return size;
}

//------------------------------------------------------------------------------
// Get file size by an opened F_FILE
// Inputs:  F_FILE *fptr
// Return:  u32 size
// Engineer: Quyen Leba
//------------------------------------------------------------------------------
u32 file_getsize_byfptr(F_FILE *fptr)
{
    if (fptr)
    {
        return fgetfilesize(fptr);
    }
    else
    {
        return 0;
    }
}

//------------------------------------------------------------------------------
// Calculate file crc32e by filename
// Input:   u8* filename
//          u8  paddingbyte (padding when filesize is not %4)
// Output:  u32 *crc32e
// Return:  u8  status
// Engineer: Patrick Downs
//------------------------------------------------------------------------------
u8 file_calcfilecrc32e_byfilename(u8* filename, u32 *crc32e, u8 paddingbyte)
{
    u8 status;
    F_FILE* f;
    u8  buffer[4096];
    u32 bufferlength;
    u32 lastbytes;
    u32 calc_crc32e;
    
    *crc32e = 0x00000000;
    
    f = genfs_general_openfile(filename,"rb");
    if (f)
    {
        crc32e_reset();
        calc_crc32e = 0xFFFFFFFF;
        while(!feof(f))
        {
            bufferlength = fread(buffer,1,sizeof(buffer),f);
            if (bufferlength)
            {
                lastbytes = bufferlength - ((bufferlength/4)*4);
                if (lastbytes)
                {
                    lastbytes = 4 - lastbytes;
                    memset(&buffer[bufferlength],paddingbyte,lastbytes);
                }
                calc_crc32e = crc32e_calculateblock(calc_crc32e,
                                                    (u32*)buffer,
                                                    (bufferlength+lastbytes)/4);
            }
        }
        *crc32e = calc_crc32e;
        status = S_SUCCESS;
        genfs_closefile(f);
    }
    else
    {
        status = S_OPENFILE;
    }
    
    return status;
}

//------------------------------------------------------------------------------
// Calculate file crc32e
// Input:   F_FILE *fptr
// Output:  u32 *crc32e
// Return:  u8  status
// Engineer: Quyen Leba
//------------------------------------------------------------------------------
u8 file_calcfilecrc32e(F_FILE *fptr, u32 *crc32e)
{
    u8  buffer[4096];
    u32 bufferlength;
    u32 lastbytes;
    u32 calc_crc32e;
    u32 currentposition;

    *crc32e = 0;
    if (fptr)
    {
        currentposition = ftell(fptr);
        if (fseek(fptr,0,SEEK_SET) != 0)
        {
            return S_SEEKFILE;
        }

        crc32e_reset();
        calc_crc32e = 0xFFFFFFFF;
        while(!feof(fptr))
        {
            bufferlength = fread(buffer,1,sizeof(buffer),fptr);
            if (bufferlength)
            {
                lastbytes = bufferlength - ((bufferlength/4)*4);
                if (lastbytes)
                {
                    lastbytes = 4 - lastbytes;
                    memset(&buffer[bufferlength],0x00,lastbytes);
                }
                calc_crc32e = crc32e_calculateblock(calc_crc32e,
                                                    (u32*)buffer,
                                                    (bufferlength+lastbytes)/4);
            }
        }
        if (fseek(fptr,currentposition,SEEK_SET) != 0)
        {
            return S_SEEKFILE;
        }
        *crc32e = calc_crc32e;
    }
    else
    {
        return S_INPUT;
    }
    return S_SUCCESS;
}

/**
 * @brief   To check if file is the same
 *
 * @param [in]  filename
 * @param [in]  file_size
 * @param [in]  file_crc32e
 *
 * @retval u8  status (S_SUCCESS: same, others: different)
 *
 * @author  Quyen Leba
 *
 */
u8 file_filematch_check(const u8 *filename, u32 file_size, u32 file_crc32e)
{
    F_FILE *fptr;
    u32 calc_crc32e;
    u32 size;
    u8 status;

    status = S_FAIL;
    fptr = genfs_general_openfile(filename,"rb");
    if (fptr)
    {
        size = file_getsize_byfptr(fptr);
        if (size != file_size)
        {
            status = S_UNMATCH;
        }
        else
        {
            status = file_calcfilecrc32e(fptr,&calc_crc32e);
            if (status == S_SUCCESS && file_crc32e != calc_crc32e)
            {
                status = S_UNMATCH;
            }
        }
        genfs_closefile(fptr);
    }
    else
    {
        status = S_OPENFILE;
    }
    
    return status;
}

//------------------------------------------------------------------------------
// Actual function that handles overlaying the flashfile
// Input:   u16 ecm_type,
//          u32 length,
//          const u8 *source_filename,
//          const u8 *dest_filename,
//          file_blockcrypto *source_decryption,
//          file_blockcrypto *dest_encryption, 
//          file_copy_progressreport progressreport_funcptr
// Return:  u8  status
// Engineer: Ricardo Cigarroa
//------------------------------------------------------------------------------
u8 file_copy_overlay(u32 position, u32 length, const u8 *source_filename, 
                     const u8 *dest_filename, file_blockcrypto *source_decryption, 
                     file_blockcrypto *dest_encryption, file_copy_progressreport progressreport_funcptr)
{
    F_FILE  *srcfptr = NULL;
    F_FILE  *dstfptr = NULL;
    u8  *databuffer;
    u8  percentage;
    u8  status;    
    u32 bytecount;
    u32 databufferlength;
    u32 bytecopiedcount;
    u32 bufferlength = 4096; 
    
    status = S_SUCCESS;
    
    databuffer = __malloc(bufferlength);
    if (databuffer == NULL)
    {
        status = S_MALLOC; 
        goto file_copy_overlay_done;
    }
    
    srcfptr = genfs_general_openfile(source_filename,"rb");
    if (srcfptr == NULL)
    {
        status = S_OPENFILE;
        goto file_copy_overlay_done;
    }
    dstfptr = genfs_general_openfile(dest_filename,"r+");
    if (dstfptr == NULL)
    {
        status = S_OPENFILE;
        goto file_copy_overlay_done;
    }    
    if (fseek(srcfptr,0,SEEK_END) != 0)
    {
        status = S_SEEKFILE;
        goto file_copy_overlay_done;
    }
    if (fseek(srcfptr,position,SEEK_SET) != 0)
    {
        status = S_SEEKFILE;
        goto file_copy_overlay_done;
    }
  
    bytecopiedcount = 0;
    percentage = 0;
    
    if (progressreport_funcptr)
    {
       progressreport_funcptr(percentage,"Copying File");
    }
    
    while(bytecopiedcount < length)
    {
        databufferlength = fread(databuffer,1,bufferlength,srcfptr);
        if (databufferlength > 0 && databufferlength <= bufferlength)
        {
            if (source_decryption)
            {
                file_handle_blockcrypto(*source_decryption, databuffer, databufferlength);
            }
            if (dest_encryption)
            {
               file_handle_blockcrypto(*dest_encryption, databuffer, databufferlength);    
            }
            bytecount = fwrite(databuffer,1,databufferlength,dstfptr);
            if (bytecount != databufferlength)
            {
               status = S_WRITEFILE;
               break;
            }
            position += databufferlength; 
        }
        else
        {
            status = S_READFILE;
            break;
        }
        
        bytecopiedcount += databufferlength;
        if (progressreport_funcptr)
        {
           u8  value;
           value = bytecopiedcount*100/length;
           if (((value%1) == 0) && (value > percentage))
           {
               percentage = value;
               //TODOQ: assign message const i.e. "Copying File"
               progressreport_funcptr(percentage,"Copying File");
           }
        }
    }
    
    if (bytecopiedcount != length)
    {
       status = S_FAIL;
    }
    
file_copy_overlay_done:
    if (databuffer)
    {
        __free(databuffer);
    }
    if (srcfptr)
    {
        genfs_closefile(srcfptr);
    }
    if (dstfptr)
    {
        genfs_closefile(dstfptr);
    }
    return status;
  
}

//------------------------------------------------------------------------------
// Read data starting from a position (any file)
// Inputs:  u32 position
//          u8  *data
//          u32 datalength
// Output:  u32 *bytecount
// Return:  u8  S_SUCCESS
// Engineer: Ricardo Cigarroa
//------------------------------------------------------------------------------
u8 file_read_at_position(u32 position, u8 *data, u32 datalength,
                         u32 *bytecount, F_FILE *file_fptr, bool is_stock_key)
{
    u8  status;
    
    if(file_fptr == NULL)
    {
        log_push_error_point(0xB011);
        return S_FAIL; 
    }
    
    status = file_set_position(position, file_fptr);
    if (status != S_SUCCESS)
    {
        log_push_error_point(0xB012);
        return S_SEEKFILE;
    }
    status = file_read(data,datalength,bytecount, file_fptr, is_stock_key);
    if (status != S_SUCCESS)
    {
        log_push_error_point(0xB019);
        return S_READFILE;
    }
    return S_SUCCESS;
}

//------------------------------------------------------------------------------
// Set a file position
// Input:   u32 pos         current position(pointer) of any file
// Return:  u8  status      S_SUCCESS
// Enginner: Ricardo Cigarroa
//------------------------------------------------------------------------------
u8 file_set_position(u32 pos, F_FILE *file_fptr)
{
    u8 status;
    
    if(file_fptr)
    {
        if(fseek(file_fptr,pos,SEEK_SET) == 0)
        {
            status = S_SUCCESS;
        }
        else
        {
            status = S_SEEKFILE;
        }
    }
    else
    {
        status = S_FAIL;
    }
    
    return status;
}

//------------------------------------------------------------------------------
// Get a file position
// Input:   u32 pos            current position(pointer) of any file
//          F_FILE *file_fptr  file pointer. 
// Return:  u8  status         S_SUCCESS
// Enginner: Ricardo Cigarroa
//------------------------------------------------------------------------------
u8 file_get_position(u32 *pos, F_FILE *file_fptr)
{
    u8 status;
    
    if(file_fptr)
    {
        *pos = ftell(file_fptr);
        status = S_SUCCESS;

    }
    else
    {
        log_push_error_point(0xB014);
        status = S_FAIL;
    }
    
    return status;
}

//------------------------------------------------------------------------------
// Read data from file
// Inputs:  u8  *data
//          u32 datalength
// Output:  u32 *bytecount
// Return:  u8  S_SUCCESS
// Engineer: Ricardo Cigarroa
//------------------------------------------------------------------------------
u8 file_read(u8 *data, u32 datalength, u32 *bytecount, F_FILE *file_fptr, 
             bool is_stock_key)
{
    u32 currentposition;
    u32 slotposition;
    u32 readlength;
    u32 total;
    u32 count;
    u8  buffer[8];
    u8  status;

    if (file_fptr == NULL)
    {
        log_push_error_point(0xB013);
        return S_FAIL;
    }
   
    total = 0;
    *bytecount = 0;
    status = file_get_position(&currentposition, file_fptr);
    if (status == S_SUCCESS)
    {
        slotposition = (currentposition/8)*8;
        if (slotposition == currentposition)
        {
            readlength = (datalength/8)*8;
            total = fread((char*)data,1,readlength,file_fptr);
            
            if(is_stock_key == TRUE)
            {
                tea_decryptblock(data,total,(u32*)stockfile_tea_key);
            }
            else
            {
                crypto_blowfish_decryptblock_critical(data, total);
            }

            if (total == datalength)
            {
                //do nothing
            }
            else if (total < datalength)    //tail is out of alignment
            {
                count = fread((char*)buffer,1,sizeof(buffer),file_fptr);
                if (count != sizeof(buffer))
                {
                    return S_READFILE;
                }
                
                if(is_stock_key == TRUE)
                {
                    tea_decryptblock(buffer,sizeof(buffer),(u32*)stockfile_tea_key);
                }
                else
                {
                    crypto_blowfish_decryptblock_critical(buffer, sizeof(buffer));
                }
                
                //crypto_blowfish_decryptblock_critical(buffer, sizeof(buffer));  
                memcpy((char*)&data[total],(char*)buffer,datalength-total);

                if (file_set_position(currentposition+datalength,file_fptr) != S_SUCCESS)
                {
                    log_push_error_point(0xB015);
                    return S_SEEKFILE;
                }
                total = datalength;
            }//else if (total < datalength)...
            else
            {
                log_push_error_point(0xB016);
                return S_ERROR;
            }
        }
        else if (slotposition < currentposition)
        {
            u32 startoffset;

            //head is out of alignment
            startoffset = currentposition - slotposition;
            if (file_set_position(slotposition,file_fptr) != S_SUCCESS)
            {
                log_push_error_point(0xB017);
                return S_SEEKFILE;
            }
            count = fread((char*)buffer,
                          1,sizeof(buffer),file_fptr);
            
            if(is_stock_key == TRUE)
            {
                tea_decryptblock(buffer,sizeof(buffer),(u32*)stockfile_tea_key);
            }
            else
            {
                crypto_blowfish_decryptblock_critical(buffer, sizeof(buffer));
            }
            
            //crypto_blowfish_decryptblock_critical(buffer, sizeof(buffer));  
            total = sizeof(buffer)-startoffset;
            if (total > datalength)
            {
                total = datalength;
            }
            memcpy((char*)data,(char*)&buffer[startoffset],total);

            //reading mid section; always in alignment
            readlength = ((datalength-total)/8)*8;
            count = fread((char*)&data[total],
                          1,readlength,file_fptr);
            if(is_stock_key == TRUE)
            {
                tea_decryptblock(&data[total],readlength,(u32*)stockfile_tea_key);
            }
            else
            {
                crypto_blowfish_decryptblock_critical(&data[total], readlength);
            }
              
            //crypto_blowfish_decryptblock_critical(&data[total],readlength);  
            total += count;

            if (total == datalength)
            {
                //do nothing
            }
            else if (total < datalength)
            {
                //tail is out of alignment
                count = fread((char*)buffer,
                              1,sizeof(buffer),file_fptr);
                
                if(is_stock_key == TRUE)
                {
                    tea_decryptblock(buffer,sizeof(buffer),(u32*)stockfile_tea_key);
                }
                else
                {
                    crypto_blowfish_decryptblock_critical(buffer, sizeof(buffer));
                }
            
                //crypto_blowfish_decryptblock_critical(buffer,sizeof(buffer)); 
                memcpy((char*)&data[total],(char*)buffer,datalength-total);
                if (file_set_position(currentposition+datalength,file_fptr) != S_SUCCESS)
                {
                    log_push_error_point(0xB018);
                    return S_SEEKFILE;
                }
                total = datalength;
            }//else if (total < datalength)...
            else
            {
                //should never happen
                return S_ERROR;
            }
        }
        else
        {
            //should not happen
            return S_ERROR;
        }
    }
    *bytecount = total;
    return S_SUCCESS;
}

/**
 *  file_encrypted_read
 *  
 *  @brief Read file with block encryption
 *  
 *  @param [in] fptr
 *  @param [in] datalength (byte to read)
 *  @param [in] decryption_info
 *  @param [out] data (unencrypted)
 *  @param [out] bytecount actual byte stored in data, NULL allowed
 *
 *  @retval u8  status
 *  
 *  @authors Quyen Leba
 */
u8 file_encrypted_read(F_FILE *fptr, u32 datalength, u8 *data, u32 *bytecount,
                       file_blockcrypto *decryption_info)
{
    u32 currentposition;
    u32 slotposition;
    u32 readlength;
    u32 total;
    u32 count;
    u8  buffer[8];

    if (!fptr || !data || datalength == 0)
    {
        return S_INPUT;
    }

    total = 0;
    if (bytecount)
    {
        *bytecount = 0;
    }
    currentposition = ftell(fptr);

    slotposition = (currentposition/8)*8;
    if (slotposition == currentposition)
    {
        readlength = (datalength/8)*8;
        total = fread((char*)data,1,readlength,fptr);
        if (decryption_info)
        {
            file_handle_blockcrypto(*decryption_info, data, total);
        }

        if (total == datalength)
        {
            //do nothing
        }
        else if (total < datalength)    //tail is out of alignment
        {
            count = fread((char*)buffer,1,sizeof(buffer),fptr);
            if (count != sizeof(buffer))
            {
                return S_READFILE;
            }
            if (decryption_info)
            {
                file_handle_blockcrypto(*decryption_info, buffer, sizeof(buffer));
            }
            memcpy((char*)&data[total],(char*)buffer,datalength-total);
            
            if (fseek(fptr,currentposition+datalength,SEEK_SET) != 0)
            {
                return S_SEEKFILE;
            }
            total = datalength;
        }//else if (total < datalength)...
        else
        {
            return S_ERROR;
        }
    }
    else if (slotposition < currentposition)
    {
        u32 startoffset;
        
        //head is out of alignment
        startoffset = currentposition - slotposition;
        if (fseek(fptr,slotposition,SEEK_SET) != 0)
        {
            return S_SEEKFILE;
        }
        count = fread((char*)buffer,1,sizeof(buffer),fptr);
        if (decryption_info)
        {
            file_handle_blockcrypto(*decryption_info, buffer, sizeof(buffer));
        }
        total = sizeof(buffer)-startoffset;
        if (total > datalength)
        {
            total = datalength;
        }
        memcpy((char*)data,(char*)&buffer[startoffset],total);
        
        //reading mid section; always in alignment
        readlength = ((datalength-total)/8)*8;
        count = fread((char*)&data[total],1,readlength,fptr);
        if (decryption_info)
        {
            file_handle_blockcrypto(*decryption_info, &data[total], readlength);
        }
        total += count;
        
        if (total == datalength)
        {
            //do nothing
        }
        else if (total < datalength)
        {
            //tail is out of alignment
            count = fread((char*)buffer,1,sizeof(buffer),fptr);
            if (decryption_info)
            {
                file_handle_blockcrypto(*decryption_info, buffer, sizeof(buffer));
            }
            memcpy((char*)&data[total],(char*)buffer,datalength-total);
            
            if (fseek(fptr,currentposition+datalength,SEEK_SET) != 0)
            {
                return S_SEEKFILE;
            }
            total = datalength;
        }//else if (total < datalength)...
        else
        {
            //should never happen
            return S_ERROR;
        }
    }
    else
    {
        //should not happen
        return S_ERROR;
    }

    if (bytecount)
    {
        *bytecount = total;
    }
    return S_SUCCESS;
}
