/**
  **************** (C) COPYRIGHT 2011 SCT Performance, LLC *********************
  * File Name          : cmdif.c
  * Device / System    : SCT products using the "CommonCode" platform
  * Author             : Quyen Leba
  *
  * Version            : 1 
  * Date               : 05/02/2011
  * Description        : 
  *                    : 
  *
  *
  * History            : 05/02/2011 M. Davis
  *                    :   Add SCT Copyright header,
  *
  ******************************************************************************
  */

#include <string.h>
#include <stdio.h>
#include <device_version.h>
#include <fs/genfs.h>
#include <board/genplatform.h>
#include <board/bootloader.h>
#include <board/properties_vb.h>
#include <common/version.h>
#include <common/statuscode.h>
#include <common/obd2.h>
#include <common/genmanuf_overload.h>
#include <common/usbcmdhandler.h>
#include <common/usbcmdhandler_tune.h>
#include <common/usbcmdhandler_datalog.h>
#include "cmdif_function.h"
#include "cmdif_func_filexfer.h"
#include "cmdif_func_flasher.h"
#include "cmdif_func_uploadtune.h"
#include "cmdif_func_downloadtune.h"
#include "cmdif_func_recoverystock.h"
#include "cmdif_func_datalog.h"
#include "cmdif_func_vehiclefunction.h"
#if USE_WIFI
#include "cmdif_func_wifi.h"
#endif
#include "cmdif.h"
#include "housekeeping.h"

extern u8 uch_tune_internal_response_ack(CMDIF_COMMAND cmd, CMDIF_ACK respack,
                                         u8 *data, u16 datalength);

cmdif_datainfo cmdif_datainfo_responsedata;

struct
{
    bool cmdif_intr_rts;
    cmdif_datainfo responsedata;
}cmdif_intr;

#ifdef SUPPORT_FILE_XFER
extern filexfer_info filexferinfo;                  //from cmdif_func_filexfer.c
#endif

cmdif_tracker cmdif_tracker_info =
{
    .commandcode = CMDIF_CMD_INVALID,
    .responsecode = CMDIF_ACK_INVALID,
    .state = CMDIF_TRACKER_STATE_IDLE,
    .progbar_string = NULL,
};

void cmdif_tracker_get_report(u8 *reportdata, u16 *reportdatalength);

//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
u8 cmdif_init()
{
    cmdif_datainfo_responsedata.dataptr = NULL;
    cmdif_datainfo_responsedata.datalength = 0;
    cmdif_datainfo_responsedata.responsetype = NormalAck;
    
    cmdif_intr.cmdif_intr_rts = FALSE;
    cmdif_intr.responsedata.dataptr = NULL;
    cmdif_intr.responsedata.datalength = 0;
    cmdif_intr.responsedata.responsetype = NormalAck;
    

#ifdef SUPPORT_FILE_XFER
    filexferinfo.transfer_id = 0;
    filexferinfo.filestream = NULL;
    //-Q- @@@ filexferinfo.databuffer = NULL;   //TODOQ: this is for now
    filexferinfo.filesize = 0;
    filexferinfo.offset = 0;
    filexferinfo.blocklength = 0;
    filexferinfo.currentposition = 0;
#endif

    return S_SUCCESS;
}

//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
u8 cmdif_handler()
{
    u8 cmddata[2048+16];
    u16 cmdlength;
    u8 cmdstatus;
    u8 status;

    cmdstatus = commlink_receive_command(cmddata,&cmdlength,sizeof(cmddata));
    if (cmdstatus == S_SUCCESS)
    {
        if (uch_tune_isbusy())
        {
            //passthrough flashing is active, reject all commands from commlink
            cmdif_response_ack(cmdif_datainfo_responsedata.commandcode,
                               CMDIF_ACK_PT_FLASHING_LOCKED,NULL,0);
            status = S_REJECT;
        }
        else if (uch_datalog_isbusy())
        {
            //passthrough datalogging is active, reject all commands from commlink
            cmdif_response_ack(cmdif_datainfo_responsedata.commandcode,
                               CMDIF_ACK_PT_DATALOGGING_LOCKED,NULL,0);
            status = S_REJECT;
        }
        else
        {
            //temporarily halt uch to allow full attention to this command 
            uch_set_temporary_halt(TRUE);

            // process the received command
            status = cmdif_command(cmddata[1], &cmddata[4], cmdlength-4);
            //Note: status is for debug; any error should be reported/handle
            //internally

            // The following commands internally perform cmdif_response_ack() via calls to 
            // the cmdif_internalresponse_ack() macro defined in cmdif.h. 
            // For all other commands, perform the cmdif_response_ack() here
            if (cmddata[1] != CMDIF_CMD_READ_TO_MONITOR &&
                cmddata[1] != CMDIF_CMD_DO_UPLOAD &&
                cmddata[1] != CMDIF_CMD_DO_DOWNLOAD &&
                cmddata[1] != CMDIF_CMD_DO_RETURN_STOCK &&
                cmddata[1] != CMDIF_CMD_DATALOG_SCAN &&
                cmddata[1] != CMDIF_CMD_HARDWARE)
            {
                cmdif_response_ack(cmdif_datainfo_responsedata.commandcode,
                                   cmdif_datainfo_responsedata.responsecode,
                                   cmdif_datainfo_responsedata.dataptr,
                                   cmdif_datainfo_responsedata.datalength);
            }

            //there's a very small chance that UCH can get in between serial commands
            //(such as CMDIF_CMD_INIT_DOWNLOAD & CMDIF_CMD_DO_DOWNLOAD)
            uch_set_temporary_halt(FALSE);
        }
    }
    return status;
}

//------------------------------------------------------------------------------
// cmdif_interrupt_command()
//
// Examine any command received from the UI app through platform-specific
// interrupt-driven commlink_process_incoming_byte(...) function. If the
// received command is CMDIF_CMD_GET_CMDTRACKER_REPORT, set up the
// static cmdif_intr struct to send a subsequent ack message, and post a
// "CmdifIntrReponseAck" task in the main-loop "housekeeping" pending-task
// list.
//
// The pending interrupt-command "ack" message will be transmitted when
// the cmdif_response_ack(...) function is called. This usually occurs when
// the cmdif_interrupt_response_ack(...) macro is called. Otherwise, if a
// disruption of comms between the MainBoard and the UI app prevents calls
// to the cmdif_interrupt_response_ack(...) macro, the main-loop
// housekeeping system will call cmdif_response_ack(...) when executing
// the pending "CmdifIntrReponseAck" task.
//
// The cmdif_response_ack(...) function removes any pending
// "CmdifIntrReponseAck" task from the main-loop housekeeping task list after
// the "ack" to the validated interrupt-command has been sent.
//
// input:  u16 command - command code received in the platform-specific
//                       interrupt-driven reception of commands from the UI app
//         u8 *data - unused
//         u16 length - unused
// return: S_FAIL
// output: update the static (file-scope) cmdif_intr struct, and if a valid
//         interrupt-command has been detected, post a "CmdifIntrReponseAck"
//         task in the main-loop "housekeeping" task list.
//
// engineer: Patrick Downs
//------------------------------------------------------------------------------
u8 cmdif_interrupt_command(u16 command, u8 *data, u16 length)
{
#define CMDTRACKER_REPORT_SIZE 300
    u8 status;
    u8 report[CMDTRACKER_REPORT_SIZE];
    u16 reportlength;
    
    status = S_FAIL;
    
    switch(command)
    {
    case CMDIF_CMD_GET_CMDTRACKER_REPORT:
        // send tracker report
        commlink_info.data_ready = 0;
        cmdif_intr.responsedata.commandcode = CMDIF_CMD_GET_CMDTRACKER_REPORT;
        cmdif_intr.responsedata.responsecode = CMDIF_ACK_OK;
        cmdif_tracker_get_report(report, &reportlength);
        if (cmdif_intr.responsedata.dataptr)
        {
            __free(cmdif_intr.responsedata.dataptr);
            cmdif_intr.responsedata.dataptr = NULL;
        }
        cmdif_intr.responsedata.dataptr = __malloc(reportlength);
        if(!cmdif_intr.responsedata.dataptr)
        {
            status = S_FAIL;
            break;
        }
        memcpy(cmdif_intr.responsedata.dataptr,report,reportlength);
        cmdif_intr.responsedata.datalength = reportlength;
        cmdif_intr.cmdif_intr_rts = TRUE; // ready to send
        // Add to house keeping task list
        housekeeping_additem(HouseKeepingType_CmdifIntrResponseAck, 0);
        break;
    default:
        break;
    }
    
    return status;
}

//------------------------------------------------------------------------------
// cmdif_interrupt_response_ack()
//
// Respond to UI commands received through the interrupt-driven
// cmdif_interrupt_command() process, using values stored in the static
// (file-scope) cmdif_intr struct by the earlier cmdif_interrupt_command(...)
// execution.
//
// This function is usually called through the cmdif_response_ack() macro,
// defined in common\cmdif.h. The cmdif_response_ack() macro is called
// called directly from the cmdif_handler() function, or elsewhere in the
// MainBoard code, through the cmdif_internalresponse_ack() macro. When
// cmdif_interrupt_response_ack() is called through any of these "chained"
// macro invocations, the transmitted interrupt-command response is sent after
// the MainBoard "ack" message that the cmdif_response_ack() macro or
// cmdif_internalresponse_ack() macro was called to generate.
//
// Alternately, this function may be called from the main-loop "housekeeping"
// system. This type of call occurs when a UI command is received through the
// platform-specific interrupt-driven commlink_process_incoming_byte(...)
// function, and the received message is validated as an interrupt-command by
// cmdif_interrupt_command(...). If the received interrupt-command is valid,
// a task to call cmdif_interrupt_response_ack() is posted in the main-loop
// "housekeeping" system.
//
// Regardless of how cmdif_interrupt_response_ack() is called, after the  "ack"
// message is sent in response to the previously-received interrupt-command, the
// corresponding task is removed from the housekeeping system pending-task list.
// If a disruption of comms between the MainBoard and the UI app prevents
// cmdif_interrupt_response_ack() from being called (because the
// cmdif_response_ack() macro can't be executed), the corresponding
// interrupt-command task remains posted in the main-loop housekeeping system.
// In this case, the response to the interrupt-command occurs when
// cmdif_interrupt_response_ack() is called from a main-loop invocation of
// the housekeeping system.
//
// Regardless of how cmdif_interrupt_response_ack() is invoked, after the
// transmission of the interrupt-command "ack" message, the corresponding "pending"
// task is removed from the housekeeping pending-task list.
//
// input: none
// return: If the cmdif_intr_rts flag is set in the static (file-scope)
//         cmdif_intr struct, the returned value is the result of the
//         commlink_send_ack() call in response to the received and recognized
//         interrupt-command. If the cmdif_intr_rts flag is not set, the return
//         value is S_NOTREQUIRED.
// 
// output: An attempt is made to send a MainBoard-to-UI "ack" message, in
//         response to the previously-received (and recognized) interrupt-
//         command. After the interrupt-command ack message is sent, the
//         corresponding "CmdifIntrReponseAck" task, posted by the prior
//         cmdif_interrupt_command(...) operation, is removed from
//         housekeeping system pending-task list.       
// 
// engineer: Patrick Downs
//------------------------------------------------------------------------------
u8 cmdif_interrupt_response_ack()
{
    u8 status;
    
    if(cmdif_intr.cmdif_intr_rts)
    {
        status = commlink_send_ack(cmdif_intr.responsedata.commandcode, 
                                   cmdif_intr.responsedata.responsecode, 
                                   cmdif_intr.responsedata.dataptr, 
                                   cmdif_intr.responsedata.datalength);
        //Remove from house keeping task list
        housekeeping_removeitem(HouseKeepingType_CmdifIntrResponseAck);
        cmdif_intr.cmdif_intr_rts = FALSE;
        if (cmdif_intr.responsedata.dataptr)
        {
            __free(cmdif_intr.responsedata.dataptr);
            cmdif_intr.responsedata.dataptr = NULL;
        }
        return status;
    }
    else
    {
        return S_NOTREQUIRED;
    }
}

//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
void cmdif_datainfo_responsedata_cleanup()
{
    if (cmdif_datainfo_responsedata.dataptr)
    {
        __free(cmdif_datainfo_responsedata.dataptr);
        cmdif_datainfo_responsedata.dataptr = NULL;
        cmdif_datainfo_responsedata.datalength = 0;
    }
}

//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
u8 cmdif_command(u16 command, u8 *data, u16 length)
{
    u32 tmp_u32;
    u8 status;
    u8 *tmp_u8ptr;

    status = S_FAIL;
    cmdif_datainfo_responsedata.responsecode = CMDIF_ACK_FAILED;
    cmdif_datainfo_responsedata.commandcode = (CMDIF_COMMAND)command;
    cmdif_datainfo_responsedata.responsetype = NormalAck;

    cmdif_datainfo_responsedata_cleanup();

    cmdif_tracker_track_new_command((CMDIF_COMMAND)command);
    cmdif_tracker_set_state(CMDIF_TRACKER_STATE_IN_PROGRESS);

    switch(command)
    {
    //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
#ifdef SUPPORT_FILE_XFER
    case CMDIF_CMD_CHECK_FILE:
        //[4:filesize][4:file crc32e][n:filename][NULL]
        if (length == (8+strlen((char*)&data[8])+1))
        {
            status = cmdif_func_filexfer_check_file(data);
        }
        break;
    //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    case CMDIF_CMD_FREAD_BLOCK:
        //[4:offset][4:blocklength(2048 max)]
        if (length == 8)
        {
            status = cmdif_func_filexfer_fread_block(*((u32*)&data[0]),
                                                     *((u32*)&data[4]));
        }
        break;
    case CMDIF_CMD_FWRITE_BLOCK:
        //[4:offset][4:blocklength(2048 max)][n:blockdata]
        tmp_u32 = *((u32*)&data[4]);
        if (length == (tmp_u32+8))
        {
            status = cmdif_func_filexfer_fwrite_block(*((u32*)&data[0]),
                                                      tmp_u32,
                                                      &data[8]);
        }
        break;
    case CMDIF_CMD_FOPEN_FILETRANSFER:
        //[1:attribute][4:filesize][n:filename][NULL]
        if (length == (5+strlen((char*)&data[5])+1))
        {
            status = cmdif_func_filexfer_fopen_filetransfer(data);
        }
        break;
    case CMDIF_CMD_FCLOSE_FILETRANSFER:
        //data: none
        status = cmdif_func_filexfer_fclose_filetransfer();
        break;
    case CMDIF_CMD_FOLDER:
        //data: [1:task][1:reserved][2:privdata][n:folder name][NULL][m:optional data]
        tmp_u32 = strlen((char*)&data[4]);
        if (length == (4+tmp_u32+1))
        {
            status = cmdif_func_filexfer_folder((filexfer_foldertask)data[0],
                                                &data[4],*(u16*)&data[2],NULL);
        }
        else if (length > (4+tmp_u32+1))
        {
            status = cmdif_func_filexfer_folder((filexfer_foldertask)data[0],
                                                &data[4],*(u16*)&data[2],&data[4+tmp_u32+1+1]);
        }
        break;
    case CMDIF_CMD_FILE:
        //data: [1:task][1:reserved][2:privdata][n:file name][NULL]
        if (length == (4+strlen((char*)&data[4])+1))
        {
            status = cmdif_func_filexfer_file((filexfer_filetask)data[0],
                                                &data[4],*(u16*)&data[2]);
        }
        break;
    //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    case CMDIF_CMD_FOPEN_FILE:          //obsolete
        status = cmdif_func_filexfer_open_file(data);
        break;
    case CMDIF_CMD_READ_BLOCK:          //obsolete
        status = cmdif_func_filexfer_read_block(data);
        break;
    case CMDIF_CMD_READ_TO_MONITOR:     //obsolete
        status = cmdif_func_filexfer_read_to_monitor();
        break;
    case CMDIF_CMD_WRITE_BLOCK:         //obsolete
        status = cmdif_func_filexfer_write_block(data);
        break;
    case CMDIF_CMD_WRITE_TO_DONGLE:     //obsolete
        status = cmdif_func_filexfer_write_to_dongle(data);
        break;
#endif
    //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
#if (OBD2_SUPPORT_DATALOG)
    case CMDIF_CMD_DATALOG_SCAN: //may generate session-drop events
        //data: [n:datalogfilename][1:NULL]
        if (length == strlen((char*)data)+1)
        {
            status = cmdif_func_datalog_scan(data);
        }
        break;
    case CMDIF_CMD_DATALOG_FEATURES:
        status = cmdif_func_datalog_getfeature();
        break;
    case CMDIF_CMD_DATALOG_REQUEST_OTF:
        //data: [n:vin][1:NULL]
        tmp_u32 = strlen((char*)data);
        if (length >= tmp_u32+1 && tmp_u32 == VIN_LENGTH)
        {
            status = cmdif_func_datalog_request_otf(data);
        }
        break;
    case CMDIF_CMD_DATALOG_START: //may generate session-drop events
        //data: [1:attribute][1:speed][n:datalogfilename][1:NULL]
        if (length == 2+strlen((char*)&data[2])+1)
        {
            status = cmdif_func_datalog_start(data);
        }
        break;
    case CMDIF_CMD_DATALOG_SET_REPORT_RATE:
        //data: [1:attribute][1:speed]
        if (length == 2)
        {
            status = cmdif_func_datalog_set_report_rate(data[0],data[1]);
        }
        break;
    case CMDIF_CMD_DATALOG_RECORD:
        if (length == 1)
        {
            status = cmdif_func_datalog_record(data[0]);
        }
        break;
    case CMDIF_CMD_DATALOG_SAVE_RESULT:
        //data: [1:result type][3:reserved][n:data]
        if (length > 6)
        {
            status = cmdif_func_datalog_save_result(data[0],&data[4],length-4);
        }
        break;
#endif  //#if (OBD2_SUPPORT_DATALOG)
    //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
#if (OBD2_SUPPORT_TUNE)
    case CMDIF_CMD_SEARCH_FOR_VEHICLE_TYPE:
        status = cmdif_func_uploadtune_vehicle_type_search();
        break;
    case CMDIF_CMD_COMPILE_USER_VEHICLE_TYPE:
        //data: ([2:count][2:type0(pcm)][2:type1(tcm)][28:reserved]
        status = cmdif_func_uploadtune_compile_user_vehicle_type(data);
        break;
    case CMDIF_CMD_GET_UPLOAD_STOCK:
        status = cmdif_func_get_upload_stock();
        break;        
    case CMDIF_CMD_INIT_UPLOAD:
        status = cmdif_func_uploadtune_init();
        break;
    case CMDIF_CMD_DO_UPLOAD: //may generate session-drop events
        status = cmdif_func_uploadtune_do();
        break;
    case CMDIF_CMD_INIT_DOWNLOAD: //may generate session-drop events
        status = cmdif_func_downloadtune_init();
        break;
    case CMDIF_CMD_DO_DOWNLOAD: //may generate session-drop events
        status = cmdif_func_downloadtune_do();
        break;
    case CMDIF_CMD_HEALTH_CHECK_DOWNLOAD:
        //data: [2:vehicle type]
        if (length >= 2)
        {
            status = cmdif_func_downloadtune_health_check(*(u16*)data);
        }
        break;
    case CMDIF_CMD_INIT_RETURN_STOCK: //may generate session-drop events
        status = cmdif_func_downloadtune_init();
        break;
    case CMDIF_CMD_DO_RETURN_STOCK: //may generate session-drop events
        status = cmdif_func_downloadtune_do();
        break;
    case CMDIF_CMD_CHECK_RECOVERYSTOCK:
        if (length == RECOVERYSTOCK_HEADER_SIZE)
        {
            status = cmdif_func_recoverystock_check(data);
        }
        break;
    case CMDIF_CMD_REGISTER_RECOVERYSTOCK: //may generate session-drop events
        if (length == RECOVERYSTOCK_HEADER_SIZE + strlen((char*)&data[RECOVERYSTOCK_HEADER_SIZE]) + 1)
        {
            status = cmdif_func_recoverystock_register(data,&data[RECOVERYSTOCK_HEADER_SIZE]);
        }
        break;
    //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    case CMDIF_CMD_FLASHER:
        tmp_u32 = *((u32*)(&data[4]));  //expect: 11.222.33 = 1122233
        tmp_u32 = tmp_u32 % 10000000;   //expect: 1122233 (major 11, minor 222, build 33)

        //check if VB meets condition for flashing
        status = properties_vb_validate_version_condition();
        if (status == S_OSUNMATCH)
        {
            cmdif_datainfo_responsedata.responsecode = CMDIF_ACK_VB_OUTOFDATE;
            cmdif_func_flasher_deinit_check();
            break;
        }
        else if (status == S_EXPIRED)
        {
            cmdif_datainfo_responsedata.responsecode = CMDIF_ACK_VB_OUTOFDATE;
            cmdif_func_flasher_deinit_check();
            break;
        }
        else if (status != S_SUCCESS)
        {
            cmdif_datainfo_responsedata.responsecode = CMDIF_ACK_FAILED;
            cmdif_func_flasher_deinit_check();
            break;
        }

        //check if App meets condition for flashing
        if (data[1] == 0x00 && (tmp_u32 % 100000) == 33300)
        {
            //old development version
            cmdif_datainfo_responsedata.responsecode = CMDIF_ACK_APP_OUTOFDATE;
            cmdif_func_flasher_deinit_check();
        }
        else if (tmp_u32 < MIN_APP_VERSION_REQUIRED)
        {
            cmdif_datainfo_responsedata.responsecode = CMDIF_ACK_APP_OUTOFDATE;
            cmdif_func_flasher_deinit_check();
        }
        else if (data[0] == CMDIF_ACT_STOCK_FLASHER)
        {
            //[1:BT_ACTION][1:AppVersionDummyFlag][2:reserved][4:AppVersion]
            //[n:"stock.bin" or "recvstock.bin"][NULL]
            if (length == (strlen((char*)&data[8])+8+1))
            {
                //[1:CMDIF_ACT_STOCK_FLASHER]
                //[n:"stock.bin" or "recvstock.bin"][NULL]
                status = cmdif_func_flasher_init_check(CMDIF_ACT_STOCK_FLASHER,
                                                       &data[8]);
            }
        }
        else if (length == 8)
        {
            //[1:flash_type from CMDIF_ACTION][3:reserved][4:AppVersion]
            status = cmdif_func_flasher_init_check(data[0],NULL);
        }
        break;
    case CMDIF_CMD_GET_STRATEGY:
        status = cmdif_func_flasher_get_strategy();
        break;
    case CMDIF_CMD_GET_STRATEGY_FLASHER:
        status = cmdif_func_flasher_get_strategy_flasher();
        break;
    case CMDIF_CMD_GET_TUNE_LIST_INFO:
        //data: [n:tunecode][NULL]
        if (length == (strlen((char*)data)+1))
        {
#if (USE_UARTTYPE_DATALINK)     //TODOQ: make this a variable
            status = cmdif_func_flasher_get_tune_list_info_complete(data);
#else
            status = cmdif_func_flasher_get_tune_list_info(data);
#endif
        }
        break;
    case CMDIF_CMD_SELECT_TUNE_STRATEGY:
        //data: [2:tune_selected_id][1:vehiclecode_index]
        if (length == 3)
        {
            status = cmdif_func_flasher_select_tune_preloaded(data);
        }
        break;
    case CMDIF_CMD_SET_EXTRA_CUSTOM_FILE_G2:
        //data: [n:checksumfilename][1:NULL]
        status = cmdif_func_set_extra_custom_file_g2(data);
        break;
    case CMDIF_CMD_SET_EXTRA_CUSTOM_FILE_G1:
        //data: [n:tuneosfilename][1:NULL][m:specialoptionfilename][1:NULL]
        status = cmdif_func_set_extra_custom_file_g1(data);
        break;
    case CMDIF_CMD_SELECT_TUNE_CUSTOM:
        tmp_u32 = 8+strlen((char*)&data[8])+1;
        if (length == (tmp_u32 + strlen((char*)&data[tmp_u32])+1))
        {
            if (strstr((char*)&data[8],".ctb"))
            {
                //data: [4:reserved][4:flags][n:tunefilename][1:NULL][m:tunedescription][1:NULL]
                status = cmdif_func_flasher_select_tune_custom_ctb(data);
            }
            else
            {
                //data:
                //[2:vehicle type defined for veh_defs]
                //[2:reserved][4:flags]
                //[n:tunefilename][1:NULL][m:tunedescription][1:NULL]
                status = cmdif_func_flasher_select_tune_custom(data);
            }
        }
        break;
    case CMDIF_CMD_SELECT_TUNE_CUSTOM_ADVANCE:
        //data: [4:flags][2:slot#]
        if (length == 6)
        {
            status = cmdif_func_flasher_select_tune_custom_advance(*(u32*)data,
                                                                   *(u16*)&data[4]);
        }
        break;
    case CMDIF_CMD_GET_FILE_FROM_OPTION_LOOKUP:
        //data: [4:flags][4:reserved][2:lookup item type][1:vehiclecode_index][1:reserved]
        if (length == 12)
        {
            //TODOQK: this is not done  //~~~
            status = cmdif_func_flasher_get_file_from_option_lookup(*(u32*)data,
                                                                    (GeneralFileLookupItemType)(*(u16*)&data[8]),
                                                                    data[11]);
        }
        break;
    //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    case CMDIF_CMD_GENERAL_REQUEST:
        if (length == 1)
        {
            status = cmdif_func_flasher_general_request(data[0]);
        }
        break;
    //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    case CMDIF_CMD_USE_OPTION:
        //data: [1:ecm_id][n:optionfilename][1:NULL]
        if (length == (1+strlen((char*)&data[1])+1))
        {
            status = cmdif_func_flasher_use_option(data,OptionSoure_RegularOption);
        }
        break;
    case CMDIF_CMD_USE_SPECIAL_OPTION:
        //data: [1:ecm_id][n:optionfilename][1:NULL]
        if (length == (1+strlen((char*)&data[1])+1))
        {
            status = cmdif_func_flasher_use_option(data,OptionSoure_SpecialOption);
        }
        break;
#endif  //#if (OBD2_SUPPORT_TUNE)
    //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
#if (OBD2_SUPPORT_DIAGNOSTIC)
    case CMDIF_CMD_READ_VIN:
        status = cmdif_func_readvin();
        break;
    case CMDIF_CMD_READ_ECM_INFO:
        status = cmdif_func_readecminfo();
        break;
    case CMDIF_CMD_GET_VEHICLE_INFO:
        status = cmdif_func_readvehicleinfo();
        break;
    case CMDIF_CMD_CLEAR_DTCS:
        status = cmdif_func_cleardtc();
        break;
    case CMDIF_CMD_READ_DTCS_RAW:
        status = cmdif_func_readdtc_raw();
        break;
    case CMDIF_CMD_READ_DTCS_NEW:
        status = cmdif_func_readdtc_new();
        break;
    case CMDIF_CMD_READ_DTCS_DATA:
        //TODOQ: what here?
        status = S_FAIL;
        break;
#endif  //#if (OBD2_SUPPORT_DIAGNOSTIC)
    case CMDIF_CMD_OBD2_GARBAGE_COLLECTOR:
        status = cmdif_func_obd2_garbagecollector();
        break;
    case CMDIF_CMD_SPECIAL_FUNCTIONS:
        status = cmdif_func_vehiclefunction(CMDIF_CMD_SPECIAL_FUNCTIONS);        
        break;
    //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    case CMDIF_CMD_GET_DONGLE_INFO:
        status = cmdif_func_get_device_info();
        break;
    case CMDIF_CMD_GET_DONGLE_DATA:
        //data: [1:opcode]
        if (length == 1)
        {
            status = cmdif_func_get_device_data(data[0]);
        }
        break;
    case CMDIF_CMD_GET_DONGLE_SETTINGS:
        //data: [1:opcode]
        if (length == 1)
        {
            status = cmdif_func_get_dongle_settings(data[0],0);
        }
        else if (length == 3)
        {
            status = cmdif_func_get_dongle_settings(data[0],*(u16*)&data[1]);
        }
        break;
    case CMDIF_CMD_PRELOADEDTUNE_RESTRICTION:
        //data: [4: restriction value]
        if (length == 4)
        {
            status = cmdif_func_preloaded_tune_set_restriction(*(u32*)data);
        }
        break;
    case CMDIF_CMD_PING_RESPONSE:
        status = cmdif_func_ping_response();
        break;
    //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    case CMDIF_CMD_BOOTLOADER_SETBOOT:
        //data: [2:BootloaderMode]
        if (length == 2)
        {
            status = cmdif_func_bootloader_setboot((BootloaderMode)(*(u16*)data));
        }
        break;
    case CMDIF_CMD_BOOTLOADER_INIT:
        //data: [64:encrypted firmware header]
        if (length == 64)
        {
            status = cmdif_func_bootloader_init(data);
        }
        break;
    case CMDIF_CMD_BOOTLOADER_DO:
        //data: [n:firmware data block]
        status = cmdif_func_bootloader_do(data,length);
        break;
    case CMDIF_CMD_BOOTLOADER_VALIDATE:
        //data: none
        status = cmdif_func_bootloader_validate();
        break;
    //----//
    case CMDIF_CMD_ENTER_BOOTLOAD:
        //data:[1:boot type]
        if (length == 1)
        {
            status = cmdif_func_enter_bootloader(data[0]);
        }
        break;
    //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    case CMDIF_CMD_GET_PROPERTIES:
        //data: [2: target]
        if (length == 2)
        {
            status = cmdif_func_get_properties(*(u16*)data);
        }
        break;
    //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    case CMDIF_CMD_HARDWARE:
        if (length >= 2)
        {
            status = cmdif_func_hardware(*(u16*)data,&data[2]);
        }
        break;
    //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    case CMDIF_CMD_VERSION_HEALTH_CHECK:
        //data:[1:type][3:reserved][4:app_version][24:reserved]
        if (length == 32)
        {
            status = cmdif_func_version_health_check(data[0],*(u32*)&data[4]);
        }
        break;
    //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
#ifdef __PRODUCTION_TEST__
    case CMDIF_CMD_PRODUCTION_TEST:
        //data:[1:test task]
        if (length == 1)
        {
            status = cmdif_func_production_test(data[0]);
        }
        break;
#endif //__PRODUCTION_TEST__
    //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    case CMDIF_CMD_DEBUG:
        //data:[4:debug task]
        if (length >= 4)
        {
            status = cmdif_func_debug(*((u32*)data),&data[4],length-4);
        }
        break;
    //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
#if (USE_WIFI)
    case CMDIF_CMD_WIFI_HANDLECONNECT:
        //data: [1:opcode][n:databuffer] 
        if (length >= 1)
        {
            status = cmdif_func_wifi_handleconnect((CMDIF_WIFI_OPCODE)data[0],(u8*)(data+1));
        }
        break;
    case CMDIF_CMD_WIFI_STATUS:        
        status = cmdif_func_wifi_status();
        break;
    case CMDIF_CMD_WIFI_UPDATES_CHECK:
        //data: [4:update request]
        if (length >= 4)
        {
            status = cmdif_func_wifi_check_update((CMDIF_ServerAccessType)(*((u32*)data)));
        }
        break;
    case CMDIF_CMD_WIFI_UPDATES_DOWNLOAD:
        //data: [4:update request]
        if (length >= 4)
        {
            status = cmdif_func_wifi_download_update((CMDIF_ServerAccessType)(*((u32*)data)));
        }
        break;
    case CMDIF_CMD_WIFI_UPDATES_APPLY:
        //data: [4:update request]
        if (length >= 4)
        {
            status = cmdif_func_wifi_apply_update((CMDIF_ServerAccessType)(*((u32*)data)));
        }
        break;
    case CMDIF_CMD_WIFI_GET_FILE_BY_ACCESS_STRING:
        //data: [4:access type request][n:ASCII Access String][1:NULL][n:Filename with path where to save file][1:NULL]
        if (length > 6)
        {
            tmp_u32 = 4;
            tmp_u32 += strlen((char*)&data[tmp_u32])+1; //length of GUID & NULL
            tmp_u8ptr = &data[tmp_u32];                 //pointer to filename
            tmp_u32 += strlen((char*)tmp_u8ptr)+1;      //length of filename & NULL
            if(tmp_u32 == length)
            {
                status = cmdif_func_wifi_download_files_by_access_string((CMDIF_ServerAccessType)(*((u32*)data)),
                                                                         &data[4], tmp_u8ptr);
            }
        }
        break;
    case CMDIF_CMD_WIFI_GET_CLOUD_TUNELIST:
        status = cmdif_func_wifi_get_cloudtune_list();
        break;
    case CMDIF_CMD_WIFI_DOWNLOAD_CLOUD_FILE:
        //data: [2:file index]
        if(length > 1)
        {
            u16 index;
            memcpy(&index, data, sizeof(index));
            status = cmdif_func_wifi_download_cloud_file(index);
        }
        break;
    case CMDIF_CMD_WIFI_CHECK_LIMIT:
        status = cmdif_func_wifi_check_limit();
        break;
    case CMDIF_CMD_WIFI_DEACTIVATE:
        status = cmdif_func_wifi_deactivate();
        break;
#endif
    //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    default:
        status = cmdif_func_unsupported_command();
        break;
    }

    cmdif_tracker_set_response_ack(cmdif_datainfo_responsedata.responsecode);
    cmdif_tracker_set_state(CMDIF_TRACKER_STATE_COMPLETE);
    settings_update(SETTINGS_UPDATE_IF_DIRTY);
    return status;
}

//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
u8 cmdif_getResponseData(cmdif_datainfo **datainfo)
{
    *datainfo = &cmdif_datainfo_responsedata;
    return S_SUCCESS;
}

//------------------------------------------------------------------------------
// Register a command to track
// Input:   CMDIF_COMMAND commandcode
// Engineer: Quyen Leba
//------------------------------------------------------------------------------
void cmdif_tracker_track_new_command(CMDIF_COMMAND commandcode)
{
    cmdif_tracker_info.commandcode = commandcode;
    cmdif_tracker_info.responsecode = CMDIF_ACK_INVALID;
    cmdif_tracker_info.state = CMDIF_TRACKER_STATE_IDLE;
}

/**
 * @brief   Get current command being tracked
 *
 * @retval  CMDIF_COMMAND
 *
 * @author  Quyen Leba
 */
CMDIF_COMMAND cmdif_tracker_get_current_command()
{
    return cmdif_tracker_info.commandcode;
}

//------------------------------------------------------------------------------
// Update tracker state
// Input:   CMDIF_TRACKER_STATE state
// Engineer: Quyen Leba
//------------------------------------------------------------------------------
void cmdif_tracker_set_state(CMDIF_TRACKER_STATE state)
{
    cmdif_tracker_info.state = state;
}

//------------------------------------------------------------------------------
// Update tracker response ack
// Input:   CMDIF_ACK responsecode
// Engineer: Quyen Leba
//------------------------------------------------------------------------------
void cmdif_tracker_set_response_ack(CMDIF_ACK responsecode)
{
    cmdif_tracker_info.responsecode = responsecode;
}

//------------------------------------------------------------------------------
// Update tracker private data
// Input:   CMDIF_ACK responsecode
// Engineer: Quyen Leba
//------------------------------------------------------------------------------
u8 cmdif_tracker_set_private_data(u8 *data, u8 datalength)
{
    if (datalength > CMDIF_TRACKER_PRIVDATA_LENGTH_MAX)
    {
        memset((char*)cmdif_tracker_info.privdata,0,CMDIF_TRACKER_PRIVDATA_LENGTH_MAX);
        cmdif_tracker_info.privdatalength = 0;
        return S_INPUT;
    }
    memcpy((char*)cmdif_tracker_info.privdata,(char*)data,datalength);
    cmdif_tracker_info.privdatalength = datalength;
    return S_SUCCESS;
}

//------------------------------------------------------------------------------
// Get a tracker report
// Outputs: u8 *reportdata (the report)
//          u16 *reportdatalength
// Engineer: Quyen Leba
// TODOQ: add more to or remove items from tracker_report; need a meetings on this
//------------------------------------------------------------------------------
void cmdif_tracker_get_report(u8 *reportdata, u16 *reportdatalength)
{
    typedef struct
    {
        u8  version;
        u8  married_status;
        u16 vehtype_min;
        u16 vehtype_max;
        u8  serialnumber[SETTINGS_CRITICAL_SERIALNUMBER_LENGTH];
        u8  devicepartnumberstring[SETTINGS_CRITICAL_DEVICEPARTNUMBERSTRING_LENGTH];

        cmdif_tracker tracker_info;
    }tracker_report;
    tracker_report *tracker_report_data = (tracker_report*)reportdata;

    tracker_report_data->version = 0;
    tracker_report_data->married_status = SETTINGS_TUNE(married_status);
    tracker_report_data->vehtype_min = VEH_GetMinVehType();
    tracker_report_data->vehtype_max = VEH_GetMaxVehType();

    settings_get_deviceserialnumber(tracker_report_data->serialnumber);
    settings_get_devicepartnumberstring(tracker_report_data->devicepartnumberstring);
    
    *reportdatalength = sizeof(tracker_report);
//    settings_get_programmerinfop2_flags;
}

//------------------------------------------------------------------------------
// Progressbar report function
// Input:   u8 percentage
// Engineer: Quyen Leba
// Note: use cmdif_progressbar_setup to setup this func
//------------------------------------------------------------------------------
void cmdif_progressbar_report(u8 percentage)
{
    u8  buffer[128+2];
    u16 bufferlength;

    buffer[0] = percentage;
    buffer[1] = NULL;
    bufferlength = 2;
    if (cmdif_tracker_info.progbar_string)
    {
        bufferlength = strlen((char*)cmdif_tracker_info.progbar_string);
        if (bufferlength > sizeof(buffer)-2)
        {
            bufferlength = sizeof(buffer)-2;
        }
        memcpy((char*)&buffer[1],(char*)cmdif_tracker_info.progbar_string,bufferlength);
        buffer[bufferlength+1] = NULL;
        bufferlength += 2;
    }
    cmdif_internalresponse_ack(cmdif_tracker_info.progbar_commandcode,
                               CMDIF_ACK_PROGRESSBAR,buffer,bufferlength);
}

/**
 *  cmdif_progressbar_report_semi
 *  
 *  @brief Report progressbar status through CMDIF
 *
 *  @param[in]  percentage
 *  @param[in]  string
 *  
 *  @authors Quyen Leba
 */
void cmdif_progressbar_report_semi(u8 percentage, u8 *string)
{
    u8  buffer[64+2];
    u16 bufferlength;

    buffer[0] = percentage;
    bufferlength = 2;

    if (string)
    {
        strncpy((char*)&buffer[1],(char*)string,sizeof(buffer)-2);
        bufferlength = 1+strlen((char*)&buffer[1]);
        buffer[bufferlength++] = NULL;
    }
    cmdif_internalresponse_ack(cmdif_tracker_info.progbar_commandcode,
                               CMDIF_ACK_PROGRESSBAR,buffer,bufferlength);
}

/**
 *  cmdif_progressbar_report_full
 *  
 *  @brief Report progressbar status through CMDIF
 *
 *  @param[in]  cmd
 *  @param[in]  percentage
 *  @param[in]  string
 *  
 *  @authors Quyen Leba
 */
void cmdif_progressbar_report_full(CMDIF_COMMAND cmd, u8 percentage, u8 *string)
{
    u8  buffer[64+2];
    u16 bufferlength;

    buffer[0] = percentage;
    bufferlength = 2;

    if (string)
    {
        strncpy((char*)&buffer[1],(char*)string,sizeof(buffer)-2);
        bufferlength = 1+strlen((char*)&buffer[1]);
        buffer[bufferlength++] = NULL;
    }
    cmdif_internalresponse_ack(cmd,CMDIF_ACK_PROGRESSBAR,buffer,bufferlength);
}

//------------------------------------------------------------------------------
// Setup for progressbar report function
// Inputs:  CMDIF_COMMAND cmd
//          const u8 *string (NULL allowed)
// Engineer: Quyen Leba
//------------------------------------------------------------------------------
void cmdif_progressbar_setup(CMDIF_COMMAND cmd, const u8 *string)
{
    cmdif_tracker_info.progbar_commandcode = cmd;
    cmdif_tracker_info.progbar_string = (u8*)string;
}

/**
 * @brief   Internal response ack
 *
 * @param   [in] command
 * @param   [in] response
 * @param   [in] data
 * @param   [in] datalen
 *
 * @retval  u8 status
 *
 * @author  Quyen Leba
 *
 * @details Send internal response ack. Used during listening mode.
 */
u8 cmdif_internalresponse_ack(CMDIF_COMMAND command, CMDIF_ACK response, u8 *data, u16 datalen)
{
    u8  status;

    status = uch_tune_internal_response_ack(command,response,data,datalen);
    if (status == S_NOTREQUIRED)
    {
        status = cmdif_response_ack(command,response,data,datalen);
    }
    cmdif_tracker_set_response_ack(response);
    cmdif_tracker_set_private_data(data,datalen);

    return status;
}
