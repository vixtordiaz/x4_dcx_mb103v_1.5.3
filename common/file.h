/**
  **************** (C) COPYRIGHT 2011 SCT Performance, LLC *********************
  * File Name          : file.h
  * Device / System    : SCT products using the "CommonCode" platform
  * Author             : Quyen Leba
  *
  * Version            : 1 
  * Date               : 05/02/2011
  * Description        : 
  *                    : 
  *
  *
  * History            : 05/02/2011 M. Davis
  *                    :   Add SCT Copyright header,
  *
  ******************************************************************************
  */

#ifndef __FILE_H
#define __FILE_H

#include <arch/gentype.h>
#include <fs/genfs.h>
#include <common/crypto_blowfish.h>

typedef void (file_copy_progressreport)(u8 percentage, u8 *message);
typedef void (simple_blockcrypto)(u8 *buffer, u32 bufferlength, u32 *key);
typedef u8 (bf_blockcrypto)(blowfish_context_ptr *ctx, u8 *buf, u32 len);

typedef enum
{
    file_blockcrypto_type_tea = 0,
    file_blockcrypto_type_bf = 1,
}file_blockcrypto_type;

typedef struct
{
    file_blockcrypto_type type;
    u32 *key;    
    simple_blockcrypto *crypto;
}file_simpleblockcrypto;

typedef struct
{
    file_blockcrypto_type type;
    blowfish_context_ptr *ctx;
    bf_blockcrypto *crypto;
}file_bfblockcrypto;

typedef union
{
    file_simpleblockcrypto simplecrypto;
    file_bfblockcrypto bfcrypto;
}file_blockcrypto;

typedef struct
{
    u8  *filename;
    u32 filecrc32e;
    u32 filesize;
}file_info;

bool file_isexist(u8 *filename);
bool file_isfilenamevalid(u8 *filename, u32 maxnamelength);
u8 file_getfilename_from_path(u8 *path, u8 *filename);
u8 file_general_getfilename(u8 *input_string, u8 *output_filename);
u8 file_user_getfilename(u8 *input_string, u8 *output_filename);
u8 file_default_getfilename(u8 *input_string, u8 *output_filename);
u8 file_getfilesize(F_FILE *fptr, u32 *filesize);
u8 file_readline(F_FILE *fptr, u8 *linedata, u32 *linedatalength);
u8 file_copy(const u8 *source_filename, const u8 *dest_filename,
             file_blockcrypto *source_decryption,
             file_blockcrypto *dest_encryption,
             file_copy_progressreport progressreport_funcptr);
u8 file_append(const u8 *source_filename, const u8 *dest_filename,
               u32 source_offset, u32 source_bytetocopy,
               file_blockcrypto *source_decryption,
               file_blockcrypto *dest_encryption,
               file_copy_progressreport progressreport_funcptr);
u32 file_getsize_byname(u8 *filename);
u32 file_getsize_byfptr(F_FILE *fptr);
u8 file_calcfilecrc32e_byfilename(u8* filename, u32 *crc32e, u8 paddingbyte);
u8 file_calcfilecrc32e(F_FILE *fptr, u32 *crc32e);
u8 file_handle_blockcrypto(file_blockcrypto function,  u8 *databuffer, u32 databufferlength);
u8 file_filematch_check(const u8 *filename, u32 file_size, u32 file_crc32e);

u8 file_copy_overlay(u32 position, u32 length, const u8 *source_filename, 
                     const u8 *dest_filename, file_blockcrypto *source_decryption, 
                     file_blockcrypto *dest_encryption, file_copy_progressreport progressreport_funcptr);
u8 file_read_at_position(u32 position, u8 *data, u32 datalength,
                        u32 *bytecount, F_FILE *file_fptr, bool is_stock_key);
u8 file_set_position(u32 pos, F_FILE *file_fptr);
u8 file_get_position(u32 *pos, F_FILE *file_fptr);
u8 file_read(u8 *data, u32 datalength, u32 *bytecount, F_FILE *file_fptr,
             bool is_stock_key);
u8 file_encrypted_read(F_FILE *fptr, u32 datalength, u8 *data, u32 *bytecount,
                       file_blockcrypto *decryption_info);

#endif    //__FILE_H
