/**
  **************** (C) COPYRIGHT 2011 SCT Performance, LLC *********************
  * File Name          : filestock_option.c
  * Device / System    : SCT products using the "CommonCode" platform
  * Author             : Quyen Leba
  *
  * Version            : 1 
  * Date               : 05/02/2011
  * Description        : 
  *                    : 
  *
  *
  * History            : 05/02/2011 M. Davis
  *                    :   Add SCT Copyright header,
  *
  ******************************************************************************
  */

#include <fs/genfs.h>
#include <board/genplatform.h>
#include <common/statuscode.h>
#include <common/tea.h>
#include "filestock_option.h"

extern const u32 stockfile_tea_key[4];              //from filestock.c

#define FILESTOCK_OPTION_VERSION                    1
#define FILESTOCK_OPTION_FILENAME                   GENFS_USER_FOLDER_PATH"stockspopts.bin"
#define FILESTOCK_OPTION_CONTENT_MAX_LENGTH         2048
#define FILESTOCK_OPTION_SKIP_VALIDATE_STOCK_FILE   0
#define FILESTOCK_OPTION_ENCRYPTION_KEY             (u32*)stockfile_tea_key

#define IsVehicleType60L(vt)           ((vt==12) || (vt==13))
#define IsVehicleType67L(vt)           ((vt==53) || (vt==57) || (vt==58) || (vt==60) || (vt==61) || (vt==73) || (vt==74))
#define IsVehicleTypeF150Ecoboost35L(vt)   ((vt==59))

// FileStockOptionHeader
// Version Change Log
// Version 0 -> Version 1 06/25/2014 PDowns.
// 1. No longer use veh_type. Options used are now in FileStockOptionContent->OptionFlags.
typedef struct
{
    u8  version;
    u8  reserved0;
    u16 veh_type;       // DEPRICATED in V1, DO NO USE!!! 
    u32 header_crc32e;
    u32 content_crc32e;
    u16 content_actual_length;
    u8  reserved1[50];
}FileStockOptionHeader;
STRUCT_SIZE_CHECK(FileStockOptionHeader,64);
STRUCT_MAX_SIZE_CHECK(FileStockOptionHeader,FILESTOCK_OPTION_CONTENT_MAX_LENGTH);

typedef struct
{
    struct
    {
        u8  valid_front_tire_size       : 1;
        u8  valid_rear_tire_size        : 1;
        u8  valid_axle_ratio            : 1;
        u8  valid_front_tire_pressure   : 1;
        u8  valid_rear_tire_pressure    : 1;
        u8  reserved0                   : 3;
        u8  reserved1[63];
    }control;
    struct
    {
        u32 front;
        u32 rear;
    }tire_size;
    struct
    {
        u32 ratio;
    }axle_ratio;
    struct
    {
        u16 front_tire_pressure;
        u8  reserved0[2];
        u16 rear_tire_pressure;
        u8  reserved1[2];
    }tire_pressure;
}FileStockOptionContentV0;
STRUCT_MAX_SIZE_CHECK(FileStockOptionContentV0,FILESTOCK_OPTION_CONTENT_MAX_LENGTH);

typedef struct
{
    OptionFlags option_flags;
    
    struct
    {
        u32 front;
        u32 rear;
    }tire_size;
    struct
    {
        u32 ratio;
    }axle_ratio;
    struct
    {
        u16 front_tire_pressure;
        u8  reserved0[2];
        u16 rear_tire_pressure;
        u8  reserved1[2];
    }tire_pressure;
}FileStockOptionContent;
STRUCT_MAX_SIZE_CHECK(FileStockOptionContent,FILESTOCK_OPTION_CONTENT_MAX_LENGTH);

//IMPORTANT: only use this struct within firmware
//IMPORTANT: this struct can be changed any time
//it is a stripped down version of FileStockOptionContent to conserve RAM
typedef struct
{
    OptionFlags option_flags;
    
    struct
    {
        u32 front;
        u32 rear;
    }tire_size;
    struct
    {
        u32 ratio;
    }axle_ratio;
    struct
    {
        u16 front;
        u16 rear;
    }tire_pressure;
}FileStockOptionInfo;

FileStockOptionInfo *filestock_option_info = NULL;

u8 filestock_option_get_option_info(FileStockOptionInfo *info);
u8 filestock_option_save_option_info(FileStockOptionInfo *info);

/**
 * @brief   Delete the filestock option file
 *
 * @author  Patrick Downs
 *
 * @retval u8  status
 *
 */
u8 filestock_option_delete()
{
    genfs_deletefile(FILESTOCK_OPTION_FILENAME);
    SETTINGS_TUNE(stockspoptsfile_crc32e) = 0;
    SETTINGS_SetTuneAreaDirty();
    
    settings_update(SETTINGS_UPDATE_IF_DIRTY);
    
    return S_SUCCESS;
}

/**
 * @brief   Init filestock_option for download (get info from file)
 *
 * @author  Quyen Leba
 *
 * @retval u8  status
 *
 */
u8 filestock_option_download_init()
{
    u8 status;
    
    status = filestock_option_init();
    if (status == S_SUCCESS)
    {
        status = filestock_option_load_info();
    }
    
    if (status != S_SUCCESS)
    {
        filestock_option_deinit();
    }

    return status;
}

/**
 * @brief   Deinit filestock_option for download
 *
 * @author  Quyen Leba
 *
 * @retval u8  status
 *
 */
u8 filestock_option_download_deinit()
{
    filestock_option_save_info();
    
    filestock_option_deinit();

    return S_SUCCESS;
}

/**
 * @brief   Init to use filestock_option functions
 *
 * @author  Quyen Leba
 *
 * @retval u8  status
 *
 */
u8 filestock_option_init()
{
    filestock_option_deinit();
    filestock_option_info = __malloc(sizeof(FileStockOptionInfo));
    if (!filestock_option_info)
    {
        return S_MALLOC;
    }

    memset((char*)filestock_option_info,0,sizeof(FileStockOptionInfo));

    return S_SUCCESS;
}

void filestock_option_deinit()
{
    if (filestock_option_info)
    {
        __free(filestock_option_info);
        filestock_option_info = NULL;
    }
}

/**
 * @brief   Load filestock_option info from file. Use when device is married.
 *
 * @author  Quyen Leba
 *
 * @retval u8  status
 *
 */
u8 filestock_option_load_info()
{
    u8  status;

    status = S_ERROR;
    if (filestock_option_info)
    {
        status = filestock_option_get_option_info(filestock_option_info);
    }
    return status;
}

/**
 * @brief   Save filestock_option info to file. Use only when set device to married.
 *
 * @author  Quyen Leba
 *
 * @retval u8  status
 *
 */
u8 filestock_option_save_info()
{
    u8  status;

    status = S_ERROR;
    if (filestock_option_info)
    {
        status = filestock_option_save_option_info(filestock_option_info);
    }
    return status;
}

/**
 * @brief   Keep axle ratio info (not yet saved to file)
 *
 * @author  Quyen Leba
 *
 * @retval u8  status
 *
 */
u8 filestock_option_keep_axle_ratio(u32 ratio)
{
    u8  status;

    status = S_ERROR;
    if (filestock_option_info)
    {
        filestock_option_info->axle_ratio.ratio = ratio;
        status = S_SUCCESS;
    }
    return status;
}

/**
 * @brief   Get axle ratio info (only use when device is married)
 *
 * @author  Quyen Leba
 *
 * @param   [out]   ratio
 *
 * @retval u8  status
 *
 */
u8 filestock_option_get_axle_ratio(u32 *ratio)
{
    u8  status;

    status = S_ERROR;
    if (filestock_option_info && ratio)
    {
        *ratio = filestock_option_info->axle_ratio.ratio;
        status = S_SUCCESS;
    }
    return status;
}

/**
 * @brief   Check For New Options Flags and save if detected
 *
* @author   Patrick Downs
 *
 * @param   [in]    option_flags
 *
 * @retval u8  status
 *
 */
u8 filestock_option_keep_new_options_flags(OptionFlags option_flags)
{
    u8  status;

    status = S_ERROR;
    if (filestock_option_info)
    {
        filestock_option_info->option_flags.i |= option_flags.i;
        
        status = S_SUCCESS;
    }
    return status;
}

/**
 * @brief   Keep Options Flags (not yet saved to file)
 *
* @author   Patrick Downs
 *
 * @param   [in]    option_flags
 *
 * @retval u8  status
 *
 */
u8 filestock_option_keep_options_flags(OptionFlags option_flags)
{
    u8  status;

    status = S_ERROR;
    if (filestock_option_info)
    {
        filestock_option_info->option_flags = option_flags;        
        status = S_SUCCESS;
    }
    return status;
}

/**
 * @brief   Get Options Flags (only use when device is married)
 *
* @author   Patrick Downs
 *
 * @param   [out]    option_flags
 *
 * @retval u8  status
 *
 */
u8 filestock_option_get_options_flags(OptionFlags *option_flags)
{
    u8  status;

    status = S_ERROR;
    if (filestock_option_info)
    {
        *option_flags = filestock_option_info->option_flags;
        status = S_SUCCESS;
    }
    return status;
}

/**
 * @brief   Keep tire size info (not yet saved to file)
 *
 * @author  Quyen Leba
 *
 * @param   [in]    front
 * @param   [in]    rear
 *
 * @retval u8  status
 *
 */
u8 filestock_option_keep_tire_size(u32 front, u32 rear)
{
    u8  status;

    status = S_ERROR;
    if (filestock_option_info)
    {
        filestock_option_info->tire_size.front = front;
        filestock_option_info->tire_size.rear = rear;
        status = S_SUCCESS;
    }
    return status;
}

/**
 * @brief   Get tire size info (only use when device is married)
 *
 * @author  Quyen Leba
 *
 * @param   [out]   front - Null will skip
 * @param   [out]   rear - Null will skip
 *
 * @retval u8  status
 *
 */
u8 filestock_option_get_tire_size(u32 *front, u32 *rear)
{
    u8  status;

    status = S_ERROR;
    if (filestock_option_info && front && rear)
    {   
        *front = filestock_option_info->tire_size.front;
        *rear = filestock_option_info->tire_size.rear;
        status = S_SUCCESS;
    }
    
    return status;
}

/**
 * @brief   Keep tire pressure info (not yet saved to file)
 *
 * @author  Quyen Leba, Patrick Downs
 *
 * @param   [in]    front - Null OK if N/A
 * @param   [in]    rear - Null OK if N/A
 *
 * @retval u8  status
 *
 */
u8 filestock_option_keep_tire_pressure(u16 front, u16 rear)
{
    u8  status;

    status = S_ERROR;
    if (filestock_option_info)
    {
        if(front)
        {
            filestock_option_info->tire_pressure.front = front;
        }
        if(rear)
        {
            filestock_option_info->tire_pressure.rear = rear;
        }
        status = S_SUCCESS;
    }
    
    return status;
}

/**
 * @brief   Get tire pressure info (only use when device is married)
 *
 * @author  Quyen Leba
 *
 * @param   [out]   front
 * @param   [out]   rear
 *
 * @retval u8  status
 *
 */
u8 filestock_option_get_tire_pressure(u16 *front, u16 *rear)
{
    u8  status;

    status = S_ERROR;
    if (filestock_option_info && front && rear)
    {
        *front = filestock_option_info->tire_pressure.front;        
        *rear = filestock_option_info->tire_pressure.rear;
        status = S_SUCCESS;
    }
    
    return status;
}

/**
 * @brief   Get option info from the stock option file
 *
 * @author  Quyen Leba
 *
 * @param   [out]   info
 *
 * @retval u8  status
 *
 */
u8 filestock_option_get_option_info(FileStockOptionInfo *info)
{
    F_FILE *fptr;
    u8  *header;
    u8  *content;
    u32 bytecount;
    u32 crc32e_calc;
    u32 crc32e_cmp;
    u8  status;

    memset((char*)info,0,sizeof(FileStockOptionInfo));

    header = NULL;
    content = NULL;
    fptr = genfs_user_openfile(FILESTOCK_OPTION_FILENAME,"r");
    if (!fptr)
    {
        if (SETTINGS_TUNE(stockspoptsfile_crc32e) == 0)
        {
            status = S_SUCCESS; // No file yet
        }
        else
        {
            status = S_OPENFILE;
        }
        goto filestock_option_validate_done;
    }

    header = __malloc(sizeof(FileStockOptionHeader));
    if (!header)
    {
        status = S_MALLOC;
        goto filestock_option_validate_done;
    }

    //check header
    bytecount = fread((char*)header,1,sizeof(FileStockOptionHeader),fptr);
    if (bytecount != sizeof(FileStockOptionHeader))
    {
        status = S_READFILE;
        goto filestock_option_validate_done;
    }
    tea_decryptblock(header,bytecount,FILESTOCK_OPTION_ENCRYPTION_KEY);

    crc32e_reset();
    crc32e_cmp = ((FileStockOptionHeader *)header)->header_crc32e;
    ((FileStockOptionHeader *)header)->header_crc32e = 0xFFFFFFFF;
    crc32e_calc = crc32e_calculateblock(0xFFFFFFFF,(u32*)header,bytecount/4);
    if (crc32e_calc != crc32e_cmp)
    {
        status = S_CRC32E;
        goto filestock_option_validate_done;
    }

    if (((FileStockOptionHeader *)header)->version > FILESTOCK_OPTION_VERSION)
    {
        status = S_NOTSUPPORT;
        goto filestock_option_validate_done;
    }
    else if (((FileStockOptionHeader *)header)->content_actual_length > FILESTOCK_OPTION_CONTENT_MAX_LENGTH)
    {
        status = S_BADCONTENT;
        goto filestock_option_validate_done;
    }
    
    content = __malloc(FILESTOCK_OPTION_CONTENT_MAX_LENGTH);
    if (!content)
    {
        status = S_MALLOC;
        goto filestock_option_validate_done;
    }
    
    //check content
    crc32e_cmp = ((FileStockOptionHeader *)header)->content_crc32e;
    bytecount = fread((char*)content,1,FILESTOCK_OPTION_CONTENT_MAX_LENGTH,fptr);
    if (bytecount != FILESTOCK_OPTION_CONTENT_MAX_LENGTH)
    {
        status = S_READFILE;
        goto filestock_option_validate_done;
    }
    crc32e_reset();
    crc32e_calc = crc32e_calculateblock(0xFFFFFFFF,(u32*)content,bytecount/4);
    if (crc32e_calc != crc32e_cmp)
    {
        status = S_CRC32E;
        goto filestock_option_validate_done;
    }
    tea_decryptblock(content,bytecount,FILESTOCK_OPTION_ENCRYPTION_KEY);
        
    if(((FileStockOptionHeader *)header)->version == 0) // Handle Upgrade to Version 1
    {
        u16 vehtype = ((FileStockOptionHeader *)header)->veh_type;
        FileStockOptionContentV0* cptr;        
        cptr = (FileStockOptionContentV0*)content;
        
        info->axle_ratio.ratio = cptr->axle_ratio.ratio;
        info->tire_size.front = cptr->tire_size.front;
        info->tire_size.rear = cptr->tire_size.rear;
        info->tire_pressure.front = cptr->tire_pressure.front_tire_pressure;
        info->tire_pressure.rear = cptr->tire_pressure.rear_tire_pressure;
        
        if(IsVehicleType60L(vehtype))
        {
            if(cptr->control.valid_axle_ratio)
            {
                info->option_flags.f.opcode_c_mathindex_12H = 1;
            }
        }
        if(IsVehicleType67L(vehtype))
        {
            if(cptr->control.valid_front_tire_size || 
               cptr->control.valid_front_tire_size)
            {
                info->option_flags.f.opcode_c_mathindex_14H = 1;
            }
            
            if(cptr->control.valid_axle_ratio)
            {
                info->option_flags.f.opcode_c_mathindex_15H = 1;
            }
            
            if(cptr->control.valid_front_tire_pressure || 
               cptr->control.valid_rear_tire_pressure)
            {
                info->option_flags.f.opcode_c_mathindex_16H = 1;
                info->option_flags.f.opcode_c_mathindex_17H = 1;
            }
        }
        if(IsVehicleTypeF150Ecoboost35L(vehtype))
        {
            if(cptr->control.valid_front_tire_pressure || 
               cptr->control.valid_rear_tire_pressure)
            {
                info->option_flags.f.opcode_c_mathindex_16H = 1;
                info->option_flags.f.opcode_c_mathindex_17H = 1;
            }
        }
        
        status = S_SUCCESS;
    }
    else
    {
        FileStockOptionContent* cptr;        
        cptr = (FileStockOptionContent*)content;
        
        info->option_flags = cptr->option_flags;
        info->axle_ratio.ratio = cptr->axle_ratio.ratio;
        info->tire_pressure.front = cptr->tire_pressure.front_tire_pressure;
        info->tire_pressure.rear = cptr->tire_pressure.rear_tire_pressure;
        info->tire_size.front = cptr->tire_size.front;
        info->tire_size.rear = cptr->tire_size.rear;
        
        status = S_SUCCESS;
    }

filestock_option_validate_done:
    if (fptr)
    {
        genfs_closefile(fptr);
    }
    if (header)
    {
        __free(header);
    }
    if (content)
    {
        __free(content);
    }
    return status;
}

/**
 * @brief   Save option info to the stock option file
 *
 * @author  Quyen Leba
 *
 * @param   [in]    info
 *
 * @retval u8  status
 *
 */
u8 filestock_option_save_option_info(FileStockOptionInfo *info)
{
    FileStockOptionHeader header;
    F_FILE *fptr;
    u8  *bptr;
    u32 bytecount;
    u8  status;
    u32 filecrc32e;

    bptr = __malloc(FILESTOCK_OPTION_CONTENT_MAX_LENGTH);
    if (!bptr)
    {
        return S_MALLOC;
    }

    memset((char*)&header,0,sizeof(FileStockOptionHeader));
    header.version = FILESTOCK_OPTION_VERSION;
    header.header_crc32e = 0xFFFFFFFF;
    header.content_actual_length = sizeof(FileStockOptionContent);

    memset((char*)bptr,0,FILESTOCK_OPTION_CONTENT_MAX_LENGTH);
    ((FileStockOptionContent *)bptr)->option_flags = info->option_flags;
    ((FileStockOptionContent *)bptr)->axle_ratio.ratio = info->axle_ratio.ratio;
    ((FileStockOptionContent *)bptr)->tire_size.front = info->tire_size.front;
    ((FileStockOptionContent *)bptr)->tire_size.rear = info->tire_size.rear;
    ((FileStockOptionContent *)bptr)->tire_pressure.front_tire_pressure = info->tire_pressure.front;
    ((FileStockOptionContent *)bptr)->tire_pressure.rear_tire_pressure = info->tire_pressure.rear;

    tea_encryptblock(bptr,FILESTOCK_OPTION_CONTENT_MAX_LENGTH,FILESTOCK_OPTION_ENCRYPTION_KEY);

    crc32e_reset();
    header.content_crc32e = crc32e_calculateblock(0xFFFFFFFF,(u32*)bptr,FILESTOCK_OPTION_CONTENT_MAX_LENGTH/4);

    crc32e_reset();
    header.header_crc32e = crc32e_calculateblock(0xFFFFFFFF,(u32*)&header,sizeof(FileStockOptionHeader)/4);
    tea_encryptblock((u8*)&header,sizeof(FileStockOptionHeader),FILESTOCK_OPTION_ENCRYPTION_KEY);

    fptr = genfs_user_openfile(FILESTOCK_OPTION_FILENAME,"w");
    if (!fptr)
    {
        status = S_OPENFILE;
        goto filestock_option_save_option_info_done;
    }

    bytecount = fwrite((char*)&header,1,sizeof(FileStockOptionHeader),fptr);
    if (bytecount != sizeof(FileStockOptionHeader))
    {
        status = S_WRITEFILE;
        goto filestock_option_save_option_info_done;
    }

    bytecount = fwrite((char*)bptr,1,FILESTOCK_OPTION_CONTENT_MAX_LENGTH,fptr);
    if (bytecount != FILESTOCK_OPTION_CONTENT_MAX_LENGTH)
    {
        status = S_WRITEFILE;
        goto filestock_option_save_option_info_done;
    }
    
    // Calculate file CRC32 and save to settings for validation
    crc32e_reset();    
    filecrc32e = crc32e_calculateblock(0xFFFFFFFF,(u32*)&header,sizeof(FileStockOptionHeader)/4);
    filecrc32e = crc32e_calculateblock(filecrc32e,(u32*)bptr,FILESTOCK_OPTION_CONTENT_MAX_LENGTH/4);
    
    SETTINGS_TUNE(stockspoptsfile_crc32e) = filecrc32e;
    SETTINGS_SetTuneAreaDirty();
    settings_update(FALSE);
    
    status = S_SUCCESS;
    
filestock_option_save_option_info_done:
    if (fptr)
    {
        genfs_closefile(fptr);
    }
    if (bptr)
    {
        __free(bptr);
    }
    return status;
}

/**
 * @brief   Validate stock option file
 *
 * @author  Quyen Leba
 *
 * @param   [in]    crc32e
 * @param   [out]   crc32e (of stock option file; NULL allowed)
 *
 * @retval u8  status
 *
 */
u8 filestock_option_validate(u32 *crc32e)
{
#if FILESTOCK_OPTION_SKIP_VALIDATE_STOCK_FILE
#ifdef __DEBUG_JTAG_
#warning filestock_option.c: Skip validate stock -- very dangerous -- debug only
#else
#error filestock_option.c: Skip validate stock !!!
#endif
    return S_SUCCESS;
#else
    F_FILE *fptr;
    u8  buffer[2048];
    u32 bytecount;
    u32 crc32e_calc;
    u8 status;

    status = S_FAIL;

    fptr = genfs_user_openfile(FILESTOCK_OPTION_FILENAME,"r");
    if (fptr)
    {
        crc32e_reset();
        crc32e_calc = 0xFFFFFFFF;
        while(!feof(fptr))
        {
            bytecount = fread(buffer,1,sizeof(buffer),fptr);
            crc32e_calc = crc32e_calculateblock(crc32e_calc,(u32*)buffer,bytecount/4);
        }
        genfs_closefile(fptr);

        if (SETTINGS_TUNE(stockspoptsfile_crc32e) == crc32e_calc)
        {
            status = S_SUCCESS;
        }
        else if (SETTINGS_TUNE(stockspoptsfile_crc32e) == 0)
        {
            //bypass crc32e check for backward compatibility (pre-X4 initial firmware release)
            if (crc32e)
            {
                *crc32e = 0;
            }
            status = S_SUCCESS;
        }
        else
        {
            status = S_CRC32E;
        }
        if (crc32e)
        {
            *crc32e = crc32e_calc;
        }
    }
    else
    {
        if (SETTINGS_TUNE(stockspoptsfile_crc32e) == 0)
        {
            //bypass crc32e check for backward compatibility (pre-X4 initial firmware release)
            if (crc32e)
            {
                *crc32e = 0;
            }
            status = S_SUCCESS;
        }
        else
        {
            status = S_OPENFILE;
        }
    }
    
    return status;
#endif
}

