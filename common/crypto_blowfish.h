/**
  **************** (C) COPYRIGHT 2011 SCT Performance, LLC *********************
  * File Name          : crypto_blowfish.h
  * Device / System    : SCT products using the "CommonCode" platform
  * Author             : Quyen Leba
  *
  * Version            : 1 
  * Date               : 05/02/2011
  * Description        : 
  *                    : 
  *
  *
  * History            : 05/02/2011 M. Davis
  *                    :   Add SCT Copyright header,
  *
  ******************************************************************************
  */

#ifndef __CRYPTO_BLOWFISH_H
#define __CRYPTO_BLOWFISH_H

#include <arch/gentype.h>

typedef struct
{
    bool usepointer;    //always FALSE
    u32 pbox[18];
    u32 sbox[4][256];
}blowfish_context_t;

typedef struct
{
    bool usepointer;    //always TRUE
    u32 *pbox;
    u32 *sbox[4];
}blowfish_context_ptr;

extern blowfish_context_ptr     m_regular_ctx;
extern blowfish_context_ptr     m_critical_ctx;
extern blowfish_context_ptr     m_external_ctx;

void crypto_blowfish_init();
u8 crypto_blowfish_encryptblock_full(blowfish_context_ptr *ctx, u8 *buf, u32 len);
u8 crypto_blowfish_decryptblock_full(blowfish_context_ptr *ctx, u8 *buf, u32 len);

// iOS app 0.9.6 and later uses Correct ExternalKey
#define crypto_blowfish_encryptblock_external_key(b,l)   \
    crypto_blowfish_encryptblock_full((void*)&m_external_ctx,b,l);
#define crypto_blowfish_decryptblock_external_key(b,l)   \
    crypto_blowfish_decryptblock_full((void*)&m_external_ctx,b,l);

#define crypto_blowfish_encryptblock_internal_key(b,l)   \
    crypto_blowfish_encryptblock_full((void*)&m_regular_ctx,b,l);
#define crypto_blowfish_decryptblock_internal_key(b,l)   \
    crypto_blowfish_decryptblock_full((void*)&m_regular_ctx,b,l);

#define crypto_blowfish_encryptblock_critical(b,l)   \
    crypto_blowfish_encryptblock_full((void*)&m_critical_ctx,b,l);
#define crypto_blowfish_decryptblock_critical(b,l)   \
    crypto_blowfish_decryptblock_full((void*)&m_critical_ctx,b,l);

#endif  //__CRYPTO_BLOWFISH_H
