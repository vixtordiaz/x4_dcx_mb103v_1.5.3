/**
  **************** (C) COPYRIGHT 2011 SCT Performance, LLC *********************
  * File Name          : cmdif_func_recoverystock.c
  * Device / System    : SCT products using the "CommonCode" platform
  * Author             : Quyen Leba
  *
  * Version            : 1 
  * Date               : 05/02/2011
  * Description        : 
  *                    : 
  *
  *
  * History            : 05/02/2011 M. Davis
  *                    :   Add SCT Copyright header,
  *
  ******************************************************************************
  */

#include <fs/genfs.h>
#include <common/statuscode.h>
#include <common/cmdif.h>
#include <common/obd2tune.h>
#include <common/obd2tune_recoverystock.h>
#include "cmdif_func_recoverystock.h"

extern cmdif_datainfo cmdif_datainfo_responsedata;  //from cmdif.c
extern u8 cmdif_scratchpad[512];                    //from cmdif.c
extern u16 cmdif_scratchpad_length;                 //from cmdif.c
extern flasher_info *flasherinfo;                   //from obd2tune.c

//------------------------------------------------------------------------------
// Initial check on .rsf
// Input:   u8  *recoverystock_header (1st 64 bytes of .rsf)
// Return:  u8  status
// Engineer: Quyen Leba
//------------------------------------------------------------------------------
u8 cmdif_func_recoverystock_check(u8 *recoverystock_header)
{
    u8  status;

    cmdif_datainfo_responsedata.responsecode = CMDIF_ACK_FAILED;
    status = obd2tune_recoverystock_check(recoverystock_header);
    switch(status)
    {
    case S_SUCCESS:
        cmdif_datainfo_responsedata.responsecode = CMDIF_ACK_OK;
        break;
    case S_NOTSUPPORT:
        cmdif_datainfo_responsedata.responsecode = CMDIF_ACK_UNSUPPORTED;
        break;
    case S_UNMATCH:
        cmdif_datainfo_responsedata.responsecode = CMDIF_ACK_SERIAL_UNMATCH;
        break;
    case S_INVALIDBINARY:
        cmdif_datainfo_responsedata.responsecode = CMDIF_ACK_INVALID_BINARYTYPE;
        break;
    case S_EXPIRED:
        cmdif_datainfo_responsedata.responsecode = CMDIF_ACK_EXPIRED;
        break;
    case S_BADCONTENT:
    case S_CRC32E:
        cmdif_datainfo_responsedata.responsecode = CMDIF_ACK_FAILED;
        break;
    default:
        cmdif_datainfo_responsedata.responsecode = CMDIF_ACK_FAILED;
        break;
    }
    return status;
}

//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
void _recoverystock_register_progress_status(u8 percentage)
{
    cmdif_internalresponse_ack(CMDIF_CMD_REGISTER_RECOVERYSTOCK,
                               CMDIF_ACK_PROGRESSBAR,&percentage,1);
}

//------------------------------------------------------------------------------
// Initial check on .rsf
// Inputs:  u8  *recoverystock_header (1st 64 bytes of .rsf)
//          u8  filename
// Return:  u8  status
// Engineer: Quyen Leba
//------------------------------------------------------------------------------
u8 cmdif_func_recoverystock_register(u8 *recoverystock_header,
                                     u8 *filename)
{
    u8  status;

    status = cmdif_func_recoverystock_check(recoverystock_header);
    if (cmdif_datainfo_responsedata.responsecode != CMDIF_ACK_OK)
    {
        return status;
    }

    // note... obd2tune_recoverystock_register() attempts to send progress-reports to the UI-app
    status = obd2tune_recoverystock_register(recoverystock_header,filename,
                                             _recoverystock_register_progress_status);
    
    switch(status)
    {
    case S_SUCCESS:
        cmdif_datainfo_responsedata.responsecode = CMDIF_ACK_DONE;
        break;
    case S_FILENOTFOUND:
        cmdif_datainfo_responsedata.responsecode = CMDIF_ACK_FILE_NOT_FOUND;
        break;
    case S_FAIL:
        cmdif_datainfo_responsedata.responsecode = CMDIF_ACK_FAILED;
        break;
    case S_UNMATCH:
    case S_CRC32E:
        cmdif_datainfo_responsedata.responsecode = CMDIF_ACK_FILE_NOT_MATCH;
        break;
    case S_NOTSUPPORT:
        cmdif_datainfo_responsedata.responsecode = CMDIF_ACK_UNSUPPORTED;
        break;
    case S_OPENFILE:
        cmdif_datainfo_responsedata.responsecode = CMDIF_ACK_FAILED;
        break;
    case S_WRITEFILE:
    case S_READFILE:
        cmdif_datainfo_responsedata.responsecode = CMDIF_ACK_FAILED;
        break;
    default:
        cmdif_datainfo_responsedata.responsecode = CMDIF_ACK_FAILED;
        break;
    }
    return status;
}
