/**
  **************** (C) COPYRIGHT 2011 SCT Performance, LLC *********************
  * File Name          : memcheck.h
  * Device / System    : SCT products using the "CommonCode" platform
  * Author             : Quyen Leba
  *
  * Version            : 1 
  * Date               : 05/02/2011
  * Description        : 
  *                    : 
  *
  *
  * History            : 05/02/2011 M. Davis
  *                    :   Add SCT Copyright header,
  *
  ******************************************************************************
  */

#ifndef __MEMCHECK_H
#define __MEMCHECK_H

#include <arch/gentype.h>

void memcheck_init();
void memcheck_test();
bool memcheck_isstackoverflow();
bool memcheck_isheapoverflow();

#endif  //__MEMCHECK_H
