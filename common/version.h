/**
  **************** (C) COPYRIGHT 2011 SCT Performance, LLC *********************
  * File Name          : version.h
  * Device / System    : SCT products using the "CommonCode" platform
  * Author             : Quyen Leba
  *
  * Version            : 1 
  * Date               : 05/02/2011
  * Description        : 
  *                    : 
  *
  *
  * History            : 05/02/2011 M. Davis
  *                    :   Add SCT Copyright header,
  *
  ******************************************************************************
  */

#ifndef __VERSION_H
#define __VERSION_H

#include <arch/gentype.h>

#define FAILSAFE_BOOTLOADER_OPERATING_MODE  0xE71A955B
#define MAIN_BOOTLOADER_OPERATING_MODE      0x36F585C2
#define APPLICATION_OPERATING_MODE          0x6318DF5A
#define MICO_CORE_VERSION                   1

typedef struct      //do not change struct size
{
    u32 signature;
    u32 version;
    u32 device_type;
    u32 market_type;
    u8  build;
    u8  reserved[15];
}FIRMWARE_TAG;

extern const FIRMWARE_TAG firmware_tag;

typedef struct
{
    u16 market;
    u16 hardware; 
    u16 major;
    u16 minor;
    u16 build;
}firmware_version;

u8 version_getfirmwareversion_string(u8 *fw_version);
u32 version_getfirmwareversion_integer();
u8 version_check();

#endif  //__VERSION_H
