/**
  **************** (C) COPYRIGHT 2011 SCT Performance, LLC *********************
  * File Name          : deviceinfo.c
  * Device / System    : SCT products using the "CommonCode" platform
  * Author             : Quyen Leba
  *
  * Version            : 1 
  * Date               : 05/02/2011
  * Description        : 
  *                    : 
  *
  *
  * History            : 05/02/2011 M. Davis
  *                    :   Add SCT Copyright header,
  *
  ******************************************************************************
  */

#include <string.h>
#include <ctype.h>
#include <stdio.h>
#include <device_config.h>
#include <device_version.h>
#include <board/genplatform.h>
#include <board/properties_vb.h>
#include <board/hardwarefeatures.h>
#include <common/statuscode.h>
#include <common/settings.h>
#include <common/obd2tune.h>
#include <common/version.h>
#include <common/cmdif.h>
#include <common/itoa.h>
#include <common/genmanuf_overload.h>
#include "deviceinfo.h"

//------------------------------------------------------------------------------
// Format a device info string for display
// Output:  u8  *info
// Return:  u8  status
//------------------------------------------------------------------------------
u8 deviceinfo_append_info(u8 *info, u32 maxinfolength, u32 *currentlength, 
                          u8 *headerstring, u8 *infostring)
{
    u32 newinfolength;
    
    if(*currentlength >= maxinfolength)
    {
        return S_FAIL;
    }
    
    newinfolength = strlen("<HEADER></HEADER>") + strlen((const char*)headerstring) + strlen((const char*)infostring);
    
    if((*currentlength + newinfolength) >  maxinfolength)
    {
        return S_FAIL;
    }
    
    strcat((char*)info, (const char*)"<HEADER>");
    strcat((char*)info, (const char*)headerstring);
    strcat((char*)info, (const char*)"</HEADER>");
    strcat((char*)info, (const char*)infostring);
    
    *currentlength += newinfolength;
    
    return S_SUCCESS;
}

//------------------------------------------------------------------------------
// Format a device info string for display
// Output:  u8  *info
// Return:  u8  status
//------------------------------------------------------------------------------
u8 deviceinfo_getinfostring(u8 *info, u32 maxinfolength)
{
    u8  fwversion[64];
    u8  errorlog[LOG_MAX_ERROR_POINT_COUNT*5];
    u8  genbuffer[128];
    u8  tempbuffer[16];
    u16 errorloglength;
    u32 bufferlength;
    float fval;
    u8  status;
    u32 infolength;
        
    memset(info, 0, maxinfolength);
    infolength = 0;
    
    //--------------------------------------------------------------------------
    // Hardware Device Name
    //--------------------------------------------------------------------------
    status = hardware_get_device_name(genbuffer,&bufferlength);
    if (status == S_SUCCESS)
    {
        deviceinfo_append_info(info, maxinfolength, &infolength, 
                               "Device Name:", genbuffer);
    }
    
    //--------------------------------------------------------------------------
    // Partnumber String
    //--------------------------------------------------------------------------
    settings_getsettings_byopcode(SETTINGS_OPCODE_DEVICE_PARTNUMBER_STRING,0,
                                  genbuffer,&bufferlength);
    strcat((char*)genbuffer,(char*)" ("VEHICLE_TYPE_STRING")");
    deviceinfo_append_info(info, maxinfolength, &infolength, 
                           "Device Part#:", genbuffer);
    
    //--------------------------------------------------------------------------
    // Serial Number
    //--------------------------------------------------------------------------
    settings_get_deviceserialnumber(genbuffer);
    deviceinfo_append_info(info, maxinfolength, &infolength, 
                           "Serial Number:", genbuffer);
    //--------------------------------------------------------------------------
    // Firmware Version
    //--------------------------------------------------------------------------    
    version_getfirmwareversion_string(genbuffer);    
    deviceinfo_append_info(info, maxinfolength, &infolength, 
                           "Firmware Version:", genbuffer);
    //--------------------------------------------------------------------------
    // Tune Type Support
    //-------------------------------------------------------------------------- 
    if (SETTINGS_IsNoPreloadedDevice())
    {
        strcpy((char*)genbuffer,"Not Supported");
    }
    else if (SETTINGS_IsPreloadedTuneDisabled())
    {
        strcpy((char*)genbuffer,"Disabled");
    }
    else
    {
        strcpy((char*)genbuffer,"Supported");
    }
    deviceinfo_append_info(info, maxinfolength, &infolength, 
                           "Preloaded Tuning:", genbuffer);

    if (SETTINGS_IsNoCustomDevice() || SETTINGS_Is50StateLegalDevice())
    {
        strcpy((char*)genbuffer,"Not Supported");
    }
    else
    {
        strcpy((char*)genbuffer,"Supported");
    }
    deviceinfo_append_info(info, maxinfolength, &infolength, 
                           "Custom Tuning:", genbuffer);
    
    
    //--------------------------------------------------------------------------
    // Hardware Specific Info Flags
    //-------------------------------------------------------------------------- 
//    status = hardware_get_hardware_specific_device_info(genbuffer,&bufferlength);
//    if (status == S_SUCCESS && bufferlength > 0)
//    {
//        strcat((char*)info,(char*)genbuffer);
//    }

    //--------------------------------------------------------------------------
    // Device Flags
    //-------------------------------------------------------------------------- 
    switch(SETTINGS_TUNE(flags) & PRELOADEDTUNE_RESTRICTION_MASK)
    {
    case PRELOADEDTUNE_RESTRICTION_ENABLED:
        strcpy((char*)genbuffer,"Preloaded Tune Support Disabled");
        deviceinfo_append_info(info, maxinfolength, &infolength, 
                           "Restrictions:", genbuffer);
        break;
    case PRELOADEDTUNE_RESTRICTION_ENABLED_WITH_REMOVAL:
        strcpy((char*)genbuffer,"Preloaded Tune Support Disabled (*)");
        deviceinfo_append_info(info, maxinfolength, &infolength, 
                           "Restrictions:", genbuffer);
        break;
    default:
        //nothing
        break;
    }
    
    //--------------------------------------------------------------------------
    // Tune Revision
    //--------------------------------------------------------------------------
    obd2tune_gettunerevision(genbuffer);    
    deviceinfo_append_info(info, maxinfolength, &infolength, 
                           "Tune Revision:", genbuffer);

    //--------------------------------------------------------------------------
    // Market Type
    //-------------------------------------------------------------------------- 
    deviceinfo_getmarketstring((MarketType)SETTINGS_CRITICAL(markettype), genbuffer, sizeof(genbuffer));   
    deviceinfo_append_info(info, maxinfolength, &infolength, 
                           "Market:", genbuffer);
    
    //--------------------------------------------------------------------------
    // Married Count
    //-------------------------------------------------------------------------- 
    if (SETTINGS_TUNE(married_count) > MARRIED_COUNT_MAX)
    {
        strcpy((char*)genbuffer,"0");
    }
    else
    {
        memset(genbuffer, 0, sizeof(genbuffer));
        itoa((MARRIED_COUNT_MAX+1)-SETTINGS_TUNE(married_count),genbuffer,10);
    }
    deviceinfo_append_info(info, maxinfolength, &infolength, 
                           "Unlocks Left:", genbuffer);

    //--------------------------------------------------------------------------
    // Married Status
    //-------------------------------------------------------------------------- 
    if (SETTINGS_IsMarried())
    {
        strcpy((char*)genbuffer,(char*)"Married");
        if (SETTINGS_IsDownloadFail())
        {
            strcpy((char*)genbuffer,(char*)" (Program Error Detected)");
        }
    }
    else
    {
        strcpy((char*)genbuffer,(char*)"Unmarried");
    }
    deviceinfo_append_info(info, maxinfolength, &infolength, 
                           "Married Status:", genbuffer);
    
    //--------------------------------------------------------------------------
    // Current Tune Info
    //-------------------------------------------------------------------------- 
    if (SETTINGS_IsMarried())
    {
        if (SETTINGS_TUNE(tuneinfotrack.flashtype) == CMDIF_ACT_PRELOADED_FLASHER)
        {
            strcpy((char*)genbuffer,(char*)"Preloaded Tune - ");
        }
        else if (SETTINGS_TUNE(tuneinfotrack.flashtype) == CMDIF_ACT_CUSTOM_FLASHER)
        {
            strcpy((char*)genbuffer,(char*)"Custom Tune - ");
        }
        else if (SETTINGS_TUNE(tuneinfotrack.flashtype) == CMDIF_ACT_STOCK_FLASHER)
        {
            strcpy((char*)genbuffer,(char*)"Stock Tune - ");
        }
        else
        {
            strcpy((char*)genbuffer,(char*)"Invalid - ");
        }
        
        memset(tempbuffer,0,sizeof(tempbuffer));
        itoa(SETTINGS_TUNE(veh_type),tempbuffer,10);
        strcat((char*)genbuffer,(char*)tempbuffer);
        strcat((char*)genbuffer,(char*)" - ");
        
        if ((strlen((char*)SETTINGS_TUNE(vehiclecodes)) > 
             (sizeof(SETTINGS_TUNE(vehiclecodes))-1)) ||
            (isgraph(SETTINGS_TUNE(vehiclecodes)[0]) == 0))
        {
            strcat((char*)genbuffer,(char*)"N/A");
        }
        else
        {
            strcat((char*)genbuffer,(char*)SETTINGS_TUNE(vehiclecodes));
        }
        
        deviceinfo_append_info(info, maxinfolength, &infolength, 
                           "Current Flash:", genbuffer);
    }
    
    //--------------------------------------------------------------------------
    // Last Flashed Tune
    //-------------------------------------------------------------------------- 
    if (SETTINGS_IsMarried())
    {   
        memset((char*)genbuffer,0,sizeof(genbuffer));

        for(int i = 0; i < ECM_MAX_COUNT; i++)
        {
            if (strlen((char*)SETTINGS_TUNE(tuneinfotrack.tunedescriptions[i])) > 0)
            {
                if (i > 0 && strlen((char*)genbuffer) > 0)
                {
                    strcat((char*)genbuffer,(char*)" - ");
                }
                strcat((char*)genbuffer,(char*)SETTINGS_TUNE(tuneinfotrack.tunedescriptions[i]));
            }            
        }  
            
        if (strlen((char*)genbuffer) == 0)
        {
            strcpy((char*)genbuffer,(char*)"N/A");
        }
        
        deviceinfo_append_info(info, maxinfolength, &infolength, 
                           "Current Tune:", genbuffer);
    }
    
    //--------------------------------------------------------------------------
    // Total Flashes
    //-------------------------------------------------------------------------- 
    itoa(SETTINGS_TUNE(tunehistoryflashcount),tempbuffer,10);
    strcpy((char*)genbuffer,(char*)tempbuffer);
    deviceinfo_append_info(info, maxinfolength, &infolength, 
                           "Total Flashes:", genbuffer);
        
    //--------------------------------------------------------------------------
    // Support Info
    //-------------------------------------------------------------------------- 
    log_get_error_point_list_string(ErrorPointPoolSource_Application,
                                    errorlog,&errorloglength);
    if (errorloglength != 0)
    {
        strcpy((char*)genbuffer,(char*)errorlog);
    }
    else
    {
        strcpy((char*)genbuffer,"N/A");
    }
    deviceinfo_append_info(info, maxinfolength, &infolength, 
                           "Support Info:", genbuffer);

    //--------------------------------------------------------------------------
    // Bootloader Error Points
    //-------------------------------------------------------------------------- 
//    log_get_error_point_list_string(ErrorPointPoolSource_Boot,
//                                    errorlog,&errorloglength);
//    strcat((char*)info,"<HEADER>Boot Debug:</HEADER>");
//    if (errorloglength != 0)
//    {
//        strcat((char*)info,(char*)errorlog);
//    }
//    else
//    {
//        strcat((char*)info,"N/A");
//    }

    //--------------------------------------------------------------------------
    // VBatt
    //-------------------------------------------------------------------------- 
    adc_init(ADC_FLASH);
    status = adc_read(ADC_VBAT,&fval);
    if (status == S_SUCCESS)
    {
        sprintf((char*)genbuffer,"%.1f",fval);
        //TODOQK: ftoa(fval,1,genbuffer);
    }
    else
    {
        strcpy((char*)genbuffer,"ADC error");
    }    
    deviceinfo_append_info(info, maxinfolength, &infolength, 
                           "VBatt:", genbuffer);
    
    //--------------------------------------------------------------------------
    // VB Board FW App Version
    //-------------------------------------------------------------------------- 
    status = deviceinfo_getVBfirmwareversion_string(fwversion);
    if(status == S_SUCCESS)
    {
        strcpy((char*)genbuffer,(char*)fwversion);
        deviceinfo_append_info(info, maxinfolength, &infolength, 
                           "VB Version:", genbuffer);
    }

    //--------------------------------------------------------------------------
    // App Compiled Timestamp
    //-------------------------------------------------------------------------- 
    if (APPLICATION_VERSION_BUILD)
    {
        strcpy((char*)genbuffer,(char*)__DATE__);
        strcat((char*)genbuffer,(char*)", ");
        strcat((char*)genbuffer,(char*)__TIME__);
        deviceinfo_append_info(info, maxinfolength, &infolength, 
                               "Compiled:", genbuffer);
    }

#if USE_WIFI    
    //--------------------------------------------------------------------------
    // Wifi Patch Version
    //--------------------------------------------------------------------------
    if (hardwarefeatures_has_wifi())
    {
        wifi_hal_getwifiversion_string(genbuffer);
        deviceinfo_append_info(info, maxinfolength, &infolength,
                               "Wifi Version:", genbuffer);
    }
    
#endif
    
    return S_SUCCESS;
}

//------------------------------------------------------------------------------
// Get firmware version in string format of VB
// Output:  u8  *fw_version (must be able to store 16+1 bytes)
// Return:  u8  status
// Engineer: Patrick Downs, Quyen Leba
//------------------------------------------------------------------------------
u8 deviceinfo_getVBfirmwareversion_string(u8 *fw_version_str)
{
    PROPERTIES_VB_INFO properties_vb_info;
    u32 properties_vb_info_length;
    u8  buffer[64];
    u32 major;
    u32 minor;
    u8  status;

    // Get the vehicle board properties and check the fw version
    status = properties_vb_getinfo((u8*)&properties_vb_info, &properties_vb_info_length);
    if(status != S_SUCCESS)
    {
        // Can't get vehicle board info
//        log_push_error_point(TODO);
        return S_FAIL;
    }

    major = (properties_vb_info.app_version/1000);
    minor = properties_vb_info.app_version - (major*1000);
    fw_version_str[0] = 0;
    itoa(major,buffer,10);
    strcat((char*)fw_version_str,(char*)buffer);
    itoa(minor,buffer,10);
    strcat((char*)fw_version_str,".");
    strcat((char*)fw_version_str,(char*)buffer);
    if (properties_vb_info.app_build)
    {
        itoa(properties_vb_info.app_build,buffer,10);
        strcat((char*)fw_version_str," build ");
        strcat((char*)fw_version_str,(char*)buffer);
    }

    return S_SUCCESS;
}

//------------------------------------------------------------------------------
// Get firmware version in string format of AB
// Output:  u8  *fw_version (must be able to store 16+1 bytes)
// Return:  u8  status
// Engineer: Patrick Downs
//------------------------------------------------------------------------------
u8 deviceinfo_getABfirmwareversion_string(u8 *fw_version_str)
{
    PROPERTIES_AB_INFO properties_ab_info;
    u32 properties_ab_info_length;
    u8  buffer[64];
    u32 major;
    u32 minor;
    u8  status;

    // Get the app board properties and check the fw version
    status = properties_ab_getinfo((u8*)&properties_ab_info, &properties_ab_info_length);
    if(status != S_SUCCESS)
    {
        // Can't get vehicle board info
//        log_push_error_point(TODO);
        return S_FAIL;
    }

    major = (properties_ab_info.app_version/1000);
    minor = properties_ab_info.app_version - (major*1000);
    fw_version_str[0] = 0;
    itoa(major,buffer,10);
    strcat((char*)fw_version_str,(char*)buffer);
    itoa(minor,buffer,10);
    strcat((char*)fw_version_str,".");
    strcat((char*)fw_version_str,(char*)buffer);
    if (properties_ab_info.app_build)
    {
        itoa(properties_ab_info.app_build,buffer,10);
        strcat((char*)fw_version_str," build ");
        strcat((char*)fw_version_str,(char*)buffer);
    }

    return S_SUCCESS;
}

u8 deviceinfo_getmarketstring(MarketType type, u8* retbuf, u32 maxlen)
{
    memset(retbuf, 0,  maxlen);
    
    switch(type)
    {
    case MarketType_US:
        strncpy((char*)retbuf, US_MARKET_TYPE_STRING,maxlen);
        break;
    case MarketType_AU:
        strncpy((char*)retbuf, AU_MARKET_TYPE_STRING,maxlen);
        break;
    case MarketType_UK:
        strncpy((char*)retbuf, UK_MARKET_TYPE_STRING,maxlen);
        break;
    case MarketType_EO:
        strncpy((char*)retbuf, EO_MARKET_TYPE_STRING,maxlen);
        break;
    case MarketType_CN:
        strncpy((char*)retbuf, CN_MARKET_TYPE_STRING,maxlen);
        break;
    case MarketType_Unknown:
    case MarketType_Any:
        strncpy((char*)retbuf, UNKNOWN_MARKET_TYPE_STRING,maxlen);
        break;
    default:
        return S_FAIL;
    }
    
    return S_SUCCESS;
}
