/**
  **************** (C) COPYRIGHT 2011 SCT Performance, LLC *********************
  * File Name          : obd2tune_upload.c
  * Device / System    : SCT products using the "CommonCode" platform
  * Author             : Quyen Leba
  *
  * Version            : 1 
  * Date               : 05/02/2011
  * Description        : 
  *                    : 
  *
  *
  * History            : 05/02/2011 M. Davis
  *                    :   Add SCT Copyright header,
  *
  ******************************************************************************
  */

//------------------------------------------------------------------------------
// Log error point: 0x40xx -> 0x41xx
//------------------------------------------------------------------------------

#include <string.h>
#include <fs/genfs.h>
#include <board/delays.h>
#include <board/power.h>
#include <board/indicator.h>
#include <board/rtc.h>
#include <common/statuscode.h>
#include <common/settings.h>
#include <common/obd2tune.h>
#include <common/genmanuf_overload.h>
#include <common/cmdif.h>
#include <common/filestock.h>
#include <common/obd2comm.h>
#include <common/log.h>
#include <common/housekeeping.h>
#include <common/debug/debug_output.h>
#include <vm_engine/ccvm/source/ccvm.h>
#include "obd2tune_upload.h"

#define USE_DUMMY_UPLOAD            0

extern flasher_info *flasherinfo;                   //from obd2tune.c
extern cmdif_datainfo cmdif_datainfo_responsedata;  //from cmdif.c
#define TUNE_UPLOADING_STATUS(var)  flasherinfo->tune_programming_status.##var

//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
u8 upload_ecm_memoryblock(u16 ecm_type, u8 block_index,
                          BlockType block_type);
u8 upload_ecm(u16 ecm_type, VehicleCommType vehiclecommtype,
              VehicleCommLevel vehiclecommlevel, bool isAllUnlocked);

//------------------------------------------------------------------------------
// Upload a memory block from an ecm
// Inputs:  u16 ecm_type
//          u8  block_index
//          BlockType block_type
// Return:  u8  status
// Engineer: Quyen Leba
//------------------------------------------------------------------------------
u8 upload_ecm_memoryblock(u16 ecm_type, u8 block_index,
                          BlockType block_type)
{
    VehicleCommType vehiclecommtype;
    VehicleCommLevel vehiclecommlevel;
    MemoryAddressType address_type;
    RequestLengthType request_type;
    u32 memblock_address;
    u32 memblock_length;
    u32 current_address;
    u32 totalbyte_uploaded;
    u32 bytetoupload;
    u8 *readbuffer;
    u32 readbufferindex;
    u32 readbufferthreshold;
    u32 bytecount_sincetesterpresent;
    u32 testerpresentthreshold;
    u16 readlength;
    u32 percentage;
    u16 memblock_flagtype;
    u32 memblock_flagdata;
    u8  block_tracking;
    u16 maxuploadblocklength;
    bool is_setup_done;
    u8 status;
    u32 progress_timeout;

    readbuffer = 0;
    progress_timeout = 0;
    is_setup_done = FALSE;
    vehiclecommtype = ECM_GetCommType(ecm_type);
    vehiclecommlevel = ECM_GetCommLevel(ecm_type);
    testerpresentthreshold = ECM_GetTesterPresentFrequency(ecm_type) *
        ECM_GetUploadBlockSize(ecm_type);
    
    //TODOQ: make this block handled specifically by vehicle manuf
    switch(block_type)
    {
    case OS_Block_Read:
        memblock_address = ECM_GetOSReadBlockAddr(ecm_type,block_index);
        memblock_length = ECM_GetOSReadBlockSize(ecm_type,block_index);
        memblock_flagtype = ECM_GetOSReadBlockFlagType(ecm_type,block_index);
        memblock_flagdata = ECM_GetOSReadBlockFlagData(ecm_type,block_index);
        break;
    case OS_Block:
        //Note: this case should not be used for upload
        memblock_address = ECM_GetOSBlockAddr(ecm_type,block_index);
        memblock_length = ECM_GetOSBlockSize(ecm_type,block_index);
        memblock_flagtype = ECM_GetOSBlockFlagType(ecm_type,block_index);
        memblock_flagdata = ECM_GetOSBlockFlagData(ecm_type,block_index);
        break;
    case Cal_Block:
        memblock_address = ECM_GetCalBlockAddr(ecm_type,block_index);
        memblock_length = ECM_GetCalBlockSize(ecm_type,block_index);
        memblock_flagtype = ECM_GetCalBlockFlagType(ecm_type,block_index);
        memblock_flagdata = ECM_GetCalBlockFlagData(ecm_type,block_index);
        break;
    default:
        log_push_error_point(0x4000);
        return S_INPUT;
    }
    
    if (memblock_length == 0)
    {
        // termination block
        status = S_SUCCESS;
        goto upload_ecm_memoryblock_done;
    }
    
    if (memblock_flagtype == MEMBLOCK_FLAGS_ADDRESS_OFFSET_BY_FDATA)
    {
        memblock_address += memblock_flagdata;
    }
    
    if (ECM_IsUploadAddr4B(ecm_type))
    {
        address_type = Address_32_Bits;     //TODOQ: simplify this
        request_type = Request_4_Byte;
    }
    else
    {
        address_type = Address_24_Bits;
        request_type = Request_3_Byte;
    }
    
    current_address = memblock_address;
    totalbyte_uploaded = 0;
    bytecount_sincetesterpresent = 0;
    
    readbufferindex = 0;
    readbufferthreshold = 
        ((MAX_UPLOAD_BUFFER_LENGTH/ECM_GetUploadBlockSize(ecm_type)) * 
         ECM_GetUploadBlockSize(ecm_type));
    if (readbufferthreshold == 0)
    {
        readbufferthreshold = ECM_GetUploadBlockSize(ecm_type);
    }
    
    readbuffer = __malloc(MAX_UPLOAD_BUFFER_LENGTH);
    if (readbuffer == NULL)
    {
        log_push_error_point(0x4009);
        status = S_MALLOC;
        goto upload_ecm_memoryblock_done;
    }
    
    block_tracking = 1;
    while(totalbyte_uploaded < memblock_length)
    {
        bytetoupload = memblock_length - totalbyte_uploaded;
        if (bytetoupload > ECM_GetUploadBlockSize(ecm_type))
        {
            bytetoupload = ECM_GetUploadBlockSize(ecm_type);
        }

        if ((bytetoupload > MAX_UPLOAD_BUFFER_LENGTH) ||
            (readbufferindex + bytetoupload > readbufferthreshold))
        {
            log_push_error_point(0x4001);
            status = S_BADCONTENT;
            goto upload_ecm_memoryblock_done;
        }
        
        //TODOQ: don't handle by comm type here, move it out
        
        //###############################
        // fill with 'S', not real upload
        //###############################
        if (memblock_flagtype == MEMBLOCK_FLAGS_PRE_FILL_WITH_S)
        {
            memset((char*)&readbuffer[readbufferindex],'S',bytetoupload);
            readlength = bytetoupload;
            status = S_SUCCESS;
        }
        else if (memblock_flagtype == MEMBLOCK_FLAGS_PRE_FILL_WITH_FF)
        {
            memset((char*)&readbuffer[readbufferindex],0xFF,bytetoupload);
            readlength = bytetoupload;
            status = S_SUCCESS;
        }
//        else if (upload_check_skipblock(skipblockinfo,
//                                        current_address, bytetoupload) == S_SUCCESS)
//        {            
//            //TODOQ: currently didn't use skipblock flags, just fill skipblocks
//            //with 0xFF
//            memset((char*)&readbuffer[readbufferindex],0xFF,bytetoupload);
//            readlength = bytetoupload;
//            goto upload_ecm_memoryblock_done;           
//        }
        //###############################
        // get data from ECM by CAN
        //###############################
        else if (vehiclecommtype == CommType_CAN)
        {
            if (ECM_IsUploadByCmd20ExtendedId(ecm_type))
            {
                if (SETTINGS_IsDemoMode())
                {
                    delays(demo_control.upload_dummy_delay,'m');
                    status = S_SUCCESS;
                }
                else
                {
                    status = obd2can_read_memory_address_by20extendedid
                        (0,0,   //TODOQ: correct these IDs
                         current_address,bytetoupload,address_type,
                         &readbuffer[readbufferindex],&readlength);
                }
            }
            else if (ECM_IsUploadByCmd23(ecm_type))
            {
                if (SETTINGS_IsDemoMode())
                {
                    delays(demo_control.upload_dummy_delay,'m');
                    status = S_SUCCESS;
                }
                else
                {
                    status = obd2can_read_memory_address
                        (ECM_GetEcmId(ecm_type), current_address, bytetoupload,
                         address_type, UPLOAD_CMD_MEMORY_SIZE,
                         &readbuffer[readbufferindex],&readlength,FALSE, vehiclecommlevel);
                }
            }
            else if (ECM_IsUploadByCmd35Single36(ecm_type) ||
                     ECM_IsUploadByCmd35Multi36(ecm_type))
            {
                if (SETTINGS_IsDemoMode())
                {
                    delays(demo_control.upload_dummy_delay,'m');
                    status = S_SUCCESS;
                }
                else
                {
                    if (is_setup_done == FALSE)
                    {
                        if (ECM_IsUploadByCmd35Single36(ecm_type))
                        {
                            status = obd2can_request_upload
                                (ECM_GetEcmId(ecm_type),
                                 current_address, bytetoupload,
                                 request_type,vehiclecommlevel,
                                 &maxuploadblocklength);
                        }
                        else    //if (ECM_IsUploadByCmd35Multi36(ecm_type))
                        {
                            status = obd2can_request_upload
                                (ECM_GetEcmId(ecm_type),
                                 memblock_address,memblock_length,
                                 request_type,vehiclecommlevel,
                                 &maxuploadblocklength);
                            is_setup_done = TRUE;
                        }
                        
                        if (status != S_SUCCESS)
                        {
                            log_push_error_point(0x4002);
                            goto upload_ecm_memoryblock_done;
                        }
                        if (maxuploadblocklength != INVALID_MEMBLOCK_SIZE &&
                            maxuploadblocklength < ECM_GetUploadBlockSize(ecm_type))
                        {
                            
                            log_push_error_point(0x4003);
                            status = S_BADCONTENT;
                            goto upload_ecm_memoryblock_done;
                        }
                    }//if (is_setup_done == FALSE)...
                    
                    status = obd2can_transfer_data_upload
                        (ECM_GetEcmId(ecm_type),block_tracking++,
                         vehiclecommlevel,current_address,request_type,
                         &readbuffer[readbufferindex],&readlength);  
                    if (status != S_SUCCESS)
                    {
                        log_push_error_point(0x4004);
                        goto upload_ecm_memoryblock_done;
                    }
                    
                    //###############################################
                    // Send $37 if required, and all data of this
                    // block is sent
                    //###############################################
                    if ((totalbyte_uploaded + bytetoupload) >= memblock_length)
                    {
                        is_setup_done = FALSE;
                        if (ECM_IsUploadRequireCmd37(ecm_type))
                        {
                            status = obd2can_request_transfer_exit
                                (ECM_GetEcmId(ecm_type),vehiclecommlevel);
                            if (status != S_SUCCESS)
                            {
                                log_push_error_point(0x4005);
                                goto upload_ecm_memoryblock_done;
                            }
                        }//if (ECM_IsUploadRequireCmd37...
                    }//if ((totalbyte_uploaded + ...
                }
            }
            else
            {
                status = S_BADCONTENT;
            }
        }//else if (vehiclecommtype == CommType_CAN)...
        //###############################
        // get data from ECM by VPW
        //###############################
        else if (vehiclecommtype == CommType_VPW)
        {
            if (SETTINGS_IsDemoMode())
            {
                delays(demo_control.upload_dummy_delay,'m');
                status = S_SUCCESS;
            }
            else
            {
                if (ECM_IsUploadByCmd35Single36(ecm_type))
                {
                    u8  retry = 3;
                    
                    do
                    {
                        delays(10,'m');
                        status = obd2vpw_helper_upload_block(current_address,
                                                             bytetoupload,
                                                             &readbuffer[readbufferindex],
                                                             &readlength);
                    }while(--retry > 0 && status != S_SUCCESS &&
                           readlength != bytetoupload);
                    
                    if(status != S_SUCCESS)
                    {
                        // fail
                        log_push_error_point(0x4035);
                        
                    }
                    //                u32 feedbackaddress;
                    //                status = obd2vpw_request_upload(current_address,bytetoupload);
                    //                if (status == S_SUCCESS)
                    //                {
                    //                    status = obd2vpw_transfer_data_upload
                    //                        (&readbuffer[readbufferindex],
                    //                         &feedbackaddress,&readlength);
                    //                }
                }
                else
                {
                    // so far (Feb052010), all VPW ECMs we flash use $35 to upload
                    // this case is $23, and probably never need to implemented
                    log_push_error_point(0x4006);
                    status = S_FAIL;
                }
            }
        }//else if (vehiclecommtype == CommType_VPW)...
        //###############################
        // get data from ECM by SCP
        //###############################
        else if (vehiclecommtype == CommType_SCP || vehiclecommtype == CommType_SCP32)
        {
            MemoryAddressType addrtype;
            
            if (vehiclecommtype == CommType_SCP32)
            {
                addrtype = Address_32_Bits;
            }
            else    //CommType_SCP
            {
                addrtype = Address_24_Bits;
            }
            
            if (SETTINGS_IsDemoMode())
            {
                delays(demo_control.upload_dummy_delay,'m');
                status = S_SUCCESS;
            }
            else
            {
                if (ECM_IsUploadByCmd35Single36(ecm_type))
                {
                    status = obd2scp_readblock_bycmd35single36single37
                        (current_address,addrtype,bytetoupload,
                         &readbuffer[readbufferindex]);
                }
                else if (ECM_IsUploadByCmd23(ecm_type))
                {
                    u8  retry;
                    
                    retry = 2;
                scp_upload_block_by23_retry:
                    status = obd2scp_read_memory_address
                        (current_address,bytetoupload,addrtype,
                         &readbuffer[readbufferindex],&readlength,TRUE);
                    if (status != S_SUCCESS)
                    {
                        log_push_error_point(0x402D);
                        if (retry--)
                        {
                            goto scp_upload_block_by23_retry;
                        }
                    }
                }
                else
                {
                    //invalid upload method
                    log_push_error_point(0x4007);
                    status = S_FAIL;
                }
            }
        }//else if (vehiclecommtype == CommType_SCP || vehiclecommtype == CommType_SCP32)...
        //###############################
        // get data from ECM by ???
        //###############################
        else
        {
            log_push_error_point(0x4008);
            status = S_COMMTYPE;
        }
        
        // Added time out for progress report, if 10s elapsed send progress to keep app from timing out
        if (status == S_SUCCESS)
        {
            //successfully collected a data chunk
            current_address += bytetoupload;
            totalbyte_uploaded += bytetoupload;
            flasherinfo->tunebytecount += bytetoupload;
            TUNE_UPLOADING_STATUS(currentbytecount) += bytetoupload;
            percentage = TUNE_UPLOADING_STATUS(currentbytecount) * 100 / 
                TUNE_UPLOADING_STATUS(totalbytecount);
            if ((percentage == 0) ||
                (percentage != TUNE_UPLOADING_STATUS(percentage)) || (progress_timeout < rtc_getvalue()))
            {
                progress_timeout = rtc_getvalue() + 10000;
                TUNE_UPLOADING_STATUS(percentage) = percentage;
                
                cmdif_internalresponse_ack_nowait
                    (CMDIF_CMD_DO_UPLOAD, CMDIF_ACK_PROGRESSBAR,
                     &TUNE_UPLOADING_STATUS(percentage),1);
                
                debug_reportprogress(TUNE_UPLOADING_STATUS(percentage), "Uploading");
            }

            readbufferindex += bytetoupload;
            bytecount_sincetesterpresent += bytetoupload;
            if (readbufferindex >= readbufferthreshold)
            {
                //readbuffer is fulled, flush to stock file
                status = filestock_write(readbuffer,readbufferindex);
                readbufferindex = 0;
            }
        }
        else
        {
            goto upload_ecm_memoryblock_done;
        }
        if ((ECM_GetTesterPresentFrequency(ecm_type) != 0) &&
            (bytecount_sincetesterpresent > testerpresentthreshold))
        {
            //this tester present is only helpful in case of fake uploading
            //such as fill with 'S'
            bytecount_sincetesterpresent = 0;
            obd2tune_testerpresent();
        }
    }//while(totalbyte_uploaded < memblock_length)...
    if (readbufferindex > 0)
    {
        //readbufferindex < readbufferthreshold, so the last block was not
        //flushed to stock file
        status = filestock_write(readbuffer,readbufferindex);
    }
    
    //##########################################################################
    //special memblock handle
    //##########################################################################
    if (status == S_SUCCESS)
    {
        u32 appendblocklength;
        u32 appendbytecount;
        bool hasSpecialMemblock;

        hasSpecialMemblock = FALSE;
        appendbytecount = 0;
        if (memblock_flagtype == MEMBLOCK_FLAGS_SIMPLE_FF_APPEND)
        {
            memset(readbuffer,0xFF,MAX_UPLOAD_BUFFER_LENGTH);
            hasSpecialMemblock = TRUE;
        }
        else if (memblock_flagtype == MEMBLOCK_FLAGS_SIMPLE_00_APPEND)
        {
            memset(readbuffer,0x00,MAX_UPLOAD_BUFFER_LENGTH);
            hasSpecialMemblock = TRUE;
        }

        if (hasSpecialMemblock)
        {
            while(appendbytecount < memblock_flagdata)
            {
                appendblocklength = memblock_flagdata - appendbytecount;
                if (appendblocklength > MAX_UPLOAD_BUFFER_LENGTH)
                {
                    appendblocklength = MAX_UPLOAD_BUFFER_LENGTH;
                }
                status = filestock_write(readbuffer,appendblocklength);
                if (status != S_SUCCESS)
                {
                    break;
                }
                appendbytecount += appendblocklength;
                flasherinfo->tunebytecount += appendblocklength;
                TUNE_UPLOADING_STATUS(currentbytecount) += appendblocklength;
            }
        }
    }

upload_ecm_memoryblock_done:
    
    if(readbuffer)
    {
        __free(readbuffer);
    }
    
    return status;
}

//------------------------------------------------------------------------------
// Check for the availablitly of prelaoded stock file
// Inputs:  u16 ecm_type
//          GenerateStockSource stocksource
// Return:  u8  status
// Engineer: Patrick Downs
//------------------------------------------------------------------------------
u8 obd2tune_check_ecm_stock(u16 ecm_type, GenerateStockSource stocksource, u8 index)
{
    F_FILE *fptr;   
    u8  stockpartfilename[256];
    u8  status;
    fptr = NULL;
    status = S_FAIL;   
    
    // Check for partnumber first to open a stock file
    if (flasherinfo->ecminfo.second_codes[index][0][0] != NULL)
    {
        strcpy((char*)stockpartfilename,
               (char*)flasherinfo->ecminfo.second_codes[index][0]);
        strcat((char*)stockpartfilename,".spf");        
    }
    else if(flasherinfo->ecminfo.codes[index][0][0] != NULL) // Then strategy
    {
        strcpy((char*)stockpartfilename,
               (char*)flasherinfo->ecminfo.codes[index][0]);
        strcat((char*)stockpartfilename,".spf");        
    }
    else
    {
        return S_INPUT; // No valid strategy or partnumber
    }

    switch(stocksource)
    {
    case GenerateStockSource_Use_Only_DEFAULT_Folder:
        fptr = genfs_default_openfile(stockpartfilename,"rb");
        if (fptr == NULL)
        {
            log_push_error_point(0x4039);
            status = S_FILENOTFOUND;
        }
        break;
    case GenerateStockSource_Use_Only_USER_Folder:
        fptr = genfs_user_openfile(stockpartfilename,"rb");
        if (fptr == NULL)
        {
            log_push_error_point(0x4034);
            status = S_FILENOTFOUND;
        }
        break;
    case GenerateStockSource_Use_DEFAULT_Folder_First:
        fptr = genfs_default_openfile(stockpartfilename,"rb");
        if (fptr == NULL)
        {
            fptr = genfs_user_openfile(stockpartfilename,"rb");
            if (fptr == NULL)
            {
                log_push_error_point(0x4033);
                status = S_FILENOTFOUND;
            }
        }
        break;
    case GenerateStockSource_Use_USER_Folder_First:
        fptr = genfs_user_openfile(stockpartfilename,"rb");
        if (fptr == NULL)
        {
            fptr = genfs_default_openfile(stockpartfilename,"rb");
            if (fptr == NULL)
            {
                log_push_error_point(0x403A);
                status = S_FILENOTFOUND;
            }
        }
        break;
    default:
        log_push_error_point(0x403B);
        status = S_NOTSUPPORT;
        break;
    }

    if (fptr)
    {
        status = S_SUCCESS;
    }
    genfs_closefile(fptr);

    return status;
}

//------------------------------------------------------------------------------
// Try to generate stock for this ecm if possible; to allow bypass upload
// Inputs:  u16 ecm_type
//          GenerateStockSource stocksource
// Return:  u8  status
// Engineer: Quyen Leba
//------------------------------------------------------------------------------
#define GENERATE_ECM_STOCK_COPY_SIZE   4096
u8 obd2tune_generate_ecm_stock(u16 ecm_type, GenerateStockSource stocksource)
{
    F_FILE *fptr;
    F_FILE *fstock;
    u32 expected_upload_size;
    u32 actual_upload_size;
    u32 currentstockposition;
    u32 currentbytecount;
    u8  stockpartfilename[64];
    u8  index;
    u32 percentage;
    u8  status;
    u32 paddingsize; 
    u32 temp_expected_upload_size; 
    
    u8  *buffer;
    u32 bufferlength;
    
    currentbytecount = TUNE_UPLOADING_STATUS(currentbytecount);
    index = TUNE_UPLOADING_STATUS(ecm_index);
    fptr = NULL;
    fstock = NULL;
    status = S_NOTSUPPORT;
    paddingsize = 0; 
    temp_expected_upload_size = 0; 

    // Check for partnumber first to open a stock file
    if (flasherinfo->ecminfo.second_codes[index][0][0] != NULL)
    {
        strcpy((char*)stockpartfilename,
               (char*)flasherinfo->ecminfo.second_codes[index][0]);
        strcat((char*)stockpartfilename,".spf");
    }
    else if(flasherinfo->ecminfo.codes[index][0][0] != NULL) // Then strategy
    {
        strcpy((char*)stockpartfilename,
               (char*)flasherinfo->ecminfo.codes[index][0]);
        strcat((char*)stockpartfilename,".spf");        
    }
    else
    {
        return status; // Not found
    }

    status = S_SUCCESS;     //be careful w/ this assignment
    switch(stocksource)
    {
    case GenerateStockSource_Use_Only_DEFAULT_Folder:
        fptr = genfs_default_openfile(stockpartfilename,"rb");
        if (fptr == NULL)
        {
            log_push_error_point(0x4036);
            status = S_OPENFILE;    // File not found
        }
        break;
    case GenerateStockSource_Use_Only_USER_Folder:
        fptr = genfs_user_openfile(stockpartfilename,"rb");
        if (fptr == NULL)
        {
            log_push_error_point(0x4032);
            status = S_OPENFILE;    // File not found
        }
        break;
    case GenerateStockSource_Use_DEFAULT_Folder_First:
        fptr = genfs_default_openfile(stockpartfilename,"rb");
        if (fptr == NULL)
        {
            fptr = genfs_user_openfile(stockpartfilename,"rb");
            if (fptr == NULL)
            {
                log_push_error_point(0x400B);
                status = S_OPENFILE;    // File not found
            }
        }
        break;
    case GenerateStockSource_Use_USER_Folder_First:
        fptr = genfs_user_openfile(stockpartfilename,"rb");
        if (fptr == NULL)
        {
            fptr = genfs_default_openfile(stockpartfilename,"rb");
            if (fptr == NULL)
            {
                log_push_error_point(0x4037);
                status = S_OPENFILE;    // File not found
            }
        }
        break;
    default:
        log_push_error_point(0x4038);
        status = S_NOTSUPPORT;
        break;
    }
    
    if (status != S_SUCCESS)
    {
        return status;
    }

    status = obd2tune_getecmsize_byecmtype(ecm_type,
                                           &expected_upload_size);
    if (status != S_SUCCESS)
    {
        log_push_error_point(0x400A);
        return status;
    }
    actual_upload_size = 0;
    
    filestock_get_position(&currentstockposition); // Save current position of stock.bin
    filestock_closestockfile();
    
    // Open stock.bin using regular filesystem functions, the preloaded stock
    // file opened above should have already been encrypted using stock.bin TEA encryption
    if(index == 0)
        fstock = genfs_user_openfile(STOCK_FILENAME_NOPATH,"wb"); 
    else
        fstock = genfs_user_openfile(STOCK_FILENAME_NOPATH,"ab");
    
    if (fstock == NULL)
    {
        log_push_error_point(0x4016);
        status = S_OPENFILE;
        goto generate_ecm_stock_done;
    }

    buffer = __malloc(GENERATE_ECM_STOCK_COPY_SIZE);
    if(buffer)
    {
        while(!(feof(fptr)))
        {
            bufferlength = fread(buffer,1,GENERATE_ECM_STOCK_COPY_SIZE,fptr);
            if(bufferlength)
            {
                bufferlength = fwrite(buffer,1,bufferlength,fstock);
                if (status == S_SUCCESS)
                {
                    actual_upload_size += bufferlength;
                    currentbytecount += bufferlength;
                    
                    percentage = currentbytecount * 100 / 
                        TUNE_UPLOADING_STATUS(totalbytecount);
                    if ((percentage == 0) ||
                        (percentage != TUNE_UPLOADING_STATUS(percentage)))
                    {
                        TUNE_UPLOADING_STATUS(percentage) = percentage;
                        
                        cmdif_internalresponse_ack_nowait
                            (CMDIF_CMD_DO_UPLOAD, CMDIF_ACK_PROGRESSBAR,
                             &TUNE_UPLOADING_STATUS(percentage),1);
                    }
                }
                else
                {
                    log_push_error_point(0x400D);
                    status = S_WRITEFILE;
                }
            }
            else // bufferlength == 0
            {
                log_push_error_point(0x400C);
                status = S_READFILE;
            }
        } // while(!(feof(fptr)))
        
        __free(buffer);
        
        if(status != S_SUCCESS)
        {
            goto generate_ecm_stock_done;
        }
    }
    else
    {
        log_push_error_point(0x4018);
        status = S_MALLOC;
        goto generate_ecm_stock_done;
    }
    
    if (actual_upload_size == expected_upload_size)
    {
        //it's ok to bypass upload this ecm
        TUNE_UPLOADING_STATUS(currentbytecount) += expected_upload_size;
        // Save new stock file position for rest of upload process
        currentstockposition = ftell(fstock);
        status = S_SUCCESS;
    }
    else if (expected_upload_size%4)
    {
        temp_expected_upload_size = expected_upload_size;
        //need to accout for Padding bytes
        for (u8 i = 0; i < 8; i++)
        {
            if (temp_expected_upload_size % 8 != 0)
            {
                temp_expected_upload_size++;
                paddingsize++; 
            }
            else
            {
                break;
            }
        }
        if((actual_upload_size - paddingsize) == expected_upload_size)
        {
            TUNE_UPLOADING_STATUS(currentbytecount) = expected_upload_size;
            // Save new stock file position for rest of upload process
            currentstockposition = ftell(fstock);
            status = S_SUCCESS;
        }
        else
        {
            status = S_STOCKFILECORRUPT;
        }
        
    }
    else
    {
        log_push_error_point(0x400E);
        status = S_BADCONTENT;
    }
   
generate_ecm_stock_done:         
    genfs_closefile(fptr);
    genfs_closefile(fstock);
    
    if(status == S_SUCCESS)
    {
        status = filestock_openstockfile("a", FALSE);
        if(status == S_SUCCESS)
        {
            status = filestock_set_position(currentstockposition);
            if(status != S_SUCCESS)
            {
                log_push_error_point(0x4017);
                status = S_SEEKFILE;
            }
        }
        else
        {
            log_push_error_point(0x400F);
            status = S_OPENFILE;
        }
    }
    
    return status;
}

//------------------------------------------------------------------------------
// Upload all memory blocks defined for an ecm
// Inputs:  u16 ecm_type
//          VehicleCommType vehiclecommtype
//          bool isAllUnlocked (TRUE: ecm already unlock)
// Return:  u8  status
// Engineer: Quyen Leba
//------------------------------------------------------------------------------
u8 upload_ecm(u16 ecm_type, VehicleCommType vehiclecommtype,
              VehicleCommLevel vehiclecommlevel, bool isAllUnlocked)
{
    BlockType block_type;
    u8  blockorder_current;
    u8  blockorder_index;
    u8 status;
    u8 i;
    bool isutilityfileused;


    isutilityfileused = FALSE;
    if (ECM_IsUploadCalOnly(ecm_type))
    {
        block_type = Cal_Block;
    }
    else
    {
        block_type = OS_Block_Read;
    }

    if (ECM_IsCoolDownTimeShort(ecm_type))
    {
        obd2_setcooldowntime(300);
    }
    else if (ECM_IsCoolDownTimeShort_1(ecm_type))
    {
        obd2_setcooldowntime(200);
    }
    else if (ECM_IsCoolDownTimeShort_2(ecm_type))
    {
        obd2_setcooldowntime(100);
    }
    else if (ECM_IsCoolDownTimeShort_3(ecm_type))
    {
        obd2_setcooldowntime(50);
    }
    else if (ECM_IsCoolDownTimeMedium(ecm_type))
    {
        obd2_setcooldowntime(800);
    }
    else if (ECM_IsCoolDownTimeMedium_1(ecm_type))
    {
        obd2_setcooldowntime(500);
    }
    else if (ECM_IsCoolDownTimeLong(ecm_type))
    {
        obd2_setcooldowntime(2000);
    }
    else
    {
        obd2_setcooldowntime(0);
    }
    
    //##########################################################################
    // check if we have a match file for this ecm, so we can bypass upload
    // Not allowed on Upload Stock Only function
    //##########################################################################
    if (!ECM_IsBlockGenerateStock(ecm_type) && !flasherinfo->isupload_only)
    {
        if (flasherinfo->flashtype == CMDIF_ACT_CUSTOM_FLASHER)
        {
            status = obd2tune_generate_ecm_stock(ecm_type,GenerateStockSource_Use_USER_Folder_First);
        }
        else
        {
            status = obd2tune_generate_ecm_stock(ecm_type,GenerateStockSource_Use_DEFAULT_Folder_First);
        }
        
        if (status == S_SUCCESS)
        {
            //it's ok to bypass upload
            return S_SUCCESS;
        }
        else if(status == S_OPENFILE)
        {
            if(ECM_IsSpfRequired(ecm_type))
            {
                // Need SPF file, update required
                // Partnumbers/Strategies should be saved in Vehicle Info Log file
                cmdif_datainfo_responsedata.responsecode = CMDIF_ACK_SPF_NOT_FOUND;
                return S_FAIL;
            }
        }
        else
        {
            return status;
        }
    }//if (ECM_IsBlockGenerateStock(ecm_type))...

    //##########################################################################
    //
    //##########################################################################
    //TODOQ: use better ACK code
    
    //report ECU unlock start
    cmdif_internalresponse_ack(CMDIF_CMD_DO_UPLOAD,
                               CMDIF_ACK_UNLOCK_ECU,NULL,0);
    
    status = S_SUCCESS;
    if (!SETTINGS_IsDemoMode())
    {
        status = obd2_setupbus_prior_upload(ecm_type);
    }
    if (status != S_SUCCESS)
    {
        cmdif_datainfo_responsedata.responsecode = CMDIF_ACK_FAILED;
        log_push_error_point(0x4010);
        goto upload_ecm_done;
    }
    
    //#########################################
    // Handle Unlock, Speed, Utility File
    //#########################################
    if (isAllUnlocked == FALSE)
    {
        status = S_SUCCESS;
        if (!SETTINGS_IsDemoMode())
        {
            u8  unlockpriv[16]; //only SCP use this now, it uses 1st byte only
            
            //report ECU unlock start
            cmdif_internalresponse_ack(CMDIF_CMD_DO_UPLOAD,
                                       CMDIF_ACK_UNLOCK_ECU,NULL,0);
            
            if (ECM_IsUnlockSkippedInUpload(ecm_type))
            {
                //don't unlock
                status = S_SUCCESS;
            }
            else
            {
                status = obd2tune_unlockecm(UnlockTask_Upload,
                                            ecm_type,vehiclecommtype,unlockpriv);
            }
            if (status != S_SUCCESS)
            {
                log_push_error_point(0x4011);
                cmdif_datainfo_responsedata.responsecode = CMDIF_ACK_FAILED;
                goto upload_ecm_done;
            }
            status = obd2tune_requesthighspeed(ecm_type,vehiclecommtype,unlockpriv);
            if (status != S_SUCCESS && status != S_BAUDUNCHANGED)
            {
                log_push_error_point(0x4012);
                cmdif_datainfo_responsedata.responsecode = CMDIF_ACK_FAILED;
                goto upload_ecm_done;
            }
            status = obd2tune_downloadutility(ecm_type,FALSE, NULL);
            if (status == S_UTILITYNOTREQUIRED)
            {
                isutilityfileused = FALSE;
                status = S_SUCCESS;
            }
            else if (status == S_SUCCESS)
            {
                isutilityfileused = TRUE;
            }
            else
            {
                log_push_error_point(0x4013);
                cmdif_datainfo_responsedata.responsecode = CMDIF_ACK_FAILED;
                goto upload_ecm_done;
            }
            obd2tune_testerpresent_add_ecm(ECM_GetEcmId(ecm_type), ECM_GetCommLevel(ecm_type));
        }//if (!SETTINGS_IsDemoMode())...
    }//if (isAllUnlocked == FALSE)...
    else
    {
        //all ECMs are already unlocked
        status = S_SUCCESS;
    }

    //#########################################
    // Handle Upload
    //#########################################
    if (status == S_SUCCESS)
    {
        if (ECM_GetDataOffset(ecm_type) > 0)
        {
            obd2tune_testerpresent();
            //TODOQ: right now, Jan202010, it is working
            //however, if this block is too large, we need to break it down
            //into smaller blocks otherwise unlock will be timeout
            status = filestock_fill('S',ECM_GetDataOffset(ecm_type));
            if (status != S_SUCCESS)
            {
                cmdif_datainfo_responsedata.responsecode = CMDIF_ACK_FAILED;
                log_push_error_point(0x4014);
                goto upload_ecm_done;
            }
            TUNE_UPLOADING_STATUS(currentbytecount) += ECM_GetDataOffset(ecm_type);
            flasherinfo->tunebytecount += ECM_GetDataOffset(ecm_type);
            obd2tune_testerpresent();
        }

#ifdef __DEBUG_CCVM_        
        int32_t ret;
        int32_t argv[1] = {block_type};
        uint8_t argc = 1;
        status = vm_execute("\\USER\\UPLOAD.vmb",argc,argv,&ret);
        if (status != S_VM_DONE)
        {
            log_push_error_point(0xFFF0);
            status = S_FAIL;
        }
        else if (ret != S_SUCCESS)
        {
            log_push_error_point(0xFFF1);
            status = S_FAIL;
        }
        else
        {
            status = S_SUCCESS;
        }
        if (status != S_SUCCESS)
        {
            cmdif_datainfo_responsedata.responsecode = CMDIF_ACK_FAILED;
            goto upload_ecm_done;
        }
#else        
        blockorder_current = ORDER_0;
        for(blockorder_index=ORDER_0;
            blockorder_index<MAX_ECM_MEMORY_BLOCKS;blockorder_index++)
        {
            for(i=0;i<ECM_MAX_COUNT;i++)
            {
                if (blockorder_index == blockorder_current)
                {
                    status = upload_ecm_memoryblock(ecm_type,
                                                    blockorder_index,block_type);
                    if (status != S_SUCCESS)
                    {
                        cmdif_datainfo_responsedata.responsecode = CMDIF_ACK_FAILED;
                        goto upload_ecm_done;
                    }
                    break;
                }
            }
            blockorder_current++;
        }//for(blockorder_index...
#endif        
    }
    else
    {
        //failed to unlock or download utility file
        cmdif_datainfo_responsedata.responsecode = CMDIF_ACK_UNLOCK_ECU_FAIL;
        log_push_error_point(0x4015);
        goto upload_ecm_done;
    }
    
    //#########################################
    // Read E2
    //#########################################
    if(ECM_IsE2Required(ecm_type))
    { 
       status = obd2tune_upload_e2_data(ecm_type); 
       if(status != S_SUCCESS)
       {
          log_push_error_point(0x4040);
          status = S_FAIL; 
       }
    }
    
    //#########################################
    // Handle Cleanup
    //#########################################
upload_ecm_done:
    if (isutilityfileused)
    {
        //        obd2tune_exitutility(ecm_type,vehiclecommtype,FALSE);
        obd2tune_exitutility(ecm_type,vehiclecommtype,TRUE);
    }
    
    if((ECM_IsSingleUnlockOnly(ecm_type) || isAllUnlocked) && status == S_SUCCESS)
    {
        // If single unlock only or all unlocked, we should skip any resets
    }
    else
    {
        obd2tune_testerpresent_remove_ecm(ECM_GetEcmId(ecm_type));
        obd2tune_reset_ecm(ecm_type);
    }
    
    obd2_setupbus_after_upload(ecm_type);
    
    return status;
}

//------------------------------------------------------------------------------
// Upload stock
// Inputs:  u8  veh_type (vehicle type, index of veh_defs[])
// Return:  u8  status
// Engineer: Quyen Leba, Patrick Downs
//
// log_push_error_point(0x4042); // Abandoned v1.5, failed "filestock_option_upload_init()" 
// log_push_error_point(0x4043); // Abandoned v1.5, failed "obd2tune_option_upload()" 
//------------------------------------------------------------------------------
u8 obd2tune_uploadstock(u16 veh_type, bool isuploadonly)
{
    VehicleCommType vehiclecommtype;
    VehicleCommLevel vehiclecommlevel;
    bool isAllUnlocked;
    bool isSingleUnlockOnly;
    u8  i;
    u8  ecm_index;
    u8  status;
    u32 ecm_type; 
    u32 timeout = 0;

    isAllUnlocked = FALSE;
    isSingleUnlockOnly = FALSE;
    // start with 1st ecm commtype, reinit bus when needed (i.e. other ecms use 
    // different commtype)
    vehiclecommtype = ECM_GetCommType(VEH_GetEcmType(veh_type,0));
    vehiclecommlevel = ECM_GetCommLevel(VEH_GetEcmType(veh_type,0));
    
    //ecm_type is required to check key off/on flag for clearing DTCs after either 
    //and upload only or flashing. All CAN DCX processors required this.
    ecm_type = VEH_GetEcmType(veh_type,0);

    status = S_SUCCESS;

    if (!SETTINGS_IsDemoMode())
    {
        status = obd2_bus_init_in_programming_mode(vehiclecommtype);
    }
    if (status != S_SUCCESS)
    {
        log_push_error_point(0x4020);
        goto upload_stock_done;
    }

    filestock_upload_init(isuploadonly);    //TODOQ: check status of this
    flasherinfo->tunebytecount = 0;
    
    //TODOQ: need to implement check for key (FORD needs it + FEPS)
    cmdif_internalresponse_ack(CMDIF_CMD_DO_UPLOAD,
                               CMDIF_ACK_KEY_ON,NULL,0);
    
    if (VEH_IsRequireUnlockAll(veh_type))
    {
        if (VEH_IsSkipRecycleKeyOn(veh_type))
        {
            //do nothing
        }
        else
        {
            u8  keycheckcount;

            //ask and wait until key is off
            if (!SETTINGS_IsDemoMode())
            {
                keycheckcount = 0;
                do
                {
                    status = obd2_checkkeyon(vehiclecommtype, TRUE);
                    if (status == S_SUCCESS)    //key is still on
                    {
                        if (keycheckcount++ > 100)
                        {
                            //timeout
                            cmdif_datainfo_responsedata.responsecode = CMDIF_ACK_UNLOCK_ECU_FAIL;
                            log_push_error_point(0x402B);
                            status = S_UNLOCKFAIL;
                            goto upload_stock_done;
                        }
                        //send request key off message 
                        cmdif_internalresponse_ack(CMDIF_CMD_DO_UPLOAD,
                                                   CMDIF_ACK_KEY_OFF,NULL,0);
                    }
                    delays(500,'m');
                }while(status == S_SUCCESS);
            }//if (!SETTINGS_IsDemoMode())...
        }
        
        //send request key on message 
        cmdif_internalresponse_ack(CMDIF_CMD_DO_UPLOAD,
                                   CMDIF_ACK_KEY_ON,NULL,0);
        
        status = obd2_setupvpp();
        if (status != S_SUCCESS)
        {
            cmdif_datainfo_responsedata.responsecode = CMDIF_ACK_FAILED;
            log_push_error_point(0x402E);
            goto upload_stock_done;
        }
        if (VEH_IsRequireWakeUp(veh_type))
        {
            if (!SETTINGS_IsDemoMode())
            {
                status = obd2_wakeup(veh_type,0xFE);
                if (status != S_SUCCESS)
                {
                    cmdif_datainfo_responsedata.responsecode = CMDIF_ACK_UNLOCK_ECU_FAIL;
                    log_push_error_point(0x4021);
                    goto upload_stock_done;
                }
                //just in case we'll have mixed commtype vehicles
                status = obd2_bus_init_in_programming_mode(vehiclecommtype);
                if (status != S_SUCCESS)
                {
                    cmdif_datainfo_responsedata.responsecode = CMDIF_ACK_UNLOCK_ECU_FAIL;
                    log_push_error_point(0x4022);
                    goto upload_stock_done;
                }
            }//if (!SETTINGS_IsDemoMode())...
        }

        status = S_FAIL;
        
        obd2tune_testerpresent_setup(TRUE,vehiclecommtype);
        
        for(ecm_index=0;ecm_index<ECM_MAX_COUNT;ecm_index++)
        {
            u8  unlockpriv[16]; //only SCP use this now, it uses 1st byte only
            u16 ecm_type;
            
            ecm_type = VEH_GetEcmType(veh_type,ecm_index);
            if (ecm_type == INVALID_ECM_DEF)
            {
                // termination
                break;
            }
            
            if (ECM_IsUnlockSkippedInUpload(ecm_type))
            {
                //don't unlock
                status = S_SUCCESS;
            }
            else
            {
                status = S_SUCCESS;
                if (!SETTINGS_IsDemoMode())
                {
                    status = obd2tune_unlockecm(UnlockTask_Upload,
                                                ecm_type,ECM_GetCommType(ecm_type),
                                                unlockpriv);
                }//if (!SETTINGS_IsDemoMode())...
            }
            if (status != S_SUCCESS)
            {
                log_push_error_point(0x4023);
                cmdif_datainfo_responsedata.responsecode = CMDIF_ACK_UNLOCK_ECU_FAIL;
                goto upload_stock_done;
            }

            status = S_SUCCESS;
            if (!SETTINGS_IsDemoMode())
            {
                status = obd2tune_requesthighspeed(ecm_type,
                                                   ECM_GetCommType(ecm_type),
                                                   unlockpriv);
            }//if (!SETTINGS_IsDemoMode())...

            if (status != S_SUCCESS && status != S_BAUDUNCHANGED)
            {
                cmdif_datainfo_responsedata.responsecode = CMDIF_ACK_CHANGE_BAUD_ECU_FAIL;
                log_push_error_point(0x4024);
                goto upload_stock_done;
            }
            obd2tune_testerpresent_add_ecm(ECM_GetEcmId(ecm_type), ECM_GetCommLevel(ecm_type));
        }
        
        //TODOQ: implement this
        //TODOQ: must unlock all and keep alive (6.0L doing this)
        if (status != S_SUCCESS)
        {
            cmdif_datainfo_responsedata.responsecode = CMDIF_ACK_UNLOCK_ECU_FAIL;
            log_push_error_point(0x4025);
            goto upload_stock_done;
        }
        isAllUnlocked = TRUE;
    }//if (VEH_IsRequireUnlockAll(veh_type))...
    else
    {
        if (VEH_IsSkipRecycleKeyOn(veh_type))
        {
            //do nothing
        }
        else
        {
            if (VEH_IsRequireWakeUp(veh_type))
            {
                u8  keycheckcount;

                //ask and wait until key is off
                keycheckcount = 0;

                if (!SETTINGS_IsDemoMode())
                {
                    do
                    {
                        status = obd2_checkkeyon(vehiclecommtype, TRUE);
                        if (status == S_SUCCESS)    //key is still on
                        {
                            if (keycheckcount++ > 100)
                            {
                                //timeout
                                cmdif_datainfo_responsedata.responsecode = CMDIF_ACK_UNLOCK_ECU_FAIL;
                                log_push_error_point(0x402A);
                                status = S_UNLOCKFAIL;
                                goto upload_stock_done;
                            }
                            //send request key off message 
                            cmdif_internalresponse_ack(CMDIF_CMD_DO_UPLOAD,
                                                       CMDIF_ACK_KEY_OFF,NULL,0);
                        }
                        delays(500,'m');
                    }while(status == S_SUCCESS);
                }//if (!SETTINGS_IsDemoMode())...
            }
        }
        
        //send request key on message
        cmdif_internalresponse_ack(CMDIF_CMD_DO_UPLOAD,
                                   CMDIF_ACK_KEY_ON,NULL,0);

        status = obd2_setupvpp();   //TODOQ: status
        if (VEH_IsRequireWakeUp(veh_type))
        {
            status = S_SUCCESS;
            if (!SETTINGS_IsDemoMode())
            {
                status = obd2_wakeup(veh_type,0);
            }//if (!SETTINGS_IsDemoMode())...
            if (status != S_SUCCESS)
            {
                cmdif_datainfo_responsedata.responsecode = CMDIF_ACK_UNLOCK_ECU_FAIL;
                log_push_error_point(0x4026);
                goto upload_stock_done;
            }
        }
        
        // Teseter Present setup here, unlocks are done in the upload_ecm() function
        // ECM IDs are added after successful unlocks
        // Highspeed baud will be requested after unlock, if it fails the upload fails.
        obd2tune_testerpresent_setup(TRUE,vehiclecommtype);
    }

    TUNE_UPLOADING_STATUS(percentage) = 0;
    TUNE_UPLOADING_STATUS(currentbytecount) = 0;
    obd2tune_getstocksize_byvehdef(veh_type,
                                   &TUNE_UPLOADING_STATUS(totalbytecount));
    if (TUNE_UPLOADING_STATUS(totalbytecount) == 0)
    {
        status = S_FAIL;
        cmdif_datainfo_responsedata.responsecode = CMDIF_ACK_FAILED;
        log_push_error_point(0x4027);
        goto upload_stock_done;
    }

    for(ecm_index=0;ecm_index<ECM_MAX_COUNT;ecm_index++)
    {
        if (VEH_GetEcmType(veh_type,ecm_index) == INVALID_ECM_DEF)
        {
            // termination
            status = S_SUCCESS;
            break;
        }
        TUNE_UPLOADING_STATUS(ecm_index) = ecm_index;
        
        if (VEH_IsRequireWakeUp(veh_type) &&
            (ECM_IsSimpleWakeupCheckUpload(VEH_GetEcmType(veh_type,ecm_index)) == FALSE))
        {
            status = S_SUCCESS;
            if (!SETTINGS_IsDemoMode())
            {
                status = obd2_wakeup(veh_type,ecm_index);
            }//if (!SETTINGS_IsDemoMode())...
        }
        
        if(ECM_IsSingleUnlockOnly(VEH_GetEcmType(veh_type,ecm_index)))
        {
            isSingleUnlockOnly = TRUE;
        }

        status = upload_ecm(VEH_GetEcmType(veh_type,ecm_index),
                            vehiclecommtype,vehiclecommlevel,isAllUnlocked);
        if (status != S_SUCCESS)
        {
            //Note: all failure must be already saved to
            //cmdif_datainfo_responsedata.responsecode in upload_ecm(...)
            log_push_error_point(0x4028);
            goto upload_stock_done;
        }
    }//for(ecm_index=0;ecm_index...
    
#ifdef __DEBUG_CCVM_
    if (1)
#else    
    if (TUNE_UPLOADING_STATUS(currentbytecount) ==
        TUNE_UPLOADING_STATUS(totalbytecount))
#endif
    {
        if (flasherinfo->flashtype == CMDIF_ACT_UPLOAD_STOCK)
        {
            SETTINGS_TUNE(vehiclecodes)[0] = NULL;
            for(i=0;i<flasherinfo->ecminfo.ecm_count;i++)
            {
                strcat((char*)SETTINGS_TUNE(vehiclecodes),
                       (char*)flasherinfo->ecminfo.codes[i][0]);
                strcat((char*)SETTINGS_TUNE(vehiclecodes),TUNECODE_SEPARATOR_STRING);
            }

            SETTINGS_TUNE(tuneinfotrack.flashtype) = flasherinfo->flashtype;
            SETTINGS_TUNE(processor_count) = flasherinfo->ecm_count;
            SETTINGS_TUNE(veh_type) = flasherinfo->vehicle_type;
            SETTINGS_TUNE(stockfilesize) = TUNE_UPLOADING_STATUS(totalbytecount);
            
            SETTINGS_TUNE(special_vehicletype) = flasherinfo->vehicle_type;
            SETTINGS_TUNE(special_stockfilesize) = TUNE_UPLOADING_STATUS(totalbytecount);
            
            if (flasherinfo->flags & FLASHERFLAG_USER_SELECT_VEHICLE_TYPE)
            {
                SETTINGS_SetUserSelect();
            }
            else
            {
                SETTINGS_ClearUserSelect();
            }
            
            SETTINGS_SetTuneAreaDirty();
        }
    }
    else
    {
        log_push_error_point(0x4029);
        status = S_FAIL;
        cmdif_datainfo_responsedata.responsecode = CMDIF_ACK_FAILED;
        goto upload_stock_done;
    }

upload_stock_done:
    filestock_upload_deinit();
    if (status == S_SUCCESS)
    {

        if(ECM_IsUseVariantId(ecm_type))
            status = filestock_bootloader_position_check();
        if((status == S_FAIL)&&(!isuploadonly))
        {
            log_push_error_point(0x4041);
            status = S_FAIL;
            cmdif_datainfo_responsedata.responsecode = CMDIF_ACK_NOT_SUPPORTED;
        }
        if((isSingleUnlockOnly || isAllUnlocked) && !isuploadonly)
        {
            // Start tester present timer task function to keep PCMs alive until download
            if(tasktimer_setup(2500, TASKTIMER_REPEAT, TaskTimerTaskType_TesterPresent, (TimerTaskFunc)&obd2tune_testerpresent, NULL) != S_SUCCESS)
            {
               log_push_error_point(0x4041);
               cmdif_datainfo_responsedata.responsecode = CMDIF_ACK_FAILED;
            }
            housekeeping_additem(HouseKeepingType_TaskTimerTO, 10000);
        }
        
        //to prevent timeout while waiting for CMDIF_ACK_UPLOAD_DONE
        TUNE_UPLOADING_STATUS(percentage) = 100;
        cmdif_internalresponse_ack
                    (CMDIF_CMD_DO_UPLOAD, CMDIF_ACK_PROGRESSBAR,
                     &TUNE_UPLOADING_STATUS(percentage),1);
        if (SETTINGS_IsDemoMode())
        {
            //IMPORTANT: this prevent using dummy stock file if incorrectly
            //switch between demo & normal
            SETTINGS_TUNE(stockfile_crc32e) = 0x12345678;
            status = S_SUCCESS;
        }
        else
        {
            status = filestock_validate(FILESTOCK_USE_STOCKFILE,
                                        &SETTINGS_TUNE(stockfile_crc32e));
        }
        if (status != S_CRC32E && status != S_SUCCESS)
        {
            log_push_error_point(0x402F);
            status = S_FAIL;
            cmdif_datainfo_responsedata.responsecode = CMDIF_ACK_FAILED;
        }
        else
        {
            //to prevent timeout while waiting for CMDIF_ACK_UPLOAD_DONE
            cmdif_internalresponse_ack
                    (CMDIF_CMD_DO_UPLOAD, CMDIF_ACK_PROGRESSBAR,
                     &TUNE_UPLOADING_STATUS(percentage),1);
            //New stockfile CRC32e was updated in settings
            status = S_SUCCESS;
            cmdif_datainfo_responsedata.responsecode = CMDIF_ACK_UPLOAD_DONE;
        }
    }
    else
    {
        //do nothing
        //cmdif_datainfo_responsedata.responsecode was already set
    }

    if(status != S_SUCCESS)
        power_setvpp(Vpp_OFF);
    
    settings_update(SETTINGS_UPDATE_IF_DIRTY);
       
    if(isuploadonly)
    {
        power_setvpp(Vpp_OFF);
        
        if (isAllUnlocked && !SETTINGS_IsDemoMode())
        {
            // For Vehicle Types that require all PCMs to be unlocked prior to upload/download
            // We reset all PCMs together after
            u16 ecm_type;
            for(ecm_index=0;ecm_index<ECM_MAX_COUNT;ecm_index++)
            {
                ecm_type = VEH_GetEcmType(veh_type,ecm_index);
                if (ecm_type == INVALID_ECM_DEF) 
                    break;   // termination
                
                obd2tune_testerpresent_remove_ecm(ECM_GetEcmId(ecm_type));
                obd2tune_reset_ecm(ecm_type);
            }
        }
        
        if(status == S_SUCCESS && !SETTINGS_IsDemoMode())
        {
            //Key off/on in order to clear DTCs successfully if required. 
            if(ECM_IsDtcKeyRequired(ecm_type))
            {
                u8  keycheckcount;

                //ask and wait until key is off
                if (!SETTINGS_IsDemoMode())
                {
                    keycheckcount = 0;
                    do
                    {
                        status = obd2_checkkeyon(vehiclecommtype, TRUE);
                        if (status == S_SUCCESS)    //key is still on
                        {
                            if (keycheckcount++ > 100)
                            {
                                //timeout
                                status = S_FAIL; 
                            }
                            //send request key off message 
                            cmdif_internalresponse_ack(CMDIF_CMD_DO_UPLOAD,
                                                       CMDIF_ACK_KEY_OFF,NULL,0);
                        }
                        delays(500,'m');
                    }while(status == S_SUCCESS);
                }//if (!SETTINGS_IsDemoMode())...
                 
                //send request key on message 
                cmdif_internalresponse_ack(CMDIF_CMD_DO_UPLOAD,
                                           CMDIF_ACK_KEY_ON,NULL,0);
                timeout = 10;
                do
                {
                    delays(300,'m');
                    status = obd2_checkkeyon(vehiclecommtype, TRUE);
                    timeout--;
                }while(status != S_SUCCESS && timeout);
                
                if(timeout == 0)
                {
                    status = S_TIMEOUT;
                    log_push_error_point(0x403F);
                    cmdif_datainfo_responsedata.responsecode = CMDIF_ACK_FAILED;
                }
                
                delays(500,'m');
            }
        }
        
        obd2_cleardtc(vehiclecommtype,0);
    }

    if (status == S_SUCCESS)
    {
        //this ack just to refresh timeout on app side
        cmdif_internalresponse_ack
                    (CMDIF_CMD_DO_UPLOAD, CMDIF_ACK_PROGRESSBAR,
                     &TUNE_UPLOADING_STATUS(percentage),1);
        
        //ack to let user know checksums might take some time.
        cmdif_internalresponse_ack(CMDIF_CMD_DO_UPLOAD,
                                   CMDIF_ACK_PROCESSING_DATA,NULL,0);
        
        if (!SETTINGS_IsDemoMode())
        {
            status = obd2tune_check_stockfile_checksum(veh_type,
                                                       &flasherinfo->selected_tunelistinfo);
            if (status == S_NOTREQUIRED)
            {
                status = S_SUCCESS;
            }
            else if (status == S_REJECT || status == S_CRC32E)
            {
                //stock file failed our checksum check
                log_push_error_point(0x4030);
                cmdif_datainfo_responsedata.responsecode = CMDIF_ACK_FAILED;    //TODOQK: better ACK
            }
            else if (status != S_SUCCESS)
            {
                log_push_error_point(0x4031);
                cmdif_datainfo_responsedata.responsecode = CMDIF_ACK_FAILED;
            }
        }
    }

    return status;
}
