/**
  **************** (C) COPYRIGHT 2011 SCT Performance, LLC *********************
  * File Name          : obd2scp.c
  * Device / System    : SCT products using the "CommonCode" platform
  * Author             : Quyen Leba
  *
  * Version            : 1 
  * Date               : 05/02/2011
  * Description        : 
  *                    : 
  *
  *
  * History            : 05/02/2011 M. Davis
  *                    :   Add SCT Copyright header,
  *
  ******************************************************************************
  */

#include <stdio.h>
#include <string.h>
#include <ctype.h>
#include <board/power.h>
#include <board/timer.h>
#include <board/delays.h>
#include <board/rtc.h>
#include <common/cmdif.h>
#include <common/log.h>
#include <common/checksum.h>
#include <common/veh_defs.h>
#include <common/statuscode.h>
#include <common/obd2def.h>
#include "obd2scp.h"
#include "obd2.h"

extern obd2_info gObd2info;             /* from obd2.c */

//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
extern u8 obd2_rxbuffer[256];
extern Datalog_Mode gDatalogmode;       //from obd2datalog.c
extern Datalog_Info *dataloginfo;       //from obd2datalog.c
bool obd2scp_bypass_interrupt = FALSE;  // Note: only applicable in low speed

//------------------------------------------------------------------------------
// Private prototypes
//------------------------------------------------------------------------------
u8 obd2scp_validatestrategy(u8 *strategy_in, u8 *strategy_out);


//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
void obd2scp_init(Obd2scpType type)
{
    if (type == Obd2scpType_32Bit)
    {
        scp_init(SCP_TYPE_32BIT,SCP_LOW_SPEED);
    }
    else
    {
        scp_init(SCP_TYPE_24BIT,SCP_LOW_SPEED);
    }
}

//------------------------------------------------------------------------------
// Set obd2scp in datalog mode to bypass interrupt (speed mode)
// Note: only applicable in low speed
//------------------------------------------------------------------------------
void obd2scp_datalogmode()
{
    obd2scp_bypass_interrupt = TRUE;
}

//------------------------------------------------------------------------------
// Set obd2scp in normal mode (strict mode)
// Note: only applicable in low speed
//------------------------------------------------------------------------------
void obd2scp_normalmode()
{
    obd2scp_bypass_interrupt = FALSE;
}

//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
void obd2scp_rxinfo_init(obd2scp_rxinfo *rxinfo)
{
    rxinfo->version = OBD2SCP_RXINFO_VERSION;
    rxinfo->raw_receive = FALSE;
    rxinfo->response_expected = TRUE;   //currently not implemented
    rxinfo->rxbuffer = obd2_rxbuffer;
    rxinfo->rxlength = 0;   
    rxinfo->cmd = 0xFF;
    rxinfo->response_cmd_offset = 0x40;
    rxinfo->first_frame_data_offset = 0;
    rxinfo->errorcode = 0;
    rxinfo->transfermode = SCP_MODE_NODE;
    rxinfo->ecmaddress = SCP_ECM_ID;
    rxinfo->tester_frame_address = SCP_DEVICENODE_RESPONSE_ID;
    rxinfo->tester_ifr_address = SCP_DEVICENODE_RESPONSE_ID;
    rxinfo->rx_timeout_ms = 2000;
}

//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
void obd2scp_servicedata_init(Obd2scp_ServiceData *servicedata)
{
    servicedata->version = OBD2SCP_SERVICEDATA_VERSION;
    servicedata->ecm_id = SCP_ECM_ID;
    servicedata->devicenode_id = SCP_DEVICENODE_RESPONSE_ID;
    servicedata->broadcast = FALSE;
    servicedata->response_expected = TRUE;
    servicedata->service = 0xFF;
    servicedata->subservice = 0xFF;
    
    memset(servicedata->service_controldatabuffer,
           0,sizeof(servicedata->service_controldatabuffer));
    servicedata->service_controldatalength = 0;
    
    servicedata->txdata = NULL;
    servicedata->txdatalength = 0;
    servicedata->rxdata = obd2_rxbuffer;
    servicedata->rxdatalength = 0;
    servicedata->errorcode = 0;
    servicedata->transfermode = SCP_MODE_NODE;
    servicedata->txretry = 2;
    servicedata->rxtimeout_extended = 1;
    servicedata->rx_timeout_ms = 2000;
}

//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
u8 obd2scp_set_controldata(Obd2scp_ServiceData *servicedata,
                           u8 *controldata, u8 controldatalength)
{
    if (controldatalength > 8)
    {
        return S_INPUT;
    }
    memcpy(servicedata->service_controldatabuffer,
           controldata,controldatalength);
    servicedata->service_controldatalength = controldatalength;

    return S_SUCCESS;
}

//------------------------------------------------------------------------------
// Inputs:  u8  *txbuffer
//          u16 txlength
//          u8  transfermode (SCP_MODE_NODE, SCP_MODE_FUNCTIONAL)
// Return:  u8  status
// Engineer: Quyen Leba
//------------------------------------------------------------------------------
u8 obd2scp_tx(u8 *txbuffer, u16 txlength, u8 transfermode)
{   
    return scp_tx(txbuffer,txlength,transfermode);
}

//------------------------------------------------------------------------------
// Inputs:  obd2scp_rxinfo *rxinfo
// Output:  obd2scp_rxinfo *rxinfo
// Return:  u8  status
// Engineer: Quyen Leba
//------------------------------------------------------------------------------
u8 obd2scp_rx(obd2scp_rxinfo *rxinfo)
{
    u8  responsed_command;
    u8  status;
    u8  buffer[24];
    u16 bufferlength;
    u32 timeout;
    
    rxinfo->rxlength = 0;
    responsed_command = rxinfo->cmd + rxinfo->response_cmd_offset;

    if (rxinfo->rx_timeout_ms > 2000)
    {
        timeout = 2000;
    }
    else
    {
        timeout = rxinfo->rx_timeout_ms;
    }
    
obd2scp_rx_begin:
    status = scp_rx(buffer, &bufferlength,
                    rxinfo->transfermode,
                    rxinfo->tester_frame_address, rxinfo->tester_ifr_address,
                    timeout);
    if (status == S_SUCCESS && 
        buffer[1] == rxinfo->tester_frame_address && buffer[2] == rxinfo->ecmaddress && 
//        (buffer[0] == 0xC4 || buffer[0] == 0x64) &&  //TODOQ: more on this, may just check for address (0x10 0xF0) instead
        bufferlength >= 4)
    {
        if (rxinfo->raw_receive)
        {
            //strip the 1st 3 bytes (ex: C4 F0 10)
            if (bufferlength <= 3)
            {
                status = S_BADCONTENT;
                goto obd2scp_rx_done;
            }
            else
            {
                rxinfo->rxlength = bufferlength - 3;
                memcpy(rxinfo->rxbuffer,(char*)&buffer[3],rxinfo->rxlength);
            }
        }
        //[0xC4][Responsed_Id][ECM_ID][Responsed_CMD][n:data]
        //[0xC4][Responsed_Id][ECM_ID][0x7F][CMD][errorcode]
        else if (buffer[3] == 0x7F && buffer[4] == rxinfo->cmd)
        {
            
            if (buffer[5] == 0x78)  //TODOQ: is it still 0x78 for SCP
            {
                //TOODQ: send TP !?!
                goto obd2scp_rx_begin;
            }
            else
            {
                status = S_ERROR;
                rxinfo->errorcode = buffer[bufferlength-2];
                if (bufferlength > 5)
                {
                    rxinfo->rxlength = bufferlength - 5;
                    memcpy(rxinfo->rxbuffer,(char*)&buffer[5],rxinfo->rxlength);
                }
                else
                {
                    rxinfo->rxlength = 0;
                }
                goto obd2scp_rx_done;
            }
        }
        else if (buffer[3] == responsed_command)
        {
            rxinfo->rxlength = bufferlength - 4;
            memcpy(rxinfo->rxbuffer,(char*)&buffer[4],rxinfo->rxlength);
        }
        else
        {
            status = S_FAIL;
            goto obd2scp_rx_done;
        }
    }
    else
    {
        status = S_FAIL;
        goto obd2scp_rx_done;
    }

    return S_SUCCESS;

obd2scp_rx_done:
    return status;
}

//------------------------------------------------------------------------------
// Inputs:  obd2scp_rxinfo *rxinfo
//          rxinfo->rxlength: expecting length to receive
// Output:  obd2scp_rxinfo *rxinfo
//          rxinfo->rxlength: actual length received
// Return:  u8  status
// Engineer: Quyen Leba
//------------------------------------------------------------------------------
u8 obd2scp_rx_complex(obd2scp_rxinfo *rxinfo)
{
    u8  status;
    u16 bufferlength;

    status = scp_rx_complex((u8*)rxinfo,sizeof(obd2scp_rxinfo),
                            rxinfo->rxbuffer,&bufferlength,
                            rxinfo->transfermode,
                            rxinfo->tester_frame_address,rxinfo->tester_ifr_address);
    rxinfo->rxlength = bufferlength;
    return status;
}

//------------------------------------------------------------------------------
// Receive a SCP message without much processing.
// Outputs: u32 *response_ecm_id
//          u8  *data (upto 16 bytes)
// Return:  u8  status
//------------------------------------------------------------------------------
u8 obd2scp_rxraw(u8 *data, u8 *datalength)
{
    u8  rxrawdata[16];
    u16 rxrawdatalength;
    u8  status;
    
    status = scp_rx(rxrawdata, &rxrawdatalength,
                    SCP_MODE_NODE,
                    SCP_DEVICENODE_RESPONSE_ID, SCP_DEVICENODE_RESPONSE_ID,
                    2000);
    
    if (status == S_SUCCESS)
    {
        *datalength = (u8)rxrawdatalength;
        memcpy(data,rxrawdata,rxrawdatalength);
    }
    
    return status;
}

//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
u8 scp_txrx(Obd2scp_ServiceData *servicedata)
{
    u8 txbuffer[SCP_MAX_TX_MSG_LENGTH+1];
    obd2scp_rxinfo rxinfo;
    u32 count;
    u8  bytecount;
    u8  status;

    obd2scp_rxinfo_init(&rxinfo);
    rxinfo.cmd = servicedata->service;
    rxinfo.transfermode = servicedata->transfermode;
    rxinfo.rxbuffer = servicedata->rxdata;

    if (servicedata->transfermode == SCP_MODE_FUNCTIONAL)
    {
        txbuffer[0] = 0x61;
        rxinfo.tester_frame_address = SCP_FuncAddrRsp;
        rxinfo.tester_ifr_address = SCP_DEVICENODE_RESPONSE_ID;
    }
    else
    {    
        txbuffer[0] = 0xC4;   
    }
    
    txbuffer[1] = servicedata->ecm_id;
    txbuffer[2] = servicedata->devicenode_id;

    bytecount = 3;
    if (servicedata->service != 0xFF)
    {
        //TODOQ: could also consider this is an error
        txbuffer[bytecount++] = servicedata->service;
    }
    if (servicedata->subservice != 0xFF)
    {
        txbuffer[bytecount++] = servicedata->subservice;
    }
    if (servicedata->service_controldatalength > 0 && 
        servicedata->service_controldatalength <= sizeof(servicedata->service_controldatabuffer))
    {
        memcpy((char*)&txbuffer[bytecount],
               servicedata->service_controldatabuffer,
               servicedata->service_controldatalength);
        bytecount += servicedata->service_controldatalength;
    }

    if ((servicedata->txdatalength + bytecount) <= SCP_MAX_TX_MSG_LENGTH)
    {
        memcpy((char*)&txbuffer[bytecount],
               servicedata->txdata,servicedata->txdatalength);
        bytecount += servicedata->txdatalength;
    }
    else
    {
        status = S_INPUT;
        goto scp_txrx_done;
    }

scp_txrx_txretry:
    status = obd2scp_tx(txbuffer,bytecount,servicedata->transfermode);
    if (status != S_SUCCESS)
    {
        if (servicedata->txretry--)
        {
            goto scp_txrx_txretry;
        }
        else
        {
            status = S_SEND;
            goto scp_txrx_done;
        }
    }
    
    if (servicedata->response_expected)
    {
        count = 0;
        while(count++ < servicedata->rxtimeout_extended)
        {
            status = obd2scp_rx(&rxinfo);
            if (status == S_SUCCESS)
            {
                servicedata->rxdatalength = rxinfo.rxlength;
                break;
            }
            else if (status == S_ERROR)
            {
                servicedata->errorcode = rxinfo.errorcode;
                servicedata->rxdatalength = rxinfo.rxlength;
                memcpy(servicedata->rxdata,rxinfo.rxbuffer,rxinfo.rxlength);
                break;
            }
            else
            {
                status = S_RECEIVE;
            }
        }
        if(status == S_RECEIVE)
        {
            if (servicedata->txretry--)
            {
                goto scp_txrx_txretry;
            }
        }
    }
    
scp_txrx_done:
    return status;
}

//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
u8 obd2scp_txrx_simple(Obd2scp_ServiceData *servicedata)
{
    u8 txbuffer[SCP_MAX_TX_MSG_LENGTH+1];
    obd2scp_rxinfo rxinfo;
    u8  responsed_command;
    u8  bytecount;
    u8  rxbuffer[64];
    u16 rxbufferlength;
    u8  status;

    obd2scp_rxinfo_init(&rxinfo);
    rxinfo.cmd = servicedata->service;
    rxinfo.transfermode = servicedata->transfermode;
    rxinfo.rxbuffer = servicedata->rxdata;
    responsed_command = rxinfo.cmd + rxinfo.response_cmd_offset;
    servicedata->rxdatalength = 0;
    
    txbuffer[0] = 0xC4;
    txbuffer[1] = servicedata->ecm_id;
    txbuffer[2] = servicedata->devicenode_id;

    status = S_FAIL;
    bytecount = 3;
    if (servicedata->service != 0xFF)
    {
        //TODOQ: could also consider this is an error
        txbuffer[bytecount++] = servicedata->service;
    }
    if (servicedata->subservice != 0xFF)
    {
        txbuffer[bytecount++] = servicedata->subservice;
    }
    if (servicedata->service_controldatalength > 0 && 
        servicedata->service_controldatalength <= sizeof(servicedata->service_controldatabuffer))
    {
        memcpy((char*)&txbuffer[bytecount],
               servicedata->service_controldatabuffer,
               servicedata->service_controldatalength);
        bytecount += servicedata->service_controldatalength;
    }

    if ((servicedata->txdatalength + bytecount) <= SCP_MAX_TX_MSG_LENGTH)
    {
        memcpy((char*)&txbuffer[bytecount],
               servicedata->txdata,servicedata->txdatalength);
        bytecount += servicedata->txdatalength;
    }
    else
    {
        status = S_INPUT;
        goto scp_txrx_simple_done;
    }

    if (servicedata->response_expected)
    {
        status = scp_txrx_simple(txbuffer,bytecount,
                                 servicedata->transfermode,
                                 rxbuffer,&rxbufferlength,servicedata->transfermode,
                                 rxinfo.tester_frame_address,
                                 rxinfo.tester_ifr_address);
        if (status == S_SUCCESS && 
            rxbuffer[1] == rxinfo.tester_frame_address &&
            rxbuffer[2] == rxinfo.ecmaddress && rxbufferlength >= 4)
        {
            if (rxinfo.raw_receive)
            {
                //strip the 1st 3 bytes (ex: C4 F0 10)
                if (rxbufferlength <= 3)
                {
                    status = S_BADCONTENT;
                    goto scp_txrx_simple_done;
                }
                else
                {
                    rxinfo.rxlength = rxbufferlength - 3;
                    memcpy(rxinfo.rxbuffer,(char*)&rxbuffer[3],rxinfo.rxlength);
                    servicedata->rxdatalength = rxinfo.rxlength;
                }
            }
            //[0xC4][Responsed_Id][ECM_ID][Responsed_CMD][n:data]
            //[0xC4][Responsed_Id][ECM_ID][0x7F][CMD][errorcode]
            else if (rxbuffer[3] == 0x7F && rxbuffer[4] == rxinfo.cmd)
            {
                if (rxbuffer[5] == 0x78)  //TODOQ: is it still 0x78 for SCP
                {
                    //TOODQ: send TP !?!
                    goto scp_txrx_simple_done;
                }
                else
                {
                    status = S_ERROR;
                    rxinfo.errorcode = rxbuffer[rxbufferlength-1];
                    if (rxbufferlength > 5)
                    {
                        rxinfo.rxlength = rxbufferlength - 5;
                        memcpy(rxinfo.rxbuffer,(char*)&rxbuffer[5],rxinfo.rxlength);
                        servicedata->rxdatalength = rxinfo.rxlength;
                    }
                    else
                    {
                        rxinfo.rxlength = 0;
                    }
                    goto scp_txrx_simple_done;
                }
            }
            else if (rxbuffer[3] == responsed_command)
            {
                rxinfo.rxlength = rxbufferlength - 4;
                memcpy(rxinfo.rxbuffer,(char*)&rxbuffer[4],rxinfo.rxlength);
                servicedata->rxdatalength = rxinfo.rxlength;
            }
            else
            {
                status = S_FAIL;
                goto scp_txrx_simple_done;
            }
        }
        else
        {
            if (status == S_SUCCESS)
            {
                status = S_FAIL;
            }
            goto scp_txrx_simple_done;
        }
    }
    else
    {
        status = S_INPUT;
        goto scp_txrx_simple_done;
    }
    
scp_txrx_simple_done:
    return status;
}

//------------------------------------------------------------------------------
// Check if vehicle has SCP by doing a $22 of 0x1172
// Return:  u8  status
// Engineer: Quyen Leba
//------------------------------------------------------------------------------
u8 obd2scp_ping()
{
    obd2scp_rxinfo rxinfo;
    u8 txbuffer[SCP_MAX_TX_MSG_LENGTH+1];
    u8 status;
    
    txbuffer[0] = 0xC4;
    txbuffer[1] = SCP_ECM_ID;
    txbuffer[2] = SCP_DEVICENODE_RESPONSE_ID;
    txbuffer[3] = 0x22;
    txbuffer[4] = 0xD1;
    txbuffer[5] = 0x00;
    txbuffer[6] = 0x00;
    txbuffer[7] = 0x00;
        
    status = obd2scp_tx(txbuffer,6,SCP_MODE_NODE);
    if(status == S_SUCCESS)
    {
        obd2scp_rxinfo_init(&rxinfo);
        rxinfo.raw_receive = TRUE;
        rxinfo.tester_frame_address = SCP_DEVICENODE_RESPONSE_ID;
        rxinfo.tester_ifr_address = SCP_DEVICENODE_RESPONSE_ID;
        rxinfo.rx_timeout_ms = 250;

        status = obd2scp_rx(&rxinfo);
        if(status != S_SUCCESS)
        {
            status = S_TIMEOUT;
        }
    }
    
    return status;
}

//------------------------------------------------------------------------------
// ReturnNormalMode ($20)
// Return:  u8  status
// Engineer: Quyen Leba
//------------------------------------------------------------------------------
u8 obd2scp_return_normal_mode()
{
    Obd2scp_ServiceData servicedata;
    u8 status;

    obd2scp_servicedata_init(&servicedata);
    servicedata.service = FORD_ReturnNormalMode;
    
    status = scp_txrx(&servicedata);
    if (status == S_ERROR && servicedata.errorcode == 0x00)
    {
        status = S_SUCCESS;
    }
    return status;
}
//------------------------------------------------------------------------------
// Read Data by ParameterId ($01)
// Inputs:  u8  pid
// Output:  u8  *output
// Return:  u8  status
// Engineer: Tristen Pierson
//------------------------------------------------------------------------------
u8 obd2scp_read_data_bypid_mode1(u8 pid, u8 pidsize, u8 *output)
{
    Obd2scp_ServiceData servicedata;
    obd2scp_rxinfo rxinfo;
    u8 status;
    u16 pidcmp;

    obd2scp_servicedata_init(&servicedata);        
    servicedata.service = FORD_RequestCurrentPowertrainDiagnosticData;
    servicedata.service_controldatabuffer[0] = pid;
    servicedata.service_controldatalength = 1;
    servicedata.transfermode = SCP_MODE_FUNCTIONAL;
    servicedata.ecm_id = SCP_FuncAddrReq;
    
    status = scp_txrx(&servicedata);
    if (status == S_SUCCESS)
    {
        if (servicedata.rxdatalength >= (2+pidsize))
        {
            pidcmp = servicedata.rxdata[0];

            if(pidcmp == pid)
            {
                memcpy((char*)output,(char*)&servicedata.rxdata[1],pidsize);
            }
            else
            {
                // Flush any backed up message
                status = S_UNMATCH;
                obd2scp_rxinfo_init(&rxinfo);
                obd2scp_rx(&rxinfo);
            }
        }
        else
        {
            status = S_FAIL;
        }
    }
    return status;
    
}

//------------------------------------------------------------------------------
// Read Data By Parameter Identifier (PID) ($22) - 2 bytes
// Request Diagnostic Data by PID
// Inputs:  u16 pid
//          u8  pidsize (size in bytes of pid; i.e. 1,2,3,4)
// Output:  u8  *output
// Return:  u8  status
// Engineer: Quyen Leba
//------------------------------------------------------------------------------
u8 obd2scp_read_data_bypid(u16 pid, u8 pidsize, u8 *output)
{
    Obd2scp_ServiceData servicedata;
    obd2scp_rxinfo rxinfo;
    u8 status;
    u16 pidcmp;

    obd2scp_servicedata_init(&servicedata);
    servicedata.service = FORD_ReadDataByParameterIdentifier;
    servicedata.service_controldatabuffer[0] = (pid >> 8);
    servicedata.service_controldatabuffer[1] = (pid & 0xFF);
    servicedata.service_controldatalength = 2;
    servicedata.txretry = 10;
    servicedata.rxtimeout_extended = 10;
    
    //example:
    //C4 10 F0 22 [11 00]
    //C4 F0 10 62 [11 00] [09 FF 00] FF
    
    status = scp_txrx(&servicedata);
    if (status == S_SUCCESS)
    {
        if (servicedata.rxdatalength >= (2+pidsize))
        {
            pidcmp = (servicedata.rxdata[0] << 8);
            pidcmp |= servicedata.rxdata[1];
            if(pidcmp == pid)
            {
                memcpy((char*)output,(char*)&servicedata.rxdata[2],pidsize);
            }
            else
            {
                // Flush any backed up message
                status = S_UNMATCH;
                obd2scp_rxinfo_init(&rxinfo);
                obd2scp_rx(&rxinfo);
            }
        }
        else
        {
            status = S_FAIL;
        }
    }
    return status;
}

//------------------------------------------------------------------------------
// Read Memory By Address ($23) - 3 bytes
// Request Diagnostic Data by Address
// Inputs:  u32 address
//          u16 length
//          MemoryAddressType addrtype
// Output:  u8  *data
//          u8  *datalength
// Return:  u8  status
// Engineer: Quyen Leba
//------------------------------------------------------------------------------
u8 obd2scp_read_memory_address(u32 address, u16 length,
                               MemoryAddressType addrtype,
                               u8 *data, u16 *datalength, bool isHighspeed)
{
    Obd2scp_ServiceData servicedata;
    u8 status;
    u16 bytecount;
    u16 readlength;
    u8 addressconfirm[2];
    u8  retry;

    *datalength = 0;
    if (length > MAX_BUFFER_LENGTH)
    {
        return S_INPUT;
    }

    obd2scp_servicedata_init(&servicedata);
    servicedata.service = FORD_ReadMemoryByAddress;
    servicedata.txretry = 10;
    servicedata.rxtimeout_extended = 10;

    bytecount = 0;
    while(bytecount < length)
    {
        retry = 3;
obd2scp_read_memory_address_retry:
        readlength = length - bytecount;
        if (readlength > 4)
        {
            readlength = 4;
        }

        addressconfirm[0] = (address >> 8);
        addressconfirm[1] = (address & 0xFF);

        if (addrtype == Address_32_Bits)
        {
            servicedata.service_controldatabuffer[0] = (address >> 24);
            servicedata.service_controldatabuffer[1] = (address >> 16);
            servicedata.service_controldatabuffer[2] = (address >> 8);
            servicedata.service_controldatabuffer[3] = (address & 0xFF);
            servicedata.service_controldatalength = 4;
        }
        else
        {
            servicedata.service_controldatabuffer[0] = (address >> 16);
            servicedata.service_controldatabuffer[1] = (address >> 8);
            servicedata.service_controldatabuffer[2] = (address & 0xFF);
            servicedata.service_controldatalength = 3;
        }
        if (!isHighspeed)
        {
            delays(10,'m');
        }

        status = obd2scp_txrx_simple(&servicedata);
        //servicedata.rxdata = [2:last 2 bytes of address][4:data]
        //TODOQ: should I check servicedata.rxdatalength == 6
        if (status == S_SUCCESS && /*servicedata.rxdatalength == 6 &&*/
            servicedata.rxdata[0] == addressconfirm[0] &&
            servicedata.rxdata[1] == addressconfirm[1])
        {
            memcpy((char*)&data[bytecount],
                   (char*)&servicedata.rxdata[2],readlength);
            bytecount += readlength;
        }
        else
        {
            //error occurs
            if (--retry == 0)
            {
                break;
            }
            else
            {
                delays(30,'m');
                goto obd2scp_read_memory_address_retry;
            }
        }
        address += readlength;
    }//while(bytecount < length)...

    *datalength = bytecount;

    return status;
}

//------------------------------------------------------------------------------
// StopTransmittingRequestedData ($25)
// Return:  u8  status
// Engineer: Quyen Leba
//------------------------------------------------------------------------------
u8 obd2scp_stop_transmitting_requested_data()
{
    Obd2scp_ServiceData servicedata;
    u8  status;

    obd2scp_servicedata_init(&servicedata);
    servicedata.service = FORD_StopTransmittingRequestedData;
    
    status = scp_txrx(&servicedata);
    return status;
}

//------------------------------------------------------------------------------
// RequestDiagnosticDataPacket ($2A)
// Inputs:  u8  datarate
//          u8  *packetidlist
//          u8  packetidcount
// Return:  u8  status
// Engineer: Quyen Leba
//------------------------------------------------------------------------------
u8 obd2scp_request_diagnostic_data_packet(u8 datarate,
                                          u8 *packetidlist, u8 packetidcount)
{
    Obd2scp_ServiceData servicedata;
    u8  count;
    u8  bytecount;
    u8  status;

    obd2scp_servicedata_init(&servicedata);
    servicedata.service = FORD_RequestDiagnosticDataPacket;
    servicedata.service_controldatabuffer[0] = datarate;
    servicedata.txretry = 10;
    servicedata.rxtimeout_extended = 10;
    
    if (datarate != 0)
    {
        servicedata.response_expected = FALSE;
    }
    
    //TODOQ: if packetidcount == 0, what to do?
    if (packetidcount > 0)
    {
        count = 0;
        while(count < packetidcount)
        {
            bytecount = packetidcount - count;
            if (bytecount > 5)
            {
                bytecount = 5;
            }
            
            memcpy((char*)&servicedata.service_controldatabuffer[1],
                   (char*)&packetidlist[count],bytecount);
            servicedata.service_controldatalength = 1 + bytecount;
            
            status = scp_txrx(&servicedata);
            if (status != S_SUCCESS)
            {
                break;
            }
            
            count += bytecount;
        }
    }
    
    return status;
}

//------------------------------------------------------------------------------
// DynamicallyDefineDiagnosticDataPacket ($2C)
// Inputs:  u8  packetid (1,2,...)
//          PidType type
//          u8  size (1,2,4)
//          u8  position (start from 1)
//          u32 address
//          MemoryAddressType addrtype (applicable if type == PidTypeDMR)
// Return:  u8  status
// Engineer: Quyen Leba
//------------------------------------------------------------------------------
u8 obd2scp_dynamicallydefinediagnosticdatapacket(u8 packetid, PidType type,
                                                 u8 size, u8 position,
                                                 u32 address,
                                                 MemoryAddressType addrtype)
{
    Obd2scp_ServiceData servicedata;
    u8  tpl;
    u8  status;

    tpl = (size) | (((u8)type) << 6) | (position << 3); 
    
    obd2scp_servicedata_init(&servicedata);
    servicedata.service = FORD_DynamicallyDefineMessage;
    servicedata.service_controldatabuffer[0] = packetid;
    servicedata.service_controldatabuffer[1] = tpl;
    servicedata.txretry = 10;
    servicedata.rxtimeout_extended = 10;
    
    if (type == PidTypeDMR)
    {
        if (addrtype == Address_32_Bits)
        {
            servicedata.service_controldatabuffer[2] = (u8)(address >> 24);
            servicedata.service_controldatabuffer[3] = (u8)(address >> 16);
            servicedata.service_controldatabuffer[4] = (u8)(address >> 8);
            servicedata.service_controldatabuffer[5] = (u8)(address);
            servicedata.service_controldatalength = 6;
        }
        else    //if (addrtype == Address_24_Bits)
        {
            servicedata.service_controldatabuffer[2] = (u8)(address >> 16);
            servicedata.service_controldatabuffer[3] = (u8)(address >> 8);
            servicedata.service_controldatabuffer[4] = (u8)(address);
            servicedata.service_controldatalength = 5;
        }
    }
    else if (type == PidTypeRegular)
    {
        servicedata.service_controldatabuffer[2] = (u8)(address >> 8);
        servicedata.service_controldatabuffer[3] = (u8)(address);
        servicedata.service_controldatalength = 4;
    }
    else
    {
        return S_INPUT;
    }
    
    //example:
    //00 10 F0 2C 01 49 11 72
    //00 F0 10 7F 2C 01 49 11 00
    //00 10 F0 2C 01 49 11 39
    //00 F0 10 7F 2C 01 49 11 00
    //00 10 F0 2C 01 4A 16 8C
    //00 F0 10 7F 2C 01 4A 16 12
    
    status = scp_txrx(&servicedata);
    if (status == S_ERROR && servicedata.errorcode == 0x00)
    {
        status = S_SUCCESS;
    }
    return status;
}

//------------------------------------------------------------------------------
// StartDiagnosticRoutineByTestNumber ($31)
// Inputs:  u8  testnumber
//          u8  *testdata
//          u8  testdatalength
// Return:  u8  status
// Engineer: Quyen Leba
//------------------------------------------------------------------------------
u8 obd2scp_start_diagnostic_routine_bytestnumber(u8 testnumber,
                                                 u8 *testdata,
                                                 u8 testdatalength)
{
    Obd2scp_ServiceData servicedata;
    u8  status;
    u8  expected_responsecode;

    //testnumber:
    //0xA0: used in ECM unlock
    //0xA1: used to issue ECM erase
    //0xA2: used after download each block
    //0xA4: used to request high speed mode
    switch(testnumber)
    {
    case 0xA0:
    case 0xA4:
        expected_responsecode = 0x00;
        break;
    case 0xA1:
    case 0xA2:
        expected_responsecode = 0x23;
        break;
    default:
        //unknown testnumber
        log_push_error_point(0x4298);
        return S_INPUT;
    }
    
    obd2scp_servicedata_init(&servicedata);
    servicedata.rxtimeout_extended = 10;
    servicedata.service = FORD_StartDiagnosticRoutineByTestNumber;
    servicedata.subservice = testnumber;
    if (testdatalength > 0 && testdata)
    {
        obd2scp_set_controldata(&servicedata,(u8*)testdata,testdatalength);
    }
    
    //this sniff is from unlock part
    //example tx: [C4 10 F0] [31 A0] [00 07] [1:seed] [00]
    //example response: [C4 F0 10] [7F 31] [A0] [00 70 00]
    //where 00 07 are from 112K EECV
    //servicedata.rxdata should contain: [A0] [00 70 00]
    //                                or [A0] [03 00 31] if there's error
    status = scp_txrx(&servicedata);
    if (status == S_ERROR && servicedata.rxdata[3] == expected_responsecode)
    {
        status = S_SUCCESS;
    }
    else if (status == S_TIMEOUT)
    {
        log_push_error_point(0x4299);
    }
    else if (status == S_FAIL)
    {
        log_push_error_point(0x429A);
    }
    else if (status == S_SEND)
    {
        log_push_error_point(0x42B7);
    }
    else if (status == S_RECEIVE)
    {
        log_push_error_point(0x42B8);
    }
    
    return status;
}

//------------------------------------------------------------------------------
// StopDiagnosticRoutineByTestNumber ($32)
// Input:   u8  testnumber
// Return:  u8  status
// Engineer: Quyen Leba
//------------------------------------------------------------------------------
u8 obd2scp_stop_diagnostic_routine_bytestnumber(u8 testnumber)
{
    Obd2scp_ServiceData servicedata;
    u8  expected_responsecodes[2];
    bool bypassresponsecode;
    u8  status;

    bypassresponsecode = FALSE;
    
    obd2scp_servicedata_init(&servicedata);
    servicedata.rxtimeout_extended = 10;
    servicedata.service = FORD_StopDiagnosticRoutineByTestNumber;
    servicedata.subservice = testnumber;
    
    //testnumber:
    //0xA1: used to check ECM erase done
    //0xA2: used after download each block
    switch(testnumber)
    {
    case 0xA1:
        expected_responsecodes[0] = 0x00;
        expected_responsecodes[1] = 0x00;
        servicedata.rxtimeout_extended = 2;
        break;
    case 0xA2:
        servicedata.service_controldatabuffer[0] = 0x00;
        servicedata.service_controldatalength = 1;
        expected_responsecodes[0] = 0x00;
        expected_responsecodes[1] = 0x22;
        break;
    default:
        //unknown testnumber
        return S_INPUT;
    }
    
    //example tx: [C4 10 F0] [32 A2]
    //example response: [C4 F0 10] [7F 32] [A2] [?? ?? 00]
    //servicedata.rxdata should contain: [A2] [?? ?? 00]
    //                                or [A2] [?? ?? 23] if there's error
    status = scp_txrx(&servicedata);
    if (status == S_ERROR && 
        ((servicedata.rxdata[3] == expected_responsecodes[0] ||
          servicedata.rxdata[3] == expected_responsecodes[1]) || bypassresponsecode))
    {
        status = S_SUCCESS;
    }
    else if (status == S_ERROR)
    {
        status = S_ERROR;
    }
//    else if (status == S_SEND)
//    {
//        log_push_error_point(0x42B9);
//    }
    else
    {
        obd2scp_rxinfo rxinfo;
        
        obd2scp_rxinfo_init(&rxinfo);
        rxinfo.cmd = servicedata.service;
        rxinfo.transfermode = servicedata.transfermode;
        rxinfo.rxbuffer = servicedata.rxdata;
        
        status = obd2scp_rx(&rxinfo);
        if (status == S_ERROR && 
            ((servicedata.rxdata[3] == expected_responsecodes[0] ||
              servicedata.rxdata[3] == expected_responsecodes[1]) || bypassresponsecode))
        {
            status = S_SUCCESS;
        }
        else
        {
            status = S_FAIL;
        }
    }

    return status;
}

//------------------------------------------------------------------------------
// RequestDownload ($34)
// Inputs:  u32 address
//          u16 length
//          MemoryAddressType addrtype
// Return:  u8  status
// Engineer: Quyen Leba
//------------------------------------------------------------------------------
u8 obd2scp_request_download(u32 address, u16 length, MemoryAddressType addrtype)
{
    Obd2scp_ServiceData servicedata;
    u8  retry;
    u8  status;

    obd2scp_servicedata_init(&servicedata);
    servicedata.service = FORD_RequestDownload;
    servicedata.rxtimeout_extended = 10;
    
    if (addrtype == Address_32_Bits)
    {
        delays(50,'m');
        servicedata.rxtimeout_extended = 10;
        servicedata.service_controldatabuffer[0] = 0xC2;
        servicedata.service_controldatabuffer[1] = 0x80;
        servicedata.service_controldatabuffer[2] = (u8)(address >> 24);
    }
    else    //Address_24_Bits
    {
        servicedata.service_controldatabuffer[0] = 0x80;
        servicedata.service_controldatabuffer[1] = (u8)(length >> 8);
        servicedata.service_controldatabuffer[2] = (u8)(length & 0xFF);
    }
    servicedata.service_controldatabuffer[3] = (u8)(address >> 16);
    servicedata.service_controldatabuffer[4] = (u8)(address >> 8);
    servicedata.service_controldatabuffer[5] = (u8)(address & 0xFF);
    servicedata.service_controldatalength = 6;

    retry = 3;
obd2scp_request_download_retry:
    delays(5,'m');
    status = scp_txrx(&servicedata);
    if (status == S_ERROR)
    {
        status = S_FAIL;
        if (addrtype == Address_32_Bits)
        {
            //CommType_SCP32 expect response:
            //where tx frame: [C4 10 F0 35] [C2 80] [4:address]
            //servicedata.rxdata should contains:
            if (servicedata.errorcode == 0x00)
            {
                status = S_SUCCESS;
            }
            else
            {
                log_push_error_point(0x429B);
            }
        }
        else
        {
            //CommType_SCP expect response: [00 F0 10 7F 34] [80] [2:length] [00]
            //where tx frame: [C4 10 F0 34] [80] [2:length] [3:address]
            //servicedata.rxdata should contains: [80] [2:length] [00]
            if (servicedata.errorcode == 0x00)
            {
                status = S_SUCCESS;
            }
            else
            {
                log_push_error_point(0x429C);
            }
        }
    }
    else if (status == S_SEND)
    {
        log_push_error_point(0x42BC);
        if (retry--)
        {
            delays(1000,'m');
            goto obd2scp_request_download_retry;
        }
    }
    else if (status == S_RECEIVE)
    {
        log_push_error_point(0x42BD);
        if (retry--)
        {
            delays(2000,'m');
            goto obd2scp_request_download_retry;
        }
    }
    else if (status != S_SUCCESS)
    {
        log_push_error_point(status);
        log_push_error_point(0x42BE);
    }
    
    return status;
}

//------------------------------------------------------------------------------
// Read a block of data from PCM using $35 $36 $37
// Inputs:  u32 address
//          MemoryAddressType addrtype
//          u16 length
// Output:  u8  *buffer
// Return:  u8  status
// Engineer: Quyen Leba
//------------------------------------------------------------------------------
u8 obd2scp_readblock_bycmd35single36single37(u32 address,
                                             MemoryAddressType addrtype,
                                             u16 length, u8 *buffer)
{
    obd2scp_rxinfo rxinfo;
    u8  retry;
    u8  status;

    obd2scp_rxinfo_init(&rxinfo);
    retry = 3;
obd2scp_readblock_bycmd35single36single37_retry:
    status = scp_readblock_bycmd35single36single37(address,addrtype,
                                                   length,buffer,
                                                   rxinfo.tester_frame_address,
                                                   rxinfo.tester_ifr_address);
    if (status != S_SUCCESS)
    {
        log_push_error_point(0x42BF);
        if (retry--)
        {
            delays(2000,'m');
            goto obd2scp_readblock_bycmd35single36single37_retry;
        }
    }
    return status;
}

//------------------------------------------------------------------------------
// RequestUpload ($35)
// Inputs:  u32 address
//          u16 length
//          MemoryAddressType addrtype
// Return:  u8  status
// Engineer: Quyen Leba
//------------------------------------------------------------------------------
u8 obd2scp_request_upload(u32 address, u16 length, MemoryAddressType addrtype)
{
    Obd2scp_ServiceData servicedata;
    u8 status;

    obd2scp_servicedata_init(&servicedata);
    servicedata.service = FORD_RequestUpload;
    
    if (addrtype == Address_32_Bits)
    {
        servicedata.service_controldatabuffer[0] = 0x42;
        servicedata.service_controldatabuffer[1] = 0x80;
        servicedata.service_controldatabuffer[2] = (u8)(address >> 24);
    }
    else    //Address_24_Bits
    {
        servicedata.service_controldatabuffer[0] = 0x01;
        servicedata.service_controldatabuffer[1] = (u8)(length >> 8);
        servicedata.service_controldatabuffer[2] = (u8)(length & 0xFF);
    }
    servicedata.service_controldatabuffer[3] = (u8)(address >> 16);
    servicedata.service_controldatabuffer[4] = (u8)(address >> 8);
    servicedata.service_controldatabuffer[5] = (u8)(address & 0xFF);
    servicedata.service_controldatalength = 6;
    
    delays(5,'m');
    status = scp_txrx(&servicedata);
    if (status == S_ERROR)
    {
        status = S_FAIL;
        if (addrtype == Address_32_Bits)
        {
            //CommType_SCP32 expect response:
            //where tx frame: [C4 10 F0 35] [42 80] [4:address]
            //servicedata.rxdata should contains: [42 80] 00 00
            if (servicedata.errorcode == 0x00)
            {
                status = S_SUCCESS;
            }
            else
            {
                log_push_error_point(0x429D);
            }
        }
        else
        {
            //CommType_SCP expect response: [00 F0 10 7F 35] [01] [04 00] [00]
            //where tx frame: [C4 10 F0 35] [01 04 00] [3:address]
            //servicedata.rxdata should contains: [01] [04 00] [00]
            if (servicedata.errorcode == 0x00)
            {
                status = S_SUCCESS;
            }
            else
            {
                log_push_error_point(0x429E);
            }
        }
    }
    else if (status != S_SUCCESS)
    {
        //do nothing; should it return S_FAIL in this case too?
    }
    
    return status;
}

//------------------------------------------------------------------------------
// DataTransfer Upload ($36)
// Input:   u16 length
// Outputs: u8  *data (must be able to store MAX_UPLOAD_BUFFER_LENGTH bytes)
// Return:  u8  status
// Engineer: Quyen Leba
//------------------------------------------------------------------------------
u8 obd2scp_transfer_data_upload(u8 *data, u16 length)
{
    obd2scp_rxinfo rxinfo;
    u8  status;

    obd2scp_rxinfo_init(&rxinfo);
    rxinfo.cmd = FORD_TransferData;
    rxinfo.response_cmd_offset = 0;
    rxinfo.rxbuffer = data;
    rxinfo.rxlength = length;

    status = obd2scp_rx_complex(&rxinfo);
    if (status == S_SUCCESS && rxinfo.rxlength == length)
    {
        return S_SUCCESS;
    }
    else
    {
        log_push_error_point(0x429F);
        return S_FAIL;
    }
}

//------------------------------------------------------------------------------
// DataTransfer Download ($36)
// Inputs:  u8  *data
//          u16 length
// Return:  u8  status
// Engineer: Quyen Leba
//------------------------------------------------------------------------------
u8 obd2scp_transfer_data_download(u8 *data, u16 length)
{
    Obd2scp_ServiceData servicedata;
    u8  txframe[32];
    u8  extrabuffer[32];
    u16 extrabufferlength;
    u8  *bptr;
    u16 calc_checksum;
    u16 bytecount;
    u16 writesize;
    int  retry;
    u8  status;

    obd2scp_servicedata_init(&servicedata);
    txframe[0] = 0xC4;
    txframe[1] = servicedata.ecm_id;
    txframe[2] = servicedata.devicenode_id;
    txframe[3] = FORD_TransferData;
    calc_checksum = (u16)checksum_simpleadditive(data,length);
    
    //##########################################################################
    // Send *data to ECM 6-byte at a time
    // Any left-over byte will be handled in next step
    //##########################################################################
    bytecount = 0;
    writesize = 0;
    bptr = &txframe[4];         //point to the beginning of data part in frame
    while(bytecount < length)
    {
        writesize = length - bytecount;
        if (writesize > 6)
        {
            writesize = 6;      //there're only 6 data-byte per (write)frame
        }
        else
        {
            break;
        }
        memcpy((char*)bptr,(char*)&data[bytecount],writesize);

        retry = 3;
obd2scp_transfer_data_download_tx_part1:
        status = obd2scp_tx(txframe,10,SCP_MODE_NODE);
        if (status != S_SUCCESS)
        {
            if (retry--)
            {
                goto obd2scp_transfer_data_download_tx_part1;
            }
            else
            {
                log_push_error_point(0x42BA);
                goto obd2scp_transfer_data_download_done;
            }
            //TODOQ:
            //log_push_error_point(0x42A0);
        }
        bytecount += 6;
    }
    
    //##########################################################################
    // Send to ECM any left-over byte from previous step, and 2-byte checksum
    // of the data
    //##########################################################################
    extrabufferlength = 0;
    if (writesize > 0)
    {
        memcpy((char*)extrabuffer,(char*)&data[bytecount],writesize);
        extrabufferlength += writesize;
    }
    extrabuffer[extrabufferlength++] = (u8)(calc_checksum >> 8);
    extrabuffer[extrabufferlength++] = (u8)(calc_checksum & 0xFF);
    
    bytecount = 0;
    while(bytecount < extrabufferlength)
    {
        writesize = extrabufferlength - bytecount;
        if (writesize > 6)
        {
            writesize = 6;      //there're only 6 data-byte per (write)frame
        }
        else
        {
            memset((char*)bptr,0,6);
        }
        memcpy((char*)bptr,(char*)&extrabuffer[bytecount],writesize);

        retry = 3;
obd2scp_transfer_data_download_tx_part2:
        status = obd2scp_tx(txframe,10,SCP_MODE_NODE);
        if (status != S_SUCCESS)
        {
            if (retry--)
            {
                goto obd2scp_transfer_data_download_tx_part2;
            }
            else
            {
                log_push_error_point(0x42BB);
                goto obd2scp_transfer_data_download_done;
            }
            //TODOQ:
//            log_push_error_point(0x42A1);
        }
        
        bytecount += 6;
    }
obd2scp_transfer_data_download_done:
    return status;
}

//------------------------------------------------------------------------------
// RequestTransferExit ($37)
// Inputs:  MemoryAddressType addrtype
//          TransferExitMode exitmode
// Return:  u8  status
// Engineer: Quyen Leba
//------------------------------------------------------------------------------
u8 obd2scp_request_transfer_exit(MemoryAddressType addrtype,
                                 TransferExitMode exitmode)
{
    Obd2scp_ServiceData servicedata;
    u8  status;

    obd2scp_servicedata_init(&servicedata);
    servicedata.service = FORD_RequestDataTransferExit;
    
    if (exitmode == TransferExitFromUpload)
    {
        if (addrtype == Address_32_Bits)
        {
            servicedata.rxtimeout_extended = 10;
            servicedata.service_controldatabuffer[0] = 0x42;
        }
        else    //Address_24_Bits
        {
            servicedata.service_controldatabuffer[0] = 0x01;
        }
    }
    else if (exitmode == TransferExitFromDownload)
    {
        if (addrtype == Address_32_Bits)
        {
            servicedata.rxtimeout_extended = 10;
            servicedata.service_controldatabuffer[0] = 0xC2;
        }
        else    //Address_24_Bits
        {
            servicedata.service_controldatabuffer[0] = 0x80;
        }
    }
    else
    {
        log_push_error_point(0x42A2);
        return S_INPUT;
    }
    servicedata.service_controldatalength = 1;
    
    //TODOQ: in the sniff, I can see correct response for every $37 sent
    //however, our device always missed it the 1st time.

    delays(5,'m');
    status = scp_txrx(&servicedata);
    if (status == S_ERROR)
    {
        status = S_FAIL;
        if (addrtype == Address_32_Bits)
        {
            //CommType_SCP32 expect response:
            //where tx frame: [C4 10 F0 35] [42]
            //servicedata.rxdata should contains: 
            if (servicedata.errorcode == 0x00 || servicedata.errorcode == 0x22)
            {
                status = S_SUCCESS;
            }
            else
            {
                log_push_error_point(servicedata.errorcode);
                log_push_error_point(0x42A3);
                delays(2000,'m');
                return S_FAIL;
            }
        }
        else
        {
            //CommType_SCP expect response: [00 F0 10 7F] [37] [01 00 00] 00
            //where tx frame: [C4 10 F0 37] [01]
            //servicedata.rxdata should contains: [01 00 00] 00
            if (servicedata.errorcode == 0x00 || servicedata.errorcode == 0x22)
            {
                status = S_SUCCESS;
            }
            else
            {
                log_push_error_point(0x42A4);
                delays(60,'m');
                return S_FAIL;
            }
        }
    }
    else if (status != S_SUCCESS)
    {
        if (status == S_SEND)
        {
            log_push_error_point(0x42B1);
        }
        else if (status == S_RECEIVE)
        {
            log_push_error_point(0x42B2);
        }
        else
        {
            log_push_error_point(0x42B3);
        }
        //do nothing; should it return S_FAIL in this case too?
    }
    
    //NOTE: current state: still not catch this response, but it's ok, if it
    //actually fails, the next command you try to do will fail too.
    //TODOQ: why? more on this
    
//    delays(50,'m');
//    return status;
    return S_SUCCESS;
}

//------------------------------------------------------------------------------
// Get the starting physical address of VID
// Output:  u32 *vidaddress
// Return:  u8  status
// Engineer: Quyen Leba
// Date: Apr 04, 2008
// Note: work on Ford SCP processors
//------------------------------------------------------------------------------
u8 obd2scp_getvidaddress(u32 *vidaddress)
{
    u8 status;
    u8 data[24];

    *vidaddress = 0;

    status = obd2scp_read_data_bypid(0x1100,3,data);
    if (status == S_SUCCESS)
    {
        *vidaddress  = (u32)data[0] << 16;
        *vidaddress |= (u32)data[1] << 8;
        *vidaddress |= (u32)data[2];
    }
    return status;
}

//------------------------------------------------------------------------------
// Read VID block by vehicle types (defined in ford_veh_defs.c)
// Input:   u16 ford_veh_type
// Output:  u8  *vid (must be able to store 128bytes)
// Return:  u8  status
// Engineer: Quyen Leba
//------------------------------------------------------------------------------
u8 obd2scp_readvid_by_vehicletype(u8 *vid, u16 ford_veh_type)
{
    u8  ecmtype;
    u8  commtype;
    u32 vidaddress;
    u16 bytecount;
    u8  status;
    MemoryAddressType addrtype;

    ecmtype = VEH_GetEcmType(ford_veh_type,0);
    
    // Service only Supported Types
    if (ecmtype != 0xFF)
    {
        commtype = ECM_GetCommType(ecmtype);
        vidaddress = ECM_GetVidAddress(ecmtype);
    }
    else
    {
        return S_VEHICLETYPE;
    }

    //obd2scp_init(SCP_LOW_SPEED);

    if (commtype == CommType_SCP)
    {
        addrtype = Address_24_Bits;
    }
    else if (commtype == CommType_SCP32)
    {
        addrtype = Address_32_Bits;
    }
    else
    {
        return S_COMMTYPE;
    }

    if ((ford_veh_type == 3) || (ford_veh_type == 4))
    {
        vidaddress += 0x10000;
    }

    status = obd2scp_read_memory_address(vidaddress,128,
                                         addrtype,vid,&bytecount,FALSE);
    return status;
}

//------------------------------------------------------------------------------
// Read PATS block by vehicle types (defined in ford_veh_defs.c)
// Input:   u16 ford_veh_type
// Output:  u8  *pats (must be able to store 14bytes)
// Return:  u8  status
// Engineer: Quyen Leba
//------------------------------------------------------------------------------
u8 obd2scp_readpats_by_vehicletype(u8 *pats, u16 ford_veh_type)
{
    u8  ecmtype;
    u8  commtype;
    u32 patsaddress;
    u16 bytecount;
    u8  status;
    MemoryAddressType addrtype;

    ecmtype = VEH_GetEcmType(ford_veh_type,0);
    
    // Service only Supported Types
    if (ecmtype != 0xFF)
    {
        commtype = ECM_GetCommType(ecmtype);
        patsaddress = ECM_GetPatsAddress(ecmtype);
    }
    else
    {
        return S_VEHICLETYPE;
    }

    //obd2scp_init(SCP_LOW_SPEED);

    if (commtype == CommType_SCP)
    {
        addrtype = Address_24_Bits;
    }
    else if (commtype == CommType_SCP32)
    {
        addrtype = Address_32_Bits;
    }
    else
    {
        return S_COMMTYPE;
    }

    if ((ford_veh_type == 3) || (ford_veh_type == 4))
    {
        patsaddress += 0x10000;
    }

    status = obd2scp_read_memory_address(patsaddress,14,
                                         addrtype,pats,&bytecount,FALSE);
    return status;
}

//------------------------------------------------------------------------------
// Read SCT security byte by vehicle types (defined in ford_veh_defs.c)
// Input:   u16 ford_veh_type
// Output:  u8  *securitybyte (only 1 byte output)
// Return:  u8  status
// Engineer: Quyen Leba
//------------------------------------------------------------------------------
u8 obd2scp_readsecuritybyte_by_vehicletype(u8 *securitybyte, u16 ford_veh_type)
{
    u8  ecmtype;
    u8  commtype;
    u32 vidaddress;
    u32 sbaddress;
    u16 bytecount;
    u8  status;
    MemoryAddressType addrtype;

    ecmtype = VEH_GetEcmType(ford_veh_type,0);
    
    // Service only Supported Types
    if (ecmtype != 0xFF)
    {
        commtype = ECM_GetCommType(ecmtype);
        vidaddress = ECM_GetVidAddress(ecmtype);
        // Get security byte address, 1 byte before strategy
        sbaddress = vidaddress - 123;
    }
    else
    {
        return S_VEHICLETYPE;
    }

    //obd2scp_init(SCP_LOW_SPEED);

    if (commtype == CommType_SCP)
    {
        addrtype = Address_24_Bits;
    }
    else if (commtype == CommType_SCP32)
    {
        addrtype = Address_32_Bits;
    }
    else
    {
        return S_COMMTYPE;
    }

    if ((ford_veh_type == 3) || (ford_veh_type == 4))
    {
        sbaddress += 0x10000;
    }

    status = obd2scp_read_memory_address(sbaddress,1,
                                         addrtype,securitybyte,&bytecount,FALSE);
    return status;
}

//------------------------------------------------------------------------------
// Check strategy_in buffer for printable character and vaild ".HEX" for FORD
// strategy.
// 
// Input:   u8  *strategy_in (pass buffer containing possible strategy)
// Output:  u8  *strategy_out (store strategy, 40bytes)
//          
// Return:  u8  status
// Engineer: Patrick Downs
//------------------------------------------------------------------------------
u8 obd2scp_validatestrategy(u8 *strategy_in, u8 *strategy_out)
{
    u8 i, h;

    for(i=0;i<20;i++)
    {
        //check if char is printable char & not a space
        if (isgraph(strategy_in[i]) == 0)
        {
            break;
        }
    }
    if (i>5 && i<19)
    {
        if(strstr((void*)strategy_in,".H"))     // Search for end of strategy
        {
            i=0;
            do
            {
                *strategy_out++ = strategy_in[i++];
            }while(strategy_in[i] != '.');
            
            *strategy_out = '*';
            *strategy_out++;
            *strategy_out = NULL;
            return S_SUCCESS;                
        }
        else
        {
            for(h=0;h<i;h++)
            {
                *strategy_out++ = strategy_in[h];
            }
            
            *strategy_out = '*';
            *strategy_out++;
            *strategy_out = NULL;
            // No ".H" found, but there are characters where strat should be
            // return for displaying
            return S_NOHEXFOUND;
        }
    }
    return S_FAIL;
}

/**
 *  @brief Read VIN (SCP)
 *  
 *  @param [in]  vin             Vehicle VIN
 *  @param [out] vehiclecommtype Comm Type (SCP or SCP32)
 *
 *  @return Status
 *  
 *  @details This function returns vehicle VIN. If VID address cannot be
 *           determined, this function scans different VID addresses until a
 *           valid VIN is found. NOTE: *strategy must be able to store
 *           VIN_LENGTH+1 bytes.
 */
u8 obd2scp_readvin(u8 *vin, VehicleCommType *vehiclecommtype)
{
    u32 vid[VID_LIST_SCP_COUNT] =   {VID_LIST_SCP};
    u32 vid32[VID_LIST_SCP32_COUNT] = {VID_LIST_SCP32};
    u8  readdata[32 + VIN_LENGTH];
    u16 bytecount;
    u32 vidaddr;
    u32 vinaddr;
    u8  status;
    u8  i;
    
    memset(vin, 0, VIN_LENGTH+1);
    
    /* Get VID Address */
    for (i = 0; i < 3; i++)
    {
        delays(300,'m');
        status = obd2scp_getvidaddress(&vidaddr);
        if (status == S_SUCCESS)
        {
            break;
        }
    }
    /* Read and validate strategy */
    if (status == S_SUCCESS)
    {
        /* VIN address is VID address + 0x60 */
        vinaddr = vidaddr + 0x60;
        
        status = obd2scp_read_memory_address(vinaddr, sizeof(readdata),
                                             Address_24_Bits, readdata,
                                             &bytecount, FALSE);
        if (status == S_SUCCESS)
        {
            memcpy(vin, readdata+32, VIN_LENGTH);
            status = obd2_validatevin(vin);
        }
        if (status == S_SUCCESS || status == S_VINBLANK)
        {
            *vehiclecommtype = CommType_SCP;
        }
        if (status != S_SUCCESS && status != S_VINBLANK)
        {
            status = obd2scp_read_memory_address(vinaddr, sizeof(readdata),
                                                 Address_32_Bits, readdata,
                                                 &bytecount, FALSE);
                            
            if (status == S_SUCCESS)
            {
                memcpy(vin, readdata+32, VIN_LENGTH);
                status = obd2_validatevin(vin);
            }
            if (status == S_SUCCESS || status == S_VINBLANK)
            {
                *vehiclecommtype = CommType_SCP32;
            }
        }
    }
    /* Scan for VIN using SCP VIDs */
    if (status != S_SUCCESS && status != S_VINBLANK)
    {
        for (i = 0; i < sizeof(vid)/4; i++)
        { 
            vinaddr = vid[i] - 0x20;
            status = obd2scp_read_memory_address(vinaddr, sizeof(readdata),
                                                 Address_24_Bits, readdata,
                                                 &bytecount, FALSE);
            if (status == S_SUCCESS)
            {
                memcpy(vin, readdata+32, VIN_LENGTH);
                status = obd2_validatevin(vin);
            }
            if (status == S_SUCCESS || status == S_VINBLANK)
            {
                *vehiclecommtype = CommType_SCP;
                break;
            }
        }
    }
    /* Scan for VIN using SCP32 VIDs */
    if (status != S_SUCCESS && status != S_VINBLANK)
    {
        for (i = 0; i < sizeof(vid32)/4; i++)
        {
            vinaddr = vid32[i] - 0x20;
            status = obd2scp_read_memory_address(vinaddr, sizeof(readdata),
                                                 Address_32_Bits, readdata,
                                                 &bytecount, FALSE);
            if (status == S_SUCCESS)
            {
                memcpy(vin, readdata+32, VIN_LENGTH);
                status = obd2_validatevin(vin);
            }
            if (status == S_SUCCESS || status == S_VINBLANK)
            {
                *vehiclecommtype = CommType_SCP32;
                break;
            }
        }
    }
    return status;
}

/**
 *  @brief Read vehicle strategy (SCP)
 *  
 *  @param [in]  strategy        Vehicle Strategy
 *  @param [out] vehiclecommtype Comm Type (SCP or SCP32)
 *
 *  @return Status
 *  
 *  @details This function returns vehicle strategy. If VID address cannot be
 *           determined, this function scans different VID addresses until a
 *           valid strategy is found. NOTE: *strategy must be able to store 40
 *           bytes.
 */
u8 obd2scp_readstrategy(u8 *strategy, VehicleCommType *vehiclecommtype)
{
    u32 vid[VID_LIST_SCP_COUNT] =   {VID_LIST_SCP};
    u32 vid32[VID_LIST_SCP32_COUNT] = {VID_LIST_SCP32};
    u32 vidaddr;
    u8  readdata[40];
    u16 bytecount;
    u32 strategyaddress;
    u8  status;
    u8  i;
    
    /* Get VID Address */
    for (i = 0; i < 3; i++)
    {
        delays(300,'m');
        status = obd2scp_getvidaddress(&vidaddr);
        if (status == S_SUCCESS)
        {
            break;
        }
    }
    /* Read and validate strategy */
    if (status == S_SUCCESS)
    {
        /* Strategy address is VID address + 6 */
        strategyaddress = vidaddr + 6;
        
        status = obd2scp_read_memory_address(strategyaddress, 20,
                                             Address_24_Bits, readdata,
                                             &bytecount, FALSE);
        if (status == S_SUCCESS)
        {
            status = obd2scp_validatestrategy(readdata, strategy);
        }
        if (status == S_SUCCESS)
        {
            *vehiclecommtype = CommType_SCP;
        }
        if (status != S_SUCCESS)
        {
            status = obd2scp_read_memory_address(strategyaddress, 20,
                                                 Address_32_Bits, readdata,
                                                 &bytecount, FALSE);
                            
            if (status == S_SUCCESS)
            {
                status = obd2scp_validatestrategy(readdata, strategy);
            }
            if (status == S_SUCCESS)
            {
                *vehiclecommtype = CommType_SCP32;
            }
        }
    }
    /* Scan for strategy using SCP VIDs */
    if (status != S_SUCCESS)
    {
        for (i = 0; i < sizeof(vid)/4; i++)
        { 
            strategyaddress = vid[i] - 0x7A;
            status = obd2scp_read_memory_address(strategyaddress, 20,
                                                 Address_24_Bits, readdata,
                                                 &bytecount, FALSE);
            if (status == S_SUCCESS)
            {
                status = obd2scp_validatestrategy(readdata, strategy);
            }
            if (status == S_SUCCESS)
            {
                *vehiclecommtype = CommType_SCP;
                break;
            }
        }
    }
    /* Scan for strategy using SCP32 VIDs */
    if (status != S_SUCCESS)
    {
        for (i = 0; i < sizeof(vid32)/4; i++)
        {
            strategyaddress = vid32[i] - 0x7A;
            status = obd2scp_read_memory_address(strategyaddress, 20,
                                                 Address_32_Bits, readdata,
                                                 &bytecount, FALSE);
            if (status == S_SUCCESS)
            {
                status = obd2scp_validatestrategy(readdata, strategy);
            }
            if (status == S_SUCCESS)
            {
                *vehiclecommtype = CommType_SCP32;
                break;
            }
        }
    }
    return status;
}

/**
 *  @brief Read ECM info from OBD2 info
 *  
 *  @param [in]  ecm_info   ECM info structure
 *
 *  @return status
 *  
 *  @details This function reads all available part numbers and serial number.
 */
u8 obd2scp_readecminfo(ecm_info *ecm)
{
    ecm->ecm_count = gObd2info.ecu_count;
    memcpy(ecm->vin, gObd2info.vin, sizeof(ecm->vin));
    
    for(u8 i = 0; i < ECM_MAX_COUNT; i++)
    {
        if (gObd2info.ecu_block[i].ecutype != EcuType_Unknown)
        {
            ecm->commtype[i] = gObd2info.ecu_block[i].commtype;
            ecm->commlevel[i] = gObd2info.ecu_block[i].commlevel;
            ecm->codecount[i] = gObd2info.ecu_block[i].partnumber_count;
            memcpy(ecm->codes[i][0],
                   gObd2info.ecu_block[i].partnumbers.ford.strategy,
                   sizeof(gObd2info.ecu_block[i].partnumbers.ford.strategy));
        }
    }
   
    return S_SUCCESS; 
}

//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
u8 obd2scp_cleardtc()
{
    Obd2scp_ServiceData servicedata;

    obd2scp_servicedata_init(&servicedata);
    servicedata.txretry = 10;
    servicedata.rxtimeout_extended = 10;
    servicedata.service = FORD_ClearDiagnosticInfomationExt;
    return scp_txrx(&servicedata);
}

//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
u8 obd2scp_readdtc(dtc_info *info)
{
    Obd2scp_ServiceData servicedata;
    obd2scp_rxinfo rxinfo;
    const u8 controldata[2] = {0xFF, 0x00};
    u8  status;
    u8  i;
    u8  dtccount;
    u16 dtccode;
    u8  retry;
    bool done;

    obd2scp_servicedata_init(&servicedata);
    servicedata.service = FORD_RequestDiagnosticInformationExt;
    servicedata.txretry = 10;
    servicedata.rxtimeout_extended = 10;
    obd2scp_set_controldata(&servicedata,(u8*)controldata,sizeof(controldata));

    obd2scp_rxinfo_init(&rxinfo);
    rxinfo.cmd = FORD_RequestDiagnosticInformationExt;

    retry = 3;
    dtccount = 0;

obd2scp_readdtc_retry:
    status = scp_txrx(&servicedata);
    if (status == S_ERROR && servicedata.errorcode == 0x62)
    {
        //not ready
        if (retry--)
        {
            delays(500,'m');
            goto obd2scp_readdtc_retry;
        }
        else
        {
            //no DTCs
            status = S_SUCCESS;
        }
    }
    else if (status == S_SUCCESS)
    {
        done = FALSE;
        do
        {
            for(i=0;i<3;i++)
            {
                dtccode = 
                    ((u16)servicedata.rxdata[i*2] << 8) |
                    ((u16)servicedata.rxdata[i*2+1]);
                if (dtccode == 0)
                {
                    done = TRUE;
                    break;
                }
                info->codes[dtccount] = dtccode;
                dtccount++;
                if (dtccount > DTC_MAX_COUNT)
                {
                    done = TRUE;
                    break;
                }
            }
            if (done == FALSE)
            {
                status = obd2scp_rx(&rxinfo);
                if (status != S_SUCCESS)
                {
                    done = TRUE;
                    if (dtccount > 0)
                    {
                        //in case there's no zero code padding
                        status = S_SUCCESS;
                    }
                }
            }
        }while(done == FALSE);
    }
    
    info->count = dtccount;
    
    return status;
}

//------------------------------------------------------------------------------
// Generate the key from the seeds
// This routine calculates the 2 byte key used to enter FLASH security mode
// on SCP Ford ECUs.
//
// Inputs:  u8  *seed (The 3 byte seed returned from the seed request)
//          u8  l_seed (The 1 byte seed sent to the ECU during entrance to
//              flash mode)
// Output:  u8  *key (2 byte key to unlock the privileged functions in the ECU)
// Return:  u8  status
//------------------------------------------------------------------------------
u16 obd2scp_generatekey(u8 *seed, u8 l_seed, u8 *key)
{
    u16 count1;
    u16 count2;
    u16 modifier[2];
    u16 seed_byte[3];
    u16 tmp_byte[2];
    s32 partial;
    u16 calc_key;
    
    seed_byte[1] = seed[0] & 0xff;
    seed_byte[0] = seed[1] & 0xff;
    seed_byte[2] = seed[2];
    
    if ((l_seed & 1) == 1)
    {
        modifier[0] = (seed[2] >> 4) & 0x03;
        modifier[1] = (seed[2] >> 6) & 0x03;
    }
    else
    {
        modifier[0] = seed[2] & 0x03;
        modifier[1] = (seed[2] >> 2) & 0x03;
    }
    
    for(count1=0;count1<2;count1++)
    {
        switch(modifier[count1])
        {
        case 0:
            partial = seed_byte[count1];
            for (count2=1;count2<l_seed;count2++)
            {
                partial = partial*seed_byte[count1] & 0xffff;
            }
            
            tmp_byte[count1] = 
                ((seed_byte[count1]+0x64) * (seed_byte[count1]+0x64)) +
                ((partial - 1) * l_seed);
            break;
        case 1:
            partial = seed_byte[count1]+5;
            tmp_byte[count1] = (l_seed*5) - (partial*partial*partial);
            break;
        case 2:
            partial = seed_byte[count1];
            partial = (s32)partial*partial*partial - (s32)l_seed*l_seed + 0x0a;
            tmp_byte[count1] = partial * partial;
            break;
        case 3:
            partial = seed_byte[count1] + 0x22;
            tmp_byte[count1] = (seed_byte[count1]*seed_byte[count1]) + l_seed
                + (partial*partial*partial) - 0x28;
            break;
        }//switch(modifier...
    }//for(count1=...
    calc_key = ((tmp_byte[1] << 8) + (tmp_byte[0] & 0xff));
    key[0] = (u8)(calc_key >> 8);
    key[1] = (u8)(calc_key & 0xFF);
    
    return S_SUCCESS;
}

//------------------------------------------------------------------------------
// Check for vehicle wake up (SCP)
// Inputs:  u8  veh_type (UNUSED)
//          u8  ecm_index (UNUSED)
// Return:  u8  status
// Engineer: Quyen Leba
// Note: in vehicles that require FEPS, it'll require this wakeup to make sure
// it's ready for upload/download; only do it once
//------------------------------------------------------------------------------
u8 obd2scp_wakeup(u8 veh_type, u8 ecm_index)
{
    obd2scp_rxinfo rxinfo;
//    u8  buffer[340];
//    u16 bufferlength;
    u8  status;
//    u32 timestamp;
    s32 timeout;    
    
    obd2scp_rxinfo_init(&rxinfo);
    rxinfo.raw_receive = TRUE;
    //set address 0x05 for power up message
    rxinfo.tester_frame_address = SCP_KEYONFEPS_DEVICENODE_ID;
    rxinfo.tester_ifr_address = SCP_DEVICENODE_RESPONSE_ID;
    
    timeout = 30;
    
    while(1)
    {
        //TODOQ: this, it need internal state
//        status = cmdif_receive_command_internal(buffer,&bufferlength,sizeof(buffer));
//        if (status == S_SUCCESS && buffer[1] == CMDIF_CMD_ABORT_TASK)
//        {
//            status = S_USERABORT;
//            goto obd2scp_unlockecm_done;
//        }
        
        //expecting: 00 [05] [10] 04 00 02 ...
        //rxinfo.rxbuffer should contains 04 00 02 ...
        
        status = obd2scp_rx(&rxinfo);
        if (status == S_SUCCESS)
        {
            break;
        }
        timeout--;
        if(timeout <= 0)
        {
            return S_TIMEOUT;
        }
        //log_push_error_point(0x42A5); //reserved for timeout here
    }
    rxinfo.tester_frame_address = SCP_DEVICENODE_RESPONSE_ID;
    rxinfo.tester_ifr_address = SCP_DEVICENODE_RESPONSE_ID;
    
    return status;
}

//------------------------------------------------------------------------------
// Unlock SCP ecm
// Inputs:  u8  ecm_type (defined for ecm_defs)
//          bool seed_peek (only get seed, don't send unlock key)
// Output:  u8  *l_seed (to be used in obd2scp_requesthighspeed)
// Return:  u8  status
// Engineer: Quyen Leba
//------------------------------------------------------------------------------
u8 obd2scp_unlockecm(u8 ecm_type, bool seed_peek, u8 *l_seed)
{
    Obd2scp_ServiceData servicedata;
    u8  seeddata[4];            //size must be 4 bytes
    u8  key[2];
    u8  status;

//    obd2scp_init();
//
//    //##########################################################################
//    // Turn FEPS on, and wait for user to turn key on
//    // There'll be a power-on/key-on frame sent from ecm when that happens
//    //##########################################################################
//    power_setvpp(Vpp_OFF);
//    //enable FEPS
//    if (power_setvpp(Vpp_FEPS) != S_SUCCESS)
//    {
//        status = S_SETPOWER;
//        goto obd2scp_unlockecm_done;
//    }
//    
//    //TODOQ: can I move this out?
//    cmdif_internalresponse_ack(CMDIF_CMD_PCM_UNLOCK_____XXXXXXX___do_not_use_like_this,
//                               CMDIF_ACK_UNLOCK_ECU,NULL,0);
    
//    obd2scp_rxinfo_init(&rxinfo);
//    rxinfo.raw_receive = TRUE;
//    //set address 0x05 for power up message
//    rxinfo.nodeaddress = SCP_KEYONFEPS_DEVICENODE_ID;
//    
//    while(1)
//    {
//        //TODOQ: this, it need internal state
////        status = cmdif_receive_command_internal(buffer,&bufferlength,?);
////        if (status == S_SUCCESS && buffer[1] == CMDIF_CMD_ABORT_TASK)
////        {
////            status = S_USERABORT;
////            goto obd2scp_unlockecm_done;
////        }
//        
//        //expecting: 00 [05] [10] 04 00 02 ...
//        //rxinfo.rxbuffer should contains 04 00 02 ...
//        status = obd2scp_rx(&rxinfo);
//        if (status == S_SUCCESS)
//        {
//            break;
//        }
//    }
    
    //##########################################################################
    //
    //##########################################################################
    *l_seed = timer1_getcounter();
    
    seeddata[2] = (u8)*l_seed;
    seeddata[3] = 0;
    //TODOQ: should I put this in ecm_defs?
    switch(ecm_type)
    {
    case 1:     // 220K EECV (SCP)
        seeddata[0] = 0x00;
        seeddata[1] = 0xdc;
        break;
    case 3:     // 88K EECV (SCP)
        seeddata[0] = 0x00;
        seeddata[1] = 0x58;
        break;
    case 2:     // 112K EECV (SCP)
        seeddata[0] = 0x00;
        seeddata[1] = 0x70;
        break;
    case 0:     // 216K EECV (SCP)
        seeddata[0] = 0x00;
        seeddata[1] = 0xd8;
        break;
    case 14:    // 1024K PTEC (SCP32)
        seeddata[0] = 0x04;
        seeddata[1] = 0x00;
        break;
    case 13:    // 1472K Black Oak (SCP32)
        seeddata[0] = 0x05;
        seeddata[1] = 0xc0;
        break;
    case 24:    // 448K Black Oak (SCP32)
        seeddata[0] = 0x01;
        seeddata[1] = 0xc0;
        break;
    case 25:    // 256K EEC (SCP)
        seeddata[0] = 0x01;
        seeddata[1] = 0x00;
        seeddata[3] = 0x01;
        break;
    default:
        status = S_FAIL;
        log_push_error_point(0x42A6);
        goto obd2scp_unlockecm_done;
    }

    status = obd2scp_start_diagnostic_routine_bytestnumber(0xA0,seeddata,4);
    if (status != S_SUCCESS)
    {
        log_push_error_point(0x42A7);
        goto obd2scp_unlockecm_done;
    }

    //##########################################################################
    // example unlock sniff:
    // 00 10 F0 31 A0 00 70 A7 00
    // 00 F0 10 7F 31 A0 00 70 00
    // 00 10 F0 27 01
    // 00 F0 10 67 01 FB FC E8
    // 00 10 F0 27 02 61 A4
    // 00 F0 10 67 02 34
    //##########################################################################
    obd2scp_servicedata_init(&servicedata);
    servicedata.service = FORD_SecurityAccess;
    servicedata.subservice = SPSrequestSeed;
    //expect servicedata.rxdata: [01 FB FC E8]
    status = scp_txrx(&servicedata);
    if (status != S_SUCCESS)
    {
        log_push_error_point(0x42A8);
        goto obd2scp_unlockecm_done;
    }
    
    obd2scp_generatekey(&servicedata.rxdata[1],*l_seed,key);
    
    obd2scp_servicedata_init(&servicedata);
    servicedata.service = FORD_SecurityAccess;
    servicedata.subservice = SPSsendKey;
    obd2scp_set_controldata(&servicedata,(u8*)key,2);
    
    //expect servicedata.rxdata: [02 34]
    status = scp_txrx(&servicedata);
    if (status != S_SUCCESS)
    {
        log_push_error_point(0x42A9);
        goto obd2scp_unlockecm_done;
    }
    else if (servicedata.rxdata[1] != 0x34)
    {
        status = S_FAIL;
        log_push_error_point(0x42AA);
        goto obd2scp_unlockecm_done;
    }

obd2scp_unlockecm_done:
    return status;
}

//------------------------------------------------------------------------------
// This routine calculates the 2 byte key used to change the baud rate on
// Ford SCP ECUs
//
// Inputs:  u8  *seed (The 3 byte seed returned from the seed request)
//          u8  l_seed (The 1 byte seed sent to the ECU during entrance to
//              flash mode)
// Output:  u8  *key (2 byte key to unlock the privileged functions in the ECU)
// Return:  u8  status
//------------------------------------------------------------------------------
u8 obd2scp_generatebaudkey(u8 *seed, u8 l_seed, u8 *key)
{
    u16 count1;
    u16 count2;
    u16 modifier[2];
    u16 seed_byte[3];
    u16 tmp_byte[2];
    s32 partial;
    u16 calc_key;
    
    seed_byte[1] = seed[0] & 0xff;
    seed_byte[0] = seed[1] & 0xff;
    seed_byte[2] = seed[2];
    
    if ((l_seed & 1) == 1)
    {
        modifier[0] = (seed[2] >> 4) & 0x03;
        modifier[1] = (seed[2] >> 6) & 0x03;
    }
    else
    {
        modifier[0] = seed[2] & 0x03;
        modifier[1] = (seed[2] >> 2) & 0x03;
    }
    
    for(count1=0;count1<2;count1++)
    {
        switch(modifier[count1])
        {
        case 0:
            partial = seed_byte[count1];
            for (count2 = 1; count2 < l_seed; count2++)
            {
                partial = partial*seed_byte[count1] & 0xffff;
            }
            
            tmp_byte[count1] =
                ((seed_byte[count1]+0x63) * (seed_byte[count1]+0x63)) +
                ((partial - 1) * l_seed);
            break;
        case 1:
            partial = seed_byte[count1]+5;
            tmp_byte[count1] = (l_seed*5) + (partial*partial*partial);
            break;
        case 2:
            partial = seed_byte[count1];
            partial = (s32)partial*partial*partial - (s32)l_seed*l_seed + 0x0b;
            tmp_byte[count1] = partial * partial;
            break;
        case 3:
            partial = seed_byte[count1] + 0x21;
            tmp_byte[count1] = 
                (seed_byte[count1]*seed_byte[count1]) + l_seed + 
                (partial*partial*partial) - 0x28;
            break;
        }//switch(modifier...
    }//for(count1=...
    
    calc_key = ((tmp_byte[1] << 8) + (tmp_byte[0] & 0xff));
    key[0] = (u8)(calc_key >> 8);
    key[1] = (u8)(calc_key & 0xFF);
    return S_SUCCESS;
}

//------------------------------------------------------------------------------
// Request high speed mode for SCP (83k)
// Input:   VehicleCommType commtype
//          u8  l_seed (obtained from immediate previous unlock)
// Return:  u8  status
// Note: use this function after a obd2scp_unlockecm(...) called, also l_seed
// is generated from that function
//------------------------------------------------------------------------------
u8 obd2scp_requesthighspeed(VehicleCommType commtype, u8 l_seed)
{
    Obd2scp_ServiceData servicedata;
    u8  key[2];
    u8  requestbuffer[1];
    u8  status;
    
    obd2scp_servicedata_init(&servicedata);
    servicedata.service = FORD_SecurityAccess;
    servicedata.subservice = SPSrequestSeed;
    //expect servicedata.rxdata: [01 FB FC E8]
    status = scp_txrx(&servicedata);
    if (status != S_SUCCESS)
    {
        log_push_error_point(0x42AB);
        goto obd2scp_requesthighspeed_done;
    }
    
    obd2scp_generatebaudkey(&servicedata.rxdata[1],l_seed,key);
    
    obd2scp_servicedata_init(&servicedata);
    servicedata.service = FORD_SecurityAccess;
    servicedata.subservice = SPSsendKey;
    obd2scp_set_controldata(&servicedata,(u8*)key,2);
    
    //expect servicedata.rxdata: [02 34]
    status = scp_txrx(&servicedata);
    if (status != S_SUCCESS)
    {
        log_push_error_point(0x42AC);
        goto obd2scp_requesthighspeed_done;
    }
    else if (servicedata.rxdata[1] != 0x34)
    {
        status = S_FAIL;
        log_push_error_point(0x42AD);
        goto obd2scp_requesthighspeed_done;
    }
    
    requestbuffer[0] = 0x03;
    status = obd2scp_start_diagnostic_routine_bytestnumber(0xA4,
                                                           requestbuffer,1);
    //expect response: [00 F0 10] [7F 31] [A4] [03 00 00]
    //expect servicedata.rxdata: [A4] [03 00 00]

    if (status != S_SUCCESS)
    {
        log_push_error_point(0x42AE);
        goto obd2scp_requesthighspeed_done;
    }
    
obd2scp_requesthighspeed_done:
    if (status == S_SUCCESS)
    {
        if (commtype == CommType_SCP32)
        {
            scp_init(SCP_TYPE_32BIT,SCP_HIGH_SPEED);
        }
        else
        {
            scp_init(SCP_TYPE_24BIT,SCP_HIGH_SPEED);
        }
    }
    return status;
}

//------------------------------------------------------------------------------
// Inputs:  u8  ecm_type (index of ecm_defs)
//          bool is_cal_only (TRUE: erase cal only)
// Return:  u8  status
// Engineer: Quyen Leba
//------------------------------------------------------------------------------
u8 obd2scp_erase_ecm(u8 ecm_type, bool is_cal_only)
{
    VehicleCommType commtype;
    u32 retry;
    u32 delaytime;
    u8  restart_erase;
    u8  status;

    restart_erase = 1;
obd2scp_erase_ecm_restart:
    //ecm_type 13 14 need extra long timeout
    status = obd2scp_start_diagnostic_routine_bytestnumber(0xA1,NULL,0);
    if (status != S_SUCCESS)
    {
        log_push_error_point(0x42AF);
        return status;
    }
    
    delays(5,'m');
    
    commtype = ECM_GetCommType(ecm_type);
    if (commtype == CommType_SCP32)
    {
        delaytime = 1000;
    }
    else //CommType_SCP
    {
        delaytime = 500;
    }
    retry = 65;

    while(--retry > 0)
    {
        status = obd2scp_stop_diagnostic_routine_bytestnumber(0xA1);
        if (status == S_SUCCESS)
        {
            break;
        }
        
        delays(delaytime,'m');
    }
    if (status != S_SUCCESS)
    {
        log_push_error_point(0x42B0);
        if (restart_erase--)
        {
            goto obd2scp_erase_ecm_restart;
        }
    }
    delays(1000,'m');

    return status;
}

//------------------------------------------------------------------------------
// Input:   u8  ecm_type (index of ecm_defs)
// Return:  u8  status
// Engineer: Quyen Leba
//------------------------------------------------------------------------------
u8 obd2scp_reset_ecm(u8 ecm_type)
{
    power_setvpp(Vpp_OFF);
    obd2scp_return_normal_mode();
    return S_SUCCESS;
}


//------------------------------------------------------------------------------
// Clear KAM (Keep Alive Memory)
// Obtained from PTDIAG sniffs of clear KAM function
// Inputs:  None
// Return:  u8  status
// Engineer: Tristen Pierson
//------------------------------------------------------------------------------
u8 obd2scp_clear_kam(void)
{    
    Obd2scp_ServiceData servicedata;
    u8 status;
    
    obd2scp_servicedata_init(&servicedata);        
    servicedata.service = FORD_DiagnosticCommand;
    servicedata.service_controldatabuffer[0] = 0x80;
    servicedata.service_controldatabuffer[1] = 0x12;
    servicedata.service_controldatalength = 2;
    
    status = scp_txrx(&servicedata);    
    
    if (status == S_ERROR)
    {
        switch (servicedata.errorcode)
        {
        case 0x00: // Successful
            status = S_SUCCESS;
            break;
        case 0x11: // Service not supported
        case 0x12: // Subfunction not supported
        case 0x31: // Request out of range
            status = S_NOTSUPPORT;
            break;
        default:
            break;
        }
    }   
    
    return status;
}

//------------------------------------------------------------------------------
// Validate a PID
// Input:   DlxBlock *dlxblock
// Return:  u8  status
// Engineer: Quyen Leba
// TODOQ: commlevel is currently not used
//------------------------------------------------------------------------------
u8 obd2scp_validatepid(DlxBlock *dlxblock,
                       VehicleCommLevel commlevel)
{
    u8  buffer[256];
    u8  status;    
        
    // Validate by mode 0x22
    do
    {
        status = obd2scp_read_data_bypid((u16)dlxblock->pidAddr,
                                     dlxblock->pidSize,buffer);
    } while (status == S_UNMATCH);    
    
    return status;
}

//------------------------------------------------------------------------------
// Validate a DMR
// Inputs:  DlxBlock *dlxblock
//          VehicleCommType commtype
//          VehicleCommLevel commlevel
// Return:  u8  status
// Engineer: Quyen Leba
// TODOQ: commlevel is currently not used
//------------------------------------------------------------------------------
u8 obd2scp_validatedmr(DlxBlock *dlxblock,
                       VehicleCommType commtype,
                       VehicleCommLevel commlevel)
{    
    // Validate by rapid-packet setup
    return obd2scp_validateByRapidPacketSetup(dlxblock, commtype, commlevel);
}

//------------------------------------------------------------------------------
// Validate a dmr/pid with Rapid Packet Setup Mode 0x2C
// Inputs:  DlxBlock *dlxblock
//          VehicleCommType commtype
//          VehicleCommLevel commlevel
// Return:  u8  status
// Engineer: Quyen Leba
// TODOQ: commlevel is currently not used
//------------------------------------------------------------------------------
u8 obd2scp_validateByRapidPacketSetup(DlxBlock *dlxblock,
                       VehicleCommType commtype,
                       VehicleCommLevel commlevel)
{
    u8  status;

    if (commtype == CommType_SCP32)
    {
        status = obd2scp_dynamicallydefinediagnosticdatapacket
            (FORD_KWP_SCP_PACKETID_START,(PidType)dlxblock->pidType,dlxblock->pidSize,1,
             dlxblock->pidAddr,Address_32_Bits);
    }
    else
    {
        status = obd2scp_dynamicallydefinediagnosticdatapacket
            (FORD_KWP_SCP_PACKETID_START,(PidType)dlxblock->pidType,dlxblock->pidSize,1,
             dlxblock->pidAddr,Address_24_Bits);
    }
    return status;
}

//------------------------------------------------------------------------------
// Check if all signals fit in available packets
// Input:   VehicleCommLevel commlevel
// Return:  u8  status
// Engineer: Quyen Leba
//------------------------------------------------------------------------------
u8 obd2scp_datalog_evaluatesignalsetup(VehicleCommLevel vehiclecommlevel)
{
    u8  packetid;
    u8  byteavailable[2][FORD_KWP_SCP_PACKETID_END-FORD_KWP_SCP_PACKETID_START];
    u8  processcheck[MAX_DATALOG_DLXBLOCK_COUNT];
    u8  ecm_index;    
    u8  processedperloop;
    u8  index;
    u16 i;
    bool packet;
    u16 pidsignalcount;
    u16 dmrsignalcount;
    u16 analogsignalcount;

    if (dataloginfo == NULL)
    {
        return S_BADCONTENT;
    }    
    // If OBD PIDs are datalogged, we cannot start a packet session
    if (vehiclecommlevel == CommLevel_Unknown || gDatalogmode.obdPids == TRUE)
    {
        gDatalogmode.packetmode = SingleRate;
    }

    memset(processcheck,FALSE,MAX_DATALOG_DLXBLOCK_COUNT);
    memset((char*)byteavailable,FORD_KWP_SCP_MAX_DATABYTE_PER_PACKET,sizeof(byteavailable));
    pidsignalcount = dmrsignalcount = analogsignalcount = 0;
    dataloginfo->packetidcount[0] = 0;
    dataloginfo->packetidcount[1] = 0;

    // Evaluate analog signals, reset packet information
    for(i=0;i<dataloginfo->datalogsignalcount;i++)
    {
        if (dataloginfo->datalogsignals[i].PidType == PidTypeAnalog &&
            (dataloginfo->datalogsignals[i].Control & DATALOG_CONTROL_SKIP) == 0 &&
            analogsignalcount < dataloginfo->analogsignalmaxavailable)
        {
            dataloginfo->analogsignalindex[analogsignalcount++] = i;
        }
        // Remove packet information since we may be recovering from
        // S_NOTFIT during rapid packet setup
        dataloginfo->datalogsignals[i].SignalType.Generic.PacketNumber = DATALOG_INVALID_PACKETID;
        dataloginfo->datalogsignals[i].SignalType.Generic.Position = 0;
    }

    // Evaluate Rapid Packet Signals
    if (gDatalogmode.packetmode == RapidRate)
    {
        for(ecm_index=0;ecm_index<2;ecm_index++)
        {
            packetid = FORD_KWP_SCP_PACKETID_START;
            index = 0;
                        
            while(packetid != FORD_KWP_SCP_PACKETID_END)
            {                
                processedperloop = 0;
                packet = FALSE;
                
                for(i=0;i<dataloginfo->datalogsignalcount;i++)
                {                    
                    if (processcheck[i])
                    {
                        //already processed
                        continue;
                    }
                    if (dataloginfo->datalogsignals[i].SignalType.Generic.PacketNumber == DATALOG_INVALID_PACKETID &&
                        dataloginfo->datalogsignals[i].EcmIndex == ecm_index &&
                        (dataloginfo->datalogsignals[i].PidType == PidTypeRegular ||
                        dataloginfo->datalogsignals[i].PidType == PidTypeDMR ||
                        dataloginfo->datalogsignals[i].PidType == PidTypeMode1) &&
                        (dataloginfo->datalogsignals[i].Control & DATALOG_CONTROL_SKIP) == 0 &&
                        dataloginfo->datalogsignals[i].Size <= byteavailable[ecm_index][index])
                    {                        
                        processcheck[i] = TRUE;
                        processedperloop++;
                        switch (dataloginfo->datalogsignals[i].PidType)
                        {
                            case PidTypeMode1:
                            case PidTypeRegular:
                                pidsignalcount++;
                                break;
                            case PidTypeDMR:                            
                                dataloginfo->dmrsignalindex[dmrsignalcount++] = i;
                                break;
                            default:
                                break;
                        }                        
                        // Mode 1 PIDs do not use packets 
                        if (dataloginfo->datalogsignals[i].PidType == PidTypeMode1)
                        {
                            continue;
                        }
                        // Position starts from 1
                        dataloginfo->datalogsignals[i].SignalType.Generic.Position = 1 + FORD_KWP_SCP_MAX_DATABYTE_PER_PACKET - byteavailable[ecm_index][index];
                        byteavailable[ecm_index][index] -= dataloginfo->datalogsignals[i].Size;
                        dataloginfo->datalogsignals[i].SignalType.Generic.PacketNumber = packetid;
                        packet = TRUE;                        
                    }
                }//for(i=...

                if (packet == TRUE)
                {
                    dataloginfo->packetidlist[ecm_index][dataloginfo->packetidcount[ecm_index]] = packetid;
                    dataloginfo->packetidcount[ecm_index]++;
                    index++;
                    packetid++;
                }                
                if ((pidsignalcount+dmrsignalcount+analogsignalcount) >= dataloginfo->datalogsignalcount)
                {
                    break;
                }
                else if (processedperloop == 0)
                {
                    break;
                }
            }//while(packetid...
            if ((pidsignalcount+dmrsignalcount+analogsignalcount) >= dataloginfo->datalogsignalcount)
            {
                break;
            }
        }//for(ecm_index=...
    }
    
    // Evaluate Non Rapid Packet Signals
    if (gDatalogmode.packetmode == SingleRate)
    {
        for(ecm_index=0;ecm_index<2;ecm_index++)
        {            
            for(i=0;i<dataloginfo->datalogsignalcount;i++)
            {                
                // Handle Non Rapid Packets
                if (dataloginfo->datalogsignals[i].EcmIndex == ecm_index &&
                    (dataloginfo->datalogsignals[i].Control & DATALOG_CONTROL_SKIP) == 0)
                {
                    switch (dataloginfo->datalogsignals[i].PidType)
                    {
                        case PidTypeRegular:
                        case PidTypeMode1:
                            pidsignalcount++;
                            break;
                        case PidTypeDMR:
                            dataloginfo->dmrsignalindex[dmrsignalcount++] = i;
                            break;
                        default:
                            break;
                    }
                }
            }//for(i=...
            if ((pidsignalcount+dmrsignalcount+analogsignalcount) >= dataloginfo->datalogsignalcount)
            {
                break;
            }
        }//for(ecm_index=...       
    }
        
    dataloginfo->pidsignalcount = pidsignalcount;
    dataloginfo->dmrsignalcount = dmrsignalcount;
    dataloginfo->analogsignalcount = analogsignalcount;
    
    if ((pidsignalcount+dmrsignalcount+analogsignalcount) < dataloginfo->datalogsignalcount)
    {
        //not enough packets to fit all signals
        return S_NOTFIT;
    }
    return S_SUCCESS;
}

//------------------------------------------------------------------------------
// Get datalog data in rapidpacket mode
// Input:   VehicleCommLevel commlevel[ECM_MAX_COUNT]
// Return:  u8  status
// Engineer: Quyen Leba
// Note: only use comlevel[0]
//------------------------------------------------------------------------------
u8 obd2scp_datalog_getdata(VehicleCommLevel *vehiclecommlevel)
{
    u8  data[16];
    u8  datalength;
    u8  i;
    u8  status;
    
    for(i=0;i<dataloginfo->packetidcount[0]+dataloginfo->packetidcount[1];i++)
    {
        status = obd2scp_rxraw(data, &datalength);
        if (status == S_SUCCESS)
        {
            //example:
            //data: 00 F0 10 6A 01 DA 34 46 00 00
            //[00 F0 10 6A] [packetid] [n:data]
            if (data[3] == 0x6A)
            {
                //because signal data position start at 1, the data passed into
                //this function includes packetid to offset it
                // ECM Index is always 0, no TCMs in SCP.
                obd2datalog_parserapidpacketdata(0,(u16)data[4],&data[4]);
                dataloginfo->lastdatapoint_timestamp = rtc_getvalue();
            }
        }
        else
        {
            //likely an S_TIMEOUT here, break out to prevent long wait loop
            break;
        }
    }//for(i=...
    
    if (rtc_getvalue() - dataloginfo->lastdatapoint_timestamp >=
        DATALOG_MAX_DATAPOINT_INTERVAL)
    {
        return S_COMMLOST;
    }
    else
    {
        return S_SUCCESS;
    }
}

//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
void obd2scp_datalog_sendtesterpresent()
{
    Obd2scp_ServiceData servicedata;

    obd2scp_servicedata_init(&servicedata);
    servicedata.service = FORD_TesterPresent;
    //servicedata.subservice = 0x80;
    servicedata.response_expected = FALSE;
    
    scp_txrx(&servicedata);
}

//------------------------------------------------------------------------------
// Clean up previous defined packets
// Return:  u8  status
// Engineer: Quyen Leba
//------------------------------------------------------------------------------
u8 obd2scp_datalog_cleanup_packets()
{
    u8  packetidlist[FORD_KWP_SCP_MAX_DATABYTE_PER_PACKET];
    u8  packetidlistcount;
    u8  status;
    u16 i;
    
    memset(packetidlist,0,sizeof(packetidlist));
    packetidlistcount = FORD_KWP_SCP_MAX_DATABYTE_PER_PACKET;
    for(i=FORD_KWP_SCP_PACKETID_START;i<=FORD_KWP_SCP_PACKETID_END;i++)
    {
        packetidlist[0] = i;
        status = obd2scp_request_diagnostic_data_packet(0x00,packetidlist,packetidlistcount);
        if (status != S_SUCCESS)
        {
            break;
        }
    }
    if (status == S_SUCCESS)
    {
        delays(300,'m');    //give ECM time to clean up
    }
    return status;
}

//------------------------------------------------------------------------------
// Initiate pids into packets
// Input:   VehicleCommLevel commlevel[ECM_MAX_COUNT]
// Return:  u8  status
// Engineer: Quyen Leba
// Note: only use commlevel[0]
//------------------------------------------------------------------------------
u8 obd2scp_datalog_initiatepackets(VehicleCommLevel *vehiclecommlevel)
{
    MemoryAddressType addrtype;
    u16 packetid;
    u16 i;
    u8  setup_pid_count;        // This counts successfully setup PIDs
    u8  status;
    
    if (dataloginfo->commtype[0] == CommType_SCP32)
    {
        //TODOQ: right now, datalog don't detect actual CommType_SCP32 yet
        addrtype = Address_32_Bits;
    }
    else if (dataloginfo->commtype[0] == CommType_SCP)
    {
        addrtype = Address_24_Bits;
    }
    else
    {
        return S_COMMTYPE;
    }

    setup_pid_count = 0;
    
    for(packetid=FORD_KWP_SCP_PACKETID_START;packetid<=FORD_KWP_SCP_PACKETID_END;packetid++)
    {
        for(i=0;i<dataloginfo->datalogsignalcount;i++)
        {
            if (dataloginfo->datalogsignals[i].SignalType.Generic.PacketNumber == packetid)
            {
                delays(30,'m');
                status = obd2scp_dynamicallydefinediagnosticdatapacket
                    (packetid,
                     (PidType)dataloginfo->datalogsignals[i].PidType,
                     dataloginfo->datalogsignals[i].Size,
                     dataloginfo->datalogsignals[i].SignalType.Generic.Position,
                     dataloginfo->datalogsignals[i].Address,
                     addrtype);
                if (status == S_SUCCESS)
                {
                     setup_pid_count++;                     
                }
                else
                {
                    // Mark this signal invalid
                    dataloginfo->datalogsignals[i].SignalType.Generic.PacketNumber = 0xFF;
                }
            }
        }//for(i=...
    }//for(packetid=...    
    
    // If no pids setup, fail
    if(setup_pid_count == 0)
    {
        return S_FAIL;
    }    
    return S_SUCCESS;
}

//------------------------------------------------------------------------------
// Test if ignition key is on using download related command
// Input:   u16 ecm_type
// Return:  u8  status
// Engineer: Quyen Leba
//------------------------------------------------------------------------------
u8 obd2scp_download_test_key_on(u16 ecm_type)
{
    u8  l_seed;
    u8  status;

    status = obd2scp_unlockecm(ecm_type, TRUE, &l_seed);    //peek only
    if (status == S_ERROR)
    {
        //responded to unlock
        //could be S_ERROR because of no FEPS
        status = S_SUCCESS;
    }
    return status;
}

/**
 *  @brief Get SCP OBD2 Info
 *  
 *  @param [in] obd2info OBD2 info structure
 *  @return status
 *  
 *  @details This function identifies and returns if applicable SCP information
 *           to the obd2info structure.
 */
u8 obd2scp_getinfo(obd2_info *obd2info)
{
    VehicleCommType commtype;
    u8 strategy[40];
    u8 vin[VIN_LENGTH+1];
    u8 status;

    if (obd2info == NULL)
    {
        return S_INPUT;
    }

    obd2scp_init(Obd2scpType_24Bit);
    status = obd2scp_ping();
    if (status == S_SUCCESS)
    {        
        status = obd2scp_readstrategy(strategy, &commtype);
        if (status == S_SUCCESS)
        {
            /* Main OBD2 Info */
            obd2info->oemtype = OemType_FORD;
            obd2scp_readvin(vin, &commtype);
            memcpy(obd2info->vin, vin, sizeof(vin));
            
            /* OBD2 Info Block */
            obd2info->ecu_block[obd2info->ecu_count].ecutype = EcuType_ECM;
            obd2info->ecu_block[obd2info->ecu_count].commtype = commtype;
            obd2info->ecu_block[obd2info->ecu_count].commlevel = CommLevel_KWP2000;
            obd2info->ecu_block[obd2info->ecu_count].genericcommlevel = GenericCommLevel_Legacy;
            /* block.ecu_id <- N/A */
            
            /* OBD2 Part Numbers */
            obd2info->ecu_block[obd2info->ecu_count].partnumber_count = 1;
            memcpy(obd2info->ecu_block[obd2info->ecu_count].partnumbers.ford.strategy, strategy, 
                   sizeof(obd2info->ecu_block[obd2info->ecu_count].partnumbers.ford.strategy));

            obd2info->ecu_count++;
        }
    }
    return status;
}
