/**
  **************** (C) COPYRIGHT 2011 SCT Performance, LLC *********************
  * File Name          : veh_defs.h
  * Device / System    : SCT products using the "CommonCode" platform
  * Author             : Quyen Leba
  *
  * Version            : 1 
  * Date               : 05/02/2011
  * Description        : 
  *                    : 
  *
  *
  * History            : 05/02/2011 M. Davis
  *                    :   Add SCT Copyright header,
  *
  ******************************************************************************
  */

#ifndef __VEH_DEFS_H
#define __VEH_DEFS_H

#include <arch/gentype.h>
#include <fs/genfs.h>
#include <common/obd2def.h>

#define MAX_UPLOAD_BUFFER_LENGTH        0x1000
#define MAX_DOWNLOAD_BUFFER_LENGTH      0x1000

#define UPLOAD_CMD_MEMORY_SIZE          MemorySize_2_Byte
#define DEFAULT_CMD_MEMORY_SIZE         MemorySize_2_Byte

#define INVALID_VEH_DEF     0xFFFF
#define INVALID_ECM_DEF     0xFFFF
#define INVALID_ECM_ID      0
#define INVALID_UTI_INDEX   0xFFFF

#define ECM_DEFS_INVALID_VID_ADDRESS    0xFFFFFFFF
#define ECM_DEFS_INVALID_VIN_ADDRESS    0xFFFFFFFF

#define MAX_ECM_MEMORY_BLOCKS           5
#define INVALID_MEMBLOCK_SIZE           0xFFFF

#define MAX_UTILITY_MEMORY_BLOCKS       5
#define MAX_UTILITY_OPTIONS             3
#define MAX_UTILITY_STORED              (MAX_UTILITY_OPTIONS*ECM_MAX_COUNT)
#define UTILITY_CHECKID_INVALID         0xFF
#define UTILITY_ADDRESS_INVALID         0xFFFFFFFF

#define EraseCmdInfoFlags_None  0
#define EraseCmdInfoCount0      0
#define EraseCmdInfoCount1      1
#define EraseCmdInfoCount2      2
#define EraseCmdInfoCount3      3

#define VDF_FILE_VERSION    1

enum
{
    UnlockId_None               = 0,
#ifdef __FORD_MANUF__
    UnlockId_Regular            = 1,
    UnlockId_64L                = 2,
    UnlockId_67L                = 3,
    UnlockId_Bosch6R            = 4,
    UnlockId_Conti              = 5,
    UnlockId_Siemens_UTCU       = 6,
    UnlockId_ME9_UK             = 7,
    UnlockId_OSC                = 8,
    UnlockId_Diag               = 9,
#endif
#ifdef __DCX_MANUF__

    UnlockId_HEMI_BOOTLOADER_SECURITY_UPLOAD1_63     = 1,
    UnlockId_HEMI_BOOTLOADER_SECURITY_DOWNL1_05      = 2,
    UnlockId_HEMI_APP_SECURITY_ACCESS1_03            = 3,
    UnlockId_HEMI_APP_SECURITY_ACCESS1_65            = 4,
    UnlockId_SCIB_UNLOCK                             = 5,
    UnlockId_JTECP_UNLOCK                            = 6,
    UnlockId_HEMI_BOOTLOADER_SECURITY_UPLOAD2_63     = 7,
    UnlockId_HEMI_BOOTLOADER_SECURITY_UPLOAD3_63     = 8,
    UnlockId_HEMI_BOOTLOADER_SECURITY_DOWNL2_05      = 9,
    UnlockId_HEMI_APP_SECURITY_ACCESS2_65            = 10,
    UnlockId_HEMI_BOOTLOADER_SECURITY_UPLOAD4_63     = 11,
    UnlockId_HEMI_APP_SECURITY_ACCESS3_65            = 12,
    UnlockId_HEMI_APP_SECURITY_ACCESS4_65            = 13,

#endif
};

enum
{
    ChecksumId_None             = 0x0000,  
    ChecksumId_64L              = 0x0001,
    ChecksumId_6R               = 0x0002,
    ChecksumId_AU_ZF            = 0x0003,
    ChecksumId_67L              = 0x0004,
    ChecksumId_BoschTricore     = 0x0005,
    ChecksumId_Conti            = 0x0006,
    ChecksumId_67L_2013UP       = 0x0007,
    ChecksumId_ME9_UK           = 0x0008,
};

enum
{
    FinalId_None                  = 0x0000,
    FinalId_SendControlRoutine31  = 0x0001,
};

typedef struct
{
    u8  flags;
    u8  count;
    u8  data[6];
}EcuEraseCmdInfo;

typedef struct
{
    u32 start_addr;             /* Upload/download address */
    u32 block_size;             /* Length */
    u32 flags;                  /* Extra tasks after upload/download */
    u32 flagdata;
}EcuMemBlockInfo;

typedef u8 (funcptr_utilfunctest)(u16 ecm_type);

typedef enum
{
    UtilTypeLegacy      = 0x00,
    UtilTypeUpload      = 0x01,
    UtilTypeDownload    = 0x02,
    UtilTypeBoth        = 0x03,
} UtilityType;

typedef struct
{
    u16 version;
    u16 reserved0;
    u32 headerCRC32E;
    u32 contentCRC32E;
    u32 flags;
    u16 oemtype;
    u16 minvehdef;
    u16 maxvehdef;
    u16 reserved1;
    u32 VehicleTypeRecordOffset;
    u32 VehicleTypeRecordCount;
    u32 EcmTypeRecordOffset;
    u32 EcmTypeRecordCount;
    u32 UtilityInfoRecordOffset;
    u32 UtilityInfoRecordCount;
    u8  reserved2[16];
}VehicleTypeHeader;
STRUCT_SIZE_CHECK(VehicleTypeHeader,64);

typedef struct
{
    char description[64];
    u16 vehicle_type;
    u16 reserved;
    u32 flags;
    u16 ecm_types[ECM_MAX_COUNT];
    u8  reserved2[50];
}VehicleTypeBlock;
STRUCT_SIZE_CHECK(VehicleTypeBlock,128);

typedef struct
{
    char description[64];
    u16 index;
    u8  reserved_0[2];
    u32 ecm_id;                     /* ECU Address (0x7E0, etc) */
    u32 reponse_ecm_id;
    u16 unlock_flags;
    u16 upload_unlock_id;           /* Determine how to unlock this ECU */
    u16 download_unlock_id;
    u16 checksum_id;                /* Determine how to checksum this ECU */
    u16 finalizing_id;              /* Determine how to checksum this ECU */
    VehicleCommType commtype;       /* 1-Byte */
    VehicleCommLevel commlevel;     /* 1-Byte */
    u32 flags;
    u32 extended_flags;
    EcuEraseCmdInfo eraseall_cmd;   /* 8-Bytes */
    EcuEraseCmdInfo erasecal_cmd;   /* 8-Bytes */
    u8  testerpresent_frequency;    /* How often to send tester present */
    u8  reserved_1[3];
    u32 vid_address;
    u32 pats_address;
    u32 vin_address;                /* Use in VIN search w/ mem read ($23) */
    u32 data_offset;
    u32 ecm_startingaddr;           /* ECU_Addr; what is this? only used by VPW */
    u16 util_upload[MAX_UTILITY_OPTIONS];   /* 2-Bytes x 3 */
    u16 util_dnload[MAX_UTILITY_OPTIONS];   /* 2-Bytes x 3 */
    u8  reserved_2[2];
    u16 os_test_method;             /* Used to check if OS reflash is required */
    u16 maxuploadblocksize;         /* Max bytes can upload per block */
    u16 maxdownloadblocksize;       /* Max bytes can download per block */
    EcuMemBlockInfo os_block_read[MAX_ECM_MEMORY_BLOCKS];   /* 16-Bytes x 5 */
    EcuMemBlockInfo os_block[MAX_ECM_MEMORY_BLOCKS];        /* 16-Bytes x 5 */
    EcuMemBlockInfo cal_block[MAX_ECM_MEMORY_BLOCKS];       /* 16-Bytes x 5 */
    u8 reserved_3[116];
}EcmTypeBlock;
STRUCT_SIZE_CHECK(EcmTypeBlock,512);

typedef struct
{
    char description[64];
    u16 index;
    u8  reserved[2];
    u8  filename[26];
    u16 flags;
    u32 call_address;
    EcuMemBlockInfo utility_block[MAX_UTILITY_MEMORY_BLOCKS];   /* 16-Bytes x 5 */
    u8 reserved_2[76];
}UtilityTypeBlock;
STRUCT_SIZE_CHECK(UtilityTypeBlock,256);

typedef struct
{
    u16 vehdefmin;
    u16 vehdefmax;
    u16 vehdefcount;
    u16 ecmdefcount;
    u16 utidefcount;
    VehicleTypeBlock VehBlock;
    EcmTypeBlock EcmBlock;
    UtilityTypeBlock UtiBlock;
}VehDefLastBlock;

typedef enum
{
    VEH_minvehdef,
    VEH_maxvehdef,
    VEH_veh_def_count,
    VEH_ecm_def_count,
    VEH_uti_def_count,
    VEH_flags,
    VEH_ecm_types
}VEH_Param;

/******************************************************************************/
/* Definition for vehicle def flags */
/******************************************************************************/
#define VEH_DEF_FLAG_NONE                               (0)
/* Bit[0] */
#define UNLOCK_ALL_ECMS_FIRST                           (1 << 0)
/* Bit[1] */
#define SKIP_RECYCLE_KEY_ON                             (1 << 1)
/* Bit[2] */
#define REQUIRE_WAKEUP                                  (1 << 2)
/* Bit[3] */
#define WAKEUP_CHECK_ONCE                               (1 << 3)
/* Bit[4] */
#define SIMPLE_SETMARRIED                               (1 << 4)
/* Bit[5] */
#define REQUIRE_OVERLAY                                 (1 << 5)
/* Bit[6]: Depends on Bit[2] REQUIRE_WAKEUP */
#define CHECK_KEYON_PRIOR_WAKEUP                        (1 << 6)
/* Bit[7]: Should only used in development */
#define ALLOW_BLANK_VIN                                 (1 << 7)
/* Bit[8] */
#define CUSTOM_TUNE_TUNEOS_FILE_NOT_REQUIRED            (1 << 8)
/* Bit[9]: Reads Vehicle Info After Download to Verify Success */
#define CHECK_SUCCESS_AFTER_DOWNLOAD                    (1 << 9)
/* Bit[10]: When TUNEOS codes do not match whats read from vehicle, Full Flash 
 *          is required, suppress Full Flash message.
 */
#define SUPPRESS_FULL_FLASH_MESSAGE                     (1 << 10)
/* Bit[11]: Prompt for approval to skip flashing TCM. Currently used to skip ZF
 *          TCM. TODOT: We only need this until segment flashing.
 */
#define APPROVE_SKIP_FLASH_TCM                          (1 << 11)
/* Bit[12]: Special checks to be done before uploading/downloading to ECU. 
 *          DCX Firmware uses this to check for original/mod bootloader hash.
 */
#define PRE_DOWNLOAD_CHECK                              (1 << 12)
/* Bit[29]: In case TCM is unsupported, set this flag will make unsupported
 *          error, i.e.: without this flag, user can choose to skip program TCM
 */
#define SUB_ECM_REQUIRED                                (1 << 29)
/* Bit[30]: In case TCM is unsupported and allow PCM only */
#define ALLOW_SKIP_UNSUPPORTED_SUB_ECM                  (1 << 30)

/******************************************************************************/
/* Definition for vehicle def macros */
/******************************************************************************/
#define vehvar(vt,var)                          vehdef_vehvar(vt,var)

#define VEH_GetMinVehType()                     (*((u16*)vehvar(NULL,VEH_minvehdef)))
#define VEH_GetMaxVehType()                     (*((u16*)vehvar(NULL,VEH_maxvehdef)))
#define VEH_GetVehTypeCount()                   (*((u16*)vehvar(NULL,VEH_veh_def_count)))
#define VEH_GetEcmDefCount()                    (*((u16*)vehvar(NULL,VEH_ecm_def_count)))
#define VEH_GetUtiDefCount()                    (*((u16*)vehvar(NULL,VEH_uti_def_count)))     
#define VEH_GetFlags(vt)                        (*((u32*)vehvar(vt,VEH_flags)))
#define VEH_GetEcmType(vt,i)                    (((u16*)vehvar(vt,VEH_ecm_types))[i])
#define VEH_GetEcmCount(vt)                     vehdef_vehecmcount(vt)
#define VEH_GetMainEcmIndex(vt)                 0 /* Main Index is always 0 */
#define VEH_GetMainEcmType(vt)                  (VEH_GetEcmType(vt,VEH_GetMainEcmIndex(vt)))

/* Flags */
#define VEH_IsRequireUnlockAll(vt)              (VEH_GetFlags(vt) & UNLOCK_ALL_ECMS_FIRST)
#define VEH_IsSkipRecycleKeyOn(vt)              (VEH_GetFlags(vt) & SKIP_RECYCLE_KEY_ON)
#define VEH_IsRequireWakeUp(vt)                 (VEH_GetFlags(vt) & REQUIRE_WAKEUP)
#define VEH_IsWakeUpCheckOnce(vt)               (VEH_GetFlags(vt) & WAKEUP_CHECK_ONCE)
#define VEH_IsSimpleSetMarried(vt)              (VEH_GetFlags(vt) & SIMPLE_SETMARRIED)
#define VEH_IsRequireOverlay(vt)                (VEH_GetFlags(vt) & REQUIRE_OVERLAY)
#define VEH_IsCheckKeyOnPriorWakeup(vt)         (VEH_GetFlags(vt) & CHECK_KEYON_PRIOR_WAKEUP)
#define VEH_IsAllowBlankVIN(vt)                 (VEH_GetFlags(vt) & ALLOW_BLANK_VIN)
#define VEH_IsCustomTuneTuneOSNotRequired(vt)   (VEH_GetFlags(vt) & CUSTOM_TUNE_TUNEOS_FILE_NOT_REQUIRED)
#define VEH_IsCheckSuccessAfterDownload(vt)     (VEH_GetFlags(vt) & CHECK_SUCCESS_AFTER_DOWNLOAD)
#define VEH_IsSuppressFullFlashMessage(vt)      (VEH_GetFlags(vt) & SUPPRESS_FULL_FLASH_MESSAGE)
#define VEH_IsApproveSkipFlashTCM(vt)           (VEH_GetFlags(vt) & APPROVE_SKIP_FLASH_TCM)
#define VEH_IsCheckPreDownload(vt)              (VEH_GetFlags(vt) & PRE_DOWNLOAD_CHECK)
#define VEH_IsSubECMRequired(vt)                (VEH_GetFlags(vt) & SUB_ECM_REQUIRED)
#define VEH_IsAllowSkipUnsupportedSubECM(vt)    (VEH_GetFlags(vt) & ALLOW_SKIP_UNSUPPORTED_SUB_ECM)

/******************************************************************************/
/* Definition for OEM specific vehicle def macros */
/******************************************************************************/

#define VEH_FORD_Is60L_PCM(et)                      ((et==5)  || (et==15))
#define VEH_FORD_Is67L_PCM(et)                      ((et==35) || (et==47))

#define VEH_IsVehicleType_Ford_60L(vt)              ((vt==12) || (vt==13))
#define VEH_IsVehicleType_Ford_64L(vt)              ((vt==27) || (vt==43))
#define VEH_IsVehicleType_Ford_67L(vt)              ((vt==53) || (vt==57) || (vt==58) || (vt==60) || (vt==61) || (vt==73) || (vt==74))
#define VEH_IsVehicleType_Ford_216K(vt)             (vt==5)
#define VEH_IsVehicleType_Ford_F150Ecoboost35L(vt)  (vt==59)

#define VEH_IsVehicleType_GM_LB7(vt)                ((vt==12) || (vt==13))
#define VEH_IsVehicleType_GM_LLY(vt)                ((vt==12) || (vt==13))
#define VEH_IsVehicleType_GM_LBZ(vt)                ((vt==12) || (vt==13))
#define VEH_IsVehicleType_GM_LMM(vt)                ((vt==12) || (vt==13))

/******************************************************************************/
/* Definition for OEM specific misc macros */
/******************************************************************************/

#define IsVariantId_DCX_GPEC2       (SETTINGS_TUNE(secondvehiclecodes[0] >= 0x23))
     
/******************************************************************************/
/* Function prototypes */
/******************************************************************************/
void vehdef_init(void);
u8 vehdef_vehecmcount(u16 veh_type);
u32 vehdef_vehvar(u16 veh_type, u16 parameter);
u32 vehdef_ecmvar(u16 ecm_type, u16 parameter);
u32 vehdef_utivar(u16 ecm_type, u8 utilitychoiceindex, u16 parameter);
u8 vehdef_get_vehdef_info(u16 *veh_type_in, u8 veh_type_in_count, u16 *veh_type_out);
u8 vehdef_check_vdf();

#endif  /* __VEH_DEFS_H */
