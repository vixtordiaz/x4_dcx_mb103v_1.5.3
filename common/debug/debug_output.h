/**
  **************** (C) COPYRIGHT 2011 SCT Performance, LLC *********************
  * File Name          : debug_output.h
  * Device / System    : SCT products using the "CommonCode" platform
  * Author             : Patrick Downs
  *
  * Version            : 1 
  * Date               : 05/23/2011
  * Description        : 
  *                    : 
  *
  *
  * History            : 05/23/2011 P. Downs
  *                    :   Created
  *
  ******************************************************************************
  */
#ifndef __DEBUG_H
#define __DEBUG_H

//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
int debug_init(void);
void debug_out(u8* outputstring);
void debug_console(void);
void debug_reportprogress(int progress, u8* outputstring);
void debug_elapsedtime_start();
void debug_elapsedtime_stop();
void debug_reportstatus(u8 status, u8* outputstring);

#endif // DEBUG 
//------------------------------------------------------------------------------
// EOF
//------------------------------------------------------------------------------