/**
  **************** (C) COPYRIGHT 2011 SCT Performance, LLC *********************
  * File Name          : opg2oph.h
  * Device / System    : SCT products using the "CommonCode" platform
  * Author             : Quyen Leba
  *
  * Version            : 1 
  * Date               : 05/02/2011
  * Description        : 
  *                    : 
  *
  *
  * History            : 05/02/2011 M. Davis
  *                    :   Add SCT Copyright header,
  *
  ******************************************************************************
  */

#ifndef __OPG2OPH_H
#define __OPG2OPH_H

#include <arch/gentype.h>

#endif    //__OPG2OPH_H
