/**
  **************** (C) COPYRIGHT 2011 SCT Performance, LLC *********************
  * File Name          : opg2oph.c
  * Device / System    : SCT products using the "CommonCode" platform
  * Author             : Quyen Leba
  *
  * Version            : 1 
  * Date               : 05/02/2011
  * Description        : 
  *                    : 
  *
  *
  * History            : 05/02/2011 M. Davis
  *                    :   Add SCT Copyright header,
  *
  ******************************************************************************
  */

#include <ctype.h>
#include <common/statuscode.h>
#include <common/crc32.h>
#include <fs/genfs.h>
#include <common/obd2tune.h>
#include "opg2oph.h"

//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
void wordswap(u32 *input, u32 *output)
{
    u8  *sptr = (u8*)input;
    u8  *dptr = (u8*)output;
    
    dptr[0] = sptr[3];
    dptr[1] = sptr[2];
    dptr[2] = sptr[1];
    dptr[3] = sptr[0];
}

//------------------------------------------------------------------------------
// Convert option file format from OPG to OPH
// Inputs:  u8 *opg_filename
//          u8 *oph_filename
// Return:  u8  status
// Engineer: Quyen Leba
//------------------------------------------------------------------------------
u8 opg2oph(u8 *opg_filename, u8 *oph_filename)
{
    F_FILE *opgfptr;
    F_FILE *ophfptr;
    ophOptionFileHeader ophHeader;
    ophOptionItemInfoBlock ophOptionItem;
    ophChoiceItemInfoBlock ophOptionChoice;
    ophOptionItemRecord_Generic ophGenericRecord;
    u8  buffer[512];
    u32 bytecount;
    u32 current_opg_offset;
    u32 filecontent_crc32e;
    u32 *wptr;
    u32 i;
    
    crc32e_reset();
    filecontent_crc32e = 0xFFFFFFFF;
//    filecontent_crc32e = crc32e_calculateblock(filecontent_crc32e,buf,len/4);
    
    opgfptr = genfs_general_openfile(opg_filename,"r");
    if (opgfptr == NULL)
    {
        return S_OPENFILE;
    }
    ophfptr = genfs_general_openfile(oph_filename,"w");
    if (ophfptr == NULL)
    {
        return S_OPENFILE;
    }
    
    bytecount = fread((char*)buffer,1,6,opgfptr);
    current_opg_offset = 6;
    
    memset((char*)&ophHeader,0,sizeof(ophHeader));
    ophHeader.version = 0;
    ophHeader.header_crc32e = 0x55555555;   //do not calc with me
    ophHeader.content_crc32e = 0xAAAAAAAA;
    ophHeader.flags = 0;
    ophHeader.header_length = sizeof(ophHeader);
    ophHeader.firstchoice_offset = 0xAABBCCDD;  //
    ophHeader.firstrecord_offset = 0x12345678;  //
    ophHeader.option_count = buffer[5];
    
    bytecount = fwrite((char*)&ophHeader,1,sizeof(ophHeader),ophfptr);
    
    //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    
    u32 jump_1 = 0;
    u32 start_of_first_oph_choice = sizeof(ophHeader) + 
            ophHeader.option_count * sizeof(ophOptionItem);
    u32 start_of_fisrt_opg_record = 6;
    u32 start_of_first_oph_record = sizeof(ophHeader);
    u32 total_BLOCK_3_length = 0;
    for(i=0;i<ophHeader.option_count;i++)
    {
        u32 content_offset;

        //get <BLOCK 2>
        bytecount = fread((char*)buffer,1,22,opgfptr);
        current_opg_offset += 22;

        content_offset = sizeof(ophHeader) + 
            ophHeader.option_count * sizeof(ophOptionItem) + jump_1;
        
        memset((char*)&ophOptionItem,0,sizeof(ophOptionItem));
        ophOptionItem.type = 0;
        ophOptionItem.choice_count = buffer[0];
        ophOptionItem.choice_default = buffer[1];
        ophOptionItem.flags = 0;
        ophOptionItem.choicecontent_offset = content_offset;
        memcpy((char*)&ophOptionItem.description,(char*)&buffer[2],16);
        
        jump_1 += (ophOptionItem.choice_count * sizeof(ophChoiceItemInfoBlock));
        
        start_of_first_oph_record += sizeof(ophOptionItem);
        start_of_first_oph_record += (ophOptionItem.choice_count * sizeof(ophChoiceItemInfoBlock));
        
        start_of_fisrt_opg_record += 22;
        start_of_fisrt_opg_record += (ophOptionItem.choice_count * (60+4)) + 6;
        total_BLOCK_3_length += (ophOptionItem.choice_count * (60+4)) + 6;
        
        bytecount = fwrite((char*)&ophOptionItem,1,sizeof(ophOptionItem),ophfptr);
        filecontent_crc32e = crc32e_calculateblock(filecontent_crc32e,
                                                   (u32*)&ophOptionItem,
                                                   sizeof(ophOptionItem)/4);
    }
    
    while(total_BLOCK_3_length > 0)
    {
        u8  number_of_choices;
        u32 changes_offset;
        u32 choicedata_offset;
        u32 recordcount;

        //get <BLOCK 3A>
        bytecount = fread((char*)buffer,1,6,opgfptr);
        total_BLOCK_3_length -= 6;
        number_of_choices = buffer[5];
        
        for(i=0;i<number_of_choices;i++)
        {
            //get <BLOCK 3B>
            bytecount = fread((char*)buffer,1,64,opgfptr);
            total_BLOCK_3_length -= 64;
            wordswap((u32*)&buffer[60],&changes_offset);
            choicedata_offset = changes_offset - start_of_fisrt_opg_record;
            if ((choicedata_offset % 27) == 0)
            {
                recordcount = choicedata_offset / 27;
                choicedata_offset = recordcount * sizeof(ophOptionItemRecord_Generic);
                choicedata_offset += start_of_first_oph_record;
            }
            else
            {
                return S_FAIL;
            }

            ophOptionChoice.flags = 0;
            ophOptionChoice.choicedata_offset = choicedata_offset;
            memcpy((char*)&ophOptionChoice.description,(char*)buffer,56);
            ophOptionChoice.description[55] = NULL;
            
            bytecount = fwrite((char*)&ophOptionChoice,1,sizeof(ophOptionChoice),ophfptr);
            filecontent_crc32e = crc32e_calculateblock(filecontent_crc32e,
                                                   (u32*)&ophOptionChoice,
                                                   sizeof(ophOptionChoice)/4);
        }
    }

    //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    
    struct
    {
        u8  change_type;
        u32 address;
        u8  length;
        u8  sign;
        float decmial_offset;
        float multiplier;
        float adjustment;
        float min;
        float max;
    }change_block;
    union
    {
        u8  hex[4];
        float fvalue;
        unsigned int uint32;
        signed int sint32;
    }fvalue;
    while(!feof(opgfptr))
    {
        bytecount = fread((char*)buffer,1,27,opgfptr);
        change_block.change_type = buffer[0];
        // Address of data in image
        change_block.address = 
            ((u32)buffer[4]) | (((u32)buffer[3]) << 8) |
            (((u32)buffer[2]) << 16) | (((u32)buffer[1]) << 24);
        change_block.length = buffer[5];
        change_block.sign = buffer[6];
        
        fvalue.hex[3] = buffer[7];
        fvalue.hex[2] = buffer[8];
        fvalue.hex[1] = buffer[9];
        fvalue.hex[0] = buffer[10];
        change_block.decmial_offset = fvalue.fvalue;
        
        fvalue.hex[3] = buffer[11];
        fvalue.hex[2] = buffer[12];
        fvalue.hex[1] = buffer[13];
        fvalue.hex[0] = buffer[14];
        change_block.multiplier = fvalue.fvalue;
        
        fvalue.hex[3] = buffer[15];
        fvalue.hex[2] = buffer[16];
        fvalue.hex[1] = buffer[17];
        fvalue.hex[0] = buffer[18];
        change_block.adjustment = fvalue.fvalue;
        
        fvalue.hex[3] = buffer[19];
        fvalue.hex[2] = buffer[20];
        fvalue.hex[1] = buffer[21];
        fvalue.hex[0] = buffer[22];
        change_block.min = fvalue.fvalue;
        
        fvalue.hex[3] = buffer[23];
        fvalue.hex[2] = buffer[24];
        fvalue.hex[1] = buffer[25];
        fvalue.hex[0] = buffer[26];
        change_block.max = fvalue.fvalue;
        
        memset((char*)&ophGenericRecord,0,sizeof(ophGenericRecord));
        ophGenericRecord.flags = 0;
        ophGenericRecord.address = change_block.address;
        ophGenericRecord.opcode = tolower(change_block.change_type);
        ophGenericRecord.length = change_block.length;
        ophGenericRecord.offset = change_block.decmial_offset;
        ophGenericRecord.multiplier = change_block.multiplier;
        ophGenericRecord.adjustment = change_block.adjustment;
        ophGenericRecord.min = change_block.min;
        ophGenericRecord.max = change_block.max;
        
        //assume ECU use BIG endian
        ophGenericRecord.flags |= ((u32)1<<3);
        if (change_block.sign)
        {
            ophGenericRecord.flags |= ((u32)1<<4);
        }
        switch(change_block.length)
        {
        case 1:
            //assume int
            ophGenericRecord.flags |= 1;
            break;
        case 2:
            //assume int
            ophGenericRecord.flags |= 1;
            break;
        case 4:
            //leave it float (0)
            break;
        default:
            //must not happen
            break;
        }
        
        bytecount = fwrite((char*)&ophGenericRecord,1,sizeof(ophGenericRecord),ophfptr);
        filecontent_crc32e = crc32e_calculateblock(filecontent_crc32e,
                                                   (u32*)&ophGenericRecord,
                                                   sizeof(ophGenericRecord)/4);
    }
    
    ophHeader.firstchoice_offset = start_of_first_oph_choice;
    ophHeader.firstrecord_offset = start_of_first_oph_record;
    
    ophHeader.content_crc32e = filecontent_crc32e;
    crc32e_reset();
    ophHeader.header_crc32e = 0xFFFFFFFF;
    ophHeader.header_crc32e = crc32e_calculateblock(ophHeader.header_crc32e,
                                                   (u32*)&ophHeader,1);
    wptr = (u32*)((u8*)&ophHeader + 8);
    ophHeader.header_crc32e = crc32e_calculateblock(ophHeader.header_crc32e,
                                                   wptr,
                                                   (sizeof(ophHeader)-8)/4);
    fseek(ophfptr,0,SEEK_SET);
    bytecount = fwrite((char*)&ophHeader,1,sizeof(ophHeader),ophfptr);
    
    genfs_closefile(ophfptr);
    genfs_closefile(opgfptr);
    
    return S_SUCCESS;
}
