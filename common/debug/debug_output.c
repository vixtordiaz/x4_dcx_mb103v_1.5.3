/**
**************** (C) COPYRIGHT 2011 SCT Performance, LLC *********************
* File Name          : debug_output.c
* Device / System    : SCT products using the "CommonCode" platform
* Author             : Patick Downs
*
* Version            : 1 
* Date               : 05/23/2011
* Description        : 
*                    : 
*
*
* History            : 05/23/2011 P. Downs
*                    :   Created
*
******************************************************************************
*/

#include <string.h>
#include <stdio.h>
#include <board/genplatform.h>
#include <board/commlink.h>
#include <board/rtc.h>
#include <common/genmanuf_overload.h>
#include <common/statuscode.h>
#include <common/genmanuf_overload.h>
#include <common/obd2.h>
#include <common/obd2scp.h>
#include <common/obd2datalog.h>
#include <common/itoa.h>
#include <common/filestock.h>
#include <common/obd2tune_upload.h>
#include <common/obd2tune_download.h>
#include <common/ess_file.h>
#include "debug_output.h"

#ifdef __FORD_DEVICE__
extern u8 obd2tune_ford_checksum_64L(u16 ecm_type, u32 offset);
#endif

typedef struct
{
  char commandname[30]; // Text command that will be used in debug console
  void  (*funcptr)(void);   // Pointer to function to be called
}DEBUG_CONSOLE_COMMAND;

// Adding commmands:
// Step 1 - Define a function to be called, must conform to "void function(void)". 
//          Handle any additional data parameter passing inside the function.
// Step 2 - Add private prototype here, unless needs to be public
// Step 3 - Add new entry to "commandlist" definition with commandname keyword

void debug_do_command(int selcmd);
void debug_Cmd_Help(void);
void debug_Cmd_BtCommandPassthru(void);
void debug_Cmd_ReadStrategy(void);
void debug_Cmd_Test(void);
void debug_Cmd_SetBTBaud(void);
void debug_Cmd_SetBTBaudDefault(void);
void debug_Cmd_SetBTBaud115kPin(void);
void debug_Cmd_ValidateApp(void);
void debug_Cmd_ValidateBoot(void);
void debug_Cmd_SetUartBaud(void);
void debug_Cmd_Sniff(void);
void debug_Cmd_IpcTest(void);
void debug_Cmd_Checksum64Test(void);
void debug_Cmd_UploadStock(void);
void debug_Cmd_DownloadTune(void);
void debug_Cmd_BtLoopBack(void);
void debug_Cmd_Checksums(void);
//void debug_Cmd_ChangeDatalogInterval(void);

// Comand list, define commands here
const DEBUG_CONSOLE_COMMAND commandlist[] = 
{
    {
        .commandname = "help",
        .funcptr = &debug_Cmd_Help,
    },
    {
        .commandname = "btcmdpt",
        .funcptr = &debug_Cmd_BtCommandPassthru,
    },
    {
        .commandname = "readstrat",
        .funcptr = &debug_Cmd_ReadStrategy,
    },
    {
        .commandname = "btbaud",
        .funcptr = &debug_Cmd_SetBTBaud,
    },
    {
        .commandname = "btbauddef",
        .funcptr = &debug_Cmd_SetBTBaudDefault,
    },
    {
        .commandname = "btbaudpin",
        .funcptr = &debug_Cmd_SetBTBaud115kPin,
    },
    {
        .commandname = "uartbaud",
        .funcptr = &debug_Cmd_SetUartBaud,
    },
    {
        .commandname = "validateapp",
        .funcptr = &debug_Cmd_ValidateApp,
    },
    {
        .commandname = "validateboot",
        .funcptr = &debug_Cmd_ValidateBoot,
    },
    {
        .commandname = "test",
        .funcptr = &debug_Cmd_Test,
    },
    {
        .commandname = "sniff",
        .funcptr = &debug_Cmd_Sniff,
    },
    {
        .commandname = "ipc",
        .funcptr = &debug_Cmd_IpcTest,
    },
    {
        .commandname = "checksum64",
        .funcptr = &debug_Cmd_Checksum64Test,
    },
    {
        .commandname = "download",
        .funcptr = &debug_Cmd_DownloadTune,
    },
    {
        .commandname = "btloopback",
        .funcptr = &debug_Cmd_BtLoopBack,
    },
    {
        .commandname = "upload",
        .funcptr = &debug_Cmd_UploadStock,
    },
    {
        .commandname = "checksums",
        .funcptr = &debug_Cmd_Checksums,
    },
//    {
//        .commandname = "dlint",
//        .funcptr = &debug_Cmd_ChangeDatalogInterval,
//    },
};

#define COMMAND_COUNT (sizeof(commandlist)/sizeof(DEBUG_CONSOLE_COMMAND))


#ifdef __DEBUG_JTAG_
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
int debug_init(void)
{
    const u8 debugprompt[] = "iTSX>";
  /* 
    F_FILE *fptr;
   u8  buffer[128];
   u32 bytecount;
   u32 crc32e_calc;
*/
   u8 status; 
   u8  response;

   debugif_uart_rx_reset();
    debug_out((u8*)debugprompt);



   // status = e2_stock_validatefile(STOCK_E2_FILENAME,&response);
 /*
    fptr = genfs_user_openfile("E2_stock_enc_body.ess","r"); 
   if (fptr == NULL)
   {
       fptr = genfs_default_openfile("E2_stock_enc_body.ess","r");  
       if (fptr == NULL)
       {
           status = S_OPENFILE;
           goto checksum_getcsffileinfo;
       }
   }



   crc32e_reset();
   crc32e_calc = 0xFFFFFFFF;
   while (!feof(fptr))
   {
       bytecount = fread((char*)buffer,1,sizeof(buffer),fptr);
       if (bytecount)
       {
           crc32e_calc = crc32e_calculateblock(crc32e_calc,(u32*)buffer,bytecount/4);    
       }
   }
   
   status = S_SUCCESS;

checksum_getcsffileinfo:

    genfs_closefile(fptr);
*/
    return status;  
}

//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
void debug_out(u8* outputstring)
{ 
    const u8 crlf[] = "\n\r";
    
    debugif_out((u8*)crlf, strlen((const char*)crlf));
    
    debugif_out(outputstring, strlen((char const*)outputstring));
}

//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
void debug_console(void)
{
    int status = S_FAIL;
    int i;
    u8 datain[64];
    u32 size = 0;
    
    const u8 debugprompt[] = "X4>";
    
    status = debugif_in(datain, (u32*)&size);
    if(status == S_SUCCESS)
    {
        datain[size] = 0; // Null terminate string
        // New Command
        for(i = 0;i<COMMAND_COUNT;i++)
        {
            if(strcmp((char const*)datain, (char const*)&commandlist[i].commandname) == 0)
            {
                // Matching Command
                debug_do_command(i);
            }
        }
        if(i > COMMAND_COUNT)
        {
            debug_out("Invaild Command");
        }
        debug_out((u8*)debugprompt);
    }
}

void debug_reportprogress(int progress, u8* outputstring)
{
    u8 buffer[64];
    
    sprintf((char*)buffer, "Progress: %d ", progress);
    strcat((char*)buffer, (const char*)outputstring);
    debug_out(buffer);
}

u32 RTC_count;
void debug_elapsedtime_start()
{
    RTC_count = rtc_getvalue();
}

void debug_elapsedtime_stop()
{
    u8 buffer[64];
    int tmin;
    int tsec;
    int tmsec;
    
    RTC_count = rtc_getvalue() - RTC_count;
    tmin = (RTC_count/1000) / 60;
    tsec = (RTC_count/1000) % 60;
    tmsec = RTC_count%1000;

    sprintf((char*)buffer, "Elapsed Time: %dm:%d.%03ds", tmin,tsec,tmsec);
    
    debug_out(buffer);
}

void debug_reportstatus(u8 status, u8* outputstring)
{
    u8 buffer[100];
    u8 statusbuf[40];
    
    strcpy((char*)buffer, (const char*)outputstring);
    
    if(status == S_SUCCESS)
    {
        strcat((char*)buffer, " Successful");
    }
    else
    {
        sprintf((char*)statusbuf, " Failed. Status = %X", status);
        strcat((char*)buffer, (const char*)statusbuf);
    }
    
    debug_out(buffer);
}

#else
int debug_init(void)
{
    return S_SUCCESS;
}
void debug_out(u8* outputstring)
{
}
void debug_console(void)
{
}
void debug_reportprogress(int progress, u8* outputstring)
{
}
void debug_elapsedtime_start()
{
}
void debug_elapsedtime_stop()
{
}
void debug_reportstatus(u8 status, u8* outputstring)
{
}
#endif

void debug_do_command(int selcmd)
{    
    commandlist[selcmd].funcptr();
}

void debug_Cmd_Help(void)
{
    int i;
    
    debug_out("Supported Commands:");
    // Display all supported command names 
    for(i = 0;i<(sizeof(commandlist)/sizeof(DEBUG_CONSOLE_COMMAND));i++)
    {
        debug_out((u8*)(commandlist[i].commandname));        
    }
}


void debug_Cmd_Checksums(void)
{
#ifdef __FORD_DEVICE__
    u8 datain[64];
    u32 size;
    u16 veh_type;
    int status;
    selected_tunelist_info tuneinfo;
    
    veh_type = 0;
    size = 0;
    
    debug_out("Vehicle Type (decimal): ");
    while(1)
    {
        status = debugif_in(datain, (u32*)&size);
        if(status == S_SUCCESS)
            break;
    }
    
    veh_type = (u16)atoi((char*)datain);
//    if(ford_veh_validate_vehicletype(veh_type) != S_SUCCESS)
    {
        debug_out("Invalid Vehicle Type!");
        return;
    }
    
    memset((char*)datain,0,sizeof(datain));
    debug_out("Enter checksumfilename: ");
    while(1)
    {
        status = debugif_in(datain, (u32*)&size);
        if(status == S_SUCCESS)
            break;
    }
    
    if(strstr((char*)datain, ".ipc") == 0)
    {
        debug_out("Bad Filename!");
        return;        
    }
    
    strcpy((char*)tuneinfo.checksumfilename[0], (char*)datain);
    
    status = obd2tune_apply_checksum(veh_type,&tuneinfo);
    if(status == S_SUCCESS)
    {
        debug_out("Done");   
    }
    else
    {
        debug_out("Failed!");
    }
#endif
}


void debug_Cmd_BtLoopBack(void)
{
    u8 datain[64];
    u32 size = 0;
    u8 status;
    
    debug_out("BT Loopback Mode (1=ON, 0=OFF): ");
    while(1)
    {
        status = debugif_in(datain, (u32*)&size);
        if(status == S_SUCCESS)
            break;
    }
    if(datain[0] == '1')
    {
        commlink_loopback_config(TRUE);
    }
    else
    {
        commlink_loopback_config(FALSE);
    }        
}

void debug_Cmd_BtCommandPassthru(void)
{
#if __COMMLINK_HAL_BLUETOOTH__
    u32  bt_connection;
    u8 passthrudata[10];
    u32 size;
    
    debug_out("Starting BT Passthru Mode...\n\r\n\r");
    commlink_passthrough_config(TRUE);
    debugif_setmode(DEBUGIF_IN_PASSTHROUGH);
    bt_connection = gpio_peek_commlink_connection_status();
    if ((bt_connection & 0x08) == 0)
    {
        bluetooth_uart_tx("^#^$^%",6,FALSE);
        while(gpio_peek_commlink_connection_status() & 0x08 == 0);
    }
    while(1)
    {
        if(debugif_in(passthrudata, (u32*)&size) == S_SUCCESS)
        {
            bluetooth_uart_tx(passthrudata, size, FALSE);
        }
        if (button_isTrigger())
        {
            commlink_passthrough_config(FALSE);
            debugif_setmode(DEBUGIF_IN_NORMAL);
            debug_out("Leaving BT Passthru Mode...\n\r\n\r");
            break;
        }
    }
#endif
}

void debug_Cmd_ReadStrategy(void)
{
#ifdef __FORD_DEVICE__
    vehicle_info vehicleinfo;
    u8 status;
//    ecm_info ecm;
//    u8 vin[20];
//    u32 vidaddr;
//    u8 buffer[20];
//    u16 datalen;
//    int msgcount;
//    char debugoutstring[30];
    
//    msgcount = 0;    
//    scp_init(SCP_TYPE_24BIT, SCP_LOW_SPEED);
//    
//    while(1)
//    {
//        status = obd2scp_readvin(ecm.vin,&ecm.commtype);
//        if(status == S_SUCCESS)
//        {
//            debug_out(ecm.vin);
//        }
//        else
//        {
//            debug_out("FAILED");
//            break;
//        }
//    }
    
//    status = obd2scp_readvin(ecm->vin,&ecm->commtype);
    
//    status = obd2scp_readecminfo(&ecm);
    
    status = obd2_readecminfo(&vehicleinfo.ecm, 0);
    if (status == S_SUCCESS)
    {
        debug_out("VIN:");
        debug_out((u8*)vehicleinfo.ecm.vin);
        debug_out("PCM STRATEGY:");
        debug_out((u8*)&vehicleinfo.ecm.codes[0]);
        debug_out("PCM PART#:");
        debug_out((u8*)vehicleinfo.ecm.second_codes[0]);
        debug_out("TCM STRATEGY:");
        debug_out((u8*)&vehicleinfo.ecm.codes[1]); 
        debug_out("TCM PART#:");
        debug_out((u8*)vehicleinfo.ecm.second_codes[1]);
    }
#endif
    
}

void debug_Cmd_Sniff(void)
{
#if __FORD_DEVICE__
    u8 datain[64];
    u32 size = 0;
    u8 rxbuffer[20];
    u8 dboutbuffer[100];
    int i = 0;
    int timestamp;
    bool newrx = FALSE;
//    u32 crctemp;
//    u32 crccalc;
    
//    scp_init(SCP_TYPE_24BIT,SCP_LOW_SPEED);
    debug_out("Sniff Mode Started...\n\r");
    
    rtc_reset();
    
//    rxbuffer[0] = spi_master_receive_byte(); // Flush
    while(1)
    {
        if(gpio_read_dav_v_busy_pin())
        {
            if(newrx)
            {
                newrx = FALSE;                
                //                    crctemp = (u32)(rxbuffer[17];
                //                    crctemp = (u32)(rxbuffer[16];
                //                    crctemp = (u32)(rxbuffer[15];
                //                    crctemp = (u32)(rxbuffer[14];
                //                    crc32_reset();
                //                    crc32e_calculateblock(0xFFFFFFFF, rxbuffer, i-4) != 
                sprintf((char*)dboutbuffer, "%d %X %X %X %X %X %X %X %X %X %X %X %X", timestamp,
                        rxbuffer[2],rxbuffer[3],rxbuffer[4],rxbuffer[5],rxbuffer[6],
                        rxbuffer[7],rxbuffer[8],rxbuffer[9],rxbuffer[10],rxbuffer[11],rxbuffer[12], rxbuffer[13]);
                debug_out(dboutbuffer);
                spi_master_send_byte(0xff);
                rxbuffer[0] = spi_master_receive_byte();
            }
            i = 0;
        }
        else
        {
            timestamp = (u32)rtc_getvalue();
            spi_master_send_byte(0xff);
            rxbuffer[i] = spi_master_receive_byte();
            newrx = TRUE;
            i++;
        }
        
        if(debugif_in(datain, (u32*)&size) == S_SUCCESS)
        {
            // quit
            debug_out("Quiting...\n\r");
            break;
        }
    }
#endif
}

void debug_Cmd_SetBTBaud(void)
{
#if __COMMLINK_HAL_BLUETOOTH__
    u8 status;
    u8 datain[64];
    u32 size = 0;
    
    debug_out("Enter new baud:\n\r");
    memset(datain, 0, sizeof(datain));
    while(1)
    {
        status = debugif_in(datain, (u32*)&size);
        if(status == S_SUCCESS)
        {
            break;
        }
    }
    if(strcmp((char*)datain,"115200") == 0)
    {
        status = bluetooth_changebaud_btmodule(COMMLINK_115200_BAUD);
    }
    else if(strcmp((char*)datain,"230400") == 0)
    {
        status = bluetooth_changebaud_btmodule(COMMLINK_230400_BAUD);
    }
    else if(strcmp((char*)datain,"921600") == 0)
    {
        status = bluetooth_changebaud_btmodule(COMMLINK_921600_BAUD);
    }
    else
    {
        // Invalid Baud rate
        status = S_FAIL;
    }
    
    if(status == S_SUCCESS)
    {
        debug_out("Success\n\r");
    }
    else
    {
        debug_out("Failed\n\r");
    }
#endif
}

void debug_Cmd_SetBTBaudDefault(void)
{
#if __COMMLINK_HAL_BLUETOOTH__
    u8 status;
    u8 datain[64];
    u32 size = 0;

    status = S_FAIL;
    debug_out("Enter new baud:\n\r");
    while(1)
    {
        status = debugif_in(datain, (u32*)&size);
        if(status == S_SUCCESS)
        {
            break;
        }
    }
    
//    if(strcmp((char*)datain,"115200"))
//    {
//        status = bluetooth_changedefaultbaud_btmodule(COMMLINK_115200_BAUD);
//    }
//    else if(strcmp((char*)datain,"230400"))
//    {
//        status = bluetooth_changedefaultbaud_btmodule(COMMLINK_230400_BAUD);
//    }
//    else
//    {
//        // Invalid Baud rate
//        status = S_FAIL;
//    }
    
    if(status == S_SUCCESS)
    {
        debug_out("Success\n\r");
    }
    else
    {
        debug_out("Failed\n\r");
    }
#endif
}

void debug_Cmd_SetBTBaud115kPin(void)
{
#if __COMMLINK_HAL_BLUETOOTH__
    bluetooth_force115kBaud_btmodule();
#endif
}

void debug_Cmd_SetUartBaud(void)
{
    u8 status;
    u8 datain[64];
    u32 size = 0;
    
    debug_out("Enter new baud:\n\r");
    while(1)
    {
        status = debugif_in(datain, (u32*)&size);
        if(status == S_SUCCESS)
        {
            break;
        }
    }
    
    if(strcmp((char*)datain,"115200"))
    {
        commlink_change_baudrate(COMMLINK_115200_BAUD);
    }
    else if(strcmp((char*)datain,"230400"))
    {
        commlink_change_baudrate(COMMLINK_230400_BAUD);
    }
    else
    {
        // Invalid Baud rate
        status = S_FAIL;
    }
    
    if(status == S_SUCCESS)
    {
        debug_out("Success\n\r");
    }
    else
    {
        debug_out("Failed\n\r");
    }
}

void debug_Cmd_ValidateApp(void)
{
    bootloader_force_application_validation();
}

void debug_Cmd_ValidateBoot(void)
{
    bootloader_force_mainboot_validation();
}


void debug_Cmd_IpcTest(void)
{
#ifdef __FORD_DEVICE__
    int status;
    flasher_info flasherinfo;
    
    flasherinfo.vehicle_type = 0x07; // '05-'09 Mustang
    strcpy((char*)flasherinfo.selected_tunelistinfo.tunefilename[0],"FBLF1_46MS.sth"); 
        
    status = obd2tune_apply_checksum((u16)flasherinfo.vehicle_type, (selected_tunelist_info*)&flasherinfo.selected_tunelistinfo);
    if(status == S_SUCCESS)
    {
        debug_out("Success");
    }
    else
    {
        debug_out("FAIL");
    }
#endif
}

extern flasher_info *flasherinfo;                   //from obd2tune.c
void debug_Cmd_UploadStock(void)
{
    int status;
    u16 veh_type = 43;
//    u8 ecm_type = 0;
    
//    VehicleCommType vehiclecommtype = CommType_SCP;
//    VehicleCommLevel vehiclecommlevel = CommLevel_KWP2000;
    
    status = obd2tune_flasherinfo_init();
    flasherinfo->flashtype = CMDIF_ACT_UPLOAD_STOCK;
    flasherinfo->uploadstock_required = TRUE;
    flasherinfo->isupload_only = TRUE;
    
    status = obd2tune_uploadstock(veh_type, TRUE);
    
    if(status == S_SUCCESS)
    {
        debug_out("Success");
    }
    else
    {
        debug_out("FAIL");
    }
}

void debug_Cmd_DownloadTune(void)
{
    int status;
    u16 ecm_type = 0x42;
    VehicleCommType vehiclecommtype = CommType_CAN;
    VehicleCommLevel vehiclecommlevel = CommLevel_ISO14229;
    
    status = download_ecm(ecm_type, vehiclecommtype, vehiclecommlevel, FALSE);
    if(status == S_SUCCESS)
    {
        debug_out("Success");
    }
    else
    {
        debug_out("FAIL");
    }
}

void debug_Cmd_Checksum64Test(void)
{
#if __FORD_DEVICE__
    int status;
    u16 ecm_type;
    u32 offset;

    ecm_type = 0x2B; // stacked 6.4L
    offset = 0;
    
    status = filestock_openflashfile("r+");
    if (status != S_SUCCESS)
    {
        debug_out("FAIL - Cannot open flash file");
        return;
    }
    
    status = obd2tune_ford_checksum_64L(ecm_type,offset);
    if(status != S_SUCCESS)
    {
        debug_out("FAIL - Error applying checksum");
    }
    
    filestock_closeflashfile();
#endif
}

void debug_Cmd_Test(void)
{
#ifdef __FORD_DEVICE__
    u8 status;
    u16 veh_type;
//    u8  l_seed;
//    u8  *ptr;
//    bool test;
//    u8 dboutbuffer[100];
//    
//    u8 txbuffer[SCP_MAX_TX_MSG_LENGTH+1];
//    u8 rxbuffer[SCP_MAX_TX_MSG_LENGTH+1];
//    obd2scp_rxinfo rxinfo;
    
    
    status = S_FAIL;

    veh_type = 0x05;
    
//    debug_out("Unlocking SCP..");
    
//    status = obd2_bus_init_in_programming_mode(CommType_SCP);
//    if(status != S_SUCCESS)
//    {
//        debug_out("Bus Init Failed!");
//        return;
//    }

/*    
    obd2scp_rxinfo_init(&rxinfo);
    rxinfo.cmd = 0x22;
    rxinfo.transfermode = SCP_MODE_NODE;
    rxinfo.rxbuffer = rxbuffer;
    
    txbuffer[0] = 0xC4;
    txbuffer[1] = 0x10;
    txbuffer[2] = 0xF0;
    txbuffer[3] = 0x22;
    txbuffer[4] = 0x11;
    txbuffer[5] = 0x00;
    txbuffer[6] = 0x00;
    
    test = 1;
    
    while(test)
    {
        status = obd2scp_tx(txbuffer,7,SCP_MODE_NODE);
        if (status != S_SUCCESS)
        {
            debug_out("TX Failed!");
        }
        
        status = obd2scp_rx(&rxinfo);
        if (status == S_SUCCESS)
        {
            sprintf((char*)dboutbuffer, "%X %X %X %X %X %X %X %X %X %X %X %X",
                    rxbuffer[0],rxbuffer[1],rxbuffer[2],rxbuffer[3],rxbuffer[4],
                    rxbuffer[5],rxbuffer[6],rxbuffer[7],rxbuffer[8],rxbuffer[9],rxbuffer[10], rxbuffer[11]);
            debug_out(dboutbuffer);
        }
//        else
//        {
//            
//        }
    }
    
    return;
*/
    status = obd2_setupvpp();
    if(status != S_SUCCESS)
    {
        debug_out("FEPS Failed!");
        return;
    }
        
    status = obd2_ford_wakeup(veh_type, 0);
    if(status != S_SUCCESS)
    {
        debug_out("Wake up Failed!");
        return;
    }
    
    status = obd2can_unlockecm(UnlockTask_Upload,51,FALSE);
//    status = obd2scp_unlockecm(VEH_GetEcmType(veh_type,0),FALSE,&l_seed);
    if(status != S_SUCCESS)
    {
        debug_out("Unlock Failed!");
        return;
    }
    
//    status = obd2scp_requesthighspeed(CommType_SCP, l_seed);
    if(status != S_SUCCESS)
    {
        debug_out("Baud Change Failed!");
        return;
    }
        
    debug_out("Success!");
    return;
#endif
}


//------------------------------------------------------------------------------
// EOF
//------------------------------------------------------------------------------
