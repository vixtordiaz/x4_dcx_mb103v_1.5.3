/**
  **************** (C) COPYRIGHT 2011 SCT Performance, LLC *********************
  * File Name          : housekeeping.c
  * Device / System    : SCT products using the "CommonCode" platform
  * Author             : Patrick Downs
  *
  * Version            : 1 
  * Date               : 12/15/2011
  * Description        : 
  *                    : 
  *
  *
  * History            : 05/02/2011 M. Davis
  *                    :   Add SCT Copyright header,
  *
  ******************************************************************************
  */

#include <string.h>
#include <arch/gentype.h>
#include <board/power.h>
#include <board/rtc.h>
#include <board/timer.h>
#include <board/indicator.h>
#include <common/statuscode.h>
#include <common/cmdif.h>
#include <common/obd2.h>
#include "housekeeping.h"

typedef struct
{
    HouseKeepingItem housekeeping_item[5];   // TODO: Make dynamic with malloc?
}HouseKeepingInfo;

HouseKeepingInfo housekeeping_info; 

//------------------------------------------------------------------------------
// Initialize the housekeeping function
// Engineer: Patrick Downs
//------------------------------------------------------------------------------
void housekeeping_init()
{
    memset((u8*)&housekeeping_info, 0, sizeof(housekeeping_info));
    obd2_load_demo_vehicle_info();      //TODOQK: leave it here for now
}

//------------------------------------------------------------------------------
// Housekeeping process. This is called by the main loop.
// Engineer: Patrick Downs
//------------------------------------------------------------------------------
void housekeeping_process()
{
    u32 currenttime;
    int i;
    for(i = 0; i < (sizeof(housekeeping_info)/sizeof(HouseKeepingItem));i++)
    {
        if(housekeeping_info.housekeeping_item[i].type != HouseKeepingType_EmptyType)
        {
            switch(housekeeping_info.housekeeping_item[i].type)
            {
            case HouseKeepingType_VppTimeout:
            case HouseKeepingType_IndicatorTimeout:
            case HouseKeepingType_CommLinkSpeedSync:
            case HouseKeepingType_CmdifIntrResponseAck:
            case HouseKeepingType_TaskTimerTO:
                currenttime = rtc_getvalue();
                if(currenttime > housekeeping_info.housekeeping_item[i].timeout_cmp_value)
                {
                    housekeeping_timeout_handler(housekeeping_info.housekeeping_item[i].type);                    
                }
                break;
            default:
                break;
            }
        }
    }
    
    return;
}

//------------------------------------------------------------------------------
// Add a housekeeping item type and timeout in miliseconds
// Engineer: Patrick Downs
//------------------------------------------------------------------------------
u8 housekeeping_additem(HouseKeepingType newtype, u32 timeout_ms)
{
    int i;
    for(i = 0; i < (sizeof(housekeeping_info)/sizeof(HouseKeepingItem));i++)
    {
        if(housekeeping_info.housekeeping_item[i].type == HouseKeepingType_EmptyType)
            break;
    }
    
    if(i==(sizeof(housekeeping_info)/sizeof(HouseKeepingItem)))
    {
        return S_FAIL; // No open item slots
    }
    
    housekeeping_info.housekeeping_item[i].type = newtype;
    housekeeping_info.housekeeping_item[i].timeout_cmp_value = (rtc_getvalue() + timeout_ms);
    return S_SUCCESS;
}

//------------------------------------------------------------------------------
// Remove an item
// Engineer: Patrick Downs
//------------------------------------------------------------------------------
u8 housekeeping_removeitem(HouseKeepingType type)
{
    int i;
    for(i = 0; i < (sizeof(housekeeping_info)/sizeof(HouseKeepingItem));i++)
    {
        if(housekeeping_info.housekeeping_item[i].type == type)
        {
            memset((u8*)&housekeeping_info.housekeeping_item[i], 0, sizeof(HouseKeepingItem));
            return S_SUCCESS;
        }
    }
  
    return S_FAIL;  
}

//------------------------------------------------------------------------------
// Execute the required code when housekeeping item timeout is reached
// Engineer: Patrick Downs
//------------------------------------------------------------------------------
void housekeeping_timeout_handler(HouseKeepingType type)
{        
    //housekeeping_removeitem(type);
    switch(type)
    {
    case HouseKeepingType_VppTimeout:
        power_setvpp(Vpp_OFF);       
        break;
    case HouseKeepingType_IndicatorTimeout:
        indicator_set_normaloperation(TRUE);
        break;
    case HouseKeepingType_CmdifIntrResponseAck:
        cmdif_interrupt_response_ack();
        break;
    case HouseKeepingType_TaskTimerTO:
        tasktimer_terminate_alltasks();
        break;
    case HouseKeepingType_CommLinkSpeedSync:
        commlink_init(USE_COMMLINK_BAUD);   //revert to default
        break;
    default:
        break;
    }
    housekeeping_removeitem(type);
}
