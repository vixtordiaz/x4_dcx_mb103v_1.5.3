/**
  **************** (C) COPYRIGHT 2011 SCT Performance, LLC *********************
  * File Name          : rle.h
  * Device / System    : SCT products using the "CommonCode" platform
  * Author             : Quyen Leba
  *
  * Version            : 1 
  * Date               : 05/02/2011
  * Description        : 
  *                    : 
  *
  *
  * History            : 05/02/2011 M. Davis
  *                    :   Add SCT Copyright header,
  *
  ******************************************************************************
  */

#ifndef __RLE_H
#define __RLE_H

#include <arch/gentype.h>

u8 rle_init();
u8 rle_deinit();

u8 rle_compressbuffer_1byte(u8 *buffer, u16 bufferlength,
                            u8 *compressdata, u16 *compressdatalength);
u8 rle_decompressbuffer_1byte(u8 *buffer, u16 bufferlength,
                              u16 bufferstartposition,
                              u16 outputthreshold,
                              u8 *uncompressdata, u16 *uncompressdatalength,
                              u16 *bufferendposition);
u8 rle_compressfile_1byte(u8 *src_filename, u8 *dst_filename);
u8 rle_decompressfile_1byte(u8 *src_filename, u8 *dst_filename);

#endif	//__RLE_H
