/**
  **************** (C) COPYRIGHT 2011 SCT Performance, LLC *********************
  * File Name          : obd2.c
  * Device / System    : SCT products using the "CommonCode" platform
  * Author             : Quyen Leba
  *
  * Version            : 1 
  * Date               : 05/02/2011
  * Description        : 
  *                    : 
  *
  *
  * History            : 05/02/2011 M. Davis
  *                    :   Add SCT Copyright header,
  *
  ******************************************************************************
  */

#include <stdio.h>
#include <board/genplatform.h>
#include <board/delays.h>
#include <fs/genfs.h>
#include <common/obd2comm.h>
#include <common/obd2tune.h>
#include <common/obd2datalog.h>
#include <common/settings.h>
#include <common/statuscode.h>
#include "obd2.h"

//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
extern flasher_info *flasherinfo;       /* from obd2tune.c */
obd2_info gObd2info = NULL;

u8  obd2_rxbuffer[256];
u32 cooldowntime = 0;       //used to slow down data transmit, adds Micro Second delay

//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
u8 obd2_garbagecollector()
{
    cooldowntime = 0;
    obd2tune_garbagecollector();
    obd2datalog_garbagecollector();
    return S_SUCCESS;
}

//------------------------------------------------------------------------------
// Load demo vehicle info from file
// Engineer: Quyen Leba
//------------------------------------------------------------------------------
void obd2_load_demo_vehicle_info()
{
#define DEMO_VEHICLE_DETAILS_FILENAME       "\\USER\\demovehinfo.bin"
    DemoVehicle tmpdemoveh;
    F_FILE *fptr;
    u32 bytecount;
    u32 crc32e_calc;

    fptr = genfs_user_openfile(DEMO_VEHICLE_DETAILS_FILENAME,"r");
    if (fptr)
    {
        bytecount = fread((char*)&tmpdemoveh,1,sizeof(DemoVehicle),fptr);
        genfs_closefile(fptr);
        if (bytecount == sizeof(DemoVehicle))
        {
            crc32e_reset();
            crc32e_calc = crc32e_calculateblock
                (0xFFFFFFFF,(u32*)&tmpdemoveh.ecm,(sizeof(tmpdemoveh.ecm)/4));
            if (crc32e_calc == tmpdemoveh.ecminfo_crc32e)
            {
                memcpy((char*)&demo_vehicle,(char*)&tmpdemoveh,
                       sizeof(DemoVehicle));
            }
        }
    }
}

//------------------------------------------------------------------------------
// Save demo vehicle info to file
// Engineer: Quyen Leba
//------------------------------------------------------------------------------
void obd2_save_demo_vehicle_info()
{
    DemoVehicle tmpdemoveh;
    F_FILE *fptr;
    u32 bytecount;

    memcpy((char*)&tmpdemoveh,(char*)&demo_vehicle,
                       sizeof(DemoVehicle));
    crc32e_reset();
    tmpdemoveh.ecminfo_crc32e = crc32e_calculateblock
        (0xFFFFFFFF,(u32*)&tmpdemoveh.ecm,(sizeof(tmpdemoveh.ecm)/4));

    fptr = genfs_user_openfile(DEMO_VEHICLE_DETAILS_FILENAME,"w");
    if (fptr)
    {
        bytecount = fwrite((char*)&tmpdemoveh,1,sizeof(DemoVehicle),fptr);
        genfs_closefile(fptr);
        if (bytecount != sizeof(DemoVehicle))
        {
            genfs_deletefile(DEMO_VEHICLE_DETAILS_FILENAME);
        }
    }
}

/**
 *  @brief Set cool downtime (TX frame seperation time)
 *  
 *  @param [in] time_us     Seperation time in micro seconds
 *
 *  @return Status
 *
 *  @details
 *           
 */
void obd2_setcooldowntime(u32 time_us)
{
    cooldowntime = time_us;
}

u32 obd2_getcooldowntime()
{
    return cooldowntime;
}

//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
u8 obd2_clearemcinfo(ecm_info *ecminfo)
{
    u16 i;

    ecminfo->ecm_count = 0;
    memset(ecminfo->codecount,0,ECM_MAX_COUNT);
    memset(ecminfo->vehicle_serial_number,0,
           sizeof(ecminfo->vehicle_serial_number));
    memset(ecminfo->hwid,0,sizeof(ecminfo->hwid));

    memset(ecminfo->codes,0,sizeof(ecminfo->codes));
    memset(ecminfo->second_codes,0,sizeof(ecminfo->second_codes));
    for(i=0;i<ECM_MAX_COUNT;i++)
    {
        memset(ecminfo->codeids[i],0,ECM_MAX_PART_COUNT);
        ecminfo->commtype[i] = CommType_Unknown;
        ecminfo->commlevel[i] = CommLevel_Unknown;
    }
    memset(ecminfo->vin,0,sizeof(ecminfo->vin));
    return S_SUCCESS;
}

/**
 *  @brief Find vehicle comm type and comm level
 *  
 *  @param [in] vehiclecommtype  Vehicle comm type
 *  @param [in] vehiclecommlevel Vehicle comm level
 *
 *  @return Status
 *
 *  @details This function serves as a wrapper for current common code
 *           compatibility
 */
u8 obd2_findvehiclecommtypecommlevel(VehicleCommType *vehiclecommtype,
                                     VehicleCommLevel *vehiclecommlevel)
{
    u8  i;
    u8  status;
        
    if (SETTINGS_IsDemoMode())
    {
        for(i = 0; i < ECM_MAX_COUNT; i++)
        {
            vehiclecommtype[i] = demo_vehicle.ecm.commtype[i];
            vehiclecommlevel[i] = demo_vehicle.ecm.commlevel[i];
        }
        return S_SUCCESS;
    }

    status = obd2_getinfo(&gObd2info);
    if (status == S_SUCCESS)
    {
        for(i = 0; i < ECM_MAX_COUNT; i++)
        {
            if (gObd2info.ecu_block[i].ecutype != EcuType_Unknown)
            {
                vehiclecommtype[i] = gObd2info.ecu_block[i].commtype;
                vehiclecommlevel[i] = gObd2info.ecu_block[i].commlevel;
            }
            else
            {
                vehiclecommtype[i] = CommType_Unknown;
                vehiclecommlevel[i] = CommLevel_Unknown;
            }
        }
    }
    return status;
}

/**
 *  @brief Find vehicle commtype
 *
 *  @return VehicleCommType
 *
 *  @details This function serves as a wrapper for current common code
 *           compatibility. This function only finds the first comm type.
 */
VehicleCommType obd2_findvehiclecommtype()
{
    VehicleCommType vehiclecommtype = CommType_Unknown;

    if (obd2_getinfo(&gObd2info) == S_SUCCESS)
    {
        for (u8 i = 0; i < ECM_MAX_COUNT; i++)
        {
            if (gObd2info.ecu_block[i].ecutype != EcuType_Unknown)
            {
                vehiclecommtype = gObd2info.ecu_block[i].commtype;
                break;
            }
        }
    }
    return vehiclecommtype;
}

//------------------------------------------------------------------------------
// Abstract: Calculate J1850 CRC
// Inputs:  u8  *msg_buf
//          u32 nbytes
// Returns: u8  calc_crc_value
//------------------------------------------------------------------------------
u8 obd2_j1850crc(u8 *msg_buf, u32 nbytes)
{
    u8  calc_crc_value=0xFF;
    u8  poly,bit_count;
    u32 byte_count;
    u8  *byte_point;
    u8  bit_point;

    for(byte_count=0,byte_point=msg_buf;
        byte_count<nbytes; ++byte_count, ++byte_point)
    {
        for(bit_count=0, bit_point=0x80;
            bit_count<8; ++bit_count, bit_point>>=1)
        {
            if (bit_point & *byte_point)    // case for new bit = 1
            {
                if (calc_crc_value & 0x80)
                {
                    poly=1;     // define the polynomial
                }
                else
                {
                    poly=0x1C;
                }

                calc_crc_value = ((calc_crc_value << 1) | 1) ^ poly;
            }
            else                            // case for new bit = 0
            {
                poly=0;
                if (calc_crc_value & 0x80)
                {
                    poly=0x1D;
                }
                calc_crc_value = (calc_crc_value << 1) ^ poly;
            }
        }//for(bit_count...
    }//for(byte_count=0...

    return ~calc_crc_value;
}

//------------------------------------------------------------------------------
// Generate a VIN using DEVICE serial number; to replace blank VIN
// Output:  u8  *vin (VIN_LENGTH+1)
// Return:  u8  status
// Engineer: Quyen Leba
//------------------------------------------------------------------------------
u8 obd2_generatevin(u8 *vin)
{
    u32 i;
    u32 val;

    //TODOQ: should not happen but validate serial number too.
    strcpy((char*)vin,"SCT");
    if (isprint(SETTINGS_CRITICAL(serialnumber)[13]))
    {
        // in case we change to use 16-byte serial number
        memcpy((char*)&vin[3],(char*)SETTINGS_CRITICAL(serialnumber),2);
        memcpy((char*)&vin[5],&SETTINGS_CRITICAL(serialnumber)[4],12);
    }
    else
    {
        memcpy((char*)&vin[3],(char*)SETTINGS_CRITICAL(serialnumber),2);
        vin[5] = '.';
        memcpy((char*)&vin[6],&SETTINGS_CRITICAL(serialnumber)[2],11);
    }

    for(i=0;i<4;i++)
    {
        val = (rand() % 25) + 'A';
        vin[5+i] = (u8)val;
    }

    vin[VIN_LENGTH] = NULL;
    return S_SUCCESS;
}

//------------------------------------------------------------------------------
// Input:   u8  *vin (VIN_LENGTH+1)
// Return:  u8  status
// Engineer: Quyen Leba
//------------------------------------------------------------------------------
u8 obd2_validatevin(u8 *vin)
{
    u8 i;
    u8 validcount;
    u8 blankcount;
    
    validcount = 0;
    blankcount = 0;
    for(i=0;i<VIN_LENGTH;i++)
    {
        if (vin[i] == 0xFF)
            blankcount++;
        else if (isgraph(vin[i]))
            validcount++;
        else
            break;
    }
    if (validcount == VIN_LENGTH)
    {
        return S_SUCCESS;
    }
    else if (blankcount == VIN_LENGTH)
    {
        return S_VINBLANK;
    }
    else
    {
        return S_VININVALID;
    }
}

/**
 *  @brief Read VIN
 *  
 *  @param [out]    vin                 VIN number
 *
 *  @return Status
 *
 *  @details This function reads the vehicles VIN. Note: *vin must be able to
 *           store VIN_LENGTH+1 bytes.
 */
u8 obd2_readvin(u8 *vin)
{
    VehicleCommType commtype;
    u8 status;

    if (SETTINGS_IsDemoMode())
    {
        return S_SUCCESS;
    }

    commtype = obd2_findvehiclecommtype();
    
    switch (commtype)
    {
    case CommType_CAN:
        /* Get VIN by OEM type */
        switch (gObd2info.oemtype)
        {
#ifdef __DCX_MANUF__
        case OemType_DCX:
            status = obd2can_readvin(vin);
            break;
#endif
#ifdef __FORD_MANUF__
        case OemType_FORD:
            status = obd2can_ford_readvin(vin);
            break;
#endif
#ifdef __GM_MANUF__
        case OemType_GM:
            status = obd2can_gm_readvin(vin);
            break;
#endif
        default:
            status = obd2can_readvin(vin);
            break;
        }
        break;
    case CommType_VPW:
        status = obd2vpw_readvin(vin);
        break;
    case CommType_SCP:
    case CommType_SCP32:
        status = obd2scp_readvin(vin, &commtype);
        break;
    case CommType_KLINE:
    case CommType_KWP2000:
        status = obd2iso_readvin(vin, commtype);
        break;
    case CommType_SCIA:
    case CommType_SCIB:
        status = obd2sci_readvin(vin, commtype, CommLevel_Unknown);
        break; 
    default:
        status = S_FAIL;
        break;
    }
    vin[VIN_LENGTH] = NULL;
    return status;
}

/**
 *  @brief Read vehicle serial number
 *  
 *  @param [out]    serialnumber    Vehicle serial number
 *  @param [in,out] vehiclecommtype Vehicle comm type
 *
 *  @return Status
 *
 *  @details This function reads the vehicle serial number.
 */
u8 obd2_readserialnumber(u8 *serialnumber, VehicleCommType *vehiclecommtype)
{
    VehicleCommType newcommtype;
    u8  status;

    memset((char*)serialnumber,0,32);

    if (SETTINGS_IsDemoMode())
    {
        if (*vehiclecommtype == demo_vehicle.ecm.commtype[0] || 
            *vehiclecommtype == CommType_Unknown)
        {
            strcpy((char*)serialnumber,(char*)demo_vehicle.ecm.vehicle_serial_number);
            *vehiclecommtype = demo_vehicle.ecm.commtype[0];
            return S_SUCCESS;
        }
        else
        {
            return S_FAIL;
        }
    }//if (SETTINGS_IsDemoMode())...

    newcommtype = obd2_findvehiclecommtype();
    if (*vehiclecommtype == CommType_Unknown)
    {
        *vehiclecommtype = newcommtype;
    }

    switch(*vehiclecommtype)
    {
    case CommType_CAN:
        /* Get serial number by OEM type */
        switch (gObd2info.oemtype)
        {
#ifdef __DCX_MANUF__
        case OemType_DCX:
            status = S_NOTSUPPORT;
            break;
#endif
#ifdef __FORD_MANUF__
        case OemType_FORD:
            status = S_NOTSUPPORT;
            break;
#endif
#ifdef __GM_MANUF__
        case OemType_GM:
            {
                u8 bcc[4];
                status = obd2can_gm_readSerialNumber(serialnumber, bcc);
                break;
            }
#endif
        default:
            status = S_NOTSUPPORT;
            break;
        }
        break;
    case CommType_VPW:
        status = obd2vpw_readserialnumber(serialnumber);
        break;
    case CommType_SCP:
    case CommType_SCP32:
        status = S_NOTSUPPORT;
        break;
    default:
        status = S_FAIL;
        break;
    }
    return status;
}

/**
 *  @brief Read ecu info
 *  
 *  @param [out]    ecm     Ecu information
 *  @param [in,out] vehiclecommtype Vehicle comm type
 *
 *  @return Status
 *
 *  @details This function reads ecu information.
 */
u8 obd2_readecminfo(ecm_info *ecm, VehicleCommType *vehiclecommtype)
{
    u8 status;
    VehicleCommType commtype;
    VehicleCommType newcommtype;

    if (vehiclecommtype == NULL)
    {
        commtype = CommType_Unknown;
    }
    else
    {
        commtype = *vehiclecommtype;
    }

    obd2_clearemcinfo(ecm);
    ecm->vin[0] = NULL;
    ecm->commtype[0] = commtype;
    ecm->commlevel[0] = CommLevel_Unknown;

    if (SETTINGS_IsDemoMode())
    {
        memcpy((char*)ecm,(char*)&demo_vehicle.ecm,sizeof(ecm_info));
        *vehiclecommtype = demo_vehicle.ecm.commtype[0];
        return S_SUCCESS;
    }
    
    newcommtype = obd2_findvehiclecommtype();
    if (commtype == CommType_Unknown)
    {
        commtype = newcommtype;
    }
    if (gObd2info.ecu_count == 0)
    {
        return S_NOCOMMUNICATION;
    }
    
    switch (commtype)
    {
    case CommType_CAN:
        /* Get info by OEM type */
        switch (gObd2info.oemtype)
        {
#ifdef __DCX_MANUF__
        case OemType_DCX:
            status = obd2can_dcx_readecminfo(ecm);
            break;
#endif
#ifdef __FORD_MANUF__
        case OemType_FORD:
            status = obd2can_ford_readecminfo(ecm);
            break;
#endif
#ifdef __GM_MANUF__
        case OemType_GM:
            status = obd2can_gm_readecminfo(ecm);
            break;
#endif
        default:
            {
                /* Get generic info */
                ecm->ecm_count = gObd2info.ecu_count;
                memcpy(ecm->vin, gObd2info.vin, sizeof(ecm->vin));
                for(u8 i = 0; i < ECM_MAX_COUNT; i++)
                {
                    if (gObd2info.ecu_block[i].ecutype != EcuType_Unknown)
                    {
                        ecm->commtype[i] = gObd2info.ecu_block[i].commtype;
                        ecm->commlevel[i] = gObd2info.ecu_block[i].commlevel;
                        if (gObd2info.ecu_block[i].partnumber_count == 1)
                        {
                            ecm->codecount[i] = 1;
                            memcpy(&ecm->codes[i], gObd2info.ecu_block[i].partnumbers.generic.strategy, sizeof(ecm->codes[i]));
                        }
                    }
                }
                status = S_SUCCESS;
                break;
            }
        } /* switch(..) */
        break;
    case CommType_VPW:
        status = obd2vpw_readecminfo(ecm);
        break;
    case CommType_SCP:
    case CommType_SCP32:
        status = obd2scp_readecminfo(ecm);
        break;
    case CommType_KLINE:
    case CommType_KWP2000:
        status = obd2iso_readecminfo(ecm);
        break;
    case CommType_SCIA:
    case CommType_SCIB:
        status = obd2sci_readecminfo(ecm);
        break;
    default:
        ecm->vin[0] = NULL;
        break;
    }
    
    ecm->commtype[0] = commtype;
    if (vehiclecommtype)
    {
        *vehiclecommtype = commtype;
    }
    ecm->vin[VIN_LENGTH] = NULL;
    
    return status;
}

/**
 *  @brief Clear DTCs
 *
 *  @param [in]     commtype -Vehicle comm type, command_if_cmd -command is from vehicle functions menu = 1
 *
 *  @return Status
 */
u8 obd2_cleardtc(VehicleCommType commtype, u8 command_if_cmd)
{
    VehicleCommType newcommtype;
    u8 status;

    if (SETTINGS_IsDemoMode())
    {
        return S_SUCCESS;
    }

    newcommtype = obd2_findvehiclecommtype();
    if (commtype == CommType_Unknown)
    {
        commtype = newcommtype;
    }
    
    switch (commtype)
    {
    case CommType_CAN:
        /* Clear DTC by OEM type */
        switch (gObd2info.oemtype)
        {
#ifdef __DCX_MANUF__
        case OemType_DCX:
            if(command_if_cmd == 1)
                status = obd2can_dcx_clear_diagnostic_information_all_modules();
            else
                status = obd2can_dcx_cleardtc();
            break;
#endif
#ifdef __FORD_MANUF__
        case OemType_FORD:
            status = obd2can_ford_cleardtc();
            break;
#endif
#ifdef __GM_MANUF__
        case OemType_GM:
            status = obd2can_gm_cleardtc();
            break;
#endif
        default:
            /* Clear with generic command (J1979) */
            status = obd2can_generic_cleardtc(gObd2info.ecu_block[0].ecu_id);
            break;
        }
        break;
    case CommType_VPW:
        obd2vpw_init(TRUE, FALSE);
        status = obd2vpw_cleardtc();
        break;
    case CommType_SCP:
    case CommType_SCP32:
        obd2scp_init(Obd2scpType_24Bit);
        status = obd2scp_cleardtc();
        break;
    case CommType_KLINE:
    case CommType_KWP2000:
        status = obd2iso_cleardtc(commtype);
        break;
    case CommType_SCIA:
    case CommType_SCIB:
        status = obd2sci_cleardtc(commtype); 
        break; 
    default:
        status = S_FAIL;
        break;
    }
    return status;
}

/**
 * @brief   Parse Packet PID
 *
 * @param   [in]  packet        Packet data
 * @param   [in]  packet_size   Packet data length in bytes
 * @param   [in]  position      Starting position of PID data
 * @param   [out] pid           Parsed PID
 * @param   [in]  pid_size      Size of PID data (max is 32-bits)
 * @param   [in]  isBit         TRUE: pid_size and position are in bits
 *                              FALSE: pid_size and position are in bytes
 *
 * @retval  u8 status
 *
 * @author  Tristen Pierson
 *
 * @details This function extracts PID data from Packeted PIDs. This function 
 *          can also be used to extract bitmapped or enumerated data. Please
 *          note that this function does not parse the extracted data. Also,
 *          position and size cannot overlap bytes; this will cause endian
 *          conversion to shift nibbles.
 *
 *          Example format:
 *
 *              packet[0]        packet[1]        packet[2]        packet[3]
 *               BYTE A           BYTE B           BYTE C           BYTE D
 *          7             0  7             0  7             0  7             0
 *          - - - - - - - -  - - - - - - - -  - - - - - - - -  - - - - - - - -
 *          7             0  15            8  23           16  31           24
 */
u8 obd2_parse_packet_pid(u8 *packet, u16 packet_size, u16 position, 
                         u8 *pid, u16 pid_size, u8 isBit)
{
    u32 mask;
    u8  byte_offset;
    u8  bit_offset;
    u8  end_bit;
    u8  data_size;
    u8  status;
  
    status = S_SUCCESS;
    
    memset(pid,0x00,4);
    
    // Parse by byte size and position
    if (isBit == FALSE)
    {
        if (position > 0)
        {
            // Qualify data position and extract PID
            if (pid_size + position <= packet_size)
            {
               memcpy(pid, &packet[position], pid_size);
            }
            else
            {
                status = S_OUTOFRANGE;
            }
        }
        else
        {
            memcpy(pid, packet, pid_size);
        }
    }
    // Parse by bit size and position
    else
    {
        end_bit = position + pid_size - 1;
        
        // Make sure PID data fits in packet
        if(end_bit < packet_size*8)
        {
            byte_offset = position / 8;
            bit_offset = position % 8;
            data_size = (pid_size-1+bit_offset)/8+1;   // Data size (offset and rounded to closest byte)

            memcpy ((u32*)pid, &packet[byte_offset], data_size);                        
            mask = (1<<pid_size)-1 << bit_offset;                        
            *(u32*)pid = (*(u32*)pid & mask) >> bit_offset;            
            
            // Force 3-byte data to be 4-bytes
            if (data_size == 3)
            {
               *(u32*)pid <<= 8;
            }
        }
        else
        {
            status = S_OUTOFRANGE;
        }
    }
    return status;
}

//------------------------------------------------------------------------------
// Input:   u16 raw_dtc
// Return:  u8  codetype ('C', 'B', 'U' or 'P')
// Engineer: Quyen Leba
//------------------------------------------------------------------------------
u8 obd2_parsetype_dtc(u16 raw_dtc)
{
    u16 type;
    u8 codetype;

    type = raw_dtc & 0xC000;
    switch(type)
    {
    case 0x4000:
        codetype = 'C';
        break;
    case 0x8000:
        codetype = 'B';
        break;
    case 0xC000:
        codetype = 'U';
        break;
    case 0x0000:
    default:
        codetype = 'P';
        break;
    }
    return codetype;
}

/**
 *  @brief Read DTCs
 *  
 *  @param [out]    info        DTC Info
 *
 *  @return Status
 */
u8 obd2_readdtc(dtc_info *info)
{
    u8 status;
    VehicleCommType commtype;
    dtc_info tmpdtcinfo;
    obd2_info_block block;
    u32 i;

    if (SETTINGS_IsDemoMode())
    {
        info->count = sizeof(demo_dtc.codes)/sizeof(demo_dtc.codes[0]);
        if (demo_dtc.count < info->count)
        {
            info->count = demo_dtc.count;
        }
        for (i=0;i<info->count;i++)
        {
            info->codes[i] = demo_dtc.codes[i];
        }
        return S_SUCCESS;
    }//if (SETTINGS_IsDemoMode())...

    commtype = obd2_findvehiclecommtype();

    switch (commtype)
    {
    case CommType_CAN:
        status = S_FAIL;
        /* ECM */
        if (obd2_info_get_block_by_type(&gObd2info, EcuType_ECM, &block) == S_SUCCESS)
        {
            status = obd2can_readdtc(block.ecu_id, info);
            if (status == S_SUCCESS)
            {
                for(i = 0; i < info->count; i++)
                {
                    /* ECM Index 0 for ECM */
                    info->code_ecmindex[i] = 0;
                }

#ifndef __DCX_MANUF__

                /* TCM */
                if (obd2_info_get_block_by_type(&gObd2info, EcuType_TCM, &block) == S_SUCCESS)
                {
                    status = obd2can_readdtc(block.ecu_id, &tmpdtcinfo);
                    if (status == S_SUCCESS)
                    {
                        for(i = 0; i < tmpdtcinfo.count; i++)
                        {
                            info->codes[info->count] = tmpdtcinfo.codes[i];
                            /* ECM Index 1 for TCM */
                            info->code_ecmindex[info->count] = 1;
                            info->count++;
                        }
                    }
                }
#endif
            }
        }
        break;
    case CommType_VPW:
        obd2vpw_init(TRUE, FALSE);
        status = obd2vpw_readdtc(info);
        break;
    case CommType_SCP:
    case CommType_SCP32:
        obd2scp_init(Obd2scpType_24Bit);
        status = obd2scp_readdtc(info);
        break;
    case CommType_KLINE:
    case CommType_KWP2000:
        status = obd2iso_readdtc(info,commtype);
        break;
    case CommType_SCIA:
    case CommType_SCIB:
        status = obd2sci_readdtc(info,commtype); 
        break;
    default:
        status = S_FAIL;
        break;
    }
    return status;
}

//------------------------------------------------------------------------------
// Input:   VehicleCommLevel vehiclecommlevel
//          u8  *dtc    dtc code ("P1000", etc)
// Output:  u8  *info   dtc info
// Return:  u8  status  (S_SUCCESS,S_FAIL)
// Engineer: Quyen Leba / Patrick Downs
// 
// This fucntion searches the new DTC files which are separated by commlevel.
//------------------------------------------------------------------------------
u8 obd2_getdtcinfo(VehicleCommLevel vehiclecommlevel, u8 *dtc, u8 *info)
{
    F_FILE  *dbfile;
    int     offset;
    int     i;
    char    dtcdb[20];
    char    *ptr;
    char    *ptr_end;
    char    buffer[2048];
    u32     filesize;
    u32     index;
    u32     bytecount;
    u8      status;

    memset(dtcdb, 0, sizeof(dtcdb));
    
    // Search for correct DTC files
    for(i=0;i<DTC_FILE_COUNT;i++)
    {
        if(dtc_file_lookup[i].vehiclecommlevel == vehiclecommlevel)
        {
            strcpy(dtcdb, dtc_file_lookup[i].filename);
            break;
        }
    }
    if(i==DTC_FILE_COUNT)
    {   
        return S_INPUT; // No file specified for vehiclecommlevel
    }
    
    dbfile = genfs_default_openfile((void*)dtcdb,"rb");
    if (dbfile == NULL)
    {
        return S_FAIL;
    }
    
    if (fseek(dbfile,0,SEEK_END) != 0)
    {
        genfs_closefile(dbfile);
        return S_SEEKFILE;
    }
    filesize = ftell(dbfile);
    if (fseek(dbfile,0,SEEK_SET) != 0)
    {
        genfs_closefile(dbfile);
        return S_SEEKFILE;
    }
    
    status = S_FAIL;
    index  = 0;
    while (index < filesize)
    {
        bytecount = fread((void*)buffer,1,2048,dbfile);
        if (bytecount == 0)
        {
            break;
        }
        
        ptr = strstr((char const*)buffer, (char const*)dtc);
        if(ptr != 0)
        {
            ptr_end = strchr((char*)ptr,0x0D);
            if(ptr_end == 0)
            {
                // Can't find carriage return;
                offset = sizeof(buffer) - (ptr - buffer);
                if (fseek(dbfile,-(offset),SEEK_CUR) != 0)
                {
                    break;
                }
            }
            else
            {
                // Read text description
                ptr = ptr + 5; // Skip past DTC code is 5 chars long
                while(ptr < ptr_end) // Skip past spaces
                {
                    if(isgraph(ptr[0]) != 0) 
                    {
                        break;
                    }
                    ptr++;
                }
                memcpy(info, ptr, (ptr_end - ptr));
                info[(ptr_end - ptr)] = 0;
                status = S_SUCCESS;
                break;   
            }
        }
    }
    
    genfs_closefile(dbfile);
    return status;
}

/**
 *  @brief Read generic PID (mode $01)
 *  
 *  @param [in]    broadcast        Use broadcast ECU identifier (for CAN only)
 *  @param [in]    ecm_id           ECU identifier (for CAN only)
 *  @param [in]    pid              PID number
 *  @param [in]    pidsize          PID size
 *  @param [out]   data             PID data
 *  @param [in]    vehiclecommtype  Vehicle comm type
 *
 *  @return Status
 */
u8 obd2_readpid_mode1(bool broadcast, u32 ecm_id, u8 pid, u8 pidsize, u8 *data, 
                      VehicleCommType vehiclecommtype)
{
    u8 datalength;
    u8 status = S_SUCCESS;

    switch (vehiclecommtype)
    {
    case CommType_CAN:
        if (!SETTINGS_IsDemoMode())
        {
            status = obd2can_read_data_bypid_mode1(broadcast, ecm_id, pid, data, &datalength, TRUE);
        }
        break;
    case CommType_VPW:
        if (!SETTINGS_IsDemoMode())
        {
            status = obd2vpw_read_data_bypid_mode1(pid, pidsize, data);
        }
        break;
    case CommType_SCP:
    case CommType_SCP32:
        if (!SETTINGS_IsDemoMode())
        {
            status = obd2scp_read_data_bypid_mode1(pid, pidsize, data);
        }
        break;
    case CommType_KLINE:
    case CommType_KWP2000:
        if (!SETTINGS_IsDemoMode())
        {
            status = obd2iso_read_data_bypid_mode1(pid, pidsize, data, vehiclecommtype);
        }
        break;
    default:
        status = S_NOTSUPPORT;
        break;
    }
    return status;    
}

/**
 *  @brief Process any vehicle specific tasks before program
 *  
 *  @param [in]    flasherinfo      Flasher info structure
 *
 *  @return Status
 */
u8 obd2_vehicle_specific_task_before_program(flasher_info *f_info)
{
    u8 status;

    switch (obd2_get_oemtype())
    {
#ifdef __DCX_MANUF__        
    case OemType_DCX:
        status = obd2_dcx_vehicle_specific_task_before_program(f_info);
        break;
#endif
#ifdef __FORD_MANUF__
    case OemType_FORD:
        status = S_SUCCESS;
        break;
#endif
#ifdef __GM_MANUF__
    case OemType_GM:
        status = S_SUCCESS;
        break;
#endif
    default:
        status = S_SUCCESS;
        break;
    }       
    return status;
}

/**
 *  @brief Process any vehicle specific tasks after program
 *  
 *  @param [in]    flasherinfo      Flasher info structure
 *
 *  @return Status
 */
u8 obd2_vehicle_specific_task_after_program(flasher_info *f_info)
{
    u8 status;

    switch (obd2_get_oemtype())
    {
#ifdef __DCX_MANUF__        
    case OemType_DCX:
        status = obd2_dcx_vehicle_specific_task_after_program(f_info);
        break;
#endif
#ifdef __FORD_MANUF__
    case OemType_FORD:
        status = obd2_ford_vehicle_specific_task_after_program(f_info);
        break;
#endif
#ifdef __GM_MANUF__
    case OemType_GM:
        status = S_SUCCESS;
        break;
#endif
    default:
        status = S_SUCCESS;
        break;
    }      
    return status;
}

//------------------------------------------------------------------------------
// Translate vehicle comm type to readable text
// Input:   VehicleCommType vehiclecommtype
// Output:  u8 *commtype_text
// Return:  u8 status
// Engineer: Quyen Leba
//------------------------------------------------------------------------------
u8 obd2_vehiclecommtotext(VehicleCommType vehiclecommtype, u8 *commtype_text)
{
    switch(vehiclecommtype)
    {
    case CommType_CAN:
        strcpy((void*)commtype_text,"CAN");
        break;
    case CommType_SCP:
        strcpy((void*)commtype_text,"SCP");
        break;
    case CommType_SCP32:
        strcpy((void*)commtype_text,"SCP32");
        break;
    case CommType_VPW:
        strcpy((void*)commtype_text,"VPW");
        break;
    case CommType_SCIA:
    case CommType_SCIB:
        strcpy((void*)commtype_text,"SCI");
        break;
    case CommType_KLINE:
        strcpy((void*)commtype_text,"K-LINE");
        break;
    case CommType_KWP2000:
        strcpy((void*)commtype_text,"KWP2000");
        break;
    default:
        strcpy((void*)commtype_text,"UNKNOWN");
        break;
    }
    return S_SUCCESS;
}

/**
 *  @brief Init bus by communication type
 *
 *  @param [in]    vehiclecommtype  Vehicle comm type
 *
 *  @return Status
 */
u8 obd2_bus_init_in_programming_mode(VehicleCommType vehiclecommtype)
{
    if (SETTINGS_IsDemoMode())
    {
        switch (vehiclecommtype)
        {
        case CommType_CAN:
        case CommType_VPW:
        case CommType_SCP:
        case CommType_SCP32:
        case CommType_KLINE:
        case CommType_KWP2000:
            return S_SUCCESS;
            break;
        default:
            return S_COMMTYPE;
            break;
        }
    }//if (SETTINGS_IsDemoMode())...

    switch (vehiclecommtype)
    {
    case CommType_CAN:
        obd2can_init(Obd2canInitProgram,0);
        break;
    case CommType_VPW:
        obd2vpw_init(TRUE, FALSE);
        break;
    case CommType_SCP:
        obd2scp_init(Obd2scpType_24Bit);
        break;
    case CommType_SCP32:
        obd2scp_init(Obd2scpType_32Bit);
        break;
    case CommType_KLINE:
    case CommType_KWP2000: 
        iso_init(&vehiclecommtype);
        break;
    default:
        return S_COMMTYPE;
        break;
    }
    return S_SUCCESS;
}

/**
 *  @brief Check if key is on
 *
 *  @param [in] vehiclecommtype  Vehicle comm type (SCAN all comm types if
 *                               vehiclecommtype = CommType_Unknown)
 *  @param [in] isFlash          Only check what is applicable to flashing
 *
 *  @return Status  S_SUCCESS - Key ON, S_NOCOMMUNICATION - Key OFF
 */
u8 obd2_checkkeyon(VehicleCommType vehiclecommtype, bool isFlash)
{
    bool isScan = FALSE; 
    u8 status;
    
    switch (vehiclecommtype)
    {
    case CommType_Unknown:
        isScan = TRUE;
    case CommType_CAN:
        obd2can_init(Obd2canInitProgram,0);
        status = obd2can_ping(Obd2CanEcuId_7E0, TRUE);
        if (status == S_TIMEOUT)
        {
            status = obd2can_ping(Obd2CanEcuId_7DF, TRUE);
        }
        if (status == S_SUCCESS)
        {
            break;
        }
        if (isScan == FALSE)
        {
            break;
        }
    case CommType_VPW:
        obd2vpw_init(TRUE, FALSE);
        status = obd2vpw_ping();
        if (status == S_SUCCESS || status == S_ERROR)
        {
            status = S_SUCCESS;
            break;
        }
        if (isScan == FALSE)
        {
            break;
        }
    case CommType_SCP:
    case CommType_SCP32:
        obd2scp_init(Obd2scpType_24Bit);
        status = obd2scp_ping();
        if (status == S_SUCCESS || status == S_ERROR)
        {
            status = S_SUCCESS;
            break;
        }
        if (isScan == FALSE)
        {
            break;
        }
    case CommType_SCIA:
    case CommType_SCIB:
        {
            VehicleCommLevel vehiclecommlevel;
            status = sci_init(&vehiclecommtype, &vehiclecommlevel);
            if (status == S_SUCCESS || status == S_ERROR)
            {
                status = S_SUCCESS;
                break;
            }
        }
        if (isScan == FALSE)
        {
            break;
        }
    case CommType_KLINE:
    case CommType_KWP2000: 
        if (isFlash == FALSE)
        {
            status = iso_init(&vehiclecommtype);
            if (status == S_SUCCESS || status == S_ERROR)
            {
                status = S_SUCCESS;
                break;
            }
            if (isScan == FALSE)
            {
                break;
            }
        }
        else
        {
            status = S_NOTSUPPORT;
        }
        break;
    default:
        if (vehiclecommtype == CommType_Unknown)
        {
            status = S_NOCOMMUNICATION;
        }
        else
        {
            status = S_NOTSUPPORT;
        }
        break;
    }
    
    return status;
}
 
/**
 *  @brief Check if key is on
 *
 *  @param [in]    vehiclecommtype  Vehicle comm type
 *  @param [in]    requirebusinit   TRUE = init bus
 *
 *  @return Status
 */
u8 obd2_checkkeyon_special(VehicleCommType vehiclecommtype, bool requirebusinit)
{
    u8  status;

    switch (vehiclecommtype)
    {
    case CommType_CAN:
        if (requirebusinit)
        {
            status = can_rx(1000);
            if(status == S_SUCCESS)
            {
                status = S_SUCCESS; 
            }
            else
            {
                status = S_FAIL;    
            }
        }
        else
        {
            status = obd2can_ping(Obd2CanEcuId_7E0, TRUE);
        }
        break;
    default:
        status = S_FAIL;
        break;
    }
    
    if (status != S_SUCCESS) 
    {
        status = S_FAIL;
    }
    
    return status;
}

/**
 *  @brief Check if a binary type is valid
 *
 *  @param [in]    binarytype   Vehicle binary type
 *
 *  @return Status
 */
u8 obd2_validate_ecmbinarytype(u16 veh_type)
{
    u8 status;

    status = vehdef_get_vehdef_info(&veh_type, 1, NULL);
    
    return status;
}

/**
*  @brief Set Vpp (used in upload/download for certain vehicles)
 *
 *  @return Status
 */
u8 obd2_setupvpp()
{
    u8 status;

    switch (obd2_get_oemtype())
    {
#ifdef __DCX_MANUF__        
    case OemType_DCX:
        status = S_SUCCESS;
        break;
#endif
#ifdef __FORD_MANUF__
    case OemType_FORD:
        status = obd2_ford_setupvpp();
        break;
#endif
#ifdef __GM_MANUF__
    case OemType_GM:
        status = S_SUCCESS;
        break;
#endif
    default:
        status = S_NOTSUPPORT;
        break;
    }      
    return status;
}

/**
 *  @brief Check for vehicle wake up
 *
 *  @param [in]     veh_type    Vehicle binary type
 *  @param [in]     ecm_index   ECM index
 *
 *  @return Status
 *
 *  @details    Some vehicles require this wakeup prior to an unlock; also, 
 *              some only allow this wakeup once.
 */
u8 obd2_wakeup(u16 veh_type, u16 ecm_index)
{
    u8 status;

    switch (obd2_get_oemtype())
    {
#ifdef __DCX_MANUF__        
    case OemType_DCX:
        status = obd2_dcx_wakeup(veh_type, ecm_index);
        break;
#endif
#ifdef __FORD_MANUF__
    case OemType_FORD:
        status = obd2_ford_wakeup(veh_type, ecm_index);
        break;
#endif
#ifdef __GM_MANUF__
    case OemType_GM:
        status = S_SUCCESS;
        break;
#endif
    default:
        status = S_NOTSUPPORT;
        break;
    }     
    return status;
}

/**
 *  @brief Setup bus prior to an upload (such as quiet bus, set programming 
 *         mode, etc)
 *
 *  @param [in]     ecm_type    index of ecm_defs
 *
 *  @return Status
 */
u8 obd2_setupbus_prior_upload(u16 ecm_type)
{
    u8 status;
    VehicleCommType vehiclecommtype;
    
    vehiclecommtype = ECM_GetCommType(ecm_type);

    
    switch (obd2_get_oemtype())
    {
#ifdef __DCX_MANUF__        
    case OemType_DCX:
        if(vehiclecommtype != CommType_Unknown)
        {
            status = S_SUCCESS; 
        }
        else
        {
            status = S_COMMTYPE; 
        }
        break;
#endif
#ifdef __FORD_MANUF__
    case OemType_FORD:
        switch(vehiclecommtype)
        {
        case CommType_CAN:
        case CommType_SCP:
        case CommType_SCP32:
            status = S_SUCCESS;
            break;
        default:
            status = S_COMMTYPE; 
            break;
        }
        break;
#endif
#ifdef __GM_MANUF__
    case OemType_GM:
        switch(vehiclecommtype)
        {
        case CommType_CAN:
                
            if (SETTINGS_IsDemoMode())
            {
                status = S_SUCCESS;
            }
            else
            {
                status = obd2can_gm_setup_prior_upload(ecm_type);
            }
            break;
        case CommType_VPW:
            status = obd2vpw_gm_setup_prior_upload(ecm_type);
            break;
        default:
            status = S_COMMTYPE;
            break;
        }break;
#endif
    default:
        status = S_NOTSUPPORT;
        break;
    }  
        
    return status;     
}

/**
 *  @brief Setup bus after an upload (return normal operations, etc)
 *
 *  @param [in]     ecm_type    index of ecm_defs
 *
 *  @return Status
 */
u8 obd2_setupbus_after_upload(u16 ecm_type)
{
    u8 status;
    VehicleCommType vehiclecommtype;
    
    vehiclecommtype = ECM_GetCommType(ecm_type);
    
    switch (obd2_get_oemtype())
    {
#ifdef __DCX_MANUF__        
    case OemType_DCX:
        if(vehiclecommtype != CommType_Unknown)
        {
            status = S_SUCCESS; 
        }
        else
        {
            status = S_COMMTYPE; 
        }
        break;
#endif
#ifdef __FORD_MANUF__
    case OemType_FORD:
        switch(vehiclecommtype)
        {
        case CommType_CAN:
        case CommType_SCP:
        case CommType_SCP32:
            status = S_SUCCESS;
            break;
        default:
            status = S_COMMTYPE; 
            break;
        }
        break;
#endif
#ifdef __GM_MANUF__
    case OemType_GM:
        switch(vehiclecommtype)
        {
        case CommType_CAN:
            if (SETTINGS_IsDemoMode())
            {
                status = S_SUCCESS;
            }
            else
            {
                status = obd2can_gm_setup_after_upload(ecm_type);
            }
            break;
        case CommType_VPW:
            status = obd2vpw_gm_setup_after_upload(ecm_type);
            break;
        default:
            status = S_COMMTYPE;
            break;
        }
        break;
#endif
    default:
        status = S_NOTSUPPORT;
        break;
    }  
    
    return status;     
}

/**
 *  @brief Setup bus prior to a download (such as quiet bus, set programming 
 *         mode, etc)
 *
 *  @param [in]     ecm_type    index of ecm_defs
 *
 *  @return Status
 */
u8 obd2_setupbus_prior_download(u16 ecm_type)
{
    u8 status;
    VehicleCommType vehiclecommtype;
    
    vehiclecommtype = ECM_GetCommType(ecm_type);
    
    switch (obd2_get_oemtype())
    {
#ifdef __DCX_MANUF__        
    case OemType_DCX:
        if(vehiclecommtype != CommType_Unknown)
        {
            status = S_SUCCESS; 
        }
        else
        {
            status = S_COMMTYPE; 
        }
        break;
#endif
#ifdef __FORD_MANUF__
    case OemType_FORD:
        switch(vehiclecommtype)
        {
        case CommType_CAN:
        case CommType_SCP:
        case CommType_SCP32:
            status = S_SUCCESS;
            break;
        default:
            status = S_COMMTYPE; 
            break;
        }
        break;
#endif
#ifdef __GM_MANUF__
    case OemType_GM:
        switch(vehiclecommtype)
        {
        case CommType_CAN:
            if (SETTINGS_IsDemoMode())
            {
                status = S_SUCCESS;
            }
            else
            {
                status = obd2can_gm_setup_prior_download(ecm_type);
            }
            break;
        case CommType_VPW:
            status = obd2vpw_gm_setup_prior_download(ecm_type);
            break;
        default:
            status = S_COMMTYPE;
            break;
        }
        break;
#endif
    default:
        status = S_NOTSUPPORT;
        break;
    }  
    
    return status;      
}

/**
 *  @brief Setup bus after a download (return normal operations, etc)
 *
 *  @param [in]     ecm_type    index of ecm_defs
 *
 *  @return Status
 */
u8 obd2_setupbus_after_download(u16 ecm_type)
{
    u8 status;
    VehicleCommType vehiclecommtype;
    
    vehiclecommtype = ECM_GetCommType(ecm_type);
    
    switch (obd2_get_oemtype())
    {
#ifdef __DCX_MANUF__        
    case OemType_DCX:
        if(vehiclecommtype != CommType_Unknown)
        {
            status = S_SUCCESS; 
        }
        else
        {
            status = S_COMMTYPE; 
        }
        break;
#endif
#ifdef __FORD_MANUF__
    case OemType_FORD:
        switch(vehiclecommtype)
        {
        case CommType_CAN:
        case CommType_SCP:
        case CommType_SCP32:
            status = S_SUCCESS;
            break;
        default:
            status = S_COMMTYPE; 
            break;
        }
        break;
#endif
#ifdef __GM_MANUF__
    case OemType_GM:
        switch(vehiclecommtype)
        {
        case CommType_CAN:
            if (SETTINGS_IsDemoMode())
            {
                status = S_SUCCESS;
            }
            else
            {
                status = obd2can_gm_setup_prior_download(ecm_type);
            }
            break;
        case CommType_VPW:
            status = obd2vpw_gm_setup_prior_download(ecm_type);
            break;
        default:
            //log_push_error_point(0x300E);
            status = S_COMMTYPE;
            break;
        }
        break;
#endif
    default:
        status = S_NOTSUPPORT;
        break;
    }  
    
    return status; 
}

/**
 *  @brief Search for vehicle type using the ecm_info data
 *
 *  @param [in]     ecm    data read from vehicle ECU
 *
 *  @return Status
 */
u8 obd2_vehicle_type_search(ecm_info *ecm)
{
    u8 status;

    obd2_open();

    switch (gObd2info.oemtype)
    {
#ifdef __DCX_MANUF__        
    case OemType_DCX:
        status = obd2_dcx_vehicle_type_search(ecm); 
        break;
#endif
#ifdef __FORD_MANUF__
    case OemType_FORD:
        status = S_FAIL;
        break;
#endif
#ifdef __GM_MANUF__
    case OemType_GM:
        status = obd2_gm_vehicle_type_search(ecm);
        break;
#endif
    default:
        status = S_NOTSUPPORT;
        break;
    }     
    return status;    
}

/**
 *  @brief Special Vehicle Functions
 *
 *  @param [in]     commtype    Vehicle comm type
 *  @param [in]     commlevel   Vehicle comm level
 *
 *  @return Status
 */
void obd2_vehiclefunction(VehicleCommType *commtype, VehicleCommLevel *commlevel)
{

    obd2_open();

    switch (gObd2info.oemtype)
    {
#ifdef __DCX_MANUF__        
    case OemType_DCX:
        obd2_dcx_vehiclefunction(commtype, commlevel);
        break;
#endif
#ifdef __FORD_MANUF__
    case OemType_FORD:
        obd2_ford_vehiclefunction(commtype, commlevel);
        break;
#endif
#ifdef __GM_MANUF__
    case OemType_GM:
        obd2_gm_vehiclefunction(commtype, commlevel);
        break;
#endif
    default:
        /* Do nothing */
        break;
    }     
    return;    
}

/**
 *  @brief Get OBD2 Info
 *  
 *  @param [in]  vehicleinfo        Vehicle info structure
 *  @param [out] vehicleinfo_text   Vehicle info text
 *
 *  @return status
 *  
 *  @details This function identifies and returns if applicable OBD2 information
 *           to the obd2info structure.
 */
u8 obd2_vehicleinfototext(vehicle_info *vehicleinfo, u8 *vehicleinfo_text)
{
    u8  commtype_text[32];
    u8  *ptr;
    u8  status;

    ptr = vehicleinfo_text;
    strcpy((char*)vehicleinfo->vin,(char*)vehicleinfo->ecm.vin);
    obd2_vehiclecommtotext(vehicleinfo->commtype,commtype_text);
    
    strcpy((char*)ptr,"<HEADER>VIN:</HEADER> ");
    status = obd2_validatevin(vehicleinfo->vin);
    if (status == S_VINBLANK || status == S_VININVALID)
    {
        strcat((char*)ptr,"Blank");
    }
    else if (status == S_SUCCESS)
    {
        strcat((char*)ptr,(char*)vehicleinfo->vin);
    }
    else
    {
        strcat((char*)ptr,"N/A");
    }

    strcat((char*)ptr,"<HEADER>COMM:</HEADER> ");
    strcat((char*)ptr,(char*)commtype_text);
   
    if (vehicleinfo->ecm.ecm_count > 0 && vehicleinfo->ecm.codecount[0] > 0 &&
        vehicleinfo->ecm.codes[0][0] != NULL)
    {
        switch (obd2_get_oemtype())
        {
        case OemType_FORD:
            strcat((char*)ptr,"<HEADER>ECU Strategy:</HEADER> ");
            break;
        case OemType_DCX:
        case OemType_GM:
            strcat((char*)ptr,"<HEADER>ECU Part Number:</HEADER> ");
            break;
        default:
            strcat((char*)ptr,"<HEADER>ECU CAL ID:</HEADER> ");
            break;
        }
        strcat((char*)ptr,(char*)vehicleinfo->ecm.codes[0][0]);
                
        if (vehicleinfo->ecm.ecm_count > 1 && vehicleinfo->ecm.codecount[1] > 0 &&
            vehicleinfo->ecm.codes[1][0] != NULL)
        {
            switch (obd2_get_oemtype())
            {
            case OemType_FORD:
                strcat((char*)ptr,"<HEADER>TCU Strategy:</HEADER> ");
                break;
            case OemType_DCX:
            case OemType_GM:
                strcat((char*)ptr,"<HEADER>TCU Part Number:</HEADER> ");
                break;
            default:
                strcat((char*)ptr,"<HEADER>TCU CAL ID:</HEADER> ");
                break;
            }
            strcat((char*)ptr,(char*)vehicleinfo->ecm.codes[1][0]);            
        }
    }
    return S_SUCCESS;
}

/**
 *  @brief Open OBD2 handler
 *
 *  @return status
 *  
 *  @details This function checks for valid OBD2 handle then calls get OBD2 info
 *           to identifify and returns if applicable OBD2 information to the
 *           global obd2info structure.
 */
u8 obd2_open()
{
    u8 status;

    status = obd2_getinfo(&gObd2info);

    return status;
}

/**
 *  @brief Close OBD2 handler
 *
 *  @return status
 *  
 *  @details This function "closes" the OBD2 handler.
 */
u8 obd2_close()
{
    obd2_info_init(&gObd2info);    
    return S_SUCCESS;
}

/**
 *  @brief Get OBD2 Info
 *  
 *  @param [in] obd2info OBD2 info structure
 *  @return status
 *  
 *  @details This function identifies and returns if applicable OBD2 information
 *           to the obd2info structure.
 */
u8 obd2_getinfo(obd2_info *obd2info)
{    
    float fval = 0;
    char buffer[64];
    u16 vehicle_type[ECM_MAX_COUNT];
    u8 lastcount = 0;
    u8 i;

    if (obd2info == NULL)
    {
        return S_INPUT;
    }

    if (SETTINGS_IsDemoMode())
    {
        obd2_close();
        return S_SUCCESS;
    }
    
    /* Check OBD2 voltage monitor; has a disconnect occured? */
    if (obd2info->status == S_OBD2_CONNECTED)
    {
        obd2_disconnect_monitor();
    }
    
    /* Check if already connected */
    if (obd2info->status == S_OBD2_CONNECTED)
    {
        return S_SUCCESS;
    }
    
    /* Initialize obd2info */
    obd2_info_init(obd2info);
    
    /* Check if currently disconnected */
    if (adc_read(ADC_VBAT, &fval) == S_SUCCESS)
    {
        /* If voltage is less than 9V, consider the OBD2 to be disconnected */
        if (fval < 9)
        {
            obd2info->status = S_OBD2_DISCONNECTED;
            log_push_error_point(0x3024);
            return S_NOCOMMUNICATION;
        }
    }
    
    /*------------------------------------------------------*/
    /* J2610 (Daimler-Chrysler SCI) */
    /*------------------------------------------------------*/
    obd2sci_getinfo(obd2info,CommType_Unknown,CommLevel_Unknown);
    
    /*------------------------------------------------------*/
    /* ISO15765 (CAN) */
    /*------------------------------------------------------*/
    if (obd2info->ecu_count < ECM_MAX_COUNT)
    {
        lastcount = obd2info->ecu_count;
        obd2can_getinfo(obd2info);
    }

    /*------------------------------------------------------*/
    /* J1850VPW (GM / Daimler-Chrysler CLASS2) */
    /*------------------------------------------------------*/
    if (obd2info->ecu_count < ECM_MAX_COUNT && obd2info->ecu_count == lastcount)
    {
        lastcount = obd2info->ecu_count;
        obd2vpw_getinfo(obd2info);
    }
    
    /*------------------------------------------------------*/
    /* J1850PWM (Ford SCP, Standard Corporate Protocol) */
    /*------------------------------------------------------*/
    if (obd2info->ecu_count < ECM_MAX_COUNT && obd2info->ecu_count == lastcount)
    {
        lastcount = obd2info->ecu_count;
        obd2scp_getinfo(obd2info);
    }
    
    /*------------------------------------------------------*/
    /* ISO9141 (K-Line) / ISO14230 (KWP2000) */
    /*------------------------------------------------------*/
    if (obd2info->ecu_count < ECM_MAX_COUNT && obd2info->ecu_count == lastcount)
    {
        obd2iso_getinfo(obd2info,CommType_Unknown);
    }
    
    if (obd2info->ecu_count > 0)
    {
        /* Reset voltage monitor */
        obd2_disconnect_monitor();
        obd2info->status = S_OBD2_CONNECTED;
    }
    else
    {
        obd2info->status = S_OBD2_DISCONNECTED;
        log_push_error_point(0x305E);
        return S_NOCOMMUNICATION;
    }
    
    /*------------------------------------------------------*/
    /* Determine vehicle type */
    /*------------------------------------------------------*/

    memset(vehicle_type,INVALID_VEH_DEF,sizeof(vehicle_type));
    memset(buffer,0,sizeof(buffer));
    for (i = 0; i < obd2info->ecu_count; i++)
    {
        strcpy(buffer,(char*)obd2info->ecu_block[i].partnumbers.generic.strategy);
        if (buffer[strlen(buffer)-1] == '*')
        {
            buffer[strlen(buffer)-1] = 0;
        }
        if (obd2tune_attempt_to_find_vehicle_type_using_tunecode((u8*)buffer, obd2info->vin,
                                                                 &vehicle_type[i]) == S_SUCCESS)
        {
            obd2info->ecu_block[i].vehicle_type = vehicle_type[i];
        }
    }

    vehdef_get_vehdef_info(vehicle_type, obd2info->ecu_count, &obd2info->vehicle_type);
        
    return S_SUCCESS;
}

/**
 *  @brief OBD2 voltage monitor task
 *  
 *  @details This function checks the OBD2 voltage to detect physical vehicle
 *           connection. This is used to determine if obd2_getinfo() needs to
 *           check for commtype and commlevel again.
 */
void obd2_disconnect_monitor()
{
    u8 status;

    /* Check OBD2 Voltage */
    status = mblexec_call(MBLEXEC_OPCODE_OBD2_VOLTAGE_MONITOR, NULL, 0,
                          NULL, NULL);
    
    gObd2info.status = (Obd2Status)status;
  
    return;
}

/**
 *  @brief Initialize OBD2 info block
 *  
 *  @param [in,out] obd2_info_block OBD2 info block
 *
 *  @return status
 */
u8 obd2_info_block_init(obd2_info_block *block)
{
    u8 status;
    
    if (block == NULL)
    {
        status = S_MALLOC;
    }
    else
    {
        block->ecutype = EcuType_Unknown;
        block->commtype = CommType_Unknown;
        block->commlevel = CommLevel_Unknown;
        block->genericcommlevel = GenericCommLevel_Unknown;
        block->ecu_id = 0xFFFFFFFF;
        block->ecu_response_id = 0xFFFFFFFF;
        block->vehicle_type = INVALID_VEH_DEF;
        block->partnumber_count = 0;
        memset(&block->partnumbers, 0, sizeof(block->partnumbers));
        status = S_SUCCESS;
    }
    return status;
}

/**
 *  @brief Initialize OBD2 info
 *  
 *  @param [in,out] obd2info OBD2 info
 *
 *  @return Nothing to return
 */
void obd2_info_init(obd2_info *obd2info)
{
    u8 i;
    
    memset(obd2info, 0, sizeof(obd2_info));
    obd2info->ecu_count = 0;
    obd2info->oemtype = OemType_Unknown;
    obd2info->vin[0] = 0;
    obd2info->vehicle_type = INVALID_VEH_DEF;
    obd2info->status = S_OBD2_DISCONNECTED;
    obd2info->state = S_OBD2_STATE_NORMAL;
    for (i = 0; i < ECM_MAX_COUNT; i++)
    {            
        obd2_info_block_init(&obd2info->ecu_block[i]);
    }
}

/**
 *  @brief Get OBD2 Info Block by type
 *  
 *  @param [in]  obd2info   OBD2 info structure
 *  @param [in]  ecutype    Type of info block to get
 *  @param [out] block      OBD2 info block
 *
 *  @return status
 *  
 *  @details This function adds a new obd2 info block.
 */
u8 obd2_info_get_block_by_type(obd2_info *obd2info, EcuType ecutype, obd2_info_block *block)
{
    u8 i;
    u8 status;
    
    if (obd2info == NULL || block == NULL)
    {
        status = S_INPUT;
    }
    else
    {
        status = S_UNMATCH;
        for (i = 0; i < ECM_MAX_COUNT; i++)
        {
            if (obd2info->ecu_block[i].ecutype == ecutype)
            {
                obd2_info_block_init(block);
                memcpy(block, &obd2info->ecu_block[i], sizeof(obd2_info_block));
                status = S_SUCCESS;
                break;
            }
        }
    }
    return status;
}

/**
 *  @brief Return OEM type of ECU
 *
 *  @return OEM Type
 */
OemType obd2_get_oemtype()
{
    /* TODOT: Save and read oem_type from settingtune */
    return OEM_TYPE;
}
