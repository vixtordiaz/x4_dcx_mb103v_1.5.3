/**
  **************** (C) COPYRIGHT 2011 SCT Performance, LLC *********************
  * File Name          : obd2def.h
  * Device / System    : SCT products using the "CommonCode" platform
  * Author             : Quyen Leba
  *
  * Version            : 1 
  * Date               : 05/02/2011
  * Description        : 
  *                    : 
  *
  *
  * History            : 05/02/2011 M. Davis
  *                    :   Add SCT Copyright header,
  *
  ******************************************************************************
  */

#ifndef __OBD2DEF_H
#define __OBD2DEF_H

#include <arch/gentype.h>

#define ECM_MAX_COUNT               3
#define ECM_MAX_PART_COUNT          10

#define MAX_TUNECODE_LENTH          31
#define MIN_TUNECODE_LENTH          4
#define VIN_LENGTH                  17
#define CALID_LENGTH                MAX_TUNECODE_LENTH

#define MAX_TUNE_CODE_LIST_COUNT    20
#define TUNECODE_SEPARATOR          '*'
#define TUNECODE_SEPARATOR_STRING   "*"
#define TUNECODE_TERMINATOR         '_'
#define TUNECODE_TUNEMASK_INFILE    '*'     /* Used to separate tunecode */
#define TUNECODE_TUNEMASK_INCODE    '^'

#define CAN_PADDING_BYTE_DEFAULT    0x00
#define CAN_PADDING_BYTE_DCX        0x00
#define CAN_PADDING_BYTE_FORD       0x00
#define CAN_PADDING_BYTE_GM         0xAA

#define MAX_VEHICLE_FUNCTIONS       10 
#define DTC_MAX_COUNT               128

typedef enum
{
    WriteDataBlock                  = 0x3B,
    ReadDataBlock                   = 0x3C,
    TesterPresent                   = 0x3F,
    GeneralResponseMessage          = 0x7F,
}Obd2FunctionCode;

typedef enum
{
    OemType_FORD                    = 0,
    OemType_GM                      = 1,
    OemType_DCX                     = 2,
    OemType_Unknown                 = 0xFF,
}OemType;

typedef enum
{
    EcuType_ECM                     = 0,
    EcuType_TCM                     = 1,
    EcuType_Unknown                 = 0xFF,
}EcuType;

typedef enum
{
    CommType_SCP                    = 0,
    CommType_SCP32                  = 1,
    CommType_VPW                    = 2,
    CommType_CAN                    = 3,
    CommType_MSCAN                  = 4,
    CommType_SCIB                   = 5,
    CommType_KLINE                  = 6,
    CommType_SCIA                   = 7,
    CommType_KWP2000                = 8,
    CommType_Unknown                = 0xFF,
}VehicleCommType;

typedef enum
{
    CommLevel_KWP2000               = 0,
    CommLevel_ISO14229              = 1,
    CommLevel_GMLAN                 = 2,
    CommLevel_SCIA                  = 3,
    CommLevel_SCIB                  = 4,
    CommLevel_SBECA                 = 5,
    CommLevel_SBECB                 = 6,
    CommLevel_SAE                   = 160,
    CommLevel_Unknown               = 0xFF,
}VehicleCommLevel;

typedef enum
{
    GenericCommLevel_Legacy         = 0,
    GenericCommLevel_UDS            = 1,
    GenericCommLevel_Unknown        = 0xFF,
}GenericCommLevel;

typedef enum
{
    VehicleFunction_CrankRelearn            = 0x00,
    VehicleFunction_KAM_Reset               = 0x01,
    VehicleFunction_Clear_TCM_Adaptives     = 0x02,
    VehicleFunction_Traction_Ctrl           = 0x03,
    VehicleFunction_Reset_ECM               = 0x04,
    VehicleFunction_Tire_size_Axle_Ratio    = 0x05,
    VehicleFunction_Bootloader_Info         = 0x06,
    VehicleFunction_ETC_Relearn             = 0x07,
    VehicleFunction_EOL                     = 0xFF, /* End of List */
}VehicleFunction; 

//TODOQ: this is ugly
#if __FORD_DEVICE__
#define CommLevel_Default           CommLevel_KWP2000
#elif __GM_DEVICE__
#define CommLevel_Default           CommLevel_GMLAN
#elif __DCX_DEVICE__
#define CommLevel_Default           CommLevel_KWP2000
#else
#error obd2def.h: INVALID_DEVICE
#endif

typedef union
{
    struct
    {
        u8  mode        : 4;
        u8  id29bit     : 1;
        u8  reserved    : 3;
        u8  padding_byte;
    }can;
    struct
    {
        u8  highspeed   : 1;
        u8  reserved    : 7;
        u8  x;
    }scp;
    struct
    {
        u8  boosted     : 1;
        u8  highspeed   : 1;
        u8  reserved    : 6;
        u8  x;
    }vpw;
    struct
    {
      u8 config_type    : 4;
      u8 speed          : 2;
      u8 reserved0      : 2;
      u8 reserved1;
    }sci;
    struct
    {
        u16 value;
    }raw;
}VehicleCommConfig;

typedef enum
{
    Diesel                          = 0,
    Gasoline                        = 1,
    UnknownFuelType                 = 0xFF,
}FuelType;

typedef enum
{
    Address_24_Bits                 = 0,
    Address_32_Bits                 = 1,
    Address_None                    = 2,
}MemoryAddressType;

typedef enum
{
    MemorySize_2_Byte               = 0,
    MemorySize_1_Byte               = 1,
}MemorySize;

typedef enum
{
    Request_3_Byte                  = 0,
    Request_4_Byte                  = 1,
}RequestLengthType;

typedef struct
{
    u8 count;
    u16 codes[DTC_MAX_COUNT];
    u8  codestatus[DTC_MAX_COUNT];
    u8  code_ecmindex[DTC_MAX_COUNT];
}dtc_info;



typedef struct
{
    u8 ecm_count;
    u8 codecount[ECM_MAX_COUNT];
    u8 codeids[ECM_MAX_COUNT][ECM_MAX_PART_COUNT];
    u8 codes[ECM_MAX_COUNT][ECM_MAX_PART_COUNT][MAX_TUNECODE_LENTH+1];
    u8 second_codes[ECM_MAX_COUNT][ECM_MAX_PART_COUNT][MAX_TUNECODE_LENTH+1];
    /* Must be the same size as Settings_Tune.vehicle_serial_number */
    u8 vehicle_serial_number[32];
    u8 hwid[16];
    u8 bcc[4+1];
    u8 vin[VIN_LENGTH+1];                       /* Only used within ecm_info */
    VehicleCommType commtype[ECM_MAX_COUNT];
    VehicleCommLevel commlevel[ECM_MAX_COUNT];  /* TODOQ: added this cause changes to tunehist.bak */
}ecm_info;
STRUCT_SIZE_CHECK(ecm_info,2031);

typedef struct
{
    ecm_info ecm;
    u8 vin[VIN_LENGTH+1];
    VehicleCommType commtype;
    VehicleCommLevel commlevel;
}vehicle_info;
STRUCT_SIZE_CHECK(vehicle_info,2051);

typedef enum
{
    Obd2CanEcuId_7E0        = 0x000007E0,
    Obd2CanEcuId_7E1        = 0x000007E1,
    Obd2CanEcuId_7E2        = 0x000007E2,
    Obd2CanEcuId_7DF        = 0x000007DF,
    Obd2CanEcuId_Undetected = 0xFFFFFFFF,
}Obd2CanEcuId;
        
typedef enum
{
    S_OBD2_DISCONNECTED     = 0x00,
    S_OBD2_CONNECTED        = 0x01,
}Obd2Status;

typedef enum
{
    S_OBD2_STATE_NORMAL     = 0x00,
    S_OBD2_STATE_DEAD       = 0x01,
}Obd2State;

typedef struct
{
    u8 part_number[16];             /* codes[ecm_index][0] */
    u8 variant_id;                  /* second_codes[ecm_index][0] */
    u8 part_type_description[16];   /* second_codes[ecm_index][1] */
    u8 part_type_number;            /* second_codes[ecm_index][2] */
    u8 boot_version[16];            /* second_codes[ecm_index][3] */
}obd2_partnumbers_dcx;

typedef struct
{
    u8 strategy[MAX_TUNECODE_LENTH+1];              /* codes[ecm_index][0] */
    u8 sw_part_number[MAX_TUNECODE_LENTH+1];        /* second_codes[ecm_index][0] */
    u8 hw_part_number[MAX_TUNECODE_LENTH+1];        /* second_codes[ecm_index][1] */
    u8 module_part_number[MAX_TUNECODE_LENTH+1];    /* second_codes[ecm_index][2] */
}obd2_partnumbers_ford;

typedef struct
{
    u8 partnumber[10][16];                          /* codes[0] - codes[9] */
    u8 partnumber_id[10];                           /* codeids[0] - codeids[9] */
    u8 vehicle_serial_number[32];
    u8 hwid[16];
    u8 bcc[4+1];
}obd2_partnumbers_gm;

typedef struct
{
    u8 strategy[MAX_TUNECODE_LENTH+1];              /* codes[ecm_index][0] */
}obd2_partnumbers_generic;

typedef struct
{
    EcuType ecutype;
    VehicleCommType commtype;
    VehicleCommLevel commlevel;
    GenericCommLevel genericcommlevel;
    u32 ecu_id;
    u32 ecu_response_id;
    u16 vehicle_type;    /* Non-stacked vehicle type */
    u8 partnumber_count;
    union
    {
        obd2_partnumbers_dcx     dcx;
        obd2_partnumbers_ford    ford;
        obd2_partnumbers_gm      gm;
        obd2_partnumbers_generic generic;
    } partnumbers;
}obd2_info_block; /* Currently 248 Bytes */

typedef struct
{
    u8 ecu_count;
    OemType oemtype;
    u8 vin[VIN_LENGTH+1];
    u16 vehicle_type;   /* Stacked vehicle type */
    Obd2Status status;
    Obd2State state;
    obd2_info_block ecu_block[ECM_MAX_COUNT];
}obd2_info;

#endif  /* __OBD2DEF_H */
