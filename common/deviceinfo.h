/**
  **************** (C) COPYRIGHT 2011 SCT Performance, LLC *********************
  * File Name          : deviceinfo.h
  * Device / System    : SCT products using the "CommonCode" platform
  * Author             : Quyen Leba
  *
  * Version            : 1 
  * Date               : 05/02/2011
  * Description        : 
  *                    : 
  *
  *
  * History            : 05/02/2011 M. Davis
  *                    :   Add SCT Copyright header,
  *
  ******************************************************************************
  */

#ifndef __DEVICEINFO_H
#define __DEVICEINFO_H

#include <arch/gentype.h>

u8 deviceinfo_getinfostring(u8 *info, u32 maxinfolength);
u8 deviceinfo_getmarketstring(MarketType type, u8* returnbuffer, u32 retbuffermaxsize);
u8 deviceinfo_getVBfirmwareversion_string(u8 *fw_version_str);
u8 deviceinfo_getABfirmwareversion_string(u8 *fw_version_str);
u8 deviceinfo_append_info(u8 *info, u32 maxinfolength, u32 *currentlength, 
                          u8 *headerstring, u8 *infostring);

#endif    //__DEVICEINFO_H
