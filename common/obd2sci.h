/**
  **************** (C) COPYRIGHT 2011 SCT Performance, LLC *********************
  * File Name          : obd2sci.h
  * Device / System    : SCT products using the "CommonCode" platform
  * Author             : Ricardo Cigarroa
  *
  * Version            : 1 
  * Date               : 11/14/2014
  * Description        : 
  *                    : 
  *
  ******************************************************************************
  */
#ifndef __OBD2sci_H
#define __OBD2sci_H

#include <arch/gentype.h>
#include "obd2.h"
#include "obd2tune.h"
#include <common/obd2def.h>

/*
  SCI/ ISO TX Flags
  bit 0 = 1 =send SCIB / 0 = send SCIA
  bit 1 = 1 check for 0x22 answer in SCIA / 0 do not check
  bit 2 = 1 use checksum / 0 do not use checksum

 SCI/ ISO RX Flags
 bit 0 = 1 = RX SCIB / 0 = RX SCIA
 bit 1 = 1 = skip 0x80 on RX / 0 = do not skip 0x80 on RX
 bit 2 = 1 = check checksum / 0 = do not check checksum
 bit 3 = 1 = wait extra time / 0 = do not wait extra time
 */

typedef enum
{
    sci_tx_flags_none               = 0,
    sci_tx_flags_configA_0x22       = 2,
    sci_tx_flags_has_check_crc      = 4,
        
}SCI_TX_FLAGS;

typedef enum
{
    sci_rx_flags_none               = 0,
    sci_rx_flags_skip80             = 2,
    sci_rx_flags_has_check_crc      = 4,
    sci_rx_flags_wait_extra_time    = 8,
}SCI_RX_FLAGS;

typedef struct
{
    u8   service; 
    u8   subservice; 
    u8   service_controldatabuffer[5];
    u8   service_controldatalength;
    u8   *rxbuffer;
    u16  rxlength;
    u8   *txbuffer; 
    u16  txlength; 
    u16  txflags;
    u16  rxflags; 
    u16  expected_rxbufferlength;
    bool response_expected;
    VehicleCommType vehiclecommtype;
    VehicleCommLevel vehiclecommlevel; 
    u32  rx_timeout_ms;
}Obd2sci_ServiceData;


void obd2sci_servicedata_init(Obd2sci_ServiceData *servicedata);

u8 obd2sci_txrx(Obd2sci_ServiceData *servicedata);
u8 obd2sci_tx(Obd2sci_ServiceData *servicedata);
u8 obd2sci_rx(Obd2sci_ServiceData *servicedata); 

u8 obd2sci_readvin(u8 *vin, VehicleCommType commtype, VehicleCommLevel commlevel);
u8 obd2sci_readecminfo(ecm_info *ecm);
u8 obd2sci_getinfo(obd2_info *obd2info, VehicleCommType commtype, 
                   VehicleCommLevel vehiclecommlevel);
u8 obd2sci_read_part_type(VehicleCommLevel *vehiclecommlevel); 
u8 obd2sci_getpartnumber(obd2_info_block *block);
u8 obd2sci_get_sci_commtype_commlevel(VehicleCommType *vehiclecommtype,
                                      VehicleCommLevel *vehiclecommlevel);
u8 obd2sci_cleardtc(VehicleCommType commtype);
u8 obd2sci_readdtc(dtc_info *info, VehicleCommType commtype);

u8 obd2sci_getpartnumber_scia(obd2_info_block *block);
u8 obd2sci_getpartnumber_scib(obd2_info_block *block);

u8 obd2sci_readvin_scia(u8 *vin, VehicleCommType commtype, VehicleCommLevel commlevel);
u8 obd2sci_readvin_scib(u8 *vin, VehicleCommType commtype, VehicleCommLevel commlevel);
u8 obd2sci_readvin_sbec(u8 *vin, VehicleCommType commtype, VehicleCommLevel commlevel);

u8 obd2sci_cleardtc_scia();
u8 obd2sci_cleardtc_scib();

u8 obd2sci_readdtc_scia(dtc_info *info, VehicleCommType commtype);
u8 obd2sci_readdtc_scib(dtc_info *info, VehicleCommType commtype);

#endif