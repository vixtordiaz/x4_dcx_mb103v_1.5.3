/**
  **************** (C) COPYRIGHT 2011 SCT Performance, LLC *********************
  * File Name          : dcx_ecm_utility_defs.c
  * Device / System    : SCT products using the "CommonCode" platform
  * Author             : Quyen Leba
  *
  * Version            : 1 
  * Date               : 05/02/2011
  * Description        : 
  *                    : 
  *
  *
  * History            : 05/02/2011 M. Davis
  *                    :   Add SCT Copyright header,
  *
  ******************************************************************************
  */

#include "dcx_ecm_utility_testfunc.h"
#include "dcx_ecm_utility_defs.h"



//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
const utility_info utilityinfolist[] =
{
  //TODO: start UBF count from 400. ex UBF400.ubf, UBF401.ubf, etc..   
    [0] =
    {
        //2007-2008 NGC4 (ECM) (CAN)                                             //0x00
        //"SCT_NGC4.bin"
        "UBF200.ubf",UTILITY_FLAGS_USE_ENCRYPTION,0x0,
        .utilityfunctest = NULL,
        .utility_block =
        {
            {0x00800000, 0x00014D6, ORDER_0 | MEMBLOCK_FLAGS_NONE, 0},
            {0, 0, ORDER_1 | MEMBLOCK_FLAGS_NONE, 0},
            {0, 0, ORDER_2 | MEMBLOCK_FLAGS_NONE, 0},
            {0, 0, ORDER_3 | MEMBLOCK_FLAGS_NONE, 0},
            {0, 0, ORDER_4 | MEMBLOCK_FLAGS_NONE, 0},
        },
    },
    [1] =
    {
        //2013 GPEC2 (ECM) (CAN)                                       //0x01
        //"GPEC2_BL_2013A.bin"
        "UBF201.ubf",UTILITY_FLAGS_USE_ENCRYPTION | UTILITY_FLAGS_SEPARATE_EXE,0x0,
        .utilityfunctest = (funcptr_utilfunctest*)&dcx_ecm_utility_testfunc_gpec2,
        .utility_block =
        {
            {0x040000, 0x280000, ORDER_0 | MEMBLOCK_FLAGS_NONE, 0},
            {0x2C0000, 0x40000, ORDER_1 | MEMBLOCK_FLAGS_NONE, 0},
            {0, 0, ORDER_2 | MEMBLOCK_FLAGS_NONE, 0},
            {0, 0, ORDER_3 | MEMBLOCK_FLAGS_NONE, 0},
            {0, 0, ORDER_4 | MEMBLOCK_FLAGS_NONE, 0},
        },
    },
    [2] =
    {
        //2014 GPEC2 (ECM) (CAN)                                       //0x02
        //"GPEC2_BL_2014A.bin"
        "UBF202.ubf",UTILITY_FLAGS_USE_ENCRYPTION | UTILITY_FLAGS_SEPARATE_EXE,0x0,
        .utilityfunctest = NULL,
        .utility_block =
        {
            {0x040000, 0x240000, ORDER_0 | MEMBLOCK_FLAGS_NONE, 0},
            {0x280000, 0x80000, ORDER_1 | MEMBLOCK_FLAGS_NONE, 0},
            {0, 0, ORDER_2 | MEMBLOCK_FLAGS_NONE, 0},
            {0, 0, ORDER_3 | MEMBLOCK_FLAGS_NONE, 0},
            {0, 0, ORDER_4 | MEMBLOCK_FLAGS_NONE, 0},
        },
    },
    [3] =
    {
        //2009-2010 NGC4A (ECM) (CAN)                                       //0x03
        //"SCT_NGC4A.bin"
        "UBF203.ubf",UTILITY_FLAGS_USE_ENCRYPTION,0x0,
        .utilityfunctest = NULL,
        .utility_block =
        {
            {0x00800000, 0x00014D6, ORDER_0 | MEMBLOCK_FLAGS_NONE, 0},
            {0, 0, ORDER_1 | MEMBLOCK_FLAGS_NONE, 0},
            {0, 0, ORDER_2 | MEMBLOCK_FLAGS_NONE, 0},
            {0, 0, ORDER_3 | MEMBLOCK_FLAGS_NONE, 0},
            {0, 0, ORDER_4 | MEMBLOCK_FLAGS_NONE, 0},
        },
    },

};

