/**
  **************** (C) COPYRIGHT 2011 SCT Performance, LLC *********************
  * File Name          : obd2can_dcx_datalog.c
  * Device / System    : SCT products using the "CommonCode" platform
  * Author             : Quyen Leba
  *
  * Version            : 1 
  * Date               : 05/02/2011
  * Description        : 
  *                    : 
  *
  *
  * History            : 05/02/2011 M. Davis
  *                    :   Add SCT Copyright header,
  *
  ******************************************************************************
  */

//------------------------------------------------------------------------------
// Log error point: 0x4900 -> 0x4AFF (shared with obd2datalog.c)
// Use 0x4980 -> 0x499F (shared with obd2datalog_ford.c)
//------------------------------------------------------------------------------

#include <string.h>
#include <board/rtc.h>
#include <common/statuscode.h>
#include <common/obd2datalog.h>
#include <common/obd2can.h>
#include "obd2can_dcx_datalog.h"

extern Datalog_Mode gDatalogmode;       //from obd2datalog.c
extern Datalog_Info *dataloginfo;       //from obd2datalog.c

//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
void obd2can_dcx_datalog_sendtesterpresent()
{
    u8  i;
    const u32 ecm_id[ECM_MAX_COUNT] = {Obd2CanEcuId_7E0,Obd2CanEcuId_7E1,0};
    Obd2can_ServiceData servicedata;

    obd2can_servicedata_init(&servicedata);
    servicedata.ecm_id = 0x7DF;
    servicedata.service = 0x3E;
    servicedata.subservice = 0x80;
    servicedata.response_expected = FALSE;
    for(i=0;i<ECM_MAX_COUNT;i++)
    {
        if (dataloginfo->commlevel[i] == CommLevel_ISO14229)
        {
            servicedata.ecm_id = 0x7DF; //TODOQ2: check this again
            servicedata.subservice = 0x80;
            servicedata.commlevel = CommLevel_ISO14229;
        }
        else if (dataloginfo->commlevel[i] == CommLevel_KWP2000)
        {
            servicedata.ecm_id = ecm_id[i];
            servicedata.subservice = 0x02;
            servicedata.commlevel = CommLevel_KWP2000;
        }
        else if (dataloginfo->commlevel[i] == CommLevel_Unknown)
        {
            break;
        }
        obd2can_txrx(&servicedata);
    }
}

//------------------------------------------------------------------------------
// Check if all signals fit in available packets
// Input:   VehicleCommLevel commlevel[ECM_MAX_COUNT]
// Return:  u8  status
// Engineer: Tristen Pierson
// TODO: Add support for ISO14229
//------------------------------------------------------------------------------
u8 obd2can_dcx_datalog_evaluatesignalsetup(VehicleCommLevel *vehiclecommlevel)
{
    u16 packetid;
    u8  byteavailable[2][DCX_KWP_CAN_PACKETID_END-DCX_KWP_CAN_PACKETID_START];
    u8  processcheck[MAX_DATALOG_DLXBLOCK_COUNT];
    u8  ecm_index;
    u8  pidcount;
    u8  processedperloop;
    u8  index;
    u16 i;
    bool packet;
    
    if (dataloginfo == NULL)
    {
        log_push_error_point(0x49A0);
        return S_BADCONTENT;
    }
    if (vehiclecommlevel[0] == CommLevel_Unknown)
    {
        gDatalogmode.packetmode = SingleRate;
    }
        
    memset(processcheck,FALSE,sizeof(processcheck));
    memset((char*)byteavailable,DCX_KWP_CAN_MAX_DATABYTE_PER_PACKET,sizeof(byteavailable));
    
    dataloginfo->pidsignalcount = 0;
    dataloginfo->dmrsignalcount = 0;
    dataloginfo->analogsignalcount = 0;
    dataloginfo->oscsignalcount = 0;
    dataloginfo->packetidcount[0] = 0;
    dataloginfo->packetidcount[1] = 0;
    
    // Evaluate non-packet signals
    for(i=0;i<dataloginfo->datalogsignalcount;i++)
    {
        if ((dataloginfo->datalogsignals[i].Control & DATALOG_CONTROL_SKIP) == 0)
        {
            switch (dataloginfo->datalogsignals[i].PidType)
            {
                case PidTypeAnalog:
                    if (dataloginfo->analogsignalcount < dataloginfo->analogsignalmaxavailable)
                    {
                        dataloginfo->analogsignalindex[dataloginfo->analogsignalcount++] = i;
                    }
                   break;
                case PidTypeMode1:
                    dataloginfo->pidsignalcount++;
                    break;
            }
        }
        if (dataloginfo->pidsignalcount + dataloginfo->analogsignalcount + 
            dataloginfo->oscsignalcount >= dataloginfo->datalogsignalcount)
        {
            break;
        }
    } 
   
    // Evaluate Rapid Packet Signals
    if (gDatalogmode.packetmode == RapidRate)   
    {        
        // Fill datalog table and packets if rapid packet supported
        for(ecm_index=0;ecm_index<2;ecm_index++)
        {
            pidcount = 0;
            index = 0;
            packetid = DCX_KWP_CAN_PACKETID_START;
                            
            while(packetid != DCX_KWP_CAN_PACKETID_END)
            {                
                // Are all signals accounted for?
                if (dataloginfo->pidsignalcount + dataloginfo->dmrsignalcount + 
                    dataloginfo->analogsignalcount + dataloginfo->oscsignalcount >= 
                    dataloginfo->datalogsignalcount)
                { 
                    break;
                }
                
                processedperloop = 0;
                packet = FALSE;
                
                for(i=0;i<dataloginfo->datalogsignalcount;i++)
                {                
                    if (processcheck[i])
                    {
                        // Already processed
                        continue;
                    }
                    if (pidcount >= DCX_KWP_CAN_MAX_PID_PACKET)
                    {
                        // All available items been assigned to packets
                        break;
                    }
                    if ((dataloginfo->datalogsignals[i].Control & DATALOG_CONTROL_SKIP) == 0 &&
                        dataloginfo->datalogsignals[i].SignalType.Generic.PacketNumber == DATALOG_INVALID_PACKETID &&
                        dataloginfo->datalogsignals[i].EcmIndex == ecm_index &&
                        dataloginfo->datalogsignals[i].Size <= byteavailable[ecm_index][index] &&
                        (dataloginfo->datalogsignals[i].PidType == PidTypeRegular ||
                        dataloginfo->datalogsignals[i].PidType == PidTypeDMR))
                    {                    
                        processcheck[i] = TRUE;
                        processedperloop++;
                        switch (dataloginfo->datalogsignals[i].PidType)
                        {
                            case PidTypeRegular:
                                dataloginfo->pidsignalcount++;
                                break;
                            case PidTypeDMR:                               
                                dataloginfo->dmrsignalindex[dataloginfo->dmrsignalcount++] = i;
                                break;
                            default:
                                break;
                        }
                        dataloginfo->datalogsignals[i].SignalType.Generic.Position = DCX_KWP_CAN_MAX_DATABYTE_PER_PACKET - byteavailable[ecm_index][index];
                        byteavailable[ecm_index][index] -= dataloginfo->datalogsignals[i].Size;
                        dataloginfo->datalogsignals[i].SignalType.Generic.PacketNumber = packetid;
                        packet = TRUE;
                        pidcount++;
                    } 
                }//for(i=...
                if (packet == TRUE)
                {                
                    dataloginfo->packetidlist[ecm_index][dataloginfo->packetidcount[ecm_index]] = packetid;
                    dataloginfo->packetidcount[ecm_index]++;
                    index++;
                    packetid++;
                }
                if (processedperloop == 0 || pidcount >= DCX_KWP_CAN_MAX_PID_PACKET)
                {
                    break;
                }
            }//while(packetid...
            // Are all signals accounted for?
            if (dataloginfo->pidsignalcount + dataloginfo->dmrsignalcount + 
                dataloginfo->analogsignalcount + dataloginfo->oscsignalcount >= 
                dataloginfo->datalogsignalcount)
            { 
                break;
            }
        }//for(ecm_index=...
    }
    // If not enough packets, evaulate for single rate
    if (dataloginfo->pidsignalcount + dataloginfo->dmrsignalcount + 
        dataloginfo->analogsignalcount + dataloginfo->oscsignalcount < 
        dataloginfo->datalogsignalcount)
    { 
        // Evaluate non-packet signals
        for(i=0;i<dataloginfo->datalogsignalcount;i++)
        {
            if ((dataloginfo->datalogsignals[i].Control & DATALOG_CONTROL_SKIP) == 0 &&
                dataloginfo->datalogsignals[i].SignalType.Generic.PacketNumber == DATALOG_INVALID_PACKETID)
            {
                switch (dataloginfo->datalogsignals[i].PidType)
                {
                    case PidTypeRegular:
                        dataloginfo->pidsignalcount++;                
                        break;
                    case PidTypeDMR:
                        dataloginfo->dmrsignalcount++;
                        break;
                }
            }        
            if (dataloginfo->pidsignalcount + dataloginfo->dmrsignalcount + 
                dataloginfo->analogsignalcount + dataloginfo->oscsignalcount >= 
                dataloginfo->datalogsignalcount)
            {
                break;
            }
        }
    }
    if (dataloginfo->pidsignalcount + dataloginfo->dmrsignalcount + 
        dataloginfo->analogsignalcount + dataloginfo->oscsignalcount < 
        dataloginfo->datalogsignalcount)
    {
        // Cannot fit all signals
        log_push_error_point(0x49A1);
        return S_NOTFIT;
    }    
    return S_SUCCESS;
}

//------------------------------------------------------------------------------
// Change Datalog Diagnostic Mode
// Input:   VehicleCommLevel vehiclecommlevel[ECM_MAX_COUNT]
// Return:  u8  status
// Engineer: Quyen Leba
//------------------------------------------------------------------------------
u8 obd2can_dcx_datalog_change_diagnostic_mode(VehicleCommLevel *vehiclecommlevel)
{
    u8  i;

    for(i=0;i<ECM_MAX_COUNT;i++)
    {
        if (vehiclecommlevel[i] == CommLevel_ISO14229)
        {
        }
        else if (vehiclecommlevel[i] == CommLevel_KWP2000)
        {
            //do nothing
        }
        else if (vehiclecommlevel[i] == CommLevel_Unknown)
        {
            break;
        }
        else
        {
            log_push_error_point(0x4984);
            return S_NOTSUPPORT;
        }
    }

    return S_SUCCESS;
}


//------------------------------------------------------------------------------
// Clean up previous defined packets
// Input:   VehicleCommLevel vehiclecommlevel[ECM_MAX_COUNT]
// Return:  u8  status
// Engineer: Quyen Leba
//------------------------------------------------------------------------------
u8 obd2can_dcx_datalog_cleanup_packets(u32 ecm_id, VehicleCommLevel *vehiclecommlevel)
{
    u8 i;
    u8 packetid;    
    
    for (i = 0; i < ECM_MAX_COUNT; i++)
    {
        for (packetid = DCX_KWP_CAN_PACKETID_START; packetid <= DCX_KWP_CAN_PACKETID_END; packetid++)
        {
            switch (vehiclecommlevel[i])
            {
            case CommLevel_KWP2000:
                obd2can_dcx_cleandynamicallydefinedidentifier(ecm_id, packetid);
                break;
            default:
                break;
            }
        }
    }
    
    return S_SUCCESS;
}

//------------------------------------------------------------------------------
// Initiate pids into packets
// Input:   VehicleCommLevel commlevel[ECM_MAX_COUNT]
// Return:  u8  status
// Engineer: Tristen Pierson
//------------------------------------------------------------------------------
u8 obd2can_dcx_datalog_initiatepackets(VehicleCommLevel *vehiclecommlevel)
{
    Obd2can_ServiceData servicedata;
    u8 *txdata;
    u32 cooldowntime;
    u8  ecm_index;
    u32 ecm_id[ECM_MAX_COUNT] = {Obd2CanEcuId_7E0,Obd2CanEcuId_7E1,0};
    u8  packetid;
    u16 i, j;
    u8  setup_pid_count = 0;
    u8  status;
    
    gDatalogmode.packetCount = (DCX_KWP_CAN_PACKETID_END-DCX_KWP_CAN_PACKETID_START);
    gDatalogmode.packetSize = DCX_KWP_CAN_MAX_DATABYTE_PER_PACKET;
    
    if (dataloginfo == NULL)
    {
        log_push_error_point(0x4986);
        return S_BADCONTENT;
    }
    
    // This should be large enough to support packets * packet size
    txdata = __malloc(0xFF);
    if (txdata == NULL)
    {
        return S_MALLOC;
    }
    
    for(ecm_index=0;ecm_index<ECM_MAX_COUNT;ecm_index++)
    {
        switch (vehiclecommlevel[ecm_index])
        {
        case CommLevel_KWP2000:
            // Make sure $10 $92
            obd2can_dcx_initiate_diagnostic_operation(FALSE, ecm_id[ecm_index], 
                                                      0, diagnosticRequestChrysler_92);
            
            // Go through all packets
            for(packetid=DCX_KWP_CAN_PACKETID_START;packetid<=DCX_KWP_CAN_PACKETID_END;packetid++)
            {
                j = 0;
                
                // Go through all packeted pids for current ecm
                for (i = 0; i < dataloginfo->datalogsignalcount; i++)
                {                 
                    if (dataloginfo->datalogsignals[i].SignalType.Generic.PacketNumber == packetid &&
                        dataloginfo->datalogsignals[i].EcmIndex == ecm_index)
                    {
                        switch (dataloginfo->datalogsignals[i].PidType)
                        {
                        case PidTypeRegular:                
                            txdata[j++] = 0x03;                     // Define By Identifier
                            txdata[j++] = 1 + dataloginfo->datalogsignals[i].SignalType.Generic.Position;
                            txdata[j++] = dataloginfo->datalogsignals[i].Size;
                            txdata[j++] = (u8)(dataloginfo->datalogsignals[i].Address >> 8);
                            txdata[j++] = (u8)(dataloginfo->datalogsignals[i].Address);
                            txdata[j++] = 1;
                            setup_pid_count++;
                            break;
                        case PidTypeDMR:        
                            txdata[j++] = 0x02;                     // Define By Memory Address
                            txdata[j++] = 1 + dataloginfo->datalogsignals[i].SignalType.Generic.Position;
                            txdata[j++] = dataloginfo->datalogsignals[i].Size;
                            txdata[j++] = (u8)(dataloginfo->datalogsignals[i].Address);         // LSB
                            txdata[j++] = (u8)(dataloginfo->datalogsignals[i].Address >> 8);
                            txdata[j++] = (u8)(dataloginfo->datalogsignals[i].Address >> 16);   // MSB
                            setup_pid_count++;
                            break;
                        }
                        if (setup_pid_count >= DCX_KWP_CAN_MAX_PID_PACKET)
                        {
                            break;
                        }
                    }
                }// for(...)
                // If any pids are set up, define data message
                if (j > 0)
                {
                    obd2can_servicedata_init(&servicedata);
                    obd2can_dcx_cleandynamicallydefinedidentifier(ecm_id[ecm_index], packetid);
                    
                    servicedata.ecm_id = ecm_id[ecm_index];
                    servicedata.commlevel = CommLevel_KWP2000;    
                    servicedata.service = DCX_DynamicallyDefineMessage;    
                    servicedata.service_controldatabuffer[0] = packetid;    
                    servicedata.service_controldatalength = 1;    
                    servicedata.flowcontrol_separationTime = 1;    
                    servicedata.txdata = txdata;
                    servicedata.response_expected = TRUE;
                    servicedata.initial_wait_extended = 3;
                    servicedata.txdatalength = j;
                    cooldowntime = obd2_getcooldowntime();
                    obd2_setcooldowntime(1500);
                    status = obd2can_txrx(&servicedata);
                    obd2_setcooldowntime(cooldowntime);
                    
                    if (status != S_SUCCESS)
                    {
                        setup_pid_count = 0;
                    }
                }
            } // for(packetid...)
            break;
        case CommLevel_Unknown:
            break;
        default:
            log_push_error_point(0x4985);
            break;
        } // switch(...)
        if (setup_pid_count == 0)
        {
            break;
        }
    } // for(ecm_index...)
    
    // Check if any pids were set up
    if(setup_pid_count == 0)
    {
        status = S_FAIL; // If no pids setup, fail
    }
    
    // Garbage collection
    __free(txdata);
    
    return status;
}

//------------------------------------------------------------------------------
// Get datalog data in rapidpacket mode
// Input:   VehicleCommLevel commlevel[ECM_MAX_COUNT]
// Return:  u8  status
// Engineer: Tristen Pierson
//------------------------------------------------------------------------------
u8 obd2can_dcx_datalog_getdata_rapidrate(VehicleCommLevel *vehiclecommlevel)
{
    u8 i;               // Index count
    u8  data[DCX_KWP_CAN_MAX_DATABYTE_PER_PACKET];
    u32 datalength;
    u32 ecm_id;
    u32 response_id;
    u8  ecm_index;    
    u8  status;
   
    for(ecm_index = 0; ecm_index < 2; ecm_index++)
    {
        // Check ecm id
        if (ecm_index == 0)
        {
            ecm_id = Obd2CanEcuId_7E0;
        }
        else if (ecm_index == 1)
        {
            ecm_id = Obd2CanEcuId_7E1;
        }
        response_id =  ECM_RESPONSE_ID(ecm_id);
        // Iterate and poll LIDs
        for (i = 0; i < dataloginfo->packetidcount[ecm_index]; i++)
        {
            //dataloginfo->datalogsignals[i].packetidlist
            if (dataloginfo->packetidlist[ecm_index][i] != 0xFF &&
                dataloginfo->packetidlist[ecm_index][i] != DATALOG_INVALID_PACKETID)
            {
                // Get packet data
                status = obd2can_dcx_read_local_identifier(ecm_id,response_id, 
                                        dataloginfo->packetidlist[ecm_index][i],
                                        data, &datalength);
                // Parse packet data
                if (status == S_SUCCESS)
                {                
                    obd2datalog_parserapidpacketdata(ecm_index, 
                                    dataloginfo->packetidlist[ecm_index][i],
                                    data);
                }
                // Check for timeout
                else
                {
                    return S_COMMLOST;
                }                
            }
        } //for(i = 0...)
    } //for (ecm_index...)
    return S_SUCCESS;
}



