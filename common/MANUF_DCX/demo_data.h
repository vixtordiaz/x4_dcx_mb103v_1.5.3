/**
  **************** (C) COPYRIGHT 2011 SCT Performance, LLC *********************
  * File Name          : demo_data.h
  * Device / System    : SCT products using the "CommonCode" platform
  * Author             : Quyen Leba
  *
  * Version            : 1 
  * Date               : 05/02/2011
  * Description        : 
  *                    : 
  *
  *
  * History            : 05/02/2011 M. Davis
  *                    :   Add SCT Copyright header,
  *
  ******************************************************************************
  */

#ifndef __DEMO_DATA_H
#define __DEMO_DATA_H

#include <common/devicedef.h>
#include <common/obd2def.h>

typedef struct
{
    u8  upload_dummy_delay;     //in ms
    u8  download_dummy_delay;   //in ms
}DemoControl;

typedef struct
{
    u32 ecminfo_crc32e;
    ecm_info ecm;
}DemoVehicle;

typedef struct
{
    u8  count;
    u16 codes[3];
}DemoDTC;

typedef struct
{
    u8  count;
    struct
    {
        u32 address;
        float lo;
        float hi;
#define DUMMYVALUETYPE_RANDOM           0
#define DUMMYVALUETYPE_SWEEP            1
        u8  dummyvaluetype;
        float dummyattrvalue;
    }items[13];
}DemoDatalog;

//------------------------------------------------------------------------------
// Demo mode data
//------------------------------------------------------------------------------
// TODOR: Change vehicle to something cooler
static const DemoVehicle demo_vehicle =
{
    .ecminfo_crc32e = 0,
    .ecm =
    {
        .ecm_count = 1,
        .codecount = {1,0,0},
        .codeids[0] = {0,0,0,0,0,0,0,0,0,0},
        .codeids[1] = {0,0,0,0,0,0,0,0,0,0},
        .codeids[2] = {0,0,0,0,0,0,0,0,0,0},
        .codes[0][0] = "68090873AH",                   //2011 3.6L JEEP WRANGLER
        .second_codes[0][0] = 0,
        .second_codes[0][1] = "NGC4A",
        .second_codes[0][2] = 0x14,
        .second_codes[0][3] = "05150475AD",
        .vehicle_serial_number[0] = 0,
        .vin = "SCT_JEEP_WRANGLER",
        .commtype = {CommType_CAN,CommType_Unknown,CommType_Unknown},
        .commlevel = {CommLevel_KWP2000,CommLevel_Unknown,CommLevel_Unknown},
    },
};

static const DemoControl demo_control =
{
    .upload_dummy_delay = 15,
    .download_dummy_delay = 15,
};

static const DemoDTC demo_dtc = 
{
    .count = 3,
    .codes = {0x0100, 0x0108, 0x0110},
};
// TODOR: Redo this when real preloaded dlx is made for DCX
static const DemoDatalog demo_datalog =
{
    .count = 5,
    .items[0] =
    {
        .address = 0x42,      //vbatt
        .lo = 11,       .hi = 14,
        .dummyvaluetype = DUMMYVALUETYPE_RANDOM,        .dummyattrvalue = 0,
    },
    .items[1] =
    {
        .address = 0x0F,      //iat F
        .lo = 110,      .hi = 120,
        .dummyvaluetype = DUMMYVALUETYPE_RANDOM,        .dummyattrvalue = 0,
    },
    .items[2] =
    {
        .address = 0x0C,      //rpm
        .lo = 2000,     .hi = 6000,
        .dummyvaluetype = DUMMYVALUETYPE_SWEEP,         .dummyattrvalue = 50,
    },
    .items[3] =
    {
        .address = 0x0E,      //spark
        .lo = 20,       .hi = 21,
        .dummyvaluetype = DUMMYVALUETYPE_RANDOM,        .dummyattrvalue = 0,
    },
    .items[4] =
    {
        .address = 0x0B,      //boost psi
        .lo = 10,       .hi = 12,
        .dummyvaluetype = DUMMYVALUETYPE_RANDOM,        .dummyattrvalue = 0,
    },
};

#endif    //__DEMO_DATA_H
