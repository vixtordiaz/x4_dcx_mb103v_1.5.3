/**
  **************** (C) COPYRIGHT 2011 SCT Performance, LLC *********************
  * File Name          : ford_ecm_utility_testfunc.h
  * Device / System    : SCT products using the "CommonCode" platform
  * Author             : Quyen Leba
  *
  * Version            : 1 
  * Date               : 05/02/2011
  * Description        : 
  *                    : 
  *
  *
  * History            : 05/02/2011 M. Davis
  *                    :   Add SCT Copyright header,
  *
  ******************************************************************************
  */

#ifndef __DCX_ECM_UTILITY_TESTFUNC_H
#define __DCX_ECM_UTILITY_TESTFUNC_H

#include <arch/gentype.h>

u8 dcx_ecm_utility_testfunc_emptytest(u16 ecm_type);
u8 dcx_ecm_utility_testfunc_gpec2(u16 ecm_type);

#endif  /* __DCX_ECM_UTILITY_TESTFUNC_H */
