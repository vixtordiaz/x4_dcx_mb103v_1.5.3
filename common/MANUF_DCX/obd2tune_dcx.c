/**
  **************** (C) COPYRIGHT 2011 SCT Performance, LLC *********************
  * File Name          : obd2tune_dcx.c
  * Device / System    : SCT products using the "CommonCode" platform
  * Author             : Quyen Leba
  *
  * Version            : 1 
  * Date               : 05/02/2011
  * Description        : 
  *                    : 
  *
  *
  * History            : 05/02/2011 M. Davis
  *                    :   Add SCT Copyright header,
  *
  ******************************************************************************
  */

//------------------------------------------------------------------------------
// Log error point: 0x4550 -> 0x456F (shared with obd2tune.c)
//------------------------------------------------------------------------------

#include <string.h>
#include <fs/genfs.h>
#include <common/log.h>
#include <common/file.h>
#include <common/filestock.h>
#include <common/statuscode.h>
#include <common/obd2can.h>
#include "obd2tune_dcx.h"


//------------------------------------------------------------------------------
// Check if the custom tune is allowed
// Input:   u16 veh_type
//          u8  *vin
//          u8  *preference_filename (contains check info)
//          selected_tunelist_info *tuneinfo
// Outputs: u8  *matchmasked (from LSB, 1=matched,0=unmatch)
//          bool *isvinmasked (TRUE: VIN is masked)
// Return:  u8  status
// Engineer: Quyen Leba
// Note: only useful for custom tune
//------------------------------------------------------------------------------
u8 obd2tune_dcx_check_customtune_support(u16 veh_type,  u8 *vin,
                                          u8 *preference_filename,
                                          ecm_info *ecminfo, u8 *matchmasked,
                                          bool *isvinmasked)
{
    customtune_tuneosdata tuneosdata;
    u8  fullfilename[128];
    bool isvinmatch;
    bool istunematch;
    bool tmpbool;
    u8  mask_cmp;
    u8  mask_reference;
    u8  i;
    u8  vinmaskcount;
    u8  status;
    u32 ecm_count;

    status = S_FAIL;
    if (preference_filename[0] == 0)
    {
        if (VEH_IsCustomTuneTuneOSNotRequired(veh_type))
        {
            status = S_NOTREQUIRED;
            goto obd2tune_dcx_check_customtune_support_done;
        }
        else
        {
            status = S_ERROR;
            log_push_error_point(0x4550);
            goto obd2tune_dcx_check_customtune_support_done;
        }
    }

    genfs_userpathcorrection(preference_filename, sizeof(fullfilename), 
                             fullfilename);
    if (strstr((char*)preference_filename,".tos"))
    {
        status = obd2tune_extracttuneosdata_tos(fullfilename,
                                                &tuneosdata);
    }
    else if (strstr((char*)preference_filename,".txt"))
    {
        status = obd2tune_extracttuneosdata_txt(fullfilename,
                                                &tuneosdata);
    }
    else
    {
        log_push_error_point(0x4551);
        return S_NOTSUPPORT;
    }
    if (status == S_NOTSUPPORT)
    {
        log_push_error_point(0x4552);
        return status;
    }
    else if (status != S_SUCCESS)
    {
        log_push_error_point(0x4553);
        return status;
    }

    //check VIN
    vinmaskcount = 0;
    *matchmasked = 0;
    *isvinmasked = FALSE;
    isvinmatch = TRUE;
    for(i=0;i<VIN_LENGTH;i++)   
    {
        if (tuneosdata.vin[i] == '*')
        {
            vinmaskcount++;
        }
        else if (vin[i] != tuneosdata.vin[i])
        {
            isvinmatch = FALSE;
            break;
        }
    }
    if(vinmaskcount == VIN_LENGTH)
    {
        // All VIN characters are masked, so don't care
    }
    else if(vinmaskcount > 0)
    {
        *isvinmasked = TRUE;
    }
    
    mask_reference = mask_cmp = 0;
    ecm_count = VEH_GetEcmCount(veh_type);
    for(i=0;i<ecm_count;i++)
    {
        mask_reference |= (u8)(1<<i);
    }
    
    for(i=0;i<ecm_count;i++)
    {
        //check ecminfo->codes[i][0] against tuneosdata.codes
        //then check ecminfo->second_codes[i][0] against tuneosdata.secondcodes
        tmpbool = FALSE;
        status = obd2tune_comparetunecode(tuneosdata.codes,
                                          ecminfo->codes[i][0],i);
        if (status == S_SUCCESS)
        {
            if (ecminfo->second_codes[i][0][0] == 0)
            {
                status = obd2tune_comparetunecode(tuneosdata.secondcodes,
                                                  "N/A",i);
            }
            //else
            //{
                //status = obd2tune_comparetunecode(tuneosdata.secondcodes,
                  //                                ecminfo->second_codes[i][0],i);
            //}
            if (status == S_SUCCESS)
            {
                tmpbool = TRUE;
            }
        }

        if (tmpbool)
        {
            mask_cmp |= (u8)(1<<i);
        }
    }

    istunematch = FALSE;
    if (mask_cmp == mask_reference)
    {
        istunematch = TRUE;
    }
    status = S_SUCCESS;
    
obd2tune_dcx_check_customtune_support_done:
    if (status == S_SUCCESS)
    {
        *matchmasked = mask_cmp;
        if (isvinmatch == FALSE)
        {
            return S_VINUNMATCH;
        }
        else if (istunematch == FALSE)
        {
            return S_TUNEUNMATCH;
        }
    }
    return status;
}

//------------------------------------------------------------------------------
// Compare Partnumbers in the settings with ones from the vehicle
// Inputs:  u8 *vehiclecodes_fromvehicle
//          u8 *secondvehiclecodes_fromvehicle
// Return:  u8  status
// Engineer: Patrick Downs
//------------------------------------------------------------------------------
u8 obd2tune_dcx_compare_vehiclecodes_with_setting(flasher_info *flasherinfo)
{
    int i;
    u8 all_match_status;
    ecm_info ecminfo;
    u8 ecm_match_status[ECM_MAX_COUNT];
    
    ecminfo = flasherinfo->ecminfo;
    
    all_match_status = S_SUCCESS;
    for(i=0;i<ecminfo.ecm_count;i++)
    {
        if(ecminfo.second_codes[i][0] != 0) // Search for matching partnumbers
        {
            if(strstr((char const*)SETTINGS_TUNE(secondvehiclecodes), (char*)ecminfo.second_codes[i][0]) == 0)
            {
                ecm_match_status[i] = S_FAIL; // No match
                all_match_status = S_FAIL;
            }
            else
                ecm_match_status[i] = S_SUCCESS; 
        }
        else if(ecminfo.codes[i][0] != 0)   // Search for matching strategies
        {
            if(strstr((char const*)SETTINGS_TUNE(vehiclecodes), (char*)ecminfo.codes[i][0]) == 0)
            {
                ecm_match_status[i] = S_FAIL; // No match
                all_match_status = S_FAIL;
            }
            else
                ecm_match_status[i] = S_SUCCESS;
        }
        else
        {
            ecm_match_status[i] = S_SUCCESS; // There's probably an error so we'll skip marking it for full reflash
        }
    }
   
    // If part numbers do not match, mark for full reflash which will force 
    // preloaded stock file download before tune download. Currently only used
    // for 6.7L PCM modified bootloader flashing.
    
    i = 0;
//    for(i=0;i<ecminfo.ecm_count;i++)
//    {
    if(ecm_match_status[i] == S_FAIL)
    {
        flasherinfo->isfullreflash_already_set = TRUE;
        flasherinfo->isfullreflash[0] = TRUE;
    }
//    }
    
    return all_match_status;
}

//------------------------------------------------------------------------------
// Compare Partnumbers in the settings with ones from the vehicle
// Inputs:  u8 *vehiclecodes_fromvehicle
//          u8 *secondvehiclecodes_fromvehicle
// Return:  u8  status
// Engineer: Patrick Downs
//------------------------------------------------------------------------------
u8 obd2tune_dcx_compare_vehiclecodes_with_tuneosdata(flasher_info *flasherinfo)
{
    int i;
    u8 all_match_status;
    ecm_info ecminfo;
    u8 ecm_match_status[ECM_MAX_COUNT];
    
    ecminfo = flasherinfo->ecminfo;
    
    all_match_status = S_SUCCESS;
    for(i=0;i<ecminfo.ecm_count;i++)
    {
        if(ecminfo.second_codes[i][0] != 0) // Search for matching partnumbers
        {
            if(strstr((char const*)flasherinfo->secondvehiclecodes, (char*)ecminfo.second_codes[i][0]) == 0)
            {
                ecm_match_status[i] = S_FAIL; // No match
                all_match_status = S_FAIL;
            }
            else
                ecm_match_status[i] = S_SUCCESS; 
        }
        else if(ecminfo.codes[i][0] != 0)   // Search for matching strategies
        {
            if(strstr((char const*)flasherinfo->vehiclecodes, (char*)ecminfo.codes[i][0]) == 0)
            {
                ecm_match_status[i] = S_FAIL; // No match
                all_match_status = S_FAIL;
            }
            else
                ecm_match_status[i] = S_SUCCESS;
        }
        else
        {
            ecm_match_status[i] = S_SUCCESS; // There's probably an error so we'll skip marking it for full reflash because we can't be sure
        }
    }
   
    // If part numbers do not match, mark for full reflash which will force 
    // preloaded stock file download before tune download. Currently only used
    // for 6.7L PCM modified bootloader flashing.
    
    i = 0;
//    for(i=0;i<ecminfo.ecm_count;i++)
//    {
    if(ecm_match_status[i] == S_FAIL)
    {
        flasherinfo->isfullreflash_already_set = TRUE;
        flasherinfo->isfullreflash[0] = TRUE;
    }
//    }
    
    return all_match_status;
}

//------------------------------------------------------------------------------
// Compare VID, PATS, and security byte data with married vehicle
// Inputs:  u16 veh_type
//
// Return:  u8  status
// Engineer: Patrick Downs
//------------------------------------------------------------------------------
u8 obd2tune_dcx_compare_overlaydata(u16 veh_type)
{
    return S_NOTREQUIRED;
}

//------------------------------------------------------------------------------
// Upload vehicle stock options
// Input:   flasherinfo *f_info
// Return:  u8  status
// Engineer: Ricardo Cigarroa
//------------------------------------------------------------------------------
u8 obd2tune_dcx_option_upload(flasher_info *f_info)
{
    //No stock options when uploading for dcx for now...

    //ignore status because always fail on a bench (no ABS module)
    return S_SUCCESS;
}

//------------------------------------------------------------------------------
// Download vehicle stock options
// Input:   flasher_info *f_info
// Return:  u8  status
// Engineer: Quyen Leba
// TODOQK: this function could get long; break it apart
//------------------------------------------------------------------------------
u8 obd2tune_dcx_option_download(flasher_info *f_info)
{
    //No stock options when downloadin for dcx for now...

    //ignore status because always fail on a bench (no ABS or BCM module)
    return S_SUCCESS;
}

//------------------------------------------------------------------------------
// This function simply checks for the processor's variant ID. 
// Inputs:  u8  percentage
//          u8  *message (if NULL -> not used)
// Return:  u8 status
// Engineer: Ricardo Cigarroa
//------------------------------------------------------------------------------
u8 obd2tune_dcx_check_variant_id(bool *variant_id)
{
    if(IsVariantId_DCX_GPEC2)
    {
        *variant_id = TRUE;   
    }
    else
    {
        *variant_id = FALSE; 
    }
    
    return S_SUCCESS; 
}
