/**
  **************** (C) COPYRIGHT 2011 SCT Performance, LLC *********************
  * File Name          : obd2datalog_dcx.h
  * Device / System    : SCT products using the "CommonCode" platform
  * Author             : Quyen Leba
  *
  * Version            : 1 
  * Date               : 05/02/2011
  * Description        : 
  *                    : 
  *
  *
  * History            : 05/02/2011 M. Davis
  *                    :   Add SCT Copyright header,
  *
  ******************************************************************************
  */

#ifndef __OBD2DATALOG_DCX_H
#define __OBD2DATALOG_DCX_H

#include <arch/gentype.h>
#include <common/obd2def.h>
#include <common/obd2datalog.h>

//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//
//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
u8 obd2datalog_dcx_prevalidate_task(u16 ecm_index, VehicleCommType commtype, 
                                    VehicleCommLevel commlevel);

u8 obd2datalog_dcx_validateByRapidPacketSetup(u32 ecm_id, DlxBlock *dlxblock,
                                VehicleCommType commtype,
                                VehicleCommLevel commlevel);
u8 obd2datalog_dcx_evaluate_fit(VehicleCommType *vehiclecommtype,
                                 VehicleCommLevel *vehiclecommlevel,
                                 u16 *valid_index_list,
                                 u16 *valid_index_count);
u8 obd2datalog_dcx_setupdatalogsession(VehicleCommType *vehiclecommtype,
                                      VehicleCommLevel *vehiclecommlevel);
u8 obd2datalog_dcx_startdatalogsession(VehicleCommType *vehiclecommtype,
                                      VehicleCommLevel *vehiclecommlevel);
u8 obd2datalog_dcx_stopdatalogsession(VehicleCommType *vehiclecommtype,
                                     VehicleCommLevel *vehiclecommlevel);
float obd2datalog_dcx_calculate_bp(DatalogSignal *datalogsignal);

#endif    //__OBD2DATALOG_DCX_H
