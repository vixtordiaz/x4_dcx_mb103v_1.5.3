/**
  **************** (C) COPYRIGHT 2011 SCT Performance, LLC *********************
  * File Name          : obd2_dcx.h
  * Device / System    : SCT products using the "CommonCode" platform
  * Author             : Quyen Leba
  *
  * Version            : 1 
  * Date               : 05/02/2011
  * Description        : 
  *                    : 
  *
  *
  * History            : 05/02/2011 M. Davis
  *                    :   Add SCT Copyright header,
  *
  ******************************************************************************
  */

#ifndef __OBD2_DCX_H
#define __OBD2_DCX_H

#include <arch/gentype.h>
#include <common/obd2def.h>
#include <common/obd2tune.h>

enum{
    None =       0x00, //- JTEC - no actual part type
    NGC1 =       0x01, //- NGC1(non-ETC)
    NGC2 =       0x02, //� NGC2(ETC), Engine-Only w/HC16.                                
    NGC3_PCM =   0x03, //� NGC3(non-ETC), Powertrain controller.                                
    NGC3_ETC =   0x04, //� NGC3(ETC), Powertrain controller.                                
    NGC3_XCORE = 0x05, //� NGC3(ETC), Engine-Only w/XCORE.                                
    NGC1A =      0x06, //� NGC1A(non-ETC), Powertrain controller with IID ignition.                                
    NGC1B =      0x07, //� NGC1B(non-ETC), Powertrain controller for 2003 MY with IGBT.                                
    NGC3L_ETC =  0x08, //� NGC3L(ETC), Engine-Only w/HC08.                                
    NGC3L =      0x09, //� NGC3L(non-ETC), Engine-Only.                                
    NGC3 =       0x0A, //� NGC3(non-ETC), Engine-Only.                                
    NGC3L_MP =   0x0B, //� NGC3L(ETC), Engine-Only w/HC08 (masked part).                                
    NGC4_PCM =   0x0C, //� NGC4 PCM                                
    NGC4_ECM =   0x0D, //� NGC4 ECM                                
    NGC4_TCM =   0x0E, //� NGC4 TCM
    JTECP =      0x0F, //- JTEC Plus, no actual part type
    SBEC3A =     0x10, //- SBECIIIA, no actual part type
    GPEC2_PCM =  0x11, //� GPEC2 PCM                                
    GPEC2_ECM =  0x12, //� GPEC2 ECM                                
    GPEC2_TCM =  0x13, //� GPEC2 TCM                                
    NGC4A_PCM =  0x14, //� NGC4A PCM                                
    NGC4A_ECM =  0x15, //� NGC4A ECM                                
    NGC4A_TCM =  0x16, //� NGC4A TCM
    GPEC2A_ECM = 0x1A, //- GPEC2A ECM
    GPEC2A_PCM = 0x1B, // - GPEC2A 2015 boot - 05150997AA
    SBEC3 =      0x58, //- SBECIII, no actual part type
};


u8 obd2_dcx_generatekey(u8 *seed, u32 algoindex, VehicleCommType commtype,
                         u8 *key);
u8 obd2_dcx_getoverlayposition_instockfile(u16 veh_type,
                                            u32 *vidposition, u32 *patsposition,
                                            u32 *securitybyteposition);
u8 obd2_dcx_wakeup(u16 veh_type, u16 ecm_index);
u8 obd2_dcx_vehicle_specific_task_before_program(flasher_info *f_info);
u8 obd2_dcx_vehicle_specific_task_after_program(flasher_info *f_info);

u8 obd2_dcx_vehicleinfototext(vehicle_info *vehicleinfo, u8 *vehicleinfo_text);

u8 obd2can_dcx_calculatekey_non_SCIB(u8 algoindex, u8 *vecSeed, u32 *vecKey);
u8 obd2can_dcx_calculatekey_SCIB(u32 *key, u8 seed1, u8 seed2, u8 algoindex);
u8 obd2sci_dcx_calculatekey_Type_SCIB(u32 *key, u8 seed1, u8 seed2); //this needs to be moved to sci.c file
void obd2sci_dcx_pull_table_vaule(short *Secret_key, short table_ptr, u8 table_number);//this needs to be moved to sci.c file

void obd2_dcx_vehiclefunction(VehicleCommType *commtype,
                              VehicleCommLevel *commlevel);

void obd2_dcx_tcm_clear_adaptives(VehicleCommType commtype, VehicleCommLevel commlevel);
void obd2_dcx_reset_memory(VehicleCommType commtype, VehicleCommLevel commlevel);
void obd2_dcx_traction_control(VehicleCommType commtype, VehicleCommLevel commlevel);
void obd2_dcx_tire_size_axle_ratio(VehicleCommType commtype, VehicleCommLevel commlevel);
void obd2_dcx_power_on_reset(VehicleCommType commtype, VehicleCommLevel commlevel);
void obd2_dcx_bootloader_info(VehicleCommType commtype, VehicleCommLevel commlevel);
void obd2_dcx_part_type_description(u8 part_type, u8 *part_type_des_buff);
u8 obd2_dcx_vehicle_type_search(ecm_info *ecm);
void obd2_dcx_ETC_relearn(VehicleCommType commtype, VehicleCommLevel commlevel);
#endif    //__OBD2_DCX_H
