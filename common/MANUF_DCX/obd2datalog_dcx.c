/**
  **************** (C) COPYRIGHT 2011 SCT Performance, LLC *********************
  * File Name          : obd2datalog_dcx.c
  * Device / System    : SCT products using the "CommonCode" platform
  * Author             : Quyen Leba
  *
  * Version            : 1 
  * Date               : 05/02/2011
  * Description        : 
  *                    : 
  *
  *
  * History            : 05/02/2011 M. Davis
  *                    :   Add SCT Copyright header,
  *
  ******************************************************************************
  */

//------------------------------------------------------------------------------
// Log error point: 0x4900 -> 0x4AFF (shared with obd2datalog.c)
// Use 0x4980 -> 0x499F
//------------------------------------------------------------------------------

#include <board/delays.h>
#include <common/statuscode.h>
#include <common/obd2.h>
#include <common/genmanuf_overload.h>
#include <common/obd2datalog.h>
#include "obd2datalog_dcx.h"

extern Datalog_Mode gDatalogmode;        //from obd2datalog.c
extern Datalog_Info *dataloginfo;       //from obd2datalog.c

/**
 *  @brief DCX Pre-PID validation task
 *
 *  @param [in]     ecm_index  ECM Index
 *  @param [in]     commtype   Vehicle comm type
 *  @param [in]     commlevel  Vehicle comm level
 *
 *  @return Status
 */
u8 obd2datalog_dcx_prevalidate_task(u16 ecm_index, VehicleCommType commtype, 
                                    VehicleCommLevel commlevel)
{    
    u32 ecm_id;
    u8 status = S_SUCCESS;
        
    /* Only handle CAN here; other comm types handled in obd2datalog.c */
    if (commtype == CommType_CAN)
    {
        switch (ecm_index)
        {
        case 0:
            ecm_id = Obd2CanEcuId_7E0;
            break;
        case 1:
            ecm_id = Obd2CanEcuId_7E1;
            break;
        default:
            status = S_FAIL;
            break;
        }
        if (status == S_SUCCESS)
        {
            switch (commlevel)
            {        
            case CommLevel_KWP2000:                      
                // DCX KWP2000/CAN requires 3E to be sent before polling PIDs
                obd2can_dcx_initiate_diagnostic_operation(FALSE, 
                                        ecm_id, 0, diagnosticRequestChrysler_92);
                 
                obd2can_dcx_testerpresent(FALSE,ecm_id,CommLevel_Unknown);
                break;        
            default:
                status = S_NOTSUPPORT;
                break;
            }
        }
    }
    else
    {
        status = S_NOTSUPPORT;
    }
    return status;    
}

//------------------------------------------------------------------------------
// Validate a datalog pid
// Inputs:  u32 ecm_id
//          u16 pid
//          VehicleCommType commtype,
//          VehicleCommLevel commlevel
// Return:  u8  status
// Engineer: Quyen Leba
//------------------------------------------------------------------------------
u8 obd2datalog_dcx_validatepid(u32 ecm_id, DlxBlock *dlxblock,
                                VehicleCommType commtype,
                                VehicleCommLevel commlevel)
{
    u8 status;

    status = S_SUCCESS;
    if (commtype == CommType_CAN)
    {
        if (!SETTINGS_IsDemoMode())
        {
            status = obd2can_validatepid(ecm_id,dlxblock,commlevel);
        }
    }
    else
    {
        log_push_error_point(0x498A);
        status = S_COMMTYPE;
    }
    return status;
}

//------------------------------------------------------------------------------
// Validate a dmr/pid with Rapid Packet Setup Mode 0x2C
// Inputs:  u32 ecm_id
//          u16 pid
//          VehicleCommType commtype,
//          VehicleCommLevel commlevel
// Return:  u8  status
// Engineer: Quyen Leba
//------------------------------------------------------------------------------
u8 obd2datalog_dcx_validateByRapidPacketSetup(u32 ecm_id, DlxBlock *dlxblock,
                                VehicleCommType commtype,
                                VehicleCommLevel commlevel)
{
  return S_SUCCESS;
}

//------------------------------------------------------------------------------
// Evaluate all items for what can datalog in one session
// Inputs:  VehicleCommType vehiclecommtype[ECM_MAX_COUNT]
//          VehicleCommLevel vehiclecommlevel[ECM_MAX_COUNT]
// Outputs: u16 *valid_index_list
//          u16 *valid_index_count
// Return:  u8  status
// Engineer: Quyen Leba
//------------------------------------------------------------------------------
u8 obd2datalog_dcx_evaluate_fit(VehicleCommType *vehiclecommtype,
                                 VehicleCommLevel *vehiclecommlevel,
                                 u16 *valid_index_list,
                                 u16 *valid_index_count)
{
    u16 i;
    u16 count;
    u8  status;

    if (dataloginfo == NULL)
    {
        log_push_error_point(0x498C);
        return S_BADCONTENT;
    }
    else if (valid_index_count == NULL || valid_index_list == NULL)
    {
        log_push_error_point(0x498D);
        return S_BADCONTENT;
    }

    *valid_index_count = 0;
    if (vehiclecommtype[0] == CommType_CAN)
    {
        status = obd2can_dcx_datalog_evaluatesignalsetup(vehiclecommlevel);
    }
    else
    {
        log_push_error_point(0x498E);
        return S_COMMTYPE;
    }
    if (status == S_SUCCESS || status == S_NOTFIT)
    {
        count = 0;
        for(i=0;i<dataloginfo->datalogsignalcount;i++)
        {
            switch (dataloginfo->datalogsignals[i].PidType)
            {
                case PidTypeRegular:
                case PidTypeDMR:
                case PidTypeMode1:
                case PidTypeAnalog:
                    valid_index_list[count++] = i;
                    break;
                default:
                    break;
            }
            if (count >= DATALOG_MAX_VALID_PER_SESSION)
            {
                break;
            }
        }
        *valid_index_count = count;
        return S_SUCCESS;
    }
    log_push_error_point(0x498F);
    return S_FAIL;
}

//------------------------------------------------------------------------------
// Prepare for datalog, setup packets (if fit), setup datalog function calls
// Inputs:  VehicleCommType vehiclecommtype[ECM_MAX_COUNT]
//          VehicleCommLevel vehiclecommlevel[ECM_MAX_COUNT]
// Return:  u8  status
// Engineer: Quyen Leba
//------------------------------------------------------------------------------
u8 obd2datalog_dcx_setupdatalogsession(VehicleCommType *vehiclecommtype,
                                        VehicleCommLevel *vehiclecommlevel)
{
    u8  status = S_SUCCESS;

    if (dataloginfo == NULL)
    {
        log_push_error_point(0x4990);
        return S_BADCONTENT;
    }
    
    // Initialize Tasks
    dataloginfo->scheduled_tasks = 0;
    
    if (vehiclecommtype[0] == CommType_CAN)
    {
        if (vehiclecommlevel[0] == CommLevel_KWP2000 &&
            gDatalogmode.packetmode == RapidRate)
        {
            status = obd2can_dcx_datalog_evaluatesignalsetup(vehiclecommlevel);            
            if (status == S_SUCCESS)
            {
                status = obd2can_dcx_datalog_initiatepackets(vehiclecommlevel);
                if (status != S_SUCCESS)
                {
                    // No packets being used or packets are unsupported
                    gDatalogmode.packetmode = SingleRate;
                    status = obd2_datalog_evaluatesignalsetup(vehiclecommlevel);
                }
                if (status != S_SUCCESS)
                {
                    log_push_error_point(0x4992);
                    return status;
                }
            }
        }
        else
        {
            // Allow single rate SAE PID datalogging on other OEM's
            gDatalogmode.packetmode = SingleRate;
            status = obd2_datalog_evaluatesignalsetup(vehiclecommlevel);
        }
        if (status != S_SUCCESS)
        {
            log_push_error_point(0x4991);            
            return status;
        }
        
        if (SETTINGS_IsDemoMode())
        {
            dataloginfo->getdatafunc = obd2datalog_demo_getdata;
            dataloginfo->getanalogdatafunc = obd2datalog_getdata_analog;
            dataloginfo->sendtesterpresent = obd2datalog_demo_sendtesterpresent;
            dataloginfo->scheduled_tasks |= DATALOG_TASK_GET_DATA
                                         |  DATALOG_TASK_ANALOG;
            status = S_SUCCESS;
        }
        else
        {
            switch(gDatalogmode.packetmode)
            {
                case RapidRate:                    
                    dataloginfo->getdatafunc = obd2can_dcx_datalog_getdata_rapidrate;
                    dataloginfo->scheduled_tasks |= DATALOG_TASK_GET_DATA
                                                 |  DATALOG_TASK_GET_DATA_SINGLE_RATE
                                                 |  DATALOG_TASK_ANALOG;
                    break;
                case SingleRate:
                    dataloginfo->getdatafunc = obd2datalog_getdata_singlerate;
                    dataloginfo->scheduled_tasks |= DATALOG_TASK_GET_DATA_SINGLE_RATE
                                                 |  DATALOG_TASK_ANALOG;
                    break;
                default:
                    status = S_FAIL;
                    break;
            }            
            dataloginfo->getanalogdatafunc = obd2datalog_getdata_analog;  
        }
    }
    else if (vehiclecommtype[1] == CommType_CAN ||
             vehiclecommtype[1] == CommType_KLINE ||
             vehiclecommtype[1] == CommType_KWP2000 ||
             vehiclecommtype[1] == CommType_VPW)
    {
        status = obd2_datalog_evaluatesignalsetup(vehiclecommlevel);
        if (status == S_SUCCESS || status == S_NOTFIT)
        {
            gDatalogmode.packetmode = SingleRate;
            dataloginfo->getanalogdatafunc = obd2datalog_getdata_analog;
            dataloginfo->getdatafunc = obd2datalog_getdata_singlerate;
            dataloginfo->scheduled_tasks |= DATALOG_TASK_GET_DATA_SINGLE_RATE
                                         |  DATALOG_TASK_ANALOG;
        }
    }
    else
    {
        log_push_error_point(0x4995);
        status = S_COMMTYPE;
    }
    return status;
}

//------------------------------------------------------------------------------
// Start datalog session
// Inputs:  VehicleCommType vehiclecommtype[ECM_MAX_COUNT]
//          VehicleCommLevel vehiclecommlevel[ECM_MAX_COUNT]
// Return:  u8  status
// Engineer: Quyen Leba
//------------------------------------------------------------------------------
u8 obd2datalog_dcx_startdatalogsession(VehicleCommType *vehiclecommtype,
                                        VehicleCommLevel *vehiclecommlevel)
{
    u8  i;
    u8  status;

    if (dataloginfo == NULL)
    {
        log_push_error_point(0x4996);
        return S_BADCONTENT;
    }

    status = S_SUCCESS;    
    if (vehiclecommtype[0] == CommType_CAN)
    {
        if(gDatalogmode.packetmode == RapidRate)
        {
            const u32 ecm_id[ECM_MAX_COUNT] = {Obd2CanEcuId_7E0,Obd2CanEcuId_7E1,0};

            for(i=0;i<ECM_MAX_COUNT;i++)
            {
                if(dataloginfo->packetidcount[i] == 0)
                    continue;
               
                // Do not start TCM packets if in broadcast mode and datalogging
                // SAE PIDs
                if (gDatalogmode.ecmBroadcast == TRUE && 
                    gDatalogmode.obdPids == TRUE &&
                    ecm_id[i] != Obd2CanEcuId_7E0)
                {
                    continue;
                }
                if (vehiclecommlevel[i] == CommLevel_KWP2000)
                {
                    status = obd2can_dcx_initiate_diagnostic_operation(FALSE, 
                                    ecm_id[i], 0, diagnosticRequestChrysler_92);
                }
                else if (vehiclecommlevel[i] == CommLevel_Unknown)
                {
                    break;
                }
                else
                {                
                    log_push_error_point(0x4997);
                    status = S_COMMLEVEL;
                    break;
                }                
            } //for(...)
        }
        else // Non-rapid packet
        {
           status = S_SUCCESS;
        }
    }
    else if (vehiclecommtype[1] == CommType_CAN ||
             vehiclecommtype[1] == CommType_KLINE ||
             vehiclecommtype[1] == CommType_KWP2000 ||
             vehiclecommtype[1] == CommType_VPW)
    {
        status = S_SUCCESS;
    }
    else
    {
        log_push_error_point(0x4998);
        status = S_COMMTYPE;
    }
    
    return status;
}

//------------------------------------------------------------------------------
// Stop datalog session
// Inputs:  VehicleCommType vehiclecommtype[ECM_MAX_COUNT]
//          VehicleCommLevel vehiclecommlevel[ECM_MAX_COUNT]
// Return:  u8  status
// Engineer: Quyen Leba
//------------------------------------------------------------------------------
u8 obd2datalog_dcx_stopdatalogsession(VehicleCommType *vehiclecommtype,
                                       VehicleCommLevel *vehiclecommlevel)
{
    u8 i;
    u8 status;

    if (vehiclecommtype[0] == CommType_CAN)
    {
        for(i=0;i<ECM_MAX_COUNT;i++)
        {
            if(vehiclecommtype[i] == CommType_CAN)
            {
                if (vehiclecommlevel[i] == CommLevel_KWP2000)
                {
                    // Nothing needs to be done
                }                
                else
                {
                    log_push_error_point(0x4999);
                    status = S_COMMLEVEL;
                    break;
                }
            }//if(vehiclecommtype[i] == CommType_CAN)...
        }//for(i=0;i<ECM_MAX_COUNT;i++)...
    }
    else if (vehiclecommtype[1] == CommType_CAN ||
             vehiclecommtype[1] == CommType_KLINE ||
             vehiclecommtype[1] == CommType_KWP2000 ||
             vehiclecommtype[1] == CommType_VPW)
    {
        status = S_SUCCESS;
    }
    else
    {
        log_push_error_point(0x499A);
        status = S_COMMTYPE;
    }
    
    status = S_SUCCESS;
    
    return status;
}
