/**
  **************** (C) COPYRIGHT 2011 SCT Performance, LLC *********************
  * File Name          : dcx_ee2_defs.h
  * Device / System    : SCT products using the "CommonCode" platform
  * Author             : Ricardo Cigarroa
  *
  * Version            : 1 
  * Date               : 06/05/2014
  * Description        : 
  *                    : 
  *
  *
  * History            : 05/02/2011 M. Davis
  *                    :   Add SCT Copyright header,
  *
  ******************************************************************************
  */
  

#ifndef __E2_DEFS_H
#define __E2_DEFS_H

#include <arch/gentype.h>

#define MAX_E2_BLOCKS             2
#define INVALID_E2_DEF            0xFF

#define E2_FLAG_NONE              0
#define E2_FLAG_MODIFY_DATA       1

typedef struct
{
    u8  identifier;
    u32 block_size;
    u32 flags;
}e2_block;

//------------------------------------------------------------------------------
// E2_Def:
// flags                  E2 specific flags.
// modified_data_bytes    These bytes replace the stock value bytes in the E2.
// E2_info                E2 struct containing identifier and block data. 
//------------------------------------------------------------------------------
typedef struct
{
    u16 flags;
    u8  modified_data_bytes[2];
    e2_block e2_data[MAX_E2_BLOCKS];
}E2_Def;


#define E2_DEFS_COUNT                                     2
#define ECM_DEF_COUNT                                     16
extern const E2_Def dcx_e2_defs[E2_DEFS_COUNT];
extern const u8 E2_EcmIndex[ECM_DEF_COUNT];


//---------------------------------------------------------------------------------
// ev: e2_def
//---------------------------------------------------------------------------------
#define e2ecmindex(ei)                                    E2_EcmIndex[ei]
#define e2var(ev,var)                                     dcx_e2_defs[ev].##var

#define E2_GetEcmIndex(ei)                    (e2ecmindex(ei))
#define E2_GetE2DataFlag(ev)                  ((e2var(ev,flags) & E2_FLAG_MODIFY_DATA) == E2_FLAG_MODIFY_DATA)
#define E2_GetModifyDataBytes(ev,t)           ((e2var(ev,modified_data_bytes[t])))
#define E2_GetE2Identifier(ev,t)              (e2var(ev,e2_data)[t].identifier)
#define E2_GetE2BlockSize(ev,t)               (e2var(ev,e2_data)[t].block_size)

#endif  //__E2_DEFS_H