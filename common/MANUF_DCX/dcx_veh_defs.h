/**
  **************** (C) COPYRIGHT 2011 SCT Performance, LLC *********************
  * File Name          : dcx_veh_defs.h
  * Device / System    : SCT products using the "CommonCode" platform
  * Author             : Ricardo Cigarroa
  *
  * Version            : 1 
  * Date               : 05/02/2011
  * Description        : 
  *                    : 
  *
  *
  * History            : 05/02/2011 M. Davis
  *                    :   Add SCT Copyright header,
  *
  ******************************************************************************
  */

#ifndef __DCX_VEH_DEFS_H
#define __DCX_VEH_DEFS_H

#include <arch/gentype.h>
#include <common/obd2def.h>

#define DCX_VEH_DEF_START                              3000
#define DCX_VEH_DEF_END                                4000

#define VEH_DEF_FLAG_NONE                               (0)
#define UNLOCK_ALL_ECMS_FIRST                           (1 << 0)
#define SKIP_RECYCLE_KEY_ON                             (1 << 1)
#define REQUIRE_WAKEUP                                  (1 << 2)
#define WAKEUP_CHECK_ONCE                               (1 << 3)
#define SIMPLE_SETMARRIED                               (1 << 4)
#define REQUIRE_OVERLAY                                 (1 << 5)
#define CHECK_KEYON_PRIOR_WAKEUP                        (1 << 6)    //depends on REQUIRE_WAKEUP
#define ALLOW_BLANK_VIN                                 (1 << 7)    //should only used in development
#define CUSTOM_TUNE_TUNEOS_FILE_NOT_REQUIRED            (1 << 8)
#define PRE_DOWNLOAD_CHECK                              (1 << 12)     //check for original/mod bootloader hash

// Reads Vehicle Info After Download to Verify Success
#define CHECK_SUCCESS_AFTER_DOWNLOAD                    (1 << 9)

// When TUNEOS codes do not match whats read from vehicle, Full Flash is required,
// suppress Full Flash message.
#define SUPPRESS_FULL_FLASH_MESSAGE                     (1 << 10)

// TODO: We only need this until segment flashing
#define APPROVE_SKIP_FLASH_TCM                          (1 << 11)

//in case TCM is unsupported, set this flag will make unsupported error
//i.e.: without this flag, user can choose to skip program TCM
#define SUB_ECM_REQUIRED                                (1 << 29)
//in case TCM is unsupported and allow PCM only
#define ALLOW_SKIP_UNSUPPORTED_SUB_ECM                  (1 << 30)

enum{
    None =       0x00, //- JTEC - no actual part type
    NGC1 =       0x01, //- NGC1(non-ETC)
    NGC2 =       0x02, //� NGC2(ETC), Engine-Only w/HC16.                                
    NGC3_PCM =   0x03, //� NGC3(non-ETC), Powertrain controller.                                
    NGC3_ETC =   0x04, //� NGC3(ETC), Powertrain controller.                                
    NGC3_XCORE = 0x05, //� NGC3(ETC), Engine-Only w/XCORE.                                
    NGC1A =      0x06, //� NGC1A(non-ETC), Powertrain controller with IID ignition.                                
    NGC1B =      0x07, //� NGC1B(non-ETC), Powertrain controller for 2003 MY with IGBT.                                
    NGC3L_ETC =  0x08, //� NGC3L(ETC), Engine-Only w/HC08.                                
    NGC3L =      0x09, //� NGC3L(non-ETC), Engine-Only.                                
    NGC3 =       0x0A, //� NGC3(non-ETC), Engine-Only.                                
    NGC3L_MP =   0x0B, //� NGC3L(ETC), Engine-Only w/HC08 (masked part).                                
    NGC4_PCM =   0x0C, //� NGC4 PCM                                
    NGC4_ECM =   0x0D, //� NGC4 ECM                                
    NGC4_TCM =   0x0E, //� NGC4 TCM
    JTECP =      0x0F, //- JTEC Plus, no actual part type
    SBEC3A =     0x10, //- SBECIIIA, no actual part type
    GPEC2_PCM =  0x11, //� GPEC2 PCM                                
    GPEC2_ECM =  0x12, //� GPEC2 ECM                                
    GPEC2_TCM =  0x13, //� GPEC2 TCM                                
    NGC4A_PCM =  0x14, //� NGC4A PCM                                
    NGC4A_ECM =  0x15, //� NGC4A ECM                                
    NGC4A_TCM =  0x16, //� NGC4A TCM                                
    SBEC3 =      0x58, //- SBECIII, no actual part type
};

#define VEH_DEFS_COUNT                                  13
extern const VEH_Def dcx_veh_defs[VEH_DEFS_COUNT];

u8 dcx_veh_get_ecmcount(u16 veh_type);
u8 dcx_veh_validate_vehicletype(u16 veh_type);

//------------------------------------------------------------------------------
// et:  ecm_type
//------------------------------------------------------------------------------
#define vehvar(vt,var)                          dcx_veh_defs[vt-DCX_VEH_DEF_START].##var
#define vehdefmin                               (DCX_VEH_DEF_START)
#define vehdefmax                               (DCX_VEH_DEF_END)
#define vehecmcount(vt)                         dcx_veh_get_ecmcount(vt)

//------------------------------------------------------------------------------
// use locally for dcx (chrysler)
// vt:  veh_type
//------------------------------------------------------------------------------
#define VEH_DCX_IsVehicleType60L(vt)           ((vt==12) || (vt==13))

#endif    //__DCX_VEH_DEFS_H
