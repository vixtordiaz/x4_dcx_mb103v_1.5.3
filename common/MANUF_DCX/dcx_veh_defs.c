/**
  **************** (C) COPYRIGHT 2011 SCT Performance, LLC *********************
  * File Name          : dcx_veh_defs.c
  * Device / System    : SCT products using the "CommonCode" platform
  * Author             : Ricardo Cigarroa
  *
  * Version            : 1 
  * Date               : 05/02/2011
  * Description        : 
  *                    : 
  *
  *
  * History            : 05/02/2011 M. Davis
  *                    :   Add SCT Copyright header,
  *
  ******************************************************************************
  */

#include <common/obd2.h>
#include <common/ecm_defs.h>
#include <common/statuscode.h>
#include <common/obd2tune.h>
#include <common/cmdif_func_flasher.h>
#include <common/settings.h>
#include "dcx_veh_defs.h"


extern flasher_info *flasherinfo;                   //from obd2tune.c
extern Settings_Tune settingstune;                  //from settings.c

//------------------------------------------------------------------------------
// Get # of ecms defined by vehicle type
// Input:   u16 veh_type (defined for veh_defs)
// Return:  u8  count
//------------------------------------------------------------------------------
u8 dcx_veh_get_ecmcount(u16 veh_type)
{
    u8 i;
    u8 count;

    //TODOQ: check veh_type first
    
    for(i=0,count=0;i<ECM_MAX_COUNT;i++)
    {
        if (VEH_GetEcmType(veh_type,i) != INVALID_ECM_DEF)
        {
            count++;
        }
    }
    return count;
}

//------------------------------------------------------------------------------
// Check that the vehicle type exists
// Input:   u16 veh_type (defined for veh_defs)
// Return:  u8 status
//------------------------------------------------------------------------------
u8 dcx_veh_validate_vehicletype(u16 veh_type)
{
    u8 i;
    u16 index;

    for(i=0;i<VEH_DEFS_COUNT;i++)
    {
        index = VEH_GetIndex(veh_type);
        if ((index+DCX_VEH_DEF_START) == veh_type)
        {
            return S_SUCCESS;
        }
    }
    return S_FAIL;
}

//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
const VEH_Def dcx_veh_defs[VEH_DEFS_COUNT] =
{
    //[0]: UNUSED
    [1] =   //NGC1 (SCI_B)                                              //0x01 -> 3001 (0x0BB9)
    {
        .index = 1,
        .flags = REQUIRE_WAKEUP | CHECK_KEYON_PRIOR_WAKEUP | SIMPLE_SETMARRIED |
            CUSTOM_TUNE_TUNEOS_FILE_NOT_REQUIRED,
        .mainecm_index = 0,
        .ecm_types = {1, INVALID_ECM_DEF, INVALID_ECM_DEF},
        .templOSfilename = NULL,
    },
    [2] =   //NGC2 (SCI_B)                                              //0x02 -> 3002 (0x0BBA)
    {
        .index = 2,
        .flags = REQUIRE_WAKEUP | CHECK_KEYON_PRIOR_WAKEUP | SIMPLE_SETMARRIED |
            CUSTOM_TUNE_TUNEOS_FILE_NOT_REQUIRED,
        .mainecm_index = 0,
        .ecm_types = {2, INVALID_ECM_DEF, INVALID_ECM_DEF},
        .templOSfilename = NULL,
    },
    [3] =   //NGC3 (SCI_B)                                              //0x03 -> 3003 (0x0BBB)
    {
        .index = 3,
        .flags = REQUIRE_WAKEUP | CHECK_KEYON_PRIOR_WAKEUP | SIMPLE_SETMARRIED |
            CUSTOM_TUNE_TUNEOS_FILE_NOT_REQUIRED,
        .mainecm_index = 0,
        .ecm_types = {3, INVALID_ECM_DEF, INVALID_ECM_DEF},
        .templOSfilename = NULL,
    },
    [4] =   //2010-2013 GPEC2 ECM (CAN)                                 //0x04 -> 3004 (0X0BBC)
    {
        .index = 4,
        .flags = REQUIRE_WAKEUP | CHECK_KEYON_PRIOR_WAKEUP | SIMPLE_SETMARRIED |
         ALLOW_SKIP_UNSUPPORTED_SUB_ECM | PRE_DOWNLOAD_CHECK | SKIP_RECYCLE_KEY_ON,
        .mainecm_index = 0,
        .ecm_types = {4, INVALID_ECM_DEF, INVALID_ECM_DEF},
        .templOSfilename = NULL,
    },
    [5] =   //NGC4A 2010 and Earlier (CAN)                              //0x05 -> 3005 (0x0BBD)
    {
        .index = 5,
        .flags = REQUIRE_WAKEUP | CHECK_KEYON_PRIOR_WAKEUP | SIMPLE_SETMARRIED |
         ALLOW_SKIP_UNSUPPORTED_SUB_ECM,
        .mainecm_index = 0,
        .ecm_types = {5, INVALID_ECM_DEF, INVALID_ECM_DEF},
        .templOSfilename = NULL,
    },
    [6] =   //JTEC (SCI_A)                                              //0x06 -> 3006 (0x0BBE)
    {
        .index = 6,
        .flags = REQUIRE_WAKEUP | CHECK_KEYON_PRIOR_WAKEUP | SIMPLE_SETMARRIED |
            CUSTOM_TUNE_TUNEOS_FILE_NOT_REQUIRED,
        .mainecm_index = 0,
        .ecm_types = {6, INVALID_ECM_DEF, INVALID_ECM_DEF},
        .templOSfilename = NULL,
    },
    [7] =   //JTEC+ (SCI_A)                                             //0x07 -> 3007 (0x0BBF)
    {
        .index = 7,
        .flags = REQUIRE_WAKEUP | CHECK_KEYON_PRIOR_WAKEUP | SIMPLE_SETMARRIED |
            CUSTOM_TUNE_TUNEOS_FILE_NOT_REQUIRED,
        .mainecm_index = 0,
        .ecm_types = {7, INVALID_ECM_DEF, INVALID_ECM_DEF},
        .templOSfilename = NULL,
    },
    [8] =   //NGC4 (CAN)                                                //0x08 -> 3008 (0x0BC0)
    {
        .index = 8,
        .flags = REQUIRE_WAKEUP | CHECK_KEYON_PRIOR_WAKEUP | SIMPLE_SETMARRIED |
         ALLOW_SKIP_UNSUPPORTED_SUB_ECM,
        .mainecm_index = 0,
        .ecm_types = {8, INVALID_ECM_DEF, INVALID_ECM_DEF},
        .templOSfilename = NULL,
    },
    [9] =   //SBEC3A (SCI_B)                                           //0x09 -> 3009 (0x0BC1)
    {
        .index = 9,
        .flags = REQUIRE_WAKEUP | CHECK_KEYON_PRIOR_WAKEUP | SIMPLE_SETMARRIED |
            CUSTOM_TUNE_TUNEOS_FILE_NOT_REQUIRED,
        .mainecm_index = 0,
        .ecm_types = {9, INVALID_ECM_DEF, INVALID_ECM_DEF},
        .templOSfilename = NULL,
    },
    [10] =   //SBEC3 (SCI_A)                                             //0x0A-> 3010 (0x0BC2)
    {
        .index = 10,
        .flags = REQUIRE_WAKEUP | CHECK_KEYON_PRIOR_WAKEUP | SIMPLE_SETMARRIED |
            CUSTOM_TUNE_TUNEOS_FILE_NOT_REQUIRED,
        .mainecm_index = 0,
        .ecm_types = {10, INVALID_ECM_DEF, INVALID_ECM_DEF},
        .templOSfilename = NULL,
    },
    [11] =   //NGC4A 2011 ECM (CAN)                                     //0x0B -> 3011 (0x0BC3)
    {
        .index = 11,
        .flags = REQUIRE_WAKEUP | CHECK_KEYON_PRIOR_WAKEUP | SIMPLE_SETMARRIED |
         ALLOW_SKIP_UNSUPPORTED_SUB_ECM,
        .mainecm_index = 0,
        .ecm_types = {11, INVALID_ECM_DEF, INVALID_ECM_DEF},
        .templOSfilename = NULL,
    },
    [12] =   //2014 GPEC2 ECM (CAN)                                    //0x0C -> 3012 (0X0BC4)
    {
        .index = 12,
        .flags = REQUIRE_WAKEUP | CHECK_KEYON_PRIOR_WAKEUP | SIMPLE_SETMARRIED |
         ALLOW_SKIP_UNSUPPORTED_SUB_ECM | PRE_DOWNLOAD_CHECK | SKIP_RECYCLE_KEY_ON,
        .mainecm_index = 0,
        .ecm_types = {12, INVALID_ECM_DEF, INVALID_ECM_DEF},
        .templOSfilename = NULL,
    },

};
