/**
  **************** (C) COPYRIGHT 2011 SCT Performance, LLC *********************
  * File Name          : dcx_ecm_defs.h
  * Device / System    : SCT products using the "CommonCode" platform
  * Author             : Quyen Leba
  *
  * Version            : 1 
  * Date               : 05/02/2011
  * Description        : 
  *                    : 
  *
  *
  * History            : 05/02/2011 M. Davis
  *                    :   Add SCT Copyright header,
  *
  ******************************************************************************
  */

#ifndef __DCX_ECM_DEFS_H
#define __DCX_ECM_DEFS_H

#include <arch/gentype.h>
#include <common/obd2.h>
#include <common/ecm_defs.h>

#define MAX_UPLOAD_BUFFER_LENGTH        0x1000
#define MAX_DOWNLOAD_BUFFER_LENGTH      0x1000

#define UPLOAD_CMD_MEMORY_SIZE          MemorySize_2_Byte
#define DEFAULT_CMD_MEMORY_SIZE         MemorySize_1_Byte


enum
{
    UnlockId_None                                       = 0,
    UnlockId_HEMI_BOOTLOADER_SECURITY_UPLOAD            = 1,
    UnlockId_HEMI_BOOTLOADER_SECURITY_DOWNL             = 2,
    UnlockId_HEMI_APP_SECURITY_ACCESS_03                = 3,
    UnlockId_HEMI_APP_SECURITY_ACCESS_65                = 4,
    UnlockId_SCIB_UNLOCK                                = 5,
    UnlockId_JTECP_UNLOCK                               = 6,   
};

enum
{
    ChecksumId_None             = 0x0000,
    ChecksumId_NGC4             = 0x0001,
    ChecksumId_NGC4A            = 0x0002,
    ChecksumId_GPEC2            = 0x0003,
    //TODO: all other SCI processors
};

enum
{
    FinalId_None                  = 0x0000,
    FinalId_SendControlRoutine31  = 0x0001,
};

#define ECM_DEF_COUNT            13
extern const ECM_Def             dcx_ecm_defs[];
extern const DCX_DL_HACK_DATA    hack_data[];

//------------------------------------------------------------------------------
// et:  ecm_type
//------------------------------------------------------------------------------
#define ecmvar(et,var)                      dcx_ecm_defs[et].##var

#endif    //__DCX_ECM_DEFS_H
