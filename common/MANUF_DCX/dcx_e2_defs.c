/**
  **************** (C) COPYRIGHT 2011 SCT Performance, LLC *********************
  * File Name          : dcx_e2_defs.c
  * Device / System    : SCT products using the "CommonCode" platform
  * Author             : Ricardo Cigarroa
  *
  * Version            : 1 
  * Date               : 06/05/2014
  * Description        : 
  *                    : 
  *
  *
  * History            : 05/02/2011 M. Davis
  *                    :   Add SCT Copyright header,
  *
  ******************************************************************************
  */

#include <common/obd2.h>
#include <common/ecm_defs.h>
#include <common/statuscode.h>
#include <common/obd2tune.h>
#include <common/cmdif_func_flasher.h>
#include "dcx_e2_defs.h"



const u8 E2_EcmIndex[ECM_DEF_COUNT] = 
{
   [0]  = INVALID_E2_DEF,
   [1]  = INVALID_E2_DEF,
   [2]  = INVALID_E2_DEF,
   [3]  = INVALID_E2_DEF,
   [4]  = 1,
   [5]  = INVALID_E2_DEF,
   [6]  = INVALID_E2_DEF,
   [7]  = INVALID_E2_DEF,
   [8]  = INVALID_E2_DEF,
   [9]  = INVALID_E2_DEF,
   [10] = INVALID_E2_DEF,
   [11] = 0,
   [12] = 1,
   [13] = 0,
   [14] = 1,
   [15] = 1,
};


//------------------------------------------------------------------------------
// E2 DEFS
//------------------------------------------------------------------------------
const E2_Def dcx_e2_defs[] =
{ 
    [0] = 
    {
        E2_FLAG_NONE, {0,0},
        .e2_data = 
        {
            {0xA9,  0x0802,  ORDER_1 | MEMBLOCK_FLAGS_NONE},
            {0xAA,  0x0802,  ORDER_2 | MEMBLOCK_FLAGS_NONE},
        },      
    },
    [1] = 
    {
        E2_FLAG_MODIFY_DATA, {0x9A,0x02},
        .e2_data = 
        {
            {0xA9,  0x0802,  ORDER_1 | MEMBLOCK_FLAGS_NONE},
            {0xAA,  0x0802,  ORDER_2 | MEMBLOCK_FLAGS_NONE},
        },
    },  
};