/**
  **************** (C) COPYRIGHT 2011 SCT Performance, LLC *********************
  * File Name          : ford_ecm_utility_testfunc.c
  * Device / System    : SCT products using the "CommonCode" platform
  * Author             : Quyen Leba
  *
  * Version            : 1 
  * Date               : 05/02/2011
  * Description        : 
  *                    : 
  *
  *
  * History            : 05/02/2011 M. Davis
  *                    :   Add SCT Copyright header,
  *
  ******************************************************************************
  */

#include <common/statuscode.h>
#include "dcx_ecm_utility_testfunc.h"
#include "obd2can_dcx.h"

//------------------------------------------------------------------------------
// This is example function to test if its utility file is applicable
// Input:   u16 ecm_type (defined in ecm_defs)
// Return:  u8  status (S_SUCCESS if applicable)
// Engineer: Quyen Leba
//------------------------------------------------------------------------------
u8 dcx_ecm_utility_testfunc_emptytest(u16 ecm_type)
{
    return S_SUCCESS;
}


//------------------------------------------------------------------------------
// Test for GPEC2 processors. Applies to years 2010-2013. 
// Input:   u16 ecm_type (defined in ecm_defs)
// Return:  u8  status (S_SUCCESS if applicable)
// Engineer: Ricardo Cigarroa
//------------------------------------------------------------------------------
u8 dcx_ecm_utility_testfunc_gpec2(u16 ecm_type)
{
    u8 status; 
        
    if(IsVariantId_DCX_GPEC2)
    {
        status = S_SUCCESS;//2013 and up (use utility)
    }
    else
    {
        status = S_NOTREQUIRED; 
    }
    
    return status; 
}

