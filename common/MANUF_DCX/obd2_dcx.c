/**
  **************** (C) COPYRIGHT 2011 SCT Performance, LLC *********************
  * File Name          : obd2_dcx.c
  * Device / System    : SCT products using the "CommonCode" platform
  * Author             : Quyen Leba
  *
  * Version            : 1
  * Date               : 05/02/2011
  * Description        :
  *                    :
  *
  *
  * History            : 05/02/2011 M. Davis
  *                    :   Add SCT Copyright header,
  *
  ******************************************************************************
  */

#include <board/power.h>
#include <board/delays.h>
#include <common/itoa.h>
#include <common/statuscode.h>
#include <common/genmanuf_overload.h>
#include <common/obd2iso.h>
#include <common/obd2tune.h>
#include <common/crypto_blowfish.h>
#include <common/filestock.h>
#include <common/settings.h>
#include <common/cmdif.h>
#include <common/cmdif_remotegui.h>
#include <common/ess_file.h>
#include <common/checksum_csf.h>
#include <common/cmdif_remotegui.h>
#include "obd2_dcx.h"

extern cmdif_datainfo cmdif_datainfo_responsedata;  //from cmdif.c
extern flasher_info *flasherinfo;                   //from obd2tune.c
extern u8 obd2_rxbuffer[256];                       //from obd2.c

//The table below and the function "Type_SCIB()" must be moved to a seperate file for sci commands... "sci.c"
const u8 obd2sci_dcx_chrysler_table1[] = {
                                0xB5,0x07,0x00,0xBC,0x27,0x05,0x00,0x00,0x2C,0xB6,0x01,0x02,0x22,
                                0x20,0x00,0x00,0x42,0x84,0x22,0x20,0x01,0x00,0x43,0x86,0x22,0x20,
                                0x02,0x00,0x44,0x88,0x9D,0x9F,0xCE,0x48,0xB0,0xF3,0xD9,0x9B,0xA7,
                                0x20,0xFD,0xD6,0x83,0x6D,0x6F,0x8E,0xA8,0x1F,0x57,0xC9,0x41,0x40,
                                0x1A,0xC7,0x7A,0xAD,0x9C,0x36,0x02,0xE2,0x77,0x16,0xE2,0x1C,0xED,
                                0x96,0x96,0xF0,0x93,0xBD,0xF8,0xCE,0x29,0x34,0xD9,0x68,0xD5,0x07,
                                0x8A,0x4F,0x52,0x45,0x93,0x08,0xD9,0x97,0xF4,0xF5,0xE3,0x24,0xC7,
                                0x6F,0x55,0x35,0x57,0xDB,0xC1,0x04,0x38,0xC9,0xA7,0x10,0xDE,0x84,
                                0x22,0xCB,0x80,0x30,0x41,0x42,0x00,0x00,0x00,0x80,0x00,0x00,0x00,
                                0x04,0x00,0x00,0x00,0x64,0x00,0x00,0x01,0xF5
                            };

//------------------------------------------------------------------------------
// Calculate keys algorithm for SCI_B processors
// Input:   u8 *key
//          u8  seed1
//          u8  seed2
// Return:  u8  status
// Engineer: Ricardo Cigarroa
//------------------------------------------------------------------------------
void obd2sci_dcx_pull_table_vaule(short *Secret_key, short table_ptr, u8 table_number)
{
    u8 temp1 = 0;
    u8 temp2 = 0;

    if(table_number == 1)
    {
       temp1  = obd2sci_dcx_chrysler_table1[table_ptr];
       temp2  = obd2sci_dcx_chrysler_table1[table_ptr+1];
       *Secret_key = temp1<<8;
       *Secret_key |= temp2;
    }
    else
    {
       *Secret_key = 0;
    }
}

//------------------------------------------------------------------------------
// Calculate keys algorithm for SCI_B processors
// Input:   u8 *key
//          u8  seed1
//          u8  seed2
// Return:  u8  status
// Engineer: Ricardo Cigarroa
//------------------------------------------------------------------------------
u8 obd2sci_dcx_calculatekey_Type_SCIB(u32 *key, u8 seed1, u8 seed2)
{
    short hold_seed = 0;
    short hold_seed2 = 0;
    short seed_A = 0;
    short seed_B = 0;
    short seed_C = 0;
    u32   shifter  = 0;
    short Secret_key = 0;
    u8  i = 0;
    u8  offset = 0x4E;

    *key = 0x00000000; //clear key since it has data from last security access command.

    shifter = seed1<<8;
    shifter &= 0x00FF00;
    hold_seed = shifter;    // Had to use this for the 1st shift or it would put a 1 in MSB
    hold_seed |= seed2;
    shifter |= seed2;

    seed_A = shifter >> 1;

    if((hold_seed & 0x0001) == 1)
        seed_A |= 0x8000;

    seed_B = seed_A;
    hold_seed2 = seed_B;

    for(i=0; i<10; i++)
        seed_A = seed_A >> 1;

    seed_A &= 0x0007;

    seed_A = seed_A << 1;
    seed_A += offset;
    obd2sci_dcx_pull_table_vaule(&Secret_key, seed_A, 1);
    seed_A = Secret_key;

    for(i=0; i<7; i++)
        seed_B = seed_B >> 1;

    seed_B &= 0x0007;
    seed_B = seed_B << 1;

    seed_B += offset;
    obd2sci_dcx_pull_table_vaule(&Secret_key, seed_B, 1);
    seed_B = Secret_key;

    seed_B = seed_A ^ seed_B;

    seed_C = hold_seed2;

    for(i=0; i<3; i++)
        seed_C = seed_C >> 1;

    seed_C &= 0x0007;
    seed_C = seed_C << 1;

    seed_C += offset;
    obd2sci_dcx_pull_table_vaule(&Secret_key, seed_C, 1);
    seed_C = Secret_key;

    seed_C = seed_C ^ seed_B;

    seed_A = hold_seed2;    // New A seed

    for(i=0; i<13; i++)
        seed_A = seed_A >> 1;
    seed_A &= 0x0007;

    seed_A = seed_A << 1;
    seed_A += offset;
    obd2sci_dcx_pull_table_vaule(&Secret_key, seed_A, 1);
    seed_A = Secret_key;

    seed_A = seed_A ^ seed_C;

    seed_B = hold_seed2;        // New B seed
    seed_B &= 0x0007;
    seed_B = seed_B << 1;

    seed_B += offset;
    obd2sci_dcx_pull_table_vaule(&Secret_key, seed_B, 1);
    seed_B = Secret_key;

    seed_B = seed_A ^ seed_B;

    seed_B = hold_seed ^ seed_B;

    seed_B = 0x537E ^ seed_B;
    seed_C = seed_B & 0xFF00;
    seed_A = 0;
    seed_C =  seed_C >> 8;
    key[0] |=(seed_B & 0xFF00) >> 8;
    key[1] = seed_B & 0x00FF;

    key[0] = ((key[0] << 24) | (key[1] << 16));

    return S_SUCCESS;
}

//------------------------------------------------------------------------------
// Calculate keys for CAN processors with SCIB unlock algorithm
// Input:   u8 algoindex
//          u8 *vecSeed
//          u8 *vecKey
// Return:  u8  status
// Engineer: Ricardo Cigarroa
//------------------------------------------------------------------------------
u8 obd2can_dcx_calculatekey_SCIB(u32 *key, u8 seed1, u8 seed2, u8 algoindex)
{
   u8 status = 0;

   switch(algoindex)
   {
     case UnlockId_SCIB_UNLOCK:
          status = obd2sci_dcx_calculatekey_Type_SCIB(key,seed1,seed2);
          break;
     case UnlockId_JTECP_UNLOCK:
          //type_JTECP(key,seed1,seed);
          break;
     default:
          status = S_FAIL;
          break;
   }

   return status;
}

//------------------------------------------------------------------------------
// Inputs:  u8  *seed
//          u32 algoindex (index algorithm)
//          VehicleCommType commtype
// Output:  u8  *key
// Return:  u8  status
// Engineer: Ricardo Cigarroa
//------------------------------------------------------------------------------
u8 obd2_dcx_generatekey(u8 *seed, u32 algoindex, VehicleCommType commtype, 
                        u8 *key)
{
    u32 calc_key;
    u8 status = S_SUCCESS;

    if (commtype == CommType_CAN)
    {
        if (algoindex == UnlockId_None)
        {
            return S_NOTREQUIRED;
        }

        if(algoindex != UnlockId_SCIB_UNLOCK)
        {
            if(obd2can_dcx_calculatekey_non_SCIB(algoindex, seed, &calc_key) != S_SUCCESS)
            {
                status = S_FAIL;
            }
        }
        else
        {
            if(obd2can_dcx_calculatekey_SCIB(&calc_key, seed[0], seed[1], algoindex) != S_SUCCESS)
            {
                status = S_FAIL;
            }
        }

        key[3] = calc_key;
        key[2] = calc_key >> 8;
        key[1] = calc_key >> 16;
        key[0] = calc_key >> 24;
    }

    return status;
}

//------------------------------------------------------------------------------
// Check if a binary type is valid
// Input:   u16 veh_type (defined for veh_defs)
// Outputs: u32 *vidposition (position of VID in stock file)
// Return:  u8  status
// Engineer: Quyen Leba
// Note: all positions are assumed belonged to one ECM (the main ECM)
// Note: securitybyteposition is relevent to vidposition
// Note: all ranges/data must be inclusive
//------------------------------------------------------------------------------
u8 obd2_dcx_getoverlayposition_instockfile(u16 veh_type,
                                            u32 *vidposition, u32 *patsposition,
                                            u32 *securitybyteposition)
{
    u16 ecm_type;
    VehicleCommType commtype;
    VehicleCommLevel commlevel;
    u32 source_vidposition;
    u32 source_patsposition;
    u32 vid_offset;
    u32 pats_offset;
    u32 securitybyte_offset;
    u32 ecm_bytecount;
    u8  ecm_index;
    u32 memblock_address;
    u32 memblock_size;
    bool vidposition_found;
    bool patsposition_found;
    u8  i;
    u8  status;

    *vidposition = 0xFFFFFFFF;          //mark invalid position
    *patsposition = 0xFFFFFFFF;
    *securitybyteposition = 0xFFFFFFFF;

    vid_offset = pats_offset = securitybyte_offset = 0;
    vidposition_found = patsposition_found = FALSE;
    status = S_FAIL;
    for(ecm_index=0;ecm_index<ECM_MAX_COUNT;ecm_index++)
    {
        ecm_type = VEH_GetMainEcmType(veh_type);
        if (ecm_type == INVALID_ECM_DEF)
        {
            break;
        }

        if (VEH_GetMainEcmIndex(veh_type) != ecm_index)
        {
            //offset the whole ecm
            status = obd2tune_getecmsize_byecmtype(ecm_type,&ecm_bytecount);
            if (status != S_SUCCESS)
            {
                return status;
            }
            vid_offset += ecm_bytecount;
            pats_offset += ecm_bytecount;
            securitybyte_offset += ecm_bytecount;
        }
        else
        {
            source_vidposition = ECM_GetVidAddress(ecm_type);
            source_patsposition = ECM_GetPatsAddress(ecm_type);

            commtype = ECM_GetCommType(ecm_type);
            commlevel = ECM_GetCommLevel(ecm_type);

            for(i=0;i<MAX_ECM_MEMORY_BLOCKS;i++)
            {
                if (ECM_IsUploadCalOnly(ecm_type))
                {
                    memblock_address = ECM_GetCalBlockAddr(ecm_type,i);
                    memblock_size = ECM_GetCalBlockSize(ecm_type,i);
                }
                else
                {
                    memblock_address = ECM_GetOSReadBlockAddr(ecm_type,i);
                    memblock_size = ECM_GetOSReadBlockSize(ecm_type,i);
                }

                if (source_vidposition >= memblock_address &&
                    source_vidposition < (memblock_address + memblock_size))
                {
                    vidposition_found = TRUE;
                    vid_offset += (source_vidposition - memblock_address);

                    // Security byte address is 1 byte prior to the strategy
                    if (commtype == CommType_CAN)
                    {
                        if (commlevel == CommLevel_ISO14229)
                        {
                            securitybyte_offset = vid_offset - 126;
                        }
                        else if (commlevel == CommLevel_KWP2000)
                        {
                            securitybyte_offset = vid_offset - 123;
                        }
                        else
                        {
                            status = S_COMMLEVEL;
                            goto obd2_dcx_getoverlayposition_instockfile_done;
                        }
                    }
                    else if (commtype == CommType_SCP || commtype == CommType_SCP32)
                    {
                        securitybyte_offset = vid_offset - 123;
                    }
                    else
                    {
                        status = S_COMMTYPE;
                        goto obd2_dcx_getoverlayposition_instockfile_done;
                    }
                }
                else if (vidposition_found == FALSE)
                {
                    vid_offset += memblock_size;
                }

                if (source_patsposition >= memblock_address &&
                    source_patsposition < (memblock_address + memblock_size))
                {
                    patsposition_found = TRUE;
                    pats_offset += (source_patsposition - memblock_address);
                }
                else if (patsposition_found == FALSE)
                {
                    pats_offset += memblock_size;
                }
            }//for(i=...

            break;
        }
    }//for(ecm_index=...

    if (vidposition_found && patsposition_found)
    {
        *vidposition = vid_offset;
        *patsposition = pats_offset;
        *securitybyteposition = securitybyte_offset;
        status = S_SUCCESS;
    }
    else
    {
        status = S_FAIL;
    }

obd2_dcx_getoverlayposition_instockfile_done:
    return status;
}

//------------------------------------------------------------------------------
// Check for vehicle wake up
// Inputs:  u16 veh_type
//          u16 ecm_index (0,1,etc; 0xFE: all)
// Return:  u8  status
// Engineer: Quyen Leba
// Some vehicles require this wakeup prior to an unlock; also, some only allow
// this wakeup once.
//------------------------------------------------------------------------------
u8 obd2_dcx_wakeup(u16 veh_type, u16 ecm_index)
{
    VehicleCommType commtype;
    u32 loopcount;
    u32 i;
    u32 index;
    u8  status;
    u8  retry;
    u32 timeout;
    u32 ecm_type;
    u32 ecm_id;
    u32 response_id;
    u8  count;

    ecm_type = VEH_GetMainEcmType(veh_type);
    ecm_id = ECM_GetEcmId(ecm_type);
    response_id = ECM_GetResponseECMId(ecm_type);

    loopcount = 1;
    if (ecm_index == 0xFE)  //TODOQ: remove hardcoded 0xFE
    {
        loopcount = ECM_MAX_COUNT;
    }
    else if (ecm_index > ECM_MAX_COUNT)
    {
        return S_INPUT;
    }

    if (SETTINGS_IsDemoMode())
    {
        return S_SUCCESS;
    }

    status = S_SUCCESS;
    index = 0;
    retry = 5;
    count = 0;
    if (VEH_IsRequireWakeUp(veh_type))
    {
        obd2can_init(Obd2canInitNormal,0);
        for(i=0; i<5; i++)
        {
            while(retry)
            {
                status = can_rx(1000);
                if(status == S_SUCCESS)
                {
                    count++;
                    if(CanMsgFrameData(can_rxframe)[1] == 0x51 && count >= 2)
                    {
                        obd2can_dcx_initiate_diagnostic_operation(FALSE,ecm_id, response_id, diagnosticRequestChrysler_92);
                        obd2can_dcx_initiate_diagnostic_operation(FALSE,ecm_id, response_id, diagnosticRequestChrysler_85);
                        return status;
                    }
                }
                retry--;
            }
            break;
        }

        if ((VEH_IsCheckKeyOnPriorWakeup(veh_type)) && (!SETTINGS_IsDownloadFail()))
        {
            timeout = 10;
            do
            {
                delays(300,'m');

                status = obd2_checkkeyon(CommType_CAN, TRUE);

                if (status != S_SUCCESS && IsVariantId_DCX_GPEC2)
                {
                    break;
                }
                timeout--;
            }while(status != S_SUCCESS && timeout);

            if(timeout == 0)
            {
                return S_TIMEOUT;
            }
            //TODOQ: for now because iTSX app doesn't handle this yet
//            cmdif_internalresponse_ack(CMDIF_CMD_PCM_UNLOCK,
//                                       CMDIF_ACK_WAIT,NULL,0);
        }
        else
        {
            if(IsVariantId_DCX_GPEC2)
            {
                obd2can_dcx_exit_vehicle_specific_hack();
            }
        }

        for(i=0;i<loopcount;i++)
        {
            if (loopcount == 1)
                index = ecm_index;
            else
                index = i;

            if (VEH_GetEcmType(veh_type,index) == INVALID_ECM_DEF)
                break;

            commtype = ECM_GetCommType(VEH_GetEcmType(veh_type,index));
            if (commtype == CommType_CAN)
            {
                status = obd2can_dcx_wakeup(veh_type,index);
            }
            else if (commtype == CommType_SCIA || commtype == CommType_SCIB)
            {
                //SCI wake up would happen here. 
                //status = obd2sci_wakeup(veh_type,index);
            }
            else
            {
                status = S_COMMTYPE;
                break;
            }
            if (status != S_SUCCESS)
            {
                break;
            }
        }
    }
    return status;
}

//------------------------------------------------------------------------------
// Process any vehicle specific tasks before program
// Input:   flasherinfo *f_info
// Return:  u8  status
// Engineer: Ricardo Cigarroa
//------------------------------------------------------------------------------
u8 obd2_dcx_vehicle_specific_task_before_program(flasher_info *f_info)
{
    u8 status;
    u8 variant_id;
    u8  WA580 = 0;
    u8  diag_type = 0;
    u8 ecm_boot_version[10];
    VehicleCommType commtype;
    Obd2can_ServiceData data;
    
    obd2can_servicedata_init(&data);
  
    if (SETTINGS_IsDemoMode())
    {
        return S_SUCCESS;
    }
    
    /*Check bootloader version*/
    commtype = f_info->ecminfo.commtype[0];
    if (commtype == CommType_Unknown)
    {
        commtype = obd2_findvehiclecommtype();
    }

    switch (commtype)
    {
    case CommType_CAN:
        status = obd2can_dcx_request_byidentifier(0x9E, 0x7E0, 0x7E8, 1);
        if(status == S_SUCCESS)
        {
            variant_id = obd2_rxbuffer[4];
            memcpy(ecm_boot_version, &obd2_rxbuffer[10], 10);
        }

        if (status == S_SUCCESS)
        {
            switch (variant_id)
            {
                case 0x23:  // 2012-2013 GPEC2
                    if (strcmp((char*)ecm_boot_version, "05150475AD") == 0)
                    {
                        // 2012 GPEC2
                        variant_id = 1;
                    }
                    else if (strcmp((char*)ecm_boot_version, "05150739AB") == 0)
                    {
                        // 2013 GPEC2
                        // Do nothing
                    }
                    else
                    {
                        // Unrecognised boot version
                        variant_id = 0xFF;
                        status = S_FAIL;
                    }
                    break;
                case 0x25:  // 2014 GPEC2
                    if (strcmp((char*)ecm_boot_version, "05150826AA") == 0)
                    {
                        // 2014 GPEC2
                        // Do nothing
                    }
                    else
                    {
                        // Unrecognised boot version
                        variant_id = 0xFF;
                        status = S_FAIL;
                    }
                    break;
                default:
                    // Other processors (e.g. NGC4)
                    // Do nothing
                    break;
            }

            //variant_id is placed in the first element of ecm_second_codes.
            flasherinfo->ecminfo.second_codes[0][0][0] = variant_id;
        }
        break;
    default:
        /* Not required for non-CAN processors yet */
        status = S_SUCCESS;
        break;
    }
    
    /*Read tire size and axle ratio values and store them in settings*/
    status = obd2can_dcx_read_tire_axel_parameters(&WA580, data, &diag_type);
    if (status == S_SUCCESS)
    {
         SETTINGS_TUNE(stockwheels.axel) = ((data.rxdata[0]<< 8) + data.rxdata[1]); 
         SETTINGS_TUNE(stockwheels.tire) = ((data.rxdata[6]<< 8) + data.rxdata[7]);
    }

    return status;
}

//------------------------------------------------------------------------------
// Process any vehicle specific tasks after program
// Input:   flasher_info *f_info
// Return:  u8  status
// Engineer: Ricardo Cigarroa
//------------------------------------------------------------------------------
u8 obd2_dcx_vehicle_specific_task_after_program(flasher_info *f_info)
{
    u32 ecm_type;

    if(SETTINGS_IsDemoMode())
    {
        return S_SUCCESS; 
    }
    
    ecm_type = VEH_GetMainEcmType(f_info->vehicle_type);

    //Key off/on in order to clear DTCs successfully if required.
    if(ECM_IsDtcKeyRequired(ecm_type))
    {
        cmdif_remotegui_msg_keyoff(CMDIF_CMD_DO_DOWNLOAD, "PROGRAM VEHICLE");
        cmdif_remotegui_msg_keyon(CMDIF_CMD_DO_DOWNLOAD, "PROGRAM VEHICLE");
    }
    
    cmdif_internalresponse_ack(CMDIF_CMD_DO_DOWNLOAD,
                               CMDIF_ACK_CLEAR_DTC_ECU,NULL,0);

    obd2_cleardtc(CommType_CAN,0);

    if(ECM_IsClearKam(ecm_type))
    {
        obd2can_dcx_clear_kam(ECM_GetEcmId(ecm_type), ECM_GetCommLevel(ecm_type));
    }

    return S_SUCCESS;
}

//------------------------------------------------------------------------------
// Format vehicleinfo to text string
// Input:   vehicle_info *vehicleinfo
// Output:  u8  *vehicleinfo_text
// Return:  u8  status
// Engineer: Ricardo Cigarroa
//------------------------------------------------------------------------------
u8 obd2_dcx_vehicleinfototext(vehicle_info *vehicleinfo, u8 *vehicleinfo_text)
{
    u8  commtype_text[32];
    u8  buffer[1];
    u8  *ptr;
    u8  status;

    ptr = vehicleinfo_text;
    strcpy((char*)vehicleinfo->vin,(char*)vehicleinfo->ecm.vin);

    obd2_vehiclecommtotext(vehicleinfo->commtype,commtype_text);

    strcpy((char*)ptr,"<HEADER>VIN:</HEADER> ");
    status = obd2_validatevin(vehicleinfo->vin);
    if (status == S_VINBLANK || status == S_VININVALID)
    {
        strcat((char*)ptr,"Blank");
    }
    else if (status == S_SUCCESS)
    {
        strcat((char*)ptr,(char*)vehicleinfo->vin);
    }
    else
    {
        strcat((char*)ptr,"N/A");
    }

    strcat((char*)ptr,"<HEADER>COMM:</HEADER> ");
    strcat((char*)ptr,(char*)commtype_text);
    strcat((char*)ptr,"<HEADER>ECU OS Part Number:</HEADER> ");
    if (vehicleinfo->ecm.ecm_count == 0 || vehicleinfo->ecm.codecount[0] == 0)
    {
        strcat((char*)ptr,"Not Found");
    }
    else
    {
        strcat((char*)ptr,(char*)vehicleinfo->ecm.codes[0][0]);
        if (vehicleinfo->ecm.second_codes[0][1] != NULL &&
            vehicleinfo->commtype == CommType_CAN)
        {
           strcat((char*)ptr,"<HEADER>ECU Part Type:</HEADER> ");
           strcat((char*)ptr,(char*)vehicleinfo->ecm.second_codes[0][1]);
           strcat((char*)ptr,":");
           itoa((s32)vehicleinfo->ecm.second_codes[0][2][0],buffer, 10);
           strcat((char*)ptr,(char*)buffer);
        }

         /* Print TCM 0S part number and tcu part type */
        if (vehicleinfo->ecm.ecm_count > 1 && 
            vehicleinfo->commtype == CommType_CAN)
        {
            if (vehicleinfo->ecm.codes[1][0] != NULL)
            {
               strcat((char*)ptr,"<HEADER>TCU OS Part Number:</HEADER> ");
               strcat((char*)ptr,(char*)vehicleinfo->ecm.codes[1][0]);
            }

            if (vehicleinfo->ecm.second_codes[1][1] != NULL)
            {
               strcat((char*)ptr,"<HEADER>TCU Part Type:</HEADER> ");
               strcat((char*)ptr,(char*)vehicleinfo->ecm.second_codes[0][1]);
               strcat((char*)ptr,":");
               itoa((s32)vehicleinfo->ecm.second_codes[1][2][0],buffer, 10);
               strcat((char*)ptr,(char*)buffer);
            }
        }

    }
    
    //######################################################################
    // Boot Version
    //######################################################################

    if (vehicleinfo->ecm.second_codes[0][3]!= NULL && 
        vehicleinfo->commtype == CommType_CAN)
    {
        strcat((char*)ptr,"<HEADER>Boot Version:</HEADER> ");
        strcat((char*)ptr,(char*)vehicleinfo->ecm.second_codes[0][3]);
    }
    
    return S_SUCCESS;
}

//------------------------------------------------------------------------------
// Calculate keys for CAN processors (non-SCI_B algorithm)
// Input:   u8 algoindex
//          u8 *vecSeed
//          u8 *vecKey
// Return:  u8  status
// Engineer: Quyen Leba, Patrick Downs
//------------------------------------------------------------------------------
u8 obd2can_dcx_calculatekey_non_SCIB(u8 algoindex, u8 *vecSeed, u32 *vecKey)
{
    unsigned int scrambledSeed, seedMagic1, seedMagic2, magic1, magic2 = 0;
    unsigned int seed, key, key_holder[4];

    seed = (vecSeed[0] << 24) + (vecSeed[1] << 16) + (vecSeed[2] << 8) + vecSeed[3];

    switch(algoindex)
    {
       case UnlockId_HEMI_BOOTLOADER_SECURITY_DOWNL1_05:
            magic1 = 0x3F711F5A;
            magic2 = 0xC3573AE9;
            break;

       case UnlockId_HEMI_APP_SECURITY_ACCESS1_03:
       case UnlockId_HEMI_APP_SECURITY_ACCESS1_65:
            magic1 = 0x938284D4;
            magic2 = 0x741500C0;
            break;
       case UnlockId_HEMI_BOOTLOADER_SECURITY_UPLOAD2_63:
            magic1 = 0x1F234AAD;
            magic2 = 0xD0D93706;
            break;

       case UnlockId_HEMI_BOOTLOADER_SECURITY_UPLOAD3_63:
            magic1 = 0xCE853A6F;
            magic2 = 0x3BA8FDC7;
            break;

       case UnlockId_HEMI_BOOTLOADER_SECURITY_DOWNL2_05:
            magic1 = 0x966AEEB1;
            magic2 = 0x440BCE28;
            break;

       case UnlockId_HEMI_APP_SECURITY_ACCESS2_65:
            magic1 = 0x549AE786;
            magic2 = 0xB54FC8D9;
            break;

       case UnlockId_HEMI_BOOTLOADER_SECURITY_UPLOAD4_63:
            magic1 = 0x47EC21F8;
            magic2 = 0xCFB81A2E;
            break;

       case UnlockId_HEMI_APP_SECURITY_ACCESS3_65:
            magic1 = 0xE78A5B13;
            magic2 = 0x1C56932A;
            break;

       case UnlockId_HEMI_APP_SECURITY_ACCESS4_65:
            magic1 = 0xE684A035;
            magic2 = 0xA778CB7B;
            break;

       case UnlockId_HEMI_BOOTLOADER_SECURITY_UPLOAD1_63:
       default:
             magic1 = 0x725EF016;
             magic2 = 0x58329671;
             break;
    }

    scrambledSeed = (((seed & 0x00FF0000)<<8) + ((seed  & 0xFF000000) >> 8) + ((seed & 0x0000FF00) >> 8) + ((seed & 0x000000FF) << 8));

    scrambledSeed = (scrambledSeed << 11) + (scrambledSeed >> 22);

    seedMagic2 = (seed & magic2);

    seedMagic1 = (seedMagic2 ^ scrambledSeed);

    key = (seedMagic1 ^ magic1);

    key_holder[0]=((key >> 24) & 0xFF);
    key_holder[1]=((key >> 16) & 0xFF);
    key_holder[2]=((key >> 8) & 0xFF);
    key_holder[3]=((key & 0xFF));

    vecKey[0] = (key_holder[0] << 24 | (key_holder[1] << 16) | (key_holder[2] <<8) | key_holder[3]);

    return S_SUCCESS;
}

/**
 * @brief   Ford Special Functions Menu
 *
 * @param   [in] commtype
 * @param   [in] commlevel
 *
 * @details This function checks commtype and commlevel before calling the
 *          OEM specific special functions menu.
 *
 * @author  Tristen Pierson
 */
void obd2_dcx_vehiclefunction(VehicleCommType *commtype, VehicleCommLevel *commlevel)
{
    CMDIF_REMOTEGUI_RESPONSETYPE responsetype;
    CMDIF_COMMAND cmd = CMDIF_CMD_SPECIAL_FUNCTIONS;
    u16 responsechoice;
    u8  functions[MAX_VEHICLE_FUNCTIONS];
    u8  variant_id; 
    u8  i;

    // Nonvolatile Memory Reset

    // So far we only support CAN special functions
    if (commtype[0] == CommType_CAN)
    {
        functions[0] = VehicleFunction_Clear_TCM_Adaptives;
        functions[1] = VehicleFunction_KAM_Reset;
        functions[2] = VehicleFunction_Traction_Ctrl;
        functions[3] = VehicleFunction_Reset_ECM;
        functions[4] = VehicleFunction_Tire_size_Axle_Ratio;
        functions[5] = VehicleFunction_ETC_Relearn;
#ifdef __DEBUG_JTAG_
        functions[6] = VehicleFunction_Bootloader_Info;
        functions[7] = VehicleFunction_EOL;
#else
        functions[6] = VehicleFunction_EOL;
#endif
    }
    else
    {
        functions[0] = VehicleFunction_EOL;
    }

    while(1)
    {
        if (cmdif_remotegui_listboxbox_init("SPECIAL FUNCTIONS", NULL) == S_SUCCESS)
        {
            // Build menu
            for (i = 0; i < MAX_VEHICLE_FUNCTIONS; i++)
            {
                if (functions[i] == VehicleFunction_EOL)
                {
                    break;
                }

                switch (functions[i])
                {
                case VehicleFunction_Clear_TCM_Adaptives:
                    cmdif_remotegui_listboxbox_additem("RESET TCM ADAPTIVES", "", CMDIF_REMOTEGUI_LISTBOX_FLAGS_CLICKABLE | CMDIF_REMOTEGUI_LISTBOX_FLAGS_ALIGN_CENTER);
                    break;
                 case VehicleFunction_KAM_Reset:
                    cmdif_remotegui_listboxbox_additem("RESET NONVOLATILE MEMORY", "", CMDIF_REMOTEGUI_LISTBOX_FLAGS_CLICKABLE | CMDIF_REMOTEGUI_LISTBOX_FLAGS_ALIGN_CENTER);
                    break;
                case VehicleFunction_Traction_Ctrl:
                    cmdif_remotegui_listboxbox_additem("TOGGLE TRACTION CONTROL", "", CMDIF_REMOTEGUI_LISTBOX_FLAGS_CLICKABLE | CMDIF_REMOTEGUI_LISTBOX_FLAGS_ALIGN_CENTER);
                    break;
                case VehicleFunction_Reset_ECM:
                    cmdif_remotegui_listboxbox_additem("POWER ON RESET", "", CMDIF_REMOTEGUI_LISTBOX_FLAGS_CLICKABLE | CMDIF_REMOTEGUI_LISTBOX_FLAGS_ALIGN_CENTER);
                    break;
                case VehicleFunction_Tire_size_Axle_Ratio:
                    cmdif_remotegui_listboxbox_additem("TIRE SIZE & AXLE RATIO", "", CMDIF_REMOTEGUI_LISTBOX_FLAGS_CLICKABLE | CMDIF_REMOTEGUI_LISTBOX_FLAGS_ALIGN_CENTER);
                    break;
                case VehicleFunction_ETC_Relearn:
                    cmdif_remotegui_listboxbox_additem("ETC RLEARN", "", CMDIF_REMOTEGUI_LISTBOX_FLAGS_CLICKABLE | CMDIF_REMOTEGUI_LISTBOX_FLAGS_ALIGN_CENTER);
                    break;
#ifdef __DEBUG_JTAG_
                case VehicleFunction_Bootloader_Info:
                    cmdif_remotegui_listboxbox_additem("REQUEST BOOTLOADER INFO", "", CMDIF_REMOTEGUI_LISTBOX_FLAGS_CLICKABLE | CMDIF_REMOTEGUI_LISTBOX_FLAGS_ALIGN_CENTER);
                    break;
#endif
                default:
                    break;
                }
            }
            // If no functions are available (list is empty)
            if (i == 0)
            {
                cmdif_remotegui_listboxbox_cleanup();
                cmdif_remotegui_messagebox(cmd,
                                           "SPECIAL FUNCTIONS", CMDIF_REMOTEGUI_ICON_NONE,
                                           CMDIF_REMOTEGUI_BUTTONFACE_Exit, CMDIF_REMOTEGUI_BUTTONFACE_None, CMDIF_REMOTEGUI_BUTTONFACE_None,
                                           CMDIF_REMOTEGUI_FOCUS_None,
                                           "No special functions are available for this vehicle");
            }
            // Show listbox if functions are available
            else
            {
                cmdif_remotegui_listbox(cmd, 0, 0,
                                        CMDIF_REMOTEGUI_BUTTONFACE_Exit, CMDIF_REMOTEGUI_BUTTONFACE_None, CMDIF_REMOTEGUI_BUTTONFACE_None,
                                        CMDIF_REMOTEGUI_FOCUS_ListBox);
            }
            cmdif_remotegui_wait_user_response(&responsetype, &responsechoice, NULL);

            // Exit button selected
            if (responsetype == CMDIF_REMOTEGUI_RESPONSETYPE_BUTTON)
            {
                return;
            }
            // Menu item selected
            else if (responsetype == CMDIF_REMOTEGUI_RESPONSETYPE_LIST)
            {
                //key on and continue.
//                cmdif_remotegui_msg_keyon(cmd,"SPECIAL FUNCTIONS");
                  if(obd2_checkkeyon(CommType_Unknown, TRUE) == S_NOCOMMUNICATION)
                  {
                     cmdif_remotegui_msg_keyon(cmd,"SPECIAL FUNCTIONS");
                  }


                switch (functions[responsechoice])
                {
                case VehicleFunction_Clear_TCM_Adaptives:
                    obd2_dcx_tcm_clear_adaptives(commtype[1], commlevel[1]);
                    break;
                case VehicleFunction_KAM_Reset:
                    obd2_dcx_reset_memory(commtype[0], commlevel[0]);
                    break;
                case VehicleFunction_Traction_Ctrl:
                    /* Get variant ID in order to prevent toggling traction control on 
                    2015 vehicles. */
                    variant_id = 0; 
                    if (obd2can_dcx_request_byidentifier(0x9E, 0x7E0, 0x7E8, 0) == S_SUCCESS)
                    {
                        variant_id = obd2_rxbuffer[4];
                        if (variant_id == 0x26 || variant_id == 0x44)
                        {
                            cmdif_remotegui_messagebox(cmd,
                                         "SPECIAL FUNCTIONS", CMDIF_REMOTEGUI_ICON_NONE,
                                         CMDIF_REMOTEGUI_BUTTONFACE_Exit, CMDIF_REMOTEGUI_BUTTONFACE_None, CMDIF_REMOTEGUI_BUTTONFACE_None,
                                         CMDIF_REMOTEGUI_FOCUS_None,
                                         "<H>TRACTION CONTROL\nThis function is not supported on this vehicle");
                            cmdif_remotegui_wait_user_response(&responsetype, &responsechoice, NULL);
                            break; 
                        }
                    }
                    obd2_dcx_traction_control(commtype[0], commlevel[0]);
                    break;
                case VehicleFunction_Reset_ECM:
                    obd2_dcx_power_on_reset(commtype[0], commlevel[0]);
                    break;
                case VehicleFunction_Tire_size_Axle_Ratio:
                    obd2_dcx_tire_size_axle_ratio(commtype[0], commlevel[0]);
                    break;
                case VehicleFunction_ETC_Relearn:
                    obd2_dcx_ETC_relearn(commtype[0], commlevel[0]);
                    break;
#ifdef __DEBUG_JTAG_
                case VehicleFunction_Bootloader_Info:
                    obd2_dcx_bootloader_info(commtype[0], commlevel[0]);
                    break;
#endif
                default:
                    break;
                }
                //key off and continue.
                cmdif_remotegui_msg_keyoff(cmd,"SPECIAL FUNCTIONS");
            }
        } // if(...)
        else
        {
            break;
        }
    } // while(1)
    return;
}

/**
 * @brief   Clear TCM Adaptives
 *
 * @param   [in] commtype
 * @param   [in] commlevel
 *
 * @author  Tristen Pierson
 */
void obd2_dcx_tcm_clear_adaptives(VehicleCommType commtype, VehicleCommLevel commlevel)
{
    CMDIF_COMMAND cmd = CMDIF_CMD_SPECIAL_FUNCTIONS;
    CMDIF_REMOTEGUI_RESPONSETYPE responsetype;
    u16 responsechoice;
    u8 status;

    enum {
        tcmClearAdaptivesContinue   = CMDIF_REMOTEGUI_BUTTONTYPE_RIGHT,
        tcmClearAdaptivesCancel     = CMDIF_REMOTEGUI_BUTTONTYPE_LEFT,
    };

    if (commtype == CommType_CAN)
    {

        cmdif_remotegui_messagebox(cmd,
                                   "SPECIAL FUNCTIONS", CMDIF_REMOTEGUI_ICON_NONE,
                                   CMDIF_REMOTEGUI_BUTTONFACE_Cancel, CMDIF_REMOTEGUI_BUTTONFACE_Continue, CMDIF_REMOTEGUI_BUTTONFACE_None,
                                   CMDIF_REMOTEGUI_FOCUS_Button0,
                                   "<HEADER>RESET TCM ADAPTIVES</HEADER>\nThis function resets the transmissions adaptive strategy.");
        cmdif_remotegui_wait_user_response(&responsetype, &responsechoice, NULL);

        switch (responsechoice)
        {
        case tcmClearAdaptivesContinue:
            cmdif_remotegui_messagebox(cmd,
                                       "SPECIAL FUNCTIONS", CMDIF_REMOTEGUI_ICON_NONE,
                                       CMDIF_REMOTEGUI_BUTTONFACE_None, CMDIF_REMOTEGUI_BUTTONFACE_None, CMDIF_REMOTEGUI_BUTTONFACE_None,
                                       CMDIF_REMOTEGUI_FOCUS_None,
                                       "<HEADER>RESET TCM ADAPTIVES</HEADER>\nProcessing...");
            status = obd2can_dcx_clear_adaptives(Obd2CanEcuId_7E1, commlevel);
            break;
        case tcmClearAdaptivesCancel:
            status = S_USERABORT;
            break;
        }
    }
    else
    {
        status = S_NOTSUPPORT;
    }

    if (status == S_SUCCESS)
    {
        cmdif_remotegui_messagebox(cmd,
                                   "SPECIAL FUNCTIONS", CMDIF_REMOTEGUI_ICON_NONE,
                                   CMDIF_REMOTEGUI_BUTTONFACE_Exit, CMDIF_REMOTEGUI_BUTTONFACE_None, CMDIF_REMOTEGUI_BUTTONFACE_None,
                                   CMDIF_REMOTEGUI_FOCUS_None,
                                   "<HEADER>RESET TCM ADAPTIVES</HEADER>\nComplete");
    }
    else if (status == S_NOTSUPPORT)
    {
        cmdif_remotegui_messagebox(cmd,
                                   "SPECIAL FUNCTIONS", CMDIF_REMOTEGUI_ICON_NONE,
                                   CMDIF_REMOTEGUI_BUTTONFACE_Exit, CMDIF_REMOTEGUI_BUTTONFACE_None, CMDIF_REMOTEGUI_BUTTONFACE_None,
                                   CMDIF_REMOTEGUI_FOCUS_None,
                                   "<HEADER>RESET TCM ADAPTIVES</HEADER>\nERROR: TCM not present or Reset TCM Adaptives is not supported.");
    }
    else if (status == S_USERABORT)
    {
        return;
    }
    else
    {
        cmdif_remotegui_messagebox(cmd,
                                   "SPECIAL FUNCTIONS", CMDIF_REMOTEGUI_ICON_NONE,
                                   CMDIF_REMOTEGUI_BUTTONFACE_Exit, CMDIF_REMOTEGUI_BUTTONFACE_None, CMDIF_REMOTEGUI_BUTTONFACE_None,
                                   CMDIF_REMOTEGUI_FOCUS_None,
                                   "<HEADER>RESET TCM ADAPTIVES</HEADER>\nERROR: Failed to reset TCM Adaptives.");
    }
    cmdif_remotegui_wait_user_response(&responsetype, &responsechoice, NULL);
}

/**
 * @brief   Nonvolatile Memory Reset
 *
 * @param   [in] commtype
 * @param   [in] commlevel
 *
 * @author  Tristen Pierson
 */
void obd2_dcx_reset_memory(VehicleCommType commtype, VehicleCommLevel commlevel)
{
    CMDIF_COMMAND cmd = CMDIF_CMD_SPECIAL_FUNCTIONS;
    CMDIF_REMOTEGUI_RESPONSETYPE responsetype;
    u16 responsechoice;
    u8 status;

    enum {
        resetMemoryContinue = CMDIF_REMOTEGUI_BUTTONTYPE_RIGHT,
        resetMemoryCancel   = CMDIF_REMOTEGUI_BUTTONTYPE_LEFT,
    };

    if (commtype == CommType_CAN)
    {
        cmdif_remotegui_messagebox(cmd,
                                   "SPECIAL FUNCTIONS", CMDIF_REMOTEGUI_ICON_NONE,
                                   CMDIF_REMOTEGUI_BUTTONFACE_Cancel, CMDIF_REMOTEGUI_BUTTONFACE_Continue, CMDIF_REMOTEGUI_BUTTONFACE_None,
                                   CMDIF_REMOTEGUI_FOCUS_Button1,
                                   "<HEADER>RESET MEMORY</HEADER>\nNonvolatile Memory Reset is used to reset ECU RAM only.");
        cmdif_remotegui_wait_user_response(&responsetype, &responsechoice, NULL);

        switch (responsechoice)
        {
        case resetMemoryContinue:
            cmdif_remotegui_messagebox(cmd,
                                       "SPECIAL FUNCTIONS", CMDIF_REMOTEGUI_ICON_NONE,
                                       CMDIF_REMOTEGUI_BUTTONFACE_None, CMDIF_REMOTEGUI_BUTTONFACE_None, CMDIF_REMOTEGUI_BUTTONFACE_None,
                                       CMDIF_REMOTEGUI_FOCUS_None,
                                       "<HEADER>RESET MEMORY</HEADER>\nProcessing...");
            status = obd2can_dcx_clear_kam(Obd2CanEcuId_7E0, commlevel);
            break;
        case resetMemoryCancel:
            status = S_USERABORT;
            break;
        }
    }
    else
    {
        status = S_NOTSUPPORT;
    }

    if (status == S_SUCCESS)
    {
        cmdif_remotegui_messagebox(cmd,
                                   "SPECIAL FUNCTIONS", CMDIF_REMOTEGUI_ICON_NONE,
                                   CMDIF_REMOTEGUI_BUTTONFACE_Exit, CMDIF_REMOTEGUI_BUTTONFACE_None, CMDIF_REMOTEGUI_BUTTONFACE_None,
                                   CMDIF_REMOTEGUI_FOCUS_None,
                                   "<HEADER>RESET MEMORY</HEADER>\nNonvolatile Memory Reset Complete.");
    }
    else if (status == S_NOTSUPPORT)
    {
        cmdif_remotegui_messagebox(cmd,
                                   "SPECIAL FUNCTIONS", CMDIF_REMOTEGUI_ICON_NONE,
                                   CMDIF_REMOTEGUI_BUTTONFACE_Exit, CMDIF_REMOTEGUI_BUTTONFACE_None, CMDIF_REMOTEGUI_BUTTONFACE_None,
                                   CMDIF_REMOTEGUI_FOCUS_None,
                                   "<HEADER>RESET MEMORY</HEADER>\nERROR: This vehicle does not support nonvolatile memory reset.");
    }
    else if (status == S_USERABORT)
    {
        return;
    }
    else
    {
        cmdif_remotegui_messagebox(cmd,
                                   "SPECIAL FUNCTIONS", CMDIF_REMOTEGUI_ICON_NONE,
                                   CMDIF_REMOTEGUI_BUTTONFACE_Exit, CMDIF_REMOTEGUI_BUTTONFACE_None, CMDIF_REMOTEGUI_BUTTONFACE_None,
                                   CMDIF_REMOTEGUI_FOCUS_None,
                                   "<HEADER>RESET MEMORY</HEADER>\nERROR: Nonvolatile Memory Reset Failed.");
    }
    cmdif_remotegui_wait_user_response(&responsetype, &responsechoice, NULL);
}

/**
 * @brief   Traction Control
 *
 * @param   [in] commtype
 * @param   [in] commlevel
 *
 * @author  Tristen Pierson
 */
void obd2_dcx_traction_control(VehicleCommType commtype, VehicleCommLevel commlevel)
{
    CMDIF_COMMAND cmd = CMDIF_CMD_SPECIAL_FUNCTIONS;
    CMDIF_REMOTEGUI_RESPONSETYPE responsetype;
    u16 responsechoice;
    u8 status;

    enum {
        tractionControlOn       = CMDIF_REMOTEGUI_BUTTONTYPE_MIDDLE,
        tractionControlOff      = CMDIF_REMOTEGUI_BUTTONTYPE_RIGHT,
        tractionControlCancel   = CMDIF_REMOTEGUI_BUTTONTYPE_LEFT,
    };

    if (commtype == CommType_CAN)
    {
        cmdif_remotegui_messagebox(cmd,
                                   "SPECIAL FUNCTIONS", CMDIF_REMOTEGUI_ICON_NONE,
                                   CMDIF_REMOTEGUI_BUTTONFACE_Cancel, CMDIF_REMOTEGUI_BUTTONFACE_Continue, CMDIF_REMOTEGUI_BUTTONFACE_None,
                                   CMDIF_REMOTEGUI_FOCUS_Button0,
                                   "<H>TRACTION CONTROL\nWARNING\nTurning Traction Control off may disable cruise control!");
        cmdif_remotegui_wait_user_response(&responsetype, &responsechoice, NULL);

        if (responsechoice == tractionControlCancel)
        {
            return;
        }

        while(1)
        {
            cmdif_remotegui_messagebox(cmd,
                                       "SPECIAL FUNCTIONS", CMDIF_REMOTEGUI_ICON_NONE,
                                       CMDIF_REMOTEGUI_BUTTONFACE_Cancel, CMDIF_REMOTEGUI_BUTTONFACE_TurnOff, CMDIF_REMOTEGUI_BUTTONFACE_TurnOn,
                                       CMDIF_REMOTEGUI_FOCUS_Button0,
                                       "<HEADER>TRACTION CONTROL</HEADER>\nTurn Traction Control On or Off.");
            cmdif_remotegui_wait_user_response(&responsetype, &responsechoice, NULL);

            switch (responsechoice)
            {
            case tractionControlOn:
                cmdif_remotegui_messagebox(cmd,
                                           "SPECIAL FUNCTIONS", CMDIF_REMOTEGUI_ICON_NONE,
                                           CMDIF_REMOTEGUI_BUTTONFACE_None, CMDIF_REMOTEGUI_BUTTONFACE_None, CMDIF_REMOTEGUI_BUTTONFACE_None,
                                           CMDIF_REMOTEGUI_FOCUS_None,
                                           "<HEADER>TRACTION CONTROL</HEADER>\nProcessing...");
                status = obd2can_dcx_traction_control(1, commlevel);
                break;
            case tractionControlOff:
                cmdif_remotegui_messagebox(cmd,
                                           "SPECIAL FUNCTIONS", CMDIF_REMOTEGUI_ICON_NONE,
                                           CMDIF_REMOTEGUI_BUTTONFACE_None, CMDIF_REMOTEGUI_BUTTONFACE_None, CMDIF_REMOTEGUI_BUTTONFACE_None,
                                           CMDIF_REMOTEGUI_FOCUS_None,
                                           "<HEADER>TRACTION CONTROL</HEADER>\nProcessing...");
                status = obd2can_dcx_traction_control(0, commlevel);
                break;
            case tractionControlCancel:
                status = S_USERABORT;
                break;
            }

            if (status == S_SUCCESS)
            {
                if (responsechoice == tractionControlOn)
                {
                    cmdif_remotegui_messagebox(cmd,
                                               "SPECIAL FUNCTIONS", CMDIF_REMOTEGUI_ICON_NONE,
                                               CMDIF_REMOTEGUI_BUTTONFACE_Exit, CMDIF_REMOTEGUI_BUTTONFACE_None, CMDIF_REMOTEGUI_BUTTONFACE_None,
                                               CMDIF_REMOTEGUI_FOCUS_None,
                                               "<HEADER>TRACTION CONTROL</HEADER>\nTraction Control has been turned ON.");
                }
                else
                {
                    cmdif_remotegui_messagebox(cmd,
                                               "SPECIAL FUNCTIONS", CMDIF_REMOTEGUI_ICON_NONE,
                                               CMDIF_REMOTEGUI_BUTTONFACE_Exit, CMDIF_REMOTEGUI_BUTTONFACE_None, CMDIF_REMOTEGUI_BUTTONFACE_None,
                                               CMDIF_REMOTEGUI_FOCUS_None,
                                               "<HEADER>TRACTION CONTROL</HEADER>\nTraction Control has been turned OFF.");
                }
            }
            else if (status == S_USERABORT)
            {
                break;
            }
            else
            {
                cmdif_remotegui_messagebox(cmd,
                                           "SPECIAL FUNCTIONS", CMDIF_REMOTEGUI_ICON_NONE,
                                           CMDIF_REMOTEGUI_BUTTONFACE_Exit, CMDIF_REMOTEGUI_BUTTONFACE_None, CMDIF_REMOTEGUI_BUTTONFACE_None,
                                           CMDIF_REMOTEGUI_FOCUS_None,
                                           "<HEADER>TRACTION CONTROL</HEADER>\nERROR: Failed to toggle Traction Control.");
            }
            cmdif_remotegui_wait_user_response(&responsetype, &responsechoice, NULL);
        } // while(1)
    } // if(...)
    else
    {
        cmdif_remotegui_messagebox(cmd,
                                   "SPECIAL FUNCTIONS", CMDIF_REMOTEGUI_ICON_NONE,
                                   CMDIF_REMOTEGUI_BUTTONFACE_Exit, CMDIF_REMOTEGUI_BUTTONFACE_None, CMDIF_REMOTEGUI_BUTTONFACE_None,
                                   CMDIF_REMOTEGUI_FOCUS_None,
                                   "<HEADER>TRACTION CONTROL</HEADER>\nVehicle does not support this function.");
        cmdif_remotegui_wait_user_response(&responsetype, &responsechoice, NULL);
    }
}

void obd2_dcx_tire_size_axle_ratio(VehicleCommType commtype, VehicleCommLevel commlevel)
{
    CMDIF_REMOTEGUI_RESPONSETYPE responsetype;
    CMDIF_COMMAND cmd = CMDIF_CMD_SPECIAL_FUNCTIONS;
    Obd2can_ServiceData contactdata;
    VehicleCommType vehiclecommtype = CommType_CAN;
    ecm_info ecminfo;
    u16 responsechoice;
    u8  title[24] = "SPECIAL FUNCTIONS";
    u8  stock_settings = 0;
    u8  WA580 = 0;
    u8  tire_changed = 0;
    u8  axle_changed = 0;
    u8  data_buffer[30];
    u8  functions[4];
    u8  print_buffer[80];
    u8  status;
    u8  i;
    u8  diag_type = 0;
    u8  settings_blank = 0xFF;
    u32 temp_number = 0; 
    u32 stock_tire_value = 0;
    u32 stock_axle_value = 0; 
    float conversion = 0.01254;
    union sintconv {char hexval[2]; float flval; unsigned int unsint; signed int sint;};
    union sintconv tire;
    union sintconv axle;
    float userinput_fval;

    enum {
        Change_Tire_size        = 1,
        Change_Axle_ratio       = 2,
        Set_new_parameters      = 3,
        Return_stock_parameters = 4
    };

    // So far we only support CAN special functions
    if (commtype == CommType_CAN)
    {
        functions[0] = Change_Tire_size;
        functions[1] = Change_Axle_ratio;
        functions[2] = Set_new_parameters;
        functions[3] = Return_stock_parameters;
        //obd2can_rxinfo_init(&rxinfo);
        memset(data_buffer, 0, 30);
    }
    else
    {
        cmdif_remotegui_messagebox(cmd,
                                   title, CMDIF_REMOTEGUI_ICON_NONE,
                                   CMDIF_REMOTEGUI_BUTTONFACE_Exit, CMDIF_REMOTEGUI_BUTTONFACE_None, CMDIF_REMOTEGUI_BUTTONFACE_None,
                                   CMDIF_REMOTEGUI_FOCUS_None,
                                   "<H>TIRE SIZE & AXLE RATIO\nVehicle does not support this function.");
        cmdif_remotegui_wait_user_response(&responsetype, &responsechoice, NULL);
        return;
    }

    obd2can_servicedata_init(&contactdata);
    contactdata.ecm_id = 0x620;

    obd2_readecminfo(&ecminfo,&vehiclecommtype);
    if ((!SETTINGS_IsMarried()) | (!(obd2tune_isvin_matched(ecminfo.vin,SETTINGS_TUNE(vin),0))))
    {
        cmdif_remotegui_messagebox(cmd,
                                   title, CMDIF_REMOTEGUI_ICON_NONE,
                                   CMDIF_REMOTEGUI_BUTTONFACE_Exit, CMDIF_REMOTEGUI_BUTTONFACE_None, CMDIF_REMOTEGUI_BUTTONFACE_None,
                                   CMDIF_REMOTEGUI_FOCUS_None,
                                   "<H>TIRE SIZE & AXLE RATIO\nERROR: Device needs to be married to the vehicle first.");
        cmdif_remotegui_wait_user_response(&responsetype, &responsechoice, NULL);
  
        return;
      
    }

    // Read tire size and axle ratio in from vehicle
    status = obd2can_dcx_read_tire_axel_parameters(&WA580, contactdata, &diag_type);
    if(status != S_SUCCESS)
    {
        cmdif_remotegui_messagebox(cmd,
                                   title, CMDIF_REMOTEGUI_ICON_NONE,
                                   CMDIF_REMOTEGUI_BUTTONFACE_Exit, CMDIF_REMOTEGUI_BUTTONFACE_None, CMDIF_REMOTEGUI_BUTTONFACE_None,
                                   CMDIF_REMOTEGUI_FOCUS_None,
                                   "<H>TIRE SIZE & AXLE RATIO\nERROR: Failed to read setting.");
        cmdif_remotegui_wait_user_response(&responsetype, &responsechoice, NULL);

        return;
    }
    memcpy((void*)data_buffer, (void*)contactdata.rxdata, 30);


    stock_tire_value = ((contactdata.rxdata[6]<< 8) + contactdata.rxdata[7]);
    tire.flval = 0;

    tire.hexval[0] = contactdata.rxdata[7];
    tire.hexval[1] = contactdata.rxdata[6];
    tire.flval = tire.unsint * conversion;

    
    stock_axle_value = ((contactdata.rxdata[0]<< 8) + contactdata.rxdata[1]);
    
    axle.flval = 0;
    axle.hexval[1] = contactdata.rxdata[0];
    axle.hexval[0] = contactdata.rxdata[1];
    axle.flval = (float)axle.unsint/1000;

    // stay until exit is hit
    while(1)
    {
        if (cmdif_remotegui_listboxbox_init(title, NULL) == S_SUCCESS)
        {
            // Build menu
            for (i = 0; i < 5; i++)
            {
                switch (functions[i])
                {
                case Change_Tire_size:
                    cmdif_remotegui_listboxbox_additem("Change Tire size", "", CMDIF_REMOTEGUI_LISTBOX_FLAGS_CLICKABLE | CMDIF_REMOTEGUI_LISTBOX_FLAGS_ALIGN_CENTER);
                    break;
                case Change_Axle_ratio:
                    cmdif_remotegui_listboxbox_additem("Change Axle ratio", "", CMDIF_REMOTEGUI_LISTBOX_FLAGS_CLICKABLE | CMDIF_REMOTEGUI_LISTBOX_FLAGS_ALIGN_CENTER);
                    break;
                case Set_new_parameters:
                    cmdif_remotegui_listboxbox_additem("Set New parameters", "", CMDIF_REMOTEGUI_LISTBOX_FLAGS_CLICKABLE | CMDIF_REMOTEGUI_LISTBOX_FLAGS_ALIGN_CENTER);
                    break;
                case Return_stock_parameters:
                    cmdif_remotegui_listboxbox_additem("Return Stock parameters", "", CMDIF_REMOTEGUI_LISTBOX_FLAGS_CLICKABLE | CMDIF_REMOTEGUI_LISTBOX_FLAGS_ALIGN_CENTER);
                    break;
                default:
                    break;
                }
            }
            // If no functions are available (list is empty)
            if (i == 0)
            {
                cmdif_remotegui_listboxbox_cleanup();
                cmdif_remotegui_messagebox(cmd,
                                           title, CMDIF_REMOTEGUI_ICON_NONE,
                                           CMDIF_REMOTEGUI_BUTTONFACE_Exit, CMDIF_REMOTEGUI_BUTTONFACE_None, CMDIF_REMOTEGUI_BUTTONFACE_None,
                                           CMDIF_REMOTEGUI_FOCUS_None,
                                           "No special functions are available for this vehicle.");
            }
            // Show listbox if functions are available
            else
            {
                cmdif_remotegui_listbox(cmd, 0, 0,
                                        CMDIF_REMOTEGUI_BUTTONFACE_Exit, CMDIF_REMOTEGUI_BUTTONFACE_None, CMDIF_REMOTEGUI_BUTTONFACE_None,
                                        CMDIF_REMOTEGUI_FOCUS_ListBox);
            }

            cmdif_remotegui_listboxbox_cleanup();
            cmdif_remotegui_wait_user_response(&responsetype, &responsechoice, NULL);

            // Exit button selected
            if (responsetype == CMDIF_REMOTEGUI_RESPONSETYPE_BUTTON)
            {
                return;
            }
            // Menu item selected
            else if (responsetype == CMDIF_REMOTEGUI_RESPONSETYPE_LIST)
            {
                switch (functions[responsechoice])
                {
                case Change_Tire_size:
                    memset(print_buffer, 0, sizeof(print_buffer));
                    sprintf((void*)print_buffer,"Current Tire setting: %.2f\nEnter the new tire size in inches.",tire.flval);
                    cmdif_remotegui_messagebox(cmd,
                                               title, CMDIF_REMOTEGUI_ICON_NONE,
                                               CMDIF_REMOTEGUI_BUTTONFACE_Exit, CMDIF_REMOTEGUI_BUTTONFACE_Continue, CMDIF_REMOTEGUI_BUTTONFACE_None,
                                               CMDIF_REMOTEGUI_FOCUS_Button1,
                                               print_buffer);
                    cmdif_remotegui_wait_user_response(&responsetype, &responsechoice, NULL);

                    // Exit button selected
                    if (responsechoice == CMDIF_REMOTEGUI_BUTTONTYPE_LEFT)
                    {
                        return;
                    }

                    //unit: "in"
                    cmdif_remotegui_numpad(cmd,"TIRE SIZE","in",CMDIF_REMOTEGUI_NUMPAD_INPUTTYPE_FLOAT,
                                           0,22.87,45.13);  //initial value: 0, range: 22.87-45.13
                    status = cmdif_remotegui_wait_user_response_numpad(&userinput_fval);

                    if (status == S_USERABORT)
                    {
                        //user cancels numpad
                        return;
                    }
                    else if (status == S_SUCCESS)
                    {
                        tire_changed = 1;
                        cmdif_remotegui_messagebox(cmd,
                                                   title, CMDIF_REMOTEGUI_ICON_NONE,
                                                   CMDIF_REMOTEGUI_BUTTONFACE_None, CMDIF_REMOTEGUI_BUTTONFACE_None, CMDIF_REMOTEGUI_BUTTONFACE_None,
                                                   CMDIF_REMOTEGUI_FOCUS_None,
                                                   "<H>TIRE SIZE & AXLE RATIO\nProcessing...");

                        //userinput_fval is the value from user input, use it and finish the rest
                        tire.flval = 0;
                        tire.flval = userinput_fval;
                        tire.flval = tire.flval/conversion;
                        tire.unsint = (u32)tire.flval;
                    }
                    break;
                case Change_Axle_ratio:

                    cmdif_remotegui_messagebox(cmd,
                                               title, CMDIF_REMOTEGUI_ICON_NONE,
                                               CMDIF_REMOTEGUI_BUTTONFACE_Exit, CMDIF_REMOTEGUI_BUTTONFACE_Continue, CMDIF_REMOTEGUI_BUTTONFACE_None,
                                               CMDIF_REMOTEGUI_FOCUS_Button1,
                                               "WARNING: Do not change axle ratio unless you have already physically changed the gears!");
                    cmdif_remotegui_wait_user_response(&responsetype, &responsechoice, NULL);

                    // Exit button selected
                    if (responsechoice == CMDIF_REMOTEGUI_BUTTONTYPE_LEFT)
                    {
                        return;
                    }

                    memset(print_buffer, 0, sizeof(print_buffer));
                    sprintf((void*)print_buffer,"Current axle ratio setting: %.2f\nEnter the new axle ratio.",axle.flval);
                    cmdif_remotegui_messagebox(cmd,
                                               title, CMDIF_REMOTEGUI_ICON_NONE,
                                               CMDIF_REMOTEGUI_BUTTONFACE_Exit, CMDIF_REMOTEGUI_BUTTONFACE_Continue, CMDIF_REMOTEGUI_BUTTONFACE_None,
                                               CMDIF_REMOTEGUI_FOCUS_Button1,
                                               print_buffer);
                    cmdif_remotegui_wait_user_response(&responsetype, &responsechoice, NULL);

                    // Exit button selected
                    if (responsechoice == CMDIF_REMOTEGUI_BUTTONTYPE_LEFT)
                    {
                        return;
                    }

                    //unit: "ratio"
                    cmdif_remotegui_numpad(cmd,"AXLE RATIO","ratio",CMDIF_REMOTEGUI_NUMPAD_INPUTTYPE_FLOAT,
                                           0,1.00,8.00);  //initial value: 0, range: 1.00-8.00
                    status = cmdif_remotegui_wait_user_response_numpad(&userinput_fval);

                    if (status == S_USERABORT)
                    {
                        //user cancels numpad
                        return;
                    }
                    else if (status == S_SUCCESS)
                    {
                        axle_changed = 1;
                        cmdif_remotegui_messagebox(cmd,
                                                   title, CMDIF_REMOTEGUI_ICON_NONE,
                                                   CMDIF_REMOTEGUI_BUTTONFACE_None, CMDIF_REMOTEGUI_BUTTONFACE_None, CMDIF_REMOTEGUI_BUTTONFACE_None,
                                                   CMDIF_REMOTEGUI_FOCUS_None,
                                                   "<H>TIRE SIZE & AXLE RATIO\nProcessing...");

                        //userinput_fval is the value from user input, use it and finish the rest

                        axle.flval = 0;
                        axle.unsint = (u32)(userinput_fval* 1000);

                    }
                    break;
                case Set_new_parameters:

                    if(obd2_checkkeyon(CommType_Unknown, TRUE) == S_NOCOMMUNICATION)
                    {
                        cmdif_remotegui_msg_keyon(CMDIF_CMD_SPECIAL_FUNCTIONS, title);
                    }

                    cmdif_remotegui_messagebox(cmd,
                                               title, CMDIF_REMOTEGUI_ICON_NONE,
                                               CMDIF_REMOTEGUI_BUTTONFACE_None, CMDIF_REMOTEGUI_BUTTONFACE_None, CMDIF_REMOTEGUI_BUTTONFACE_None,
                                               CMDIF_REMOTEGUI_FOCUS_None,
                                               "<H>TIRE SIZE & AXLE RATIO\nProcessing...");
                    //save stock values before changing values
                    if (SETTINGS_TUNE(stockwheels.tire) == 0)
                    {
                        SETTINGS_TUNE(stockwheels.tire) = stock_tire_value;
                        stock_settings = 1;
                    }
                    if (SETTINGS_TUNE(stockwheels.axel) == 0)
                    {
                        SETTINGS_TUNE(stockwheels.axel) = stock_axle_value;
                        stock_settings = 1;
                    }
                    if (stock_settings == 1)
                    {
                        SETTINGS_SetTuneAreaDirty();
                        settings_update(SETTINGS_FORCE_UPDATE);
                    }
                    
                    if(axle_changed == 1)
                    {
                        data_buffer[1] = axle.hexval[0];
                        data_buffer[0] = axle.hexval[1];
                    }
                    if(tire_changed == 1)
                    {
                        data_buffer[7] = tire.hexval[0];
                        data_buffer[6] = tire.hexval[1];
                    }

                    if((axle_changed == 0)&&(tire_changed == 0))
                    {
                        cmdif_remotegui_messagebox(cmd,
                                                   title, CMDIF_REMOTEGUI_ICON_NONE,
                                                   CMDIF_REMOTEGUI_BUTTONFACE_Exit, CMDIF_REMOTEGUI_BUTTONFACE_None, CMDIF_REMOTEGUI_BUTTONFACE_None,
                                                   CMDIF_REMOTEGUI_FOCUS_None,
                                                   "No changes were made to parameters.");
                        cmdif_remotegui_wait_user_response(&responsetype, &responsechoice, NULL);
                    }
                    else
                    {
                        status = obd2can_dcx_write_tire_axel_parameters(cmd, title, WA580, data_buffer, diag_type);
                    }

                    if(status == S_SUCCESS)
                    {
                        cmdif_remotegui_messagebox(cmd,
                                                   title, CMDIF_REMOTEGUI_ICON_NONE,
                                                   CMDIF_REMOTEGUI_BUTTONFACE_Exit, CMDIF_REMOTEGUI_BUTTONFACE_None, CMDIF_REMOTEGUI_BUTTONFACE_None,
                                                   CMDIF_REMOTEGUI_FOCUS_None,
                                                   "<H>TIRE SIZE & AXLE RATIO\nChange successful!");
                    }
                    else
                    {
                        cmdif_remotegui_messagebox(cmd,
                                                   title, CMDIF_REMOTEGUI_ICON_NONE,
                                                   CMDIF_REMOTEGUI_BUTTONFACE_Exit, CMDIF_REMOTEGUI_BUTTONFACE_None, CMDIF_REMOTEGUI_BUTTONFACE_None,
                                                   CMDIF_REMOTEGUI_FOCUS_None,
                                                   "<H>TIRE SIZE & AXLE RATIO\nChange failed!");
                    }
                    cmdif_remotegui_wait_user_response(&responsetype, &responsechoice, NULL);

                    break;

                case Return_stock_parameters:

                    if(obd2_checkkeyon(CommType_Unknown, TRUE) == S_NOCOMMUNICATION)
                    {
                        cmdif_remotegui_msg_keyon(CMDIF_CMD_SPECIAL_FUNCTIONS, title);
                    }

                    cmdif_remotegui_messagebox(cmd,
                                               title, CMDIF_REMOTEGUI_ICON_NONE,
                                               CMDIF_REMOTEGUI_BUTTONFACE_None, CMDIF_REMOTEGUI_BUTTONFACE_None, CMDIF_REMOTEGUI_BUTTONFACE_None,
                                               CMDIF_REMOTEGUI_FOCUS_None,
                                               "<H>TIRE SIZE & AXLE RATIO\nProcessing...");

                    temp_number = SETTINGS_TUNE(stockwheels.axel);
                    if(temp_number != 0)
                    {
                        data_buffer[1] = temp_number;
                        data_buffer[0] = (temp_number>>8);
                        settings_blank = 0;
                    }

                    temp_number = SETTINGS_TUNE(stockwheels.tire);
                    if(temp_number != 0)
                    {
                        data_buffer[7] = temp_number;
                        data_buffer[6] = (temp_number>>8);
                        settings_blank = 0;
                    }
                    if(settings_blank == 0)
                        status = obd2can_dcx_write_tire_axel_parameters(cmd, title, WA580, data_buffer, diag_type);
                    if(status == S_SUCCESS)
                    {
                        SETTINGS_TUNE(stockwheels.tire) = 0;
                        SETTINGS_TUNE(stockwheels.axel) = 0;
                        SETTINGS_SetTuneAreaDirty();
                        settings_update(SETTINGS_FORCE_UPDATE);

                        tire.flval = 0;                           // set new parameters
                        tire.hexval[0] = data_buffer[7];
                        tire.hexval[1] = data_buffer[6];
                        tire.flval = tire.unsint * conversion;

                        axle.flval = 0;
                        axle.hexval[1] = data_buffer[0];
                        axle.hexval[0] = data_buffer[1];
                        axle.flval = (float)axle.unsint/1000;

                        cmdif_remotegui_messagebox(cmd,
                                                   title, CMDIF_REMOTEGUI_ICON_NONE,
                                                   CMDIF_REMOTEGUI_BUTTONFACE_Exit, CMDIF_REMOTEGUI_BUTTONFACE_None, CMDIF_REMOTEGUI_BUTTONFACE_None,
                                                   CMDIF_REMOTEGUI_FOCUS_None,
                                                   "<H>TIRE SIZE & AXLE RATIO\nSettings back to stock, successful!");
                    }
                    else
                    {
                        cmdif_remotegui_messagebox(cmd,
                                                   title, CMDIF_REMOTEGUI_ICON_NONE,
                                                   CMDIF_REMOTEGUI_BUTTONFACE_Exit, CMDIF_REMOTEGUI_BUTTONFACE_None, CMDIF_REMOTEGUI_BUTTONFACE_None,
                                                   CMDIF_REMOTEGUI_FOCUS_None,
                                                   "<H>TIRE SIZE & AXLE RATIO\nSettings back to stock, failed!");
                    }
                    cmdif_remotegui_wait_user_response(&responsetype, &responsechoice, NULL);

                    break;

                default:
                    break;
                }
            }
        } // if(...)
        else
        {
            break;
        }
    } // while(1)
    return;
}
void obd2_dcx_ETC_relearn(VehicleCommType commtype, VehicleCommLevel commlevel)
{
    CMDIF_REMOTEGUI_RESPONSETYPE responsetype;
    CMDIF_COMMAND cmd = CMDIF_CMD_SPECIAL_FUNCTIONS;
    u16 responsechoice;
    u8 status;

    enum {
            ETC_relearn_Continue = CMDIF_REMOTEGUI_BUTTONTYPE_RIGHT,
            ETC_relearn_Cancel   = CMDIF_REMOTEGUI_BUTTONTYPE_LEFT,
         };

    if (commtype == CommType_CAN)
    {
        cmdif_remotegui_messagebox(cmd,
                                   "SPECIAL FUNCTIONS", CMDIF_REMOTEGUI_ICON_NONE,
                                   CMDIF_REMOTEGUI_BUTTONFACE_Cancel, CMDIF_REMOTEGUI_BUTTONFACE_Continue, CMDIF_REMOTEGUI_BUTTONFACE_None,
                                   CMDIF_REMOTEGUI_FOCUS_Button1,
                                   "<HEADER>ETC RELEARN</HEADER>\nRelearn Electronic Throttle Control.");
        cmdif_remotegui_wait_user_response(&responsetype, &responsechoice, NULL);

        switch (responsechoice)
        {
        case ETC_relearn_Continue:
            cmdif_remotegui_messagebox(cmd,
                                       "SPECIAL FUNCTIONS", CMDIF_REMOTEGUI_ICON_NONE,
                                       CMDIF_REMOTEGUI_BUTTONFACE_None, CMDIF_REMOTEGUI_BUTTONFACE_None, CMDIF_REMOTEGUI_BUTTONFACE_None,
                                       CMDIF_REMOTEGUI_FOCUS_None,
                                       "<HEADER>ETC RELEARN</HEADER>\nProcessing...");
            status = obd2can_dcx_ETC_relearn();
            break;
        case ETC_relearn_Cancel:
            status = S_USERABORT;
            break;
        }
    }
    else
    {
        status = S_NOTSUPPORT;
    }
    if (status == S_SUCCESS)
    {
        cmdif_remotegui_messagebox(cmd,
                                   "SPECIAL FUNCTIONS", CMDIF_REMOTEGUI_ICON_NONE,
                                   CMDIF_REMOTEGUI_BUTTONFACE_Exit, CMDIF_REMOTEGUI_BUTTONFACE_None, CMDIF_REMOTEGUI_BUTTONFACE_None,
                                   CMDIF_REMOTEGUI_FOCUS_None,
                                   "<HEADER>ETC RELEARN</HEADER>\n ETC relearn Complete.");
    }
    else if (status == S_NOTSUPPORT)
    {
        cmdif_remotegui_messagebox(cmd,
                                   "SPECIAL FUNCTIONS", CMDIF_REMOTEGUI_ICON_NONE,
                                   CMDIF_REMOTEGUI_BUTTONFACE_Exit, CMDIF_REMOTEGUI_BUTTONFACE_None, CMDIF_REMOTEGUI_BUTTONFACE_None,
                                   CMDIF_REMOTEGUI_FOCUS_None,
                                   "<HEADER>ETC RELEARN</HEADER>\nERROR: This vehicle does not support an ETC relearn.");
    }
    else if (status == S_USERABORT)
    {
        return;
    }
    else
    {
        cmdif_remotegui_messagebox(cmd,
                                   "SPECIAL FUNCTIONS", CMDIF_REMOTEGUI_ICON_NONE,
                                   CMDIF_REMOTEGUI_BUTTONFACE_Exit, CMDIF_REMOTEGUI_BUTTONFACE_None, CMDIF_REMOTEGUI_BUTTONFACE_None,
                                   CMDIF_REMOTEGUI_FOCUS_None,
                                   "<HEADER>ETC RELEARN</HEADER>\nERROR: ETC Relearn Failed.");
    }
    cmdif_remotegui_wait_user_response(&responsetype, &responsechoice, NULL);
}


/**
 * @brief   ECU Reset
 *
 * @param   [in] commtype
 * @param   [in] commlevel
 *
 * @author  Tristen Pierson & Rhonda Rusak
 */
void obd2_dcx_power_on_reset(VehicleCommType commtype, VehicleCommLevel commlevel)
{
    CMDIF_COMMAND cmd = CMDIF_CMD_SPECIAL_FUNCTIONS;
    CMDIF_REMOTEGUI_RESPONSETYPE responsetype;
    u16 responsechoice;
    u8 status;

    enum {
        PowerOnReset_Continue = CMDIF_REMOTEGUI_BUTTONTYPE_RIGHT,
        PowerOnReset_Cancel   = CMDIF_REMOTEGUI_BUTTONTYPE_LEFT,
    };

    if (commtype == CommType_CAN)
    {
        cmdif_remotegui_messagebox(cmd,
                                   "SPECIAL FUNCTIONS", CMDIF_REMOTEGUI_ICON_NONE,
                                   CMDIF_REMOTEGUI_BUTTONFACE_Cancel, CMDIF_REMOTEGUI_BUTTONFACE_Continue, CMDIF_REMOTEGUI_BUTTONFACE_None,
                                   CMDIF_REMOTEGUI_FOCUS_Button1,
                                   "<HEADER>POWER ON RESET</HEADER>\nPower on reset performs a PCM off/on cycle.");
        cmdif_remotegui_wait_user_response(&responsetype, &responsechoice, NULL);

        switch (responsechoice)
        {
        case PowerOnReset_Continue:
            cmdif_remotegui_messagebox(cmd,
                                       "SPECIAL FUNCTIONS", CMDIF_REMOTEGUI_ICON_NONE,
                                       CMDIF_REMOTEGUI_BUTTONFACE_None, CMDIF_REMOTEGUI_BUTTONFACE_None, CMDIF_REMOTEGUI_BUTTONFACE_None,
                                       CMDIF_REMOTEGUI_FOCUS_None,
                                       "<HEADER>POWER ON RESET</HEADER>\nProcessing...");
            status = obd2can_dcx_power_on_reset(Obd2CanEcuId_7E0, commlevel);            

            break;
        case PowerOnReset_Cancel:
            status = S_USERABORT;
            break;
        }
    }
    else
    {
        status = S_NOTSUPPORT;
    }

    if (status == S_SUCCESS)
    {
        cmdif_remotegui_messagebox(cmd,
                                   "SPECIAL FUNCTIONS", CMDIF_REMOTEGUI_ICON_NONE,
                                   CMDIF_REMOTEGUI_BUTTONFACE_Exit, CMDIF_REMOTEGUI_BUTTONFACE_None, CMDIF_REMOTEGUI_BUTTONFACE_None,
                                   CMDIF_REMOTEGUI_FOCUS_None,
                                   "<HEADER>POWER ON RESET</HEADER>\n Reset Complete.");
    }
    else if (status == S_NOTSUPPORT)
    {
        cmdif_remotegui_messagebox(cmd,
                                   "SPECIAL FUNCTIONS", CMDIF_REMOTEGUI_ICON_NONE,
                                   CMDIF_REMOTEGUI_BUTTONFACE_Exit, CMDIF_REMOTEGUI_BUTTONFACE_None, CMDIF_REMOTEGUI_BUTTONFACE_None,
                                   CMDIF_REMOTEGUI_FOCUS_None,
                                   "<HEADER>RESET MEMORY</HEADER>\nERROR: This vehicle does not support an ECU reset.");
    }
    else if (status == S_USERABORT)
    {
        return;
    }
    else
    {
        cmdif_remotegui_messagebox(cmd,
                                   "SPECIAL FUNCTIONS", CMDIF_REMOTEGUI_ICON_NONE,
                                   CMDIF_REMOTEGUI_BUTTONFACE_Exit, CMDIF_REMOTEGUI_BUTTONFACE_None, CMDIF_REMOTEGUI_BUTTONFACE_None,
                                   CMDIF_REMOTEGUI_FOCUS_None,
                                   "<HEADER>RESET MEMORY</HEADER>\nERROR: ECU Reset Failed.");
    }
    cmdif_remotegui_wait_user_response(&responsetype, &responsechoice, NULL);
}


/**
 * @brief   Bootloader Info
 *
 * @param   [in] commtype
 * @param   [in] commlevel
 *
 * @author  Tristen Pierson
 */
#ifdef __DEBUG_JTAG_
void obd2_dcx_bootloader_info(VehicleCommType commtype, VehicleCommLevel commlevel)
{
    CMDIF_COMMAND cmd = CMDIF_CMD_SPECIAL_FUNCTIONS;
    CMDIF_REMOTEGUI_RESPONSETYPE responsetype;
    u16 responsechoice;
    u8 hashResult[0xFF] = "<HEADER>BOOTLOADER INFO</HEADER>\n";
    u8 status;

    enum {
        BootHashContinue = CMDIF_REMOTEGUI_BUTTONTYPE_RIGHT,
        BootHashCancel   = CMDIF_REMOTEGUI_BUTTONTYPE_LEFT,
    };

    if (commtype == CommType_CAN)
    {
        cmdif_remotegui_messagebox(cmd,
                "SPECIAL FUNCTIONS", CMDIF_REMOTEGUI_ICON_NONE,
                CMDIF_REMOTEGUI_BUTTONFACE_None, CMDIF_REMOTEGUI_BUTTONFACE_None, CMDIF_REMOTEGUI_BUTTONFACE_None,
                CMDIF_REMOTEGUI_FOCUS_None,
                "<HEADER>BOOTLOADER INFO</HEADER>\nProcessing...");

        obd2can_init(Obd2canInitNormal,0);
        obd2can_ping(Obd2CanEcuId_7E0, FALSE);
        status = obd2can_dcx_initiate_diagnostic_operation(FALSE,0x7E0, 0x7E8, diagnosticRequestChrysler_92);
        if (status == S_SUCCESS)
        {
            #if 0
            // This is a good place to write VIN if needed (only for debug)
            {
                u32 temp_cooldown_time = obd2_getcooldowntime();
                u8 vin[17] = "1C6RR6FT3DS584512";

                obd2_setcooldowntime(1000);
                obd2can_dcx_write_data_by_local_id(0x7E0, 0x90, vin, 17);
                obd2_setcooldowntime(temp_cooldown_time);
            }
            #endif

            status = obd2can_dcx_initiate_diagnostic_operation(FALSE,0x7E0, 0x7E8, diagnosticRequestChrysler_85);
        }
        if (status == S_SUCCESS)
        {
            status = obd2can_unlockecm(UnlockTask_Upload, 4, FALSE);
        }
        else
        {
            strcat((char*)hashResult, "Bootloader hash is not required");
        }
        if(status == S_SUCCESS)
        {
            u8  databuffer[21];
            u32 datalength;
            u8  hexstr[20 * 3 + 1] = "";
            u8  bootVersion[11] = "";
            u8  i;

            // Request bootloader version
            status = obd2can_dcx_request_byidentifier(0x9E, 0x7E0, 0x7E8, 0);
            if(status == S_SUCCESS)
            {
                memcpy(bootVersion, &obd2_rxbuffer[10], 10);
                strcat((char*)hashResult, "Bootloader Version: ");
                strcat((char*)hashResult, (char*)bootVersion);
                strcat((char*)hashResult, "\n");
            }

            // Request bootloader hash
            status = obd2can_dcx_start_routine_by_local_id_rx(0x7E0, 0xDF, databuffer, &datalength);
            if(status == S_SUCCESS && datalength == 21)
            {
                strcat((char*)hashResult, "Bootloader Hash:\n");
                memcpy(hexstr, &databuffer[1], 20);

                for (i = 0; i < 20; i++)
                {
                    char temp[3];
                    sprintf(temp, "%02X ", hexstr[i]);
                    strcat((char*)hashResult, temp);
                    if (i == 9)
                    {
                        strcat((char*)hashResult, "\n");
                    }
                }
            }
        }
    }// if CommType_CAN
    else
    {
        status = S_NOTSUPPORT;
    }

    if (status == S_SUCCESS)
    {
        cmdif_remotegui_messagebox(cmd,
                                   "SPECIAL FUNCTIONS", CMDIF_REMOTEGUI_ICON_NONE,
                                   CMDIF_REMOTEGUI_BUTTONFACE_Exit, CMDIF_REMOTEGUI_BUTTONFACE_None, CMDIF_REMOTEGUI_BUTTONFACE_None,
                                   CMDIF_REMOTEGUI_FOCUS_None,
                                   hashResult);
    }
    else if (status == S_USERABORT)
    {
        return;
    }
    else
    {
        cmdif_remotegui_messagebox(cmd,
                                   "SPECIAL FUNCTIONS", CMDIF_REMOTEGUI_ICON_NONE,
                                   CMDIF_REMOTEGUI_BUTTONFACE_Exit, CMDIF_REMOTEGUI_BUTTONFACE_None, CMDIF_REMOTEGUI_BUTTONFACE_None,
                                   CMDIF_REMOTEGUI_FOCUS_None,
                                   "<HEADER>BOOTLOADER INFO</HEADER>\nERROR: Failed to read bootloader information");
    }
    cmdif_remotegui_wait_user_response(&responsetype, &responsechoice, NULL);
}
#endif

//------------------------------------------------------------------------------
// Get vehicle part type description
// Inputs:  u8 part type, 
//          u8 part_type_des_buff 
// Return:  Nothing, void
// This function simply gets the vehicle's part type in text form (ex: NGC4, GPEC, etc.)
// Engineer: Ricardo Cigarroa
//------------------------------------------------------------------------------
void obd2_dcx_part_type_description(u8 part_type, u8 *part_type_des_buff)
{
    switch(part_type)
    {
      case None:
          strcpy((void*)part_type_des_buff, "JTEC");
          break;
      case NGC1:
          strcpy((void*)part_type_des_buff, "NGC1(non-ETC)");
          break;
      case NGC2:
          strcpy((void*)part_type_des_buff, "NGC2(ETC)");
          break;
      case NGC3_PCM:
          strcpy((void*)part_type_des_buff,"NGC3(non-ETC)\nPowertrain controller");
          break;
      case NGC3_ETC:
      case NGC3_XCORE:
          strcpy((void*)part_type_des_buff,"NGC3(ETC)\nPowertrain controller");
          break;
      case NGC1A:
          strcpy((void*)part_type_des_buff,"NGC1A(non-ETC)\nPowertrain controller with IID ignition");
          break;
      case NGC1B:
          strcpy((void*)part_type_des_buff,"NGC1B(non-ETC)\nPowertrain controller 2003 MY with IGBT");
          break;
      case NGC3L_ETC:
      case NGC3L_MP:              
          strcpy((void*)part_type_des_buff,"NGC3L(ETC)");
          break;
      case NGC3L:
          strcpy((void*)part_type_des_buff,"NGC3L(non-ETC)");
          break;
      case NGC3:
          strcpy((void*)part_type_des_buff,"NGC3(non-ETC)");
          break;
      case NGC4_PCM:
          strcpy((void*)part_type_des_buff,"NGC4 PCM");
          break;
      case NGC4_ECM:
          strcpy((void*)part_type_des_buff,"NGC4 ECM");     //2007-2008
          break;                                             
      case NGC4_TCM:                                            
          strcpy((void*)part_type_des_buff,"NGC4 TCM");             
          break;            
      case JTECP:   
          strcpy((void*)part_type_des_buff, "JTECP");
          break;
      case SBEC3A:
          strcpy((void*)part_type_des_buff, "SBECIIIA");
          break;
      case GPEC2_PCM:
          strcpy((void*)part_type_des_buff,"GPEC2 PCM");
          break;
      case GPEC2_ECM:
          strcpy((void*)part_type_des_buff,"GPEC2 ECM");    //2012-2014
          break;
      case GPEC2_TCM:
          strcpy((void*)part_type_des_buff,"GPEC2 TCM");
          break;
      case NGC4A_PCM:
          strcpy((void*)part_type_des_buff,"NGC4A PCM");
          break;
      case NGC4A_ECM:
          strcpy((void*)part_type_des_buff,"NGC4A ECM");    //2009-2012
          break;
      case NGC4A_TCM:
          strcpy((void*)part_type_des_buff,"NGC4A TCM");
          break;
      case SBEC3:
          strcpy((void*)part_type_des_buff, "SBECIII");
          break;
      case GPEC2A_ECM:
          strcpy((void*)part_type_des_buff,"GPEC2A ECM"); //2015 GPEC2A ECM
          break;
      case GPEC2A_PCM:
          strcpy((void*)part_type_des_buff,"GPEC2A PCM"); //GPEC2A 2015 boot - 05150997AA
          break;
      default:
          strcpy((void*)part_type_des_buff,"N/A");
          break;      
      };
}

//------------------------------------------------------------------------------
// Search for vehicle type using ECMs' part type
// Only use for 'Upload Stock' feature
// Return:  u8  status
// Engineer: Ricardo Cigarroa
//------------------------------------------------------------------------------
u8 obd2_dcx_vehicle_type_search(ecm_info *ecm)
{
    u16 ecm_type_list[5];  
    u8  ecm_type_list_count;
    u16 reference_ecm_type;
    u16 vehicle_type;
    u32 ecm_found;
    u8  ecm_found_count;
    u8  i,j;
    u8  variant_id; 
    u8  reference_part_type; 
    u8  status = S_SUCCESS; 

    ecm_found_count = 0;
    ecm_found = 0;
    ecm_type_list_count = 0;
    reference_part_type = 0;
    reference_ecm_type = 0;
        
    //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    // Find vehicle type based on the processor part type. 
    //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

    for (i=0;i<ecm->ecm_count;i++)
    {
        reference_part_type = ecm->second_codes[i][2][0];
        
        /*We use the part type to get an idea of which processor we are 
        working with. We still need to look at things such as the variant ID
        and the VIN number to identify the processor properly.*/
        switch (reference_part_type)
        {
        case NGC4_PCM:
        case NGC4_ECM:
        case NGC4_TCM:
            reference_ecm_type = 3008; 
            break;
        case GPEC2_PCM:
        case GPEC2_ECM:
        case GPEC2_TCM:
        case GPEC2A_ECM:
        case GPEC2A_PCM:
            status = obd2can_dcx_request_byidentifier(0x9E, 0x7E0+i, 0x7E8+i, 1);
            if (status == S_SUCCESS)
            {
                variant_id = obd2_rxbuffer[4];
                
                switch (variant_id)
                {
                case 0x23:
                    reference_ecm_type = 3004; //2011-2013 GPEC2
                    break;
                case 0x25: 
                    reference_ecm_type = 3012; //2014 GPEC2
                    break;
                case 0x26: 
                    reference_ecm_type = 3015; //2015 GPEC2
                    break;
/*                case 0x42: 
                    reference_ecm_type = 3013; //2014 GPEC2A
                    break;
*/
                case 0x44: 
                    reference_ecm_type = 3014; //2015 GPEC2A
                    break;
                default:
                    /*unsupported variant id*/
                    status = S_FAIL; 
                    break; 
                }
            }
            else
            {
                status = S_FAIL; 
            }
            break;
        case NGC4A_ECM:
        case NGC4A_TCM:
        case NGC4A_PCM:
            if (ecm->vin[9] <= 0x41)
            {
                reference_ecm_type = 3005; //2009-2010
            }
            else
            {
                reference_ecm_type = 3011; //2011-2012
            }
            break;
        default:
            /*unsupported vehicle type*/
            status = S_FAIL; 
            break;
        }
        
        if (status != S_SUCCESS)
        {
            status = S_FAIL; 
            goto obd2_dcx_vehicle_type_search_done; 
        }
        else
        {
            for(j=0;j<ecm_type_list_count;j++)
            {
                if (ecm_type_list[j] == reference_ecm_type)
                {
                    //already added to list
                    break;
                }
            }
            if (j >= ecm_type_list_count)
            {
                ecm_type_list[ecm_type_list_count++] = reference_ecm_type;
                ecm_found |= ((u32)1<<i);
                ecm_found_count++;
                break;
            }    
        }
    }

    //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    // Find vehicle def. 
    //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    status = vehdef_get_vehdef_info(ecm_type_list, ecm_type_list_count,&vehicle_type);
    if (status == S_SUCCESS)
    {
        if (ecm_found_count != ecm->ecm_count)
        {
            //at least one ecm is not in the supported list
            if (VEH_IsAllowSkipUnsupportedSubECM(vehicle_type))
            {
                flasherinfo->vehicle_type = vehicle_type;
                status = S_SUCCESS;
            }
            else
            {
                status = S_UNMATCH; 
            }
        }
        
        flasherinfo->vehicle_type = vehicle_type;
        status = S_SUCCESS;
    }
    else
    {
        flasherinfo->vehicle_type = INVALID_VEH_DEF;
        status = S_UNMATCH; 
    }
    
obd2_dcx_vehicle_type_search_done: 
    return status;
}
