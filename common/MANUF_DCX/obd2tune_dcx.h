/**
  **************** (C) COPYRIGHT 2011 SCT Performance, LLC *********************
  * File Name          : obd2tune_dcx.h
  * Device / System    : SCT products using the "CommonCode" platform
  * Author             : Quyen Leba
  *
  * Version            : 1 
  * Date               : 05/02/2011
  * Description        : 
  *                    : 
  *
  *
  * History            : 05/02/2011 M. Davis
  *                    :   Add SCT Copyright header,
  *
  ******************************************************************************
  */

#ifndef __OBD2TUNE_DCX_H
#define __OBD2TUNE_DCX_H

#include <arch/gentype.h>
#include <common/obd2tune.h>

//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//
//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
u8 obd2tune_dcx_check_customtune_support(u16 veh_type,  u8 *vin,
                                          u8 *preference_filename,
                                          ecm_info *ecminfo, u8 *matchmasked,
                                          bool *isvinmasked);
u8 obd2tune_dcx_compare_vehiclecodes_with_setting(flasher_info *flasherinfo);
u8 obd2tune_dcx_compare_vehiclecodes_with_tuneosdata(flasher_info *flasherinfo);

u8 obd2tune_dcx_compare_overlaydata(u16 veh_type);
u8 obd2tune_dcx_option_upload(flasher_info *f_info);
u8 obd2tune_dcx_option_download(flasher_info *f_info);
u8 obd2tune_dcx_check_variant_id(bool *variant_id);

//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
// These functions will override their default definitions from obd2tune.c
//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
#define OBD2TUNE_CHECK_CUSTOMTUNE_SUPPORT_DEFINED
#define obd2tune_check_customtune_support(veh_type,vin,preference_filename,ecminfo,matchmasked,isvinmasked) \
    obd2tune_dcx_check_customtune_support(veh_type,vin,preference_filename,ecminfo,matchmasked,isvinmasked)

#define OBD2TUNE_COMPARE_VEHICLE_CODES_WITH_SETTINGS_DEFINED
#define obd2tune_compare_vehiclecodes_with_setting(flasherinfo)\
        obd2tune_dcx_compare_vehiclecodes_with_setting(flasherinfo)

#define OBD2TUNE_COMPARE_VEHICLE_CODES_WITH_TUNEOSDATA_DEFINED
#define obd2tune_compare_vehiclecodes_with_tuneosdata(flasherinfo)\
        obd2tune_dcx_compare_vehiclecodes_with_tuneosdata(flasherinfo)

#define OBD2TUNE_COMPARE_OVERLAYDATA_DEFINED
#define obd2tune_compare_overlaydata(veh_type) \
    obd2tune_dcx_compare_overlaydata(veh_type)
      
#define OBD2TUNE_OPTION_UPLOAD_DEFINED
#define obd2tune_option_upload(f_info) \
    obd2tune_dcx_option_upload(f_info)

#define OBD2TUNE_OPTION_DOWNLOAD_DEFINED
#define obd2tune_option_download(f_info) \
    obd2tune_dcx_option_download(f_info)

#endif    //__OBD2TUNE_DCX_H
