/**
  **************** (C) COPYRIGHT 2011 SCT Performance, LLC *********************
  * File Name          : obd2can_dcx.c
  * Device / System    : SCT products using the "CommonCode" platform
  * Author             : Quyen Leba
  *
  * Version            : 1 
  * Date               : 05/02/2011
  * Description        : 
  *                    : 
  *
  *
  * History            : 05/02/2011 M. Davis
  *                    :   Add SCT Copyright header,
  *
  ******************************************************************************
  */

//------------------------------------------------------------------------------
// Log error point: 0x30xx -> 0x32xx
//------------------------------------------------------------------------------

#include <ctype.h>
#include <string.h>
#include <board/delays.h>
#include <fs/genfs.h>
#include <common/statuscode.h>
#include <common/crypto_blowfish.h>
#include <common/cmdif.h>
#include <common/obd2tune.h>
#include <common/filestock.h>
#include <common/file.h>
#include <common/housekeeping.h>
#include <common/obd2def.h>
#include <common/ubf.h>
#include <common/debug/debug_output.h>
#include <common/checksum_csf.h>
#include <common/ess_file.h>
#include <common/cmdif_remotegui.h>
#include "dcx_e2_defs.h"
#include "obd2can_dcx.h"

extern u8 obd2_rxbuffer[256];                       /* from obd2.c */
extern obd2_info gObd2info;                         /* from obd2.c */
extern cmdif_datainfo cmdif_datainfo_responsedata;  /* from cmdif.c */
extern flasher_info *flasherinfo;                   /* from obd2tune.c */
extern FileStock_Info *filestock_info;              /* from filestock.c */

const DCX_DL_HACK_DATA hack_data[] = 
{
    {
        {
            {0x4E,0x80,0x00,0x20},
            {0x7D,0x8C,0x53,0x78},           // varaiant ID 23 and 25
            {0x7C,0x0C,0x58,0x40},
            {0x41,0x82,0x00,0x08},
            {0x3B,0x9B,0x00,0x00}
        },
        {0x7C,0x08,0x40,0x40}
    },
};

//------------------------------------------------------------------------------
// Private functions
//------------------------------------------------------------------------------
u8 obd2can_dcx_read_strategy_bysearch(u32 ecm_id, u8 *strategy, VehicleCommLevel commlevel);
u8 obd2can_dcx_WA580_transType(void);
u8 obd2can_dcx_find_DiagType(void);
u8 obd2can_dcx_read_tire_axle_typeTIPMCGW(u8 *buffer);
u8 obd2can_dcx_read_tire_axle_typeBCM(u8 *buffer);
u8 obd2can_dcx_write_tire_axle_typeTIPMCGW(CMDIF_COMMAND cmd, const u8 *title, u8 WA580, u8 * data_buffer);
u8 obd2can_dcx_write_tire_axle_typeBCM(CMDIF_COMMAND cmd, const u8 *title, u8 WA580, u8 * data_buffer);
void obd2can_dcx_initializeEGS(CMDIF_COMMAND cmd, const u8 *title);
void obd2can_dcx_initializeABS(CMDIF_COMMAND cmd, const u8 *title);
u8 obd2can_dcx_ReadDataByParameterIdentifier(u32 ecm_id, u32 device_id, u8 subcommand, u8 service_command, 
                                             u8 *data, u16 *length);
u8 obd2can_dcx_WriteDataByParameterIdentifier(u32 ecm_id, u32 response_id, u8 identifier,
                                              u8 *data, u8 length);
u8 obd2can_dcx__chekcomputer(u32 ecm_id, u32 ecm_response_id);
u8 obdcan_dcx_sendcommand(u32 ecm_id, u32 response_id, u8 *tx_data, DCX_ServiceNumber service, 
                          Obd2can_SubServiceNumber subservice,
                            u8 length, bool broadcast);
u8 obd2can_dcx_clear_diagnostic_information_iso14229(bool broadcast, u32 ecm_id, u32 response_id);

//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
u8 obd2can_dcx_txrx(Obd2can_ServiceData *servicedata)
{
    obd2can_rxinfo rxinfo;
    u8 status;
    u32 response_id;
    

     if (servicedata->response_id == NULL)
       response_id = ECM_RESPONSE_ID(servicedata->ecm_id);
     else
       response_id = servicedata->response_id;

    if (servicedata->broadcast)
    {
//        //always single frame
//        //TODOQ: this
//        return S_NOTSUPPORT;
        //TODOQ: not fully supported; only catch responses with ecm_id for now
        status = obd2can_txcomplex(servicedata->broadcast_id,
                                   servicedata->response_id,
                                   servicedata->service,
                                   servicedata->subservice,
                                   servicedata->service_controldatabuffer,
                                   servicedata->service_controldatalength,
                                   servicedata->txdata,
                                   servicedata->txdatalength,
                                   servicedata->nowait_cantx);
    }
    else
    {

        status = obd2can_txcomplex(servicedata->ecm_id,
                                   servicedata->response_id,
                                   servicedata->service,
                                   servicedata->subservice,
                                   servicedata->service_controldatabuffer,
                                   servicedata->service_controldatalength,
                                   servicedata->txdata,
                                   servicedata->txdatalength,
                                   servicedata->nowait_cantx);
    }


    if (status == S_SUCCESS && servicedata->response_expected == TRUE)
    {
        obd2can_rxinfo_init(&rxinfo);

        rxinfo.rxbuffer = servicedata->rxdata;
        rxinfo.cmd = (u8)servicedata->service;
        rxinfo.first_frame_data_offset = servicedata->first_frame_data_offset;
        rxinfo.tester_present_type = servicedata->tester_present_type;
        rxinfo.initial_wait_extended = servicedata->initial_wait_extended;
        rxinfo.busy_wait_extended = servicedata->busy_wait_extended;
        rxinfo.commlevel = servicedata->commlevel;
        rxinfo.rx_timeout_ms = servicedata->rx_timeout_ms;
        rxinfo.flowcontrol_blocksize = servicedata->flowcontrol_blocksize;
        rxinfo.flowcontrol_separationTime = servicedata->flowcontrol_separationTime;
                
        status = obd2can_rx(response_id, &rxinfo);
        
        if (status == S_SUCCESS)
        {
            servicedata->rxdatalength = rxinfo.rxlength;
        }      
    }

    return status;
}

//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
u8 obd2can_dcx_txrxcomplex(Obd2can_ServiceData *servicedata)
{
    obd2can_rxinfo rxinfo;
    u8 status;
    u32 response_id;
    
    if (servicedata->broadcast)
    {
        //always single frame
        //TODOQ: this
        return S_NOTSUPPORT;
    }
    else
    {
        if (servicedata->response_id == NULL)
        {
            response_id = ECM_RESPONSE_ID(servicedata->ecm_id);
        }
        else
        {
            response_id = servicedata->response_id;
        }

        status = obd2can_txcomplex(servicedata->ecm_id,
                                   servicedata->response_id,
                                   servicedata->service,
                                   servicedata->subservice,
                                   servicedata->service_controldatabuffer,
                                   servicedata->service_controldatalength,
                                   servicedata->txdata,
                                   servicedata->txdatalength,
                                   servicedata->nowait_cantx);
    }
    
    if (status == S_SUCCESS && servicedata->response_expected == TRUE)
    {
        obd2can_rxinfo_init(&rxinfo);

        rxinfo.rxbuffer = servicedata->rxdata;
        rxinfo.cmd = (u8)servicedata->service;
        rxinfo.first_frame_data_offset = servicedata->first_frame_data_offset;
        rxinfo.tester_present_type = servicedata->tester_present_type;
        rxinfo.initial_wait_extended = servicedata->initial_wait_extended;
        rxinfo.busy_wait_extended = servicedata->busy_wait_extended;
        rxinfo.commlevel = servicedata->commlevel;
        rxinfo.rx_timeout_ms = servicedata->rx_timeout_ms;
        rxinfo.flowcontrol_blocksize = servicedata->flowcontrol_blocksize;
        rxinfo.flowcontrol_separationTime = servicedata->flowcontrol_separationTime;
        rxinfo.secondary_response_ecm_id = servicedata->ecm_id;
        status = obd2can_rxcomplex(response_id, &rxinfo);
        if (status == S_SUCCESS)
        {
            servicedata->rxdatalength = rxinfo.rxlength;
        }
    }

    return status;
}

//------------------------------------------------------------------------------
// Send Tester Present $3E, DCX processors
// Input:   u8 type
// Output:  None
// Engineer: Ricardo Cigarroa
//------------------------------------------------------------------------------
u8 obd2can_dcx_testerpresent(bool broadcast, u32 ecm_id,
                               VehicleCommLevel commlevel)
{
    u8 status;
  
    Obd2can_ServiceData servicedata;
    obd2can_servicedata_init(&servicedata);
   
    servicedata.ecm_id = ecm_id;
    servicedata.service = 0x3E;
    
    if (broadcast == TRUE)
    {
        servicedata.subservice = 0x02;
        servicedata.response_expected = FALSE; 
    }
    else
    {
        servicedata.subservice = 0x01;
        servicedata.response_expected = TRUE; 
    }
    
    status = obd2can_txrx(&servicedata);
    if (status != S_SUCCESS)
    {
        servicedata.ecm_id = 0x7DF;
        obd2can_txrx(&servicedata);
    }
    
    delays(30,'m');     //this delay is important
    
    return status; 
}

//------------------------------------------------------------------------------
// Enter utility file $31
// Input:   u8 ecm_id
// Output:  None
// Return:  Nothing (void)
// Engineer: Ricardo Cigarroa
//------------------------------------------------------------------------------
u8 obd2can_dcx_enter_utility(u32 ecm_id)
{
    u8 status;
  
    Obd2can_ServiceData servicedata;

    obd2can_servicedata_init(&servicedata);
    servicedata.ecm_id = ecm_id;
    servicedata.service = DCX_StartRoutineByLocalId;
    servicedata.service_controldatabuffer[0] = 0xE1;
    servicedata.service_controldatabuffer[1] = 0x01;
    servicedata.service_controldatalength = 2;
    status = obd2can_txrx(&servicedata);
    
    if (status == S_SUCCESS)
    {
        servicedata.service_controldatabuffer[0] = 0x19;
        servicedata.service_controldatalength = 1;
        status = obd2can_txrx(&servicedata);    
    }
    
    return status; 
}
//------------------------------------------------------------------------------
// Read DTC info
// Input:   u8 ecm_id
// Output:  DTC data
// Return:  S_SUCCCESS/S_FAIL
// Engineer: Rhonda Rusak
//------------------------------------------------------------------------------
u8 obd2can_dcx_read_DTC_info(u32 ecm_id,u32 ecm_response_id, u8 first_byte_ID, u8 sencond_byte_ID, u8 *data)
{
    u8 status;
  
    Obd2can_ServiceData servicedata;

    obd2can_servicedata_init(&servicedata);
    servicedata.ecm_id = ecm_id;
    servicedata.response_id = ecm_response_id;
    servicedata.service = DCX_ReadDTCInfoByID;
    servicedata.service_controldatabuffer[0] = first_byte_ID;
    servicedata.service_controldatabuffer[1] = sencond_byte_ID;
    servicedata.service_controldatalength = 2;
    status = obd2can_txrx(&servicedata);

    memcpy((void*)data,(void*)servicedata.rxdata,servicedata.rxdatalength);

    
    return status; 
}
//------------------------------------------------------------------------------
// ClearDiagnosticInformation ($04)
// Inputs:  bool broadcast
//          u32 ecm_id
// Return:  u8  status
// Engineer: Ricardo Cigarroa
//------------------------------------------------------------------------------
u8 obd2can_dcx_clear_diagnostic_information(u32 ecm_id)
{
    Obd2can_ServiceData servicedata;
    u8 status;

    obd2can_servicedata_init(&servicedata);
    servicedata.ecm_id = 0x7DF;
    
    servicedata.service = DCX_ClearDiagnosticInformation;
    servicedata.service_controldatabuffer[0] = 0x99;
    servicedata.service_controldatabuffer[1] = 0x99;
    servicedata.service_controldatabuffer[2] = 0x99;
    servicedata.service_controldatabuffer[3] = 0x99;
    servicedata.service_controldatabuffer[4] = 0x99;
    servicedata.service_controldatabuffer[5] = 0x99;
    servicedata.service_controldatalength = 1; 
    servicedata.initial_wait_extended = 0;     // 0sec
    servicedata.busy_wait_extended = 3;        //default 30sec
    servicedata.rx_timeout_ms = 2000;          // 2sec
    
    obd2can_txrx(&servicedata);
    
    servicedata.ecm_id = ecm_id;
    
    status = obd2can_txrx(&servicedata);
    if (status == S_ERROR)
    {
        status = S_SUCCESS;
    }
    
    return status;
}

//------------------------------------------------------------------------------
// ClearDiagnosticInformationExtended ($14)
// Inputs:  bool broadcast
//          u32 ecm_id
// Return:  u8  status
// Engineer: Ricardo Cigarroa
//------------------------------------------------------------------------------
u8 obd2can_dcx_clear_diagnostic_information_extended(u32 ecm_id, u32 response_id)
{
    Obd2can_ServiceData servicedata;
    u8 status;

    obd2can_servicedata_init(&servicedata);
    servicedata.ecm_id = ecm_id;
    servicedata.response_id = response_id;
    servicedata.service = DCX_ClearDiagnosticInfomationExt;
    
    servicedata.service_controldatabuffer[0] = 0xFF;
    servicedata.service_controldatabuffer[1] = 0x00;
    servicedata.service_controldatalength = 2; 
    servicedata.initial_wait_extended = 0;     // 0sec
    servicedata.busy_wait_extended = 3;        //default 30sec
    servicedata.rx_timeout_ms = 2000;          // 2sec
    
    status = obd2can_txrx(&servicedata);
    
    return status;
}

//------------------------------------------------------------------------------
// ClearDiagnosticInformationISO14229 ($14)
// Inputs:  bool broadcast
//          u32 ecm_id
//          u32 response_id
// Return:  u8  status
// Engineer: Quyen Leba
//------------------------------------------------------------------------------
u8 obd2can_dcx_clear_diagnostic_information_iso14229(bool broadcast, u32 ecm_id, u32 response_id)
{
    Obd2can_ServiceData servicedata;
    u8 status;

    obd2can_servicedata_init(&servicedata);
    servicedata.ecm_id = ecm_id;
    servicedata.response_id = response_id;
    servicedata.broadcast_id = 0x7DF;
    servicedata.broadcast = broadcast;
    servicedata.service = DCX_ClearDiagnosticInfomationExt;
    servicedata.service_controldatabuffer[0] = 0xFF;
    servicedata.service_controldatabuffer[1] = 0xFF;
    servicedata.service_controldatabuffer[2] = 0xFF;
    servicedata.service_controldatalength = 3;
    servicedata.initial_wait_extended = 0;     // 0sec
    servicedata.busy_wait_extended = 3;        //default 30sec
    servicedata.rx_timeout_ms = 2000;          // 2sec

    status = obd2can_txrx(&servicedata);
    
    return status;
}
u8 obd2can_dcx_clear_diagnostic_information_all_modules(void)
{
    u8 status = S_FAIL, type = 0;
    u8 overallstatus = S_FAIL,i=0;
    u32 modules[9]={0x7E0,0x7E1,0x784,0x622,0x600,0x7F8,0x620,0x747,0x740};
    u32 response[9]={0x7E8,0x7E9,0x785,0x484,0x500,0x53F,0x504,0x4C7,0x4C0};

    obd2can_dcx_clear_diagnostic_information_iso14229(TRUE, modules[i], response[i]);

    for(i=0; i< 9; i++)
    {
        status = obd2can_dcx_clear_diagnostic_information_iso14229(FALSE, modules[i], response[i]);
        if((status == S_SUCCESS)&(i == 0))
            overallstatus = S_SUCCESS;
    
        status = obd2can_dcx_clear_diagnostic_information(modules[i]);
        if((status == S_SUCCESS)&(i == 0))
            overallstatus = S_SUCCESS;



        if(i == 6)
        {
            type = obd2can_dcx_find_DiagType();

            if(type == 1)
              obd2can_dcx_initiate_diagnostic_operation(FALSE,modules[i], response[i], diagnosticRequestChrysler_92);
            else if (type == 2)
              obd2can_dcx_initiate_diagnostic_operation(FALSE,modules[i], response[i], diagnosticRequestChrysler_03);
        }


        status = obd2can_dcx_clear_diagnostic_information_extended(modules[i], response[i]);
        if((status == S_SUCCESS)&(i == 0))
            overallstatus = S_SUCCESS;

        cmdif_internalresponse_ack(CMDIF_CMD_DO_DOWNLOAD,
                               CMDIF_ACK_WAIT,NULL,0);     //just to reset app timeout


    }

    return overallstatus;
}
//------------------------------------------------------------------------------
// ReadVehicleInformation ($09)
// Inputs:  u32 ecm_id
//          u8  infotype - Type of data to be requested (ie. VIN, CalID, etc)
// Outputs: u8  *data
//          u16 *datalength
// Return:  u8  status
// Engineer: Ricardo Cigarroa
//------------------------------------------------------------------------------
u8 obd2can_dcx_request_vehicle_information(u32 ecm_id, u8 infotype,
                                            u8 *data, u16 *datalength)
{
    Obd2can_ServiceData servicedata;
    u8  buffer[100];
    u8  length;
    u8  i;
    u8  status;

    obd2can_servicedata_init(&servicedata);
    servicedata.ecm_id = ecm_id;
    servicedata.service = DCX_RequestVehicleInformation;
    servicedata.subservice = infotype;
    status = obd2can_txrx(&servicedata);
    
    //expected reponse: [1:infotype][1:messagecount][n:messagedata]
    if (status == S_SUCCESS && servicedata.rxdatalength > 2 &&
        servicedata.rxdatalength <= sizeof(buffer) &&
        servicedata.rxdata[0] == infotype)
    {
        //ECM and TCM part number are 10 char long. 
        length = servicedata.rxdatalength-8;
        memcpy(buffer,(char*)&servicedata.rxdata[2],length);
        for(i=0;i<length;i++)
        {
            if (isgraph(buffer[i]))
            {
                break;
            }
        }
        length -= i;
        memcpy(data,(char*)&buffer[i],length);
        data[length] = NULL;
        *datalength = length;
    }
    else
    {
        //attempt to get OS part number using command 1A, identifier 87
        servicedata.service = DCX_ReadDataByIdentifier;
        servicedata.subservice = 0x87;
        status = obd2can_txrx(&servicedata);
        if (status == S_SUCCESS)
        {
            length = servicedata.rxdatalength-11;
            memcpy(buffer,(char*)&servicedata.rxdata[11],length);
            for(i=0;i<length;i++)
            {
                if (isgraph(buffer[i]))
                {
                    break;
                }
            }
            length -= i;
            memcpy(data,(char*)&buffer[i],length);
            data[length] = NULL;
            *datalength = length;
        }
        else
        {
            data[0] = NULL;
            *datalength = 0;
        }
    }
    
    return status;
}

/**
 * @brief   Clear TCM Adaptives
 *  
 * @param   [in] ecm_id (Should be 0x7E9)
 * @param   [in] commlevel
 *
 * @author  Tristen Pierson, Ricardo Cigarroa
 */
u8 obd2can_dcx_clear_adaptives(u32 ecm_id, VehicleCommLevel commlevel)
{
    Obd2can_ServiceData servicedata;
    u8 status;

    obd2can_servicedata_init(&servicedata);
    servicedata.ecm_id = ecm_id;
    servicedata.service = DCX_StartRoutineByLocalId;
    servicedata.service_controldatabuffer[0] = 0x31;
    servicedata.service_controldatabuffer[1] = 0x00;
    servicedata.service_controldatalength = 2;
    servicedata.nowait_cantx = TRUE;
    servicedata.response_expected = TRUE;
    
    status = obd2can_dcx_initiate_diagnostic_operation(FALSE,ecm_id, 
                         ECM_RESPONSE_ID(ecm_id), diagnosticRequestChrysler_92);
    
    if (status == S_SUCCESS)
    {
        status = obd2can_txrx(&servicedata);
    }
     
    if (status != S_SUCCESS)
    {
        /*UDS commlevel*/
        status = obd2can_dcx_initiate_diagnostic_operation(FALSE,ecm_id, 
                        ECM_RESPONSE_ID(ecm_id), diagnosticRequestChrysler_03);
         
        if (status == S_SUCCESS)
        {
            servicedata.service_controldatabuffer[0] = 0x01;
            servicedata.service_controldatabuffer[1] = 0x30;
            servicedata.service_controldatabuffer[2] = 0x13;
            servicedata.service_controldatalength = 3; 
            
            status = obd2can_txrx(&servicedata);
        }
    }
        
    return status;
}

/**
 * @brief   Nonvolatile Memory Reset
 *  
 * @param   [in] ecm_id
 * @param   [in] commlevel
 *
 * @author  Tristen Pierson
 */
u8 obd2can_dcx_clear_kam(u32 ecm_id, VehicleCommLevel commlevel)
{
    u8 status;
   
    if(commlevel == CommLevel_KWP2000)
    {

        obd2can_dcx_initiate_diagnostic_operation(FALSE,ecm_id, 
                      ECM_RESPONSE_ID(ecm_id), diagnosticRequestChrysler_92);
        
        status = obd2can_dcx_ecu_reset_request(FALSE,ecm_id,NULL,
                                  nonvolatileMemoryReset,TRUE);
        
        obd2can_dcx_initiate_diagnostic_operation(FALSE,ecm_id, 
                      ECM_RESPONSE_ID(ecm_id), defaultSession_DCX);       
               
    }
    else
    {
        // Comm Level not valid
        status = S_INPUT;        
    }
    
    return status;
}

/**
 * @brief   Traction Control
 *  
 * @param   [in] operation (Set: 0 = Disable, 1 = Enable) 
 * @param   [in] commlevel
 *
 * @return  status
 *
 * @author  Tristen Pierson
 */
u8 obd2can_dcx_traction_control(u8 operation, VehicleCommLevel commlevel)
{
    Obd2can_ServiceData txdata;
    obd2can_rxinfo rxinfo;
    u8 status;
    u8 cycle_key = 0;

    obd2can_servicedata_init(&txdata);
    txdata.response_expected = FALSE;
    obd2can_rxinfo_init(&rxinfo);
    rxinfo.commlevel = commlevel;               
    
    if (operation > 1)
    {
        status = S_INPUT;
    }
    else if(commlevel == CommLevel_KWP2000)
    {
        // Procedure 1
        switch (operation)
        {
        case 0: // Disable            
            txdata.service = DCX_StartRoutineByLocalId;
            cycle_key = 0;
            break;
        case 1: // Enable            
            txdata.service = DCX_StopDiagnosticRoutineByTestNumber;
            cycle_key = 1;
            break;
        }        
        
        obd2can_init(Obd2canInitCustom,0x504);       
        obd2can_dcx_power_on_reset(0x620, commlevel);    
        
        delays(1000,'m');
        
        txdata.ecm_id = 0x620;
        txdata.subservice = 0xA4;
        status = obd2can_dcx_initiate_diagnostic_operation(FALSE,0x620, 0x504, 
                                          diagnosticRequestChrysler_92);
        
        obd2can_dcx_txrx(&txdata);
        rxinfo.cmd = txdata.service;        
        status = obd2can_rx(0x504, &rxinfo);
            
        if (status == S_SUCCESS)
        {
            obd2can_dcx_initiate_diagnostic_operation(FALSE,0x620, 0x504, 
                                                      defaultSession_DCX);
            obd2can_init(Obd2canInitNormal,0);
            obd2can_dcx_clear_kam(Obd2CanEcuId_7E0, commlevel);
        }
        // Procedure 2
        else
        {
            obd2can_init(Obd2canInitCustom,0x4C7);
            txdata.service = DCX_StartRoutineByLocalId;
            txdata.ecm_id = 0x747;
            txdata.service_controldatabuffer[0] = 0x30;
            txdata.service_controldatabuffer[1] = 0x46;
            
            switch (operation)
            {
            case 0: // Disable                
                txdata.subservice = 0x01;                
                txdata.service_controldatabuffer[2] = 0x05;
                txdata.service_controldatalength = 3;
                cycle_key = 0;
                break;
            case 1: // Enable
                txdata.subservice = 0x02;
                txdata.service_controldatalength = 2; 
                cycle_key = 1;
                break;
            }            
      
            obd2can_dcx_txrx(&txdata);
            rxinfo.cmd = txdata.service;        
            status = obd2can_rx(0x4C7, &rxinfo);

        }
        obd2can_init(Obd2canInitNormal,0);
        obd2can_dcx_clear_diagnostic_information(0x7DF);
    } //if (commlevel...)
    else
    {
        // Comm Level not valid
        status = S_INPUT;        
    }    

    if(cycle_key == 1)
    {
        cmdif_remotegui_msg_keycycle(CMDIF_CMD_SPECIAL_FUNCTIONS,"SPECIAL FUNCTIONS");

        cmdif_remotegui_messagebox(CMDIF_CMD_SPECIAL_FUNCTIONS, "SPECIAL FUNCTIONS", CMDIF_REMOTEGUI_ICON_NONE,
                             CMDIF_REMOTEGUI_BUTTONFACE_None, CMDIF_REMOTEGUI_BUTTONFACE_None, 
                             CMDIF_REMOTEGUI_BUTTONFACE_None,
                             CMDIF_REMOTEGUI_FOCUS_None,
                             "<HEADER>TRACTION CONTROL</HEADER>\nProcessing...");
    }

    obd2can_dcx_clear_diagnostic_information(0x7DF);
    
    obd2can_dcx_clear_diagnostic_information_extended(0x7E0, 0x7E8);

    return status;
}

//------------------------------------------------------------------------------
// InitiateDiagnosticOperation ($10)
// Inputs:  bool broadcast
//          u32 ecm_id
//          u32 ecm_response_id (0 = default, ecm_id + 8)
//          Obd2can_SubServiceNumber subservice (disableAllDTCs,
//              enableDTCsDuringDevCntrl, wakeUpLinks)
// Return:  u8  status
// Engineer: Ricardo Cigarroa
// Note: obd2can_init must be used to change the ecm response id filter before
//       calling this function.
//------------------------------------------------------------------------------
u8 obd2can_dcx_initiate_diagnostic_operation(bool broadcast, u32 ecm_id, u32 ecm_response_id,
                                             Obd2can_SubServiceNumber subservice)
{
    Obd2can_ServiceData servicedata;
    obd2can_rxinfo rxinfo;
    u8 status;
    
    obd2can_servicedata_init(&servicedata);
    servicedata.ecm_id = ecm_id;
    servicedata.service = DCX_InitiateDiagnosticOperation;
    servicedata.subservice = subservice;
    servicedata.response_expected = TRUE;
    servicedata.broadcast = broadcast;
    if(broadcast)
      servicedata.broadcast_id = 0x7DF;
    servicedata.rx_timeout_ms = 5000;
    
    if (ecm_response_id == 0)
    {
        servicedata.response_id = ECM_RESPONSE_ID(ecm_id);
    }
    else
        servicedata.response_id = ecm_response_id;
    
    obd2can_rxinfo_init(&rxinfo);
    rxinfo.commlevel = servicedata.commlevel;
    
    status = obd2can_dcx_txrx(&servicedata);
    
    return status;
}

//------------------------------------------------------------------------------
// ECUResetRequest ($11)
// Inputs:  bool broadcast
//          u32 ecm_id
//          u32 response_id
//          Obd2can_SubServiceNumber subservice
//          bool isreponse_expected (TRUE: wait for response)
// Return:  u8  status
// Engineer: Ricardo Cigarroa
//------------------------------------------------------------------------------
u8 obd2can_dcx_ecu_reset_request(bool broadcast,                       //$11
                                  u32 ecm_id,
                                  u32 response_id,
                                  Obd2can_SubServiceNumber subservice,
                                  bool isreponse_expected)
{
    Obd2can_ServiceData servicedata;
    u8 status;

    obd2can_servicedata_init(&servicedata);
    servicedata.ecm_id = ecm_id;
    servicedata.response_id = response_id;
    servicedata.broadcast = broadcast;
    servicedata.service = DCX_RequestModuleReset;
    servicedata.subservice = subservice;
    servicedata.response_expected = isreponse_expected;
    status = obd2can_txrx(&servicedata);
    
    return status;
}

//------------------------------------------------------------------------------
// ReturnNormalMode ($20)
// Inputs:  u8 type (u8 type (0: 101, else: 7E0)
// Return all nodes to normal operating mode         
// Return:  u8  status
// Engineer: Ricardo Cigarroa
//------------------------------------------------------------------------------
u8 obd2can_dcx_returnnormalmode(u8 type)
{
    Obd2can_ServiceData servicedata;
    u8 status;

    obd2can_servicedata_init(&servicedata);
    if (type == 0)
    {
        servicedata.ecm_id = 0x7DF;
    }
    else
    {
        servicedata.ecm_id = 0x7E0;
    }
    
    servicedata.service = DCX_ReturnNormalMode;
    status = obd2can_txrx(&servicedata);
    
    return status;
}

//------------------------------------------------------------------------------
// ReadByLocalIdentifier ($21)
// Inputs:  u32 ecm_id
//          u8  identifier
// Outputs: u8  *data
//          u32 *datalength
// Return:  u8  status
// Engineer: Tristen Pierson
//------------------------------------------------------------------------------
u8 obd2can_dcx_read_local_identifier(u32 ecm_id, u32 response_id, u8 identifier, u8 *data,
                                     u32 *datalength)
{
    Obd2can_ServiceData servicedata;
    u8  status;
        
     *datalength = 0;
       
    obd2can_servicedata_init(&servicedata);    
    servicedata.ecm_id = ecm_id;
    servicedata.response_id = response_id;
    servicedata.service = DCX_ReadDataByLocalIdentifier;   
    servicedata.service_controldatabuffer[0] = identifier;
    servicedata.service_controldatalength = 1;
    servicedata.rxdata = data;
    servicedata.flowcontrol_separationTime = 1;
    servicedata.response_expected = TRUE;
    
    status = obd2can_txrxcomplex(&servicedata);
    *datalength = servicedata.rxdatalength;
    
    if (status == S_SUCCESS)
    {
        if (servicedata.rxdatalength > 1)
        {
            *datalength = servicedata.rxdatalength - 1;
            memcpy((char*)data,(char*)&servicedata.rxdata[1],*datalength);
            return S_SUCCESS;
        }
        else
        {
            log_push_error_point(0x3000);
            return S_BADCONTENT;
        }
    }
    return status;
}

//------------------------------------------------------------------------------
// ReadMemoryByAddress ($23)
// High Level Read Memory By Address function
// Inputs:  u32 ecm_id
//          u32 address (memory address)
//          u16 length (to request upload)
//          MemorySize memsize
//          VehicleCommLevel commlevel
// Output:  u8  *data
//          u16 *datalength
// Return:  u8  status
// Engineer: Tristen Pierson
//------------------------------------------------------------------------------
u8 obd2can_dcx_read_memory_address(u32 ecm_id, u32 address, u16 length,
                                   MemorySize memsize,
                                   u8 *data, u16 *datalength,                                   
                                   VehicleCommLevel commlevel)
{
    u8  status = 0;

    if(commlevel == CommLevel_KWP2000)
    {
        status = obd2can_dcx_read_memory_address_kwp2000(ecm_id, address, length,
                                                         memsize, data, datalength);
    }
    else
    {
        log_push_error_point(0x3001);
        status = S_NOTSUPPORT;
    }    
    return status;
}

//------------------------------------------------------------------------------
// ReadMemoryByAddress (KWP2000) ($23)
// Read memory by address formatted for KWP2000
// Inputs:  u32 ecm_id
//          u32 address (24-bit memory address)
//          u8  length (to request upload)
//          MemorySize memsize
// Output:  u8  *data
//          u16 *datalength
// Return:  u8  status
// Engineer: Tristen Pierson
//------------------------------------------------------------------------------
u8 obd2can_dcx_read_memory_address_kwp2000(u32 ecm_id, u32 address, u16 length,
                                           MemorySize memsize,
                                           u8 *data, u16 *datalength)
{
    Obd2can_ServiceData servicedata;
    u8  status;

    if (datalength)
    {
        *datalength = 0;
    }
    status = S_FAIL;
    
#ifndef DEFAULT_CMD_MEMORY_SIZE
#error "odb2can_dcx.c: no definition for DEFAULT_CMD_MEMORY_SIZE"
#endif
#ifndef DEFAULT_CMD_MEMORY_SIZE
#error "odb2can_dcx.c: no definition for DEFAULT_CMD_MEMORY_SIZE"
#endif
    
    obd2can_servicedata_init(&servicedata);
    servicedata.ecm_id = ecm_id;
    servicedata.service = DCX_ReadMemoryByAddress;
    servicedata.rxdata = data;

    if (memsize == MemorySize_2_Byte)
    {
        servicedata.service_controldatabuffer[0] = (u8)(address >> 16);
        servicedata.service_controldatabuffer[1] = (u8)(address >> 8);
        servicedata.service_controldatabuffer[2] = (u8)(address & 0xFF);
        servicedata.service_controldatabuffer[3] = (u8)(length >> 8);
        servicedata.service_controldatabuffer[4] = (u8)(length & 0xFF);
        servicedata.service_controldatalength = 5;
    }
    else if (memsize == MemorySize_1_Byte)
    {
        servicedata.service_controldatabuffer[0] = (u8)(address >> 16);
        servicedata.service_controldatabuffer[1] = (u8)(address >> 8);
        servicedata.service_controldatabuffer[2] = (u8)(address & 0xFF);
        servicedata.service_controldatabuffer[3] = length;
        servicedata.service_controldatalength = 4;
    }
    else
    {
        return S_ERROR;
    }
    
    servicedata.first_frame_data_offset =
            servicedata.service_controldatalength - 2;
    
    status = obd2can_txrxcomplex(&servicedata);
    if (datalength)
    {
        *datalength = 0;
        if (status == S_SUCCESS)
        {
            *datalength = servicedata.rxdatalength;
        }
        else
        {
            log_push_error_point(0x3002);
            status = S_FAIL; 
        }
    }
    return status;
}

//------------------------------------------------------------------------------
// SecurityAccess ($27)
// Inputs:  u32 ecm_id
//          Obd2can_SubServiceNumber subservice
// InOuts:  u8  *data (In:seed, Out:key)
//          u8  *datalength (Out:seedlength, In:keylength)
// Return:  u8  status
// Engineer: Ricardo Cigarroa
//------------------------------------------------------------------------------
u8 obd2can_dcx_security_access(u32 ecm_id, Obd2can_SubServiceNumber subservice,
                                u8 *data, u8 *datalength, u32 algoindex)
{
    Obd2can_ServiceData servicedata;
    u8  status;
    
    obd2can_servicedata_init(&servicedata);
    servicedata.ecm_id = ecm_id;
    servicedata.service = DCX_SecurityAccess; 
    servicedata.tester_present_type = TESTER_PRESENT_BROADCAST;
    
    
    if(subservice == SPSrequestSeed)
    {
        switch(algoindex)
        {
           case UnlockId_HEMI_BOOTLOADER_SECURITY_UPLOAD1_63:
           case UnlockId_HEMI_BOOTLOADER_SECURITY_UPLOAD2_63:
           case UnlockId_HEMI_BOOTLOADER_SECURITY_UPLOAD3_63:
           case UnlockId_HEMI_BOOTLOADER_SECURITY_UPLOAD4_63:
                servicedata.service_controldatabuffer[0] = 0x63;
                break;      
            case UnlockId_HEMI_BOOTLOADER_SECURITY_DOWNL1_05:
            case UnlockId_HEMI_BOOTLOADER_SECURITY_DOWNL2_05:
            case UnlockId_SCIB_UNLOCK:
                 servicedata.service_controldatabuffer[0] = 0x05; 
                 break;
            case UnlockId_HEMI_APP_SECURITY_ACCESS1_03:
                 servicedata.service_controldatabuffer[0] = 0x03; 
                 break;
            case UnlockId_HEMI_APP_SECURITY_ACCESS1_65:
            case UnlockId_HEMI_APP_SECURITY_ACCESS2_65:
            case UnlockId_HEMI_APP_SECURITY_ACCESS3_65:
                 servicedata.service_controldatabuffer[0] = 0x65; 
                 break;
            default:
                log_push_error_point(0x3003);
                return S_FAIL;
        }
    }
    else if(subservice == SPSsendKey)
    {
          switch(algoindex)
          {
              case UnlockId_HEMI_BOOTLOADER_SECURITY_UPLOAD1_63:
              case UnlockId_HEMI_BOOTLOADER_SECURITY_UPLOAD2_63:
              case UnlockId_HEMI_BOOTLOADER_SECURITY_UPLOAD3_63:
              case UnlockId_HEMI_BOOTLOADER_SECURITY_UPLOAD4_63:
                  servicedata.service_controldatabuffer[0] = 0x64; 
                  break;      
              case UnlockId_HEMI_BOOTLOADER_SECURITY_DOWNL1_05:
              case UnlockId_HEMI_BOOTLOADER_SECURITY_DOWNL2_05:
              case UnlockId_SCIB_UNLOCK:
                   servicedata.service_controldatabuffer[0] = 0x06; 
                   break;
              case UnlockId_HEMI_APP_SECURITY_ACCESS1_03:
                   servicedata.service_controldatabuffer[0] = 0x04; 
                   break;
              case UnlockId_HEMI_APP_SECURITY_ACCESS1_65:
              case UnlockId_HEMI_APP_SECURITY_ACCESS2_65:
              case UnlockId_HEMI_APP_SECURITY_ACCESS3_65:
                   servicedata.service_controldatabuffer[0] = 0x66;  
                   break;
              default:
                  log_push_error_point(0x3004);
                  return S_FAIL;
          }
    }
    else //action_none, return
    {
        log_push_error_point(0x3005);
        return S_FAIL;
    }

 
    if (subservice == SPSsendKey)
    {
        if(algoindex == UnlockId_SCIB_UNLOCK)
        {
            servicedata.service_controldatabuffer[1] = data[0];
            servicedata.service_controldatabuffer[2] = data[1];
            servicedata.service_controldatalength = 3;
        }
        else
        {
            servicedata.service_controldatabuffer[1] = data[0];
            servicedata.service_controldatabuffer[2] = data[1];
            servicedata.service_controldatabuffer[3] = data[2];
            servicedata.service_controldatabuffer[4] = data[3];
            servicedata.service_controldatalength = 5;
        }
    }
    else//subservice = SPSrequestseed
    {
        servicedata.service_controldatalength = 1; 
    }
  
    status = obd2can_txrx(&servicedata);
    if(status == S_SUCCESS)
    {
        *datalength = servicedata.rxdatalength;           
        memcpy(data,&servicedata.rxdata[1],*datalength);
    }
    else
    { 
        log_push_error_point(0x3006);
        status = S_FAIL; 
    }
    
    return status;
}

//------------------------------------------------------------------------------
// DisableNormalCommunication ($28)
// Inputs:  None
// Return:  Nothing
// Engineer: Ricardo Cigarroa
//------------------------------------------------------------------------------
void obd2can_dcx_disablenormalcommunication()
{
    Obd2can_ServiceData servicedata;
    
    obd2can_servicedata_init(&servicedata);
    servicedata.ecm_id = 0x7DF;
    servicedata.response_expected = FALSE;
    servicedata.service = DCX_DisableNormalCommunication;
    servicedata.subservice = 0x02;
    obd2can_txrx(&servicedata);
     
}

//------------------------------------------------------------------------------
// EnableNormalCommunication ($29)
// Inputs:  None
// Return:  Nothing
// Engineer: Ricardo Cigarroa
//------------------------------------------------------------------------------
void obd2can_dcx_enablenormalcommunication()
{
   Obd2can_ServiceData servicedata;
    
   obd2can_servicedata_init(&servicedata);
   servicedata.ecm_id = 0x7DF;
   servicedata.response_expected = FALSE;
   servicedata.service = DCX_EnableNormalMessageTransmission;
   servicedata.subservice = 0x02;
   obd2can_txrx(&servicedata);  
}

//------------------------------------------------------------------------------
// RequestDiagnosticDataPacket ($2A)
// ReadDataByPeriodIdentifier (ISO14229)
// Inputs:  u32 ecm_id
//          u18 datarate
//          u8  *packetidlist
//          u8  packetidcount
//          VehicleCommLevel commlevel
// Return:  u8  status
// Engineer: Quyen Leba
//------------------------------------------------------------------------------
u8 obd2can_dcx_requestdiagnosticdatapacket_2A(u32 ecm_id, u8 datarate,
                                               u8 *packetidlist,
                                               u8 packetidcount,
                                               VehicleCommLevel commlevel)
{
    Obd2can_ServiceData servicedata;
    u8  txdata[64];
    u8  status;
    
    if(packetidcount == 0)
        return S_INPUT;
    
    obd2can_servicedata_init(&servicedata);
    servicedata.ecm_id = ecm_id;
    servicedata.service = DCX_RequestDiagnosticDataPacket;
    servicedata.commlevel = commlevel;
    if (commlevel == CommLevel_ISO14229)
    {
        txdata[0] = datarate;
        servicedata.txdata = txdata;
        if (packetidcount > 0 && packetidlist)
        {
            memcpy((char*)&txdata[1],(char*)packetidlist,packetidcount);
        }
        servicedata.txdatalength = 1 + packetidcount;
        
        status = obd2can_txrx(&servicedata);
    }
    else    //if (commlevel == CommLevel_KWP2000)
    {
        //TODOQ: do we need to implement this !?!
        status = S_NOTSUPPORT;
    }
    
    return status;
}

//------------------------------------------------------------------------------
// DynamicallyDefineMessage ($2C)
// Inputs:  u32 ecm_id
//          u8  packetid
//          u8  type
//          u8  size
//          u8  position
//          u32 address
//          VehicleCommLevel commlevel
// Return:  u8  status
// Engineer: Tristen Pierson
//------------------------------------------------------------------------------
u8 obd2can_dcx_dynamicallydefinemessage(u32 ecm_id, u8 packetid,
                                        u8 *type, u8 *size, u8 *position,
                                        u32 *address, u8 pidcount,
                                        VehicleCommLevel commlevel)
{
    Obd2can_ServiceData servicedata;
    u8 *txdata;
    u8  status;
    u8  i;
    u16 j;
    u32 cooldowntime;
    
    txdata = __malloc(0xFF);
    if (txdata == NULL)
    {
        return S_MALLOC;
    }
    
    if (pidcount > DCX_KWP_CAN_MAX_PID_PACKET)
    {
        return S_NOTFIT;
    }
    
    obd2can_servicedata_init(&servicedata);
    servicedata.ecm_id = ecm_id;
    servicedata.commlevel = commlevel;    
    servicedata.service = DCX_DynamicallyDefineMessage;    
    servicedata.service_controldatabuffer[0] = packetid;    
    servicedata.service_controldatalength = 1;    
    servicedata.flowcontrol_separationTime = 1;    
    servicedata.txdatalength = 0;
    servicedata.response_expected = TRUE;
    servicedata.initial_wait_extended = 3; 
    
    j = 0;
    status = S_SUCCESS; 
    
    for (i = 0; i < pidcount; i++)
    {
        switch (commlevel)
        {
        case CommLevel_KWP2000:
            switch ((PidType)type[i])
            {
            case PidTypeRegular:                
                txdata[j++] = 0x03;                     // Define By Identifier
                txdata[j++] = position[i];
                txdata[j++] = size[i];
                txdata[j++] = (u8)(address[i] >> 8);
                txdata[j++] = (u8)(address[i]);
                txdata[j++] = 1;
                servicedata.txdatalength += 6;
                break;
            case PidTypeDMR:        
                txdata[j++] = 0x02;                     // Define By Memory Address
                txdata[j++] = position[i];
                txdata[j++] = size[i];
                txdata[j++] = (u8)(address[i]);         // LSB
                txdata[j++] = (u8)(address[i] >> 8);
                txdata[j++] = (u8)(address[i] >> 16);   // MSB
                servicedata.txdatalength += 6;
                break;
            default:
                status = S_INPUT;
                break;
            }               
            break;
            // TODO: Add other comm levels if applicable
        default:
            status = S_NOTSUPPORT;
        } // switch
        if (status != S_SUCCESS)
        {
            break;
        }
    }// for(...)

    if (status == S_SUCCESS)
    {
        cooldowntime = obd2_getcooldowntime();
        obd2_setcooldowntime(1500);
        
        servicedata.txdata = txdata;
        status = obd2can_txrx(&servicedata);
        
        obd2_setcooldowntime(cooldowntime);
    }
    
    // Garbage collection
    __free(txdata);
    
    return status;
}

//------------------------------------------------------------------------------
// Clear Dynamically Defined Local Identifier ($2C)
// Inputs:  u32 ecm_id
// Return:  N/A
// Engineer: Tristen Pierson
// TODO: Add different comm level support
//------------------------------------------------------------------------------
u8 obd2can_dcx_cleandynamicallydefinedidentifier(u32 ecm_id, u8 packetid)
{
    Obd2can_ServiceData servicedata;

    obd2can_servicedata_init(&servicedata);
    servicedata.ecm_id = ecm_id;
    servicedata.service = DCX_DynamicallyDefineMessage;

    servicedata.service_controldatabuffer[0] = packetid;
    servicedata.service_controldatabuffer[1] = 0x04; // Clear LID    
    servicedata.service_controldatalength = 2;    

    return obd2can_txrx(&servicedata);
}

//------------------------------------------------------------------------------
// Start Routine by Local ID Tx ($31) - tx data with command
// Inputs:  u32 ecm_id
//          u32 response_id
//          u8  routine_Identifier
//          u8  *data
//          u32 datalength
// Return:  u8  status
// Engineer: Ricardo Cigarroa
//------------------------------------------------------------------------------
u8 obd2can_dcx_start_routine_by_local_id_tx(u32 ecm_id, u32 response_id, u8 routine_identifier, 
                                         u8 routine_entry, u8 *data, u32 datalength)
{
    u8 status;
    Obd2can_ServiceData servicedata;
    
    obd2can_servicedata_init(&servicedata);
    servicedata.ecm_id = ecm_id;
    servicedata.service = DCX_StartRoutineByLocalId;
    servicedata.subservice = routine_identifier;
    servicedata.response_id = response_id;
    if(routine_entry != 0)
    {
        servicedata.service_controldatabuffer[0] = routine_entry;
        servicedata.service_controldatalength = 1;
    }
    
    servicedata.txdata = data;
    servicedata.txdatalength = datalength;
    servicedata.response_expected = TRUE;
    
    status = obd2can_txrx(&servicedata);
    if(status != S_SUCCESS)
    {
        log_push_error_point(0x3007);
    }
    
    return status; 
}

//------------------------------------------------------------------------------
// Start Routine by Local ID Rx ($31) - recieve data from command
// Inputs:  u32 ecm_id
//          u8  routine_Identifier (0xE0-Erase routine, 0xE1-verification routine)
//          u8  *data
//          u32 datalength
// Return:  u8  status
// Engineer: Ricardo Cigarroa
//------------------------------------------------------------------------------
u8 obd2can_dcx_start_routine_by_local_id_rx(u32 ecm_id, u8 routine_identifier, 
                                            u8 *data, u32 *datalength)
{
    u8 status;
    Obd2can_ServiceData servicedata;
    
    obd2can_servicedata_init(&servicedata);
    servicedata.ecm_id = ecm_id;
    servicedata.service = DCX_StartRoutineByLocalId;
    servicedata.subservice = routine_identifier;
    servicedata.rxdata = data; 
    servicedata.response_expected = TRUE;
    servicedata.flowcontrol_separationTime = 1;
    servicedata.initial_wait_extended = 3; 
    
    status = obd2can_txrx(&servicedata);
    if(status != S_SUCCESS)
    {
        log_push_error_point(0x3008);
    }
    else
    {
        *datalength = servicedata.rxdatalength;
    }
    
    return status; 
}
//------------------------------------------------------------------------------
// Results of Routine by Local ID Tx ($33) - tx data with command
// Inputs:  u32 ecm_id
//          u32 response_id
//          u8  routine_Identifier
// Return:  u8  status
// Engineer: Rhonda Rusak
//------------------------------------------------------------------------------
u8 obd2can_dcx_results_routine_by_local_id(u32 ecm_id, u32 response_id, u8 routine_identifier, 
                                         u8 routine_entry)
{
    u8 status;
    Obd2can_ServiceData servicedata;
    
    obd2can_servicedata_init(&servicedata);
    servicedata.ecm_id = ecm_id;
    servicedata.service = DCX_Results_of_RoutineByLocalID;
    servicedata.subservice = routine_identifier;
    servicedata.response_id = response_id;
    if(routine_entry != 0)
    {
        servicedata.service_controldatabuffer[0] = routine_entry;
        servicedata.service_controldatalength = 1;
    }
    
    servicedata.txdata = NULL;
    servicedata.txdatalength = 0;
    servicedata.response_expected = TRUE;
    
    status = obd2can_txrx(&servicedata);
    if(status != S_SUCCESS)
    {
        log_push_error_point(0x3007);
    }
    
    return status; 
}
//------------------------------------------------------------------------------
// RequestDownload ($34)
// Inputs:  u32 ecm_id
//          u8  dataformat
//          u32 requestaddress
//          u32 requestlength
//          RequestLengthType reqtype
//          VehicleCommLevel commlevel
// Output:  u16 *maxdownloadblocklength
// Return:  u8  status
// Engineer: Ricardo Cigarroa
//------------------------------------------------------------------------------
u8 obd2can_dcx_request_download(u32 ecm_id, u8 dataformat,
                                 u32 requestaddress, u32 requestlength,
                                 RequestLengthType reqtype,
                                 VehicleCommLevel commlevel,
                                 u16 *maxdownloadblocklength)
{
    Obd2can_ServiceData servicedata;
    u8  requestdata[8];
    u8  status;
    u16 maxlength;

    maxlength = INVALID_MEMBLOCK_SIZE;
    
    obd2can_servicedata_init(&servicedata);
    servicedata.ecm_id = ecm_id;
    servicedata.service = DCX_RequestDownload;
    servicedata.tester_present_type = TESTER_PRESENT_BROADCAST;
    servicedata.commlevel = commlevel;    

    requestdata[0] = (u8)(requestaddress >> 16);
    requestdata[1] = (u8)(requestaddress >> 8);
    requestdata[2] = (u8)(requestaddress & 0xFF);
    
    requestdata[3] = (u8)(requestlength >> 24);
    requestdata[4] = (u8)(requestlength >> 16);
    requestdata[5] = (u8)(requestlength >> 8);
    requestdata[6] = (u8)(requestlength & 0xFF);

    servicedata.txdata = requestdata;
    servicedata.txdatalength = 7;    
    status = obd2can_txrxcomplex(&servicedata);
    
    if (status == S_SUCCESS)
    {
       maxlength = ((u16)servicedata.rxdata[0]) << 8 |
                    (u16)servicedata.rxdata[1];
       
        maxlength &= 0xFFFC;
    }
    else
    {
        log_push_error_point(0x3009);
    }
    
    if (maxdownloadblocklength)
    {
        *maxdownloadblocklength = maxlength;
    }
    
    return status;
}

//------------------------------------------------------------------------------
// RequestUpload ($35)
// Inputs:  u32 ecm_id
//          u32 requestaddress
//          u32 requestlength
//          RequestLengthType reqtype
//          VehicleCommLevel commlevel
// Output:  u16 *maxuploadblocklength
// Return:  u8  status
// Engineer: Ricardo Cigarroa
//------------------------------------------------------------------------------
u8 obd2can_dcx_request_upload(u32 ecm_id,
                               u32 requrestaddress, u32 requestlength,
                               RequestLengthType reqtype,
                               VehicleCommLevel commlevel,
                               u16 *maxuploadblocklength)  
{
    Obd2can_ServiceData servicedata;
    u8  requestbuffer[8];
    u8  status;

    if (maxuploadblocklength)
    {
        *maxuploadblocklength = INVALID_MEMBLOCK_SIZE;
    }
    
    obd2can_servicedata_init(&servicedata);
    servicedata.ecm_id = ecm_id;
    servicedata.service = DCX_RequestUpload;
    servicedata.tester_present_type = TESTER_PRESENT_NODE;
    if (reqtype == Request_4_Byte) 
    {
        requestbuffer[0] = (u8)(requrestaddress >> 24);
        requestbuffer[1] = (u8)(requrestaddress >> 16);
        requestbuffer[2] = (u8)(requrestaddress >> 8);
        requestbuffer[3] = (u8)(requrestaddress & 0xFF);
       
        requestbuffer[4] = (u8)(requestlength >> 16);
        requestbuffer[5] = (u8)(requestlength >> 8);
        requestbuffer[6] = (u8)(requestlength & 0xFF);
        
        servicedata.txdata = requestbuffer;
        servicedata.txdatalength = 7;
    }
    else
    {
        return S_BADCONTENT;
    }

    status = obd2can_txrx(&servicedata);
    if (status == S_SUCCESS && maxuploadblocklength)
    {
       *maxuploadblocklength = 
                (servicedata.rxdata[0] << 8) + servicedata.rxdata[1];
        *maxuploadblocklength &= 0xFFFC;
    }
    else
    {
        log_push_error_point(0x300A);
    }
    
    return status;
}

//------------------------------------------------------------------------------
// TransferData Upload ($36)
// Inputs:  u32 ecm_id
//          u8  blocktracking
//          VehicleCommLevel commlevel
//          u32 request_address (UNUSED)
//          RequestLengthType request_type (UNUSED)
// Outputs: u8  *data
//          u16 *datalength
// Return:  u8  status
// Engineer: Ricardo Cigarroa
//------------------------------------------------------------------------------
u8 obd2can_dcx_transfer_data_upload(u32 ecm_id, u8 blocktracking,
                                     VehicleCommLevel commlevel,
                                     u32 request_address,
                                     RequestLengthType request_type,
                                     u8 *data, u16 *datalength)
{
    Obd2can_ServiceData servicedata;
    u8 status;

    *datalength = 0;
    obd2can_servicedata_init(&servicedata);
    servicedata.ecm_id = ecm_id;
    servicedata.service = DCX_TransferData;
    servicedata.rxdata = data;
    servicedata.flowcontrol_separationTime = 1;
    servicedata.initial_wait_extended = 3;

    servicedata.service_controldatabuffer[0] = blocktracking;
    servicedata.service_controldatabuffer[1] = (u8)((request_address >> 16) & 0xFF);
    servicedata.service_controldatabuffer[2] = (u8)((request_address >> 8) & 0xFF);
    servicedata.service_controldatabuffer[3] = (u8)(request_address & 0xFF);
    servicedata.service_controldatalength = 4;

    status = obd2can_txrxcomplex(&servicedata);
    if (status == S_SUCCESS)
    {
        *datalength = servicedata.rxdatalength;
    }
    else
    {
        log_push_error_point(0x300B);
    }
   
    return status;
}

//------------------------------------------------------------------------------
// TransferData Download ($36)
// Inputs:  u32 ecm_id
//          Obd2can_SubServiceNumber subservice (UNUSED)
//          u8  blocktracking
//          u32 startingaddr
//          MemoryAddressType addrtype
//          u8  *data
//          u16 datalength
//          RequestLengthType reqtype
//          VehicleCommLevel commlevel
//          bool response_expected
// Return:  u8  status

// Engineer: Ricardo Cigarroa
//------------------------------------------------------------------------------
u8 obd2can_dcx_transfer_data_download(u32 ecm_id,
                                       Obd2can_SubServiceNumber subservice,
                                       u8 blocktracking, u32 startingaddr,
                                       MemoryAddressType addrtype,
                                       u8 *data, u16 datalength,
                                       VehicleCommLevel commlevel,
                                       bool response_expected)
{
    Obd2can_ServiceData servicedata;
    u8 status;

    obd2can_servicedata_init(&servicedata);
    servicedata.ecm_id = ecm_id;
    servicedata.service = DCX_TransferData;
    servicedata.txdata = data;
    servicedata.txdatalength = datalength;
    servicedata.response_expected = response_expected;
    servicedata.flowcontrol_separationTime = 1;
    servicedata.commlevel = commlevel;    

    if (addrtype == Address_24_Bits) 
    {
        servicedata.subservice = (u8)subservice; 
        servicedata.service_controldatabuffer[0] = (u8)(startingaddr >> 16);
        servicedata.service_controldatabuffer[1] = (u8)(startingaddr >> 8);
        servicedata.service_controldatabuffer[2] = (u8)(startingaddr & 0xFF);
        servicedata.service_controldatalength = 3;
    }
    
    status = obd2can_txrx(&servicedata);
    if(status != S_SUCCESS)
    {
        log_push_error_point(0x300C);
    }

    return status;
}

//------------------------------------------------------------------------------
// RequestTransferExit ($37)
// Inputs:  u32 ecm_id
//          VehicleCommLevel commlevel (UNUSED)
// Return:  u8  status
// Engineer: Ricardo Cigarroa
//------------------------------------------------------------------------------
u8 obd2can_dcx_request_transfer_exit(u32 ecm_id, VehicleCommLevel commlevel)
{
    Obd2can_ServiceData servicedata;
    u8 status;

    obd2can_servicedata_init(&servicedata);
    servicedata.ecm_id = ecm_id;
    servicedata.service = DCX_RequestDataTransferExit;
    servicedata.commlevel = commlevel;
    if (commlevel == CommLevel_ISO14229)
    {
        servicedata.tester_present_type = TESTER_PRESENT_BROADCAST;
    }
    else
    {
        //do nothing for now
    }
    
    status = obd2can_txrx(&servicedata);
    if(status != S_SUCCESS)
    {
        log_push_error_point(0x300D);
    }
    return status;
}

//------------------------------------------------------------------------------
// DCX_WriteDataByIdentifier ($3B)
// Inputs:  u32 ecm_id
//          u8  data_id
//          u8  *data
//          u8  datalength
// Return:  u8  status
// Engineer: Ricardo Cigarroa
//------------------------------------------------------------------------------
u8 obd2can_dcx_write_data_by_local_id(u32 ecm_id, u8 data_id,
                                       u8 *data, u32 datalength)
{
    // Need this if using mode $92
    //u32 temp_cooldown_time = obd2_getcooldowntime();
    Obd2can_ServiceData servicedata;
    u8 status;

    // Need this if using in mode $92
    //obd2_setcooldowntime(1000);

    obd2can_servicedata_init(&servicedata);
    servicedata.ecm_id = ecm_id;
    servicedata.service = DCX_WriteDataByIdentifier;
    servicedata.flowcontrol_separationTime = 1; 

    servicedata.service_controldatabuffer[0] = data_id;
    servicedata.service_controldatalength = 1;

    servicedata.txdata = data;
    servicedata.txdatalength = datalength;
    servicedata.response_expected = TRUE;

    status = obd2can_txrx(&servicedata);
    if(status != S_SUCCESS)
    {
        log_push_error_point(0x300E);
    }

    // Need this if using in mode $92
    //obd2_setcooldowntime(temp_cooldown_time);
    
    return status;
}
//------------------------------------------------------------------------------
// DCX_WriteDataByIdentifier ($3B)
// Inputs:  u32 ecm_id
//          u8  *data_id
//          u8  *data
//          u8  datalength
// Return:  u8  status
// Engineer: Quyen Leba & Rhonda Rusak
//------------------------------------------------------------------------------
u8 obd2can_dcx_write_data_by_localID(u32 ecm_id, u32 response_id, u8 subservice,
                                       u8 *data, u32 datalength)
{
    Obd2can_ServiceData servicedata;
    u8 status;

    obd2can_servicedata_init(&servicedata);
    servicedata.ecm_id = ecm_id;
    servicedata.service = DCX_WriteDataByIdentifier;
    servicedata.subservice = subservice;
    servicedata.rx_timeout_ms = 5000;
    servicedata.response_id = response_id;

    servicedata.txdata = data;

    servicedata.service_controldatalength = 0;

    servicedata.txdatalength = datalength;
    servicedata.response_expected = TRUE;

    status = obd2can_txrx(&servicedata);
    return status;
}
//------------------------------------------------------------------------------
// RequestDiagnosticDataPacket ($A0)
// Inputs:  u32 ecm_id
//          u8  packetid
//          PidType type
//          u8  size
//          u8  position
//          u32 address
//          VehicleCommLevel commlevel
// Return:  u8  status
// Engineer: Quyen Leba

//------------------------------------------------------------------------------
u8 obd2can_dcx_requestdiagnosticdatapacket(u32 ecm_id, u8 datarate,
                                            u8 *packetidlist, u8 packetidcount,
                                            VehicleCommLevel commlevel)
{
    Obd2can_ServiceData servicedata;
    u8  status;
    
    obd2can_servicedata_init(&servicedata);
    servicedata.ecm_id = ecm_id;
    servicedata.service = DCX_RequestDiagnosticDataPacket_A0;
    servicedata.service_controldatabuffer[0] = datarate;
    servicedata.service_controldatalength = 1;
    
    if (packetidcount > 5)
    {
        return S_INPUT;
    }
    if (packetidcount > 0)
    {
        memcpy((char*)&servicedata.service_controldatabuffer[1],
               packetidlist,packetidcount);
        servicedata.service_controldatalength += packetidcount;
    }
    else
    {
        if(datarate == 0x08)
            servicedata.response_expected = FALSE;  
        else
            servicedata.response_expected = TRUE;
    }
    
    status = obd2can_txrx(&servicedata);
    if(status != S_SUCCESS)
    {
        log_push_error_point(0x300F);
    }
    return status;
}

//------------------------------------------------------------------------------
// DynamicallyDefineDiagnosticDataPacket ($A1)
// Inputs:  u32 ecm_id
//          u8  packetid (1,2,...)
//          PidType type
//          u8  size (1,2,4)
//          u8  position (start from 1)
//          u32 address
//          VehicleCommLevel commlevel
// Return:  u8  status
// Engineer: Quyen Leba
//------------------------------------------------------------------------------
u8 obd2can_dcx_dynamicallydefinediagnosticdatapacket(u32 ecm_id,
                                                      u8 packetid,
                                                      PidType type,
                                                      u8 size, u8 position,
                                                      u32 address,
                                                      VehicleCommLevel commlevel)
{
    Obd2can_ServiceData servicedata;
    u8  tpl;
    u8  status;
    
    tpl = (size) | (((u8)type) << 6) | (position << 3);
    
    obd2can_servicedata_init(&servicedata);
    servicedata.ecm_id = ecm_id;
    servicedata.service = DCX_DynamicallyDefineDiagnosticDataPacket;
    servicedata.service_controldatabuffer[0] = packetid;
    servicedata.service_controldatabuffer[1] = tpl;
    servicedata.commlevel = commlevel;
    
    if (type == PidTypeRegular)
    {
        servicedata.service_controldatabuffer[2] = (u8)(address >> 8);
        servicedata.service_controldatabuffer[3] = (u8)(address);
        servicedata.service_controldatalength = 4;
    }
    else if (type == PidTypeDMR)
    {
        servicedata.service_controldatabuffer[2] = (u8)(address >> 24);
        servicedata.service_controldatabuffer[3] = (u8)(address >> 16);
        servicedata.service_controldatabuffer[4] = (u8)(address >> 8);
        servicedata.service_controldatabuffer[5] = (u8)(address);
        servicedata.service_controldatalength = 6;
    }
    else
    {
        return S_INPUT;
    }
    
    status = obd2can_txrx(&servicedata);
    if(status != S_SUCCESS)
    {
        log_push_error_point(0x3010);
    }
    return status;
}

/**
 *  @brief Clear DTCs on DCX CAN processors
 *
 *  @return Status
 */
u8 obd2can_dcx_cleardtc() 
{
    u8 i;
    u8 status;

    obd2_open();

    for (i = 0; i < ECM_MAX_COUNT; i++)
    {
        if (gObd2info.ecu_block[i].ecutype != EcuType_Unknown)
        {
            switch(gObd2info.ecu_block[i].commlevel)
            {
            case CommLevel_ISO14229:
                status = obd2can_dcx_clear_diagnostic_information_iso14229(FALSE, 
                                                  gObd2info.ecu_block[i].ecu_id, 
                                                  gObd2info.ecu_block[i].ecu_response_id);
                break;
            case CommLevel_KWP2000:
                status = obd2can_dcx_clear_diagnostic_information(gObd2info.ecu_block[i].ecu_id);
                if (status == S_SUCCESS)
                {                            
                    status = obd2can_dcx_clear_diagnostic_information_extended(gObd2info.ecu_block[i].ecu_id, NULL);   
                }
                break;
            default:
                status = S_COMMLEVEL;
                break;
            }
        }
        if (status == S_TIMEOUT)
        {
            break;
        }
        else
        {
            status = S_SUCCESS; 
        }
    }
    return status;
}

//------------------------------------------------------------------------------
// Read DTC using $18 command
// Input:   u32 ecm_id
// Output:  dtc_info *info
// Return:  u8  overall status
// Engineer: Ricardo Cigarroa
//------------------------------------------------------------------------------
/*u8 obd2can_dcx_alt_readdtc(u32 ecm_id, dtc_info *info)
{
    Obd2can_ServiceData servicedata;
    u8 status;
    u8 i;
    u8 dtccount;
    u16 code;
    u32 codeindex;

    obd2can_servicedata_init(&servicedata);
    servicedata.ecm_id = ecm_id;
    servicedata.service = DCX_Read_DTCs;
    
    servicedata.service_controldatabuffer[0] = (u8)0x00; // Status
    servicedata.service_controldatabuffer[1] = (u8)0xFF; // Group - 0xFF = ALL
    servicedata.service_controldatabuffer[2] = (u8)0x00; // 0x00 inlcuded by by spec
    servicedata.service_controldatalength = 3;
    status = obd2can_txrx(&servicedata);
    if (status == S_SUCCESS)
    {
        dtccount = obd2_rxbuffer[0]; // Number of DTCs reported
        info->count = dtccount;
        if (dtccount)
        {
            for(i=0; i<dtccount; i++)
            {
                // DTCs receieved in this format:
                // [DTC MSB] [DTC LSB] [Status Byte]
                // Refer to SAEJ2190 spec Mode $18 Section 5.8 Table 1 for a
                // CLOSE BUT NOT EXACTLY message format
                codeindex = (1 + (i*3));
                code = (u16)(obd2_rxbuffer[codeindex] << 8) & 0xFF00;
                code |= (u16)(obd2_rxbuffer[codeindex + 1] & 0x00FF);
                info->codes[i] = code;
                info->codestatus[i] = obd2_rxbuffer[codeindex + 2];
            }
        }
    }
    else
    {
        log_push_error_point(0x3011);
    }
    return status;obd2can_init(Obd2canInitCustom,0x504);
}
*/
//------------------------------------------------------------------------------
// Read DTC using $18 command
// Output:  dtc_info *info
// Return:  u8  status
// Engineer: Rhonda Rusak
//------------------------------------------------------------------------------
u8 obd2can_dcx_readdtc_all(dtc_info *info)
{
    Obd2can_ServiceData servicedata;
    u8 status;
    u8 overallstatus = S_FAIL;
    u8 i,x,y=0;
    u8 dtccount;
    u16 code;
    u32 codeindex;
    u32 Node_IDs[9]={0x7E0,0x7E1,0x784,0x622,0x600,0x7F8,0x620,0x747,0x740};
    u32 Device_IDs[9]={0x7E8,0x7E9,0x785,0x484,0x500,0x53F,0x504,0x4C7,0x4C0};

    obd2can_servicedata_init(&servicedata);
    info->count = 0;

    for(x=0; x< 9; x++)
    {

        if(x>2)
            obd2can_init(Obd2canInitCustom,Device_IDs[x]);
        else
            obd2can_init(Obd2canInitNormal,0);

        servicedata.ecm_id = Node_IDs[x];
        servicedata.response_id = Device_IDs[x];
        servicedata.service = DCX_Read_DTCs;
    
        servicedata.service_controldatabuffer[0] = (u8)0x00; // Status
        servicedata.service_controldatabuffer[1] = (u8)0xFF; // Group - 0xFF = ALL
        servicedata.service_controldatabuffer[2] = (u8)0x00; // 0x00 inlcuded by by spec
        servicedata.service_controldatalength = 3;
        status = obd2can_txrx(&servicedata);
        if (status == S_SUCCESS)
        {
            overallstatus = S_SUCCESS;
            dtccount = obd2_rxbuffer[0]; // Number of DTCs reported
            info->count += dtccount;
            if (dtccount)
            {
                for(i=0; i<dtccount; i++)
                {
                    // DTCs receieved in this format:
                    // [DTC MSB] [DTC LSB] [Status Byte]
                    // Refer to SAEJ2190 spec Mode $18 Section 5.8 Table 1 for a
                    // CLOSE BUT NOT EXACTLY message format
                    codeindex = (1 + (i*3));
                    code = (u16)(obd2_rxbuffer[codeindex] << 8) & 0xFF00;
                    code |= (u16)(obd2_rxbuffer[codeindex + 1] & 0x00FF);
                    info->codes[y] = code;
                    info->codestatus[y] = obd2_rxbuffer[codeindex + 2];
                    y++;
                }
            }
        }
        else
        {
            log_push_error_point(0x3011);
        }

        //prevent premature timeout
        tasktimer_setup(15000, TASKTIMER_REPEAT, TaskTimerTaskType_PreventAppTimeout,
                            NULL, NULL);

    }

    obd2can_init(Obd2canInitNormal,0);
    return overallstatus;
}
/**
 *  @brief Read ECM info from OBD2 info
 *  
 *  @param [in]  ecm_info   ECM info structure
 *
 *  @return status
 *  
 *  @details This function reads all available part numbers and serial number.
 */
u8 obd2can_dcx_readecminfo(ecm_info *ecm)
{
    ecm->ecm_count = gObd2info.ecu_count;
    memcpy(ecm->vin, gObd2info.vin, sizeof(ecm->vin));
    
    for(u8 i = 0; i < ECM_MAX_COUNT; i++)
    {
        if (gObd2info.ecu_block[i].ecutype != EcuType_Unknown)
        {
            ecm->commtype[i] = gObd2info.ecu_block[i].commtype;
            ecm->commlevel[i] = gObd2info.ecu_block[i].commlevel;
            ecm->codecount[i] = gObd2info.ecu_block[i].partnumber_count;
            memcpy(ecm->codes[i][0],
                   gObd2info.ecu_block[i].partnumbers.dcx.part_number,
                   sizeof(gObd2info.ecu_block[i].partnumbers.dcx.part_number));
            memcpy(ecm->second_codes[i][1],
                   gObd2info.ecu_block[i].partnumbers.dcx.part_type_description, 
                   sizeof(gObd2info.ecu_block[i].partnumbers.dcx.part_type_description));
            ecm->second_codes[i][2][0] = gObd2info.ecu_block[i].partnumbers.dcx.part_type_number;
            memcpy(ecm->second_codes[i][3],
                   gObd2info.ecu_block[i].partnumbers.dcx.boot_version, 
                   sizeof(gObd2info.ecu_block[i].partnumbers.dcx.boot_version));
        }
    }
    return S_SUCCESS;
}

//------------------------------------------------------------------------------
// Check for vehicle wake up (CAN)
// Inputs:  u8  veh_type
//          u8  ecm_index
// Return:  u8  status
// Engineer: Ricardo Cigarroa
// Note: DCX CAN processors require a specific sequence of commands before the 
// actual wake up command (when setting up the ecu). 
//------------------------------------------------------------------------------
u8 obd2can_dcx_wakeup(u16 veh_type, u8 ecm_index)
{
    u32 ecm_id;
    u32 response_ecm_id;
    u8  status;
    u8  part_type;
    
    ecm_id = ECM_GetEcmId(VEH_GetEcmType(veh_type,ecm_index));
    response_ecm_id = ECM_GetResponseECMId(VEH_GetEcmType(veh_type,ecm_index));
    part_type =  flasherinfo->ecminfo.second_codes[ecm_index][2][0];
       
    //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    // Begin ECU Setup
    //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    
    //execute upload/download intro sequence
    obd2can_dcx_initsequence(part_type, ecm_id, response_ecm_id);
     
    //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    // Wake up ECU
    //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    
    obd2can_ping(Obd2CanEcuId_7E0, FALSE);
    obd2can_dcx_initiate_diagnostic_operation(FALSE,ecm_id, response_ecm_id, diagnosticRequestChrysler_92);
    status = obd2can_dcx_initiate_diagnostic_operation(FALSE,ecm_id, response_ecm_id, diagnosticRequestChrysler_85);
    if(status != S_SUCCESS)
    {
        log_push_error_point(0x3012);
    }
    
    return status;
}

//------------------------------------------------------------------------------
// Utility file is a small worker loaded (downloaded) to ECM to help upload
// and download tune (i.e. read/write ECM flash memory). Not all ECMs requires.
// Inputs:  u32 ecm_type (defined for ecm_defs)
//          u8 utilitychoiceindex (which util to use)
//          bool isdownloadutility (TRUE: download utlity)
// Return:  u8  status
// Engineer: Ricardo Cigarroa
// it uses ($34) ($36 $36 ...) ($37)
// Note: data is download from top to bottom (forward)
//------------------------------------------------------------------------------
u8 obd2can_dcx_downloadutility(u32 ecm_type, u8 utilitychoiceindex,
                                bool isdownloadutility)
{
#define DCX_UTILITY_BLOCKSIZE           0x800
    Obd2can_SubServiceNumber subservice;
    F_FILE *fptr = NULL;
    u8 databuffer[DCX_UTILITY_BLOCKSIZE];
    u16 maxdownloadblocklength;
    u32 utilfilesize;
    //u32 utilblocksize;
    u32 blocklength;
    u8 status;
    u32 ecm_id;
    u32 targetaddr;                     //ecm physical address
    VehicleCommLevel commlevel;
    u8  blocktracking;
    u32 bytecount;
    u8  order;
    u8  order_index;
    u8  order_current;
    u8  loop;
    u8  value;
    u8 *utilityfilename;
    u8 require_separate_execution;    //TRUE if separate execute instruction required
    bool logical_block_index;
    bool useencryption;    
    bool skipblock;
    bool use_variant_id; 
    u32 decryptblocklength;
    u32 decryptedutilitylength=0;
    u32 decryptedutilityposition=0;
    u8 i;
    u32 bytesdownloaded;
    u32 totalbytecount; 
    u32 percentage; 
    
     /*Download E2 Data for 2015 GPEC2 and 2015 GPEC2A*/
    if((settingstune.variant_ID == 0x26)||(settingstune.variant_ID == 0x44))
    {
        status = obd2tune_unlockecm(UnlockTask_Upload,
                                    ecm_type,CommType_CAN,0);

        status = obd2can_dcx_download_e2_data(ecm_type, Modify);
        if(status != S_SUCCESS)
        {
            log_push_error_point(0x4425);
            return status;
        }
        else
        {
            return status; 
        }
    } 
    
    /*If a processor uses a variant ID in order to be distinguished(ex: 2012 and 
    2013 GPEC, do a test to figure out if the modified OS needs to be 
    downloaded or not. */
    use_variant_id = FALSE; 
    if(ECM_IsUseVariantId(ecm_type))
    {
        status = dcx_ecm_utility_testfunc_gpec2(ecm_type); 
        if(status != S_SUCCESS)
        {
            /*2012 GPEC processors do not require mod OS download.*/
            return S_UTILITYNOTREQUIRED; 
        }
        else 
        {
            use_variant_id = TRUE; 
        }
    }
    
    if(use_variant_id)
    {
        if (!SETTINGS_IsDownloadFail())
        {
            //set download fail flag before downloading modified OS (utility)
            //this flag will be cleared once all download is successful
            SETTINGS_SetDownloadFail();
            SETTINGS_SetTuneAreaDirty();
            settings_update(SETTINGS_UPDATE_IF_DIRTY);
        }
            
/*        if(flasherinfo->flashtype != CMDIF_ACT_STOCK_FLASHER)
        {
            if(flasherinfo->uploadstock_required == FALSE)
            {
                status = S_UTILITYNOTREQUIRED;
                goto obd2can_dcx_downloadutility_done;
            }
        }
*/
        obd2can_dcx_initiate_diagnostic_operation(FALSE,0x7E0, 0x7E8, diagnosticRequestChrysler_92);
        obd2can_dcx_initiate_diagnostic_operation(FALSE,0x7E0, 0x7E8, diagnosticRequestChrysler_85);
        status = obd2tune_unlockecm(UnlockTask_Upload,
                                    ecm_type,CommType_CAN,0);   
        if(status != S_SUCCESS)
        {
            cmdif_datainfo_responsedata.responsecode = CMDIF_ACK_UNLOCK_ECU_FAIL;
            log_push_error_point(0x3013);
            goto obd2can_dcx_downloadutility_done;;
        }       
    }
    if (isdownloadutility)
    {
        utilityfilename = (u8*)ECM_GetDUtiFilename(ecm_type,utilitychoiceindex);
        useencryption = (bool)ECM_IsDUtiUseEncryption(ecm_type,utilitychoiceindex);
    }
    else
    {
        utilityfilename = (u8*)ECM_GetUUtiFilename(ecm_type,utilitychoiceindex);
        useencryption = (bool)ECM_IsUUtiUseEncryption(ecm_type,utilitychoiceindex);
    }
    if (utilityfilename == NULL)
    {
        return S_UTILITYNOTREQUIRED;
    }
    else if (utilityfilename[0] == NULL)
    {
        return S_UTILITYNOTREQUIRED;
    }
    if(useencryption)
    {
        obd2tune_testerpresent_add_ecm(ECM_GetEcmId(ecm_type), ECM_GetCommLevel(ecm_type));
        
        //Enable the TaskTimer to send tester present. 
        if(tasktimer_setup(2000, TASKTIMER_REPEAT, TaskTimerTaskType_TesterPresent, (TimerTaskFunc)&obd2tune_testerpresent, NULL) != S_SUCCESS)
        {
            log_push_error_point(0x3014);
            cmdif_datainfo_responsedata.responsecode = CMDIF_ACK_FAILED;
        }
        housekeeping_additem(HouseKeepingType_TaskTimerTO, 10000);
        
        status = ubf_validate_file(utilityfilename);
        if (status != S_SUCCESS)
        {
            log_push_error_point(0x3015);
            return status;
        }
       
        // Disable the TaskTimer sending tester present
        if(tasktimer_setup(0, TASKTIMER_REPEAT, TaskTimerTaskType_TesterPresent, NULL, NULL) != S_SUCCESS)
        {
            log_push_error_point(0x3016);
            cmdif_datainfo_responsedata.responsecode = CMDIF_ACK_FAILED;
        }
        housekeeping_removeitem(HouseKeepingType_TaskTimerTO);
    }
    
    totalbytecount = 0; 
    utilfilesize = 0; 
    for(i=0; i<MAX_UTILITY_OPTIONS; i++)
    {
        utilfilesize = (u32)ECM_GetDUtiBlockSize(ecm_type,utilitychoiceindex,i);
        totalbytecount += utilfilesize; 
    }
    utilfilesize = 0; //initialize variable. 
    
    fptr = NULL;
    if((fptr = genfs_default_openfile(utilityfilename, "rb")) == NULL)
    {
        log_push_error_point(0x3017);
        return S_OPENFILE;
    }
    if ((fseek(fptr,0,SEEK_END)) != 0)
    {
        log_push_error_point(0x3018);
        status = S_SEEKFILE;
        goto obd2can_dcx_downloadutility_done;
    }
    
    status = ubf_get_data_info(fptr,&decryptedutilityposition,&decryptedutilitylength);
    
    if (fseek(fptr, 0, SEEK_SET) != 0)
    {
        log_push_error_point(0x3019);
        status = S_SEEKFILE;
        goto obd2can_dcx_downloadutility_done;
    }
    if (decryptedutilitylength == 0)
    {
        log_push_error_point(0x301A);
        status = S_BADCONTENT;
        goto obd2can_dcx_downloadutility_done;
    }
    
    if(useencryption)
    {
        if(ubf_seek_start(fptr) != S_SUCCESS)
        {
            log_push_error_point(0x301B);
            return S_SEEKFILE;
        }
    }
    
    targetaddr = (u32)ECM_GetDUtiBlockAddr(ecm_type,utilitychoiceindex,0);
    
    if(use_variant_id)
    {
        if (fseek(fptr, (targetaddr + 0x2800), SEEK_SET) != 0)
        {
            log_push_error_point(0x301C);
            status = S_SEEKFILE;
            goto obd2can_dcx_downloadutility_done;
        }
    }
        
    //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    ecm_id = ECM_GetEcmId(ecm_type);
    commlevel = ECM_GetCommLevel(ecm_type);
    require_separate_execution = ECM_IsDUtiSeparateExe(ecm_type, utilitychoiceindex);
    logical_block_index = FALSE;
    bytesdownloaded = 0; 
    value = 0; 
    percentage = 0;
    
    order_current = ORDER_0;
    for(loop=0;loop<MAX_ECM_MEMORY_BLOCKS;loop++)
    {
        for(order_index=0;
            order_index<MAX_ECM_MEMORY_BLOCKS;order_index++)
        {
            if (isdownloadutility)
                order = (u8)ECM_GetDUtiBlockFlagOrder(ecm_type,utilitychoiceindex,order_index);
            else
                order = (u8)ECM_GetUUtiBlockFlagOrder(ecm_type,utilitychoiceindex,order_index);
            
            if (order_current == order)
            {
                if (isdownloadutility)
                {
                    targetaddr = (u32)ECM_GetDUtiBlockAddr(ecm_type,utilitychoiceindex,order_index);
                    utilfilesize = (u32)ECM_GetDUtiBlockSize(ecm_type,utilitychoiceindex,order_index);
                    skipblock = (bool)ECM_IsDUtiBlockSkipDownload(ecm_type,utilitychoiceindex,order_index);
                }
                else
                {
                    targetaddr = (u32)ECM_GetUUtiBlockAddr(ecm_type,utilitychoiceindex,order_index);
                    utilfilesize = (u32)ECM_GetUUtiBlockSize(ecm_type,utilitychoiceindex,order_index);
                    skipblock = (bool)ECM_IsUUtiBlockSkipDownload(ecm_type,utilitychoiceindex,order_index);
                }
                
                if (skipblock)
                {
                    status = S_SUCCESS;
                    continue;
                }
                if(utilfilesize == 0)
                {
                    //memblock with blocksize zero reached
                    status = S_SUCCESS;
                    goto obd2can_dcx_downloadutility_done;
                }
                
                //Flash starter and fingerprint interlock    
                if(ECM_IsFingerPrintInterlock(ecm_type))
                {
                    status = obd2can_dcx_fingerprint_interlock(ecm_type, logical_block_index);
                    if(status != S_SUCCESS)
                    {
                        log_push_error_point(0x301D);
                        status = S_FAIL; 
                        goto obd2can_dcx_downloadutility_done;  
                    }
                    logical_block_index = TRUE; 
                } 
                
                //Erase. Only certain processors require this...ex: 2013 GPEC2
                if(use_variant_id)
                {
                    status = obd2can_dcx_erase_ecm(ecm_type, TRUE);
                    if(status != S_SUCCESS)
                    {
                        log_push_error_point(0x301E);
                        status = S_FAIL; 
                        goto obd2can_dcx_downloadutility_done;                        
                    }
                }
                                      
                status = obd2can_dcx_request_download(ecm_id, 0x01,
                                                       targetaddr, utilfilesize,
                                                       Request_4_Byte,
                                                       commlevel,
                                                       &maxdownloadblocklength);
                maxdownloadblocklength &= 0xFFFC;
                if(maxdownloadblocklength == 0)
                {
                    log_push_error_point(0x301F);
                    status = S_FAIL;
                    goto obd2can_dcx_downloadutility_done;
                }
                
                else if (maxdownloadblocklength > DCX_UTILITY_BLOCKSIZE)
                {
                    maxdownloadblocklength = DCX_UTILITY_BLOCKSIZE;
                }
                if (useencryption)
                {
                    maxdownloadblocklength = (maxdownloadblocklength/8)*8;
                }
                
                if (status == S_SUCCESS)
                {
                    //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
                    //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
                    //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
                    bytecount = 0;
                    blocktracking = 0;
                    while(bytecount < utilfilesize)
                    {
                        blocktracking++;
                        blocklength = utilfilesize - bytecount;
                        if (blocklength > maxdownloadblocklength)
                        {
                            blocklength = maxdownloadblocklength;
                        }
                        
                        //TODOQ: right now, it's always subservice = download (0x00)
                        subservice = download;
                        
                        if (useencryption)
                        {
                            decryptblocklength = blocklength;
                            
                            if(blocklength%8)
                                decryptblocklength += (8-(blocklength%8)); // Check blocklength for even mutltiple of 8 byte  
                            
                            if(ubf_read_block(databuffer, decryptblocklength, fptr) != S_SUCCESS)
                            {
                                log_push_error_point(0x3020);
                                status = S_READFILE;
                                goto obd2can_dcx_downloadutility_done;
                            }
                        }
                        else
                        {
                            if ((fread(databuffer,1,blocklength, fptr)) != blocklength)
                            {
                                log_push_error_point(0x3021);
                                status = S_READFILE;
                                goto obd2can_dcx_downloadutility_done;
                            }
                        }
                        
                        //Note: startingaddr (0) is not used here
                        status = obd2can_dcx_transfer_data_download_utility
                            (ecm_id, subservice, blocktracking, 0, Address_32_Bits,
                             databuffer, blocklength, commlevel, TRUE);
                                                
                        bytecount += blocklength;
                        bytesdownloaded += blocklength; 
                        
                        if(status == S_SUCCESS)
                        {
                            //update utility progress bar
                            value = bytesdownloaded*100/totalbytecount;
                            if(((value%1) == 0) && (value > percentage))
                            {
                                percentage = value;
                                obd2can_dcx_utility_progressbar(percentage, 
                                                             "DOWNLOADING BOOT INFO");
                                debug_reportprogress(percentage, "Downloading Bootloader");
                            }
                        }
                        else
                        {
                            log_push_error_point(0x3022);
                            status = S_FAIL;
                            break;
                        }
                    }//while(utilblockcount > 0)...
                }//if (status == S_SUCCESS)...
                
                if (status == S_SUCCESS)
                {
                    status = obd2can_dcx_request_transfer_exit(ecm_id,commlevel);
                }
                else                
                {
                    log_push_error_point(0x3023);
                    goto obd2can_dcx_downloadutility_done;
                }
                
                //additional task after block download 
                if(require_separate_execution)
                {
                    if(order == 0)
                    {
                        status = obd2can_dcx_download_e2_data(ecm_type, Modify);
                        if(status != S_SUCCESS)
                        {
                            log_push_error_point(0x3024);
                            status = S_FAIL;
                            goto obd2can_dcx_downloadutility_done;
                        }
                        
                        obd2can_dcx_initiate_diagnostic_operation(FALSE,0x7E0, 0x7E8, diagnosticRequestChrysler_92);
                        obd2can_dcx_initiate_diagnostic_operation(FALSE,0x7E0, 0x7E8, diagnosticRequestChrysler_85);
                    }
                    else if(order == 1)
                    {
                        status = obd2can_dcx_download_utility_rsa_signature(ecm_type,fptr);
                        if(status != S_SUCCESS)
                        {
                            log_push_error_point(0x3025);
                            status = S_FAIL;
                            goto obd2can_dcx_downloadutility_done;
                        }
                    }                
                }
                break;
            }
        }//for(memblock_index...
        order_current++;
    }//for(loop=...
    
obd2can_dcx_downloadutility_done:    

    if(status == S_SUCCESS)
    {
        if(!require_separate_execution)
        {
            status = obd2can_dcx_enter_utility(ecm_id);
            if(status == S_SUCCESS)
            {
                if(ECM_IsUtilityCheck(ecm_type))
                {
                    status = obd2can_dcx_checkutility(ecm_type, UtilTypeBoth);
                }
                if (status != S_SUCCESS)
                {
                    log_push_error_point(0x3026);
                    status = S_FAIL;                
                }
            }
            else
            {
                log_push_error_point(0x3027);
            }
        } 

    }
    if (fptr)
    {
        genfs_closefile(fptr);
    }
    return status;
}

//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
u8 obd2can_dcx_checkutility(u32 ecm_type, u8 utility_type)
{
    Obd2can_ServiceData servicedata;
    u8 status;
    u16 ecm_def;
    
    obd2can_servicedata_init(&servicedata);
    servicedata.ecm_id = ECM_GetEcmId(ecm_type);
    servicedata.service = 0x1A;
    servicedata.subservice = 0x21;
    status = obd2can_txrx(&servicedata);

    if (status == S_SUCCESS)
    {
        switch (utility_type)
        {
            case UtilTypeUpload:
            case UtilTypeDownload:
            case UtilTypeBoth:
                if (servicedata.rxdatalength == 3)
                {
                    ecm_def = (servicedata.rxdata[0] >> 8) + servicedata.rxdata[1];
                    if (servicedata.rxdata[2] == utility_type && ecm_type == ecm_def)
                    {
                        status = S_SUCCESS;
                    }
                    else
                    {
                        status = S_FAIL;
                    }
                }
                else
                {
                    status = S_FAIL;
                }
                break;
            default:
                status = S_FAIL;
                break;
        }
    }
    return status;
}

//------------------------------------------------------------------------------
// Some ECM requires utility file to upload/download tune, and requires this
// function to take ECM out of utility-file mode
// Input:   u32 ecm_type (defined in ecm_defs)
// Return:  u8  status
// Engineer: Ricardo Cigarroa
//------------------------------------------------------------------------------
u8 obd2can_dcx_exitutility(u32 ecm_type)
{
   u8 status;
   u32 ecm_id;
   u32 response_ecm_id;
   
   if(IsVariantId_DCX_GPEC2)
   {
       //2013+ GPEC processors do not require 
       //this exit function. 
       return S_SUCCESS; 
   }
   
   ecm_id = ECM_GetEcmId(ecm_type);                    
   response_ecm_id =  ECM_GetResponseECMId(ecm_type);
   
   Obd2can_ServiceData servicedata;
    
   obd2can_servicedata_init(&servicedata);
   servicedata.ecm_id = ecm_id;
   servicedata.service = 0x1A;
   servicedata.subservice = 0x55;
    
   status = obd2can_txrx(&servicedata);
   
   if(status == S_SUCCESS)
   {
      obd2can_dcx_returnnormalmode(0);
      obd2can_dcx_returnnormalmode(1);
     
      obd2can_dcx_initiate_diagnostic_operation(FALSE,ecm_id, response_ecm_id, diagnosticRequestChrysler_92);
      obd2can_dcx_initiate_diagnostic_operation(FALSE,ecm_id, response_ecm_id, diagnosticRequestChrysler_92);
     
      obd2can_dcx_returnnormalmode(0);
      obd2can_dcx_returnnormalmode(1);
     
      obd2can_dcx_reset_ecm(ecm_type);
      obdcan_dcx_control_faults(0x01, NULL);
     
      obd2can_dcx_cleardtc();
   }
    
    return status;
}

//------------------------------------------------------------------------------
// Some ECM requires a finalization after a download
// Input:   u32 ecm_type (defined in ecm_defs)
// Return:  u8  status
// Engineer: Ricardo Cigarroa
//------------------------------------------------------------------------------
u8 obd2can_dcx_downloadfinalizing(u32 ecm_type)
{
    u8  status; 
    u8  unlockpriv[16]; //only SCP use this now, it uses 1st byte only
    u8  count = 5; 
    u8  resetcount = 0;
//    u8  WA580 = 0;
//    u8  diag_type = 0;
//    u8  data_buffer[16];
//    u32 temp_number = 0;
    Obd2can_ServiceData data;
    
    obd2can_servicedata_init(&data);
    
    if (SETTINGS_IsDemoMode())
    {
        return S_SUCCESS;
    }
    
    cmdif_internalresponse_ack(CMDIF_CMD_DO_DOWNLOAD,
                                       CMDIF_ACK_PROCESSING_DATA,NULL,0);
    
    /*We wait 6 seconds to allow the processor to send its reset command (0x51)
    after completing a download. We then check for another 0x51 to make sure we 
    have a legitimate failure.*/
    
    delays(6000,'m');
    while(count)
    {
        delays(300,'m');
        obd2tune_testerpresent();
        status = can_rx(1000);
        if(CanMsgFrameData(can_rxframe)[1] == 0x51)
        {   
            resetcount++;
            if(resetcount == 3)
            {
                status = S_FAIL;
                break;
            }
        }
        else
        {
            status = S_SUCCESS; 
        }
        count--;
    }
        
    if(ECM_IsE2Required(ecm_type) && status == S_SUCCESS)
    {

            //obd2can_dcx_enablenormalcommunication();
            //obd2can_dcx_reset_ecm(ecm_type);
              
            obd2can_dcx_initiate_diagnostic_operation(FALSE,0x7E0, 0x7E8, diagnosticRequestChrysler_92);
            obd2can_dcx_initiate_diagnostic_operation(FALSE,0x7E0, 0x7E8, diagnosticRequestChrysler_85);
                     
            status = obd2tune_unlockecm(UnlockTask_Upload,ecm_type,
                                        ECM_GetCommType(ecm_type),unlockpriv);
            if(status != S_SUCCESS)
            {
                log_push_error_point(0x3028);
                return S_FAIL;
            }
            status = obd2can_dcx_download_e2_data(ecm_type,Normal);
              
            if(status != S_SUCCESS)
            {
                log_push_error_point(0x3029);
                return S_FAIL;
            }
         
        obdcan_dcx_control_faults(0x01,ECM_GetEcmId(ecm_type));
        obd2can_dcx_cleardtc();
    }

/*    
    if (status == S_SUCCESS)
    {
        //read tire size and axle ratio and set them back to stock if they are not.
        status = obd2can_dcx_read_tire_axel_parameters(&WA580, data, &diag_type);
        if (status == S_SUCCESS)
        {
            if ((SETTINGS_TUNE(stockwheels.axel) != 0) && (SETTINGS_TUNE(stockwheels.tire) != 0))
            {
                if (((data.rxdata[0]<< 8) + data.rxdata[1]) != SETTINGS_TUNE(stockwheels.axel) ||
                    ((data.rxdata[6]<< 8 + data.rxdata[7])) != SETTINGS_TUNE(stockwheels.tire))
                {
                    //tire size or axle ratio is not stock. Return to stock.
                    temp_number = SETTINGS_TUNE(stockwheels.axel);
                    if(temp_number != 0)
                    {
                        data_buffer[1] = temp_number;
                        data_buffer[0] = (temp_number>>8);
                    }

                    temp_number = SETTINGS_TUNE(stockwheels.tire);
                    if(temp_number != 0)
                    {
                        data_buffer[7] = temp_number;
                        data_buffer[6] = (temp_number>>8);
                    }
                    
                    status = obd2can_dcx_write_tire_axel_parameters(CMDIF_CMD_DO_DOWNLOAD, "PROGRAM VEHICLE", 
                                                                    WA580, data_buffer, diag_type);
                    if (status != S_SUCCESS)
                    {
                        //log_point_error_code
                        status = S_FAIL; 
                    }
                    else
                    {
                        SETTINGS_TUNE(stockwheels.tire) = 0;
                        SETTINGS_TUNE(stockwheels.axel) = 0;
                        SETTINGS_SetTuneAreaDirty();
                        settings_update(SETTINGS_FORCE_UPDATE);
                    }
                }
            }
        }
        else
        {
            //only fail if attempt to return tire size/axle ratio to stock is
            //unsuccessful.
            status = S_SUCCESS; 
        }
                
    }
*/
    return status; 
}

//------------------------------------------------------------------------------
// Reset an ECM or both (if present)
// Inputs:  u8  ecm_type
// Return:  u8  status
// Engineer: Ricardo Cigarroa
//------------------------------------------------------------------------------
u8 obd2can_dcx_reset_ecm(u16 ecm_type)
{
    u32 ecm_id;
    VehicleCommType commtype;
    VehicleCommLevel commlevel;

    ecm_id = ECM_GetEcmId(ecm_type);
    commtype = ECM_GetCommType(ecm_type);
    commlevel = ECM_GetCommLevel(ecm_type);
    
    if (commtype != CommType_CAN)
    {
        return S_COMMTYPE;
    }
    
    if (commlevel == CommLevel_ISO14229)//either ISO or any other comm_type
    {
        // do nothing for now
        //obd2can_dcx_ecu_reset_request(FALSE,ecm_id,hardReset,TRUE);
    }
    else if (commlevel == CommLevel_KWP2000)
    {
        obd2can_dcx_ecu_reset_request(FALSE,ecm_id,NULL,hardReset,TRUE);
    }
    else
    {
        return S_COMMLEVEL;
    }
    //delays(750, 'm');   //PCM is allowed 750ms re-init time after reset...
    return S_SUCCESS;
}

//------------------------------------------------------------------------------
// Inputs:  u8  ecm_type
//          bool is_cal_only
// Return:  u8  status
// Engineer: Ricardo Cigarroa
//TODOQ: T43 handled differently, check it again
//------------------------------------------------------------------------------
u8 obd2can_dcx_erase_ecm(u8 ecm_type, bool is_cal_only)
{
    Obd2can_ServiceData servicedata;
    u32 ecm_id;
    u8  status;
    
    ecm_id = ECM_GetEcmId(ecm_type);
        
    obd2can_servicedata_init(&servicedata);
    servicedata.ecm_id = ecm_id;
    servicedata.service = 0x31;
    servicedata.service_controldatabuffer[0] = 0xE1;
    servicedata.service_controldatabuffer[1] = 0x01;
    servicedata.service_controldatalength = 2;
    servicedata.initial_wait_extended = 3;
    servicedata.busy_wait_extended = 3;

    status = obd2can_txrx(&servicedata);
    
    if((status == S_SUCCESS) && (CanMsgFrameData(can_rxframe)[1] == 0x71))
    {
       obd2can_servicedata_init(&servicedata);
       servicedata.ecm_id = ecm_id;
       servicedata.service = 0x31;
       servicedata.service_controldatabuffer[0] = 0xE0;
       servicedata.service_controldatalength = 1;
       servicedata.initial_wait_extended = 3;
       servicedata.busy_wait_extended = 3;

       status = obd2can_txrx(&servicedata);
    }
    else
    {
       log_push_error_point(0x302A);
       status = S_FAIL;
    }
  
    return status;
}

//------------------------------------------------------------------------------
// Validate DMR
// Inputs:  u32 ecm_id
//          DlxBlock *dlxblock
//          VehicleCommLevel commlevel
// Return:  u8  status
// Engineer: Quyen Leba/Tristen Pierson
// TODOQ: commlevel is currently not used
//------------------------------------------------------------------------------
u8 obd2can_dcx_validatedmr(u32 ecm_id, DlxBlock *dlxblock,
                            VehicleCommLevel commlevel)
{
    return obd2can_dcx_validateByRapidPacketSetup(ecm_id, dlxblock, commlevel);
}

//------------------------------------------------------------------------------
// Validate By Rapid Packet Setup
// Inputs:  u32 ecm_id
//          DlxBlock *dlxblock
//          VehicleCommLevel commlevel
// Return:  u8  status
// Engineer: Quyen Leba/Patrick Downs/Tristen Pierson
//------------------------------------------------------------------------------
u8 obd2can_dcx_validateByRapidPacketSetup(u32 ecm_id, DlxBlock *dlxblock,
                                          VehicleCommLevel commlevel)
{
    u8 status;
    u8 pidPos[1] = {1};
    
    switch (commlevel)
    {
    case CommLevel_KWP2000:
        obd2can_dcx_cleandynamicallydefinedidentifier(ecm_id, DCX_KWP_CAN_PACKETID_START);
        status = obd2can_dcx_dynamicallydefinemessage
            (ecm_id, DCX_KWP_CAN_PACKETID_START, &dlxblock->pidType, &dlxblock->pidSize, 
             pidPos, &dlxblock->pidAddr, 1, CommLevel_KWP2000);
        break;
    default:
        status = S_COMMLEVEL;
        break;
    }

    return status;
}
                                 
//---------------------------------------------------------------------------------------------------------------------------
// Request_byIdentifer  ($1A)
// Input:   u8  Indentifier     
//          u32 ecm_id         
//          u32 Device_ID
//          u8  Use_retrys                                 
// Return:  u8  status          (S_SUCCESS, S_FAIL, S_NOCOMMUNICATION)
// This routine asks the ECU for specific data indentified by a predertimerned identifier.
// Engineer: Ricardo Cigarroa
//TODO: use CAN_RX_Block to simplify this function
//---------------------------------------------------------------------------------------------------------------------------
u8 obd2can_dcx_request_byidentifier(u8 Identifier, u32 ecm_id, u32 Device_ID, u8 Use_retrys)           // Grab information by Indentifer
{
  
     Obd2can_ServiceData servicedata;
     u8 status;
     u8 count = 0;
     signed char retry = 2;
    
     if(Use_retrys == 0)
     {
       retry = 1;
     }
     
     
     while(retry > 0)
     {
         obd2can_servicedata_init(&servicedata);
         servicedata.ecm_id = ecm_id;
         servicedata.service = DCX_ReadDataByIdentifier;
         servicedata.service_controldatabuffer[0] = Identifier;
         servicedata.service_controldatalength = 1;
         servicedata.flowcontrol_separationTime = 1; 
         servicedata.initial_wait_extended = 3; 
         status = obd2can_txrx(&servicedata);
              
         if(status == S_SUCCESS)
         {
            return status;
         }
         else
         {
            if(Use_retrys == 0)
            {
               retry = 0;
               break;
            }
            count++;
            obd2can_testerpresent(FALSE,Obd2CanEcuId_7DF,CommLevel_Unknown);
            if(count >= 3)
            {
                if(retry > 0)
                {
                  retry--;
                  count = 0;
                  break;
                }
                else
                {
                  return status; //fail status
                }
                
            }
    
         }
     }
      
     return status;
}
                                                             
//------------------------------------------------------------------------------
// Get vehicle part type 
// Inputs:  u32 ecm_id, u8 *parttype, u8 *parttype_numerical_value
// Return:  u8 status
// This function simply requests the vehicle's part type (ex: NGC4, GPEC, etc.)
// Engineer: Ricardo Cigarroa
//------------------------------------------------------------------------------
u8 obd2can_dcx_getparttype(u32 ecm_id, u8 *vehicle_parttype, u8 *parttype_numerical_value)
{
    u8 status;
    u8 part_type_des_buff[64];
  
    can_init(CAN_INITMODE_NORMAL,0);
       
    if(obd2can_dcx_initiate_diagnostic_operation(FALSE,ecm_id, 0, diagnosticRequestChrysler_92) == S_SUCCESS)
    {
        status = S_SUCCESS;
    }
    
    //TODO: modify the function below so that it adds 8 to ecm_id like the function above. 
    if(obd2can_dcx_request_byidentifier(0x87, 0x7E0, 0x7E8 ,1) == S_SUCCESS)
    {
        parttype_numerical_value[0] = obd2_rxbuffer[6];
           
        obd2_dcx_part_type_description(parttype_numerical_value[0], part_type_des_buff);
        memcpy((void*)vehicle_parttype, (void*)part_type_des_buff,10);
        if(vehicle_parttype != NULL)
        {
            status = S_SUCCESS;
        }
        else
        {
            status = S_FAIL;
        }
     }
    
    return status;
}
                                                                  
//------------------------------------------------------------------------------
// Reset processor using $22 command
// Inputs:  u8 command
// Return:  u8 status 
// The purpose of this function is to reset 2012 Chrysler ECMs using mode 22
// Engineer: Ricardo Cigarroa
//------------------------------------------------------------------------------    
u8 obd2can_dcx_reset_22()
{
    Obd2can_ServiceData servicedata;
    u8 status;

    obd2can_servicedata_init(&servicedata);
    servicedata.ecm_id = 0x7E0;
    servicedata.nowait_cantx = TRUE;
    servicedata.service = DCX_ReadDataByParameterIdentifier;
    servicedata.service_controldatabuffer[0] = 0x01;
    servicedata.service_controldatabuffer[1] = 0xD5;
    servicedata.service_controldatalength = 2;
    status = obd2can_txrx(&servicedata);
    
    if(status != S_SUCCESS)
    {
        log_push_error_point(0x302B);
        status = S_FAIL;
    }
    
    servicedata.service_controldatabuffer[0] = 0x02;
    servicedata.service_controldatabuffer[1] = 0x33;
    status = obd2can_txrx(&servicedata);
    
    if(status != S_SUCCESS)
    {
        log_push_error_point(0x302C);
        status = S_FAIL;
    }
    
    return status;  
}

//------------------------------------------------------------------------------
// obd2can_dcx_block_IDs
// Inputs:  u32 ecm_id
//          u32 response_ecm_id
// Return:  u8 count
// This function counts block IDs
// Engineer: Ricardo Cigarroa
//------------------------------------------------------------------------------  
u8 obd2can_dcx_block_IDs(u32 ecm_id, u32 response_ecm_id)
{
    u8 i;
    u8 count = 0;
    u8 status;
    
    for(i=0x9C; i<0x9F; i++)
    {
       status = obd2can_dcx_request_byidentifier(i,ecm_id, response_ecm_id,0);
       if(status == S_SUCCESS)
       {
         count++;
       }
       else
       {
         log_push_error_point(0x302D);
       }
    }
    
    return count;
    
}

//------------------------------------------------------------------------------
// obd2can_dcx_block_IDs
// Inputs:  None
// Return:  Nothing
// This function sends a 10 command for the Chrysler ECM.
// Engineer: Ricardo Cigarroa
//------------------------------------------------------------------------------  
void obd2can_dcx_92_85()
{
    Obd2can_ServiceData servicedata;

    obd2can_servicedata_init(&servicedata);
    servicedata.ecm_id = 0x7DF;
    servicedata.nowait_cantx = TRUE;
    servicedata.response_expected = FALSE;
    servicedata.service = DCX_InitiateDiagnosticOperation;
    servicedata.subservice = 0x92;
    obd2can_txrx(&servicedata);
    
    delays(20, 'm');
    obd2can_dcx_testerpresent(FALSE,Obd2CanEcuId_7DF,CommLevel_Unknown);
    obdcan_dcx_control_faults(0x02,NULL);
    delays(30, 'm');
}

//------------------------------------------------------------------------------
// obdcan_dcx_control_faults
// Inputs:  u8 Enable_Faults
// Return:  Nothing
// Engineer: Ricardo Cigarroa
//------------------------------------------------------------------------------  
void obdcan_dcx_control_faults(u8 Enable_Faults, u32 ecm_id)
{
    Obd2can_ServiceData servicedata;

    obd2can_servicedata_init(&servicedata);

    if(ecm_id == NULL)
      servicedata.ecm_id = 0x7DF;
    else
      servicedata.ecm_id = ecm_id;

    servicedata.nowait_cantx = TRUE;
    servicedata.response_expected = FALSE;
    servicedata.service = 0x85;
    servicedata.subservice = 0x02;
    servicedata.service_controldatabuffer[0] = 0xFF;
    servicedata.service_controldatabuffer[1] = 0x00; 
    servicedata.service_controldatabuffer[2] = Enable_Faults;
    servicedata.service_controldatalength = 3;
    obd2can_txrx(&servicedata);
    
    if(Enable_Faults == 0x01)
    {
       delays(20, 'm');
       obd2can_dcx_enablenormalcommunication();
    }
}

//------------------------------------------------------------------------------
// Get vehicle boot version
// Inputs:  ecm_info *ecm
// Return:  u8 status
// This function gets the boot version from the ecm. 
// Engineer: Ricardo Cigarroa
//------------------------------------------------------------------------------
u8 obd2can_dcx_get_boot_version(u32 ecm_id, u8 *ecm_boot_version)
{  
   u8 i,j=0;
   u8 status;

   if(obd2can_dcx_request_byidentifier(0x9E, 0x7E0, 0x7E8 ,1) == S_SUCCESS)
   {
      status = S_SUCCESS;
      for(i=0; i<10; i++)
      {
         ecm_boot_version[i] = obd2_rxbuffer[10+j];
         j++;
      }
   }
   else
   {
      status = S_FAIL;
   }
        
   return status;
  
}

//------------------------------------------------------------------------------
// Get the number of DTCs read
// Input: u8 *count, u32 ecm_id
// Output: u8 *count
// Return:  u8  status
// Engineer: Ricardo Cigarroa
//------------------------------------------------------------------------------
u8 obd2can_dcx_getdtccount(u8 *count, u32 ecm_id)
{
    u8  status;    
    
    *count = 0;
  
    Obd2can_ServiceData servicedata;

    obd2can_servicedata_init(&servicedata);
    servicedata.ecm_id = ecm_id;
    servicedata.service = DCX_Read_DTCs;
    servicedata.service_controldatabuffer[0] = 0x00;
    servicedata.service_controldatabuffer[1] = 0xFF;
    servicedata.service_controldatabuffer[2] = 0x00;
    servicedata.service_controldatalength = 3;
    
    status = obd2can_txrx(&servicedata);
    
    if(status == S_SUCCESS)
    {
        *count = servicedata.rxdata[0];
        status = S_SUCCESS;
    }
    else
    {
       log_push_error_point(0x302E);
       status = S_FAIL;
    }  

    return status;
}

//------------------------------------------------------------------------------
// Executes a specific sequence of commands before doing an upload or a download.
// Input: u8 part_type
// Output: None
// Return:  Nothing
// Engineer: Ricardo Cigarroa
//------------------------------------------------------------------------------
void obd2can_dcx_initsequence(u8 part_type, u32 ecm_id, u32 response_ecm_id)
{
  
   if((part_type == NGC4A_ECM)|(part_type == NGC4A_PCM)|(part_type == NGC4_PCM)|(part_type == NGC4_ECM))
   {     
       obd2can_dcx_disablenormalcommunication();          
   }
   else //GPEC or other CAN processor
   {
      if(flasherinfo->flashtype == CMDIF_ACT_UPLOAD_STOCK)
      {
         obd2can_dcx_request_byidentifier(0x90,ecm_id, response_ecm_id,1);
         obd2can_dcx_block_IDs(ecm_id, response_ecm_id);
      }
      else //download: either strategy, custom or stock. 
      {
         obd2can_dcx_block_IDs(ecm_id, response_ecm_id);
         obd2can_dcx_disablenormalcommunication();
      }
   }
   
}

//------------------------------------------------------------------------------
//CAN DCX Erase 
//Input: u32 ecm_id
//This function erases the area set up in the finger print area
//Engineer: Ricardo Cigarroa
//------------------------------------------------------------------------------
u8 obd2can_dcx_flash_starter(u32 ecm_id)
{
   u8 status; 
   Obd2can_ServiceData servicedata;
   
   obd2can_servicedata_init(&servicedata);
   servicedata.ecm_id = ecm_id;
   servicedata.service = 0x31;
   servicedata.service_controldatabuffer[0] = 0xDD;
   servicedata.service_controldatabuffer[1] = 0x01;
   servicedata.service_controldatalength = 2;
   servicedata.response_expected = TRUE; 
   servicedata.initial_wait_extended = 3; 
   servicedata.busy_wait_extended = 3; 
   
   status = obd2can_txrx(&servicedata);
   
   if(status != S_SUCCESS)
   {
       log_push_error_point(0x302F);
       status = S_FAIL;
   }
   
   return status;
}

//------------------------------------------------------------------------------
// CAN Fingerprint interlock
// Input: u16 ecm_type
//        bool logical_block_index
// Output: None
// Return:  u8 status
// Engineer: Ricardo Cigarroa
//------------------------------------------------------------------------------
u8 obd2can_dcx_fingerprint_interlock(u16 ecm_type, bool logical_block_index)
{
   u8 status; 
   u32 ecm_id;
   
   ecm_id = ECM_GetEcmId(ecm_type);
   
   obd2can_dcx_flash_starter(ecm_id);
   
   status = obd2can_dcx_fingerprint(ecm_type, ecm_id, logical_block_index);
   
   if(status != S_SUCCESS)
   {
      log_push_error_point(0x3030);
      return S_FAIL;
   }
   
   status = obd2can_dcx_softwareinterlock(ecm_id);
   if(status != S_SUCCESS)
   {
       status = S_FAIL; 
       log_push_error_point(0x3031);
   }
   
   return status; 
}

//------------------------------------------------------------------------------
// CAN Fingerprint interlock - Trans Info
// Input: u32 ecm_type
// Output: None
// Return:  u8 status
// Engineer: Tristen Pierson
//------------------------------------------------------------------------------
u8 obd2can_dcx_fingerprint_interlock_trans_info(u32 ecm_type)
{
   u8 status; 
   u32 ecm_id;
   
   ecm_id = ECM_GetEcmId(ecm_type);
   
   obd2can_dcx_flash_starter(ecm_id);
   
   status = obd2can_dcx_fingerprint_trans_info(ecm_type, ecm_id);
   if(status != S_SUCCESS)
   {
      log_push_error_point(0x3032);
      status = S_FAIL;
   }
   if (status == S_SUCCESS)
   {
       status = obd2can_dcx_softwareinterlock(ecm_id);
       if(status != S_SUCCESS)
       {
           status = S_FAIL; 
           log_push_error_point(0x3033);
       }
   }
   
   return status; 
}

//------------------------------------------------------------------------------
// CAN Fingerprint
// Input: u16 ecm_type
//        bool logical_block_index. If true, begin with 2nd logical block
//        (CAL download). If false, Start with 1st logical block (OS Download).
// Output: None
// Return:  u8 status
// Engineer: Ricardo Cigarroa
//------------------------------------------------------------------------------
u8 obd2can_dcx_fingerprint(u16 ecm_type, u32 ecm_id, bool logical_block_index)
{
   u8 status;
   u8 buffer[12];
   
   u8 NGC4_NGC4A_buffer[12]   = {0x9A, 0x01, 0xFE, 0xBC, 0x13, 0x04, 0x22, 0x00, 0x61, 0x38, 0xC9, 0x00};
   u8 NGC4A_11_buffer[12]     = {0x9A, 0x01, 0x02, 0xBC, 0x13, 0x08, 0x27, 0x00, 0x61, 0x38, 0xDF, 0x00};
   u8 GPEC2_buffer[11]        = {0x9A, 0x01, 0x02, 0xBC, 0x11, 0x12, 0x09, 0x70, 0x31, 0x42, 0x05};
   
   Obd2can_ServiceData servicedata;
   obd2can_servicedata_init(&servicedata);
   servicedata.ecm_id = ecm_id;
   servicedata.service = 0x3B;
   servicedata.response_expected = TRUE; 
   servicedata.initial_wait_extended = 3; 
 
   switch(ecm_type)
   {
      case 0x08:
      case 0x05:
           memcpy((void*)buffer, (void*)NGC4_NGC4A_buffer, 12);
           break;
      case 0x0B:
           memcpy((void*)buffer, (void*)NGC4A_11_buffer, 12);
           break;
      case 0x04:
      case 0x0C:
      case 0x0D:
      case 0x0E:
      case 0x0F:
           if(logical_block_index == FALSE)
           {
               //this byte indicates that the fingerprint for the first logical
               //block will be written. 
               GPEC2_buffer[2] = 0x01; 
           }
           memcpy((void*)buffer, (void*)GPEC2_buffer, 11);
           break;
      default: 
           status = S_FAIL;
           return status; 
     
   }
      
   servicedata.txdata = buffer;
   servicedata.txdatalength = 11;
   
   status = obd2can_txrx(&servicedata);
   if(status != S_SUCCESS)
   {
       log_push_error_point(0x3034);
   }
   
   return status;
}

//------------------------------------------------------------------------------
// CAN Fingerprint - Trans Info
// Input: u32 ecm_type
//        u32 ecm_id
// Output: None
// Return:  u8 status
// Engineer: Tristen Pierson
//------------------------------------------------------------------------------
u8 obd2can_dcx_fingerprint_trans_info(u32 ecm_type, u32 ecm_id)
{
    u8 status;
    u8 buffer[12] = {0x9B, 0x01, 0x01, 0xBA, 0x12, 0x05, 0x06, 0x02, 0x14, 0x53, 0x08};
    
    Obd2can_ServiceData servicedata;
    obd2can_servicedata_init(&servicedata);
    servicedata.ecm_id = ecm_id;
    servicedata.service = 0x3B;      
    servicedata.txdata = buffer;
    servicedata.txdatalength = 12;
   
    status = obd2can_txrx(&servicedata);
    if(status != S_SUCCESS)
    {
        log_push_error_point(0x3035);
    }
   
    return status; 
}

//------------------------------------------------------------------------------
// CAN software interlock
// Input: u32 ecm_id
// Output: None
// Return:  u8 status
// Software interlock (where to copy, small sw from boot, address setup)
// Engineer: Ricardo Cigarroa
//------------------------------------------------------------------------------
u8 obd2can_dcx_softwareinterlock(u32 ecm_id)
{
   u8 status; 
   u8 request_buffer[10]   = {0x81,0x7B,0xF0,0x00,0x00,0x00,0x08,0x00};
   u8 transfer_buffer[10]  = {0xA1,0xA1,0xB2,0xB2,0xC3,0xC3,0x13,0x14};
   Obd2can_ServiceData servicedata;
   
   
   obd2can_servicedata_init(&servicedata);
   servicedata.ecm_id = ecm_id;
   servicedata.service = 0x34;
   servicedata.txdata = request_buffer;
   servicedata.txdatalength = 7;
   
   status = obd2can_txrx(&servicedata);
   
   if(status == S_SUCCESS)
   {
      obd2can_servicedata_init(&servicedata);
      servicedata.ecm_id = ecm_id;
      servicedata.service = 0x36;
      servicedata.txdata = transfer_buffer;
      servicedata.txdatalength = 8;
      
      status = obd2can_txrx(&servicedata);
      
      if(status == S_SUCCESS)
      {
         obd2can_servicedata_init(&servicedata);
         servicedata.ecm_id = ecm_id;
         servicedata.service = 0x37;
         
         status = obd2can_txrx(&servicedata);
      }
      else
      {
         status = S_FAIL;
      }
   }
   else
   {
      status = S_FAIL;
   }
   
   return status; 
}

//------------------------------------------------------------------------------
// TransferData Download utility ($36)
// Inputs:  u32 ecm_id
//          Obd2can_SubServiceNumber subservice (UNUSED)
//          u8  blocktracking
//          u32 startingaddr
//          MemoryAddressType addrtype
//          u8  *data
//          u16 datalength
//          RequestLengthType reqtype
//          VehicleCommLevel commlevel
//          bool response_expected
// Return:  u8  status
// Engineer: Ricardo Cigarroa
//------------------------------------------------------------------------------
u8 obd2can_dcx_transfer_data_download_utility(u32 ecm_id,
                                       Obd2can_SubServiceNumber subservice,
                                       u8 blocktracking, u32 startingaddr,
                                       MemoryAddressType addrtype,
                                       u8 *data, u16 datalength,
                                       VehicleCommLevel commlevel,
                                       bool response_expected)
{
     Obd2can_ServiceData servicedata;
     u8 status;

     obd2can_servicedata_init(&servicedata);
     servicedata.ecm_id = ecm_id;
     servicedata.service = DCX_TransferData;
     servicedata.txdata = data;
     servicedata.txdatalength = datalength;
     servicedata.response_expected = response_expected;
     servicedata.initial_wait_extended = 3; 
     servicedata.commlevel = commlevel; 
     
     if (addrtype == Address_32_Bits)
     {
         
     }
     else
     {
         return S_BADCONTENT;
     }

     if (commlevel == CommLevel_ISO14229)
     {
         servicedata.tester_present_type = TESTER_PRESENT_BROADCAST;
     }
     else
     { 
         //do nothing for now
     }
    
     status = obd2can_txrx(&servicedata);
     if(status != S_SUCCESS)
     {
         log_push_error_point(0x3036);
     }
     return status;
}

//------------------------------------------------------------------------------
// Upload E2 data 
// Inputs:  u32 ecm_type
// Return:  u8  status
// Engineer: Ricardo Cigarroa
//------------------------------------------------------------------------------
u8 obd2can_dcx_upload_e2_data(u32 ecm_type)
{
    u8  status; 
    u8  *readbuffer;
    u8  e2_index; 
    u8  e2choice;
    u32 readbufferindex; 
    u32 readlength; 
    u32 identifier; 
    u32 bytecount;
    F_FILE *file_E2 = NULL; 
    ESS_HEADER *essheader = NULL;   
    ESS_BLOCK  *essblock = NULL;
    
   
    readbuffer = __malloc(MAX_UPLOAD_BUFFER_LENGTH);
    if (readbuffer == NULL)
    {
       log_push_error_point(0x3037);
       status = S_MALLOC;
       goto obd2can_dcx_upload_e2_data_done;
    }
    essheader = __malloc(sizeof(ESS_HEADER)); 
    if (essheader == NULL)
    {
        log_push_error_point(0x3038);
        status = S_MALLOC;
        goto obd2can_dcx_upload_e2_data_done; 
    }
    essblock =  __malloc(sizeof(ESS_BLOCK));
    if(essblock == NULL)
    {
        log_push_error_point(0x3039);
        status = S_MALLOC;
        goto obd2can_dcx_upload_e2_data_done;
    }
     
    readbufferindex = 0;
    
    //Delete E2_stock.bin file if it exists
    status = genfs_deletefile(STOCK_E2_FILENAME);
    
    //Create/open E2_stock. bin file
    file_E2 = genfs_user_openfile(STOCK_E2_FILENAME,"ab+");
    if(file_E2 == NULL)
    {
       log_push_error_point(0x303A);
       status = S_OPENFILE;
       goto obd2can_dcx_upload_e2_data_done; 
    }
    
    //set default values to E2_Stock.ess file. 
    status = e2_stock_createfile(file_E2, essheader, essblock); 
    if(status != S_SUCCESS)
    {
        log_push_error_point(0x303B);
        status = S_FAIL;
        goto obd2can_dcx_upload_e2_data_done;
    }
    
    //skip header and type of E2.
    if(fseek(file_E2,sizeof(ESS_HEADER),SEEK_SET) != 0)
    {
        log_push_error_point(0x303C);
        status = S_SEEKFILE;
        goto obd2can_dcx_upload_e2_data_done;
    }
    
    e2choice = E2_GetEcmIndex(ecm_type);
     
    //Get E2 data
    for(e2_index = 0; e2_index<MAX_E2_BLOCKS; e2_index++)
    {
       identifier = E2_GetE2Identifier(e2choice, e2_index);
    
       if(identifier == 0)
       {
          break;
       }
       else
       {
          status = obd2can_dcx_read_local_identifier(ECM_GetEcmId(ecm_type), NULL,identifier,
                                                     &readbuffer[readbufferindex],&readlength);
          if(status == S_SUCCESS)
          {
             readlength = (readlength/8) * 8;
             bytecount = fwrite(readbuffer,1,readlength, file_E2);
             
             if(bytecount != readlength)
             {
                log_push_error_point(0x303D);
                status = S_WRITEFILE;
                goto obd2can_dcx_upload_e2_data_done;
             }
             
             readbufferindex = 0;
             essheader->e2blockcount++;
          }
          else
          {
             log_push_error_point(0x303E);
             status = S_FAIL; 
             goto obd2can_dcx_upload_e2_data_done; 
          }
       }
    }
    
    //update e2 file header with the block count; 
    if(fseek(file_E2,0x0C,SEEK_SET) != 0)
    {
        log_push_error_point(0x303F);
        status = S_SEEKFILE;
        goto obd2can_dcx_upload_e2_data_done;  
    }
    
    bytecount = 0; 
    bytecount = fwrite(&essheader->e2blockcount,1,1,file_E2); 
    if(bytecount != 1)
    {
        log_push_error_point(0x3040);
        status = S_FAIL;
        goto obd2can_dcx_upload_e2_data_done;
    }
    
    //encrypt the e2_stock.ess file
    status = e2_stock_encryptfile(file_E2, essheader);
    if(status != S_SUCCESS)
    {
        log_push_error_point(0x3041);
        status = S_FAIL;
        goto obd2can_dcx_upload_e2_data_done;
    }
            
obd2can_dcx_upload_e2_data_done: 
     //Close file
     if(file_E2)
     {
        genfs_closefile(file_E2);
     }
     //free memory
     if(readbuffer)
     {
        __free(readbuffer);
     }
     if(essheader)
     {
        __free(essheader);
     }
     if(essblock)
     {
        __free(essblock);
     }
     return status; 
  
}

//------------------------------------------------------------------------------
// Download E2 data 
// Inputs:  u32 ecm_type
// Return:  u8  status
// Engineer: Ricardo Cigarroa
//------------------------------------------------------------------------------
u8 obd2can_dcx_download_e2_data(u32 ecm_type, u8 modify_E2)
{
   u8 status; 
   u8 *writebuffer;
   u8 *temp_buff;
   u8 e2_index;
   u8 write_modified_data;
   u8 e2choice;
   u8 modified_bytes[2] = {0,0};  
   u32 writebufferindex = 0; 
   u32 bufferlength;  
   u32 block_size; 
   u32 actual_byte_count;
   u32 identifier; 
   F_FILE *file_E2 = NULL;   
   
   temp_buff = __malloc(MAX_DOWNLOAD_BUFFER_LENGTH);
   if(temp_buff == NULL)
   {
       log_push_error_point(0x3042);
       status = S_MALLOC;
       goto obd2can_dcx_download_e2_data_done; 
   }
   
   //Open E2_stock. bin file
   file_E2 = genfs_user_openfile(STOCK_E2_FILENAME,"rb");
   if(file_E2 == NULL)
   {
       log_push_error_point(0x3043);
       status = S_OPENFILE;
       goto obd2can_dcx_download_e2_data_done; 
   }
      

   if (fseek(file_E2,0,SEEK_END) != 0)
   {
       log_push_error_point(0x3043);
       status = S_SEEKFILE;
       goto obd2can_dcx_download_e2_data_done;
   }

   bufferlength = ftell(file_E2);       // need to find the beginning of the E2
   bufferlength = bufferlength - 0x1000;// found issues with header size

   if(fseek(file_E2,(bufferlength),SEEK_SET) != 0)
   {
       log_push_error_point(0x3044);
       status = S_SEEKFILE;
       goto obd2can_dcx_download_e2_data_done;
   }
    
   e2choice = E2_GetEcmIndex(ecm_type);
   write_modified_data = E2_GetE2DataFlag(e2choice);
   
   //Read E2_stock.bin file. 
   for(e2_index = 0; e2_index<MAX_E2_BLOCKS; e2_index++)
   {
       writebuffer = (u8*)temp_buff;
       block_size = E2_GetE2BlockSize(e2choice, e2_index);
       actual_byte_count = block_size & 0xFFF0;
       identifier = E2_GetE2Identifier(e2choice, e2_index);
             
       if(identifier == 0)
       {
           break;
       }
       else
       {
           actual_byte_count = (actual_byte_count/8) * 8;
           bufferlength = fread((char*)writebuffer,
                                1,actual_byte_count,file_E2);
                              
           if(bufferlength == 0)
           {
               log_push_error_point(0x3045);
               status = S_FAIL; 
               goto obd2can_dcx_download_e2_data_done;
           }
         
           crypto_blowfish_decryptblock_critical(writebuffer, bufferlength);   
         
           //TODO: make E2_FLAG_MODIFY_DATA into a block flag and add offset. 
           if((write_modified_data == E2_FLAG_MODIFY_DATA )&&(modify_E2 == 1))
           {
               if((e2_index == 0)&&((settingstune.variant_ID == 0x23)|(settingstune.variant_ID == 0x25)))//IsVariantId_DCX_GPEC2
               {
                   modified_bytes[0] = E2_GetModifyDataBytes(e2choice, 0);
                   modified_bytes[1] = E2_GetModifyDataBytes(e2choice, 1);
                   writebuffer = writebuffer + 0x210; 
                   *writebuffer++ = modified_bytes[0];
                   *writebuffer = modified_bytes[1];  
               }
               else if((e2_index == 1)&&((settingstune.variant_ID == 0x26)||(settingstune.variant_ID == 0x44)))
               {
                    writebuffer = writebuffer +  0x0A;
                    *writebuffer = 0x47;
               }
           }

           writebuffer = (u8*)temp_buff;
             
           //send command to write data. 
           status = obd2can_dcx_write_data_by_local_id(ECM_GetEcmId(ecm_type),identifier,
                                                       &writebuffer[writebufferindex],bufferlength);
           if(status != S_SUCCESS)    
           {
               log_push_error_point(0x3046);
               status = S_FAIL;
               goto obd2can_dcx_download_e2_data_done; 
           }
         
           writebufferindex = 0;      
       }
   }
     
obd2can_dcx_download_e2_data_done:
    //Close file
    if(file_E2)
    {
        genfs_closefile(file_E2);
    }
    //free malloc
    if(temp_buff)
    {
        __free(temp_buff);
    }
     
    return status;  
  
}

//------------------------------------------------------------------------------
// obd2can_dcx_read_tire_axel_parameters 
// Inputs:  u8 *WA580 (transmission there?), Obd2can_ServiceData contactdata
// Return:  u8  status
// Purpose: To read the tire size and axle ratio of the vehicle.
// Engineer: Rhonda Rusak
//------------------------------------------------------------------------------
u8 obd2can_dcx_read_tire_axel_parameters(u8 *WA580, Obd2can_ServiceData contactdata, u8 *diag_type)
{
    u8 status;


    // Find Diagnostic type
    *diag_type = obd2can_dcx_find_DiagType();
    if(contactdata.flowcontrol_blocksize == 0xFF)
      return S_FAIL;
    
    *WA580 = obd2can_dcx_WA580_transType();

    if(*diag_type == 1)
        status = obd2can_dcx_read_tire_axle_typeTIPMCGW(contactdata.rxdata);
    else
        status = obd2can_dcx_read_tire_axle_typeBCM(contactdata.rxdata);

    return status; 
}
//------------------------------------------------------------------------------
// obd2can_dcx_write_tire_axel_parameters 
// Inputs:  u8 WA580 (transmission there?), Obd2can_ServiceData contactdata
// Return:  u8  status
// Purpose: To write the tire size and axle ratio of the vehicle.
// Engineer: Rhonda Rusak
//------------------------------------------------------------------------------
u8 obd2can_dcx_write_tire_axel_parameters(CMDIF_COMMAND cmd, const u8 *title,
                                          u8 WA580, u8 *data, u8 diag_type)
{
    u8 status;


    obd2can_init(Obd2canInitCustom,0x504);

    if(diag_type == 1)
        status = obd2can_dcx_write_tire_axle_typeTIPMCGW(cmd, title, WA580, data);
    else
        status = obd2can_dcx_write_tire_axle_typeBCM(cmd, title, WA580, data);

    return status; 
}
//------------------------------------------------------------------------------
// obd2can_dcx_find_DiagType 
// Inputs:  None
// Return:  u8  status
// Purpose: Find the diagnostic type (Diag level for module: TIPMCGW or BCM)
// Engineer: Rhonda Rusak
//------------------------------------------------------------------------------
u8 obd2can_dcx_find_DiagType(void)
{
  u8 status = 0;
 
  obd2can_init(Obd2canInitCustom,0x504);                   
 
  status = obd2can_dcx_initiate_diagnostic_operation(FALSE,0x620, 0x504, 
                                          diagnosticRequestChrysler_03);
  if(status == S_SUCCESS)
    return 2; // BCM

  status = obd2can_dcx_initiate_diagnostic_operation(FALSE,0x620, 0x504, 
                                          diagnosticRequestChrysler_92);
  if(status == S_SUCCESS)
    return 1; // TIPMCGW

  return 0xFF; // Unknown

}
//------------------------------------------------------------------------------
// obd2can_dcx_WA580_transType 
// Inputs:  None
// Return:  u8  status - Trans is or is not WA580 type
// Purpose: Find the if the WA580 transmission is present (requires reset of module)
// Engineer: Rhonda Rusak
//------------------------------------------------------------------------------
u8 obd2can_dcx_WA580_transType(void)
{
  u8 buffer[50];
  u32 bytecount = 0;

  obd2can_init(Obd2canInitNormal,0);
  memset((void*)buffer, 0, 50);


  if((obd2can_dcx_read_local_identifier(0x7E0, NULL, 0xEA, buffer, &bytecount)) == S_SUCCESS)
  {
      if((buffer[1] & 0x40) == 0x40)            // Bit 6 marks WA580 transmission is there or not
        return 1;
      else
        return 0;
  }

  return 0;
}
//------------------------------------------------------------------------------
// obd2can_dcx_read_tire_axle_typeTIPMCGW (1)
// Inputs:  u8 buffer
// Return:  u8  status
// Purpose: To read the tire size and axle ratio of the TIPMCGW vehicle.
// Engineer: Rhonda Rusak
//------------------------------------------------------------------------------
u8 obd2can_dcx_read_tire_axle_typeTIPMCGW(u8 *buffer)
{
    u8 status = S_FAIL,i;
    u8 *buffer_ptr;
    u32 bytecount = 0;
    

    obd2can_init(Obd2canInitCustom,0x504);


    for(i=0; i<3; i++)
    {
        buffer_ptr = buffer;

        obd2can_dcx_initiate_diagnostic_operation(FALSE,0x620, 0x504, diagnosticRequestChrysler_92);

        status = obd2can_dcx_read_local_identifier(0x620, 0x504, 0x0C, buffer_ptr, &bytecount);
        if (status == S_SUCCESS)
          break;

        delays(30,'m');
    }


    return status;
}
//------------------------------------------------------------------------------
// obd2can_dcx_read_tire_axle_typeBCM (2)
// Inputs:  u8 buffer
// Return:  u8  status
// Purpose: To read the tire size and axle ratio of the BCM vehicle.
// Engineer: Rhonda Rusak
//------------------------------------------------------------------------------
u8 obd2can_dcx_read_tire_axle_typeBCM(u8 *buffer)
{
    u8 status = 0;
    u16 bytecount = 0;
    u8 i;
    u8 *buffer_ptr;

    obd2can_init(Obd2canInitCustom,0x504); 

    for(i=0; i<3; i++)
    {
        buffer_ptr = buffer;

        status = obd2can_dcx_ReadDataByParameterIdentifier(0x620, 0x504, 0x01,0x25, buffer_ptr, &bytecount);
        if (status == S_SUCCESS)
          break;

        delays(30,'m');
        obd2can_dcx_initiate_diagnostic_operation(FALSE,0x620, 0x504, diagnosticRequestChrysler_03);
    }

    return status;
}
//------------------------------------------------------------------------------
// obd2can_dcx_write_tire_axle_typeTIPMCGW (1)
// Inputs:  u8 buffer
// Return:  u8  status
// Purpose: To write the tire size and axle ratio of the TIPMCGW vehicle.
// Engineer: Rhonda Rusak
//------------------------------------------------------------------------------
u8 obd2can_dcx_write_tire_axle_typeTIPMCGW(CMDIF_COMMAND cmd, const u8 *title, 
                                           u8 WA580, u8 * data_buffer)
{
    u8 status = S_FAIL;
    u8 *buffer_ptr;
    u8 data[30],i;

    memset(data,0,30);

    for(i=0; i<3; i++)
    {
        buffer_ptr = data_buffer;

        obd2can_dcx_initiate_diagnostic_operation(FALSE,0x620, 0x504, diagnosticRequestChrysler_92);

        status = obd2can_dcx_write_data_by_localID(0x620, 0x504,0x0C, buffer_ptr,19);
        if (status == S_SUCCESS)
          break;

        delays(30,'m');
    }


    delays(1000, 'm');

    cmdif_remotegui_msg_keyoff(cmd, title);
     
    cmdif_remotegui_messagebox(cmd,
                        title, CMDIF_REMOTEGUI_ICON_NONE,
                        CMDIF_REMOTEGUI_BUTTONFACE_None, CMDIF_REMOTEGUI_BUTTONFACE_None, CMDIF_REMOTEGUI_BUTTONFACE_None,
                        CMDIF_REMOTEGUI_FOCUS_None,
                        "<H>TIRE SIZE & AXLE RATIO\nPowering down, please wait...");



    if(WA580 == 1)
      obd2can_dcx_initializeEGS(cmd, title);//Only for NAC1/WA580 transmission 5 speed tranny
    else
    {
      delays(9000, 'm');
      cmdif_remotegui_msg_keyon(cmd, title);

      cmdif_remotegui_messagebox(cmd,
                        title, CMDIF_REMOTEGUI_ICON_NONE,
                        CMDIF_REMOTEGUI_BUTTONFACE_None, CMDIF_REMOTEGUI_BUTTONFACE_None, CMDIF_REMOTEGUI_BUTTONFACE_None,
                        CMDIF_REMOTEGUI_FOCUS_None,
                        "<H>TIRE SIZE & AXLE RATIO\nPowering down, please wait...");
    }
    


    obd2can_dcx_initiate_diagnostic_operation(FALSE,0x620, 0x504, defaultSession_DCX);       

    
    if(obd2can_dcx_read_tire_axle_typeTIPMCGW(data) == S_SUCCESS)
    {
      if((data[7] == buffer_ptr[7])&&(data[8] == buffer_ptr[8]))
          status = S_SUCCESS;
      else
          status = S_FAIL;
    }

    obd2can_init(Obd2canInitNormal,0);
    obd2can_dcx_cleardtc();
    
    return status;
}
//------------------------------------------------------------------------------
// obd2can_dcx_write_tire_axle_typeBCM (2)
// Inputs:  u8 buffer
// Return:  u8  status
// Purpose: To write the tire size and axle ratio of the BCM vehicle.
// Engineer: Rhonda Rusak
//------------------------------------------------------------------------------
u8 obd2can_dcx_write_tire_axle_typeBCM(CMDIF_COMMAND cmd, const u8 *title, 
                                       u8 WA580, u8 * data_buffer)
{
    u8 status = S_FAIL;
    u8 *buffer_ptr;
    u8 data[50],i;

    memset(data,0,50);

    data[0] = 0x25;
    memcpy((void*)&data[1], (void*)data_buffer,9);


    for(i=0; i<3; i++)
    {
        buffer_ptr = data;

        obd2can_dcx_initiate_diagnostic_operation(FALSE,0x620, 0x504, diagnosticRequestChrysler_03);

        status = obd2can_dcx_WriteDataByParameterIdentifier(0x620, 0x504, 0x01, buffer_ptr, 9);
        if (status == S_SUCCESS)
          break;

        delays(30,'m');
    }

    cmdif_remotegui_msg_keyoff(cmd, title);
     
    cmdif_remotegui_messagebox(cmd,
                        title, CMDIF_REMOTEGUI_ICON_NONE,
                        CMDIF_REMOTEGUI_BUTTONFACE_None, CMDIF_REMOTEGUI_BUTTONFACE_None, CMDIF_REMOTEGUI_BUTTONFACE_None,
                        CMDIF_REMOTEGUI_FOCUS_None,
                        "<H>TIRE SIZE & AXLE RATIO\nPowering down, please wait...");
    if(WA580 == 1)
      obd2can_dcx_initializeEGS(cmd, title);//Only for NAC1/WA580 transmission 5 speed tranny
    else
    {
      delays(9000, 'm');
      cmdif_remotegui_msg_keyon(cmd, title);

      cmdif_remotegui_messagebox(cmd,
                        title, CMDIF_REMOTEGUI_ICON_NONE,
                        CMDIF_REMOTEGUI_BUTTONFACE_None, CMDIF_REMOTEGUI_BUTTONFACE_None, CMDIF_REMOTEGUI_BUTTONFACE_None,
                        CMDIF_REMOTEGUI_FOCUS_None,
                        "<H>TIRE SIZE & AXLE RATIO\nProcessing...");
    }
    
    obd2can_dcx_initializeABS(cmd, title);

    obd2can_init(Obd2canInitNormal,0);
    obd2can_dcx_cleardtc();
    
    if(obd2can_dcx_read_tire_axle_typeBCM(data) == S_SUCCESS)
    {
      if((data[7] == buffer_ptr[7])&&(data[8] == buffer_ptr[8]))
          status = S_SUCCESS;
      else
          status = S_FAIL;
    }

    obd2can_init(Obd2canInitNormal,0);
    obd2can_dcx_cleardtc();

    return status;
}

void obd2can_dcx_initializeEGS(CMDIF_COMMAND cmd, const u8 *title)
{
    u8 status = 0;
    u8 buffer[45];
    u8 DCT_count = 0,i;
    u32 bytecount = 0;
    signed int timeout = 0xFFFF;

    unsigned char command3bb1[0x30] = {	0x6c,0x7a,0x40,0x40,0x00,0x00,0x00,0x00,0x00,0x00,		//States 1-10
					0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,		//States 11-20
					0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,		//States 21-30
					0x00,0x04,0x00,0x00,0x00,0x00,0x00,0x04,0x01,0x00,		//States 31-40
					0xc6,0xd8,												//Intel CRC
					0x18,0x42,0x88,0xf7};						//Tester ID

    obd2can_init(Obd2canInitNormal,0);

    delays(20000, 'm');


    //Part 1
    cmdif_remotegui_msg_keyon(cmd, title);

    cmdif_remotegui_messagebox(cmd,
                        title, CMDIF_REMOTEGUI_ICON_NONE,
                        CMDIF_REMOTEGUI_BUTTONFACE_None, CMDIF_REMOTEGUI_BUTTONFACE_None, CMDIF_REMOTEGUI_BUTTONFACE_None,
                        CMDIF_REMOTEGUI_FOCUS_None,
                        "<H>TIRE SIZE & AXLE RATIO\nProcessing...");

    obd2can_dcx_read_local_identifier(0x7E1, NULL, 0x34, buffer, &bytecount);
    delays(85,'m');
    obd2can_dcx_read_local_identifier(0x7E1, NULL, 0xB2, buffer, &bytecount);
    delays(85,'m');


    buffer[0] = 0x10;
    buffer[1] = 0;
    for(i=0; i< 4; i++)
    {
        status = obd2can_dcx_write_data_by_localID(0x7E1, NULL, 0xB2, buffer, 1);
        if(status == S_SUCCESS)
          break;

        delays(85,'m');
        obd2can_dcx_initiate_diagnostic_operation(FALSE,0x7E1, 0x7E9, diagnosticRequestChrysler_92);
    }
      
    obd2can_dcx_read_local_identifier(0x7E1, NULL, 0xB2, buffer, &bytecount);
    delays(73,'m');
    obd2can_dcx_write_data_by_localID(0x7E1, NULL, 0xB1, command3bb1, 46);
    delays(80,'m');
    obd2can_dcx_read_local_identifier(0x7E1, NULL, 0xB2, buffer, &bytecount);
    delays(5,'m');
    buffer[0] = 0x11;
    buffer[1] = 0;
    obd2can_dcx_write_data_by_localID(0x7E1, NULL, 0xB2, buffer, 1);
    delays(20,'m');
    obd2can_dcx_read_local_identifier(0x7E1, NULL, 0xB2, buffer, &bytecount);

    // Part 2
    cmdif_remotegui_msg_keycycle(CMDIF_CMD_SPECIAL_FUNCTIONS,"SPECIAL FUNCTIONS");

    cmdif_remotegui_messagebox(cmd,
                        title, CMDIF_REMOTEGUI_ICON_NONE,
                        CMDIF_REMOTEGUI_BUTTONFACE_None, CMDIF_REMOTEGUI_BUTTONFACE_None, CMDIF_REMOTEGUI_BUTTONFACE_None,
                        CMDIF_REMOTEGUI_FOCUS_None,
                        "<H>TIRE SIZE & AXLE RATIO\nProcessing...");

    delays(20000,'m');
    status = S_FAIL;

    while(timeout--)
    {
      status = obd2can_dcx_clear_diagnostic_information_extended(0x7E1, NULL);
      if(status == S_SUCCESS)
          break;
      
    }

    status = S_FAIL;
    timeout = 0xFFFF;
    delays(85,'m');
    while(timeout--)
    {
      status = obd2can_dcx_testerpresent(FALSE,0x7E1,CommLevel_Unknown);
      if(status == S_SUCCESS)
          break;
      
    }

    if(status == S_FAIL)
      goto EGS_INTIT_DONE;

    status = S_FAIL;
    timeout = 0xFFFF;

    while(timeout--)
    {
      status = obd2can_dcx_getdtccount(&DCT_count, 0x7E1);
       if(status == S_SUCCESS)
          break;

       delays(85,'m');
    }


    delays(60,'m');
    obd2can_dcx_read_local_identifier(0x7E1, NULL, 0xB2, buffer, &bytecount);


    buffer[0] = 0x10;
    buffer[1] = 0;
   
    for(i=0; i< 4; i++)
    {
        status = obd2can_dcx_write_data_by_localID(0x7E1, NULL, 0xB2, buffer, 1);
        if(status == S_SUCCESS)
          break;

        delays(10,'m');
        obd2can_dcx_initiate_diagnostic_operation(FALSE,0x7E1, 0x7E9, diagnosticRequestChrysler_92);
        delays(10,'m');
    }

    delays(20,'m');
    obd2can_dcx_read_local_identifier(0x7E1, NULL, 0xB2, buffer, &bytecount);

    // Part 3
    cmdif_remotegui_msg_keycycle(cmd, title);

   cmdif_remotegui_messagebox(cmd,
                        title, CMDIF_REMOTEGUI_ICON_NONE,
                        CMDIF_REMOTEGUI_BUTTONFACE_None, CMDIF_REMOTEGUI_BUTTONFACE_None, CMDIF_REMOTEGUI_BUTTONFACE_None,
                        CMDIF_REMOTEGUI_FOCUS_None,
                        "<H>TIRE SIZE & AXLE RATIO\nProcessing...");

    delays(30,'m');

    obd2can_dcx_read_local_identifier(0x7E1, NULL, 0xB0, buffer, &bytecount);
    delays(85,'m');
    obd2can_dcx_clear_diagnostic_information_extended(0x7E1, NULL);

EGS_INTIT_DONE:

    return;
}

void obd2can_dcx_initializeABS(CMDIF_COMMAND cmd, const u8 *title)
{
    u8 test_data[7] = {0x30,0x51,0x00,0x00,0x00,0x00,0x00};
    u8 i = 0;
    u8 rxdata[10];
    u8 status = 0;
    u32 node_id = 0x747;
    u32 response_id = 0x4C7;
    u16 bytecount = 0;
    signed int timeout = 0x20;


    memset(rxdata, 0, 10);

    if(obd2can_dcx__chekcomputer(node_id, response_id) == 2) //check for UDS computer.
    {
      cmdif_remotegui_messagebox(cmd,
                        title, CMDIF_REMOTEGUI_ICON_NONE,
                        CMDIF_REMOTEGUI_BUTTONFACE_None, CMDIF_REMOTEGUI_BUTTONFACE_None, CMDIF_REMOTEGUI_BUTTONFACE_None,
                        CMDIF_REMOTEGUI_FOCUS_None,
                        "Please wait, this could take some time.");

        obd2can_init(Obd2canInitCustom,response_id);

        for(i=0; i<3; i++)
        {
            while(timeout--)    //31 01 30 51 00 00 00 --Static ECU Initialization
            {
                status = obd2can_dcx_start_routine_by_local_id_tx(node_id, response_id, 0x01,0,test_data, 2);
                if(status != S_SUCCESS)
                {                                                               // wrong diagnostic mode
                    obd2can_dcx_initiate_diagnostic_operation(FALSE,node_id, response_id, diagnosticRequestChrysler_03);
                    status = obd2can_dcx_start_routine_by_local_id_tx(node_id, response_id, 0x01,0,test_data, 2);
                }
                if(status == S_SUCCESS)
                    break;
            }
            timeout = 0x20;
            delays(1500, 'm');
        }

        
                            //31 01 30 51 00 00 00 -- read ECU Configuration
        obd2can_dcx_ReadDataByParameterIdentifier(node_id, response_id, 0xF1,0x0B, rxdata, &bytecount);

        test_data[1] = 0x20;
        test_data[2] = 0x02;
        timeout = 0x20;


        while(timeout--)        //Sensor Cluster Calibration ,Delete SC fault Memory NO, Calibrate AX-Sensor YES
        {                       //31 01 30 20 02 00 00
            status = obd2can_dcx_start_routine_by_local_id_tx(node_id, response_id, 0x01,0, test_data, 3);
            if(status != S_SUCCESS)
            {                                                               // wrong diagnostic mode
              obd2can_dcx_initiate_diagnostic_operation(FALSE,node_id, response_id, diagnosticRequestChrysler_03);
              status = obd2can_dcx_start_routine_by_local_id_tx(node_id, response_id, 0x01,0,test_data, 3);
            }

            if(status == S_SUCCESS)
                break;
        }

        test_data[2] = 0x00;
        timeout = 0x20;

        while(timeout--)        //31 03 30 20 00 00 00  Sensor Cluster Calibration Related...
        {
            status = obd2can_dcx_start_routine_by_local_id_tx(node_id, response_id, 0x03,0, test_data, 2);
            if(status != S_SUCCESS)
            {                                                               // wrong diagnostic mode
              obd2can_dcx_initiate_diagnostic_operation(FALSE,node_id, response_id, diagnosticRequestChrysler_03);
              status = obd2can_dcx_start_routine_by_local_id_tx(node_id, response_id, 0x01,0, test_data, 3);
            }


            if(status == S_SUCCESS)
                break;
        }

        timeout = 0x20;

        while(timeout--)        //14 FF FF FF 00 00 00  Clear DTC's
        {
            status = obd2can_dcx_clear_diagnostic_information_iso14229(FALSE, node_id, response_id);
            if(status == S_SUCCESS)
                break;  
        }

        obd2can_init(Obd2canInitNormal,0);
        delays(30,'m');
        obd2can_dcx_initiate_diagnostic_operation(TRUE,0x7DF, NULL, diagnosticRequestChrysler_92);
        delays(60,'m');
        obd2can_dcx_initiate_diagnostic_operation(FALSE,0x01C, NULL, diagnosticRequestChrysler_92);

        test_data[0] = 0xFF;
        test_data[1] = 0x00;
        test_data[2] = 0x02;
        delays(60,'m');

        obdcan_dcx_sendcommand(0x7DF, 0x7E8, test_data, DCX_Fault_control, disableAllDTCs, 3, TRUE);
        delays(60,'m');

        obdcan_dcx_sendcommand(0x01C, NULL, test_data, DCX_Fault_control, disableAllDTCs, 3, FALSE);
        delays(30,'m');

        memset((void*)test_data,0, 7);
        obd2can_init(Obd2canInitCustom,response_id);

        for(i=0; i<2; i++)
        {
          obd2can_dcx_initiate_diagnostic_operation(FALSE,0x441, response_id, diagnosticRequestChrysler_83);

          obdcan_dcx_sendcommand(0x441, response_id, test_data, DCX_Fault_control, disableAllDTCs, 0, FALSE); // 0x85, 0x02
          delays(2500,'m');
        }

        
        timeout = 0x20;

        while(timeout--)
        {
            status = obd2can_dcx_ecu_reset_request(FALSE,node_id, response_id, hardReset,TRUE);
            if(status == S_SUCCESS)
                break;
            delays(100, 'm');
        }


        obd2can_init(Obd2canInitNormal,0);
        delays(60,'m');
        obd2can_dcx_initiate_diagnostic_operation(TRUE,0x7DF, NULL, diagnosticRequestChrysler_92);
        delays(60,'m');
        obd2can_dcx_initiate_diagnostic_operation(FALSE,0x01C, NULL, diagnosticRequestChrysler_92);

        test_data[0] = 0xFF;
        test_data[1] = 0x00;
        test_data[2] = 0x01;
        delays(60,'m');

        obdcan_dcx_sendcommand(0x7DF, 0x7E8, test_data, DCX_Fault_control, disableAllDTCs, 3, TRUE);
        delays(60,'m');

        obdcan_dcx_sendcommand(0x01C, NULL, test_data, DCX_Fault_control, disableAllDTCs, 3, FALSE);
        delays(30,'m');

        memset((void*)test_data,0, 7);

        obd2can_init(Obd2canInitCustom,response_id);

        for(i=0; i<2; i++)
        {
          obd2can_dcx_initiate_diagnostic_operation(FALSE,0x441, response_id, diagnosticRequestChrysler_83);

          obdcan_dcx_sendcommand(0x441, response_id, test_data, DCX_Fault_control, enableAllDTCs, 0, FALSE); // 0x85, 0x01
          delays(2500,'m');
        }
    }
}
u8 obd2can_dcx__chekcomputer(u32 ecm_id, u32 ecm_response_id)
{
  signed int timeout = 20;
  signed int timer = 0x3F;
  u8 status = S_FAIL;
  u8 buffer[20];
  u8 *buf_ptr;
  Obd2can_ServiceData servicedata;


  memset((void*)buffer, 0, 20);
  obd2can_servicedata_init(&servicedata);
  obd2can_init(Obd2canInitCustom,ecm_response_id);

  buf_ptr = buffer;
  
  servicedata.ecm_id = ecm_id;
  servicedata.response_id = ecm_response_id;
  servicedata.service = DCX_InitiateDiagnosticOperation;
  servicedata.subservice = diagnosticRequestChrysler_92;
  servicedata.response_expected = TRUE;
  servicedata.nowait_cantx = FALSE;
  servicedata.rx_timeout_ms = 5000;
  servicedata.rxdata = buf_ptr;


  while(timeout--)
  {

      while(status == S_FAIL)
      {
        status = obd2can_txrx(&servicedata);
        if (status == S_ERROR)
        {
            status = S_SUCCESS; 
        }
        if(timer <= 0)
          return 0;
        else
          timer--;
      }

      if((buffer[0] == 0x92)&&(status == S_SUCCESS))
      {
        return 1;                 // It's kwp2000
      }
      else if((buffer[0] == 0x7F) && (buffer[1] == 0x10)&&
              (servicedata.subservice == diagnosticRequestChrysler_92))
      {
        servicedata.subservice = diagnosticRequestChrysler_03;
	timeout = 20;                   // Reset timeout
      }
      else if((buffer[0] == 0x03)&&(status == S_SUCCESS))
      {
	return 2;                   // It's UDS
      }                                                 // 0x78 = wait, 0x21 = busy repeat request
      else if((buffer[0] == 0x7F)&&((buffer[2] != 0x78)&&(buffer[2] != 0x21)) &&
             (servicedata.subservice == diagnosticRequestChrysler_03))
      {
	return 3;                 // Neither KWP or UDS
      }

      status = S_FAIL;
      timer = 0x3F;
      memset((void*)buffer, 0, 20);

  }

  return 0;
  
}
//------------------------------------------------------------------------------
// Read data by parameter ID using $22 command
// Inputs:  u32 ecm_id, u32 device_id, u8 subcommand, u8 *data, u8 length
// Outputs: data
// Return:  u8 status 
// The purpose of this function is read the  data using the parameter ID
// Engineer: Ricardo Cigarroa & Rhonda Rusak
//------------------------------------------------------------------------------    
u8 obd2can_dcx_ReadDataByParameterIdentifier(u32 ecm_id, u32 device_id, u8 subcommand, u8 service_command, u8 *data, u16 *length)
{
    Obd2can_ServiceData servicedata;
    u8 status;

    obd2can_servicedata_init(&servicedata);
    servicedata.ecm_id = ecm_id;
    servicedata.response_id = device_id;
    servicedata.rxdata = data;
    servicedata.nowait_cantx = TRUE;
    servicedata.service = DCX_ReadDataByParameterIdentifier;
    servicedata.subservice = subcommand;
    servicedata.service_controldatabuffer[0] = service_command;
    servicedata.service_controldatalength = 1;
    servicedata.first_frame_data_offset = 2;
    status = obd2can_txrxcomplex(&servicedata);
    
    if(status != S_SUCCESS)
    {
      status = S_FAIL;
    }

    *length = servicedata.rxdatalength;

    return status;  
    
}

//------------------------------------------------------------------------------
// obd2can_dcx_WriteDataByParameterIdentifier ($2E)
// Inputs:  u32 ecm_id
//          u32 response_id
//          Obd2can_SubServiceNumber subservice
//          u8 data
// Return:  u8  status
// Engineer: Ricardo Cigarroa & Rhonda Rusak
//------------------------------------------------------------------------------
u8 obd2can_dcx_WriteDataByParameterIdentifier(u32 ecm_id, u32 response_id, u8 identifier,
                                              u8 *data, u8 length)
{
    Obd2can_ServiceData servicedata;
    u8 status;

    obd2can_servicedata_init(&servicedata);
    servicedata.ecm_id = ecm_id;
    servicedata.response_id = response_id;
    servicedata.broadcast = FALSE;
    servicedata.service = DCX_WriteDataByParamID;
    servicedata.subservice = identifier;
    servicedata.txdata = data;
    servicedata.txdatalength = length;
    status = obd2can_dcx_txrxcomplex(&servicedata);
    
    return status;
}

//------------------------------------------------------------------------------
// obdcan_dcx_sendcommand
// Inputs:  ECM id, response id, tx buffer data, service, subservice, length
// Return:  Nothing
// Engineer: Rhonda Rusak
//------------------------------------------------------------------------------  
u8 obdcan_dcx_sendcommand(u32 ecm_id, u32 response_id, u8 *tx_data, DCX_ServiceNumber service, 
                          Obd2can_SubServiceNumber subservice,
                            u8 length, bool broadcast)
{
    u8 status = S_FAIL;
    signed int timer = 0xFF;
    Obd2can_ServiceData servicedata;

    obd2can_servicedata_init(&servicedata);

    if(broadcast)
    {
      servicedata.ecm_id = 0x7DF;
      servicedata.broadcast_id = 0x7DF;
      servicedata.broadcast = broadcast;
    }
    else
      servicedata.ecm_id = ecm_id;

    servicedata.response_id = response_id;
    servicedata.nowait_cantx = TRUE;
    servicedata.response_expected = TRUE;
    servicedata.service = service;
    servicedata.subservice = subservice;
    servicedata.txdata = tx_data;
    servicedata.txdatalength = length;
    while(status == S_FAIL)
    {
      status = obd2can_dcx_txrx(&servicedata);

/*      while(timer > 0)
      {
        status = obd2can_rx(response_id, &rxinform);
        if(status == S_SUCCESS)
          break;
      }
*/
      if(timer <= 0)
       return S_FAIL;
      else
        timer--;
   }

   return status;

}

//------------------------------------------------------------------------------
// Download RSA signature 
// Inputs:  u32 ecm_type
//          u8 block_index
//          BlockType block_type
// Return:  u8  status
// Engineer: Ricardo Cigarroa
//------------------------------------------------------------------------------
u8 obd2can_dcx_download_rsa_signature(u32 ecm_type,u8 block_index,BlockType block_type)
{
    u8  status;
    
    if(IsVariantId_DCX_GPEC2)
    {
        if(block_index == 0) //OS RSA 
        {
            if(block_type == Cal_Block)
            {
                //Download CAL RSA is doing cal only flash. 
                status = obd2can_dcx_download_cal_rsa_signature(ecm_type);
                if(status != S_SUCCESS)
                {
                    log_push_error_point(0x3047);
                    status = S_FAIL;
                }
                
            }
            else
            {
                status = obd2can_dcx_download_os_rsa_signature(ecm_type, filestock_info->filestock_fptr);
                if(status != S_SUCCESS)
                {
                    log_push_error_point(0x3048);
                    status = S_FAIL; 
                }
            }
        }
        else if(block_index == 1) //CAL RSA
        {
            status = obd2can_dcx_download_cal_rsa_signature(ecm_type);
            if(status != S_SUCCESS)
            {
                log_push_error_point(0x3049);
                status = S_FAIL;
            }
        }
        else
        {
            log_push_error_point(0x304A);
            status = S_FAIL; 
        }
    }
    else
    {
        return S_SUCCESS; 
    }
    
    return status;   
}

//------------------------------------------------------------------------------
// Download Download Transmission Info
// Return:  u8  status
// Engineer: Tristen Pierson
//------------------------------------------------------------------------------
u8 obd2can_dcx_download_trans_info(u32 ecm_type)
{
    u8  status = S_SUCCESS;
    u8  count;
    s32 offset;
    u32 bytesread, bytecount, i;
    u32 blocklength;
    u32 address;
    u32 checksum;
    u32 response_id;
    u16 maxblock;
    u8  *buffer;
    F_FILE *csf_fptr = NULL;
    Csf_Header *csfheader = NULL; 
    Csf_Block_Info *csfinfo = NULL; 

    response_id = ECM_GetResponseECMId(ecm_type);
    // Open csf file
    csf_fptr = genfs_user_openfile(CSFTEMPFILE,"r");    
    if(csf_fptr == NULL)
    {
        log_push_error_point(0x304B);
        status = S_FAIL;
    }
    // Malloc buffer,csfheader and csfinfo
    if (status == S_SUCCESS)
    {
        buffer = __malloc(0xFF);
        if (buffer == NULL)
        {
            log_push_error_point(0x304C);
            status = S_MALLOC;
        }
        csfheader = __malloc(sizeof(Csf_Header));
        if (csfheader == NULL)
        {
            log_push_error_point(0x304D);
            status = S_MALLOC;
        }
        csfinfo = __malloc(sizeof(Csf_Block_Info));
        if (csfinfo == NULL)
        {
            log_push_error_point(0x304E);
            status = S_MALLOC;
        }
    }
    // Read csf header
    if (status == S_SUCCESS)
    {
        count = 0;
        bytesread = 0;
        
        // Read csf header
        bytesread = fread((char*)csfheader, 1, sizeof(Csf_Header), csf_fptr);
        if(bytesread != sizeof(Csf_Header))
        {
            log_push_error_point(0x304F);
            status = S_FAIL;
        }
    }
    // Find trans info if any
    if (status == S_SUCCESS)
    {
        crypto_blowfish_decryptblock_critical((u8*)csfheader,sizeof(Csf_Header));
    
        // Loop through the blocks until the trans_info block is found. 
        while(!(feof(csf_fptr)))
        {
            if (count >= csfheader->checksumcount)
            {
                break;
            }
            
            // Read generic block info
            bytesread = fread((char*)csfinfo,
                            1,sizeof(Csf_Block_Info),csf_fptr);
            if (bytesread != sizeof(Csf_Block_Info))
            {
                log_push_error_point(0x3050);
                status = S_READFILE;
                break;
            }
            crypto_blowfish_decryptblock_critical((u8*)csfinfo,
                                                  sizeof(Csf_Block_Info));
            // Offset to beginning of block
            offset = -sizeof(Csf_Block_Info);
            if(fseek(csf_fptr,offset,SEEK_CUR) != 0)
            {
                log_push_error_point(0x3051);
                status = S_FAIL;
                break; 
            }
            
            switch (csfinfo->type)
            {
            case CRC32:
            case Hemicrc16_Block:
            case Level2_Sum_Block:
            case Equalizer_Sum_Block:
            case IPC:                       // Other generic blocks
                // Offset to next block
                offset = sizeof(Csf_Block_Generic);
                if(fseek(csf_fptr,offset,SEEK_CUR) != 0)
                {
                    log_push_error_point(0x3052);
                    status = S_FAIL; 
                    goto obd2can_dcx_download_trans_info_done; 
                }            
                break;   
            case RSA_Signature_Block:       // Offset for RSA block
                // Offset to next block
                offset = sizeof(Csf_Block_BlockReplacement_496); 
                if(fseek(csf_fptr,offset,SEEK_CUR) != 0)
                {
                    log_push_error_point(0x3053);
                    status = S_FAIL; 
                    goto obd2can_dcx_download_trans_info_done; 
                }            
                break;
            case Trans_Info:                // Handle Trans_Info download
                // Offset to beginning of data
                if(fseek(csf_fptr,16,SEEK_CUR) != 0)
                {
                    log_push_error_point(0x3054);
                    status = S_FAIL;                    
                }
                // Fingerprint Interlock
                if (status == S_SUCCESS)
                {
                    status = obd2can_dcx_fingerprint_interlock_trans_info(ecm_type);
                }
                // Erase trans_info block
                if (status == S_SUCCESS)
                {
                    status = obd2can_dcx_erase_ecm(ecm_type, TRUE);
                }
                // Download trans info data
                if (status == S_SUCCESS)
                {
                    address = 0x00E000; // Trans_Info address
                    bytecount = 0;
                    checksum = 0;       // Trans info additive checksum
                    
                    status = obd2can_dcx_request_download(0x7E0, 0x01, address,
                        csfinfo->length, Request_4_Byte, 
                        CommLevel_KWP2000, &maxblock);
                    
                    // Download blocks
                    if (status == S_SUCCESS && maxblock > 0)
                    {
                        if (maxblock > 0xFF)
                        {
                            maxblock = 0xFF;
                        }                        
                                                
                        while (bytecount < csfinfo->length)
                        {
                            blocklength = csfinfo->length - bytecount;
                            if (blocklength > maxblock)
                            {
                                blocklength = maxblock;
                            }
                            
                            bytesread = fread(buffer,1,blocklength,csf_fptr);
                            
                            if (bytesread != blocklength)
                            {
                                log_push_error_point(0x3055);
                                status = S_READFILE;
                                break;
                            }
                            
                            // Decrypt data
                            crypto_blowfish_decryptblock_critical(buffer,blocklength);
                            
                            // Calculate checksum
                            for (i = 0; i < blocklength; i++)
                            {                                
                                checksum +=  buffer[i];
                            }
                            
                            // Download data
                            status = obd2can_dcx_transfer_data_download(0x7E0, 
                                        download, 0, address, Address_None,
                                        buffer, blocklength, CommLevel_KWP2000, TRUE);
                             
                            bytecount += blocklength;
                            address += blocklength;
                        } // while(..)
                        
                        // Request Transfer Exit
                        if (status == S_SUCCESS)
                        {
                            status = obd2can_dcx_request_transfer_exit(0x7E0, CommLevel_KWP2000);
                        }
                                               
                        // Download trans info checksum
                        if (status == S_SUCCESS)
                        {
                            u8 data[5] = {0x04, 0x00, 0x02, 0x00, 0x00};
                            
                            data[3] = (u8)(checksum >> 8);
                            data[4] = (u8)checksum;
                            
                            status = obd2can_dcx_start_routine_by_local_id_tx(0x7E0, 
                                                            response_id, 0xE1, 0, data, 5);
                        }
                        if (status != S_SUCCESS)
                        {
                            // Fail transfer exit or checksum
                            log_push_error_point(0x3056);
                            status = S_FAIL;
                            goto obd2can_dcx_download_trans_info_done; 
                        }
                    }
                    else
                    {
                        // Failed to request download
                        log_push_error_point(0x3057);
                        status = S_FAIL;
                        goto obd2can_dcx_download_trans_info_done; 
                    }
                }
                if (status != S_SUCCESS)
                {
                    // Failed to erase
                    log_push_error_point(0x3058);
                    status = S_FAIL;
                    goto obd2can_dcx_download_trans_info_done; 
                }
                break;
            default:                        // Invalid block type
                log_push_error_point(0x3059);
                status = S_FAIL;
                goto obd2can_dcx_download_trans_info_done; 
            } // Switch(..)
            count++;
        } // while(..)
    }
    
obd2can_dcx_download_trans_info_done:
    // Close csf file
    if(csf_fptr)
    {
        genfs_closefile(csf_fptr);
    }
    /*free*/
    if (buffer)
    {
        __free(buffer);
    }
    if (csfheader)
    {
        __free(csfheader); 
    }
    if (csfinfo)
    {
        __free(csfinfo);
    }
    
    return status;
}

//------------------------------------------------------------------------------
// Exit vehicle specific hack
// Inputs:  none
// Return:  u8 S_SUCCESS
// Engineer: Ricardo Cigarroa
//------------------------------------------------------------------------------
u8 obd2can_dcx_exit_vehicle_specific_hack()
{
    u8 status; 
    
    Obd2can_ServiceData servicedata;

    obd2can_servicedata_init(&servicedata);
    servicedata.ecm_id = 0x7E0;
    servicedata.nowait_cantx = TRUE;
    servicedata.response_expected = FALSE;
    servicedata.service = 0xBD;
    servicedata.subservice = 0x06;
    servicedata.service_controldatabuffer[0] = 0xBD;
    servicedata.service_controldatabuffer[1] = 0x00;
    servicedata.service_controldatabuffer[2] = 0x00;
    servicedata.service_controldatabuffer[3] = 0x00;
    servicedata.service_controldatabuffer[4] = 0x00;
    servicedata.service_controldatalength = 5; 
    
    status = obd2can_txrx(&servicedata);
    if(status != S_SUCCESS)
    {
        return S_FAIL; 
    }
    
    return S_SUCCESS;
}

//------------------------------------------------------------------------------
// Download CAL RSA signature 
// Inputs:  u32 ecm_type
// Return:  u8  status
// Engineer: Ricardo Cigarroa
//------------------------------------------------------------------------------
u8 obd2can_dcx_download_cal_rsa_signature(u32 ecm_type)
{
    u8  status;
    u8  *databuffer;
    u8  routine_identifier;
    u32 position;
    u32 length;    
    u32 bytecount; 
    u32 ecm_id; 
    
    if(filestock_info->filestock_fptr == NULL)
    {
        log_push_error_point(0x305A);
        return S_FAIL;
    }
    
    databuffer = __malloc(MAX_DOWNLOAD_BUFFER_LENGTH);
    if(databuffer == NULL)
    {
        log_push_error_point(0x305B);
        return S_MALLOC;      
    }
    
    ecm_id = ECM_GetEcmId(ecm_type);
    position = 0x001C008;
    length = 0x88;  
    routine_identifier = 0xE1;  
    
    filestock_read_at_position(position,databuffer,length,&bytecount);
    if(bytecount != length)
    {
        log_push_error_point(0x305C);
        return S_FAIL;
    }
    
    status = obd2can_dcx_start_routine_by_local_id_tx(ecm_id,ECM_GetResponseECMId(ecm_type),routine_identifier,0x04,databuffer,length);
    if(status != S_SUCCESS)
    {
        log_push_error_point(0x305D);
        status = S_FAIL; 
    }
    
    if(databuffer)
    {
        __free(databuffer); 
    }
    
    return status;
}

//------------------------------------------------------------------------------
// Download OS RSA signature 
// Inputs:  u32 ecm_type
// Return:  u8  status
// Engineer: Ricardo Cigarroa
//------------------------------------------------------------------------------
u8 obd2can_dcx_download_os_rsa_signature(u32 ecm_type, F_FILE *file_fptr)
{
    u8  status;
    u8  *databuffer; 
    u8  routine_identifier;
    u32 length; 
    F_FILE *csf_fptr = NULL; 

    Csf_Block_BlockReplacement_496 *csfblockreplacement; 
    
    csf_fptr = genfs_user_openfile(CSFTEMPFILE,"r"); 
    
    if (csf_fptr == NULL)
    {
        log_push_error_point(0x305E);
        status = S_FAIL; 
        goto obd2can_dcx_download_os_rsa_signature_done;
    }
    
    databuffer = __malloc(0x800);
    if (databuffer == NULL)
    {
        log_push_error_point(0x305F);
        status = S_MALLOC;  
        goto obd2can_dcx_download_os_rsa_signature_done;
    }
    csfblockreplacement = __malloc(sizeof(Csf_Block_BlockReplacement_496));
    if (csfblockreplacement == NULL)
    {
        log_push_error_point(0x3060); 
        status = S_MALLOC;  
        goto obd2can_dcx_download_os_rsa_signature_done;
    }
    
    status = obd2can_dcx_download_os_rsa_signature_helper(csfblockreplacement,
                                                          ecm_type, csf_fptr, 
                                                          file_fptr);
    if (status != S_SUCCESS)
    {
        log_push_error_point(0x3061);
        status = S_FAIL;
        goto obd2can_dcx_download_os_rsa_signature_done;
    }  
    
    length = 0x88; //size of RSA signature
    routine_identifier = 0xE1; 
    memcpy((char*)databuffer, (char*)csfblockreplacement->blockdata, length);
    
    //download rsa signature
    status = obd2can_dcx_start_routine_by_local_id_tx(0x7E0,ECM_GetResponseECMId(ecm_type), routine_identifier,
                                                      0x04,databuffer,length);
    if (status != S_SUCCESS)
    {
        log_push_error_point(0x3062);
        goto obd2can_dcx_download_os_rsa_signature_done;
    }
   
obd2can_dcx_download_os_rsa_signature_done: 
    if (csf_fptr)
    {
        genfs_closefile(csf_fptr);
    }
    if (databuffer)
    {
        __free(databuffer); 
    }
    if (csfblockreplacement)
    {
        __free(csfblockreplacement);
    }
       
    return status; 
}

//------------------------------------------------------------------------------
// Utililty Download OS RSA signature  (2013+ GPEC Bootloader file)
// Inputs:  u32 ecm_type
// Return:  u8  status
// Engineer: Ricardo Cigarroa
//------------------------------------------------------------------------------
u8 obd2can_dcx_download_utility_rsa_signature(u32 ecm_type, F_FILE *file_fptr)
{
    u8  status;
    u8  *databuffer;
    u8  routine_identifier;
    u32 position;
    u32 length;    
    u32 bytecount; 
    u32 ecm_id; 
    
    if(file_fptr == NULL)
    {
        log_push_error_point(0x3063);
        return S_FAIL;
    }
    
    databuffer = __malloc(MAX_DOWNLOAD_BUFFER_LENGTH);
    if(databuffer == NULL)
    {
        log_push_error_point(0x3064);
        return S_MALLOC;      
    }
    
    ecm_id = ECM_GetEcmId(ecm_type);
    //0x001C008 is the address of the RSA signature, 0x2800 is the address
    //where the binary data begins in the UBF file. 
    position = (0x001C008 + 0x2800); 
    length = 0x8B;  
    routine_identifier = 0xE1; 
    
    status = file_read_at_position(position,databuffer,length,&bytecount,file_fptr,
                                   FALSE);
    if(status != S_SUCCESS || bytecount != length)
    {
        log_push_error_point(0x3065);
        status = S_FAIL;
        goto obd2can_dcx_download_utility_rsa_signature_done;
    }
    
    status = obd2can_dcx_start_routine_by_local_id_tx(ecm_id,ECM_GetResponseECMId(ecm_type), routine_identifier,
                                                      0x04,databuffer,length);
    if(status != S_SUCCESS)
    {
        log_push_error_point(0x3066);
        status = S_FAIL; 
        goto obd2can_dcx_download_utility_rsa_signature_done;
    }
    
obd2can_dcx_download_utility_rsa_signature_done:
    if(databuffer)
    {
        __free(databuffer);
    }
    return status; 
}

//------------------------------------------------------------------------------
// Download OS RSA signature_helper (2013+ GPEC Bootloader file)
// Inputs:  u32 ecm_type
//          F_FILE *csf_fptr
//          F_FILE *file_fptr
// Return:  u8  status
// Engineer: Ricardo Cigarroa
//------------------------------------------------------------------------------
u8 obd2can_dcx_download_os_rsa_signature_helper(Csf_Block_BlockReplacement_496 *csfblockreplacement, 
                                                u32 ecm_type, F_FILE *csf_fptr, 
                                                F_FILE *file_fptr)
{
    u8  status; 
    u8  count; 
    u32 bytesread;
    s32 offset;
    Csf_Header *csfheader = NULL; 
    Csf_Block_Info *csfinfo = NULL; 
        
    count = 0;
    bytesread = 0;
    
    csfheader = __malloc(sizeof(Csf_Header));
    if (csfheader == NULL)
    {
        log_push_error_point(0x3067);
        status = S_MALLOC; 
        goto obd2can_dcx_download_os_rsa_signature_helper_done;
    }
    csfinfo = __malloc(sizeof(Csf_Block_Info)); 
    if (csfinfo == NULL)
    {
        log_push_error_point(0x3068);
        status = S_MALLOC; 
        goto obd2can_dcx_download_os_rsa_signature_helper_done;
    }
    
    // Read csf header
    bytesread = fread((char*)csfheader, 1, sizeof(Csf_Header), csf_fptr);
    if(bytesread != sizeof(Csf_Header))
    {
        log_push_error_point(0x3069);
        status = S_FAIL; 
        goto obd2can_dcx_download_os_rsa_signature_helper_done;
    }
    
    crypto_blowfish_decryptblock_critical((u8*)csfheader,sizeof(Csf_Header));

    // Loop through the blocks until the rsa block is found 
    while(!(feof(csf_fptr)))
    {
        if (count >= csfheader->checksumcount)
        {
            // If RSA block is not found
            status = S_FAIL;
            break;
        }
        
        // Read generic block info
        bytesread = fread((char*)csfinfo,1,sizeof(Csf_Block_Info),csf_fptr);
        if (bytesread != sizeof(Csf_Block_Info))
        {
            log_push_error_point(0x306A);
            status = S_READFILE;
            goto obd2can_dcx_download_os_rsa_signature_helper_done;
        }
        
        crypto_blowfish_decryptblock_critical((u8*)csfinfo,sizeof(Csf_Block_Info));

        // Offset to beginning of block
        offset = -sizeof(Csf_Block_Info);
        if(fseek(csf_fptr,offset,SEEK_CUR) != 0)
        {
            log_push_error_point(0x306B);
            status = S_READFILE;
            goto obd2can_dcx_download_os_rsa_signature_helper_done; 
        }
        
        switch (csfinfo->type)
        {
        case RSA_Signature_Block:   // Read RSA block
            bytesread = fread((char*)csfblockreplacement,1,
                sizeof(Csf_Block_BlockReplacement_496),csf_fptr);
        
            if (bytesread != sizeof(Csf_Block_BlockReplacement_496))
            {
                log_push_error_point(0x306C);
                status = S_READFILE;
                goto obd2can_dcx_download_os_rsa_signature_helper_done; 
            }
            
            crypto_blowfish_decryptblock_critical((u8*)csfblockreplacement,
                                                  sizeof(Csf_Block_BlockReplacement_496));
            
            status =  S_SUCCESS;
            goto obd2can_dcx_download_os_rsa_signature_helper_done;
        case CRC32:
        case Hemicrc16_Block:
        case Level2_Sum_Block:
        case Equalizer_Sum_Block:
        case IPC:                   // Offset generic blocks
            offset = sizeof(Csf_Block_Generic);
            if(fseek(csf_fptr,offset,SEEK_CUR) != 0)
            {
                log_push_error_point(0x306D);
                status = S_READFILE;
                goto obd2can_dcx_download_os_rsa_signature_helper_done; 
            }            
            break; 
        case Trans_Info:            // Offset Trans_Info block
            offset = sizeof(Csf_Block_BlockReplacement_5632);
            if(fseek(csf_fptr,offset,SEEK_CUR) != 0)
            {
                log_push_error_point(0x306E);
                status = S_READFILE;
                goto obd2can_dcx_download_os_rsa_signature_helper_done; 
            }            
            break;
        default:                    // Invalid csf block found
            log_push_error_point(0x306F);
            status = S_FAIL;
            goto obd2can_dcx_download_os_rsa_signature_helper_done; 
            break;
        }
        count++;
    }
    
obd2can_dcx_download_os_rsa_signature_helper_done:
    if (csfheader)
    {
        __free(csfheader);
    }
    if (csfinfo)
    {
        __free(csfinfo);
    }
    return status; 
}

//------------------------------------------------------------------------------
// This fucntion simply unlocks multiple securities: upload and download
// Inputs:  u32 ecm_type
// Return:  u8  status
// Engineer: Ricardo Cigarroa
//------------------------------------------------------------------------------
u8 obd2can_dcx_unlock_multiple_securities(u32 ecm_type)
{
    u8  status; 
    u32 ecm_id;
    u32 response_id; 
    VehicleCommType commtype;
    
    ecm_id = ECM_GetEcmId(ecm_type);
    response_id = ECM_GetResponseECMId(ecm_type);
    commtype = ECM_GetCommType(ecm_type);
    
    obd2can_dcx_initiate_diagnostic_operation(FALSE,ecm_id, response_id, diagnosticRequestChrysler_92);
    obd2can_dcx_initiate_diagnostic_operation(FALSE,ecm_id, response_id, diagnosticRequestChrysler_85);
    
    status = obd2tune_unlockecm(UnlockTask_Download, ecm_type,commtype,0); 
    if(status != S_SUCCESS)
    {
        obd2can_dcx_ecu_reset_request(FALSE,ecm_id, response_id,hardReset,TRUE);
        obd2can_dcx_reset_22();
        obd2can_dcx_initiate_diagnostic_operation(FALSE,ecm_id, response_id, diagnosticRequestChrysler_92);
        obd2can_dcx_initiate_diagnostic_operation(FALSE,ecm_id, response_id, diagnosticRequestChrysler_85);
    
        status = obd2tune_unlockecm(UnlockTask_Download, ecm_type,commtype,0); 
        if(status != S_SUCCESS)
        {
            return S_FAIL;
        }
    }  
   
    obd2can_dcx_initiate_diagnostic_operation(FALSE,ecm_id, response_id, diagnosticRequestChrysler_92);
    obd2can_dcx_initiate_diagnostic_operation(FALSE,ecm_id, response_id, diagnosticRequestChrysler_85);
    status = obd2tune_unlockecm(UnlockTask_Upload, ecm_type,commtype,0);           
                           
    return status; 
}

//------------------------------------------------------------------------------
// 2013+ GPEC2 hack functions. 
//------------------------------------------------------------------------------
u8 obd2can_dcx_preflash_task(u32 ecm_type, u8 flash_type, bool is_cal_only)
{
    u8  title[16] = "PROGRAM VEHICLE";
    u8  waitMsg[48] = "<HEADER>POWER DOWN ECU</HEADER>\nPlease wait...";
    u8  status,x,z; 
    u8  data[4];
    u8  answer[4];
    u8  temp[4];
    u8  hack_reference;
    u8  hack_is_present = 0;
    u32 hack_address; 
    u32 buffer_address;
    u32 working_address, i; 
    u32 changing_data[3];
    u32 flash_block;
    u32 BLR_address;
    u32 instr_block;
    u8  *cmd_data;
    u8  offset_used = 0;
    u8  fail_once = 0;
    
    u8 cmdsize = 0x80;
    u8 blrsize = 0x04;
    u8 flsblksize = 0x28;
    u8 instrsize = 0x10;
    u32 ramend = 0x810000;

    if(settingstune.DCX_Bootloader_offset != 0)
    {
        offset_used = 1;
        hack_address = 0x27688 + settingstune.DCX_Bootloader_offset;
    }
    else
        hack_address = 0x27688;

    buffer_address = 0x800000;
    hack_reference = 0; 
    
    working_address = hack_address; 
    
    if(is_cal_only == TRUE)
    {
        return S_SUCCESS; 
    }
    
    if(flash_type == CMDIF_ACT_STOCK_FLASHER)
    {
        flash_type = 0;
    }
    else
    {
        flash_type = 1; 
    }
    //************************************************
    // key on/off sequence for 2013+ GPEC2 processor
    //************************************************
    
    if(IsVariantId_DCX_GPEC2)
    {
        u8 keycheckcount = 0;
        
        cmd_data = __malloc(2048+16);
        if(!data)
        {
            status = S_MALLOC;
            goto obd2can_dcx_predownloadtask_done;
        }
        
        cmdif_remotegui_msg_keyoff(CMDIF_CMD_DO_DOWNLOAD, title);
        
        cmdif_remotegui_messagebox(CMDIF_CMD_DO_DOWNLOAD,
            title, CMDIF_REMOTEGUI_ICON_NONE,
            CMDIF_REMOTEGUI_BUTTONFACE_None, CMDIF_REMOTEGUI_BUTTONFACE_None, CMDIF_REMOTEGUI_BUTTONFACE_None,
            CMDIF_REMOTEGUI_FOCUS_Button1, waitMsg);
        
        delays(15000, 'm');
        
        cmdif_remotegui_msg_keyon(CMDIF_CMD_DO_DOWNLOAD, title);
        
        cmdif_remotegui_messagebox(CMDIF_CMD_DO_DOWNLOAD,
            title, CMDIF_REMOTEGUI_ICON_NONE,
            CMDIF_REMOTEGUI_BUTTONFACE_None, CMDIF_REMOTEGUI_BUTTONFACE_None, CMDIF_REMOTEGUI_BUTTONFACE_None,
            CMDIF_REMOTEGUI_FOCUS_Button1, waitMsg);
        
        delays(3000, 'm');
        
        cmdif_remotegui_msg_keyoff(CMDIF_CMD_DO_DOWNLOAD, title);
        cmdif_remotegui_msg_keyon(CMDIF_CMD_DO_DOWNLOAD, title);
        
        cmdif_remotegui_messagebox(CMDIF_CMD_DO_DOWNLOAD,
            title, CMDIF_REMOTEGUI_ICON_NONE,
            CMDIF_REMOTEGUI_BUTTONFACE_None, CMDIF_REMOTEGUI_BUTTONFACE_None, CMDIF_REMOTEGUI_BUTTONFACE_None,
            CMDIF_REMOTEGUI_FOCUS_Button1, waitMsg);

        // Wait for processor reset response
        do
        {
            status = obd2_checkkeyon_special(CommType_CAN,TRUE);
            if(status != S_SUCCESS)
            {
                if(keycheckcount++ > 15)
                {
                    //timeout
                    log_push_error_point(0x3070);
                    status = S_FAIL;
                    goto obd2can_dcx_predownloadtask_done;
                }
            }
        }
        while(status != S_SUCCESS);               
    }
            
    cmdif_internalresponse_ack(CMDIF_CMD_DO_DOWNLOAD,
                                       CMDIF_ACK_PROCESSING_DATA,NULL,0);
TRYAGAIN: 
    for(x=0; x<4; x++)
    {
        memcpy((void*)temp,(void*) hack_data[hack_reference].check_strings[x+1], 4);
             
        status = obd2can_dcx_hack_read_buffer(working_address,answer);
        if(status == S_SUCCESS)
        {
            for(z=0; z<4; z++)
            {
                if(temp[z] != answer[z])
                {
                    memcpy((void*)temp,(void*) hack_data[hack_reference].change_string, 4); 
                    if(temp[z] != answer[z])
                    {
                        log_push_error_point(0x3071);
                        if(fail_once == 0)
                        {
                            if(offset_used == 0)
                            {
                                offset_used = 1;
                                settingstune.DCX_Bootloader_offset = 0x10000;
                            }
                            else
                            {
                                offset_used = 0;
                                settingstune.DCX_Bootloader_offset = 0;
                            }
                            hack_address = 0x27688 + settingstune.DCX_Bootloader_offset;
                            working_address = hack_address;
                            fail_once = 1;
                            goto TRYAGAIN;
                        }
                        else
                          status = S_FAIL;
                        break;
                    }
                    else
                    {
                        hack_is_present = 1;
                    }
                }
                else
                {
                    status = S_SUCCESS;
                }
            }
        }
        if(status == S_FAIL)
        {
            break;
        }
        working_address += 4;
    }
      
    if(status == S_FAIL)
    {
        status = S_NOTREQUIRED;
        log_push_error_point(0x3072);
        goto obd2can_dcx_predownloadtask_done; 
    }
    
    if((hack_is_present == 1) && (flash_type == 1))//hack already there
    {
        status = S_SUCCESS;
        goto obd2can_dcx_predownloadtask_done;         
    }
    else if((hack_is_present == 0) && (flash_type == 0))//hack already gone
    {
        status = S_SUCCESS;
        goto obd2can_dcx_predownloadtask_done;
    }
   
    status = obd2can_dcx_hack_set_RAM_buffer(0x01,buffer_address);//set Ram buffer location
    if(status != S_SUCCESS)
    {
        log_push_error_point(0x3073);
    }
    
    working_address = buffer_address + 0x80;
    memset((void*)data,0,4);
    memset((void*)answer,0,4);
    
    for(i=0; i<0xE0; i++)  // Clearing All of Ram buffer
    {
        status = obd2can_dcx_hack_write_buffer(working_address,data,answer);
        if(status != S_SUCCESS)
        {
            log_push_error_point(0x3074);
        }
        working_address += 4;
    }
    
    working_address = buffer_address + 8;   
    
    for(i=0; i<0x1E; i++)  // Clearing command size of Ram buffer
    {
        status = obd2can_dcx_hack_write_buffer(working_address,data,answer);
        if(status != S_SUCCESS)
        {
            log_push_error_point(0x3075);
        }
        working_address += 4;
    }
    
    working_address = buffer_address; 
    
    status = obd2can_dcx_hack_write_buffer(working_address,data,answer); 
    if(status != S_SUCCESS)
    {
        log_push_error_point(0x3076);
    }
    
    buffer_address = obd2can_dcx_hack_check_RAM_buffer(working_address,ramend,cmdsize,0);//find command block 
    
    if(buffer_address == 0)
    {
        log_push_error_point(0x3077);
        status = S_FAIL;
        goto obd2can_dcx_predownloadtask_done;
    }
    
    working_address = buffer_address + cmdsize; 
    
    BLR_address = obd2can_dcx_hack_check_RAM_buffer(working_address,ramend,blrsize,buffer_address);
    if(BLR_address == 0)
    {
        log_push_error_point(0x3078);
        status = S_FAIL;
        goto obd2can_dcx_predownloadtask_done;
    }
    
    memcpy((void*)data,(void*) hack_data[hack_reference].check_strings[0], 4);
    memcpy((void*)answer,(void*)data, 4);
    
    status = obd2can_dcx_hack_write_buffer(BLR_address,data,answer);//write first line of hack   
    if(status != S_SUCCESS)
    {
        log_push_error_point(0x3079);
    }
    
    working_address = BLR_address + blrsize;//find flash block area
    
    flash_block = obd2can_dcx_hack_check_RAM_buffer(working_address,ramend,flsblksize,buffer_address); 
    if(flash_block == 0)
    {
        log_push_error_point(0x307A);
        status = S_FAIL;
        goto obd2can_dcx_predownloadtask_done;
    }
    
    working_address = flash_block + flsblksize;// find instruction block area   
    instr_block = obd2can_dcx_hack_check_RAM_buffer(working_address,ramend,instrsize,buffer_address);
    if(instr_block == 0)
    {
        log_push_error_point(0x307B);
        status = S_FAIL;
        goto obd2can_dcx_predownloadtask_done;
    }
    
    working_address = instr_block;
    
    for(i=1; i<5; i++)
    {
        if((flash_type == 1) && (i == 2))
        {
            memcpy((void*)data,(void*) hack_data[hack_reference].change_string, 4);
        }
        else
        {
            memcpy((void*)data,(void*) hack_data[hack_reference].check_strings[i], 4); //write original bootloader. 
        }

        memcpy((void*)answer,(void*)data, 4);
        obd2can_dcx_hack_write_buffer(working_address,data,answer);//write hack
        working_address += 4; 
    }

    status = obd2can_dcx_hack_flash_initialize_7(flash_block,buffer_address); 
    if(status != S_SUCCESS)
    {
        log_push_error_point(0x307C);
    }
    status = obd2can_dcx_hack_set_lock_9(buffer_address);
    if(status != S_SUCCESS)
    {
        log_push_error_point(0x307D);
    }

    if(offset_used == 0)
    {
        status = obd2can_dcx_hack_flash_erase_8(0x20,buffer_address); // erase 0x30000, 0x10000 bytes
        if(status != S_SUCCESS)
        {
            log_push_error_point(0x307E);
        }
  
        changing_data[0] = 0x00020000;      // move Bootloader from 0x20000 to 0x30000, 0x10000 bytes
        changing_data[1] = 0x00030000;
        changing_data[2] = 0x00010000;
    }
    else
    {
        status = obd2can_dcx_hack_flash_erase_8(0x10,buffer_address); // erase 0x20000, 0x10000 bytes
        if(status != S_SUCCESS)
        {
            log_push_error_point(0x307E);
        }
  
        changing_data[0] = 0x00030000;      // move Bootloader from 0x30000 to 0x20000, 0x10000 bytes
        changing_data[1] = 0x00020000;
        changing_data[2] = 0x00010000;
    }

    status = obd2can_dcx_hack_flash_write_6(changing_data,buffer_address,BLR_address,flash_block);
    if(status != S_SUCCESS)
    {
        log_push_error_point(0x307F);
    }
    status = obd2can_dcx_hack_flash_initialize_7(flash_block,buffer_address);
    if(status != S_SUCCESS)
    {
        log_push_error_point(0x3080); 
    }
    status = obd2can_dcx_hack_set_lock_9(buffer_address);
    if(status != S_SUCCESS)
    {
        log_push_error_point(0x3081);
    }


    if(offset_used == 0)
    {
        status = obd2can_dcx_hack_flash_erase_8(0x10,buffer_address); // erase 0x20000, 0x10000 bytes
        if(status != S_SUCCESS)
        {
            log_push_error_point(0x3082);
        }
    
        changing_data[0] = 0x00030000;
        changing_data[1] = 0x00020000;
        changing_data[2] = 0x00007688;
    }
    else
    {
        status = obd2can_dcx_hack_flash_erase_8(0x20,buffer_address); // erase 0x30000, 0x10000 bytes
        if(status != S_SUCCESS)
        {
            log_push_error_point(0x3082);
        }
    
        changing_data[0] = 0x00020000;
        changing_data[1] = 0x00030000;
        changing_data[2] = 0x00007688;
    }

    status = obd2can_dcx_hack_flash_write_6(changing_data,buffer_address,BLR_address,flash_block);
    if(status != S_SUCCESS)
    {
        log_push_error_point(0x3083);
    }
    
    changing_data[0] = instr_block;
    if(offset_used == 0)
        changing_data[1] = 0x00027688;
    else
        changing_data[1] = 0x00037688;
    changing_data[2] = 0x00000010;
    
    status = obd2can_dcx_hack_flash_write_6(changing_data,buffer_address,BLR_address,flash_block);
    if(status != S_SUCCESS)
    {
        log_push_error_point(0x3084);
    }
    
    if(offset_used == 0)
    {
        changing_data[0] = 0x00037698;
        changing_data[1] = 0x00027698;
    }
    else
    {
        changing_data[0] = 0x00027698;
        changing_data[1] = 0x00037698; 
    }
    changing_data[2] = 0x00008968;
    
    status = obd2can_dcx_hack_flash_write_6(changing_data,buffer_address,BLR_address,flash_block);
    if(status != S_SUCCESS)
    {
        log_push_error_point(0x3085);
    }
    status = obd2can_dcx_hack_flash_initialize_7(flash_block,buffer_address);
    if(status != S_SUCCESS)
    {
        log_push_error_point(0x3086);
    }
    status = obd2can_dcx_hack_set_lock_9(buffer_address);
    if(status != S_SUCCESS)
    {
        log_push_error_point(0x3087);
    }

    if(offset_used == 0)
        status = obd2can_dcx_hack_flash_erase_8(0x20,buffer_address);
    else
        status = obd2can_dcx_hack_flash_erase_8(0x10,buffer_address);
    if(status != S_SUCCESS)
    {
        log_push_error_point(0x3088);
    }
    
    changing_data[0] = 0x00000000;
    if(offset_used == 0)
        changing_data[1] = 0x00030000;
    else
        changing_data[1] = 0x00020000;
    changing_data[2] = 0x00010000;
    
    status = obd2can_dcx_hack_flash_write_6(changing_data,buffer_address,BLR_address,flash_block);
    if(status != S_SUCCESS)
    {
        log_push_error_point(0x3089);
    }
    working_address = BLR_address;
    
    memset((void*)data,0,4);
    memset((void*)answer,0,4);
    
    for(i=0; i<15; i++)  // Clearing All of Ram buffer
    {
        status = obd2can_dcx_hack_write_buffer(working_address,data,answer);
        if(status != S_SUCCESS)
        {
            log_push_error_point(0x308A);
        }
        working_address += 4;
    }
    
    working_address = buffer_address +8;
    
    for(i=0; i<30; i++) //Clearing All of Ram buffer
    {
        status = obd2can_dcx_hack_write_buffer(working_address,data,answer);
        if(status != S_SUCCESS)
        {
            log_push_error_point(0x308B);
        }
        working_address += 4; 
    }
    
    status = obd2can_dcx_hack_write_buffer(buffer_address,data,answer);
    if(status != S_SUCCESS)
    {
        log_push_error_point(0x308C);
    }
    
    data[0] = (u8)((hack_address >> 24) & 0xFF);
    data[1] = (u8)((hack_address >> 16) & 0xFF);
    data[2] = (u8)((hack_address >> 8) & 0xFF);
    data[3] = (u8)(hack_address & 0xFF);
    
   working_address = hack_address;
   
   for(x=0; x<4; x++)
   {
       memcpy((void*)temp,(void*) hack_data[hack_reference].check_strings[x+1], 4); 

       status = obd2can_dcx_hack_read_buffer(working_address,answer);//look for correct code to change
       if(status == S_SUCCESS)
       {
           for(z=0; z<4; z++)
           {
               if(temp[z] != answer[z])
               {
                   memcpy((void*)temp,(void*)hack_data[hack_reference].change_string,4);
                   if(temp[z] != answer[z])
                   {
                       log_push_error_point(0x308D);
                       status = S_FAIL; 
                       break;
                   }
               }
               else
               {
                   status = S_SUCCESS;
               }
           }
       }
       if(status == S_FAIL)
       {
           break;
       }
       working_address += 4;
       
   }
 
obd2can_dcx_predownloadtask_done:   
    
    obd2can_dcx_exit_vehicle_specific_hack();
    
    if(cmd_data)
    {
        __free(cmd_data);
    }
    
    if(status == S_SUCCESS)
    {
        status = obd2can_dcx_unlock_multiple_securities(ecm_type);
        if(status != S_SUCCESS)
        {
            log_push_error_point(0x308E);
            status = S_FAIL; 
        }
    }
             
   return status;
}

//------------------------------------------------------------------------------
u8 obd2can_dcx_hack_read_buffer(u32 address, u8 *answer)
{
    u8 i; 
    u8 status = S_SUCCESS;
    obd2can_rxinfo rxinfo;
    Obd2can_ServiceData servicedata;

    obd2can_servicedata_init(&servicedata);
    servicedata.ecm_id = 0x7E0;
    servicedata.response_expected = TRUE;
    servicedata.initial_wait_extended = 3; 
    servicedata.service = 0xBD;
    servicedata.subservice = 0x06;
    
    servicedata.service_controldatabuffer[0] = 0x00;
    servicedata.service_controldatabuffer[1] = (u8)((address >> 24) & 0xFF);
    servicedata.service_controldatabuffer[2] = (u8)((address >> 16) & 0xFF);
    servicedata.service_controldatabuffer[3] = (u8)((address >> 8) & 0xFF);
    servicedata.service_controldatabuffer[4] = (u8)(address & 0xFF);
    servicedata.service_controldatalength = 5; 
    
    //send data.Tx
    status = obd2can_txcomplex(servicedata.ecm_id,
                               servicedata.response_id,
                               servicedata.service,
                               servicedata.subservice,
                               servicedata.service_controldatabuffer,
                               servicedata.service_controldatalength,
                               servicedata.txdata,
                               servicedata.txdatalength,
                               servicedata.nowait_cantx);
    
    //receive data. Rx
    if (status == S_SUCCESS && servicedata.response_expected == TRUE)
    {
        obd2can_rxinfo_init(&rxinfo);

        rxinfo.rxbuffer = servicedata.rxdata;
        rxinfo.cmd = (u8)servicedata.service;
        rxinfo.response_cmd_offset = 0x00;
        rxinfo.initial_wait_extended = servicedata.initial_wait_extended;
        rxinfo.busy_wait_extended = servicedata.busy_wait_extended;
        rxinfo.commlevel = servicedata.commlevel;
        rxinfo.rx_timeout_ms = servicedata.rx_timeout_ms;
                
        status = obd2can_rx(ECM_RESPONSE_ID(servicedata.ecm_id), &rxinfo);
        
        if(status != S_SUCCESS)
        {
            status = obd2can_rx(ECM_RESPONSE_ID(servicedata.ecm_id), &rxinfo);
            if(status == S_SUCCESS)
            { 
                for(i=0; i<4; i++)
                {
                    answer[i] = rxinfo.rxbuffer[i+2];
                    status = S_SUCCESS;
                }
            }
            else
            {
                log_push_error_point(0x308F);
                status = S_FAIL;                 
            }
        } 
        else
        {
            for(i=0; i<4; i++)
            {
                answer[i] = rxinfo.rxbuffer[i+2];
                status = S_SUCCESS;
            }
        }
    }
    else //error with Tx
    {
        log_push_error_point(0x3090);
        status = S_FAIL; 
    }
    status = S_SUCCESS;
    return status; 
}

//------------------------------------------------------------------------------
u8 obd2can_dcx_hack_set_RAM_buffer(u8 command, u32 buffer_address)
{
    u8 status;
    u8 data[4];
    u8 answer[4];
    
    data[0] = (u8)((buffer_address >> 24) & 0xFF);
    data[1] = (u8)((buffer_address >> 16) & 0xFF);
    data[2] = (u8)((buffer_address >> 8) & 0xFF);
    data[3] = (u8)(buffer_address& 0xFF);
    
    memcpy((void*)answer,(void*)data, 4);
    
    status = obd2can_dcx_hack_command(command,data,answer);
    if(status != S_SUCCESS)
    {
        log_push_error_point(0x3091);
        return S_FAIL; 
    }
    
    return status;
    
}

//------------------------------------------------------------------------------
u8 obd2can_dcx_hack_command(u8 command, u8 *data, u8 *answer)
{
    u8 i;
    u8 status;
    obd2can_rxinfo rxinfo;
    Obd2can_ServiceData servicedata;
    
    obd2can_servicedata_init(&servicedata);
    servicedata.ecm_id = 0x7E0;
    servicedata.response_expected = TRUE;
    servicedata.initial_wait_extended = 3; 
    servicedata.service = 0xBD;
    servicedata.subservice = 0x06;
    
    servicedata.service_controldatabuffer[0] = command; 
    servicedata.service_controldatalength = 1; 
    servicedata.txdata = data; 
    servicedata.txdatalength = 4; 
    
    //send data.Tx
    status = obd2can_txcomplex(servicedata.ecm_id,
                               servicedata.response_id,
                               servicedata.service,
                               servicedata.subservice,
                               servicedata.service_controldatabuffer,
                               servicedata.service_controldatalength,
                               servicedata.txdata,
                               servicedata.txdatalength,
                               servicedata.nowait_cantx);
    
    if(status == S_SUCCESS && servicedata.response_expected == TRUE)
    {
        obd2can_rxinfo_init(&rxinfo);
        
        rxinfo.rxbuffer = servicedata.rxdata;
        rxinfo.cmd = (u8)servicedata.service;
        rxinfo.response_cmd_offset = 0x00;
        rxinfo.initial_wait_extended = servicedata.initial_wait_extended;
        rxinfo.busy_wait_extended = servicedata.busy_wait_extended;
        rxinfo.commlevel = servicedata.commlevel;
        rxinfo.rx_timeout_ms = servicedata.rx_timeout_ms;
        
        status = obd2can_rx(ECM_RESPONSE_ID(servicedata.ecm_id), &rxinfo);
        if(status != S_SUCCESS)
        {
            status = obd2can_rx(ECM_RESPONSE_ID(servicedata.ecm_id), &rxinfo);
            if(status == S_SUCCESS)
            {
                for(i=0; i<4; i++)
                {
                    if(answer[i] != rxinfo.rxbuffer[i+2])
                    {
                        log_push_error_point(0x3092);
                        status = S_FAIL; 
                        break;
                    }
                    else
                    {
                        status = S_SUCCESS; 
                    } 
                }
            }
            else
            {
                log_push_error_point(0x3093);
                status = S_FAIL;
            }
        }
        else
        {
            for(i=0; i<4; i++)
            {
                if(answer[i] != rxinfo.rxbuffer[i+2])
                {
                    //log_push_error_point(0x306F);
                    status = S_FAIL; 
                    break;
                }
                else
                {
                    status = S_SUCCESS; 
                }  
            }
        }
    }
    else
    {
        log_push_error_point(0x3094);
        status = S_FAIL; 
    }
    status = S_SUCCESS; 
    return status; 

}

//------------------------------------------------------------------------------
u8 obd2can_dcx_hack_write_buffer(u32 working_address, u8 *data, u8 *answer)
{
    u8 status;
    u8 blank_buffer[4] = {0,0,0,0};
    
    status = obd2can_dcx_hack_set_RAM_buffer(0x02,working_address);//write to RAM buffer location
    if(status != S_SUCCESS)
    {
       log_push_error_point(0x3095);
       return S_FAIL;        
    }
    status = obd2can_dcx_hack_command(0x03,data,answer);//Data to write
    if(status != S_SUCCESS)
    {
        log_push_error_point(0x3096);
        return S_FAIL;
    }
    status = obd2can_dcx_hack_command(0x04,blank_buffer,blank_buffer);//execute write
    if(status != S_SUCCESS)
    {
        log_push_error_point(0x3097);
    }
    delays(20,'m');
    
    return status; 
}

//------------------------------------------------------------------------------
u32 obd2can_dcx_hack_check_RAM_buffer(u32 start_address, u32 end_address, 
                                      u32 length, u32 ram_buffer_add) 
{
    u8 data[4] = {0,0,0,0};
    u8 answer[4] = {0,0,0,0};
    u8 status = 0,i,j,z;
    u8 round_count = 0;
    u32 working_address = 0;
    u32 match_count = 0; 

    working_address = start_address;
    while(working_address < end_address)
    {
        if((working_address + 4) > end_address)
          return 0;

        status = obd2can_dcx_hack_read_buffer(working_address, answer);
        if(status == S_FAIL)
        {
            match_count = 0;
            round_count = 0;
        }
        else
        {
            for(z=0; z<4; z++)
            {
                if(answer[z] != data[z])
                {
                    log_push_error_point(0x3098);
                    status = S_FAIL;
                }
            }
            if(status == S_FAIL)
            {
                match_count = 0;
                round_count = 0;
            }
            else
                match_count += 4;
        }

        working_address += 4;

        if(match_count >= length)
        {
           if(round_count < 2)
           {
                if(round_count == 0)
                {
                    round_count = 1;
                    memset((void*)data,0xBD,4);
                    memset((void*)answer,0xBD,4);
                }
                else if(round_count == 1)
                {
                    round_count = 2;
                    memset((void*)data,0x00,4);
                    memset((void*)answer,0x00,4);
                }

                working_address = working_address - length;

                if((ram_buffer_add == 0)|(ram_buffer_add == working_address))
                {
                    obd2can_dcx_hack_set_RAM_buffer(0x01,working_address); // set Ram buffer location
                    j=2;
                }
                else
                {
                    obd2can_dcx_hack_set_RAM_buffer(0x01,ram_buffer_add); // set Ram buffer location
                    j=0;
                }
				for(i=j;i<(length/4);i++)
				{
					obd2can_dcx_hack_write_buffer(working_address+(4*i),data, answer);
				}
				if((length%4)>0)
				{
					obd2can_dcx_hack_read_buffer(working_address+(4*i),answer);
                    obd2can_dcx_hack_write_buffer(working_address+(4*i),data,answer);

				}
				if((ram_buffer_add==0)||(ram_buffer_add==working_address))
				{
                    obd2can_dcx_hack_write_buffer(working_address,data,answer);
				}
                
                delays(30,'m');
             
           }
           else if(round_count == 2)
           {
                return (working_address - length);
           }
           match_count = 0;
        }
        
    }

    return 0;
}

//------------------------------------------------------------------------------
u8 obd2can_dcx_hack_flash_initialize_7(u32 flash_block, u32 buffer_add)
{
    u8 status; 
    u8 i; 
    u8 additions[4] = {0x00,0x0C,0x10,0x24};
    u8 answer[4];
    u8 final[4] = {0x00,0x80,0x00,0x84};
    u8 data[4][4] = {{0xC3,0xF8,0x80,0x00},
                     {0x00,0xFF,0xFC,0x00},
                     {0x00,0x00,0x04,0x00},
                     {0x00,0x00,0x00,0x00},
                    };
    u32 final_add = buffer_add + (4*6);
    u32 working_address; 
   
    for(i=0; i<4; i++)
    {
        memcpy((void*)answer, (void*)data[i], 4);
        working_address = (flash_block + additions[i]);
       
        status = obd2can_dcx_hack_write_buffer(working_address,data[i],answer);
        if(status == S_FAIL)
        {
            log_push_error_point(0x3099);
            break;
        }
    }
   
    memcpy((void*)answer, (void*)final, 4);
    obd2can_dcx_hack_write_buffer(final_add,final,answer);
    memset((void*)answer, 0,4);
   
    obd2can_dcx_hack_command(0x07,answer,answer);
   
    return status; 
   
}

//------------------------------------------------------------------------------
u8 obd2can_dcx_hack_flash_write_6(u32 *changing_data, u32 buffer_add, 
                                  u32 blr_add, u32 flsh_blk_add)
{
    u8  status;
    u8  i;
    u8  answer[4];
    u8  multipliers[5] = {3,2,4,5,6};
    u8  data[4];
    u32 working_address; 
    
    memcpy((void*)answer,(void*)data, 4);
    
    for(i=0; i<5; i++)
    {
        working_address = buffer_add +(4*multipliers[i]);

        if(i<3)
        {
            data[0] = (u8)((changing_data[i] >> 24) & 0xFF);
            data[1] = (u8)((changing_data[i] >> 16) & 0xFF);
            data[2] = (u8)((changing_data[i] >> 8) & 0xFF);
            data[3] = (u8)(changing_data[i] & 0xFF);

            memcpy((void*)answer, (void*)data[i], 4);

            obd2can_dcx_hack_write_buffer(working_address,data,answer);
        }
        else
        {
            if(i == 3)
            {
                data[0] = (u8)((blr_add >> 24) & 0xFF);
                data[1] = (u8)((blr_add >> 16) & 0xFF);
                data[2] = (u8)((blr_add >> 8) & 0xFF);
                data[3] = (u8)(blr_add & 0xFF);
            }
            else
            {
                data[0] = (u8)((flsh_blk_add >> 24) & 0xFF);
                data[1] = (u8)((flsh_blk_add >> 16) & 0xFF);
                data[2] = (u8)((flsh_blk_add >> 8) & 0xFF);
                data[3] = (u8)(flsh_blk_add & 0xFF);
            }

            memcpy((void*)answer, (void*)data, 4);

            obd2can_dcx_hack_write_buffer(working_address,data,answer);
            
        }
    }

    memset((void*)answer, 0,4);

    status = obd2can_dcx_hack_command(0x06, answer, answer);

    return status;
    
}

//------------------------------------------------------------------------------
u8 obd2can_dcx_hack_set_lock_9(u32 buffer_address)
{
    u32 working_address = 0;
    u8 multipliers[5] = {6,11,12,13,13};
    u8 i,x;
    u8 answer[4];
    u8 data[5][4] = {{0x00,0x80,0x00,0x84},
                     {0x00,0x00,0x00,0x02},
                     {0x00,0x00,0x00,0x00},
                     {0xA1,0xA1,0x11,0x11},
                     {0xC3,0xC3,0x33,0x33}
                    };
                    
    for(x=0; x<2; x++)
    {
        for(i=0; i<4; i++)
        {
            if((x == 1) && (i == 3))
            {
                i = 4; 
            }
             
            memcpy((void*)answer, (void*)data[i], 4);
            working_address = buffer_address +(4* multipliers[i]);
            obd2can_dcx_hack_write_buffer(working_address,data[i],answer);
        }
        
        memset((void*)answer, 0,4);
        obd2can_dcx_hack_command(0x09, answer, answer);
        data[1][3] = 0x03;       
    }
    
    return S_SUCCESS; 
}

//------------------------------------------------------------------------------
u8 obd2can_dcx_hack_flash_erase_8(u8 flash_section, u32 buffer_add)
{
    u32 working_address = 0;
    u8 multipiers[6] = {6,7,8,9,10,5};
    u8 status = 0,i;
    u8 answer[4];
    u8 data[6][4] = {{0x00,0x80,0x00,0x84},
                     {0x00,0x00,0x00,0x00},
                     {0x00,0x00,0x00,0x20},
                     {0x00,0x00,0x00,0x00},
                     {0x00,0x00,0x00,0x00},
                     {0x00,0x80,0x00,0x80}
                    };
                    
    data[2][3] = flash_section;

    for(i=0; i<6; i++)
    {
        memcpy((void*)answer, (void*)data[i], 4);

        working_address = buffer_add +(4*multipiers[i]);
        obd2can_dcx_hack_write_buffer(working_address,data[i],answer);
    }

    memset((void*)answer, 0,4);

    status = obd2can_dcx_hack_command(0x08, answer, answer);

    return status;           
}

//------------------------------------------------------------------------------
// Inputs:  u8  percentage
//          u8  *message (if NULL -> not used)
// Return:  none
// Engineer: Ricardo Cigarroa
//------------------------------------------------------------------------------
void obd2can_dcx_utility_progressbar(u8 percentage,
                                          u8 *message)
{
    u8  buffer[128+2];
    u8  bufferlength;
    u32 messagelength;
    
    buffer[0] = (u8)percentage;
    bufferlength = 1;
    if (message)
    {
        messagelength = strlen((char*)message);
        if (messagelength < 32)
        {
            strcpy((char*)&buffer[1],(char*)message);
            bufferlength += (messagelength+1);
        }
    }
    //Note: because this function only used in CMDIF_CMD_INIT_DOWNLOAD
    cmdif_internalresponse_ack_nowait
        (CMDIF_CMD_DO_DOWNLOAD, CMDIF_ACK_PROGRESSBAR, buffer, bufferlength);
}
//------------------------------------------------------------------------------
// ETC Relearn - Electronic Thottle Control 
// Inputs:  None
// Return:  u8  status
// Engineer: Rhonda Rusak
//------------------------------------------------------------------------------
u8 obd2can_dcx_ETC_relearn()
{
    u8  status = 0,j,i;
    u8  routine = 0x14;
    u8  DTC_count = 0;
    u8  buffer[40];
    u16 bytecount = 0;
    u16 responsechoice = 0;
    u32 node_ID = 0x7E0;
    u32 response_ID = 0x7E8;
    CMDIF_REMOTEGUI_RESPONSETYPE responsetype;


    signed int timeout = 0xFF;

    memset((void*)buffer,0,40);

    obd2can_init(Obd2canInitNormal,0);

     while(timeout > 0)
    {
        status = obd2can_ping(Obd2CanEcuId_7E0, FALSE);
        if(status == S_SUCCESS)
          break;
        timeout-- ;
        delays(300, 'm');
    }

    if(timeout == 0)
      return S_FAIL;

    for(i=0; i<4; i++)
        obd2can_testerpresent(TRUE,Obd2CanEcuId_7E0,CommLevel_KWP2000);
    

    memset((void*)buffer,0,40);
    obd2can_dcx_ReadDataByParameterIdentifier(node_ID, response_ID, 0x01,0xD5, buffer, &bytecount);

    obd2can_testerpresent(TRUE,Obd2CanEcuId_7E0,CommLevel_KWP2000);
    obd2can_testerpresent(TRUE,Obd2CanEcuId_7E0,CommLevel_KWP2000);


    for(j=0; j<2; j++)
    {

        while(timeout--)
        {
            status = obd2can_dcx_start_routine_by_local_id_tx(node_ID, response_ID, routine,0,buffer, 0);
            if(status == S_SUCCESS)
                break;

           obd2can_dcx_initiate_diagnostic_operation(FALSE,node_ID, response_ID, diagnosticRequestChrysler_92);

        }


        if(status == S_FAIL)
            return S_NOTSUPPORT;

        timeout = 0xFF;
        obd2can_testerpresent(TRUE,Obd2CanEcuId_7E0,CommLevel_KWP2000);
        obd2can_testerpresent(TRUE,Obd2CanEcuId_7E0,CommLevel_KWP2000);

        while(timeout--)
        {
            status = obd2can_dcx_results_routine_by_local_id(node_ID, response_ID, routine,0);
            if(status == S_SUCCESS)
                break;

           obd2can_dcx_initiate_diagnostic_operation(FALSE,node_ID, response_ID, diagnosticRequestChrysler_92);

        }

        routine = 0x0F;
        timeout = 0xFF;


        obd2can_testerpresent(TRUE,Obd2CanEcuId_7E0,CommLevel_KWP2000);

        if(j==0)
        {
          obd2can_ping(Obd2CanEcuId_7E0, FALSE);
          obd2can_dcx_getdtccount(&DTC_count, node_ID);
          obd2can_testerpresent(TRUE,Obd2CanEcuId_7E0,CommLevel_KWP2000);
        }
    }

    obd2can_testerpresent(TRUE,Obd2CanEcuId_7E0,CommLevel_KWP2000);
    obd2can_testerpresent(TRUE,Obd2CanEcuId_7E0,CommLevel_KWP2000);

    routine = 0x10;
    timeout = 0xFF;


    while(timeout--)
    {
        status = obd2can_dcx_start_routine_by_local_id_tx(node_ID, response_ID, routine,0,buffer, 0);
        if(status == S_SUCCESS)
            break;

        obd2can_dcx_initiate_diagnostic_operation(FALSE,node_ID, response_ID, diagnosticRequestChrysler_92);

    }

    obd2can_testerpresent(TRUE,Obd2CanEcuId_7E0,CommLevel_KWP2000);
    obd2can_testerpresent(TRUE,Obd2CanEcuId_7E0,CommLevel_KWP2000);




    cmdif_remotegui_messagebox(CMDIF_CMD_SPECIAL_FUNCTIONS,
                          "SPECIAL FUNCTIONS", CMDIF_REMOTEGUI_ICON_NONE,
                           CMDIF_REMOTEGUI_BUTTONFACE_None, CMDIF_REMOTEGUI_BUTTONFACE_Continue, CMDIF_REMOTEGUI_BUTTONFACE_None,
                           CMDIF_REMOTEGUI_FOCUS_None,
                           "Press Accelerator pedal to the floor, then press continue.");
    
    cmdif_remotegui_wait_user_response(&responsetype, &responsechoice, NULL);



    obd2can_testerpresent(TRUE,Obd2CanEcuId_7E0,CommLevel_KWP2000);


    cmdif_remotegui_messagebox(CMDIF_CMD_SPECIAL_FUNCTIONS,
                           "SPECIAL FUNCTIONS", CMDIF_REMOTEGUI_ICON_NONE,
                            CMDIF_REMOTEGUI_BUTTONFACE_None, CMDIF_REMOTEGUI_BUTTONFACE_None, CMDIF_REMOTEGUI_BUTTONFACE_None,
                            CMDIF_REMOTEGUI_FOCUS_None,
                            "Please wait, this could take a minute.");

/////////////////////   FIRST PART /////////////////

    for(i=0; i<5; i++)
    {
        obd2can_testerpresent(TRUE,Obd2CanEcuId_7E0,CommLevel_KWP2000);
        delays(500, 'm');
    }


    obd2can_dcx_ReadDataByParameterIdentifier(node_ID, response_ID, 0x04,0x0E, buffer, &bytecount);
    if(buffer[3] < 0xA0)
    {
        cmdif_remotegui_messagebox(CMDIF_CMD_SPECIAL_FUNCTIONS,
                          "SPECIAL FUNCTIONS", CMDIF_REMOTEGUI_ICON_NONE,
                           CMDIF_REMOTEGUI_BUTTONFACE_Exit, CMDIF_REMOTEGUI_BUTTONFACE_None, CMDIF_REMOTEGUI_BUTTONFACE_None,
                           CMDIF_REMOTEGUI_FOCUS_None,
                           "Accelerator pedal not pressed to the floor. Please re-start test.");
    
        cmdif_remotegui_wait_user_response(&responsetype, &responsechoice, NULL);

        return S_FAIL;
    }


    for(i=0; i<4; i++)
    {
        obd2can_testerpresent(TRUE,Obd2CanEcuId_7E0,CommLevel_KWP2000);
        delays(500, 'm');
    }

    obd2can_dcx_ReadDataByParameterIdentifier(node_ID, response_ID, 0x04,0x0E, buffer, &bytecount);

    for(i=0; i<6; i++)
    {
        obd2can_testerpresent(TRUE,Obd2CanEcuId_7E0,CommLevel_KWP2000);
        delays(500, 'm');
    }

    obd2can_dcx_ReadDataByParameterIdentifier(node_ID, response_ID, 0x04,0x0E, buffer, &bytecount);

    for(i=0; i<5; i++)
    {
        obd2can_testerpresent(TRUE,Obd2CanEcuId_7E0,CommLevel_KWP2000);
        delays(50, 'm');
    }

    obd2can_dcx_ReadDataByParameterIdentifier(node_ID, response_ID, 0x04,0x0E, buffer, &bytecount);

/////////////////////   SECOND PART /////////////////


    cmdif_remotegui_messagebox(CMDIF_CMD_SPECIAL_FUNCTIONS,
                        "SPECIAL FUNCTIONS", CMDIF_REMOTEGUI_ICON_NONE,
                         CMDIF_REMOTEGUI_BUTTONFACE_None, CMDIF_REMOTEGUI_BUTTONFACE_Continue, CMDIF_REMOTEGUI_BUTTONFACE_None,
                         CMDIF_REMOTEGUI_FOCUS_None,
                         "Release the Accelerator pedal and do not touch it, then press continue.");

    cmdif_remotegui_wait_user_response(&responsetype, &responsechoice, NULL);


    obd2can_testerpresent(TRUE,Obd2CanEcuId_7E0,CommLevel_KWP2000);
    obd2can_testerpresent(TRUE,Obd2CanEcuId_7E0,CommLevel_KWP2000);


    cmdif_remotegui_messagebox(CMDIF_CMD_SPECIAL_FUNCTIONS,
                    "SPECIAL FUNCTIONS", CMDIF_REMOTEGUI_ICON_NONE,
                     CMDIF_REMOTEGUI_BUTTONFACE_None, CMDIF_REMOTEGUI_BUTTONFACE_None, CMDIF_REMOTEGUI_BUTTONFACE_None,
                     CMDIF_REMOTEGUI_FOCUS_None,
                    "Please wait, this could take a minute.");


    obd2can_testerpresent(TRUE,Obd2CanEcuId_7E1,CommLevel_KWP2000);

    obd2can_dcx_read_DTC_info(0x7E1, 0x7E9, 0x02, 0x0D, buffer);

    for(i=0; i<4; i++)
    {
        obd2can_testerpresent(TRUE,Obd2CanEcuId_7E1,CommLevel_KWP2000);
        delays(100, 'm');
    }

    obd2can_testerpresent(TRUE,Obd2CanEcuId_7E0,CommLevel_KWP2000);
    obd2can_dcx_getdtccount(&DTC_count, node_ID);

    obd2can_init(Obd2canInitCustom,0x4C7);
    obd2can_dcx_read_DTC_info(0x747, 0x4C7, 0x02, 0x0D, buffer);
    obd2can_init(Obd2canInitNormal,0);
    
    for(i=0; i<5; i++)
    {
        obd2can_testerpresent(TRUE,Obd2CanEcuId_7E0,CommLevel_KWP2000);
        delays(100, 'm');
    }


    obd2can_init(Obd2canInitCustom,0x504);
    obd2can_dcx_read_DTC_info(0x620, 0x504, 0x02, 0x0D, buffer);

    obd2can_init(Obd2canInitNormal,0);

    for(i=0; i<10; i++)
    {
        obd2can_testerpresent(TRUE,Obd2CanEcuId_7E0,CommLevel_KWP2000);
        delays(100, 'm');
    }

    obd2can_dcx_read_DTC_info(0x7E1, 0x7E9, 0x02, 0x0D, buffer);

    obd2can_dcx_ReadDataByParameterIdentifier(node_ID, response_ID, 0x04,0x0E, buffer, &bytecount);

    for(i=0; i<3; i++)
    {
        obd2can_testerpresent(TRUE,Obd2CanEcuId_7E0,CommLevel_KWP2000);
        delays(100, 'm');
    }

    obd2can_dcx_ReadDataByParameterIdentifier(node_ID, response_ID, 0x04,0x0E, buffer, &bytecount);

    for(i=0; i<5; i++)
    {
        obd2can_testerpresent(TRUE,Obd2CanEcuId_7E0,CommLevel_KWP2000);
        delays(100, 'm');
    }

    obd2can_init(Obd2canInitCustom,0x4C7);
    obd2can_dcx_read_DTC_info(0x747, 0x4C7, 0x02, 0x0D, buffer);
    obd2can_init(Obd2canInitNormal,0);

    for(i=0; i<3; i++)
    {
        obd2can_testerpresent(TRUE,Obd2CanEcuId_7E0,CommLevel_KWP2000);
    }

    obd2can_dcx_getdtccount(&DTC_count, node_ID);

    obd2can_testerpresent(TRUE,Obd2CanEcuId_7E1,CommLevel_KWP2000);

    obd2can_dcx_ReadDataByParameterIdentifier(node_ID, response_ID, 0x04,0x0E, buffer, &bytecount);

    for(i=0; i<7; i++)
    {
        obd2can_testerpresent(TRUE,Obd2CanEcuId_7E0,CommLevel_KWP2000);
        delays(100, 'm');
    }

    obd2can_dcx_read_DTC_info(0x7E1, 0x7E9, 0x02, 0x0D, buffer);

    for(i=0; i<3; i++)
    {
        obd2can_testerpresent(TRUE,Obd2CanEcuId_7E0,CommLevel_KWP2000);
    }

    obd2can_dcx_ReadDataByParameterIdentifier(node_ID, response_ID, 0x04,0x0E, buffer, &bytecount);

    obd2can_testerpresent(TRUE,Obd2CanEcuId_7E1,CommLevel_KWP2000);
    obd2can_dcx_read_DTC_info(0x7E1, 0x7E9, 0x02, 0x0D, buffer);
    
    for(i=0; i<3; i++)
    {
        obd2can_testerpresent(TRUE,Obd2CanEcuId_7E0,CommLevel_KWP2000);
        delays(100, 'm');
    }

    obd2can_dcx_getdtccount(&DTC_count, node_ID);

    for(i=0; i<3; i++)
    {
        obd2can_testerpresent(TRUE,Obd2CanEcuId_7E1,CommLevel_KWP2000);
    }

    obd2can_dcx_read_DTC_info(0x7E1, 0x7E9, 0x02, 0x0D, buffer);

    for(i=0; i<7; i++)
    {
        obd2can_testerpresent(TRUE,Obd2CanEcuId_7E0,CommLevel_KWP2000);
        delays(100, 'm');
    }


    obd2can_dcx_ReadDataByParameterIdentifier(node_ID, response_ID, 0x04,0x0E, buffer, &bytecount);

////////////////////   LAST PART /////////////////

    cmdif_remotegui_messagebox(CMDIF_CMD_SPECIAL_FUNCTIONS,
                    "SPECIAL FUNCTIONS", CMDIF_REMOTEGUI_ICON_NONE,
                     CMDIF_REMOTEGUI_BUTTONFACE_None, CMDIF_REMOTEGUI_BUTTONFACE_None, CMDIF_REMOTEGUI_BUTTONFACE_None,
                     CMDIF_REMOTEGUI_FOCUS_None,
                    "Checking ETC relearn.");

    for(i=0; i<5; i++)
    {
        obd2can_testerpresent(TRUE,Obd2CanEcuId_7E0,CommLevel_KWP2000);
    }

    timeout = 0xFF;
    routine = 0x10;

    while(timeout--)
    {
        status = obd2can_dcx_results_routine_by_local_id(node_ID, response_ID, routine,0);
        if(status == S_SUCCESS)
            break;

        obd2can_dcx_initiate_diagnostic_operation(FALSE,node_ID, response_ID, diagnosticRequestChrysler_92);
    }

    obd2can_testerpresent(TRUE,Obd2CanEcuId_7E0,CommLevel_KWP2000);
    obd2can_dcx_clear_diagnostic_information_extended(node_ID, response_ID);
    obd2can_dcx_ecu_reset_request(FALSE,node_ID,NULL,hardReset,TRUE);
    
    for(i=0; i<3; i++)
    {
        obd2can_testerpresent(TRUE,Obd2CanEcuId_7E0,CommLevel_KWP2000);
    }

    obd2can_dcx_read_DTC_info(0x7E1, 0x7E9, 0x02, 0x0D, buffer);

    for(i=0; i<6; i++)
    {
        obd2can_testerpresent(TRUE,Obd2CanEcuId_7E0,CommLevel_KWP2000);
    }

    obd2can_dcx_read_DTC_info(0x7E1, 0x7E9, 0x02, 0x0D, buffer);


    for(i=0; i<4; i++)
    {
        obd2can_testerpresent(TRUE,Obd2CanEcuId_7E0,CommLevel_KWP2000);
    }


    if(status == S_SUCCESS)
      return S_SUCCESS;
    else
      return S_FAIL;

}

//------------------------------------------------------------------------------
// ECU Reset 
// Inputs:  u32 ecm_id, VehicleCommLevel commlevel
// Return:  u8  status
// Engineer: Tristen Pierson & Rhonda Rusak
//------------------------------------------------------------------------------
u8 obd2can_dcx_power_on_reset(u32 ecm_id, VehicleCommLevel commlevel)
{
    u8 status;
   
    if(commlevel == CommLevel_KWP2000)
    {

        obd2can_dcx_initiate_diagnostic_operation(FALSE,ecm_id, 
                      ECM_RESPONSE_ID(ecm_id), diagnosticRequestChrysler_92);
        
        status = obd2can_dcx_ecu_reset_request(FALSE,ecm_id,ECM_RESPONSE_ID(ecm_id),
                                               hardReset,TRUE);
        if(status == S_FAIL)
          status = obd2can_dcx_reset_22();
               
    }
    else
    {
        // Comm Level not valid
        status = S_INPUT;        
    }
    
    return status;
}

//------------------------------------------------------------------------------
// Check for bootloader hash values for 2013 and 2014 GPEC processors. 
// Inputs:  u32 response    CMDIF response
// Return:  u8  status
// Engineer: Ricardo Cigarroa
// NOTE: After performing this check, processor must have power cycled.
//------------------------------------------------------------------------------
u8 obd2can_dcx_do_check(u8 *response, bool checksupportfiles, u16 ecm_type)
{
    u8 status;
    u8 buffer[10];
    u8 variant_id; 
    u8 ecm_boot_version[10]; 
    u8 *databuffer = NULL;
    u8 recovery_check = 0;
    u16 cksmcount = 0;
    u32 length;  
  
    enum 
    {
        ORG_BOOT_HASH     = 1,
        MOD_BOOT_HASH     = 2,
        UNKNOWN_BOOT_HASH = 3
    }BootHashStatus;

    u8 org_boot_hash_2013[20]       = {0x0C,0x40,0xC0,0x3F,0x41,0x57,0xAC,0x4F,0xEF,0xDE,
                                        0x83,0xD0,0x57,0x24,0xD6,0xC0,0x2F,0xB4,0x8C,0xE4};
    u8 mod_boot_hash_2013[20]       = {0xED,0x5D,0x51,0x96,0x9B,0xBC,0x7A,0x6D,0x8A,0xB6,
                                        0x1B,0x20,0x8D,0x26,0xF9,0x7A,0x9D,0xD1,0xF0,0xEC};
    u8 org_boot_hash_2014_2[20]     = {0x39,0xAC,0x5B,0xA9,0x77,0xF1,0x51,0x07,0x04,0x08,
                                        0x1D,0xA1,0x8E,0x8F,0x9A,0xC7,0xD3,0x6E,0x56,0xD6};
    u8 mod_boot_hash_2014_2[20]     = {0xE7,0x73,0xFA,0x54,0x76,0x25,0x74,0xF1,0xB4,0xC4,
                                        0x10,0x1A,0x61,0xED,0x65,0xCC,0x2D,0x89,0x23,0x63};
    u8 org_boot_hash_2015_2[20]     = {0xA7,0xD8,0xA9,0x9F,0x42,0xEB,0xDC,0xC0,0x42,0xC6,
                                        0xC5,0xBF,0xF5,0x96,0x3E,0x91,0xE7,0x3D,0x49,0x09};
    u8 mod_boot_hash_2015_2[20]     = {0x21,0xE1,0x3A,0x63,0xEF,0x5A,0xAB,0xEC,0x0D,0x94,
                                        0x0D,0x35,0xD1,0x77,0xEC,0xAC,0x03,0x94,0x11,0x89};
    u8 org_boot_hash_2015_2A[20]    = {0xE1,0xEF,0x16,0x2F,0x7F,0xA3,0x9F,0xAD,0x9B,0xC3,
                                        0x6A,0x91,0x3F,0x1B,0x0D,0xF1,0x1F,0x90,0xCF,0xD8};
    u8 mod_boot_hash_2015_2A[20]    = {0x01,0x45,0xAE,0x67,0x8E,0xE8,0xEF,0xB0,0x50,0xF2,
                                        0xD3,0x27,0xB7,0x41,0x6E,0x8F,0xB8,0xE5,0xE3,0x0B};

    u8 *pOrgBootHash;
    u8 *pModBootHash;
    
    
    *response = CMDIF_ACK_FAILED; 
    status = S_SUCCESS; 
    
    databuffer = __malloc(21);
    if(databuffer == NULL)
    {
        log_push_error_point(0x309A);
        status = S_FAIL;
        goto obd2can_dcx_do_check_done; 
    }
    
    length = 0; 
    
Recovery_check:
    if (checksupportfiles == FALSE)
    {

        if (!ECM_IsUseVariantId(ecm_type))
        {
            status = S_NOTREQUIRED; 
            goto obd2can_dcx_do_check_done;
        }
        
        obd2can_init(Obd2canInitNormal,0);
        obd2can_ping(Obd2CanEcuId_7E0, FALSE);
        obd2can_dcx_initiate_diagnostic_operation(FALSE,0x7E0, 0x7E8, diagnosticRequestChrysler_92);
        status = obd2can_dcx_initiate_diagnostic_operation(FALSE,0x7E0, 0x7E8, diagnosticRequestChrysler_85);
        
        if (status == S_SUCCESS)
        {
            status = obd2can_unlockecm(UnlockTask_Upload,ecm_type,FALSE);
        }
        
        if(status != S_SUCCESS)
        {
            if(recovery_check == 1)
            {
                status = S_SUCCESS;
                goto obd2can_dcx_do_check_done;              
            }
              
            log_push_error_point(0x309B);
            status = S_FAIL; 
            goto obd2can_dcx_do_check_done;
        }
        
        // Check bootloader version first.
        if(obd2can_dcx_request_byidentifier(0x9E, 0x7E0, 0x7E8, 0) == S_SUCCESS)
        {
            variant_id = obd2_rxbuffer[4];
            memcpy(ecm_boot_version, &obd2_rxbuffer[10], 10);
            switch (variant_id)
            {
                case 0x23:
                    // Check for 2013 GPEC2
                    strcpy((void*)buffer, "05150739AB");
                    if (memcmp(ecm_boot_version, buffer, 10) != 0)
                    {
                        // Check for 2012 GPEC2
                        strcpy((void*)buffer, "05150475AD");
                        if (memcmp(ecm_boot_version, buffer, 10) != 0)
                        {
                            // Unknown bootloader
                            log_push_error_point(0x309C);
                            status = S_FAIL;
                            goto obd2can_dcx_do_check_done;
                        }
                        else
                        {
                            // 2012 GPEC2 does not require bootloader check. 
                            status = S_SUCCESS;
                            *response = CMDIF_ACK_OK;
                            settingstune.variant_ID = 1;
                            gObd2info.ecu_block[0].partnumbers.dcx.variant_id = 1;
                            goto obd2can_dcx_do_check_done;
                        }
                    }
                    break;
                case 0x25:
                    strcpy((void*)buffer, "05150826AA");
                    if (memcmp(ecm_boot_version, buffer, 10) != 0)
                    {
                        log_push_error_point(0x309D);
                        status = S_FAIL;
                        goto obd2can_dcx_do_check_done;
                    }
                    break;
                case 0x26: // Check for 2015 GPEC2
                    strcpy((void*)buffer, "05150998AA");
                    if (memcmp(ecm_boot_version, buffer, 10) != 0)
                    {
                        log_push_error_point(0x309D);
                        status = S_FAIL;
                        goto obd2can_dcx_do_check_done;
                }
                break;
/*                case 0x42: // Check for 2014 GPEC2A 
                    strcpy((void*)buffer, "05150765AC");
                    if (memcmp(ecm_boot_version, buffer, 10) != 0)
                    {
                        log_push_error_point(0x309D);
                        status = S_FAIL;
                        goto obd2can_dcx_do_check_done;
                    }
                    break;
*/
                case 0x44: // Check for 2015 GPEC2A
                    strcpy((void*)buffer, "05150997AA");
                    if (memcmp(ecm_boot_version, buffer, 10) != 0)
                    {
                        log_push_error_point(0x309B);
                        status = S_FAIL;
                        goto obd2can_dcx_do_check_done;
                    }
                    break;
                default:
                    log_push_error_point(0x309E);
                    status = S_FAIL;
                    goto obd2can_dcx_do_check_done;
            }
        }
        else
        {
            if(recovery_check == 1)
            {
                status = S_SUCCESS; 
                goto obd2can_dcx_do_check_done;              
            }

            log_push_error_point(0x309F);
            status = S_FAIL; 
            goto obd2can_dcx_do_check_done;
        }
        
        // Make sure variant_id is stored in case this is a recovery
        settingstune.variant_ID = variant_id;
        gObd2info.ecu_block[0].partnumbers.dcx.variant_id = variant_id;
        
        // Now we check for the bootloader hash
        if(obd2can_dcx_start_routine_by_local_id_rx(0x7E0,0xDF,databuffer,&length) == S_SUCCESS)
        {
            switch (variant_id)
            {
                case 0x23:
                    pOrgBootHash = (u8*)&org_boot_hash_2013;
                    pModBootHash = (u8*)&mod_boot_hash_2013;
                    break;
                case 0x25:
                    pOrgBootHash = (u8*)&org_boot_hash_2014_2;
                    pModBootHash = (u8*)&mod_boot_hash_2014_2;
                    break;
                case 0x26:
                    pOrgBootHash = (u8*)&org_boot_hash_2015_2;
                    pModBootHash = (u8*)&mod_boot_hash_2015_2;
                break;
/*              case 0x42:
                    pOrgBootHash = (u8*)&org_boot_hash_2014_2A;
                    pModBootHash = (u8*)&mod_boot_hash_2014_2A;
                    break;
*/
                case 0x44:
                    pOrgBootHash = (u8*)&org_boot_hash_2015_2A;
                    pModBootHash = (u8*)&mod_boot_hash_2015_2A;
                    break;
                default:
                    // This shouldn't happen
                    log_push_error_point(0x30A0);
                    status = S_FAIL; 
                    goto obd2can_dcx_do_check_done;
                    break;
            }        
            
            // Check for original bootloader hash
            BootHashStatus = ORG_BOOT_HASH;
            if (memcmp(&databuffer[1], pOrgBootHash, 10) != 0)
            {
                 // Check for modified bootloader hash
                BootHashStatus = MOD_BOOT_HASH; 
                if (memcmp(&databuffer[1], pModBootHash, 10) != 0)
                {
                    BootHashStatus = UNKNOWN_BOOT_HASH;
                }
            }
        }
        else
        {
            log_push_error_point(0x30A1);
            status = S_FAIL;
            goto obd2can_dcx_do_check_done;
        }

        switch (BootHashStatus)
        {
        case ORG_BOOT_HASH:
            if((variant_id == 0x26)||(variant_id == 0x44))
                status = S_FAIL;
            else
            {
                flasherinfo->isfullreflash_already_set = TRUE;
                flasherinfo->isfullreflash[0] = TRUE;
                if (!SETTINGS_IsMarried())
                    SETTINGS_SetFullFlash(0);
                status = S_SUCCESS;
            }
            break;

        case MOD_BOOT_HASH:
            if((variant_id == 0x26)||(variant_id == 0x44))
            {
                flasherinfo->isfullreflash_already_set = TRUE;
                flasherinfo->isfullreflash[0] = TRUE;
                if (!SETTINGS_IsMarried())
                    SETTINGS_SetFullFlash(0);
                status = S_SUCCESS;
            }
            else
            {
                flasherinfo->isfullreflash_already_set = TRUE;
                flasherinfo->isfullreflash[0] = FALSE;
                if (!SETTINGS_IsMarried())
                    SETTINGS_ClearFullFlash(0);
                status = S_SUCCESS;
            }
            break;

            case UNKNOWN_BOOT_HASH:
                log_push_error_point(0x309E);    
                status = S_FAIL;                 
                *response = CMDIF_ACK_VEH_FLASHED_UNKNOWN_TUNER;
/*
                flasherinfo->isfullreflash_already_set = TRUE;
                flasherinfo->isfullreflash[0] = FALSE;
                if (!SETTINGS_IsMarried())
                    SETTINGS_ClearFullFlash(0);
                status = S_SUCCESS;
*/
                break;
            default:
                status = S_FAIL;
                break;
            }
            SETTINGS_SetTuneAreaDirty();
            settings_update(SETTINGS_UPDATE_IF_DIRTY);


    }
    else //check support files TRUE.
    {
        /*Check for download support files. EX: E2 Stock file and .csf file. We only
        check for these files after performing an upload and checksums on the 
        flash.bin file.*/

        if(ECM_IsE2Required(ecm_type))
        {
            /*validate for E2_stock.ess file*/
            status = e2_stock_validatefile(STOCK_E2_FILENAME,response); 
            if (status != S_SUCCESS)
            {
                log_push_error_point(0x30A4);
                goto obd2can_dcx_do_check_done;
            }

        }
        
        if (status == S_SUCCESS)
        {
            if (ECM_IsCsfRequired(ecm_type) && ECM_IsRSAsignatureRequired(ecm_type))
            {
                status = checksum_csf_validate_file(CSFTEMPFILE,&cksmcount,response); 
                if (status != S_SUCCESS)
                {
                    log_push_error_point(0x30A5);
                }
            } 
        }
        
        if (status == S_SUCCESS)
        {
            if(ECM_IsUseVariantId(ecm_type) && IsVariantId_DCX_GPEC2)
            {
                /*On return to stock, we set the flash type (full or cal) here 
                based on settings. If recovery file and set to cal only, we check the boothash.*/
                if(SETTINGS_IsStockRecovery() || SETTINGS_IsDownloadFail()||(flasherinfo->flashtype == CMDIF_ACT_STOCK_FLASHER))
                {
                    flasherinfo->isfullreflash_already_set = TRUE;
                    
                    if(SETTINGS_IsFullFlash(0))
                    {
                        flasherinfo->isfullreflash[0] = TRUE;
                    }
                    else
                    {
                        flasherinfo->isfullreflash[0] = FALSE;
                    }
                     if((SETTINGS_IsStockRecovery()) && (flasherinfo->isfullreflash[0] == FALSE))
                     {
                        gObd2info.oemtype = OEM_TYPE;
                        checksupportfiles = FALSE;
                        recovery_check = 1;
                        goto Recovery_check;
                     }

                }
            }
        }
    }
    
obd2can_dcx_do_check_done: 
    if(databuffer)
    {
        __free(databuffer); 
    }
   
    return status; 
}

/**
 *  @brief Get DCX CAN OBD2 Info
 *  
 *  @param [in] obd2_info_block_t OBD2 info block
 *
 *  @return status
 */
u8 obd2can_dcx_getinfo(obd2_info_block *block)
{
    u8  buffer[64];
    u16 bufferlength;
    u8  charcount;
    u8  status;

    block->partnumber_count = 0;

    if (block->commtype == CommType_CAN && block->commlevel != CommLevel_Unknown)
    {
        status = obd2can_dcx_request_vehicle_information(block->ecu_id, 0x04,
                                                         buffer, &bufferlength);
        /* Get Part Number */
        if (status == S_SUCCESS)
        {
            block->partnumber_count = 1;
                        
            /* Check part number length first */
            charcount = 0;
            if (strlen((char*)buffer) <= bufferlength)
            {
                while(isprint(buffer[charcount]))
                {
                   charcount++;
                }
                if (charcount == strlen((char*)buffer))
                {
                   /* Store part number */
                   strcpy((char*)block->partnumbers.dcx.part_number,
                          (char*)buffer);
                }
            }
        }
 
        /* Get vehicle part type */
        status = obd2can_dcx_getparttype(block->ecu_id,
                                         block->partnumbers.dcx.part_type_description,
                                         &block->partnumbers.dcx.part_type_number);
        if(status != S_SUCCESS)
        {
          block->partnumbers.dcx.part_type_description[0] = NULL;
          block->partnumbers.dcx.part_type_number = 0;
        }
        
        /* Get vehicle bootloader version */
        if (block->ecutype == EcuType_ECM)
        {
            status = obd2can_dcx_get_boot_version(block->ecu_id,
                                                  block->partnumbers.dcx.boot_version);
            if(status != S_SUCCESS)
            {
                block->partnumbers.dcx.boot_version[0] = NULL;
            }
        }
    }
    return S_SUCCESS;
}

u32 obd2can_dcx_findMatchingID(u32 ID, u8 type)
{
    u8 x = 0;
    u32 *type_in = NULL;
    u32 *type_out = NULL;
    u32 answer = 0;
    u32 Node_IDs[12]={0x7E0,0x7E1,0x784,0x6E0,0x622,0x600,0x6A0,0x7F8,0x6B0,0x620,0x747,0x740};
    u32 Device_IDs[12]={0x7E8,0x7E9,0x785,0x51C,0x484,0x500,0x514,0x53F,0x516,0x504,0x4C7,0x4C0};

    if(type == 0)
    {
        type_in = Node_IDs;
        type_out = Device_IDs;
    }
    else
    {
        type_in = Device_IDs;
        type_out = Node_IDs;
    }

    for(x=0; x<12; x++)
    {

      if(ID == type_in[x])
        break;
      
    }

    if((x== 12)&&(type == 0))
      answer = ECM_ID(ID);
    else if ((x== 12)&&(type == 1))
      answer = ECM_RESPONSE_ID(ID);
    else
      answer = type_out[x];

    return(answer);
}