/**
  **************** (C) COPYRIGHT 2011 SCT Performance, LLC *********************
  * File Name          : dca_ecm_defs.c
  * Device / System    : SCT products using the "CommonCode" platform
  * Author             : Quyen Leba
  *
  * Version            : 1 
  * Date               : 05/02/2011
  * Description        : 
  *                    : 
  *
  *
  * History            : 05/02/2011 M. Davis
  *                    :   Add SCT Copyright header,
  *
  ******************************************************************************
  */

#include "dcx_ecm_utility_defs.h"
#include "dcx_ecm_defs.h"
#include "dcx_veh_defs.h"

enum
{
    FILE_0      = 0,
    FILE_1      = 1,
    FILE_2      = 2,
    FILE_3      = 3,
};

typedef struct
{
    u32 filesize;
    u8 *filename;
}IndexedAppendFile;

//Filename used in some processor to append file content instead of upload from
//processor. Note: firmware won't rely on reading filesize from file while
//doing stock verification
const IndexedAppendFile IndexedAppendFiles[1] = {
    [FILE_0] = {65536,  "/XXX_1C0000_01.bin"}
};


//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
const ECM_Def dcx_ecm_defs[] =
{
   
    //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    [1] =   //Chrysler NCG1 (SCI_B)                                      //0x01
    {
        .index = 1,
        .ecm_id = 0x00, .reponse_ecm_id = 0x00,                                 //added 2 parameters: part_type, E2_needed. 
        .unlock_flags = UNLOCK_FLAG_NONE,
        .upload_unlock_id =   UnlockId_SCIB_UNLOCK ,
        .download_unlock_id =   UnlockId_SCIB_UNLOCK ,
        .checksum_id = ChecksumId_None,
        .finalizing_id = FinalId_None,
        .flags = SIMPLE_WAKEUP_CHECK_UPLOAD | SIMPLE_WAKEUP_CHECK_DOWNLOAD | SINGLE_UNLOCK_ONLY |
            UPLOAD_ADDR_TYPE_3B | UPLOAD_BY_CMD_35_SINGLE_36 |
            USE_ERASE_COMMAND |
            DOWNLOAD_ADDR_TYPE_3B | DOWNLOAD_REQUEST_TYPE_3B |
            DOWNLOAD_BLOCK_BACKWARD | DOWNLOAD_REQUIRE_CMD_37,
        .extended_flags = ECM_FLAGS_NONE,
        .commtype = CommType_SCIB, .commlevel = CommLevel_Unknown,
        .eraseall_cmd = {EraseCmdInfoFlags_None,EraseCmdInfoCount1,{0x00}},
        .erasecal_cmd = {EraseCmdInfoFlags_None,EraseCmdInfoCount1,{0x00}},
        .vid_address = 0, .pats_address = 0,
        .testerpresent_frequency = 10,
        
        .data_offset = 0x0,
        .ecm_startingaddr = 0x0,
        
        .util_upload = {NULL,NULL,NULL},
        .util_dnload = {NULL,NULL,NULL},
        .maxuploadblocksize = 0x400, .maxdownloadblocksize = 0x400,
        .os_block_read_skipblock_info = NULL,
        .os_block_read =
        {
            {0x008000,  0x78000,     ORDER_0 | MEMBLOCK_FLAGS_NONE, 0},
            {0,  0,     ORDER_1 | MEMBLOCK_FLAGS_NONE, 0},
            {0,  0,     ORDER_2 | MEMBLOCK_FLAGS_NONE, 0},
            {0,  0,     ORDER_3 | MEMBLOCK_FLAGS_NONE, 0},
            {0,  0,     ORDER_4 | MEMBLOCK_FLAGS_NONE, 0},
        },
        .os_block =
        {
            {0x008000,  0x78000,     ORDER_0 | MEMBLOCK_FLAGS_NONE, 0},
            {0,  0,     ORDER_1 | MEMBLOCK_FLAGS_NONE, 0},
            {0,  0,     ORDER_2 | MEMBLOCK_FLAGS_NONE, 0},
            {0,  0,     ORDER_3 | MEMBLOCK_FLAGS_NONE, 0},
            {0,  0,     ORDER_4 | MEMBLOCK_FLAGS_NONE, 0},
        },
        .cal_block =
        {
            {0x008000,  0x78000,     ORDER_0 | MEMBLOCK_FLAGS_NONE, 0},
            {0,  0,     ORDER_1 | MEMBLOCK_FLAGS_NONE, 0},
            {0,  0,     ORDER_2 | MEMBLOCK_FLAGS_NONE, 0},
            {0,  0,     ORDER_3 | MEMBLOCK_FLAGS_NONE, 0},
            {0,  0,     ORDER_4 | MEMBLOCK_FLAGS_NONE, 0},
        },
    },
    //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    [2] =   //Chrysler NGC2  (SCI_B)                                     //0x02
    { 
        .index = 2,
        .ecm_id = 0x00, .reponse_ecm_id = 0x00, 
        .unlock_flags = UNLOCK_FLAG_NONE,
        .upload_unlock_id = UnlockId_SCIB_UNLOCK,
        .download_unlock_id = UnlockId_SCIB_UNLOCK,
        .checksum_id = ChecksumId_None,
        .finalizing_id = FinalId_None,
        .flags = SIMPLE_WAKEUP_CHECK_UPLOAD | SIMPLE_WAKEUP_CHECK_DOWNLOAD | SINGLE_UNLOCK_ONLY |
            UPLOAD_ADDR_TYPE_3B | UPLOAD_BY_CMD_35_SINGLE_36 |
            USE_ERASE_COMMAND |
            DOWNLOAD_ADDR_TYPE_3B | DOWNLOAD_REQUEST_TYPE_3B |
            DOWNLOAD_BLOCK_BACKWARD | DOWNLOAD_REQUIRE_CMD_37,
        .extended_flags = ECM_FLAGS_NONE,
        .commtype = CommType_SCIB, .commlevel = CommLevel_Unknown,
        .eraseall_cmd = {EraseCmdInfoFlags_None,EraseCmdInfoCount1,{0x00}},
        .erasecal_cmd = {EraseCmdInfoFlags_None,EraseCmdInfoCount1,{0x00}},
        .vid_address = 0, .pats_address = 0,
        .testerpresent_frequency = 10,
        
        .data_offset = 0x0,
        .ecm_startingaddr = 0x0,
        
        .util_upload = {NULL,NULL,NULL},
        .util_dnload = {NULL,NULL,NULL},
        .maxuploadblocksize = 0x400, .maxdownloadblocksize = 0x400,
        .os_block_read_skipblock_info = NULL,
        .os_block_read =
        {
            {0x002000,  0xe000,     ORDER_0 | MEMBLOCK_FLAGS_NONE, 0},
            {0x012000,  0xd000,     ORDER_1 | MEMBLOCK_FLAGS_NONE, 0},
            {0x092000,  0xe000,     ORDER_2 | MEMBLOCK_FLAGS_NONE, 0},
            {0x082000,  0xe000,     ORDER_3 | MEMBLOCK_FLAGS_NONE, 0},
            {0, 0, ORDER_4 | MEMBLOCK_FLAGS_NONE, 0},
        },
        .os_block =
        {
            {0x002000,  0xe000,     ORDER_0 | MEMBLOCK_FLAGS_NONE, 0},
            {0x012000,  0xd000,     ORDER_1 | MEMBLOCK_FLAGS_NONE, 0},
            {0x092000,  0xe000,     ORDER_2 | MEMBLOCK_FLAGS_NONE, 0},
            {0x082000,  0xe000,     ORDER_3 | MEMBLOCK_FLAGS_NONE, 0},
            {0, 0, ORDER_4 | MEMBLOCK_FLAGS_NONE, 0},
        },
        .cal_block =
        {
            {0x002000,  0xe000,     ORDER_0 | MEMBLOCK_FLAGS_NONE, 0},
            {0x012000,  0xd000,     ORDER_1 | MEMBLOCK_FLAGS_NONE, 0},
            {0x092000,  0xe000,     ORDER_2 | MEMBLOCK_FLAGS_NONE, 0},
            {0x082000,  0xe000,     ORDER_3 | MEMBLOCK_FLAGS_NONE, 0},
            {0, 0, ORDER_4 | MEMBLOCK_FLAGS_NONE, 0},
        },
    },
    //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    [3] =   //Chrysler NGC3 (SCI_B)                                      //0x03
    {
        .index = 3,
         .ecm_id = 0x00, .reponse_ecm_id = 0x00, 
         .unlock_flags = UNLOCK_FLAG_NONE,
        .upload_unlock_id = UnlockId_SCIB_UNLOCK,
        .download_unlock_id = UnlockId_SCIB_UNLOCK,
        .checksum_id = ChecksumId_None,
        .finalizing_id = FinalId_None,
        .flags = SIMPLE_WAKEUP_CHECK_UPLOAD | SIMPLE_WAKEUP_CHECK_DOWNLOAD | SINGLE_UNLOCK_ONLY |
            UPLOAD_ADDR_TYPE_3B | UPLOAD_BY_CMD_35_SINGLE_36 |
            USE_ERASE_COMMAND |
            DOWNLOAD_ADDR_TYPE_3B | DOWNLOAD_REQUEST_TYPE_3B |
            DOWNLOAD_BLOCK_BACKWARD | DOWNLOAD_REQUIRE_CMD_37,
        .extended_flags = ECM_FLAGS_NONE,
        .commtype = CommType_SCIB, .commlevel = CommLevel_Unknown,
        .eraseall_cmd = {EraseCmdInfoFlags_None,EraseCmdInfoCount1,{0x00}},
        .erasecal_cmd = {EraseCmdInfoFlags_None,EraseCmdInfoCount1,{0x00}},
        .vid_address = 0, .pats_address = 0,
        .testerpresent_frequency = 10,
        
        .data_offset = 0x0,
        .ecm_startingaddr = 0x0,
        
        .util_upload = {NULL,NULL,NULL},
        .util_dnload = {NULL,NULL,NULL},
        .maxuploadblocksize = 0x400, .maxdownloadblocksize = 0x400,
        .os_block_read_skipblock_info = NULL,
        .os_block_read =
        {
            {0x002000,  0xe000,     ORDER_0 | MEMBLOCK_FLAGS_NONE, 0},
            {0x012000,  0xc000,     ORDER_1 | MEMBLOCK_FLAGS_NONE, 0},
            {0x092000,  0xe000,     ORDER_2 | MEMBLOCK_FLAGS_NONE, 0},
            {0x082000,  0xe000,     ORDER_3 | MEMBLOCK_FLAGS_NONE, 0},
            {0, 0, ORDER_4 | MEMBLOCK_FLAGS_NONE, 0},
        },
        .os_block =
        {
            {0x002000,  0xe000,     ORDER_0 | MEMBLOCK_FLAGS_NONE, 0},
            {0x012000,  0xc000,     ORDER_1 | MEMBLOCK_FLAGS_NONE, 0},
            {0x092000,  0xe000,     ORDER_2 | MEMBLOCK_FLAGS_NONE, 0},
            {0x082000,  0xe000,     ORDER_3 | MEMBLOCK_FLAGS_NONE, 0},
            {0, 0, ORDER_4 | MEMBLOCK_FLAGS_NONE, 0},
        },
        .cal_block =
        {
            {0x002000,  0xe000,     ORDER_0 | MEMBLOCK_FLAGS_NONE, 0},
            {0x012000,  0xc000,     ORDER_1 | MEMBLOCK_FLAGS_NONE, 0},
            {0x092000,  0xe000,     ORDER_2 | MEMBLOCK_FLAGS_NONE, 0},
            {0x082000,  0xe000,     ORDER_3 | MEMBLOCK_FLAGS_NONE, 0},
            {0, 0, ORDER_4 | MEMBLOCK_FLAGS_NONE, 0},
        }, 
    },
    //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    [4] =   //Chrysler 2010-2013 GPEC2 ECM (CAN)                         //0x04
    {
        .index = 4,
        .ecm_id = 0x7E0, .reponse_ecm_id = 0x7E8,  
        .unlock_flags = UNLOCK_FLAG_NONE,
        .upload_unlock_id = UnlockId_HEMI_BOOTLOADER_SECURITY_UPLOAD,
        .download_unlock_id = UnlockId_HEMI_BOOTLOADER_SECURITY_DOWNL,
        .checksum_id = ChecksumId_None,
        .finalizing_id = FinalId_None,
        .flags = SIMPLE_WAKEUP_CHECK_UPLOAD | SIMPLE_WAKEUP_CHECK_DOWNLOAD | 
            UPLOAD_ADDR_TYPE_4B | UPLOAD_BY_CMD_35_MULTI_36 | USE_ERASE_COMMAND |
            DOWNLOAD_ADDR_TYPE_4B | DOWNLOAD_REQUEST_TYPE_4B | DOWNLOAD_REQUIRE_CMD_37 |
            COOLDOWNTIME_SHORT_1,
        .extended_flags = REQUIRE_DOWNLOAD_FINALIZING | CSF_REQUIRED | USE_FINGERPRINT_INTERLOCK
        | E2_REQUIRED | OVERLAY_FLASH | RSA_SIGNATURE | PREDOWNLOAD_TASK | USE_VARIANT_ID | REQUIRE_KEY_DTC
        | DOWNLOAD_HEALTH_CHECK_STRICT | TRANS_INFO | CLEAR_KAM_AFTER_DOWNLOAD | REQUIRE_KEYOFF_BEFORE_DOWNLOAD,
        .commtype = CommType_CAN, .commlevel = CommLevel_KWP2000,
        .eraseall_cmd = {EraseCmdInfoFlags_None,EraseCmdInfoCount1,{0x00}},
        .erasecal_cmd = {EraseCmdInfoFlags_None,EraseCmdInfoCount1,{0x00}},
        .vid_address = 0, .pats_address = 0,
        .testerpresent_frequency = 0,
        
        .data_offset = 0x0,
        .ecm_startingaddr = 0x0,
        
        .util_upload = {NULL,NULL,NULL},
        .util_dnload = {(utility_info*)&utilityinfolist[1],NULL,NULL},
        .maxuploadblocksize = 0xFE0, .maxdownloadblocksize = 0xFFC,
        .os_block_read_skipblock_info = NULL,
        .os_block_read =
        {
            {0x000000,  0x300000,     ORDER_0 | MEMBLOCK_FLAGS_NONE, 0},
            {0,  0,     ORDER_1 | MEMBLOCK_FLAGS_NONE, 0},
            {0,  0,     ORDER_2 | MEMBLOCK_FLAGS_NONE, 0},
            {0,  0,     ORDER_3 | MEMBLOCK_FLAGS_NONE, 0},
            {0,  0,     ORDER_4 | MEMBLOCK_FLAGS_NONE, 0},
        },
        .os_block =
        {
            {0x040000,  0x280000,     ORDER_0 | MEMBLOCK_FLAGS_NONE, 0},
            {0x2C0000,  0x40000,      ORDER_1 | MEMBLOCK_FLAGS_NONE, 0},
            {0,  0,        ORDER_2 | MEMBLOCK_FLAGS_NONE, 0},
            {0,  0,        ORDER_3 | MEMBLOCK_FLAGS_NONE, 0},
            {0,  0,        ORDER_4 | MEMBLOCK_FLAGS_NONE, 0},
        },
        .cal_block =
        {
            {0x2C0000,  0x40000,      ORDER_0 | MEMBLOCK_FLAGS_NONE, 0},
            {0,  0,     ORDER_1 | MEMBLOCK_FLAGS_NONE, 0},
            {0,  0,     ORDER_2 | MEMBLOCK_FLAGS_NONE, 0},
            {0,  0,        ORDER_3 | MEMBLOCK_FLAGS_NONE, 0},
            {0,  0,        ORDER_4 | MEMBLOCK_FLAGS_NONE, 0},
        },
    },
    //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    [5] =   //Chrysler NGC4A 2010 and earlier (CAN)                      //0x05
    {
        .index = 5,
        .ecm_id = 0x7E0, .reponse_ecm_id = 0x7E8, 
        .unlock_flags = UNLOCK_FLAG_NONE,
        .upload_unlock_id = UnlockId_SCIB_UNLOCK,
        .download_unlock_id = UnlockId_SCIB_UNLOCK,
        .checksum_id = ChecksumId_None,
        .finalizing_id = FinalId_None,
        .flags = SIMPLE_WAKEUP_CHECK_UPLOAD | SIMPLE_WAKEUP_CHECK_DOWNLOAD | 
          COOLDOWNTIME_SHORT_3 | UPLOAD_ADDR_TYPE_3B | UPLOAD_BY_CMD_23 |
          USE_ERASE_COMMAND | DOWNLOAD_ADDR_TYPE_3B | DOWNLOAD_REQUEST_TYPE_3B |
          UTILITY_CHECK,
        .extended_flags = REQUIRE_DOWNLOAD_FINALIZING | CSF_REQUIRED | OVERLAY_FLASH
          | REQUIRE_KEY_DTC | DOWNLOAD_HEALTH_CHECK_STRICT,
        .commtype = CommType_CAN, .commlevel = CommLevel_KWP2000,
        .eraseall_cmd = {EraseCmdInfoFlags_None,EraseCmdInfoCount1,{0x00}},
        .erasecal_cmd = {EraseCmdInfoFlags_None,EraseCmdInfoCount1,{0x00}},
        .vid_address = 0, .pats_address = 0,
        .testerpresent_frequency = 0,
        
        .data_offset = 0x0,
        .ecm_startingaddr = 0x0,
        
        .util_upload = {(utility_info*)&utilityinfolist[3],NULL,NULL},
        .util_dnload = {(utility_info*)&utilityinfolist[3],NULL,NULL},
        .maxuploadblocksize = 0x800, .maxdownloadblocksize = 0x800,
        .os_block_read_skipblock_info = NULL,
        .os_block_read =
        {
            {0x000000,  0x200000,     ORDER_0 | MEMBLOCK_FLAGS_NONE, 0},
            {0,  0,     ORDER_1 | MEMBLOCK_FLAGS_NONE, 0},
            {0,  0,     ORDER_2 | MEMBLOCK_FLAGS_NONE, 0},
            {0,  0,     ORDER_3 | MEMBLOCK_FLAGS_NONE, 0},
            {0,  0,     ORDER_4 | MEMBLOCK_FLAGS_NONE, 0},
        },
        .os_block =
        {
            {0x000000,  0x200000,     ORDER_0 | MEMBLOCK_FLAGS_NONE, 0},
            {0,  0,     ORDER_1 | MEMBLOCK_FLAGS_NONE, 0},
            {0,  0,     ORDER_2 | MEMBLOCK_FLAGS_NONE, 0},
            {0,  0,     ORDER_3 | MEMBLOCK_FLAGS_NONE, 0},
            {0,  0,     ORDER_4 | MEMBLOCK_FLAGS_NONE, 0},
        },
        .cal_block =
        {
            {0x1C0000,  0x40000,     ORDER_0 | MEMBLOCK_FLAGS_NONE, 0},
            {0,  0,     ORDER_1 | MEMBLOCK_FLAGS_NONE, 0},
            {0,  0,     ORDER_2 | MEMBLOCK_FLAGS_NONE, 0},
            {0,  0,     ORDER_3 | MEMBLOCK_FLAGS_NONE, 0},
            {0,  0,     ORDER_4 | MEMBLOCK_FLAGS_NONE, 0},
        },
    },
    //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    [6] =   //Chrysler JTEC (SCI_A)                                      //0x06
    {
        .index = 6,
        .ecm_id = 0x00, .reponse_ecm_id = 0x00, 
        .unlock_flags = UNLOCK_FLAG_NONE,
        .upload_unlock_id = UnlockId_None,
        .download_unlock_id = UnlockId_None,
        .checksum_id = ChecksumId_None,
        .finalizing_id = FinalId_None,
        .flags = SIMPLE_WAKEUP_CHECK_UPLOAD | SIMPLE_WAKEUP_CHECK_DOWNLOAD | SINGLE_UNLOCK_ONLY |
            UPLOAD_ADDR_TYPE_3B | UPLOAD_BY_CMD_35_SINGLE_36 |
            USE_ERASE_COMMAND |
            DOWNLOAD_ADDR_TYPE_3B | DOWNLOAD_REQUEST_TYPE_3B |
            DOWNLOAD_BLOCK_BACKWARD | DOWNLOAD_REQUIRE_CMD_37,
        .extended_flags = ECM_FLAGS_NONE,
        .commtype = CommType_SCIA, .commlevel = CommLevel_Unknown,
        .eraseall_cmd = {EraseCmdInfoFlags_None,EraseCmdInfoCount1,{0x00}},
        .erasecal_cmd = {EraseCmdInfoFlags_None,EraseCmdInfoCount1,{0x00}},
        .vid_address = 0, .pats_address = 0,
        .testerpresent_frequency = 10,
        
        .data_offset = 0x0,
        .ecm_startingaddr = 0x0,
        
        .util_upload = {NULL,NULL,NULL},
        .util_dnload = {NULL,NULL,NULL},
        .maxuploadblocksize = 0x400, .maxdownloadblocksize = 0x400,
        .os_block_read_skipblock_info = NULL,
        .os_block_read =
        {
            {0x040000,  0x040000,     ORDER_0 | MEMBLOCK_FLAGS_NONE, 0},
            {0,  0,     ORDER_1 | MEMBLOCK_FLAGS_NONE, 0},
            {0,  0,     ORDER_2 | MEMBLOCK_FLAGS_NONE, 0},
            {0,  0,     ORDER_3 | MEMBLOCK_FLAGS_NONE, 0},
            {0,  0,     ORDER_4 | MEMBLOCK_FLAGS_NONE, 0},
        },
        .os_block =
        {
            {0x000000,  0x040000,     ORDER_0 | MEMBLOCK_FLAGS_NONE, 0},
            {0,  0,     ORDER_1 | MEMBLOCK_FLAGS_NONE, 0},
            {0,  0,     ORDER_2 | MEMBLOCK_FLAGS_NONE, 0},
            {0,  0,     ORDER_3 | MEMBLOCK_FLAGS_NONE, 0},
            {0,  0,     ORDER_4 | MEMBLOCK_FLAGS_NONE, 0},
        },
        .cal_block =
        {
            {0x000000,  0x040000,     ORDER_0 | MEMBLOCK_FLAGS_NONE, 0},
            {0,  0,     ORDER_1 | MEMBLOCK_FLAGS_NONE, 0},
            {0,  0,     ORDER_2 | MEMBLOCK_FLAGS_NONE, 0},
            {0,  0,     ORDER_3 | MEMBLOCK_FLAGS_NONE, 0},
            {0,  0,     ORDER_4 | MEMBLOCK_FLAGS_NONE, 0},
        },  
    },
    //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    [7] =   //Chrysler JTEC+ (SCI_A)                                     //0x07
    {
        .index = 7,
        .ecm_id = 0x00, .reponse_ecm_id = 0x00, 
        .unlock_flags = UNLOCK_FLAG_NONE,
        .upload_unlock_id = UnlockId_None,
        .download_unlock_id = UnlockId_None,
        .checksum_id = ChecksumId_None,
        .finalizing_id = FinalId_None,
        .flags = SIMPLE_WAKEUP_CHECK_UPLOAD | SIMPLE_WAKEUP_CHECK_DOWNLOAD | SINGLE_UNLOCK_ONLY |
            UPLOAD_ADDR_TYPE_3B | UPLOAD_BY_CMD_35_SINGLE_36 |
            USE_ERASE_COMMAND |
            DOWNLOAD_ADDR_TYPE_3B | DOWNLOAD_REQUEST_TYPE_3B |
            DOWNLOAD_BLOCK_BACKWARD | DOWNLOAD_REQUIRE_CMD_37,
        .extended_flags = ECM_FLAGS_NONE,
        .commtype = CommType_SCIA, .commlevel = CommLevel_Unknown,
        .eraseall_cmd = {EraseCmdInfoFlags_None,EraseCmdInfoCount1,{0x00}},
        .erasecal_cmd = {EraseCmdInfoFlags_None,EraseCmdInfoCount1,{0x00}},
        .vid_address = 0, .pats_address = 0,
        .testerpresent_frequency = 10,
        
        .data_offset = 0x0,
        .ecm_startingaddr = 0x0,
        
        .util_upload = {NULL,NULL,NULL},
        .util_dnload = {NULL,NULL,NULL},
        .maxuploadblocksize = 0x400, .maxdownloadblocksize = 0x400,
        .os_block_read_skipblock_info = NULL,
        .os_block_read =
        {
            {0x040000,  0x040000,     ORDER_0 | MEMBLOCK_FLAGS_NONE, 0},
            {0,  0,     ORDER_1 | MEMBLOCK_FLAGS_NONE, 0},
            {0,  0,     ORDER_2 | MEMBLOCK_FLAGS_NONE, 0},
            {0,  0,     ORDER_3 | MEMBLOCK_FLAGS_NONE, 0},
            {0,  0,     ORDER_4 | MEMBLOCK_FLAGS_NONE, 0},
        },
        .os_block =
        {
            {0x000000,  0x040000,     ORDER_0 | MEMBLOCK_FLAGS_NONE, 0},
            {0,  0,     ORDER_1 | MEMBLOCK_FLAGS_NONE, 0},
            {0,  0,     ORDER_2 | MEMBLOCK_FLAGS_NONE, 0},
            {0,  0,     ORDER_3 | MEMBLOCK_FLAGS_NONE, 0},
            {0,  0,     ORDER_4 | MEMBLOCK_FLAGS_NONE, 0},
        },
        .cal_block =
        {
            {0x000000,  0x040000,     ORDER_0 | MEMBLOCK_FLAGS_NONE, 0},
            {0,  0,     ORDER_1 | MEMBLOCK_FLAGS_NONE, 0},
            {0,  0,     ORDER_2 | MEMBLOCK_FLAGS_NONE, 0},
            {0,  0,     ORDER_3 | MEMBLOCK_FLAGS_NONE, 0},
            {0,  0,     ORDER_4 | MEMBLOCK_FLAGS_NONE, 0},
        },  
    },
    //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    [8] =   //Chrysler NGC4 (CAN)                                        //0x08
    {
        .index = 8,
        .ecm_id = 0x7E0, .reponse_ecm_id = 0x7E8, 
        .unlock_flags = UNLOCK_FLAG_NONE,
        .upload_unlock_id = UnlockId_SCIB_UNLOCK ,
        .download_unlock_id = UnlockId_SCIB_UNLOCK ,
        .checksum_id = ChecksumId_None,
        .finalizing_id = FinalId_None,
        .flags = SIMPLE_WAKEUP_CHECK_UPLOAD | SIMPLE_WAKEUP_CHECK_DOWNLOAD | 
          COOLDOWNTIME_SHORT_3 | UPLOAD_ADDR_TYPE_3B | UPLOAD_BY_CMD_23 |
          USE_ERASE_COMMAND | DOWNLOAD_ADDR_TYPE_3B | DOWNLOAD_REQUEST_TYPE_3B |
          UTILITY_CHECK,
        .extended_flags = REQUIRE_DOWNLOAD_FINALIZING | CSF_REQUIRED | OVERLAY_FLASH
          | REQUIRE_KEY_DTC | DOWNLOAD_HEALTH_CHECK_STRICT,
        .commtype = CommType_CAN, .commlevel = CommLevel_KWP2000,
        .eraseall_cmd = {EraseCmdInfoFlags_None,EraseCmdInfoCount1,{0x00}},
        .erasecal_cmd = {EraseCmdInfoFlags_None,EraseCmdInfoCount1,{0x00}},
        .vid_address = 0, .pats_address = 0,
        .testerpresent_frequency = 0,
        
        .data_offset = 0x0,
        .ecm_startingaddr = 0x0,
        
        .util_upload = {(utility_info*)&utilityinfolist[0],NULL,NULL},
        .util_dnload = {(utility_info*)&utilityinfolist[0],NULL,NULL},
        .maxuploadblocksize = 0x800, .maxdownloadblocksize = 0x800,
        .os_block_read_skipblock_info = NULL,
        .os_block_read =
        {
            {0x000000,  0x200000,     ORDER_0 | MEMBLOCK_FLAGS_NONE, 0},
            {0,  0,     ORDER_1 | MEMBLOCK_FLAGS_NONE, 0},
            {0,  0,     ORDER_2 | MEMBLOCK_FLAGS_NONE, 0},
            {0,  0,     ORDER_3 | MEMBLOCK_FLAGS_NONE, 0},
            {0,  0,     ORDER_4 | MEMBLOCK_FLAGS_NONE, 0},
        },
        .os_block =
        {
            {0x000000,  0x200000,     ORDER_0 | MEMBLOCK_FLAGS_NONE, 0},
            {0,  0,     ORDER_1 | MEMBLOCK_FLAGS_NONE, 0},
            {0,  0,     ORDER_2 | MEMBLOCK_FLAGS_NONE, 0},
            {0,  0,     ORDER_3 | MEMBLOCK_FLAGS_NONE, 0},
            {0,  0,     ORDER_4 | MEMBLOCK_FLAGS_NONE, 0},
        },
        .cal_block =
        {
            {0x1C0000,  0x40000,     ORDER_0 | MEMBLOCK_FLAGS_NONE, 0},
            {0,  0,     ORDER_1 | MEMBLOCK_FLAGS_NONE, 0},
            {0,  0,     ORDER_2 | MEMBLOCK_FLAGS_NONE, 0},
            {0,  0,     ORDER_3 | MEMBLOCK_FLAGS_NONE, 0},
            {0,  0,     ORDER_4 | MEMBLOCK_FLAGS_NONE, 0},
        },
    },
    //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    [9] =   //Chrysler SBEC3A (SCI_B)                                    //0x09
    {
        .index = 9,
        .ecm_id = 0x00, .reponse_ecm_id = 0x00, 
        .unlock_flags = UNLOCK_FLAG_NONE,
        .upload_unlock_id = UnlockId_None,
        .download_unlock_id = UnlockId_None,
        .checksum_id = ChecksumId_None,
        .finalizing_id = FinalId_None,
        .flags = SIMPLE_WAKEUP_CHECK_UPLOAD | SIMPLE_WAKEUP_CHECK_DOWNLOAD | SINGLE_UNLOCK_ONLY |
            UPLOAD_ADDR_TYPE_3B | UPLOAD_BY_CMD_35_SINGLE_36 |
            USE_ERASE_COMMAND |
            DOWNLOAD_ADDR_TYPE_3B | DOWNLOAD_REQUEST_TYPE_3B |
            DOWNLOAD_BLOCK_BACKWARD | DOWNLOAD_REQUIRE_CMD_37,
        .extended_flags = ECM_FLAGS_NONE,
        .commtype = CommType_SCP, .commlevel = CommLevel_Unknown,
        .eraseall_cmd = {EraseCmdInfoFlags_None,EraseCmdInfoCount1,{0x00}},
        .erasecal_cmd = {EraseCmdInfoFlags_None,EraseCmdInfoCount1,{0x00}},
        .vid_address = 0, .pats_address = 0,
        .testerpresent_frequency = 10,
        
        .data_offset = 0x0,
        .ecm_startingaddr = 0x0,
        
        .util_upload = {NULL,NULL,NULL},
        .util_dnload = {NULL,NULL,NULL},
        .maxuploadblocksize = 0x400, .maxdownloadblocksize = 0x400,
        .os_block_read_skipblock_info = NULL,
        .os_block_read =
        {
            {0x040000,  0x040000,     ORDER_0 | MEMBLOCK_FLAGS_NONE, 0},
            {0,  0,     ORDER_1 | MEMBLOCK_FLAGS_NONE, 0},
            {0,  0,     ORDER_2 | MEMBLOCK_FLAGS_NONE, 0},
            {0,  0,     ORDER_3 | MEMBLOCK_FLAGS_NONE, 0},
            {0,  0,     ORDER_4 | MEMBLOCK_FLAGS_NONE, 0},
        },
        .os_block =
        {
            {0x000000,  0x040000,     ORDER_0 | MEMBLOCK_FLAGS_NONE, 0},
            {0,  0,     ORDER_1 | MEMBLOCK_FLAGS_NONE, 0},
            {0,  0,     ORDER_2 | MEMBLOCK_FLAGS_NONE, 0},
            {0,  0,     ORDER_3 | MEMBLOCK_FLAGS_NONE, 0},
            {0,  0,     ORDER_4 | MEMBLOCK_FLAGS_NONE, 0},
        },
        .cal_block =
        {
            {0x000000,  0x040000,     ORDER_0 | MEMBLOCK_FLAGS_NONE, 0},
            {0,  0,     ORDER_1 | MEMBLOCK_FLAGS_NONE, 0},
            {0,  0,     ORDER_2 | MEMBLOCK_FLAGS_NONE, 0},
            {0,  0,     ORDER_3 | MEMBLOCK_FLAGS_NONE, 0},
            {0,  0,     ORDER_4 | MEMBLOCK_FLAGS_NONE, 0},
        },
    },
    //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    [10] =   //Chrysler SBEC3(SCI_A)                                     //0x0A
    {
        .index = 10,
        .ecm_id = 0x00, .reponse_ecm_id = 0x00, 
        .unlock_flags = UNLOCK_FLAG_NONE,
        .upload_unlock_id = UnlockId_None,
        .download_unlock_id = UnlockId_None,
        .checksum_id = ChecksumId_None,
        .finalizing_id = FinalId_None,
        .flags = SIMPLE_WAKEUP_CHECK_UPLOAD | SIMPLE_WAKEUP_CHECK_DOWNLOAD | SINGLE_UNLOCK_ONLY |
            UPLOAD_ADDR_TYPE_3B | UPLOAD_BY_CMD_35_SINGLE_36 |
            USE_ERASE_COMMAND |
            DOWNLOAD_ADDR_TYPE_3B | DOWNLOAD_REQUEST_TYPE_3B |
            DOWNLOAD_BLOCK_BACKWARD | DOWNLOAD_REQUIRE_CMD_37,
        .extended_flags = ECM_FLAGS_NONE,
        .commtype = CommType_SCP, .commlevel = CommLevel_Unknown,
        .eraseall_cmd = {EraseCmdInfoFlags_None,EraseCmdInfoCount1,{0x00}},
        .erasecal_cmd = {EraseCmdInfoFlags_None,EraseCmdInfoCount1,{0x00}},
        .vid_address = 0, .pats_address = 0,
        .testerpresent_frequency = 10,
        
        .data_offset = 0x0,
        .ecm_startingaddr = 0x0,
        
        .util_upload = {NULL,NULL,NULL},
        .util_dnload = {NULL,NULL,NULL},
        .maxuploadblocksize = 0x400, .maxdownloadblocksize = 0x400,
        .os_block_read_skipblock_info = NULL,
        .os_block_read =
        {
            { 0x040000,  0x020000,     ORDER_0 | MEMBLOCK_FLAGS_NONE, 0},
            {0,  0,     ORDER_1 | MEMBLOCK_FLAGS_NONE, 0},
            {0,  0,     ORDER_2 | MEMBLOCK_FLAGS_NONE, 0},
            {0,  0,     ORDER_3 | MEMBLOCK_FLAGS_NONE, 0},
            {0,  0,      ORDER_4 | MEMBLOCK_FLAGS_NONE, 0},
        },
        .os_block =
        {
            {0x000000,  0x020000,     ORDER_0 | MEMBLOCK_FLAGS_NONE, 0},
            {0,  0,     ORDER_1 | MEMBLOCK_FLAGS_NONE, 0},
            {0,  0,     ORDER_2 | MEMBLOCK_FLAGS_NONE, 0},
            {0,  0,     ORDER_3 | MEMBLOCK_FLAGS_NONE, 0},
            {0,  0,      ORDER_4 | MEMBLOCK_FLAGS_NONE, 0},
        },
        .cal_block =
        {
            {0x000000,  0x020000,     ORDER_0 | MEMBLOCK_FLAGS_NONE, 0},
            {0,  0,     ORDER_1 | MEMBLOCK_FLAGS_NONE, 0},
            {0,  0,     ORDER_2 | MEMBLOCK_FLAGS_NONE, 0},
            {0,  0,     ORDER_3 | MEMBLOCK_FLAGS_NONE, 0},
            {0,  0,      ORDER_4 | MEMBLOCK_FLAGS_NONE, 0},
        },
    },
    //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    [11] =   //Chrysler NGC4A 2011 ECM (CAN)                             //0x0B
    {
        .index = 11,
        .ecm_id = 0x7E0, .reponse_ecm_id = 0x7E8, 
        .unlock_flags = UNLOCK_FLAG_NONE,
        .upload_unlock_id = UnlockId_HEMI_BOOTLOADER_SECURITY_UPLOAD,
        .download_unlock_id = UnlockId_HEMI_BOOTLOADER_SECURITY_DOWNL,
        .checksum_id = ChecksumId_None,
        .finalizing_id = FinalId_None,
        .flags = SIMPLE_WAKEUP_CHECK_UPLOAD | SIMPLE_WAKEUP_CHECK_DOWNLOAD | 
          UPLOAD_ADDR_TYPE_4B | UPLOAD_BY_CMD_35_MULTI_36 | USE_ERASE_COMMAND |
          DOWNLOAD_ADDR_TYPE_4B | DOWNLOAD_REQUEST_TYPE_4B | DOWNLOAD_REQUIRE_CMD_37 |
          COOLDOWNTIME_SHORT_1,
        .extended_flags = REQUIRE_DOWNLOAD_FINALIZING | CSF_REQUIRED | USE_FINGERPRINT_INTERLOCK
        | E2_REQUIRED | OVERLAY_FLASH | REQUIRE_KEY_DTC | DOWNLOAD_HEALTH_CHECK_STRICT,
        .commtype = CommType_CAN, .commlevel = CommLevel_KWP2000,
        .eraseall_cmd = {EraseCmdInfoFlags_None,EraseCmdInfoCount1,{0x00}},
        .erasecal_cmd = {EraseCmdInfoFlags_None,EraseCmdInfoCount1,{0x00}},
        .vid_address = 0, .pats_address = 0,
        .testerpresent_frequency = 0,
        
        .data_offset = 0x0,
        .ecm_startingaddr = 0x0,
        
        .util_upload = {NULL,NULL,NULL},
        .util_dnload = {NULL,NULL,NULL},
        .maxuploadblocksize = 0xFE0, .maxdownloadblocksize = 0xFFC,
        .os_block_read_skipblock_info = NULL,
        .os_block_read =
        {
            {0x000000,  0x200000,     ORDER_0 | MEMBLOCK_FLAGS_NONE, 0},
            {0,  0,     ORDER_1 | MEMBLOCK_FLAGS_NONE, 0},
            {0,  0,     ORDER_2 | MEMBLOCK_FLAGS_NONE, 0},
            {0,  0,     ORDER_3 | MEMBLOCK_FLAGS_NONE, 0},
            {0,  0,     ORDER_4 | MEMBLOCK_FLAGS_NONE, 0},
        },
        .os_block =
        {
            {0x000000,  0x200000,     ORDER_0 | MEMBLOCK_FLAGS_NONE, 0},
            {0,  0,     ORDER_1 | MEMBLOCK_FLAGS_NONE, 0},
            {0,  0,     ORDER_2 | MEMBLOCK_FLAGS_NONE, 0},
            {0,  0,     ORDER_3 | MEMBLOCK_FLAGS_NONE, 0},
            {0,  0,     ORDER_4 | MEMBLOCK_FLAGS_NONE, 0},
        },
        .cal_block =
        {
            {0x1C0000,  0x40000,     ORDER_0 | MEMBLOCK_FLAGS_NONE, 0},
            {0,  0,     ORDER_1 | MEMBLOCK_FLAGS_NONE, 0},
            {0,  0,     ORDER_2 | MEMBLOCK_FLAGS_NONE, 0},
            {0,  0,     ORDER_3 | MEMBLOCK_FLAGS_NONE, 0},
            {0,  0,     ORDER_4 | MEMBLOCK_FLAGS_NONE, 0},
        },
    },
    //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    [12] =   //Chrysler 2014 GPEC2 ECM (CAN)                             //0x0C
    {
        .index = 12,
        .ecm_id = 0x7E0, .reponse_ecm_id = 0x7E8, 
        .unlock_flags = UNLOCK_FLAG_NONE,
        .upload_unlock_id = UnlockId_HEMI_BOOTLOADER_SECURITY_UPLOAD,
        .download_unlock_id = UnlockId_HEMI_BOOTLOADER_SECURITY_DOWNL,
        .checksum_id = ChecksumId_None,
        .finalizing_id = FinalId_None,
        .flags = SIMPLE_WAKEUP_CHECK_UPLOAD | SIMPLE_WAKEUP_CHECK_DOWNLOAD | 
          UPLOAD_ADDR_TYPE_4B | UPLOAD_BY_CMD_35_MULTI_36 | USE_ERASE_COMMAND |
          DOWNLOAD_ADDR_TYPE_4B | DOWNLOAD_REQUEST_TYPE_4B | DOWNLOAD_REQUIRE_CMD_37 |
          COOLDOWNTIME_SHORT_1,
        .extended_flags = REQUIRE_DOWNLOAD_FINALIZING | CSF_REQUIRED | USE_FINGERPRINT_INTERLOCK
        | E2_REQUIRED | OVERLAY_FLASH | RSA_SIGNATURE | PREDOWNLOAD_TASK | USE_VARIANT_ID | REQUIRE_KEY_DTC
        | DOWNLOAD_HEALTH_CHECK_STRICT | TRANS_INFO | CLEAR_KAM_AFTER_DOWNLOAD | REQUIRE_KEYOFF_BEFORE_DOWNLOAD,
        .commtype = CommType_CAN, .commlevel = CommLevel_KWP2000,
        .eraseall_cmd = {EraseCmdInfoFlags_None,EraseCmdInfoCount1,{0x00}},
        .erasecal_cmd = {EraseCmdInfoFlags_None,EraseCmdInfoCount1,{0x00}},
        .vid_address = 0, .pats_address = 0,
        .testerpresent_frequency = 0,
        
        .data_offset = 0x0,
        .ecm_startingaddr = 0x0,
        
        .util_upload = {NULL,NULL,NULL},
        .util_dnload = {(utility_info*)&utilityinfolist[2],NULL,NULL}, 
        .maxuploadblocksize = 0xFE0, .maxdownloadblocksize = 0xFFC,
        .os_block_read_skipblock_info = NULL,
        .os_block_read =
        {
            {0x000000,  0x300000,     ORDER_0 | MEMBLOCK_FLAGS_NONE, 0},
            {0,  0,     ORDER_1 | MEMBLOCK_FLAGS_NONE, 0},
            {0,  0,     ORDER_2 | MEMBLOCK_FLAGS_NONE, 0},
            {0,  0,     ORDER_3 | MEMBLOCK_FLAGS_NONE, 0},
            {0,  0,     ORDER_4 | MEMBLOCK_FLAGS_NONE, 0},
        },
        .os_block =
        {
            {0x040000,  0x240000,     ORDER_0 | MEMBLOCK_FLAGS_NONE, 0},
            {0x280000,  0x80000,      ORDER_1 | MEMBLOCK_FLAGS_NONE, 0},
            {0,  0,     ORDER_2 | MEMBLOCK_FLAGS_NONE, 0},
            {0,  0,     ORDER_3 | MEMBLOCK_FLAGS_NONE, 0},
            {0,  0,     ORDER_4 | MEMBLOCK_FLAGS_NONE, 0},
        },
        .cal_block =
        {
            {0x280000,  0x80000,      ORDER_0 | MEMBLOCK_FLAGS_NONE, 0},
            {0,  0,     ORDER_1 | MEMBLOCK_FLAGS_NONE, 0},
            {0,  0,     ORDER_2 | MEMBLOCK_FLAGS_NONE, 0},
            {0,  0,     ORDER_3 | MEMBLOCK_FLAGS_NONE, 0},
            {0,  0,     ORDER_4 | MEMBLOCK_FLAGS_NONE, 0},
        },
    },
};

const DCX_DL_HACK_DATA hack_data[] = 
{
  {
     {
       {0x4E,0x80,0x00,0x20},
       {0x7D,0x8C,0x53,0x78},           // varaiant ID 23 and 25
       {0x7C,0x0C,0x58,0x40},
       {0x41,0x82,0x00,0x08},
       {0x3B,0x9B,0x00,0x00}
     },
    {0x7C,0x08,0x40,0x40}
  },
  
};
