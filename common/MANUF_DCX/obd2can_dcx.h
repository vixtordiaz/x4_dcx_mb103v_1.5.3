/**
  **************** (C) COPYRIGHT 2011 SCT Performance, LLC *********************
  * File Name          : obd2can_ford.h
  * Device / System    : SCT products using the "CommonCode" platform
  * Author             : Quyen Leba
  *
  * Version            : 1 
  * Date               : 05/02/2011
  * Description        : 
  *                    : 
  *
  *
  * History            : 05/02/2011 M. Davis
  *                    :   Add SCT Copyright header,
  *
  ******************************************************************************
  */

#ifndef __OBD2CAN_DCX_H
#define __OBD2CAN_DCX_H

#include <common/obd2can.h>
#include <common/obd2datalog.h>
#include <common/obd2def.h>
#include <common/checksum_csf.h>


typedef struct
{
    u8 check_strings[5][4];
    u8 change_string[4];
}DCX_DL_HACK_DATA;

enum
{
    Normal = 0,
    Modify = 1,
};

//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
// CHRYSLER funcions to support ECM communication
//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

u8 obd2can_dcx_wakeup(u16 veh_type, u8 ecm_index);
u8 obd2can_dcx_downloadutility(u32 ecm_type, u8 utilitychoiceindex,
                               bool isdownloadutility);
u8 obd2can_dcx_checkutility(u32 ecm_type, u8 utility_type);
u8 obd2can_dcx_exitutility(u32 ecm_type);
u8 obd2can_dcx_downloadfinalizing(u32 ecm_type);

//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
// Functions from J2190
//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
u8 obd2can_dcx_clear_diagnostic_information(u32 ecm_id);                   //$04
u8 obd2can_dcx_request_vehicle_information(u32 ecm_id, u8 infotype,        //$09
                                            u8 *data, u16 *datalength);
u8 obd2can_dcx_initiate_diagnostic_operation(bool broadcast,               //$10
                                             u32 ecm_id,                
                                             u32 device_address,  
                                             Obd2can_SubServiceNumber subservice);
u8 obd2can_dcx_ecu_reset_request(bool broadcast,                           //$11
                                 u32 ecm_id,
                                 u32 response_id,
                                 Obd2can_SubServiceNumber subservice,
                                 bool isreponse_expected);
u8 obd2can_dcx_returnnormalmode(u8 type);                                  //$20 
u8 obd2can_dcx_read_local_identifier(u32 ecm_id,  u32 response_id,  	   //$21
                                     u8 identifier, u8 *data,
									 u32 *datalength);
u8 obd2can_dcx_read_memory_address(u32 ecm_id,                             //$23
                                   u32 address, u16 length,                                   
                                   MemorySize memsize,
                                   u8 *data, u16 *datalength,                                   
                                   VehicleCommLevel commlevel);
u8 obd2can_dcx_read_memory_address_kwp2000(u32 ecm_id, u32 address, u16 length,
                                           MemorySize memsize,
                                           u8 *data, u16 *datalength);
u8 obd2can_dcx_security_access(u32 ecm_id,                                 //$27
                               Obd2can_SubServiceNumber subservice,
                               u8 *data, u8 *datalength, u32 algoindex);
void obd2can_dcx_disablenormalcommunication();                             //$28
void obd2can_dcx_enablenormalcommunication();                              //$29                                                                                       
u8 obd2can_dcx_requestdiagnosticdatapacket_2A(u32 ecm_id, u8 datarate,     //$2A
                                              u8 *packetidlist,
                                              u8 packetidcount,
                                              VehicleCommLevel commlevel);
u8 obd2can_dcx_dynamicallydefinemessage(u32 ecm_id, u8 packetid,           //$2C
                                        u8 *type, u8 *size, u8 *position,
                                        u32 *address, u8 pidcount,
                                        VehicleCommLevel commlevel);
u8 obd2can_dcx_cleandynamicallydefinedidentifier(u32 ecm_id, u8 packetid); //$2C
u8 obd2can_dcx_start_routine_by_local_id_tx(u32 ecm_id, u32 response_id,   //$31
                                            u8 routine_identifier,
                                            u8 routine_entry, u8 *data, 
                                            u32 datalength);
u8 obd2can_dcx_results_routine_by_local_id(u32 ecm_id, u32 response_id,    //$33
                                              u8 routine_identifier, 
                                              u8 routine_entry);
u8 obd2can_dcx_request_download(u32 ecm_id,                               //$34
                                u8 dataformat,
                                u32 requestaddress, u32 requestlength,
                                RequestLengthType reqtype,
                                VehicleCommLevel commlevel,
                                u16 *maxdownloadblocklength);
u8 obd2can_dcx_request_upload(u32 ecm_id,                                  //$35
                              u32 requestaddress, u32 requestlength,
                              RequestLengthType reqtype,
                              VehicleCommLevel commlevel,
                              u16 *maxuploadblocklength);
u8 obd2can_dcx_transfer_data_upload(u32 ecm_id, u8 blocktracking,          //$36     
                                    VehicleCommLevel commlevel,
                                    u32 request_address,
                                    RequestLengthType request_type,
                                    u8 *data, u16 *datalength); 
u8 obd2can_dcx_transfer_data_download(u32 ecm_id,                          //$36
                                      Obd2can_SubServiceNumber subservice,
                                      u8 blocktracking, u32 startingaddr,
                                      MemoryAddressType addrtype,
                                      u8 *data, u16 datalength,
                                      VehicleCommLevel commlevel,
                                      bool response_expected);
u8 obd2can_dcx_request_transfer_exit(u32 ecm_id,                           //$37
                                     VehicleCommLevel commlevel);
u8 obd2can_dcx_write_data_by_local_id(u32 ecm_id, u8 data_id,              //$3B
                                       u8 *data, u32 datalength);
u8 obd2can_dcx_write_data_by_localID(u32 ecm_id,u32 response_id, 
                                     u8 subservice, u8 *data,              //$3B
                                     u32 datalength);
u8 obd2can_dcx_requestdiagnosticdatapacket(u32 ecm_id, u8 datarate,        //$A0
                                           u8 *packetidlist, u8 packetidcount,
                                           VehicleCommLevel commlevel);
u8 obd2can_dcx_dynamicallydefinediagnosticdatapacket(u32 ecm_id,           //$A1
                                                      u8 packetid,
                                                      PidType type,
                                                      u8 size, u8 position,
                                                      u32 address,
                                                      VehicleCommLevel commlevel);
u8 obd2can_dcx_transfer_data_download_utility(u32 ecm_id,
                                              Obd2can_SubServiceNumber subservice,
                                              u8 blocktracking, u32 startingaddr,
                                              MemoryAddressType addrtype,
                                              u8 *data, u16 datalength,
                                              VehicleCommLevel commlevel,
                                              bool response_expected);
u8 obd2can_dcx_validatedmr(u32 ecm_id, DlxBlock *dlxblock,
                           VehicleCommLevel commlevel);
u8 obd2can_dcx_validateByRapidPacketSetup(u32 ecm_id, DlxBlock *dlxblock,
                                          VehicleCommLevel commlevel);

void obdcan_dcx_control_faults(u8 Enable_Faults, u32 ecm_id);
void obd2can_dcx_92_85();
void obd2can_dcx_initsequence(u8 part_type, u32 ecm_id, u32 response_ecm_id);
//u8 obd2can_dcx_alt_readdtc(u32 ecm_id, dtc_info *info);
u8 obd2can_dcx_readdtc_all(dtc_info *info);
u8 obd2can_dcx_clear_adaptives(u32 ecm_id, VehicleCommLevel commlevel);
u8 obd2can_dcx_traction_control(u8 operation, VehicleCommLevel commlevel);
u8 obd2can_dcx_request_byidentifier(u8 Identifier, u32 ecm_id, u32 Device_ID, 
                                    u8 Use_retrys);
u8 obd2can_dcx_getparttype(u32 ecm_id, u8 *vehicle_parttype, 
                           u8*parttype_numerical_value);
u8 obd2can_dcx_block_IDs(u32 ecm_id, u32 response_ecm_id);
u8 obd2can_dcx_reset_22();
u8 obd2can_dcx_getdtccount(u8 *count, u32 ecm_id);
u8 obd2can_dcx_enter_utility(u32 ecm_id);
u8 obd2can_dcx_ETC_relearn();
u8 obd2can_dcx_read_DTC_info(u32 ecm_id,u32 ecm_response_id, u8 first_byte_ID, u8 sencond_byte_ID, u8 *data);
u8 obd2can_dcx_get_boot_version(u32 ecm_id, u8 *ecm_boot_version);
u8 obd2can_dcx_fingerprint_interlock(u16 ecm_type, bool logical_block_index);
u8 obd2can_dcx_fingerprint_interlock_trans_info(u32 ecm_type);
u8 obd2can_dcx_flash_starter(u32 ecm_id);
u8 obd2can_dcx_fingerprint(u16 ecm_type, u32 ecm_id, bool logical_block_index);
u8 obd2can_dcx_fingerprint_trans_info(u32 ecm_type, u32 ecm_id);
u8 obd2can_dcx_softwareinterlock(u32 ecm_id);
u8 obd2can_dcx_upload_e2_data(u32 ecm_type);
u8 obd2can_dcx_download_e2_data(u32 ecm_type, u8 modify_E2);
u8 obd2can_dcx_read_tire_axel_parameters(u8 *WA580, Obd2can_ServiceData contactdata, 
                                         u8 *diag_type);
u8 obd2can_dcx_write_tire_axel_parameters(CMDIF_COMMAND cmd, const u8 *title,
                                          u8 WA580, u8 *data, u8 diag_type);
u8 obd2can_dcx_download_rsa_signature(u32 ecm_type, u8 block_index, 
                                      BlockType block_type);
u8 obd2can_dcx_download_cal_rsa_signature(u32 ecm_type);
u8 obd2can_dcx_download_os_rsa_signature(u32 ecm_type, F_FILE *file_fptr);
u8 obd2can_dcx_download_utility_rsa_signature(u32 ecm_type, F_FILE *file_fptr);
u8 obd2can_dcx_download_os_rsa_signature_helper(Csf_Block_BlockReplacement_496 *csfblockreplacement, 
                                                u32 ecm_tpye, F_FILE *csf_fptr, 
                                                F_FILE *file_fptr);
u8 obd2can_dcx_download_trans_info(u32 ecm_type);
u8 obd2can_dcx_unlock_multiple_securities(u32 ecm_type);
u8 obd2can_dcx_power_on_reset(u32 ecm_id, VehicleCommLevel commlevel);
u8 obd2can_dcx_do_check(u8 *response, bool checksupportfiles, u16 ecm_type);
u8 obd2can_dcx_getinfo(obd2_info_block *block);



/*All the function below use to be overloaded functions*/
u8 obd2can_dcx_reset_ecm(u16 ecm_type);
u8 obd2can_dcx_erase_ecm(u8 ecm_type, bool is_cal_only);
u8 obd2can_dcx_clear_kam(u32 ecm_id, VehicleCommLevel commlevel);
u8 obd2can_dcx_testerpresent(bool broadcast, u32 ecm_id,
                             VehicleCommLevel commlevel);
u8 obd2can_dcx_readecminfo(ecm_info *ecm);
u8 obd2can_dcx_cleardtc();
u8 obd2can_dcx_clear_diagnostic_information_all_modules(void);

//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
// 2013+ GPEC2 hack functions
//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
u8 obd2can_dcx_hack_read_buffer(u32 address, u8 *answer);
u8 obd2can_dcx_hack_set_RAM_buffer(u8 command, u32 buffer_address);
u8 obd2can_dcx_hack_command(u8 command, u8 *data, u8 *answer);
u8 obd2can_dcx_hack_write_buffer(u32 working_address, u8 *data, u8 *answer);
u32 obd2can_dcx_hack_check_RAM_buffer(u32 start_address, u32 end_address, 
                                      u32 length, u32 ram_buffer_add);
u8 obd2can_dcx_hack_flash_initialize_7(u32 flash_block, u32 buffer_add);
u8 obd2can_dcx_hack_flash_write_6(u32 *changing_data, u32 buffer_add, u32 blr_add, 
                                  u32 flsh_blk_add);
u8 obd2can_dcx_hack_set_lock_9(u32 buffer_address);
u8 obd2can_dcx_hack_flash_erase_8(u8 flash_section, u32 buffer_add);
u8 obd2can_dcx_preflash_task(u32 ecm_type, u8 flash_type, bool is_cal_only);
u8 obd2can_dcx_exit_vehicle_specific_hack();
void obd2can_dcx_utility_progressbar(u8 percentage,u8 *message);
u8 obd2can_dcx_start_routine_by_local_id_rx(u32 ecm_id, u8 routine_identifier, 
                                            u8 *data, u32 *datalength);
u32 obd2can_dcx_findMatchingID(u32 ID, u8 type);

#endif    //__OBD2CAN_DCX_H
