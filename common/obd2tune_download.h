/**
  **************** (C) COPYRIGHT 2011 SCT Performance, LLC *********************
  * File Name          : obd2tune_download.h
  * Device / System    : SCT products using the "CommonCode" platform
  * Author             : Quyen Leba
  *
  * Version            : 1 
  * Date               : 05/02/2011
  * Description        : 
  *                    : 
  *
  *
  * History            : 05/02/2011 M. Davis
  *                    :   Add SCT Copyright header,
  *
  ******************************************************************************
  */

#ifndef __OBD2TUNE_DOWNLOAD_H
#define __OBD2TUNE_DOWNLOAD_H

u8 obd2tune_parsevin(u8 *vin);
u8 download_ecm(u16 ecm_type, VehicleCommType vehiclecommtype,
                VehicleCommLevel vehiclecommlevel, bool isAllUnlocked);
u8 obd2tune_downloadtune(u16 veh_type);
u8 obd2tune_download_test_key_on(u16 ecm_type);
u8 obd2tune_download_health_check(u16 veh_type);

#endif  //__OBD2TUNE_DOWNLOAD_H
