/**
  **************** (C) COPYRIGHT 2011 SCT Performance, LLC *********************
  * File Name          : genmanuf_overload.h
  * Device / System    : SCT products using the "CommonCode" platform
  * Author             : Quyen Leba
  *
  * Version            : 1 
  * Date               : 05/02/2011
  * Description        : 
  *                    : 
  *
  *
  * History            : 05/02/2011 M. Davis
  *                    :   Add SCT Copyright header,
  *
  ******************************************************************************
  */

#ifndef __GENMANUF_OVERLOAD_H
#define __GENMANUF_OVERLOAD_H

#ifdef __GM_MANUF__
#include "MANUF_GM/demo_data.h"
#include "MANUF_GM/obd2_gmdef.h"
#include "MANUF_GM/obd2_gm.h"
#include "MANUF_GM/obd2can_gm.h"
#include "MANUF_GM/obd2tune_gm.h"
#include "MANUF_GM/obd2tune_gm_checksum.h"
#include "MANUF_GM/obd2datalog_gm.h"
#include "MANUF_GM/obd2can_gm_datalog.h"

#elif __FORD_MANUF__
#include "MANUF_FORD/demo_data.h"
#include "MANUF_FORD/obd2_forddef.h"
#include "MANUF_FORD/obd2_ford.h"
#include "MANUF_FORD/obd2can_ford.h"
#include "MANUF_FORD/obd2tune_ford.h"
#include "MANUF_FORD/obd2tune_ford_checksum.h"
#include "MANUF_FORD/obd2datalog_ford.h"
#include "MANUF_FORD/obd2can_ford_datalog.h"
#include "MANUF_FORD/obd2can_ford_60L.h"
#include "MANUF_FORD/obd2can_ford_67L.h"
#include "MANUF_FORD/ford_ecm_utility_testfunc.h"

#elif __DCX_MANUF__
#include "MANUF_DCX/demo_data.h"
#include "MANUF_DCX/obd2_dcxdef.h"
#include "MANUF_DCX/obd2_dcx.h"
#include "MANUF_DCX/obd2can_dcx.h"
#include "MANUF_DCX/obd2tune_dcx.h"
#include "MANUF_DCX/obd2tune_dcx_checksum.h"
#include "MANUF_DCX/obd2datalog_dcx.h"
#include "MANUF_DCX/obd2can_dcx_datalog.h"
#include "MANUF_DCX/dcx_ecm_utility_testfunc.h"
#else
#error "genmanuf_overload.h: Invalid MANUF"
#endif

#endif    //__GENMANUF_OVERLOAD_H
