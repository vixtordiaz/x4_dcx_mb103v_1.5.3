/**
  **************** (C) COPYRIGHT 2011 SCT Performance, LLC *********************
  * File Name          : obd2tune_otf.h
  * Device / System    : SCT products using the "CommonCode" platform
  * Author             : Quyen Leba
  *
  * Version            : 1 
  * Date               : 05/02/2011
  * Description        : 
  *                    : 
  *
  *
  * History            : 05/02/2011 M. Davis
  *                    :   Add SCT Copyright header,
  *
  ******************************************************************************
  */

#ifndef __OBD2TUNE_OTF_H
#define __OBD2TUNE_OTF_H

#include <arch/gentype.h>

typedef enum
{
    OTFPowerLevel_Stock         = 0,
    OTFPowerLevel_Tow           = 1,
    OTFPowerLevel_Performance   = 2,
    OTFPowerLevel_Extreme       = 3
}OTFPowerLevel;

bool obd2tune_otf_isFeatureAvailable(u8 *vin);
u8 obd2tune_otf_setPowerLevel(u16 vehicle_type,
                              u8 processor_index, OTFPowerLevel power_level);


#endif  //__OBD2TUNE_OTF_H
