/**
  **************** (C) COPYRIGHT 2011 SCT Performance, LLC *********************
  * File Name          : cmdif_remotegui.h
  * Device / System    : SCT products using the "CommonCode" platform
  * Author             : Quyen Leba
  *
  * Version            : 1 
  * Date               : 05/02/2011
  * Description        : 
  *                    : 
  *
  *
  * History            : 05/02/2011 M. Davis
  *                    :   Add SCT Copyright header,
  *
  ******************************************************************************
  */

#ifndef __CMDIF_REMOTEGUI_H
#define __CMDIF_REMOTEGUI_H

#include <arch/gentype.h>

typedef enum    //must match tkBaseRemoteGUIResponseType (X4)
{
    CMDIF_REMOTEGUI_RESPONSETYPE_BUTTON         = 0,
    CMDIF_REMOTEGUI_RESPONSETYPE_LIST           = 1,
    CMDIF_REMOTEGUI_RESPONSETYPE_KEYBOARD       = 2,
    CMDIF_REMOTEGUI_RESPONSETYPE_NUMPAD         = 3,

    CMDIF_REMOTEGUI_RESPONSETYPE_USERABORT      = 0xA0,
    CMDIF_REMOTEGUI_RESPONSETYPE_ERROR          = 0xF0,
}CMDIF_REMOTEGUI_RESPONSETYPE;

typedef enum    //must match GenMenuIcon (X4) & MessageBoxIconType (LWTS)
{
    CMDIF_REMOTEGUI_ICON_MAGNIFYGLASS           = 1,
    CMDIF_REMOTEGUI_ICON_KEYON                  = 2,
    CMDIF_REMOTEGUI_ICON_KEYOFF                 = 3,
    CMDIF_REMOTEGUI_ICON_WARNING                = 4,

    CMDIF_REMOTEGUI_ICON_BLANK                  = 0xFE,
    CMDIF_REMOTEGUI_ICON_NONE                   = 0xFF,
}CMDIF_REMOTEGUI_ICON;

typedef enum    //must match tkBaseRemoteGUIButtonType (X4)
{
    CMDIF_REMOTEGUI_BUTTONTYPE_LEFT             = 0,
    CMDIF_REMOTEGUI_BUTTONTYPE_MIDDLE           = 1,
    CMDIF_REMOTEGUI_BUTTONTYPE_RIGHT            = 2,
}CMDIF_REMOTEGUI_BUTTONTYPE;

typedef enum    //must match tkBaseRemoteGUINumPadInputType (X4)
{
    CMDIF_REMOTEGUI_NUMPAD_INPUTTYPE_UNSIGNEDINT= 0,
    CMDIF_REMOTEGUI_NUMPAD_INPUTTYPE_SIGNINT    = 1,
    CMDIF_REMOTEGUI_NUMPAD_INPUTTYPE_FLOAT      = 2,
}CMDIF_REMOTEGUI_NUMPAD_INPUTTYPE;

typedef enum    //must match tkBaseRemoteGUIKeyBoardDefaultType (X4)
{
    CMDIF_REMOTEGUI_KEYBOARD_DEFAULTTYPE_UPPERCASE                      = 0,
    CMDIF_REMOTEGUI_KEYBOARD_DEFAULTTYPE_LOWERCASE                      = 1,
    CMDIF_REMOTEGUI_KEYBOARD_DEFAULTTYPE_NUMBERSYMBOL0_WITHUPPERCASE    = 2,
    CMDIF_REMOTEGUI_KEYBOARD_DEFAULTTYPE_NUMBERSYMBOL0_WITHLOWERCASE    = 3,
    CMDIF_REMOTEGUI_KEYBOARD_DEFAULTTYPE_NUMBERSYMBOL1_WITHUPPERCASE    = 4,
    CMDIF_REMOTEGUI_KEYBOARD_DEFAULTTYPE_NUMBERSYMBOL1_WITHLOWERCASE    = 5,
}CMDIF_REMOTEGUI_KEYBOARD_DEFAULTTYPE;

typedef enum    //must match GenMenuFocus (X4)
{
    CMDIF_REMOTEGUI_FOCUS_ListBox               = 0,
    CMDIF_REMOTEGUI_FOCUS_Button0               = 1,
    CMDIF_REMOTEGUI_FOCUS_Button1               = 2,
    CMDIF_REMOTEGUI_FOCUS_Button2               = 3,
    CMDIF_REMOTEGUI_FOCUS_None                  = 0xFF,
}CMDIF_REMOTEGUI_FOCUS;

typedef enum    //must match FunctionButtonFace (LWTS), GenMenuButtonFace (X4)
{
    CMDIF_REMOTEGUI_BUTTONFACE_OK                = 0,
    CMDIF_REMOTEGUI_BUTTONFACE_Cancel            = 1,
    CMDIF_REMOTEGUI_BUTTONFACE_Submit            = 2,
    CMDIF_REMOTEGUI_BUTTONFACE_Abort             = 3,
    CMDIF_REMOTEGUI_BUTTONFACE_Next              = 4,
    CMDIF_REMOTEGUI_BUTTONFACE_Back              = 5,
    CMDIF_REMOTEGUI_BUTTONFACE_ReadCodes         = 6,
    CMDIF_REMOTEGUI_BUTTONFACE_ClearCodes        = 7,
    CMDIF_REMOTEGUI_BUTTONFACE_ReadDTC           = 8,
    CMDIF_REMOTEGUI_BUTTONFACE_ClearDTC          = 9,
    CMDIF_REMOTEGUI_BUTTONFACE_Continue          = 10,
    CMDIF_REMOTEGUI_BUTTONFACE_Done              = 11,
    CMDIF_REMOTEGUI_BUTTONFACE_ReturnStock       = 12,
    CMDIF_REMOTEGUI_BUTTONFACE_UploadStock       = 13,
    CMDIF_REMOTEGUI_BUTTONFACE_RestoreDefault    = 14,
    CMDIF_REMOTEGUI_BUTTONFACE_Approve           = 15,
    CMDIF_REMOTEGUI_BUTTONFACE_Confirm           = 16,
    CMDIF_REMOTEGUI_BUTTONFACE_Retry             = 17,
    CMDIF_REMOTEGUI_BUTTONFACE_LicenseNotice     = 18,
    CMDIF_REMOTEGUI_BUTTONFACE_Configuration     = 19,
    CMDIF_REMOTEGUI_BUTTONFACE_MetricUnit        = 20,
    CMDIF_REMOTEGUI_BUTTONFACE_ImperialUnit      = 21,
    CMDIF_REMOTEGUI_BUTTONFACE_SwitchUnit        = 22,
    CMDIF_REMOTEGUI_BUTTONFACE_SaveResult        = 23,
    CMDIF_REMOTEGUI_BUTTONFACE_Shutdown          = 24,
    CMDIF_REMOTEGUI_BUTTONFACE_Restart           = 25,
    CMDIF_REMOTEGUI_BUTTONFACE_Weight            = 26,
    CMDIF_REMOTEGUI_BUTTONFACE_Exit              = 27,
    CMDIF_REMOTEGUI_BUTTONFACE_LastInfo          = 28,
    CMDIF_REMOTEGUI_BUTTONFACE_Skip              = 29,
    CMDIF_REMOTEGUI_BUTTONFACE_Change            = 30,
    CMDIF_REMOTEGUI_BUTTONFACE_TurnOn            = 31,
    CMDIF_REMOTEGUI_BUTTONFACE_TurnOff           = 32,
    CMDIF_REMOTEGUI_BUTTONFACE_ReScan            = 33,
    CMDIF_REMOTEGUI_BUTTONFACE_Connect           = 34,
    CMDIF_REMOTEGUI_BUTTONFACE_Disconnect        = 35,
    CMDIF_REMOTEGUI_BUTTONFACE_RestoreDefaults   = 36,
    CMDIF_REMOTEGUI_BUTTONFACE_TireSize          = 37,
    CMDIF_REMOTEGUI_BUTTONFACE_AxleRatio         = 38,

    CMDIF_REMOTEGUI_BUTTONFACE_None              = 0xFF,
}CMDIF_REMOTEGUI_BUTTONFACE;

typedef enum    //must match flags in GenMenuListBoxItem (X4)
{
    CMDIF_REMOTEGUI_LISTBOX_FLAGS_NONE                  = 0,
    CMDIF_REMOTEGUI_LISTBOX_FLAGS_HEADER_ITEM           = (1u<<0),
    CMDIF_REMOTEGUI_LISTBOX_FLAGS_MENU_ITEM             = (1u<<1),
    CMDIF_REMOTEGUI_LISTBOX_FLAGS_MENU_DESCRIPTION_ITEM = (1u<<2),
//text is "BITMAP#" to point to which bitmap to use
    CMDIF_REMOTEGUI_LISTBOX_FLAGS_BITMAP_ITEM           = (1u<<3),
//use with normal items with short text to improve readability
    CMDIF_REMOTEGUI_LISTBOX_FLAGS_ALLOW_LARGER_FONT     = (1u<<7),
    CMDIF_REMOTEGUI_LISTBOX_FLAGS_HASARROW              = (1u<<8),
    CMDIF_REMOTEGUI_LISTBOX_FLAGS_ALIGN_CENTER          = (1u<<9),
    CMDIF_REMOTEGUI_LISTBOX_FLAGS_ALIGN_RIGHT           = (1u<<10),
    CMDIF_REMOTEGUI_LISTBOX_FLAGS_CLICKABLE             = (1u<<11),
    CMDIF_REMOTEGUI_LISTBOX_FLAGS_CHECKABLE             = (1u<<12),
    CMDIF_REMOTEGUI_LISTBOX_FLAGS_CHECKED               = (1u<<13),
    CMDIF_REMOTEGUI_LISTBOX_FLAGS_SUBTEXT_HIGHLIGHTED   = (1u<<14),
}CMDIF_REMOTEGUI_LISTBOX_FLAGS;

typedef enum
{
    keyon                                   = 1,
    keyoff                                  = 2,
    keycycle                                = 3,
}DCX_Key_state;

u8 cmdif_remotegui_listboxbox_init(const u8 *title, const u8 *persistent_header);
u8 cmdif_remotegui_listboxbox_additem(const u8 *text, const u8 *subtext, u16 flags);
void cmdif_remotegui_listbox(CMDIF_COMMAND cmd, u16 flags, u8 current_item_index,
                             CMDIF_REMOTEGUI_BUTTONFACE button0, CMDIF_REMOTEGUI_BUTTONFACE button1, CMDIF_REMOTEGUI_BUTTONFACE button2,
                             CMDIF_REMOTEGUI_FOCUS focus);
void cmdif_remotegui_listboxbox_cleanup();

void cmdif_remotegui_messagebox(CMDIF_COMMAND cmd, const u8 *title, CMDIF_REMOTEGUI_ICON icon,
                                CMDIF_REMOTEGUI_BUTTONFACE button0, CMDIF_REMOTEGUI_BUTTONFACE button1, CMDIF_REMOTEGUI_BUTTONFACE button2,
                                CMDIF_REMOTEGUI_FOCUS focus, const u8 *text);

void cmdif_remotegui_keyboard(CMDIF_COMMAND cmd, const u8 *title, const u8 *initial_string,
                              CMDIF_REMOTEGUI_KEYBOARD_DEFAULTTYPE defaulttype);
void cmdif_remotegui_numpad(CMDIF_COMMAND cmd, const u8 *title, const u8 *unit,
                            CMDIF_REMOTEGUI_NUMPAD_INPUTTYPE inputtype,
                            float initial_value, float range_low, float range_high);

u8 cmdif_remotegui_terminate(CMDIF_COMMAND cmd);
u8 cmdif_remotegui_peek_user_response(CMDIF_REMOTEGUI_RESPONSETYPE *responsetype,
                                      u16 *responsechoice, u8 *responsedata);
u8 cmdif_remotegui_wait_user_response(CMDIF_REMOTEGUI_RESPONSETYPE *responsetype,
                                      u16 *responsechoice, u8 *responsedata);
u8 cmdif_remotegui_wait_user_response_keyboard(u8 *string_out, u8 maxlength);
u8 cmdif_remotegui_wait_user_response_numpad(float *value_out);
void cmdif_remotegui_msg_keycycle(CMDIF_COMMAND cmd, const u8 *title);
void cmdif_remotegui_msg_keyoff(CMDIF_COMMAND cmd, const u8 *title);
void cmdif_remotegui_msg_keyon(CMDIF_COMMAND cmd, const u8 *title);
void cmdif_remotegui_keypadinput(CMDIF_COMMAND cmd, const u8 *title, CMDIF_REMOTEGUI_ICON icon,
                                const u8 *text, const u8 *values);

#endif //__CMDIF_REMOTEGUI_H
