/**
  **************** (C) COPYRIGHT 2011 SCT Performance, LLC *********************
  * File Name          : ecm_defs.h
  * Device / System    : SCT products using the "CommonCode" platform
  * Author             : Quyen Leba
  *
  * Version            : 1 
  * Date               : 05/02/2011
  * Description        : 
  *                    : 
  *
  *
  * History            : 05/02/2011 M. Davis
  *                    :   Add SCT Copyright header,
  *
  ******************************************************************************
  */

#ifndef __ECM_DEFS_H
#define __ECM_DEFS_H

#include <common/veh_defs.h>

/******************************************************************************/
/* Definition for utility flags */
/******************************************************************************/
#define UTILITY_FLAGS_NONE                      (0)
/* Bit[0] */
#define UTILITY_FLAGS_SEPARATE_EXE              (1<<0)
/* Bit[1] */
#define UTILITY_FLAGS_EXE_HAS_REPONSE           (1<<1)
/* Bit[2] */
#define UTILITY_FLAGS_EXE_ALLOW_NEG_RESPONSE    (1<<2)
/* Bit[3] */
#define UTILITY_FLAGS_USE_ENCRYPTION            (1<<3)
/* Bit[4] */
#define UTILITY_FLAGS_LAST_36_FAIL_OK           (1<<4)

/******************************************************************************/
/* Definition for memblock flags */
/******************************************************************************/
enum
{
    MEMBLOCK_FLAGS_NONE                                 = (0),
    /* Bit[7...0]: order */
    ORDER_0                                             = 0,
    ORDER_1                                             = 1,
    ORDER_2                                             = 2,
    ORDER_3                                             = 3,
    ORDER_4                                             = 4,
    /* Bit[15...8]: type */
    /* Memoryblock block_size */
    MEMBLOCK_FLAGS_AS_MAX_BLOCK_SIZE                    = (1<<8),
    /* Fill the memblock with 'S', no upload */
    MEMBLOCK_FLAGS_PRE_FILL_WITH_S                      = (8<<8),
    /* Fill the memblock with 0xFF, no upload */
    MEMBLOCK_FLAGS_PRE_FILL_WITH_FF                     = (9<<8),
    /* After memblock upload, append flagdata byte of 0xFF */
    MEMBLOCK_FLAGS_SIMPLE_FF_APPEND                     = (10<<8),
    /* After memblock upload, append flagdata byte of 0x00 */
    MEMBLOCK_FLAGS_SIMPLE_00_APPEND                     = (11<<8),
/*    MEMBLOCK_FLAGS_SIMPLE_FF_FILL                       = (12<<8), */
/*    MEMBLOCK_FLAGS_SIMPLE_00_FILL                       = (13<<8), */
    /* Append content of a file after memblock upload */
    MEMBLOCK_FLAGS_APPEND_FILE_BY_INDEX                 = (20<<8),
    /* Add offset to memblock address */
    MEMBLOCK_FLAGS_ADDRESS_OFFSET_BY_FDATA              = (21<<8),
    /* Bit[31...16]: control (bitwise) */
    MEMBLOCK_FLAGS_NOP                                  = 0x80000000,
    /* Don't allow download, skip download */
    MEMBLOCK_FLAGS_SKIP_DOWNLOAD                        = 0x40000000,
    /* Memblock masks */
    /* Mask to get flag order */
    MEMBLOCK_FLAGS_ORDER_MASK                           = 0x000000FF,
    /* Mask to get flag type */
    MEMBLOCK_FLAGS_TYPE_MASK                            = 0x0000FF00,
    /* Mask to get flag control */
    MEMBLOCK_FLAGS_CONTROL_MASK                         = 0xFFFF0000,
};

/******************************************************************************/
/* Definition for ecm def flags */
/******************************************************************************/
#define ECM_FLAGS_NONE                                  (0)
/* Bit[0] */
#define UPLOAD_CAL_ONLY                                 (1 << 0)
/* Bit[1] */
#define DOWNLOAD_ALL                                    (1 << 1)
/* Bit[2]: Require erase command */
#define USE_ERASE_COMMAND                               (1 << 2)
/* Bit[3] */
#define ERASE_COMMAND_COMPLEX                           (1 << 3)
/* Bit[4] */
#define USE_DOWNLOAD_EXECUTE                            (1 << 4)
/* Bit[5] */
#define UPLOAD_BY_DETECT_CMD                            (1 << 5)
/* Bit[6]: Upload by $20 with extended ID (a special case in Allison TCM) */
#define UPLOAD_BY_CMD_20_EXTENDED_ID                    (1 << 6)
/* Bit[7]: Upload by $23 */
#define UPLOAD_BY_CMD_23                                (1 << 7)
/* Bit[8]: Upload by request with $35 + single $36 block transfer */
#define UPLOAD_BY_CMD_35_SINGLE_36                      (1 << 8)
/* Bit[9]: Upload by request with $35 + multiple $36 blocks transfer */
#define UPLOAD_BY_CMD_35_MULTI_36                       (1 << 9)
/* Bit[10] */
#define UPLOAD_ADDR_TYPE_3B                             (1 << 10)
/* Bit[11] */
#define UPLOAD_ADDR_TYPE_4B                             (1 << 11)
/* Bit[12] */
#define DOWNLOAD_REQUEST_TYPE_3B                        (1 << 12)
/* Bit[13] */
#define DOWNLOAD_REQUEST_TYPE_4B                        (1 << 13)
/* Bit[14] */
#define DOWNLOAD_ADDR_TYPE_3B                           (1 << 14)
/* Bit[15] */
#define DOWNLOAD_ADDR_TYPE_4B                           (1 << 15)
/* Bit[16] */
#define DOWNLOAD_BLOCK_BACKWARD                         (1 << 16)
/* Bit[17] */
#define SINGLE_UNLOCK_ONLY                              (1 << 17)
/* Bit[18] */
#define UPLOAD_REQUIRE_CMD_37                           (1 << 18)
/* Bit[19] */
#define DOWNLOAD_REQUIRE_CMD_37                         (1 << 19)
/* Bit[20] */
#define CAN_READ_OS_NUMBER_BY_DMR                       (1 << 20)
/* Bit[21] */
#define UTILITY_CHECK                                   (1 << 21)
/* Bit[22]: Should only use for debug */
#define FORCE_DOWNLOAD_ALL                              (1 << 22)
/* Bit[23]: Should only use for debug */
#define FORCE_DOWNLOAD_CAL_ONLY                         (1 << 23)
/* Bit[24]: If stock-data file is available to bypass upload, block it */
#define BLOCK_GENERATE_STOCK                            (1 << 24)
/* Bit[27..25]: Cooldown time: Some processors are too slow, need to slow down 
 * data transfer.
 */
#define COOLDOWNTIME_NONE                               (0)
/* SHORT (300), SHORT_1 (200), SHORT_2 (100), SHORT_3 (50) */
#define COOLDOWNTIME_SHORT                              (1 << 25)
#define COOLDOWNTIME_SHORT_1                            (3 << 25)
#define COOLDOWNTIME_SHORT_2                            (5 << 25)
#define COOLDOWNTIME_SHORT_3                            (6 << 25)
/* MEDIUM (800), MEDIUM_1 (500) */
#define COOLDOWNTIME_MEDIUM                             (2 << 25)
#define COOLDOWNTIME_MEDIUM_1                           (7 << 25)
/* LONG (2000) */
#define COOLDOWNTIME_LONG                               (4 << 25)
/* Bit[28]: Skip $34 if download only uses $36 */
#define DOWNLOAD_SKIP_REQUEST_CMD_34                    (1 << 28)
/* Bit[29]: Don't need to re-check wakeup when handle ECM */
#define SIMPLE_WAKEUP_CHECK_UPLOAD                      (1 << 29)
/* Bit[30] */
#define SIMPLE_WAKEUP_CHECK_DOWNLOAD                    (1 << 30)
/* Bit[31] */
#define FORCE_FULL_STOCK_FILE_BEFORE_FLASH              (1ul << 31)

/******************************************************************************/
/* Definition for ecm def extended flags */
/******************************************************************************/
/* Bit[0]: Used with FinalizeId to send commands at the end of download */
#define REQUIRE_DOWNLOAD_FINALIZING                     (1 << 0)
/* Bit[1]: Available */
/* Bit[2]: Default Endian if not specified in file */
#define LITTLE_ENDIAN                                   (1 << 2)
/* Bit[3]: Data from the tuneos file is required when flashing this ECM */
#define TUNEOSDATA_REQUIRED                             (1 << 3)
/* Bit[4]: Disable normal comm at download blocks */
#define CONSTANT_DISABLE_COMM_AT_BLOCKDOWNLOAD_REQUIRED (1 << 4)
/* Bit[5]: Preloaded Stock File is required, ie. when upload is not support */
#define SPF_REQUIRED                                    (1 << 5)
/* Bit[6]: Clears the ECM KAM (Keep alive memory) at the end of download */
#define CLEAR_KAM_AFTER_DOWNLOAD                        (1 << 6)
/* Bit[7]: Skips the last 37 command on the download */
#define SKIP_LAST_37_COMMAND                            (1 << 7)
/* Bit[9..8]: Check if PCM/TCM dead after download
 *            Strict: must pass within programming process
 *            After ignition key recycle: a retry after key recycle
 */
#define DOWNLOAD_HEALTH_CHECK_NONE                      (0 << 8)
#define DOWNLOAD_HEALTH_CHECK_STRICT                    (1 << 8)
#define DOWNLOAD_HEALTH_CHECK_AFTER_IGNKEY_RECYCLE      (2 << 8)
/* Bit[11..10]: Enabled power down time after Key Off (15,30,60 second wait) */
#define REQUIRE_KEYOFF_POWERDOWN_MASK                   (3 << 10)
#define REQUIRE_KEYOFF_POWERDOWN_NONE                   (0)
#define REQUIRE_KEYOFF_POWERDOWN_15S                    (1 << 10)
#define REQUIRE_KEYOFF_POWERDOWN_30S                    (2 << 10)
#define REQUIRE_KEYOFF_POWERDOWN_45S                    (3 << 10)
/* Bit[12]: Overlay VIN in a non-critical area (such as VID area) to enable VIN
 *          search in VIN with blank VIN (such as 5.0L Mustang crate motor).
 *          VIN address must be defined in ecm_defs.
 */
#define ALLOW_INDIRECT_BLANKVIN_OVERLAY                 (1 << 12)
/* Bit[13]: Used to ask user to key off and key on before downloading tune */
#define REQUIRE_KEYOFF_BEFORE_DOWNLOAD                  (1 << 13)
/* Bit[14]: Used to have the utility check via 1A 21 and 1A BB */
#define UTILITY_CHECK_LEGACY                            (1 << 14)
/* Bit[15]: Use .csf file for checksums */
#define CSF_REQUIRED                                    (1 << 15)
/* Bit[16]: Use fingerprint interlock for download */
#define USE_FINGERPRINT_INTERLOCK                       (1 << 16)
/* Bit[17]: Requires E2 download */
#define E2_REQUIRED                                     (1 << 17)
/* Bit[18]: Used to indicate that the flash.bin file needs to be overlayed with 
 *          stock.bin file.
 */
#define OVERLAY_FLASH                                   (1 << 18)
/* Bit[19]: Used to check if the processor requires a key off/on in order to 
 *          clear DTCs at the end of either an upload only or flash. 
 */
#define REQUIRE_KEY_DTC                                 (1 << 19)
/* Bit[20]: Used to indicate that a test is required to make sure processor is 
 *          properly responding.
 */
#define REQUIRE_KEY_ON_TEST                             (1 << 20)
/* Bit{21]: Used to indicate whether the processor has RSA signature */
#define RSA_SIGNATURE                                   (1 << 21)
/* Bit[22]: Used to check if processor needs a special pre download task. */
#define PREDOWNLOAD_TASK                                (1 << 22)
/* Bit[23]: Used to check if the processor has a specific variant ID. */
#define USE_VARIANT_ID                                  (1 << 23)
/* Bit[24]: Used to check if the processor may require transmission info to be
 *          written.
 */
#define TRANS_INFO                                      (1 << 24)
/*Bit[25]: Used to check if the processor requires a signature patch*/
#define REQUIRE_SIGNATURE_PATCH                         (1 << 25)

/******************************************************************************/
/* Definition for unlock flags */
/******************************************************************************/
#define UNLOCK_FLAG_NONE                                (0)
#define UNLOCK_FLAG_UPLOAD_NO_UNLOCK                    (1 << 0)

typedef enum
{
    ECM_ecm_index,
    ECM_upload_unlock_id,
    ECM_download_unlock_id,
    ECM_unlock_flags,
    ECM_checksum_id,
    ECM_finalizing_id,
    ECM_ecm_id,
    ECM_reponse_ecm_id,
    ECM_commtype,
    ECM_commlevel,
    ECM_eraseall_cmd,
    ECM_erasecal_cmd,
    ECM_testerpresent_frequency,
    ECM_os_test_method,
    ECM_maxuploadblocksize,
    ECM_maxdownloadblocksize,
    ECM_data_offset,
    ECM_vid_address,
    ECM_pats_address,
    ECM_vin_address,
    ECM_flags,
    ECM_extended_flags,
    ECM_util_upload,
    ECM_util_dnload,
    ECM_os_block_read,
    ECM_os_block,
    ECM_cal_block
}ECM_Param;

typedef enum
{
    U_UTI_index,
    U_UTI_filename,
    U_UTI_flags,
    U_UTI_call_address,
    U_UTI_block,
    D_UTI_index,
    D_UTI_filename,
    D_UTI_flags,
    D_UTI_call_address,
    D_UTI_block
}UTI_Param;

/******************************************************************************/
/* Definition for ecm def macros */
/******************************************************************************/
#define ecmvar(et,var)                      vehdef_ecmvar(et,var)
#define utivar(et,t,var)                    vehdef_utivar(et,t,var)

#define ECM_GetEcmIndex(et)                 (*((u16*)ecmvar(et,ECM_ecm_index)))
#define ECM_GetEcmId(et)                    (*((u32*)ecmvar(et,ECM_ecm_id)))
#define ECM_GetResponseECMId(et)            (*((u32*)ecmvar(et,ECM_reponse_ecm_id)))
#define ECM_GetUnlockFlags(et)              (*((u16*)ecmvar(et,ECM_unlock_flags)))
#define ECM_GetUnlockIndex(et)              (*((u16*)ecmvar(et,ECM_upload_unlock_id)))
#define ECM_GetDownloadUnlockIndex(et)      (*((u16*)ecmvar(et,ECM_download_unlock_id)))
#define ECM_GetChecksumId(et)               (*((u16*)ecmvar(et,ECM_checksum_id)))
#define ECM_GetFinalizeId(et)               (*((u16*)ecmvar(et,ECM_finalizing_id)))
#define ECM_GetCommType(et)                 (*((VehicleCommType*)ecmvar(et,ECM_commtype)))
#define ECM_GetCommLevel(et)                (*((VehicleCommLevel*)ecmvar(et,ECM_commlevel)))
#define ECM_GetFlags(et)                    (*((u32*)ecmvar(et,ECM_flags)))
#define ECM_GetExtendedFlags(et)            (*((u32*)ecmvar(et,ECM_extended_flags)))
#define ECM_GetEraseAllCmd(et)              (*((EcuEraseCmdInfo*)ecmvar(et,ECM_eraseall_cmd)))
#define ECM_GetEraseCalCmd(et)              (*((EcuEraseCmdInfo*)ecmvar(et,ECM_erasecal_cmd)))
#define ECM_GetTesterPresentFrequency(et)   (*((u8*)ecmvar(et,ECM_testerpresent_frequency)))
#define ECM_GetVidAddress(et)               (*((u32*)ecmvar(et,ECM_vid_address)))
#define ECM_GetPatsAddress(et)              (*((u32*)ecmvar(et,ECM_pats_address)))
#define ECM_GetVINsAddress(et)              (*((u32*)ecmvar(et,ECM_vin_address)))
#define ECM_IsValidVINsAddress(et)          (ECM_GetVINsAddress(et) != ECM_DEFS_INVALID_VIN_ADDRESS)
#define ECM_GetDataOffset(et)               (*((u32*)ecmvar(et,ECM_data_offset)))
#define ECM_GetUUtiIndex(et,t)              (((u16*)ecmvar(et,ECM_util_upload))[t])
#define ECM_GetDUtiIndex(et,t)              (((u16*)ecmvar(et,ECM_util_dnload))[t])

/* Upload Utility */
#define ECM_IsUUtiRequired(et)              (*((u16*)utivar(et,0,U_UTI_index)) != 0xFFFF ? 1 : 0)
#define ECM_GetUUtiFilename(et,t)           (utivar(et,t,U_UTI_filename))
#define ECM_GetUUtiFlags(et,t)              (*((u16*)utivar(et,t,U_UTI_flags)))
#define ECM_GetUUtiCallAddress(et,t)        (*((u32*)utivar(et,t,U_UTI_call_address)))
#define ECM_GetUUtiBlock(et,t,i)            (((EcuMemBlockInfo*)utivar(et,t,U_UTI_block))[i])
#define ECM_GetUUtiBlockAddr(et,t,i)        (ECM_GetUUtiBlock(et,t,i).start_addr)
#define ECM_GetUUtiBlockSize(et,t,i)        (ECM_GetUUtiBlock(et,t,i).block_size)
#define ECM_GetUUtiBlockFlags(et,t,i)       (ECM_GetUUtiBlock(et,t,i).flags)
#define ECM_GetUUtiBlockFlagData(et,t,i)    (ECM_GetUUtiBlock(et,t,i).flagdata)
#define ECM_GetUUtiBlockFlagType(et,t,i)    (ECM_GetUUtiBlockFlags(et,t,i) & MEMBLOCK_FLAGS_TYPE_MASK)
#define ECM_GetUUtiBlockFlagOrder(et,t,i)   (ECM_GetUUtiBlockFlags(et,t,i) & MEMBLOCK_FLAGS_ORDER_MASK)
#define ECM_GetUUtiBlockFlagControl(et,t,i) (ECM_GetUUtiBlockFlags(et,t,i) & MEMBLOCK_FLAGS_CONTROL_MASK)
#define ECM_IsUUtiBlockSkipDownload(et,t,i) ((ECM_GetUUtiBlockFlagControl(et,t,i) & MEMBLOCK_FLAGS_SKIP_DOWNLOAD) == MEMBLOCK_FLAGS_SKIP_DOWNLOAD)

/* Upload Utility Flags */
#define ECM_IsUUtiSeparateExe(et,t)         ((ECM_GetUUtiFlags(et,t) & UTILITY_FLAGS_SEPARATE_EXE) == UTILITY_FLAGS_SEPARATE_EXE)
#define ECM_IsUUtiExeHasResponse(et,t)      ((ECM_GetUUtiFlags(et,t) & UTILITY_FLAGS_EXE_HAS_REPONSE) == UTILITY_FLAGS_EXE_HAS_REPONSE)
#define ECM_IsUUtiExeAllowNegResponse(et,t) ((ECM_GetUUtiFlags(et,t) & UTILITY_FLAGS_EXE_ALLOW_NEG_RESPONSE) == UTILITY_FLAGS_EXE_ALLOW_NEG_RESPONSE)
#define ECM_IsUUtiUseEncryption(et,t)       ((ECM_GetUUtiFlags(et,t) & UTILITY_FLAGS_USE_ENCRYPTION) == UTILITY_FLAGS_USE_ENCRYPTION)
#define ECM_IsUUtiLast36FailOk(et,t)        ((ECM_GetUUtiFlags(et,t) & UTILITY_FLAGS_LAST_36_FAIL_OK) == UTILITY_FLAGS_LAST_36_FAIL_OK)

/* Download Utility */
#define ECM_IsDUtiRequired(et)              (*((u16*)utivar(et,0,D_UTI_index)) != 0xFFFF ? 1 : 0)
#define ECM_GetDUtiFilename(et,t)           (utivar(et,t,D_UTI_filename))
#define ECM_GetDUtiFlags(et,t)              (*((u16*)utivar(et,t,D_UTI_flags)))
#define ECM_GetDUtiCallAddress(et,t)        (*((u32*)utivar(et,t,D_UTI_call_address)))
#define ECM_GetDUtiBlock(et,t,i)            (((EcuMemBlockInfo*)utivar(et,t,D_UTI_block))[i])
#define ECM_GetDUtiBlockAddr(et,t,i)        (ECM_GetDUtiBlock(et,t,i).start_addr)
#define ECM_GetDUtiBlockSize(et,t,i)        (ECM_GetDUtiBlock(et,t,i).block_size)
#define ECM_GetDUtiBlockFlags(et,t,i)       (ECM_GetDUtiBlock(et,t,i).flags)
#define ECM_GetDUtiBlockFlagData(et,t,i)    (ECM_GetDUtiBlock(et,t,i).flagdata)
#define ECM_GetDUtiBlockFlagType(et,t,i)    (ECM_GetDUtiBlockFlags(et,t,i) & MEMBLOCK_FLAGS_TYPE_MASK)
#define ECM_GetDUtiBlockFlagOrder(et,t,i)   (ECM_GetDUtiBlockFlags(et,t,i) & MEMBLOCK_FLAGS_ORDER_MASK)
#define ECM_GetDUtiBlockFlagControl(et,t,i) (ECM_GetDUtiBlockFlags(et,t,i) & MEMBLOCK_FLAGS_CONTROL_MASK)
#define ECM_IsDUtiBlockSkipDownload(et,t,i) ((ECM_GetDUtiBlockFlagControl(et,t,i) & MEMBLOCK_FLAGS_SKIP_DOWNLOAD) == MEMBLOCK_FLAGS_SKIP_DOWNLOAD)

/* Download Utility Flags */
#define ECM_IsDUtiSeparateExe(et,t)         ((ECM_GetDUtiFlags(et,t) & UTILITY_FLAGS_SEPARATE_EXE) == UTILITY_FLAGS_SEPARATE_EXE)
#define ECM_IsDUtiExeHasResponse(et,t)      ((ECM_GetDUtiFlags(et,t) & UTILITY_FLAGS_EXE_HAS_REPONSE) == UTILITY_FLAGS_EXE_HAS_REPONSE)
#define ECM_IsDUtiExeAllowNegResponse(et,t) ((ECM_GetDUtiFlags(et,t) & UTILITY_FLAGS_EXE_ALLOW_NEG_RESPONSE) == UTILITY_FLAGS_EXE_ALLOW_NEG_RESPONSE)
#define ECM_IsDUtiUseEncryption(et,t)       ((ECM_GetDUtiFlags(et,t) & UTILITY_FLAGS_USE_ENCRYPTION) == UTILITY_FLAGS_USE_ENCRYPTION)
#define ECM_IsDUtiLast36FailOk(et,t)        ((ECM_GetDUtiFlags(et,t) & UTILITY_FLAGS_LAST_36_FAIL_OK) == UTILITY_FLAGS_LAST_36_FAIL_OK)

#define ECM_GetOSTestMethod(et)             (*((u16*)ecmvar(et,ECM_os_test_method)))
#define ECM_GetUploadBlockSize(et)          (*((u16*)ecmvar(et,ECM_maxuploadblocksize)))
#define ECM_GetDownloadBlockSize(et)        (*((u16*)ecmvar(et,ECM_maxdownloadblocksize)))

/* OS Block Read */
#define ECM_GetOSReadBlock(et,i)            (((EcuMemBlockInfo*)ecmvar(et,ECM_os_block_read))[i])
#define ECM_GetOSReadBlockAddr(et,i)        (ECM_GetOSReadBlock(et,i).start_addr)
#define ECM_GetOSReadBlockSize(et,i)        (ECM_GetOSReadBlock(et,i).block_size)
#define ECM_GetOSReadBlockFlags(et,i)       (ECM_GetOSReadBlock(et,i).flags)
#define ECM_GetOSReadBlockFlagData(et,i)    (ECM_GetOSReadBlock(et,i).flagdata)
#define ECM_GetOSReadBlockFlagType(et,i)    (ECM_GetOSReadBlockFlags(et,i) & MEMBLOCK_FLAGS_TYPE_MASK)
#define ECM_GetOSReadBlockFlagOrder(et,i)   (ECM_GetOSReadBlockFlags(et,i) & MEMBLOCK_FLAGS_ORDER_MASK)
#define ECM_GetOSReadBlockFlagControl(et,i) (ECM_GetOSReadBlockFlags(et,i) & MEMBLOCK_FLAGS_CONTROL_MASK)

/* OS Block */
#define ECM_GetOSBlock(et,i)                (((EcuMemBlockInfo*)ecmvar(et,ECM_os_block))[i])
#define ECM_GetOSBlockAddr(et,i)            (ECM_GetOSBlock(et,i).start_addr)
#define ECM_GetOSBlockSize(et,i)            (ECM_GetOSBlock(et,i).block_size)
#define ECM_GetOSBlockFlags(et,i)           (ECM_GetOSBlock(et,i).flags)
#define ECM_GetOSBlockFlagData(et,i)        (ECM_GetOSBlock(et,i).flagdata)
#define ECM_GetOSBlockFlagType(et,i)        (ECM_GetOSBlockFlags(et,i) & MEMBLOCK_FLAGS_TYPE_MASK)
#define ECM_GetOSBlockFlagOrder(et,i)       (ECM_GetOSBlockFlags(et,i) & MEMBLOCK_FLAGS_ORDER_MASK)
#define ECM_GetOSBlockFlagControl(et,i)     (ECM_GetOSBlockFlags(et,i) & MEMBLOCK_FLAGS_CONTROL_MASK)
#define ECM_IsOSBlockSkipDownload(et,i)     ((ECM_GetOSBlockFlagControl(et,i) & MEMBLOCK_FLAGS_SKIP_DOWNLOAD) == MEMBLOCK_FLAGS_SKIP_DOWNLOAD)

/* CAL Block */
#define ECM_GetCalBlock(et,i)               (((EcuMemBlockInfo*)ecmvar(et,ECM_cal_block))[i])
#define ECM_GetCalBlockAddr(et,i)           (ECM_GetCalBlock(et,i).start_addr)
#define ECM_GetCalBlockSize(et,i)           (ECM_GetCalBlock(et,i).block_size)
#define ECM_GetCalBlockFlags(et,i)          (ECM_GetCalBlock(et,i).flags)
#define ECM_GetCalBlockFlagData(et,i)       (ECM_GetCalBlock(et,i).flagdata)
#define ECM_GetCalBlockFlagType(et,i)       (ECM_GetCalBlockFlags(et,i) & MEMBLOCK_FLAGS_TYPE_MASK)
#define ECM_GetCalBlockFlagOrder(et,i)      (ECM_GetCalBlockFlags(et,i) & MEMBLOCK_FLAGS_ORDER_MASK)
#define ECM_GetCalBlockFlagControl(et,i)    (ECM_GetCalBlockFlags(et,i) & MEMBLOCK_FLAGS_CONTROL_MASK)
#define ECM_IsCalBlockSkipDownload(et,i)    ((ECM_GetCalBlockFlagControl(et,i) & MEMBLOCK_FLAGS_SKIP_DOWNLOAD) == MEMBLOCK_FLAGS_SKIP_DOWNLOAD)

/* Unlock Flags */
#define ECM_IsUnlockSkippedInUpload(et)     ((ECM_GetUnlockFlags(et) & UNLOCK_FLAG_UPLOAD_NO_UNLOCK) == UNLOCK_FLAG_UPLOAD_NO_UNLOCK)

/* Flags */
#define ECM_IsUploadCalOnly(et)             ((ECM_GetFlags(et) & UPLOAD_CAL_ONLY) == UPLOAD_CAL_ONLY)
#define ECM_IsDownloadAll(et)               ((ECM_GetFlags(et) & DOWNLOAD_ALL) == DOWNLOAD_ALL)
#define ECM_IsUseEraseCommand(et)           (ECM_GetFlags(et) & USE_ERASE_COMMAND)
#define ECM_IsUseDownloadExecute(et)        (ECM_GetFlags(et) & USE_DOWNLOAD_EXECUTE)
#define ECM_IsUploadByDetectCmd(et)         (ECM_GetFlags(et) & UPLOAD_BY_DETECT_CMD)
#define ECM_IsUploadByCmd20ExtendedId(et)   (ECM_GetFlags(et) & UPLOAD_BY_CMD_20_EXTENDED_ID)
#define ECM_IsUploadByCmd23(et)             (ECM_GetFlags(et) & UPLOAD_BY_CMD_23)
#define ECM_IsUploadByCmd35Single36(et)     (ECM_GetFlags(et) & UPLOAD_BY_CMD_35_SINGLE_36)
#define ECM_IsUploadByCmd35Multi36(et)      (ECM_GetFlags(et) & UPLOAD_BY_CMD_35_MULTI_36)
#define ECM_IsUploadAddr3B(et)              (ECM_GetFlags(et) & UPLOAD_ADDR_TYPE_3B)
#define ECM_IsUploadAddr4B(et)              (ECM_GetFlags(et) & UPLOAD_ADDR_TYPE_4B)
#define ECM_IsDownloadRequest3B(et)         (ECM_GetFlags(et) & DOWNLOAD_REQUEST_TYPE_3B)
#define ECM_IsDownloadRequest4B(et)         (ECM_GetFlags(et) & DOWNLOAD_REQUEST_TYPE_4B)
#define ECM_IsDownloadAddr3B(et)            (ECM_GetFlags(et) & DOWNLOAD_ADDR_TYPE_3B)
#define ECM_IsDownloadAddr4B(et)            (ECM_GetFlags(et) & DOWNLOAD_ADDR_TYPE_4B)
#define ECM_IsDownloadBlockBackward(et)     ((ECM_GetFlags(et) & DOWNLOAD_BLOCK_BACKWARD) == DOWNLOAD_BLOCK_BACKWARD)
#define ECM_IsSingleUnlockOnly(et)          ((ECM_GetFlags(et) & SINGLE_UNLOCK_ONLY) == SINGLE_UNLOCK_ONLY)
#define ECM_IsUploadRequireCmd37(et)        ((ECM_GetFlags(et) & UPLOAD_REQUIRE_CMD_37) == UPLOAD_REQUIRE_CMD_37)
#define ECM_IsDownloadRequireCmd37(et)      ((ECM_GetFlags(et) & DOWNLOAD_REQUIRE_CMD_37) == DOWNLOAD_REQUIRE_CMD_37)
#define ECM_IsReadPartByDMR(et)             (ECM_GetFlags(et) & CAN_READ_OS_NUMBER_BY_DMR)
#define ECM_IsUtilityCheck(et)              (ECM_GetFlags(et) & UTILITY_CHECK)
#define ECM_IsForceDownloadAll(et)          (ECM_GetFlags(et) & FORCE_DOWNLOAD_ALL)
#define ECM_IsForceDownloadCalOnly(et)      (ECM_GetFlags(et) & FORCE_DOWNLOAD_CAL_ONLY)
#define ECM_IsBlockGenerateStock(et)        (ECM_GetFlags(et) & BLOCK_GENERATE_STOCK)
#define ECM_IsCoolDownTimeShort(et)         ((ECM_GetFlags(et) & COOLDOWNTIME_SHORT) == COOLDOWNTIME_SHORT)
#define ECM_IsCoolDownTimeShort_1(et)       ((ECM_GetFlags(et) & COOLDOWNTIME_SHORT) == COOLDOWNTIME_SHORT_1)
#define ECM_IsCoolDownTimeShort_2(et)       ((ECM_GetFlags(et) & COOLDOWNTIME_SHORT) == COOLDOWNTIME_SHORT_2)
#define ECM_IsCoolDownTimeShort_3(et)       ((ECM_GetFlags(et) & COOLDOWNTIME_SHORT) == COOLDOWNTIME_SHORT_3)
#define ECM_IsCoolDownTimeMedium(et)        ((ECM_GetFlags(et) & COOLDOWNTIME_MEDIUM) == COOLDOWNTIME_MEDIUM)
#define ECM_IsCoolDownTimeMedium_1(et)      ((ECM_GetFlags(et) & COOLDOWNTIME_MEDIUM) == COOLDOWNTIME_MEDIUM_1)
#define ECM_IsCoolDownTimeLong(et)          ((ECM_GetFlags(et) & COOLDOWNTIME_LONG) == COOLDOWNTIME_LONG)
#define ECM_IsDownloadSkipRequestCmd34(et)  (ECM_GetFlags(et) & DOWNLOAD_SKIP_REQUEST_CMD_34)
#define ECM_IsSimpleWakeupCheckUpload(et)   ((ECM_GetFlags(et) & SIMPLE_WAKEUP_CHECK_UPLOAD) == SIMPLE_WAKEUP_CHECK_UPLOAD)
#define ECM_IsSimpleWakeupCheckDownload(et) ((ECM_GetFlags(et) & SIMPLE_WAKEUP_CHECK_DOWNLOAD) == SIMPLE_WAKEUP_CHECK_DOWNLOAD)
#define ECM_IsForceStockFirst(et)           (ECM_GetFlags(et) & FORCE_FULL_STOCK_FILE_BEFORE_FLASH)

/* Extended Flags */
#define ECM_IsRequireDownloadFinalzing(et)  ((ECM_GetExtendedFlags(et) & REQUIRE_DOWNLOAD_FINALIZING) == REQUIRE_DOWNLOAD_FINALIZING)
#define ECM_IsLittleEndian(et)              ((ECM_GetExtendedFlags(et) & LITTLE_ENDIAN) == LITTLE_ENDIAN)
#define ECM_IsRequireTuneOsData(et)         ((ECM_GetExtendedFlags(et) & TUNEOSDATA_REQUIRED) == TUNEOSDATA_REQUIRED)
#define ECM_IsBlockDownloadDisableComm(et)  ((ECM_GetExtendedFlags(et) & CONSTANT_DISABLE_COMM_AT_BLOCKDOWNLOAD_REQUIRED) == CONSTANT_DISABLE_COMM_AT_BLOCKDOWNLOAD_REQUIRED)
#define ECM_IsSpfRequired(et)               ((ECM_GetExtendedFlags(et) & SPF_REQUIRED) == SPF_REQUIRED)
#define ECM_IsClearKam(et)                  ((ECM_GetExtendedFlags(et) & CLEAR_KAM_AFTER_DOWNLOAD) == CLEAR_KAM_AFTER_DOWNLOAD)
#define ECM_IsSkipLast37Command(et)         ((ECM_GetExtendedFlags(et) & SKIP_LAST_37_COMMAND) == SKIP_LAST_37_COMMAND)
#define ECM_IsDownloadHealthCheckNone(et)           (!ECM_IsDownloadHealthCheckStrict(et) && !ECM_IsDownloadHealthCheckKeyRecycle(et))
#define ECM_IsDownloadHealthCheckStrict(et)         ((ECM_GetExtendedFlags(et) & DOWNLOAD_HEALTH_CHECK_STRICT) == DOWNLOAD_HEALTH_CHECK_STRICT)
#define ECM_IsDownloadHealthCheckKeyRecycle(et)     ((ECM_GetExtendedFlags(et) & DOWNLOAD_HEALTH_CHECK_AFTER_IGNKEY_RECYCLE) == DOWNLOAD_HEALTH_CHECK_AFTER_IGNKEY_RECYCLE)
#define ECM_IsKeyOffPowerDown(et)           ((ECM_GetExtendedFlags(et) & REQUIRE_KEYOFF_POWERDOWN_MASK))
#define ECM_IsKeyOffPowerDown15s(et)        ((ECM_GetExtendedFlags(et) & REQUIRE_KEYOFF_POWERDOWN_15S) == REQUIRE_KEYOFF_POWERDOWN_15S)
#define ECM_IsKeyOffPowerDown30s(et)        ((ECM_GetExtendedFlags(et) & REQUIRE_KEYOFF_POWERDOWN_30S) == REQUIRE_KEYOFF_POWERDOWN_30S)
#define ECM_IsKeyOffPowerDown45s(et)        ((ECM_GetExtendedFlags(et) & REQUIRE_KEYOFF_POWERDOWN_45S) == REQUIRE_KEYOFF_POWERDOWN_45S)
#define ECM_GetPowerDownTime(et)            (ECM_IsKeyOffPowerDown15s(et) ? 15 : (ECM_IsKeyOffPowerDown30s(et) ? 30 : 60))
#define ECM_IsAllowIndirectBlankVINOverlay(et)      ((ECM_GetExtendedFlags(et) & ALLOW_INDIRECT_BLANKVIN_OVERLAY) == ALLOW_INDIRECT_BLANKVIN_OVERLAY)
#define ECM_IsRequireKeyoffBeforeDownload(et)       ((ECM_GetExtendedFlags(et) & REQUIRE_KEYOFF_BEFORE_DOWNLOAD) == REQUIRE_KEYOFF_BEFORE_DOWNLOAD)
#define ECM_IsUtilCheckLegacy(et)           ((ECM_GetExtendedFlags(et) & UTILITY_CHECK_LEGACY) == UTILITY_CHECK_LEGACY)
#define ECM_IsFingerPrintInterlock(et)      (ECM_GetExtendedFlags(et) & USE_FINGERPRINT_INTERLOCK)
#define ECM_IsCsfRequired(et)               ((ECM_GetExtendedFlags(et) & CSF_REQUIRED) == CSF_REQUIRED)
#define ECM_IsE2Required(et)                ((ECM_GetExtendedFlags(et) & E2_REQUIRED) == E2_REQUIRED)
#define ECM_IsOverlayFlashRequired(et)      ((ECM_GetExtendedFlags(et) & OVERLAY_FLASH) == OVERLAY_FLASH)
#define ECM_IsDtcKeyRequired(et)            ((ECM_GetExtendedFlags(et) & REQUIRE_KEY_DTC) == REQUIRE_KEY_DTC)
#define ECM_IsRequireKeyOnTest(et)          ((ECM_GetExtendedFlags(et) & REQUIRE_KEY_ON_TEST) == REQUIRE_KEY_ON_TEST)
#define ECM_IsRSAsignatureRequired(et)      ((ECM_GetExtendedFlags(et) & RSA_SIGNATURE) == RSA_SIGNATURE)
#define ECM_IsPreDownloadTaskRequired(et)   ((ECM_GetExtendedFlags(et) & PREDOWNLOAD_TASK) == PREDOWNLOAD_TASK)
#define ECM_IsUseVariantId(et)              ((ECM_GetExtendedFlags(et) & USE_VARIANT_ID) == USE_VARIANT_ID)
#define ECM_IsTransInfoRequired(et)         ((ECM_GetExtendedFlags(et) & TRANS_INFO) == TRANS_INFO)
#define ECM_IsSignaturePatchRequired(et)    ((ECM_GetExtendedFlags(et) & REQUIRE_SIGNATURE_PATCH) == REQUIRE_SIGNATURE_PATCH)           

#endif  //__ECM_DEFS_H
