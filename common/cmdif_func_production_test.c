/**
  **************** (C) COPYRIGHT 2011 SCT Performance, LLC *********************
  * File Name          : cmdif_func_production_test.c
  * Device / System    : SCT products using the "CommonCode" platform
  * Author             : Quyen Leba
  *
  * Version            : 1 
  * Date               : 05/02/2011
  * Description        : 
  *                    : 
  *
  *
  * History            : 05/02/2011 M. Davis
  *                    :   Add SCT Copyright header,
  *
  ******************************************************************************
  */

#ifdef __PRODUCTION_TEST__

#include <string.h>
#include <stdio.h>
#include <board/genplatform.h>
#include <board/delays.h>
#include <board/power.h>
#include <board/properties_vb.h>
#include <common/statuscode.h>
#include <common/crypto_messageblock.h>
#include <common/housekeeping.h>
#include <common/obd2.h>
#include <common/obd2comm.h>
#include "cmdif.h"
#include <CC3000/cc3000.h>
#include "device_config.h"
#include "cmdif_func_production_test.h"

extern cmdif_datainfo cmdif_datainfo_responsedata;  //from cmdif.c
extern bool usb_enumeration_detected;               //from usb_prop.c

//------------------------------------------------------------------------------
// Production test of board rev hardware
// Output:  u8  *testreport (appending report)
// Return:  u8  status (not important)
// Engineer: Quyen Leba
//------------------------------------------------------------------------------
u8 cmdif_func_production_test_rev(u8 *testreport)
{
    u8  workdata[256];
    u32 tmpu32;
    u8  status;

    status = properties_mb_getinfo(workdata,&tmpu32);
    if (status == S_SUCCESS && tmpu32 == sizeof(PROPERTIES_MB_INFO))
    {
        PROPERTIES_MB_INFO *mbinfo = (PROPERTIES_MB_INFO *)workdata;
        if (mbinfo->board_rev != MB_REV)
        {
            strcat((char*)testreport,"MBrev bad, ");
            cmdif_datainfo_responsedata.responsecode = CMDIF_ACK_FAILED;
        }
    }
    else
    {
        strcat((char*)testreport,"MBrev ReadErr, ");
        cmdif_datainfo_responsedata.responsecode = CMDIF_ACK_FAILED;
    }

    status = properties_vb_getinfo(workdata,&tmpu32);
    if (status == S_SUCCESS && tmpu32 == sizeof(PROPERTIES_VB_INFO))
    {
        PROPERTIES_VB_INFO *vbinfo = (PROPERTIES_VB_INFO *)workdata;
        if (vbinfo->board_rev != VB_REV)
        {
            strcat((char*)testreport,"VBrev bad, ");
            cmdif_datainfo_responsedata.responsecode = CMDIF_ACK_FAILED;
        }
    }
    else
    {
        strcat((char*)testreport,"VBrev ReadErr, ");
        cmdif_datainfo_responsedata.responsecode = CMDIF_ACK_FAILED;
    }
    
    
    status = properties_ab_getinfo(workdata,&tmpu32);
    if (status == S_SUCCESS && tmpu32 == sizeof(PROPERTIES_AB_INFO))
    {
        PROPERTIES_AB_INFO *abinfo = (PROPERTIES_AB_INFO *)workdata;
        if (abinfo->board_rev != AB_REV)
        {
            strcat((char*)testreport,"ABrev bad, ");
            cmdif_datainfo_responsedata.responsecode = CMDIF_ACK_FAILED;
        }
    }
    else
    {
        strcat((char*)testreport,"ABrev ReadErr, ");
        cmdif_datainfo_responsedata.responsecode = CMDIF_ACK_FAILED;
    }

    return status;
}

//------------------------------------------------------------------------------
// Production test of VB boot enable hardware
// Output:  u8  *testreport (appending report)
// Return:  u8  status (not important)
// Engineer: Quyen Leba
//------------------------------------------------------------------------------
u8 cmdif_func_production_test_vbbooten(u8 *testreport)
{
    u8  status;

    status = bootloader_vb_forceboot_booten_pin();
    if(status != S_SUCCESS)
    {
        strcat((char*)testreport,"VB BOOT_EN Pin Err, ");
        cmdif_datainfo_responsedata.responsecode = CMDIF_ACK_FAILED;
    }
    
    bootloader_vb_resetandrun();

    return status;
}

//------------------------------------------------------------------------------
// Production test of board MB SD card & FS hardware
// Output:  u8  *testreport (appending report)
// Return:  u8  status (not important)
// Engineer: Quyen Leba
//------------------------------------------------------------------------------
u8 cmdif_func_production_test_format(u8 *testreport)
{
    u8  status;

    status = genfs_format();
    if (status != S_SUCCESS)
    {
        strcat((char*)testreport,"Format bad, ");
        cmdif_datainfo_responsedata.responsecode = CMDIF_ACK_FAILED;
    }

    return status;
}

//------------------------------------------------------------------------------
// Production test of board MB SD card & FS hardware
// Output:  u8  *testreport (appending report)
// Return:  u8  status (not important)
// Engineer: Quyen Leba
//------------------------------------------------------------------------------
u8 cmdif_func_production_test_vehicle_comm(u8 *testreport)
{
    u8  status;
    u8 rxdata[8];

    obd2can_init(Obd2canInitNormal,0);
    status = obd2can_ping(Obd2CanEcuId_7E0, FALSE);
    if (status != S_SUCCESS)
    {
        strcat((char*)testreport,"No CAN, ");
        cmdif_datainfo_responsedata.responsecode = CMDIF_ACK_FAILED;
    }
    obd2scp_init(Obd2scpType_24Bit);
    
    // Read PID "0x00", old PING function
    status = obd2scp_read_data_bypid_mode1(0x00, 1, rxdata);
    if (status != S_SUCCESS)
    {
        strcat((char*)testreport,"No SCP, ");
        cmdif_datainfo_responsedata.responsecode = CMDIF_ACK_FAILED;
    }
    return status;
}
//------------------------------------------------------------------------------
// Production test of ADC hardware
// Output:  u8  *testreport (appending report)
// Return:  u8  status (not important)
// Engineer: Quyen Leba
//------------------------------------------------------------------------------
u8 cmdif_func_production_test_adc(u8 *testreport)
{
    u8  workdata[256];
    bool ain1err;
    bool ain2err;
    u8  opcode;
    float fval;
    u32 tmpu32;
    u8  status;

    ain1err = FALSE;
    ain2err = FALSE;
    
    adc_init(ADC_DATALOG);
    //turn on 5Vext
    opcode = EXT5VCONFIG_ON;
    status = hardware_access(HardwareAccess_SetExt5VConfig,
                             &opcode, workdata,&tmpu32);
#if USE_VB_ANALOG_INPUT_PORT
    bool gpio2err = FALSE;
    
    //turn on GPIO1
    opcode = GPIO1CONFIG_ON;
    status = hardware_access(HardwareAccess_SetGPIO1Config,
                             &opcode, workdata,&tmpu32);
    delays(100,'m');
    //check 5Vext & GPIO1
    status = adc_read(ADC_AIN1,&fval);
    if (status != S_SUCCESS || fval < 4)
    {
        ain1err = TRUE;
    }
    status = adc_read(ADC_AIN2,&fval);
    if (status != S_SUCCESS || fval < 4)
    {
        ain2err = TRUE;
    }
    opcode = 0;
    status = hardware_access(HardwareAccess_GetGPIO2Input,
                             &opcode, workdata,&tmpu32);
    if (status != S_SUCCESS || workdata[0] != 1)
    {
        gpio2err = TRUE;
    }
    //turn off GPIO1
    opcode = GPIO1CONFIG_OFF;
    status = hardware_access(HardwareAccess_SetGPIO1Config,
                             &opcode, workdata,&tmpu32);
    //check GPIO1
    opcode = 0;
    status = hardware_access(HardwareAccess_GetGPIO2Input,
                             &opcode, workdata,&tmpu32);
    if (status != S_SUCCESS || workdata[0] != 0)
    {
        gpio2err = TRUE;
    }
    
    //turn off 5Vext
    opcode = EXT5VCONFIG_OFF;
    status = hardware_access(HardwareAccess_SetExt5VConfig,
                             &opcode, workdata,&tmpu32);
    delays(100,'m');
    //check 5Vext
    status = adc_read(ADC_AIN1,&fval);
    if (status != S_SUCCESS || fval > 0.5)
    {
        ain1err = TRUE;
    }
    status = adc_read(ADC_AIN2,&fval);
    if (status != S_SUCCESS || fval > 0.5)
    {
        ain2err = TRUE;
    }
    
    if (ain1err)
    {
        strcat((char*)testreport,"AIN1 err, ");
        cmdif_datainfo_responsedata.responsecode = CMDIF_ACK_FAILED;
    }
    if (ain2err)
    {
        strcat((char*)testreport,"AIN2 err, ");
        cmdif_datainfo_responsedata.responsecode = CMDIF_ACK_FAILED;
    }
    if (gpio2err)
    {
        strcat((char*)testreport,"GPIO2 err, ");
        cmdif_datainfo_responsedata.responsecode = CMDIF_ACK_FAILED;
    }
#else
    delays(100,'m');
    //check 5Vext on AIN7 for 5V
    status = adc_read(ADC_AIN7,&fval);
    if (status != S_SUCCESS || fval < 4)
    {
        ain1err = TRUE;
    }
    //check 5Vext on AIN8 for 5V
    status = adc_read(ADC_AIN8,&fval);
    if (status != S_SUCCESS || fval < 4)
    {
        ain2err = TRUE;
    }
    //turn off 5Vext
    opcode = EXT5VCONFIG_OFF;
    status = hardware_access(HardwareAccess_SetExt5VConfig,
                             &opcode, workdata,&tmpu32);
    delays(100,'m');
    //check 5Vext on AIN7 for 5V
    status = adc_read(ADC_AIN7,&fval);
    if (status != S_SUCCESS || fval > 0.5)
    {
        ain1err = TRUE;
    }
    //check 5Vext on AIN8 for 5V
    status = adc_read(ADC_AIN8,&fval);
    if (status != S_SUCCESS || fval > 0.5)
    {
        ain2err = TRUE;
    }
    
    if (ain1err)
    {
        strcat((char*)testreport,"AIN1 err, ");
        cmdif_datainfo_responsedata.responsecode = CMDIF_ACK_FAILED;
    }
    if (ain2err)
    {
        strcat((char*)testreport,"AIN2 err, ");
        cmdif_datainfo_responsedata.responsecode = CMDIF_ACK_FAILED;
    }
#endif
    //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    adc_init(ADC_FLASH);
    //VBATT
    status = adc_read(ADC_VBAT,&fval);
    if (status == S_SUCCESS)
    {
        if (fval < 9)
        {
            strcat((char*)testreport,"VBAT TooLow, ");
            cmdif_datainfo_responsedata.responsecode = CMDIF_ACK_FAILED;
        }
        else if (fval > 15)
        {
            strcat((char*)testreport,"VBAT TooHigh, ");
            cmdif_datainfo_responsedata.responsecode = CMDIF_ACK_FAILED;
        }
    }
    else
    {
        strcat((char*)testreport,"VBAT ReadErr, ");
        cmdif_datainfo_responsedata.responsecode = CMDIF_ACK_FAILED;
    }
    //VPP
    power_setvpp(Vpp_FEPS);
    delays(300,'m');
    status = adc_read(ADC_VPP,&fval);
    if (status == S_SUCCESS)
    {
        if (fval < 16)
        {
            strcat((char*)testreport,"VPP TooLow, ");
            cmdif_datainfo_responsedata.responsecode = CMDIF_ACK_FAILED;
        }
        else if (fval > 20)
        {
            strcat((char*)testreport,"VPP TooHigh, ");
            cmdif_datainfo_responsedata.responsecode = CMDIF_ACK_FAILED;
        }
    }
    else
    {
        strcat((char*)testreport,"VPP ReadErr, ");
        cmdif_datainfo_responsedata.responsecode = CMDIF_ACK_FAILED;
    }
    power_setvpp(Vpp_OFF);

    return S_SUCCESS;
}

//------------------------------------------------------------------------------
// Production test of WiFi hardware
// Output:  u8  *testreport (appending report)
// Return:  u8  status
// Engineer: Patrick Downs, Quyen Leba
//------------------------------------------------------------------------------
u8 cmdif_func_production_test_wifi(u8 *testreport)
{
#if USE_CC3000_WIFI
    u8  workdata[256];
    CC3000_AP_RESULTS *apresult_ptr;
    u32 tmpu32;
    u8  opcode;
    u8  status;

    apresult_ptr = (CC3000_AP_RESULTS *)workdata;
    cc3000_init(0);
    opcode = CC3000_SCAN_OPCODE_START;
    status = hardware_access(HardwareAccess_Scan,
                             &opcode, (u8*)apresult_ptr, &tmpu32);
    if (status == S_SUCCESS)
    {
        if (apresult_ptr->count > 0)
        {
            while(apresult_ptr->count > 1)
            {
                opcode = CC3000_SCAN_OPCODE_GETSSID;
                status = hardware_access(HardwareAccess_Scan,
                                         &opcode, (u8*)apresult_ptr, &tmpu32);
                if (status != S_SUCCESS)
                {
                    strcat((char*)testreport,"WIFI GetNextSSID Fail, ");
                    cmdif_datainfo_responsedata.responsecode = CMDIF_ACK_FAILED;
                    break;
                }
            }
        }
        else
        {
            status = S_FAIL;
            strcat((char*)testreport,"WIFI No SSIDs Found Fail, ");
            cmdif_datainfo_responsedata.responsecode = CMDIF_ACK_FAILED;
        }

        opcode = CC3000_SCAN_OPCODE_STOP;
        status = hardware_access(HardwareAccess_Scan,
                                 &opcode, (u8*)apresult_ptr, &tmpu32);
        if(status != S_SUCCESS)
        {
            status = S_FAIL;
            strcat((char*)testreport,"WIFI StopScan Fail, ");
            cmdif_datainfo_responsedata.responsecode = CMDIF_ACK_FAILED;
        }
    }
    else
    {
        status = S_FAIL;
        strcat((char*)testreport,"WIFI StartScan Fail, ");
        cmdif_datainfo_responsedata.responsecode = CMDIF_ACK_FAILED;
    }

    cc3000_deinit();

    return status;
#else
    cmdif_datainfo_responsedata.responsecode = CMDIF_ACK_UNSUPPORTED;
    return S_NOTSUPPORT;
#endif
}

//------------------------------------------------------------------------------
// Production test of USB hardware
// Output:  u8  *testreport (appending report)
// Return:  u8  status
// Engineer: Patrick Downs, Quyen Leba
//------------------------------------------------------------------------------
u8 cmdif_func_production_test_usb(u8 *testreport)
{
    u8  status;

    //Test USB RE-ENUMERATION 
    if (usb_enumeration_detected == TRUE) // Is USB Cable connected?
    {
        // Force disconnect/reconnect
        usb_enumeration_detected = FALSE;
        USB_Cable_Config(DISABLE);
        delays(100,'m');
        USB_Cable_Config(ENABLE);
        delays(1000,'m');
        // Check for re-enumeration
        if(usb_enumeration_detected != TRUE)
        {
            // Fail
            status = S_FAIL;
            strcat((char*)testreport,"USB Disconnect HW Fail, ");
            cmdif_datainfo_responsedata.responsecode = CMDIF_ACK_FAILED;
        }
        else
        {
            status = S_SUCCESS;
        }
    }
    else
    {
        // Fail, need to have USB connected before running test
        status = S_FAIL;
        strcat((char*)testreport,"No USB connected, ");
        cmdif_datainfo_responsedata.responsecode = CMDIF_ACK_FAILED;
    }

    return status;
}

//------------------------------------------------------------------------------
// Production test record completion results
// Output:  u8  *testreport (appending report)
// Return:  u8  status
// Engineer: Patrick Downs
//------------------------------------------------------------------------------
u8 cmdif_func_production_test_completion(u8 *testreport)
{
    u8 status;
    
    status = S_SUCCESS; // Not a real test, just commits settingscritical.productiontest_status to flash
    
    return status;
}

#endif  //__PRODUCTION_TEST__