/**
  **************** (C) COPYRIGHT 2011 SCT Performance, LLC *********************
  * File Name          : version.c
  * Device / System    : SCT products using the "CommonCode" platform
  * Author             : Quyen Leba
  *
  * Version            : 1 
  * Date               : 05/02/2011
  * Description        : 
  *                    : 
  *
  *
  * History            : 05/02/2011 M. Davis
  *                    :   Add SCT Copyright header,
  *
  ******************************************************************************
  */

#include <string.h>
#include <common/itoa.h>
#include <common/bootsettings.h>
#include <common/settings.h>
#include <common/statuscode.h>
#include <device_version.h>
#include <device_config.h>
#include "version.h"

#pragma location = ".firmware_tag"
const FIRMWARE_TAG firmware_tag =
{
    .signature      = APPLICATION_OPERATING_MODE,
    .version        = APPLICATION_VERSION,
    .device_type    = DEVICE_TYPE,
    .market_type    = MARKET_TYPE,
    .build          = APPLICATION_VERSION_BUILD,
    .reserved = {0,0,0,0,0,0,0,0,0,0,0,0,0,0,0},
};

//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
u8 version_firmware_patch(u32 newversion, u32 oldversion);

//------------------------------------------------------------------------------
// Get firmware version in string format
// Output:  u8  *fw_version (must be able to store 16+1 bytes)
// Return:  u8  status
// Engineer: Patrick Downs, Quyen Leba
//------------------------------------------------------------------------------
u8 version_getfirmwareversion_string(u8 *fw_version_str)
{
    u8  market[16];
    u8  hardware[16];
    u8  major[16];
    u8  minor[16];
    u8  build[16];
    
    itoa(fwversion.market,market, 10);
    itoa(fwversion.hardware,hardware, 10);
    itoa(fwversion.major,major, 10);
    itoa(fwversion.minor,minor, 10);
    itoa(fwversion.build,build, 10);

    fw_version_str[0] = NULL;
    strcpy((char*)fw_version_str, (char*)market);
    strcat((char*)fw_version_str, ".");
    strcat((char*)fw_version_str, (char*)hardware);
    strcat((char*)fw_version_str, ".");
    strcat((char*)fw_version_str, (char*)major);
    strcat((char*)fw_version_str, ".");
    strcat((char*)fw_version_str, (char*)minor);

    // Don't display the build version if it's a release firmware
    if(fwversion.build)
    {
        strcat((char*)fw_version_str, " build ");
        strcat((char*)fw_version_str, (char*)build);
    }
    return S_SUCCESS;
}

//------------------------------------------------------------------------------
// Get firmware version in integer format
// Return:  u32 version (1122333444 where 11:market, 22:hardware,
//                       333:version, 444:build)
// Engineer: Quyen Leba
//------------------------------------------------------------------------------
//u32 version_getfirmwareversion_integer()
//{
//    u32 version;
//    
//    version = fwversion.minor;
//    version += fwversion.major * 1000;
//    
//    version = fwversion.market;
//    version *= 100;
//    version += fwversion.hardware;
//    version *= 1000;
//    version += fwversion.major;
//    version *= 1000;
//    version += fwversion.minor;
//
//    return version;
//}

//------------------------------------------------------------------------------
// 
// Return:  u8  status
// Engineer: Quyen Leba
//------------------------------------------------------------------------------
u8 version_check()
{
    u32 firmware_version;
    u8  status;

    status = S_SUCCESS;
    firmware_version = APPLICATION_VERSION;
    if ((firmware_version % 1000000) != (SETTINGS_CRITICAL(firmware_version) % 1000000))
    {
        status = version_firmware_patch(firmware_version,SETTINGS_CRITICAL(firmware_version));
        if (status == S_SUCCESS)
        {
            SETTINGS_CRITICAL(firmware_version) = firmware_version;
            SETTINGS_SetCriticalAreaDirty();
            settings_update(SETTINGS_UPDATE_IF_DIRTY);
        }
    }
    if ((firmware_version % 1000000) != (BOOTSETTINGS_GetAppVersion() % 1000000))
    {
        //should not happen as bootloader already take care of this
        BOOTSETTINGS_SetAppVersion(firmware_version);
        status = bootsettings_update();
    }
    return status;
}

//------------------------------------------------------------------------------
// 
// Return:  u8  status
//------------------------------------------------------------------------------
u8 version_firmware_patch(u32 newversion, u32 oldversion)
{
    return S_SUCCESS;
}
