/**
 *  ************* Copyright 2014 SCT Performance, LLC ******
 *  @file json.c
 *  @brief Functions for handing JSON files
 *  
 *  @authors Patrick Downs
 *  @date 01/11/2014
 *  ********************************************************
 */
#ifndef __JSON_H
#define __JSON_H

#include <arch/gentype.h>
#include <fs/genfs.h>
#include <common/cmdif_func_wifi.h>

//---------------------------------------------------------------------------
// JSON File Definitions
//---------------------------------------------------------------------------
#define JSON_READ_NAME_DATA_FILEDATA_LENGTH 2048

#define JSON_NAME_SERIALNUMBER      "SerialNumber"
#define JSON_NAME_TUNEVERSION       "TuneVersion"
#define JSON_NAME_DEVICETYPE        "DeviceType"
#define JSON_NAME_MARKETTYPE        "MarketType"
#define JSON_NAME_PARTNUMBER        "PartNumber"
#define JSON_NAME_FILECOUNT         "FileCount"
#define JSON_NAME_MARRIEDCOUNT      "MarriedCount"
#define JSON_NAME_VIN               "VIN"
#define JSON_NAME_PREVIOUSVIN       "PreviousVIN"
#define JSON_NAME_STRATEGY          "Strategy"
#define JSON_NAME_CUSTOMTUNENAME    "CustomTuneName"
#define JSON_NAME_FLASHLIMIT        "FlashLimit"
#define JSON_NAME_REQUESTINGAPP     "RequestingApplication"
#define JSON_NAME_DEVICEFLAGS       "DeviceFlags"
#define JSON_NAME_MARRIEDFLAG       "MarriedFlag"
#define JSON_NAME_DLFAILEDFLAG      "DownloadFailedFlag"
#define JSON_NAME_SUPPORTINFO       "SupportInfo"
#define JSON_NAME_FWLIST            "DeviceFirmwareList"
#define JSON_NAME_FWVERSION         "FirmwareVersion"
#define JSON_NAME_FWURL             "FirmwareUrl"
#define JSON_NAME_FWNAME            "FirmwareName"
#define JSON_NAME_FWSIZE            "FirmwareSize"
#define JSON_NAME_FWCHECKSUM        "FirmwareChecksum"
#define JSON_NAME_FWTYPEID          "FirmwareTypeID"
#define JSON_NAME_BOARDTYPE         "BoardTypeID"
#define JSON_NAME_DEVICEFILENAME    "DeviceFileName"
#define JSON_NAME_ROWGUID           "RowGuid"
#define JSON_NAME_DEVICEFILESIZE    "DeviceFileSize"
#define JSON_NAME_CRC32             "Crc32"


#define JSON_BOARDTYPE_MAINBOARD        '1'
#define JSON_BOARDTYPE_VEHICLEBOARD     '2'
#define JSON_BOARDTYPE_APPBOARD         '3'

#define JSON_FWTYPEID_APP               '1'
#define JSON_FWTYPEID_MAINBOOT          '2'

//---------------------------------------------------------------------------
// FWUPDATE File Definitions
//---------------------------------------------------------------------------
#define FWUPDATE_STRUCTURE_VERSION      0
typedef struct
{
    u32 structureversion; // = Version 0
    u8  tuneversion[16];
    u8  datecreated[32];
    u32 fwblockcount;
}FWUPDATE_HEADER;

#define FWUPDATE_BOARDTYPE_MAINBOARD        1
#define FWUPDATE_BOARDTYPE_VEHICLEBOARD     2
#define FWUPDATE_BOARDTYPE_APPBOARD         3

#define FWUPDATE_FWTYPEID_APP               1
#define FWUPDATE_FWTYPEID_MAINBOOT          2
typedef struct
{
    u8  boardtypeid;
    u8  fwtypeid;
    u8  version[12]; // Ex. 1001001  v1.1b1
    u8  url[128];
    u8  name[58];
    u32 size;
    u32 crc32e;
}FWUPDATE_BLOCK;

//---------------------------------------------------------------------------

u8 json_write_open(F_FILE *f);
u8 json_write_name_data(const u8* name, const u8* data, F_FILE *f);
u8 json_write_close(F_FILE *f);
u8 json_read_name_data(const u8* name, u32 arrayIndex, const u8* subName, u32 subArrayIndex, u8* data, u32 datamaxsize, F_FILE *f);
u8 json_read_firmware_info(SERVER_FW_INFO *info, u8 boardtype, F_FILE *f);

void json_get_fwversions(u8 *destbuffer);
u8 json_generate_post_file(const u8* filename);
u8 json_read_object_count(F_FILE *f, u32 *count);

u8 json_read_object_begin(F_FILE *f);
u8 json_read_object(u8* objectdata, u32 maxdatasize, F_FILE *f);
void json_read_object_done(void);

u8 json_read_name_data_frombuffer(const u8* name, u8* data, u32 datamaxsize, u8 *objectdata);


u8 fwupdate_validate_file(const u8* filename);
u8 fwupdate_get_block(const u8* filename, u8 boardtypeid, u8 fwtypeid, FWUPDATE_BLOCK *retfwblock);
u8 fwupdate_read_firmware_info(SERVER_FW_INFO *info, u8 boardtype);

#endif    //__CMDIF_FUNC_WIFI_H