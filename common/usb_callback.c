/**
  **************** (C) COPYRIGHT 2011 SCT Performance, LLC *********************
  * File Name          : usb_callback.c
  * Device / System    : SCT products using the "CommonCode" platform
  * Author             : Quyen Leba
  *
  * Version            : 1 
  * Date               : 05/02/2011
  * Description        : 
  *                    : 
  *
  *
  * History            : 05/02/2011 M. Davis
  *                    :   Add SCT Copyright header,
  *
  ******************************************************************************
  */

#include <string.h>
#include <stdlib.h>
#include <device_config.h>
#include <board/genplatform.h>
#include <board/timer.h>
#include <common/statuscode.h>
#include <common/usbcmdhandler.h>
#include <common/usbcmdhandler_datalog.h>
#include <common/usbcmdhandler_tune.h>
#include "usb_callback.h"

#define USB_SECURITY_ENABLE         1

extern UsbCmdHandler_Datalog_Info usbcmdhandler_datalog_info;
u8  *usb_databuffer = NULL;
u32 usb_databufferindex = 0;
u32 usb_databufferlength = 0;
u32 usb_databuffermaxlength = USB_DATABUFFER_MAXSIZE;
struct
{
    u8  databuffer[2][512];
    u16 databufferindex[2];
    u16 databufferlength[2];
    u8  current_buffer_index;

//    u8  buffer[2][512];
//    u32 bufferindex[2];
//    u32 bufferlength[2];
    u16 max_data_report_frame_length;
    struct
    {
        u8  updated     : 1;
        u8  busy        : 1;
        u8  reserved    : 6;
    }control;
}usb_altbuffer = 
//usb_isobuffer = 
{
    .databufferindex = {0,0},
    .databufferlength = {0,0},    

//    .bufferindex = {0,0},
//    .bufferlength = {0,0},
    .current_buffer_index = 0,
    .control.updated = 0,
    .control.busy = 0,
};

struct
{
#ifdef __KEN_EMULATOR__
#define USB_VIRCOM_DATABUFFER_MAXSIZE       2048+16+64
#else
#define USB_VIRCOM_DATABUFFER_MAXSIZE       64
#endif
    u8  databuffer[USB_VIRCOM_DATABUFFER_MAXSIZE];
    u16 databufferindex;
    u16 databufferlength;

    struct
    {
        u8  updated     : 1;
        u8  inxferbusy  : 1;
        u8  reserved    : 6;
    }control;
}usb_vircombuffer = 
{
    .databufferindex = 0,
    .databufferlength = 0,
    .control.updated = 0,
    
    .control.inxferbusy = 0,
};

USB_CALLBACK_INFO usb_callback_info =
{
    .control =
    {
        .unlocked       = 0,
        .validseed      = 0,
    },
    .flowcontrol        = CB_FC_IDLE,
    .flowcontrol_alt    = CB_FC_IDLE,
    .flowcontrol_vircom = CB_FC_IDLE,
    .bulkbufferlength   = 0,
    .cbw.datalength     = 0,
    .pdev               = NULL,
};

//------------------------------------------------------------------------------
// Private prototypes
//------------------------------------------------------------------------------
void usb_cbw_decode(void *pdev);
void usb_outbuffercomplete_action();

//------------------------------------------------------------------------------
// Input:   void *pdev (not used in MB103V)
//------------------------------------------------------------------------------
void usb_stall_all_ep(void *pdev)
{
    SetStallEPIN();
    SetStallEPOUT();
}

//------------------------------------------------------------------------------
// Input:   void *pdev (not used in MB103V)
//------------------------------------------------------------------------------
void usb_stall_out_ep(void *pdev)
{
    SetStallEPOUT();
}

//------------------------------------------------------------------------------
// Input:   void *pdev (not used in MB103V)
//------------------------------------------------------------------------------
void usb_stall_in_ep(void *pdev)
{
    SetStallEPIN();
}

//------------------------------------------------------------------------------
// Handle callback of EP1 IN
// Inputs:  void *pdev (not used in MB103V)
//          u8  epnum (not used in MB103V)
// Engineer: Quyen Leba
//------------------------------------------------------------------------------
void usb_callback_ep1in(void *pdev, u8 epnum)
{
    switch(usb_callback_info.flowcontrol)
    {
    case CB_FC_DATA_IN:
        if (usb_databufferlength <= usb_databuffermaxlength &&
            usb_databufferlength != 0)
        {
            if ((usb_databufferindex + BULK_MAX_PACKET_SIZE) < usb_databufferlength)
            {
                SendDataEPIN(pdev,&usb_databuffer[usb_databufferindex],BULK_MAX_PACKET_SIZE);
                usb_databufferindex += BULK_MAX_PACKET_SIZE;
                SCTUSB_StatusResidue -= BULK_MAX_PACKET_SIZE;
                SetReadyEPIN();
            }
            else if (usb_databufferindex < usb_databufferlength)
            {
                u32 remaininglength;
                
                remaininglength = usb_databufferlength - usb_databufferindex;
                //send the last chunk of data
                SendDataEPIN(pdev,&usb_databuffer[usb_databufferindex],remaininglength);
                usb_databufferindex = 0;
                SCTUSB_StatusResidue -= remaininglength;
                SCTUSB_SetFlowControl(CB_FC_DATA_IN_LAST);
                SetReadyEPIN();
            }
            else
            {
                //this is error
                SCTUSB_SetFlowControl(CB_FC_IDLE);
                //enable the Endpoint to recive the next cmd
                SetReadyEPOUT_NoDataPending();
            }
        }
        else
        {
            //this is error
            SCTUSB_SetFlowControl(CB_FC_IDLE);
            //enable the Endpoint to recive the next cmd
            SetReadyEPOUT_NoDataPending();
        }
        break;
    case CB_FC_STATUS_SEND:
    case CB_FC_ERROR:
        SCTUSB_SetFlowControl(CB_FC_IDLE);
        //enable the Endpoint to recive the next cmd
        SetReadyEPOUT_NoDataPending();
        break;
    case CB_FC_DATA_IN_LAST:
        usb_set_csw(pdev,SCTUSB_GetCmdStatus(),TRUE);
        SetReadyEPOUT_NoDataPending();
        break;
    default:
        break;
    }
}

//------------------------------------------------------------------------------
// Handle callback of EP2 OUT
// Inputs:  void *pdev (not used in MB103V)
//          u8  epnum (not used in MB103V)
// Engineer: Quyen Leba
//------------------------------------------------------------------------------
void usb_callback_ep2out(void *pdev, u8 epnum)
{
    GetEPOUTData();

    switch(SCTUSB_GetFlowControl())
    {
    case CB_FC_IDLE:
        usb_cbw_decode(pdev);
        break;
    case CB_FC_DATA_OUT:
        if (usb_databufferindex + usb_callback_info.bulkbufferlength > usb_databuffermaxlength)
        {
            usb_stall_all_ep(pdev);
            usb_set_csw(pdev,S_USB_FLOWCONTROL,FALSE);
        }
        else if ((usb_databufferindex + usb_callback_info.bulkbufferlength) < usb_callback_info.cbw.datalength)
        {
            // TODOPD: Add Mem 2 Mem DMA?
            memcpy((char*)&usb_databuffer[usb_databufferindex],
                   (char*)usb_callback_info.bulkbuffer,
                   usb_callback_info.bulkbufferlength);
            usb_databufferindex += usb_callback_info.bulkbufferlength;
            SCTUSB_StatusResidue -= usb_callback_info.bulkbufferlength;

            SetReadyEPOUT_DataPending();
        }
        else if ((usb_databufferindex + usb_callback_info.bulkbufferlength) >= usb_callback_info.cbw.datalength)
        {
            //last chunk of data
            memcpy((char*)&usb_databuffer[usb_databufferindex],
                   (char*)usb_callback_info.bulkbuffer,
                   usb_callback_info.bulkbufferlength);
            usb_databufferindex += usb_callback_info.bulkbufferlength;
            SCTUSB_StatusResidue -= usb_callback_info.bulkbufferlength;

            usb_databufferlength = usb_databufferindex;
            //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
            //all data received, now process it
            usb_outbuffercomplete_action();
            //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

            SetReadyEPOUT_NoDataPending();
        }
        else
        {
            //never happen
        }
        break;
    default:
        usb_stall_all_ep(pdev);
        usb_set_csw(pdev,S_USB_FLOWCONTROL,FALSE);
        break;
    }
}

//------------------------------------------------------------------------------
// Handle callback of EP3 IN Bulk
// Inputs:  void *pdev (not used in MB103V)
//          u8  epnum (not used in MB103V)
// Engineer: Quyen Leba
//------------------------------------------------------------------------------
void usb_callback_ep3in(void *pdev, u8 epnum)
{
    if (usb_altbuffer.control.busy)
    {
        return;
    }
    usb_altbuffer.control.busy = 1;

    switch(usb_callback_info.flowcontrol_alt)
    {
    case CB_FC_DATA_IN:
        if (usb_altbuffer.databufferlength[usb_altbuffer.current_buffer_index] <= USB_ALTDATABUFFER_MAXSIZE &&
            usb_altbuffer.databufferlength != 0)
        {
            if ((usb_altbuffer.databufferindex[usb_altbuffer.current_buffer_index] + BULK_MAX_PACKET_SIZE) < usb_altbuffer.databufferlength[usb_altbuffer.current_buffer_index])
            {
                SendDataEPIN_ALT(pdev,
                                 &usb_altbuffer.databuffer[usb_altbuffer.current_buffer_index][usb_altbuffer.databufferindex[usb_altbuffer.current_buffer_index]],
                                 BULK_MAX_PACKET_SIZE);
                usb_altbuffer.databufferindex[usb_altbuffer.current_buffer_index] += BULK_MAX_PACKET_SIZE;
                //SCTUSB_StatusResidue -= BULK_MAX_PACKET_SIZE;
                SetReadyEPIN_ALT();
            }
            else if (usb_altbuffer.databufferindex[usb_altbuffer.current_buffer_index] < usb_altbuffer.databufferlength[usb_altbuffer.current_buffer_index])
            {
                u32 remaininglength;
                
                remaininglength = usb_altbuffer.databufferlength[usb_altbuffer.current_buffer_index] - usb_altbuffer.databufferindex[usb_altbuffer.current_buffer_index];
                //send the last chunk of data
                SendDataEPIN_ALT(pdev,
                                 &usb_altbuffer.databuffer[usb_altbuffer.current_buffer_index][usb_altbuffer.databufferindex[usb_altbuffer.current_buffer_index]],
                                 remaininglength);
                usb_altbuffer.databufferindex[usb_altbuffer.current_buffer_index] = 0;
                //SCTUSB_StatusResidue -= remaininglength;
//                SCTUSB_SetFlowControlAlt(CB_FC_DATA_IN_LAST);
                SCTUSB_SetFlowControlAlt(CB_FC_IDLE);
                SetReadyEPIN_ALT();
            }
            else
            {
                //this is error
                //enable the Endpoint to recive the next cmd
                SetReadyEPOUT_NoDataPending();
                usb_altbuffer.databufferindex[usb_altbuffer.current_buffer_index] = 0;
                usb_altbuffer.databufferlength[usb_altbuffer.current_buffer_index] = 0;
                SCTUSB_SetFlowControlAlt(CB_FC_IDLE);
            }
        }
        else
        {
            //this is error
            //enable the Endpoint to recive the next cmd
            SetReadyEPOUT_NoDataPending();
            usb_altbuffer.databufferindex[usb_altbuffer.current_buffer_index] = 0;
            usb_altbuffer.databufferlength[usb_altbuffer.current_buffer_index] = 0;
            SCTUSB_SetFlowControlAlt(CB_FC_IDLE);
        }
        break;
    case CB_FC_STATUS_SEND:
    case CB_FC_ERROR:
        //enable the Endpoint to recive the next cmd
        SetReadyEPOUT_NoDataPending();
        usb_altbuffer.databufferindex[usb_altbuffer.current_buffer_index] = 0;
        usb_altbuffer.databufferlength[usb_altbuffer.current_buffer_index] = 0;
        SCTUSB_SetFlowControlAlt(CB_FC_IDLE);
        break;
    case CB_FC_DATA_IN_LAST:
        //-Q-usb_set_csw(pdev,SCTUSB_GetCmdStatus(),TRUE);
        SetReadyEPOUT_NoDataPending();
        usb_altbuffer.databufferindex[usb_altbuffer.current_buffer_index] = 0;
        usb_altbuffer.databufferlength[usb_altbuffer.current_buffer_index] = 0;

//        if (usb_altbuffer.control.updated)
//        {
//            usb_altbuffer.control.updated = 0;
//            usb_altbuffer.current_buffer_index++;
//            usb_altbuffer.current_buffer_index &= 0x01;
//        }
        
        SCTUSB_SetFlowControlAlt(CB_FC_IDLE);
        break;
    default:
        break;
    }
    usb_altbuffer.control.busy = 0;
}

//------------------------------------------------------------------------------
// Handle callback of EP3 IN
// Engineer: Quyen Leba
//------------------------------------------------------------------------------
//void usb_callback_ep3in_bulk(void *pdev, u8 epnum)
//{
//    s32 length;
//
//    if (usb_altbuffer.control.updated)
//    {
//        usb_altbuffer.control.updated = 0;
//        usb_altbuffer.current_buffer_index++;
//        usb_altbuffer.current_buffer_index &= 0x01;
//    }
//    length = usb_altbuffer.bufferlength[usb_altbuffer.current_buffer_index] - usb_altbuffer.bufferindex[usb_altbuffer.current_buffer_index];
//    if (length >= ALT_BULK_MAX_PACKET_SIZE)
//    {
//        SendDataEPIN_ALT(pdev,
//                         &usb_altbuffer.buffer[usb_altbuffer.current_buffer_index][usb_altbuffer.bufferindex[usb_altbuffer.current_buffer_index]],
//                         ALT_BULK_MAX_PACKET_SIZE);
//        usb_altbuffer.current_buffer_index += ALT_BULK_MAX_PACKET_SIZE;
//        SetReadyEPIN_ALT();
//    }
//    else if (length > 0)
//    {
//        SendDataEPIN_ALT(pdev,
//                         &usb_altbuffer.buffer[usb_altbuffer.current_buffer_index][usb_altbuffer.bufferindex[usb_altbuffer.current_buffer_index]],
//                         length);
//        usb_altbuffer.current_buffer_index += length;
//        SetReadyEPIN_ALT();
//    }
//    else
//    {
//        //nothing
//    }
//}

//------------------------------------------------------------------------------
// Handle callback of EP3 IN Iso
// Engineer: Quyen Leba
//------------------------------------------------------------------------------
void usb_callback_ep3in_iso(void *pdev, u8 epnum)
{
//    const u8 dummy[32] = {1,2,3,4,5,6,7,8,9,0};
//    s32 length;
//
//    if (usb_altbuffer.control.updated)
//    {
//        usb_altbuffer.control.updated = 0;
//        usb_altbuffer.current_buffer_index++;
//        usb_altbuffer.current_buffer_index &= 0x01;
//    }
//    //length = usb_altbuffer.bufferlength[usb_altbuffer.current_buffer_index] - usb_altbuffer.bufferindex[usb_altbuffer.current_buffer_index];
//    //force to a fixed length report
////    length = usbcmdhandler_datalog_info.max_data_report_frame_length - usb_altbuffer.bufferindex[usb_altbuffer.current_buffer_index];
//    length = usb_altbuffer.max_data_report_frame_length - usb_altbuffer.bufferindex[usb_altbuffer.current_buffer_index];
//    if (length >= ALT_ISO_MAX_PACKET_SIZE)
//    {
//        SendDataEPIN_ALT_ISO(pdev,
//                             &usb_altbuffer.buffer[usb_altbuffer.current_buffer_index][usb_altbuffer.bufferindex[usb_altbuffer.current_buffer_index]],
//                             ALT_ISO_MAX_PACKET_SIZE);
//        SetReadyEPIN_ALT();
//        usb_altbuffer.bufferindex[usb_altbuffer.current_buffer_index] += ALT_ISO_MAX_PACKET_SIZE;
//    }
//    else if (length > 0)
//    {
//        SendDataEPIN_ALT_ISO(pdev,
//                             &usb_altbuffer.buffer[usb_altbuffer.current_buffer_index][usb_altbuffer.bufferindex[usb_altbuffer.current_buffer_index]],
//                             length);
//        SetReadyEPIN_ALT();
//        usb_altbuffer.bufferindex[usb_altbuffer.current_buffer_index] += length;
//    }
//    else if (usb_altbuffer.max_data_report_frame_length == usb_altbuffer.bufferindex[usb_altbuffer.current_buffer_index] &&
//             usb_altbuffer.bufferindex[usb_altbuffer.current_buffer_index] != 0)
//    {
//        SendDataEPIN_ALT_ISO(pdev,(u8*)dummy,sizeof(dummy));
//    }
////    else
////    {
////    }
}

//------------------------------------------------------------------------------
// Handle callback of EP5 IN Bulk
// Inputs:  void *pdev (not used in MB103V)
//          u8  epnum (not used in MB103V)
// Engineer: Quyen Leba
//------------------------------------------------------------------------------
void usb_callback_ep5in(void *pdev, u8 epnum)
{
    switch(usb_callback_info.flowcontrol_vircom)
    {
    case CB_FC_DATA_IN:
        usb_vircombuffer.control.inxferbusy = 1;
        if (usb_vircombuffer.databufferlength <= USB_VIRCOM_DATABUFFER_MAXSIZE &&
            usb_vircombuffer.databufferlength != 0)
        {
            if ((usb_vircombuffer.databufferindex + VIRCOM_BULK_MAX_PACKET_SIZE) < usb_vircombuffer.databufferlength)
            {
                SendDataEPIN_VirCOM(pdev,&usb_vircombuffer.databuffer[usb_vircombuffer.databufferindex],VIRCOM_BULK_MAX_PACKET_SIZE);
                usb_vircombuffer.databufferindex += VIRCOM_BULK_MAX_PACKET_SIZE;
                SetReadyEPIN_VirCOM();
            }
            else if (usb_vircombuffer.databufferindex < usb_vircombuffer.databufferlength)
            {
                u32 remaininglength;
                
                remaininglength = usb_vircombuffer.databufferlength - usb_vircombuffer.databufferindex;
                //send the last chunk of data
                SendDataEPIN_VirCOM(pdev,&usb_vircombuffer.databuffer[usb_vircombuffer.databufferindex],remaininglength);
                usb_vircombuffer.databufferindex = 0;
                SCTUSB_SetFlowControlVirCOM(CB_FC_DATA_IN_LAST);
                SetReadyEPIN_VirCOM();
            }
            else
            {
                //this is error
                usb_vircombuffer.control.inxferbusy = 0;
                SCTUSB_SetFlowControlVirCOM(CB_FC_IDLE);
            }
        }
        else
        {
            //this is error
            usb_vircombuffer.control.inxferbusy = 0;
            SCTUSB_SetFlowControlVirCOM(CB_FC_IDLE);
        }
        break;
    case CB_FC_STATUS_SEND:
    case CB_FC_ERROR:
        usb_vircombuffer.control.inxferbusy = 0;
        SCTUSB_SetFlowControlVirCOM(CB_FC_IDLE);
        break;
    case CB_FC_DATA_IN_LAST:
        usb_vircombuffer.control.inxferbusy = 0;
        break;
    default:
        break;
    }
}

//------------------------------------------------------------------------------
// Push/prepare vircom data to deliver through usb
// Inputs:  u8  *data
//          u32 datalength
// Engineer: Quyen Leba
//------------------------------------------------------------------------------
void usb_push_vircom_data(u8 *data, u32 datalength)
{
    if (usb_vircombuffer.control.inxferbusy)
    {
        return;
    }
    else if (datalength > sizeof(usb_vircombuffer.databuffer))
    {
        return;
    }

    usb_vircombuffer.control.inxferbusy = 1;

    memcpy(usb_vircombuffer.databuffer,data,datalength);
    usb_vircombuffer.databufferlength = datalength;
    usb_vircombuffer.databufferindex = 0;
    
    usb_trigger_send_vircomdatabuffer_to_host();
    while(usb_vircombuffer.control.inxferbusy != 0)
    {
        //TODOQ: add timeout
    }
}

//------------------------------------------------------------------------------
// A trigger to send vircom data buffer to HOST
// Engineer: Quyen Leba
//------------------------------------------------------------------------------
void usb_trigger_send_vircomdatabuffer_to_host()
{
    if (usb_vircombuffer.databufferlength > VIRCOM_BULK_MAX_PACKET_SIZE)
    {
        SCTUSB_SetFlowControlVirCOM(CB_FC_DATA_IN);
        usb_vircombuffer.databufferindex += VIRCOM_BULK_MAX_PACKET_SIZE;
        SendDataEPIN_VirCOM(usb_callback_info.pdev,
                            usb_vircombuffer.databuffer,
                            VIRCOM_BULK_MAX_PACKET_SIZE);
    }
    else
    {
        SCTUSB_SetFlowControlVirCOM(CB_FC_DATA_IN_LAST);
        usb_vircombuffer.databufferindex += usb_vircombuffer.databufferlength;
        SendDataEPIN_VirCOM(usb_callback_info.pdev,
                            usb_vircombuffer.databuffer,
                            usb_vircombuffer.databufferlength);
    }
    SetReadyEPIN_VirCOM();
}

//------------------------------------------------------------------------------
// Clear any data in vircom data buffer
// Engineer: Quyen Leba
//------------------------------------------------------------------------------
void usb_clear_vircom_data()
{
    memset((char*)&usb_vircombuffer,0,sizeof(usb_vircombuffer));
}

//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
void usb_cbw_decode(void *pdev)
{
    u8  status;

    status = S_USB_UNKNOWN;
    SCTUSB_SetCmdStatus(status);
    usb_databufferindex = 0;
    usb_databufferlength = 0;

    if (usb_callback_info.bulkbufferlength != CB_CBW_LENGTH)
    {
        usb_stall_all_ep(pdev);
        // reset the id to disable the clear feature until
        // receiving a SCTUSB_reset
        usb_callback_info.cbw.id = 0;
        usb_set_csw(pdev,S_USB_CBW_SIZE,FALSE);
        return;
    }

    memcpy((char*)&usb_callback_info.cbw,
           (char*)usb_callback_info.bulkbuffer,CB_CBW_LENGTH);
    SCTUSB_SetStatusTag(usb_callback_info.cbw.tag);
    SCTUSB_SetStatusPrivData(0);
    SCTUSB_SetStatusResidue(usb_callback_info.cbw.datalength);
    usb_callback_info.pdev = pdev;

    if (usb_callback_info.cbw.id == CB_CBW_ID)
    {
        if (uch_is_halted() &&
            (UCH_USBCommand)usb_callback_info.cbw.cmd != UCH_USBCommand_ForceUnHalt)
        {
            usb_set_csw(pdev,S_USB_HALTED,TRUE);
            return;
        }

#if USB_SECURITY_ENABLE
        if (SCTUSB_isSecurityLocked())
        {
            bool isAllowed = FALSE;

            switch((UCH_USBCommand)usb_callback_info.cbw.cmd)
            {
            case UCH_USBCommand_Security:
            case UCH_USBCommand_ForceUnHalt:
                isAllowed = TRUE;
                break;
            case UCH_USBCommand_GetSettings:
                switch(usb_callback_info.cbw.flags)
                {
                case USBCmdSettings_FailsafeBootVersion:
                case USBCmdSettings_MainBootVersion:
                case USBCmdSettings_AppVersion:
                case USBCmdSettings_SerialNumber:
                case USBCmdSettings_DevicePartNumberString:
                case USBCmdSettings_DeviceType:
                case USBCmdSettings_MarketType:
                case USBCmdSettings_Properties:
                    isAllowed = TRUE;
                    break;
                default:
                    //do nothing
                    break;
                }
                break;
            //other commands are always blocked
            default:
                //do nothing
                break;
            }
            if (!isAllowed)
            {
                usb_set_csw(pdev,S_USB_SECURITY,TRUE);
                return;
            }
        }
#endif

        if (usb_callback_info.cbw.datalength > usb_databuffermaxlength)
        {
            usb_set_csw(pdev,S_USB_CBW_DATALENGTH,TRUE);
            return;
        }

        uch_ui_lock_set();
        switch((UCH_USBCommand)usb_callback_info.cbw.cmd)
        {
        //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
        // File transfer
        //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
#if SUPPORT_USBCMD_FILETRANSFER
        case UCH_USBCommand_ReadFile:
            uch_readfile();
            break;
        case UCH_USBCommand_WriteFile:
        case UCH_USBCommand_OpenFile:
            SCTUSB_SetFlowControl(CB_FC_DATA_OUT);
            SetReadyEPOUT_DataPending();
            //handled later; when received all data
            break;
        case UCH_USBCommand_CloseFile:
            status = uch_closefile();
            usb_set_csw(pdev,status,TRUE);
            SetReadyEPOUT_NoDataPending();
            break;
        case UCH_USBCommand_RecoveryStock_Check:
        case UCH_USBCommand_RecoveryStock_Register:
            SCTUSB_SetFlowControl(CB_FC_DATA_OUT);
            SetReadyEPOUT_DataPending();
            //handled later; when received all data
            break;
#endif  //SUPPORT_USBCMD_FILETRANSFER
        //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
        // Security
        //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
        case UCH_USBCommand_ForceUnHalt:
            //give USB ability to force a takeover
            SCTUSB_LockSecurity();
            uch_set_temporary_halt(FALSE);
            usb_set_csw(pdev,S_SUCCESS,TRUE);
            SetReadyEPOUT_NoDataPending();
            break;
        case UCH_USBCommand_Security:
            SCTUSB_LockSecurity();
            switch((USBCmdSecurityFlags)usb_callback_info.cbw.flags)
            {
            case USBCmdSecurity_Seed:
                uch_security_seed();
                break;
            case USBCmdSecurity_Key:
                SCTUSB_SetFlowControl(CB_FC_DATA_OUT);
                SetReadyEPOUT_DataPending();
                //handled later; when received all data
                break;
            case USBCmdSecurity_Lock:
                usb_set_csw(pdev,S_SUCCESS,TRUE);
                SetReadyEPOUT_NoDataPending();
                break;
            default:
                usb_set_csw(pdev,S_USB_UNKNOWN_FLAGS,TRUE);
                SetReadyEPOUT_NoDataPending();
                break;
            }
            break;
        //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
        // Hardware
        //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
#if SUPPORT_USBCMD_HARDWARE
        case UCH_USBCommand_Hardware:
            switch((USBCmdHardwareFlags)usb_callback_info.cbw.flags)
            {
            //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
            case USBCmdHardware_GetDeviceInfoString:
            case USBCmdHardware_GetInfo:
            case USBCmdHardware_FTLInit:
            case USBCmdHardware_FTLInitAdvance:
            case USBCmdHardware_FTLRead:
            case USBCmdHardware_FTLDeinit:
            case USBCmdHardware_FilesystemReinit:
            case USBCmdHardware_FormatMedia:
            case USBCmdHardware_Listing:
            case USBCmdHardware_GetFreeSpace:
            case USBCmdHardware_GetCapacity:
            case USBCmdHardware_GetAnalogData:
            case USBCmdHardware_SetBaudrate:
            case USBCmdHardware_SetVolume:
            case USBCmdHardware_SetBrightness:
            case USBCmdHardware_Unpair:
            case USBCmdHardware_Reset:
            case USBCmdHardware_GetFriendlyName:
            case USBCmdHardware_GetMacAddress:
            case USBCmdHardware_GetCurrentDir:
            case USBCmdHardware_GetCurrentDrive:
            case USBCmdHardware_LEDSet:
            case USBCmdHardware_LEDRelease:
                //commands without incoming data
                uch_hardware();
                break;
            //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
            case USBCmdHardware_FTLWrite:
            case USBCmdHardware_GetFileSize:
            case USBCmdHardware_DeleteFile:
            case USBCmdHardware_RenameFile:
            case USBCmdHardware_MoveFile:
            case USBCmdHardware_TruncateFile:
            case USBCmdHardware_CopyFile:
            case USBCmdHardware_VerifyFile:
            case USBCmdHardware_CreateDir:
            case USBCmdHardware_RemoveDir:
            case USBCmdHardware_ChangeDir:
            case USBCmdHardware_ChangeDrive:
            case USBCmdHardware_ShowMessage:
            case USBCmdHardware_SetFriendlyName:
            case USBCmdHardware_SetMacAddress:
            case USBCmdHardware_ValidateLookupHeader:
            case USBCmdHardware_ListingSetFolder:
                //commands have incoming data
                SCTUSB_SetFlowControl(CB_FC_DATA_OUT);
                SetReadyEPOUT_DataPending();
                //handled later; when received all data
                break;
            default:
                usb_set_csw(pdev,S_USB_UNKNOWN_FLAGS,TRUE);
                SetReadyEPOUT_NoDataPending();
                break;
            }
            break;
#endif  //SUPPORT_USBCMD_HARDWARE
        //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
        // Passthrough Datalog
        //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
#if SUPPORT_USBCMD_DATALOG
        case UCH_USBCommand_Datalog:
            switch((USBCmdDatalogFlags)usb_callback_info.cbw.flags)
            {
            //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
            case USBCmdDatalog_GetVehicleInfo:
            case USBCmdDatalog_GetTestResultDLX:
            case USBCmdDatalog_GetVehicleCommInfo:
            case USBCmdDatalog_GetFeatures:
            case USBCmdDatalog_GetStatus:
            case USBCmdDatalog_GetPacket:
            case USBCmdDatalog_Stop:
            case USBCmdDatalog_SetDataReportInterval:
            case USBCmdDatalog_Execute:
            case USBCmdDatalog_DTC:
                //commands without incoming data
                uch_datalog();
                break;
            //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
            case USBCmdDatalog_SetVehicleComm:
            case USBCmdDatalog_StartTestDLX:            
            case USBCmdDatalog_ScanDLX:
            case USBCmdDatalog_ScanSingle:
            case USBCmdDatalog_Start:
            case USBCmdDatalog_SendSignalOSC:
                //commands have incoming data
                SCTUSB_SetFlowControl(CB_FC_DATA_OUT);
                SetReadyEPOUT_DataPending();
                break;
            //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
            default:
                usb_set_csw(pdev,S_USB_UNKNOWN_FLAGS,TRUE);
                SetReadyEPOUT_NoDataPending();
                break;
            }
            break;
#endif
        //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
        // Passthrough Tune
        //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
#if SUPPORT_USBCMD_TUNE
        case UCH_USBCommand_Tune:
            switch((USBCmdTuneFlags)usb_callback_info.cbw.flags)
            {
            //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
            case USBCmdTune_Init:
            case USBCmdTune_DeInit:
            case USBCmdTune_GetStragyFlasher:
            case USBCmdTune_GetVehicleInfo:
            case USBCmdTune_InitUpload:
            case USBCmdTune_DoUpload:
            case USBCmdTune_InitDownload:
            case USBCmdTune_DoDownload:
//            case USBCmdTune_InitReturnStock:
//            case USBCmdTune_DoReturnStock:
            case USBCmdTune_KeyConfirm:
            case USBCmdTune_MarriedCountWarnConfirm:
            case USBCmdTune_MarriedCountWarnCancel:
            case USBCmdTune_SearchVehicleType:
            case USBCmdTune_GetUploadStock:
                //commands without incoming data
                uch_tune();
                break;
            //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
            case USBCmdTune_InitFlasher:
            case USBCmdTune_SetCustomTuneInfo:
            case USBCmdTune_HealthCheckDownload:
            case USBCmdTune_CompileUserVehicleType:
                //commands have incoming data
                SCTUSB_SetFlowControl(CB_FC_DATA_OUT);
                SetReadyEPOUT_DataPending();
                break;
            //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
            default:
                usb_set_csw(pdev,S_USB_UNKNOWN_FLAGS,TRUE);
                SetReadyEPOUT_NoDataPending();
                break;
            }
            break;
#endif
        //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
        // Settings
        //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
        case UCH_USBCommand_GetSettings:
            uch_settings_get();
            break;
        case UCH_USBCommand_SetSettings:
            SCTUSB_SetFlowControl(CB_FC_DATA_OUT);
            SetReadyEPOUT_DataPending();
            //handled later; when received all data
            break;
        //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
        // Bootloader
        //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
        case UCH_USBCommand_Bootloader:
            switch((USBCmdBootloaderFlags)usb_callback_info.cbw.flags)
            {
            case USBCmdBootloader_SetBoot:
            case USBCmdBootloader_Init:
            case USBCmdBootloader_Do:
            case 0x3F:
                SCTUSB_SetFlowControl(CB_FC_DATA_OUT);
                SetReadyEPOUT_DataPending();
                //handled later; when received all data
                break;
            case USBCmdBootloader_Validate:
                uch_bootloader();
                break;
            default:
                usb_set_csw(pdev,S_USB_UNKNOWN_FLAGS,TRUE);
                SetReadyEPOUT_NoDataPending();
                break;
            }
            break;
        //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
        //
        //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
        default:
            usb_set_csw(pdev,S_USB_UNKNOWN_CMD,TRUE);
            SetReadyEPOUT_NoDataPending();
            break;
        }//switch((UCH_USBCommand)...
    }
    else
    {
        // invalid CBW
        usb_stall_all_ep(pdev);
        usb_set_csw(pdev,S_USB_CSW_ID,FALSE);
    }
}

//------------------------------------------------------------------------------
// Action when OUT buffer completely received
// Engineer: Quyen Leba
//------------------------------------------------------------------------------
void usb_outbuffercomplete_action()
{
    switch((UCH_USBCommand)SCTUSB_GetRequestedCmd())
    {
#if SUPPORT_USBCMD_FILETRANSFER
    case UCH_USBCommand_WriteFile:
        uch_writefile();
        break;
    case UCH_USBCommand_CheckFile:
        uch_checkfile();
        break;
    case UCH_USBCommand_OpenFile:
        uch_openfile();
        break;
    case UCH_USBCommand_RecoveryStock_Check:
        uch_recoverystock_check();
        break;
    case UCH_USBCommand_RecoveryStock_Register:
        uch_recoverystock_register();
        break;
#endif  //SUPPORT_USBCMD_FILETRANSFER
    case UCH_USBCommand_Security:
        uch_security_key();
        break;
#if SUPPORT_USBCMD_HARDWARE
    case UCH_USBCommand_Hardware:
        uch_hardware();
        break;
#endif  //SUPPORT_USBCMD_HARDWARE
#if SUPPORT_USBCMD_DATALOG
    case UCH_USBCommand_Datalog:
        uch_datalog();
        break;
#endif  //SUPPORT_USBCMD_DATALOG
#if SUPPORT_USBCMD_TUNE
    case UCH_USBCommand_Tune:
        uch_tune();
        break;
#endif  //SUPPORT_USBCMD_DATALOG
    case UCH_USBCommand_SetSettings:
        uch_settings_set();
        break;
    case UCH_USBCommand_Bootloader:
        uch_bootloader();
        break;
    }
}

//------------------------------------------------------------------------------
// Push/prepare alt data to deliver through usb
// Inputs:  u8  *data
//          u32 datalength
// Engineer: Quyen Leba
//------------------------------------------------------------------------------
void usb_push_alt_data(u8 *data, u32 datalength)
{
    u32 length;
//    if (usb_altbuffer.current_buffer_index == 0)
//    {
//        memset(usb_altbuffer.buffer[1],0xBB,sizeof(usb_altbuffer.buffer[1]));
//        memcpy(usb_altbuffer.buffer[1],data,datalength);
//        usb_altbuffer.bufferindex[1] = 0;
//        usb_altbuffer.bufferlength[1] = datalength;
//    }
//    else
//    {
//        memset(usb_altbuffer.buffer[0],0xAA,sizeof(usb_altbuffer.buffer[0]));
//        memcpy(usb_altbuffer.buffer[0],data,datalength);
//        usb_altbuffer.bufferindex[0] = 0;
//        usb_altbuffer.bufferlength[0] = datalength;
//    }
//    usb_altbuffer.control.updated = 1;

//    if (usb_altbuffer.databufferindex == 0 ||
//        usb_altbuffer.databufferindex >= usb_altbuffer.databufferlength)
//    {
//        memcpy(usb_altbuffer.databuffer,data,datalength);
//        usb_altbuffer.databufferlength = (datalength+ALT_BULK_MAX_PACKET_SIZE-1)/ALT_BULK_MAX_PACKET_SIZE*ALT_BULK_MAX_PACKET_SIZE;
//        usb_altbuffer.databufferindex = 0;
//        //usb_altbuffer.control.updated = 1;    //TODOQ
//    }

//    if (usb_altbuffer.current_buffer_index == 0)
//    {
//        memcpy(usb_altbuffer.databuffer[1],data,datalength);
//        usb_altbuffer.databufferlength[1] = (datalength+ALT_BULK_MAX_PACKET_SIZE-1)/ALT_BULK_MAX_PACKET_SIZE*ALT_BULK_MAX_PACKET_SIZE;
//        usb_altbuffer.databufferindex[1] = 0;
//    }
//    else
//    {
//        memcpy(usb_altbuffer.databuffer[0],data,datalength);
//        usb_altbuffer.databufferlength[0] = (datalength+ALT_BULK_MAX_PACKET_SIZE-1)/ALT_BULK_MAX_PACKET_SIZE*ALT_BULK_MAX_PACKET_SIZE;
//        usb_altbuffer.databufferindex[0] = 0;
//    }
//    usb_altbuffer.control.updated = 1;


    if (usb_altbuffer.control.busy)
    {
        return;
    }

    if (usb_altbuffer.databufferindex[usb_altbuffer.current_buffer_index] == 0 ||
        usb_altbuffer.databufferindex[usb_altbuffer.current_buffer_index] >= usb_altbuffer.databufferlength[usb_altbuffer.current_buffer_index])
    {
        usb_altbuffer.control.busy = 1;

        length = (datalength+ALT_BULK_MAX_PACKET_SIZE-1)/ALT_BULK_MAX_PACKET_SIZE*ALT_BULK_MAX_PACKET_SIZE;
        memset(usb_altbuffer.databuffer[usb_altbuffer.current_buffer_index],
               0xEE,length);
        memcpy(usb_altbuffer.databuffer[usb_altbuffer.current_buffer_index],data,datalength);
        usb_altbuffer.databufferlength[usb_altbuffer.current_buffer_index] = length;
        usb_altbuffer.databufferindex[usb_altbuffer.current_buffer_index] = 0;

        usb_altbuffer.control.busy = 0;
        usb_trigger_send_altdatabuffer_to_host();
    }
}

//------------------------------------------------------------------------------
// A trigger to send usb_databuffer to HOST
// Engineer: Quyen Leba
//------------------------------------------------------------------------------
void usb_trigger_send_altdatabuffer_to_host()
{
//    if (SCTUSB_GetFlowControlAlt() == CB_FC_IDLE)
//    {
//        if (usb_altbuffer.control.updated)
//        {
//            usb_altbuffer.current_buffer_index++;
//            usb_altbuffer.current_buffer_index &= 0x01;
//            usb_altbuffer.control.updated = 0;
//        }
//    }
//    else
//    {
//        return;
//    }

    if (usb_altbuffer.databufferlength[usb_altbuffer.current_buffer_index] > BULK_MAX_PACKET_SIZE)
    {
        SCTUSB_SetFlowControlAlt(CB_FC_DATA_IN);
        usb_altbuffer.databufferindex[usb_altbuffer.current_buffer_index] += BULK_MAX_PACKET_SIZE;
        //SCTUSB_StatusResidue -= BULK_MAX_PACKET_SIZE;
        SendDataEPIN_ALT(usb_callback_info.pdev,
                         usb_altbuffer.databuffer[usb_altbuffer.current_buffer_index],
                         BULK_MAX_PACKET_SIZE);
    }
    else
    {
        SCTUSB_SetFlowControlAlt(CB_FC_DATA_IN_LAST);
        usb_altbuffer.databufferindex[usb_altbuffer.current_buffer_index] += usb_altbuffer.databufferlength[usb_altbuffer.current_buffer_index];
        //SCTUSB_StatusResidue -= usb_databufferlength;
        SendDataEPIN_ALT(usb_callback_info.pdev,
                         usb_altbuffer.databuffer[usb_altbuffer.current_buffer_index],
                         usb_altbuffer.databufferlength[usb_altbuffer.current_buffer_index]);
    }
    SetReadyEPIN_ALT();
}

//------------------------------------------------------------------------------
// Clear any data in alt data buffer
// Engineer: Quyen Leba
//------------------------------------------------------------------------------
void usb_clear_alt_data()
{
    memset((char*)&usb_altbuffer,0,sizeof(usb_altbuffer));
}

//------------------------------------------------------------------------------
// Clear any data in alt data buffer
// Engineer: Quyen Leba
//------------------------------------------------------------------------------
void usb_set_alt_data_max_frame_length(u16 length)
{
    usb_altbuffer.max_data_report_frame_length = length;
}

//------------------------------------------------------------------------------
// A hook task when USB connection is established
// Engineer: Quyen Leba
//------------------------------------------------------------------------------
void usb_connection_hook()
{
    usb_clear_alt_data();
    uch_set_usb_ready(TRUE);
}

//------------------------------------------------------------------------------
// A hook task when USB connection is removed
// Engineer: Quyen Leba
//------------------------------------------------------------------------------
void usb_disconnection_hook()
{
    usb_clear_alt_data();
    uch_set_usb_ready(FALSE);
    uch_ui_lock_remove();
}

