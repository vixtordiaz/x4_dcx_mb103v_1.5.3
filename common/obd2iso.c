/**
  **************** (C) COPYRIGHT 2011 SCT Performance, LLC *********************
  * File Name          : obd2iso.c
  * Device / System    : SCT products using the "CommonCode" platform
  * Author             : Rhonda Rusak
  *
  * Version            : 1 
  * Date               : 08/11/2014
  * Description        : 
  *                    : 
  *
  ******************************************************************************
  */

#include <board/delays.h>
#include <board/rtc.h>
#include <board/iso.h>
#include <common/statuscode.h>
#include <common/log.h>
#include <string.h>
#include "obd2iso.h"


//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
extern u8 obd2_rxbuffer[256];           /* from obd2.c */
extern obd2_info gObd2info;             /* from obd2.c */

static const u8 KLINE_FUNCTIONAL_HEADER[3] = {0x68, 0x6A, 0xF1};
static const u8 KWP2000_FUNCTIONAL_HEADER[3] = {0xC1, 0x33, 0xF1};

//------------------------------------------------------------------------------
// ISO txrx fucntion
// Input:   Obd2iso_ServiceData *servicedata
// Return:  u8  status
// Engineer: Ricardo Cigarroa
//------------------------------------------------------------------------------
u8 obd2iso_txrx(Obd2iso_ServiceData *servicedata)
{
    u8 status; 
    
    status = obd2iso_tx(servicedata);
                          
    if(status == S_SUCCESS && servicedata->response_expected == TRUE)
    {
        status = obd2iso_rx(servicedata);
        if(status != S_SUCCESS)
        {
            //error point 
            status = S_FAIL; 
        }
    }
    else
    {
        status = S_FAIL; 
    }
    
    return status; 
}

//------------------------------------------------------------------------------
// ISO tx fucntion
// Input:   Obd2iso_ServiceData *servicedata
// Return:  u8  status
// Engineer: Ricardo Cigarroa
//------------------------------------------------------------------------------
u8 obd2iso_tx(Obd2iso_ServiceData *servicedata)
{
    u8 status;
    u8 txframe[5];
    
    switch(servicedata->vehiclecommtype)
    {
        case CommType_KLINE:
             memcpy(txframe, KLINE_FUNCTIONAL_HEADER, 
                    sizeof(KLINE_FUNCTIONAL_HEADER)); 
             break;
        case CommType_KWP2000:
             memcpy(txframe, KWP2000_FUNCTIONAL_HEADER, 
                    sizeof(KWP2000_FUNCTIONAL_HEADER)); 
             break;
        default:
             return S_INPUT; 
    }
    
    /*Some KWP2000 commands require a different format byte (0xC2)*/
    txframe[0] = servicedata->format_byte; 
    
    txframe[3] = servicedata->service; 
    txframe[4] = servicedata->subservice;
    
    status = iso_tx(txframe,servicedata->txlength,
                      servicedata->txflags,
                      servicedata->rxflags);
    
    return status; 
}

//------------------------------------------------------------------------------
// ISO rx fucntion
// Input:   Obd2iso_ServiceData *servicedata
// Return:  u8  status
// Engineer: Ricardo Cigarroa
//------------------------------------------------------------------------------   
u8 obd2iso_rx(Obd2iso_ServiceData *servicedata)
{
    u8 status; 
    
    status = iso_rx(servicedata->rxbuffer,
                      &servicedata->rxlength,
                      servicedata->opcode_length,
                      servicedata->rxflags,
                      servicedata->rx_timeout_ms);
    if(status == S_SUCCESS)
    {
        /*K-LINE response*/
        if(servicedata->rxbuffer[0] == 0x48 && servicedata->rxbuffer[1] == 0x6B && 
           servicedata->rxbuffer[3] == servicedata->service + 0x40)
        {
            servicedata->rxlength -= 4; 
            memcpy((char*)servicedata->rxbuffer,(char*)&servicedata->rxbuffer[4],
                   servicedata->rxlength); 
        }
        /*KWP2000 response*/
        else if(servicedata->rxbuffer[1] == 0xF1 
                && servicedata->rxbuffer[3] == servicedata->service + 0x40)
        {
            servicedata->rxlength -= 4; 
            memcpy((char*)servicedata->rxbuffer,(char*)&servicedata->rxbuffer[4],
                   servicedata->rxlength); 
        }
        else
        {
            status = S_FAIL;
            servicedata->rxlength = 0; 
        }
    }
    else
    {
        status = S_FAIL; 
        servicedata->rxlength = 0; 
    }
    
    return status; 
}

//------------------------------------------------------------------------------
// Initializes ISO servicedata.
// Input:   Obd2iso_ServiceData *servicedata
// Return:  void
// Engineer: Ricardo Cigarroa
//------------------------------------------------------------------------------
void obd2iso_servicedata_init(Obd2iso_ServiceData *servicedata)
{     
    servicedata->service = 0x00; 
    servicedata->subservice = 0x00; 
    servicedata->response_expected = TRUE; 
    servicedata->rxbuffer = obd2_rxbuffer; 
    servicedata->rxlength = 0;
    servicedata->txbuffer = NULL; 
    servicedata->txlength = 0;
    servicedata->txflags = 4;
    servicedata->rxflags = 4;
    servicedata->format_byte = 0x68; /* K-Line = 0x68, KWP2000 = 0xC1 or 0xC2 */
    servicedata->vehiclecommtype = CommType_KLINE;
    servicedata->rx_timeout_ms = 1000;
    //11 bytes is the max number of bytes that can be sent in 1 frame. 
    servicedata->opcode_length = 11;
}

//------------------------------------------------------------------------------
// ISO ping fucntion
// Input:   void
// Return:  u8  status
// Engineer: Ricardo Cigarroa
//------------------------------------------------------------------------------
u8 obd2iso_ping(VehicleCommType commtype)
{  
    u8  status;
    Obd2iso_ServiceData servicedata;
    
    obd2iso_servicedata_init(&servicedata);
    servicedata.vehiclecommtype = commtype;
    servicedata.service = 0x01;
    servicedata.txlength = 5;
    
    if (commtype == CommType_KWP2000)
    {
        servicedata.format_byte = 0xC2;
    }
    
    status = obd2iso_txrx(&servicedata);
    if (status != S_SUCCESS)
    {
        return status;
    }
    
    return S_SUCCESS;
}

//------------------------------------------------------------------------------
// Clear DTCs using standard service (04 from J1979)
// Input:   void
// Return:  u8  status
// Engineer: Ricardo Cigarroa
//------------------------------------------------------------------------------
u8 obd2iso_cleardtc(VehicleCommType commtype)
{
    u8  status;
    Obd2iso_ServiceData servicedata;
    
    obd2iso_servicedata_init(&servicedata);
    servicedata.vehiclecommtype = commtype;
    servicedata.service = 0x04;
    servicedata.txlength = 4; 
    
    status = iso_init(&commtype);
    if(status == S_SUCCESS)
    {    
        //set format byte
        if (commtype == CommType_KWP2000)
        {
            servicedata.format_byte = 0xC1;
        }
    
        //set comm type
        servicedata.vehiclecommtype = commtype;
        
        //send request
        status = obd2iso_txrx(&servicedata); 
        if(status != S_SUCCESS)
        {
            status = S_FAIL;
        }
    }
    else
    {
      status = S_FAIL;
    }
    
    return status;
}

//------------------------------------------------------------------------------
// Read DTCs using standard service (01 from J1979)
// Input:   void
// Return:  u8  status
// Engineer: Ricardo Cigarroa
//------------------------------------------------------------------------------
u8 obd2iso_readdtc(dtc_info *info, VehicleCommType commtype)
{
    u8  status = S_SUCCESS;
    u8  dtccount; 
    u8  i;
    u8  codeindex;
    s8  dtc_lines = 0;
    
    Obd2iso_ServiceData servicedata;
    
    obd2iso_servicedata_init(&servicedata);
    servicedata.vehiclecommtype = commtype;
    servicedata.service = 0x01;
    servicedata.subservice = 0x01; 
    servicedata.txlength = 5;
    dtccount = 0; 
    codeindex = 0; 
    
    status = iso_init(&commtype);
    if(status == S_SUCCESS)
    {   
        //set format byte
        if (commtype == CommType_KWP2000)
        {
            servicedata.format_byte = 0xC2;
        }
    
        //set comm type
        servicedata.vehiclecommtype = commtype;
        
        //send request
        status = obd2iso_txrx(&servicedata); 
        if(status != S_SUCCESS)
        {
            status = S_FAIL;
        }
        else
        {
            dtccount = servicedata.rxbuffer[1] & 0x7F; 
            info->count = dtccount; 
            
            if(dtccount > 3)
            {
                dtc_lines = dtccount/3;
                if((dtccount % 3) != 0)
                {
                    dtc_lines++; 
                }
            }
            else
            {
                dtc_lines = 1; 
            }
            
            if(dtccount)
            {
                servicedata.service = 0x03;
                servicedata.subservice = 0x00; 
                servicedata.txlength = 4;
                if (commtype == CommType_KWP2000)
                {
                    servicedata.format_byte = 0xC1;
                }
                
                status = obd2iso_tx(&servicedata);
                if(status == S_SUCCESS)
                {
                    for(i=0;i<dtc_lines;i++)
                    {
                        status = obd2iso_rx(&servicedata);
                        if(status != S_SUCCESS)
                        {
                            //error point 
                            status = S_FAIL; 
                        }
                        
                        if(servicedata.rxbuffer[0] != 0 && servicedata.rxbuffer[1] != 0)
                        {
                            info->codes[codeindex] = servicedata.rxbuffer[0] << 8;
                            info->codes[codeindex] |= servicedata.rxbuffer[1];
                            codeindex++; 
                        }
                        if(servicedata.rxbuffer[2] != 0 && servicedata.rxbuffer[3] != 0)
                        {
                            info->codes[codeindex] = servicedata.rxbuffer[2] << 8;
                            info->codes[codeindex] |= servicedata.rxbuffer[3];
                            codeindex++;
                        }
                        if(servicedata.rxbuffer[4] != 0 && servicedata.rxbuffer[5] != 0)
                        {
                            info->codes[codeindex] = servicedata.rxbuffer[4] << 8;
                            info->codes[codeindex] |= servicedata.rxbuffer[5];
                            codeindex++; 
                        }                   
                    }
                }
                else
                {
                    status = S_FAIL; 
                }                
            } 
        }
    }
    else
    {
      status = S_FAIL;
    }
     
    return status; 
}

//------------------------------------------------------------------------------
// Read Data by PID (by SAE PID)
// Inputs:  u8  pid
//          u8  pidsize
//          u8  *data
// Return:  u8  status
// Engineer: Ricardo Cigarroa
//------------------------------------------------------------------------------
u8 obd2iso_read_data_bypid_mode1(u8 pid, u8 pidsize, u8 *data, 
                                 VehicleCommType commtype)
{
    u8  status;
    Obd2iso_ServiceData servicedata;
    
    obd2iso_servicedata_init(&servicedata);
    servicedata.vehiclecommtype = commtype;
    servicedata.service = 0x01;
    servicedata.subservice = pid; 
    servicedata.txlength = 5;
    
    if (commtype == CommType_KWP2000)
    {
        servicedata.format_byte = 0xC2;
    }
        
    //This delay is required to slow down the device's transmission speed when
    //datalogging. 
    delays(30,'m');

    status = obd2iso_txrx(&servicedata); 
    if(status != S_SUCCESS)
    {
        status = S_FAIL;
    }
    else if(servicedata.rxbuffer[0] == pid)
    {
        memcpy((char*)data,(char*)&servicedata.rxbuffer[1],pidsize);
        status = S_SUCCESS; 
    }
    else
    {
        status = S_FAIL; 
    }
    
    return status;
}

//------------------------------------------------------------------------------
// Read VIN
// Inputs:  u8  *vin
//          
// Return:  u8  status
// Engineer: Tristen Pierson, Ricardo Cigarroa
//------------------------------------------------------------------------------
u8 obd2iso_readvin(u8 *vin, VehicleCommType commtype)
{
    u8  status;
    u8  temp[20];
    u8  i;
    Obd2iso_ServiceData servicedata;
    
    if (!vin)
    {
        return S_INPUT;
    }
    
    memset(vin,0x00,17);
    
    obd2iso_servicedata_init(&servicedata);
    servicedata.vehiclecommtype = commtype;
    servicedata.service = 0x09; //Read Info
    servicedata.subservice = 0x02;  //Info Type: VIN
    servicedata.txlength = 5; 
    
    if (commtype == CommType_KWP2000)
    {
        servicedata.format_byte = 0xC2;
    } 
        
    //check comm type
    status = iso_init(&commtype);
    if(status == S_SUCCESS)
    {
        //set comm type
        servicedata.vehiclecommtype = commtype;
        // Request VIN    
        status = obd2iso_tx(&servicedata);
        
        if (status == S_SUCCESS)
        {
            // Receive VIN in 5 parts
            for (i = 0; i < 5; i++)
            {
                status = obd2iso_rx(&servicedata);
                
                if (servicedata.rxbuffer[1]-1 != i)
                {
                    /* If VIN cannot be read, treat it as blank */
                    status = S_VINBLANK;
                    break;
                }
                memcpy(&temp[i*4],&servicedata.rxbuffer[2],4);
            }
        }
        // Remove fill bytes and validate VIN
        if (status == S_SUCCESS)
        {
            memcpy(vin,&temp[3],17);
            status = obd2_validatevin(vin);
        }
    }
    
    return status;
}

/**
 *  @brief Get CAL ID
 *  
 *  @param [in]  calid      CAL ID Data
 *  @param [in]  commtype   VehicleCommType; passed to lower level
 *
 *  @return status
 */
u8 obd2iso_read_calid(u8 *calid, VehicleCommType commtype)
{
    u8  status;
    u8  temp[CALID_LENGTH+3];
    u8  i, validcount, messagecount;
    Obd2iso_ServiceData servicedata;
    
    if (!calid)
    {
        return S_INPUT;
    }
    
    memset(calid,0,CALID_LENGTH);
    memset(temp,0,sizeof(temp));
    
    obd2iso_servicedata_init(&servicedata);
    servicedata.vehiclecommtype = commtype;
    servicedata.service = 0x09;
    servicedata.txlength = 5; 
    
    if (commtype == CommType_KWP2000)
    {
        servicedata.format_byte = 0xC2;
    } 
    
    status = iso_init(&commtype);
    if(status == S_SUCCESS)
    {        
        servicedata.vehiclecommtype = commtype;
     
        servicedata.subservice = 0x03;
        status = obd2iso_txrx(&servicedata);
    }
    if (status == S_SUCCESS)
    {
        messagecount = servicedata.rxbuffer[1];
        servicedata.subservice = 0x04;
        status = obd2iso_tx(&servicedata);
        if (status == S_SUCCESS)
        {
            // Receive CAL ID
            for (i = 0; i < messagecount; i++)
            {
                status = obd2iso_rx(&servicedata);
                if (servicedata.rxbuffer[1]-1 != i)
                {
                    break;
                }
                memcpy(&temp[i*4],&servicedata.rxbuffer[2],4);
            }
        }
        // Remove fill bytes and validate CAL ID
        if (status == S_SUCCESS)
        {
            for(i=0;i<CALID_LENGTH;i++)
            {
                if (isgraph(temp[3+i]))
                    validcount++;
                else
                    break;
            }
            if (validcount >= 4)
            {
                memcpy(calid,&temp[3],CALID_LENGTH);
                status = S_SUCCESS;
            }
            else
            {
                status = S_STRATEGY;
            }
        }
    }
    
    return status;
}

/**
 *  @brief Read ECM info from OBD2 info
 *  
 *  @param [in]  ecm_info   ECM info structure
 *
 *  @return status
 *  
 *  @details This function reads all available part numbers and serial number.
 */
u8 obd2iso_readecminfo(ecm_info *ecm)
{
    ecm->ecm_count = gObd2info.ecu_count;
    memcpy(ecm->vin, gObd2info.vin, sizeof(ecm->vin));
    
    for(u8 i = 0; i < ECM_MAX_COUNT; i++)
    {
        if (gObd2info.ecu_block[i].ecutype != EcuType_Unknown)
        {
            ecm->commtype[i] = gObd2info.ecu_block[i].commtype;
            ecm->commlevel[i] = gObd2info.ecu_block[i].commlevel;
            if (gObd2info.ecu_block[i].partnumber_count == 1)
            {
                ecm->codecount[i] = gObd2info.ecu_block[i].partnumber_count;
                memcpy(ecm->codes[i],gObd2info.ecu_block[i].partnumbers.generic.strategy,sizeof(ecm->codes[i]));
            }
        }
    }
    return S_SUCCESS;
}

/**
 *  @brief Get ISO OBD2 Info
 *  
 *  @param [in] obd2info OBD2 info structure
 *  @return status
 *  
 *  @details This function identifies and returns if applicable K-LINE/KWP2000
 *           information to the obd2info structure.
 */
u8 obd2iso_getinfo(obd2_info *obd2info, VehicleCommType vehiclecommtype)
{
    u8 vin[VIN_LENGTH+1];
    u8 status;
    
    if (obd2info == NULL)
    {
        return S_INPUT;
    }
    
    status = iso_init(&vehiclecommtype);
    if (status == S_SUCCESS)
    {   
        /* Main OBD2 Info */
        if (obd2info->ecu_count == 0)
        {
            obd2info->oemtype = OemType_Unknown;
            /* Read VIN if primary ECU */
            obd2iso_readvin(vin,vehiclecommtype);
            memcpy(obd2info->vin, vin, sizeof(vin));
        }
        
        /* OBD2 Info Block */
        obd2info->ecu_block[obd2info->ecu_count].ecutype = EcuType_ECM;
        obd2info->ecu_block[obd2info->ecu_count].commtype = vehiclecommtype;
        obd2info->ecu_block[obd2info->ecu_count].commlevel = CommLevel_Unknown;
        obd2info->ecu_block[obd2info->ecu_count].genericcommlevel = GenericCommLevel_Legacy;
        /* block.ecu_id <- N/A */
                
        /* OBD2 Part Numbers <- N/A */
        
        if (obd2iso_read_calid(obd2info->ecu_block[obd2info->ecu_count].partnumbers.generic.strategy, vehiclecommtype) == S_SUCCESS)
        {
            obd2info->ecu_block[obd2info->ecu_count].partnumber_count = 1;
        }
        
        obd2info->ecu_count++;
    }
    return status;
}
