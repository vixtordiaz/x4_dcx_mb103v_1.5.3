/**
  **************** (C) COPYRIGHT 2011 SCT Performance, LLC *********************
  * File Name          : checksum.h
  * Device / System    : SCT products using the "CommonCode" platform
  * Author             : Quyen Leba
  *
  * Version            : 1 
  * Date               : 05/02/2011
  * Description        : 
  *                    : 
  *
  *
  * History            : 05/02/2011 M. Davis
  *                    :   Add SCT Copyright header,
  *
  ******************************************************************************
  */

#ifndef __CHECKSUM_H
#define __CHECKSUM_H

#include <arch/gentype.h>

#define CRC32_FLAGS_NONE        (0)
#define CRC32_FLAGS_MASK_0xFF   (1<<0)

#define CRC_BUFFER_SIZE         1024
#define ADDITIVE_MAX            64
      
typedef union  {
    u32 word;
    u8  byte[4];
}_word;

typedef struct  {
    u32 start_address[ADDITIVE_MAX];
    u32 end_address[ADDITIVE_MAX];
    u32 checksum_address[ADDITIVE_MAX];
    u32 checksum[ADDITIVE_MAX];
    u8 amount;
}_additives;

u32 checksum_simpleadditive(u8 *data, u32 datalength);
u8 checksum_apply_additives(u32 start_address, u32 segment_offset,
                            u32 segment_length, u8 quantity, bool iscomplement,
                            bool ischeckonly);
u8 checksum_locate_additives(u32 *databuf, u32 segment_offset, u32 segment_length, 
                             u8 quantity, _additives *additives);
u8 checksum_calculate_additives(u32 *databuf, u32 segment_offset,
                                u32 segment_start_address, _additives *additives);
u8 checksum_write_additives(u32* databuf, u32 segment_offset, 
                            _additives *additives, bool iscomplementt);
u8 checksum_apply_crc32(u32 start_address, u32 end_address, u32 seed, u32 flags,
                        u32 *CRC_value);
u8 checksum_calc_file_segment_crc32(u32 seed, u32 address, u32 length, 
                                    u32 *CRCvalue, u32 flags);
u8 checksum_calc_file_segment_md5(u32 start_address, u32 end_address, u8 *md5_hash);

#endif    //__CHECKSUM_H
