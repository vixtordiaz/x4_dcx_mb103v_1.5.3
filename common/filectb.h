/**
  **************** (C) COPYRIGHT 2011 SCT Performance, LLC *********************
  * File Name          : filectb.h
  * Device / System    : SCT products using the "CommonCode" platform
  * Author             : Quyen Leba
  *
  * Version            : 1 
  * Date               : 05/02/2011
  * Description        : 
  *                    : 
  *
  *
  * History            : 05/02/2011 M. Davis
  *                    :   Add SCT Copyright header,
  *
  ******************************************************************************
  */

#ifndef __FILECTB_H
#define __FILECTB_H

#include <arch/gentype.h>

#define FILECTB_VERSION             0
typedef void (filectb_progressreport)(u8 percentage, u8 *message);

//15 bytes or less
#define FILECTB_BASETUNE_FILENAME   "ctbbasetune.bin"

typedef struct
{
    u8  filename[32];
    u32 crc32e;     //header_crc32e of the file or CRC32E of entire file (append upto 3 bytes 0xFF) 
    u32 flags;
}CtbFile;
STRUCT_SIZE_CHECK(CtbFile,40);

typedef struct
{
    u16 version;
    u16 vehicleType;
    u32 headerCRC32E;
    u32 contentCRC32E;
    u32 flags;
    u32 marketType;
    u32 signature;
    u32 minSoftwareVersion;
    u32 minFirmwareVersion;
    u32 dealerDongleSerial;
    u8  reserved0[12];
    u8  deviceSerialNumber[16];
    u8  tuneDescription[32];
    CtbFile spfFile[3]; //only has file info, not included in bundle file
    CtbFile sthFile;
    CtbFile ophFile;
    CtbFile csfFile;
    CtbFile tuneosFile;
    CtbFile sofFile;
    CtbFile bootloaderUploadFile[3];
    CtbFile bootloaderDownloadFile[3];
    u8  reserved1[144];
}CtbHeader;
STRUCT_SIZE_CHECK(CtbHeader,800);

u8 filectb_openfile(const u8 *filename);
void filectb_closefile();
u8 filectb_validateheader(CtbHeader *header);

u16 filectb_getVehicleType(bool *isOK);
u8 filectb_getDescription(u8 *description);
u8 filectb_getSPFInfo(u8 index, u8 *filename, u32 *crc32e);
u8 filectb_getSTHInfo(u8 *filename, u32 *crc32e);
u8 filectb_getOPHInfo(u8 *filename, u32 *crc32e);
u8 filectb_getCSFInfo(u8 *filename, u32 *crc32e);
u8 filectb_getTuneOSInfo(u8 *filename, u32 *crc32e);
u8 filectb_getSOFInfo(u8 *filename, u32 *crc32e);
u8 filectb_parseCustomTuneBlock(customtune_block *tuneblock);

u8 filectb_extractAllFiles(filectb_progressreport progressreport_funcptr);
u8 filectb_generateTuneBinary(filectb_progressreport progressreport_funcptr);

#endif     //__FILECTB_H
