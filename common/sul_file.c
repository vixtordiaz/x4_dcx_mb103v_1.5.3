/**
  **************** (C) COPYRIGHT 2012 SCT Performance, LLC *********************
  * File Name          : sul_file.c
  * Device / System    : SCT products using the "CommonCode" platform
  * Author             : Patrick Downs
  *
  * Version            : 1 
  * Date               : 03/20/2012
  * Description        : File format handling functions for Stock Upload File (.SUL)
  *                    : 
  *
  *
  * History            : 03/20/2012 P. Downs
  *                    :   Created
  *
  ******************************************************************************
  */

#include <device_version.h>
#include <common/statuscode.h>
#include <common/crc32.h>
#include <common/crypto_blowfish.h>
#include <common/file.h>
#include <fs/genfs.h>
#include "sul_file.h"
#include "version.h"
#include "settings.h"

#define SULVERSION                  3
//changelog:
// - 3: change ecm_info (MAX_TUNECODE_LENTH increases to 31; from 21)

struct
{
    u8 last_sul_filename[32];
}sul_file_info;

//------------------------------------------------------------------------------
// Creates an SUL file from a file (usualy stock.bin) and flasher info
// Input:   u8  *filename
// Return:  bool status
//------------------------------------------------------------------------------
#define SULTEMPFILE "\\USER\\sultemp.bin"
#define READBUFFERSIZE 4096
u8 sul_createfile(u8 *filename, u16 binarytype, ecm_info ecminfo,
                  sul_file_progressreport progressreport_funcptr)
{
#define SUL_CREATEFILE_REPORT_THRESHOLD             (50*1024)
    file_blockcrypto sulcrypto;
    SUL_HEADER *sulheader = NULL;
    F_FILE* fsul;
    F_FILE* fsultemp;
    u32 databufferlength;
    u32 bytecount;
    u32 threscount;
    u32 byteindex;
    u32 filesize;
    u8  percentage;
    u8 *readbuffer = NULL;    
    u8 sul_filename[32];
    u8 status;
    
    memset((char*)sul_filename,0,sizeof(sul_filename));
    strcpy((char*)sul_filename,"\\USER\\");
    if(ecminfo.vin[0] == NULL)
    {
        strcat((char*)sul_filename,"NO_VIN_NUMBER");
    }
    else
    {
        strcat((char*)sul_filename,(char*)ecminfo.vin);
    }
    strcat((char*)sul_filename,".sul");
    
    sulcrypto.bfcrypto.type = file_blockcrypto_type_bf;
    sulcrypto.bfcrypto.ctx = (blowfish_context_ptr*)&m_critical_ctx;
    sulcrypto.bfcrypto.crypto = &crypto_blowfish_encryptblock_full;
//    status = file_copy(filename, SULTEMPFILE,NULL,&sulcrypto,NULL);
    status = file_copy(filename, SULTEMPFILE,NULL,&sulcrypto,progressreport_funcptr);
    if(status != S_SUCCESS)
    {
        return status;
    }

    sulheader = __malloc(sizeof(SUL_HEADER));
    if(!sulheader)
    {
        return S_MALLOC;
    }

    memset(sulheader,0,sizeof(SUL_HEADER));
    sulheader->headerVersion = SULVERSION;
    memcpy(sulheader->companyID, "SCT", 3);
    sulheader->headerCrc32 = 0xFFFFFFFF;
    sulheader->contentCrc32 = 0xFFFFFFFF;    
    sulheader->hwFirmwareVersion = APPLICATION_VERSION;
    settings_get_devicepartnumberstring(sulheader->hwPartNumber);
    strcpy((char*)sulheader->hwSerialNumber, (char const*)settingscritical.serialnumber);
    sulheader->BinaryType = binarytype;
    sulheader->ecminfoblock.ecminfo = ecminfo;
    
    fsultemp = genfs_general_openfile(SULTEMPFILE,"rb");
    if(!fsultemp)
    {
        status = S_OPENFILE; 
        goto SUL_DONE;
    }
    status = file_getfilesize(fsultemp,&filesize);
    if (status != S_SUCCESS)
    {
        status = S_FAIL;
        goto SUL_DONE;
    }

    status = file_calcfilecrc32e(fsultemp, &sulheader->contentCrc32);
    if(status != S_SUCCESS)
    {
        status = S_CRC32E;
        goto SUL_DONE;
    }

    if (fseek(fsultemp,0,SEEK_SET) != 0)
    {
        status = S_SEEKFILE;
        goto SUL_DONE;
    }

    crc32e_reset();
    sulheader->headerCrc32 = crc32e_calculateblock(sulheader->headerCrc32,
                                        (u32*)sulheader,sizeof(SUL_HEADER)/4);
    
    crypto_blowfish_encryptblock_critical((u8*)sulheader,sizeof(SUL_HEADER));
    
    fsul = genfs_general_openfile(sul_filename,"wb");
    if(!fsul)
    {
        status = S_OPENFILE;       
        goto SUL_DONE;
    }
    
    fwrite((u8*)sulheader,1,sizeof(SUL_HEADER),fsul);
    __free(sulheader);
    sulheader = NULL;
    
    readbuffer = __malloc(READBUFFERSIZE);
    if(!readbuffer)
    {
        readbuffer = NULL;
        status = S_MALLOC;
        goto SUL_DONE;
    }

    threscount = 0;
    byteindex = 0;
    percentage = 0;
    while(!feof(fsultemp))
    {
        databufferlength = fread(readbuffer,1,READBUFFERSIZE,fsultemp);
        if (databufferlength > 0 && databufferlength <= READBUFFERSIZE)
        {
            bytecount = fwrite(readbuffer,1,databufferlength,fsul);
            if (bytecount != databufferlength)
            {
                status = S_WRITEFILE;
                break;
            }
        }
        else
        {
            status = S_READFILE;
            break;
        }
        threscount += bytecount;
        byteindex += bytecount;
        if (threscount >= SUL_CREATEFILE_REPORT_THRESHOLD)
        {
            threscount = 0;
            if (percentage != (byteindex*100/filesize))
            {
                percentage = byteindex*100/filesize;
                progressreport_funcptr(percentage,
                                       "Finalizing Stock File");
            }
        }
    }
    
SUL_DONE:
    if(sulheader)
        __free(sulheader);
    if(readbuffer)
        __free(readbuffer);
    
    genfs_closefile(fsultemp);
    genfs_closefile(fsul);
    
    memset(sul_file_info.last_sul_filename,
           0, sizeof(sul_file_info.last_sul_filename));
    if(status == S_SUCCESS)
    {        
        strcpy((char*)sul_file_info.last_sul_filename,(char*)ecminfo.vin);
        strcat((char*)sul_file_info.last_sul_filename,".sul");
    }
    
    return status;
}

//------------------------------------------------------------------------------
// Creates an SUL file from a file (usualy stock.bin) and flasher info
// Input:   u8  *filename
// Return:  bool status
//------------------------------------------------------------------------------
u8 sul_getlastfilename(u8 *filename)
{
    strcpy((char*)filename,(char const*)sul_file_info.last_sul_filename);
    return S_SUCCESS;
}
