/**
  **************** (C) COPYRIGHT 2011 SCT Performance, LLC *********************
  * File Name          : obd2comm.h
  * Device / System    : SCT products using the "CommonCode" platform
  * Author             : Quyen Leba
  *
  * Version            : 1 
  * Date               : 05/02/2011
  * Description        : 
  *                    : 
  *
  *
  * History            : 05/02/2011 M. Davis
  *                    :   Add SCT Copyright header,
  *
  ******************************************************************************
  */

#ifndef __OBD2COMM_H
#define __OBD2COMM_H

#include <board/genplatform.h>
#include <common/obd2iso.h>
#include <common/obd2sci.h>
#include <common/obd2can.h>
#include <common/obd2vpw.h>
#include <common/obd2scp.h>

#endif    //__OBD2COMM_H
