/**
  **************** (C) COPYRIGHT 2011 SCT Performance, LLC *********************
  * File Name          : usbcmdhandler_tune.h
  * Device / System    : SCT products using the "CommonCode" platform
  * Author             : Quyen Leba
  *
  * Version            : 1 
  * Date               : 05/02/2011
  * Description        : 
  *                    : 
  *
  *
  * History            : 05/02/2011 M. Davis
  *                    :   Add SCT Copyright header,
  *
  ******************************************************************************
  */

#ifndef __USBCMDHANDLER_TUNE_H
#define __USBCMDHANDLER_TUNE_H

#include <arch/gentype.h>

typedef struct
{
    u8  percentage;
    u16 flags;
    u32 max_data_report_frame_length;
    u32 signature;
    struct
    {
        u8  active                      : 1;    //tuning session is active
        u8  critical                    : 1;    //session entered critical process
        u8  halted                      : 1;
        u8  key_confirm                 : 1;    //user input received
        u8  marriedcountwarn_confirm    : 1;    //user input for married count warn received (confirm or cancel)
        u8  marriedcountwarn_cancel     : 1;
        u8  reserved                    : 2;
    }control;
    VehicleCommType commtype[ECM_MAX_COUNT];
    VehicleCommLevel commlevel[ECM_MAX_COUNT];
}UsbCmdHandler_Tune_Info;

typedef struct
{
    u32 signature;
    u8 cmd;
    u8 ack;
    u8 sequence;
    u8 datalength;
    u8 reserved[4];
    u8 data[44];
}UsbCmdHandlerPassthroughTuneResponseAckFrame;

//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
void uch_tune();
bool uch_tune_isbusy();

u8 uch_tune_internal_response_ack(CMDIF_COMMAND cmd, CMDIF_ACK respack,
                                  u8 *data, u16 datalength);

#endif     //__USBCMDHANDLER_TUNE_H
