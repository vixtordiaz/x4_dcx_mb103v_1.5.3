/**
  **************** (C) COPYRIGHT 2011 SCT Performance, LLC *********************
  * File Name          : obd2tune.h
  * Device / System    : SCT products using the "CommonCode" platform
  * Author             : Quyen Leba
  *
  * Version            : 1 
  * Date               : 05/02/2011
  * Description        : 
  *                    : 
  *
  *
  * History            : 05/02/2011 M. Davis
  *                    :   Add SCT Copyright header,
  *
  ******************************************************************************
  */

#ifndef __OBD2TUNE_H
#define __OBD2TUNE_H

#include <arch/gentype.h>
#include <fs/genfs.h>
#include <common/obd2def.h>
#include <common/ecm_defs.h>
#include <common/filestock_option.h>

/* This file is used to allow skipping checksum for specific part numbers */
#define SKIPCHECKSUMLIST                        "SKIPCHECKSUMLIST.txt"

#define DEFAULT_CUSTOM_TUNE_INDEX_FILENAME      GENFS_USER_FOLDER_PATH"indexfile.ctf"
#define CURRENT_CTF_VERSION                     3
#define TUNE_REVISION_FILENAME                  "tuneversion.txt"
#define DEFAULT_PRELOADED_TUNE_LOOKUP_FILE      "lookup.plf"
#define CURRENT_PRELOADED_TUNE_FILETYPE_13      0x0D
#define CURRENT_PRELOADED_TUNE_FILETYPE_14      0x0E
#define CURRENT_OPH_VERSION                     1
#define VIN_MASK_BYTE                           '*'
#define TUNE_MASK_BYTE                          '*'
#define MERGED_OPTS_CUSTOM_FILENAME             GENFS_USER_FOLDER_PATH"mcustomopts.oph"
//merge of regular option file and special option file as specified in .sth file
#define MERGED_OPTS_PRELOAD_FILENAME_PARTA      GENFS_USER_FOLDER_PATH"mpreloadoptsA#.oph"
//merge of regular option file (possibly MERGED_OPTS_PRELOAD_FILENAME_PARTA) with special option file from DEFAULT_OPTION_LOOKUP_FILE
#define MERGED_OPTS_PRELOAD_FILENAME_PARTB      GENFS_USER_FOLDER_PATH"mpreloadoptsB#.oph"
//merge of regular option file with special option file from customtune_block
#define MERGED_OPTS_CUSTOM_FILENAME_PARTA       GENFS_USER_FOLDER_PATH"mcustomoptsA.oph"
//merge of regular option file with special option file from DEFAULT_OPTION_LOOKUP_FILE
#define MERGED_OPTS_CUSTOM_FILENAME_PARTB       GENFS_USER_FOLDER_PATH"mcustomoptsB.oph"
#define CURRENT_GLF_VERSION                     0
#define DEFAULT_OPTION_LOOKUP_FILE              "optionlookup.glf"
#define DEFAULT_OTF_PRELOAD_GLF                 GENFS_DEFAULT_FOLDER_PATH"otflookup.glf"

#define SUPPORT_LOOKUP_PLF_VERSION              1   //version 0x01
//set '1' to use new preloaded tune lookup method
#define USE_LOOKUP_PLF_PROCESS                  1   //use lookup.plf
//clear '0' to separate PCM & TCM 6.0L tune files (old method was combined)
#define SUPPORT_COMBINED_STRATEGY_FOR_6_0L      0
//set '1' to enable sth & oph files with encryption
#define STH_OPH_ENCRYPTION_ENABLED              1
//set '1' to handle saved option using old method
#define USE_OPTION_HANDLING_OPT                 1   //SavedOpts_ECUx.opt
//set '1' to handle saved option using new method
#define USE_OPTION_HANDLING_SOF                 0   //SavedOpts_ECUx.sof
//set '1' to enable IPC checksum file with encryption
#define IPC_CHECKSUM_ENCRYPTION_ENABLED         1
//set '1' to enable CSF checksum file with encryption
#define CSF_CHECKSUM_ENCRYPTION_ENABLED         1
//clear '0' to ignore an invalid preloaded tune instead of causing error
#define IGNORE_INVALID_PRELOADED_TUNE           0
//set '1' to enable checking PreloadedTuneLookupItem.tunefilecrc32einfo vs sthFileHeader.header_crc32e
#define PRELOADED_TUNE_LOOKUP_CRC32E_TUNE_CHECK 1 
//set '1' to check tune header/content crc32e
#define PRELOADED_TUNE_CRC32E_TUNE_CHECK        1
//set '1' to enable glf files with encryption
#define GLF_ENCRYPTION_ENABLED                  1

typedef enum
{
    UnlockTask_Upload,
    UnlockTask_Download,
}UnlockTask;

typedef enum
{
    OS_Block_Read,
    OS_Block,
    Cal_Block,
}BlockType;

typedef enum
{
    PRELOADED_TUNE,
    CUSTOM_TUNE,
}TuneType;

typedef enum
{
    CAL_DOWNLOAD = 1,
    FULL_DOWNLOAD = 2,
}DownloadType;

// CTF File Header (indexfile.ctf)
typedef struct  //32 bytes
{
    u16 version;
    u16 tunecount;
    u32 header_crc32e;
    u32 flags;
    u32 content_crc32e;
    u8  reserved_1[16];
}customtune_header;
STRUCT_SIZE_CHECK(customtune_header,32);

//[bit1..0]
#define TUNEFLAG_CAL_ONLY                       (0<<0)
#define TUNEFLAG_ALLOW_FULL                     (1<<0)
#define TUNEFLAG_FORCED_FULL                    (2<<0)
#define TUNEFLAG_FLASH_CONTROL_MASKED           (0x00000003)
//[bit2]
#define TUNEFLAG_OFF_ROAD                       (1<<2)
////////[bit3]
//////#define TUNEFLAG_OVERRIDE_SEGMENT_CHECK         (1<<4)
//[bit3]
#define TUNEFLAG_FORCE_MERGE                    (1<<3)
//[bit6..4]
#define TUNEFLAG_BOOTLOADER_NORMAL              (0<<4)
//[bit7]: reserved
//[bit10..8]
#define TUNEFLAG_DISABLE_PRELOADED_TUNE         (1<<8)  //keep custom tunes after re-enable
#define TUNEFLAG_DISABLE_PRELOADED_TUNE_REMOVE  (2<<8)  //remove all custom tunes after re-enable
//[bit14..11]
#define TUNEFLAG_COMPRESSION_NONE               (0<<11)
#define TUNEFLAG_COMPRESSION_LZW                (1<<11)
#define TUNEFLAG_COMPRESSION_HOFFMAN            (2<<11)
//[bit18..15]
#define TUNEFLAG_ENCRYPTION_NONE                (0<<15)
#define TUNEFLAG_ENCRYPTION_BLOWFISH            (1<<15)
#define TUNEFLAG_ENCRYPTION_RESEVED             (2<<15)
#define TUNEFLAG_ENCRYPTION_TEA                 (3<<15)
//[bit31..19]: reserved

#define obd2tune_tuneflags_iscalonly(f)   \
    ((f & TUNEFLAG_FLASH_CONTROL_MASKED) == TUNEFLAG_CAL_ONLY)
#define obd2tune_tuneflags_isallowfull(f)   \
    ((f & TUNEFLAG_FLASH_CONTROL_MASKED) == TUNEFLAG_ALLOW_FULL)
#define obd2tune_tuneflags_isforcefull(f)   \
    ((f & TUNEFLAG_FLASH_CONTROL_MASKED) == TUNEFLAG_FORCED_FULL)

#define obd2tune_tuneflags_isencrptionTEA(f)   \
    ((f & TUNEFLAG_ENCRYPTION_TEA) == TUNEFLAG_ENCRYPTION_TEA)

#define obd2tune_tuneflags_isdisablepreload(f)   \
    ((f & TUNEFLAG_DISABLE_PRELOADED_TUNE) == TUNEFLAG_DISABLE_PRELOADED_TUNE)
#define obd2tune_tuneflags_isdisablepreloadremovetunes(f)   \
    ((f & TUNEFLAG_DISABLE_PRELOADED_TUNE_REMOVE) == TUNEFLAG_DISABLE_PRELOADED_TUNE_REMOVE)

//////#define obd2tune_tuneflags_isoverridesegmentcheck(f)   \
//////    ((f & TUNEFLAG_OVERRIDE_SEGMENT_CHECK) == TUNEFLAG_OVERRIDE_SEGMENT_CHECK)

// CTF File Block (indexfile.ctf)
typedef struct  //256 bytes
{
    u32 flags;
    u16 veh_type;
    u8  reserved_1[2];
    u32 market_type;
    u32 signature;
    u8  tunedescription[32];
    u8  tunefilename[16];
    u8  optionfilename[16];
    u8  tuneosfilename[16];
    u8  specialoptionfilename[16];
    u8  checksumfilename[16];
    u8  serialnumber[16];
    u32 tunefilecrc32e;
    u32 optionfilecrc32e;
    u8  reserved_2[20];
    u32 bootloader_crc32e;
    u8  bootloader_upload[16];
    u8  bootloader_download[16];
    u8  note[48];
}customtune_block;
STRUCT_SIZE_CHECK(customtune_block,256);

typedef struct
{
#define CUSTOMTUNE_TUNEOS_VERSION           1
    u16 version;
    u8  reserved1;
    u8  infocount;
    u32 flags;
    u32 header_crc32e;
    u32 content_crc32e;
    u8  maskable_vin[17+1];
    u8  reserved2;
    u8  ecm_count;
    u32 basestockcrc32e[ECM_MAX_COUNT];
    u8  basestockfilename[ECM_MAX_COUNT][32];
    u8  reserved3[112];
}customtune_tuneosheader; // Total 256 bytes
STRUCT_SIZE_CHECK(customtune_tuneosheader,256);

typedef struct
{
    u32 flags;
    u8  type;
    u8  reserved1[10];
    u8  ecm_count;
    u8  codes[3][10][32];
    u8  second_codes[3][10][32];
    u8  reserved2[112];
}customtune_tuneosinfoblock; // Total 2048 bytes
STRUCT_SIZE_CHECK(customtune_tuneosinfoblock,2048);

typedef struct
{
    u8  codes[128]; //TODOQK: codes & secondcodes could be enlarged but be careful.
    u8  secondcodes[128];    //must be same size as tuneos_codes
    u8  vin[VIN_LENGTH+1];
    u8  codecount;
    u8  ecm_count;
    u8  basestockfilename[ECM_MAX_COUNT][32];
    u32 basestockcrc32e[ECM_MAX_COUNT];
}customtune_tuneosdata;

typedef enum
{
    OptionSoure_RegularOption,
    OptionSoure_SpecialOption,
}OptionSource;

typedef struct  //64 bytes
{
    // option file structure definition version (current: 0)
    u8  version;
    u8  reserved_1[3];
    // crc32e of option file header
    u32 header_crc32e;
    // crc32e of option file content
    u32 content_crc32e;
    // global flags of option file (default: 0)
    u32 flags;
    // length of option file header
    u32 header_length;
    // where the 1st ophChoiceItemInfoBlock located in option file
    u32 firstchoice_offset;
    // where the 1st ophOptionItemRecord located in option file
    u32 firstrecord_offset;
    // how many options available
    u16 option_count;
    u8  reserved_2[34];
}ophOptionFileHeader;
STRUCT_SIZE_CHECK(ophOptionFileHeader,64);

typedef struct  //64 bytes
{
    // type of this option
    u8  type;
    // how many choices of an option
    u8  choice_count;
    // the default choice/selection
    u8  choice_default;
    u8  reserved_1;
    // flags of this option (default: 0)
    u32 flags;
    // where the 1st ophChoiceItemInfoBlock of this option located in option file
    u32 choicecontent_offset;
    u32 reserved_2;
    // description of the option
    u8  description[32];
    u8  reserved_3[16];
}ophOptionItemInfoBlock;
STRUCT_SIZE_CHECK(ophOptionItemInfoBlock,64);

typedef struct
{
    // flags of this choice (default: 0)
    u32 flags;
    // where the 1st ophOptionItemRecord of this choice located in option file
    u32 choicedata_offset;
    // description of the choice
    u8  description[56];
}ophChoiceItemInfoBlock;
STRUCT_SIZE_CHECK(ophChoiceItemInfoBlock,64);

#define ECU_DataType_Float          0
#define ECU_DataType_Int            1
#define ECU_DataType_Mask           0x07
#define ECU_Data_BigEndian          BIT3
#define ECU_Data_Signed             BIT4

typedef struct
{
    u32 flags;
    u32 address;
    u8  opcode;
    u8  length;
    u8  reserved[2];
    float offset;
    float multiplier;
    float adjustment;
    float min;
    float max;
}ophOptionItemRecord_Generic;
STRUCT_SIZE_CHECK(ophOptionItemRecord_Generic,32);

typedef struct
{
    u32 flags;
    u32 address;
    u8  opcode;
    u8  length;
    u8  data[22];
}ophOptionItemRecord_Opcode_d;
STRUCT_SIZE_CHECK(ophOptionItemRecord_Opcode_d,32);

typedef struct
{
    u32 flags;
    u32 address;
    u8  opcode;
    u8  zero;
    u8  reserved0[2];
    u32 length;
    u8  reserved1[16];
}ophOptionItemRecord_Opcode_D;
STRUCT_SIZE_CHECK(ophOptionItemRecord_Opcode_D,32);

typedef struct
{
    u32 flags;
    u32 address;
    u8  opcode;
    u8  length;
    u8  math_index;
    u8  math_flags;
    float equ_default_fval;
    float equ_calc_fval_0;
    float equ_calc_fval_1;
    u32 equ_calc_u32val;
    s32 equ_calc_s32val;
}ophOptionItemRecord_Opcode_c;
STRUCT_SIZE_CHECK(ophOptionItemRecord_Opcode_c,32);

typedef struct
{
    //same as struct ophOptionItemRecord + the following fields:
    
    float starting_value;
    float step_value;
    u16 step_count;
    u8  subopcode;
    u8  reserved2[21];
}ophOptionItemRecord_Opcode_l;

typedef struct      //256 bytes
{
    u8  file_type;          // 0x04... stf, stg type 1; 0x0D... sth (later type incremented from 0x0D)
    u8  reserved_1[3];
    u32 header_crc32e;
    u32 content_crc32e;
    u16 header_length;
    u16 ecm_type;
    u32 datecode;           // 0020101212, i.e. YYYYYYMMDD (we'll worry about it 427 millenia later)
    u32 flags;
    u32 optionfile_headercrc32einfo;
    u32 basestock_crc32e;
    u8  reserved_2[16];

    u8  tunecode[32];       //strategy, OS part# etc
    u8  tunedescription[32];
    u8  optionfilename[16];
    u8  tuneosfilename[16];
    u8  specialoptionfilename[16];
    u8  checksumfilename[16];
    u8  reserved_3[80];
}sthFileHeader_type13;
STRUCT_SIZE_CHECK(sthFileHeader_type13,256);

typedef struct      //512 bytes
{
    u8  file_type;          // 0x0E (14) ... sth with long filename (later type incremented from 0x0E)
    u8  reserved_1[3];
    u32 header_crc32e;
    u32 content_crc32e;
    u16 header_length;
    u16 ecm_type;
    u32 datecode;           // 0020101212, i.e. YYYYYYMMDD
    u32 flags;
    u32 optionfile_headercrc32einfo;
    u32 basestock_crc32e;
    u8  reserved_2[16];

    u8  tunecode[32];       //strategy, OS part# etc
    u8  reserved_3[64];
    u8  tunedescription[32];
    u8  optionfilename[32];
    u8  tuneosfilename[32];
    u8  specialoptionfilename[32];
    u8  checksumfilename[32];
    u8  reserved_4[208];
}sthFileHeader_type14;
STRUCT_SIZE_CHECK(sthFileHeader_type14,512);

//sthFileHeader.flags:
// - Bit[1..0]: flash control (0: cal only, 1: allow full, 2: force full)
// - Bit[2]: off-road (0: regular tune, 1: off-road tune)
// - Bit[5..4]: On-the-fly tune capability (0: none, 1: enabled, 2: reserved, 3: reserved)
// - Bit[7..6]: starting source (0: stock from vehicle or stock.bin -  1: SPF stock preloaded file, 2: reserved, 3: reserved)
//        Starting source is the source image that this STH applies the changes on
//        Case 0: if vehicle doesn't allow upload stock, use stock.bin � this is the default case.
//        Case 1: essential for OTF tunes as they require true stock file.
// - Bit[31..8]: reserved

typedef union
{
    sthFileHeader_type13 type13;
    sthFileHeader_type14 type14;
    struct
    {
        u8  file_type;
        u8  reserved_1[3];
        u32 header_crc32e;
        u32 content_crc32e;
        u16 header_length;
        u16 ecm_type;
        u32 datecode;           // 0020101212, i.e. YYYYYYMMDD
        u32 flags;
        u32 optionfile_headercrc32einfo;
        u32 basestock_crc32e;
        u8  reserved_2[16];
    }generic;
}sthFileHeader;

// Bit[1..0]: flash control (0: cal only, 1: allow full, 2: force full)
#define sthFileHeaderFlags_getFlashControl(f)       (f & 0x03)
#define sthFileHeaderFlags_isCalOnlyFlash(f)        (sthFileHeaderFlags_getFlashControl(f) == 0)
#define sthFileHeaderFlags_isAllowFullFlash(f)      (sthFileHeaderFlags_getFlashControl(f) == 1)
#define sthFileHeaderFlags_isForceFullFlash(f)      (sthFileHeaderFlags_getFlashControl(f) == 2)
// Bit[2]: off-road (0: regular tune, 1: off-road tune)
#define sthFileHeaderFlags_getOffRoad(f)            ((f>>2) & 0x01)
// Bit[3]: Reserved
#define sthFileHeaderFlags_getOnTheFlyMode(f)       ((f>>4) & 0x03)
#define sthFileHeaderFlags_isOnTheFlyEnabled(f)     (sthFileHeaderFlags_getOnTheFlyMode(f) == 1)
#define sthFileHeaderFlags_getVehicleStockSource(f) ((f>>6) & 0x03)
#define sthFileHeaderFlags_isVehicleStockSource(f)  (sthFileHeaderFlags_getVehicleStockSource(f) == 0)
#define sthFileHeaderFlags_isSPFSrouce(f)           (sthFileHeaderFlags_getVehicleStockSource(f) == 1)

typedef ophOptionItemRecord_Generic         sthOptionItemRecord_Generic;
typedef ophOptionItemRecord_Opcode_d        sthOptionItemRecord_Opcode_d;
typedef ophOptionItemRecord_Opcode_l        sthOptionItemRecord_Opcode_l;

typedef struct
{
    u8  version;                        //current version: 0x00
    u8  oem_type;
    u8  reserved_1[2];
    u32 header_crc32e;                  //crc32e of option file header
    u32 content_crc32e;                 //crc32e of option file content
    u32 flags;                          //global flags (default: 0)
    u32 header_length;                  //length of this header: 128 bytes
 
    u32 hardware;
    u32 device_market;
    u32 device_type;
    u8  serialnumber[16];
    u8  devicepartnumberstring[32];
    u8  reserved_2[176];
}GeneralFileLookupHeader;               //SIZE: 256
STRUCT_SIZE_CHECK(GeneralFileLookupHeader,256);

typedef enum
{
    GeneralFileLookupItemType_SpecialOptionFile                     = 0x0001,
    GeneralFileLookupItemType_SpecialOptionFile_CommandOnly         = 0x0002,
    GeneralFileLookupItemType_OtfPreloadFile                        = 0x0003,
}GeneralFileLookupItemType;

typedef struct
{
    u16 type;
    u8  reserved_1[2];
    u32 flags;
    u8  data[120];
}GeneralFileLookupGenericItem;          //SIZE: 128
STRUCT_SIZE_CHECK(GeneralFileLookupGenericItem,128);

typedef enum
{
    //strategy/OSpart#
    GeneralFileLookupFileProcessorCodeType_Regular     = 0,
    //second_code
    GeneralFileLookupFileProcessorCodeType_SecondCode  = 1,
}GeneralFileLookupFileProcessorCodeType;

typedef struct
{
    u16 type;                           //0x0001: special option file
    u8  reserved_1[2];
#define GeneralFileLookupSpecialOptionFileFlags_None                0
//Bit[1..0]
#define GeneralFileLookupSpecialOptionFileFlags_ListMergeFormat     0
#define GeneralFileLookupSpecialOptionFileFlags_ListSeparateFormat  1
#define GeneralFileLookupSpecialOptionFileFlags_IndividualFormat    2
//Bit[31..2]: reserved
    u32 flags;                          //item flags (default: 0)

    u8  processor_code[32];             //strategy, OSpart#, etc (NULL terminated)
    u8  vin[17+1];                      //maskable w/ '*' (NULL terminated)
    u8  processor_code_type;
    u8  reserved_2[9];
    u32 special_optionfile_headercrc32e;
    u8  special_option_filename[32];    //use .oph (NULL terminated)
    u8  reserved_3[24];
}GeneralFileLookupSpecialOptionFile;    //SIZE: 128
STRUCT_SIZE_CHECK(GeneralFileLookupSpecialOptionFile,128);

typedef struct
{
    u16 type;                           //0x0003: OTF Preload File
    u8  reserved_1[2];
#define GeneralFileLookupOtfPreloadFileFlags_None                0
//Bit[1..0]
#define GeneralFileLookupOtfPreloadFileFlags_ListMergeFormat     0
#define GeneralFileLookupOtfPreloadFileFlags_ListSeparateFormat  1
#define GeneralFileLookupOtfPreloadFileFlags_IndividualFormat    2
//Bit[31..2]: reserved
    u32 flags;                          //item flags (default: 0)

    u8  processor_code[32];             //strategy, OSpart#, etc (NULL terminated)
    u8  vin[17+1];                      //maskable w/ '*' (NULL terminated)
    u8  processor_code_type;
    u8  reserved;
    u8  otf_preload_description[32];    //use .oph (NULL terminated)
    u8  otf_preload_filename[32];    //use .oph (NULL terminated)
    u32 otf_preload_headercrc32e;
}GeneralFileLookupOtfPreloadFile;    //SIZE: 128
STRUCT_SIZE_CHECK(GeneralFileLookupOtfPreloadFile,128);

#define isGeneralFileLookupSpecialOptionFileFlags_ListMergeFormat(f)    ((f & 0x3) == 0)
#define isGeneralFileLookupSpecialOptionFileFlags_ListSeparateFormat(f) ((f & GeneralFileLookupSpecialOptionFileFlags_ListSeparateFormat) == GeneralFileLookupSpecialOptionFileFlags_ListSeparateFormat)
#define isGeneralFileLookupSpecialOptionFileFlags_IndividualFormat(f)   ((f & GeneralFileLookupSpecialOptionFileFlags_IndividualFormat) == GeneralFileLookupSpecialOptionFileFlags_IndividualFormat)

typedef enum
{
    PreloadedTuneLookupHeaderFlags_None             = 0,
    PreloadedTuneLookupHeaderFlags_OffRoad          = (1<<0),
    PreloadedTuneLookupHeaderFlags_50StateLegal     = (1<<1),
    //Bit[31..2]: reserved, set to ZERO
}PreloadedTuneLookupHeaderFlags;

typedef struct
{
    u16 version;
    u8  oem_type;
    u8  reserved1;
    u32 datecode;           //YYYYYMMDD in decimal
    u32 signature;
    u32 header_crc32e;
    u32 content_crc32e;
    u32 flags;
    u32 hardware;
    u32 market_type;
    u32 device_type;
    u32 tunecount;
    u16 totalfulltunedescription_count;
    u8  reserved2[22];
    u8  revisionstring[32];
    u8  serialnumber[16];
    u8  devicepartnumberstring[32];
    u8  description[128];
    u8  reserved3[240];
}PreloadedTuneLookupHeader;
STRUCT_SIZE_CHECK(PreloadedTuneLookupHeader,512);

typedef struct
{
    u8  description[320];
}PreloadedTuneLookupFullTuneDescription;
STRUCT_SIZE_CHECK(PreloadedTuneLookupFullTuneDescription,320);

//Bit[2..0]
#define PreloadedTuneLookupItemFlags_Any                    (0)
#define PreloadedTuneLookupItemFlags_AutomaticOnly          (1)
#define PreloadedTuneLookupItemFlags_ManualOnly             (2)
//Bit3
#define PreloadedTuneLookupItemFlags_HasFullTuneDescription (1<<3)

#define PreloadedTuneLookupItem_InvalidFullDescriptionIndex 0xFFFF
typedef struct
{
    u8  tunefilename[28];
    u32 tunefilecrc32einfo;
    u8  tunedescription[28];
    u32 flags;
    u8  vinmask[10];
    u16 fulltunedescription_index;
    u8  reserved[20];
}PreloadedTuneLookupItem;
STRUCT_SIZE_CHECK(PreloadedTuneLookupItem,96);



#define TUNENAMEID_START                        0x0020
#define MAX_DESCRIPTION_LENGTH                  30
#define MAX_TUNE_NAME_LENGTH                    30

typedef struct
{
    u8  count;
    u8  veh_type[MAX_TUNE_CODE_LIST_COUNT];  //currently used in custom tune
    u8  description[MAX_TUNE_CODE_LIST_COUNT][MAX_DESCRIPTION_LENGTH+1];
    u16 fulldescription_index[MAX_TUNE_CODE_LIST_COUNT];
    u32 tuneflags[MAX_TUNE_CODE_LIST_COUNT];
    u8  tunefilename[MAX_TUNE_CODE_LIST_COUNT][MAX_TUNE_NAME_LENGTH+1];
    u8  optionfilename[MAX_TUNE_CODE_LIST_COUNT][MAX_TUNE_NAME_LENGTH+1];
    FuelType fueltype[MAX_TUNE_CODE_LIST_COUNT];
}tunecode_infolist;

typedef struct
{
    u16 tuneindex[ECM_MAX_COUNT];
    u8  description[ECM_MAX_COUNT][MAX_DESCRIPTION_LENGTH+1];
    u8  tunefilename[ECM_MAX_COUNT][MAX_TUNE_NAME_LENGTH+1];
    u8  optionfilename[ECM_MAX_COUNT][MAX_TUNE_NAME_LENGTH+1];
    u8  specialoptionfilename[ECM_MAX_COUNT][MAX_TUNE_NAME_LENGTH+1];
    u8  checksumfilename[ECM_MAX_COUNT][MAX_TUNE_NAME_LENGTH+1];
    u32 flags[ECM_MAX_COUNT];
    u8  useoption[ECM_MAX_COUNT];
    bool supported[ECM_MAX_COUNT];
}selected_tunelist_info;

typedef enum
{
    FLASHERFLAG_SKIP_APPLY_OPTIONS              = ((u32)1<<0),
    FLASHERFLAG_SKIP_APPLY_CHECKSUM             = ((u32)1<<1),
    FLASHERFLAG_SKIP_MARRIED_CHECK              = ((u32)1<<2),
    FLASHERFLAG_SKIP_MARRIED_COUNT              = ((u32)1<<3),
    FLASHERFLAG_CLEAR_MARRIED_STATUS            = ((u32)1<<4),
    FLASHERFLAG_SKIP_OVERLAY                    = ((u32)1<<5),
    FLASHERFLAG_USER_SELECT_VEHICLE_TYPE        = ((u32)1<<6),
    FLASHERFLAG_RETURNSTOCK_RECOVERY            = ((u32)1<<7),
    FLASHERFLAG_VALID_VEHICLE_INFO              = ((u32)1<<8),
    FLASHERFLAG_VALID_ACTUAL_TIRE_SIZE          = ((u32)1<<9),
    FLASHERFLAG_DISABLE_PRELOADED_TUNE          = ((u32)1<<10),
    FLASHERFLAG_DISABLE_PRELOADED_TUNE_REMOVE   = ((u32)1<<11),
    FLASHERFLAG_SKIP_FLASH_TCM                  = ((u32)1<<15),
}flasher_flags;

//flasher_info changelog:
// 1: ecm_info (increase MAX_TUNECODE_LENTH to 31 from 21)
// 2: Added Special Options (OPH Opcode 'C' type) math index
#define FLASHER_INFO_VERSION        2
typedef struct
{
    //vehicle_type is enerated from selected_tunelist_info.tuneindex[]
    u16 vehicle_type;   //used to reference with veh_defs
    //flashtype: guide upload/download process (preload,custom,stock)
    u8  flashtype;      //CMDIF_ACT_CUSTOM_FLASHER, CMDIF_ACT_PRELOADED_FLASHER
                        //CMDIF_ACT_STOCK_FLASHER
    u8  ecm_count;
    u8  vin[VIN_LENGTH+1];
    u8  reserved_1[2];
    u8  vehicle_serial_number[32];  //must be same size as Settings_Tune.vehicle_serial_number
    selected_tunelist_info selected_tunelistinfo;
    tunecode_infolist tunecodeinfolist;
    ecm_info ecminfo;       //only available after a ..._get_strategy_flasher()
    
    u32 flags;      //for now, all flags are for return to stock (and debug)
    //uploadstock_required: TRUE indicating device is unmarried and an upload
    //is required (set by unlock process)
    bool uploadstock_required;
    //isupload_only: TRUE - perform upload only (no married)
    bool isupload_only;
    //isunlockpriordownload: TRUE means need to unlock before a download
    //in some cases, unlock is not required because of an upload and can only
    //unlock once when key recycled
    bool isunlockpriordownload;
    //isfullreflash_already_set: TRUE -> isfullreflash[] already set
    bool isfullreflash_already_set;
    //isfullreflash: TRUE -> a full reflash required
    bool isfullreflash[ECM_MAX_COUNT];
    //isfullreflash: TRUE -> VIN is masked
    bool isvinmasked;
    u8  tunecheck_status;
    u16 utilityflags;
    
    // Special Options flags - 
    // Track when user selects special options to be applied. ie. TPMS, Tire Size, Axel Ratio
    OptionFlags options_flags;
    struct
    {
        u8  pcm : 4;
        u8  tcm : 4;
        u8  reserved;
    }otfinfo;
    //isstockcrc: TRUE means that we are flashing with a stock file. If we are 
    //doing so, we skip the download of a modified bootloader. Ex: fiesta, 
    //China Focus. 
    bool isstockcrc; 
    
    u8  reserved_2[75];
    
    //tunebytecount: size of the whole tune (equivalent to stockfilesize)
    u32 tunebytecount;
    
    //tune_programming_status: use to track progress (upload/download)
    struct
    {
        u8 ecm_index;
        u8 percentage;
        u32 totalbytecount;
        u32 currentbytecount;
    }tune_programming_status;
    struct
    {
        u32 front;          //generic 4 tire size byte data
        u32 rear;           //generic 4 tire size byte data
    }actualtire;
    //store vehiclecodes to check if full reflash required and/or base stock
    //file required; if 1st byte NULL, skip this check
    //base stock file is required to flash to vehicle first before flashing
    //actual tune file/data in certain vehicles
    u8  vehiclecodes[128];  //must be same size as settingstune.vehiclecodes
    u8  secondvehiclecodes[128];
    u8  basestockfilename[ECM_MAX_COUNT][32];
    u32 basestockcrc32e[ECM_MAX_COUNT];
    struct
    {
        u8  front;
        u8  reserved0[3];
        u8  rear;
        u8  reserved1[3];
    }actualtpms;
    u32 actualaxleratio;    //generic 4 byte axle ratio data
}flasher_info;

u8 obd2tune_flasherinfo_init();
u8 obd2tune_flasherinfo_deinit();
u8 obd2tune_garbagecollector();
u8 obd2tune_gettunerevision(u8 *tunerevision);
u8 obd2tune_getdatecode_plf(u32 *datecode);
bool obd2tune_is_plfdatecode_old();
u8 obd2tune_validate_preloadedtune_lookupheader(u32 filesize,
                                                PreloadedTuneLookupHeader *lookupheader,
                                                u8 *revisionstring,
                                                u8 *description);
u8 obd2tune_getecmsize_byecmtype(u16 ecm_type, u32 *ecmsize);
u8 obd2tune_getstocksize_byvehdef(u16 veh_type, u32 *stocksize);
u8 obd2tune_getecmdownloadsize(u16 ecm_type,
                               u32 *cal_only_size, u32 *all_size);
u8 obd2tune_testerpresent_setup(bool highspeed, VehicleCommType commtype);
u8 obd2tune_testerpresent_add_ecm(u32 ecm_id, VehicleCommLevel commlevel);
u8 obd2tune_testerpresent_remove_ecm(u32 ecm_id);
void obd2tune_testerpresent();
void obd2tune_disableallnormalcomm(VehicleCommType commtype);
u8 obd2tune_checkprogrammingcondition();
u8 obd2tune_reset_ecm(u16 ecm_type);
u8 obd2tune_unlockecm(UnlockTask unlocktask,
                      u16 ecm_type, VehicleCommType commtype, void *priv);
u8 obd2tune_requesthighspeed(u16 ecm_type, VehicleCommType commtype, void *priv);
u8 obd2tune_findapplicableutility(u16 ecm_type, bool isdownloadutility, u8 *utilitychoice);
u8 obd2tune_downloadutility(u16 ecm_type, bool isdownloadutility, bool is_cal_only);
u8 obd2tune_exitutility(u16 ecm_type, VehicleCommType commtype,
                        bool isdownloadutility);
u8 obd2tune_downloadfinalizing(u16 ecm_type, flasher_info *flasherinfo);
u8 obd2tune_getcustomtune_infolist(tunecode_infolist *tunecodeinfolist);
u8 obd2tune_gettunecodelistinfo(u8 *tunecode, u8 *vin,
                                tunecode_infolist *tunecodeinfolist);
u8 obd2tune_getoptionfilename(u8 *tunefilename, u8 *optionfilename);
u8 obd2tune_getspecialoptionfilename(u8 *tunefilename, u8 *specialoptionfilename);
u8 obd2tune_checkvehiclecode(ecm_info *ecm, TuneType tunetype);
bool obd2tune_isvin_matched(const u8 *vin1, const u8 *vin2, u8 mask_byte);
bool obd2tune_ispreviousvehicle_matched(u8 *vin, u8 *codes);
u8 obd2tune_setpreviousvin(u8 *vin);
u8 obd2tune_validate_vehiclecodes(u8 *vehiclecodes);
u8 obd2tune_setmarried(u16 veh_type, u32 stocktunesize,
                       u8 *vehicle_serial_number);
u8 obd2tune_checkmarried(u16 veh_type, u8 *vin, u8 *vehicle_serial_number);
u8 obd2tune_checkstockfile(u16 veh_type);
u8 obd2tune_handle_overlaydata(u16 veh_type, u8 *vin, bool isdoapply);
u8 obd2tune_handle_overlaydata(u16 veh_type, u8 *vin, bool ishandleapply);
u8 obd2tune_apply_preloadedtune_sth(u32 offset, u8 *tunefilename);
u8 obd2tune_apply_preloadedtune(u16 veh_type, selected_tunelist_info *tuneinfo);
u8 obd2tune_apply_options(u8 flash_type, u16 veh_type,
                          selected_tunelist_info *tuneinfo);
u8 obd2tune_read_sth_header(F_FILE *sth_fptr,
                            sthFileHeader *header, u32 *header_length);
u8 obd2tune_validate_sth_header(sthFileHeader *header);
void obd2tune_sth_header_parse_optionfilename(sthFileHeader *header,
                                              u8 *optionfilename);
void obd2tune_sth_header_parse_specialoptionfilename(sthFileHeader *header,
                                                     u8 *specialoptionfilename);
u32 obd2tune_sth_header_parse_optionfile_headercrc32einfo(sthFileHeader *header);
u8 obd2tune_validate_sth_file(const u8 *filename,
                              sthFileHeader *header);
u8 obd2tune_calculate_oph_text_portion_crc32e(const u8 *optionfilename,
                                              u32 *crc32e);
u8 obd2tune_validate_oph_header(ophOptionFileHeader *option_header);
u8 obd2tune_validate_oph_file(const u8 *filename);
u8 obd2tune_translate_ecm_address_to_ecm_offset(u16 ecm_type, u32 ecm_address,
                                                u32 *ecm_offset);
u8 obd2tune_remove_customtune_files();
u8 obd2tune_preloaded_tune_get_restriction(u32 *restriction_value);
u8 obd2tune_preloaded_tune_set_restriction(u32 restriction_value);
u8 obd2tune_validate_ctf_header(customtune_header *ctf_header);
u8 obd2tune_validate_ctf_file(const u8 *filename);
u8 obd2tune_get_custom_tune_block(u16 slot, customtune_block *block);
u8 obd2tune_validate_custom_tune_block(customtune_block *block, bool skip_signature_check);
u8 obd2tune_process_custom_tune_block(customtune_block *block, bool isUseOption);
u8 obd2tune_extracttuneosdata_tos(const u8 *tuneosfilename,
                                  customtune_tuneosdata *tuneosdata);
u8 obd2tune_extracttuneosdata_txt(const u8 *tuneosfilename,
                                  customtune_tuneosdata *tuneosdata);
u8 obd2tune_maskablestringcompare(const u8 *str1, const u8 *str2, u8 maskbyte);
u8 obd2tune_maskablestringncompare(const u8 *str1, const u8 *str2, u8 maskbyte, u32 length);
u8 obd2tune_comparetunecode(const u8 *vehiclecodes, const u8 *code, u8 slot);
void obd2tune_stringreplace(u8 *str, u8 searchchar, u8 replacechar);
u8 obd2tune_mergeoptionfile(const u8 *mainoptionfilename,
                            const u8 *suboptionfilename,
                            const u8 *mergeoptionfilename);
u8 obd2tune_read_glf_header(F_FILE *glf_fptr,
                            GeneralFileLookupHeader *header, u32 *header_length);
u8 obd2tune_validate_glf_header(GeneralFileLookupHeader *header);
u8 obd2tune_validate_glf_file(const u8 *filename);
u8 obd2tune_search_glf_for_otf_file(const u8 *glf_filename,
                                               const u8 *vin,
                                               const u8 *processor_code,
                                               GeneralFileLookupFileProcessorCodeType code_type,
                                               u8 *filename,
                                               u8 *description,
                                               u32 *flags);
u8 obd2tune_search_glf_for_special_option_file(const u8 *glf_filename,
                                               const u8 *vin,
                                               const u8 *processor_code,
                                               GeneralFileLookupFileProcessorCodeType code_type,
                                               u8 *special_option_filename, u32 *flags);
u8 obd2tune_read_preloadedtune_index_from_preloadedtunefile(u8 *tunefilename,
                                                            u16 *preloadedtuneindex);
u8 obd2tune_attempt_to_find_vehicle_type_using_tunecode(u8 *tunecode,
                                                        u8 *vin,
                                                        u16 *veh_type);
u8 obd2tune_attempt_to_validate_vehicle_type_against_tunecode(u16 veh_type,
                                                              u8 *tunecode,
                                                              u8 *vin,
                                                              u16 *veh_type_found);
u8 obd2tune_search_customtune_specialoptionfile_with_tuneos(u16 veh_type,
                                                            const u8 *tuneos_filename,
                                                            const u8 *vin,
                                                            u8 *special_option_filename_pcm, u32 *special_option_flags_pcm,
                                                            u8 *special_option_filename_tcm, u32 *special_option_flags_tcm);
u8 obd2tune_upload_e2_data(u32 ecm_type);
u8 obd2tune_preflash_task(u32 ecm_type, u8 flash_type, bool is_cal_only);
u8 obd2tune_check_variant_id(u16 ecm_type, bool *variant_id, VehicleCommType vehiclecommtype);
u8 obd2tune_download_rsa_signature(u32 ecm_type, u8 block_index, BlockType block_type);
u8 obd2tune_download_trans_info(u32 ecm_type);
u8 obd2tune_do_check(u8 *response, u16 vehicle_type, bool checksupportfiles);

#endif  //__OBD2TUNE_H
