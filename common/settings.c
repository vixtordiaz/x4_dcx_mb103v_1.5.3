/**
  **************** (C) COPYRIGHT 2011 SCT Performance, LLC *********************
  * File Name          : settings.c
  * Device / System    : SCT products using the "CommonCode" platform
  * Author             : Quyen Leba
  *
  * Version            : 1 
  * Date               : 05/02/2011
  * Description        : 
  *                    : 
  *
  *
  * History            : 05/02/2011 M. Davis
  *                    :   Add SCT Copyright header,
  *
  ******************************************************************************
  */

#include <ctype.h>
#include <string.h>
#include <board/genplatform.h>
#include <board/properties_vb.h>
#include <fs/genfs.h>
#include <common/devicedef.h>
#include <common/statuscode.h>
#include <common/version.h>
#include <common/obd2tune.h>
#include <common/obd2tune_recoverystock.h>
#include <common/bootsettings.h>
#include <common/ecm_defs.h>
#include "settings.h"
#include <common/genmanuf_overload.h>
#include <common/veh_defs.h>

Settings_Critical settingscritical;
Settings_Tune settingstune;
Settings_Fleet settingsfleet;
Settings_DatalogGeneral settingsdataloggeneral;
extern const firmware_version fwversion;

#define SCT_MARKER0             'Q'
#define SCT_MARKER1             'W'
#define SCT_MARKER2             'E'

#define SETTINGS_DEMO_TUNE_FILENAME             "demotuse.bak"
#define SETTINGS_DEMO_DATALOGGENERAL_FILENAME   "demodase.bak"

bool iscriticalarea_dirty       = FALSE;
bool istunearea_dirty           = FALSE;
bool isfleetarea_dirty          = FALSE;
bool isdataloggeneralarea_dirty = FALSE;

const Settings_Critical SettingsCriticalDef =
{
    .version = 0,
    .sct = {SCT_MARKER0,SCT_MARKER1,SCT_MARKER2},
    .signature = 0x62139562,
    .crc32e = 0xFFFFFFFF,
    .flags = DEFAULT_CRITICALSETTINGS_FLAGS,
    .normaloperation_signature = 0,
    .firmware_version = 0,
    .devicetype = DEVICE_TYPE,
    .markettype = MARKET_TYPE,
    .baudrate = 0,
    .serialnumber = {0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,
    0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF},
};

const Settings_Tune SettingsTuneDef =
{
    .version = 4,
    .sct = {SCT_MARKER0,SCT_MARKER1,SCT_MARKER2},
    .signature = 0x617A34E0,
    .crc32e = 0xFFFFFFFF,
    .flags = 0,
    .veh_type = 0xFFFF,
    .comm_type = 0xFF,
    .comm_level = 0xFF,
    .oem_type = 0xFF,
    .married_count = 0,
    .married_status = 0,
    .processor_count = 0,
    .vehiclecodes[0] = 0,
    .vin[0] = 0,
    .prev_vin = {'Q','Q','Q','Q','Q','Q','Q','Q','Q','Q','Q','Q','Q','Q','Q','Q','Q',0},    //IMPORTANT: do not change this line
    .vehicle_serial_number[0] = 0,
    .epats[0] = 0,
    .vid[0] = 0,
    .preloadedlookup_crc32einfo = 0,
    .tunehistoryindex = 0,
    .tunehistoryflashcount = 0,
    .stockfilesize = 0,
    
    .tuneinfotrack =
    {
        .flashtype = 0xFF,
        .useoption = {TUNEUSEOPTION_NONE,TUNEUSEOPTION_NONE,TUNEUSEOPTION_NONE},
        .tunedescriptions = {"","",""},
        .tunefilenames = {"","",""},
        .optionfilenames = {"","",""},
        .tuneflags = 0,
        .otfinfo.reserved = {0,0},
        .otfinfo.pcm = 0,
        .otfinfo.tcm = 0,
        .otfinfo.powerlevel = 0,
    },

    .special_stockfilesize = 0,
    .special_vehicletype = INVALID_VEH_DEF,
    .recv_id_crc32e = 0,
    
    .stockwheels.tire = 0,
    .stockwheels.axel = 0,
    .customtune_vehiclecodes[0] = 0,
    .customtune_secondvehiclecodes[0] = 0,
    .secondvehiclecodes[0] = 0,

    .stockspoptsfile_crc32e = 0,
};

const Settings_Fleet SettingsFleetDef =
{
    .version = 0,
    .sct = {SCT_MARKER0,SCT_MARKER1,SCT_MARKER2},
    .signature = 0x7E191C3A,
    .crc32e = 0xFFFFFFFF,
};

const Settings_DatalogGeneral SettingsDatalogGeneralDef =
{
    .version = 1,
    .sct = {SCT_MARKER0,SCT_MARKER1,SCT_MARKER2},
    .signature = 0x617A34E0,
    .crc32e = 0xFFFFFFFF,
    .flags = 0,
    .errorpointpool.flags = 0,
    .errorpointpool.position = 0,
    .errorpointpool.looped = FALSE,
    .analog_port = 
    {   
        .ext5Vconfig = EXT5VCONFIG_AUTO,
        .gpio1config = GPIO1CONFIG_OFF,
    },
    .control = 
    {
        .demo_mode = 0,
        .reserved0 = 0,
        .reserved1 = 0,
    }
};

//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
void __settings_load_default_settings_for_demo_mode();

//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
typedef struct
{
    u32 signature[2];
    firmware_version *fwversion_ptr;
    u32 serialnumber_addr[3];
    u8  sct[16];
}version_signature;
#pragma location = ".mico"
const version_signature versionsignature =
{
    .signature[0] = 0xAA5571E8,
    .signature[1] = 0xF0A5C369,
    .fwversion_ptr = (firmware_version*)&fwversion,
    .serialnumber_addr[0] = 0,
    .serialnumber_addr[1] = (u32)(&SettingsCriticalDef.serialnumber),
    .serialnumber_addr[2] = (u32)(&SettingsCriticalDef),
    .sct = "SCT LLC",
};

//------------------------------------------------------------------------------
// Restore all settings to default values (only used by production)
// return:  u8  status
// Engineer: Quyen Leba
//------------------------------------------------------------------------------
u8 settings_setproductiondefault()
{
    u32 recv_id_crc32e;
    u8  status;

    status = bootsettings_verifycorrect();
    if(status != S_SUCCESS)
        return S_FAIL;
    
    status = obd2tune_recoverystock_validate_idfile(&recv_id_crc32e);
    if (status == S_FILENOTFOUND)
    {
        recv_id_crc32e = 0;
    }
    else if (status == S_BADCONTENT)
    {
        recv_id_crc32e = 0;
        obd2tune_recoverystock_remove_idfile();
    }
    else if (status != S_SUCCESS && status != S_CRC32E)
    {
        return S_FAIL;
    }
    
    log_tunehistory_delete();

    memcpy((char*)&settingscritical,
           (char*)&SettingsCriticalDef,sizeof(SettingsCriticalDef));
    memcpy((char*)&settingstune,
           (char*)&SettingsTuneDef,sizeof(SettingsTuneDef));
    memcpy((char*)&settingsdataloggeneral,
           (char*)&SettingsDatalogGeneralDef,sizeof(SettingsDatalogGeneralDef));

    memset((char*)settingstune.vehiclecodes,0,sizeof(settingstune.vehiclecodes));
    memset((char*)settingstune.vin,0,sizeof(settingstune.vin));
    memset((char*)settingstune.vehicle_serial_number,0,
           sizeof(settingstune.vehicle_serial_number));
    memset((char*)settingstune.epats,0,sizeof(settingstune.epats));
    memset((char*)settingstune.vid,0,sizeof(settingstune.vid));

    memset((char*)settingstune.tuneinfotrack.tunedescriptions,
           0,sizeof(settingstune.tuneinfotrack.tunedescriptions));
    memset((char*)settingstune.tuneinfotrack.tunefilenames,
           0,sizeof(settingstune.tuneinfotrack.tunefilenames));
    memset((char*)settingstune.tuneinfotrack.optionfilenames,
           0,sizeof(settingstune.tuneinfotrack.optionfilenames));

    //Note: see __settings_load_default_settings_for_demo_mode() if making changes to this function

    SETTINGS_TUNE(recv_id_crc32e) = recv_id_crc32e;

    flash_save_setting(CriticalArea,
                       (u8*)&settingscritical,sizeof(settingscritical));
    flash_save_setting(TuneArea,
                       (u8*)&settingstune,sizeof(settingstune));
    flash_save_setting(DatalogGeneralArea,
                       (u8*)&settingsdataloggeneral,sizeof(settingsdataloggeneral));

    return S_SUCCESS;
}

//------------------------------------------------------------------------------
// Similar to settings_setproductiondefault but doesn't reset:
// 1. Serial Number
// 2. Market Type
// 3. Device Type
// 4. Partnumber
// 5. Device Flags
// 6. Baudrate
// 7. Normal Operation Signature
//
// return:  u8  status
// Engineer: Quyen Leba
//------------------------------------------------------------------------------
u8 settings_restoredefault()
{
    u8  serialnumber[SETTINGS_CRITICAL_SERIALNUMBER_LENGTH];
    u8 partnumberstring[SETTINGS_CRITICAL_DEVICEPARTNUMBERSTRING_LENGTH];
    MarketType mt;
    DeviceType dt;
    u32 criticalflags;
    u32 normaloperation_signature;
    u32 baudrate;
    
    log_tunehistory_delete();

    // Backup specific settings
    criticalflags = settingscritical.flags & 0xFFFF;
    normaloperation_signature = settingscritical.normaloperation_signature;
    baudrate = settingscritical.baudrate;
    dt = (DeviceType)settingscritical.devicetype;
    mt = (MarketType)settingscritical.markettype;
    memcpy((char*)serialnumber,
           (char*)settingscritical.serialnumber,sizeof(serialnumber));
    memcpy((char*)partnumberstring,
           (char*)settingscritical.devicepartnumberstring,sizeof(partnumberstring));
    
    // Restore default settings
    settings_setproductiondefault();
    
    // Restore Backup settings
    settingscritical.flags = criticalflags;
    settingscritical.normaloperation_signature = normaloperation_signature;
    settingscritical.baudrate = baudrate;
    settingscritical.devicetype = dt;
    settingscritical.markettype = mt;
    memcpy((char*)settingscritical.serialnumber,
           (char*)serialnumber,sizeof(serialnumber));
    memcpy((char*)settingscritical.devicepartnumberstring,
           (char*)partnumberstring,sizeof(partnumberstring));
    
    // Commit to flash
    flash_save_setting(CriticalArea,
                       (u8*)&settingscritical,sizeof(settingscritical));
    
    return S_SUCCESS;
}

//------------------------------------------------------------------------------
// Activate Mico signature (switch to normal operation)
// return:  u8  status
// Engineer: Quyen Leba
//------------------------------------------------------------------------------
u8 settings_activate_mico_signature()
{
    settingscritical.normaloperation_signature = MICO_NORMAL_OPERATION_SIGNATURE;
    flash_save_setting(CriticalArea,
                       (u8*)&settingscritical,sizeof(settingscritical));
    return S_SUCCESS;
}

//------------------------------------------------------------------------------
// 
// return:  u8  status
// Engineer: Quyen Leba
//------------------------------------------------------------------------------
u8 settings_load()
{
#if (SETTINGS_DEMO_USE_FS)
    F_FILE *fptr;
    u32 bytecount;
    u8  status;
#endif

    flash_load_setting(CriticalArea,
                       (u8*)&settingscritical, sizeof(settingscritical));
    flash_load_setting(TuneArea,
                       (u8*)&settingstune, sizeof(settingstune));
    flash_load_setting(DatalogGeneralArea,
                       (u8*)&settingsdataloggeneral, sizeof(settingsdataloggeneral));

    if (SETTINGS_IsDemoMode())
    {
#if (SETTINGS_DEMO_USE_FS)
        status = S_SUCCESS;
        //load demo settings from files
        fptr = genfs_user_openfile(SETTINGS_DEMO_DATALOGGENERAL_FILENAME,"r");
        if (fptr)
        {
            bytecount = fread((char*)&settingsdataloggeneral,1,sizeof(settingsdataloggeneral),fptr);
            genfs_closefile(fptr);
            if (bytecount != sizeof(settingsdataloggeneral))
            {
                status = S_READFILE;
            }
        }
        else
        {
            status = S_OPENFILE;
        }

        if (status == S_SUCCESS)
        {
            fptr = genfs_user_openfile(SETTINGS_DEMO_TUNE_FILENAME,"r");
            if (fptr)
            {
                bytecount = fread((char*)&settingstune,1,sizeof(settingstune),fptr);
                genfs_closefile(fptr);
                if (bytecount != sizeof(settingstune))
                {
                    status = S_READFILE;
                }
            }
            else
            {
                status = S_OPENFILE;
            }
        }

        if (status != S_SUCCESS)
        {
            __settings_load_default_settings_for_demo_mode();
            SETTINGS_SetTuneAreaDirty();
            SETTINGS_SetGeneralAreaDirty();
            //since device is in demo mode at this point, settings_update() won't touch the real settings in flash
            //but use demo settings in file system
            settings_update(SETTINGS_UPDATE_IF_DIRTY);
        }
#else
        //always reset to default demo settings
        __settings_load_default_settings_for_demo_mode();
#endif
    }
    return S_SUCCESS;
}

//------------------------------------------------------------------------------
// 
// return:  u8  status
// Engineer: Quyen Leba
//------------------------------------------------------------------------------
u8 settings_validate()
{
    if (settingscritical.sct[0] != SCT_MARKER0 ||
        settingscritical.sct[1] != SCT_MARKER1 || 
        settingscritical.sct[2] != SCT_MARKER2)
    {
        return S_UNMATCH;
    }
    else if (settingstune.sct[0] != SCT_MARKER0 ||
             settingstune.sct[1] != SCT_MARKER1 || 
             settingstune.sct[2] != SCT_MARKER2)
    {
        return S_UNMATCH;
    }
    else if (settingsdataloggeneral.sct[0] != SCT_MARKER0 ||
             settingsdataloggeneral.sct[1] != SCT_MARKER1 || 
             settingsdataloggeneral.sct[2] != SCT_MARKER2)
    {
        return S_UNMATCH;
    }
    
    //TODOQ: check crc32e S_BADCONTENT or S_CRC32E
    
    return S_SUCCESS;
}

//------------------------------------------------------------------------------
// Input:   bool forced_update
// return:  u8  status
// Engineer: Quyen Leba
//------------------------------------------------------------------------------
u8 settings_update(bool forced_update)
{
#if (SETTINGS_DEMO_USE_FS)
    F_FILE *fptr;
#endif

    //TODOQ: do crc32e
    if (isdataloggeneralarea_dirty || forced_update)
    {
        isdataloggeneralarea_dirty = FALSE;
        if (SETTINGS_IsDemoMode())
        {
#if (SETTINGS_DEMO_USE_FS)
            fptr = genfs_user_openfile(SETTINGS_DEMO_DATALOGGENERAL_FILENAME,"w");
            if (fptr)
            {
                fwrite((char*)&settingsdataloggeneral,1,sizeof(settingsdataloggeneral),fptr);
                genfs_closefile(fptr);
            }
#endif
        }
        else
        {
            flash_save_setting(DatalogGeneralArea,
                               (u8*)&settingsdataloggeneral,sizeof(settingsdataloggeneral));
        }
    }
    if (istunearea_dirty || forced_update)
    {
        istunearea_dirty = FALSE;
        if (SETTINGS_IsDemoMode())
        {
#if (SETTINGS_DEMO_USE_FS)
            fptr = genfs_user_openfile(SETTINGS_DEMO_TUNE_FILENAME,"w");
            if (fptr)
            {
                fwrite((char*)&settingstune,1,sizeof(settingstune),fptr);
                genfs_closefile(fptr);
            }
#endif
        }
        else
        {
            flash_save_setting(TuneArea,
                               (u8*)&settingstune,sizeof(settingstune));
        }
    }
    if (iscriticalarea_dirty || forced_update)
    {
        iscriticalarea_dirty = FALSE;
        flash_save_setting(CriticalArea,
                           (u8*)&settingscritical,sizeof(settingscritical));
    }
    return S_SUCCESS;
}

//-----------------------------------------------------------------------------
// Set Production Test status
// Input:   u8 prodtest - production test task nunmber
//          bool teststatus - TRUE - PASS or FALSE - FAIL
// Return:  u8  status
// Engineer: Patrick Downs
//-----------------------------------------------------------------------------
u8 settings_set_productiontest(u8 prodtest, bool teststatus)
{
    u8 status;
    
    status = S_SUCCESS;
    
    if(SETTINGS_CRITICAL(productiontest_signature) != SETTINGS_CRITICAL_PRODUCTIONTESTVALID_SIG)
    {
        SETTINGS_CRITICAL(productiontest_signature) = SETTINGS_CRITICAL_PRODUCTIONTESTVALID_SIG;
        memset(&settingscritical.productiontest_status, 0, sizeof(settingscritical.productiontest_status));
    }
    
    switch(prodtest)
    {
    case 0:
        SETTINGS_CRITICAL(productiontest_status).task0_status = teststatus;
        break;
    case 1:
        SETTINGS_CRITICAL(productiontest_status).task1_status = teststatus;
        break;
    case 2:
        SETTINGS_CRITICAL(productiontest_status).task2_status = teststatus;
        break;
    case 3:
        SETTINGS_CRITICAL(productiontest_status).task3_status = teststatus;
        break;
    case 4:
        SETTINGS_CRITICAL(productiontest_status).task4_status = teststatus;
        break;
    case 5:
        SETTINGS_CRITICAL(productiontest_status).task5_status = teststatus;
        break;
    case 6:
        SETTINGS_CRITICAL(productiontest_status).task6_status = teststatus;
        break;
    case 7:
        SETTINGS_CRITICAL(productiontest_status).task7_status = teststatus;
        break;
    case 8:
        SETTINGS_CRITICAL(productiontest_status).task8_status = teststatus;
        break;
    case 9:
        SETTINGS_CRITICAL(productiontest_status).task9_status = teststatus;
        if((settingscritical.productiontest_status.task0_status &&
            settingscritical.productiontest_status.task1_status &&
            settingscritical.productiontest_status.task2_status &&
            settingscritical.productiontest_status.task3_status &&
            settingscritical.productiontest_status.task4_status &&
            settingscritical.productiontest_status.task5_status &&
            settingscritical.productiontest_status.task6_status &&
#if USE_CC3000_WIFI
            settingscritical.productiontest_status.task7_status &&
#endif
            settingscritical.productiontest_status.task8_status &&
            settingscritical.productiontest_status.task9_status) == TRUE)
        {
            // All tests passed
            SETTINGS_CRITICAL(productiontest_status).complete_status = TRUE;
        }
        else
        {
            SETTINGS_CRITICAL(productiontest_status).complete_status = FALSE;
        }
        SETTINGS_SetCriticalAreaDirty();
        status = settings_update(SETTINGS_UPDATE_IF_DIRTY);
        break;
    default:
        status = S_FAIL;
        break; 
    }
    
    return status;
}

//-----------------------------------------------------------------------------
// Get Production Test Status
// Input:   none
// Return:  u32 *retstatus - production test status
//          u8  status
// Engineer: Patrick Dowms
//-----------------------------------------------------------------------------
u8 settings_get_productiontest(u32 *retstatus)
{
    u8 status;
    
    if(SETTINGS_CRITICAL(productiontest_signature) == SETTINGS_CRITICAL_PRODUCTIONTESTVALID_SIG)
    {
        *retstatus = *((u32*)&settingscritical.productiontest_status);
        status = S_SUCCESS;
    }
    else // No signature, never been set
    {
        *retstatus = 0; 
        status = S_FAIL;
    }
    
    return status;
}

//-----------------------------------------------------------------------------
// Get ProgrammerInfoP2 flags
// Return:  u16 flags
// Engineer: Quyen Leba
//-----------------------------------------------------------------------------
u16 settings_get_programmerinfop2_flags()
{
    u16 flags;
    flags = (SETTINGS_CRITICAL(flags) & 0xFFFF);
    if (SETTINGS_IsPreloadedTuneDisabled())
    {
        flags |= SettingsDeviceFlags_NoPreloadedTuneSupport;
    }
    return flags;
}

//-----------------------------------------------------------------------------
// Set ProgrammerInfoP2 flags
// Input:   u16 flags
// Return:  u8  status
// Engineer: Quyen Leba
//-----------------------------------------------------------------------------
u8 settings_set_programmerinfop2_flags(u16 flags)
{
    if ((SETTINGS_CRITICAL(flags) & 0xFFFF) != flags)
    {
        SETTINGS_CRITICAL(flags) &= 0xFFFF0000;
        SETTINGS_CRITICAL(flags) |= (u32)(flags & 0xFFFF);
        SETTINGS_SetCriticalAreaDirty();
        return settings_update(SETTINGS_UPDATE_IF_DIRTY);
    }
    return S_SUCCESS;
}

//-----------------------------------------------------------------------------
// Get settings by opcode
// Input:   SETTINGS_OPCODE opcode
//          u16 privdata (specific to opcode)
// Outputs: u8  *settings_data
//          u32 settings_len
// Return:  u8  status
// Engineer: Quyen Leba
//-----------------------------------------------------------------------------
u8 settings_getsettings_byopcode(SETTINGS_OPCODE opcode, u16 privdata,
                                  u8 *settings_data, u32 *settings_len)
{
    SettingsProgrammerInfoP2 *p2info;
    u8  *bptr;
    u16 tmp_u16;
    u32 status;
    
    status = S_SUCCESS;
    *settings_len = 0;
    bptr = NULL;
    
    switch(opcode)
    {
    case SETTINGS_OPCODE_FLAGS:
        *settings_len = 4;
        switch((SettingsRegion)privdata)
        {
        case SettingsRegion_Critical:
            memcpy((char*)settings_data,
                   (char*)&SETTINGS_CRITICAL(flags),4);
            break;
        case SettingsRegion_Tune:
            memcpy((char*)settings_data,
                   (char*)&SETTINGS_TUNE(flags),4);
            break;
        case SettingsRegion_Fleet:
            memcpy((char*)settings_data,
                   (char*)&SETTINGS_FLEET(flags),4);
            break;
        case SettingsRegion_DatalogGeneral:
            memcpy((char*)settings_data,
                   (char*)&SETTINGS_GENERAL(flags),4);
            break;
        default:
            *settings_len = 0;
            status = S_INPUT;
        }
        break;
    case SETTINGS_OPCODE_VERSION:
        version_getfirmwareversion_string(settings_data);
        *settings_len = strlen((char*)settings_data)+1;
        break;
    case SETTINGS_OPCODE_SERIAL:
        strncpy((char*)settings_data,
                (char*)SETTINGS_CRITICAL(serialnumber),13+3);
        settings_data[16] = NULL;
        *settings_len = 13+3+1;
        break;
    case SETTINGS_OPCODE_DEVICE_INFO:
        memcpy((char*)&settings_data[0],
               (char*)&SETTINGS_CRITICAL(firmware_version),4);
        memcpy((char*)&settings_data[4],
               (char*)&SETTINGS_CRITICAL(devicetype),4);
        memcpy((char*)&settings_data[8],
               (char*)&SETTINGS_CRITICAL(markettype),4);
        memcpy((char*)&settings_data[12],
               (char*)&SETTINGS_CRITICAL(baudrate),4);
        *settings_len = 16;
        break;
    //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    case SETTINGS_OPCODE_PROGRAMMER_INFO_P1:
//        settings_data[0] = SETTINGS_TUNE(married_status);
//        settings_data[1] = SETTINGS_IsDownloadFail();
//        settings_data[2] = 0x00;    //reserved
//        //TODO: need settings for oem and level
//        settings_data[3] = SETTINGS_TUNE(oem_type);
//        settings_data[4] = SETTINGS_TUNE(comm_type);
//        settings_data[5] = SETTINGS_TUNE(comm_level);
//        settings_data[6] = SETTINGS_TUNE(veh_type) & 0xFF;
//        settings_data[7] = SETTINGS_TUNE(veh_type) >> 8;
//        memcpy((char*)&settings_data[8],(char*)&SETTINGS_TUNE(stockfilesize),4);
//        *settings_len = 12;

        //TODOQ: don't use this SETTINGS_OPCODE_PROGRAMMER_INFO_P1
        *settings_len = 0;
        status = S_NOTSUPPORT;
        break;
    case SETTINGS_OPCODE_PROGRAMMER_INFO_P2:
        p2info = (SettingsProgrammerInfoP2*)settings_data;
        
        p2info->married_count = SETTINGS_TUNE(married_count);
        p2info->married_status = SETTINGS_TUNE(married_status);
        p2info->reserved1 = 0;
        p2info->oem_type = SETTINGS_TUNE(oem_type);
        p2info->comm_type = SETTINGS_TUNE(comm_level);
        p2info->comm_level = SETTINGS_TUNE(comm_type);
        p2info->processor_count = SETTINGS_TUNE(processor_count);
        p2info->flags = settings_get_programmerinfop2_flags();
        p2info->veh_type = SETTINGS_TUNE(veh_type);
        p2info->stockfilesize = SETTINGS_TUNE(stockfilesize);

        *settings_len = sizeof(SettingsProgrammerInfoP2);
        break;
    case SETTINGS_OPCODE_PROGRAMMER_INFO_P3:
        *settings_len = 0;
        status = S_NOTSUPPORT;
        break;
    case SETTINGS_OPCODE_PROGRAMMER_INFO_P4:
        *settings_len = 0;
        status = S_NOTSUPPORT;
        break;
    //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    case SETTINGS_OPCODE_PRELOADEDTUNE_LOOKUP_CRC32E:
        *(u32*)settings_data = SETTINGS_TUNE(preloadedlookup_crc32einfo);
        *settings_len = 4;
        break;
    case SETTINGS_OPCODE_PRELOADEDTUNE_RESTRICTION:
        *(u32*)settings_data = SETTINGS_TUNE(flags) & PRELOADEDTUNE_RESTRICTION_MASK;
        *settings_len = 4;
        break;
    case SETTINGS_OPCODE_DEMO_MODE:
        if (SETTINGS_IsDemoMode())
        {
            *(u32*)settings_data = 1;
        }
        else
        {
            *(u32*)settings_data = 0;
        }
        *settings_len = 4;
        break;
    case SETTINGS_OPCODE_TUNE_REVISION:
        status = obd2tune_gettunerevision(settings_data);
        *settings_len = 0;
        if (status == S_SUCCESS)
        {
            *settings_len = strlen((char*)settings_data);
        }
        break;
    case SETTINGS_OPCODE_DEVICE_PARTNUMBER_STRING:
        strcpy((char*)settings_data,"N/A");
        if (isgraph(SETTINGS_CRITICAL(devicepartnumberstring)[0]))
        {
            if (strlen((char*)SETTINGS_CRITICAL(devicepartnumberstring)) <
                sizeof(SETTINGS_CRITICAL(devicepartnumberstring)))
            {
                strcpy((char*)settings_data,
                       (char*)SETTINGS_CRITICAL(devicepartnumberstring));
            }
        }
        *settings_len = strlen((char*)settings_data)+1;
        break;
    case SETTINGS_OPCODE_DEVICE_VERSION_STRING:
        status = version_getfirmwareversion_string(settings_data);
        *settings_len = strlen((char*)settings_data)+1;
        break;
    case SETTINGS_OPCODE_DEVICE_TYPE:
        memcpy((char*)settings_data,(char*)&SETTINGS_CRITICAL(devicetype),4);
        *settings_len = 4;
        break;
    case SETTINGS_OPCODE_MARKET_TYPE:
        memcpy((char*)settings_data,(char*)&SETTINGS_CRITICAL(markettype),4);
        *settings_len = 4;
        break;
    case SETTINGS_OPCODE_DEVICE_PROPERTIES:
        if (privdata == 0)
        {
            status = properties_mb_getinfo(settings_data,settings_len);
        }
        else if (privdata == 1)
        {
            status = properties_vb_getinfo(settings_data,settings_len);
        }
        else if (privdata == 2)
        {
            status = properties_ab_getinfo(settings_data,settings_len);
        }
        else
        {
            status = S_NOTSUPPORT;
        }
        break;
    //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    case SETTINGS_OPCODE_STOCKFILESIZE:
        memcpy((char*)settings_data,(char*)&SETTINGS_TUNE(stockfilesize),4);
        *settings_len = 4;
        break;
    case SETTINGS_OPCODE_VID:
        if (privdata < sizeof(SETTINGS_TUNE(vid))/32)
        {
            memcpy((char*)settings_data,
                   (char*)&SETTINGS_TUNE(vid)[privdata*32],32);
            *settings_len = 32;
        }
        else
        {
            status = S_INPUT;
        }
        break;
    case SETTINGS_OPCODE_EPATS:
        memcpy((char*)settings_data,
               (char*)SETTINGS_TUNE(epats),sizeof(SETTINGS_TUNE(epats)));
        *settings_len = sizeof(SETTINGS_TUNE(epats));
        break;
    case SETTINGS_OPCODE_VEHICLE_CODES:
        if (privdata < sizeof(SETTINGS_TUNE(vehiclecodes))/32)
        {
            memcpy((char*)settings_data,
                   (char*)&SETTINGS_TUNE(vehiclecodes)[privdata*32],32);
            *settings_len = 32;
        }
        else
        {
            status = S_INPUT;
        }
        break;
    case SETTINGS_OPCODE_VEHICLE_SERIAL_NUMBER:
        memcpy((char*)settings_data,
               (char*)SETTINGS_TUNE(vehicle_serial_number),
               sizeof(SETTINGS_TUNE(vehicle_serial_number)));
        *settings_len = sizeof(SETTINGS_TUNE(vehicle_serial_number));
        break;
    case SETTINGS_OPCODE_VIN:
        memcpy((char*)settings_data,
               (char*)SETTINGS_TUNE(vin),sizeof(SETTINGS_TUNE(vin)));
        *settings_len = sizeof(SETTINGS_TUNE(vin));
        break;
    case SETTINGS_OPCODE_PREV_VIN:
        memcpy((char*)settings_data,
               (char*)SETTINGS_TUNE(prev_vin),sizeof(SETTINGS_TUNE(prev_vin)));
        *settings_len = sizeof(SETTINGS_TUNE(prev_vin));
        break;
    case SETTINGS_OPCODE_TUNEINFOTRACK:
        if (privdata < sizeof(SETTINGS_TUNE(tuneinfotrack))/32)
        {
            bptr = (u8*)&SETTINGS_TUNE(tuneinfotrack);
            memcpy((char*)settings_data,
                   (char*)&bptr[privdata*32],32);
            *settings_len = 32;
        }
        else
        {
            status = S_INPUT;
        }
        break;
    case SETTINGS_OPCODE_TUNEHISTORYINFO:
        status = S_NOTSUPPORT;  //TODOQ:
        break;
    //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    case SETTINGS_OPCODE_DEVICE_CRITICAL_FLAGS:
        memcpy((char*)settings_data,
               (char*)&SETTINGS_CRITICAL(flags),sizeof(SETTINGS_CRITICAL(flags)));
        *settings_len = sizeof(SETTINGS_CRITICAL(flags));
        break;
    case SETTINGS_OPCODE_DEVICE_TUNE_FLAGS:
        memcpy((char*)settings_data,
               (char*)&SETTINGS_TUNE(flags),sizeof(SETTINGS_TUNE(flags)));
        *settings_len = sizeof(SETTINGS_TUNE(flags));
        break;
    case SETTINGS_OPCODE_DEVICE_FLEET_FLAGS:
        memcpy((char*)settings_data,
               (char*)&SETTINGS_FLEET(flags),sizeof(SETTINGS_FLEET(flags)));
        *settings_len = sizeof(SETTINGS_FLEET(flags));
        break;
    case SETTINGS_OPCODE_DEVICE_DATALOG_GENERAL_FLAGS:
        memcpy((char*)settings_data,
               (char*)&SETTINGS_GENERAL(flags),sizeof(SETTINGS_GENERAL(flags)));
        *settings_len = sizeof(SETTINGS_GENERAL(flags));
        break;
    //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    case SETTINGS_OPCODE_ERRORPOOL:
        //privdata is ErrorPointPoolSource
        bptr = __malloc(LOG_MAX_ERROR_POINT_COUNT*5*2);
        if (bptr)
        {
            if (log_get_error_point_list_string((ErrorPointPoolSource)privdata,
                                                bptr,&tmp_u16) == S_SUCCESS)
            {
                tmp_u16 = strlen((char*)bptr);
                if (tmp_u16 < 48)
                {
                    strcpy((char*)settings_data,(char*)bptr);
                    *settings_len = tmp_u16+1;
                }
                else
                {
                    memcpy((char*)settings_data,(char*)bptr,48);
                    settings_data[48] = NULL;
                    *settings_len = 49;
                }
            }
            __free(bptr);
            bptr = NULL;
        }
        else
        {
            status = S_MALLOC;
        }
        break;
   //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    case SETTINGS_OPCODE_ANALOGPORT_EXT5V:
        settings_data[0] = SETTINGS_GENERAL(analog_port.ext5Vconfig);
        *settings_len = sizeof(SETTINGS_GENERAL(analog_port));
        break;
    case SETTINGS_OPCODE_ANALOGPORT_GPIO1:
        settings_data[0] = SETTINGS_GENERAL(analog_port.gpio1config);
        *settings_len = sizeof(SETTINGS_GENERAL(analog_port));
        break;        
   //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    case SETTINGS_OPCODE_PRODUCTIONTEST_STATUS:
        status = settings_get_productiontest((u32*)settings_data);
        *settings_len = 4;
        break;
        
    default:
        status = S_SERVICENOTSUPPORTED;
        break;
    }
    
    return status;
}

//-----------------------------------------------------------------------------
// Set settings by opcode
// Inputs:  SETTINGS_OPCODE opcode
//          u16 privdata (specific to opcode)
//          u8  *settings_data
//          u32 settings_len
// Return:  u8  status
// Engineer: Quyen Leba
//-----------------------------------------------------------------------------
u8 settings_setsettings_byopcode(SETTINGS_OPCODE opcode, u16 privdata,
                                  u8 *settings_data, u32 settings_len)
{
    SettingsProgrammerInfoP2 *p2info;
    u8  status;
    
    status = S_BADCONTENT;

    switch(opcode)
    {
    case SETTINGS_OPCODE_FLAGS:
        if (settings_len == 4)
        {
            switch((SettingsRegion)privdata)
            {
            case SettingsRegion_Critical:
                memcpy((char*)&SETTINGS_CRITICAL(flags),
                       (char*)settings_data,4);
                SETTINGS_SetCriticalAreaDirty();
                status = S_SUCCESS;
                break;
            case SettingsRegion_Tune:
                memcpy((char*)&SETTINGS_TUNE(flags),
                       (char*)settings_data,4);
                SETTINGS_SetTuneAreaDirty();
                status = S_SUCCESS;
                break;
            case SettingsRegion_Fleet:
                memcpy((char*)&SETTINGS_FLEET(flags),
                       (char*)settings_data,4);
                SETTINGS_SetFleetAreaDirty();
                status = S_SUCCESS;
                break;
            case SettingsRegion_DatalogGeneral:
                memcpy((char*)&SETTINGS_GENERAL(flags),
                       (char*)settings_data,4);
                SETTINGS_SetGeneralAreaDirty();
                status = S_SUCCESS;
                break;
            default:
                status = S_INPUT;
            }
        }
        break;
    case SETTINGS_OPCODE_VERSION:
        status = S_NOTSUPPORT;  //TODOQ:
        break;
    case SETTINGS_OPCODE_SERIAL:
        if (settings_len == 13 || settings_len == 16)
        {
            memset((char*)SETTINGS_CRITICAL(serialnumber),
                   0x00,sizeof(SETTINGS_CRITICAL(serialnumber)));
            memcpy((char*)SETTINGS_CRITICAL(serialnumber),
                   (char*)settings_data,settings_len);
            SETTINGS_SetCriticalAreaDirty();
            status = S_SUCCESS;
        }
        break;
    case SETTINGS_OPCODE_DEVICE_INFO:
        if (settings_len == 16)
        {
            memcpy((char*)(char*)&SETTINGS_CRITICAL(firmware_version),&settings_data[0],4);
            memcpy((char*)&SETTINGS_CRITICAL(devicetype),(char*)&settings_data[4],4);
            memcpy((char*)&SETTINGS_CRITICAL(markettype),(char*)&settings_data[8],4);
            memcpy((char*)&SETTINGS_CRITICAL(baudrate),(char*)&settings_data[12],4);
            SETTINGS_SetCriticalAreaDirty();
            status = S_SUCCESS;
        }
        break;
    //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    case SETTINGS_OPCODE_PROGRAMMER_INFO_P1:
//        if (settings_len == 12)
//        {
//            SETTINGS_TUNE(married_status) = settings_data[0];
//            SETTINGS_ClearDownloadFail();
//            if (settings_data[1])
//            {
//                SETTINGS_SetDownloadFail();
//            }
//            //settings_data[2]: reserved
//            SETTINGS_TUNE(oem_type) = settings_data[3];
//            SETTINGS_TUNE(comm_type) = settings_data[4];
//            SETTINGS_TUNE(comm_level) = settings_data[5];
//            memcpy((char*)&SETTINGS_TUNE(veh_type),(char*)&settings_data[6],2);
//            memcpy((char*)&SETTINGS_TUNE(stockfilesize),(char*)&settings_data[8],4);
//            SETTINGS_SetTuneAreaDirty();
//            status = S_SUCCESS;
//        }
        status = S_NOTSUPPORT;
        break;
    case SETTINGS_OPCODE_PROGRAMMER_INFO_P2:
        if (settings_len == sizeof(SettingsProgrammerInfoP2))
        {
            p2info = (SettingsProgrammerInfoP2*)settings_data;

            //Note: p2info->flags has no effect here
            SETTINGS_TUNE(married_count) = p2info->married_count;
            SETTINGS_TUNE(married_status) = p2info->married_status;
            SETTINGS_TUNE(oem_type) = p2info->oem_type;
            SETTINGS_TUNE(comm_level) = p2info->comm_type;
            SETTINGS_TUNE(comm_type) = p2info->comm_level;
            SETTINGS_TUNE(processor_count) = p2info->processor_count;
            SETTINGS_TUNE(veh_type) = p2info->veh_type;
            SETTINGS_TUNE(stockfilesize) = p2info->stockfilesize;

            SETTINGS_SetTuneAreaDirty();
            status = S_SUCCESS;
        }
        break;
    case SETTINGS_OPCODE_PROGRAMMER_INFO_P3:
        status = S_NOTSUPPORT;
        break;
    case SETTINGS_OPCODE_PROGRAMMER_INFO_P4:
        status = S_NOTSUPPORT;
        break;
    case SETTINGS_OPCODE_PRELOADEDTUNE_LOOKUP_CRC32E:
        if (settings_len == 4)
        {
            memcpy(&SETTINGS_TUNE(preloadedlookup_crc32einfo),settings_data,4);
            SETTINGS_SetTuneAreaDirty();
            status = S_SUCCESS;
        }
        break;
    case SETTINGS_OPCODE_PRELOADEDTUNE_RESTRICTION:
        if (settings_len == 4)
        {
            status = obd2tune_preloaded_tune_set_restriction(*(u32*)settings_data);
        }
        break;
    case SETTINGS_OPCODE_DEMO_MODE:
        if (settings_len == 4)
        {
            switch(*(u32*)settings_data)
            {
            case 0:             //disable demo mode
                if (SETTINGS_IsDemoMode())
                {
                    status = settings_unregister_demo_mode();
                    obd2_close();
                }
                break;
            case 0xB31AC862:    //enable demo mode
                if (!SETTINGS_IsDemoMode())
                {
                    //only allow if device unmarried
                    if (!SETTINGS_IsMarried())
                    {
                        status = settings_register_demo_mode();
                        obd2_close();
                        return status;
                    }
                    else
                    {
                        return S_MARRIED;
                    }
                }
                else
                {
                    //already in demo mode
                    status = S_SUCCESS;
                }
                break;
            default:
                status = S_UNMATCH;
                break;
            }
        }
        break;
    //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    case SETTINGS_OPCODE_STOCKFILESIZE:
        if (settings_len == 4)
        {
            memcpy((char*)&SETTINGS_TUNE(stockfilesize),(char*)settings_data,4);
            SETTINGS_SetTuneAreaDirty();
            status = S_SUCCESS;
        }
        break;
    case SETTINGS_OPCODE_VID:
        if (privdata < sizeof(SETTINGS_TUNE(vid))/32 && settings_len == 32)
        {
            memcpy((char*)&SETTINGS_TUNE(vid)[privdata*32],
                   (char*)settings_data,32);
            SETTINGS_SetTuneAreaDirty();
            status = S_SUCCESS;
        }
        break;
    case SETTINGS_OPCODE_EPATS:
        if (settings_len == sizeof(SETTINGS_TUNE(epats)))
        {
            memcpy((char*)SETTINGS_TUNE(epats),
                   (char*)settings_data,sizeof(SETTINGS_TUNE(epats)));
            SETTINGS_SetTuneAreaDirty();
            status = S_SUCCESS;
        }
        break;
    case SETTINGS_OPCODE_VEHICLE_CODES:
        if (privdata < sizeof(SETTINGS_TUNE(vehiclecodes))/32 &&
            settings_len == 32)
        {
            memcpy((char*)&SETTINGS_TUNE(vehiclecodes)[privdata*32],
                   (char*)settings_data,32);
            SETTINGS_SetTuneAreaDirty();
            status = S_SUCCESS;
        }
        break;
    case SETTINGS_OPCODE_VEHICLE_SERIAL_NUMBER:
        if (settings_len == sizeof(SETTINGS_TUNE(vehicle_serial_number)))
        {
            memcpy((char*)SETTINGS_TUNE(vehicle_serial_number),
                   (char*)settings_data,
                   sizeof(SETTINGS_TUNE(vehicle_serial_number)));
            SETTINGS_SetTuneAreaDirty();
            status = S_SUCCESS;
        }
        break;
    case SETTINGS_OPCODE_VIN:
        if (settings_len == sizeof(SETTINGS_TUNE(vin)))
        {
            memcpy((char*)SETTINGS_TUNE(vin),
                   (char*)settings_data,sizeof(SETTINGS_TUNE(vin)));
            SETTINGS_SetTuneAreaDirty();
            status = S_SUCCESS;
        }
        break;
    case SETTINGS_OPCODE_PREV_VIN:
        if (settings_len == sizeof(SETTINGS_TUNE(prev_vin)))
        {
            memcpy((char*)SETTINGS_TUNE(prev_vin),
                   (char*)settings_data,sizeof(SETTINGS_TUNE(prev_vin)));
            SETTINGS_SetTuneAreaDirty();
            status = S_SUCCESS;
        }
        break;
    case SETTINGS_OPCODE_TUNEINFOTRACK:
        if (privdata < sizeof(SETTINGS_TUNE(tuneinfotrack))/32 &&
            settings_len == 32)
        {
            u8  *bptr = (u8*)&SETTINGS_TUNE(tuneinfotrack);
            memcpy((char*)&bptr[privdata*32],(char*)settings_data,32);
            SETTINGS_SetTuneAreaDirty();
            status = S_SUCCESS;
        }
        break;
    case SETTINGS_OPCODE_TUNEHISTORYINFO:
        status = S_NOTSUPPORT;  //TODOQ:
        break;
    //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    case SETTINGS_OPCODE_TUNE_REVISION:
        status = S_NOTSUPPORT;  //TODOQ: want to do this?
        break;
    case SETTINGS_OPCODE_DEVICE_PARTNUMBER_STRING:
        if (settings_len < sizeof(SETTINGS_CRITICAL(devicepartnumberstring)))
        {
            memcpy((char*)SETTINGS_CRITICAL(devicepartnumberstring),
                   (char*)settings_data,settings_len);
            SETTINGS_CRITICAL(devicepartnumberstring)[settings_len] = NULL;
            SETTINGS_SetCriticalAreaDirty();
            status = S_SUCCESS;
        }
        break;
    case SETTINGS_OPCODE_DEVICE_VERSION_STRING:
        status = S_NOTSUPPORT;  //not allowed
        break;
    case SETTINGS_OPCODE_DEVICE_TYPE:
        if (settings_len == 4)
        {
            memcpy((char*)&SETTINGS_CRITICAL(devicetype),
                   (char*)settings_data,4);
            SETTINGS_SetCriticalAreaDirty();
            status = S_SUCCESS;
        }
        break;
    case SETTINGS_OPCODE_MARKET_TYPE:
        if (settings_len == 4)
        {
            memcpy((char*)&SETTINGS_CRITICAL(markettype),
                   (char*)settings_data,4);
            SETTINGS_SetCriticalAreaDirty();
            status = S_SUCCESS;
        }
        break;
    case SETTINGS_OPCODE_INIT_SETTINGS:
        if (settings_len == 8)
        {
            if (*(u32*)settings_data == 0x6193E51C &&
                *(u32*)&settings_data[4] == 0xA61C82E1)
            {
                status = settings_setproductiondefault();
            }
        }
        break;
    case SETTINGS_OPCODE_RESET_SETTINGS:
        if (settings_len == 8)
        {
            if (*(u32*)settings_data == 0x52B2839C &&
                *(u32*)&settings_data[4] == 0x1C462EA5)
            {
                //keep serial number
                status = settings_restoredefault();
            }
        }
        break;
    //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    case SETTINGS_OPCODE_DEVICE_CRITICAL_FLAGS:
        if (settings_len == 12)
        {
            u32 newflags;

            if (*(u32*)settings_data == 0x72C1FA5D &&
                *(u32*)&settings_data[4] == 0xC5A39326)
            {
                newflags = *(u32*)&settings_data[8];
                if (newflags == SETTINGS_CRITICAL(flags))
                {
                    status = S_SUCCESS;
                }
                else
                {
                    if (SETTINGS_Is50StateLegalDevice() &&
                        ((newflags & SettingsDeviceFlags_50StateLegal) == 0 ||
                         (newflags & SettingsDeviceFlags_NoCustomTuneSupport) == 0))
                    {
                        //do not allow this device to change from 50StateLegal
                        status = S_INVALIDSETTINGS;
                    }
                    else
                    {
                        SETTINGS_CRITICAL(flags) = newflags;
                        SETTINGS_SetCriticalAreaDirty();
                        status = S_SUCCESS;
                    }
                }
            }
        }
        break;
    case SETTINGS_OPCODE_DEVICE_TUNE_FLAGS:
        if (settings_len == 12)
        {
            if (*(u32*)settings_data == 0xB2A57C46 &&
                *(u32*)&settings_data[4] == 0x8632A3F0)
            {
                SETTINGS_TUNE(flags) = *(u32*)&settings_data[8];
                SETTINGS_SetTuneAreaDirty();
                status = S_SUCCESS;
            }
        }
        break;
    case SETTINGS_OPCODE_DEVICE_FLEET_FLAGS:
        if (settings_len == 12)
        {
            if (*(u32*)settings_data == 0x23B26038 &&
                *(u32*)&settings_data[4] == 0xAC7C8997)
            {
                SETTINGS_FLEET(flags) = *(u32*)&settings_data[8];
                SETTINGS_SetFleetAreaDirty();
                status = S_SUCCESS;
            }
        }
        break;
    case SETTINGS_OPCODE_DEVICE_DATALOG_GENERAL_FLAGS:
        if (settings_len == 12)
        {
            if (*(u32*)settings_data == 0x8AC19D93 &&
                *(u32*)&settings_data[4] == 0x9705C0EA)
            {
                SETTINGS_GENERAL(flags) = *(u32*)&settings_data[8];
                SETTINGS_SetGeneralAreaDirty();
                status = S_SUCCESS;
            }
        }
        break;
    //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    case SETTINGS_OPCODE_ANALOGPORT_EXT5V:
        if (settings_len == 1)
        {
            SETTINGS_GENERAL(analog_port.ext5Vconfig) = *(u8*)&settings_data[0];
            SETTINGS_SetGeneralAreaDirty();
            status = S_SUCCESS;
        }
        break;
    case SETTINGS_OPCODE_ANALOGPORT_GPIO1:
        if (settings_len == 1)
        {
            SETTINGS_GENERAL(analog_port.gpio1config) = *(u8*)&settings_data[0];
            SETTINGS_SetGeneralAreaDirty();
            status = S_SUCCESS;
        }
        break;        
    //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    default:
        status = S_SERVICENOTSUPPORTED;
        break;
    }
    
    if (status == S_SUCCESS)
    {
        status = settings_update(SETTINGS_UPDATE_IF_DIRTY);
    }
    return status;
}

////------------------------------------------------------------------------------
//// Set bootloader type
//// Input:   BootloaderType bootloadertype
//// Return   u8  status
//// Engineer: Patrick Downs, Quyen Leba
//// Note: obsolete
////------------------------------------------------------------------------------
//u8 settings_set_bootloader_type(BootloaderType bootloadertype)
//{
//
//    u8  status;
//    
//    
//    
//    status = flash_save_bootloader_signature(bootloadertype);
//    if (status != S_SUCCESS)
//    {
//        return status;
//    }
//    
//    return status;
//}

//------------------------------------------------------------------------------
// Get Device Serial Number
// Output:  u8  *serialnumber
//              (must be able to store DEVICE_SERIAL_NUMBER_LENGTH+1 bytes)
// Return   u8  status
// Engineer: Patrick Downs, Quyen Leba
//------------------------------------------------------------------------------
u8 settings_get_deviceserialnumber(u8 *serialnumber)
{
    u8  i;
    
    for(i=0;i<DEVICE_SERIAL_NUMBER_LENGTH;i++)
    {
        //isalnum
        if ((SETTINGS_CRITICAL(serialnumber)[i] >= '0' &&
             SETTINGS_CRITICAL(serialnumber)[i] <= '9') ||
            (SETTINGS_CRITICAL(serialnumber)[i] >= 'A' &&
             SETTINGS_CRITICAL(serialnumber)[i] <= 'Z'))
        {
            //do nothing
        }
        else
        {
            strcpy((char*)serialnumber,"N/A");
            return S_BADCONTENT;
        }
    }
    
    memcpy((char*)serialnumber,(char*)SETTINGS_CRITICAL(serialnumber),
           DEVICE_SERIAL_NUMBER_LENGTH);
    serialnumber[DEVICE_SERIAL_NUMBER_LENGTH] = NULL;
    return S_SUCCESS;
}

//------------------------------------------------------------------------------
// Compare Device Serial Number
// Input:   u8  *serialnumber
// Return   u8  status
// Engineer: Quyen Leba
//------------------------------------------------------------------------------
u8 settings_compare_deviceserialnumber(u8 *serialnumber)
{
    if (strncmp((char*)serialnumber,
                (char*)SETTINGS_CRITICAL(serialnumber),13) == 0)
    {
        return S_SUCCESS;
    }
    return S_UNMATCH;
}

//------------------------------------------------------------------------------
// Get Device PartNumber String
// Output:  u8  *devicepartnumberstring
//              (min length: SETTINGS_CRITICAL_DEVICEPARTNUMBERSTRING_LENGTH)
// Return   u8  status
// Engineer: Patrick Downs, Quyen Leba
//------------------------------------------------------------------------------
u8 settings_get_devicepartnumberstring(u8 *devicepartnumberstring)
{
    memcpy((char*)devicepartnumberstring,
           (char*)SETTINGS_CRITICAL(devicepartnumberstring),
           SETTINGS_CRITICAL_DEVICEPARTNUMBERSTRING_LENGTH);
    return S_SUCCESS;
}

//------------------------------------------------------------------------------
// Compare Device Part Number String
// Input:   u8  *devicepartnumberstring
// Return   u8  status
// Engineer: Quyen Leba
//------------------------------------------------------------------------------
u8 settings_compare_devicepartnumberstring(u8 *devicepartnumberstring)
{
    if (strcmp((char*)devicepartnumberstring,
               (char*)SETTINGS_CRITICAL(devicepartnumberstring)) == 0)
    {
        return S_SUCCESS;
    }
    return S_UNMATCH;
}

//------------------------------------------------------------------------------
// Compare Device Serial Number
// Inputs:  u32 stockfilesize
//          u32 filestock_crc32e
//          u32 filestock_option_crc32e
//          u16 veh_type
// Return   u8  status
// Engineer: Quyen Leba
//------------------------------------------------------------------------------
u8 settings_recoverystock_set(u32 stockfilesize,
                              u32 filestock_crc32e, u32 filestock_option_crc32e,
                              u16 veh_type, u8 variantid)
{
    SETTINGS_SetMarried();
    SETTINGS_SetDownloadFail();
    SETTINGS_SetRecoveryStock();
    SETTINGS_TUNE(veh_type) = veh_type;    
    SETTINGS_TUNE(processor_count) = VEH_GetEcmCount(veh_type);
    
    SETTINGS_TUNE(stockfilesize) = stockfilesize;
    SETTINGS_TUNE(stockfile_crc32e) = filestock_crc32e;
    SETTINGS_TUNE(stockspoptsfile_crc32e) = filestock_option_crc32e;
    /*Variant ID needs to be set for 2013 and newer GPEC2 and 2A processors*/
    SETTINGS_TUNE(secondvehiclecodes)[0] = variantid;
       
    SETTINGS_SetTuneAreaDirty();
    return settings_update(SETTINGS_UPDATE_IF_DIRTY);
}

//------------------------------------------------------------------------------
// Get unlock tea key
// Input:   u32 markettype
// Output:  u32 *key (i.e. u32 key[4])
// Return   u8  status
// Engineer: Quyen Leba
//------------------------------------------------------------------------------
u8 settings_get_unlock_tea_key(u32 markettype, u32 *key)
{
    switch(markettype)
    {
    case MarketType_US:
        key[0] = TEA_KEY_0_M(US);
        key[1] = TEA_KEY_1_M(US);
        key[2] = TEA_KEY_2_M(US);
        key[3] = TEA_KEY_3_M(US);
        break;
    case MarketType_AU:
        key[0] = TEA_KEY_0_M(AU);
        key[1] = TEA_KEY_1_M(AU);
        key[2] = TEA_KEY_2_M(AU);
        key[3] = TEA_KEY_3_M(AU);
        break;
    case MarketType_UK:
        key[0] = TEA_KEY_0_M(UK);
        key[1] = TEA_KEY_1_M(UK);
        key[2] = TEA_KEY_2_M(UK);
        key[3] = TEA_KEY_3_M(UK);
        break;
    case MarketType_EO:
        key[0] = TEA_KEY_0_M(EO);
        key[1] = TEA_KEY_1_M(EO);
        key[2] = TEA_KEY_2_M(EO);
        key[3] = TEA_KEY_3_M(EO);
        break;
    case MarketType_CN:
        key[0] = TEA_KEY_0_M(CN);
        key[1] = TEA_KEY_1_M(CN);
        key[2] = TEA_KEY_2_M(CN);
        key[3] = TEA_KEY_3_M(CN);
        break;
    default:
        key[0] = TEA_KEY_0_M(US);
        key[1] = TEA_KEY_1_M(US);
        key[2] = TEA_KEY_2_M(US);
        key[3] = TEA_KEY_3_M(US);        
        //return S_NOTSUPPORT;
        break;
    }
    return S_SUCCESS;
}

//------------------------------------------------------------------------------
// Patch if settings structure changed
// Return   u8  status
// Engineer: Quyen Leba
//------------------------------------------------------------------------------
u8 settings_patch()
{
    if (SETTINGS_TUNE(version) < 4)
    {
        SETTINGS_TUNE(stockwheels.tire) = 0;
        SETTINGS_TUNE(stockwheels.axel) = 0;
        SETTINGS_SetTuneAreaDirty();
    }
    if (SETTINGS_TUNE(version) < 3)
    {
        //add otfinfo
        SETTINGS_TUNE(tuneinfotrack.otfinfo.reserved[0]) = 0;
        SETTINGS_TUNE(tuneinfotrack.otfinfo.reserved[1]) = 0;
        SETTINGS_TUNE(tuneinfotrack.otfinfo.pcm) = 0;
        SETTINGS_TUNE(tuneinfotrack.otfinfo.tcm) = 0;
        SETTINGS_TUNE(tuneinfotrack.otfinfo.powerlevel) = 0;
        SETTINGS_SetTuneAreaDirty();
    }
    if (SETTINGS_TUNE(version) < 2)
    {
        //add stockspoptsfile_crc32e
        SETTINGS_TUNE(stockspoptsfile_crc32e) = 0;
        SETTINGS_SetTuneAreaDirty();
    }
    if (SETTINGS_TUNE(version) < 1)
    {
        //add customtune_vehiclecodes
        if (SETTINGS_IsMarried())
        {
            memcpy((char*)SETTINGS_TUNE(customtune_vehiclecodes),
                   (char*)SETTINGS_TUNE(vehiclecodes),
                   sizeof(SETTINGS_TUNE(vehiclecodes)));
            memset((char*)SETTINGS_TUNE(customtune_secondvehiclecodes), 0,
                   sizeof(SETTINGS_TUNE(customtune_secondvehiclecodes)));
            memset((char*)SETTINGS_TUNE(secondvehiclecodes), 0,
                   sizeof(SETTINGS_TUNE(secondvehiclecodes)));
            SETTINGS_TUNE(version) = 1;
            SETTINGS_SetTuneAreaDirty();
        }
    }

    if (SETTINGS_GENERAL(version) < 1)
    {
        SETTINGS_GENERAL(control.demo_mode) = 0;
        SETTINGS_GENERAL(control.reserved0) = 0;
        SETTINGS_GENERAL(control.reserved1) = 0;
        SETTINGS_GENERAL(version) = 1;
        SETTINGS_SetGeneralAreaDirty();
    }

    return settings_update(SETTINGS_UPDATE_IF_DIRTY);
}

//------------------------------------------------------------------------------
// Settings check
// Return   u8  status
// Engineer: Quyen Leba
//------------------------------------------------------------------------------
u8 settings_check()
{
    //market could change by USB command
//    if (SETTINGS_GetMarketType() != MARKET_TYPE)
//    {
//        return S_FAIL;
//    }
    if (SETTINGS_GetDeviceType() != DEVICE_TYPE)
    {
        return S_FAIL;
    }
    else if (settings_patch() != S_SUCCESS)
    {
        return S_FAIL;
    }
    
    return S_SUCCESS;
}

/**
 * @brief   Load default settings when enable demo mode
 * 
 * @retval  None
 *
 * @author  Quyen Leba
 */
static void __settings_load_default_settings_for_demo_mode()
{
    memcpy((char*)&settingstune,
           (char*)&SettingsTuneDef,sizeof(SettingsTuneDef));
    memcpy((char*)&settingsdataloggeneral,
           (char*)&SettingsDatalogGeneralDef,sizeof(SettingsDatalogGeneralDef));
    
    memset((char*)settingstune.vehiclecodes,0,sizeof(settingstune.vehiclecodes));
    memset((char*)settingstune.vin,0,sizeof(settingstune.vin));
    memset((char*)settingstune.vehicle_serial_number,0,
           sizeof(settingstune.vehicle_serial_number));
    memset((char*)settingstune.epats,0,sizeof(settingstune.epats));
    memset((char*)settingstune.vid,0,sizeof(settingstune.vid));
    
    memset((char*)settingstune.tuneinfotrack.tunedescriptions,
           0,sizeof(settingstune.tuneinfotrack.tunedescriptions));
    memset((char*)settingstune.tuneinfotrack.tunefilenames,
           0,sizeof(settingstune.tuneinfotrack.tunefilenames));
    memset((char*)settingstune.tuneinfotrack.optionfilenames,
           0,sizeof(settingstune.tuneinfotrack.optionfilenames));

    SETTINGS_SetDemoMode();
}

/**
 * @brief   Register device to demo mode
 * 
 * @retval  u8 status
 *
 * @author  Quyen Leba
 */
u8 settings_register_demo_mode()
{
    u8  status;

    status = flash_save_setting(DatalogGeneralArea,
                                (u8*)&settingsdataloggeneral,sizeof(settingsdataloggeneral));

    if (status == S_SUCCESS)
    {
        __settings_load_default_settings_for_demo_mode();

        SETTINGS_SetDemoMode();
        SETTINGS_SetTuneAreaDirty();
        SETTINGS_SetGeneralAreaDirty();
        //since device is in demo mode at this point, settings_update() won't touch the real settings in flash
        //but use demo settings in file system
        settings_update(SETTINGS_UPDATE_IF_DIRTY);
    }
    return status;
}

/**
 * @brief   Unregister device from demo mode
 * 
 * @retval  u8 status
 *
 * @author  Quyen Leba
 */
u8 settings_unregister_demo_mode()
{
    u8  status;
    
    SETTINGS_ClearDemoMode();
    
    status = settings_load();
    
    if (status == S_SUCCESS)
    {
        SETTINGS_SetGeneralAreaDirty();
        status = settings_update(SETTINGS_UPDATE_IF_DIRTY);
    }
    else
    {
        SETTINGS_SetDemoMode();
        flash_save_setting(DatalogGeneralArea,
                           (u8*)&settingsdataloggeneral,sizeof(settingsdataloggeneral));
    }
    
    return status;
}
