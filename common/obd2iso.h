/**
  **************** (C) COPYRIGHT 2011 SCT Performance, LLC *********************
  * File Name          : obd2iso.h
  * Device / System    : SCT products using the "CommonCode" platform
  * Author             : Rhonda Rusak
  *
  * Version            : 1 
  * Date               : 08/11/2014
  * Description        : 
  *                    : 
  *
  ******************************************************************************
  */
#ifndef __OBD2iso_H
#define __OBD2iso_H

#include <arch/gentype.h>
#include "obd2.h"
#include "obd2tune.h"
#include <common/obd2def.h>

/*
  SCI/ ISO TX Flags
  bit 0 = 1 =send SCIB / 0 = send SCIA
  bit 1 = 1 check for 0x22 answer in SCIA / 0 do not check
  bit 2 = 1 use checksum / 0 do not use checksum

 SCI/ ISO RX Flags
 bit 0 = 1 = RX SCIB / 0 = RX SCIA
 bit 1 = 1 = skip 0x80 on RX / 0 = do not skip 0x80 on RX
 bit 2 = 1 = check checksum / 0 = do not check checksum
 bit 3 = 1 = wait extra time / 0 = do not wait extra time
 */

typedef struct
{
    u8   service; 
    u8   subservice; 
    bool response_expected;
    u8   *rxbuffer;
    u16  rxlength;
    u8   *txbuffer; 
    u16  txlength; 
    u16  txflags;
    u16  rxflags; 
    u8   format_byte;  
    VehicleCommType vehiclecommtype;
    u16  opcode_length; 
    u32  rx_timeout_ms; 
}Obd2iso_ServiceData;


void obd2iso_servicedata_init(Obd2iso_ServiceData *servicedata);

u8 obd2iso_txrx(Obd2iso_ServiceData *servicedata);
u8 obd2iso_tx(Obd2iso_ServiceData *servicedata);
u8 obd2iso_rx(Obd2iso_ServiceData *servicedata); 

u8 obd2iso_ping(VehicleCommType commtype);
u8 obd2iso_cleardtc(VehicleCommType commtype);
u8 obd2iso_readdtc(dtc_info *info, VehicleCommType commtype);
u8 obd2iso_read_data_bypid_mode1(u8 pid, u8 pidsize, u8 *data, 
                                 VehicleCommType commtype);
u8 obd2iso_readvin(u8 *vin, VehicleCommType commtype);
u8 obd2iso_read_calid(u8 *calid, VehicleCommType commtype);
u8 obd2iso_readecminfo(ecm_info *ecm);
u8 obd2iso_getinfo(obd2_info *obd2info, VehicleCommType commtype);

#endif
