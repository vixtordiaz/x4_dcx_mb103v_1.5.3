/**
  **************** (C) COPYRIGHT 2011 SCT Performance, LLC *********************
  * File Name          : atoi.c
  * Device / System    : SCT products using the "CommonCode" platform
  * Author             : Quyen Leba
  *
  * Version            : 1 
  * Date               : 05/02/2011
  * Description        : 
  *                    : 
  *
  *
  * History            : 05/02/2011 M. Davis
  *                    :   Add SCT Copyright header,
  *
  ******************************************************************************
  */

#include <common/statuscode.h>
#include "atoi.h"

#define IS_AF(c)  ((c >= 'A') && (c <= 'F'))
#define IS_af(c)  ((c >= 'a') && (c <= 'f'))
#define IS_09(c)  ((c >= '0') && (c <= '9'))
#define ISVALIDHEX(c)  IS_AF(c) || IS_af(c) || IS_09(c)
#define ISVALIDDEC(c)  IS_09(c)
#define CONVERTDEC(c)  (c - '0')

#define CONVERTHEX_alpha(c)  (IS_AF(c) ? (c - 'A'+10) : (c - 'a'+10))
#define CONVERTHEX(c)   (IS_09(c) ? (c - '0') : CONVERTHEX_alpha(c))

//------------------------------------------------------------------------------
// Convert a string to an integer
// Input:   u8  *inputstr
// Output   s32 *intnum
// Return:  u8  status
//------------------------------------------------------------------------------
u8 atoi(const u8 *inputstr, s32 *intnum)
{
    u32 i = 0;
    u32 val = 0;
    u8  status;

    if (inputstr[0] == '0' && (inputstr[1] == 'x' || inputstr[1] == 'X'))
    {
        if (inputstr[2] == '\0')
        {
            return S_ERROR;
        }
        for (i = 2; i < 11; i++)
        {
            if (inputstr[i] == '\0')
            {
                *intnum = val;
                status = S_FAIL;
                break;
            }
            if (ISVALIDHEX(inputstr[i]))
            {
                val = (val << 4) + CONVERTHEX(inputstr[i]);
            }
            else
            {
                status = S_BADCONTENT;  //invalid input
                break;
            }
        }
        // Over 8 digit hex --invalid
        if (i >= 11)
        {
            status = S_BADCONTENT;
        }
    }
    else    //max 10-digit decimal input
    {
        for (i = 0;i < 11;i++)
        {
            if (inputstr[i] == '\0')
            {
                *intnum = val;
                status = S_SUCCESS;
                break;
            }
            else if ((inputstr[i] == 'k' || inputstr[i] == 'K') && (i > 0))
            {
                val = val << 10;
                *intnum = val;
                status = S_SUCCESS;
                break;
            }
            else if ((inputstr[i] == 'm' || inputstr[i] == 'M') && (i > 0))
            {
                val = val << 20;
                *intnum = val;
                status = S_SUCCESS;
                break;
            }
            else if (ISVALIDDEC(inputstr[i]))
            {
                val = val * 10 + CONVERTDEC(inputstr[i]);
            }
            else
            {
                status = S_BADCONTENT;  //invalid input
                break;
            }
        }
        // Over 10 digit decimal --invalid
        if (i >= 11)
        {
            status = S_BADCONTENT;  //invalid input
        }
    }
    
    return status;
}
