/**
  **************** (C) COPYRIGHT 2011 SCT Performance, LLC *********************
  * File Name          : statuscode.h
  * Device / System    : SCT products using the "CommonCode" platform
  * Author             : Quyen Leba
  *
  * Version            : 1 
  * Date               : 05/02/2011
  * Description        : 
  *                    : 
  *
  *
  * History            : 05/02/2011 M. Davis
  *                    :   Add SCT Copyright header,
  *
  ******************************************************************************
  */

#ifndef __STATUSCODE_H
#define __STATUSCODE_H

typedef enum
{
    //Pass always = 0
    S_SUCCESS                   = 0,

    //fail always equals not zero
    S_ERROR                     = 1,                //0x01
    S_NOCOMMUNICATION           = 2,                //0x02
    S_OUTOFRANGE                = 3,                //0x03
    S_UNLOCKFAIL                = 4,                //0x04
    S_INVALIDSEEDKEY            = 5,                //0x05
    S_NOSEEDFOUND               = 6,                //0x06
    S_INVALIDPROCESSORCOUNT     = 7,                //0x07
    S_NOSECURITYBYTE            = 8,                //0x08
    S_READSECURITYBYTEFAIL      = 9,                //0x09
    S_STOCKFILECORRUPT          = 10,               //0x0A
    S_ERASEECUFAIL              = 11,               //0x0B
    S_DOWNLOADECUFAIL           = 12,               //0x0C
    S_VINBLANK                  = 13,               //0x0D
    S_VININVALID                = 14,               //0x0E
    S_NOHEXFOUND                = 15,               //0x0F
    S_READVINFAIL               = 16,               //0x10
    S_READPATSFAIL              = 17,               //0x11
    S_RESETECUFAIL              = 18,               //0x12
    S_PATSUNMATCH               = 19,               //0x13
    S_VINUNMATCH                = 20,               //0x14
    S_FAIL                      = 21,               //0x15
    S_LOWVBATT                  = 22,               //0x16
    S_LOWVPP                    = 23,               //0x17
    S_TIMEOUT                   = 24,               //0x18
    S_UNMATCH                   = 25,               //0x19
    S_USERABORT                 = 26,               //0x1A
    S_NOLOOKUP                  = 27,               //0x1B
    S_SCPNEEDRETRYBLOCK         = 28,               //0x1C
    S_BADCRCFAIL                = 29,               //0x1D
    S_SERVICENOTSUPPORTED       = 30,               //0x1E
    S_INVALIDSETTINGS           = 31,               //0x1F
    S_INVALIDBINARYMARRIED      = 32,               //0x20
    S_STRATNOTFOUND             = 33,               //0x21
    S_MARRIEDCOUNTMAXED         = 34,               //0x22
    S_MARRIEDCOUNTWARN          = 35,               //0x23
    S_FILENOTFOUND              = 36,               //0x24
    S_COMMLOST                  = 37,               //0x25
    S_PARTNUMBER                = 38,               //0x26
    S_STRATEGY                  = 39,               //0x27
    S_BADCONTENT                = 40,               //0x28
    S_CRC32E                    = 41,               //0x29
    S_MALLOC                    = 42,               //0x2A
    S_OPENFILE                  = 43,               //0x2B
    S_SEEKFILE                  = 44,               //0x2C
    S_READFILE                  = 45,               //0x2D
    S_WRITEFILE                 = 46,               //0x2E
    S_TCMUNMATCH                = 47,               //0x2F
    S_SPFNOTFOUND               = 48,               //0x30
    
    S_INPUT                     = 50,               //0x32
    S_STALL                     = 51,               //0x33
    S_STRATEGY_64               = 52,               //0x34
    S_ALREADY_UNLOCKED          = 53,               //0x35
    S_VEHICLETYPE               = 54,               //0x36
    S_ECMTYPE                   = 55,               //0x37
    S_COMMTYPE                  = 56,               //0x38
    S_COMMLEVEL                 = 57,               //0x39
    S_REJECT                    = 58,               //0x3A
    S_READSERIAL                = 59,               //0x3B
    S_SERIALUNMATCH             = 60,               //0x3C
    S_UTILITYNOTREQUIRED        = 61,               //0x3D
    S_NOTFIT                    = 62,               //0x3E
    S_NOTSUPPORT                = 63,               //0x3F
    S_SETPOWER                  = 64,               //0x40
    S_BAUDUNCHANGED             = 65,               //0x41
    S_SKIPOPTION                = 66,               //0x42
    S_NOTREQUIRED               = 67,               //0x43
    S_INVALIDTYPE               = 68,               //0x44
    S_TERMINATED                = 69,               //0x45
    S_TUNEUNMATCH               = 70,               //0x46
    S_OSUNMATCH                 = 71,               //0x47
    S_SEGMENTUNMATCH            = 72,               //0x48
    S_INVALIDBINARY             = 73,               //0x49
    S_TUNEMATCH_VINMASK         = 74,               //0x4A
    S_SEND                      = 75,               //0x4B
    S_RECEIVE                   = 76,               //0x4C
    S_EXPIRED                   = 77,               //0x4D
    S_SERIAL                    = 78,               //0x4E
    S_BUSY                      = 79,               //0x4F
    S_HALTED                    = 80,               //0x50
    S_SESSION_DROPPED           = 81,               //0x51
    S_END                       = 82,               //0x52
    S_MARRIED                   = 83,               //0x53
    S_FULL                      = 84,               //0x54
    S_EMPTY                     = 85,               //0x55
    S_FILE                      = 86,               //0x56
    S_LAYOUT                    = 87,               //0x57
    S_UNCOMPATIBLE              = 88,               //0x58

    S_USER_EXIT                 = 99,               //0X63

    S_WRONG_HARDWARE            = 160,              //0xA0
    S_INVALID_BOOTMODE          = 161,              //0xA1
    S_INVALID_VERSION           = 162,              //0xA2
    S_NO_UPDATE                 = 163,              //0xA3
    S_LOADFILE                  = 164,              //0xA4
    S_DEVICETYPE                = 165,              //0xA5
    S_MARKETTYPE                = 166,              //0xA6
    S_INVALID_CONDITION         = 167,              //0xA7
    
    //only use for USB control
    S_USB_UNKNOWN               = 200,              //0xC8
    S_USB_SECURITY              = 201,              //0xC9
    S_USB_FLOWCONTROL           = 202,              //0xCA
    S_USB_HALTED                = 203,              //0xCB
    S_USB_CBW_ID                = 210,              //0xD2
    S_USB_CBW_SIZE              = 211,              //0xD3
    S_USB_CBW_DATALENGTH        = 212,              //0xD4
    S_USB_CSW_ID                = 213,              //0xD5
    S_USB_UNSUPPORTED_CMD       = 214,              //0xD6
    S_USB_UNKNOWN_CMD           = 215,              //0xD7
    S_USB_UNSUPPORTED_FLAGS     = 216,              //0xD8
    S_USB_UNKNOWN_FLAGS         = 217,              //0xD9
}STATUS_CODE;

#endif  //__STATUSCODE_H
