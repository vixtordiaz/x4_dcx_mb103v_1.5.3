/**
  **************** (C) COPYRIGHT 2011 SCT Performance, LLC *********************
  * File Name          : obd2tune_upload.h
  * Device / System    : SCT products using the "CommonCode" platform
  * Author             : Quyen Leba
  *
  * Version            : 1 
  * Date               : 05/02/2011
  * Description        : 
  *                    : 
  *
  *
  * History            : 05/02/2011 M. Davis
  *                    :   Add SCT Copyright header,
  *
  ******************************************************************************
  */

#ifndef __OBD2TUNE_UPLOAD_H
#define __OBD2TUNE_UPLOAD_H

typedef enum
{
    GenerateStockSource_Use_Only_DEFAULT_Folder,
    GenerateStockSource_Use_Only_USER_Folder,
    GenerateStockSource_Use_DEFAULT_Folder_First,
    GenerateStockSource_Use_USER_Folder_First,
}GenerateStockSource;

u8 obd2tune_uploadstock(u16 veh_type, bool isuploadonly);
u8 obd2tune_check_ecm_stock(u16 ecm_type, GenerateStockSource stocksource, u8 index);
u8 obd2tune_generate_ecm_stock(u16 ecm_type, GenerateStockSource stocksource);

#endif  //__OBD2TUNE_UPLOAD_H
