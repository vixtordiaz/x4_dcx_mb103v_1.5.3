/*
 *  COPYRIGHT (C) 1986 Gary S. Brown.  You may use this program, or
 *  code or tables extracted from it, as desired without restriction.
 *
 *  First, the polynomial itself and its table of feedback terms.  The
 *  polynomial is
 *  X^32+X^26+X^23+X^22+X^16+X^12+X^11+X^10+X^8+X^7+X^5+X^4+X^2+X^1+X^0
 *
 *  Note that we take it "backwards" and put the highest-order term in
 *  the lowest-order bit.  The X^32 term is "implied"; the LSB is the
 *  X^31 term, etc.  The X^0 term (usually shown as "+1") results in
 *  the MSB being 1
 *
 *  Note that the usual hardware shift register implementation, which
 *  is what we're using (we're merely optimizing it by doing eight-bit
 *  chunks at a time) shifts bits into the lowest-order term.  In our
 *  implementation, that means shifting towards the right.  Why do we
 *  do it this way?  Because the calculated CRC must be transmitted in
 *  order from highest-order term to lowest-order term.  UARTs transmit
 *  characters in order from LSB to MSB.  By storing the CRC this way
 *  we hand it to the UART in the order low-byte to high-byte; the UART
 *  sends each low-bit to hight-bit; and the result is transmission bit
 *  by bit from highest- to lowest-order term without requiring any bit
 *  shuffling on our part.  Reception works similarly
 *
 *  The feedback terms table consists of 256, 32-bit entries.  Notes
 *
 *      The table can be generated at runtime if desired; code to do so
 *      is shown later.  It might not be obvious, but the feedback
 *      terms simply represent the results of eight shift/xor opera
 *      tions for all combinations of data and CRC register values
 *
 *      The values must be right-shifted by eight bits by the "updcrc
 *      logic; the shift must be unsigned (bring in zeroes).  On some
 *      hardware you could probably optimize the shift in assembler by
 *      using byte-swap instructions
 *      polynomial $edb88320
 */

#ifndef __CRC32_H
#define __CRC32_H

#include <arch/gentype.h>
#include <board/genplatform.h>
#include <common/bitreverse.h>

#define CRC32_USE_SOFTWARE

#ifdef CRC32_USE_SOFTWARE

extern const u32 crc32_table[256];

#define crc32_sw_reset()               //do nothing
#define crc32e_sw_reset()              //do nothing

//------------------------------------------------------------------------------
// Inputs:  u32 val (initial value)
//          const void *ss (the data)
//          u32 len (data length)
// Return:  u8  value (crc32e value)
//------------------------------------------------------------------------------
static inline u32
crc32_sw_calculateblock(u32 val, const void *ss, u32 len)
{
    const u8 *s = (const u8 *)ss;
    while (len-- > 0)
    {
        val = crc32_table[(val ^ *s++) & 0xff] ^ (val >> 8);
    }
    return val;
}
//------------------------------------------------------------------------------
// Inputs:  u32 val (initial value)
//          const u32 *ss (the data)
//          u32 len (data length)
// Return:  u8  value (crc32e value)
//------------------------------------------------------------------------------
static inline u32
crc32e_sw_calculateblock(u32 val, const u32 *ss, u32 len)
{
    u8  *s;
    u32 *a;
    u32 value;
    u32 tmpdata;

    a = (u32*)ss;
    val = bitrev32(val);

    while (len-- > 0)
    {
        tmpdata = bitrev32(*a++);
        s = (u8*)&tmpdata;

        val = crc32_table[(val & 0xff) ^ s[0]] ^ (val >> 8);
        val = crc32_table[(val & 0xff) ^ s[1]] ^ (val >> 8);
        val = crc32_table[(val & 0xff) ^ s[2]] ^ (val >> 8);
        val = crc32_table[(val & 0xff) ^ s[3]] ^ (val >> 8);
    }

    value = bitrev32(val);

    return value;
}

#endif  //#ifdef CRC32_USE_SOFTWARE

//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
#define K_Code_CRC32(val,ss,len)    \
    crc32_sw_calculateblock(val,ss,len)
#define K_Code_CRC32_Ethernet(val,ss,len)   \
    crc32e_sw_calculateblock(val,ss,len)

#endif //__CRC32_H
