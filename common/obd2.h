/**
  **************** (C) COPYRIGHT 2011 SCT Performance, LLC *********************
  * File Name          : obd2.h
  * Device / System    : SCT products using the "CommonCode" platform
  * Author             : Quyen Leba
  *
  * Version            : 1 
  * Date               : 05/02/2011
  * Description        : 
  *                    : 
  *
  *
  * History            : 05/02/2011 M. Davis
  *                    :   Add SCT Copyright header,
  *
  ******************************************************************************
  */

#ifndef __OBD2_H
#define __OBD2_H

#include <arch/gentype.h>
#include <board/genplatform.h>
#include <common/obd2def.h>
#include <common/obd2tune.h>

#ifdef __PRODUCTION_TEST__
#define OBD2_SUPPORT_TUNE               0
#define OBD2_SUPPORT_DATALOG            0
#define OBD2_SUPPORT_DIAGNOSTIC         0
#else
#define OBD2_SUPPORT_TUNE               1
#define OBD2_SUPPORT_DATALOG            1
#define OBD2_SUPPORT_DIAGNOSTIC         1
#endif

/******************************************************************************/
/* Enhanced Diagnostic Services - Chrysler (KWP2000) */
/******************************************************************************/

typedef enum
{
    DCX_Mode_01                                = 0x01,
    DCX_Read_DTCs                              = 0x18,
    DCX_ClearDiagnosticInformation             = 0x04,     // M1
    DCX_RequestVehicleInformation              = 0x09,
    DCX_InitiateDiagnosticOperation            = 0x10,     // C1
    DCX_RequestModuleReset                     = 0x11,
    DCX_ReadFailureRecordData                  = 0x12,     // C
    DCX_RequestDiagnosticInformationExt        = 0x13,
    DCX_ClearDiagnosticInfomationExt           = 0x14,
    DCX_ReadDTCInfoByID                        = 0x19,
    DCX_ReadDataByIdentifier                   = 0x1A,     // M
    DCX_ReturnNormalMode                       = 0x20,     // M
    DCX_ReadDataByLocalIdentifier              = 0x21,
    DCX_ReadDataByParameterIdentifier          = 0x22,     // U
    DCX_ReadMemoryByAddress                    = 0x23,     // U
    DCX_StopTransmittingRequestedData          = 0x25,
    DCX_SecurityAccess                         = 0x27,     // C2
    DCX_DisableNormalCommunication             = 0x28,     // M
    DCX_EnableNormalMessageTransmission        = 0x29,
    DCX_RequestDiagnosticDataPacket            = 0x2A,
    DCX_DynamicallyDefineMessage               = 0x2C,     // C
    DCX_DefinePIDByAddress                     = 0x2D,     // U
    DCX_WriteDataByParamID                     = 0x2E,
    DCX_StartRoutineByLocalId                  = 0x31,
    DCX_Results_of_RoutineByLocalID            = 0x33,
    DCX_StopDiagnosticRoutineByTestNumber      = 0x32,
    DCX_RequestDownload                        = 0x34,     // N/A
    DCX_RequestUpload                          = 0x35,     // N/A
    DCX_TransferData                           = 0x36,     // N/A
    DCX_RequestDataTransferExit                = 0x37,     // N/A
    DCX_WriteDataByIdentifier                  = 0x3B,     // C
    DCX_WriteDataBlock                         = DCX_WriteDataByIdentifier,
    DCX_ReadDataBlock                          = DCX_ReadDataByIdentifier,
    DCX_TesterPresent                          = 0x3F,     // M
    DCX_RequestDiagnosticDataPacket_A0         = 0xA0,
    DCX_DynamicallyDefineDiagnosticDataPacket  = 0xA1,
    DCX_Fault_control                          = 0x85,
    DCX_InvalidServiceNumber              = 0xFF,     // NeverSupported
}DCX_ServiceNumber;

/******************************************************************************/
/* Enhanced Diagnostic Services - Ford (ISO14229 and KWP2000) */
/******************************************************************************/
typedef enum
{
    FORD_RequestCurrentPowertrainDiagnosticData = 0x01,     // M1
    FORD_ClearDiagnosticInformation             = 0x04,     // M1
    FORD_RequestVehicleInformation              = 0x09,
    FORD_InitiateDiagnosticOperation            = 0x10,     // C1
    FORD_RequestModuleReset                     = 0x11,
    FORD_ReadFailureRecordData                  = 0x12,     // C
    FORD_RequestDiagnosticInformationExt        = 0x13,
    FORD_ClearDiagnosticInfomationExt           = 0x14,
    FORD_ReturnNormalMode                       = 0x20,     // M
    FORD_ReadDataByLocalId                      = 0x21,
    FORD_ReadDataByParameterIdentifier          = 0x22,     // U
    FORD_ReadMemoryByAddress                    = 0x23,     // U
    FORD_StopTransmittingRequestedData          = 0x25,
    FORD_SecurityAccess                         = 0x27,     // C2
    FORD_DisableNormalCommunication             = 0x28,     // M
    FORD_DisableNormalMessageTransmission       = FORD_DisableNormalCommunication,
    FORD_EnableNormalMessageTransmission        = 0x29,
    FORD_RequestDiagnosticDataPacket            = 0x2A,
    FORD_DynamicallyDefineMessage               = 0x2C,     // C
    FORD_DefinePIDByAddress                     = 0x2D,     // U
    FORD_WriteDataByParameterIdentifier         = 0x2E,
    FORD_InputOutputControlByIdentifier         = 0x2F,
    FORD_StartDiagnosticRoutineByTestNumber     = 0x31,
    FORD_StopDiagnosticRoutineByTestNumber      = 0x32,
    FORD_RequestDownload                        = 0x34,     // N/A
    FORD_RequestUpload                          = 0x35,     // N/A
    FORD_TransferData                           = 0x36,     // N/A
    FORD_RequestDataTransferExit                = 0x37,     // N/A
    FORD_WriteDataByIdentifier                  = 0x3B,     // C
    FORD_WriteDataBlock                         = FORD_WriteDataByIdentifier,
    FORD_ReadDataByIdentifier                   = 0x3C,
    FORD_ReadDataBlock                          = FORD_ReadDataByIdentifier,
    FORD_TesterPresent                          = 0x3F,     // M
    FORD_RequestDiagnosticDataPacket_A0         = 0xA0,
    FORD_DynamicallyDefineDiagnosticDataPacket  = 0xA1,
    FORD_DiagnosticCommand                      = 0xB1,
    
    FORD_InvalidServiceNumber              = 0xFF,     // NeverSupported
}FORD_ServiceNumber;

/******************************************************************************/
/* Enhanced Diagnostic Services - GM (GMLAN) */
/******************************************************************************/

typedef enum
{
    GMLAN_ClearDiagnosticInformation        = 0x04,     // M1
    GMLAN_InitiateDiagnosticOperation       = 0x10,     // C1
    GMLAN_RequestModuleReset                = 0x11,     //TODOQ4: check me
    GMLAN_ReadFailureRecordData             = 0x12,     // C
    GMLAN_ReadDataByIdentifier              = 0x1A,     // M
    GMLAN_ReturnNormalMode                  = 0x20,     // M
    GMLAN_ReadDataByParameterIdentifier     = 0x22,     // U
    GMLAN_ReadMemoryByAddress               = 0x23,     // U
    GMLAN_SecurityAccess                    = 0x27,     // C2
    GMLAN_DisableNormalCommunication        = 0x28,     // M
    GMLAN_DynamicallyDefineMessage          = 0x2C,     // C
    GMLAN_DefinePIDByAddress                = 0x2D,     // U
    GMLAN_RequestDownload                   = 0x34,     // N/A
    GMLAN_RequestUpload                     = 0x35,     // N/A
    GMLAN_TransferData                      = 0x36,     // N/A
    GMLAN_RequestDataTransferExit           = 0x37,     // N/A
    GMLAN_WriteDataByIdentifier             = 0x3B,     // C
    GMLAN_TesterPresent                     = 0x3E,     // M
    GMLAN_ReportProgrammedState             = 0xA2,     // N/A
    GMLAN_ProgammingMode                    = 0xA5,     // M
    GMLAN_ReadDiagnosticInformation         = 0xA9,     // M5
    GMLAN_ReadDataByPacketIdentifier        = 0xAA,     // C
    GMLAN_DeviceControl                     = 0xAE,     // C
    
    GMLAN_InvalidServiceNumber              = 0xFF,     // NeverSupported
}GMLAN_ServiceNumber;

typedef enum
{
    programmedState_fullyprogram            = 0x00,     // $A2
    programmedState_no_opsw_caldata         = 0x01,
    programmedState_opswpresent_calmissing  = 0x02,
    programmedState_swpresent_nostartcal    = 0x03,
    programmedState_generalmemoryfault      = 0x50,
    programmedState_rammemoryfault          = 0x51,
    programmedState_nvrammemoryfault        = 0x52,
    programmedState_bootmemoryfailure       = 0x53,
    programmedState_flashmemoryfailure      = 0x54,
    programmedState_eeprommemoryfailure     = 0x55,    
}GMLAN_ProgrammedStateStatus;

/******************************************************************************/
/* Diagnostic Subservices */
/******************************************************************************/

typedef enum
{
    /* Diagnostic Service $10 */
    wakeUpLinks                             = 0x04,
    wakeUpFEPS                              = 0x85,
    enableAllDTCs                           = 0x01,
    disableAllDTCs                          = 0x02,
    enableDTCsDuringDevCntrl                = 0x03,
    defaultSession_kwp2000                  = 0x81,
    defaultSession_14229                    = 0x01,
    programmingSession_14229                = 0x02,
    extendedDianosticSession_14229          = 0x03,
    safetySystemDiagnosticSession_14229     = 0x04,
    programmingSessionNoResponse_14229      = 0x82,
    
    /* Diagnostic Service $10 (Chrysler KWP2000) */
    diagnosticRequestChrysler_92            = 0x92,
    diagnosticRequestChrysler_87            = 0x87,
    diagnosticRequestChrysler_85            = 0x85,
    diagnosticRequestChrysler_83            = 0x83,
    diagnosticRequestChrysler_03            = 0x03,
    defaultSession_DCX                      = 0x81,    
    
    /* Diagnostic Service $11 */
    hardReset                               = 0x01,
    hardReset_14229                         = 0x81,     /* ISO14229 Only */
    nonvolatileMemoryReset                  = 0x82,
    
    /* Diagnostic Service $12 */
    readFailureRecordIdentifiers            = 0x01,     /* $12, [sub] [data:4] */
    readFailureRecordParameters             = 0x02,
    
    /* Diagnostic Service $27 */
    SPSrequestSeed                          = 0x01,
    SPSsendKey                              = 0x02,
    DevCtrlrequestSeed                      = 0x03,
    DevCtrlsendKey                          = 0x04,
    DiagRequestSeed                         = 0x05,
    DiagSendKey                             = 0x06,
    
    /* Diagnostic Service $2A */
    stopSending_$2A                         = 0x00,
    scheduleAtFastRate_$2A                  = 0x14,
    
    /* Diagnostic Service $36 */
    download                                = 0x00,
    downloadAndExecuteOrExecute             = 0x80,
    
    /* Diagnostic Service $A5 (GMLAN) */
    requestProgrammingMode                  = 0x01,
    requestProgrammingMode_HighSpeed        = 0x02,
    enableProgrammingMode                   = 0x03,     /* No response */
    
    /* Diagnostic Service $AA (GMLAN) */
    stopSending_$AA                         = 0x00,
    sendOneResponse_$AA                     = 0x01,
    scheduleAtSlowRate_$AA                  = 0x02,
    scheduleAtMediumRate_$AA                = 0x03,
    scheduleAtFastRate_$AA                  = 0x04,
    
    invalidSubServiceNumber                 = 0xFF,     /* Not Supported */
}SubServiceNumber;

/******************************************************************************/
/* Generic Structures */
/******************************************************************************/

typedef struct
{
#define MAX_SEGMENT_COUNT           7
#define MAX_PARTNUMBER_COUNT        ECM_MAX_PART_COUNT
    u8 count;
    u8 id[MAX_PARTNUMBER_COUNT];
    // number in this order: OSNumber, CalId, Hardware, Segments[up to 7]
    u32 number[MAX_PARTNUMBER_COUNT];
    u8  bcc[4+1];
#define PARTNUMBER_INFO_NOFLAG      (0)
#define OSNUMBER_USE_C2             (1<<0)
#define OSNUMBER_USE_C3             (1<<1)
    u16 flags;
}partnumber_info;

#define Obd2can_SubServiceNumber    SubServiceNumber

/******************************************************************************/
/* Function Prototypes */
/******************************************************************************/

u8 obd2_garbagecollector();
void obd2_load_demo_vehicle_info();
void obd2_save_demo_vehicle_info();
void obd2_setcooldowntime(u32 time);
u32 obd2_getcooldowntime();
u8 obd2_clearemcinfo(ecm_info *ecminfo);
u8 obd2_findvehiclecommtypecommlevel(VehicleCommType *vehiclecommtype,
                                     VehicleCommLevel *vehiclecommlevel);
VehicleCommType obd2_findvehiclecommtype();
u8 obd2_j1850crc(u8 *msg_buf, u32 nbytes);
u8 obd2_generatevin(u8 *vin);
u8 obd2_validatevin(u8 *vin);
u8 obd2_readvin(u8 *vin);
u8 obd2_readserialnumber(u8 *serialnumber, VehicleCommType *vehiclecommtype);
u8 obd2_readecminfo(ecm_info *ecm, VehicleCommType *vehiclecommtype);
u8 obd2_cleardtc(VehicleCommType commtype, u8 command_if_cmd);
u8 obd2_parse_packet_pid(u8 *packet, u16 packet_size, u16 position, 
                         u8 *pid, u16 pid_size, u8 isBit);
u8 obd2_parsetype_dtc(u16 raw_dtc);
u8 obd2_getdtcinfo(VehicleCommLevel vehiclecommlevel, u8 *dtc, u8 *info);
u8 obd2_readdtc(dtc_info *info);
u8 obd2_vehicle_specific_task_before_program(flasher_info *f_info);
u8 obd2_vehicle_specific_task_after_program(flasher_info *f_info);
u8 obd2_vehiclecommtotext(VehicleCommType vehiclecommtype, u8 *commtype_text);
u8 obd2_vehicleinfototext(vehicle_info *vehicleinfo, u8 *vehicleinfo_text);

u8 obd2_wakeup(u16 veh_type, u16 ecm_index);
u8 obd2_unlockecm(u32 ecm_id, u32 algoindex);

u8 obd2_bus_init_in_programming_mode(VehicleCommType vehiclecommtype);
u8 obd2_checkkeyon(VehicleCommType vehiclecommtype, bool isFlash);
u8 obd2_checkkeyon_special(VehicleCommType vehiclecommtype, bool requirebusinit);
u8 obd2_validate_ecmbinarytype(u16 veh_type);
u8 obd2_setupvpp();
u8 obd2_wakeup(u16 veh_type, u16 ecm_index);
u8 obd2_setupbus_prior_upload(u16 ecm_type);
u8 obd2_setupbus_after_upload(u16 ecm_type);
u8 obd2_setupbus_prior_download(u16 ecm_type);
u8 obd2_setupbus_after_download(u16 ecm_type);
u8 obd2_vehicle_type_search(ecm_info *ecm);
void obd2_vehiclefunction(VehicleCommType *commtype, VehicleCommLevel *commlevel);


u8 obd2_keycycle(u16 vehicle_type, VehicleCommType vehiclecommtype, CMDIF_COMMAND command);

u8 obd2_readpid_mode1(bool broadcast, u32 ecm_id, u8 pid, u8 pidsize, u8 *data,
                      VehicleCommType vehiclecommtype);

u8 obd2_open();
u8 obd2_close();
u8 obd2_getinfo();
void obd2_disconnect_monitor();
void obd2_info_init(obd2_info *obd2info);
u8 obd2_info_block_init(obd2_info_block *block);
u8 obd2_info_get_block_by_type(obd2_info *obd2info, EcuType ecutype, obd2_info_block *block);
OemType obd2_get_oemtype();

#endif    //__OBD2_H
