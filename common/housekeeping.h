/**
  **************** (C) COPYRIGHT 2011 SCT Performance, LLC *********************
  * File Name          : housekeeping.h
  * Device / System    : SCT products using the "CommonCode" platform
  * Author             : Patrick Downs
  *
  * Version            : 1 
  * Date               : 12/15/2011
  * Description        : 
  *                    : 
  *
  *
  * History            : 05/02/2011 M. Davis
  *                    :   Add SCT Copyright header,
  *
  ******************************************************************************
  */

#ifndef __HOUSEKEEPING_H
#define __HOUSEKEEPING_H

typedef enum
{
    HouseKeepingType_EmptyType              = 0,
    HouseKeepingType_VppTimeout             = 1,
    HouseKeepingType_FileTransferTimeout    = 2,
    HouseKeepingType_IndicatorTimeout       = 3,
    HouseKeepingType_CmdifIntrResponseAck   = 4,
    HouseKeepingType_TaskTimerTO            = 5,
    HouseKeepingType_CommLinkSpeedSync      = 6,
}HouseKeepingType;

typedef struct
{
    HouseKeepingType type;
    u32 flags;
    u32 timeout_cmp_value;
}HouseKeepingItem;

void housekeeping_init();
u8 housekeeping_additem(HouseKeepingType newtype, u32 timeout_ms);
u8 housekeeping_removeitem(HouseKeepingType type);
void housekeeping_process();
void housekeeping_timeout_handler(HouseKeepingType type);



#endif    //__HOUSEKEEPING_H
