/**
  **************** (C) COPYRIGHT 2011 SCT Performance, LLC *********************
  * File Name          : cmdif_func_recoverystock.h
  * Device / System    : SCT products using the "CommonCode" platform
  * Author             : Quyen Leba
  *
  * Version            : 1 
  * Date               : 05/02/2011
  * Description        : 
  *                    : 
  *
  *
  * History            : 05/02/2011 M. Davis
  *                    :   Add SCT Copyright header,
  *
  ******************************************************************************
  */

#ifndef __CMDIF_FUNC_RECOVERYSTOCK_H
#define __CMDIF_FUNC_RECOVERYSTOCK_H

#include <arch/gentype.h>
#include <common/obd2tune_recoverystock.h>

u8 cmdif_func_recoverystock_check(u8 *recoverystock_header);
u8 cmdif_func_recoverystock_register(u8 *recoverystock_header,
                                     u8 *filename);


#endif //__CMDIF_FUNC_RECOVERYSTOCK_H
