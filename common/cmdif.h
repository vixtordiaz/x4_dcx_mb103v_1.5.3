/**
  **************** (C) COPYRIGHT 2011 SCT Performance, LLC *********************
  * File Name          : cmdif.h
  * Device / System    : SCT products using the "CommonCode" platform
  * Author             : Quyen Leba
  *
  * Version            : 1 
  * Date               : 05/02/2011
  * Description        : 
  *                    : 
  *
  *
  * History            : 05/02/2011 M. Davis
  *                    :   Add SCT Copyright header,
  *
  ******************************************************************************
  */

#ifndef __CMDIF_H
#define __CMDIF_H

#include <arch/gentype.h>

//set to 1 to support all debug functions in cmdif_func_debug(...)
#define SUPPORT_CMDIF_FUNC_DEBUG        0

// generate a compiler error for typedef values outside allowed range
#pragma diag_error=Pe229

#define MAX_FILENAME_LENGTH             64

typedef enum
{
    CMDIF_CMD_PING_RESPONSE             = 0x03,
    CMDIF_CMD_ACK                       = 0x30,
    CMDIF_CMD_FILE                      = 0x40,
    CMDIF_CMD_CONNECT                   = 0x50,
    
    CMDIF_CMD_FLASHER                   = 0x10,
    CMDIF_CMD_GET_STRATEGY              = 0x11,
    CMDIF_CMD_GET_TUNE_LIST_INFO        = 0x12,
    CMDIF_CMD_CHECK_TUNE_FILE           = 0x13,
    CMDIF_CMD_SELECT_TUNE_STRATEGY      = 0x14,
    CMDIF_CMD_USE_OPTION                = 0x15,
    CMDIF_CMD_USE_SPECIAL_OPTION        = 0x7E,
    CMDIF_CMD_INIT_UPLOAD               = 0x16,
    CMDIF_CMD_DO_UPLOAD                 = 0x17,
    CMDIF_CMD_INIT_DOWNLOAD             = 0x18,
    CMDIF_CMD_DO_DOWNLOAD               = 0x19,
    CMDIF_CMD_HEALTH_CHECK_DOWNLOAD     = 0x7F,
    CMDIF_CMD_READ_DTCS_INFO            = 0x1A,
    CMDIF_CMD_CLEAR_DTCS                = 0x1B,
    CMDIF_CMD_CHECK_FILE                = 0x1C,
    CMDIF_CMD_FOPEN_FILE                = 0x1D,
    CMDIF_CMD_READ_BLOCK                = 0x1E,
    CMDIF_CMD_READ_TO_MONITOR           = 0x1F,
    CMDIF_CMD_READ_BLOCK_CONFIRM        = 0x20,
    CMDIF_CMD_WRITE_BLOCK               = 0x21,
    CMDIF_CMD_WRITE_TO_DONGLE           = 0x22,
    CMDIF_CMD_WRITE_BLOCK_CONFIRM       = 0x2A,
    CMDIF_CMD_FOPEN_FILETRANSFER        = 0x3A,
    CMDIF_CMD_FCLOSE_FILETRANSFER       = 0x3D,
    CMDIF_CMD_FREAD_BLOCK               = 0x3B,
    CMDIF_CMD_FWRITE_BLOCK              = 0x3C,
    CMDIF_CMD_GET_TUNE_LIST_DATA        = 0x23,
    CMDIF_CMD_SELECT_TUNE_CUSTOM        = 0x24,
    CMDIF_CMD_SELECT_TUNE_CUSTOM_ADVANCE        = 0x42,
    CMDIF_CMD_GET_FILE_FROM_OPTION_LOOKUP       = 0x47,
    CMDIF_CMD_SET_EXTRA_CUSTOM_FILE_G1  = 0x2B,
    CMDIF_CMD_SET_EXTRA_CUSTOM_FILE_G2  = 0x2C,
    CMDIF_CMD_ABORT_TASK                = 0x25,
    CMDIF_CMD_INIT_RETURN_STOCK         = 0x26,
    CMDIF_CMD_DO_RETURN_STOCK           = 0x27,
    CMDIF_CMD_READ_DTCS_DATA            = 0x28,
    CMDIF_CMD_GET_DONGLE_INFO           = 0x29,
    CMDIF_CMD_SEARCH_FOR_VEHICLE_TYPE   = 0x2E,
    CMDIF_CMD_COMPILE_USER_VEHICLE_TYPE = 0x2F,
    
    CMDIF_CMD_WIFI_UPDATES_CHECK        = 0x44,
    CMDIF_CMD_WIFI_UPDATES_DOWNLOAD     = 0x45,
    CMDIF_CMD_WIFI_UPDATES_APPLY        = 0x46,
    CMDIF_CMD_WIFI_HANDLECONNECT        = 0x48,
    CMDIF_CMD_WIFI_STATUS               = 0x49,
    CMDIF_CMD_WIFI_GET_FILE_BY_ACCESS_STRING    = 0x4A,
    CMDIF_CMD_WIFI_GET_CLOUD_TUNELIST   = 0x4C,
    CMDIF_CMD_WIFI_DOWNLOAD_CLOUD_FILE  = 0x4D,
    CMDIF_CMD_WIFI_CHECK_LIMIT          = 0x4E,
    CMDIF_CMD_WIFI_DEACTIVATE           = 0x4F,
    
    CMDIF_CMD_GET_UPLOAD_STOCK          = 0x43,
    
    CMDIF_CMD_CHECK_RECOVERYSTOCK       = 0x3E,
    CMDIF_CMD_REGISTER_RECOVERYSTOCK    = 0x3F,
    CMDIF_CMD_FOLDER                    = 0x41,
    
    CMDIF_CMD_READ_DTCS_NEW             = 0x36,
    CMDIF_CMD_READ_DTCS_RAW             = 0x39,
    CMDIF_CMD_READ_VIN                  = 0x37,
    CMDIF_CMD_READ_ECM_INFO             = 0x38,
    
    CMDIF_CMD_OBD2_GARBAGE_COLLECTOR    = 0x4B,
    CMDIF_CMD_SPECIAL_FUNCTIONS         = 0x2D,
    
    CMDIF_CMD_PCM_UNLOCK                = 0x80,
    CMDIF_CMD_PROGRESS_BAR              = 0x81,
    CMDIF_CMD_GET_STRATEGY_FLASHER      = 0x82,

    CMDIF_CMD_GET_DONGLE_DATA           = 0x31,
    CMDIF_CMD_ENTER_BOOTLOAD            = 0x32,
    CMDIF_CMD_ENTER_BOOTLOAD_CONFIRM    = 0x33,
    CMDIF_CMD_BOOTLOADER_INIT           = 0xE0,
    CMDIF_CMD_BOOTLOADER_DO             = 0xE1,
    CMDIF_CMD_BOOTLOADER_VALIDATE       = 0xE3,
    CMDIF_CMD_BOOTLOADER_SETBOOT        = 0xE4,
    CMDIF_CMD_BOOTLOADER_CLEARENGSIGN   = 0xE5,
    CMDIF_CMD_BOOTLOADER_GETBOOT        = 0xE6,

    CMDIF_CMD_SET_DONGLE_DATA           = 0x34,
    CMDIF_CMD_GET_VEHICLE_INFO          = 0x35,
    
    CMDIF_CMD_MARRIEDCOUNTWARN_COMFIRM  = 0x65,
    CMDIF_CMD_MARRIEDCOUNTWARN_CANCEL   = 0x66,
    
    CMDIF_CMD_DATALOG_SCAN              = 0x70,
    CMDIF_CMD_DATALOG_DONGLE_FEATURES   = 0x71,
    CMDIF_CMD_DATALOG_FEATURES          = CMDIF_CMD_DATALOG_DONGLE_FEATURES,
    CMDIF_CMD_DATALOG_SETUP             = 0x72,
    CMDIF_CMD_DATALOG_START             = 0x73,
    CMDIF_CMD_DATALOG_CONFIRM           = 0x74,
    CMDIF_CMD_DATALOG_STOP              = 0x75,
    CMDIF_CMD_DATALOG_PAUSE             = 0x76,
    CMDIF_CMD_DATALOG_STATUS            = 0x77,
    CMDIF_CMD_DATALOG_KEEP_ALIVE        = 0x78,
    CMDIF_CMD_DATALOG_RESUME            = 0x79,
    CMDIF_CMD_DATALOG_RECORD            = 0x7A,
    CMDIF_CMD_DATALOG_SET_REPORT_RATE   = 0x7C,
    CMDIF_CMD_DATALOG_SAVE_RESULT       = 0x7D,
    CMDIF_CMD_DATALOG_UPDATE_OSC        = 0x7B,
    CMDIF_CMD_DATALOG_UNSET_ALL_OSC     = 0x6F,
    CMDIF_CMD_DATALOG_REQUEST_OTF       = 0x63,
    CMDIF_CMD_DATALOG_SET_OTF_POWER     = 0x64,
    
    CMDIF_CMD_GENERAL_REQUEST           = 0x90,
    CMDIF_CMD_GENERAL_ANSWER            = 0x91,
    CMDIF_CMD_KEY_CONFIRM               = 0x92,

    CMDIF_CMD_GET_DONGLE_SETTINGS       = 0xA0, //used with SETTINGS_OPCODE
    CMDIF_CMD_HARDWARE                  = 0xA1,
    CMDIF_CMD_PRELOADEDTUNE_RESTRICTION = 0xA2,
    CMDIF_CMD_GET_PROPERTIES            = 0xA3,
    CMDIF_CMD_GET_CMDTRACKER_REPORT     = 0xA4,
    CMDIF_CMD_VERSION_HEALTH_CHECK      = 0xA5,

    //these commands do not exist
    CMDIF_CMD_PRODUCTION_TEST           = 0xEC,
    CMDIF_CMD_DEBUG                     = 0xEE,
    
    CMDIF_CMD_INVALID                   = 0xFF,
}CMDIF_COMMAND;

typedef enum
{
    CMDIF_ACK_OK                        = 0x00,
    CMDIF_ACK_FAILED                    = 0x01,
    CMDIF_ACK_DOWNLOAD_FAILED           = 0x10,
    CMDIF_ACK_NOT_SUPPORTED             = 0x11,
    CMDIF_ACK_NOT_MARRIED               = 0x12,
    CMDIF_ACK_READ_STRATEGY_FAILED      = 0x13,
    CMDIF_ACK_FILE_NOT_FOUND            = 0x14,
    CMDIF_ACK_NO_FILEOPEN               = 0x28,
    CMDIF_ACK_TUNE_EXIST                = 0x15,
    CMDIF_ACK_TUNE_NOT_FOUND            = 0x16,
    CMDIF_ACK_OPTION_NOT_FOUND          = 0x29,
    CMDIF_ACK_STOCK_EXIST               = 0x17,
    CMDIF_ACK_STOCK_NOT_FOUND           = 0x18,
    CMDIF_ACK_VPP_LOW                   = 0x19,
    CMDIF_ACK_VBATT_LOW                 = 0x1A,
    CMDIF_ACK_SKIP                      = 0x1B,
    CMDIF_ACK_FILE_NOT_MATCH            = 0x1C,
    CMDIF_ACK_STOCK_CORRUPTED           = 0x21,
    CMDIF_ACK_FILESIZE_MISMATCH         = 0x22,
    CMDIF_ACK_FILECOPY_FAIL             = 0x24,
    CMDIF_ACK_STRAGEY_CHANGES_FAIL      = 0x25,
    CMDIF_ACK_MARRIED_FAIL              = 0x26,
    CMDIF_ACK_ERASE_FAIL                = 0x27,
    CMDIF_ACK_STOCK_SPEC_OPTS_BAD       = 0x2B,
    CMDIF_ACK_VEH_FLASHED               = 0x2C,
    CMDIF_ACK_VEH_FLASHED_UNKNOWN_TUNER = 0x2D,
    CMDIF_ACK_E2_STOCK_NOT_FOUND        = 0x2E,
    CMDIF_ACK_E2_STOCK_CORRUPTED        = 0x2F,
    //CMDIF_ACK_TUNEFILE_SIZE             = 0x23,
    
    CMDIF_ACK_OK_STOP                   = 0x1D,
    CMDIF_ACK_OK_CONT                   = 0x1E,
    CMDIF_ACK_FAIL_RETRY                = 0x1F,
    CMDIF_ACK_FAIL_NORETRY              = 0x20,

    CMDIF_ACK_KEY_OFF                   = 0x30,
    CMDIF_ACK_KEY_OFF_PRESS_CONTINUE    = 0x4D,
    CMDIF_ACK_KEY_ON                    = 0x31,
    CMDIF_ACK_UNLOCK_ECU                = 0x32,
    CMDIF_ACK_CHANGE_BAUD_ECU           = 0x33,
    CMDIF_ACK_CHANGE_BAUD_ECU_FAIL      = 0x4A,
    CMDIF_ACK_PROGRESSBAR               = 0x34,
    CMDIF_ACK_LISTBOX                   = 0x4B,
    CMDIF_ACK_MESSAGEBOX                = 0x4C,
    CMDIF_ACK_UPLOAD_DONE               = 0x35,
    CMDIF_ACK_UPDATE_MSG1               = 0x36,
    CMDIF_ACK_UPDATE_MSG2               = 0x37,
    CMDIF_ACK_UPDATE_MSG3               = 0x38,
    CMDIF_ACK_UPDATE_HEADER             = 0x39,
    CMDIF_ACK_UPLOAD_ERROR              = 0x3A,
    CMDIF_ACK_CHECK_MARRIED             = 0x3B,
    CMDIF_ACK_ERASE_ECU                 = 0x3C,
    CMDIF_ACK_RESET_ECU                 = 0x3D,
    CMDIF_ACK_CLEAR_DTC_ECU             = 0x3E,
    CMDIF_ACK_DOWNLOAD_DONE             = 0x3F,
    CMDIF_ACK_BUILDING_TUNE             = 0x40,
    CMDIF_ACK_MARRYING_VEHICLE          = 0x41,
    CMDIF_ACK_APPLY_STRATEGY            = 0x42,
    CMDIF_ACK_APPLY_OPTIONS             = 0x43,
    CMDIF_ACK_APPLY_CHECKSUM            = 0x64,
    
    CMDIF_ACK_NORMAL_MODE               = 0x44,
    CMDIF_ACK_BOOTLOADER_MODE           = 0x45,
    CMDIF_ACK_STANDBY_MODE              = 0x46,
    CMDIF_ACK_UNLOCK_ECU_FAIL           = 0x47,
    
    CMDIF_ACK_INVALID_BINARYTYPE        = 0x48,
    CMDIF_ACK_INVALID_PCMNUMBER         = 0x49,
    
    CMDIF_ACK_VEHICLE_UNSUPPORTED       = 0x50,
    CMDIF_ACK_NO_LOOKUP                 = 0x51,
    CMDIF_ACK_MARRIEDCOUTNMAXED         = 0x52,
    CMDIF_ACK_MARRIEDCOUNTWARN          = 0x53,
    
    CMDIF_ACK_COMMLOST                  = 0x54,
    CMDIF_ACK_COMMACTIVE                = 0x55,
    CMDIF_ACK_RESUME                    = 0x56,
    CMDIF_ACK_PAUSE                     = 0x57,
    CMDIF_ACK_DATA                      = 0x58,
    CMDIF_ACK_STATUS                    = 0x28,
    
    CMDIF_ACK_DONE                      = 0x60,
    CMDIF_ACK_UNSUPPORTED               = 0x61,
    CMDIF_ACK_INCORRECT_VEHTYPE         = 0x62,
    CMDIF_ACK_TUNEREVISION_OUTOFDATE    = 0x63,
    CMDIF_ACK_VIN_UNMATCH               = 0x65,
    CMDIF_ACK_TUNE_UNMATCH              = 0x66,
    CMDIF_ACK_FORCE_FULL_FLASH          = 0x67,
    CMDIF_ACK_FULL_FLASH                = 0x68,
    CMDIF_ACK_WAIT                      = 0x69,
    CMDIF_ACK_APP_OUTOFDATE             = 0x6A,
    CMDIF_ACK_SERIAL_UNMATCH            = 0x6B,
    CMDIF_ACK_EXPIRED                   = 0x6C,
    CMDIF_ACK_RECOVERY_STOCK            = 0x6D,
    CMDIF_ACK_BACKDATE                  = 0x6E,
    CMDIF_ACK_SPF_NOT_FOUND             = 0x6F,
    CMDIF_ACK_FULL_FLASH_NOT_ALLOWED    = 0x70,
    CMDIF_ACK_VB_OUTOFDATE              = 0x71,
    CMDIF_ACK_OPTION_LOOKUP_SEARCH      = 0x72,
    CMDIF_ACK_UNCOMPATIBLE              = 0x73,
    CMDIF_ACK_TERMINATE                 = 0x74,
    CMDIF_ACK_PROCESSING_DATA           = 0x75,
    CMDIF_ACK_KEYBOARD_INPUT            = 0x76,
    CMDIF_ACK_NUMPAD_INPUT              = 0x77,
    CMDIF_ACK_CSF_FILE_NOT_FOUND        = 0x78,
    CMDIF_ACK_CSF_FILE_CORRUPTED        = 0x79,

    CMDIF_ACK_PT_FLASHING_LOCKED        = 0xF0,     //locked for passthough flashing
    CMDIF_ACK_PT_DATALOGGING_LOCKED     = 0xF1,     //locked for passthough datalogging
    CMDIF_ACK_INVALID                   = 0xFF,     //not a valid ACK
}CMDIF_ACK;

typedef enum
{
    CMDIF_ACT_DATALOG_RESUME            = 0x00,
    CMDIF_ACT_DATALOG_PAUSE             = 0x01,
    CMDIF_ACT_STRATEGY_FLASHER          = 0x50,     //do not change
    CMDIF_ACT_PRELOADED_FLASHER         = CMDIF_ACT_STRATEGY_FLASHER,   //dnc
    CMDIF_ACT_CUSTOM_FLASHER            = 0x51,     //do not change
    CMDIF_ACT_STOCK_FLASHER             = 0x52,     //do not change
    CMDIF_ACT_UPLOAD_STOCK              = 0x53,     //do not change
    
    CMDIF_ACT_INVALID                   = 0xFF,
}CMDIF_ACTION;

typedef enum
{
    CMDIF_GETDEVICEDATA_MODESTATUS      = 0x00,     //do not change these values
    CMDIF_GETDEVICEDATA_SERIAL          = 0x01,
    CMDIF_GETDEVICEDATA_FIRMWARE        = 0x02,
    CMDIF_GETDEVICEDATA_TUNEREV         = 0x03,
    CMDIF_GETDEVICEDATA_DEVICETYPE      = 0x05,
    CMDIF_GETDEVICEDATA_MARKETTYPE      = 0x06,
    CMDIF_GETDEVICEDATA_MARRIEDSTATUS   = 0x07,    
    CMDIF_GETDEVICEDATA_MARRIEDCOUNT    = 0x08,
    CMDIF_GETDEVICEDATA_MARRIEDVIN      = 0x09,
    CMDIF_GETDEVICEDATA_BTADDR          = 0x10,
    CMDIF_GETDEVICEDATA_BETAFLAG        = 0x11,
    CMDIF_GETDEVICEDATA_OEMTYPE         = 0x12,
    
    CMDIF_GETDEVICEDATA_PROPERTIES      = 0x70,
    CMDIF_GETDEVICEDATA_RESETSETTING    = 0x20,
}CMDIF_GETDEVICEDATA_OPCODE;

typedef enum
{
    NormalAck,
    RawSPP,
}ResponseType;

// The following typedef enum is used as a u8 element of the following
// cmdif_tracker struct. 
typedef enum
{
    CMDIF_TRACKER_STATE_IDLE            = 0,
    CMDIF_TRACKER_STATE_IN_PROGRESS     = 1,
    CMDIF_TRACKER_STATE_LISTENING       = 2,    //in listening mode
    CMDIF_TRACKER_STATE_GET_COMMAND     = 3,    //expect command
    CMDIF_TRACKER_STATE_COMPLETE        = 7,
}CMDIF_TRACKER_STATE;

// Prevent more than 32 assigned values in the preceding typedef enum.
// The "#pragma diag_error=Pe229" at the top of this file causes a compiler 
// error to be generated when this limit is exceeded
typedef struct
{
  CMDIF_TRACKER_STATE checkRange : 5;
}CMDIF_TRACKER_STATE_RangeConfirm;

#define CMDIF_TRACKER_PRIVDATA_LENGTH_MAX   64
typedef struct
{
    CMDIF_COMMAND commandcode;
    CMDIF_ACK responsecode;
    CMDIF_TRACKER_STATE state; 
    u8  privdatalength;
    u8  privdata[CMDIF_TRACKER_PRIVDATA_LENGTH_MAX];
    //for progressbar
    CMDIF_COMMAND progbar_commandcode;
    u8  *progbar_string;
}cmdif_tracker;

typedef struct
{
    u8 *dataptr;
    u16 datalength;
    CMDIF_COMMAND commandcode;
    CMDIF_ACK responsecode;
    ResponseType responsetype;
    CMDIF_COMMAND flowcommandcode;
}cmdif_datainfo;

typedef struct
{
    u8 *data;
    u16 length;
    CMDIF_COMMAND commandcode;
    CMDIF_ACK responsecode;
}cmdif_response_info;

typedef struct      //must match UploadDownloadInitAttrib of AB
{
    u8  keyoffpowerdownrequired     :1;
    u8  reserved0                   :7;
    u8  powerdowntime;
    u16 veh_type;
}cmdif_upl_dnl_init_flags;

u8 cmdif_init();
u8 cmdif_handler();
u8 cmdif_interrupt_command(u16 command, u8 *data, u16 length);
u8 cmdif_command(u16 command, u8 *data, u16 length);
CMDIF_COMMAND cmdif_get_current_command();
u8 cmdif_getResponseData(cmdif_datainfo **datainfo);

void cmdif_tracker_track_new_command(CMDIF_COMMAND commandcode);
CMDIF_COMMAND cmdif_tracker_get_current_command();
void cmdif_tracker_set_state(CMDIF_TRACKER_STATE state);
void cmdif_tracker_set_response_ack(CMDIF_ACK responsecode);
u8 cmdif_tracker_set_private_data(u8 *data, u8 datalength);
u8 cmdif_interrupt_response_ack();

void cmdif_progressbar_setup(CMDIF_COMMAND cmd, const u8 *string);
void cmdif_progressbar_report(u8 percentage);
void cmdif_progressbar_report_semi(u8 percentage, u8 *string);
void cmdif_progressbar_report_full(CMDIF_COMMAND cmd, u8 percentage, u8 *string);

void cmdif_datainfo_responsedata_cleanup();

#include <board/commlink.h>

//IMPORTANT: first response must be commlink_send_ack(...)
#define cmdif_response_ack(cmd,resp,data,len)   \
    commlink_send_ack(cmd,resp,data,len);   \
    cmdif_interrupt_response_ack()

#define cmdif_response_ack_nowait(cmd,resp,data,len)    commlink_send_ack(cmd,resp,data,len)

u8 cmdif_internalresponse_ack(CMDIF_COMMAND command, CMDIF_ACK response, u8 *data, u16 datalen);

#define cmdif_internalresponse_ack_nowait(command,response,data,length) \
    cmdif_internalresponse_ack(command,response,data,length)

#define cmdif_receive_command_internal(data,length,max_length) \
    commlink_receive_command(data,length,max_length)

#define cmdif_reset_watchdog()  commlink_reset_transmission_watchdog()

#endif    //__CMDIF_H
