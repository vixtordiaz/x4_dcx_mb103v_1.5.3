/**
  **************** (C) COPYRIGHT 2011 SCT Performance, LLC *********************
  * File Name          : cmdif_remotegui.c
  * Device / System    : SCT products using the "CommonCode" platform
  * Author             : Quyen Leba
  *
  * Version            : 1 
  * Date               : 05/02/2011
  * Description        : 
  *                    : 
  *
  *
  * History            : 05/02/2011 M. Davis
  *                    :   Add SCT Copyright header,
  *
  ******************************************************************************
  */

#include <fs/genfs.h>
#include <board/genplatform.h>
#include <common/statuscode.h>
#include <common/cmdif.h>
#include "cmdif_remotegui.h"

extern cmdif_datainfo cmdif_datainfo_responsedata;  //from cmdif.c

/**
 * @brief   A
 *
 * @param [in]  cmd
 * @param [in]  flags
 * @param [in]  title
 * @param [in]  percentage
 * @param [in]  text (NULL allowed; max 512byte or will be truncated)
 *
 * @retval none
 *
 * @author  Quyen Leba
 *
 */
void cmdif_remotegui_progressbar(CMDIF_COMMAND cmd, u32 flags,
                                 const u8 *title, u8 percentage, const u8 *text)
{
    u32 length;
    u8  buffer[512+2];

    length = 1;
    buffer[0] = percentage;
    if (text)
    {
        length = strlen((char*)text);
        if (length > (sizeof(buffer)-2))
        {
            length = sizeof(buffer)-2;
        }
        memcpy((char*)&buffer[1],(char*)text,length);
        buffer[1+length] = 0;
        length += 2;
    }

    cmdif_internalresponse_ack(cmd,CMDIF_ACK_PROGRESSBAR,buffer,length);
}

/**
 * @brief   An init to use listbox on AppBoard
 *
 * @param [in]  title
 * @param [in]  persistent_header (allow NULL)
 *
 * @retval u8   status
 *
 * @author  Quyen Leba
 *
 */
u8 cmdif_remotegui_listboxbox_init(const u8 *title, const u8 *persistent_header)
{
    u32 length;

    if (cmdif_datainfo_responsedata.dataptr)
    {
        __free(cmdif_datainfo_responsedata.dataptr);
        cmdif_datainfo_responsedata.dataptr = NULL;
    }
    cmdif_datainfo_responsedata.datalength = 0;

#define LISTBOX_RESP_DATA_MAX_LENGTH        2048
    cmdif_datainfo_responsedata.dataptr = __malloc(LISTBOX_RESP_DATA_MAX_LENGTH);
    cmdif_datainfo_responsedata.datalength = 0;
    if (!cmdif_datainfo_responsedata.dataptr)
    {
        return S_MALLOC;
    }

    memset((char*)cmdif_datainfo_responsedata.dataptr,0,LISTBOX_RESP_DATA_MAX_LENGTH);
    cmdif_datainfo_responsedata.datalength = 12;
    if (!title)
    {
        cmdif_datainfo_responsedata.dataptr[cmdif_datainfo_responsedata.datalength++] = NULL;
    }
    else
    {
        length = strlen((char*)title);
        if (length > 64)
        {
            length = 64;
        }
        memcpy((char*)&cmdif_datainfo_responsedata.dataptr[cmdif_datainfo_responsedata.datalength],
               (char*)title,length);
        cmdif_datainfo_responsedata.datalength += length;
        cmdif_datainfo_responsedata.dataptr[cmdif_datainfo_responsedata.datalength++] = NULL;
    }

    if (!persistent_header)
    {
        cmdif_datainfo_responsedata.dataptr[cmdif_datainfo_responsedata.datalength++] = NULL;
    }
    else
    {
        length = strlen((char*)persistent_header);
        if (length > 64)
        {
            length = 64;
        }
        memcpy((char*)&cmdif_datainfo_responsedata.dataptr[cmdif_datainfo_responsedata.datalength],
               (char*)persistent_header,length);
        cmdif_datainfo_responsedata.datalength += length;
        cmdif_datainfo_responsedata.dataptr[cmdif_datainfo_responsedata.datalength++] = NULL;
    }

    return S_SUCCESS;
}

/**
 * @brief   Add an item to listbox on AppBoard
 *
 * @param [in]  text
 * @param [in]  subtext
 * @param [in]  flags: see CMDIF_REMOTEGUI_LISTBOX_FLAGS
 *
 * @retval u8   status (S_NOTFIT: can't add)
 *
 * @author  Quyen Leba
 *
 */
u8 cmdif_remotegui_listboxbox_additem(const u8 *text, const u8 *subtext, u16 flags)
{
    u32 text_length;
    u32 subtext_length;
    u8  status;

    //[4:flags][1:reserved][1:button0][1:button1][1:button2][1:focus][1:reserved][1:count][1:sel_index]
    //[m:title][NULL]
    //[m:persistent header][NULL][?:item0][NULL][?:item1][NULL]...[?:item#][NULL]   -> text
    //item: "[2:flags][maintext][|][subtext]

    status = S_FAIL;
    if (cmdif_datainfo_responsedata.dataptr)
    {
        text_length = strlen((char*)text);
        subtext_length = strlen((char*)subtext);

        if ((2+cmdif_datainfo_responsedata.datalength + text_length) >= LISTBOX_RESP_DATA_MAX_LENGTH)
        {
            text_length = LISTBOX_RESP_DATA_MAX_LENGTH-1 - 2 - cmdif_datainfo_responsedata.datalength;
        }
        if (text_length)
        {
            *((u16*)&cmdif_datainfo_responsedata.dataptr[cmdif_datainfo_responsedata.datalength]) = flags;
            cmdif_datainfo_responsedata.datalength += 2;
            memcpy((char*)&cmdif_datainfo_responsedata.dataptr[cmdif_datainfo_responsedata.datalength],
                   (char*)text,text_length);
            cmdif_datainfo_responsedata.datalength += text_length;
            
            if (subtext_length)
            {
                if ((cmdif_datainfo_responsedata.datalength + 1 + subtext_length) >= LISTBOX_RESP_DATA_MAX_LENGTH)
                {
                    subtext_length = LISTBOX_RESP_DATA_MAX_LENGTH-1 - 1 - cmdif_datainfo_responsedata.datalength;
                }
                if (subtext_length)
                {
                    cmdif_datainfo_responsedata.dataptr[cmdif_datainfo_responsedata.datalength++] = '|';
                    memcpy((char*)&cmdif_datainfo_responsedata.dataptr[cmdif_datainfo_responsedata.datalength],
                           (char*)subtext,subtext_length);
                    cmdif_datainfo_responsedata.datalength += subtext_length;
                }//if (subtext_length)...
            }//if (subtext_length)...

            cmdif_datainfo_responsedata.dataptr[cmdif_datainfo_responsedata.datalength++] = NULL;
            cmdif_datainfo_responsedata.dataptr[10]++;
            status = S_SUCCESS;
        }//if (text_length)...
        else
        {
            status = S_NOTFIT;
        }
    }//if (cmdif_datainfo_responsedata.dataptr)...
    else
    {
        status = S_ERROR;
    }

    return status;
}

/**
 * @brief   Show listbox on AppBoard
 *
 * @param [in]  cmd
 * @param [in]  title
 * @param [in]  flags
 * @param [in]  current_item_index
 * @param [in]  button0 (NULL or "": not used) - exit/left button
 * @param [in]  button1 (NULL or "": not used) - right button
 * @param [in]  button2 (NULL or "": not used) - left/middle button
 * @param [in]  focus
 *
 * @retval none
 *
 * @author  Quyen Leba
 *
 * @details For compatibility with LWTS, only use CMDIF_REMOTEGUI_BUTTONFACE_Exit on button0
 *
 */
void cmdif_remotegui_listbox(CMDIF_COMMAND cmd, u16 flags, u8 current_item_index,
                             CMDIF_REMOTEGUI_BUTTONFACE button0, CMDIF_REMOTEGUI_BUTTONFACE button1, CMDIF_REMOTEGUI_BUTTONFACE button2,
                             CMDIF_REMOTEGUI_FOCUS focus)
{
    if (cmdif_datainfo_responsedata.dataptr)
    {
        //[4:flags][1:reserved][1:button0][1:button1][1:button2][1:focus][1:reserved][1:count][1:sel_index]
        *((u32*)cmdif_datainfo_responsedata.dataptr) = flags;
        cmdif_datainfo_responsedata.dataptr[4] = 0;
        cmdif_datainfo_responsedata.dataptr[5] = (u8)button0;
        cmdif_datainfo_responsedata.dataptr[6] = (u8)button1;
        cmdif_datainfo_responsedata.dataptr[7] = (u8)button2;
        cmdif_datainfo_responsedata.dataptr[8] = (u8)focus;
        cmdif_datainfo_responsedata.dataptr[9] = 0;
        //cmdif_datainfo_responsedata.dataptr[10]: count when cmdif_remotegui_listboxbox_additem()
        cmdif_datainfo_responsedata.dataptr[11] = current_item_index;

        cmdif_internalresponse_ack(cmd,CMDIF_ACK_LISTBOX,
                                   cmdif_datainfo_responsedata.dataptr,
                                   cmdif_datainfo_responsedata.datalength);
        __free(cmdif_datainfo_responsedata.dataptr);
        cmdif_datainfo_responsedata.dataptr = NULL;
    }
}

/**
 * @brief   Cleanup listbox on AppBoard
 *
 * @author  Quyen Leba
 *
 */
void cmdif_remotegui_listboxbox_cleanup()
{
    if (cmdif_datainfo_responsedata.dataptr)
    {
        __free(cmdif_datainfo_responsedata.dataptr);
        cmdif_datainfo_responsedata.dataptr = NULL;
    }
    cmdif_datainfo_responsedata.datalength = 0;
}

/**
 * @brief   Show messagebox on AppBoard
 *
 * @param [in]  cmd
 * @param [in]  title
 * @param [in]  icon
 * @param [in]  button0 (NULL or "": not used) - exit/left button
 * @param [in]  button1 (NULL or "": not used) - right button
 * @param [in]  button2 (NULL or "": not used) - left/middle button
 * @param [in]  focus
 * @param [in]  text
 *
 * @retval none
 *
 * @author  Quyen Leba
 *
 * @details For compatibility with LWTS, only use CMDIF_REMOTEGUI_BUTTONFACE_Exit on button0
 *
 */
void cmdif_remotegui_messagebox(CMDIF_COMMAND cmd, const u8 *title, CMDIF_REMOTEGUI_ICON icon,
                                CMDIF_REMOTEGUI_BUTTONFACE button0, CMDIF_REMOTEGUI_BUTTONFACE button1, CMDIF_REMOTEGUI_BUTTONFACE button2,
                                CMDIF_REMOTEGUI_FOCUS focus, const u8 *text)
{
    u32 tmp_u32;
    u32 length;
    u8  *bptr[2];
    u16 bptr_length[2];
    u8  buffer[512];
    u8  i;

    //[4:flags][1:icon][1:button0][1:button1][1:button2][1:focus][3:reserved][m:title][NULL][n:text][NULL]

    *((u32*)buffer) = 0;    //flags
    buffer[4] = (u8)icon;
    buffer[5] = (u8)button0;
    buffer[6] = (u8)button1;
    buffer[7] = (u8)button2;
    buffer[8] = (u8)focus;
    buffer[9] = 0;
    buffer[10] = 0;
    buffer[11] = 0;
    length = 12;

    bptr[0] = (u8*)title;
    bptr[1] = (u8*)text;
    bptr_length[0] = 64;    //as max length
    bptr_length[1] = sizeof(buffer) - bptr_length[0] - length-2;
    
    for(i=0;i<2;i++)
    {
        if (!bptr[i])
        {
            bptr_length[i] = 0;
        }
        else
        {
            tmp_u32 = strlen((char*)bptr[i]);
            if (tmp_u32 < bptr_length[i])
            {
                bptr_length[i] = tmp_u32;
            }
        }

        if (bptr_length[i])
        {
            memcpy((char*)&buffer[length],(char*)bptr[i],bptr_length[i]);
            length += bptr_length[i];
        }
        buffer[length++] = 0;
    }

    cmdif_internalresponse_ack(cmd,CMDIF_ACK_MESSAGEBOX,buffer,length);
}

/**
 * @brief   Show full keyboard input on AppBoard
 *
 * @param [in]  cmd
 * @param [in]  title
 * @param [in]  initial_string
 * @param [in]  defaulttype
 *
 * @retval none
 *
 * @author  Quyen Leba
 *
 */
void cmdif_remotegui_keyboard(CMDIF_COMMAND cmd, const u8 *title, const u8 *initial_string,
                              CMDIF_REMOTEGUI_KEYBOARD_DEFAULTTYPE defaulttype)
{
    u32 length;
    u8  buffer[128];

    //[4:flags][7:reserved][1:default type][m:title][NULL][n:initial string][NULL]
    // default type: see CMDIF_REMOTEGUI_KEYBOARD_DEFAULTTYPE

    memset((char*)buffer,0,sizeof(buffer));
    buffer[11] = (u8)defaulttype;
    length = 12;

    strncpy((char*)&buffer[12],(char*)title,48);
    length += (strlen((char*)&buffer[12])+1);
    strncpy((char*)&buffer[length],(char*)initial_string,31);
    length += (strlen((char*)&buffer[length])+1);

    cmdif_internalresponse_ack(cmd,CMDIF_ACK_KEYBOARD_INPUT,buffer,length);
}

/**
 * @brief   Show numpad input on AppBoard
 *
 * @param [in]  cmd
 * @param [in]  title
 * @param [in]  unit
 * @param [in]  inputtype
 *
 * @retval none
 *
 * @author  Quyen Leba
 *
 */
void cmdif_remotegui_numpad(CMDIF_COMMAND cmd, const u8 *title, const u8 *unit,
                            CMDIF_REMOTEGUI_NUMPAD_INPUTTYPE inputtype,
                            float initial_value, float range_low, float range_high)
{
    u32 length;
    u8  buffer[128];

    //[4:flags][7:reserved][1:input type][m:title][NULL][n:unit string][NULL][4:initial value][4:range low][4:range high]
    // value must be within range low & high
    // input type: see CMDIF_REMOTEGUI_NUMPAD_INPUTTYPE

    memset((char*)buffer,0,sizeof(buffer));
    buffer[11] = (u8)inputtype;
    length = 12;

    strncpy((char*)&buffer[12],(char*)title,48);
    length += (strlen((char*)&buffer[12])+1);
    strncpy((char*)&buffer[length],(char*)unit,16);
    length += (strlen((char*)&buffer[length])+1);

    switch(inputtype)
    {
    case CMDIF_REMOTEGUI_NUMPAD_INPUTTYPE_UNSIGNEDINT:
        *(u32*)&buffer[length] = (u32)initial_value;
        *(u32*)&buffer[length+4] = (u32)range_low;
        *(u32*)&buffer[length+8] = (u32)range_high;
        break;
    case CMDIF_REMOTEGUI_NUMPAD_INPUTTYPE_SIGNINT:
        *(s32*)&buffer[length] = (s32)initial_value;
        *(s32*)&buffer[length+4] = (s32)range_low;
        *(s32*)&buffer[length+8] = (s32)range_high;
        break;
    case CMDIF_REMOTEGUI_NUMPAD_INPUTTYPE_FLOAT:
        *(float*)&buffer[length] = initial_value;
        *(float*)&buffer[length+4] = range_low;
        *(float*)&buffer[length+8] = range_high;
        break;
    default:
        buffer[11] = CMDIF_REMOTEGUI_NUMPAD_INPUTTYPE_FLOAT;
        *(float*)&buffer[length] = initial_value;
        *(float*)&buffer[length+4] = range_low;
        *(float*)&buffer[length+8] = range_high;
        break;
    }
    length += (3*4);

    cmdif_internalresponse_ack(cmd,CMDIF_ACK_NUMPAD_INPUT,buffer,length);
}

/**
 * @brief   Terminate remote menu
 *
 * @param [in]  cmd
 *
 * @retval u8   status
 *
 * @author  Quyen Leba
 *
 */
u8 cmdif_remotegui_terminate(CMDIF_COMMAND cmd)
{
    cmdif_internalresponse_ack(cmd,CMDIF_ACK_TERMINATE,NULL,0);
    return S_SUCCESS;
}

/**
 * @brief   Peek for user response from a remote GUI
 *
 * @param [out] responsetype
 * @param [out] responsechoice  button or index of listbox
 * @param [out] responsedata    numpad value or keyboard string
 *
 * @retval u8   status (S_SUCCESS: valid response received)
 *
 * @author  Quyen Leba
 *
 */
u8 cmdif_remotegui_peek_user_response(CMDIF_REMOTEGUI_RESPONSETYPE *responsetype,
                                      u16 *responsechoice, u8 *responsedata)
{
    u8  data[64];
    u16 datalength;
    u8  status;

    cmdif_datainfo_responsedata.responsecode = CMDIF_ACK_FAILED;
    cmdif_datainfo_responsedata.dataptr = NULL;
    cmdif_datainfo_responsedata.datalength = 0;

    status = cmdif_receive_command_internal(data,&datalength,sizeof(data));
    if (status == S_SUCCESS)
    {
        switch((CMDIF_COMMAND)data[1])
        {
        case CMDIF_CMD_KEY_CONFIRM:
            if (datalength >= (4+8) && datalength <= (4+8+32))  //payload starts at 4
            {
                //[1:responsetype][1:reserved][2:responsechoice][32:responsedata]
                cmdif_datainfo_responsedata.responsecode = CMDIF_ACK_OK;
                *responsetype = (CMDIF_REMOTEGUI_RESPONSETYPE)data[4];
                *responsechoice = *(u16*)&data[6];

                if (responsedata)
                {
                    memcpy((char*)responsedata,(char*)&data[8],datalength-8);
                }

                status = S_SUCCESS;
            }
            else if (datalength >= (4+8))   //payload starts at 4
            {
                //[1:responsetype][1:reserved][2:responsechoice][4:reserved]
                cmdif_datainfo_responsedata.responsecode = CMDIF_ACK_OK;
                *responsetype = (CMDIF_REMOTEGUI_RESPONSETYPE)data[4];
                *responsechoice = *(u16*)&data[6];

                status = S_SUCCESS;
            }
            else
            {
                status = S_BADCONTENT;
            }
            break;
        default:
            //not the expecting response
            status = S_UNMATCH;
            break;
        }

        cmdif_internalresponse_ack((CMDIF_COMMAND)data[1],
                                   cmdif_datainfo_responsedata.responsecode,
                                   cmdif_datainfo_responsedata.dataptr,
                                   cmdif_datainfo_responsedata.datalength);
    }
    else
    {
        //no response
        status = S_EMPTY;
    }    
    return status;
}

/**
 * @brief   Wait for user response from a remote GUI
 *
 * @param [out] responsetype
 * @param [out] responsechoice  button or index of listbox
 * @param [out] responsedata    numpad value or keyboard string
 *
 * @retval u8   status (S_SUCCESS: valid response received)
 *
 * @author  Quyen Leba
 *
 */
u8 cmdif_remotegui_wait_user_response(CMDIF_REMOTEGUI_RESPONSETYPE *responsetype,
                                      u16 *responsechoice, u8 *responsedata)
{
    u8  status;

    //TODOQ: might add timeout
    do
    {
        status = cmdif_remotegui_peek_user_response(responsetype,responsechoice,responsedata);
    }while(status != S_SUCCESS);

    return status;
}

/**
 * @brief   Wait for user response from a remote keyboard GUI
 *
 * @param [out] value_out
 * @param [in]  maxlength
 *
 * @retval u8   status      S_SUCCESS: valid response received, S_USERABORT: cancelled, others: failure
 *
 * @author  Quyen Leba
 *
 */
u8 cmdif_remotegui_wait_user_response_keyboard(u8 *string_out, u8 maxlength)
{
    CMDIF_REMOTEGUI_RESPONSETYPE responsetype;
    u16 responsechoice;
    u8  responsedata[32];
    u8  length;
    u8  status;

    cmdif_remotegui_wait_user_response(&responsetype,&responsechoice,responsedata);

    if (responsetype == CMDIF_REMOTEGUI_RESPONSETYPE_KEYBOARD)
    {
        length = strlen((char*)responsedata);
        if (length < maxlength)
        {
            strcpy((char*)string_out,(char*)responsedata);
        }
        else
        {
            memcpy((char*)string_out,(char*)responsedata,sizeof(responsedata)-1);
            string_out[sizeof(responsedata)-1] = 0;
        }
        status = S_SUCCESS;
    }
    else if (responsetype == CMDIF_REMOTEGUI_RESPONSETYPE_USERABORT)
    {
        status = S_USERABORT;
    }
    else if (responsetype == CMDIF_REMOTEGUI_RESPONSETYPE_ERROR)
    {
        status = S_ERROR;
    }
    else
    {
        status = S_FAIL;
    }

    return status;
}

/**
 * @brief   Wait for user response from a remote numpad GUI
 *
 * @param [out] value_out
 *
 * @retval u8   status      S_SUCCESS: valid response received, S_USERABORT: cancelled, others: failure
 *
 * @author  Quyen Leba
 *
 */
u8 cmdif_remotegui_wait_user_response_numpad(float *value_out)
{
    CMDIF_REMOTEGUI_RESPONSETYPE responsetype;
    u16 responsechoice;
    u8  responsedata[32];
    u8  status;

    cmdif_remotegui_wait_user_response(&responsetype,&responsechoice,responsedata);

    if (responsetype == CMDIF_REMOTEGUI_RESPONSETYPE_NUMPAD)
    {
        *value_out = *(float*)responsedata;
        status = S_SUCCESS;
    }
    else if (responsetype == CMDIF_REMOTEGUI_RESPONSETYPE_USERABORT)
    {
        status = S_USERABORT;
    }
    else if (responsetype == CMDIF_REMOTEGUI_RESPONSETYPE_ERROR)
    {
        status = S_ERROR;
    }
    else
    {
        status = S_FAIL;
    }

    return status;
}

//------------------------------------------------------------------------------
// cmdif_remotegui_msg_keycycle --- no cancel
// Input:   cmd, title
// Output:  None
// Return:  None
// Engineer: Rhonda Rusak
//------------------------------------------------------------------------------
void cmdif_remotegui_msg_keycycle(CMDIF_COMMAND cmd, const u8 *title)
{
    CMDIF_REMOTEGUI_RESPONSETYPE responsetype;    
    u16 responsechoice;


        cmdif_remotegui_messagebox(cmd,
                                   title, CMDIF_REMOTEGUI_ICON_NONE,
                                   CMDIF_REMOTEGUI_BUTTONFACE_None, CMDIF_REMOTEGUI_BUTTONFACE_Continue, 
                                   CMDIF_REMOTEGUI_BUTTONFACE_None,
                                   CMDIF_REMOTEGUI_FOCUS_Button1,
                                   "<HEADER>CYCLE KEY</HEADER>\nTurn Key off, then back on");

    cmdif_remotegui_wait_user_response(&responsetype, &responsechoice, NULL);
}
/**
 * @brief   Request key off
 *
 * @param [in]  cmd
 *
 * @parm [in]  *title
 *
 * @author  Ricardo Cigarroa
 *
 */
void cmdif_remotegui_msg_keyoff(CMDIF_COMMAND cmd, const u8 *title)
{
    CMDIF_REMOTEGUI_RESPONSETYPE responsetype;    
    u16 responsechoice;

    // turn key off and hit continue
    cmdif_remotegui_messagebox(cmd,
                               title, CMDIF_REMOTEGUI_ICON_KEYOFF,
                               CMDIF_REMOTEGUI_BUTTONFACE_None, CMDIF_REMOTEGUI_BUTTONFACE_Continue, CMDIF_REMOTEGUI_BUTTONFACE_None,
                               CMDIF_REMOTEGUI_FOCUS_Button1,
                               "<HEADER>TURN KEY OFF</HEADER>\nTurn the vehicle's ignition key to the OFF position then press CONTINUE");
    cmdif_remotegui_wait_user_response(&responsetype, &responsechoice, NULL);
}

/**
 * @brief   Request key on
 *
 * @param [in]  cmd
 *
 * @parm [in]  *title
 *
 * @author  Ricardo Cigarroa
 *
 */
void cmdif_remotegui_msg_keyon(CMDIF_COMMAND cmd, const u8 *title)
{
    CMDIF_REMOTEGUI_RESPONSETYPE responsetype;
    u16 responsechoice;

    // turn key on and hit continue
    cmdif_remotegui_messagebox(cmd,
                               title, CMDIF_REMOTEGUI_ICON_KEYON,
                               CMDIF_REMOTEGUI_BUTTONFACE_None, CMDIF_REMOTEGUI_BUTTONFACE_Continue, CMDIF_REMOTEGUI_BUTTONFACE_None,
                               CMDIF_REMOTEGUI_FOCUS_Button1,
                               "<HEADER>TURN KEY ON</HEADER>\nTurn the vehicle's ignition key to the ON position or " \
                               "push & hold the Engine Start Button for 2-3 seconds. " \
                               "DO NOT START the engine. " \
                               "Press CONTINUE when ready.");
    cmdif_remotegui_wait_user_response(&responsetype, &responsechoice, NULL);
}
