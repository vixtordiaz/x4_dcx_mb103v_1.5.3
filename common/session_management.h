/**
  **************** (C) COPYRIGHT 2011 SCT Performance, LLC *********************
  * File Name          : session_management.h
  * Device / System    : SCT products using the "CommonCode" platform
  * Author             : Mark Davis
  *
  * Version            : 1 
  * Date               : 12/06/2011
  * Description        : header file for session_management.c - Manage 
  *                    : disconnect - reconnect activity between flash
  *                    : tuner sub-system and user-interface sub-system.
  *
  *
  * History            : 12/06/2011 M. Davis
  *                    :   Creation
  *
  ******************************************************************************
  */

#ifndef __SESSION_MANAGEMENT_H
#define __SESSION_MANAGEMENT_H

#include <arch/gentype.h>

enum TUNER_ACTIVITY
{
    tuner_activity_cmdifIdle = 1,
    tuner_activity_programUpload,
    tuner_activity_programDownloadInit,
    tuner_activity_programDownloadDo,
    tuner_activity_retToStockRegRecovery,
    tuner_activity_retToStockDownloadInit,
    tuner_activity_retToStockDownloadDo,
    tuner_activity_performanceSetup,
    tuner_activity_performanceRun,
    tuner_activity_gaugesSetup,
    tuner_activity_gaugesRun,
    tuner_activity_datalogSetup,
    tuner_activity_datalogRun,
    tuner_activity_diagnosticsReadDTC,
    tuner_activity_diagnosticsClearDTC,
    tuner_activity_infoGetAccInfo,
    tuner_activity_infoGetVehInfo,
};

// The following enum is used to identify the point in the
// code where a high-level cmdif_internalresponse_ack() or 
// cmdif_internalresponse_ack_nowait() call is made, to send data 
// to the controlling app.  
// On the iTSX system, both the cmdif_internalresponse_ack()  and 
// cmdif_internalresponse_ack_nowait() high-level Bluetooth-send 
// calls are mapped, in cmdif_MB103V.h, to the low-level 
// bluetooth_send_ack() function.
//
// The calls that send data to the app can be made directly
// from Level 0, or from lower-level functions called from Level 0.
enum APP_TX_CALLED_FROM_FN
{
    from_L0_cmdif_handler = 1,
    from_L0_cmdif_func_datalog_scan,
    from_L0_cmdif_func_datalog_start,
    from_L0_cmdif_func_uploadtune_do,
    from_L0_cmdif_func_downloadtune_init,
    from_L0_cmdif_func_downloadtune_do,
    from_L0_cmdif_func_recoverystock_register,
    from_L0_cmdif_func_hardware,

// The following Level 1 functions are all called from one of the
// preceding Level 0 functions. Calls that send data to the app 
// can be made directly from this level, or from functions called 
// from this level.
    L1_idle = 0x20,
    from_L1_obd2datalog_getvalidpidlist_fromfile = 0x21, // called from cmdif_func_datalog_scan()
    from_L1_filestock_generate_flashfile_fromstock,  // called from cmdif_func_downloadtune_init()
    from_L1_filestock_generate_flashfile_fromfile,   // called from cmdif_func_downloadtune_init()
    from_L1_obd2tune_uploadstock,                    // called from cmdif_func_uploadtune_do()
    from_L1_obd2tune_downloadtune,                   // called from cmdif_func_downloadtune_do()

// The following Level 2 functions are all called from one of the
// preceding Level 1 functions. Calls that send data to the app 
// can be made directly from this level, or from functions called 
// from this level.
    L2_idle = 0x30,
    from_L2_download_ecm = 0x31,   // called from L1_obd2tune_downloadtune()
    from_L2_upload_ecm,            // called from L1_obd2tune_uploadstock()

// The following Level 3 functions are all called from one of the
// preceding Level 2 functions. Calls that send data to the app 
// can be made directly from this level.

    L3_idle = 0x40,
    from_L3_download_ecm_memoryblock = 0x41,  // called from L2_download_ecm()
    from_L3_upload_ecm_memoryblock,                  // called from L2_upload_ecm()
    from_L3_generate_ecm_stock,                      // called from L2_upload_ecm()
};

#if 0 // we're not ready to use this yet.
typedef struct
{
    u8 activityStep[32];  
}BluetoothDisconnectReconnectBehaviorTable;
#endif

typedef struct
{
    enum TUNER_ACTIVITY currentActivity;
    enum APP_TX_CALLED_FROM_FN L0_caller;
    u8  tuner_L0_tx_step; // the "sequence point" where the bluetooth_send_ack() call was made
    enum APP_TX_CALLED_FROM_FN L1_caller;
    u8  tuner_L1_tx_step; // the "sequence point" where the bluetooth_send_ack() call was made
    enum APP_TX_CALLED_FROM_FN L2_caller;
    u8  tuner_L2_tx_step; // the "sequence point" where the bluetooth_send_ack() call was made
    enum APP_TX_CALLED_FROM_FN L3_caller;
    u8  tuner_L3_tx_step; // the "sequence point" where the bluetooth_send_ack() call was made
}CallHistory;

typedef struct
{
    CallHistory PriorSessionCloseCalledFrom; // Accessory execution state on the prior session drop
    CallHistory LastSessionOpenCalledFrom;   // Accessory execution state when current session was opened
    CallHistory LastSessionCloseCalledFrom;  // Accessory execution state on the last session drop
    CallHistory LastCalledFrom;              // Accessory execution state at the point of a bluetooth_send_ack() call 
}SessionMgmtInfo;

//
// The following session-resume messages are all defined in session_management.c
//

// session-resume "program" messages
extern const u8 msg_sessionResume_ProgramCancelled[];
extern const u8 msg_sessionResume_RetToStockCancelled[];
extern const u8 msg_sessionResume_ProgramHalted[];
extern const u8 msg_sessionResume_RetToStockHalted[];
extern const u8 msg_sessionResume_ProgramPaused[];
extern const u8 msg_sessionResume_RetToStockPaused[];
extern const u8 msg_sessionResume_ProgramFinished[];
extern const u8 msg_sessionResume_RetToStockFinished[];
extern const u8 msg_sessionResume_ProgramError[];
extern const u8 msg_sessionResume_RetToStockError[];

// session-resume "performance" messages
extern const u8 msg_sessionResume_PerformanceSetupPaused[];
extern const u8 msg_sessionResume_PerformanceHalted[];
extern const u8 msg_sessionResume_PerformanceError[];

// session-resume "gauges" messages
extern const u8 msg_sessionResume_GaugesSetupPaused[];
extern const u8 msg_sessionResume_GaugesSetupError[];
extern const u8 msg_sessionResume_GaugesSetupFinished[];
extern const u8 msg_sessionResume_GaugesPaused[];
extern const u8 msg_sessionResume_GaugesError[];

// session-resume "diagnostics" messages
extern const u8 msg_sessionResume_DTC_ReadPaused[];
extern const u8 msg_sessionResume_DTC_ClearFinished[];
extern const u8 msg_sessionResume_DTC_Error[];

// session-resume "info" messages
extern const u8 msg_sessionResume_InfoPaused[];
extern const u8 msg_sessionResume_InfoError[];

extern SessionMgmtInfo sessionMgmt_info;

void session_mgmt_accActivityUpdate(enum TUNER_ACTIVITY activity);
void session_mgmt_callHistoryUpdate(enum APP_TX_CALLED_FROM_FN calledFromFn, u8 tuner_tx_step);

#endif    //__SESSION_MANAGEMENT_H