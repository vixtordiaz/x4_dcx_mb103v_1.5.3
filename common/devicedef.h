/**
  **************** (C) COPYRIGHT 2011 SCT Performance, LLC *********************
  * File Name          : devicedef.h
  * Device / System    : SCT products using the "CommonCode" platform
  * Author             : Quyen Leba
  *
  * Version            : 1 
  * Date               : 05/02/2011
  * Description        : 
  *                    : 
  *
  *
  * History            : 05/02/2011 M. Davis
  *                    :   Add SCT Copyright header,
  *
  ******************************************************************************
  */

#ifndef __DEVICEDEF_H
#define __DEVICEDEF_H

#define MARRIED_COUNT_MAX   4

//------------------------------------------------------------------------------
// Define Device Type
//------------------------------------------------------------------------------
#define REGLW   0x00        // 00 -  Regular LiveWire
#define FLEETFD 0x01        // 01 -  FLEET FORD device
#define FLEETGM 0x02        // 02 -  FLEET GM device
#define B64LW   0x10        // 16 -  Basic 6.4L LiveWire
#define E64LW   0x20        // 32 -  Extreme!! ARRRRGGG! 6.4L LiveWire
#define O64LW   0x30        // 48 -  Extreme!! Offroad 6.4L LiveWire
#define REGX3   0x40        // 64 -  X3 & X3P Power Flash - Binary and Strategy
#define ECONO   0x50        // 80 -  X3 Economizer X3 & X3P
#define BANKS   0x60        // 96 -  BANKS ENGINEERING Ford
#define BINX3   0X70        // 112 - Binary flash-only X3 & X3P
#define GMX3    0x80        // 128 - GM X3 & X3P
#define BX364   0x90        // 144 - Basic 6.4L enabled X3P
#define BKS64   0xA0        // 160 - Banks 6.4L enabled X3P
#define GMBINX3 0XB0        // 176 - GM Binary flash-only X3P
#define EX364   0xC0        // 192 - Extreme!! ARRRRGGG! 6.4L X3P
#define OX364   0xD0        // 208 - Extreme!! Offroad 6.4L X3P
#define GMBANKS 0xE0        // 224 - BANKS GM
#define X3PEO   0xF0        // 240 - EO device for carb, 3015 strategy only

#define TSXD_FORD   8901    // Tsx Dongle Ford
#define TSXD_GM     9901    // Tsx Dongle GM    //TODOQ: # is for now

//------------------------------------------------------------------------------
// Define Device Type
//------------------------------------------------------------------------------
typedef enum
{
    DeviceType_iTSX_FORD        = 9000,
    DeviceType_iTSX_GM          = 9100,
    DeviceType_iTSX_DCX         = 9200,
    DeviceType_Ken_FORD         = 9010,
    DeviceType_Ken_GM           = 9110,
    DeviceType_Ken_DCX          = 9210,
    DeviceType_X4_FORD          = 9020,
    DeviceType_X4_GM            = 9120,
    DeviceType_X4_DCX           = 9220,
    DeviceType_Unknown          = 0xFFEFFFEF,
    DeviceType_Any              = 0xFFFFFFFF, 
}DeviceType;

//------------------------------------------------------------------------------
// Define Market Type
//------------------------------------------------------------------------------
typedef enum
{
    MarketType_US               = 0,
    MarketType_AU               = 1,
    MarketType_UK               = 2,
    MarketType_EO               = 3,    //CARB market byte
    MarketType_CN               = 4,
    MarketType_Unknown          = 0xFF,
    MarketType_Any              = 0xFFFFFFFF,
}MarketType;

#define US_MARKET_TYPE_STRING   "US"
#define AU_MARKET_TYPE_STRING   "AU"
#define UK_MARKET_TYPE_STRING   "UK"
#define EO_MARKET_TYPE_STRING   "EO"
#define CN_MARKET_TYPE_STRING   "CN"
#define UNKNOWN_MARKET_TYPE_STRING   "N/A"

//------------------------------------------------------------------------------
// Project specific device config
//------------------------------------------------------------------------------
#include <device_config.h>

//------------------------------------------------------------------------------
// Market unlock key
//------------------------------------------------------------------------------
#define TEA_KEY_0_US            0x0F38409F
#define TEA_KEY_1_US            0x03C350A2
#define TEA_KEY_2_US            0xD6E59954
#define TEA_KEY_3_US            0x43542102

#define TEA_KEY_0_AU            0x37C8EC86
#define TEA_KEY_1_AU            0x078AEFDF
#define TEA_KEY_2_AU            0x5DB0E60B
#define TEA_KEY_3_AU            0x19BF5A87

#define TEA_KEY_0_UK            0x3941C995
#define TEA_KEY_1_UK            0x5607E9EC
#define TEA_KEY_2_UK            0x2F3A8FC9
#define TEA_KEY_3_UK            0x293CEE63

#define TEA_KEY_0_EO            0x928F5990
#define TEA_KEY_1_EO            0x81159952
#define TEA_KEY_2_EO            0x65198178
#define TEA_KEY_3_EO            0xDA6F371A

#define TEA_KEY_0_CN            0xB078E40E
#define TEA_KEY_1_CN            0x3B16DC72
#define TEA_KEY_2_CN            0x9B8BA79D
#define TEA_KEY_3_CN            0x53663802

#define TEA_KEY_0_M(m)          TEA_KEY_0_##m
#define TEA_KEY_1_M(m)          TEA_KEY_1_##m
#define TEA_KEY_2_M(m)          TEA_KEY_2_##m
#define TEA_KEY_3_M(m)          TEA_KEY_3_##m

#endif    //__DEVICEDEF_H
