/**
  **************** (C) COPYRIGHT 2011 SCT Performance, LLC *********************
  * File Name          : cmdif_func_uploadtune.h
  * Device / System    : SCT products using the "CommonCode" platform
  * Author             : Quyen Leba
  *
  * Version            : 1 
  * Date               : 05/02/2011
  * Description        : 
  *                    : 
  *
  *
  * History            : 05/02/2011 M. Davis
  *                    :   Add SCT Copyright header,
  *
  ******************************************************************************
  */

#ifndef __CMDIF_FUNC_UPLOADTUNE_H
#define __CMDIF_FUNC_UPLOADTUNE_H

#include <arch/gentype.h>

u8 cmdif_func_uploadtune_init();
u8 cmdif_func_uploadtune_do();
u8 cmdif_func_uploadtune_vehicle_type_search();
u8 cmdif_func_uploadtune_compile_user_vehicle_type();
u8 cmdif_func_get_upload_stock();

#endif    //__CMDIF_FUNC_UPLOADTUNE_H
