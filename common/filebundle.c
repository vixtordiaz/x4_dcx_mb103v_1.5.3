/**
  **************** (C) COPYRIGHT 2011 SCT Performance, LLC *********************
  * File Name          : filebundle.c
  * Device / System    : SCT products using the "CommonCode" platform
  * Author             : Quyen Leba
  *
  * Version            : 1 
  * Date               : 05/02/2011
  * Description        : 
  *                    : 
  *
  *
  * History            : 05/02/2011 M. Davis
  *                    :   Add SCT Copyright header,
  *
  ******************************************************************************
  */

#include <fs/genfs.h>
#include <board/genplatform.h>
#include <common/statuscode.h>
#include <common/crc32.h>
#include <common/file.h>
#include <common/filecrypto.h>
#include <common/crypto_blowfish.h>
#include "filebundle.h"

#define FILEBUNDLE_CHILD_FILENAME_MAXLENGTH     128

struct
{
    F_FILE *fptr;
    u32 bundlefile_offset;  //start position of bundle file in its parent file (bundle file might not have parent file; i.e. standalone)
    u32 childfile_offset;   //start position of child file in bundle file's parent file
    u8  childfilename[32];
    u32 fileposition;
    u32 filesize;
}filebundleinfo =
{
    .fptr = NULL,
    .bundlefile_offset = 0,
    .childfile_offset = 0,
};

u8 filebundle_getfullheader(FileCryptoHeaderInfo *general_header,
                            u8 *header_info, u8 *header_extended,
                            u32 *header_extended_length);
u8 filebundle_checkfilecrc32e(FileCryptoHeaderInfo *general_header,
                              u32 start_position, u32 *crc32_e_output);
u8 filebundle_getfilerawdata(s32 start_position, u32 length,
                             u8 *data_output, u32 *data_length);

/**
 * filebundle_openfirstfile_by_fptr
 *
 * @brief   Open 1st file in bundle file (readonly access)
 *
 * @param   [in]    fptr
 * @param   [in]    offset          starting position of bundle file in parent file
 * @param   [in]    dofilevalidation
 * @param   [out]   child_filename  filename of child file in bundle file
 *
 * @retval u8  status
 *
 * @author  Quyen Leba
 *
 */
u8 filebundle_openfirstfile_by_fptr(F_FILE *fptr, u32 offset, bool dofilevalidation,
                                    u8 *child_filename)
{
    u8  status;

    status = filebundle_openfile_by_fptr(fptr,offset,NULL,TRUE);
    if (status == S_SUCCESS)
    {
        if (child_filename)
        {
            strcpy((char*)child_filename,(char*)filebundleinfo.childfilename);
        }
    }
    return status;
}

/**
 * filebundle_opennextfile_by_fptr
 *
 * @brief   Open next file in bundle file (readonly access). Must already open a child file
 *
 * @param   [out]   child_filename  filename of child file in bundle file
 *
 * @retval u8  status
 *
 * @author  Quyen Leba
 *
 */
u8 filebundle_opennextfile_by_fptr(u8 *child_filename)
{
    FileCryptoFileBundleDataNode fb_data_node;
    u8  filename[FILEBUNDLE_CHILD_FILENAME_MAXLENGTH];
    u8  buffer[32];
    u32 bufferlength;
    u32 fileindex;
    u32 totalfiledatalength;
    u8  status;

    totalfiledatalength = ((filebundleinfo.filesize + 7) / 8) * 8;  //including filedata padding
    fileindex = filebundleinfo.childfile_offset+totalfiledatalength;

    status = filebundle_getfilerawdata(fileindex,16,buffer,&bufferlength);
    if (status != S_SUCCESS)
    {
        return status;
    }
    crypto_blowfish_decryptblock_external_key(buffer,16);
    memcpy((char*)&fb_data_node, (char*)buffer, 12);
    memcpy((char*)filename, (char*)&buffer[12], 4);
    
    if (fb_data_node.filenamelength >= FILEBUNDLE_CHILD_FILENAME_MAXLENGTH)
    {
        return S_BADCONTENT;
    }
    
    fileindex += 16;
    status = filebundle_getfilerawdata(fileindex, fb_data_node.filenamelength - 4,
                                       &filename[4],&bufferlength);
    crypto_blowfish_decryptblock_external_key(&filename[4],bufferlength);
    filename[(u32)fb_data_node.filenamelength] = 0;
    fileindex += (fb_data_node.filenamelength - 4);

    //TODOQK: handle path list
    
    totalfiledatalength = ((fb_data_node.filesize + 7) / 8) * 8;    //including filedata padding
    //filepaddinglength = totalfiledatalength - fb_data_node.filesize;

    //found next child file in bundle file
    filebundleinfo.childfile_offset = ftell(filebundleinfo.fptr) + filebundleinfo.bundlefile_offset;
    filebundleinfo.filesize = fb_data_node.filesize;
    strcpy((char*)filebundleinfo.childfilename,(char*)filename);

    if (child_filename)
    {
        strcpy((char*)child_filename,(char*)filename);
    }

    return S_SUCCESS;
}

/**
 * filebundle_openfirstfile
 *
 * @brief   Open 1st file in bundle file (readonly access)
 *
 * @param   [in]    bundle_filename
 * @param   [in]    offset          starting position of bundle file in parent file
 * @param   [in]    dofilevalidation
 * @param   [out]   child_filename  filename of child file in bundle file
 *
 * @retval u8  status
 *
 * @author  Quyen Leba
 *
 */
u8 filebundle_openfirstfile(const u8 *bundle_filename, u32 bundle_fileoffset, bool dofilevalidation,
                            u8 *child_filename)
{
    filebundle_closefile();
    filebundleinfo.fptr = genfs_general_openfile(bundle_filename,"r");
    if (!filebundleinfo.fptr)
    {
        return S_OPENFILE;
    }
    return filebundle_openfirstfile_by_fptr(filebundleinfo.fptr, bundle_fileoffset, dofilevalidation, child_filename);
}

/**
 * filebundle_opennextfile
 *
 * @brief   Open next file in bundle file (readonly access). Must already open a child file
 *
 * @param   [out]   child_filename  filename of child file in bundle file
 *
 * @retval u8  status
 *
 * @author  Quyen Leba
 *
 */
u8 filebundle_opennextfile(u8 *child_filename)
{
    return filebundle_opennextfile_by_fptr(child_filename);
}

/**
 * filebundle_openfile_by_fptr
 *
 * @brief   Open a file in bundle file (readonly access)
 *
 * @param   [in]    fptr
 * @param   [in]    offset          starting position of bundle file in parent file
 * @param   [in]    child_filename  file in bundle file. If NULL, find 1st child file
 * @param   [in]    dofilevalidation
 *
 * @retval u8  status
 *
 * @author  Quyen Leba
 *
 */
u8 filebundle_openfile_by_fptr(F_FILE *fptr, u32 offset, const u8 *child_filename, bool dofilevalidation)
{
    FileCryptoHeaderInfo general_header;
    u8  header_info[FILECRYPTO_INFO_HEADER_SIZE];
    u8  header_extended[FILECRYPTO_EXTENDED_HEADER_MAX_SIZE];
    u32 header_extended_length;
    FileBundleBasicInfo fb_header;
    FileCryptoFileBundleDataNode fb_data_node;
    u8  buffer[32];
    u32 bufferlength;
    u32 fileindex;
    u32 crc32e_out;
    u32 totalfiledatalength;
    //u32 filepaddinglength;
    u32 i;
    u8  status;

    filebundleinfo.fileposition = 0;
    filebundleinfo.filesize = 0;
    filebundleinfo.bundlefile_offset = 0;
    filebundleinfo.childfile_offset = 0;
    fb_data_node.filename = NULL;
    fb_data_node.padding = NULL;
    fb_data_node.filedata = NULL;

    if (!fptr)
    {
        return S_INPUT;
    }
    if (fseek(fptr,offset,SEEK_SET) != 0)
    {
        return S_SEEKFILE;
    }
    filebundleinfo.bundlefile_offset = offset;
    filebundleinfo.fptr = fptr;

    status = filebundle_getfullheader(&general_header, header_info,
                                      header_extended, &header_extended_length);
    if (status != S_SUCCESS)
    {
        goto filebundle_openfile_by_fptr_done;
    }
    if ((general_header.ftype != FileCryptoFileType_FileBundle) ||
        (general_header.fsubtype != FILESUBTYPE_NONE))
    {
        status = S_BADCONTENT;
        goto filebundle_openfile_by_fptr_done;
    }

    if (dofilevalidation)
    {
        status = filebundle_checkfilecrc32e(&general_header, general_header.hsize, &crc32e_out);
        if (status != S_SUCCESS)
        {
            goto filebundle_openfile_by_fptr_done;
        }
    }

    memcpy((char*)&fb_header,(char*)header_info,sizeof(fb_header));

    //TODOQ: more header check (serial#, etc)
    //TODOQ: get path list

    fb_data_node.filename = __malloc(FILEBUNDLE_CHILD_FILENAME_MAXLENGTH);
    if (!fb_data_node.filename)
    {
        status = S_MALLOC;
        goto filebundle_openfile_by_fptr_done;
    }

    fileindex = general_header.hsize + filebundleinfo.bundlefile_offset;
    for (i = 0; i < fb_header.filecount; i++)
    {
        status = filebundle_getfilerawdata(fileindex,16,buffer,&bufferlength);
        if (status != S_SUCCESS)
        {
            goto filebundle_openfile_by_fptr_done;
        }
        crypto_blowfish_decryptblock_external_key(buffer,16);
        memcpy((char*)&fb_data_node, (char*)buffer, 12);
        //fb_data_node.filename = __malloc(fb_data_node.filenamelength + 1);    //malloc'ed aboved
        memcpy((char*)fb_data_node.filename, (char*)&buffer[12], 4);

        if (fb_data_node.filenamelength >= FILEBUNDLE_CHILD_FILENAME_MAXLENGTH)
        {
            status = S_BADCONTENT;
            goto filebundle_openfile_by_fptr_done;
        }

        fileindex += 16;
        status = filebundle_getfilerawdata(fileindex, fb_data_node.filenamelength - 4,
                                           &fb_data_node.filename[4],
                                           &bufferlength);
        crypto_blowfish_decryptblock_external_key(&fb_data_node.filename[4],bufferlength);
        fb_data_node.filename[(u32)fb_data_node.filenamelength] = 0;
        fileindex += (fb_data_node.filenamelength - 4);

        //TODOQK: handle path list
        
        totalfiledatalength = ((fb_data_node.filesize + 7) / 8) * 8;    //including filedata padding
        //filepaddinglength = totalfiledatalength - fb_data_node.filesize;

        if (!child_filename)
        {
            //found 1st child file in bundle file
            filebundleinfo.childfile_offset = ftell(filebundleinfo.fptr) + filebundleinfo.bundlefile_offset;
            filebundleinfo.filesize = fb_data_node.filesize;
            strcpy((char*)filebundleinfo.childfilename,(char*)fb_data_node.filename);

            status = S_SUCCESS;
            goto filebundle_openfile_by_fptr_done;
        }
        else if (strstr((char*)fb_data_node.filename,(char*)child_filename))
        {
            //found child file in bundle file
            filebundleinfo.childfile_offset = fileindex;
            filebundleinfo.filesize = fb_data_node.filesize;
            strcpy((char*)filebundleinfo.childfilename,(char*)fb_data_node.filename);

            status = S_SUCCESS;
            goto filebundle_openfile_by_fptr_done;
        }

        fileindex += totalfiledatalength;   //move to next file node
    }
    status = S_FILENOTFOUND;

filebundle_openfile_by_fptr_done:
    if (status != S_SUCCESS)
    {
        filebundleinfo.childfile_offset = 0;
    }
    if (fb_data_node.filename)
    {
        __free(fb_data_node.filename);
    }

    return status;
}

/**
 * filebundle_openfile
 *
 * @brief   Open a file in bundle file (readonly access)
 *
 * @param   [in]    bundle_filename
 * @param   [in]    offset          starting position of bundle file in parent file
 * @param   [in]    child_filename  file in bundle file
 * @param   [in]    dovalidatefile
 *
 * @retval u8  status
 *
 * @author  Quyen Leba
 *
 */
u8 filebundle_openfile(const u8 *bundle_filename, u32 offset, const u8 *child_filename, bool dovalidatefile)
{
    u8  status;

    filebundleinfo.childfile_offset = 0;
    filebundle_closefile();
    filebundleinfo.fptr = genfs_general_openfile(bundle_filename,"r");
    if (!filebundleinfo.fptr)
    {
        return S_OPENFILE;
    }
    status = filebundle_openfile_by_fptr(filebundleinfo.fptr, offset, child_filename, dovalidatefile);
    if (status != S_SUCCESS)
    {
        filebundle_closefile();
    }

    return status;
}

/**
 * filebundle_closefile
 *
 * @brief   Close bundle file
 *
 * @retval u8  status
 *
 * @author  Quyen Leba
 * 
 */
void filebundle_closefile()
{
    if (filebundleinfo.fptr)
    {
        genfs_closefile(filebundleinfo.fptr);
        filebundleinfo.fptr = NULL;
    }
}

/**
 * filebundle_validate
 *
 * @brief   Validate the bundle file
 *
 * @param   [in]    bundle_filename
 * @param   [in]    offset
 *
 * @retval u8  status
 *
 * @author  Quyen Leba
 *
 */
u8 filebundle_validate(const u8 *bundle_filename, u32 offset)
{
    u8 status;

    status = filebundle_openfile(bundle_filename,offset,"dummy.abc",TRUE);
    if (status == S_SUCCESS || status == S_FILENOTFOUND)
    {
        status = S_SUCCESS;
    }
    filebundle_closefile();

    return status;
}

/**
 * filebundle_seekfile
 *
 * @brief   Seek to position within child file
 *
 * @param   [in]    position
 *
 * @retval u8  status
 *
 * @author  Quyen Leba
 *
 */
u8 filebundle_seekfile(u32 position)
{
    if (!filebundleinfo.fptr)
    {
        return S_ERROR;
    }

    if (filebundleinfo.childfile_offset == 0)
    {
        //no child file opened
        return S_ERROR;
    }

    if (position >= filebundleinfo.filesize)
    {
        //out of range
        return S_INPUT;
    }

    if (fseek(filebundleinfo.fptr,filebundleinfo.childfile_offset+position,SEEK_SET) != 0)
    {
        return S_SEEKFILE;
    }

    return S_SUCCESS;
}

/**
 * filebundle_readfile
 *
 * @brief   Read from child file
 *
 * @param   [in]    length
 * @param   [out]   data
 * @param   [out]   actual_readcount
 *
 * @retval u8  status
 *
 * @author  Quyen Leba
 *
 */
u8 filebundle_readfile(u8 *data, u32 length, u32 *actual_readcount)
{
    file_blockcrypto decryption_info;
    u32 bytecount;
    u8  status;

    if (!filebundleinfo.fptr)
    {
        return S_ERROR;
    }

    if (filebundleinfo.childfile_offset == 0)
    {
        //no child file opened
        return S_ERROR;
    }

    if (filebundleinfo.fileposition + length > filebundleinfo.filesize)
    {
        length = filebundleinfo.filesize - filebundleinfo.fileposition;
    }

    decryption_info.bfcrypto.type = file_blockcrypto_type_bf;
    decryption_info.bfcrypto.ctx = (blowfish_context_ptr*)&m_external_ctx;
    decryption_info.bfcrypto.crypto = &crypto_blowfish_decryptblock_full;
    status = file_encrypted_read(filebundleinfo.fptr,length,data,&bytecount,
                                 &decryption_info);
    filebundleinfo.fileposition += length;

    if (actual_readcount)
    {
        *actual_readcount = bytecount;
    }
    return status;
}

/**
 * filebundle_eof
 *
 * @brief   Check if eof of child file
 *
 * @param   [out]   isOK NULL allowed
 *
 * @retval bool TRUE: eof
 *
 * @author  Quyen Leba
 *
 */
bool filebundle_eof(bool *isOK)
{
    if (!filebundleinfo.fptr)
    {
        if (isOK)
        {
            *isOK = FALSE;
        }
        return TRUE;
    }

    if (filebundleinfo.childfile_offset == 0)
    {
        //no child file opened
        if (isOK)
        {
            *isOK = FALSE;
        }
        return TRUE;
    }

    if (isOK)
    {
        *isOK = TRUE;
    }
    if (filebundleinfo.fileposition >= filebundleinfo.filesize)
    {
        return TRUE;
    }
    return FALSE;
}

/**
 * filebundle_filesize
 *
 * @brief   Get file size
 *
 * @param   [out]   isOK NULL allowed
 *
 * @retval u32
 *
 * @author  Quyen Leba
 *
 */
u32 filebundle_filesize(bool *isOK)
{
    if (!filebundleinfo.fptr)
    {
        if (isOK)
        {
            *isOK = FALSE;
        }
        return 0;
    }

    if (filebundleinfo.childfile_offset == 0)
    {
        //no child file opened
        if (isOK)
        {
            *isOK = FALSE;
        }
        return 0;
    }

    if (isOK)
    {
        *isOK = TRUE;
    }

    return filebundleinfo.filesize;
}

//-----------------------------------------------------------------------------
// All Crypto file has at least 64-byte header of which first 24 byte is generic
// general_header: 1st 24 bytes
// header_info: next 40 bytes
// header_extended: the rest of header (size varies)
//-----------------------------------------------------------------------------
/**
 * filebundle_getfullheader
 *
 * @brief   Get filebundle header
 *
 * @param   [out]   general_header
 * @param   [out]   header_info
 * @param   [out]   header_extended
 * @param   [out]   header_extended_length
 *
 * @retval u8  status
 *
 * @author  Quyen Leba
 * 
 */
u8 filebundle_getfullheader(FileCryptoHeaderInfo *general_header,
                            u8 *header_info, u8 *header_extended,
                            u32 *header_extended_length)
{
    u8  buffer[FILECRYPTO_COMMON_HEADER_SIZE];
    u8  extended_buffer[FILECRYPTO_EXTENDED_HEADER_MAX_SIZE];
    u32 count;
    u32 extended_buffer_length;
    u32 crc32_e;

    extended_buffer_length = 0;

    if (!filebundleinfo.fptr)
    {
        return S_ERROR;
    }

    count = fread(buffer,1,FILECRYPTO_COMMON_HEADER_SIZE,filebundleinfo.fptr);
    if (count != FILECRYPTO_COMMON_HEADER_SIZE)
    {
        filebundle_closefile();
        return S_FAIL;
    }

    crypto_blowfish_decryptblock_external_key(buffer,FILECRYPTO_COMMON_HEADER_SIZE);
    crc32e_reset();
    crc32_e = crc32e_calculateblock(0xFFFFFFFF,
                                    (u32*)&buffer[4],(FILECRYPTO_COMMON_HEADER_SIZE-4)/4);

    memcpy((char*)general_header,(char*)buffer,FILECRYPTO_GENERIC_HEADER_SIZE);

    if (general_header->hsize > (FILECRYPTO_COMMON_HEADER_SIZE + FILECRYPTO_EXTENDED_HEADER_MAX_SIZE))
    {
        filebundle_closefile();
        return S_BADCONTENT;
    }
    else if (general_header->hsize == FILECRYPTO_COMMON_HEADER_SIZE)
    {
        if (crc32_e != general_header->hcrc32e)
        {
            filebundle_closefile();
            return S_CRC32E;
        }
    }
    else if (general_header->hsize > FILECRYPTO_COMMON_HEADER_SIZE)
    {
        extended_buffer_length = general_header->hsize -
                                 FILECRYPTO_COMMON_HEADER_SIZE;
        count = fread(extended_buffer,1,extended_buffer_length,filebundleinfo.fptr);
        if (count != extended_buffer_length)
        {
            filebundle_closefile();
            return S_READFILE;
        }

        crypto_blowfish_decryptblock_external_key(extended_buffer,count);
        crc32_e = crc32e_calculateblock(crc32_e,
                                    (u32*)extended_buffer,count/4);

        if (crc32_e != general_header->hcrc32e)
        {
            filebundle_closefile();
            return S_CRC32E;
        }
    }
    else
    {
    }

    memcpy(header_info,&buffer[FILECRYPTO_GENERIC_HEADER_SIZE],
           FILECRYPTO_COMMON_HEADER_SIZE - FILECRYPTO_GENERIC_HEADER_SIZE);
    memcpy(header_extended,extended_buffer,extended_buffer_length);
    *header_extended_length = extended_buffer_length;

    return S_SUCCESS;
}

//-----------------------------------------------------------------------------
// Validate file content using crc32e. Start_position should be start of file
// content
// Inputs:  FileCryptoGeneralHeader *general_header
//          int start_position
// Outputs: unsigned int *crc32_e_output
//          unsigned int *errorcode
// Return:  STATUS_CODE
// Engineer: Quyen Leba
//-----------------------------------------------------------------------------
u8 filebundle_checkfilecrc32e(FileCryptoHeaderInfo *general_header,
                              u32 start_position, u32 *crc32_e_output)
{
    u32 count;
    u32 testcount;
    u8  buffer[1024];   //~~~ big buffer hurt stack
    u32 crc32_e;

    if (!filebundleinfo.fptr)
    {
        return S_ERROR;
    }

    if (fseek(filebundleinfo.fptr,filebundleinfo.bundlefile_offset + start_position,SEEK_SET) != 0)
    {
        filebundle_closefile();
        return S_SEEKFILE;
    }

    crc32_e = 0xFFFFFFFF;
    crc32e_reset();
    testcount = 0;

    while(1)
    {
        count = fread(buffer,1,sizeof(buffer),filebundleinfo.fptr);
        testcount += count;
        if ((count % 8) != 0)
        {
            filebundle_closefile();
            return S_BADCONTENT;
        }

        crc32_e = crc32e_calculateblock(crc32_e,(u32*)buffer,count/4);
        if (count < sizeof(buffer))
        {
            break;
        }
    }
    *crc32_e_output = crc32_e;

    if (crc32_e != general_header->fcrc32e)
    {
        return S_CRC32E;
    }
    return S_SUCCESS;
}

/**
 * filebundle_getfilerawdata
 *
 * @brief   Get data from bundle file
 *
 * @param   [in]    start_position if -1, don't seek
 * @param   [in]    length
 * @param   [out]   data_output
 * @param   [out]   data_length
 *
 * @retval u8  status
 *
 * @author  Quyen Leba
 *
 */
u8 filebundle_getfilerawdata(s32 start_position, u32 length,
                             u8 *data_output, u32 *data_length)
{
    if (!filebundleinfo.fptr)
    {
        return S_ERROR;
    }

    if (start_position >= 0)
    {
        if (fseek(filebundleinfo.fptr,start_position,SEEK_SET) != 0)
        {
            return S_SEEKFILE;
        }
    }

    if (length <= 0)
    {
        if (data_length)
        {
            *data_length = 0;
        }
        return S_FAIL;
    }

    *data_length = fread(data_output,1,length,filebundleinfo.fptr);
    return S_SUCCESS;
}

/**
 * filebundle_extractfile
 *
 * @brief   Extract current opened file
 *
 * @param   [in]    out_filename Filename to extract file to
 *
 * @retval u8  status
 *
 * @author  Quyen Leba
 *
 */
u8 filebundle_extractcurrentfile(const u8 *out_filename)
{
    F_FILE *fptr;
    u8  *bptr;
    u32 actualcount;
    u32 count;
    u8  status;

    fptr = NULL;
    bptr = NULL;
    if (!filebundleinfo.fptr)
    {
        return S_ERROR;
    }

    status = filebundle_seekfile(0);
    if (status != S_SUCCESS)
    {
        return status;
    }

    fptr = genfs_general_openfile(out_filename,"w");
    if (!fptr)
    {
        return S_OPENFILE;
    }

    bptr = __malloc(2048);
    if (!bptr)
    {
        status = S_MALLOC;
        goto filebundle_extractfile_done;
    }

    while(!filebundle_eof(NULL))
    {
        status = filebundle_readfile(bptr,2048,&actualcount);
        if (status != S_SUCCESS)
        {
            goto filebundle_extractfile_done;
        }
        count = fwrite((char*)bptr,1,actualcount,fptr);
        if (count != actualcount)
        {
            status = S_WRITEFILE;
            goto filebundle_extractfile_done;
        }
    }

filebundle_extractfile_done:
    if (bptr)
    {
        __free(bptr);
    }
    if (fptr)
    {
        genfs_closefile(fptr);
    }
    return status;
}

/**
 * filebundle_extractfile_by_fptr
 *
 * @brief   Extract current opened file. Assume bundle file is already validated
 *
 * @param   [in]    bundle_filename
 * @param   [in]    bundle_fileoffset   Start position of bundle file
 * @param   [in]    child_filename
 * @param   [in]    out_filename
 *
 * @retval u8  status
 *
 * @author  Quyen Leba
 *
 */
u8 filebundle_extractfile_by_fptr(F_FILE *fptr, u32 bundle_fileoffset, const u8 *child_filename, const u8 *out_filename)
{
    u8  status;

    status = filebundle_openfile_by_fptr(fptr,bundle_fileoffset,child_filename,FALSE);
    if (status != S_SUCCESS)
    {
        goto filebundle_extractfile_done;
    }

    status = filebundle_extractcurrentfile(out_filename);
    if (status != S_SUCCESS)
    {
        goto filebundle_extractfile_done;
    }

filebundle_extractfile_done:
    if (status != S_SUCCESS && status != S_FILENOTFOUND)
    {
        filebundle_closefile();
    }
    return status;
}

/**
 * filebundle_extractfile
 *
 * @brief   Extract current opened file. Assume bundle file is already validated
 *
 * @param   [in]    bundle_filename
 * @param   [in]    bundle_fileoffset   Start position of bundle file
 * @param   [in]    child_filename
 * @param   [in]    out_filename
 *
 * @retval u8  status
 *
 * @author  Quyen Leba
 *
 */
u8 filebundle_extractfile(const u8 *bundle_filename, u32 bundle_fileoffset, const u8 *child_filename, const u8 *out_filename)
{
    u8  status;

    status = filebundle_openfile(bundle_filename,bundle_fileoffset,child_filename,FALSE);
    if (status != S_SUCCESS)
    {
        goto filebundle_extractfile_done;
    }

    status = filebundle_extractcurrentfile(out_filename);
    if (status != S_SUCCESS)
    {
        goto filebundle_extractfile_done;
    }

filebundle_extractfile_done:
    filebundle_closefile();
    return status;
}
