/**
  **************** (C) COPYRIGHT 2011 SCT Performance, LLC *********************
  * File Name          : obd2vpw.c
  * Device / System    : SCT products using the "CommonCode" platform
  * Author             : Quyen Leba
  *
  * Version            : 1 
  * Date               : 05/02/2011
  * Description        : 
  *                    : 
  *
  *
  * History            : 05/02/2011 M. Davis
  *                    :   Add SCT Copyright header,
  *
  ******************************************************************************
  */

//------------------------------------------------------------------------------
// Log error point: 0x32xx -> 0x32xx
//------------------------------------------------------------------------------

#include <stdio.h>
#include <string.h>
#include <board/genplatform.h>
#include <board/interrupt.h>
#include <board/delays.h>
#include <board/rtc.h>
#include <fs/genfs.h>
#include <common/statuscode.h>
#include <common/checksum.h>
#include <common/obd2datalog.h>
#include <common/crypto_blowfish.h>
#include <common/itoa.h>
#include <common/ubf.h>
#include "obd2vpw.h"
#include "obd2.h"

extern obd2_info gObd2info;             /* from obd2.c */

//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
extern u8 obd2_rxbuffer[256];                       //from obd2.c
extern Datalog_Mode gDatalogmode;                   //from obd2datalog.c
extern Datalog_Info *dataloginfo;                   //from obd2datalog.c
bool obd2vpw_bypass_interrupt = FALSE;  // Note: only applicable in low speed
const u8 vpwheader[] =
{
    [FUNCTIONAL_P2] = 0x88,
    [NODE_P2] = 0x8C,
    [FUNCTIONAL_P3] = 0x68,
    [NODE_P3] = 0x6C,
};

//------------------------------------------------------------------------------
// Private prototypes
//------------------------------------------------------------------------------
void obd2vpw_tx_break();

//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
u8 obd2vpw_init(bool boosted, bool highspeed)
{
    return vpw_init(boosted, highspeed);
}

//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
void obd2vpw_tx_break()
{
    vpw_send_break();
}

//------------------------------------------------------------------------------
// Set obd2vpw in datalog mode to bypass interrupt (speed mode)
// Note: only applicable in low speed
//------------------------------------------------------------------------------
void obd2vpw_datalogmode()
{
    obd2vpw_bypass_interrupt = TRUE;
}

//------------------------------------------------------------------------------
// Set obd2vpw in normal mode (strict mode)
// Note: only applicable in low speed
//------------------------------------------------------------------------------
void obd2vpw_normalmode()
{
    obd2vpw_bypass_interrupt = FALSE;
}

//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
void obd2vpw_rxinfo_init(obd2vpw_rxinfo *rxinfo)
{    
    rxinfo->version = OBD2VPW_RXINFO_VERSION;
    rxinfo->max_retry_count = 10000;  // 10 Seconds (default)
    rxinfo->response_expected = TRUE;
    rxinfo->rxbuffer = obd2_rxbuffer;
    rxinfo->rxlength = 0;
    rxinfo->cmd = 0xFF;
    rxinfo->response_cmd_offset = 0x40;
    rxinfo->first_frame_data_offset = 0;
    rxinfo->errorcode = 0;
    rxinfo->timeout = 1000;
    rxinfo->transfermode = VPW_MODE_NODE;
    rxinfo->functionaladdress = VPW_ECM_FUNCTIONAL_RESPONSE_ID;
}

//------------------------------------------------------------------------------
// Inputs:  u8  *databuffer
//          u16 datalength
//          u8  speedmode
// Return:  u8  status
// Engineer: Quyen Leba
// Note: when use in high speed mode, make sure txbuffer is defined with at
// least one extra byte (for frame checksum)
//------------------------------------------------------------------------------
u8 obd2vpw_tx(u8 *databuffer, u16 datalength)
{
    return vpw_tx(databuffer,datalength);
}

//------------------------------------------------------------------------------
// Inputs:  obd2vpw_rxinfo *rxinfo
// Output:  obd2vpw_rxinfo *rxinfo
// Return:  u8  status
// Engineer: Quyen Leba
// Note: if run in high-speed mode, rxinfo->rxbuffer is also used as tmp buffer
//------------------------------------------------------------------------------
u8 obd2vpw_rx(obd2vpw_rxinfo *rxinfo)
{
    u8  responsed_command;
    u8  status;
    u32 timeout;
    u32 rtcvalue;
    
    rxinfo->rxlength = 0;
    responsed_command = rxinfo->cmd + rxinfo->response_cmd_offset;
    timeout = rxinfo->max_retry_count;
    
    //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    // We expect 2 types of responses:
    // [1:6C/6D] [1:response_id] [1:ecm_id] [1:responsed_command] [n: data]
    // [1:48] [1:6B] [1:ecm_id] [1:responsed_command] [n: data]
    // responsed_command = command + 0x40
    // Negative response:
    // [1:6C] [1:response_id] [1:ecm_id] [1:7F] [1:command] [1:error]
    // 6C 10 F0 19 DA FF 00
    // 6C F0 10 59 00 00 FF
    //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    // Other frames sent by ECMs:
    // 6C FE 10 3F (high speed, 'seems' indicating busy handling your cmd)
    // 6C F0 10 7F 34 00 04 00 FF 91 33
    //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

    rtcvalue = rtc_getvalue();
    
    do
    {
        status = vpw_rx(rxinfo->rxbuffer, &rxinfo->rxlength, rxinfo->timeout, rxinfo->transfermode, 
                        rxinfo->functionaladdress);
        if (status == S_SUCCESS &&
            (rxinfo->rxbuffer[0] == 0x6C || rxinfo->rxbuffer[0] == 0x6D || rxinfo->rxbuffer[0] == 0x48) &&
            (rxinfo->rxbuffer[3] == responsed_command || rxinfo->rxbuffer[3] == 0x7F) &&
            (rxinfo->rxbuffer[1] == VPW_ECM_RESPONSE_ID || rxinfo->rxbuffer[1] == VPW_ECM_FUNCTIONAL_RESPONSE_ID) &&
            (rxinfo->rxlength >= 4))
        {
            rxinfo->rxlength -= 5;
            memcpy((char*)rxinfo->rxbuffer,(char*)&rxinfo->rxbuffer[4],
                   rxinfo->rxlength);        
            
            if (rxinfo->rxbuffer[3] == 0x7F)
            {
                status = S_ERROR;
            }
            break;
        }
        else if (rxinfo->rxlength < 4)
        {
            // Data was not received
            status = S_TIMEOUT;
        }
        if ((rtc_getvalue() - rtcvalue) >= timeout)
        {
            status = S_TIMEOUT;
            break;
        }
    } while (status == S_TIMEOUT);
    
    return status;
}

//------------------------------------------------------------------------------
// Inputs:  u8  *databuffer
//          u16 datalength
//          u8  speedmode
// Return:  u8  status
// Engineer: Quyen Leba
//------------------------------------------------------------------------------
u8 obd2vpw_txrx_simple(u8 *txbuffer, u16 txlength, u8 *rxbuffer, u16 *rxlength)
{
    u8  status;
    status  = vpw_txrx_simple(txbuffer,txlength,rxbuffer,rxlength);
    return status;
}

//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
u8 obd2vpw_ping()
{
    // Mode 1 request with functional address
    u8  pidframe[5] = {0x68,VPW_ECM_FUNCTIONAL_ID,VPW_ECM_RESPONSE_ID_SECONDARY,
                        0x01, 0x00};
    u8  status;
    obd2vpw_rxinfo rxinfo;
    
    obd2vpw_rxinfo_init(&rxinfo);
    rxinfo.max_retry_count = 100;
    rxinfo.timeout = 100;
    rxinfo.cmd = 0x01;
    rxinfo.transfermode = VPW_MODE_FUNCTIONAL;
    
    status = obd2vpw_tx(pidframe,5);
    if (status != S_SUCCESS)
    {
        return status;
    }
    status = obd2vpw_rx(&rxinfo);
    if (status != S_SUCCESS)
    {
        return status;
    }
    
    return S_SUCCESS;
}

//------------------------------------------------------------------------------
// Read Data by ParameterId ($01)
// Inputs:  u8  pid
// Output:  u8  *output
// Return:  u8  status
// Engineer: Tristen Pierson
//------------------------------------------------------------------------------
u8 obd2vpw_read_data_bypid_mode1(u8 pid, u8 pidsize, u8 *output)
{
    u8  pidframe[5] = {0x68,VPW_ECM_FUNCTIONAL_ID,VPW_ECM_RESPONSE_ID_SECONDARY,0x01};
    u8  status;
    obd2vpw_rxinfo rxinfo;

    obd2vpw_rxinfo_init(&rxinfo);
    rxinfo.max_retry_count = 1000;
    rxinfo.transfermode = VPW_MODE_FUNCTIONAL;
    rxinfo.cmd = 0x01;
    pidframe[4] = pid;
   
    status = obd2vpw_tx(pidframe,5);
    if (status != S_SUCCESS)
    {
        return status;
    }

    status = obd2vpw_rx(&rxinfo);
    if (status != S_SUCCESS)
    {
        return status;
    }
    else if (rxinfo.rxbuffer[0] == pidframe[4])
    {
        memcpy((char*)output,(char*)&rxinfo.rxbuffer[1],pidsize);
        return S_SUCCESS;
    }
    return S_FAIL;
}

//------------------------------------------------------------------------------
// InitiateDiagnosticOperation ($10)
// Return:  u8  status
// Engineer: Quyen Leba, Tristen Pierson
// Note: currently only use this function in low speed mode
//------------------------------------------------------------------------------
u8 obd2vpw_enter_diag_mode()
{
    const u8 diagnostic[5] = {0x6C,VPW_ECM_ID,VPW_ECM_RESPONSE_ID,0x10,0x01};
    u8  status;
    obd2vpw_rxinfo rxinfo;
    u8 pcm_response = 0;

    obd2vpw_rxinfo_init(&rxinfo);
    rxinfo.cmd = 0x28;

    obd2vpw_return_normal_mode(0);
    delays(100, 'm');
    
    status = obd2vpw_tx((u8*)diagnostic,5);
    if (status == S_SUCCESS)
    {
        rxinfo.max_retry_count = 250;
        while(1)
        {
            // Recieve all module responses, look for PCM response
            status = obd2vpw_rx(&rxinfo);
            if(status == S_SUCCESS)
            {
                pcm_response = TRUE;
            }
            else
            {
                if(pcm_response)
                {
                    status = S_SUCCESS;
                }
                else
                {
                    log_push_error_point(0x3221);
                }
                break;
            }
        }
    }
            
    return status;
}

//------------------------------------------------------------------------------
// ReturnNormalMode ($20)
// Inputs:  u8 broadcast
// Return:  u8 status
// Engineer: Quyen Leba
// Note: currently only use this function in low speed mode
//------------------------------------------------------------------------------
u8 obd2vpw_return_normal_mode(u8 broadcast)
{
    u8  returnnormalframe[4+1] = {0x6C,VPW_ECM_ID,VPW_ECM_RESPONSE_ID,0x20};
    u8  status;
    obd2vpw_rxinfo rxinfo;
    u32 retry;
    u8 pcm_response;

    obd2vpw_rxinfo_init(&rxinfo);
    rxinfo.cmd = 0x20;
    retry = 3;
    pcm_response = FALSE;
    
    rxinfo.max_retry_count = 3000; // Waiting for 3 second w/o tester present will also cause return to normal mode
    
    if(broadcast == 1)
    {
        returnnormalframe[1] = VPW_ECM_BROADCAST_ID;
    }
    
    while(retry) // TX msg
    {
        status = obd2vpw_tx(returnnormalframe,4);
        if(status == S_SUCCESS)
        {
            break;
        }
        else
        {   
            retry--;            
        }
    }
    
    if(status == S_SUCCESS) // RX msgs
    {
        do
        {
            // Recieve all module responses, look for PCM response
            status = obd2vpw_rx(&rxinfo);
            if(status == S_SUCCESS)
            {
                pcm_response = TRUE;
            }
            else
            {
                if(pcm_response)
                {
                    status = S_SUCCESS;
                }
                else
                {
                    log_push_error_point(0x320C);
                }
                break;
            }
        } while(broadcast);
    }
    
    return status;
}

//------------------------------------------------------------------------------
// Read Data by ParameterId ($22)
// Inputs:  u16 pid
//          u8  pidsize (size in bytes of pid; i.e. 1,2,4)
// Output:  u8  *output
// Return:  u8  status
// Engineer: Quyen Leba
//------------------------------------------------------------------------------
u8 obd2vpw_read_data_bypid(u16 pid, u8 pidsize, u8 *output)
{
    u8  pidframe[7] = {0x6C,VPW_ECM_ID,VPW_ECM_RESPONSE_ID,0x22};
    u8  status;
    obd2vpw_rxinfo rxinfo;

    obd2vpw_rxinfo_init(&rxinfo);
    rxinfo.max_retry_count = 1000;
    rxinfo.cmd = 0x22;
    pidframe[4] = (u8)(pid >> 8);
    pidframe[5] = (u8)(pid & 0xFF);
    pidframe[6] = 0x01;

    //6C 10 F0 22 11 9D 01      (pid: 0x119D)
    //6C F0 10 62 11 9D BC      (data: 0xBC)
    //6C 10 F0 22 FC 1E 01      (pid: 0xFC1E)
    //6C 10 F0 62 FC 1E 00 00   (data: 0x0000)
    
    status = obd2vpw_tx(pidframe,7);
    if (status != S_SUCCESS)
    {
        return status;
    }

    status = obd2vpw_rx(&rxinfo);
    if (status != S_SUCCESS)
    {
        return status;
    }
    else if (rxinfo.rxbuffer[0] == pidframe[4] &&
             rxinfo.rxbuffer[1] == pidframe[5])
    {
        memcpy((char*)output,(char*)&rxinfo.rxbuffer[2],pidsize);
        return S_SUCCESS;
    }
    return S_FAIL;
}

//------------------------------------------------------------------------------
// ReadMemoryByAddress ($23) - read only 4 bytes
// Inputs:  u32 address (memory address)
// Output:  u8  *data
// Return:  u8  status
// Engineer: Quyen Leba
// Note: so far (Feb082010), all VPW ECMs we flash use 3-byte address
//------------------------------------------------------------------------------
u8 obd2vpw_read_memory_byaddress(u32 address, u8 *data)
{
    u8  memframe[8] = {0x6C,VPW_ECM_ID,VPW_ECM_RESPONSE_ID,0x23};
    u8  status;
    obd2vpw_rxinfo rxinfo;

    obd2vpw_rxinfo_init(&rxinfo);
    rxinfo.cmd = 0x23;
    memframe[4] = (u8)(address >> 16);
    memframe[5] = (u8)(address >> 8);
    memframe[6] = (u8)(address & 0xFF);
    memframe[7] = 0x01;

    //6C 10 F0 23 FF A2 F8 01 (address: 0xFFA2F8)
    //6C F0 10 63 A2 F8 1D 36 00 00 (data: 0x1D360000)
    
    status = obd2vpw_tx(memframe,8);
    if (status != S_SUCCESS)
    {
        return status;
    }

    status = obd2vpw_rx(&rxinfo);
    if (status != S_SUCCESS)
    {
        return status;
    }
    else if (rxinfo.rxbuffer[0] == memframe[5] &&
             rxinfo.rxbuffer[1] == memframe[6])
    {
        memcpy((char*)data,(char*)&rxinfo.rxbuffer[2],4);
        return S_SUCCESS;
    }
    return S_FAIL;
}

//------------------------------------------------------------------------------
// ReadMemoryByAddress ($23)
// Inputs:  u32 address (memory address)
//          u16 length (to request upload)
// Output:  u8  *data
//          u16 *datalength
// Return:  u8  status
// Engineer: Quyen Leba
//TODOQ: 
//------------------------------------------------------------------------------
u8 obd2vpw_read_memory_address(u32 address, u16 length,
                               u8 *data, u16 *datalength)
{
    u8  status;
    u8  databuffer[4];
    u32 bytecount;
    u32 currentaddress;
    u32 tmpcount;

    bytecount = 0;
    currentaddress = address;
    while(bytecount < (u32)length)
    {
        status = obd2vpw_read_memory_byaddress(currentaddress, databuffer);
        if (status == S_SUCCESS)
        {
            tmpcount = length - bytecount;
            if (tmpcount > 4)
            {
                tmpcount = 4;
            }
            memcpy((char*)&data[bytecount],(char*)databuffer,tmpcount);
            
            currentaddress += 4;
            bytecount += 4;
        }
        else
        {
            break;
        }
    }
    
    if (status == S_SUCCESS)
    {
        *datalength = bytecount;
    }
    
    return status;
}

//------------------------------------------------------------------------------
// SecurityAccess ($27)
// Input:   Obd2vpw_SubServiceNumber subservice
// Input:   u8  *data (if send key, 2 bytes)
// Output:  u8  *data (if request seed, 2 bytes)
// Return:  u8  status
// Engineer: Quyen Leba
//------------------------------------------------------------------------------
u8 obd2vpw_security_access(SubServiceNumber subservice, u8 *data)
{
    u8  securityaccessframe[7] = {0x6C,VPW_ECM_ID,VPW_ECM_RESPONSE_ID,0x27,0};
    u8  status;
    u8  framelength;
    obd2vpw_rxinfo rxinfo;
    int wait_timeout = 10;

obd2vpw_security_access_tx:    
    obd2vpw_rxinfo_init(&rxinfo);
    rxinfo.cmd = 0x27;
    securityaccessframe[4] = (u8)subservice;

    framelength = 5;
    if (subservice == SPSsendKey || subservice == DevCtrlsendKey)
    {
        framelength = 7;
        memcpy((char*)&securityaccessframe[5],(char*)data,2);
    }
    

    status = obd2vpw_tx(securityaccessframe,framelength);
    if (status != S_SUCCESS)
    {
        return status;
    }

//obd2vpw_security_access_rx:
    status = obd2vpw_rx(&rxinfo);
    if (status != S_SUCCESS)
    {
        return status;
    }
    
    if(subservice == SPSsendKey || subservice == DevCtrlsendKey)
    {
        //successful frame: 6C F1 10 67 02 34
        if(rxinfo.rxbuffer[0] != 0x02 && rxinfo.rxbuffer[1] != 0x34)// Security Access Allowed(SAEJ2190)
        {
            return S_FAIL;            
        }
    }
    else if(subservice == SPSrequestSeed || subservice == DevCtrlrequestSeed)
    {       
        if(rxinfo.rxbuffer[1] == 0x37) // Required Time Delay Not Expired(SAEJ2190)
        {
            obd2vpw_testerpresent();
            delays(1500,'m');
            if(!wait_timeout--)
                return S_FAIL;
            goto obd2vpw_security_access_tx;            
        }
        memcpy(data,&rxinfo.rxbuffer[1],2);
    }
    else
    {
        return S_FAIL;
    }
        
    return S_SUCCESS;
}

//------------------------------------------------------------------------------
// DisableNormalCommunication ($28)
// Return:  u8  status
// Engineer: Quyen Leba
//------------------------------------------------------------------------------
u8 obd2vpw_disablenormalcommunication()
{
    const u8 disablecommframe[5] = {0x6C,VPW_ECM_BROADCAST_ID,VPW_ECM_RESPONSE_ID,0x28,0x00};
    u8  status;
    obd2vpw_rxinfo rxinfo;
    u8 pcm_response = 0;

    obd2vpw_rxinfo_init(&rxinfo);
    rxinfo.cmd = 0x28;

    status = obd2vpw_tx((u8*)disablecommframe,5);
    
    if (status == S_SUCCESS)
    {    
        rxinfo.max_retry_count = 250;
        while(1)
        {
            // Recieve all module responses, look for PCM response
            status = obd2vpw_rx(&rxinfo);
            if(status == S_SUCCESS)
            {
                    pcm_response = TRUE;
            }
            else
            {
                if(pcm_response)
                {
                    status = S_SUCCESS;
                }
                else
                {
                    log_push_error_point(0x320F);
                }
                break;
            }
        }
    }
        
    return status;
}

//------------------------------------------------------------------------------
// DisableNormalCommunication ($2A)
// Return:  u8  status
// Engineer: Quyen Leba
//------------------------------------------------------------------------------
u8 obd2vpw_stop_periodic_message()
{
//    const u8 stopmsg[5] = {0x6C,VPW_ECM_ID,VPW_ECM_RESPONSE_ID,0x2A,0x00};
//    const u8 testing[7] = {0x6C,VPW_ECM_ID,VPW_ECM_RESPONSE_ID,0x22,0x11,0x41,0x01};
    
    const u8 stopmsg[5] = {0x6C,VPW_ECM_BROADCAST_ID,VPW_ECM_RESPONSE_ID,0x2A,0x00};
    const u8 testing[7] = {0x6C,VPW_ECM_ID,VPW_ECM_RESPONSE_ID,0x22,0x11,0x41,0x01};
    
    u8  status;
    obd2vpw_rxinfo rxinfo;

    obd2vpw_rxinfo_init(&rxinfo);
    rxinfo.cmd = 0x22;

    status = obd2vpw_tx((u8*)testing,7);
    if (status != S_SUCCESS)
    {
        return status;
    }
    status = obd2vpw_rx(&rxinfo);
    if (status != S_SUCCESS)
    {
        return status;
    }
    
    //----
    
    obd2vpw_rxinfo_init(&rxinfo);
    rxinfo.cmd = 0x2A;

    status = obd2vpw_tx((u8*)stopmsg,5);
    if (status != S_SUCCESS)
    {
        return status;
    }
    status = obd2vpw_rx(&rxinfo);
    if (status != S_SUCCESS)
    {
        return status;
    }

    return S_SUCCESS;
}

//------------------------------------------------------------------------------
// Request Diagnostic Data Packet ($2A)
// Inputs:  u8  requesttype (see GMLAN_SubServiceNumber for $2A)
//          u8  *packetidlist
//          u8  packetidcount
// Return:  u8  status
// Engineer: Quyen Leba
//------------------------------------------------------------------------------
u8 obd2vpw_request_diagnostic_data_packet(u8 requesttype,
                                          u8 *packetidlist, u8 packetidcount)
{
    u8 requestpacket[24] = {0x6C,VPW_ECM_ID,VPW_ECM_RESPONSE_ID,0x2A,0x00,0x00,0x00,0x00};
    u8  length;
    u8  status;
    u8  i;
    obd2vpw_rxinfo rxinfo;

    if (packetidcount > 4) 
    {
        return S_INPUT;
    }
    
    obd2vpw_rxinfo_init(&rxinfo);
    rxinfo.cmd = 0x2A;

    length = 5;
    requestpacket[4] = requesttype;
    if (requesttype != stopSending_$2A)
    {
        for(i=0;i<packetidcount;i++)
        {
            requestpacket[5+i] = packetidlist[i];
        }
        length = 9;
    }
    
    //example: schedule a send data
    //[6C 10 F0] [2A [14] [FE FD FC] 00
    //[6C F0 10] [7F 2A] [14] [FE FD FC] [00 23]
    //[6C F0 10] [6A] [FE] [0D EF 00 00 00 00]  //data is sent repeatedly
    //[6C F0 10] [6A] [FD] [00 01 01 4A 7F 01]
    //[6C F0 10] [6A] [FC] [01 0A 00 00 00 00]
    //...
    
    //example: stop a schedule
    //[6C 10 F0] [2A] 00
    //[6C F0 10] [6A] 00
    
    status = obd2vpw_tx((u8*)requestpacket,length);
    if (status != S_SUCCESS)
    {
        return status;
    }
    status = obd2vpw_rx(&rxinfo);
    
    if (requesttype != stopSending_$2A && status == S_ERROR)
    {
        if (rxinfo.rxbuffer[0] == requesttype && rxinfo.rxbuffer[5] == 0x23)
        {
            status = S_SUCCESS;
        }
    }
    else if (requesttype == stopSending_$2A)
    {
        //TODOQ: move this out to improve speed, make a stop datalog or something
        
        //if there's a schedule, there's also a response when all stops:
        //[6C F0 10] [60]
        
        //TODOQ: more on this later -> really need to move this out
//        rxinfo.cmd = 0x20;
//        obd2vpw_rx(&rxinfo);
    }
    
    return status;
}

//------------------------------------------------------------------------------
// Dynamically Define Message ($2C)
// Inputs:  u8  packetid
//          u32 address (PID or DMR address)
//          PidType type
//          u8  position (data location in packet)
//          u8  size (data size: 1,2,4)
// Return:  u8  status
// Engineer: Quyen Leba
//------------------------------------------------------------------------------
u8 obd2vpw_dynamicallydefinemessage(u8 packetid, u32 address,
                                    PidType type, u8 size, u8 position)
{
    obd2vpw_rxinfo rxinfo;
    u8  tpl;    //bit 7,6:      00 - defined by offset (1 byte)
                //              01 - defined by pid (2 byte)
                //              02 - defined by memory address (3 byte)
                //bit 5,4,3:    starting byte (position), start at 1
                //bit 2,1,0:    number of data bytes (size)
    u8  definemsg[10] = {0x6C,VPW_ECM_ID,VPW_ECM_RESPONSE_ID,0x2C};
    u8  status;
    
    definemsg[4] = packetid;
    
    //tpl = size | (type << 6) | (position << 3);
    if (size != 1 && size != 2 && size != 4)
    {
        return S_INPUT;
    }
    else if (position == 0 || position > 8)
    {
        return S_INPUT;
    }
    
    tpl = (position << 3) | size;
    if (type == PidTypeRegular)
    {
        tpl |= (u8)(01 << 6);
        definemsg[6] = (u8)(address >> 8);
        definemsg[7] = (u8)(address & 0xFF);
        definemsg[8] = 0xFF;
    }
    else if (type == PidTypeDMR)
    {
        tpl |= (u8)(02 << 6);
        definemsg[6] = (u8)(address >> 16);
        definemsg[7] = (u8)(address >> 8);
        definemsg[8] = (u8)(address & 0xFF);
    }
    else
    {
        return S_INPUT;
    }
    
    definemsg[5] = tpl;
    definemsg[9] = 0xFF;

    obd2vpw_rxinfo_init(&rxinfo);
    rxinfo.cmd = 0x2C;

    //positive response
    // 6C 10 F0 2C FE 4A 00 21 FF FF
    // 6C F0 10 6C FE 4A
    //negative response
    // 6C 10 F0 2C FE 49 1A 67 FF FF
    // 6C F0 10 7F 2C FE 49 1A 66 FF 31
    status = obd2vpw_txrx_simple(definemsg,10,rxinfo.rxbuffer,&rxinfo.rxlength);
    if (status == S_SUCCESS && rxinfo.rxlength >= 6)
    {
        if (rxinfo.rxbuffer[3] == 0x6C)
        {
            return S_SUCCESS;
        }
        else if (rxinfo.rxbuffer[3] == 0x7F)
        {
            return S_ERROR;
        }
        else
        {
            return S_FAIL;
        }
    }
    else
    {
        return S_FAIL;
    }
}

//------------------------------------------------------------------------------
// RequestDownload ($34)
// Inputs:  u32 address
// Return:  u8  status
// Engineer: Quyen Leba
// Note: so far (Feb022010), all VPW ECMs we flash use 3-byte address
// Note: this function can be combined with obd2vpw_request_upload(...)
// Note: this function always used in high-speed mode
//------------------------------------------------------------------------------
u8 obd2vpw_request_download(u32 address/*, u16 length*/)
{
    u8 reqdownload[10+1] = {0x6C,VPW_ECM_ID,VPW_ECM_RESPONSE_ID,0x34,0x00};
    u8 status;
    obd2vpw_rxinfo rxinfo;

    //example:
    //6C 10 F0 34 00 04 00 FF 91 00 (where: 0xFF9100 is 1Meg utility address)
    //6C F0 10 74 00 44

    reqdownload[5] = 0x04;  //(length >> 8) & 0xFF; //TODOQ: what is this?
    reqdownload[6] = 0x00;  //(length & 0xFF);
    reqdownload[7] = (address >> 16) & 0xFF;
    reqdownload[8] = (address >> 8) & 0xFF;
    reqdownload[9] = (address & 0xFF);

    obd2vpw_rxinfo_init(&rxinfo);
    rxinfo.cmd = 0x34;

    status = obd2vpw_txrx_simple(reqdownload,10,rxinfo.rxbuffer,&rxinfo.rxlength);
    if (status != S_SUCCESS)
    {
        log_push_error_point(0x3200);
        return status;
    }
    else if (status == S_SUCCESS &&
        rxinfo.rxbuffer[3] == 0x74 && rxinfo.rxbuffer[4] == 0 && rxinfo.rxbuffer[5] == 0x44)
    {
        return S_SUCCESS;
    }
    else
    {
        log_push_error_point(0x3202);
        return S_BADCONTENT;
    }
}

//------------------------------------------------------------------------------
// RequestUpload ($35)
// Inputs:  u32 address
// Return:  u8  status
// Engineer: Quyen Leba
// Note: so far (Feb022010), all VPW ECMs we flash use 3-byte address
// Note: this function can be combined with obd2vpw_request_download(...)
// Note: this function always used in high-speed mode
//------------------------------------------------------------------------------
u8 obd2vpw_request_upload(u32 address, u16 length)
{
    u8 requpload[10+1] = {0x6C,VPW_ECM_ID,VPW_ECM_RESPONSE_ID,0x35,0x01};
    u8  status;
    obd2vpw_rxinfo rxinfo;

    requpload[5] = (length >> 8) & 0xFF;
    requpload[6] = (length & 0xFF);
    requpload[7] = (address >> 16) & 0xFF;
    requpload[8] = (address >> 8) & 0xFF;
    requpload[9] = (address & 0xFF);
    
    obd2vpw_rxinfo_init(&rxinfo);
    rxinfo.cmd = 0x35;
    
    status = obd2vpw_tx((u8*)requpload,10);
    if (status != S_SUCCESS)
    {
        log_push_error_point(0x3203);
        return status;
    }
    status = obd2vpw_rx(&rxinfo);
    if (status != S_SUCCESS)
    {
        log_push_error_point(0x3204);
        return status;
    }
//    if (rxinfo.rxbuffer[0] != 0x01 || rxinfo.rxbuffer[1] != 0x54)
//    {
//        //TODOQ: is it always 01 54
//        log_push_error_point(0x3205);
//        return S_FAIL;
//    }

    return S_SUCCESS;
}

//------------------------------------------------------------------------------
// DataTransfer Upload ($36)
// Outputs: u8  *data (must be able to store MAX_UPLOAD_BUFFER_LENGTH bytes)
//          u32 *address
//          u16 *length
// Return:  u8  status
// Engineer: Quyen Leba
// Note: so far (Feb022010), all VPW ECMs we flash use 3-byte address
// Note: this function always used in high-speed mode
//------------------------------------------------------------------------------
u8 obd2vpw_transfer_data_upload(u8 *data, u32 *address, u16 *length)
{
    u8  status;
    u8  databuffer[MAX_UPLOAD_BUFFER_LENGTH+7+5];   //+5:extra,unused
    obd2vpw_rxinfo rxinfo;
    u16 calc_checksum;
    u16 data_checksum;
    
    obd2vpw_rxinfo_init(&rxinfo);
    rxinfo.rxbuffer = databuffer;
    rxinfo.cmd = 0x36;
    rxinfo.response_cmd_offset = 0;     //we expect response_cmd == 0x36
    
    status = obd2vpw_rx(&rxinfo);
    if (status != S_SUCCESS)
    {
        log_push_error_point(0x3206);
        return status;
    }

    //rxinfo.rxbuffer:
    //[01][2:length][3:address][length:data][2:datachecksum]
    *length = ((u16)(rxinfo.rxbuffer[1]) << 8) | (u16)(rxinfo.rxbuffer[2]);
    *address = ((u16)(rxinfo.rxbuffer[3]) << 16) | 
        ((u16)(rxinfo.rxbuffer[4]) << 8) | (u16)(rxinfo.rxbuffer[5]);
    if (*length > MAX_UPLOAD_BUFFER_LENGTH)
    {
        return S_BADCONTENT;    //TODOQ: log_push_error_point
    }
    
    //Note: block checksum includes the data and its 6 message data bytes
    //which is: [01][2:length][3:address]
//    data_checksum = ((u16)(rxinfo.rxbuffer[6+*length]) << 8) | 
//        (u16)(rxinfo.rxbuffer[6+1+*length]);
//    calc_checksum = (u16)checksum_simpleadditive(rxinfo.rxbuffer,
//                                                 (*length)+6);
//    if (data_checksum != calc_checksum)
//    {
//        log_push_error_point(0x3207);
//        return S_BADCONTENT;
//    }
    //TODOQ: new utility from David skip checksum for length & address
    data_checksum = ((u16)(rxinfo.rxbuffer[6+*length]) << 8) | 
        (u16)(rxinfo.rxbuffer[6+1+*length]);
    calc_checksum = (u16)checksum_simpleadditive(&rxinfo.rxbuffer[6],
                                                 *length);
    if (data_checksum != calc_checksum)
    {
        log_push_error_point(0x3207);
        return S_BADCONTENT;
    }
    
    memcpy((char*)data,(char*)&rxinfo.rxbuffer[6],*length);
    
    return S_SUCCESS;
}

//------------------------------------------------------------------------------
// DataTransfer Download ($36)
// Inputs:  u8  subservice (of $36)
//          u8  *data (must be able to store MAX_UPLOAD_BUFFER_LENGTH bytes)
//          u32 address
//          u16 length
// Return:  u8  status
// Engineer: Quyen Leba
// Note: so far (Feb022010), all VPW ECMs we flash use 3-byte address
// Note: this function always used in high-speed mode
//------------------------------------------------------------------------------
u8 obd2vpw_transfer_data_download(u8 subservice,
                                  u8 *data, u32 address, u16 length)
{
    u8  status;
    u8  databuffer[MAX_UPLOAD_BUFFER_LENGTH+7+5];   //+5:extra,unused
    obd2vpw_rxinfo rxinfo;
    u16 calc_checksum;
    
    obd2vpw_rxinfo_init(&rxinfo);
    rxinfo.rxbuffer = databuffer;
    rxinfo.cmd = 0x36;

    databuffer[0] = 0x6D;
    databuffer[1] = VPW_ECM_ID;
    databuffer[2] = VPW_ECM_RESPONSE_ID;
    databuffer[3] = 0x36;
    databuffer[4] = subservice;
    databuffer[5] = (u8)(length >> 8);
    databuffer[6] = (u8)(length & 0xFF);
    databuffer[7] = (u8)(address >> 16);
    databuffer[8] = (u8)(address >> 8);
    databuffer[9] = (u8)(address & 0xFF);
    memcpy((char*)&databuffer[10],(char*)data,length);

    calc_checksum = (u16)checksum_simpleadditive(&databuffer[4],length+6);
    databuffer[10+length] = (u8)((calc_checksum >> 8) & 0xFF);
    databuffer[10+1+length] = (u8)(calc_checksum & 0xFF);

    status = obd2vpw_tx((u8*)databuffer,10+length+2);
    if (status != S_SUCCESS)
    {
        log_push_error_point(0x3208);
        return status;
    }

    status = obd2vpw_rx(&rxinfo);
    if (status != S_SUCCESS)
    {
        log_push_error_point(0x3209);
        return status;
    }
    if (rxinfo.rxbuffer[0] != 0x00 || rxinfo.rxbuffer[1] != 0x73)
    {
        //TODOQ: is it always 00 73
        log_push_error_point(0x320A);
        return S_FAIL;
    }
    
    return S_SUCCESS;
}

//------------------------------------------------------------------------------
// ReadDataBlock ($3C)
// Inputs:  u8  blocknumber (block to read from)
// Outputs: u8  *blockdata
//          u8  *blockdatalength
// Return:  u8  status
// Engineer: Quyen Leba
//------------------------------------------------------------------------------
u8 obd2vpw_read_data_block(u8 blocknumber, u8 *blockdata, u8 *blockdatalength)
{
    u8  readframe[5] = {0x6C,VPW_ECM_ID,VPW_ECM_RESPONSE_ID,0x3C,blocknumber};
    u8  status;
    obd2vpw_rxinfo rxinfo;

    *blockdatalength = 0;

    obd2vpw_rxinfo_init(&rxinfo);
    rxinfo.cmd = 0x3C;
    status = obd2vpw_tx(readframe,5);
    if (status != S_SUCCESS)
    {
        return status;
    }
    status = obd2vpw_rx(&rxinfo);
    if (status != S_SUCCESS)
    {
        return status;
    }

    *blockdatalength = rxinfo.rxlength;
    memcpy((char*)blockdata,(char*)rxinfo.rxbuffer,rxinfo.rxlength);
    
    return S_SUCCESS;
}

//------------------------------------------------------------------------------
// DeviceControl ($AE)
// Inputs:  u8  cpid (control packet identifier)
//          u8  *controlbytes (output control data)
// Return:  u8  status
//          u8  *controlbytes (error code for S_REJECT)
// Engineer: Tristen Pierson
//------------------------------------------------------------------------------
u8 obd2vpw_gm_device_control(u8 cpid, u8 *controlbytes, u16 datalength)
{
    obd2vpw_rxinfo rxinfo;
    u8 devicecontrol[11] = {0x6C, VPW_ECM_ID, VPW_ECM_RESPONSE_ID, 0xAE};
    u8 status;    
    
    if (datalength > 6)
    {
        return S_ERROR;
    }
    
    devicecontrol[4] = cpid;    
    memcpy(&devicecontrol[5],controlbytes,datalength);
    
    obd2vpw_rxinfo_init(&rxinfo);
    rxinfo.cmd = 0xAE;    
    
    status = obd2vpw_txrx_simple(devicecontrol,6+datalength,rxinfo.rxbuffer,&rxinfo.rxlength);
    if (status == S_SUCCESS && rxinfo.rxlength >= 5)
    {
        if (rxinfo.rxbuffer[3] == 0xEE)
        {
            if (rxinfo.rxbuffer[4] != cpid)
            {
                status = S_FAIL;
            }            
        }
        else if (rxinfo.rxbuffer[3] == 0x7F)
        {
            switch (rxinfo.rxbuffer[rxinfo.rxlength-2])
            {
            case 0x23:      // Success
                status = S_SUCCESS;
                break;
            case 0x12:      // Invalid Format
                status = S_NOTFIT;
                break;
            case 0x31:      // Request out of range
                status = S_OUTOFRANGE;
                break;            
            case 0xE3:      // Device Control Limits Exceeded      
                if (rxinfo.rxlength >= 7)
                {
                    controlbytes[0] = rxinfo.rxbuffer[5];
                    controlbytes[1] = rxinfo.rxbuffer[6];
                }
                status = S_REJECT;
                break;
            default:
                status = S_ERROR;
                break;
            }
        }
        else
        {
            status = S_FAIL;
        }
    }
    else
    {
        status =  S_FAIL;
    }    
        
    return status;
}

//------------------------------------------------------------------------------
// RequestHighSpeed ($A0 & $A1)
// Return:  u8  status
// Engineer: Quyen Leba
//------------------------------------------------------------------------------
u8 obd2vpw_requesthighspeed()
{
    const u8 request_hs_frame[4] = {0x6C, VPW_ECM_BROADCAST_ID, VPW_ECM_RESPONSE_ID, 0xA0};
    const u8 start_hs_frame[4] = {0x6C, VPW_ECM_BROADCAST_ID, VPW_ECM_RESPONSE_ID, 0xA1};
    u8  status;
    obd2vpw_rxinfo rxinfo;
    bool pcm_hs_response;
    u8 retry;

    pcm_hs_response = FALSE;
    retry = 3;
    
    obd2vpw_rxinfo_init(&rxinfo);
    rxinfo.cmd = 0xA0;
       
    /* Request High Speed Mode */
    status = obd2vpw_tx((u8*)request_hs_frame,4);
    if (status == S_SUCCESS)
    {
        rxinfo.max_retry_count = 50;
        while(1)
        {
            // Recieve all module responses, look for PCM response
            status = obd2vpw_rx(&rxinfo);
            if(status == S_SUCCESS)
            {
                if (rxinfo.rxbuffer[0] == 0xAA || rxinfo.rxbuffer[0] == 0xBB)
                    pcm_hs_response = TRUE;
            }
            else
            {
                if(pcm_hs_response)
                {
                    status = S_SUCCESS;
                }
                else
                {
                    status = S_FAIL;
                    log_push_error_point(0x321D);
                }
                break;
            }
        }
    }
    else
    {
        log_push_error_point(0x320B); // Fail to request high speed
    }
    
    /* Start High Speed */
    if (status == S_SUCCESS)
    {
        rxinfo.max_retry_count = 750;
        while(retry)
        { 
            status = obd2vpw_tx((u8*)start_hs_frame,4);
            delays(5,'m');
            if(status == S_SUCCESS)
            {
                obd2vpw_init(TRUE, TRUE); // Init VPW to HIGHSPEED baud
                break;
            }
            retry--;
        }
        if (!retry)
        {
            delays(200,'m');
            status = S_FAIL;
            log_push_error_point(0x320D); // Fail to enter high speed
        }
        obd2vpw_testerpresent();
    }
    
    return status;
}

//------------------------------------------------------------------------------
// Read VIN ($09 $02)
// Output:  u8  *vin (must be able to store VIN_LENGTH+1 bytes)
// Return:  u8  status
// Engineer: Quyen Leba, Tristen Pierson
//------------------------------------------------------------------------------
u8 obd2vpw_readvin(u8 *vin)
{    
    obd2vpw_rxinfo rxinfo;
    u8  getvin[6] = {0x68, VPW_ECM_FUNCTIONAL_ID, VPW_ECM_RESPONSE_ID_SECONDARY, 0x09, 0x02};    
    u8  tempVin[20];    
    u8  temp[4];
    u8  messageCnt;
    u8  vinbuffer[24];
    u8  tmpbuffer[256];    
    u8  bytecount;
    u8  blocknum;
    u8  i;    
    u8  status;
    
    vin[0] = NULL;
    
    // Check for GMLAN commlevel        
    if (obd2vpw_read_data_bypid(0x000C, 2, temp) == S_SUCCESS)
    {
        // Get VIN via GMLAN
        for(blocknum=1,i=0;i<3;i++)
        {
            status = obd2vpw_read_data_block(blocknum++,tmpbuffer,&bytecount);
            if (status != S_SUCCESS)
            {
                break;
            }
            else if (bytecount != 7)
            {
                status = S_BADCONTENT;
                break;
            }        
            memcpy((char*)&vinbuffer[i*6],(char*)&tmpbuffer[1],6);
        }
        if (status == S_SUCCESS)
        {
            memcpy((char*)vin,(char*)&vinbuffer[1],17);
            vin[17] = NULL;        
            status = obd2_validatevin(vin);
        }
    }
    else    
    {
        // Get VIN via J1979
        obd2vpw_rxinfo_init(&rxinfo);
        rxinfo.max_retry_count = 1000;
        rxinfo.transfermode = VPW_MODE_FUNCTIONAL;
        rxinfo.cmd = 0x09;
         
        status = obd2vpw_tx(getvin,5);
        if (status != S_SUCCESS)
        {        
            return status;
        }
        // The next five frames contain VIN data
        for (messageCnt = 0; messageCnt < 5; messageCnt++)
        {        
            status = obd2vpw_rx(&rxinfo);
            
            if (rxinfo.rxlength == 6 && status == S_SUCCESS)
            {
                memcpy(&tempVin[messageCnt*4], &rxinfo.rxbuffer[2], 4);
            }
            else
            {
                if (status != S_TIMEOUT)
                {
                    status = S_FAIL;
                }
                break;
            }        
        }
        if (status == S_SUCCESS)
        {
            memcpy((char*)vin,&tempVin[3],17);
            vin[17] = NULL;
            status = obd2_validatevin(vin);
        }
        else if (status == S_TIMEOUT)        
        {
            // Make VIN blank (in same cases service $09 is unsupported)
            memset(vin,0xFF,17);
            vin[17] = NULL;
            status = obd2_validatevin(vin);
        }
        else
        {
            status = S_FAIL;
        }
    }
    return status;    
}

//------------------------------------------------------------------------------
// Read ecm serial number
// Output:  u8  *serialnumber
// Return:  u8  status
// Engineer: Quyen Leba
//------------------------------------------------------------------------------
u8 obd2vpw_readserialnumber(u8 *serialnumber)
{
    u8 status;
    u8 i;
    u8 blocknum;
    u8 serialbuffer[24];
    u8 tmpbuffer[256];
    u8 bytecount;

    memset(serialnumber,0,13);

    blocknum = 5;
    for(i=0;i<3;i++)
    {
        status = obd2vpw_read_data_block(blocknum++,tmpbuffer,&bytecount);
        if (status != S_SUCCESS)
        {
            goto obd2vpw_readserialnumber_done;
        }
        memcpy((char*)&serialbuffer[i*4],(char*)&tmpbuffer[1],4);
    }

    memcpy((char*)serialnumber,(char*)serialbuffer,12);
    return S_SUCCESS;

obd2vpw_readserialnumber_done:
    return status;
}

//------------------------------------------------------------------------------
// Read ecm hardware id
// Output:  u8  *hwid (in ASCII)
// Return:  u8  status
// Engineer: Quyen Leba
//------------------------------------------------------------------------------
u8 obd2vpw_readhardwareid(u8 *hwid)
{
    u8  status;
    u32 id;
    u8  tmpbuffer[256];
    u8  bytecount;

    memset(hwid,0,16);
    memset(tmpbuffer,0,256);
    status = obd2vpw_read_data_block(0x04,tmpbuffer,&bytecount);
    if (status == S_SUCCESS && bytecount >= 5)
    {
        id = (tmpbuffer[2] << 16) | (tmpbuffer[3] << 8) | tmpbuffer[4];
        itoa(id,tmpbuffer,10);
        if (strlen((char*)tmpbuffer) >= 8)
        {
            strcpy((char*)hwid,(char*)tmpbuffer);
        }
        else
        {
            strncpy((char*)hwid,"00000000",8-strlen((char*)tmpbuffer));
            strcat((char*)hwid,(char*)tmpbuffer);
        }
    }
    else
    {
        return S_FAIL;
    }
    return S_SUCCESS;
}

//------------------------------------------------------------------------------
// Read all available partnumbers
// Read and store in this order: OSNumber, CalId, Hardware, and upto 7 Segments
// Output:  partnumber_info *partnumbers
// Return:  u8  status
// Engineer: Quyen Leba
//------------------------------------------------------------------------------
u8 obd2vpw_readSoftwarePartNumber(partnumber_info *partnumbers)
{
    u8 status;
    u8 i;
    u8 tmpbuffer[256];
    u8 blocknums[MAX_PARTNUMBER_COUNT] = {0x0A,0x08,0x04,
                                          0x0B,0x0C,0x0D,0x0E,0x0F,0x10,0x11};
    u8 bytecount;

    if (partnumbers == NULL)
    {
        return S_INPUT;
    }

    partnumbers->count = 0;

    // get BCC
    memset((char*)partnumbers->bcc,0,sizeof(partnumbers->bcc));
    status = obd2vpw_read_data_block(0x14,tmpbuffer,&bytecount);
    if (status == S_SUCCESS && bytecount == 5)
    {
        memcpy((char*)partnumbers->bcc,(char*)&tmpbuffer[1],4);
    }

    for(i=0;i<MAX_PARTNUMBER_COUNT;i++)
    {
        partnumbers->number[i] = 0;
        partnumbers->id[i] = blocknums[i];
        status = obd2vpw_read_data_block(blocknums[i],tmpbuffer,&bytecount);
        if (status == S_SUCCESS && bytecount >= 5)
        {
            partnumbers->number[i] = (u32)tmpbuffer[1] << 24 |
                                     (u32)tmpbuffer[2] << 16 |
                                     (u32)tmpbuffer[3] << 8 |
                                     (u32)tmpbuffer[4];
            partnumbers->count++;
        }
        else// if (status == S_ERROR)
        {
            //do nothing
            log_push_error_point(0x320E);
        }
    }

    if (partnumbers->count == 0)
    {
        return S_FAIL;
    }

    return S_SUCCESS;
}

/**
 *  @brief Read ECM info from OBD2 info
 *  
 *  @param [in]  ecm_info   ECM info structure
 *
 *  @return status
 *  
 *  @details This function reads all available part numbers and serial number.
 */

/* TODO: Combine this with obd2can_gm_readecminfo function since they do the
 * same thing.
 */
u8 obd2vpw_readecminfo(ecm_info *ecm)
{
    ecm->ecm_count = gObd2info.ecu_count;
    memcpy(ecm->vin, gObd2info.vin, sizeof(ecm->vin));
    
    for(u8 i = 0; i < ECM_MAX_COUNT; i++)
    {
        if (gObd2info.ecu_block[i].ecutype != EcuType_Unknown)
        {
            ecm->commtype[i] = gObd2info.ecu_block[i].commtype;
            ecm->commlevel[i] = gObd2info.ecu_block[i].commlevel;
            ecm->codecount[i] = gObd2info.ecu_block[i].partnumber_count;
            
            for (u8 j = 0; j < ecm->codecount[i]; j++)
            {
                ecm->codeids[i][j] = gObd2info.ecu_block[i].partnumbers.gm.partnumber_id[j];
                memcpy(ecm->codes[i][j], 
                       gObd2info.ecu_block[i].partnumbers.gm.partnumber[j], sizeof(ecm->codes[i][j])); 
            }
            
            memcpy(ecm->vehicle_serial_number, 
                   gObd2info.ecu_block[i].partnumbers.gm.vehicle_serial_number, sizeof(ecm->vehicle_serial_number)); 
            memcpy(ecm->hwid, 
                   gObd2info.ecu_block[i].partnumbers.gm.hwid, sizeof(ecm->hwid)); 
            memcpy(ecm->bcc, 
                   gObd2info.ecu_block[i].partnumbers.gm.bcc, sizeof(ecm->bcc));                
        }
    }
    return S_SUCCESS;  
}

//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
u8 obd2vpw_cleardtc()
{
    u8  cleardtcframe[4] = {0x68,VPW_ECM_FUNCTIONAL_ID,VPW_ECM_RESPONSE_ID,0x04};
    u8  status;
    obd2vpw_rxinfo rxinfo;

    obd2vpw_rxinfo_init(&rxinfo);
    rxinfo.transfermode = VPW_MODE_FUNCTIONAL;
    rxinfo.cmd = 0x04;
    status = obd2vpw_tx(cleardtcframe,4);
    if (status != S_SUCCESS)
    {
        return status;
    }

    status = obd2vpw_rx(&rxinfo);
    if (status != S_SUCCESS)
    {
        return status;
    }

    return S_SUCCESS;
}

//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
u8 obd2vpw_readdtc(dtc_info *info)
{
    u8  readdtcframe[7] = {0x6C,VPW_ECM_ID,VPW_ECM_RESPONSE_ID,0x19,0xDA,0xFF,0x00};
    u8  status;
    obd2vpw_rxinfo rxinfo;

    if (info == NULL)
    {
        return S_INPUT;
    }

    info->count = 0;
    obd2vpw_rxinfo_init(&rxinfo);
    rxinfo.cmd = 0x19;
    status = obd2vpw_tx(readdtcframe,7);
    if (status != S_SUCCESS)
    {
        return status;
    }

    while(1)
    {
        status = obd2vpw_rx(&rxinfo);
        if (status != S_SUCCESS)
        {
            return status;
        }
        else if (rxinfo.rxlength != 3)
        {
            return S_BADCONTENT;
        }

        if (rxinfo.rxbuffer[0] == 0 && rxinfo.rxbuffer[1] == 0)
        {
            break;
        }
        else
        {
            info->codes[info->count] = 
                ((u16)rxinfo.rxbuffer[0] << 8) + (u16)rxinfo.rxbuffer[1];
            info->count++;
            if (info->count >= DTC_MAX_COUNT)
            {
                break;
            }
        }
    }//while(1)...

    return S_SUCCESS;
}

//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
void obd2vpw_testerpresent(void)
{
    //u8 tester6Cframe[4+1] = {0x6C, VPW_ECM_BROADCAST_ID, VPW_ECM_RESPONSE_ID, 0x3F};
    u8 tester8Cframe[4+1] = {0x8C, VPW_ECM_BROADCAST_ID, VPW_ECM_RESPONSE_ID, 0x3F};

    obd2vpw_tx((u8*)tester8Cframe,4);
    //        delays(10,'m');
    //        //TODOQ: 8C is just same as 6C but lower priority, I don't see why
    //        //need to send testerpresent twice with different priorities
    //        obd2vpw_tx((u8*)tester8Cframe,4,speedmode);
//    delays(10,'m');
    //obd2vpw_tx((u8*)tester6Cframe,4,speedmode);
    
    delays(10,'m');
}

/**
 *  @brief Get ecm unlock algorithm
 *
 *  @param [in]     seed        Unlock seed
 *  @param [in]     algoindex   Algorithm index
 *  @param [in]     commtype    Vehicle comm type
 *  @param [out]    key         Generated key
 *
 *  @return Status
 */
u8 obd2vpw_getecmunlockalgorithm(u8 *seed, u32 algoindex, VehicleCommType commtype,
                                 u8 *key)
{
    u8 status;

    switch (gObd2info.oemtype)
    {
#ifdef __DCX_MANUF__        
    case OemType_DCX:
        status = S_FAIL;
        break;
#endif
#ifdef __FORD_MANUF__
    case OemType_FORD:
        status = S_FAIL;
        break;
#endif
#ifdef __GM_MANUF__
    case OemType_GM:
        status = obd2_gm_generatekey(seed, algoindex, commtype, key);
        break;
#endif
    default:
        status = S_FAIL;
        break;
    }     
    return status;    
    
}
//------------------------------------------------------------------------------
// Unlock VPW ecm
// Input:   u32 algoindex
//          bool seed_peek (only get seed, don't send unlock key)
// Return:  u8  status
// Engineer: Quyen Leba
//------------------------------------------------------------------------------
u8 obd2vpw_unlockecm(u32 algoindex, bool seed_peek)
{
    u8 status;
    u8 ecmseed[2];
    u8 ecmkey[2];

    status = obd2vpw_security_access(SPSrequestSeed,ecmseed);
    if (status == S_SUCCESS)
    {
        if (seed_peek == FALSE)
        {
            if (ecmseed[0] == 0 && ecmseed[1] == 0)
            {
                //already unlocked
                return S_SUCCESS;
            }
            status = obd2vpw_getecmunlockalgorithm(ecmseed,algoindex,
                                                   CommType_VPW,ecmkey);
            if (status == S_SUCCESS)
            {
                status = obd2vpw_security_access(SPSsendKey,ecmkey);
            }
        }//if (seed_peek == FALSE)...
    }
    return status;
}

//------------------------------------------------------------------------------
// Input:   u16 ecm_type (index of ecm_defs)
// Return:  u8  status
// Engineer: Quyen Leba
//------------------------------------------------------------------------------
u8 obd2vpw_gm_setup_prior_upload(u16 ecm_type)
{
    //stop periodic message is not required, but just in case
    obd2vpw_stop_periodic_message();
    delays(200,'m');
    obd2vpw_testerpresent();
    obd2vpw_disablenormalcommunication();
    obd2vpw_testerpresent();
    delays(80,'m');
    return S_SUCCESS;
}

//------------------------------------------------------------------------------
// Input:   u16 ecm_type (index of ecm_defs)
// Return:  u8  status
// Engineer: Quyen Leba
//------------------------------------------------------------------------------
u8 obd2vpw_gm_setup_after_upload(u16 ecm_type)
{
    obd2vpw_tx_break();
    obd2vpw_init(TRUE, FALSE); // Switch back to low speed
    obd2vpw_testerpresent();
    obd2vpw_return_normal_mode(1);
    delays(6500,'m');
    return S_SUCCESS;
}

//------------------------------------------------------------------------------
// Input:   u16 ecm_type (index of ecm_defs)
// Return:  u8  status
// Engineer: Quyen Leba
//------------------------------------------------------------------------------
u8 obd2vpw_gm_setup_prior_download(u16 ecm_type)
{
//    obd2vpw_return_normal_mode(1);
//    delays(6500,'m');
    //stop periodic message is not required, but just in case
    obd2vpw_stop_periodic_message();
    delays(200,'m');
    obd2vpw_testerpresent();
    obd2vpw_disablenormalcommunication();
    obd2vpw_testerpresent();
    delays(80,'m');
    return S_SUCCESS;
}

//------------------------------------------------------------------------------
// Input:   u16 ecm_type (index of ecm_defs)
// Return:  u8  status
// Engineer: Quyen Leba
//------------------------------------------------------------------------------
u8 obd2vpw_gm_setup_after_download(u16 ecm_type)
{
    obd2vpw_tx_break();
    obd2vpw_init(TRUE, FALSE); // Switch back to low speed
    obd2vpw_testerpresent();
    obd2vpw_return_normal_mode(1);
    delays(6500,'m');
    return S_SUCCESS;
}

//------------------------------------------------------------------------------
// Utility file is a small worker loaded (downloaded) to ECM to help upload
// and download tune (i.e. read/write ECM flash memory). Not all ECMs requires.
// Inputs:  u32 ecm_type (defined for ecm_defs)
//          u8 utilitychoiceindex (which util to use)
//          bool isdownloadutility (TRUE: utility is for download tune)
// Return:  u8  status
// Engineer: Quyen Leba
// Date: Jan 10, 2008
// Note: data blocks are sent backward (last block first)
//------------------------------------------------------------------------------
u8 obd2vpw_downloadutility(u32 ecm_type, u8 utilitychoiceindex,
                           bool isdownloadutility)
{
#define MAX_VPW_UTILITY_FILESIZE        4096
    F_FILE *fptr;
    u8  *utilityfilename;
    u32 utilfilesize;
    u32 utilitydatastartposition;   //extracted from ubf
    u32 utilitydatalength;
    u32 blockaddress;
    u8  *utildatabuffer;
    u32 bytecount;
    bool useencryption;
    u8  blockindex;
    u8  status;

    fptr = NULL;
    utildatabuffer = NULL;
    blockindex = 0;     //VPW utility has only 1 block
    
    obd2vpw_testerpresent();

    if (isdownloadutility)
    {
        utilityfilename = (u8*)ECM_GetDUtiFilename(ecm_type,utilitychoiceindex);
        useencryption = (bool)ECM_IsDUtiUseEncryption(ecm_type,utilitychoiceindex);
        blockaddress = ECM_GetDUtiBlockAddr(ecm_type,utilitychoiceindex,blockindex);
    }
    else
    {
        utilityfilename = (u8*)ECM_GetUUtiFilename(ecm_type,utilitychoiceindex);
        useencryption = (bool)ECM_IsUUtiUseEncryption(ecm_type,utilitychoiceindex);
        blockaddress = ECM_GetUUtiBlockAddr(ecm_type,utilitychoiceindex,blockindex);
    }
    if (utilityfilename == NULL)
    {
        return S_UTILITYNOTREQUIRED;
    }
    else if (utilityfilename[0] == NULL)
    {
        return S_UTILITYNOTREQUIRED;
    }
    
    if(useencryption)
    {
        status = ubf_validate_file(utilityfilename);
        if (status != S_SUCCESS)
        {
            log_push_error_point(0x3220);
            return status;
        }
    }

    if((fptr = genfs_default_openfile(utilityfilename, "rb")) == NULL)
    {
        log_push_error_point(0x3210);
        return S_OPENFILE;
    }
    if ((fseek(fptr,0,SEEK_END)) != 0)
    {
        status = S_SEEKFILE;
        log_push_error_point(0x3211);
        goto obd2vpw_downloadutility_done;
    }
    utilfilesize = ftell(fptr);
//    if (utilfilesize > MAX_VPW_UTILITY_FILESIZE)
//    {
//        status = S_BADCONTENT;
//        log_push_error_point(0x3212);
//        goto obd2vpw_downloadutility_done;
//    }
    if ((fseek(fptr,0,SEEK_SET)) != 0)
    {
        status = S_SEEKFILE;
        log_push_error_point(0x3213);
        goto obd2vpw_downloadutility_done;
    }
    
    if (useencryption)
    {
        status = ubf_get_data_info(fptr,
                                   &utilitydatastartposition,&utilitydatalength);
        if (status != S_SUCCESS)
        {
            log_push_error_point(0x321B);
            goto obd2vpw_downloadutility_done;
        }
        else if (utilitydatalength == 0)
        {
            log_push_error_point(0x321C);
            status = S_BADCONTENT;
            goto obd2vpw_downloadutility_done;
        }
        else if (utilitydatastartposition+utilitydatalength > utilfilesize)
        {
            log_push_error_point(0x301D);
            status = S_BADCONTENT;
            goto obd2vpw_downloadutility_done;
        }
    }
    else
    {
        utilitydatastartposition = 0;
        utilitydatalength = utilfilesize;
    }

    if (utilitydatalength > MAX_VPW_UTILITY_FILESIZE)
    {
        log_push_error_point(0x301E);
        status = S_NOTFIT;
        goto obd2vpw_downloadutility_done;
    }
    if (fseek(fptr, utilitydatastartposition, SEEK_SET) != 0)
    {
        log_push_error_point(0x301F);
        status = S_SEEKFILE;
        goto obd2vpw_downloadutility_done;
    }

    //may consider to malloc with utilfilesize
    utildatabuffer = __malloc(MAX_VPW_UTILITY_FILESIZE);
    if (utildatabuffer == NULL)
    {
        status = S_MALLOC;
        log_push_error_point(0x3214);
        goto obd2vpw_downloadutility_done;
    }
    
    bytecount = fread(utildatabuffer,1,utilitydatalength,fptr);
    if (bytecount != utilitydatalength)
    {
        status = S_READFILE;
        log_push_error_point(0x3215);
        goto obd2vpw_downloadutility_done;
    }
    if ((bytecount % 8))
    {
        status = S_BADCONTENT;
        log_push_error_point(0x3216);
        goto obd2vpw_downloadutility_done;
    }
    if (useencryption)
    {
        crypto_blowfish_decryptblock_critical(utildatabuffer,bytecount);
    }
    genfs_closefile(fptr);
    fptr = NULL;

    status = obd2vpw_request_download(blockaddress/*,utilitydatalength*/);
    if (status != S_SUCCESS)
    {
        status = S_FAIL;
        log_push_error_point(0x3217);
        goto obd2vpw_downloadutility_done;
    }

    status = obd2vpw_helper_download_block(0x80,blockaddress,
                                           utilitydatalength,
                                           utildatabuffer);
//    status = obd2vpw_transfer_data_download(0x80,   //subservice of this service
//                                            utildatabuffer,
//                                            blockaddress,
//                                            utilitydatalength);
    if (status != S_SUCCESS)
    {
        status = S_FAIL;
        log_push_error_point(0x3218);
        goto obd2vpw_downloadutility_done;
    }
    delays(50,'m');

obd2vpw_downloadutility_done:
    if (fptr)
    {
        genfs_closefile(fptr);
    }
    if (utildatabuffer)
    {
        __free(utildatabuffer);
    }
    return status;
}

//------------------------------------------------------------------------------
// Exit utility session
// Input:   u16 ecm_type
// Return:  u8  status
// Engineer: Quyen Leba
// Note: this function only used in high speed mode
//------------------------------------------------------------------------------
u8 obd2vpw_exitutility(u16 ecm_type)
{
    u8 exitframe[14] = {0x6D,VPW_ECM_ID,VPW_ECM_RESPONSE_ID,
                        0x36, 0xAA,0x00,0x00,0xFF,0xFF,0xFF};
    u8 status;
    obd2vpw_rxinfo rxinfo;
    
    exitframe[10] = 0x03;
    exitframe[11] = 0xA7;
    
    obd2vpw_rxinfo_init(&rxinfo);
    rxinfo.cmd = 0x36;
    
    status = obd2vpw_tx(exitframe,12);
    return status;
}

//------------------------------------------------------------------------------
// Inputs:  u16 ecm_type (index of ecm_defs)
//          bool is_cal_only (TRUE: erase cal only)
// Return:  u8  status
// Engineer: Quyen Leba
//------------------------------------------------------------------------------
u8 obd2vpw_erase_ecm(u16 ecm_type, bool is_cal_only)
{
    u8  eraseframe[12+1] = {0x6D,VPW_ECM_ID,VPW_ECM_RESPONSE_ID,
                          0x36, 0xAA, 0x00, 0x00, 0x00, 0x00};
    u8  status;
    u8  rxretry;
    u8  eraseretry;
    obd2vpw_rxinfo rxinfo;

    //TODOQ: use erase command from ecm_defs
    if (is_cal_only)
    {
        eraseframe[9] = 0x01;
        eraseframe[11] = 0xAB;
    }
    else
    {
        eraseframe[9] = 0x02;
        eraseframe[11] = 0xAC;
    }
    eraseframe[10] = 0;
    
    obd2vpw_rxinfo_init(&rxinfo);
    rxinfo.cmd = 0x36;

//    status = obd2vpw_tx(eraseframe,12,VPW_HIGH_SPEED);
//    if (status != S_SUCCESS)
//    {
//        log_push_error_point(0x3219);
//        return S_FAIL;
//    }
//
//    status = S_FAIL;
//    retry = 0;
//    while(retry++ < VPW_RX_MAX_RETRY_COUNT)
//    {
//        //TODOQ: probably not need retry, but just in case
//        status = obd2vpw_rx(&rxinfo);
//        if (status == S_SUCCESS)
//        {
//            if (rxinfo.rxbuffer[1] == 0x73)
//            {
//                status = S_SUCCESS;
//                break;
//            }
//        }
//    }
//    return status;

    eraseretry = 0;
obd2vpw_erase_ecm_retry:
    status = obd2vpw_txrx_simple(eraseframe,12,rxinfo.rxbuffer,&rxinfo.rxlength);
    if (status == S_SUCCESS && rxinfo.rxbuffer[5] == 0x73)
    {
        return S_SUCCESS;
    }
    else
    {
        rxretry = 0;
obd2vpw_erase_rx_retry:
        status = obd2vpw_rx(&rxinfo);
        if (status == S_SUCCESS)
        {
            if (rxinfo.rxbuffer[1] == 0x73)
            {
                return S_SUCCESS;
            }
        }
        if (rxretry++ < 3)
        {
            goto obd2vpw_erase_rx_retry;
        }

        if (eraseretry++ < 3)
        {
            goto obd2vpw_erase_ecm_retry;
        }
    }

    log_push_error_point(0x3219);
    return S_FAIL;
}

//------------------------------------------------------------------------------
// Inputs:  u16 pid
//          VehicleCommType commtype,
//          VehicleCommLevel commlevel
// Return:  u8  status
// Engineer: Quyen Leba
// TODOQ: commlevel is currently not used
//------------------------------------------------------------------------------
u8 obd2vpw_validatepid(DlxBlock *dlxblock, VehicleCommLevel commlevel)
{
    return obd2vpw_validateByRapidPacketSetup(dlxblock, commlevel);
}

//------------------------------------------------------------------------------
// Inputs:  u16 pid
//          VehicleCommType commtype,
//          VehicleCommLevel commlevel
// Return:  u8  status
// Engineer: Quyen Leba
// TODOQ: commlevel is currently not used
//------------------------------------------------------------------------------
u8 obd2vpw_validatedmr(DlxBlock *dlxblock, VehicleCommLevel commlevel)
{
    return obd2vpw_validateByRapidPacketSetup(dlxblock, commlevel);
}

//------------------------------------------------------------------------------
// Inputs:  u16 pid
//          VehicleCommType commtype,
//          VehicleCommLevel commlevel
// Return:  u8  status
// Engineer: Quyen Leba
// TODOQ: commlevel is currently not used
//------------------------------------------------------------------------------
u8 obd2vpw_validateByRapidPacketSetup(DlxBlock *dlxblock, VehicleCommLevel commlevel)
{
    u8 status;
    
    status = obd2vpw_dynamicallydefinemessage(GM_GMLAN_VPW_PACKETID_START,
                                              dlxblock->pidAddr,
                                              (PidType)dlxblock->pidType,
                                              dlxblock->pidSize,1);
    return status;
}


//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
void obd2vpw_datalog_sendtesterpresent()
{
    const u8 tester6Cframe[4+1] = {0x6C, VPW_ECM_BROADCAST_ID, VPW_ECM_RESPONSE_ID, 0x3F};

    obd2vpw_tx((u8*)tester6Cframe,4);
}

//------------------------------------------------------------------------------
// Check if all signals fit in available packets
// Input:   VehicleCommLevel commlevel[ECM_MAX_COUNT]
// Return:  u8  status
// Engineer: Quyen Leba, Tristen Pierson
//------------------------------------------------------------------------------
u8 obd2vpw_datalog_evaluatesignalsetup(VehicleCommLevel *vehiclecommlevel)
{
    u8  packetid;
    u8  byteavailable[2][GM_GMLAN_VPW_PACKETID_START-GM_GMLAN_VPW_PACKETID_END];
    u8  processcheck[MAX_DATALOG_DLXBLOCK_COUNT];
    u8  ecm_index;
    u8  processedperloop;
    u8  index;
    u16 i;
    bool packet;
    u16 pidsignalcount;
    u16 dmrsignalcount;
    u16 analogsignalcount;

    if (dataloginfo == NULL)
    {
        return S_BADCONTENT;
    }
    // If OBD PIDs are datalogged, we cannot start a packet session
    if (vehiclecommlevel[0] == CommLevel_Unknown  || gDatalogmode.obdPids == TRUE)
    {
        gDatalogmode.packetmode = SingleRate;
    }
    
    memset(processcheck,FALSE,MAX_DATALOG_DLXBLOCK_COUNT);
    memset((char*)byteavailable,GM_GMLAN_VPW_MAX_DATABYTE_PER_PACKET,sizeof(byteavailable));        
    pidsignalcount = dmrsignalcount = analogsignalcount = 0;
    dataloginfo->packetidcount[0] = 0;
    dataloginfo->packetidcount[1] = 0;
  
    // Evaluate analog signals, reset packet information
    for(i=0;i<dataloginfo->datalogsignalcount;i++)
    {
        if (dataloginfo->datalogsignals[i].PidType == PidTypeAnalog &&
            (dataloginfo->datalogsignals[i].Control & DATALOG_CONTROL_SKIP) == 0 &&
            analogsignalcount < dataloginfo->analogsignalmaxavailable)
        {
            dataloginfo->analogsignalindex[analogsignalcount++] = i;
        }
        // Remove packet information since we may be recovering from
        // S_NOTFIT during rapid packet setup
        dataloginfo->datalogsignals[i].SignalType.Generic.PacketNumber = DATALOG_INVALID_PACKETID;
        dataloginfo->datalogsignals[i].SignalType.Generic.Position = 0;
    }

    // Evaluate Rapid Packet Siganls
    if (gDatalogmode.packetmode == RapidRate)
    {
        // Fill datalog table and packets if rapid packet supported 
        for(ecm_index=0;ecm_index<1;ecm_index++)
        {            
            packetid = GM_GMLAN_VPW_PACKETID_START;
            index = 0;
            
            while(packetid != GM_GMLAN_VPW_PACKETID_END)
            {    
                processedperloop = 0;
                packet = FALSE;
                
                for(i=0;i<dataloginfo->datalogsignalcount;i++)
                {                  
                    if (processcheck[i])
                    {
                        //already processed
                        continue;
                    }
                    if (dataloginfo->datalogsignals[i].SignalType.Generic.PacketNumber == DATALOG_INVALID_PACKETID &&
                        dataloginfo->datalogsignals[i].EcmIndex == ecm_index &&                     
                        (dataloginfo->datalogsignals[i].PidType == PidTypeRegular ||
                        dataloginfo->datalogsignals[i].PidType == PidTypeDMR ||
                        dataloginfo->datalogsignals[i].PidType == PidTypeMode1) &&
                        (dataloginfo->datalogsignals[i].Control & DATALOG_CONTROL_SKIP) == 0 &&
                        dataloginfo->datalogsignals[i].Size <= byteavailable[ecm_index][index])
                    {                    
                        processcheck[i] = TRUE;
                        processedperloop++;
                        switch (dataloginfo->datalogsignals[i].PidType)
                        {
                            case PidTypeMode1:
                            case PidTypeRegular:
                                pidsignalcount++;
                                break;
                            case PidTypeDMR:                            
                                dataloginfo->dmrsignalindex[dmrsignalcount++] = i;
                                break;
                            default:
                                break;
                        }
                        // Mode 1 PIDs do not use packets 
                        if (dataloginfo->datalogsignals[i].PidType == PidTypeMode1)
                        {
                            continue;
                        }                        
                        // Position starts from 1
                        dataloginfo->datalogsignals[i].SignalType.Generic.Position = 1 + GM_GMLAN_VPW_MAX_DATABYTE_PER_PACKET - byteavailable[ecm_index][index];
                        byteavailable[ecm_index][index] -= dataloginfo->datalogsignals[i].Size;
                        dataloginfo->datalogsignals[i].SignalType.Generic.PacketNumber = packetid;
                        packet = TRUE;
                    }                    
                }//for(i=...
             
                if (packet == TRUE)
                {
                    dataloginfo->packetidlist[ecm_index][dataloginfo->packetidcount[ecm_index]] = packetid;
                    dataloginfo->packetidcount[ecm_index]++;
                    index++;
                    packetid--; 
                }                
                if ((pidsignalcount+dmrsignalcount+analogsignalcount) >= dataloginfo->datalogsignalcount)
                {
                    break;
                }
                else if (processedperloop == 0)
                {
                    break;
                }
            }//while(packetid...
            if ((pidsignalcount+dmrsignalcount+analogsignalcount) >= dataloginfo->datalogsignalcount)
            {
                break;
            }
        }//for(ecm_index=...
    }
    
    // Evaluate Non Rapid Packet Signals    
    if (gDatalogmode.packetmode == SingleRate)
    {
        for(ecm_index=0;ecm_index<1;ecm_index++)
        {            
            for(i=0;i<dataloginfo->datalogsignalcount;i++)
            {                
                // Handle Non Rapid Packets
                if (dataloginfo->datalogsignals[i].EcmIndex == ecm_index &&
                    (dataloginfo->datalogsignals[i].Control & DATALOG_CONTROL_SKIP) == 0)
                {
                    switch (dataloginfo->datalogsignals[i].PidType)
                    {
                        case PidTypeRegular:
                        case PidTypeMode1:
                            pidsignalcount++;
                            break;
                        case PidTypeDMR:
                            dataloginfo->dmrsignalindex[dmrsignalcount++] = i;
                            break;
                        default:
                            break;
                    }
                }
            }//for(i=...
            if ((pidsignalcount+dmrsignalcount+analogsignalcount) >= dataloginfo->datalogsignalcount)
            {
                break;
            }
        }//for(ecm_index=...
    }
    
    dataloginfo->pidsignalcount = pidsignalcount;
    dataloginfo->dmrsignalcount = dmrsignalcount;
    dataloginfo->analogsignalcount = analogsignalcount;
    
    if ((pidsignalcount+dmrsignalcount+analogsignalcount) < dataloginfo->datalogsignalcount)
    {
        //not enough packets to fit all signals
        return S_NOTFIT;
    }
    return S_SUCCESS;
}

//------------------------------------------------------------------------------
// Initiate pids into packets
// Input:   VehicleCommLevel commlevel[ECM_MAX_COUNT]
// Return:  u8  status
// Engineer: Quyen Leba
//------------------------------------------------------------------------------
u8 obd2vpw_datalog_initiatepackets(VehicleCommLevel *vehiclecommlevel)
{
    u8  packetid;
    u16 i;
    u8  status;
    
    status = S_FAIL;

    for(packetid=GM_GMLAN_VPW_PACKETID_START;packetid>=GM_GMLAN_VPW_PACKETID_END;packetid--)
    {
        for(i=0;i<dataloginfo->datalogsignalcount;i++)
        {
            if (dataloginfo->datalogsignals[i].SignalType.Generic.PacketNumber == packetid)
            {
                status = obd2vpw_dynamicallydefinemessage
                    (packetid, dataloginfo->datalogsignals[i].Address,
                     (PidType)dataloginfo->datalogsignals[i].PidType,
                     dataloginfo->datalogsignals[i].Size,
                     dataloginfo->datalogsignals[i].SignalType.Generic.Position);
                if (status != S_SUCCESS)
                {
                    //TODOQ: this might be too strict, but there was a lot of checking
                    return S_FAIL;
                }
            }//if (dataloginfo->...
        }//for(i=...
    }//for(packetid=...
    return status;
}

//------------------------------------------------------------------------------
// Get datalog data in rapidpacket mode
// Input:   VehicleCommLevel commlevel[ECM_MAX_COUNT]
// Return:  u8  status
// Engineer: Quyen Leba
//------------------------------------------------------------------------------
u8 obd2vpw_datalog_getdata(VehicleCommLevel *vehiclecommlevel)
{
    obd2vpw_rxinfo rxinfo;
    u8  i;
    u8  status;

    obd2vpw_rxinfo_init(&rxinfo);
    rxinfo.cmd = 0x2A;
    
    for(i=0;i<dataloginfo->packetidcount[0]+dataloginfo->packetidcount[1];i++)
    {
        status = obd2vpw_rx(&rxinfo);
        if (status == S_SUCCESS)
        {
            obd2datalog_parserapidpacketdata(0, rxinfo.rxbuffer[0],&rxinfo.rxbuffer[0]);            
            dataloginfo->lastdatapoint_timestamp = rtc_getvalue();
        }
    }
    
    if (rtc_getvalue() - dataloginfo->lastdatapoint_timestamp >=
        DATALOG_MAX_DATAPOINT_INTERVAL)
    {
        obd2vpw_normalmode();
        return S_COMMLOST;
    }
    else
    {
        return S_SUCCESS;
    }
}

//------------------------------------------------------------------------------
// 
// Inputs:  
// Return:  u8  status
// Engineer: Quyen Leba
//------------------------------------------------------------------------------
u8 obd2vpw_helper_upload_block(u32 address, u16 length,
                               u8 *buffer, u16 *bufferlength)
{
    struct
    {
        u32 flags;
        u8  type;
        u8  x;
        u16 length;
        u32 address;
        u8  reserved[16];
    }vpw_readblock_info;
    u8  status;

    memset((char*)&vpw_readblock_info,0,sizeof(vpw_readblock_info));
    vpw_readblock_info.type = Obd2vpwHelper_ReadblockByCmd35single36;
    vpw_readblock_info.length = length;
    vpw_readblock_info.address = address;
    status = mblexec_call(MBLEXEC_OPCODE_READBLOCK_HELPER,
                          (u8*)&vpw_readblock_info,
                          sizeof(vpw_readblock_info),
                          buffer,bufferlength);
    if (status == S_SUCCESS && length != *bufferlength)
    {
        return S_BADCONTENT;
    }
    return status;
}

//------------------------------------------------------------------------------
// 
// Inputs:  
// Return:  u8  status
// Engineer: Quyen Leba
//------------------------------------------------------------------------------
u8 obd2vpw_helper_download_block(u8 subservice, u32 address, u16 length,
                                 u8 *buffer)
{
    struct
    {
        u32 flags;
        u8  type;
        u8  x;
        u16 length;
        u32 address;
        u8  subservice;
        u8  reserved[15];
    }vpw_writeblock_info;
    u8  status;

    memset((char*)&vpw_writeblock_info,0,sizeof(vpw_writeblock_info));
    vpw_writeblock_info.type = Obd2vpwHelper_WriteblockByCmdSingle36;
    vpw_writeblock_info.length = length;
    vpw_writeblock_info.address = address;
    vpw_writeblock_info.subservice = subservice;
    status = mblexec_call(MBLEXEC_OPCODE_WRITEBLOCK_HELPER,
                          (u8*)&vpw_writeblock_info,
                          sizeof(vpw_writeblock_info),
                          buffer,&length);
    return status;
}

//------------------------------------------------------------------------------
// Test if ignition key is on using download related command
// Input:   u16 ecm_type
// Return:  u8  status
// Engineer: Quyen Leba
//------------------------------------------------------------------------------
u8 obd2vpw_download_test_key_on(u16 ecm_type)
{
    u8  status;

    status = obd2vpw_unlockecm(0, TRUE);    //peek only
    return status;
}

/**
 *  @brief Get VPW OBD2 Info
 *  
 *  @param [in] obd2info OBD2 info structure
 *  @return status
 *  
 *  @details This function identifies and returns if applicable VPW information
 *           to the obd2info structure.
 */
u8 obd2vpw_getinfo(obd2_info *obd2info)
{
    u8 vin[VIN_LENGTH+1];
    partnumber_info partnumber;
    u8 tmpcount;
    u8 temp[2];
    u8 i;
    u8 status;
    
    if (obd2info == NULL)
    {
        return S_INPUT;
    }
    
    obd2info->ecu_block[obd2info->ecu_count].partnumber_count = 0;
    
    obd2vpw_init(TRUE, FALSE);
    status = obd2vpw_ping();
    if (status == S_SUCCESS)
    {
        /* Main OBD2 Info (read only if primary ECU) */
        if (obd2info->ecu_count == 0)
        {
            obd2vpw_readvin(vin);
            memcpy(obd2info->vin, vin, sizeof(vin));
        }
        
        /* OBD2 Info Block */
        obd2info->ecu_block[obd2info->ecu_count].ecutype = EcuType_ECM;
        obd2info->ecu_block[obd2info->ecu_count].commtype = CommType_VPW;
        obd2info->ecu_block[obd2info->ecu_count].commlevel = CommLevel_Unknown;
        obd2info->ecu_block[obd2info->ecu_count].genericcommlevel = GenericCommLevel_Legacy;
        /* obd2info->ecu_block[obd2info->ecu_count].ecu_id <- N/A */        
        obd2info->ecu_block[obd2info->ecu_count].vehicle_type = INVALID_VEH_DEF;
        
        /* OBD2 Part Numbers */
        if (obd2vpw_read_data_bypid(0x000C, 2, temp) == S_SUCCESS)
        {
            obd2info->oemtype = OemType_GM;
            obd2info->ecu_block[obd2info->ecu_count].commlevel = CommLevel_GMLAN;

            status = obd2vpw_readSoftwarePartNumber(&partnumber);
            if (status == S_SUCCESS && partnumber.count > 0)
            {
                obd2info->ecu_block[obd2info->ecu_count].partnumber_count = partnumber.count;
                strcpy((char*)obd2info->ecu_block[obd2info->ecu_count].partnumbers.gm.bcc,
                       (char*)partnumber.bcc);
                
                for(i = 0, tmpcount = 0; i < ECM_MAX_PART_COUNT; i++)
                {
                    obd2info->ecu_block[obd2info->ecu_count].partnumbers.gm.partnumber_id[i] = partnumber.id[i];
                    if (partnumber.number[i] == 0)
                    {
                        strcpy((char*)obd2info->ecu_block[obd2info->ecu_count].partnumbers.gm.partnumber[i],"N/A");
                    }
                    else
                    {
                        sprintf((char*)obd2info->ecu_block[obd2info->ecu_count].partnumbers.gm.partnumber[i],
                                "%08u",partnumber.number[i]);   // Requires 8 digits and should be unsigned
                        if (++tmpcount >= partnumber.count)
                        {
                            break;
                        }
                    }
                }
            }
            if (status == S_SUCCESS)
            {
                status = obd2vpw_readserialnumber(obd2info->ecu_block[obd2info->ecu_count].partnumbers.gm.vehicle_serial_number);
            }
            if (status == S_SUCCESS)
            {
                status = obd2vpw_readhardwareid(obd2info->ecu_block[obd2info->ecu_count].partnumbers.gm.hwid);
            }
            
        }
        obd2info->ecu_count++;
    }
    return status;
}
