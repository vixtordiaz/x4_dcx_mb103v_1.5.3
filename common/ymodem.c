/**
**************** (C) COPYRIGHT 2011 SCT Performance, LLC *********************
* File Name          : ymodem.c
* Device / System    : SCT products using the "CommonCode" platform
* Author             : Quyen Leba
*
* Version            : 1 
* Date               : 05/02/2011
* Description        : 
*                    : 
*
* Usage              : ymodem_init(); ymodem_set_fileinfo(...);
*                      repeat ymodem_send_datablock(...) to send a file or
*                      repeat ymodem_receive_datablock(...) to receive a file
*                      ymodem_deinit() to finish
*
* History            : 05/02/2011 M. Davis
*                    :   Add SCT Copyright header,
*
******************************************************************************
*/

#include <string.h>
#include <stdlib.h>
#include <board/genplatform.h>
#include <board/delays.h>
#include <common/itoa.h>
#include <common/statuscode.h>
#include "ymodem.h"

#define USER_FLASH_SIZE             256*1024    //TODOQ:
#define ymodem_log_push_error_point log_push_error_point

typedef struct
{
    pFuncYmodemSendByte sendbyte;
    pFuncYmodemRecvByte receivebyte;
    struct
    {
        u8  initialpacket_sent      :1;
        u8  use_crc16               :1;
        u8  reserved                :6;
    }control;
    u8  filename[FILE_NAME_LENGTH+1];
    u32 filesize;
    u32 bytecount;
    u16 blocknumber;
}YmodemInfo;
YmodemInfo  *ymodem_info = NULL;

//------------------------------------------------------------------------------
// Init a ymodem session
// Return:  u8  status
// Engineer: Quyen Leba
//------------------------------------------------------------------------------
u8 ymodem_init(pFuncYmodemSendByte sendfunc, pFuncYmodemRecvByte recievefunc)
{
    if(!ymodem_info)
    {
        ymodem_info = __malloc(sizeof(YmodemInfo));
        if (ymodem_info == NULL)
        {
            return S_MALLOC;
        }
        ymodem_info->sendbyte = sendfunc;
        ymodem_info->receivebyte = recievefunc;
        ymodem_info->control.initialpacket_sent = 0;
        ymodem_info->control.use_crc16 = 0;
        ymodem_info->filesize = 0;
        ymodem_info->blocknumber = 1;
        ymodem_info->bytecount = 0;
    }
    return S_SUCCESS;
}

//------------------------------------------------------------------------------
// Init a ymodem session
// Return:  u8  status
// Engineer: Quyen Leba
//------------------------------------------------------------------------------
void ymodem_deinit()
{
    if (ymodem_info)
    {
        __free(ymodem_info);
        ymodem_info = NULL;
    }
}

//------------------------------------------------------------------------------
// Set file info (file name & file size)
// Return:  u8  status
// Engineer: Quyen Leba
//------------------------------------------------------------------------------
u8 ymodem_set_fileinfo(const u8 *filename, u32 filesize)
{
    if (strlen((char*)filename) > FILE_NAME_LENGTH)
    {
        return S_INPUT;
    }
    else if (ymodem_info == NULL)
    {
        return S_ERROR;
    }
    strcpy((char*)ymodem_info->filename,(char*)filename);
    ymodem_info->filesize = filesize;
    return S_SUCCESS;
}


//------------------------------------------------------------------------------
// Receive a packet from sender
// Output:  
// Return:  u8  status
// Engineer: Quyen Leba
//------------------------------------------------------------------------------
static u8 receivebyte(u8 *c, u32 timeout)
{
    while (timeout-- > 0)
    {
        if (ymodem_info->receivebyte)
        {
            if (ymodem_info->receivebyte(c) == S_SUCCESS)
            {
                return S_SUCCESS;
            }
        }
    }
    return S_TIMEOUT;
}

//------------------------------------------------------------------------------
// Receive a packet from sender
// Output:  
// Return:  u8  status
// Engineer: Quyen Leba
//------------------------------------------------------------------------------
static u8 sendbyte(u8 c)
{
    if (ymodem_info->sendbyte)
    {
        ymodem_info->sendbyte(c);
    }
    return S_SUCCESS;
}

//------------------------------------------------------------------------------
// Receive a packet from sender
// Input:   u32 timeout
// Output:  u8  *data
//          s32 *datalength
// Return:  u8  status
// Engineer: Quyen Leba
//------------------------------------------------------------------------------
u8 ymodem_receivepacket(u8 *data, s32 *datalength, u32 timeout)
{
    u16 i, packet_size;
    u8 c;

    *datalength = 0;
    if (receivebyte(&c, timeout) != S_SUCCESS)
    {
        return S_FAIL;
    }

    switch (c)
    {
    case SOH:
        packet_size = PACKET_SIZE;
        break;
    case STX:
        packet_size = PACKET_1K_SIZE;
        break;
    case EOT:
        return S_SUCCESS;
    case CA:
        if ((receivebyte(&c, timeout) == S_SUCCESS) && (c == CA))
        {
            *datalength = -1;
            return S_SUCCESS;
        }
        else
        {
            return S_FAIL;
        }
    case ABORT1:
    case ABORT2:
        return S_USERABORT;
    default:
        return S_FAIL;
    }
    *data = c;
    for (i = 1; i < (packet_size + PACKET_OVERHEAD); i ++)
    {
        if (receivebyte(data + i, timeout) != S_SUCCESS)
        {
            return S_FAIL;
        }
    }
    if (data[PACKET_SEQNO_INDEX] != ((data[PACKET_SEQNO_COMP_INDEX] ^ 0xff) & 0xff))
    {
        return S_FAIL;
    }
    *datalength = packet_size;
    return S_SUCCESS;
}

//------------------------------------------------------------------------------
// Receive a file using the ymodem protocol
// Output:  
// Return:  u8  status
// Engineer: Quyen Leba
// Note: not finished; not required.
//------------------------------------------------------------------------------
u8 ymodem_receive_file(u8 *buf)
{
    u8  packet_data[PACKET_1K_SIZE + PACKET_OVERHEAD];
    u8  file_size[FILE_SIZE_LENGTH];
    u8  *file_ptr;
//    u8  *buf_ptr;
    s32 i, packet_length, session_done, file_done, packets_received, errors, session_begin, size = 0;

    for (session_done = 0, errors = 0, session_begin = 0; ;)
    {
        for (packets_received = 0, file_done = 0/*, buf_ptr = buf*/; ;)
        {
            switch (ymodem_receivepacket(packet_data, &packet_length, NAK_TIMEOUT))
            {
            case S_SUCCESS:
                errors = 0;
                switch (packet_length)
                {
                // Abort by sender
                case -1:
                    sendbyte(ACK);
                    ymodem_info->filesize = 0;
                    return S_USERABORT;
                // End of transmission
                case 0:
                    sendbyte(ACK);
                    file_done = 1;
                    break;
                // Normal packet
                default:
                    if ((packet_data[PACKET_SEQNO_INDEX] & 0xff) != (packets_received & 0xff))
                    {
                        sendbyte(NAK);
                    }
                    else
                    {
                        if (packets_received == 0)
                        {
                            // Filename packet
                            if (packet_data[PACKET_HEADER] != 0)
                            {
                                // Filename packet has valid data
                                for (i = 0, file_ptr = packet_data + PACKET_HEADER; (*file_ptr != 0) && (i < FILE_NAME_LENGTH);)
                                {
                                    ymodem_info->filename[i++] = *file_ptr++;
                                }
                                ymodem_info->filename[i++] = '\0';
                                for (i = 0, file_ptr ++; (*file_ptr != ' ') && (i < FILE_SIZE_LENGTH);)
                                {
                                    file_size[i++] = *file_ptr++;
                                }
                                file_size[i++] = '\0';
                                size = atoi((char*)file_size);

                                // Test the size of the image to be sent
                                // Image size is greater than Flash size
                                if (size > (USER_FLASH_SIZE + 1))
                                {
                                    // End session
                                    sendbyte(CA);
                                    sendbyte(CA);
                                    ymodem_info->filesize = 0;
                                    return S_BADCONTENT;
                                }

                                sendbyte(ACK);
                                sendbyte(CRC16);
                            }
                            // Filename packet is empty, end session
                            else
                            {
                                sendbyte(ACK);
                                file_done = 1;
                                session_done = 1;
                                break;
                            }
                        }
                        else    // data packet
                        {
                            //TODOQ: do something with the data
                        }
                        packets_received ++;
                        session_begin = 1;
                    }
                }
                break;
            case S_USERABORT:
                sendbyte(CA);
                sendbyte(CA);
                ymodem_info->filesize = 0;
                return S_USERABORT;
            default:
                if (session_begin > 0)
                {
                    errors ++;
                }
                if (errors > MAX_ERRORS)
                {
                    sendbyte(CA);
                    sendbyte(CA);
                    return 0;
                }
                sendbyte(CRC16);
                break;
            }
            if (file_done != 0)
            {
                break;
            }
        }
        if (session_done != 0)
        {
            break;
        }
    }
    ymodem_info->filesize = size;
    return S_SUCCESS;
}

//------------------------------------------------------------------------------
// Prepare the first block
// Inputs:  u8  *data
//          const u8 *filename
// 
// Return:  u8  status
// Engineer: Quyen Leba
//------------------------------------------------------------------------------
void ymodem_prepare_initalpacket(u8 *data, const u8 *filename, u32 length)
{
    u16 i, j;
    u8 file_ptr[10];

    // make first three packet
    data[0] = STX;
    data[1] = 0x00;
    data[2] = 0xff;

    // filename packet has valid data
    for (i = 0; (filename[i] != '\0') && (i < FILE_NAME_LENGTH);i++)
    {
        data[i + PACKET_HEADER] = filename[i];
    }
    data[i + PACKET_HEADER] = 0x00;

    itoa(length,file_ptr,10);
    for (j = 0, i = i + PACKET_HEADER + 1; file_ptr[j] != '\0';)
    {
        data[i++] = file_ptr[j++];
    }

    for (j = i; j < PACKET_1K_SIZE + PACKET_HEADER; j++)
    {
        data[j] = 0;
    }
}

void ymodem_prepare_initalpacket_128(u8 *data, const u8 *filename, u32 length)
{
    u16 i, j;
    u8 file_ptr[10];

    // make first three packet
    data[0] = SOH;
    data[1] = 0x00;
    data[2] = 0xff;
    
    // filename packet has valid data
    for (i = 0; (filename[i] != '\0') && (i < FILE_NAME_LENGTH);i++)
    {
        data[i + PACKET_HEADER] = filename[i];
    }
    data[i + PACKET_HEADER] = 0x00;

    itoa(length,file_ptr,10);
    for (j = 0, i = i + PACKET_HEADER + 1; file_ptr[j] != '\0';)
    {
        data[i++] = file_ptr[j++];
    }

    for (j = i; j < PACKET_SIZE + PACKET_HEADER; j++)
    {
        data[j] = 0;
    }
}

//------------------------------------------------------------------------------
// Prepare the data packet
// Inputs:  u8  *srcbuffer
//          u8  pktNo
//          u32 blocksize
// Output:  u8  *data
// Engineer: Quyen Leba
//------------------------------------------------------------------------------
void ymodem_preparepacket(u8 *srcbuffer, u8 *data, u8 pktNo, u32 blocksize)
{
    u16 i, size, packetSize;
    u8  *file_ptr;

    // Make first three packet
    packetSize = blocksize >= PACKET_1K_SIZE ? PACKET_1K_SIZE : PACKET_SIZE;
    size = blocksize < packetSize ? blocksize :packetSize;
    if (packetSize == PACKET_1K_SIZE)
    {
        data[0] = STX;
    }
    else
    {
        data[0] = SOH;
    }
    data[1] = pktNo;
    data[2] = (~pktNo);
    file_ptr = srcbuffer;

    // Filename packet has valid data
    for (i = PACKET_HEADER; i < size + PACKET_HEADER;i++)
    {
        data[i] = *file_ptr++;
    }
    if ( size  <= packetSize)
    {
        for (i = size + PACKET_HEADER; i < packetSize + PACKET_HEADER; i++)
        {
            data[i] = 0x1A; // EOF (0x1A) or 0x00
        }
    }
}

//------------------------------------------------------------------------------
// Update CRC16 for input byte
// Inputs:  u16 current_crc16
//          u8  new_byte
// Return:  u16 new_crc16
// Engineer: Quyen Leba
//------------------------------------------------------------------------------
u16 __update_crc16(u16 current_crc16, u8 new_byte)
{
    u32 crc = current_crc16;
    u32 in = new_byte | 0x100;

    do
    {
        crc <<= 1;
        in <<= 1;
        if(in & 0x100)
        {
            ++crc;
        }
        if(crc & 0x10000)
        {
            crc ^= 0x1021;
        }
    }while(!(in & 0x10000));

    return crc & 0xffffu;
}

//------------------------------------------------------------------------------
// Calculare CRC16 for ymodem packet
// Inputs:  const u8 *data
//          u32 datalength
// Return:  u8  status
// Engineer: Quyen Leba
//------------------------------------------------------------------------------
u16 ymodem_calc_crc16(const u8 *data, u32 datalength)
{
    u32 i;
    u32 count;
    u8  *ptr = (u8*)data;
    s32 crc;

    crc    = 0;
    count = 0;
    while (count++ < datalength)
    {
        crc = crc ^ (s32)*ptr++ << 8;
        for (i = 0; i < 8; ++i)
        {
            if (crc & 0x8000)
                crc = crc << 1 ^ 0x1021;
            else
                crc = crc << 1;
        }
    }
    return (crc    & 0xFFFF);
//    u32 crc = 0;
//    const u8* dataEnd = data+datalength;
//    
//    while(data < dataEnd)
//    {
//        crc = __update_crc16(crc, *data++);
//    }
//
//    crc = __update_crc16(crc, 0);
//    crc = __update_crc16(crc, 0);
//
//    return crc&0xffffu;
}

//------------------------------------------------------------------------------
// Calculate checksum for ymodem packet
// Input:   const u8 *data
//          u32 size
// Return:  u8  checksum
// Engineer: Quyen Leba
//------------------------------------------------------------------------------
u8 ymodem_calc_checksum(const u8 *data, u32 size)
{
    u32 sum = 0;
    const u8* dataEnd = data+size;

    while(data < dataEnd)
    {
        sum += *data++;
    }

    return (sum & 0xffu);
}

//------------------------------------------------------------------------------
// Send a data packet using ymodem protocol
// Output:  
// Return:  u8  status
// Engineer: Quyen Leba
//------------------------------------------------------------------------------
void ymodem_sendpacket(u8 *data, u16 length)
{
    u16 i;

    for(i=0;i<length;i++)
    {
        sendbyte(data[i]);
    }
}

//------------------------------------------------------------------------------
// Send file datablock using ymodem protocol
// Inputs:  u8  *blockdata
//          u32 blockdatalength
// Return:  u8  status
// Engineer: Quyen Leba
// Note: repeat this function to send all file content
//------------------------------------------------------------------------------
u8 ymodem_send_datablock(u8 *blockdata, u32 blockdatalength)
{
    u8  packet_data[PACKET_1K_SIZE + PACKET_OVERHEAD];
    u8  *bptr;
    u32 blength;
    u8  tempCheckSum;
    u16 tempCRC;
    u8  receivedC[2];
    u8  i;
    u32 errors;
    u32 ackReceived;
    u32 pktSize;
    u32 retry;

    if (ymodem_info == NULL)
    {
        return S_ERROR;
    }

    ymodem_info->control.use_crc16 = 1;

    //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    // Initial Packet
    //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    errors = 0;
    ackReceived = 0;
    if (ymodem_info->control.initialpacket_sent == 0)
    {
        // Prepare first block
        ymodem_prepare_initalpacket(&packet_data[0],
                                    ymodem_info->filename,
                                    ymodem_info->filesize);
        do
        {
            // Send Packet
            ymodem_sendpacket(packet_data, PACKET_1K_SIZE + PACKET_HEADER);
            
            // Send CRC or checksum
            if (ymodem_info->control.use_crc16)
            {
                tempCRC = ymodem_calc_crc16(&packet_data[3], PACKET_1K_SIZE);
                //tempCRC--;
                sendbyte(tempCRC >> 8);
                sendbyte(tempCRC & 0xFF);
            }
            else
            {
                tempCheckSum = ymodem_calc_checksum(&packet_data[3], PACKET_1K_SIZE);
                sendbyte(tempCheckSum);
            }

            // Wait for Ack and 'C'
            if (receivebyte(&receivedC[0], 4*10000) == S_SUCCESS)
            {
                if (receivedC[0] == ACK)
                {
                    // Packet transferred correctly
                    ackReceived = 1;
                }
                else
                {
                    errors++;
                }
            }
            else
            {
                errors++;
            }
        }while(!ackReceived && (errors < 0x0A));

        if (errors >= 0x0A)
        {
            return S_FAIL;
        }

        ymodem_info->control.initialpacket_sent = 1;
        ymodem_info->blocknumber = 0x01;
        ymodem_info->bytecount = 0;

        retry = 8000;   //wait about 8sec for 'C'
        while(retry--)
        {
            if ((receivebyte(&receivedC[0], 10) == S_SUCCESS))
            {
                if (receivedC[0] == 'C')
                {
                    break;
                }
            }
            delays_counter(DELAYS_COUNTER_10MS);
        }
        if (retry == 0)
        {
            return S_TIMEOUT;
        }
    }//if (ymodem_info->control.initialpacket_sent == 0)...

    //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    // Content packets
    //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    bptr = blockdata;
    blength = blockdatalength;

    // Resend packet if NAK  for a count of 10 else end of communication
    while(blength)
    {
        // Prepare next packet
        ymodem_preparepacket(bptr,&packet_data[0],
                             ymodem_info->blocknumber,blength);
        ackReceived = 0;
        receivedC[0]= 0;
        errors = 0;
        do
        {
            // Send next packet
            if (blength >= PACKET_1K_SIZE)
            {
                pktSize = PACKET_1K_SIZE;
                
            }
            else
            {
                pktSize = PACKET_SIZE;
            }
            ymodem_sendpacket(packet_data, pktSize + PACKET_HEADER);
            // Send CRC or checksum
            if (ymodem_info->control.use_crc16)
            {
                tempCRC = ymodem_calc_crc16(&packet_data[3], pktSize);
                sendbyte(tempCRC >> 8);
                sendbyte(tempCRC & 0xFF);
            }
            else
            {
                tempCheckSum = ymodem_calc_checksum (&packet_data[3], pktSize);
                sendbyte(tempCheckSum);
            }

            // Wait for Ack
            if ((receivebyte(&receivedC[0], 4*100000) == S_SUCCESS)  && (receivedC[0] == ACK))
            {
                ackReceived = 1;
                if (blength > pktSize)
                {
                    bptr += pktSize;
                    blength -= pktSize;
                    if (ymodem_info->blocknumber == (USER_FLASH_SIZE/1024))    //TODOQ: check it sooner
                    {
                        return S_OUTOFRANGE;
                    }
                    else
                    {
                        ymodem_info->blocknumber++;
                    }
                }
                else
                {
                    bptr += pktSize;
                    blength = 0;
                    ymodem_info->blocknumber++;
                }
                ymodem_info->bytecount += pktSize;
            }
            else
            {
                errors++;
            }
        }while(!ackReceived && (errors < 0x0A));
        // Resend packet if NAK  for a count of 10 else end of communication

        if (errors >= 0x0A)
        {
            return S_FAIL;
        }
    }

    //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    // Termination packet
    //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    if (ymodem_info->bytecount >= ymodem_info->filesize)
    {
        ackReceived = 0;
        receivedC[0] = 0x00;
        errors = 0;
        do
        {
            // Send EOT;
            sendbyte(EOT);
            // Wait for Ack
            if ((receivebyte(&receivedC[0], 4*10000) == S_SUCCESS) && receivedC[0] == ACK)
            {
                ackReceived = 1;
            }
            else
            {
                errors++;
            }
        }while(!ackReceived && (errors < 0x0A));
        
        if (errors >= 0x0A)
        {
            return S_FAIL;
        }

        ackReceived = 0;
        receivedC[0] = 0x00;
        errors = 0;
        
        packet_data[0] = SOH;
        packet_data[1] = 0;
        packet_data [2] = 0xFF;
        
        for (i = PACKET_HEADER; i < (PACKET_SIZE + PACKET_HEADER); i++)
        {
            packet_data [i] = 0x00;
        }
        
        do
        {
            // Send Packet
            ymodem_sendpacket(packet_data, PACKET_SIZE + PACKET_HEADER);
            
            //TODOQ: Send CRC or Check Sum based on CRC16_F
            tempCRC = ymodem_calc_crc16(&packet_data[3], PACKET_SIZE);
            sendbyte(tempCRC >> 8);
            sendbyte(tempCRC & 0xFF);

            // Wait for Ack and 'C'
            if (receivebyte(&receivedC[0], 10000) == S_SUCCESS)
            {
                if (receivedC[0] == ACK)
                { 
                    // Packet transferred correctly
                    ackReceived = 1;
                }
            }
            else
            {
                errors++;
            }
        }while(!ackReceived && (errors < 0x0A));
        
        // Resend packet if NAK  for a count of 10 else end of communication
        if (errors >= 0x0A)
        {
            return S_FAIL;
        }
    }//if (ymodem_info->bytecount >= ymodem_info->filesize)...

    return S_SUCCESS;
}
