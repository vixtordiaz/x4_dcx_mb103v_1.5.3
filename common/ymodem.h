/**
  **************** (C) COPYRIGHT 2011 SCT Performance, LLC *********************
  * File Name          : ymodem.h
  * Device / System    : SCT products using the "CommonCode" platform
  * Author             : Quyen Leba
  *
  * Version            : 1 
  * Date               : 05/02/2011
  * Description        : 
  *                    : 
  *
  *
  * History            : 05/02/2011 M. Davis
  *                    :   Add SCT Copyright header,
  *
  ******************************************************************************
  */

#ifndef __YMODEM_H
#define __YMODEM_H

#include <arch/gentype.h>

#define PACKET_SEQNO_INDEX      (1)
#define PACKET_SEQNO_COMP_INDEX (2)

#define PACKET_HEADER           (3)
#define PACKET_TRAILER          (2)
#define PACKET_OVERHEAD         (PACKET_HEADER + PACKET_TRAILER)
#define PACKET_SIZE             (128)
#define PACKET_1K_SIZE          (1024)

#define FILE_NAME_LENGTH        (256)
#define FILE_SIZE_LENGTH        (16)

#define SOH                     (0x01)  // start of 128-byte data packet
#define STX                     (0x02)  // start of 1024-byte data packet
#define EOT                     (0x04)  // end of transmission
#define ACK                     (0x06)  // acknowledge
#define NAK                     (0x15)  // negative acknowledge
#define CA                      (0x18)  // two of these in succession aborts transfer
#define CRC16                   (0x43)  // 'C' == 0x43, request 16-bit CRC

#define ABORT1                  (0x41)  // 'A' == 0x41, abort by user
#define ABORT2                  (0x61)  // 'a' == 0x61, abort by user

#define NAK_TIMEOUT             (0x100000)
#define MAX_ERRORS              (5)

typedef void (*pFuncYmodemSendByte)(u8);
typedef u8 (*pFuncYmodemRecvByte)(u8*);

u8 ymodem_init(pFuncYmodemSendByte sendfunc, pFuncYmodemRecvByte recievefunc);
void ymodem_deinit();
u8 ymodem_set_fileinfo(const u8 *filename, u32 filesize);
u8 ymodem_send_datablock(u8 *blockdata, u32 blockdatalength);

#endif  //__YMODEM_H
