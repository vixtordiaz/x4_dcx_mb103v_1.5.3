/**
  **************** (C) COPYRIGHT 2011 SCT Performance, LLC *********************
  * File Name          : cmdif_func_vehiclefunction.h
  * Device / System    : SCT products using the "CommonCode" platform
  * Author             : Quyen Leba
  *
  * Version            : 1 
  * Date               : 05/02/2011
  * Description        : 
  *                    : 
  *
  *
  * History            : 05/02/2011 M. Davis
  *                    :   Add SCT Copyright header,
  *
  ******************************************************************************
  */

#ifndef __CMDIF_FUNC_VEHICLEFUNCTION_H
#define __CMDIF_FUNC_VEHICLEFUNCTION_H

#include <arch/gentype.h>

u8 cmdif_func_vehiclefunction(CMDIF_COMMAND cmd);

#endif    //__CMDIF_FUNC_VEHICLEFUNCTION_H
