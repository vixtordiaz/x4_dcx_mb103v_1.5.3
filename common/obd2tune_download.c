/**
  **************** (C) COPYRIGHT 2011 SCT Performance, LLC *********************
  * File Name          : obd2tune_download.c
  * Device / System    : SCT products using the "CommonCode" platform
  * Author             : Quyen Leba
  *
  * Version            : 1 
  * Date               : 05/02/2011
  * Description        : 
  *                    : 
  *
  *
  * History            : 05/02/2011 M. Davis
  *                    :   Add SCT Copyright header,
  *
  ******************************************************************************
  */

//------------------------------------------------------------------------------
// Log error point: 0x42xx -> 0x43xx
//------------------------------------------------------------------------------

#include <string.h>
#include <board/delays.h>
#include <board/power.h>
#include <board/indicator.h>
#include <common/statuscode.h>
#include <common/settings.h>
#include <common/obd2.h>
#include <common/obd2tune.h>
#include <common/genmanuf_overload.h>
#include <common/cmdif.h>
#include <common/filestock.h>
#include <common/filestock_option.h>
#include <common/obd2comm.h>
#include <common/veh_defs.h>
#include <common/log.h>
#include <common/housekeeping.h>
#include <common/debug/debug_output.h>
#include "obd2tune_download.h"


#define USE_DUMMY_DOWNLOAD              0

extern obd2_info gObd2info;                         /* from obd2.c */
extern flasher_info *flasherinfo;                   /* from obd2tune.c */
extern cmdif_datainfo cmdif_datainfo_responsedata;  /* from cmdif.c */
#define TUNE_DOWNLOADING_STATUS(var)    flasherinfo->tune_programming_status.##var


FileStock_Info *forcestockdata = NULL;

u8 obd2tune_forcefullstock_backup(u8 ecm_index, u8 flashtype);
u8 obd2tune_forcefullstock_setup(u8 ecm_index);
u8 obd2tune_forcefullstock_complete(u8 ecm_index);
u8 clear_kam(u16 ecm_type);
u8 obd2tune_comfirm_download_success(u16 veh_type);

//------------------------------------------------------------------------------
// Clear KAM (Keep Alive Memory)
// Inputs:  u16 ecm_type
//
// Return:  u8  status
// Engineer: Patrick Downs
//------------------------------------------------------------------------------
u8 clear_kam(u16 ecm_type)
{
    VehicleCommType vehiclecommtype;
    VehicleCommLevel commlevel;
    u32 ecm_id;
    
    u8 status;
    
    vehiclecommtype = ECM_GetCommType(ecm_type);
    commlevel = ECM_GetCommLevel(ecm_type);
    ecm_id = ECM_GetEcmId(ecm_type);
    status = S_FAIL;
    
    switch(vehiclecommtype)
    {
    case CommType_CAN:
        switch(obd2_get_oemtype())
        {
#ifdef __DCX_MANUF__        
        case OemType_DCX:
            status = obd2can_dcx_clear_kam(ecm_id,commlevel);
            break; 
#endif
#ifdef __FORD_MANUF__
        case OemType_FORD:
            status = obd2can_ford_clear_kam(ecm_id,commlevel);
            break;
#endif
#ifdef __GM_MANUF__
        case OemType_GM:
            status = obd2can_gm_clear_kam(ecm_id,commlevel);
            break; 
#endif
        default:
            break; 
        }
        break;
    case CommType_VPW:
        // TODO P: Add support    
        break;
    case CommType_SCP:
    case CommType_SCP32:
        // TODO P: Add support    
        break;
    default:
        status = S_COMMTYPE;
        log_push_error_point(0x422F);
        break;
    }
    return status;   
}

//------------------------------------------------------------------------------
// Erase ECM
// Inputs:  u16 ecm_type
//          bool is_cal_only
// Return:  u8  status
// Engineer: Quyen Leba
//------------------------------------------------------------------------------
u8 erase_ecm(u16 ecm_type, bool is_cal_only)
{
    VehicleCommType vehiclecommtype;
    u8 status;

    vehiclecommtype = ECM_GetCommType(ecm_type);
    
    switch (obd2_get_oemtype())
    {
#ifdef __DCX_MANUF__        
    case OemType_DCX:
        switch(vehiclecommtype)
        {
        case CommType_CAN:
            status = obd2can_dcx_erase_ecm(ecm_type,is_cal_only);
            break; 
        default:
            status = S_COMMTYPE; 
            break;
        }
        break;
#endif
#ifdef __FORD_MANUF__
    case OemType_FORD:
        switch(vehiclecommtype)
        {
        case CommType_CAN:
            status = obd2can_ford_erase_ecm(ecm_type,is_cal_only);
            break; 
        case CommType_SCP:
        case CommType_SCP32:
            status = obd2scp_erase_ecm(ecm_type,is_cal_only);
            break;
        default:
            status = S_COMMTYPE; 
            break;
        }
        break;
#endif
#ifdef __GM_MANUF__
    case OemType_GM:
        switch(vehiclecommtype)
        {
        case CommType_CAN:
            status = obd2can_gm_erase_ecm(ecm_type,is_cal_only);
            break; 
        case CommType_VPW:
            status = obd2vpw_erase_ecm(ecm_type,is_cal_only);
            break;
        default:
            status = S_COMMTYPE; 
            break;
        }
        break;
#endif
    default:
        status = S_FAIL;
        break;
    }  
    
    if(status != S_SUCCESS)
    {
        log_push_error_point(0x4200); 
    }
    
    return status;
}

//------------------------------------------------------------------------------
//Fingerprint Interlock
//Input:    u32 ecm_type
//          bool logical block index
//Return:   u8 status
//Engineer: Ricardo Cigarroa
//------------------------------------------------------------------------------
u8 fingerprint_interlock(u16 ecm_type, bool logical_block_index)
{    
    u8 status;  
    VehicleCommType vehiclecommtype;
    
    if(SETTINGS_IsDemoMode())
    {
        return S_SUCCESS; 
    }
    
    vehiclecommtype = ECM_GetCommType(ecm_type);
    
    switch (obd2_get_oemtype())
    {
#ifdef __DCX_MANUF__        
    case OemType_DCX:
        switch(vehiclecommtype)
        {
        case CommType_CAN:
            status = obd2can_dcx_fingerprint_interlock(ecm_type, logical_block_index);
            break; 
        default:
            status = S_COMMTYPE; 
            break;
        }
        break;
#endif
#ifdef __FORD_MANUF__
    case OemType_FORD:
        if(vehiclecommtype != CommType_Unknown)
        {
            status = S_SUCCESS; 
        }
        else
        {
            status = S_COMMTYPE; 
        }
        break;
#endif
#ifdef __GM_MANUF__
    case OemType_GM:
        if(vehiclecommtype != CommType_Unknown)
        {
            status = S_SUCCESS; 
        }
        else
        {
            status = S_COMMTYPE; 
        }
        break;
#endif
    default:
        status = S_FAIL;
        break;
    }  
    
    if(status != S_SUCCESS)
    {
        log_push_error_point(0x4252); 
    }
    
    return status;
}

//------------------------------------------------------------------------------
// Download a memory block to an ecm
// Inputs:  u16 ecm_type
//          u8  block_index
//          BlockType block_type
//          u32 ecm_starting_offset (starting point of this ecm in stock/flash
//              file)
//          VehicleCommType vehiclecommtype
// Return:  u8  status
// Engineer: Quyen Leba
//------------------------------------------------------------------------------
u8 download_ecm_memoryblock(u16 ecm_type, u8 block_index,
                            BlockType block_type, u32 ecm_starting_offset,
                            VehicleCommType vehiclecommtype, bool is_variant_id)
{
    VehicleCommLevel vehiclecommlevel;
    MemoryAddressType address_type;
    RequestLengthType request_type;
    u32 memblock_address;
    u32 memblock_length;
    u8  memblock_index;
    u32 tunebyteoffset;
    u32 current_address;
    u32 totalbyte_downloaded;
    u32 bytetodownload;         //to write to processor each write cycle
    s32 byteingroupread;        //to read from filesystem at once
    u32 positioningroupread;    //current position
    u8  *writebuffer;
    u32 writebufferlength;
    u32 percentage;
    u8  block_tracking;
    u16 maxdownloadblocklength;
    bool is_setup_done;
    u8 i;
    u8 status;
    u32 bytecount_sincetesterpresent;
    u32 testerpresentthreshold;

    writebuffer = __malloc(MAX_DOWNLOAD_BUFFER_LENGTH);
    if (writebuffer == NULL)
    {
        log_push_error_point(0x423A);
        status = S_MALLOC;
        goto download_ecm_memoryblock_done;
    }

    if (block_index > MAX_ECM_MEMORY_BLOCKS)
    {
        log_push_error_point(0x423B);
        status = S_INPUT;
        goto download_ecm_memoryblock_done;
    }

    is_setup_done = FALSE;
    vehiclecommlevel = ECM_GetCommLevel(ecm_type);
    
    testerpresentthreshold = ECM_GetTesterPresentFrequency(ecm_type) *
        ECM_GetDownloadBlockSize(ecm_type);
    
    //TODOQ: make this block handled specifically by vehicle manuf
    status = S_FAIL;
    switch(block_type)
    {
    case OS_Block_Read:
        //Note: this case should not be used for download
        status = S_SUCCESS;
        memblock_index = block_index;
        memblock_address = ECM_GetOSReadBlockAddr(ecm_type,memblock_index);
        memblock_length = ECM_GetOSReadBlockSize(ecm_type,memblock_index);
        status = S_FAIL;
        break;
    case OS_Block:
        status = S_SUCCESS;
        memblock_index = block_index;
        memblock_address = ECM_GetOSBlockAddr(ecm_type,memblock_index);
        memblock_length = ECM_GetOSBlockSize(ecm_type,memblock_index);
        if (ECM_IsOSBlockSkipDownload(ecm_type,memblock_index))
        {
            status = S_SUCCESS;
            goto download_ecm_memoryblock_done;
        }
        break;
    case Cal_Block:
        status = S_SUCCESS;
        memblock_index = block_index;
        memblock_address = ECM_GetCalBlockAddr(ecm_type,memblock_index);
        memblock_length = ECM_GetCalBlockSize(ecm_type,memblock_index);
        if (ECM_IsCalBlockSkipDownload(ecm_type,memblock_index))
        {
            status = S_SUCCESS;
            goto download_ecm_memoryblock_done;
        }
        break;
    default:
        log_push_error_point(0x4202);
        status = S_INPUT;
        goto download_ecm_memoryblock_done;
    }
    
    if (status != S_SUCCESS)
    {
        log_push_error_point(0x4230);
        goto download_ecm_memoryblock_done;
    }
    
    if (memblock_length == 0)
    {
        // termination block
        status = S_TERMINATED;
        goto download_ecm_memoryblock_done;
    }
    
    //###############################################
    // Set starting point of tune file for this block
    // Reference addresses/sizes from OS_Block_Read
    //###############################################
    status = S_BADCONTENT;
    tunebyteoffset = 0;
    if (ECM_GetDataOffset(ecm_type) > 0)
    {
        tunebyteoffset += ECM_GetDataOffset(ecm_type);
    }

    for(i=0;i<MAX_ECM_MEMORY_BLOCKS;i++)
    {
        u32 __block_address;
        u32 __block_length;
        u16 __block_flagtype;
        u32 __block_flagdata;

        if (ECM_IsUploadCalOnly(ecm_type))
        {
            __block_address = ECM_GetCalBlockAddr(ecm_type,i);
            __block_length = ECM_GetCalBlockSize(ecm_type,i);
            __block_length = ECM_GetCalBlockFlagType(ecm_type,i);
            __block_length = ECM_GetCalBlockFlagData(ecm_type,i);
        }
        else
        {
            __block_address = ECM_GetOSReadBlockAddr(ecm_type,i);
            __block_length = ECM_GetOSReadBlockSize(ecm_type,i);
            __block_flagtype = ECM_GetOSReadBlockFlagType(ecm_type,i);
            __block_flagdata = ECM_GetOSReadBlockFlagData(ecm_type,i);
        }
        
        if (__block_flagtype == MEMBLOCK_FLAGS_SIMPLE_FF_APPEND ||
            __block_flagtype == MEMBLOCK_FLAGS_SIMPLE_00_APPEND)
        {
            __block_length += __block_flagdata;
        }
        else if (__block_flagtype == MEMBLOCK_FLAGS_APPEND_FILE_BY_INDEX)
        {
            log_push_error_point(0x4203);
            status = S_NOTSUPPORT;
            break;
        }
        
        if ((memblock_address >= __block_address) &&
            ((memblock_address + memblock_length) <= 
             (__block_address + __block_length)))
        {
            //the memory block about to download must be enclosed in one of
            //OS block
            tunebyteoffset += (memblock_address - __block_address);
            status = S_SUCCESS;
            break;
        }
        else
        {
            tunebyteoffset += __block_length;
        }
    }
    if (status != S_SUCCESS)
    {
        //should not happen if ecm_defs defined correctly
        goto download_ecm_memoryblock_done;
    }
    status = filestock_set_position(ecm_starting_offset + tunebyteoffset);
    if (status != S_SUCCESS)
    {
        goto download_ecm_memoryblock_done;
    } 
    
    if (ECM_IsDownloadRequest4B(ecm_type))
    {
        request_type = Request_4_Byte;
    }
    else if (ECM_IsDownloadRequest3B(ecm_type))
    {
        request_type = Request_3_Byte;
    }
    else
    {
        log_push_error_point(0x4231);
        status = S_BADCONTENT;
        goto download_ecm_memoryblock_done;
    }
    
    if (ECM_IsDownloadAddr4B(ecm_type))
    {
        address_type = Address_32_Bits;
    }
    else if (ECM_IsDownloadAddr3B(ecm_type))
    {
        address_type = Address_24_Bits;
    }
    else
    {
        log_push_error_point(0x4204);
        status = S_BADCONTENT;
        goto download_ecm_memoryblock_done;
    }
    
    current_address = memblock_address;
    if (ECM_IsDownloadBlockBackward(ecm_type))
    {
        //TODOQ: handle 'odd' block size
        //Right now, assume memblock_length is multiple of blocksize
        current_address = memblock_address + memblock_length;
        current_address -= ECM_GetDownloadBlockSize(ecm_type);
        filestock_forward(memblock_length);
    }

    byteingroupread = 0;
    positioningroupread = 0;
    totalbyte_downloaded = 0;
    block_tracking = 1;
    while(totalbyte_downloaded < memblock_length)
    {
        //keep it simple, byteingroupread = multiple of bytetodownload
        //group read is to reduce filesystem read by reading multiple of bytetodownload
        if (byteingroupread == 0 || positioningroupread >= byteingroupread)
        {
            positioningroupread = 0;
            bytetodownload = memblock_length - totalbyte_downloaded;
            if (bytetodownload > ECM_GetDownloadBlockSize(ecm_type))
            {
                if(bytetodownload > MAX_DOWNLOAD_BUFFER_LENGTH)
                {
                    bytetodownload = ECM_GetDownloadBlockSize(ecm_type);
                    byteingroupread = (MAX_DOWNLOAD_BUFFER_LENGTH/bytetodownload)*bytetodownload;
                }
                else
                {
                    byteingroupread = (bytetodownload/ECM_GetDownloadBlockSize(ecm_type))*ECM_GetDownloadBlockSize(ecm_type);
                    bytetodownload = ECM_GetDownloadBlockSize(ecm_type);
                }
            }
            else
            {
                //last chunk
                byteingroupread = bytetodownload;
            }
            if (bytetodownload > MAX_DOWNLOAD_BUFFER_LENGTH)
            {
                //should not happen if we define ecm_defs correctly, but check
                //it anyway.
                bytetodownload = MAX_DOWNLOAD_BUFFER_LENGTH;
                byteingroupread = bytetodownload;
            }

            if (byteingroupread == 0)
            {
                //should never happen; just to catch bad code
                status = S_ERROR;
                log_push_error_point(0x4237);
                goto download_ecm_memoryblock_done;
            }
        }
        else if (byteingroupread < 0)
        {
            //should never happen; just to catch bad code
            status = S_ERROR;
            log_push_error_point(0x4238);
            goto download_ecm_memoryblock_done;
        }
        else if (byteingroupread < bytetodownload)
        {
            //should never happen; just to catch bad code
            status = S_ERROR;
            log_push_error_point(0x4239);
            goto download_ecm_memoryblock_done;
        }

        //###############################################
        // Get data from tune file
        //###############################################
        if (ECM_IsDownloadBlockBackward(ecm_type))
        {
            //don't do group read if download in backward order (just to keep it simple)
            byteingroupread = bytetodownload;
            filestock_backward(bytetodownload);
        }
        if (positioningroupread == 0)
        {
            status = filestock_read(writebuffer, byteingroupread,
                                    &writebufferlength);
            if (status != S_SUCCESS)
            {
                log_push_error_point(0x4205);
                goto download_ecm_memoryblock_done;
            }
            else if (byteingroupread != writebufferlength)
            {
                log_push_error_point(0x4206);
                goto download_ecm_memoryblock_done;
            }
        }

        //----------------------------------------------------------------------
        //----------------------------------------------------------------------
        if (vehiclecommtype == CommType_CAN)
        {
            if (SETTINGS_IsDemoMode())
            {
                delays(demo_control.download_dummy_delay,'m');
                status = S_SUCCESS;
            }
            else
            {
                if (is_setup_done == FALSE &&
                    ECM_IsDownloadSkipRequestCmd34(ecm_type) == FALSE)
                {
                     if(ECM_IsUseVariantId(ecm_type))
                     {
                         if(is_variant_id)
                         {
                             if(block_index == 1)
                             {
                                 //fingerprint interlock for second logical block 
                                 status = fingerprint_interlock(ecm_type, TRUE);
                                 if(status != S_SUCCESS)
                                 {
                                     log_push_error_point(0x4255);
                                     status = S_FAIL; 
                                     goto download_ecm_memoryblock_done;
                                 }
                                 //erase second logical block 
                                 status = erase_ecm(ecm_type, TRUE);
                                 if(status != S_SUCCESS)
                                 {
                                     log_push_error_point(0x4256);
                                     status = S_FAIL; 
                                     goto download_ecm_memoryblock_done;                        
                                 }
                             }
                         }      
                     }
                    //###############################################
                    // Send $34 with block length
                    // "34 00 <3/4:length>"
                    //###############################################
                    status = obd2can_request_download
                        (ECM_GetEcmId(ecm_type),0x00,
                         memblock_address,memblock_length,
                         request_type,vehiclecommlevel,&maxdownloadblocklength);
                    if (status != S_SUCCESS)
                    {
                        log_push_error_point(0x4207);
                        goto download_ecm_memoryblock_done;
                    }
                    
                    //NOTE: for simplicity, we use size defined in ecm_defs and
                    //compare it here (if possible)
                    if (maxdownloadblocklength != INVALID_MEMBLOCK_SIZE &&
                        maxdownloadblocklength < ECM_GetDownloadBlockSize(ecm_type))
                    {
                        log_push_error_point(0x4208);
                        status = S_BADCONTENT;
                        goto download_ecm_memoryblock_done;
                    }
                    
                    //###############################################
                    // Send #36 with download execute and address
                    // some ECMs require a "36 80 <3/4:address>"
                    //###############################################
                    if (ECM_IsUseDownloadExecute(ecm_type))
                    {
                        status = obd2can_transfer_data_download
                            (ECM_GetEcmId(ecm_type),
                             (Obd2can_SubServiceNumber)0x80,0,
                             memblock_address,address_type,
                             NULL,0,vehiclecommlevel,TRUE);
                        if (status != S_SUCCESS)
                        {
                            if(flasherinfo->utilityflags & UTILITY_FLAGS_LAST_36_FAIL_OK &&
                               TUNE_DOWNLOADING_STATUS(totalbytecount) - TUNE_DOWNLOADING_STATUS(currentbytecount) == bytetodownload)
                            {
                                // Because of modified bootloader, this is OK.
                                status = S_SUCCESS;
                                goto download_ecm_memoryblock_done;
                            }
                            else
                            {
                                log_push_error_point(0x4209);
                                goto download_ecm_memoryblock_done;
                            }
                        }
                    }
                    is_setup_done = TRUE;
                }//if (is_setup_done ...
                
                //###############################################
                // Send $36 with address and data
                // "36 00 <3/4:address> <n:data>
                //###############################################
                status = obd2can_transfer_data_download
                    (ECM_GetEcmId(ecm_type),
                     (Obd2can_SubServiceNumber)0x00,block_tracking,
                     current_address,address_type,
                     &writebuffer[positioningroupread],bytetodownload,vehiclecommlevel,TRUE);
                if (status != S_SUCCESS)
                {                    
                    if(flasherinfo->utilityflags & UTILITY_FLAGS_LAST_36_FAIL_OK &&
                       TUNE_DOWNLOADING_STATUS(totalbytecount) - TUNE_DOWNLOADING_STATUS(currentbytecount) == bytetodownload)
                    {
                        // Because of modified bootloader, this is OK.
                        // Download is done, don't send $37
                        status = S_SUCCESS;
                        goto download_ecm_memoryblock_done;
                    }
                    else
                    {
                        log_push_error_point(0x420A);
                        goto download_ecm_memoryblock_done;
                    }
                }
                block_tracking++;
                
                //###############################################
                // Send $37 if required, and all data of this
                // block is sent
                //###############################################
                if ((totalbyte_downloaded + bytetodownload) >= memblock_length)
                {
                    is_setup_done = FALSE;
                    if (ECM_IsDownloadRequireCmd37(ecm_type))
                    {
                        if(ECM_IsSkipLast37Command(ecm_type))
                        {
                            if(ECM_GetOSReadBlockSize(ecm_type,memblock_index+1) != 0)
                            {
                                status = obd2can_request_transfer_exit
                                    (ECM_GetEcmId(ecm_type),vehiclecommlevel);
                                if (status != S_SUCCESS)
                                {
                                    log_push_error_point(0x4236);
                                    goto download_ecm_memoryblock_done;
                                }
                            }
                        }
                        else
                        {
                            status = obd2can_request_transfer_exit
                                (ECM_GetEcmId(ecm_type),vehiclecommlevel);
                            if (status != S_SUCCESS)
                            {
                                log_push_error_point(0x420B);
                                goto download_ecm_memoryblock_done;
                            }                            
                        }
                    }//if (ECM_IsDownloadRequireCmd37...
                }//if ((totalbyte_downloaded + bytetodownload)...
            }
        }//if (vehiclecommtype == CommType_CAN)...
        //----------------------------------------------------------------------
        //----------------------------------------------------------------------
        else if (vehiclecommtype == CommType_VPW)
        {
            if (SETTINGS_IsDemoMode())
            {
                delays(demo_control.download_dummy_delay,'m');
                status = S_SUCCESS;
            }
            else
            {
                status = obd2vpw_helper_download_block(0xAA,current_address,
                                                       bytetodownload,
                                                       &writebuffer[positioningroupread]);
//            status = obd2vpw_transfer_data_download(0xAA,   //subservice
//                                                    &writebuffer[positioningroupread],
//                                                    current_address,
//                                                    bytetodownload);
                if (status != S_SUCCESS)
                {
                    log_push_error_point(0x420C);
                    goto download_ecm_memoryblock_done;
                }
            }
        }
        //----------------------------------------------------------------------
        //----------------------------------------------------------------------
        else if (vehiclecommtype == CommType_SCP || vehiclecommtype == CommType_SCP32)
        {
            if (SETTINGS_IsDemoMode())
            {
                delays(demo_control.download_dummy_delay,'m');
                status = S_SUCCESS;
            }
            else
            {
                u32 retry;
                
                retry = 3;
            scp_download_block_retry:
                status = obd2scp_request_download(current_address,bytetodownload,
                                                  address_type);
                if (status != S_SUCCESS)
                {
                    log_push_error_point(0x420D);
                    if (retry)
                    {
                        retry--;
                        goto scp_download_block_retry;
                    }
                    goto download_ecm_memoryblock_done;
                }
                status = obd2scp_transfer_data_download(&writebuffer[positioningroupread],bytetodownload);
                if (status != S_SUCCESS)
                {
                    log_push_error_point(0x420E);
                    goto download_ecm_memoryblock_done;
                }
                status = obd2scp_request_transfer_exit(address_type,
                                                       TransferExitFromDownload);
                if (status != S_SUCCESS)
                {
                    log_push_error_point(0x420F);
                    if (retry)
                    {
                        retry--;
                        goto scp_download_block_retry;
                    }
                    goto download_ecm_memoryblock_done;
                }
                status = obd2scp_start_diagnostic_routine_bytestnumber(0xA2,NULL,0);
                if (status != S_SUCCESS)
                {
                    log_push_error_point(0x4210);
                    if (retry)
                    {
                        retry--;
                        goto scp_download_block_retry;
                    }
                    goto download_ecm_memoryblock_done;
                }
                status = obd2scp_stop_diagnostic_routine_bytestnumber(0xA2);
                if (status != S_SUCCESS)
                {
                    log_push_error_point(0x4211);
                    if (retry)
                    {
                        retry--;
                        goto scp_download_block_retry;
                    }
                    goto download_ecm_memoryblock_done;
                }
            }
        }
        //----------------------------------------------------------------------
        //----------------------------------------------------------------------
        else
        {
            log_push_error_point(0x4212);
            status = S_COMMTYPE;
        }
        
        if (status == S_SUCCESS)
        {
            //successfully downloaded a data chunk
            if (ECM_IsDownloadBlockBackward(ecm_type))
            {
                current_address -= bytetodownload;
                filestock_backward(bytetodownload);
            }
            else
            {
                current_address += bytetodownload;
                positioningroupread += bytetodownload;
            }
            
            totalbyte_downloaded += bytetodownload;
            bytecount_sincetesterpresent += bytetodownload;
            
            TUNE_DOWNLOADING_STATUS(currentbytecount) += bytetodownload;
            percentage = TUNE_DOWNLOADING_STATUS(currentbytecount) * 100 / 
                TUNE_DOWNLOADING_STATUS(totalbytecount);
            if ((percentage == 0) ||
                (percentage != TUNE_DOWNLOADING_STATUS(percentage)))
            {
                u8 reportmsg[32];
                char message[31];
                
                //reportmsg: [1:percentage][n:msg][1:NULL]
                reportmsg[0] = (u8)TUNE_DOWNLOADING_STATUS(percentage);
                memset(message,0,sizeof(message));
                strcat(message,"WRITING ");
                switch (TUNE_DOWNLOADING_STATUS(ecm_index))
                {
                case 0:
                    strcat(message, "ECM ");
                    break;
                case 1:
                    strcat(message, "TCM ");
                    break;
                }
                if(flasherinfo->flashtype == CMDIF_ACT_STOCK_FLASHER)
                {
                    strcat(message, "STOCK ");
                }
                else
                {
                    strcat(message, "TUNE ");
                }
                strcat(message, "DATA");
                memcpy(&reportmsg[1], message, sizeof(reportmsg)-1);
                TUNE_DOWNLOADING_STATUS(percentage) = percentage;
                cmdif_internalresponse_ack_nowait
                    (CMDIF_CMD_DO_DOWNLOAD, CMDIF_ACK_PROGRESSBAR,
                     reportmsg,strlen((char*)&reportmsg[1])+2);
                
                debug_reportprogress(percentage, "Downloading");
            }
        }
        else
        {
            break;
        }
        
        if ((ECM_GetTesterPresentFrequency(ecm_type) != 0) &&
            (bytecount_sincetesterpresent > testerpresentthreshold))
        {
            bytecount_sincetesterpresent = 0;
            obd2tune_testerpresent();
            if (ECM_IsBlockDownloadDisableComm(ecm_type))
            {
                obd2tune_disableallnormalcomm(vehiclecommtype);
            }
        }        
    }//while(totalbyte_downloaded < ...
    
    if (status == S_SUCCESS &&
        ECM_IsDownloadBlockBackward(ecm_type))
    {
        //done with this memblock, set file position to next one
        filestock_forward(memblock_length);
    }
    
download_ecm_memoryblock_done:
    if (writebuffer)
    {
        __free(writebuffer);
    }
    return status;
}

//------------------------------------------------------------------------------
// Download all memory blocks defined for an ecm
// Inputs:  u16 ecm_type
//          VehicleCommType vehiclecommtype
//          bool isAllUnlocked (TRUE: ecm already unlock)
// Return:  u8  status
// Engineer: Quyen Leba
//------------------------------------------------------------------------------
u8 download_ecm(u16 ecm_type, VehicleCommType vehiclecommtype,
                VehicleCommLevel vehiclecommlevel, bool isAllUnlocked)
{
    BlockType block_type;
    u32 cal_only_size;
    u32 all_size;
    u32 ecm_starting_offset;    //starting point of this ecm in stock/flash file
    u8  order;
    u8  order_index;
    u8  order_current;
    u8  status;
    bool isutilityfileused;
    bool is_cal_only;
    bool is_variant_id = FALSE;
    
    isutilityfileused = FALSE;
    is_cal_only = TRUE;
    
    if (ECM_IsCoolDownTimeShort(ecm_type))
    {
        obd2_setcooldowntime(300);
    }
    else if (ECM_IsCoolDownTimeShort_1(ecm_type))
    {
        obd2_setcooldowntime(200);
    }
    else if (ECM_IsCoolDownTimeShort_2(ecm_type))
    {
        obd2_setcooldowntime(100);
    }
    else if (ECM_IsCoolDownTimeShort_3(ecm_type))
    {
        obd2_setcooldowntime(50);
    }
    else if (ECM_IsCoolDownTimeMedium(ecm_type))
    {
        obd2_setcooldowntime(800);
    }
    else if (ECM_IsCoolDownTimeMedium_1(ecm_type))
    {
        obd2_setcooldowntime(500);
    }
    else if (ECM_IsCoolDownTimeLong(ecm_type))
    {
        obd2_setcooldowntime(2000);
    }
    else
    {
        obd2_setcooldowntime(0);
    }
    
    //each ECM download has each own progress bar (i.e. start from 0->100 each)
    TUNE_DOWNLOADING_STATUS(percentage) = 0;
    TUNE_DOWNLOADING_STATUS(currentbytecount) = 0;
    obd2tune_getecmdownloadsize(ecm_type,&cal_only_size,&all_size);
    
    // This checks to see if the flasherinfo->isfullreflash flags have already been 
    // setup, so they don't need to be setup again here.

    if (flasherinfo->isfullreflash_already_set)
    {
        if (flasherinfo->isfullreflash[0] == TRUE) // FULL FLASH
        {
            is_cal_only = FALSE;
            block_type = OS_Block;
            TUNE_DOWNLOADING_STATUS(totalbytecount) = all_size;
        }
        else
        {
            is_cal_only = TRUE;
            block_type = Cal_Block;
            TUNE_DOWNLOADING_STATUS(totalbytecount) = cal_only_size;
        }
    }
    else
    {
        if (ECM_IsForceDownloadCalOnly(ecm_type))
        {
            //should only use for debug
            is_cal_only = TRUE;
            block_type = Cal_Block;
            flasherinfo->isfullreflash[TUNE_DOWNLOADING_STATUS(ecm_index)] = FALSE;
            TUNE_DOWNLOADING_STATUS(totalbytecount) = cal_only_size;
        }
        else if (ECM_IsForceDownloadAll(ecm_type))
        {
            //should only use for debug
            is_cal_only = FALSE;
            block_type = OS_Block;
            flasherinfo->isfullreflash[TUNE_DOWNLOADING_STATUS(ecm_index)] = TRUE;
            TUNE_DOWNLOADING_STATUS(totalbytecount) = all_size;
        }
        else if (ECM_IsDownloadAll(ecm_type))
        {
            //download all allowed; check which is required
            if (flasherinfo->isfullreflash[TUNE_DOWNLOADING_STATUS(ecm_index)])
            {
                is_cal_only = FALSE;
                block_type = OS_Block;
                TUNE_DOWNLOADING_STATUS(totalbytecount) = all_size;
            }
            else
            {
                is_cal_only = TRUE;
                block_type = Cal_Block;
                TUNE_DOWNLOADING_STATUS(totalbytecount) = cal_only_size;
            }
        }
        else    //only cal download allowed here
        {
            if (flasherinfo->isfullreflash[TUNE_DOWNLOADING_STATUS(ecm_index)])
            {
                //full flash is not allowed
                log_push_error_point(0x422D);
                status = S_REJECT;
                cmdif_datainfo_responsedata.responsecode = CMDIF_ACK_FAILED;
                goto download_ecm_done;
            }
            else
            {
                is_cal_only = TRUE;
                block_type = Cal_Block;
                TUNE_DOWNLOADING_STATUS(totalbytecount) = cal_only_size;
            }
        }
    }
    
    if(ECM_IsUseVariantId(ecm_type))
    {
        //this is a simple check (TRUE or FALSE)
        obd2tune_check_variant_id(ecm_type,&is_variant_id,vehiclecommtype);
        if(is_variant_id == TRUE)
        {
            //Some processors require a full flash when first tuning. 
            //If tuned again without returning to stock, only a CAL download is 
            //required. 
/*            if(flasherinfo->flashtype == CMDIF_ACT_STOCK_FLASHER)
            {
                is_cal_only = FALSE; 
                block_type = OS_Block;
                TUNE_DOWNLOADING_STATUS(totalbytecount) = all_size;
            }
            else
            {
*/
                if(flasherinfo->isfullreflash[0] == TRUE)
                {
                    is_cal_only = FALSE; 
                    block_type = OS_Block;
                    TUNE_DOWNLOADING_STATUS(totalbytecount) = all_size;
 
                }
                else
                {
                    //at this point, processor is married. Do CAL flash. 
                    is_cal_only = TRUE;
                    block_type = Cal_Block;
                    TUNE_DOWNLOADING_STATUS(totalbytecount) = cal_only_size; 
                }
/*
                if((flasherinfo->uploadstock_required == FALSE)&&(flasherinfo->secondvehiclecodes[0] != 0x26))
                {
                    //at this point, processor is married. Do CAL flash. 
                    is_cal_only = TRUE;
                    block_type = Cal_Block;
                    TUNE_DOWNLOADING_STATUS(totalbytecount) = cal_only_size; 
                }
                else
                {
                    is_cal_only = FALSE; 
                    block_type = OS_Block;
                    TUNE_DOWNLOADING_STATUS(totalbytecount) = all_size;
                }
*/
//            }
        }
    }

    if (TUNE_DOWNLOADING_STATUS(totalbytecount) == 0)
    {
        log_push_error_point(0x4213);
        status = S_BADCONTENT;
        cmdif_datainfo_responsedata.responsecode = CMDIF_ACK_FAILED;
        goto download_ecm_done;
    }
    
    //TODOQ: use better ACK code
    cmdif_internalresponse_ack(CMDIF_CMD_DO_DOWNLOAD,
                               CMDIF_ACK_UNLOCK_ECU,NULL,0);
    status = S_SUCCESS;
    if (!SETTINGS_IsDemoMode())
    {
        status = obd2_setupbus_prior_download(ecm_type);
    }
    if (status != S_SUCCESS)
    {
        cmdif_datainfo_responsedata.responsecode = CMDIF_ACK_FAILED;
        log_push_error_point(0x4214);
        goto download_ecm_done;
    }
    
    //#########################################
    // Handle Keyoff Before Download
    //#########################################
    if (ECM_IsRequireKeyoffBeforeDownload(ecm_type))
    {
        u8 keycheckcount = 0;
        
        status = S_SUCCESS;
        if (!SETTINGS_IsDemoMode())
        {
            // Send request key off message
            cmdif_internalresponse_ack(CMDIF_CMD_DO_DOWNLOAD,
                                       CMDIF_ACK_KEY_OFF,NULL,0);
            do
            {
                status = obd2_checkkeyon_special(vehiclecommtype, FALSE);
                if (status == S_SUCCESS)    // Key is still on
                { 
                    // Timeout
                    if (keycheckcount++ > 100)
                    {                        
                        status = S_TIMEOUT;
                        break;
                    }
                }
                delays(500,'m');
            } while(status == S_SUCCESS);
            
            if (status != S_TIMEOUT)
            {
                // Send request key on message
                cmdif_internalresponse_ack(CMDIF_CMD_DO_DOWNLOAD,
                                           CMDIF_ACK_KEY_ON,NULL,0);                              
                status = obd2_wakeup(flasherinfo->vehicle_type,0xFE);
            }
            
        }
        if (status != S_SUCCESS)
        {
            cmdif_datainfo_responsedata.responsecode = CMDIF_ACK_FAILED;
            log_push_error_point(0x4243);
            goto download_ecm_done;
        }
    } 
    
    //#########################################
    // Handle Unlock, Speed, Utility File
    //#########################################
    if (isAllUnlocked == FALSE && flasherinfo->isunlockpriordownload == TRUE)
    {
        u8  unlockpriv[16]; //only SCP use this now, it uses 1st byte only
        cmdif_internalresponse_ack(CMDIF_CMD_DO_DOWNLOAD,
                                   CMDIF_ACK_UNLOCK_ECU,NULL,0);
        status = S_SUCCESS;
        if (!SETTINGS_IsDemoMode())
        {
            status = obd2tune_unlockecm(UnlockTask_Download,
                                        ecm_type,vehiclecommtype,unlockpriv);
            if (status != S_SUCCESS)
            {
                cmdif_datainfo_responsedata.responsecode = CMDIF_ACK_UNLOCK_ECU_FAIL;
                log_push_error_point(0x4215);
                goto download_ecm_done;
            }
            status = obd2tune_requesthighspeed(ecm_type,vehiclecommtype,unlockpriv);
            if (status != S_SUCCESS && status != S_BAUDUNCHANGED)
            {
                cmdif_datainfo_responsedata.responsecode = CMDIF_ACK_CHANGE_BAUD_ECU_FAIL;
                log_push_error_point(0x4216);
                goto download_ecm_done;
            }

            status = obd2tune_downloadutility(ecm_type,TRUE, is_cal_only);
            if (status == S_UTILITYNOTREQUIRED)
            {
                isutilityfileused = FALSE;
                status = S_SUCCESS;
            }
            else if (status == S_SUCCESS)
            {
                isutilityfileused = TRUE;
            }
            else
            {
                cmdif_datainfo_responsedata.responsecode = CMDIF_ACK_FAILED;
                log_push_error_point(0x4217);
                goto download_ecm_done;
            }
        }//if (!SETTINGS_IsDemoMode())...

        obd2tune_testerpresent_add_ecm(ECM_GetEcmId(ecm_type), ECM_GetCommLevel(ecm_type));
    }//if (isAllUnlocked == FALSE)...
    else
    {
        //all ECMs are already unlocked
        status = S_SUCCESS;
    }
    
    //#########################################
    // Check if a OS reflash is required
    //#########################################
    if (status == S_SUCCESS)
    {
        status = S_SUCCESS;
        if (!SETTINGS_IsDemoMode())
        {
            status = obd2tune_ecu_test_os_checksum(ecm_type);
        }
        if (status == S_NOTREQUIRED)
        {
            //do nothing
        }
        else if (status == S_UNMATCH)
        {
            if (flasherinfo->isfullreflash[TUNE_DOWNLOADING_STATUS(ecm_index)] == FALSE)
            {
                //TODOQ: flags said cal only but OS changed, should put error here or continue!?!
                //for now, force download cal only in this case
                //do nothing
            }
        }
        else if (status == S_SUCCESS)
        {
            //OS match, force download cal only
            is_cal_only = TRUE;
            block_type = Cal_Block;
            flasherinfo->isfullreflash[TUNE_DOWNLOADING_STATUS(ecm_index)] = FALSE;
            TUNE_DOWNLOADING_STATUS(totalbytecount) = cal_only_size;
        }
        else
        {
            cmdif_datainfo_responsedata.responsecode = CMDIF_ACK_FAILED;
            log_push_error_point(0x4232);
            goto download_ecm_done;
        }
        status = S_SUCCESS;
    }
    
    //#########################################
    // Handle Download
    //#########################################
    if (status == S_SUCCESS)
    {
        u8  loop;

        if (!SETTINGS_IsDownloadFail())
        {
            //set download fail flag before erase ecm content
            //this flag will be cleared once all download is successful
            SETTINGS_SetDownloadFail();
            SETTINGS_SetTuneAreaDirty();
            settings_update(SETTINGS_UPDATE_IF_DIRTY);
        }
        
        //#########################################
        //Pre Flash task
        //########################################
        if(ECM_IsPreDownloadTaskRequired(ecm_type))  
        {
            //rename to preflash_task...
            status = obd2tune_preflash_task(ecm_type,flasherinfo->flashtype,is_cal_only);
            if(status != S_SUCCESS)
            {
                if(status != S_NOTREQUIRED)
                {
                    cmdif_datainfo_responsedata.responsecode = CMDIF_ACK_FAILED;
                    log_push_error_point(0x4257);
                    goto download_ecm_done;
                }
                status = S_SUCCESS;
            }
        }
         
        //#########################################
        // Check if fingerprint is required         
        //#########################################  
        if (ECM_IsFingerPrintInterlock(ecm_type))
        {
           //do the flash starter here and the actual fingerprint interlock. 
           status = fingerprint_interlock(ecm_type, is_cal_only);
           
           if(status != S_SUCCESS)
           {
               if(status != S_NOTREQUIRED)
               {
                   cmdif_datainfo_responsedata.responsecode = CMDIF_ACK_FAILED;
                   log_push_error_point(0x4253);
                   goto download_ecm_done;   
               }
               status = S_SUCCESS;
           }
        }

        //#########################################
        // Report ECU erase start      
        //#########################################  
        cmdif_internalresponse_ack(CMDIF_CMD_DO_DOWNLOAD,
                                   CMDIF_ACK_ERASE_ECU,NULL,0);
        if (ECM_IsUseEraseCommand(ecm_type))
        {
            status = S_SUCCESS;
            if (!SETTINGS_IsDemoMode())
            {
                status = erase_ecm(ecm_type,is_cal_only);
            }
            if (status != S_SUCCESS)
            {
                cmdif_datainfo_responsedata.responsecode = CMDIF_ACK_FAILED;
                log_push_error_point(0x4218);
                goto download_ecm_done;
            }
        }

        if (flasherinfo->isfullreflash[TUNE_DOWNLOADING_STATUS(ecm_index)])
        {
            SETTINGS_SetFullFlash(TUNE_DOWNLOADING_STATUS(ecm_index));
        }
        status = filestock_get_position(&ecm_starting_offset);
        
        order_current = ORDER_0;
        for(loop=0;loop<MAX_ECM_MEMORY_BLOCKS;loop++)
        {
            for(order_index=0;
                order_index<MAX_ECM_MEMORY_BLOCKS;order_index++)
            {
                switch(block_type)
                {
                case OS_Block:
                    order = ECM_GetOSBlockFlagOrder(ecm_type,order_index);
                    break;
                case Cal_Block:
                    order = ECM_GetCalBlockFlagOrder(ecm_type,order_index);
                    break;
                default:
                    order = 0xFF;
                    break;
                }

                if (order_current == order)
                {
                    status = download_ecm_memoryblock(ecm_type,
                                                      order_index,block_type,
                                                      ecm_starting_offset,
                                                      vehiclecommtype, is_variant_id);
                     
                    if (status == S_TERMINATED)
                    {
                        //memblock with blocksize zero reached
                        status = S_SUCCESS;
                        goto download_ecm_done;
                    }
                    else if (status != S_SUCCESS)
                    {
                        cmdif_datainfo_responsedata.responsecode = CMDIF_ACK_FAILED;
                        log_push_error_point(0x4219);
                        goto download_ecm_done;
                    }
                    else if (status == S_SUCCESS)
                    {
                        //#########################################
                        // Check if RSA signature is required      
                        //#########################################
                        if(ECM_IsRSAsignatureRequired(ecm_type) && (status == S_SUCCESS))
                        {
                            status = obd2tune_download_rsa_signature(ecm_type, 
                                                                    order_index, 
                                                                    block_type); 
                            if(status != S_SUCCESS)
                            {
                                if (status != S_NOTREQUIRED)
                                {
                                    cmdif_datainfo_responsedata.responsecode = CMDIF_ACK_FAILED;
                                    log_push_error_point(0x4254);
                                    status = S_FAIL;
                                    goto download_ecm_done;
                                }
                                status = S_SUCCESS;
                            }
                        }
                    }                    
                    break;
                }
            }//for(memblock_index...
            order_current++;
        }//for(loop=...
    }
    else
    {
        //failed to unlock or download utility file
        cmdif_datainfo_responsedata.responsecode = CMDIF_ACK_UNLOCK_ECU_FAIL;
        log_push_error_point(0x421A);
        goto download_ecm_done;
    }

download_ecm_done:
    obd2tune_testerpresent();
    if (!SETTINGS_IsDemoMode())
    {
        //#########################################
        // Check if trans info block is required
        // (for FULL download only)
        //#########################################
        if (ECM_IsTransInfoRequired(ecm_type) && is_cal_only == FALSE &&
            status == S_SUCCESS)
        {
            status = obd2tune_download_trans_info(ecm_type);
            if (status != S_SUCCESS && status != S_NOTREQUIRED)
            {
                cmdif_datainfo_responsedata.responsecode = CMDIF_ACK_FAILED;
                log_push_error_point(0x4261);
                status = S_FAIL;
            }
            else
            {
                status = S_SUCCESS;
            }
        }
        if (ECM_IsRequireDownloadFinalzing(ecm_type) && status == S_SUCCESS)
        { 
            status = obd2tune_downloadfinalizing(ecm_type,flasherinfo);
        }
        if (isutilityfileused)
        {
            obd2tune_exitutility(ecm_type,vehiclecommtype,TRUE);
        }
        if (!isAllUnlocked)
        {
            obd2tune_reset_ecm(ecm_type);
        }
        obd2_setupbus_after_download(ecm_type);
        
        if(ECM_IsClearKam(ecm_type) && status == S_SUCCESS)
        {
            clear_kam(ecm_type); // Check status?
        }        
    }//if (!SETTINGS_IsDemoMode())...
    return status;
}

//------------------------------------------------------------------------------
// Download tune
// Inputs:  u8  ecm_index (0: PCM, 1: TCM, etc)
// Return:  u8  status
// Engineer: Patrick Downs
//------------------------------------------------------------------------------
u8 obd2tune_forcefullstock_backup(u8 ecm_index, u8 flashtype)
{
    u8 status;
    u8 filename[128];
    
    if (forcestockdata)
    {
        __free(forcestockdata);
        forcestockdata = NULL;
    }

    forcestockdata = __malloc(sizeof(FileStock_Info));
    if(!forcestockdata)
        return S_MALLOC;
    
    // Backup current ecm data
    *forcestockdata = filestock_getinfo();
    
    filestock_closeflashfile(); // Close current download file
    
    // Setup for full flash stock file
    flasherinfo->isfullreflash[ecm_index] = TRUE;    
    strcpy((char*)filename, (char const*)flasherinfo->basestockfilename[ecm_index]);
    
    if(flashtype == CMDIF_ACT_PRELOADED_FLASHER)
    {
        // If preloaded tune doesn't have basestockfilename
        // Use stock file.
        if(filename[0] == NULL)
        {
            strcpy((char*)filename, STOCK_FILENAME);
        }
    }
    
    status = filestock_openfile(filename, "rb", FILESTOCK_USE_STOCKFILE);
    
    return status;
}

//------------------------------------------------------------------------------
// Download tune
// Inputs:  u8  ecm_index (0: PCM, 1: TCM, etc)
// Return:  u8  status
// Engineer: Patrick Downs
//------------------------------------------------------------------------------
u8 obd2tune_forcefullstock_setup(u8 ecm_index)
{
    u8 status;
    filestock_closeflashfile(); // Close current download file
    
    // Full basestockfile downloaded, now continue with cal-only flash.
    flasherinfo->isfullreflash[ecm_index] = FALSE;
    
    if (forcestockdata->control.filehandlingtype == FILESTOCK_HANDLING_FLASHFILE)
    {
        status = filestock_openflashfile("rb");
    }
    else if (forcestockdata->control.filehandlingtype == FILESTOCK_HANDLING_STOCKFILE)
    {
        status = filestock_openstockfile("rb", FALSE);
    }
    else
    {
        status = S_FAIL; // Shouldn't get here
    }
    
    if(forcestockdata)
    {
        __free(forcestockdata);
        forcestockdata = NULL;
    }
    
    return status;
}

//------------------------------------------------------------------------------
// Download tune
// Inputs:  u8  ecm_index (0: PCM, 1: TCM, etc)
// Return:  u8  status
// Engineer: Patrick Downs
//------------------------------------------------------------------------------
u8 obd2tune_forcefullstock_complete(u8 ecm_index)
{
    // Set full flash in settings so when return to stock it will download
    // a full stock file.
    SETTINGS_SetFullFlash(ecm_index);
    
    return S_SUCCESS;
}

//------------------------------------------------------------------------------
// Comfirm Download Success
// Used to comfirm successful download before clearing the download fail flag
//
// Inputs:  u8  veh_type (vehicle type, index of veh_defs[])
// Return:  u8  status
// Engineer: Patrick Downs
//------------------------------------------------------------------------------
u8 obd2tune_comfirm_download_success(u16 veh_type)
{
    VehicleCommType vehiclecommtype;
    ecm_info ecminfo;
    u8 status;
    u8 retry;
    
    vehiclecommtype = ECM_GetCommType(VEH_GetEcmType(veh_type,0));
    
    retry = 3;
    
    delays(5000, 'm'); // Give time for PCM to reset/start up    
    
CONFIRM_DOWNLOAD_SUCCESS:
    status = obd2_readecminfo(&ecminfo, &vehiclecommtype);
    if(status == S_SUCCESS)
    {
        if(memcmp((char*)ecminfo.vin,(char*)SETTINGS_TUNE(vin),VIN_LENGTH))
        {
            // VIN Doesn't match, fail
            log_push_error_point(0x4234);
            status = S_FAIL;
        }
        
        if(ecminfo.ecm_count != VEH_GetEcmCount(veh_type))
        {
            // ECM Count doesn't match what we started with. TCM must be dead.
            log_push_error_point(0x4235);
            status = S_FAIL;
        }
    }
    
    if((status != S_SUCCESS) && retry)
    {
        retry--;
        delays(15000, 'm'); // Give more time for PCM to start up
        goto CONFIRM_DOWNLOAD_SUCCESS;
    }
    
    return status;
}

//------------------------------------------------------------------------------
// Download tune
// Inputs:  u8  veh_type (vehicle type, index of veh_defs[])
// Return:  u8  status
// Engineer: Quyen Leba
//
// log_push_error_point(0x4251); Abandoned v1.5, failed filestock_option_download_init();
//------------------------------------------------------------------------------
u8 obd2tune_downloadtune(u16 veh_type)
{
    VehicleCommType vehiclecommtype;
    VehicleCommLevel vehiclecommlevel;
    bool isAllUnlocked;
    u16 ecm_type;
    u32 ecm_length;             //total length of an ECM in stock/flash file
    u32 ecm_startposition;      //position of an ECM in stock/flash file
    u8  status;
    u8  ecm_index;
    bool islogtunehistory;
    u8  i;

    status = S_SUCCESS;
    if (SETTINGS_IsDemoMode())
    {
        //because it doesn't prepare flash.bin in demo mode
        status = filestock_download_init(FILESTOCK_USE_STOCKFILE);
    }
    else
    {
        if (flasherinfo->flashtype == CMDIF_ACT_STOCK_FLASHER)
        {
            //TODOQ: handle recvstock.bin
            status = filestock_download_init(FILESTOCK_USE_STOCKFILE);
        }
        else
        {
            status = filestock_download_init(FILESTOCK_USE_FLASHFILE);
        }
    }
    
    if (status != S_SUCCESS)
    {
        //TODOQ: some error msg here
        log_push_error_point(0x421B);
        goto obd2tune_downloadtune_done;
    }

    ecm_startposition = 0;
    isAllUnlocked = FALSE;
    ecm_type = VEH_GetEcmType(veh_type,0);
    vehiclecommtype = ECM_GetCommType(ecm_type);
    vehiclecommlevel = ECM_GetCommLevel(ecm_type);
    /* TODOT: Get oemtype a better way; maybe get from settingstune.oem_type */
    gObd2info.oemtype = OEM_TYPE;
    
    //some processors can only be unlocked once; if there was an upload,
    //it was unlocked, we need to skip unlock here
    if(flasherinfo->uploadstock_required)
    {
        //skip unlock(s)        
        if(ECM_IsSingleUnlockOnly(VEH_GetMainEcmType(veh_type)) || VEH_IsRequireUnlockAll(veh_type))
        {
            flasherinfo->isunlockpriordownload = FALSE;
            
            // Disable the TaskTimer sending tester present
            if(tasktimer_setup(0, TASKTIMER_REPEAT, TaskTimerTaskType_TesterPresent, NULL, NULL) != S_SUCCESS)
            {
                log_push_error_point(0x4250);
                cmdif_datainfo_responsedata.responsecode = CMDIF_ACK_FAILED;
                goto obd2tune_downloadtune_done;
            }
            housekeeping_removeitem(HouseKeepingType_TaskTimerTO);
            
            obd2tune_testerpresent();
            if(VEH_IsRequireUnlockAll(veh_type))
            {
                isAllUnlocked = TRUE;
            }
            
            // Check for download specific unlock
            for(ecm_index=0;ecm_index<ECM_MAX_COUNT;ecm_index++)
            {                                
                ecm_type = VEH_GetEcmType(veh_type,ecm_index);
                if (ecm_type == INVALID_ECM_DEF)
                {                    
                    break;
                }
                if (ECM_GetUnlockIndex(ecm_type) != ECM_GetDownloadUnlockIndex(ecm_type))
                {
                    // Do not skip unlock
                    flasherinfo->isunlockpriordownload = TRUE;
                    isAllUnlocked = FALSE;
                }
            }
        }
    }
    else
    {
        // start with 1st ecm commtype, reinit bus when needed (i.e. other ecms
        // use different commtype)
        status = S_SUCCESS;
        if (!SETTINGS_IsDemoMode())
        {
            status = obd2_bus_init_in_programming_mode(vehiclecommtype);
        }
        if (status != S_SUCCESS)
        {
            //TODOQ: some error msg here
            log_push_error_point(0x421C);
            goto obd2tune_downloadtune_done;
        }
    }

    //send request key on message
    cmdif_internalresponse_ack(CMDIF_CMD_DO_DOWNLOAD,CMDIF_ACK_KEY_ON,NULL,0);
    if (flasherinfo->isunlockpriordownload)
    {
        if (flasherinfo->uploadstock_required == FALSE)
        {
            if (VEH_IsSkipRecycleKeyOn(veh_type))
            {
                //do nothing
            }
            else
            {
                //as wakeup required, if there wasn't an upload, check for key on;
                //if key on, ask user to turn key off
                u8  keycheckcount;

                keycheckcount = 0;
                if (!SETTINGS_IsDemoMode())
                {
                    do
                    {
                        status = obd2_checkkeyon(vehiclecommtype, TRUE);
                        if (status == S_SUCCESS)    //key is still on
                        {
                            if (keycheckcount++ > 100)
                            {
                                //timeout
                                cmdif_datainfo_responsedata.responsecode = CMDIF_ACK_UNLOCK_ECU_FAIL;
                                log_push_error_point(0x4228);
                                status = S_UNLOCKFAIL;
                                goto obd2tune_downloadtune_done;
                            }
                            //send request key off message
                            cmdif_internalresponse_ack(CMDIF_CMD_DO_DOWNLOAD,
                                                       CMDIF_ACK_KEY_OFF,NULL,0);
                        }
                        delays(500,'m');
                    }while(status == S_SUCCESS);
                }//if (!SETTINGS_IsDemoMode())...
            }
            status = obd2_setupvpp();
            if (status != S_SUCCESS)
            {
                cmdif_datainfo_responsedata.responsecode = CMDIF_ACK_FAILED;
                log_push_error_point(0x4227);
                goto obd2tune_downloadtune_done;
            }
        }//if (flasherinfo->uploadstock_required...

        //send request key on message
        cmdif_internalresponse_ack(CMDIF_CMD_DO_DOWNLOAD,
                                   CMDIF_ACK_KEY_ON,NULL,0);

        // Make sure ECM really is ready (Required for ST 225)
        if (ECM_GetCommType(VEH_GetEcmType(veh_type,0)) == CommType_CAN &&
            ECM_IsRequireKeyOnTest(VEH_GetEcmType(veh_type,0)))
        {
            status = obd2can_key_on_test(Obd2CanEcuId_7E0);
            if (status != S_SUCCESS)
            {
                cmdif_datainfo_responsedata.responsecode = CMDIF_ACK_UNLOCK_ECU_FAIL;
                log_push_error_point(0x4246);
                goto obd2tune_downloadtune_done;
            }
        }
        
        if (VEH_IsRequireWakeUp(veh_type))
        {
            //some vehicles can only do wakeup once; if there was an upload,
            //wakeup already done, we'll need to skip the wakeup here
            if (!(flasherinfo->uploadstock_required == TRUE &&
                  VEH_IsWakeUpCheckOnce(veh_type)))
            {
                if (VEH_IsRequireUnlockAll(veh_type))
                {
                    status = S_SUCCESS;
                    if (!SETTINGS_IsDemoMode())
                    {
                        status = obd2_wakeup(veh_type,0xFE);
                    }
                    if (status != S_SUCCESS)
                    {
                        cmdif_datainfo_responsedata.responsecode = CMDIF_ACK_UNLOCK_ECU_FAIL;
                        log_push_error_point(0x421D);
                        goto obd2tune_downloadtune_done;
                    }
                    if (flasherinfo->uploadstock_required == FALSE)
                    {
                        // start with 1st ecm commtype, reinit bus when needed
                        // (i.e. other ecms use different commtype)
                        // generally, we don't need this here; just in case
                        // we'll have mixed commtype vehicle

                        status = S_SUCCESS;
                        if (!SETTINGS_IsDemoMode())
                        {
                            status = obd2_bus_init_in_programming_mode(vehiclecommtype);
                        }
                        if (status != S_SUCCESS)
                        {
                            //TODOQ: some error msg here
                            log_push_error_point(0x421E);
                            goto obd2tune_downloadtune_done;
                        }
                    }
                }
                else
                {
                    status = S_SUCCESS;
                    if (!SETTINGS_IsDemoMode())
                    {
                        status = obd2_wakeup(veh_type,0);
                    }
                }
                
                if (status != S_SUCCESS)
                {
                    cmdif_datainfo_responsedata.responsecode = CMDIF_ACK_UNLOCK_ECU_FAIL;
                    log_push_error_point(0x421F);
                    goto obd2tune_downloadtune_done;
                }
            }
        }//if (VEH_IsRequireWakeUp(veh_type))...
        
        if (VEH_IsRequireUnlockAll(veh_type) && (flasherinfo->uploadstock_required == FALSE))
        {
            status = S_FAIL;
            
            obd2tune_testerpresent_setup(TRUE,vehiclecommtype);
            
            for(ecm_index=0;ecm_index<ECM_MAX_COUNT;ecm_index++)
            {
                u8  unlockpriv[16]; //only SCP use this now, it uses 1st byte only
                
                ecm_type = VEH_GetEcmType(veh_type,ecm_index);
                if (ecm_type == INVALID_ECM_DEF)
                {
                    // termination
                    break;
                }
                
                status = S_SUCCESS;
                if (!SETTINGS_IsDemoMode())
                {
                    status = obd2tune_unlockecm(UnlockTask_Download,ecm_type,
                                                ECM_GetCommType(ecm_type),unlockpriv);
                }
                if (status != S_SUCCESS)
                {
                    cmdif_datainfo_responsedata.responsecode = CMDIF_ACK_UNLOCK_ECU_FAIL;
                    log_push_error_point(0x4220);
                    goto obd2tune_downloadtune_done;
                }

                status = S_SUCCESS;
                if (!SETTINGS_IsDemoMode())
                {
                    status = obd2tune_requesthighspeed(ecm_type,
                                                       ECM_GetCommType(ecm_type),
                                                       unlockpriv);
                }
                if (status != S_SUCCESS && status != S_BAUDUNCHANGED)
                {
                    cmdif_datainfo_responsedata.responsecode = CMDIF_ACK_CHANGE_BAUD_ECU_FAIL;
                    log_push_error_point(0x4221);
                    goto obd2tune_downloadtune_done;
                }
                obd2tune_testerpresent_add_ecm(ECM_GetEcmId(ecm_type), ECM_GetCommLevel(ecm_type));
            }
            if (status != S_SUCCESS)
            {
                cmdif_datainfo_responsedata.responsecode = CMDIF_ACK_UNLOCK_ECU_FAIL;
                log_push_error_point(0x4222);
                goto obd2tune_downloadtune_done;
            }
            isAllUnlocked = TRUE;
            flasherinfo->isunlockpriordownload = FALSE;
        }//if (VEH_IsRequireUnlockAll(veh_type))...
        else
        {
            // Setup tester present here but ECMs will be added as they are unlocked
            // in the download_ecm() function
            obd2tune_testerpresent_setup(TRUE, vehiclecommtype);
        }
    }//if (flasherinfo->isunlockpriordownload)...
    else
    {
        //do nothing
    }
    
    //##########################################################################
    // Start of the actual flashing
    //##########################################################################
    SETTINGS_TUNE(comm_type) = (u8)vehiclecommtype;
    SETTINGS_SetTuneAreaDirty();
    
    islogtunehistory = FALSE;
    status = S_FAIL;
    for(ecm_index=0;ecm_index<ECM_MAX_COUNT;ecm_index++)
    {
        if (ecm_index == 1 && (flasherinfo->flags & FLASHERFLAG_SKIP_FLASH_TCM))
        {
            continue;
        }
        ecm_type = VEH_GetEcmType(veh_type,ecm_index);
        if (ecm_type == INVALID_ECM_DEF)
        {
            // termination
            break;
        }
        TUNE_DOWNLOADING_STATUS(ecm_index) = ecm_index;
        
        if (flasherinfo->uploadstock_required == TRUE &&
            VEH_IsWakeUpCheckOnce(veh_type))
        {
            //there was an upload and wakeup only allowed once; we need to
            //skip it here
        }
        else if ((flasherinfo->isunlockpriordownload == TRUE) &&
                 (VEH_IsRequireWakeUp(veh_type)) &&
                 (ECM_IsSimpleWakeupCheckDownload(ecm_type)) == FALSE)    //TODOQ !?!?!?!?! ###
        {
            status = S_SUCCESS;
            if (!SETTINGS_IsDemoMode())
            {
                status = obd2_wakeup(veh_type,ecm_index);
            }
            if (status != S_SUCCESS)
            {
                log_push_error_point(0x4223);
                goto obd2tune_downloadtune_done;
            }
        }
        
        if (ECM_IsForceStockFirst(ecm_type) && (flasherinfo->isfullreflash[ecm_index]) &&
            flasherinfo->flashtype != CMDIF_ACT_STOCK_FLASHER &&
            !SETTINGS_IsDemoMode())
        {
            debug_out("Force Stock First - Download");
            // Download a full stock file first
            status = obd2tune_forcefullstock_backup(ecm_index, flasherinfo->flashtype);
            if(status != S_SUCCESS)
            {
                log_push_error_point(0x4229);
                goto obd2tune_downloadtune_done;
            }
            
            status = download_ecm(ecm_type,vehiclecommtype,
                                  vehiclecommlevel,isAllUnlocked);
            if (status != S_SUCCESS)
            {
                //Note: all failure must be already saved to
                //cmdif_datainfo_responsedata.responsecode in download_ecm(...)
                log_push_error_point(0x422A);
                goto obd2tune_downloadtune_done;
            }
            
            status = obd2tune_forcefullstock_setup(ecm_index);
            if(status != S_SUCCESS)
            {
                log_push_error_point(0x422B);
                goto obd2tune_downloadtune_done;
            }
            
             delays(20000, 'm'); // Give PCM time to restart

            //TODOQK: Sep27,12: only Ford 6.7L run through here; how should this wakeup be handled?
            status = obd2_wakeup(veh_type,ecm_index);
            if (status != S_SUCCESS)
            {
                log_push_error_point(0x422E);
                goto obd2tune_downloadtune_done;
            }
        }

        status = filestock_set_position(ecm_startposition);
        if (status != S_SUCCESS)
        {
            log_push_error_point(0x4225);
            goto obd2tune_downloadtune_done;
        }
        
        islogtunehistory = TRUE;
 #ifdef __DEBUG_JTAG_       
        if(ecm_index == 0)
            debug_out("Downloading ECM");
        else
            debug_out("Downloading TCM");
#endif        
        status = download_ecm(ecm_type,vehiclecommtype,
                              vehiclecommlevel,isAllUnlocked);
        if (status != S_SUCCESS)
        {
            //Note: all failure must be already saved to
            //cmdif_datainfo_responsedata.responsecode in download_ecm(...)
            log_push_error_point(0x4224);
            goto obd2tune_downloadtune_done;
        }
        
        if (ECM_IsForceStockFirst(ecm_type) && flasherinfo->flashtype != CMDIF_ACT_STOCK_FLASHER &&
            !SETTINGS_IsDemoMode())
        {
            status = obd2tune_forcefullstock_complete(ecm_index);
            if(status != S_SUCCESS)
            {
                log_push_error_point(0x422C);
                goto obd2tune_downloadtune_done;
            }
        }

        status = obd2tune_getecmsize_byecmtype(ecm_type,&ecm_length);
        if (status != S_SUCCESS || ecm_length == 0)
        {
            status = S_FAIL;
            log_push_error_point(0x4226);
            goto obd2tune_downloadtune_done;
        }
        ecm_startposition += ecm_length;
        
    }//for(ecm_index=0;ecm_index...
    
    power_setvpp(Vpp_OFF);
    
    if (isAllUnlocked && !SETTINGS_IsDemoMode())
    {
        // For Vehicle Types that require all PCMs to be unlocked prior to upload/download
        // We reset all PCMs together after download. Otherwise they are reset after
        // individual PCM downloads are complete
        for(ecm_index=0;ecm_index<ECM_MAX_COUNT;ecm_index++)
        {
            ecm_type = VEH_GetEcmType(veh_type,ecm_index);
            if (ecm_type == INVALID_ECM_DEF)
                break;   // termination
            
            obd2tune_reset_ecm(ecm_type);
        }
    }
    
    //##########################################################################
    // Done flashing, now do checking and clean up
    //##########################################################################
    if (flasherinfo->flashtype != CMDIF_ACT_STOCK_FLASHER && VEH_IsCheckSuccessAfterDownload(veh_type))
    {
        status = obd2tune_comfirm_download_success(veh_type);
        if(status != S_SUCCESS)
        {
            log_push_error_point(0x4233);
            goto obd2tune_downloadtune_done;
        }
    }
    
    SETTINGS_ClearDownloadFail();
    SETTINGS_ClearDownloadHealthCheckFail();
    for(i=0;i<ECM_MAX_COUNT;i++)
    {
        strcpy((char*)SETTINGS_TUNE(tuneinfotrack.tunedescriptions)[i],
               (char*)flasherinfo->selected_tunelistinfo.description[i]);
        file_getfilename_from_path(flasherinfo->selected_tunelistinfo.tunefilename[i],
                                   SETTINGS_TUNE(tuneinfotrack.tunefilenames)[i]);
        file_getfilename_from_path(flasherinfo->selected_tunelistinfo.optionfilename[i],
                                   SETTINGS_TUNE(tuneinfotrack.optionfilenames)[i]);
        SETTINGS_TUNE(tuneinfotrack.useoption)[i] = flasherinfo->selected_tunelistinfo.useoption[i];
    }
    
    if (!(flasherinfo->flags & FLASHERFLAG_SKIP_MARRIED_COUNT))
    {
        if ((strncmp((char*)SETTINGS_TUNE(prev_vin),
                     (char*)SETTINGS_TUNE(vin),VIN_LENGTH) != 0) ||
            (strncmp((char*)SETTINGS_TUNE(vehicle_serial_number),
                     (char*)flasherinfo->vehicle_serial_number,
                     sizeof(SETTINGS_TUNE(vehicle_serial_number))) != 0))
        {
            //TODOQ: no point to compare vehicle serial number here, because
            //check married would fail if they are different and would never
            //get this far
            if (strstr((char*)SETTINGS_TUNE(prev_vin),"QQQQQQQQQQQQQQQQQ") == NULL)
            {
                if(memcmp((char*)SETTINGS_TUNE(vin), "SCT", 3) == 0 && 
                   (SETTINGS_TUNE(current_vid_crc32e) ==
                    SETTINGS_TUNE(previous_vid_crc32e)))
                {
                    // If generated VIN and CRCs match 
                    // don't increment married count.
                }
                else
                {
                    SETTINGS_TUNE(married_count)++;
                }
            }
            
            // Update all previous vehicle settings to current
            memcpy((char*)SETTINGS_TUNE(prev_vin),
                   (char*)SETTINGS_TUNE(vin),VIN_LENGTH);
            SETTINGS_TUNE(prev_vin)[VIN_LENGTH] = NULL;
            memcpy((char*)SETTINGS_TUNE(vehicle_serial_number),
                   (char*)flasherinfo->vehicle_serial_number,
                   sizeof(SETTINGS_TUNE(vehicle_serial_number)));
            SETTINGS_TUNE(previous_vid_crc32e)=SETTINGS_TUNE(current_vid_crc32e);
        }//if ((strncmp...
    }//if (!(flasherinfo->...
    
    SETTINGS_TUNE(processor_count) = flasherinfo->ecm_count;
        
    if(flasherinfo->flashtype == CMDIF_ACT_CUSTOM_FLASHER)
    {
        // If we just downloaded a custom tune, update vehicle codes in settings if needed
        strcpy((char*)SETTINGS_TUNE(customtune_vehiclecodes),(char const*)flasherinfo->vehiclecodes);
        strcpy((char*)SETTINGS_TUNE(customtune_secondvehiclecodes),(char const*)flasherinfo->secondvehiclecodes);
    }
    
    SETTINGS_TUNE(tuneinfotrack.flashtype) = flasherinfo->flashtype;
    SETTINGS_TUNE(stockwheels.tire) = 0;
    SETTINGS_TUNE(stockwheels.axel) = 0;
    SETTINGS_SetTuneAreaDirty();
    
obd2tune_downloadtune_done:
    settings_update(SETTINGS_UPDATE_IF_DIRTY);
    filestock_download_deinit();

    if (islogtunehistory)
    {
        log_tunehistory();
    }
    
    power_setvpp(Vpp_OFF);
    
    obd2_setcooldowntime(0);

    indicator_clear();
    indicator_link_status();
    return status;
}

//------------------------------------------------------------------------------
// Test if ignition key is on using download related command
// Input:   u16 ecm_type
// Return:  u8  status
//------------------------------------------------------------------------------
u8 obd2tune_download_test_key_on(u16 ecm_type)
{
    VehicleCommType vehiclecommtype;
    u8  status;

    vehiclecommtype = ECM_GetCommType(ecm_type);
    status = S_FAIL;

    if (vehiclecommtype == CommType_CAN)
    {
        status = obd2can_download_test_key_on(ecm_type);
    }
    if (vehiclecommtype == CommType_VPW)
    {
        status = obd2vpw_download_test_key_on(ecm_type);
    }
    if (vehiclecommtype == CommType_SCP)
    {
        status = obd2scp_download_test_key_on(ecm_type);
    }
    else if (vehiclecommtype == CommType_SCP32)
    {
        status = obd2scp_download_test_key_on(ecm_type);
    }
    return status;
}

//------------------------------------------------------------------------------
// Check after download to confirm pcm/tcm not dead.
// Inputs:  u16 veh_type
// Return:  u8  status (S_SUCCESS: all passed, S_NOTREQUIRED: no check)
// Engineer: Quyen Leba
//------------------------------------------------------------------------------
u8 obd2tune_download_health_check(u16 veh_type)
{
    VehicleCommType vehiclecommtype;
    ecm_info ecminfo;
    u16 ecm_type0;
    u16 ecm_type1;
    u8  status;

    ecm_type0 = VEH_GetEcmType(veh_type,0);     //of PCM
    ecm_type1 = VEH_GetEcmType(veh_type,1);     //of TCM
    vehiclecommtype = ECM_GetCommType(ecm_type0);

    status = S_FAIL;
    if ((ECM_IsDownloadHealthCheckNone(ecm_type0)) &&
        (ecm_type1 == INVALID_ECM_DEF || ECM_IsDownloadHealthCheckNone(ecm_type1)))
    {
        return S_NOTREQUIRED;
    }

    if (!ECM_IsDownloadHealthCheckNone(ecm_type0))
    {
        //test PCM & TCM
        status = obd2_readecminfo(&ecminfo,&vehiclecommtype);
        if (status == S_SUCCESS)
        {
            if (flasherinfo->flashtype != CMDIF_ACT_STOCK_FLASHER &&
                !obd2tune_isvin_matched(ecminfo.vin,SETTINGS_TUNE(vin),0))
            {
                log_push_error_point(0x423C);
                return S_UNMATCH;
            }
            if (ecm_type1 != INVALID_ECM_DEF && !ECM_IsDownloadHealthCheckNone(ecm_type1))
            {
                //there's TCM
                if (ecminfo.ecm_count < 2)
                {
                    log_push_error_point(0x423F);
                    status = S_FAIL;
                }
            }
        }
        else
        {
            log_push_error_point(0x4244);
            status = S_FAIL;
        }
        
    }
    else if (ecm_type1 != INVALID_ECM_DEF && !ECM_IsDownloadHealthCheckNone(ecm_type1))
    {
        //test TCM
        status = obd2_readecminfo(&ecminfo,&vehiclecommtype);
        if (status == S_SUCCESS)
        {
            if (ecminfo.ecm_count < 2)
            {
                log_push_error_point(0x4241);
                status = S_FAIL;
            }
        }
        else
        {
            log_push_error_point(0x4245);
        }
    }

    return status;
}
