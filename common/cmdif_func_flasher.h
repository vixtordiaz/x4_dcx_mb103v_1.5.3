/**
  **************** (C) COPYRIGHT 2011 SCT Performance, LLC *********************
  * File Name          : cmdif_func_flasher.h
  * Device / System    : SCT products using the "CommonCode" platform
  * Author             : Quyen Leba
  *
  * Version            : 1 
  * Date               : 05/02/2011
  * Description        : 
  *                    : 
  *
  *
  * History            : 05/02/2011 M. Davis
  *                    :   Add SCT Copyright header,
  *
  ******************************************************************************
  */

#ifndef __CMDIF_FUNC_FLASHER_H
#define __CMDIF_FUNC_FLASHER_H

#include <common/obd2.h>
#include <common/obd2tune.h>

u8 cmdif_func_flasher_init_check(u8 flash_type, u8 *tunefileinfo);
u8 cmdif_func_flasher_deinit_check();
u8 cmdif_func_flasher_get_strategy();
u8 cmdif_func_flasher_get_strategy_flasher();
u8 cmdif_func_flasher_get_tune_list_info(u8 *tunecode);
u8 cmdif_func_flasher_get_tune_list_info_complete(u8 *tunecode);
u8 cmdif_func_flasher_select_tune_preloaded(u8 *tune_selection_data);
u8 cmdif_func_set_extra_custom_file_g1(u8 *extra_file_data);
u8 cmdif_func_set_extra_custom_file_g2(u8 *extra_file_data);
u8 cmdif_func_flasher_select_tune_custom(u8 *tune_selection_data);
u8 cmdif_func_flasher_select_tune_custom_ctb(u8 *tune_selection_data);
u8 cmdif_func_flasher_select_tune_custom_advance(u32 cmd_flags, u16 cmd_slot);
u8 cmdif_func_flasher_get_file_from_option_lookup(u32 cmd_flags,
                                                  GeneralFileLookupItemType item_type,
                                                  u8 code_index);
u8 cmdif_func_flasher_set_selected_tune_info(u8 ecm_index,
                                             u8 selected_tune_index);
u8 cmdif_func_flasher_general_request(u8 ecm_id);
u8 cmdif_func_flasher_use_option(u8 *optiondata, OptionSource optionsource);

#endif     //__CMDIF_FUNC_FLASHER_H
