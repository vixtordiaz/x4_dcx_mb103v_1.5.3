/**
  **************** (C) COPYRIGHT 2011 SCT Performance, LLC *********************
  * File Name          : usbcmdhandler_datalog.h
  * Device / System    : SCT products using the "CommonCode" platform
  * Author             : Quyen Leba
  *
  * Version            : 1 
  * Date               : 05/02/2011
  * Description        : 
  *                    : 
  *
  *
  * History            : 05/02/2011 M. Davis
  *                    :   Add SCT Copyright header,
  *
  ******************************************************************************
  */

#ifndef __USBCMDHANDLER_DATALOG_H
#define __USBCMDHANDLER_DATALOG_H

#include <arch/gentype.h>

typedef struct
{
    u8  state;
    u8  percentage;
    u16 flags;
    u32 max_data_report_frame_length;
    u32 signature;
    struct
    {
        u8  busy        : 1;    //1: performing a task
        u8  critical    : 1;    //1: entered critical process
        u8  stop        : 1;
        u8  reserved    : 5;
    }control;
    VehicleCommType commtype[ECM_MAX_COUNT];
    VehicleCommLevel commlevel[ECM_MAX_COUNT];
}UsbCmdHandler_Datalog_Info;

typedef enum
{
    DatalogExecuteCode_PrevalidateTask  = 0x0000,
    DatalogExecuteCode_GetPacketInfo    = 0x0001,
    DatalogExecuteCode_GetItemInfo      = 0x0002,
}UsbCmdHandler_DatalogExecuteCode;

typedef enum
{
    DatalogDTC_ClearDTC                 = 0x0000,
    DatalogDTC_ScanDTC                  = 0x0001,
    DatalogDTC_GetDTC                   = 0x0002,
}UsbCmdHandler_DatalogDTC;

//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
void uch_datalog();
bool uch_datalog_isbusy();

#endif     //__USBCMDHANDLER_DATALOG_H
