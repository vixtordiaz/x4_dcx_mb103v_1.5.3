/**
  **************** (C) COPYRIGHT 2011 SCT Performance, LLC *********************
  * File Name          : usbcmdhandler_tune.c
  * Device / System    : SCT products using the "CommonCode" platform
  * Author             : Quyen Leba
  *
  * Version            : 1 
  * Date               : 05/02/2011
  * Description        : 
  *                    : 
  *
  *
  * History            : 05/02/2011 M. Davis
  *                    :   Add SCT Copyright header,
  *
  ******************************************************************************
  */

//------------------------------------------------------------------------------
// Log error point: 0xB200 -> 0xB2FF; shared with usbcmdhandler.c
//------------------------------------------------------------------------------

#include <string.h>
#include <arch/gentype.h>
#include <board/indicator.h>
#include <board/rtc.h>
#include <common/obd2def.h>
#include <common/obd2.h>
#include <common/genmanuf_overload.h>
#include <common/file.h>
#include <common/crypto_blowfish.h>
#include <common/crypto_messageblock.h>
#include <common/statuscode.h>
#include <common/cmdif.h>
#include "usb_callback.h"
#include "usbcmdhandler.h"
#include "usbcmdhandler_tune.h"

extern cmdif_datainfo cmdif_datainfo_responsedata;  //from cmdif.c
extern USB_CALLBACK_INFO usb_callback_info;         //from usb_callback.c
extern u8  *usb_databuffer;
extern u32 usb_databufferindex;
extern u32 usb_databufferlength;

typedef struct
{
    u32 signature;
    u8  cmd;
    u8  ack;
    u8  sequence;
    u8  datalength;
    u8  reserved[4];
    u8  data[44];
}PassthroughTuneResponseAckFrame;
STRUCT_SIZE_CHECK(PassthroughTuneResponseAckFrame,56);

typedef struct
{
    u8  version;
    u8  oemtype;
    u8  pcm_commtype;
    u8  pcm_commlevel;
    u8  tcm_commtype;
    u8  tcm_commlevel;
    u8  reserved1[42];
    u8  vin[VIN_LENGTH+1];
    u8  reserved2[62];
    u8  vehicle_codes[128];
    u8  reserved3[256];
}PassthroughTuneVehicleInfo;
STRUCT_SIZE_CHECK(PassthroughTuneVehicleInfo,512);

UsbCmdHandler_Tune_Info usbcmdhandler_tune_info = 
{
    .percentage = 0,
    .control.active = 0,
    .control.critical = 0,
    .control.key_confirm = 0,
    .control.marriedcountwarn_confirm = 0,
    .control.marriedcountwarn_cancel = 0,
    .flags = 0,
};

//------------------------------------------------------------------------------
// Prototypes
//------------------------------------------------------------------------------
u8 uch_tune_init();
u8 uch_tune_deinit();
u8 uch_tune_getvehicleinfo(PassthroughTuneVehicleInfo *info);
u8 uch_tune_upload_init();
void uch_tune_upload_init_trigger();
u8 uch_tune_upload_do();
void uch_tune_upload_do_trigger();
u8 uch_tune_download_init();
void uch_tune_download_init_trigger();
u8 uch_tune_download_do();
void uch_tune_download_do_trigger();
u8 uch_tune_healthcheck_download(u16 veh_type);
void uch_tune_healthcheck_download_trigger();

//------------------------------------------------------------------------------
// Handle passthrough tune command
// Engineer: Quyen Leba
//------------------------------------------------------------------------------
void uch_tune()
{
    u8  status;

    status = S_FAIL;

    if ((USBCmdTuneFlags)usb_callback_info.cbw.flags != USBCmdTune_Init &&
        (USBCmdTuneFlags)usb_callback_info.cbw.flags != USBCmdTune_DeInit &&
        usbcmdhandler_tune_info.control.active == 0)
    {
        //do not allow if not successfully inited first
        usb_set_csw(usb_callback_info.pdev,S_INVALID_CONDITION,TRUE);
        return;
    }
    
    switch((USBCmdTuneFlags)usb_callback_info.cbw.flags)
    {
    //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    case USBCmdTune_Init:
        //check if system is ready/available for passthrough flashing
        status = uch_tune_init();
        usb_set_csw(usb_callback_info.pdev,status,TRUE);
        break;
    case USBCmdTune_DeInit:
        status = uch_tune_deinit();
        usb_set_csw(usb_callback_info.pdev,status,TRUE);
        break;
    //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    case USBCmdTune_InitFlasher:
        //Action CMDIF_ACT_CUSTOM_FLASHER, CMDIF_ACT_UPLOAD_STOCK
        //[1: Action][1: Flags][2: reserved][4: AppVerison]
        //Action CMDIF_ACT_STOCK_FLASHER
        //[1: Action][1: Flags][2: reserved][4: AppVerison][n: optional data]

        if (usb_databufferlength >= 8)
        {
            //only allow these actions
            if (usb_databuffer[0] == CMDIF_ACT_CUSTOM_FLASHER ||
                usb_databuffer[0] == CMDIF_ACT_UPLOAD_STOCK ||
                usb_databuffer[0] == CMDIF_ACT_STOCK_FLASHER)
            {
                cmdif_command(CMDIF_CMD_FLASHER,usb_databuffer,usb_databufferlength);
                status = S_SUCCESS;
            }
            else
            {
                status = S_NOTSUPPORT;
//                log_push_error_point();
            }
        }
        else
        {
//            log_push_error_point();
            status = S_BADCONTENT;
        }

        SCTUSB_SetStatusPrivData((u32)cmdif_datainfo_responsedata.responsecode);
        usb_set_csw(usb_callback_info.pdev,status,TRUE);
        break;
    //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    case USBCmdTune_GetStragyFlasher:
        cmdif_command(CMDIF_CMD_GET_STRATEGY,NULL,0);
        memset((char*)usb_databuffer,0,sizeof(usb_databuffer));
        if (cmdif_datainfo_responsedata.responsecode == CMDIF_ACK_OK)
        {
            //wrap result(codes) in 256-byte messageblock
            status = crypto_messageblock_encrypt(cmdif_datainfo_responsedata.dataptr,
                                                 cmdif_datainfo_responsedata.datalength,
                                                 usb_databuffer,&usb_databufferlength,
                                                 CryptoMessageBlockLength_Exact256, CRYPTO_USE_INTERNAL_KEY);
            if (status != S_SUCCESS)
            {
//                log_push_error_point();
            }
        }
        else
        {
            status = S_SUCCESS;     //USB part is ok, check ACK
        }
        usb_databufferlength = 256;

        SCTUSB_SetStatusPrivData((u32)cmdif_datainfo_responsedata.responsecode);
        SCTUSB_SetCmdStatus(status);
        usb_trigger_send_databuffer_to_host();
        break;
    //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    case USBCmdTune_GetVehicleInfo:
        status = uch_tune_getvehicleinfo((PassthroughTuneVehicleInfo*)usb_databuffer);
        SCTUSB_SetCmdStatus(status);
        if (status == S_SUCCESS)
        {
            usb_databufferlength = sizeof(PassthroughTuneVehicleInfo);
            usb_trigger_send_databuffer_to_host();
        }
        else
        {
//            log_push_error_point();
            usb_set_csw(usb_callback_info.pdev,status,TRUE);
        }
        break;
    //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//    case USBCmdTune_GetVehicleCommInfo:
//        break;
//    //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//    case USBCmdTune_SetVehicleComm:
//        break;
    //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    case USBCmdTune_InitUpload:
        status = uch_tune_upload_init();
        if (status != S_SUCCESS)
        {
//            log_push_error_point();
        }
        usb_set_csw(usb_callback_info.pdev,status,TRUE);
        break;
    //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    case USBCmdTune_DoUpload:
        status = uch_tune_upload_do();
        if (status != S_SUCCESS)
        {
//            log_push_error_point();
        }
        usb_set_csw(usb_callback_info.pdev,status,TRUE);
        break;
    //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    case USBCmdTune_InitDownload:
        status = uch_tune_download_init();
        if (status != S_SUCCESS)
        {
//            log_push_error_point();
        }
        usb_set_csw(usb_callback_info.pdev,status,TRUE);
        break;
    //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    case USBCmdTune_DoDownload:
        status = uch_tune_download_do();
        if (status != S_SUCCESS)
        {
//            log_push_error_point();
        }
        usb_set_csw(usb_callback_info.pdev,status,TRUE);
        break;
//    //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//    case USBCmdTune_InitReturnStock:
//        //not used
//        break;
//    //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//    case USBCmdTune_DoReturnStock:
//        //not used
//        break;
    //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    case USBCmdTune_HealthCheckDownload:
        if (usb_databufferlength >= 2)
        {
            status = uch_tune_healthcheck_download(*(u16*)usb_databuffer);
            if (status != S_SUCCESS)
            {
//                log_push_error_point();
            }
        }
        else
        {
//            log_push_error_point();
            status = S_BADCONTENT;
        }
        usb_set_csw(usb_callback_info.pdev,status,TRUE);
        break;
    //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    case USBCmdTune_KeyConfirm:
        usbcmdhandler_tune_info.control.key_confirm = 1;
        break;
    case USBCmdTune_MarriedCountWarnConfirm:
        usbcmdhandler_tune_info.control.marriedcountwarn_confirm = 1;
        usbcmdhandler_tune_info.control.marriedcountwarn_cancel = 0;
        break;
    case USBCmdTune_MarriedCountWarnCancel:
        usbcmdhandler_tune_info.control.marriedcountwarn_confirm = 0;
        usbcmdhandler_tune_info.control.marriedcountwarn_cancel = 1;
        break;
    //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    case USBCmdTune_SearchVehicleType:
        cmdif_command(CMDIF_CMD_SEARCH_FOR_VEHICLE_TYPE,NULL,0);
        SCTUSB_SetStatusPrivData((u32)cmdif_datainfo_responsedata.responsecode);
        usb_set_csw(usb_callback_info.pdev,S_SUCCESS,TRUE);
        break;
    //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    case USBCmdTune_CompileUserVehicleType:
        if (usb_databufferlength == 64)
        {
            status = crypto_messageblock_encrypt(usb_databuffer,64,
                                                 &usb_databuffer[64],&usb_databufferlength,
                                                 CryptoMessageBlockLength_Exact64, CRYPTO_USE_INTERNAL_KEY);
            if (status == S_SUCCESS)
            {
                cmdif_command(CMDIF_CMD_COMPILE_USER_VEHICLE_TYPE,
                              &usb_databuffer[64],usb_databufferlength);
                SCTUSB_SetStatusPrivData((u32)cmdif_datainfo_responsedata.responsecode);
            }
            else
            {
//                log_push_error_point();
            }
        }
        else
        {
            status = S_INPUT;
//            log_push_error_point();
        }
        usb_set_csw(usb_callback_info.pdev,S_SUCCESS,TRUE);
        break;
//    //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//    case USBCmdTune_GetUploadStock:
//        //later
//        break;
    //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    default:
        //should never happen (because already checked usb_callback.c)
//        log_push_error_point();
        usb_set_csw(usb_callback_info.pdev,S_USB_UNKNOWN_FLAGS,TRUE);
        break;
    }
    settings_update(SETTINGS_UPDATE_IF_DIRTY);
}

/**
 * @brief   Init passthrough flashing session. When inited, must deinit() to close the session
 *
 * @retval  u8 status
 *
 * @author  Quyen Leba
 *
 */
u8 uch_tune_init()
{
    if (usbcmdhandler_tune_info.control.critical)
    {
        return S_REJECT;
    }
//    else if (usbcmdhandler_tune_info.control.active)
//    {
//        //this could be too strict
//        return S_REJECT;
//    }

    usbcmdhandler_tune_info.control.active = 1;
    usbcmdhandler_tune_info.control.key_confirm = 0;
    usbcmdhandler_tune_info.control.marriedcountwarn_confirm = 0;
    usbcmdhandler_tune_info.control.marriedcountwarn_cancel = 0;

    uch_ui_lock_set_force_lock();
    return S_SUCCESS;
}

/**
 * @brief   Deinit(cleanup) passthrough flashing session
 *
 * @retval  u8 status
 *
 * @author  Quyen Leba
 *
 */
u8 uch_tune_deinit()
{
    if (usbcmdhandler_tune_info.control.critical)
    {
        //in critical process, can't deinit yet
        return S_REJECT;
    }
    usbcmdhandler_tune_info.control.active = 0;
    uch_ui_lock_clear_force_lock();

    return S_SUCCESS;
}

/**
 * @brief   Check if passthrough flashing session is busy
 *
 * @retval  bool TRUE: busy
 *
 * @author  Quyen Leba
 *
 */
bool uch_tune_isbusy()
{
    return (bool)(usbcmdhandler_tune_info.control.active || usbcmdhandler_tune_info.control.critical);
}

//------------------------------------------------------------------------------
// Get vehicle info
// Output:  PassthroughTuneVehicleInfo *info
// Engineer: Quyen Leba
//------------------------------------------------------------------------------
u8 uch_tune_getvehicleinfo(PassthroughTuneVehicleInfo *info)
{
    vehicle_info vehicleinfo;
    u8  status;

    usb_clear_alt_data();

    vehicleinfo.commtype = CommType_Unknown;
    status = obd2_readecminfo(&vehicleinfo.ecm,&vehicleinfo.commtype);
    if (status == S_SUCCESS)
    {
        memset((char*)info,0,sizeof(PassthroughTuneVehicleInfo));
        info->version = 0;
        info->oemtype = OEM_TYPE;
        memcpy((char*)info->vin,(char*)vehicleinfo.ecm.vin,sizeof(info->vin));

        info->pcm_commtype = vehicleinfo.ecm.commtype[0];
        info->pcm_commlevel = vehicleinfo.ecm.commlevel[0];
        if (vehicleinfo.ecm.ecm_count > 1)
        {
            info->tcm_commtype = vehicleinfo.ecm.commtype[1];
            info->tcm_commlevel = vehicleinfo.ecm.commlevel[1];
        }
        else
        {
            info->tcm_commtype = CommType_Unknown;
            info->tcm_commlevel = CommLevel_Unknown;
        }

        strcpy((char*)info->vehicle_codes,(char*)vehicleinfo.ecm.codes[0][0]);
        strcat((char*)info->vehicle_codes,TUNECODE_SEPARATOR_STRING);
        if (vehicleinfo.ecm.ecm_count > 1)
        {
            strcat((char*)info->vehicle_codes,(char*)vehicleinfo.ecm.codes[1][0]);
            strcat((char*)info->vehicle_codes,TUNECODE_SEPARATOR_STRING);
        }
    }
    else
    {
//        log_push_error_point();
    }
    return status;
}

/**
 * @brief   Internal response ack
 *
 * @param   [in] cmd
 * @param   [in] respack
 * @param   [in] data
 * @param   [in] datalength
 *
 * @retval  u8 status
 *
 * @author  Quyen Leba
 *
 * @details Send internal response ack to HOST. Used during listening mode.
 */
u8 uch_tune_internal_response_ack(CMDIF_COMMAND cmd, CMDIF_ACK respack,
                                  u8 *data, u16 datalength)
{
    UsbCmdHandlerPassthroughTuneResponseAckFrame ackframe;
    u8  buffer[64];
    u32 tmp_u32;
    u16 bytesent;
    u8  bytetosend;
    u8  status;

    if (!uch_tune_isbusy())
    {
        return S_NOTREQUIRED;
    }

    ackframe.cmd = cmd;
    ackframe.ack = respack;
    ackframe.sequence = 0;
    ackframe.signature = 0x38F25A71;

    bytesent = 0;
    while(bytesent < datalength)
    {
        bytetosend = datalength - bytesent;
        if (bytetosend > sizeof(ackframe.data))
        {
            bytetosend = sizeof(ackframe.data);
        }

        memcpy((char*)ackframe.data,(char*)&data[bytesent],bytetosend);
        ackframe.datalength = bytetosend;

        status = crypto_messageblock_encrypt((u8*)&ackframe,sizeof(ackframe),
                                             buffer,&tmp_u32,
                                             CryptoMessageBlockLength_Exact64,
                                             CRYPTO_USE_EXTERNAL_KEY);
        if (status != S_SUCCESS)
        {
            break;
        }
        usb_push_alt_data(buffer,sizeof(buffer));

        bytesent += bytetosend;
        ackframe.sequence++;
    }

    return status;
}

/**
 * @brief   Upload init
 *
 * @retval  u8 status
 *
 * @author  Quyen Leba
 *
 * @details Initiate the process of upload init
 */
u8 uch_tune_upload_init()
{
    u8  status;

    interrupt_sw_trigger_set_func(SoftwareIntrTrigger_0,
                                  uch_tune_upload_init_trigger,TRUE);
    status = interrupt_sw_trigger(SoftwareIntrTrigger_0);

    return status;
}

//------------------------------------------------------------------------------
// Used by a software interrupt trigger to start upload init session
// Engineer: Quyen Leba
//------------------------------------------------------------------------------
void uch_tune_upload_init_trigger()
{
    usb_clear_alt_data();

    cmdif_command(CMDIF_CMD_INIT_UPLOAD,NULL,0);
    //currently, upload init has only 1 ACK response (no listening mode)
    uch_tune_internal_response_ack(cmdif_datainfo_responsedata.commandcode,
                                   cmdif_datainfo_responsedata.responsecode,
                                   cmdif_datainfo_responsedata.dataptr,
                                   cmdif_datainfo_responsedata.datalength);
}

/**
 * @brief   Upload do
 *
 * @retval  u8 status
 *
 * @author  Quyen Leba
 *
 * @details Initiate the process of upload do
 */
u8 uch_tune_upload_do()
{
    u8  status;

    interrupt_sw_trigger_set_func(SoftwareIntrTrigger_0,
                                  uch_tune_upload_do_trigger,TRUE);
    status = interrupt_sw_trigger(SoftwareIntrTrigger_0);

    return status;
}

//------------------------------------------------------------------------------
// Used by a software interrupt trigger to start upload do session
// Engineer: Quyen Leba
//------------------------------------------------------------------------------
void uch_tune_upload_do_trigger()
{
    usb_clear_alt_data();

    //upload do has listening mode, all ACKs handled internally
    cmdif_command(CMDIF_CMD_DO_UPLOAD,NULL,0);
}

/**
 * @brief   Download init
 *
 * @retval  u8 status
 *
 * @author  Quyen Leba
 *
 * @details Initiate the process of download init
 */
u8 uch_tune_download_init()
{
    u8  status;

    interrupt_sw_trigger_set_func(SoftwareIntrTrigger_0,
                                  uch_tune_download_init_trigger,TRUE);
    status = interrupt_sw_trigger(SoftwareIntrTrigger_0);

    return status;
}

//------------------------------------------------------------------------------
// Used by a software interrupt trigger to start download init session
// Engineer: Quyen Leba
//------------------------------------------------------------------------------
void uch_tune_download_init_trigger()
{
    usb_clear_alt_data();

    //upload do has listening mode, all ACKs handled internally
    cmdif_command(CMDIF_CMD_INIT_DOWNLOAD,NULL,0);
}

/**
 * @brief   Download do
 *
 * @retval  u8 status
 *
 * @author  Quyen Leba
 *
 * @details Initiate the process of download do
 */
u8 uch_tune_download_do()
{
    u8  status;

    interrupt_sw_trigger_set_func(SoftwareIntrTrigger_0,
                                  uch_tune_download_do_trigger,TRUE);
    status = interrupt_sw_trigger(SoftwareIntrTrigger_0);

    return status;
}

//------------------------------------------------------------------------------
// Used by a software interrupt trigger to start download do session
// Engineer: Quyen Leba
//------------------------------------------------------------------------------
void uch_tune_download_do_trigger()
{
    usb_clear_alt_data();

    //upload do has listening mode, all ACKs handled internally
    cmdif_command(CMDIF_CMD_DO_DOWNLOAD,NULL,0);
}

/**
 * @brief   Check vehicle after download
 *
 * @param   [in] veh_type
 *
 * @retval  u8 status
 *
 * @author  Quyen Leba
 *
 */
u8 uch_tune_healthcheck_download(u16 veh_type)
{
    u8  status;

    interrupt_sw_trigger_set_func(SoftwareIntrTrigger_0,
                                  uch_tune_healthcheck_download_trigger,TRUE);
    interrupt_sw_trigger_set_privdata(SoftwareIntrTrigger_0,
                                      (u8*)&veh_type,2);
    status = interrupt_sw_trigger(SoftwareIntrTrigger_0);

    return status;
}

//------------------------------------------------------------------------------
// Used by a software interrupt trigger to start healthcheck download session
// Engineer: Quyen Leba
//------------------------------------------------------------------------------
void uch_tune_healthcheck_download_trigger()
{
    u8  privdata[SW_INTR_PRIVDATA_MAX_LENGTH];
    u8  privdata_length;
    u8  status;

    status = interrupt_sw_trigger_get_privdata(SoftwareIntrTrigger_0,
                                               privdata, &privdata_length);
    if (status == S_SUCCESS && privdata_length >= 2)
    {
        cmdif_command(CMDIF_CMD_HEALTH_CHECK_DOWNLOAD,privdata,privdata_length);
    }
    else
    {
        cmdif_datainfo_responsedata.responsecode = CMDIF_ACK_FAILED;
        cmdif_datainfo_responsedata.dataptr = NULL;
        cmdif_datainfo_responsedata.datalength = 0;
//        log_push_error_point();
    }

    //healthcheck has listen mode but also terminating ACK handled outside of its function
    uch_tune_internal_response_ack(cmdif_datainfo_responsedata.commandcode,
                                   cmdif_datainfo_responsedata.responsecode,
                                   cmdif_datainfo_responsedata.dataptr,
                                   cmdif_datainfo_responsedata.datalength);
}
