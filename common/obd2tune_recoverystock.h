/**
  **************** (C) COPYRIGHT 2011 SCT Performance, LLC *********************
  * File Name          : obd2tune_recoverystock.h
  * Device / System    : SCT products using the "CommonCode" platform
  * Author             : Quyen Leba
  *
  * Version            : 1 
  * Date               : 05/02/2011
  * Description        : 
  *                    : 
  *
  *
  * History            : 05/02/2011 M. Davis
  *                    :   Add SCT Copyright header,
  *
  ******************************************************************************
  */

#ifndef __OBD2TUNE_RECOVERYSTOCK_H
#define __OBD2TUNE_RECOVERYSTOCK_H

#include <arch/gentype.h>

#define RECOVERYSTOCK_HEADER_SIZE           64
#define RECOVERYSTOCK_IDFILE                "recv_id.bak"

typedef void(*recoverystock_progressfunction)(u8 percentage);

u8 obd2tune_recoverystock_check(u8 *recoverystock_header);
u8 obd2tune_recoverystock_register(u8 *recoverystock_header, u8 *filename,
                                   recoverystock_progressfunction func);
u8 obd2tune_recoverystock_validate_idfile(u32 *crc32e);
u8 obd2tune_recoverystock_remove_idfile();


#endif  //__OBD2TUNE_RECOVERYSTOCK_H
