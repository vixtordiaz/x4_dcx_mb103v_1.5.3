/**
  **************** (C) COPYRIGHT 2011 SCT Performance, LLC *********************
  * File Name          : obd2can.h
  * Device / System    : SCT products using the "CommonCode" platform
  * Author             : Quyen Leba
  *
  * Version            : 1 
  * Date               : 05/02/2011
  * Description        : 
  *                    : 
  *
  *
  * History            : 05/02/2011 M. Davis
  *                    :   Add SCT Copyright header,
  *
  ******************************************************************************
  */

#ifndef __OBD2CAN_H
#define __OBD2CAN_H

#include "obd2.h"
#include "obd2tune.h"
#include <common/obd2def.h>

#define OBD2CAN_ANY_ECMID               0xFFFF
#define OBD2CAN_ANY_CMD                 0xFF

#define ECM_ID(ecm_id)                  (ecm_id-8)      // Rhonda fix this
#define ECM_RESPONSE_ID(ecm_id)         (ecm_id+8)
#define ECM_2ND_RESPONSE_ID(ecm_id)     (ecm_id-0x200+8)

/* VIN List for VIN search */
#define VIN_LIST_CAN            0x000100C0, 0x000101C0, 0x001C3220
#define VIN_LIST_CAN_COUNT      3  

typedef enum
{
    Obd2canInitNormal                   = 0,
    Obd2canInitProgram                  = 1,
    Obd2canInitCustom                   = 2,
}Obd2canInitMode;

typedef enum
{
    TESTER_PRESENT_NONE,
    TESTER_PRESENT_BROADCAST,
    TESTER_PRESENT_NODE,
}TesterPresentType;

typedef enum
{
    Obd2canTxRaw,
    Obd2canTxNormal,
}Obd2canTxMode;

typedef struct
{
    u32 ecm_id;
    bool broadcast;
    u8 service;
    u8 subservice;
    u8 service_controldatabuffer[8];
    u8 service_controldatalength;
    
    u8 *txdata;
    u16 txdatalength;
    Obd2canTxMode txmode;
}obd2can_txinfo;

//IMPORTANT: MB & VB share this struct, changes to this struct must be made in 
//              both and versions must be incremented.
#define OBD2CAN_RXINFO_VERSION      1
typedef struct
{
    u8 version;
    bool response_expected;
    bool secondary_response;
    u32 secondary_response_ecm_id;
    u8  *rxbuffer;
    u16 rxlength;
    u8  cmd;                    /* Same as u8 service of txinfo */
    u8  response_cmd_offset;
    u8  first_frame_data_offset;
    TesterPresentType tester_present_type;
    u8  errorcode;
    u16 initial_wait_extended;  /* Extended wait time of 1st response; 1->x (1sec -> x sec) */
    u16 busy_wait_extended;     /* Extended wait time of 7F 78 negative response; 1->6 (10s->60s) */
    u32 rx_timeout_ms;          /* Low level driver COM timeout in miliseconds */
    VehicleCommLevel commlevel;
    u8  flowcontrol_blocksize;
    u8  flowcontrol_separationTime;
}obd2can_rxinfo;

typedef struct
{
    obd2can_txinfo txinfo;
    obd2can_rxinfo rxinfo;
}obd2can_diagserviceinfo;

typedef struct
{
    u32 ecm_id;
    u32 response_id;
    u32 broadcast_id;
    bool broadcast;
    bool response_expected;
    bool nowait_cantx;
    TesterPresentType tester_present_type;
    u8 service;
    u8 subservice;
    u8 service_controldatabuffer[8];
    u8 service_controldatalength;
    u32 rx_timeout_ms;
    u8 *txdata;
    u16 txdatalength;
    u8 first_frame_data_offset;
    u8 *rxdata;
    u16 rxdatalength;
    void *priv;
    u8 errorcode; //not yet used
    //used to extend wait time of 1st response; 1->x (1sec -> x sec)
    u16 initial_wait_extended;
    //used to extend wait time of 7F 78 negative response; 1->6 (10s->60s)
    u16 busy_wait_extended;
    VehicleCommLevel commlevel;
    u8  flowcontrol_blocksize;
    u8  flowcontrol_separationTime;
}Obd2can_ServiceData;

//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
void obd2can_init(Obd2canInitMode initmode, u32 custom_filter_addr);
void obd2can_servicedata_init(Obd2can_ServiceData *servicedata);
void obd2can_rxinfo_init(obd2can_rxinfo *rxinfo);
u8 obd2can_txraw(u32 ecm_id, u8 *txbuffer, u16 txlength, bool nowait_cantx);
u8 obd2can_txraw_ext(u32 ecm_extended_id, u8 *txbuffer, u16 txlength);
u8 obd2can_tx(u32 ecm_id, u8 *txbuffer, u16 txlength, bool nowait_cantx);
u8 obd2can_txcomplex(u32 ecm_id, u32 response_id, u8 cmd, u8 subcmd,
                     u8 *subcmdbuffer, u16 subcmdbufferlength,
                     u8 *txbuffer, u16 txlength, bool nowait_cantx);
u8 obd2can_rxraw(u32 *response_ecm_id, u8 *data);
u8 obd2can_rxraw_ext(u32 ecm_response_extended_id, u8 *rxbuffer, u16 *rxlength);
u8 obd2can_rxraw_complete(u32 *response_ecm_id, obd2can_rxinfo *rxinfo,
                          u32 *packet_filter, u8 packet_count);
u8 obd2can_rx(u32 response_ecm_id, obd2can_rxinfo *rxinfo);
u8 obd2can_rxcomplex(u32 response_ecm_id, obd2can_rxinfo *rxinfo);
u8 obd2can_txrx(Obd2can_ServiceData *servicedata);
u8 obd2can_txrxcomplex(Obd2can_ServiceData *servicedata);

//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
#include <common/genmanuf_overload.h>
u8 obd2can_read_memory_address_by20extendedid(u32 ecm_id,
                                              u32 ecm_response_id,
                                              u32 address, u32 length,
                                              MemoryAddressType addrtype,
                                              u8 *data, u16 *datalength);
u8 obd2can_read_data_bypid_mode1(bool broadcast, u32 ecm_id, u8 pid,    //$01
                                u8 *data, u8 *datalength, bool response_expected);
u8 obd2can_read_data_bypid(u32 ecm_id, u16 pid,                         //$22
                           u8 *data, u8 *datalength, bool response_expected);
u8 obd2can_write_data_bypid(u32 ecm_id, u16 pid, u8 *data, u16 datalength);  //$2E

u8 obd2can_validatepid(u32 ecm_id, DlxBlock *dlxblock, VehicleCommLevel commlevel);
u8 obd2can_validatedmr(u32 ecm_id, DlxBlock *dlxblock, VehicleCommLevel commlevel);
u8 obd2can_validateByRapidPacketSetup(u32 ecm_id, DlxBlock *dlxblock, 
                                      VehicleCommLevel commlevel);
u8 obd2can_validate_oscpid(u32 ecm_id, DlxBlock *dlxblock, VehicleCommLevel commlevel);

u8 obd2can_read_memory_address(u32 ecm_id, u32 address, u16 length,     //$23
                               MemoryAddressType addrtype,
                               MemorySize memsize,
                               u8 *data, u16 *datalength,
                               bool force_readby4bytes,
                               VehicleCommLevel commlevel);
u8 obd2can_security_access(u32 ecm_id,                                  //$27   
                           Obd2can_SubServiceNumber subservice,
                           u8 *data, u8 *datalength, u32 algoindex);
u8 obd2can_request_download(u32 ecm_id,                                 //$34
                            u8 dataformat,
                            u32 requestaddress, u32 requestlength,
                            RequestLengthType reqtype,
                            VehicleCommLevel vehiclecommlevel,
                            u16 *maxdownloadblocklength);
u8 obd2can_request_upload(u32 ecm_id,                                  //$35
                          u32 requestaddress, u32 requestlength,
                          RequestLengthType reqtype,
                          VehicleCommLevel commlevel,
                          u16 *maxuploadblocklength);
u8 obd2can_transfer_data_upload(u32 ecm_id, u8 blocktracking,           //$36
                                VehicleCommLevel commlevel,
                                u32 request_address,
                                RequestLengthType request_type,
                                u8 *data, u16 *datalength);
u8 obd2can_transfer_data_download(u32 ecm_id,                           //$36
                                  Obd2can_SubServiceNumber subservice,
                                  u8 blocktracking, u32 startingaddr, 
                                  MemoryAddressType addrtype, u8 *data, 
                                  u16 datalength, VehicleCommLevel commlevel,
                                  bool response_expected);
u8 obd2can_request_transfer_exit(u32 ecm_id,                            //$37 
                                 VehicleCommLevel commlevel); 

//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
void obd2can_testerpresent(bool broadcast, u32 ecm_id,
                           VehicleCommLevel commlevel);
u8 obd2can_ping(u32 ecm_id, bool fast);
u8 obd2can_uds_ping(u32 ecm_id);
u8 obd2can_key_on_test(u32 ecm_id);

u8 obd2can_cleardtc();
u8 obd2can_readdtc(u32 ecm_id, dtc_info *info);
u8 obd2can_alt_readdtc(u32 ecm_id, dtc_info *info);
u8 obd2can_disableallnormalcomm();

u8 obd2can_readecminfo(ecm_info *ecm);
u8 obd2can_readvin_indirect_blankvin_overlay(u8 *vin);
u8 obd2can_readvin(u8 *vin);
u8 obd2can_read_calid(u32 ecm_id, u8 *calid);

u8 obd2can_resetecm(u32 ecm_id);
u8 obd2can_resetfueltrim(u32 ecm_id);
u8 obd2can_getecmunlockalgorithm(u8 *seed, u32 algoindex, VehicleCommType commtype,
                                 u8 *key);
u8 obd2can_unlockecm(UnlockTask unlocktask, u16 ecm_type, bool seed_peek);
u8 obd2can_download_test_key_on(u16 ecm_type);

u8 obd2can_generic_readdtc(u32 ecm_id, dtc_info *info);
u8 obd2can_generic_cleardtc(u32 ecm_id);
u8 obd2can_getinfo(obd2_info *obd2info);
u8 obd2can_getcommlevelinfo(u32 pcm_id, VehicleCommLevel *commlevel, OemType *oemtype);
u8 obd2can_get_padding_byte(void);
void obd2can_testerpresent(bool broadcast, u32 ecm_id,
                                VehicleCommLevel commlevel);

#endif    //__OBD2CAN_H
