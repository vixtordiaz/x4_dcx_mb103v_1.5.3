/**
  **************** (C) COPYRIGHT 2011 SCT Performance, LLC *********************
  * File Name          : cmdif_func_datalog.h
  * Device / System    : SCT products using the "CommonCode" platform
  * Author             : Quyen Leba
  *
  * Version            : 1 
  * Date               : 05/02/2011
  * Description        : 
  *                    : 
  *
  *
  * History            : 05/02/2011 M. Davis
  *                    :   Add SCT Copyright header,
  *
  ******************************************************************************
  */

#ifndef __CMDIF_FUNC_DATALOG_H
#define __CMDIF_FUNC_DATALOG_H

#include <arch/gentype.h>
#include <common/obd2datalog.h>

u8 cmdif_func_datalog_scan(u8 *datalogfilename);
u8 cmdif_func_datalog_getfeature();
u8 cmdif_func_datalog_request_otf(u8 *vin);
u8 cmdif_func_datalog_start(u8 *datalogfilename);
u8 cmdif_func_datalog_set_report_rate(u8 attribute, u8 speed);
u8 cmdif_func_datalog_record(u8 record_cmd);
u8 cmdif_func_datalog_save_result(u8 type, u8 *data, u16 datalength);
u8 cmdif_func_datalog_update_signal_osc(u8 *data, u16 count);

#endif    //__CMDIF_FUNC_DATALOG_H
