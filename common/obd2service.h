/*
 *  Common Code Platform
 *
 *  Copyright 2014 SCT Performance, LLC
 *
 */

/**
 * @file    obd2service.h
 * @brief   Definitions used for OBD2 Services
 * @details This file contains definitions for OBD2 enhanced diagnostic 
 *          services. These services which are shared across multiple vehicle 
 *          manufacturer OEMs.
 * @date    10/15/2014 
 * @author  Tristen Pierson
 */ 


/******************************************************************************/
/* OBD2 flags */
/******************************************************************************/

#define OBD2_FLAGS_NONE         (0)
#define OBD2_ADDRESS_3_BYTES    (1<<0)
#define OBD2_ADDRESS_4_BYTES    (1<<1)
#define OBD2_LENGTH_1_BYTE      (1<<2)
#define OBD2_LENGTH_2_BYTES     (1<<3)
#define OBD2_FORCE_READ_4_BYTES (1<<4)
#define OBD2_UDS_FORMAT_ID      (1<<5)

/******************************************************************************/
/* Function Prototypes */
/******************************************************************************/

u8 obd2service_ReadMemoryByAddress(u32 ecu_id, u32 address, u16 length, 
                                   u8 *data, u16 *datalength);      /* $23 */

