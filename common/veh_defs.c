/**
  **************** (C) COPYRIGHT 2014 SCT Performance, LLC *********************
  * File Name          : veh_defs.c
  * Device / System    : SCT products using the "CommonCode" platform
  * Author             : Tristen Pierson
  *
  * Version            : 1 
  * Date               : 12/15/2014
  * Description        : 
  *                    : 
  *
  ******************************************************************************
  */
  
#include <fs/genfs.h>
#include <common/statuscode.h>
#include <common/crypto_blowfish.h>
#include <common/obd2.h>
#include <common/veh_defs.h>

//------------------------------------------------------------------------------
// Log error point: 0x44xx -> 0x46xx (shared with obd2tune.c)
//------------------------------------------------------------------------------

/* ONLY USED FOR TESTING! When offical release, this needs to be cleared */
#ifdef __FORD_MANUF__
#if (APPLICATION_VERSION_BUILD == 0)
#define TESTING_ALLOW_BLANK_VIN         0
#else
#define TESTING_ALLOW_BLANK_VIN         1
#endif
#endif

VehDefLastBlock gVehDefLastBlock;

static u8 vehdef_vehblock_init(VehicleTypeBlock *block);
static u8 vehdef_ecmblock_init(EcmTypeBlock *block);
static u8 vehdef_utiblock_init(UtilityTypeBlock *block);
static u8 vehdef_get_header_info(VehicleTypeHeader *info);
static u8 vehdef_get_ecm_info(u16 ecm_type);
static u8 vehdef_get_utility_info(u16 ecm_type, u16 search_index);
static u8 vehdef_open_vdf_file(F_FILE **pFile, VehicleTypeHeader *header);
static void vehdef_close_vdf_file(F_FILE *pFile);
static u8 vehdef_validate_vdf(F_FILE *pFile, VehicleTypeHeader *header);


/**
 *  @brief Initialize Vehicle Definitions Info Structures
 * 
 *  @return Nothing to return
 */ 
void vehdef_init(void)
{
    gVehDefLastBlock.vehdefmin = 0;
    gVehDefLastBlock.vehdefmax = 0;
    gVehDefLastBlock.vehdefcount = 0;
    gVehDefLastBlock.ecmdefcount = 0;
    gVehDefLastBlock.utidefcount = 0;
    vehdef_vehblock_init(&gVehDefLastBlock.VehBlock);
    vehdef_ecmblock_init(&gVehDefLastBlock.EcmBlock);
    vehdef_utiblock_init(&gVehDefLastBlock.UtiBlock);
    return;
}

/**
 *  @brief Get ECM Count
 *
 *  @param [in]     veh_type    Vehicle Type
 *
 *  @return Status
 */
u8 vehdef_vehecmcount(u16 veh_type) 
{
    u8 i, count;
    for(i = 0, count = 0; i < ECM_MAX_COUNT; i++)
    {
        if (VEH_GetEcmType(veh_type, i) != INVALID_ECM_DEF)
        {
            count++;
        }
    }
    return count;
}

/**
 *  @brief Get Vehicle Type Variable
 *
 *  @param [in]     veh_type    Vehicle Type
 *  @param [in]     parameter   Parameter to read
 *
 *  @return Status
 */
u32 vehdef_vehvar(u16 veh_type, u16 parameter) 
{   
    u32 retv = 0;
    u8 status;

    switch (parameter)
    {
    case VEH_minvehdef:
    case VEH_maxvehdef:
    case VEH_veh_def_count:
    case VEH_ecm_def_count:
    case VEH_uti_def_count:
        status = vehdef_get_header_info(NULL);
        if (status == S_SUCCESS)
        {
            switch (parameter)
            {
            case VEH_minvehdef:
                retv = (u32)&gVehDefLastBlock.vehdefmin;
                break;
            case VEH_maxvehdef:
                retv = (u32)&gVehDefLastBlock.vehdefmax;
                break;
            case VEH_veh_def_count:
                retv = (u32)&gVehDefLastBlock.vehdefcount;
                break;
            case VEH_ecm_def_count:
                retv = (u32)&gVehDefLastBlock.ecmdefcount;
                break;
            case VEH_uti_def_count:
                retv = (u32)&gVehDefLastBlock.utidefcount;
                break;
            }
        }
        break;
    case VEH_flags:
    case VEH_ecm_types:
        status = vehdef_get_vehdef_info(&veh_type, 1, NULL);
        if (status == S_SUCCESS)
        {
            switch (parameter)
            {
            case VEH_flags:
                retv = (u32)&gVehDefLastBlock.VehBlock.flags;
                break;
            case VEH_ecm_types:
                retv = (u32)&gVehDefLastBlock.VehBlock.ecm_types;
                break;
            }
        }
        break;
    default:
        status = S_INPUT;
        break;
    }
    if (status != S_SUCCESS && status != S_UNMATCH)
    {
        /* Error reading veh_var */
        log_push_error_point(0x4610);
        led_emergency_indicator_system_error();
    }
    return retv;
}

/**
 *  @brief Get ECM Type Variable
 *
 *  @param [in]     ecm_type    Vehicle Type
 *  @param [in]     parameter   Parameter to read
 *
 *  @return Status
 */
u32 vehdef_ecmvar(u16 ecm_type, u16 parameter)
{
    u32 retv = 0;
    u8 status;

    status = vehdef_get_ecm_info(ecm_type);

    switch (parameter)
    {
    case ECM_ecm_index:
        retv = (u32)&gVehDefLastBlock.EcmBlock.index;
        break;
    case ECM_upload_unlock_id:
        retv = (u32)&gVehDefLastBlock.EcmBlock.upload_unlock_id;
        break;
    case ECM_download_unlock_id:
        retv = (u32)&gVehDefLastBlock.EcmBlock.download_unlock_id;
        break;
    case ECM_unlock_flags:
        retv = (u32)&gVehDefLastBlock.EcmBlock.unlock_flags;
        break;
    case ECM_checksum_id:
        retv = (u32)&gVehDefLastBlock.EcmBlock.checksum_id;
        break;
    case ECM_finalizing_id:
        retv = (u32)&gVehDefLastBlock.EcmBlock.finalizing_id;
        break;
    case ECM_ecm_id:
        retv = (u32)&gVehDefLastBlock.EcmBlock.ecm_id;
        break;
    case ECM_reponse_ecm_id:
        retv = (u32)&gVehDefLastBlock.EcmBlock.reponse_ecm_id;
        break;
    case ECM_commtype:
        retv = (u32)&gVehDefLastBlock.EcmBlock.commtype;
        break;
    case ECM_commlevel:
        retv = (u32)&gVehDefLastBlock.EcmBlock.commlevel;
        break;
    case ECM_eraseall_cmd:
        retv = (u32)&gVehDefLastBlock.EcmBlock.eraseall_cmd;
        break;
    case ECM_erasecal_cmd:
        retv = (u32)&gVehDefLastBlock.EcmBlock.erasecal_cmd;
        break;
    case ECM_testerpresent_frequency:
        retv = (u32)&gVehDefLastBlock.EcmBlock.testerpresent_frequency;
        break;
    case ECM_os_test_method:
        retv = (u32)&gVehDefLastBlock.EcmBlock.os_test_method;
        break;
    case ECM_maxuploadblocksize:
        retv = (u32)&gVehDefLastBlock.EcmBlock.maxuploadblocksize;
        break;
    case ECM_maxdownloadblocksize:
        retv = (u32)&gVehDefLastBlock.EcmBlock.maxdownloadblocksize;
        break;
    case ECM_data_offset:
        retv = (u32)&gVehDefLastBlock.EcmBlock.data_offset;
        break;
    case ECM_vid_address:
        retv = (u32)&gVehDefLastBlock.EcmBlock.vid_address;
        break;
    case ECM_pats_address:
        retv = (u32)&gVehDefLastBlock.EcmBlock.pats_address;
        break;
    case ECM_vin_address:
        retv = (u32)&gVehDefLastBlock.EcmBlock.vin_address;
        break;
    case ECM_flags:
        retv = (u32)&gVehDefLastBlock.EcmBlock.flags;
        break;
    case ECM_extended_flags:
        retv = (u32)&gVehDefLastBlock.EcmBlock.extended_flags;
        break;
    case ECM_util_upload:
        retv = (u32)&gVehDefLastBlock.EcmBlock.util_upload;
        break;
    case ECM_util_dnload:
        retv = (u32)&gVehDefLastBlock.EcmBlock.util_dnload;
        break;
    case ECM_os_block_read:
        retv = (u32)&gVehDefLastBlock.EcmBlock.os_block_read;
        break;
    case ECM_os_block:
        retv = (u32)&gVehDefLastBlock.EcmBlock.os_block;
        break;
    case ECM_cal_block:
        retv = (u32)&gVehDefLastBlock.EcmBlock.cal_block;
        break;
    default:
        status = S_INPUT;
        break;
    }
    if (status != S_SUCCESS && status != S_UNMATCH)
    {
        /* Error reading ecm_var */
        log_push_error_point(0x4611);
        led_emergency_indicator_system_error();
    }
    return retv;
}

/**
 *  @brief Get Utility Info Variable
 *
 *  @param [in]     ecm_type            ECM Type
 *  @param [in]     utilitychoiceindex  Utility choice index
 *  @param [in]     parameter           Parameter to read
 *
 *  @return Status
 */
u32 vehdef_utivar(u16 ecm_type, u8 utilitychoiceindex, u16 parameter)
{
    u32 retv = 0;
    u16 search_index;
    u8 status;

    switch (parameter)
    {
    case U_UTI_index:
    case U_UTI_filename:
    case U_UTI_flags:
    case U_UTI_call_address:
    case U_UTI_block:
        search_index = ECM_GetUUtiIndex(ecm_type,utilitychoiceindex);
        break;
    case D_UTI_index:
    case D_UTI_filename:
    case D_UTI_flags:
    case D_UTI_call_address:
    case D_UTI_block:
        search_index = ECM_GetDUtiIndex(ecm_type,utilitychoiceindex);
        break;
    default:
        search_index = INVALID_UTI_INDEX;
        break;
    }

    status = vehdef_get_utility_info(ecm_type,search_index);
    
    switch (parameter)
    {
    case U_UTI_index:
        retv = (u32)&gVehDefLastBlock.UtiBlock.index;
        break;
    case U_UTI_filename:
        retv = (u32)&gVehDefLastBlock.UtiBlock.filename;
        break;
    case U_UTI_flags:
        retv = (u32)&gVehDefLastBlock.UtiBlock.flags;
        break;
    case U_UTI_call_address:
        retv = (u32)&gVehDefLastBlock.UtiBlock.call_address;
        break;
    case U_UTI_block:
        retv = (u32)&gVehDefLastBlock.UtiBlock.utility_block;
        break;
    case D_UTI_index:
        retv = (u32)&gVehDefLastBlock.UtiBlock.index;
        break;
    case D_UTI_filename:
        retv = (u32)&gVehDefLastBlock.UtiBlock.filename;
        break;
    case D_UTI_flags:
        retv = (u32)&gVehDefLastBlock.UtiBlock.flags;
        break;
    case D_UTI_call_address:
        retv = (u32)&gVehDefLastBlock.UtiBlock.call_address;
        break;
    case D_UTI_block:
        retv = (u32)&gVehDefLastBlock.UtiBlock.utility_block;
        break;
    default:
        status = S_INPUT;
        break;
    }
    if (status != S_SUCCESS && status != S_UNMATCH)
    {
        /* Error reading uti_var */
        log_push_error_point(0x4612);
        led_emergency_indicator_system_error();
    }
    return retv;
}

/**
 *  @brief Get vehicle type block
 *
 *  @param [in]     veh_type_in         List of vehicle types (stacked or unstacked);
 *                                      If NULL, only read vdf header.
 *  @param [in]     veh_type_in_count   Number of unstacked vehicle types (1 = stacked)
 *  @param [out]    veh_type_out        Vehicle type (stacked)
 *
 *  @return Status
 */
u8 vehdef_get_vehdef_info(u16 *veh_type_in, u8 veh_type_in_count, 
                          u16 *veh_type_out)
{
    VehicleTypeHeader header;
    VehicleTypeBlock veh_type_block;
    F_FILE *pFile;
    bool match;
    u32 bytecount;
    u32 i, j, k;
    u16 ecm_types[ECM_MAX_COUNT];
    u8 count;
    u8 found;
    u8 status;
    
    /* Check if info already in memory */
    if (veh_type_in && veh_type_in_count == 1)
    {
        if (veh_type_in[0] == gVehDefLastBlock.VehBlock.vehicle_type &&
            veh_type_in[0] != INVALID_VEH_DEF)
        {
            if (veh_type_out)
            {
                *veh_type_out = gVehDefLastBlock.VehBlock.vehicle_type;
            }
            return S_SUCCESS;
        }
    }
    
    /* Initialize Defaults */
    memset(ecm_types, INVALID_ECM_DEF, sizeof(ecm_types));
    vehdef_vehblock_init(&gVehDefLastBlock.VehBlock);
    
    pFile = NULL;
    status = vehdef_open_vdf_file(&pFile, &header);
    
    /* Find unstacked/stacked vehicle type info */
    if (status == S_SUCCESS && veh_type_in && veh_type_in_count > 0)
    { 
        status = S_UNMATCH;
        count = 0;
        
        fseek(pFile,header.VehicleTypeRecordOffset,SEEK_SET);
        for (i = 0; i < header.VehicleTypeRecordCount; i++)
        {
            bytecount = fread(&veh_type_block,1,sizeof(VehicleTypeBlock),pFile);
            if (bytecount == sizeof(VehicleTypeBlock))
            {
                crypto_blowfish_decryptblock_critical((u8*)&veh_type_block,sizeof(VehicleTypeBlock));
                
                /* Check for match */
                match = FALSE;
                for (j = 0; j < veh_type_in_count; j++)
                {
                    if (veh_type_block.vehicle_type == veh_type_in[j])
                    {
                        match = TRUE;
                        break;
                    }
                }
                /* Match Found */
                if (match == TRUE)
                {
                    /* Unstacked */
                    if (veh_type_block.ecm_types[0] != INVALID_ECM_DEF &&
                        veh_type_block.ecm_types[1] == INVALID_ECM_DEF && 
                        veh_type_block.ecm_types[2] == INVALID_ECM_DEF)
                    {
                        /* Store ecm types for vehicle type search */
                        ecm_types[j] = veh_type_block.ecm_types[0];
                        if (++count >= veh_type_in_count)
                        {
                            break;
                        }
                    }
                    /* Stacked */
                    else
                    {
                        status = S_SUCCESS;
                        break;
                    }
                }
            }
        }
        /* Find stacked vehicle type by ecm type match */
        if (status == S_UNMATCH && count > 0)
        {
            fseek(pFile,header.VehicleTypeRecordOffset,SEEK_SET);
            for (i = 0; i < header.VehicleTypeRecordCount; i++)
            {
                bytecount = fread(&veh_type_block,1,sizeof(VehicleTypeBlock),pFile);
                if (bytecount == sizeof(VehicleTypeBlock))
                {
                    crypto_blowfish_decryptblock_critical((u8*)&veh_type_block,sizeof(VehicleTypeBlock));
                    
                    /* Search for matching stack */
                    found = 0;
                    for (j = 0; j < count; j++)
                    {
                        for (k = 0; k < count; k++)
                        {
                            if (ecm_types[j] == veh_type_block.ecm_types[k])
                            {
                                found++;
                                break;
                            }
                        }
                        if (found == count)
                        {
                            /* Stacked Match Found */
                            status = S_SUCCESS;
                            break;
                        }
                    }
                    if (status == S_SUCCESS)
                    {
                        break;
                    }
                }
            }
        }
    }
    if (status == S_SUCCESS)
    {
        /* Save last block to memory */
        if (veh_type_in_count == 1)
        {
            memcpy(&gVehDefLastBlock.VehBlock,&veh_type_block,sizeof(VehicleTypeBlock));
        }
        if (veh_type_out)
        {
            *veh_type_out = veh_type_block.vehicle_type;
        }
        #ifdef TESTING_ALLOW_BLANK_VIN
        gVehDefLastBlock.VehBlock.flags |= ALLOW_BLANK_VIN;
        #endif
    }
    
    vehdef_close_vdf_file(pFile);
    
    return status;
}

/**
 *  @brief Check and Validate VDF File
 *
 *  @return Status
 */
u8 vehdef_check_vdf()
{
    VehicleTypeHeader header;
    F_FILE *pFile;
    u8 status;
    
    pFile = NULL;
    status = vehdef_open_vdf_file(&pFile, &header);
    if(status == S_SUCCESS)
    {
        vehdef_close_vdf_file(pFile);
        if (header.oemtype == obd2_get_oemtype())
        {
            status = S_SUCCESS;
        }
        else
        {
            status = S_FAIL;
        }
    }
    
    return status;
}

/**
 *  @brief Initialize Vehicle Def Block Structure
 * 
 *  @return Status
 */ 
static u8 vehdef_vehblock_init(VehicleTypeBlock *block)
{
    if (block)
    {
        memset(block,0,sizeof(VehicleTypeBlock));
        block->vehicle_type = INVALID_VEH_DEF;
    }
    else
    {
        return S_INPUT;
    }
    return S_SUCCESS;    
}

/**
 *  @brief Initialize ECM Def Block Structure
 * 
 *  @return Status
 */ 
static u8 vehdef_ecmblock_init(EcmTypeBlock *block)
{
    if (block)
    {
        memset(block,0,sizeof(EcmTypeBlock));
        block->index = INVALID_ECM_DEF;
        memset(block->util_upload,INVALID_UTI_INDEX,sizeof(block->util_upload));
        memset(block->util_dnload,INVALID_UTI_INDEX,sizeof(block->util_dnload));
    }
    else
    {
        return S_INPUT;
    }
    return S_SUCCESS;    
}

/**
 *  @brief Initialize Utility Def Block Structure
 * 
 *  @return Status
 */ 
static u8 vehdef_utiblock_init(UtilityTypeBlock *block)
{
    if (block)
    {
        memset(block,0,sizeof(UtilityTypeBlock));
        block->index = INVALID_UTI_INDEX;
    }
    else
    {
        return S_INPUT;
    }
    return S_SUCCESS;    
}

/**
 *  @brief Get vehicle definitions header block
 *
 *  @param [in]     info    Vehicle definitions file header
 *
 *  @return Status
 */
static u8 vehdef_get_header_info(VehicleTypeHeader *info)
{
    VehicleTypeHeader header;
    F_FILE *pFile;
    u8 status;
    
    pFile = NULL;
    status = vehdef_open_vdf_file(&pFile, &header);
    if(status == S_SUCCESS)
    {        
        vehdef_close_vdf_file(pFile);
        
        gVehDefLastBlock.vehdefmin = header.minvehdef;
        gVehDefLastBlock.vehdefmax = header.maxvehdef;
        gVehDefLastBlock.vehdefcount = header.VehicleTypeRecordCount;
        gVehDefLastBlock.ecmdefcount = header.EcmTypeRecordCount;
        gVehDefLastBlock.utidefcount = header.UtilityInfoRecordCount;
        
        if (info)
        {
            memcpy(info,&header,sizeof(VehicleTypeHeader));
        }
    }
    return status;
}
   
/**
 *  @brief Get ecm info block
 *
 *  @param [in]     ecm_type        ECM Type to read
 *
 *  @return Status
 */
static u8 vehdef_get_ecm_info(u16 ecm_type)
{
    VehicleTypeHeader header;
    EcmTypeBlock *ecm_type_block;
    F_FILE *pFile;
    u32 bytecount;
    u32 i;
    u8 status;

    /* Check if already in memory */
    if (ecm_type == gVehDefLastBlock.EcmBlock.index)
    {
        return S_SUCCESS;
    }

    /* Initialize Defaults */
    vehdef_ecmblock_init(&gVehDefLastBlock.EcmBlock);
    
    /* Validate other inputs */
    if (ecm_type == INVALID_ECM_DEF)
    {
        return S_UNMATCH;
    }
    
    pFile = NULL;
    status = vehdef_open_vdf_file(&pFile, &header);
    if(status == S_SUCCESS)
    {
        status = S_UNMATCH;
        fseek(pFile,header.EcmTypeRecordOffset,SEEK_SET);
        ecm_type_block = __malloc(sizeof(EcmTypeBlock));
        if (ecm_type_block)
        {
            for (i = 0; i < header.EcmTypeRecordCount; i++)
            {
                bytecount = fread((char*)ecm_type_block,1,sizeof(EcmTypeBlock),pFile);
                if (bytecount == sizeof(EcmTypeBlock))
                {
                    crypto_blowfish_decryptblock_critical((u8*)ecm_type_block,sizeof(EcmTypeBlock));
                    
                    if (ecm_type == ecm_type_block->index)
                    {
                        /* Get ecm type block here */
                        memcpy((char*)&gVehDefLastBlock.EcmBlock,(char*)ecm_type_block,sizeof(EcmTypeBlock));
                        status = S_SUCCESS;
                        break;
                    }
                }
            }
            __free(ecm_type_block);
        }
        else
        {
            status = S_MALLOC;
        }
    }
    
    vehdef_close_vdf_file(pFile);
    
    return status;
}

/**
 *  @brief Get Utility Info Variable
 *
 *  @param [in]     ecm_type        ECM Type
 *  @param [in]     search_index    Utility search index
 *
 *  @return Status
 */
static u8 vehdef_get_utility_info(u16 ecm_type, u16 search_index)
{
    VehicleTypeHeader header;
    UtilityTypeBlock util_type_block;
    F_FILE *pFile;
    u32 bytecount;
    u32 i;
    u8 status;
        
    /* Check if already in memory */
    if (gVehDefLastBlock.UtiBlock.index == search_index)
    {
        return S_SUCCESS;
    }
    
    /* Initialize defaults */
    vehdef_utiblock_init(&gVehDefLastBlock.UtiBlock);
    
    /* Validate other inputs */
    if (search_index == INVALID_UTI_INDEX || ecm_type == INVALID_ECM_DEF)
    {
        return S_UNMATCH;
    }
    
    pFile = NULL;
    status = vehdef_open_vdf_file(&pFile, &header);
    if(status == S_SUCCESS)
    {   
        status = S_UNMATCH;
        fseek(pFile,header.UtilityInfoRecordOffset,SEEK_SET);
        for (i = 0; i < header.UtilityInfoRecordCount; i++)
        {
            bytecount = fread(&util_type_block,1,sizeof(UtilityTypeBlock),pFile);
            if (bytecount == sizeof(UtilityTypeBlock))
            {
                crypto_blowfish_decryptblock_critical((u8*)&util_type_block,sizeof(UtilityTypeBlock));
                
                if (search_index == util_type_block.index)
                {
                    memcpy(&gVehDefLastBlock.UtiBlock,&util_type_block,sizeof(UtilityTypeBlock));
                    status = S_SUCCESS;
                    break;
                }
            }
        }
    }
    
    vehdef_close_vdf_file(pFile);
    
    return status;
}

/**
 *  @brief Open Vehicle Definitions File (*.vdf)
 *
 *  @param [in]     pFile   Pointer to vdf file
 *  @param [out]    header  Vehicle Definitions File Header
 *
 *  @return status
 */
static u8 vehdef_open_vdf_file(F_FILE **pFile, VehicleTypeHeader *header)
{
    char filename[10];
    u8 status;
    
    status = S_SUCCESS;
    
    switch (obd2_get_oemtype())
    {
#ifdef __DCX_MANUF__        
    case OemType_DCX:
        strcpy(filename, "dcx.vdf");
        break;
#endif
#ifdef __FORD_MANUF__
    case OemType_FORD:
        strcpy(filename, "ford.vdf");
        break;
#endif
#ifdef __GM_MANUF__
    case OemType_GM:
        strcpy(filename, "gm.vdf");
        break;
#endif
    default:
        status = S_OPENFILE;
        break;
    } 
    
    /* Open VDF file */
    if (status == S_SUCCESS)
    {
        *pFile = genfs_default_openfile((u8*)filename,"r");
        if (*pFile == NULL)
        {
            status = S_OPENFILE;
        }
    }
    /* Validate VDF file */
    if (status == S_SUCCESS)
    {
        status = vehdef_validate_vdf(*pFile, header);
        if(status != S_SUCCESS)
        {
            *pFile = NULL;
        }
    }
    
    return status;
}

/**
 *  @brief Close Vehicle Definitions File (*.vdf)
 *
 *  @param [in] fptr    File pointer
 */
static void vehdef_close_vdf_file(F_FILE *pFile)
{
    if (pFile)
    {
        genfs_closefile(pFile);
    }
}

/**
 *  @brief Validate vdf file
 *
 *  @param [in]     pFile       VDF file pointer
 *  @param [out]    vtheader    VehicleTypeHeader
 *
 *  @return Status
 */
static u8 vehdef_validate_vdf(F_FILE *pFile, VehicleTypeHeader *header)
{
    u8  buffer[256];
    u32 bytecount;
    u32 crc32e_calc;
    u32 crc32e_cmp;
    u8  status = S_SUCCESS;

    if (pFile == NULL || header == NULL)
    {
        status = S_INPUT;
    }
   
    if (status == S_SUCCESS)
    {       
        fseek(pFile,0,SEEK_SET);
        if (fread(header,1,sizeof(VehicleTypeHeader),pFile) != sizeof(VehicleTypeHeader))
        {
            status = S_READFILE;
        }
    }
    /* Decrypt header */
    if (status == S_SUCCESS)
    {
        status = crypto_blowfish_decryptblock_critical((u8*)header,sizeof(VehicleTypeHeader));
    }
    /* Validate header checksum */
    if (status == S_SUCCESS)
    {
        crc32e_reset();
        crc32e_cmp = header->headerCRC32E;
        header->headerCRC32E = 0xFFFFFFFF;
        crc32e_calc = crc32e_calculateblock(0xFFFFFFFF,(u32*)header,sizeof(VehicleTypeHeader)/4);
        if (crc32e_calc != crc32e_cmp)
        {
            status = S_CRC32E;
        } 
    }
    /* Validate version */
    if (status == S_SUCCESS)
    {
        if (header->version != VDF_FILE_VERSION)
        {
            status = S_FAIL;
        }
    }
    /* Validate content checksum */
    if (status == S_SUCCESS)
    {
        crc32e_reset();
        crc32e_calc = 0xFFFFFFFF;
        while(!feof(pFile))
        {
            bytecount = fread((char*)buffer,1,sizeof(buffer),pFile);
            crc32e_calc = crc32e_calculateblock(crc32e_calc,(u32*)buffer,bytecount/4);    
        }
        if (header->contentCRC32E != 0 && header->contentCRC32E != crc32e_calc)
        {
            status = S_CRC32E;
        }
    }

    return status;
}