/**
  **************** (C) COPYRIGHT 2011 SCT Performance, LLC *********************
  * File Name          : filechecksum.c
  * Device / System    : SCT products using the "CommonCode" platform
  * Author             : Quyen Leba
  *
  * Version            : 1 
  * Date               : 05/02/2011
  * Description        : 
  *                    : 
  *
  *
  * History            : 05/02/2011 M. Davis
  *                    :   Add SCT Copyright header,
  *
  ******************************************************************************
  */

#include <stdio.h>
#include <board/genplatform.h>
#include <fs/genfs.h>
#include <common/statuscode.h>
#include "filechecksum.h"

//------------------------------------------------------------------------------
//  Filecheck sum functions
//  Simple file checksum function used to keep track of changes in certain files
//  10/01/2007 P.D.
//------------------------------------------------------------------------------
u8 fileCheckSumCheck(u8 *filename, u32 checksum)
{
    u32 checksumtemp = 0;
    u8  status = 0;

    status = fileCheckSumCalculate(filename, &checksumtemp);

    if((checksumtemp == checksum) && (status == S_SUCCESS))
    {
        return S_SUCCESS;   // We have a match
    }
    else
    {
        return S_FAIL;      // No match
    }
}

//------------------------------------------------------------------------------
// Input:   u8  *filename
// Output:  u32 *checksum
// Return:  u8  status
//------------------------------------------------------------------------------
u8 fileCheckSumCalculate(u8 *filename, u32 *checksum)
{
    F_FILE  *fileptr = 0;
    u32     bytecount = 0, checksumtemp = 0, i = 0;  
    u8      *databuf;

    databuf = __malloc(1024);
    if(!databuf)
    {
        return S_MALLOC;
    }

    if((fileptr = __fopen((void*)filename,"rb"))== NULL)
    {
        __free(databuf);
        return S_OPENFILE;
    }

    while(f_eof(fileptr) == NULL)
    {
        bytecount = f_read((void*)databuf,1,1024,fileptr);
        for(i=0;i<bytecount;i++)
        {
            checksumtemp += databuf[i]; 
        }
    }

    *checksum = checksumtemp;
    __fclose(fileptr);
    __free(databuf);
    return S_SUCCESS;
}

/*
u8 Checksum_routine(u8 eec_type)
{
  u8 status = 0;
  u8 count =0;
  u8 dnld_type;
  u8 g;
  u32 OSLength =0;

  if(settings.comm_type == VPW_COMM)
    status = UpdateVPWChecksum();
  else if(settings.comm_type == CAN_COMM)
  {
        ShowMessage(NOKEY,NOAUDIO,(void*)*Language->HDR_ProgramVehicleECU,
                (void*)*Language->TXT_NULLSTR,
                (void*)*Language->TXT_Calc_Checksum,
                (void*)*Language->TXT_NULLSTR,
                (void*)*Language->TXT_PleaseWait,CLEAR_TOP);

        for(count =0; count <3; count++)
        {
            dnld_type = EEC_GetDnldType(eec_type,count);
            if(dnld_type == 0xFF)
            {
                break;
            }

            if(count == 0)
            {
                //get processor total image size
                for(g=0; g<5; g++)
                {
                    OSLength += (DNLD_GetOS_Read_BlkSz(dnld_type,g));
                    OSLength += DNLD_GetAddedBlockLength(dnld_type,g);
                }
                OSLength += DNLD_GetOffsetAtStockBin(dnld_type);
            }

            if((dnld_type == 2)||(dnld_type == 3))
                status = LBZLMM_Checksum();
            else if(dnld_type == 12)                
                Allison_checksum(OSLength);
            else if (dnld_type == 0x0D)
            {
                //TODO: -Q- what about E69
                status = S_SUCCESS;
            }
            else
                status = UpdateCANChecksums(eec_type, OSLength, (count+1));
        }
  }
  else
    status = S_FAIL;

  return (status);
}


u8 UpdatesimpleChecksum(u8 start_with_file)
{
  unsigned short checksum = 0;
  unsigned int start;
  unsigned int block_read_end;
  unsigned int offset;
  unsigned char file_checksum[2];
  unsigned short File_Cksm;
  signed int file_size;
  char data[2];
  int i;
  u8 first_pass=1;
  u8 *bp;
  F_FILE  *fileptr = 0;

    G_RAM_Buffer = __malloc(RAM_BUFFER_SIZE);
    if (G_RAM_Buffer == NULL)
    {
        ERROR_Message(MALLOC);
        return S_FAIL;
    }


    if((fileptr = __fopen("/flash.bin","a+"))== NULL)
    {
        ERROR_Message(OPENFILE);
        __free(G_RAM_Buffer);
        return S_FAIL;
    }


    for (i = start_with_file ; i < 8; i++)
	{
		checksum = 0;
        bp = (u8*)G_RAM_Buffer;
		start = block_info[i].start_address;
        file_size = block_info[i].size;
        first_pass = 1;

        f_seek(fileptr,start,SEEK_SET);

         while(file_size > 0)
         {

            
            if(f_read(G_RAM_Buffer, 1, RAM_BUFFER_SIZE,fileptr) < 1 )
            {
                ERROR_Message(READFILE);
                __fclose(fileptr);
                __free(G_RAM_Buffer);
                return S_FAIL;
            }
            bp = (u8*)G_RAM_Buffer;


            if(file_size > RAM_BUFFER_SIZE)
                block_read_end = RAM_BUFFER_SIZE;
            else
                block_read_end = file_size;

            if(first_pass == 1)
            {
                first_pass = 0;
                file_checksum[0] = *bp++;
                file_checksum[1] = *bp++;
                File_Cksm = (file_checksum[0] << 8) + file_checksum[1];
                block_read_end -= 2;
                file_size -= 2;
            }
            
            
		    for (offset = 0; offset < block_read_end; offset = offset + 2)
		    {
                data[0] = bp[offset];
                data[1] = bp[offset+1];            
		        checksum += (unsigned short)((data[0] <<8)+data[1]);
		    }
            file_size -= block_read_end;

        }
		checksum = (unsigned short)( (checksum ^ 0xFFFF) & 0xFFFF );
		checksum++;
        if(File_Cksm != checksum)
        {
            f_seek(fileptr,start,SEEK_SET);
		    data[0] = (char)( (checksum & 0xFF00) >> 8 );
		    data[1] = (char)(checksum & 0xFF);
            f_write(data,1,sizeof(data),fileptr);
        }
	}

    __fclose(fileptr);
    __free(G_RAM_Buffer);
    return S_PASS;
}



u8 Get_VPW_headerinfo(void)
{
    unsigned int partial;
    u8 *ram_buffer = (u8*)__malloc(512);
    u8* byte_ptr = ram_buffer;
    u8 temp[4];
    u8 i, c;
    F_FILE  *fileptr = 0;

    if((fileptr = __fopen("/flash.bin","rb"))== NULL)
    {
        ERROR_Message(OPENFILE);
        __free(ram_buffer);
        return S_FAIL;
    }

    f_seek(fileptr,0x50C,SEEK_SET);         //0x50c is where it sits in file
    if(f_read(ram_buffer, 1, 512,fileptr) < 1 )
    {
        ERROR_Message(READFILE);
        __fclose(fileptr);
        __free(ram_buffer);
        return S_FAIL;
    }

    for(c=0; c<8; c++)
    {
        for(i=0; i<4; i++)
            temp[i] = *byte_ptr++;
        block_info[c].start_address = swap32(temp);
        for(i=0; i<4; i++)
            temp[i] = *byte_ptr++;
        partial = swap32(temp);
        block_info[c].size = (int)(partial - block_info[c].start_address)+1;
    }

    __fclose(fileptr);
    __free(ram_buffer);
    return S_PASS;
}


u8 UpdateVPWChecksum(void)
{
  u8 status =0;

  status = Get_VPW_headerinfo();
  if(status == S_PASS)
    status = UpdatesimpleChecksum(1);
  return (status);
}



u8 UpdateCANChecksums(u8 eec_type, u32 OSLength, u8 proc_number)
{
  signed long length;
  unsigned short crc;
  unsigned int temp =0;
  short offset = 32;
  int start;
  u8 i;
  u8 cheksum[2];
  u8 check_sum_ck[2];
  u8 num_of_files =0;
  u8 start_checksum_file =0;
  u8 CRC_location;
  u8 *bp;
  u8 dnld_type = GM_eec_defs[eec_type - GM_EEC_DEF_START].cal_type[proc_number-1];
  unsigned int File_start_address = GM_dnld_defs[dnld_type].os_block_read[proc_number-1].eec_addr;
  F_FILE  *fileptr = 0;

    G_RAM_Buffer = __malloc(512);
    if (G_RAM_Buffer == NULL)
    {
        ERROR_Message(MALLOC);
        return S_FAIL;
    }


    if((fileptr = __fopen("/flash.bin","a+"))== NULL)
    {
        ERROR_Message(OPENFILE);
        __free(G_RAM_Buffer);
        return S_FAIL;
    }


    bp = (u8*)G_RAM_Buffer;

    switch(dnld_type)
    {
        case 0x00:
            temp = 0x1FF00;     // E40
            num_of_files = 5;
            start_checksum_file = 0;
            break;
        case 0x0A:              // T42
            temp = 0x10024;
            if(proc_number == 2)
              temp += OSLength;
            num_of_files = 3;
            start_checksum_file =0;
            break;
        case 0x0B:              // T43
            temp = 0x02A0C0;
            if(proc_number == 2)
              temp += OSLength;
            num_of_files = 3;
            start_checksum_file =0;
            break;
        default:                // E67 & E38
            temp = 0x10024;
            num_of_files = 6;
            start_checksum_file =0;
            break;
    }

    f_seek(fileptr,temp,SEEK_SET);

    if(f_read(bp, 1, 512,fileptr) < 1 )
    {
        ERROR_Message(READFILE);
        __fclose(fileptr);
        __free(G_RAM_Buffer);
        return S_FAIL;
    }

    block_info[0].start_address = (unsigned int)(bp[0]<<24)+ (bp[1]<<16) + (bp[2]<<8) + bp[3];
    block_info[0].start_address -= File_start_address;
    if(proc_number == 2)
       block_info[0].start_address += OSLength;
    bp += 4;
    temp = (unsigned int)(bp[0]<<24)+ (bp[1]<<16) + (bp[2]<<8) + bp[3];
    temp -= File_start_address;
    if(proc_number == 2)
              temp += OSLength;
    block_info[0].size = (temp - block_info[0].start_address);

    switch(dnld_type)
    {
        case 0x00:
            bp += 0x2F;
            offset = 0x2F;
            CRC_location = 0x1C;
            i=1;
            break;
        case 0x0A:
            bp += 0x38;
            offset = 0x1F;
            CRC_location = 0x1C;
            i=0;
            break;
        case 0x0B:
            bp += 0x1F;
            offset = 0x1F;
            CRC_location = 0x1A;
            i=1;
            break;
        default:
            bp += 0x20;
            offset = 0x1F;
            CRC_location = 0x1C;
            i=1;
            break;
    }

    for(; i<num_of_files; i++)
    {
        block_info[i].start_address = (unsigned int)(bp[0]<<24)+ (bp[1]<<16) + (bp[2]<<8) + bp[3];
        block_info[i].start_address -= File_start_address;
        if(proc_number == 2)
            block_info[i].start_address += OSLength;
        bp += 4;
        temp = (unsigned int)(bp[0]<<24)+ (bp[1]<<16) + (bp[2]<<8) + bp[3];
        temp -= File_start_address;
        if(proc_number == 2)
              temp += OSLength;
        block_info[i].size = (temp - block_info[i].start_address);
        bp += offset;
    }


    for(i=start_checksum_file; i<num_of_files; i++)
    {
        length = block_info[i].size -2;
        start = block_info[i].start_address +2;
        f_seek(fileptr,start,SEEK_SET);
        crc = GMCAN_CalcCRC16(fileptr, length, cheksum, CRC_location);
        check_sum_ck[0] = (unsigned char)(crc & 0xff);
        check_sum_ck[1] = (unsigned char)((crc>>8) & 0xff);
        if((check_sum_ck[0] != cheksum[0]) || (check_sum_ck[1] != cheksum[1]))
        {
            f_seek(fileptr,(start+ CRC_location),SEEK_SET);
            f_write(check_sum_ck,1,sizeof(check_sum_ck),fileptr);
        }
    }

    SimpleCANChecksum(fileptr, 1, num_of_files);

    __fclose(fileptr);
    __free(G_RAM_Buffer);
    return(S_PASS);
}



unsigned short GMCAN_CalcCRC16(F_FILE* fp, signed long uLength, u8* cheksum, u8 CRC_location)
{
  unsigned short crc16 = 0;
  unsigned long offset;
  unsigned long block_read_end;
  u8* Buffer;
  u8* ptr;
  u8 first_pass =1;

    Buffer = __malloc(RAM_BUFFER_SIZE);
    if (G_RAM_Buffer == NULL)
    {
        ERROR_Message(MALLOC);
        return S_FAIL;
    }


    while(uLength > 0)
    {
        ptr = (u8*)Buffer;

        if(f_read(ptr, 1, RAM_BUFFER_SIZE,fp) < 1 )
        {
            ERROR_Message(READFILE);
            __free(Buffer);
            return S_FAIL;
        }

        if(uLength > RAM_BUFFER_SIZE)
            block_read_end = RAM_BUFFER_SIZE;
        else
            block_read_end = (uLength +1);

        if(first_pass == 1)
        {
            cheksum[0] = ptr[CRC_location];
            cheksum[1] = ptr[CRC_location+1];

            first_pass = 0;
                
            for(offset = 0; offset < block_read_end; offset++)
            {

                if((offset !=  CRC_location)&&(offset != (CRC_location+1)))
                {
                    crc16 = (unsigned short)((crc16 >> 8) ^ CRC16Tab[ptr[offset] ^ (crc16 & 0x00FF)]);
                }
            }

        }
        else
        {

            for(offset = 0; offset < block_read_end; offset++)
            {
                crc16 = (unsigned short)((crc16 >> 8) ^ CRC16Tab[ptr[offset] ^ (crc16 & 0x00FF)]);
            }
        }
        uLength -= block_read_end;
    }


    __free(Buffer);
    return crc16;
}

unsigned int swap32(u8 data[4])
{
  unsigned int answer;

	answer = ((swap16(data, 0)) << 16);
    answer += swap16(data, 2);

    return(answer);
}

unsigned int swap16(u8 data[2], u8 offset)
{
  unsigned int answer;

	answer = (unsigned int) (data[offset] << 8);
    answer += data[offset + 1] ;
    
    return(answer);
}



u8 SimpleCANChecksum(F_FILE* fp, u8 start_with_file, u8 end_with_file)
{
  unsigned short checksum = 0;
  unsigned int start;
  unsigned int block_read_end;
  unsigned int offset;
  unsigned short File_Cksm;
  signed int file_size;
  char data[2];
  u8 i;
  u8* ptr;
  u8* Buffer;
  u8 first_pass=1;


    Buffer = __malloc(RAM_BUFFER_SIZE);
    if (G_RAM_Buffer == NULL)
    {
        ERROR_Message(MALLOC);
        return S_FAIL;
    }

    start_with_file--;

    for (i = start_with_file ; i < end_with_file; i++)
	{
		checksum = 0;
		start = block_info[i].start_address;
        file_size = block_info[i].size;
        f_seek(fp,start,SEEK_SET);
        first_pass = 1;

         while(file_size > 0)
         {
            ptr = (u8*)Buffer;
            if(f_read(ptr, 1, RAM_BUFFER_SIZE,fp) < 1 )
            {
                ERROR_Message(READFILE);
                __free(Buffer);
                return S_FAIL;
            }
            if(file_size > RAM_BUFFER_SIZE)
                block_read_end = RAM_BUFFER_SIZE;
            else
                block_read_end = file_size;

            if(first_pass == 1)
            {
                first_pass = 0;
                data[0] = *ptr++;
                data[1] = *ptr++;
                File_Cksm = (data[0] << 8) + data[1];
                block_read_end -= 2;
                file_size -= 2;
            }
            
		    for (offset = 0; offset < block_read_end; offset = offset + 2)
		    {
                data[0] = ptr[offset];
                data[1] = ptr[offset+1];            
		        checksum += (unsigned short)((data[0] <<8)+data[1]);
		    }
            file_size -= block_read_end;

        }
		checksum = (unsigned short)( (checksum ^ 0xFFFF) & 0xFFFF );
		checksum++;
        if(File_Cksm != checksum)
        {
		    data[0] = (char)( (checksum & 0xFF00) >> 8 );
		    data[1] = (char)(checksum & 0xFF);
            f_seek(fp,start,SEEK_SET);
            f_write(data,1,sizeof(data),fp);
        }
	}

    __free(Buffer);
    return S_PASS;
}

//---------------------------------------------------------------------------------------------------------------------------
// Apply checksum in flash.bin (before download it to ECU)
// Engineer: Quyen Leba
// Date: Jan 14, 2008
// Note: checksum signature {0xfa, 0xde, 0xca, 0xfe, 0xca, 0xfe, 0xaf, 0xfe}
//---------------------------------------------------------------------------------------------------------------------------
u8 LBZLMM_Checksum(void)
{
    u32 Flash_BlockAddr;
    u8  signature[8] = {0xfa, 0xde, 0xca, 0xfe, 0xca, 0xfe, 0xaf, 0xfe};
    u32 start_addr,end_addr,checksum_addr;
    u32 Flash_start, ECUstart, Flash_offset;     //where to checksum block start in Flash_
    u8* buffer;
    u32 i;
    u32 tmp;
    u8  j,k, dnld_def;
    
    buffer = __malloc(1024);
    if (buffer == NULL)
    {
        ERROR_Message(MALLOC);
        return S_FAIL;
    }

    Flash_BlockAddr = 0;

        
    dnld_def = EEC_GetDnldType(settings.eec_type,0);
    if(dnld_def == 0xFF)
    {
        __free(buffer);
        return S_FAIL;
    }
    Flash_start = Flash_BlockAddr;

    //checksum for calibration area only (for now)
    for(k=0;k<MAX_CAL_BLOCKS;k++)
    {
        if (DNLD_GetCalBlockSize(dnld_def,k) == 0)
        {
            break;
        }
        
        ECUstart = DNLD_GetCalBlockAddr(dnld_def,k);
        Flash_start = 0 + DNLD_GetOffsetAtStockBin(dnld_def);  //in bytes
        for(i=0; i<MAX_EEC_BLOCKS; i++)
        {
            if ((ECUstart >= DNLD_GetOSBlockAddr(dnld_def,i)) && 
                (ECUstart < DNLD_GetOSBlockAddr(dnld_def,i) + DNLD_GetOSBlockSize(dnld_def,i)))
            {
                Flash_start += ECUstart - DNLD_GetOSBlockAddr(dnld_def,i);
                Flash_offset = Flash_start % 512;
                Flash_start -= Flash_offset;
                Flash_start /= 512;                                //convert to blocks of 512bytes
                break;
            }
            else
            {
                Flash_start += DNLD_GetOSBlockSize(dnld_def,i);
            }
        }
        if (i >= MAX_EEC_BLOCKS)
        {
            __free(buffer);
            return S_FAIL;
        }

#if (DP_X3P)
        Stock_InitDownload(STOCK_USEFLASH_FILE);
        Stock_Forward(Flash_start*512);
        Stock_Read(buffer,512,2,0);
        Stock_DeinitDownload();
#else
#endif
        for(i=0;i<1024;i++)
        {
            for(j=0;j<8;j++)
            {
                if (signature[j] != buffer[i+j])
                {
                    break;
                }
            }
            if (j>=8)   //signature found
            {
                checksum_addr = (buffer[i+12]<<24) | (buffer[i+13]<<16) | (buffer[i+14]<<8) | buffer[i+15];
                end_addr =      (buffer[i-4]<<24) | (buffer[i-3]<<16) | (buffer[i-2]<<8) | buffer[i-1];
                start_addr =    (buffer[i-8]<<24) | (buffer[i-7]<<16) | (buffer[i-6]<<8) | buffer[i-5];
                
                if (
                   (start_addr < ECUstart) || 
                   (start_addr >= (ECUstart + DNLD_GetCalBlockAddr(dnld_def,k))) 
                   || (start_addr >= end_addr) || (checksum_addr < start_addr) || (checksum_addr > end_addr)
                   )
                {
                    __free(buffer);  //invalid checksum block range
                    return S_FAIL;
                }
                tmp = LBZLMM_SetChecksum(start_addr,end_addr,checksum_addr,Flash_start,ECUstart,Flash_offset);  //apply
                if (tmp != 0)
                {
                    tmp = LBZLMM_SetChecksum(start_addr,end_addr,checksum_addr,Flash_start,ECUstart,Flash_offset);  //double check
                }
                if (tmp != 0)
                {
                    __free(buffer);
                    return S_FAIL;
                }
                i+=30;
            }
        }
        //starting address of next calibration block
        Flash_start += DNLD_GetOSBlockSize(dnld_def,k);
    }

    __free(buffer);
    return S_SUCCESS;
}

//---------------------------------------------------------------------------------------------------------------------------
// Apply checksum
// Return: 0 (already checksum), 0xFFFFFFFF(invalid), else (value to correct checksum)
// Note: correct checksum = 0xD01FE500 (little-endian)
// Engineer: Quyen Leba
// Date: Jan 14, 2008
//---------------------------------------------------------------------------------------------------------------------------
u32 LBZLMM_SetChecksum(u32 start_addr, u32 end_addr, u32 checksum_addr, u32 Flash_start, u32 ECUstart, u32 Flash_offset)       // Quyen's checksum routne
{
    u32 len, i;
    u8  *ptr;
    u32 value = 0;
    u32 tmp;
    u32 Flash_BlockAddr;
    u8  *buf;
    u32 offset,index;
    
    Flash_BlockAddr = start_addr - ECUstart + Flash_offset;
    offset = Flash_BlockAddr % 512;
    Flash_BlockAddr -= offset;
    Flash_BlockAddr /= 512;
    Flash_BlockAddr += Flash_start;
    
    len = (end_addr - start_addr +1)/4;
    value = ((end_addr - start_addr +1 +offset)/512) + 1;

    buf = __malloc(1024);
    if (buf == NULL)
    {
        ERROR_Message(MALLOC);
        return S_FAIL;
    }

#if (DP_X3P)
    Stock_InitDownload(STOCK_USEFLASH_FILE);
    Stock_Forward(Flash_BlockAddr*512);
    Stock_Read(buf,512,1,0);
#else
    INVALID_PLATFORM
#endif
    
    index = offset;
    
    ptr = (u8*)(buf + offset);
    
    value = 0;

    for(i=0; i<len; i++)
    {
        tmp = ((u32)ptr[0] << 24) | ((u32)ptr[1] << 16) | ((u32)ptr[2] << 8) | ((u32)ptr[3]);
        ptr += 4;
        value += tmp;
        index += 4;
        if (index >= 512)
        {
#if (DP_X3P)
            Stock_Read(buf,512,1,0);
#else
            INVALID_PLATFORM
#endif
            index = 0;
            ptr = (u8*)buf;
        }
    }
    Stock_DeinitDownload();

    value = 0xD01FE500 - value;

    Flash_BlockAddr = start_addr - ECUstart + Flash_offset;
    Flash_BlockAddr += end_addr - start_addr;
    offset = Flash_BlockAddr % 512;

    offset -= 3;

    Flash_BlockAddr -= offset;
    Flash_BlockAddr /= 512;
    Flash_BlockAddr += Flash_start;
#if (DP_X3P)
    Stock_InitDownload(STOCK_USEFLASH_FILE);
    Stock_Forward(Flash_BlockAddr*512);
    Stock_Read(buf,512,2,0);
    Stock_DeinitDownload();
#else
    INVALID_PLATFORM
#endif

    ptr = (u8*)(buf + offset);
    tmp = ((u32)ptr[0] << 24) | ((u32)ptr[1] << 16) | ((u32)ptr[2] << 8) | ((u32)ptr[3]);
    tmp += value;
    if (value != 0)
    {
        ptr[0] = (tmp >> 24);
        ptr[1] = (tmp >> 16);
        ptr[2] = (tmp >> 8);
        ptr[3] = (tmp & 0xFF);
    }


#if (DP_X3P)
    Stock_UpdateFlashFile(Flash_BlockAddr*512       ,&buf[0],   256);   //256bytes max
    Stock_UpdateFlashFile(Flash_BlockAddr*512+256   ,&buf[256], 256);
    Stock_UpdateFlashFile(Flash_BlockAddr*512+512   ,&buf[512], 256);
    Stock_UpdateFlashFile(Flash_BlockAddr*512+768   ,&buf[768], 256);
#else
    INVALID_PLATFORM
#endif

    
    __free(buf);
    return (value);

}

u8 Allison_checksum(u32 offset)
{
    u8  signature[8] = {0x4A, 0xFC, 0x4A, 0xFC, 0x4A, 0xFC, 0x4A, 0xFC};
    u8* buffer;
    u32 i;
    u8  j;
    u8  data[4];
    u32 flash_sig_addr = 0;
    u32 Flash_start =0;
    u32 length = 0x20000;
    u32 bytes_read = 0;
    u32 start_addr = 0;
    unsigned short crc = 0;
    unsigned short additive_chksm =0;
    unsigned short file_crc = 0;
    unsigned short file_additive = 0;
    u32 CrC_checksum_addr;
    F_FILE* file;
    
    buffer = __malloc(1024);
    if (buffer == NULL)
    {
        ERROR_Message(MALLOC);
        return S_FAIL;
    }

    Flash_start = 0x8000 + offset;
    flash_sig_addr = Flash_start;

    Stock_InitDownload(STOCK_USEFLASH_FILE);
    Stock_Forward(Flash_start);

    while(bytes_read < length)
    {
        Stock_Read(buffer,512,2,0);
        bytes_read += 1024;

        for(i=0;i<1024;i++)
        {
            for(j=0;j<8;j++)
            {
                if (signature[j] != buffer[i+j])
                {
                    break;
                }
            }
            if (j>=8)   //signature found
            {
                file_crc = buffer[i-4]<<8;
                file_crc |= buffer[i-3];
                file_additive = buffer[i-2]<<8;
                file_additive |= buffer[i-1];
                CrC_checksum_addr = flash_sig_addr - 4;
                start_addr = Flash_start;
                Stock_DeinitDownload();
                crc = Allison_Checksums(start_addr, CrC_checksum_addr, &additive_chksm);
                if((file_crc != crc)||(file_additive != additive_chksm))
                {
                    file = __fopen("/flash.bin","a+");
                    f_seek(file,CrC_checksum_addr,SEEK_SET);
                    data[0] = (unsigned char)( (crc & 0xFF00) >> 8 );
		            data[1] = (unsigned char)(crc & 0xFF);
                    data[2] = (unsigned char)( (additive_chksm & 0xFF00) >> 8 );
		            data[3] = (unsigned char)(additive_chksm & 0xFF);
                    f_write(data,1,sizeof(data),file);
                    __fclose(file);
                }
                __free(buffer);
                return(S_SUCCESS);
            }
            else
              flash_sig_addr++;
        }
        
    }

    Stock_DeinitDownload();
    __free(buffer);
    return(S_FAIL);
}

unsigned short Allison_Checksums(u32 start, u32 end, unsigned short* additive_cheksum)
{
    unsigned short crc = 0;
    unsigned short additive_result =0;
    signed long i, bytes_read =0;
    u32 buffer_size = 1024;
    u32 length = end - start;
    F_FILE* file;
    u8* bytes = __malloc(buffer_size);
    if (bytes == NULL)
    {
        ERROR_Message(MALLOC);
        return 0;
    }
    file = __fopen("/flash.bin","rb");
    f_seek(file,start,SEEK_SET);

    while(length > 0)
    {
        if(length < buffer_size)
          buffer_size = length;
        bytes_read = f_read((void*)bytes,1,buffer_size,file);
        if(bytes_read == 0)
            break;
        length -= bytes_read;

        for (i = 0; i < bytes_read; i++)
        {
            crc = (unsigned short)((crc >> 8) ^ CRC16Tab[bytes[i] ^ (crc & 0x00FF)]);
        }

        for(i=0; i<bytes_read; i+=2)
        {
            additive_result += (unsigned short)swap16(&bytes[i], 0);
        }
    }
    additive_result += crc;
    additive_result = (unsigned short)( (additive_result ^ 0xFFFF) & 0xFFFF );
    additive_result++;

    *additive_cheksum = additive_result;
    __fclose(file);
    __free(bytes);
    return(crc);
    
}
*/