/*
 *  Common Code Platform
 *
 *  Copyright 2014 SCT Performance, LLC
 *
 */

/**
 * @file    obd2service.c
 * @brief   OBD2 Services
 * @details This file contains OBD2 enhanced diagnostic services. These services
 *          are shared across multiple vehicle manufacturer OEMs.
 * @date    10/15/2014 
 * @author  Tristen Pierson
 */ 

#include <common/obd2tune.h>
#include <common/statuscode.h>
#include <common/obd2def.h>
#include <common/obd2scp.h>
#include "obd2service.h"

extern obd2_info gObd2info;                         /* from obd2.c */
extern flasher_info *flasherinfo;                   /* from obd2tune.c */

/**
 * @brief       OBD2 Service 0x23 - Read Memory By Address
 * @details     Requests memory data from the server via starting address and
 *              size of memory to read.
 *
 * @param[in]   ecu_id      ECU Identifier
 * @param[in]   address     Memory address to upload from
 * @param[in]   length      Number of bytes to upload
 * @param[out]  data        Data uploaded
 * @param[out]  datalength  Number of bytes uploaded
 *
 * @return      status
 */ 
u8 obd2service_ReadMemoryByAddress(u32 ecu_id, u32 address, u16 length, 
                                   u8 *data, u16 *datalength)
{
    u32 ecm_id = 0x7E0; // TODO: Get this from flasherinfo
    u8 index = 0;       // TODO: Get this from flasherinfo
    u8 status;
    
    /***************************************************************************    
     * OEM Type
     **************************************************************************/
    switch (gObd2info.oemtype)
    {
#ifdef __FORD_MANUF__
    case OemType_FORD:
        /***********************************************************************    
         * Ford Commtype (CAN, SCP, SCP32)
         **********************************************************************/
        switch (gObd2info.ecu_block[index].commtype)
        {
        case CommType_CAN:
            status = obd2can_ford_read_memory_address(ecm_id, address, length,
                                                      data, datalength, FALSE,
                                                      gObd2info.ecu_block[index].commlevel);
            break;
        case CommType_SCP:
        case CommType_SCP32:
            {
                MemoryAddressType addrtype;
                u8 retry = 2;
                
                if (gObd2info.ecu_block[index].commtype == CommType_SCP32)
                {
                    addrtype = Address_32_Bits;
                }
                else
                {
                    addrtype = Address_24_Bits;
                }
                
                do
                {
                    status = obd2scp_read_memory_address(address, length, addrtype,
                                                         data, datalength, TRUE);
                    if (status == S_SUCCESS)
                    {
                        break;
                    }
                } while (retry--);
            }
            break;
        default:
            status = S_INPUT;
            break;
        }
        break;
#endif        
#ifdef __GM_MANUF__    
    case OemType_GM:
        /***********************************************************************    
         * GM Commtype (CAN, VPW)
         **********************************************************************/
        switch (gObd2info.ecu_block[index].commtype)
        {
        case CommType_CAN:
            status = obd2can_gm_read_memory_address(ecm_id, address, length,
                                                    MemorySize_2_Byte,
                                                    data, datalength, FALSE);
            break;
        case CommType_VPW:
            status = S_NOTSUPPORT;
            break;
        default:
            status = S_INPUT;
            break;
        }
        break;
#endif        
#ifdef __DCX_MANUF__    
    case OemType_DCX:
        /***********************************************************************    
         * DCX Commtype (CAN)
         **********************************************************************/
        switch (gObd2info.ecu_block[index].commtype)
        {
        case CommType_CAN:
            status = obd2can_dcx_read_memory_address(ecm_id, address, length,
                                                     MemorySize_2_Byte,
                                                     data, datalength,                                   
                                                     gObd2info.ecu_block[index].commlevel);
            break;
        default:
            status = S_INPUT;
            break;
        }
        break;
#endif        
    default:
        status = S_INPUT;
        break;
    }
    return status;    
}


