/**
  **************** (C) COPYRIGHT 2011 SCT Performance, LLC *********************
  * File Name          : rle.c
  * Device / System    : SCT products using the "CommonCode" platform
  * Author             : Quyen Leba
  *
  * Version            : 1 
  * Date               : 05/02/2011
  * Description        : 
  *                    : 
  *
  *
  * History            : 05/02/2011 M. Davis
  *                    :   Add SCT Copyright header,
  *
  ******************************************************************************
  */

#include <string.h>
#include <board/genplatform.h>
#include <fs/genfs.h>
#include <common/statuscode.h>
#include "rle.h"

#define RLE_MAX_COUNT                   255
#define RLE_BLOCK                       16

typedef struct
{
    u8  count;
    u32 data;
}rle_4byte_node;

typedef struct
{
    u8  count;
    u8  cmpdata[RLE_BLOCK];
    bool cmpdata_available;
    bool compression_inprogress;
}rle_info;

rle_info *rleinfo = NULL;

//------------------------------------------------------------------------------
// Return:  u8  status
// Engineer: Quyen Leba
//------------------------------------------------------------------------------
u8 rle_init()
{
    if (rleinfo == NULL)
    {
        rleinfo = __malloc(sizeof(rle_info));
        if (rleinfo == NULL)
        {
            return S_MALLOC;
        }
    }
    rleinfo->count = 0;
    rleinfo->cmpdata_available = FALSE;
    rleinfo->compression_inprogress = FALSE;
    
    return S_SUCCESS;
}

//------------------------------------------------------------------------------
// Return:  u8  status
// Engineer: Quyen Leba
//------------------------------------------------------------------------------
u8 rle_deinit()
{
    if (rleinfo)
    {
        __free(rleinfo);
        rleinfo = NULL;
    }
    return S_SUCCESS;
}

//------------------------------------------------------------------------------
// Compress a file using run-length encoding (n byte)
// Inputs:  u8  *buffer
//          u16 bufferlength
//          bool islastblock (i.e. no more data to compress after this block)
// Outputs: u8  *compressdata
//          u16 *compressdatalength
// Return:  u8  status
// Engineer: Quyen Leba
// Note: using this function, make sure compressdata is allocated with at least
// twice of bufferlength because worst case of this simple compression can cause
// compressed data double in size of uncompressed data
//------------------------------------------------------------------------------
u8 rle_compressbuffer_nbyte(u8 *buffer, u16 bufferlength, bool islastblock,
                            u8 *compressdata, u16 *compressdatalength)
{
    u16 i;
    u8  *buffer_ptr;
    u32 buffer_blockcount;
    u8  *compressdata_bptr;

    compressdata_bptr = compressdata;
    buffer_ptr = buffer;
    buffer_blockcount = bufferlength / RLE_BLOCK;

    if (rleinfo->cmpdata_available == FALSE)
    {
        memcpy(rleinfo->cmpdata,buffer_ptr,RLE_BLOCK);
        rleinfo->cmpdata_available = TRUE;
    }
    
    for(i=0;i<buffer_blockcount;i++)
    {
        rleinfo->compression_inprogress = TRUE;
        if (memcmp(&buffer_ptr[i*RLE_BLOCK],rleinfo->cmpdata,RLE_BLOCK) == 0)
        {
            rleinfo->count++;
            if (rleinfo->count == RLE_MAX_COUNT)
            {
                *compressdata_bptr++ = rleinfo->count;
                memcpy(compressdata_bptr,rleinfo->cmpdata,RLE_BLOCK);
                compressdata_bptr += RLE_BLOCK;
                rleinfo->count = 0;
                if ((i+1) < buffer_blockcount)
                {
                    memcpy(rleinfo->cmpdata,
                           &buffer_ptr[(i+1)*RLE_BLOCK],RLE_BLOCK);
                }
                rleinfo->compression_inprogress = FALSE;
            }
        }
        else
        {
            *compressdata_bptr++ = rleinfo->count;
            memcpy(compressdata_bptr,rleinfo->cmpdata,RLE_BLOCK);
            compressdata_bptr += RLE_BLOCK;
            rleinfo->count = 1;
            memcpy(rleinfo->cmpdata,&buffer_ptr[(i)*RLE_BLOCK],RLE_BLOCK);
        }
    }
    
    if (rleinfo->compression_inprogress && islastblock)
    {
        *compressdata_bptr++ = rleinfo->count;
        memcpy(compressdata_bptr,rleinfo->cmpdata,RLE_BLOCK);
        compressdata_bptr += RLE_BLOCK;
    }
    
    *compressdatalength = (u16)(compressdata_bptr - compressdata);
    
    return S_SUCCESS;
}

//------------------------------------------------------------------------------
// Compress a file using run-length encoding (n byte)
// Inputs:  u8  *src_filename
//          u8  *dst_filename
// Return:  u8  status
// Engineer: Quyen Leba
//------------------------------------------------------------------------------
u8 rle_compressfile_nbyte(u8 *src_filename, u8 *dst_filename)
{
    F_FILE *src_fptr;
    F_FILE *dst_fptr;
    u8  uncompressedbuffer[(2048/RLE_BLOCK)*RLE_BLOCK];
    u8  compressedbuffer[(2048/RLE_BLOCK)*(RLE_BLOCK+1)];
    u16 compressedbufferlength;
    u32 filesize;
    u32 totalbytecount;
    u16 bytecount;
    u8  status;
    
    status = S_SUCCESS;
    src_fptr = dst_fptr = NULL;
    //open files
    src_fptr = __fopen((char*)src_filename,"rb");
    if (src_fptr == NULL)
    {
        status = S_OPENFILE;
        goto rle_compressfile_nbyte_done;
    }
    dst_fptr = __fopen((char*)dst_filename,"wb");
    if (dst_fptr == NULL)
    {
        status = S_OPENFILE;
        goto rle_compressfile_nbyte_done;
    }
    //get filesize
    if ((f_seek(src_fptr,0,SEEK_END)) != 0)
    {
        status = S_SEEKFILE;
        goto rle_compressfile_nbyte_done;
    }
    filesize = f_tell(src_fptr);
    if ((f_seek(src_fptr,0,SEEK_SET)) != 0)
    {
        status = S_SEEKFILE;
        goto rle_compressfile_nbyte_done;
    }
    //do compression
    totalbytecount = 0;
    while(!f_eof(src_fptr))
    {
        bytecount = f_read(uncompressedbuffer,1,sizeof(uncompressedbuffer),
                           src_fptr);
        totalbytecount += bytecount;
        
        status = rle_compressbuffer_nbyte
            (uncompressedbuffer,bytecount,(bool)(totalbytecount >= filesize),
             compressedbuffer,&compressedbufferlength);
        if (status != S_SUCCESS)
        {
            goto rle_compressfile_nbyte_done;
        }
        
        bytecount = f_write(compressedbuffer,1,compressedbufferlength,
                            dst_fptr);
        if (bytecount != compressedbufferlength)
        {
            status = S_WRITEFILE;
            goto rle_compressfile_nbyte_done;
        }
    }
    
    if (totalbytecount != filesize)
    {
        status = S_BADCONTENT;
//        goto rle_compressfile_nbyte_done;
    }
    
rle_compressfile_nbyte_done:
    if (src_fptr)
    {
        __fclose(src_fptr);
    }
    if (dst_fptr)
    {
        __fclose(dst_fptr);
    }
    return status;
}

























//------------------------------------------------------------------------------
// Compress a file using run-length encoding (1 byte)
// Inputs:  u8  *buffer
//          u16 bufferlength
// Outputs: u8  *compressdata
//          u16 *compressdatalength
// Return:  u8  status
// Engineer: Quyen Leba
// Note: using this function, make sure compressdata is allocated with at least
// twice of bufferlength because worst case of this simple compression can cause
// compressed data double in size of uncompressed data
//------------------------------------------------------------------------------
u8 rle_compressbuffer_1byte(u8 *buffer, u16 bufferlength,
                            u8 *compressdata, u16 *compressdatalength)
{
    u16 i;
    u8  cmpbyte;
    u8  count;
    u8  *compressdata_bptr;
    bool dataavailable;

    compressdata_bptr = compressdata;
    count = 0;
    cmpbyte = buffer[0];
    for(i=0;i<bufferlength;i++)
    {
        dataavailable = TRUE;
        if (buffer[i] == cmpbyte)
        {
            count++;
            if (count == RLE_MAX_COUNT)
            {
                *compressdata_bptr++ = count;
                *compressdata_bptr++ = cmpbyte;
                count = 0;
                if ((i+1) < bufferlength)
                {
                    cmpbyte = buffer[i+1];
                }
                dataavailable = FALSE;
            }
        }
        else
        {
            *compressdata_bptr++ = count;
            *compressdata_bptr++ = cmpbyte;
            count = 1;
            cmpbyte = buffer[i];
        }
    }
    
    if (dataavailable)
    {
        *compressdata_bptr++ = count;
        *compressdata_bptr++ = cmpbyte;
    }
    
    *compressdatalength = (u16)(compressdata_bptr - compressdata);
    
    return S_SUCCESS;
}

//------------------------------------------------------------------------------
// Compress a file using run-length encoding (1 byte)
// Inputs:  u8  *buffer
//          u16 bufferstartposition (where in buffer to start decompression)
//          u16 outputthreshold (must be at least 255)
// Output:  u8  *uncompressdata
//          u16 *uncompressdatalength
//          u16 *bufferendposition
// Return:  u8  status
// Engineer: Quyen Leba
//------------------------------------------------------------------------------
u8 rle_decompressbuffer_1byte(u8 *buffer, u16 bufferlength,
                              u16 bufferstartposition,
                              u16 outputthreshold,
                              u8 *uncompressdata, u16 *uncompressdatalength,
                              u16 *bufferendposition)
{
    u8  *buffer_bptr;
    u8  *uncompressdata_bptr;
    u16 currentposition;
    u16 bytecount;
    
    buffer_bptr = (u8*)&buffer[bufferstartposition];
    uncompressdata_bptr = uncompressdata;
    currentposition = bufferstartposition;
    
    bytecount = 0;
    while((currentposition < bufferlength) &&
          ((bytecount + *buffer_bptr) <= outputthreshold))
    {
        memset((u8*)uncompressdata_bptr,buffer_bptr[1],buffer_bptr[0]);
        bytecount += buffer_bptr[0];
        uncompressdata_bptr += buffer_bptr[0];
        buffer_bptr += 2;
        currentposition += 2;
    }
    
    *uncompressdatalength = bytecount;
    *bufferendposition = currentposition;
    
    return S_SUCCESS;
}

//------------------------------------------------------------------------------
// Compress a file using run-length encoding (1 byte)
// Inputs:  u8  *src_filename
//          u8  *dst_filename
// Return:  u8  status
// Engineer: Quyen Leba
//------------------------------------------------------------------------------
u8 rle_compressfile_1byte(u8 *src_filename, u8 *dst_filename)
{
    F_FILE *src_fptr;
    F_FILE *dst_fptr;
    u8  uncompressedbuffer[2048];
    u8  compressedbuffer[2048*2];
    u16 compressedbufferlength;
    u32 filesize;
    u32 totalbytecount;
    u16 bytecount;
    u8  status;
    
    status = S_SUCCESS;
    src_fptr = dst_fptr = NULL;
    //open files
    src_fptr = __fopen((char*)src_filename,"rb");
    if (src_fptr == NULL)
    {
        status = S_OPENFILE;
        goto rle_compressfile_done;
    }
    dst_fptr = __fopen((char*)dst_filename,"wb");
    if (dst_fptr == NULL)
    {
        status = S_OPENFILE;
        goto rle_compressfile_done;
    }
    //get filesize
    if ((f_seek(src_fptr,0,SEEK_END)) != 0)
    {
        status = S_SEEKFILE;
        goto rle_compressfile_done;
    }
    filesize = f_tell(src_fptr);
    if ((f_seek(src_fptr,0,SEEK_SET)) != 0)
    {
        status = S_SEEKFILE;
        goto rle_compressfile_done;
    }
    //do compression
    totalbytecount = 0;
    while(!f_eof(src_fptr))
    {
        bytecount = f_read(uncompressedbuffer,1,sizeof(uncompressedbuffer),
                           src_fptr);
        totalbytecount += bytecount;
        
        status = rle_compressbuffer_1byte
            (uncompressedbuffer,bytecount,
             compressedbuffer,&compressedbufferlength);
        if (status != S_SUCCESS)
        {
            goto rle_compressfile_done;
        }
        
        bytecount = f_write(compressedbuffer,1,compressedbufferlength,
                            dst_fptr);
        if (bytecount != compressedbufferlength)
        {
            status = S_WRITEFILE;
            goto rle_compressfile_done;
        }
    }
    
    if (totalbytecount != filesize)
    {
        status = S_BADCONTENT;
//        goto rle_compressfile_done;
    }
    
rle_compressfile_done:
    if (src_fptr)
    {
        __fclose(src_fptr);
    }
    if (dst_fptr)
    {
        __fclose(dst_fptr);
    }
    return status;
}

//------------------------------------------------------------------------------
// Decompress a file using run-length encoding (1 byte)
// Inputs:  u8  *src_filename
//          u8  *dst_filename
// Return:  u8  status
// Engineer: Quyen Leba
//------------------------------------------------------------------------------
u8 rle_decompressfile_1byte(u8 *src_filename, u8 *dst_filename)
{
    F_FILE *src_fptr;
    F_FILE *dst_fptr;
    u8  compressedbuffer[2048];
    u16 compressedbufferlength;
    u8  uncompressedbuffer[2048];
    u16 uncompressedbufferlength;
    u32 totalbytecount;
    u16 bytecount;
    
    u16 bufferstartposition;
    u16 bufferendposition;
    u16 outputthreshold;
    
    u8  status;
    
    status = S_SUCCESS;
    src_fptr = dst_fptr = NULL;
    //open files
    src_fptr = __fopen((char*)src_filename,"rb");
    if (src_fptr == NULL)
    {
        status = S_OPENFILE;
        goto rle_decompressfile_done;
    }
    dst_fptr = __fopen((char*)dst_filename,"wb");
    if (dst_fptr == NULL)
    {
        status = S_OPENFILE;
        goto rle_decompressfile_done;
    }
    //do uncompression
    totalbytecount = 0;
    outputthreshold = sizeof(uncompressedbuffer);
    while(!f_eof(src_fptr))
    {
        compressedbufferlength = f_read(compressedbuffer,
                                        1,sizeof(compressedbuffer),src_fptr);
        bufferstartposition = 0;
        bufferendposition = 0;
        while(bufferendposition < compressedbufferlength)
        {
            status = rle_decompressbuffer_1byte
                (compressedbuffer, compressedbufferlength,
                 bufferstartposition, outputthreshold,
                 uncompressedbuffer, &uncompressedbufferlength,
                 &bufferendposition);
            if (status != S_SUCCESS)
            {
                goto rle_decompressfile_done;
            }
            
            bytecount = f_write(uncompressedbuffer,1,uncompressedbufferlength,
                                dst_fptr);
            if (bytecount != uncompressedbufferlength)
            {
                status = S_WRITEFILE;
                goto rle_decompressfile_done;
            }
            
            totalbytecount += bytecount;
            bufferstartposition = bufferendposition;
        }//while(bufferendposition < compressedbufferlength)...
    }//while(!f_eof(src_fptr))...
    
rle_decompressfile_done:
    if (src_fptr)
    {
        __fclose(src_fptr);
    }
    if (dst_fptr)
    {
        __fclose(dst_fptr);
    }
    return status;
}

//------------------------------------------------------------------------------
// Compress a file using run-length encoding (4 byte)
// Inputs:  u8  *buffer
//          u16 bufferlength
// Outputs: u8  *compressdata
//          u16 *compressdatalength
// Return:  u8  status
// Engineer: Quyen Leba
// Note: using this function, make sure compressdata is allocated with at least
// twice of bufferlength because worst case of this simple compression can cause
// compressed data double in size of uncompressed data
//------------------------------------------------------------------------------
u8 rle_compressbuffer_4byte(u8 *buffer, u16 bufferlength,
                            u8 *compressdata, u16 *compressdatalength)
{
    u16 i;
    u32 cmpdata;
    u8  count;
    bool dataavailable;
    u32 *buffer_dwptr;
    u32 buffer_dwptrlength;
    rle_4byte_node *compressednode_ptr;
    u16 nodecount;

    compressednode_ptr = (rle_4byte_node*)compressdata;
    buffer_dwptr = (u32*)buffer;
    buffer_dwptrlength = bufferlength / 4;
    
    count = 0;
    nodecount = 0;
    cmpdata = *buffer_dwptr;
    for(i=0;i<buffer_dwptrlength;i++)
    {
        dataavailable = TRUE;
        if (buffer_dwptr[i] == cmpdata)
        {
            count++;
            if (count == RLE_MAX_COUNT)
            {
                (*compressednode_ptr).count = count;
                (*compressednode_ptr).data = cmpdata;
                compressednode_ptr++;
                nodecount++;
                count = 0;
                if ((i+1) < buffer_dwptrlength)
                {
                    cmpdata = buffer_dwptr[i+1];
                }
                dataavailable = FALSE;
            }
        }
        else
        {
            (*compressednode_ptr).count = count;
            (*compressednode_ptr).data = cmpdata;
            compressednode_ptr++;
            nodecount++;
            count = 1;
            cmpdata = buffer_dwptr[i];
        }
    }
    
    if (dataavailable)
    {
        (*compressednode_ptr).count = count;
        (*compressednode_ptr).data = cmpdata;
        compressednode_ptr++;
        nodecount++;
    }
    
    *compressdatalength = (u16)(nodecount*sizeof(rle_4byte_node));
    
    return S_SUCCESS;
}

//------------------------------------------------------------------------------
// Compress a file using run-length encoding (4 byte)
// Inputs:  u8  *src_filename
//          u8  *dst_filename
// Return:  u8  status
// Engineer: Quyen Leba
//------------------------------------------------------------------------------
u8 rle_compressfile_4byte(u8 *src_filename, u8 *dst_filename)
{
    F_FILE *src_fptr;
    F_FILE *dst_fptr;
    u8  uncompressedbuffer[800];
    u8  compressedbuffer[4000];
    u16 compressedbufferlength;
    u32 filesize;
    u32 totalbytecount;
    u16 bytecount;
    u8  status;
    
    status = S_SUCCESS;
    src_fptr = dst_fptr = NULL;
    //open files
    src_fptr = __fopen((char*)src_filename,"rb");
    if (src_fptr == NULL)
    {
        status = S_OPENFILE;
        goto rle_compressfile_4byte_done;
    }
    dst_fptr = __fopen((char*)dst_filename,"wb");
    if (dst_fptr == NULL)
    {
        status = S_OPENFILE;
        goto rle_compressfile_4byte_done;
    }
    //get filesize
    if ((f_seek(src_fptr,0,SEEK_END)) != 0)
    {
        status = S_SEEKFILE;
        goto rle_compressfile_4byte_done;
    }
    filesize = f_tell(src_fptr);
    if ((f_seek(src_fptr,0,SEEK_SET)) != 0)
    {
        status = S_SEEKFILE;
        goto rle_compressfile_4byte_done;
    }
    //do compression
    totalbytecount = 0;
    while(!f_eof(src_fptr))
    {
        bytecount = f_read(uncompressedbuffer,1,sizeof(uncompressedbuffer),
                           src_fptr);
        totalbytecount += bytecount;
        
        status = rle_compressbuffer_1byte
            (uncompressedbuffer,bytecount,
             compressedbuffer,&compressedbufferlength);
        if (status != S_SUCCESS)
        {
            goto rle_compressfile_4byte_done;
        }
        
        bytecount = f_write(compressedbuffer,1,compressedbufferlength,
                            dst_fptr);
        if (bytecount != compressedbufferlength)
        {
            status = S_WRITEFILE;
            goto rle_compressfile_4byte_done;
        }
    }
    
    if (totalbytecount != filesize)
    {
        status = S_BADCONTENT;
//        goto rle_compressfile_4byte_done;
    }
    
rle_compressfile_4byte_done:
    if (src_fptr)
    {
        __fclose(src_fptr);
    }
    if (dst_fptr)
    {
        __fclose(dst_fptr);
    }
    return status;
}

////------------------------------------------------------------------------------
//// Compress a file using run-length encoding (n byte)
//// Inputs:  u8  *buffer
////          u16 bufferlength
////          bool prev_cmpdata_available
////          u8  *prev_cmpdata (used to join previous data with current data)
////          u16 *prev_count
//// Outputs: u8  *compressdata
////          u16 *compressdatalength
////          u8  *prev_cmpdata (updated)
////          u16 *prev_count
//// Return:  u8  status
//// Engineer: Quyen Leba
//// Note: using this function, make sure compressdata is allocated with at least
//// twice of bufferlength because worst case of this simple compression can cause
//// compressed data double in size of uncompressed data
////------------------------------------------------------------------------------
//u8 rle_compressbuffer_nbyte(u8 *buffer, u16 bufferlength,
//                            bool prev_cmpdata_available,
//                            u8 *prev_cmpdata, u16 *prev_count,
//                            u8 *compressdata, u16 *compressdatalength)
//{
//    u16 i;
//    u8  cmpdata[RLE_BLOCK];
//    u8  count;
//    bool dataavailable;
//    u8  *buffer_ptr;
//    u32 buffer_ptrlength;
//    u8  *compressdata_bptr;
//
//    compressdata_bptr = compressdata;
//    buffer_ptr = buffer;
//    buffer_ptrlength = bufferlength / RLE_BLOCK;
//    
//    if (prev_cmpdata_available)
//    {
//        memcpy(cmpdata,prev_cmpdata,sizeof(cmpdata));
//        count = *prev_count;
//    }
//    else
//    {
//        count = 0;
//        memcpy(cmpdata,buffer_ptr,sizeof(cmpdata));
//    }
//    
//    for(i=0;i<buffer_ptrlength;i++)
//    {
//        dataavailable = TRUE;
//        if (memcmp(&buffer_ptr[i*RLE_BLOCK],cmpdata,sizeof(cmpdata)) == 0)
//        {
//            count++;
//            if (count == RLE_MAX_COUNT)
//            {
//                *compressdata_bptr++ = count;
//                memcpy(compressdata_bptr,cmpdata,sizeof(cmpdata));
//                compressdata_bptr += sizeof(cmpdata);
//                count = 0;
//                if ((i+1) < buffer_ptrlength)
//                {
//                    memcpy(cmpdata,
//                           &buffer_ptr[(i+1)*RLE_BLOCK],sizeof(cmpdata));
//                }
//                dataavailable = FALSE;
//            }
//        }
//        else
//        {
//            *compressdata_bptr++ = count;
//            memcpy(compressdata_bptr,cmpdata,sizeof(cmpdata));
//            compressdata_bptr += sizeof(cmpdata);
//            count = 1;
//            memcpy(cmpdata,&buffer_ptr[(i)*RLE_BLOCK],sizeof(cmpdata));
//        }
//    }
//    
//    if (dataavailable)
//    {
//        *compressdata_bptr++ = count;
//        memcpy(compressdata_bptr,cmpdata,sizeof(cmpdata));
//        compressdata_bptr += sizeof(cmpdata);
//    }
//    
//    *compressdatalength = (u16)(compressdata_bptr - compressdata);
//    
//    return S_SUCCESS;
//}


//------------------------------------------------------------------------------
// Compress a file using run-length encoding (n byte)
// Inputs:  u8  *src_filename
//          u8  *dst_filename
// Return:  u8  status
// Engineer: Quyen Leba
//------------------------------------------------------------------------------
//u8 rle_compressfile_nbyte(u8 *src_filename, u8 *dst_filename)
//{
//    F_FILE *src_fptr;
//    F_FILE *dst_fptr;
//    u8  uncompressedbuffer[RLE_BLOCK*8];
//    u8  compressedbuffer[(RLE_BLOCK+1)*8];
//    u16 compressedbufferlength;
//    u32 filesize;
//    u32 totalbytecount;
//    u16 bytecount;
//    u8  status;
//    
//    status = S_SUCCESS;
//    src_fptr = dst_fptr = NULL;
//    //open files
//    src_fptr = __fopen((char*)src_filename,"rb");
//    if (src_fptr == NULL)
//    {
//        status = S_OPENFILE;
//        goto rle_compressfile_nbyte_done;
//    }
//    dst_fptr = __fopen((char*)dst_filename,"wb");
//    if (dst_fptr == NULL)
//    {
//        status = S_OPENFILE;
//        goto rle_compressfile_nbyte_done;
//    }
//    //get filesize
//    if ((f_seek(src_fptr,0,SEEK_END)) != 0)
//    {
//        status = S_SEEKFILE;
//        goto rle_compressfile_nbyte_done;
//    }
//    filesize = f_tell(src_fptr);
//    if ((f_seek(src_fptr,0,SEEK_SET)) != 0)
//    {
//        status = S_SEEKFILE;
//        goto rle_compressfile_nbyte_done;
//    }
//    //do compression
//    totalbytecount = 0;
//    while(!f_eof(src_fptr))
//    {
//        bytecount = f_read(uncompressedbuffer,1,sizeof(uncompressedbuffer),
//                           src_fptr);
//        totalbytecount += bytecount;
//        
////        status = rle_compressbuffer_nbyte
////            (uncompressedbuffer,bytecount,
////             compressedbuffer,&compressedbufferlength);
//        if (status != S_SUCCESS)
//        {
//            goto rle_compressfile_nbyte_done;
//        }
//        
//        bytecount = f_write(compressedbuffer,1,compressedbufferlength,
//                            dst_fptr);
//        if (bytecount != compressedbufferlength)
//        {
//            status = S_WRITEFILE;
//            goto rle_compressfile_nbyte_done;
//        }
//    }
//    
//    if (totalbytecount != filesize)
//    {
//        status = S_BADCONTENT;
////        goto rle_compressfile_nbyte_done;
//    }
//    
//rle_compressfile_nbyte_done:
//    if (src_fptr)
//    {
//        __fclose(src_fptr);
//    }
//    if (dst_fptr)
//    {
//        __fclose(dst_fptr);
//    }
//    return status;
//}









#define BUCKET_MAX_ITEM_COUNT       256
#define BUCKET_MAX_EXAM_DATA_LENGTH 2048

typedef struct
{
    u32 count;
    u32 value;
}bucket_item;

typedef struct
{
    bucket_item items[BUCKET_MAX_ITEM_COUNT];
    u16 bucket_currentmaxindex;
    u32 maxvalue;
    u16 bucket_currentminindex;
    u32 minvalue;
    u32 itemtvaluethreshold;
}bucket_info;

bucket_info *bucketinfo = NULL;

u8 bbb()
{
    if (bucketinfo == NULL)
    {
        bucketinfo = __malloc(sizeof(bucket_info));
        if (bucketinfo == NULL)
        {
            return S_MALLOC;
        }
    }
    memset(bucketinfo->items,0,sizeof(bucketinfo->items));
    bucketinfo->bucket_currentmaxindex = 0;
    bucketinfo->maxvalue = 0;
    bucketinfo->bucket_currentminindex = 0;
    bucketinfo->minvalue = 0;
    bucketinfo->itemtvaluethreshold = 32;
    
    return S_SUCCESS;
}

u8 xxx(u8 *src_filename)
{
    F_FILE *fptr;
    u8  *databuffer;
    u32 databufferlength;
    u32 *databuffer_dwptr;
    u32 i,j;
    u32 chunk;
    
    databuffer = __malloc(BUCKET_MAX_EXAM_DATA_LENGTH);
    if (databuffer == NULL)
    {
        return S_MALLOC;
    }
    
    fptr = __fopen((char*)src_filename,"rb");
    if (fptr == NULL)
    {
        //TODOQ:
    }
    
    databuffer_dwptr = (u32*)databuffer;
    
    databufferlength = f_read(databuffer,1,BUCKET_MAX_EXAM_DATA_LENGTH,fptr);
    chunk = 1;
    bucketinfo->items[0].value = *databuffer_dwptr++;
    bucketinfo->items[0].count = 1;
    bucketinfo->maxvalue = 1;
    bucketinfo->bucket_currentmaxindex = 0;
    bucketinfo->bucket_currentminindex = 1;
    
    while(1)
    {
        for(;chunk<databufferlength/4;chunk++)
        {
            for(j=0;j<BUCKET_MAX_ITEM_COUNT;j++)
            {
                if (*databuffer_dwptr == bucketinfo->items[j].value)
                {
                    bucketinfo->items[j].count++;
                }
                else
                {
                }
            }
        }
        
        if(!(f_eof(fptr)))
        {
            databufferlength = f_read(databuffer,1,BUCKET_MAX_EXAM_DATA_LENGTH,
                                      fptr);
            chunk = 0;
        }
        else
        {
            break;
        }
    }
    
    return S_SUCCESS;
}
