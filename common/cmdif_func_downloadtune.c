/**
  **************** (C) COPYRIGHT 2011 SCT Performance, LLC *********************
  * File Name          : cmdif_func_downloadtune.c
  * Device / System    : SCT products using the "CommonCode" platform
  * Author             : Quyen Leba
  *
  * Version            : 1 
  * Date               : 05/02/2011
  * Description        : 
  *                    : 
  *
  *
  * History            : 05/02/2011 M. Davis
  *                    :   Add SCT Copyright header,
  *
  ******************************************************************************
  */

//------------------------------------------------------------------------------
// Log error point: 0x4B00 -> 0x4BFF
//------------------------------------------------------------------------------

#include <fs/genfs.h>
#include <board/indicator.h>
#include <board/power.h>
#include <board/delays.h>
#include <common/statuscode.h>
#include <common/settings.h>
#include <common/obd2tune.h>
#include <common/devicedef.h>
#include <common/file.h>
#include <common/filestock.h>
#include <common/cmdif.h>
#include <common/obd2tune_download.h>
#include <common/genmanuf_overload.h>
#include <common/log.h>
#include <common/housekeeping.h>
#include <common/debug/debug_output.h>
#include "cmdif_func_downloadtune.h"

#define DEBUG_FORCE_NO_TUNE_CHANGE                  0   //debug only
#define DEBUG_FORCE_NO_OPTION_CHANGE                0   //debug only
#define DEBUG_FORCE_NO_CHECKSUM_CHANGE              0   //debug only

extern cmdif_datainfo cmdif_datainfo_responsedata;  /* from cmdif.c */
extern flasher_info *flasherinfo;                   /* from obd2tune.c */
extern obd2_info gObd2info;                         /* from obd2.c */

typedef struct
{
    u8  recyclekey_healthcheck          : 1;
    u8  reserved0                       : 7;
    u8  reserved1[3];
}DownloadCompleteInstruction;

//------------------------------------------------------------------------------
// Inputs:  u8  percentage
//          u8  *message (if NULL -> not used)
// Return:  none
// Engineer: Quyen Leba
//------------------------------------------------------------------------------
void downloadtune_filecopy_progressreport(u8 percentage,
                                          u8 *message)
{
    u8  buffer[128+2];
    u8  bufferlength;
    u32 messagelength;
    
    buffer[0] = (u8)percentage;
    bufferlength = 1;
    if (message)
    {
        messagelength = strlen((char*)message);
        if (messagelength < 32)
        {
            strcpy((char*)&buffer[1],(char*)message);
            bufferlength += (messagelength+1);
        }
    }
    //Note: because this function only used in CMDIF_CMD_INIT_DOWNLOAD
    cmdif_internalresponse_ack_nowait
        (CMDIF_CMD_INIT_DOWNLOAD, CMDIF_ACK_PROGRESSBAR, buffer, bufferlength);
}

//------------------------------------------------------------------------------
// A helper to repeat CMDIF_ACK_APPLY_STRATEGY & prevent premature timeout if
// apply strategy process is very long
// Engineer: Quyen Leba
//------------------------------------------------------------------------------
void __apply_strategy_ack_repeater()
{
    cmdif_internalresponse_ack(CMDIF_CMD_INIT_DOWNLOAD,
                                   CMDIF_ACK_APPLY_STRATEGY,NULL,0);
}

//------------------------------------------------------------------------------
// A helper to repeat CMDIF_ACK_APPLY_OPTIONS & prevent premature timeout if
// apply option process is very long
// Engineer: Quyen Leba
//------------------------------------------------------------------------------
void __apply_option_ack_repeater()
{
    cmdif_internalresponse_ack(CMDIF_CMD_INIT_DOWNLOAD,
                               CMDIF_ACK_APPLY_OPTIONS,NULL,0);
}

//------------------------------------------------------------------------------
// A helper to repeat CMDIF_ACK_APPLY_CHECKSUM & prevent premature timeout if
// apply checksum process is very long
// Engineer: Quyen Leba
//------------------------------------------------------------------------------
void __apply_checksum_ack_repeater()
{
    cmdif_internalresponse_ack(CMDIF_CMD_INIT_DOWNLOAD,
                                   CMDIF_ACK_APPLY_CHECKSUM,NULL,0);
}

//------------------------------------------------------------------------------
// Check download condition (stock corrupted, stock already uploaded, etc)
// Use with cmdif_func_downloadtune_do if this function is successful
// Input:   u8  *tuneimagefilename (name of tune image file to download, only
//              used in case of return to stock; for now)
// Return:  u8  status
// Note: status does not include negative response from cmdif, therefore,
// negative response is handled outside of this function
// Engineer: Patrick Downs, Quyen Leba
//------------------------------------------------------------------------------
u8 cmdif_func_downloadtune_init()
{
    u8  i;
    u8  status;
    cmdif_upl_dnl_init_flags downloadinitflags;
    u16 ecm_type;
    u32 position;
    u32 length; 
    
    debug_elapsedtime_start();

    housekeeping_removeitem(HouseKeepingType_VppTimeout);
    status = S_SUCCESS;

    button_lock(TRUE);
    indicator_clear();
    indicator_link_status();

    cmdif_datainfo_responsedata.responsecode = CMDIF_ACK_FAILED;
    if (flasherinfo == NULL)
    {
        log_push_error_point(0x4B00);
        status = S_BADCONTENT;
        goto cmdif_func_downloadtune_init_done;
    }
    else if ((!(flasherinfo->flags & FLASHERFLAG_VALID_VEHICLE_INFO)) &&
             flasherinfo->flashtype != CMDIF_ACT_STOCK_FLASHER)
    {
        log_push_error_point(0x4B12);
        status = S_BADCONTENT;
        goto cmdif_func_downloadtune_init_done;
    }
    else if (SETTINGS_IsNoPreloadedDevice() &&
             flasherinfo->flashtype == CMDIF_ACT_PRELOADED_FLASHER)
    {
        log_push_error_point(0x4B13);
        status = S_NOTSUPPORT;
        goto cmdif_func_downloadtune_init_done;
    }
    else if ((SETTINGS_IsNoCustomDevice() || SETTINGS_Is50StateLegalDevice()) &&
             flasherinfo->flashtype == CMDIF_ACT_CUSTOM_FLASHER)
    {
        log_push_error_point(0x4B14);
        status = S_NOTSUPPORT;
        goto cmdif_func_downloadtune_init_done;
    }

    cmdif_tracker_set_state(CMDIF_TRACKER_STATE_LISTENING);

    if (flasherinfo->flashtype == CMDIF_ACT_PRELOADED_FLASHER)
    {
        // Compare the partnumber/strategy in vehicle 
        // Preloaded only checked if married
        if(SETTINGS_IsMarried())
        {
            obd2tune_compare_vehiclecodes_with_setting(flasherinfo);
        }
        
        //make sure vehicle_type is within range
        if (obd2_validate_ecmbinarytype(flasherinfo->vehicle_type) != S_SUCCESS)
        {
            log_push_error_point(0x4B19);
            status = S_ERROR;
            goto cmdif_func_downloadtune_init_done;
        }

        //#############################
        // Step 1: copy stock to flash
        //#############################
        //report "CMDIF_ACK_BUILDING_TUNE" initiated
        cmdif_internalresponse_ack(CMDIF_CMD_INIT_DOWNLOAD,
                                   CMDIF_ACK_BUILDING_TUNE,NULL,0);
        status = S_SUCCESS;
        if (!SETTINGS_IsDemoMode())
        {
            file_info basefilestockinfo[ECM_MAX_COUNT];

            memset((char*)basefilestockinfo,0,sizeof(basefilestockinfo));
            for(i=0;i<ECM_MAX_COUNT;i++)
            {
                ecm_type = VEH_GetEcmType(flasherinfo->vehicle_type,i);
                if (ecm_type != INVALID_ECM_DEF)
                {
                    status = obd2tune_getecmsize_byecmtype(ecm_type,&length);
                    if (status != S_SUCCESS)
                    {
                        log_push_error_point(0x4B27);
                        cmdif_datainfo_responsedata.responsecode = CMDIF_ACK_FAILED;
                        goto cmdif_func_downloadtune_init_done;
                    }
                    basefilestockinfo[i].filesize = length;
                }
                basefilestockinfo[i].filename = flasherinfo->basestockfilename[i];
                basefilestockinfo[i].filecrc32e = flasherinfo->basestockcrc32e[i];
            }
            status = filestock_generate_flashfile_fromstock_advance(basefilestockinfo,
                                                                    downloadtune_filecopy_progressreport);
//            status = filestock_generate_flashfile_fromstock(downloadtune_filecopy_progressreport);
        }
        if (status != S_SUCCESS)
        {
            log_push_error_point(0x4B01);
            cmdif_datainfo_responsedata.responsecode = CMDIF_ACK_FILECOPY_FAIL;
            goto cmdif_func_downloadtune_init_done;
        }

        //#############################
        // Step 2: apply preloaded tune
        //#############################
        cmdif_internalresponse_ack(CMDIF_CMD_INIT_DOWNLOAD,
                                   CMDIF_ACK_APPLY_STRATEGY,NULL,0);
        status = S_SUCCESS;
#if DEBUG_FORCE_NO_TUNE_CHANGE
        //do nothing
#else
        if (!SETTINGS_IsDemoMode())
        {
            //prevent premature timeout
            if(tasktimer_setup(15000, TASKTIMER_REPEAT, TaskTimerTaskType_PreventAppTimeout,
                               (TimerTaskFunc)&__apply_strategy_ack_repeater, NULL) != S_SUCCESS)
            {
                log_push_error_point(0x4B20);
                cmdif_datainfo_responsedata.responsecode = CMDIF_ACK_FAILED;
                goto cmdif_func_downloadtune_init_done;
            }

            // TODOPD: Stack overlow in this function
            status = obd2tune_apply_preloadedtune(flasherinfo->vehicle_type,
                                                  &flasherinfo->selected_tunelistinfo);
            if(tasktimer_setup(0, TASKTIMER_REPEAT, TaskTimerTaskType_PreventAppTimeout, NULL, NULL) != S_SUCCESS)
            {
                log_push_error_point(0x4B21);
                cmdif_datainfo_responsedata.responsecode = CMDIF_ACK_FAILED;
                goto cmdif_func_downloadtune_init_done;
            }
        }
#endif
        if (status != S_SUCCESS)
        {
            log_push_error_point(0x4B02);
            cmdif_datainfo_responsedata.responsecode = CMDIF_ACK_STRAGEY_CHANGES_FAIL;
            goto cmdif_func_downloadtune_init_done;
        }
    }
    else if (flasherinfo->flashtype == CMDIF_ACT_CUSTOM_FLASHER)
    {
        // Compare the partnumber/strategy in vehicle Setup Full Flash Flags
        obd2tune_compare_vehiclecodes_with_tuneosdata(flasherinfo);

        //make sure vehicle_type is within range
        if (obd2_validate_ecmbinarytype(flasherinfo->vehicle_type) != S_SUCCESS)
        {
            log_push_error_point(0x4B1A);
            status = S_ERROR;
            goto cmdif_func_downloadtune_init_done;
        }

        //#############################
        // Step 1: copy tune to flash
        //#############################
        status = S_SUCCESS;
        if (!SETTINGS_IsDemoMode())
        {
            status = filestock_generate_flashfile_fromfile
                (flasherinfo->selected_tunelistinfo.tunefilename[0],    //TODOQ: ~~~ correction done
                 downloadtune_filecopy_progressreport);
            
            ecm_type = VEH_GetEcmType(flasherinfo->vehicle_type,0); 
            //overlay flashfile
            if(ECM_IsOverlayFlashRequired(ecm_type) && status == S_SUCCESS)
            {
                position = ECM_GetOSReadBlockAddr(ecm_type,0);
                length = ECM_GetCalBlockAddr(ecm_type,0);
                status = filestock_overlay_flashfile(position, length,
                                                     downloadtune_filecopy_progressreport);
            }
        }
        if (status != S_SUCCESS)
        {
            log_push_error_point(0x4B03);
            cmdif_datainfo_responsedata.responsecode = CMDIF_ACK_FILECOPY_FAIL;
            goto cmdif_func_downloadtune_init_done;
        }        
    }
    else if (flasherinfo->flashtype == CMDIF_ACT_STOCK_FLASHER)
    {
        flasherinfo->flags |= FLASHERFLAG_SKIP_APPLY_OPTIONS;
        flasherinfo->flags |= FLASHERFLAG_SKIP_APPLY_CHECKSUM;
        flasherinfo->flags |= FLASHERFLAG_SKIP_OVERLAY;
        flasherinfo->flags |= FLASHERFLAG_CLEAR_MARRIED_STATUS;
        if (SETTINGS_IsDownloadFail())
        {
            flasherinfo->flags |= FLASHERFLAG_SKIP_MARRIED_CHECK;
            flasherinfo->flags |= FLASHERFLAG_SKIP_MARRIED_COUNT;

            if (SETTINGS_IsStockRecovery())
            {
                flasherinfo->flags |= FLASHERFLAG_RETURNSTOCK_RECOVERY;
            }
        }
        flasherinfo->uploadstock_required = FALSE;
        flasherinfo->vehicle_type = SETTINGS_TUNE(veh_type);
        flasherinfo->ecm_count =  SETTINGS_TUNE(processor_count);
        
        // Do Force Full Flash when IsForceStockFirst flag set on return to stock
        if(ECM_IsForceStockFirst(VEH_GetMainEcmType(flasherinfo->vehicle_type)))
        {
            SETTINGS_SetFullFlash(0);
        }

        flasherinfo->isfullreflash_already_set = TRUE;
        for(i=0;i<flasherinfo->ecm_count;i++)
        {
            flasherinfo->isfullreflash[i] = FALSE;
            if (SETTINGS_IsFullFlash(i))
            {
                flasherinfo->isfullreflash[i] = TRUE;
            }
        }
    }
    else
    {
        log_push_error_point(0x4B05);
        status = S_BADCONTENT;
        goto cmdif_func_downloadtune_init_done;
    }
    
    //#############################
    // Apply options
    //#############################
    if (!(flasherinfo->flags & FLASHERFLAG_SKIP_APPLY_OPTIONS))
    {
        //report "CMDIF_ACK_APPLY_OPTIONS" initiated
        cmdif_internalresponse_ack(CMDIF_CMD_INIT_DOWNLOAD,
                                   CMDIF_ACK_APPLY_OPTIONS,NULL,0);

        status = S_SUCCESS;
#if DEBUG_FORCE_NO_OPTION_CHANGE
        //do nothing
#else
        if (!SETTINGS_IsDemoMode())
        {
            //prevent premature timeout
            if(tasktimer_setup(15000, TASKTIMER_REPEAT, TaskTimerTaskType_PreventAppTimeout,
                            (TimerTaskFunc)&__apply_option_ack_repeater, NULL) != S_SUCCESS)
            {
                log_push_error_point(0x4B22);
                cmdif_datainfo_responsedata.responsecode = CMDIF_ACK_FAILED;
                goto cmdif_func_downloadtune_init_done;
            }

            status = obd2tune_apply_options(flasherinfo->flashtype,
                                            flasherinfo->vehicle_type,
                                            &flasherinfo->selected_tunelistinfo);
            if(tasktimer_setup(0, TASKTIMER_REPEAT, TaskTimerTaskType_PreventAppTimeout, NULL, NULL) != S_SUCCESS)
            {
                log_push_error_point(0x4B23);
                cmdif_datainfo_responsedata.responsecode = CMDIF_ACK_FAILED;    
                goto cmdif_func_downloadtune_init_done;
            }
        }
#endif
        if (status != S_SUCCESS)
        {
            log_push_error_point(0x4B06);
            //TODOQ: need a better ACK code
            cmdif_datainfo_responsedata.responsecode = CMDIF_ACK_FAILED;
            goto cmdif_func_downloadtune_init_done;
        }
    }
    
    //#############################
    // Save overlay data
    //#############################
    if (flasherinfo->uploadstock_required == TRUE)
    {
        //if VIN blank or invalid, generate a VIN
        status = obd2_validatevin(flasherinfo->vin);
        if (status != S_SUCCESS)
        {
//            if (status == S_VINBLANK && VEH_IsAllowBlankVIN(flasherinfo->vehicle_type))
//            {
//                //do nothing: should only used in development
//                status = S_SUCCESS;
//            }
//            else
//            {
                if(VEH_IsRequireOverlay(flasherinfo->vehicle_type))
                {
                    obd2_generatevin(flasherinfo->vin);
                    status = S_SUCCESS;
                }
                else if ((status == S_VINBLANK || status == S_VININVALID) &&
                         ECM_IsAllowIndirectBlankVINOverlay(VEH_GetEcmType(flasherinfo->vehicle_type,0)) &&
                         ECM_IsValidVINsAddress(VEH_GetEcmType(flasherinfo->vehicle_type,0)))
                {
                    //we can place VIN in this vehicle in some non-critical area
                    //but not available through regular VIN read method (must
                    //use $23 with our defined VIN address)
                    //VIN is placed in flash.bin prior to download
                    obd2_generatevin(flasherinfo->vin);
                    status = S_SUCCESS;
                }
                else
                {
                    // ERROR: ISO14229 can't have a blank VIN if overlay is not required
                    // TODO: We may need to support this for Ford Racing PCMs w/o VINs
                    // Add compiler switch statement here to enable/disable support
                    status = S_FAIL;
                    log_push_error_point(0x4B17); 
                    cmdif_datainfo_responsedata.responsecode = CMDIF_ACK_FAILED; // TODO Better ACK for this
                    goto cmdif_func_downloadtune_init_done;
                }
//            }
        }
        
        //save overlay data
        status = S_SUCCESS;
        if (!SETTINGS_IsDemoMode())
        {
            status = obd2tune_handle_overlaydata(flasherinfo->vehicle_type,
                                                 flasherinfo->vin,FALSE);
        }
        if (status != S_SUCCESS && status != S_NOTREQUIRED)
        {
            log_push_error_point(0x4B08);
            cmdif_datainfo_responsedata.responsecode = CMDIF_ACK_MARRIED_FAIL;
            goto cmdif_func_downloadtune_init_done;
        }
    }
    
    //#############################
    // Apply overlay
    //#############################
    if (!(flasherinfo->flags & FLASHERFLAG_SKIP_OVERLAY))
    {
        u8  overlay_vin[VIN_LENGTH+1];

        if (flasherinfo->uploadstock_required)
        {
            memcpy((char*)overlay_vin,flasherinfo->vin,sizeof(overlay_vin));
        }
        else
        {
            memcpy((char*)overlay_vin,SETTINGS_TUNE(vin),sizeof(overlay_vin));
        }
               
        //apply overlay data
        if (!SETTINGS_IsDemoMode())
        {
            status = obd2tune_handle_overlaydata(flasherinfo->vehicle_type,
                                                 overlay_vin,TRUE);
            if (status != S_SUCCESS && status != S_NOTREQUIRED)
            {
                log_push_error_point(0x4B0F);
                cmdif_datainfo_responsedata.responsecode = CMDIF_ACK_FAILED;    
                goto cmdif_func_downloadtune_init_done;
            }
        }
    }
    
    //#############################
    // Apply checksum
    //#############################
    if (!(flasherinfo->flags & FLASHERFLAG_SKIP_APPLY_CHECKSUM))
    {
        //report "CMDIF_ACK_APPLY_CHECKSUM" initiated
        cmdif_internalresponse_ack(CMDIF_CMD_INIT_DOWNLOAD,
                                   CMDIF_ACK_APPLY_CHECKSUM,NULL,0);
        status = S_SUCCESS;
#if DEBUG_FORCE_NO_CHECKSUM_CHANGE
        //do nothing
#else
        if (!SETTINGS_IsDemoMode())
        {
            //prevent premature timeout
            if(tasktimer_setup(15000, TASKTIMER_REPEAT, TaskTimerTaskType_PreventAppTimeout,
                            (TimerTaskFunc)&__apply_checksum_ack_repeater, NULL) != S_SUCCESS)
            {
                log_push_error_point(0x4B24);
                cmdif_datainfo_responsedata.responsecode = CMDIF_ACK_FAILED;
                goto cmdif_func_downloadtune_init_done;
            }

            status = obd2tune_apply_checksum(flasherinfo->vehicle_type,
                                             &flasherinfo->selected_tunelistinfo);
            if(tasktimer_setup(0, TASKTIMER_REPEAT, TaskTimerTaskType_PreventAppTimeout, NULL, NULL) != S_SUCCESS)
            {
                log_push_error_point(0x4B25);
                cmdif_datainfo_responsedata.responsecode = CMDIF_ACK_FAILED;
                goto cmdif_func_downloadtune_init_done;
            }
        }
#endif
        if (status != S_SUCCESS)
        {
            log_push_error_point(0x4B07);
            //TODOQ: need a better ACK code
            cmdif_datainfo_responsedata.responsecode = CMDIF_ACK_FAILED;
            goto cmdif_func_downloadtune_init_done;
        }
    }

    //##########################################################################
    // Download check (checking for download support files)
    //##########################################################################
    if (VEH_IsCheckPreDownload(flasherinfo->vehicle_type))
    {
        u8 response = 0;
        bool support_files = TRUE;
        status = obd2tune_do_check(&response, flasherinfo->vehicle_type, support_files);
        if(status != S_SUCCESS)
        {
            log_push_error_point(0x4B04);
            cmdif_datainfo_responsedata.responsecode = CMDIF_ACK_FAILED;
            status = S_FAIL;
            goto cmdif_func_downloadtune_init_done;
        }

    }

    //#############################
    // Validate stock file
    //#############################
    status = obd2tune_checkstockfile(flasherinfo->vehicle_type);
    if (status != S_SUCCESS)
    {
        log_push_error_point(0x4B0A);
        cmdif_datainfo_responsedata.responsecode = CMDIF_ACK_FILESIZE_MISMATCH;
        goto cmdif_func_downloadtune_init_done;
    }

    //#############################
    // Set married, if just upload
    //#############################
    if (flasherinfo->uploadstock_required == TRUE)
    {
        //report "CMDIF_ACK_MARRYING_VEHICLE" initiated
        cmdif_internalresponse_ack(CMDIF_CMD_INIT_DOWNLOAD,
                                   CMDIF_ACK_MARRYING_VEHICLE,NULL,0);
        
        obd2_vehicle_specific_task_before_program(flasherinfo);
        
        status = obd2tune_setmarried(flasherinfo->vehicle_type,
                                     flasherinfo->tunebytecount,
                                     flasherinfo->vehicle_serial_number);
        if (status != S_SUCCESS)
        {
            log_push_error_point(0x4B09);
            cmdif_datainfo_responsedata.responsecode = CMDIF_ACK_MARRIED_FAIL;
            goto cmdif_func_downloadtune_init_done;
        }
    }//if (flasherinfo->uploadstock_required == TRUE)...
       
    status = S_SUCCESS;
    cmdif_datainfo_responsedata.responsecode = CMDIF_ACK_OK;
    
    ecm_type = VEH_GetEcmType(flasherinfo->vehicle_type,0); 
    
    // Setup Download Init Flags
    memset(&downloadinitflags, 0, sizeof(downloadinitflags));
    if(ECM_IsKeyOffPowerDown(ecm_type))
    {
        downloadinitflags.keyoffpowerdownrequired = 1;
        downloadinitflags.powerdowntime = ECM_GetPowerDownTime(ecm_type);
    }
    downloadinitflags.veh_type = flasherinfo->vehicle_type;
    
    // Send Download init flags
    cmdif_datainfo_responsedata.dataptr = __malloc(sizeof(downloadinitflags));
    if(!cmdif_datainfo_responsedata.dataptr)
    {
        cmdif_datainfo_responsedata.responsecode = CMDIF_ACK_FAILED;
        status = S_MALLOC;
    }
    else
    {
        memcpy(cmdif_datainfo_responsedata.dataptr,&downloadinitflags,sizeof(downloadinitflags));
        cmdif_datainfo_responsedata.datalength = sizeof(downloadinitflags);
    }
    
    housekeeping_additem(HouseKeepingType_VppTimeout,  3000);
    
cmdif_func_downloadtune_init_done:
    cmdif_tracker_set_state(CMDIF_TRACKER_STATE_IN_PROGRESS);
    if (cmdif_datainfo_responsedata.responsecode != CMDIF_ACK_OK)
    {
        power_setvpp(Vpp_OFF);
        obd2_garbagecollector();
    }
    
    button_lock(FALSE);
    
    debug_reportstatus(status, "Download Init");
    debug_elapsedtime_stop();
    
    return status;
}

//------------------------------------------------------------------------------
// Handle download tune
// Return:  u8  status (including fail statuscode for negative response)
// Note: status does include negative response from cmdif; this means negative
// response is handled internally here and status equivalent to negative
// response
// Engineer: Patrick Downs, Quyen Leba
//------------------------------------------------------------------------------
u8 cmdif_func_downloadtune_do()
{
    u8  status;
    DownloadCompleteInstruction downloadcompleteinstruction;
    u32 errornumbercode;

    debug_elapsedtime_start();
    memset((char*)&downloadcompleteinstruction,0,sizeof(downloadcompleteinstruction));

    housekeeping_removeitem(HouseKeepingType_VppTimeout);

    errornumbercode = 0;
    
    button_lock(TRUE);
    indicator_set(Indicator_DownloadTune_Programming);
    indicator_set_normaloperation(FALSE); // Lock LED
    
    if (flasherinfo == NULL)
    {
        log_push_error_point(0x4B0B);
        cmdif_internalresponse_ack(CMDIF_CMD_DO_DOWNLOAD,
                                   CMDIF_ACK_FAILED,NULL,0);
        
        status = S_BADCONTENT;
        goto cmdif_func_downloadtune_do_done;
    }
    else if (SETTINGS_IsNoPreloadedDevice() &&
             flasherinfo->flashtype == CMDIF_ACT_PRELOADED_FLASHER)
    {
        log_push_error_point(0x4B15);
        //report "NoPreloadedDevice" incompatible w/ flasherinfo->flashtype
        cmdif_internalresponse_ack(CMDIF_CMD_DO_DOWNLOAD,
                                   CMDIF_ACK_FAILED,NULL,0);
        
        status = S_NOTSUPPORT;
        goto cmdif_func_downloadtune_do_done;
    }
    else if ((SETTINGS_IsNoCustomDevice() || SETTINGS_Is50StateLegalDevice()) &&
             flasherinfo->flashtype == CMDIF_ACT_CUSTOM_FLASHER)
    {
        log_push_error_point(0x4B16);
        //report "NoCustomDevice" incompatible w/ flasherinfo->flashtype
        cmdif_internalresponse_ack(CMDIF_CMD_DO_DOWNLOAD,
                                   CMDIF_ACK_FAILED,NULL,0);
        
        status = S_NOTSUPPORT;
        goto cmdif_func_downloadtune_do_done;
    }
    
    //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    // Check if vehicle & device are healthy for programming
    //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    status = obd2tune_checkprogrammingcondition();
    if (status == S_LOWVBATT)
    {
        log_push_error_point(0x4B0C);
        cmdif_internalresponse_ack(CMDIF_CMD_DO_DOWNLOAD,
                                   CMDIF_ACK_VBATT_LOW,NULL,0);
        
        goto cmdif_func_downloadtune_do_done;
    }
    else if (status == S_LOWVPP)
    {
        log_push_error_point(0x4B0D);
        cmdif_internalresponse_ack(CMDIF_CMD_DO_DOWNLOAD,
                                   CMDIF_ACK_VPP_LOW,NULL,0);
        
        goto cmdif_func_downloadtune_do_done;
    }
    else if (status != S_SUCCESS)
    {
        //report other obd2tune_checkprogrammingcondition() fail
        log_push_error_point(0x4B0E);
        cmdif_internalresponse_ack(CMDIF_CMD_DO_DOWNLOAD,
                                   CMDIF_ACK_FAILED,NULL,0);
        
        goto cmdif_func_downloadtune_do_done;
    }

    cmdif_tracker_set_state(CMDIF_TRACKER_STATE_LISTENING);

    //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    // Check married
    //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    if (flasherinfo->uploadstock_required == FALSE)
    {
        if (!(flasherinfo->flags & FLASHERFLAG_SKIP_MARRIED_CHECK))
        {
            u8  keycheckcount;

            keycheckcount = 0;
            if (!SETTINGS_IsDemoMode())
            {
                while(1)
                {
                    status = obd2_checkkeyon(CommType_Unknown, TRUE);
                    if (status == S_SUCCESS)
                    {
                        break;
                    }
                    else if (keycheckcount++ > 100)
                    {
                        //timeout
                        log_push_error_point(0x4B1B);
                        errornumbercode = 10464;
                        cmdif_internalresponse_ack(CMDIF_CMD_DO_DOWNLOAD,
                                                   CMDIF_ACK_MARRIED_FAIL,
                                                   (u8*)&errornumbercode,4);
                        goto cmdif_func_downloadtune_do_done;
                    }
                    //send request key on message 
                    cmdif_internalresponse_ack(CMDIF_CMD_DO_DOWNLOAD,
                                               CMDIF_ACK_KEY_ON,NULL,0);
                    delays(300,'m');
                }//while(1)...
            }//if (!SETTINGS_IsDemoMode())...

            //for security reasons, obd2tune_checkmarried also reads vin and
            //vehicle serial number, update flasherinfo with these info
            //Note: if doing a return stock, this is the 1st time flasherinfo
            //gets these info
            cmdif_internalresponse_ack(CMDIF_CMD_DO_DOWNLOAD,
                                       CMDIF_ACK_CHECK_MARRIED,NULL,0);
            
            status = obd2tune_checkmarried(flasherinfo->vehicle_type,
                                           flasherinfo->vin,
                                           flasherinfo->vehicle_serial_number);
            if (status != S_SUCCESS)
            {
                //TODOQ: more specific about the error
                log_push_error_point(0x4B10);
                errornumbercode = 10463;
                cmdif_internalresponse_ack(CMDIF_CMD_DO_DOWNLOAD,
                                           CMDIF_ACK_MARRIED_FAIL,
                                           (u8*)&errornumbercode,4);
                goto cmdif_func_downloadtune_do_done;
            }
        }
    }//if (flasherinfo->uploadstock_required == FALSE)...


    //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    // Download
    //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    status = obd2tune_downloadtune(flasherinfo->vehicle_type);

    if (status != S_SUCCESS)
    {
        log_push_error_point(0x4B11);
        cmdif_internalresponse_ack(CMDIF_CMD_DO_DOWNLOAD,
                                   cmdif_datainfo_responsedata.responsecode,
                                   NULL,0);
        goto cmdif_func_downloadtune_do_done;
    }
    else
    {
        //track on the fly info
        SETTINGS_TUNE(tuneinfotrack.otfinfo.pcm) = flasherinfo->otfinfo.pcm;
        SETTINGS_TUNE(tuneinfotrack.otfinfo.tcm) = flasherinfo->otfinfo.tcm;
        SETTINGS_SetTuneAreaDirty();
    }

    if (flasherinfo->flashtype == CMDIF_ACT_CUSTOM_FLASHER)
    {
        //handle if this custom tune disable preloaded tune support
        if (flasherinfo->flags & FLASHERFLAG_DISABLE_PRELOADED_TUNE)
        {
            obd2tune_preloaded_tune_set_restriction(PRELOADEDTUNE_RESTRICTION_ENABLED);
        }
        else if (flasherinfo->flags & FLASHERFLAG_DISABLE_PRELOADED_TUNE_REMOVE)
        {
            obd2tune_preloaded_tune_set_restriction(PRELOADEDTUNE_RESTRICTION_ENABLED_WITH_REMOVAL);
        }
    }
    else if (flasherinfo->flashtype == CMDIF_ACT_STOCK_FLASHER)
    {
        //clear on the fly info
        SETTINGS_TUNE(tuneinfotrack.otfinfo.pcm) = 0;
        SETTINGS_TUNE(tuneinfotrack.otfinfo.tcm) = 0;
        SETTINGS_TUNE(tuneinfotrack.otfinfo.powerlevel) = 0;
        SETTINGS_TUNE(stockwheels.tire) = 0;           // stock file clear tire/axle info
        SETTINGS_TUNE(stockwheels.axel) = 0;
        SETTINGS_SetTuneAreaDirty();
    }
    
//    cmdif_internalresponse_ack(CMDIF_CMD_DO_DOWNLOAD,
//                               CMDIF_ACK_RESET_ECU,NULL,0);

    cmdif_internalresponse_ack(CMDIF_CMD_DO_DOWNLOAD,
                               CMDIF_ACK_CLEAR_DTC_ECU,NULL,0);
    
    delays(500,'m');   //give Vpp_OFF some time
    //TODOQ: might want to handle this differently
    
    
    // If require key on test, do this first so we can later get VIN and strategy
    if (ECM_IsRequireKeyOnTest(VEH_GetEcmType(flasherinfo->vehicle_type,0)) &&
        ECM_GetCommType(VEH_GetEcmType(flasherinfo->vehicle_type,0)) == CommType_CAN)
    {
         cmdif_internalresponse_ack(CMDIF_CMD_DO_DOWNLOAD,
                               CMDIF_ACK_CLEAR_DTC_ECU,NULL,0);     //borrow this ACK to reset ACK timeout on AB
        if (obd2can_key_on_test(Obd2CanEcuId_7E0) != S_SUCCESS)
        {
            log_push_error_point(0x4247);
            return S_FAIL;
        }
    }

    cmdif_internalresponse_ack(CMDIF_CMD_DO_DOWNLOAD,
                               CMDIF_ACK_CLEAR_DTC_ECU,NULL,0);     //just to reset app timeout to do task after program
    
    obd2_vehicle_specific_task_after_program(flasherinfo);
   
    cmdif_internalresponse_ack(CMDIF_CMD_DO_DOWNLOAD,
                               CMDIF_ACK_CLEAR_DTC_ECU,NULL,0); //borrow this ACK to reset ACK timeout on AB
    
cmdif_func_downloadtune_do_done:
    
    cmdif_tracker_set_state(CMDIF_TRACKER_STATE_IN_PROGRESS);
    power_setvpp(Vpp_OFF);
    
    /* Disconnect OBD2 handle to allow the OBD2 handler to rescan the bus;
     * otherwise ecm info may be incorrect such as VIN if overlay was used).
     * This must be done before performing health check.
     */
    obd2_close();
    
    if (status == S_SUCCESS)
    {
        status = obd2tune_download_health_check(flasherinfo->vehicle_type);
        
        if (status == S_SUCCESS || status == S_NOTREQUIRED)
        {
            status = S_SUCCESS;
        
            // Clear DTCs
            if(obd2_cleardtc((ECM_GetCommType(VEH_GetMainEcmType(flasherinfo->vehicle_type))),1) != S_SUCCESS)
            {
                log_push_error_point(0x4B18);
            }


            // Clear settings if stock flash
            if (flasherinfo->flags & FLASHERFLAG_CLEAR_MARRIED_STATUS)
            {
                SETTINGS_ClearMarried();
                SETTINGS_TUNE(veh_type) = INVALID_VEH_DEF;
                SETTINGS_TUNE(comm_type) = CommType_Unknown;
                SETTINGS_TUNE(processor_count) = 0;
                SETTINGS_TUNE(tuneinfotrack.tuneflags) = 0;
                SETTINGS_TUNE(stockspoptsfile_crc32e) = 0;
                //the variant ID for processors (ex: 2013 gpec2)is stored in the first element
                //of secondvehiclecodes under settings tune. This needs to be cleared when 
                //returning vehicle to stock. 
                SETTINGS_TUNE(secondvehiclecodes)[0] = 0;
                SETTINGS_SetTuneAreaDirty();
                settings_update(SETTINGS_UPDATE_IF_DIRTY);
            }
        
            cmdif_internalresponse_ack(CMDIF_CMD_DO_DOWNLOAD,
                                       CMDIF_ACK_DOWNLOAD_DONE,NULL,0);
            obd2_garbagecollector();
        }
        else
        {
            //need to recycle ignition key and try again
            status = S_SUCCESS;
            SETTINGS_SetMarried();
            SETTINGS_SetDownloadFail();
            SETTINGS_SetDownloadHealthCheckFail();
            SETTINGS_SetTuneAreaDirty();
            settings_update(SETTINGS_UPDATE_IF_DIRTY);
            
            downloadcompleteinstruction.recyclekey_healthcheck = 1;
            cmdif_internalresponse_ack(CMDIF_CMD_DO_DOWNLOAD,
                                       CMDIF_ACK_DOWNLOAD_DONE,(u8*)&downloadcompleteinstruction,sizeof(downloadcompleteinstruction));
            
            // obd2_garbagecollector() is handled in cmdif_func_downloadtune_health_check(...)
        }
    }
    else
    {
        if (flasherinfo->uploadstock_required &&
            SETTINGS_IsMarried() && !SETTINGS_IsDownloadFail())
        {
            //this means ECM(s) unchanged
//            SETTINGS_ClearMarried();          // Take out clear married flag, needs more testing
            //clear on the fly info
            SETTINGS_TUNE(tuneinfotrack.otfinfo.pcm) = 0;
            SETTINGS_TUNE(tuneinfotrack.otfinfo.tcm) = 0;
            SETTINGS_TUNE(tuneinfotrack.otfinfo.powerlevel) = 0;
        }
//        else
//        {
//            SETTINGS_SetMarried();
//            SETTINGS_SetDownloadFail();
//        }
        
        SETTINGS_SetTuneAreaDirty();
        settings_update(SETTINGS_UPDATE_IF_DIRTY);
        cmdif_internalresponse_ack(CMDIF_CMD_DO_DOWNLOAD,
                                   CMDIF_ACK_FAILED,NULL,0);
        obd2_garbagecollector();
    }
    
    indicator_set_normaloperation(TRUE); // Unlock LED
    indicator_clear();
    indicator_link_status();
    button_lock(FALSE);
    
    debug_reportstatus(status, "Download Do");
    debug_elapsedtime_stop();
    
    return status;
}

//------------------------------------------------------------------------------
// Handle download health check & confirm pcm/tcm not dead
// Return:  u8  status (including fail statuscode for negative response)
// Note: status does include negative response from cmdif; this means negative
// response is handled internally here and status equivalent to negative
// response
// Engineer: Quyen Leba
//------------------------------------------------------------------------------
u8 cmdif_func_downloadtune_health_check(u16 veh_type)
{
    u8  *data;
    u16 datalength;
    u16 ecm_type;
    u8  count;
    u8  status;
    u32 vehmin;
    u32 vehmax;
    
    vehmin = VEH_GetMinVehType();
    vehmax = VEH_GetMaxVehType();
    
    if (((u32)veh_type+1) < (vehmin+1) || veh_type > vehmax)
    {
        //vehicle type is not in valid range
        cmdif_datainfo_responsedata.responsecode = CMDIF_ACK_FAILED;
        return S_OUTOFRANGE;
    }

    cmdif_datainfo_responsedata.responsecode = CMDIF_ACK_FAILED;
    data = __malloc(2048+16);
    if (data == NULL)
    {
        log_push_error_point(0x4B1C);
        return S_MALLOC;
    }

    ecm_type = VEH_GetEcmType(veh_type,0);

    //give it some time, in case key just turn on
    delays(5000,'m');
    
    //confirm ignition key is on
    count = 0;
    while(1)
    {
        status = obd2_checkkeyon(ECM_GetCommType(ecm_type), TRUE);
        if (status != S_SUCCESS)
        {
            //some ECM's will not respond to pings but will respond to security access service
            status = obd2tune_download_test_key_on(ecm_type);
        }
        if (status == S_SUCCESS)
        {
            break;
        }
        else if (count++ >= 2)
        {
            //appears that ignition key is not on
            log_push_error_point(0x4B28);
            cmdif_datainfo_responsedata.responsecode = CMDIF_ACK_COMMLOST;
            status = S_NOCOMMUNICATION;
            goto cmdif_func_downloadtune_health_check_done;
        }
        else
        {
            //ask again to turn ignition key on
            cmdif_internalresponse_ack(CMDIF_CMD_HEALTH_CHECK_DOWNLOAD,
                                       CMDIF_ACK_KEY_ON,NULL,0);
            while(1)
            {
                //TODOQ: IMPORTANT - need timeout
                status = cmdif_receive_command_internal(data,&datalength,2048+16);
                if (status == S_SUCCESS)
                {
                    status = S_SUCCESS;
                    break;
                }
            }//while(1)...
            if (data[1] == CMDIF_CMD_KEY_CONFIRM)
            {
                cmdif_internalresponse_ack((CMDIF_COMMAND)data[1],
                                           CMDIF_ACK_OK,NULL,0);
            }
            else
            {
                //unknown command
                cmdif_internalresponse_ack((CMDIF_COMMAND)data[1],
                                           CMDIF_ACK_FAILED,NULL,0);
            }
        }
    }//while(1)...

    //give it some time, in case key just turn on
    delays(5000,'m');
    cmdif_internalresponse_ack(CMDIF_CMD_HEALTH_CHECK_DOWNLOAD,
                               CMDIF_ACK_CHECK_MARRIED,NULL,0);

    status = obd2tune_download_health_check(veh_type);
    if (status == S_SUCCESS || status == S_NOTREQUIRED)
    {
        if(obd2_cleardtc(ECM_GetCommType(VEH_GetMainEcmType(flasherinfo->vehicle_type)),0) != S_SUCCESS)
        {
            log_push_error_point(0x4B26);
        }
        // Clear settings if stock flash
        if (flasherinfo->flags & FLASHERFLAG_CLEAR_MARRIED_STATUS)
        {
            SETTINGS_ClearMarried();
            SETTINGS_TUNE(veh_type) = INVALID_VEH_DEF;
            SETTINGS_TUNE(comm_type) = CommType_Unknown;
            SETTINGS_TUNE(processor_count) = 0;
            SETTINGS_TUNE(tuneinfotrack.tuneflags) = 0;
            SETTINGS_TUNE(stockspoptsfile_crc32e) = 0;
            //the variant ID for processors (ex: 2013 gpec2)is stored in the first element
            //of secondvehiclecodes under settings tune. This needs to be cleared when 
            //returning vehicle to stock. 
            SETTINGS_TUNE(secondvehiclecodes)[0] = 0;
            SETTINGS_SetTuneAreaDirty();
            settings_update(SETTINGS_UPDATE_IF_DIRTY);
        }
        status = S_SUCCESS;
        cmdif_datainfo_responsedata.responsecode = CMDIF_ACK_OK;
    }
    else
    {
        SETTINGS_SetMarried();
        SETTINGS_SetDownloadFail();
        SETTINGS_SetDownloadHealthCheckFail();
        SETTINGS_SetTuneAreaDirty();
        settings_update(SETTINGS_UPDATE_IF_DIRTY);
        cmdif_datainfo_responsedata.responsecode = CMDIF_ACK_DOWNLOAD_FAILED;
    }

cmdif_func_downloadtune_health_check_done:
    obd2_garbagecollector();
    if (data)
    {
        __free(data);
    }
    return status;
}
