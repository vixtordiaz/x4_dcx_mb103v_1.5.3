/**
  **************** (C) COPYRIGHT 2011 SCT Performance, LLC *********************
  * File Name          : cmdif_func_wifi.c
  * Device / System    : SCT products using the "CommonCode" platform
  * Author             : Quyen Leba
  *
  * Version            : 1 
  * Date               : 05/02/2011
  * Description        : 
  *                    : 
  *
  *
  * History            : 05/02/2011 M. Davis
  *                    :   Add SCT Copyright header,
  *
  ******************************************************************************
  */

#include <string.h>
#include <stdio.h>
#include <fs/genfs.h>
#include <board/delays.h>
#include <board/genplatform.h>
#include <board/hardwarefeatures.h>
#include <board/properties_vb.h>
#include <common/statuscode.h>
#include <common/crypto_messageblock.h>
#include <common/housekeeping.h>
#include <common/itoa.h>
#include <common/file.h>
#include <common/json.h>
#include <common/cmdif.h>
#include <common/obd2datalog.h>
#include <common/debug/debug_output.h>
#include <common/crypto_blowfish.h>
#include <common/vehicleinfolog.h>
#include <common/cloud_lib.h>
#include "cmdif_func_wifi.h"

//------------------------------------------------------------------------------
// Log error point: 0x4E00 - 0x4EFF
//------------------------------------------------------------------------------

#define V_TUNE_CUSTOM_TUNE_FROM_CTB_COUNT_MAX   10

typedef enum
{
    V_Tune_CustomTune_Type_Active_CTF           = 1,
    V_Tune_CustomTune_Type_Active_CTB           = 2,

    /* inactive due to incompatible */
    V_Tune_CustomTune_Type_Inactive_CTF         = 11,
    V_Tune_CustomTune_Type_Inactive_CTB         = 12,

    /* other conditions */
    V_Tune_CustomTune_Type_Error_Getting_List   = 40,   /* unable to get list */
    V_Tune_CustomTune_Type_Feature_Disabled     = 41,   /* custom tune disabled */
    V_Tune_CustomTune_Type_No_Tune_Found        = 42,   /* device has no custom tunes */
}V_Tune_CustomTune_Type;

typedef struct
{
    u8  count;
    struct
    {
        /* where the tune is located */
        V_Tune_CustomTune_Type type;

        /* only for CTF type */
        u8  ctf_slot_index;

        u8  description[32];
        u8  filename[32];
    }tune[V_TUNE_CUSTOM_TUNE_FROM_CTB_COUNT_MAX];
}V_Tune_CustomTune_Collection;

/**< available custom tunes: 1 collection for CTB types */
V_Tune_CustomTune_Collection available_custom_tune_collection;

extern cmdif_datainfo cmdif_datainfo_responsedata;  //from cmdif.c

u8 cmdif_func_wifi_download_firmware_files(SERVER_FW fwinfo, u8 *filename, u8 *progress_string);
u8 cmdif_func_wifi_download_firmware_update();
u8 cmdif_func_wifi_download_tunerevision_update();
u8 cmdif_func_wifi_check_tunerevision_update(const u8* filename);
u8 cmdif_func_wifi_check_firmware_update(const u8* filename, FW_VERSIONS versions);

u8 cmdif_func_wifi_diffile_createnew(F_FILE *fdif);
u8 cmdif_func_wifi_diffile_append(F_FILE *fdif, SERVER_TUNEFILE_INFO tuneinfo);
u8 cmdif_func_wifi_diffile_finish(F_FILE *fdif);
u8 cmdif_func_wifi_diffile_validatefile(void);
u8 cmdif_func_wifi_diffile_validateheader(DIFF_FILE_HEADER header, u32 *retcrc);

/**
 *  cmdif_func_wifi_check_update
 *  
 *  @brief Check for available update
 *  
 *  @param [in] update_request
 *
 *  @retval u8  status
 *  
 *  @details Details
 *  
 *  @authors Quyen Leba, Patrick Downs
 */
u8 cmdif_func_wifi_check_update(CMDIF_ServerAccessType update_request)
{
    FW_VERSIONS versions;
    u8 status;
    u8 fwstatus;
    u8 trstatus;
    u8 ctstatus;

    if (!hardwarefeatures_has_wifi())
    {
        cmdif_datainfo_responsedata.responsecode = CMDIF_ACK_NOT_SUPPORTED;
        return S_NOTSUPPORT;
    }

    fwstatus = S_SUCCESS;
    trstatus = S_SUCCESS;
    ctstatus = S_SUCCESS;
    status = S_SUCCESS;
    
    if(update_request == CMDIF_ServerAccessType_Firmware || update_request == CMDIF_ServerAccessType_TuneRevision)
    {
        cmdif_func_wifi_get_fwversions(&versions); // Save FW versions for comparison later
        
        status = json_generate_post_file(SERVER_REQUEST_INFO_FILENAME);
        if(status != S_SUCCESS)
        {
            log_push_error_point(0x4E00);
            cmdif_datainfo_responsedata.responsecode = CMDIF_ACK_FAILED;
            return S_FAIL;
        }
        
        status = wifi_hal_post_request("services.sctflash.com", "/api/CurrentDeviceConfigurationStream", SERVER_REQUEST_INFO_FILENAME, 
                                       SERVER_RESPONSE_INFO_FILENAME, NULL);
        if(status != S_SUCCESS)
        {
            log_push_error_point(0x4E01);
            cmdif_datainfo_responsedata.responsecode = CMDIF_ACK_FAILED;
            return S_FAIL;
        }
    }
    
    switch(update_request)
    {
    case CMDIF_ServerAccessType_Firmware:
        fwstatus = cmdif_func_wifi_check_firmware_update(SERVER_RESPONSE_INFO_FILENAME, versions);
        break;
    case CMDIF_ServerAccessType_TuneRevision:
        trstatus = cmdif_func_wifi_check_tunerevision_update(SERVER_RESPONSE_INFO_FILENAME);
        break;
    case CMDIF_ServerAccessType_CustomTunes:
        ctstatus = cmdif_func_wifi_tunes_check_update();
        break;
    default:
        log_push_error_point(0x4E02);
        status = S_NOTSUPPORT;
        break;
    }
    
    if (status == S_NOTSUPPORT)
    {
        cmdif_datainfo_responsedata.responsecode = CMDIF_ACK_NOT_SUPPORTED;
    }
    else if ((fwstatus == S_EXPIRED) || (trstatus == S_EXPIRED) || (ctstatus == S_EXPIRED))
    {
        status = S_SUCCESS;
        cmdif_datainfo_responsedata.responsecode = CMDIF_ACK_EXPIRED; // updates available
    }
    else
    {
        //TODOQ: more error catch
        cmdif_datainfo_responsedata.responsecode = CMDIF_ACK_OK; // up-to-date
    }

    return status;
}

/**
 *  cmdif_func_wifi_tunes_check_update
 *  
 *  @brief Check for available custom tunes with serialnumber
 *  
 *  @param [in] update_request
 *
 *  @retval u8  status
 *  
 *  @details Details
 *  
 *  @authors Patrick Downs
 */
u8 cmdif_func_wifi_tunes_check_update(void)
{
    u8 status;
    u8  serial[24];
    u32 length;
    
    if (!hardwarefeatures_has_wifi())
    {
        cmdif_datainfo_responsedata.responsecode = CMDIF_ACK_NOT_SUPPORTED;
        return S_NOTSUPPORT;
    }
 
    // Serial Number
    settings_getsettings_byopcode(SETTINGS_OPCODE_SERIAL,0,
                                  serial,&length);

    // Update File List
    cmdif_progressbar_setup(cmdif_tracker_get_current_command(),"Acquiring Tune Info");
    
    status = cloud_handle_activation_authentication(serial);
    if(status == S_SUCCESS)
    {   
        status = cloud_update_tunelist(serial);
    }
    else
    {
        log_push_error_point(0x4E28); // Failed activate/authenticate
    }

    return status;
}

/**
 *  cmdif_func_wifi_check_for_tunes
 *  
 *  @brief Check for available custom tunes with serialnumber
 *  
 *  @param [in] update_request
 *
 *  @retval u8  status
 *  
 *  @details Details
 *  
 *  @authors Patrick Downs
 */
u8 cmdif_func_wifi_get_cloudtune_list(void)
{
    u8 status;
    
    if (!hardwarefeatures_has_wifi())
    {
        cmdif_datainfo_responsedata.responsecode = CMDIF_ACK_NOT_SUPPORTED;
        return S_NOTSUPPORT;
    }
    
    // Download File List
    cmdif_progressbar_setup(cmdif_tracker_get_current_command(),"Acquiring Tune Info");
        
    u8 *tunelist;
    u32 tunelistlength;
    
    status = cloud_get_tunelist(&tunelist, &tunelistlength);
    if(status == S_SUCCESS)
    {
        if (tunelist)
        {
            cmdif_datainfo_responsedata.dataptr = tunelist;
            cmdif_datainfo_responsedata.datalength = tunelistlength;
            cmdif_datainfo_responsedata.responsecode = CMDIF_ACK_OK;
        }
        else
        {
            //log_push_error_point(); // TODO new EP
            status = S_MALLOC;
        }
    }

    return status;
}

/**
 *  cmdif_func_wifi_check_for_tunes
 *  
 *  @brief Check for available custom tunes with serialnumber
 *  
 *  @param [in] update_request
 *
 *  @retval u8  status
 *  
 *  @details Details
 *  
 *  @authors Patrick Downs
 */
u8 cmdif_func_wifi_download_cloud_file(u16 index)
{
    u8 status;
    
    if (!hardwarefeatures_has_wifi())
    {
        cmdif_datainfo_responsedata.responsecode = CMDIF_ACK_NOT_SUPPORTED;
        return S_NOTSUPPORT;
    }
     
    // Download File    
    cmdif_progressbar_setup(cmdif_tracker_get_current_command(),"Downloading File");
    
    status = cloud_download_file_by_searchlist_index(index, cmdif_progressbar_report);
    if(status == S_SUCCESS)
    {
        cmdif_datainfo_responsedata.responsecode = CMDIF_ACK_OK;
    }
    else
    {
        cmdif_datainfo_responsedata.responsecode = CMDIF_ACK_FAILED;
    }

    return status;
}

/**
 *  cmdif_func_wifi_check_for_tunes
 *  
 *  @brief Check for available custom tunes with serialnumber
 *  
 *  @param [in] update_request
 *
 *  @retval u8  status
 *  
 *  @details Details
 *  
 *  @authors Patrick Downs
 */
u8 cmdif_func_wifi_check_limit()
{
    u8 status;
    u8  serial[18];
    u32 length;
    u8 pin[CLOUD_PIN_LENGTH];
    CloudCheckLimit limitdata;
    
    if (!hardwarefeatures_has_wifi())
    {
        cmdif_datainfo_responsedata.responsecode = CMDIF_ACK_NOT_SUPPORTED;
        return S_NOTSUPPORT;
    }
    
    // Serial Number
    settings_getsettings_byopcode(SETTINGS_OPCODE_SERIAL,0,
                                  serial,&length);
    
    status = cloud_handle_activation_authentication(serial);
    if(status != S_SUCCESS)
    {
        log_push_error_point(0x4E29);
    }
    
    status = cloud_checklimit(serial, &limitdata); // limit.message gets malloc'd
    if(status == S_SUCCESS)
    {
        memset(pin, 0, sizeof(pin));
        cloud_getpin(pin);
        
        length = sizeof(limitdata.accountrequired)+sizeof(limitdata.limit)+sizeof(serial)+sizeof(pin)+strlen((const char*)limitdata.message)+1;
        u8 *bptr = __malloc(length);
        if(bptr)
        {
            // [1:accountrequired][4:limit][6:PIN][18:SERIAL]["message"][NULL]
            u8* p = bptr;
            memcpy(p,&limitdata.accountrequired, sizeof(limitdata.accountrequired));
            p += sizeof(limitdata.accountrequired);
            memcpy(p,&limitdata.limit, sizeof(limitdata.limit));
            p += sizeof(limitdata.limit);
            memcpy(p, pin, sizeof(pin));
            p += sizeof(pin);
            memcpy(p, serial, sizeof(serial));
            p += sizeof(serial);
            strcpy((char*)p,(const char*)limitdata.message);
            
            cmdif_datainfo_responsedata.dataptr = bptr;
            cmdif_datainfo_responsedata.datalength = length;
            cmdif_datainfo_responsedata.responsecode = CMDIF_ACK_OK;
        }
        else
        {
//            log_push_error_point(0x4E18);
            status = S_MALLOC;
        }
        if(limitdata.message)
        {
            __free(limitdata.message);
        }
    }
    else
    {
        cmdif_datainfo_responsedata.responsecode = CMDIF_ACK_FAILED;
    }

    return status;
}

/**
 *  cmdif_func_wifi_check_for_tunes
 *  
 *  @brief Check for available custom tunes with serialnumber
 *  
 *  @param [in] update_request
 *
 *  @retval u8  status
 *  
 *  @details Details
 *  
 *  @authors Patrick Downs
 */
u8 cmdif_func_wifi_deactivate()
{
    u8 status;
    u8  serial[24];
    u32 length;
    
    if (!hardwarefeatures_has_wifi())
    {
        cmdif_datainfo_responsedata.responsecode = CMDIF_ACK_NOT_SUPPORTED;
        return S_NOTSUPPORT;
    }
    
    // Serial Number
    settings_getsettings_byopcode(SETTINGS_OPCODE_SERIAL,0,
                                  serial,&length);
    
    status = cloud_deactivate(serial);
    if(status == S_SUCCESS)
    {
        cmdif_datainfo_responsedata.responsecode = CMDIF_ACK_OK;
    }
    else
    {
        cmdif_datainfo_responsedata.responsecode = CMDIF_ACK_FAILED;
    }

    return status;
}

/**
 *  cmdif_func_wifi_check_for_tunes
 *  
 *  @brief Check for available custom tunes with serialnumber
 *  
 *  @param [in] update_request
 *
 *  @retval u8  status
 *  
 *  @details Details
 *  
 *  @authors Patrick Downs
 */
u8 cmdif_func_wifi_tunes_process_tunelist(void)
{
    F_FILE* tunefilelist_fptr;
    F_FILE* tunedifffile_fptr;
    u32 tunecount;
    u32 tunefilesize;
    SERVER_TUNEFILE_INFO tuneinfo;
    u32 bytesread;
    u32 i;
    bool hasNewTunes;
    u8  status;
    
    if (!hardwarefeatures_has_wifi())
    {
        cmdif_datainfo_responsedata.responsecode = CMDIF_ACK_NOT_SUPPORTED;
        return S_NOTSUPPORT;
    }

    hasNewTunes = FALSE;
    tunedifffile_fptr = NULL;
    
    tunefilelist_fptr = genfs_user_openfile(SERVER_TUNEFILELIST_FILENAME,"rb");
    if(tunefilelist_fptr)
    {
        if(fseek(tunefilelist_fptr, 0, SEEK_END) == 0)
        {
            tunefilesize = ftell(tunefilelist_fptr);
            tunecount = (tunefilesize / sizeof(SERVER_TUNEFILE_INFO));
            if(tunecount > 0)
            {
                fseek(tunefilelist_fptr, 0, SEEK_SET);
                for(i = 0; i < tunecount; i++)
                {
                    bytesread = fread((char*)&tuneinfo, 1, sizeof(tuneinfo), tunefilelist_fptr);
                    if(bytesread == sizeof(tuneinfo))
                    {
                        status = cmdif_func_wifi_tunes_check_tune(tuneinfo);
                        if(status != S_SUCCESS)
                        {
                            hasNewTunes = TRUE;
                            // File needs to be downloaded
                            if(tunedifffile_fptr == NULL)   // Create new DFL if not made yet
                            {
                                tunedifffile_fptr = genfs_user_openfile(CTB_DIF_FILENAME,"w+");
                                status = cmdif_func_wifi_diffile_createnew(tunedifffile_fptr);
                                if(status != S_SUCCESS)
                                {
                                    break;
                                }
                            }
                            
                            status = cmdif_func_wifi_diffile_append(tunedifffile_fptr, tuneinfo);
                            if(status != S_SUCCESS)
                            {
                                break;
                            }
                        }
                    }
                    else
                    {
                        status = S_READFILE;
                    }
                }//for(...
            }
        }
        else
        {
            status = S_SEEKFILE;
        }
        
        if(tunedifffile_fptr)
        {
            // Update Dif File header and close
            status = cmdif_func_wifi_diffile_finish(tunedifffile_fptr);
            genfs_closefile(tunedifffile_fptr);
        }
        
        genfs_closefile(tunefilelist_fptr);
    }
    else
    {
        status = S_OPENFILE;
    }

    if (status == S_SUCCESS && hasNewTunes)
    {
        status = S_EXPIRED;
    }

    return status;
}

/**
 *  cmdif_func_wifi_tunes_download_tunelist
 *  
 *  @brief Downloads files from a tunelist from the server
 *
 *  @retval u8  status
 *  
 *  @details Details
 *  
 *  @authors Patrick Downs
 */
u8 cmdif_func_wifi_tunes_download_tunelist(void)
{
    u8 status;
    F_FILE* tunefilelist_fptr;
    u32 tunecount;
    u32 tunefilesize;
    SERVER_TUNEFILE_INFO tuneinfo;
    u32 bytesread;
    int i;
    u8 tmpString[40];
    
    if (!hardwarefeatures_has_wifi())
    {
        cmdif_datainfo_responsedata.responsecode = CMDIF_ACK_NOT_SUPPORTED;
        return S_NOTSUPPORT;
    }
    
    cmdif_datainfo_responsedata.responsecode = CMDIF_ACK_FAILED;
    
    tunefilelist_fptr = genfs_general_openfile(SERVER_TUNEFILELIST_FILENAME,"rb");
    if(tunefilelist_fptr)
    {
        fseek(tunefilelist_fptr, 0, SEEK_END);
        tunefilesize = ftell(tunefilelist_fptr);
        tunecount = (tunefilesize / sizeof(SERVER_TUNEFILE_INFO));
        if(tunecount > 0)
        {
            fseek(tunefilelist_fptr, 0, SEEK_SET);
            
            for(i=0;i<tunecount;i++)
            {
                bytesread = fread((char*)&tuneinfo, 1, sizeof(tuneinfo), tunefilelist_fptr);
                if(bytesread == sizeof(tuneinfo))
                {
                    strcpy((char*)tmpString,"Downloading File ");
                    itoa(i+1,&tmpString[strlen((char*)tmpString)],10);
                    strcat((char*)tmpString,"/");
                    itoa(tunecount,&tmpString[strlen((char*)tmpString)],10);
                    
                    cmdif_progressbar_setup(cmdif_tracker_get_current_command(),tmpString);
                    status = cmdif_func_wifi_download_files_by_access_string(CMDIF_ServerAccessType_CustomTunes, 
                                                                             tuneinfo.rowguid, tuneinfo.filename);
                    if(status == S_SUCCESS)
                    {
                        cmdif_datainfo_responsedata.responsecode = CMDIF_ACK_DONE;
                    }
                    else
                    {
                        cmdif_datainfo_responsedata.responsecode = CMDIF_ACK_FAILED;
                        break;
                    }
                }
                else
                {
                    status = S_READFILE;
                }
            }
        }
        
        genfs_closefile(tunefilelist_fptr);
    }
    else
    {
        status = S_OPENFILE;
    }
    
    return status;
}

/**
 *  cmdif_func_wifi_download_current_spf_by_vif
 *  
 *  @brief Download SPF using info from last vehicle info read
 *
 *  @retval u8  status
 *  
 *  @authors Quyen Leba
 */
u8 cmdif_func_wifi_download_current_spf_by_vif()
{
    ecm_info *ecminfo;
    u8  fullfilename[32+MAX_TUNECODE_LENTH+1];
    u8  filedownloaded;
    u8  i;
    u8  status;
    
    if (!hardwarefeatures_has_wifi())
    {
        cmdif_datainfo_responsedata.responsecode = CMDIF_ACK_NOT_SUPPORTED;
        return S_NOTSUPPORT;
    }

    filedownloaded = 0;
    status = S_FAIL;
    cmdif_datainfo_responsedata.responsecode = CMDIF_ACK_FAILED;

    ecminfo = __malloc(sizeof(ecm_info));
    if (!ecminfo)
    {
        status = S_MALLOC;
    }
    else
    {
        status = vehicleinfolog_get_latest_block(ecminfo);
        if (status == S_SUCCESS)
        {
            for(i=0;i<ecminfo->ecm_count;i++)
            {
                if (isgraph(ecminfo->second_codes[i][0][0]))
                {
                    strcpy((char*)fullfilename,GENFS_USER_FOLDER_PATH);
                    if (strlen((char*)ecminfo->second_codes[i][0]) < sizeof(ecminfo->second_codes[i][0]))
                    {
                        strcat((char*)fullfilename,(char*)ecminfo->second_codes[i][0]);
                        strcat((char*)fullfilename,".spf");
                        
                        if(file_isexist(fullfilename)) // For now, just check if it already exists
                        {
                            status = S_SUCCESS;
                            filedownloaded++;
                        }
                        else
                        {
                            //do not assign status
                            cmdif_progressbar_setup(cmdif_tracker_get_current_command(), "Downloading Vehicle Specific Files");
                            if (cmdif_func_wifi_download_files_by_access_string(CMDIF_ServerAccessType_SPF,
                                                                                ecminfo->second_codes[i][0],
                                                                                fullfilename)
                                == S_SUCCESS)
                            {
                                filedownloaded++;
                            }
                        }
                    }
                }
                else if (isgraph(ecminfo->codes[i][0][0]))
                {
                    strcpy((char*)fullfilename,GENFS_USER_FOLDER_PATH);
                    if (strlen((char*)ecminfo->codes[i][0]) < sizeof(ecminfo->codes[i][0]))
                    {
                        strcat((char*)fullfilename,(char*)ecminfo->codes[i][0]);
                        strcat((char*)fullfilename,".spf");

                        if(file_isexist(fullfilename) == FALSE) // For now, just check if it already exists
                        {
                            status = S_SUCCESS;
                            filedownloaded++;
                        }
                        else
                        {
                            //do not assign status                        
                            if (cmdif_func_wifi_download_files_by_access_string(CMDIF_ServerAccessType_SPF,
                                                                                ecminfo->codes[i][0],
                                                                                fullfilename)
                                == S_SUCCESS)
                            {
                                filedownloaded++;
                            }
                        }
                    }
                }
                else
                {
                    //do nothing
                }
            }//for(i=...

            if (ecminfo->ecm_count == 0)
            {
                status = S_BADCONTENT;
            }
            else if (filedownloaded == 0)
            {
                status = S_UNMATCH;
                cmdif_datainfo_responsedata.responsecode = CMDIF_ACK_SPF_NOT_FOUND;
            }
        }
        else if (status == S_OPENFILE)
        {
            cmdif_datainfo_responsedata.responsecode = CMDIF_ACK_FILE_NOT_FOUND;
        }
        else if (status == S_READFILE)
        {
            cmdif_datainfo_responsedata.responsecode = CMDIF_ACK_FAILED;
        }
        else if (status == S_BADCONTENT)
        {
            cmdif_datainfo_responsedata.responsecode = CMDIF_ACK_FAILED;
        }
        else
        {
            cmdif_datainfo_responsedata.responsecode = CMDIF_ACK_FAILED;
        }

        __free(ecminfo);
    }

    if (status == S_SUCCESS)
    {
        cmdif_datainfo_responsedata.responsecode = CMDIF_ACK_OK;
    }

    return status;
}

/**
 *  cmdif_func_wifi_tunes_check_tune
 *  
 *  @brief Compares tune info from server with local tune files
 *  
 *  @param [in] tuneinfo  SERVER_TUNEFILE_INFO block with tunefile information
 *
 *  @retval STATUS (S_SUCCESS - File matches local file, S_EXPIRED - Doesn't match any local files)
 *  
 *  @details Details
 *  
 *  @authors Patrick Downs
 */
u8 cmdif_func_wifi_tunes_check_tune(SERVER_TUNEFILE_INFO tuneinfo)
{
    u8 status;
    F_FILE* ftf;
    u32 tunefilecrc;
    
    ftf = genfs_user_openfile(tuneinfo.filename,"rb");
    if(ftf)
    {
        // File found, run CRC32
        status = file_calcfilecrc32e(ftf, &tunefilecrc);
        if(status == S_SUCCESS)
        {
            if(tunefilecrc == tuneinfo.crc32e)
            {
                // CRC matches, file already downloaded
                status = S_SUCCESS;
            }
            else
            {
                status = S_EXPIRED;
            }
        }
        genfs_closefile(ftf);
    }
    else
    {
        status = S_EXPIRED;
    }
    
    return status;
}


/**
 *  cmdif_func_wifi_download_update_progress_report
 *  
 *  @brief Send progress report
 *  
 *  @param [in] percentage  Progress in 0 to 100 percent
 *
 *  @retval none
 *  
 *  @details Details
 *  
 *  @authors Quyen Leba, Patrick Downs
 */
void cmdif_func_wifi_download_update_progress_report(u8 percentage)
{
    cmdif_internalresponse_ack(CMDIF_CMD_WIFI_UPDATES_DOWNLOAD,CMDIF_ACK_PROGRESSBAR,NULL,0);
}

/**
 *  cmdif_func_wifi_download_update
 *  
 *  @brief Download available update
 *  
 *  @param [in] update_request
 *
 *  @retval u8  status
 *  
 *  @details Details
 *  
 *  @authors Quyen Leba, Patrick Downs
 */
u8 cmdif_func_wifi_download_update(CMDIF_ServerAccessType update_request)
{
    u8  status;

    if (!hardwarefeatures_has_wifi())
    {
        cmdif_datainfo_responsedata.responsecode = CMDIF_ACK_NOT_SUPPORTED;
        return S_NOTSUPPORT;
    }

    switch(update_request)
    {
    case CMDIF_ServerAccessType_Firmware:
        status = cmdif_func_wifi_download_firmware_update();
        break;
    case CMDIF_ServerAccessType_TuneRevision:
        status = cmdif_func_wifi_download_tunerevision_update();
        break;
    case CMDIF_ServerAccessType_CustomTunes:
        status = cmdif_func_wifi_tunes_download_tunelist();
        break;
    case CMDIF_ServerAccessType_SPF:
        status = cmdif_func_wifi_download_current_spf_by_vif();
        break;
    default:
        log_push_error_point(0x4E03);
        status = S_NOTSUPPORT;
        cmdif_datainfo_responsedata.responsecode = CMDIF_ACK_NOT_SUPPORTED;
        break;
    }

    return status;
}

/**
 *  cmdif_func_wifi_download_firmware_update
 *  
 *  @brief Download available firmware update
 *
 *  @retval u8  status
 *  
 *  @details Need cmdif_func_wifi_apply_update(...) to complete firmware update
 *  
 *  @authors Quyen Leba, Patrick Downs
 */
u8 cmdif_func_wifi_download_firmware_update()
{
    u8  status;
    SERVER_FW_INFO updateinfo;
    SERVER_FW fw[6];
    u8 filename[6][32];
    u8 filecount;
    u8 filecurrent;
    u8 progressstring[32];
    u8 filenumber[8];
    
    if (!hardwarefeatures_has_wifi())
    {
        cmdif_datainfo_responsedata.responsecode = CMDIF_ACK_NOT_SUPPORTED;
        return S_NOTSUPPORT;
    }

    // Read info about server firmware files
    filecount = 0;
    status = fwupdate_read_firmware_info(&updateinfo, FWUPDATE_BOARDTYPE_MAINBOARD);   // Mainboard
    if(status == S_SUCCESS)
    {
      if(updateinfo.mboot.fwsize)
      {
        fw[filecount] = updateinfo.mboot;
        strcpy((char*)filename[filecount], MB_MAINBOOT_FILENAME);
        filecount++;
      }
      if(updateinfo.app.fwsize)
      {
        fw[filecount] = updateinfo.app;
        strcpy((char*)filename[filecount], MB_APP_FILENAME);
        filecount++;
      }
    }
    
    status = fwupdate_read_firmware_info(&updateinfo, FWUPDATE_BOARDTYPE_VEHICLEBOARD); // VehicleBoard
    if(status == S_SUCCESS)
    {
      if(updateinfo.mboot.fwsize)
      {
        fw[filecount] = updateinfo.mboot;
        strcpy((char*)filename[filecount], VB_MAINBOOT_FILENAME);
        filecount++;
      }
      if(updateinfo.app.fwsize)
      {
        fw[filecount] = updateinfo.app;
        strcpy((char*)filename[filecount], VB_APP_FILENAME);
        filecount++;
      }
    }
    
    status = fwupdate_read_firmware_info(&updateinfo,FWUPDATE_BOARDTYPE_APPBOARD); // Appboard
    if(status == S_SUCCESS)
    {
      if(updateinfo.mboot.fwsize)
      {
        fw[filecount] = updateinfo.mboot;
        strcpy((char*)filename[filecount], AB_MAINBOOT_FILENAME);
        filecount++;
      }
      if(updateinfo.app.fwsize)
      {       
        fw[filecount] = updateinfo.app;
        strcpy((char*)filename[filecount], AB_APP_FILENAME);
        filecount++;
      }
    }
//        log_push_error_point(0x4E04); // Abandon?
//        status = S_OPENFILE; // Couldn't open file
    
    for(filecurrent = 0; filecurrent < filecount; filecurrent++)
    {
      // Download Firmware Files
      strcpy((char*)progressstring, "Software Update ");
      itoa(filecurrent+1, filenumber, 10);
      strcat((char*)progressstring, (char*)filenumber);
      strcat((char*)progressstring, "/");
      itoa(filecount, filenumber, 10);
      strcat((char*)progressstring, (char*)filenumber);
      status = cmdif_func_wifi_download_firmware_files(fw[filecurrent], filename[filecurrent], progressstring);
      if(status != S_SUCCESS)
      {
        break; // Failed to download file
      }
    }
    
    if(status == S_SUCCESS && filecount)
    {
      cmdif_datainfo_responsedata.responsecode = CMDIF_ACK_DONE; // All files downloaded
    }
    else
    {
      log_push_error_point(0x4E05);
      cmdif_datainfo_responsedata.responsecode = CMDIF_ACK_FAILED;
    }    
    
    return status;
}

/**
 *  cmdif_func_wifi_download_tunerevision_update
 *  
 *  @brief Download available tune revision update
 *
 *  @retval u8  status
 *  
 *  @details Details
 *  
 *  @authors Quyen Leba, Patrick Downs
 */
#define TRUPDATE_TUNELIST_VERSION   1
u8 cmdif_func_wifi_download_tunerevision_update()
{
    F_FILE* ftru;
    F_FILE* ftrd;
    u8  resource[128];
    u8  temp[64];
    u8  partnumber[SETTINGS_CRITICAL_DEVICEPARTNUMBERSTRING_LENGTH+1];
    u8  tuneversion[40];
    u8  serial[24];
    u8  tmpString[32];
    u32 length;
    u32 i;
    TRUPDATE_HEADER trheader;
    TRUPDATE_BLOCK trblock;
    u32 bytesrw;
    u32 totaldownloadsize;
    u32 totaldownloadcount;
    u8  percentage;
    u8  status;

    if (!hardwarefeatures_has_wifi())
    {
        cmdif_datainfo_responsedata.responsecode = CMDIF_ACK_NOT_SUPPORTED;
        return S_NOTSUPPORT;
    }

    totaldownloadsize = 0;
    totaldownloadcount = 0;
    ftru = NULL;
    ftrd = NULL;

    cmdif_datainfo_responsedata.responsecode = CMDIF_ACK_FAILED;

    // Partnumber
    settings_getsettings_byopcode(SETTINGS_OPCODE_DEVICE_PARTNUMBER_STRING,0,
                                  partnumber,&length);
    strcpy((char*)resource, "/api/GetUpdatedFileDetails?PartNumber=");
    strcat((char*)resource, (char const*)partnumber);
    
    // Tune Version
    status = obd2tune_gettunerevision(tuneversion);
    strcat((char*)resource, (char const*)"&TuneVersion=");
    if (status == S_SUCCESS)
    {
        strcat((char*)resource, (char const*)tuneversion);
    }
    else
    {
        //unknown tune revision, request everything
        strcat((char*)resource,"0");
        strcpy((char*)tuneversion,"0");
    }

    // Serial Number
    settings_getsettings_byopcode(SETTINGS_OPCODE_SERIAL,0,
                                  serial,&length);
    strcat((char*)resource, "&SerialNumber=");
    strcat((char*)resource, (char const*)serial);
    
    status = cmdif_func_wifi_check_tunerevision_update(SERVER_RESPONSE_INFO_FILENAME);
    if (status != S_EXPIRED)
    {
        cmdif_datainfo_responsedata.responsecode = CMDIF_ACK_DONE;
        return S_SUCCESS;
    }

    // Download File List
    cmdif_progressbar_setup(cmdif_tracker_get_current_command(),"Acquiring Tune Info");
    status = wifi_hal_download_file("services.sctflash.com", resource, TR_UPDATEFILELIST_FILENAME, 
                                    cmdif_progressbar_report);
    if(status == S_SUCCESS)
    {
        // -----------------------------------------------------------------------------------------
        // Run checksum compare between server files and device files
        // Any files different or missing, add to the Tune Revision Download List File
        // -----------------------------------------------------------------------------------------
        ftru = genfs_general_openfile(TR_UPDATEFILELIST_FILENAME,"rb");
        if (ftru)
        {
            bytesrw = fread((char*)&trheader, 1, sizeof(trheader), ftru); // Read the file header, contains file count
            if(bytesrw ==  sizeof(trheader))
            {
                // Validate Header
                if( (trheader.version == TRUPDATE_TUNELIST_VERSION) && // Version
                    (strcmp((char*)trheader.partnumber, (const char*)partnumber) == 0) &&  // Partnumber
                    (strcmp((char*)trheader.tuneversion, (const char*)tuneversion) == 0))  // Tune Revision
                {
                    if(trheader.filecount > 0)
                    {
                        ftrd = genfs_general_openfile(TR_DOWNLOADFILELIST_FILENAME,"wb");
                        if(ftrd)
                        {
                            percentage = 0;
                            cmdif_progressbar_setup(cmdif_tracker_get_current_command(),"Scanning for Changes");
                            cmdif_progressbar_report(0);
                            for(i = 0; i < trheader.filecount; i++)
                            {
                                if ((i*100/trheader.filecount) != percentage)
                                {
                                    percentage = i*100/trheader.filecount;
                                    cmdif_progressbar_report(percentage);
                                }

                                // Read each block sequenctially out of the file
                                bytesrw = fread((char*)&trblock, 1, sizeof(trblock), ftru);
                                if(bytesrw ==  sizeof(trblock))
                                {
                                    // Filename and path
                                    strcpy((char*)temp, (const char*)GENFS_DEFAULT_FOLDER_PATH);
                                    strcat((char*)temp, (const char*)trblock.filename);
                                    
                                    status = file_filematch_check(temp, trblock.filesize, trblock.filecrc32e);
                                    if (status != S_SUCCESS) //file is out-of-date or doesn't exist, need to download file
                                    {
                                        totaldownloadsize += trblock.filesize;
                                        totaldownloadcount++;
                                        
                                        // Save filedata structure to download list file
                                        bytesrw = fwrite((char*)&trblock, 1, sizeof(trblock), ftrd);
                                        if(bytesrw > 0)
                                        {
                                            status = S_SUCCESS;
                                        }
                                    }
                                }
                                else
                                {
                                    log_push_error_point(0x4E06);
                                    status = S_READFILE;
                                    break;                        
                                }
                            }
                            
                            sprintf((char*)temp, "Total Count = %d and Size = %d", totaldownloadcount, totaldownloadsize);
                            debug_out(temp);
                            
                            genfs_closefile(ftrd);
                        }
                    }
                    else
                    {
                        log_push_error_point(0x4E07);
                        status = S_ERROR;
                    }
                }
                else
                {
                    log_push_error_point(0x4E08);
                    status = S_INVALID_VERSION; // Problem with Tune Revision File list from server
                }
            }
            else
            {
                log_push_error_point(0x4E09);
                status = S_READFILE;
            }
        }
        genfs_closefile(ftru);
    }

    if (status == S_SUCCESS)
    {
        genfs_deletefile(GENFS_DEFAULT_FOLDER_PATH"lookup.plf.tmp");
        // -----------------------------------------------------------------------------------------
        // All file have been checked, now download from server the required files
        // -----------------------------------------------------------------------------------------
        ftrd = genfs_general_openfile(TR_DOWNLOADFILELIST_FILENAME,"rb");
        if(ftrd)
        {
            for(i = 0; i < totaldownloadcount; i++)
            {
                bytesrw = fread((char*)&trblock, 1, sizeof(trblock), ftrd);
                if(bytesrw > 0)
                {
                    status = S_SUCCESS;

                    // Get FileStream by GUID
                    memset(temp, 0, sizeof(temp));
                    memcpy((char*)temp, (char const*)trblock.rowguid, sizeof(trblock.rowguid));
                    strcpy((char*)resource, "/api/GetFileStreamByGuid?id=");
                    strcat((char*)resource, (char const*)temp);

                    // Filename and path
                    genfs_generalpathcorrection(trblock.filename,sizeof(temp)-1,temp);
                    if (strstr((char*)temp,"\\lookup.plf"))
                    {
                        //this file is very important for tune revision update
                        //download as a tmp file and rename once everything is successful
                        strcat((char*)temp,".tmp");
                        if (!strstr((char*)temp,"\\lookup.plf.tmp"))
                        {
                            //not the lookup.plf we're waiting for, retore the file name
                            genfs_generalpathcorrection(trblock.filename,sizeof(temp)-1,temp);
                        }
                    }//if (strstr...

                    strcpy((char*)tmpString,"Downloading File ");
                    itoa(i+1,&tmpString[strlen((char*)tmpString)],10);
                    strcat((char*)tmpString,"/");
                    itoa(totaldownloadcount,&tmpString[strlen((char*)tmpString)],10);

                    cmdif_progressbar_setup(cmdif_tracker_get_current_command(),tmpString);
                    status = wifi_hal_download_file("services.sctflash.com", resource, temp, 
                                                    cmdif_progressbar_report);
                    if (status == S_SUCCESS)
                    {
                        status = file_filematch_check(temp, trblock.filesize, trblock.filecrc32e);
                        if(status != S_SUCCESS)
                        {
                            log_push_error_point(0x4E0A);
                            break;
                        }
                    }
                    else
                    {
                        log_push_error_point(0x4E0B);
                        break; // Failed file download
                    }
                }
                else
                {
                    log_push_error_point(0x4E0C);
                    status = S_READFILE;
                    break;
                }
            }
            
            genfs_closefile(ftrd);
        }
    }

    cmdif_datainfo_responsedata.responsecode = CMDIF_ACK_FAILED;
    if (status == S_SUCCESS)
    {
        cmdif_progressbar_setup(cmdif_tracker_get_current_command(),"Committing tune files...");
        cmdif_progressbar_report(50);
        //commit lookup.plf to register new tune revision
        if (file_isexist(GENFS_DEFAULT_FOLDER_PATH"lookup.plf.tmp"))
        {
            genfs_deletefile(GENFS_DEFAULT_FOLDER_PATH"lookup.plf.bak");
            //keep a backup of lookup.plf; don't check status because lookup.plf could be corrupted or not exist
            genfs_renamefile(GENFS_DEFAULT_FOLDER_PATH"lookup.plf","lookup.plf.bak");
            status = genfs_renamefile(GENFS_DEFAULT_FOLDER_PATH"lookup.plf.tmp","lookup.plf");
            if (status == S_SUCCESS)
            {
                cmdif_datainfo_responsedata.responsecode = CMDIF_ACK_DONE;
            }
            else
            {
                log_push_error_point(0x4E1C);
            }
        }
        else
        {
            log_push_error_point(0x4E1D);
        }
    }

    return status;
}

/**
 *  cmdif_func_wifi_download_files_by_access_string
 *  
 *  @brief Download files using FileStream By access string (row GUID, vehicle part#, etc
 *  
 *  @param [in] type            CMDIF_ServerAccessType - Type of file to download
 *  @param [in] access_string   ASCII STRING to access file on server
 *  @param [in] dest_filename   local filename to save file as
 *
 *  @retval u8  status
 *  
 *  @details Download files from server by GUID FileStream request
 *  
 *  @authors Patrick Downs
 */
u8 cmdif_func_wifi_download_files_by_access_string(CMDIF_ServerAccessType type,
                                                   u8 *access_string, u8 *dest_filename)
{
    u32 length;
    u8  fullfilename[64];
    u8  hostname[32];
    u8  resource[128];
    u8  status;

    cmdif_datainfo_responsedata.responsecode = CMDIF_ACK_FAILED;
    status = S_FAIL;

    length = strlen((char*)access_string);
    if (length == 0)
    {
        return S_INPUT;
    }

    status = S_INPUT;

    if (type == CMDIF_ServerAccessType_TuneRevision)
    {
        if (length <= 36)
        {
            strcpy((char*)resource, "http://services.sctflash.com/api/GetFileStreamByGuid?id=");
            strcat((char*)resource, (char*)access_string);    // Row GUID
            strcpy((char*)hostname, "services.sctflash.com");
            genfs_generalpathcorrection(dest_filename, (sizeof(fullfilename)-1), fullfilename);
            status = S_SUCCESS;
        }
    }
    else if (type == CMDIF_ServerAccessType_CustomTunes)
    {
        if (length <= 36)
        {
            strcpy((char*)resource, "http://scttunevault.com/api/GetCustomTuneFileDataStream/");
            strcat((char*)resource, (char*)access_string);    // Row GUID
            strcpy((char*)hostname, "scttunevault.com");
            genfs_userpathcorrection(dest_filename, (sizeof(fullfilename)-1), fullfilename);
            status = S_SUCCESS;
        }
    }
    else if (type == CMDIF_ServerAccessType_SPF)
    {
        strcpy((char*)resource, "http://services.sctflash.com/api/GetStockPreloadFile?PartNumber=");
        length += strlen((char*)resource);

        if ((length+20) < sizeof(resource))
        {
            strcat((char*)resource, (char*)access_string);  // Vehicle part number
            strcat((char*)resource, "&EncryptionTypeID=3");
            strcpy((char*)hostname, "services.sctflash.com");
            genfs_userpathcorrection(dest_filename, (sizeof(fullfilename)-1), fullfilename);
            status = S_SUCCESS;
        }
    }
    else
    {
        cmdif_datainfo_responsedata.responsecode = CMDIF_ACK_UNSUPPORTED;
        status = S_INPUT;
    }

    if (status == S_SUCCESS)
    {
        status = wifi_hal_download_file(hostname, resource, fullfilename, 
                                        cmdif_progressbar_report);
    }

    if (status == S_SUCCESS)
    {
        cmdif_datainfo_responsedata.responsecode = CMDIF_ACK_OK;
    }
    
    return status;
}

/**
 *  cmdif_func_wifi_download_firmware_file
 *  
 *  @brief Download firmware files over wifi
 *  
 *  @param [in] fwinfo          firmware info from server
 *  @param [in] mbootfilename   local filename to save mainboot firmware
 *  @param [in] mbpbstring      mainboot progress bar string
 *  @param [in] appfilename     local filename to save app firmware
 *  @param [in] appbstring      app progress bar string
 *
 *  @retval u8  status
 *  
 *  @details Download firmware from update server over wifi
 *  
 *  @authors Patrick Downs
 */
u8 cmdif_func_wifi_download_firmware_files(SERVER_FW fwinfo, u8 *filename, u8 *progress_string)
{
    u8 status;
    u32 crc32etemp;
    u8 host[32];
    u8 resource[100];

    if (!hardwarefeatures_has_wifi())
    {
        return S_NOTSUPPORT;
    }

    // Compare exisiting firmware file CRC with file to be downloaded
    // If CRC is match, don't download again
    file_calcfilecrc32e_byfilename(filename, &crc32etemp, 0x00);
    
    if(crc32etemp == fwinfo.fwcrc32e)
    {
        status = S_SUCCESS; // Already downloaded
    }
    else
    {
        status = wifi_hal_parse_url(fwinfo.url, host, resource);
        if(status == S_SUCCESS)
        {
            cmdif_progressbar_setup(cmdif_tracker_get_current_command(),progress_string);
            status = wifi_hal_download_file(host, resource, filename, 
                                            cmdif_progressbar_report);
            if(status == S_SUCCESS)
            {
                file_calcfilecrc32e_byfilename(filename, &crc32etemp, 0x00);
                if(crc32etemp != fwinfo.fwcrc32e)
                {
                    log_push_error_point(0x4E0D);
                    status = S_CRC32E;
                }
            }
            else
            {
                log_push_error_point(0x4E0F);
            }
        }
        else
        {
            log_push_error_point(0x4E10);
        }
    }
    
    //log_push_error_point(0x4E11); // Abandoned, App FW CRC check fail/Download Fail
    //log_push_error_point(0x4E13); // URL Parse fail
    
    return status;
}

/**
 *  cmdif_func_wifi_apply_update
 *  
 *  @brief Apply available update
 *  
 *  @param [in] update_request
 *
 *  @retval u8  status
 *  
 *  @details Details
 *  
 *  @authors Quyen Leba
 */
u8 cmdif_func_wifi_apply_update(CMDIF_ServerAccessType update_request)
{
    u8  status;
    
    if (!hardwarefeatures_has_wifi())
    {
        cmdif_datainfo_responsedata.responsecode = CMDIF_ACK_NOT_SUPPORTED;
        return S_NOTSUPPORT;
    }

    if (update_request == CMDIF_ServerAccessType_Firmware)
    {
        //log_push_error_point(0x4E14); Error with firmware file
        // ---------------------------------------------------------
        // Check Appboard Firmware files
        // ---------------------------------------------------------
        status = bootloader_updatefromfile_init_ab_boot();
        if(status != S_EXPIRED && status != S_NO_UPDATE && status != S_OPENFILE)
        {
            log_push_error_point(0x4E1E);
            cmdif_datainfo_responsedata.responsecode = CMDIF_ACK_FAILED;
            return status;
        }
        status = bootloader_updatefromfile_init_ab_app();
        if(status != S_EXPIRED && status != S_NO_UPDATE && status != S_OPENFILE)
        {
            log_push_error_point(0x4E1F);
            cmdif_datainfo_responsedata.responsecode = CMDIF_ACK_FAILED;
            return status;
        }
        
        // ---------------------------------------------------------
        // Check Vehicleboard Firmware files
        // ---------------------------------------------------------
        status = bootloader_updatefromfile_init_vb_boot();
        if(status != S_EXPIRED && status != S_NO_UPDATE && status != S_OPENFILE)
        {
            log_push_error_point(0x4E20);
            cmdif_datainfo_responsedata.responsecode = CMDIF_ACK_FAILED;
            return status;
        }
        status = bootloader_updatefromfile_init_vb_app();
        if(status != S_EXPIRED && status != S_NO_UPDATE && status != S_OPENFILE)
        {
            log_push_error_point(0x4E21);
            cmdif_datainfo_responsedata.responsecode = CMDIF_ACK_FAILED;
            return status;
        }
        
        // ---------------------------------------------------------
        // Check Mainboard Firmware files
        // ---------------------------------------------------------
        status = bootloader_updatefromfile_init_mb_boot();
        if(status != S_EXPIRED && status != S_NO_UPDATE && status != S_OPENFILE)
        {
            log_push_error_point(0x4E22);
            cmdif_datainfo_responsedata.responsecode = CMDIF_ACK_FAILED;
            return status;
        }
        status = bootloader_updatefromfile_init_mb_app();
        if(status != S_EXPIRED && status != S_NO_UPDATE && status != S_OPENFILE)
        {
            log_push_error_point(0x4E23);
            cmdif_datainfo_responsedata.responsecode = CMDIF_ACK_FAILED;
            return status;
        }
        
        cmdif_internalresponse_ack(CMDIF_CMD_WIFI_UPDATES_APPLY,CMDIF_ACK_OK,NULL,0);
        
        bootloader_updatefromfile_start();
    }
    else
    {
        cmdif_internalresponse_ack(CMDIF_CMD_WIFI_UPDATES_APPLY,CMDIF_ACK_NOT_SUPPORTED,NULL,0);
    }

    return S_SUCCESS;
}

/**
 *  cmdif_func_wifi_check_tunerevision_update
 *  
 *  @brief Check sever response for newer tune revision
 *  
 *  @param [in] filename Name of file containing the server response data
 *
 *  @retval u8  status
 *  
 *  @details  Compares tune revision version with version from on the server
 *              to detect the if an update exists.
 *          
 *  
 *  @authors Patrick Downs
 */
u8 cmdif_func_wifi_check_tunerevision_update(const u8* filename)
{
    FWUPDATE_HEADER fwheader;
    F_FILE *f;
    u8 temp[16];
    u32 result;
    u32 bytesread;
    u8 status;

    f = genfs_user_openfile(filename,"rb");
    if (f)
    {
        bytesread = fread((char*)&fwheader, 1, sizeof(fwheader), f);            // Read FW Update Header from server response data
        if(bytesread == sizeof(fwheader))
        {
            if(fwheader.structureversion == FWUPDATE_STRUCTURE_VERSION)         // Validate
            {
                status = obd2tune_gettunerevision(temp);
                result = strcmp((char const*)fwheader.tuneversion, (char const*)temp);
                if((result > 0) || status != S_SUCCESS)
                {
                    status = S_EXPIRED; // Needs update
                }
                else
                {
                    status = S_NO_UPDATE; // No update
                }
            }
        }
        else
        {
            log_push_error_point(0x4E15);
        }
        
        genfs_closefile(f);
    }
    else
    {
        log_push_error_point(0x4E16);
        status = S_OPENFILE; // Couldn't open file
    }
    
    return status;
}

/**
 *  cmdif_func_wifi_handleconnect
 *  
 *  @brief Handle Wifi connection related functions
 *  
 *  @param [in] opcode      
 *  @param [in] datain
 *
 *  @retval u8  status
 *  
 *  @details  
 *  
 *  @authors Patrick Downs
 */
u8 cmdif_func_wifi_handleconnect(CMDIF_WIFI_OPCODE opcode, u8 *datain)
{
    u8 status;
    u8  *bptr;
    u32 bufferlength;
    WIFI_HAL_AP_INFO apinfo;

    if (!hardwarefeatures_has_wifi())
    {
        cmdif_datainfo_responsedata.responsecode = CMDIF_ACK_NOT_SUPPORTED;
        return S_NOTSUPPORT;
    }

    status = S_FAIL;
    bufferlength = 0;
    cmdif_datainfo_responsedata.responsecode = CMDIF_ACK_FAILED;
    
    switch(opcode)
    {
    case CMDIF_WIFI_STARTSCAN:
        status = wifi_hal_scan_start((WIFI_HAL_AP_INFO*)&apinfo);
        bufferlength = sizeof(apinfo);
        break;
    case CMDIF_WIFI_GETAPINFO:
        status = wifi_hal_scan_next((WIFI_HAL_AP_INFO*)&apinfo);
        bufferlength = sizeof(apinfo);
        break;
    case CMDIF_WIFI_STOPSCAN:
        status = wifi_hal_scan_stop();
        break;
    case CMDIF_WIFI_ADDPROFILE:
        status = wifi_hal_profile_add((WIFI_HAL_AP_INFO*)datain);
        break;
    case CMDIF_WIFI_DELETEPROFILE:
        status = wifi_hal_profile_delete(datain[0]);
        break;
    case CMDIF_WIFI_RESETANDCONNECT:
        status = wifi_hal_resetandconnect();
        break;
    case CMDIF_WIFI_CONNECT:
        status = wifi_hal_connect((WIFI_HAL_AP_INFO *)datain);
        break;
    case CMDIF_WIFI_DISCONNECT:
        status = wifi_hal_disconnect();
        break;
    default:
        log_push_error_point(0x4E17);
        status = S_NOTSUPPORT;
        break;
    }
    
    if (status == S_SUCCESS)
    {
        if(bufferlength)
        {
            bptr = __malloc(bufferlength);
            if (bptr)
            {
                memcpy((char*)bptr,(char*)&apinfo,bufferlength);
                
                cmdif_datainfo_responsedata.dataptr = bptr;
                cmdif_datainfo_responsedata.datalength = bufferlength;
                cmdif_datainfo_responsedata.responsecode = CMDIF_ACK_OK;
            }
            else
            {
                log_push_error_point(0x4E18);
                status = S_MALLOC;
            }
        }
        else
        {
            cmdif_datainfo_responsedata.responsecode = CMDIF_ACK_OK;
        }
    }
    else
    {
        log_push_error_point(0x4E19);
    }
    
    return status;
}

/**
 *  cmdif_func_wifi_status
 *  
 *  @brief Get the wifi status
 *   
 *  @retval u8  status
 *  
 *  @details  
 *  
 *  @authors Patrick Downs
 */
u8 cmdif_func_wifi_status(void)
{   
    u8 status;
    u8  *bptr;
    WIFI_STATUS_INFO wifiinfo;

    if (!hardwarefeatures_has_wifi())
    {
        cmdif_datainfo_responsedata.responsecode = CMDIF_ACK_NOT_SUPPORTED;
        return S_NOTSUPPORT;
    }

    status = S_FAIL;
    cmdif_datainfo_responsedata.responsecode = CMDIF_ACK_FAILED;
    
    wifi_hal_status(&wifiinfo);
    
    bptr = __malloc(sizeof(wifiinfo));
    if (bptr)
    {
        memcpy((char*)bptr,(char*)&wifiinfo,sizeof(wifiinfo));
        
        cmdif_datainfo_responsedata.dataptr = bptr;
        cmdif_datainfo_responsedata.datalength = sizeof(wifiinfo);
        cmdif_datainfo_responsedata.responsecode = CMDIF_ACK_OK;
        status = S_SUCCESS;
    }
    else
    {
        log_push_error_point(0x4E1A);
        status = S_MALLOC;
    }
    
    return status;
}

/**
 *  cmdif_func_wifi_check_firmware_update
 *  
 *  @brief Check sever response for newer firmware
 *  
 *  @param [in] filename    Name of file containing the server response data
 *  @param [in] versions    Current device firmware versions
 *
 *  @retval u8  status
 *  
 *  @details  Compares firmware version with version from the server to detect
 *              if an update exists.
 *  
 *  @authors Patrick Downs
 */
u8 cmdif_func_wifi_check_firmware_update(const u8* filename, FW_VERSIONS versions)
{
    FWUPDATE_BLOCK fwblock;
    bool mbneedsupdate;
    bool vbneedsupdate;
    bool abneedsupdate;
    u8 status;

    if (!hardwarefeatures_has_wifi())
    {
        return S_NOTSUPPORT;
    }

    status = S_FAIL;
    mbneedsupdate = FALSE;
    vbneedsupdate = FALSE;
    abneedsupdate = FALSE;
    
    status = fwupdate_validate_file(filename); // Validate the file
    if(status == S_SUCCESS)
    {
        // MainBoard
        status = fwupdate_get_block(filename, FWUPDATE_BOARDTYPE_MAINBOARD, FWUPDATE_FWTYPEID_APP, &fwblock);
        if(status == S_SUCCESS)
        {
            if(versions.mb_app_version < atoi((char const*)fwblock.version))
            {
                mbneedsupdate = TRUE;                    
            }
        }
        
        // VehicleBoard
        status = fwupdate_get_block(filename, FWUPDATE_BOARDTYPE_VEHICLEBOARD, FWUPDATE_FWTYPEID_APP, &fwblock);
        if(status == S_SUCCESS)
        {
            if(versions.vb_app_version < atoi((char const*)fwblock.version))
            {
                vbneedsupdate = TRUE;                    
            }
        }
        
        // AppBoard
        status = fwupdate_get_block(filename, FWUPDATE_BOARDTYPE_APPBOARD, FWUPDATE_FWTYPEID_APP, &fwblock);
        if(status == S_SUCCESS)
        {
            if(versions.ab_app_version < atoi((char const*)fwblock.version))
            {
                abneedsupdate = TRUE;                    
            }
        }
        
        if(abneedsupdate | vbneedsupdate | mbneedsupdate)
        {
            status = S_EXPIRED; // Updated files available
        }
        else
        {
            status = S_NO_UPDATE;
        }
    }
    else if(status == S_OPENFILE)
    {
        log_push_error_point(0x4E1B);
    }
    
    return status; 
}

/**
 *  cmdif_func_wifi_get_fwversions
 *  
 *  @brief Gets the current device firmware versions
 *  
 *  @param [out] versions structure containing firmware versions
 *
 *  @retval u8  status
 *  
 *  @details  Compares firmware version with version from the server to detect
 *              if an update exists.
 *  
 *  @authors Patrick Downs
 */
void cmdif_func_wifi_get_fwversions(FW_VERSIONS *versions)
{
    PROPERTIES_MB_INFO mbprops;
    PROPERTIES_VB_INFO vbprops;
    PROPERTIES_AB_INFO abprops;
    u32 info_length;
    u8 status;

    //Mainboard
    status = properties_mb_getinfo((u8*)&mbprops, &info_length);
    if(status != S_SUCCESS)
    {
        memset((u8*)&mbprops, 0, sizeof(mbprops));
    }
    
    //VehicleBoard
    status = properties_vb_getinfo((u8*)&vbprops, &info_length);
    if(status != S_SUCCESS)
    {
        memset((u8*)&vbprops, 0, sizeof(vbprops));
    }
    
    
    //Appboard
    status = properties_ab_getinfo((u8*)&abprops, &info_length);
    if(status != S_SUCCESS)
    {
        memset((u8*)&abprops, 0, sizeof(abprops));
    }
    
    versions->mb_fboot_version = mbprops.failsafeboot_version;
    versions->mb_mboot_version = mbprops.mainboot_version;
    versions->mb_app_version = (u32)(mbprops.app_version*1000);
    versions->mb_app_version += mbprops.app_build;
    versions->vb_fboot_version = vbprops.failsafeboot_version;
    versions->vb_mboot_version = vbprops.mainboot_version;
    versions->vb_app_version = (u32)(vbprops.app_version*1000);
    versions->vb_app_version += vbprops.app_build;
    versions->ab_fboot_version = abprops.failsafeboot_version;
    versions->ab_mboot_version = abprops.mainboot_version;
    versions->ab_app_version = (u32)(abprops.app_version*1000);
    versions->ab_app_version += abprops.app_build;
}

/**
 *  cmdif_func_wifi_diffile_createnew
 *  
 *  @brief  Write a default header to new dif file
 *
 *  @retval u8  status
 *  
 *  @details  
 *  
 *  @authors Patrick Downs
 */
u8 cmdif_func_wifi_diffile_createnew(F_FILE *fdfl)
{
    u8 status;
    DIFF_FILE_HEADER header;
    u32 writesize;
    
    if(fdfl)
    {
        header.version = DIF_VERSION;
        header.filecount = 0;
        header.headercrc32e = 0xFFFFFFFF;
        header.contentcrc32e = 0xFFFFFFFF;
        memset((char*)&header.reservered, 0, sizeof(header.reservered));
        
        writesize = fwrite((char*)&header, 1, sizeof(header), fdfl);
        if(writesize == sizeof(header))
        {
            status = S_SUCCESS;
        }
        else
        {
            status = S_WRITEFILE;
        }
    }
    else
    {
        status = S_INPUT;
    }
    
    return status;
}


/**
 *  cmdif_func_wifi_diffile_append
 *  
 *  @brief Adds file info to diff file
 *  
 *  @param [in] fdif        Diff file pointer
 *  @param [in] tuneinfo    Tunefile info block from server
 *
 *  @retval u8  status
 *  
 *  @details  Appends new file info to currently open dif file
 *  
 *  @authors Patrick Downs
 */
u8 cmdif_func_wifi_diffile_append(F_FILE *fdif, SERVER_TUNEFILE_INFO tuneinfo)
{
    u8 status;
    u32 writesize;
    
    DIFF_FILE_BLOCK block;

    memset((char*)&block,0,sizeof(block));
    strcpy((char*)&block.description, (char const*)&tuneinfo.tunedescription);
    block.description[sizeof(block.description)-1] = NULL;
    strcpy((char*)&block.filename, (char const*)&tuneinfo.filename);
    strcpy((char*)&block.rowguid, (char const*)&tuneinfo.rowguid);
    block.filecrc32e = tuneinfo.crc32e;
    block.filesize = tuneinfo.storedfilesize;
    
    status = crypto_blowfish_encryptblock_external_key((u8*)&block, sizeof(block));
    if(status == S_SUCCESS)
    {
        writesize = fwrite((char*)&block, 1, sizeof(block), fdif);
        if(writesize != sizeof(DIFF_FILE_BLOCK))
        {
            status = S_WRITEFILE;
        }
    }
    
    return status;
}


/**
 *  cmdif_func_wifi_diffile_finish
 *  
 *  @brief  Writes updated header to file
 *  
 *  @param [in] fdif        Diff file pointer
 *
 *  @retval u8  status
 *  
 *  @details  Writes updated header to the file with corrected CRCs for header and content
 *  
 *  @authors Patrick Downs
 */
u8 cmdif_func_wifi_diffile_finish(F_FILE *fdif)
{
    u8 status;
    u32 size;
    u32 fpos_last;
    u32 contentlength;
    int i;
    
    DIFF_FILE_HEADER header;
    DIFF_FILE_BLOCK block;
    
    fpos_last = ftell(fdif); // Save las file position, we'll restore it later
    
    // Get Content Length to calculate file count
    fseek(fdif, 0, SEEK_END); // Seek to end of file
    
    contentlength = ftell(fdif) - sizeof(header); 
    
    header.version = DIF_VERSION;
    header.filecount = (contentlength / sizeof(block));
    header.headercrc32e = 0xFFFFFFFF;
    header.contentcrc32e = 0xFFFFFFFF;
    memset((char*)&header.reservered, 0, sizeof(header.reservered));
    
    fseek(fdif, sizeof(header), SEEK_SET); // Seek to begining of content
    
    status = S_SUCCESS;
    crc32e_reset();
    
    for(i = 0; i < header.filecount; i++) // Calculate content CRC
    {
        size = fread((char*)&block, 1, sizeof(block), fdif); 
        if(size == sizeof(block))
        {
            header.contentcrc32e = crc32e_calculateblock(header.contentcrc32e,(u32*)&block, (sizeof(block)/4));
        }
        else
        {
            status = S_READFILE;
            break;            
        }
    }
    
    if(status == S_SUCCESS)
    {
        // Calculate header CRC
        crc32e_reset();
        header.headercrc32e = crc32e_calculateblock(header.headercrc32e,(u32*)&header, (sizeof(header)/4));
        
        status = crypto_blowfish_encryptblock_external_key((u8*)&header, sizeof(header));
        if(status == S_SUCCESS)
        {
            fseek(fdif, 0, SEEK_SET); // Seek to begining of file to write header
            size = fwrite((char*)&header, 1, sizeof(header), fdif);
            if(size == sizeof(header))
            {
                status = S_SUCCESS;
            }
            else
            {
                status = S_WRITEFILE;
            }
        }
    }
    
    fseek(fdif, fpos_last, SEEK_SET); // Restore file pointer position
    
    return status;
}

/**
 *  cmdif_func_wifi_diffile_validatefile
 *  
 *  @brief Validates the Diff File. 
 *
 *  @retval u8  status
 *  
 *  @details  Validates that the dif file
 *  
 *  @authors Patrick Downs
 */
u8 cmdif_func_wifi_diffile_validatefile(void)
{
    u8 status;    
    F_FILE* f;
    u32 crc32e;
    u32 readsize;
    int i;
    
    DIFF_FILE_HEADER header;
    DIFF_FILE_BLOCK block;

    f = genfs_user_openfile(CTB_DIF_FILENAME,"rb"); // Try to to open Diff File
    if(f)
    {
        // 1st - Validate Header
        readsize = fread((char*)&header, 1, sizeof(header), f);
        
        status = crypto_blowfish_decryptblock_external_key((u8*)&header, sizeof(header));
        if(status == S_SUCCESS)
        {   
            status = cmdif_func_wifi_diffile_validateheader(header, NULL); 
            if(status == S_SUCCESS)
            {
                // 2nd - Validate content
                status = S_SUCCESS;
                crc32e_reset();
                crc32e = 0xFFFFFFFF;
                for(i = 0; i < header.filecount; i++)
                {
                    readsize = fread((char*)&block, 1, sizeof(block), f);
                    if(readsize == sizeof(block))
                    {
                        crc32e = crc32e_calculateblock(crc32e,(u32*)&block, (sizeof(block)/4));
                    }
                    else
                    {
                        status = S_READFILE;
                        break;
                    }
                }
                
                if(status == S_SUCCESS)
                {
                    if(header.contentcrc32e != crc32e)
                    {
                        status = S_CRC32E; // Calculated CRC doesn't match header CRC
                    }
                }
            }
        }
        fclose(f);
    }
    else
    {
        status = S_OPENFILE;
    }
    
    return status;
}

/**
 *  cmdif_func_wifi_diffile_validateheader
 *  
 *  @brief Validates the Diff File header
 *
 *  @param [in] header    Decrypted header
 *  @param [out] retcrc   Pointer to return location for updated CRC, NULL - if no return CRC
 *
 *  @retval u8  status
 *  
 *  @details  Validates the Diff File header 
 *  
 *  @authors Patrick Downs
 */
u8 cmdif_func_wifi_diffile_validateheader(DIFF_FILE_HEADER header, u32 *retcrc)
{
    u8 status;
    u32 headercrc;
    u32 crc32e;    
    
    headercrc = header.headercrc32e;    // Save current CRC for comparision
    
    crc32e_reset();
    crc32e = 0xFFFFFFFF;
    header.headercrc32e = 0xFFFFFFFF; // CRC calc requires init value of all 0xFF's
    
    crc32e = crc32e_calculateblock(crc32e,(u32*)&header, (sizeof(header)/4));
    if(crc32e == headercrc)
    {
        if(header.version == DIF_VERSION)
        {
            status = S_SUCCESS;
        }
        else
        {
            status = S_INVALID_VERSION;
        }
    }
    else
    {
        status = S_CRC32E;
    }
    
    if(retcrc)
    {
        *retcrc = crc32e; // Return calculated CRC if pointer is valid
    }
    
    return status;
}

