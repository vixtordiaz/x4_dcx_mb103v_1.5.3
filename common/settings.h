/**
  **************** (C) COPYRIGHT 2011 SCT Performance, LLC *********************
  * File Name          : settings.h
  * Device / System    : SCT products using the "CommonCode" platform
  * Author             : Quyen Leba
  *
  * Version            : 1 
  * Date               : 05/02/2011
  * Description        : 
  *                    : 
  *
  *
  * History            : 05/02/2011 M. Davis
  *                    :   Add SCT Copyright header,
  *
  ******************************************************************************
  */

#ifndef __SETTINGS_H
#define __SETTINGS_H

#include <arch/gentype.h>
#include <common/obd2def.h>
#include <common/log.h>

#define DEVICE_SERIAL_NUMBER_LENGTH             13
#define MICO_NORMAL_OPERATION_SIGNATURE         0xAA558F31
#define SETTINGS_DEMO_USE_FS                    0

typedef enum
{
    BooloaderType_Regular       = 0x00, // Regular bootloader
    BooloaderType_Recovery      = 0x01, // Recovery bootloader 
    BooloaderType_App           = 0x02, // Boot Application
    BooloaderType_Production    = 0x03, // Boot application in production-programming mode
}BootloaderType;

//typedef enum
//{
//    CriticalSettingsVersion,
//    TuneSettingsVersion,
//    DatalogGeneralSettingsVersion,
//    SerialNumber,
//    BluetoothBaudrate,
//    MarketType,
//    DeviceType,
//    
//    VehicleType,
//    CommType,
//    MarriedCount,
//    MarriedStatus,
//    ProcessorCount,
//    VehicleCodes,
//    VIN,
//    PreviousVIN,
//    EPATS,
//    VID,
//    StockFileSize,
//    
//    ProgrammedFlashType,
//    TuneDescription,
//    TuneFileName,
//    TuneOptionFileName,
//    
//}SettingsParameter;


//TODOQ: this here for now
typedef union
{
  struct
  {
    short Market;
    short Hardware; 
    short FirmwareVersion;
    short Build;
  }s;
  struct
  {
    int MH;
    int FB;
  }i;
}FirmwareVersion_T;
#define MAX_TUNE_VER_STRING   12  //string in tuneversion.txt should be no more than 12 bytes

// -----------------------------------------------------------------------------
//  IMPORTANT!!
//  Opcodes are matched to TSX application code(k_code_settings.h) and PC Admin 
//  Any changes made to opcodes need to be updated in above applications also!
// -----------------------------------------------------------------------------
typedef enum
{
    SETTINGS_OPCODE_FLAGS                       = 20,
    SETTINGS_OPCODE_DEMO_MODE                   = 21,
    SETTINGS_OPCODE_VERSION                     = 30,
    SETTINGS_OPCODE_SERIAL                      = 31,
    SETTINGS_OPCODE_HARDWARE_ID                 = 32,
    SETTINGS_OPCODE_DEVICE_INFO                 = 33,
    SETTINGS_OPCODE_PROGRAMMER_INFO_P1          = 34,   //married, etc
    SETTINGS_OPCODE_PROGRAMMER_INFO_P2          = 35,   //
    SETTINGS_OPCODE_PROGRAMMER_INFO_P3          = 36,   //reserved
    SETTINGS_OPCODE_PROGRAMMER_INFO_P4          = 37,   //reserved
    SETTINGS_OPCODE_MAC_ADDRESS                 = 38,
    SETTINGS_OPCODE_KERNEL_ID                   = 39,   //reserved
    SETTINGS_OPCODE_MODE                        = 40,
    SETTINGS_OPCODE_STATUS                      = 41,
    SETTINGS_OPCODE_CAPACITY                    = 42,
    SETTINGS_OPCODE_FREESPACE                   = 43,
    SETTINGS_OPCODE_INIT_SETTINGS               = 44,
    SETTINGS_OPCODE_RESET_SETTINGS              = 45,
    SETTINGS_OPCODE_BRIGHTNESS                  = 46,
    SETTINGS_OPCODE_VOLUME                      = 47,
    SETTINGS_OPCODE_MICO_SIGNATURE              = 48,
    SETTINGS_OPCODE_TUNE_REVISION               = 49,
    SETTINGS_OPCODE_DEVICE_PARTNUMBER_STRING    = 50,
    SETTINGS_OPCODE_DEVICE_PROPERTIES           = 51,
    SETTINGS_OPCODE_ERRORPOOL                   = 52,
    SETTINGS_OPCODE_DEVICE_TYPE                 = 53,
    SETTINGS_OPCODE_MARKET_TYPE                 = 54,
    SETTINGS_OPCODE_DEVICE_CRITICAL_FLAGS       = 55,
    SETTINGS_OPCODE_DEVICE_TUNE_FLAGS           = 56,
    SETTINGS_OPCODE_DEVICE_FLEET_FLAGS          = 57,
    SETTINGS_OPCODE_DEVICE_DATALOG_GENERAL_FLAGS= 58,
    SETTINGS_OPCODE_PRELOADEDTUNE_LOOKUP_CRC32E = 59,
    SETTINGS_OPCODE_PRELOADEDTUNE_RESTRICTION   = 60,
    SETTINGS_OPCODE_DEVICE_VERSION_STRING       = 61,
    
    SETTINGS_OPCODE_ANALOGPORT_EXT5V            = 62,
    SETTINGS_OPCODE_ANALOGPORT_GPIO1            = 63,
    
    SETTINGS_OPCODE_PRODUCTIONTEST_STATUS       = 64,
    
    SETTINGS_OPCODE_STOCKFILESIZE               = 80,
    SETTINGS_OPCODE_VID                         = 81,
    SETTINGS_OPCODE_EPATS                       = 82,
    SETTINGS_OPCODE_VEHICLE_CODES               = 83,
    SETTINGS_OPCODE_VEHICLE_SERIAL_NUMBER       = 84,
    SETTINGS_OPCODE_VIN                         = 85,
    SETTINGS_OPCODE_PREV_VIN                    = 86,
    SETTINGS_OPCODE_TUNEINFOTRACK               = 87,
    SETTINGS_OPCODE_TUNEHISTORYINFO             = 88,
    
    SETTINGS_OPCODE_TUNE_HISTORY_INFO,
    SETTINGS_OPCODE_SPECIAL_STOCK_UPLOAD_INFO,
    
    //-------- Dongle Specific Opcodes -------
    SETTINGS_OPCODE_ENABLE_MASS_STORAGE         = 200,
}SETTINGS_OPCODE;

typedef enum
{
    SettingsDeviceFlags_None                    = 0,
    SettingsDeviceFlags_OffRoad                 = (1<<0),
    SettingsDeviceFlags_50StateLegal            = (1<<1),
    SettingsDeviceFlags_NoPreloadedTuneSupport  = (1<<2),
    SettingsDeviceFlags_NoCustomTuneSupport     = (1<<3),
    SettingsDeviceFlags_UploadOnlySupport       = (1<<4)
    //Bit15..5: reserved (set to ZERO)
}SettingsDeviceFlags;

typedef enum
{
    SettingsRegion_Critical                     = 0,
    SettingsRegion_Tune                         = 1,
    SettingsRegion_Fleet                        = 2,
    SettingsRegion_DatalogGeneral               = 3,
}SettingsRegion;

typedef struct
{
    float a[2];
    float b[2];
    u8 type[2];
}CustomEq;

#define SETTINGS_CRITICAL_SERIALNUMBER_LENGTH               16
#define SETTINGS_CRITICAL_DEVICEPARTNUMBERSTRING_LENGTH     32
#define SETTINGS_CRITICAL_PRODUCTIONTESTVALID_SIG           0xAA5513F8

typedef struct
{
    u8  version;
    u8  sct[3];
    u32 signature;
    u32 crc32e;
    u32 flags;              //lower 16 bits used as SettingsDeviceFlags
    u32 normaloperation_signature;
    //----------------
    u32 firmware_version;
    u32 devicetype;
    u32 markettype;
    u32 baudrate;           //only applicable to devices support Bluetooth
    u8  serialnumber[SETTINGS_CRITICAL_SERIALNUMBER_LENGTH];    //only use 13 bytes now
    u8  devicepartnumberstring[SETTINGS_CRITICAL_DEVICEPARTNUMBERSTRING_LENGTH];    
    u32 productiontest_signature;
    struct
    {                               // TRUE=PASS or FALSE=FAIL
        u32  complete_status : 1;   // Overall Production Test status
        u32  task0_status    : 1;   // PRODUCTION_TEST_TASK_COMMLINK
        u32  task1_status    : 1;   // PRODUCTION_TEST_TASK_REV
        u32  task2_status    : 1;   // PRODUCTION_TEST_TASK_FORMAT
        u32  task3_status    : 1;   // PRODUCTION_TEST_TASK_VEHICLE_COMM
        u32  task4_status    : 1;   // PRODUCTION_TEST_TASK_ADC
        u32  task5_status    : 1;   // PRODUCTION_TEST_TASK_LED
        u32  task6_status    : 1;   // PRODUCTION_TEST_TASK_USB
        u32  task7_status    : 1;   // PRODUCTION_TEST_TASK_WIFI
        u32  task8_status    : 1;   // PRODUCTION_TEST_TASK_VBBOOTEN
        u32  task9_status    : 1;   // PRODUCTION_TEST_TASK_COMPLETED
        u32  reserved0       : 21;
    }productiontest_status;
}Settings_Critical;

typedef struct
{
    u8  version;
    u8  sct[3];
    u32 signature;
    u32 crc32e;
#define PRELOADEDTUNE_RESTRICTION_MASK                          ((u32)(7<<8))
#define PRELOADEDTUNE_RESTRICTION_NONE                          ((u32)0<<8)
#define PRELOADEDTUNE_RESTRICTION_ENABLED                       ((u32)1<<8)
#define PRELOADEDTUNE_RESTRICTION_ENABLED_WITH_REMOVAL          ((u32)2<<8)
//Bit9: abandoned (DO NOT USED)                                 ((u32)1<<9)
    u32 flags;  //Bit[0..7]: reserved (ZERO), Bit[31..13]: reserved (ZERO)
                //Bit[10..8]: 0 (none), 1 (disable preloaded tune support)
                //            2 (disable preloaded tune support w/ custom tune removal when re-enable)
                //            other (reserved)
                //Bit9: abandoned (DO NOT USED) !!!
    u16 veh_type;
    u8  comm_type;
    u8  comm_level;
    u8  oem_type;
    u8  married_count;
#define MARRIED                         (1<<0)
#define DOWNLOAD_FAILED                 (1<<4)
#define RECOVERY_STOCK                  (1<<5)
#define DOWNLOAD_HEALTH_CHECK_FAILED    (1<<6)
    u8  married_status;
    u8  processor_count;
    u8  vehiclecodes[128];      //e.g. "code1*code2*", available after upload
    u8  vin[VIN_LENGTH+1];
    u8  prev_vin[VIN_LENGTH+1];
    // must be the same size as flasher_info.vehicle_serial_number
    // do not change it
    u8  vehicle_serial_number[32];
    u8  epats[14];
    u8  vid[256];
    u32 preloadedlookup_crc32einfo;
    //to track current record of LOG_TUNEHISTORY_FILENAME and limit its filesize
    u16 tunehistoryindex;
    u32 tunehistoryflashcount;
    u32 stockfilesize;

    struct
    {
        u8  flashtype;
#define TUNEUSEOPTION_NONE          (0)
#define TUNEUSEOPTION_REGULAR       (1<<0)
#define TUNEUSEOPTION_SPECIAL       (1<<1)
        u8  useoption[3];
#define TUNEFLAGS_FULLFLASH_ECM0    (1<<0)
#define TUNEFLAGS_FULLFLASH_ECM1    (1<<1)
#define TUNEFLAGS_FULLFLASH_ECM2    (1<<2)
        u32 tuneflags;
        u8  tunedescriptions[3][40];
        u8  tunefilenames[3][40];
        u8  optionfilenames[3][40];
        u8  reserved[16];
        struct
        {
            u8  reserved[2];
            //0: none; 1: enabled; others: reserved
            u8  pcm : 4;
            //0: none; 1: enabled; others: reserved
            u8  tcm : 4;
            u8  powerlevel;
        }otfinfo;       //OnTheFly tuning info
    }tuneinfotrack;

    //these parameters are for the upload stock only feature
    u32 special_stockfilesize;
    u16 special_vehicletype;
    u16 reserved1;
    u32 stockfile_crc32e;
    u32 flashfile_crc32e;
    u32 recv_id_crc32e;     //crc32e of recv_id.bak file
    struct
    {
        u32 tire;      		//Used by DCX
        u32 axel;       	//Used by DCX
    }stockwheels;
    //same size as vehiclecodes[128] but to store codes of current tune flashed
    //into vehicle
    u8  customtune_vehiclecodes[128];       // Strategy of custom tune file
    u8  customtune_secondvehiclecodes[128]; // Partnumbers of custom tune file
    union
    {
        u8  secondvehiclecodes[128];      //Partnumbers of stock file
        u8  variant_ID;                   // variant ID of bootloader
    };
    union
    {
        // VID CRC32e used if blank VIN to deciede if married count should be decremented
        u32 current_vid_crc32e;
        // offset if bootloader has been moved by another tuner
        u32 DCX_Bootloader_offset; 
    };
    u32 previous_vid_crc32e;
    u32 stockspoptsfile_crc32e;     //keep crc32e of stockspopts.bin
}Settings_Tune;
STRUCT_MAX_SIZE_CHECK(Settings_Tune,2048);

typedef struct
{
    u8  version;
    u8  sct[3];
    u32 signature;
    u32 crc32e;
    u32 flags;
    u32 flash_limit;
    u32 married_count;
    u8  strategy[32];
}Settings_Fleet;
STRUCT_MAX_SIZE_CHECK(Settings_Fleet,1024);

typedef struct
{
    u8  version;
    u8  sct[3];
    u32 signature;
    u32 crc32e;
    u32 flags;
    
    u16 backlight;
    u16 contrast;
    
    u8  reserved_1[64];
    error_point_pool errorpointpool;
    
    u8  selected_pids[30];
    u8  analog1_on;
    u8  analog1_equation;
    u8  analog1_selected_index;
    u8  analog1_description[24];
    u8  analog2_on;
    u8  analog2_equation;
    u8  analog2_selected_index;
    u8  analog2_description[24];
    CustomEq
        analogcustomequation[2];
    u8  alert_selected_index[6];
    float
        alertmax[6];

#define EXT5VCONFIG_AUTO    0x00
#define EXT5VCONFIG_OFF     0x01
#define EXT5VCONFIG_ON      0x02
#define GPIO1CONFIG_AUTO    0x00
#define GPIO1CONFIG_OFF     0x01    
#define GPIO1CONFIG_ON      0x02
    struct
    {
        u8 ext5Vconfig:4;
        u8 gpio1config:4;    
    }analog_port;
    struct
    {
        u8  demo_mode       : 1;
        u8  reserved0       : 7;
        u8  reserved1;
    }control;
}Settings_DatalogGeneral;
STRUCT_MAX_SIZE_CHECK(Settings_DatalogGeneral,1024);

typedef struct
{
    u32 married_count;
    u8  married_status; //Bit0: HIGH (married), Bit4: HIGH (download failed)
    u8  reserved1;
    u8  oem_type;
    u8  comm_type;
    u8  comm_level;
    u8  processor_count;
    u16 flags;          //see SettingsDeviceFlags
    u32 veh_type;       //i.e. binary type, eec type, etc
    u32 stockfilesize;
}SettingsProgrammerInfoP2;
STRUCT_SIZE_CHECK(SettingsProgrammerInfoP2,20);

//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
extern bool iscriticalarea_dirty;
extern bool istunearea_dirty;
extern bool isfleetarea_dirty;
extern bool isdataloggeneralarea_dirty;

extern Settings_Critical settingscritical;
extern Settings_Tune settingstune;
extern Settings_Fleet settingsfleet;
extern Settings_DatalogGeneral settingsdataloggeneral;

#define SETTINGS_CRITICAL(def)          settingscritical.##def
#define SETTINGS_TUNE(def)              settingstune.##def
#define SETTINGS_FLEET(def)             settingsfleet.##def
#define SETTINGS_DATALOG(def)           settingsdataloggeneral.##def
#define SETTINGS_GENERAL(def)           settingsdataloggeneral.##def

#define SETTINGS_SetCriticalAreaDirty() (iscriticalarea_dirty = TRUE)
#define SETTINGS_SetTuneAreaDirty()     (istunearea_dirty = TRUE)
#define SETTINGS_SetFleetAreaDirty()    (isfleetarea_dirty = TRUE)
#define SETTINGS_SetGeneralAreaDirty()  (isdataloggeneralarea_dirty = TRUE)
#define SETTINGS_SetDatalogAreaDirty()  (isdataloggeneralarea_dirty = TRUE)

#define SETTINGS_Is50StateLegalDevice() ((settingscritical.flags & SettingsDeviceFlags_50StateLegal) == SettingsDeviceFlags_50StateLegal)
#define SETTINGS_IsOffRoadDevice()      ((settingscritical.flags & SettingsDeviceFlags_OffRoad) == SettingsDeviceFlags_OffRoad)
#define SETTINGS_IsNoPreloadedDevice()  ((settingscritical.flags & SettingsDeviceFlags_NoPreloadedTuneSupport) == SettingsDeviceFlags_NoPreloadedTuneSupport)
#define SETTINGS_IsNoCustomDevice()     ((settingscritical.flags & SettingsDeviceFlags_NoCustomTuneSupport) == SettingsDeviceFlags_NoCustomTuneSupport)
#define SETTINGS_HasUploadOnly()        ((settingscritical.flags & SettingsDeviceFlags_UploadOnlySupport) == SettingsDeviceFlags_UploadOnlySupport)

#define SETTINGS_IsPreloadedTuneDisabled()  (((settingstune.flags & PRELOADEDTUNE_RESTRICTION_ENABLED_WITH_REMOVAL) == PRELOADEDTUNE_RESTRICTION_ENABLED_WITH_REMOVAL) || \
    ((settingstune.flags & PRELOADEDTUNE_RESTRICTION_ENABLED) == PRELOADEDTUNE_RESTRICTION_ENABLED))

#define SETTINGS_IsMarried()            ((settingstune.married_status & MARRIED) == MARRIED)
#define SETTINGS_SetMarried()           (settingstune.married_status |= MARRIED)
#define SETTINGS_ClearMarried()         (settingstune.married_status &= ~(MARRIED))

#define SETTINGS_IsRecoveryStockSet()   ((settingstune.married_status & RECOVERY_STOCK) == RECOVERY_STOCK)
#define SETTINGS_SetRecoveryStock()     (settingstune.married_status |= RECOVERY_STOCK)
#define SETTINGS_ClearRecoveryStock()   (settingstune.married_status &= ~(RECOVERY_STOCK))

#define SETTINGS_IsDownloadFail()       ((settingstune.married_status & DOWNLOAD_FAILED) == DOWNLOAD_FAILED)
#define SETTINGS_SetDownloadFail()      (settingstune.married_status |= DOWNLOAD_FAILED)
#define SETTINGS_ClearDownloadFail()    (settingstune.married_status &= ~(DOWNLOAD_FAILED))

#define SETTINGS_IsStockRecovery()      ((settingstune.married_status & RECOVERY_STOCK) == RECOVERY_STOCK)
#define SETTINGS_SetStockRecovery()     (settingstune.married_status |= RECOVERY_STOCK)
#define SETTINGS_ClearStockRecovery()   (settingstune.married_status &= ~(RECOVERY_STOCK))

#define SETTINGS_IsDownloadHealthCheckFail()    ((settingstune.married_status & DOWNLOAD_HEALTH_CHECK_FAILED) == DOWNLOAD_HEALTH_CHECK_FAILED)
#define SETTINGS_SetDownloadHealthCheckFail()   (settingstune.married_status |= DOWNLOAD_HEALTH_CHECK_FAILED)
#define SETTINGS_ClearDownloadHealthCheckFail() (settingstune.married_status &= ~(DOWNLOAD_HEALTH_CHECK_FAILED))

#define SETTINGS_IsFullFlash(ecm)       ((settingstune.tuneinfotrack.tuneflags & (1<<ecm)) == (1<<ecm))
#define SETTINGS_SetFullFlash(ecm)      (settingstune.tuneinfotrack.tuneflags |= (1<<ecm))
#define SETTINGS_ClearFullFlash(ecm)    (settingstune.tuneinfotrack.tuneflags &= ~((u32)1<<ecm))

#define SETTINGS_IsUserSelect()         ((settingstune.tuneinfotrack.tuneflags & (1<<30)) == (1<<30))
#define SETTINGS_SetUserSelect()        (settingstune.tuneinfotrack.tuneflags |= (1<<30))
#define SETTINGS_ClearUserSelect()      (settingstune.tuneinfotrack.tuneflags &= ~((u32)1<<30))

#define SETTINGS_IsDemoMode()           (settingsdataloggeneral.control.demo_mode == 1)
#define SETTINGS_SetDemoMode()          (settingsdataloggeneral.control.demo_mode = 1)
#define SETTINGS_ClearDemoMode()        (settingsdataloggeneral.control.demo_mode = 0)

#define SETTINGS_GetDeviceType()        (settingscritical.devicetype)
#define SETTINGS_GetMarketType()        (settingscritical.markettype)

//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
// Picked from GMX3P settings.c
//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
#define Settings_GetMarketType       (SettingsInfo.MarketType)
#define Settings_GetDeviceType       (SettingsInfo.DeviceType)
#define     FWverSize 24    //version.c

typedef struct
{
    u8          BuzzerDisabledUSB; // Disable sounds when USB-powered
    u32         PreviousMarketType;   // test & update after firmware download
    u32         PreviousDeviceType;   // test & update after firmware download
    u8          PreviousFWVersion[FWverSize];   // test & update after firmware download
}SettingsFlags;
extern SettingsFlags settingsFlags;

//force to update settings
#define SETTINGS_FORCE_UPDATE               TRUE
//only update settings if dirty
#define SETTINGS_UPDATE_IF_DIRTY            FALSE
u8 settings_update(bool forced_update);
u8 settings_load();
u8 settings_getsettings_byopcode(SETTINGS_OPCODE opcode, u16 privdata,
                                  u8 *settings_data, u32 *settings_len);
u8 settings_setsettings_byopcode(SETTINGS_OPCODE opcode, u16 privdata,
                                  u8 *settings_data, u32 settings_len);
u8 settings_setproductiondefault();
u8 settings_restoredefault();
u8 settings_activate_mico_signature();
u8 settings_get_deviceserialnumber(u8 *serialnumber);
u8 settings_compare_deviceserialnumber(u8 *serialnumber);
u8 settings_get_devicepartnumberstring(u8 *devicepartnumberstring);
u8 settings_compare_devicepartnumberstring(u8 *partnumberstring);
u8 settings_recoverystock_set(u32 stockfilesize,
                              u32 filestock_crc32e, u32 filestock_option_crc32e,
                              u16 veh_type, u8 variantid);
u8 settings_get_unlock_tea_key(u32 markettype, u32 *key);
u8 settings_check();
u8 settings_register_demo_mode();
u8 settings_unregister_demo_mode();

u8 settings_set_productiontest(u8 prodtest, bool teststatus);
u8 settings_get_productiontest(u32 *retstatus);

#endif  //__SETTINGS_H
