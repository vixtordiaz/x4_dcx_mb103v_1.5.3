#ifndef __CLOUD_LIB_H
#define __CLOUD_LIB_H

#include <common/statuscode.h>
#include <arch/gentype.h>
#include <common/function_ptr_defs.h>

#define CLOUD_PIN_LENGTH 6

typedef struct {
    u8* host;
    u8* contenttype;
    u8 serialnumber[128];
    u8 password[128];
    u8 pin[5];
    u8 username[128];
    u8 domainname[128];
    u8* token;
    u32 tokenlength;
    u8 tokenexpireson[32];
}CloudData;

typedef struct
{
    bool accountrequired;
    u32 limit;
    u8* message;
}CloudCheckLimit;

u8 test_func();

u8 cloud_activate(u8 *serialnumber);
u8 cloud_deactivate();
u8 cloud_getpin(u8* pin);

u8 cloud_update_tunelist(u8 *serialnumber);
u8 cloud_get_tunelist_length(u32 *stringlength);
u8 cloud_get_tunelist(u8 **list, u32 *list_length);
u8 cloud_download_file_by_searchlist_index(u16 index, PROGRESS_REPORT_FUNC_SIMPLE progress_report_func);
u8 cloud_handle_activation_authentication(u8 *serialnumber);
u8 cloud_checklimit(u8 *serialnumber, CloudCheckLimit *limitdata);


#endif //__CLOUD_LIB_H
