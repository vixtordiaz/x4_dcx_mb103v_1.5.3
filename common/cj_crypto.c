
#include <arch/gentype.h>
#include <board/genplatform.h>
#include "cj_crypto.h"


u8 RotateLeft(u8 value, s32 bits)
{                    
    while(bits-- > 0)
    {
        value = (value<<1)|(value>>7);
    }             
    return value;
}

u8 RotateRight(u8 value, s32 bits)
{
    while(bits-- > 0)
    {
        value = (value>>1)|(value<<7);
    }             
    return value;
}


u8 cj_crypto_encrypt(u8 *src, u8 *dest, u32 length)
{
    u8 status;
    
    s32 rValue;
    s32 encryptPad;
    
    u8 stolenBits[8];
    u8 insertBits[8];    
    u8 mask[7];
    u8 *oData;
    u8 *toEncrypt;
    s32 toEncryptLength;
        
    status = 0;
    rValue = 55; // random number
    rValue = rValue - (rValue % 2);
    encryptPad = 0;
    mask[0] = 0x02;
    mask[1] = 0x04;
    mask[2] = 0x08;
    mask[3] = 0x10;
    mask[4] = 0x20;
    mask[5] = 0x40;
    mask[6] = 0x80;
    
    if (length < 8)
        encryptPad = (8 - length);  //cj verify that the encryption data is at least 8 bytes -> lose byte 8
    
    toEncryptLength = length + encryptPad;
    
    for(int i = length; i < toEncryptLength; i++) // fill the padding with 0x00 -> note MIN DATA Size is 7 with byte 8 holding other data.
    {
        toEncrypt[i] = 0x00;
    }
    
    for (int x = 0; x < 8; x++)
    {
        insertBits[x] = (u8)((rValue >> x) & 1); 
    }
    
//    memcpy(oData, src, length);
    oData = src;
    toEncrypt = dest;
    
    for (int i = 0; i < toEncryptLength; i++)  //cj encrypt and encode
    {
//        toEncrypt[i] = Bits.RotateRight(oData[i], rValue);  
        toEncrypt[i] = RotateRight(oData[i], rValue);
        if (i < 7)
        {
            stolenBits[i] = (u8)((toEncrypt[i] >> i+1) & 1); 
            int orig = toEncrypt[i];
            orig = (orig & ~mask[i]);  
            orig += (int)insertBits[i+1] << i+1; 
            toEncrypt[i] = (u8)orig;
        }
        if (i == 8)
        {
            int blowup = 0;
            for (int x = 0; x < 7; x++)
                blowup += (int)(stolenBits[x] << x);  
            toEncrypt[i] = (u8)blowup;
        }
    }
    
    memcpy(dest, toEncrypt, length);
    
    return status;
}

u8 cj_crypto_decrypt(u8 *src, u8 *dest, u32 length)
{
    u8 status;
    u8 stolenBitsIn[8];
    u8 insertBitsIn[8];
    u8 *toDecrypt;
    u8 mask[7];
    int rValueRead;
    
    status = 0;
    mask[0] = 0x02;
    mask[1] = 0x04;
    mask[2] = 0x08;
    mask[3] = 0x10;
    mask[4] = 0x20;
    mask[5] = 0x40;
    mask[6] = 0x80;
    memset(stolenBitsIn, 0, sizeof(stolenBitsIn));
    memset(insertBitsIn, 0, sizeof(insertBitsIn));
    
    toDecrypt = src;
    
    for (int x = 0; x < 8; x++)
    {
        stolenBitsIn[x] = (u8)((toDecrypt[8] >> x) & 1); 
    }
    
    for (int i = 0; i < 7; i++) 
    {
        insertBitsIn[i + 1] = (u8)((toDecrypt[i] >> i + 1) & 1);
        
        int orig = toDecrypt[i];
        orig = (orig & ~mask[i]);  
        orig += (int)stolenBitsIn[i] << i + 1;  
        toDecrypt[i] = (u8)orig;
    }
    
    rValueRead = 0;
    for (int x = 0; x < 8; x++)
    {
        rValueRead += (int)(insertBitsIn[x] << x);  
    }
    
    for (int i = 0; i < length; i++)  //cj decrypt and decode
    {
        toDecrypt[i] = RotateLeft(toDecrypt[i], rValueRead);  //cj decrypt
    }
    
    strcpy((char*)toDecrypt+7, ".HEX");
    
    memcpy(dest, toDecrypt, length);
    
    return status;
}
