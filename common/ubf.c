/**
  **************** (C) COPYRIGHT 2011, 2012 SCT Performance, LLC ***************
  * File Name          : ubf.c
  * Device / System    : SCT products using the "CommonCode" platform
  * Author             : Patrick Downs
  *
  * Version            : 1 
  * Date               : 09/05/2012
  * Description        : Utility Bootloader Files. These are preloaded utility/
  *                    : secondary bootloader files for PCMs/TCMs
  *
  * 
  *
  ******************************************************************************
  */

//------------------------------------------------------------------------------
// Log error point: 0x -> 0x
//------------------------------------------------------------------------------

#include <string.h>
#include <common/statuscode.h>
#include <common/crypto_blowfish.h>
#include <fs/genfs.h>
#include "ubf.h"
#include <common/crc32.h>
#include <common/file.h>

typedef struct
{
    u16 UbfVersion;
    u16 BinaryPadByteCount;
    u32 BinaryDataLength;
    u32 BinaryDataOffset;
    u32 UbfFileLength;
    u32 BinaryDataCrc32e;
    u32 HeaderCrc32e;
    u8  BinaryFilename[32];
    u8  reserved1[8];
}UbfHeader; // Header size is 64 bytes

#define UBFVERSION 0

//-----------------------------------------------------
// Validates the UBF file, checks for proper extention
// Input: Filename
// return: status
// Engr: Patrick Downs
//-----------------------------------------------------
u8 ubf_validate_file(u8 *filename)
{
    F_FILE *fptr;
    UbfHeader header;
    u8 status;
    u32 crc32ecmp;
    u8 databuffer[1024];
    u32 bytesread;
    u32 bytesread_total;
    u32 binarydata_total;
    u32 readsize;
    
    if(strstr((char const*)filename, ubf_extention) == 0)
        return S_FAIL; // incorrect file extension
    
    fptr = NULL;
    if((fptr = genfs_default_openfile(filename, "rb")) == NULL)
        return S_OPENFILE;
    
    if ((fread((u8*)&header,1,sizeof(header), fptr)) != sizeof(header))
    {
        status = S_READFILE;
        goto ubf_validate_file_done;
    }
    
    status = crypto_blowfish_decryptblock_critical((u8*)&header,sizeof(header));
    if(status != S_SUCCESS)
    {
        goto ubf_validate_file_done;
    }
    
    crc32ecmp = header.HeaderCrc32e; // Save the current Header CRC
    header.HeaderCrc32e = 0xFFFFFFFF;
    
    crc32e_reset();
    header.HeaderCrc32e = crc32e_calculateblock(header.HeaderCrc32e,(u32*)&header, sizeof(header)/4);
    if(header.HeaderCrc32e != crc32ecmp)
    {
        status = S_BADCRCFAIL; // Bad header CRC
        goto ubf_validate_file_done;
    }
    
    if(header.UbfVersion != UBFVERSION)
    {
        status = S_NOTSUPPORT; // Unsupported version
        goto ubf_validate_file_done;
    }
    
    if ((fseek(fptr,header.BinaryDataOffset,SEEK_SET)) != 0) // Seek to binary data
    {
        status = S_SEEKFILE;
        goto ubf_validate_file_done;
    }
    
    crc32ecmp = header.BinaryDataCrc32e;
    crc32e_reset();
    bytesread_total = 0;
    binarydata_total = (header.BinaryDataLength + header.BinaryPadByteCount);
    while(bytesread_total < binarydata_total)
    {
        readsize = sizeof(databuffer);
        if(readsize > (binarydata_total - bytesread_total))
        {
            readsize = (binarydata_total - bytesread_total);
        }
            
        bytesread = fread(databuffer, 1, readsize, fptr);
        if (bytesread != readsize)
        {
            status = S_READFILE;
            goto ubf_validate_file_done;
        }
        
        status = crypto_blowfish_decryptblock_critical(databuffer,bytesread);
        if(status != S_SUCCESS)
        {
            goto ubf_validate_file_done;
        }
        
        header.BinaryDataCrc32e = crc32e_calculateblock(header.BinaryDataCrc32e, (u32*)&databuffer, bytesread/4);
        bytesread_total += bytesread;
    }

    if(header.BinaryDataCrc32e != crc32ecmp)
    {
        status = S_BADCRCFAIL; // Bad binary data CRC
        goto ubf_validate_file_done;
    } 
    // Makes it here, everthings good
    status = S_SUCCESS;
ubf_validate_file_done:
    genfs_closefile(fptr);
    
    return status;    
}

//------------------------------------------------------------------------------
// Get starting position & length of actual data of ubf
// Input:   F_FILE *f
// Outputs: u32 *position
//          u32 *length
// Return:  u8 status
// Engineer: Quyen Leba
//------------------------------------------------------------------------------
u8 ubf_get_data_info(F_FILE *f, u32 *position, u32 *length)
{
    UbfHeader header;
    u32 curpos;

    if (f == NULL || position == NULL || length == NULL)
    {
        return S_INPUT;
    }
    
    curpos = ftell(f);

    if ((fseek(f,0,SEEK_SET)) != 0) 
    {
        return S_SEEKFILE;
    }
    
    if ((fread((u8*)&header,1,sizeof(header), f)) != sizeof(header))
    {
        return S_READFILE;
    }

    //restore file position
    if ((fseek(f,curpos,SEEK_SET)) != 0) 
    {
        return S_SEEKFILE;
    }
    
    crypto_blowfish_decryptblock_critical((u8*)&header,sizeof(header));

    *position = header.BinaryDataOffset;
    *length = header.BinaryDataLength;

    return S_SUCCESS;
}

//-----------------------------------------------------
// Seeks to where the real data in the UBF file starts
// Input: fptr *f - opened file pointer to the UBF 
//                  working file
// return: status
// Engr: Patrick Downs
//-----------------------------------------------------
u8 ubf_seek_start(F_FILE *f)
{
    UbfHeader header;
    int status;
    
    // Get Offset from header
    if((fseek(f,0,SEEK_SET)) != 0) 
    {
        return S_SEEKFILE;        
    }
    
    if ((fread((u8*)&header,1,sizeof(header), f)) != sizeof(header))
    {
        return S_READFILE;
    }
    
    status = crypto_blowfish_decryptblock_critical((u8*)&header,sizeof(header));
    if(status != S_SUCCESS)  
    {
        return status;
    }
    
    // Seek to binary data
    if ((fseek(f,header.BinaryDataOffset,SEEK_SET)) != 0) 
    {
        return S_SEEKFILE;
    }
        
    return S_SUCCESS;    
}

//-----------------------------------------------------
// Reads and decrypts a block of data from the file
// LENGTH NEEDS TO BE MULTIPLES OF 8!
// Input: blocklength - length of the block to be read
// Output: databuffer - pointer to the buffer where data
//                      is to be copied
// return: status
// Engr: Patrick Downs
//-----------------------------------------------------
u8 ubf_read_block(u8 *databuffer, u32 blocklength, F_FILE *f)
{    
    u32 readlength;
    u8 status;
    file_blockcrypto ubfcrypto;
    
    ubfcrypto.bfcrypto.type = file_blockcrypto_type_bf;
    ubfcrypto.bfcrypto.ctx = (blowfish_context_ptr*)&m_critical_ctx;
    ubfcrypto.bfcrypto.crypto = &crypto_blowfish_decryptblock_full;
    
    status = file_encrypted_read(f, blocklength, databuffer, &readlength, &ubfcrypto);
    
    return status;
}



