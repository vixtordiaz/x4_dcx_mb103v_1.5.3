/**
  **************** (C) COPYRIGHT 2011 SCT Performance, LLC *********************
  * File Name          : function_ptr_defs.h
  * Device / System    : SCT products using the "CommonCode" platform
  * Author             : Quyen Leba
  *
  * Version            : 1 
  * Date               : 05/02/2011
  * Description        : 
  *                    : 
  *
  *
  * History            : 05/02/2011 M. Davis
  *                    :   Add SCT Copyright header,
  *
  ******************************************************************************
  */

#ifndef __FUNCTION_PTR_DEFS_H
#define __FUNCTION_PTR_DEFS_H

#include <arch/gentype.h>

typedef void (* PROGRESS_REPORT_FUNC_SIMPLE)(u8 percentage);

#endif    //__FUNCTION_PTR_DEFS_H
