/**
  **************** (C) COPYRIGHT 2011 SCT Performance, LLC *********************
  * File Name          : cmdif_function.h
  * Device / System    : SCT products using the "CommonCode" platform
  * Author             : Quyen Leba
  *
  * Version            : 1 
  * Date               : 05/02/2011
  * Description        : 
  *                    : 
  *
  *
  * History            : 05/02/2011 M. Davis
  *                    :   Add SCT Copyright header,
  *
  ******************************************************************************
  */

#ifndef __CMDIF_FUNCTION_H
#define __CMDIF_FUNCTION_H

#include <board/bootloader.h>
#include "obd2.h"

u8 cmdif_func_readvin();
u8 cmdif_func_cleardtc();
u8 cmdif_func_readdtc_raw();
u8 cmdif_func_readdtc_new();
u8 cmdif_func_readecminfo();
u8 cmdif_func_readvehicleinfo();
u8 cmdif_func_ping_response();
u8 cmdif_func_unsupported_command();
u8 cmdif_func_get_device_info();
u8 cmdif_func_get_device_data(u8 dataopcode);
u8 cmdif_func_get_dongle_settings(u8 settings_opcode, u16 privdata);
u8 cmdif_func_preloaded_tune_set_restriction(u32 restriction_value);
u8 cmdif_func_enter_bootloader(u8 bootloadertype);
u8 cmdif_func_bootloader_setboot(BootloaderMode bootmode);
u8 cmdif_func_bootloader_init(u8 *encryptedheader);
u8 cmdif_func_bootloader_do(u8 *encrypteddata, u32 length);
u8 cmdif_func_bootloader_validate();
u8 cmdif_func_get_properties(u16 target);
u8 cmdif_func_hardware(u16 opcode, u8 *opcodedata);
u8 cmdif_func_version_health_check(u8 check_type, u32 app_version);
u8 cmdif_func_debug(u32 task, u8 *data, u32 datalength);
u8 cmdif_func_production_test(u8 test_task);
u8 cmdif_func_obd2_garbagecollector();

#endif    //__CMDIF_FUNCTION_H
