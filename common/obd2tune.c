/**
  **************** (C) COPYRIGHT 2011 SCT Performance, LLC *********************
  * File Name          : obd2tune.c
  * Device / System    : SCT products using the "CommonCode" platform
  * Author             : Quyen Leba
  *
  * Version            : 1 
  * Date               : 05/02/2011
  * Description        : 
  *                    : 
  *
  *
  * History            : 05/02/2011 M. Davis
  *                    :   Add SCT Copyright header,
  *
  ******************************************************************************
  */

//------------------------------------------------------------------------------
// Log error point: 0x44xx -> 0x46xx
//------------------------------------------------------------------------------

#include <ctype.h>
#include <fs/genfs.h>
#include <common/statuscode.h>
#include <common/ecm_defs.h>
#include <common/settings.h>
#include <common/obd2def.h>
#include <common/obd2comm.h>
#include <common/genmanuf_overload.h>
#include <common/filestock.h>
#include <common/ess_file.h>
#include <common/crypto_blowfish.h>
#include <common/log.h>
#include <common/cmdif.h>
#include <device_version.h>
#include "obd2tune.h"

extern obd2_info gObd2info;             /* from obd2.c */

#if (USE_OPTION_HANDLING_SOF & USE_OPTION_HANDLING_OPT)
#error USE_OPTION_HANDLING_SOF & USE_OPTION_HANDLING_OPT conflict
#elif !(USE_OPTION_HANDLING_SOF | USE_OPTION_HANDLING_OPT)
#error USE_OPTION_HANDLING_SOF & USE_OPTION_HANDLING_OPT invalid
#endif

#define DEBUG_ALLOW_SKIP_TUNE_VALIDATION_AGAINST_LOOKUP     0
#if DEBUG_ALLOW_SKIP_TUNE_VALIDATION_AGAINST_LOOKUP
#warning: Dangerous!!! DEBUG_ALLOW_SKIP_TUNE_VALIDATION_AGAINST_LOOKUP
#endif

#define MAX_SAVED_OPTION_COUNT                              50

typedef struct
{
//Bit[3..0]
#define SavedOptionsECU_SOF_FLAGS_USE_VAL_U32               0
#define SavedOptionsECU_SOF_FLAGS_USE_VAL_S32               1
#define SavedOptionsECU_SOF_FLAGS_USE_VAL_FLOAT             2
//Bit4
#define SavedOptionsECU_SOF_FLAGS_DEFAULT_OPTION_CHOICE     0
#define SavedOptionsECU_SOF_FLAGS_USER_OPTION_CHOICE        (1u<<4)
//Bit[31:5] - reserved
    u32 flags;
    union
    {
        u32 val_u32;
        s32 val_s32;
        float val_float;
    }selection;
    u32 reserved;
    u32 file_offsets;
}option_selection_sof;

typedef struct
{
    u32 optionfile_crc32e;
#define CURRENT_SOF_VERSION                                 0x0000
    u16 version;
    u8  reserved[26];

    option_selection_sof option_selection[MAX_SAVED_OPTION_COUNT];
}SavedOptionsECU_SOF;

struct
{
    u8 ecm_id_count;
    u8 speedmode;
    u32 ecm_id[ECM_MAX_COUNT];
    VehicleCommType commtype;
    VehicleCommLevel commlevel[ECM_MAX_COUNT];
    //broadcast: TRUE if send testerpresent to all modules/nodes
    //bool broadcast;
    //loop: use with broadcast; TRUE: send testerpresent by looping ecm_id[]
    //bool loop;
}tester_present_info;

flasher_info *flasherinfo = NULL;

//------------------------------------------------------------------------------
// Private prototypes
//------------------------------------------------------------------------------
u8 obd2tune_apply_options_use_opt(u8 flash_type, u16 veh_type, OptionSource optionsource,
                                  selected_tunelist_info *tuneinfo);
u8 obd2tune_apply_options_use_sof(u8 flash_type, u16 veh_type, OptionSource optionsource,
                                  selected_tunelist_info *tuneinfo);
u32 obd2tune_calculate_custom_tune_block_signature(customtune_block *block);

//------------------------------------------------------------------------------
// Initialize the flasher info structure
// Return:  u8  status
// Engineer: Quyen Leba
//------------------------------------------------------------------------------
u8 obd2tune_flasherinfo_init()
{
    u8  i;

    if (flasherinfo == NULL)
    {
        /* Disconnect OBD2 handle to allow the OBD2 handler to rescan the bus.
         * This only needs to be done once so only do if flasherinfo has not
         * already been initialized.
         */
        obd2_close();
    }
    
    obd2_garbagecollector();

    if (flasherinfo == NULL)
    {
        flasherinfo = __malloc(sizeof(flasher_info));
        if (flasherinfo == NULL)
        {
            log_push_error_point(0x4400);
            return S_MALLOC;
        }

        memset((char*)flasherinfo,0,sizeof(flasher_info));
        memset((char*)flasherinfo->vin,0,VIN_LENGTH+1);
        memset((char*)flasherinfo->vehicle_serial_number,0,
               sizeof(flasherinfo->vehicle_serial_number));
        memset((char*)flasherinfo->vehiclecodes,0,
               sizeof(flasherinfo->vehiclecodes));
        
        for(i=0; i < ECM_MAX_COUNT; i++)
        {
            memset((char*)flasherinfo->basestockfilename[i],0,
                   sizeof(flasherinfo->basestockfilename[i]));
            flasherinfo->basestockcrc32e[i] = 0;
        }
        
        flasherinfo->flags = 0;
        flasherinfo->ecm_count = 0;
        flasherinfo->tunecodeinfolist.count = 0;
        flasherinfo->tunebytecount = 0;
        for(i=0;i<ECM_MAX_COUNT;i++)
        {
            memset((char*)flasherinfo->tunecodeinfolist.description[i],
                   0,MAX_DESCRIPTION_LENGTH+1);
            memset((char*)flasherinfo->tunecodeinfolist.tunefilename[i],
                   0,MAX_TUNE_NAME_LENGTH+1);
            memset((char*)flasherinfo->tunecodeinfolist.optionfilename[i],
                   0,MAX_TUNE_NAME_LENGTH+1);
            flasherinfo->tunecodeinfolist.fueltype[i] = UnknownFuelType;
            
            memset((char*)flasherinfo->selected_tunelistinfo.description[i],
                   0,MAX_DESCRIPTION_LENGTH+1);
            memset((char*)flasherinfo->selected_tunelistinfo.tunefilename[i],
                   0,MAX_TUNE_NAME_LENGTH+1);
            memset((char*)flasherinfo->selected_tunelistinfo.optionfilename[i],
                   0,MAX_TUNE_NAME_LENGTH+1);
            
            memset((char*)flasherinfo->selected_tunelistinfo.specialoptionfilename[i],
                   0,MAX_TUNE_NAME_LENGTH+1);
            memset((char*)flasherinfo->selected_tunelistinfo.checksumfilename[i],
                   0,MAX_TUNE_NAME_LENGTH+1);
            
            flasherinfo->selected_tunelistinfo.flags[i] = TUNEFLAG_CAL_ONLY;
            flasherinfo->selected_tunelistinfo.useoption[i] = TUNEUSEOPTION_NONE;
            flasherinfo->selected_tunelistinfo.supported[i] = FALSE;
        }
        
        flasherinfo->tune_programming_status.ecm_index = 0;
        flasherinfo->tune_programming_status.percentage = 0;
        flasherinfo->tune_programming_status.totalbytecount = 0;
        flasherinfo->tune_programming_status.currentbytecount = 0;
        
        flasherinfo->uploadstock_required = FALSE;
        flasherinfo->isupload_only = FALSE;
        flasherinfo->isunlockpriordownload = TRUE;
        flasherinfo->isfullreflash_already_set = FALSE;
        flasherinfo->isstockcrc = FALSE; 
        memset(flasherinfo->isfullreflash,
               FALSE,sizeof(flasherinfo->isfullreflash));
        flasherinfo->isvinmasked = FALSE;
        flasherinfo->tunecheck_status = S_SUCCESS;
        
        memset(&flasherinfo->options_flags, 0, sizeof(flasherinfo->options_flags));
    }
    
    return S_SUCCESS;
}

//------------------------------------------------------------------------------
// Deinitialize the flasher info structure
// Return:  u8  status
// Engineer: Quyen Leba
//------------------------------------------------------------------------------
u8 obd2tune_flasherinfo_deinit()
{
    obd2_garbagecollector();
    return S_SUCCESS;
}

//------------------------------------------------------------------------------
// Clean up after done tuning
//------------------------------------------------------------------------------
u8 obd2tune_garbagecollector()
{
    if (flasherinfo)
    {
        __free(flasherinfo);
        flasherinfo = NULL;
    }
    return S_SUCCESS;
}

//------------------------------------------------------------------------------
// Get Tune Revision (devices use lookup.plf)
// Output:  u8  *tunerevision (upto 64+1 bytes)
// Return:  u8  status
// Engineer: Quyen Leba
//------------------------------------------------------------------------------
u8 obd2tune_gettunerevision_plf(u8 *tunerevision)
{
    F_FILE  *lookupfile;
    PreloadedTuneLookupHeader lookupheader;
    u32 bytecount;
    u32 filesize;
    u8  status;

    status = S_FAIL;
    lookupfile = genfs_default_openfile(DEFAULT_PRELOADED_TUNE_LOOKUP_FILE,"r");
    if (lookupfile == NULL)
    {
        log_push_error_point(0x4530);
        status = S_NOLOOKUP;
        goto obd2tune_gettunerevision_plf_done;
    }

    filesize = fgetfilesize(lookupfile);
    bytecount = fread((char*)&lookupheader,
                      1,sizeof(PreloadedTuneLookupHeader),lookupfile);
    if (bytecount != sizeof(PreloadedTuneLookupHeader))
    {
        log_push_error_point(0x4531);
        status = S_READFILE;
        goto obd2tune_gettunerevision_plf_done;
    }
    crypto_blowfish_decryptblock_critical((u8*)&lookupheader,
                                          sizeof(PreloadedTuneLookupHeader));
    //don't care about this status
    obd2tune_validate_preloadedtune_lookupheader(filesize,&lookupheader,
                                                 tunerevision,NULL);
    if (isgraph(tunerevision[0]))
    {
        status = S_SUCCESS;
    }

obd2tune_gettunerevision_plf_done:
    if (lookupfile)
    {
        genfs_closefile(lookupfile);
    }
    if (status != S_SUCCESS)
    {
        strcpy((char*)tunerevision,"N/A");
    }
    return status;
}

//------------------------------------------------------------------------------
// Get current PLF datecode from header
// Output:  u32 *datecode 
// Return:  u8  status
// Engineer: Patrick Downs
//------------------------------------------------------------------------------
u8 obd2tune_getdatecode_plf(u32 *datecode)
{
    F_FILE  *lookupfile;
    PreloadedTuneLookupHeader lookupheader;
    u32 bytecount;
    u8  status;

    status = S_FAIL;
    lookupfile = genfs_default_openfile(DEFAULT_PRELOADED_TUNE_LOOKUP_FILE,"r");
    if (lookupfile == NULL)
    {
        log_push_error_point(0x4563);
        status = S_NOLOOKUP;
        goto obd2tune_getdatecode_plf_done;
    }
    
    bytecount = fread((char*)&lookupheader,
                      1,sizeof(PreloadedTuneLookupHeader),lookupfile);
    if (bytecount != sizeof(PreloadedTuneLookupHeader))
    {
        log_push_error_point(0x4564);
        status = S_READFILE;
        goto obd2tune_getdatecode_plf_done;
    }
    crypto_blowfish_decryptblock_critical((u8*)&lookupheader,
                                          sizeof(PreloadedTuneLookupHeader));
    
    *datecode = lookupheader.datecode;
    
    status = S_SUCCESS;    

obd2tune_getdatecode_plf_done:
    if (lookupfile)
    {
        genfs_closefile(lookupfile);
    }
    
    return status;
}

//------------------------------------------------------------------------------
// Get Tune Revision
// Output:  u8  *tunerevision (upto 64+1 bytes)
// Return:  u8  status
// Engineer: Quyen Leba
//------------------------------------------------------------------------------
u8 obd2tune_gettunerevision(u8 *tunerevision)
{
    return obd2tune_gettunerevision_plf(tunerevision);
}

//------------------------------------------------------------------------------
// Compare PLF Datecode with Minimum Datecode
// Return:  bool TRUE: PLF is old, FALSE: PLF is not old
// Engineer: Patrick Downs
//------------------------------------------------------------------------------
bool obd2tune_is_plfdatecode_old()
{
    u32 currentdatecode;
    
    if(obd2tune_getdatecode_plf(&currentdatecode) != S_SUCCESS)
    {
        return TRUE; // Error reading datecode
    }
       
    if((u32)MIN_PLF_DATECODE_REQUIRED > (u32)currentdatecode)
    {
        return TRUE; // PLF datecode is not equal or greater than min required 
    }
    
    return FALSE; // PLF is not old
}

//------------------------------------------------------------------------------
// Validate preloaded tune lookup header
// Inputs:  u32 filesize (lookup file size)
//          PreloadedTuneLookupHeader *lookupheader (decrypted)
// Outputs: u8  *revisionstring (32 bytes)
//          u8  *description (128 bytes)
// Return:  u8  status
// Engineer: Quyen Leba
//------------------------------------------------------------------------------
u8 obd2tune_validate_preloadedtune_lookupheader(u32 filesize,
                                                PreloadedTuneLookupHeader *lookupheader,
                                                u8 *revisionstring,
                                                u8 *description)
{
    u32 crc32e_cmp;
    u32 crc32e_calc;
    u8  status;

    status = S_FAIL;
    if (revisionstring)
    {
        revisionstring[0] = NULL;
    }
    if (description)
    {
        description[0] = NULL;
    }

    crc32e_reset();
    crc32e_cmp = lookupheader->header_crc32e;
    lookupheader->header_crc32e = 0xFFFFFFFF;
    crc32e_calc = crc32e_calculateblock(0xFFFFFFFF,(u32*)lookupheader,
                                        sizeof(PreloadedTuneLookupHeader)/4);
    lookupheader->header_crc32e = crc32e_cmp;
    if (crc32e_calc != crc32e_cmp)
    {
        log_push_error_point(0x4506);
        status = S_CRC32E;
        goto obd2tune_validate_preloadedtune_header_done;
    }
    if (revisionstring)
    {
        strcpy((char*)revisionstring,(char*)lookupheader->revisionstring);
    }
    if (description)
    {
        strcpy((char*)description,(char*)lookupheader->description);
    }

    if (filesize != 
        (lookupheader->tunecount * sizeof(PreloadedTuneLookupItem) +
         lookupheader->totalfulltunedescription_count * sizeof(PreloadedTuneLookupFullTuneDescription) +
         sizeof(PreloadedTuneLookupHeader)))
    {
        log_push_error_point(0x4505);
        status = S_BADCONTENT;
        goto obd2tune_validate_preloadedtune_header_done;
    }

    //TODOQ: this might be too strict
    if (SETTINGS_TUNE(preloadedlookup_crc32einfo) != lookupheader->header_crc32e &&
        SETTINGS_TUNE(preloadedlookup_crc32einfo) != 0)
    {
        log_push_error_point(0x4536);
        status = S_CRC32E;
        goto obd2tune_validate_preloadedtune_header_done;
    }

    if (lookupheader->version != SUPPORT_LOOKUP_PLF_VERSION)
    {
        log_push_error_point(0x451F);
        status = S_NOTSUPPORT;
        goto obd2tune_validate_preloadedtune_header_done;
    }
    else if (lookupheader->oem_type != OemType_Unknown &&
             lookupheader->oem_type != OEM_TYPE)
    {
        log_push_error_point(0x4525);
        status = S_UNMATCH;
        goto obd2tune_validate_preloadedtune_header_done;
    }
//    else if (lookupheader->hardware !=  &&
//             lookupheader->hardware != )
//    {
//        log_push_error_point(0x4526);
//        status = S_UNMATCH;
//        goto obd2tune_validate_preloadedtune_header_done;
//    }
    else if (lookupheader->market_type != MarketType_Unknown &&
             lookupheader->market_type != MarketType_Any &&
             lookupheader->market_type != SETTINGS_GetMarketType())
    {
        log_push_error_point(0x4527);
        status = S_UNMATCH;
        goto obd2tune_validate_preloadedtune_header_done;
    }
    else if (lookupheader->device_type != DeviceType_Unknown &&
             lookupheader->device_type != DeviceType_Any &&
             lookupheader->device_type != DEVICE_TYPE)
    {
        log_push_error_point(0x4528);
        status = S_UNMATCH;
        goto obd2tune_validate_preloadedtune_header_done;
    }
    else if (lookupheader->serialnumber[0])
    {
        if (settings_compare_deviceserialnumber(lookupheader->serialnumber) != S_SUCCESS)
        {
            log_push_error_point(0x4529);
            status = S_SERIALUNMATCH;
            goto obd2tune_validate_preloadedtune_header_done;
        }
    }
    else if (lookupheader->devicepartnumberstring[0])
    {
        if (settings_compare_devicepartnumberstring(lookupheader->devicepartnumberstring) != S_SUCCESS)
        {
            log_push_error_point(0x452A);
            status = S_UNMATCH;
            goto obd2tune_validate_preloadedtune_header_done;
        }
    }

    if (SETTINGS_Is50StateLegalDevice())
    {
        if (!(lookupheader->flags & PreloadedTuneLookupHeaderFlags_50StateLegal))
        {
            //can't allow non-50StateLegal tunes on 50StateLegal device
            log_push_error_point(0x452F);
            status = S_INVALIDTYPE;
            goto obd2tune_validate_preloadedtune_header_done;
        }
    }

    if (!SETTINGS_IsOffRoadDevice())
    {
        if ((lookupheader->flags & PreloadedTuneLookupHeaderFlags_OffRoad))
        {
            //can't allow offroad tunes on non-offroad device
            log_push_error_point(0x4534);
            status = S_INVALIDTYPE;
            goto obd2tune_validate_preloadedtune_header_done;
        }
    }

    if (SETTINGS_IsNoPreloadedDevice())
    {
        //shouldn't get here
        log_push_error_point(0x4535);
        status = S_INVALIDTYPE;
        goto obd2tune_validate_preloadedtune_header_done;
    }
    
    status = S_SUCCESS;

obd2tune_validate_preloadedtune_header_done:
    return status;
}

//------------------------------------------------------------------------------
// Using lookup file (plf format) to get a list of available codes for tunecode
// Inputs:  u8  *tunecode (the vehicle tune code)
//          u8  *vin
// Outputs: tunecode_infolist *tunecodeinfolist
// Return:  u8  status
// Engineer: Quyen Leba
//------------------------------------------------------------------------------
u8 obd2tune_gettunecodelistinfo_plf(u8 *tunecode, u8 *vin,
                                    tunecode_infolist *tunecodeinfolist)
{
    F_FILE *lookupfile;
    F_FILE *tunefile;
    PreloadedTuneLookupHeader lookupheader;
    PreloadedTuneLookupItem lookupitem;
    const u8 tunecode_terminator_string[2] = {TUNECODE_TERMINATOR,0};
    u32 bytecount;
    u32 filesize;
    struct{
        s32 lower;
        s32 upper;
        s32 middle;
    }index;
    u32 crc32e_calc;
    bool isfound;
    s32 i;
    u8  tunecode_count;
    u8  *bptr;
    u8  status;

    lookupfile = NULL;
    tunefile = NULL;
    status = S_FAIL;
    if (tunecode[0] == TUNECODE_SEPARATOR ||
        !isgraph(tunecode[0]))
    {
        log_push_error_point(0x4500);
        return S_INPUT;
    }
    for(i=0,tunecode_count=0;;i++)
    {
        if (tunecode[i] == TUNECODE_SEPARATOR)
        {
            tunecode_count++;
            if (tunecode_count > 1)
            {
                //must be 1 code only
                log_push_error_point(0x4501);
                return S_INPUT;
            }
        }
        else if (!isgraph(tunecode[i]) || i >= 128)
        {
            break;
        }
    }
    if (tunecode_count != 1)
    {
        log_push_error_point(0x4502);
        return S_INPUT;
    }
    tunecodeinfolist->count = 0;

    lookupfile = genfs_default_openfile(DEFAULT_PRELOADED_TUNE_LOOKUP_FILE,"r");
    if (lookupfile == NULL)
    {
        log_push_error_point(0x4503);
        return S_NOLOOKUP;
    }

    //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    // Validation
    //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    filesize = fgetfilesize(lookupfile);
    bytecount = fread((char*)&lookupheader,
                      1,sizeof(PreloadedTuneLookupHeader),lookupfile);
    if (bytecount != sizeof(PreloadedTuneLookupHeader))
    {
        log_push_error_point(0x4504);
        status = S_READFILE;
        goto obd2tune_gettunecodelistinfo_plf_done;
    }
    crypto_blowfish_decryptblock_critical((u8*)&lookupheader,
                                          sizeof(PreloadedTuneLookupHeader));
    status = obd2tune_validate_preloadedtune_lookupheader(filesize,
                                                          &lookupheader,
                                                          NULL,NULL);
    if (status != S_SUCCESS)
    {
        goto obd2tune_gettunecodelistinfo_plf_done;
    }

    //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    // Check content
    //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    crc32e_reset();
    crc32e_calc = 0xFFFFFFFF;
    //calculate crc32e of full descriptions (section 2)
    for(i=0;i<lookupheader.totalfulltunedescription_count;i++)
    {
        PreloadedTuneLookupFullTuneDescription fulldescription;

        bytecount = fread((char*)&fulldescription,
                      1,sizeof(PreloadedTuneLookupFullTuneDescription),lookupfile);
        if (bytecount != sizeof(PreloadedTuneLookupFullTuneDescription))
        {
            log_push_error_point(0x452B);
            status = S_READFILE;
            goto obd2tune_gettunecodelistinfo_plf_done;
        }
        crc32e_calc = crc32e_calculateblock(crc32e_calc,(u32*)&fulldescription,
                                            sizeof(PreloadedTuneLookupFullTuneDescription)/4);
    }
    //calculate crc32e of lookup items (section 3)
    while(!feof(lookupfile))
    {
        bytecount = fread((char*)&lookupitem,
                      1,sizeof(PreloadedTuneLookupItem),lookupfile);
        if (bytecount != sizeof(PreloadedTuneLookupItem))
        {
            log_push_error_point(0x451D);
            status = S_READFILE;
            goto obd2tune_gettunecodelistinfo_plf_done;
        }
        crc32e_calc = crc32e_calculateblock(crc32e_calc,(u32*)&lookupitem,
                                            sizeof(PreloadedTuneLookupItem)/4);
    }
    if (crc32e_calc != lookupheader.content_crc32e)
    {
        log_push_error_point(0x451E);
        status = S_CRC32E;
        goto obd2tune_gettunecodelistinfo_plf_done;
    }

    //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    // Search the info
    //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    index.lower = 0;
    index.upper = lookupheader.tunecount-1;
    isfound = FALSE;
    while(index.lower <= index.upper)
    {
        index.middle = (index.upper + index.lower)/2;
        if (fseek(lookupfile,
                  sizeof(PreloadedTuneLookupHeader) +
                  (lookupheader.totalfulltunedescription_count * sizeof(PreloadedTuneLookupHeader)) +
                  (index.middle * sizeof(PreloadedTuneLookupItem)),
                  SEEK_SET) != 0)
        {
            log_push_error_point(0x4507);
            status = S_SEEKFILE;
            goto obd2tune_gettunecodelistinfo_plf_done;
        }
        bytecount = fread((char*)&lookupitem,
                      1,sizeof(PreloadedTuneLookupItem),lookupfile);
        if (bytecount != sizeof(PreloadedTuneLookupItem))
        {
            log_push_error_point(0x4508);
            status = S_READFILE;
            goto obd2tune_gettunecodelistinfo_plf_done;
        }
        crypto_blowfish_decryptblock_critical((u8*)&lookupitem,
                                              sizeof(PreloadedTuneLookupItem));
        bptr = (u8*)strstr((char*)lookupitem.tunefilename,
                           (char*)tunecode_terminator_string);
        if (!bptr)
        {
            log_push_error_point(0x4509);
            status = S_BADCONTENT;
            goto obd2tune_gettunecodelistinfo_plf_done;
        }
        *bptr = NULL;
        i = memcmp((char*)tunecode,(char*)lookupitem.tunefilename,
                   strlen((char*)lookupitem.tunefilename));
        if (i == 0)
        {
            //found it
            isfound = TRUE;
            break;
        }
        else if (i < 0)     //tunecode < tunefilename
        {
            //go left
            index.upper = index.middle-1;
            
        }
        else                //tunecode > tunefilename
        {
            //go right
            index.lower = index.middle+1;
        }
    }
    if (!isfound)
    {
        log_push_error_point(0x450A);
        status = S_STRATNOTFOUND;
        goto obd2tune_gettunecodelistinfo_plf_done;
    }

    //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    // Get the info
    //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    index.lower = index.middle - MAX_TUNE_CODE_LIST_COUNT;
    if (index.lower < 0)
    {
        index.lower = 0;
    }
    if (fseek(lookupfile,sizeof(PreloadedTuneLookupHeader) +
              (lookupheader.totalfulltunedescription_count * sizeof(PreloadedTuneLookupHeader)) +
              (index.lower * sizeof(PreloadedTuneLookupItem)),SEEK_SET) != 0)
    {
        log_push_error_point(0x450B);
        status = S_SEEKFILE;
        goto obd2tune_gettunecodelistinfo_plf_done;
    }

    while(tunecodeinfolist->count < MAX_TUNE_CODE_LIST_COUNT)
    {
        bytecount = fread((char*)&lookupitem,
                          1,sizeof(PreloadedTuneLookupItem),lookupfile);
        if (bytecount == 0)
        {
            break;
        }
        else if (bytecount != sizeof(PreloadedTuneLookupItem))
        {
            log_push_error_point(0x450C);
            status = S_READFILE;
            goto obd2tune_gettunecodelistinfo_plf_done;
        }
        crypto_blowfish_decryptblock_critical((u8*)&lookupitem,
                                              sizeof(PreloadedTuneLookupItem));
        bptr = (u8*)strstr((char*)lookupitem.tunefilename,
                           (char*)tunecode_terminator_string);
        if (!bptr)
        {
            log_push_error_point(0x450D);
            status = S_BADCONTENT;
            goto obd2tune_gettunecodelistinfo_plf_done;
        }
        i = memcmp((char*)tunecode,(char*)lookupitem.tunefilename,
                   bptr-lookupitem.tunefilename);
        if (i == 0)
        {
            u8  vin_index;
            bool isvinpass;

            for(vin_index=0,isvinpass=TRUE;
                vin_index < sizeof(lookupitem.vinmask);vin_index++)
            {
                if (lookupitem.vinmask[vin_index] != VIN_MASK_BYTE &&
                    lookupitem.vinmask[vin_index] != 0x00 &&
                    lookupitem.vinmask[vin_index] != vin[vin_index])
                {
                    isvinpass = FALSE;
                    break;
                }
            }

            if (isvinpass)
            {
                bool istuneaccepted;

                istuneaccepted = TRUE;

#if PRELOADED_TUNE_LOOKUP_CRC32E_TUNE_CHECK
                if (tunefile)
                {
                    genfs_closefile(tunefile);
                    tunefile = NULL;
                }

                // lookupitem.tunefilecrc32einfo must match sthheader.header_crc32e
                tunefile = genfs_default_openfile(lookupitem.tunefilename,"r");
                if (tunefile)
                {
                    sthFileHeader sth_header;
                    u32 sth_header_length;

                    status = obd2tune_read_sth_header(tunefile,
                                                      &sth_header,&sth_header_length);
                    if (status != S_SUCCESS)
                    {
                        istuneaccepted = FALSE;
#if !IGNORE_INVALID_PRELOADED_TUNE
                        log_push_error_point(0x452C);
                        status = S_READFILE;
                        goto obd2tune_gettunecodelistinfo_plf_done;
#endif
                    }

                    if (sth_header.generic.header_crc32e != lookupitem.tunefilecrc32einfo)
                    {
#if !DEBUG_ALLOW_SKIP_TUNE_VALIDATION_AGAINST_LOOKUP
                        istuneaccepted = FALSE;
#if !IGNORE_INVALID_PRELOADED_TUNE
                        log_push_error_point(0x452D);
                        status = S_CRC32E;
                        goto obd2tune_gettunecodelistinfo_plf_done;
#endif  //#if !IGNORE_INVALID_PRELOADED_TUNE
#endif  //#if DEBUG_ALLOW_SKIP_TUNE_VALIDATION_AGAINST_LOOKUP
                    }
                    
                    status = obd2tune_validate_sth_header(&sth_header);
                    if (status != S_SUCCESS)
                    {
                        istuneaccepted = FALSE;
#if !IGNORE_INVALID_PRELOADED_TUNE
                        log_push_error_point(0x452E);
                        status = S_CRC32E;
                        goto obd2tune_gettunecodelistinfo_plf_done;
#endif
                    }
                }
                else
                {
                    istuneaccepted = FALSE;
#if !IGNORE_INVALID_PRELOADED_TUNE
                    log_push_error_point(0x450F);
                    status = S_FILENOTFOUND;
                    goto obd2tune_gettunecodelistinfo_plf_done;
#endif
                }
#endif  //PRELOADED_TUNE_LOOKUP_CRC32E_TUNE_CHECK
                if (istuneaccepted)
                {
                    tunecodeinfolist->fueltype[tunecodeinfolist->count] = Gasoline; //TODOQ: not used
                    memcpy((char*)tunecodeinfolist->tunefilename[tunecodeinfolist->count],
                           (char*)lookupitem.tunefilename,
                           sizeof(lookupitem.tunefilename));
                    memcpy((char*)tunecodeinfolist->description[tunecodeinfolist->count],
                           (char*)lookupitem.tunedescription,
                           sizeof(lookupitem.tunedescription));
                    tunecodeinfolist->fulldescription_index[tunecodeinfolist->count] = PreloadedTuneLookupItem_InvalidFullDescriptionIndex;
                    if (lookupitem.flags & PreloadedTuneLookupItemFlags_HasFullTuneDescription &&
                        lookupitem.fulltunedescription_index < lookupheader.totalfulltunedescription_count)
                    {
                        tunecodeinfolist->fulldescription_index[tunecodeinfolist->count] = lookupitem.fulltunedescription_index;
                    }
                    tunecodeinfolist->tuneflags[tunecodeinfolist->count] = lookupitem.flags;
                    tunecodeinfolist->count++;
                }//if (istuneaccepted)...
            }//if (isvinpass)...
        }
        else if (i < 0)     //tunecode < tunefilename
        {
            break;
        }
    }
    status = S_SUCCESS;
    if (tunecodeinfolist->count == 0)
    {
        log_push_error_point(0x450E);
        status = S_FAIL;
    }

obd2tune_gettunecodelistinfo_plf_done:
    if (lookupfile)
    {
        genfs_closefile(lookupfile);
    }
    if (tunefile)
    {
        genfs_closefile(tunefile);
    }
    return status;
}

//------------------------------------------------------------------------------
// Using lookup file to get a list of available codes for tunecode
// Inputs:  u8  *tunecode (the vehicle tune code)
// Outputs: tunecode_infolist *tunecodeinfolist
// Return:  u8  status
// Engineer: Quyen Leba
//------------------------------------------------------------------------------
u8 obd2tune_gettunecodelistinfo(u8 *tunecode, u8 *vin,
                                tunecode_infolist *tunecodeinfolist)
{
    u8 plf_status;
    u8 status;
    u8 filename[32];
    u8 description[32];
    u32 flags;

    // Search PLF file for regular preloaded tunes
    status = obd2tune_gettunecodelistinfo_plf(tunecode,vin,tunecodeinfolist);
    if(status == S_SUCCESS || status == S_FILENOTFOUND)
    {
        plf_status = status; // temp backup PLF status
        
        // Search GLF for OTF preloaded tunes
        status = obd2tune_search_glf_for_otf_file(DEFAULT_OTF_PRELOAD_GLF, vin, 
                                                      tunecode,
                                                      GeneralFileLookupFileProcessorCodeType_Regular,
                                                      filename, description, &flags);
        if(status == S_SUCCESS)
        {
            tunecodeinfolist->fueltype[tunecodeinfolist->count] = Gasoline; //TODOQ: not used
            memcpy((char*)tunecodeinfolist->tunefilename[tunecodeinfolist->count],
                   (char*)filename, MAX_TUNE_NAME_LENGTH);
            memcpy((char*)tunecodeinfolist->description[tunecodeinfolist->count],
                   (char*)description, MAX_DESCRIPTION_LENGTH);
            tunecodeinfolist->fulldescription_index[tunecodeinfolist->count] = PreloadedTuneLookupItem_InvalidFullDescriptionIndex;
            tunecodeinfolist->tuneflags[tunecodeinfolist->count] = flags;
            tunecodeinfolist->count++;
        }
        status = plf_status;
    }
    
    return status;
}

//------------------------------------------------------------------------------
// This routine grabs the option file name stored in the tune file of choice
// Input:   u8  *tunefilename
// Output:  u8  *optionfilename
// Return:  u8  status
// Note: successful output optionfilename maybe a null string
// Engineer: Rhonda Burns, Quyen Leba
// Note: only used in preloaded tune
//------------------------------------------------------------------------------
u8 obd2tune_getoptionfilename(u8 *tunefilename, u8 *optionfilename)
{
    F_FILE  *fpt;
    u8  tmpfilename[MAX_TUNE_NAME_LENGTH+1];
    u8  status;

    optionfilename[0] = NULL;

    fpt = genfs_default_openfile((void*)tunefilename,"rb");
    if (fpt == NULL)
    {
        log_push_error_point(0x4520);
        return S_OPENFILE;
    }

    if (strstr((char*)tunefilename,"sth"))
    {
        sthFileHeader sth_header;
        ophOptionFileHeader oph_header;

        status = obd2tune_read_sth_header(fpt,&sth_header,NULL);
        if (status == S_SUCCESS)
        {
            genfs_closefile(fpt);
            fpt = NULL;

            status = obd2tune_validate_sth_header(&sth_header);
            if (status != S_SUCCESS)
            {
                log_push_error_point(0x4521);
                return S_BADCONTENT;
            }

            //check option file
            memset(tmpfilename, 0, sizeof(tmpfilename));
            obd2tune_sth_header_parse_optionfilename(&sth_header,tmpfilename);
            
            if(!isgraph(tmpfilename[0]))
                return S_NOTREQUIRED;                
            
            fpt = genfs_default_openfile(tmpfilename,"r");
            if(!fpt)
            {
                log_push_error_point(0x4532);
                return S_OPENFILE;
            }
            if (fread((char*)&oph_header,
                      1,sizeof(ophOptionFileHeader),fpt) != sizeof(ophOptionFileHeader))
            {
                genfs_closefile(fpt);
            }
            genfs_closefile(fpt);
            fpt = NULL;
#if PRELOADED_TUNE_CRC32E_TUNE_CHECK
            if (oph_header.header_crc32e != obd2tune_sth_header_parse_optionfile_headercrc32einfo(&sth_header))
            {
                log_push_error_point(0x4533);
                return S_CRC32E;
            }
#endif
        }
        else
        {
            log_push_error_point(0x4522);
            genfs_closefile(fpt);
            return S_READFILE;
        }
    }
    else
    {
        if((fseek(fpt, 36, SEEK_SET)) != 0)
        {
            log_push_error_point(0x4523);
            genfs_closefile(fpt);
            return S_SEEKFILE;
        }
    
        // Grab the name of the corresponding option file
        if(fread((void*)tmpfilename, 1, 20, fpt) < 1)
        {
            log_push_error_point(0x4524);
            genfs_closefile(fpt);
            return S_READFILE;
        }
        tmpfilename[20] = 0;
    }
    if (fpt)
    {
        genfs_closefile(fpt);
    }

    return file_default_getfilename(tmpfilename,optionfilename);
}

//------------------------------------------------------------------------------
// This routine grabs the special option file name stored in the tune file of choice
// Input:   u8  *tunefilename
// Output:  u8  *specialoptionfilename
// Return:  u8  status
// Note: successful output optionfilename maybe a null string
// Engineer: Patrick Downs
// Note: only used in preloaded tune
//------------------------------------------------------------------------------
u8 obd2tune_getspecialoptionfilename(u8 *tunefilename, u8 *specialoptionfilename)
{
    F_FILE  *fpt;
    sthFileHeader sth_header;
    u8  tmpfilename[MAX_TUNE_NAME_LENGTH+1];
    u8  status;
    
    specialoptionfilename[0] = NULL;
    
    fpt = genfs_default_openfile(tunefilename,"rb");
    if (fpt == NULL)
    {
        log_push_error_point(0x454A);
        return S_OPENFILE;
    }
    
    if (!strstr((char*)tunefilename,"sth"))
    {
        genfs_closefile(fpt);
        return S_INPUT;
    }
    
    status = obd2tune_read_sth_header(fpt,&sth_header,NULL);
    if (status == S_SUCCESS)
    {
        genfs_closefile(fpt);
        fpt = NULL;
        
        status = obd2tune_validate_sth_header(&sth_header);
        if (status != S_SUCCESS)
        {
            log_push_error_point(0x454B);
            return S_BADCONTENT;
        }
        
        //check option file
        memset(tmpfilename, 0, sizeof(tmpfilename));
        obd2tune_sth_header_parse_specialoptionfilename(&sth_header,tmpfilename);
        
        if(!isgraph(tmpfilename[0]))
            return S_NOTREQUIRED;
        
        fpt = genfs_default_openfile(tmpfilename,"r");
        if(!fpt)
        {
            log_push_error_point(0x454C);
            return S_OPENFILE;
        }
        
        genfs_closefile(fpt);
    }
    else
    {
        log_push_error_point(0x454D);
        genfs_closefile(fpt);
        return S_READFILE;
    }
    
    return file_default_getfilename(tmpfilename,specialoptionfilename);
}

//------------------------------------------------------------------------------
// Check for a healthy VBatt, VPP, etc
// Return:  u8  status
// Engineer: Quyen Leba
//------------------------------------------------------------------------------
u8 obd2tune_checkprogrammingcondition()
{
    float fval;
    u8  status;

    if (SETTINGS_IsDemoMode())
    {
        return S_SUCCESS;
    }//if (SETTINGS_IsDemoMode())...

    adc_init(ADC_FLASH);
    status = adc_read(ADC_VBAT,&fval);
    if (status != S_SUCCESS)
    {
        return S_FAIL;
    }
    else if (fval < 11.0)
    {
        return S_LOWVBATT;
    }
    
//    status = adc_read(ADC_VPP,&fval);
//    if (status != S_SUCCESS)
//    {
//        return S_FAIL;
//    }
//    else if (fval < 11.0)
//    {
//        return S_LOWVPP;
//    }
    
    return S_SUCCESS;
}

//------------------------------------------------------------------------------
// Reset ECM
// Inputs:  u16 ecm_type
//          VehicleCommType vehiclecommtype
// Return:  u8  status
// Engineer: Quyen Leba
//------------------------------------------------------------------------------
u8 obd2tune_reset_ecm(u16 ecm_type)
{    
    VehicleCommType vehiclecommtype;
    u8 status;

    if (SETTINGS_IsDemoMode())
    {
        return S_SUCCESS;
    }
    
    vehiclecommtype = ECM_GetCommType(ecm_type);
    status = S_SUCCESS; 
    
    switch (obd2_get_oemtype())
    {
#ifdef __DCX_MANUF__        
    case OemType_DCX:
        switch(vehiclecommtype)
        {
        case CommType_CAN:
            status = obd2can_dcx_reset_ecm(ecm_type);
            break; 
        default:
            status = S_COMMTYPE; 
            break;
        }
        break;
#endif
#ifdef __FORD_MANUF__
    case OemType_FORD:
        switch(vehiclecommtype)
        {
        case CommType_CAN:
            status = obd2can_ford_reset_ecm(ecm_type);
            break; 
        case CommType_SCP:
        case CommType_SCP32:
            status = obd2scp_reset_ecm(ecm_type);
            break;
        default:
            status = S_COMMTYPE; 
            break;
        }
        break;
#endif
#ifdef __GM_MANUF__
    case OemType_GM:
        switch(vehiclecommtype)
        {
        case CommType_CAN:
            status = obd2can_gm_reset_ecm(ecm_type);
            break; 
        case CommType_VPW:
            break;
        default:
            status = S_COMMTYPE; 
            break;
        }
        break;
#endif
    default:
        status = S_NOTSUPPORT;
        break;
    }  
    
    if(status != S_SUCCESS)
    {
        log_push_error_point(0x4201); 
    }
    
    return status;
}

//------------------------------------------------------------------------------
// Inputs:  UnlockTask unlocktask
//          u16 ecm_type
//          VehicleCommType commtype
// Output:  void *priv (private data)
// Return:  u8  status
// Engineer: Quyen Leba
//------------------------------------------------------------------------------
u8 obd2tune_unlockecm(UnlockTask unlocktask,
                      u16 ecm_type, VehicleCommType commtype, void *priv)
{
    u8 status;

    if (SETTINGS_IsDemoMode())
    {
        if (commtype == CommType_CAN)
        {
            return S_SUCCESS;
        }
        else if (commtype == CommType_VPW)
        {
            return S_SUCCESS;
        }
        else if (commtype == CommType_SCP || commtype == CommType_SCP32)
        {
            return S_SUCCESS;
        }
        else
        {
            return S_COMMTYPE;
        }
    }//if (SETTINGS_IsDemoMode())...

    if (commtype == CommType_CAN)
    {
        status = obd2can_unlockecm(unlocktask,ecm_type,FALSE);
    }
    else if (commtype == CommType_VPW)
    {
        status = obd2vpw_unlockecm(ECM_GetUnlockIndex(ecm_type),FALSE);
    }
    else if (commtype == CommType_SCP || commtype == CommType_SCP32)
    {
        u8  l_seed;
        u8  *ptr;
        
        status = obd2scp_unlockecm(ecm_type,FALSE,&l_seed);
        if (priv && (status == S_SUCCESS))
        {
            ptr = (u8*)priv;
            *ptr = l_seed;
        }
    }
    else
    {
        log_push_error_point(0x4412);
        status = S_COMMTYPE;
    }
    return status;
}

//------------------------------------------------------------------------------
// Inputs:  u16 ecm_type
//          VehicleCommType commtype
//          void *priv (private data)
// Return:  u8  status
// Engineer: Quyen Leba
//------------------------------------------------------------------------------
u8 obd2tune_requesthighspeed(u16 ecm_type, VehicleCommType commtype, void *priv)
{
    u8 status;

    if (SETTINGS_IsDemoMode())
    {
        if (commtype == CommType_CAN)
        {
            return S_SUCCESS;
        }
        else if (commtype == CommType_VPW)
        {
            return S_SUCCESS;
        }
        else if (commtype == CommType_SCP || commtype == CommType_SCP32)
        {
            return S_SUCCESS;
        }
        else
        {
            return S_COMMTYPE;
        }
    }//if (SETTINGS_IsDemoMode())...

    if (commtype == CommType_CAN)
    {
        //CAN for now, doesn't need to request high speed
        status = S_SUCCESS;
    }
    else if (commtype == CommType_VPW)
    {
        status = obd2vpw_requesthighspeed();
    }
    else if (commtype == CommType_SCP || commtype == CommType_SCP32)
    {
        u8 *ptr;
        
        if (priv)
        {
            ptr = (u8*)priv;
            //TODOQ: skip this if 448K Black Oak (SCP32)
            status = obd2scp_requesthighspeed(commtype,*ptr);
            if (status != S_SUCCESS)
            {
                //in case of SCP, if change to high speed fail, it's ok,
                //we can do it at low speed
                log_push_error_point(0x4413);
                status = S_BAUDUNCHANGED;
            }
        }
        else
        {
            log_push_error_point(0x4414);
            status = S_INPUT;
        }
    }
    else
    {
        log_push_error_point(0x4415);
        status = S_COMMTYPE;
    }
    return status;
}


//------------------------------------------------------------------------------
// Find applicable utility for an ecm.
// Input:   u16 ecm_type
//          VehicleCommType commtype
//          bool isdownloadutility (TRUE: it's a download utility)
// Output:  u8  *utilitychoice
// Return:  u8  status
// Engineer: Quyen Leba, Patrick Downs
//------------------------------------------------------------------------------
u8 obd2tune_findapplicableutility(u16 ecm_type, bool isdownloadutility,
                                  u8 *utilitychoice)
{
    u8  i;
    u16 utility_index;
    funcptr_utilfunctest *utilityfunctest;

    *utilitychoice = 0;
    if (isdownloadutility)
    {
        if (!ECM_IsDUtiRequired(ecm_type))
        {
            return S_UTILITYNOTREQUIRED;
        }
    }
    else
    {
        if (!ECM_IsUUtiRequired(ecm_type))
        {
            return S_UTILITYNOTREQUIRED;
        }
    }
    
    for(i=0;i<MAX_UTILITY_OPTIONS;i++)
    {
        utilityfunctest = NULL;
        if (isdownloadutility)
        {
            utility_index = ECM_GetDUtiIndex(ecm_type, i);
        }
        else
        {
            utility_index = ECM_GetUUtiIndex(ecm_type, i);
        }
        /* This is temporary; Implement with scripting later */
#ifdef __FORD_MANUF__        
        /* Ford - Bosch - Diesel (A) EDC17 CP05 2870k (6.7L) requires test function */
        if (obd2_get_oemtype() == OemType_FORD && utility_index == 7)
        {
            utilityfunctest = (funcptr_utilfunctest*)&ford_ecm_utility_testfunc_67L_PCM;
        }
#endif
#ifdef __DCX_MANUF__
        /* Chrysler 2010-2013 GPEC2 ECM (CAN) */
        if (obd2_get_oemtype() == OemType_DCX && utility_index == 1)
        {
            utilityfunctest = (funcptr_utilfunctest*)&dcx_ecm_utility_testfunc_gpec2;
        }
#endif
        
        if (utility_index)
        {
        }
        
        if (utilityfunctest == NULL)
        {
            //assume always allowed
            *utilitychoice = i;
            return S_SUCCESS;
        }
        else
        {
            if ((*utilityfunctest)(ecm_type) == S_SUCCESS)
            {
                *utilitychoice = i;
                return S_SUCCESS;
            }
        }
    }
    return S_UNMATCH;
}

//------------------------------------------------------------------------------
// For ECMs using utility files for upload/download tune, this function will
// download utility file to ECM internal volatile memory and execute it.
// Inputs:  u16 ecm_type
//          VehicleCommType commtype
//          bool isdownloadutility (TRUE: utility is for download tune)
// Return:  u8  status
// Engineer: Quyen Leba
// Note: ECM must be setup (unlocked, switch speed, etc) prior to this function
// Note: this function uses ecm_defs to get utility filename, if name is NULL,
// S_UTILITYNOTREQUIRED is returned
//------------------------------------------------------------------------------
u8 obd2tune_downloadutility(u16 ecm_type, bool isdownloadutility, bool is_cal_only)
{    
    u8 utilitychoice;
    u8 status;
    VehicleCommType commtype;
    
    commtype = ECM_GetCommType(ecm_type);
#ifdef __DCX_MANUF__    
    if((is_cal_only == TRUE)&&(isdownloadutility == TRUE) && (ECM_IsUseVariantId(ecm_type)))
    {
        return S_UTILITYNOTREQUIRED;
    }
#endif

    status = obd2tune_findapplicableutility(ecm_type,isdownloadutility,&utilitychoice);
    if (status == S_UTILITYNOTREQUIRED)
    {
        return S_UTILITYNOTREQUIRED;
    }
    else if (status != S_SUCCESS)
    {
        log_push_error_point(0x4425);
        return status;
    }
    
    if (SETTINGS_IsDemoMode())
    {
        if (commtype == CommType_CAN)
        {
            return S_SUCCESS;
        }
        else if (commtype == CommType_VPW)
        {
            return S_SUCCESS;
        }
        else
        {
            return S_COMMTYPE;
        }
    }//if (SETTINGS_IsDemoMode())...
    
    if(isdownloadutility)
        flasherinfo->utilityflags = (u16)ECM_GetDUtiFlags(ecm_type,utilitychoice);
    else
        flasherinfo->utilityflags = (u16)ECM_GetUUtiFlags(ecm_type,utilitychoice);
    
    switch (obd2_get_oemtype())
    {
#ifdef __DCX_MANUF__        
    case OemType_DCX:
        switch(commtype)
        {
        case CommType_CAN:
            status = obd2can_dcx_downloadutility(ecm_type,utilitychoice,isdownloadutility);
            break; 
        default:
            status = S_COMMTYPE; 
            break;
        }
        break;
#endif
#ifdef __FORD_MANUF__
    case OemType_FORD:
        switch(commtype)
        {
        case CommType_CAN:
            status = obd2can_ford_downloadutility(ecm_type,utilitychoice,isdownloadutility);
            break; 
        default:
            status = S_COMMTYPE; 
            break;
        }
        break;
#endif
#ifdef __GM_MANUF__
    case OemType_GM:
        switch(commtype)
        {
        case CommType_CAN:
            status = obd2can_gm_downloadutility(ecm_type,utilitychoice,isdownloadutility);
            break; 
        case CommType_VPW:
            status = obd2vpw_downloadutility(ecm_type,utilitychoice,isdownloadutility);
            break;
        default:
            status = S_COMMTYPE; 
            break;
        }
        break;
#endif
    default:
        status = S_NOTSUPPORT;
        break;
    }  
    
    if (status != S_SUCCESS && status != S_UTILITYNOTREQUIRED)
    {
        log_push_error_point(0x4416);
    }
        
    return status;
}

//------------------------------------------------------------------------------
// For ECMs using utility files for upload/download tune, it might require
// sending a command to terminate utility session
// Inputs:  u16 ecm_type
//          VehicleCommType commtype
//          bool isdownloadutility (TRUE: utility is for download tune)
// Return:  u8  status
// Engineer: Quyen Leba
//------------------------------------------------------------------------------
u8 obd2tune_exitutility(u16 ecm_type, VehicleCommType commtype,
                        bool isdownloadutility)
{    
    u8 status; 
    
    if (SETTINGS_IsDemoMode())
    {
        if (commtype == CommType_CAN)
        {
            return S_SUCCESS;
        }
        else if (commtype == CommType_VPW)
        {
            return S_SUCCESS;
        }
        else
        {
            return S_COMMTYPE;
        }
    }//if (SETTINGS_IsDemoMode())...
    
    /*Note: for now (Feb062010), all CAN ECMs using same command to exit utility 
    for upload and download. All VPW ECMs we flash don't need exit utility 
    when doing upload*/
    
    switch (obd2_get_oemtype())
    {
#ifdef __DCX_MANUF__        
    case OemType_DCX:
        switch(commtype)
        {
        case CommType_CAN:
            status = obd2can_dcx_exitutility(ecm_type);
            break; 
        default:
            status = S_COMMTYPE; 
            break;
        }
        break;
#endif
#ifdef __FORD_MANUF__
    case OemType_FORD:
        if(commtype != CommType_Unknown)
        {
            status = S_SUCCESS; 
        }
        else
        {
            status = S_COMMTYPE; 
        }
        break;
#endif
#ifdef __GM_MANUF__
    case OemType_GM:
        switch(commtype)
        {
        case CommType_CAN:
            status = obd2can_gm_exitutility(ecm_type);
            break; 
        case CommType_VPW:
            status = obd2vpw_exitutility(ecm_type);
            break;
        default:
            status = S_COMMTYPE; 
            break;
        }
        break;
#endif
    default:
        status = S_NOTSUPPORT;
        break;
    }  
    
    if (status != S_SUCCESS)
    {
        log_push_error_point(0x4417);
    }
        
    return status;
}

//------------------------------------------------------------------------------
// Run a command to finalize download
// Inputs:  u16 ecm_type
//          VehicleCommType commtype
// Return:  u8  status
// Engineer: Quyen Leba
// Note: not all processors require this command; need to check for
// ECM_IsRequireDownloadFinalzing(...)
//------------------------------------------------------------------------------
u8 obd2tune_downloadfinalizing(u16 ecm_type, flasher_info *flasherinfo)
{    
    u8 status; 
    VehicleCommType vehiclecommtype;
    
    vehiclecommtype = ECM_GetCommType(ecm_type);
    
    switch (vehiclecommtype)
    {
    case CommType_CAN:
        if (SETTINGS_IsDemoMode())
        {
            return S_SUCCESS;
        }
        
        switch (obd2_get_oemtype())
        {
#ifdef __DCX_MANUF__
        case OemType_DCX:
            status = obd2can_dcx_downloadfinalizing(ecm_type);
            break;
#endif
#ifdef __FORD_MANUF__
        case OemType_FORD:
            status = obd2can_ford_downloadfinalizing(ecm_type,flasherinfo);
            break;
#endif
#ifdef __GM_MANUF__
        case OemType_GM:
            status = obd2can_gm_downloadfinalizing(ecm_type);
            break;
#endif
        default:
            status = S_FAIL; 
            break; 
        }
        break;
    default:
        status = S_NOTSUPPORT;
        break;
    }
    
    if (status != S_SUCCESS)
    {
        log_push_error_point(0x444A);
    }
    
    return status;
}

//------------------------------------------------------------------------------
// Setup how tester presents are sent during upload/download
// Inputs:  u32 ecm_id
//          bool highspeed,
//          VehicleCommType commtype
//          VehicleCommLevel commlevel
//          bool broadcast
// Return:  u8  status
// Engineer: Quyen Leba
//------------------------------------------------------------------------------
u8 obd2tune_testerpresent_setup(bool highspeed, VehicleCommType commtype)
{
    memset(&tester_present_info,0,sizeof(tester_present_info));
    tester_present_info.commtype = commtype;

    if (commtype == CommType_CAN)
    {
        tester_present_info.speedmode = 0xFF;   //not applicable
    }
    else if (commtype == CommType_VPW)
    {
        if (highspeed)
        {
            tester_present_info.speedmode = VPW_HIGH_SPEED;
        }
        else
        {
            tester_present_info.speedmode = VPW_LOW_SPEED;
        }
    }

    return S_SUCCESS;
}

//------------------------------------------------------------------------------
// Add ecm_id to list of tester present used with loop
// Input:   u32 ecm_id
// Return:  u8  status
// Engineer: Quyen Leba
//------------------------------------------------------------------------------
u8 obd2tune_testerpresent_add_ecm(u32 ecm_id, VehicleCommLevel commlevel)
{
    u8 i;

    if (ecm_id == INVALID_ECM_ID)
    {
        log_push_error_point(0x4418);
        return S_INPUT;
    }
    
    if(commlevel == CommLevel_ISO14229)
    {
        ecm_id = 0x07DF;
    }
    
    for(i=0;i<ECM_MAX_COUNT;i++)
    {
        if (tester_present_info.ecm_id[i] == ecm_id)
        {
            //already in list
            return S_SUCCESS;
        }
    }

    //add new ecm_id
    if (tester_present_info.ecm_id_count >= ECM_MAX_COUNT)
    {
        log_push_error_point(0x4419);
        return S_FAIL;
    }
    else
    {
        tester_present_info.ecm_id[tester_present_info.ecm_id_count] = ecm_id;
        tester_present_info.commlevel[tester_present_info.ecm_id_count] = commlevel;
        tester_present_info.ecm_id_count++;
        return S_SUCCESS;
    }
}

//------------------------------------------------------------------------------
// Remove ecm_id from list of tester present used with loop
// Input:   u32 ecm_id
// Return:  u8  status
// Engineer: Quyen Leba
//------------------------------------------------------------------------------
u8 obd2tune_testerpresent_remove_ecm(u32 ecm_id)
{
    u32 tmp_ecm_id[ECM_MAX_COUNT];
    u8 i,count;

    for(i=0;i<ECM_MAX_COUNT;i++)
    {
        tmp_ecm_id[i] = INVALID_ECM_ID;
    }
    for(i=0,count=0;i<ECM_MAX_COUNT;i++)
    {
        if (tester_present_info.ecm_id[i] != ecm_id &&
            tester_present_info.ecm_id[i] != INVALID_ECM_ID)
        {
            tmp_ecm_id[i] = tester_present_info.ecm_id[count];
            count++;
        }
    }
    for(i=0;i<ECM_MAX_COUNT;i++)
    {
        tester_present_info.ecm_id[count] = tmp_ecm_id[i];
    }
    tester_present_info.ecm_id_count = count;

    return S_SUCCESS;
}

//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
void obd2tune_testerpresent()
{
    int i;
    if (SETTINGS_IsDemoMode())
    {
        return;
    }
    
    if (tester_present_info.commtype == CommType_CAN)
    {
        for(i=0;i<tester_present_info.ecm_id_count;i++)
        {
            obd2can_testerpresent(FALSE, tester_present_info.ecm_id[i],
                                  tester_present_info.commlevel[i]);
        }
    }
    else if (tester_present_info.commtype == CommType_VPW)
    {
        obd2vpw_testerpresent();
    }
    else if (tester_present_info.commtype == CommType_SCP ||
             tester_present_info.commtype == CommType_SCP32)
    {
    }
    else
    {
        //do nothing
    }
}

//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
void obd2tune_disableallnormalcomm(VehicleCommType commtype)
{
    if (SETTINGS_IsDemoMode())
    {
        return;
    }

    if (commtype == CommType_CAN)
    {
        obd2can_disableallnormalcomm();
    }
    else if (commtype == CommType_VPW)
    {
    }
    else if (commtype == CommType_SCP ||
             commtype == CommType_SCP32)
    {
    }
    else
    {
        //do nothing
    }
}

//------------------------------------------------------------------------------
// Get the total bytecount of an ecm defined in ecm defs
// Input:   u16 ecm_type (index of ecm_defs)
// Output:  u32 *ecmsize
// Return:  u8  status
// Engineer: Quyen Leba
//------------------------------------------------------------------------------
u8 obd2tune_getecmsize_byecmtype(u16 ecm_type, u32 *ecmsize)
{
    u16 memblock_flagtype;
    u32 memblock_flagdata;
    u32 bytecount;
    u32 blocksize;
    u8 mem_index;
    
    //TODOQ: check ecm_type

    bytecount = ECM_GetDataOffset(ecm_type);
    
    if (ECM_IsUploadCalOnly(ecm_type))
    {
        for(mem_index=0;mem_index<MAX_ECM_MEMORY_BLOCKS;mem_index++)
        {
            blocksize = ECM_GetCalBlockSize(ecm_type,mem_index);
            if (blocksize == 0)
            {
                break;
            }
            bytecount += blocksize;
            
            memblock_flagtype = ECM_GetCalBlockFlagType(ecm_type,mem_index);
            memblock_flagdata = ECM_GetCalBlockFlagData(ecm_type,mem_index);
            
            if (memblock_flagtype == MEMBLOCK_FLAGS_SIMPLE_FF_APPEND ||
                memblock_flagtype == MEMBLOCK_FLAGS_SIMPLE_00_APPEND)
            {
                bytecount += memblock_flagdata;
            }
            else if (memblock_flagtype == MEMBLOCK_FLAGS_APPEND_FILE_BY_INDEX)
            {
                //TODOQ: handle me
                log_push_error_point(0x441A);
                return S_FAIL;
            }
        }
    }
    else
    {
        for(mem_index=0;mem_index<MAX_ECM_MEMORY_BLOCKS;mem_index++)
        {
            blocksize = ECM_GetOSReadBlockSize(ecm_type,mem_index);
            if (blocksize == 0)
            {
                break;
            }
            bytecount += blocksize;
            
            memblock_flagtype = ECM_GetOSReadBlockFlagType(ecm_type,mem_index);
            memblock_flagdata = ECM_GetOSReadBlockFlagData(ecm_type,mem_index);
            
            if (memblock_flagtype == MEMBLOCK_FLAGS_SIMPLE_FF_APPEND ||
                memblock_flagtype == MEMBLOCK_FLAGS_SIMPLE_00_APPEND)
            {
                bytecount += memblock_flagdata;
            }
            else if (memblock_flagtype == MEMBLOCK_FLAGS_APPEND_FILE_BY_INDEX)
            {
                //TODOQ: handle me
                log_push_error_point(0x441B);
                return S_FAIL;
            }
        }
    }
    *ecmsize = bytecount;
    return S_SUCCESS;
}

//------------------------------------------------------------------------------
// Get the total bytecount of stock file defined for a veh_type
// Input:   u8  veh_type (index of veh_defs)
// Output:  u32 *stocksize
// Return:  u8  status
// Engineer: Quyen Leba
//------------------------------------------------------------------------------
u8 obd2tune_getstocksize_byvehdef(u16 veh_type, u32 *stocksize)
{
    u8  ecm_index;
    u16 ecm_type;
    u32 ecm_size;
    u32 bytecount;
    u8  status;

    //TODOQ: check veh_type

    bytecount = 0;
    for(ecm_index=0;ecm_index<ECM_MAX_COUNT;ecm_index++)
    {
        ecm_type = VEH_GetEcmType(veh_type,ecm_index);
        if (ecm_type == INVALID_ECM_DEF)
        {
            break;
        }
        
        status = obd2tune_getecmsize_byecmtype(ecm_type,&ecm_size);
        if (status != S_SUCCESS)
        {
            log_push_error_point(0x441C);
            return status;
        }
        bytecount += ecm_size;
    }

    *stocksize = bytecount;
    return S_SUCCESS;
}

//------------------------------------------------------------------------------
// Get the total bytecount of all memory blocks of an ecm about to download
// Inputs:  u16 ecm_type
//          bool is_cal_only
// Outputs: u32 *cal_only_size
//          u32 *all_size
// Return:  u8  status
// Engineer: Quyen Leba
// TODOQ: Ford handled a bit differently, for now, only valid for GM
//------------------------------------------------------------------------------
u8 obd2tune_getecmdownloadsize(u16 ecm_type,
                               u32 *cal_only_size, u32 *all_size)
{
    u8  mem_index;
    u32 blocksize;
    u32 cal_bytecount;
    u32 all_bytecount;

    cal_bytecount = 0;
    all_bytecount = 0;
        
    for(mem_index=0;mem_index<MAX_ECM_MEMORY_BLOCKS;mem_index++)
    {
        blocksize = ECM_GetCalBlockSize(ecm_type,mem_index);
        if (blocksize == 0)
        {
            break;
        }
        cal_bytecount += blocksize;
    }
    
    for(mem_index=0;mem_index<MAX_ECM_MEMORY_BLOCKS;mem_index++)
    {
        blocksize = ECM_GetOSBlockSize(ecm_type,mem_index);
        if (blocksize == 0)
        {
            break;
        }
        all_bytecount += blocksize;
    }

    *cal_only_size = cal_bytecount;
    *all_size = all_bytecount;
    return S_SUCCESS;
}

//------------------------------------------------------------------------------
// Compare vin with one stored in tuneos4.txt, if matched, retrieve the tune
// code. This function is used when normal tune code flow has failure.
// Input:   u8  *vin
// Output:  u8  *tunecode (partnumber, strategy, etc)
// Return:  u8  status
// TODOQ: why do we need this crap? Is this some backdoor bs?
//------------------------------------------------------------------------------
u8 obd2tune_get_tunecode_byvin(u8 *vin, u8 *tunecode)
//u8 Do_Wehave_Vinfile(u8 *file_OSpartnumber)
{
    u8 i;
    u8 vin_from_file[19];
    F_FILE *fptr;

    fptr = genfs_default_openfile("tuneos4.txt", "rb");
    if (fptr == NULL)
    {
        log_push_error_point(0x441D);
        return S_OPENFILE;
    }
    
    fread(vin_from_file, 1, 19, fptr);
    fread(tunecode, 1, 8, fptr);
    genfs_closefile(fptr);
    
    for(i=0; i<VIN_LENGTH; i++)
    {
        if(vin[i] != vin_from_file[i])
        {
            log_push_error_point(0x441E);
            return S_FAIL;
        }
    }
    
    tunecode[8] =0;
    tunecode[9] ='*';       //TODOQ: why put this here?

    return(S_SUCCESS);
}

//------------------------------------------------------------------------------
// Extract custom tune list from indexfile.txt
// Assume indexfile.txt file size is 2k bytes max (for now, that's plenty)
// Outputs: tunecode_infolist *tunecodeinfolist
//          u16 *veh_type
// Return:  u8  status
// Engineer: Quyen Leba
//------------------------------------------------------------------------------
u8 obd2tune_getcustomtune_infolist(tunecode_infolist *tunecodeinfolist)
{
#define MAX_CUSTOMTUNE_INFOLIST_BUFFER_SIZE         2048
    F_FILE *ifptr;
    u32 filesize;
    u32 bytecount;
    u32 count;
    u8 i;
    u8 buffer[MAX_CUSTOMTUNE_INFOLIST_BUFFER_SIZE];
    u8 *bptr;
    u8 tuneprop[5][2];  //TODOQ: more on this
    u8 status;

    //#########################################
    // check indexfile.txt first
    //#########################################
    tunecodeinfolist->count = 0;
    ifptr = genfs_user_openfile("indexfile.txt","rb");
    if (ifptr == NULL)
    {
        log_push_error_point(0x441F);
        return S_OPENFILE;
    }
    if (fseek(ifptr,0,SEEK_END) != 0)
    {
        log_push_error_point(0x4420);
        status = S_SEEKFILE;
        goto obd2tune_getcustomtune_infolist_done;
    }
    filesize = ftell(ifptr);
    if (filesize == 0 || filesize > MAX_CUSTOMTUNE_INFOLIST_BUFFER_SIZE)
    {
        log_push_error_point(0x4421);
        status = S_BADCONTENT;
        goto obd2tune_getcustomtune_infolist_done;
    }
    if (fseek(ifptr,0,SEEK_SET) != 0)
    {
        log_push_error_point(0x4422);
        status = S_SEEKFILE;
        goto obd2tune_getcustomtune_infolist_done;
    }
    
    bytecount = fread((char*)buffer,
                       1,MAX_CUSTOMTUNE_INFOLIST_BUFFER_SIZE,ifptr);
    if (bytecount == 0 || bytecount > MAX_CUSTOMTUNE_INFOLIST_BUFFER_SIZE)
    {
        log_push_error_point(0x4423);
        status = S_READFILE;
        goto obd2tune_getcustomtune_infolist_done;
    }

    //#########################################
    // extract custom tunes infolist
    //#########################################
    bptr = buffer;
    if (bptr[1] != 'H' || bptr[2] != ':')
    {
        log_push_error_point(0x4424);
        status = S_BADCONTENT;
        goto obd2tune_getcustomtune_infolist_done;
    }
    else
    {
        bptr += 3;
        while(*bptr++ != ':');

        count = 0;
        while(1)
        {
            tunecodeinfolist->description[count][0] = NULL;
            tunecodeinfolist->tunefilename[count][0] = NULL;
            tunecodeinfolist->optionfilename[count][0] = NULL;
            
            //get tune name (description)
            i = 0;
            while(bptr[0] != 0x0D && bptr[1] != 0x0A && i < MAX_TUNE_NAME_LENGTH)
            {
                tunecodeinfolist->description[count][i++] = *bptr++;
            }
            tunecodeinfolist->description[count][i] = NULL;
            
            bptr += 2;
            tuneprop[count][0] = *bptr++;           // image type
            bptr++;                                 // :
            tuneprop[count][1] = 0x00;
            while(*bptr != ':')                     // processor type
            {
                tuneprop[count][1] <<= 4;
                if (*bptr >= '0' && *bptr <= '9')
                {
                    tuneprop[count][1] |= (*bptr - 0x30);
                }
                else if (*bptr >= 'A' && *bptr <= 'F')
                {
                    tuneprop[count][1] |= (*bptr - 0x37);
                }
                else if (*bptr >= 'a' && *bptr <= 'f')
                {
                    tuneprop[count][1] |= (*bptr - 0x57);
                }
                else
                {
                    tuneprop[count][1] = 0xFF;      // invalid
                }
                bptr++;
            }
            tunecodeinfolist->veh_type[count] = tuneprop[count][1];
            tunecodeinfolist->veh_type[count] = 0x71;   //TODOQ: for debug for now
            
            bptr++;                                 // :

            // get tune filename
            i = 0;
            while(bptr[0] != ':' && bptr[0] != 0x0D && bptr[1] != 0x0A && 
                  i < MAX_TUNE_NAME_LENGTH)
            {
                tunecodeinfolist->tunefilename[count][i++] = *bptr++;
            }
            tunecodeinfolist->tunefilename[count][i] = NULL;    //TODOQ: need to strcat ".bin"  //TODOQ: change indexfile instead
            strcat((char*)tunecodeinfolist->tunefilename[count],".bin");   //this is for now

            // get option filename
            if (*bptr == ':')
            {
                bptr++;
                i = 0;
                while(bptr[0] != 0x0D && bptr[1] != 0x0A && i < MAX_TUNE_NAME_LENGTH)
                {
                    tunecodeinfolist->optionfilename[count][i++] = *bptr++;
                }
                tunecodeinfolist->optionfilename[count][i] = NULL;  //TODOQ: need to strcat ".bin"  //TODOQ: change indexfile instead
                strcat((char*)tunecodeinfolist->optionfilename[count],".bin");   //this is for now
            }
            bptr += 2;
            count++;

            if (((bptr[0] != '0') &&( bptr[1] != ':'))||(bptr[0] == 0))
            {
                break;
            }
            bptr+=2;
        }//while(1)...
    }
    tunecodeinfolist->count = count;
    status = S_SUCCESS;

obd2tune_getcustomtune_infolist_done:
    if (ifptr)
    {
        genfs_closefile(ifptr);
    }
    return status;
}

//------------------------------------------------------------------------------
// Inputs:  ecm_info *ecm
//          TuneType tunetype
// Return:  u8  status
// Engineer: Quyen Leba
//------------------------------------------------------------------------------
u8 obd2tune_checkvehiclecode(ecm_info *ecm, TuneType tunetype)
{
    //TODOQ: complete me
    return S_SUCCESS;
}

//------------------------------------------------------------------------------
// Compare vin2 against vin1 (skip byte check with mask_byte)
// Inputs:  u8  *vin1 (may have mask bytes)
//          u8  *vin2
//          u8  mask_byte (0: no mask)
// Return:  bool matched (TRUE: matched)
// Engineer: Quyen Leba
//------------------------------------------------------------------------------
bool obd2tune_isvin_matched(const u8 *vin1, const u8 *vin2, u8 mask_byte)
{
    u8  i;

    for(i=0;i<VIN_LENGTH;i++)
    {
        if (vin1[i] != vin2[i])
        {
            if (mask_byte == 0)
            {
                return FALSE;
            }
            else if (vin1[i] != mask_byte)
            {
                return FALSE;
            }
        }
    }//for(i=...
    return TRUE;
}

//------------------------------------------------------------------------------
// Use whenever upload stock is required. This will check against previous VIN.
// Use this function to determine if married count must be incremented.
// Input:   u8  *vin
//          u8  *codes
// Return:  bool matched (TRUE: matched)
// Engineer: Quyen Leba, Patrick Downs
//------------------------------------------------------------------------------
bool obd2tune_ispreviousvehicle_matched(u8 *vin, u8 *codes)
{
    u32 length;
    u32 i;
    u8* p;
    bool isMatch;
    
    isMatch = FALSE;
    
    if (strncmp((char*)vin,(char*)SETTINGS_TUNE(prev_vin),VIN_LENGTH) == 0)
    {
        isMatch = TRUE;
    }
    else
    {
        for(i=0; i<VIN_LENGTH; i++) // Check for BLANK VIN
        {
            if(vin[i] != 0xFF)
            {
                break;
            }
        }
        
        if(i == VIN_LENGTH) // Blank VIN is All 17 digits 0xFF
        {
            // If blank VIN, compare with codes
            p = (u8*)strchr((char*)SETTINGS_TUNE(vehiclecodes), '*'); // Look for '*' delimiter between codes
            if(p)
            {
                length = (p - SETTINGS_TUNE(vehiclecodes)); // Get code length
                if (strncmp((char*)codes,(char*)SETTINGS_TUNE(vehiclecodes),length) == 0)
                {
                    isMatch = TRUE; // If codes match, accept as vehicle match
                }
            }
        }
    }
    
    return isMatch;
}

//------------------------------------------------------------------------------
// Save vin to previous vin in settings
// Input:   u8  *vin
// Return:  u8  status
// Engineer: Quyen Leba
//------------------------------------------------------------------------------
u8 obd2tune_setpreviousvin(u8 *vin)
{
    memcpy(SETTINGS_TUNE(prev_vin),vin,VIN_LENGTH);
    SETTINGS_SetTuneAreaDirty();
    settings_update(SETTINGS_UPDATE_IF_DIRTY);
    return S_SUCCESS;
}

//------------------------------------------------------------------------------
// Check if vehiclecodes is valid (i.e. "code1*codes2*" format)
// Inputs:  u8  *vehiclecodes
// Return:  u8  status
// Engineer: Quyen Leba
//------------------------------------------------------------------------------
u8 obd2tune_validate_vehiclecodes(u8 *vehiclecodes)
{
    u32 i;
    u8  codecount;

    if (vehiclecodes == NULL)
    {
        log_push_error_point(0x4443);
        return S_INPUT;
    }
    else if (!isgraph(vehiclecodes[0]))
    {
        log_push_error_point(0x4444);
        return S_BADCONTENT;
    }
    else if (strlen((char*)vehiclecodes) >= sizeof(SETTINGS_TUNE(vehiclecodes)))
    {
        log_push_error_point(0x4445);
        return S_BADCONTENT;
    }

    codecount = 0;
    for(i=0;i<strlen((char*)vehiclecodes);i++)
    {
        if (!isgraph(vehiclecodes[i]))
        {
            log_push_error_point(0x4446);
            return S_BADCONTENT;
        }
        if (vehiclecodes[i] == TUNECODE_SEPARATOR)
        {
            codecount++;
        }
    }
    if (codecount == 0 || codecount > ECM_MAX_COUNT)
    {
        log_push_error_point(0x4447);
        return S_FAIL;
    }
    return S_SUCCESS;
}

//------------------------------------------------------------------------------
// Marry device to vehicle
// Inputs:  u16 veh_type (define by veh_defs)
//          u32 stocktunesize
//          u8  *vehicle_serial_number
// Return:  u8  status
// Engineer: Quyen Leba
//------------------------------------------------------------------------------
u8 obd2tune_setmarried(u16 veh_type, u32 stocktunesize,
                       u8 *vehicle_serial_number)
{
    u8 vin[VIN_LENGTH+1];
    VehicleCommType vehiclecommtype;
    u8  i;
    u8  status;

    vehiclecommtype = ECM_GetCommType(VEH_GetMainEcmType(veh_type));

    memcpy((char*)vin,(char*)flasherinfo->vin,sizeof(vin));
    status = obd2_validatevin(vin);
    if (status != S_SUCCESS)
    {
        if (VEH_IsAllowBlankVIN(veh_type))
        {
            //do nothing: should only used in development
            status = S_SUCCESS;
        }
        else
        {
            log_push_error_point(0x4430);
            return S_FAIL;
        }
    }

    SETTINGS_TUNE(veh_type) = veh_type;
    SETTINGS_TUNE(comm_type) = (u8)vehiclecommtype;
    SETTINGS_TUNE(stockfilesize) = stocktunesize;
    memcpy(SETTINGS_TUNE(vin),vin,VIN_LENGTH);
    memcpy(SETTINGS_TUNE(vehicle_serial_number),vehicle_serial_number,
           sizeof(SETTINGS_TUNE(vehicle_serial_number)));
    SETTINGS_SetMarried();
    
    SETTINGS_TUNE(vehiclecodes)[0] = NULL;
    SETTINGS_TUNE(secondvehiclecodes)[0] = NULL;
    for(i=0;i<flasherinfo->ecminfo.ecm_count;i++)
    {
        strcat((char*)SETTINGS_TUNE(vehiclecodes),
               (char*)flasherinfo->ecminfo.codes[i][0]);
        strcat((char*)SETTINGS_TUNE(vehiclecodes),TUNECODE_SEPARATOR_STRING);
        
        memcpy((char*)SETTINGS_TUNE(customtune_vehiclecodes),
               (char*)SETTINGS_TUNE(vehiclecodes),sizeof(SETTINGS_TUNE(vehiclecodes)));
        
        strcat((char*)SETTINGS_TUNE(secondvehiclecodes),
               (char*)flasherinfo->ecminfo.second_codes[i][0]);
        strcat((char*)SETTINGS_TUNE(secondvehiclecodes),TUNECODE_SEPARATOR_STRING);
        
        memcpy((char*)SETTINGS_TUNE(customtune_secondvehiclecodes),
               (char*)SETTINGS_TUNE(secondvehiclecodes),sizeof(SETTINGS_TUNE(secondvehiclecodes)));
    }

    SETTINGS_SetTuneAreaDirty();
    settings_update(SETTINGS_UPDATE_IF_DIRTY);
    
    //TODOQ: this + manuf specific set
    return S_SUCCESS;
}

//------------------------------------------------------------------------------
// Check if device is communcating with its married vehicle
// Input:   u16 veh_type (define by veh_defs)
// Output:  u8  *vin (must be able to store VIN_LENGTH+1 bytes)
//          u8  *vehicle_serial_number (must be able to store 32 bytes)
// Return:  u8  status
// Engineer: Quyen Leba
// Note: if status is not S_SUCCESS, vin and vehicle_serial_number are invalid
//------------------------------------------------------------------------------
u8 obd2tune_checkmarried(u16 veh_type, u8 *vin, u8 *vehicle_serial_number)
{
    ecm_info ecm;
    u8 vin_buffer[VIN_LENGTH+1];
    u8 vehicle_serial_number_buffer[32];    //do not change buffer size
                                            //must be same as Settings_Tune.vehicle_serial_number
    VehicleCommType vehiclecommtype;
    u8 status;

    if (veh_type != SETTINGS_TUNE(veh_type))
    {
        log_push_error_point(0x442E);
        return S_INVALIDBINARYMARRIED;
    }
    
    if (obd2_validate_ecmbinarytype(veh_type) != S_SUCCESS)
    {
        log_push_error_point(0x442F);
        return S_INVALIDBINARY;
    }
    
    vehiclecommtype = ECM_GetCommType(VEH_GetMainEcmType(veh_type));
    
    if (obd2_readecminfo(&ecm, &vehiclecommtype) != S_SUCCESS)
    {
        log_push_error_point(0x4449);
        return S_READVINFAIL;
    }
    
    strcpy((char*)vin_buffer, (char const*)ecm.vin);
    status = obd2_validatevin(vin_buffer);
    
    if (status != S_SUCCESS && status != S_VINBLANK && status != S_VININVALID)
    {
        //TODOQ: more on this, it's something wrong if married and vin has problem
        log_push_error_point(0x4431);
        return S_READVINFAIL;
    }
    
    if (strncmp((char*)vin_buffer,(char*)SETTINGS_TUNE(vin),VIN_LENGTH) != 0)
    {
        log_push_error_point(0x4432);
        return S_VINUNMATCH;
    }

    status = obd2_readserialnumber(vehicle_serial_number_buffer,
                                   &vehiclecommtype);
    if (status == S_NOTSUPPORT)
    {
        memset(vehicle_serial_number_buffer,0,
               sizeof(vehicle_serial_number_buffer));
    }
    else if (status != S_SUCCESS)
    {
        log_push_error_point(0x4433);
        return S_READSERIAL;
    }
    
    if (strncmp((char*)vehicle_serial_number_buffer,
                (char*)SETTINGS_TUNE(vehicle_serial_number),
                sizeof(SETTINGS_TUNE(vehicle_serial_number))) != 0)
    {
        log_push_error_point(0x4434);
        return S_SERIALUNMATCH;
    }
    
    // Compare overlay data
    if (!SETTINGS_IsDemoMode())
    {
        status = obd2tune_compare_overlaydata(flasherinfo->vehicle_type);
        if (status != S_SUCCESS && status != S_NOTREQUIRED)
        {
            log_push_error_point(0x4448);
            return S_UNMATCH;
        }
    }
    
    if (vin)
    {
        memcpy((char*)vin,(char*)vin_buffer,VIN_LENGTH+1);
    }
    if (vehicle_serial_number)
    {
        memcpy((char*)vehicle_serial_number,
               (char*)vehicle_serial_number_buffer,
               sizeof(vehicle_serial_number_buffer));
    }
    
    //TODOQ: this + manuf specific check (i.e. Ford: security byte)
    return S_SUCCESS;
}

//------------------------------------------------------------------------------
// Check if stock file is healthy
// Input:   u16 veh_type
// Return:  u8  status
// Engineer: Quyen Leba
//------------------------------------------------------------------------------
u8 obd2tune_checkstockfile(u16 veh_type)
{
    u8 status;
    u32 calc_length;

//    if (veh_type != SETTINGS_TUNE(veh_type))
//    {
//        log_push_error_point(0x4435);
//        return S_FAIL;
//    }
    status = obd2tune_getstocksize_byvehdef(veh_type,&calc_length);
    if (status != S_SUCCESS)
    {
        log_push_error_point(0x4436);
        return status;
    }
    return filestock_verify(SETTINGS_TUNE(stockfilesize),calc_length);
}

/**
 *  @brief Save vehicle specific parameters such as PATS, VID, etc to 
 *         settingstune
 *
 *  @param [in]     veh_type        Vehicle binary type
 *  @param [in]     ishandleapply   Apply overlay data?
 *
 *  @return Status
 */
u8 obd2tune_handle_overlaydata(u16 veh_type, u8 *vin, bool isdoapply)
{
    u8 status;

    switch (obd2_get_oemtype())
    {
#ifdef __DCX_MANUF__        
    case OemType_DCX:
        status = S_NOTREQUIRED;
        break;
#endif
#ifdef __FORD_MANUF__
    case OemType_FORD:
        status = obd2tune_ford_handle_overlaydata(veh_type, vin, isdoapply);
        break;
#endif
#ifdef __GM_MANUF__
    case OemType_GM:
        status = S_NOTREQUIRED;
        break;
#endif
    default:
        status = S_NOTSUPPORT;
        break;
    }
    return status; 
}

//------------------------------------------------------------------------------
// Save vehicle specific parameters such as PATS, VID, etc to settingstune
// Inputs:  u16 veh_type
//          bool ishandleapply
// Return:  u8  status
// Engineer: Quyen Leba
//------------------------------------------------------------------------------
#ifndef OBD2TUNE_COMPARE_OVERLAYDATA_DEFINED
#error "odb2.c: no definition for obd2tune_compare_overlaydata(...)"
#endif

//------------------------------------------------------------------------------
// This routine makes the preloaded tune changes in the copy of the stock file
// preloaded tune file Format:
//      Header Indentifer       = 1 byte    (char)  = 0x04
//      Header Length           = 4 bytes   (int)
//      ECM type                = 1 byte    (char)
//      preloaded tune name     = 10 bytes  (char)
//      Tune description        = 20 bytes  (char)
//      Option file name        = 20 bytes  (char)
//      Data                    = variable
// Inputs:  u32 offset (offset to correct position in flash file where this
//                      tune file can apply changes)
//          u8  *tunefilename
// Return:  u8  status
// Engineer: Rhonda Burns, Quyen Leba
// Date: 6/15/06
//------------------------------------------------------------------------------
u8 obd2tune_apply_preloadedtune_stf(u32 offset, u8 *tunefilename)
{
    F_FILE  *sfptr;
    u8  databuffer[256];
    u8  tmpaddress[4];
    u32 position;
    u8  sequence;
    u8  data_length;
    u8  status;

    sfptr = NULL;
    status = S_SUCCESS;
        
    sfptr = genfs_default_openfile(tunefilename,"rb");
    if (sfptr == NULL)
    {
        log_push_error_point(0x4437);
        status = S_OPENFILE;
        goto obd2tune_apply_preloadedtune_stf_done;
    }
    
    //skip the header
    if (fseek(sfptr,56,SEEK_SET) != 0)
    {
        log_push_error_point(0x4438);
        status = S_SEEKFILE;
        goto obd2tune_apply_preloadedtune_stf_done;
    }
    
    while (!feof(sfptr))
    {
        // Read sequence number
        if(fread(&sequence, 1, 1,sfptr) != 1)
        {
            log_push_error_point(0x4439);
            status = S_READFILE;
            goto obd2tune_apply_preloadedtune_stf_done;
        }
        // Get address of change
        if(fread(tmpaddress, 1, sizeof(tmpaddress),sfptr) != sizeof(tmpaddress))
        {
            log_push_error_point(0x443A);
            status = S_READFILE;
            goto obd2tune_apply_preloadedtune_stf_done;
        }
        
        // Put address in correct order
        position = tmpaddress[0];
        position = ((position<<8)|tmpaddress[1]);
        position = ((position<<8)|tmpaddress[2]);
        position = ((position<<8)|tmpaddress[3]);
        // offset: starting point of this ecm in stock file
        position += offset;
        
        // How many data bytes?
        if(fread(&data_length, 1, sizeof(u8),sfptr) != 1)
        {
            log_push_error_point(0x443B);
            status = S_READFILE;
            goto obd2tune_apply_preloadedtune_stf_done;
        }
        
        // No more data, exit
        if(data_length == 0)
        {
            break;
        }
        
        // Grab data
        if (fread(databuffer, 1, (size_t)data_length,sfptr) != data_length)
        {
            log_push_error_point(0x443C);
            status = S_READFILE;
            goto obd2tune_apply_preloadedtune_stf_done;
        }
        status = filestock_updateflashfile(position,databuffer,data_length);
        if(status != S_SUCCESS)
        {
            log_push_error_point(0x443D);
            status = S_WRITEFILE;
            goto obd2tune_apply_preloadedtune_stf_done;
        }
    }//while (!feof(sfptr))...
    
obd2tune_apply_preloadedtune_stf_done:
    if (sfptr)
    {
        genfs_closefile(sfptr);
    }
    return status;
}

//------------------------------------------------------------------------------
// A helper to apply the change from stg/opg change block
// Inputs:  u8  *change_block_data
//          u32 offset
// Return:  u8  status
// Engineer: Rhonda Burns, Quyen Leba
//------------------------------------------------------------------------------
u8 obd2tune_apply_change_helper_stg_opg(u8 *change_block_data, u32 offset)
{
    struct
    {
        u8  change_type;
        u32 address;
        u8  length;
        u8  sign;
        float decmial_offset;
        float multiplier;
        float adjustment;
        float min;
        float max;
    }change_block;
    union
    {
        u8  hex[4];
        float fvalue;
        unsigned int uint32;
        signed int sint32;
    }fvalue;
    u8  originaldata[256];
    u8  newdata[256];
    u32 bytecount;
    u8  status;
    
    //##########################################################################
    // Translate data
    //##########################################################################
    change_block.change_type = change_block_data[0];
    switch(change_block.change_type)
    {
    case 'E':
        return S_SUCCESS;
    case 'R':
    case 'M':
    case 'A':
        //acceptable type
        break;
    default:
        log_push_error_point(0x443E);
        return S_BADCONTENT;
    }
    
    // Address of data in image
    change_block.address = 
        ((u32)change_block_data[4]) | (((u32)change_block_data[3]) << 8) |
        (((u32)change_block_data[2]) << 16) | (((u32)change_block_data[1]) << 24);
    change_block.address += offset;
    
    change_block.length = change_block_data[5];
    change_block.sign = change_block_data[6];
    
    fvalue.hex[3] = change_block_data[7];
    fvalue.hex[2] = change_block_data[8];
    fvalue.hex[1] = change_block_data[9];
    fvalue.hex[0] = change_block_data[10];
    change_block.decmial_offset = fvalue.fvalue;
    
    fvalue.hex[3] = change_block_data[11];
    fvalue.hex[2] = change_block_data[12];
    fvalue.hex[1] = change_block_data[13];
    fvalue.hex[0] = change_block_data[14];
    change_block.multiplier = fvalue.fvalue;
    
    fvalue.hex[3] = change_block_data[15];
    fvalue.hex[2] = change_block_data[16];
    fvalue.hex[1] = change_block_data[17];
    fvalue.hex[0] = change_block_data[18];
    change_block.adjustment = fvalue.fvalue;
    
    fvalue.hex[3] = change_block_data[19];
    fvalue.hex[2] = change_block_data[20];
    fvalue.hex[1] = change_block_data[21];
    fvalue.hex[0] = change_block_data[22];
    change_block.min = fvalue.fvalue;
    
    fvalue.hex[3] = change_block_data[23];
    fvalue.hex[2] = change_block_data[24];
    fvalue.hex[1] = change_block_data[25];
    fvalue.hex[0] = change_block_data[26];
    change_block.max = fvalue.fvalue;
    
    //##########################################################################
    //
    //##########################################################################
    
    status = filestock_set_position(change_block.address);
    if (status != S_SUCCESS)
    {
        log_push_error_point(0x443F);
        return status;
    }
    status = filestock_read(originaldata,change_block.length,&bytecount);
    if (status != S_SUCCESS || bytecount != change_block.length)
    {
        log_push_error_point(0x4440);
        status = S_READFILE;
        return status;
    }

    if (change_block.change_type == 'R')
    {
        fvalue.fvalue = change_block.adjustment;
    }
    else
    {
        fvalue.uint32 = 0;
        if (change_block.length == 1)
        {
            if (change_block.sign)
            {
                fvalue.fvalue = (float)((s8)(originaldata[0]));
            }
            else
            {
                fvalue.fvalue = (float)((u8)(originaldata[0]));
            }
        }
        else if (change_block.length == 2)
        {
            fvalue.hex[1] = originaldata[0];
            fvalue.hex[0] = originaldata[1];
            if (change_block.sign)
            {
                fvalue.fvalue = (float)((s16)(fvalue.uint32));
            }
            else
            {
                fvalue.fvalue = (float)((u16)(fvalue.uint32));
            }
        }
        else if (change_block.length == 4)
        {
            fvalue.hex[3] = originaldata[0];
            fvalue.hex[2] = originaldata[1];
            fvalue.hex[1] = originaldata[2];
            fvalue.hex[0] = originaldata[3];
            
            //TODOQ: old code did this, does it actually work this way !?!
            if (change_block.sign)
            {
                fvalue.fvalue = (float)((s32)(fvalue.uint32));
            }
            else
            {
                //do nothing
            }
        }
        else
        {
            log_push_error_point(0x4441);
            return S_BADCONTENT;
        }
        
        fvalue.fvalue *= change_block.multiplier;
        fvalue.fvalue += change_block.decmial_offset;
        
        if (change_block.change_type == 'A')
        {
            fvalue.fvalue += change_block.adjustment;
        }
        else
        {
            fvalue.fvalue *= change_block.adjustment;
        }
        
        if (fvalue.fvalue < change_block.min)
        {
            fvalue.fvalue = change_block.min;
        }
        else if (fvalue.fvalue > change_block.max)
        {
            fvalue.fvalue = change_block.max;
        }
    }
    
    //TODOQ: from old code, what about 'R' !?!
    fvalue.fvalue -= change_block.decmial_offset;
    fvalue.fvalue /= change_block.multiplier;
    
    if (change_block.length == 1)
    {
        //TODOQ: from old code, does it really want to cast it like that !?!
        if (change_block.sign)
        {
            fvalue.sint32 = (s32)fvalue.fvalue;
        }
        else
        {
            fvalue.uint32 = (u32)fvalue.fvalue;
        }
        newdata[0] = fvalue.hex[0];
    }
    else if (change_block.length == 2)
    {
        //TODOQ: from old code, does it really want to cast it like that !?!
        if (change_block.sign)
        {
            fvalue.sint32 = (s32)fvalue.fvalue;
        }
        else
        {
            fvalue.uint32 = (u32)fvalue.fvalue;
        }
        newdata[0] = fvalue.hex[1];
        newdata[1] = fvalue.hex[0];
    }
    else if (change_block.length == 4)
    {
        newdata[0] = fvalue.hex[3];
        newdata[1] = fvalue.hex[2];
        newdata[2] = fvalue.hex[1];
        newdata[3] = fvalue.hex[0];
    }
    else
    {
        log_push_error_point(0x4442);
        return S_BADCONTENT;
    }
    
    status = filestock_updateflashfile(change_block.address,
                                       newdata,change_block.length);
    return status;
}

//------------------------------------------------------------------------------
// A helper to apply the change from sth/oph change block
// Inputs:  F_FILE *optionfptr
//          u32 starting_offset (effective starting position of flash.bin)
//          u32 offset_4_changes (starting address of option file)
// Return:  u8  status
// Engineer: Quyen Leba
//------------------------------------------------------------------------------
u8 obd2tune_apply_change_helper_sth_oph(F_FILE *optionfptr, u32 starting_offset,
                                        u32 offset_4_changes)
{
    ophOptionItemRecord_Generic option_record;
    union
    {
        u8  hex[4];
        float fvalue;
        unsigned int uint32;
        signed int sint32;
    }fvalue;
    u8  originaldata[256];
    u8  newdata[256];
    u32 readlength;
    u32 bytecount;
    u32 processed;
    u8  status;
    
    status = S_SUCCESS;

    if(offset_4_changes != 0xFFFFFFFF) // Has a new adjustment point been selected
    {
        //move to correct place in file
        if(fseek(optionfptr,offset_4_changes, SEEK_SET) != 0)
        {
            log_push_error_point(0x4460);
            status = S_SEEKFILE;
            goto obd2tune_apply_change_helper_sth_oph_done;
        }
        
        while(1)
        {
            if (fread(&option_record,1,sizeof(option_record),optionfptr) !=
                sizeof(option_record))
            {
                log_push_error_point(0x4461);
                status = S_READFILE;
                goto obd2tune_apply_change_helper_sth_oph_done;
            }
#if STH_OPH_ENCRYPTION_ENABLED
            crypto_blowfish_decryptblock_critical((u8*)&option_record,
                                                  sizeof(option_record));
#else
            //do nothing
#endif
            
            //TODOQ: shouldn't do it like this
            if ((option_record.opcode == 'E') || (option_record.opcode == 'e'))
            {
                //termination found
                break;
            }
            
            //check for valid opcode
            switch(option_record.opcode)
            {
            case 'a': case 'A':
            case 'r': case 'R':
            case 'm': case 'M':
            case 'd': case 'D':
            case 'c': case 'C':
            //case 'l': case 'L':
                //do nothing
                break;
            default:
                log_push_error_point(0x4462);
                status = S_BADCONTENT;
                goto obd2tune_apply_change_helper_sth_oph_done;
            }
            
            //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
            // Direct replace: ophOptionItemRecord_Opcode_d
            //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
            if (option_record.opcode == 'd')
            {
                ophOptionItemRecord_Opcode_d *option_record_d_ptr;

                option_record_d_ptr = (ophOptionItemRecord_Opcode_d*)&option_record;
                if (option_record_d_ptr->length > sizeof(option_record_d_ptr->data))
                {
                    u32 length;

                    length = sizeof(option_record_d_ptr->data);
                    memcpy((char*)originaldata,
                           (char*)option_record_d_ptr->data,length);
                    processed = 0;
                    while(1)
                    {
                        status = filestock_updateflashfile(option_record.address + processed,
                                                           originaldata,
                                                           length);
                        if (status != S_SUCCESS)
                        {
                            log_push_error_point(0x4465);
                            goto obd2tune_apply_change_helper_sth_oph_done;
                        }

                        processed += length;
                        if (processed >= option_record_d_ptr->length)
                        {
                            break;
                        }
                        else
                        {
                            //this record is raw data of 'd' opcode
                            if (fread(originaldata,1,sizeof(ophOptionItemRecord_Generic),optionfptr) !=
                                sizeof(ophOptionItemRecord_Generic))
                            {
                                log_push_error_point(0x4466);
                                status = S_READFILE;
                                goto obd2tune_apply_change_helper_sth_oph_done;
                            }
#if STH_OPH_ENCRYPTION_ENABLED
                            crypto_blowfish_decryptblock_critical((u8*)originaldata,
                                                                  sizeof(ophOptionItemRecord_Generic));
#else
            //do nothing
#endif
                            if (processed + sizeof(ophOptionItemRecord_Generic) <= option_record_d_ptr->length)
                            {
                                length = sizeof(ophOptionItemRecord_Generic);
                            }
                            else
                            {
                                length = option_record_d_ptr->length - processed;
                            }
                        }
                    }//while(1)...
                }
                else
                {
                    status = filestock_updateflashfile(option_record.address,
                                                       option_record_d_ptr->data,
                                                       option_record_d_ptr->length);
                }
                if (status != S_SUCCESS)
                {
                    log_push_error_point(0x4467);
                    goto obd2tune_apply_change_helper_sth_oph_done;
                }
            }
            //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
            // Direct replace: ophOptionItemRecord_Opcode_D
            //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
            else if (option_record.opcode == 'D')
            {
                ophOptionItemRecord_Opcode_D *option_record_D_ptr;
                u32 total_length;   //include padding

                option_record_D_ptr = (ophOptionItemRecord_Opcode_D*)&option_record;
                if (option_record_D_ptr->zero)
                {
                    log_push_error_point(0x446F);
                    status = S_BADCONTENT;
                    goto obd2tune_apply_change_helper_sth_oph_done;
                }

                total_length = option_record_D_ptr->length % 32;
                if (total_length)
                {
                    total_length = option_record_D_ptr->length + (32-total_length);
                }
                else
                {
                    total_length = option_record_D_ptr->length;
                }

                processed = 0;
                while(processed < total_length)
                {
                    readlength = total_length - processed;
                    if (readlength > sizeof(originaldata))
                    {
                        readlength = sizeof(originaldata);
                    }

                    //raw data of 'D' opcode
                    if (fread(originaldata,1,readlength,optionfptr) != readlength)
                    {
                        log_push_error_point(0x4470);
                        status = S_READFILE;
                        goto obd2tune_apply_change_helper_sth_oph_done;
                    }
#if STH_OPH_ENCRYPTION_ENABLED
                    crypto_blowfish_decryptblock_critical((u8*)originaldata,readlength);
#else
                    //do nothing
#endif

                    //actual data bytecount
                    bytecount = option_record_D_ptr->length - processed;
                    if (bytecount > sizeof(originaldata))
                    {
                        bytecount = sizeof(originaldata);
                    }
                    status = filestock_updateflashfile(option_record.address + processed,
                                                       originaldata,
                                                       bytecount);
                    if (status != S_SUCCESS)
                    {
                        goto obd2tune_apply_change_helper_sth_oph_done;
                    }
                    
                    processed += readlength;
                }//while(...
            }
            //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
            // Calculation: ophOptionItemRecord_Opcode_c
            //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
            else if (option_record.opcode == 'C' || option_record.opcode == 'c')
            {
                ophOptionItemRecord_Opcode_c *option_record_c_ptr;

                option_record_c_ptr = (ophOptionItemRecord_Opcode_c*)&option_record;
                switch(option_record_c_ptr->math_index)
                {
                case 0x12:  //Ford 6.0L truck tire size simple method
                    flasherinfo->options_flags.f.opcode_c_mathindex_12H = 1;
                    flasherinfo->actualtire.front = (u32)option_record_c_ptr->equ_default_fval; 
                    flasherinfo->actualtire.rear = flasherinfo->actualtire.front;   //Note: only rear tire size affects speedometer
                    break;
                case 0x13:  //Ford 6.4L truck tire size simple method
                    flasherinfo->options_flags.f.opcode_c_mathindex_13H = 1;
                    fvalue.fvalue = (1 - option_record_c_ptr->equ_default_fval / option_record_c_ptr->equ_calc_fval_0) * 10000 / 100;
                    fvalue.fvalue = option_record_c_ptr->equ_calc_u32val - (fvalue.fvalue * option_record_c_ptr->equ_calc_u32val * 0.01) * 100 / 100;
                    if (option_record_c_ptr->length == 4 &&
                        option_record_c_ptr->flags & ECU_Data_BigEndian)
                    {
                        //it's always 4 bytes & BIG endian
                        newdata[0] = fvalue.hex[3];
                        newdata[1] = fvalue.hex[2];
                        newdata[2] = fvalue.hex[1];
                        newdata[3] = fvalue.hex[0];
                        status = filestock_updateflashfile(option_record_c_ptr->address,
                                                           newdata,option_record_c_ptr->length);
                    }
                    else
                    {
                        log_push_error_point(0x4561);
                        return S_BADCONTENT;
                    }
                    break;
                //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
                case 0x14:  //Ford Tire Size Method 1 - 6.7L, 6.2L, 5.0L Trucks
                    flasherinfo->options_flags.f.opcode_c_mathindex_14H = 1;
                    flasherinfo->actualtire.front = (u32)(option_record_c_ptr->equ_default_fval*77.18);
                    flasherinfo->actualtire.rear = flasherinfo->actualtire.front;
                    break;
                case 0x15:  //Ford Axle Ratio Method 1 - 6.7L, 6.2L, 5.0L Trucks
                    flasherinfo->options_flags.f.opcode_c_mathindex_15H = 1;
                    flasherinfo->actualaxleratio = (u32)(option_record_c_ptr->equ_default_fval*100);
                    break;
                case 0x16:  //Ford TPMS Front Tire Method 1 - 6.7L, 6.2L, 5.0L, 3.5L Ecoboost trucks
                    flasherinfo->options_flags.f.opcode_c_mathindex_16H = 1;
                    flasherinfo->actualtpms.front = (u8)(option_record_c_ptr->equ_default_fval);
                    break;
                case 0x17:  //Ford TPMS Rear Tire Method 1 - 6.7L, 6.2L, 5.0L, 3.5L Ecoboost trucks
                    flasherinfo->options_flags.f.opcode_c_mathindex_17H = 1;
                    flasherinfo->actualtpms.rear = (u8)(option_record_c_ptr->equ_default_fval);
                    break;
                //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
                case 0x10:  //Ford 6.0L truck tire size
                    flasherinfo->options_flags.f.opcode_c_mathindex_10H = 1;
                case 0x11:  //Ford 6.4L truck tire size
                    flasherinfo->options_flags.f.opcode_c_mathindex_11H = 1;
                default:
                    log_push_error_point(0x4562);
                    return S_NOTSUPPORT;
                }
            }
            //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
            // Others
            //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
            else
            {
                option_record.address += starting_offset;
                status = filestock_set_position(option_record.address);
                if (status != S_SUCCESS)
                {
                    log_push_error_point(0x4463);
                    goto obd2tune_apply_change_helper_sth_oph_done;
                }
                
                status = filestock_read(originaldata,option_record.length,&bytecount);
                if (status != S_SUCCESS || bytecount != option_record.length)
                {
                    log_push_error_point(0x4464);
                    status = S_READFILE;
                    goto obd2tune_apply_change_helper_sth_oph_done;
                }
                
                if (option_record.opcode == 'R' || option_record.opcode == 'r')
                {
                    fvalue.fvalue = option_record.adjustment;
                }
                else
                {
                    fvalue.uint32 = 0;
                    if (option_record.length == 1)
                    {
                        if (option_record.flags & ECU_Data_Signed)
                        {
                            fvalue.fvalue = (float)((s8)(originaldata[0]));
                        }
                        else
                        {
                            fvalue.fvalue = (float)((u8)(originaldata[0]));
                        }
                    }
                    else if (option_record.length == 2)
                    {
                        if (option_record.flags & ECU_Data_BigEndian)
                        {
                            fvalue.hex[1] = originaldata[0];
                            fvalue.hex[0] = originaldata[1];
                        }
                        else
                        {
                            fvalue.hex[1] = originaldata[1];
                            fvalue.hex[0] = originaldata[0];
                        }
                        
                        if (option_record.flags & ECU_Data_Signed)
                        {
                            fvalue.fvalue = (float)((s16)(fvalue.uint32));
                        }
                        else
                        {
                            fvalue.fvalue = (float)((u16)(fvalue.uint32));
                        }
                    }
                    else if (option_record.length == 4)
                    {
                        float value;
                        
                        if (option_record.flags & ECU_Data_BigEndian)
                        {
                            fvalue.hex[3] = originaldata[0];
                            fvalue.hex[2] = originaldata[1];
                            fvalue.hex[1] = originaldata[2];
                            fvalue.hex[0] = originaldata[3];
                        }
                        else
                        {
                            fvalue.hex[3] = originaldata[3];
                            fvalue.hex[2] = originaldata[2];
                            fvalue.hex[1] = originaldata[1];
                            fvalue.hex[0] = originaldata[0];
                        }
                        
                        if ((option_record.flags & ECU_DataType_Mask) == ECU_DataType_Int)
                        {
                            value = (float)((u32)(fvalue.uint32));
                            if (option_record.flags & ECU_Data_Signed)
                            {
                                value = (float)((s32)(fvalue.uint32));
                            }
                        }
                        else    //ECU_DataType_Float
                        {
                            value = fvalue.fvalue;
                        }
                        fvalue.fvalue = value;
                    }
                    else
                    {
                        log_push_error_point(0x4468);
                        return S_BADCONTENT;
                    }
                    
                    fvalue.fvalue -= option_record.offset;
                    fvalue.fvalue *= option_record.multiplier;     
                    
                    if (option_record.opcode == 'A' || option_record.opcode == 'a')
                    {
                        fvalue.fvalue += option_record.adjustment;
                    }
                    else
                    {
                        fvalue.fvalue *= option_record.adjustment;
                    }
                    
                    if (fvalue.fvalue < option_record.min)
                    {
                        fvalue.fvalue = option_record.min;
                    }
                    else if (fvalue.fvalue > option_record.max)
                    {
                        fvalue.fvalue = option_record.max;
                    }
                }
                
                fvalue.fvalue /= option_record.multiplier;  //TODOQK: failed if multiplier is ZERO
                fvalue.fvalue += option_record.offset;
                
                if (option_record.length == 1)
                {
                    //TODOQ: from old code, does it really want to cast it like that !?!
                    if (option_record.flags & ECU_Data_Signed)
                    {
                        fvalue.sint32 = (s32)fvalue.fvalue;
                    }
                    else
                    {
                        fvalue.uint32 = (u32)fvalue.fvalue;
                    }
                    newdata[0] = fvalue.hex[0];
                }
                else if (option_record.length == 2)
                {
                    //TODOQ: from old code, does it really want to cast it like that !?!
                    if (option_record.flags & ECU_Data_Signed)
                    {
                        fvalue.sint32 = (s32)fvalue.fvalue;
                    }
                    else
                    {
                        fvalue.uint32 = (u32)fvalue.fvalue;
                    }
                    if (option_record.flags & ECU_Data_BigEndian)
                    {
                        newdata[0] = fvalue.hex[1];
                        newdata[1] = fvalue.hex[0];
                    }
                    else
                    {
                        newdata[0] = fvalue.hex[0];
                        newdata[1] = fvalue.hex[1];
                    }
                }
                else if (option_record.length == 4)
                {
                    if ((option_record.flags & ECU_DataType_Mask) == ECU_DataType_Int)
                    {
                        if (option_record.flags & ECU_Data_Signed)
                        {
                            s32 value;
                            
                            value = (s32)(fvalue.fvalue);
                            fvalue.sint32 = value;
                        }
                        else
                        {
                            u32 value;
                            
                            value = (u32)(fvalue.fvalue);
                            fvalue.uint32 = value;
                        }
                    }
                    else    //ECU_DataType_Float
                    {
                        //do nothing
                    }
                    
                    if (option_record.flags & ECU_Data_BigEndian)
                    {
                        newdata[0] = fvalue.hex[3];
                        newdata[1] = fvalue.hex[2];
                        newdata[2] = fvalue.hex[1];
                        newdata[3] = fvalue.hex[0];
                    }
                    else
                    {
                        newdata[0] = fvalue.hex[0];
                        newdata[1] = fvalue.hex[1];
                        newdata[2] = fvalue.hex[2];
                        newdata[3] = fvalue.hex[3];
                    }
                }
                else
                {
                    log_push_error_point(0x4469);
                    return S_BADCONTENT;
                }
                
                status = filestock_updateflashfile(option_record.address,
                                                   newdata,option_record.length);
                if (status != S_SUCCESS)
                {
                    log_push_error_point(0x446A);
                    goto obd2tune_apply_change_helper_sth_oph_done;
                }
            }
            //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
            //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
        }//while(1)...
    }//if(offset_4_changes...
    
obd2tune_apply_change_helper_sth_oph_done:
    return status;
}

//------------------------------------------------------------------------------
// This routine makes the AM strategy changes in the copy of the stock file
// preloaded tune file Format:
//      Header Indentifer       = 1 byte    (char)  = 0x04
//      Header Length           = 4 bytes   (int)
//      ECM type                = 1 byte    (char)
//      strategy name           = 10 bytes  (char)
//      Tune description        = 20 bytes  (char)
//      Option file name        = 20 bytes  (char)
//      Data                    = variable
// Inputs:  u32 offset (offset to correct position in flash file where this
//                      tune file can apply changes)
//          u8  *tunefilename
// Return:  u8  status
// Engineer: Rhonda Burns, Quyen Leba
// Date: 6/15/06
//------------------------------------------------------------------------------
u8 obd2tune_apply_preloadedtune_stg(u32 offset, u8 *tunefilename)
{
    F_FILE  *sfptr;
    u8  change_block_data[27];
    u8  status;

    sfptr = NULL;
    status = S_SUCCESS;
        
    sfptr = genfs_default_openfile(tunefilename,"rb");
    if (sfptr == NULL)
    {
        log_push_error_point(0x4480);
        status = S_OPENFILE;
        goto obd2tune_apply_preloadedtune_stg_done;
    }
    
    //skip the header
    if (fseek(sfptr,56,SEEK_SET) != 0)
    {
        log_push_error_point(0x4481);
        status = S_SEEKFILE;
        goto obd2tune_apply_preloadedtune_stg_done;
    }
    
    while (!feof(sfptr))
    {
        //each change is 27-byte block
        if (fread(change_block_data,1,27,sfptr) != 27)
        {
            log_push_error_point(0x4482);
            status = S_READFILE;
            goto obd2tune_apply_preloadedtune_stg_done;
        }
        
        status = obd2tune_apply_change_helper_stg_opg(change_block_data,offset);
        if (status != S_SUCCESS)
        {
            log_push_error_point(0x4483);
            goto obd2tune_apply_preloadedtune_stg_done;
        }
    }//while (!feof(sfptr))...
    
obd2tune_apply_preloadedtune_stg_done:
    if (sfptr)
    {
        genfs_closefile(sfptr);
    }
    return status;
}

//------------------------------------------------------------------------------
// Apply preloaded tune changes in sth file format
// Inputs:  u32 offset (offset to correct position in flash file where this
//                      tune file can apply changes)
//          u8  *tunefilename
// Return:  u8  status
// Engineer: Quyen Leba
//------------------------------------------------------------------------------
u8 obd2tune_apply_preloadedtune_sth(u32 offset, u8 *tunefilename)
{
    F_FILE  *sfptr;
    sthFileHeader sth_header;
    u32 sth_header_length;
    u8  status;

    sfptr = NULL;
    status = S_SUCCESS;

    sfptr = genfs_general_openfile(tunefilename,"rb");
    if (sfptr == NULL)
    {
        log_push_error_point(0x4490);
        status = S_OPENFILE;
        goto obd2tune_apply_preloadedtune_sth_done;
    }

    //skip the header
    status = obd2tune_read_sth_header(sfptr,&sth_header,&sth_header_length);
    if (status != S_SUCCESS)
    {
        log_push_error_point(0x4492);
        status = S_SEEKFILE;
        goto obd2tune_apply_preloadedtune_sth_done;
    }
    
    status = obd2tune_apply_change_helper_sth_oph
        (sfptr, offset, sth_header_length);
    if (status != S_SUCCESS)
    {
        log_push_error_point(0x4493);
        goto obd2tune_apply_preloadedtune_sth_done;
    }
    
obd2tune_apply_preloadedtune_sth_done:
    if (sfptr)
    {
        genfs_closefile(sfptr);
    }
    return status;
}

//------------------------------------------------------------------------------
// Apply preloaded tune changes to flash file
// Inputs:  u16 veh_type
//          selected_tunelist_info *tuneinfo
// Return:  u8  status
// Engineer: Quyen Leba
// Note: this function support stf and stg
//------------------------------------------------------------------------------
u8 obd2tune_apply_preloadedtune(u16 veh_type,
                                selected_tunelist_info *tuneinfo)
{
    u32 ecm_bytecount;
    u16 ecm_type;
    u32 offset;
    u8  buffer[64];
    u32 filenamelength;
    u8  i;
    u8  status;

    status = S_SUCCESS;

    status = filestock_openflashfile("r+");
    if (status != S_SUCCESS)
    {
        log_push_error_point(0x44A0);
        goto obd2tune_apply_preloadedtune_done;
    }
    
    offset = 0;
    for(i=0;i<ECM_MAX_COUNT;i++)
    {
        if (tuneinfo->tunefilename[i][0] == NULL)
        {
            //TODOQ: use tune count to break
            break;
        }
        
        ecm_type = VEH_GetEcmType(veh_type,i);
        if (ecm_type == INVALID_ECM_DEF)
        {
            log_push_error_point(0x44A1);
            status = S_BADCONTENT;
            goto obd2tune_apply_preloadedtune_done;
        }
        
        //check for tune file type; for now, there're stf and stg
        filenamelength = strlen((char*)tuneinfo->tunefilename[i]);
        if (filenamelength < 4)
        {
            log_push_error_point(0x44A2);
            status = S_BADCONTENT;
            goto obd2tune_apply_preloadedtune_done;
        }
        strcpy((char*)buffer,
               (char*)&tuneinfo->tunefilename[i][filenamelength-3]);
        if (strcmp((char*)buffer,"stf") == 0)
        {
            status = obd2tune_apply_preloadedtune_stf
                (offset, tuneinfo->tunefilename[i]);
        }
        else if (strcmp((char*)buffer,"stg") == 0)
        {
            status = obd2tune_apply_preloadedtune_stg
                (offset, tuneinfo->tunefilename[i]);
        }
        else if (strcmp((char*)buffer,"sth") == 0)
        {
            status = obd2tune_apply_preloadedtune_sth
                (offset, tuneinfo->tunefilename[i]);
        }
        else
        {
            log_push_error_point(0x44A3);
            status = S_NOTSUPPORT;
            goto obd2tune_apply_preloadedtune_done;
        }
        
        if (status != S_SUCCESS)
        {
            log_push_error_point(0x44A4);
            goto obd2tune_apply_preloadedtune_done;
        }
        
        status = obd2tune_getecmsize_byecmtype(ecm_type, &ecm_bytecount);
        if (status != S_SUCCESS)
        {
            log_push_error_point(0x44A5);
            goto obd2tune_apply_preloadedtune_done;
        }
        offset += ecm_bytecount;
    }

obd2tune_apply_preloadedtune_done:
    filestock_closeflashfile();
    return status;
}

//------------------------------------------------------------------------------
// This routine takes the information stored in the newAdjst.opt file
// on the card and sends it to Get_AdjustPoint_Data to make the neccessary changes
// The newAdjst.opt file is created in RunOptionsMenu
// Inputs:  u32 offset (starting point of this option file in the flash file)
//          u8  *optionfilename (can have either default or user path)
//          u8  *changed_options (UNUSED)
//          u32 *offset_4_changes
// Return:  u8  status
// Engineer: Rhonda Burns, Quyen Leba
// Date: 7/24/06
//------------------------------------------------------------------------------
u8 obd2tune_apply_options_opt(u32 offset, u8 *optionfilename,
                              u8 *changed_options, u32 *offset_4_changes)
{
    F_FILE *optionfptr;
    u8  buffer[4];
    u8  adjust_point_data[256];
    struct Option_Header_struct
    {
        u8  OH_Identifier;
        u32 Header_length;
        u8  Num_of_Options;
    }Option_Header;
    u32 i;
    u8  seqnum;
    u8  seqcomp;
    u8  numofdatabytes;
    u32 address;
    u8  status;

    optionfptr = NULL;
    memset(adjust_point_data,0,sizeof(adjust_point_data));
    status = S_SUCCESS;
    
    optionfptr = genfs_general_openfile(optionfilename,"rb");
    if (optionfptr == NULL)
    {
        log_push_error_point(0x44B0);
        status = S_OPENFILE;
        goto obd2tune_apply_options_opt_done;
    }
    
    //##########################################################################
    // Get Option Header
    //##########################################################################
    //Get file identifier
    if(fread(&Option_Header.OH_Identifier,
              1,sizeof(Option_Header.OH_Identifier),
              optionfptr) != sizeof(Option_Header.OH_Identifier))
    {
        log_push_error_point(0x44B1);
        status = S_READFILE;
        goto obd2tune_apply_options_opt_done;
    }

    //Get header length and put in correct order
    if(fread((char*)buffer,1,4,optionfptr) != 4)
    {
        log_push_error_point(0x44B2);
        status = S_READFILE;
        goto obd2tune_apply_options_opt_done;
    }

    Option_Header.Header_length = buffer[0];
    Option_Header.Header_length = ((Option_Header.Header_length<<8)|buffer[1]);
    Option_Header.Header_length = ((Option_Header.Header_length<<8)|buffer[2]);
    Option_Header.Header_length = ((Option_Header.Header_length<<8)|buffer[3]);

    //number of available options
    if(fread(&Option_Header.Num_of_Options,
              1,sizeof(Option_Header.Num_of_Options),
              optionfptr) != sizeof(Option_Header.Num_of_Options))
    {
        log_push_error_point(0x44B3);
        status = S_READFILE;
        goto obd2tune_apply_options_opt_done;
    }

    //##########################################################################
    //
    //##########################################################################
    seqcomp = 0;
    for(i=0;i<Option_Header.Num_of_Options;i++)
    {
        if(offset_4_changes[i] != 0xFFFFFFFF) // Has a new adjustment point been selected
        {
            //move to correct place in file
            if(fseek(optionfptr,offset_4_changes[i], SEEK_SET) != 0)
            {
                log_push_error_point(0x44B4);
                status = S_SEEKFILE;
                goto obd2tune_apply_options_opt_done;
            }

            numofdatabytes = 1;     // Do at least once

            while(numofdatabytes)
            {
                //get sequence number of data block
                if ((numofdatabytes = fread((&seqnum),1,1,optionfptr)) != 1)
                {
                    log_push_error_point(0x44B5);
                    status = S_READFILE;
                    goto obd2tune_apply_options_opt_done;
                }

                if(seqnum != seqcomp)
                {
                    log_push_error_point(0x44B6);
                    status = S_BADCONTENT;
                    goto obd2tune_apply_options_opt_done;
                }
                seqcomp++;

                //Get address of data in binary image
                if (fread(buffer,1,4,optionfptr) != 4)
                {
                    log_push_error_point(0x44B7);
                    status = S_READFILE;
                    goto obd2tune_apply_options_opt_done;
                }

                address = buffer[0];    //put in correct format
                address = ((address<<8)|buffer[1]);
                address = ((address<<8)|buffer[2]);
                address = ((address<<8)|buffer[3]);

                //get number of data bytes to change in binary
                if(fread(&numofdatabytes,1,1,optionfptr) != 1)
                {
                    log_push_error_point(0x44B8);
                    status = S_READFILE;
                    goto obd2tune_apply_options_opt_done;
                }

                if (numofdatabytes == 0)
                {
                    seqcomp = 0;
                    break;
                }
                //get Data bytes
                if(fread(adjust_point_data,
                          1,numofdatabytes, optionfptr) != numofdatabytes)
                {
                    log_push_error_point(0x44B9);
                    status = S_READFILE;
                    goto obd2tune_apply_options_opt_done;
                }

                status = filestock_updateflashfile(address+offset,
                                                   adjust_point_data,
                                                   numofdatabytes);
                if(status != S_SUCCESS)
                {
                    log_push_error_point(0x44BA);
                    status = S_FAIL;
                    goto obd2tune_apply_options_opt_done;
                }
            }//while(numofdatabytes)...
        }//if(offset_4_changes[i]...
    }//for(i=0;...

obd2tune_apply_options_opt_done:
    if (optionfptr)
    {
        genfs_closefile(optionfptr);
    }
    return status;
}

//------------------------------------------------------------------------------
// This routine takes the information stored in the newAdjst.opt file
// on the card and sends it to Get_AdjustPoint_Data to make the neccessary changes
// The newAdjst.opt file is created in RunOptionsMenu
// Inputs:  u32 offset (starting point of this option file in the flash file)
//          u8  *optionfilename (can have either default or user path)
//          u8  *changed_options (UNUSED)
//          u32 *offset_4_changes
// Return:  u8  status
// Engineer: Rhonda Burns, Quyen Leba
// Date: 7/24/06
//------------------------------------------------------------------------------
u8 obd2tune_apply_options_opg(u32 offset, u8 *optionfilename,
                              u8 *changed_options, u32 *offset_4_changes)
{
    F_FILE *optionfptr;
    u8  buffer[4];
    u8  adjust_point_data[256];
    u8  change_block_data[27];
    struct Option_Header_struct
    {
        u8  OH_Identifier;
        u32 Header_length;
        u8  Num_of_Options;
    }Option_Header;
    u32 i;
    u8  status;

    optionfptr = NULL;
    memset(adjust_point_data,0,sizeof(adjust_point_data));
    status = S_SUCCESS;
    
    optionfptr = genfs_general_openfile(optionfilename,"rb");
    if (optionfptr == NULL)
    {
        log_push_error_point(0x44C0);
        status = S_OPENFILE;
        goto obd2tune_apply_options_opg_done;
    }
    
    //##########################################################################
    // Get Option Header
    //##########################################################################
    //Get file identifier
    if(fread(&Option_Header.OH_Identifier,
              1,sizeof(Option_Header.OH_Identifier),
              optionfptr) != sizeof(Option_Header.OH_Identifier))
    {
        log_push_error_point(0x44C1);
        status = S_READFILE;
        goto obd2tune_apply_options_opg_done;
    }

    //Get header length and put in correct order
    if(fread((char*)buffer,1,4,optionfptr) != 4)
    {
        log_push_error_point(0x44C2);
        status = S_READFILE;
        goto obd2tune_apply_options_opg_done;
    }

    Option_Header.Header_length = buffer[0];
    Option_Header.Header_length = ((Option_Header.Header_length<<8)|buffer[1]);
    Option_Header.Header_length = ((Option_Header.Header_length<<8)|buffer[2]);
    Option_Header.Header_length = ((Option_Header.Header_length<<8)|buffer[3]);

    //number of available options
    if(fread(&Option_Header.Num_of_Options,
              1,sizeof(Option_Header.Num_of_Options),
              optionfptr) != sizeof(Option_Header.Num_of_Options))
    {
        log_push_error_point(0x44C3);
        status = S_READFILE;
        goto obd2tune_apply_options_opg_done;
    }

    //##########################################################################
    //
    //##########################################################################
    
    for(i=0;i<Option_Header.Num_of_Options;i++)
    {
        if(offset_4_changes[i] != 0xFFFFFFFF) // Has a new adjustment point been selected
        {
            //move to correct place in file
            if(fseek(optionfptr,offset_4_changes[i], SEEK_SET) != 0)
            {
                log_push_error_point(0x44C4);
                status = S_SEEKFILE;
                goto obd2tune_apply_options_opg_done;
            }

            while(1)
            {
                if (fread(change_block_data,1,27,optionfptr) != 27)
                {
                    log_push_error_point(0x44C5);
                    status = S_READFILE;
                    goto obd2tune_apply_options_opg_done;
                }
                
                //TODOQ: shouldn't do it like this
                if (change_block_data[0] == 'E')
                {
                    //termination found
                    break;
                }
                
                status = obd2tune_apply_change_helper_stg_opg(change_block_data,
                                                              offset);
                if (status != S_SUCCESS)
                {
                    log_push_error_point(0x44C6);
                    goto obd2tune_apply_options_opg_done;
                }
            }//while(1)...
        }//if(offset_4_changes[i]...
    }//for(i=0;...

obd2tune_apply_options_opg_done:
    if (optionfptr)
    {
        genfs_closefile(optionfptr);
    }
    return status;
}

//------------------------------------------------------------------------------
// Inputs:  u32 offset
//          u8  *optionfilename (can have either default or user path)
//          u8  *changed_options
//          u32 *offset_4_changes
// Return   u8  status
// Engineer: Quyen Leba
//------------------------------------------------------------------------------
u8 obd2tune_apply_options_oph(u32 offset, u8 *optionfilename,
                              u8 *changed_options, u32 *offset_4_changes)
{
    F_FILE *optionfptr;
    ophOptionFileHeader option_header;
    u32 i;
    u8  status;

    optionfptr = NULL;
    status = S_SUCCESS;

    optionfptr = genfs_general_openfile(optionfilename,"rb");
    if (optionfptr == NULL)
    {
        log_push_error_point(0x44D0);
        status = S_OPENFILE;
        goto obd2tune_apply_options_oph_done;
    }

    //##########################################################################
    // Get Option Header
    //##########################################################################
    //Get file identifier
    if(fread(&option_header,1,sizeof(option_header),optionfptr) !=
       sizeof(option_header))
    {
        log_push_error_point(0x44D1);
        status = S_READFILE;
        goto obd2tune_apply_options_oph_done;
    }
    status = obd2tune_validate_oph_header(&option_header);
    if (status != S_SUCCESS)
    {
        log_push_error_point(0x44EB);
        goto obd2tune_apply_options_oph_done;
    }

    //##########################################################################
    //
    //##########################################################################
    
    for(i=0;i<option_header.option_count;i++)
    {
        status = obd2tune_apply_change_helper_sth_oph(optionfptr,offset,
                                                      offset_4_changes[i]);
        if (status != S_SUCCESS)
        {
            log_push_error_point(0x44D2);
            goto obd2tune_apply_options_oph_done;
        }
    }//for(i=0;...

obd2tune_apply_options_oph_done:
    if (optionfptr)
    {
        genfs_closefile(optionfptr);
    }
    return status;
}

//------------------------------------------------------------------------------
// Inputs:  u32 offset
//          u8  *optionfilename
//          option_selection_sof *option_selection
// Return   u8  status
// Engineer: Quyen Leba
//------------------------------------------------------------------------------
u8 obd2tune_apply_options_oph_use_sof(u32 offset, u8 *optionfilename,
                                      option_selection_sof *option_selection)
{
    F_FILE *optionfptr;
    ophOptionFileHeader option_header;
    u32 i;
    u8  status;

    optionfptr = NULL;
    status = S_SUCCESS;

    optionfptr = genfs_general_openfile(optionfilename,"rb");   //TODOQ: ~~~ check again
    if (optionfptr == NULL)
    {
        log_push_error_point(0x44F6);
        status = S_OPENFILE;
        goto obd2tune_apply_options_oph_use_sof_done;
    }

    //##########################################################################
    // Get Option Header
    //##########################################################################
    //Get file identifier
    if(fread(&option_header,1,sizeof(option_header),optionfptr) !=
       sizeof(option_header))
    {
        log_push_error_point(0x44F7);
        status = S_READFILE;
        goto obd2tune_apply_options_oph_use_sof_done;
    }
    status = obd2tune_validate_oph_header(&option_header);
    if (status != S_SUCCESS)
    {
        log_push_error_point(0x44FB);
        goto obd2tune_apply_options_oph_use_sof_done;
    }

    //##########################################################################
    //
    //##########################################################################
    
    for(i=0;i<option_header.option_count;i++)
    {
        //TODOQ: fully process option_selection (i.e. flags, etc)
        status = obd2tune_apply_change_helper_sth_oph(optionfptr,offset,
                                                      option_selection[i].file_offsets);
        if (status != S_SUCCESS)
        {
            log_push_error_point(0x44F8);
            goto obd2tune_apply_options_oph_use_sof_done;
        }
    }//for(i=0;...

obd2tune_apply_options_oph_use_sof_done:
    if (optionfptr)
    {
        genfs_closefile(optionfptr);
    }
    return status;
}

//------------------------------------------------------------------------------
// Apply option changes to flash file
// Inputs:  u8  flash_type
//          u16 veh_type
//          selected_tunelist_info *tuneinfo
// Return:  u8  status
// Engineer: Quyen Leba
//------------------------------------------------------------------------------
u8 obd2tune_apply_options(u8 flash_type, u16 veh_type,
                          selected_tunelist_info *tuneinfo)
{
    u8  status;

    //TODOQK: get rid of obd2tune_apply_options_use_opt
#if USE_OPTION_HANDLING_SOF
    status = obd2tune_apply_options_use_sof(flash_type,veh_type,OptionSoure_RegularOption,tuneinfo);
#elif USE_OPTION_HANDLING_OPT
    status = obd2tune_apply_options_use_opt(flash_type,veh_type,OptionSoure_RegularOption,tuneinfo);
#else
    return S_NOTSUPPORT;
#endif

    if (status == S_SUCCESS)
    {
        //only use .sof
        status = obd2tune_apply_options_use_sof(flash_type,veh_type,OptionSoure_SpecialOption,tuneinfo);
    }
    return status;
}

//------------------------------------------------------------------------------
// Apply option changes to flash file using SavedOpts_ECUx.opt or .sof method
// Inputs:  u8 flash_type
//          u16 veh_type
//          OptionSource optionsource
//          selected_tunelist_info *tuneinfo
// Return:  u8  status
// Engineer: Quyen Leba
//------------------------------------------------------------------------------
u8 obd2tune_apply_options_use_opt(u8 flash_type, u16 veh_type, OptionSource optionsource,
                                  selected_tunelist_info *tuneinfo)
{
    F_FILE  *changed_fptr;
    u8  changed_options[50];    //do not change size
    u32 offset_4_changes[50];   //do not change size
    u32 ecm_bytecount;
    u16 ecm_type;
    u32 offset;
    u8  buffer[64];
    u32 filenamelength;
    u8  i;
    bool useoption;
    u8  status;

    status = S_SUCCESS;
    changed_fptr = NULL;

    status = filestock_openflashfile("r+");
    if (status != S_SUCCESS)
    {
        log_push_error_point(0x44E0);
        goto obd2tune_apply_options_done;
    }
    
    offset = 0;
    for(i=0;i<ECM_MAX_COUNT;i++)
    {
        if (tuneinfo->tunefilename[i][0] == NULL)
        {
            //TODOQ: use tune count to break
            break;
        }
        
        ecm_type = VEH_GetEcmType(veh_type,i);
        if (ecm_type == INVALID_ECM_DEF)
        {
            log_push_error_point(0x44E1);
            status = S_BADCONTENT;
            goto obd2tune_apply_options_done;
        }

        useoption = FALSE;
        if (optionsource == OptionSoure_SpecialOption &&
            (tuneinfo->useoption[i] & TUNEUSEOPTION_SPECIAL) == TUNEUSEOPTION_SPECIAL)
        {
            useoption = TRUE;
        }
        else if (optionsource == OptionSoure_RegularOption &&
                 (tuneinfo->useoption[i] & TUNEUSEOPTION_REGULAR) == TUNEUSEOPTION_REGULAR)
        {
            useoption = TRUE;
        }

        if (useoption)
        {
            //user selected options for this ecm
            if (flash_type == CMDIF_ACT_PRELOADED_FLASHER)
            {
                //##################################################################
                // Get data from SavedOpts_ECUx.opt (where x:0,1)
                //##################################################################
                if (optionsource == OptionSoure_SpecialOption)
                {
                    strcpy((char*)buffer,"SavedSpOpts_ECU0.opt");
                    buffer[15] = (u8)('0' + i);
                }
                else    //assume OptionSoure_RegularOption
                {
                    strcpy((char*)buffer,"SavedOpts_ECU0.opt");
                    buffer[13] = (u8)('0' + i);
                }
            }
            else if (flash_type == CMDIF_ACT_CUSTOM_FLASHER)
            {
                if (optionsource == OptionSoure_SpecialOption)
                {
                    strcpy((char*)buffer,"SavedSpOptsCustom.opt");
                }
                else    //assume OptionSoure_RegularOption
                {
                    strcpy((char*)buffer,"SavedOptsCustom.opt");
                }
            }
            else
            {
                log_push_error_point(0x45BB);
                status = S_ERROR;
                goto obd2tune_apply_options_done;
            }
            
            if (changed_fptr)
            {
                genfs_closefile(changed_fptr);
                changed_fptr = NULL;
            }
            
            changed_fptr = genfs_user_openfile(buffer,"rb");
            if (changed_fptr == NULL)
            {
                log_push_error_point(0x44E2);
                status = S_OPENFILE;
                goto obd2tune_apply_options_done;
            }
            
            if (fseek(changed_fptr,20,F_SEEK_SET) != 0)
            {
                log_push_error_point(0x44E3);
                status = S_SEEKFILE;
                goto obd2tune_apply_options_done;
            }
            
            if(fread(changed_options,
                     1,sizeof(changed_options),
                     changed_fptr) != sizeof(changed_options))
            {
                log_push_error_point(0x44E4);
                status = S_READFILE;
                goto obd2tune_apply_options_done;
            }
            //get offsets for selections
            if(fread(offset_4_changes,
                     1,sizeof(offset_4_changes),
                     changed_fptr) != sizeof(offset_4_changes))
            {
                log_push_error_point(0x44E5);
                status = S_READFILE;
                goto obd2tune_apply_options_done;
            }
            
            genfs_closefile(changed_fptr);
            changed_fptr = NULL;
            
            //######################################################################
            //
            //######################################################################
            
            //check for tune file type; for now, there're stf and stg
            //filenamelength = strlen((char*)tuneinfo->tunefilename[i]);
            filenamelength = strlen((char*)tuneinfo->optionfilename[i]);
            if (filenamelength < 4)
            {
                log_push_error_point(0x44E6);
                status = S_BADCONTENT;
                goto obd2tune_apply_options_done;
            }
            
            strcpy((char*)buffer,
                   (char*)&tuneinfo->optionfilename[i][filenamelength-3]);
            if (strcmp((char*)buffer,"opt") == 0)
            {
                status = obd2tune_apply_options_opt
                    (offset, tuneinfo->optionfilename[i],
                     changed_options,offset_4_changes);
            }
            else if (strcmp((char*)buffer,"opg") == 0)
            {
                status = obd2tune_apply_options_opg
                    (offset, tuneinfo->optionfilename[i],
                     changed_options,offset_4_changes);
            }
            else if (strcmp((char*)buffer,"oph") == 0)
            {
                status = obd2tune_apply_options_oph
                    (offset, tuneinfo->optionfilename[i],
                     changed_options,offset_4_changes);
            }
            else
            {
                log_push_error_point(0x44E7);
                status = S_NOTSUPPORT;
                goto obd2tune_apply_options_done;
            }
            
            if (status != S_SUCCESS)
            {
                goto obd2tune_apply_options_done;
            }
        }
        
        status = obd2tune_getecmsize_byecmtype(ecm_type, &ecm_bytecount);
        if (status != S_SUCCESS)
        {
            log_push_error_point(0x44E8);
            goto obd2tune_apply_options_done;
        }
        offset += ecm_bytecount;
    }

obd2tune_apply_options_done:
    filestock_closeflashfile();
    if (changed_fptr)
    {
        genfs_closefile(changed_fptr);
    }
    return status;
}

//------------------------------------------------------------------------------
// Apply option changes to flash file using SavedOpts_ECUx.sof method
// Inputs:  u8  flash_type
//          u16 veh_type
//          OptionSource optionsource
//          selected_tunelist_info *tuneinfo
// Return:  u8  status
// Engineer: Quyen Leba
//------------------------------------------------------------------------------
u8 obd2tune_apply_options_use_sof(u8 flash_type, u16 veh_type, OptionSource optionsource,
                                  selected_tunelist_info *tuneinfo)
{
    struct
    {
        u32 optionfile_crc32e;
        u16 version;
        u8  reserved[26];
        option_selection_sof option_selection[MAX_SAVED_OPTION_COUNT];
    }savedoptions_sof;          //TODOQK: this is large, TODOQK: use malloc
    F_FILE  *changed_fptr;
    u32 ecm_bytecount;
    u16 ecm_type;
    u32 offset;
    u8  buffer[64];
    u8  workingoptionfilename[MAX_TUNE_NAME_LENGTH+1];
    u32 filenamelength;
    u8  i;
    bool useoption;
    u8  status;

    status = S_SUCCESS;
    changed_fptr = NULL;

    status = filestock_openflashfile("r+");
    if (status != S_SUCCESS)
    {
        log_push_error_point(0x44EA);
        goto obd2tune_apply_options_use_sof_done;
    }
    
    offset = 0;
    for(i=0;i<ECM_MAX_COUNT;i++)
    {
        if (tuneinfo->tunefilename[i][0] == NULL)
        {
            //TODOQ: use tune count to break
            break;
        }

        useoption = FALSE;
        if (optionsource == OptionSoure_SpecialOption &&
            (tuneinfo->useoption[i] & TUNEUSEOPTION_SPECIAL) == TUNEUSEOPTION_SPECIAL)
        {
            useoption = TRUE;
        }
        else if (optionsource == OptionSoure_RegularOption &&
                 (tuneinfo->useoption[i] & TUNEUSEOPTION_REGULAR) == TUNEUSEOPTION_REGULAR)
        {
            useoption = TRUE;
        }

        if (!useoption)
        {
            //user selected not to use options for this ecm
            continue;
        }
        
        ecm_type = VEH_GetEcmType(veh_type,i);
        if (ecm_type == INVALID_ECM_DEF)
        {
            log_push_error_point(0x44EC);
            status = S_BADCONTENT;
            goto obd2tune_apply_options_use_sof_done;
        }

        if (flash_type == CMDIF_ACT_PRELOADED_FLASHER)
        {
            //##################################################################
            // Get data from SavedOpts_ECUx.opt (where x:0,1)
            //##################################################################
            if (optionsource == OptionSoure_SpecialOption)
            {
                strcpy((char*)workingoptionfilename,(char*)tuneinfo->specialoptionfilename[i]);
                strcpy((char*)buffer,"SavedSpOpts_ECU0.sof");
                buffer[15] = (u8)('0' + i);
            }
            else    //assume OptionSoure_RegularOption
            {
                strcpy((char*)workingoptionfilename,(char*)tuneinfo->optionfilename[i]);
                strcpy((char*)buffer,"SavedOpts_ECU0.sof");
                buffer[13] = (u8)('0' + i);
            }
        }
        else if (flash_type == CMDIF_ACT_CUSTOM_FLASHER)
        {
            if (optionsource == OptionSoure_SpecialOption)
            {
                strcpy((char*)workingoptionfilename,(char*)tuneinfo->specialoptionfilename[i]);
                strcpy((char*)buffer,"SavedSpOptsCustom.sof");
            }
            else    //assume OptionSoure_RegularOption
            {
                strcpy((char*)workingoptionfilename,(char*)tuneinfo->optionfilename[i]);
                strcpy((char*)buffer,"SavedOptsCustom.sof");
            }
        }
        else
        {
            log_push_error_point(0x45BC);
            status = S_ERROR;
            goto obd2tune_apply_options_use_sof_done;
        }

        if (changed_fptr)
        {
            genfs_closefile(changed_fptr);
            changed_fptr = NULL;
        }
        
        changed_fptr = genfs_user_openfile(buffer,"rb");
        if (changed_fptr == NULL)
        {
            log_push_error_point(0x44ED);
            status = S_OPENFILE;
            goto obd2tune_apply_options_use_sof_done;
        }

        if(fread((char*)&savedoptions_sof,1,32,changed_fptr) != 32)
        {
            log_push_error_point(0x44EF);
            status = S_READFILE;
            goto obd2tune_apply_options_use_sof_done;
        }
        if(fread(&savedoptions_sof.option_selection,
                  1,sizeof(savedoptions_sof.option_selection),
                  changed_fptr) != sizeof(savedoptions_sof.option_selection))
        {
            log_push_error_point(0x44F0);
            status = S_READFILE;
            goto obd2tune_apply_options_use_sof_done;
        }
        
        genfs_closefile(changed_fptr);
        changed_fptr = NULL;
        
        //######################################################################
        //
        //######################################################################
        
        //check for tune file type; for now, there're stf and stg
        filenamelength = strlen((char*)workingoptionfilename);
        if (filenamelength < 4)
        {
            log_push_error_point(0x44F1);
            status = S_BADCONTENT;
            goto obd2tune_apply_options_use_sof_done;
        }
        
        strcpy((char*)buffer,
               (char*)&workingoptionfilename[filenamelength-3]);
        if (strcmp((char*)buffer,"opt") == 0)
        {
            log_push_error_point(0x44F2);
            status = S_NOTSUPPORT;
        }
        else if (strcmp((char*)buffer,"opg") == 0)
        {
            log_push_error_point(0x44F3);
            status = S_NOTSUPPORT;
        }
        else if (strcmp((char*)buffer,"oph") == 0)
        {
            status = obd2tune_apply_options_oph_use_sof
                (offset, workingoptionfilename,
                 savedoptions_sof.option_selection);
        }
        else
        {
            log_push_error_point(0x44F4);
            status = S_NOTSUPPORT;
            goto obd2tune_apply_options_use_sof_done;
        }

        if (status != S_SUCCESS)
        {
            goto obd2tune_apply_options_use_sof_done;
        }
        
        status = obd2tune_getecmsize_byecmtype(ecm_type, &ecm_bytecount);
        if (status != S_SUCCESS)
        {
            log_push_error_point(0x44F5);
            goto obd2tune_apply_options_use_sof_done;
        }
        offset += ecm_bytecount;
    }

obd2tune_apply_options_use_sof_done:
    filestock_closeflashfile();
    if (changed_fptr)
    {
        genfs_closefile(changed_fptr);
    }
    return status;
}

//------------------------------------------------------------------------------
// Read sth header
// Inputs:  F_FILE *sth_fptr
// Outputs: sthFileHeader *header
//          u32 *header_length (NULL allowed)
// Return:  u8  status
// Engineer: Quyen Leba
//------------------------------------------------------------------------------
u8 obd2tune_read_sth_header(F_FILE *sth_fptr,
                            sthFileHeader *header, u32 *header_length)
{
    u32 bytecount;
    u8  *bptr;

    //TODOQ: add EP
    if (header_length)
    {
        *header_length = 0;
    }
    bptr = (u8*)header;
    bytecount = fread(&header->type13,1,sizeof(sthFileHeader_type13),sth_fptr);
    if (bytecount != sizeof(sthFileHeader_type13))
    {
        log_push_error_point(0x4537);
        return S_READFILE;
    }
#if STH_OPH_ENCRYPTION_ENABLED
    crypto_blowfish_decryptblock_critical((u8*)&header->type13,
                                          sizeof(sthFileHeader_type13));
#else
            //do nothing
#endif
    if (header->generic.file_type == CURRENT_PRELOADED_TUNE_FILETYPE_13)
    {
        if (header_length)
        {
            *header_length = sizeof(sthFileHeader_type13);
        }
        return S_SUCCESS;
    }
    else if (header->generic.file_type == CURRENT_PRELOADED_TUNE_FILETYPE_14)
    {
        bptr += sizeof(sthFileHeader_type13);
        bytecount = fread(bptr,
                          1,sizeof(sthFileHeader_type14)-sizeof(sthFileHeader_type13),
                          sth_fptr);
        if (bytecount != (sizeof(sthFileHeader_type14)-sizeof(sthFileHeader_type13)))
        {
            log_push_error_point(0x4538);
            return S_READFILE;
        }
#if STH_OPH_ENCRYPTION_ENABLED
    crypto_blowfish_decryptblock_critical((u8*)bptr,
                                          sizeof(sthFileHeader_type14)-sizeof(sthFileHeader_type13));
#else
            //do nothing
#endif
        if (header_length)
        {
            *header_length = sizeof(sthFileHeader_type14);
        }
        return S_SUCCESS;
    }
    else
    {
        log_push_error_point(0x4539);
        return S_BADCONTENT;
    }
}

//------------------------------------------------------------------------------
// Validate sth header
// Inputs:  sthFileHeader *header
// Return:  u8  status
// Engineer: Quyen Leba
//------------------------------------------------------------------------------
u8 obd2tune_validate_sth_header(sthFileHeader *header)
{
    u32 crc32e;
    u32 header_length;

    switch(header->generic.file_type)
    {
    case CURRENT_PRELOADED_TUNE_FILETYPE_13:
        header_length = sizeof(sthFileHeader_type13);
        break;
    case CURRENT_PRELOADED_TUNE_FILETYPE_14:
        header_length = sizeof(sthFileHeader_type14);
        break;
    default:
        log_push_error_point(0x44FA);
        return S_NOTSUPPORT;
    }
    crc32e_reset();
    crc32e = crc32e_calculateblock(0xFFFFFFFF,
                                   (u32*)header,1);
    crc32e = crc32e_calculateblock(crc32e,
                                   (u32*)&header->generic.content_crc32e,
                                   (header_length-8)/4);    
#if PRELOADED_TUNE_CRC32E_TUNE_CHECK
    if (header->generic.header_crc32e != crc32e)
    {
        log_push_error_point(0x44F9);
        return S_CRC32E;
    }
#endif
    return S_SUCCESS;
}

//------------------------------------------------------------------------------
// Parse optionfilename from sth header
// Input:   sthFileHeader *header
// Output:  u8  *optionfilename
// Engineer: Quyen Leba
//------------------------------------------------------------------------------
void obd2tune_sth_header_parse_optionfilename(sthFileHeader *header,
                                              u8 *optionfilename)
{
    switch(header->generic.file_type)
    {
    case CURRENT_PRELOADED_TUNE_FILETYPE_13:
        strcpy((char*)optionfilename,(char*)header->type13.optionfilename);
        break;
    case CURRENT_PRELOADED_TUNE_FILETYPE_14:
        strcpy((char*)optionfilename,(char*)header->type14.optionfilename);
        break;
    default:
        strcpy((char*)optionfilename,"");
        break;
    }
}

//------------------------------------------------------------------------------
// Parse specialoptionfilename from sth header
// Input:   sthFileHeader *header
// Output:  u8  *specialoptionfilename
// Engineer: Patrick Downs
//------------------------------------------------------------------------------
void obd2tune_sth_header_parse_specialoptionfilename(sthFileHeader *header,
                                              u8 *specialoptionfilename)
{
    switch(header->generic.file_type)
    {
    case CURRENT_PRELOADED_TUNE_FILETYPE_13:
        strcpy((char*)specialoptionfilename,(char*)header->type13.specialoptionfilename);
        break;
    case CURRENT_PRELOADED_TUNE_FILETYPE_14:
        strcpy((char*)specialoptionfilename,(char*)header->type14.specialoptionfilename);
        break;
    default:
        strcpy((char*)specialoptionfilename,"");
        break;
    }
}

//------------------------------------------------------------------------------
// Parse optionfile_headercrc32einfo from sth header
// Input:   sthFileHeader *header
// Return:  u32 optionfile_headercrc32einfo
// Engineer: Quyen Leba
//------------------------------------------------------------------------------
u32 obd2tune_sth_header_parse_optionfile_headercrc32einfo(sthFileHeader *header)
{
    switch(header->generic.file_type)
    {
    case CURRENT_PRELOADED_TUNE_FILETYPE_13:
        return header->type13.optionfile_headercrc32einfo;
    case CURRENT_PRELOADED_TUNE_FILETYPE_14:
        return header->type13.optionfile_headercrc32einfo;
    default:
        return 0;
    }
}

//------------------------------------------------------------------------------
// Validate sth file
// Input:   const u8 *filename
// Output:  sthFileHeader *header (NULL allowed)
// Return:  u8  status
// Engineer: Quyen Leba
//------------------------------------------------------------------------------
u8 obd2tune_validate_sth_file(const u8 *filename,
                              sthFileHeader *header)
{
#define VALIDATE_STH_FILE_BUFFER_SIZE   4096
    sthFileHeader sth_header;
    F_FILE *fptr;
    u8  *buffer;
    u32 bytecount;
    u32 crc32e_calc;
    u8  status;

    fptr = NULL;
    status = S_SUCCESS;

    buffer = __malloc(VALIDATE_STH_FILE_BUFFER_SIZE);
    if (buffer == NULL)
    {
        log_push_error_point(0x44C7);
        status = S_MALLOC;
        goto obd2tune_validate_sth_file_done;
    }

    fptr = genfs_general_openfile((u8*)filename,"r");
    if (fptr == NULL)
    {
        log_push_error_point(0x44C8);
        status = S_OPENFILE;
        goto obd2tune_validate_sth_file_done;
    }
    status = obd2tune_read_sth_header(fptr,&sth_header,NULL);
    if (status != S_SUCCESS)
    {
        log_push_error_point(0x44C9);
        status = S_READFILE;
        goto obd2tune_validate_sth_file_done;
    }

    status = obd2tune_validate_sth_header(&sth_header);
    if (status != S_SUCCESS)
    {
        log_push_error_point(0x44CA);
        status = S_READFILE;
        goto obd2tune_validate_sth_file_done;
    }

    if (header)
    {
        memcpy((char*)header,(char*)&sth_header,sizeof(sthFileHeader));
    }

    crc32e_reset();
    crc32e_calc = 0xFFFFFFFF;
    while(!feof(fptr))
    {
        bytecount = fread((char*)buffer,1,VALIDATE_STH_FILE_BUFFER_SIZE,fptr);
        crc32e_calc = crc32e_calculateblock(crc32e_calc,(u32*)buffer,bytecount/4);    
    }
    if (sth_header.generic.content_crc32e != crc32e_calc)
    {
#if PRELOADED_TUNE_CRC32E_TUNE_CHECK
        log_push_error_point(0x44CB);
        status = S_CRC32E;
        goto obd2tune_validate_sth_file_done;
#endif
    }
    
obd2tune_validate_sth_file_done:
    if (buffer)
    {
        __free(buffer);
    }
    if (fptr)
    {
        genfs_closefile(fptr);
    }
    return status;
}

//------------------------------------------------------------------------------
// Calculate the text portion of an oph file
// Input:   const u8 *optionfilename
// Output:  u32 *crc32e_output
// Return:  u8  status
// Engineer: Quyen Leba
//------------------------------------------------------------------------------
u8 obd2tune_calculate_oph_text_portion_crc32e(const u8 *optionfilename,
                                              u32 *crc32e_output)
{
#define CALCULATE_OPH_TEXT_PORTION_BUFFER_SIZE  4096
    ophOptionFileHeader ophHeader;
    F_FILE *fptr;
    u8  *buffer;
    u32 bytecount;
    u32 crc32e_calc;
    u32 offset;
    u32 bytetoread;
    u8  status;

    fptr = NULL;
    status = S_SUCCESS;
    *crc32e_output = 0;

    buffer = __malloc(CALCULATE_OPH_TEXT_PORTION_BUFFER_SIZE);
    if (buffer == NULL)
    {
        log_push_error_point(0x45DA);
        status = S_MALLOC;
        goto obd2tune_calculate_oph_text_portion_crc32e_done;
    }
    fptr = genfs_general_openfile((u8*)optionfilename,"r");
    if (fptr == NULL)
    {
        log_push_error_point(0x45DB);
        status = S_OPENFILE;
        goto obd2tune_calculate_oph_text_portion_crc32e_done;
    }

    bytecount = fread((char*)&ophHeader,
                      1,sizeof(ophOptionFileHeader),fptr);
    if (bytecount != sizeof(ophOptionFileHeader))
    {
        log_push_error_point(0x45DC);
        status = S_READFILE;
        goto obd2tune_calculate_oph_text_portion_crc32e_done;
    }

    crc32e_reset();
    crc32e_calc = crc32e_calculateblock(0xFFFFFFFF,
                                        (u32*)&ophHeader,sizeof(ophOptionFileHeader)/4);
    offset = sizeof(ophOptionFileHeader);
    while(offset < ophHeader.firstrecord_offset)
    {
        bytetoread = ophHeader.firstrecord_offset - offset;
        if (bytetoread > CALCULATE_OPH_TEXT_PORTION_BUFFER_SIZE)
        {
            bytetoread = CALCULATE_OPH_TEXT_PORTION_BUFFER_SIZE;
        }
        bytecount = fread((char*)buffer,1,bytetoread,fptr);
        if (bytecount != bytetoread)
        {
            log_push_error_point(0x45DD);
            status = S_READFILE;
            goto obd2tune_calculate_oph_text_portion_crc32e_done;
        }
        else
        {
            crc32e_calc = crc32e_calculateblock(crc32e_calc,(u32*)buffer,bytecount/4);
        }
        offset += bytecount;
    }//while(offset...

    *crc32e_output = crc32e_calc;

obd2tune_calculate_oph_text_portion_crc32e_done:
    if (buffer)
    {
        __free(buffer);
    }
    if (fptr)
    {
        genfs_closefile(fptr);
    }
    return status;
}

//------------------------------------------------------------------------------
// Validate oph file header
// Inputs:  ophOptionFileHeader *option_header
// Return:  u8  status
// Engineer: Quyen Leba
//------------------------------------------------------------------------------
u8 obd2tune_validate_oph_header(ophOptionFileHeader *option_header)
{
    u32 crc32e_calc;

    if (!option_header)
    {
        return S_INPUT;
    }
    crc32e_reset();
    crc32e_calc = 0xFFFFFFFF;
    crc32e_calc = crc32e_calculateblock(crc32e_calc,(u32*)option_header,1);
    crc32e_calc = crc32e_calculateblock(crc32e_calc,(u32*)&option_header->content_crc32e,
                                        (sizeof(ophOptionFileHeader)-8)/4);
#if PRELOADED_TUNE_CRC32E_TUNE_CHECK
    if (crc32e_calc != option_header->header_crc32e)
    {
        log_push_error_point(0x44D5);
        return S_CRC32E;
    }
    else if (option_header->version != CURRENT_OPH_VERSION)
    {
        log_push_error_point(0x44D6);
        return S_NOTSUPPORT;
    }
#endif
    return S_SUCCESS;
}

//------------------------------------------------------------------------------
// Validate oph file
// Input:   const u8 *filename (can have either default or user path)
// Return:  u8  status
// Engineer: Quyen Leba
//------------------------------------------------------------------------------
u8 obd2tune_validate_oph_file(const u8 *filename)
{
#define VALIDATE_OPH_FILE_BUFFER_SIZE   4096
    ophOptionFileHeader ophheader;
    F_FILE *fptr;
    u8  *buffer;
    u32 bytecount;
    u32 crc32e_calc;
    u8  status;

    fptr = NULL;
    status = S_SUCCESS;

    buffer = __malloc(VALIDATE_OPH_FILE_BUFFER_SIZE);
    if (buffer == NULL)
    {
        log_push_error_point(0x44CD);
        status = S_MALLOC;
        goto obd2tune_validate_oph_file_done;
    }
    fptr = genfs_general_openfile((u8*)filename,"r");
    if (fptr == NULL)
    {
        log_push_error_point(0x44CE);
        status = S_OPENFILE;
        goto obd2tune_validate_oph_file_done;
    }
    bytecount = fread((char*)&ophheader,1,sizeof(ophOptionFileHeader),fptr);
    if (bytecount != sizeof(ophOptionFileHeader))
    {
        log_push_error_point(0x44CF);
        status = S_READFILE;
        goto obd2tune_validate_oph_file_done;
    }
    status = obd2tune_validate_oph_header(&ophheader);
    if (status != S_SUCCESS)
    {
        log_push_error_point(0x44D3);
        status = S_READFILE;
        goto obd2tune_validate_oph_file_done;
    }
    crc32e_reset();
    crc32e_calc = 0xFFFFFFFF;
    while(!feof(fptr))
    {
        bytecount = fread((char*)buffer,1,VALIDATE_OPH_FILE_BUFFER_SIZE,fptr);
        crc32e_calc = crc32e_calculateblock(crc32e_calc,(u32*)buffer,bytecount/4);    
    }
    if (ophheader.content_crc32e != crc32e_calc)
    {
#if PRELOADED_TUNE_CRC32E_TUNE_CHECK
        log_push_error_point(0x44D4);
        status = S_CRC32E;
        goto obd2tune_validate_oph_file_done;
#endif
    }
    
obd2tune_validate_oph_file_done:
    if (buffer)
    {
        __free(buffer);
    }
    if (fptr)
    {
        genfs_closefile(fptr);
    }
    return status;
}

//------------------------------------------------------------------------------
// Validate CTF (indexfile.ctf) header
// Inputs:  customtune_header *ctf_header          
// Return:  u8  status
// Engineer: Patrick Downs
//------------------------------------------------------------------------------
u8 obd2tune_validate_ctf_header(customtune_header *ctf_header)
{
    u32 crc32e_calc;
    u32 crc32e_cmp;

    if (!ctf_header)
    {
        return S_INPUT;
    }
    crc32e_cmp = ctf_header->header_crc32e;
    ctf_header->header_crc32e = 0xFFFFFFFF;

    crc32e_reset();
    crc32e_calc = 0xFFFFFFFF;
    crc32e_calc = crc32e_calculateblock(crc32e_calc,(u32*)ctf_header,(sizeof(customtune_header)/4));

    if (crc32e_calc != crc32e_cmp)
    {
        log_push_error_point(0x44D7);
        return S_CRC32E;
    }
    else if (ctf_header->version != CURRENT_CTF_VERSION)
    {
        log_push_error_point(0x44D8);
        return S_NOTSUPPORT;
    }
    return S_SUCCESS;
}

//------------------------------------------------------------------------------
// Validate CTF (indexfie.ctf) file
// Input:   const u8 *filename (only user path)
// Return:  u8  status
// Engineer: Patrick Downs
//------------------------------------------------------------------------------
u8 obd2tune_validate_ctf_file(const u8 *filename)
{
#define VALIDATE_CTF_FILE_BUFFER_SIZE   4096
    customtune_header ctfheader;
    F_FILE *fptr;
    u8  *buffer;
    u32 bytecount;
    u32 crc32e_calc;
    u8  status;

    fptr = NULL;
    status = S_SUCCESS;

    buffer = __malloc(VALIDATE_CTF_FILE_BUFFER_SIZE);
    if (buffer == NULL)
    {
        log_push_error_point(0x44D9);
        status = S_MALLOC;
        goto obd2tune_validate_ctf_file_done;
    }
    fptr = genfs_user_openfile((u8*)filename,"r");
    if (fptr == NULL)
    {
        log_push_error_point(0x44DA);
        status = S_OPENFILE;
        goto obd2tune_validate_ctf_file_done;
    }
    bytecount = fread((char*)&ctfheader,1,sizeof(ctfheader),fptr);
    if (bytecount != sizeof(customtune_header))
    {
        log_push_error_point(0x44DB);
        status = S_READFILE;
        goto obd2tune_validate_ctf_file_done;
    }
    crypto_blowfish_decryptblock_external_key((u8*)&ctfheader,sizeof(customtune_header));
    status = obd2tune_validate_ctf_header(&ctfheader);
    if (status != S_SUCCESS)
    {
        log_push_error_point(0x44DC);
        status = S_READFILE;
        goto obd2tune_validate_ctf_file_done;
    }
    crc32e_reset();
    crc32e_calc = 0xFFFFFFFF;
    while(!feof(fptr))
    {
        bytecount = fread((char*)buffer,1,VALIDATE_CTF_FILE_BUFFER_SIZE,fptr);
        crc32e_calc = crc32e_calculateblock(crc32e_calc,(u32*)buffer,bytecount/4);    
    }
    if (ctfheader.content_crc32e != crc32e_calc)
    {
        log_push_error_point(0x44DD);
        status = S_CRC32E;
        goto obd2tune_validate_ctf_file_done;
    }
    
obd2tune_validate_ctf_file_done:
    if (buffer)
    {
        __free(buffer);
    }
    if (fptr)
    {
        genfs_closefile(fptr);
    }
    return status;
}

//------------------------------------------------------------------------------
// Translate ECM physical address to it's ECM stock area offset
// The offset doesn't start from the beginning of the file but from where
// this ECM starts in the file
// Inputs:  u16 ecm_type
//          u32 ecm_address
// Output:  u32 *ecm_offset
// Return:  u8  status
// Engineer: Quyen Leba
//------------------------------------------------------------------------------
u8 obd2tune_translate_ecm_address_to_ecm_offset(u16 ecm_type, u32 ecm_address,
                                                u32 *ecm_offset)
{
    u32 memblock_address;
    u32 memblock_size;
    u32 memblock_flagtype;
    u32 memblock_flagdata;
    u32 position;
    u8  i;
    u8  status;
    
    status = S_FAIL;
    *ecm_offset = 0;
    position = ECM_GetDataOffset(ecm_type);
    for(i=0;i<MAX_ECM_MEMORY_BLOCKS;i++)
    {
        if (ECM_IsUploadCalOnly(ecm_type))
        {
            memblock_address = ECM_GetCalBlockAddr(ecm_type,i);
            memblock_size = ECM_GetCalBlockSize(ecm_type,i);
            memblock_flagtype = ECM_GetCalBlockFlagType(ecm_type,i);
            memblock_flagdata = ECM_GetCalBlockFlagData(ecm_type,i);
        }
        else
        {
            memblock_address = ECM_GetOSReadBlockAddr(ecm_type,i);
            memblock_size = ECM_GetOSReadBlockSize(ecm_type,i);
            memblock_flagtype = ECM_GetOSReadBlockFlagType(ecm_type,i);
            memblock_flagdata = ECM_GetOSReadBlockFlagData(ecm_type,i);
        }
        if (memblock_size == 0)
        {
            //terminated
            break;
        }
        
        if (ecm_address >= memblock_address &&
            ecm_address < (memblock_address + memblock_size))
        {
            position += (ecm_address - memblock_address);
            *ecm_offset = position;
            status = S_SUCCESS;
            break;
        }
        else
        {
            position += memblock_size;
        }
        
        if (memblock_flagtype == MEMBLOCK_FLAGS_SIMPLE_FF_APPEND ||
            memblock_flagtype == MEMBLOCK_FLAGS_SIMPLE_00_APPEND)
        {
            if (ecm_address >= (memblock_address + memblock_size) &&
                ecm_address < (memblock_address + memblock_size + memblock_flagdata))
            {
                //memblock_size already added to position
                position += (memblock_address + memblock_flagdata - ecm_address);
                *ecm_offset = position;
                status = S_SUCCESS;
                break;
            }
            else
            {
                position += memblock_flagdata;
            }
        }
        else if (memblock_flagtype == MEMBLOCK_FLAGS_APPEND_FILE_BY_INDEX)
        {
            //TODOQ: add support for this
            log_push_error_point(0x44E9);
            return S_BADCONTENT;
        }
    }
    
    return status;
}

//------------------------------------------------------------------------------
// Remove custom tune files
// Return:  u8  status
// Engineer: Quyen Leba
//------------------------------------------------------------------------------
u8 obd2tune_remove_customtune_files()
{
    u8 status;
    u8 filename[256];
    u32 filesize;
    
    // Delete Index File
    genfs_deletefile(DEFAULT_CUSTOM_TUNE_INDEX_FILENAME);
    
    status = genfs_listing_init(2); // 2 - USER folder        
    if(status == S_SUCCESS)
    {
        while(status == S_SUCCESS)
        {
            status = genfs_listing_get_entry(filename,&filesize);
            if(status == S_SUCCESS)
            {
                if(strstr((char*)filename, "ctb"))
                {
                    genfs_deletefile(filename);
                }
            }
        }
    }
    
    return S_SUCCESS;
}

//------------------------------------------------------------------------------
// Get preloaded tune restriction
// Output:   u32 restriction_value (see PRELOADEDTUNE_RESTRICTION_...)
// Return:  u8  status
// Engineer: Patrick Downs
//------------------------------------------------------------------------------
u8 obd2tune_preloaded_tune_get_restriction(u32 *restriction_value)
{   
    
    *restriction_value = SETTINGS_TUNE(flags); 
    *restriction_value = (*restriction_value & PRELOADEDTUNE_RESTRICTION_MASK);
        
    return S_SUCCESS;
}

//------------------------------------------------------------------------------
// Set preloaded tune restriction
// Input:   u32 restriction_value (see PRELOADEDTUNE_RESTRICTION_...)
// Return:  u8  status
// Engineer: Quyen Leba
//------------------------------------------------------------------------------
u8 obd2tune_preloaded_tune_set_restriction(u32 restriction_value)
{
    u32 current_value;
    u8  status;

    status = S_SUCCESS;
    current_value = SETTINGS_TUNE(flags) & PRELOADEDTUNE_RESTRICTION_MASK;
    if (restriction_value & ~PRELOADEDTUNE_RESTRICTION_MASK)
    {
        log_push_error_point(0x44FC);
        status = S_INPUT;
    }
    else if (restriction_value == PRELOADEDTUNE_RESTRICTION_NONE)
    {
        if(!SETTINGS_IsMarried())
        {
            status = obd2tune_remove_customtune_files();
        }
        else
        {
            log_push_error_point(0x45B1);
            status = S_INVALIDSETTINGS; // Can't restore unless unlocked
        }
        if (status == S_SUCCESS)
        {
            if (restriction_value != current_value)
            {
                SETTINGS_TUNE(flags) &= ~PRELOADEDTUNE_RESTRICTION_MASK;
                SETTINGS_TUNE(flags) |= PRELOADEDTUNE_RESTRICTION_NONE;
                SETTINGS_SetTuneAreaDirty();
            }
        }
        else
        {
            log_push_error_point(0x44FD);
        }
    }
    else
    {
        if (restriction_value != current_value)
        {
            SETTINGS_TUNE(flags) &= ~PRELOADEDTUNE_RESTRICTION_MASK;
            SETTINGS_TUNE(flags) |= restriction_value;
            SETTINGS_SetTuneAreaDirty();
        }
    }

    if (status == S_SUCCESS)
    {
        status = settings_update(SETTINGS_UPDATE_IF_DIRTY);
        if (status != S_SUCCESS)
        {
            log_push_error_point(0x44FF);
        }
    }

    return status;
}

//------------------------------------------------------------------------------
// Get custom tune block by slot
// Input:   u16 slot_index
// Output:  customtune_block *block
// Return:  u8  status
// Engineer: Quyen Leba
// Note: DEFAULT_CUSTOM_TUNE_INDEX_FILENAME must be validated first
//------------------------------------------------------------------------------
u8 obd2tune_get_custom_tune_block(u16 slot_index, customtune_block *block)
{
    F_FILE *fptr;
    u32 bytecount;
    u8  status;

    fptr = genfs_user_openfile(DEFAULT_CUSTOM_TUNE_INDEX_FILENAME,"r");
    if (!fptr)
    {
        log_push_error_point(0x4555);
        return S_OPENFILE;
    }
    //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    // Get block
    //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    if (fseek(fptr,
              sizeof(customtune_header)+slot_index*sizeof(customtune_block),SEEK_SET) != 0)
    {
        log_push_error_point(0x4556);
        status = S_SEEKFILE;
        goto obd2tune_get_custom_tune_block_done;
    }
    bytecount = fread((char*)block,1,sizeof(customtune_block),fptr);
    if (bytecount != sizeof(customtune_block))
    {
        log_push_error_point(0x4557);
        status = S_READFILE;
        goto obd2tune_get_custom_tune_block_done;
    }
    crypto_blowfish_decryptblock_external_key((u8*)block,sizeof(customtune_block));
    status = S_SUCCESS;
    
obd2tune_get_custom_tune_block_done:
    if (fptr)
    {
        genfs_closefile(fptr);
    }
    return status;
}

//------------------------------------------------------------------------------
// Calculate custom tune block signature
// Input:   customtune_block *block
// Return:  u32 signature
// Engineer: Quyen Leba
//------------------------------------------------------------------------------
u32 obd2tune_calculate_custom_tune_block_signature(customtune_block *block)
{
    u8  buffer[32];

    memcpy((char*)&buffer[0],(char*)&block->market_type,4);
    memcpy((char*)&buffer[4],(char*)&block->tunefilecrc32e,4);
    memcpy((char*)&buffer[8],(char*)&block->optionfilecrc32e,4);
    memcpy((char*)&buffer[12],(char*)&block->bootloader_crc32e,4);
    memcpy((char*)&buffer[16],(char*)block->serialnumber,16);

    crypto_blowfish_encryptblock_critical(buffer,sizeof(buffer));
    crc32e_reset();
    return crc32e_calculateblock(0xFFFFFFFF,(u32*)buffer,sizeof(buffer)/4);
}

/**
 *  obd2tune_validate_custom_tune_block
 *  
 *  @brief Validate custom tune block info
 *
 *  @param[in]  block
 *  @param[in]  skip_signature_check
 *
 *  @retval u8 status
 *  
 *  @authors Quyen Leba
 */
u8 obd2tune_validate_custom_tune_block(customtune_block *block, bool skip_signature_check)
{
    F_FILE *fptr;
    u32 crc32e_calc;
    u8  status;

    fptr = NULL;
    if (settings_compare_deviceserialnumber(block->serialnumber) != S_SUCCESS)
    {
        log_push_error_point(0x4558);
        return S_SERIALUNMATCH;
    }

    if (!skip_signature_check)
    {
        if (obd2tune_calculate_custom_tune_block_signature(block) != block->signature)
        {
            log_push_error_point(0x4559);
            return S_UNMATCH;
        }
    }

    if (!isgraph(block->tunefilename[0]))
    {
        log_push_error_point(0x455A);
        return S_BADCONTENT;
    }
    else
    {
        fptr = genfs_user_openfile(block->tunefilename,"r");
        if (!fptr)
        {
            log_push_error_point(0x455B);
            return S_FILENOTFOUND;
        }
        /* Handle sth files */
        if(strstr((char*)block->tunefilename, ".sth"))
        {
            u32 length;
            sthFileHeader header;
            
            if (obd2tune_read_sth_header(fptr, &header, &length) != S_SUCCESS)
            {
                log_push_error_point(0x4566);
                status = S_READFILE;
                goto obd2tune_validate_custom_tune_block_done;
            }
            crc32e_calc = header.generic.header_crc32e;
        }
        /* Other type (.bin) */
        else
        {
            if (file_calcfilecrc32e(fptr,&crc32e_calc) != S_SUCCESS)
            {
                log_push_error_point(0x455C);
                status = S_FAIL;
                goto obd2tune_validate_custom_tune_block_done;
            }
        }
        if (crc32e_calc != block->tunefilecrc32e)
        {
            log_push_error_point(0x455D);
            status = S_CRC32E;
            goto obd2tune_validate_custom_tune_block_done;
        }
        genfs_closefile(fptr);
        fptr = NULL;
    }

    if (isgraph(block->optionfilename[0]))
    {
        fptr = genfs_user_openfile(block->optionfilename,"r");
        if (!fptr)
        {
            log_push_error_point(0x455E);
            return S_FILENOTFOUND;
        }
        if (file_calcfilecrc32e(fptr,&crc32e_calc) != S_SUCCESS)
        {
            log_push_error_point(0x455F);
            status = S_FAIL;
            goto obd2tune_validate_custom_tune_block_done;
        }
        if (crc32e_calc != block->optionfilecrc32e)
        {
            log_push_error_point(0x4560);
            status = S_CRC32E;
            goto obd2tune_validate_custom_tune_block_done;
        }
        genfs_closefile(fptr);
        fptr = NULL;
    }

    if (isgraph(block->tuneosfilename[0]))
    {
        fptr = genfs_user_openfile(block->tuneosfilename,"r");
        if (!fptr)
        {
            log_push_error_point(0x4570);
            return S_FILENOTFOUND;
        }
        //TODO: check crc32e
        genfs_closefile(fptr);
    }

    if (isgraph(block->specialoptionfilename[0]))
    {
        fptr = genfs_user_openfile(block->specialoptionfilename,"r");
        if (!fptr)
        {
            log_push_error_point(0x4580);
            return S_FILENOTFOUND;
        }
        //TODO: check crc32e
        genfs_closefile(fptr);
    }

    if (isgraph(block->checksumfilename[0]))
    {
        fptr = genfs_user_openfile(block->checksumfilename,"r");
        if (!fptr)
        {
            log_push_error_point(0x4590);
            return S_FILENOTFOUND;
        }
        //TODO: check crc32e
        genfs_closefile(fptr);       
    }

    if (isgraph(block->bootloader_upload[0]))
    {
        fptr = genfs_user_openfile(block->bootloader_upload,"r");
        if (!fptr)
        {
            log_push_error_point(0x45A0);
            return S_FILENOTFOUND;
        }
        //TODO: check crc32e
        genfs_closefile(fptr);
    }

    if (isgraph(block->bootloader_download[0]))
    {
        fptr = genfs_user_openfile(block->bootloader_download,"r");
        if (!fptr)
        {
            log_push_error_point(0x45B0);
            return S_FILENOTFOUND;
        }
        //TODO: check crc32e
        genfs_closefile(fptr);
    }
    status = S_SUCCESS;

obd2tune_validate_custom_tune_block_done:
    if (fptr)
    {
        genfs_closefile(fptr);
    }
    return status;
}

//------------------------------------------------------------------------------
// Process custom tune block (prepare it for programming)
// Inputs:  customtune_block *block
//          bool isUseOption (TRUE: user adjust options)
// Return:  u8  status
// Engineer: Quyen Leba
//------------------------------------------------------------------------------
u8 obd2tune_process_custom_tune_block(customtune_block *block, bool isUseOption)
{
    customtune_tuneosdata tuneosdata;
    u8  correctedfilename[512];
    u8 tuneosfilepath[64];
    u8  status;
    int i;

    if(genfs_userpathcorrection((u8*)block->tunefilename,sizeof(correctedfilename),correctedfilename) == S_SUCCESS)
        strcpy((char*)flasherinfo->selected_tunelistinfo.tunefilename[0],(char*)correctedfilename);
    else
        strcpy((char*)flasherinfo->selected_tunelistinfo.tunefilename[0],(char*)block->tunefilename);

    memcpy((char*)flasherinfo->selected_tunelistinfo.description[0],
           (char*)block->tunedescription,MAX_DESCRIPTION_LENGTH);
    flasherinfo->selected_tunelistinfo.description[0][MAX_DESCRIPTION_LENGTH] = NULL;
    flasherinfo->selected_tunelistinfo.tuneindex[0] = block->veh_type;
    flasherinfo->vehicle_type = block->veh_type;

    status = vehdef_get_vehdef_info(&flasherinfo->vehicle_type, 1, NULL);
    
    if (status != S_SUCCESS)
    {
        log_push_error_point(0x460F);
        return S_BADCONTENT;
    }
    
    flasherinfo->ecm_count = VEH_GetEcmCount(block->veh_type);
    flasherinfo->selected_tunelistinfo.flags[0] = block->flags;
    flasherinfo->selected_tunelistinfo.useoption[0] = TUNEUSEOPTION_NONE;
    flasherinfo->selected_tunelistinfo.optionfilename[0][0] = NULL;
    
    if (strlen((char*)block->checksumfilename) + strlen(GENFS_USER_FOLDER_PATH) > MAX_TUNE_NAME_LENGTH)
    {
        //filename is too long
        log_push_error_point(0x44DE);
        return S_BADCONTENT;
    }
    else if (strlen((char*)block->checksumfilename) > 0)
    {
        strcpy((char*)flasherinfo->selected_tunelistinfo.checksumfilename[0],
               GENFS_USER_FOLDER_PATH);
        strcat((char*)flasherinfo->selected_tunelistinfo.checksumfilename[0],
               (char*)block->checksumfilename);
    }
    
    if (isUseOption)
    {
        flasherinfo->selected_tunelistinfo.useoption[0] |= TUNEUSEOPTION_REGULAR;
        if(genfs_userpathcorrection((u8*)block->optionfilename,sizeof(correctedfilename),correctedfilename) == S_SUCCESS)
            strcpy((char*)flasherinfo->selected_tunelistinfo.optionfilename[0],(char*)correctedfilename);
        else
            strcpy((char*)flasherinfo->selected_tunelistinfo.optionfilename[0],(char*)block->optionfilename);
    }

    if (isgraph(block->tuneosfilename[0]))
    {
        if (strstr((char*)block->tuneosfilename,".tos"))        //if tuneos#.tos
        {
            genfs_userpathcorrection(block->tuneosfilename, sizeof(tuneosfilepath), 
                                     tuneosfilepath);
            status = obd2tune_extracttuneosdata_tos(tuneosfilepath, &tuneosdata);
            if (status != S_SUCCESS)
            {
                log_push_error_point(0x44CC);
                return status;
            }
            if (strlen((char*)tuneosdata.basestockfilename) >=
                sizeof(flasherinfo->basestockfilename))
            {
                log_push_error_point(0x44EE);
                return S_BADCONTENT;
            }
            else if (strlen((char*)tuneosdata.codes) >=
                     sizeof(flasherinfo->vehiclecodes))
            {
                log_push_error_point(0x44BB);
                return S_BADCONTENT;
            }
            else if (strlen((char*)tuneosdata.secondcodes) >=
                     sizeof(flasherinfo->secondvehiclecodes))
            {
                log_push_error_point(0x44BC);
                return S_BADCONTENT;
            }
            for(i=0; i < tuneosdata.ecm_count; i++)
            {
                /* no path for SPF here since custom tune could use any */
                strcpy((char*)flasherinfo->basestockfilename[i],
                       (char*)tuneosdata.basestockfilename[i]);
                flasherinfo->basestockcrc32e[i] = tuneosdata.basestockcrc32e[i];
            }
            strcpy((char*)flasherinfo->vehiclecodes,(char*)tuneosdata.codes);
            strcpy((char*)flasherinfo->secondvehiclecodes,(char*)tuneosdata.secondcodes);
        }
        else if (strstr((char*)block->tuneosfilename,".txt"))   //if tuneos#.txt
        {
            status = obd2tune_extracttuneosdata_txt(block->tuneosfilename,
                                                    &tuneosdata);
            if (status != S_SUCCESS)
            {
                log_push_error_point(0x44BE);
                return S_FAIL;
            }
            for(i =0;i<ECM_MAX_COUNT;i++)
            {
                memset((char*)flasherinfo->basestockfilename[i],
                       0,sizeof(flasherinfo->basestockfilename[i]));
                flasherinfo->basestockcrc32e[i] = 0;
            }
            strcpy((char*)flasherinfo->vehiclecodes,
                   (char*)tuneosdata.codes);
        }
        else
        {
            log_push_error_point(0x44BD);
            return S_NOTSUPPORT;
        }
    }

    // Custom Tune Block Flags
    // Preloaded Tune Disable
    if (obd2tune_tuneflags_isdisablepreload(block->flags))
        flasherinfo->flags |= FLASHERFLAG_DISABLE_PRELOADED_TUNE;
    else if (obd2tune_tuneflags_isdisablepreloadremovetunes(block->flags))
        flasherinfo->flags |= FLASHERFLAG_DISABLE_PRELOADED_TUNE_REMOVE;
    // Custom Tune Encrpytion Flags, TEA only supported as right now
    if (!obd2tune_tuneflags_isencrptionTEA(block->flags))
    {
        log_push_error_point(0x44BF);
        return S_NOTSUPPORT;
    }

    return S_SUCCESS;
}

//------------------------------------------------------------------------------
// Extract tuneos data (tos format)
// Input:   const u8 *tuneosfilename
//          u32 maxcodebufferlength (for tuneos_codes & tuneos_secondcodes)
// Output:  customtune_tuneosdata *tuneosdata
// Return:  u8  status
// Engineer: Quyen Leba
//------------------------------------------------------------------------------
u8 obd2tune_extracttuneosdata_tos(const u8 *tuneosfilename,
                                  customtune_tuneosdata *tuneosdata)
{
    F_FILE *fptr;
    customtune_tuneosheader tuneosheader;
    customtune_tuneosinfoblock tuneosinfoblock;
    u8  tmpbuffer[128];
    u32 bytecount;
    u32 crc32e_cmp;
    u32 crc32e_calc;
    u32 i;
    u8  numberofcodefound;      //total codes found
    u8  numberofcodefit;        //how many fit in buffer
    u8  status;

    memset((char*)tuneosdata,0,sizeof(customtune_tuneosdata));
    status = S_SUCCESS;
    numberofcodefound = 0;
    numberofcodefit = 0;
    if (tuneosdata == NULL)
    {
        log_push_error_point(0x45B2);
        return S_INPUT;
    }
    fptr = genfs_general_openfile(tuneosfilename,"r");
    if (!fptr)
    {
        log_push_error_point(0x45B3);
        return S_OPENFILE;
    }
    bytecount = fread((char*)&tuneosheader,1,sizeof(tuneosheader),fptr);
    if (bytecount != sizeof(tuneosheader))
    {
        log_push_error_point(0x45B4);
        status = S_READFILE;
        goto obd2tune_gettuneosheader_tos_done;
    }
    crypto_blowfish_decryptblock_critical((u8*)&tuneosheader,
                                              sizeof(tuneosheader));
    crc32e_cmp = tuneosheader.header_crc32e;
    tuneosheader.header_crc32e = 0xFFFFFFFF;
    crc32e_reset();
    crc32e_calc = crc32e_calculateblock(0xFFFFFFFF,(u32*)&tuneosheader,
                                        sizeof(tuneosheader)/4);
    if (crc32e_cmp != crc32e_calc)
    {
        log_push_error_point(0x45B5);
        status = S_CRC32E;
        goto obd2tune_gettuneosheader_tos_done;
    }
    if (tuneosheader.version != CUSTOMTUNE_TUNEOS_VERSION)
    {
        log_push_error_point(0x45B6);
        status = S_NOTSUPPORT;
        goto obd2tune_gettuneosheader_tos_done;
    }
    if (strlen((char*)tuneosheader.maskable_vin) > VIN_LENGTH)
    {
        log_push_error_point(0x45C0);
        status = S_BADCONTENT;
        goto obd2tune_gettuneosheader_tos_done;
    }
    strcpy((char*)tuneosdata->vin,(char*)tuneosheader.maskable_vin);
    if (strlen((char*)tuneosheader.basestockfilename) >= sizeof(tuneosdata->basestockfilename))
    {
        log_push_error_point(0x45C3);
        status = S_BADCONTENT;
        goto obd2tune_gettuneosheader_tos_done;
    }
    
    tuneosdata->ecm_count = tuneosheader.ecm_count;
    for(i = 0; i < tuneosheader.ecm_count; i++)
    {
        strcpy((char*)tuneosdata->basestockfilename[i],(char*)tuneosheader.basestockfilename[i]);
        tuneosdata->basestockcrc32e[i] = tuneosheader.basestockcrc32e[i];
        // Validate stockfile CRC32?
    }
    
    tuneosdata->codes[0] = NULL;         //result: "code1*code2*"
    tuneosdata->secondcodes[0] = NULL;   //result: "secondcode1*secondcode2*"

    crc32e_reset();
    crc32e_calc = 0xFFFFFFFF;
    while(!feof(fptr))
    {
        bytecount = fread((char*)&tuneosinfoblock,1,sizeof(tuneosinfoblock),fptr);
        if (bytecount == sizeof(tuneosinfoblock))
        {
            numberofcodefound++;
            crc32e_calc = crc32e_calculateblock(crc32e_calc,(u32*)&tuneosinfoblock,
                                                sizeof(tuneosinfoblock)/4);
            crypto_blowfish_decryptblock_critical((u8*)&tuneosinfoblock,
                                              sizeof(tuneosinfoblock));

            for(i=0;i<ECM_MAX_COUNT;i++)
            {
                if (tuneosinfoblock.codes[i][0][0] == 0)
                {
                    strcpy((char*)tuneosinfoblock.codes[i][0],"N/A");
                }
                if (tuneosinfoblock.second_codes[i][0][0] == 0)
                {
                    strcpy((char*)tuneosinfoblock.second_codes[i][0],"N/A");
                }
                
                if (((strlen((char*)tuneosdata->codes) + strlen((char*)tuneosinfoblock.codes[i][0])+1) < sizeof(tuneosdata->codes)) &&
                    ((strlen((char*)tuneosdata->secondcodes) + strlen((char*)tuneosinfoblock.second_codes[i][0])+1) < sizeof(tuneosdata->codes)))
                {
                    if (strlen((char*)tuneosinfoblock.codes[i][0]) < sizeof(tmpbuffer) &&
                        strlen((char*)tuneosinfoblock.second_codes[i][0]) < sizeof(tmpbuffer))
                    {
                        memcpy((char*)tmpbuffer,tuneosinfoblock.codes[i][0],
                               sizeof(tuneosinfoblock.codes[i][0]));
                        obd2tune_stringreplace(tmpbuffer,TUNECODE_TUNEMASK_INFILE,
                                               TUNECODE_TUNEMASK_INCODE);
                        strcat((char*)tuneosdata->codes,(char*)tmpbuffer);
                        strcat((char*)tuneosdata->codes,TUNECODE_SEPARATOR_STRING);
                        
                        memcpy((char*)tmpbuffer,tuneosinfoblock.second_codes[i][0],
                               sizeof(tuneosinfoblock.second_codes[i][0]));
                        obd2tune_stringreplace(tmpbuffer,TUNECODE_TUNEMASK_INFILE,
                                               TUNECODE_TUNEMASK_INCODE);
                        strcat((char*)tuneosdata->secondcodes,(char*)tmpbuffer);
                        strcat((char*)tuneosdata->secondcodes,TUNECODE_SEPARATOR_STRING);
                    }
                }
                else
                {
                    break;
                }
            }//for(i=0;i<ECM_MAX_COUNT;i++)...
            numberofcodefit++;
        }//if (bytecount ...
    }//while(!feof(fptr))...

    if (tuneosheader.content_crc32e != crc32e_calc)
    {
        log_push_error_point(0x45BE);
        status = S_CRC32E;
        goto obd2tune_gettuneosheader_tos_done;
    }
    if (numberofcodefound != numberofcodefit)
    {
        log_push_error_point(0x45BF);
        status = S_NOTFIT;
        goto obd2tune_gettuneosheader_tos_done;
    }

obd2tune_gettuneosheader_tos_done:
    if (fptr)
    {
        genfs_closefile(fptr);
    }
    tuneosdata->codecount = 0;
    if (status == S_SUCCESS)
    {
        tuneosdata->codecount = numberofcodefound;
    }

    return status;
}

//------------------------------------------------------------------------------
// Extract tuneos data (txt format)
// Input:   const u8 *tuneosfilename
//          u32 maxcodebufferlength (for tuneos_codes & tuneos_secondcodes)
// Output:  customtune_tuneosdata *tuneosdata
// Return:  u8  status
// Engineer: Quyen Leba
//------------------------------------------------------------------------------
u8 obd2tune_extracttuneosdata_txt(const u8 *tuneosfilename,
                                  customtune_tuneosdata *tuneosdata)
{
    return S_NOTSUPPORT;
//    F_FILE *fptr;
//    u8  linebuffer[1024];
//    u32 linebufferlength;
//    u32 filelength;
//    u8  numberofcodefound;
//    u8  status;
//    u32 i;
//
//    memset((char*)tuneosdata,0,sizeof(customtune_tuneosdata));
//    numberofcodefound = 0;
//    status = S_SUCCESS;
//    if (tuneosdata == NULL)
//    {
//        log_push_error_point(0x45C2);
//        return S_INPUT;
//    }
//
//    fptr = genfs_user_openfile(tuneosfilename,"rb");
//    if (fptr == NULL)
//    {
//        status = S_OPENFILE;
//        log_push_error_point(0x45B7);
//        goto obd2tune_extracttuneosdata_txt_done;
//    }
//
//    status = file_getfilesize(fptr,&filelength);
//    if (status != S_SUCCESS)
//    {
//        log_push_error_point(0x45B8);
//        goto obd2tune_extracttuneosdata_txt_done;
//    }
//
//    //read VIN
//    status = file_readline(fptr,linebuffer,&linebufferlength);
//    if (status != S_SUCCESS)
//    {
//        status = S_READFILE;
//        log_push_error_point(0x45B9);
//        goto obd2tune_extracttuneosdata_txt_done;
//    }
//
//    if (linebufferlength != VIN_LENGTH)
//    {
//        status = S_BADCONTENT;
//        log_push_error_point(0x45BA);
//        goto obd2tune_extracttuneosdata_txt_done;
//    }
//
//    memcpy((char*)tuneosdata->vin,linebuffer,VIN_LENGTH);
//    tuneosdata->vin[VIN_LENGTH] = 0;
//    //these info not available in this file
//    memset((char*)tuneosdata->secondcodes,0,sizeof(tuneosdata->secondcodes));
//    for(i=0; i<ECM_MAX_COUNT; i++)
//    {
//        memset((char*)tuneosdata->basestockfilename[i],0,sizeof(tuneosdata->basestockfilename[i]));
//        tuneosdata->basestockcrc32e[i] = 0;
//    }
//
//    //get part#
//    while(!feof(fptr))
//    {
//        status = file_readline(fptr,linebuffer,&linebufferlength);
//        if (status != S_SUCCESS)
//        {
//            status = S_READFILE;
//            log_push_error_point(0x45BD);
//            goto obd2tune_extracttuneosdata_txt_done;
//        }
//        if (strlen((char*)tuneosdata->codes)+strlen((char*)linebuffer)+1 < sizeof(tuneosdata->codes))
//        {
//            strcat((char*)tuneosdata->codes,(char*)linebuffer);
//            strcat((char*)tuneosdata->codes,TUNECODE_SEPARATOR_STRING);
//            numberofcodefound++;
//        }
//    }//while(!f_eof(fptr))...
//
//obd2tune_extracttuneosdata_txt_done:
//    if (fptr)
//    {
//        genfs_closefile(fptr);
//    }
//    tuneosdata->codecount = 0;
//    if (status == S_SUCCESS)
//    {
//        tuneosdata->codecount = numberofcodefound;
//    }
//    return status;
}

//------------------------------------------------------------------------------
// Compare str2 against str1 w/ mask byte
// Inputs:  const u8 *str1 (main string & contains mask bytes)
//          const u8 *str2
//          u8  maskbyte
// Return:  u8  status
// Engineer: Quyen Leba
//------------------------------------------------------------------------------
u8 obd2tune_maskablestringcompare(const u8 *str1, const u8 *str2, u8 maskbyte)
{
    u32 str1len;
    u32 str2len;
    u32 i;

    str1len = strlen((char*)str1);
    str2len = strlen((char*)str2);
    if (str1len != str2len)
    {
        return S_UNMATCH;
    }

    for(i=0;i<str1len;i++)
    {
        if (str1[i] != maskbyte && str1[i] != str2[i])
        {
            return S_UNMATCH;
        }
    }
    return S_SUCCESS;
}

//------------------------------------------------------------------------------
// Compare str2 against str1 w/ mask byte up to length
// Inputs:  const u8 *str1 (main string & contains mask bytes)
//          const u8 *str2
//          u8  maskbyte
//          u32 length (to compare string with)
// Return:  u8  status
// Engineer: Quyen Leba
//------------------------------------------------------------------------------
u8 obd2tune_maskablestringncompare(const u8 *str1, const u8 *str2, u8 maskbyte, u32 length)
{
    u32 str1len;
    u32 str2len;
    u32 i;

    str1len = strlen((char*)str1);
    str2len = strlen((char*)str2);
    if (str1len < length)
    {
        return S_UNMATCH;
    }

    for(i=0;i<length;i++)
    {
        if (str1[i] != maskbyte)
        {
            if (i >= str2len)
            {
                return S_UNMATCH;
            }
            else if (str1[i] != str2[i])
            {
                return S_UNMATCH;
            }
        }
    }
    return S_SUCCESS;
}

//------------------------------------------------------------------------------
// Compare to check if code is supported in vehiclecodes
// Inputs:  const u8 *vehiclecodes
//          const u8 *code
//          u8  slot (start from ZERO; slot in vehiclecodes to compare)
// Return:  u8  status
// Engineer: Quyen Leba
//------------------------------------------------------------------------------
u8 obd2tune_comparetunecode(const u8 *vehiclecodes, const u8 *code, u8 slot)
{
    u8  codebuffer[512];
    u8  *bptr;
    u8  index;

    if (strlen((char*)vehiclecodes) >= sizeof(codebuffer))
    {
        return S_INPUT;
    }
    strcpy((char*)codebuffer,(char*)vehiclecodes);

    index = 0;
    bptr = (u8*)strtok((char*)codebuffer,TUNECODE_SEPARATOR_STRING);
    while(bptr != NULL)
    {
        if (index++ == slot)
        {
            if (obd2tune_maskablestringncompare(bptr,code,TUNECODE_TUNEMASK_INCODE,strlen((char*)bptr)) == S_SUCCESS)
            {
                return S_SUCCESS;
            }
        }
        bptr = (u8*)strtok(NULL,TUNECODE_SEPARATOR_STRING);
        if (index >= ECM_MAX_COUNT)
        {
            index = 0;
        }
    }
    return S_UNMATCH;
}

//------------------------------------------------------------------------------
// Replace chars in string
// Inputs:  const u8 *str
//          u8  searchchar
//          u8  replacechar
//          u8  maskbyte
// Engineer: Quyen Leba
//------------------------------------------------------------------------------
void obd2tune_stringreplace(u8 *str, u8 searchchar, u8 replacechar)
{
    u32 i;

    if (searchchar == 0)
    {
        return;
    }

    for(i=0;i<strlen((char*)str);i++)
    {
        if (str[i] == searchchar)
        {
            str[i] = replacechar;
        }
    }
}

//------------------------------------------------------------------------------
// Apply checksum to file
// Input:   u16 veh_type
//          selected_tunelist_info *tuneinfo
// Return:  u8  status
// Engineer: Quyen Leba
//------------------------------------------------------------------------------
#ifndef OBD2TUNE_APPLY_CHECKSUM_DEFINED
#error "odb2.c: no definition for obd2tune_apply_checksum(...)"
#endif

//------------------------------------------------------------------------------
// Check checksum of stock file
// Input:   u16 veh_type
//          selected_tunelist_info *tuneinfo
// Return:  u8  status
// Engineer: Quyen Leba
//------------------------------------------------------------------------------
#ifndef OBD2TUNE_CHECK_STOCKFILE_CHECKSUM_DEFINED
#error "odb2.c: no definition for obd2tune_check_stockfile_checksum(...)"
#endif

//------------------------------------------------------------------------------
// Check if the custom tune is allowed
// Input:   u16 veh_type
//          selected_tunelist_info *tuneinfo
// Return:  u8  status
// Engineer: Quyen Leba
// Note: only useful for custom tune
//------------------------------------------------------------------------------
#ifndef OBD2TUNE_CHECK_CUSTOMTUNE_SUPPORT_DEFINED
#error "odb2.c: no definition for obd2tune_check_customtune_support(...)"
#endif

//------------------------------------------------------------------------------
// Test ECU OS checksum to determine if OS reflash is required
// Input:   u16 veh_type
// Return:  u8  status
// Engineer: Quyen Leba
//------------------------------------------------------------------------------
#ifndef OBD2TUNE_ECU_TEST_OS_CHECKSUM_DEFINED
#error "odb2.c: no definition for obd2tune_ecu_test_os_checksum(...)"
#endif

//------------------------------------------------------------------------------
// Test file OS checksum to determine if file is good
// Input:   u16 veh_type
// Return:  u8  status
// Engineer: Quyen Leba
//------------------------------------------------------------------------------
#ifndef OBD2TUNE_FILE_TEST_OS_CHECKSUM_DEFINED
#error "odb2.c: no definition for obd2tune_file_test_os_checksum(...)"
#endif

//------------------------------------------------------------------------------
// Comapre vehicle codes from vehicle with the ones saved in settings
// Input:   ecm_info ecminfo
// Output:  u8 *ecm_match_status
// Return:  u8 all_match_status;
// Engineer: Patrick Downs
//------------------------------------------------------------------------------
#ifndef OBD2TUNE_COMPARE_VEHICLE_CODES_WITH_SETTINGS_DEFINED
#error "odb2.c: no definition for obd2tune_compare_vehiclecodes_with_setting(...)"
#endif

//------------------------------------------------------------------------------
// Comapre vehicle codes from vehicle with the ones from the tuneos file
// Input:   ecm_info ecminfo
// Output:  u8 *ecm_match_status
// Return:  u8 all_match_status;
// Engineer: Patrick Downs
//------------------------------------------------------------------------------
#ifndef OBD2TUNE_COMPARE_VEHICLE_CODES_WITH_TUNEOSDATA_DEFINED
#error "odb2.c: no definition for obd2tune_compare_vehiclecodes_with_tuneosdata(...)"
#endif

//------------------------------------------------------------------------------
// Merge 2 option files (only accept oph)
// Inputs:  const u8 *mainoptionfilename (include path)
//          const u8 *suboptionfilename (include path)
//          const u8 *mergeoptionfilename (include path)
//          bool iscompletemerge (FALSE: only merge text parts)
// Return:  u8  status
// Engineer: Patrick Downs
//------------------------------------------------------------------------------
u8 obd2tune_mergeoptionfile(const u8 *mainoptionfilename,
                            const u8 *suboptionfilename,
                            const u8 *mergeoptionfilename)
{
    F_FILE *fmainptr;
    F_FILE *fsubptr;
    F_FILE *fmergeptr;
    u8  buffer[2048];
    ophOptionFileHeader mainHeader;
    ophOptionFileHeader subHeader;
    ophOptionFileHeader mergeHeader;
    ophOptionItemInfoBlock itemInfoBlock;
    ophChoiceItemInfoBlock choiceInfoBlock;
    u32 mainOptionfileLength;
    u32 choice_offset;
    u32 bytecount;
    u32 length;
    u32 crc32e_calc;
    u32 i;
    u8  status;

    fmainptr = NULL;
    fsubptr = NULL;
    fmergeptr = NULL;
    status = S_FAIL;
    //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    // Error checking
    //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    if (strstr((char*)mainoptionfilename,".oph") == NULL)
    {
        log_push_error_point(0x45C4);
        return S_NOTSUPPORT;
    }
    else if (strstr((char*)suboptionfilename,".oph") == NULL)
    {
        log_push_error_point(0x45C5);
        return S_NOTSUPPORT;
    }
    else if (strstr((char*)mergeoptionfilename,".oph") == NULL)
    {
        log_push_error_point(0x45D8);
        return S_NOTSUPPORT;
    }
    status = obd2tune_validate_oph_file(mainoptionfilename);
    if (status != S_SUCCESS)
    {
        log_push_error_point(0x45C6);
        return status;
    }
    status = obd2tune_validate_oph_file(suboptionfilename);
    if (status != S_SUCCESS)
    {
        log_push_error_point(0x45C7);
        return status;
    }

    fmainptr = genfs_general_openfile(mainoptionfilename,"r");
    if (fmainptr == NULL)
    {
        log_push_error_point(0x45C8);
        return S_OPENFILE;
    }
    fsubptr = genfs_general_openfile(suboptionfilename,"r");
    if (fsubptr == NULL)
    {
        log_push_error_point(0x45C9);
        status = S_OPENFILE;
        goto obd2tune_mergeoptionfile_done;
    }
    fmergeptr = genfs_general_openfile(mergeoptionfilename,"w+");
    if (fmergeptr == NULL)
    {
        log_push_error_point(0x45CA);
        status = S_OPENFILE;
        goto obd2tune_mergeoptionfile_done;
    }

    bytecount = fread((char*)&mainHeader,1,sizeof(ophOptionFileHeader),fmainptr);
    if (bytecount != sizeof(ophOptionFileHeader))
    {
        log_push_error_point(0x45CB);
        status = S_READFILE;
        goto obd2tune_mergeoptionfile_done;
    }
    bytecount = fread((char*)&subHeader,1,sizeof(ophOptionFileHeader),fsubptr);
    if (bytecount != sizeof(ophOptionFileHeader))
    {
        log_push_error_point(0x45CC);
        status = S_READFILE;
        goto obd2tune_mergeoptionfile_done;
    }
    status = file_getfilesize(fmainptr,&mainOptionfileLength);
    if (status != S_SUCCESS)
    {
        log_push_error_point(0x45C1);
        goto obd2tune_mergeoptionfile_done;
    }

    //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    // Process option files
    //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    memcpy((char*)&mergeHeader,(char*)&mainHeader,sizeof(mergeHeader));

    crc32e_reset();
    crc32e_calc = 0xFFFFFFFF;       //to calculate merge content crc32e

    mergeHeader.option_count += subHeader.option_count;
    //header + main option list + sub option list
    mergeHeader.firstchoice_offset += (subHeader.firstchoice_offset - subHeader.header_length);
    //firstchoice_offset + main option choices + sub option choices
    mergeHeader.firstrecord_offset += (subHeader.firstrecord_offset - subHeader.header_length);

    bytecount = fwrite((char*)&mergeHeader,1,sizeof(mergeHeader),fmergeptr);
    if (bytecount != sizeof(mergeHeader))
    {
        log_push_error_point(0x45D9);
        status = S_WRITEFILE;
        goto obd2tune_mergeoptionfile_done;
    }

    //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

    //converting choice offset of main option list in merge file
    choice_offset = subHeader.firstchoice_offset - subHeader.header_length;
    for(i=0;i<mainHeader.option_count;i++)
    {
        bytecount = fread((char*)&itemInfoBlock,1,sizeof(itemInfoBlock),fmainptr);
        if (bytecount != sizeof(itemInfoBlock))
        {
            log_push_error_point(0x45CD);
            status = S_READFILE;
            goto obd2tune_mergeoptionfile_done;
        }
        itemInfoBlock.choicecontent_offset += choice_offset;

        crc32e_calc = crc32e_calculateblock(crc32e_calc,
                                            (u32*)&itemInfoBlock,sizeof(itemInfoBlock)/4);
        bytecount = fwrite((char*)&itemInfoBlock,1,sizeof(itemInfoBlock),fmergeptr);
        if (bytecount != sizeof(itemInfoBlock))
        {
            log_push_error_point(0x45CE);
            status = S_WRITEFILE;
            goto obd2tune_mergeoptionfile_done;
        }
    }//for(i=...

    //converting choice offset of sub option list in merge file
    choice_offset = mainHeader.firstrecord_offset - subHeader.header_length;
    for(i=0;i<subHeader.option_count;i++)
    {
        bytecount = fread((char*)&itemInfoBlock,1,sizeof(itemInfoBlock),fsubptr);
        if (bytecount != sizeof(itemInfoBlock))
        {
            log_push_error_point(0x45CF);
            status = S_READFILE;
            goto obd2tune_mergeoptionfile_done;
        }
        itemInfoBlock.choicecontent_offset += choice_offset;

        crc32e_calc = crc32e_calculateblock(crc32e_calc,
                                            (u32*)&itemInfoBlock,sizeof(itemInfoBlock)/4);
        bytecount = fwrite((char*)&itemInfoBlock,1,sizeof(itemInfoBlock),fmergeptr);
        if (bytecount != sizeof(itemInfoBlock))
        {
            log_push_error_point(0x45D0);
            status = S_WRITEFILE;
            goto obd2tune_mergeoptionfile_done;
        }
    }//for(i=...

    //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

    //converting data choice offset of main option choices in merge file
    choice_offset = mergeHeader.firstrecord_offset - mainHeader.firstrecord_offset;
    //length of all choice data in main option file
    length = mainHeader.firstrecord_offset - mainHeader.firstchoice_offset;
    i = 0;
    while(i < length)
    {
        bytecount = fread((char*)&choiceInfoBlock,1,sizeof(choiceInfoBlock),fmainptr);
        i += bytecount;
        if (bytecount != sizeof(choiceInfoBlock))
        {
            log_push_error_point(0x45D1);
            status = S_READFILE;
            goto obd2tune_mergeoptionfile_done;
        }
        choiceInfoBlock.choicedata_offset += choice_offset;

        crc32e_calc = crc32e_calculateblock(crc32e_calc,
                                            (u32*)&choiceInfoBlock,sizeof(choiceInfoBlock)/4);
        bytecount = fwrite((char*)&choiceInfoBlock,1,sizeof(choiceInfoBlock),fmergeptr);
        if (bytecount != sizeof(choiceInfoBlock))
        {
            log_push_error_point(0x45D2);
            status = S_WRITEFILE;
            goto obd2tune_mergeoptionfile_done;
        }
    }//while(i < length)...

    //converting data choice offset of sub option choices in merge file
    choice_offset = mainOptionfileLength - mainHeader.header_length;
    //length of all choice data in main option file
    length = subHeader.firstrecord_offset - subHeader.firstchoice_offset;
    i = 0;
    while(i < length)
    {
        bytecount = fread((char*)&choiceInfoBlock,1,sizeof(choiceInfoBlock),fsubptr);
        i += bytecount;
        if (bytecount != sizeof(choiceInfoBlock))
        {
            log_push_error_point(0x45D3);
            status = S_READFILE;
            goto obd2tune_mergeoptionfile_done;
        }
        choiceInfoBlock.choicedata_offset += choice_offset;

        crc32e_calc = crc32e_calculateblock(crc32e_calc,
                                            (u32*)&choiceInfoBlock,sizeof(choiceInfoBlock)/4);
        bytecount = fwrite((char*)&choiceInfoBlock,1,sizeof(choiceInfoBlock),fmergeptr);
        if (bytecount != sizeof(choiceInfoBlock))
        {
            log_push_error_point(0x45D4);
            status = S_WRITEFILE;
            goto obd2tune_mergeoptionfile_done;
        }
    }//while(i < length)...

    //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

    //add main records to merge file
    while(!feof(fmainptr))
    {
        bytecount = fread((char*)buffer,1,sizeof(buffer),fmainptr);
        if (bytecount > 0)
        {
            crc32e_calc = crc32e_calculateblock(crc32e_calc,
                                            (u32*)buffer,bytecount/4);
            if (fwrite((char*)buffer,1,bytecount,fmergeptr) != bytecount)
            {
                log_push_error_point(0x45D5);
                status = S_WRITEFILE;
                goto obd2tune_mergeoptionfile_done;
            }
        }
    }

    //add sub records to merge file
    while(!feof(fsubptr))
    {
        bytecount = fread((char*)buffer,1,sizeof(buffer),fsubptr);
        if (bytecount > 0)
        {
            crc32e_calc = crc32e_calculateblock(crc32e_calc,
                                            (u32*)buffer,bytecount/4);
            if (fwrite((char*)buffer,1,bytecount,fmergeptr) != bytecount)
            {
                log_push_error_point(0x45D5);
                status = S_WRITEFILE;
                goto obd2tune_mergeoptionfile_done;
            }
        }
    }

    //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    // Finalize merge option file header
    mergeHeader.content_crc32e = crc32e_calc;
    
    crc32e_reset();
    crc32e_calc = crc32e_calculateblock(0xFFFFFFFF,(u32*)&mergeHeader,1);
    crc32e_calc = crc32e_calculateblock(crc32e_calc,(u32*)&mergeHeader.content_crc32e,
                                        (sizeof(ophOptionFileHeader)-8)/4);
    mergeHeader.header_crc32e = crc32e_calc;
    if (fseek(fmergeptr,0,F_SEEK_SET) != 0)
    {
        log_push_error_point(0x45D6);
        status = S_SEEKFILE;
        goto obd2tune_mergeoptionfile_done;
    }
    bytecount = fwrite((char*)&mergeHeader,1,sizeof(mergeHeader),fmergeptr);
    if (bytecount != sizeof(mergeHeader))
    {
        log_push_error_point(0x45D7);
        status = S_WRITEFILE;
        goto obd2tune_mergeoptionfile_done;
    }

    status = S_SUCCESS;
    
obd2tune_mergeoptionfile_done:
    if (fmergeptr)
    {
        genfs_closefile(fmergeptr);
    }
    if (fmainptr)
    {
        genfs_closefile(fmainptr);
    }
    if (fsubptr)
    {
        genfs_closefile(fsubptr);
    }
    
    return status;
}

//------------------------------------------------------------------------------
// Read glf header
// Inputs:  F_FILE *sth_fptr
// Outputs: GeneralFileLookupHeader *header (unencrypted)
//          u32 *header_length (NULL allowed)
// Return:  u8  status
// Engineer: Quyen Leba
//------------------------------------------------------------------------------
u8 obd2tune_read_glf_header(F_FILE *glf_fptr,
                            GeneralFileLookupHeader *header, u32 *header_length)
{
    u32 bytecount;

    if (header_length)
    {
        *header_length = 0;
    }

    bytecount = fread(header,1,sizeof(GeneralFileLookupHeader),glf_fptr);
    if (bytecount != sizeof(GeneralFileLookupHeader))
    {
        log_push_error_point(0x45E2);
        return S_READFILE;
    }
#if GLF_ENCRYPTION_ENABLED
    crypto_blowfish_decryptblock_critical((u8*)header,
                                          sizeof(GeneralFileLookupHeader));
#else
            //do nothing
#endif
    if (header_length)
    {
        *header_length = header->header_length;
    }

    return S_SUCCESS;
}

//------------------------------------------------------------------------------
// Validate header of a glf (generic lookup file)
// Input:   const GeneralFileLookupHeader *header (unencrypted)
// Return:  u8  status
// Engineer: Quyen Leba
//------------------------------------------------------------------------------
u8 obd2tune_validate_glf_header(GeneralFileLookupHeader *header)
{
    u32 cmp_crc32e;
    u32 calc_crc32e;
    u8  status;

    if (header->version != CURRENT_GLF_VERSION)
    {
        log_push_error_point(0x45F0);
        return S_NOTSUPPORT;
    }
    else if (header->header_length != sizeof(GeneralFileLookupHeader))
    {
        log_push_error_point(0x45DE);
        return S_BADCONTENT;
    }

    cmp_crc32e = header->header_crc32e;
    header->header_crc32e = 0xFFFFFFFF;
    crc32e_reset();
    calc_crc32e = crc32e_calculateblock(0xFFFFFFFF,(u32*)header,
                                        sizeof(GeneralFileLookupHeader)/4);
    header->header_crc32e = cmp_crc32e;
    if (cmp_crc32e != calc_crc32e)
    {
        log_push_error_point(0x45DF);
        return S_CRC32E;
    }

    if (header->devicepartnumberstring[0])
    {
        status = settings_compare_devicepartnumberstring(header->devicepartnumberstring);
        if (status != S_SUCCESS)
        {
            log_push_error_point(0x45E0);
            return status;
        }
    }

    if (header->serialnumber[0])
    {
        status = settings_compare_deviceserialnumber(header->serialnumber);
        if (status != S_SUCCESS)
        {
            log_push_error_point(0x45E1);
            return status;
        }
    }

    //TODOQK: check hardware

    if (header->device_market != MarketType_Unknown &&
        header->device_market != MarketType_Any &&
        header->device_market != SETTINGS_GetMarketType())
    {
        log_push_error_point(0x45E3);
        return S_MARKETTYPE;
    }

    if (header->device_type != DeviceType_Unknown &&
        header->device_type != DeviceType_Any &&
        header->device_type != SETTINGS_GetDeviceType())
    {
        log_push_error_point(0x45E4);
        return S_DEVICETYPE;
    }

    return S_SUCCESS;
}

//------------------------------------------------------------------------------
// Validate whole glf file (generic lookup file)
// Input:   const u8 *filename
// Return:  u8  status
// Engineer: Quyen Leba
//------------------------------------------------------------------------------
u8 obd2tune_validate_glf_file(const u8 *filename)
{
#define VALIDATE_GLF_FILE_BUFFER_SIZE   4096
    GeneralFileLookupHeader glf_header;
    F_FILE *fptr;
    u8  *buffer;
    u32 bytecount;
    u32 crc32e_calc;
    u8  status;

    fptr = NULL;
    status = S_SUCCESS;

    buffer = __malloc(VALIDATE_GLF_FILE_BUFFER_SIZE);
    if (buffer == NULL)
    {
        log_push_error_point(0x45E5);
        status = S_MALLOC;
        goto obd2tune_validate_glf_file_done;
    }
    fptr = genfs_default_openfile((u8*)filename,"r");
    if (fptr == NULL)
    {
        log_push_error_point(0x45E6);
        status = S_OPENFILE;
        goto obd2tune_validate_glf_file_done;
    }
    status = obd2tune_read_glf_header(fptr,&glf_header,NULL);
    if (status != S_SUCCESS)
    {
        log_push_error_point(0x45E7);
        status = S_READFILE;
        goto obd2tune_validate_glf_file_done;
    }

    status = obd2tune_validate_glf_header(&glf_header);
    if (status != S_SUCCESS)
    {
        log_push_error_point(0x45E8);
        status = S_READFILE;
        goto obd2tune_validate_glf_file_done;
    }
    crc32e_reset();
    crc32e_calc = 0xFFFFFFFF;
    while(!feof(fptr))
    {
        bytecount = fread((char*)buffer,1,VALIDATE_GLF_FILE_BUFFER_SIZE,fptr);
        crc32e_calc = crc32e_calculateblock(crc32e_calc,(u32*)buffer,bytecount/4);    
    }
    if (glf_header.content_crc32e != crc32e_calc)
    {
        log_push_error_point(0x45E9);
        status = S_CRC32E;
        goto obd2tune_validate_glf_file_done;
    }

obd2tune_validate_glf_file_done:
    if (buffer)
    {
        __free(buffer);
    }
    if (fptr)
    {
        genfs_closefile(fptr);
    }
    return status;
}

//------------------------------------------------------------------------------
// Search glf for special option file
// Inputs:  const u8 *glf_filename (glf; may include path)
//          const u8 *vin (maskable w/ '*')
//          const u8 *processor_code
//          GeneralFileLookupSpecialOptionFileProcessorCodeType code_type
// Outputs: u8  *special_option_filename (store 32 bytes including NULL)
//          u32 *flags (for this special option file)
// Return:  u8  status (S_NOTREQUIRED: lookup file not exist, no options needed)
// Engineer: Quyen Leba
//------------------------------------------------------------------------------
u8 obd2tune_search_glf_for_special_option_file(const u8 *glf_filename,
                                               const u8 *vin,
                                               const u8 *processor_code,
                                               GeneralFileLookupFileProcessorCodeType code_type,
                                               u8 *special_option_filename, u32 *flags)
{
    F_FILE *fptr;
    GeneralFileLookupHeader glf_header;
    GeneralFileLookupGenericItem glf_item;
    GeneralFileLookupSpecialOptionFile *glf_item_ptr;
    ophOptionFileHeader oph_header;
    u32 bytecount;
    u8  status;

    special_option_filename[0] = NULL;
    *flags = GeneralFileLookupSpecialOptionFileFlags_None;
    fptr = NULL;

    status = obd2tune_validate_glf_file(glf_filename);
    if (status == S_OPENFILE)
    {
        log_push_error_point(0x45EF);
        return S_NOTREQUIRED;
    }
    else if (status != S_SUCCESS)
    {
        log_push_error_point(0x45EA);
        return status;
    }

    fptr = genfs_general_openfile(glf_filename,"r");
    if (fptr == NULL)
    {
        log_push_error_point(0x45EB);
        status = S_NOLOOKUP;
        goto obd2tune_search_glf_for_special_option_file_done;
    }

    status = obd2tune_read_glf_header(fptr,&glf_header,NULL);
    if (status != S_SUCCESS)
    {
        log_push_error_point(0x45EC);
        goto obd2tune_search_glf_for_special_option_file_done;
    }

    status = S_FILENOTFOUND;
    glf_item_ptr = (GeneralFileLookupSpecialOptionFile*)&glf_item;
    while(!feof(fptr))
    {
        bytecount = fread((char*)&glf_item,
                          1,sizeof(GeneralFileLookupGenericItem),fptr);
        if (bytecount != sizeof(GeneralFileLookupGenericItem))
        {
            log_push_error_point(0x45ED);
            status = S_READFILE;
            goto obd2tune_search_glf_for_special_option_file_done;
        }
#if GLF_ENCRYPTION_ENABLED
        crypto_blowfish_decryptblock_critical((u8*)&glf_item,
                                              sizeof(GeneralFileLookupGenericItem));
#endif
        if (glf_item.type == GeneralFileLookupItemType_SpecialOptionFile)
        {
            //TODOQK: include second codes

//            if (strncmp((char*)processor_code,(char*)glf_item_ptr->processor_code,
//                        strlen((char*)glf_item_ptr->processor_code)) != 0)
            if (obd2tune_maskablestringncompare(glf_item_ptr->processor_code,
                                                processor_code,TUNE_MASK_BYTE,
                                                strlen((char*)glf_item_ptr->processor_code)) != S_SUCCESS)
            {
                continue;   //not matched
            }
            if (!obd2tune_isvin_matched(glf_item_ptr->vin,vin,VIN_MASK_BYTE))
            {
                continue;   //not matched
            }

            //found it
            if (strlen((char*)glf_item_ptr->special_option_filename) >= sizeof(glf_item_ptr->special_option_filename))
            {
                status = S_BADCONTENT;
                log_push_error_point(0x45EE);
                goto obd2tune_search_glf_for_special_option_file_done;
            }

            strcpy((char*)special_option_filename,
                   (char*)glf_item_ptr->special_option_filename);

            genfs_closefile(fptr);

            //verify this option file (quick check: special_optionfile_headercrc32e against oph header_crc32e)
            //will verify it later with actual crc32e calculation
            fptr = genfs_general_openfile(special_option_filename,"r");
            if (fptr == NULL)
            {
                status = S_OPENFILE;
                log_push_error_point(0x45F1);
                goto obd2tune_search_glf_for_special_option_file_done;
            }

            bytecount = fread((char*)&oph_header,
                              1,sizeof(ophOptionFileHeader),fptr);
            if (bytecount != sizeof(ophOptionFileHeader))
            {
                status = S_READFILE;
                log_push_error_point(0x45F2);
                goto obd2tune_search_glf_for_special_option_file_done;
            }
            else if (oph_header.header_crc32e != glf_item_ptr->special_optionfile_headercrc32e)
            {
                status = S_CRC32E;
                log_push_error_point(0x45F3);
                goto obd2tune_search_glf_for_special_option_file_done;
            }

            *flags = glf_item_ptr->flags;
            status = S_SUCCESS;
            break;
        }
    }//while(!f_eof(fptr))...

obd2tune_search_glf_for_special_option_file_done:
    if (fptr)
    {
        genfs_closefile(fptr);
    }
    return status;
}


/**
 *  obd2tune_search_glf_for_otf_file
 *  
 *  @brief Search GLF file for preloaded OTF files
 *  
 *  @param [in] glf_filename
 *  @param [in] vin
 *  @param [in] processor_code
 *  @param [in] code_type
 *  @param [out] filename
 *  @param [out] description
 *  @param [out] flags
 *
 *  @retval u8  status (S_NOTREQUIRED: lookup file not exist, no options needed)
 *  
 *  @details Details
 *  
 *  @authors Patrick Downs
 */
u8 obd2tune_search_glf_for_otf_file(const u8 *glf_filename,
                                               const u8 *vin,
                                               const u8 *processor_code,
                                               GeneralFileLookupFileProcessorCodeType code_type,
                                               u8 *filename,
                                               u8 *description,
                                               u32 *flags)
{
    F_FILE *fptr;
    GeneralFileLookupHeader glf_header;
    GeneralFileLookupGenericItem glf_item;
    GeneralFileLookupOtfPreloadFile *glf_item_ptr;
    sthFileHeader sth_header;
    u32 bytecount;
    u8  status;

    filename[0] = NULL;
    *flags = GeneralFileLookupOtfPreloadFileFlags_None;
    fptr = NULL;

    status = obd2tune_validate_glf_file(glf_filename);
    if(status == S_SUCCESS)
    {
        fptr = genfs_general_openfile(glf_filename,"r");
        if (fptr)
        {
            status = obd2tune_read_glf_header(fptr,&glf_header,NULL);
            if (status == S_SUCCESS)
            {
                status = S_FILENOTFOUND;
                glf_item_ptr = (GeneralFileLookupOtfPreloadFile*)&glf_item;
                while(!feof(fptr))
                {
                    bytecount = fread((char*)&glf_item,
                                      1,sizeof(GeneralFileLookupGenericItem),fptr);
                    if (bytecount == sizeof(GeneralFileLookupGenericItem))
                    {
#if GLF_ENCRYPTION_ENABLED
                        crypto_blowfish_decryptblock_critical((u8*)&glf_item,
                                                              sizeof(GeneralFileLookupGenericItem));
#endif
                        if (glf_item.type == GeneralFileLookupItemType_OtfPreloadFile)
                        {
                            if (obd2tune_maskablestringncompare(glf_item_ptr->processor_code,
                                                                processor_code,TUNE_MASK_BYTE,
                                                                strlen((char*)glf_item_ptr->processor_code)) != S_SUCCESS)
                            {
                                continue;   //not matched
                            }
                            if (!obd2tune_isvin_matched(glf_item_ptr->vin,vin,VIN_MASK_BYTE))
                            {
                                continue;   //not matched
                            }
                            
                            //found it
                            if (strlen((char*)glf_item_ptr->otf_preload_filename) >= sizeof(glf_item_ptr->otf_preload_filename))
                            {
                                status = S_BADCONTENT;
                                log_push_error_point(0x45F9);
                                break;
                            }
                            
                            strcpy((char*)filename,
                                   (char*)glf_item_ptr->otf_preload_filename);
                            
                            genfs_closefile(fptr);
                            
                            //verify this STH file (quick check: sth_header.generic.header_crc32e against oph header_crc32e)
                            //will verify it later with actual crc32e calculation
                            fptr = genfs_general_openfile(glf_item_ptr->otf_preload_filename,"r");
                            if (fptr == NULL)
                            {
                                status = S_OPENFILE;
                                log_push_error_point(0x45FA);
                                break;
                            }
                            
                            bytecount = fread((char*)&sth_header,
                                              1,sizeof(sthFileHeader),fptr);
                            if (bytecount == sizeof(sthFileHeader))
                            {
//#if STH_OPH_ENCRYPTION_ENABLED
//                                crypto_blowfish_decryptblock_critical((u8*)&sth_header,
//                                                                      sizeof(sthFileHeader));
//#else
//                                //do nothing
//#endif                       
//                                if (sth_header.generic.header_crc32e != glf_item_ptr->otf_preload_headercrc32e)
//                                {
//                                    status = S_CRC32E;
//                                    log_push_error_point(0x45FC);
//                                    break;
//                                }
                            }
                            else
                            {
                                status = S_READFILE;
                                log_push_error_point(0x45FB);
                                break;
                            }
                            
                            strcpy((char*)description, (char const*)glf_item_ptr->otf_preload_description);
                            *flags = glf_item_ptr->flags;
                            status = S_SUCCESS;
                            break;
                        }
                    }
                    else
                    {
                        log_push_error_point(0x45F8);
                        status = S_READFILE;
                        break;
                    }
                }//while(!f_eof(fptr))...
            }
            else
            {
                log_push_error_point(0x45F7);
            }
            if(fptr)
            {
                genfs_closefile(fptr);
            }
        }
        else 
        {
            log_push_error_point(0x45F6);
            status = S_NOLOOKUP;
        }
    }
    else if (status == S_OPENFILE)
    {
        log_push_error_point(0x45F4);
        status = S_NOTREQUIRED;
    }
    else
    {
        log_push_error_point(0x45F5);
    }

    return status;
}

//------------------------------------------------------------------------------
// Read preload tune index (binary type) from a preloaded tune file
// Input:   u8  *tunefilename
// Output:  u16 *preloadedtuneindex
// Return:  u8  status
//------------------------------------------------------------------------------
u8 obd2tune_read_preloadedtune_index_from_preloadedtunefile(u8 *tunefilename,
                                                            u16 *preloadedtuneindex)
{
    F_FILE  *file;
    u8      filetype;
    u16      tuneindex;
    u8      status;

    *preloadedtuneindex = 0xFFFF;
    file = genfs_default_openfile((void*)tunefilename, "rb");
    if(file == NULL)
    {
        log_push_error_point(0x4827);
        return S_OPENFILE;
    }

    status = S_SUCCESS;
    if (strstr((char*)tunefilename,".sth"))
    {
        sthFileHeader sth_header;

        status = obd2tune_read_sth_header(file,&sth_header,NULL);
        if (status != S_SUCCESS)
        {
            log_push_error_point(0x4850);
            status = S_READFILE;
            goto obd2tune_read_preloadedtune_index_from_preloadedtunefile_Done;
        }

        status = obd2tune_validate_sth_header(&sth_header);
        if (status != S_SUCCESS)
        {
            log_push_error_point(0x484F);
            goto obd2tune_read_preloadedtune_index_from_preloadedtunefile_Done;
        }
        tuneindex = sth_header.generic.ecm_type;
    }
    else
    {
        if((fread(&filetype,sizeof(filetype),1,file)) < 1) // Read file type
        {
            log_push_error_point(0x4828);
            status = S_READFILE;
            goto obd2tune_read_preloadedtune_index_from_preloadedtunefile_Done;
        }
        if((fseek(file, 5, SEEK_SET)) != 0)
        {
            log_push_error_point(0x4829);
            status = S_SEEKFILE;
            goto obd2tune_read_preloadedtune_index_from_preloadedtunefile_Done;
        }
    
        if(fread(&tuneindex, 1,sizeof(u8), file) < 1)
        {
            log_push_error_point(0x482A);
            status = S_READFILE;
            goto obd2tune_read_preloadedtune_index_from_preloadedtunefile_Done;
        }
    }
    *preloadedtuneindex = tuneindex;

obd2tune_read_preloadedtune_index_from_preloadedtunefile_Done:
    genfs_closefile(file);
    return status;
}

//------------------------------------------------------------------------------
// Attempt to find vehicle type (PCM or TCM; not stacked) using tunecode
// veh_type
// Inputs:  u8  *tunecode (PCM strategy, OSpart#, etc)
//          u8  *vin
// Output:  u16 *veh_type
// Return:  u8  status (S_UNCOMPATIBLE: using wrong tune on vehicle)
// Engineer: Quyen Leba
//------------------------------------------------------------------------------
u8 obd2tune_attempt_to_find_vehicle_type_using_tunecode(u8 *tunecode,
                                                        u8 *vin,
                                                        u16 *veh_type)
{
    u16 veh_type_found;
    u8  status;

    /* Do we already have vehicle type? */
    if (gObd2info.status == S_OBD2_CONNECTED && gObd2info.vehicle_type != INVALID_VEH_DEF)
    {
        *veh_type = gObd2info.vehicle_type;
    }
    /* If not, scan for vehicle type and validate */
    else
    {
        *veh_type = INVALID_VEH_DEF;
        status = S_FAIL;
        status = obd2tune_attempt_to_validate_vehicle_type_against_tunecode(INVALID_VEH_DEF,
                                                                            tunecode,vin,&veh_type_found);
        if (status == S_UNCOMPATIBLE)
        {
            *veh_type = veh_type_found;
            status = S_SUCCESS;
        }
    }

    return status;
}

//------------------------------------------------------------------------------
// Search for supported preloaded tune to get vehicle type and compare it with
// veh_type
// Inputs:  u16 veh_type
//          u8  *tunecode (PCM strategy, OSpart#, etc)
//          u8  *vin
// Output:  u16 *veh_type_found (NULL allowed; the actual vehicle type found)
// Return:  u8  status (S_UNCOMPATIBLE: using wrong tune on vehicle)
// Engineer: Quyen Leba
//------------------------------------------------------------------------------
u8 obd2tune_attempt_to_validate_vehicle_type_against_tunecode(u16 veh_type,
                                                              u8 *tunecode,
                                                              u8 *vin,
                                                              u16 *veh_type_found)
{
    tunecode_infolist *tunecodeinfolist;
    u8  buffer[64];
    u16 preloadedtune_index;
    u8  status;

    if (veh_type_found)
    {
        *veh_type_found = INVALID_VEH_DEF;
    }

    status = S_FAIL;
    if ((strlen((char*)tunecode)+strlen(TUNECODE_SEPARATOR_STRING)) >= sizeof(buffer))
    {
        log_push_error_point(0x4640);
        return S_INPUT;
    }

    tunecodeinfolist = __malloc(sizeof(tunecode_infolist));
    if (tunecodeinfolist == NULL)
    {
        log_push_error_point(0x4641);
        return S_MALLOC;
    }

    strcpy((char*)buffer,(char*)tunecode);
    strcat((char*)buffer,TUNECODE_SEPARATOR_STRING);
    status = obd2tune_gettunecodelistinfo(buffer,vin,tunecodeinfolist);
    if (status != S_SUCCESS)
    {
        log_push_error_point(0x4642);
        goto obd2tune_attempt_to_validate_vehicle_type_against_tunecode_done;
    }

    if (strlen((char*)tunecodeinfolist->tunefilename[0]) >= sizeof(buffer))
    {
        log_push_error_point(0x4643);
        goto obd2tune_attempt_to_validate_vehicle_type_against_tunecode_done;
    }
    strcpy((char*)buffer,(char*)tunecodeinfolist->tunefilename[0]);
    status = file_default_getfilename(buffer,tunecodeinfolist->tunefilename[0]);
    if (status != S_SUCCESS)
    {
        log_push_error_point(0x4644);
        goto obd2tune_attempt_to_validate_vehicle_type_against_tunecode_done;
    }

    status = obd2tune_read_preloadedtune_index_from_preloadedtunefile(tunecodeinfolist->tunefilename[0],
                                                                      &preloadedtune_index);
    if (status != S_SUCCESS)
    {
        log_push_error_point(0x4645);
        goto obd2tune_attempt_to_validate_vehicle_type_against_tunecode_done;
    }

    if (veh_type == INVALID_VEH_DEF)
    {
        //can't be compatible; probably trying to find vehicle type instead
        status = S_UNCOMPATIBLE;
        if (veh_type_found)
        {
            *veh_type_found = preloadedtune_index;
        }
    }
    else if (VEH_GetMainEcmType(veh_type) != VEH_GetMainEcmType(preloadedtune_index))
    {
        //trying to use wrong tune on the vehicle
        status = S_UNCOMPATIBLE;
        if (veh_type_found)
        {
            *veh_type_found = preloadedtune_index;
        }
    }

obd2tune_attempt_to_validate_vehicle_type_against_tunecode_done:
    if (tunecodeinfolist)
    {
        __free(tunecodeinfolist);
    }
    return status;
}

//------------------------------------------------------------------------------
// Search for special option file for custom tune
// Inputs:  u16 veh_type (not used)
//          const u8 *tuneos_filename
//          const u8 *vin
// Outputs: u8 *special_option_filename_pcm (32 bytes including NULL, with path)
//          u32 *special_option_flags_pcm
//          u8 *special_option_filename_tcm (32 bytes including NULL, with path)
//          u32 *special_option_flags_tcm
// Return:  u8  status (S_SUCCESS: found, S_UNMATCH: not found)
// Engineer: Quyen Leba
// Note: assume to use this function after obd2tune_check_customtune_support(...)
// therefore, some information are already validated
//------------------------------------------------------------------------------
u8 obd2tune_search_customtune_specialoptionfile_with_tuneos(u16 veh_type,
                                                            const u8 *tuneos_filename,
                                                            const u8 *vin,
                                                            u8 *special_option_filename_pcm, u32 *special_option_flags_pcm,
                                                            u8 *special_option_filename_tcm, u32 *special_option_flags_tcm)
{
    customtune_tuneosinfoblock *tuneosinfoblock_ptr;
    F_FILE *fptr;
    u32 bytecount;
    u8  tmpBuffer[64];
    bool pcmoptionfound;
    bool tcmoptionfound;
    u8  status;

    special_option_filename_pcm[0] = NULL;
    special_option_filename_pcm[0] = NULL;
    tuneosinfoblock_ptr = NULL;
    fptr = NULL;

    if (strstr((char*)tuneos_filename,".tos") == NULL)
    {
        log_push_error_point(0x4646);
        status = S_INPUT;
        goto obd2tune_search_customtune_specialoptionfile_with_tuneos_done;
    }
    
    tuneosinfoblock_ptr = __malloc(sizeof(customtune_tuneosinfoblock));
    if (tuneosinfoblock_ptr == NULL)
    {
        log_push_error_point(0x4647);
        status = S_MALLOC;
        goto obd2tune_search_customtune_specialoptionfile_with_tuneos_done;
    }

    fptr = genfs_user_openfile(tuneos_filename,"r");
    if (!fptr)
    {
        log_push_error_point(0x4648);
        status = S_OPENFILE;
        goto obd2tune_search_customtune_specialoptionfile_with_tuneos_done;
    }
    if (fseek(fptr, sizeof(customtune_tuneosheader), F_SEEK_SET) != 0)
    {
        log_push_error_point(0x4649);
        status = S_SEEKFILE;
        goto obd2tune_search_customtune_specialoptionfile_with_tuneos_done;
    }

    while(!feof(fptr))
    {
        bytecount = fread((char*)tuneosinfoblock_ptr,1,sizeof(customtune_tuneosinfoblock),fptr);
        if (bytecount == sizeof(customtune_tuneosinfoblock))
        {
            crypto_blowfish_decryptblock_critical((u8*)tuneosinfoblock_ptr,
                                                  sizeof(customtune_tuneosinfoblock));

            pcmoptionfound = FALSE;
            tcmoptionfound = FALSE;

            //search with vehicle codes
            status = obd2tune_search_glf_for_special_option_file(DEFAULT_OPTION_LOOKUP_FILE,
                                                                 vin,
                                                                 tuneosinfoblock_ptr->codes[0][0],
                                                                 GeneralFileLookupFileProcessorCodeType_Regular,
                                                                 tmpBuffer,special_option_flags_pcm);
            if (status == S_NOTREQUIRED)
            {
                goto obd2tune_search_customtune_specialoptionfile_with_tuneos_done;
            }
            else if (status != S_SUCCESS)
            {
                //search with vehicle second_codes (because nothing found for codes)
                status = obd2tune_search_glf_for_special_option_file(DEFAULT_OPTION_LOOKUP_FILE,
                                                                     vin,
                                                                     tuneosinfoblock_ptr->second_codes[0][0],
                                                                     GeneralFileLookupFileProcessorCodeType_SecondCode,
                                                                     tmpBuffer,special_option_flags_pcm);
            }
            if (status == S_SUCCESS)
            {
                pcmoptionfound = TRUE;
                genfs_generalpathcorrection(tmpBuffer,32-1,special_option_filename_pcm);
            }

            if (tuneosinfoblock_ptr->ecm_count > 1)
            {
                //search with vehicle codes
                status = obd2tune_search_glf_for_special_option_file(DEFAULT_OPTION_LOOKUP_FILE,
                                                                     vin,
                                                                     tuneosinfoblock_ptr->codes[1][0],
                                                                     GeneralFileLookupFileProcessorCodeType_Regular,
                                                                     special_option_filename_tcm,special_option_flags_tcm);
                if (status != S_SUCCESS)
                {
                    //search with vehicle second_codes (because nothing found for codes)
                    status = obd2tune_search_glf_for_special_option_file(DEFAULT_OPTION_LOOKUP_FILE,
                                                                         vin,
                                                                         tuneosinfoblock_ptr->second_codes[1][0],
                                                                         GeneralFileLookupFileProcessorCodeType_SecondCode,
                                                                         special_option_filename_tcm,special_option_flags_tcm);
                    if (status != S_SUCCESS)
                    {
                        special_option_filename_tcm[0] = NULL;
                    }
                }
                if (status == S_SUCCESS)
                {
                    tcmoptionfound = TRUE;
                    genfs_generalpathcorrection(tmpBuffer,32-1,special_option_filename_tcm);
                }
            }//if (tuneosinfoblock_ptr->ecm_count > 1)...
        }//if (bytecount ...

        if (pcmoptionfound || tcmoptionfound)
        {
            status = S_SUCCESS;
            break;
        }
    }//while(!feof(fptr))...

    if (!pcmoptionfound && !tcmoptionfound)
    {
        status = S_UNMATCH;
    }

obd2tune_search_customtune_specialoptionfile_with_tuneos_done:
    if (tuneosinfoblock_ptr)
    {
        __free(tuneosinfoblock_ptr);
    }
    if (fptr)
    {
        genfs_closefile(fptr);
    }
    return status;
}

//------------------------------------------------------------------------------
// Upload E2 data
// Inputs:  u32 ecm_type
// Return:  u8  status 
// Engineer: Ricardo Cigarroa
//------------------------------------------------------------------------------
u8 obd2tune_upload_e2_data(u32 ecm_type)
{    
    u8 status;  
    VehicleCommType commtype;
    
    commtype = ECM_GetCommType(ecm_type);
    
    if(SETTINGS_IsDemoMode())
    {
        return S_SUCCESS; 
    }
    
    switch (obd2_get_oemtype())
    {
#ifdef __DCX_MANUF__        
    case OemType_DCX:
        switch(commtype)
        {
        case CommType_CAN:
            status = obd2can_dcx_upload_e2_data(ecm_type);
            break; 
        default:
            status = S_COMMTYPE; 
            break;
        }
        break;
#endif
#ifdef __FORD_MANUF__
    case OemType_FORD:
        if(commtype != CommType_Unknown)
        {
            status = S_SUCCESS; 
        }
        else
        {
            status = S_COMMTYPE; 
        }
        break;
#endif
#ifdef __GM_MANUF__
    case OemType_GM:
        if(commtype != CommType_Unknown)
        {
            status = S_SUCCESS; 
        }
        else
        {
            status = S_COMMTYPE; 
        }
        break;
#endif
    default:
        status = S_NOTSUPPORT;
        break;
    }  
    
    if(status != S_SUCCESS)
    {
        log_push_error_point(0x425B); 
    }
    
    return status;
}

//------------------------------------------------------------------------------
//Executes any special pre-download task before downloading tune
//Input:  u32 ecm_type
//        u8 flash_type
//        bool is_cal_only
//Return: u8 status
//Engineer: Ricardo Cigarroa
//------------------------------------------------------------------------------   
u8 obd2tune_preflash_task(u32 ecm_type, u8 flash_type, bool is_cal_only)
{    
    u8 status; 
    VehicleCommType commtype;
    
    commtype = ECM_GetCommType(ecm_type);
    
    switch (obd2_get_oemtype())
    {
#ifdef __DCX_MANUF__        
    case OemType_DCX:
        switch(commtype)
        {
        case CommType_CAN:
            status = obd2can_dcx_preflash_task(ecm_type, flash_type, is_cal_only);
            if(status != S_SUCCESS)
            {
                log_push_error_point(0x425C);
            }
            break;
        default:
            status = S_COMMTYPE; 
            break;
        }
        break;
#endif
#ifdef __FORD_MANUF__
    case OemType_FORD:
        if(commtype != CommType_Unknown)
        {
            status = S_SUCCESS; 
        }
        else
        {
            status = S_COMMTYPE; 
        }
        break;
#endif
#ifdef __GM_MANUF__
    case OemType_GM:
        if(commtype != CommType_Unknown)
        {
            status = S_SUCCESS; 
        }
        else
        {
            status = S_COMMTYPE; 
        }
        break;
#endif
    default:
        status = S_NOTSUPPORT;
        break;
    }  
    
    if(status != S_SUCCESS)
    {
        log_push_error_point(0x4258); 
    }
    
    return status;
}

//------------------------------------------------------------------------------
//Finds out if a cal only flash is required. 
//Input:    flasher_info *flasherinfo
//          VehicleCommType vehiclecommtype
//          u16 ecm_type
//return:  u8 status
//Engineer: Ricardo Cigarroa
//------------------------------------------------------------------------------ 
u8 obd2tune_check_variant_id(u16 ecm_type, bool *variant_id, 
                             VehicleCommType vehiclecommtype)
{
    u8 status;  
    
    switch (obd2_get_oemtype())
    {
#ifdef __DCX_MANUF__        
    case OemType_DCX:
        switch(vehiclecommtype)
        {
        case CommType_CAN:
            status = obd2tune_dcx_check_variant_id(variant_id); 
            break; 
        default:
            status = S_COMMTYPE; 
            break;
        }
        break;
#endif
#ifdef __FORD_MANUF__
    case OemType_FORD:
        if(vehiclecommtype != CommType_Unknown)
        {
            status = S_SUCCESS; 
        }
        else
        {
            status = S_COMMTYPE; 
        }
        break;
#endif
#ifdef __GM_MANUF__
    case OemType_GM:
        if(vehiclecommtype != CommType_Unknown)
        {
            status = S_SUCCESS; 
        }
        else
        {
            status = S_COMMTYPE; 
        }
        break;
#endif
    default:
        status = S_NOTSUPPORT;
        break;
    }  
    
    if(status != S_SUCCESS)
    {
        log_push_error_point(0x4259); 
    }
    
    return status;
}

//------------------------------------------------------------------------------
//Download RSA signature
//Input: u32 ecm_type
//       u8  block_index
//       BlockType  block_type
//Engineer: Ricardo Cigarroa
//------------------------------------------------------------------------------   
u8 obd2tune_download_rsa_signature(u32 ecm_type, u8 block_index, BlockType block_type)
{    
    u8 status; 
    VehicleCommType commtype;
    
    if(SETTINGS_IsDemoMode())
    {
        return S_SUCCESS; 
    }
    
    commtype = ECM_GetCommType(ecm_type);
    
    switch (obd2_get_oemtype())
    {
#ifdef __DCX_MANUF__        
    case OemType_DCX:
        switch(commtype)
        {
        case CommType_CAN:
            status = obd2can_dcx_download_rsa_signature(ecm_type, block_index, block_type);
            if(status != S_SUCCESS)
            {
                log_push_error_point(0x425E);
            }
            break;
        default:
            status = S_COMMTYPE; 
            break;
        }
        break;
#endif
#ifdef __FORD_MANUF__
    case OemType_FORD:
        if(commtype != CommType_Unknown)
        {
            status = S_SUCCESS; 
        }
        else
        {
            status = S_COMMTYPE; 
        }
        break;
#endif
#ifdef __GM_MANUF__
    case OemType_GM:
        if(commtype != CommType_Unknown)
        {
            status = S_SUCCESS; 
        }
        else
        {
            status = S_COMMTYPE; 
        }
        break;
#endif
    default:
        status = S_NOTSUPPORT;
        break;
    }  
    
    if(status != S_SUCCESS)
    {
        log_push_error_point(0x425A); 
    }
    
    return status;
}

//------------------------------------------------------------------------------
//Check and Download transinfo if neccessary
//Input:  u32 ecm_type
//Return: u8 status
//Engineer: Tristen Pierson
//------------------------------------------------------------------------------  
u8 obd2tune_download_trans_info(u32 ecm_type)
{   
    u8 status; 
    VehicleCommType commtype;
    
    if(SETTINGS_IsDemoMode())
    {
        return S_SUCCESS; 
    }
    
    commtype = ECM_GetCommType(ecm_type);
    
    switch (obd2_get_oemtype())
    {
#ifdef __DCX_MANUF__        
    case OemType_DCX:
        switch(commtype)
        {
        case CommType_CAN:
            status = obd2can_dcx_download_trans_info(ecm_type);
            if(status != S_SUCCESS)
            {
                log_push_error_point(0x425F);
            }
            break;
        default:
            status = S_COMMTYPE; 
            break;
        }
        break;
#endif
#ifdef __FORD_MANUF__
    case OemType_FORD:
        if(commtype != CommType_Unknown)
        {
            status = S_SUCCESS; 
        }
        else
        {
            status = S_COMMTYPE; 
        }
        break;
#endif
#ifdef __GM_MANUF__
    case OemType_GM:
        if(commtype != CommType_Unknown)
        {
            status = S_SUCCESS; 
        }
        else
        {
            status = S_COMMTYPE; 
        }
        break;
#endif
    default:
        status = S_NOTSUPPORT;
        break;
    }  
    
    if(status != S_SUCCESS)
    {
        log_push_error_point(0x4260); 
    }
    
    return status;
}

//------------------------------------------------------------------------------
//Does a check before programming(ex: bootloader hash value for dcx)
//Input: u8 *response
//Return: u8 status; 
//Engineer: Ricardo Cigarroa
//------------------------------------------------------------------------------   
u8 obd2tune_do_check(u8 *response, u16 vehicle_type, bool checksupportfiles)
{
    u8 status; 
    u32 ecm_type; 
    VehicleCommType commtype;
    
    if(SETTINGS_IsDemoMode())
    {
        return S_SUCCESS; 
    }
    
    /*TODO: use gObd2info in order to get the vehicle index. This will be 
    important for when flashing more than 1 processor. */
    ecm_type = VEH_GetEcmType(vehicle_type,0);
    
    commtype = ECM_GetCommType(ecm_type);
    
    switch (obd2_get_oemtype())
    {
#ifdef __DCX_MANUF__        
    case OemType_DCX:
        switch(commtype)
        {
        case CommType_CAN:
            status = obd2can_dcx_do_check(response,checksupportfiles,ecm_type);
            break; 
        default:
            status = S_COMMTYPE; 
            break;
        }
        break;
#endif
#ifdef __FORD_MANUF__
    case OemType_FORD:
        if(commtype != CommType_Unknown)
        {
            status = S_SUCCESS; 
        }
        else
        {
            status = S_COMMTYPE; 
        }
        break;
#endif
#ifdef __GM_MANUF__
    case OemType_GM:
        if(commtype != CommType_Unknown)
        {
            status = S_SUCCESS; 
        }
        else
        {
            status = S_COMMTYPE; 
        }
        break;
#endif
    default:
        status = S_NOTSUPPORT;
        break;
    }  
    
    if(status != S_SUCCESS)
    {
        log_push_error_point(0x45A1); 
    }
    
    return status;
}





