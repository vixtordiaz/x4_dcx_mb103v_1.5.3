/**
  **************** (C) COPYRIGHT 2012 SCT Performance, LLC *********************
  * File Name          : sul_file.h
  * Device / System    : SCT products using the "CommonCode" platform
  * Author             : Patrick Downs
  *
  * Version            : 1 
  * Date               : 03/20/2012
  * Description        : File format handling functions for Stock Upload File (.SUL)
  *                    : 
  *
  *
  * History            : 03/20/2012 P. Downs
  *                    :   Created
  *
  ******************************************************************************
  */
#ifndef __SUL_FILE_H
#define __SUL_FILE_H

typedef void (sul_file_progressreport)(u8 percentage, u8 *message);

typedef struct
{
    u8 headerVersion;
    u8 companyID[3];
    u32 headerCrc32;
    u32 contentCrc32;
    u32 hwFirmwareVersion;
    u8 hwPartNumber[32];
    u8 hwSerialNumber[32];
    u16 BinaryType;
    u8 reserved1[430];
    union ecminfoblock
    {
        ecm_info ecminfo;
        u8 raw[4096];
    }ecminfoblock;

    u8 reserved2[512];
}SUL_HEADER;
STRUCT_SIZE_CHECK(SUL_HEADER,5120);

u8 sul_createfile(u8 *filename, u16 binarytype, ecm_info ecminfo,
                  sul_file_progressreport progressreport_funcptr);
u8 sul_getlastfilename(u8 *filename);

#endif  //__SUL_FILE_H
