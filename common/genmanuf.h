/**
  **************** (C) COPYRIGHT 2011 SCT Performance, LLC *********************
  * File Name          : genmanuf.h
  * Device / System    : SCT products using the "CommonCode" platform
  * Author             : Quyen Leba
  *
  * Version            : 1 
  * Date               : 05/02/2011
  * Description        : 
  *                    : 
  *
  *
  * History            : 05/02/2011 M. Davis
  *                    :   Add SCT Copyright header,
  *
  ******************************************************************************
  */

#ifndef __GENMANUF_H
#define __GENMANUF_H

#ifdef __GM_MANUF__
#define SUPPORT_COMM_CAN            1
#define SUPPORT_COMM_SCP            0
#define SUPPORT_COMM_VPW            1
#define CAN_DATA_PADDING_BYTE       0xAA

#elif __FORD_MANUF__
#define SUPPORT_COMM_CAN            1
#define SUPPORT_COMM_SCP            1
#define SUPPORT_COMM_VPW            0
#define CAN_DATA_PADDING_BYTE       0x00

#elif __DCX_MANUF__
#define SUPPORT_COMM_CAN            1
#define SUPPORT_COMM_SCP            0
#define SUPPORT_COMM_VPW            0
#define CAN_DATA_PADDING_BYTE       0x00
#else
#error "genmanuf.h: Invalid MANUF"
#endif

#endif    //__GENMANUF_H
