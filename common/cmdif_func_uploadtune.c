/**
  **************** (C) COPYRIGHT 2011 SCT Performance, LLC *********************
  * File Name          : cmdif_func_uploadtune.c
  * Device / System    : SCT products using the "CommonCode" platform
  * Author             : Quyen Leba
  *
  * Version            : 1 
  * Date               : 05/02/2011
  * Description        : 
  *                    : 
  *
  *
  * History            : 05/02/2011 M. Davis
  *                    :   Add SCT Copyright header,
  *
  ******************************************************************************
  */

//------------------------------------------------------------------------------
// Log error point: 0x4A00 -> 0x4AFF
//------------------------------------------------------------------------------

#include <fs/genfs.h>
#include <board/indicator.h>
#include <common/statuscode.h>
#include <common/settings.h>
#include <common/obd2tune.h>
#include <common/devicedef.h>
#include <common/obd2tune_upload.h>
#include <common/genmanuf_overload.h>
#include <common/filestock.h>
#include <common/cmdif.h>
#include <common/cmdif_remotegui.h>
#include <common/log.h>
#include <common/housekeeping.h>
#include <common/sul_file.h>
#include <common/debug/debug_output.h>
#include "cmdif_func_uploadtune.h"

extern cmdif_datainfo cmdif_datainfo_responsedata;  //from cmdif.c
extern u8 cmdif_scratchpad[512];                    //from cmdif.c
extern u16 cmdif_scratchpad_length;                 //from cmdif.c
extern flasher_info *flasherinfo;                   //from obd2tune.c

//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
void uploadtune_progressreport(u8 percentage, u8 *message);

//------------------------------------------------------------------------------
// Check upload condition (stock corrupted, stock already uploaded, etc)
// Use with cmdif_func_uploadtune_do if upload stock is required
// Return:  u8  status
// Note: status does not include negative response from cmdif, therefore,
// negative response is handled outside of this function
// Engineer: Patrick Downs, Quyen Leba
//------------------------------------------------------------------------------
u8 cmdif_func_uploadtune_init()
{
    u8  i;
    u8  current_index;
    u8  status;
    cmdif_upl_dnl_init_flags uploadinitflags;
    u16 ecm_type;
    CMDIF_COMMAND cmd = CMDIF_CMD_INIT_UPLOAD;
    CMDIF_REMOTEGUI_RESPONSETYPE responsetype;    
    u16 responsechoice;
    u8  response; 

    debug_elapsedtime_start();
    
    button_lock(TRUE);
    cmdif_datainfo_responsedata.responsecode = CMDIF_ACK_FAILED;
    if (flasherinfo == NULL)
    {
        log_push_error_point(0x4A00);
        status = S_BADCONTENT;
        goto cmdif_func_uploadtune_init_done;
    }
    else if (!(flasherinfo->flags & FLASHERFLAG_VALID_VEHICLE_INFO))
    {
        log_push_error_point(0x4A07);
        status = S_BADCONTENT;
        goto cmdif_func_uploadtune_init_done;
    }
    else if (SETTINGS_IsNoPreloadedDevice() &&
             flasherinfo->flashtype == CMDIF_ACT_PRELOADED_FLASHER)
    {
        log_push_error_point(0x4A08);
        status = S_NOTSUPPORT;
        goto cmdif_func_uploadtune_init_done;
    }
    else if ((SETTINGS_IsNoCustomDevice() || SETTINGS_Is50StateLegalDevice()) &&
             flasherinfo->flashtype == CMDIF_ACT_CUSTOM_FLASHER)
    {
        log_push_error_point(0x4A09);
        status = S_NOTSUPPORT;
        goto cmdif_func_uploadtune_init_done;
    }
    
    if (flasherinfo->flashtype == CMDIF_ACT_PRELOADED_FLASHER)
    {
        //strip out any unsupport ECM info (no longer needed)
        //1st ECM must be supported
        current_index = 1;
        for(i=1;i<ECM_MAX_COUNT;i++)
        {
            if (flasherinfo->selected_tunelistinfo.supported[i])
            {
                if (current_index != i)
                {
                    flasherinfo->selected_tunelistinfo.tuneindex[current_index] =
                        flasherinfo->selected_tunelistinfo.tuneindex[i];
                    memcpy(flasherinfo->selected_tunelistinfo.description[current_index],
                           flasherinfo->selected_tunelistinfo.description[i],
                           sizeof(flasherinfo->selected_tunelistinfo.description[i]));
                    memcpy(flasherinfo->selected_tunelistinfo.tunefilename[current_index],  //TODOQ: ~~~ check if correction needed
                           flasherinfo->selected_tunelistinfo.tunefilename[i],
                           sizeof(flasherinfo->selected_tunelistinfo.tunefilename[i]));
                    memcpy(flasherinfo->selected_tunelistinfo.optionfilename[current_index],    //TODOQ: ~~~ check if correction needed
                           flasherinfo->selected_tunelistinfo.optionfilename[i],
                           sizeof(flasherinfo->selected_tunelistinfo.optionfilename[i]));
                    flasherinfo->selected_tunelistinfo.useoption[current_index] =
                        flasherinfo->selected_tunelistinfo.useoption[i];
                    flasherinfo->selected_tunelistinfo.supported[current_index] =
                        flasherinfo->selected_tunelistinfo.supported[i];
                    
                    flasherinfo->selected_tunelistinfo.tuneindex[i] = INVALID_ECM_DEF;
                    flasherinfo->selected_tunelistinfo.description[i][0] = NULL;
                    flasherinfo->selected_tunelistinfo.tunefilename[i][0] = NULL;
                    flasherinfo->selected_tunelistinfo.optionfilename[i][0] = NULL;
                    flasherinfo->selected_tunelistinfo.useoption[i] = TUNEUSEOPTION_NONE;
                    flasherinfo->selected_tunelistinfo.supported[i] = FALSE;
                }
                
                current_index++;
                
            }//if (flasherinfo->selected_tunelistinfo.supported[i])...
        }//for(i=1;i<ECM_MAX_COUNT;i++)...
        
        status = vehdef_get_vehdef_info(flasherinfo->selected_tunelistinfo.tuneindex, 
                                        flasherinfo->ecm_count, &flasherinfo->vehicle_type);
        
        if (status != S_SUCCESS)
        {
            log_push_error_point(0x4A01);
            cmdif_datainfo_responsedata.responsecode = CMDIF_ACK_FAILED;
            goto cmdif_func_uploadtune_init_done;
        }
    }
    else if (VEH_IsApproveSkipFlashTCM(flasherinfo->vehicle_type) && flasherinfo->flashtype == CMDIF_ACT_CUSTOM_FLASHER)
    {        
        if (cmdif_remotegui_listboxbox_init("PROGRAM VEHICLE", NULL) == S_SUCCESS)
        {
            cmdif_remotegui_listboxbox_additem("DOWNLOAD TCM TUNE", "", CMDIF_REMOTEGUI_LISTBOX_FLAGS_CLICKABLE | CMDIF_REMOTEGUI_LISTBOX_FLAGS_ALIGN_CENTER);
            cmdif_remotegui_listboxbox_additem("SKIP TCM TUNE", "", CMDIF_REMOTEGUI_LISTBOX_FLAGS_CLICKABLE | CMDIF_REMOTEGUI_LISTBOX_FLAGS_ALIGN_CENTER);
            cmdif_remotegui_listbox(cmd, 0, 0,
                                    CMDIF_REMOTEGUI_BUTTONFACE_None, CMDIF_REMOTEGUI_BUTTONFACE_None, CMDIF_REMOTEGUI_BUTTONFACE_None,
                                    CMDIF_REMOTEGUI_FOCUS_ListBox);
            cmdif_remotegui_wait_user_response(&responsetype, &responsechoice, NULL);

            if (responsechoice == 1)
            {
                flasherinfo->flags |= FLASHERFLAG_SKIP_FLASH_TCM;
            }
        }
    }
    
    if (VEH_IsCheckPreDownload(flasherinfo->vehicle_type) && !flasherinfo->isupload_only)
    {
        response = 0;
        status = obd2tune_do_check(&response, flasherinfo->vehicle_type, FALSE);
        if (status == S_SUCCESS)
        {
            // turn key off and hit continue
            cmdif_remotegui_messagebox(cmd,
                                       "PROGRAM VEHICLE", CMDIF_REMOTEGUI_ICON_KEYOFF,
                                       CMDIF_REMOTEGUI_BUTTONFACE_None, CMDIF_REMOTEGUI_BUTTONFACE_Continue, CMDIF_REMOTEGUI_BUTTONFACE_None,
                                       CMDIF_REMOTEGUI_FOCUS_Button1,
                                       "<HEADER>TURN KEY OFF</HEADER>\nTurn the vehicle's ignition key to the OFF position then press CONTINUE");
            cmdif_remotegui_wait_user_response(&responsetype, &responsechoice, NULL);
        }
        if(status != S_SUCCESS && status != S_NOTREQUIRED)
        {
            if(response != CMDIF_ACK_FAILED)
            {
                cmdif_datainfo_responsedata.responsecode = (CMDIF_ACK)response;
                log_push_error_point(0x4A0E);
                status = S_FAIL; 
                goto cmdif_func_uploadtune_init_done;
            }
            else
            {
                cmdif_datainfo_responsedata.responsecode = CMDIF_ACK_FAILED;
                log_push_error_point(0x4A0F);
                status = S_FAIL; 
                goto cmdif_func_uploadtune_init_done;
            }
        }
    }
    
    status = S_SUCCESS;
    if (flasherinfo->isupload_only)
    {
        cmdif_datainfo_responsedata.responsecode = CMDIF_ACK_OK;
    }
    else
    {
        if(SETTINGS_IsMarried())
        {
            if (flasherinfo->vehicle_type != SETTINGS_TUNE(veh_type))
            {
                log_push_error_point(0x4A02);
                status = S_VEHICLETYPE;
                cmdif_datainfo_responsedata.responsecode = CMDIF_ACK_INCORRECT_VEHTYPE;
                goto cmdif_func_uploadtune_init_done;
            }
            
            status = obd2tune_checkstockfile(SETTINGS_TUNE(veh_type));
            if (status == S_SUCCESS)
            {
                //this is not a failure; it means no need to do upload
                cmdif_datainfo_responsedata.responsecode = CMDIF_ACK_STOCK_EXIST;
            }
            else if (status == S_FILENOTFOUND)
            {
                cmdif_datainfo_responsedata.responsecode = CMDIF_ACK_STOCK_NOT_FOUND;
            }
            else
            {
                cmdif_datainfo_responsedata.responsecode = CMDIF_ACK_STOCK_CORRUPTED;
            }
            goto cmdif_func_uploadtune_init_done;
        }
        else
        {
            //this is not a failure; it means need to do upload
            cmdif_datainfo_responsedata.responsecode = CMDIF_ACK_NOT_MARRIED;
            goto cmdif_func_uploadtune_init_done;
        }
    }

cmdif_func_uploadtune_init_done:
    if (cmdif_datainfo_responsedata.responsecode != CMDIF_ACK_OK &&
        cmdif_datainfo_responsedata.responsecode != CMDIF_ACK_NOT_MARRIED &&
        cmdif_datainfo_responsedata.responsecode != CMDIF_ACK_STOCK_EXIST)
    {
        log_push_error_point(0x4A0C);
        obd2_garbagecollector();
    }
    else
    {
        //make sure vehicle_type is within range
        if (obd2_validate_ecmbinarytype(flasherinfo->vehicle_type) != S_SUCCESS)
        {
            log_push_error_point(0x4A0D);
            obd2_garbagecollector();
            cmdif_datainfo_responsedata.responsecode = CMDIF_ACK_FAILED;
        }
        else
        {
            ecm_type = VEH_GetEcmType(flasherinfo->vehicle_type,0);
            
            // Setup Upload Init Flags
            memset(&uploadinitflags, 0, sizeof(uploadinitflags));
            if (flasherinfo->isupload_only)
            {
                if(ECM_IsKeyOffPowerDown(ecm_type))
                {
                    uploadinitflags.keyoffpowerdownrequired = 1;
                    uploadinitflags.powerdowntime = ECM_GetPowerDownTime(ecm_type);
                }
            }
            uploadinitflags.veh_type = flasherinfo->vehicle_type;
            
            // Send upload init flags
            cmdif_datainfo_responsedata.dataptr = __malloc(sizeof(uploadinitflags));
            if(!cmdif_datainfo_responsedata.dataptr)
            {
                cmdif_datainfo_responsedata.responsecode = CMDIF_ACK_FAILED;
                status = S_MALLOC;
            }
            else
            {
                memcpy(cmdif_datainfo_responsedata.dataptr,&uploadinitflags,sizeof(uploadinitflags));
                cmdif_datainfo_responsedata.datalength = sizeof(uploadinitflags);
            }
        }
    }
    button_lock(FALSE);
    
    debug_reportstatus(status, "Upload Init");
    debug_elapsedtime_stop();
    
    return status;
}

//------------------------------------------------------------------------------
// Handle upload stock
// Return:  u8  status (including fail statuscode for negative response)
// Note: status does include negative response from cmdif; this means negative
// response is handled internally here and status equivalent to negative
// response
// Engineer: Patrick Downs, Quyen Leba
//------------------------------------------------------------------------------
u8 cmdif_func_uploadtune_do()
{
    u8  status;
    u8  *data;
    u16 datalength;
    
    data = NULL;
    
    debug_elapsedtime_start();

    button_lock(TRUE);
    indicator_set(Indicator_UploadStock);
    indicator_set_normaloperation(FALSE); // Lock LED
    
    if (flasherinfo == NULL)
    {
        log_push_error_point(0x4A03);
        cmdif_internalresponse_ack(CMDIF_CMD_DO_UPLOAD,
                                   CMDIF_ACK_FAILED,NULL,0);
        
        status = S_BADCONTENT;
        goto cmdif_func_uploadtune_do_done;
        
    }
    else if (SETTINGS_IsNoPreloadedDevice() &&
             flasherinfo->flashtype == CMDIF_ACT_PRELOADED_FLASHER)
    {
        //NoPreloadedDevice conflict with flasherinfo->flashtype
        log_push_error_point(0x4A0A);
        cmdif_internalresponse_ack(CMDIF_CMD_DO_UPLOAD,
                                   CMDIF_ACK_FAILED,NULL,0);
        
        status = S_NOTSUPPORT;
        goto cmdif_func_uploadtune_do_done;
    }
    else if ((SETTINGS_IsNoCustomDevice() || SETTINGS_Is50StateLegalDevice()) &&
             flasherinfo->flashtype == CMDIF_ACT_CUSTOM_FLASHER)
    {
        //NoCustomDevice or 50StateLegalDevice conflict
        log_push_error_point(0x4A0B);
        cmdif_internalresponse_ack(CMDIF_CMD_DO_UPLOAD,
                                   CMDIF_ACK_FAILED,NULL,0);
        
        status = S_NOTSUPPORT;
        goto cmdif_func_uploadtune_do_done;
    }

    //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    // Check if vehicle & device are healthy for programming
    //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    status = obd2tune_checkprogrammingcondition();
    if (status == S_LOWVBATT)
    {
        cmdif_internalresponse_ack(CMDIF_CMD_DO_UPLOAD,
                                   CMDIF_ACK_VBATT_LOW,NULL,0);
        
        goto cmdif_func_uploadtune_do_done;
    }
    else if (status == S_LOWVPP)
    {
        cmdif_internalresponse_ack(CMDIF_CMD_DO_UPLOAD,
                                   CMDIF_ACK_VPP_LOW,NULL,0);
        
        goto cmdif_func_uploadtune_do_done;
    }
    else if (status != S_SUCCESS)
    {
        //other obd2tune_checkprogrammingcondition() fail
        log_push_error_point(0x4A04);
        cmdif_internalresponse_ack(CMDIF_CMD_DO_UPLOAD,
                                   CMDIF_ACK_FAILED,NULL,0);
        
        goto cmdif_func_uploadtune_do_done;
    }

    if (flasherinfo->isupload_only)
    {
        //do nothing
    }
    else
    {
        //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
        // Check if trying to program the same vehicle
        // If not, checking married count; warn and confirm for last count
        //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
        if (obd2tune_ispreviousvehicle_matched(flasherinfo->vin, (u8*)&flasherinfo->ecminfo.codes[0]) == FALSE)  //unmatched
        {
            if (SETTINGS_TUNE(married_count) > MARRIED_COUNT_MAX)
            {
                //MARRIED_COUNT_MAX reached; this is an user error
                cmdif_internalresponse_ack(CMDIF_CMD_DO_UPLOAD,
                                           CMDIF_ACK_MARRIEDCOUTNMAXED,NULL,0);
                
                status = S_MARRIEDCOUNTMAXED;
                goto cmdif_func_uploadtune_do_done;
            }
            else if ((SETTINGS_TUNE(married_count)+1) == MARRIED_COUNT_MAX)
            {
                // Warn next vehicle change last..
                data = __malloc(2048+16);
                if(!data)
                {
                    status = S_MALLOC;
                    goto cmdif_func_uploadtune_do_done;
                }
                
                cmdif_internalresponse_ack(CMDIF_CMD_DO_UPLOAD,
                                           CMDIF_ACK_MARRIEDCOUNTWARN,NULL,0);
                cmdif_tracker_set_state(CMDIF_TRACKER_STATE_GET_COMMAND);
                cmdif_tracker_set_response_ack(CMDIF_ACK_MARRIEDCOUNTWARN);
                while(1)
                {
                    //TODOQ: IMPORTANT - need timeout
                    //TODOQ: give me a timeout
                    status = cmdif_receive_command_internal(data,&datalength,2048+16);
                    if (status == S_SUCCESS)
                    {
                        break;
                    }
                }
                cmdif_tracker_set_state(CMDIF_TRACKER_STATE_IN_PROGRESS);
                if (data[1] != CMDIF_CMD_MARRIEDCOUNTWARN_COMFIRM)
                {
                    // user doesn't want to use the last count
                    // acknowledge user max married-count abort
                    cmdif_internalresponse_ack(CMDIF_CMD_MARRIEDCOUNTWARN_CANCEL,
                                               CMDIF_ACK_OK,NULL,0);
                    status = S_USERABORT;
                    goto cmdif_func_uploadtune_do_done;
                }
                else
                {
                    // acknowledge user max married-count proceed 
                    cmdif_internalresponse_ack(CMDIF_CMD_MARRIEDCOUNTWARN_COMFIRM,
                                               CMDIF_ACK_OK,NULL,0);
                }
                __free(data);
                data = NULL;
            }
        }//if (obd2tune_ispreviousvin_matched...
    }
    
    //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    // Upload stock file
    //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    cmdif_tracker_set_state(CMDIF_TRACKER_STATE_LISTENING);
    status = obd2tune_uploadstock(flasherinfo->vehicle_type,
                                  flasherinfo->isupload_only);
    cmdif_tracker_set_state(CMDIF_TRACKER_STATE_IN_PROGRESS);
    if (status != S_SUCCESS)
    {
        cmdif_internalresponse_ack(CMDIF_CMD_DO_UPLOAD,
                                   cmdif_datainfo_responsedata.responsecode,
                                   NULL,0);
        goto cmdif_func_uploadtune_do_done;
    }
        
    if (flasherinfo->isupload_only && !SETTINGS_IsDemoMode())
    {
        // If Upload Stock only action, export stock.bin as SUL file
        status = sul_createfile(STOCK_FILENAME, flasherinfo->vehicle_type,
                                flasherinfo->ecminfo,
                                uploadtune_progressreport);
    }
    
    cmdif_internalresponse_ack(CMDIF_CMD_DO_UPLOAD,CMDIF_ACK_UPLOAD_DONE,NULL,0);
    
cmdif_func_uploadtune_do_done:
    if (status == S_SUCCESS)
    {
        housekeeping_additem(HouseKeepingType_VppTimeout, 3000);
    }
    else
    {
        obd2_garbagecollector();
    }
    
    if(data)
    {
        __free(data);
    }
    
    indicator_set_normaloperation(TRUE); // Unlock LED
    indicator_clear();
    indicator_link_status();
    button_lock(FALSE);
    
    debug_reportstatus(status, "Upload Do");
    debug_elapsedtime_stop();
    
    return status;
}

//------------------------------------------------------------------------------
// Helper to report progress ACK for CMDIF_CMD_DO_UPLOAD
// Inputs:  u8  pecentage
//          u8  *message (allow NULL)
// Engineer: Quyen Leba
// Note: only use this func while in cmdif_func_uploadtune_do(...)
//------------------------------------------------------------------------------
void uploadtune_progressreport(u8 percentage, u8 *message)
{
    u8  buffer[128+2];
    u8  bufferlength;
    u32 messagelength;
    
    buffer[0] = (u8)percentage;
    bufferlength = 1;
    if (message)
    {
        messagelength = strlen((char*)message);
        if (messagelength < 32)
        {
            strcpy((char*)&buffer[1],(char*)message);
            bufferlength += (messagelength+1);
        }
    }
    cmdif_internalresponse_ack_nowait
        (CMDIF_CMD_DO_UPLOAD, CMDIF_ACK_PROGRESSBAR, buffer, bufferlength);
    
    debug_reportprogress(percentage, message);
}

//------------------------------------------------------------------------------
// Search for vehicle type using ECMs' hardware and OS part#
// Only use for 'Upload Stock' feature
// Return:  u8  status
// Engineer: Quyen Leba
//------------------------------------------------------------------------------
u8 cmdif_func_uploadtune_vehicle_type_search()
{
    u8  status;

    cmdif_datainfo_responsedata.responsecode = CMDIF_ACK_FAILED;
    if (flasherinfo == NULL || !(flasherinfo->flags & FLASHERFLAG_VALID_VEHICLE_INFO))
    {
        log_push_error_point(0x4A05);
        return S_BADCONTENT;
    }
        
    flasherinfo->flags &= ~FLASHERFLAG_USER_SELECT_VEHICLE_TYPE;
    SETTINGS_ClearUserSelect();
    
    //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    // search files to find available ecm types
    //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    status = obd2_vehicle_type_search((ecm_info*)&flasherinfo->ecminfo);
    switch(status)
    {
    case S_SUCCESS:
        cmdif_datainfo_responsedata.responsecode = CMDIF_ACK_OK;
        break;
    case S_FILENOTFOUND:
        cmdif_datainfo_responsedata.responsecode = CMDIF_ACK_FILE_NOT_FOUND;
        break;
    case S_NOLOOKUP:
        cmdif_datainfo_responsedata.responsecode = CMDIF_ACK_NO_LOOKUP;
        break;
    case S_UNMATCH:
        cmdif_datainfo_responsedata.responsecode = CMDIF_ACK_VEHICLE_UNSUPPORTED;
        break;
    default:
        cmdif_datainfo_responsedata.responsecode = CMDIF_ACK_FAILED;
        break;
    }
    
    return status;
}

//------------------------------------------------------------------------------
// Compile user selected ECMs to a vehicle type
// Only use for 'Upload Stock' feature
// Input:   u8  *ecm_type_list
//              ([2:count][2:type0(pcm)][2:type1(tcm)][28:reserved]
// Return:  u8  status
// Engineer: Quyen Leba
//------------------------------------------------------------------------------
u8 cmdif_func_uploadtune_compile_user_vehicle_type(u8 *ecm_type_list)
{
    typedef struct
    {
        u16 count;
        u16 ecm_types[2];
        u8  reserved[28];
    }ecm_type_list_struct;
    u16 vehicle_type;
    u8  status;

    cmdif_datainfo_responsedata.responsecode = CMDIF_ACK_FAILED;
    if (flasherinfo == NULL || !(flasherinfo->flags & FLASHERFLAG_VALID_VEHICLE_INFO))
    {
        log_push_error_point(0x4A06);
        return S_BADCONTENT;
    }

    ecm_type_list_struct *plist = (ecm_type_list_struct*)ecm_type_list;

    status = vehdef_get_vehdef_info(plist->ecm_types, plist->count, &vehicle_type);
    if (status == S_SUCCESS)
    {
        flasherinfo->flags |= FLASHERFLAG_USER_SELECT_VEHICLE_TYPE;
        flasherinfo->vehicle_type = vehicle_type;
        cmdif_datainfo_responsedata.responsecode = CMDIF_ACK_OK;
    }
    else
    {
        flasherinfo->vehicle_type = INVALID_VEH_DEF;
        cmdif_datainfo_responsedata.responsecode = CMDIF_ACK_VEHICLE_UNSUPPORTED;
    }
    
    return S_SUCCESS;
}

//------------------------------------------------------------------------------
// Returns the filename of the uploaded stock file in .SUL V2 format
// Only use for 'Upload Stock' feature
// Return:  u8  status
// Engineer: Quyen Leba
//------------------------------------------------------------------------------
u8 cmdif_func_get_upload_stock()
{
    u8  status;
    F_FILE *fptr;
    u8 filename[32];
    
    status = sul_getlastfilename(filename);
    if(status != S_SUCCESS)
    {
        cmdif_datainfo_responsedata.responsecode = CMDIF_ACK_FAILED;
        return S_FAIL;
    }
    
    fptr = genfs_user_openfile(filename,"rb");
    if(!fptr)
    {
        cmdif_datainfo_responsedata.responsecode = CMDIF_ACK_FAILED;
        return S_FAIL;
    }
    genfs_closefile(fptr);
    
    //report uploaded stock filename 
    cmdif_internalresponse_ack(CMDIF_CMD_GET_UPLOAD_STOCK,CMDIF_ACK_OK,
                                        filename,
                                        strlen((char const*)filename)+1);
    return S_SUCCESS;
}
