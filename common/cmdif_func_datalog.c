/**
  **************** (C) COPYRIGHT 2011 SCT Performance, LLC *********************
  * File Name          : cmdif_func_datalog.c
  * Device / System    : SCT products using the "CommonCode" platform
  * Author             : Quyen Leba
  *
  * Version            : 1 
  * Date               : 05/02/2011
  * Description        : 
  *                    : 
  *
  *
  * History            : 05/02/2011 M. Davis
  *                    :   Add SCT Copyright header,
  *
  ******************************************************************************
  */

//------------------------------------------------------------------------------
// Log error point: 0x4C00 -> 0x4CFF
//------------------------------------------------------------------------------

#include <fs/genfs.h>
#include <board/genplatform.h>
#include <board/delays.h>
#include <board/indicator.h>
#include <board/rtc.h>
#include <common/genmanuf_overload.h>
#include <common/statuscode.h>
#include <common/file.h>
#include <common/cmdif.h>
#if SUPPORT_OTF
#include <common/obd2tune_otf.h>
#endif
#include <common/obd2datalog.h>
#include "cmdif_func_filexfer.h"
#include "cmdif_func_datalog.h"

extern cmdif_datainfo cmdif_datainfo_responsedata;  //from cmdif.c
extern Datalog_Mode gDatalogmode;                   //from obd2datalog.c
extern Datalog_Info *dataloginfo;                   //from obd2datalog.c

//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
u8 validate_datalogfilename(const u8 *datalogfilename);

//------------------------------------------------------------------------------
// Qualifies the DLX filename before trying to process the file
// Input:   const u8 *datalogfilename (must be in user folder)
// Return:  u8  status
// July 12, 2009
// Engineer: Patrick Downs, Quyen Leba
//------------------------------------------------------------------------------
u8 validate_datalogfilename(const u8 *datalogfilename)
{
    F_FILE *fptr;
    
    if (strstr((char*)datalogfilename, ".dlx") == 0)
    {
        log_push_error_point(0x4C00);
        return S_FAIL;          // Incorrect file extension
    }
    
    fptr = genfs_user_openfile((u8*)datalogfilename,"r");
    if(!fptr)
    {
        log_push_error_point(0x4C01);
        return S_FILENOTFOUND;
    }
    else
    {
        genfs_closefile(fptr);
    }
    
    return S_SUCCESS;
}

//------------------------------------------------------------------------------
// Input:   u8  percentage
// Return:  none
// Engineer: Quyen Leba
//TODOQ: make it a general function
//------------------------------------------------------------------------------
void report_progress(u8 percentage)
{
    cmdif_internalresponse_ack_nowait
        (CMDIF_CMD_DATALOG_SCAN, CMDIF_ACK_PROGRESSBAR, &percentage, 1);
}

//------------------------------------------------------------------------------
// Input:   u8  *datalogfilename
// Return:  u8  status
// Engineer: Patrick Downs, Quyen Leba
// Note: all CMDIF_ACK_ are already handled
//------------------------------------------------------------------------------
u8 cmdif_func_datalog_scan(u8 *datalogfilename)
{
#define MAX_VALID_PID_ALLOWED               256
    u8  validpidlist[MAX_VALID_PID_ALLOWED];
    u16 validpidcount;
    u8  filename[MAX_DATALOG_FILENAME_LENGTH+1];
    VehicleCommType vehiclecommtype[ECM_MAX_COUNT];
    VehicleCommLevel vehiclecommlevel[ECM_MAX_COUNT];
    u8  status;

    indicator_set(Indicator_Datalog_Setup);

    cmdif_datainfo_responsedata.responsecode = CMDIF_ACK_FAILED;
    if (file_isfilenamevalid(datalogfilename,MAX_DATALOG_FILENAME_LENGTH) == FALSE)
    {
        log_push_error_point(0x4C02);
        status = S_INPUT;
        cmdif_internalresponse_ack(CMDIF_CMD_DATALOG_SCAN,CMDIF_ACK_FAILED,NULL,0);
        goto cmdif_func_datalog_scan_done;
    }

    status = file_user_getfilename(datalogfilename,filename);
    if (status != S_SUCCESS)
    {
        log_push_error_point(0x4C03);
        status = S_INPUT;
        cmdif_internalresponse_ack(CMDIF_CMD_DATALOG_SCAN,CMDIF_ACK_FAILED,NULL,0);
        goto cmdif_func_datalog_scan_done;
    }
    
    status = validate_datalogfilename(filename);
    if (status != S_SUCCESS)
    {
        log_push_error_point(0x4C04);
        cmdif_internalresponse_ack(CMDIF_CMD_DATALOG_SCAN,CMDIF_ACK_FAILED,NULL,0);
        goto cmdif_func_datalog_scan_done;
    }
    
//    status = obd2datalog_getdatalogfileinfo(filename,&dlxheader,dlxblocks);
//    if (status == S_NOTFIT)
//    {
//        //Note: this should be the case of passthrough datalog
//        islargefilescan = TRUE;
//    }
//    else if (status != S_SUCCESS)
//    {
//        log_push_error_point(0x4C06);
//        cmdif_datainfo_responsedata.responsecode = CMDIF_ACK_FAILED;
//        goto cmdif_func_datalog_scan_done;
//    }

    status = obd2datalog_init(vehiclecommtype,vehiclecommlevel);
    if (status != S_SUCCESS)
    {
        log_push_error_point(0x4C07);
        cmdif_internalresponse_ack(CMDIF_CMD_DATALOG_SCAN,CMDIF_ACK_FAILED,NULL,0);
        goto cmdif_func_datalog_scan_done;
    }
    
    obd2datalog_cleanup_packets(vehiclecommtype,vehiclecommlevel);

    //obd2datalog_init completion
    status = cmdif_internalresponse_ack(CMDIF_CMD_DATALOG_SCAN,CMDIF_ACK_OK,NULL,0);
    if (status != S_SUCCESS)
    {
        goto cmdif_func_datalog_scan_done;
    }
    cmdif_tracker_set_state(CMDIF_TRACKER_STATE_LISTENING);
    
    //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    // Scan the whole list to find valid pids
    //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    status = obd2datalog_getvalidpidlist_fromfile
        (filename,vehiclecommtype,vehiclecommlevel,MAX_VALID_PID_ALLOWED,
         validpidlist, &validpidcount,report_progress);

    if (status != S_SUCCESS)
    {
        log_push_error_point(0x4C08);
        cmdif_internalresponse_ack(CMDIF_CMD_DATALOG_SCAN,CMDIF_ACK_FAILED,NULL,0);
        goto cmdif_func_datalog_scan_done;
    }
    
    //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    // Report result
    //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    if (validpidcount == 0)
    {
        log_push_error_point(0x4C09);
        //TODOQ: a better ACK
        cmdif_internalresponse_ack(CMDIF_CMD_DATALOG_SCAN,CMDIF_ACK_FAILED,NULL,0);
        goto cmdif_func_datalog_scan_done;
    }

    status = obd2datalog_dataloginfo_init();
    if (status != S_SUCCESS)
    {
        log_push_error_point(0x4C0A);
        cmdif_internalresponse_ack(CMDIF_CMD_DATALOG_SCAN,CMDIF_ACK_FAILED,NULL,0);
        goto cmdif_func_datalog_scan_done;
    }

    status = obd2datalog_generatevalidpidlistfile(filename,
                                                  validpidlist,validpidcount);
    if (status != S_SUCCESS)
    {
        log_push_error_point(0x4C0B);
        cmdif_internalresponse_ack(CMDIF_CMD_DATALOG_SCAN,CMDIF_ACK_FAILED,NULL,0);
        goto cmdif_func_datalog_scan_done;
    }

    //report datalog scan completion
    status = cmdif_internalresponse_ack(CMDIF_CMD_DATALOG_SCAN,CMDIF_ACK_DONE,
                                        DEFAULT_DATALOG_VALID_PID_FILENAME,
                                        strlen(DEFAULT_DATALOG_VALID_PID_FILENAME)+1);
    //TODOQ: process status
    
cmdif_func_datalog_scan_done:
    cmdif_tracker_set_state(CMDIF_TRACKER_STATE_IN_PROGRESS);
    indicator_clear();
    indicator_link_status();
    obd2_garbagecollector();
    return S_SUCCESS;
}

//------------------------------------------------------------------------------
// Get datalog feature such as analog inputs, etc
// Return:  u8  status
// Engineer: Patrick Downs, Quyen Leba
//------------------------------------------------------------------------------
u8 cmdif_func_datalog_getfeature()
{
    u8  status;

    status = obd2datalog_createdatalogfeaturefile(DATALOG_FEATURE_FILENAME);
    if (status == S_SUCCESS)
    {
        cmdif_datainfo_responsedata.dataptr = __malloc(strlen(DATALOG_FEATURE_FILENAME)+1);
        if (cmdif_datainfo_responsedata.dataptr == NULL)
        {
            log_push_error_point(0x4C0C);
            cmdif_datainfo_responsedata.responsecode = CMDIF_ACK_FAILED;
        }
        else
        {
            strcpy((char*)cmdif_datainfo_responsedata.dataptr,
                   (char*)DATALOG_FEATURE_FILENAME);
            cmdif_datainfo_responsedata.datalength = strlen(DATALOG_FEATURE_FILENAME)+1;
            cmdif_datainfo_responsedata.responsecode = CMDIF_ACK_OK;
        }
    }
    else
    {
        log_push_error_point(0x4C0D);
        cmdif_datainfo_responsedata.responsecode = CMDIF_ACK_FAILED;
    }
    return S_SUCCESS;
}

/**
 *  cmdif_func_datalog_request_otf
 *  
 *  @brief Request for OTF functions
 *
 *  @param[in] vin
 *
 *  @retval u8  status
 *  
 *  @authors Quyen Leba
 */
u8 cmdif_func_datalog_request_otf(u8 *vin)
{
#if SUPPORT_OTF
    if (obd2tune_otf_isFeatureAvailable(vin))
    {
        cmdif_datainfo_responsedata.dataptr = __malloc(8);
        if (!cmdif_datainfo_responsedata.dataptr)
        {
            cmdif_datainfo_responsedata.responsecode = CMDIF_ACK_FAILED;
        }
        else
        {
            memset((char*)cmdif_datainfo_responsedata.dataptr,0,8);
            cmdif_datainfo_responsedata.datalength = 8;
            //[1:powerlevel][7:reserved]
            cmdif_datainfo_responsedata.dataptr[0] = SETTINGS_TUNE(tuneinfotrack.otfinfo.powerlevel);
            cmdif_datainfo_responsedata.responsecode = CMDIF_ACK_OK;
        }
    }
    else
    {
        cmdif_datainfo_responsedata.responsecode = CMDIF_ACK_TUNE_NOT_FOUND;
    }
    return S_SUCCESS;
#else
    cmdif_datainfo_responsedata.responsecode = CMDIF_ACK_NOT_SUPPORTED;
    return S_NOTSUPPORT;
#endif
}

//------------------------------------------------------------------------------
// Start datalog session
// Input:   u8  *datalogstartinfo
//              ([1:attribute][1:speed][n:datalogfilename][1:NULL])
// Return:  u8  status
// Engineer: Patrick Downs, Quyen Leba
//------------------------------------------------------------------------------
u8 cmdif_func_datalog_start(u8 *datalogstartinfo)
{
    u8  data[2048+16];              //TODOQ: more on this
    u16 datalength;
    u8  filename[MAX_DATALOG_FILENAME_LENGTH+1];
    u32 current_timestamp;
    u32 commlost_time;              //when commlost happens
    u32 commlost_timestamp;
    //u8  datalog_speed;              //TODOQ: more on this later
    u8  datalog_attribute;
    u8  *datalogfilename;
    u8  mode1_commlost_detection;   //is mode1 comm lost detection supported?
    u32 commlost_mode1count;        //how often to check for mode 1 comm lost
        
#define COMMLOST_NOCOMMREPORT_INTERVAL          5000
    u32 commlost_report_time;
#define COMMLOST_TIMECOUNT_THRESHOLD            10000
    u32 commlost_timecountms;
#define COMMLOST_PINGTIME_INTERVAL_UNIT         500
#define COMMLOST_PINGTIME_L0                    500
#define COMMLOST_PINGTIME_L1                    5000                //5s
#define COMMLOST_PINGTIME_L2                    15000
#define COMMLOST_PINGTIME_L3                    30000
#define COMMLOST_PINGTIME_L4                    60000
    u32 commlost_pingintervalms;
//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
// Adjust ping interval based on commlost time; only L1 is used now as we're
// using vbatt monitoring method @ cranking
//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
#define COMMLOST_PINGTIME_L1_THRESHOLD          (40*1000)         //40sec
#define COMMLOST_PINGTIME_L2_THRESHOLD          (3*60*1000)
#define COMMLOST_PINGTIME_L3_THRESHOLD          (4*60*1000)
#define COMMLOST_PINGTIME_L4_THRESHOLD          (5*60*1000)
    u32 commlost_pingtimecountms;       //when reached, go to longer interval
#define COMMLOSTRESUME_DETECTMENTHOD_PING       0
#define COMMLOSTRESUME_DETECTMENTHOD_VBATT      1
    u8  commlostresume_detectmethod;
    struct
    {
#define VBATT_MAX_SAMPLE_POINTS                 75
        float samplepool[VBATT_MAX_SAMPLE_POINTS];
        u8  looped      : 1;
        u8  flipped     : 1;
        u8  samplepoolindex;
        u32 timestamp;
    }vbatt_monitor;
    bool datalog_paused;    //TRUE: do not report datalog data
    u32 tmp_u32;
    u8  status;

    datalog_paused = FALSE;
    datalog_attribute = datalogstartinfo[0];
    //datalog_speed = datalogstartinfo[1];
    //obd2datalog_setdatareportinterval(datalogstartinfo[1]);   //TODOQ2: do this
    datalogfilename = &datalogstartinfo[2];

    cmdif_reset_watchdog();
    cmdif_datainfo_responsedata.responsecode = CMDIF_ACK_FAILED;
    if (file_isfilenamevalid(datalogfilename,MAX_DATALOG_FILENAME_LENGTH) == FALSE)
    {
        log_push_error_point(0x4C10);
        return S_INPUT;
    }
    
    status = file_user_getfilename(datalogfilename,filename);
    if (status != S_SUCCESS)
    {
        log_push_error_point(0x4C11);
        return S_INPUT;
    }

    indicator_set(Indicator_Datalog_Setup);

    status = obd2datalog_dataloginfo_init();
    if (status != S_SUCCESS)
    {
        log_push_error_point(0x4C12);
        goto cmdif_func_datalog_start_done;
    }

    status = obd2datalog_parsedatalogfile(filename);
    if (status != S_SUCCESS)
    {
        log_push_error_point(0x4C13);
        goto cmdif_func_datalog_start_done;
    }
    
    //this is 1st internal ACK of CMDIF_CMD_DATALOG_START
    status = cmdif_internalresponse_ack(CMDIF_CMD_DATALOG_START,CMDIF_ACK_OK,NULL,0);
    if (status != S_SUCCESS)
    {
        goto cmdif_func_datalog_start_done;
    }
    if (datalog_attribute & DATALOG_START_ATTRIB_DISABLE_NEWDATA_FILTER)
    {
        obd2datalog_newdata_filter(FALSE);
    }
    else
    {
        obd2datalog_newdata_filter(TRUE);
    }
    
    //##########################################################################
    //
    //##########################################################################
    status = obd2datalog_init(dataloginfo->commtype,dataloginfo->commlevel);
    if (status != S_SUCCESS)
    {
        commlost_pingtimecountms = 0;
        commlost_pingintervalms = COMMLOST_PINGTIME_L0;
        dataloginfo->commstatus = DatalogCommLost;
        status = cmdif_internalresponse_ack(CMDIF_CMD_DATALOG_START,
                                            CMDIF_ACK_COMMLOST,NULL,0);
        if (status != S_SUCCESS &&
            status != S_SESSION_DROPPED && status != S_HALTED)
        {
            goto cmdif_func_datalog_start_done;
        }
        indicator_set(Indicator_Datalog_CommLost);
    }
    else
    {        
        obd2datalog_datastream_stop(dataloginfo->commtype,
                                    dataloginfo->commlevel);
        
        status = obd2datalog_startup(dataloginfo->commtype,
                                     dataloginfo->commlevel);
        if (status != S_SUCCESS)
        {
            log_push_error_point(0x4C14);
            cmdif_datainfo_responsedata.responsecode = CMDIF_ACK_FAILED;
            goto cmdif_func_datalog_start_done;
        }
        
        //test if mode1 commlost detection is supported
        mode1_commlost_detection = FALSE;
        if (dataloginfo->scheduled_tasks & DATALOG_TASK_MODE1_COMMLOST)
        {
            if (obd2datalog_check_voltage_mode1(0) == S_SUCCESS)
            {
                if (gDatalogmode.ecmBroadcast == FALSE)
                {
                    mode1_commlost_detection = TRUE;
                }
            }
        }
        
        indicator_set_normaloperation(TRUE);
        indicator_set(Indicator_Datalog_Active);
        rtc_reset();
        dataloginfo->commstatus = DatalogCommActive;
    }
    
    // Handle analog port external power and outputs
    obd2datalog_analogport_init();

    //##########################################################################
    //
    //##########################################################################
    cmdif_tracker_set_state(CMDIF_TRACKER_STATE_LISTENING);

    commlost_pingtimecountms = 0;
    commlost_pingintervalms = COMMLOST_PINGTIME_L0;
    commlost_timecountms = 0;
    current_timestamp = rtc_getvalue();
    commlost_timestamp = current_timestamp;
    commlost_time = current_timestamp;
    commlost_mode1count = current_timestamp;
    dataloginfo->lastdatapoint_timestamp = current_timestamp;
    dataloginfo->lastdatareport_timestamp = current_timestamp;
    dataloginfo->lasttesterpresent_timestamp = current_timestamp;
    dataloginfo->control.ecm_laststreamdata = current_timestamp;
    dataloginfo->control.tcm_laststreamdata = current_timestamp;    
    dataloginfo->lastsinglerate_timestamp = current_timestamp;
    dataloginfo->lastosc_timestamp = current_timestamp;
    dataloginfo->pending = FALSE;
    dataloginfo->osc_pending = FALSE;

    dataloginfo->control.hasotf = 0;
#if SUPPORT_OTF
    if (datalog_attribute & DATALOG_START_ATTRIB_HASOTF)
    {
        dataloginfo->control.hasotf = 1;
    }
#endif
    
    while(1)
    {   
        //######################################################################
        // normal operation, getting data and detecting comm lost
        //######################################################################
        if (dataloginfo->commstatus == DatalogCommActive)
        {
            // Execute Tasks   
            status = S_SUCCESS;            
            if (dataloginfo->scheduled_tasks & DATALOG_TASK_GET_DATA)
            {
                status = dataloginfo->getdatafunc(dataloginfo->commlevel);
            }
            if ((dataloginfo->scheduled_tasks & DATALOG_TASK_GET_DATA_SINGLE_RATE) && status == S_SUCCESS)
            {
                status = obd2datalog_getdata_singlerate(dataloginfo->commlevel);
            }
            if ((dataloginfo->scheduled_tasks & DATALOG_TASK_ANALOG) && status == S_SUCCESS)
            {
                status = dataloginfo->getanalogdatafunc();
            }
            
            //##############################################################
            // check mode 1 comm lost
            //##############################################################            
            
            if (dataloginfo->pending == FALSE && 
                dataloginfo->osc_pending == FALSE &&
                mode1_commlost_detection == TRUE)
                
            {
                current_timestamp = rtc_getvalue();
                if ((current_timestamp - commlost_mode1count) > COMMLOST_TIMECOUNT_THRESHOLD)
                {
                    commlost_mode1count = current_timestamp;
                    if (obd2datalog_check_voltage_mode1(1) != S_SUCCESS)
                    {
                        status = S_COMMLOST;
                    }
                }
            }
            
            if (status == S_SUCCESS)
            {
                current_timestamp = rtc_getvalue();
                commlost_timestamp = current_timestamp;
                commlost_time = current_timestamp;

                //##############################################################
                // report data
                //##############################################################
                if ((current_timestamp -
                     dataloginfo->lastdatareport_timestamp) > 
                    dataloginfo->datareport_interval)
                {
                    u8  report[4+2+6*MAX_DATALOG_DLXBLOCK_COUNT+2];
                    u16 reportlength;
                    status = obd2datalog_getdatareport(report,&reportlength);
                    if (status == S_SUCCESS && reportlength > 0 && !datalog_paused)
                    {
                        //send datalog packet
                        status = cmdif_internalresponse_ack(CMDIF_CMD_DATALOG_START,
                                                            CMDIF_ACK_DATA,
                                                            report,reportlength);
                        //TODOQ: process status
                    }
                    dataloginfo->lastdatareport_timestamp = current_timestamp;
                }
                
                //##############################################################
                // send testerpreset
                //##############################################################
                if (dataloginfo->sendtesterpresent)
                {
                    if ((current_timestamp - 
                         dataloginfo->lasttesterpresent_timestamp) > 
                        DATALOG_MAX_TESTERPRESENT_INTERVAL)
                    {
                        dataloginfo->lasttesterpresent_timestamp = current_timestamp;
                        dataloginfo->sendtesterpresent();
                    }
                }
                //##############################################################
                // handle record
                //##############################################################
                obd2datalog_record();
            }
            else if (status == S_HALTED)
            {
                //force stop session
                dataloginfo->control.ecm_stream = 0;
                dataloginfo->control.tcm_stream = 0;
                obd2datalog_datastream_stop(dataloginfo->commtype,
                                            dataloginfo->commlevel);
                //if above stop command doesn't work, this delay w/o tester
                //present will
                delays(5000,'m');
            }
            else if (status == S_COMMLOST)
            {
                commlost_timecountms = 0;
                indicator_set(Indicator_Datalog_CommLost);
                commlost_time = rtc_getvalue();
                commlost_pingtimecountms = 0;
                commlost_pingintervalms = COMMLOST_PINGTIME_L0;

                memset((char*)&vbatt_monitor.samplepool,0,sizeof(vbatt_monitor.samplepool));
                vbatt_monitor.samplepoolindex = 0;
                vbatt_monitor.looped = 0;
                vbatt_monitor.flipped = 0;
                commlostresume_detectmethod = COMMLOSTRESUME_DETECTMENTHOD_PING;

                dataloginfo->commstatus = DatalogCommLost;
                //report "DatalogCommLost" error
                commlost_report_time = (rtc_getvalue() + COMMLOST_NOCOMMREPORT_INTERVAL);
                status = cmdif_internalresponse_ack(CMDIF_CMD_DATALOG_START,
                                                    CMDIF_ACK_COMMLOST,NULL,0);
                if (status != S_SUCCESS &&
                    status != S_SESSION_DROPPED && status != S_HALTED)
                {
                    goto cmdif_func_datalog_start_done;
                }
            }
        }
        else if (dataloginfo->commstatus == DatalogCommLost)
        {            
            if(rtc_getvalue() > commlost_report_time)
            {
                commlost_report_time = (u32)(rtc_getvalue() + COMMLOST_NOCOMMREPORT_INTERVAL);
                cmdif_internalresponse_ack(CMDIF_CMD_DATALOG_START,
                                           CMDIF_ACK_COMMLOST,NULL,0);
            }
            
            if (commlostresume_detectmethod == COMMLOSTRESUME_DETECTMENTHOD_PING)
            {
                //setup ping intervals after certain time passed when comm lost
                //frequent ping might keep some vehicle modules using more power
                //and deplete vehicle battery faster
//////                if (commlost_pingtimecountms >= COMMLOST_PINGTIME_L4_THRESHOLD &&
//////                    commlost_pingintervalms < COMMLOST_PINGTIME_L4)
//////                {
//////                    commlost_pingintervalms = COMMLOST_PINGTIME_L4;
//////                    commlostresume_detectmethod = COMMLOSTRESUME_DETECTMENTHOD_VBATT;
//////                }
//////                else if (commlost_pingtimecountms >= COMMLOST_PINGTIME_L3_THRESHOLD &&
//////                         commlost_pingintervalms < COMMLOST_PINGTIME_L3)
//////                {
//////                    commlost_pingintervalms = COMMLOST_PINGTIME_L3;
//////                    commlostresume_detectmethod = COMMLOSTRESUME_DETECTMENTHOD_VBATT;
//////                }
//////                else if (commlost_pingtimecountms >= COMMLOST_PINGTIME_L2_THRESHOLD &&
//////                         commlost_pingintervalms < COMMLOST_PINGTIME_L2)
//////                {
//////                    commlost_pingintervalms = COMMLOST_PINGTIME_L2;
//////                    commlostresume_detectmethod = COMMLOSTRESUME_DETECTMENTHOD_VBATT;
//////                }
//////                else
                if (commlost_pingtimecountms >= COMMLOST_PINGTIME_L1_THRESHOLD &&
                    commlost_pingintervalms < COMMLOST_PINGTIME_L1)
                {
                    //commlost time threshold reached, now switch to monitor
                    //vbatt to detect cranking
                    commlost_pingintervalms = COMMLOST_PINGTIME_L1;
                    memset((char*)&vbatt_monitor.samplepool,0,sizeof(vbatt_monitor.samplepool));
                    vbatt_monitor.samplepoolindex = 0;
                    vbatt_monitor.looped = 0;
                    vbatt_monitor.flipped = 0;
                    vbatt_monitor.timestamp = rtc_getvalue();
                    commlostresume_detectmethod = COMMLOSTRESUME_DETECTMENTHOD_VBATT;

                    cmdif_internalresponse_ack(CMDIF_CMD_DATALOG_START,
                                                   CMDIF_ACK_COMMLOST,NULL,0);
                }
                else if (commlost_pingtimecountms < COMMLOST_PINGTIME_L3_THRESHOLD)
                {
                    commlost_pingtimecountms = rtc_getvalue() - commlost_time;
                }

                if (rtc_getvalue() - commlost_timestamp >= commlost_pingintervalms)
                {                                
                    //check for key on via mode 1 control module voltage            
                    status = S_SUCCESS;
                    if (mode1_commlost_detection == TRUE)
                    {
                        status = obd2datalog_check_voltage_mode1(1);                        
                    }
                    if (status == S_SUCCESS)
                    {
                        status = obd2datalog_init(dataloginfo->commtype,
                                                  dataloginfo->commlevel);
                        if (status == S_SUCCESS)
                        {
                            obd2datalog_datastream_stop(dataloginfo->commtype,
                                                        dataloginfo->commlevel);
                            
                            status = obd2datalog_parsedatalogfile(filename);
                            
                            if (status == S_SUCCESS)
                            {
                                status = obd2datalog_startup(dataloginfo->commtype,
                                                             dataloginfo->commlevel);
                            }
                            
                            if (status == S_SUCCESS)
                            {
                                indicator_set_normaloperation(TRUE);
                                indicator_set(Indicator_Datalog_Active);
                                dataloginfo->commstatus = DatalogCommActive;
                                //report "DatalogCommLost" recovery
                                status = cmdif_internalresponse_ack(CMDIF_CMD_DATALOG_START,
                                                                    CMDIF_ACK_COMMACTIVE,NULL,0);
                                if (status != S_SUCCESS &&
                                    status != S_SESSION_DROPPED && status != S_HALTED)
                                {
                                    goto cmdif_func_datalog_start_done;
                                }

#if SUPPORT_OTF
                                if (dataloginfo->control.hasotf)
                                {
                                    //resume OTF power level
                                    obd2tune_otf_setPowerLevel(SETTINGS_TUNE(veh_type),0,(OTFPowerLevel)SETTINGS_TUNE(tuneinfotrack.otfinfo.powerlevel));
                                }
#endif

                                cmdif_reset_watchdog();

                                current_timestamp = rtc_getvalue();
                                dataloginfo->control.ecm_laststreamdata = current_timestamp;
                                dataloginfo->control.tcm_laststreamdata = current_timestamp;
                                dataloginfo->lastdatapoint_timestamp = current_timestamp;
                                dataloginfo->lastsinglerate_timestamp = current_timestamp;
                                dataloginfo->lastosc_timestamp = current_timestamp;
                            }
                        }
                    }                    
                    commlost_timestamp = rtc_getvalue();
                    if (commlost_timecountms >= COMMLOST_TIMECOUNT_THRESHOLD)
                    {
                        //turn down indicator
                        indicator_set(Indicator_None);
                        indicator_set_normaloperation(FALSE);
                    }
                    else
                    {
                        commlost_timecountms += commlost_pingintervalms;
                    }
                }
            }//if (commlostresume_detectmethod == COMMLOSTRESUME_DETECTMENTHOD_PING)
            else //if (commlostresume_detectmethod == COMMLOSTRESUME_DETECTMENTHOD_VBATT)
            {
                float vbatt_min;
                float vbatt_max;
                u8  vbatt_i;
                
                //detect engine start by monitoring VBatt
                if (vbatt_monitor.samplepoolindex >= VBATT_MAX_SAMPLE_POINTS)
                {
                    vbatt_monitor.samplepoolindex = 0;
                    vbatt_monitor.looped = 1;
                }

                status = S_FAIL;
                if (vbatt_monitor.flipped++ == 0)
                {
                    status = adc_read(ADC_VBAT,&vbatt_monitor.samplepool[vbatt_monitor.samplepoolindex]);
                }
                if (status == S_SUCCESS)
                {
                    vbatt_min = 100;
                    vbatt_max = 0;
                    vbatt_monitor.samplepoolindex++;
                    if (vbatt_monitor.looped)
                    {
                        for(vbatt_i=0;vbatt_i<vbatt_monitor.samplepoolindex;vbatt_i++)
                        {
                            if (vbatt_monitor.samplepool[vbatt_i] > vbatt_max)
                            {
                                vbatt_max = vbatt_monitor.samplepool[vbatt_i];
                            }
                            if (vbatt_monitor.samplepool[vbatt_i] < vbatt_min)
                            {
                                vbatt_min = vbatt_monitor.samplepool[vbatt_i];
                            }
                        }
                    }
                    else
                    {
                        for(vbatt_i=0;vbatt_i<VBATT_MAX_SAMPLE_POINTS;vbatt_i++)
                        {
                            if (vbatt_monitor.samplepool[vbatt_i] > vbatt_max)
                            {
                                vbatt_max = vbatt_monitor.samplepool[vbatt_i];
                            }
                            if (vbatt_monitor.samplepool[vbatt_i] < vbatt_min)
                            {
                                vbatt_min = vbatt_monitor.samplepool[vbatt_i];
                            }
                        }
                    }
                    
                    //check if vbatt suddenly drops at least 1.5V -> cranking
                    if (vbatt_min > 6 && (vbatt_min+1.5) <= vbatt_max)
                    {
                        //resume ping method
                        commlost_pingtimecountms = 0;
                        commlost_pingintervalms = COMMLOST_PINGTIME_L0;
                        commlost_time = rtc_getvalue();
                        commlostresume_detectmethod = COMMLOSTRESUME_DETECTMENTHOD_PING;
                        
                        cmdif_internalresponse_ack(CMDIF_CMD_DATALOG_START,
                                                   CMDIF_ACK_RESUME,NULL,0);
                    }//if (vbatt_min...
                }
            }//else if (commlostresume_detectmethod == COMMLOSTRESUME_DETECTMENTHOD_VBATT)
        }//else if (dataloginfo->commstatus == DatalogCommLost)...
        
        //######################################################################
        // check for other inputs and handle datalog conditions
        //######################################################################
        if(commlink_get_link_status() == CommLinkInfoLink_None)
        {
            // No host connected. Quit datalog.
            obd2datalog_datastream_stop(dataloginfo->commtype,
                                        dataloginfo->commlevel);
            // TODOPD: Need below for cmdtracker info?
//            cmdif_internalresponse_ack(CMDIF_CMD_DATALOG_STOP,
//                                       CMDIF_ACK_OK,NULL,0);
            cmdif_datainfo_responsedata.responsecode = CMDIF_ACK_DONE;
            goto cmdif_func_datalog_start_done;
        }

        status = cmdif_receive_command_internal(data,&datalength,sizeof(data));
        if (status == S_SUCCESS)
        {
            cmdif_datainfo_responsedata.responsecode = CMDIF_ACK_FAILED;
            cmdif_datainfo_responsedata.dataptr = NULL;
            cmdif_datainfo_responsedata.datalength = 0;
            switch((CMDIF_COMMAND)data[1])
            {
            //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
            case CMDIF_CMD_DATALOG_UPDATE_OSC:
                if (datalength == *(u16*)&data[4]*7+6)
                {
                    status = cmdif_func_datalog_update_signal_osc(&data[6],*(u16*)&data[4]);
                    if (cmdif_datainfo_responsedata.responsecode != CMDIF_ACK_OK)
                    {
                        status = cmdif_internalresponse_ack(CMDIF_CMD_DATALOG_UPDATE_OSC,
                                                            cmdif_datainfo_responsedata.responsecode,NULL,0);
                    }
                    else
                    {
                        status = cmdif_internalresponse_ack(CMDIF_CMD_DATALOG_UPDATE_OSC,
                                                            CMDIF_ACK_OK,NULL,0);
                        if (status != S_SUCCESS)
                        {
                            goto cmdif_func_datalog_start_done;
                        }
                    }
                }
                break;
            //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
            case CMDIF_CMD_DATALOG_UNSET_ALL_OSC:
                obd2datalog_unset_all_osc(dataloginfo->commtype, 
                                          dataloginfo->commlevel);
                status = cmdif_internalresponse_ack(CMDIF_CMD_DATALOG_UNSET_ALL_OSC,
                                                    CMDIF_ACK_OK,NULL,0);
                break;
            //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
            case CMDIF_CMD_DATALOG_SET_OTF_POWER:
#if SUPPORT_OTF
                if (datalength == 4+8)
                {
                    if (dataloginfo->control.hasotf)
                    {
                        status = obd2tune_otf_setPowerLevel(SETTINGS_TUNE(veh_type),0,(OTFPowerLevel)data[4]);
                        if (status != S_SUCCESS)
                        {
                            status = cmdif_internalresponse_ack(CMDIF_CMD_DATALOG_SET_OTF_POWER,
                                                                CMDIF_ACK_FAILED,NULL,0);
                        }
                        else
                        {
                            SETTINGS_TUNE(tuneinfotrack.otfinfo.powerlevel) = data[4];
                            SETTINGS_SetTuneAreaDirty();
                            settings_update(SETTINGS_UPDATE_IF_DIRTY);
                            status = cmdif_internalresponse_ack(CMDIF_CMD_DATALOG_SET_OTF_POWER,
                                                                CMDIF_ACK_OK,NULL,0);
                        }
                    }
                    else
                    {
                        status = cmdif_internalresponse_ack(CMDIF_CMD_DATALOG_SET_OTF_POWER,
                                                            CMDIF_ACK_FAILED,NULL,0);
                    }
                }
                else
                {
                    status = cmdif_internalresponse_ack(CMDIF_CMD_DATALOG_SET_OTF_POWER,
                                                        CMDIF_ACK_FAILED,NULL,0);
                }
#else
                status = cmdif_internalresponse_ack(CMDIF_CMD_DATALOG_SET_OTF_POWER,
                                                    CMDIF_ACK_NOT_SUPPORTED,NULL,0);
#endif
                break;
            //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
            case CMDIF_CMD_DATALOG_RECORD:
                if (datalength == 5)
                {
                    status = cmdif_func_datalog_record(data[4]);
                    if (cmdif_datainfo_responsedata.responsecode != CMDIF_ACK_OK)
                    {
                        status = cmdif_internalresponse_ack(CMDIF_CMD_DATALOG_RECORD,
                                                            cmdif_datainfo_responsedata.responsecode,NULL,0);
                        goto cmdif_func_datalog_start_done;
                    }
                    else
                    {
                        status = cmdif_internalresponse_ack(CMDIF_CMD_DATALOG_RECORD,
                                                            CMDIF_ACK_OK,NULL,0);
                        if (status != S_SUCCESS)
                        {
                            goto cmdif_func_datalog_start_done;
                        }
                    }
                }
                break;
            //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
            case CMDIF_CMD_DATALOG_PAUSE:
                datalog_paused = TRUE;
                status = cmdif_internalresponse_ack(CMDIF_CMD_DATALOG_PAUSE,
                                                    CMDIF_ACK_OK,NULL,0);
                break;
            //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
            case CMDIF_CMD_DATALOG_RESUME:
                datalog_paused = FALSE;
                status = cmdif_internalresponse_ack(CMDIF_CMD_DATALOG_RESUME,
                                                    CMDIF_ACK_OK,NULL,0);
                break;
            //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
            case CMDIF_CMD_DATALOG_STOP:
                obd2datalog_datastream_stop(dataloginfo->commtype,
                                            dataloginfo->commlevel);
                //report "CMDIF_CMD_DATALOG_STOP" command recognized
                cmdif_internalresponse_ack(CMDIF_CMD_DATALOG_STOP,
                                           CMDIF_ACK_OK,NULL,0);
                cmdif_datainfo_responsedata.responsecode = CMDIF_ACK_DONE;
                goto cmdif_func_datalog_start_done;
#ifdef SUPPORT_FILE_XFER
            //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
            case CMDIF_CMD_CHECK_FILE:
                //cmdif payload starts at byte 4th of data
                //[4:filesize][4:file crc32e][n:filename][NULL]
                status = S_FAIL;
                if (datalength == (8+strlen((char*)&data[8+(4)])+1+(4)))
                {
                    status = cmdif_func_filexfer_check_file(&data[0+(4)]);
                }
                cmdif_internalresponse_ack(CMDIF_CMD_CHECK_FILE,
                                           cmdif_datainfo_responsedata.responsecode,
                                           cmdif_datainfo_responsedata.dataptr,
                                           cmdif_datainfo_responsedata.datalength);
                break;
            //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
            case CMDIF_CMD_FREAD_BLOCK:
                //cmdif payload starts at byte 4th of data
                //[4:offset][4:blocklength(2048 max)]
                status = S_FAIL;
                if (datalength == 8+(4))
                {
                    status = cmdif_func_filexfer_fread_block(*((u32*)&data[0+(4)]),
                                                             *((u32*)&data[4+(4)]));
                }
                cmdif_internalresponse_ack(CMDIF_CMD_FREAD_BLOCK,
                                           cmdif_datainfo_responsedata.responsecode,
                                           cmdif_datainfo_responsedata.dataptr,
                                           cmdif_datainfo_responsedata.datalength);
                break;
            //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
            case CMDIF_CMD_FWRITE_BLOCK:
                //cmdif payload starts at byte 4th of data
                //[4:offset][4:blocklength(2048 max)][n:blockdata]
                status = S_FAIL;
                tmp_u32 = *((u32*)&data[4+(4)]);
                if (datalength == (tmp_u32+8+(4)))
                {
                    status = cmdif_func_filexfer_fwrite_block(*((u32*)&data[0+(4)]),
                                                              tmp_u32,
                                                              &data[8+(4)]);
                }
                cmdif_internalresponse_ack(CMDIF_CMD_FWRITE_BLOCK,
                                           cmdif_datainfo_responsedata.responsecode,
                                           cmdif_datainfo_responsedata.dataptr,
                                           cmdif_datainfo_responsedata.datalength);
                break;
            //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
            case CMDIF_CMD_FOPEN_FILETRANSFER:
                //cmdif payload starts at byte 4th of data
                //[1:attribute][4:filesize][n:filename][NULL]
                status = S_FAIL;
                if (datalength == (5+strlen((char*)&data[5+(4)])+1+(4)))
                {
                    status = cmdif_func_filexfer_fopen_filetransfer(&data[0+(4)]);
                }
                cmdif_internalresponse_ack(CMDIF_CMD_FOPEN_FILETRANSFER,
                                           cmdif_datainfo_responsedata.responsecode,
                                           cmdif_datainfo_responsedata.dataptr,
                                           cmdif_datainfo_responsedata.datalength);
                break;
            //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
            case CMDIF_CMD_FCLOSE_FILETRANSFER:
                //data: none
                status = cmdif_func_filexfer_fclose_filetransfer();
                cmdif_internalresponse_ack(CMDIF_CMD_FCLOSE_FILETRANSFER,
                                           cmdif_datainfo_responsedata.responsecode,
                                           cmdif_datainfo_responsedata.dataptr,
                                           cmdif_datainfo_responsedata.datalength);
                break;
            //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
            case CMDIF_CMD_FOLDER:
                //cmdif payload starts at byte 4th of data
                //data: [1:task][1:reserved][2:privdata][n:folder name][NULL][m:optional_data]
                status = S_FAIL;
                tmp_u32 = strlen((char*)&data[4+(4)]);
                if (datalength == (4+tmp_u32+1+(4)))
                {
                    status = cmdif_func_filexfer_folder((filexfer_foldertask)data[0+(4)],
                                                        &data[4+(4)],*(u16*)&data[2+(4)],NULL);
                }
                else if (datalength > (4+tmp_u32+1+(4)))
                {
                    status = cmdif_func_filexfer_folder((filexfer_foldertask)data[0+(4)],
                                                        &data[4+(4)],*(u16*)&data[2+(4)],&data[4+tmp_u32+1+1+(4)]);
                }
                cmdif_internalresponse_ack(CMDIF_CMD_FOLDER,
                                           cmdif_datainfo_responsedata.responsecode,
                                           cmdif_datainfo_responsedata.dataptr,
                                           cmdif_datainfo_responsedata.datalength);
                break;
            //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
            case CMDIF_CMD_FILE:
                //cmdif payload starts at byte 4th of data
                //data: [1:task][1:reserved][2:privdata][n:file name][NULL]
                status = S_FAIL;
                if (datalength == (4+strlen((char*)&data[4+(4)])+1+(4)))
                {
                    status = cmdif_func_filexfer_file((filexfer_filetask)data[0+(4)],
                                                      &data[4+(4)],*(u16*)&data[2+(4)]);
                }
                cmdif_internalresponse_ack(CMDIF_CMD_FILE,
                                           cmdif_datainfo_responsedata.responsecode,
                                           cmdif_datainfo_responsedata.dataptr,
                                           cmdif_datainfo_responsedata.datalength);
                break;
#endif  //SUPPORT_FILE_XFER
            //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
            default:
                //other commands are not valid during a datalog session
                //report invalid datalog command received
                cmdif_internalresponse_ack((CMDIF_COMMAND)data[1],
                                           CMDIF_ACK_FAILED,NULL,0);

                obd2datalog_datastream_stop(dataloginfo->commtype,
                                            dataloginfo->commlevel);
                cmdif_datainfo_responsedata.responsecode = CMDIF_ACK_DONE;
                goto cmdif_func_datalog_start_done;
            }//switch(data[1])...

            if (cmdif_datainfo_responsedata.dataptr)
            {
                __free(cmdif_datainfo_responsedata.dataptr);
                cmdif_datainfo_responsedata.dataptr = NULL;
                cmdif_datainfo_responsedata.datalength = 0;
            }
        }//if (status == S_SUCCESS)...
    }//while(1)...
    
cmdif_func_datalog_start_done:
    cmdif_tracker_set_state(CMDIF_TRACKER_STATE_IN_PROGRESS);
    obd2datalog_analogport_deinit();
    indicator_set_normaloperation(TRUE);
    indicator_clear();
    indicator_link_status();
    obd2_garbagecollector();
    return S_SUCCESS;
}

//------------------------------------------------------------------------------
// Set data report interval
// Inputs:  u8  attribute
//          u8  speed (valid value: 50->250 in ms)
// Return:  u8  status
// Engineer: Quyen Leba
//------------------------------------------------------------------------------
u8 cmdif_func_datalog_set_report_rate(u8 attribute, u8 speed)
{
    cmdif_datainfo_responsedata.responsecode = CMDIF_ACK_FAILED;
    if (speed < 50 || speed > 250)
    {
        cmdif_datainfo_responsedata.responsecode = CMDIF_ACK_FAILED;
        return S_INPUT;
    }
    if (obd2datalog_setdatareportinterval(speed) == S_SUCCESS)
    {
        cmdif_datainfo_responsedata.responsecode = CMDIF_ACK_OK;
        return S_SUCCESS;
    }
    return S_FAIL;
}

//------------------------------------------------------------------------------
// Start/Stop datalog recording
// Inputs:  u8  attribute
//          u8  speed (valid value: 50->250 in ms)
// Return:  u8  status
// Engineer: Quyen Leba
//------------------------------------------------------------------------------
u8 cmdif_func_datalog_record(u8 record_cmd)
{
    u8  status;

    cmdif_datainfo_responsedata.responsecode = CMDIF_ACK_FAILED;
    if (record_cmd == 0x00)
    {
        status = obd2datalog_stop_record();
    }
    else if (record_cmd == 0x01)
    {
        status = obd2datalog_start_record();
    }

    if (status == S_SUCCESS)
    {
        cmdif_datainfo_responsedata.responsecode = CMDIF_ACK_OK;
    }
    return status;
}

//------------------------------------------------------------------------------
// Save datalog result
// Inputs:  u8  type (0: perf test result)
//          u8  *data
//          u16 datalength
// Return:  u8  status
// Engineer: Quyen Leba
//------------------------------------------------------------------------------
u8 cmdif_func_datalog_save_result(u8 type, u8 *data, u16 datalength)
{
#define DATALOG_PERFORMANCE_TEST_RESULT_FILENAME        "perftestlog.txt"
    F_FILE *fptr;
    u32 bytecount;

    cmdif_datainfo_responsedata.responsecode = CMDIF_ACK_FAILED;
    switch(type)
    {
    case 0:     //save performance test result
        fptr = genfs_user_openfile((u8*)DATALOG_PERFORMANCE_TEST_RESULT_FILENAME,"a+");
        break;
    default:
        cmdif_datainfo_responsedata.responsecode = CMDIF_ACK_NOT_SUPPORTED;
        return S_NOTSUPPORT;
    }
    if(!fptr)
    {
        log_push_error_point(0x4C0E);
        cmdif_datainfo_responsedata.responsecode = CMDIF_ACK_FILE_NOT_FOUND;
        return S_FILENOTFOUND;
    }

    bytecount = fwrite((char*)data,1,datalength,fptr);
    genfs_closefile(fptr);
    if (bytecount != datalength)
    {
        log_push_error_point(0x4C0F);
        cmdif_datainfo_responsedata.responsecode = CMDIF_ACK_FAILED;
        return S_WRITEFILE;
    }

    cmdif_datainfo_responsedata.responsecode = CMDIF_ACK_OK;
    return S_SUCCESS;
}

/**
 * @brief   Update OSC Signal
 *
 * @param   [in] *data
 * @param   [in] count
 *
 * @retval  u8 status
 *
 * @author  Tristen Pierson
 *
 * @details This function updates an OSC signal item from the CMD IF. Data
 *          format is as follows: 
 *
 *          [2:index 1][4:data 1][1:status 1]...[2:index n][4:data n][1:status n]
 */
u8 cmdif_func_datalog_update_signal_osc(u8 *data, u16 count)
{
    u8 status;
    
    cmdif_datainfo_responsedata.responsecode = CMDIF_ACK_FAILED;
    status = obd2_datalog_update_signal_osc(data, count);
    
    if (status == S_SUCCESS)
    {
        cmdif_datainfo_responsedata.responsecode = CMDIF_ACK_OK;
    }
    return status;
}
