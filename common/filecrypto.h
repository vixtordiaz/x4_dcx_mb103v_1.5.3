/**
  **************** (C) COPYRIGHT 2011 SCT Performance, LLC *********************
  * File Name          : filecrypto.h
  * Device / System    : SCT products using the "CommonCode" platform
  * Author             : Quyen Leba
  *
  * Version            : 1 
  * Date               : 05/02/2011
  * Description        : 
  *                    : 
  *
  *
  * History            : 05/02/2011 M. Davis
  *                    :   Add SCT Copyright header,
  *
  ******************************************************************************
  */

#ifndef __FILECRYPTO_H
#define __FILECRYPTO_H

#include <arch/gentype.h>


#define FILECRYPTO_GENERAL_HEADER_VERSION_MAX       1

#define FILECRYPTO_INFO_HEADER_SIZE                 40
#define FILECRYPTO_COMMON_HEADER_SIZE               64
#define FILECRYPTO_EXTENDED_HEADER_MAX_SIZE         256

#define FILECRYPTO_GENERIC_HEADER_SIZE              24
#define FILECRYPTO_GENERIC_HEADER_VERSION           1

enum
{
    FileCryptoFileType_FileBundle                   = 4,
};
enum
{
    FileCryptoFileSubType_Firmware                  = 3,
};

typedef struct
{
    u32 hcrc32e;
    u16 hversion;
    u16 ftype;
    u16 hsize;
    u8  fsubtype;
    u8  reserved0;
    u32 fsize;
    u32 fcrc32e;
    u32 contentversion;
}FileCryptoHeaderInfo;

typedef struct
{
    u8  serialnumber[16];
    u32 flags;  // Bit[3..0]: 0 (No encryption), 1 (Blowfish)
                // Bit[7..4]: 0 (No compression), 1 (LZW), 2 (Hoffman)
                // Bit[8]: 0 (No Stock Special Options File), 1 (Stock Special Options File Included)
                // Bit[31..9]: reserved
    u16 vehicletype;
    u8  paddingbytecount;
    u8  variantid;
    u32 recoveryid;
    u8  reserved1[12];
}RecoveryStuneInfo;
#define RecoveryStuneInfoFlags_UnEncrypted  (0)
#define RecoveryStuneInfoFlags_Blowfish     (1u)
#define RecoveryStuneInfoFlags_UnCompressed (0<<4)
#define RecoveryStuneInfoFlags_LZW          (1u<<4)
#define RecoveryStuneInfoFlags_Hoffman      (2u<<4)

#define RecoveryStuneInfoFlags_StockSpecialOptionsIncluded  (1u<<8)

// Minimum content version for RSF files
#define RecoverySContentVersion_Min       1

typedef struct
{ 
    u16 filecount;
    u8  pathcount;
    u8  compressiontype;
    u16 flags;
    u16 version;
    u32 totalfilesize;
    u32 hardware;
    u32 device_market;
    u32 device_type;
    u8  serial[13];
    u8  serial_padding[3];
}FileBundleBasicInfo;
//SIZE: 40 bytes

typedef struct
{
    u32 crc32e;
    u32 filesize;
    u8  pathindex;
    u8  filenamelength;
    u8  reserved[2];
    u8  *filename;
    u8  *padding;
    u8  *filedata;
}FileCryptoFileBundleDataNode;

typedef enum
{
    HARDWARE_LW                             = 0,
    HARDWARE_TSX_D                          = 1,    //Dongle
    HARDWARE_TSX_M                          = 2,    //Kii (Monitor)
    HARDWARE_X3                             = 3,
    HARDWARE_X3P                            = 4,
    HARDWARE_iTSX                           = 10,
    HARDWARE_MainBoard_RevA                 = 11,
    HARDWARE_VehicleBoard_RevB              = 12,
    HARDWARE_AppBoardiTSX_RevA              = 13,
    HARDWARE_AppBoardKen257_RevA            = 14,
    HARDWARE_AppBoardX4_RevA                = 15,
    
    HARDWARE_UNKNOWN                        = 0xA0,
    HARDWARE_ANY                            = 0xFF,
}FileCrypto_Hardware;

typedef enum
{
    FILETYPE_FIRMWARE                       = 0,
    FILETYPE_CUSTOM_TUNE                    = 1,
    FILETYPE_TUNE_OPTION                    = 2,
    FILETYPE_STOCK_RECOVERY                 = 3,
    FILETYPE_FILE_BUNDLE                    = 4,
    FILETYPE_SYSTEM_RECOVERY_IMAGE          = 5,
    FILETYPE_SYSTEM_RECOVERY_FILE_BUNDLE    = 6,
    FILETYPE_UNKNOWN                        = 0xFF,
}FileCrypto_FileType;

typedef enum
{
    FILESUBTYPE_NONE                        = 0,
    FILESUBTYPE_SYSTEM_RECOVERY             = 1,
    FILESUBTYPE_TUNE_REVISION               = 2,
    FILESUBTYPE_FIRMWARE                    = 3,

    FILESUBTYPE_UNKNOWN                     = 0xFF,
}FileCrypto_FileSubType;

#endif	//__FILECRYPTO_H
