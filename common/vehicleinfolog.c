/**
  **************** (C) COPYRIGHT 2012 SCT Performance, LLC *********************
  * File Name          : vehicleinfolog.c
  * Device / System    : SCT products using the "CommonCode" platform
  * Author             : Patrick Downs
  *
  * Version            : 1 
  * Date               : 09/12/2012
  * Description        : 
  *                    : 
  *
  *
  ******************************************************************************
  */

#include <string.h>
#include <device_version.h>
#include <board/genplatform.h>
#include <common/statuscode.h>
#include <common/settings.h>
#include <fs/genfs.h>

#include "vehicleinfolog.h"

//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
u8 vehicleinfolog_init_header(VehicleInfoLogHeader *header);
u8 vehicleinfolog_validate_header(VehicleInfoLogHeader *header, F_FILE *fptr);

//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
u8 vehicleinfolog_init_header(VehicleInfoLogHeader *header)
{
    // Initalize the header
    memset((char*)header, 0, sizeof(VehicleInfoLogHeader));
    header->version = VIF_VERSION;
    memcpy((char*)header->device_serial,
           (char*)settingscritical.serialnumber,sizeof(header->device_serial));
//    header->block_count = 0;
    header->currententryindex = 0;
    header->header_crc32e = 0xFFFFFFFF;
    header->content_crc32e = 0xFFFFFFFF;

    return S_SUCCESS;
}

//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
u8 vehicleinfolog_validate_header(VehicleInfoLogHeader *header, F_FILE *fptr)
{
    u32 crc32e_calc;
    u32 crc32e_cmp;
    u32 bytecount;
    u8  buffer[1024];       //do not change buffer size

    // Check header CRC32e
    crc32e_cmp = header->header_crc32e;
    crc32e_reset();
    header->header_crc32e = 0xFFFFFFFF;
    crc32e_calc = crc32e_calculateblock(0xFFFFFFFF,
                                        (u32*)header, (sizeof(VehicleInfoLogHeader)/4));
    header->header_crc32e = crc32e_cmp;
    if(crc32e_calc != crc32e_cmp)
    {
        return S_CRC32E;
    }
    
    // Check the version
    if (header->version == 0)
    {
        return S_BADCONTENT;    //abandon version 0 file
    }
    else if (header->version > VIF_VERSION)
    {
        return S_NOTSUPPORT;
    }

    //make sure it's at beginning of content
    if(fseek(fptr, sizeof(VehicleInfoLogHeader), F_SEEK_SET) != 0)
    {
        return S_SEEKFILE;
    }
    
    // Check content CRC32e; content has multiple 4k vehicle info blocks
    crc32e_cmp = header->content_crc32e;
    crc32e_reset();
    crc32e_calc = 0xFFFFFFFF;
    while(!feof(fptr))
    {
        bytecount = fread(buffer,1,sizeof(buffer),fptr);
        if (bytecount != sizeof(buffer))
        {
            return S_READFILE;
        }
        crc32e_calc = crc32e_calculateblock(crc32e_calc,
                                            (u32*)buffer,(sizeof(buffer)/4));
    }
    if(crc32e_calc != crc32e_cmp)
    {
        return S_CRC32E;
    }

    return S_SUCCESS;
}

//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
u8 vehicleinfolog_add_block(ecm_info *ecminfo)
{
    F_FILE *fptr;
    VehicleInfoLogHeader header;
    VehicleInfoLogBlockInfo vehicleinfologblockinfo;
    u8  *bptr;
    u8  buffer[1024];       //do not change buffer size
    u16 bytecount;
    u16 byteread; 
    u32 currentindexfp;
    u32 crc32e_calc;
    bool isneedtocreatefile;
    u8  status;

    isneedtocreatefile = FALSE;
    // Try to open file
    fptr = genfs_user_openfile(VIF_FILENAME,"rb+");
    if (!fptr)
    {
        isneedtocreatefile = TRUE;
    }
    else
    {
        // Read header
        bytecount = fread((u8*)&header, 1, sizeof(header), fptr);
        if (bytecount != sizeof(header))
        {
            isneedtocreatefile = TRUE; //2nd chance: treat as new file
        }
        else if(vehicleinfolog_validate_header(&header, fptr) != S_SUCCESS)
        {
            isneedtocreatefile = TRUE; //2nd chance: treat as new file
        }
    }
    
    if (isneedtocreatefile)
    {
        header.currententryindex = 0;
        genfs_closefile(fptr);
        fptr = genfs_user_openfile(VIF_FILENAME,"wb+");
        if (!fptr)
        {
            return S_OPENFILE;
        }
        // Initalize the header
        vehicleinfolog_init_header(&header);
        bytecount = fwrite((u8*)&header, 1, sizeof(header), fptr);
        if (bytecount != sizeof(header))
        {
            status = S_WRITEFILE;
            goto vehicleinfolog_add_block_done;
        }
    }
    else
    {
        header.currententryindex++;
        if (header.currententryindex >= VIF_MAX_BLOCKS)
        {
            // When max blocks reached index loops around
            header.currententryindex = 0;
        }
    }

    //find position to write new log
    currentindexfp = (header.currententryindex * VIF_BLOCK_MAX_SIZE) + sizeof(header);
    if (fseek(fptr, currentindexfp, F_SEEK_SET) != 0)
    {
        status = S_SEEKFILE;
        goto vehicleinfolog_add_block_done;
    }

    //prepare data for new log
    memset((char*)&vehicleinfologblockinfo,0,sizeof(vehicleinfologblockinfo));
    vehicleinfologblockinfo.fwversion = APPLICATION_VERSION;
    vehicleinfologblockinfo.infoversion = VIF_INFO_BLOCK_VERSION;

    //calculate block crc32e including padding bytes
    crc32e_reset();
    vehicleinfologblockinfo.blockcrc32e = 0xFFFFFFFF;
    vehicleinfologblockinfo.blockcrc32e = crc32e_calculateblock
        (0xFFFFFFFF,(u32*)&vehicleinfologblockinfo,(sizeof(vehicleinfologblockinfo)/4));

    bptr = (u8*)ecminfo;
    bytecount = 0;
    while(bytecount < sizeof(ecm_info))
    {
        byteread = sizeof(ecm_info) - bytecount;
        if (byteread >= sizeof(buffer))
        {
            byteread = sizeof(buffer);
        }
        else
        {
            break;
        }
        memcpy((char*)buffer,(char*)bptr,byteread);
        vehicleinfologblockinfo.blockcrc32e = crc32e_calculateblock(vehicleinfologblockinfo.blockcrc32e,
                                                                    (u32*)buffer,byteread/4);
        bptr += byteread;
        bytecount += byteread;
        byteread = 0;
    }
    memset((char*)buffer,0,sizeof(buffer));
    if (byteread)
    {
        memcpy((char*)buffer,(char*)bptr,byteread);
    }
    //left over of ecm_info & padding
    bytecount += sizeof(vehicleinfologblockinfo);
    while(bytecount < VIF_BLOCK_MAX_SIZE)
    {
        byteread = VIF_BLOCK_MAX_SIZE - bytecount;
        if (byteread >= sizeof(buffer))
        {
            byteread = sizeof(buffer);
        }
        vehicleinfologblockinfo.blockcrc32e = crc32e_calculateblock(vehicleinfologblockinfo.blockcrc32e,
                                                                    (u32*)buffer,byteread/4);
        bytecount += byteread;
        memset((char*)buffer,0,sizeof(buffer));
    }

    // Write New Log To File
    //  Block size is VIF_BLOCK_MAX_SIZE
    //
    //  BlockInfo
    //  EcmInfo
    //  Padding[VIF_BLOCK_MAX_SIZEsizeof(BlockInfo)+sizeof(EcmInfo) 
    //
    
    bytecount = fwrite((u8*)&vehicleinfologblockinfo, 1, sizeof(vehicleinfologblockinfo), fptr);
    if(bytecount != sizeof(vehicleinfologblockinfo))
    {
        status = S_WRITEFILE;
        goto vehicleinfolog_add_block_done;
    }
    bytecount = fwrite((u8*)ecminfo, 1, sizeof(ecm_info), fptr);
    if(bytecount != sizeof(ecm_info))
    {
        status = S_WRITEFILE;
        goto vehicleinfolog_add_block_done;
    }
    bytecount = sizeof(vehicleinfologblockinfo) + sizeof(ecm_info);
    while(bytecount < VIF_BLOCK_MAX_SIZE)
    {
        memset((char*)buffer,0,sizeof(buffer));
        byteread = VIF_BLOCK_MAX_SIZE - bytecount;
        if (byteread >= sizeof(buffer))
        {
            byteread = sizeof(buffer);
        }
        if (fwrite(buffer, 1, byteread, fptr) != byteread)
        {
            status = S_WRITEFILE;
            goto vehicleinfolog_add_block_done;
        }
        bytecount += byteread;
    }

    //recalculate crc32e of content
    if(fseek(fptr, sizeof(header), F_SEEK_SET) != 0)    
    {
        status = S_SEEKFILE;
        goto vehicleinfolog_add_block_done;
    }
    crc32e_reset();
    crc32e_calc = 0xFFFFFFFF;
    while(!feof(fptr))
    {
        bytecount = fread(buffer,1,sizeof(buffer),fptr);
        if (bytecount != sizeof(buffer))
        {
            status = S_READFILE;
            goto vehicleinfolog_add_block_done;            
        }
        crc32e_calc = crc32e_calculateblock(crc32e_calc,(u32*)buffer,bytecount/4);
    }
    header.content_crc32e = crc32e_calc;

    // Calculate header CRC32e
    crc32e_reset();
    header.header_crc32e = 0xFFFFFFFF;
    header.header_crc32e = crc32e_calculateblock(0xFFFFFFFF,
                                                 (u32*)&header,(sizeof(header)/4));
    if(fseek(fptr, 0, F_SEEK_SET) != 0)
    {
        status = S_FAIL;
        goto vehicleinfolog_add_block_done;
    }

    bytecount = fwrite((u8*)&header, 1, sizeof(header), fptr);
    if(bytecount != sizeof(header))
    {
        status = S_WRITEFILE;
        goto vehicleinfolog_add_block_done;
    }
    status = S_SUCCESS;
    
vehicleinfolog_add_block_done:
    if (fptr)
    {
        genfs_closefile(fptr);
    }
    return status;
}

/**
 *  vehicleinfolog_get_latest_block
 *  
 *  @brief Get the latest ecm block saved in vehicle info log
 *
    @param[out] ecminfo
 *  @retval u8  status
 *  
 *  @authors Quyen Leba
 */
u8 vehicleinfolog_get_latest_block(ecm_info *ecminfo)
{
    F_FILE *fptr;
    VehicleInfoLogHeader header;
    VehicleInfoLogBlockInfo vehicleinfologblockinfo;
    u32 bytecount;
    u8  status;

    fptr = genfs_user_openfile(VIF_FILENAME,"r");
    if (!fptr)
    {
        return S_OPENFILE;
    }

    bytecount = fread((u8*)&header, 1, sizeof(header), fptr);
    if (bytecount != sizeof(header))
    {
        status = S_READFILE;
        goto vehicleinfolog_get_latest_block_done;
    }

    if (vehicleinfolog_validate_header(&header, fptr) != S_SUCCESS)
    {
        status = S_BADCONTENT;
        goto vehicleinfolog_get_latest_block_done;
    }
    
    if (fseek(fptr,sizeof(VehicleInfoLogHeader)+VIF_BLOCK_MAX_SIZE*header.currententryindex,SEEK_SET) != 0)
    {
        status = S_SEEKFILE;
        goto vehicleinfolog_get_latest_block_done;
    }

    bytecount = fread((char*)&vehicleinfologblockinfo,1,sizeof(vehicleinfologblockinfo),fptr);
    if (bytecount != sizeof(VehicleInfoLogBlockInfo))
    {
        status = S_READFILE;
        goto vehicleinfolog_get_latest_block_done;
    }

    bytecount = fread((char*)ecminfo,1,sizeof(ecm_info),fptr);
    if (bytecount != sizeof(ecm_info))
    {
        status = S_READFILE;
        goto vehicleinfolog_get_latest_block_done;
    }

    status = S_SUCCESS;

vehicleinfolog_get_latest_block_done:
    if (fptr)
    {
        genfs_closefile(fptr);
    }
    return status;
}
