
#ifndef PROTOBUF_LIB_H
#define PROTOBUF_LIB_H

#include <board/genplatform.h>

enum {
    PROTOBUF_LIB_TYPE_VARINT = 0,
    PROTOBUF_LIB_TYPE_64BIT = 1,
    PROTOBUF_LIB_TYPE_LENGTH_DELIMITED = 2,
    PROTOBUF_LIB_TYPE_START_GROUP = 3,
    PROTOBUF_LIB_TYPE_END_GROUP = 4,
    PROTOBUF_LIB_TYPE_32BIT = 5,
};

void protobuf_serialize_byte_array(u8* serialbuffer, u8* string, u32 length);

// returns length
u32 protobuf_deserialize_byte_array(u8* string, u8* serialbuffer);

void protobuf_serialize_string(u8* serialbuffer, u8* string);

// returns string length
u32 protobuf_deserialize_string(u8* string, u8* serialbuffer);

#define PROTOBUF_LIB_TEST       1

#ifdef PROTOBUF_LIB_TEST

u8 protobuf_test();

#endif //PROTOBUF_LIB_TEST

#endif // PROTOBUF_LIB_H