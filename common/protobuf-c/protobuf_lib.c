#include "protobuf_lib.h"

#define PROTOBUF_SET_FIELD(BYTE, ID, TYPE)      BYTE = ( TYPE | (ID << 3));
#define PROTOBUF_GET_FIELD_ID(BYTE)             ((BYTE >> 3) & 0x1F)
#define PROTOBUF_GET_FIELD_TYPE(BYTE)           ((BYTE & 0x07))

static u8 protobuf_uint32_to_varint(u8* serialbuffer, u32 value);
static u32 protobuf_varint_to_uint32(u8* serialbuffer);

#ifdef PROTOBUF_LIB_TEST
u8 protobuf_test()
{
    u8 serialbuffer[25];
    u32 length;
    u32 result;
    u32 i;
    u8 fieldtest[5];
    u8 field;
    u8 id;

    // Simple varint verification
    for (i = 0; i < 32; i++)
    {
        length = protobuf_uint32_to_varint(serialbuffer, 1 << i);
        result = protobuf_varint_to_uint32(serialbuffer);
        if (result != 1 << i) break;
    }
    
    if (i < 32)
        return 1;
    
    // Simple field test
    PROTOBUF_SET_FIELD(fieldtest[0], 1, PROTOBUF_LIB_TYPE_LENGTH_DELIMITED);
    field = PROTOBUF_GET_FIELD_TYPE(fieldtest[0]);
    id = PROTOBUF_GET_FIELD_ID(fieldtest[0]);
    
    if (field != PROTOBUF_LIB_TYPE_LENGTH_DELIMITED)
        return 1;
    if (id !=1)
        return 1;
    
    // byte arrary serialization test
    
    // uint32 serialization test
    
    // string serialization test
    
    return 0;
}
#endif

//this fuction not tested yet
// returns length of data copied to serialbuffer
u8 protobuf_uint32_to_varint(u8* serialbuffer, u32 value)
{
//    u8 varintbyte;
    u32 shiftvar = value;
    u8 length = 1;
    u8 buf[5];
    u8 i = 0;
    
    memset(buf,0,sizeof(buf));
    //start with the LSB then flip data after splitting the 7 bit chuncks

    //first 7bits will always be copied with MSB set to 0 indicating end of varint
    buf[i] = (0x7F & shiftvar);
    i++;
    // loop through the rest of the 7 bit sections
    do
    {
        shiftvar = shiftvar >> 7;
        
        buf[i] = (0x7F & shiftvar) | 0x80; // grab the next 7 bits

        if ((shiftvar & 0x7F) > 0) 
        {
            length = i+1;
        }
        i++;
    } while (i < 4);
    
    // remainder of 4 bits.
    
    shiftvar = shiftvar >> 7;
    
    buf[4] = (0x0F & shiftvar) | 0x80;  // grab the remaining 4 bits
    
    if ((shiftvar & 0x0F) > 0)
    {
        length = 5;
    }
    
    // Flip the bytes for the length
    for (i=0;i<length;i++)
    {
        serialbuffer[i] = buf[(length-1)-i];
    }
    
    return length;
}

// returns the decoded uint32 value
u32 protobuf_varint_to_uint32(u8* serialbuffer)
{
    int i = 0;
    u32 shiftvar = 0;
    
    do
    {
        shiftvar = (shiftvar << 7) & 0xFFFFFF80;
        shiftvar |= (serialbuffer[i] & 0x7F);
        
        //Check for end bit
        if ((serialbuffer[i] & 0x80) == 0x00) break;
        i++;
    } while(i<5);
    
    return shiftvar;
}

 
// returns id
u8 protobuf_get_field_id(u8* serialbuffer); // make macro?
u8 protobuf_set_field_type(u8* serialbuffer);  //make macro?
// returns type
u8 protobuf_get_field_type(u8* serialbuffer);  //make macro?
