/**
  **************** (C) COPYRIGHT 2011 SCT Performance, LLC *********************
  * File Name          : filectb.c
  * Device / System    : SCT products using the "CommonCode" platform
  * Author             : Quyen Leba
  *
  * Version            : 1 
  * Date               : 05/02/2011
  * Description        : 
  *                    : 
  *
  *
  * History            : 05/02/2011 M. Davis
  *                    :   Add SCT Copyright header,
  *
  ******************************************************************************
  */

#include <fs/genfs.h>
#include <board/genplatform.h>
#include <common/statuscode.h>
#include <common/crc32.h>
#include <common/settings.h>
#include <common/file.h>
#include <common/filecrypto.h>
#include <common/filestock.h>
#include <common/filebundle.h>
#include <common/crypto_blowfish.h>
#include <common/obd2tune.h>
#include <common/tea.h>
#include <common/veh_defs.h>
#include "filectb.h"

extern const u32 stockfile_tea_key[4];              //from filestock.c
extern const u32 flashfile_tea_key[4];              //from filestock.c

struct
{
    F_FILE *fptr;
    CtbHeader *header;
}filectbinfo =
{
    .fptr = NULL,
    .header = NULL,
};

bool filectb_hasValidCondition();

/**
 * filectb_openfile
 *
 * @brief   Open a file in bundle file (readonly access)
 *
 * @param   [in]    filename
 *
 * @retval u8  status
 *
 * @author  Quyen Leba
 *
 */
u8 filectb_openfile(const u8 *filename)
{
    u8  *bptr;
    u32 bytecount;
    u32 crc32e_calc;
    u8  status;

    bptr = NULL;
    filectb_closefile();
    filectbinfo.fptr = genfs_user_openfile(filename,"r");
    if (!filectbinfo.fptr)
    {
        return S_OPENFILE;
    }
    filectbinfo.header = __malloc(sizeof(CtbHeader));
    if (!filectbinfo.header)
    {
        return S_MALLOC;
    }
    bytecount = fread((char*)filectbinfo.header,1,sizeof(CtbHeader),filectbinfo.fptr);
    if (bytecount != sizeof(CtbHeader))
    {
        status = S_READFILE;
        goto filectb_openfile_done;
    }
    crypto_blowfish_decryptblock_external_key((u8*)filectbinfo.header,sizeof(CtbHeader));
    status = filectb_validateheader(filectbinfo.header);
    if (status != S_SUCCESS)
    {
        goto filectb_openfile_done;
    }

    bptr = __malloc(2048);
    if (!bptr)
    {
        status = S_MALLOC;
        goto filectb_openfile_done;
    }

    //check version
    if (filectbinfo.header->version != FILECTB_VERSION)
    {
        status = S_NOTSUPPORT;
        goto filectb_openfile_done;
    }

    //validate signature
    memset((char*)bptr,0,64);
    *(u32*)(bptr) = filectbinfo.header->marketType;
    *(u32*)(&bptr[4]) = filectbinfo.header->dealerDongleSerial;
    *(u32*)(&bptr[8]) = filectbinfo.header->spfFile[0].crc32e;
    *(u32*)(&bptr[12]) = filectbinfo.header->spfFile[1].crc32e;
    *(u32*)(&bptr[16]) = filectbinfo.header->spfFile[2].crc32e;
    *(u32*)(&bptr[20]) = filectbinfo.header->sthFile.crc32e;
    *(u32*)(&bptr[24]) = filectbinfo.header->ophFile.crc32e;
    memcpy((char*)&bptr[28],(char*)filectbinfo.header->deviceSerialNumber,16);
    crypto_blowfish_encryptblock_critical((u8*)bptr,64);
    crc32e_reset();
    crc32e_calc = crc32e_calculateblock(0xFFFFFFFF,(u32*)bptr,64/4);
    if (crc32e_calc != filectbinfo.header->signature)
    {
        status = S_REJECT;
        goto filectb_openfile_done;
    }

    //check device market type
    if (filectbinfo.header->marketType != MarketType_Unknown &&
        filectbinfo.header->marketType != MarketType_Any &&
        filectbinfo.header->marketType != SETTINGS_GetMarketType())
    {
        status = S_MARKETTYPE;
        goto filectb_openfile_done;
    }

    //check device serial number
    if (settings_compare_deviceserialnumber(filectbinfo.header->deviceSerialNumber) != S_SUCCESS)
    {
        status = 0;
        for(bytecount=0;bytecount<sizeof(filectbinfo.header->deviceSerialNumber);bytecount++)
        {
            status |= filectbinfo.header->deviceSerialNumber[bytecount];
        }
        if (status != 0)
        {
            status = S_SERIALUNMATCH;
            goto filectb_openfile_done;
        }
    }
    
    if (!isgraph(filectbinfo.header->spfFile[0].filename[0]))
    {
        //at least something must be in 1st file
        status = S_BADCONTENT;
        goto filectb_openfile_done;
    }
    
    //validate content
    crc32e_reset();
    crc32e_calc = 0xFFFFFFFF;
    while(!feof(filectbinfo.fptr))
    {
        //file should be multiple of 4 (8 to be exact)
        bytecount = fread(bptr,1,2048,filectbinfo.fptr);
        crc32e_calc = crc32e_calculateblock(crc32e_calc,(u32*)bptr,bytecount/4);
    }
    if (crc32e_calc != filectbinfo.header->contentCRC32E)
    {
        status = S_CRC32E;
        goto filectb_openfile_done;
    }

    if (fseek(filectbinfo.fptr,sizeof(CtbHeader),SEEK_SET) != 0)
    {
        status = S_SEEKFILE;
        goto filectb_openfile_done;
    }
    
filectb_openfile_done:
    if (bptr)
    {
        __free(bptr);
    }
    if (status != S_SUCCESS)
    {
        if (filectbinfo.header)
        {
            __free(filectbinfo.header);
            filectbinfo.header = NULL;
        }
        filectb_closefile();
    }
    return status;
}

void filectb_closefile()
{
    if (filectbinfo.fptr)
    {
        genfs_closefile(filectbinfo.fptr);
        filectbinfo.fptr = NULL;
    }
}

/**
 * filectb_hasValidCondition
 *
 * @brief   Check if valid condition (i.e. file opened, valid header, etc)
 *
 * @retval bool
 *
 * @author  Quyen Leba
 *
 */
bool filectb_hasValidCondition()
{
    if (filectbinfo.header && filectbinfo.fptr)
    {
        return TRUE;
    }
    return FALSE;
}

/**
 * filectb_validateheader
 *
 * @brief   Validate the CtbHeader
 *
 * @param   [in]    header
 *
 * @retval u8  status
 *
 * @author  Quyen Leba
 *
 */
u8 filectb_validateheader(CtbHeader *header)
{
    u32 crc32e_cmp;
    u32 crc32e_calc;

    crc32e_cmp = header->headerCRC32E;
    header->headerCRC32E = 0xFFFFFFFF;
    crc32e_reset();
    crc32e_calc = crc32e_calculateblock(0xFFFFFFFF,(u32*)header,sizeof(CtbHeader)/4);
    header->headerCRC32E = crc32e_cmp;      //restore original value
    if (crc32e_calc != crc32e_cmp)
    {
        return S_CRC32E;
    }
    return S_SUCCESS;
}

/**
 * filectb_getVehicleType
 *
 * @brief   Get vehicle type defined in CTB file
 *
 * @param   [out]   isOK
 *
 * @retval u16
 *
 * @author  Quyen Leba
 *
 */
u16 filectb_getVehicleType(bool *isOK)
{
    bool isValid;

    isValid = filectb_hasValidCondition();

    if (isOK)
    {
        *isOK = isValid;
    }
    if (isValid)
    {
        return filectbinfo.header->vehicleType;
    }
    else
    {
        return INVALID_VEH_DEF;
    }
}

/**
 * filectb_getDescription
 *
 * @brief   Get tune description in CTB file
 *
 * @param   [out]   filename
 *
 * @retval u8 status
 *
 * @author  Quyen Leba
 *
 */
u8 filectb_getDescription(u8 *description)
{
    if (!filectb_hasValidCondition())
    {
        return S_ERROR;
    }
    else if (!description)
    {
        return S_INPUT;
    }
    else if (strlen((char*)filectbinfo.header->tuneDescription) >= sizeof(filectbinfo.header->tuneDescription))
    {
        return S_BADCONTENT;
    }

    strcpy((char*)description,(char*)filectbinfo.header->tuneDescription);
    return S_SUCCESS;
}

/**
 * filectb_getDeviceSerialNumber
 *
 * @brief   Get device serial number in CTB file
 *
 * @param   [out]   filename
 *
 * @retval u8 status
 *
 * @author  Quyen Leba
 *
 */
u8 filectb_getDeviceSerialNumber(u8 *serialnumber)
{
    if (!filectb_hasValidCondition())
    {
        return S_ERROR;
    }
    else if (!serialnumber)
    {
        return S_INPUT;
    }
    else if (strlen((char*)filectbinfo.header->deviceSerialNumber) >= sizeof(filectbinfo.header->deviceSerialNumber))
    {
        return S_BADCONTENT;
    }

    strcpy((char*)serialnumber,(char*)filectbinfo.header->deviceSerialNumber);
    return S_SUCCESS;
}

/**
 * filectb_getSPFInfo
 *
 * @brief   Get SPF info in CTB file
 *
 * @param   [in]    index of SPF file to get info
 * @param   [out]   filename
 * @param   [out]   crc32e NULL allowed
 *
 * @retval u8 status
 *
 * @author  Quyen Leba
 *
 */
u8 filectb_getSPFInfo(u8 index, u8 *filename, u32 *crc32e)
{
    if (!filectb_hasValidCondition())
    {
        return S_ERROR;
    }
    else if (index >= 3 || !filename)
    {
        return S_INPUT;
    }
    else if (strlen((char*)filectbinfo.header->spfFile[index].filename) >= sizeof(filectbinfo.header->spfFile[0].filename))
    {
        return S_BADCONTENT;
    }

    strcpy((char*)filename,(char*)filectbinfo.header->spfFile[index].filename);
    if (crc32e)
    {
        *crc32e = filectbinfo.header->spfFile[index].crc32e;
    }
    return S_SUCCESS;
}

/**
 * filectb_getSTHInfo
 *
 * @brief   Get STH info in CTB file
 *
 * @param   [out]   filename
 * @param   [out]   crc32e NULL allowed
 *
 * @retval u8 status
 *
 * @author  Quyen Leba
 *
 */
u8 filectb_getSTHInfo(u8 *filename, u32 *crc32e)
{
    if (!filectb_hasValidCondition())
    {
        return S_ERROR;
    }
    else if (!filename)
    {
        return S_INPUT;
    }
    else if (strlen((char*)filectbinfo.header->sthFile.filename) >= sizeof(filectbinfo.header->sthFile.filename))
    {
        return S_BADCONTENT;
    }

    strcpy((char*)filename,(char*)filectbinfo.header->sthFile.filename);
    if (crc32e)
    {
        *crc32e = filectbinfo.header->sthFile.crc32e;
    }
    return S_SUCCESS;
}

/**
 * filectb_getOPHInfo
 *
 * @brief   Get OPH info in CTB file
 *
 * @param   [out]   filename
 * @param   [out]   crc32e NULL allowed
 *
 * @retval u8 status
 *
 * @author  Quyen Leba
 *
 */
u8 filectb_getOPHInfo(u8 *filename, u32 *crc32e)
{
    if (!filectb_hasValidCondition())
    {
        return S_ERROR;
    }
    else if (!filename)
    {
        return S_INPUT;
    }
    else if (strlen((char*)filectbinfo.header->ophFile.filename) >= sizeof(filectbinfo.header->ophFile.filename))
    {
        return S_BADCONTENT;
    }

    strcpy((char*)filename,(char*)filectbinfo.header->ophFile.filename);
    if (crc32e)
    {
        *crc32e = filectbinfo.header->ophFile.crc32e;
    }
    return S_SUCCESS;
}

/**
 * filectb_getCSFInfo
 *
 * @brief   Get CSF info in CTB file
 *
 * @param   [out]   filename
 * @param   [out]   crc32e NULL allowed
 *
 * @retval u8 status
 *
 * @author  Quyen Leba
 *
 */
u8 filectb_getCSFInfo(u8 *filename, u32 *crc32e)
{
    if (!filectb_hasValidCondition())
    {
        return S_ERROR;
    }
    else if (!filename)
    {
        return S_INPUT;
    }
    else if (strlen((char*)filectbinfo.header->csfFile.filename) >= sizeof(filectbinfo.header->csfFile.filename))
    {
        return S_BADCONTENT;
    }

    strcpy((char*)filename,(char*)filectbinfo.header->csfFile.filename);
    if (crc32e)
    {
        *crc32e = filectbinfo.header->csfFile.crc32e;
    }
    return S_SUCCESS;
}

/**
 * filectb_getTuneOSInfo
 *
 * @brief   Get TuneOS info in CTB file
 *
 * @param   [out]   filename
 * @param   [out]   crc32e NULL allowed
 *
 * @retval u8 status
 *
 * @author  Quyen Leba
 *
 */
u8 filectb_getTuneOSInfo(u8 *filename, u32 *crc32e)
{
    if (!filectb_hasValidCondition())
    {
        return S_ERROR;
    }
    else if (!filename)
    {
        return S_INPUT;
    }
    else if (strlen((char*)filectbinfo.header->tuneosFile.filename) >= sizeof(filectbinfo.header->tuneosFile.filename))
    {
        return S_BADCONTENT;
    }

    strcpy((char*)filename,(char*)filectbinfo.header->tuneosFile.filename);
    if (crc32e)
    {
        *crc32e = filectbinfo.header->tuneosFile.crc32e;
    }
    return S_SUCCESS;
}

/**
 * filectb_getSOFInfo
 *
 * @brief   Get SOF info in CTB file (special option file)
 *
 * @param   [out]   filename
 * @param   [out]   crc32e NULL allowed
 *
 * @retval u8 status
 *
 * @author  Quyen Leba
 *
 */
u8 filectb_getSOFInfo(u8 *filename, u32 *crc32e)
{
    if (!filectb_hasValidCondition())
    {
        return S_ERROR;
    }
    else if (!filename)
    {
        return S_INPUT;
    }
    else if (strlen((char*)filectbinfo.header->sofFile.filename) >= sizeof(filectbinfo.header->sofFile.filename))
    {
        return S_BADCONTENT;
    }

    strcpy((char*)filename,(char*)filectbinfo.header->sofFile.filename);
    if (crc32e)
    {
        *crc32e = filectbinfo.header->sofFile.crc32e;
    }
    return S_SUCCESS;
}

/**
 * filectb_parseCustomTuneBlock
 *
 * @brief   Parse CTB info into customtune_block
 *
 * @param   [out]   tuneblock
 *
 * @retval u8 status
 *
 * @author  Quyen Leba
 *
 */
u8 filectb_parseCustomTuneBlock(customtune_block *tuneblock)
{
    if (!filectb_hasValidCondition())
    {
        return S_ERROR;
    }

    memset((char*)tuneblock,0,sizeof(customtune_block));
    tuneblock->flags = filectbinfo.header->flags;
    tuneblock->veh_type = filectbinfo.header->vehicleType;
    tuneblock->market_type = filectbinfo.header->marketType;
    tuneblock->signature = filectbinfo.header->signature;

    strncpy((char*)tuneblock->tunedescription,(char*)filectbinfo.header->tuneDescription,sizeof(tuneblock->tunedescription)-1);
    strncpy((char*)tuneblock->tunefilename,(char*)filectbinfo.header->sthFile.filename,sizeof(tuneblock->tunefilename)-1);
    strncpy((char*)tuneblock->optionfilename,(char*)filectbinfo.header->ophFile.filename,sizeof(tuneblock->optionfilename)-1);
    strncpy((char*)tuneblock->tuneosfilename,(char*)filectbinfo.header->tuneosFile.filename,sizeof(tuneblock->tuneosfilename)-1);
    strncpy((char*)tuneblock->specialoptionfilename,(char*)filectbinfo.header->sofFile.filename,sizeof(tuneblock->specialoptionfilename)-1);
    strncpy((char*)tuneblock->checksumfilename,(char*)filectbinfo.header->csfFile.filename,sizeof(tuneblock->checksumfilename)-1);

    memcpy((char*)tuneblock->serialnumber,(char*)filectbinfo.header->deviceSerialNumber,sizeof(tuneblock->serialnumber));
    tuneblock->tunefilecrc32e = filectbinfo.header->sthFile.crc32e;
    tuneblock->optionfilecrc32e = filectbinfo.header->ophFile.crc32e;

    //TODOQ: not yet parse bootloader

    return S_SUCCESS;
}

/**
 * filectb_extractAllFiles
 *
 * @brief   Extract all files in CTB (except SPF)
 *
 * @param   [in]    progressreport_funcptr
 *
 * @retval u8 status
 *
 * @author  Quyen Leba
 *
 */
u8 filectb_extractAllFiles(filectb_progressreport progressreport_funcptr)
{
    u8  out_filename[64];
    u8  child_filename[64];
    u32 crc32e_cmp;
//    u8  i;
    u8  status = S_FAIL;

    if (!filectb_hasValidCondition())
    {
        return S_ERROR;
    }

    if (progressreport_funcptr)
    {
        progressreport_funcptr(0,"Copying File");
    }
    
    //extract sthFile
    if (isgraph(filectbinfo.header->sthFile.filename[0]))
    {
        if (progressreport_funcptr)
        {
            progressreport_funcptr(16,"Copying File");
        }
        status = filectb_getSTHInfo(child_filename,&crc32e_cmp);
        if (status != S_SUCCESS)
        {
            goto filectb_extractAllFiles_done;
        }
        strcpy((char*)out_filename,GENFS_USER_FOLDER_PATH);
        strcat((char*)out_filename,(char*)child_filename);
        status = filebundle_extractfile_by_fptr(filectbinfo.fptr,sizeof(CtbHeader),
                                                child_filename,out_filename);
        if (status != S_SUCCESS)
        {
            goto filectb_extractAllFiles_done;
        }
    }
    
    //extract ophFile
    if (isgraph(filectbinfo.header->ophFile.filename[0]))
    {
        if (progressreport_funcptr)
        {
            progressreport_funcptr(33,"Copying File");
        }
        status = filectb_getOPHInfo(child_filename,&crc32e_cmp);
        if (status != S_SUCCESS)
        {
            goto filectb_extractAllFiles_done;
        }
        strcpy((char*)out_filename,GENFS_USER_FOLDER_PATH);
        strcat((char*)out_filename,(char*)child_filename);
        status = filebundle_extractfile_by_fptr(filectbinfo.fptr,sizeof(CtbHeader),
                                                child_filename,out_filename);
        if (status != S_SUCCESS)
        {
            goto filectb_extractAllFiles_done;
        }
    }

    //extract csfFile
    if (isgraph(filectbinfo.header->csfFile.filename[0]))
    {
        if (progressreport_funcptr)
        {
            progressreport_funcptr(50,"Copying File");
        }
        status = filectb_getCSFInfo(child_filename,&crc32e_cmp);
        if (status != S_SUCCESS)
        {
            goto filectb_extractAllFiles_done;
        }
        strcpy((char*)out_filename,GENFS_USER_FOLDER_PATH);
        strcat((char*)out_filename,(char*)child_filename);
        status = filebundle_extractfile_by_fptr(filectbinfo.fptr,sizeof(CtbHeader),
                                                child_filename,out_filename);
        if (status != S_SUCCESS)
        {
            goto filectb_extractAllFiles_done;
        }
    }

    //extract tuneosFile
    if (isgraph(filectbinfo.header->tuneosFile.filename[0]))
    {
        if (progressreport_funcptr)
        {
            progressreport_funcptr(66,"Copying File");
        }
        status = filectb_getTuneOSInfo(child_filename,&crc32e_cmp);
        if (status != S_SUCCESS)
        {
            goto filectb_extractAllFiles_done;
        }
        strcpy((char*)out_filename,GENFS_USER_FOLDER_PATH);
        strcat((char*)out_filename,(char*)child_filename);
        status = filebundle_extractfile_by_fptr(filectbinfo.fptr,sizeof(CtbHeader),
                                                child_filename,out_filename);
        if (status != S_SUCCESS)
        {
            goto filectb_extractAllFiles_done;
        }
    }

    //extract sofFile
    if (isgraph(filectbinfo.header->sofFile.filename[0]))
    {
        if (progressreport_funcptr)
        {
            progressreport_funcptr(83,"Copying File");
        }
        status = filectb_getSOFInfo(child_filename,&crc32e_cmp);
        if (status != S_SUCCESS)
        {
            goto filectb_extractAllFiles_done;
        }
        strcpy((char*)out_filename,GENFS_USER_FOLDER_PATH);
        strcat((char*)out_filename,(char*)child_filename);
        status = filebundle_extractfile_by_fptr(filectbinfo.fptr,sizeof(CtbHeader),
                                                child_filename,out_filename);
        if (status != S_SUCCESS)
        {
            goto filectb_extractAllFiles_done;
        }
    }

    if (progressreport_funcptr)
    {
        progressreport_funcptr(100,"Copying File");
    }

filectb_extractAllFiles_done:
    return status;
}

/**
 * filectb_generateTuneBinary
 *
 * @brief   Generate tune binary file from SPF and STH in CTB. STH must be extracted first
 * @details Use filestock function of flash.bin to generate it then rename result to FILECTB_BASETUNE_FILENAME ("ctbbasetune.bin")
 *
 * @param   [in]    progressreport_funcptr
 *
 * @retval u8 status
 *
 * @author  Quyen Leba
 *
 */
u8 filectb_generateTuneBinary(filectb_progressreport progressreport_funcptr)
{
    file_blockcrypto source_decryption;
    file_blockcrypto dest_encryption;
    sthFileHeader sthheader;
    u8  filename[64];
    u32 totalbytecount;
    u32 stocksize;
    u32 crc32e_cmp;
    u32 crc32e_calc;
    u8  i;
    u8  status;

    status = S_FAIL;
    if (!filectb_hasValidCondition())
    {
        return S_ERROR;
    }

    source_decryption.simplecrypto.type = file_blockcrypto_type_tea;
    source_decryption.simplecrypto.key = (u32*)stockfile_tea_key;
    source_decryption.simplecrypto.crypto = &tea_decryptblock;

    dest_encryption.simplecrypto.type = file_blockcrypto_type_tea;
    dest_encryption.simplecrypto.key = (u32*)flashfile_tea_key;
    dest_encryption.simplecrypto.crypto = &tea_encryptblock;

    //Part1: generate flash.bin
    totalbytecount = 0;
    genfs_deletefile(FLASH_FILENAME);
    for(i=0;i<3;i++)
    {
        strcpy((char*)filename,GENFS_USER_FOLDER_PATH);
        
        if (isgraph(filectbinfo.header->spfFile[i].filename[0]))
        {
            status = filectb_getSPFInfo(i,&filename[strlen((char*)filename)],&crc32e_cmp);
            if (status != S_SUCCESS)
            {
                goto filectb_generateTuneBinary_done;
            }
            
            status = file_calcfilecrc32e_byfilename(filename,&crc32e_calc,0x00);
            if (status != S_SUCCESS)
            {
                goto filectb_generateTuneBinary_done;
            }
            else if (crc32e_calc != crc32e_cmp)
            {
                status = S_CRC32E;
                goto filectb_generateTuneBinary_done;
            }
            
            totalbytecount += file_getsize_byname(filename);
            
            status = file_append(filename, FLASH_FILENAME, 0, 0,    //whole file
                                 &source_decryption, &dest_encryption,
                                 progressreport_funcptr);
            if (status != S_SUCCESS)
            {
                goto filectb_generateTuneBinary_done;
            }
        }
    }
    
    status = obd2tune_getstocksize_byvehdef(filectbinfo.header->vehicleType,&stocksize);
    if (status != S_SUCCESS)
    {
        goto filectb_generateTuneBinary_done;
    }
    else if (totalbytecount != stocksize)
    {
        status = S_BADCONTENT;
        goto filectb_generateTuneBinary_done;
    }

    //Part2: apply STH
    strcpy((char*)filename,GENFS_USER_FOLDER_PATH);
    status = filectb_getSTHInfo(&filename[strlen((char*)filename)],&crc32e_cmp);
    if (status != S_SUCCESS)
    {
        goto filectb_generateTuneBinary_done;
    }

    status = obd2tune_validate_sth_file(filename,&sthheader);
    if (status != S_SUCCESS)
    {
        goto filectb_generateTuneBinary_done;
    }
    else if (crc32e_cmp != sthheader.generic.header_crc32e)
    {
        status = S_CRC32E;
        goto filectb_generateTuneBinary_done;
    }
    
    status = filestock_openflashfile("r+");
    if (status != S_SUCCESS)
    {
        goto filectb_generateTuneBinary_done;
    }

    status = obd2tune_apply_preloadedtune_sth(0,filename);
    if (status != S_SUCCESS)
    {
        goto filectb_generateTuneBinary_done;
    }

filectb_generateTuneBinary_done:
    filestock_closeflashfile();
    if (status == S_SUCCESS)
    {
        status = genfs_renamefile(FLASH_FILENAME,FILECTB_BASETUNE_FILENAME);
        if (status != S_SUCCESS)
        {
            //TODOQ: errorpoint
        }
    }

    return status;
}
