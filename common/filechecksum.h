/**
  **************** (C) COPYRIGHT 2011 SCT Performance, LLC *********************
  * File Name          : filechecksum.h
  * Device / System    : SCT products using the "CommonCode" platform
  * Author             : Quyen Leba
  *
  * Version            : 1 
  * Date               : 05/02/2011
  * Description        : 
  *                    : 
  *
  *
  * History            : 05/02/2011 M. Davis
  *                    :   Add SCT Copyright header,
  *
  ******************************************************************************
  */

//------------------------------------------------------------------------------
// Simple file checksum function used to keep track of changes in certain files
//  10/01/2007 P.D.
//------------------------------------------------------------------------------
#ifndef __FILECHECKSUM_H__
#define __FILECHECKSUM_H__

#include <arch/gentype.h>

typedef struct
{
	int	start_address;
	int size;
}Block_Info;

u8  fileCheckSumCheck(u8 *filename, u32 checksum);
u8  fileCheckSumCalculate(u8 *filename, u32 *checksum);
/*
u8  Checksum_routine(u8 eec_type);
u8  UpdatesimpleChecksum(u8 start_with_file);
u8  Get_VPW_headerinfo(void);
u8  UpdateVPWChecksum(void);
u8  UpdateCANChecksums(u8 eec_type, u32 OSLength, u8 proc_number);
u8  UpdatesimpleChecksum(u8 start_with_file);
unsigned short GMCAN_CalcCRC16(F_FILE* fp, signed long uLength, u8* cheksum, u8 CRC_location);
unsigned int swap32(u8[4]);
unsigned int swap16(u8[2], u8);
u8 SimpleCANChecksum(F_FILE* fp, u8 start_with_file, u8 end_with_file);
u8 LBZLMM_Checksum(void);
u32 LBZLMM_SetChecksum(u32 start_addr, u32 end_addr, u32 checksum_addr, u32 Flash_start, u32 ECUstart, u32 Flash_offset);
u8 Allison_checksum(u32 offset);
unsigned short Allison_Checksums(u32 start, u32 end, unsigned short* additive_cheksum);
*/

#endif
