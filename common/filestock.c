/**
  **************** (C) COPYRIGHT 2011 SCT Performance, LLC *********************
  * File Name          : filestock.c
  * Device / System    : SCT products using the "CommonCode" platform
  * Author             : Quyen Leba
  *
  * Version            : 1 
  * Date               : 05/02/2011
  * Description        : 
  *                    : 
  *
  *
  * History            : 05/02/2011 M. Davis
  *                    :   Add SCT Copyright header,
  *
  ******************************************************************************
  */

#include <board/genplatform.h>
#include <common/statuscode.h>
#include <common/settings.h>
#include <common/tea.h>
#include <fs/genfs.h>
#include "filestock.h"

#if (FILESTOCK_USE_ENCRYPTION == 0)
#ifdef __DEBUG_JTAG_
#warning filestock.c: FILESTOCK Un-Encrypted -- debug only
#else
#error #warning filestock.c: FILESTOCK Un-Encrypted !!!
#endif
#endif

const u32 stockfile_tea_key[4] =
{
    0x79DFFF0B, 0x39F23254, 0x85C3D6A0, 0x030ABF99,
};

const u32 flashfile_tea_key[4] =
{
    0xC201D017, 0x83B50E8C, 0x13408709, 0xD6813D16,
};

#define FILESTOCK_HANDLING_NONE         0
#define FILESTOCK_HANDLING_STOCKFILE    1
#define FILESTOCK_HANDLING_FLASHFILE    2

FileStock_Info filestock_info =
{
    .filestock_fptr = NULL,
    .residuebuffer = NULL,
    .residuebufferlength = 0,
    .control.filehandlingtype = FILESTOCK_HANDLING_NONE,
    .control.readonly = 1,
};

//------------------------------------------------------------------------------
// Get the FileStock_Info data
// 
// Inputs:            
// Return:  FileStock_Info  
// Engineer: Patrick Downs
//------------------------------------------------------------------------------
FileStock_Info filestock_getinfo(void)
{
    return filestock_info;
}

//------------------------------------------------------------------------------
// Verify stock file is healthy
// For now, only check if size is correct. Could do error checking later.
// Inputs:  u32 size_fromsettings
//          u32 size_fromvehdef
// Return:  u8  status
// Engineer: Quyen Leba
//------------------------------------------------------------------------------
u8 filestock_verify(u32 size_fromsettings, u32 size_fromvehdef)
{
    u32 stockfilesize;
    u8  status;
    u32 paddingsize;
    u32 temp_stockfilesize;

    if (SETTINGS_IsDemoMode())
    {
        return S_SUCCESS;
    }

    paddingsize = 0; 
    temp_stockfilesize = 0; 
    
    status = filestock_getfilesize(&stockfilesize);
    if(status == S_SUCCESS)
    {
        if(stockfilesize == size_fromvehdef)
        {
            status = S_SUCCESS;
        }
        else if (stockfilesize == 0)
        {
            status = S_STOCKFILECORRUPT;
        }
        else if(size_fromvehdef%4)  // Vehicle Def stock filesize not multiple of 4, need to accout for Padding bytes
        {
            temp_stockfilesize = size_fromvehdef;
            for (u8 i = 0; i < 8; i++)
            {
                if (temp_stockfilesize % 8 != 0)
                {
                    temp_stockfilesize++;
                    paddingsize++;
                }
                else
                {
                    break;
                }
            }
            if((stockfilesize - paddingsize) == size_fromvehdef)
            {
                status = S_SUCCESS;
            }
            else
            {
                status = S_STOCKFILECORRUPT;
            }
        }
    }
    else if (status == S_OPENFILE)
    {
        status = S_FILENOTFOUND;
    }
    
    return status;
}

//------------------------------------------------------------------------------
// Delete stock file
// Input:   bool isspecialstock (TRUE: in upload only feature)
// Engineer: Quyen Leba
//------------------------------------------------------------------------------
u8 filestock_delete(bool isspecialstock)
{
    if (SETTINGS_IsDemoMode())
    {
        return S_SUCCESS;
    }

    if (isspecialstock)
    {
        genfs_deletefile(SPECIAL_STOCK_FILENAME);
    }
    else
    {
        genfs_deletefile(STOCK_FILENAME);
    }
    return S_SUCCESS;
}

//------------------------------------------------------------------------------
// Inputs:  u8 *filename - Name of file to be opened. If no path, assume user folder.
//          u8 *attrib - ("wb","rb","r+","a+")
//          FileStock_Filetype filetype - Filetype for encryption key
// Return:  u8  status
// Engineer: Patrick Downs
//------------------------------------------------------------------------------
u8 filestock_openfile(u8 *filename, u8 *attrib, FileStock_Filetype filetype)
{
    u8  fullfilepath[128];

    if (SETTINGS_IsDemoMode())
    {
        return S_SUCCESS;
    }

    filestock_closestockfile(); //just in case it was already open

    // Check if filename has a path, if no path default to USER folder
    if(genfs_checkfilehaspath(filename) == S_SUCCESS)
    {
        strcpy((char *)fullfilepath, (const char*)filename);
    }
    else
    {
        genfs_userpathcorrection(filename, sizeof(fullfilepath), fullfilepath);
    }    
    
    filestock_info.control.readonly = 1;
    if (attrib[0] == 'r')
    {
        if (attrib[1] == '+')
        {
            filestock_info.filestock_fptr = genfs_general_openfile(fullfilepath,"r+");
            filestock_info.control.readonly = 0;
        }
        else
        {
            filestock_info.filestock_fptr = genfs_general_openfile(fullfilepath,"rb");
        }
    }
    else if (attrib[0] == 'w')
    {
        filestock_info.filestock_fptr = genfs_general_openfile(fullfilepath,"wb");
        filestock_info.control.readonly = 0;
    }
    else if (attrib[0] == 'a')
    {
        filestock_info.filestock_fptr = genfs_general_openfile(fullfilepath,"a+");
        filestock_info.control.readonly = 0;
    }
    else
        return S_INPUT;
    
    if (filestock_info.filestock_fptr == NULL) 
        return S_OPENFILE;
    
    if(filetype == FILESTOCK_USE_STOCKFILE)
    {
        filestock_info.control.filehandlingtype = FILESTOCK_HANDLING_STOCKFILE;
        filestock_info.key = (u32*)stockfile_tea_key;
    }
    else if(filetype == FILESTOCK_USE_FLASHFILE)
    {
        filestock_info.control.filehandlingtype = FILESTOCK_HANDLING_FLASHFILE;
        filestock_info.key = (u32*)flashfile_tea_key;
    }
    else
    {
        filestock_closestockfile(); 
        return S_INPUT;
    }
        
    return S_SUCCESS;
}

//------------------------------------------------------------------------------
// Inputs:  u8 *attrib ("wb","rb","r+","a+")
//          bool isspecialstock (TRUE: in upload only feature)
// Return:  u8  status
// Engineer: Quyen Leba
//------------------------------------------------------------------------------
u8 filestock_openstockfile(u8 *attrib, bool isspecialstock)
{
    u8  *filename;

    if (SETTINGS_IsDemoMode())
    {
        return S_SUCCESS;
    }

    if (isspecialstock)
        filename = SPECIAL_STOCK_FILENAME;
    else
        filename = STOCK_FILENAME;
    
    return filestock_openfile(filename, attrib, FILESTOCK_USE_STOCKFILE);
}

//------------------------------------------------------------------------------
// Close stock file if it's open
// Engineer: Quyen Leba
//------------------------------------------------------------------------------
u8 filestock_closestockfile()
{
    if (filestock_info.filestock_fptr)
    {
        genfs_closefile(filestock_info.filestock_fptr);
        filestock_info.filestock_fptr = NULL;
    }
    
    filestock_info.residuebufferlength = 0;
    filestock_info.control.filehandlingtype = FILESTOCK_HANDLING_NONE;
    filestock_info.key = NULL;
    return S_SUCCESS;
}

//------------------------------------------------------------------------------
// Input:   u8 *attrib ("wb","rb","r+","a+")
// Return:  u8  status
// Engineer: Quyen Leba
//------------------------------------------------------------------------------
u8 filestock_openflashfile(u8 *attrib)
{
    if (SETTINGS_IsDemoMode())
    {
        return S_SUCCESS;
    }

    return filestock_openfile(FLASH_FILENAME, attrib, FILESTOCK_USE_FLASHFILE);
}

//------------------------------------------------------------------------------
// Get the file pointer of filestock_openflashfile(...)
// Return:  F_FILE *filestock_info.filestock_fptr
// Engineer: Quyen Leba
//------------------------------------------------------------------------------
F_FILE * filestock_getflashfile_fptr()
{
    return filestock_info.filestock_fptr;
}

//------------------------------------------------------------------------------
// Close flash file if it's open
// Engineer: Quyen Leba
//------------------------------------------------------------------------------
u8 filestock_closeflashfile()
{
    if (filestock_info.filestock_fptr)
    {
        genfs_closefile(filestock_info.filestock_fptr);
        filestock_info.filestock_fptr = NULL;
    }
    filestock_info.residuebufferlength = 0;
    filestock_info.control.filehandlingtype = FILESTOCK_HANDLING_NONE;
    filestock_info.key = NULL;
    return S_SUCCESS;
}

//------------------------------------------------------------------------------
// Init stock file for upload session
// Input:   bool isspecialstock (TRUE: in upload only feature)
// Return:  u8  status
// Engineer: Quyen Leba
//------------------------------------------------------------------------------
u8 filestock_upload_init(bool isspecialstock)
{
    filestock_delete(isspecialstock);
    SETTINGS_TUNE(stockfilesize) = 0;
    SETTINGS_SetTuneAreaDirty();
    settings_update(SETTINGS_UPDATE_IF_DIRTY);
    return filestock_openstockfile("wb",isspecialstock);
}

//------------------------------------------------------------------------------
// Close stock upload session
// Engineer: Quyen Leba
//------------------------------------------------------------------------------
u8 filestock_upload_deinit()
{
    return filestock_closestockfile();
}

//------------------------------------------------------------------------------
// Prepare flash file for download session
// Engineer: Quyen Leba
//------------------------------------------------------------------------------
u8 filestock_download_init(FileStock_Filetype filetype)
{
    if (filetype == FILESTOCK_USE_FLASHFILE)
    {
        return filestock_openflashfile("r");
    }
    else    //FILESTOCK_USE_STOCKFILE
    {
        return filestock_openstockfile("r",FALSE);
    }
}

//------------------------------------------------------------------------------
// Close flash file after download session
// Engineer: Quyen Leba
//------------------------------------------------------------------------------
u8 filestock_download_deinit()
{
    filestock_closestockfile();
    filestock_closeflashfile();
    return S_SUCCESS;
}

//------------------------------------------------------------------------------
// Write data to stock file
// Inputs:  u8  *data
//          u32 datalength
// Return:  u8  status
// Engineer: Quyen Leba
// Note: residuebuffer will not be flushed into file when closed; we must work
// with stock & flash files with length multiple of 8 bytes.
//------------------------------------------------------------------------------
u8 filestock_write(u8 *data, u32 datalength)
{
    if (SETTINGS_IsDemoMode())
    {
        return S_SUCCESS;
    }

#if (FILESTOCK_USE_ENCRYPTION == 0)
    u32 bytecount;

    bytecount = fwrite((char*)data,
                       1,datalength,filestock_info.filestock_fptr);
    if (bytecount != datalength)
    {
        return S_WRITEFILE;
    }
    return S_SUCCESS;
#else
    u32 startposition;
    u32 writelength;
    u32 bytecount;
    u8 status;
    u8  l_data[FILESTOCK_DATABUF_SIZE];

    if(datalength <= FILESTOCK_DATABUF_SIZE)
    {
        if (filestock_info.filestock_fptr)
        {
            // Copy data to local buffer, encryption function will change local buffer data
            memcpy(l_data, data, datalength); 
            
            status = S_SUCCESS;
            writelength = (datalength/8)*8;
            if (filestock_info.residuebufferlength)
            {
                if (filestock_info.residuebufferlength + datalength < sizeof(filestock_info.residuebuffer))
                {
                    //data is too small, just to it to residue buffer
                    memcpy((char*)&filestock_info.residuebuffer[filestock_info.residuebufferlength],
                           (char*)l_data,datalength);
                    filestock_info.residuebufferlength += datalength;
                }
                else
                {
                    //take care residue data
                    startposition = sizeof(filestock_info.residuebuffer) - filestock_info.residuebufferlength;
                    memcpy((char*)&filestock_info.residuebuffer[filestock_info.residuebufferlength],
                           (char*)l_data,startposition);
                    tea_encryptblock(filestock_info.residuebuffer,
                                     sizeof(filestock_info.residuebuffer),
                                     filestock_info.key);
                    bytecount = fwrite((char*)filestock_info.residuebuffer,
                                       1,sizeof(filestock_info.residuebuffer),
                                       filestock_info.filestock_fptr);
                    if (bytecount == sizeof(filestock_info.residuebuffer))
                    {
                        filestock_info.residuebufferlength = 0;
                        //then write as many as possible with the rest of data
                        writelength = ((datalength-startposition)/8)*8;
                        tea_encryptblock(&l_data[startposition],writelength,
                                         filestock_info.key);
                        bytecount = fwrite((char*)&l_data[startposition],
                                           1,writelength,filestock_info.filestock_fptr);
                        if (bytecount == writelength)
                        {
                            //residue data
                            filestock_info.residuebufferlength = datalength - writelength - startposition;
                            memcpy((char*)filestock_info.residuebuffer,
                                   (char*)&l_data[writelength+startposition],
                                   filestock_info.residuebufferlength);
                        }
                        else
                        {
                            status = S_WRITEFILE;
                        }
                    }
                    else
                    {
                        status = S_WRITEFILE;
                    }
                }
            }
            else if (writelength == datalength)
            {
                tea_encryptblock(l_data,writelength,filestock_info.key);
                bytecount = fwrite((char*)l_data,
                                   1,writelength,filestock_info.filestock_fptr);
                if(writelength != bytecount)
                {
                    status = S_WRITEFILE;
                }
                
            }
            else if (writelength < datalength)
            {
                tea_encryptblock(l_data,writelength,filestock_info.key);
                bytecount = fwrite((char*)l_data,
                                   1,writelength,filestock_info.filestock_fptr);
                filestock_info.residuebufferlength = datalength - writelength;
                memcpy((char*)filestock_info.residuebuffer,
                       (char*)&l_data[writelength],filestock_info.residuebufferlength);
                if(writelength != bytecount)
                {
                    status = S_WRITEFILE;
                }
            }
            else
            {
                status = S_ERROR; //should not happen unless divider round up
            }
        }
        else
        {
            status = S_FAIL;
        }
    }
    else
    {
        status = S_INPUT;     // datalength too big
    }
    
    return status;
#endif
}

//------------------------------------------------------------------------------
// Read data from stock file
// Inputs:  u8  *data
//          u32 datalength
// Output:  u32 *bytecount
// Return:  u8  status
// Engineer: Quyen Leba
//------------------------------------------------------------------------------
u8 filestock_read(u8 *data, u32 datalength, u32 *bytecount)
{
    if (SETTINGS_IsDemoMode())
    {
        *bytecount = datalength;
        return S_SUCCESS;
    }

#if FILESTOCK_USE_ENCRYPTION
    u32 currentposition;
    u32 slotposition;
    u32 readlength;
    u32 total;
    u32 count;
    u8  buffer[8];
    u8  status;
#endif

    if (filestock_info.filestock_fptr == NULL)
    {
        return S_FAIL;
    }
#if (FILESTOCK_USE_ENCRYPTION == 0)
    *bytecount = fread((char*)data,1,datalength,filestock_info.filestock_fptr);
    return S_SUCCESS;
#else
    total = 0;
    *bytecount = 0;
    status = filestock_get_position(&currentposition);
    if (status == S_SUCCESS)
    {
        slotposition = (currentposition/8)*8;
        if (slotposition == currentposition)
        {
            readlength = (datalength/8)*8;
            total = fread((char*)data,1,readlength,filestock_info.filestock_fptr);
            tea_decryptblock(data,total,filestock_info.key);

            if (total == datalength)
            {
                //do nothing
            }
            else if (total < datalength)    //tail is out of alignment
            {
                count = fread((char*)buffer,1,sizeof(buffer),filestock_info.filestock_fptr);
                if (count != sizeof(buffer))
                {
                    return S_READFILE;
                }
                tea_decryptblock(buffer,sizeof(buffer),filestock_info.key);
                memcpy((char*)&data[total],(char*)buffer,datalength-total);

                if (filestock_set_position(currentposition+datalength) != S_SUCCESS)
                {
                    return S_SEEKFILE;
                }
                total = datalength;
            }//else if (total < datalength)...
            else
            {
                return S_ERROR;
            }
        }
        else if (slotposition < currentposition)
        {
            u32 startoffset;

            //head is out of alignment
            startoffset = currentposition - slotposition;
            if (filestock_set_position(slotposition) != S_SUCCESS)
            {
                return S_SEEKFILE;
            }
            count = fread((char*)buffer,
                          1,sizeof(buffer),filestock_info.filestock_fptr);
            tea_decryptblock(buffer,sizeof(buffer),filestock_info.key);
            total = sizeof(buffer)-startoffset;
            if (total > datalength)
            {
                total = datalength;
            }
            memcpy((char*)data,(char*)&buffer[startoffset],total);

            //reading mid section; always in alignment
            readlength = ((datalength-total)/8)*8;
            count = fread((char*)&data[total],
                          1,readlength,filestock_info.filestock_fptr);
            tea_decryptblock(&data[total],readlength,filestock_info.key);
            total += count;

            if (total == datalength)
            {
                //do nothing
            }
            else if (total < datalength)
            {
                //tail is out of alignment
                count = fread((char*)buffer,
                              1,sizeof(buffer),filestock_info.filestock_fptr);
                tea_decryptblock(buffer,sizeof(buffer),filestock_info.key);
                memcpy((char*)&data[total],(char*)buffer,datalength-total);
                if (filestock_set_position(currentposition+datalength) != S_SUCCESS)
                {
                    return S_SEEKFILE;
                }
                total = datalength;
            }//else if (total < datalength)...
            else
            {
                //should never happen
                return S_ERROR;
            }
        }
        else
        {
            //should not happen
            return S_ERROR;
        }
    }
    *bytecount = total;
    return S_SUCCESS;
#endif
}

//------------------------------------------------------------------------------
// Read data starting from a position
// Inputs:  u32 position
//          u8  *data
//          u32 datalength
// Output:  u32 *bytecount
// Return:  u8  status
// Engineer: Quyen Leba
//------------------------------------------------------------------------------
u8 filestock_read_at_position(u32 position, u8 *data, u32 datalength,
                              u32 *bytecount)
{
    u8  status;

    if (SETTINGS_IsDemoMode())
    {
        *bytecount = datalength;
        return S_SUCCESS;
    }

    status = filestock_set_position(position);
    if (status != S_SUCCESS)
    {
        return S_SEEKFILE;
    }
    status = filestock_read(data,datalength,bytecount);
    if (status != S_SUCCESS)
    {
        return S_READFILE;
    }
    return S_SUCCESS;
}

//------------------------------------------------------------------------------
// Forward read/write position in stock file
// Input:   u32 count (# of bytes to skip, for now: must = n x 512)
// Return:  u8  status
// Engineer: Quyen Leba
//------------------------------------------------------------------------------
u8 filestock_forward(u32 count)
{
    u32 pos;

    if (SETTINGS_IsDemoMode())
    {
        return S_SUCCESS;
    }

    if (filestock_info.filestock_fptr == NULL)
    {
        return S_FAIL;
    }

    pos = ftell(filestock_info.filestock_fptr);
    pos += count;
    if (fseek(filestock_info.filestock_fptr,pos,SEEK_SET) == 0)
    {
        return S_SUCCESS;
    }
    else
    {
        return S_SEEKFILE;
    }
}

//------------------------------------------------------------------------------
// Set backward read/write position in stock file
// Input:   u32 count (# of bytes to go back, for now: must = n x 512)
// Return:  u8  status
// Engineer: Quyen Leba
//------------------------------------------------------------------------------
u8 filestock_backward(u32 count)
{
    u32 pos;

    if (SETTINGS_IsDemoMode())
    {
        return S_SUCCESS;
    }

    if (filestock_info.filestock_fptr == NULL)
    {
        return S_FAIL;
    }

    pos = ftell(filestock_info.filestock_fptr);
    pos -= count;
    if (fseek(filestock_info.filestock_fptr,pos,SEEK_SET) == 0)
    {
        return S_SUCCESS;
    }
    else
    {
        return S_SEEKFILE;
    }
}

//------------------------------------------------------------------------------
// Fill stock with count of byte
// Inputs:  u8  byte        data to fill
//          u32 count       fill with # of count
// Return:  u8  status
// Engineer: Quyen Leba
//------------------------------------------------------------------------------
u8 filestock_fill(u8 byte, u32 count)
{
    u32         len = count;
    u8          *buf;

    if (SETTINGS_IsDemoMode())
    {
        return S_SUCCESS;
    }

    if (filestock_info.filestock_fptr == NULL)
    {
        return S_FAIL;
    }

    buf = __malloc(2048);
    if (buf == NULL)
    {
        return S_MALLOC;
    }
    memset((void*)buf,byte,2048);

    while(len != 0)
    {
        //TODOQ: check if fwrite is successful
        if (len >= 2048)
        {
#if FILESTOCK_USE_ENCRYPTION
            tea_encryptblock(buf,2048,filestock_info.key);
#endif
            fwrite((char*)buf,1,2048,filestock_info.filestock_fptr);
            len -= 2048;
        }
        else
        {
#if FILESTOCK_USE_ENCRYPTION
            tea_encryptblock(buf,len,filestock_info.key);
#endif
            fwrite((char*)buf,1,len,filestock_info.filestock_fptr);
            len = 0;
        }
    }
    __free(buf);
    return S_SUCCESS;
}

//------------------------------------------------------------------------------
// Input:   u32 pos         current position(pointer) of stock/flash file
// Return:  u8  status      S_SUCCESS
// Enginner: Quyen Leba
//------------------------------------------------------------------------------
u8 filestock_set_position(u32 pos)
{
    u8 status;

    if (SETTINGS_IsDemoMode())
    {
        return S_SUCCESS;
    }

    if(filestock_info.filestock_fptr)
    {
        if (fseek(filestock_info.filestock_fptr,pos,SEEK_SET) == 0)
        {
            status = S_SUCCESS;
        }
        else
        {
            status = S_SEEKFILE;
        }
    }
    else
    {
        status = S_FAIL;
    }
    
    return status;
}

//------------------------------------------------------------------------------
// Output:  u32 pos         current position(pointer) of stock/flash file
// Return:  u8  status      S_SUCCESS
// Enginner: Quyen Leba
//------------------------------------------------------------------------------
u8 filestock_get_position(u32 *pos)
{
    u8 status;

    if (SETTINGS_IsDemoMode())
    {
        *pos = 0;
        return S_SUCCESS;
    }

    if(filestock_info.filestock_fptr)
    {
        *pos = ftell(filestock_info.filestock_fptr);
        status = S_SUCCESS;
    }
    else
    {
        status = S_FAIL;
    }
    
    return status;
}

//------------------------------------------------------------------------------
// Output:  u32 *size       current size of stock.bin
// Return:  u8  status      S_SUCCESS, S_FAIL
// Engineer: Quyen Leba
//------------------------------------------------------------------------------
u8 filestock_getfilesize(u32 *size)
{
    F_FILE *file;

    *size = 0;

    if (SETTINGS_IsDemoMode())
    {
        return S_SUCCESS;
    }

    file = genfs_user_openfile(STOCK_FILENAME,"rb");
    if (file == NULL)
    {
        return S_OPENFILE;
    }
    if (fseek(file,0,SEEK_END) != 0)
    {
        genfs_closefile(file);
        return S_SEEKFILE;
    }

    *size = ftell(file);
    genfs_closefile(file);
    return S_SUCCESS;
}

//------------------------------------------------------------------------------
// Copy stock file to flash file
// Return:  u8  status
// Engineer: Quyen Leba
//------------------------------------------------------------------------------
u8 filestock_generate_flashfile_fromstock(file_copy_progressreport progressreport_funcptr)
{
    if (SETTINGS_IsDemoMode())
    {
        return S_SUCCESS;
    }

#if FILESTOCK_USE_ENCRYPTION
    file_blockcrypto stockcrypto;
    file_blockcrypto flashcrypto;

    stockcrypto.simplecrypto.type = file_blockcrypto_type_tea;
    stockcrypto.simplecrypto.key = (u32*)stockfile_tea_key;
    stockcrypto.simplecrypto.crypto = &tea_decryptblock;
    
    flashcrypto.simplecrypto.type = file_blockcrypto_type_tea;
    flashcrypto.simplecrypto.key = (u32*)flashfile_tea_key;
    flashcrypto.simplecrypto.crypto = &tea_encryptblock;

    //IMPORTANT: this can generate a progress-report session-drop event
    return file_copy(STOCK_FILENAME,FLASH_FILENAME,
                     &stockcrypto,&flashcrypto,progressreport_funcptr);
#else
    return file_copy(STOCK_FILENAME,FLASH_FILENAME,
                     NULL,NULL,progressreport_funcptr);
#endif
}

/**
 *  filestock_generate_flashfile_fromstock_advance
 *  
 *  @brief Generate flash file from SPF (if available) and stock file
 *  
 *  @param [in] basefilestockinfo NULL allowed
 *  @param [in] progressreport_funcptr
 *
 *  @retval u8  status
 *  
 *  @details filesize of basefilestockinfo must always be valid if basefilestockinfo is used. All must be %8==0 for now.
 *  
 *  @authors Quyen Leba
 */
u8 filestock_generate_flashfile_fromstock_advance(file_info *basefilestockinfo,
                                                  file_copy_progressreport progressreport_funcptr)
{
    file_blockcrypto *stockcrypto_ptr;
    file_blockcrypto *flashcrypto_ptr;
    u32 bytecount;
    u32 crc32e_calc;
    u8  i;
    u8  status;

    if (SETTINGS_IsDemoMode())
    {
        return S_SUCCESS;
    }

    genfs_deletefile(FLASH_FILENAME);

#if FILESTOCK_USE_ENCRYPTION
    file_blockcrypto stockcrypto;
    file_blockcrypto flashcrypto;

    stockcrypto.simplecrypto.type = file_blockcrypto_type_tea;
    stockcrypto.simplecrypto.key = (u32*)stockfile_tea_key;
    stockcrypto.simplecrypto.crypto = &tea_decryptblock;
    
    flashcrypto.simplecrypto.type = file_blockcrypto_type_tea;
    flashcrypto.simplecrypto.key = (u32*)flashfile_tea_key;
    flashcrypto.simplecrypto.crypto = &tea_encryptblock;

    stockcrypto_ptr = &stockcrypto;
    flashcrypto_ptr = &flashcrypto;
#else
    stockcrypto_ptr = NULL;
    flashcrypto_ptr = NULL;
#endif

    if (basefilestockinfo == NULL)
    {
        //IMPORTANT: this can generate a progress-report session-drop event
        return file_copy(STOCK_FILENAME,FLASH_FILENAME,
                         stockcrypto_ptr,flashcrypto_ptr,progressreport_funcptr);
    }
    else
    {
        bytecount = 0;  //track current byte count of flash file
        for(i=0;i<ECM_MAX_COUNT;i++)
        {
            if (basefilestockinfo[i].filename != NULL && isgraph(basefilestockinfo[i].filename[0]))
            {
                if (!file_isexist(basefilestockinfo[i].filename))
                {
                    return S_FILENOTFOUND;
                }

                if (file_getsize_byname(basefilestockinfo[i].filename) != basefilestockinfo[i].filesize)
                {
                    return S_BADCONTENT;
                }

                status = file_calcfilecrc32e_byfilename(basefilestockinfo[i].filename,&crc32e_calc,0x00);
                if (status != S_SUCCESS)
                {
                    return status;
                }

                if (crc32e_calc != basefilestockinfo[i].filecrc32e /*&& basefilestockinfo[i].filecrc32e != 0*/)
                {
                    return S_CRC32E;
                }

                //append the SPF of this ECM to flash file
                status = file_append(basefilestockinfo[i].filename,FLASH_FILENAME,0,0,
                                     stockcrypto_ptr,flashcrypto_ptr,progressreport_funcptr);
                if (status != S_SUCCESS)
                {
                    return status;
                }
            }
            else if (basefilestockinfo[i].filesize > 0)
            {
                //append vehicle stock data of this ECM to flash file
                status = file_append(STOCK_FILENAME,FLASH_FILENAME,
                                     bytecount,basefilestockinfo[i].filesize,
                                     stockcrypto_ptr,flashcrypto_ptr,progressreport_funcptr);
                if (status != S_SUCCESS)
                {
                    return status;
                }
            }

            bytecount += basefilestockinfo[i].filesize;
        }//for(i=...
    }
    return S_SUCCESS;
}

//------------------------------------------------------------------------------
// Copy a file to flash file
// Return:  u8  status
// Engineer: Quyen Leba
//------------------------------------------------------------------------------
u8 filestock_generate_flashfile_fromfile(u8 *filename,
                                         file_copy_progressreport progressreport_funcptr)
{
    if (SETTINGS_IsDemoMode())
    {
        return S_SUCCESS;
    }

    //IMPORTANT: this can generate a progress-report session-drop event
    return file_copy(filename,FLASH_FILENAME,NULL,NULL,
                     progressreport_funcptr);
}

//------------------------------------------------------------------------------
// Apply changes to flash flash
// When use this function, use filestock_openflashfile(...) first
// Inputs:  u32 position        (byte position)
//          u32 *data
//          u32 length          # of bytes to change
// Return:  u8  status
// Engineer: Quyen Leba
// Date: Feb 29, 2008
//------------------------------------------------------------------------------
u8 filestock_updateflashfile(u32 position, u8 *pdata, u32 length)
{
    u32 total;
    u32 count;
    u32 slotposition;
    u8  buffer[8];
    u32 bufferlength;
    u8 status;
    u8 data[FILESTOCK_DATABUF_SIZE];

    if (SETTINGS_IsDemoMode())
    {
        return S_SUCCESS;
    }

    if (filestock_info.filestock_fptr == NULL)
    {
        return S_BADCONTENT;
    }
    
    if(length > FILESTOCK_DATABUF_SIZE)
    {
        return S_INPUT;     // datalength too big
    }
    
    memcpy(data,pdata,length);
    
    total = 0;
    slotposition = (position/8)*8;  //closest left position in multiple of 8

    if (fseek(filestock_info.filestock_fptr,slotposition,SEEK_SET) != 0)
    {
        status = S_SEEKFILE;
        goto filestock_updateflashfile_done;
    }
    if (slotposition == position)   //position is in alignment
    {
        u32 slotlength;

        slotlength = (length/8)*8;
#if FILESTOCK_USE_ENCRYPTION
        tea_encryptblock(data,slotlength,(u32*)flashfile_tea_key);
#endif
        total = fwrite(data,1,slotlength,filestock_info.filestock_fptr);
        if (total != slotlength)
        {
            status = S_WRITEFILE;
            goto filestock_updateflashfile_done;
        }
        if (total == length)
        {
            //do nothing
        }
        else if (total < length)    //tail is out of aligment
        {
            bufferlength = fread(buffer,1,sizeof(buffer),filestock_info.filestock_fptr);
            if (bufferlength)
            {
                tea_decryptblock(buffer,bufferlength,(u32*)flashfile_tea_key);
                memcpy(buffer,&data[total],length-total);

                if (fseek(filestock_info.filestock_fptr,position+total,SEEK_SET) != 0)
                {
                    status = S_SEEKFILE;
                    goto filestock_updateflashfile_done;
                }
#if FILESTOCK_USE_ENCRYPTION
                tea_encryptblock(buffer,bufferlength,(u32*)flashfile_tea_key);
#endif
                count = fwrite(buffer,1,bufferlength,filestock_info.filestock_fptr);
                if (count != bufferlength)
                {
                    status = S_WRITEFILE;
                    goto filestock_updateflashfile_done;
                }
                total = length;

                if (fseek(filestock_info.filestock_fptr,position+length,SEEK_SET) != 0)
                {
                    status = S_SEEKFILE;
                    goto filestock_updateflashfile_done;
                }
            }
            else
            {
                //only allow to update existing data
                status = S_READFILE;
                goto filestock_updateflashfile_done;
            }
        }//elseif (slotlength < length)...
        else
        {
            //should never happen
            status = S_ERROR;
            goto filestock_updateflashfile_done;
        }
    }//if (slotposition == position)...
    else if (slotposition < position)
    {
        u32 startoffset;
        u32 midlength;
        u32 tmpu32;

        //head is out of alignment
        startoffset = position - slotposition;
        bufferlength = fread(buffer,1,sizeof(buffer),filestock_info.filestock_fptr);
        tea_decryptblock(buffer,bufferlength,(u32*)flashfile_tea_key);
        tmpu32 = bufferlength-startoffset;
        if (length <= tmpu32)
        {
            //update data is very small
            tmpu32 = length;
        }
        memcpy(&buffer[startoffset],data,tmpu32);   //update new data
        
        if (fseek(filestock_info.filestock_fptr,slotposition,SEEK_SET) != 0)
        {
            status = S_SEEKFILE;
            goto filestock_updateflashfile_done;
        }
#if FILESTOCK_USE_ENCRYPTION
        tea_encryptblock(buffer,bufferlength,(u32*)flashfile_tea_key);
#endif
        count = fwrite(buffer,1,bufferlength,filestock_info.filestock_fptr);
        if (count != bufferlength)
        {
            status = S_WRITEFILE;
            goto filestock_updateflashfile_done;
        }
        total += tmpu32;

        midlength = ((length-total)/8)*8;
#if FILESTOCK_USE_ENCRYPTION
        tea_encryptblock(&data[total],midlength,(u32*)flashfile_tea_key);
#endif
        count = fwrite(&data[total],1,midlength,filestock_info.filestock_fptr);
        if (count != midlength)
        {
            status = S_WRITEFILE;
            goto filestock_updateflashfile_done;
        }
        total += midlength;

        if (total == length)
        {
            //do nothing
        }
        else if (total < length)   //tail is out of alignment
        {
            bufferlength = fread(buffer,1,sizeof(buffer),filestock_info.filestock_fptr);
            tea_decryptblock(buffer,bufferlength,(u32*)flashfile_tea_key);
            memcpy(buffer,&data[total],length-total);

            if (fseek(filestock_info.filestock_fptr,position+total,SEEK_SET) != 0)
            {
                status = S_SEEKFILE;
                goto filestock_updateflashfile_done;
            }
#if FILESTOCK_USE_ENCRYPTION
            tea_encryptblock(buffer,bufferlength,(u32*)flashfile_tea_key);
#endif
            count = fwrite(buffer,1,bufferlength,filestock_info.filestock_fptr);
            if (count != bufferlength)
            {
                status = S_WRITEFILE;
                goto filestock_updateflashfile_done;
            }
            if (fseek(filestock_info.filestock_fptr,position+length,SEEK_SET) != 0)
            {
                status = S_SEEKFILE;
                goto filestock_updateflashfile_done;
            }
            total = length;
        }
        else
        {
            //should never happen
            status = S_ERROR;
            goto filestock_updateflashfile_done;
        }
    }//else if (slotposition < position)...
    else
    {
        //should never happen
        status = S_ERROR;
        goto filestock_updateflashfile_done;
    }
    if (total == length)
    {
        status = S_SUCCESS;
    }

filestock_updateflashfile_done:
    return status;
}

//------------------------------------------------------------------------------
// Append a file to stock file
// Input:   u8  *filename
// Output:  u32 *appendlength
// Return:  u8  status
// Engineer: Quyen Leba
// Note: do not use if there's filestock_info.residuebuffer
//------------------------------------------------------------------------------
u8 filestock_append_file(u8 *filename, u32 *appendlength)
{
    F_FILE *fptr;
    u8  buffer[2048];
    u32 bufferlength;
    u32 bytecount;
    u8  status;

    if (SETTINGS_IsDemoMode())
    {
        *appendlength = 0;
        return S_SUCCESS;
    }

    fptr = NULL;
    bytecount = 0;
    if (filestock_info.residuebufferlength)
    {
        status = S_ERROR;
        goto filestock_append_file_done;
    }
    fptr = genfs_user_openfile(filename,"rb");
    if (fptr == NULL)
    {
        status = S_OPENFILE;
        goto filestock_append_file_done;
    }
    
    while(!(feof(fptr)))
    {
        bufferlength = fread(buffer,1,2048,fptr);
        if (bufferlength == 0)
        {
            status = S_READFILE;
            goto filestock_append_file_done;
        }
        status = filestock_write(buffer,bufferlength);
        if (status != S_SUCCESS)
        {
            status = S_WRITEFILE;
            goto filestock_append_file_done;
        }
        bytecount += bufferlength;
    }
    
filestock_append_file_done:
    if (fptr)
    {
        genfs_closefile(fptr);
    }
    *appendlength = bytecount;
    return status;
}

//------------------------------------------------------------------------------
// Validate stock/flash file
// Input:   FileStock_Filetype filetype
// Output:  u32 *crc32e (NULL allowed)
// Return:  u8  status
// Engineer: Quyen Leba
//------------------------------------------------------------------------------
u8 filestock_validate(FileStock_Filetype filetype, u32 *crc32e)
{
#if FILESTOCK_SKIP_VALIDATE_STOCK_FILE
#ifdef __DEBUG_JTAG_ 
#warning filestock.c: Skip validate stock -- very dangerous -- debug only
#else
#error filestock.c: Skip validate stock !!!
#endif
    return S_SUCCESS;
#else
    F_FILE *fptr;
    u8  buffer[2048];
    u32 bytecount;
    u32 crc32e_calc;
    u32 crc32e_cmp;

    if (SETTINGS_IsDemoMode())
    {
        *crc32e = 0;
        return S_SUCCESS;
    }

    if (crc32e)
    {
        *crc32e = 0;
    }
    if (filetype == FILESTOCK_USE_FLASHFILE)
    {
        fptr = genfs_user_openfile(FLASH_FILENAME,"r");
        crc32e_cmp = SETTINGS_TUNE(flashfile_crc32e);
    }
    else
    {
        fptr = genfs_user_openfile(STOCK_FILENAME,"r");
        crc32e_cmp = SETTINGS_TUNE(stockfile_crc32e);
    }
    if (!fptr)
    {
        return S_OPENFILE;
    }

    crc32e_reset();
    crc32e_calc = 0xFFFFFFFF;
    while(!feof(fptr))
    {
        bytecount = fread(buffer,1,sizeof(buffer),fptr);
        crc32e_calc = crc32e_calculateblock(crc32e_calc,(u32*)buffer,bytecount/4);
    }
    if (crc32e)
    {
        *crc32e = crc32e_calc;
    }
    genfs_closefile(fptr);

    if (crc32e_cmp == crc32e_calc)
    {
        return S_SUCCESS;
    }
    return S_CRC32E;
#endif
}

//------------------------------------------------------------------------------
// Overlay tune file with stock.bin file
// Input:   F_FILE *fptr
// Return:  u8  status
// Engineer: Ricardo Cigarroa
//------------------------------------------------------------------------------
u8 filestock_overlay_flashfile(u32 position, u32 length, 
                               file_copy_progressreport progressreport_funcptr)
{
    if (SETTINGS_IsDemoMode())
    {
        return S_SUCCESS;
    }

#if FILESTOCK_USE_ENCRYPTION
    file_blockcrypto stockcrypto;
    file_blockcrypto flashcrypto;
    
    stockcrypto.simplecrypto.type = file_blockcrypto_type_tea;
    stockcrypto.simplecrypto.key = (u32*)stockfile_tea_key;
    stockcrypto.simplecrypto.crypto = &tea_decryptblock;
    
    flashcrypto.simplecrypto.type = file_blockcrypto_type_tea;
    flashcrypto.simplecrypto.key = (u32*)flashfile_tea_key;
    flashcrypto.simplecrypto.crypto = &tea_encryptblock;
    
    return file_copy_overlay(position, length, STOCK_FILENAME, FLASH_FILENAME, 
                             &stockcrypto, &flashcrypto,progressreport_funcptr);
#else
    return file_copy_overlay(position, length, STOCK_FILENAME, FLASH_FILENAME, 
                             NULL, NULLL, progressreport_funcptr);
#endif
}
//------------------------------------------------------------------------------
// Check bootloader position, other tuners move it from 0x20000 to 0x30000
// using an offset allows for furture changes in positions.
// Input:   None
// Return:  u8  status
// Engineer: Rhonda Rusak
//------------------------------------------------------------------------------
u8 filestock_bootloader_position_check(void)
{
  u8 buffer[4];
  u8 status = 0;
  u32 bytes_read = 0;

  memset((void*)buffer,0,4);

  status =filestock_openstockfile("rb", FALSE);
  filestock_read_at_position(0x20000, buffer, 4, &bytes_read);
  if(buffer[0] == 0xFF)
  {
    filestock_read_at_position(0x30000, buffer, 4, &bytes_read);
    if(buffer[0] == 0xFF)
    {
        settingstune.DCX_Bootloader_offset = 0;
        status = S_FAIL;
    }
    else
      settingstune.DCX_Bootloader_offset = 0x10000;
  }
  else
    settingstune.DCX_Bootloader_offset = 0;

  filestock_closestockfile();

  SETTINGS_SetTuneAreaDirty();
  settings_update(SETTINGS_UPDATE_IF_DIRTY);

  return status;
}