/**
  **************** (C) COPYRIGHT 2011 SCT Performance, LLC *********************
  * File Name          : checksum_csf.h
  * Device / System    : SCT products using the "CommonCode" platform
  * Author             : Ricardo Cigarroa
  *
  * Version            : 1 
  * Date               : 03/14/2014
  * Description        : 
  *                    : 
  *
  *
  * History            : 03/14/2014 R. Cigarroa
  *                    :   Add SCT Copyright header,
  *
  ******************************************************************************
  */

#ifndef __CHECKSUM_CSF_H
#define __CHECKSUM_CSF_H

#include <arch/gentype.h>
#include <fs/genfs.h>
#include <common/cmdif.h>

/*
.CSF file, (CheckSum File)

1.    Whole file encrypted with BF Data 
2.    All reserved bytes filled with 0x00
3.    CsfBlocks must be in correct order of operations for cases of nested/overlapping checksums.
4.    Preloaded files are non-stacked address data
5.    Custom tune files are stacked address data
*/

#define MAX_CSF_BLOCK_COUNT          10
#define MAX_CSF_BLOCK_SIZE           1024
#define CSFTEMPFILE "\\USER\\checksumfiletemp.csf"

// .CSF File Header (partnumber.csf)  Ex: 68057126AF.csf
typedef struct
{
#define CSF_VERSION           2
    u8  version;           // Version 2
    u8  reserved[3];
    u32 flags;             // Reserved
    u32 header_crc32e;
    u32 content_crc32e;
    u16 checksumcount;     // Total count of CsfBlocks
    u8  reserved_header[14];
}Csf_Header;
STRUCT_SIZE_CHECK(Csf_Header,32);

typedef struct
{
    u16 type;              //Checksum type enum
    u16 flags;             //Reserved
    u32 location;          //Binary Address in file where checksum is saved
    u32 starting_address;  //Binary address to start calculation in file (Preloaded Tune Non Stacked, Custom Tune Stacked)
    u32 length;            //Length of data to be calculated
}Csf_Block_Info;
STRUCT_SIZE_CHECK(Csf_Block_Info,16);

typedef struct
{
    Csf_Block_Info info;
    u8  reserved[48];
}Csf_Block_Generic;
STRUCT_SIZE_CHECK(Csf_Block_Generic,64);

typedef struct
{
    Csf_Block_Info info;
    u8  blockdata[496];
}Csf_Block_BlockReplacement_496;
STRUCT_SIZE_CHECK(Csf_Block_BlockReplacement_496,512);

typedef struct
{
    Csf_Block_Info info;
    u8  blockdata[5632];
}Csf_Block_BlockReplacement_5632;
STRUCT_SIZE_CHECK(Csf_Block_BlockReplacement_5632,5648);

typedef struct
{
    u32 segment_block_start;
    u32 segment_block_end;
}Segment_Info;


typedef enum
{
    CRC32                = 0, 
    Hemicrc16_Block      = 1,
    Level2_Sum_Block     = 2,
    Equalizer_Sum_Block  = 3,
    RSA_Signature_Block  = 4,
    IPC                  = 5,
    Trans_Info           = 6,
    ETC                  = 7,
}ChecksumBlockType;



u8 checksum_csf_apply_to_tunefile(F_FILE *tune_fptr, u8 *ipc_filename, 
                                  u16 ecm_type, bool ischeckonly);
u8 checksum_csf_validate_file(u8 *csf_filename, u16 *checksumcount, u8 *response);
u8 checksum_csf_process_header(Csf_Header *csfheader, u32 *crc32e);
u8 checksum_csf_getcsffileinfo(u8 *csf_filename, bool ischeckonly, u16 ecm_type, 
                               F_FILE *tune_fptr);
u8 checksum_csf_process_blocks(F_FILE *csf_fptr, F_FILE *file_fptr, u16 ecm_type, 
                               bool ischeckonly, u16 block_type, u32 address, 
                               Csf_Block_Info block_info);
u8 checksum_csf_process_hemicrc16_block(Csf_Block_Info block_info, 
                                        F_FILE *file_fptr, bool ischeckonly);
u8 checksum_csf_process_level2_sum_block(Csf_Block_Info block_info, 
                                         F_FILE *file_fptr, bool ischeckonly);
u8 checksum_csf_process_equalizer_sum_block(Csf_Block_Info block_info, F_FILE *file_fptr, 
                                            u16 ecm_type, bool ischeckonly); 
u8 checksum_csf_process_rsa_signature_block(F_FILE *file_fptr, F_FILE *csf_fptr, 
                                            u16 ecm_type, bool ischeckonly, 
                                            u32 address);
u8 checksum_csf_calcualte_crc16(u16 *crc, u32 segment_start, u32 segment_end, 
                                bool bigendian);
u8 checksum_csf_calcualte_etc_checksum(u32 *checksum, u32 segment_start, 
                                       u32 length);
u8 checksum_csf_calcualte_level2_sum_block(int *level2_calc_cksum, 
                                           Csf_Block_Info block_info, 
                                           F_FILE *file_fptr);
u8 checksum_csf_calculate_equalizer_sum(u32 start_address, u32 end_address, 
                                        F_FILE *file_fptr, Csf_Block_Info block_info);
u8 checksum_csf_calculate_rsa_modified_bytes(F_FILE *file_fptr, u16 ecm_type,
                                             F_FILE *csf_fptr, bool ischeckonly,
                                             u32 address);
u8 checksum_csf_calculate_ee2_crc32(F_FILE *file_fptr, u32 start_address, 
                                    u32 end_address, u8 *rsa_buffer, bool is_stock_key);
u8 checksum_csf_updatefile(F_FILE *csf_fptr, u32 position, u8 *pdata, u32 length);
u8 checksum_csf_process_etc_block(Csf_Block_Info block_info, F_FILE *file_fptr, 
                                  u16 ecm_type, bool ischeckonly);

#endif    //__CHECKSUM_CSF_H
