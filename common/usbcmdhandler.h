/**
  **************** (C) COPYRIGHT 2011 SCT Performance, LLC *********************
  * File Name          : usbcmdhandler.h
  * Device / System    : SCT products using the "CommonCode" platform
  * Author             : Quyen Leba
  *
  * Version            : 1 
  * Date               : 05/02/2011
  * Description        : 
  *                    : 
  *
  *
  * History            : 05/02/2011 M. Davis
  *                    :   Add SCT Copyright header,
  *
  ******************************************************************************
  */

#ifndef __USBCMDHANDLER_H
#define __USBCMDHANDLER_H

#include <arch/gentype.h>
#include <device_config.h>

#if CONFIG_FOR_TEST
#define SUPPORT_USBCMD_FILETRANSFER             0
#define SUPPORT_USBCMD_FULL_GETSETTINGS         0
#define SUPPORT_USBCMD_FULL_SETSETTINGS         0
#define SUPPORT_USBCMD_DATALOG                  0
#define SUPPORT_USBCMD_TUNE                     0
#define SUPPORT_USBCMD_HARDWARE                 0
#else
#define SUPPORT_USBCMD_FILETRANSFER             1
#define SUPPORT_USBCMD_FULL_GETSETTINGS         1
#define SUPPORT_USBCMD_FULL_SETSETTINGS         1
#define SUPPORT_USBCMD_DATALOG                  1
#define SUPPORT_USBCMD_TUNE                     1
#define SUPPORT_USBCMD_HARDWARE                 1
#endif

typedef enum
{
    UCH_USBCommand_ForceUnHalt                  = 0x09,
    UCH_USBCommand_Security                     = 0x10,
    UCH_USBCommand_Hardware                     = 0x11,

    UCH_USBCommand_OpenFile                     = 0x30,
    UCH_USBCommand_WriteFile                    = 0x31,
    UCH_USBCommand_ReadFile                     = 0x32,
    UCH_USBCommand_CloseFile                    = 0x33,
    UCH_USBCommand_CheckFile                    = 0x34,

    UCH_USBCommand_RecoveryStock_Check          = 0x40,
    UCH_USBCommand_RecoveryStock_Register       = 0x41,

    UCH_USBCommand_Datalog                      = 0x50,
    UCH_USBCommand_Tune                         = 0x51,
    UCH_USBCommand_GetSettings                  = 0x60,
    UCH_USBCommand_SetSettings                  = 0x61,

    UCH_USBCommand_Bootloader                   = 0xB3,
}UCH_USBCommand;

typedef enum
{
    USBCmdSecurity_Seed                         = 0x0032,
    USBCmdSecurity_Key                          = 0x00C2,
    USBCmdSecurity_Lock                         = 0x0082,
}USBCmdSecurityFlags;

typedef enum
{
    USBCmdBootloader_SetBoot                    = 0x0030,
    USBCmdBootloader_Init                       = 0x0031,
    USBCmdBootloader_Do                         = 0x0032,
    USBCmdBootloader_Validate                   = 0x0033,
    USBCmdBootloader_ClearEngSign               = 0x0034,
}USBCmdBootloaderFlags;

typedef enum
{
    //UCH_USBCommand_GetSettings & UCH_USBCommand_SetSettings
    USBCmdSettings_BootMode                     = 0x0040,
    USBCmdSettings_FailsafeBootVersion          = 0x0050,
    USBCmdSettings_MainBootVersion              = 0x0051,
    USBCmdSettings_AppVersion                   = 0x0052,
    USBCmdSettings_AppSignature                 = 0x0053,
    USBCmdSettings_SerialNumber                 = 0x0054,
    USBCmdSettings_FailsafeBootCRC32E           = 0x0055,
    USBCmdSettings_MainBootCRC32E               = 0x0056,
    USBCmdSettings_AppCRC32E                    = 0x0057,
    USBCmdSettings_CustomPID                    = 0x0058,
    USBCmdSettings_Flags                        = 0x0059,
    
    USBCmdSettings_DevicePartNumberString       = 0x0060,
    USBCmdSettings_DeviceType                   = 0x0061,
    USBCmdSettings_MarketType                   = 0x0062,
    USBCmdSettings_ProgrammerInfoP1             = 0x0063,
    USBCmdSettings_ProgrammerInfoP2             = 0x0064,
    USBCmdSettings_ProgrammerInfoP3             = 0x0065,
    USBCmdSettings_ProgrammerInfoP4             = 0x0066,
    USBCmdSettings_PreloadedTuneLookupCRC32E    = 0x0067,
    USBCmdSettings_PreloadedTuneRestriction     = 0x0068,
    USBCmdSettings_DemoMode                     = 0x0069,
    USBCmdSettings_ProductionTestStatus         = 0x006A,
    
    USBCmdSettings_VIN                          = 0x0080,
    USBCmdSettings_PreviousVIN                  = 0x0081,
    USBCmdSettings_VID                          = 0x0082,
    USBCmdSettings_EPATS                        = 0x0083,
    USBCmdSettings_StockFilesize                = 0x0084,
    USBCmdSettings_VehicleCodes                 = 0x0085,
    USBCmdSettings_VehicleSerialNumber          = 0x0086,
    USBCmdSettings_TuneInfoTrack                = 0x0087,
    USBCmdSettings_TuneHistoryInfo              = 0x0088,
    USBCmdSettings_DeviceCriticalFlags          = 0x0089,
    USBCmdSettings_DeviceTuneFlags              = 0x008A,
    USBCmdSettings_DeviceFleetFlags             = 0x008B,
    USBCmdSettings_DeviceDatalogGeneralFlags    = 0x008C,
    
    //UCH_USBCommand_GetSettings only
    USBCmdSettings_Properties                   = 0x005A,
    USBCmdSettings_TuneRevision                 = 0x005B,
    USBCmdSettings_DeviceInfoString             = 0x005C,
    USBCmdSettings_ErrorPool                    = 0x005D,
    USBCmdSettings_DeviceVersionString          = 0x005E,
    
    //UCH_USBCommand_SetSettings only
    USBCmdSettings_ResetFactoryDefault          = 0x00A1,
    USBCmdSettings_ReInit                       = 0x00A2,
    USBCmdSettings_EnableEngSign                = 0x00A3,
    USBCmdSettings_DisableEngSign               = 0x00A4,
}USBCmdSettingsFlags;

typedef enum
{
    USBCmdHardware_GetDeviceInfoString          = 0x0050,
    USBCmdHardware_ValidateLookupHeader         = 0x0051,

    USBCmdHardware_GetInfo                      = 0x0070,
    USBCmdHardware_FTLInit                      = 0x0071,
    USBCmdHardware_FTLRead                      = 0x0072,
    USBCmdHardware_FTLWrite                     = 0x0073,
    USBCmdHardware_FTLDeinit                    = 0x0074,
    USBCmdHardware_FilesystemReinit             = 0x0075,
    USBCmdHardware_FormatMedia                  = 0x0076,
    USBCmdHardware_Listing                      = 0x0077,
    USBCmdHardware_GetFreeSpace                 = 0x0078,
    USBCmdHardware_GetCapacity                  = 0x0079,
    USBCmdHardware_GetFileSize                  = 0x007A,
    USBCmdHardware_DeleteFile                   = 0x007B,
    USBCmdHardware_RenameFile                   = 0x007C,
    USBCmdHardware_MoveFile                     = 0x007D,
    USBCmdHardware_TruncateFile                 = 0x007E,
    USBCmdHardware_CopyFile                     = 0x007F,
    USBCmdHardware_VerifyFile                   = 0x0080,
    USBCmdHardware_CreateDir                    = 0x0081,
    USBCmdHardware_RemoveDir                    = 0x0082,
    USBCmdHardware_ChangeDir                    = 0x0083,
    USBCmdHardware_GetCurrentDir                = 0x0084,
    USBCmdHardware_ChangeDrive                  = 0x0085,
    USBCmdHardware_GetCurrentDrive              = 0x0086,
    USBCmdHardware_ListingSetFolder             = 0x0087,
    USBCmdHardware_FTLInitAdvance               = 0x0088,
    

    USBCmdHardware_ShowMessage                  = 0x0090,
    USBCmdHardware_GetAnalogData                = 0x0091,
    USBCmdHardware_SetBaudrate                  = 0x0092,
    USBCmdHardware_SetVolume                    = 0x0093,
    USBCmdHardware_SetBrightness                = 0x0094,
    USBCmdHardware_Unpair                       = 0x0095,
    USBCmdHardware_GetFriendlyName              = 0x0096,
    USBCmdHardware_SetFriendlyName              = 0x0097,
    USBCmdHardware_GetMacAddress                = 0x0098,
    USBCmdHardware_SetMacAddress                = 0x0099,
    USBCmdHardware_Reset                        = 0x009A,
    USBCmdHardware_FirmwareVersionString        = 0x009B,
    USBCmdHardware_LEDSet                       = 0x009C,
    USBCmdHardware_LEDRelease                   = 0x009D,
}USBCmdHardwareFlags;

typedef enum
{
    USBCmdFile_None                             = 0x0000,
    USBCmdFile_Read                             = 0x0010,
    USBCmdFile_Write                            = 0x0011,
    USBCmdFile_Append                           = 0x0012,
}USBCmdFileFlags;

typedef enum
{
    USBCmdDatalog_GetVehicleInfo                = 0x0096,
    USBCmdDatalog_GetVehicleCommInfo            = 0x0094,
    USBCmdDatalog_SetVehicleComm                = 0x0093,
    USBCmdDatalog_StartTestDLX                  = 0x009D,
    USBCmdDatalog_GetTestResultDLX              = 0x009E,
    USBCmdDatalog_Execute                       = 0x009F,
    USBCmdDatalog_ScanDLX                       = 0x0097,
    USBCmdDatalog_ScanSingle                    = 0x0095,
    USBCmdDatalog_GetFeatures                   = 0x0092,
    USBCmdDatalog_GetStatus                     = 0x0098,
    USBCmdDatalog_GetPacket                     = 0x0099,
    USBCmdDatalog_Start                         = 0x009A,
    USBCmdDatalog_Stop                          = 0x009B,
    USBCmdDatalog_SetDataReportInterval         = 0x009C,
    USBCmdDatalog_DTC                           = 0x0091,
    USBCmdDatalog_SendSignalOSC                 = 0x0090,
}USBCmdDatalogFlags;

typedef enum
{
    USBCmdTune_Init                             = 0x0010,
    USBCmdTune_DeInit                           = 0x0011,
    USBCmdTune_InitFlasher                      = 0x0020,

    USBCmdTune_GetStragyFlasher                 = 0x0034,
    USBCmdTune_GetVehicleInfo                   = 0x0035,

    USBCmdTune_SetCustomTuneInfo                = 0x0040,

    USBCmdTune_InitUpload                       = 0x0060,
    USBCmdTune_DoUpload                         = 0x0061,
    USBCmdTune_InitDownload                     = 0x0062,
    USBCmdTune_DoDownload                       = 0x0063,
//    USBCmdTune_InitReturnStock                  = 0x0064,
//    USBCmdTune_DoReturnStock                    = 0x0065,
    USBCmdTune_HealthCheckDownload              = 0x0066,

    USBCmdTune_KeyConfirm                       = 0x0090,
    USBCmdTune_MarriedCountWarnConfirm          = 0x0091,
    USBCmdTune_MarriedCountWarnCancel           = 0x0092,

    //for upload only function
    USBCmdTune_SearchVehicleType                = 0x00A0,
    USBCmdTune_CompileUserVehicleType           = 0x00A1,
    USBCmdTune_GetUploadStock                   = 0x00A2,    
}USBCmdTuneFlags;

//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
void uch_init();
bool uch_is_halted();
void uch_set_temporary_halt(bool yes);
bool uch_is_usb_ready();
void uch_set_usb_ready(bool yes);
void uch_printf(const char* format, ...);

void uch_ui_lock_time_reset();
void uch_ui_lock_set();
void uch_ui_lock_remove();
void uch_ui_lock_set_force_lock();
void uch_ui_lock_clear_force_lock();
void uch_ui_lock_status_check();

void uch_security_seed();
void uch_security_key();
void uch_bootloader();
void uch_settings_get();
void uch_settings_set();
void uch_hardware();
u8 uch_checkfile();
u8 uch_openfile();
u8 uch_readfile();
u8 uch_writefile();
u8 uch_closefile();
u8 uch_recoverystock_check();
u8 uch_recoverystock_register();

#endif     //__USBCMDHANDLER_H
