/**
  **************** (C) COPYRIGHT 2011 SCT Performance, LLC *********************
  * File Name          : obd2vpw.h
  * Device / System    : SCT products using the "CommonCode" platform
  * Author             : Quyen Leba
  *
  * Version            : 1 
  * Date               : 05/02/2011
  * Description        : 
  *                    : 
  *
  *
  * History            : 05/02/2011 M. Davis
  *                    :   Add SCT Copyright header,
  *
  ******************************************************************************
  */

#ifndef __OBD2VPW_H
#define __OBD2VPW_H

#include <arch/gentype.h>
#include <board/genplatform.h>

#include <common/genmanuf_overload.h>
#include "obd2.h"

//#define VPW_RX_MAX_RETRY_COUNT              3*100
#define VPW_RX_DATALOG_MAX_RETRY_COUNT      1//3*4

#define VPW_ECM_ID                          0x10
#define VPW_ECM_BROADCAST_ID                0xFE
#define VPW_ECM_FUNCTIONAL_ID               0x6A

#define VPW_ECM_RESPONSE_ID                 0xF0
#define VPW_ECM_RESPONSE_ID_SECONDARY       0xF1
#define VPW_ECM_FUNCTIONAL_RESPONSE_ID      0x6B

#define Obd2vpw_ServiceNumber               GMLAN_ServiceNumber
#define Obd2vpw_SubServiceNumber            GMLAN_SubServiceNumber

typedef enum
{
    FUNCTIONAL_P2,      //Px is priority, lower-P == higher-priority
    NODE_P2,
    FUNCTIONAL_P3,
    NODE_P3,
}obd2vpw_headerindex;

enum
{
    VPW_LOW_SPEED   = 0,
    VPW_HIGH_SPEED  = 1,
};

enum
{
    VPW_CRC,
    VPW_NOCRC,
};

enum
{
    Obd2vpwHelper_ReadblockByCmd35single36              = 0,
};

enum
{
    Obd2vpwHelper_WriteblockByCmdSingle36               = 0,
};

//IMPORTANT: MB & VB share this struct, changes to this struct must be made in 
//              both and versions must be incremented.
#define OBD2VPW_RXINFO_VERSION      1
typedef struct
{
    u8 version;
    u32 max_retry_count;
    bool response_expected;
    u8  *rxbuffer;
    u16 rxlength;
    u8  cmd;                 //same as u8 service of txinfo;
    u8  response_cmd_offset; //usually is 0x40, to calculate response_cmd
    u8  first_frame_data_offset;
    u8  errorcode;
    u16 timeout; // rxtimeout
    u8  transfermode;
    u8  functionaladdress;
}obd2vpw_rxinfo;

//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
u8 obd2vpw_init(bool boosted, bool highspeed);
void obd2vpw_rxinfo_init(obd2vpw_rxinfo *rxinfo);
u8 obd2vpw_tx(u8 *txbuffer, u16 txlength);
u8 obd2vpw_rx(obd2vpw_rxinfo *rxinfo);
u8 obd2vpw_txrx_simple(u8 *txbuffer, u16 txlength, u8 *rxbuffer, u16 *rxlength);

//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
u8 obd2vpw_ping();
u8 obd2vpw_read_data_bypid_mode1(u8 pid, u8 pidsize, u8 *output);       //$01
u8 obd2vpw_enter_diag_mode();                                           //$10
u8 obd2vpw_return_normal_mode(u8 broadcast);                            //$20
u8 obd2vpw_read_data_bypid(u16 pid, u8 pidsize, u8 *output);            //$22
u8 obd2vpw_read_memory_byaddress(u32 address, u8 *data);                //$23
u8 obd2vpw_security_access(SubServiceNumber subservice,                 //$27
                           u8 *data);
u8 obd2vpw_disablenormalcommunication();                                //$28
u8 obd2vpw_request_diagnostic_data_packet(u8 requesttype,               //$2A
                                          u8 *packetidlist, u8 packetidcount);
u8 obd2vpw_request_upload(u32 address, u16 length);                     //$35
u8 obd2vpw_request_download(u32 address/*, u16 length*/);               //$34
u8 obd2vpw_transfer_data_upload(u8 *data, u32 *address, u16 *length);   //$36
u8 obd2vpw_transfer_data_download(u8 subservice,                        //$36
                                  u8 *data, u32 address, u16 length);
u8 obd2vpw_stop_periodic_message();
u8 obd2vpw_dynamicallydefinemessage(u8 packetid, u32 address,           //$2C
                                    PidType type, u8 size, u8 position);
u8 obd2vpw_read_data_block(u8 blocknumber,                              //$3C
                           u8 *blockdata, u8 *blockdatalength);

u8 obd2vpw_gm_device_control(u8 cpid, u8 *controlbytes, u16 datalength);//$AE

u8 obd2vpw_requesthighspeed();

u8 obd2vpw_readSoftwarePartNumber(partnumber_info *partnumbers);

//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
void obd2vpw_datalogmode();
void obd2vpw_normalmode();
void obd2vpw_testerpresent(void);

u8 obd2vpw_cleardtc();
u8 obd2vpw_readdtc(dtc_info *info);
u8 obd2vpw_readecminfo(ecm_info *ecm);
u8 obd2vpw_readvin(u8 *vin);
u8 obd2vpw_readserialnumber(u8 *serialnumber);
u8 obd2vpw_readhardwareid(u8 *hwid);

u8 obd2vpw_unlockecm(u32 algoindex, bool seed_peek);

u8 obd2vpw_gm_setup_prior_upload(u16 ecm_type);
u8 obd2vpw_gm_setup_after_upload(u16 ecm_type);
u8 obd2vpw_gm_setup_prior_download(u16 ecm_type);
u8 obd2vpw_gm_setup_after_download(u16 ecm_type);
u8 obd2vpw_downloadutility(u32 ecm_type, u8 utilitychoiceindex,
                           bool isdownloadutility);
u8 obd2vpw_exitutility(u16 ecm_type);
u8 obd2vpw_erase_ecm(u16 ecm_type, bool is_cal_only);

u8 obd2vpw_validatepid(DlxBlock *dlxblock, VehicleCommLevel commlevel);
u8 obd2vpw_validatedmr(DlxBlock *dlxblock, VehicleCommLevel commlevel);
u8 obd2vpw_validateByRapidPacketSetup(DlxBlock *dlxblock, VehicleCommLevel commlevel);
void obd2vpw_datalog_sendtesterpresent();
u8 obd2vpw_datalog_evaluatesignalsetup(VehicleCommLevel *vehiclecommlevel);
u8 obd2vpw_datalog_initiatepackets(VehicleCommLevel *vehiclecommlevel);
u8 obd2vpw_datalog_getdata(VehicleCommLevel *vehiclecommlevel);

u8 obd2vpw_helper_upload_block(u32 address, u16 length,
                               u8 *buffer, u16 *bufferlength);
u8 obd2vpw_helper_download_block(u8 subservice, u32 address, u16 length,
                                 u8 *buffer);
u8 obd2vpw_download_test_key_on(u16 ecm_type);
u8 obd2vpw_getinfo(obd2_info *obd2info);

#endif    //__OBD2VPW_H
