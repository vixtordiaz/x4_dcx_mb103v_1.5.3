/**
  **************** (C) COPYRIGHT 2012 SCT Performance, LLC *********************
  * File Name          : ess_file.c
  * Device / System    : SCT products using the "CommonCode" platform
  * Author             : Ricardo Cigarroa
  *
  * Version            : 1 
  * Date               : 09/09/2014
  * Description        : File format handling functions for E Squared Stock File (.EES)
  *                    : 
  *
  *
  * History            : 09/09/2014 R. Cigarroa
  *                    :   Created
  *
  ******************************************************************************
  */
  
//------------------------------------------------------------------------------
// Log error point: 0x5000 -> 0x504F
//------------------------------------------------------------------------------  
  
#include <string.h>
#include <board/genplatform.h>
#include <common/genmanuf_overload.h>
#include <common/obd2tune.h>
#include <common/crypto_blowfish.h>
#include <common/statuscode.h>
#include <common/filestock.h>
#include <common/file.h>
#include <common/crc32.h>
#include <board/delays.h>
#include "ess_file.h"

#define ESSVERSION                  0 
#define STOCK_E2_FILENAME "\\USER\\E2_stock.ess"

//-----------------------------------------------------------------------------------------
//creates an ESS file with data read from the processor right after performing an upload. 
//Input: F_FILE *file_E2
//       ESS_HEADER *essheader
//       ESS_BLOCK *essblock
//Return: status; 
//-----------------------------------------------------------------------------------------
u8 e2_stock_createfile(F_FILE *file_E2, ESS_HEADER *essheader, ESS_BLOCK *essblock)
{
    u8 status = S_SUCCESS; 
    u32 bytecount;
    u32 header_size = 0;
    u32 block_size = 0;
    
    //File has been created and opened, now we set all all data in this file to its default values.
    

    header_size = sizeof(ESS_HEADER);
    memset(essheader,0,header_size);
    essheader->version = ESSVERSION; 
    essheader->header_crc32e = 0xFFFFFFFF; 
    essheader->content_crc32e = 0xFFFFFFFF; 
    essheader->e2blockcount = 0; 

    block_size = sizeof(ESS_BLOCK);
    memset(essblock,0,block_size);
    essblock->type = CHRYSLER; 
    
    if(fseek(file_E2,0,SEEK_SET) != 0)
    {
        log_push_error_point(0x5000);
        status = S_SEEKFILE;  
        goto e2_stock_createfile_done;
    }
    
    bytecount = 0; 
    bytecount = fwrite((u8*)essheader,1,header_size,file_E2);
    if(bytecount != header_size)
    {
        log_push_error_point(0x5001);
        status = S_FAIL; 
        goto e2_stock_createfile_done;
    }
    
    bytecount = 0; 
    bytecount = fwrite((u8*)essblock,1,block_size,file_E2);
    if(bytecount != block_size)
    {
        log_push_error_point(0x5002); 
        status = S_FAIL; 
        goto e2_stock_createfile_done;
    }
  
e2_stock_createfile_done:

    return status; 
}

//--------------------------------------------------------------------------------------------
// This function encrypts the e2_stock.ess file.
//Input: F_FILE *file_E2
//Return: status
//--------------------------------------------------------------------------------------------
u8 e2_stock_encryptfile(F_FILE *file_E2, ESS_HEADER *essheader)
{
    u8  status; 
    u8  *buffer = NULL; 
    u8  new_crc32[4];
    u32 bytecount; 
    u32 crc32e_calc;
    u32 length; 
    u32 totalcount; 
    u32 filesize;
    u32 decryptblockcount;
    u32 header_size = 0;
    
   
    buffer = __malloc(0x400);
    if(buffer == NULL)
    {
        log_push_error_point(0x5003);
        status = S_MALLOC;
        return status;
    }

   if (fseek(file_E2,0,SEEK_END) != 0)
   {
       log_push_error_point(0x5004);
       status = S_SEEKFILE;
       goto e2_stock_encryptfile_done;
   }

   bytecount = ftell(file_E2);       // need to find the beginning of the E2
   header_size = bytecount - 0x1000;// found issues with header size


    //skip header
    if(fseek(file_E2,header_size,SEEK_SET) != 0)
    {
        log_push_error_point(0x5004);
        status = S_SEEKFILE;
        goto e2_stock_encryptfile_done;  
    }
    
    //calculate content crc32e
    crc32e_reset();
    crc32e_calc = 0xFFFFFFFF; 
    while(!feof(file_E2))
    {
        bytecount = fread((char*)buffer,1,0x400,file_E2);
        if(bytecount == 0)
        {
            log_push_error_point(0x5005);
            status = S_FAIL; 
            goto e2_stock_encryptfile_done; 
        }
        
        bytecount = (bytecount/8) * 8; 
        crypto_blowfish_encryptblock_critical(buffer,bytecount); 
        
        crc32e_calc = crc32e_calculateblock(crc32e_calc,(u32*)buffer,bytecount/4);
    }
    
    new_crc32[3] = ((crc32e_calc >> 24) & 0xFF);
    new_crc32[2] = ((crc32e_calc >> 16) & 0xFF);
    new_crc32[1] = ((crc32e_calc >> 8) & 0xFF);
    new_crc32[0] = (crc32e_calc & 0xFF);
    
    memcpy(&essheader->content_crc32e,&crc32e_calc,4);
    
    if(fseek(file_E2,0x08,SEEK_SET) != 0)
    {
        log_push_error_point(0x5006);
        status = S_SEEKFILE;
        goto e2_stock_encryptfile_done;  
    }
    
    bytecount = 0;
    bytecount =  fwrite(new_crc32,1,4,file_E2);
    if(bytecount != 4)
    {
        log_push_error_point(0x5007);
        status = S_FAIL;
        goto e2_stock_encryptfile_done;
    }
    
    //calculate header crc32
    crc32e_reset();
    crc32e_calc = 0xFFFFFFFF;
    
    crc32e_calc = crc32e_calculateblock(crc32e_calc,(u32*)essheader,
                                        (header_size/4));
                                        
    new_crc32[3] = ((crc32e_calc >> 24) & 0xFF);
    new_crc32[2] = ((crc32e_calc >> 16) & 0xFF);
    new_crc32[1] = ((crc32e_calc >> 8) & 0xFF);
    new_crc32[0] = (crc32e_calc & 0xFF);
                                        
    if(fseek(file_E2,0x04,SEEK_SET) != 0)
    {
        log_push_error_point(0x5008);
        status = S_FAIL;
        goto e2_stock_encryptfile_done;
    }
    
    bytecount = 0;
    bytecount = fwrite(new_crc32,1,4,file_E2);
    if(bytecount != 4)
    {
        log_push_error_point(0x5009);
        status = S_FAIL;
        goto e2_stock_encryptfile_done;
    }

    //encrypt file
    bytecount = 0; 
    totalcount = 0; 
    filesize = 0;
    decryptblockcount = 0; 
    length = 0x400; 
    memset(buffer,0,length);
       
    while(!feof(file_E2))
    { 
        if(fseek(file_E2,(totalcount),SEEK_SET) != 0)
        {
            log_push_error_point(0x500A);
            status = S_FAIL;
            goto e2_stock_encryptfile_done;
        }
        
        bytecount = fread((char*)buffer,1,length,file_E2);
        if(bytecount == 0)
        {
            log_push_error_point(0x500B);
            status = S_FAIL; 
            goto e2_stock_encryptfile_done;
        }
        totalcount += bytecount; 
        
        bytecount = (bytecount/8) * 8; 
        crypto_blowfish_encryptblock_critical(buffer,bytecount); 
        
        if(fseek(file_E2,(totalcount-bytecount),SEEK_SET) != 0)
        {
            log_push_error_point(0x500C);
            status = S_FAIL;
            goto e2_stock_encryptfile_done;
        }
        decryptblockcount = fwrite(buffer,1,bytecount,file_E2);
        if(decryptblockcount != bytecount)
        {
            log_push_error_point(0x500D);
            status = S_FAIL;
            goto e2_stock_encryptfile_done;
        }
    }
    
    status = file_getfilesize(file_E2, &filesize);
    if(status != S_SUCCESS)
    {
        log_push_error_point(0x500E);
        status = S_FAIL; 
        goto e2_stock_encryptfile_done;
    }
    if(totalcount  != filesize)
    {
        log_push_error_point(0x500F);
        status = S_FAIL; 
    }
    
e2_stock_encryptfile_done:
   
    if(buffer)
    {
        __free(buffer); 
    }
    return status; 
    
}

//------------------------------------------------------------------------------
// Validate ess file 
// Input:   const u8 *filename
// Return:  u8  status
// Engineer: Ricardo Cigarroa
//------------------------------------------------------------------------------
u8 e2_stock_validatefile(const u8 *filename, u8 *response)
{
#define VALIDATEFILE_BUFFER_SIZE   0x800
    ESS_HEADER *ess_header =  NULL;
    F_FILE *fptr;
    u8  *buffer;
    u32 bytecount;
    u32 crc32e_calc = 0;
    u32 hold_crc32e_calc = 0;
    u8  status;
    u8 fail_once = 0;
    
    fptr = NULL;
    status = S_SUCCESS;
    
    buffer = __malloc(VALIDATEFILE_BUFFER_SIZE);
    if (buffer == NULL)
    {
        log_push_error_point(0x5010);
        status = S_MALLOC;
        goto e2_stock_validate_ess_file_done;
    }
    ess_header = __malloc(32);
    if(ess_header == NULL)
    {
        log_push_error_point(0x5011);
        status = S_MALLOC;
        goto e2_stock_validate_ess_file_done; 
    }
    fptr = genfs_user_openfile((u8*)filename,"r");
    if (fptr == NULL)
    {
        log_push_error_point(0x5012);
        *response = CMDIF_ACK_E2_STOCK_NOT_FOUND;
        status = S_OPENFILE;
        goto e2_stock_validate_ess_file_done;
    }
    
    status = e2_stock_readheader(fptr,ess_header); 
    if(status != S_SUCCESS)
    {
        log_push_error_point(0x5013);
        status = S_READFILE;
        goto e2_stock_validate_ess_file_done;
    }
    status = e2_stock_validateheader(ess_header);
    if (status != S_SUCCESS)
    {
        log_push_error_point(0x5014);
        //goto e2_stock_validate_ess_file_done;
    }
    
   if (fseek(fptr,0,SEEK_END) != 0)
   {
       log_push_error_point(0x5014);
       status = S_SEEKFILE;
       goto e2_stock_validate_ess_file_done;
   }

   bytecount = ftell(fptr);       // need to find the beginning of the E2
   bytecount = bytecount - 0x1000;// found issues with header size

   if(fseek(fptr,(bytecount),SEEK_SET) != 0)
   {
       log_push_error_point(0x5014);
       status = S_SEEKFILE;
       goto e2_stock_validate_ess_file_done;
   }

TRY_AGAIN:
    crc32e_reset();
    crc32e_calc = 0xFFFFFFFF;
    while(!feof(fptr))
    {
        bytecount = fread((char*)buffer,1,VALIDATEFILE_BUFFER_SIZE,fptr);
        if(bytecount == 0)
        {
            log_push_error_point(0x5015);
            status = S_FAIL; 
            goto e2_stock_validate_ess_file_done; 
        }
                
        crc32e_calc = crc32e_calculateblock(crc32e_calc,(u32*)buffer,bytecount/4);    
    }
    
    if (ess_header->content_crc32e != crc32e_calc)
    {
        log_push_error_point(0x5016);
        if(fail_once == 0)
        {
            hold_crc32e_calc = crc32e_calc;
 
            if(fseek(fptr,0,SEEK_SET) != 0)
            {
                log_push_error_point(0x5014);
                status = S_SEEKFILE;
                goto e2_stock_validate_ess_file_done;  
            }

            status = e2_stock_readheader(fptr,ess_header); 

            fail_once = 1;
            delays(30,'m');
            goto TRY_AGAIN;
        }
        else
        {
            if(hold_crc32e_calc == crc32e_calc)
                status = S_SUCCESS;
            else
                status = S_CRC32E;
            
        }
    }
        
e2_stock_validate_ess_file_done:
    
    if(buffer)
    {
        __free(buffer);
    }
    if(ess_header)
    {
        __free(ess_header);
    }
    if(fptr)
    {
        genfs_closefile(fptr);
    }
    if(status == S_CRC32E)
    {
        *response = CMDIF_ACK_E2_STOCK_CORRUPTED;
    }
    return status; 
}

//------------------------------------------------------------------------------
// Read header
// Input:   F_FILE *fptr
//          ESS_HEADER *header
// Return:  u8  status
// Engineer: Ricardo Cigarroa
//------------------------------------------------------------------------------
u8 e2_stock_readheader(F_FILE *fptr, ESS_HEADER *header)
{
    u32 bytecount;

    bytecount = fread(header,1,sizeof(ESS_HEADER),fptr);
    if (bytecount != sizeof(ESS_HEADER))
    {
        log_push_error_point(0x5017);
        return S_READFILE;
    }

    crypto_blowfish_decryptblock_critical((u8*)header,sizeof(ESS_HEADER));

    return S_SUCCESS;
}

//------------------------------------------------------------------------------
// Validate file header
// Input:   const GeneralFileLookupHeader *header (unencrypted)
// Return:  u8  status
// Engineer: Ricardo Cigarroa
//------------------------------------------------------------------------------
u8 e2_stock_validateheader(ESS_HEADER *header)
{
    u32 cmp_crc32e;
    u32 calc_crc32e;

    if (header->version != ESS_VERSION)
    {
        log_push_error_point(0x5018);
        return S_BADCONTENT;
    }

    cmp_crc32e = header->header_crc32e;
    crc32e_reset();
    header->header_crc32e = 0xFFFFFFFF;

    calc_crc32e = crc32e_calculateblock(0xFFFFFFFF,(u32*)header,
                                        sizeof(ESS_HEADER)/4);
    header->header_crc32e = cmp_crc32e;
    if (cmp_crc32e != calc_crc32e)
    {
        log_push_error_point(0x5019);
        //return S_CRC32E;
    }

    return S_SUCCESS;
}