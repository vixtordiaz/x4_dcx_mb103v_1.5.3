/**
  **************** (C) COPYRIGHT 2011 SCT Performance, LLC *********************
  * File Name          : obd2tune_otf.c
  * Device / System    : SCT products using the "CommonCode" platform
  * Author             : Quyen Leba
  *
  * Version            : 1 
  * Date               : 05/02/2011
  * Description        : 
  *                    : 
  *
  *
  * History            : 05/02/2011 M. Davis
  *                    :   Add SCT Copyright header,
  *
  ******************************************************************************
  */

//------------------------------------------------------------------------------
// Log error point: 0x4700 -> 0x47FF
//------------------------------------------------------------------------------

#include <string.h>
#include <fs/genfs.h>
#include <common/statuscode.h>
#include <common/settings.h>
#include <common/obd2tune.h>
#include <common/filestock.h>
#include <common/filestock_option.h>
#include <common/obd2comm.h>
#include <common/crypto_blowfish.h>
#include <common/filecrypto.h>
#include <common/file.h>
#include <common/log.h>
#include <common/veh_defs.h>
#include "obd2tune_otf.h"

// Private prototypes
u8 obd2tune_otf_setPowerLevel_Ford_73L_ECM(OTFPowerLevel power_level);
u8 obd2tune_otf_setPowerLevel_Ford_60L_ECM(OTFPowerLevel power_level);
u8 obd2tune_otf_setPowerLevel_Ford_64L_ECM(u16 vehicle_type, OTFPowerLevel power_level);
u8 obd2tune_otf_setPowerLevel_Ford_67L_ECM(OTFPowerLevel power_level);

u8 obd2tune_otf_setPowerLevel_GM_LB7_LLY_ECM(OTFPowerLevel power_level);
u8 obd2tune_otf_setPowerLevel_GM_LBZ_ECM(OTFPowerLevel power_level);
u8 obd2tune_otf_setPowerLevel_GM_LMM_ECM(OTFPowerLevel power_level);


/**
 *  obd2tune_otf_isFeatureAvailable
 *  
 *  @brief Check if OTF is available (i.e. device connects to its married vehicle w/ OTF tune)
 *
 *  @param[in] vin
 *
 *  @retval bool TRUE: OTF feature available
 *  
 *  @authors Quyen Leba
 */
bool obd2tune_otf_isFeatureAvailable(u8 *vin)
{
    bool isAvailable;

    isAvailable = FALSE;
    if (!SETTINGS_IsMarried())
    {
        isAvailable = FALSE;
    }
    else if (!obd2tune_isvin_matched(vin,SETTINGS_TUNE(vin),0))
    {
        isAvailable = FALSE;
    }
    else if (SETTINGS_TUNE(tuneinfotrack.otfinfo.pcm != 0))
    {
        isAvailable = TRUE;
    }

    return isAvailable;
}

/**
 *  obd2tune_otf_setPowerLevel
 *  
 *  @brief Set OTF Power level
 *
 *  @param[in] vehicle_type
 *  @param[in] processor_index 0: PCM, 1: TCM
 *  @param[in] power_level
 *
 *  @retval u8 status
 *  
 *  @authors Quyen Leba
 */
u8 obd2tune_otf_setPowerLevel(u16 vehicle_type,
                              u8 processor_index, OTFPowerLevel power_level)
{    
    u8  status;
    
    // --------------------------------------
    // Verfiy OTF criteria up front 
    // --------------------------------------
    if (!SETTINGS_IsMarried())
    {
        return S_REJECT;    // Must be married to vehicle to perform OTF functions
    }
    else if (processor_index > 0)
    {
        return S_NOTSUPPORT; // Only support for ECM so far, no TCM or others
    }
    else if (SETTINGS_TUNE(tuneinfotrack.otfinfo.pcm) == 0)
    {
        return S_REJECT; //tune doesn't support OTF
    }        

    // --------------------------------------
    // Handle vehicle specific OTF functions
    // --------------------------------------
    status = S_FAIL;
    if (VEH_IsVehicleType_Ford_60L(vehicle_type))
    {
        status = obd2tune_otf_setPowerLevel_Ford_60L_ECM(power_level);
    }
    else if (VEH_IsVehicleType_Ford_64L(vehicle_type))
    {
        status = obd2tune_otf_setPowerLevel_Ford_64L_ECM(vehicle_type, power_level);
    }
    else if (VEH_IsVehicleType_Ford_67L(vehicle_type))
    {
        status = obd2tune_otf_setPowerLevel_Ford_67L_ECM(power_level);
    }
#if (SUPPORT_COMM_SCP)
    else if (VEH_IsVehicleType_Ford_216K(vehicle_type))
    {
        status = obd2tune_otf_setPowerLevel_Ford_73L_ECM(power_level);        
    }
#endif  //#if (SUPPORT_COMM_SCP)
#if (SUPPORT_COMM_VPW)
    else if (VEH_IsVehicleType_GM_LB7(vehicle_type) ||
             VEH_IsVehicleType_GM_LLY(vehicle_type))
    {
        status = obd2tune_otf_setPowerLevel_GM_LB7_LLY_ECM(power_level);
    }
#endif  //#if (SUPPORT_COMM_VPW)
    else if (VEH_IsVehicleType_GM_LBZ(vehicle_type))
    {
        status = obd2tune_otf_setPowerLevel_GM_LBZ_ECM(power_level);
    }
    else if (VEH_IsVehicleType_GM_LMM(vehicle_type))
    {
        status = obd2tune_otf_setPowerLevel_GM_LMM_ECM(power_level);
    }
    else
    {
        status = S_NOTSUPPORT;
    }

    return status;
}

/**
 *  obd2tune_otf_setPowerLevel_Ford73L_ECM
 *  
 *  @brief Set OTF Power level for Ford 7.3L
 *
 *  @param[in] power_level
 *
 *  @retval u8 status
 *  
 *  @authors Patrick Downs
 */
u8 obd2tune_otf_setPowerLevel_Ford_73L_ECM(OTFPowerLevel power_level)
{
    u8 buffer[12];
    u8 status;
    
    buffer[0] = 0xC4;
    buffer[1] = 0x10;
    buffer[2] = 0xF1;
    
    switch(power_level)
    {
    case OTFPowerLevel_Stock:
        buffer[3] = 0x00;
        break;
    case OTFPowerLevel_Tow:
        buffer[3] = 0x11;
        break;
    case OTFPowerLevel_Performance:
        buffer[3] = 0x12;
        break;
    case OTFPowerLevel_Extreme:
        buffer[3] = 0x13;
        break;
    default:
        buffer[3] = 0x00;
        break;
    }
    
    status = obd2scp_tx(buffer,4,SCP_MODE_NODE);
    
    return status;
}

/**
 *  obd2tune_otf_setPowerLevel_Ford60L_ECM
 *  
 *  @brief Set OTF Power level for Ford 6.0L, uses pid request 0x11C1 to set power level
 *
 *  @param[in] power_level
 *
 *  @retval u8 status
 *  
 *  @authors Patrick Downs
 */
u8 obd2tune_otf_setPowerLevel_Ford_60L_ECM(OTFPowerLevel power_level)
{
    u8 buffer[12];
    u8 status;
    
    buffer[0] = 0x02;
    buffer[1] = 0x22;
    buffer[2] = 0x11;
    buffer[3] = 0xC1;
    buffer[4] = 0x00;
    buffer[5] = 0xFF;
    buffer[6] = 0x00;
    
    switch(power_level)
    {
    case OTFPowerLevel_Stock:
        buffer[7] = 0x65;
        break;
    case OTFPowerLevel_Tow:
        buffer[7] = 0x32;
        break;
    case OTFPowerLevel_Performance:
        buffer[7] = 0x4B;
        break;
    case OTFPowerLevel_Extreme:
        buffer[7] = 0x64;
        break;
    default:
        buffer[7] = 0x65;
        break;
    }
    status = obd2can_txraw(0x7E0, buffer, 8, FALSE);
    
    return status;
}

/**
 *  obd2tune_otf_setPowerLevel_Ford64L_ECM
 *  
 *  @brief Set OTF Power level for Ford 6.4L, uses VID block read/write to set power level
 *
 *  @param[in] vehicle_type
 *  @param[in] power_level
 *
 *  @retval u8 status
 *  
 *  @authors Patrick Downs
 */
u8 obd2tune_otf_setPowerLevel_Ford_64L_ECM(u16 vehicle_type, OTFPowerLevel power_level)
{
    Obd2can_ServiceData servicedata;
    u32 currentFrameSeperationTime;
    u16 checksum;
    u8 buffer[128];
    u8 currentPowerLevel;
    u8 status;
    
    // request VID block 0x80 bytes      
    obd2can_servicedata_init(&servicedata);
    servicedata.ecm_id = 0x7e0;
    servicedata.service = 0x21;     // Read Data By Local ID (LID) 
    servicedata.subservice = 0x00;  // LID = VID Block 0x00
    servicedata.rx_timeout_ms = 250;
    servicedata.initial_wait_extended = 0;
    
    status = obd2can_txrx(&servicedata);
    if(status == S_SUCCESS)
    {
        memcpy(buffer, servicedata.rxdata+1, sizeof(buffer)); // Copy VID Block data from rxdata to buffer. ID is at rxdata[0], VID block data starts at rxdata[1]
        
        currentPowerLevel = buffer[127]; // Save current power level value
        
        switch(power_level)
        {
        case OTFPowerLevel_Stock:
            buffer[127] = 0x00;
            break;
        case OTFPowerLevel_Tow:
            buffer[127] = 0x01;
            break;
        case OTFPowerLevel_Performance:
            buffer[127] = 0x02;
            break;
        case OTFPowerLevel_Extreme:
            buffer[127] = 0x03;
            break;
        default:
            buffer[127] = 0xFF;
            break;
        }
        
        // Re-calculate VID block checksum
        checksum = (buffer[124] << 8) & 0xFF00;
        checksum |= buffer[125];
        checksum += currentPowerLevel;          // Add previous power level value
        checksum -= buffer[127];                // Subtract new value
        buffer[124] = (checksum >> 8) & 0xFF;   // Update value in VID block
        buffer[125] = (checksum & 0xFF);
        
        // Enter Security mode
        status = obd2can_unlockecm(UnlockTask_Download, VEH_GetMainEcmType(vehicle_type), FALSE);
        if(status == S_SUCCESS)
        {
            obd2can_servicedata_init(&servicedata);
            servicedata.ecm_id = 0x7e0; 
            servicedata.service = 0x3B;     // Write Data By Local ID (LID)
            servicedata.subservice = 0x00;  // LID = Vid Block 0x00
            servicedata.rx_timeout_ms = 250;
            servicedata.initial_wait_extended = 0;
            servicedata.txdata = buffer;
            servicedata.txdatalength = 0x80;
            
            // When writing VID block, ECM reponds with 1ms seperation time in 
            // low control frame so we need to adjust accordingly
            currentFrameSeperationTime = obd2_getcooldowntime();    // Get current frame seperation time (cooldowntime)
            obd2_setcooldowntime(1000);                             // Set new 1ms frame seperation
            
            status = obd2can_txrx(&servicedata);
            
            obd2_setcooldowntime(currentFrameSeperationTime);            // Restore previous cooldown time
        }
    }
    
    return status;
}

/**
 *  obd2tune_otf_setPowerLevel_Ford67L_ECM
 *  
 *  @brief Set OTF Power level for Ford 6.7L, uses a proprietary command 0xBD to set power level
 *
 *  @param[in] power_level
 *
 *  @retval u8 status
 *  
 *  @authors Patrick Downs
 */
u8 obd2tune_otf_setPowerLevel_Ford_67L_ECM(OTFPowerLevel power_level)
{
    u8 status;
    u8 buffer[8];
    
    buffer[0] = 0x07;
    buffer[1] = 0xBD;
    buffer[2] = 0x00;
    buffer[3] = 0x00;
    buffer[4] = 0x00;
    buffer[5] = 0xFF;
    buffer[6] = 0x00;
    switch(power_level)
    {
    case OTFPowerLevel_Stock:
        buffer[4] = 0x00;
        buffer[5] = 0x00;
        buffer[6] = 0x00;
        buffer[7] = 0x00;
        break;
    case OTFPowerLevel_Tow:
        buffer[4] = 0x01;
        buffer[5] = 0x01;
        buffer[6] = 0x01;
        buffer[7] = 0x01;
        break;
    case OTFPowerLevel_Performance:
        buffer[4] = 0x02;
        buffer[5] = 0x02;
        buffer[6] = 0x02;
        buffer[7] = 0x02;
        break;
    case OTFPowerLevel_Extreme:
        buffer[4] = 0x03;
        buffer[5] = 0x03;
        buffer[6] = 0x03;
        buffer[7] = 0x03;
        break;
    default:
        buffer[4] = 0x00;
        buffer[5] = 0x00;
        buffer[6] = 0x00;
        buffer[7] = 0x00;
        break;
    }
    status = obd2can_txraw(0x7E0, buffer, 8, FALSE);
    return status;
}

/**
 *  obd2tune_otf_setPowerLevel_GM_LB7_LLY_ECM
 *  
 *  @brief Set OTF Power level for GM LB7 and LLY
 *
 *  @param[in] power_level
 *
 *  @retval u8 status
 *  
 *  @authors Patrick Downs
 */
u8 obd2tune_otf_setPowerLevel_GM_LB7_LLY_ECM(OTFPowerLevel power_level)
{
    u8 buffer[8];
    u8 status;
    
    buffer[0] = 0x6C;
    buffer[1] = 0x10;
    buffer[2] = 0xF1;
    buffer[3] = 0x3C;
    buffer[4] = 0x01;
    switch(power_level)
    {
    case OTFPowerLevel_Stock:
        buffer[5] = 0x2F;
        break;
    case OTFPowerLevel_Tow:
        buffer[5] = 0x30;
        break;
    case OTFPowerLevel_Performance:
        buffer[5] = 0x31;
        break;
    case OTFPowerLevel_Extreme:
        buffer[5] = 0x32;
        break;
    default:
        buffer[5] = 0x2F;
        break;
    }
    status = obd2vpw_tx(buffer,6);
    
    return status;
}

/**
 *  obd2tune_otf_setPowerLevel_GM_LBZ_ECM
 *  
 *  @brief Set OTF Power level for GM LBZ
 *
 *  @param[in] power_level
 *
 *  @retval u8 status
 *  
 *  @authors Patrick Downs
 */
u8 obd2tune_otf_setPowerLevel_GM_LBZ_ECM(OTFPowerLevel power_level)
{
    Obd2can_ServiceData servicedata;
    u8 buffer[8];
    u32 response_ecm_id;
    u8 status;
    
    //send:     0x7E0 (0x02) 0x1A 0xAA 0x00 0x00 0x00 0x00 0x00
    //expect:   0x7E8 (0x10 0x09) 0x1A 0xAA X1 X2 X3 X4 X5 X6 X7
    //0x7E0 0x02 0x1A 0xAA 0x00 0x00 0x00 0x00 0x00
    //0x7E8 0x10 0x09 0x1A 0xAA X1   X2   X3   X4
    //0x7E0 0x30 0x00 0x00 0x00 0x00 0x00 0x00 0x00
    //0x7E8 0x21 X5   X6   X7
    obd2can_servicedata_init(&servicedata);
    servicedata.ecm_id = 0x7E0;
    servicedata.service = 0x1A;
    servicedata.service_controldatabuffer[0] = 0xAA;
    servicedata.service_controldatalength = 1;
    status = obd2can_txrx(&servicedata);
    if (status == S_SUCCESS)
    {
        if (servicedata.rxdatalength >= 9)
        {
            //send: 0x7E0 (0x10 0x09) 0x1A 0xAA X1 X2 X3 X4 X5 X6 X7 [0x00 0x00 0x00 PowerLevel]
            //0x7E0 0x10 0x09 0x3B 0xAA X1   X2   X3   X4
            //0x7E8 0x30
            //0x7E0 0x21 X5   X6   X7   0x00 0x00 0x00 PowerLevel
            //note that frame has only 9 bytes of data, so we have to send the frame manually as below
            buffer[0] = 0x10;
            buffer[1] = 0x09;
            buffer[2] = 0x3B;
            buffer[3] = 0xAA;
            buffer[4] = servicedata.rxdata[1];
            buffer[5] = servicedata.rxdata[2];
            buffer[6] = servicedata.rxdata[3];
            buffer[7] = servicedata.rxdata[4];
            status = obd2can_txraw(0x7E0, buffer, 8, FALSE);
            if (status == S_SUCCESS)
            {
                do
                {
                    status = obd2can_rxraw(&response_ecm_id,buffer);
                }while(status == S_SUCCESS && (response_ecm_id != 0x7E8 || buffer[1] != 0x30));
                
                if (status == S_SUCCESS)
                {
                    buffer[0] = 0x21;
                    buffer[1] = servicedata.rxdata[5];
                    buffer[2] = servicedata.rxdata[6];
                    buffer[3] = servicedata.rxdata[7];
                    buffer[4] = 0x00;
                    buffer[5] = 0x00;
                    buffer[6] = 0x00;
                    
                    switch(power_level)
                    {
                    case OTFPowerLevel_Stock:
                        buffer[3] = 0x65;
                        break;
                    case OTFPowerLevel_Tow:
                        buffer[3] = 0x32;
                        break;
                    case OTFPowerLevel_Performance:
                        buffer[3] = 0x4B;
                        break;
                    case OTFPowerLevel_Extreme:
                        buffer[3] = 0x64;
                        break;
                    default:
                        buffer[3] = 0x65;
                        break;
                    }//switch(power_level)...
                    status = obd2can_txraw(0x7E0, buffer, 8, FALSE);
                }
            }
        }//if (servicedata.rxdatalength >= 9)...
    }
    
    return status;
}

/**
 *  obd2tune_otf_setPowerLevel_GM_LMM_ECM
 *  
 *  @brief Set OTF Power level for GM LMM
 *
 *  @param[in] power_level
 *
 *  @retval u8 status
 *  
 *  @authors Patrick Downs
 */
u8 obd2tune_otf_setPowerLevel_GM_LMM_ECM(OTFPowerLevel power_level)
{
    u8 buffer[8];
    u8 status;
    
    buffer[0] = 0x06;
    buffer[1] = 0xBD;
    buffer[3] = 0x00;
    buffer[4] = 0x00;
    buffer[5] = 0x00;
    buffer[6] = 0x00;
    buffer[7] = 0x00;
    switch(power_level)
    {
    case OTFPowerLevel_Stock:
        buffer[2] = 0x00;
        break;
    case OTFPowerLevel_Tow:
        buffer[2] = 0x01;
        break;
    case OTFPowerLevel_Performance:
        buffer[2] = 0x02;
        break;
    case OTFPowerLevel_Extreme:
        buffer[2] = 0x03;
        break;
    default:
        buffer[2] = 0x00;
        break;
    }
    status = obd2can_txraw(0x7E0, buffer, 8, FALSE);
        
    return status;
}



