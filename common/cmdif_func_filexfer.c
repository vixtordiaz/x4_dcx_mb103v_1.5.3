/**
  **************** (C) COPYRIGHT 2011 SCT Performance, LLC *********************
  * File Name          : cmdif_func_filexfer.c
  * Device / System    : SCT products using the "CommonCode" platform
  * Author             : Quyen Leba
  *
  * Version            : 1 
  * Date               : 05/02/2011
  * Description        : 
  *                    : 
  *
  *
  * History            : 05/02/2011 M. Davis
  *                    :   Add SCT Copyright header,
  *
  ******************************************************************************
  */

//------------------------------------------------------------------------------
// Log error point: 0x4D00 -> 0x4DFF
//------------------------------------------------------------------------------

#include <board/debug.h>
#include <fs/genfs.h>
#include <common/statuscode.h>
#include <common/obd2tune.h>
#include <common/checksum.h>
#include <common/cmdif.h>
#include <common/file.h>
#include <common/crc32.h>
#include "cmdif_func_filexfer.h"

#define MAX_BLOCK_LENGTH                2048
extern cmdif_datainfo cmdif_datainfo_responsedata;

//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
filexfer_info filexferinfo;

//------------------------------------------------------------------------------
// Input:   u8  *opendata
//          [n:string][NULL][1: attrib ('w','r','a')][4:filesize]
// Return:  u8  status
// Engineer: Patrick Downs, Quyen Leba
// Note: obsolete
//------------------------------------------------------------------------------
u8 cmdif_func_filexfer_open_file(u8 *opendata)
{
    cmdif_datainfo_responsedata.responsecode = CMDIF_ACK_UNSUPPORTED;
    return S_NOTSUPPORT;
}

//------------------------------------------------------------------------------
// Check if file exist & has same content (used before a file transfer)
// Input:   u8  *filedata
//          [4:filesize][4:file crc32e][n:filename][NULL]
// Return:  u8  status (status is vague)
// Engineer: Patrick Downs, Quyen Leba
//------------------------------------------------------------------------------
u8 cmdif_func_filexfer_check_file(u8 *filedata)
{
    u8  data_filename[MAX_FILENAME_LENGTH+1];
    u32 data_filesize;
    u32 data_crc32e;
    F_FILE *fptr;
    u32 filesize;
    u32 file_crc32e;
    u8  *buffer;
    u32 bytecount;
    s32 count;
    u8  status;

    status = S_SUCCESS;
    cmdif_datainfo_responsedata.responsecode = CMDIF_ACK_FAILED;

    data_filesize  = (u32)filedata[0];
    data_filesize |= (u32)filedata[1] << 8;
    data_filesize |= (u32)filedata[2] << 16;
    data_filesize |= (u32)filedata[3] << 24;
    
    data_crc32e  = (u32)filedata[4];
    data_crc32e |= (u32)filedata[5] << 8;
    data_crc32e |= (u32)filedata[6] << 16;
    data_crc32e |= (u32)filedata[7] << 24;    

    if (file_isfilenamevalid(&filedata[8],MAX_FILENAME_LENGTH) == FALSE)
    {
        return S_INPUT;
    }

    status = file_general_getfilename(&filedata[8],data_filename);
    if (status != S_SUCCESS)
    {
        return status;
    }

    fptr = genfs_general_openfile(data_filename, "r");
    if (fptr == NULL)
    {
        cmdif_datainfo_responsedata.responsecode = CMDIF_ACK_FILE_NOT_FOUND;
        goto cmdif_func_filexfer_check_file_done;
    }
    
    fseek(fptr, 0, F_SEEK_END);
    filesize = ftell(fptr);
    fseek(fptr, 0, F_SEEK_SET);
    if(filesize != data_filesize)
    {
        cmdif_datainfo_responsedata.responsecode = CMDIF_ACK_FILE_NOT_MATCH;
        goto cmdif_func_filexfer_check_file_done;
    }
    
    buffer = __malloc(MAX_BLOCK_LENGTH);
    if(buffer == NULL)
    {
        status = S_MALLOC;
        goto cmdif_func_filexfer_check_file_done;
    }

    crc32e_reset();
    file_crc32e = 0xFFFFFFFF;
    while(feof(fptr)==0)
    {
        bytecount = fread(buffer, 1, MAX_BLOCK_LENGTH, fptr);
        count = bytecount - (bytecount/4)*4;
        
        file_crc32e = crc32e_calculateblock(file_crc32e,
                                            (u32*)buffer,bytecount/4);
        if (count > 0)
        {
            u8  leftover[4];
            
            memset(leftover,0xFF,4);
            memcpy(leftover,(char*)&buffer[bytecount - count],count);
            file_crc32e = crc32e_calculateblock(file_crc32e,
                                                (u32*)&leftover,1);
        }
    }
    __free(buffer);
    genfs_closefile(fptr); //TODOQ: simplify this

    if(file_crc32e != data_crc32e)
    {
        cmdif_datainfo_responsedata.responsecode = CMDIF_ACK_FILE_NOT_MATCH;
        goto cmdif_func_filexfer_check_file_done;
    }

    // file matched
    cmdif_datainfo_responsedata.responsecode = CMDIF_ACK_OK;

    return S_SUCCESS;

cmdif_func_filexfer_check_file_done:
    if (fptr)
    {
        genfs_closefile(fptr);
    }
    return status;
}

//------------------------------------------------------------------------------
// Check if file exist & has same content (used before a file transfer)
// Input:   u8  *requestdata
//          [4:offset][4:block length]
// Return:  u8  status
// Engineer: Patrick Downs, Quyen Leba
// Note: obsolete
//------------------------------------------------------------------------------
u8 cmdif_func_filexfer_read_block(u8 *requestdata)
{
    cmdif_datainfo_responsedata.responsecode = CMDIF_ACK_UNSUPPORTED;
    return S_NOTSUPPORT;
}

//------------------------------------------------------------------------------
// Return:  u8  status
// Engineer: Patrick Downs, Quyen Leba
//TODOQ: name not sound, change it
// Note: obsolete
//------------------------------------------------------------------------------
u8 cmdif_func_filexfer_read_to_monitor()
{
    cmdif_datainfo_responsedata.responsecode = CMDIF_ACK_UNSUPPORTED;
    return S_NOTSUPPORT;
}

//------------------------------------------------------------------------------
// Input:   u8  *requestdata
//          [4:offset][4:blocklength]
// Return:  u8  status
// Engineer: Patrick Downs, Quyen Leba
// Note: obsolete
//------------------------------------------------------------------------------
u8 cmdif_func_filexfer_write_block(u8 *requestdata)
{
    cmdif_datainfo_responsedata.responsecode = CMDIF_ACK_UNSUPPORTED;
    return S_NOTSUPPORT;
}

//------------------------------------------------------------------------------
// Input:   u8  *writedatainfo
//          [4:blockchecksum]
// Return:  u8  status
// Engineer: Patrick Downs, Quyen Leba
//TODOQ: name not sound, change it
// Note: obsolete
//------------------------------------------------------------------------------
u8 cmdif_func_filexfer_write_to_dongle(u8 *writedatainfo)
{
    cmdif_datainfo_responsedata.responsecode = CMDIF_ACK_UNSUPPORTED;
    return S_NOTSUPPORT;
}

//------------------------------------------------------------------------------
// Input:   u8  *opendata
//          [1: attrib ('w','r','a')][4:filesize][n:string][NULL]
// Return:  u8  status
// Engineer: Quyen Leba
// Note: new method //TODO
//------------------------------------------------------------------------------
u8 cmdif_func_filexfer_fopen_filetransfer(u8 *opendata)
{
    u8  attribute[2];
    u8  filename[MAX_FILENAME_LENGTH+1];
    u8  *bptr;
    u8  status = S_FAIL;

    cmdif_datainfo_responsedata.responsecode = CMDIF_ACK_FAILED;
    if (file_isfilenamevalid(&opendata[5],MAX_TUNE_NAME_LENGTH) == FALSE)
    {
        log_push_error_point(0x4D20);
        return S_INPUT;
    }

    //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    //TODOQ: app needs to handle this; get rid of it here
    bptr = &opendata[5];
    if (strstr((char*)bptr,".dlx"))
    {
        if (strstr((char*)bptr,GENFS_DEFAULT_FOLDER_PATH))
        {
            status = file_default_getfilename(&opendata[5],filename);
        }
        else
        {
            status = file_user_getfilename(&opendata[5],filename);
        }
    }
    else if (strstr((char*)bptr,"SavedOpts"))
    {
        status = file_user_getfilename(&opendata[5],filename);
    }
    else
    {
        status = file_general_getfilename(&opendata[5],filename);
    }
    //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    
    if (status != S_SUCCESS)
    {
        log_push_error_point(0x4D21);
        return status;
    }

    attribute[0] = opendata[0];
    attribute[1] = NULL;    //just a NULL terminator
    
    if ((attribute[0] != 'w') && (attribute[0] != 'r') && (attribute[0] != 'a'))
    {
        log_push_error_point(0x4D22);
        return S_BADCONTENT;
    }

    if (attribute[0] == 'w')
    {
        filexferinfo.filesize  = (u32)opendata[1];
        filexferinfo.filesize |= (u32)opendata[2] << 8;
        filexferinfo.filesize |= (u32)opendata[3] << 16;
        filexferinfo.filesize |= (u32)opendata[4] << 24;
    }

    if (filexferinfo.filestream)
    {
        genfs_closefile(filexferinfo.filestream);
    }
    filexferinfo.filestream = genfs_general_openfile(filename,attribute);
    if (filexferinfo.filestream == NULL)
    {
        log_push_error_point(0x4D23);
        cmdif_datainfo_responsedata.responsecode = CMDIF_ACK_FILE_NOT_FOUND;
        return S_OPENFILE;
    }
    filexferinfo.transfer_id = 0x01;
    filexferinfo.currentposition = 0;

    bptr = __malloc(8);
    if (bptr == NULL)
    {
        log_push_error_point(0x4D24);
        return S_MALLOC;
    }

    cmdif_datainfo_responsedata.dataptr = bptr;

    // Now use file name buffer to send back ID and Filesize
    bptr[0] = filexferinfo.transfer_id;
    bptr[1] = filexferinfo.transfer_id >> 8;
    bptr[2] = filexferinfo.transfer_id >> 16;
    bptr[3] = filexferinfo.transfer_id >> 24;

    if (attribute[0] == 'r')
    {
        fseek(filexferinfo.filestream, 0, F_SEEK_END);
        filexferinfo.filesize = ftell(filexferinfo.filestream);
        fseek(filexferinfo.filestream, 0, F_SEEK_SET);
        
        bptr[4] = filexferinfo.filesize;
        bptr[5] = filexferinfo.filesize >> 8;
        bptr[6] = filexferinfo.filesize >> 16;
        bptr[7] = filexferinfo.filesize >> 24;

        cmdif_datainfo_responsedata.datalength = 8;
    }
    else
    {
        cmdif_datainfo_responsedata.datalength = 4;
    }

    cmdif_datainfo_responsedata.responsecode = CMDIF_ACK_OK;
    return S_SUCCESS;
}

//------------------------------------------------------------------------------
// Close file transfer session; clean up opened file if any
// Return:  u8  status
// Engineer: Quyen Leba
//------------------------------------------------------------------------------
u8 cmdif_func_filexfer_fclose_filetransfer()
{
    filexferinfo.transfer_id = 0;
    if (filexferinfo.filestream)
    {
        genfs_closefile(filexferinfo.filestream);
        filexferinfo.filestream = NULL;
    }
    cmdif_datainfo_responsedata.responsecode = CMDIF_ACK_OK;
    return S_SUCCESS;
}

//------------------------------------------------------------------------------
// Handle folder
// Inputs:  filexfer_foldertask task (task on folder)
//          u8  *folder_name
//          u16 privdata (depend on task)
//          u8 *optional_data (depend on task)
// Return:  u8  status
// Engineer: Quyen Leba
//------------------------------------------------------------------------------
u8 cmdif_func_filexfer_folder(filexfer_foldertask task, u8 *folder_name,
                              u16 privdata, u8 *optional_data)
{
    u8  *bptr;
    u32 datalength;
    u8  status;

    cmdif_datainfo_responsedata.responsecode = CMDIF_ACK_FAILED;
    switch(task)
    {
    case FolderTask_Create:
        status = genfs_createdir(folder_name);
        break;
    case FolderTask_Remove:
        status = genfs_removedir(folder_name);
        break;
    case FolderTask_CheckExist:
        status = genfs_isfolderexist(folder_name);
        break;
    case FolderTask_GetListing:
        status = S_ERROR;
        bptr = __malloc(1024);
        if (bptr == NULL)
        {
            log_push_error_point(0x4D25);
            return S_MALLOC;
        }
        if (strcmp((char*)folder_name,GENFS_DEFAULT_FOLDER_NAME) == 0)
        {
            status = genfs_get_listing_bulk(privdata,1,optional_data,1024,
                                            bptr,&datalength);
        }
        else if (strcmp((char*)folder_name,GENFS_USER_FOLDER_NAME) == 0)
        {
            status = genfs_get_listing_bulk(privdata,2,optional_data,1024,
                                            bptr,&datalength);
        }
        if (status == S_SUCCESS)
        {
            cmdif_datainfo_responsedata.dataptr = bptr;
            cmdif_datainfo_responsedata.datalength = datalength;
            cmdif_datainfo_responsedata.responsecode = CMDIF_ACK_OK;
            return S_SUCCESS;
        }
        else if (status == S_TERMINATED)
        {
            cmdif_datainfo_responsedata.dataptr = bptr;
            cmdif_datainfo_responsedata.datalength = datalength;
            cmdif_datainfo_responsedata.responsecode = CMDIF_ACK_DONE;
            return S_SUCCESS;
        }
        break;
    default:
        status = S_NOTSUPPORT;
        break;
    }
    if (status == S_SUCCESS)
    {
        cmdif_datainfo_responsedata.responsecode = CMDIF_ACK_OK;
    }
    else if (status == S_NOTSUPPORT)
    {
        cmdif_datainfo_responsedata.responsecode = CMDIF_ACK_UNSUPPORTED;
    }
    return status;
}

//------------------------------------------------------------------------------
// Handle file
// Inputs:  filexfer_filetask task (task on file)
//          u8  *file_name
//          u16 privdata
// Return:  u8  status
// Engineer: Quyen Leba
//------------------------------------------------------------------------------
u8 cmdif_func_filexfer_file(filexfer_filetask task, u8 *file_name,
                            u16 privdata)
{
    F_FILE *fptr;
    u8  *bptr1;
    u8  *bptr2;
    u32 datalength;
    u8  status;
    u32 temp;

    cmdif_datainfo_responsedata.responsecode = CMDIF_ACK_FAILED;
    status = S_FAIL;
    
    switch(task)
    {
    case FileTask_GetFileSize:
        //input data: [n:filename][NULL]
        status = genfs_getfilesize(file_name, &temp);
        break;
    case FileTask_Delete:
        //input data: [n:filename][NULL]
        if (strlen((char*)file_name) > MAX_FILENAME_LENGTH)
        {
            log_push_error_point(0x4D34);
            status = S_INPUT;
        }
        else
        {
            status = genfs_deletefile(file_name);
        }
        break;
    case FileTask_DlxFileList:
        //file_name: not used
        bptr1 = __malloc(1024);
        if (bptr1)
        {
            //2: USER folder
            status = genfs_get_listing_bulk(privdata,2,"dlx",1024,
                                            bptr1,&datalength);
            if (status == S_SUCCESS)
            {
                cmdif_datainfo_responsedata.dataptr = bptr1;
                cmdif_datainfo_responsedata.datalength = datalength;
                cmdif_datainfo_responsedata.responsecode = CMDIF_ACK_OK;
                return S_SUCCESS;
            }
            else if (status == S_TERMINATED)
            {
                cmdif_datainfo_responsedata.dataptr = bptr1;
                cmdif_datainfo_responsedata.datalength = datalength;
                cmdif_datainfo_responsedata.responsecode = CMDIF_ACK_DONE;
                return S_SUCCESS;
            }
        }
        break;
    case FileTask_WholeFileCRC32E:
        //input data: [n:filename][NULL]
        status = S_FAIL;
        bptr1 = __malloc(4);
        if (bptr1)
        {
            fptr = genfs_general_openfile(file_name, "r");
            if (fptr == NULL)
            {
                status = S_OPENFILE;
                __free(bptr1);
                cmdif_datainfo_responsedata.responsecode = CMDIF_ACK_NO_FILEOPEN;
            }
            else
            {
                status = file_calcfilecrc32e(fptr,(u32*)bptr1);
                if (status != S_SUCCESS)
                {
                    __free(bptr1);
                    cmdif_datainfo_responsedata.responsecode = CMDIF_ACK_FAILED;
                }
                else
                {
                    cmdif_datainfo_responsedata.dataptr = bptr1;
                    cmdif_datainfo_responsedata.datalength = 4;
                    cmdif_datainfo_responsedata.responsecode = CMDIF_ACK_OK;
                }
                genfs_closefile(fptr);
            }
        }
        else
        {
            status = S_MALLOC;
            cmdif_datainfo_responsedata.responsecode = CMDIF_ACK_FAILED;
        }
        break;
    case FileTask_TuneOptionFileHeaderCRC32E:
        status = S_FAIL;
        cmdif_datainfo_responsedata.responsecode = CMDIF_ACK_FAILED;
        bptr1 = __malloc(4);
        if (bptr1)
        {
            //must be .oph file
            if (strstr((char*)file_name,".oph"))
            {
                status = obd2tune_calculate_oph_text_portion_crc32e(file_name,(u32*)bptr1);
            }
            else
            {
                log_push_error_point(0x4D36);
                status = S_NOTSUPPORT;
            }
            if (status == S_SUCCESS)
            {
                cmdif_datainfo_responsedata.dataptr = bptr1;
                cmdif_datainfo_responsedata.datalength = 4;
                cmdif_datainfo_responsedata.responsecode = CMDIF_ACK_OK;
            }
            else
            {
                __free(bptr1);
            }
        }
        else
        {
            log_push_error_point(0x4D37);
            status = S_MALLOC;
        }
        break;
    case FileTask_Rename:
        //input data: [n:filename][NULL]
        //filename: [x:oldname][1:'|'][y:newname] note: x+1+y == n
        bptr1 = (u8*)strstr((char*)file_name,"|");
        if (bptr1)
        {
            cmdif_datainfo_responsedata.responsecode = CMDIF_ACK_OK;
            *bptr1++ = NULL; //separate 2 names
            status = genfs_renamefile(file_name,bptr1);
            if (status != S_SUCCESS)
            {
                log_push_error_point(0x4D38);
            }
        }
        else
        {
            status = S_FAIL;
            log_push_error_point(0x4D39);
        }
        break;
    case FileTask_MergeDLX:
        //input data: [n:filename][NULL]
        //filename: [x:newdlxfilename][1:'|'][y:dlxfilename1][1:'|'][z:dlxfilename2] note: x+1+y+1+z == n
        bptr1 = (u8*)strstr((char*)file_name,"|");
        bptr2 = (u8*)strstr((char*)(bptr1+1),"|");
        if (bptr1 && bptr2)
        {
            cmdif_datainfo_responsedata.responsecode = CMDIF_ACK_OK;
            *bptr1++ = NULL; //separate 3 names
            *bptr2++ = NULL;
            status = obd2datalog_merge_dlxfile(file_name,bptr1,bptr2);
            if (status != S_SUCCESS)
            {
                log_push_error_point(0x4D3B);
            }
        }
        else
        {
            status = S_FAIL;
            log_push_error_point(0x4D3C);
        }
        status = S_FAIL;
        break;
    case FileTask_GenerateDatalogFeature:
        //input data: [n:filename][NULL]
        status = obd2datalog_createdatalogfeaturefile(file_name);
        if (status != S_SUCCESS)
        {
            log_push_error_point(0x4D3A);
        }
        break;
    default:
        log_push_error_point(0x4D35);
        status = S_NOTSUPPORT;
        break;
    }
    if (status == S_SUCCESS)
    {
        cmdif_datainfo_responsedata.responsecode = CMDIF_ACK_OK;
    }
    else if (status == S_NOTSUPPORT)
    {
        cmdif_datainfo_responsedata.responsecode = CMDIF_ACK_UNSUPPORTED;
    }
    return status;
}

//------------------------------------------------------------------------------
// Read a block data of a file and send to monitor
// Inputs:  u32 offset
//          u32 blocklength
// Return:  u8  status
// Engineer: Quyen Leba
//------------------------------------------------------------------------------
u8 cmdif_func_filexfer_fread_block(u32 offset, u32 blocklength)
{
    u8  *bptr;
    u32 bytecount;

    cmdif_datainfo_responsedata.responsecode = CMDIF_ACK_FAILED;
    if (filexferinfo.filestream == NULL)
    {
        log_push_error_point(0x4D30);
        cmdif_datainfo_responsedata.responsecode = CMDIF_ACK_NO_FILEOPEN;
        return S_OPENFILE;
    }
    else if (blocklength > 2048 || blocklength == 0)
    {
        log_push_error_point(0x4D31);
        cmdif_datainfo_responsedata.responsecode = CMDIF_ACK_FAILED;
        return S_INPUT;
    }

    if (offset != filexferinfo.currentposition)
    {
        if (fseek(filexferinfo.filestream, offset, F_SEEK_SET) != 0)
        {
            log_push_error_point(0x4D32);
            filexferinfo.transfer_id = 0;
            genfs_closefile(filexferinfo.filestream);
            filexferinfo.filestream = NULL;

            cmdif_datainfo_responsedata.responsecode = CMDIF_ACK_NO_FILEOPEN;
            return S_SEEKFILE;
        }
        filexferinfo.currentposition = offset;
    }

    bptr = __malloc(blocklength);
    if (bptr == NULL)
    {
        log_push_error_point(0x4D33);
        filexferinfo.transfer_id = 0;
        genfs_closefile(filexferinfo.filestream);
        filexferinfo.filestream = NULL;

        cmdif_datainfo_responsedata.responsecode = CMDIF_ACK_FAILED;
        return S_MALLOC;
    }
    bytecount = fread(bptr, 1, blocklength, filexferinfo.filestream);
    filexferinfo.currentposition += bytecount;

    cmdif_datainfo_responsedata.dataptr = bptr;
    cmdif_datainfo_responsedata.datalength = bytecount;
    cmdif_datainfo_responsedata.responsecode = CMDIF_ACK_OK;

    return S_SUCCESS;    
}

//------------------------------------------------------------------------------
// Write a block data of a file with block data received from monitor
// Inputs:  u32 offset
//          u32 blocklength
//          u8  *blockdata
// Return:  u8  status
// Engineer: Quyen Leba
//------------------------------------------------------------------------------
u8 cmdif_func_filexfer_fwrite_block(u32 offset, u32 blocklength, u8 *blockdata)
{
    u32 bytecount;

    cmdif_datainfo_responsedata.responsecode = CMDIF_ACK_FAILED;
    if (filexferinfo.filestream == NULL)
    {
        log_push_error_point(0x4D40);
        cmdif_datainfo_responsedata.responsecode = CMDIF_ACK_NO_FILEOPEN;
        return S_OPENFILE;
    }
    else if (blocklength > 2048 || blocklength == 0)
    {
        log_push_error_point(0x4D41);
        cmdif_datainfo_responsedata.responsecode = CMDIF_ACK_FAILED;
        return S_INPUT;
    }

    if (offset != filexferinfo.currentposition)
    {
        if (fseek(filexferinfo.filestream, offset, F_SEEK_SET) != 0)
        {
            log_push_error_point(0x4D42);
            filexferinfo.transfer_id = 0;
            genfs_closefile(filexferinfo.filestream);
            filexferinfo.filestream = NULL;

            cmdif_datainfo_responsedata.responsecode = CMDIF_ACK_NO_FILEOPEN;
            return S_SEEKFILE;
        }
        filexferinfo.currentposition = offset;
    }

    bytecount = fwrite(blockdata, 1, blocklength, filexferinfo.filestream);
    filexferinfo.currentposition += bytecount;

    if (bytecount == blocklength)
    {
        cmdif_datainfo_responsedata.responsecode = CMDIF_ACK_OK;
    }
    else
    {
        log_push_error_point(0x4D43);
        cmdif_datainfo_responsedata.responsecode = CMDIF_ACK_FAILED;
        return S_WRITEFILE;
    }

    return S_SUCCESS;
}
