/**
  **************** (C) COPYRIGHT 2012 SCT Performance, LLC *********************
  * File Name          : ess_file.h
  * Device / System    : SCT products using the "CommonCode" platform
  * Author             : Ricardo Cigarroa
  *
  * Version            : 1 
  * Date               : 09/09/2014
  * Description        : File format handling functions for E2 Stock Upload File (.EES)
  *                    : 
  *
  *
  * History            : 09/09/2014 R. Cigarroa
  *                    :   Created
  *
  ******************************************************************************
  */
  
  
#define ESS_VERSION                  0 
#define STOCK_E2_FILENAME "\\USER\\E2_stock.ess"
  
  typedef struct
  {
      u8  version;
      u8  reserved[3];
      u32 header_crc32e;
      u32 content_crc32e; 
      u8  e2blockcount; 
      u8  reserved_header[3];
      u8  reseverd_later[16];
  }ESS_HEADER;  //SIZE: 32
  STRUCT_SIZE_CHECK(ESS_HEADER,32);
  
  typedef struct
  {
      u16  type;  //e2 type enum 
      u8   reserved[14];
      u8   blockdata[1024];   
  }ESS_BLOCK;  //SIZE: 1040
  STRUCT_SIZE_CHECK(ESS_BLOCK,1040);
  
  typedef enum
  {
      CHRYSLER           = 0x0000,
      //FORD             = 0x0001
      //GM               = 0x0002
  }e2_type;
  
u8 e2_stock_createfile(F_FILE *file_E2, ESS_HEADER *essheader, ESS_BLOCK *essblock);
u8 e2_stock_encryptfile(F_FILE *file_E2, ESS_HEADER *essheader);
u8 e2_stock_validatefile(const u8 *filename, u8 *response);
u8 e2_stock_readheader(F_FILE *fptr, ESS_HEADER *header);
u8 e2_stock_validateheader(ESS_HEADER *header);