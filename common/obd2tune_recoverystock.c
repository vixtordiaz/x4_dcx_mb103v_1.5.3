/**
  **************** (C) COPYRIGHT 2011 SCT Performance, LLC *********************
  * File Name          : obd2tune_recoverystock.c
  * Device / System    : SCT products using the "CommonCode" platform
  * Author             : Quyen Leba
  *
  * Version            : 1 
  * Date               : 05/02/2011
  * Description        : 
  *                    : 
  *
  *
  * History            : 05/02/2011 M. Davis
  *                    :   Add SCT Copyright header,
  *
  ******************************************************************************
  */

//------------------------------------------------------------------------------
// Log error point: 0x4700 -> 0x47FF
//------------------------------------------------------------------------------

#include <string.h>
#include <fs/genfs.h>
#include <common/statuscode.h>
#include <common/settings.h>
#include <common/obd2tune.h>
#include <common/filestock.h>
#include <common/filestock_option.h>
#include <common/obd2comm.h>
#include <common/crypto_blowfish.h>
#include <common/filecrypto.h>
#include <common/file.h>
#include <common/log.h>
#include "obd2tune_recoverystock.h"

//------------------------------------------------------------------------------
// Private prototypes
//------------------------------------------------------------------------------
u8 obd2tune_recoverystock_set_id_expired(u32 id);

//------------------------------------------------------------------------------
// Initial check on .rsf
// Input:   u8  *recoverystock_header (1st 64 bytes of .rsf)
// Return:  u8  status
// Engineer: Quyen Leba
//------------------------------------------------------------------------------
u8 obd2tune_recoverystock_check(u8 *recoverystock_header)
{
    FileCryptoHeaderInfo *general_header;
    RecoveryStuneInfo *recovery_header;
    u8  header_buffer[RECOVERYSTOCK_HEADER_SIZE];
    F_FILE  *fptr;
    u8  buffer[4096];
    u32 bytecount;
    u32 crc32e_calc;
    u32 i;

    memcpy(header_buffer,recoverystock_header,sizeof(header_buffer));
    crypto_blowfish_decryptblock_critical(header_buffer,
                                          RECOVERYSTOCK_HEADER_SIZE);
    general_header = (FileCryptoHeaderInfo*)header_buffer;
    recovery_header = (RecoveryStuneInfo*)&header_buffer[sizeof(FileCryptoHeaderInfo)];

    crc32e_reset();
    crc32e_calc = crc32e_calculateblock(0xFFFFFFFF,(u32*)(header_buffer + 4),
                                        (RECOVERYSTOCK_HEADER_SIZE - 4)/4);

    if (general_header->hversion > FILECRYPTO_GENERAL_HEADER_VERSION_MAX)
    {
        log_push_error_point(0x4700);
        return S_NOTSUPPORT;
    }
    else if (general_header->ftype != FILETYPE_STOCK_RECOVERY)
    {
        log_push_error_point(0x4701);
        return S_BADCONTENT;
    }
    else if (crc32e_calc != general_header->hcrc32e)
    {
        log_push_error_point(0x4702);
        return S_CRC32E;
    }

    if (settings_compare_deviceserialnumber(recovery_header->serialnumber) != S_SUCCESS)
    {
        log_push_error_point(0x4703);
        return S_UNMATCH;
    }
    
    if(general_header->contentversion < RecoverySContentVersion_Min)
    {
        log_push_error_point(0x4707);
        return S_INVALID_VERSION;        
    }

    if (obd2_validate_ecmbinarytype(recovery_header->vehicletype) != S_SUCCESS)
    {
        log_push_error_point(0x4704);
        return S_INVALIDBINARY;
    }

    fptr = genfs_user_openfile(RECOVERYSTOCK_IDFILE,"r");
    if (fptr)
    {
        crc32e_reset();
        crc32e_calc = 0xFFFFFFFF;
        while(!feof(fptr))
        {
            bytecount = fread(buffer,1,sizeof(buffer),fptr);
            crc32e_calc = crc32e_calculateblock(crc32e_calc,
                                                (u32*)buffer,bytecount/4);
            for(i=0;i<bytecount;i+=4)
            {
                if (recovery_header->recoveryid == *(u32*)(&buffer[i]))
                {
                    genfs_closefile(fptr);
                    log_push_error_point(0x4705);
                    return S_EXPIRED;
                }
            }
        }
        genfs_closefile(fptr);
        if (crc32e_calc != SETTINGS_TUNE(recv_id_crc32e))
        {
            //the file is different
            return S_CRC32E;
        }
    }
    else if (SETTINGS_TUNE(recv_id_crc32e) != 0)
    {
        //the file must be removed
        log_push_error_point(0x4706);
        return S_FILENOTFOUND;
    }

    return S_SUCCESS;
}

//------------------------------------------------------------------------------
// Register a .rsf
// Inputs:  u8  *recoverystock_header (1st 64 bytes of .rsf)
//          u8  *filename
// Return:  u8  status
// Engineer: Quyen Leba
//------------------------------------------------------------------------------
u8 obd2tune_recoverystock_register(u8 *recoverystock_header, u8 *filename,
                                   recoverystock_progressfunction func)
{
    FileCryptoHeaderInfo *general_header;
    RecoveryStuneInfo *recovery_header;
    u8  header_buffer[RECOVERYSTOCK_HEADER_SIZE];
    F_FILE  *fptr;
    F_FILE  *tmpfptr;
    u8  buffer[4096];
    u32 bytecount;
    u8  *current_working_filename;
    u32 filesize;
    u32 calc_crc32e;
    u32 filestock_crc32e;
    u32 filestock_option_crc32e;
    u32 totalbytecount;
    u8  tmp_u8;
    u8  percentage;
    bool isstockfilechanged;
    u8  status;

    fptr = NULL;
    tmpfptr = NULL;
    isstockfilechanged = FALSE;
    memcpy(header_buffer,recoverystock_header,sizeof(header_buffer));
    crypto_blowfish_decryptblock_critical(header_buffer,
                                          RECOVERYSTOCK_HEADER_SIZE);
    general_header = (FileCryptoHeaderInfo*)header_buffer;
    recovery_header = (RecoveryStuneInfo*)&header_buffer[sizeof(FileCryptoHeaderInfo)];

    fptr = genfs_user_openfile(filename,"r");
    if (fptr == NULL)
    {
        log_push_error_point(0x4720);
        return S_FILENOTFOUND;
    }

    status = file_getfilesize(fptr,&filesize);
    if (status != S_SUCCESS)
    {
        log_push_error_point(0x4721);
        status = S_FAIL;
        goto obd2tune_recoverystock_check_done;
    }
    if (filesize != general_header->fsize)
    {
        log_push_error_point(0x4722);
        status = S_UNMATCH;
        goto obd2tune_recoverystock_check_done;
    }
    
    status = file_calcfilecrc32e(fptr,&calc_crc32e);
    if (status != S_SUCCESS)
    {
        log_push_error_point(0x4723);
        status = S_FAIL;
        goto obd2tune_recoverystock_check_done;
    }
    if (calc_crc32e != general_header->fcrc32e)
    {
        log_push_error_point(0x4724);
        status = S_CRC32E;
        goto obd2tune_recoverystock_check_done;
    }

    //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    // No encryption, no compression
    //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    if ((recovery_header->flags & 0x0F) == RecoveryStuneInfoFlags_UnEncrypted &&
        (recovery_header->flags & 0xF0) == RecoveryStuneInfoFlags_UnCompressed)
    {
        //just rename it to stock file
        if (strcmp((char*)filename,(char*)STOCK_FILENAME) != 0)
        {
            filestock_delete(FALSE);
            genfs_closefile(fptr);
            fptr = NULL;
            status = genfs_renamefile(filename,STOCK_FILENAME_NOPATH);
            if (status != S_SUCCESS)
            {
                log_push_error_point(0x4725);
                return S_FAIL;
            }
        }
    }
    else
    {
#define RECOVERYSTOCK_TEMP_FILENAME     GENFS_USER_FOLDER_PATH"recv.tmp"
        //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
        // Handle encryption
        //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
        if ((recovery_header->flags & 0x0F) != RecoveryStuneInfoFlags_UnEncrypted)
        {
            tmpfptr = genfs_user_openfile(RECOVERYSTOCK_TEMP_FILENAME,"w");
            if (!tmpfptr)
            {
                log_push_error_point(0x4726);
                status = S_OPENFILE;
                goto obd2tune_recoverystock_check_done;
            }

            totalbytecount = 0;
            percentage = 0;
            while(!feof(fptr))
            {
                bytecount = fread(buffer,1,sizeof(buffer),fptr);

                //check encryption type
                switch(recovery_header->flags & 0x0F)
                {
                case RecoveryStuneInfoFlags_Blowfish:
                    crypto_blowfish_decryptblock_external_key(buffer,bytecount);
                    break;
                default:
                    status = S_NOTSUPPORT;
                    log_push_error_point(0x4727);
                    goto obd2tune_recoverystock_check_done;
                }
                if (fwrite(buffer,1,bytecount,tmpfptr) != bytecount)
                {
                    log_push_error_point(0x4728);
                    status = S_WRITEFILE;
                    goto obd2tune_recoverystock_check_done;
                }
                
                tmp_u8 = totalbytecount*100 / general_header->fsize;
                if (tmp_u8 >= (percentage+5) && func)
                {
                    percentage = tmp_u8;
                    //a helper to process progress report
                    func(percentage);
                }
                totalbytecount += bytecount;
            }//while(!feof...
            genfs_closefile(tmpfptr);
            tmpfptr = NULL;

            //use this file for uncompression in next step
            current_working_filename = RECOVERYSTOCK_TEMP_FILENAME;
        }
        else
        {
            //unencrypted, prepare for uncompression in next step
            current_working_filename = filename;
        }
        genfs_closefile(fptr);
        fptr = NULL;
        //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
        // Handle compression
        //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
        if ((recovery_header->flags & 0xF0) != RecoveryStuneInfoFlags_UnCompressed)
        {
            //TODOQ: decompress
            
            //check encryption type
            switch(recovery_header->flags & 0xF0)
            {
            case RecoveryStuneInfoFlags_LZW:
                //TODOQ:
                break;
            default:
                log_push_error_point(0x4729);
                status = S_NOTSUPPORT;
                goto obd2tune_recoverystock_check_done;
            }
        }
        else
        {
            //encrypted but uncompressed, just rename it to stock file
            filestock_delete(FALSE);
            status = genfs_renamefile(current_working_filename,STOCK_FILENAME_NOPATH);
            if (status != S_SUCCESS)
            {
                log_push_error_point(0x472A);
                status = S_FAIL;
                goto obd2tune_recoverystock_check_done;
            }
        }
    }

    //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    // Adjust settings to register the stock recovery
    //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    status = filestock_validate(FILESTOCK_USE_STOCKFILE,&filestock_crc32e);
    if (status != S_SUCCESS && status != S_CRC32E)
    {
        status = S_FAIL;
        goto obd2tune_recoverystock_check_done;
    }
    
    if(recovery_header->flags & RecoveryStuneInfoFlags_StockSpecialOptionsIncluded)
    {
        status = filestock_option_validate(&filestock_option_crc32e);
        if (status == S_OPENFILE)
        {
            filestock_option_crc32e = 0;
        }
        else if (status != S_SUCCESS && status != S_CRC32E)
        {
            status = S_FAIL;
            goto obd2tune_recoverystock_check_done;
        }
    }
    else
    {
        filestock_option_crc32e = 0;
    }

    status = settings_recoverystock_set(general_header->fsize,
                                        filestock_crc32e, filestock_option_crc32e,
                                        recovery_header->vehicletype,
                                        recovery_header->variantid);
    if (status == S_SUCCESS)
    {
        status = obd2tune_recoverystock_set_id_expired(recovery_header->recoveryid);
    }

obd2tune_recoverystock_check_done:
    genfs_closefile(fptr);
    genfs_closefile(tmpfptr);
    if (status != S_SUCCESS && isstockfilechanged)
    {
        filestock_delete(FALSE);
    }
    return status;
}

//------------------------------------------------------------------------------
// Set an id as expired (i.e. not to reuse)
// Input:   u32 id
// Return:  u8  status
// Engineer: Quyen Leba
//------------------------------------------------------------------------------
u8 obd2tune_recoverystock_set_id_expired(u32 id)
{
    F_FILE *fptr;
    u8  buffer[4096];
    u32 crc32e_calc;
    u32 bytecount;
    u8  status;

    //save id to file
    fptr = genfs_user_openfile(RECOVERYSTOCK_IDFILE,"a");
    if (!fptr)
    {
        log_push_error_point(0x4740);
        return S_OPENFILE;
    }
    status = genfs_getfilesize_fptr(fptr,&bytecount);
    if (status != S_SUCCESS)
    {
        log_push_error_point(0x4741);
        status = S_FAIL;
        goto obd2tune_recoverystock_set_id_expired_done;
    }
    if (bytecount > 0 && (bytecount % 4))
    {
        //prevent a bad write cause other data useless
        if (fseek(fptr,(bytecount/4)*4,SEEK_SET) != 0)
        {
            log_push_error_point(0x4742);
            status = S_SEEKFILE;
            goto obd2tune_recoverystock_set_id_expired_done;
        }
    }

    bytecount = fwrite((char*)&id,1,sizeof(id),fptr);
    if (bytecount != sizeof(id))
    {
        log_push_error_point(0x4743);
        status = S_WRITEFILE;
        goto obd2tune_recoverystock_set_id_expired_done;
    }
    genfs_closefile(fptr);
    //save file crc32e to settings
    fptr = genfs_user_openfile(RECOVERYSTOCK_IDFILE,"r");
    if (!fptr)
    {
        log_push_error_point(0x4744);
        status = S_OPENFILE;
        goto obd2tune_recoverystock_set_id_expired_done;
    }
    crc32e_reset();
    crc32e_calc = 0xFFFFFFFF;
    while(!feof(fptr))
    {
        bytecount = fread((char*)buffer,1,sizeof(buffer),fptr);
        crc32e_calc = crc32e_calculateblock(crc32e_calc,
                                            (u32*)buffer,bytecount/4);
    }
    genfs_closefile(fptr);
    fptr = NULL;
    SETTINGS_TUNE(recv_id_crc32e) = crc32e_calc;
    SETTINGS_SetTuneAreaDirty();
    status = settings_update(SETTINGS_UPDATE_IF_DIRTY);

obd2tune_recoverystock_set_id_expired_done:
    genfs_closefile(fptr);
    return status;
}

//------------------------------------------------------------------------------
// Validate RECOVERYSTOCK_IDFILE
// Output:  u32 *crc32e
// Return:  u8  status
// Engineer: Quyen Leba
//------------------------------------------------------------------------------
u8 obd2tune_recoverystock_validate_idfile(u32 *crc32e)
{
    F_FILE *fptr;
    u32 crc32e_calc;
    u8  buffer[4096];
    u32 bytecount;

    *crc32e = 0;
    fptr = genfs_user_openfile(RECOVERYSTOCK_IDFILE,"r");
    if (!fptr)
    {
        return S_FILENOTFOUND;
    }
    if (genfs_getfilesize_fptr(fptr,&bytecount) != S_SUCCESS)
    {
        genfs_closefile(fptr);
        return S_BADCONTENT;
    }
    if (bytecount % 4)
    {
        genfs_closefile(fptr);
        return S_BADCONTENT;
    }

    crc32e_reset();
    crc32e_calc = 0xFFFFFFFF;
    while(!feof(fptr))
    {
        bytecount = fread((char*)buffer,1,sizeof(buffer),fptr);
        crc32e_calc = crc32e_calculateblock(crc32e_calc,
                                            (u32*)buffer,bytecount/4);
    }
    genfs_closefile(fptr);

    *crc32e = crc32e_calc;
    if (SETTINGS_TUNE(recv_id_crc32e) != crc32e_calc)
    {
        return S_CRC32E;
    }
    return S_SUCCESS;
}

//------------------------------------------------------------------------------
// Remove RECOVERYSTOCK_IDFILE
// Return:  u8  status
// Engineer: Quyen Leba
//------------------------------------------------------------------------------
u8 obd2tune_recoverystock_remove_idfile()
{
    return genfs_deletefile(RECOVERYSTOCK_IDFILE);
}
