/**
  **************** (C) COPYRIGHT 2011 SCT Performance, LLC *********************
  * File Name          : log.c
  * Device / System    : SCT products using the "CommonCode" platform
  * Author             : Quyen Leba
  *
  * Version            : 1 
  * Date               : 05/02/2011
  * Description        : 
  *                    : 
  *
  *
  * History            : 05/02/2011 M. Davis
  *                    :   Add SCT Copyright header,
  *
  ******************************************************************************
  */

#include <string.h>
#include <device_version.h>
#include <fs/genfs.h>
#include <common/settings.h>
#include <common/bootsettings.h>
#include <common/statuscode.h>
#include <common/itoa.h>
#include <common/debug/debug_output.h>
#include <common/file.h>
#include <common/crc32.h>
#include <common/log.h>

//minimum 256bytes+sizeof(flash_info)
#define LOG_TUNEHISTORY_RECORD_SIZE     (6*1024)

error_point_pool *apperrorpointpool = &SETTINGS_GENERAL(errorpointpool);
error_point_pool *booterrorpointpool = &BOOTSETTINGS(errorpointpool);
extern flasher_info *flasherinfo;

//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
void log_reset()
{
    memset(apperrorpointpool->error_point,
           0,sizeof(apperrorpointpool->error_point));
    apperrorpointpool->position = 0;
    apperrorpointpool->looped = FALSE;

    memset(booterrorpointpool->error_point,
           0,sizeof(booterrorpointpool->error_point));
    booterrorpointpool->position = 0;
    booterrorpointpool->looped = FALSE;
}

//------------------------------------------------------------------------------
// Log an error point; a trail of error points can be helpful to track down
// an error and possibly its cause.
// Inputs:  u16 error_point (a unique error_point number)
// Return:  none
// Engineer: Quyen Leba
//------------------------------------------------------------------------------
void log_push_error_point(u16 error_point)
{
    u8  epstring[20];

    if (apperrorpointpool->position >= LOG_MAX_ERROR_POINT_COUNT)
    {
        apperrorpointpool->position = 0;
        apperrorpointpool->looped = TRUE;
    }
    apperrorpointpool->error_point[apperrorpointpool->position] = error_point;
    apperrorpointpool->position++;
    SETTINGS_SetGeneralAreaDirty();

    //sprintf(epstring,"EP: 0x%X", error_point);
    epstring[0] = 'E';
    epstring[1] = 'P';
    epstring[2] = ':';
    epstring[3] = ' ';
    epstring[4] = '0';
    epstring[5] = 'x';
    itoa(error_point,&epstring[6],16);
    debug_out((u8*)epstring);
}

//------------------------------------------------------------------------------
// Get the list of past error points
// Outputs: u16 *error_point_list
//          u8  *error_point_count
// Return:  u8  status
// Engineer: Quyen Leba
//------------------------------------------------------------------------------
u8 log_get_error_point_list(ErrorPointPoolSource source,
                            u16 *error_point_list, u8 *error_point_count)
{
    u16 i;
    u8  count;
    error_point_pool *errorpointpool;

    if (source == ErrorPointPoolSource_Application)
    {
        errorpointpool = apperrorpointpool;
    }
    else if (source == ErrorPointPoolSource_Boot)
    {
        errorpointpool = booterrorpointpool;
    }
    else
    {
        *error_point_list = NULL;
        *error_point_count = 0;
        return S_INPUT;
    }
    
    if (apperrorpointpool->position == 0 ||
        apperrorpointpool->position > LOG_MAX_ERROR_POINT_COUNT)
    {
        *error_point_count = 0;
        return S_SUCCESS;
    }
    
    count = 0;
    if (errorpointpool->looped == FALSE)
    {
        for(i=errorpointpool->position;i>0;i--)
        {
            error_point_list[count++] = errorpointpool->error_point[i-1];
            if (count >= LOG_MAX_ERROR_POINT_COUNT)
            {
                break;
            }
        }
    }
    else
    {
        for(i=errorpointpool->position;i>0;i--)
        {
            error_point_list[count++] = errorpointpool->error_point[i-1];
            if (count >= LOG_MAX_ERROR_POINT_COUNT)
            {
                break;
            }
        }
        for(i=LOG_MAX_ERROR_POINT_COUNT;i>errorpointpool->position;i--)
        {
            error_point_list[count++] = errorpointpool->error_point[i-1];
            if (count >= LOG_MAX_ERROR_POINT_COUNT)
            {
                break;
            }
        }
    }
    *error_point_count = count;
    return S_SUCCESS;
}

//------------------------------------------------------------------------------
// Get the list of past error points
// Outputs: u16 *error_point_list
//          u8  *error_point_count
// Return:  u8  status
// Engineer: Quyen Leba
//------------------------------------------------------------------------------
u8 log_get_error_point_list_string(ErrorPointPoolSource source,
                                     u8 *list, u16 *listlength)
{
    u16 error_point_list[LOG_MAX_ERROR_POINT_COUNT];
    u8  error_point_count;
    u8  varstr[16];
    u16 currentpointnumber;
    u16 currentpointcount;
    u16 i;
    u8  status;
    
    list[0] = NULL;
    currentpointnumber = 0;
    currentpointcount = 0;
    
    status = log_get_error_point_list(source,error_point_list,&error_point_count);
    if (status != S_SUCCESS)
    {
        *list = NULL;
        *listlength = 0;
        return status;
    }

    if (error_point_count > 0)
    {
        itoa(error_point_list[0],varstr,16);
        strcat((char*)list,(char*)varstr);
        currentpointnumber = error_point_list[0];
        currentpointcount = 1;
    }
    else if (error_point_count > LOG_MAX_ERROR_POINT_COUNT)
    {
        error_point_count = LOG_MAX_ERROR_POINT_COUNT;
    }
    
    for(i=1;i<error_point_count;i++)
    {
        if (currentpointnumber == error_point_list[i])
        {
            currentpointcount++;
        }
        else
        {
            if (currentpointcount > 1)
            {
                itoa(currentpointcount,varstr,10);
                strcat((char*)list,".");
                strcat((char*)list,(char*)varstr);
            }
            
            strcat((char*)list,"-");
            
            currentpointcount = 1;
            itoa(error_point_list[i],varstr,16);
            strcat((char*)list,(char*)varstr);
            
            currentpointnumber = error_point_list[i];
        }
    }
    
    if (currentpointcount > 1)
    {
        itoa(currentpointcount,varstr,10);
        strcat((char*)list,".");
        strcat((char*)list,(char*)varstr);
    }
    
    *listlength = strlen((char*)list);
    return S_SUCCESS;
}

//------------------------------------------------------------------------------
// Log a history of tune programmed
// Return:  u8  status
// Engineer: Quyen Leba
// Note:
//------------------------------------------------------------------------------
u8 log_tunehistory()
{
    F_FILE  *fptr;
    u8  buffer[256];
    u32 bufferlength;
    u32 tmp_u32;
    u8  tmp_u8;
    u32 filesize;
    u8  status;

    fptr = NULL;
    if (LOG_TUNEHISTORY_RECORD_SIZE < (256 + sizeof(flasher_info)))
    {
        //should never happen
        return S_ERROR;
    }
    if (flasherinfo == NULL)
    {
        return S_BADCONTENT;
    }

    file_user_getfilename(LOG_TUNEHISTORY_FILENAME,buffer);
    if (!file_isexist(buffer))
    {
        //it's a new file
        SETTINGS_TUNE(tunehistoryindex) = 0;
    }

    fptr = genfs_user_openfile(LOG_TUNEHISTORY_FILENAME,"a+");
    if (fptr == NULL)
    {
        return S_OPENFILE;
    }

    //fileposition = f_tell(fptr);
    if (fseek(fptr,0,SEEK_END) != 0)
    {
        status = S_SEEKFILE;
        goto log_tunehistory_done;
    }
    filesize = ftell(fptr);
    if (fseek(fptr,0,SEEK_SET) != 0)
    {
        status = S_SEEKFILE;
        goto log_tunehistory_done;
    }

    if (filesize > (LOG_TUNEHISTORY_MAX_RECORD * LOG_TUNEHISTORY_RECORD_SIZE))
    {
        //correct filesize
        if (fseek(fptr,LOG_TUNEHISTORY_MAX_RECORD * LOG_TUNEHISTORY_RECORD_SIZE,
                  SEEK_SET) != 0)
        {
            status = S_SEEKFILE;
            goto log_tunehistory_done;
        }
        fseteof(fptr);
        
        //set file pointer back to the beginning
        if (fseek(fptr,0,SEEK_SET) != 0)
        {
            status = S_SEEKFILE;
            goto log_tunehistory_done;
        }
        SETTINGS_TUNE(tunehistoryindex) = 0;
    }
    else if ((SETTINGS_TUNE(tunehistoryindex)+1) >= LOG_TUNEHISTORY_MAX_RECORD)
    {
        //set file pointer back to the beginning
        if (fseek(fptr,0,SEEK_SET) != 0)
        {
            status = S_SEEKFILE;
            goto log_tunehistory_done;
        }
        SETTINGS_TUNE(tunehistoryindex) = 0;
    }
    else if (filesize >= (SETTINGS_TUNE(tunehistoryindex) * LOG_TUNEHISTORY_RECORD_SIZE))
    {
        //looped
        if (fseek(fptr,SETTINGS_TUNE(tunehistoryindex) * LOG_TUNEHISTORY_RECORD_SIZE,
                  SEEK_SET) != 0)
        {
            status = S_SEEKFILE;
            goto log_tunehistory_done;
        }
        SETTINGS_TUNE(tunehistoryindex)++;
    }
    else
    {
        SETTINGS_TUNE(tunehistoryindex) = (filesize / LOG_TUNEHISTORY_RECORD_SIZE);
        if (fseek(fptr,SETTINGS_TUNE(tunehistoryindex)*LOG_TUNEHISTORY_RECORD_SIZE,
                  SEEK_SET) != 0)
        {
            status = S_SEEKFILE;
            goto log_tunehistory_done;
        }
    }
    
    SETTINGS_TUNE(tunehistoryflashcount)++;

    
    
    //256 bytes
    memset((char*)buffer,0x00,sizeof(buffer));
    //[4: CAFECAFE -> TAG for easy id]
    buffer[0] = 0xCA; buffer[1] = 0xFE; buffer[2] = 0xCA; buffer[3] = 0xFE;
    //[4:flash count]
    memcpy((char*)&buffer[4],(char*)&SETTINGS_TUNE(tunehistoryflashcount),4);
    //[4: flasherinfo crc32e]
    crc32e_reset();
    tmp_u32 = crc32e_calculateblock(0xFFFFFFFF,
                                    (u32*)flasherinfo,sizeof(flasher_info)/4);
    memcpy((char*)&buffer[8],(char*)&tmp_u32,4);
    //[4: Major/Minor/Build]
    tmp_u32 = (APPLICATION_VERSION * 1000) + APPLICATION_VERSION_BUILD;
    memcpy((char*)&buffer[12],(char*)&tmp_u32,4);
    //[32: firmware_tag]
    memcpy((char*)&buffer[16],(char*)&firmware_tag,sizeof(FIRMWARE_TAG));
    fwrite((char*)buffer,1,sizeof(buffer),fptr);
    //[1: flasher_info version]
    tmp_u8 = FLASHER_INFO_VERSION;
    memcpy((char*)&buffer[16+sizeof(FIRMWARE_TAG)],(char*)&tmp_u8,1);

    //flasher info
    fwrite((char*)flasherinfo,1,sizeof(flasher_info),fptr);

    //padding data
    memset((char*)buffer,0x00,sizeof(buffer));
    bufferlength = LOG_TUNEHISTORY_RECORD_SIZE - sizeof(flasher_info) - 256;
    while(bufferlength)
    {
        if (bufferlength > sizeof(buffer))
        {
            fwrite((char*)buffer,1,sizeof(buffer),fptr);
            bufferlength -= sizeof(buffer);
        }
        else
        {
            fwrite((char*)buffer,1,bufferlength,fptr);
            bufferlength = 0;
        }
    }
    SETTINGS_SetTuneAreaDirty();
    settings_update(SETTINGS_UPDATE_IF_DIRTY);
    
log_tunehistory_done:
    if (fptr)
    {
        genfs_closefile(fptr);
    }
    return status;
}

//------------------------------------------------------------------------------
// Log a history of tune programmed
// Return:  u8  status
// Engineer: Quyen Leba
// Note:
//------------------------------------------------------------------------------
void log_tunehistory_delete()
{
    u8  buffer[256];
    
    file_user_getfilename(LOG_TUNEHISTORY_FILENAME,buffer);
    if (file_isexist(buffer))
    {
        genfs_deletefile(buffer);
    }
}

//------------------------------------------------------------------------------
// Log a history of tune programmed
// Return:  u8  status
// Engineer: Quyen Leba
// Note:
//------------------------------------------------------------------------------
u8 log_tunehistory_readblock(u32 blockindex, tunehistoryblock *returnblock)
{
    F_FILE  *fptr;
    s32 readsize;
    u8  buffer[256];
    u8  status;

    fptr = NULL;
    if (LOG_TUNEHISTORY_RECORD_SIZE < (256 + sizeof(flasher_info)))
        return S_ERROR;     //should never happen

    file_user_getfilename(LOG_TUNEHISTORY_FILENAME,buffer);    
    fptr = genfs_user_openfile(LOG_TUNEHISTORY_FILENAME,"r");
    if (fptr == NULL)
        return S_OPENFILE;  //No tunehistory.log file
    
    readsize = fread((char*)returnblock,1,sizeof(tunehistoryblock),fptr);
    if(readsize != sizeof(tunehistoryblock))
        status = S_READFILE;
    else
        status = S_SUCCESS;
    
    if (fptr)
        genfs_closefile(fptr);

    return status;
}
