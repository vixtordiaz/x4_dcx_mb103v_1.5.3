/**
  **************** (C) COPYRIGHT 2011 SCT Performance, LLC *********************
  * File Name          : obd2_gmdef.h
  * Device / System    : SCT products using the "CommonCode" platform
  * Author             : Quyen Leba
  *
  * Version            : 1 
  * Date               : 05/02/2011
  * Description        : 
  *                    : 
  *
  *
  * History            : 05/02/2011 M. Davis
  *                    :   Add SCT Copyright header,
  *
  ******************************************************************************
  */

#ifndef __OBD2_GMDEF_H
#define __OBD2_GMDEF_H

#include <arch/gentype.h>
#include <common/ecm_defs.h>
#include <common/veh_defs.h>

#define VEHICLE_TYPE_STRING                 "GM"
#define PRELOADED_TUNE_LOOKUP_FILENAME      "GM_lookup.txt"

#endif  //__OBD2_GMDEF_H
