/**
  **************** (C) COPYRIGHT 2011 SCT Performance, LLC *********************
  * File Name          : gm_ecm_utility_testfunc.c
  * Device / System    : SCT products using the "CommonCode" platform
  * Author             : Quyen Leba
  *
  * Version            : 1 
  * Date               : 05/02/2011
  * Description        : 
  *                    : 
  *
  *
  * History            : 05/02/2011 M. Davis
  *                    :   Add SCT Copyright header,
  *
  ******************************************************************************
  */

#include <common/statuscode.h>
#include "gm_ecm_utility_testfunc.h"

//------------------------------------------------------------------------------
// This is example function to test if its utility file is applicable
// Input:   u16 ecm_type (defined in ecm_defs)
// Return:  u8  status (S_SUCCESS if applicable)
// Engineer: Quyen Leba
//------------------------------------------------------------------------------
u8 gm_ecm_utility_testfunc_emptytest(u16 ecm_type)
{
    return S_SUCCESS;
}
