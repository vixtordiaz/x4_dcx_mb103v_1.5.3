/**
  **************** (C) COPYRIGHT 2011 SCT Performance, LLC *********************
  * File Name          : obd2tune_gm_checksum.c
  * Device / System    : SCT products using the "CommonCode" platform
  * Author             : Quyen Leba
  *
  * Version            : 1 
  * Date               : 05/02/2011
  * Description        : 
  *                    : 
  *
  *
  * History            : 05/02/2011 M. Davis
  *                    :   Add SCT Copyright header,
  *
  ******************************************************************************
  */

//------------------------------------------------------------------------------
// Log error point: 0x30xx -> 0x31xx
//------------------------------------------------------------------------------

#include <string.h>
#include <common/genmanuf_overload.h>
#include <common/statuscode.h>
#include <common/filestock.h>
#include <common/crc16.h>
#include <common/ecm_defs.h>
#include <common/obd2tune.h>
#include <common/checksum_csf.h>
#include "obd2tune_gm_checksum.h"

extern flasher_info *flasherinfo;                   /* from obd2tune.c */

//------------------------------------------------------------------------------
// Private functions
//------------------------------------------------------------------------------
u8 obd2tune_gm_helper_checksum(u16 veh_type, selected_tunelist_info *tuneinfo,
                               bool ischeckonly);
u8 obd2tune_gm_checksum_generic_vpw(u16 ecm_type, u32 offset,bool ischeckonly);
u8 obd2tune_gm_checksum_lbz_lmm(u16 ecm_type, u32 offset,bool ischeckonly);
u8 obd2tune_gm_checksum_allison(u16 ecm_type, u32 offset,bool ischeckonly);
u8 obd2tune_gm_checksum_generic_group_1(u16 ecm_type, u32 offset,
                                        bool ischeckonly);
u8 obd2tune_gm_checksum_generic_group_1_Exx_OS_checksum(u16 ecm_type, u32 offset,
                                                        bool ischeckonly);
u8 obd2tune_gm_checksum_skip_by_part_number(u8 *os_part_number);

//------------------------------------------------------------------------------
// Get filename for preloaded tunes - NOT USED YET BY GM
// Input:   u16 veh_type
//          selected_tunelist_info *tuneinfo
// Return:  u8  status
// Engineer: Patrick Downs
//------------------------------------------------------------------------------
u8 obd2tune_gm_preloadedcsumfilename(u8 *tunefilename, u8 *checksumfilename)
{
    u8  csf_filename[128];
    u32 status;
    u8  *bptr;
    
    status = S_SUCCESS;
    
    //get csf checksum filename from tunefilename
    //for example: Ex: 68057126AF.csf
    if ((strlen((char*)tunefilename)+strlen(GENFS_DEFAULT_FOLDER_PATH)) >= sizeof(csf_filename))
    {
        log_push_error_point(0x4570);
        status = S_BADCONTENT;
        goto preloadedcsumfilename_done;
    }
    strcpy((char*)csf_filename,(char*)tunefilename);
    bptr = (u8*)strstr((char*)csf_filename,"_");
    if (bptr == NULL)
    {
        bptr = (u8*)strstr((char*)csf_filename,".");
    }
    if (bptr)
    {
        if (strlen((char*)bptr) >= 4)
        {
            strcpy((char*)bptr,".csf");
        }
        else
        {
            log_push_error_point(0x4571);
            status = S_BADCONTENT;
        }
    }
    else
    {
        log_push_error_point(0x4572);
        status = S_BADCONTENT;
    }
    
preloadedcsumfilename_done:    
    //All preloaded checksum files must be in default folder
    if(status == S_SUCCESS)
    {
        if(strstr((const char*)csf_filename, GENFS_DEFAULT_FOLDER_PATH) == 0)
        {
            strcpy((char*)checksumfilename,GENFS_DEFAULT_FOLDER_PATH);
            strcat((char*)checksumfilename,(char*)csf_filename);
        }
        else
            strcpy((char*)checksumfilename,(char*)csf_filename);
    }
    else
    {
        memset((char*)checksumfilename, 0, MAX_TUNE_NAME_LENGTH);
    }
    
    return status;
}

//------------------------------------------------------------------------------
// Apply checksum
// Input:   u16 veh_type
//          selected_tunelist_info *tuneinfo (UNUSED)
// Return:  u8  status
// Engineer: Quyen Leba
//------------------------------------------------------------------------------
u8 obd2tune_gm_apply_checksum(u16 veh_type, selected_tunelist_info *tuneinfo)
{
    return obd2tune_gm_helper_checksum(veh_type,tuneinfo,FALSE);
}

//------------------------------------------------------------------------------
// Check checksum of stock file
// Input:   u16 veh_type
//          selected_tunelist_info *tuneinfo (UNUSED)
// Return:  u8  status
// Engineer: Quyen Leba
//------------------------------------------------------------------------------
u8 obd2tune_gm_check_stockfile_checksum(u16 veh_type, selected_tunelist_info *tuneinfo)
{
    return obd2tune_gm_helper_checksum(veh_type,tuneinfo,TRUE);
}

//------------------------------------------------------------------------------
// Checksum checksum
// Input:   u16 veh_type
//          selected_tunelist_info *tuneinfo (UNUSED)
//          bool ischeckonly (TRUE: check; FALSE: apply checksum to file)
// Return:  u8  status
// Engineer: Quyen Leba
//------------------------------------------------------------------------------
u8 obd2tune_gm_helper_checksum(u16 veh_type, selected_tunelist_info *tuneinfo,
                               bool ischeckonly)
{
    u16 ecm_type;
    u32 ecm_size;
    u32 offset;     //the beginning of an ECM in flash file
    u8  status;
    u8  i;

    if (ischeckonly)    //assume check on stock file
    {
        status = filestock_openstockfile("r",FALSE);
    }
    else
    {
        status = filestock_openflashfile("r+");
    }
    if (status != S_SUCCESS)
    {
        log_push_error_point(0x3051);
        goto obd2tune_gm_helper_checksum;
    }
    
    status = S_FAIL;
    offset = 0;
    for(i=0;i<ECM_MAX_COUNT;i++)
    {
        ecm_type = VEH_GetEcmType(veh_type,i);
        if (ecm_type == INVALID_ECM_DEF)
        {
            //terminated
            break;
        }
        
        // CSF Checksums do not apply to uploads
        if(ECM_IsCsfRequired(ecm_type) && ischeckonly == FALSE)
        {      
            // Handle the CSF Checksums
            if(strlen((char*)tuneinfo->checksumfilename[i]) != 0)
            {
                 status = checksum_csf_apply_to_tunefile(filestock_getflashfile_fptr(),
                                                         tuneinfo->checksumfilename[i],
                                                         ecm_type, ischeckonly);
                 if (status != S_SUCCESS && status != S_FILENOTFOUND)
                 {
                     log_push_error_point(0x3023);
                     status = S_FAIL;
                     goto obd2tune_gm_helper_checksum;
                 }
            }
        }
        
        /* Check if checksums are skipped (also applies to checkonly) */
        status = obd2tune_gm_checksum_skip_by_part_number(flasherinfo->ecminfo.codes[i][0]);
        if (status == S_SUCCESS)
        {
            /* Checksums are skipped here */
            continue;
        }
        else if (status != S_UNMATCH)
        {
            /* Something happened */
            log_push_error_point(0x3060);
            status = S_FAIL;
            goto obd2tune_gm_helper_checksum;
        }
       
        //TODOQ: move this junk to ecm_defs
        switch(ecm_type)
        {
        case 0:     // E40
        case 1:     // E67
        case 4:     // E38
        case 14:    // E37
        case 20:    // E78
        case 21:    // E39-1
        case 22:    // E39-2
        case 24:    // E83A
        case 25:    // E83B
        case 28:    // E92
        case 29:    // E39A
            status = obd2tune_gm_checksum_generic_group_1(ecm_type,offset,ischeckonly);
            if (status == S_SUCCESS)
            {
                status = obd2tune_gm_checksum_generic_group_1_Exx_OS_checksum(ecm_type,offset,ischeckonly);
            }
            break;
        case 10:    // T42
        case 11:    // T43
        case 13:    // T43A
        case 27:    // T76  
            status = obd2tune_gm_checksum_generic_group_1(ecm_type,offset,ischeckonly);
            break;
        case 2:     // LBZ
        case 3:     // LMM
            status = obd2tune_gm_checksum_lbz_lmm(ecm_type,offset,ischeckonly);
            break;
        case 5:     // LB7
        case 6:     // 1Meg
        case 7:     // 1/2Meg
        case 8:     // LLY
            status = obd2tune_gm_checksum_generic_vpw(ecm_type,offset,ischeckonly);
            break;
        case 12:    // Allison
            status = obd2tune_gm_checksum_allison(ecm_type,offset,ischeckonly);
            break;
        default:
            log_push_error_point(0x302E);
            status = S_BADCONTENT;
            goto obd2tune_gm_helper_checksum;
        }
        
        if (status == S_SUCCESS || status == S_NOTREQUIRED)
        {
            status = obd2tune_getecmsize_byecmtype(ecm_type,&ecm_size);
            if (status != S_SUCCESS)
            {
                break;
            }
        }
        else
        {
            // Bad checksum
            break;
        }        
        
        offset += ecm_size;
    }
    
obd2tune_gm_helper_checksum:
    filestock_closeflashfile();
    return status;
}

//------------------------------------------------------------------------------
// Apply checksum for generic VPW (1Meg, 1/2Meg, LB7, LLY)
// Inputs:  u16 ecm_type
//          u32 offset (flash file offset)
//          bool ischeckonly (TRUE: check; FALSE: apply checksum to file)
// Return:  u8  status
// Engineer: Rhonda Burns, Quyen Leba
// Note: require flash file already opened; use filestock_openflashfile(...)
//------------------------------------------------------------------------------
u8 obd2tune_gm_checksum_generic_vpw(u16 ecm_type, u32 offset, bool ischeckonly)
{
    struct
    {
        u32 start_address;
        u32 size;
    }checksum_blockinfo[8];
    u32 end_address;
    u8  databuffer[1024];
    u8  *databuffer_ptr;
    u32 bytecount;
    u32 i,j;
    u32 start_position;
    u32 checksum_position;
    u32 checksum_bytecount;
    u32 blocklength;
    bool checksum_found;
    u16 file_checksum;
    u16 calc_checksum;
    bool isChecksumValid;
    u8  status;

    isChecksumValid = TRUE;     //assume valid then confirm
    //##########################################################################
    // Collect checksum info (start address and size of each checksum block)
    //##########################################################################
    status = filestock_read_at_position(0x50C + offset,databuffer,512,
                                        &bytecount);
    if (status != S_SUCCESS || bytecount != 512)
    {
        log_push_error_point(0x302F);
        status = S_READFILE;
        goto obd2tune_gm_checksum_generic_vpw_done;
    }
    
    databuffer_ptr = databuffer;
    for(i=0;i<8;i++)
    {
        checksum_blockinfo[i].start_address =
            ((u32)databuffer_ptr[0] << 24) | ((u32)databuffer_ptr[1] << 16) |
            ((u32)databuffer_ptr[2] << 8) | ((u32)databuffer_ptr[3]);
        end_address =
            ((u32)databuffer_ptr[4] << 24) | ((u32)databuffer_ptr[5] << 16) |
            ((u32)databuffer_ptr[6] << 8) | ((u32)databuffer_ptr[7]);
        checksum_blockinfo[i].size =
            end_address - checksum_blockinfo[i].start_address + 1;
        
        databuffer_ptr += 8;
    }
    
    //##########################################################################
    // Calculate checksum for each checksum block
    //##########################################################################
    for(i=1;i<8;i++)    //skip 1st block
    {
        status = obd2tune_translate_ecm_address_to_ecm_offset
            (ecm_type, checksum_blockinfo[i].start_address, &start_position);
        if (status != S_SUCCESS)
        {
            log_push_error_point(0x3030);
            goto obd2tune_gm_checksum_generic_vpw_done;
        }
        start_position += offset;
        checksum_position = start_position;
        
        //shift file pointer to that position
        status = filestock_set_position(start_position);
        if (status != S_SUCCESS)
        {
            log_push_error_point(0x3031);
            goto obd2tune_gm_checksum_generic_vpw_done;
        }
        
        checksum_found = FALSE;
        checksum_bytecount = 0;
        calc_checksum = 0;
        while(checksum_bytecount < checksum_blockinfo[i].size)
        {
            blocklength = checksum_blockinfo[i].size - checksum_bytecount;      //TODOQ: check for odd size
            if (blocklength > sizeof(databuffer))
            {
                blocklength = sizeof(databuffer);
            }
            
            //the 1st 2 byte of checksum block is the checksum
            //checksum calculation excludes these 2 bytes
            //checksum is 16-bit additive
            //then xor 0xFFFF, then add 1 to get final checksum value
            //i.e. if block has correct checksum, 16-bit additive checksum of
            //this block (including checksum data) is zero
            
            status = filestock_read(databuffer,blocklength,&bytecount);
            if (status != S_SUCCESS || bytecount != blocklength)
            {
                log_push_error_point(0x3032);
                status = S_READFILE;
                goto obd2tune_gm_checksum_generic_vpw_done;
            }

            if (checksum_found == FALSE)
            {
                checksum_found = TRUE;
                file_checksum = ((u16)databuffer[0] << 8) | ((u16)databuffer[1]);
                for(j=2;j<blocklength;j+=2)
                {
                    calc_checksum +=
                        ((u16)databuffer[j] << 8) | ((u16)databuffer[j+1]);
                }
            }
            else
            {
                for(j=0;j<blocklength;j+=2)
                {
                    calc_checksum +=
                        ((u16)databuffer[j] << 8) | ((u16)databuffer[j+1]);
                }
            }
            checksum_bytecount += blocklength;
        }//while(checksum_bytecount...

        calc_checksum = (calc_checksum ^ 0xFFFF) & 0xFFFF;
        calc_checksum++;

        if (ischeckonly)
        {
            if (calc_checksum != file_checksum)
            {
                isChecksumValid = FALSE;
                status = S_SUCCESS;
            }
        }
        else
        {
            if (calc_checksum != file_checksum)
            {
                databuffer[0] = (u8)(calc_checksum >> 8);
                databuffer[1] = (u8)(calc_checksum);
                status = filestock_updateflashfile(checksum_position,databuffer,2);
                if (status != S_SUCCESS)
                {
                    log_push_error_point(0x3033);
                    goto obd2tune_gm_checksum_generic_vpw_done;
                }
            }
        }
    }//for(i=1...
    
obd2tune_gm_checksum_generic_vpw_done:
    if (status == S_SUCCESS && ischeckonly)
    {
        if (isChecksumValid == FALSE)
        {
            return S_REJECT;
        }
    }

    return status;
}

//------------------------------------------------------------------------------
// Apply checksum for LBZ, LMM
// Inputs:  u16 ecm_type
//          u32 offset (flash file offset)
//          bool ischeckonly (TRUE: check; FALSE: apply checksum to file)
// Return:  u8  status
// Engineer: Rhonda Burns, Quyen Leba
// Note: require flash file already opened; use filestock_openflashfile(...)
//------------------------------------------------------------------------------
u8 obd2tune_gm_checksum_lbz_lmm(u16 ecm_type, u32 offset, bool ischeckonly)
{
    const u8 signature[8] = {0xfa, 0xde, 0xca, 0xfe, 0xca, 0xfe, 0xaf, 0xfe};
    struct
    {
        u32 part1;
        u32 part2;
    }signature_ref;
    struct
    {
        u32 part1;
        u32 part2;
    }signature_cmp;
    u8  header_blockbuffer[1024];
    u8  checksum_blockbuffer[1024];
    u32 i,j,k;
    u32 memblock_address;
    u32 memblock_size;
    u32 position;
    u32 bytecount;
    u32 blocklength;
    u32 checksumblock_checksumaddr;
    u32 checksumblock_length;
    u32 checksumblock_startaddr;
    u32 checksumblock_endaddr;
    u32 checksum_value;
    u32 checksumblock_bytecount;
    u32 tmpvalue;
    bool isChecksumValid;
    u8  status;
    
    memcpy((char*)&signature_ref,(char*)signature,8);
    status = S_FAIL;
    isChecksumValid = TRUE;     //assume valid then confirm
    
    //TODOQ: checksum for calibration area only (for now)
    for(i=0;i<MAX_ECM_MEMORY_BLOCKS;i++)
    {
        if (ECM_GetCalBlockSize(ecm_type,i) == 0)
        {
            break;
        }
        
        //get position where this memblock starts in stock/flash file
        memblock_address = ECM_GetCalBlockAddr(ecm_type,i);
        memblock_size = ECM_GetCalBlockSize(ecm_type,i);
        status = obd2tune_translate_ecm_address_to_ecm_offset
            (ecm_type, memblock_address, &position);
        if (status != S_SUCCESS)
        {
            log_push_error_point(0x3034);
            goto obd2tune_gm_checksum_lbz_lmm_done;
        }
        position += offset;

        status = filestock_read_at_position(position,header_blockbuffer,
                                            sizeof(header_blockbuffer),
                                            &bytecount);
        if (status != S_SUCCESS || bytecount != sizeof(header_blockbuffer))
        {
            log_push_error_point(0x3035);
            status = S_READFILE;
            goto obd2tune_gm_checksum_lbz_lmm_done;
        }
        
        //the signature is somewhere within 1st 1k bytes of memblock
        //search for it
        for(j=0;j<(1024-8);j++)
        {
            memcpy((char*)&signature_cmp,(char*)&header_blockbuffer[j],8);
            if (signature_cmp.part1 == signature_ref.part1 &&
                signature_cmp.part2 == signature_ref.part2)
            {
                //signature found
                
                checksumblock_checksumaddr =
                    ((u32)header_blockbuffer[j+12] << 24) |
                    ((u32)header_blockbuffer[j+13] << 16) |
                    ((u32)header_blockbuffer[j+14] << 8)  |
                    ((u32)header_blockbuffer[j+15]);
                checksumblock_endaddr =
                    ((u32)header_blockbuffer[j-4] << 24) |
                    ((u32)header_blockbuffer[j-3] << 16) |
                    ((u32)header_blockbuffer[j-2] << 8)  |
                    ((u32)header_blockbuffer[j-1]);
                checksumblock_startaddr =
                    ((u32)header_blockbuffer[j-8] << 24) |
                    ((u32)header_blockbuffer[j-7] << 16) |
                    ((u32)header_blockbuffer[j-6] << 8)  |
                    ((u32)header_blockbuffer[j-5]);
                checksumblock_length = checksumblock_endaddr - checksumblock_startaddr + 1;
                
                if (checksumblock_startaddr < memblock_address ||
                    checksumblock_startaddr > (memblock_address + memblock_size) ||
                    checksumblock_startaddr > checksumblock_endaddr ||
                    checksumblock_checksumaddr < checksumblock_startaddr ||
                    checksumblock_checksumaddr > checksumblock_endaddr)
                {
                    //address is invalid
                    log_push_error_point(0x3036);
                    status = S_BADCONTENT;
                    goto obd2tune_gm_checksum_lbz_lmm_done;
                }
                else
                {
                    //set file to position of start_addr
                    status = obd2tune_translate_ecm_address_to_ecm_offset
                        (ecm_type, checksumblock_startaddr, &position);
                    if (status != S_SUCCESS)
                    {
                        log_push_error_point(0x3037);
                        goto obd2tune_gm_checksum_lbz_lmm_done;
                    }
                    position += offset;
                    status = filestock_set_position(position);
                    if (status != S_SUCCESS)
                    {
                        log_push_error_point(0x3038);
                        goto obd2tune_gm_checksum_lbz_lmm_done;
                    }
                    //##########################################################
                    // Calculate the checksum
                    //##########################################################
                    checksumblock_bytecount = 0;
                    checksum_value = 0;
                    while(checksumblock_bytecount < checksumblock_length)
                    {
                        blocklength = checksumblock_length - checksumblock_bytecount;
                        if (blocklength > sizeof(checksum_blockbuffer))
                        {
                            blocklength = sizeof(checksum_blockbuffer);
                        }
                        status = filestock_read(checksum_blockbuffer,blocklength,
                                                &bytecount);
                        if (status != S_SUCCESS || bytecount != blocklength)
                        {
                            log_push_error_point(0x3039);
                            status = S_READFILE;
                            goto obd2tune_gm_checksum_lbz_lmm_done;
                        }
                        
                        for(k=0;k<blocklength;k+=4)
                        {
                            checksum_value += 
                                ((u32)checksum_blockbuffer[k+0] << 24) |
                                ((u32)checksum_blockbuffer[k+1] << 16) |
                                ((u32)checksum_blockbuffer[k+2] << 8) |
                                ((u32)checksum_blockbuffer[k+3]);
                        }
                        checksumblock_bytecount += blocklength;
                    }//while(bytecount...
                    checksum_value = 0xD01FE500 - checksum_value;

                    if (ischeckonly)
                    {
                        if (checksum_value != 0)
                        {
                            isChecksumValid = FALSE;
                            status = S_SUCCESS;
                        }
                    }
                    else
                    {
                        //TODOQ: why we didn't use checksumblock_checksumaddr
                        if (checksum_value != 0)
                        {
                            //set file to position where checksum data stored
                            status = obd2tune_translate_ecm_address_to_ecm_offset
                                (ecm_type, checksumblock_endaddr, &position);
                            if (status != S_SUCCESS)
                            {
                                log_push_error_point(0x303A);
                                goto obd2tune_gm_checksum_lbz_lmm_done;
                            }
                            position += offset;
                            position -= 3;
                            
                            status = filestock_read_at_position
                                (position,checksum_blockbuffer,4,&bytecount);
                            if (status != S_SUCCESS || bytecount != 4)
                            {
                                log_push_error_point(0x303B);
                                status = S_READFILE;
                                goto obd2tune_gm_checksum_lbz_lmm_done;
                            }
                            
                            tmpvalue = 
                                ((u32)checksum_blockbuffer[0] << 24) |
                                    ((u32)checksum_blockbuffer[1] << 16) |
                                        ((u32)checksum_blockbuffer[2] << 8)  |
                                            ((u32)checksum_blockbuffer[3]);
                            tmpvalue += checksum_value;
                            checksum_blockbuffer[0] = (u8)(tmpvalue >> 24);
                            checksum_blockbuffer[1] = (u8)(tmpvalue >> 16);
                            checksum_blockbuffer[2] = (u8)(tmpvalue >> 8);
                            checksum_blockbuffer[3] = (u8)(tmpvalue);
                            
                            status = filestock_updateflashfile(position,
                                                               checksum_blockbuffer,
                                                               4);
                            if (status != S_SUCCESS)
                            {
                                log_push_error_point(0x303C);
                                status = S_WRITEFILE;
                                goto obd2tune_gm_checksum_lbz_lmm_done;
                            }
                            //status = S_SUCCESS;   //should already S_SUCCESS
                            
                        }//if (checksum_value != 0)...
                    }
                }
                
                j += 30;
                
            }//if (signature_cmp...
        }//for(j=0;j<(1024-8);j++)...
    }//for(i=0;i<MAX_ECM_MEMORY_BLOCKS;i++)...
    
obd2tune_gm_checksum_lbz_lmm_done:
    if (status == S_SUCCESS && ischeckonly)
    {
        if (isChecksumValid == FALSE)
        {
            return S_REJECT;
        }
    } 
    return status;
}

//------------------------------------------------------------------------------
// Apply checksum for Allison transmission
// Input:   u32 offset (flash file offset)
//          bool ischeckonly (TRUE: check; FALSE: apply checksum to file)
// Return:  u8  status
// Engineer: Rhonda Burns, Quyen Leba
// Note: require flash file already opened; use filestock_openflashfile(...)
//------------------------------------------------------------------------------
u8 obd2tune_gm_checksum_allison(u16 ecm_type, u32 offset, bool ischeckonly)
{
    const u8 signature[8] = {0x4A, 0xFC, 0x4A, 0xFC, 0x4A, 0xFC, 0x4A, 0xFC};
    struct
    {
        u32 part1;
        u32 part2;
    }signature_ref;
    struct
    {
        u32 part1;
        u32 part2;
    }signature_cmp;
    u8  header_blockbuffer[1024];
    u8  checksum_blockbuffer[1024];
    u32 checksum_blockbufferlength;
    u32 start_position;
    u32 checksum_position;
    u32 current_position;
    u32 bytecount;
    u32 i;
    u16 calc_crc16;
    u16 calc_additive_checksum;
    u16 file_crc16;
    u16 file_additive_checksum;
    u32 memblock_address;
    u32 memblock_size;
    u8  memblock_index;
    u32 memblock_bytecount;
    u32 blocklength;
    bool isChecksumValid;
    u8  status;
    
    memcpy((char*)&signature_ref,(char*)signature,8);
    status = S_FAIL;
    isChecksumValid = TRUE;     //assume valid then confirm
    
    //TODOQ: for now, it only does checksum on the 1st memblock of cal only
    //however, the code is already setup so that loop is possible to handle
    //other memblocks
    
    memblock_index = 0;
    memblock_address = ECM_GetCalBlockAddr(ecm_type,memblock_index);
    memblock_size = ECM_GetCalBlockSize(ecm_type,memblock_index);
    
    //set file to position where this memblock located
    status = obd2tune_translate_ecm_address_to_ecm_offset
        (ecm_type, memblock_address, &start_position);
    if (status != S_SUCCESS)
    {
        log_push_error_point(0x303D);
        goto obd2tune_gm_checksum_allison_done;
    }
    start_position += offset;
    status = filestock_set_position(start_position);
    if (status != S_SUCCESS)
    {
        log_push_error_point(0x303E);
        goto obd2tune_gm_checksum_allison_done;
    }
    
    //search for signature
    memblock_bytecount = 0;
    checksum_position = 0;
    while(memblock_bytecount < memblock_size)
    {
        checksum_blockbufferlength = memblock_size - memblock_bytecount;
        if (checksum_blockbufferlength > sizeof(header_blockbuffer))
        {
            checksum_blockbufferlength = sizeof(header_blockbuffer);
        }
        status = filestock_read(header_blockbuffer,checksum_blockbufferlength,
                                &bytecount);
        if (status != S_SUCCESS || bytecount != checksum_blockbufferlength)
        {
            log_push_error_point(0x303F);
            status = S_READFILE;
            goto obd2tune_gm_checksum_allison_done;
        }
        for(i=0;i<checksum_blockbufferlength;i++)
        {
            memcpy((char*)&signature_cmp,(char*)&header_blockbuffer[i],8);
            if (signature_cmp.part1 == signature_ref.part1 &&
                signature_cmp.part2 == signature_ref.part2)
            {
                //signature found
                file_crc16  = (u16)(header_blockbuffer[i-4] << 8);
                file_crc16 |= (u16)(header_blockbuffer[i-3]);
                file_additive_checksum  = (u16)header_blockbuffer[i-2] << 8;
                file_additive_checksum |= (u16)header_blockbuffer[i-1];
                
                //find where this checksum store in flash file
                status = obd2tune_translate_ecm_address_to_ecm_offset
                    (ecm_type, memblock_address + memblock_bytecount + i - 4,
                     &checksum_position);
                if (status != S_SUCCESS)
                {
                    log_push_error_point(0x3040);
                    goto obd2tune_gm_checksum_allison_done;
                }
                checksum_position += offset;
                
                //##############################################################
                // calculate checksum
                //##############################################################
                calc_crc16 = 0;
                calc_additive_checksum = 0;
                current_position = start_position;  //where this memblock starts
                status = filestock_set_position(start_position);
                if (status != S_SUCCESS)
                {
                    log_push_error_point(0x3041);
                    goto obd2tune_gm_checksum_allison_done;
                }
                while(current_position < checksum_position)
                {
                    blocklength = checksum_position - current_position;
                    if (blocklength > sizeof(checksum_blockbuffer))
                    {
                        blocklength = sizeof(checksum_blockbuffer);
                    }
                    status = filestock_read(checksum_blockbuffer,blocklength,
                                            &bytecount);
                    if (status != S_SUCCESS || bytecount != blocklength)
                    {
                        log_push_error_point(0x3042);
                        status = S_READFILE;
                        goto obd2tune_gm_checksum_allison_done;
                    }
                    
                    calc_crc16 = crc16_calculateblock(calc_crc16,
                                                      checksum_blockbuffer,
                                                      blocklength);
                    for(i=0;i<blocklength;i+=2)
                    {
                        calc_additive_checksum += 
                            (((u16)checksum_blockbuffer[i]) << 8) |
                            (((u16)checksum_blockbuffer[i+1]));
                    }
                    
                    current_position += blocklength;
                }
                
                calc_additive_checksum += calc_crc16;
                calc_additive_checksum = calc_additive_checksum ^ 0xFFFF;
                calc_additive_checksum++;
                
                //##############################################################
                // check or apply checksum
                //##############################################################
                if (ischeckonly)
                {
                    if (calc_additive_checksum != file_additive_checksum ||
                        calc_crc16 != file_crc16)
                    {
                        isChecksumValid = FALSE;
                        status = S_SUCCESS;
                    }
                }
                else
                {
                    if (calc_additive_checksum != file_additive_checksum ||
                        calc_crc16 != file_crc16)
                    {
                        checksum_blockbuffer[0] = (u8)(calc_crc16 >> 8);
                        checksum_blockbuffer[1] = (u8)(calc_crc16);
                        checksum_blockbuffer[2] = (u8)(calc_additive_checksum >> 8);
                        checksum_blockbuffer[3] = (u8)(calc_additive_checksum);
                        
                        status = filestock_updateflashfile(checksum_position,
                                                           checksum_blockbuffer,4);
                        if (status != S_SUCCESS)
                        {
                            log_push_error_point(0x3043);
                            goto obd2tune_gm_checksum_allison_done;
                        }
                    }
                }
                
                //terminate; there's only one account of checksum
                memblock_bytecount = memblock_size;
                break;
            }
        }//for(i=...
        
        memblock_bytecount += checksum_blockbufferlength;
        
    }//while(memblock_bytecount...
    
obd2tune_gm_checksum_allison_done:
    if (status == S_SUCCESS && ischeckonly)
    {
        if (isChecksumValid == FALSE)
        {
            return S_REJECT;
        }
    }

    return status;
}

//------------------------------------------------------------------------------
// Apply checksum for ECMs in generic group 1 (E38,E40,E67,T42,T43)
// Inputs:  u16 ecm_type (defined for ecm_defs)
//          u32 offset (flash file offset)
//          bool ischeckonly (TRUE: check; FALSE: apply)
// Return:  u8  status
// Engineer: Rhonda Burns, Quyen Leba
// Note: require flash file already opened; use filestock_openflashfile(...)
//------------------------------------------------------------------------------
u8 obd2tune_gm_checksum_generic_group_1(u16 ecm_type, u32 offset,
                                        bool ischeckonly)
{
    u32 start_position;
    u32 end_address;
    u8  checksum_part_start_crc16;
    u8  checksum_part_start_additive;
    u8  checksum_part_count;
    u8  databuffer[4096];
    u8  *databuffer_bptr;
    u32 bytecount;
    struct
    {
        u32 start_address;
        u32 size;
    }checksum_blockinfo[8];
//    u32 memblock_address;
//    u32 memblock_size;
//    u32 memblock_index;
//    u32 memblock_position;
    u32 checksum_blockinfo_offset;
    bool checksum_found;
    u16 file_crc16;
    u16 file_checksum;
    u16 calc_crc16;
    u16 calc_checksum;
    u32 crc16_offset;
    u32 checksumblock_bytecount;
    u32 blocklength;
    u32 i,j;
    bool isChecksumValid;
    u8  status;

    isChecksumValid = TRUE;     //assume valid then confirm it
//    memblock_index = 0;
//    memblock_address = ECM_GetCalBlockAddr(ecm_type,memblock_index);
//    memblock_size = ECM_GetCalBlockSize(ecm_type,memblock_index);

    //##########################################################################
    // Collect chechsum info
    //##########################################################################
    switch(ecm_type)
    {
    case 0x00:  //E40
        start_position = 0x01FF00 + offset;
        checksum_part_start_crc16 = 0;
        checksum_part_start_additive = 1-1;
        checksum_part_count = 5;
        break;
    case 0x01:  //E67
    case 0x04:  //E38
        start_position = 0x10024 + offset;
        checksum_part_start_crc16 = 1;
        checksum_part_start_additive = 2-1;
        checksum_part_count = 6;
        break;
    case 0x0A:  //T42
        //start_position = 0x10024 + offset;
        start_position = 0x10024 + 0x38 + 0x04 + offset;    //skip main OS checksum
        checksum_part_start_crc16 = 0;
        checksum_part_start_additive = 1-1;
        checksum_part_count = 3;
        break;
    case 0x0B:  //T43
    case 0x0D:  //T43A
        //start_position = 0x02A0C0 + offset;   //old block addres & didn't work
        start_position = 0x06006E + offset;
        checksum_part_start_crc16 = 0;
        checksum_part_start_additive = 1-1;
        checksum_part_count = 3;
        break;
    case 0x0E:  //E37
        start_position = 0x0E024 + offset;
        checksum_part_start_crc16 = 1;
        checksum_part_start_additive = 2-1;
        checksum_part_count = 6;
        break;
    case 0x14:  //E78
    case 0x1D:  //E39A  
        start_position = 0xCB026 + offset;
        checksum_part_start_crc16 = 1;
        checksum_part_start_additive = 2-1;
        checksum_part_count = 6;
        break;
    case 0x15:  //E39-1
    case 0x16:  //E39-2
        start_position = 0xCB05A + offset;
        checksum_part_start_crc16 = 0;
        checksum_part_start_additive = 1-1;
        checksum_part_count = 4;
        break;
    case 0x18:  //E83A
        start_position = 0x10062 + offset;
        checksum_part_start_crc16 = 0;
        checksum_part_start_additive = 1-1;
        checksum_part_count = 5;
        break;
    case 0x19:  //E83B
        start_position = 0x1007A + offset;
        checksum_part_start_crc16 = 0;
        checksum_part_start_additive = 1-1;
        checksum_part_count = 5;
        break;
    case 0x1B:  //T76
        start_position = 0x20422 + offset;
        checksum_part_start_crc16 = 1;
        checksum_part_start_additive = 2-1;
        checksum_part_count = 4;
        break;
    case 0x1C:  //E92
        start_position = 0xC0126 + offset;
        checksum_part_start_crc16 = 1;
        checksum_part_start_additive = 2-1;
        checksum_part_count = 6;
        break;
    default:
        log_push_error_point(0x3044);
        return S_INPUT;
    }
    
    status = filestock_read_at_position(start_position,databuffer, 512,
                                        &bytecount);
    if (status != S_SUCCESS || bytecount != 512)
    {
        log_push_error_point(0x3045);
        status = S_READFILE;
        goto obd2tune_gm_checksum_generic_group_1_done;
    }
    
    checksum_blockinfo[0].start_address = 
        (u32)(databuffer[0] << 24) + (u32)(databuffer[1] << 16) +
        (u32)(databuffer[2] << 8) + (u32)(databuffer[3]);
    end_address =
        (u32)(databuffer[4] << 24) + (u32)(databuffer[5] << 16) +
        (u32)(databuffer[6] << 8) + (u32)(databuffer[7]);
    checksum_blockinfo[0].size =
        end_address - checksum_blockinfo[0].start_address + 1;

    //in this switch, databuffer_bptr += ### because of checksum_blockinfo[0]
    databuffer_bptr = databuffer;
    switch(ecm_type)
    {
    case 0x00:  //E40
        databuffer_bptr += 0x33;            //0x2F + 4;
        checksum_blockinfo_offset = 0x33;   //0x2F + 4;
        crc16_offset = 0x1C;
        break;
    case 0x01:  //E67
    case 0x04:  //E38
    case 0x0E:  //E37
        databuffer_bptr += 0x24;            //0x20 + 4;
        checksum_blockinfo_offset = 0x23;   //0x1F + 4;
        crc16_offset = 0x1C;
        break;
    case 0x0A:  //T42
        //the databuffer_bptr += (0x38 + 0x04) because of skip main OS checksum
        databuffer_bptr += 0x23;            //0x1F + 4;
        checksum_blockinfo_offset = 0x23;   //0x1F + 4;
        crc16_offset = 0x1C;
        break;
    case 0x0B:  //T43
    case 0x0D:  //T43A
        databuffer_bptr += 0x23;            //0x1F + 4;
        checksum_blockinfo_offset = 0x23;   //0x1F + 4;
        crc16_offset = 0x1A;
        break;
    case 0x14:  //E78
    case 0x1D:  //E39A
        databuffer_bptr += 0x34;            //0x30 + 4;
        checksum_blockinfo_offset = 0x24;   //0x20 + 4;
        crc16_offset = 0x1E;
        break;
    case 0x15:  //E39-1
    case 0x16:  //E39-2
        databuffer_bptr += 0x48;            //0x44 + 4;
        checksum_blockinfo_offset = 0x24;   //0x20 + 4;
        crc16_offset = 0x1E;
        break;
    case 0x18:  //E83A
    case 0x19:  //E83B
        databuffer_bptr += 0x24;            //0x20 + 4;
        checksum_blockinfo_offset = 0x24;   //0x20 + 4;
        crc16_offset = 0x1E;
        break;
    case 0x1B:  //T76
        databuffer_bptr += 0x34;            //0x30 + 4;
        checksum_blockinfo_offset = 0x23;   //0x1F + 4;
        crc16_offset = 0x1A;
        break;
    case 0x1C:  //E92
        databuffer_bptr += 0x3C;            //0x38 + 4;
        checksum_blockinfo_offset = 0x24;   //0x20 + 4;
        crc16_offset = 0x1E;
        break;
    default:
        log_push_error_point(0x3046);
        return S_INPUT;
    }
    //NOTE:
    //databuffer_bptr is now set to the beginning of the 2nd checksum info
    //checksum_blockinfo_offset is the offset between checksum info
    //crc16_offset is the location of the checksum starting from .start_address
    
    for(i=1;i<checksum_part_count;i++)
    {
        checksum_blockinfo[i].start_address =
            ((u32)databuffer_bptr[0] << 24) |
            ((u32)databuffer_bptr[1] << 16) |
            ((u32)databuffer_bptr[2] << 8)  |
            ((u32)databuffer_bptr[3]);
        end_address =
            ((u32)databuffer_bptr[4] << 24) |
            ((u32)databuffer_bptr[5] << 16) |
            ((u32)databuffer_bptr[6] << 8)  |
            ((u32)databuffer_bptr[7]);
        checksum_blockinfo[i].size =
            end_address - checksum_blockinfo[i].start_address + 1;
        databuffer_bptr += checksum_blockinfo_offset;
    }
    
    //##########################################################################
    // Check and apply crc16 (1st) & checksum (2nd)
    //##########################################################################
    for(i=checksum_part_start_crc16;i<checksum_part_count;i++)
    {
        checksum_found = FALSE;
        calc_crc16 = 0;
        calc_checksum = 0;
        
        //######################################################################
        // Part 1: calculate the crc16
        //######################################################################
        
        status = obd2tune_translate_ecm_address_to_ecm_offset
            (ecm_type, checksum_blockinfo[i].start_address, &start_position);
        if (status != S_SUCCESS)
        {
            log_push_error_point(0x3047);
            goto obd2tune_gm_checksum_generic_group_1_done;
        }
        start_position += offset;
        start_position += 2;
        //shift file pointer to that position
        status = filestock_set_position(start_position);
        if (status != S_SUCCESS)
        {
            log_push_error_point(0x3048);
            goto obd2tune_gm_checksum_generic_group_1_done;
        }
        
        //Note:
        //The 1st 2-byte is the checksum and must not include in crc16
        //calculation
        
        checksumblock_bytecount = 2;
        while(checksumblock_bytecount < checksum_blockinfo[i].size)
        {
            blocklength = checksum_blockinfo[i].size - checksumblock_bytecount;
            if (blocklength > sizeof(databuffer))
            {
                blocklength = sizeof(databuffer);
            }
            status = filestock_read(databuffer,blocklength,&bytecount);
            if (status != S_SUCCESS || bytecount != blocklength)
            {
                log_push_error_point(0x3049);
                goto obd2tune_gm_checksum_generic_group_1_done;
            }
            
            if (checksum_found == FALSE)
            {
                checksum_found = TRUE;
                
                //NOTE: crc16 is store in the 1st 1k of checksum block
                if (crc16_offset < blocklength)
                {
                    calc_crc16 = crc16_calculateblock
                        (calc_crc16, databuffer,crc16_offset);
                    calc_crc16 = crc16_calculateblock
                        (calc_crc16, &databuffer[crc16_offset+2],
                         blocklength - crc16_offset - 2);
                    file_crc16 =
                        ((u16)databuffer[crc16_offset+1] << 8) |
                        ((u16)databuffer[crc16_offset]);
                }
                else
                {
                    log_push_error_point(0x304A);
                    status = S_BADCONTENT;
                    goto obd2tune_gm_checksum_generic_group_1_done;
                }
            }
            else
            {
                calc_crc16 = crc16_calculateblock(calc_crc16,
                                                  databuffer,blocklength);
            }
            checksumblock_bytecount += blocklength;
        }//while(checksumblock_bytecount...

        if (ischeckonly)
        {
            if (calc_crc16 != file_crc16)
            {
                isChecksumValid = FALSE;
                status = S_SUCCESS;
            }
        }
        else
        {
            if (calc_crc16 != file_crc16)
            {
                databuffer[0] = (u8)(calc_crc16);
                databuffer[1] = (u8)(calc_crc16 >> 8);
                status = filestock_updateflashfile(start_position + crc16_offset,
                                                   databuffer,2);
                if (status != S_SUCCESS)
                {
                    log_push_error_point(0x304B);
                    goto obd2tune_gm_checksum_generic_group_1_done;
                }
            }
        }
    }//for(i=checksum_part_start_crc16;i<checksum_part_count;i++)...

    for(i=checksum_part_start_additive;i<checksum_part_count;i++)
    {
        //######################################################################
        // Part 2: calculate the checksum
        //######################################################################
        status = obd2tune_translate_ecm_address_to_ecm_offset
            (ecm_type, checksum_blockinfo[i].start_address, &start_position);
        if (status != S_SUCCESS)
        {
            log_push_error_point(0x304C);
            goto obd2tune_gm_checksum_generic_group_1_done;
        }
        start_position += offset;
        //shift file pointer to that position
        status = filestock_set_position(start_position);
        if (status != S_SUCCESS)
        {
            log_push_error_point(0x304D);
            goto obd2tune_gm_checksum_generic_group_1_done;
        }
        
        checksum_found = FALSE;
        checksumblock_bytecount = 0;
        calc_checksum = 0;
        while(checksumblock_bytecount < checksum_blockinfo[i].size)
        {
            blocklength = checksum_blockinfo[i].size - checksumblock_bytecount;
            if (blocklength > sizeof(databuffer))
            {
                blocklength = sizeof(databuffer);
            }
            status = filestock_read(databuffer,blocklength,&bytecount);
            if (status != S_SUCCESS || bytecount != blocklength)
            {
                log_push_error_point(0x304E);
                goto obd2tune_gm_checksum_generic_group_1_done;
            }

            if (checksum_found == FALSE)
            {
                checksum_found = TRUE;
                file_checksum = ((u16)databuffer[0] << 8) | ((u16)databuffer[1]);
                for(j=2;j<blocklength;j+=2)
                {
                    calc_checksum +=
                        ((u16)databuffer[j] << 8) | ((u16)databuffer[j+1]);
                }
            }
            else
            {
                for(j=0;j<blocklength;j+=2)
                {
                    calc_checksum +=
                        ((u16)databuffer[j] << 8) | ((u16)databuffer[j+1]);
                }
            }
            checksumblock_bytecount += blocklength;
        }//while(checksumblock_bytecount...

        calc_checksum = (calc_checksum ^ 0xFFFF) & 0xFFFF;
        calc_checksum++;

        if (ischeckonly)
        {
            if (calc_checksum != file_checksum)
            {
                isChecksumValid = FALSE;
                status = S_SUCCESS;
            }
        }
        else
        {
            if (calc_checksum != file_checksum)
            {
                databuffer[0] = (u8)(calc_checksum >> 8);
                databuffer[1] = (u8)(calc_checksum);
                status = filestock_updateflashfile(start_position,
                                                   databuffer,2);
                if (status != S_SUCCESS)
                {
                    log_push_error_point(0x304F);
                    goto obd2tune_gm_checksum_generic_group_1_done;
                }
            }
        }
    }///for(i=checksum_part_start_additive;i<checksum_part_count;i++)...
    
obd2tune_gm_checksum_generic_group_1_done:
    if (status == S_SUCCESS && ischeckonly)
    {
        if (isChecksumValid == FALSE)
        {
            return S_REJECT;
        }
    }

    return status;
}

//------------------------------------------------------------------------------
// Apply checksum for ECMs in generic group 1 (E38,E40,E67,T42,T43)
// Inputs:  u16 ecm_type (defined for ecm_defs)
//          u32 offset (flash file offset)
//          bool ischeckonly (TRUE: check; FALSE: apply)
// Return:  u8  status
// Engineer: Rhonda Burns, Quyen Leba
// Note: require flash file already opened; use filestock_openflashfile(...)
//------------------------------------------------------------------------------
u8 obd2tune_gm_checksum_generic_group_1_Exx_OS_checksum(u16 ecm_type, u32 offset,
                                                        bool ischeckonly)
{
#define MAX_GROUP_1_EXX_OSCHECKSUM_BLOCK_INFO_COUNT             5
    struct
    {
        u32 address;
        u32 length;
    }group_1_oschecksum_block_info[MAX_GROUP_1_EXX_OSCHECKSUM_BLOCK_INFO_COUNT];
    u32 checksum_location;
    u32 CRC_location;
    u32 checksum_info_start_position;
    u32 checksum_part_count;
    u32 i,j;
    u8  databuffer[4096];
    u8  *bptr;
    u32 bytetoread;
    u32 bytecount;
    u32 tmp_u32;
    u16 checksum16calc, checksum16cmp;
    u8  crc16cmp[2];
    u16 crc16calc;
    u8  status;

    status = S_FAIL;
    //##########################################################################
    // Collect chechsum info
    //##########################################################################
    switch(ecm_type)
    {
    case 0x00:  //E40
        checksum_location = 0x1FE90;
        CRC_location = 0x1FEAE;
        checksum_info_start_position = 0x1FEB4;
        checksum_part_count = 2;
        break;
    case 0x01:  //E67
    case 0x04:  //E38
        checksum_location = 0x10000;
        CRC_location = 0x1001E;
        checksum_info_start_position = 0x10024;
        checksum_part_count = 1;
        break;
    case 0x0E:  //E37
        checksum_location = 0x0E000;
        CRC_location = 0x0E01E;
        checksum_info_start_position = 0x0E024;
        checksum_part_count = 1;
        break;
    case 0x14:  //E78
    case 0x15:  //E39-1
    case 0x16:  //E39-2
    case 0x1D:  //E39A
        checksum_location = 0xCB000;
        CRC_location = 0xCB020;
        checksum_info_start_position = 0xCB046;
        checksum_part_count = 2;
        break;
    case 0x18:  //E83A
        checksum_location = 0x10000;
        CRC_location = 0x10020;
        checksum_info_start_position = 0x10026;
        checksum_part_count = 2;
        break;
    case 0x19:  //E83B
        checksum_location = 0x10000;
        CRC_location = 0x10020;
        checksum_info_start_position = 0x10026;
        checksum_part_count = 3;
        break;
    case 0x1C:  //E92
        checksum_location = 0xC0100;
        CRC_location = 0xC0120;
        checksum_info_start_position = 0xC0126;
        checksum_part_count = 2;
        break;
    default:
        log_push_error_point(0x3053);
        return S_NOTREQUIRED;
    }

    checksum_location += offset;
    CRC_location += offset;
    checksum_info_start_position += offset;
    if (checksum_part_count > MAX_GROUP_1_EXX_OSCHECKSUM_BLOCK_INFO_COUNT)
    {
        log_push_error_point(0x3057);
        return S_ERROR;
    }

    status = filestock_read_at_position(CRC_location,
                                        crc16cmp, 2, &bytecount);
    if (status != S_SUCCESS || bytecount != 2)
    {
        log_push_error_point(0x3058);
        status = S_READFILE;
        goto obd2tune_gm_checksum_generic_group_1_Exx_OS_checksum;
    }

    status = filestock_read_at_position(checksum_info_start_position,
                                        databuffer, 512, &bytecount);
    if (status != S_SUCCESS || bytecount != 512)
    {
        log_push_error_point(0x3054);
        status = S_READFILE;
        goto obd2tune_gm_checksum_generic_group_1_Exx_OS_checksum;
    }

    bptr = databuffer;
    for(i=0;i<checksum_part_count;i++)
    {
        group_1_oschecksum_block_info[i].address =
            ((u32)bptr[0]<<24) + ((u32)bptr[1]<<16) + ((u32)bptr[2]<<8) + (u32)bptr[3];
        bptr += 4;
        //translate memory address to location in file
        group_1_oschecksum_block_info[i].address -= ECM_GetOSReadBlockAddr(ecm_type,0);
        group_1_oschecksum_block_info[i].address += offset;

        tmp_u32 =
            ((u32)bptr[0]<<24) + ((u32)bptr[1]<<16) + ((u32)bptr[2]<<8) + (u32)bptr[3];
        bptr += 4;
        tmp_u32 -= ECM_GetOSReadBlockAddr(ecm_type,0);
        tmp_u32 += offset;

        group_1_oschecksum_block_info[i].length = tmp_u32 - group_1_oschecksum_block_info[i].address + 1;
    }

    //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    // Calculate CRC
    //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    crc16calc = 0;
    for(i=0;i<checksum_part_count;i++)
    {
        bytetoread = 0;
        tmp_u32 = 0;
        while(tmp_u32 < group_1_oschecksum_block_info[i].length)
        {
            bytetoread = group_1_oschecksum_block_info[i].length - tmp_u32;
            if (bytetoread > sizeof(databuffer))
            {
                bytetoread = sizeof(databuffer);
            }

            status = filestock_read_at_position(group_1_oschecksum_block_info[i].address + tmp_u32,
                                                databuffer, bytetoread, &bytecount);
            if (status != S_SUCCESS || bytecount != bytetoread)
            {
                log_push_error_point(0x3055);
                status = S_READFILE;
                goto obd2tune_gm_checksum_generic_group_1_Exx_OS_checksum;
            }
            
            for(j=0;j<bytecount;j++)
            {
                if ((group_1_oschecksum_block_info[i].address+tmp_u32+j != (CRC_location)) &&
                    (group_1_oschecksum_block_info[i].address+tmp_u32+j != (CRC_location+1)) &&
                    (group_1_oschecksum_block_info[i].address+tmp_u32+j != (checksum_location)) &&
                    (group_1_oschecksum_block_info[i].address+tmp_u32+j != (checksum_location+1)))
                {
                    crc16calc = crc16_calculate(crc16calc,databuffer[j]);
                }
            }//for(j=...

            tmp_u32 += bytecount;
        }//while(tmp_u32 < ...
    }//for(i=0;i<checksum_part_count;i++)...

    //check crc result
    if ((crc16calc & 0xFF) != crc16cmp[0] || (crc16calc>>8) != crc16cmp[1])
    {
        if (ischeckonly)
        {
            log_push_error_point(0x3058);
            status = S_CRC32E;
            goto obd2tune_gm_checksum_generic_group_1_Exx_OS_checksum;
        }
        else
        {
            crc16cmp[0] = crc16calc & 0xFF;
            crc16cmp[1] = crc16calc >> 8;
            status = filestock_updateflashfile(CRC_location,
                                               crc16cmp, 2);
            if (status != S_SUCCESS)
            {
                log_push_error_point(0x3059);
                goto obd2tune_gm_checksum_generic_group_1_Exx_OS_checksum;
            }
        }
    }

    //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    // Calculate simple checksum
    //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    switch(ecm_type)
    {
    case 0x14:  //E78
    case 0x15:  //E39-1
    case 0x16:  //E39-2
    case 0x1D:  //E39A
        // E78, E39-1, E39-2, and E39A are a little different here
        group_1_oschecksum_block_info[1].address -= 2;
        group_1_oschecksum_block_info[1].length += 2;
        break;
    default:
        //do nothing
        break;
    }

    checksum16calc = 0;
    for(i=0;i<checksum_part_count;i++)
    {
        tmp_u32 = 0;
        while(tmp_u32 < group_1_oschecksum_block_info[i].length)
        {
            bytetoread = group_1_oschecksum_block_info[i].length - tmp_u32;
            if (bytetoread > sizeof(databuffer))
            {
                bytetoread = sizeof(databuffer);
            }

            status = filestock_read_at_position(group_1_oschecksum_block_info[i].address + tmp_u32,
                                                databuffer, bytetoread, &bytecount);
            if (status != S_SUCCESS || bytecount != bytetoread)
            {
                log_push_error_point(0x3056);
                status = S_READFILE;
                goto obd2tune_gm_checksum_generic_group_1_Exx_OS_checksum;
            }

            bptr = databuffer;
            for(j=0;j<bytecount;j+=2)
            {
                if ((group_1_oschecksum_block_info[i].address+tmp_u32+j) == (checksum_location))
                {
                    checksum16cmp = ((u16)bptr[j]<<8) | bptr[j+1];
                }
                else
                {
                    checksum16calc += ((u16)bptr[j]<<8) | bptr[j+1];
                }
            }//for(j=...

            tmp_u32 += bytetoread;
        }//while(tmp_u32 < ...
    }//for(i=0;i<checksum_part_count;i++)...
    checksum16calc = (u16)((checksum16calc ^ 0xFFFF) & 0xFFFF);
    checksum16calc++;

    //check simple checksum result
    if (checksum16calc != checksum16cmp)
    {
        if (ischeckonly)
        {
            log_push_error_point(0x305A);
            status = S_CRC32E;
            goto obd2tune_gm_checksum_generic_group_1_Exx_OS_checksum;
        }
        else
        {
            databuffer[0] = checksum16calc >> 8;
            databuffer[1] = checksum16calc & 0xFF;
            status = filestock_updateflashfile(checksum_location,
                                               databuffer, 2);
            if (status != S_SUCCESS)
            {
                log_push_error_point(0x305B);
                goto obd2tune_gm_checksum_generic_group_1_Exx_OS_checksum;
            }
        }
    }
    status = S_SUCCESS;

obd2tune_gm_checksum_generic_group_1_Exx_OS_checksum:
    return status;
}

//------------------------------------------------------------------------------
// Test ECU OS checksum/crc to determine if an OS reflash is required
// Input:   u16 ecm_type
// Return:  u8  status
// Engineer: Rhonda Burns, Quyen Leba
//------------------------------------------------------------------------------
u8 obd2tune_gm_ecu_test_os_checksum(u16 ecm_type)
{
    u32 ecm_testvalue_1;
    u32 ecm_testvalue_2;
    u32 file_testvalue_1;
    u32 file_testvalue_2;
    u32 current_file_position;
    u32 ecm_id;
    u16 test_method;
    u8  buffer[512];
    u32 buffer_length;
    u16 bytecount;
    u8  status;
    
    ecm_id = ECM_GetEcmId(ecm_type);
    test_method = ECM_GetOSTestMethod(ecm_type);
    
    if (test_method == 0)
    {
        return S_NOTREQUIRED;
    }

    filestock_get_position(&current_file_position);
    
    if (test_method == 1 || test_method == 2)
    {
        u32 start;

        if (test_method == 1)
        {
            //E40
            start = 0x1FE90;
        }
        else
        {
            //E38, E67
            start = 0x10000;
        }

        status = filestock_read_at_position
            (current_file_position + start, buffer,512,&buffer_length);
        if (status != S_SUCCESS || buffer_length != 512)
        {
            log_push_error_point(0x3025);
            return S_READFILE;
        }
        file_testvalue_1 = (buffer[0] << 8) | (buffer[1]);
        file_testvalue_2 = (buffer[0x1E] << 8) | (buffer[0x1F]);
        
        status = filestock_set_position(current_file_position);
        if (status != S_SUCCESS)
        {
            log_push_error_point(0x3026);
            return S_SEEKFILE;
        }
        
        status = obd2can_gm_read_memory_address
            (ecm_id,start,2,Address_24_Bits,buffer,&bytecount,FALSE);
        if (status != S_SUCCESS)
        {
            log_push_error_point(0x3027);
            return S_FAIL;
        }
        ecm_testvalue_1 = (buffer[3] << 8) | (buffer[4]);
        
        status = obd2can_gm_read_memory_address
            (ecm_id,start+0x1E,2,Address_24_Bits,buffer,&bytecount,FALSE);
        if (status != S_SUCCESS)
        {
            log_push_error_point(0x3028);
            return S_FAIL;
        }
        ecm_testvalue_2 = (buffer[3] << 8) | (buffer[4]);
        
        if ((ecm_testvalue_1 != file_testvalue_1) ||
            (ecm_testvalue_2 != file_testvalue_2))
        {
            log_push_error_point(0x3029);
            return S_UNMATCH;
        }
        else
        {
            return S_SUCCESS;
        }
    }
    else
    {
        //TODOQ: add other methods when supported
        log_push_error_point(0x302A);
        return S_NOTSUPPORT;
    }
}

//------------------------------------------------------------------------------
// Test file OS checksum/crc to determine if file is good
// Inputs:  const u8 *filename
//          u16 ecm_type
// Return:  u8  status
// Engineer: Rhonda Burns, Quyen Leba
//------------------------------------------------------------------------------
u8 obd2tune_gm_file_test_os_checksum(const u8 *filename, u16 ecm_type)
{
    return S_FAIL;
//    
//    switch(ecm_type)
//    {
//    case 0x00:  // E40
//        CK_location = 0x1FE90;
//        CRC_location = 0x1FEAE;
//        checksm_info = 0x1FEB4;
//        num_of_files = 2;
//        break;
//    case 0x01:  // E67
//    case 0x04:  // E38
//        CK_location = 0x10000;
//        CRC_location = 0x1001E;
//        checksm_info = 0x10024;
//        num_of_files = 1;
//        break;
//    case 0x0E:  // E37
//        CK_location = 0x0E000;
//        CRC_location = 0x0E01E;
//        checksm_info = 0x0E024;
//        num_of_files = 1;
//        break;
//    case 0x14:  // E78
//    case 0x15:  // E39
//        CK_location = 0xCB000;
//        CRC_location = 0xCB020;
//        checksm_info = 0xCB046;
//        num_of_files = 2;
//        break;
//    default:
//        __fclose(fileptr);
//        __free(G_RAM_Buffer);
//        return S_SUCCESS;
//    }
}

/**
 *  @brief Check if checksum is skipped by part number match
 *
 *  @param [in]     os_part_number  Part number to skip checksums on
 *
 *  @return Status  S_UNMATCH: Perform checksums, S_SUCCESS: Skip Checksums
 *
 *  @details    Some PCM's require the ability to program OS part numbers
 *              without valid checksums. This is because some PCMs have
 *              checksums disabled from the factory. This check also makes sure
 *              a full image reflash does not occur if the OS checksum does not
 *              match.
 */
u8 obd2tune_gm_checksum_skip_by_part_number(u8 *os_part_number)
{
    F_FILE *pFile;
    u8  linedata[512];
    u32 linedatalength;
    u8  status = S_SUCCESS;
    
    pFile = genfs_default_openfile(SKIPCHECKSUMLIST,"r");
    
    if (pFile == NULL)
    {
        /* File not found; this is okay, we'll never skip checksums when this
         * is the case.
         */
        log_push_error_point(0x302D);
        status = S_UNMATCH;
    }
    else
    {
        while(1)
        {
            status = file_readline(pFile,linedata,&linedatalength);
            if (status == S_END)
            {
                /* Checksums are not to be skipped */
                status = S_UNMATCH;
                break;
            }
            else if (status != S_SUCCESS)
            {
                log_push_error_point(0x3061);
                break;
            }
            
            if (strncmp((char*)os_part_number,(char*)linedata,strlen((char*)os_part_number)) == 0)
            {
                /* Checksums are skipped */
                status = S_SUCCESS;
                break;
            }
        }
        genfs_closefile(pFile);
    }
    
    return status;
}