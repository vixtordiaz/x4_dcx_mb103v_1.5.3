/**
  **************** (C) COPYRIGHT 2011 SCT Performance, LLC *********************
  * File Name          : obd2can_gm_datalog.c
  * Device / System    : SCT products using the "CommonCode" platform
  * Author             : Quyen Leba
  *
  * Version            : 1 
  * Date               : 05/02/2011
  * Description        : 
  *                    : 
  *
  *
  * History            : 05/02/2011 M. Davis
  *                    :   Add SCT Copyright header,
  *
  ******************************************************************************
  */

//------------------------------------------------------------------------------
// Log error point: 0x4900 -> 0x4AFF (shared with obd2datalog.c)
// Use 0x49A0 -> 0x49BF (shared with obd2datalog_gm.c)
//------------------------------------------------------------------------------

#include <string.h>
#include <board/rtc.h>
#include <board/delays.h>
#include <common/statuscode.h>
#include <common/obd2datalog.h>
#include <common/obd2can.h>
#include "obd2can_gm_datalog.h"

extern Datalog_Mode gDatalogmode;       //from obd2datalog.c
extern Datalog_Info *dataloginfo;       //from obd2datalog.c

//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
void obd2can_gm_datalog_sendtesterpresent()
{
    obd2can_gm_tester_present(TRUE,0);
}

//------------------------------------------------------------------------------
// Check if all signals fit in available packets
// Input:   VehicleCommLevel commlevel[ECM_MAX_COUNT]
// Return:  u8  status
// Engineer: Quyen Leba, Tristen Pierson
//------------------------------------------------------------------------------
u8 obd2can_gm_datalog_evaluatesignalsetup(VehicleCommLevel *vehiclecommlevel)
{
    u8  packetid;               // Data packet identifier
    u16 pid;                    // Dynamically definable PID
    u8  byteavailable[2][GM_GMLAN_CAN_PACKETID_START-GM_GMLAN_CAN_PACKETID_END];
    u8  processcheck[MAX_DATALOG_DLXBLOCK_COUNT];
    u8  ecm_index;
    u32 ecm_id[ECM_MAX_COUNT] = {Obd2CanEcuId_7E0,Obd2CanEcuId_7E2,0};
    u8  processedperloop;
    u8  index;
    u16 i;      
    u8  status;                 // Temp status
    u16 testPID[1] = {0x000C};  // $000C = Voltage
    bool packet;
    
    if (dataloginfo == NULL)
    {
        log_push_error_point(0x49A0);
        return S_BADCONTENT;
    }
        
    memset(processcheck,FALSE,sizeof(processcheck));
    memset((char*)byteavailable,GM_GMLAN_CAN_MAX_DATABYTE_PER_PACKET,sizeof(byteavailable));    
    
    dataloginfo->pidsignalcount = 0;
    dataloginfo->dmrsignalcount = 0;
    dataloginfo->analogsignalcount = 0;
    dataloginfo->oscsignalcount = 0;
    dataloginfo->packetidcount[0] = 0;
    dataloginfo->packetidcount[1] = 0;
        
    // Evaluate non-packet signals
    for(i=0;i<dataloginfo->datalogsignalcount;i++)
    {
        if ((dataloginfo->datalogsignals[i].Control & DATALOG_CONTROL_SKIP) == 0)
        {
            switch (dataloginfo->datalogsignals[i].PidType)
            {
                case PidTypeAnalog:
                    if (dataloginfo->analogsignalcount < dataloginfo->analogsignalmaxavailable)
                    {
                        dataloginfo->analogsignalindex[dataloginfo->analogsignalcount++] = i;
                    }
                   break;
                case PidTypeMode1:
                    dataloginfo->pidsignalcount++;
                    break;
                case PidTypeOSC:
                    dataloginfo->oscsignalcount++;
                    break;
            }
        }
        if (dataloginfo->pidsignalcount + dataloginfo->analogsignalcount + 
            dataloginfo->oscsignalcount >= dataloginfo->datalogsignalcount)
        {
            break;
        }
    }

    // Evaluate signals and build packets
    for(ecm_index=0;ecm_index<2;ecm_index++)
    {
        packetid = GM_GMLAN_CAN_PACKETID_START;
        pid = GM_GMLAN_CAN_PID_START;
        index = 0;
        packet = FALSE;
        
        while(packetid != GM_GMLAN_CAN_PACKETID_END && pid != GM_GMLAN_CAN_PID_END)
        {
            // Are all signals accounted for?
            if (dataloginfo->pidsignalcount + dataloginfo->dmrsignalcount + 
                dataloginfo->analogsignalcount + dataloginfo->oscsignalcount >= 
                dataloginfo->datalogsignalcount)
            { 
                break;
            }
            // Test for valid packet identifier         
            if (packet == TRUE)
            {
                if(obd2can_gm_dynamicallydefinemessage(ecm_id[ecm_index],packetid,testPID,1) != S_SUCCESS)
                {                
                    packetid--;     // Skip to next packet if invalid
                    continue;
                }
            }
            
            processedperloop = 0;
            packet = FALSE;
            
            for(i=0;i<dataloginfo->datalogsignalcount;i++)
            {                
                if (processcheck[i])
                {                    
                    continue;       // Already processed
                }                
                if ((dataloginfo->datalogsignals[i].Control & DATALOG_CONTROL_SKIP) == 0 &&
                    dataloginfo->datalogsignals[i].SignalType.Generic.PacketNumber == DATALOG_INVALID_PACKETID &&
                    dataloginfo->datalogsignals[i].EcmIndex == ecm_index &&
                    dataloginfo->datalogsignals[i].Size <= byteavailable[ecm_index][index] &&
                    (dataloginfo->datalogsignals[i].PidType == PidTypeRegular ||
                    dataloginfo->datalogsignals[i].PidType == PidTypeDMR))
                {
                    processcheck[i] = TRUE;
                    processedperloop++;
                    switch (dataloginfo->datalogsignals[i].PidType)
                    {
                        case PidTypeRegular:
                            dataloginfo->pidsignalcount++;                                   
                            break;
                        case PidTypeDMR:
                            // Dynamically define PID from DMA address
                            status = obd2can_gm_definePIDByAddress(ecm_id[ecm_index],
                                                                   pid,
                                                                   dataloginfo->datalogsignals[i].Address,
                                                                   dataloginfo->datalogsignals[i].Size);                              
                            
                            if (status != S_SUCCESS)
                            {
                                continue;
                            }
                            dataloginfo->datalogsignals[i].Address = pid++;
                            dataloginfo->dmrsignalindex[dataloginfo->dmrsignalcount++] = i;                                
                            break;
                    }
                    dataloginfo->datalogsignals[i].SignalType.Generic.Position = GM_GMLAN_CAN_MAX_DATABYTE_PER_PACKET - byteavailable[ecm_index][index];
                    byteavailable[ecm_index][index] -= dataloginfo->datalogsignals[i].Size;
                    dataloginfo->datalogsignals[i].SignalType.Generic.PacketNumber = packetid;
                    packet = TRUE;
                }//if(...) 
            }//for(i=...
            if (packet == TRUE)
            {                
                dataloginfo->packetidlist[ecm_index][dataloginfo->packetidcount[ecm_index]] = packetid;
                dataloginfo->packetidcount[ecm_index]++;
                index++;
                packetid--;
            }            
            if (processedperloop == 0)
            {
                break;
            }
        }//while(packetid...
        // Are all signals accounted for?
        if (dataloginfo->pidsignalcount + dataloginfo->dmrsignalcount + 
            dataloginfo->analogsignalcount + dataloginfo->oscsignalcount >= 
            dataloginfo->datalogsignalcount)
        { 
            break;
        }
    }//for(ecm_index=...
    // Evaluate left over signals if any
    if (dataloginfo->pidsignalcount + dataloginfo->dmrsignalcount + 
         dataloginfo->analogsignalcount + dataloginfo->oscsignalcount < 
         dataloginfo->datalogsignalcount)
    { 
        // Evaluate non-packet signals
        for(i=0;i<dataloginfo->datalogsignalcount;i++)
        {
            if ((dataloginfo->datalogsignals[i].Control & DATALOG_CONTROL_SKIP) == 0 &&
                dataloginfo->datalogsignals[i].SignalType.Generic.PacketNumber == DATALOG_INVALID_PACKETID)
            {
                switch (dataloginfo->datalogsignals[i].PidType)
                {
                    case PidTypeRegular:
                        dataloginfo->pidsignalcount++;                
                        break;
                    case PidTypeDMR:
                        dataloginfo->dmrsignalcount++;
                        break;
                }
            }        
            if (dataloginfo->pidsignalcount + dataloginfo->dmrsignalcount + 
                dataloginfo->analogsignalcount + dataloginfo->oscsignalcount >= 
                dataloginfo->datalogsignalcount)
            {
                break;
            }
        }
    }    
    if (dataloginfo->pidsignalcount + dataloginfo->dmrsignalcount + 
        dataloginfo->analogsignalcount + dataloginfo->oscsignalcount < 
        dataloginfo->datalogsignalcount)
    {
        // Cannot fit all signals
        log_push_error_point(0x49A1);
        return S_NOTFIT;
    }
    
    return S_SUCCESS;
}

//------------------------------------------------------------------------------
// Clean up previous defined packets
// Input:   VehicleCommLevel vehiclecommlevel[ECM_MAX_COUNT]
// Return:  u8  status
// Engineer: Quyen Leba
//------------------------------------------------------------------------------
u8 obd2can_gm_datalog_cleanup_packets(VehicleCommLevel *vehiclecommlevel)
{
    return S_SUCCESS;   //TODOQ4: check me
}

//------------------------------------------------------------------------------
// Initiate pids into packets
// Input:   VehicleCommLevel commlevel[ECM_MAX_COUNT]
// Return:  u8  status
// Engineer: Quyen Leba
//------------------------------------------------------------------------------
u8 obd2can_gm_datalog_initiatepackets(VehicleCommLevel *vehiclecommlevel)
{
    u8  ecm_index;
    u32 ecm_id[ECM_MAX_COUNT] = {Obd2CanEcuId_7E0,Obd2CanEcuId_7E2,0};
    u8  packetid;
    u16 i;
    u16 pid[GM_GMLAN_CAN_MAX_DATABYTE_PER_PACKET];
    u8  setup_pid_count;        // This counts successfully setup PIDs
    u8  position;               // Packet position for $2C
    u8  status;
    
    status = S_FAIL;

    if (dataloginfo == NULL)
    {
        log_push_error_point(0x49A2);
        return S_BADCONTENT;
    }
        
    setup_pid_count = 0;
        
    for(ecm_index=0;ecm_index<ECM_MAX_COUNT;ecm_index++)
    {
        if (vehiclecommlevel[ecm_index] == CommLevel_GMLAN)
        {
            for(packetid=GM_GMLAN_CAN_PACKETID_START;packetid>=GM_GMLAN_CAN_PACKETID_END;packetid--)
            {
                position = 0;
                for(i=0;i<dataloginfo->datalogsignalcount;i++)
                {                    
                    if (dataloginfo->datalogsignals[i].SignalType.Generic.PacketNumber == packetid &&
                        dataloginfo->datalogsignals[i].EcmIndex == ecm_index &&
                        (dataloginfo->datalogsignals[i].PidType == PidTypeRegular ||
                         dataloginfo->datalogsignals[i].PidType == PidTypeDMR))
                    {
                        pid[position] = (u16)dataloginfo->datalogsignals[i].Address; 
                        
                        status = obd2can_gm_dynamicallydefinemessage(
                                            ecm_id[ecm_index], packetid, 
                                            pid, position+1);
                        if (status == S_SUCCESS)
                        {
                             setup_pid_count++;
                             position++;
                        }
                        else
                        {
                            // Mark this signal invalid
                            dataloginfo->datalogsignals[i].SignalType.Generic.PacketNumber = 0xFF;
                        }
                    }
                } // for(i=1...
            } // for(packetid...
        }
        else if (vehiclecommlevel[ecm_index] == CommLevel_Unknown)
        {
            break;
        }
        else
        {
            log_push_error_point(0x49A4);
            return S_NOTSUPPORT;
        }
        if (setup_pid_count == 0)
            gDatalogmode.packetmode = SingleRate;
    } // for(ecm_index...

    // If no pids setup on PCM or TCM, fail; firmware will treat this as single rate
    // since it is now assumed that there are SAE PIDs
    if (gDatalogmode.packetmode == SingleRate)
    {
        return S_FAIL;
    }    
    return S_SUCCESS;
}

//------------------------------------------------------------------------------
// Get datalog data (rapid and/or single rate)
// Input:   VehicleCommLevel commlevel[ECM_MAX_COUNT]
// Return:  u8  status
// Engineer: Quyen Leba, Tristen Pierson
//------------------------------------------------------------------------------
u8 obd2can_gm_datalog_getdata(VehicleCommLevel *vehiclecommlevel)
{
    static u8 index;                // Retains index count (do not remove static qualifier)        
    static u8 packetread;           // Retains consecutive packets read (do not remove static qualifier)
    static u8 expected_ecm_index;   // Retains expected ecm index
    obd2can_rxinfo rxinfo;
    u8  status;        
    u32 response_ecm_id;
    u32 packet_filter[2] = {0x5E8, 0x5EA};
    u32 ecm_id[2] = {Obd2CanEcuId_7E0,Obd2CanEcuId_7E2};
    u8  ecm_index;
    u8  pidData[4];
    u8  data[DATALOG_MAX_PID_SIZE]; // For Packet PIDs
    u8  datalength;                 // For Packet PIDs
    u8  controlbytes[5];            // For OSC
    bool oscprocessed = FALSE;      // For OSC
    bool skip;                      // For Packet PIDs    
    u8  i;    
    
    // Need to detect ECM
    if (dataloginfo->control.ecm_stream)
    {
        if ((rtc_getvalue() - dataloginfo->control.ecm_laststreamdata) > DATALOG_MAX_DATAPOINT_INTERVAL)
        {
            return S_HALTED;
        }
    }
    else
    {
        packetread = 0;
    }
    
    // If only OSC session, update datapoint timestamp
    if (dataloginfo->oscsignalcount > 0 && dataloginfo->dmrsignalcount == 0 &&
        dataloginfo->pidsignalcount == 0)
    {
        dataloginfo->lastdatapoint_timestamp = rtc_getvalue();
    }
    
    status = S_SUCCESS;

    // Request non-packet data but only after reading a packet if available
    if ((packetread > 0 || dataloginfo->control.ecm_stream == 0) &&
        dataloginfo->pending == FALSE && dataloginfo->osc_pending == FALSE)
    {        
        // Get next non-packet for request
        do
        {
            // Enforce signal loop bound
            if (++index >= dataloginfo->datalogsignalcount)
            {
                index = 0;
            }
            
            // Only request non-packet data
            if (dataloginfo->datalogsignals[index].SignalType.Generic.PacketNumber == DATALOG_SKIPPED_PACKETID)
            {
                ecm_index = dataloginfo->datalogsignals[index].EcmIndex;
                if (ecm_index < 2)
                {
                    // Handle PIDs/DMRs
                    if (dataloginfo->datalogsignals[index].PidType == PidTypeRegular ||
                        dataloginfo->datalogsignals[index].PidType == PidTypeDMR ||
                        dataloginfo->datalogsignals[index].PidType == PidTypeMode1)
                    {
                        // Skip other bitmapped PIDs if covered by previous poll
                        if (dataloginfo->datalogsignals[index].Flags & DATALOG_PIDFLAGS_DATATYPE_BIT)
                        {
                            skip = FALSE;
                            for (i = 0; i < index; i++)
                            {
                                if (dataloginfo->datalogsignals[i].Flags & DATALOG_PIDFLAGS_DATATYPE_BIT &&
                                    dataloginfo->datalogsignals[i].Address == dataloginfo->datalogsignals[index].Address)
                                {
                                    skip = TRUE;
                                    break;
                                }
                            }
                            if (skip == TRUE)
                            {
                                continue;   // Next iteration
                            }
                        }
                        switch (dataloginfo->datalogsignals[index].PidType)
                        {
                            case PidTypeRegular:
                                status = obd2can_read_data_bypid(ecm_id[ecm_index],
                                            (u16)dataloginfo->datalogsignals[index].Address,
                                            NULL, NULL, FALSE);                    
                                break;
                            case PidTypeDMR:
                                status = obd2can_gm_read_data_bydma(ecm_id[ecm_index],
                                            dataloginfo->datalogsignals[index].Address,
                                            dataloginfo->datalogsignals[index].Size,
                                            gDatalogmode.addressType, NULL, NULL, FALSE);                    
                                break;
                            case PidTypeMode1:
                                // Handle packet OBD PIDs - single rate only
                                if (dataloginfo->datalogsignals[index].Flags & DATALOG_PIDFLAGS_IS_PACKET_PID)
                                {   
                                    // Skip other Packet PIDs if covered by previous Packet PID
                                    skip = FALSE;
                                    for (i = 0; i < index; i++)
                                    {
                                        if (dataloginfo->datalogsignals[i].Flags & DATALOG_PIDFLAGS_IS_PACKET_PID &&
                                            dataloginfo->datalogsignals[i].Address == dataloginfo->datalogsignals[index].Address)
                                        {
                                            skip = TRUE;
                                            break;
                                        }
                                    }
                                    if (skip == TRUE)
                                    {
                                        continue;   // Next iteration
                                    }
                                    
                                    status = obd2can_read_data_bypid_mode1(
                                               gDatalogmode.ecmBroadcast,ecm_id[ecm_index],
                                               (u8)dataloginfo->datalogsignals[index].Address,
                                               &data[0], &datalength, TRUE);
                                    
                                    if (status != S_SUCCESS)
                                    {
                                        break;
                                    }
                                    
                                    // Parse all PIDs used in packet
                                    skip = TRUE;
                                    for (i = 0; i < dataloginfo->datalogsignalcount; i++)
                                    {
                                        if (dataloginfo->datalogsignals[i].Flags & DATALOG_PIDFLAGS_IS_PACKET_PID &&
                                            dataloginfo->datalogsignals[i].Address == dataloginfo->datalogsignals[index].Address)
                                        {
                                            obd2_parse_packet_pid(&data[0], (u8)datalength, 
                                                dataloginfo->datalogsignals[i].packetStartPos,
                                                &pidData[0], 
                                                dataloginfo->datalogsignals[i].Size,
                                                (dataloginfo->datalogsignals[i].Flags & DATALOG_PIDFLAGS_IS_BITS ? 1 : 0));
                                            
                                            obd2datalog_parserdata(dataloginfo->datalogsignals[i].EcmIndex, 
                                                                   i, 0, &pidData[0]);
                                            
                                            skip = FALSE;
                                        }
                                    }
                                    if (skip == FALSE)
                                    {
                                        packetread = 0;
                                        dataloginfo->pending = FALSE;
                                        dataloginfo->lastsinglerate_timestamp = rtc_getvalue();
                                        dataloginfo->lastdatapoint_timestamp = rtc_getvalue();
                                        continue; // Next iteration
                                    }
                                }
                                else
                                {
                                    status = obd2can_read_data_bypid_mode1(
                                               gDatalogmode.ecmBroadcast,ecm_id[ecm_index],
                                               (u8)dataloginfo->datalogsignals[index].Address,
                                               NULL, NULL, FALSE);
                                }
                                break;
                        default:
                            break;
                        }
                        if (status == S_SUCCESS)
                        {
                            expected_ecm_index = ecm_index;
                            dataloginfo->lastsinglerate_timestamp = rtc_getvalue();
                            dataloginfo->pending = TRUE;
                            break;
                        }
                    } // Handle PIDs/DMRs
                    
                    // Handle OSC
                    if (dataloginfo->datalogsignals[index].PidType == PidTypeOSC)
                    {
                        // If OSC CPID was changed, send command
                        if (dataloginfo->datalogsignals[index].StructType == StructTypeOSC &&
                            (DatalogControl_GetOSC(dataloginfo->datalogsignals[index].Control) == OSC_CmdSend ||
                             DatalogControl_GetOSC(dataloginfo->datalogsignals[index].Control) == OSC_CmdReset))
                        {
                            // Initialize control bytes to 0x00's
                            memset(controlbytes,0x00,sizeof(controlbytes));                            
                            oscprocessed = FALSE;
                            // Parse and mask entire CPID group into control bytes                            
                            for (i = 0; i < dataloginfo->datalogsignalcount; i++)
                            {
                                // Check for items belonging CPID group
                                if (dataloginfo->datalogsignals[i].Address == dataloginfo->datalogsignals[index].Address)
                                {
                                    // If UI Group, unset other items
                                    if (index != i &&
                                        ((dataloginfo->datalogsignals[index].SignalType.OSC.UiPidGroup != 0 &&
                                        dataloginfo->datalogsignals[index].SignalType.OSC.UiPidGroup ==
                                        dataloginfo->datalogsignals[i].SignalType.OSC.UiPidGroup)))
                                    {
                                        DatalogControl_SetOSC(dataloginfo->datalogsignals[i].Control,OSC_CmdReset);
                                        dataloginfo->datalogsignals[i].Value = OSC_CmdReset;
                                        dataloginfo->datalogsignals[i].Control |= DATALOG_CONTROL_NEWDATA_SET;
                                    }
                                    // Mask active items
                                    if (DatalogControl_GetOSC(dataloginfo->datalogsignals[i].Control) == OSC_CmdSend ||
                                        DatalogControl_GetOSC(dataloginfo->datalogsignals[i].Control) == OSC_Pending ||
                                        DatalogControl_GetOSC(dataloginfo->datalogsignals[i].Control) == OSC_Success)
                                    {
                                        // Skip item if control byte position is invalid
                                        if (dataloginfo->datalogsignals[i].SignalType.OSC.ControlBytePosition < 1)
                                        {
                                            DatalogControl_SetOSC(dataloginfo->datalogsignals[i].Control,OSC_Fail);
                                            dataloginfo->datalogsignals[i].Value = OSC_Fail;
                                            dataloginfo->datalogsignals[i].Control |= DATALOG_CONTROL_NEWDATA_SET;
                                            continue;
                                        }
                                        // Mask control byte into position
                                        controlbytes[dataloginfo->datalogsignals[i].SignalType.OSC.ControlBytePosition-1] |=
                                            dataloginfo->datalogsignals[i].SignalType.OSC.ControlByteMask;
                                        // If data available, mask data byte to position
                                        if (dataloginfo->datalogsignals[i].Size > 0 &&
                                            dataloginfo->datalogsignals[i].SignalType.OSC.DataBytePosition > 0)
                                        {
                                            //Compose data                                            
                                            controlbytes[dataloginfo->datalogsignals[i].SignalType.OSC.DataBytePosition-1] |=
                                                (u8)obd2datalog_composedata(i, dataloginfo->datalogsignals[i].Data);
                                        }
                                    }
                                    else if (DatalogControl_GetOSC(dataloginfo->datalogsignals[i].Control) == OSC_CmdReset)
                                    {
                                        DatalogControl_SetOSC(dataloginfo->datalogsignals[i].Control,OSC_Default);
                                        dataloginfo->datalogsignals[i].Value = OSC_Default;
                                        dataloginfo->datalogsignals[i].Control |= DATALOG_CONTROL_NEWDATA_SET;
                                    }
                                    oscprocessed = TRUE;
                                }
                            } // for (i = 0...)
                            
                            if (oscprocessed == TRUE)
                            {
                                status = obd2can_gm_device_control(ecm_id[ecm_index], 
                                           dataloginfo->datalogsignals[index].Address,
                                           controlbytes,sizeof(controlbytes), FALSE);

                                if (status != S_SUCCESS)
                                {
                                    DatalogControl_SetOSC(dataloginfo->datalogsignals[index].Control,OSC_Fail);
                                    dataloginfo->datalogsignals[index].Value = OSC_Fail;
                                }
                                else
                                {
                                    switch (DatalogControl_GetOSC(dataloginfo->datalogsignals[index].Control))
                                    {                                    
                                        case OSC_CmdReset:
                                        case OSC_Default:
                                            dataloginfo->datalogsignals[index].Value = OSC_Default;                                          
                                            break;
                                        default:
                                            dataloginfo->datalogsignals[index].Value = OSC_Pending;
                                            break;
                                    }
                                    expected_ecm_index = ecm_index;
                                    DatalogControl_SetOSC(dataloginfo->datalogsignals[index].Control,OSC_Pending);
                                    dataloginfo->lastosc_timestamp = rtc_getvalue();
                                    dataloginfo->lastsinglerate_timestamp = rtc_getvalue();
                                    dataloginfo->osc_pending = TRUE;
                                }
                                dataloginfo->datalogsignals[index].Control |= DATALOG_CONTROL_NEWDATA_SET;
                                break;
                            } // if (oscprocessed == TRUE)
                        } // If OSC CPID was changed, send command
                    } // Handle OSC
                } // if (ecm_index < 2)                
            } // Request non-packet data
        } while (index > 0); // do
    } // request non-packet data
    
    // Check for data
    status = S_FAIL;
    if (gDatalogmode.packetmode == RapidRate || dataloginfo->pending || dataloginfo->osc_pending)
    {
        obd2can_rxinfo_init(&rxinfo);
        status = obd2can_rxraw_complete(&response_ecm_id,&rxinfo,packet_filter,sizeof(packet_filter));
    }
  
    // Process data
    if (status == S_SUCCESS)
    {        
        switch (response_ecm_id)
        {
        case 0x5E8:     // Handle PCM Packets            
            obd2datalog_parserapidpacketdata(0, rxinfo.rxbuffer[0], &rxinfo.rxbuffer[1]);
            packetread = 1;
            dataloginfo->control.ecm_stream = 1;
            dataloginfo->control.ecm_laststreamdata = rtc_getvalue();
            dataloginfo->lastdatapoint_timestamp = rtc_getvalue();
            break;
        case 0x5EA:     // Handle TCM Packets            
            obd2datalog_parserapidpacketdata(1, rxinfo.rxbuffer[0], &rxinfo.rxbuffer[1]);
            packetread = 1;
            dataloginfo->control.tcm_stream = 1;
            dataloginfo->control.tcm_laststreamdata = rtc_getvalue();            
            dataloginfo->lastdatapoint_timestamp = rtc_getvalue();
            break;
        case 0x7EA:     // Handle TCM Non-packets
        case 0x7E8:     // Handle PCM Non-packets
            ecm_index = 0;                      
            if (response_ecm_id == 0x7EA)
            {
                ecm_index = 1;
            }
            // Only accept expected ecm addresses
            if (ecm_index != expected_ecm_index)
            {
                break;
            }
            memset(&pidData, 0, sizeof(pidData));
            // Process non-packet data
            switch (rxinfo.cmd)
            {
            case 0x01:  // Handle SAE PID
                if (rxinfo.rxlength > 1)
                {
                    memcpy(&pidData,&rxinfo.rxbuffer[1],rxinfo.rxlength-1);
                }
                break;
            case 0x22:  // Handle OEM PID
                if (rxinfo.rxlength > 2)
                {
                    memcpy(&pidData,&rxinfo.rxbuffer[2],rxinfo.rxlength-2);
                }
                break;
            case 0x23:  // Handle DMA
                if (gDatalogmode.addressType == Address_32_Bits &&
                    rxinfo.rxlength > 4)
                {
                    memcpy(&pidData,&rxinfo.rxbuffer[4],rxinfo.rxlength-4);
                }
                else if (gDatalogmode.addressType == Address_24_Bits &&
                    rxinfo.rxlength > 3)
                {
                    memcpy(&pidData,&rxinfo.rxbuffer[3],rxinfo.rxlength-3);
                }
                break;
            case 0xAE:  // Handle OSC Response
                // Do nothing here
                break;
            default:
                status = S_UNMATCH; 
                break;
            }                    
            // Process CAN Data
            if (status == S_SUCCESS)
            {
                switch (rxinfo.cmd)
                {
                case 0x01:  // Process SAE PID
                case 0x22:  // Process OEM PID
                case 0x23:  // Process DMA
                    // Parse all bitmapped data within PID
                    if (dataloginfo->datalogsignals[index].Flags & DATALOG_PIDFLAGS_DATATYPE_BIT)
                    {
                        for (i = 0; i < dataloginfo->datalogsignalcount; i++)
                        {
                            if (dataloginfo->datalogsignals[i].Flags & DATALOG_PIDFLAGS_DATATYPE_BIT &&
                                dataloginfo->datalogsignals[i].Address == dataloginfo->datalogsignals[index].Address)
                            {                                
                                obd2datalog_parserdata(dataloginfo->datalogsignals[i].EcmIndex, 
                                                       i, 0, &pidData[0]);
                            }
                        }
                    }
                    // Or parse single PID data
                    else
                    {
                        obd2datalog_parserdata(ecm_index, index, 0, pidData);
                    }
                    packetread = 0;
                    dataloginfo->pending = FALSE;
                    dataloginfo->lastsinglerate_timestamp = rtc_getvalue();
                    dataloginfo->lastdatapoint_timestamp = rtc_getvalue();
                    break;
                case 0xAE:  // Process OSC Response
                    // OSC_Default status holds true
                    if (dataloginfo->datalogsignals[index].Value != OSC_Default)
                    {
                        dataloginfo->datalogsignals[index].Value = OSC_Success;
                        DatalogControl_SetOSC(dataloginfo->datalogsignals[index].Control,OSC_Success);
                    }
                    packetread = 0;
                    dataloginfo->lastosc_timestamp = rtc_getvalue();
                    dataloginfo->datalogsignals[index].Control &= ~DATALOG_CONTROL_NEWDATA_SET;
                    break;
                }
            }
            break;
        } // switch(..)
    } // if (status == S_SUCCESS)
    // Process Negative Responses
    else if (status == S_ERROR)
    {
        switch (rxinfo.cmd)
        {
        case 0xAE:  // Process OSC Negative Response
            // Device Control Limits Exceeded            
            if (rxinfo.errorcode == 0xE3)
            {
                dataloginfo->datalogsignals[index].Value = OSC_RangeErr;
                DatalogControl_SetOSC(dataloginfo->datalogsignals[index].Control,OSC_RangeErr);
            }
            // Anything else
            else
            {
                dataloginfo->datalogsignals[index].Value = OSC_Fail;
                DatalogControl_SetOSC(dataloginfo->datalogsignals[index].Control,OSC_Fail);
            }
            packetread = 0;
            dataloginfo->osc_pending = FALSE;
            dataloginfo->lastosc_timestamp = rtc_getvalue();
            dataloginfo->datalogsignals[index].Control |= DATALOG_CONTROL_NEWDATA_SET;
            break;
        default:
            // Other negative responses are not handled
            break;
        }
    }
    
    // Check for single rate response timeout
    if (dataloginfo->pending == TRUE && 
        rtc_getvalue() - dataloginfo->lastsinglerate_timestamp >= DATALOG_SINGLERATE_TIMEOUT)
    {
        // Assume single rate PID response is lost
        dataloginfo->pending = FALSE;
    }
    
    // Check for OSC response timeout
    if (dataloginfo->osc_pending == TRUE && 
        rtc_getvalue() - dataloginfo->lastosc_timestamp >= DATALOG_OSC_TIMEOUT)
    {
        // Assume OSC PID response is lost
        dataloginfo->osc_pending = FALSE;
        dataloginfo->datalogsignals[index].Value = OSC_Timeout;
        DatalogControl_SetOSC(dataloginfo->datalogsignals[index].Control,OSC_Timeout);
        dataloginfo->datalogsignals[index].Control |= DATALOG_CONTROL_NEWDATA_SET;
    }    
    // Check if OSC response wait time exceeded and update OSC record
    else if (dataloginfo->osc_pending == TRUE &&
             rtc_getvalue() - dataloginfo->lastosc_timestamp >= DATALOG_WAIT_OSC_INTERVAL)
    {
        dataloginfo->osc_pending = FALSE;
        dataloginfo->datalogsignals[index].Control |= DATALOG_CONTROL_NEWDATA_SET;
    }        
        
    // Data not received, session is probably dead
    if (rtc_getvalue() - dataloginfo->lastdatapoint_timestamp >= DATALOG_MAX_DATAPOINT_INTERVAL)
    {
        return S_COMMLOST;
    }
    
    return S_SUCCESS;
}
       
