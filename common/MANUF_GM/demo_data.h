/**
  **************** (C) COPYRIGHT 2011 SCT Performance, LLC *********************
  * File Name          : demo_data.h
  * Device / System    : SCT products using the "CommonCode" platform
  * Author             : Quyen Leba
  *
  * Version            : 1 
  * Date               : 05/02/2011
  * Description        : 
  *                    : 
  *
  *
  * History            : 05/02/2011 M. Davis
  *                    :   Add SCT Copyright header,
  *
  ******************************************************************************
  */

#ifndef __DEMO_DATA_H
#define __DEMO_DATA_H

#include <common/devicedef.h>
#include <common/obd2def.h>

typedef struct
{
    u8  upload_dummy_delay;     //in ms
    u8  download_dummy_delay;   //in ms
}DemoControl;

typedef struct
{
    u32 ecminfo_crc32e;
    ecm_info ecm;
}DemoVehicle;

typedef struct
{
    u8  count;
    u16 codes[3];
}DemoDTC;

typedef struct
{
    u8  count;
    struct
    {
        u32 address;
        float lo;
        float hi;
#define DUMMYVALUETYPE_RANDOM           0
#define DUMMYVALUETYPE_SWEEP            1
        u8  dummyvaluetype;
        float dummyattrvalue;
    }items[12];
}DemoDatalog;

//------------------------------------------------------------------------------
// Demo mode data
//------------------------------------------------------------------------------
static const DemoVehicle demo_vehicle =
{
    .ecminfo_crc32e = 0,
    .ecm =
    {
        .ecm_count = 1,
        .codecount = {0x0A,0,0},
        .codeids[0] = {0xC1,0xCB,0xC0,0xC2,0xC3,0xC4,0xC5,0xC6,0xC7,0xC8},
        .codeids[1] = {0,0,0,0,0,0,0,0,0,0},
        .codeids[2] = {0,0,0,0,0,0,0,0,0,0},
        .codes[0] =     //E38 Demo
        {
            [0] = "12617631",
            [1] = "1129855025",
            [2] = "12605900",
            [3] = "12608463",
            [4] = "12624797",
            [5] = "12617890",
            [6] = "12617883",
            [7] = "12617879",
            [8] = "N/A",
            [9] = "N/A",
        },
        .second_codes[0][0][0] = 0,
        .vehicle_serial_number = "16K17302QC0P",
        .hwid[0] = 0,
        .bcc = "5069",  //4 char max
        .vin = "SCT_E38_837898218",
        .commtype = {CommType_CAN,CommType_Unknown,CommType_Unknown},
        .commlevel = {CommLevel_GMLAN,CommLevel_Unknown,CommLevel_Unknown},
    }
};

////static const DemoVehicle demo_vehicle =
////{
////    .ecminfo_crc32e = 0,
////    .ecm =
////    {
////        .ecm_count = 1,
////        .codecount = {0x0A,0,0},
////        .codeids[0] = {0xC1,0xCB,0xC0,0xC2,0xC3,0xC4,0xC5,0xC6,0xC7,0xC8},
////        .codeids[1] = {0,0,0,0,0,0,0,0,0,0},
////        .codeids[2] = {0,0,0,0,0,0,0,0,0,0},
////        .codes[0] =     //E38 Demo
////        {
////            [0] = "12637084",
////            [1] = "12635862",
////            [2] = "12605900",
////            [3] = "12635479",
////            [4] = "12635029",
////            [5] = "12634071",
////            [6] = "12635475",
////            [7] = "12637085",
////            [8] = "N/A",
////            [9] = "N/A",
////        },
////        .second_codes[0][0][0] = 0,
////        .vehicle_serial_number = "86K19146GTXV",
////        .hwid[0] = 0,
////        .bcc = "AAKC",  //4 char max
////        .vin = "2G1FT1EW5A9121515",
////        .commtype = {CommType_CAN,CommType_Unknown,CommType_Unknown},
////        .commlevel = {CommLevel_GMLAN,CommLevel_Unknown,CommLevel_Unknown},
////    }
////};

static const DemoControl demo_control =
{
    .upload_dummy_delay = 1,
    .download_dummy_delay = 1,
};

static const DemoDTC demo_dtc = 
{
    .count = 3,
    .codes = {0x0100, 0x0108, 0x0110},
};

static const DemoDatalog demo_datalog =
{
    .count = 5,
    //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    // GM Gas
    //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    .items[0] =
    {
        .address = 0x000F,      //iat
        .lo = 90,       .hi = 120,
        .dummyvaluetype = DUMMYVALUETYPE_RANDOM,        .dummyattrvalue = 0,
    },
    .items[1] =
    {
        .address = 0x0005,      //ect
        .lo = 200,      .hi = 230,
        .dummyvaluetype = DUMMYVALUETYPE_RANDOM,        .dummyattrvalue = 0,
    },
    .items[2] =
    {
        .address = 0x000C,      //rpm
        .lo = 2000,     .hi = 6000,
        .dummyvaluetype = DUMMYVALUETYPE_SWEEP,         .dummyattrvalue = 50,
    },
    .items[3] =
    {
        .address = 0x000B,      //map
        .lo = 70,       .hi = 90,
        .dummyvaluetype = DUMMYVALUETYPE_RANDOM,        .dummyattrvalue = 0,
    },
    .items[4] =
    {
        .address = 0x000E,      //spark advance
        .lo = 20,       .hi = 22,
        .dummyvaluetype = DUMMYVALUETYPE_RANDOM,        .dummyattrvalue = 0,
    },
};

#endif    //__DEMO_DATA_H
