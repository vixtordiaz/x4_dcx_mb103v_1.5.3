/**
  **************** (C) COPYRIGHT 2011 SCT Performance, LLC *********************
  * File Name          : gm_veh_defs.c
  * Device / System    : SCT products using the "CommonCode" platform
  * Author             : Quyen Leba
  *
  * Version            : 1 
  * Date               : 05/02/2011
  * Description        : 
  *                    : 
  *
  *
  * History            : 05/02/2011 M. Davis
  *                    :   Add SCT Copyright header,
  *
  ******************************************************************************
  */

#include <common/obd2.h>
#include <common/ecm_defs.h>
#include "gm_veh_defs.h"
#include <common/statuscode.h>

//------------------------------------------------------------------------------
// Get # of ecms defined by vehicle type
// Input:   u16 veh_type (defined for veh_defs)
// Return:  u8  count
//------------------------------------------------------------------------------
u8 gm_veh_get_ecmcount(u16 veh_type)
{
    u8 i;
    u8 count;

    //TODOQ: check veh_type first
    
    for(i=0,count=0;i<ECM_MAX_COUNT;i++)
    {
        if (VEH_GetEcmType(veh_type,i) != INVALID_ECM_DEF)
        {
            count++;
        }
    }
    return count;
}

//------------------------------------------------------------------------------
// Check that the vehicle type exists
// Input:   u16 veh_type (defined for veh_defs)
// Return:  u8 status
//------------------------------------------------------------------------------
u8 gm_veh_validate_vehicletype(u16 veh_type)
{
    u8 i;
    u16 index;

    for(i=0;i<VEH_DEFS_COUNT;i++)
    {
        index = VEH_GetIndex(veh_type);
        if ((index+GM_VEH_DEF_START) == veh_type)
        {
            return S_SUCCESS;
        }
    }
    return S_FAIL;
}

//------------------------------------------------------------------------------
// Indexes as defined in gm_ecm_defs.c
//------------------------------------------------------------------------------
enum
{
    ECM_DEF_INDEX__E40                      = 0x00,
    ECM_DEF_INDEX__E67                      = 0x01,
    ECM_DEF_INDEX__LBZ                      = 0x02,
    ECM_DEF_INDEX__LMM                      = 0x03,
    ECM_DEF_INDEX__E38                      = 0x04,
    ECM_DEF_INDEX__LB7                      = 0x05,
    ECM_DEF_INDEX__MEG                      = 0x06,
    ECM_DEF_INDEX__HMEG                     = 0x07,
    ECM_DEF_INDEX__LLY                      = 0x08,
    ECM_DEF_INDEX__T42                      = 0x0A,
    ECM_DEF_INDEX__T43                      = 0x0B,
    ECM_DEF_INDEX__ALLISON                  = 0x0C,
    ECM_DEF_INDEX__T43A                     = 0x0D,
    ECM_DEF_INDEX__E37                      = 0x0E,
    ECM_DEF_INDEX__V6HMEG                   = 0x0F,     // NOT SUPPORTED YET
    ECM_DEF_INDEX__P121MEG                  = 0x10,     // NOT SUPPORTED YET
    ECM_DEF_INDEX__P122MEG                  = 0x11,     // NOT SUPPORTED YET
    ECM_DEF_INDEX__ALLI5SPD                 = 0x12,     // NOT SUPPORTED YET
    ECM_DEF_INDEX__A4CYL9704                = 0x13,     // NOT SUPPORTED YET
    ECM_DEF_INDEX__E78                      = 0x14,
    ECM_DEF_INDEX__E39_1                    = 0x15,
    ECM_DEF_INDEX__E39_2                    = 0x16,
    ECM_DEF_INDEX__E86                      = 0x17,     // NOT SUPPORTED YET
    ECM_DEF_INDEX__E83A                     = 0x18,     // NOT SUPPORTED YET
    ECM_DEF_INDEX__E83B                     = 0x19,     // NOT SUPPORTED YET
    ECM_DEF_INDEX__E69                      = 0x1A,     // NOT SUPPORTED YET
    ECM_DEF_INDEX__T76                      = 0x1B,
    ECM_DEF_INDEX__E92                      = 0x1C,
    ECM_DEF_INDEX__E39A                     = 0x1D,    
};

//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
const VEH_Def gm_veh_defs[VEH_DEFS_COUNT] =
{
    //[0]: UNUSED
    [1] =   // E40 only - CAN                                           //0x01 -> 111 (0x6F)
    {
        .index = 1,
        .flags = SKIP_RECYCLE_KEY_ON,
        .ecm_types = {ECM_DEF_INDEX__E40, INVALID_ECM_DEF, INVALID_ECM_DEF},
        .templOSfilename = NULL,
    },
    [2] =   // E67 only - CAN                                           //0x02 -> 112 (0x70)
    {
        .index = 2,
        .flags = SKIP_RECYCLE_KEY_ON,
        .ecm_types = {ECM_DEF_INDEX__E67, INVALID_ECM_DEF, INVALID_ECM_DEF},
        .templOSfilename = NULL,
    },
    [3] =   // LBZ only - CAN                                           //0x03 -> 113 (0x71)
    {
        .index = 3,
        .flags = ALLOW_SKIP_UNSUPPORTED_SUB_ECM | SKIP_RECYCLE_KEY_ON,
        .ecm_types = {ECM_DEF_INDEX__LBZ, INVALID_ECM_DEF, INVALID_ECM_DEF},
        .templOSfilename = "LBZtmplOS.bin",
    },
    [4] =   // LMM only - CAN                                           //0x04 -> 114 (0x72)
    {
        .index = 4,
        .flags = ALLOW_SKIP_UNSUPPORTED_SUB_ECM | SKIP_RECYCLE_KEY_ON,
        .ecm_types = {ECM_DEF_INDEX__LMM, INVALID_ECM_DEF, INVALID_ECM_DEF},
        .templOSfilename = "LMMtmplOS.bin",
    },
    [5] =   // LB7 only - VPW                                           //0x05 -> 115 (0x73)
    {
        .index = 5,
        .flags = SKIP_RECYCLE_KEY_ON,
        .ecm_types = {ECM_DEF_INDEX__LBZ, INVALID_ECM_DEF, INVALID_ECM_DEF},
        .templOSfilename = NULL,
    },
    [6] =   // LLY only - VPW                                           //0x06 -> 116 (0x74)
    {
        .index = 6,
        .flags = SKIP_RECYCLE_KEY_ON,
        .ecm_types = {ECM_DEF_INDEX__LLY, INVALID_ECM_DEF, INVALID_ECM_DEF},
        .templOSfilename = NULL,
    },
    [7] =   // 1 Meg only - VPW                                         //0x07 -> 117 (0x75)
    {
        .index = 7,
        .flags = SKIP_RECYCLE_KEY_ON,
        .ecm_types = {ECM_DEF_INDEX__MEG, INVALID_ECM_DEF, INVALID_ECM_DEF},
        .templOSfilename = NULL,
    },
    [8] =   // 1/2 Meg only - VPW                                       //0x08 -> 118 (0x76)
    {
        .index = 8,
        .flags = SKIP_RECYCLE_KEY_ON,
        .ecm_types = {ECM_DEF_INDEX__HMEG, INVALID_ECM_DEF, INVALID_ECM_DEF},
        .templOSfilename = NULL,
    },
    [9] =   // E38 only - CAN                                           //0x09 -> 119 (0x77)
    {
        .index = 9,
        .flags = SKIP_RECYCLE_KEY_ON,
        .ecm_types = {ECM_DEF_INDEX__E38, INVALID_ECM_DEF, INVALID_ECM_DEF},
        .templOSfilename = NULL,
    },
    [10] =  // ME7XX (768k) only - CAN                                  //0x0A      //UNUSED
    {
        .index = 10,
        .flags = SKIP_RECYCLE_KEY_ON,
        .ecm_types = {INVALID_ECM_DEF, INVALID_ECM_DEF, INVALID_ECM_DEF},
        .templOSfilename = NULL,
    },
    [11] =  // T42 Trans only - CAN                                     //0x0B -> 121 (0x79)
    {
        .index = 11,
        .flags = SKIP_RECYCLE_KEY_ON,
        .ecm_types = {ECM_DEF_INDEX__T42, INVALID_ECM_DEF, INVALID_ECM_DEF},
        .templOSfilename = NULL,
    },
    [12] =  // T43 Trans only - CAN                                     //0x0C -> 122 (0x7A)
    {
        .index = 12,
        .flags = SKIP_RECYCLE_KEY_ON,
        .ecm_types = {ECM_DEF_INDEX__T43, INVALID_ECM_DEF, INVALID_ECM_DEF},
        .templOSfilename = NULL,
    },
    [13] =  // ALLISON Trans only - CAN                                 //0x0D -> 123 (0x7B)
    {
        .index = 13,
        .flags = SKIP_RECYCLE_KEY_ON,
        .ecm_types = {ECM_DEF_INDEX__ALLISON, INVALID_ECM_DEF, INVALID_ECM_DEF},
        .templOSfilename = NULL,
    },
    [14] =  // E67 - CAN & T42 - CAN                                    //0x0E -> 124 (0x7C)
    {
        .index = 14,
        .flags = SKIP_RECYCLE_KEY_ON,
        .ecm_types = {ECM_DEF_INDEX__E67, ECM_DEF_INDEX__T42, INVALID_ECM_DEF},
        .templOSfilename = NULL,
    },
    [15] =  // E38 - CAN & T42 - CAN                                    //0x0F -> 125 (0x7D)
    {
        .index = 15,
        .flags = SKIP_RECYCLE_KEY_ON,
        .ecm_types = {ECM_DEF_INDEX__E38, ECM_DEF_INDEX__T42, INVALID_ECM_DEF},
        .templOSfilename = NULL,
    },
    [16] =  // E40 - CAN & T42 - CAN                                    //0x10 -> 126 (0x7E)
    {
        .index = 16,
        .flags = SKIP_RECYCLE_KEY_ON,
        .ecm_types = {ECM_DEF_INDEX__E40, ECM_DEF_INDEX__T42, INVALID_ECM_DEF},
        .templOSfilename = NULL,
    },
    [17] =  // E38 - CAN & T43 - CAN                                    //0x11 -> 127 (0x7F)
    {
        .index = 17,
        .flags = SKIP_RECYCLE_KEY_ON,
        .ecm_types = {ECM_DEF_INDEX__E38, ECM_DEF_INDEX__T43, INVALID_ECM_DEF},
        .templOSfilename = NULL,
    },
    [18] =  // E67 - CAN & T43 - CAN                                    //0x12 -> 128 (0x80)
    {
        .index = 18,
        .flags = SKIP_RECYCLE_KEY_ON,
        .ecm_types = {ECM_DEF_INDEX__E67, ECM_DEF_INDEX__T43, INVALID_ECM_DEF},
        .templOSfilename = NULL,
    },
    [19] =  // LBZ - CAN & ALLISON - CAN                                //0x13 -> 129 (0x81)
    {
        .index = 19,
        .flags = SKIP_RECYCLE_KEY_ON,
        .ecm_types = {ECM_DEF_INDEX__LBZ, ECM_DEF_INDEX__ALLISON, INVALID_ECM_DEF},
        .templOSfilename = NULL,
    },
    [20] =  // LMM - CAN & ALLISON - CAN                                //0x14 -> 130 (0x82)
    {
        .index = 20,
        .flags = SKIP_RECYCLE_KEY_ON,
        .ecm_types = {ECM_DEF_INDEX__LMM, ECM_DEF_INDEX__ALLISON, INVALID_ECM_DEF},
        .templOSfilename = NULL,
    },
    [21] =  // LBZ - CAN & T42 - CAN                                    //0x15 -> 131 (0x83)
    {
        .index = 21,
        .flags = SKIP_RECYCLE_KEY_ON,
        .ecm_types = {ECM_DEF_INDEX__LBZ, ECM_DEF_INDEX__T42, INVALID_ECM_DEF},
        .templOSfilename = NULL,
    },
    [22] =  // LMM - CAN & T42 - CAN                                    //0x16 -> 132 (0x84)
    {
        .index = 22,
        .flags = SKIP_RECYCLE_KEY_ON,
        .ecm_types = {ECM_DEF_INDEX__LMM, ECM_DEF_INDEX__T42, INVALID_ECM_DEF},
        .templOSfilename = NULL,
    },
    [23] =  // T43A Trans only - CAN                                    //0x17 -> 133 (0x85)
    {
        .index = 23,
        .flags = SKIP_RECYCLE_KEY_ON,
        .ecm_types = {ECM_DEF_INDEX__T43A, INVALID_ECM_DEF, INVALID_ECM_DEF},
        .templOSfilename = NULL,
    },
    [24] =  // E38 - CAN & T43A - CAN                                   //0x18 -> 134 (0x86)
    {
        .index = 24,
        .flags = SKIP_RECYCLE_KEY_ON,
        .ecm_types = {ECM_DEF_INDEX__E38, ECM_DEF_INDEX__T43A, INVALID_ECM_DEF},
        .templOSfilename = NULL,
    },
    [25] =  // E67 - CAN & T43A - CAN                                   //0x19 -> 135 (0x87)
    {
        .index = 25,
        .flags = SKIP_RECYCLE_KEY_ON,
        .ecm_types = {ECM_DEF_INDEX__E67, ECM_DEF_INDEX__T43A, INVALID_ECM_DEF},
        .templOSfilename = NULL,
    },
    [26] =  // E37 Hitachi - CAN                                        //0x1A -> 136 (0x88)
    {
        .index = 26,
        .flags = SKIP_RECYCLE_KEY_ON,
        .ecm_types = {ECM_DEF_INDEX__E37, INVALID_ECM_DEF, INVALID_ECM_DEF},
        .templOSfilename = NULL,
    },
//    [27] =  // 1/2Meg V6 - VPW                                          //0x1B -> 137 (0x89)
//    {
//        .index = 27,
//        .flags = SKIP_RECYCLE_KEY_ON,
//        .ecm_types = {ECM_DEF_INDEX__V6HMEG, INVALID_ECM_DEF, INVALID_ECM_DEF},
//        .templOSfilename = NULL,
//    },
//    [28] =  // 1Meg P12 - VPW                                           //0x1C -> 138 (0x8A)
//    {
//        .index = 28,
//        .flags = SKIP_RECYCLE_KEY_ON,
//        .ecm_types = {ECM_DEF_INDEX__P121MEG, INVALID_ECM_DEF, INVALID_ECM_DEF},
//        .templOSfilename = NULL,
//    },
//    [29] =  // 2Meg V6 - VPW                                            //0x1D -> 139 (0x8B)
//    {
//        .index = 29,
//        .flags = SKIP_RECYCLE_KEY_ON,
//        .ecm_types = {ECM_DEF_INDEX__P122MEG, INVALID_ECM_DEF, INVALID_ECM_DEF},
//        .templOSfilename = NULL,
//    },
//    [30] =  // Allison 5 Speed TCM - VPW                                //0x1E -> 140 (0x8C)
//    {
//        .index = 30,
//        .flags = SKIP_RECYCLE_KEY_ON,
//        .ecm_types = {ECM_DEF_INDEX__ALLI5SPD, INVALID_ECM_DEF, INVALID_ECM_DEF},
//        .templOSfilename = NULL,
//    },
//    [31] =  // 97-04 4 Cylinder A - VPW                                 //0x1F -> 141 (0x8D)
//    {
//        .index = 31,
//        .flags = SKIP_RECYCLE_KEY_ON,
//        .ecm_types = {ECM_DEF_INDEX__A4CYL9704, INVALID_ECM_DEF, INVALID_ECM_DEF},
//        .templOSfilename = NULL,
//    },
    [32] =  // E37 Hitachi - CAN & T42 - CAN                            //0x20 -> 142 (0x8E)
    {
        .index = 32,
        .flags = SKIP_RECYCLE_KEY_ON,
        .ecm_types = {ECM_DEF_INDEX__E37, ECM_DEF_INDEX__T42, INVALID_ECM_DEF},
        .templOSfilename = NULL,
    },
    [33] =  // T76 Only - CAN                                           //0x21 -> 143 (0x8F)
    {
        .index = 33,
        .flags = SKIP_RECYCLE_KEY_ON,
        .ecm_types = {ECM_DEF_INDEX__T76, INVALID_ECM_DEF, INVALID_ECM_DEF},
        .templOSfilename = NULL,
    },
    [34] =  // E92 Only - CAN                                           //0x22 -> 144 (0x90)
    {
        .index = 34,
        .flags = SKIP_RECYCLE_KEY_ON,
        .ecm_types = {ECM_DEF_INDEX__E92, INVALID_ECM_DEF, INVALID_ECM_DEF},
        .templOSfilename = NULL,
    },
    [35] =  // E39A Only - CAN                                          //0x23 -> 145 (0x91)
    {
        .index = 35,
        .flags = SKIP_RECYCLE_KEY_ON,
        .ecm_types = {ECM_DEF_INDEX__E39A, INVALID_ECM_DEF, INVALID_ECM_DEF},
        .templOSfilename = NULL,
    },
    [36] =  // E37 Hitachi - CAN & T43 - CAN                            //0x24 -> 146 (0x92)
    {
        .index = 36,
        .flags = SKIP_RECYCLE_KEY_ON,
        .ecm_types = {ECM_DEF_INDEX__E37, ECM_DEF_INDEX__T43, INVALID_ECM_DEF},
        .templOSfilename = NULL,
    },
    [37] =  // E37 Hitachi - CAN & T43A - CAN                           //0x25 -> 147 (0x93)
    {
        .index = 37,
        .flags = SKIP_RECYCLE_KEY_ON,
        .ecm_types = {ECM_DEF_INDEX__E37, ECM_DEF_INDEX__T43A, INVALID_ECM_DEF},
        .templOSfilename = NULL,
    },
    [38] =  // E78 Only - CAN                                           //0x26 -> 148 (0x94)
    {
        .index = 38,
        .flags = SKIP_RECYCLE_KEY_ON,
        .ecm_types = {ECM_DEF_INDEX__E78, INVALID_ECM_DEF, INVALID_ECM_DEF},
        .templOSfilename = NULL,
    },
    [39] =  // E78 - CAN & T43A - CAN                                   //0x27 -> 149 (0x95)
    {
        .index = 39,
        .flags = SKIP_RECYCLE_KEY_ON,
        .ecm_types = {ECM_DEF_INDEX__E78, ECM_DEF_INDEX__T43A, INVALID_ECM_DEF},
        .templOSfilename = NULL,
    },
    [40] =  // E78 - CAN & T76 - CAN                                    //0x28 -> 150 (0x96)
    {
        .index = 40,
        .flags = SKIP_RECYCLE_KEY_ON,
        .ecm_types = {ECM_DEF_INDEX__E78, ECM_DEF_INDEX__T76, INVALID_ECM_DEF},
        .templOSfilename = NULL,
    },
    //41,42: none
    [43] =  // E39-1 Only - CAN                                         //0x2B -> 153 (0x99)
    {
        .index = 43,
        .flags = SKIP_RECYCLE_KEY_ON,
        .ecm_types = {ECM_DEF_INDEX__E39_1, INVALID_ECM_DEF, INVALID_ECM_DEF},
        .templOSfilename = NULL,
    },
    [44] =  // E39-1 - CAN & T43 - CAN                                  //0x2C -> 154 (0x9A)
    {
        .index = 44,
        .flags = SKIP_RECYCLE_KEY_ON,
        .ecm_types = {ECM_DEF_INDEX__E39_1, ECM_DEF_INDEX__T43, INVALID_ECM_DEF},
        .templOSfilename = NULL,
    },
    [45] =  // E39-1 - CAN & T43A - CAN                                 //0x2D -> 155 (0x9B)
    {
        .index = 45,
        .flags = SKIP_RECYCLE_KEY_ON,
        .ecm_types = {ECM_DEF_INDEX__E39_1, ECM_DEF_INDEX__T43A, INVALID_ECM_DEF},
        .templOSfilename = NULL,
    },
    [46] =  // E39-2 Only - CAN                                         //0x2E -> 156 (0x9C)
    {
        .index = 46,
        .flags = SKIP_RECYCLE_KEY_ON,
        .ecm_types = {ECM_DEF_INDEX__E39_2, INVALID_ECM_DEF, INVALID_ECM_DEF},
        .templOSfilename = NULL,
    },
    [47] =  // E39-2 - CAN & T43 - CAN                                  //0x2F -> 157 (0x9D)
    {
        .index = 47,
        .flags = SKIP_RECYCLE_KEY_ON,
        .ecm_types = {ECM_DEF_INDEX__E39_2, ECM_DEF_INDEX__T43, INVALID_ECM_DEF},
        .templOSfilename = NULL,
    },
    [48] =  // E39-2 - CAN & T43A - CAN                                 //0x30 -> 158 (0x9E)
    {
        .index = 48,
        .flags = SKIP_RECYCLE_KEY_ON,
        .ecm_types = {ECM_DEF_INDEX__E39_2, ECM_DEF_INDEX__T43A, INVALID_ECM_DEF},
        .templOSfilename = NULL,
    },
//    [49] =  // E86 LML Diesel - CAN                                     //0x31 -> 159 (0x9F)
//    {
//        .index = 49,
//        .flags = SKIP_RECYCLE_KEY_ON,
//        .ecm_types = {ECM_DEF_INDEX__E86, INVALID_ECM_DEF, INVALID_ECM_DEF},
//        .templOSfilename = NULL,
//    },  
    [50] =  // E39-1 - CAN & T76 - CAN                                  //0x32 -> 160 (0xA0)
    {
        .index = 50,
        .flags = SKIP_RECYCLE_KEY_ON,
        .ecm_types = {ECM_DEF_INDEX__E39_1, ECM_DEF_INDEX__T76, INVALID_ECM_DEF},
        .templOSfilename = NULL,
    },
    [51] =  // E39-2 - CAN & T76 - CAN                                  //0x33 -> 161 (0xA1)
    {
        .index = 51,
        .flags = SKIP_RECYCLE_KEY_ON,
        .ecm_types = {ECM_DEF_INDEX__E39_2, ECM_DEF_INDEX__T76, INVALID_ECM_DEF},
        .templOSfilename = NULL,
    },
    [52] =  // E39A - CAN & T76 - CAN                                   //0x34 -> 162 (0xA2)
    {
        .index = 52,
        .flags = SKIP_RECYCLE_KEY_ON,
        .ecm_types = {ECM_DEF_INDEX__E39A, ECM_DEF_INDEX__T76, INVALID_ECM_DEF},
        .templOSfilename = NULL,
    },
    [53] =  // E39A - CAN & T43A - CAN                                  //0x35 -> 163 (0xA3)
    {
        .index = 53,
        .flags = SKIP_RECYCLE_KEY_ON,
        .ecm_types = {ECM_DEF_INDEX__E39A, ECM_DEF_INDEX__T43A, INVALID_ECM_DEF},
        .templOSfilename = NULL,
    },    
    [54] =  // E39 - CAN & T42 - CAN                                    //0x35 -> 164 (0xA4)
    {
        .index = 54,
        .flags = SKIP_RECYCLE_KEY_ON,
        .ecm_types = {ECM_DEF_INDEX__E39_1, ECM_DEF_INDEX__T42, INVALID_ECM_DEF},
        .templOSfilename = NULL,
    },
    [55] =  // E39A - CAN & T42 - CAN                                   //0x36 -> 165 (0xA5)
    {
        .index = 55,
        .flags = SKIP_RECYCLE_KEY_ON,
        .ecm_types = {ECM_DEF_INDEX__E39A, ECM_DEF_INDEX__T42, INVALID_ECM_DEF},
        .templOSfilename = NULL,
    },
    [56] =  // E92 - CAN & T43A - CAN                                   //0x37 -> 166 (0xA6)
    {
        .index = 56,
        .flags = SKIP_RECYCLE_KEY_ON,
        .ecm_types = {ECM_DEF_INDEX__E92, ECM_DEF_INDEX__T43A, INVALID_ECM_DEF},
        .templOSfilename = NULL,
    },
    //57-58: none
//    [59] =  // E69 Bosch - CAN                                          //0x3B -> 169 (0xA9)
//    {
//        .index = 59,
//        .flags = SKIP_RECYCLE_KEY_ON,
//        .ecm_types = {ECM_DEF_INDEX__E69, INVALID_ECM_DEF, INVALID_ECM_DEF},
//        .templOSfilename = NULL,
//    },
    //60-69: none
//    [70] =  // E83A - CAN                                               //0x46 -> 180 (0xB4)
//    {
//        .index = 70,
//        .flags = SKIP_RECYCLE_KEY_ON,
//        .ecm_types = {ECM_DEF_INDEX__E83A, INVALID_ECM_DEF, INVALID_ECM_DEF},
//        .templOSfilename = NULL,
//    },
//    [71] =  // E83B - CAN                                               //0x47 -> 181 (0xB5)
//    {
//        .index = 70,
//        .flags = SKIP_RECYCLE_KEY_ON,
//        .ecm_types = {ECM_DEF_INDEX__E83B, INVALID_ECM_DEF, INVALID_ECM_DEF},
//        .templOSfilename = NULL,
//    },
};

