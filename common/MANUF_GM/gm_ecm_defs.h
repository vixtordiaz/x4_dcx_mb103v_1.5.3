/**
  **************** (C) COPYRIGHT 2011 SCT Performance, LLC *********************
  * File Name          : gm_ecm_defs.h
  * Device / System    : SCT products using the "CommonCode" platform
  * Author             : Quyen Leba
  *
  * Version            : 1 
  * Date               : 05/02/2011
  * Description        : 
  *                    : 
  *
  *
  * History            : 05/02/2011 M. Davis
  *                    :   Add SCT Copyright header,
  *
  ******************************************************************************
  */

#ifndef __GM_ECM_DEFS_H
#define __GM_ECM_DEFS_H

#include <arch/gentype.h>
#include <common/obd2.h>
#include <common/ecm_defs.h>

#define MAX_UPLOAD_BUFFER_LENGTH        0x1000
#define MAX_DOWNLOAD_BUFFER_LENGTH      0x1000

#define UPLOAD_CMD_MEMORY_SIZE          MemorySize_2_Byte
#define DEFAULT_CMD_MEMORY_SIZE         MemorySize_2_Byte

enum
{
    UnlockId_None               = 0,
};

enum
{
    ChecksumId_None             = 0x0000,
};

enum
{
    FinalId_None                = 0x0000,
};

#define ECM_DEF_COUNT           30
extern const ECM_Def gm_ecm_defs[ECM_DEF_COUNT];

u32 GM_ECM_GetAddedBlockLength(u16 ecm_type, u8 index);

//------------------------------------------------------------------------------
// et:  ecm_type
//------------------------------------------------------------------------------
#define ecmvar(et,var)                      gm_ecm_defs[et].##var

#endif    //__GM_ECM_DEFS_H
