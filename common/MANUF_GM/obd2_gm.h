/**
  **************** (C) COPYRIGHT 2011 SCT Performance, LLC *********************
  * File Name          : obd2_gm.h
  * Device / System    : SCT products using the "CommonCode" platform
  * Author             : Quyen Leba
  *
  * Version            : 1 
  * Date               : 05/02/2011
  * Description        : 
  *                    : 
  *
  *
  * History            : 05/02/2011 M. Davis
  *                    :   Add SCT Copyright header,
  *
  ******************************************************************************
  */

#ifndef __OBD2_GM_H
#define __OBD2_GM_H

#include <arch/gentype.h>
#include <common/obd2def.h>
#include <common/obd2tune.h>

u8 obd2_gm_generatekey(u8 *seed, u32 algoindex, VehicleCommType commtype,
                       u8 *key);
u8 obd2_gm_wakeup(u16 veh_type, u16 ecm_index);

u8 obd2_gm_vehicleinfototext(vehicle_info *vehicleinfo, u8 *vehicleinfo_text);

u8 obd2_gm_search_veh_type_by_hardware_id(u8 *hwid, u16 *veh_type);
u8 obd2_gm_search_ecm_type_by_cal_id(u8 *calid, u16 *veh_type);
u8 obd2_gm_vehicle_type_search(ecm_info* ecm);

void obd2_gm_crank_relearn(VehicleCommType commtype,
                           VehicleCommLevel commlevel);

void obd2_gm_vehiclefunction(VehicleCommType *commtype,
                             VehicleCommLevel *commlevel);

#endif    //__OBD2_GM_H
