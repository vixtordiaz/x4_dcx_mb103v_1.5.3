/**
  **************** (C) COPYRIGHT 2011 SCT Performance, LLC *********************
  * File Name          : obd2can_gmlan.h
  * Device / System    : SCT products using the "CommonCode" platform
  * Author             : Quyen Leba
  *
  * Version            : 1 
  * Date               : 05/02/2011
  * Description        : 
  *                    : 
  *
  *
  * History            : 05/02/2011 M. Davis
  *                    :   Add SCT Copyright header,
  *
  ******************************************************************************
  */

#ifndef __OBD2CAN_GMLAN_H
#define __OBD2CAN_GMLAN_H

#include <common/obd2can.h>

u8 obd2can_txrx(Obd2can_ServiceData *servicedata);
u8 obd2can_txrxcomplex(Obd2can_ServiceData *servicedata);

//#define OBD2CAN_TXRX_DEFINED
//#define obd2can_txrx(servicedata)           obd2can_gmlan_txrx(servicedata)
//
//#define OBD2CAN_TXRXCOMPLEX_DEFINED
//#define obd2can_txrxcomplex(servicedata)    obd2can_gmlan_txrxcomplex(servicedata)

#endif    //__OBD2CAN_GMLAN_H
