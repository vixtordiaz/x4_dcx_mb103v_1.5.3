/**
  **************** (C) COPYRIGHT 2011 SCT Performance, LLC *********************
  * File Name          : obd2datalog_gm.c
  * Device / System    : SCT products using the "CommonCode" platform
  * Author             : Quyen Leba
  *
  * Version            : 1 
  * Date               : 05/02/2011
  * Description        : 
  *                    : 
  *
  *
  * History            : 05/02/2011 M. Davis
  *                    :   Add SCT Copyright header,
  *
  ******************************************************************************
  */

//------------------------------------------------------------------------------
// Log error point: 0x4900 -> 0x4AFF (shared with obd2datalog.c)
// Use 0x49A0 -> 0x49BF
//------------------------------------------------------------------------------

#include <common/statuscode.h>
#include <common/obd2.h>
#include <common/obd2vpw.h>
#include <common/genmanuf_overload.h>
#include <common/obd2datalog.h>
#include "obd2datalog_gm.h"

extern Datalog_Mode gDatalogmode;       //from obd2datalog.c
extern Datalog_Info *dataloginfo;       //from obd2datalog.c

/**
 *  @brief GM Pre-PID validation task
 *
 *  @param [in]     ecm_index  ECM Index
 *  @param [in]     commtype   Vehicle comm type
 *  @param [in]     commlevel  Vehicle comm level
 *
 *  @return Status
 */
u8 obd2datalog_gm_prevalidate_task(u16 ecm_index, VehicleCommType commtype, VehicleCommLevel commlevel)
{    
    u32 ecm_id;
    u8 data[4];
    u8 status = S_SUCCESS;
    
    /* Only handle CAN here; other comm types handled in obd2datalog.c */
    if (commtype == CommType_CAN)
    {
        switch (ecm_index)
        {
        case 0:
            ecm_id = Obd2CanEcuId_7E0;
            break;
        case 1:
            ecm_id = Obd2CanEcuId_7E2;
            break;
        default:
            status = S_FAIL;
            break;
        }
        if (status == S_SUCCESS)
        {
            status = obd2_readpid_mode1(FALSE, ecm_id, 0x00, 4, data, commtype);
            if (status != S_SUCCESS)
            {
                gDatalogmode.ecmBroadcast = TRUE;
                status = S_SUCCESS;
            }
        }
    }
    else
    {
        status = S_NOTSUPPORT;
    }
    return status;
}

//------------------------------------------------------------------------------
// Validate a datalog pid
// Inputs:  u32 ecm_id
//          u16 pid
//          VehicleCommType commtype,
//          VehicleCommLevel commlevel
// Return:  u8  status
// Engineer: Quyen Leba
//------------------------------------------------------------------------------
u8 obd2datalog_gm_validatepid(u32 ecm_id, DlxBlock *dlxblock,
                              VehicleCommType commtype,
                              VehicleCommLevel commlevel)
{
    u8 status;

    status = S_SUCCESS;
    if (commtype == CommType_CAN)
    {
        if (!SETTINGS_IsDemoMode())
        {
            status = obd2can_validatepid(ecm_id,dlxblock,commlevel);
        }
    }
    else if (commtype == CommType_VPW)
    {
        if (!SETTINGS_IsDemoMode())
        {
            status = obd2vpw_validatepid(dlxblock,commlevel);
        }
    }
    else
    {
        log_push_error_point(0x49B9);
        status = S_COMMTYPE;
    }
    return status;
}

//------------------------------------------------------------------------------
// Validate a datalog pid/dmr by rapid packet setup
// Inputs:  u32 ecm_id
//          u16 pid
//          VehicleCommType commtype,
//          VehicleCommLevel commlevel
// Return:  u8  status
// Engineer: Quyen Leba
//------------------------------------------------------------------------------
u8 obd2datalog_gm_validateByRapidPacketSetup(u32 ecm_id, DlxBlock *dlxblock,
                              VehicleCommType commtype,
                              VehicleCommLevel commlevel)
{
    u8 status;

    status = S_SUCCESS;
    if (commtype == CommType_CAN)
    {
        if (!SETTINGS_IsDemoMode())
        {
            status = obd2can_gm_validateByRapidPacketSetup(ecm_id,dlxblock,commlevel);
        }
    }
    else if (commtype == CommType_VPW)
    {
        if (!SETTINGS_IsDemoMode())
        {
            status = obd2vpw_validateByRapidPacketSetup(dlxblock,commlevel);
        }
    }
    else
    {
        log_push_error_point(0x49A9);
        status = S_COMMTYPE;
    }
    return status;
}

//------------------------------------------------------------------------------
// Evaluate all items for what can datalog in one session
// Inputs:  VehicleCommType vehiclecommtype[ECM_MAX_COUNT]
//          VehicleCommLevel vehiclecommlevel[ECM_MAX_COUNT]
// Outputs: u16 *valid_index_list
//          u16 *valid_index_count
// Return:  u8  status
// Engineer: Quyen Leba
//------------------------------------------------------------------------------
u8 obd2datalog_gm_evaluate_fit(VehicleCommType *vehiclecommtype,
                               VehicleCommLevel *vehiclecommlevel,
                               u16 *valid_index_list,
                               u16 *valid_index_count)
{
    u16 i;
    u16 count;
    u8  status;

    if (dataloginfo == NULL)
    {
        log_push_error_point(0x49AA);
        return S_BADCONTENT;
    }
    else if (valid_index_count == NULL || valid_index_list == NULL)
    {
        log_push_error_point(0x49AB);
        return S_BADCONTENT;
    }

    *valid_index_count = 0;
    if (vehiclecommtype[0] == CommType_CAN)
    {
        status = obd2can_gm_datalog_evaluatesignalsetup(vehiclecommlevel);
    }
    else if (vehiclecommtype[0] == CommType_VPW)
    {
        status = obd2vpw_datalog_evaluatesignalsetup(vehiclecommlevel);
        if (status == S_NOTFIT)
        {
            //allow to continue but use non rapid packet mode
            gDatalogmode.packetmode = SingleRate;
            status = obd2vpw_datalog_evaluatesignalsetup(vehiclecommlevel);
        }
    }
    else
    {
        log_push_error_point(0x49AC);
        return S_COMMTYPE;
    }
    if (status == S_SUCCESS || status == S_NOTFIT)
    {
        count = 0;
        for(i=0;i<dataloginfo->datalogsignalcount;i++)
        {
            switch (dataloginfo->datalogsignals[i].PidType)
            {
                case PidTypeRegular:
                case PidTypeDMR:
                    // VPW is either RapidRate only or SingleRate only
                    if (vehiclecommtype[0] == CommType_VPW && 
                        gDatalogmode.packetmode == RapidRate)
                    {
                        // Only count valid packets
                        if (dataloginfo->datalogsignals[i].SignalType.Generic.PacketNumber == DATALOG_INVALID_PACKETID)
                        {
                            break;
                        }                        
                    }
                case PidTypeMode1:    
                case PidTypeAnalog:
                case PidTypeOSC:
                    valid_index_list[count++] = i;
                    break;
                default:
                    break;
            }
            if (count >= DATALOG_MAX_VALID_PER_SESSION)
            {
                break;
            }
        }
        *valid_index_count = count;
        return S_SUCCESS;
    }
    log_push_error_point(0x49AD);
    return S_FAIL;
}

//------------------------------------------------------------------------------
// Prepare for datalog, setup packets (if fit), setup datalog function calls
// Inputs:  VehicleCommType vehiclecommtype[ECM_MAX_COUNT]
//          VehicleCommLevel vehiclecommlevel[ECM_MAX_COUNT]
// Return:  u8  status
// Engineer: Quyen Leba
//------------------------------------------------------------------------------
u8 obd2datalog_gm_setupdatalogsession(VehicleCommType *vehiclecommtype,
                                      VehicleCommLevel *vehiclecommlevel)
{
    u8  status = S_SUCCESS;

    if (dataloginfo == NULL)
    {
        log_push_error_point(0x49AE);
        return S_BADCONTENT;
    }
    
    // Initialize Tasks
    dataloginfo->scheduled_tasks = DATALOG_TASK_NONE;    

    if (vehiclecommtype[0] == CommType_CAN)
    {
        if (vehiclecommlevel[0] == CommLevel_GMLAN &&
            gDatalogmode.packetmode == RapidRate)
        {
            status = obd2can_gm_datalog_evaluatesignalsetup(vehiclecommlevel);
            if (status == S_SUCCESS)
            {
                status = obd2can_gm_datalog_initiatepackets(vehiclecommlevel);       
                if (status != S_SUCCESS)
                {
                    // No packets being used or packets are unsupported
                    gDatalogmode.packetmode = SingleRate;
                    status = obd2_datalog_evaluatesignalsetup(vehiclecommlevel);
                }
                if (status != S_SUCCESS)
                {
                    log_push_error_point(0x49B0);
                    return status;
                }
            }
        }
        else
        {
            // Allow single rate SAE PID datalogging on other OEM's
            gDatalogmode.packetmode = SingleRate;
            status = obd2_datalog_evaluatesignalsetup(vehiclecommlevel);
        }
        if (status != S_SUCCESS)
        {
            log_push_error_point(0x49AF);            
            return status;
        }        

        if (SETTINGS_IsDemoMode())
        {
            dataloginfo->getdatafunc = obd2datalog_demo_getdata;
            dataloginfo->getanalogdatafunc = obd2datalog_getdata_analog;
            dataloginfo->sendtesterpresent = obd2datalog_demo_sendtesterpresent;
            dataloginfo->scheduled_tasks |= DATALOG_TASK_GET_DATA
                                         |  DATALOG_TASK_ANALOG;
            status = S_SUCCESS;
        }
        else
        {
            switch(gDatalogmode.packetmode)
            {
                case RapidRate:
                    dataloginfo->getdatafunc = obd2can_gm_datalog_getdata;
                    dataloginfo->sendtesterpresent = obd2can_gm_datalog_sendtesterpresent;
                    dataloginfo->scheduled_tasks |= DATALOG_TASK_GET_DATA
                                                 |  DATALOG_TASK_MODE1_COMMLOST
                                                 |  DATALOG_TASK_ANALOG;
                    break;
                case SingleRate:
                    dataloginfo->getdatafunc = obd2can_gm_datalog_getdata;
                    dataloginfo->scheduled_tasks |= DATALOG_TASK_GET_DATA                                                 
                                                 |  DATALOG_TASK_ANALOG;
                    break;
                default:
                    status = S_FAIL;
                    break;
            }            
            // If in OSC, make sure to send testerpresents and set OSC MODE
            if (dataloginfo->oscsignalcount > 0)
            {
                dataloginfo->scheduled_tasks |= DATALOG_TASK_OSC;
                dataloginfo->sendtesterpresent = obd2can_gm_datalog_sendtesterpresent;
            }
            dataloginfo->getanalogdatafunc = obd2datalog_getdata_analog;                        
        }
    }
    else if (vehiclecommtype[0] == CommType_VPW)
    {
        status = obd2vpw_datalog_evaluatesignalsetup(vehiclecommlevel);
        
        if (status == S_SUCCESS && gDatalogmode.packetmode == RapidRate)
        {
            status = obd2vpw_datalog_initiatepackets(vehiclecommlevel);
        }
        
        if (status != S_SUCCESS)
        {
            // Rapid packets failed to set up or no packets needed
            log_push_error_point(0x49B1);            
            gDatalogmode.packetmode = SingleRate;
            status = obd2vpw_datalog_evaluatesignalsetup(vehiclecommlevel);
        }
        
        if (status != S_SUCCESS)
        {
            log_push_error_point(0x49B2);
            return status;
        }

        if (SETTINGS_IsDemoMode())
        {
            dataloginfo->getdatafunc = obd2datalog_demo_getdata;
            dataloginfo->getanalogdatafunc = obd2datalog_getdata_analog;
            dataloginfo->sendtesterpresent = obd2datalog_demo_sendtesterpresent;
            dataloginfo->scheduled_tasks |= DATALOG_TASK_GET_DATA
                                         |  DATALOG_TASK_ANALOG;
            status = S_SUCCESS;
        }
        else
        {            
            switch(gDatalogmode.packetmode)
            {
                case RapidRate:
                    dataloginfo->getdatafunc = obd2vpw_datalog_getdata;                    
                    dataloginfo->sendtesterpresent = obd2vpw_datalog_sendtesterpresent;
                    obd2vpw_datalog_sendtesterpresent();
                    // VPW does not allow mixed rapid/single rate datalogging
                    dataloginfo->scheduled_tasks |= DATALOG_TASK_GET_DATA
                                                 |  DATALOG_TASK_ANALOG;
                    break;
                case SingleRate:
                    dataloginfo->getdatafunc = obd2datalog_getdata_singlerate;
                    dataloginfo->scheduled_tasks |= DATALOG_TASK_GET_DATA_SINGLE_RATE                                                 
                                                 |  DATALOG_TASK_ANALOG;
                    break;
                default:
                    status = S_FAIL;
                    break;
            } 
            dataloginfo->getanalogdatafunc = obd2datalog_getdata_analog;                        
        }
    }
    else if (vehiclecommtype[1] == CommType_CAN ||
             vehiclecommtype[1] == CommType_KLINE ||
             vehiclecommtype[1] == CommType_KWP2000 ||
             vehiclecommtype[1] == CommType_VPW)
    {
        status = obd2_datalog_evaluatesignalsetup(vehiclecommlevel);
        if (status == S_SUCCESS || status == S_NOTFIT)
        {
            gDatalogmode.packetmode = SingleRate;
            dataloginfo->getanalogdatafunc = obd2datalog_getdata_analog;
            dataloginfo->getdatafunc = obd2datalog_getdata_singlerate;
            dataloginfo->scheduled_tasks |= DATALOG_TASK_GET_DATA_SINGLE_RATE
                                         |  DATALOG_TASK_ANALOG;
        }
    }
    else
    {
        log_push_error_point(0x49BF);
        return S_COMMTYPE;
    }
    return status;
}

//------------------------------------------------------------------------------
// Start datalog session
// Inputs:  VehicleCommType vehiclecommtype[ECM_MAX_COUNT]
//          VehicleCommLevel vehiclecommlevel[ECM_MAX_COUNT]
// Return:  u8  status
// Engineer: Quyen Leba
//------------------------------------------------------------------------------
u8 obd2datalog_gm_startdatalogsession(VehicleCommType *vehiclecommtype,
                                      VehicleCommLevel *vehiclecommlevel)
{
    u32 i;
    u8  status;

    if (dataloginfo == NULL)
    {
        log_push_error_point(0x49B4);
        return S_BADCONTENT;
    }
    
    status = S_SUCCESS;
    if (vehiclecommtype[0] == CommType_CAN)
    {        
        if (gDatalogmode.packetmode == RapidRate)
        {
            const u32 ecm_id[ECM_MAX_COUNT] = {Obd2CanEcuId_7E0,Obd2CanEcuId_7E2,0};

            for(i=0;i<ECM_MAX_COUNT;i++)
            {
                if(dataloginfo->packetidcount[i] == 0)
                    continue;

                if (vehiclecommlevel[i] == CommLevel_GMLAN)
                {
                    status = obd2can_gm_read_data_by_packetid_request
                        (ecm_id[i],scheduleAtFastRate_$AA,
                         dataloginfo->packetidlist[i],dataloginfo->packetidcount[i]);

                    if (status != S_SUCCESS)
                    {
                        obd2can_gm_read_data_by_packetid_request(ecm_id[i],stopSending_$AA,NULL,0);
                    }
                }
                else if (vehiclecommlevel[i] == CommLevel_Unknown)
                {
                    break;
                }
                else
                {
                    log_push_error_point(0x49B5);
                    status = S_COMMLEVEL;
                    break;
                }
            }
        }
        else // Non-rapid packet
        {
            status = S_SUCCESS;
        }
        // Unset all OSC items (if no OSC PIDs, this function does nothing)
        obd2datalog_gm_unset_all_osc(vehiclecommtype,vehiclecommlevel);
    }
    else if (vehiclecommtype[0] == CommType_VPW)
    {
        if (gDatalogmode.packetmode == RapidRate)
        {
            obd2vpw_request_diagnostic_data_packet(stopSending_$2A,NULL,0);            
            
            status = obd2vpw_request_diagnostic_data_packet
                (scheduleAtFastRate_$2A,
                 dataloginfo->packetidlist[0],dataloginfo->packetidcount[0]);
           
            if (status == S_SUCCESS)
            {
                obd2vpw_datalogmode();
            }
            else
            {
                obd2vpw_request_diagnostic_data_packet(stopSending_$2A,NULL,0);
            }
        }
        else // Non-rapid packet
        {
            obd2vpw_datalogmode();
        }
    }
    else if (vehiclecommtype[1] == CommType_CAN ||
             vehiclecommtype[1] == CommType_KLINE ||
             vehiclecommtype[1] == CommType_KWP2000 ||
             vehiclecommtype[1] == CommType_VPW)
    {
        status = S_SUCCESS;
    }
    else
    {
        log_push_error_point(0x49B6);
        return S_COMMTYPE;
    }
    
    return status;
}

//------------------------------------------------------------------------------
// Stop datalog session
// Inputs:  VehicleCommType vehiclecommtype[ECM_MAX_COUNT]
//          VehicleCommLevel vehiclecommlevel[ECM_MAX_COUNT]
// Return:  u8  status
// Engineer: Quyen Leba
//------------------------------------------------------------------------------
u8 obd2datalog_gm_stopdatalogsession(VehicleCommType *vehiclecommtype,
                                     VehicleCommLevel *vehiclecommlevel)
{
    const u32 ecm_id[ECM_MAX_COUNT] = {Obd2CanEcuId_7E0,Obd2CanEcuId_7E2,0};
    u8  i;

    if (vehiclecommtype[0] == CommType_CAN)
    {
        for(i=0;i<ECM_MAX_COUNT;i++)
        {
            if (vehiclecommlevel[i] == CommLevel_GMLAN)
            {
                obd2can_gm_read_data_by_packetid_request(ecm_id[i],stopSending_$AA,NULL,0);
                // Unset all OSC items (if no OSC PIDs, this function does nothing)
                obd2datalog_gm_unset_all_osc(vehiclecommtype,vehiclecommlevel);
            }
            else if (vehiclecommlevel[i] == CommLevel_Unknown)
            {
                break;
            }
            else
            {
                log_push_error_point(0x49B7);
                return S_COMMLEVEL;
            }
        }
    }
    else if (vehiclecommtype[0] == CommType_VPW)
    {
        if (gDatalogmode.packetmode == RapidRate)
        {
            obd2vpw_request_diagnostic_data_packet(stopSending_$2A,NULL,0);
            obd2vpw_normalmode();
        }
    }
    else if (vehiclecommtype[1] == CommType_CAN ||
             vehiclecommtype[1] == CommType_KLINE ||
             vehiclecommtype[1] == CommType_KWP2000 ||
             vehiclecommtype[1] == CommType_VPW)
    {
        return S_SUCCESS;
    }
    else
    {
        log_push_error_point(0x49B8);
        return S_COMMTYPE;
    }
    
    return S_SUCCESS;
}

/**
 * @brief   Unset all GMLAN OSC Signals
 *
 * @param   [in] VehicleCommType
 * @param   [in] VehicleCommLevel
 *
 * @retval  u8 status
 *
 * @author  Tristen Pierson
 *
 * @details This function resets all GMLAN OSC items
 */
u8 obd2datalog_gm_unset_all_osc(VehicleCommType *vehiclecommtype,
                                VehicleCommLevel *vehiclecommlevel)
{
    u32 ecm_id[2] = {Obd2CanEcuId_7E0,Obd2CanEcuId_7E2};
    u32 ecm_index;
    u16 i,j,k;
    u8  skip;
    u8  status;
    u8  addressList[MAX_DATALOG_DLXBLOCK_COUNT];
    u8  controlbytes[5];
    
    memset(addressList,0x00,sizeof(addressList));
    memset(controlbytes,0x00,sizeof(controlbytes));
    k = 0;
    
    if (vehiclecommtype[0] == CommType_CAN && 
        vehiclecommlevel[0] == CommLevel_GMLAN)
    {
        obd2can_gm_datalog_sendtesterpresent();
        for (i = 0; i < dataloginfo->datalogsignalcount; i++)
        {
            if (dataloginfo->datalogsignals[i].StructType == StructTypeOSC)
            {
                ecm_index = dataloginfo->datalogsignals[i].EcmIndex;
                skip = 0;
                // Skip addresses already processed
                for (j = 0; j < dataloginfo->datalogsignalcount; j++)
                {
                    if (dataloginfo->datalogsignals[i].Address == addressList[j])
                    {
                        skip = 1;
                        break;
                    }
                }
                // Reset current CPID
                if (skip == 0)
                {
                    addressList[k++] = dataloginfo->datalogsignals[i].Address;
                    status = obd2can_gm_device_control(ecm_id[ecm_index], 
                                       dataloginfo->datalogsignals[i].Address,
                                       controlbytes,sizeof(controlbytes), TRUE);
                    
                    // Update status for all items in current CPID address
                    for (j = 0; j < dataloginfo->datalogsignalcount; j++)
                    {
                        if (dataloginfo->datalogsignals[j].StructType == StructTypeOSC &&
                            dataloginfo->datalogsignals[j].Address == dataloginfo->datalogsignals[i].Address)
                        {
                            if (status == S_SUCCESS)
                            {
                                DatalogControl_SetOSC(dataloginfo->datalogsignals[j].Control,OSC_Success);
                                dataloginfo->datalogsignals[j].Value = OSC_Default;
                            }
                            else
                            {
                                DatalogControl_SetOSC(dataloginfo->datalogsignals[j].Control,OSC_Fail);
                                dataloginfo->datalogsignals[j].Value = OSC_Fail;
                            }
                            dataloginfo->datalogsignals[j].Control |= DATALOG_CONTROL_NEWDATA_SET;
                        }
                    }
                }
            }
        } //for (i=0...
        obd2can_gm_datalog_sendtesterpresent();
    } // if(vehiclecommlevel[0]...
    else
    {
        log_push_error_point(0x49BD);
        return S_COMMTYPE;
    }
    return S_SUCCESS;
}