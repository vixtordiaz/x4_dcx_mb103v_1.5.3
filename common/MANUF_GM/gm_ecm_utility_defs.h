/**
  **************** (C) COPYRIGHT 2011 SCT Performance, LLC *********************
  * File Name          : gm_ecm_utility_defs.h
  * Device / System    : SCT products using the "CommonCode" platform
  * Author             : Quyen Leba
  *
  * Version            : 1 
  * Date               : 05/02/2011
  * Description        : 
  *                    : 
  *
  *
  * History            : 05/02/2011 M. Davis
  *                    :   Add SCT Copyright header,
  *
  ******************************************************************************
  */

#ifndef __GM_ECM_UTILITY_DEFS_H
#define __GM_ECM_UTILITY_DEFS_H

#include <arch/gentype.h>
#include <common/obd2.h>
#include <common/ecm_defs.h>

extern const utility_info utilityinfolist[];

#endif    // __GM_ECM_UTILITY_DEFS_H
