/**
  **************** (C) COPYRIGHT 2011 SCT Performance, LLC *********************
  * File Name          : gm_ecm_defs.c
  * Device / System    : SCT products using the "CommonCode" platform
  * Author             : Quyen Leba
  *
  * Version            : 1 
  * Date               : 05/02/2011
  * Description        : 
  *                    : 
  *
  *
  * History            : 05/02/2011 M. Davis
  *                    :   Add SCT Copyright header,
  *
  ******************************************************************************
  */

#include "gm_ecm_utility_defs.h"
#include "gm_ecm_defs.h"

enum
{
    FILE_0      = 0,
    FILE_1      = 1,
    FILE_2      = 2,
    FILE_3      = 3,
};

typedef struct
{
    u32 filesize;
    u8 *filename;
}IndexedAppendFile;

//Filename used in some processor to append file content instead of upload from
//processor. Note: firmware won't rely on reading filesize from file while
//doing stock verification
const IndexedAppendFile IndexedAppendFiles[1] = {
    [FILE_0] = {65536,  "/E69_1C0000_01.bin"}
};

//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
const skipblock_info os_block_read_skipblock_info_e39[] =
{
    //{address, length}:    must be multiple of 0x800 bytes
    {   0xF000,     0x1000},
    {   0x13000,    0x7800},
    {   0x1B000,    0x1000},
    {   0x1E000,    0x6000},
    {   0x25800,    0x2800},
    {   0x2A000,    0x5800},
    {   0xC6800,    0x4000},
    {   0x25B000,   0xA0800},
    {   0x2FE000,   0x1800},
    {   0, 0}
};

//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
const ECM_Def gm_ecm_defs[ECM_DEF_COUNT] =
{
    //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    [0] =   // E40                                                      //0x00
    {
        .index = 0,
        .ecm_id = 0x7E0, .reponse_ecm_id = 0x7E8,
        .unlock_flags = UNLOCK_FLAG_NONE,
        .upload_unlock_id = 360,
        .download_unlock_id = 360,
        .checksum_id = ChecksumId_None,
        .finalizing_id = FinalId_None,
        .flags = UTILITY_CHECK | UPLOAD_BY_CMD_23 | UPLOAD_ADDR_TYPE_3B |
            USE_ERASE_COMMAND | USE_DOWNLOAD_EXECUTE | COOLDOWNTIME_SHORT |
            DOWNLOAD_REQUEST_TYPE_3B | DOWNLOAD_ADDR_TYPE_3B | DOWNLOAD_ALL,
        .extended_flags = UTILITY_CHECK_LEGACY | REQUIRE_KEYOFF_POWERDOWN_15S,
        .commtype = CommType_CAN, .commlevel = CommLevel_GMLAN,
        .eraseall_cmd = {EraseCmdInfoFlags_None,EraseCmdInfoCount1,{0xEE}},
        .erasecal_cmd = {EraseCmdInfoFlags_None,EraseCmdInfoCount1,{0xEA}},
        .testerpresent_frequency = 1,
        
        .data_offset = 0x0,
        .ecm_startingaddr = 0x0,

        .util_upload = {(utility_info*)&utilityinfolist[0],NULL,NULL},
        .util_dnload = {(utility_info*)&utilityinfolist[1],NULL,NULL},
        .os_test_method = 1,
        
        .maxuploadblocksize = 0x800, .maxdownloadblocksize = 0x400,
        .os_block_read_skipblock_info = NULL,
        .os_block_read =
        {
            {0x00000,   0x100000,   ORDER_0 | MEMBLOCK_FLAGS_NONE, 0},
            {0, 0, ORDER_1 | MEMBLOCK_FLAGS_NONE, 0},
            {0, 0, ORDER_2 | MEMBLOCK_FLAGS_NONE, 0},
            {0, 0, ORDER_3 | MEMBLOCK_FLAGS_NONE, 0},
            {0, 0, ORDER_4 | MEMBLOCK_FLAGS_NONE, 0},
        },
        .os_block =
        {
            {0x8000,    0xF8000,    ORDER_0 | MEMBLOCK_FLAGS_NONE, 0},
            {0, 0, ORDER_1 | MEMBLOCK_FLAGS_NONE, 0},
            {0, 0, ORDER_2 | MEMBLOCK_FLAGS_NONE, 0},
            {0, 0, ORDER_3 | MEMBLOCK_FLAGS_NONE, 0},
            {0, 0, ORDER_4 | MEMBLOCK_FLAGS_NONE, 0},
        },
        .cal_block =
        {
            {0x20000,   0x20000,    ORDER_0 | MEMBLOCK_FLAGS_NONE, 0},
            {0, 0, ORDER_1 | MEMBLOCK_FLAGS_NONE, 0},
        },
    },
    //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    [1] =   //E67                                                       //0x01
    {
        .index = 1,
        .ecm_id = 0x7E0, .reponse_ecm_id = 0x7E8,
        .unlock_flags = UNLOCK_FLAG_NONE,
        .upload_unlock_id = 393,
        .download_unlock_id = 393,
        .checksum_id = ChecksumId_None,
        .finalizing_id = FinalId_None,
        .flags = UTILITY_CHECK | UPLOAD_BY_CMD_23 | UPLOAD_ADDR_TYPE_3B |
            USE_ERASE_COMMAND | USE_DOWNLOAD_EXECUTE |
            DOWNLOAD_REQUEST_TYPE_3B | DOWNLOAD_ADDR_TYPE_3B | DOWNLOAD_ALL,
        .extended_flags = UTILITY_CHECK_LEGACY | REQUIRE_KEYOFF_POWERDOWN_15S,
        .commtype = CommType_CAN, .commlevel = CommLevel_GMLAN,
        .eraseall_cmd = {EraseCmdInfoFlags_None,EraseCmdInfoCount1,{0xEE}},
        .erasecal_cmd = {EraseCmdInfoFlags_None,EraseCmdInfoCount1,{0xEA}},
        .testerpresent_frequency = 1,
        
        .data_offset = 0x0,
        .ecm_startingaddr = 0x0,

        .util_upload = {(utility_info*)&utilityinfolist[2],NULL,NULL},
        .util_dnload = {(utility_info*)&utilityinfolist[3],NULL,NULL},
        .os_test_method = 2,
        
        .maxuploadblocksize = 0x800, .maxdownloadblocksize = 0x400,
        .os_block_read_skipblock_info = NULL,
        .os_block_read =
        {
            {0x00000,   0x1C0000,   ORDER_0 | MEMBLOCK_FLAGS_NONE, 0},
            {0x1C0000,  0x40000,    ORDER_1 | MEMBLOCK_FLAGS_NONE, 0},
            {0, 0, ORDER_2 | MEMBLOCK_FLAGS_NONE, 0},
            {0, 0, ORDER_3 | MEMBLOCK_FLAGS_NONE, 0},
            {0, 0, ORDER_4 | MEMBLOCK_FLAGS_NONE, 0},
        },
        .os_block =
        {
            {0x10000,   0x1B0000,   ORDER_0 | MEMBLOCK_FLAGS_NONE, 0},
            {0x1C0000,  0x40000,    ORDER_1 | MEMBLOCK_FLAGS_NONE, 0},
            {0, 0, ORDER_2 | MEMBLOCK_FLAGS_NONE, 0},
            {0, 0, ORDER_3 | MEMBLOCK_FLAGS_NONE, 0},
            {0, 0, ORDER_4 | MEMBLOCK_FLAGS_NONE, 0},
        },
        .cal_block =
        {
            {0x1C0000,  0x40000,    ORDER_0 | MEMBLOCK_FLAGS_NONE, 0},
            {0, 0, ORDER_1 | MEMBLOCK_FLAGS_NONE, 0},
        },
    },
    //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    [2] =   //LBZ: no utility, no erase                                 //0x02
    {
        .index = 2,
        .ecm_id = 0x7E0, .reponse_ecm_id = 0x7E8,
        .unlock_flags = UNLOCK_FLAG_NONE,
        .upload_unlock_id = 374,
        .download_unlock_id = 374,
        .checksum_id = ChecksumId_None,
        .finalizing_id = FinalId_None,
        .flags = UPLOAD_BY_CMD_23 | UPLOAD_ADDR_TYPE_3B |
            USE_DOWNLOAD_EXECUTE | COOLDOWNTIME_MEDIUM |
            DOWNLOAD_REQUEST_TYPE_3B | DOWNLOAD_ADDR_TYPE_3B,            
        .extended_flags = REQUIRE_KEYOFF_POWERDOWN_15S,
        .commtype = CommType_CAN, .commlevel = CommLevel_GMLAN,
        .eraseall_cmd = {EraseCmdInfoFlags_None,EraseCmdInfoCount1,{0xEE}},
        .erasecal_cmd = {EraseCmdInfoFlags_None,EraseCmdInfoCount1,{0xEA}},
        .testerpresent_frequency = 1,
        
        .data_offset = 0x00010000,
        .ecm_startingaddr = 0x01B0000,  //what this?

        .util_upload = {NULL,NULL,NULL},
        .util_dnload = {NULL,NULL,NULL},
        .os_test_method = 0,
        
        .maxuploadblocksize = 0x0F8, .maxdownloadblocksize = 0x400, //was 0xFA & 0x5D2
        .os_block_read_skipblock_info = NULL,
        .os_block_read =
        {
            {0x0010000, 0x030000,   ORDER_0 | MEMBLOCK_FLAGS_PRE_FILL_WITH_S, 0},
            {0x0040000, 0x170000,   ORDER_1 | MEMBLOCK_FLAGS_PRE_FILL_WITH_S, 0},
            {0x01B0000, 0x040000,   ORDER_2 | MEMBLOCK_FLAGS_NONE, 0},
            {0x01F0000, 0x00E000,   ORDER_3 | MEMBLOCK_FLAGS_NONE, 0},
            {0x01FE000, 0x002000,   ORDER_4 | MEMBLOCK_FLAGS_PRE_FILL_WITH_S, 0},
        },
        .os_block =
        {
            {0x0010000, 0x030000,   ORDER_0 | MEMBLOCK_FLAGS_PRE_FILL_WITH_S, 0},
            {0x0040000, 0x170000,   ORDER_1 | MEMBLOCK_FLAGS_PRE_FILL_WITH_S, 0},
            {0x01B0000, 0x040000,   ORDER_2 | MEMBLOCK_FLAGS_NONE, 0},
            {0x01F0000, 0x00E000,   ORDER_3 | MEMBLOCK_FLAGS_NONE, 0},
            {0x01FE000, 0x002000,   ORDER_4 | MEMBLOCK_FLAGS_PRE_FILL_WITH_S, 0},
        },
        .cal_block =
        {
            {0x01B0000, 0x040000,   ORDER_0 | MEMBLOCK_FLAGS_NONE, 0},
            {0x01F0000, 0x00E000,   ORDER_1 | MEMBLOCK_FLAGS_NONE, 0},
            {0, 0, ORDER_2 | MEMBLOCK_FLAGS_NONE, 0},
        },
    },
    //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    [3] =   //LMM: no utility, no erase                                 //0x03
    {
        .index = 3,
        .ecm_id = 0x7E0, .reponse_ecm_id = 0x7E8,
        .unlock_flags = UNLOCK_FLAG_NONE,
        .upload_unlock_id = 374,
        .download_unlock_id = 374,
        .checksum_id = ChecksumId_None,
        .finalizing_id = FinalId_None,
        .flags = UPLOAD_BY_CMD_23 | UPLOAD_ADDR_TYPE_3B |
            USE_DOWNLOAD_EXECUTE | COOLDOWNTIME_MEDIUM |
            DOWNLOAD_REQUEST_TYPE_3B | DOWNLOAD_ADDR_TYPE_3B,
        .extended_flags = REQUIRE_KEYOFF_POWERDOWN_15S,
        .commtype = CommType_CAN, .commlevel = CommLevel_GMLAN,
        .eraseall_cmd = {EraseCmdInfoFlags_None,EraseCmdInfoCount1,{0xEE}},
        .erasecal_cmd = {EraseCmdInfoFlags_None,EraseCmdInfoCount1,{0xEA}},
        .testerpresent_frequency = 1,
        
        .data_offset = 0x0,
        .ecm_startingaddr = 0x0180000,  //what this?
        
        .util_upload = {NULL,NULL,NULL},
        .util_dnload = {NULL,NULL,NULL},
        .os_test_method = 0,
        
        .maxuploadblocksize = 0x0F8, .maxdownloadblocksize = 0x400, //was 0xFA & 0x5D2
        .os_block_read_skipblock_info = NULL,
        .os_block_read =
        {
            {0x0180000, 0x07C000,   ORDER_0 | MEMBLOCK_FLAGS_NONE, 0},
            {0x01FC000, 0x004000,   ORDER_1 | MEMBLOCK_FLAGS_NONE, 0},
            {0, 0, ORDER_2 | MEMBLOCK_FLAGS_NONE, 0},
            {0, 0, ORDER_3 | MEMBLOCK_FLAGS_NONE, 0},
            {0, 0, ORDER_4 | MEMBLOCK_FLAGS_NONE, 0},
        },
        .os_block =
        {
            {0x0180000, 0x07C000,   ORDER_0 | MEMBLOCK_FLAGS_NONE, 0},
            {0x01FC000, 0x004000,   ORDER_1 | MEMBLOCK_FLAGS_NONE, 0},
            {0, 0, ORDER_2 | MEMBLOCK_FLAGS_NONE, 0},
            {0, 0, ORDER_3 | MEMBLOCK_FLAGS_NONE, 0},
            {0, 0, ORDER_4 | MEMBLOCK_FLAGS_NONE, 0},
        },
        .cal_block =
        {
            {0x0180000, 0x07C000,   ORDER_0 | MEMBLOCK_FLAGS_NONE, 0},
            {0x01FC000, 0x004000,   ORDER_1 | MEMBLOCK_FLAGS_NONE, 0},
            {0, 0, ORDER_2 | MEMBLOCK_FLAGS_NONE, 0},
        },
    },
    //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    [4] =   //E38                                                       //0x04
    {
        .index = 4,
        .ecm_id = 0x7E0, .reponse_ecm_id = 0x7E8,
        .unlock_flags = UNLOCK_FLAG_NONE,
        .upload_unlock_id = 402,
        .download_unlock_id = 402,
        .checksum_id = ChecksumId_None,
        .finalizing_id = FinalId_None,
        .flags = UTILITY_CHECK | UPLOAD_BY_CMD_23 | UPLOAD_ADDR_TYPE_3B |
            USE_ERASE_COMMAND | USE_DOWNLOAD_EXECUTE | 
            DOWNLOAD_REQUEST_TYPE_3B | DOWNLOAD_ADDR_TYPE_3B | DOWNLOAD_ALL,
        .extended_flags = UTILITY_CHECK_LEGACY | REQUIRE_KEYOFF_POWERDOWN_15S,
        .commtype = CommType_CAN, .commlevel = CommLevel_GMLAN,
        .eraseall_cmd = {EraseCmdInfoFlags_None,EraseCmdInfoCount1,{0xEE}},
        .erasecal_cmd = {EraseCmdInfoFlags_None,EraseCmdInfoCount1,{0xEA}},
        .testerpresent_frequency = 1,

        .data_offset = 0x0,
        .ecm_startingaddr = 0x0,

        .util_upload = {(utility_info*)&utilityinfolist[4],NULL,NULL},
        .util_dnload = {(utility_info*)&utilityinfolist[5],NULL,NULL},
        .os_test_method = 2,
        
        .maxuploadblocksize = 0x800, .maxdownloadblocksize = 0x400,
        .os_block_read_skipblock_info = NULL,
        .os_block_read =
        {
            {0x00000,   0x1C0000,   ORDER_0 | MEMBLOCK_FLAGS_NONE, 0},
            {0x1C0000,  0x40000,    ORDER_1 | MEMBLOCK_FLAGS_NONE, 0},
            {0, 0, ORDER_2 | MEMBLOCK_FLAGS_NONE, 0},
            {0, 0, ORDER_3 | MEMBLOCK_FLAGS_NONE, 0},
            {0, 0, ORDER_4 | MEMBLOCK_FLAGS_NONE, 0},
        },
        .os_block =
        {
            {0x10000,   0x1B0000,   ORDER_0 | MEMBLOCK_FLAGS_NONE, 0},
            {0x1C0000,  0x40000,    ORDER_1 | MEMBLOCK_FLAGS_NONE, 0},
            {0, 0, ORDER_2 | MEMBLOCK_FLAGS_NONE, 0},
            {0, 0, ORDER_3 | MEMBLOCK_FLAGS_NONE, 0},
            {0, 0, ORDER_4 | MEMBLOCK_FLAGS_NONE, 0},
        },
        .cal_block =
        {
            {0x1C0000,  0x40000,    ORDER_0 | MEMBLOCK_FLAGS_NONE, 0},
            {0, 0, ORDER_1 | MEMBLOCK_FLAGS_NONE, 0},
        },
    },
    //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    [5] =   // LB7 Diesel                                               //0x05
    {
        .index = 5,
        .ecm_id = 0x10, .reponse_ecm_id = 0xF0,
        .unlock_flags = UNLOCK_FLAG_NONE,
        .upload_unlock_id = 54,
        .download_unlock_id = 54,
        .checksum_id = ChecksumId_None,
        .finalizing_id = FinalId_None,
        .flags = UPLOAD_BY_CMD_35_SINGLE_36 | UPLOAD_ADDR_TYPE_3B |
            USE_ERASE_COMMAND | USE_DOWNLOAD_EXECUTE |
            DOWNLOAD_REQUEST_TYPE_3B | DOWNLOAD_ADDR_TYPE_3B,
        .extended_flags = REQUIRE_KEYOFF_POWERDOWN_15S,
        .commtype = CommType_VPW, .commlevel = CommLevel_GMLAN,
        .eraseall_cmd = {EraseCmdInfoFlags_None,EraseCmdInfoCount1,{0x20}},
        .erasecal_cmd = {EraseCmdInfoFlags_None,EraseCmdInfoCount1,{0x10}},
        .testerpresent_frequency = 0,
        
        .data_offset = 0x0,
        .ecm_startingaddr = 0x008000,   //TODOQ: what is this for?

        .util_upload = {(utility_info*)&utilityinfolist[6],NULL,NULL},
        .util_dnload = {(utility_info*)&utilityinfolist[7],NULL,NULL},
        .os_test_method = 0,
        
        .maxuploadblocksize = 0x400, .maxdownloadblocksize = 0x400,
        .os_block_read_skipblock_info = NULL,
        .os_block_read =
        {
            {0x0008000, 0x078000,   ORDER_0 | MEMBLOCK_FLAGS_NONE, 0},
            {0, 0, ORDER_1 | MEMBLOCK_FLAGS_NONE, 0},
            {0, 0, ORDER_2 | MEMBLOCK_FLAGS_NONE, 0},
            {0, 0, ORDER_3 | MEMBLOCK_FLAGS_NONE, 0},
            {0, 0, ORDER_4 | MEMBLOCK_FLAGS_NONE, 0},
        },
        .os_block =
        {
            {0x0008000, 0x078000,    ORDER_0 | MEMBLOCK_FLAGS_NONE, 0},
            {0, 0, ORDER_1 | MEMBLOCK_FLAGS_NONE, 0},
            {0, 0, ORDER_2 | MEMBLOCK_FLAGS_NONE, 0},
            {0, 0, ORDER_3 | MEMBLOCK_FLAGS_NONE, 0},
            {0, 0, ORDER_4 | MEMBLOCK_FLAGS_NONE, 0},
        },
        .cal_block =
        {
            {0x0008000, 0x018000,   MEMBLOCK_FLAGS_NONE, 0},
            {0, 0, ORDER_0 | MEMBLOCK_FLAGS_NONE, 0},
        },
    },
    //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    [6] =   // 1 Meg (BLUE-GREEN)                                       //0x06
    {
        .index = 6,
        .ecm_id = 0x10, .reponse_ecm_id = 0xF0,
        .unlock_flags = UNLOCK_FLAG_NONE,
        .upload_unlock_id = 40,
        .download_unlock_id = 40,
        .checksum_id = ChecksumId_None,
        .finalizing_id = FinalId_None,
        .flags = UPLOAD_BY_CMD_35_SINGLE_36 | UPLOAD_ADDR_TYPE_3B |
            USE_ERASE_COMMAND | USE_DOWNLOAD_EXECUTE |
            DOWNLOAD_REQUEST_TYPE_3B | DOWNLOAD_ADDR_TYPE_3B,
        .extended_flags = REQUIRE_KEYOFF_POWERDOWN_15S,
        .commtype = CommType_VPW, .commlevel = CommLevel_GMLAN,
        .eraseall_cmd = {EraseCmdInfoFlags_None,EraseCmdInfoCount1,{0x20}},
        .erasecal_cmd = {EraseCmdInfoFlags_None,EraseCmdInfoCount1,{0x10}},
        .testerpresent_frequency = 0,
        
        .data_offset = 0x0,
        .ecm_startingaddr = 0x0,

        .util_upload = {(utility_info*)&utilityinfolist[8],NULL,NULL},
        .util_dnload = {(utility_info*)&utilityinfolist[9],NULL,NULL},
        .os_test_method = 0,
        
        .maxuploadblocksize = 0x200, .maxdownloadblocksize = 0x200,
        .os_block_read_skipblock_info = NULL,
        .os_block_read =
        {
            {0x0000000, 0x100000,   ORDER_0 | MEMBLOCK_FLAGS_NONE, 0},
            {0, 0, ORDER_1 | MEMBLOCK_FLAGS_NONE, 0},
            {0, 0, ORDER_2 | MEMBLOCK_FLAGS_NONE, 0},
            {0, 0, ORDER_3 | MEMBLOCK_FLAGS_NONE, 0},
            {0, 0, ORDER_4 | MEMBLOCK_FLAGS_NONE, 0},
        },
        .os_block =
        {
            {0x0000000, 0x100000,    ORDER_0 | MEMBLOCK_FLAGS_NONE, 0},
            {0, 0, ORDER_1 | MEMBLOCK_FLAGS_NONE, 0},
            {0, 0, ORDER_2 | MEMBLOCK_FLAGS_NONE, 0},
            {0, 0, ORDER_3 | MEMBLOCK_FLAGS_NONE, 0},
            {0, 0, ORDER_4 | MEMBLOCK_FLAGS_NONE, 0},
        },
        .cal_block =
        {
            {0x0008000, 0x018000,   ORDER_0 | MEMBLOCK_FLAGS_NONE, 0},
            {0, 0, ORDER_1 | MEMBLOCK_FLAGS_NONE, 0},
        },
    },
    //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    [7] =   // 1/2 Meg Intel(BLUE-CLEAR) (BLUE-RED)                     //0x07
    {
        .index = 7,
        .ecm_id = 0x10, .reponse_ecm_id = 0xF0,
        .unlock_flags = UNLOCK_FLAG_NONE,
        .upload_unlock_id = 40,
        .download_unlock_id = 40,
        .checksum_id = ChecksumId_None,
        .finalizing_id = FinalId_None,
        .flags = UPLOAD_BY_CMD_35_SINGLE_36 | UPLOAD_ADDR_TYPE_3B |
            USE_ERASE_COMMAND | USE_DOWNLOAD_EXECUTE |
            DOWNLOAD_REQUEST_TYPE_3B | DOWNLOAD_ADDR_TYPE_3B,
        .extended_flags = REQUIRE_KEYOFF_POWERDOWN_15S,
        .commtype = CommType_VPW, .commlevel = CommLevel_GMLAN,
        .eraseall_cmd = {EraseCmdInfoFlags_None,EraseCmdInfoCount1,{0x20}},
        .erasecal_cmd = {EraseCmdInfoFlags_None,EraseCmdInfoCount1,{0x10}},
        .testerpresent_frequency = 0,
        
        .data_offset = 0x0,
        .ecm_startingaddr = 0x0,

        .util_upload = {(utility_info*)&utilityinfolist[10],NULL,NULL},
        .util_dnload = {(utility_info*)&utilityinfolist[11],NULL,NULL},
        .os_test_method = 0,
        
        .maxuploadblocksize = 0x200, .maxdownloadblocksize = 0x200,
        .os_block_read_skipblock_info = NULL,
        .os_block_read =
        {
            {0x0000000, 0x080000,   ORDER_0 | MEMBLOCK_FLAGS_NONE, 0},
            {0, 0, ORDER_1 | MEMBLOCK_FLAGS_NONE, 0},
            {0, 0, ORDER_2 | MEMBLOCK_FLAGS_NONE, 0},
            {0, 0, ORDER_3 | MEMBLOCK_FLAGS_NONE, 0},
            {0, 0, ORDER_4 | MEMBLOCK_FLAGS_NONE, 0},
        },
        .os_block =
        {
            {0x0000000, 0x080000,   ORDER_0 | MEMBLOCK_FLAGS_NONE, 0},
            {0, 0, ORDER_1 | MEMBLOCK_FLAGS_NONE, 0},
            {0, 0, ORDER_2 | MEMBLOCK_FLAGS_NONE, 0},
            {0, 0, ORDER_3 | MEMBLOCK_FLAGS_NONE, 0},
            {0, 0, ORDER_4 | MEMBLOCK_FLAGS_NONE, 0},
        },
        .cal_block =
        {
            {0x0000000, 0x080000,   ORDER_0 | MEMBLOCK_FLAGS_NONE, 0},
            {0, 0, ORDER_1 | MEMBLOCK_FLAGS_NONE, 0},
        },
    },
    //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    [8] =   // LLY Diesel                                               //0x08
    {
        .index = 8,
        .ecm_id = 0x10, .reponse_ecm_id = 0xF0,
        .unlock_flags = UNLOCK_FLAG_NONE,
        .upload_unlock_id = 40,
        .download_unlock_id = 40,
        .checksum_id = ChecksumId_None,
        .finalizing_id = FinalId_None,
        .flags = UPLOAD_BY_CMD_35_SINGLE_36 | UPLOAD_ADDR_TYPE_3B |
            USE_ERASE_COMMAND | USE_DOWNLOAD_EXECUTE |
            DOWNLOAD_REQUEST_TYPE_3B | DOWNLOAD_ADDR_TYPE_3B,
        .extended_flags = REQUIRE_KEYOFF_POWERDOWN_15S,
        .commtype = CommType_VPW, .commlevel = CommLevel_GMLAN,
        .eraseall_cmd = {EraseCmdInfoFlags_None,EraseCmdInfoCount1,{0x20}},
        .erasecal_cmd = {EraseCmdInfoFlags_None,EraseCmdInfoCount1,{0x10}},
        .testerpresent_frequency = 0,
        
        .data_offset = 0x0,
        .ecm_startingaddr = 0x008000,   //TOODQ: what is this?

        .util_upload = {(utility_info*)&utilityinfolist[12],NULL,NULL},
        .util_dnload = {(utility_info*)&utilityinfolist[13],NULL,NULL},
        .os_test_method = 0,
        
        .maxuploadblocksize = 0x200, .maxdownloadblocksize = 0x400,
        .os_block_read_skipblock_info = NULL,
        .os_block_read =
        {
            {0x0008000, 0x0F8000,   ORDER_0 | MEMBLOCK_FLAGS_NONE, 0},
            {0, 0, ORDER_1 | MEMBLOCK_FLAGS_NONE, 0},
            {0, 0, ORDER_2 | MEMBLOCK_FLAGS_NONE, 0},
            {0, 0, ORDER_3 | MEMBLOCK_FLAGS_NONE, 0},
            {0, 0, ORDER_4 | MEMBLOCK_FLAGS_NONE, 0},
        },
        .os_block =
        {
            {0x0008000, 0x0F8000,   ORDER_0 | MEMBLOCK_FLAGS_NONE, 0},
            {0, 0, ORDER_1 | MEMBLOCK_FLAGS_NONE, 0},
            {0, 0, ORDER_2 | MEMBLOCK_FLAGS_NONE, 0},
            {0, 0, ORDER_3 | MEMBLOCK_FLAGS_NONE, 0},
            {0, 0, ORDER_4 | MEMBLOCK_FLAGS_NONE, 0},
        },
        .cal_block =
        {
            {0x0008000, 0x0F8000,   ORDER_0 | MEMBLOCK_FLAGS_NONE, 0},
            {0, 0, ORDER_1 | MEMBLOCK_FLAGS_NONE, 0},
        },
    },
    
    //TODOQ: [9] ME7XX
    
    //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    [10] =  //T42                                                       //0x0A
    {
        .index = 10,
        .ecm_id = 0x7E2, .reponse_ecm_id = 0x7EA,
        .unlock_flags = UNLOCK_FLAG_NONE,
        .upload_unlock_id = 371,
        .download_unlock_id = 371,
        .checksum_id = ChecksumId_None,
        .finalizing_id = FinalId_None,
        .flags = UTILITY_CHECK | UPLOAD_BY_CMD_23 | UPLOAD_ADDR_TYPE_3B |
            USE_ERASE_COMMAND | USE_DOWNLOAD_EXECUTE | COOLDOWNTIME_MEDIUM |
            DOWNLOAD_REQUEST_TYPE_3B | DOWNLOAD_ADDR_TYPE_3B,
        .extended_flags = UTILITY_CHECK_LEGACY | REQUIRE_KEYOFF_POWERDOWN_15S,
        .commtype = CommType_CAN, .commlevel = CommLevel_GMLAN,
        .eraseall_cmd = {EraseCmdInfoFlags_None,EraseCmdInfoCount1,{0xEE}},
        .erasecal_cmd = {EraseCmdInfoFlags_None,EraseCmdInfoCount1,{0xEA}},
        .testerpresent_frequency = 1,
        
        .data_offset = 0x0,
        .ecm_startingaddr = 0x0,

        .util_upload = {(utility_info*)&utilityinfolist[14],NULL,NULL},
        .util_dnload = {(utility_info*)&utilityinfolist[15],NULL,NULL},
        .os_test_method = 0,
        
        .maxuploadblocksize = 0x800, .maxdownloadblocksize = 0x200,
        .os_block_read_skipblock_info = NULL,
        .os_block_read =
        {
            {0x00000,   0x100000,   ORDER_0 | MEMBLOCK_FLAGS_NONE, 0},
            {0, 0, ORDER_1 | MEMBLOCK_FLAGS_NONE, 0},
            {0, 0, ORDER_2 | MEMBLOCK_FLAGS_NONE, 0},
            {0, 0, ORDER_3 | MEMBLOCK_FLAGS_NONE, 0},
            {0, 0, ORDER_4 | MEMBLOCK_FLAGS_NONE, 0},
        },
        .os_block =
        {
            {0x0020000, 0x020000,   ORDER_0 | MEMBLOCK_FLAGS_NONE, 0},
            {0, 0, ORDER_1 | MEMBLOCK_FLAGS_NONE, 0},
            {0, 0, ORDER_2 | MEMBLOCK_FLAGS_NONE, 0},
            {0, 0, ORDER_3 | MEMBLOCK_FLAGS_NONE, 0},
            {0, 0, ORDER_4 | MEMBLOCK_FLAGS_NONE, 0},
        },
        .cal_block =
        {
            {0x0020000, 0x020000,   ORDER_0 | MEMBLOCK_FLAGS_NONE, 0},
            {0, 0, ORDER_1 | MEMBLOCK_FLAGS_NONE, 0},
        },
    },
    //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    [11] =  //T43                                                       //0x0B
    {       //MPC561/562 ST M58BW016xB (2MB)
        .index = 11,
        .ecm_id = 0x7E2, .reponse_ecm_id = 0x7EA,
        .unlock_flags = UNLOCK_FLAG_NONE,
        .upload_unlock_id = 388,
        .download_unlock_id = 388,
        .checksum_id = ChecksumId_None,
        .finalizing_id = FinalId_None,
        .flags = UTILITY_CHECK | UPLOAD_BY_CMD_23 | UPLOAD_ADDR_TYPE_3B |
            USE_ERASE_COMMAND | USE_DOWNLOAD_EXECUTE | COOLDOWNTIME_SHORT |
            DOWNLOAD_REQUEST_TYPE_3B | DOWNLOAD_ADDR_TYPE_3B,            
        .extended_flags = UTILITY_CHECK_LEGACY | REQUIRE_DOWNLOAD_FINALIZING | 
            REQUIRE_KEYOFF_POWERDOWN_15S,
        .commtype = CommType_CAN, .commlevel = CommLevel_GMLAN,
        .eraseall_cmd = {EraseCmdInfoFlags_None,EraseCmdInfoCount1,{0xEE}},
        .erasecal_cmd = {EraseCmdInfoFlags_None,EraseCmdInfoCount1,{0xEA}},
        .testerpresent_frequency = 1,
        
        .data_offset = 0x0,
        .ecm_startingaddr = 0x0,

        .util_upload = {(utility_info*)&utilityinfolist[16],NULL,NULL},
        .util_dnload = {(utility_info*)&utilityinfolist[17],NULL,NULL},
        //IMPORTANT NOTE: request to execute utility has response (7EA 01 76)
        .os_test_method = 0,
        
        .maxuploadblocksize = 0x800, .maxdownloadblocksize = 0x200,
        .os_block_read_skipblock_info = NULL,
        .os_block_read =
        {
            {0x00000,   0x200000,   ORDER_0 | MEMBLOCK_FLAGS_NONE, 0},
            {0, 0, ORDER_1 | MEMBLOCK_FLAGS_NONE, 0},
            {0, 0, ORDER_2 | MEMBLOCK_FLAGS_NONE, 0},
            {0, 0, ORDER_3 | MEMBLOCK_FLAGS_NONE, 0},
            {0, 0, ORDER_4 | MEMBLOCK_FLAGS_NONE, 0},
        },
        .os_block =
        {
            {0x01C0000, 0x03FFF8,   ORDER_0 | MEMBLOCK_FLAGS_NONE, 0},
            {0, 0, ORDER_1 | MEMBLOCK_FLAGS_NONE, 0},
            {0, 0, ORDER_2 | MEMBLOCK_FLAGS_NONE, 0},
            {0, 0, ORDER_3 | MEMBLOCK_FLAGS_NONE, 0},
            {0, 0, ORDER_4 | MEMBLOCK_FLAGS_NONE, 0},
        },
        .cal_block =
        {
            {0x01C0000, 0x03FFF8,   ORDER_0 | MEMBLOCK_FLAGS_NONE, 0},
            {0, 0, ORDER_1 | MEMBLOCK_FLAGS_NONE, 0},
        },
    },
    //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    [12] =  //ALLI                                                      //0x0C
    {
        .index = 12,
        .ecm_id = 0x7E2, .reponse_ecm_id = 0x7EA,
        .unlock_flags = UNLOCK_FLAG_NONE,
        .upload_unlock_id = 404,
        .download_unlock_id = 404,
        .checksum_id = ChecksumId_None,
        .finalizing_id = FinalId_None,
        .flags = UPLOAD_BY_CMD_20_EXTENDED_ID | UPLOAD_ADDR_TYPE_3B |
            COOLDOWNTIME_MEDIUM |
            DOWNLOAD_REQUEST_TYPE_3B | DOWNLOAD_ADDR_TYPE_4B,
        .extended_flags = REQUIRE_KEYOFF_POWERDOWN_15S,
        .commtype = CommType_CAN, .commlevel = CommLevel_GMLAN,
        .eraseall_cmd = {EraseCmdInfoFlags_None,EraseCmdInfoCount1,{0x00}},
        .erasecal_cmd = {EraseCmdInfoFlags_None,EraseCmdInfoCount1,{0xEE}},
        .testerpresent_frequency = 1,
        
        .data_offset = 0x0,
        .ecm_startingaddr = 0x0,

        .util_upload = {NULL,NULL,NULL},
        .util_dnload = {NULL,NULL,NULL},
        .os_test_method = 0,

        .maxuploadblocksize = 0x200, .maxdownloadblocksize = 0x200,
        .os_block_read_skipblock_info = NULL,
        .os_block_read =
        {
            {0x008000,  0x028600,   ORDER_0 | MEMBLOCK_FLAGS_NONE, 0},
            {0, 0, ORDER_1 | MEMBLOCK_FLAGS_NONE, 0},
            {0, 0, ORDER_2 | MEMBLOCK_FLAGS_NONE, 0},
            {0, 0, ORDER_3 | MEMBLOCK_FLAGS_NONE, 0},
            {0, 0, ORDER_4 | MEMBLOCK_FLAGS_NONE, 0},
        },
        .os_block =
        {
            {0x010000,  0x020000,   ORDER_0 | MEMBLOCK_FLAGS_NONE, 0},
            {0, 0, ORDER_1 | MEMBLOCK_FLAGS_NONE, 0},
            {0, 0, ORDER_2 | MEMBLOCK_FLAGS_NONE, 0},
            {0, 0, ORDER_3 | MEMBLOCK_FLAGS_NONE, 0},
            {0, 0, ORDER_4 | MEMBLOCK_FLAGS_NONE, 0},
        },
        .cal_block =
        {
            {0x010000,  0x020000,   ORDER_0 | MEMBLOCK_FLAGS_NONE, 0},
            {0, 0, ORDER_1 | MEMBLOCK_FLAGS_NONE, 0},
        },
    },
    //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    [13] =  //T43A                                                      //0x0D
    {
        .index = 13,
        .ecm_id = 0x7E2, .reponse_ecm_id = 0x7EA,
        .unlock_flags = UNLOCK_FLAG_NONE,
        .upload_unlock_id = 388,
        .download_unlock_id = 388,
        .checksum_id = ChecksumId_None,
        .finalizing_id = FinalId_None,
        .flags = UTILITY_CHECK | UPLOAD_BY_CMD_23 | UPLOAD_ADDR_TYPE_3B |
            USE_ERASE_COMMAND | USE_DOWNLOAD_EXECUTE | COOLDOWNTIME_SHORT |
            DOWNLOAD_REQUEST_TYPE_3B | DOWNLOAD_ADDR_TYPE_3B,            
        .extended_flags = UTILITY_CHECK_LEGACY | REQUIRE_KEYOFF_POWERDOWN_15S,
        .commtype = CommType_CAN, .commlevel = CommLevel_GMLAN,
        .eraseall_cmd = {EraseCmdInfoFlags_None,EraseCmdInfoCount1,{0xEE}},
        .erasecal_cmd = {EraseCmdInfoFlags_None,EraseCmdInfoCount1,{0xEA}},
        .testerpresent_frequency = 1,
        
        .data_offset = 0x0,
        .ecm_startingaddr = 0x0,

        .util_upload = {(utility_info*)&utilityinfolist[16],NULL,NULL},
        .util_dnload = {(utility_info*)&utilityinfolist[17],NULL,NULL},
        //IMPORTANT NOTE: request to execute utility has response (7EA 01 76)
        .os_test_method = 0,
        
        .maxuploadblocksize = 0x800, .maxdownloadblocksize = 0x200,
        .os_block_read_skipblock_info = NULL,
        .os_block_read =
        {
            {0x00000,   0x200000,   ORDER_0 | MEMBLOCK_FLAGS_NONE, 0},
            {0, 0, ORDER_1 | MEMBLOCK_FLAGS_NONE, 0},
            {0, 0, ORDER_2 | MEMBLOCK_FLAGS_NONE, 0},
            {0, 0, ORDER_3 | MEMBLOCK_FLAGS_NONE, 0},
            {0, 0, ORDER_4 | MEMBLOCK_FLAGS_NONE, 0},
        },
        .os_block =
        {
            {0x01C0000, 0x040000,   ORDER_0 | MEMBLOCK_FLAGS_NONE, 0},
            {0, 0, ORDER_1 | MEMBLOCK_FLAGS_NONE, 0},
            {0, 0, ORDER_2 | MEMBLOCK_FLAGS_NONE, 0},
            {0, 0, ORDER_3 | MEMBLOCK_FLAGS_NONE, 0},
            {0, 0, ORDER_4 | MEMBLOCK_FLAGS_NONE, 0},
        },
        .cal_block =
        {
            {0x01C0000, 0x040000,   ORDER_0 | MEMBLOCK_FLAGS_NONE, 0},
            {0, 0, ORDER_1 | MEMBLOCK_FLAGS_NONE, 0},
        },
    },
    //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    [14] =  //E37                                                       //0x0E
    {
        .index = 14,
        .ecm_id = 0x7E0, .reponse_ecm_id = 0x7E8,
        .unlock_flags = UNLOCK_FLAG_NONE,
        .upload_unlock_id = 512,
        .download_unlock_id = 512,
        .checksum_id = ChecksumId_None,
        .finalizing_id = FinalId_None,
        .flags = UTILITY_CHECK | UPLOAD_BY_CMD_23 | UPLOAD_ADDR_TYPE_3B |
            USE_ERASE_COMMAND | USE_DOWNLOAD_EXECUTE | COOLDOWNTIME_MEDIUM |
            DOWNLOAD_REQUEST_TYPE_3B | DOWNLOAD_ADDR_TYPE_3B | DOWNLOAD_ALL,
        .extended_flags = UTILITY_CHECK_LEGACY | REQUIRE_KEYOFF_POWERDOWN_15S,
        .commtype = CommType_CAN, .commlevel = CommLevel_GMLAN,
        .eraseall_cmd = {EraseCmdInfoFlags_None,EraseCmdInfoCount1,{0xEE}},
        .erasecal_cmd = {EraseCmdInfoFlags_None,EraseCmdInfoCount1,{0xEA}},
        .testerpresent_frequency = 1,

        .data_offset = 0x0,
        .ecm_startingaddr = 0x0,

        .util_upload = {(utility_info*)&utilityinfolist[18],NULL,NULL},
        .util_dnload = {(utility_info*)&utilityinfolist[19],NULL,NULL},
        .os_test_method = 0,
        
        .maxuploadblocksize = 0x800, .maxdownloadblocksize = 0x200,
        .os_block_read_skipblock_info = NULL,
        .os_block_read =
        {
            {0x00000,   0x1C0000,   ORDER_0 | MEMBLOCK_FLAGS_NONE, 0},
            {0x1C0000,  0x40000,    ORDER_1 | MEMBLOCK_FLAGS_NONE, 0},
            {0, 0, ORDER_2 | MEMBLOCK_FLAGS_NONE, 0},
            {0, 0, ORDER_3 | MEMBLOCK_FLAGS_NONE, 0},
            {0, 0, ORDER_4 | MEMBLOCK_FLAGS_NONE, 0},
        },
        .os_block =
        {
            {0xE000,    0x1B2000,   ORDER_0 | MEMBLOCK_FLAGS_NONE, 0},
            {0x1C0000,  0x40000,    ORDER_1 | MEMBLOCK_FLAGS_NONE, 0},
            {0, 0, ORDER_2 | MEMBLOCK_FLAGS_NONE, 0},
            {0, 0, ORDER_3 | MEMBLOCK_FLAGS_NONE, 0},
            {0, 0, ORDER_4 | MEMBLOCK_FLAGS_NONE, 0},
        },
        .cal_block =
        {
            {0x1C0000,  0x40000,    ORDER_0 | MEMBLOCK_FLAGS_NONE, 0},
            {0, 0, ORDER_1 | MEMBLOCK_FLAGS_NONE, 0},
        },
    },
    //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    [20] =  //E78                                                       //0x14
    {
        .index = 20,
        .ecm_id = 0x7E0, .reponse_ecm_id = 0x7E8,
        .unlock_flags = UNLOCK_FLAG_NONE,
        .upload_unlock_id = 475,
        .download_unlock_id = 475,
        .checksum_id = ChecksumId_None,
        .finalizing_id = FinalId_None,
        .flags = UTILITY_CHECK | UPLOAD_BY_CMD_23 | UPLOAD_ADDR_TYPE_3B |
            USE_ERASE_COMMAND | USE_DOWNLOAD_EXECUTE |
            DOWNLOAD_REQUEST_TYPE_3B | DOWNLOAD_ADDR_TYPE_3B | DOWNLOAD_ALL,
        .extended_flags = REQUIRE_KEYOFF_POWERDOWN_15S,
        .commtype = CommType_CAN, .commlevel = CommLevel_GMLAN,
        .eraseall_cmd = {EraseCmdInfoFlags_None,EraseCmdInfoCount1,{0xEE}},
        .erasecal_cmd = {EraseCmdInfoFlags_None,EraseCmdInfoCount1,{0xEA}},
        .testerpresent_frequency = 1,

        .data_offset = 0x0,
        .ecm_startingaddr = 0x0,

        .util_upload = {(utility_info*)&utilityinfolist[20],NULL,NULL},
        .util_dnload = {(utility_info*)&utilityinfolist[21],NULL,NULL},
        .os_test_method = 0,

        .maxuploadblocksize = 0x800, .maxdownloadblocksize = 0x800,        
        .os_block_read_skipblock_info = NULL, // Utility handles block skipping
        .os_block_read =
        {
            {0x000000,  0x300000,   ORDER_0 | MEMBLOCK_FLAGS_NONE, 0},
            {0, 0, ORDER_1 | MEMBLOCK_FLAGS_NONE, 0},
            {0, 0, ORDER_2 | MEMBLOCK_FLAGS_NONE, 0},
            {0, 0, ORDER_3 | MEMBLOCK_FLAGS_NONE, 0},
            {0, 0, ORDER_4 | MEMBLOCK_FLAGS_NONE, 0},
        },
        .os_block =
        {
            {0x20000,       0x20000,   ORDER_0 | MEMBLOCK_FLAGS_NONE, 0},
            {0x40000,       0x40000,   ORDER_1 | MEMBLOCK_FLAGS_NONE, 0},
            {0x80000,       0x280000,  ORDER_2 | MEMBLOCK_FLAGS_NONE, 0},
            {0, 0, ORDER_3 | MEMBLOCK_FLAGS_NONE, 0},
            {0, 0, ORDER_4 | MEMBLOCK_FLAGS_NONE, 0},
        },
        .cal_block =
        {
            {0x20000,       0x20000,    ORDER_0 | MEMBLOCK_FLAGS_NONE, 0},
            {0x40000,       0x40000,    ORDER_1 | MEMBLOCK_FLAGS_NONE, 0},
            {0, 0, ORDER_2 | MEMBLOCK_FLAGS_NONE, 0},
            {0, 0, ORDER_3 | MEMBLOCK_FLAGS_NONE, 0},
            {0, 0, ORDER_4 | MEMBLOCK_FLAGS_NONE, 0},
        },
    },
    //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    [21] =  //E39-1                                                     //0x15
    {
        .index = 21,
        .ecm_id = 0x7E0, .reponse_ecm_id = 0x7E8,
        .unlock_flags = UNLOCK_FLAG_NONE,
        .upload_unlock_id = 513,
        .download_unlock_id = 513,
        .checksum_id = ChecksumId_None,
        .finalizing_id = FinalId_None,
        .flags = UTILITY_CHECK | UPLOAD_BY_CMD_23 | UPLOAD_ADDR_TYPE_3B |
            USE_ERASE_COMMAND | USE_DOWNLOAD_EXECUTE | 
            DOWNLOAD_REQUEST_TYPE_3B | DOWNLOAD_ADDR_TYPE_3B | DOWNLOAD_ALL,
        .extended_flags = REQUIRE_KEYOFF_POWERDOWN_15S,
        .commtype = CommType_CAN, .commlevel = CommLevel_GMLAN,
        .eraseall_cmd = {EraseCmdInfoFlags_None,EraseCmdInfoCount1,{0xEE}},
        .erasecal_cmd = {EraseCmdInfoFlags_None,EraseCmdInfoCount1,{0xEA}},
        .testerpresent_frequency = 1,

        .data_offset = 0x0,
        .ecm_startingaddr = 0x0,

        .util_upload = {(utility_info*)&utilityinfolist[22],NULL,NULL},
        .util_dnload = {(utility_info*)&utilityinfolist[23],NULL,NULL},
        .os_test_method = 0,
        
        .maxuploadblocksize = 0x800, .maxdownloadblocksize = 0x800,
        .os_block_read_skipblock_info = NULL, // Utility handles block skipping
        .os_block_read =
        {
            {0x000000,  0x300000,   ORDER_0 | MEMBLOCK_FLAGS_NONE, 0},
            {0, 0, ORDER_1 | MEMBLOCK_FLAGS_NONE, 0},
            {0, 0, ORDER_2 | MEMBLOCK_FLAGS_NONE, 0},
            {0, 0, ORDER_3 | MEMBLOCK_FLAGS_NONE, 0},
            {0, 0, ORDER_4 | MEMBLOCK_FLAGS_NONE, 0},
        },
        .os_block =
        {
            {0x20000,       0x20000,   ORDER_0 | MEMBLOCK_FLAGS_NONE, 0},
            {0x40000,       0x40000,   ORDER_1 | MEMBLOCK_FLAGS_NONE, 0},
            {0x80000,       0x280000,  ORDER_2 | MEMBLOCK_FLAGS_NONE, 0},
            {0, 0, ORDER_3 | MEMBLOCK_FLAGS_NONE, 0},
            {0, 0, ORDER_4 | MEMBLOCK_FLAGS_NONE, 0},
        },
        .cal_block =
        {
            {0x20000,       0x20000,    ORDER_0 | MEMBLOCK_FLAGS_NONE, 0},
            {0x40000,       0x40000,    ORDER_1 | MEMBLOCK_FLAGS_NONE, 0},
            {0, 0, ORDER_2 | MEMBLOCK_FLAGS_NONE, 0},
            {0, 0, ORDER_3 | MEMBLOCK_FLAGS_NONE, 0},
            {0, 0, ORDER_4 | MEMBLOCK_FLAGS_NONE, 0},
        },
    },
    //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    [22] =  //E39-2                                                     //0x16
    {
        .index = 22,
        .ecm_id = 0x7E0, .reponse_ecm_id = 0x7E8,
        .unlock_flags = UNLOCK_FLAG_NONE,
        .upload_unlock_id = 513,
        .download_unlock_id = 513,
        .checksum_id = ChecksumId_None,
        .finalizing_id = FinalId_None,
        .flags = UTILITY_CHECK | UPLOAD_BY_CMD_23 | UPLOAD_ADDR_TYPE_3B |
            USE_ERASE_COMMAND | USE_DOWNLOAD_EXECUTE | 
            DOWNLOAD_REQUEST_TYPE_3B | DOWNLOAD_ADDR_TYPE_3B | DOWNLOAD_ALL,
        .extended_flags = REQUIRE_KEYOFF_POWERDOWN_15S,
        .commtype = CommType_CAN, .commlevel = CommLevel_GMLAN,
        .eraseall_cmd = {EraseCmdInfoFlags_None,EraseCmdInfoCount1,{0xEE}},
        .erasecal_cmd = {EraseCmdInfoFlags_None,EraseCmdInfoCount1,{0xEA}},
        .testerpresent_frequency = 1,

        .data_offset = 0x0,
        .ecm_startingaddr = 0x0,

        .util_upload = {(utility_info*)&utilityinfolist[22],NULL,NULL},
        .util_dnload = {(utility_info*)&utilityinfolist[23],NULL,NULL},
        .os_test_method = 0,
        
        .maxuploadblocksize = 0x800, .maxdownloadblocksize = 0x800,
        .os_block_read_skipblock_info = NULL, // Utility handles block skipping
        .os_block_read =
        {
            {0x000000,  0x300000,   ORDER_0 | MEMBLOCK_FLAGS_NONE, 0},
            {0, 0, ORDER_1 | MEMBLOCK_FLAGS_NONE, 0},
            {0, 0, ORDER_2 | MEMBLOCK_FLAGS_NONE, 0},
            {0, 0, ORDER_3 | MEMBLOCK_FLAGS_NONE, 0},
            {0, 0, ORDER_4 | MEMBLOCK_FLAGS_NONE, 0},
        },
        .os_block =
        {
            {0x20000,       0x20000,   ORDER_0 | MEMBLOCK_FLAGS_NONE, 0},
            {0x40000,       0x40000,   ORDER_1 | MEMBLOCK_FLAGS_NONE, 0},
            {0x80000,       0x280000,  ORDER_2 | MEMBLOCK_FLAGS_NONE, 0},
            {0, 0, ORDER_3 | MEMBLOCK_FLAGS_NONE, 0},
            {0, 0, ORDER_4 | MEMBLOCK_FLAGS_NONE, 0},
        },
        .cal_block =
        {
            {0x20000,       0x20000,    ORDER_0 | MEMBLOCK_FLAGS_NONE, 0},
            {0x40000,       0x40000,    ORDER_1 | MEMBLOCK_FLAGS_NONE, 0},
            {0, 0, ORDER_2 | MEMBLOCK_FLAGS_NONE, 0},
            {0, 0, ORDER_3 | MEMBLOCK_FLAGS_NONE, 0},
            {0, 0, ORDER_4 | MEMBLOCK_FLAGS_NONE, 0},
        },
    },
    //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    [24] =  //E83A (NOT SUPPOERTED YET)                                 //0x18
    {
        .index = 24,
        .ecm_id = 0x7E0, .reponse_ecm_id = 0x7E8,
        .unlock_flags = UNLOCK_FLAG_NONE,
        .upload_unlock_id = 478,
        .download_unlock_id = 478,
        .checksum_id = ChecksumId_None,
        .finalizing_id = FinalId_None,
        .flags = UPLOAD_BY_CMD_23 | UPLOAD_ADDR_TYPE_3B |
            USE_ERASE_COMMAND | USE_DOWNLOAD_EXECUTE | 
            DOWNLOAD_REQUEST_TYPE_3B | DOWNLOAD_ADDR_TYPE_3B | DOWNLOAD_ALL,
        .extended_flags = REQUIRE_KEYOFF_POWERDOWN_15S,
        .commtype = CommType_CAN, .commlevel = CommLevel_GMLAN,
        .eraseall_cmd = {EraseCmdInfoFlags_None,EraseCmdInfoCount1,{0xEE}},
        .erasecal_cmd = {EraseCmdInfoFlags_None,EraseCmdInfoCount1,{0xEA}},
        .testerpresent_frequency = 0,

        .data_offset = 0x0,
        .ecm_startingaddr = 0x0,

        .util_upload = {(utility_info*)&utilityinfolist[30],NULL,NULL},
        .util_dnload = {(utility_info*)&utilityinfolist[32],NULL,NULL},
        .os_test_method = 0,
        
        .maxuploadblocksize = 0x800, .maxdownloadblocksize = 0x800,
        .os_block_read_skipblock_info = NULL,
        .os_block_read =
        {
            {0x000000,  0x200000,   ORDER_0 | MEMBLOCK_FLAGS_NONE, 0},
            {0, 0, ORDER_1 | MEMBLOCK_FLAGS_NONE, 0},
            {0, 0, ORDER_2 | MEMBLOCK_FLAGS_NONE, 0},
            {0, 0, ORDER_3 | MEMBLOCK_FLAGS_NONE, 0},
            {0, 0, ORDER_4 | MEMBLOCK_FLAGS_NONE, 0},
        },
        .os_block =
        {
            {0x030000,       0x00C000,  ORDER_0 | MEMBLOCK_FLAGS_NONE, 0},
            {0x03D800,       0x035800,  ORDER_1 | MEMBLOCK_FLAGS_NONE, 0},
            {0x07F800,       0x0007D8,  ORDER_1 | MEMBLOCK_FLAGS_NONE, 0},
            {0, 0, ORDER_1 | MEMBLOCK_FLAGS_NONE, 0},
            {0, 0, ORDER_1 | MEMBLOCK_FLAGS_NONE, 0},
        },
        .cal_block =
        {
            {0x030000,       0x00C000,  ORDER_0 | MEMBLOCK_FLAGS_NONE, 0},
            {0x03D800,       0x035800,  ORDER_1 | MEMBLOCK_FLAGS_NONE, 0},
            {0x07F800,       0x000800,  ORDER_1 | MEMBLOCK_FLAGS_NONE, 0},
            {0, 0, ORDER_1 | MEMBLOCK_FLAGS_NONE, 0},
            {0, 0, ORDER_1 | MEMBLOCK_FLAGS_NONE, 0},
        },
    },    
    //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    [25] =  //E83B (NOT SUPPOERTED YET)                                 //0x19
    {
        .index = 25,
        .ecm_id = 0x7E0, .reponse_ecm_id = 0x7E8,
        .unlock_flags = UNLOCK_FLAG_NONE,
        .upload_unlock_id = 478,
        .download_unlock_id = 478,
        .checksum_id = ChecksumId_None,
        .finalizing_id = FinalId_None,
        .flags = UPLOAD_BY_CMD_23 | UPLOAD_ADDR_TYPE_3B |
            USE_ERASE_COMMAND | USE_DOWNLOAD_EXECUTE | 
            DOWNLOAD_REQUEST_TYPE_3B | DOWNLOAD_ADDR_TYPE_3B | DOWNLOAD_ALL,
        .extended_flags = REQUIRE_KEYOFF_POWERDOWN_15S,
        .commtype = CommType_CAN, .commlevel = CommLevel_GMLAN,
        .eraseall_cmd = {EraseCmdInfoFlags_None,EraseCmdInfoCount1,{0xEE}},
        .erasecal_cmd = {EraseCmdInfoFlags_None,EraseCmdInfoCount1,{0xEA}},
        .testerpresent_frequency = 1,

        .data_offset = 0x0,
        .ecm_startingaddr = 0x0,

        .util_upload = {(utility_info*)&utilityinfolist[30],NULL,NULL},
        .util_dnload = {(utility_info*)&utilityinfolist[31],NULL,NULL},
        .os_test_method = 0,
        
        .maxuploadblocksize = 0x800, .maxdownloadblocksize = 0x800,
        .os_block_read_skipblock_info = NULL,
        .os_block_read =
        {
            {0x000000,  0x200000,   ORDER_0 | MEMBLOCK_FLAGS_NONE, 0},
            {0, 0, ORDER_1 | MEMBLOCK_FLAGS_NONE, 0},
            {0, 0, ORDER_2 | MEMBLOCK_FLAGS_NONE, 0},
            {0, 0, ORDER_3 | MEMBLOCK_FLAGS_NONE, 0},
            {0, 0, ORDER_4 | MEMBLOCK_FLAGS_NONE, 0},
        },
        .os_block =
        {
            {0x010000,       0x1B0000,  ORDER_0 | MEMBLOCK_FLAGS_NONE, 0},
            {0x1C0000,       0x03C800,  ORDER_1 | MEMBLOCK_FLAGS_NONE, 0},
            {0x1FF800,       0x000800,  ORDER_1 | MEMBLOCK_FLAGS_NONE, 0},
            {0, 0, ORDER_1 | MEMBLOCK_FLAGS_NONE, 0},
            {0, 0, ORDER_1 | MEMBLOCK_FLAGS_NONE, 0},
        },
        .cal_block =
        {
            {0x1C0000,       0x03C800,  ORDER_0 | MEMBLOCK_FLAGS_NONE, 0},
            {0x1FF800,       0x000800,  ORDER_1 | MEMBLOCK_FLAGS_NONE, 0},
            {0, 0, ORDER_1 | MEMBLOCK_FLAGS_NONE, 0},
            {0, 0, ORDER_1 | MEMBLOCK_FLAGS_NONE, 0},
            {0, 0, ORDER_1 | MEMBLOCK_FLAGS_NONE, 0},
        },
    },
    //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    [27] =  //T76                                                       //0x1B
    {
        .index = 27,
        .ecm_id = 0x7E2, .reponse_ecm_id = 0x7EA,
        .unlock_flags = UNLOCK_FLAG_NONE,
        .upload_unlock_id = 514,
        .download_unlock_id = 514,
        .checksum_id = ChecksumId_None,
        .finalizing_id = FinalId_None,
        .flags = UTILITY_CHECK | UPLOAD_BY_CMD_23 | UPLOAD_ADDR_TYPE_3B |
            USE_ERASE_COMMAND | USE_DOWNLOAD_EXECUTE | 
            DOWNLOAD_REQUEST_TYPE_3B | DOWNLOAD_ADDR_TYPE_3B | DOWNLOAD_ALL,
        .extended_flags = REQUIRE_KEYOFF_POWERDOWN_15S,
        .commtype = CommType_CAN, .commlevel = CommLevel_GMLAN,
        .eraseall_cmd = {EraseCmdInfoFlags_None,EraseCmdInfoCount1,{0xEE}},
        .erasecal_cmd = {EraseCmdInfoFlags_None,EraseCmdInfoCount1,{0xEA}},
        .testerpresent_frequency = 1,

        .data_offset = 0x0,
        .ecm_startingaddr = 0x0,

        .util_upload = {(utility_info*)&utilityinfolist[24],NULL,NULL},
        .util_dnload = {(utility_info*)&utilityinfolist[25],NULL,NULL},
        .os_test_method = 0,
        
        .maxuploadblocksize = 0x800, .maxdownloadblocksize = 0x800,
        .os_block_read_skipblock_info = NULL,
        .os_block_read =
        {
            {0x000000,  0x200000,   ORDER_0 | MEMBLOCK_FLAGS_NONE, 0},
            {0, 0, ORDER_1 | MEMBLOCK_FLAGS_NONE, 0},
            {0, 0, ORDER_2 | MEMBLOCK_FLAGS_NONE, 0},
            {0, 0, ORDER_3 | MEMBLOCK_FLAGS_NONE, 0},
            {0, 0, ORDER_4 | MEMBLOCK_FLAGS_NONE, 0},
        },
        .os_block =
        {
            {0x1C0000,      0x40000,    ORDER_0 | MEMBLOCK_FLAGS_NONE, 0},
            {0, 0, ORDER_1 | MEMBLOCK_FLAGS_NONE, 0},
            {0, 0, ORDER_2 | MEMBLOCK_FLAGS_NONE, 0},
            {0, 0, ORDER_3 | MEMBLOCK_FLAGS_NONE, 0},
            {0, 0, ORDER_4 | MEMBLOCK_FLAGS_NONE, 0},
        },
        .cal_block =
        {
            {0x1C0000,      0x40000,    ORDER_0 | MEMBLOCK_FLAGS_NONE, 0},
            {0, 0, ORDER_1 | MEMBLOCK_FLAGS_NONE, 0},
            {0, 0, ORDER_2 | MEMBLOCK_FLAGS_NONE, 0},
            {0, 0, ORDER_3 | MEMBLOCK_FLAGS_NONE, 0},
            {0, 0, ORDER_4 | MEMBLOCK_FLAGS_NONE, 0},
        },
    },
    //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    [28] =  //E92                                                       //0x1C
    {
        .index = 28,
        .ecm_id = 0x7E0, .reponse_ecm_id = 0x7E8,
        .unlock_flags = UNLOCK_FLAG_NONE,
        .upload_unlock_id = 515,
        .download_unlock_id = 515,
        .checksum_id = ChecksumId_None,
        .finalizing_id = FinalId_None,
        .flags = UTILITY_CHECK | UPLOAD_BY_CMD_23 | UPLOAD_ADDR_TYPE_3B |
            USE_ERASE_COMMAND | USE_DOWNLOAD_EXECUTE | 
            DOWNLOAD_REQUEST_TYPE_3B | DOWNLOAD_ADDR_TYPE_3B | DOWNLOAD_ALL,
        .extended_flags = REQUIRE_KEYOFF_POWERDOWN_15S,
        .commtype = CommType_CAN, .commlevel = CommLevel_GMLAN,
        .eraseall_cmd = {EraseCmdInfoFlags_None,EraseCmdInfoCount1,{0xEE}},
        .erasecal_cmd = {EraseCmdInfoFlags_None,EraseCmdInfoCount1,{0xEA}},
        .testerpresent_frequency = 1,

        .data_offset = 0x0,
        .ecm_startingaddr = 0x0,

        .util_upload = {(utility_info*)&utilityinfolist[26],NULL,NULL},
        .util_dnload = {(utility_info*)&utilityinfolist[27],NULL,NULL},
        .os_test_method = 0,        

        .maxuploadblocksize = 0x800, .maxdownloadblocksize = 0x800,
        .os_block_read_skipblock_info = NULL, // Utility handles block skipping
        .os_block_read =
        {
            {0x000000,  0x400000,   ORDER_0 | MEMBLOCK_FLAGS_NONE, 0},
            {0, 0, ORDER_1 | MEMBLOCK_FLAGS_NONE, 0},
            {0, 0, ORDER_2 | MEMBLOCK_FLAGS_NONE, 0},
            {0, 0, ORDER_3 | MEMBLOCK_FLAGS_NONE, 0},
            {0, 0, ORDER_4 | MEMBLOCK_FLAGS_NONE, 0},
        },
        .os_block =
        {
            {0x40000,       0x40000,    ORDER_0 | MEMBLOCK_FLAGS_NONE, 0},
            {0x80000,       0x40000,    ORDER_1 | MEMBLOCK_FLAGS_NONE, 0},
            {0xC0000,       0x40000,    ORDER_2 | MEMBLOCK_FLAGS_NONE, 0},
            {0x100000,      0x300000,   ORDER_3 | MEMBLOCK_FLAGS_NONE, 0},
            {0, 0, ORDER_4 | MEMBLOCK_FLAGS_NONE, 0},
        },
        .cal_block =
        {
            {0x40000,       0x40000,    ORDER_0 | MEMBLOCK_FLAGS_NONE, 0},
            {0x80000,       0x40000,    ORDER_1 | MEMBLOCK_FLAGS_NONE, 0},
            {0, 0, ORDER_2 | MEMBLOCK_FLAGS_NONE, 0},
            {0, 0, ORDER_3 | MEMBLOCK_FLAGS_NONE, 0},
            {0, 0, ORDER_4 | MEMBLOCK_FLAGS_NONE, 0},
        },
    },
    //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    [29] =  //E39A                                                      //0x1D
    {
        .index = 29,
        .ecm_id = 0x7E0, .reponse_ecm_id = 0x7E8,
        .unlock_flags = UNLOCK_FLAG_NONE,
        .upload_unlock_id = 219,
        .download_unlock_id = 219,
        .checksum_id = ChecksumId_None,
        .finalizing_id = FinalId_None,
        .flags = UTILITY_CHECK | UPLOAD_BY_CMD_23 | UPLOAD_ADDR_TYPE_3B |
            USE_ERASE_COMMAND | USE_DOWNLOAD_EXECUTE | 
            DOWNLOAD_REQUEST_TYPE_3B | DOWNLOAD_ADDR_TYPE_3B | DOWNLOAD_ALL,
        .extended_flags = REQUIRE_KEYOFF_POWERDOWN_15S,
        .commtype = CommType_CAN, .commlevel = CommLevel_GMLAN,
        .eraseall_cmd = {EraseCmdInfoFlags_None,EraseCmdInfoCount1,{0xEE}},
        .erasecal_cmd = {EraseCmdInfoFlags_None,EraseCmdInfoCount1,{0xEA}},
        .testerpresent_frequency = 1,

        .data_offset = 0x0,
        .ecm_startingaddr = 0x0,

        .util_upload = {(utility_info*)&utilityinfolist[28],NULL,NULL},
        .util_dnload = {(utility_info*)&utilityinfolist[29],NULL,NULL},
        .os_test_method = 0,
        
        .maxuploadblocksize = 0x800, .maxdownloadblocksize = 0x800,
        .os_block_read_skipblock_info = NULL, // Utility handles block skipping
        .os_block_read =
        {
            {0x000000,  0x300000,   ORDER_0 | MEMBLOCK_FLAGS_NONE, 0},
            {0, 0, ORDER_1 | MEMBLOCK_FLAGS_NONE, 0},
            {0, 0, ORDER_2 | MEMBLOCK_FLAGS_NONE, 0},
            {0, 0, ORDER_3 | MEMBLOCK_FLAGS_NONE, 0},
            {0, 0, ORDER_4 | MEMBLOCK_FLAGS_NONE, 0},
        },
        .os_block =
        {
            {0x20000,       0x20000,   ORDER_0 | MEMBLOCK_FLAGS_NONE, 0},
            {0x40000,       0x40000,   ORDER_1 | MEMBLOCK_FLAGS_NONE, 0},
            {0x80000,       0x280000,  ORDER_2 | MEMBLOCK_FLAGS_NONE, 0},
            {0, 0, ORDER_3 | MEMBLOCK_FLAGS_NONE, 0},
            {0, 0, ORDER_4 | MEMBLOCK_FLAGS_NONE, 0},
        },
        .cal_block =
        {
            {0x20000,       0x20000,    ORDER_0 | MEMBLOCK_FLAGS_NONE, 0},
            {0x40000,       0x40000,    ORDER_1 | MEMBLOCK_FLAGS_NONE, 0},
            {0, 0, ORDER_2 | MEMBLOCK_FLAGS_NONE, 0},
            {0, 0, ORDER_3 | MEMBLOCK_FLAGS_NONE, 0},
            {0, 0, ORDER_4 | MEMBLOCK_FLAGS_NONE, 0},
        },
    },
    //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
};

//------------------------------------------------------------------------------
//Some processors have special blocks that are appended during upload, this
//function to get the size of those blocks
//------------------------------------------------------------------------------
//u32 GM_ECM_GetAddedBlockLength(u16 ecm_type, u8 index)
//{
//    u32 length;
//    u32 filenumber;
//    
//    length = 0;
//    switch(ECM_GetOSReadBlock(ecm_type,index)->flags & MEMBLOCK_FLAGS_TYPE_MASK)
//    {
//    case MEMBLOCK_FLAGS_APPEND_FILE_BY_INDEX:
//        filenumber = ECM_GetOSReadBlock(ecm_type,index)->flagdata;
//        length += IndexedAppendFiles[filenumber].filesize;
//        break;
//    case MEMBLOCK_FLAGS_SIMPLE_FF_FILL:
//    case MEMBLOCK_FLAGS_SIMPLE_00_FILL:
//        length += ECM_GetOSReadBlock(ecm_type,index)->flagdata;
//        break;
//    default:
//        break;
//    }
//    
//    return length;
//}
