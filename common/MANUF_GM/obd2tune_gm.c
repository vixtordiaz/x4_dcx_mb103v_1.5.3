/**
  **************** (C) COPYRIGHT 2011 SCT Performance, LLC *********************
  * File Name          : obd2tune_gm.c
  * Device / System    : SCT products using the "CommonCode" platform
  * Author             : Quyen Leba
  *
  * Version            : 1 
  * Date               : 05/02/2011
  * Description        : 
  *                    : 
  *
  *
  * History            : 05/02/2011 M. Davis
  *                    :   Add SCT Copyright header,
  *
  ******************************************************************************
  */

//------------------------------------------------------------------------------
// Log error point: 0x30xx -> 0x31xx
//------------------------------------------------------------------------------

#include <string.h>
#include <fs/genfs.h>
#include <common/file.h>
#include <common/statuscode.h>
#include <common/filestock.h>
#include <common/obd2can.h>
#include "obd2tune_gm.h"

//------------------------------------------------------------------------------
// Check if the custom tune is allowed
// Input:   u16 veh_type
//          u8  *vin
//          u8  *preference_filename (contains check info)
//          selected_tunelist_info *tuneinfo
// Outputs: u8  *matchmasked (from LSB, 1=matched,0=unmatch)
//          bool *isvinmasked (TRUE: VIN is masked)
// Return:  u8  status
// Engineer: Quyen Leba
// Note: only useful for custom tune
// Example content of a preference file (a VIN and supported OSes):
//1G1YY26W485130664
//12617631
//24243170
//------------------------------------------------------------------------------
u8 obd2tune_gm_check_customtune_support(u16 veh_type,  u8 *vin,
                                        u8 *preference_filename,
                                        ecm_info *ecminfo, u8 *matchmasked,
                                        bool *isvinmasked)
{
    customtune_tuneosdata tuneosdata;
    u8  fullfilename[128];
    bool isvinmatch;
    bool istunematch;
    bool tmpbool;
    u8  ecm_count;
    u8  mask_cmp;
    u8  mask_reference;
    u8  i;
    u8  status;

    //abandon these error codes: 0x3023

    ecm_count = VEH_GetEcmCount(veh_type);
    status = S_FAIL;
    if (preference_filename[0] == 0)
    {
        if (VEH_IsCustomTuneTuneOSNotRequired(veh_type))
        {
            status = S_NOTREQUIRED;
            goto obd2tune_gm_check_customtune_support_done;
        }
        else
        {
            status = S_ERROR;
            log_push_error_point(0x3020);
            goto obd2tune_gm_check_customtune_support_done;
        }
    }

    genfs_userpathcorrection(preference_filename, sizeof(fullfilename), 
                             fullfilename);
    if (strstr((char*)preference_filename,".tos"))
    {
        status = obd2tune_extracttuneosdata_tos(fullfilename,
                                                &tuneosdata);
    }
    else if (strstr((char*)preference_filename,".txt"))
    {
        status = obd2tune_extracttuneosdata_txt(fullfilename,
                                                &tuneosdata);
    }
    else
    {
        log_push_error_point(0x3021);
        return S_NOTSUPPORT;
    }
    if (status != S_SUCCESS)
    {
        log_push_error_point(0x3022);
        return status;
    }

    //check VIN
    *matchmasked = 0;
    *isvinmasked = FALSE;
    isvinmatch = TRUE;
    for(i=0;i<VIN_LENGTH;i++)
    {
        if (tuneosdata.vin[i] == '*')
        {
            *isvinmasked = TRUE;
        }
        else if (vin[i] != tuneosdata.vin[i])
        {
            isvinmatch = FALSE;
            break;
        }
    }

    mask_reference = mask_cmp = 0;
    for(i=0;i<ecm_count;i++)
    {
        mask_reference |= (u8)(1<<i);
    }
    
    for(i=0;i<ecm_count;i++)
    {
        //check ecminfo->codes[i][0] against tuneosdata.codes
        //then check ecminfo->second_codes[i][0] against tuneosdata.secondcodes
        tmpbool = FALSE;
        status = obd2tune_comparetunecode(tuneosdata.codes,
                                          ecminfo->codes[i][0],i);
        if (status == S_SUCCESS)
        {
            //TODOQK: Sep27,12: don't care about second codes in GM
//            if (ecminfo->second_codes[i][0][0] == 0)
//            {
//                status = obd2tune_comparetunecode(tuneosdata.secondcodes,
//                                                  "N/A",i);
//            }
//            else
//            {
//                status = obd2tune_comparetunecode(tuneosdata.secondcodes,
//                                                  ecminfo->second_codes[i][0],i);
//            }
            if (status == S_SUCCESS)
            {
                tmpbool = TRUE;
            }
        }

        if (tmpbool)
        {
            mask_cmp |= (u8)(1<<i);
        }
    }

    istunematch = FALSE;
    if (mask_cmp == mask_reference)
    {
        istunematch = TRUE;
    }
    status = S_SUCCESS;
    
obd2tune_gm_check_customtune_support_done:
    if (status == S_SUCCESS)
    {
        *matchmasked = mask_cmp;
        if (isvinmatch == FALSE)
        {
            return S_VINUNMATCH;
        }
        else if (istunematch == FALSE)
        {
            return S_TUNEUNMATCH;
        }
    }
    return status;
}

//------------------------------------------------------------------------------
// Compare Partnumbers in the settings with ones from the vehicle
// Inputs:  u8 *vehiclecodes_fromvehicle
//          u8 *secondvehiclecodes_fromvehicle
// Return:  u8  status
// Engineer: Patrick Downs
//------------------------------------------------------------------------------
u8 obd2tune_gm_compare_vehiclecodes_with_setting(flasher_info *flasherinfo)
{
    //TODOQK: check this
    return S_NOTREQUIRED;
}

//------------------------------------------------------------------------------
// Compare Partnumbers in the settings with ones from the vehicle
// Inputs:  u8 *vehiclecodes_fromvehicle
//          u8 *secondvehiclecodes_fromvehicle
// Return:  u8  status
// Engineer: Patrick Downs
//------------------------------------------------------------------------------
u8 obd2tune_gm_compare_vehiclecodes_with_tuneosdata(flasher_info *flasherinfo)
{
    //TODOQK: check this
    return S_NOTREQUIRED;
}

//------------------------------------------------------------------------------
// Compare VID, PATS, and security byte data with married vehicle
// Inputs:  u16 veh_type
//
// Return:  u8  status
// Engineer: Patrick Downs
//------------------------------------------------------------------------------
u8 obd2tune_gm_compare_overlaydata(u16 veh_type)
{
    return S_NOTREQUIRED;
}

//------------------------------------------------------------------------------
// Upload vehicle stock options
// Input:   flasher_info *f_info
// Return:  u8  status
// Engineer: Quyen Leba
//------------------------------------------------------------------------------
u8 obd2tune_gm_option_upload(flasher_info *f_info)
{
    //nothing for GM
    return S_SUCCESS;
}

//------------------------------------------------------------------------------
// Download vehicle stock options
// Input:   flasher_info *f_info
// Return:  u8  status
// Engineer: Quyen Leba
//------------------------------------------------------------------------------
u8 obd2tune_gm_option_download(flasher_info *f_info)
{
    //nothing for GM
    return S_SUCCESS;
}
