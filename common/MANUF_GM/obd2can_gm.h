/**
  **************** (C) COPYRIGHT 2011 SCT Performance, LLC *********************
  * File Name          : obd2can_gm.h
  * Device / System    : SCT products using the "CommonCode" platform
  * Author             : Quyen Leba
  *
  * Version            : 1 
  * Date               : 05/02/2011
  * Description        : 
  *                    : 
  *
  *
  * History            : 05/02/2011 M. Davis
  *                    :   Add SCT Copyright header,
  *
  ******************************************************************************
  */

#ifndef __OBD2CAN_GM_H
#define __OBD2CAN_GM_H

#include <arch/gentype.h>
#include <common/obd2.h>
#include <common/obd2def.h>
#include <common/obd2can.h>
#include <common/obd2datalog.h>
#include "obd2_gmdef.h"

//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
// GM funcions to support ECM communication
//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
u8 obd2can_gm_programmingmode(bool broadcast, u16 ecm_type, bool highspeedmode);
u8 obd2can_gm_readSoftwarePartNumber(u32 ecm_id, partnumber_info *partnumbers);
u8 obd2can_gm_readSerialNumber(u8 *serialnumber, u8 *bcc);
u8 obd2can_gm_findcommlevel(u32 ecm_id, VehicleCommLevel *commlevel);
u8 obd2can_gm_readvin(u8 *vin);

//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
// Functions from GMLAN
//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
u8 obd2can_gm_clear_diagnostic_information(bool broadcast, u32 ecm_id); //$04
u8 obd2can_gm_initiate_diagnostic_operation(bool broadcast, u32 ecm_id, //$10
                                            Obd2can_SubServiceNumber subservice);
u8 obd2can_gm_ecu_reset_request(bool broadcast,                         //$11
                                  u32 ecm_id,
                                  Obd2can_SubServiceNumber subservice,
                                  bool isreponse_expected);
u8 obd2can_gm_read_data_bypid(u32 ecm_id, u16 pid, u8 pidsize, u8 *output);
u8 obd2can_gm_read_data_by_identifier(bool broadcast, u32 ecm_id,
                                      u8 identifier, u8 *data, u16 *datalength);
u8 obd2can_gm_return_normal_mode(bool broadcast, u32 ecm_id);           //$20
u8 obd2can_gm_read_memory_address(u32 ecm_id, u32 address, u16 length,  //$23
                                  MemoryAddressType addrtype,
                                  u8 *data, u16 *datalength,
                                  bool force_readby4bytes);
u8 obd2can_gm_read_data_bydma(u32 ecm_id, u32 address, u16 length,      //$23
                              MemoryAddressType addrtype,
                              u8 *data, u16 *datalength,
                              bool response_expected);
u8 obd2can_gm_security_access(u32 ecm_id,                               //$27
                              Obd2can_SubServiceNumber subservice,
                              u8 *data, u8 *datalength,
                              u32 algoindex);
u8 obd2can_gm_disablenormalcommunication(bool broadcast, u32 ecm_id,    //$28
                                         bool expect_response);
u8 obd2can_gm_dynamicallydefinemessage(u32 ecm_id, u8 msg_id,           //$2C
                                       u16 *pidlist, u8 pidcount);
u8 obd2can_gm_definePIDByAddress(u32 ecm_id, u16 pid, u32 address,      //$2D
                                 u8 size);
u8 obd2can_gm_request_download(u32 ecm_id, u8 dataformat,               //$34
                               u32 requestaddress, u32 requestlength,
                               RequestLengthType reqtype,
                               VehicleCommLevel commlevel,
                               u16 *maxdownloadblocklength);
u8 obd2can_gm_request_upload(u32 ecm_id,                                //$35
                             u32 requestaddress, u32 requestlength,
                             RequestLengthType reqtype,
                             VehicleCommLevel commlevel,
                             u16 *maxuploadblocklength);
u8 obd2can_gm_transfer_data_upload(u32 ecm_id, u8 blocktracking,        //$36
                                   VehicleCommLevel commlevel,
                                   u32 request_address,
                                   RequestLengthType request_type,
                                   u8 *data, u16 *datalength);
u8 obd2can_gm_transfer_data_download(u32 ecm_id,                        //$36
                                     Obd2can_SubServiceNumber subservice,
                                     u8 blocktracking, u32 startingaddr,
                                     MemoryAddressType addrtype,
                                     u8 *data, u16 datalength,
                                     VehicleCommLevel commlevel,
                                     bool response_expected);
u8 obd2can_gm_request_transfer_exit(u32 ecm_id,                         //$37
                                    VehicleCommLevel commlevel);
u8 obd2can_gm_write_data_by_identifier(u32 ecm_id, u8 identifier,
                                       u8 *data, u16 datalength);
u8 obd2can_gm_tester_present(bool broadcast, u32 ecm_id);
u8 obd2can_gm_report_programmedstate(bool broadcast, u32 ecm_id,
                                     GMLAN_ProgrammedStateStatus *programmedstate);
u8 obd2can_gm_programmingmode(bool broadcast, u16 ecm_type, bool highspeedmode);
u8 obd2can_gm_read_diagnostic_information(u32 ecm_id, dtc_info *dtcinfo);
u8 obd2can_gm_read_data_by_packetid_request(u32 ecm_id, u8 requesttype,
                                            u8 *packetidlist, u8 packetidcount);
u8 obd2can_gm_downloadutility(u32 ecm_type, u8 utilitychoiceindex,
                              bool isdownloadutility);
u8 obd2can_gm_checkutility(u32 ecm_type, u8 utility_type);
u8 obd2can_gm_exitutility(u32 ecm_type);
u8 obd2can_gm_downloadfinalizing(u32 ecm_type);
u8 obd2can_gm_setup_prior_upload(u16 ecm_type);
u8 obd2can_gm_setup_after_upload(u16 ecm_type);
u8 obd2can_gm_setup_prior_download(u16 ecm_type);
u8 obd2can_gm_setup_after_download(u16 ecm_type);

u8 obd2can_gm_validate_oscpid(u32 ecm_id, DlxBlock *dlxblock,
                              VehicleCommLevel commlevel);
u8 obd2can_gm_validatedmr(u32 ecm_id, DlxBlock *dlxblock,
                          VehicleCommLevel commlevel);
u8 obd2can_gm_validateByRapidPacketSetup(u32 ecm_id, DlxBlock *dlxblock,
                          VehicleCommLevel commlevel);
u8 obd2can_gm_secondaryping(u32 ecm_id);
u8 obd2can_gm_disableallnormalcomm();
u8 obd2can_gm_device_control(u32 ecm_id, u8 cpid, u8 *controlbytes,     //$AE
                             u16 datalength, bool response_expected);
u8 obd2can_gm_getinfo(obd2_info_block *block);


u8 obd2can_gm_reset_ecm(u8 ecm_type);
u8 obd2can_gm_erase_ecm(u16 ecm_type, bool is_cal_only);
u8 obd2can_gm_clear_kam(u32 ecm_id, VehicleCommLevel commlevel);
void obd2can_gm_testerpresent(bool broadcast, u32 ecm_id,
                             VehicleCommLevel commlevel);
u8 obd2can_gm_readecminfo(ecm_info *ecm);
u8 obd2can_gm_cleardtc();


u8 obd2can_gm_read_memory_address_by20extendedid(u32 ecm_id,
                                                 u32 ecm_response_id,
                                                 u32 address, u32 length,
                                                 MemoryAddressType addrtype,
                                                 u8 *data, u16 *datalength);

#endif    //__OBD2CAN_GM_H
