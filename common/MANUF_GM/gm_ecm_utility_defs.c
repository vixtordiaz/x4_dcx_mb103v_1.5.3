/**
**************** (C) COPYRIGHT 2011 SCT Performance, LLC *********************
* File Name          : gm_ecm_utility_defs.c
* Device / System    : SCT products using the "CommonCode" platform
* Author             : Quyen Leba
*
* Version            : 1 
* Date               : 05/02/2011
* Description        : 
*                    : 
*
*
* History            : 05/02/2011 M. Davis
*                    :   Add SCT Copyright header,
*
******************************************************************************
*/

#include "gm_ecm_utility_defs.h"

//IMPORTANT: defined functions for utilityfunctest are in gm_ecm_utility_testfunc.c
//set utilityfunctest to ignore test and assume utility is applicable

//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
const utility_info utilityinfolist[] =
{
    [0] =
    {
        //"SCT_E40Ue.bin"
        "UBF110.ubf", UTILITY_FLAGS_USE_ENCRYPTION | UTILITY_FLAGS_SEPARATE_EXE,
        .call_address = 0xFF1000,
        .utilityfunctest = NULL,
        .utility_block =
        {
            {0xFF1000,  0x1800,     ORDER_0 | MEMBLOCK_FLAGS_AS_MAX_BLOCK_SIZE, 0},
            {0,         0,          ORDER_1 | MEMBLOCK_FLAGS_NONE, 0},
            {0,         0,          ORDER_2 | MEMBLOCK_FLAGS_NONE, 0},
            {0,         0,          ORDER_3 | MEMBLOCK_FLAGS_NONE, 0},
            {0,         0,          ORDER_4 | MEMBLOCK_FLAGS_NONE, 0},
        },
    },
    [1] =
    {
        //"SCT_E40D2e.bin"
        "UBF111.ubf", UTILITY_FLAGS_USE_ENCRYPTION | UTILITY_FLAGS_SEPARATE_EXE,
        .call_address = 0xFF1000,
        .utilityfunctest = NULL,
        .utility_block =
        {
            {0xFF1000,  0x1800,     ORDER_0 | MEMBLOCK_FLAGS_AS_MAX_BLOCK_SIZE, 0},
            {0,         0,          ORDER_1 | MEMBLOCK_FLAGS_NONE, 0},
            {0,         0,          ORDER_2 | MEMBLOCK_FLAGS_NONE, 0},
            {0,         0,          ORDER_3 | MEMBLOCK_FLAGS_NONE, 0},
            {0,         0,          ORDER_4 | MEMBLOCK_FLAGS_NONE, 0},
        },
    },
    [2] =
    {
        //"SCT_E67U_Qe.bin"
        "UBF114.ubf", UTILITY_FLAGS_USE_ENCRYPTION | UTILITY_FLAGS_SEPARATE_EXE,
        .call_address = 0x3FC400,
        .utilityfunctest = NULL,
        .utility_block =
        {
            {0x3FC400,  0x1800,     ORDER_0 | MEMBLOCK_FLAGS_AS_MAX_BLOCK_SIZE, 0},
            {0,         0,          ORDER_1 | MEMBLOCK_FLAGS_NONE, 0},
            {0,         0,          ORDER_2 | MEMBLOCK_FLAGS_NONE, 0},
            {0,         0,          ORDER_3 | MEMBLOCK_FLAGS_NONE, 0},
            {0,         0,          ORDER_4 | MEMBLOCK_FLAGS_NONE, 0},
        },
    },
    [3] =
    {
        //"SCT_E67D2e.bin"
        "UBF113.ubf", UTILITY_FLAGS_USE_ENCRYPTION | UTILITY_FLAGS_SEPARATE_EXE,
        .call_address = 0x3FC400,
        .utilityfunctest = NULL,
        .utility_block =
        {
            {0x3FC400,  0x1800,     ORDER_0 | MEMBLOCK_FLAGS_AS_MAX_BLOCK_SIZE, 0},
            {0,         0,          ORDER_1 | MEMBLOCK_FLAGS_NONE, 0},
            {0,         0,          ORDER_2 | MEMBLOCK_FLAGS_NONE, 0},
            {0,         0,          ORDER_3 | MEMBLOCK_FLAGS_NONE, 0},
            {0,         0,          ORDER_4 | MEMBLOCK_FLAGS_NONE, 0},
        },
    },
    [4] =
    {
        //"SCT_E67U_Qe.bin"
        "UBF114.ubf", UTILITY_FLAGS_USE_ENCRYPTION | UTILITY_FLAGS_SEPARATE_EXE,
        .call_address = 0x3FC400,
        .utilityfunctest = NULL,
        .utility_block =
        {
            {0x3FC400,  0x1800,     ORDER_0 | MEMBLOCK_FLAGS_AS_MAX_BLOCK_SIZE, 0},
            {0,         0,          ORDER_1 | MEMBLOCK_FLAGS_NONE, 0},
            {0,         0,          ORDER_2 | MEMBLOCK_FLAGS_NONE, 0},
            {0,         0,          ORDER_3 | MEMBLOCK_FLAGS_NONE, 0},
            {0,         0,          ORDER_4 | MEMBLOCK_FLAGS_NONE, 0},
        },
    },
    [5] =
    {
        //"SCT_E38D2e.bin"
        "UBF115.ubf", UTILITY_FLAGS_USE_ENCRYPTION | UTILITY_FLAGS_SEPARATE_EXE,
        .call_address = 0x3FC400,
        .utilityfunctest = NULL,
        .utility_block =
        {
            {0x3FC400,  0x1800,     ORDER_0 | MEMBLOCK_FLAGS_AS_MAX_BLOCK_SIZE, 0},
            {0,         0,          ORDER_1 | MEMBLOCK_FLAGS_NONE, 0},
            {0,         0,          ORDER_2 | MEMBLOCK_FLAGS_NONE, 0},
            {0,         0,          ORDER_3 | MEMBLOCK_FLAGS_NONE, 0},
            {0,         0,          ORDER_4 | MEMBLOCK_FLAGS_NONE, 0},
        },
    },
    [6] =
    {
        //"SCTLB7VPWQe.bin"
        "UBF116.ubf", UTILITY_FLAGS_USE_ENCRYPTION,
        .call_address = 0xFF9100,
        .utilityfunctest = NULL,
        .utility_block =
        {
            {0xFF9100,  0x1800,     ORDER_0 | MEMBLOCK_FLAGS_AS_MAX_BLOCK_SIZE, 0},
            {0,         0,          ORDER_1 | MEMBLOCK_FLAGS_NONE, 0},
            {0,         0,          ORDER_2 | MEMBLOCK_FLAGS_NONE, 0},
            {0,         0,          ORDER_3 | MEMBLOCK_FLAGS_NONE, 0},
            {0,         0,          ORDER_4 | MEMBLOCK_FLAGS_NONE, 0},
        },
    },
    [7] =
    {
        //"SCT_LB7De.bin"
        "UBF117.ubf", UTILITY_FLAGS_USE_ENCRYPTION,
        .call_address = 0x3FC400,
        .utilityfunctest = NULL,
        .utility_block =
        {
            {0xFF9100,  0x1800,     ORDER_0 | MEMBLOCK_FLAGS_AS_MAX_BLOCK_SIZE, 0},
            {0,         0,          ORDER_1 | MEMBLOCK_FLAGS_NONE, 0},
            {0,         0,          ORDER_2 | MEMBLOCK_FLAGS_NONE, 0},
            {0,         0,          ORDER_3 | MEMBLOCK_FLAGS_NONE, 0},
            {0,         0,          ORDER_4 | MEMBLOCK_FLAGS_NONE, 0},
        },
    },
    [8] =
    {
        //"SCTVPW1MQe.bin"
        "UBF118.ubf", UTILITY_FLAGS_USE_ENCRYPTION,
        .call_address = 0xFF9100,
        .utilityfunctest = NULL,
        .utility_block =
        {
            {0xFF9100,  0x1800,     ORDER_0 | MEMBLOCK_FLAGS_AS_MAX_BLOCK_SIZE, 0},
            {0,         0,          ORDER_1 | MEMBLOCK_FLAGS_NONE, 0},
            {0,         0,          ORDER_2 | MEMBLOCK_FLAGS_NONE, 0},
            {0,         0,          ORDER_3 | MEMBLOCK_FLAGS_NONE, 0},
            {0,         0,          ORDER_4 | MEMBLOCK_FLAGS_NONE, 0},
        },
    },
    [9] =
    {
        //"SCT_VPWDe.bin"
        "UBF119.ubf", UTILITY_FLAGS_USE_ENCRYPTION,
        .call_address = 0xFF9100,
        .utilityfunctest = NULL,
        .utility_block =
        {
            {0xFF9100,  0x1800,     ORDER_0 | MEMBLOCK_FLAGS_AS_MAX_BLOCK_SIZE, 0},
            {0,         0,          ORDER_1 | MEMBLOCK_FLAGS_NONE, 0},
            {0,         0,          ORDER_2 | MEMBLOCK_FLAGS_NONE, 0},
            {0,         0,          ORDER_3 | MEMBLOCK_FLAGS_NONE, 0},
            {0,         0,          ORDER_4 | MEMBLOCK_FLAGS_NONE, 0},
        },
    },
    [10] =
    {
        //"SCTVPWHMQe.bin"
        "UBF120.ubf", UTILITY_FLAGS_USE_ENCRYPTION,
        .call_address = 0xFF9100,
        .utilityfunctest = NULL,
        .utility_block =
        {
            {0xFF9100,  0x1800,     ORDER_0 | MEMBLOCK_FLAGS_AS_MAX_BLOCK_SIZE, 0},
            {0,         0,          ORDER_1 | MEMBLOCK_FLAGS_NONE, 0},
            {0,         0,          ORDER_2 | MEMBLOCK_FLAGS_NONE, 0},
            {0,         0,          ORDER_3 | MEMBLOCK_FLAGS_NONE, 0},
            {0,         0,          ORDER_4 | MEMBLOCK_FLAGS_NONE, 0},
        },
    },
    [11] =
    {
        //"SCT_VPW_hmegDe.bin"
        "UBF121.ubf", UTILITY_FLAGS_USE_ENCRYPTION,
        .call_address = 0xFF9100,
        .utilityfunctest = NULL,
        .utility_block =
        {
            {0xFF9100,  0x1800,     ORDER_0 | MEMBLOCK_FLAGS_AS_MAX_BLOCK_SIZE, 0},
            {0,         0,          ORDER_1 | MEMBLOCK_FLAGS_NONE, 0},
            {0,         0,          ORDER_2 | MEMBLOCK_FLAGS_NONE, 0},
            {0,         0,          ORDER_3 | MEMBLOCK_FLAGS_NONE, 0},
            {0,         0,          ORDER_4 | MEMBLOCK_FLAGS_NONE, 0},
        },
    },
    [12] =
    {
        //"SCTVPWLLYQe.bin"
        "UBF122.ubf", UTILITY_FLAGS_USE_ENCRYPTION,
        .call_address = 0xFFC100,
        .utilityfunctest = NULL,
        .utility_block =
        {
            {0xFFC100,  0x1800,     ORDER_0 | MEMBLOCK_FLAGS_AS_MAX_BLOCK_SIZE, 0},
            {0,         0,          ORDER_1 | MEMBLOCK_FLAGS_NONE, 0},
            {0,         0,          ORDER_2 | MEMBLOCK_FLAGS_NONE, 0},
            {0,         0,          ORDER_3 | MEMBLOCK_FLAGS_NONE, 0},
            {0,         0,          ORDER_4 | MEMBLOCK_FLAGS_NONE, 0},
        },
    },
    [13] =
    {
        //"SCT_LLYDe.bin"
        "UBF123.ubf", UTILITY_FLAGS_USE_ENCRYPTION,
        .call_address = 0xFFC100,
        .utilityfunctest = NULL,
        .utility_block =
        {
            {0xFFC100,  0x1800,     ORDER_0 | MEMBLOCK_FLAGS_AS_MAX_BLOCK_SIZE, 0},
            {0,         0,          ORDER_1 | MEMBLOCK_FLAGS_NONE, 0},
            {0,         0,          ORDER_2 | MEMBLOCK_FLAGS_NONE, 0},
            {0,         0,          ORDER_3 | MEMBLOCK_FLAGS_NONE, 0},
            {0,         0,          ORDER_4 | MEMBLOCK_FLAGS_NONE, 0},
        },
    },
    [14] =
    {
        //"SCT_T42U_Qe.bin"
        "UBF124.ubf", UTILITY_FLAGS_USE_ENCRYPTION,
        .call_address = 0xFF9000,
        .utilityfunctest = NULL,
        .utility_block =
        {
            {0xFF9000,  0x1800,     ORDER_0 | MEMBLOCK_FLAGS_AS_MAX_BLOCK_SIZE, 0},
            {0,         0,          ORDER_1 | MEMBLOCK_FLAGS_NONE, 0},
            {0,         0,          ORDER_2 | MEMBLOCK_FLAGS_NONE, 0},
            {0,         0,          ORDER_3 | MEMBLOCK_FLAGS_NONE, 0},
            {0,         0,          ORDER_4 | MEMBLOCK_FLAGS_NONE, 0},
        },
    },
    [15] =
    {
        //"SCT_T42D_Qe.bin"
        "UBF125.ubf", UTILITY_FLAGS_USE_ENCRYPTION,
        .call_address = 0xFF9000,
        .utilityfunctest = NULL,
        .utility_block =
        {
            {0xFF9000,  0x1800,     ORDER_0 | MEMBLOCK_FLAGS_AS_MAX_BLOCK_SIZE, 0},
            {0,         0,          ORDER_1 | MEMBLOCK_FLAGS_NONE, 0},
            {0,         0,          ORDER_2 | MEMBLOCK_FLAGS_NONE, 0},
            {0,         0,          ORDER_3 | MEMBLOCK_FLAGS_NONE, 0},
            {0,         0,          ORDER_4 | MEMBLOCK_FLAGS_NONE, 0},
        },
    },
    [16] =
    {
        //"SCT_T43U_Qe.bin"
        "UBF126.ubf", UTILITY_FLAGS_USE_ENCRYPTION | UTILITY_FLAGS_SEPARATE_EXE |
            UTILITY_FLAGS_EXE_HAS_REPONSE | UTILITY_FLAGS_EXE_ALLOW_NEG_RESPONSE,
            .call_address = 0x3FC400,
            .utilityfunctest = NULL,
            .utility_block =
            {
                {0x3FC400,  0x1800,     ORDER_0 | MEMBLOCK_FLAGS_AS_MAX_BLOCK_SIZE, 0},
                {0,         0,          ORDER_1 | MEMBLOCK_FLAGS_NONE, 0},
                {0,         0,          ORDER_2 | MEMBLOCK_FLAGS_NONE, 0},
                {0,         0,          ORDER_3 | MEMBLOCK_FLAGS_NONE, 0},
                {0,         0,          ORDER_4 | MEMBLOCK_FLAGS_NONE, 0},
            },    
    },
    [17] =
    {
        //"SCT_T43D_Q2e.bin",
        "UBF127.ubf", UTILITY_FLAGS_USE_ENCRYPTION | UTILITY_FLAGS_SEPARATE_EXE | \
            UTILITY_FLAGS_EXE_HAS_REPONSE | UTILITY_FLAGS_EXE_ALLOW_NEG_RESPONSE,
            .call_address = 0x3FC400,
            .utilityfunctest = NULL,
            .utility_block =
            {
                {0x3FC400,  0x1800,     ORDER_0 | MEMBLOCK_FLAGS_AS_MAX_BLOCK_SIZE, 0},
                {0,         0,          ORDER_1 | MEMBLOCK_FLAGS_NONE, 0},
                {0,         0,          ORDER_2 | MEMBLOCK_FLAGS_NONE, 0},
                {0,         0,          ORDER_3 | MEMBLOCK_FLAGS_NONE, 0},
                {0,         0,          ORDER_4 | MEMBLOCK_FLAGS_NONE, 0},
            },    
    },
    [18] =
    {
        //"SCT_E37U_Qe.bin"
        "UBF128.ubf", UTILITY_FLAGS_USE_ENCRYPTION | UTILITY_FLAGS_SEPARATE_EXE,
        .call_address = 0x400000,
        .utilityfunctest = NULL,
        .utility_block =
        {
            {0x400000,  0x1800,     ORDER_0 | MEMBLOCK_FLAGS_AS_MAX_BLOCK_SIZE, 0},
            {0,         0,          ORDER_1 | MEMBLOCK_FLAGS_NONE, 0},
            {0,         0,          ORDER_2 | MEMBLOCK_FLAGS_NONE, 0},
            {0,         0,          ORDER_3 | MEMBLOCK_FLAGS_NONE, 0},
            {0,         0,          ORDER_4 | MEMBLOCK_FLAGS_NONE, 0},
        },
    },
    [19] =
    {
        //"SCT_E37D_Qe.bin"
        "UBF129.ubf", UTILITY_FLAGS_USE_ENCRYPTION | UTILITY_FLAGS_SEPARATE_EXE,
        .call_address = 0x400000,
        .utilityfunctest = NULL,
        .utility_block =
        {
            {0x400000,  0x1800,     ORDER_0 | MEMBLOCK_FLAGS_AS_MAX_BLOCK_SIZE, 0},
            {0,         0,          ORDER_1 | MEMBLOCK_FLAGS_NONE, 0},
            {0,         0,          ORDER_2 | MEMBLOCK_FLAGS_NONE, 0},
            {0,         0,          ORDER_3 | MEMBLOCK_FLAGS_NONE, 0},
            {0,         0,          ORDER_4 | MEMBLOCK_FLAGS_NONE, 0},
        },
    },
    [20] =
    {
        //"SCT_E78U_REV_A.bin"
        "UBF130.ubf", UTILITY_FLAGS_USE_ENCRYPTION | UTILITY_FLAGS_SEPARATE_EXE,
        .call_address = 0x40008000,
        .utilityfunctest = NULL,
        .utility_block =
        {
            {0x40008000,0x1800,     ORDER_0 | MEMBLOCK_FLAGS_AS_MAX_BLOCK_SIZE, 0},
            {0,         0,          ORDER_1 | MEMBLOCK_FLAGS_NONE, 0},
            {0,         0,          ORDER_2 | MEMBLOCK_FLAGS_NONE, 0},
            {0,         0,          ORDER_3 | MEMBLOCK_FLAGS_NONE, 0},
            {0,         0,          ORDER_4 | MEMBLOCK_FLAGS_NONE, 0},
        },
    },
    [21] =
    {
        //"SCT_E78D_REV_A.bin"
        "UBF131.ubf", UTILITY_FLAGS_USE_ENCRYPTION | UTILITY_FLAGS_SEPARATE_EXE,
        .call_address = 0x40008000,
        .utilityfunctest = NULL,
        .utility_block =
        {
            {0x40008000,0x1800,     ORDER_0 | MEMBLOCK_FLAGS_AS_MAX_BLOCK_SIZE, 0},
            {0,         0,          ORDER_1 | MEMBLOCK_FLAGS_NONE, 0},
            {0,         0,          ORDER_2 | MEMBLOCK_FLAGS_NONE, 0},
            {0,         0,          ORDER_3 | MEMBLOCK_FLAGS_NONE, 0},
            {0,         0,          ORDER_4 | MEMBLOCK_FLAGS_NONE, 0},
        },
    },
    [22] =
    {
        //"SCT_E39U_REV_A"
        "UBF132.ubf", UTILITY_FLAGS_USE_ENCRYPTION | UTILITY_FLAGS_SEPARATE_EXE,
        .call_address = 0x40008000,
        .utilityfunctest = NULL,
        .utility_block =
        {
            {0x40008000,0x1800,     ORDER_0 | MEMBLOCK_FLAGS_AS_MAX_BLOCK_SIZE, 0},
            {0,         0,          ORDER_1 | MEMBLOCK_FLAGS_NONE, 0},
            {0,         0,          ORDER_2 | MEMBLOCK_FLAGS_NONE, 0},
            {0,         0,          ORDER_3 | MEMBLOCK_FLAGS_NONE, 0},
            {0,         0,          ORDER_4 | MEMBLOCK_FLAGS_NONE, 0},
        },
    },
    [23] =
    {
        //"SCT_E39D_REV_A"
        "UBF133.ubf", UTILITY_FLAGS_USE_ENCRYPTION | UTILITY_FLAGS_SEPARATE_EXE,
        .call_address = 0x40008000,
        .utilityfunctest = NULL,
        .utility_block =
        {
            {0x40008000,0x1800,     ORDER_0 | MEMBLOCK_FLAGS_AS_MAX_BLOCK_SIZE, 0},
            {0,         0,          ORDER_1 | MEMBLOCK_FLAGS_NONE, 0},
            {0,         0,          ORDER_2 | MEMBLOCK_FLAGS_NONE, 0},
            {0,         0,          ORDER_3 | MEMBLOCK_FLAGS_NONE, 0},
            {0,         0,          ORDER_4 | MEMBLOCK_FLAGS_NONE, 0},
        },
    },
    [24] =
    {
        //"SCT_T76U.bin"
        "UBF134.ubf", UTILITY_FLAGS_USE_ENCRYPTION | UTILITY_FLAGS_SEPARATE_EXE,
        .call_address = 0x3FC400,
        .utilityfunctest = NULL,
        .utility_block =
        {
            {0x3FC400,  0x1800,     ORDER_0 | MEMBLOCK_FLAGS_AS_MAX_BLOCK_SIZE, 0},
            {0,         0,          ORDER_1 | MEMBLOCK_FLAGS_NONE, 0},
            {0,         0,          ORDER_2 | MEMBLOCK_FLAGS_NONE, 0},
            {0,         0,          ORDER_3 | MEMBLOCK_FLAGS_NONE, 0},
            {0,         0,          ORDER_4 | MEMBLOCK_FLAGS_NONE, 0},
        },
    },
    [25] =
    {
        //"SCT_T76D.bin"
        "UBF135.ubf", UTILITY_FLAGS_USE_ENCRYPTION | UTILITY_FLAGS_SEPARATE_EXE,
        .call_address = 0x3FC400,
        .utilityfunctest = NULL,
        .utility_block =
        {
            {0x3FC400,  0x1800,     ORDER_0 | MEMBLOCK_FLAGS_AS_MAX_BLOCK_SIZE, 0},
            {0,         0,          ORDER_1 | MEMBLOCK_FLAGS_NONE, 0},
            {0,         0,          ORDER_2 | MEMBLOCK_FLAGS_NONE, 0},
            {0,         0,          ORDER_3 | MEMBLOCK_FLAGS_NONE, 0},
            {0,         0,          ORDER_4 | MEMBLOCK_FLAGS_NONE, 0},
        },
    },
    [26] =
    {
        //"SCT_E92U.bin"
        "UBF136.ubf", UTILITY_FLAGS_USE_ENCRYPTION | UTILITY_FLAGS_SEPARATE_EXE,
        .call_address = 0x40001000,
        .utilityfunctest = NULL,
        .utility_block =
        {
            {0x40001000,0x1800,     ORDER_0 | MEMBLOCK_FLAGS_AS_MAX_BLOCK_SIZE, 0},
            {0,         0,          ORDER_1 | MEMBLOCK_FLAGS_NONE, 0},
            {0,         0,          ORDER_2 | MEMBLOCK_FLAGS_NONE, 0},
            {0,         0,          ORDER_3 | MEMBLOCK_FLAGS_NONE, 0},
            {0,         0,          ORDER_4 | MEMBLOCK_FLAGS_NONE, 0},
        },
    },
    [27] =
    {
        //"SCT_E92D.bin"
        "UBF137.ubf", UTILITY_FLAGS_USE_ENCRYPTION | UTILITY_FLAGS_SEPARATE_EXE,
        .call_address = 0x40001000,
        .utilityfunctest = NULL,
        .utility_block =
        {
            {0x40001000,0x1800,     ORDER_0 | MEMBLOCK_FLAGS_AS_MAX_BLOCK_SIZE, 0},
            {0,         0,          ORDER_1 | MEMBLOCK_FLAGS_NONE, 0},
            {0,         0,          ORDER_2 | MEMBLOCK_FLAGS_NONE, 0},
            {0,         0,          ORDER_3 | MEMBLOCK_FLAGS_NONE, 0},
            {0,         0,          ORDER_4 | MEMBLOCK_FLAGS_NONE, 0},
        },
    },
    [28] =
    {
        //"SCT_E39AU.bin"
        "UBF138.ubf", UTILITY_FLAGS_USE_ENCRYPTION | UTILITY_FLAGS_SEPARATE_EXE,
        .call_address = 0x40008000,
        .utilityfunctest = NULL,
        .utility_block =
        {
            {0x40008000,0x1800,     ORDER_0 | MEMBLOCK_FLAGS_AS_MAX_BLOCK_SIZE, 0},
            {0,         0,          ORDER_1 | MEMBLOCK_FLAGS_NONE, 0},
            {0,         0,          ORDER_2 | MEMBLOCK_FLAGS_NONE, 0},
            {0,         0,          ORDER_3 | MEMBLOCK_FLAGS_NONE, 0},
            {0,         0,          ORDER_4 | MEMBLOCK_FLAGS_NONE, 0},
        },
    },
    [29] =
    {
        //"SCT_E39AD.bin"
        "UBF139.ubf", UTILITY_FLAGS_USE_ENCRYPTION | UTILITY_FLAGS_SEPARATE_EXE,
        .call_address = 0x40008000,
        .utilityfunctest = NULL,
        .utility_block =
        {
            {0x40008000,0x1800,     ORDER_0 | MEMBLOCK_FLAGS_AS_MAX_BLOCK_SIZE, 0},
            {0,         0,          ORDER_1 | MEMBLOCK_FLAGS_NONE, 0},
            {0,         0,          ORDER_2 | MEMBLOCK_FLAGS_NONE, 0},
            {0,         0,          ORDER_3 | MEMBLOCK_FLAGS_NONE, 0},
            {0,         0,          ORDER_4 | MEMBLOCK_FLAGS_NONE, 0},
        },
    },
    [30] =
    {
        //"SCT_E83U.bin"
        "UBF140.ubf", UTILITY_FLAGS_USE_ENCRYPTION | UTILITY_FLAGS_SEPARATE_EXE,
        .call_address = 0x40008000,
        .utilityfunctest = NULL,
        .utility_block =
        {
            {0x40008000,0x1800,     ORDER_0 | MEMBLOCK_FLAGS_AS_MAX_BLOCK_SIZE, 0},
            {0,         0,          ORDER_1 | MEMBLOCK_FLAGS_NONE, 0},
            {0,         0,          ORDER_2 | MEMBLOCK_FLAGS_NONE, 0},
            {0,         0,          ORDER_3 | MEMBLOCK_FLAGS_NONE, 0},
            {0,         0,          ORDER_4 | MEMBLOCK_FLAGS_NONE, 0},
        },
    },
    [31] =
    {
        //"SCT_E83AD.bin"
        "UBF141.ubf", UTILITY_FLAGS_USE_ENCRYPTION | UTILITY_FLAGS_SEPARATE_EXE,
        .call_address = 0x40008000,
        .utilityfunctest = NULL,
        .utility_block =
        {
            {0x40008000,0x1800,     ORDER_0 | MEMBLOCK_FLAGS_AS_MAX_BLOCK_SIZE, 0},
            {0,         0,          ORDER_1 | MEMBLOCK_FLAGS_NONE, 0},
            {0,         0,          ORDER_2 | MEMBLOCK_FLAGS_NONE, 0},
            {0,         0,          ORDER_3 | MEMBLOCK_FLAGS_NONE, 0},
            {0,         0,          ORDER_4 | MEMBLOCK_FLAGS_NONE, 0},
        },
    },
    [32] =
    {
        //"SCT_E83BD.bin"
        "UBF142.ubf", UTILITY_FLAGS_USE_ENCRYPTION | UTILITY_FLAGS_SEPARATE_EXE,
        .call_address = 0x40008000,
        .utilityfunctest = NULL,
        .utility_block =
        {
            {0x40008000,0x1800,     ORDER_0 | MEMBLOCK_FLAGS_AS_MAX_BLOCK_SIZE, 0},
            {0,         0,          ORDER_1 | MEMBLOCK_FLAGS_NONE, 0},
            {0,         0,          ORDER_2 | MEMBLOCK_FLAGS_NONE, 0},
            {0,         0,          ORDER_3 | MEMBLOCK_FLAGS_NONE, 0},
            {0,         0,          ORDER_4 | MEMBLOCK_FLAGS_NONE, 0},
        },
    },
};

