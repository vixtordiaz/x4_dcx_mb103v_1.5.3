/**
  **************** (C) COPYRIGHT 2011 SCT Performance, LLC *********************
  * File Name          : gm_veh_defs.h
  * Device / System    : SCT products using the "CommonCode" platform
  * Author             : Quyen Leba
  *
  * Version            : 1 
  * Date               : 05/02/2011
  * Description        : 
  *                    : 
  *
  *
  * History            : 05/02/2011 M. Davis
  *                    :   Add SCT Copyright header,
  *
  ******************************************************************************
  */

#ifndef __GM_VEH_DEFS_H
#define __GM_VEH_DEFS_H

#include <arch/gentype.h>
#include <common/veh_defs.h>

#define GM_VEH_DEF_START                                110
#define GM_VEH_DEF_END                                  209

#define VEH_DEF_FLAG_NONE                               (0)
#define UNLOCK_ALL_ECMS_FIRST                           (1 << 0)
#define SKIP_RECYCLE_KEY_ON                             (1 << 1)
#define REQUIRE_WAKEUP                                  (1 << 2)
#define WAKEUP_CHECK_ONCE                               (1 << 3)
#define SIMPLE_SETMARRIED                               (1 << 4)
#define REQUIRE_OVERLAY                                 (1 << 5)
#define CHECK_KEYON_PRIOR_WAKEUP                        (1 << 6)    //depends on REQUIRE_WAKEUP
#define ALLOW_BLANK_VIN                                 (1 << 7)    //should only used in development
#define CUSTOM_TUNE_TUNEOS_FILE_NOT_REQUIRED            (1 << 8)
#define PRE_DOWNLOAD_CHECK                              (1 << 12)     //check for original/mod bootloader hash

// Reads Vehicle Info After Download to Verify Success
#define CHECK_SUCCESS_AFTER_DOWNLOAD                    (1 << 9)

// When TUNEOS codes do not match whats read from vehicle, Full Flash is required,
// suppress Full Flash message.
#define SUPPRESS_FULL_FLASH_MESSAGE                     (1 << 10)

// TODO: We only need this until segment flashing
#define APPROVE_SKIP_FLASH_TCM                          (1 << 11)

//in case TCM is unsupported, set this flag will make unsupported error
//i.e.: without this flag, user can choose to skip program TCM
#define SUB_ECM_REQUIRED                                (1 << 29)
//in case TCM is unsupported and allow PCM only
#define ALLOW_SKIP_UNSUPPORTED_SUB_ECM                  (1 << 30)

#define VEH_DEFS_COUNT                                  72
extern const VEH_Def gm_veh_defs[VEH_DEFS_COUNT];

u8 gm_veh_get_ecmcount(u16 veh_type);
u8 gm_veh_validate_vehicletype(u16 veh_type);

#define vehvar(vt,var)                          gm_veh_defs[vt-GM_VEH_DEF_START].##var
#define vehdefmax                               GM_VEH_DEF_END
#define vehdefmin                               GM_VEH_DEF_START
#define vehecmcount(vt)                         gm_veh_get_ecmcount(vt)

#endif    //__GM_VEH_DEFS_H
