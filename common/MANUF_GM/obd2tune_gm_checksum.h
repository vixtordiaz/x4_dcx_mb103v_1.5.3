/**
  **************** (C) COPYRIGHT 2011 SCT Performance, LLC *********************
  * File Name          : obd2tune_gm_checksum.h
  * Device / System    : SCT products using the "CommonCode" platform
  * Author             : Quyen Leba
  *
  * Version            : 1 
  * Date               : 05/02/2011
  * Description        : 
  *                    : 
  *
  *
  * History            : 05/02/2011 M. Davis
  *                    :   Add SCT Copyright header,
  *
  ******************************************************************************
  */

#ifndef __OBD2TUNE_GM_CHECKSUM_H
#define __OBD2TUNE_GM_CHECKSUM_H

#include <arch/gentype.h>
#include <common/obd2tune.h>

u8 obd2tune_gm_apply_checksum(u16 veh_type, selected_tunelist_info *tuneinfo);
u8 obd2tune_gm_check_stockfile_checksum(u16 veh_type, selected_tunelist_info *tuneinfo);
u8 obd2tune_gm_ecu_test_os_checksum(u16 ecm_type);
u8 obd2tune_gm_file_test_os_checksum(const u8 *filename, u16 ecm_type);
u8 obd2tune_gm_preloadedcsumfilename(u8 *tunefilename, u8 *checksumfilename);

#define OBD2TUNE_APPLY_CHECKSUM_DEFINED
#define obd2tune_apply_checksum(veh_type,tuneinfo)  \
    obd2tune_gm_apply_checksum(veh_type,tuneinfo)

#define OBD2TUNE_CHECK_STOCKFILE_CHECKSUM_DEFINED
#define obd2tune_check_stockfile_checksum(veh_type,tuneinfo)  \
    obd2tune_gm_check_stockfile_checksum(veh_type,tuneinfo)

#define OBD2TUNE_ECU_TEST_OS_CHECKSUM_DEFINED
#define obd2tune_ecu_test_os_checksum(ecm_type) \
    obd2tune_gm_ecu_test_os_checksum(ecm_type)

#define OBD2TUNE_FILE_TEST_OS_CHECKSUM_DEFINED
#define obd2tune_file_test_os_checksum(filename,ecm_type)   \
    obd2tune_gm_file_test_os_checksum(filename,ecm_type)

#define OBD2TUNE_GET_PRELOADED_CHECKSUM_FILENAME
#define obd2tune_preloadedcsumfilename(tunefilename,checksumfilename)   \
    obd2tune_gm_preloadedcsumfilename(tunefilename,checksumfilename)


#endif    //__OBD2TUNE_GM_CHECKSUM_H
