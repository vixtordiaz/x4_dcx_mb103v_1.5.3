/**
  **************** (C) COPYRIGHT 2011 SCT Performance, LLC *********************
  * File Name          : obd2can_gm.c
  * Device / System    : SCT products using the "CommonCode" platform
  * Author             : Quyen Leba
  *
  * Version            : 1 
  * Date               : 05/02/2011
  * Description        : 
  *                    : 
  *
  *
  * History            : 05/02/2011 M. Davis
  *                    :   Add SCT Copyright header,
  *
  ******************************************************************************
  */

//------------------------------------------------------------------------------
// Log error point: 0x30xx -> 0x31xx
//------------------------------------------------------------------------------

#include <string.h>
#include <stdio.h>
#include <fs/genfs.h>
#include <board/delays.h>
#include <board/rtc.h>
#include <common/obd2can.h>
#include <common/statuscode.h>
#include <common/bitreverse.h>
#include <common/crypto_blowfish.h>
#include <common/log.h>
#include <common/itoa.h>
#include "obd2can_gm.h"
#include <common/ubf.h>

extern obd2_info gObd2info;             /* from obd2.c */
extern Datalog_Mode gDatalogmode;       /* from obd2datalog.c */

//------------------------------------------------------------------------------
// Clear KAM (Keep Alive Memory)
// Obtained from PTDIAG sniffs of clear KAM function
// Inputs:  u32 ecm_id
//          VehicleCommLevel commlevel
// Return:  u8  status
// Engineer: Patrick Downs
//------------------------------------------------------------------------------
u8 obd2can_gm_clear_kam(u32 ecm_id, VehicleCommLevel commlevel)
{
    // TODO P: Add support if needed
    return S_SUCCESS;
}

//------------------------------------------------------------------------------
// ClearDiagnosticInformation ($04)
// Inputs:  bool broadcast
//          u32 ecm_id
// Return:  u8  status
// Engineer: Quyen Leba
//------------------------------------------------------------------------------
u8 obd2can_gm_clear_diagnostic_information(bool broadcast, u32 ecm_id)
{
    Obd2can_ServiceData servicedata;
    u8 status;

    obd2can_servicedata_init(&servicedata);
    servicedata.ecm_id = ecm_id;
    servicedata.broadcast = broadcast;
    servicedata.service = GMLAN_ClearDiagnosticInformation;
    status = obd2can_txrx(&servicedata);
    
    return status;
}

//------------------------------------------------------------------------------
// InitiateDiagnosticOperation ($10)
// Inputs:  bool broadcast
//          u32 ecm_id
//          Obd2can_SubServiceNumber subservice (disableAllDTCs,
//              enableDTCsDuringDevCntrl, wakeUpLinks)
// Return:  u8  status
// Engineer: Quyen Leba
//------------------------------------------------------------------------------
u8 obd2can_gm_initiate_diagnostic_operation(bool broadcast, u32 ecm_id,
                                            Obd2can_SubServiceNumber subservice)
{
    Obd2can_ServiceData servicedata;
    u8 status;

    obd2can_servicedata_init(&servicedata);
    servicedata.ecm_id = ecm_id;
    servicedata.broadcast = broadcast;
    servicedata.service = GMLAN_InitiateDiagnosticOperation;
    servicedata.subservice = subservice;
    status = obd2can_txrx(&servicedata);
    
    return status;
}

//------------------------------------------------------------------------------
// ECUResetRequest ($11)
// Inputs:  bool broadcast
//          u32 ecm_id
//          Obd2can_SubServiceNumber subservice
//          bool isreponse_expected (TRUE: wait for response)
// Return:  u8  status
// Engineer: Quyen Leba
//------------------------------------------------------------------------------
u8 obd2can_gm_ecu_reset_request(bool broadcast,                         //$11
                                  u32 ecm_id,
                                  Obd2can_SubServiceNumber subservice,
                                  bool isreponse_expected)
{
    Obd2can_ServiceData servicedata;
    u8 status;
    //TODOQ4: check for GMLAN reset
    obd2can_servicedata_init(&servicedata);
    servicedata.ecm_id = ecm_id;
    servicedata.broadcast = broadcast;
    servicedata.service = GMLAN_RequestModuleReset;
    servicedata.subservice = subservice;
    servicedata.response_expected = isreponse_expected;
    status = obd2can_txrx(&servicedata);
    
    return status;
}

//------------------------------------------------------------------------------
// ReadDataByIdentifier ($1A)
// Inputs:  bool broadcast
//          u32 ecm_id
// Output:  u8  *data
//          u16 *datalength;
// Return:  u8  status
// Engineer: Quyen Leba
//------------------------------------------------------------------------------
u8 obd2can_gm_read_data_by_identifier(bool broadcast, u32 ecm_id,
                                      u8 identifier, u8 *data, u16 *datalength)
{
    Obd2can_ServiceData servicedata;
    u8 status;

    *datalength = 0;
    obd2can_servicedata_init(&servicedata);
    servicedata.ecm_id = ecm_id;
    servicedata.broadcast = broadcast;
    servicedata.service = GMLAN_ReadDataByIdentifier;
    servicedata.subservice = identifier;
    status = obd2can_txrx(&servicedata);

    if (status == S_SUCCESS && data && datalength)
    {
        memcpy(data,servicedata.rxdata,servicedata.rxdatalength);
        *datalength = servicedata.rxdatalength;
    }

    return status;
}

//------------------------------------------------------------------------------
// ReturnNormalMode ($20)
// Inputs:  bool broadcast
//          u32 ecm_id
// Return:  u8  status
// Engineer: Quyen Leba
//------------------------------------------------------------------------------
u8 obd2can_gm_return_normal_mode(bool broadcast, u32 ecm_id)
{
    Obd2can_ServiceData servicedata;
    u8 status;

    obd2can_servicedata_init(&servicedata);
    servicedata.ecm_id = ecm_id;
    servicedata.broadcast = broadcast;
    servicedata.service = GMLAN_ReturnNormalMode;
    status = obd2can_txrx(&servicedata);

    return status;
}

//------------------------------------------------------------------------------
// Read Data by ParameterId
// Inputs:  u32 ecm_id
//          u16 pid
//          u8  pidsize (size in bytes of pid; i.e. 1,2,4)
// Output:  u8  *output
// Return:  u8  status
// Engineer: Quyen Leba
//------------------------------------------------------------------------------
u8 obd2can_gm_read_data_bypid(u32 ecm_id, u16 pid, u8 pidsize, u8 *output)
{
    Obd2can_ServiceData servicedata;
    u8 status;

    obd2can_servicedata_init(&servicedata);
    servicedata.ecm_id = ecm_id;
    servicedata.service_controldatabuffer[0] = (pid >> 8);
    servicedata.service_controldatabuffer[1] = (pid & 0xFF);
    servicedata.service_controldatalength = 2;
    servicedata.service = GMLAN_ReadDataByParameterIdentifier;
    status = obd2can_txrx(&servicedata);

    if (status == S_SUCCESS)
    {
        memcpy((char*)output,(char*)&servicedata.rxdata[2],pidsize);
    }
    else
    {
        *output = 0;
    }
    
    return status;
}

//------------------------------------------------------------------------------
// Inputs:  u32 ecm_id (NOT_USED)
//          u32 ecm_response_id (NOT_USED)
//          u32 address
//          u32 length (multiple of 4)
//          MemoryAddressType addrtype
// Outputs: u8  *data
//          u16 *datalength
// Engineer: Quyen Leba
// TODOQ: currently hardcoded, come back when have more info on this
//------------------------------------------------------------------------------
u8 obd2can_gm_read_memory_address_by20extendedid(u32 ecm_id,
                                                 u32 ecm_response_id,
                                                 u32 address, u32 length,
                                                 MemoryAddressType addrtype,
                                                 u8 *data, u16 *datalength)
{
    u32 currentaddress;
    u32 bytecount;
    u8  buffer[8];
    u8  rxbuffer[256];
    u16 rxbufferlength;
    u8  status;
    
    *datalength = 0;
    currentaddress = address;
    bytecount = 0;
    status = S_SUCCESS;
    
    buffer[0] = 0x20;
    buffer[4] = 0x04;
    
    while(bytecount < length)
    {
        //NOTE: assume 4 byte per read and length is multiple of 4
        
        buffer[1] = (u8)(currentaddress >> 16);
        buffer[2] = (u8)(currentaddress >> 8);
        buffer[3] = (u8)(currentaddress);
        status = obd2can_txraw_ext(0x18EF03FA,buffer,5);
        if (status != S_SUCCESS)
        {
            goto obd2can_gm_read_memory_address_by20extendedid_done;
        }
        status = obd2can_rxraw_ext(0x18EFFA03, rxbuffer, &rxbufferlength);
        if (status != S_SUCCESS)
        {
            goto obd2can_gm_read_memory_address_by20extendedid_done;
        }
        if (buffer[0] != rxbuffer[0] || buffer[3] != rxbuffer[3] ||
            rxbufferlength != 8)
        {
            goto obd2can_gm_read_memory_address_by20extendedid_done;
        }
        memcpy((char*)&data[bytecount],(char*)&rxbuffer[4],4);
        
        bytecount += 4;
        currentaddress += 4;
    }
    
    *datalength = bytecount;
    
obd2can_gm_read_memory_address_by20extendedid_done:
    return status;
}

//------------------------------------------------------------------------------
// ReadMemoryByAddress ($23)
// Inputs:  u32 ecm_id
//          u32 address (memory address)
//          u16 length (to request upload)
//          MemoryAddressType addrtype
//          bool force_readby4bytes
// Output:  u8  *data
//          u16 *datalength
// Return:  u8  status
// Engineer: Quyen Leba
//------------------------------------------------------------------------------
u8 obd2can_gm_read_memory_address(u32 ecm_id, u32 address, u16 length,
                                  MemoryAddressType addrtype,
                                  u8 *data, u16 *datalength,
                                  bool force_readby4bytes)
{
    Obd2can_ServiceData servicedata;
    u32 i;
    u8 status;

    
    if (force_readby4bytes && length > 4)
    {
        u16 this_length;
        u16 this_datalength;

        i = 0;
        while(i<length)
        {
            this_length = length - i;
            if (this_length > 4)
            {
                this_length = 4;
            }
            status = obd2can_gm_read_memory_address(ecm_id,address+i,
                                                    this_length,addrtype,
                                                    &data[i],&this_datalength,
                                                    FALSE);
            if (status != S_SUCCESS)
            {
                return status;
            }
            i += 4;
        }
        if (datalength)
        {
            *datalength = i;
        }
    }
    else
    {
        obd2can_servicedata_init(&servicedata);
        servicedata.ecm_id = ecm_id;
        servicedata.service = GMLAN_ReadMemoryByAddress;
        servicedata.rxdata = data;
        if (addrtype == Address_24_Bits)
        {
            servicedata.service_controldatabuffer[0] = (u8)(address >> 16);
            servicedata.service_controldatabuffer[1] = (u8)(address >> 8);
            servicedata.service_controldatabuffer[2] = (u8)(address & 0xFF);
            servicedata.service_controldatabuffer[3] = (u8)(length >> 8);
            servicedata.service_controldatabuffer[4] = (u8)(length & 0xFF);
            servicedata.service_controldatalength = 5;
        }
        else if (addrtype == Address_32_Bits)
        {
            servicedata.service_controldatabuffer[0] = (u8)(address >> 24);
            servicedata.service_controldatabuffer[1] = (u8)(address >> 16);
            servicedata.service_controldatabuffer[2] = (u8)(address >> 8);
            servicedata.service_controldatabuffer[3] = (u8)(address & 0xFF);
            servicedata.service_controldatabuffer[4] = (u8)(length >> 8);
            servicedata.service_controldatabuffer[5] = (u8)(length & 0xFF);
            servicedata.service_controldatalength = 6;
        }
        else
        {
            log_push_error_point(0x3000);
            return S_BADCONTENT;
        }
    
        servicedata.first_frame_data_offset =
            servicedata.service_controldatalength - 2;
    
        //status = obd2can_txrx(&servicedata);
        status = obd2can_txrxcomplex(&servicedata);
        if (datalength)
        {
            *datalength = 0;
            if (status == S_SUCCESS)
            {
                *datalength = servicedata.rxdatalength;
            }
        }
    }

    return status;
}

//------------------------------------------------------------------------------
// ReadMemoryByAddress ($23)
// Inputs:  u32 ecm_id
//          u32 address (memory address)
//          u16 length (to request upload)
//          MemoryAddressType addrtype
//          bool response_expected
// Output:  u8  *data
//          u16 *datalength
// Return:  u8  status
// Engineer: Tristen Pierson
//------------------------------------------------------------------------------
u8 obd2can_gm_read_data_bydma(u32 ecm_id, u32 address, u16 length,
                              MemoryAddressType addrtype,
                              u8 *data, u16 *datalength,
                              bool response_expected)
{
    Obd2can_ServiceData servicedata;
    u8 status;

    if (response_expected == TRUE && (data == NULL || datalength == NULL))
    {
        log_push_error_point(0x305C);
        return S_INPUT;
    }    
    obd2can_servicedata_init(&servicedata);
    servicedata.ecm_id = ecm_id;
    servicedata.service = GMLAN_ReadMemoryByAddress;
    servicedata.rxdata = data;
    
    if (addrtype == Address_24_Bits)
    {
        servicedata.service_controldatabuffer[0] = (u8)(address >> 16);
        servicedata.service_controldatabuffer[1] = (u8)(address >> 8);
        servicedata.service_controldatabuffer[2] = (u8)(address & 0xFF);
        servicedata.service_controldatabuffer[3] = (u8)(length >> 8);
        servicedata.service_controldatabuffer[4] = (u8)(length & 0xFF);
        servicedata.service_controldatalength = 5;
    }
    else if (addrtype == Address_32_Bits)
    {
        servicedata.service_controldatabuffer[0] = (u8)(address >> 24);
        servicedata.service_controldatabuffer[1] = (u8)(address >> 16);
        servicedata.service_controldatabuffer[2] = (u8)(address >> 8);
        servicedata.service_controldatabuffer[3] = (u8)(address & 0xFF);
        servicedata.service_controldatabuffer[4] = (u8)(length >> 8);
        servicedata.service_controldatabuffer[5] = (u8)(length & 0xFF);
        servicedata.service_controldatalength = 6;
    }
        
    if (response_expected == FALSE)
    {    
        servicedata.response_expected = FALSE;
        return obd2can_txrx(&servicedata);
    }
    status = obd2can_txrx(&servicedata);
    
    *datalength = 0;
    if (status == S_SUCCESS)
    {
        if (servicedata.rxdatalength > 1)
        {
            *datalength = servicedata.rxdatalength - 1;
            memcpy((char*)data,(char*)&servicedata.rxdata[1],*datalength);
            return S_SUCCESS;
        }
        else
        {
            log_push_error_point(0x305D);
            return S_BADCONTENT;
        }
    }
    return status;
}

//------------------------------------------------------------------------------
// SecurityAccess ($27)
// Inputs:  u32 ecm_id
// Return:  u8  status
// Engineer: Quyen Leba
//------------------------------------------------------------------------------
u8 obd2can_gm_security_access(u32 ecm_id, Obd2can_SubServiceNumber subservice,
                              u8 *data, u8 *datalength, u32 algoindex)
{
    Obd2can_ServiceData servicedata;
    u8 status;

    obd2can_servicedata_init(&servicedata);
    servicedata.ecm_id = ecm_id;
    servicedata.service = GMLAN_SecurityAccess;
    servicedata.subservice = (u8)subservice;
    
    if (subservice == SPSsendKey || subservice == DevCtrlsendKey)
    {
        if (*datalength != 2)
        {
            log_push_error_point(0x3001);
            return S_INPUT;
        }
        memcpy(servicedata.service_controldatabuffer,data,2);
        servicedata.service_controldatalength = 2;
    }
    
    status = obd2can_txrx(&servicedata);

    if (status == S_SUCCESS)
    {
        if ((data && servicedata.rxdatalength == 3) &&
            (subservice == SPSrequestSeed || subservice == DevCtrlrequestSeed))
        {
            *datalength = 2;
            memcpy(data,&servicedata.rxdata[1],2);
        }
    }
    return status;
}

//------------------------------------------------------------------------------
// DisableNormalCommunication ($28)
// Inputs:  bool broadcast
//          u32 ecm_id
//          bool expect_response (TRUE: just send it)
// Return:  u8  status
// Engineer: Quyen Leba
//------------------------------------------------------------------------------
u8 obd2can_gm_disablenormalcommunication(bool broadcast, u32 ecm_id,
                                         bool expect_response)
{
    Obd2can_ServiceData servicedata;
    u8 status;

    obd2can_servicedata_init(&servicedata);
    servicedata.ecm_id = ecm_id;
    servicedata.broadcast = broadcast;
    servicedata.response_expected = expect_response;
    servicedata.service = GMLAN_DisableNormalCommunication;
    status = obd2can_txrx(&servicedata);
    
    return status;
}

//------------------------------------------------------------------------------
// DynamicallyDefineMessage ($2C)
// Inputs:  bool broadcast
//          u32 ecm_id
// Return:  u8  status
// Engineer: Quyen Leba
// Note: use this function to define message as we as validate pid
// Note: to validate a pid, pidcount is 1
//------------------------------------------------------------------------------
u8 obd2can_gm_dynamicallydefinemessage(u32 ecm_id, u8 msg_id,
                                       u16 *pidlist, u8 pidcount)
{
    Obd2can_ServiceData servicedata;
    u8 i,j;
    u8 status;
    u8 count;
    u8 pidlistdata[2*8];

    if (pidcount > 8 || pidcount == 0)
    {
        //it is not allowed to define this many pids in one message
        //max should be 5 -> 7
        //because message: [5E8] [FE] [00 00 00 00 00 00 00]
        log_push_error_point(0x3002);
        return S_INPUT;
    }
    
    obd2can_servicedata_init(&servicedata);
    servicedata.ecm_id = ecm_id;
    servicedata.service = GMLAN_DynamicallyDefineMessage;
    servicedata.service_controldatabuffer[0] = msg_id;
    servicedata.service_controldatalength = 1;
    obd2_setcooldowntime(15000);

    count = 0;
    for(i=0;i<pidcount;i++)
    {
        count++;
        for(j=0;j<count;j++)
        {
            pidlistdata[2*j] = (u8)(pidlist[j] >> 8);
            pidlistdata[2*j+1] = (u8)(pidlist[j] & 0xFF);
        }
        servicedata.txdata = pidlistdata;
        servicedata.txdatalength = j*2;
        status = obd2can_txrx(&servicedata);
    }
    obd2_setcooldowntime(0);

    if (status == S_ERROR)
    {
        switch (servicedata.errorcode)
        {
        case 0x31:  // Request Out Of Range (PID unsupported or DPID# invalid)
            status = S_OUTOFRANGE;
            break;            
        case 0x12: // Invalid Format (Message exceeds 7 bytes, or no bytes)
            status = S_NOTFIT;
            break;
        default:
            break;
        }
    }

    return status;
}

//------------------------------------------------------------------------------
// obd2can_gm_definePIDByAddress ($2D)
// Inputs:  u32 ecm_id
//          u16 pid
//          u32 address (DMA)
//          u8  size
//          u8  addrbytes (3 or 4)
// Return:  u8  status
// Engineer: Tristen Pierson
// Note: Use this function to map ECU variables to a dynamic Parameter
//       Indentifier (PID) using ECU memory addresses.
//------------------------------------------------------------------------------
u8 obd2can_gm_definePIDByAddress(u32 ecm_id, u16 pid, u32 address, u8 size)
{
    Obd2can_ServiceData servicedata;  
    
    obd2can_servicedata_init(&servicedata);
    servicedata.ecm_id = ecm_id;
    servicedata.service = GMLAN_DefinePIDByAddress;   
    servicedata.service_controldatabuffer[0] = (u8)(pid >> 8);
    servicedata.service_controldatabuffer[1] = (u8)(pid & 0xFF);
    servicedata.service_controldatabuffer[2] = (u8)(address >> 24);
    servicedata.service_controldatabuffer[3] = (u8)(address >> 16);
    servicedata.service_controldatabuffer[4] = (u8)(address >> 8);
    servicedata.service_controldatabuffer[5] = (u8)(address & 0xFF);
    servicedata.service_controldatabuffer[6] = size;
    servicedata.service_controldatalength = 7;
   
    return obd2can_txrxcomplex(&servicedata);    
}
//------------------------------------------------------------------------------
// RequestDownload ($34)
// Inputs:  u32 ecm_id
// Output:  u8  format (0x00 for now; 0xFF: ignored)
//          u32 requestlength
//          RequestLengthType reqtype
// Return:  u8  status
// Engineer: Quyen Leba
//------------------------------------------------------------------------------
u8 obd2can_gm_request_download(u32 ecm_id, u8 dataformat,
                               u32 requestaddress, u32 requestlength,
                               RequestLengthType reqtype,
                               VehicleCommLevel commlevel,
                               u16 *maxdownloadblocklength)
{
    Obd2can_ServiceData servicedata;
    u8 status;

    if (maxdownloadblocklength)
    {
        //GM doesn't give out this info
        *maxdownloadblocklength = INVALID_MEMBLOCK_SIZE;
    }
    
    obd2can_servicedata_init(&servicedata);
    servicedata.ecm_id = ecm_id;
    servicedata.service = GMLAN_RequestDownload;
    servicedata.subservice = (u8)dataformat;
    servicedata.tester_present_type = TESTER_PRESENT_NODE;
    if (reqtype == Request_3_Byte)
    {
        servicedata.service_controldatabuffer[0] = (u8)(requestlength >> 16);
        servicedata.service_controldatabuffer[1] = (u8)(requestlength >> 8);
        servicedata.service_controldatabuffer[2] = (u8)(requestlength & 0xFF);
        servicedata.service_controldatalength = 3;
    }
    else if (reqtype == Request_4_Byte)
    {
        servicedata.service_controldatabuffer[0] = (u8)(requestlength >> 24);
        servicedata.service_controldatabuffer[1] = (u8)(requestlength >> 16);
        servicedata.service_controldatabuffer[2] = (u8)(requestlength >> 8);
        servicedata.service_controldatabuffer[3] = (u8)(requestlength & 0xFF);
        servicedata.service_controldatalength = 4;
    }
    else
    {
        log_push_error_point(0x3003);
        return S_BADCONTENT;
    }

    status = obd2can_txrx(&servicedata);
    return status;
}

//------------------------------------------------------------------------------
// RequestUpload ($35)
// Inputs:  u32 ecm_id
//          u32 requestaddress
//          u32 requestlength
//          RequestLengthType reqtype (UNUSED)
//          VehicleCommLevel commlevel
// Output:  u16 *maxuploadblocklength
// Return:  u8  status
// Engineer: Quyen Leba
//------------------------------------------------------------------------------
u8 obd2can_gm_request_upload(u32 ecm_id,
                             u32 requestaddress, u32 requestlength,
                             RequestLengthType reqtype,
                             VehicleCommLevel commlevel,
                             u16 *maxuploadblocklength)
{
    Obd2can_ServiceData servicedata;
    u8 status;

    if (maxuploadblocklength)
    {
        //GM don't give out this length
        *maxuploadblocklength = 0xFFFF;
    }
    
    obd2can_servicedata_init(&servicedata);
    servicedata.ecm_id = ecm_id;
    servicedata.service = GMLAN_RequestUpload;
    servicedata.subservice = 0x01;  //dataformat
    servicedata.tester_present_type = TESTER_PRESENT_NODE;
    
    servicedata.service_controldatabuffer[0] = (u8)(requestaddress >> 16);
    servicedata.service_controldatabuffer[1] = (u8)(requestaddress >> 8);
    servicedata.service_controldatabuffer[2] = (u8)(requestaddress & 0xFF);
    servicedata.service_controldatalength = 3;
    
    //example: [35] [01] [3:address]
    status = obd2can_txrx(&servicedata);
    return status;
}

//------------------------------------------------------------------------------
// TransferData Upload ($36)
// Inputs:  u32 ecm_id
//          u8  blocktracking (UNUSED)
//          VehicleCommLevel commlevel (UNUSED)
//          u32 request_address
//          RequestLengthType request_type
// Outputs: u8  *data
//          u16 *datalength
// Return:  u8  status
// Engineer: Quyen Leba
//------------------------------------------------------------------------------
u8 obd2can_gm_transfer_data_upload(u32 ecm_id, u8 blocktracking,
                                   VehicleCommLevel commlevel,
                                   u32 request_address,
                                   RequestLengthType request_type,
                                   u8 *data, u16 *datalength)
{
    obd2can_rxinfo rxinfo;
    u8 status;

    *datalength = 0;
    obd2can_rxinfo_init(&rxinfo);
    rxinfo.rxbuffer = data;
    rxinfo.cmd = (u8)GMLAN_TransferData;
    rxinfo.response_cmd_offset = 0;
    rxinfo.first_frame_data_offset = 5;     //0x01 ADDR[3] [1:?]
    rxinfo.tester_present_type = TESTER_PRESENT_BROADCAST;
    status = obd2can_rx(ECM_RESPONSE_ID(ecm_id), &rxinfo);
    if (status == S_SUCCESS)
    {
        *datalength = rxinfo.rxlength;
    }
    return status;
}

//------------------------------------------------------------------------------
// TransferData Download ($36)
// Inputs:  u32 ecm_id
//          Obd2can_SubServiceNumber subservice (download or execute)
//          RequestLengthType reqtype
// Return:  u8  status
// Engineer: Quyen Leba
//------------------------------------------------------------------------------
u8 obd2can_gm_transfer_data_download(u32 ecm_id, Obd2can_SubServiceNumber subservice,
                                     u8 blocktracking, u32 startingaddr,
                                     MemoryAddressType addrtype,
                                     u8 *data, u16 datalength,
                                     VehicleCommLevel commlevel,
                                     bool response_expected)
{
    Obd2can_ServiceData servicedata;
    u8 status;

    obd2can_servicedata_init(&servicedata);
    servicedata.ecm_id = ecm_id;
    servicedata.service = GMLAN_TransferData;
    servicedata.subservice = (u8)subservice;
    servicedata.txdata = data;
    servicedata.txdatalength = datalength;
    servicedata.response_expected = response_expected;

    if (addrtype == Address_24_Bits)
    {
        servicedata.service_controldatabuffer[0] = (u8)(startingaddr >> 16);
        servicedata.service_controldatabuffer[1] = (u8)(startingaddr >> 8);
        servicedata.service_controldatabuffer[2] = (u8)(startingaddr & 0xFF);
        servicedata.service_controldatalength = 3;
    }
    else if (addrtype == Address_32_Bits)
    {
        servicedata.service_controldatabuffer[0] = (u8)(startingaddr >> 24);
        servicedata.service_controldatabuffer[1] = (u8)(startingaddr >> 16);
        servicedata.service_controldatabuffer[2] = (u8)(startingaddr >> 8);
        servicedata.service_controldatabuffer[3] = (u8)(startingaddr & 0xFF);
        servicedata.service_controldatalength = 4;
    }
    else
    {
        log_push_error_point(0x3004);
        return S_BADCONTENT;
    }

    status = obd2can_txrx(&servicedata);
    return status;
}

//------------------------------------------------------------------------------
// RequestTransferExit ($37)
// Inputs:  u32 ecm_id
//          VehicleCommLevel commlevel
// Return:  u8  status
// Engineer: Quyen Leba
//------------------------------------------------------------------------------
u8 obd2can_gm_request_transfer_exit(u32 ecm_id, VehicleCommLevel commlevel)
{
    //NOTE: GM doesn't have $37 (so far)
    return S_FAIL;
}

//------------------------------------------------------------------------------
// WriteDataByIdentifier ($3B)
// Inputs:  u32 ecm_id
// Return:  u8  status
// Engineer: Quyen Leba
//------------------------------------------------------------------------------
u8 obd2can_gm_write_data_by_identifier(u32 ecm_id, u8 identifier,
                                       u8 *data, u16 datalength)
{
    Obd2can_ServiceData servicedata;
    u8 status;

    obd2can_servicedata_init(&servicedata);
    servicedata.ecm_id = ecm_id;
    servicedata.service = GMLAN_WriteDataByIdentifier;
    servicedata.subservice = identifier;
    servicedata.txdata = data;
    servicedata.txdatalength = datalength;
    status = obd2can_txrx(&servicedata);

    return status;
}

//------------------------------------------------------------------------------
// TesterPresent ($3E)
// Inputs:  u32 ecm_id
// Return:  u8  status
// Engineer: Quyen Leba
//------------------------------------------------------------------------------
u8 obd2can_gm_tester_present(bool broadcast, u32 ecm_id)
{
    Obd2can_ServiceData servicedata;
    u8 status;

    obd2can_servicedata_init(&servicedata);
    servicedata.ecm_id = ecm_id;
    servicedata.nowait_cantx = TRUE;
    servicedata.broadcast = TRUE;
    servicedata.service = GMLAN_TesterPresent;
    if (servicedata.broadcast)
    {
        servicedata.response_expected = FALSE;
    }
    status = obd2can_txrx(&servicedata);

    return status;
}

//------------------------------------------------------------------------------
// ReportProgrammedState ($A2)
// Inputs:  bool broadcast
//          u32 ecm_id
// Output:  GMLAN_ProgrammedStateStatus *programmedstate
// Return:  u8  status
// Engineer: Quyen Leba
//------------------------------------------------------------------------------
u8 obd2can_gm_report_programmedstate(bool broadcast, u32 ecm_id,
                                     GMLAN_ProgrammedStateStatus *programmedstate)
{
    Obd2can_ServiceData servicedata;
    u8 status;

    obd2can_servicedata_init(&servicedata);
    servicedata.ecm_id = ecm_id;
    servicedata.broadcast = broadcast;
    servicedata.service = GMLAN_ReportProgrammedState;
    status = obd2can_txrx(&servicedata);
    if (status == S_SUCCESS && programmedstate &&
        servicedata.rxdatalength >= 1)
    {
        *programmedstate = (GMLAN_ProgrammedStateStatus)servicedata.rxdata[0];
    }

    return status;
}

//------------------------------------------------------------------------------
// Request to enter(enable) programming mode ($A5)
// Inputs:  bool broadcast
//          u16 ecm_type
//          bool highspeedmode
//Return:   u8  status
// Engineer: Quyen Leba
// Note: so far, we only request normal speed (0x01)
//------------------------------------------------------------------------------
u8 obd2can_gm_programmingmode(bool broadcast, u16 ecm_type, bool highspeedmode)
{
    Obd2can_ServiceData servicedata;
    VehicleCommLevel commlevel;
    u32 ecm_id;
    u8 status;

    ecm_id = ECM_GetEcmId(ecm_type);
    commlevel = ECM_GetCommLevel(ecm_type);
    obd2can_servicedata_init(&servicedata);
    servicedata.ecm_id = ecm_id;
    servicedata.broadcast = broadcast;
    servicedata.service = GMLAN_ProgammingMode;

    //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    //requestProgrammingMode (sub_func: either 0x01 or 0x02)
    //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    if (highspeedmode == FALSE)
    {
        servicedata.subservice = requestProgrammingMode;
    }
    else
    {
        servicedata.subservice = requestProgrammingMode_HighSpeed;
    }
    status = obd2can_txrx(&servicedata);
    if (status != S_SUCCESS)
    {
        log_push_error_point(0x321A);
        return status;
    }

    delays(2000, 'm');
    obd2can_testerpresent(TRUE,ecm_id,commlevel);
    delays(2000, 'm');
    obd2can_testerpresent(TRUE,ecm_id,commlevel);
    delays(2000, 'm');
    obd2can_testerpresent(TRUE,ecm_id,commlevel);
    delays(2000, 'm');
    obd2can_testerpresent(TRUE,ecm_id,commlevel);
    delays(2000, 'm');
    obd2can_testerpresent(TRUE,ecm_id,commlevel);
    delays(2000, 'm');
    obd2can_testerpresent(TRUE,ecm_id,commlevel);

    //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    //enableProgrammingMode (sub_func: 0x03), no response from ECM
    //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    servicedata.subservice = enableProgrammingMode;
    servicedata.response_expected = FALSE;
    if (status == S_SUCCESS)
    {
        servicedata.subservice = enableProgrammingMode;
        status = obd2can_txrx(&servicedata);
        delays(20,'m');     // required
    }

    return status;
}

//------------------------------------------------------------------------------
// Read Diagnostic Information ($A9)
// Inputs:  u32 ecm_id (ecm_id is 7Ex only; i.e. 7E0, 7E2, etc...)
// Output:  dtc_info *dtcinfo
// Return:  u8  status
// Engineer: Quyen Leba
//------------------------------------------------------------------------------
u8 obd2can_gm_read_diagnostic_information(u32 ecm_id, dtc_info *dtcinfo)
{
    const u8 txframe[3] = {0xA9, 0x81, 0xDA};
    u8  rxrawdata[8];
    u32 response_ecm_id;
    u16 dtccode;
    u32 time_mscount;
    u32 timeout_mscount;
    u32 i;
    u8  status;

    if (dtcinfo == NULL)
    {
        log_push_error_point(0x3005);
        return S_INPUT;
    }

    dtcinfo->count = 0;
    status = obd2can_tx(ecm_id,(u8*)txframe,3,FALSE);
    if (status != S_SUCCESS)
    {
        log_push_error_point(0x3006);
        return status;
    }

    timeout_mscount = 1000;
    time_mscount = rtc_getvalue();
    while(1)
    {
        //TODOQ: there's retry in obd2can_rxraw but the correct way to hangle
        //it is timeout by 7F 78
        status = obd2can_rxraw(&response_ecm_id,rxrawdata);
        if (status == S_TIMEOUT)
        {
            //status = S_SUCCESS;
            //break;
            if ((rtc_getvalue() - time_mscount) > timeout_mscount)
            {
                status = S_SUCCESS;
                break;
            }
        }
        else if (response_ecm_id == ECM_RESPONSE_ID(ecm_id))        //i.e. 7Ex
        {
            if (rxrawdata[1] == 0x7F && rxrawdata[2] == 0xA9 &&
                rxrawdata[3] == 0x78)
            {
                time_mscount = rtc_getvalue();
                timeout_mscount = 4*1000;
                obd2can_gm_tester_present(TRUE, ecm_id);
            }
            else if (rxrawdata[1] == 0xE9)
            {
                //TODOQ: I'm not sure if this case will happen, maybe when there's no codes
            }
        }
        else if (response_ecm_id == ECM_2ND_RESPONSE_ID(ecm_id))    //i.e. 5Ex
        {
            if (rxrawdata[0] == 0x81)
            {
                dtccode = ((u16)rxrawdata[1] << 8) | ((u16)rxrawdata[2]);
                if (dtccode == 0)
                {
                    break;
                }
                else
                {
                    for(i=0;i<dtcinfo->count;i++)
                    {
                        if (dtcinfo->codes[i] == dtccode)
                        {
                            break;
                        }
                    }
                    if (i >= dtcinfo->count)    //new code found
                    {
                        dtcinfo->codes[dtcinfo->count] = dtccode;
                        dtcinfo->count++;
                        if (dtcinfo->count >= DTC_MAX_COUNT)
                        {
                            break;
                        }
                    }//if (i >= dtcinfo->count)...
                }
            }//if (rxrawdata[0] == 0x81)...
        }
        else
        {
            //TODO: timeout here too
        }
    }//while(1)...
    return status;
}

//------------------------------------------------------------------------------
// Read Data by PacketId Request ($AA)
// Inputs:  u32 ecm_id (ecm_id is 7Ex only; i.e. 7E0, 7E2, etc...)
//          u8  requesttype (see GMLAN_SubServiceNumber for $AA)
//          u8  *packetidlist
//          u8  packetidcount
// Return:  u8  status
// Engineer: Quyen Leba, Tristen Pierson
//------------------------------------------------------------------------------
u8 obd2can_gm_read_data_by_packetid_request(u32 ecm_id, u8 requesttype,
                                            u8 *packetidlist, u8 packetidcount)
{
    Obd2can_ServiceData servicedata;
    u8 rxrawdata[8];
    u32 response_id;
    u8 status;

    obd2can_servicedata_init(&servicedata);
    servicedata.ecm_id = ecm_id;
    servicedata.service = GMLAN_ReadDataByPacketIdentifier;
    servicedata.subservice = (SubServiceNumber)requesttype;    
    servicedata.response_expected = FALSE;
    
    switch ((SubServiceNumber)requesttype)
    {
        // NOTE: Periodic rate (scheduling rate) is defined as the time between
        // any two consecutive messages of the SAME DPID when it is scheduled.
        case scheduleAtSlowRate_$AA:    // Periodic rate = 1000ms
        case scheduleAtMediumRate_$AA:  // Periodic rate = 200ms
        case scheduleAtFastRate_$AA:    // Periodic rate = 25ms
            servicedata.txdata = packetidlist;
            servicedata.txdatalength = packetidcount;
            status = obd2can_txrx(&servicedata);            
            delays(250,'m');
            obd2can_gm_datalog_sendtesterpresent();
            break;
        case sendOneResponse_$AA:       // Schedule a one-shot response
            servicedata.txdata = packetidlist;
            servicedata.txdatalength = 1;
            status = obd2can_txrx(&servicedata);            
            break;
        case stopSending_$AA:           // Stop all scheduled DPIDs         
            response_id = ECM_2ND_RESPONSE_ID(ecm_id);
            status = obd2can_txrx(&servicedata);
            if (status == S_SUCCESS)
            {        
                status = obd2can_rxraw(&response_id, rxrawdata);
                if (status == S_SUCCESS)
                {
                    if (response_id != ECM_2ND_RESPONSE_ID(ecm_id) || 
                        rxrawdata[0] != 0x00)
                    {
                        status = S_FAIL;
                    }
                }
            }
            break;
        default:
            status = S_INPUT;
            break;
    }
        
    return status;
}

//------------------------------------------------------------------------------
// DeviceControl ($AE)
// Inputs:  u32 ecm_id
//          u8  cpid (control packet identifier)
//          u8  *controlbytes (output control data)
//          u8  datalength (length of control bytes, usually 5)
//          bool response_expected
// Return:  u8  status
//          u8  *controlbytes (error code for S_REJECT)
// Engineer: Tristen Pierson
//------------------------------------------------------------------------------
u8 obd2can_gm_device_control(u32 ecm_id, u8 cpid, u8 *controlbytes, u16 datalength,
                             bool response_expected)
{
    Obd2can_ServiceData servicedata;
    u8 status;

    obd2can_servicedata_init(&servicedata);
    servicedata.ecm_id = ecm_id;
    servicedata.service = GMLAN_DeviceControl;
    servicedata.subservice = cpid;
    servicedata.txdata = controlbytes;
    servicedata.txdatalength = datalength;
    
    if (response_expected == FALSE)
    {
        servicedata.response_expected = FALSE;
        return obd2can_txrx(&servicedata);
    }
    
    status = obd2can_txrx(&servicedata);

    if (status == S_SUCCESS && servicedata.rxdatalength >= 2)
    {
       if (servicedata.rxdata[1] != cpid)
       {
           status = S_FAIL;
       }
    }    
    else if (status != S_SUCCESS)
    {
        switch (servicedata.errorcode)
        {
        case 0x12:      // Invalid Format
            status = S_NOTFIT;
            break;
        case 0x31:      // Request out of range
            status = S_OUTOFRANGE;
            break;            
        case 0xE3:      // Device Control Limits Exceeded 
            controlbytes[0] = servicedata.rxdata[0];
            controlbytes[1] = servicedata.rxdata[1];           
            status = S_REJECT;
            break;
        default:
            break;
        }
    }            
        
    return status;
}

//------------------------------------------------------------------------------
// Read VIN
// Output:  u8  *vin (min length: VIN_LENGTH+1)
// Return:  u8  status
// Engineer: Quyen Leba
//------------------------------------------------------------------------------
u8 obd2can_gm_readvin(u8 *vin)
{
    Obd2can_ServiceData servicedata;
    u8  tmpbuffer[256];
    u8  tmpvin[VIN_LENGTH+1];
    u16 tmpbufferlength;
    bool isfirstread;
    u8  retry;
    u8  readmatchcount;
    u8  status;

    memset((char*)tmpvin,0,sizeof(tmpvin));
    //do multiple VIN read and check they're matched before accept VIN
    readmatchcount = 0;
#define READVIN_MATCHCOUNT_MAX          2
#define READVIN_RETRY_MAX               3
    retry = READVIN_RETRY_MAX;
    isfirstread = TRUE;

obd2can_gm_readvin_retry:
    status = obd2can_gm_read_data_by_identifier(FALSE,0x7E0,0x90,
                                                tmpbuffer,&tmpbufferlength);
    if ((status == S_SUCCESS) && (tmpbufferlength == (VIN_LENGTH+1)))
    {
        //tmpbuffer: [0x90][17:VIN]
        memcpy(vin,(char*)&tmpbuffer[1],VIN_LENGTH);
        vin[VIN_LENGTH] = NULL;
    }
    else
    {
        
        obd2can_servicedata_init(&servicedata);
        servicedata.nowait_cantx = TRUE;
        servicedata.ecm_id = 0x7E0;
        servicedata.service = 0x09;
        servicedata.subservice = 0x02;
        servicedata.first_frame_data_offset = 2;
        status = obd2can_txrx(&servicedata);
        
        if (status == S_SUCCESS && servicedata.rxdatalength != VIN_LENGTH)
        {
            status = S_VININVALID;
        }
        memcpy(vin,servicedata.rxdata,VIN_LENGTH);
        vin[VIN_LENGTH] = NULL;
    }

    if (obd2_validatevin(vin) != S_SUCCESS)
    {
        //give it some extra time if VIN is blank or invalid
        delays(500,'m');
    }
    
    if (isfirstread)
    {
        //first time read VIN in this function
        //need another read to compare VIN read
        isfirstread = FALSE;
        memcpy(tmpvin,vin,VIN_LENGTH);
        tmpvin[VIN_LENGTH] = NULL;
        //give it some extra time between 1st and 2nd read
        delays(500,'m');
        goto obd2can_gm_readvin_retry;
    }
    
    if (memcmp((char*)tmpvin,(char*)vin,VIN_LENGTH) != 0)
    {
        //2 consecutive VIN read give 2 different VIN, wait & try again
        memcpy(tmpvin,vin,VIN_LENGTH);
        tmpvin[VIN_LENGTH] = NULL;
        if (--retry != 0)
        {
            //give it even more extra time
            delays(3000,'m');
            readmatchcount = 0;
            goto obd2can_gm_readvin_retry;
        }
        else
        {
            log_push_error_point(0x3052);
            return S_FAIL;
        }
    }
    else
    {
        retry = READVIN_RETRY_MAX;
        readmatchcount++;
        if (readmatchcount == READVIN_MATCHCOUNT_MAX)
        {
            return status;
        }
        else
        {
            goto obd2can_gm_readvin_retry;
        }
    }
}

/**
 *  @brief Read ECM info from OBD2 info
 *  
 *  @param [in]  ecm_info   ECM info structure
 *
 *  @return status
 *  
 *  @details This function reads all available part numbers and serial number.
 */

/* TODO: Combine this with obd2vpw_gm_readecminfo function since they do the
 * same thing.
 */
u8 obd2can_gm_readecminfo(ecm_info *ecm)
{
    ecm->ecm_count = gObd2info.ecu_count;
    memcpy(ecm->vin, gObd2info.vin, sizeof(ecm->vin));
    
    for(u8 i = 0; i < ECM_MAX_COUNT; i++)
    {
        if (gObd2info.ecu_block[i].ecutype != EcuType_Unknown)
        {
            ecm->commtype[i] = gObd2info.ecu_block[i].commtype;
            ecm->commlevel[i] = gObd2info.ecu_block[i].commlevel;
            ecm->codecount[i] = gObd2info.ecu_block[i].partnumber_count;
            
            for (u8 j = 0; j < ecm->codecount[i]; j++)
            {
                ecm->codeids[i][j] = gObd2info.ecu_block[i].partnumbers.gm.partnumber_id[j];
                memcpy(ecm->codes[i][j], 
                       gObd2info.ecu_block[i].partnumbers.gm.partnumber[j], 
                       sizeof(ecm->codes[i][j])); 
            }
            
            memcpy(ecm->vehicle_serial_number, 
                   gObd2info.ecu_block[i].partnumbers.gm.vehicle_serial_number, 
                   sizeof(ecm->vehicle_serial_number)); 
            memcpy(ecm->hwid, 
                   gObd2info.ecu_block[i].partnumbers.gm.hwid, 
                   sizeof(ecm->hwid)); 
            memcpy(ecm->bcc, 
                   gObd2info.ecu_block[i].partnumbers.gm.bcc, 
                   sizeof(ecm->bcc));                
        }
    }
    
    return S_SUCCESS;  
}

//------------------------------------------------------------------------------
// Inputs:  bool broadcast
//          u32 ecm_id
//          VehicleCommLevel commlevel
// Return:  none
// Engineer: Quyen Leba
// Note: always broadcast
//------------------------------------------------------------------------------
void obd2can_gm_testerpresent(bool broadcast, u32 ecm_id,
                              VehicleCommLevel commlevel)
{
    obd2can_gm_tester_present(broadcast, ecm_id);
    delays(30,'m');     //this delay is important
}

//------------------------------------------------------------------------------
// Read all available software partnumbers
// Read and store in this order: OSNumber, CalId, Hardware, and upto 7 Segments
// Input:   u32 ecm_id  (7E0, 7E2, etc)
// Output:  partnumber_info *partnumbers
// Return:  u8  status
// Engineer: Quyen Leba
//------------------------------------------------------------------------------
u8 obd2can_gm_readSoftwarePartNumber(u32 ecm_id, partnumber_info *partnumbers)
{
    u8 identifiers[MAX_PARTNUMBER_COUNT] = {0xC1,0xCB,0xC0,     //Q: must be in this order
                                            0xC2,0xC3,0xC4,0xC5,0xC6,0xC7,0xC8};
    u8 databuffer[128];
    u16 datalength;
    u8 i;
    u8 status;
    u8 codecount;
    u32ext code;
    
    codecount = 0;
    for (i=0;i<MAX_PARTNUMBER_COUNT;i++)
    {
        partnumbers->id[codecount] = identifiers[i];
        partnumbers->number[codecount] = 0;

        status = obd2can_gm_read_data_by_identifier(FALSE, ecm_id,
                                                    identifiers[i],
                                                    databuffer, &datalength);
        if (status == S_SUCCESS &&
            databuffer[0] == identifiers[i] && datalength == 5)
        {
            code.c[0] = databuffer[4];
            code.c[1] = databuffer[3];
            code.c[2] = databuffer[2];
            code.c[3] = databuffer[1];
            
            partnumbers->number[codecount] = code.i;
            codecount++;
        }
        else if (status == S_ERROR)
        {
            codecount++;
        }
    }
    partnumbers->count = codecount;

    return S_SUCCESS;
}

//------------------------------------------------------------------------------
// Read vehicle serial number
// Output:  u8  *serialnumber (must be allocated with at least 32 bytes,
//              however, only 12 bytes + NULL used for GM
//          u8  *bcc (must be allocated with at least 5 bytes)
// Return:  u8  status
// Engineer: Quyen Leba
//------------------------------------------------------------------------------
u8 obd2can_gm_readSerialNumber(u8 *serialnumber, u8 *bcc)
{
    u8 status;
    u8 databuffer[128];
    u16 datalength;

    memset(serialnumber,0,32);
    bcc[0] = NULL;
    
    status = obd2can_gm_read_data_by_identifier(FALSE, 0x7E0, 0xB4,
                                                databuffer, &datalength);
    //databuffer should look like:
    //[0]0xB4,[1..]serial
    if (status == S_SUCCESS && databuffer[0] == 0xB4)
    {
        memcpy(bcc,(void*)&databuffer[3],4);
        bcc[4] = NULL;
        memcpy(&serialnumber[0],(void*)&databuffer[1],2);
        memcpy(&serialnumber[2],(void*)&databuffer[7],10);
        if(serialnumber[11] == 0x20)
        {
            serialnumber[11] = 0x30;
        }
    }
    else
    {
        status = S_FAIL;
        serialnumber[0] = NULL;
    }
    
    return status; 
}

//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
u8 obd2can_gm_readdtc(u32 ecm_id, dtc_info *dtcinfo)
{
    VehicleCommLevel commlevel;
    u8 status;
    
    OemType tempOem;
    status = obd2can_getcommlevelinfo(ecm_id, &commlevel, &tempOem);
    
    if (status == S_SUCCESS && commlevel == CommLevel_Unknown)
    {   
        return obd2can_generic_readdtc(ecm_id, dtcinfo);
    }
    if (status == S_SUCCESS && commlevel == CommLevel_GMLAN)
    {
        return obd2can_gm_read_diagnostic_information(ecm_id,dtcinfo);
    }
    
    return S_FAIL;
}

/**
 *  @brief Clear DTCs on GM CAN processors
 *
 *  @return Status
 */
u8 obd2can_gm_cleardtc()
{
    u8 i;
    u8 status;

    obd2_open();

    for (i = 0; i < ECM_MAX_COUNT; i++)
    {
        if (gObd2info.ecu_block[i].ecutype != EcuType_Unknown)
        {
            switch(gObd2info.ecu_block[i].commlevel)
            {
            case CommLevel_GMLAN:
                status = obd2can_gm_clear_diagnostic_information(TRUE, gObd2info.ecu_block[i].ecu_id);
                break;
            default:
                status = S_COMMLEVEL;
                break;
            }
            /* Is there another ecu? If so, a 3s delay is needed before
             * clearing its DTCs.
             */
            if (i < gObd2info.ecu_count)
            {
                delays(3000,'m');
            }
        }
    }
    return status;
}

//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
u8 obd2can_gm_disableallnormalcomm()
{
    obd2can_gm_disablenormalcommunication(TRUE,0x7E0,FALSE);
    return S_SUCCESS;
}

//------------------------------------------------------------------------------
// Utility file is a small worker loaded (downloaded) to ECM to help upload
// and download tune (i.e. read/write ECM flash memory). Not all ECMs requires.
// Inputs:  u32 ecm_type (defined for ecm_defs)
//          u8  utilitychoiceindex
//          bool isdownloadutility (TRUE: utility is for download tune)
// Return:  u8  status
// Engineer: Quyen Leba
// Date: Jan 10, 2008
// Note: data blocks are sent backward (last block first)
//------------------------------------------------------------------------------
u8 obd2can_gm_downloadutility(u32 ecm_type, u8 utilitychoiceindex,
                              bool isdownloadutility)
{
#define UTILITY_BLOCKSIZE               0x200
    Obd2can_SubServiceNumber subservice;
    F_FILE *fptr;
    u8  *utilityfilename;
    u32 utilitydatastartposition;   //extracted from ubf
    u32 utilitydatalength;
    bool useencryption;
    u32 blockaddress;
    u32 calladdress;
    u8  blockindex;
    u8  databuffer[UTILITY_BLOCKSIZE];
    u32 utilfilesize;
    u32 utilblockcount;
    u32 currentblockaddr;
    u32 tmp_u32;
    s32 blocklength;
    u32 dataindex;
    u32 ecm_id;
    bool require_separate_execution;    //TRUE if separate execute instruction required
    bool execution_has_response;        //TRUE if there's a positive response
    bool allow_negative_response;
    bool response_expected;
    u8  status;
    u32 decryptblocklength;

    response_expected = TRUE;
    execution_has_response = FALSE;
    allow_negative_response = FALSE;
    blockindex = 0;     //IMPORTANT: GM CAN utility has only 1 block
                        //when multi block required, put a loop of blockindex
    
    if (isdownloadutility)
    {
        utilityfilename = (u8*)ECM_GetDUtiFilename(ecm_type,utilitychoiceindex);
        useencryption = (bool)ECM_IsDUtiUseEncryption(ecm_type,utilitychoiceindex);
        calladdress = ECM_GetDUtiCallAddress(ecm_type,utilitychoiceindex);
        blockaddress = ECM_GetDUtiBlockAddr(ecm_type,utilitychoiceindex,blockindex);
    }
    else
    {
        utilityfilename = (u8*)ECM_GetUUtiFilename(ecm_type,utilitychoiceindex);
        useencryption = (bool)ECM_IsUUtiUseEncryption(ecm_type,utilitychoiceindex);
        calladdress = ECM_GetUUtiCallAddress(ecm_type,utilitychoiceindex);
        blockaddress = ECM_GetUUtiBlockAddr(ecm_type,utilitychoiceindex,blockindex);
    }
    if (utilityfilename == NULL)
    {
        return S_UTILITYNOTREQUIRED;
    }
    else if (utilityfilename[0] == NULL)
    {
        return S_UTILITYNOTREQUIRED;
    }
    
    if(useencryption)
    {
        status = ubf_validate_file(utilityfilename);
        if (status != S_SUCCESS)
        {
            log_push_error_point(0x3050);
            return status;
        }
    }
    
    if((fptr = genfs_default_openfile(utilityfilename, "rb")) == NULL)
    {
        log_push_error_point(0x3010);
        return S_OPENFILE;
    }
    if ((fseek(fptr,0,SEEK_END)) != 0)
    {
        log_push_error_point(0x3011);
        status = S_SEEKFILE;
        goto obd2can_gm_downloadutility_done;
    }
    utilfilesize = ftell(fptr);
    if (fseek(fptr, 0, SEEK_SET) != 0)
    {
        log_push_error_point(0x3012);
        status = S_SEEKFILE;
        goto obd2can_gm_downloadutility_done;
    }
    if (utilfilesize == 0)
    {
        log_push_error_point(0x3013);
        status = S_FAIL;
        goto obd2can_gm_downloadutility_done;
    }
    
    if (useencryption)
    {
        status = ubf_get_data_info(fptr,
                                   &utilitydatastartposition,&utilitydatalength);
        if (status != S_SUCCESS)
        {
            log_push_error_point(0x3019);
            goto obd2can_gm_downloadutility_done;
        }
        else if (utilitydatalength == 0)
        {
            log_push_error_point(0x301A);
            status = S_BADCONTENT;
            goto obd2can_gm_downloadutility_done;
        }
        else if (utilitydatastartposition+utilitydatalength > utilfilesize)
        {
            log_push_error_point(0x301B);
            status = S_BADCONTENT;
            goto obd2can_gm_downloadutility_done;
        }
    }
    else
    {
        utilitydatastartposition = 0;
        utilitydatalength = utilfilesize;
    }

    //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    //TODOQ: separate this from U & D
    ecm_id = ECM_GetEcmId(ecm_type);
    require_separate_execution = (bool)ECM_IsUUtiSeparateExe(ecm_type,utilitychoiceindex);
    execution_has_response = (bool)ECM_IsUUtiExeHasResponse(ecm_type,utilitychoiceindex);
    allow_negative_response = (bool)ECM_IsDUtiExeHasResponse(ecm_type,utilitychoiceindex);

    delays(300,'m');
    obd2can_gm_disablenormalcommunication(TRUE, ecm_id,TRUE);
    delays(200,'m');
    obd2can_testerpresent(TRUE,ecm_id,CommLevel_GMLAN);
    status = obd2can_gm_programmingmode(TRUE, ecm_type, FALSE);
    if (status != S_SUCCESS)
    {
        log_push_error_point(0x3014);
        goto obd2can_gm_downloadutility_done;
    }

    delays(100,'m');
    obd2can_gm_tester_present(TRUE, ecm_id);
    delays(100,'m');
    obd2can_gm_tester_present(TRUE, ecm_id);
    delays(50,'m');

    //Note: length and address are swapped, only blockaddress is actually used
    //TODOQ: check that swap
    status = obd2can_gm_request_download(ecm_id, 0xFF,
                                         0,blockaddress,Request_4_Byte,
                                         ECM_GetCommLevel(ecm_type),
                                         NULL);
    if (status == S_SUCCESS)
    {
        //now send data
        //INPORTANT: download utility data from bottom up
        utilblockcount = utilitydatalength / UTILITY_BLOCKSIZE;
        tmp_u32 = utilblockcount * UTILITY_BLOCKSIZE;
        blocklength = utilitydatalength - tmp_u32;
        if (blocklength > 0)
        {
            utilblockcount++;
        }
        dataindex = utilitydatastartposition + utilitydatalength;

        //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
        //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
        //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
        while(utilblockcount > 0)
        {
            if (blocklength == 0)
            {
                blocklength = UTILITY_BLOCKSIZE;
            }
            //TODOQ4: make sure blocklength is multiple of 8 bytes

            dataindex -= blocklength;
            utilblockcount--;
            currentblockaddr = blockaddress + (dataindex - utilitydatastartposition);

            response_expected = TRUE;
            subservice = download;
            if (utilblockcount == 0)
            {
                if (require_separate_execution == FALSE)
                {
                    response_expected = execution_has_response;
                    subservice = downloadAndExecuteOrExecute;
                }
            }
            
            if (fseek(fptr, dataindex, SEEK_SET) != 0)
            {
                log_push_error_point(0x3015);
                status = S_SEEKFILE;
                goto obd2can_gm_downloadutility_done;
            }
            
            if (useencryption)
            {
                decryptblocklength = blocklength;
                
                if(blocklength%8)
                    decryptblocklength += (8-(blocklength%8)); // Check blocklength for even mutltiple of 8 byte  
                
                if(ubf_read_block(databuffer, decryptblocklength, fptr) != S_SUCCESS)
                {
                    log_push_error_point(0x3018);
                    status = S_READFILE;
                    goto obd2can_gm_downloadutility_done;
                }
            }
            else
            {
                if ((fread(databuffer,1,blocklength, fptr)) < 1)
                {
                    log_push_error_point(0x3016);
                    status = S_READFILE;
                    goto obd2can_gm_downloadutility_done;
                }
            }
            //Note: blocktracking (0) is not used here
            status = obd2can_gm_transfer_data_download
                (ecm_id, subservice, 0,  currentblockaddr, Address_32_Bits,
                 databuffer, blocklength, ECM_GetCommLevel(ecm_type),
                 response_expected);
            if (response_expected && allow_negative_response &&
                status == S_ERROR)
            {
                //negative response is accepted in this case
                status = S_SUCCESS;
            }

            //E40 does not require this
            delays(80,'m'); obd2can_testerpresent(FALSE,ecm_id,CommLevel_GMLAN);
            delays(80,'m'); obd2can_testerpresent(FALSE,ecm_id,CommLevel_GMLAN);
            delays(80,'m'); obd2can_testerpresent(FALSE,ecm_id,CommLevel_GMLAN);

            blocklength = 0;
            if (status != S_SUCCESS)
            {
                break;
            }
        }//while(utilblockcount > 0)...
    }//if (status == S_SUCCESS)...
    else
    {
        log_push_error_point(0x301C);
        goto obd2can_gm_downloadutility_done;
    }

    if (status == S_SUCCESS && require_separate_execution == TRUE)
    {
        //Note: blocktracking (0) is not used here
        status = obd2can_gm_transfer_data_download
            (ecm_id, downloadAndExecuteOrExecute, 0, calladdress,
             Address_32_Bits, NULL, 0, ECM_GetCommLevel(ecm_type),
             execution_has_response);
        if (response_expected && allow_negative_response &&
            status == S_ERROR)
        {
            //TODOQ: use a flag to check this
            //negative response is accepted in this case
            status = S_SUCCESS;
        }
        delays(1050,'m');
        //IMPORTANT: This 1 sec delay is a must & no more command after this
    }
    genfs_closefile(fptr);
    fptr = NULL;
    
    delays(80,'m'); obd2can_testerpresent(FALSE,ecm_id,CommLevel_GMLAN);
    delays(80,'m'); obd2can_testerpresent(FALSE,ecm_id,CommLevel_GMLAN);
    delays(80,'m'); obd2can_testerpresent(FALSE,ecm_id,CommLevel_GMLAN);
    
    if (ECM_IsUtilityCheck(ecm_type))
    {
        // For older utilities, just check for 1A 21 and 1A BB responses
        if (ECM_IsUtilCheckLegacy(ecm_type))
        {            
            status = obd2can_gm_checkutility(ecm_type, UtilTypeLegacy);
        }
        // All other utilities have hard coded constants for download and upload
        else if (isdownloadutility)
        {
            status = obd2can_gm_checkutility(ecm_type, UtilTypeDownload);
        }
        else
        {
            status = obd2can_gm_checkutility(ecm_type, UtilTypeUpload);
        }
        if (status != S_SUCCESS)
        {
            log_push_error_point(0x3017);
            status = S_FAIL;
            goto obd2can_gm_downloadutility_done;
        }
    }
    
obd2can_gm_downloadutility_done:
    if (fptr)
    {
        genfs_closefile(fptr);
    }
    return status;
}

//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
u8 obd2can_gm_checkutility(u32 ecm_type, u8 utility_type)
{
    Obd2can_ServiceData servicedata;
    u8 status;
    u16 ecm_def;
    
    obd2can_servicedata_init(&servicedata);
    servicedata.ecm_id = ECM_GetEcmId(ecm_type);
    servicedata.service = 0x1A;
    servicedata.subservice = 0x21;
    status = obd2can_txrx(&servicedata);

    if (status == S_SUCCESS)
    {
        switch (utility_type)
        {
            case UtilTypeLegacy:
                delays(400,'m');    
                servicedata.subservice = 0xBB;
                status = obd2can_txrx(&servicedata);
                break;
            case UtilTypeUpload:
            case UtilTypeDownload:
            case UtilTypeBoth:
                if (servicedata.rxdatalength == 3)
                {
                    ecm_def = (servicedata.rxdata[0] >> 8) + servicedata.rxdata[1];
                    if (servicedata.rxdata[2] == utility_type && ecm_type == ecm_def)
                    {
                        status = S_SUCCESS;
                    }
                    else
                    {
                        status = S_FAIL;
                    }
                }
                else
                {
                    status = S_FAIL;
                }
                break;
            default:
                status = S_FAIL;
                break;
        }
    }
    return status;
}

//------------------------------------------------------------------------------
// Some ECM requires utility file to upload/download tune, and requires this
// function to take ECM out of utility-file mode
// Input:   u32 ecm_type (defined in ecm_defs)
// Return:  u8  status
// Engineer: Quyen Leba
// Date: Jan 10, 2008
//------------------------------------------------------------------------------
u8 obd2can_gm_exitutility(u32 ecm_type)
{
    Obd2can_ServiceData servicedata;
    u8 status;

    obd2can_servicedata_init(&servicedata);
    servicedata.ecm_id = ECM_GetEcmId(ecm_type);
    servicedata.service = 0x1A;
    servicedata.subservice = 0x55;
    servicedata.response_expected = FALSE;
    status = obd2can_txrx(&servicedata);

    delays(200,'m');
    obd2can_gm_reset_ecm(ecm_type);
    delays(1000,'m');

    return status;
}

//------------------------------------------------------------------------------
// Some ECM requires a finalization after a download
// Input:   u32 ecm_type (defined in ecm_defs)
// Return:  u8  status
// Engineer: Quyen Leba
//------------------------------------------------------------------------------
u8 obd2can_gm_downloadfinalizing(u32 ecm_type)
{
    Obd2can_ServiceData servicedata;
    u8 status;

    obd2can_servicedata_init(&servicedata);
    servicedata.ecm_id = ECM_GetEcmId(ecm_type);
    servicedata.service = 0x36;
    //Note: can't set subservice to 0xFF
    servicedata.service_controldatabuffer[0] = 0xFF;
    servicedata.service_controldatalength = 1;
    servicedata.response_expected = TRUE;
    status = obd2can_txrx(&servicedata);

    return status;
}

//------------------------------------------------------------------------------
// Input:   u16 ecm_type (index of ecm_defs)
// Return:  u8  status
// Engineer: Quyen Leba
//TODOQ: clean up all those delays
//------------------------------------------------------------------------------
u8 obd2can_gm_setup_prior_upload(u16 ecm_type)
{
    u32 ecm_id;
    VehicleCommLevel commlevel;
    
    ecm_id = ECM_GetEcmId(ecm_type);
    commlevel = ECM_GetCommLevel(ecm_type);

    obd2can_gm_return_normal_mode(TRUE,ecm_id);
    delays(6500,'m');
    obd2can_testerpresent(TRUE,ecm_id,commlevel);

    //101 FE 02 1A B0   //CAN_GM_DiagAddress(0);
    obd2can_gm_read_data_by_identifier(TRUE,ecm_id,0xB0,NULL,NULL);
    delays(1500,'m');
    obd2can_gm_initiate_diagnostic_operation(TRUE,ecm_id,disableAllDTCs);
    obd2can_testerpresent(TRUE,ecm_id,commlevel);
    delays(1500,'m');
    obd2can_gm_disablenormalcommunication(TRUE,ecm_id,TRUE);
    obd2can_testerpresent(TRUE,ecm_id,commlevel);
    delays(1000,'m');
    obd2can_testerpresent(TRUE,ecm_id,commlevel);
    delays(1000,'m');
    obd2can_testerpresent(TRUE,ecm_id,commlevel);
    delays(20,'m');
    obd2can_testerpresent(TRUE,ecm_id,commlevel);
    delays(300,'m');
    obd2can_testerpresent(TRUE,ecm_id,commlevel);
    delays(600,'m');
    obd2can_testerpresent(TRUE,ecm_id,commlevel);
    delays(1500, 'm');
    obd2can_testerpresent(TRUE,ecm_id,commlevel);
    delays(1500, 'm');
    obd2can_testerpresent(TRUE,ecm_id,commlevel);

    return S_SUCCESS;
}

//------------------------------------------------------------------------------
// Input:   u16 ecm_type (index of ecm_defs)
// Return:  u8  status
// Engineer: Quyen Leba
//------------------------------------------------------------------------------
u8 obd2can_gm_setup_after_upload(u16 ecm_type)
{
    u32 ecm_id;

    ecm_id = ECM_GetEcmId(ecm_type);
    obd2can_gm_return_normal_mode(TRUE,ecm_id);
    return S_SUCCESS;
}

//------------------------------------------------------------------------------
// Inputs:  u16 ecm_type (index for ecm_defs)
// Return:  u8  status
// Engineer: Quyen Leba
//------------------------------------------------------------------------------
u8 obd2can_gm_setup_prior_download(u16 ecm_type)
{
    u8  unlockpriv[16]; //only SCP use this now, it uses 1st byte only
    u8 status;
    bool request_programming_mode;
    VehicleCommType commtype;
    VehicleCommLevel commlevel;
    u32 ecm_id;
    GMLAN_ProgrammedStateStatus programmedstate;
    
    request_programming_mode = FALSE;
    ecm_id = ECM_GetEcmId(ecm_type);
    commtype = ECM_GetCommType(ecm_type);
    commlevel = ECM_GetCommLevel(ecm_type);
    
    obd2can_gm_return_normal_mode(TRUE,ecm_id);
    delays(1000, 'm');
    //delays(6500, 'm');

    obd2can_testerpresent(TRUE,ecm_id,commlevel);
    //7Ex 02 1A B0  //CAN_GM_DiagAddress(0);
    obd2can_gm_read_data_by_identifier(FALSE,ecm_id,0xB0,NULL,NULL);
    delays(6500, 'm');

    // TODO: Shouldn't this status be checked somewhere within this function?
    status = obd2tune_unlockecm(UnlockTask_Download,
                                ecm_type,commtype,unlockpriv);
    // TODO: Add something like this?
    //if(status != S_SUCCESS)
    //{
    //    return(S_FAIL);
    //}
    
    obd2can_gm_disablenormalcommunication(TRUE,ecm_id,TRUE);
    delays(3000, 'm');
    obd2can_testerpresent(TRUE,ecm_id,commlevel);
    delays(3000, 'm');
    obd2can_testerpresent(TRUE,ecm_id,commlevel);
    delays(3000, 'm');

    //7Ex 02 1A B4   //
    obd2can_gm_read_data_by_identifier(FALSE,ecm_id,0xB4,NULL,NULL);
    delays(30, 'm');
    obd2can_gm_read_data_by_identifier(FALSE,ecm_id,0xB4,NULL,NULL);
    delays(30, 'm');
    obd2can_gm_read_data_by_identifier(FALSE,ecm_id,0xB4,NULL,NULL);
    delays(30, 'm');
    obd2can_gm_read_data_by_identifier(FALSE,ecm_id,0xB4,NULL,NULL);
    delays(30, 'm');
    obd2can_gm_read_data_by_identifier(FALSE,ecm_id,0xB4,NULL,NULL);
    delays(30, 'm');
    obd2can_gm_read_data_by_identifier(FALSE,ecm_id,0xB4,NULL,NULL);
    delays(30, 'm');

    obd2can_testerpresent(TRUE,ecm_id,commlevel);
    delays(1500, 'm');
    obd2can_testerpresent(TRUE,ecm_id,commlevel);
    delays(1500, 'm');
    obd2can_testerpresent(TRUE,ecm_id,commlevel);
    delays(1500, 'm');
    obd2can_testerpresent(TRUE,ecm_id,commlevel);
    delays(1500, 'm');

    if (!ECM_IsDUtiRequired(ecm_type))
    {
        request_programming_mode = TRUE;
    }
    
    if (request_programming_mode)
    {
        //Note: if using utility file, this step will be handled there
        //CAN_GM_ReportProgrammedState(0);
        obd2can_gm_report_programmedstate(TRUE,ecm_id,&programmedstate);
        obd2can_testerpresent(TRUE,ecm_id,commlevel);
        delays(1500, 'm');
        obd2can_testerpresent(TRUE,ecm_id,commlevel);
        status = obd2can_gm_programmingmode(FALSE,ecm_type,FALSE);
        if(status == S_FAIL)
        {
            return(S_FAIL);
        }
    }

    delays(2000, 'm');
    obd2can_testerpresent(TRUE,ecm_id,commlevel);
    delays(2000, 'm');
    obd2can_testerpresent(TRUE,ecm_id,commlevel);
    delays(2000, 'm');
    obd2can_testerpresent(TRUE,ecm_id,commlevel);
    delays(2000, 'm');
    obd2can_testerpresent(TRUE,ecm_id,commlevel);
    delays(2000, 'm');
    obd2can_testerpresent(TRUE,ecm_id,commlevel);
    delays(2000, 'm');
    obd2can_testerpresent(TRUE,ecm_id,commlevel);

    obd2can_gm_initiate_diagnostic_operation(FALSE,ecm_id,disableAllDTCs);
    obd2can_testerpresent(TRUE,ecm_id,commlevel);
    obd2can_gm_report_programmedstate(FALSE,ecm_id,&programmedstate);
    
    delays(1500, 'm');
    obd2can_testerpresent(TRUE,ecm_id,commlevel);
    delays(1500, 'm');
    obd2can_testerpresent(TRUE,ecm_id,commlevel);
    delays(1500, 'm');
    obd2can_testerpresent(TRUE,ecm_id,commlevel);
    delays(1500, 'm');
    obd2can_testerpresent(TRUE,ecm_id,commlevel);
    delays(200, 'm');
    obd2can_testerpresent(TRUE,ecm_id,commlevel);
    delays(100, 'm');

    return S_SUCCESS;
}

#if 0
/*
u8 obd2can_gm_setup_prior_download_ori(u16 ecm_type)
{
    u8 status;
    u32 ecm_id;
    bool request_programming_mode;
    VehicleCommLevel commlevel;
    GMLAN_ProgrammedStateStatus programmedstate;
    
    request_programming_mode = FALSE;
    ecm_id = ECM_GetEcmId(ecm_type);
    commlevel = ECM_GetCommLevel(ecm_type);
    
    obd2can_gm_return_normal_mode(TRUE,ecm_id);
    //delays(6500, 'm');
    obd2can_testerpresent(TRUE,ecm_id,commlevel);
    //101 FE 02 1A B0   //CAN_GM_DiagAddress(0);
    obd2can_gm_read_data_by_identifier(TRUE,ecm_id,0xB0,NULL,NULL);
    delays(1500, 'm');
    obd2can_gm_initiate_diagnostic_operation(TRUE,ecm_id,disableAllDTCs);
    obd2can_testerpresent(TRUE,ecm_id,commlevel);
    delays(1500, 'm');
    obd2can_gm_disablenormalcommunication(TRUE,ecm_id,TRUE);
    obd2can_testerpresent(TRUE,ecm_id,commlevel);
    delays(1500, 'm');

    if (ECM_GetDUtiFilename(ecm_type) == NULL)
    {
        request_programming_mode = TRUE;
    }
    else if (ECM_GetDUtiFilename(ecm_type)[0] == NULL)
    {
        request_programming_mode = TRUE;
    }
    
    if (request_programming_mode)
    {
        //Note: if using utility file, this step will be handled there
        //CAN_GM_ReportProgrammedState(0);
        obd2can_gm_report_programmedstate(TRUE,ecm_id,&programmedstate);
        obd2can_testerpresent(TRUE,ecm_id,commlevel);
        delays(1500, 'm');
        obd2can_testerpresent(TRUE,ecm_id,commlevel);
        //status = obd2can_gm_programmingmode(FALSE,ecm_id,FALSE);
        status = obd2can_gm_programmingmode(TRUE,ecm_id,FALSE);
        if(status == S_FAIL)
        {
            return(S_FAIL);
        }
    }

    obd2can_testerpresent(TRUE,ecm_id,commlevel);
    delays(100, 'm');
    obd2can_gm_initiate_diagnostic_operation(TRUE,ecm_id,disableAllDTCs);
    obd2can_testerpresent(TRUE,ecm_id,commlevel);
    obd2can_gm_report_programmedstate(FALSE,ecm_id,&programmedstate);
    
    delays(1500, 'm');
    obd2can_testerpresent(TRUE,ecm_id,commlevel);
    delays(200, 'm');
    obd2can_testerpresent(TRUE,ecm_id,commlevel);
    delays(100, 'm');

    return S_SUCCESS;
}
*/
#endif

//------------------------------------------------------------------------------
// Input:   u16 ecm_type (index of ecm_defs)
// Return:  u8  status
// Engineer: Quyen Leba
//------------------------------------------------------------------------------
u8 obd2can_gm_setup_after_download(u16 ecm_type)
{
    u32 ecm_id;
    
    ecm_id = ECM_GetEcmId(ecm_type);
    obd2can_gm_return_normal_mode(TRUE,ecm_id);
    return S_SUCCESS;
}

//------------------------------------------------------------------------------
// Reset an ECM by ecm_type
// Inputs:  u8  ecm_type
// Return:  u8  status
// Engineer: Quyen Leba
//------------------------------------------------------------------------------
u8 obd2can_gm_reset_ecm(u8 ecm_type)
{
    u32 ecm_id;
    VehicleCommType commtype;
    VehicleCommLevel commlevel;

    ecm_id = ECM_GetEcmId(ecm_type);
    commtype = ECM_GetCommType(ecm_type);
    commlevel = ECM_GetCommLevel(ecm_type);

    if (commtype != CommType_CAN)
    {
        return S_COMMTYPE;
    }
    
    if (commlevel == CommLevel_GMLAN)
    {
        obd2can_gm_ecu_reset_request(FALSE,ecm_id,hardReset,TRUE);
    }
    else
    {
        return S_COMMLEVEL;
    }
    delays(750, 'm');
    return S_SUCCESS;
}

//------------------------------------------------------------------------------
// Inputs:  u16 ecm_type
//          bool is_cal_only
// Return:  u8  status
// Engineer: Quyen Leba
//TODOQ: T43 handled differently, check it again
//------------------------------------------------------------------------------
u8 obd2can_gm_erase_ecm(u16 ecm_type, bool is_cal_only)
{
    Obd2can_ServiceData servicedata;
    u8 status;

    obd2can_servicedata_init(&servicedata);
    servicedata.ecm_id = ECM_GetEcmId(ecm_type);
    servicedata.service = GMLAN_TransferData;
    servicedata.subservice = 0xEE;
    servicedata.service_controldatalength = 1;
    servicedata.initial_wait_extended = 30;
    servicedata.busy_wait_extended = 3;

    if (is_cal_only)
    {
        if (ECM_GetEraseCalCmd(ecm_type).count != 1)
        {
            return S_NOTSUPPORT;
        }
        servicedata.service_controldatabuffer[0] = ECM_GetEraseCalCmd(ecm_type).data[0];
    }
    else
    {
        if (ECM_GetEraseAllCmd(ecm_type).count != 1)
        {
            return S_NOTSUPPORT;
        }
        servicedata.service_controldatabuffer[0] = ECM_GetEraseAllCmd(ecm_type).data[0];
    }
    
    status = obd2can_txrx(&servicedata);

    return status;
}

//------------------------------------------------------------------------------
// Validate OSC PID
// Inputs:  u32 ecm_id
//          DlxBlock *dlxblock
//          VehicleCommLevel commlevel
// Return:  u8  status
// Engineer: Tristen Pierson
//------------------------------------------------------------------------------
u8 obd2can_gm_validate_oscpid(u32 ecm_id, DlxBlock *dlxblock,
                              VehicleCommLevel commlevel)
{
    u8 status;
    u8 controlbytes[5];
        
    switch (commlevel)
    {
        case CommLevel_GMLAN:
            memset(controlbytes,0x00,sizeof(controlbytes)); 
            status = obd2can_gm_device_control(ecm_id, (u8)dlxblock->pidAddr,
                                               controlbytes, 5, TRUE);
            break;        
        default:
            status = S_COMMLEVEL;
            break;
    }
    
    return status;
}

//------------------------------------------------------------------------------
// Inputs:  u32 ecm_id
//          DlxBlock *dlxblock
//          VehicleCommType commtype,
//          VehicleCommLevel commlevel
// Return:  u8  status
// Engineer: Quyen Leba/Patrick Downs
// TODOQ: commlevel is currently not used
//------------------------------------------------------------------------------
u8 obd2can_gm_validatedmr(u32 ecm_id, DlxBlock *dlxblock,
                          VehicleCommLevel commlevel)
{ 
    u8 status;
    u8 buffer[4];         // Required for read; Maximum address size is 4 bytes
    u16 bufferlength;     // Required for read

    // Address type always reverts to 32-bit
    if (gDatalogmode.addressType != Address_24_Bits &&
        gDatalogmode.addressType != Address_32_Bits)
    {
        gDatalogmode.addressType = Address_32_Bits;
    }
    
    // Validate by mode 0x23
    status = obd2can_gm_read_memory_address(ecm_id, dlxblock->pidAddr, 
                                            dlxblock->pidSize,
                                            gDatalogmode.addressType,                                               
                                            buffer, &bufferlength, FALSE);

    if (status != S_SUCCESS && gDatalogmode.addressType == Address_32_Bits)
    {
        // Try 3-byte address (E40)        
        status = obd2can_gm_read_memory_address(ecm_id, dlxblock->pidAddr, 
                                                dlxblock->pidSize,
                                                Address_24_Bits,                                               
                                                buffer, &bufferlength, FALSE);
        
        if (status == S_SUCCESS)
        {
            gDatalogmode.addressType = Address_24_Bits;
        }
    }
        
    return status; 
}

//------------------------------------------------------------------------------
// Inputs:  u32 ecm_id
//          DlxBlock *dlxblock
//          VehicleCommType commtype,
//          VehicleCommLevel commlevel
// Return:  u8  status
// Engineer: Quyen Leba/Patrick Downs/Tristen Pierson
// Notes: This function validates DMA's by defining a DPID# then dynamically
//        defining a message. PIDs are validated simply by dynamically defining
//        a message.
// TODOQ: commlevel is currently not used
//------------------------------------------------------------------------------
u8 obd2can_gm_validateByRapidPacketSetup(u32 ecm_id, DlxBlock *dlxblock,
                          VehicleCommLevel commlevel)
{
    u8  status;
    u16 pidlist[1];    

    switch (dlxblock->pidType)
    {
        case PidTypeDMR:
             // Define PID by DMA
            status = obd2can_gm_definePIDByAddress(ecm_id,0xFE00,
                                                   dlxblock->pidAddr,
                                                   dlxblock->pidSize);
            // Test PID
            if (status == S_SUCCESS)
            {               
                pidlist[0] = 0xFE00;
                status = obd2can_gm_dynamicallydefinemessage(ecm_id,0xFE,pidlist,1);
            }
            break;
        case PidTypeRegular:
            pidlist[0] = (u16)dlxblock->pidAddr;
            status = obd2can_gm_dynamicallydefinemessage(ecm_id,0xFE,pidlist,1);
            break;
        default:
            status = S_FAIL;
            break;
    }
    
    return status;
}

//------------------------------------------------------------------------------
// Secondary ping
// Inputs:  u16 ecm_type
// Return:  u8  status
// Engineer: Quyen Leba
//------------------------------------------------------------------------------
u8 obd2can_gm_secondaryping(u32 ecm_id)
{
    Obd2can_ServiceData servicedata;
    u8 status;

    obd2can_servicedata_init(&servicedata);
    servicedata.ecm_id = ecm_id;
    servicedata.initial_wait_extended = 0;
    servicedata.rx_timeout_ms = 100;
    servicedata.service = GMLAN_ReadDataByIdentifier;
    servicedata.subservice = 0xC1;
    status = obd2can_txrx(&servicedata);

    if (status == S_SUCCESS || status == S_ERROR)
    {
        return S_SUCCESS;
    }
    return status;
}

/**
 *  @brief Get GM CAN OBD2 Info
 *  
 *  @param [in] obd2_info_block_t OBD2 info block
 *
 *  @return status
 */
u8 obd2can_gm_getinfo(obd2_info_block *block)
{
    u8  status;
    u16 ecm_type;
    u8 i;
    partnumber_info partnumber;

    memset((char*)block->partnumbers.gm.vehicle_serial_number,0,
           sizeof(block->partnumbers.gm.vehicle_serial_number));

    memset((char*)block->partnumbers.gm.hwid,0,
           sizeof(block->partnumbers.gm.hwid));
        
    block->partnumber_count = 0;

    partnumber.flags = PARTNUMBER_INFO_NOFLAG;

    if (block->commtype == CommType_CAN && block->commlevel != CommLevel_Unknown)
    {
       /* Get ECM Part Numbers */
        if (block->ecutype == EcuType_ECM)
        {
            status = obd2can_gm_readSoftwarePartNumber(block->ecu_id,&partnumber);
            if (status == S_SUCCESS && partnumber.count > 0)
            {
                u16 veh_type;
                u8  tmpbuffer[32];

                itoa(partnumber.number[1],tmpbuffer,10);            //0xCB
                status = obd2_gm_search_veh_type_by_hardware_id(tmpbuffer,&veh_type);
                if (status != S_SUCCESS)
                {
                    itoa(partnumber.number[0],tmpbuffer,10);        //0xC1
                    status = obd2_gm_search_ecm_type_by_cal_id(tmpbuffer,&veh_type);
                    if (status != S_SUCCESS)
                    {
                        itoa(partnumber.number[3],tmpbuffer,10);    //0xC2
                        status = obd2_gm_search_ecm_type_by_cal_id(tmpbuffer,&veh_type);
                    }
                }
                if (status == S_SUCCESS)
                {
                    switch(veh_type)
                    {
                    case 113:   //LBZ
                    case 114:   //LMM
                        partnumber.flags |= OSNUMBER_USE_C2;
                        break;
                    default:
                        break;
                    }
                }
                else if (gObd2info.vin != NULL)
                {
                    //not in known list, try to find it by VIN & unlock
                    ecm_type = INVALID_ECM_DEF;
                    if (gObd2info.vin[7] == '2')         //possibly LBZ
                    {
                        //this is LLY module but used as LBZ if year is 2006-2010
                        switch(gObd2info.vin[9])
                        {
                        case '6':
                        case '7':
                        case '8':
                        case '9':
                        case 'A':
                            ecm_type = 2;   // this is not binary type
                            break;
                        default:
                            break;
                        }
                    }
                    else if (gObd2info.vin[7] == 'D')    //possibly LBZ
                    {
                        ecm_type = 2;       // this is not binary type
                    }
                    else if (gObd2info.vin[7] == '6')    //possibly LMM
                    {
                        ecm_type = 3;       // this is not binary type
                    }
                    
                    if (ecm_type != INVALID_ECM_DEF)
                    {
                        delays(300,'m');
                        status = obd2can_unlockecm(UnlockTask_Upload,ecm_type,FALSE);
                        if (status == S_SUCCESS)
                        {
                            partnumber.flags |= OSNUMBER_USE_C2;
                        }
                    }
                }
                
                // LBZ/LMM ecm needs a switch
                // LBZ/LMM use 0xC2 as OS part number, 0xC1 is boot loader
                // (C3 switch is added as example but not used)
                if ((partnumber.flags & OSNUMBER_USE_C2) ||
                    (partnumber.flags & OSNUMBER_USE_C3))
                {
                    u8  switch_id;
                    u8  n1,n2;
                    u32 tmpcode;
                    
                    switch_id = 0xC2;
                    if (partnumber.flags & OSNUMBER_USE_C3)
                    {
                        switch_id = 0xC3;
                    }
                    
                    for(i=0,n1=0;i<partnumber.count;i++)
                    {
                        if (partnumber.id[i] == switch_id)
                        {
                            n1 = i;
                            break;
                        }
                    }
                    for(i=0,n2=0;i<partnumber.count;i++)
                    {
                        if (partnumber.id[i] == 0xC1)
                        {
                            n2 = i;
                            break;
                        }
                    }
                    if (n1 < partnumber.count && n2 < partnumber.count)
                    {
                        tmpcode = partnumber.number[n1];
                        partnumber.number[n1] = partnumber.number[n2];
                        partnumber.number[n2] = tmpcode;
                        
                        tmpcode = partnumber.id[n1];
                        partnumber.id[n1] = partnumber.id[n2];
                        partnumber.id[n2] = tmpcode;
                    }
                }

                block->partnumber_count = partnumber.count;
                for(i=0;i<partnumber.count;i++)
                {
                    block->partnumbers.gm.partnumber_id[i] = partnumber.id[i];
                    if (partnumber.number[i] == 0)
                    {
                        strcpy((char*)block->partnumbers.gm.partnumber[i],"N/A");
                    }
                    else
                    {
                        sprintf((char*)block->partnumbers.gm.partnumber[i],
                                "%08d",partnumber.number[i]);
                    }
                }            
            }
        }

        /* Get TCM Part Numbers */
        if (block->ecutype == EcuType_TCM)
        {
            status = obd2can_gm_readSoftwarePartNumber(block->ecu_id,&partnumber);
            if (status == S_SUCCESS && partnumber.count > 0)
            {
                block->partnumber_count = partnumber.count;
                for(i = 0; i < partnumber.count; i++)
                {
                    block->partnumbers.gm.partnumber_id[i] = partnumber.id[i];
                    if (partnumber.number[i] == 0)
                    {
                        strcpy((char*)block->partnumbers.gm.partnumber[i]  ,"N/A");
                    }
                    else
                    {
                        sprintf((char*)block->partnumbers.gm.partnumber[i],
                                "%08d",partnumber.number[i]);
                    }
                }              
            }        
        }
        obd2can_gm_readSerialNumber(block->partnumbers.gm.vehicle_serial_number,
                                    block->partnumbers.gm.bcc);
    }
    return S_SUCCESS;
}