/**
  **************** (C) COPYRIGHT 2011 SCT Performance, LLC *********************
  * File Name          : obd2_gm.c
  * Device / System    : SCT products using the "CommonCode" platform
  * Author             : Quyen Leba
  *
  * Version            : 1 
  * Date               : 05/02/2011
  * Description        : 
  *                    : 
  *
  *
  * History            : 05/02/2011 M. Davis
  *                    :   Add SCT Copyright header,
  *
  ******************************************************************************
  */

//------------------------------------------------------------------------------
// Log error point: 0x30xx -> 0x31xx
//------------------------------------------------------------------------------

#include <stdio.h>
#include <board/rtc.h>
#include <common/statuscode.h>
#include <common/obd2comm.h>
#include <common/file.h>
#include <common/itoa.h>
#include <common/cmdif_remotegui.h>
#include <fs/genfs.h>
#include "obd2_gm.h"

extern flasher_info *flasherinfo;                   //from obd2tune.c
extern cmdif_datainfo cmdif_datainfo_responsedata;  //from cmdif.c

//------------------------------------------------------------------------------
// Crank Relearn Timeouts
//------------------------------------------------------------------------------
#define CRANK_RELEARN_TIMEOUT       10000
#define CRANK_RELEARN_TIMEOUT_TP    2000
#define CRANK_RELEARN_REFRESH       500

//------------------------------------------------------------------------------
// Private prototypes
//------------------------------------------------------------------------------
u8 obd2_gm_search_veh_def_by_id_from_text_file(u8 *filename, u8 *id, u16 *veh_type);
u8 obd2_gm_crank_relearn_setup_packets(VehicleCommType commtype);

//------------------------------------------------------------------------------
// Inputs:  u8  *seed
//          u32 algoindex (index algorithm)
//          VehicleCommType commtype
// Output:  u8  *key
// Return:  u8  status
// Jan10,2008: QLB add algorithm 0x05, 0x75
//------------------------------------------------------------------------------
u8 obd2_gm_generatekey(u8 *seed, u32 algoindex, VehicleCommType commtype,
                       u8 *key)
{
#define ALGOINSTRUCTION_LENGTH              13
    F_FILE *fpt;
    u32 fileoffset;
    u8 algoinstruction[ALGOINSTRUCTION_LENGTH];
    u8 *bptr;
    u16 calc_key;
    u16 calc_temp;
    u8 c, i;
    u8 op_code, HH, LL;
    u8 tmp1_u8, tmp2_u8;
    u16 tmp1_u16, tmp2_u16;

    if (commtype == CommType_CAN)
    {
        if((fpt = genfs_default_openfile("SK_Algorithm.bin", "rb")) == NULL)
        {
            log_push_error_point(0x3007);
            return S_OPENFILE;
        }
    }
    else if (commtype == CommType_VPW)
    {
        if((fpt = genfs_default_openfile("SK_AlgoVPW.bin", "rb")) == NULL)
        {
            log_push_error_point(0x3008);
            return S_OPENFILE;
        }
    }
    else
    {
        log_push_error_point(0x3009);
        return S_NOCOMMUNICATION;
    }
    
    fileoffset = algoindex * ALGOINSTRUCTION_LENGTH;
    if(fseek(fpt, fileoffset, SEEK_SET) != 0)
    {
        genfs_closefile(fpt);
        log_push_error_point(0x300A);
        return S_SEEKFILE;
    }
    if((fread(algoinstruction,1,ALGOINSTRUCTION_LENGTH, fpt)) != ALGOINSTRUCTION_LENGTH)
    {
        genfs_closefile(fpt);
        log_push_error_point(0x300B);
        return S_READFILE;
    }
    genfs_closefile(fpt);
    
    calc_key = seed[0] << 8;
    calc_key += seed[1];
    bptr = algoinstruction;
    
    bptr++; //1st byte not used

    for(c=0; c<4; c++)
    {
        op_code = *(bptr++);
        HH = *(bptr++);
        LL = *(bptr++);

#define ALGOINSTRUCTION_SWAP                            0x05
#define ALGOINSTRUCTION_ADD_HHLL                        0x14
#define ALGOINSTRUCTION_COMPLEMENT                      0x2A
#define ALGOINSTRUCTION_ROL_BY_HH                       0x4C
#define ALGOINSTRUCTION_OR_OF_SR_BY_LL_SL_BY_16_LL      0x5C
#define ALGOINSTRUCTION_ROR_BY_LL                       0x6B
#define ALGOINSTRUCTION_SWAP_ADD                        0x75
#define ALGOINSTRUCTION_SWAP_SUB                        0x77
#define ALGOINSTRUCTION_SWAP_SUB2                       0xF8
#define ALGOINSTRUCTION_FLIP_ADD_COND                   0x7E
#define ALGOINSTRUCTION_SUBSTRACT_HHLL                  0x98

        switch(op_code)
        {
        case ALGOINSTRUCTION_SWAP:
            calc_temp = calc_key;
            tmp1_u16 = (u8)(calc_key & 0xFF);
            tmp1_u16 <<= 8;
            tmp1_u16 |= 0x00FF;
            calc_temp >>= 8;
            calc_temp |= 0xFF00;
            calc_temp &= (tmp1_u16);
            calc_key = calc_temp;
            break;
        case ALGOINSTRUCTION_ADD_HHLL:
            calc_temp = HH << 8;
            calc_temp += LL;
            calc_key += calc_temp;
            break;
        case ALGOINSTRUCTION_COMPLEMENT:    //if HH>LL use 2's complement, else use 1's complement
            if(LL > HH) // 2's complement
            {
                calc_key = (0xFFFF ^ calc_key);
                calc_key += 1;
            }
            else    // 1's complement
            {
                calc_key = (0xFFFF ^ calc_key);
            }
            break;
        case ALGOINSTRUCTION_ROL_BY_HH:     // Rotate Left by HH bits
            tmp2_u16 = calc_key;
            if(HH > 16)
            {
                tmp1_u8 = HH/16;
                tmp1_u8 = tmp1_u8 * 16;
                tmp2_u8 = HH - tmp1_u8;
            }
            else
                tmp2_u8 = HH;
            
            calc_temp = 0x00;
            for(i=0; i< tmp2_u8; i++)
            {
                tmp1_u16 = (tmp2_u16 & 0x8000);
                tmp1_u16 = ((tmp1_u16 >> 15) & 0x0001);
                calc_temp = (tmp2_u16 << 1);
                calc_key = calc_temp | tmp1_u16;
                tmp2_u16 = calc_key;
            }
            break;
        case ALGOINSTRUCTION_OR_OF_SR_BY_LL_SL_BY_16_LL:
            //Shift right by LL, reserve answer 1
            //Shift left by 16-LL, or answer 1 & answer 2.
            tmp1_u16 = calc_key;
            tmp2_u16 = calc_key;
            tmp1_u16 >>= LL;
            tmp2_u16 <<= (16-LL);
            calc_key = tmp1_u16 | tmp2_u16;
            break;
        case ALGOINSTRUCTION_ROR_BY_LL:     // Rotate Right by LL bits
            tmp2_u16 = calc_key;
            if(LL > 16)
            {
                tmp1_u8 = LL/16;
                tmp1_u8 = tmp1_u8 * 16;
                tmp2_u8 = LL - tmp1_u8;
            }
            else
                tmp2_u8 = LL;
            
            calc_temp = 0x00;
            for(i=0; i< tmp2_u8; i++)
            {
                tmp1_u16 = (tmp2_u16 & 0x0001);
                tmp1_u16 = ((tmp1_u16 << 15) & 0x8000);
                calc_temp = ((tmp2_u16 >> 1) & 0x7FFF);
                calc_key = calc_temp | tmp1_u16;
                tmp2_u16 = calc_key;
            }
            
            break;
        case ALGOINSTRUCTION_SWAP_ADD:      // Swap HH & LL then add to current calculated key value
            calc_temp = LL;
            calc_temp <<= 8;
            calc_temp += HH;
            calc_key += calc_temp;
            break;
        case ALGOINSTRUCTION_SWAP_SUB:      // Swap HH & LL then subtract from current calculated key value
        case ALGOINSTRUCTION_SWAP_SUB2:
            calc_temp = LL;
            calc_temp <<= 8;
            calc_temp += HH;
            calc_key -= calc_temp;
            break;
        case ALGOINSTRUCTION_FLIP_ADD_COND: // Flip hi/low bytes of current calculated key value.  Then if LL>HH add LLHH, else add HHLL
            calc_temp =  seed[1] << 8;
            calc_temp += seed[0];
            if(LL > HH)
            {
                calc_key = calc_temp;
                calc_temp =  LL << 8;
                calc_temp += HH;
                calc_key += calc_temp;
            }
            else
            {
                calc_key = calc_temp;
                calc_temp =  HH << 8;
                calc_temp += LL;
                calc_key += calc_temp;
            }
            break;
            
        case ALGOINSTRUCTION_SUBSTRACT_HHLL:
            calc_temp = HH << 8;
            calc_temp += LL;
            calc_key -= calc_temp;
            break;
        default:    // No-Operation
            break;
        }
        seed[0] = calc_key >> 8;
        seed[1] = calc_key;
    }
    key[0] = calc_key >> 8;
    key[1] = calc_key;

    return S_SUCCESS;
}

//------------------------------------------------------------------------------
// Process any vehicle specific tasks before program
// Input:   flasherinfo *f_info
// Return:  u8  status
// Engineer: Quyen Leba
//------------------------------------------------------------------------------
u8 obd2_gm_vehicle_specific_task_before_program(flasher_info *f_info)
{
    //nothing yet
    return S_SUCCESS;
}

//------------------------------------------------------------------------------
// Format vehicleinfo to text string
// Input:   vehicle_info *vehicleinfo
// Output:  u8  *vehicleinfo_text
// Return:  u8  status
// Engineer: Quyen Leba
//------------------------------------------------------------------------------
u8 obd2_gm_vehicleinfototext(vehicle_info *vehicleinfo, u8 *vehicleinfo_text)
{
    u8  commtype_text[32];
    u8  tmpbuffer[32];
    u8  *ptr;
    u32 i;
    u8  count;
    u8  status;

    ptr = vehicleinfo_text;
    strcpy((char*)vehicleinfo->vin,(char*)vehicleinfo->ecm.vin);
    obd2_vehiclecommtotext(vehicleinfo->commtype,commtype_text);
    
    strcpy((char*)ptr,"<HEADER>VIN:</HEADER> ");
    status = obd2_validatevin(vehicleinfo->vin);
    if (status == S_VINBLANK || status == S_VININVALID)
    {
        strcat((char*)ptr,"Blank");
    }
    else if (status == S_SUCCESS)
    {
        strcat((char*)ptr,(char*)vehicleinfo->vin);
    }
    else
    {
        strcat((char*)ptr,"N/A");
    }

    strcat((char*)ptr,"<HEADER>COMM:</HEADER> ");
    strcat((char*)ptr,(char*)commtype_text);
    strcat((char*)ptr,"<HEADER>ECU OS:</HEADER> ");
    if (vehicleinfo->ecm.ecm_count == 0 || vehicleinfo->ecm.codecount[0] == 0)
    {
        strcat((char*)ptr,"Not Found");
    }
    else
    {
        strcat((char*)ptr,(char*)vehicleinfo->ecm.codes[0][0]);
        if (vehicleinfo->ecm.codecount[0] >= 2)
        {
            strcat((char*)ptr," <HEADER>ECU CalID:</HEADER> ");
            strcat((char*)ptr,(char*)vehicleinfo->ecm.codes[0][1]);
        }
        
        if (vehicleinfo->ecm.second_codes[0][0][0] != NULL)
        {
            strcat((char*)ptr,"<HEADER>ECU SWPN:</HEADER> ");
            strcat((char*)ptr,(char*)vehicleinfo->ecm.second_codes[0][0]);
        }
        
        if (vehicleinfo->ecm.ecm_count > 1)
        {
            strcat((char*)ptr,"<HEADER>TCU OS:</HEADER> ");
            strcat((char*)ptr,(char*)vehicleinfo->ecm.codes[1][0]);
            
            if (vehicleinfo->ecm.codecount[0] >= 2)
            {
                strcat((char*)ptr," <HEADER>TCU CalID:</HEADER> ");
                strcat((char*)ptr,(char*)vehicleinfo->ecm.codes[1][1]);
            }
            
            if (vehicleinfo->ecm.second_codes[1][0][0] != NULL)
            {
                strcat((char*)ptr,"<HEADER>TCU SWPN:</HEADER> ");
                strcat((char*)ptr,(char*)vehicleinfo->ecm.second_codes[1][0]);
            }
        }

        if (vehicleinfo->ecm.codecount[0] > 1)
        {
            strcat((char*)ptr,"<HEADER>ECU PartNumbers:</HEADER> ");
            for(i=0,count=0;i<ECM_MAX_PART_COUNT;i++)
            {
                strcat((char*)ptr,(char*)vehicleinfo->ecm.codes[0][i]);
                strcat((char*)ptr," (");
                itoa(vehicleinfo->ecm.codeids[0][i],tmpbuffer,16);
                strcat((char*)ptr,(char*)tmpbuffer);
                strcat((char*)ptr,")");
                strcat((char*)ptr," - ");
                if (strcmp((char*)vehicleinfo->ecm.codes[0][i],"N/A") != 0)
                {
                    if (++count >= vehicleinfo->ecm.codecount[0])
                    {
                        break;
                    }
                }
            }//for(i=0...
            for(i=1;i<3;i++)    //just text cleanup
            {
                switch(*(ptr+strlen((char*)ptr)-i))
                {
                case ' ':
                case '-':
                    *(ptr+strlen((char*)ptr)-i) = NULL;
                    break;
                default:
                    break;
                }
            }
        }//if (vehicleinfo->ecm.codecount[0] > 1)...

        if (vehicleinfo->ecm.codecount[1] > 1)
        {
            strcat((char*)ptr,"<HEADER>TCU PartNumbers:</HEADER> ");
            for(i=0,count=0;i<ECM_MAX_PART_COUNT;i++)
            {
                strcat((char*)ptr,(char*)vehicleinfo->ecm.codes[1][i]);
                strcat((char*)ptr," (");
                itoa(vehicleinfo->ecm.codeids[1][i],tmpbuffer,16);
                strcat((char*)ptr,(char*)tmpbuffer);
                strcat((char*)ptr,")");
                if ((count+1) < vehicleinfo->ecm.codecount[1])
                {
                    strcat((char*)ptr," - ");
                }
                if (strcmp((char*)vehicleinfo->ecm.codes[0][i],"N/A") != 0)
                {
                    if (++count >= vehicleinfo->ecm.codecount[1])
                    {
                        break;
                    }
                }
            }//for(i=0...
        }//if (vehicleinfo->ecm.codecount[1] > 1)...
    }
    
    //######################################################################
    //
    //######################################################################
    
    if (vehicleinfo->ecm.hwid[0] != NULL && strlen((char*)vehicleinfo->ecm.hwid) < 16)
    {
        strcat((char*)ptr,"<HEADER>HardwareID:</HEADER> ");
        strcat((char*)ptr,(char*)vehicleinfo->ecm.hwid);
    }

    if (vehicleinfo->ecm.vehicle_serial_number[0] != NULL)
    {
        strcat((char*)ptr,"<HEADER>Serial:</HEADER> ");
        strcat((char*)ptr,(char*)vehicleinfo->ecm.vehicle_serial_number);
    }

    if (vehicleinfo->ecm.bcc[0] != NULL && strlen((char*)vehicleinfo->ecm.bcc) <= 4)
    {
        strcat((char*)ptr,"<HEADER>BCC:</HEADER> ");
        strcat((char*)ptr,(char*)vehicleinfo->ecm.bcc);
    }
    
    return S_SUCCESS;
}

//------------------------------------------------------------------------------
// Search for veh_type by id from a text file
// Inputs:  u8  *filename
//          u8  *id (in ASCII)
// Output:  u16 *veh_type
// Return:  u8  status
// Engineer: Quyen Leba
// Note: each line of text file is id and ecmtype separated by a comma without
// white spaces; ecmtype is decimal
// E.g.: 
//      09360361,118
//      09361140,118
//------------------------------------------------------------------------------
u8 obd2_gm_search_veh_def_by_id_from_text_file(u8 *filename, u8 *id, u16 *veh_type)
{
    F_FILE *fptr;
    u8  linedata[512];
    u32 linedatalength;
    bool continue_search;
    u8  *bptr;
    u32 tmp_u32;
    u8  status;

    *veh_type = INVALID_VEH_DEF;
    fptr = genfs_default_openfile(filename,"rb");
    if (!fptr)
    {
        log_push_error_point(0x300F);
        return S_OPENFILE;
    }

    continue_search = TRUE;
    do
    {
        status = file_readline(fptr,linedata,&linedatalength);
        if (status != S_SUCCESS)
        {
            goto obd2_gm_search_veh_def_by_id_from_text_file_done;
        }
        if (strncmp((char*)id,(char*)linedata,strlen((char*)id)) == 0)
        {
            continue_search = FALSE;
            bptr = (u8*)strstr((char*)linedata,",");
            if (bptr++)
            {
                while(*bptr == ' ')     //skip any white spaces
                {
                    bptr++;
                }
                tmp_u32 = 0;
                while(bptr[tmp_u32] != NULL)
                {
                    //must be digits until end of line
                    if (bptr[tmp_u32] < '0' || bptr[tmp_u32] > '9')
                    {
                        log_push_error_point(0x302B);
                        goto obd2_gm_search_veh_def_by_id_from_text_file_done;
                    }
                    tmp_u32++;
                }
                if (strlen((char*)bptr) == 0)
                {
                    log_push_error_point(0x302C);
                    goto obd2_gm_search_veh_def_by_id_from_text_file_done;
                }
                *veh_type = atoi((char*)bptr);
                status = S_SUCCESS;
            }
        }
    }while (continue_search);

obd2_gm_search_veh_def_by_id_from_text_file_done:
    genfs_closefile(fptr);
    return status;
}

//------------------------------------------------------------------------------
// Search for veh type by known hardware id
// Input:   u8  *hwid (in ASCII)
// Output:  u16 *veh_type
// Return:  u8  status
// Engineer: Quyen Leba
//------------------------------------------------------------------------------
u8 obd2_gm_search_veh_type_by_hardware_id(u8 *hwid, u16 *veh_type)
{
    return obd2_gm_search_veh_def_by_id_from_text_file("hardware.txt",
                                                       hwid, veh_type);
}

//------------------------------------------------------------------------------
// Search for ecm type by known hardware id
// Input:   u8  *calid (in ASCII)
// Output:  u16 *veh_type
// Return:  u8  status
// Engineer: Quyen Leba
//------------------------------------------------------------------------------
u8 obd2_gm_search_ecm_type_by_cal_id(u8 *calid, u16 *veh_type)
{
    return obd2_gm_search_veh_def_by_id_from_text_file("GM_OSlist.txt",
                                                       calid, veh_type);
}

//------------------------------------------------------------------------------
// Search for vehicle type using ECMs' hardware and OS part#
// Only use for 'Upload Stock' feature
// Return:  u8  status
// Engineer: Quyen Leba, Patrick Downs
//------------------------------------------------------------------------------
u8 obd2_gm_vehicle_type_search(ecm_info *ecm)
{
#define TOTAL_SEARCH_FILE_COUNT         2
    const u8 *searchfilenames[TOTAL_SEARCH_FILE_COUNT] = {HARDWARE_LOOKUP_FILE,OS_LOOKUP_FILE};
    const u8 tunecode_index[TOTAL_SEARCH_FILE_COUNT] = {2,0};
    F_FILE *fptr;
    u8  linedata[512];
    u32 linedatalength;
    u16 ecm_type_list[20];
    u8  ecm_type_list_count;
    u16 vehicle_type;
    u32 ecm_found;
    u8  ecm_found_count;
    u32 i,j;
    u8  fileindex;
    u8  *bptr;
    u8  status;
    u8  returnstatus;

    ecm_found_count = 0;
    ecm_found = 0;
    ecm_type_list_count = 0;
    returnstatus = S_SUCCESS;
        
    //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    // search files to find available ecm types
    //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    for(fileindex=0;fileindex<TOTAL_SEARCH_FILE_COUNT;fileindex++)
    {
        fptr = genfs_default_openfile((u8*)searchfilenames[fileindex],"rb");
        if (!fptr)
        {
            returnstatus = S_FILENOTFOUND;            
            goto obd2_gm_vehicle_type_search_done;
        }
        
        while(!feof(fptr))
        {
            status = file_readline(fptr,linedata,&linedatalength);
            if (status != S_SUCCESS)
            {
                returnstatus = S_NOLOOKUP;
                goto obd2_gm_vehicle_type_search_done;
            }
            
            bptr = (u8*)strstr((char*)linedata,",");
            if (bptr)
            {
                u16 reference_ecm_type;
                
                reference_ecm_type = (u16)atoi((char*)bptr+1);
                *bptr = NULL;
                
                for(i=0;i<ecm->ecm_count;i++)
                {
                    if (ecm_found & ((u32)1<<i))
                    {
                        continue;
                    }
                    if (strcmp((char*)linedata,
                               (char*)ecm->codes[i][tunecode_index[fileindex]]) == 0)
                    {
                        for(j=0;j<ecm_type_list_count;j++)
                        {
                            if (ecm_type_list[j] == reference_ecm_type)
                            {
                                //already added to list
                                break;
                            }
                        }
                        if (j >= ecm_type_list_count)
                        {
                            ecm_type_list[ecm_type_list_count++] = reference_ecm_type;
                            ecm_found |= ((u32)1<<i);
                            ecm_found_count++;
                            break;
                        }
                    }
                }//for(i=0;i<ecm.ecm_count;i++)...
            }//if (bptr)...
        }//while(!f_eof(fptr))...

        if (fptr)
        {
            genfs_closefile(fptr);
            fptr = NULL;
        }
        
        if (ecm_found_count == ecm->ecm_count)
        {
            break;
        }
    }//for(fileindex=0;fileindex<TOTAL_SEARCH_FILE_COUNT;fileindex++)
    
    //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    // 
    //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    status = vehdef_get_vehdef_info(ecm_type_list, ecm_type_list_count,&vehicle_type);
    if (status == S_SUCCESS)
    {
        if (ecm_found_count != ecm->ecm_count)
        {
            //at least one ecm is not in the supported list
            if (VEH_IsAllowSkipUnsupportedSubECM(vehicle_type))
            {
                flasherinfo->vehicle_type = vehicle_type;
                returnstatus = S_SUCCESS;
            }
            else
            {
                returnstatus = S_UNMATCH; //CMDIF_ACK_VEHICLE_UNSUPPORTED;
            }
            goto obd2_gm_vehicle_type_search_done;
        }
        
        flasherinfo->vehicle_type = vehicle_type;
        returnstatus = S_SUCCESS;
    }
    else
    {
        flasherinfo->vehicle_type = INVALID_VEH_DEF;
        returnstatus = S_UNMATCH; //CMDIF_ACK_VEHICLE_UNSUPPORTED;
    }
    
obd2_gm_vehicle_type_search_done:
    if (fptr)
    {
        genfs_closefile(fptr);
    }
    return returnstatus;
}

/**
 * @brief   Crank Relearn
 *  
 * @param   [in] commtype
 * @param   [in] commlevel
 *
 * @details Procedure:
 *          1. Make sure A/C off, parking break is applied, etc.
 *          2. Start engine.
 *          3. Wait until ECT > 70 degC
 *          4. Press gas pedal to floor (WOT) until fuel cut off RPM is reached
 *
 * @author  Tristen Pierson
 */
void obd2_gm_crank_relearn(VehicleCommType commtype,
                           VehicleCommLevel commlevel)
{    
    float RPM = 0;
    float ECT = 0;          // Engine Coolant Temperature
    float MaxRPM = 0;       // Maximum (peak) RPM
    float ENGINE_RT = 0;    // Engine Runtime
    u8  CDLS = 0;           // Crank Diagnostic Learning Status
    u8  AIR_COND = 0;       // A/C Status
    u8  seconds = 0;
    u8  minutes = 0;
    u8  status = 0;    
    u32 errorcode = 0;
    u32 response_ecm_id;
    u32 guiRefresh;
    u32 timeout = 0;
    u32 commandTimeout = 0;
    bool commandWait = FALSE;
    u32 testerpresent;
    u8  buffer[5];
    u8  rxrawdata[8];
    u8  i;   
    obd2vpw_rxinfo rxinfo;
    void (*fpTesterpresent)(void);    
    CMDIF_REMOTEGUI_RESPONSETYPE responsetype;
    CMDIF_COMMAND cmd = CMDIF_CMD_SPECIAL_FUNCTIONS;
    u16 responsechoice;    
    u8 displayData[150]; // 5 Lines, 30 characters per line
    u8 statusMsg[30];
                
    enum {
        startPageAbout,
        startPage1,
        startPage2,
        startPage3,
        startPage4,
        startDone,
        startAbort,
    } startPageState;
    
    enum {
        processDiagMode,
        processSetupPackets,
        processStart,
        processAirCondIsOn,
        processCommandSent,        
        processFail,
        processAbort,
        processSuccess,        
        processDone,
    } processState;
    
    // Crank Relearn Pre-Start Instructions
    startPageState = startPageAbout;
    while(startPageState != startDone)
    {
        switch (startPageState)
        {
        case startPageAbout:
            cmdif_remotegui_messagebox(cmd,
                                       "CRANK RELEARN", CMDIF_REMOTEGUI_ICON_NONE,
                                       CMDIF_REMOTEGUI_BUTTONFACE_Exit, CMDIF_REMOTEGUI_BUTTONFACE_Continue, CMDIF_REMOTEGUI_BUTTONFACE_None,
                                       CMDIF_REMOTEGUI_FOCUS_Button1,
                                       "<HEADER>NOTICE</HEADER>\nThis procedure should be performed following PCM replacement, " \
                                       "certain internal engine repairs, CKP sensor or trigger replacement, " \
                                       "clutch replacement, or when directed to by service bulletin or service manual.");
            cmdif_remotegui_wait_user_response(&responsetype, &responsechoice, NULL);
  
            if (responsetype == CMDIF_REMOTEGUI_RESPONSETYPE_BUTTON && 
                responsechoice == CMDIF_REMOTEGUI_BUTTONTYPE_RIGHT)
            {
                startPageState = startPage1;
            }
            else
            {
                startPageState = startAbort;
            }
            break; 
        case startPage1:
            cmdif_remotegui_messagebox(cmd,
                                       "CRANK RELEARN", CMDIF_REMOTEGUI_ICON_NONE,
                                       CMDIF_REMOTEGUI_BUTTONFACE_Exit, CMDIF_REMOTEGUI_BUTTONFACE_Continue, CMDIF_REMOTEGUI_BUTTONFACE_Back,
                                       CMDIF_REMOTEGUI_FOCUS_Button1,
                                       "<HEADER>STEP 1</HEADER>\nVerify no DTCs are present and engine misfire events are not occuring.");            
            cmdif_remotegui_wait_user_response(&responsetype, &responsechoice, NULL);

            if (responsetype == CMDIF_REMOTEGUI_RESPONSETYPE_BUTTON && 
                responsechoice == CMDIF_REMOTEGUI_BUTTONTYPE_RIGHT)
            {
                startPageState = startPage2;
            }
            else if (responsetype == CMDIF_REMOTEGUI_RESPONSETYPE_BUTTON && 
                     responsechoice == CMDIF_REMOTEGUI_BUTTONTYPE_MIDDLE) 
            {
                startPageState = startPageAbout;
            }
            else
            {
                startPageState = startAbort;
            }
            break; 
        case startPage2:
            cmdif_remotegui_messagebox(cmd,
                                       "CRANK RELEARN", CMDIF_REMOTEGUI_ICON_NONE,
                                       CMDIF_REMOTEGUI_BUTTONFACE_Exit, CMDIF_REMOTEGUI_BUTTONFACE_Continue, CMDIF_REMOTEGUI_BUTTONFACE_Back,
                                       CMDIF_REMOTEGUI_FOCUS_Button1,
                                       "<HEADER>STEP 2</HEADER>\nPlease return rev limiter, engine idle, and misfire parameters to stock.");            
            cmdif_remotegui_wait_user_response(&responsetype, &responsechoice, NULL);

            if (responsetype == CMDIF_REMOTEGUI_RESPONSETYPE_BUTTON && 
                responsechoice == CMDIF_REMOTEGUI_BUTTONTYPE_RIGHT)
            {
                startPageState = startPage3;
            }
            else if (responsetype == CMDIF_REMOTEGUI_RESPONSETYPE_BUTTON && 
                     responsechoice == CMDIF_REMOTEGUI_BUTTONTYPE_MIDDLE) 
            {
                startPageState = startPage1;
            }
            else
            {
                startPageState = startAbort;
            }
            break;       
        case startPage3:
            cmdif_remotegui_messagebox(cmd,
                                       "CRANK RELEARN", CMDIF_REMOTEGUI_ICON_NONE,
                                       CMDIF_REMOTEGUI_BUTTONFACE_Exit, CMDIF_REMOTEGUI_BUTTONFACE_Continue, CMDIF_REMOTEGUI_BUTTONFACE_Back,
                                       CMDIF_REMOTEGUI_FOCUS_Button1,
                                       "<HEADER>STEP 3</HEADER>\nStart and idle engine. Vehicle MUST remain in park or neutral. Brake MUST be applied when throttling to fuel cut off.");            
            cmdif_remotegui_wait_user_response(&responsetype, &responsechoice, NULL);

            if (responsetype == CMDIF_REMOTEGUI_RESPONSETYPE_BUTTON && 
                responsechoice == CMDIF_REMOTEGUI_BUTTONTYPE_RIGHT)
            {
                startPageState = startPage4;
            }
            else if (responsetype == CMDIF_REMOTEGUI_RESPONSETYPE_BUTTON && 
                     responsechoice == CMDIF_REMOTEGUI_BUTTONTYPE_MIDDLE) 
            {
                startPageState = startPage2;
            }
            else
            {
                startPageState = startAbort;
            }
            break;            
        case startPage4:
            cmdif_remotegui_messagebox(cmd,
                                       "CRANK RELEARN", CMDIF_REMOTEGUI_ICON_NONE,
                                       CMDIF_REMOTEGUI_BUTTONFACE_Exit, CMDIF_REMOTEGUI_BUTTONFACE_Continue, CMDIF_REMOTEGUI_BUTTONFACE_Back,
                                       CMDIF_REMOTEGUI_FOCUS_Button1,
                                       "<HEADER>STEP 4</HEADER>\nTurn A/C and defrosters off. Power steering or " \
                                       "accessory loads must not be present. Fuel cut off occurs " \
                                       "usually around 4000 RPMs. Refer to your service manual for " \
                                       "more information on fuel cut off. Select CONTINUE to begin.");            
            cmdif_remotegui_wait_user_response(&responsetype, &responsechoice, NULL);

            if (responsetype == CMDIF_REMOTEGUI_RESPONSETYPE_BUTTON && 
                responsechoice == CMDIF_REMOTEGUI_BUTTONTYPE_RIGHT)
            {
                // Prepare for operation
                startPageState = startDone;
                cmdif_remotegui_messagebox(cmd,
                                           "CRANK RELEARN", CMDIF_REMOTEGUI_ICON_NONE,
                                           CMDIF_REMOTEGUI_BUTTONFACE_None, CMDIF_REMOTEGUI_BUTTONFACE_None, CMDIF_REMOTEGUI_BUTTONFACE_None,
                                           CMDIF_REMOTEGUI_FOCUS_None,
                                           "<HEADER>INITIALIZING</HEADER>\nPlease wait...");
            }
            else if (responsetype == CMDIF_REMOTEGUI_RESPONSETYPE_BUTTON && 
                     responsechoice == CMDIF_REMOTEGUI_BUTTONTYPE_MIDDLE) 
            {
                startPageState = startPage3;
            }
            else
            {
                startPageState = startAbort;
            }
            break;
        case startAbort:
            return;
        default:
            startPageState = startAbort;
            break;
        }
    }
    
    // Crank Relearn Process State Machine
    processState = processDiagMode;
    while (processState != processDone)
    {
        switch (processState)
        {
        case processDiagMode:       // Enter diag mode, set tester present fp
            switch (commtype)
            {
            case CommType_VPW:        
                if (obd2vpw_enter_diag_mode() != S_SUCCESS)
                {
                    sprintf((void*)displayData, "<HEADER>ERROR</HEADER>\nFailed to initialize\0");
                    processState = processFail;
                }
                fpTesterpresent = &obd2vpw_datalog_sendtesterpresent;
                processState = processSetupPackets;
                break;
            case CommType_CAN:
                fpTesterpresent = &obd2can_gm_datalog_sendtesterpresent;
                processState = processSetupPackets;
                break;
            default:
                processState = processFail;
                break;
            }
            break;
        case processSetupPackets:   // Setup packets
            if (obd2_gm_crank_relearn_setup_packets(commtype) != S_SUCCESS)
            {
                sprintf((void*)displayData, "<HEADER>ERROR</HEADER>\nFailed to initialize\0");
                processState = processFail;
            }
            else
            {
                processState = processStart;
            }
            break;
        case processStart:          // Start crank relearn procedure                       
            sprintf((void*)statusMsg, "Waiting for minimum ECT\0");
            timeout = rtc_getvalue();
            testerpresent = timeout;
            guiRefresh = timeout;            
            // Main procedure loop
            while(1)
            {
                // Check for timeout
                if ((rtc_getvalue() - timeout) >= CRANK_RELEARN_TIMEOUT)
                {                    
                    sprintf((void*)displayData, "<HEADER>ERROR</HEADER>\nOperation timeout\0");
                    processState = processFail;                    
                    break;
                }
                // Check for timeout after command sent
                if (commandWait == TRUE && (rtc_getvalue() - commandTimeout) >= CRANK_RELEARN_TIMEOUT)
                {
                    sprintf((void*)displayData, "<HEADER>ERROR</HEADER>\nOperation timeout\0");
                    processState = processFail;                    
                    break;
                }
                // Handle Tester Present
                if ((rtc_getvalue() - testerpresent) >= CRANK_RELEARN_TIMEOUT_TP)
                {
                    (*fpTesterpresent)();
                    testerpresent = rtc_getvalue();                    
                }            
                // Get packet data
                switch (commtype)
                {
                case CommType_VPW:
                    // Read and parse all packets
                    for (i = 0; i < 2; i++)
                    {
                        obd2vpw_rxinfo_init(&rxinfo);
                        rxinfo.cmd = 0x2A;            
                        status = obd2vpw_rx(&rxinfo);
                        if (status == S_SUCCESS)
                        {
                            memcpy(rxrawdata, rxinfo.rxbuffer, rxinfo.rxlength);
                            switch (rxrawdata[0])
                            {
                            case 0xFE:
                                timeout = rtc_getvalue();
                                RPM = (float)((u16)(rxrawdata[1] << 8) | rxrawdata[2]);
                                RPM *= .25;
                                ECT = (float)rxrawdata[3];
                                ECT -= 40;                    
                                CDLS = rxrawdata[4];
                                break;
                            case 0xFD:
                                timeout = rtc_getvalue();
                                ENGINE_RT = (float)((u16)(rxrawdata[1] << 8) | rxrawdata[2]);
                                break;
                            default:
                                break;
                            }
                        }
                    } // for(..)
                    break;
                case CommType_CAN:        
                    status = obd2can_rxraw(&response_ecm_id, rxrawdata);                        
                    if (response_ecm_id != 0x5E8)
                    {
                        status = S_FAIL;
                    }
                    // Parse packet data: 0x05 0xE8 0xFE xx xx xx xx xx xx xx
                    // Only one packet to parse
                    if (status == S_SUCCESS && rxrawdata[0] == 0xFE)
                    {                    
                        timeout = rtc_getvalue();                        
                        RPM = (float)((u16)(rxrawdata[1] << 8) | rxrawdata[2]);
                        RPM *= .25;
                        ECT = (float)rxrawdata[3];
                        ECT -= 40;                    
                        CDLS = rxrawdata[4];
                        ENGINE_RT = (float)((u16)(rxrawdata[5] << 8) | rxrawdata[6]);                                            
                        AIR_COND = rxrawdata[7];
                    }  
                    break;
                }
                // Update GUI                
                if ((rtc_getvalue() - guiRefresh) >= CRANK_RELEARN_REFRESH)
                {                    
                    seconds = (u32)(ENGINE_RT) % 60;
                    minutes = (((u32)(ENGINE_RT) - seconds) / 60) % 60;
                    
                    sprintf((void*)displayData, "<HEADER>STEP 5</HEADER>\nRPM: %5.0F\nENGINE RUNTIME: %u:%02u\nECT: %3.0F C\n<HEADER>%s</HEADER>", RPM, minutes, seconds, ECT, statusMsg);
                    
                    cmdif_remotegui_messagebox(cmd,
                                               "CRANK RELEARN", CMDIF_REMOTEGUI_ICON_NONE,
                                               CMDIF_REMOTEGUI_BUTTONFACE_Abort, CMDIF_REMOTEGUI_BUTTONFACE_None, CMDIF_REMOTEGUI_BUTTONFACE_None,
                                               CMDIF_REMOTEGUI_FOCUS_None,
                                               displayData);
                    
                    guiRefresh = rtc_getvalue();
                }
                // Check for GUI response
                status = cmdif_remotegui_peek_user_response(&responsetype, &responsechoice, NULL);
                if (status == S_SUCCESS && responsetype == CMDIF_REMOTEGUI_RESPONSETYPE_BUTTON)
                {                    
                    processState = processAbort;
                    break;
                }
                // Check if A/C is on
                if (AIR_COND != 0)
                {                    
                    sprintf((void*)displayData, "<HEADER>ERROR</HEADER>\nA/C must be kept off\0");
                    processState = processFail;
                    break;
                }                
                // Is Engine Runtime > 15 Minutes?
                if (ENGINE_RT > 900)
                {                    
                    processState = processFail;
                    sprintf((void*)displayData, "<HEADER>ERROR</HEADER>\nEngine runtime exceeded\0");
                    break;
                }        
                // Check RPM
                if ((MaxRPM < RPM) && (MaxRPM > 3200.0) && (processState == processCommandSent))
                {
                    sprintf((void*)statusMsg, "Return to Idle RPM\0");
                    commandWait = TRUE;
                    commandTimeout = rtc_getvalue();
                }
                else
                {
                    MaxRPM = RPM;
                }                
                // Check if we're done
                if(CDLS == 0x80)
                {                    
                    if (processState != processCommandSent)
                    {
                        sprintf((void*)displayData, "<HEADER>NOTICE</HEADER>\nCrank relearn already successfully performed. Turn off vehicle and start again.\0");
                        processState = processFail;
                    }
                    else
                    {
                        processState = processSuccess;
                    }
                    break;
                }
                // Send command if minimum coolant temperature is met                
                if((ECT > 80.0) && (processState != processCommandSent))
                {                    
                    // Transmit Command
                    switch (commtype)
                    {
                    case CommType_CAN:
                        buffer[0] = 0x40;
                        status = obd2can_gm_device_control(Obd2CanEcuId_7E0, 0x10, buffer, 1, TRUE);
                        break;
                    case CommType_VPW:                        
                        memset(buffer,0x00,sizeof(buffer));
                        buffer[4] = 0x08;
                        status = obd2vpw_gm_device_control(0x01, buffer, sizeof(buffer));
                        break;
                    }
                    
                    if (status == S_SUCCESS)
                    {                        
                        sprintf((void*)statusMsg, "Rev engine to cut off RPM\0");
                        processState = processCommandSent;
                    }
                    else if ((status == S_REJECT) && (buffer[0] == 0x11))
                    {     
                        errorcode = (buffer[0] << 8) | buffer[1];
                        errorcode = 0x1106;
                        processState = processFail;
                        switch(errorcode)
                        {
                        case 0x1101:                            
                            sprintf((void*)displayData, "<HEADER>ERROR</HEADER>\nVehicle must remain in park or neutral.\0");
                            break;
                        case 0x1102:                            
                            sprintf((void*)displayData, "<HEADER>ERROR</HEADER>\nClear DTC's and start again.\0");
                            break;
                        case 0x1103:                            
                            sprintf((void*)displayData, "<HEADER>ERROR</HEADER>\nCoolant temperature not in range.\0");
                            break;
                        case 0x1104:
                        case 0x1105:                            
                            sprintf((void*)displayData, "<HEADER>ERROR</HEADER>\nNeed ECM restart. Turn off vehicle and start again.\0");
                            break;
                        case 0x1107:                            
                            sprintf((void*)displayData, "<HEADER>ERROR</HEADER>\nBrake is not applied.\0");
                            break;
                        default:
                            sprintf((void*)displayData, "<HEADER>ERROR</HEADER>\nDevice Control Error: %02X\0", errorcode);                            
                            break;
                        }                        
                        break;                
                    }
                    else
                    {                        
                        sprintf((void*)displayData, "<HEADER>ERROR</HEADER>\nCrank Relearn Failure\0");
                        processState = processFail;
                        break;
                    }              
                }
            } // while(1)
            break;    
        case processFail:           // Handle failure            
            if (displayData == NULL)
            {                
                sprintf((void*)displayData, "<HEADER>ERROR</HEADER>\nGeneral Failure\0");
            }                        
            cmdif_remotegui_messagebox(cmd,
                                       "CRANK RELEARN", CMDIF_REMOTEGUI_ICON_NONE,
                                       CMDIF_REMOTEGUI_BUTTONFACE_Exit, CMDIF_REMOTEGUI_BUTTONFACE_None, CMDIF_REMOTEGUI_BUTTONFACE_None,
                                       CMDIF_REMOTEGUI_FOCUS_None,
                                       displayData);        
            cmdif_remotegui_wait_user_response(&responsetype, &responsechoice, NULL);

            processState = processDone;
            break;
        case processAbort:          // Handle abort            
            cmdif_remotegui_messagebox(cmd,
                                       "CRANK RELEARN", CMDIF_REMOTEGUI_ICON_NONE,
                                       CMDIF_REMOTEGUI_BUTTONFACE_Exit, CMDIF_REMOTEGUI_BUTTONFACE_None, CMDIF_REMOTEGUI_BUTTONFACE_None,
                                       CMDIF_REMOTEGUI_FOCUS_None,
                                       "<HEADER>CRANK RELEARN ABORTED</HEADER>");        
            cmdif_remotegui_wait_user_response(&responsetype, &responsechoice, NULL);

            processState = processDone;
            break;
        case processSuccess:        // Handle success            
            cmdif_remotegui_messagebox(cmd,
                                       "CRANK RELEARN", CMDIF_REMOTEGUI_ICON_NONE,
                                       CMDIF_REMOTEGUI_BUTTONFACE_Exit, CMDIF_REMOTEGUI_BUTTONFACE_None, CMDIF_REMOTEGUI_BUTTONFACE_None,
                                       CMDIF_REMOTEGUI_FOCUS_None,
                                       "<HEADER>CRANK RELEARN SUCCESSFUL</HEADER>");        
            cmdif_remotegui_wait_user_response(&responsetype, &responsechoice, NULL);

            processState = processDone;
            break;
        default:            
            break;
            
        } // switch(..)
    } // while(..)
    
    // Exit diagnostics mode, stop sending packets
    cmdif_remotegui_messagebox(cmd,
                               "CRANK RELEARN", CMDIF_REMOTEGUI_ICON_NONE,
                               CMDIF_REMOTEGUI_BUTTONFACE_None, CMDIF_REMOTEGUI_BUTTONFACE_None, CMDIF_REMOTEGUI_BUTTONFACE_None,
                               CMDIF_REMOTEGUI_FOCUS_None,
                               "<HEADER>PROCESSING</HEADER>\nPlease wait...");
    switch (commtype)
    {
    case CommType_CAN:
        obd2can_gm_read_data_by_packetid_request(Obd2CanEcuId_7E0,stopSending_$AA,NULL,0);
        obd2can_gm_return_normal_mode(TRUE,Obd2CanEcuId_7E0);
        obd2can_gm_clear_diagnostic_information(TRUE,Obd2CanEcuId_7E0);
        break;
    case CommType_VPW:
        obd2vpw_request_diagnostic_data_packet(stopSending_$AA, NULL, 0);
        obd2vpw_return_normal_mode(0);
        obd2vpw_cleardtc();
        break;
    }    
    return;
}

/**
 * @brief   Crank Relearn Packet Setup
 *  
 * @param   [in] commtype
 *
 * @retval  u8 status
 *
 * @details This function sets up rapid packet data used for the crank relearn
 *          procedure. If the packets are properly setup, they are then started.
 *
 *          CAN Packet FE: 
 *          [1-2]   0x000C  RPM 
 *          [3]     0x0005  ECT (Engine Coolant Temperature) 
 *          [4]     0x12F0  CDLS (Crank Diagnostic Learning Status)
 *          [5-6]   0x001F  Engine Runtime
 *          [7]     0x1123  AC
 *
 *          VPW Packet FE: 
 *          [1-2]   0x000C  RPM 
 *          [3]     0x0005  ECT (Engine Coolant Temperature) 
 *          [4]     0x12F0  CDLS (Crank Diagnostic Learning Status)
 *          VPW Packet FD:
 *          [1-2]   0x11A1  Engine Runtime
 *
 * @author  Tristen Pierson
 */
u8 obd2_gm_crank_relearn_setup_packets(VehicleCommType commtype)
{
    u16 pids[5] = {0x000C, 0x0005, 0x12F0, 0x001F, 0x1123};
    u8  size[5] = {2, 1, 1, 2, 1};
    u8  packetidlist[2] = {0xFE, 0xFD};
    u8  status, i, position;    

    switch (commtype)
    {
    case CommType_CAN:    
        status = obd2can_gm_dynamicallydefinemessage(Obd2CanEcuId_7E0, 0xFE, &pids[0], 5);        
        if (status == S_SUCCESS)
        {
            status = obd2can_gm_read_data_by_packetid_request(Obd2CanEcuId_7E0, 
                                                              scheduleAtMediumRate_$AA, packetidlist, 1);
        }
        if (status != S_SUCCESS)
        {
            status = S_FAIL;
        }
        break;
    case CommType_VPW:   
        pids[3] = 0x11A1;   // VPW Engine Runtime PID = $11A1
        position = 1;
        for (i = 0; i < 3; i++)  
        {
            status = obd2vpw_dynamicallydefinemessage(packetidlist[0], pids[i],
                                                      PidTypeRegular, size[i], position);
            
            if (status != S_SUCCESS)
            {
                break;
            }
            position += size[i];
        }
        if (status == S_SUCCESS)
        {
            position = 1;
            for (i = 3; i < 4; i++)  
            {
                status = obd2vpw_dynamicallydefinemessage(packetidlist[1], pids[i],
                                                          PidTypeRegular, size[i], position); 

                if (status != S_SUCCESS)
                {
                    break;
                }
                position += size[i];
            }
        }
        if (status == S_SUCCESS)
        {
            status = obd2vpw_request_diagnostic_data_packet(scheduleAtFastRate_$2A,
                                                            packetidlist, 2);
        }
        if (status != S_SUCCESS)
        {
            status = S_FAIL;
        }
        break;        
    default:
        status = S_FAIL;
    }
    
    return status;
}

/**
 * @brief   GM Special Functions Menu
 *  
 * @param   [in] commtype
 * @param   [in] commlevel
 *
 * @details This function handles the OEM specific special functions menu.
 *  
 * @author  Tristen Pierson
 */
void obd2_gm_vehiclefunction(VehicleCommType *commtype, VehicleCommLevel *commlevel)
{
    CMDIF_REMOTEGUI_RESPONSETYPE responsetype;
    CMDIF_COMMAND cmd = CMDIF_CMD_SPECIAL_FUNCTIONS;
    u16 responsechoice;
    u8  functions[MAX_VEHICLE_FUNCTIONS];
    u8  i;
  
    functions[0] = VehicleFunction_CrankRelearn;
    functions[1] = VehicleFunction_EOL;
    
    while(1)
    {
        if (cmdif_remotegui_listboxbox_init("SPECIAL FUNCTIONS", NULL) == S_SUCCESS)
        {
            // Build menu
            for (i = 0; i < MAX_VEHICLE_FUNCTIONS; i++)
            {
                if (functions[i] == VehicleFunction_EOL)
                {
                    break;
                }
                switch (functions[i])
                {
                case VehicleFunction_CrankRelearn:
                    cmdif_remotegui_listboxbox_additem("CRANK RELEARN", "", CMDIF_REMOTEGUI_LISTBOX_FLAGS_CLICKABLE | CMDIF_REMOTEGUI_LISTBOX_FLAGS_ALIGN_CENTER);
                    break;
                default:
                    break;
                }
            }        
            // If no functions are available (list is empty)
            if (i == 0)
            {
                cmdif_remotegui_listboxbox_cleanup();
                cmdif_remotegui_messagebox(cmd,
                        "SPECIAL FUNCTIONS", CMDIF_REMOTEGUI_ICON_NONE,
                        CMDIF_REMOTEGUI_BUTTONFACE_Exit, CMDIF_REMOTEGUI_BUTTONFACE_None, CMDIF_REMOTEGUI_BUTTONFACE_None,
                        CMDIF_REMOTEGUI_FOCUS_None,
                        "No special functions are available for this vehicle");
            }
            // Show listbox if functions are available
            else
            {
                cmdif_remotegui_listbox(cmd, 0, 0,
                                        CMDIF_REMOTEGUI_BUTTONFACE_Exit, CMDIF_REMOTEGUI_BUTTONFACE_None, CMDIF_REMOTEGUI_BUTTONFACE_None,
                                        CMDIF_REMOTEGUI_FOCUS_ListBox);
            }
            cmdif_remotegui_wait_user_response(&responsetype, &responsechoice, NULL);

            // Exit button selected
            if (responsetype == CMDIF_REMOTEGUI_RESPONSETYPE_BUTTON)
            {
                return;
            }            
            // Menu item selected
            else if (responsetype == CMDIF_REMOTEGUI_RESPONSETYPE_LIST)
            {    
                //key on and continue. 
                cmdif_remotegui_msg_keyon(cmd,"SPECIAL FUNCTIONS");
                
                //VPW may require init
                if (commtype[0] == CommType_VPW)
                {
                    obd2vpw_init(TRUE, FALSE);
                }
                
                switch (functions[responsechoice])
                {
                case VehicleFunction_CrankRelearn:
                    obd2_gm_crank_relearn(commtype[0], commlevel[0]);
                    break;
                default:
                    break;
                } 
                //key off and continue.
                cmdif_remotegui_msg_keyoff(cmd,"SPECIAL FUNCTIONS");
            }            
        } // if (...)
        else
        {
            break;
        }        
    } // while(1)
    return;
}
