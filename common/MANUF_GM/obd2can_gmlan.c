/**
  **************** (C) COPYRIGHT 2011 SCT Performance, LLC *********************
  * File Name          : obd2can_gmlan.c
  * Device / System    : SCT products using the "CommonCode" platform
  * Author             : Quyen Leba
  *
  * Version            : 1 
  * Date               : 05/02/2011
  * Description        : 
  *                    : 
  *
  *
  * History            : 05/02/2011 M. Davis
  *                    :   Add SCT Copyright header,
  *
  ******************************************************************************
  */

//------------------------------------------------------------------------------
// Log error point: 0x30xx -> 0x31xx
//------------------------------------------------------------------------------

#include <string.h>
#include <board/delays.h>
#include <common/statuscode.h>
#include "obd2can_gmlan.h"

typedef struct
{
    bool highspeedmode;
}GMLAN_Priv_ProgrammingMode;

//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
u8 obd2can_gmlan_txrx(Obd2can_ServiceData *servicedata)
{
    obd2can_rxinfo rxinfo;
    u8 service_frame[8];
    u8 status;
    u8 index;

    if (servicedata->broadcast)
    {
        if (servicedata->commlevel == CommLevel_Unknown)
        {
            status = obd2can_txcomplex(servicedata->broadcast_id,
                                       servicedata->response_id,
                                       servicedata->service,
                                       servicedata->subservice,
                                       servicedata->service_controldatabuffer,
                                       servicedata->service_controldatalength,
                                       servicedata->txdata,
                                       servicedata->txdatalength,
                                       servicedata->nowait_cantx);
            
        }
        // GMLAN Broadcast
        else
        {
            //always single frame
            memset(service_frame,obd2can_get_padding_byte(),8);
            service_frame[0] = 0xFE;
            service_frame[2] = (u8)servicedata->service;
            index = 3;
            if (servicedata->subservice != invalidSubServiceNumber)
            {
                service_frame[index++] = (u8)servicedata->subservice;
            }
            if ((servicedata->txdatalength + index) <= 8)
            {
                memcpy(&service_frame[index],
                       servicedata->txdata,servicedata->txdatalength);
            }
            else
            {
                return S_BADCONTENT;
            }
            service_frame[1] = servicedata->txdatalength + index - 2;
            status = obd2can_txraw(0x101, (u8*)service_frame, 8,
                                   servicedata->nowait_cantx);
        }
        

    }
    else
    {
        status = obd2can_txcomplex(servicedata->ecm_id,
                                   servicedata->response_id,
                                   servicedata->service,
                                   servicedata->subservice,
                                   servicedata->service_controldatabuffer,
                                   servicedata->service_controldatalength,
                                   servicedata->txdata,
                                   servicedata->txdatalength,
                                   servicedata->nowait_cantx);
    }
    
    if (status == S_SUCCESS && servicedata->response_expected == TRUE)
    {
        obd2can_rxinfo_init(&rxinfo);

        rxinfo.rxbuffer = servicedata->rxdata;
        rxinfo.cmd = (u8)servicedata->service;
        rxinfo.first_frame_data_offset = servicedata->first_frame_data_offset;
        rxinfo.tester_present_type = servicedata->tester_present_type;
        rxinfo.initial_wait_extended = servicedata->initial_wait_extended;
        rxinfo.busy_wait_extended = servicedata->busy_wait_extended;
        rxinfo.commlevel = servicedata->commlevel;
        rxinfo.rx_timeout_ms = servicedata->rx_timeout_ms;

        status = obd2can_rx(ECM_RESPONSE_ID(servicedata->ecm_id), &rxinfo);
        
        if (status == S_SUCCESS)
        {
            servicedata->rxdatalength = rxinfo.rxlength;
        }
    }
    
    if (status == S_ERROR)
    {
        servicedata->errorcode = rxinfo.errorcode;
    }

    return status;
}

//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
u8 obd2can_gmlan_txrxcomplex(Obd2can_ServiceData *servicedata)
{
    obd2can_rxinfo rxinfo;
    u8 status;
    
    if (servicedata->broadcast)
    {
        //always single frame
        //TODOQ: this
        return S_NOTSUPPORT;
    }
    else
    {
        status = obd2can_txcomplex(servicedata->ecm_id,
                                   servicedata->response_id,
                                   servicedata->service,
                                   servicedata->subservice,
                                   servicedata->service_controldatabuffer,
                                   servicedata->service_controldatalength,
                                   servicedata->txdata,
                                   servicedata->txdatalength,
                                   servicedata->nowait_cantx);
    }
    
    if (status == S_SUCCESS && servicedata->response_expected == TRUE)
    {
        obd2can_rxinfo_init(&rxinfo);

        rxinfo.rxbuffer = servicedata->rxdata;
        rxinfo.cmd = (u8)servicedata->service;
        rxinfo.first_frame_data_offset = servicedata->first_frame_data_offset;
        rxinfo.tester_present_type = servicedata->tester_present_type;
        rxinfo.initial_wait_extended = servicedata->initial_wait_extended;
        rxinfo.busy_wait_extended = servicedata->busy_wait_extended;
        rxinfo.commlevel = servicedata->commlevel;
        rxinfo.rx_timeout_ms = servicedata->rx_timeout_ms;
        status = obd2can_rxcomplex(ECM_RESPONSE_ID(servicedata->ecm_id), &rxinfo);
        if (status == S_SUCCESS)
        {
            servicedata->rxdatalength = rxinfo.rxlength;
        }
    }

    return status;
}
