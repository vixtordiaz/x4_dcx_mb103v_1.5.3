/**
 *  ************* Copyright 2014 SCT Performance, LLC ******
 *  @file json.c
 *  @brief Functions for handing JSON files
 *  
 *  @authors Patrick Downs
 *  @date 01/11/2014
 *  ********************************************************
 */

#include "json.h"
#include <string.h>
#include <stdio.h>
#include <board/genplatform.h>
#include <common/statuscode.h>
#include <common/itoa.h>

 /**
 *  json_generate_post_file
 *  
 *  @brief Generates POST request body data required by the update server
 *  
 *  @param [in] filename file where data is to be saved  
 *
 *  @retval u8  status
 *  
 *  @details Creates JSON formatted data required by the update server and saves 
 *          it to a file specified by the input filename
 *  
 *  @authors Patrick Downs
 */
u8 fwupdate_validate_file(const u8* filename)
{
    FWUPDATE_HEADER fwheader;
    FWUPDATE_BLOCK fwblock;
    F_FILE *f;
    u32 bytesread;
    u32 i;
    u8 status;
    
    f = genfs_user_openfile(filename,"rb");
    if (f)
    {
        bytesread = fread((char*)&fwheader, 1, sizeof(fwheader), f);                // Read FW Update Header from server response data
        if(bytesread == sizeof(fwheader))
        {
            if(fwheader.structureversion == FWUPDATE_STRUCTURE_VERSION)             // Validate structure version
            {
                if(fwheader.fwblockcount > 0)                                       // Check block count
                {
                    status = S_SUCCESS;
                    for(i = 0; i < fwheader.fwblockcount; i++)                      
                    {
                        bytesread = fread((char*)&fwblock, 1, sizeof(fwblock), f);  // Check block sizes
                        if(bytesread != sizeof(fwblock))            
                        {
                            status = S_FAIL;     // Incorrect block size
                            break;
                        }
                    }
                }
                else
                {
                    status = S_INPUT;   // Block count can't be less than 1
                }
            }
            else
            {
                status = S_INVALID_VERSION; // FWUPDATE_HEADER version fail
            }
        }
    }
    else
    {
        status = S_OPENFILE;
    }
    
    genfs_closefile(f);
    
    return status;
}

/**
 *  json_generate_post_file
 *  
 *  @brief Generates POST request body data required by the update server
 *  
 *  @param [in] filename file where data is to be saved  
 *
 *  @retval u8  status
 *  
 *  @details Creates JSON formatted data required by the update server and saves 
 *          it to a file specified by the input filename
 *  
 *  @authors Patrick Downs
 */
u8 fwupdate_get_block(const u8* filename, u8 boardtypeid, u8 fwtypeid, FWUPDATE_BLOCK *retfwblock)
{
    FWUPDATE_HEADER fwheader;
    FWUPDATE_BLOCK fwblock;
    F_FILE *f;
    u32 bytesread;
    u32 i;
    u8 status;
    
    f = genfs_user_openfile(filename,"rb");
    if (f)
    {
        bytesread = fread((char*)&fwheader, 1, sizeof(fwheader), f);            // Read FW Update Header from server response data
        if(bytesread == sizeof(fwheader))
        {
            if(fwheader.structureversion == FWUPDATE_STRUCTURE_VERSION)         // Validate
            {
                status = S_FAIL;
                for(i = 0; i < fwheader.fwblockcount; i++)
                {
                    bytesread = fread((char*)&fwblock, 1, sizeof(fwblock), f);  // Find block
                    if(bytesread == sizeof(fwblock))
                    {
                        if(fwblock.boardtypeid == boardtypeid && fwblock.fwtypeid == fwtypeid)
                        {
                            memcpy((char*)retfwblock, (char const*)&fwblock, sizeof(fwblock));
                            status = S_SUCCESS;
                            break;
                        }
                    }
                    else
                    {
                        status = S_READFILE; // FWUPDATE_BLOCK read fail
                        break;
                    }
                }
            }
            else
            {
                status = S_INVALID_VERSION; // FWUPDATE_HEADER version fail
            }
        }
        else
        {
            status = S_READFILE; // FWUPDATE_HEADER read fail
        }
        
        genfs_closefile(f);
    }
    else
    {
        status = S_OPENFILE; // Couldn't open file
    }
    
    return status;    
}
    

/**
 *  json_read_firmware_info
 *  
 *  @brief Reads firmware version from server response file for boardtype
 *  
 *  @param [out] info       Structure containing server firmware info
 *  @param [in] boardtype   Boardtype to get firmware for. See JSON_BOARDTYPE definitions.
 *  @param [in] f           file pointer to server response date 
 *
 *  @retval u8  status
 *  
 *  @details    Reads firmware version from server response data for specified boardtype
 *              and returns it in a SERVER_FW_INFO structure
 *          
 *  
 *  @authors Patrick Downs
 */
u8 fwupdate_read_firmware_info(SERVER_FW_INFO *info, u8 boardtype)
{
    FWUPDATE_BLOCK fwblock;
    u8 app_status;
    u8 mb_status;
    
    memset(info, 0, sizeof(SERVER_FW_INFO));
    
    // App FW
    app_status = fwupdate_get_block(SERVER_RESPONSE_INFO_FILENAME, boardtype, FWUPDATE_FWTYPEID_APP, &fwblock);
    if(app_status == S_SUCCESS)
    {
        info->app.version = atoi((const char*)fwblock.version); // Version
        info->app.fwsize = fwblock.size;                        // Size
        info->app.fwcrc32e = fwblock.crc32e;                    // CRC32e
        strcpy((char*)info->app.url, (const char*)fwblock.url); // URL
    }
    
    // Mainboot FW
    // Certain Boards may not have bootloaders, leave mboot info NULL
    mb_status = fwupdate_get_block(SERVER_RESPONSE_INFO_FILENAME, boardtype, FWUPDATE_FWTYPEID_MAINBOOT, &fwblock);
    if(mb_status == S_SUCCESS)
    {
        info->mboot.version = atoi((const char*)fwblock.version);   // Version
        info->mboot.fwsize = fwblock.size;                          // Size
        info->mboot.fwcrc32e = fwblock.crc32e;                      // CRC32e
        strcpy((char*)info->mboot.url, (const char*)fwblock.url);   // URL
    }
    
    return app_status; // All boards will have App FW, use this status
}
            
        
        

 /**
 *  json_generate_post_file
 *  
 *  @brief Generates POST request body data required by the update server
 *  
 *  @param [in] filename file where data is to be saved  
 *
 *  @retval u8  status
 *  
 *  @details Creates JSON formatted data required by the update server and saves 
 *          it to a file specified by the input filename
 *  
 *  @authors Patrick Downs
 */
u8 json_generate_post_file(const u8* filename)
{
    F_FILE *f;
    u8 temp[256];
    u8 status;
    u32 length;
    
    f = genfs_user_openfile(filename,"wb");
    if (f)
    {
        // Generate the file        
        json_write_open(f);
        
        // Serial Number
        settings_get_deviceserialnumber(temp);
        json_write_name_data(JSON_NAME_SERIALNUMBER, temp, f);
        
        // Firmware Version
        json_get_fwversions(temp);
        json_write_name_data(JSON_NAME_FWVERSION, temp, f);
        
        // Tune Version
        obd2tune_gettunerevision(temp);
        json_write_name_data(JSON_NAME_TUNEVERSION, temp, f);
        
        // Device Type
        itoa(SETTINGS_CRITICAL(devicetype),temp,10);
        json_write_name_data(JSON_NAME_DEVICETYPE, temp, f);
        
        // Market Type
        itoa(SETTINGS_CRITICAL(markettype),temp,10);
        json_write_name_data(JSON_NAME_MARKETTYPE, temp, f);
        
        // Partnumber
        settings_getsettings_byopcode(SETTINGS_OPCODE_DEVICE_PARTNUMBER_STRING,0,
                                  temp,&length);
        json_write_name_data(JSON_NAME_PARTNUMBER, temp, f);
        
        // Filecount
        json_write_name_data(JSON_NAME_FILECOUNT, "2577", f); // TODOPD: need to update this?
        
        // Married Count
        itoa(SETTINGS_TUNE(married_count),temp,10);
        json_write_name_data(JSON_NAME_MARRIEDCOUNT, temp, f);
        
        // VIN
        json_write_name_data(JSON_NAME_VIN, SETTINGS_TUNE(vin), f);
        
        // Previous VIN
        json_write_name_data(JSON_NAME_PREVIOUSVIN, SETTINGS_TUNE(prev_vin), f);
        
        // Strategy
        json_write_name_data(JSON_NAME_STRATEGY, SETTINGS_TUNE(vehiclecodes), f);
        
        // Custom Tune Name
        json_write_name_data(JSON_NAME_CUSTOMTUNENAME, SETTINGS_TUNE(tuneinfotrack.tunedescriptions[0]), f);
        
        // Flash Limit
        json_write_name_data(JSON_NAME_FLASHLIMIT, "0", f);
        
        // Requesting Application
        json_write_name_data(JSON_NAME_REQUESTINGAPP, "X4", f); // TODOPD: this too
        
        // Device Flags
        json_write_name_data(JSON_NAME_DEVICEFLAGS, "0", f);
        
        // Married Flag
        if(SETTINGS_IsMarried())
            strcpy((char*)temp, "true");
        else
            strcpy((char*)temp, "false");
        
        json_write_name_data(JSON_NAME_MARRIEDFLAG, temp, f);
        
        // Download Failed Flag
        if(SETTINGS_IsDownloadFail())
            strcpy((char*)temp, "true");
        else
            strcpy((char*)temp, "false");
        
        json_write_name_data(JSON_NAME_DLFAILEDFLAG, temp, f);
        
        // Support Info
        //TODO:
        json_write_name_data(JSON_NAME_SUPPORTINFO, "", f);
        
        json_write_close(f);
        
        genfs_closefile(f);
        
        status = S_SUCCESS;
    }
    else
    {
        status = S_OPENFILE; // Couldn't create file
    }
    
    return status;
}

/**
 *  json_read_firmware_info
 *  
 *  @brief Reads firmware version from server response file for boardtype
 *  
 *  @param [out] info       Structure containing server firmware info
 *  @param [in] boardtype   Boardtype to get firmware for. See JSON_BOARDTYPE definitions.
 *  @param [in] f           file pointer to server response date 
 *
 *  @retval u8  status
 *  
 *  @details    Reads firmware version from server response data for specified boardtype
 *              and returns it in a SERVER_FW_INFO structure
 *          
 *  
 *  @authors Patrick Downs
 */
u8 json_read_firmware_info(SERVER_FW_INFO *info, u8 boardtype, F_FILE *f)
{
    u8 status;    
    u8 temp[128];    
    int i;
    bool boardtypefound;
    
    boardtypefound = FALSE;
    
    memset(info, 0, sizeof(SERVER_FW_INFO));
    
    if (f)
    {
        for(i=0; i<6; i++)
        {
            status = json_read_name_data(JSON_NAME_FWLIST, 0, JSON_NAME_BOARDTYPE, i, temp, sizeof(temp), f);
            if(status == S_SUCCESS)
            {
                if(temp[0] == boardtype)
                {
                    status = json_read_name_data(JSON_NAME_FWLIST, 0, JSON_NAME_FWTYPEID, i, temp, sizeof(temp), f);
                    if(status == S_SUCCESS)
                    {
                        if(temp[0] == JSON_FWTYPEID_APP) // App FW
                        {
                            status = json_read_name_data(JSON_NAME_FWLIST, 0, JSON_NAME_FWVERSION, i, temp, sizeof(temp), f);
                            if(status == S_SUCCESS)
                            {
                                info->app.version = atoi((const char*)temp); // Version
                            }
                            
                            status = json_read_name_data(JSON_NAME_FWLIST, 0, JSON_NAME_FWSIZE, i, temp, sizeof(temp), f);
                            if(status == S_SUCCESS)
                            {
                                info->app.fwsize = atoi((const char*)temp); // Size
                            }
                            
                            status = json_read_name_data(JSON_NAME_FWLIST, 0, JSON_NAME_FWCHECKSUM, i, temp, sizeof(temp), f);
                            if(status == S_SUCCESS)
                            {
                                info->app.fwcrc32e = atoi((const char*)temp); // CRC32e
                            }
                            
                            status = json_read_name_data(JSON_NAME_FWLIST, 0, JSON_NAME_FWURL, i, temp, sizeof(temp), f);
                            if(status == S_SUCCESS)
                            {
                                strcpy((char*)info->app.url, (const char*)temp); // URL
                            }
                        }
                        else if(temp[0] == JSON_FWTYPEID_MAINBOOT) // Mainboot FW
                        {
                            status = json_read_name_data(JSON_NAME_FWLIST, 0, JSON_NAME_FWVERSION, i, temp, sizeof(temp), f);
                            if(status == S_SUCCESS)
                            {
                                info->mboot.version = atoi((const char*)temp); // Version
                            }
                            
                            status = json_read_name_data(JSON_NAME_FWLIST, 0, JSON_NAME_FWSIZE, i, temp, sizeof(temp), f);
                            if(status == S_SUCCESS)
                            {
                                info->mboot.fwsize = atoi((const char*)temp); // Size
                            }
                            
                            status = json_read_name_data(JSON_NAME_FWLIST, 0, JSON_NAME_FWCHECKSUM, i, temp, sizeof(temp), f);
                            if(status == S_SUCCESS)
                            {
                                info->mboot.fwcrc32e = atoi((const char*)temp); // CRC32e
                            }
                            
                            status = json_read_name_data(JSON_NAME_FWLIST, 0, JSON_NAME_FWURL, i, temp, sizeof(temp), f);
                            if(status == S_SUCCESS)
                            {
                                strcpy((char*)info->mboot.url, (const char*)temp); // URL
                            }
                        }
                        else
                        {
                            // Unsupported FirmwareTypeId
                        }
                    }
                    boardtypefound = TRUE;
                }
            }
            else
            {
                break;
            }
        }
        
        if(boardtypefound)
        {
            status = S_SUCCESS; 
        }
        else
        {
            status = S_ERROR;
        }
    }
    else
    {
        status = S_INPUT;
    }
    
    return status;
}

/**
 *  json_write_open
 *  
 *  @brief Writes the opening bracket
 *  
 *  @param [in] f           file pointer to write json data 
 *
 *  @retval u8  status
 *  
 *  @details    Writes the opening bracket
 *          
 *  
 *  @authors Patrick Downs
 */
u8 json_write_open(F_FILE *f)
{
    if(f)
    {
        fwrite("{", 1, 1, f);
    }
    else
    {
        return S_INPUT;
    }
    
    return S_SUCCESS;
}

/**
 *  json_write_name_data
 *  
 *  @brief Writes name/value to JSON file
 *  
 *  @param [in] name    Name of object
 *  @param [in] data    Value of object
 *  @param [in] f       file pointer of JSON file
 *
 *  @retval u8  status
 *  
 *  @details    Writes the name/value pair to JSON file
 *          
 *  
 *  @authors Patrick Downs
 */
u8 json_write_name_data(const u8* name, const u8* data, F_FILE *f)
{
    u8 filedata[256];
    
    if((strlen((char const*)name) + strlen((char const*)data)) > sizeof(filedata))
    {
        return S_INPUT;
    }

    strcpy((char*)filedata, "\"");
    strcat((char*)filedata, (char*)name);
    strcat((char*)filedata, "\":\"");
    strcat((char*)filedata, (char*)data);
    strcat((char*)filedata, "\",");
    
    if(f)
    {
        fwrite(filedata, 1, strlen((char const*)filedata), f);
    }
    else
    {
        return S_INPUT;
    }
    
    return S_SUCCESS;
}

/**
 *  json_write_close
 *  
 *  @brief Writes the closing bracket
 *  
 *  @param [in] f           file pointer to JSON file
 *
 *  @retval u8  status
 *  
 *  @details    Writes the closing bracket
 *          
 *  
 *  @authors Patrick Downs
 */
u8 json_write_close(F_FILE *f)
{
    if(f)
    {
        fseek(f, -1, SEEK_CUR); // Back up and overwrite last name/value delimiter
        fwrite("}", 1, 1, f);
    }
    else
    {
        return S_INPUT;
    }
    
    return S_SUCCESS;
}

/**
 *  json_read_object_begin
 *  
 *  @brief Begin reading objects from file sequentially
 *  
 *  @param [out] objectdata     Return object data
 *  @param [in] f               File pointer to json file
 *
 *  @retval u8  status
 *  
 *  @details   
 *          
 *  
 *  @authors Patrick Downs
 */
u8 *filedata;
u8 *objptr;
u8 json_read_object_begin(F_FILE *f)
{
    u8 status;
    u32 bytesread;
    
    filedata = __malloc(JSON_READ_NAME_DATA_FILEDATA_LENGTH);
    if(filedata)
    {
        if (fseek(f,0,SEEK_SET) == 0)
        {
            bytesread = fread(filedata, 1, JSON_READ_NAME_DATA_FILEDATA_LENGTH, f);
            if(bytesread)
            {
                objptr = filedata;
                status = S_SUCCESS;
            }
            else
            {
                status = S_READFILE;
            }
        }
        else
        {
            status = S_SEEKFILE;
        }
    }
    else
    {
        status = S_MALLOC;
    }
    
    return status;
}

/**
 *  json_read_object_next
 *  
 *  @brief Read next object
 *  
 *  @param [out] objectdata     Return object data
 *  @param [in] f               File pointer to json file
 *
 *  @retval u8  status
 *  
 *  @details   
 *          
 *  
 *  @authors Patrick Downs
 */
u8 json_read_object(u8* objectdata, u32 maxdatasize, F_FILE *f)
{
    u32 bytesread;
    u32 datasize;
    u8 *end;
    bool readFile;
    bool done;
    u32 state;
    u8 status;
    
    readFile = FALSE;
    done = FALSE;
    state = 0;
    
    do
    {
        switch(state)
        {
        case 0:
            objptr = (u8*)strchr((const char*)objptr, '{');
            if(objptr)
            {
                state = 1;
            }
            else
            {
                readFile = TRUE;
            }
            break;
        case 1:
            end = (u8*)strchr((const char*)objptr, '}');
            if(end)
            {
                end++;                
                done = TRUE;
            }
            else
            {
                end = filedata + JSON_READ_NAME_DATA_FILEDATA_LENGTH;
                readFile = TRUE;
            }
            datasize = (end - objptr);
            if(datasize > maxdatasize)
            {
                status = S_NOTFIT;
            }
            else
            {
                memcpy((char*)objectdata, objptr, datasize);
                objectdata += datasize;
                *objectdata = 0x00; //Null terminate
                objptr = end;
                status = S_SUCCESS;
            }
            break;
        default:
            break;
        }
        
        if(readFile)
        {
            readFile = FALSE;
            if(feof(f))
            {
                done = TRUE;
            }
            else
            {
                bytesread = fread(filedata, 1, JSON_READ_NAME_DATA_FILEDATA_LENGTH, f);
                if(bytesread)
                {
                    objptr = filedata;                    
                }
                else
                {
                    status = S_READFILE;
                    done = TRUE;
                }
            }
        }
    }
    while(done == 0);
    
    return status;
}

/**
 *  json_read_object_done
 *  
 *  @brief 
 *
 *  @retval u8  status
 *  
 *  @details   
 *          
 *  
 *  @authors Patrick Downs
 */
void json_read_object_done(void)
{
    if(filedata)
    {
        __free(filedata);
    }
}

/**
 *  json_read_name_data
 *  
 *  @brief Read the name/value data
 *  
 *  @param [in] name           Name of object
 *  @param [in] arrayIndex     Array index of name/value to read 
 *  @param [in] subName        Sub-Name value to read from array index
 *  @param [in] subArrayIndex  Sub Array index of name/value to read 
 *  @param [out] data          Pointer where to save value data
 *  @param [in] datamaxsize    Max size of return buffer location
 *  @param [in] f              file pointer to JSON file
 *
 *  @retval u8  status
 *  
 *  @details    Reads name/value data from JSON file.
 *          
 *  
 *  @authors Patrick Downs
 */
#define JSON_READ_NAME_DATA_FILEDATA_LENGTH 2048
u8 json_read_name_data(const u8* name, u32 arrayIndex, const u8* subName, u32 subArrayIndex, u8* data, u32 datamaxsize, F_FILE *f)
{
    u8 filedata[JSON_READ_NAME_DATA_FILEDATA_LENGTH+1];
    char *start;
    char *end;
    u32 length;
    u8 *findName;
    u32 findIndex;
    u32 currentArrayIndex;
    u8 state;
    bool done;
    bool readfile;
    u8 status;
    
    status = S_FAIL;
    
    if(f)
    {
        //Set to defaults and file pointer to beginning
        done = FALSE;
        readfile = TRUE;
        state = 0;
        
        findName = (u8*)name;
        findIndex = arrayIndex;
        currentArrayIndex = 0;
        filedata[JSON_READ_NAME_DATA_FILEDATA_LENGTH] = 0x00; // Null terminate
        
        if (fseek(f,0,SEEK_SET) == 0)
        {
            do
            {
                if(readfile)
                {
                    if(feof(f))
                    {
                        done = TRUE; // Reached end of file no more data to read
                    }
                    else
                    {
                        fread(filedata, 1, JSON_READ_NAME_DATA_FILEDATA_LENGTH, f);
                    }
                    start = (char*)filedata;
                    readfile = FALSE;
                }
                
                switch(state)
                {
                case 0: // Find the start of the array
                    if(findIndex != 0)
                    {
                        if(start[0] == '[')
                        {
                            currentArrayIndex = 0; // Array values
                            state = 1;
                        }
                        else
                        {
                            status = S_INPUT; // No array found
                            done = TRUE;
                        }
                    }
                    else
                    {
                        state = 2; // Single value
                    }
                    break;
                case 1: // Find the correct array index
                    if(currentArrayIndex < findIndex)
                    {
                        start = strchr(start, '}');
                        if(start)
                        {
                            start++;
                            currentArrayIndex++;
                        }
                        else
                        {
                            readfile = TRUE;
                        }
                    }
                    else
                    {
                        state = 2;
                    }                    
                    break;
                case 2: // Find name
                    if(findName)
                    {
                        start = strstr((char const*)start, (char const*)findName);
                        if(start)
                        {
                            state = 3;
                        }
                        else
                        {
                            readfile = TRUE;
                        }
                    }
                    else
                    {
                        status = S_INPUT;
                        done = TRUE;
                    }
                    break;
                case 3: // Find name/value delimiter
                    start = strchr(start, ':');
                    if(start)
                    {
                        start++;
                        if(start[0] == '[') // Nested value?
                        {
                            findName = (u8*)subName;
                            findIndex = subArrayIndex;
                            subArrayIndex = 0;
                            currentArrayIndex = 0;
                            state = 1;
                        }
                        else
                        {
                            state = 4; // Current value
                        }
                    }
                    else
                    {
                        readfile = TRUE;
                    }
                    break; 
                case 4: // Find object delimiter
                    end = strchr(start, ',');
                    if(!end)
                    {
                        end = strchr(start, '}');
                    }
                    if(end)
                    {
                        length = (u32)(end - start);
                        state = 5;
                    }
                    else
                    {
                        if(start[0] == '"') // Omit quations
                        {
                            start++;
                        }
                        // Copy what we got so far
                        length = (JSON_READ_NAME_DATA_FILEDATA_LENGTH - ((u8*)start - filedata));
                        memcpy(data, start, length);
                        data += length;
                        
                        readfile = TRUE;
                    }
                    break;
                case 5: // Copy value to return buffer
                    if(start[0] == '"') // Omit quations
                    {
                        start++;
                        length--;
                    }
                    if(start[length-1] == '"')
                    {
                        length--;
                    }
                    if(length < datamaxsize)
                    {
                        memcpy(data, start, length);
                        data[length] = 0;
                        status = S_SUCCESS;
                    }
                    else
                    {
                        status = S_NOTFIT;
                    }
                    done = TRUE;
                    break;
                default:
                    done = TRUE;
                    status = S_ERROR;
                    break;
                }
            }
            while(done == FALSE);
        }
        else
        {
            status = S_SEEKFILE;
        }
    }
    else
    {
        status = S_INPUT;
    }
    
    return status;
}


/**
 *  json_read_name_data
 *  
 *  @brief Read the name/value data
 *  
 *  @param [in] name           Name of object
 *  @param [in] arrayIndex     Array index of name/value to read 
 *  @param [in] subName        Sub-Name value to read from array index
 *  @param [in] subArrayIndex  Sub Array index of name/value to read 
 *  @param [out] data          Pointer where to save value data
 *  @param [in] datamaxsize    Max size of return buffer location
 *  @param [in] f              file pointer to JSON file
 *
 *  @retval u8  status
 *  
 *  @details    Reads name/value data from JSON file.
 *          
 *  
 *  @authors Patrick Downs
 */
u8 json_read_name_data_frombuffer(const u8* name, u8* data, u32 datamaxsize, u8 *objectdata)
{
    char *start;
    char *end;
    u32 length;
    u8 state;
    bool done;
    u8 status;
    
    status = S_FAIL;
    
    //Set to defaults and file pointer to beginning
    done = FALSE;
    state = 0;
    
    do
    {
        switch(state)
        {   
        case 0: // Find name
            start = strstr((char const*)objectdata, (char const*)name);
            if(start)
            {
                state = 1;
            }
            else
            {
                status = S_FAIL;
                done = TRUE;
            }
            break;
        case 1: // Find name/value delimiter
            start = strchr(start, ':');
            if(start)
            {
                start++;
                state = 2; // Current value
            }
            else
            {
                status = S_FAIL;
                done = TRUE;
            }
            break; 
        case 2: // Find object delimiter
            end = strchr(start, ',');
            if(!end)
            {
                end = strchr(start, '}');
            }
            if(end)
            {
                length = (u32)(end - start);
                state = 3;
            }
            else
            {
                status = S_FAIL;
                done = TRUE;
            }
            break;
        case 3: // Copy value to return buffer
            if(start[0] == '"') // Omit quations
            {
                start++;
                length--;
            }
            if(start[length-1] == '"')
            {
                length--;
            }
            if(length < datamaxsize)
            {
                memcpy(data, start, length);
                data[length] = 0;
                status = S_SUCCESS;
            }
            else
            {
                status = S_NOTFIT;
            }
            done = TRUE;
            break;
        default:
            done = TRUE;
            status = S_ERROR;
            break;
        }
    }
    while(done == FALSE);
    
    return status;
}

/**
 *  json_get_object_count
 *  
 *  @brief Gets the count of objects in the file
 *  
 *  @param [in] file pointer to open readable file
 *  @param [out] return number of objects found in the file
 *
 *  @retval u8  status
 *  
 *  @details Gets the count of the objects in the file
 *  
 *  @authors Patrick Downs
 */
u8 json_read_object_count(F_FILE *f, u32 *count)
{
    u8 filedata[4096];
    u8 status;
    bool done;
    u32 bytesread;
    
    status = S_FAIL;
    
    if (f)
    {
        fseek(f,0,SEEK_SET);
        
        done = FALSE;
        *count = 0;
        while(done == FALSE)
        {
            bytesread = fread(filedata, 1, sizeof(filedata), f);
            if(bytesread > 0)
            {
                for(int i = 0; i < bytesread; i++)
                {
                    if(filedata[i] == '}')
                        (u32)(*count)++;
                }
            }
            else
                done = TRUE;
            
            if(feof(f))
                done = TRUE; // Reached end of file no more data to read
        }
        status = S_SUCCESS;
        fseek(f,0,SEEK_SET);
    }
    else
    {
        status = S_OPENFILE; // Couldn't open file
    }
    
    return status;
}

/**
 *  json_get_fwversions
 *  
 *  @brief  Get firmware versions in correct format for JSON file
 *  
 *  @param [out] destbuffer     Pointer to buffer where to save firmware versions
 *
 *  @retval u8  status
 *  
 *  @details    Reads device firmwares and formats for server request JSON file
 *  
 *  @authors Patrick Downs
 */
void json_get_fwversions(u8 *destbuffer)
{
    u8  buffer[16];
    FW_VERSIONS versions;
    
    cmdif_func_wifi_get_fwversions(&versions);
    destbuffer[0] = 0;
    
    strcpy((char*)destbuffer,  "MB FW "); itoa(versions.mb_app_version,buffer,10); strcat((char*)destbuffer,(char*)buffer);
    strcat((char*)destbuffer, " MB BL "); itoa(versions.mb_mboot_version,buffer,10); strcat((char*)destbuffer,(char*)buffer);
    strcat((char*)destbuffer, " MB FB "); itoa(versions.mb_fboot_version,buffer,10); strcat((char*)destbuffer,(char*)buffer);
    
    strcat((char*)destbuffer, " VB FW "); itoa(versions.vb_app_version,buffer,10); strcat((char*)destbuffer,(char*)buffer);
    strcat((char*)destbuffer, " VB BL "); itoa(versions.vb_mboot_version,buffer,10); strcat((char*)destbuffer,(char*)buffer);
    strcat((char*)destbuffer, " VB FB "); itoa(versions.vb_fboot_version,buffer,10); strcat((char*)destbuffer,(char*)buffer);
    
    strcat((char*)destbuffer, " AB FW "); itoa(versions.ab_app_version,buffer,10); strcat((char*)destbuffer,(char*)buffer);
    strcat((char*)destbuffer, " AB BL "); itoa(versions.ab_mboot_version,buffer,10); strcat((char*)destbuffer,(char*)buffer);
    strcat((char*)destbuffer, " AB FB "); itoa(versions.ab_fboot_version,buffer,10); strcat((char*)destbuffer,(char*)buffer);
}
