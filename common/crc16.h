/**
  **************** (C) COPYRIGHT 2011 SCT Performance, LLC *********************
  * File Name          : crc16.h
  * Device / System    : SCT products using the "CommonCode" platform
  * Author             : Quyen Leba
  *
  * Version            : 1 
  * Date               : 05/02/2011
  * Description        : 
  *                    : 
  *
  *
  * History            : 05/02/2011 M. Davis
  *                    :   Add SCT Copyright header,
  *
  ******************************************************************************
  */

#ifndef __CRC16_H
#define __CRC16_H

#include <arch/gentype.h>

u16 crc16_calculateblock(u16 initial_value, u8 *data, u32 datalength);
u16 crc16_calculate(u16 initial_value, u8 data);

#endif //__CRC16_H
