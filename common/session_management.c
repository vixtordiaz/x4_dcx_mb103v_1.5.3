/**
  **************** (C) COPYRIGHT 2011 SCT Performance, LLC *********************
  * File Name          : session_management.c
  * Device / System    : SCT products using the "CommonCode" platform
  * Author             : Mark Davis
  *
  * Version            : 1 
  * Date               : 12/06/2011
  * Description        : Manage disconnect - reconnect activity between the 
  *                    : mainboard/vehicleboard system and the user-interface
  *                    : implemented on the appboard, or accessed through
  *                    : a wireless interface on the app-board.
  *
  *
  * History            : 12/06/2011 M. Davis
  *                    :   Creation
  *
  ******************************************************************************
  */

#include <arch/gentype.h>
#include "session_management.h"

//------------------------------------------------------------------------------
// const strings
//------------------------------------------------------------------------------
// session-resume "program" messages
const u8 msg_sessionResume_ProgramCancelled[] = 
  "\"program\" activity was cancelled, due to an interruption. No changes were applied to your vehicle."; // "OK" button returns to the iTSX main screen

const u8 msg_sessionResume_RetToStockCancelled[] = 
  "\"program - return to stock\" activity was cancelled, due to an interruption. No changes were applied to your vehicle."; // "OK" button returns to the iTSX main screen

const u8 msg_sessionResume_ProgramHalted[] = 
  "\"program\" activity was halted, due to an interruption."; // "Start Over" returns first 'program' screen. "CANCEL" returns to iTSX main screen

const u8 msg_sessionResume_RetToStockHalted[] = 
  "\"program - return to stock\" activity was halted, due to an interruption."; // "Start Over" returns first 'program' screen. "CANCEL" returns to iTSX main screen

const u8 msg_sessionResume_ProgramPaused[] = 
  "\"program\" activity was paused, due to an interruption."; // Pressing "CONTINUE" continues. Pressing "CANCEL" returns to the iTSX main screen

const u8 msg_sessionResume_RetToStockPaused[] = 
  "\"program - return to stock\" activity was paused, due to an interruption.";     // Pressing "CONTINUE" continues. Pressing "CANCEL" returns to the iTSX main screen

const u8 msg_sessionResume_ProgramFinished[] = 
  "\"program\" activity finished during an interruption. The selected changes were applied to your vehicle."; // Pressing the displayed "OK" button displays the closing 'program' screen.

const u8 msg_sessionResume_RetToStockFinished[] = 
  "\"program - return to stock\" activity finished during an interruption. The selected changes were applied to your vehicle."; // Pressing the displayed "OK" button displays the closing 'program' screen. 

const u8 msg_sessionResume_ProgramError[] = 
  "\"program\" activity reported an error. No changes were applied to your vehicle."; // "OK" button returns to the iTSX main screen

const u8 msg_sessionResume_RetToStockError[] = 
  "\"program - return to stock\" activity reported an error. No changes were applied to your vehicle."; // "OK" button returns to the iTSX main screen 

// session-resume "performance" messages
const u8 msg_sessionResume_PerformanceSetupPaused[] = 
  "\"performance\" - setup was paused, due to an interruption."; // "CONTINUE" - "CANCEL"

const u8 msg_sessionResume_PerformanceHalted[] = 
  "\"performance\" activity was halted, due to an interruption."; // "OK" button returns to the iTSX main screen

const u8 msg_sessionResume_PerformanceError[] = 
  "\"performance\" activity reported an error."; // "OK" button returns to the iTSX main screen

// session-resume "gauges" messages
const u8 msg_sessionResume_GaugesSetupPaused[] = 
  "\"gauges - setup\" was paused, due to an interruption."; // if time-out, main screen... otherwise, "CONTINUE" or "CANCEL"

const u8 msg_sessionResume_GaugesSetupError[] = 
  "\"gauges - setup\" reported an error. Please try again."; // if time-out, main screen... otherwise, "RETRY" or "CANCEL"

const u8 msg_sessionResume_GaugesSetupFinished[] = 
  ""; // if accessory is still in datalog, proceed to gauges display without message or user interaction, otherwise, main screen

const u8 msg_sessionResume_GaugesPaused[] = 
  ""; // if accessory is still in datalog, resume gauges display without message or user interaction, otherwise, main screen

const u8 msg_sessionResume_GaugesError[] = 
  "\"gauges\" activity reported an error. \"gauges\" operation aborted."; // "OK" button displayed for acknowledgement... return to iTSX main screen

// session-resume "diagnostics" messages
const u8 msg_sessionResume_DTC_ReadPaused[] = 
  "\"diagnostics - read codes\" activity was interrupted."; // if time-out, main screen... otherwise, "RESTART" or "CANCEL"

const u8 msg_sessionResume_DTC_ClearFinished[] = 
  "\"diagnostics - clear codes\" finished during an interruption."; // if time-out, main screen... otherwise, "OK" button for acknowledgement

const u8 msg_sessionResume_DTC_Error[] = 
  "\"diagnostics - clear codes\" reported an error."; // "OK" button returns to the iTSX main screen

// session-resume "info" messages
const u8 msg_sessionResume_InfoPaused[] = 
  ""; // if time-out, main screen... otherwise, simply repeat info process, without prompt
const u8 msg_sessionResume_InfoError[] = 
  "\"info\" reported an error.";  // "OK" button returns to the iTSX main screen

SessionMgmtInfo sessionMgmt_info;


//------------------------------------------------------------------------------
// Update the current "activity" information in sessionMgmt_info...
//  Called only from cmdif_command(), in common\cmdif.c
//
// Input:  enum TUNER_ACTIVITY activity, as defined in session_management.h
//
// Return:  none
//
// Engineer: Mark Davis
//------------------------------------------------------------------------------
void session_mgmt_accActivityUpdate(enum TUNER_ACTIVITY activity)
{
    sessionMgmt_info.LastCalledFrom.currentActivity = activity;
}

//------------------------------------------------------------------------------
// Update Bluetooth caller history, at any point where a Bluetooth transmission
// is about to be made, or upon return from called functions that can generate
// a Bluetooth transmission.
// Input:   enum APP_TX_CALLED_FROM_FN calledFromFn, u8 tuner_tx_step
//          (enum APP_TX_CALLED_FROM_FN defined session_management.h)
//   
//   "tuner_tx_step" is a sequential "sequence point" that identifies the unique 
//   location in calling function where the Bluetooth transmission will occur,
//   or where an underlying routine is being called that can make a Bluetooth
//   transmission. 
//
// Return:  none
// Engineer: Mark Davis
//------------------------------------------------------------------------------
void session_mgmt_callHistoryUpdate(enum APP_TX_CALLED_FROM_FN calledFromFn, u8 tuner_tx_step)
{
    switch(calledFromFn)
    {
    // called from level 0
    case from_L0_cmdif_handler:
    case from_L0_cmdif_func_datalog_scan:
    case from_L0_cmdif_func_datalog_start:
    case from_L0_cmdif_func_uploadtune_do:
    case from_L0_cmdif_func_downloadtune_init:
    case from_L0_cmdif_func_downloadtune_do:
    case from_L0_cmdif_func_recoverystock_register:
    case from_L0_cmdif_func_hardware:
        sessionMgmt_info.LastCalledFrom.L0_caller = (enum APP_TX_CALLED_FROM_FN)calledFromFn;
        sessionMgmt_info.LastCalledFrom.tuner_L0_tx_step = tuner_tx_step;
        sessionMgmt_info.LastCalledFrom.L1_caller = L1_idle;
        sessionMgmt_info.LastCalledFrom.tuner_L1_tx_step = 0;
        sessionMgmt_info.LastCalledFrom.L2_caller = L2_idle;
        sessionMgmt_info.LastCalledFrom.tuner_L2_tx_step = 0;
        sessionMgmt_info.LastCalledFrom.L3_caller = L3_idle;
        sessionMgmt_info.LastCalledFrom.tuner_L3_tx_step = 0;

        break;

    // called from level 1
    case from_L1_obd2datalog_getvalidpidlist_fromfile:
    case from_L1_filestock_generate_flashfile_fromstock:
    case from_L1_filestock_generate_flashfile_fromfile:
    case from_L1_obd2tune_downloadtune:
    case from_L1_obd2tune_uploadstock:
        sessionMgmt_info.LastCalledFrom.L1_caller = (enum APP_TX_CALLED_FROM_FN)calledFromFn;
        sessionMgmt_info.LastCalledFrom.tuner_L1_tx_step = tuner_tx_step;
        sessionMgmt_info.LastCalledFrom.L2_caller = L2_idle;
        sessionMgmt_info.LastCalledFrom.tuner_L2_tx_step = 0;
        sessionMgmt_info.LastCalledFrom.L3_caller = L3_idle;
        sessionMgmt_info.LastCalledFrom.tuner_L3_tx_step = 0;

        break;
        
    // called from level 2
    case from_L2_download_ecm:
    case from_L2_upload_ecm:
        sessionMgmt_info.LastCalledFrom.L2_caller = (enum APP_TX_CALLED_FROM_FN)calledFromFn;
        sessionMgmt_info.LastCalledFrom.tuner_L2_tx_step = tuner_tx_step;
        sessionMgmt_info.LastCalledFrom.L3_caller = L3_idle;
        sessionMgmt_info.LastCalledFrom.tuner_L3_tx_step = 0;

        break;

    // called from level 3
    case from_L3_download_ecm_memoryblock:
    case from_L3_upload_ecm_memoryblock:
    case from_L3_generate_ecm_stock:
        sessionMgmt_info.LastCalledFrom.L3_caller = (enum APP_TX_CALLED_FROM_FN)calledFromFn;
        sessionMgmt_info.LastCalledFrom.tuner_L3_tx_step = tuner_tx_step;

    }
}
