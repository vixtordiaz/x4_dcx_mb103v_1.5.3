/**
  **************** (C) COPYRIGHT 2011 SCT Performance, LLC *********************
  * File Name          : obd2datalog.h
  * Device / System    : SCT products using the "CommonCode" platform
  * Author             : Quyen Leba
  *
  * Version            : 1 
  * Date               : 05/02/2011
  * Description        : 
  *                    : 
  *
  *
  * History            : 05/02/2011 M. Davis
  *                    :   Add SCT Copyright header,
  *
  ******************************************************************************
  */

#ifndef __OBD2DATALOG_H
#define __OBD2DATALOG_H

#include <arch/gentype.h>
#include <board/genplatform.h>
#include <fs/genfs.h>
#include <common/obd2def.h>
#include <common/cmdif.h>

typedef void (funcptr_v_u8)(u8 p);
typedef u8 (funcptr_getdatalogdata)(VehicleCommLevel *vehiclecommlevel);
typedef u8 (funcptr_getdataloganalogdata)(void);
typedef void (funcptr_senddatalogtesterpresent)(void);

#define DATALOG_USE_ENCRYPTION              1
#define DATALOG_DLX_CURRENT_VERSION         4
#define DATALOG_ANALOG_INPUT_COUNT_LIMIT    8

#define DEFAULT_DATALOG_VALID_PID_FILENAME  "validpid.dlx"
#define DEFAULT_DATALOG_RECORD_FILENAME     "\\USER\\record.dat"

#define MAX_DATALOG_FILENAME_LENGTH         128

#define MAX_DATALOG_DLXBLOCK_COUNT          70
#define DATALOG_FEATURE_FILENAME            DEFAULT_DATALOG_FEATURE_FILENAME
#define DATALOG_VALID_PID_FILENAME          DEFAULT_DATALOG_VALID_PID_FILENAME
#define DATALOG_INVALID_PACKETID            0x00
#define DATALOG_SKIPPED_PACKETID            0x00
#define DATALOG_OSC_TIMEOUT                 5000    // 5s timeout
#define DATALOG_WAIT_OSC_INTERVAL           250     // 0.250s wait to see if any additional responses
#define DATALOG_MAX_DATAPOINT_INTERVAL      2500    // 2.5s timeout
#define DATALOG_MAX_TESTERPRESENT_INTERVAL  2000    // 2.0s timeout
#define DATALOG_DEFAULT_DATAREPORT_INTERVAL 200
#define DATALOG_MAX_VALID_PER_SESSION       255
#define DATALOG_MAX_TIMEOUTS                3       // Consecutive timeouts allowed (non rapid packet)
#define DATALOG_POLL_INTERVAL               20      // Maximum Polling interval (non-rapid packet)
#define DATALOG_SINGLERATE_TIMEOUT          200     // Assume single rate response lost after this period
#define DATALOG_USB_TIMEOUT                 5000    // 5.0s timeout

#define DATALOG_MAX_PID_SIZE                0x48    // Adjust if PIDs contain more data (e.g. packeted PIDs)

//Bit0: 1 (disable new data filter)
#define DATALOG_START_ATTRIB_DISABLE_NEWDATA_FILTER             (1<<0)
//Bit1: 1 (has OTF feature)
#define DATALOG_START_ATTRIB_HASOTF                             (1<<1)

//------------------------------------------------------------------------------
// Datalog Packet Definitions
//------------------------------------------------------------------------------

//------------------------------------------------------
// DCX
//------------------------------------------------------
//-KWP/CAN----------------------------------------------
// Dynamically Defined Local Identifier Range F0-F9
// NOTE: Only one Dynamic LID may be assigned or we get errors
#define DCX_KWP_CAN_PACKETID_START              0xF0
#define DCX_KWP_CAN_PACKETID_END                0xF1        // Only one packet can be defined
#define DCX_KWP_CAN_MAX_PID_PACKET              42          // Number of items which can be defined in a packet
#define DCX_KWP_CAN_MAX_DATABYTE_PER_PACKET     168
//------------------------------------------------------
// Ford
//------------------------------------------------------
//-ISO/CAN----------------------------------------------
// ISO14229 Periodic Record Data Identifier Range F200-F2FF
// ISO14229 Dynamic Defined Data Identifier Range F300-F3FF
#define FORD_UDS_CAN_PACKETID_START             0xF200
#define FORD_UDS_CAN_PACKETID_END               0xF20A      // Only $F200 - $F20A will work
#define FORD_UDS_CAN_MAX_DATABYTE_PER_PACKET    7
//-KWP/CAN----------------------------------------------
// Data Packet Identifier (DPID) Range 00-FF
#define FORD_KWP_CAN_PACKETID_START             0x01
#define FORD_KWP_CAN_PACKETID_END               0x0F
#define FORD_KWP_CAN_MAX_DATABYTE_PER_PACKET    5
//-KWP/SCP----------------------------------------------
// Data Packet Identifier (DPID) Range 00-FF
#define FORD_KWP_SCP_PACKETID_START             0x01
#define FORD_KWP_SCP_PACKETID_END               0x0F
#define FORD_KWP_SCP_MAX_DATABYTE_PER_PACKET    5

//------------------------------------------------------
// GM
//------------------------------------------------------
//-GMLAN/CAN--------------------------------------------
// Data Packet Identifier (DPID) Range FE-90 or 7F-01
#define GM_GMLAN_CAN_PACKETID_START             0xFE
#define GM_GMLAN_CAN_PACKETID_END               0xE6	// 24 packets is max we allocate for
#define GM_GMLAN_CAN_MAX_DATABYTE_PER_PACKET    7
// Dynamic PID Range FE00-FE3F
#define GM_GMLAN_CAN_PID_START                  0xFE00
#define GM_GMLAN_CAN_PID_END                    0xFE3F
//-GMLAN/VPW--------------------------------------------
#define GM_GMLAN_VPW_PACKETID_START             0xFE
#define GM_GMLAN_VPW_PACKETID_END               0xFA
#define GM_GMLAN_VPW_MAX_DATABYTE_PER_PACKET    6

//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------

//##########################################################################
// Datalog Signal PID Flags
//##########################################################################
#define DATALOG_PIDFLAGS_DATATYPE_FLOAT     0
#define DATALOG_PIDFLAGS_DATATYPE_INT       1
#define DATALOG_PIDFLAGS_DATATYPE_BIT       2
#define DATALOG_PIDFLAGS_DATATYPE(f)        (f & 0x07)
#define DATALOG_PIDFLAGS_BIGENDIAN          (1<<3)
#define DATALOG_PIDFLAGS_SIGNED             (1<<4)
#define DATALOG_PIDFLAGS_IS_PACKET_PID      (1<<8)
#define DATALOG_PIDFLAGS_IS_BITS            (1<<9)
//##########################################################################
// Datalog Signal Control Flags
//##########################################################################
#define DATALOG_CONTROL_NEWDATA_SET         (3<<0)
#define DATALOG_CONTROL_NEWDATA_REPORT_SET  (1<<0)
#define DATALOG_CONTROL_NEWDATA_RECORD_SET  (1<<1)
#define DATALOG_CONTROL_NEWDATA_REPORT_CLR  (0xFFFE)
#define DATALOG_CONTROL_NEWDATA_RECORD_CLR  (0xFFFD)
#define DATALOG_CONTROL_SKIP                (1<<3)
// bits 4-7
#define DATALOG_CONTROL_OSC_MASK            (0xF0)
//##########################################################################
// Datalog Task
//##########################################################################
#define DATALOG_TASK_NONE                   (0)
#define DATALOG_TASK_GET_DATA               (1<<0)
#define DATALOG_TASK_GET_DATA_SINGLE_RATE   (1<<1)
#define DATALOG_TASK_ANALOG                 (1<<2)
#define DATALOG_TASK_OSC                    (1<<3)
#define DATALOG_TASK_MODE1_COMMLOST         (1<<4)
//##########################################################################
// DlxHeader Features Flags
//##########################################################################
#define DATALOG_FEATURES_NONE               (0)
#define DATALOG_FEATURES_DTC                (1<<0)
#define DATALOG_FEATURES_OSC                (1<<1)
#define DATALOG_FEATURES_PACKET_PID         (1<<2)

//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------

typedef struct
{
    u16 dlxVersion;         // version of dlx
    u16 ecm;                // ecm type
    u8  description[32];    // Optional file description
    u8  strategy[20];       // Strategy Partnumber, etc., if DLF is for a specific vehicle
    u32 header_crc32e;
    u16 count;              // number of entries in file
    u16 features;
    u32 content_crc32e;
    u8  reserved2[60];      // padding... header is 128 bytes
}DlxHeader;
STRUCT_SIZE_CHECK(DlxHeader,128);

typedef struct 
{
    u32 pidModuleType;          // Destination ECM of PID (PCM, TCM, ABS, etc)
    u8  pidShortName[16];       // Short version of PID name
    u8  pidName[32];            // Normal PID name
    u32 pidAddr;                // Actual PID number or DMR address to be sent
    u8  pidSize;                // Size in bytes of the PID response value
    s8  pidBP;                  // Bit Point value to be applied with Bit Point conversion
    u8  pidBit;                 // Bit mask value for bit PID response values or byte position of packet PIDs
    u8  pidType;                // 1:PID, 2:DMR, 3:analog
    u8  pidBitMapped;           // Bit Mapped (True or False)
    u8  reserved1;   
    u16 pidFlags;               // Bit[2..0]: ECUDataType (0: float, 1: int), Bit[3]: isECUDataBigEndian (1: BIG, 0: little), Bit[4]: ECUDataSign (1: signed, 0: unsigned), Bit[7..5]: extra(optional) encryption on the 8 bytes from DlxBlock.addr (0: none, 1: TEA, other: reserved), Bit[8]: isPacketPID, Bit[9]: isBits
    double  pidMultiplier;      // Multiplier and offset for value conversion 
    double  pidOffset;          //
    float   pidRangeMin;        // Minimum default range for pid value (For Gauges)
    float   pidRangeMax;        // Maximum default range for pid value
    u16 pidUnitID;              // Unit ID specifies the unit of measure
    u16 packetStartPos;         // Packet PID start position
    u32 pidOemType;
    u32 pidCommType;
    u32 pidCommLevel;
    u8  uiControlType;          // UI GUI Control Type
    u8  oscDataBytePos;         // OSC data byte position
    u8  oscControlBytePos;      // OSC control byte position
    u8  oscControlByteMask;     // OSC control byte mask    
    u8  oscUiPidGroup;          // OSC pid group (for latch action)
    u8  reserved2;
    u16 enumID;                 // Enumerated data identifier
    u8  reserved3[16];
}DlxBlock;
STRUCT_SIZE_CHECK(DlxBlock,128);

typedef enum
{
    SignalAttrib_Data   = 0x0,
    SignalAttrib_Status = 0x1,
}DatalogSignalAttrib;

typedef enum
{
    //----Status (0x0-0x7)----
    OSC_Default         = 0x00,    // CPID default state (all 0x00's)
    OSC_Pending         = 0x01,    // CPID has been sent, waiting on response
    OSC_Success         = 0x02,    // CPID successfully processed by ECM
    OSC_Fail            = 0x03,    // CPID failed to be processed by ECM
    OSC_RangeErr        = 0x04,    // CPID device control limits exceeded
    OSC_Timeout         = 0x05,    // CPID device control timeout    
    //---Commands (0x8-0xF)---
    OSC_CmdReset        = 0x08,    // Reset CPID
    OSC_CmdSend         = 0x09,    // Send CPID
}OSC_Status;

typedef enum
{
    PidUserSpecified    = 0,    // do not change these values
    PidTypeRegular      = 1,
    PidTypeDMR          = 2,
    PidTypeAnalog       = 3,
    PidTypeMode1        = 4,
    PidTypeOSC          = 5,
    PidTypeUnknown      = 0xFF,
}PidType;

typedef enum
{
    uiTypeNumCtrl                   = 0,
    uiTypeButton                    = 1,
    uiTypeToggle_On_1_Off_0         = 2,
    uiTypeToggle_On_0_Off_1         = 3,
    uiTypeToggle_Enable_1_Disable_0 = 4,
    uiTypeToggle_Enable_0_Disable_1 = 5,
    uiTypeToggle_Open_1_Close_0     = 6,
    uiTypeToggle_Open_0_Close_1     = 7,
    uiTypeToggle_Lock_1_Unlock_0    = 8,
    uiTypeToggle_Lock_0_Unlock_1    = 9,
}uiType;    
    
typedef enum
{
    PidRapidPacket      = 1,
    PidSingleRate       = 2,
}pidPacketType;

typedef enum
{
    StructTypeMultiplierOffset  = 0,
    StructTypeBitPtr            = 1,
    StructTypeBitmapped         = 2,
    StructTypeOSC               = 3,
    StructTypeUnknown           = 0xFF,
}StructType;    

typedef struct
{
    u16 PacketNumber;
    u8  Position;
    u8  Reserved;
}DatalogSignalGeneric;

typedef struct
{
    u16 PacketNumber;
    u8  Position;
    float Multiplier;
    float Offset;
}DatalogSignalMultiplierOffset;

typedef struct
{
    u16 PacketNumber;
    u8  Position;
    float Multiplier;
    float Offset;
    s32 Bp;
}DatalogSignalBitPtr;

typedef struct
{
    u16 PacketNumber;
    u8  Position;    
    u32 Bit;
}DatalogSignalBitmapped;

typedef struct
{
    u16 PacketNumber;
    u8  Position;    
    float Multiplier;
    float Offset;
    u8  DataBytePosition;
    u8  ControlBytePosition;
    u8  ControlByteMask;
    u8  UiPidGroup;
}DatalogSignalOSC;

typedef struct
{
    u32 Address;            // PID, DMR address, Analog channel, CPID, etc
    u8  EcmIndex;           // 0:1st ECM, 1: 2nd ECM
    u8  PidType;            // 1:OEM PID, 2:DMR, 3:Analog, 4:SAE PID, 5: OSC, etc
    u8  StructType;
    u8  Size;
    u16 DlxIndex;
    u16 Flags;
    u16 Control;
    u16 packetStartPos;
    float Data;             // Note: For OSC, Data is raw and sent from app
    float Value;            // Note: For OSC, Value is OSC status
    union {
        DatalogSignalGeneric Generic;
        DatalogSignalMultiplierOffset MultiplierOffset;
        DatalogSignalBitPtr BitPtr;
        DatalogSignalBitmapped Bitmapped;
        DatalogSignalOSC OSC;
    } SignalType;
}DatalogSignal;

typedef enum
{
    DatalogCommActive,
    DatalogCommLost,
    DatalogCommActiveWaiting,
}DatalogCommStatus;

typedef struct
{
    VehicleCommType commtype[ECM_MAX_COUNT];
    VehicleCommLevel commlevel[ECM_MAX_COUNT];
    DatalogCommStatus commstatus;
    u8 dlxfilename[MAX_FILENAME_LENGTH];
    DlxHeader *dlxheader;
    DlxBlock *dlxblocks;
    u16 validpidcount;
    u16 datalogsignalcount;
    DatalogSignal datalogsignals[MAX_DATALOG_DLXBLOCK_COUNT];
    u16 pidsignalcount;
    u16 dmrsignalcount;
    u16 dmrsignalindex[MAX_DATALOG_DLXBLOCK_COUNT];
    u8  analogsignalmaxavailable;
    u8  analogsignalcount;
    u16 analogsignalindex[DATALOG_ANALOG_INPUT_COUNT_LIMIT];
    u32 analogsignalsamplecount[DATALOG_ANALOG_INPUT_COUNT_LIMIT];
    u16 oscsignalcount;
    u8  packetidlist[2][24];        // TODOQ: more on this # //TODOQ: mutiple ecm
    u8  packetidcount[2];
    u32 datareport_interval;                //how frequent to report data
    u32 lastosc_timestamp;                  //when last osc command was sent
    u32 lastsinglerate_timestamp;           //when last single rate command was sent
    u32 lastdatapoint_timestamp;            //when last data point taken
    u32 lastdatareport_timestamp;           //when last data reported
    u32 lasttesterpresent_timestamp;        //when last testerpresent sent
    bool pending;                           //Tracks if non-packet data has been requested
    bool osc_pending;                       //Tracks if osc response is pending
    funcptr_getdatalogdata *getdatafunc;
    funcptr_getdataloganalogdata *getanalogdatafunc;
    funcptr_senddatalogtesterpresent *sendtesterpresent;
    u16 scheduled_tasks;
    struct
    {
        F_FILE *fptr;
        u32 totallength;
        struct
        {
            u8  recording   : 1;
            u8  reserved    : 7;
            u8  percentusedspace;
        }reportstatus;
        u8  buffer[512];
        u16 bufferlength;
        struct
        {
            u8  start_pending   : 1;
            u8  stop_pending    : 1;
            u8  state           : 4;
#define DATALOG_RECORD_STATE_IDLE       0
#define DATALOG_RECORD_STATE_ERROR      1
#define DATALOG_RECORD_STATE_FULL       2
#define DATALOG_RECORD_STATE_STOPPED    3
#define DATALOG_RECORD_STATE_STARTED    4
            u8  reserved        : 2;
        }control;
    }record;
    struct
    {
        u8  newdata_filtered    : 1;
        u8  ecm_stream          : 1;
        u8  tcm_stream          : 1;
        u8  hasotf              : 1;    //OTF feature is available
        u8  reserved            : 4;
        u32 ecm_laststreamdata;
        u32 tcm_laststreamdata;
    }control;
}Datalog_Info;

typedef enum
{
    RapidRate       = 1,
    SingleRate      = 2,
}ModeType;

// Defaults are set in obd2datalog.c -> obd2datalog_datalogmode_init()
typedef struct
{
    u8                  packetmode;
    MemoryAddressType   addressType;    // Define default in obd2datalog_datalogmode_init()
    u8                  packetCount;
    u8                  packetSize;
    bool                ecmBroadcast;   // Used for mode $01 (some PCMs need 0x7DF for reading mode $01 PIDs)
    bool                obdPids;        // Set true if mode $01 PIDs are to be logged
    bool                unlockRequired; // Tracks if security required for datalogging
}Datalog_Mode;

//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------

#define DatalogControl_GetOSC(control)      ((control&DATALOG_CONTROL_OSC_MASK)>>4)
#define DatalogControl_SetOSC(control,val)  (control=control&0x0F|val<<4&0xF0)

//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
u8 obd2datalog_dataloginfo_init();
u8 obd2datalog_datalogmode_init();
void obd2datalog_datalogmode_get(VehicleCommType commtype, VehicleCommLevel commlevel);
u8 obd2datalog_garbagecollector();
u8 obd2datalog_setdatareportinterval(u8 interval);
u8 obd2datalog_init(VehicleCommType *commtype, VehicleCommLevel *commlevel);
u8 obd2datalog_prevalidate_task(u16 ecm_index, VehicleCommType commtype, 
                                VehicleCommLevel commlevel);
u8 obd2datalog_setcomm(VehicleCommType *commtype, VehicleCommLevel *commlevel);
u8 obd2datalog_cleanup_packets(VehicleCommType *commtype, VehicleCommLevel *commlevel);
u8 obd2datalog_validatepid(u32 ecm_id, DlxBlock *dlxblock,
                           VehicleCommType commtype, VehicleCommLevel commlevel);
u8 obd2datalog_validatedmr(u32 ecm_id, DlxBlock *dlxblock,
                           VehicleCommType commtype, VehicleCommLevel commlevel);
u8 obd2datalog_validateByRapidPacketSetup(u32 ecm_id, DlxBlock *dlxblock,
                                          VehicleCommType commtype, 
                                          VehicleCommLevel commlevel);
u8 obd2datalog_validate_oscpid(u32 ecm_id, DlxBlock *dlxblock,
                               VehicleCommType commtype,
                               VehicleCommLevel commlevel);
u8 obd2datalog_validatepid_mode1(u32 ecm_id,DlxBlock *dlxblock,
                                 VehicleCommType vehiclecommtype);
u8 obd2datalog_check_voltage_mode1(u8 mode);

u8 obd2datalog_evaluate_fit(VehicleCommType *commtype, VehicleCommLevel *commlevel,
                            u16 *valid_index_list, u16 *valid_index_count);

u8 obd2datalog_unset_all_osc(VehicleCommType *commtype, VehicleCommLevel *commlevel);

u8 obd2datalog_getdatalogfileinfo(const u8 *datalogfilename,
                                  DlxHeader *dlxheader, DlxBlock *dlxblock);
u8 obd2datalog_generatevalidpidfile(const u8 *datalogfilename,
                                    const u8 *datalogfeaturefilename,
                                    const u8 *validpidfilename,
                                    const u8 *validpidlist, u16 validpidcount);
u8 obd2datalog_createdatalogfeaturefile(const u8 *filename);
u8 obd2datalog_validate_by_dlx_block(DlxBlock *dlxblock,
                                     VehicleCommType vehiclecommtype,
                                     VehicleCommLevel vehiclecommlevel);
u8 obd2datalog_getdata_singlerate(VehicleCommLevel *vehiclecommlevel);
u8 obd2datalog_getvalidpidlist_fromfile(u8 *datalogfilename,
                                        VehicleCommType *vehiclecommtype,
                                        VehicleCommLevel *vehiclecommlevel,
                                        u16 maxvalidpidallow,
                                        u8 *validpidlist, u16 *validpidcount,
                                        funcptr_v_u8 reportprogressfunc);
u8 obd2datalog_getvalidpidlist(DlxHeader *dlxheader,DlxBlock *dlxblocks,
                               VehicleCommType *vehiclecommtype,
                               VehicleCommLevel *vehiclecommlevel,
                               u8 *validpidlist, u16 *validpidcount,
                               funcptr_v_u8 reportprogressfunc);
u8 obd2datalog_generatevalidpidlistfile(u8 *sourcedlxfilename,
                                        u8 *validpidlist, u16 validpidcount);
u8 obd2datalog_parsedatalogfile(const u8 *datalogfilename);
u8 obd2datalog_trimdatalogsignals(u8 *validpidlist, u16 validpidcount);
u8 obd2datalog_datastream_start(VehicleCommType *vehiclecommtype,
                                VehicleCommLevel *vehiclecommlevel);
u8 obd2datalog_datastream_stop(VehicleCommType *vehiclecommtype,
                               VehicleCommLevel *vehiclecommlevel);
u8 obd2datalog_startup(VehicleCommType *vehiclecommtype,
                       VehicleCommLevel *vehiclecommlevel);
#define obd2datalog_getdata_analog      obd2datalog_getdata_analog_no_average_sampling
u8 obd2datalog_getdata_analog_long_average_sampling();
u8 obd2datalog_getdata_analog_short_average_sampling();
u8 obd2datalog_getdata_analog_no_average_sampling();
u8 obd2datalog_getdatareport(u8 *report, u16 *reportlength);
u8 obd2datalog_validate_dlxheader(DlxHeader *dlxheader, u32 *crc32e);
u8 obd2datalog_validate_dlxfile(const u8 *filename);
u8 obd2datalog_merge_dlxfile(const u8 *newdlxfilename,
                             const u8 *dlxfilename1, const u8 *dlxfilename2);
u8 obd2datalog_get_total_signal(u32 *total);
u8 obd2datalog_newdata_filter(bool enabled);
u8 obd2datalog_start_record();
u8 obd2datalog_stop_record();
u8 obd2datalog_record();
void obd2datalog_analogport_init();
void obd2datalog_analogport_deinit();
u8 obd2datalog_evaluate_dlx_file_for_datalog_session(const u8 *dlx_filename,
                                                     u16 *index_list,
                                                     u16 *index_count);
u8 obd2datalog_parserapidpacketdata(u8 ecm_index, u16 packetid, u8 *data);
void obd2datalog_parserdata(u8 ecm_index, u16 signal_index, u16 packetid, u8 *data);
float obd2datalog_composedata(u16 signal_index, float data);
float obd2datalog_calculate_bp(DatalogSignal *datalogsignal);

void obd2datalog_demo_sendtesterpresent();
u8 obd2datalog_demo_getdata(VehicleCommLevel *vehiclecommlevel);

u8 obd2_datalog_evaluatesignalsetup(VehicleCommLevel *vehiclecommlevel);

u8 obd2_datalog_update_signal_osc(u8 *data, u16 count);
u8 obd2_datalog_unset_all_osc(VehicleCommType *vehiclecommtype, 
                              VehicleCommLevel *vehiclecommlevel);

u8 obd2datalog_setupdatalogsession(VehicleCommType *commtype, VehicleCommLevel *commlevel);
u8 obd2datalog_startdatalogsession(VehicleCommType *commtype, VehicleCommLevel *commlevel);
u8 obd2datalog_stopdatalogsession(VehicleCommType *commtype, VehicleCommLevel *commlevel);
u8 obd2_datalog_unset_all_osc(VehicleCommType *commtype, VehicleCommLevel *commlevel);

#endif  //__OBD2DATALOG_H
