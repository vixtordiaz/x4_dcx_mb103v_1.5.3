/**
  **************** (C) COPYRIGHT 2011 SCT Performance, LLC *********************
  * File Name          : bootsettings.c
  * Device / System    : SCT products using the "CommonCode" platform
  * Author             : Quyen Leba
  *
  * Version            : 1 
  * Date               : 05/02/2011
  * Description        : 
  *                    : 
  *
  *
  * History            : 05/02/2011 M. Davis
  *                    :   Add SCT Copyright header,
  *
  ******************************************************************************
  */

#include <stdio.h>
#include <string.h>
#include <device_version.h>
#include <arch/gentype.h>
#include <board/genplatform.h>
#include <board/bootloader.h>
#include <common/crc32.h>
#include <common/statuscode.h>
#include "crypto_messageblock.h"
#include "bootsettings.h"

#define BOOTSETTINGS_USE_ENCRYPTED_MESSAGE_BLOCK                1

BootSettings bootsettings;

//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
u8 bootsettings_load()
{
    bool restore_settings = FALSE;

    if (restore_settings)
    {
        bootsettings_retoredefault();
    }

    flash_load_bootsettings((u8*)&bootsettings,sizeof(bootsettings));
    //make sure correct version loaded to bootsettings
    BOOTSETTINGS_SetAppVersion(APPLICATION_VERSION);

    return S_SUCCESS;
}

//------------------------------------------------------------------------------
// Will check all bootsettings values and correct if needed.
//------------------------------------------------------------------------------
u8 bootsettings_verifycorrect()
{
    FIRMWARE_TAG tag;
    u32 crc32etemp;
    bool bootsettingsdirty;
    
    bootsettingsdirty = FALSE;
    
    if(bootsettings.signature != SETTINGS_SIGNATURE)
    {
        bootsettings.signature  = SETTINGS_SIGNATURE;
        bootsettingsdirty = TRUE;
    }
    if(bootsettings.app_version != APPLICATION_VERSION)
    {
        bootsettings.app_version = APPLICATION_VERSION;
        bootsettingsdirty = TRUE;
    }
    
    //IMPORTANT: be sure that failsafe bootloader is valid
    memcpy((u8*)&tag, (u8*)FLASH_FAILSAFE_BOOT_FWTAG_ADDRESS, sizeof(FIRMWARE_TAG));
    if(bootsettings.failsafeboot_version != tag.version && tag.signature == FAILSAFE_BOOTLOADER_OPERATING_MODE)
    {
        bootsettings.failsafeboot_version = tag.version;   
        bootsettingsdirty = TRUE;
    }
    
    crc32e_reset();
    crc32etemp = crc32e_calculateblock(0xFFFFFFFF,(u32*)FLASH_FAILSAFE_BOOT_ADDRESS,FLASH_FAILSAFE_BOOT_SIZE/4);
    if(bootsettings.failsafeboot_crc32e != crc32etemp)
    {
        bootsettings.failsafeboot_crc32e = crc32etemp;
        bootsettingsdirty = TRUE;
    }
        
    //IMPORTANT: be sure that main bootloader is valid
    memcpy((u8*)&tag, (void const*)FLASH_MAIN_BOOT_FWTAG_ADDRESS, sizeof(FIRMWARE_TAG));
    if(bootsettings.mainboot_version != tag.version && tag.signature == MAIN_BOOTLOADER_OPERATING_MODE)
    {
        bootsettings.mainboot_version = tag.version;
        bootsettingsdirty = TRUE;
    }
    
    crc32e_reset();
    crc32etemp = crc32e_calculateblock(0xFFFFFFFF,(u32*)FLASH_MAIN_BOOT_ADDRESS,FLASH_MAIN_BOOT_SIZE/4);
    if(bootsettings.mainboot_crc32e != crc32etemp)
    {
        bootsettings.mainboot_crc32e = crc32etemp;
        bootsettingsdirty = TRUE;
    }
    
    if(bootsettingsdirty)
        return bootsettings_update();
    else
        return S_SUCCESS;
}

//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
u8 bootsettings_retoredefault()
{
    FIRMWARE_TAG tag;
    
    bootsettings.signature  = SETTINGS_SIGNATURE;
    bootsettings.crc32e = 0xFFFFFFFF;
    bootsettings.app_version = APPLICATION_VERSION;
    bootsettings.bootmode = BootloaderMode_MB_Application;

    //IMPORTANT: be sure that failsafe bootloader is valid
    memcpy((u8*)&tag, (u8*)FLASH_FAILSAFE_BOOT_FWTAG_ADDRESS, sizeof(FIRMWARE_TAG));
    bootsettings.failsafeboot_version = tag.version;
    
    crc32e_reset();
    bootsettings.failsafeboot_crc32e = crc32e_calculateblock
        (0xFFFFFFFF,(u32*)FLASH_FAILSAFE_BOOT_ADDRESS,FLASH_FAILSAFE_BOOT_SIZE/4);

    //IMPORTANT: be sure that main bootloader is valid
    memcpy((u8*)&tag, (u8*)FLASH_MAIN_BOOT_FWTAG_ADDRESS, sizeof(FIRMWARE_TAG));
    bootsettings.mainboot_version = tag.version;
    
    crc32e_reset();
    bootsettings.mainboot_crc32e = crc32e_calculateblock
        (0xFFFFFFFF,(u32*)FLASH_MAIN_BOOT_ADDRESS,FLASH_MAIN_BOOT_SIZE/4);

    crc32e_reset();
    bootsettings.app_crc32e = 0xFFFFFFFF;
    bootsettings.app_length = 0;

    return bootsettings_update();
}

//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
u8 bootsettings_update()
{
    u8  *flashbuffer;
    u8  status;

    flashbuffer = __malloc(FLASHSETTINGS_MAX_SETTINGS_SIZE);
    if (flashbuffer == NULL)
    {
        return S_MALLOC;
    }

    flash_load_bootsettings((u8*)flashbuffer,FLASHSETTINGS_MAX_SETTINGS_SIZE);
    memcpy((char*)flashbuffer,(char*)&bootsettings,sizeof(bootsettings));

    crc32e_reset();
    bootsettings.crc32e = 0xFFFFFFFF;
    bootsettings.crc32e = crc32e_calculateblock(0xFFFFFFFF,
                                                (u32*)flashbuffer,
                                                FLASHSETTINGS_MAX_SETTINGS_SIZE/4);
    memcpy((char*)flashbuffer,(char*)&bootsettings,sizeof(bootsettings));
    status = flash_store_bootsettings((u8*)flashbuffer, FLASHSETTINGS_MAX_SETTINGS_SIZE);
    __free(flashbuffer);
    return status;
}

//------------------------------------------------------------------------------
// Get bootsettings by opcode and pack it in encrypted message
// Input:   u16 opcode
// Outputs: u8  *settingsdata
//          u32 *settingsdata_length (max 56 bytes)
// Return:  u8  status
// Engineer: Quyen Leba
//------------------------------------------------------------------------------
u8 bootsettings_get(u16 opcode, u8 *settingsdata, u32 *settingsdata_length)
{
    u8  buffer[16];
    u8  *inputdata_ptr;
    u32 inputdata_length;
    u8  status;

    status = S_SUCCESS;
    memset(buffer,0,sizeof(buffer));

    switch((BootSettingsOpcode)opcode)
    {
    case BootSettingsOpcode_BootMode:
        inputdata_ptr = (u8*)&bootsettings.bootmode;
        inputdata_length = sizeof(bootsettings.bootmode);
        break;
    case BootSettingsOpcode_FailSafeBootVersion:
        inputdata_ptr = (u8*)&bootsettings.failsafeboot_version;
        inputdata_length = sizeof(bootsettings.failsafeboot_version);
        break;
    case BootSettingsOpcode_MainBootVersion:
        inputdata_ptr = (u8*)&bootsettings.mainboot_version;
        inputdata_length = sizeof(bootsettings.mainboot_version);
        break;
    case BootSettingsOpcode_AppVersion:
        inputdata_ptr = (u8*)&bootsettings.app_version;
        inputdata_length = sizeof(bootsettings.app_version);
        break;
    case BootSettingsOpcode_AppSignature:
        inputdata_ptr = (u8*)&bootsettings.app_signature;
        inputdata_length = sizeof(bootsettings.app_signature);
        break;
    case BootSettingsOpcode_FailSafeBootCRC32E:
        memcpy((char*)buffer,(char*)&bootsettings.failsafeboot_crc32e,4);
        inputdata_length = FLASH_FAILSAFE_BOOT_SIZE;
        memcpy((char*)&buffer[4],(char*)&inputdata_length,4);
        inputdata_ptr = buffer;
        inputdata_length = 8;
        break;
    case BootSettingsOpcode_MainBootCRC32E:
        memcpy((char*)buffer,(char*)&bootsettings.mainboot_crc32e,4);
        inputdata_length = FLASH_MAIN_BOOT_SIZE;
        memcpy((char*)&buffer[4],(char*)&inputdata_length,4);
        inputdata_ptr = buffer;
        inputdata_length = 8;
        break;
    case BootSettingsOpcode_AppCRC32E:
        memcpy((char*)buffer,(char*)&bootsettings.app_crc32e,4);
        inputdata_length = bootsettings.app_length;
        memcpy((char*)&buffer[4],(char*)&inputdata_length,4);
        inputdata_ptr = buffer;
        inputdata_length = 8;
        break;
    case BootSettingsOpcode_CustomUSBPID:
        memcpy((char*)buffer,(char*)&bootsettings.customUSBPIDsignature,4);
        memcpy((char*)&buffer[4],(char*)&bootsettings.customUSBPID,4);
        inputdata_ptr = buffer;
        inputdata_length = 8;
        break;
    default:
        *settingsdata_length = 0;
        return S_NOTSUPPORT;
    }

    memcpy(settingsdata,inputdata_ptr,inputdata_length);
    *settingsdata_length = inputdata_length;

    return status;
}

//------------------------------------------------------------------------------
// Set bootsettings by opcode from an encrypted message
// Inputs:  u16 opcode
//          u8  *settingsdata
//          u32 settingsdata_length (max 56 bytes)
// Return:  u8  status
// Engineer: Quyen Leba
//------------------------------------------------------------------------------
u8 bootsettings_set(u16 opcode, u8 *settingsdata, u32 settingsdata_length)
{
    u8  status;

    status = S_SUCCESS;
    if (settingsdata_length == 0 || settingsdata_length > 56)
    {
        return S_INPUT;
    }

    //TODOQ: check for length each opcode
    switch((BootSettingsOpcode)opcode)
    {
    case BootSettingsOpcode_FailSafeBootVersion:
        memcpy((char*)&bootsettings.failsafeboot_version, settingsdata,
               sizeof(bootsettings.failsafeboot_version));
        break;
    case BootSettingsOpcode_MainBootVersion:
        memcpy((char*)&bootsettings.mainboot_version, settingsdata,
               sizeof(bootsettings.mainboot_version));
        break;
    case BootSettingsOpcode_AppVersion:
        memcpy((char*)&bootsettings.app_version, settingsdata,
               sizeof(bootsettings.app_version));
        break;
    case BootSettingsOpcode_AppSignature:
        memcpy((char*)&bootsettings.app_signature, settingsdata,
               sizeof(bootsettings.app_signature));
        break;
    case BootSettingsOpcode_BootMode:
        memcpy((char*)&bootsettings.bootmode, settingsdata,
               sizeof(bootsettings.bootmode));
        break;
    case BootSettingsOpcode_FailSafeBootCRC32E:
        memcpy((char*)&bootsettings.failsafeboot_crc32e, settingsdata,
               sizeof(bootsettings.failsafeboot_crc32e));
        break;
    case BootSettingsOpcode_MainBootCRC32E:
        memcpy((char*)&bootsettings.mainboot_crc32e, settingsdata,
               sizeof(bootsettings.mainboot_crc32e));
        break;
    case BootSettingsOpcode_AppCRC32E:
        memcpy((char*)&bootsettings.app_crc32e, settingsdata,
               sizeof(bootsettings.app_crc32e));
        memcpy((char*)&bootsettings.app_length, (char*)&settingsdata[4],
               sizeof(bootsettings.app_length));
        break;
    case BootSettingsOpcode_CustomUSBPID:
        memcpy((char*)&bootsettings.customUSBPIDsignature, settingsdata,
               sizeof(bootsettings.customUSBPIDsignature));
        memcpy((char*)&bootsettings.customUSBPID, (char*)&settingsdata[4],
               sizeof(bootsettings.customUSBPID));
        break;
    default:
        return S_NOTSUPPORT;
    }

    if (status == S_SUCCESS)
    {
        status = bootsettings_update();
    }
    return status;
}
