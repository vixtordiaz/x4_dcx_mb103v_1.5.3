/*
 *  Common Code Platform
 *
 *  Copyright 2014 SCT Performance, LLC
 *
 */

/**
 * @file    rsa.c
 * @brief   RSA functions
 * @details This file handles RSA functions.
 * @date    7/31/2014 
 * @author  Tristen Pierson
 */ 

#include <board/genplatform.h>
#include <common/filestock.h>
#include <common/statuscode.h>
#include <common/bigdigits.h>
#include <common/checksum.h>
#include <common/rsa.h>

u8 new_signature_data[0x80] = 
{
    0x00,0x01,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0x00,0xCC,0xCC,0xCC,0xCC,0xCC,
    0xCC,0xCC,0xCC,0xCC,0xCC,0xCC,0xCC,0xCC,0xCC,0xCC,0xCC,0x42,0x50,0x47,0x2D,0x38,
    0x31,0x30,0x31,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
    0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
    0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
    0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
    0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
    0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00
};

/******************************************************************************/
/* Functions */
/******************************************************************************/

/**
 * @brief       Generate ME9 UK RSA Signature
 *
 * @details     This function calculates a new RSA signature for Ford ME9 UK
 *
 * @param[in]   start               Starting RSA address
 * @param[in]   end                 Ending RSA address
 * @param[in]   modulus             Modulus
 * @param[in]   signature_address   Address of signature
 *
 * @return      Status
 */
u8 rsa_calc_signature_me9uk(u32 start, u32 end, u8 *modulus, u32 signature_address)
{
    u8 status;    
    u8 new_calc_buffer[0x80];
    u8 temp_buffer[0x80];
    u8 signature[128];
    u8 new_signature[128];
    u8 public_exponent[128];
    u8 rsa_result[128];
    DIGIT_T bdModulus[128/sizeof(DIGIT_T)];
    DIGIT_T bdExponent[128/sizeof(DIGIT_T)];
    DIGIT_T bdSignature[128/sizeof(DIGIT_T)];
    DIGIT_T bdResult[128/sizeof(DIGIT_T)];
    DIGIT_T bdCalcBuff[128/sizeof(DIGIT_T)];
    DIGIT_T bdMultipliedBuff[256/sizeof(DIGIT_T)];
    DIGIT_T bdTempBuffer[128/sizeof(DIGIT_T)];
    u8 md5_hash[16];
    u32 bytes_read;
    int counter;    
    int shifter;
    char val;
    char new_pos;

    memcpy(new_signature, new_signature_data, sizeof(new_signature));
    
    /* Get the signature */
    status = filestock_read_at_position(signature_address, signature, 0x80, &bytes_read);
    
    /* Calculate MD5 Hash */
    if (status == S_SUCCESS)
    {
        status = checksum_calc_file_segment_md5(start, end, md5_hash);
    }
                
    /**********************************************************************/
    /** Decrypt RSA Signature **/
    /**********************************************************************/
    if (status == S_SUCCESS)
    {
        memset(public_exponent,0x00,0x80);
        public_exponent[0x7F] = 0x03;	    /* The public exponent is 'e' = 0x00000003 */

        status = rsa_decrypt(modulus, public_exponent, signature, rsa_result);
    }
    /**********************************************************************/
    /** Compare decrypted signature to MD5 and sign new key if needed **/
    /**********************************************************************/
    if (status == S_SUCCESS)
    {
        if(memcmp(rsa_result + 0x0B, md5_hash, 0x10) != NULL)        
        {
            /* Prepare new rsa_result to be signed */
            memcpy(new_signature + 0x0B, md5_hash, 0x10);

            /* Still using the public key */
            mpConvFromOctets(bdModulus, MOD_SIZE, modulus, 0x80);

            /* The public exponent is 'e' = 0x00000003 */
            memset(public_exponent, 0x00, 0x80);
            public_exponent[0x7F] = 0x03;
            mpConvFromOctets(bdExponent, MOD_SIZE, public_exponent, 0x80);

            /* Zero out the rsa_result */
            memset(rsa_result,0x00,0x80);

            /* Subtract signature from public key */
            mpConvFromOctets(bdSignature, MOD_SIZE, new_signature, 0x80);        
            mpSubtract(bdResult, bdModulus, bdSignature, 0x20);            
            mpConvToOctets(bdResult, MOD_SIZE, new_signature, 0x80);            
            
            /* Zero out the new signature from 0x29 to the end */
            memset(new_signature + 0x29, 0x00, 0x57);
            mpConvFromOctets(bdSignature, MOD_SIZE, new_signature, 0x80);

            memset(new_calc_buffer, 0x00, 0x80);
            memset(temp_buffer, 0x00, 0x80);

            /******************************************************************/
            /** Generate key **/
            /******************************************************************/
            counter = 0x155;
            while(counter >= 0)
            {
                shifter = counter & 0x07;
                val = (char)(0x01 << (char)shifter);
                new_pos = 0x7F - (char)(counter >> 0x03);

                new_calc_buffer[new_pos] |= val;

                mpConvFromOctets(bdCalcBuff, MOD_SIZE, new_calc_buffer, 0x80);
                mpSquare(bdMultipliedBuff, bdCalcBuff, 0x20);

                memcpy(bdTempBuffer, bdMultipliedBuff, 0x80);
                mpMultiply(bdMultipliedBuff, bdTempBuffer, bdCalcBuff, 0x20);

                mpConvToOctets(bdMultipliedBuff, MOD_SIZE, temp_buffer, 0x80);

                if(mpCompare(bdSignature, bdMultipliedBuff, 0x20) <= 0)
                {
                    new_calc_buffer[new_pos] &= ~val;
                }
                counter--;
            }
            
            mpConvFromOctets(bdCalcBuff, MOD_SIZE, new_calc_buffer, 0x80);
            mpSubtract(bdResult, bdModulus, bdCalcBuff, 0x20);
            mpConvToOctets(bdResult, MOD_SIZE, rsa_result, 0x80);

            /******************************************************************/
            /** Write the new signature **/
            /******************************************************************/            
            status = filestock_updateflashfile(signature_address, rsa_result, 0x80);
        }
        else
        {
            status = S_SUCCESS;
        }
    }

    return status;
}

/**
 * @brief       Decrypt RSA
 *
 * @details     This function decrypts an RSA signature
 *
 * @param[in]   modulus             RSA Modulus
 * @param[in]   public_exponent     RSA Public Exponent
 * @param[in]   signature           Encrypted Signature
 * @param[out]  result              Decrypted Signature
 *
 * @return      Status
 */
u8 rsa_decrypt(u8 *modulus, u8 *public_exponent, u8 *signature, u8 *result)
{
    DIGIT_T bdModulus[128/sizeof(DIGIT_T)];
    DIGIT_T bdExponent[128/sizeof(DIGIT_T)];
    DIGIT_T bdSignature[128/sizeof(DIGIT_T)];
    DIGIT_T bdResult[128/sizeof(DIGIT_T)];

    memset(result,0x00,0x80);

    /* Convert to 32-bit numbers */
    mpConvFromOctets(bdModulus, MOD_SIZE, modulus, 0x80);
    mpConvFromOctets(bdExponent, MOD_SIZE, public_exponent, 0x80);
    mpConvFromOctets(bdSignature, MOD_SIZE, signature, 0x80);
    mpConvFromOctets(bdResult, MOD_SIZE, result, 0x80);
    
    /* Decrypt the signature */
    mpModExp(bdResult, bdSignature, bdExponent, bdModulus, MOD_SIZE);

    /* Convert rsa_result back to 8-bit */
    mpConvToOctets(bdResult, MOD_SIZE, result, 0x80);
    
    return S_SUCCESS;
}