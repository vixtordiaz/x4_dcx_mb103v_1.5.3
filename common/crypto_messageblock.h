/**
  **************** (C) COPYRIGHT 2011 SCT Performance, LLC *********************
  * File Name          : crypto_messageblock.h
  * Device / System    : SCT products using the "CommonCode" platform
  * Author             : Quyen Leba
  *
  * Version            : 1 
  * Date               : 05/02/2011
  * Description        : 
  *                    : 
  *
  *
  * History            : 05/02/2011 M. Davis
  *                    :   Add SCT Copyright header,
  *
  ******************************************************************************
  */

#ifndef __CRYPTO_MESSAGEBLOCK_H
#define __CRYPTO_MESSAGEBLOCK_H

#include <arch/gentype.h>

#define MAX_CRYPTO_MESSAGE_BLOCK_LENGTH     64
#define CRYPTO_USE_INTERNAL_KEY             TRUE
#define CRYPTO_USE_EXTERNAL_KEY             FALSE

typedef enum
{
    CryptoMessageBlockLength_Exact64,   //64 must be smallest length
    CryptoMessageBlockLength_Exact256,
    CryptoMessageBlockLength_Exact512,
    CryptoMessageBlockLength_Exact800,
    CryptoMessageBlockLength_UptoMax,
}CryptoMessageBlockLength;

u8 crypto_messageblock_decrypt(u8 *encrypted_data, u32 encrypted_data_length,
                               u8 *decrypted_data, u32 *decrypted_data_length,
                               bool extendedlength, bool useinternalkey);

u8 crypto_messageblock_encrypt(u8 *raw_data, u32 raw_data_length,
                               u8 *encrypted_data, u32 *encrypted_data_length,
                               CryptoMessageBlockLength lengthtype, bool useinternalkey);

#endif  //__CRYPTO_MESSAGEBLOCK_H
