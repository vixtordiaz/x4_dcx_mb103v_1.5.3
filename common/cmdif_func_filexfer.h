/**
  **************** (C) COPYRIGHT 2011 SCT Performance, LLC *********************
  * File Name          : cmdif_func_filexfer.h
  * Device / System    : SCT products using the "CommonCode" platform
  * Author             : Quyen Leba
  *
  * Version            : 1 
  * Date               : 05/02/2011
  * Description        : 
  *                    : 
  *
  *
  * History            : 05/02/2011 M. Davis
  *                    :   Add SCT Copyright header,
  *
  ******************************************************************************
  */

#ifndef __CMDIF_FUNC_FILEXFER_H
#define __CMDIF_FUNC_FILEXFER_H

#include <arch/gentype.h>
#include <fs/genfs.h>

typedef struct
{
    u32 filesize;
    u32 transfer_id;
    u32 currentposition;
    u32 offset;
    u32 blocklength;
    F_FILE *filestream;
    //-Q- @@@ u8  *databuffer;
    u8  databuffer[(2048+16)];
}filexfer_info;

typedef enum
{
    FolderTask_Create                   = 1,
    FolderTask_Remove                   = 2,
    FolderTask_CheckExist               = 3,
    FolderTask_GetListing               = 4,
}filexfer_foldertask;

typedef enum
{
    FileTask_GetFileSize                = 1,
    FileTask_Delete                     = 2,
    FileTask_DlxFileList                = 3,
    FileTask_WholeFileCRC32E            = 4,
    FileTask_TuneOptionFileHeaderCRC32E = 5,
    FileTask_Rename                     = 6,
    FileTask_MergeDLX                   = 7,
    FileTask_GenerateDatalogFeature     = 8,
}filexfer_filetask;

u8 cmdif_func_filexfer_open_file(u8 *opendata);
u8 cmdif_func_filexfer_fclose_filetransfer();
u8 cmdif_func_filexfer_folder(filexfer_foldertask task, u8 *folder_name,
                              u16 privdata, u8 *optional_data);
u8 cmdif_func_filexfer_check_file(u8 *filedata);
u8 cmdif_func_filexfer_read_block(u8 *requestdata);
u8 cmdif_func_filexfer_read_to_monitor();
u8 cmdif_func_filexfer_write_block(u8 *requestdata);
u8 cmdif_func_filexfer_write_to_dongle(u8 *writedatainfo);

u8 cmdif_func_filexfer_fopen_filetransfer(u8 *opendata);
u8 cmdif_func_filexfer_fread_block(u32 offset, u32 blocklength);
u8 cmdif_func_filexfer_fwrite_block(u32 offset, u32 blocklength, u8 *blockdata);
u8 cmdif_func_filexfer_file(filexfer_filetask task, u8 *file_name,
                            u16 privdata);

#endif    //__CMDIF_FUNC_FILEXFER_H
