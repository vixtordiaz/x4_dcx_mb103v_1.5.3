/**
  **************** (C) COPYRIGHT 2011 SCT Performance, LLC *********************
  * File Name          : cmdif_function.c
  * Device / System    : SCT products using the "CommonCode" platform
  * Author             : Quyen Leba
  *
  * Version            : 1 
  * Date               : 05/02/2011
  * Description        : 
  *                    : 
  *
  *
  * History            : 05/02/2011 M. Davis
  *                    :   Add SCT Copyright header,
  *
  ******************************************************************************
  */

#include <string.h>
#include <stdio.h>
#include <device_version.h>
#include <board/delays.h>
#include <board/genplatform.h>
#include <board/indicator.h>
#include <board/properties_vb.h>
#include <common/statuscode.h>
#include <common/crypto_messageblock.h>
#include <common/genmanuf_overload.h>
#include <common/deviceinfo.h>
#include <common/housekeeping.h>
#include <common/vehicleinfolog.h>
#if USE_CC3000_WIFI
#include <common/cmdif_func_wifi.h>
#endif
#include <common/cmdif_func_production_test.h>
#include <common/cmdif.h>
#include "cmdif_function.h"

extern cmdif_datainfo cmdif_datainfo_responsedata;  /* from cmdif.c */
extern u8 cmdif_scratchpad[512];                    /* from cmdif.c */
extern u16 cmdif_scratchpad_length;                 /* from cmdif.c */
extern obd2_info gObd2info;                         /* from obd2.c */
extern flasher_info *flasherinfo;                   /* from obd2tune.c */

//------------------------------------------------------------------------------
// Read VIN
// This function also copies VIN to flasherinfo (if flasherinfo initialized)
// Return:  u8  status
// Engineer: Quyen Leba
//------------------------------------------------------------------------------
u8 cmdif_func_readvin()
{
    u8 *bptr;
    u8 status;
    //VehicleCommType *commtype;

    cmdif_datainfo_responsedata.responsecode = CMDIF_ACK_FAILED;
    bptr = __malloc(VIN_LENGTH + 1 + sizeof(VehicleCommType));
    //commtype = (VehicleCommType*)&bptr[VIN_LENGTH + 1];
    //*commtype = CommType_Unknown;
    if (bptr)
    {
        status = obd2_readvin(bptr);
        cmdif_datainfo_responsedata.dataptr = bptr;
        cmdif_datainfo_responsedata.datalength = 
            VIN_LENGTH + sizeof(VehicleCommType);
        if (status == S_SUCCESS || status == S_VINBLANK || status == S_VININVALID)
        {
            status = S_SUCCESS;
            cmdif_datainfo_responsedata.responsecode = CMDIF_ACK_OK;
            if (flasherinfo)
            {
                memcpy((char*)flasherinfo->vin,(char*)bptr,VIN_LENGTH);
                flasherinfo->vin[VIN_LENGTH] = NULL;
            }
        }
        else
        {
            if (flasherinfo)
            {
                flasherinfo->vin[0] = NULL;
            }
#ifndef __DEBUG_JTAG_
            //in case VB crash or stuck. Only in RELEASE so we can see this in development
            peripherals_init_vehicleboard();
#endif
        }
    }
    else
    {
        status = S_MALLOC;
    }
    return status;
}

//------------------------------------------------------------------------------
// Clear DTC
// Engineer: Patrick Downs, Quyen Leba
//------------------------------------------------------------------------------
u8 cmdif_func_cleardtc()
{
    u8 status;

    status = obd2_cleardtc(CommType_Unknown, 1);
    if (status == S_SUCCESS)
    {
        cmdif_datainfo_responsedata.responsecode = CMDIF_ACK_OK;
    }
    else
    {
#ifndef __DEBUG_JTAG_
        //in case VB crash or stuck. Only in RELEASE so we can see this in development
        peripherals_init_vehicleboard();
#endif
        cmdif_datainfo_responsedata.responsecode = CMDIF_ACK_FAILED;
    }
    return status;
}

//------------------------------------------------------------------------------
// Read DTC (all DTCs are in original data)
// Engineer: Patrick Downs, Quyen Leba
//------------------------------------------------------------------------------
u8 cmdif_func_readdtc_raw()
{
    u8 *bptr;
    u8 status;

    bptr = __malloc(sizeof(dtc_info));
    if (bptr)
    {
        status = obd2_readdtc((dtc_info*)bptr);
        cmdif_datainfo_responsedata.dataptr = bptr;
        cmdif_datainfo_responsedata.datalength = sizeof(dtc_info);
        if (status == S_SUCCESS)
        {
            cmdif_datainfo_responsedata.responsecode = CMDIF_ACK_OK;
        }
        else
        {
#ifndef __DEBUG_JTAG_
            //in case VB crash or stuck. Only in RELEASE so we can see this in development
            peripherals_init_vehicleboard();
#endif
        }
    }
    else
    {
        status = S_MALLOC;
    }
    return status;
}

//------------------------------------------------------------------------------
// Read DTC (codes are formatted and include description if available)
// Engineer: Patrick Downs, Quyen Leba
//------------------------------------------------------------------------------
u8 cmdif_func_readdtc_new()
{
    dtc_info dtc_info;
    u8 *bptr;
    u32 length;
    u32 i;
    u16 tmpcode;
    u8  tmpecmindex;
    u8 tmpcodebuf[16];
    u8 tmpbuf[256];
    u32 dtctotallength;
    u8 status;
    VehicleCommType vehiclecommtype[ECM_MAX_COUNT];
    VehicleCommLevel vehiclecommlevel[ECM_MAX_COUNT];

    cmdif_datainfo_responsedata.responsecode = CMDIF_ACK_FAILED;
    
    status = obd2_findvehiclecommtypecommlevel(vehiclecommtype, vehiclecommlevel);
    
    if (status == S_SUCCESS)
    {
        status = obd2_readdtc(&dtc_info);
    }
    
    if (status == S_SUCCESS)
    {
#if (USE_UARTTYPE_DATALINK)     //TODOQ: make this a variable
#define MAX_READDTC_DATALINK_BUFFER_LEN     2048
        bptr = __malloc(MAX_READDTC_DATALINK_BUFFER_LEN);
        if (bptr == NULL)
        {
            goto cmdif_func_readdtc_new_done;
        }
        memset(bptr,0,MAX_READDTC_DATALINK_BUFFER_LEN);
        cmdif_datainfo_responsedata.dataptr = bptr;
        cmdif_datainfo_responsedata.datalength = 0;
#else
        bptr = XXX
#endif
        bptr[0] = dtc_info.count;
        bptr[1] = 0;
        bptr[2] = 0;
        length = 2;

        for(i=0;i<dtc_info.count;i++)
        {
            tmpcode = dtc_info.codes[i];
            tmpecmindex = dtc_info.code_ecmindex[i];
            tmpcodebuf[0] = obd2_parsetype_dtc(tmpcode);
            tmpcode &= 0x3FFF;
            sprintf((void*)&tmpcodebuf[1],"%04X",tmpcode);
            sprintf((char*)tmpbuf,"<C>%s<D>",tmpcodebuf);

            if ((length + strlen((char*)tmpbuf)) < (MAX_READDTC_DATALINK_BUFFER_LEN-3))
            {
                strcat((char*)&bptr[length],(char*)tmpbuf);
                length += strlen((char*)tmpbuf);

                status = obd2_getdtcinfo(vehiclecommlevel[tmpecmindex],tmpcodebuf,tmpbuf);
                if (status == S_SUCCESS)
                {
                    if ((length + strlen((char*)tmpbuf)) < (MAX_READDTC_DATALINK_BUFFER_LEN-3))
                    {
                        strcat((char*)&bptr[length],(char*)tmpbuf);
                        length += strlen((char*)tmpbuf);
                    }
                }
            }//if ((length + ...
        }//for(i=0;i<dtc_info.count;i++)...

        dtctotallength = strlen((char*)&bptr[2]) + 3;

#if (USE_UARTTYPE_DATALINK)     //TODOQ: make this a variable
        cmdif_datainfo_responsedata.datalength = dtctotallength;
#else
        cmdif_scratchpad_length = dtctotallength;

        cmdif_datainfo_responsedata.dataptr = __malloc(4);
        cmdif_datainfo_responsedata.datalength = 4;
        cmdif_datainfo_responsedata.dataptr[0] = dtctotallength;
        cmdif_datainfo_responsedata.dataptr[1] = dtctotallength >> 8;
        cmdif_datainfo_responsedata.dataptr[2] = dtctotallength >> 16;
        cmdif_datainfo_responsedata.dataptr[3] = dtctotallength >> 24;
#endif
        cmdif_datainfo_responsedata.responsecode = CMDIF_ACK_OK;
        cmdif_datainfo_responsedata.flowcommandcode = CMDIF_CMD_READ_DTCS_NEW;
    }
    else
    {
#ifndef __DEBUG_JTAG_
        //in case VB crash or stuck. Only in RELEASE so we can see this in development
        peripherals_init_vehicleboard();
#endif
        cmdif_datainfo_responsedata.responsecode = CMDIF_ACK_FAILED;
    }

cmdif_func_readdtc_new_done:
    return S_SUCCESS;
}

//------------------------------------------------------------------------------
// Read ECM info
// Engineer: Quyen Leba
//------------------------------------------------------------------------------
u8 cmdif_func_readecminfo()
{
    u8 *bptr;
    u8 status;
    VehicleCommType vehiclecommtype;

    vehiclecommtype = CommType_Unknown;
    bptr = __malloc(sizeof(ecm_info));
    if (bptr)
    {        
        status = obd2_readecminfo((ecm_info*)bptr,&vehiclecommtype);
        cmdif_datainfo_responsedata.dataptr = bptr;
        cmdif_datainfo_responsedata.datalength = sizeof(ecm_info);
        if (status == S_SUCCESS)
        {
            if (flasherinfo)
            {
                memcpy((char*)flasherinfo->vin,
                       (char*)((ecm_info*)bptr)->vin,VIN_LENGTH);
                flasherinfo->vin[VIN_LENGTH] = NULL;

                memcpy((char*)flasherinfo->vehicle_serial_number,
                       (char*)((ecm_info*)bptr)->vehicle_serial_number,
                       sizeof(flasherinfo->vehicle_serial_number));
            }
            cmdif_datainfo_responsedata.responsecode = CMDIF_ACK_OK;
        }
        else
        {
#ifndef __DEBUG_JTAG_
            //in case VB crash or stuck. Only in RELEASE so we can see this in development
            peripherals_init_vehicleboard();
#endif
        }
    }
    else
    {
        status = S_MALLOC;
    }
    return status;
}

//------------------------------------------------------------------------------
// Read vehicle info (VIN, commtype, strategy, partnumbers, etc)
// Engineer: Patrick Downs, Quyen Leba
//------------------------------------------------------------------------------
u8 cmdif_func_readvehicleinfo()
{
    vehicle_info vehicleinfo;
    u8 *ptr;
    u8 status;
    u32 infototallength;

    cmdif_datainfo_responsedata.responsecode = CMDIF_ACK_FAILED;
    vehicleinfo.commtype = CommType_Unknown;

    status = obd2_readecminfo(&vehicleinfo.ecm,&vehicleinfo.commtype);
    if (status == S_SUCCESS)
    {
        // Save data to vehicle info log file.
        vehicleinfolog_add_block(&vehicleinfo.ecm);
        
#if (USE_UARTTYPE_DATALINK)     //TODOQ: make this a variable
#define MAX_VEHICLEINFO_DATALINK_BUFFER_LEN     1024
        ptr = __malloc(MAX_VEHICLEINFO_DATALINK_BUFFER_LEN);
        if (ptr == NULL)
        {
            goto cmdif_func_readvehicleinfo_done;
        }
        cmdif_datainfo_responsedata.dataptr = ptr;
        cmdif_datainfo_responsedata.datalength = 0;
        //TODOQ: check for length, make sure not more than 1024 as malloc'ed
#else
        cmdif_scratchpad[0] = 0;
        ptr = cmdif_scratchpad;
        cmdif_scratchpad_length = 0;
        cmdif_datainfo_responsedata.dataptr = __malloc(4);
        cmdif_datainfo_responsedata.datalength = 4;
#endif

        switch(obd2_get_oemtype())
        {
#ifdef __DCX_MANUF__
        case OemType_DCX:
            obd2_dcx_vehicleinfototext(&vehicleinfo,ptr);
            break;
#endif
#ifdef __FORD_MANUF__
        case OemType_FORD:
            obd2_ford_vehicleinfototext(&vehicleinfo,ptr);
            break;
#endif
#ifdef __GM_MANUF__
        case OemType_GM:
            obd2_gm_vehicleinfototext(&vehicleinfo,ptr);
            break;
#endif   
        default:
            obd2_vehicleinfototext(&vehicleinfo,ptr);
            break;
        }
        
        infototallength = strlen((char*)ptr)+1;

#if (USE_UARTTYPE_DATALINK)     //TODOQ: make this a variable
        cmdif_datainfo_responsedata.datalength = infototallength;
#else
        cmdif_scratchpad_length = infototallength;
        cmdif_datainfo_responsedata.dataptr[0] = infototallength;
        cmdif_datainfo_responsedata.dataptr[1] = infototallength >> 8;
        cmdif_datainfo_responsedata.dataptr[2] = infototallength >> 16;
        cmdif_datainfo_responsedata.dataptr[3] = infototallength >> 24;
#endif

        cmdif_datainfo_responsedata.responsecode = CMDIF_ACK_OK;
        cmdif_datainfo_responsedata.flowcommandcode = CMDIF_CMD_GET_VEHICLE_INFO;
    }

cmdif_func_readvehicleinfo_done:
    return status;
}

//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
u8 cmdif_func_ping_response()
{
    cmdif_datainfo_responsedata.responsecode = CMDIF_ACK_OK;
    return S_SUCCESS;
}

//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
u8 cmdif_func_unsupported_command()
{
    cmdif_datainfo_responsedata.responsecode = CMDIF_ACK_NOT_SUPPORTED;
    return S_SUCCESS;
}

//------------------------------------------------------------------------------
// Get device info
// Return:  u8  status
// Engineer: Patrick Downs, Quyen Leba
//------------------------------------------------------------------------------
u8 cmdif_func_get_device_info()
{
    u8  *ptr;
    u32 infototallength;
    u8  status;

    cmdif_datainfo_responsedata.responsecode = CMDIF_ACK_FAILED;
    status = S_SUCCESS;

#if (USE_UARTTYPE_DATALINK)     //TODOQ: make this a variable
#define MAX_DEVICEINFO_DATALINK_BUFFER_LEN      1024
    ptr = __malloc(MAX_DEVICEINFO_DATALINK_BUFFER_LEN);
    if (ptr == NULL)
    {
        goto cmdif_func_get_device_info_done;
    }

    memset((char*)ptr,0,MAX_DEVICEINFO_DATALINK_BUFFER_LEN);
    cmdif_datainfo_responsedata.dataptr = ptr;
    cmdif_datainfo_responsedata.datalength = 0;
    //TODOQ: check for length, make sure not more than 1024 as malloc'ed
#else
    cmdif_scratchpad[0] = 0;
    ptr = cmdif_scratchpad;
    cmdif_scratchpad_length = 0;
#endif

    deviceinfo_getinfostring(ptr, MAX_DEVICEINFO_DATALINK_BUFFER_LEN);
    infototallength = strlen((char*)ptr)+1;

#if (USE_UARTTYPE_DATALINK)     //TODOQ: make this a variable
    cmdif_datainfo_responsedata.datalength = infototallength;
#else
    cmdif_datainfo_responsedata.dataptr = __malloc(4);
    cmdif_datainfo_responsedata.datalength = 4;

    cmdif_scratchpad_length = infototallength;
    cmdif_datainfo_responsedata.dataptr[0] = infototallength;
    cmdif_datainfo_responsedata.dataptr[1] = infototallength >> 8;
    cmdif_datainfo_responsedata.dataptr[2] = infototallength >> 16;
    cmdif_datainfo_responsedata.dataptr[3] = infototallength >> 24;
#endif

    cmdif_datainfo_responsedata.responsecode = CMDIF_ACK_OK;
    cmdif_datainfo_responsedata.flowcommandcode = CMDIF_CMD_GET_DONGLE_INFO;

cmdif_func_get_device_info_done:
    return status;
}

//------------------------------------------------------------------------------
// Get device data (settings, etc)
// Input:   u8  __opcode (what data is requesting)
// Return   u8  status
// Engineer: Patrick Downs, Quyen Leba
//------------------------------------------------------------------------------
u8 cmdif_func_get_device_data(u8 dataopcode)
{
    CMDIF_GETDEVICEDATA_OPCODE opcode = (CMDIF_GETDEVICEDATA_OPCODE)dataopcode;
    u8  *bptr;
    u8  databuffer[64];
    u32 databufferlength;

    cmdif_datainfo_responsedata.responsecode = CMDIF_ACK_OK;
    databufferlength = 0;

    switch(opcode)
    {
    case CMDIF_GETDEVICEDATA_MODESTATUS:
        databuffer[0] = CMDIF_ACK_NORMAL_MODE;
        databufferlength = 1;
        break;
    case CMDIF_GETDEVICEDATA_SERIAL:
        strncpy((char*)databuffer, (char*)SETTINGS_CRITICAL(serialnumber),
                sizeof(SETTINGS_CRITICAL(serialnumber)));
        databufferlength = sizeof(SETTINGS_CRITICAL(serialnumber));
        break;
    case CMDIF_GETDEVICEDATA_FIRMWARE:
        cmdif_datainfo_responsedata.responsecode = CMDIF_ACK_UNSUPPORTED;
        //TODOQ: this
//        databuffer[0] = settings.FirmwareVersionSettings.i.MH;
//        databuffer[1] = settings.FirmwareVersionSettings.i.MH>>8;
//        databuffer[2] = settings.FirmwareVersionSettings.i.MH>>16;
//        databuffer[3] = settings.FirmwareVersionSettings.i.MH>>24;
//        databuffer[4] = settings.FirmwareVersionSettings.i.FB;
//        databuffer[5] = settings.FirmwareVersionSettings.i.FB>>8;
//        databuffer[6] = settings.FirmwareVersionSettings.i.FB>>16;
//        databuffer[7] = settings.FirmwareVersionSettings.i.FB>>24;
        break;
    case CMDIF_GETDEVICEDATA_TUNEREV:
        cmdif_datainfo_responsedata.responsecode = CMDIF_ACK_UNSUPPORTED;
        //TODOQ: this
//        strcpy((char *)databuffer, settings.TuneRevision);
        break;
    case CMDIF_GETDEVICEDATA_DEVICETYPE:
        databuffer[0] = SETTINGS_CRITICAL(devicetype);
        databuffer[1] = SETTINGS_CRITICAL(devicetype) >> 8;
        databuffer[2] = SETTINGS_CRITICAL(devicetype) >> 16;
        databuffer[3] = SETTINGS_CRITICAL(devicetype) >> 24;
        databufferlength = 4;
        break;
    case CMDIF_GETDEVICEDATA_MARKETTYPE:
        databuffer[0] = SETTINGS_CRITICAL(markettype);
        databuffer[1] = SETTINGS_CRITICAL(markettype) >> 8;
        databuffer[2] = SETTINGS_CRITICAL(markettype) >> 16;
        databuffer[3] = SETTINGS_CRITICAL(markettype) >> 24;
        databufferlength = 4;
        break;
    case CMDIF_GETDEVICEDATA_OEMTYPE:
        databuffer[0] = (u8)obd2_get_oemtype();
        databufferlength = 1;
        break;
    case CMDIF_GETDEVICEDATA_MARRIEDSTATUS:
        databuffer[0] = SETTINGS_TUNE(married_status);
        databufferlength = 1;
        break;
    case CMDIF_GETDEVICEDATA_MARRIEDCOUNT:
        databuffer[0] = SETTINGS_TUNE(married_count);
        databufferlength = 1;
        break;
    case CMDIF_GETDEVICEDATA_MARRIEDVIN:
        strncpy((char*)databuffer, (char*)SETTINGS_TUNE(vin),VIN_LENGTH+1);
        databufferlength = VIN_LENGTH+1;
        break;
    case CMDIF_GETDEVICEDATA_BETAFLAG:
        //TODOQ: this
//        strncpy((char*)databuffer, (char*)BOOTLOADBLOCKPAGE, 4);
        cmdif_datainfo_responsedata.responsecode = CMDIF_ACK_UNSUPPORTED;
        break;    
    default:
        cmdif_datainfo_responsedata.responsecode = CMDIF_ACK_UNSUPPORTED;
        break;
    }

    if (cmdif_datainfo_responsedata.responsecode == CMDIF_ACK_OK)
    {
        if (databufferlength > sizeof(databuffer))
        {
            cmdif_datainfo_responsedata.responsecode = CMDIF_ACK_FAILED;
        }
        else
        {
            bptr = __malloc(sizeof(databuffer));
            if (bptr == NULL)
            {
                cmdif_datainfo_responsedata.responsecode = CMDIF_ACK_FAILED;
            }
            else
            {
                memcpy(bptr,databuffer,sizeof(databuffer));
                cmdif_datainfo_responsedata.dataptr = bptr;
                cmdif_datainfo_responsedata.datalength = databufferlength;
            }
        }
    }
    
    return S_SUCCESS;
}

//------------------------------------------------------------------------------
// Get dongle settings
// Inputs:  u8  settings_opcode
//          u16 privdata
// Return   u8  status
// Engineer: Patrick Downs, Quyen Leba
//------------------------------------------------------------------------------
u8 cmdif_func_get_dongle_settings(u8 settings_opcode, u16 privdata)
{
    u8  settings[128];
    u8  *encrypted_buffer_bptr;
    u32 length;
    u8  status;
    
    cmdif_datainfo_responsedata.responsecode = CMDIF_ACK_FAILED;

    status = settings_getsettings_byopcode((SETTINGS_OPCODE)settings_opcode,
                                           privdata,
                                           settings,&length);
    if (status == S_SUCCESS)
    {
        encrypted_buffer_bptr = __malloc(MAX_CRYPTO_MESSAGE_BLOCK_LENGTH);
        if (encrypted_buffer_bptr == NULL)
        {
            goto cmdif_func_get_dongle_settings_done;
        }

        status = crypto_messageblock_encrypt(settings,length,
                                             encrypted_buffer_bptr,
                                             NULL, CryptoMessageBlockLength_Exact64, CRYPTO_USE_EXTERNAL_KEY);
        
        if (status == S_SUCCESS)
        {
            cmdif_datainfo_responsedata.dataptr =
                encrypted_buffer_bptr;
            cmdif_datainfo_responsedata.datalength =
                MAX_CRYPTO_MESSAGE_BLOCK_LENGTH;
            cmdif_datainfo_responsedata.responsecode = CMDIF_ACK_OK;
        }
        else
        {
            __free(encrypted_buffer_bptr);
        }
    }
    else
    {
        if (status == S_SERVICENOTSUPPORTED)
        {
            cmdif_datainfo_responsedata.responsecode = CMDIF_ACK_UNSUPPORTED;
        }
        else
        {
            cmdif_datainfo_responsedata.responsecode = CMDIF_ACK_FAILED;
        }
    }
    
cmdif_func_get_dongle_settings_done:
    return S_SUCCESS;
}

//------------------------------------------------------------------------------
// Set preloaded tune restriction
// Input:   u32 restriction_value (see PRELOADEDTUNE_RESTRICTION_...)
// Return   u8  status
// Engineer: Quyen Leba
//------------------------------------------------------------------------------
u8 cmdif_func_preloaded_tune_set_restriction(u32 restriction_value)
{
    u8  status;

    status = obd2tune_preloaded_tune_set_restriction(restriction_value);
    if (status == S_SUCCESS)
    {
        cmdif_datainfo_responsedata.responsecode = CMDIF_ACK_OK;
    }
    else if(status == S_INVALIDSETTINGS)
    {
        cmdif_datainfo_responsedata.responsecode = CMDIF_ACK_MARRIED_FAIL;
    }   
    else
    {
        cmdif_datainfo_responsedata.responsecode = CMDIF_ACK_FAILED;
    }
    return status;
}

//------------------------------------------------------------------------------
// Enter bootloader
// Input:   u8  bootloadertype
// Return   u8  status
// Engineer: Patrick Downs, Quyen Leba
//------------------------------------------------------------------------------
u8 cmdif_func_enter_bootloader(u8 bootloadertype)
{
//    u8  status;
//    
//    status = flash_save_bootloader_signature((BootloaderType)bootloadertype);
//    if (status != S_SUCCESS)
//    {
//        cmdif_internalresponse_ack(CMDIF_CMD_ENTER_BOOTLOAD,
//                                   CMDIF_ACK_FAILED,NULL,0);
//    }
//    else
//    {
//        cmdif_internalresponse_ack(CMDIF_CMD_ENTER_BOOTLOAD,
//                                   CMDIF_ACK_OK,NULL,0);
//        //bootloader_jump_to_main_bootloader();
//        usbcmds_start_primary_bootloader(); //TODO:
//    }
//    
    return S_SUCCESS;
}

//------------------------------------------------------------------------------
// Set bootloader mode
// Input:   BootloaderMode bootmode
// Return   u8  status
// Engineer: Quyen Leba
//------------------------------------------------------------------------------
u8 cmdif_func_bootloader_setboot(BootloaderMode bootmode)
{
    u8  status;

    cmdif_datainfo_responsedata.responsecode = CMDIF_ACK_FAILED;
    status = bootloader_setboot(bootmode);
    if (status == S_SUCCESS)
    {
        cmdif_datainfo_responsedata.responsecode = CMDIF_ACK_OK;
        watchdog_set();
    }
    return S_SUCCESS;
}

//------------------------------------------------------------------------------
// Init bootloader session
// Input:   u8  *encryptedheader (header of firmware)
// Return   u8  status
// Engineer: Quyen Leba
//------------------------------------------------------------------------------
u8 cmdif_func_bootloader_init(u8 *encryptedheader)
{
    u8  *ptr;
    u8  status;
    u16 sector_size;

    cmdif_datainfo_responsedata.responsecode = CMDIF_ACK_FAILED;

    ptr = __malloc(16);
    if (ptr == NULL)
    {
        return S_MALLOC;
    }

    status = bootloader_setup_session(encryptedheader,&sector_size,TRUE);
    if (status == S_SUCCESS)
    {
        memcpy((char*)ptr,(char*)&sector_size,sizeof(sector_size));

        cmdif_datainfo_responsedata.dataptr = ptr;
        cmdif_datainfo_responsedata.datalength = sizeof(sector_size);
        cmdif_datainfo_responsedata.responsecode = CMDIF_ACK_OK;
    }
    else if(status == S_NO_UPDATE)
    {
        cmdif_datainfo_responsedata.responsecode = CMDIF_ACK_DONE;
    }
    else if(status == S_INVALID_VERSION)
    {
        cmdif_datainfo_responsedata.responsecode = CMDIF_ACK_BACKDATE;
    }
    else if (status == S_INPUT)
    {
        cmdif_datainfo_responsedata.responsecode = CMDIF_ACK_UNSUPPORTED;
    }

    return S_SUCCESS;
}

//------------------------------------------------------------------------------
// Program firmware to flash
// Inputs:  u8  *encrypteddata (firmware content)
//          u32 length
// Return   u8  status
// Engineer: Quyen Leba
//------------------------------------------------------------------------------
u8 cmdif_func_bootloader_do(u8 *encrypteddata, u32 length)
{
    u8  status;

    cmdif_datainfo_responsedata.responsecode = CMDIF_ACK_FAILED;
    status = bootloader_write_flash(encrypteddata,length);
    if (status == S_SUCCESS)
    {
        cmdif_datainfo_responsedata.responsecode = CMDIF_ACK_OK;
    }
    return S_SUCCESS;
}

//------------------------------------------------------------------------------
// Validate firmware content from flash
// Return   u8  status
// Engineer: Quyen Leba
//------------------------------------------------------------------------------
u8 cmdif_func_bootloader_validate()
{
    u8  status;

    cmdif_datainfo_responsedata.responsecode = CMDIF_ACK_FAILED;
    status = bootloader_validate();
    if (status == S_SUCCESS)
    {
        cmdif_datainfo_responsedata.responsecode = CMDIF_ACK_OK;
    }
    else if(status == S_LOADFILE)
    {
        cmdif_internalresponse_ack(CMDIF_CMD_BOOTLOADER_VALIDATE,
                                   CMDIF_ACK_OK,NULL,0);
        // Bootloader file validated and ACK sent, next function will disconnect
        // bluetooth and load new firmware.
        status = bootloader_updatebyfile();
    }
        
    return S_SUCCESS;
}

//------------------------------------------------------------------------------
// Get Properties
// Input:   u16 target (0:MainBoard, 1:VehicleBoard, 2:AppBoard, other:N/A)
// Return   u8  status
// Engineer: Quyen Leba
//------------------------------------------------------------------------------
u8 cmdif_func_get_properties(u16 target)
{
    u8  buffer[512];
    u8  *bptr;
    u32 bufferlength;
    u8  status;

    status = S_FAIL;
    bufferlength = 0;
    cmdif_datainfo_responsedata.responsecode = CMDIF_ACK_FAILED;
    switch(target)
    {
    case 0:     //MainBoard
        status = properties_mb_getinfo(buffer,&bufferlength);
        break;
    case 1:     //VehicleBoard
        status = properties_vb_getinfo(buffer,&bufferlength);
        break;
    case 2:     //AppBoard
        status = properties_ab_getinfo(buffer,&bufferlength);
        break;
    default:
        status = S_NOTSUPPORT;
        cmdif_datainfo_responsedata.responsecode = CMDIF_ACK_UNSUPPORTED;
        break;
    }

    if (status == S_SUCCESS)
    {
        bptr = __malloc(sizeof(buffer));
        if (!bptr)
        {
            return S_MALLOC;
        }

        memcpy((char*)bptr,(char*)buffer,bufferlength);

        cmdif_datainfo_responsedata.dataptr = bptr;
        cmdif_datainfo_responsedata.datalength = bufferlength;
        cmdif_datainfo_responsedata.responsecode = CMDIF_ACK_OK;
    }
    return status;
}

//------------------------------------------------------------------------------
// Hardware function
// Inputs:  u16 opcode
//          u8 *opcodedata
// Outputs: u8  *returndata
//          u32 *returndatalength
// Return   u8  status
// Engineer: Quyen Leba
//------------------------------------------------------------------------------
u8 cmdif_func_hardware(u16 opcode, u8 *opcodedata)
{
    HardwareAccess hwopcode;
    u8  returndata[256];
    u32 returndatalength;
    u8  status;

    returndatalength = 0;
    hwopcode = (HardwareAccess)opcode;

    if (hardware_access_checksupport(hwopcode) != S_SUCCESS)
    {
        cmdif_internalresponse_ack(CMDIF_CMD_HARDWARE,
                                   CMDIF_ACK_NOT_SUPPORTED,NULL,0);
        
    }
    else if (hwopcode == HardwareAccess_Unpair ||
             hwopcode == HardwareAccess_SetFriendlyName)
    {
        cmdif_internalresponse_ack(CMDIF_CMD_HARDWARE,
                                   CMDIF_ACK_OK,NULL,0);
        
        status = hardware_access(hwopcode,opcodedata,
                                 returndata,&returndatalength);
        //assume successful because reset required
    }
    else if (hwopcode == HardwareAccess_SetCommLinkSpeed)
    {
        //new speed must be supported to switch
        switch((CommLinkBaudrate)(*(u16*)opcodedata))
        {
        case COMMLINK_115200_BAUD:
        case COMMLINK_230400_BAUD:
        case COMMLINK_460800_BAUD:
        case COMMLINK_921600_BAUD:
            cmdif_internalresponse_ack(CMDIF_CMD_HARDWARE,
                                       CMDIF_ACK_OK,NULL,0);
            delays(2,'m');  //small delay to prevent ambiguous start bit
            hardware_access(hwopcode,opcodedata,
                                         returndata,&returndatalength);
            //Require HardwareAccess_CommLinkSpeedSyncReq & HardwareAccess_CommLinkSpeedSyncCfm
            //to complete this request. Otherwise, housekeeper will revert to default speed
            housekeeping_additem(HouseKeepingType_CommLinkSpeedSync,2500);
            break;
        default:
            cmdif_internalresponse_ack(CMDIF_CMD_HARDWARE,
                                       CMDIF_ACK_UNSUPPORTED,NULL,0);
            break;
        }
    }
    else
    {
        if(hwopcode == HardwareAccess_SetExt5VConfig || hwopcode == HardwareAccess_SetGPIO1Config)
        {
            // Save the new values to settings
            if(hwopcode == HardwareAccess_SetExt5VConfig)
            {
                status = settings_setsettings_byopcode(SETTINGS_OPCODE_ANALOGPORT_EXT5V, 0,
                                  opcodedata, sizeof(settingsdataloggeneral.analog_port));
            }
            else
            {
                status = settings_setsettings_byopcode(SETTINGS_OPCODE_ANALOGPORT_GPIO1, 0,
                                  opcodedata, sizeof(settingsdataloggeneral.analog_port));
            }
        }
        else if(hwopcode == HardwareAccess_GetExt5VConfig || hwopcode == HardwareAccess_GetGPIO1Config)
        {
            // Read the current values from settings
            if(hwopcode == HardwareAccess_SetExt5VConfig)
            {
                status = settings_getsettings_byopcode(SETTINGS_OPCODE_ANALOGPORT_EXT5V, 0,
                                  returndata, &returndatalength);
            }
            else
            {
                status = settings_getsettings_byopcode(SETTINGS_OPCODE_ANALOGPORT_GPIO1, 0,
                                 returndata, &returndatalength);
            }
            
            // No Hardware access needed, just return settings value            
            cmdif_internalresponse_ack(CMDIF_CMD_HARDWARE,CMDIF_ACK_OK,
                                       returndata,returndatalength); 
            
            return S_SUCCESS;            
        }
        
        status = hardware_access(hwopcode,opcodedata,
                                 returndata,&returndatalength);
        if (status == S_SUCCESS && returndatalength <= sizeof(returndata))
        {
            if (returndatalength > 0)
            {
                cmdif_internalresponse_ack(CMDIF_CMD_HARDWARE,CMDIF_ACK_OK,
                                           returndata,returndatalength);
                
            }
            else
            {
                cmdif_internalresponse_ack(CMDIF_CMD_HARDWARE,
                                           CMDIF_ACK_OK,NULL,0);
            }
        }
        else if(status == S_NOTSUPPORT)
        {
            cmdif_internalresponse_ack(CMDIF_CMD_HARDWARE,
                                       CMDIF_ACK_NOT_SUPPORTED,NULL,0);
            
        }
        else
        {
            cmdif_internalresponse_ack(CMDIF_CMD_HARDWARE,
                                       CMDIF_ACK_FAILED,NULL,0);
            
        }
    }
    
    return S_SUCCESS;
}

//------------------------------------------------------------------------------
// Check version between AppBoard, MainBoard & VehicleBoard to ensure valid
// communication & functions.
// Inputs:  u8 check_type
//          u32 app_version
// Return   u8  status
// Engineer: Quyen Leba
//------------------------------------------------------------------------------
u8 cmdif_func_version_health_check(u8 check_type, u32 app_version)
{
    u8  status;

    cmdif_datainfo_responsedata.responsecode = CMDIF_ACK_FAILED;
    status = S_FAIL;

    //CMDIF_ACK_UNCOMPATIBLE: not used yet
    if (check_type != 1)
    {
        cmdif_datainfo_responsedata.responsecode = CMDIF_ACK_UNSUPPORTED;
        status = S_NOTSUPPORT;
    }
    else if (app_version < MIN_APP_VERSION_REQUIRED)
    {
        cmdif_datainfo_responsedata.responsecode = CMDIF_ACK_APP_OUTOFDATE;
        status = S_EXPIRED;
    }
    else
    {
        //check if VB meets condition for valid communication
        status = properties_vb_validate_version_condition();
        if (status == S_SUCCESS)
        {
            cmdif_datainfo_responsedata.responsecode = CMDIF_ACK_OK;
        }
        else if (status == S_OSUNMATCH)
        {
            cmdif_datainfo_responsedata.responsecode = CMDIF_ACK_VB_OUTOFDATE;
        }
        else if (status == S_EXPIRED)
        {
            cmdif_datainfo_responsedata.responsecode = CMDIF_ACK_VB_OUTOFDATE;
        }
        else
        {
            cmdif_datainfo_responsedata.responsecode = CMDIF_ACK_FAILED;
        }
    }

    return status;
}

//------------------------------------------------------------------------------
// TODOQ: IMPORTANT: remove this for production firmware
//------------------------------------------------------------------------------
u8 cmdif_func_debug(u32 task, u8 *data, u32 datalength)
{
    
    const u32 testpatterns[10] =
    {
        0x6E22A8C4, 
        0x0072942C, 
        0x938A136E, 
        0x62117D28, 
        0x28E3ED05, 
        0xD3E3F6A6, 
        0xD36BCAE8, 
        0xFA76EDB7, 
        0x31B471CD, 
        0x21C6F4DC
    };
    u32 tmpu32;
    u8  *bptr;
    u32 *wptr;
    u8  status;

    status = S_FAIL;
    cmdif_datainfo_responsedata.responsecode = CMDIF_ACK_FAILED;
    if (task == 0x7EC328B1)        //demo mode
    {
        if (datalength == 1)
        {
            if (data[0] == 0)   //disable demo mode
            {
                status = settings_unregister_demo_mode();
                obd2_close();
            }
            else                //enable demo mode
            {
                if (!SETTINGS_IsDemoMode())
                {
                    //only allow if device unmarried
                    if (!SETTINGS_IsMarried())
                    {
                        status = settings_register_demo_mode();
                        obd2_close();
                    }
                    else
                    {
                        status = S_MARRIED;
                    }
                }
                else
                {
                    //already in demo mode
                    status = S_SUCCESS;
                }
            }
            if (status == S_SUCCESS)
            {
                cmdif_datainfo_responsedata.responsecode = CMDIF_ACK_OK;
            }
        }
    }
#if SUPPORT_CMDIF_FUNC_DEBUG
    else if (task == 0xE318A312)    //encryption control
    {
        if (datalength > 0)
        {
            if (data[0] == 0)   //disable encryption
            {
                commlink_info.use_encryption = FALSE;
            }
            else
            {
                commlink_info.use_encryption = TRUE;
            }
            cmdif_datainfo_responsedata.responsecode = CMDIF_ACK_OK;
        }
    }
#endif
    else if (task == 0xE361C132)    //buffer test 1: send buffer filled w/ test data
    {
        //data: [2:length multiple of 8][8:test data]
        if (datalength >= 10)
        {
            tmpu32 = (*(u16*)data);
            if (tmpu32 == 0 || tmpu32 > 2048 || tmpu32)
            {
                cmdif_datainfo_responsedata.responsecode = CMDIF_ACK_FAILED;
            }
            else
            {
                bptr = __malloc(sizeof(tmpu32+8));
                if (!bptr)
                {
                    return S_MALLOC;
                }
                cmdif_datainfo_responsedata.dataptr = bptr;
                cmdif_datainfo_responsedata.datalength = tmpu32;
                for(tmpu32=0;tmpu32<cmdif_datainfo_responsedata.datalength;tmpu32+=8)
                {
                    memcpy((char*)bptr,(char*)&data[2],8);
                    bptr += 8;
                }
                cmdif_datainfo_responsedata.responsecode = CMDIF_ACK_OK;
            }
        }
    }
    else if (task == 0xE38F43B9)    //buffer test 2: echo buffer
    {
        if (datalength > 0 && datalength <= 2048)
        {
            bptr = __malloc(sizeof(datalength));
            if (!bptr)
            {
                return S_MALLOC;
            }
            memcpy((char*)bptr,(char*)data,datalength);

            cmdif_datainfo_responsedata.dataptr = bptr;
            cmdif_datainfo_responsedata.datalength = datalength;
            cmdif_datainfo_responsedata.responsecode = CMDIF_ACK_OK;
        }
        else if (datalength == 0)
        {
            cmdif_datainfo_responsedata.responsecode = CMDIF_ACK_OK;
        }
        else
        {
            cmdif_datainfo_responsedata.responsecode = CMDIF_ACK_FAILED;
        }
    }
    else if (task == 0xE19C52A0)    //buffer test 3: listening mode test
    {
        //data: [4: delay in count of 10us]
        u32 i;
        u32 testpatternindex;
        u32 delaytime;

        tmpu32 = 0;
        if (datalength >= 4)
        {
            delaytime = *(u32*)data;
        }

        bptr = __malloc(2048);
        if (!bptr)
        {
            return S_MALLOC;
        }
        cmdif_datainfo_responsedata.dataptr = bptr;
//        srand((u32)XXX);  //get a timer count here

        testpatternindex = 0;
        indicator_set_normaloperation(TRUE);
        indicator_set(Indicator_Datalog_Active);    //-Q-
        while(1)
        {
            //generate buffer data
            cmdif_datainfo_responsedata.datalength = (u32)(((double)rand()/((double)RAND_MAX + (double)1))*2048);
            cmdif_datainfo_responsedata.datalength = (cmdif_datainfo_responsedata.datalength+7)/8*8;
            if (cmdif_datainfo_responsedata.datalength > 2048)
            {
                cmdif_datainfo_responsedata.datalength = 2048;
            }
            else if (cmdif_datainfo_responsedata.datalength == 0)
            {
                cmdif_datainfo_responsedata.datalength = 4;
            }

            wptr = (u32*)cmdif_datainfo_responsedata.dataptr;
            for(i=0;i<cmdif_datainfo_responsedata.datalength/4;i++)
            {
                *wptr++ = testpatterns[testpatternindex];
            }

            //send it
            cmdif_internalresponse_ack(CMDIF_CMD_DEBUG,
                                       CMDIF_ACK_OK,
                                       cmdif_datainfo_responsedata.dataptr,
                                       cmdif_datainfo_responsedata.datalength);
            tmpu32 = delaytime;
            while(tmpu32--)
            {
                delays_counter(DELAYS_COUNTER_10MS/1000);     //~10us each
            }
            if (cmdif_receive_command_internal(cmdif_datainfo_responsedata.dataptr,
                                               &cmdif_datainfo_responsedata.datalength,2048) == S_SUCCESS)
            {
                break;
            }

            testpatternindex++;
            if (testpatternindex >= sizeof(testpatterns)/4)
            {
                testpatternindex = 0;
            }
        }
        indicator_set_normaloperation(TRUE);
        indicator_clear();
        indicator_link_status();

        __free(bptr);
        cmdif_datainfo_responsedata.dataptr = NULL;
        cmdif_datainfo_responsedata.datalength = 0;
        cmdif_datainfo_responsedata.responsecode = CMDIF_ACK_DONE;
    }
#if SUPPORT_CMDIF_FUNC_DEBUG
    else if (task == 0x7214D28F)
    {
        //reset settings
        settings_setproductiondefault();
        cmdif_datainfo_responsedata.responsecode = CMDIF_ACK_OK;
    }
    else if (task == 0x62C26A1E)
    {
        if (datalength == sizeof(SETTINGS_TUNE(prev_vin)))
        {
            memcpy((char*)SETTINGS_TUNE(prev_vin),data,sizeof(SETTINGS_TUNE(prev_vin)));
            cmdif_datainfo_responsedata.responsecode = CMDIF_ACK_OK;
        }
    }
    else if (task == 0x2B5A24C1)
    {
        if (datalength == 1)
        {
            SETTINGS_TUNE(married_count) = data[0];
            cmdif_datainfo_responsedata.responsecode = CMDIF_ACK_OK;
        }
    }
#endif
    else
    {
        cmdif_datainfo_responsedata.responsecode = CMDIF_ACK_UNSUPPORTED;
    }
    return S_SUCCESS;
}

//------------------------------------------------------------------------------
// Production test function
// Input:   u8  test_task
// Return   u8  status
// Engineer: Quyen Leba
//------------------------------------------------------------------------------
u8 cmdif_func_production_test(u8 test_task)
{
    u8  buffer[256];
    u16 bufferlength;
    u8  *bptr;
    u8  status;
    
    status = S_SUCCESS;
    buffer[0] = NULL;
    bufferlength = 0;
    
    cmdif_datainfo_responsedata.responsecode = CMDIF_ACK_OK;
    switch(test_task)
    {
    //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    case PRODUCTION_TEST_TASK_COMMLINK:
        buffer[0] = 0; bufferlength = 1;    //version 0
        break;
    //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    case PRODUCTION_TEST_TASK_REV:
        status = cmdif_func_production_test_rev(buffer);
        bufferlength = strlen((char*)buffer);
        break;
    //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    case PRODUCTION_TEST_TASK_VBBOOTEN:
        status = cmdif_func_production_test_vbbooten(buffer);
        bufferlength = strlen((char*)buffer);
        break;
    //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    case PRODUCTION_TEST_TASK_FORMAT:
        status = cmdif_func_production_test_format(buffer);
        bufferlength = strlen((char*)buffer);
        break;
    //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    case PRODUCTION_TEST_TASK_VEHICLE_COMM:
        status = cmdif_func_production_test_vehicle_comm(buffer);
        bufferlength = strlen((char*)buffer);
        break;
    //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    case PRODUCTION_TEST_TASK_ADC:
        status = cmdif_func_production_test_adc(buffer);
        bufferlength = strlen((char*)buffer);
        break;
    //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    case PRODUCTION_TEST_TASK_LED:
        led_cycle_test();
        break;
    //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
#if USE_CC3000_WIFI
    case PRODUCTION_TEST_TASK_WIFI:
        status = cmdif_func_production_test_wifi(buffer);
        bufferlength = strlen((char*)buffer);
        break;
#endif        
    //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    case PRODUCTION_TEST_TASK_USB:
        status = cmdif_func_production_test_usb(buffer);
        bufferlength = strlen((char*)buffer);
        break;
        //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    case PRODUCTION_TEST_TASK_COMPLETED:
        status = cmdif_func_production_test_completion(buffer);
        bufferlength = strlen((char*)buffer);
        break;
    //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    default:
        cmdif_datainfo_responsedata.responsecode = CMDIF_ACK_UNSUPPORTED;
        status = S_NOTSUPPORT;
        break;
    }
    
    if(cmdif_datainfo_responsedata.responsecode == CMDIF_ACK_OK)
        settings_set_productiontest(test_task, TRUE);
    else
        settings_set_productiontest(test_task, FALSE);

    if (bufferlength)
    {
        bptr = __malloc(sizeof(buffer));
        if (!bptr)
        {
            return S_MALLOC;
        }

        memcpy((char*)bptr,(char*)buffer,bufferlength);

        cmdif_datainfo_responsedata.dataptr = bptr;
        cmdif_datainfo_responsedata.datalength = bufferlength;
    }
    return status;
}

/**
 *  @brief OBD2 garbage collector
 *  
 *  @return Status
 */
u8 cmdif_func_obd2_garbagecollector()
{
    u8 status;

    status = obd2_garbagecollector();
    if (status == S_SUCCESS)
    {
        cmdif_datainfo_responsedata.responsecode = CMDIF_ACK_OK;
    }
    else
    {
        cmdif_datainfo_responsedata.responsecode = CMDIF_ACK_FAILED;
    }
    return status;
}