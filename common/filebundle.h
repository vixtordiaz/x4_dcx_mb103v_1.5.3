/**
  **************** (C) COPYRIGHT 2011 SCT Performance, LLC *********************
  * File Name          : filebundle.h
  * Device / System    : SCT products using the "CommonCode" platform
  * Author             : Quyen Leba
  *
  * Version            : 1 
  * Date               : 05/02/2011
  * Description        : 
  *                    : 
  *
  *
  * History            : 05/02/2011 M. Davis
  *                    :   Add SCT Copyright header,
  *
  ******************************************************************************
  */

#ifndef __FILEBUNDLE_H
#define __FILEBUNDLE_H

#include <arch/gentype.h>

u8 filebundle_openfirstfile_by_fptr(F_FILE *fptr, u32 offset, bool dofilevalidation,
                                    u8 *child_filename);
u8 filebundle_opennextfile_by_fptr(u8 *child_filename);
u8 filebundle_openfirstfile(const u8 *bundle_filename, u32 bundle_fileoffset, bool dofilevalidation,
                            u8 *child_filename);
u8 filebundle_opennextfile(u8 *child_filename);

u8 filebundle_openfile_by_fptr(F_FILE *fptr, u32 offset, const u8 *child_filename, bool dofilevalidation);
u8 filebundle_openfile(const u8 *bundle_filename, u32 offset, const u8 *filename, bool dovalidatefile);
void filebundle_closefile();
u8 filebundle_validate(const u8 *bundle_filename, u32 offset);
u8 filebundle_seekfile(u32 position);
u8 filebundle_readfile(u8 *data, u32 length, u32 *actual_readcount);
bool filebundle_eof(bool *isOK);
u32 filebundle_filesize(bool *isOK);

u8 filebundle_extractcurrentfile(const u8 *out_filename);
u8 filebundle_extractfile_by_fptr(F_FILE *fptr, u32 bundle_fileoffset, const u8 *child_filename, const u8 *out_filename);
u8 filebundle_extractfile(const u8 *bundle_filename, u32 bundle_fileoffset, const u8 *child_filename, const u8 *out_filename);

#endif     //__FILEBUNDLE_H
