/**
  **************** (C) COPYRIGHT 2011 SCT Performance, LLC *********************
  * File Name          : filestock_option.h
  * Device / System    : SCT products using the "CommonCode" platform
  * Author             : Quyen Leba
  *
  * Version            : 1 
  * Date               : 05/02/2011
  * Description        : 
  *                    : 
  *
  *
  * History            : 05/02/2011 M. Davis
  *                    :   Add SCT Copyright header,
  *
  ******************************************************************************
  */

#ifndef __FILESTOCK_OPTION_H
#define __FILESTOCK_OPTION_H

#include <arch/gentype.h>

typedef union
{
    struct
    {
        u32 opcode_c_mathindex_10H  : 1;
        u32 opcode_c_mathindex_11H  : 1;
        u32 opcode_c_mathindex_12H  : 1;
        u32 opcode_c_mathindex_13H  : 1;
        u32 opcode_c_mathindex_14H  : 1;
        u32 opcode_c_mathindex_15H  : 1;
        u32 opcode_c_mathindex_16H  : 1;
        u32 opcode_c_mathindex_17H  : 1;
        u32 reserved                : 23;
    }f;
    u32 i;
}OptionFlags;


u8 filestock_option_download_init();
u8 filestock_option_download_deinit();
u8 filestock_option_delete();

u8 filestock_option_init();
void filestock_option_deinit();
u8 filestock_option_load_info();
u8 filestock_option_save_info();
u8 filestock_option_keep_new_options_flags(OptionFlags option_flags);
u8 filestock_option_keep_options_flags(OptionFlags option_flags);
u8 filestock_option_get_options_flags(OptionFlags *option_flags);
u8 filestock_option_keep_veh_type(u16 veh_type);
u8 filestock_option_test_veh_type(u16 veh_type);
u8 filestock_option_keep_axle_ratio(u32 ratio);
u8 filestock_option_get_axle_ratio(u32 *ratio);
u8 filestock_option_keep_tire_size(u32 front, u32 rear);
u8 filestock_option_get_tire_size(u32 *front, u32 *rear);
u8 filestock_option_keep_tire_pressure(u16 front, u16 rear);
u8 filestock_option_get_tire_pressure(u16 *front, u16 *rear);
u8 filestock_option_validate(u32 *crc32e);


#endif     //__FILESTOCK_OPTION_H
