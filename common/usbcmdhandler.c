/**
  **************** (C) COPYRIGHT 2011 SCT Performance, LLC *********************
  * File Name          : usbcmdhandler.c
  * Device / System    : SCT products using the "CommonCode" platform
  * Author             : Quyen Leba
  *
  * Version            : 1 
  * Date               : 05/02/2011
  * Description        : 
  *                    : 
  *
  *
  * History            : 05/02/2011 M. Davis
  *                    :   Add SCT Copyright header,
  *
  ******************************************************************************
  */

//------------------------------------------------------------------------------
// Log error point: 0xB000 -> 0xBFFF
//------------------------------------------------------------------------------

#include <string.h>
#include <ctype.h>
#include <stdarg.h>
#include <device_config.h>
#include <board/genplatform.h>
#include <board/properties_vb.h>
#include <board/bootloader.h>
#include <common/statuscode.h>
#include <common/crc32.h>
#include <common/crypto_blowfish.h>
#include <common/crypto_messageblock.h>
#include <common/bootsettings.h>
#include <common/settings.h>
#include <common/tea.h>
#include <common/log.h>
#include <common/usb_callback.h>
#include <common/deviceinfo.h>
#include <common/obd2tune_recoverystock.h>
#include <common/obd2tune.h>
#include "usbcmdhandler.h"

//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
extern u8  *usb_databuffer;                     //from usb_callback.c
extern u32 usb_databufferindex;
extern u32 usb_databufferlength;
extern u32 usb_databuffermaxlength;

extern void USB_CONSOLE_OUT(const char *data, uint32_t datalength);

#if SUPPORT_USBCMD_FILETRANSFER
#include <fs/genfs.h>
#include <common/file.h>
struct
{
    F_FILE  *fileptr;
    u32 file_position;
    u32 file_length;
    struct
    {
        u8  ready           : 1;    //1: USB connection is active (fully enumerated)
        //cmds from commlink and uch can access cmdif; whichever currently has cmdif control must block others
        //set halted to block uch from accessing cmdif
        u8  halted          : 1;    //1: halted
        u8  force_ui_lock   : 1;    //1: lock ui until this flag cleared
        u8  reserved        : 5;
    }control;
#define UI_LOCK_TIMETHRES       1000
    u16 ui_lock_timecount;      //in ms, expired if no USB comm in UI_LOCK_TIMETHRES
}usbcmdhandler_info =
{
    .fileptr = NULL,
    .control.ready = 0,
    .control.halted = 0,
    .control.force_ui_lock = 0,
    .ui_lock_timecount = 0,
};
#endif

//------------------------------------------------------------------------------
// Handle security seed setup command
// Engineer: Quyen Leba
//------------------------------------------------------------------------------
void uch_init()
{
    if (usb_databuffer != NULL)
    {
        __free(usb_databuffer);
        usb_databuffer = NULL;
    }
    usb_databuffermaxlength = USB_DATABUFFER_MAXSIZE;
    usb_databuffer = __malloc(usb_databuffermaxlength);
    if (usb_databuffer == NULL)
    {
        //there's critical error, show indicator
        usb_databuffermaxlength = 0;
    }
}

/**
 * @brief   Check if uch is halted
 *
 * @retval  bool TRUE: halted
 *
 * @author  Quyen Leba
 *
 */
bool uch_is_halted()
{
    return (bool)usbcmdhandler_info.control.halted;
}

/**
 * @brief   Halt uch feature, reject any uch commands. Use it to avoid conflict between different channels trying to access cmdif, fs, etc.
 *
 * @param   [in] yes (TRUE: halt it)
 *
 * @author  Quyen Leba
 *
 */
void uch_set_temporary_halt(bool yes)
{
    usbcmdhandler_info.control.halted = yes;
}

/**
 * @brief   Check USB connection status
 *
 * @retval  bool TRUE: valid connection
 *
 * @author  Quyen Leba
 *
 */
bool uch_is_usb_ready()
{
    return (bool)usbcmdhandler_info.control.ready;
}

/**
 * @brief   Set ready status of USB connection
 *
 * @param   [in] yes
 *
 * @author  Quyen Leba
 *
 */
void uch_set_usb_ready(bool yes)
{
    if (yes)
    {
        usbcmdhandler_info.control.ready = 1;
    }
    else
    {
        usbcmdhandler_info.control.ready = 0;
    }
}

/**
 * @brief   Use as printf to send messages through USB virtual COM port
 *
 * @param   [in] format
 *
 * @author  Quyen Leba
 *
 */
void uch_printf(const char* format, ...)
{
    u8  buffer[64];
    u8  bytecount;
    va_list args;

    if (uch_is_usb_ready())
    {
        va_start(args,format);
        bytecount = vsnprintf((char*)buffer,sizeof(buffer),format,args);
        va_end(args);
    }
    USB_CONSOLE_OUT((char*)buffer,bytecount);
}

/**
 * @brief   Reset whenever a USB command received. To keep UI lock alive.
 *
 * @author  Quyen Leba
 *
 */
void uch_ui_lock_time_reset()
{
    usbcmdhandler_info.ui_lock_timecount = 0;
}

/**
 * @brief   Lock UI
 *
 * @author  Quyen Leba
 *
 */
void uch_ui_lock_set()
{
    usbcmdhandler_info.ui_lock_timecount = 0;
    gpio_block_remote_ui(TRUE);
}

/**
 * @brief   Unlock UI
 *
 * @author  Quyen Leba
 *
 */
void uch_ui_lock_remove()
{
    gpio_block_remote_ui(FALSE);
}

/**
 * @brief   Force lock UI
 *
 * @author  Quyen Leba
 *
 */
void uch_ui_lock_set_force_lock()
{
    usbcmdhandler_info.control.force_ui_lock = 1;
    uch_ui_lock_set();
}

/**
 * @brief   Force unlock UI
 *
 * @author  Quyen Leba
 *
 */
void uch_ui_lock_clear_force_lock()
{
    usbcmdhandler_info.control.force_ui_lock = 0;
    //uch_ui_lock_remove();
}

/**
 * @brief   Check if lock timecount expired. Put this function in 1ms periodic process
 *
 * @author  Quyen Leba
 *
 */
void uch_ui_lock_status_check()
{
    if (usbcmdhandler_info.control.force_ui_lock)
    {
        //do nothing
    }
    else if (usbcmdhandler_info.ui_lock_timecount++ >= UI_LOCK_TIMETHRES)
    {
        uch_ui_lock_remove();
        usbcmdhandler_info.ui_lock_timecount = UI_LOCK_TIMETHRES;
    }
}

//------------------------------------------------------------------------------
// Handle security seed setup command
// Engineer: Quyen Leba
//------------------------------------------------------------------------------
void uch_security_seed()
{
    u16 market;
    u8  seed_block[SCTUSB_SECURITY_SEEDKEY_SIZE];
    u8  encrypted_seed[MAX_CRYPTO_MESSAGE_BLOCK_LENGTH];
    u32 encrypted_seed_length;
    
    SCTUSB_InvalidSeed();
    
    market = usb_callback_info.cbw.privatedata[0];
    memset((char*)seed_block,0,sizeof(seed_block));
    if (usb_callback_info.cbw.datalength != MAX_CRYPTO_MESSAGE_BLOCK_LENGTH)
    {
        log_push_error_point(0xB001);
        SCTUSB_SetCmdStatus(S_BADCONTENT);
    }
    else if (market == SUPPORT_DEVICE_MARKET)
    {
        u32 i;
        u32 *wptr;
        u32 tea_key[4];
        
        srand(get_systick_count());
        wptr = (u32*)seed_block;
        for(i=0;i<SCTUSB_SECURITY_SEEDKEY_SIZE/4;i++)
        {
            *wptr++ = rand();
        }

        settings_get_unlock_tea_key(SETTINGS_GetMarketType(),tea_key);
        memcpy(usb_callback_info.securitybuffer,
               seed_block,SCTUSB_SECURITY_SEEDKEY_SIZE);
        wptr = (u32*)usb_callback_info.securitybuffer;
        for(i=0;i<SCTUSB_SECURITY_SEEDKEY_SIZE/8;i++)
        {
            tea_encryption(wptr,tea_key);
            wptr+=2;
        }
        
        SCTUSB_SetCmdStatus(S_SUCCESS);
        SCTUSB_ValidSeed();
    }
    else
    {
        log_push_error_point(0xB002);
        SCTUSB_SetCmdStatus(S_ERROR);
    }
    
    crypto_messageblock_encrypt(seed_block,sizeof(seed_block),
                                encrypted_seed,&encrypted_seed_length,
                                CryptoMessageBlockLength_Exact64,CRYPTO_USE_INTERNAL_KEY);
    SCTUSB_SetStatusResidue(usb_callback_info.cbw.datalength - encrypted_seed_length);
    SCTUSB_SetFlowControl(CB_FC_DATA_IN_LAST);
    usb_send_data_to_host(encrypted_seed,encrypted_seed_length);
}

//------------------------------------------------------------------------------
// Handle security key validation command
// Engineer: Quyen Leba
//------------------------------------------------------------------------------
void uch_security_key()
{
    u8  decrypted_key[MAX_CRYPTO_MESSAGE_BLOCK_LENGTH];
    u32 decrypted_key_length;
    u16 status;
    
    SCTUSB_LockSecurity();
    if (SCTUSB_IsSeedValid())
    {
        if (usb_databufferlength == MAX_CRYPTO_MESSAGE_BLOCK_LENGTH)
        {
            status = crypto_messageblock_decrypt(usb_databuffer,usb_databufferlength,
                                                 decrypted_key,&decrypted_key_length,
                                                 FALSE,CRYPTO_USE_INTERNAL_KEY);
            if (status == S_SUCCESS &&
                decrypted_key_length == SCTUSB_SECURITY_SEEDKEY_SIZE)
            {
                if (memcmp((char*)decrypted_key,
                           (char*)usb_callback_info.securitybuffer,
                           SCTUSB_SECURITY_SEEDKEY_SIZE) == 0)
                {
                    SCTUSB_UnlockSecurity();
                    status = S_SUCCESS;
                }
                else
                {
                    log_push_error_point(0xB010);
                    status = S_UNMATCH;
                    
                }
            }
            else
            {
                log_push_error_point(0xB011);
                status = S_BADCONTENT;
            }
        }
        else
        {
            log_push_error_point(0xB012);
            status = S_BADCONTENT;
        }
    }
    else
    {
        log_push_error_point(0xB013);
        status = S_NOSEEDFOUND;
    }
    
    usb_set_csw(usb_callback_info.pdev,status,TRUE);
}

//------------------------------------------------------------------------------
// Handle bootloader commands
// Engineer: Quyen Leba
//------------------------------------------------------------------------------
void uch_bootloader()
{
    bool reboot;
    u8  status;
    
    reboot = FALSE;
    status = S_INPUT;
    
    switch((USBCmdBootloaderFlags)(usb_callback_info.cbw.flags))
    {
    case USBCmdBootloader_SetBoot:
        //data: [2: bootloadermode]
        if (usb_databufferlength == 2)
        {
            status = bootloader_setboot((BootloaderMode)(*(u16*)usb_databuffer));
            if (status == S_SUCCESS)
            {
                reboot = TRUE;
            }
        }
        break;
    case USBCmdBootloader_Init:
        //data: [64: firmware encrypted header]
        if (usb_databufferlength == 64)
        {
            u16 sector_size;
            status = bootloader_setup_session(usb_databuffer,&sector_size,FALSE);
        }
        break;
    case USBCmdBootloader_Do:
        //data: [n: firmware encrypted block]
        status = bootloader_write_flash(usb_databuffer,usb_databufferlength);
        break;
    case USBCmdBootloader_Validate:
        status = bootloader_validate();
        if (status == S_LOADFILE)
        {
            status = bootloader_updatebyfile();
        }
        break;
    case USBCmdBootloader_ClearEngSign:
        status = bootloader_clearengineeringsignature();
        break;
    case 0x3F:
        status = S_USER_EXIT;
        break;
    default:
        log_push_error_point(0xB016);
        status = S_NOTSUPPORT;
        break;
    }
    
    usb_set_csw(usb_callback_info.pdev,status,TRUE);
    if (reboot)
    {
        watchdog_set();
    }
}

//------------------------------------------------------------------------------
// Handle get settings commands
// Engineer: Quyen Leba
//------------------------------------------------------------------------------
void uch_settings_get()
{
    u8  settingsbuffer[MAX_CRYPTO_MESSAGE_BLOCK_LENGTH];
    u32 settingsbufferlength;
    u8  settingsencryptedblock[MAX_CRYPTO_MESSAGE_BLOCK_LENGTH];
    u32 settingsencryptedblocklength;
    u8  status;
    
    settingsbufferlength = 0;
    settingsencryptedblocklength = 0;

    memset((char*)settingsencryptedblock,0,sizeof(settingsencryptedblocklength));
    status = S_SUCCESS;
    switch((USBCmdSettingsFlags)usb_callback_info.cbw.flags)
    {
    case USBCmdSettings_BootMode:
        status = bootsettings_get(BootSettingsOpcode_BootMode,
                                  settingsbuffer,&settingsbufferlength);
        break;
    case USBCmdSettings_FailsafeBootVersion:
        status = bootsettings_get(BootSettingsOpcode_FailSafeBootVersion,
                                  settingsbuffer,&settingsbufferlength);
        break;
    case USBCmdSettings_MainBootVersion:
        status = bootsettings_get(BootSettingsOpcode_MainBootVersion,
                                  settingsbuffer,&settingsbufferlength);
        break;
    case USBCmdSettings_AppVersion:
        status = bootsettings_get(BootSettingsOpcode_AppVersion,
                                  settingsbuffer,&settingsbufferlength);
        break;
    case USBCmdSettings_AppSignature:
        status = bootsettings_get(BootSettingsOpcode_AppSignature,
                                  settingsbuffer,&settingsbufferlength);
        break;
    case USBCmdSettings_SerialNumber:
        status = settings_getsettings_byopcode(SETTINGS_OPCODE_SERIAL,0,
                                               settingsbuffer,
                                               &settingsbufferlength);
        break;
    case USBCmdSettings_FailsafeBootCRC32E:
        status = bootsettings_get(BootSettingsOpcode_FailSafeBootCRC32E,
                                  settingsbuffer,&settingsbufferlength);
        break;
    case USBCmdSettings_MainBootCRC32E:
        status = bootsettings_get(BootSettingsOpcode_MainBootCRC32E,
                                  settingsbuffer,&settingsbufferlength);
        break;
    case USBCmdSettings_AppCRC32E:
        status = bootsettings_get(BootSettingsOpcode_AppCRC32E,
                                  settingsbuffer,&settingsbufferlength);
        break;
    case USBCmdSettings_CustomPID:
        status = bootsettings_get(BootSettingsOpcode_CustomUSBPID,
                                  settingsbuffer,&settingsbufferlength);
        break;
#if SUPPORT_USBCMD_FULL_GETSETTINGS
    case USBCmdSettings_Flags:
        status = settings_getsettings_byopcode(SETTINGS_OPCODE_FLAGS,
                                               usb_callback_info.cbw.privatedata[0],
                                               settingsbuffer,
                                               &settingsbufferlength);
        break;
#endif
    //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    case USBCmdSettings_DevicePartNumberString:
        status = settings_getsettings_byopcode(SETTINGS_OPCODE_DEVICE_PARTNUMBER_STRING,0,
                                               settingsbuffer,
                                               &settingsbufferlength);
        break;
    case USBCmdSettings_DeviceType:
        status = settings_getsettings_byopcode(SETTINGS_OPCODE_DEVICE_TYPE,0,
                                               settingsbuffer,
                                               &settingsbufferlength);
        break;
    case USBCmdSettings_MarketType:
        status = settings_getsettings_byopcode(SETTINGS_OPCODE_MARKET_TYPE,0,
                                               settingsbuffer,
                                               &settingsbufferlength);
        break;
    case USBCmdSettings_ProductionTestStatus:
        status = settings_getsettings_byopcode(SETTINGS_OPCODE_PRODUCTIONTEST_STATUS,0,
                                               settingsbuffer,
                                               &settingsbufferlength);
        break;
#if SUPPORT_USBCMD_FULL_GETSETTINGS
    case USBCmdSettings_ProgrammerInfoP1:
        status = settings_getsettings_byopcode(SETTINGS_OPCODE_PROGRAMMER_INFO_P1,0,
                                               settingsbuffer,
                                               &settingsbufferlength);
        break;
    case USBCmdSettings_ProgrammerInfoP2:
        status = settings_getsettings_byopcode(SETTINGS_OPCODE_PROGRAMMER_INFO_P2,0,
                                               settingsbuffer,
                                               &settingsbufferlength);
        break;
    case USBCmdSettings_ProgrammerInfoP3:
        status = settings_getsettings_byopcode(SETTINGS_OPCODE_PROGRAMMER_INFO_P3,0,
                                               settingsbuffer,
                                               &settingsbufferlength);
        break;
    case USBCmdSettings_ProgrammerInfoP4:
        status = settings_getsettings_byopcode(SETTINGS_OPCODE_PROGRAMMER_INFO_P4,0,
                                               settingsbuffer,
                                               &settingsbufferlength);
        break;
    case USBCmdSettings_PreloadedTuneLookupCRC32E:
        status = settings_getsettings_byopcode(SETTINGS_OPCODE_PRELOADEDTUNE_LOOKUP_CRC32E,0,
                                               settingsbuffer,
                                               &settingsbufferlength);
        break;
    case USBCmdSettings_PreloadedTuneRestriction:
        status = settings_getsettings_byopcode(SETTINGS_OPCODE_PRELOADEDTUNE_RESTRICTION,0,
                                               settingsbuffer,
                                               &settingsbufferlength);
        break;
    case USBCmdSettings_DemoMode:
        status = settings_getsettings_byopcode(SETTINGS_OPCODE_DEMO_MODE,0,
                                               settingsbuffer,
                                               &settingsbufferlength);
        break;
    case USBCmdSettings_TuneRevision:
        status = settings_getsettings_byopcode(SETTINGS_OPCODE_TUNE_REVISION,0,
                                               settingsbuffer,
                                               &settingsbufferlength);
        break;
    case USBCmdSettings_DeviceVersionString:
        status = settings_getsettings_byopcode(SETTINGS_OPCODE_DEVICE_VERSION_STRING,0,
                                               settingsbuffer,
                                               &settingsbufferlength);
        break;
    //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    case USBCmdSettings_VIN:
        status = settings_getsettings_byopcode(SETTINGS_OPCODE_VIN,0,
                                               settingsbuffer,
                                               &settingsbufferlength);
        break;
    case USBCmdSettings_PreviousVIN:
        status = settings_getsettings_byopcode(SETTINGS_OPCODE_PREV_VIN,0,
                                               settingsbuffer,
                                               &settingsbufferlength);
        break;
    case USBCmdSettings_VID:
        status = settings_getsettings_byopcode(SETTINGS_OPCODE_VID,
                                               usb_callback_info.cbw.privatedata[0],
                                               settingsbuffer,
                                               &settingsbufferlength);
        break;
    case USBCmdSettings_EPATS:
        status = settings_getsettings_byopcode(SETTINGS_OPCODE_EPATS,0,
                                               settingsbuffer,
                                               &settingsbufferlength);
        break;
    case USBCmdSettings_VehicleCodes:
        status = settings_getsettings_byopcode(SETTINGS_OPCODE_VEHICLE_CODES,
                                               usb_callback_info.cbw.privatedata[0],
                                               settingsbuffer,
                                               &settingsbufferlength);
        break;
    case USBCmdSettings_VehicleSerialNumber:
        status = settings_getsettings_byopcode(SETTINGS_OPCODE_VEHICLE_SERIAL_NUMBER,0,
                                               settingsbuffer,
                                               &settingsbufferlength);
        break;
    case USBCmdSettings_StockFilesize:
        status = settings_getsettings_byopcode(SETTINGS_OPCODE_STOCKFILESIZE,0,
                                               settingsbuffer,
                                               &settingsbufferlength);
        break;
    case USBCmdSettings_TuneInfoTrack:
        status = settings_getsettings_byopcode(SETTINGS_OPCODE_TUNEINFOTRACK,
                                               usb_callback_info.cbw.privatedata[0],
                                               settingsbuffer,
                                               &settingsbufferlength);
        break;
    case USBCmdSettings_TuneHistoryInfo:
        status = settings_getsettings_byopcode(SETTINGS_OPCODE_TUNEHISTORYINFO,0,
                                               settingsbuffer,
                                               &settingsbufferlength);
        break;
    //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    case USBCmdSettings_DeviceCriticalFlags:
        status = settings_getsettings_byopcode(SETTINGS_OPCODE_DEVICE_CRITICAL_FLAGS,0,
                                               settingsbuffer,
                                               &settingsbufferlength);
        break;
    case USBCmdSettings_DeviceTuneFlags:
        status = settings_getsettings_byopcode(SETTINGS_OPCODE_DEVICE_TUNE_FLAGS,0,
                                               settingsbuffer,
                                               &settingsbufferlength);
        break;
    case USBCmdSettings_DeviceFleetFlags:
        status = settings_getsettings_byopcode(SETTINGS_OPCODE_DEVICE_FLEET_FLAGS,0,
                                               settingsbuffer,
                                               &settingsbufferlength);
        break;
    case USBCmdSettings_DeviceDatalogGeneralFlags:
        status = settings_getsettings_byopcode(SETTINGS_OPCODE_DEVICE_DATALOG_GENERAL_FLAGS,0,
                                               settingsbuffer,
                                               &settingsbufferlength);
        break;
    //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    case USBCmdSettings_ErrorPool:
        status = settings_getsettings_byopcode(SETTINGS_OPCODE_ERRORPOOL,
                                               usb_callback_info.cbw.privatedata[0],
                                               settingsbuffer,
                                               &settingsbufferlength);
        break;
    //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
#endif
    case USBCmdSettings_Properties:
        switch(usb_callback_info.cbw.privatedata[0])
        {
        case 0:
            status = properties_mb_getinfo(settingsbuffer,&settingsbufferlength);
            break;
        case 1:
            status = properties_vb_getinfo(settingsbuffer,&settingsbufferlength);
            break;
        case 2:
            status = properties_ab_getinfo(settingsbuffer,&settingsbufferlength);
            break;
        default:
            status = S_NOTSUPPORT;
            break;
        }
        break;
    default:
        log_push_error_point(0xB020);
        status = S_NOTSUPPORT;
        break;
    }
    
    if (status == S_SUCCESS)
    {
        if(settingsbufferlength == 0 || settingsbufferlength > 56)
        {
            log_push_error_point(0xB021);
            status = S_ERROR;
        }
        else
        {
            status = crypto_messageblock_encrypt(settingsbuffer,settingsbufferlength,
                                                 settingsencryptedblock,
                                                 &settingsencryptedblocklength,
                                                 CryptoMessageBlockLength_Exact64,CRYPTO_USE_INTERNAL_KEY);
            if (status == S_SUCCESS &&
                (settingsencryptedblocklength == 0 || 
                 settingsencryptedblocklength > MAX_CRYPTO_MESSAGE_BLOCK_LENGTH))
            {
                log_push_error_point(0xB022);
                status = S_ERROR;
            }
        }
    }
    
    if (status != S_SUCCESS)
    {
        memset((char*)settingsencryptedblock,0,sizeof(settingsencryptedblock));
        usb_set_csw(usb_callback_info.pdev,status,TRUE);
        return;
    }
    SCTUSB_SetCmdStatus(status);
    
    SCTUSB_SetFlowControl(CB_FC_DATA_IN_LAST);
    usb_send_data_to_host(settingsencryptedblock,sizeof(settingsencryptedblock));
}

//------------------------------------------------------------------------------
// Handle set settings commands
// Engineer: Quyen Leba
//------------------------------------------------------------------------------
void uch_settings_set()
{
    u8  settingsbuffer[64];
    u32 settingsbufferlength;
    u8  *settingsencryptedblock;
    u32 settingsencryptedblocklength;
    u8  status;
    
    settingsencryptedblock = usb_databuffer;
    settingsencryptedblocklength = usb_databufferlength;
    status = S_SUCCESS;
    
    status = crypto_messageblock_decrypt(settingsencryptedblock,
                                         settingsencryptedblocklength,
                                         settingsbuffer,&settingsbufferlength,
                                         FALSE,CRYPTO_USE_INTERNAL_KEY);
    if (status != S_SUCCESS)
    {
        log_push_error_point(0xB040);
        goto uch_settings_set_done;
    }

    switch((USBCmdSettingsFlags)usb_callback_info.cbw.flags)
    {
    case USBCmdSettings_BootMode:
        status = bootsettings_set(BootSettingsOpcode_BootMode,
                                  settingsbuffer,settingsbufferlength);
        break;
    case USBCmdSettings_FailsafeBootVersion:
        status = bootsettings_set(BootSettingsOpcode_FailSafeBootVersion,
                                  settingsbuffer,settingsbufferlength);
        break;
    case USBCmdSettings_MainBootVersion:
        status = bootsettings_set(BootSettingsOpcode_MainBootVersion,
                                  settingsbuffer,settingsbufferlength);
        break;
    case USBCmdSettings_AppVersion:
        status = bootsettings_set(BootSettingsOpcode_AppVersion,
                                  settingsbuffer,settingsbufferlength);
        break;
    case USBCmdSettings_AppSignature:
        status = bootsettings_set(BootSettingsOpcode_AppSignature,
                                  settingsbuffer,settingsbufferlength);
        break;
#if SUPPORT_USBCMD_FULL_SETSETTINGS
    case USBCmdSettings_SerialNumber:
        status = settings_setsettings_byopcode(SETTINGS_OPCODE_SERIAL,0,
                                               settingsbuffer,
                                               settingsbufferlength);
        break;
#endif  //SUPPORT_USBCMD_FULL_SETSETTINGS
    case USBCmdSettings_FailsafeBootCRC32E:
        status = bootsettings_set(BootSettingsOpcode_FailSafeBootCRC32E,
                                  settingsbuffer,settingsbufferlength);
        break;
    case USBCmdSettings_MainBootCRC32E:
        status = bootsettings_set(BootSettingsOpcode_MainBootCRC32E,
                                  settingsbuffer,settingsbufferlength);
        break;
    case USBCmdSettings_AppCRC32E:
        status = bootsettings_set(BootSettingsOpcode_AppCRC32E,
                                  settingsbuffer,settingsbufferlength);
        break;
    case USBCmdSettings_CustomPID:
        status = bootsettings_set(BootSettingsOpcode_CustomUSBPID,
                                  settingsbuffer,settingsbufferlength);
        break;
#if SUPPORT_USBCMD_FULL_SETSETTINGS
    case USBCmdSettings_Flags:
        status = settings_setsettings_byopcode(SETTINGS_OPCODE_FLAGS,
                                               usb_callback_info.cbw.privatedata[0],
                                               settingsbuffer,
                                               settingsbufferlength);
        break;
    case USBCmdSettings_DevicePartNumberString:
        status = settings_setsettings_byopcode(SETTINGS_OPCODE_DEVICE_PARTNUMBER_STRING,0,
                                               settingsbuffer,
                                               settingsbufferlength);
        break;
    case USBCmdSettings_DeviceType:
        status = settings_setsettings_byopcode(SETTINGS_OPCODE_DEVICE_TYPE,0,
                                               settingsbuffer,
                                               settingsbufferlength);
        break;
    case USBCmdSettings_MarketType:
        status = settings_setsettings_byopcode(SETTINGS_OPCODE_MARKET_TYPE,0,
                                               settingsbuffer,
                                               settingsbufferlength);
        break;
    //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    case USBCmdSettings_ProgrammerInfoP1:
        status = settings_setsettings_byopcode(SETTINGS_OPCODE_PROGRAMMER_INFO_P1,0,
                                               settingsbuffer,
                                               settingsbufferlength);
        break;
    case USBCmdSettings_ProgrammerInfoP2:
        status = settings_setsettings_byopcode(SETTINGS_OPCODE_PROGRAMMER_INFO_P2,0,
                                               settingsbuffer,
                                               settingsbufferlength);
        break;
    case USBCmdSettings_ProgrammerInfoP3:
        status = settings_setsettings_byopcode(SETTINGS_OPCODE_PROGRAMMER_INFO_P3,0,
                                               settingsbuffer,
                                               settingsbufferlength);
        break;
    case USBCmdSettings_ProgrammerInfoP4:
        status = settings_setsettings_byopcode(SETTINGS_OPCODE_PROGRAMMER_INFO_P4,0,
                                               settingsbuffer,
                                               settingsbufferlength);
        break;
    case USBCmdSettings_PreloadedTuneLookupCRC32E:
        status = settings_setsettings_byopcode(SETTINGS_OPCODE_PRELOADEDTUNE_LOOKUP_CRC32E,0,
                                               settingsbuffer,
                                               settingsbufferlength);
        break;
    case USBCmdSettings_PreloadedTuneRestriction:
        status = settings_setsettings_byopcode(SETTINGS_OPCODE_PRELOADEDTUNE_RESTRICTION,0,
                                               settingsbuffer,
                                               settingsbufferlength);
        break;
    case USBCmdSettings_DemoMode:
        status = settings_setsettings_byopcode(SETTINGS_OPCODE_DEMO_MODE,0,
                                               settingsbuffer,
                                               settingsbufferlength);
        break;
    //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    case USBCmdSettings_VIN:
        status = settings_setsettings_byopcode(SETTINGS_OPCODE_VIN,0,
                                               settingsbuffer,
                                               settingsbufferlength);
        break;
    case USBCmdSettings_PreviousVIN:
        status = settings_setsettings_byopcode(SETTINGS_OPCODE_PREV_VIN,0,
                                               settingsbuffer,
                                               settingsbufferlength);
        break;
    case USBCmdSettings_VID:
        status = settings_setsettings_byopcode(SETTINGS_OPCODE_VID,
                                               usb_callback_info.cbw.privatedata[0],
                                               settingsbuffer,
                                               settingsbufferlength);
        break;
    case USBCmdSettings_EPATS:
        status = settings_setsettings_byopcode(SETTINGS_OPCODE_EPATS,0,
                                               settingsbuffer,
                                               settingsbufferlength);
        break;
    case USBCmdSettings_VehicleCodes:
        status = settings_setsettings_byopcode(SETTINGS_OPCODE_VEHICLE_CODES,
                                               usb_callback_info.cbw.privatedata[0],
                                               settingsbuffer,
                                               settingsbufferlength);
        break;
    case USBCmdSettings_VehicleSerialNumber:
        status = settings_setsettings_byopcode(SETTINGS_OPCODE_VEHICLE_SERIAL_NUMBER,0,
                                               settingsbuffer,
                                               settingsbufferlength);
        break;
    case USBCmdSettings_StockFilesize:
        status = settings_setsettings_byopcode(SETTINGS_OPCODE_STOCKFILESIZE,0,
                                               settingsbuffer,
                                               settingsbufferlength);
        break;
    case USBCmdSettings_TuneInfoTrack:
        status = settings_setsettings_byopcode(SETTINGS_OPCODE_TUNEINFOTRACK,
                                               usb_callback_info.cbw.privatedata[0],
                                               settingsbuffer,
                                               settingsbufferlength);
        break;
    case USBCmdSettings_TuneHistoryInfo:
        status = settings_setsettings_byopcode(SETTINGS_OPCODE_TUNEHISTORYINFO,0,
                                               settingsbuffer,
                                               settingsbufferlength);
        break;
    //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    case USBCmdSettings_ResetFactoryDefault:
        status = settings_setsettings_byopcode(SETTINGS_OPCODE_INIT_SETTINGS,0,
                                               settingsbuffer,
                                               settingsbufferlength);
        if (status == S_SUCCESS)
        {
            status = init_factorydefault();
        }
        break;
    case USBCmdSettings_ReInit:
        status = settings_setsettings_byopcode(SETTINGS_OPCODE_RESET_SETTINGS,0,
                                               settingsbuffer,
                                               settingsbufferlength);
        break;
    //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    case USBCmdSettings_DeviceCriticalFlags:
        status = settings_setsettings_byopcode(SETTINGS_OPCODE_DEVICE_CRITICAL_FLAGS,0,
                                               settingsbuffer,
                                               settingsbufferlength);
        break;
    case USBCmdSettings_DeviceTuneFlags:
        status = settings_setsettings_byopcode(SETTINGS_OPCODE_DEVICE_TUNE_FLAGS,0,
                                               settingsbuffer,
                                               settingsbufferlength);
        break;
    case USBCmdSettings_DeviceFleetFlags:
        status = settings_setsettings_byopcode(SETTINGS_OPCODE_DEVICE_FLEET_FLAGS,0,
                                               settingsbuffer,
                                               settingsbufferlength);
        break;
    case USBCmdSettings_DeviceDatalogGeneralFlags:
        status = settings_setsettings_byopcode(SETTINGS_OPCODE_DEVICE_DATALOG_GENERAL_FLAGS,0,
                                               settingsbuffer,
                                               settingsbufferlength);
        break;
    //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
#endif
    default:
        log_push_error_point(0xB048);
        status = S_NOTSUPPORT;
        break;
    }
    if (status != S_SUCCESS)
    {
        log_push_error_point(0xB049);
    }

uch_settings_set_done:
    usb_set_csw(usb_callback_info.pdev,status,TRUE);
}

//------------------------------------------------------------------------------
// Handle hardware commands
// Engineer: Quyen Leba
//------------------------------------------------------------------------------
#if SUPPORT_USBCMD_HARDWARE
void uch_hardware()
{
    u8  *bptrS;
    u8  *bptrD;
    bool isSetStatus;
    u32 crc32e_calc;
    u32 crc32e_cmp;
    u8  status;
    u8 tempstring[32];

    isSetStatus = FALSE;
    status = S_FAIL;
    switch((USBCmdHardwareFlags)usb_callback_info.cbw.flags)
    {
    case USBCmdHardware_GetDeviceInfoString:
        status = deviceinfo_getinfostring(usb_databuffer, 1024);
        if(status == S_SUCCESS)
        {
          usb_databufferlength = strlen((char*)usb_databuffer);
          status = deviceinfo_getABfirmwareversion_string(tempstring);
          if(status == S_SUCCESS)
          {
            deviceinfo_append_info(usb_databuffer, 1024, &usb_databufferlength, 
                           "AB Version:", tempstring);
          }
          status = S_SUCCESS;
        }
        usb_databufferlength = strlen((char*)usb_databuffer)+1;
        SCTUSB_SetCmdStatus(status);
        if (status == S_SUCCESS)
        {
            if (usb_databufferlength <= usb_callback_info.cbw.datalength &&
                usb_databufferlength > 0)
            {
                //CSW private data has the actual length
                SCTUSB_SetStatusPrivData(usb_databufferlength);
                usb_databufferlength = usb_callback_info.cbw.datalength;
                usb_trigger_send_databuffer_to_host();
            }
            else
            {
                usb_set_csw(usb_callback_info.pdev,S_FAIL,TRUE);
            }
        }
        else
        {
            usb_set_csw(usb_callback_info.pdev,status,TRUE);
        }
        break;
    case USBCmdHardware_GetInfo:
        //input data: none
        //TODOQ:
        usb_set_csw(usb_callback_info.pdev,S_NOTSUPPORT,TRUE);
        break;
    case USBCmdHardware_FTLInit:
    case USBCmdHardware_FTLInitAdvance:
        genfs_delete_journal();
        //input data: none
        //privatedata[0] is chunk_index; each chunk is privatedata[1]*1024 bytes
        if (usb_callback_info.cbw.privatedata[1] > 0 &&
            usb_callback_info.cbw.privatedata[1] <= 16)
        {
            status = media_ftlinit(usb_callback_info.cbw.privatedata[0],
                                   usb_callback_info.cbw.privatedata[1]*1024);
            if (status == S_SUCCESS)
            {
                // ------- Heap Cleanup ---------------------
                cmdif_datainfo_responsedata_cleanup();
                if (usb_databuffer != NULL)
                {
                    __free(usb_databuffer);
                    usb_databuffer = NULL;
                }
                // ------------------------------------------
                usb_databuffermaxlength = usb_callback_info.cbw.privatedata[1]*1024;
                usb_databuffer = __malloc(usb_databuffermaxlength);
                if (usb_databuffer == NULL)
                {
                    usb_databuffermaxlength = 0;
                    status = S_MALLOC;
                    // Remalloc original 4k buffer for next USB command
                    uch_init();
                }
            }
        }
        else if (usb_callback_info.cbw.privatedata[1] == 0)
        {
            //old method, always 4096
            status = media_ftlinit(usb_callback_info.cbw.privatedata[0],USB_DATABUFFER_MAXSIZE);
            if (status == S_SUCCESS)
            {
                if (usb_databuffer != NULL)
                {
                    __free(usb_databuffer);
                    usb_databuffer = NULL;
                }
                usb_databuffermaxlength = USB_DATABUFFER_MAXSIZE;
                usb_databuffer = __malloc(USB_DATABUFFER_MAXSIZE);
                if (usb_databuffer == NULL)
                {
                    usb_databuffermaxlength = 0;
                    status = S_MALLOC;
                }
            }
        }
        else
        {
            status = S_OUTOFRANGE;
        }
        SCTUSB_SetStatusPrivData(MEDIA_TRAILING_DATA);
        usb_set_csw(usb_callback_info.pdev,status,TRUE);
        break;
    case USBCmdHardware_FTLRead:
        //input data: none
        status = media_ftlread(usb_databuffer,usb_callback_info.cbw.datalength,
                               &usb_databufferlength);
        SCTUSB_SetCmdStatus(status);
        if (status == S_SUCCESS)
        {
            if (usb_databufferlength == usb_callback_info.cbw.datalength &&
                usb_databufferlength > 0)
            {
                usb_trigger_send_databuffer_to_host();
            }
            else
            {
                usb_set_csw(usb_callback_info.pdev,S_FAIL,TRUE);
            }
        }
        else
        {
            usb_set_csw(usb_callback_info.pdev,status,TRUE);
        }
        break;
    case USBCmdHardware_FTLWrite:
        //input data: [4096: ftl block]
        if ((usb_callback_info.cbw.privatedata[0] | usb_callback_info.cbw.privatedata[1]) == 0)
        {
            status = media_ftlwrite(usb_databuffer,usb_callback_info.cbw.datalength);
        }
        else
        {
            crc32e_cmp = *(u32*)usb_callback_info.cbw.privatedata;
            crc32e_reset();
            crc32e_calc = crc32e_calculateblock(0xFFFFFFFF,(u32*)usb_databuffer,
                                                usb_callback_info.cbw.datalength/4);
            if (crc32e_calc == crc32e_cmp)
            {
                status = media_ftlwrite(usb_databuffer,usb_callback_info.cbw.datalength);
            }
            else
            {
                status = S_CRC32E;
            }
        }
        usb_set_csw(usb_callback_info.pdev,status,TRUE);
        break;
    case USBCmdHardware_FTLDeinit:
        status = media_ftldeinit();
        if (status == S_SUCCESS)
        {
            status = genfs_reinit();
            if (status == S_SUCCESS)
            {
                status = genfs_create_journal();
            }
        }

        //input data: none
        if (usb_databuffer != NULL)
        {
            __free(usb_databuffer);
            usb_databuffer = NULL;
        }
        uch_init();
        usb_set_csw(usb_callback_info.pdev,status,TRUE);
        break;
    case USBCmdHardware_FilesystemReinit:
        //input data: none
        status = genfs_reinit();
        usb_set_csw(usb_callback_info.pdev,status,TRUE);
        break;
    case USBCmdHardware_FormatMedia:
        //input data: none
        status = genfs_format();
        usb_set_csw(usb_callback_info.pdev,status,TRUE);
        break;
    case USBCmdHardware_Listing:
        //input data: none
        //privatedata[0] is marker; zero to start from 1st entry, otherwise
        //from current entry
        //privatedata[1] is folder selection
        memset((char*)usb_databuffer,0,usb_databuffermaxlength);
        status = genfs_get_listing_bulk(usb_callback_info.cbw.privatedata[0],
                                        usb_callback_info.cbw.privatedata[1],
                                        NULL,
                                        usb_callback_info.cbw.datalength,
                                        usb_databuffer, &usb_databufferlength);
        SCTUSB_SetCmdStatus(status);
        if (status == S_SUCCESS || status == S_TERMINATED)
        {
            if (usb_databufferlength > 0)
            {
                SCTUSB_SetStatusPrivData(usb_databufferlength);
                usb_databufferlength = usb_callback_info.cbw.datalength;
                usb_trigger_send_databuffer_to_host();
            }
            else
            {
                usb_set_csw(usb_callback_info.pdev,S_FAIL,TRUE);
            }
        }
        else
        {
            usb_set_csw(usb_callback_info.pdev,status,TRUE);
        }
        break;
    case USBCmdHardware_ListingSetFolder:
        //input data: folder name
        if ((strlen((char*)usb_databuffer)+1) == usb_databufferlength)
        {
            status = genfs_listing_set_folder(usb_databuffer);
        }
        else
        {
            status = S_BADCONTENT;
        }
        usb_set_csw(usb_callback_info.pdev,status,TRUE);
        break;
    case USBCmdHardware_GetFreeSpace:
        //input data: none
        SCTUSB_SetStatusPrivData(genfs_getfreespace());
        usb_set_csw(usb_callback_info.pdev,S_SUCCESS,TRUE);
        break;
    case USBCmdHardware_GetCapacity:
        //input data: none
        SCTUSB_SetStatusPrivData(genfs_getcapacity());
        usb_set_csw(usb_callback_info.pdev,S_SUCCESS,TRUE);
        break;
    case USBCmdHardware_GetFileSize:
        //input data: [n:filename][NULL]
        status = S_BADCONTENT;
        if (usb_databufferlength < usb_databuffermaxlength)
        {
            usb_databuffer[usb_databufferlength] = NULL;
            status = genfs_getfilesize(usb_databuffer, &SCTUSB_StatusPrivData);
        }
        usb_set_csw(usb_callback_info.pdev,status,TRUE);
        break;
    case USBCmdHardware_DeleteFile:
        //input data: [n:filename][NULL]
        status = S_BADCONTENT;
        if (usb_databufferlength < usb_databuffermaxlength)
        {
            usb_databuffer[usb_databufferlength] = NULL;
            status = genfs_deletefile(usb_databuffer);
        }
        usb_set_csw(usb_callback_info.pdev,status,TRUE);
        break;
    case USBCmdHardware_RenameFile:
        //input data: [n:existing filename][,][m:new filename][NULL]
        status = S_BADCONTENT;
        if (usb_databufferlength < usb_databuffermaxlength)
        {
            usb_databuffer[usb_databufferlength] = NULL;
            bptrD = (u8*)strstr((char*)usb_databuffer,",");
            if (bptrD != NULL && (bptrD - usb_databuffer < usb_databufferlength))
            {
                *bptrD++ = NULL;
                bptrS = usb_databuffer;
                if (strlen((char*)bptrS)+strlen((char*)bptrD)+2 <= usb_databufferlength)
                {
                    status = genfs_renamefile(bptrS,bptrD);
                }
            }
        }
        usb_set_csw(usb_callback_info.pdev,status,TRUE);
        break;
    case USBCmdHardware_MoveFile:
        //input data: [n:src filename][,][m:dest filename][NULL]
        status = S_BADCONTENT;
        if (usb_databufferlength < usb_databuffermaxlength)
        {
            usb_databuffer[usb_databufferlength] = NULL;
            bptrD = (u8*)strstr((char*)usb_databuffer,",");
            if (bptrD != NULL && (bptrD - usb_databuffer < usb_databufferlength))
            {
                *bptrD++ = NULL;
                bptrS = usb_databuffer;
                if (strlen((char*)bptrS)+strlen((char*)bptrD)+2 <= usb_databufferlength)
                {
                    status = genfs_movefile(bptrS,bptrD);
                }
            }
        }
        usb_set_csw(usb_callback_info.pdev,status,TRUE);
        break;
    case USBCmdHardware_TruncateFile:
        //input data: [4:newfilesize][n:filename][NULL]
        status = S_BADCONTENT;
        if (usb_databufferlength < usb_databuffermaxlength &&
            usb_databufferlength >= 10)
        {
            usb_databuffer[usb_databufferlength] = NULL;
            status = genfs_truncatefile(&usb_databuffer[4],
                                        *(u32*)usb_databuffer);
        }
        usb_set_csw(usb_callback_info.pdev,status,TRUE);
        break;
    case USBCmdHardware_CopyFile:
        //input data: [n:src filename][,][m:dest filename][NULL]
        status = S_BADCONTENT;
        if (usb_databufferlength < usb_databuffermaxlength)
        {
            usb_databuffer[usb_databufferlength] = NULL;
            bptrD = (u8*)strstr((char*)usb_databuffer,",");
            if (bptrD != NULL && (bptrD - usb_databuffer < usb_databufferlength))
            {
                *bptrD++ = NULL;
                bptrS = usb_databuffer;
                if (strlen((char*)bptrS)+strlen((char*)bptrD)+2 <= usb_databufferlength)
                {
                    status = genfs_copyfile(bptrS,bptrD);
                }
            }
        }
        usb_set_csw(usb_callback_info.pdev,status,TRUE);
        break;
    case USBCmdHardware_VerifyFile:
        //input data: [4:filesize][4:file crc32e][n:filename][NULL]
        status = S_BADCONTENT;
        if (usb_databufferlength < usb_databuffermaxlength &&
            usb_databufferlength >= 14)
        {
            usb_databuffer[usb_databufferlength] = NULL;
            status = genfs_verifyfile(&usb_databuffer[8],
                                      *(u32*)&usb_databuffer[0],
                                      *(u32*)&usb_databuffer[4]);
        }
        usb_set_csw(usb_callback_info.pdev,status,TRUE);
        break;
    //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    case USBCmdHardware_CreateDir:
        if ((strlen((char*)usb_databuffer)+1) == usb_databufferlength)
        {
            status = genfs_createdir(usb_databuffer);
        }
        else
        {
            status = S_BADCONTENT;
        }
        usb_set_csw(usb_callback_info.pdev,status,TRUE);
        break;
    case USBCmdHardware_RemoveDir:
        if ((strlen((char*)usb_databuffer)+1) == usb_databufferlength)
        {
            status = genfs_removedir(usb_databuffer);
        }
        else
        {
            status = S_BADCONTENT;
        }
        usb_set_csw(usb_callback_info.pdev,status,TRUE);
        break;
    case USBCmdHardware_ChangeDir:
        if ((strlen((char*)usb_databuffer)+1) == usb_databufferlength)
        {
            status = genfs_changedir(usb_databuffer);
        }
        else
        {
            status = S_BADCONTENT;
        }
        usb_set_csw(usb_callback_info.pdev,status,TRUE);
        break;
    case USBCmdHardware_GetCurrentDir:
        memset((char*)usb_databuffer,0,512);
        strcpy((char*)usb_databuffer,"\\");

        SCTUSB_SetCmdStatus(S_SUCCESS);
        usb_databufferlength = 512;
        usb_trigger_send_databuffer_to_host();
        break;
    case USBCmdHardware_ChangeDrive:
        usb_set_csw(usb_callback_info.pdev,S_NOTSUPPORT,TRUE);
        break;
    case USBCmdHardware_GetCurrentDrive:
        memset((char*)usb_databuffer,0,32);
        strcpy((char*)usb_databuffer,"MB");

        SCTUSB_SetCmdStatus(S_SUCCESS);
        usb_databufferlength = 32;
        usb_trigger_send_databuffer_to_host();
        break;
    //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    case USBCmdHardware_ShowMessage:
        //input data: [n:message][NULL]
        status = S_BADCONTENT;
        if (usb_databufferlength < usb_databuffermaxlength)
        {
            usb_databuffer[usb_databufferlength] = NULL;
            //TODOQ: display message
            status = S_SUCCESS;
        }
        usb_set_csw(usb_callback_info.pdev,status,TRUE);
        break;
    case USBCmdHardware_GetAnalogData:
        usb_set_csw(usb_callback_info.pdev,S_NOTSUPPORT,TRUE);
        break;
    case USBCmdHardware_SetBaudrate:
        usb_set_csw(usb_callback_info.pdev,S_NOTSUPPORT,TRUE);
        break;
    case USBCmdHardware_SetVolume:
        usb_set_csw(usb_callback_info.pdev,S_NOTSUPPORT,TRUE);
        break;
    case USBCmdHardware_SetBrightness:
        usb_set_csw(usb_callback_info.pdev,S_NOTSUPPORT,TRUE);
        break;
    //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    case USBCmdHardware_Unpair:
        status = hardware_access(HardwareAccess_Unpair,NULL,NULL,NULL);
        usb_set_csw(usb_callback_info.pdev,status,TRUE);
        break;
    case USBCmdHardware_GetFriendlyName:
        memset((char*)usb_databuffer,0,32);
        status = hardware_access(HardwareAccess_GetFriendlyName,NULL,
                                 usb_databuffer,&usb_databufferlength);
        SCTUSB_SetCmdStatus(status);
        if (status != S_SUCCESS)
        {
            usb_set_csw(usb_callback_info.pdev,status,TRUE);
        }
        else if (status == S_SUCCESS && usb_databufferlength > 0)
        {
            //friendly name max length is 24+NULL, however, always report
            //32 bytes back to HOST
            usb_databuffer[31] = NULL;
            usb_databufferlength = 32;
            usb_trigger_send_databuffer_to_host();
        }
        else
        {
            usb_set_csw(usb_callback_info.pdev,S_FAIL,TRUE);
        }
        break;
    case USBCmdHardware_SetFriendlyName:
        status = hardware_access(HardwareAccess_SetFriendlyName,usb_databuffer,
                                 NULL,NULL);
        usb_set_csw(usb_callback_info.pdev,status,TRUE);
        break;
    case USBCmdHardware_GetMacAddress:
        memset((char*)usb_databuffer,0,32);
        status = hardware_access(HardwareAccess_GetMacAddress,NULL,
                                 usb_databuffer,&usb_databufferlength);
        SCTUSB_SetCmdStatus(status);
        if (status != S_SUCCESS)
        {
            usb_set_csw(usb_callback_info.pdev,status,TRUE);
        }
        else if (status == S_SUCCESS && usb_databufferlength > 0)
        {
            //MAC address is 00:11:22:33:44:55, however, always report
            //32 bytes back to HOST
            usb_databuffer[17] = NULL;
            usb_databufferlength = 32;
            usb_trigger_send_databuffer_to_host();
        }
        else
        {
            usb_set_csw(usb_callback_info.pdev,S_FAIL,TRUE);
        }
        break;
    case USBCmdHardware_SetMacAddress:
        status = hardware_access(HardwareAccess_SetMacAddress,usb_databuffer,
                                 NULL,NULL);
        usb_set_csw(usb_callback_info.pdev,status,TRUE);
        break;
    //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    case USBCmdHardware_Reset:
        status = hardware_access(HardwareAccess_Reset,
                                 (u8*)&usb_callback_info.cbw.privatedata[0],
                                 NULL,NULL);
        usb_set_csw(usb_callback_info.pdev,status,TRUE);
        break;
    case USBCmdHardware_FirmwareVersionString:
        status = hardware_access(HardwareAccess_FirmwareVersionStr,
                                 (u8*)&usb_callback_info.cbw.privatedata[0],
                                 NULL,NULL);
        usb_set_csw(usb_callback_info.pdev,status,TRUE);
        break;
    //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    case USBCmdHardware_ValidateLookupHeader:
        status = obd2tune_validate_preloadedtune_lookupheader
            (*((u32*)usb_callback_info.cbw.privatedata[0]),
             (PreloadedTuneLookupHeader*)usb_databuffer,NULL,NULL);
        usb_set_csw(usb_callback_info.pdev,status,TRUE);
        break;
    //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    case USBCmdHardware_LEDSet:
        status = hardware_access(HardwareAccess_LEDSet,
                                 (u8*)&usb_callback_info.cbw.privatedata[0],
                                 NULL,NULL);
        usb_set_csw(usb_callback_info.pdev,status,TRUE);
        break;
    case USBCmdHardware_LEDRelease:
        status = hardware_access(HardwareAccess_LEDRelease,
                                 (u8*)&usb_callback_info.cbw.privatedata[0],
                                 NULL,NULL);
        usb_set_csw(usb_callback_info.pdev,status,TRUE);
        break;
    //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    default:
        status = S_INPUT;
        isSetStatus = TRUE;
        break;
    }

    if (isSetStatus)
    {
        usb_set_csw(usb_callback_info.pdev,status,TRUE);
    }
}
#endif  //SUPPORT_USBCMD_HARDWARE

#if SUPPORT_USBCMD_FILETRANSFER
//------------------------------------------------------------------------------
// Check if file unchanged
// Return:  u8  status (S_SUCCESS: unchanged)
// Engineer: Quyen Leba
//------------------------------------------------------------------------------
u8 uch_checkfile()
{
    struct
    {
        u32 filesize;
        u32 crc32e;
    }source,target;
    u8  *filename;
    u8  status;

    uch_closefile();
    status = S_SUCCESS;
    source.filesize = *(u32*)(usb_databuffer);
    source.crc32e = *(u32*)(&usb_databuffer[4]);
    filename = &usb_databuffer[8];
    if (usb_databufferlength > usb_databuffermaxlength ||
        (strlen((char*)filename)+4+4+1) != usb_databufferlength)
    {
        return S_INPUT;
    }

    usbcmdhandler_info.fileptr = genfs_general_openfile(filename,"r");
    if (usbcmdhandler_info.fileptr == NULL)
    {
        status = S_FILENOTFOUND;
    }
    else
    {
        status = file_getfilesize(usbcmdhandler_info.fileptr,
                                  &target.filesize);
        if (status == S_SUCCESS)
        {
            if (target.filesize != source.filesize)
            {
                status = S_UNMATCH;
            }
            else
            {
                status = file_calcfilecrc32e(usbcmdhandler_info.fileptr,
                                             &target.crc32e);
                if (status == S_SUCCESS)
                {
                    if (target.crc32e != source.crc32e)
                    {
                        status = S_UNMATCH;
                    }
                }
            }
        }
    }

    uch_closefile();
    return status;
}

//------------------------------------------------------------------------------
// Open file to start a file transfer session
// Return:  u8  status
// Engineer: Quyen Leba
//------------------------------------------------------------------------------
u8 uch_openfile()
{
    u32 totalbytetransfer;
    u8  status;

    status = S_SUCCESS;
    uch_closefile();
    //privatedata[0..1] is total bytes to transfer (to write or append)
    totalbytetransfer = *((u32*)usb_callback_info.cbw.privatedata);

    switch((USBCmdFileFlags)SCTUSB_GetCmdFlags())
    {
    case USBCmdFile_Read:
        usbcmdhandler_info.fileptr = genfs_general_openfile(usb_databuffer,"r");
        usbcmdhandler_info.file_length = file_getsize_byfptr(usbcmdhandler_info.fileptr);
        SCTUSB_SetStatusPrivData(usbcmdhandler_info.file_length);
        break;
    case USBCmdFile_Write:
        usbcmdhandler_info.fileptr = genfs_general_openfile(usb_databuffer,"w+");
        usbcmdhandler_info.file_length = totalbytetransfer;
        break;
    case USBCmdFile_Append:
        usbcmdhandler_info.fileptr = genfs_general_openfile(usb_databuffer,"a+");
        usbcmdhandler_info.file_position = file_getsize_byfptr(usbcmdhandler_info.fileptr);
        usbcmdhandler_info.file_length = usbcmdhandler_info.file_position + totalbytetransfer;
        SCTUSB_SetStatusPrivData(usbcmdhandler_info.file_position);
        break;
    default:
        status = S_BADCONTENT;
        break;
    }
    if (status == S_SUCCESS)
    {
        if (usbcmdhandler_info.fileptr == NULL)
        {
            uch_closefile();
            status = S_OPENFILE;
        }
    }

    usb_set_csw(usb_callback_info.pdev,status,TRUE);
    return status;
}

//------------------------------------------------------------------------------
// Read and prepare block data from file to transfer to HOST
// Engineer: Quyen Leba
//------------------------------------------------------------------------------
u8 uch_readfile()
{
    if (usbcmdhandler_info.fileptr)
    {
        if (usb_callback_info.cbw.datalength > usb_databuffermaxlength ||
            usb_callback_info.cbw.datalength == 0)
        {
            usb_set_csw(usb_callback_info.pdev,S_BADCONTENT,TRUE);
            return S_BADCONTENT;
        }

        usb_databufferindex = 0;
        usb_databufferlength = fread(usb_databuffer,
                                     1,usb_databuffermaxlength,
                                     usbcmdhandler_info.fileptr);
        usbcmdhandler_info.file_position += usb_databufferlength;
        
        if (usbcmdhandler_info.file_position >= usbcmdhandler_info.file_length)
        {
            uch_closefile();
        }
        else if (usb_databufferlength == 0)
        {
            uch_closefile();
            usb_set_csw(usb_callback_info.pdev,S_READFILE,TRUE);
            return S_READFILE;
        }

        SCTUSB_SetCmdStatus(S_SUCCESS);
        usb_trigger_send_databuffer_to_host();
    }
    else
    {
        usb_set_csw(usb_callback_info.pdev,S_OPENFILE,TRUE);
        return S_OPENFILE;
    }
    return S_SUCCESS;
}

//------------------------------------------------------------------------------
// Process data received from HOST and write it to file
// Engineer: Quyen Leba
//------------------------------------------------------------------------------
u8 uch_writefile()
{
    u8  status;

    if (usbcmdhandler_info.fileptr)
    {
        u32 bytecount;

        status = S_SUCCESS;
        bytecount = fwrite(usb_databuffer,
                           1,usb_databufferlength,usbcmdhandler_info.fileptr);
        if (bytecount != usb_databufferlength)
        {
            status = S_WRITEFILE;
        }
        else
        {
            usbcmdhandler_info.file_position += bytecount;
            if (usbcmdhandler_info.file_position >= usbcmdhandler_info.file_length)
            {
                status = uch_closefile();
            }
        }
    }
    else
    {
        status = S_OPENFILE;
    }

    usb_set_csw(usb_callback_info.pdev,status,TRUE);
    return status;
}

//------------------------------------------------------------------------------
// Close a file transfer session; if any
// Return:  u8  status
// Engineer: Quyen Leba
//------------------------------------------------------------------------------
u8 uch_closefile()
{
    if (usbcmdhandler_info.fileptr)
    {
        genfs_closefile(usbcmdhandler_info.fileptr);
        usbcmdhandler_info.fileptr = NULL;
    }
    usbcmdhandler_info.file_position = 0;
    usbcmdhandler_info.file_length = 0;
    
    return S_SUCCESS;
}

//------------------------------------------------------------------------------
// Close a file transfer session; if any
// Return:  u8  status
// Engineer: Quyen Leba
//------------------------------------------------------------------------------
u8 uch_recoverystock_check()
{
    u8  status;

    if (usb_databufferlength != RECOVERYSTOCK_HEADER_SIZE)
    {
        return S_INPUT;
    }
    status = obd2tune_recoverystock_check(usb_databuffer);
    usb_set_csw(usb_callback_info.pdev,status,TRUE);
    return status;
}

//------------------------------------------------------------------------------
// Close a file transfer session; if any
// Return:  u8  status
// Engineer: Quyen Leba
//------------------------------------------------------------------------------
u8 uch_recoverystock_register()
{
    u8  status;

    if (usb_databufferlength < RECOVERYSTOCK_HEADER_SIZE)
    {
        return S_INPUT;
    }
    status = obd2tune_recoverystock_check(usb_databuffer);
    if (status != S_SUCCESS)
    {
        usb_set_csw(usb_callback_info.pdev,status,TRUE);
        return status;
    }

    if (usb_databufferlength != RECOVERYSTOCK_HEADER_SIZE+1+strlen((char*)&usb_databuffer[RECOVERYSTOCK_HEADER_SIZE]))
    {
        return S_INPUT;
    }
    status = obd2tune_recoverystock_register(usb_databuffer,
                                             &usb_databuffer[RECOVERYSTOCK_HEADER_SIZE],
                                             NULL);
    usb_set_csw(usb_callback_info.pdev,status,TRUE);
    return status;
}

#endif  //SUPPORT_USBCMD_FILETRANSFER
