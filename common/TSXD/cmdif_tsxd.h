/**
  **************** (C) COPYRIGHT 2011 SCT Performance, LLC *********************
  * File Name          : cmdif_tsxd.h
  * Device / System    : SCT products using the "CommonCode" platform
  * Author             : Quyen Leba
  *
  * Version            : 1 
  * Date               : 05/02/2011
  * Description        : 
  *                    : 
  *
  *
  * History            : 05/02/2011 M. Davis
  *                    :   Add SCT Copyright header,
  *
  ******************************************************************************
  */

#ifndef __CMDIF_TSXD_H
#define __CMDIF_TSXD_H

#include <board/TSXD/bluetooth.h>

#define cmdif_response_ack(cmd,resp,data,len)   bluetooth_send_ack(cmd,resp,data,len)
#define cmdif_response_ack_nowait(cmd,resp,data,len)    bluetooth_send_ack_nowait(cmd,resp,data,len)
#define cmdif_response_rawspp(data,len)         bluetooth_send_rawspp(data,len)

#define cmdif_internalresponse_rawspp(data,len)         \
    cmdif_response_rawspp(data,len)

#define cmdif_internalresponse_ack(command,response,data,length)        \
    cmdif_response_ack(command,response,data,length)

#define cmdif_internalresponse_ack_nowait(command,response,data,length) \
    cmdif_response_ack_nowait(command,response,data,length)

#define cmdif_receive_command_internal(data,length)     \
    bluetooth_receive_command(data,length)

#endif  //__CMDIF_TSXD_H