/**
  **************** (C) COPYRIGHT 2011 SCT Performance, LLC *********************
  * File Name          : log.h
  * Device / System    : SCT products using the "CommonCode" platform
  * Author             : Quyen Leba
  *
  * Version            : 1 
  * Date               : 05/02/2011
  * Description        : 
  *                    : 
  *
  *
  * History            : 05/02/2011 M. Davis
  *                    :   Add SCT Copyright header,
  *
  ******************************************************************************
  */

#ifndef __LOG_H
#define __LOG_H

#include <arch/gentype.h>
#include <common/version.h>
#include <common/obd2tune.h>

#define LOG_TUNEHISTORY_FILENAME        "tunehist.log"
#define LOG_TUNEHISTORY_MAX_RECORD      128

#define LOG_MAX_ERROR_POINT_COUNT       40
typedef struct
{
    u16 error_point[LOG_MAX_ERROR_POINT_COUNT];
    u16 flags;
    u8  position;
    bool looped;
}error_point_pool;

typedef enum
{
    ErrorPointPoolSource_Application    = 0,
    ErrorPointPoolSource_Boot           = 1,
}ErrorPointPoolSource;

typedef struct
{
    u32 tagid;
    u32 flashcount;
    u32 flasherinfoCRC32e;
    u32 build;
    FIRMWARE_TAG fwtag;
}tunehistoryheader;

typedef struct
{
    union headerblock
    {
        tunehistoryheader data;
        u8 rawblock[256];        
    }headerblock;
    flasher_info flasherinfo;
}tunehistoryblock;

void log_reset();
void log_push_error_point(u16 errorpoint);
u8 log_get_error_point_list(ErrorPointPoolSource source,
                            u16 *error_point_list, u8 *error_point_count);
u8 log_get_error_point_list_string(ErrorPointPoolSource source,
                                   u8 *list, u16 *listlength);
u8 log_tunehistory();
void log_tunehistory_delete();
u8 log_tunehistory_readblock(u32 blockindex, tunehistoryblock *returnblock);

#endif  //__LOG_H
