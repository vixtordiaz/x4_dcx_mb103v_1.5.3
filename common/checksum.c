/**
  **************** (C) COPYRIGHT 2011 SCT Performance, LLC *********************
  * File Name          : checksum.c
  * Device / System    : SCT products using the "CommonCode" platform
  * Author             : Quyen Leba
  *
  * Version            : 1 
  * Date               : 05/02/2011
  * Description        : 
  *                    : 
  *
  *
  * History            : 05/02/2011 M. Davis
  *                    :   Add SCT Copyright header,
  *
  ******************************************************************************
  */

//------------------------------------------------------------------------------
// Log error point: 0x44xx -> 0x46xx (shared with obd2tune.c)
//------------------------------------------------------------------------------

#include <common/genmanuf_overload.h>
#include <common/filestock.h>
#include <common/statuscode.h>
#include <common/crc32.h>
#include <common/md5.h>
#include "checksum.h"

//------------------------------------------------------------------------------
// Calculate simple additive checksum of bytes
// Inputs:  u8  *data
//          u32 datalength
// Return:  u32 checksum
// Engineer: Quyen Leba
//------------------------------------------------------------------------------
/**
 * @brief       Additive Checksum
 * @details     Calculate simple additive checksum of bytes
 * @author      Quyen Leba 
 *
 * @param[in]   data        Data to checksum
 * @param[in]   datalength  Data length
 * @param[in]   checksum    Resulting checksum
 *
 * @return      status
 */ 
u32 checksum_simpleadditive(u8 *data, u32 datalength)
{
    u32 checksum;
    u32 i;
    checksum = 0;
    for(i=0;i<datalength;i++)
    {
        checksum += data[i];
    }
    return checksum;
}

/**
 * @brief       Apply additive checksum
 * @details     This function applies an additive checksum
 * @author      Tristen Pierson
 *
 * @param[in]   start_address       Checksum start address
 * @param[in]   segment_offset      File offset to segment data 
 * @param[in]   segment_length      Length of additive table
 * @param[in]   iscomplement        Apply complement?
 * @param[in]   ischeckonly         Check only?
 *
 * @return      status
 */ 
u8 checksum_apply_additives(u32 start_address, u32 segment_offset,
                            u32 segment_length, u8 quantity, bool iscomplement,
                            bool ischeckonly) 
{
    _additives additives;
    u32 *databuf;
    u8 status = S_SUCCESS;
    
    /* Malloc data buffer */ 
    databuf = __malloc(CRC_BUFFER_SIZE);
    
    /* Check malloc */   
    if ( !(databuf))
    {
       status = S_MALLOC;
    }
    
    /* Locate additives block and store info */
    if (status == S_SUCCESS)
    {
        status = checksum_locate_additives(databuf, segment_offset, segment_length, 
                                           quantity, &additives);
    }
    /* Calculate additive checksums */
    if (status == S_SUCCESS)
    {
        status = checksum_calculate_additives(databuf, segment_offset, 
                                              start_address, &additives);
    }
    /* Write additive checksums */
    if (status == S_SUCCESS)
    {
        status = checksum_write_additives(databuf, segment_offset, &additives, 
                                          iscomplement);
    }    
    
    /* Garbage Collection */
    __free(databuf);
    
    return status;
}

/**
 * @brief       Locate additive checksums
 * @details     This function applies a 32-bit additive checksum
 * @author      Tristen Pierson
 *
 * @param[in]   databuf         Buffer containing data checksum was applied to
 * @param[in]   segment_offset  File offset to segment data
 * @param[in]   segment_length  Segment length
 * @param[in]   quantity        Number of checksum segments
 * @param[in]   additives       Additive table info
 *
 * @return      status
 */ 
u8 checksum_locate_additives(u32 *databuf, u32 segment_offset, u32 segment_length, 
                              u8 quantity, _additives *additives)
{
    u8 status;
    u32 cnt1, i;
    u32 first, second, temp;
    u32 cnt, length;
    u32 bytes_read;
    
    additives->amount = 0;
    length = segment_length - CRC_BUFFER_SIZE - 16;
    
    status = filestock_set_position(segment_offset);
    if(status == S_SUCCESS)
    {
        for (cnt = 0; cnt < length;)         
        {
            status = filestock_read((u8*)databuf, CRC_BUFFER_SIZE, (u32*)&bytes_read);
            if(status != S_SUCCESS) 
            {
                status = S_FAIL;
                break;
            }
                        
            /* Process 16 bytes at a time */
            /* [32:start_address][32:end_address][32:additive][32:complement] */
            i = 0;
            for (cnt1 = 0; cnt1 < 128; cnt1++)  
            {
                first = databuf[2 + i];     /* Additive */
                second = databuf[3 + i];    /* Complement of Additive */
                temp = ~ second;            /* Apply complement to additive for check */
                
                /* Check for checksum and complement */
                /* and validate start and end address */
                if ( (first == temp) && (first != 0) && (second != 0) &&
                     (databuf[0 + i] != 0xFFFFFFFF) &&
                     (databuf[1 + i] != 0xFFFFFFFF) )
                {
                    additives->start_address[additives->amount] = __REV(databuf[0 + i]);
                    additives->end_address[additives->amount] = __REV(databuf[1 + i]);
                    additives->checksum_address[additives->amount++] = cnt + (cnt1 * 16);
                    if (additives->amount >= quantity) break;
                }
                i += 4;
            }
            if (additives->amount >= quantity) break;
            cnt += CRC_BUFFER_SIZE;
        }
    }
    
    if (status == S_SUCCESS && additives->amount == quantity) 
    {
        return S_SUCCESS;
    }
    return S_FAIL;
}

/**
 * @brief       Calculate additives
 * @details     Calculate additives in file
 * @author      Tristen Pierson
 *
 * @param[in]   databuf                 Buffer containing data checksum was applied to
 * @param[in]   segment_offset          File offset to segment data
 * @param[in]   segment_start_address   Checksum start address 
 * @param[in]   additives               Additive table info
 *
 * @return      status
 */ 
u8 checksum_calculate_additives(u32 *databuf, u32 segment_offset,
                                u32 segment_start_address, _additives *additives)
{
    u8 status;
    u32 cnt;    
    u32 additive_start_address;
    u32 additive_length;
    u32 checksum = 0;
    u32 cnt1, byteCnt, i, temp, temp2;

    for (cnt = 0; cnt < additives->amount; cnt++)
    {
        additive_start_address = segment_offset + additives->start_address[cnt] - segment_start_address;
        additive_length = additives->end_address[cnt] - additives->start_address[cnt];
        status = filestock_set_position(additive_start_address);
        if(status == S_SUCCESS)
        {
            cnt1 = checksum = 0;
            while (cnt1 < additive_length)
            {
                status = filestock_read((u8*)databuf, CRC_BUFFER_SIZE, (u32*)&byteCnt);
                if(status != S_SUCCESS)
                {
                    break;
                }
                    
                byteCnt/=4;                    // each read is 4 bytes
                for(i=0; i<byteCnt; i++)
                {
                    temp = __REV(databuf[i]); 
                    temp2=temp>>16;
                    temp&=0xFFFF;
                    checksum += temp;
                    checksum += temp2;
                    cnt1+=4;
                    if (cnt1 >= additive_length)
                    {
                        break;
                    }
                }
            }
            additives->checksum[cnt] = checksum;
            if (status != S_SUCCESS)
            {
                break;
            }
        }
    }
    
    return status;
}

/**
 * @brief       Write additive checksum
 * @details     This function writes additive checksum and data
 *              NOTE: require flash file already opened; use filestock_openflashfile(...)
 * @author      Tristen Pierson
 *
 * @param[in]   databuf         Buffer containing data checksum was applied to
 * @param[in]   segment_offset  File offset to segment data
 * @param[in]   additives       Additive table info
 * @param[in]   iscomplement    Calculate and write checksum complement?
 *
 * @return      status
 */ 
u8 checksum_write_additives(u32* databuf, u32 segment_offset, 
                            _additives* additives, bool iscomplement)
{
    u8 status = S_SUCCESS;
    u32 cnt;
    u32 check_address;
    u32 write_data;    
    
    for (cnt = 0; cnt < additives->amount; cnt++)
    {
        check_address = segment_offset + additives->checksum_address[cnt] + 8;
        write_data = __REV(additives->checksum[cnt]);
        
        status = filestock_updateflashfile(check_address,(u8*)&write_data, 4);
        if(status != S_SUCCESS)
        {
            break;
        }
        if (status == S_SUCCESS && iscomplement == TRUE)
        {
            write_data = ~write_data;
            status = filestock_updateflashfile(check_address+4,(u8*)&write_data, 4);
        }
        if (status != S_SUCCESS)
        {
            break;
        }
    }
    return status;
}

/**
 * @brief       CRC-32
 * @details     This function applies a CRC32
 * @author      Tristen Pierson
 *
 * @param[in]   start_address       Start address of segment
 * @param[in]   end_address         Segment ending address
 * @param[in]   seed                Starting CRC32 seed
 * @param[in]   CRC_value           CRC32 flags
 * @param[in]   flags               CRC32 flags
 *
 * @return      status
 */ 
u8 checksum_apply_crc32(u32 start_address, u32 end_address, u32 seed, u32 flags,
                        u32 *CRC_value)
{
    u8 status;
    _word value;
    if (end_address > start_address)
    {
        status = checksum_calc_file_segment_crc32(seed, start_address, end_address - start_address + 1, (u32*)&value.word, flags);
    }
    else
    {
        status = S_FAIL;
    }
    if(status == S_SUCCESS)
    {
        *CRC_value = value.word;
        return S_SUCCESS;
    }
    return S_FAIL;
}

/**
 * @brief       Calculate CRC-32
 * @details     This function calculates the CRC32 for a flash file segment.
 *              NOTE: require flash file already opened; use filestock_openflashfile(...)
 * @authors     Patrick Downs, Quyen Leba
 *
 * @param[in]   seed                Starting CRC32 seed 
 * @param[in]   address             Starting address of segment
 * @param[in]   length              Length of the segment
 * @param[in]   CRCvalue            Return address of CRC value
 *
 * @return      status
 */
u8 checksum_calc_file_segment_crc32(u32 seed, u32 address, u32 length, u32 *CRCvalue, u32 flags)
{
    u8 status;
    u8 *buffer;
    u32 currentaddress = address;
    const u32 endaddress = address + length;
    
    u32 bytecount;
    
    
    buffer = __malloc(CRC_BUFFER_SIZE);
    if(!buffer) return S_MALLOC;
    
    // Seek to the first spot in the file to be used in the CRC.
    status = filestock_set_position(address);
    if(status != S_SUCCESS)
    {
        __free(buffer);
        return status;
    }
    
    // Continue processing until we reach the end of the block.
    while (currentaddress != endaddress)
    {
        // Calculate the total amount of data left to be processed.
        length = endaddress - currentaddress;
        
        // If the amount left is greater than what can fit in our buffer,
        // cap it to the buffer size.
        if (length > CRC_BUFFER_SIZE)
            length = CRC_BUFFER_SIZE;
        
        if ((flags & CRC32_FLAGS_MASK_0xFF) == 0)
        {
            // Read out the next block of data to be processed.
            status = filestock_read((u8*)buffer, length, (u32*)&bytecount);
            if(bytecount != length)
            {
                __free(buffer);
                return S_READFILE;
            }
        }
        else
        {
            // Next block of data is treated as 0xFF's
            memset(buffer, 0xFF, length);
        }
        
        // Send it into the CRC function
        seed = crc32_sw_calculateblock(seed, buffer, length);
        
        // Update the index into the file.
        currentaddress += length;
    }
    
    __free(buffer);
    
    // Update the CRC of this block.
    *CRCvalue = seed;
    
    return S_SUCCESS;
}

/**
 * @brief       Calculate MD5
 * @details     This function calculates a MD5 hash for a flash file segment
 *              NOTE: require flash file already opened; use filestock_openflashfile(...)
 * @author      Tristen Pierson
 *
 * @param[in]   start_address       Start address of segment
 * @param[in]   end_address         Segment ending address
 * @param[in]   md5_hash            The MD5 hash for the segment
 *
 * @return      status
 */ 
u8 checksum_calc_file_segment_md5(u32 start_address, u32 end_address, u8 *md5_hash)
{  
    u8 status = S_SUCCESS;
    MD5_CTX md5_ctx;
    u8 md5_buffer[64];
    u32 md5_pos;
    u32 md5_length;
    u32 bytes_read;

    if (start_address > end_address)
    {
        status = S_INPUT;
    }
    
    if (status == S_SUCCESS)
    {
    
        md5_pos = start_address;
        md5_length = end_address - start_address + 1;
        
        /* Initialize the hash state */
        MD5_Init(&md5_ctx);
        
        /* Process full blocks */
        for (; md5_length >= 64; md5_pos += 64, md5_length -= 64)         
        {
            status = filestock_read_at_position(md5_pos, md5_buffer, 64, &bytes_read);
            MD5_Update(&md5_ctx, md5_buffer, 64);
        }
        /* Process final partial block */
        if (md5_length)
        {
            status = filestock_read_at_position(md5_pos, md5_buffer, md5_length, &bytes_read);
            MD5_Update(&md5_ctx, md5_buffer, md5_length);
        }
        /* Get the hash */
        MD5_Final(md5_hash, &md5_ctx);
    }
    return status;
}