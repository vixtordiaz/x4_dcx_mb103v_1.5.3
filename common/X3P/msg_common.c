/**
  **************** (C) COPYRIGHT 2011 SCT Performance, LLC *********************
  * File Name          : msg_common.c
  * Device / System    : SCT products using the "CommonCode" platform
  * Author             : Quyen Leba
  *
  * Version            : 1 
  * Date               : 05/02/2011
  * Description        : 
  *                    : 
  *
  *
  * History            : 05/02/2011 M. Davis
  *                    :   Add SCT Copyright header,
  *
  ******************************************************************************
  */

#include "msg_common.h"

const u8    AnalogMenuIndex[] = {5,8,2,3,2};    // count for menu
const u8    *AnalogMenu[5][8] =
                        {
                            {"WideBand","EGT","MAP","Raw (0-5V)","Off"},
                            {"LM1","TE 2A0","PLX","LC1","AEM","AFM1000","Dynojet WBC","Custom WB"},
                            {"SCT EGT","PLX EGT"},
                            {"GM 1BAR","GM 2BAR","GM 3BAR"},
                            {"Raw 0-5V","Custom 0-5V"}
                        };
const u8    AnalogUnit[5][8] =
                        {
                            {0,0,0,0,0,0,0,0},
                            {0,0,0,0,0,0,0,0},
                            {1,1},
                            {3,3,3},
                            {2,0}
                        };

    //------------------------------------------= "123456789012345";//15chars
    const u8*   HDR_SetupMonitor                = "DATALOG SETUP"  ;
    const u8*   HDR_Monitor                     = "DATALOG"        ;
    const u8*   HDR_EstimatedMPHTime            = "0-MPH TIME"     ;
    const u8*   HDR_ClearDiagTroubleCode        = "CLEAR DTCs"     ;
    const u8*   HDR_ReadDiagTroubleCode         = "READ DTCs"      ;
    const u8*   HDR_ReadPendingDiagTroubleCode  = "READ PEND. DTCs";
    const u8*   HDR_PIDAlertSetup               = "PID ALERT SETUP";
    const u8*   HDR_UploadECU                   = "UPLOAD ECU"     ;
    const u8*   HDR_DownloadTune                = "DOWNLOAD TUNE"  ;
    const u8*   HDR_DownloadStock               = "DOWNLOAD STOCK" ;
    const u8*   HDR_UnlockECU                   = "UNLOCK ECU"     ;
#ifdef BANKS_ENG
    const u8*   HDR_VehicleInfo                 = "READ FAMILY"    ;
    const u8*   HDR_DealerInfo                  = "CONTACT INFO"   ;
    const u8*   HDR_SelectVehicle               = "SELECT VEHICLE" ;
    const u8*   HDR_UserAdjOpts                 = "ADJUST OPTIONS" ;
    const u8*   HDR_TuneVehicleECU              = "TUNE VEHICLE"   ;
    const u8*   HDR_UserAdjustmentOptions       = "ADJ. OPTIONS"   ;
    const u8*   HDR_ProgramVehicleECU           = "FLASH VEHICLE"  ;
    const u8*   HDR_Warning                     = "WARNING!"       ;
    const u8*   HDR_DeviceInfo                  = "SOFTWARE INFO"  ;
#else    
    const u8*   HDR_VehicleInfo                 = "VEHICLE INFO"   ;
    const u8*   HDR_DealerInfo                  = "DEALER INFO"    ;
    const u8*   HDR_UserAdjustmentOptions       = "ADJUST OPTIONS" ;
    const u8*   HDR_ProgramVehicleECU           = "PROGRAM VEHICLE";
    const u8*   HDR_Warning                     = "- WARNING -"    ;
    const u8*   HDR_DeviceInfo                  = "DEVICE INFO"    ;
    const u8*   HDR_SelectAuto                  = "SELECT AUTO"    ;
#endif    
    const u8*   HDR_Attention                   = "- Attention -"  ;
    const u8*   HDR_DeviceSettings              = "DEVICE SETTINGS";
    const u8*   HDR_SetupAnalog                 = "SETUP ANALOG"   ;
    const u8*   HDR_SetupAnalog1                = "SETUP ANALOG 1" ;
    const u8*   HDR_SetupAnalog2                = "SETUP ANALOG 2" ;
    const u8*   HDR_EstimatedQuarterMileTime    = "1/4 MILE TIME"  ;
    const u8*   HDR_ReturnVehicleToStock        = "RETURN TO STOCK";
    const u8*   HDR_MatchingVehicle             = "MATCH VEHICLE"  ;
    const u8*   HDR_SelectFuelType              = "SELECT FUELTYPE";
    const u8*   HDR_CopyFile                    = "COPY FILE"      ;
    const u8*   HDR_SetupCustomEquation         = "SETUP EQUATION" ;
    const u8*   HDR_SetupStorage                = "SETUP STORAGE"  ;
    const u8*   HDR_SetupECU                    = "SETUP ECU"      ;
    const u8*   HDR_ResetECU                    = "RESET ECU"      ;
    const u8*   HDR_VehicleNotSupported         = "ECU UNSUPPORTED";
    const u8*   HDR_EstimatedHPTQ               = "HP & TORQUE"    ;
    const u8*   HDR_StockfileUpdated            = "Stock Updated"  ;
    const u8*   HDR_PassthroughDatalog          = "PT DATALOG"     ;
    const u8*   HDR_Disclaimer_Title            = "- DISCLAIMER - ";
    const u8*   HDR_TuneType                    = "TUNE TYPE"      ;
    const u8*   HDR_OS_Mismatch                 = "OS MISMATCH"    ;
    const u8*   HDR_OS_Blocked                  = "OS BLOCKED"     ;
    const u8*   HDR_POWERDOWN                   = "POWER DOWN ECU" ;

    //------------------------------------------= "123456789012345";//15chars

    //------------------------------------------= "123456789012345678901";//21chars
#ifdef BANKS_ENG
    const u8*   TXT_BANKS_Backlight             = "Backlight1";  
    const u8*   TXT_BANKS_TurnKeyOn_No_ENT      = "Turn key On";
#endif
    const u8*   TXT_SetupDevice                 = "SETUP DEVICE"         ;
    const u8*   TXT_Backlight                   = "Backlight"            ;
    const u8*   TXT_Audio                       = "Audio"                ;
    const u8*   TXT_ClearDTCSuccessful          = "DTC's Cleared Successfully";   //ok
    const u8*   TXT_ReadDTCFail                 = "Unable to Read DTC's" ;
    const u8*   TXT_ClearDTCFail                = "DTC Clear Command Sent";     //ok
    const u8*   TXT_AdjustOption                = "Adjust Options"       ;
    const u8*   TXT_SkipOption                  = "Skip Options"         ;
    const u8*   TXT_KeepPreviousOption          = "Keep Prev Opts"       ;
    const u8*   TXT_SelectOption                = "Select Option:"       ;
    const u8*   TXT_ConfigAnalog                = "Config Analog Inputs?";
    const u8*   TXT_SelectAFile                 = "Select a File"        ;
    const u8*   TXT_SelectPID                   = "Select PIDs"          ;
    const u8*   TXT_StartMonitor                = "Start Monitor"        ;
    const u8*   TXT_DataViewFormat              = "Data Format:"         ;
    const u8*   TXT_CheckConnectionAndEngine    = "Check Connection"     ;  //TODO: vague message
    const u8*   TXT_SelectEquation              = "Select format:"       ;
    const u8*   TXT_SelectEquationNote1         = "where v = volt input" ;
    const u8*   TXT_SelectEquationNote2         = "a,b = your constants" ;
    const u8*   TXT_PressKeyRetryProgram        = "Please Retry to Program Vehicle";
    const u8*   TXT_OS_Mismatch_1               = "The OS part number"   ;
    const u8*   TXT_OS_Mismatch_2               = "does not"             ;
    const u8*   TXT_OS_Mismatch_3               = "match the vehicle."   ;
    const u8*   TXT_OS_Mismatch_4               = "ECM damage may occur!";
    const u8*   TXT_OS_Mismatch_5               = "Press Cancel to Quit ";
    const u8*   TXT_ECU_CAL_DL                      = "ECU CAL Download";
    const u8*   TXT_ECU_FULL_DL                     = "ECU FULL Download";
    const u8*   TXT_TCU_CAL_DL                      = "TCU CAL Download";
    const u8*   TXT_TCU_FULL_DL                     = "TCU FULL Download";
    const u8*   TXT_TCM_Mismatch_1               = "The TCM part number"   ;

#ifdef BANKS_ENG
    const u8*   TXT_BANKS_Disclaimer            = "THE BANKS ECONOMIND MUST BE USED WITH\n A FACTORY TUNE, OTHERWISE DAMAGE MAY RESULT TO YOUR VEHICLE.";
    const u8*   TXT_BANKS_Disclaimer_2          = "IF YOU ARE NOT SURE ABOUT YOUR TUNE, CONTACT GALE BANKS ENGINEERING.";
    const u8*   TXT_BANKS_Yes_No                = "To Accept press <ENT>\n\nor\n\n To Reject press <ESC>";
    const u8*   TXT_BANKS_Key_ON                = "Turn Key On then\n\nPress ENT";
    const u8*   TXT_BANKS_Key_OFF               = "Turn Key Off then\n\nPress ENT";
    const u8*   TXT_BANKS_Key_OFF_ESC           = "Turn Key Off then\n\nPress ENT or ESC";
    const u8*   TXT_BANKS_Scroll                = "Up/Down-Scroll Files";
    const u8*   TXT_BANKS_Begin_Program         = "BEGIN PROGRAM";
    const u8*   TXT_BANKS_Cancel_Program        = "CANCEL PROGRAM";
    const u8*   TXT_BANKS_Load_StockText        = "This will return ECU  to factory program   \n\nPress ENT to continue\nPress ESC to exit    ";
    const u8*   TXT_BANKS_ENT_ESC               = "\n\nPress ENT to continue\nPress ESC to exit    ";
    const u8*   TXT_BANKS_ENT_Description       = "ENT for Description";
#else
    const u8*   TXT_TurnKey_ON                  = "Turn Key On\nPress Select";
    const u8*   TXT_SCT_SEL_CAN                 = "\n\nPress SEL to continue\nPress CAN to exit    ";
    const u8*   TXT_Load_StockText              = "This will return ECU to stock  \n\nPress SEL to continue\nPress ESC to exit    ";
    const u8*   TXT_BATTERY_LOW                 = "Battery is too LOW!";
#endif                               
    //------------------------------------------= "123456789012345678901";//21chars

    //------------------------------------------= "123456789012345";//15chars

#ifdef BANKS_ENG
    const u8*   FDR_ExitDoneKeyMsg              = "ESC: Done"  ;
    const u8*   FDR_ContinueKeyMsg              = "ESC/ENT: Exit";
    const u8*   FDR_ContinueMsg                 = "ENT: Continue";
    const u8*   FDR_SelectMsg                   = "ENT: Select";
    const u8*   FDR_ExitBackKeyMsg              = "ESC: Go Back";
    const u8*   FDR_ESCExitMsg                  = "ESC: Exit";
    const u8*   FDR_SelectResume                = "ENT: Resume"  ;
#else
    const u8*   FDR_ContinueKeyMsg              = "Select:Continue";
    const u8*   FDR_ExitDoneKeyMsg              = "Cancel - Done"  ;
    const u8*   FDR_ExitBackKeyMsg              = "Cancel - GoBack";
    const u8*   FDR_SelectResume                = "Select:Resume"  ;
#endif    
    const u8*   FDR_ExitKeyMsg                  = "Press Cancel"   ;
    const u8*   FDR_PressESCExit                = "Cancel - Exit"  ;
    //------------------------------------------= "123456789012345";//15chars


    //------------------------------------------= "123456789012345678901";//21chars
    const u8*   TXT_NULLSTR                     = "";
    const u8*   TXT_PressWheelToSelect          = "Push Wheel: Select";
    const u8*   TXT_PressWheelToSelectUnSelect  = "Push Wheel to (Un)Select - ESC to Save";
    const u8*   TXT_PressESCToSave              = "Press ESC to Save Selection";
    const u8*   TXT_TurnWheelToScroll           = "Turn Wheel: Scroll";
    const u8*   TXT_TurnKeyOn                   = "Turn Key On\nDo Not Start Engine";
    const u8*   TXT_TurnKeyOff                  = "Turn Key Off";   
    const u8*   TXT_TurnKeyOff_Remove           = "Turn Key Off\nAnd Remove key";
    const u8*   TXT_DoNotStartEngine            = "Do Not Start Engine" ;
    const u8*   TXT_UploadComplete              = "Upload Complete";
    const u8*   TXT_PrepareDownload             = "Prepare to Download Tune";
    const u8*   TXT_ProgramPCMOnly              = "Program PCM Only. Continue?";
    const u8*   TXT_DownloadComplete            = "Download Complete"    ;
    const u8*   TXT_UnlockingECU                = "Unlocking ECU"        ;
    const u8*   TXT_ECMStock                    = "ECM is now Stock"     ;
    const u8*   TXT_PleaseWait                  = "Please Wait ..."      ;
    const u8*   TXT_SearchFiles                 = "Searching for files"  ;
    const u8*   TXT_ProcessFile                 = "Processing File"      ;
    const u8*   TXT_NoPIDs                      = "No PIDs Selected"     ;
    const u8*   TXT_StartEngineAndPrepare       = "Start Your Engine & Prepare";
    const u8*   TXT_CompleteStopVehicle         = "Your Vehicle must be at Complete Stop";
    const u8*   TXT_On                          = "ON"                   ;
    const u8*   TXT_Off                         = "OFF"                  ;
    const u8*   TXT_StandbyFlash                = "(Standby: Flash)";
    const u8*   TXT_StandbyOff                  = "(Standby: Off)";
    const u8*   TXT_SetupECUFail                = "Setup ECU Failed"     ;
    const u8*   TXT_NoDTCFound                  = "No DTC's found"       ;
    const u8*   TXT_ReturnStockMsg1             = "After Returning this Vehicle to Stock";
    const u8*   TXT_ReturnStockMsg2             = "You will have only\none UNLOCK left";
    const u8*   TXT_ReturnStockMsg3             = "Your Unlock Left will be ";
    const u8*   TXT_ProgramWithCustom           = "Program with Custom Tune";
    const u8*   TXT_DownloadFail                = "Download Failed"      ;
    const u8*   TXT_ReuploadStockfile           = "Reupload Stock File?" ;
    const u8*   TXT_DeviceUnlocked              = "ECM has been Unlocked";
    const u8*   TXT_PressESCGoBack              = "Press ESC to Go Back" ;
    const u8*   TXT_SetWeight                   = "Set Vehicle Weight (lbs)";
    const u8*   TXT_WheelResetEscExit           = "Press Wheel: Reset - Press ESC: Exit";
    const u8*   TXT_ReadingOptions              = "Loading options..."   ;
    const u8*   TXT_UploadSuccessful            = "Upload Successful";
    const u8*   TXT_UploadFailed                = "Upload Failed";
    const u8*   TXT_UploadAborted               = "Upload not attempted";
#ifdef BANKS_ENG 
    const u8*   TXT_BANKS_Contact_Literal       = "contact:\n";
    const u8*   TXT_ProgramWithStrategy         = "Program with Strategy Tune";
    const u8*   TXT_BANKS_Contact               = "Gale Banks\nEngineering\nwww.bankspower.com\n1-800-GET-POWER";    
    const u8*   TXT_UNLOCKPleaseWait            = "Unlocking PCM\n  Please wait";
    const u8*   TXT_ContactPhoneWeb             = "www.bankspower.com\n1-800-GET-POWER";    
#else
    const u8*   TXT_ProgramWithStrategy         = "\nwith Strategy Tune";
    const u8*   TXT_UNLOCKPleaseWait            = "Unlocking ECU\n  Please wait";
    const u8*   TXT_ContactPhoneWeb             = "(407)774-2447\nwww.sctflash.com";
    const u8*   TXT_ContactName                 = "SCT LLC"              ;
    const u8*   TXT_ContactPhone                = "(407)774-2447"        ;
    const u8*   TXT_ContactWeb                  = "www.sctflash.com"     ;
#endif    
    const u8*   TXT_MemStorage                  = "Memory Storage"       ;
    const u8*   TXT_No_OSPartnumber             = "No OS Part number\n   Found";
    const u8*   TXT_No_Strategy                 = "No Strategy Found"    ;
    const u8*   TXT_DefaultOptions              = "Default Options"      ;
    const u8*   TXT_SavedOptions                = "Saved Options"        ;
    const u8*   TXT_NoInfo                      = "Info Not Available"   ;
    const u8*   TXT_Contrast                    = "Contrast"             ;
    const u8*   TXT_Sounds_USB_pwr              = "Sounds (USB-power)"   ;
    const u8*   TXT_Playback                    = "Playback"             ;
    const u8*   TXT_RecordLog                   = "Record Log? "         ;
    const u8*   TXT_SetupMonitor                = "Setup Monitor"        ;
    const u8*   TXT_SetDataViewFormat           = "Set Data View Format:";
    const u8*   TXT_Yes                         = "Yes"                  ;
    const u8*   TXT_No                          = "No"                   ;
    const u8*   TXT_RecordMonitorData           = "Record Monitor Data?" ;
    const u8*   TXT_CommunicationLost           = "Communication Lost!"  ;
    const u8*   TXT_ResumeDatalog               = "Resume Datalog"       ;
    const u8*   TXT_Exit                        = "Exit"                 ;
    const u8*   TXT_CommunicationFail           = "Communication Failed!";
    const u8*   TXT_InitializeMonitor           = "Initialize Monitor"   ;
    const u8*   TXT_StartEngine                 = "Start Engine"         ;
    const u8*   TXT_Channel1                    = "Channel 1"            ;
    const u8*   TXT_Channel2                    = "Channel 2"            ;
    const u8*   TXT_EnterValueA                 = "Enter value for 'a'"  ;
    const u8*   TXT_EnterValueB                 = "Enter value for 'b'"  ;
    const u8*   TXT_LiveLinkActivated           = "LiveLink Activated"   ;
    const u8*   TXT_CANDetected                 = "CAN Detected"         ;
    const u8*   TXT_SCPDetected                 = "SCP Detected"         ;
    const u8*   TXT_VPWDetected                 = "VPW Detected"         ;  
    const u8*   TXT_DLF_Files                   = "No DLF files found"   ; 
    const u8*   TXT_Load_DLF_Files              = "Load DLF files using\nSCT LiveLink\nsoftware.";
#ifdef BANKS_ENG
    const u8*   TXT_AlreadyStock                = "Vehicle is Already Stock";
    const u8*   TXT_NoDTCInfo                   = "DTC Information is Not  Available";
#else
    const u8*   TXT_AlreadyStock                = "Device Already Returned to Stock";
    const u8*   TXT_SCT_Disclaimer              = "This is a performance product. See disclaimer at www.sctflash.com.\n\nPush Select to Accept";
    const u8*   TXT_SCT_Accept                  = "Push Select to Accept";
#endif    
    const u8*   TXT_UpdateTuneRevision          = "Please Update Device with Lastest Tune Revision";
    const u8*   TXT_InitFileSystem              = "Init FileSystem ..."  ;
    const u8*   TXT_InvalidFileSystem           = "FileSystem Not Found" ;
    const u8*   TXT_ReloadFileSystem            = "Please reload it"     ;
    const u8*   TXT_Engine_DL                   = "Engine"               ;
    const u8*   TXT_Transmission_DL             = "Transmission"         ;
    const u8*   TXT_Auto_Upload                 = "Attempting to upload" ;
    const u8*   TXT_Calc_Checksum               = "Calculating Checksums";
    const u8*   TXT_Blank                       = "Blank";
    const u8*   TXT_Invalid                     = "Invalid";
    const u8*   TXT_Unknown                     = "Unknown";

    //------------------------------------------= "123456789012345";//15chars
    const u8*   HDR_Bootloader                  = "BOOTLOADER"     ;
    const u8*   HDR_ReadStrategy                = "READ OS PART #"  ;
    const u8*   HDR_SelectFile                  = "SELECT FILE"    ;
    const u8*   HDR_SelectPID                   = "SELECT PIDs"    ;
    const u8*   FDR_DoNotUnplug                 = "DO NOT UNPLUG"  ;
    //------------------------------------------= "123456789012345";//15chars





    
const LANGUAGE Language_English =
{
    .HDR_ReadDiagTroubleCode        = (void*)&HDR_ReadDiagTroubleCode,
    .HDR_ReadPendingDiagTroubleCode = (void*)&HDR_ReadPendingDiagTroubleCode,
    .HDR_DealerInfo                 = (void*)&HDR_DealerInfo,
    .HDR_DeviceInfo                 = (void*)&HDR_DeviceInfo,
    .HDR_DeviceSettings             = (void*)&HDR_DeviceSettings,
    .HDR_VehicleInfo                = (void*)&HDR_VehicleInfo,
    .HDR_UploadECU                  = (void*)&HDR_UploadECU,
    .HDR_DownloadTune               = (void*)&HDR_DownloadTune,
    .HDR_DownloadStock              = (void*)&HDR_DownloadStock,
    .HDR_UnlockECU                  = (void*)&HDR_UnlockECU,
    .HDR_SetupMonitor               = (void*)&HDR_SetupMonitor,
    .HDR_Monitor                    = (void*)&HDR_Monitor,
    .HDR_VehicleNotSupported        = (void*)&HDR_VehicleNotSupported,
    .HDR_EstimatedHPTQ              = (void*)&HDR_EstimatedHPTQ,
    .HDR_SetupAnalog1               = (void*)&HDR_SetupAnalog1,
    .HDR_SetupAnalog2               = (void*)&HDR_SetupAnalog2,
    .HDR_EstimatedQuarterMileTime   = (void*)&HDR_EstimatedQuarterMileTime,
    .HDR_PIDAlertSetup              = (void*)&HDR_PIDAlertSetup,
    .HDR_ClearDiagTroubleCode       = (void*)&HDR_ClearDiagTroubleCode,
    .HDR_ProgramVehicleECU          = (void*)&HDR_ProgramVehicleECU,
    .HDR_SetupStorage               = (void*)&HDR_SetupStorage,
    .HDR_SetupECU                   = (void*)&HDR_SetupECU,
    .HDR_ResetECU                   = (void*)&HDR_ResetECU,
    .HDR_MatchingVehicle            = (void*)&HDR_MatchingVehicle,
    .HDR_SelectFuelType             = (void*)&HDR_SelectFuelType,
    .HDR_UserAdjustmentOptions      = (void*)&HDR_UserAdjustmentOptions,
    .HDR_CopyFile                   = (void*)&HDR_CopyFile,
    .HDR_ReadStrategy               = (void*)&HDR_ReadStrategy,
    .HDR_SelectFile                 = (void*)&HDR_SelectFile,
    .HDR_SelectPID                  = (void*)&HDR_SelectPID,
    .HDR_SetupCustomEquation        = (void*)&HDR_SetupCustomEquation,
    .HDR_SetupAnalog                = (void*)&HDR_SetupAnalog,
    .HDR_Bootloader                 = (void*)&HDR_Bootloader,
    .HDR_PassthroughDatalog         = (void*)&HDR_PassthroughDatalog,
    .HDR_Disclaimer_Title           = (void*)&HDR_Disclaimer_Title,
    .HDR_Warning                    = (void*)&HDR_Warning,
	.HDR_OS_Mismatch                = (void*)&HDR_OS_Mismatch,
    .HDR_OS_Blocked                 = (void*)&HDR_OS_Blocked,
    .HDR_POWERDOWN                  = (void*)&HDR_POWERDOWN,
#ifdef BANKS_ENG
    .HDR_SelectVehicle              = (void*)&HDR_SelectVehicle,
    .HDR_UserAdjOpts                = (void*)&HDR_UserAdjOpts,
    .HDR_TuneVehicleECU             = (void*)&HDR_TuneVehicleECU,
#else
    .HDR_TuneType                   = (void*)&HDR_TuneType,
    .HDR_SelectAuto                 = (void*)&HDR_SelectAuto,
#endif
    .HDR_Attention                  = (void*)&HDR_Attention,
    .TXT_NULLSTR                    = (void*)&TXT_NULLSTR,
    .TXT_TurnKeyOn                  = (void*)&TXT_TurnKeyOn,
    .TXT_TurnKeyOff                 = (void*)&TXT_TurnKeyOff, 
    .TXT_TurnKeyOff_Remove          = (void*)&TXT_TurnKeyOff_Remove,
    .TXT_DoNotStartEngine           = (void*)&TXT_DoNotStartEngine,
    .TXT_UploadComplete             = (void*)&TXT_UploadComplete,
    .TXT_PrepareDownload            = (void*)&TXT_PrepareDownload,
    .TXT_ProgramPCMOnly             = (void*)&TXT_ProgramPCMOnly,
    .TXT_DownloadComplete           = (void*)&TXT_DownloadComplete,
    .TXT_UnlockingECU               = (void*)&TXT_UnlockingECU,
    .TXT_ECMStock                   = (void*)&TXT_ECMStock,
    .TXT_PleaseWait                 = (void*)&TXT_PleaseWait,
    .TXT_SearchFiles                = (void*)&TXT_SearchFiles,
    .TXT_SelectAFile                = (void*)&TXT_SelectAFile,
    .TXT_PressWheelToSelect         = (void*)&TXT_PressWheelToSelect,
    .TXT_TurnWheelToScroll          = (void*)&TXT_TurnWheelToScroll,
    .TXT_ProcessFile                = (void*)&TXT_ProcessFile,
    .TXT_PressWheelToSelectUnSelect = (void*)&TXT_PressWheelToSelectUnSelect,
    .TXT_PressESCToSave             = (void*)&TXT_PressESCToSave,
    .TXT_SelectPID                  = (void*)&TXT_SelectPID,
    .TXT_NoPIDs                     = (void*)&TXT_NoPIDs,
    .TXT_StartEngineAndPrepare      = (void*)&TXT_StartEngineAndPrepare,
    .TXT_CompleteStopVehicle        = (void*)&TXT_CompleteStopVehicle,
    .TXT_ClearDTCSuccessful         = (void*)&TXT_ClearDTCSuccessful,
    .TXT_DLF_Files                  = (void*)&TXT_DLF_Files,
    .TXT_NoDTCFound                 = (void*)&TXT_NoDTCFound,
    .HDR_EstimatedMPHTime           = (void*)&HDR_EstimatedMPHTime,
    .HDR_ReturnVehicleToStock       = (void*)&HDR_ReturnVehicleToStock,
    .TXT_ReturnStockMsg1            = (void*)&TXT_ReturnStockMsg1,
    .TXT_ReturnStockMsg2            = (void*)&TXT_ReturnStockMsg2,
    .TXT_ReturnStockMsg3            = (void*)&TXT_ReturnStockMsg3,
    .TXT_ProgramWithStrategy        = (void*)&TXT_ProgramWithStrategy,
    .TXT_ProgramWithCustom          = (void*)&TXT_ProgramWithCustom,
    .TXT_On                         = (void*)&TXT_On,
    .TXT_Off                        = (void*)&TXT_Off,
    .TXT_StandbyFlash               = (void*)&TXT_StandbyFlash,
    .TXT_StandbyOff                 = (void*)&TXT_StandbyOff,
    .TXT_SetupECUFail               = (void*)&TXT_SetupECUFail,
    .TXT_DownloadFail               = (void*)&TXT_DownloadFail,
    .HDR_StockfileUpdated           = (void*)&HDR_StockfileUpdated,
    .TXT_ReuploadStockfile          = (void*)&TXT_ReuploadStockfile,
    .TXT_DeviceUnlocked             = (void*)&TXT_DeviceUnlocked,
    .TXT_PressKeyRetryProgram       = (void*)&TXT_PressKeyRetryProgram,
    .TXT_OS_Mismatch_1              = (void*)&TXT_OS_Mismatch_1,
    .TXT_OS_Mismatch_2              = (void*)&TXT_OS_Mismatch_2,
    .TXT_OS_Mismatch_3              = (void*)&TXT_OS_Mismatch_3,
    .TXT_OS_Mismatch_4              = (void*)&TXT_OS_Mismatch_4,
    .TXT_OS_Mismatch_5              = (void*)&TXT_OS_Mismatch_5,
    .TXT_TCM_Mismatch_1             = (void*)&TXT_TCM_Mismatch_1,
    .TXT_Load_DLF_Files             = (void*)&TXT_Load_DLF_Files,
    .TXT_PressESCGoBack             = (void*)&TXT_PressESCGoBack,
    .TXT_SetWeight                  = (void*)&TXT_SetWeight,
    .TXT_WheelResetEscExit          = (void*)&TXT_WheelResetEscExit,
    .TXT_ReadingOptions             = (void*)&TXT_ReadingOptions,
    .TXT_UploadSuccessful           = (void*)&TXT_UploadSuccessful,
    .TXT_UploadFailed               = (void*)&TXT_UploadFailed,
    .TXT_UploadAborted              = (void*)&TXT_UploadAborted,
    .TXT_ClearDTCFail               = (void*)&TXT_ClearDTCFail,
    .TXT_ReadDTCFail                = (void*)&TXT_ReadDTCFail,
    .TXT_ContactPhoneWeb            = (void*)&TXT_ContactPhoneWeb,
    .TXT_DefaultOptions             = (void*)&TXT_DefaultOptions,
    .TXT_SavedOptions               = (void*)&TXT_SavedOptions,
    .TXT_NoInfo                     = (void*)&TXT_NoInfo,
    .TXT_AdjustOption               = (void*)&TXT_AdjustOption,
    .TXT_SkipOption                 = (void*)&TXT_SkipOption,
    .TXT_KeepPreviousOption         = (void*)&TXT_KeepPreviousOption,
    .TXT_Backlight                  = (void*)&TXT_Backlight,
    .TXT_Audio                      = (void*)&TXT_Audio,
    .TXT_Contrast                   = (void*)&TXT_Contrast,
    .TXT_Sounds_USB_pwr             = (void*)&TXT_Sounds_USB_pwr,
    .TXT_SelectOption               = (void*)&TXT_SelectOption,
    .TXT_ConfigAnalog               = (void*)&TXT_ConfigAnalog,
    .TXT_StartMonitor               = (void*)&TXT_StartMonitor,
    .TXT_SetupMonitor               = (void*)&TXT_SetupMonitor,
    .TXT_Playback                   = (void*)&TXT_Playback,
    .TXT_RecordLog                  = (void*)&TXT_RecordLog,
    .TXT_DataViewFormat             = (void*)&TXT_DataViewFormat,
    .TXT_SetDataViewFormat          = (void*)&TXT_SetDataViewFormat,
    .TXT_Yes                        = (void*)&TXT_Yes,
    .TXT_No                         = (void*)&TXT_No,
    .TXT_RecordMonitorData          = (void*)&TXT_RecordMonitorData,
    .TXT_CommunicationLost          = (void*)&TXT_CommunicationLost,
    .TXT_ResumeDatalog              = (void*)&TXT_ResumeDatalog,
    .TXT_Exit                       = (void*)&TXT_Exit,
    .TXT_CommunicationFail          = (void*)&TXT_CommunicationFail,
    .TXT_CheckConnectionAndEngine   = (void*)&TXT_CheckConnectionAndEngine,
    .TXT_InitializeMonitor          = (void*)&TXT_InitializeMonitor,
    .TXT_StartEngine                = (void*)&TXT_StartEngine,
    .TXT_Channel1                   = (void*)&TXT_Channel1,
    .TXT_Channel2                   = (void*)&TXT_Channel2,
    .TXT_SelectEquation             = (void*)&TXT_SelectEquation,
    .TXT_SelectEquationNote1        = (void*)&TXT_SelectEquationNote1,
    .TXT_SelectEquationNote2        = (void*)&TXT_SelectEquationNote2,
    .TXT_EnterValueA                = (void*)&TXT_EnterValueA,
    .TXT_EnterValueB                = (void*)&TXT_EnterValueB,
    .TXT_LiveLinkActivated          = (void*)&TXT_LiveLinkActivated,
    .TXT_CANDetected                = (void*)&TXT_CANDetected,
    .TXT_SCPDetected                = (void*)&TXT_SCPDetected,
    .TXT_VPWDetected                = (void*)&TXT_VPWDetected,
    .TXT_AlreadyStock               = (void*)&TXT_AlreadyStock,
    .TXT_UpdateTuneRevision         = (void*)&TXT_UpdateTuneRevision,
    .TXT_InitFileSystem             = (void*)&TXT_InitFileSystem,
    .TXT_InvalidFileSystem          = (void*)&TXT_InvalidFileSystem,
    .TXT_ReloadFileSystem           = (void*)&TXT_ReloadFileSystem,
    .TXT_Engine_DL                  = (void*)&TXT_Engine_DL,
    .TXT_Transmission_DL            = (void*)&TXT_Transmission_DL,
    .TXT_Auto_Upload                = (void*)&TXT_Auto_Upload,
    .TXT_Calc_Checksum              = (void*)&TXT_Calc_Checksum,
    .TXT_Invalid                    = (void*)&TXT_Invalid,
    .TXT_Blank                      = (void*)&TXT_Blank,
    .TXT_Unknown                    = (void*)&TXT_Unknown,
    .TXT_No_Strategy                = (void*)&TXT_No_Strategy,
    .TXT_No_OSPartnumber            = (void*)&TXT_No_OSPartnumber,
    .TXT_ECU_CAL_DL                 = (void*)&TXT_ECU_CAL_DL,
    .TXT_ECU_FULL_DL                = (void*)&TXT_ECU_FULL_DL,
    .TXT_TCU_CAL_DL                 = (void*)&TXT_TCU_CAL_DL,
    .TXT_TCU_FULL_DL                = (void*)&TXT_TCU_FULL_DL,

#ifdef BANKS_ENG                       
    .TXT_BANKS_Contact_Literal      = (void*)&TXT_BANKS_Contact_Literal,
    .TXT_BANKS_Disclaimer           = (void*)&TXT_BANKS_Disclaimer,
    .TXT_BANKS_Disclaimer_2         = (void*)&TXT_BANKS_Disclaimer_2,
    .TXT_BANKS_Yes_No               = (void*)&TXT_BANKS_Yes_No,
    .TXT_BANKS_Key_ON               = (void*)&TXT_BANKS_Key_ON,
    .TXT_BANKS_Key_OFF              = (void*)&TXT_BANKS_Key_OFF,
    .TXT_BANKS_Key_OFF_ESC          = (void*)&TXT_BANKS_Key_OFF_ESC,
    .TXT_BANKS_Scroll               = (void*)&TXT_BANKS_Scroll,
    .TXT_BANKS_Begin_Program        = (void*)&TXT_BANKS_Begin_Program,
    .TXT_BANKS_Cancel_Program       = (void*)&TXT_BANKS_Cancel_Program,
    .TXT_BANKS_Load_StockText       = (void*)&TXT_BANKS_Load_StockText,
    .TXT_NoDTCInfo                  = (void*)&TXT_NoDTCInfo,
    .TXT_BANKS_ENT_ESC              = (void*)&TXT_BANKS_ENT_ESC,
    .TXT_BANKS_Contact              = (void*)&TXT_BANKS_Contact,
    .TXT_BANKS_Backlight            = (void*)&TXT_BANKS_Backlight,  
    .TXT_BANKS_ENT_Description      = (void*)&TXT_BANKS_ENT_Description,
    .TXT_BANKS_TurnKeyOn_No_ENT     = (void*)&TXT_BANKS_TurnKeyOn_No_ENT,
#else
    .TXT_ContactName                = (void*)&TXT_ContactName,
    .TXT_ContactWeb                 = (void*)&TXT_ContactWeb,
    .TXT_ContactPhone               = (void*)&TXT_ContactPhone,    
    .TXT_SCT_Disclaimer             = (void*)&TXT_SCT_Disclaimer,
    .TXT_SCT_Accept                 = (void*)&TXT_SCT_Accept,
    .TXT_TurnKey_ON                 = (void*)&TXT_TurnKey_ON,
    .TXT_SCT_SEL_CAN                = (void*)&TXT_SCT_SEL_CAN,
    .TXT_Load_StockText             = (void*)&TXT_Load_StockText,
    .TXT_BATTERY_LOW                = (void*)&TXT_BATTERY_LOW,

#endif                               
    .TXT_MemStorage                 = (void*)&TXT_MemStorage,
    .TXT_SetupDevice                = (void*)&TXT_SetupDevice,
    .TXT_UNLOCKPleaseWait           = (void*)&TXT_UNLOCKPleaseWait,
    .FDR_ExitKeyMsg                 = (void*)&FDR_ExitKeyMsg,
    .FDR_ExitBackKeyMsg             = (void*)&FDR_ExitBackKeyMsg,
    .FDR_ExitDoneKeyMsg             = (void*)&FDR_ExitDoneKeyMsg,
    .FDR_DoNotUnplug                = (void*)&FDR_DoNotUnplug,
    .FDR_PressESCExit               = (void*)&FDR_PressESCExit,
#ifdef BANKS_ENG
    .FDR_SelectMsg                  = (void*)&FDR_SelectMsg,
    .FDR_ESCExitMsg                 = (void*)&FDR_ESCExitMsg,
    .FDR_ContinueMsg                = (void*)&FDR_ContinueMsg,
#else
#endif
    .FDR_ContinueKeyMsg             = (void*)&FDR_ContinueKeyMsg,
    .FDR_SelectResume               = (void*)&FDR_SelectResume,
};

//------------------------------------------------------------------------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------------------------------------------------------------------------
LANGUAGE    *Language   = (LANGUAGE*)&Language_English;

//------------------------------------------------------------------------------------------------------------------------------------------------
// Set language
// Input:   LANGUAGE_TYPE   l       (LANGUAGE_EN, LANGUAGE_ES)
// Engineer: Quyen Leba
// Date: Mar 05, 2008
//------------------------------------------------------------------------------------------------------------------------------------------------
void Language_Set(LANGUAGE_TYPE l)
{
    switch(l)
    {
      case LANGUAGE_EN:
        Language = (LANGUAGE*)&Language_English;
        break;
      case LANGUAGE_ES:
        Language = (LANGUAGE*)&Language_English;
        break;
      default:
        Language = (LANGUAGE*)&Language_English;
        break;
    }
}

    
    
//------------------------------------------------------------------------------------------------------------------------------------------------
// EOF
//------------------------------------------------------------------------------------------------------------------------------------------------
