/**
  **************** (C) COPYRIGHT 2011 SCT Performance, LLC *********************
  * File Name          : port.h
  * Device / System    : SCT products using the "CommonCode" platform
  * Author             : Quyen Leba
  *
  * Version            : 1 
  * Date               : 05/02/2011
  * Description        : 
  *                    : 
  *
  *
  * History            : 05/02/2011 M. Davis
  *                    :   Add SCT Copyright header,
  *
  ******************************************************************************
  */

#ifndef __PORT_H
#define __PORT_H

#define __MARRIED_LOCK_

//use this so JLink can run
#ifdef  __X3P_DEBUG__           //this symbol defined in IAR options under C/C++ Compiler > Preprocessor
    #define __DEBUG_JTAG_
#endif

#define EXT extern

//------------------------------------------------------------------------------------------------------------------------------------------------
// Define Device Platform
//------------------------------------------------------------------------------------------------------------------------------------------------
#define DP_X3P              1           //1 = use this device


//------------------------------------------------------------------------------------------------------------------------------------------------
// Define Device Type
//------------------------------------------------------------------------------------------------------------------------------------------------
#define REGLW   0x00        // 00 -  Regular LiveWire
#define FLEETFD 0x01        // 01 -  FLEET FORD device
#define FLEETGM 0x02        // 02 -  FLEET GM device
#define B64LW   0x10        // 16 -  Basic 6.4L LiveWire
#define E64LW   0x20        // 32 -  Extreme!! ARRRRGGG! 6.4L LiveWire
#define O64LW   0x30        // 48 -  Extreme!! Offroad 6.4L LiveWire
#define REGX3   0x40        // 64 -  X3 & X3P Power Flash - Binary and Strategy
#define ECONO   0x50        // 80 -  X3 Economizer X3 & X3P
#define BANKS   0x60        // 96 -  BANKS ENGINEERING Ford
#define BINX3   0X70        // 112 - Binary flash-only X3 & X3P
#define GMX3    0x80        // 128 - GM X3 & X3P
#define BX364   0x90        // 144 - Basic 6.4L enabled X3P
#define BKS64   0xA0        // 160 - Banks 6.4L enabled X3P
#define GMBINX3 0XB0        // 176 - GM Binary flash-only X3P
#define EX364   0xC0        // 192 - Extreme!! ARRRRGGG! 6.4L X3P
#define OX364   0xD0        // 208 - Extreme!! Offroad 6.4L X3P
#define GMBANKS 0xE0        // 224 - BANKS GM
#define X3PEO   0xF0        // 240 - EO device for carb, 3015 strategy only

//!!!******* Define the DEVICE TYPE HERE **********!!!!

#define DEVICE_TYPE         GMX3
// If DEVICE_TYPE is "BANKS" or "GMBANKS", define "BANKS_ENG" to enable BANKS menus
#if (DEVICE_TYPE == BANKS) || (DEVICE_TYPE == GMBANKS)
    #define BANKS_ENG       1
#endif

//------------------------------------------------------------------------------------------------------------------------------------------------
// Define Vehicle Type, controlled by IAR under C/C++ Compiler > Preprocessor 
//------------------------------------------------------------------------------------------------------------------------------------------------
#ifdef __GM_DEVICE__
    #define GM              1
#else
    #define GM              1
#endif

//------------------------------------------------------------------------------------------------------------------------------------------------
// Define Market
//------------------------------------------------------------------------------------------------------------------------------------------------
// MARKET BYTE SETTING
#define US                  0x00 // 0 - US
#define AU                  0x01 // 1 - Australia
#define UK                  0x02 // 2 - UK
//!!!******* Define the MARKET TYPE HERE **********!!!!
#define MARKET_TYPE         US

//------------------------------------------------------------------------------------------------------------------------------------------------
// Define Language
//------------------------------------------------------------------------------------------------------------------------------------------------
//TODO
#define SUPPORT_MULTILANGUAGE   0

#define SUPPORT_ENGLISH         1
#if (SUPPORT_MULTILANGUAGE)
    #define SUPPORT_SPANISH     0
#endif

//------------------------------------------------------------------------------------------------------------------------------------------------
// Define Device Features
//------------------------------------------------------------------------------------------------------------------------------------------------
#define SUPPORT_CUSTOMTUNE      1

   #if ((DEVICE_TYPE != BINX3)||(DEVICE_TYPE != GMBINX3))
     #define SUPPORT_STRATEGYTUNE    1
   #endif

#endif

#if (__X3P_RELEASE__)
    //note: thumb mode enable through compiler
    #define __ENABLE_PROGRAMVEHICLE             1
    #define __ENABLE_DATALOG                    1                         
#else                    
    #if (__GM_PROGRAM__)
        #define __ENABLE_PROGRAMVEHICLE     1
    #else
        #define __ENABLE_PROGRAMVEHICLE     0
    #endif
    #if (__GM_DATALOG__)
        #define __ENABLE_DATALOG            1
    #else
        #define __ENABLE_DATALOG            0
    #endif
#endif

//------------------------------------------------------------------------------------------------------------------------------------------------
// EOF
//------------------------------------------------------------------------------------------------------------------------------------------------
