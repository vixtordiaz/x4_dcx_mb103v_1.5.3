/**
  **************** (C) COPYRIGHT 2011 SCT Performance, LLC *********************
  * File Name          : menus_tuneoption.c
  * Device / System    : SCT products using the "CommonCode" platform
  * Author             : Quyen Leba
  *
  * Version            : 1 
  * Date               : 05/02/2011
  * Description        : 
  *                    : 
  *
  *
  * History            : 05/02/2011 M. Davis
  *                    :   Add SCT Copyright header,
  *
  ******************************************************************************
  */

#include <board/genplatform.h>
#include <fs/genfs.h>
#include <common/statuscode.h>
#include <common/filechecksum.h>
#include "error.h"
#include "menus.h"
#include "X3_menus_helper.h"
#include "menus_tuneoption.h"

#define OFFSET4CHANGES_SIZE     200
#define CHANGEDOPTIONS_SIZE     50

typedef struct
{
    u8  Adjust_Point_Description[16];
    u32 Data_Block_offset;
}ADJUSTPOINT_INFO;

struct Option_Header_struct
{
    u8  OH_Identifier;
    u32 Header_length;
    u8  Num_of_Options;
}Option_Header;

struct Option_N_struct
{
    u8  Num_of_Adjust_Points_PerOPtions;
    u8  Default_Adjust_Point;
    u8  Option_Description[16];
    u32 Adjust_Header_offset;
}Option_Info;

struct Adjust_point_Header_struct
{
    u8  APH_Identifer;
    u32 Adjust_Header_length;
    u8  Num_Adjust_point_entries;
}AdjustPoint_Header;

//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
u8 Get_Option_Header(F_FILE *fp);
u8 Get_AdjustPoint_Header(F_FILE *fp);
u8 Get_Option_Info(F_FILE *fp, int index);
u8 Get_AdjustPoint_Info(F_FILE *fp, ADJUSTPOINT_INFO *AdjustPoints_forOption);

//------------------------------------------------------------------------------
// Inputs:  u8 *optionfilename
//          u8  ecm_index   (used to specify x of SavedOpts_ECU#.opt)
//                          (normally, 0:pcm and 1:tcm)
// Output:  bool *is_option_use (TRUE if user select to use option)
// Return:  u8  status
//------------------------------------------------------------------------------
u8 RunOptionsMenu(u8 *optionfilename, u8 ecm_index, bool *is_option_use)
{
    ADJUSTPOINT_INFO *AdjustPoints_ThisOption;
    F_FILE  *optionfile         = NULL;
    F_FILE  *newdefault         = NULL;
    u32     i, Option_index     = 0;
    u32     filechecksum        = 0;
    u8      *ptr                = 0;
    u32     *offset_4_changes;
    u8      strbuf[40];
    u8      *changed_options;
    u8      defile[21];
    u8      Points_index, status=99, error=0;
    u8      databuf[50];
    u8      savedoptionfilename[40];
    u8      index;
    u8      choice              = 0;
    bool    keepprevopts        = FALSE;
    bool    new_saved_option    = FALSE;

    *is_option_use = FALSE;
    if (ecm_index >= ECM_MAX_COUNT)
    {
        //TODOQ: in case should not happen; for now, we only do PCM & TCM
        return S_INPUT;
    }

    strcpy((char*)savedoptionfilename,"SavedOpts_ECU#.opt");
    savedoptionfilename[13] = '0' + ecm_index;
    
    offset_4_changes = (u32*)__malloc(OFFSET4CHANGES_SIZE);
    if(!offset_4_changes)
    {
        ERROR_Message(MALLOC);
        return(S_MALLOC);
    }
    changed_options = __malloc (CHANGEDOPTIONS_SIZE);
    if(!changed_options)
    {
        ERROR_Message(MALLOC);
        __free(offset_4_changes);
        return(S_MALLOC);
    }
    
    AdjustPoints_ThisOption = __malloc(20*150);
    if(!AdjustPoints_ThisOption)
    {
        ERROR_Message(MALLOC);
        __free(offset_4_changes);
        __free(changed_options);
        return(S_MALLOC);
    }
    
    strncpy((void*)defile, (void*)optionfilename, 21);        
    
    // Open saved options file if present
    newdefault = __fopen((char*)savedoptionfilename,"rb");
    if(newdefault == NULL)
    {
        new_saved_option = TRUE;
        status = 0;                                         // No file saved
    }
    else
    {
        memset(databuf,0,50);
        f_read(databuf,1,20,newdefault);                    // Read back 20 byte header
        // containing CRC of option file (first 4 bytes)
        __fclose(newdefault);
        
        filechecksum |= databuf[0]<<24;
        filechecksum |= databuf[1]<<16;
        filechecksum |= databuf[2]<<8;
        filechecksum |= databuf[3];                         // get checksum
        
        if(filechecksum == 0)
        {
            status = 0;                                     // No options have been saved
        }
        else if(fileCheckSumCheck(defile, filechecksum) != S_SUCCESS)
        {
            // checksum doesnt match, options file must have changed, force default
            status = 0;
        }
        else
        {
            //checksum matched
            status = 1;
        }
    }
    
    MENUITEMSTR* mitem;
    mitem = __malloc(3*sizeof(MENUITEMSTR));
#ifdef BANKS_ENG
    ShowMessage(NOKEY,NOAUDIO,(void*)*Language->HDR_UserAdjOpts,
                (void*)*Language->FDR_SelectMsg,
                (void*)*Language->TXT_NULLSTR,
                (void*)*Language->TXT_NULLSTR,
                (void*)*Language->TXT_NULLSTR,CLEAR_TOP);
#else
    ShowMessage(NOKEY,NOAUDIO,(void*)*Language->HDR_UserAdjustmentOptions,
                (void*)*Language->FDR_ContinueKeyMsg,
                (void*)*Language->TXT_NULLSTR,
                (void*)*Language->TXT_NULLSTR,
                (void*)*Language->TXT_NULLSTR,CLEAR_TOP);
#endif    
    
    if(status == 0)
    {
        // Put up go to User option question menu
        ParseMenuItemStr((u8*)*Language->TXT_AdjustOption, 0, mitem, 0);
        ParseMenuItemStr((u8*)*Language->TXT_SkipOption, 1, mitem, 0);
        if(Scroller(&choice, 0, mitem, 2, 2, 0, 8, 0, 32, NullHelper, NullHelper) == S_USERABORT)
        {
            choice = 1; // Skip options if user abort
        }
    }
    else if(status == 1)
    {
        // Put up go to User option question menu
        ParseMenuItemStr((void*)*Language->TXT_KeepPreviousOption, 0, mitem, 0);
        ParseMenuItemStr((void*)*Language->TXT_AdjustOption, 1, mitem, 0);
        ParseMenuItemStr((u8*)*Language->TXT_SkipOption, 2, mitem, 0);
        if(Scroller(&choice, 0, mitem, 3, 3, 0, 8, 0, 32, NullHelper, NullHelper) == S_USERABORT)
        {
            choice = 1; // Skip options if user abort
        }
        else
        {
            choice += 2;            // Offset these choices by 1
        }
    }
    __free(mitem);
    ShowMessage(NOKEY,NOAUDIO,(void*)*Language->HDR_UserAdjustmentOptions,
                (void*)*Language->TXT_NULLSTR,
                (void*)*Language->TXT_NULLSTR,
                (void*)*Language->TXT_PleaseWait,
                (void*)*Language->TXT_NULLSTR,CLEAR_TOP);
    
    if(choice == 1 || choice == 4)  // Skip options
    {
        *is_option_use = FALSE;
        status = S_SKIPOPTION;
        goto EXIT;
    }
    else if(choice == 2)            // Keep previous
    {
        *is_option_use = TRUE;
        status = S_SUCCESS;
        keepprevopts = TRUE;
        goto EXIT;
    }
    
    if (new_saved_option == TRUE)
    {
        newdefault = __fopen((char*)savedoptionfilename, "wb");
        if(newdefault != NULL)
        {
            memset(databuf,0,50);
            f_write(databuf,20,1,newdefault);   // Initialize the header
            __fclose(newdefault);
        }
        else
        {
            ERROR_Message(OPENFILE);
            __free(offset_4_changes);
            __free(AdjustPoints_ThisOption);
            return S_FAIL;
        }
    }
#ifdef BANKS_ENG    
    ShowMessage(NOKEY,NOAUDIO,(void*)*Language->HDR_UserAdjOpts,                // 3
                (void*)*Language->FDR_SelectMsg,
                (void*)*Language->TXT_NULLSTR,
                (void*)*Language->TXT_NULLSTR,
                (void*)*Language->TXT_NULLSTR,CLEAR_TOP);
#else   
    ShowMessage(NOKEY,NOAUDIO,(void*)*Language->HDR_UserAdjustmentOptions,      // 4
                (void*)*Language->FDR_ContinueKeyMsg,
                (void*)*Language->TXT_NULLSTR,
                (void*)*Language->TXT_NULLSTR,
                (void*)*Language->TXT_NULLSTR,CLEAR_TOP);
#endif
    if(status == 1)
    {
        // Ask to return to default, or use last changes? 
        mitem = __malloc(2*sizeof(MENUITEMSTR));          
        // Put up go to User option question menu
        ParseMenuItemStr((u8*)*Language->TXT_SavedOptions, 0, mitem, 0);
        ParseMenuItemStr((u8*)*Language->TXT_DefaultOptions, 1, mitem, 0);
        if(Scroller(&choice, 0, mitem, 2, 2, 0, 8, 0, 32, NullHelper, NullHelper) == S_USERABORT)
        {
            choice = 2; // Skip options if user abort
        }
        __free(mitem);
    }
    else
    {
        choice = 1;
    }
    
    if(choice == 1)
    {
        memset(changed_options, 'D',CHANGEDOPTIONS_SIZE);                                     //mark all of buffers at default
        memset(offset_4_changes, 0xff, OFFSET4CHANGES_SIZE);
    }
    else if(choice == 0)
    {
#ifdef BANKS_ENG
        ShowMessage(NOKEY,NOAUDIO,(void*)*Language->HDR_UserAdjOpts,
                    (void*)*Language->TXT_NULLSTR,
                    (void*)*Language->TXT_ReadingOptions,
                    (void*)*Language->TXT_NULLSTR,
                    (void*)*Language->TXT_NULLSTR,CLEAR_TOP); 
#else
        ShowMessage(NOKEY,NOAUDIO,(void*)*Language->HDR_UserAdjustmentOptions,
                    (void*)*Language->TXT_NULLSTR,
                    (void*)*Language->TXT_NULLSTR,
                    (void*)*Language->TXT_NULLSTR,
                    (void*)*Language->TXT_ReadingOptions,CLEAR_TOP); 
#endif
        
        newdefault = __fopen((char*)savedoptionfilename, "rb");
        if(newdefault != NULL)
        {
            f_seek(newdefault, 20, SEEK_SET);   // Seek past CRC
            if(f_read(changed_options,1,CHANGEDOPTIONS_SIZE,newdefault) != CHANGEDOPTIONS_SIZE)
            {
                error = 1;
            }
            else
            {   //get offsets for selctions
                //if(f_read(offset_4_changes,1,sizeof(offset_4_changes),newdefault) != sizeof(offset_4_changes))
                //offset_4_changes is a __malloc, OFFSET4CHANGES_SIZE
                if(f_read(offset_4_changes,1,OFFSET4CHANGES_SIZE,newdefault) != OFFSET4CHANGES_SIZE)
                {
                    error = 1;
                }
            }
            __fclose(newdefault);
        }
    }
    
    if(choice == 2) // User aborted
    {
        *is_option_use = FALSE;
        status = 2;
        goto EXIT; 
    }
    
#ifdef BANKS_ENG
    ShowMessage(NOKEY,NOAUDIO,(void*)*Language->HDR_UserAdjOpts,
                (void*)*Language->TXT_NULLSTR,
                (void*)*Language->TXT_NULLSTR,
                (void*)*Language->TXT_ReadingOptions,
                (void*)*Language->TXT_NULLSTR,CLEAR_TOP);        
#else
    ShowMessage(NOKEY,NOAUDIO,(void*)*Language->HDR_UserAdjustmentOptions,
                (void*)*Language->TXT_NULLSTR,
                (void*)*Language->TXT_NULLSTR,
                (void*)*Language->TXT_NULLSTR,
                (void*)*Language->TXT_ReadingOptions,CLEAR_TOP);        
#endif    
    if((optionfile = __fopen((void*)defile,"rb"))== NULL)   //open option's file
    {
        ERROR_Message(OPENFILE);
        status = S_FAIL;
        goto EXIT;
    }
    
    Points_index = 'D';     //set adjustment points index to default
    status  = Get_Option_Header(optionfile);
    
    //TODO: what this error?
    if(error)
    {
        ERROR_Message(READFILE);    //Let them we know we had an error, but continue
        memset(changed_options, 'D',CHANGEDOPTIONS_SIZE);   //mark all of buffers at default
        memset(offset_4_changes, 0xff, OFFSET4CHANGES_SIZE);
    }
    
    i=0;
    status = Get_AdjustPoint_Header(optionfile);
    if(status != S_SUCCESS)
    {
        goto EXIT;
    }
    
    while(1)
    {
#ifdef BANKS_ENG
        ShowMessage(NOKEY,NOAUDIO,(void*)*Language->HDR_UserAdjOpts,
                    (void*)*Language->FDR_ExitDoneKeyMsg,
                    (void*)*Language->TXT_NULLSTR,
                    (void*)*Language->TXT_NULLSTR,
                    (void*)*Language->TXT_NULLSTR,CLEAR_TOP);
#else
        ShowMessage(NOKEY,NOAUDIO,(void*)*Language->HDR_UserAdjustmentOptions,
                    (void*)*Language->FDR_ExitDoneKeyMsg,
                    (void*)*Language->TXT_NULLSTR,
                    (void*)*Language->TXT_NULLSTR,
                    (void*)*Language->TXT_NULLSTR,CLEAR_TOP);
#endif
        
        LCD_gputscpos(0,2,(void*)*Language->TXT_SelectOption);
        
        MENUITEMSTR* mitem;
        mitem = __malloc(Option_Header.Num_of_Options*sizeof(MENUITEMSTR));
        for(i=0;i<Option_Header.Num_of_Options;i++)
        {
            status = Get_Option_Info(optionfile, i);
            //TODO: this
            if(status != S_SUCCESS)
            {
                __free(mitem);
                goto EXIT;
                //break;
            }
            memcpy((void*)strbuf,(void*)Option_Info.Option_Description,16);
            strbuf[16] = 0;
            ParseMenuItemStr(strbuf, i, mitem, 0);
            //ParseMenuItemStr(Option_Info.Option_Description, i, mitem, 0);
        }
        if (status != S_SUCCESS)
        {
            __free(mitem);
            goto EXIT;
            //break;
        }
        
        status = Scroller(&index, 0, mitem, i, 3, 0, 8, 0, 32, NullHelper, NullHelper);
        
        __free(mitem);
        
        // user press ESC, does not mean abort, but done with adjustment
        if (status == S_USERABORT)
        {
            ShowMessage(NOKEY,NOAUDIO,(void*)*Language->HDR_UserAdjustmentOptions,
                        (void*)*Language->TXT_NULLSTR,
                        (void*)*Language->TXT_NULLSTR,
                        (void*)*Language->TXT_PleaseWait,
                        (void*)*Language->TXT_NULLSTR,CLEAR_TOP);
            
            status = S_SUCCESS;
            break;
        }
        
        Option_index = index;
        //status = Get_Option_Info(optionfile, Option_index);    //Get this options info
        status = Get_Option_Info(optionfile, index);    //Get this options info
        if(status != S_SUCCESS)
        {
            __free(mitem);
            goto EXIT;
            //break;
        }
        
#ifdef BANKS_ENG
        ShowMessage(NOKEY,NOAUDIO,(void*)*Language->HDR_UserAdjOpts,
                    (void*)*Language->FDR_ExitBackKeyMsg,
                    (void*)*Language->TXT_NULLSTR,
                    (void*)*Language->TXT_NULLSTR,
                    (void*)*Language->TXT_NULLSTR,CLEAR_TOP);
#else
        ShowMessage(NOKEY,NOAUDIO,(void*)*Language->HDR_UserAdjustmentOptions,
                    (void*)*Language->FDR_ExitBackKeyMsg,
                    (void*)*Language->TXT_NULLSTR,
                    (void*)*Language->TXT_NULLSTR,
                    (void*)*Language->TXT_NULLSTR,CLEAR_TOP);
#endif
        
        LCD_gputsrpos(0,24,Option_Info.Option_Description);
        
        status = Get_AdjustPoint_Info(optionfile,AdjustPoints_ThisOption);
        if(status != S_SUCCESS)
        {
            __free(mitem);
            goto EXIT;
            //break;
        }
        
        if(changed_options[Option_index] != 'D')    //Not default 'D'
        {
            Points_index = changed_options[Option_index];   //take new
            Option_Info.Default_Adjust_Point = Points_index;
        }
        else
        {
            Points_index = (Option_Info.Default_Adjust_Point);  //no selection made, use file default
        }
        
        
        mitem = __malloc(Option_Info.Num_of_Adjust_Points_PerOPtions*sizeof(MENUITEMSTR));
        for (i=0;i<Option_Info.Num_of_Adjust_Points_PerOPtions;i++)
        {
            memcpy((void*)strbuf,(void*)AdjustPoints_ThisOption[i].Adjust_Point_Description,16);
            ptr = (u8*)strchr((void*)strbuf,0x0A); // null terminate before -'s                        
            if(ptr)
            {
                *ptr = 0;
            }
            else
            {
                strbuf[16] = 0;
            }
            ParseMenuItemStr(strbuf, i, mitem, 0);
        }
        mitem[Points_index].checked[0] = STAR;     // set STAR
        
        status = Scroller(&index, 0, mitem, i, 3, 0, 8, 0, 32, NullHelper, NullHelper);
        
        __free(mitem);
        
        //not userabort, just go back to previous menu
        if (status == S_USERABORT)
        {
            continue;
        }
        
        //save selection
        Points_index = index;
        changed_options[Option_index] = Points_index;   //save new default
        //save offset to write new data to file
        offset_4_changes[Option_index] = AdjustPoints_ThisOption[Points_index].Data_Block_offset;
        Option_Info.Default_Adjust_Point = Points_index;
    }//while(1)...
EXIT:
    // Close options file
    if(optionfile != NULL)
    {
        __fclose(optionfile);
    }
    
    if ((status == S_SUCCESS) && (keepprevopts == FALSE))   //changes were good
    {
        newdefault = __fopen((char*)savedoptionfilename, "wb");
        if(newdefault != NULL)
        {
            fileCheckSumCalculate(defile,&filechecksum);
            
            databuf[0] = filechecksum>>24;
            databuf[1] = filechecksum>>16;
            databuf[2] = filechecksum>>8;
            databuf[3] = filechecksum;
            f_write(databuf,4,1,newdefault);
            
            f_seek(newdefault, 20, SEEK_SET);   // Seek past CRC
            f_write(changed_options, 1, CHANGEDOPTIONS_SIZE,newdefault);
            //f_write(offset_4_changes, 1, sizeof(offset_4_changes),newdefault);
            //offset_4_changes is a __malloc, OFFSET4CHANGES_SIZE
            f_write(offset_4_changes, 1, OFFSET4CHANGES_SIZE,newdefault);
            __fclose(newdefault);
            *is_option_use = TRUE;
        }
        else
        {
            ERROR_Message(OPENFILE);
            status = S_FAIL;
        }
    }
    
    __free(AdjustPoints_ThisOption);
    __free(offset_4_changes);   
    __free(changed_options);
    
    if(status == S_SUCCESS)
    {
        return S_SUCCESS;
    }
    else if(status == S_SKIPOPTION)
    {
        return S_USERABORT;
    }
    else
    {
        return S_FAIL;
    }
}

//------------------------------------------------------------------------------
// This routine gets the option header from the Option file
// Header Format:
//      Header Identifier               = 1 byte    (char)  = 0x0A
//      Total length of header block    = 4 bytes   (int)
//      Number of available options     = 1 byte    (char)
// Return:  status (S_FAIL,S_SUCCESS)
// Engineer: Rhonda Burns
// Date: 6/15/06
//------------------------------------------------------------------------------
u8 Get_Option_Header(F_FILE *fp)
{
    u8 temp[4], *p;
    
    p = temp;
    
    //Get file identifier
    if(f_read(&Option_Header.OH_Identifier,1,sizeof(Option_Header.OH_Identifier), fp)< 1)
    {
        ERROR_Message(READFILE);
        return S_FAIL;
    }
    
    //Get header length and put in correct order
    if(f_read(p,1,sizeof(temp), fp)< 1)
    {
        ERROR_Message(READFILE);
        return S_FAIL;
    }
    
    Option_Header.Header_length = temp[0];
    Option_Header.Header_length = ((Option_Header.Header_length<<8)|temp[1]);
    Option_Header.Header_length = ((Option_Header.Header_length<<8)|temp[2]);
    Option_Header.Header_length = ((Option_Header.Header_length<<8)|temp[3]);
    
    //number of available options
    if(f_read(&Option_Header.Num_of_Options,1,sizeof(Option_Header.Num_of_Options), fp)< 1)
    {
        ERROR_Message(READFILE);
        return S_FAIL;
    }
    
    return S_SUCCESS;
}

//------------------------------------------------------------------------------
// This routine gets the option information for each option in the Option file
// Option Format:
//      Number of adjustment points for option      = 1 byte    (char)
//      Default adjustment point                    = 1 byte    (char)
//      Option description                          = 16 bytes  (char)
//      Offset for adjustment point headers         = 4 bytes   (int)
// Return:  u8  status      (S_FAIL,S_SUCCESS)
// Engineer: Rhonda Burns
// Date: 6/15/06
//------------------------------------------------------------------------------
u8 Get_Option_Info(F_FILE *fp, int index)
{
    u8  temp[sizeof(Option_Info.Adjust_Header_offset)];
    u8  *p;
    u32 offset, OptionheaderSize, OptioninfoSize;
    
    p = temp;
    
    //Calculate new offset
    OptionheaderSize = sizeof(Option_Header.OH_Identifier)+sizeof(Option_Header.Header_length)+
        sizeof(Option_Header.Num_of_Options);
    
    OptioninfoSize = ((sizeof(Option_Info.Num_of_Adjust_Points_PerOPtions)+
                       sizeof(Option_Info.Default_Adjust_Point)+sizeof(Option_Info.Option_Description)+
                           sizeof(Option_Info.Adjust_Header_offset))*index);
    
    
    offset = OptionheaderSize + OptioninfoSize;                 //new offset
    
    if(f_seek(fp,offset, SEEK_SET) != 0)                        //move to correct posistion in file
    {
        ERROR_Message(SEEKFILE);
        return S_FAIL;
    }
    //get number of adjustment points
    if(f_read(&Option_Info.Num_of_Adjust_Points_PerOPtions,1,
              sizeof(Option_Info.Num_of_Adjust_Points_PerOPtions), fp)< 1)
    {
        ERROR_Message(READFILE);
        return S_FAIL;
    }
    //get default adjustment point
    if(f_read(&Option_Info.Default_Adjust_Point,1,
              sizeof(Option_Info.Default_Adjust_Point), fp)< 1)
    {
        ERROR_Message(READFILE);
        return S_FAIL;
    }
    //get option description
    if(f_read(&Option_Info.Option_Description,1,
              sizeof(Option_Info.Option_Description), fp)< 1)
    {
        ERROR_Message(READFILE);
        return S_FAIL;
    }
    
    if(f_read(p,1,sizeof(temp), fp)< 1)                                //get offset to adjustment point headers
    {
        ERROR_Message(READFILE);
        return S_FAIL;
    }
    Option_Info.Adjust_Header_offset = temp[0];                    //put offset in correct format
    Option_Info.Adjust_Header_offset = ((Option_Info.Adjust_Header_offset<<8)|temp[1]);
    Option_Info.Adjust_Header_offset = ((Option_Info.Adjust_Header_offset<<8)|temp[2]);
    Option_Info.Adjust_Header_offset = ((Option_Info.Adjust_Header_offset<<8)|temp[3]);
    
    return S_SUCCESS;
}

//------------------------------------------------------------------------------
// This routine gets the ajustment point header out of the Option file
// Adjustment point header format:
//      Header Indentifier                  = 1 byte    (char)  = 0x0B
//      Length of Header block              = 4 bytes   (int)
//      Number of adjustment point entries  = 1 byte    (char)
// Return:  status      (S_FAIL, S_SUCCESS)
// Engineer: Rhonda Burns
// Date: 6/15/06
//------------------------------------------------------------------------------
u8 Get_AdjustPoint_Header(F_FILE *fp)
{
    u8 temp[sizeof(AdjustPoint_Header.Adjust_Header_length)];
    u8 *p;
    
    p = temp;
    
    if (f_seek(fp,Option_Header.Header_length, SEEK_SET) != 0)    //use option header length to get to point header
    {
        ERROR_Message(SEEKFILE);
        return S_FAIL;
    }
    //get header indentifer
    if(f_read(&AdjustPoint_Header.APH_Identifer,1,
              sizeof(AdjustPoint_Header.APH_Identifer), fp)< 1)
    {
        ERROR_Message(READFILE);
        return S_FAIL;
    }
    
    if(f_read(p,1,sizeof(temp), fp)< 1)                                    //get length of point header
    {
        ERROR_Message(READFILE);
        return S_FAIL;
    }
    
    AdjustPoint_Header.Adjust_Header_length = temp[0];                //put in correct format
    AdjustPoint_Header.Adjust_Header_length = ((AdjustPoint_Header.Adjust_Header_length<<8)|temp[1]);
    AdjustPoint_Header.Adjust_Header_length = ((AdjustPoint_Header.Adjust_Header_length<<8)|temp[2]);
    AdjustPoint_Header.Adjust_Header_length = ((AdjustPoint_Header.Adjust_Header_length<<8)|temp[3]);
    
    //grab number of point entries, I don't use this
    if((f_read(&AdjustPoint_Header.Num_Adjust_point_entries,1,
               sizeof(AdjustPoint_Header.Num_Adjust_point_entries),fp)) < 1)
    {
        ERROR_Message(READFILE);
        return S_FAIL;
    }
    
    return S_SUCCESS;
}

//------------------------------------------------------------------------------
// This routine gets the adjustment point description and the offset for the
// beginning of the data blocks. All offsets are from the start of the file
// Adjustment point Format:
//      Adjustment point description        = 60 bytes  (char)
//      offset for start of data blocks     = 4 bytes   (int)
// Engineer: Rhonda Burns
// Date: 6/15/06
//------------------------------------------------------------------------------
u8 Get_AdjustPoint_Info(F_FILE *fp, ADJUSTPOINT_INFO *AdjustPoints_forOption)
{
    u8  temp[sizeof(AdjustPoints_forOption[0].Data_Block_offset)];
    u8  i=0, c, t, x;
    u8  *p, tempBuf[34];
    int blanks = 26;
    
    p = temp;
    
    //offset in file for first adjustment point off by size
    //of adjustment header so we correct it here
    Option_Info.Adjust_Header_offset += 
        (sizeof(AdjustPoint_Header.APH_Identifer) +
         sizeof(AdjustPoint_Header.Adjust_Header_length) +
         sizeof(AdjustPoint_Header.Num_Adjust_point_entries));
    
    //move to correct posistion in file
    if (f_seek(fp,(Option_Info.Adjust_Header_offset), SEEK_SET) != 0)
    {
        ERROR_Message(SEEKFILE);
        return S_FAIL;
    }
    
    //use for loop to get each adjustment point
    for(i=0; i< Option_Info.Num_of_Adjust_Points_PerOPtions; i++)
    {
        if(f_read(tempBuf,1,sizeof(tempBuf), fp)< 1)        //grab only 34 bytes of description
        {
            ERROR_Message(READFILE);
            return S_FAIL;
        }
        
        //start at length of option description, then grab only the
        c = 0;//(sizeof(Option_Info.Option_Description));        //start at length of option description
        
        t = 0;
        
        memset(AdjustPoints_forOption[i].Adjust_Point_Description,0,16);    //put spaces in buffer memory
        
        //grab only the adjustment point description
        for(x=(c); x< 34; x++)
        {                                                                //And make sure it's a valid character
            if((tempBuf[x] == 0x20) || ((0x2C< tempBuf[x])&&(tempBuf[x] < 0x3A))||((0x40< tempBuf[x])&&(tempBuf[x] < 0x5B))||
               ((0x60< tempBuf[x])&&(tempBuf[x] < 0x7B)))
            {
                AdjustPoints_forOption[i].Adjust_Point_Description[t] = tempBuf[x];
                t++;
            }
            else if(tempBuf[x] == 0x0A)
            {
                AdjustPoints_forOption[i].Adjust_Point_Description[t] = 0;
                break;
            }
        }
        
        
        if(f_seek(fp, blanks, SEEK_CUR) != 0)                //Skip Blank spaces, but if size changes this will too
        {
            ERROR_Message(SEEKFILE);
            return S_FAIL;
        }
        
        memset(temp,0,4);                                            //clear buffer memory
        
        if(f_read(p,1,sizeof(temp), fp)< 1)                    //get the data block offset
        {
            ERROR_Message(READFILE);
            return S_FAIL;
        }
        
        AdjustPoints_forOption[i].Data_Block_offset = temp[0];    //put offset in correct format
        AdjustPoints_forOption[i].Data_Block_offset = ((AdjustPoints_forOption[i].Data_Block_offset<<8)|temp[1]);
        AdjustPoints_forOption[i].Data_Block_offset = ((AdjustPoints_forOption[i].Data_Block_offset<<8)|temp[2]);
        AdjustPoints_forOption[i].Data_Block_offset = ((AdjustPoints_forOption[i].Data_Block_offset<<8)|temp[3]);
    }
    return S_SUCCESS;
}
