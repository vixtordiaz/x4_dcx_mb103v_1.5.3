/**
  **************** (C) COPYRIGHT 2011 SCT Performance, LLC *********************
  * File Name          : version.c
  * Device / System    : SCT products using the "CommonCode" platform
  * Author             : Quyen Leba
  *
  * Version            : 1 
  * Date               : 05/02/2011
  * Description        : 
  *                    : 
  *
  *
  * History            : 05/02/2011 M. Davis
  *                    :   Add SCT Copyright header,
  *
  ******************************************************************************
  */

#include "source.h"
#include "version.h"
#include "app.h"
#include "X3_menus_helper.h"
#include "port.h"

// Version number formatting:
// Because of new firmware update handling code, firmware version converted to an interger and saved into settings. 
// Settings.firmware version is compared to the firmware version found in version.c
//
// Each individual digit must be 0 - 9, do not use HEX number representations
// Do not increase the 1st and 2nd numbers past 9.
// Do not increase the last number past 3 digits.

u8      Firmware_Version[FWverSize]         = {'1','.','2','.','1','1','6','.','2',0};
//u8      Firmware_Version[FWverSize]         = {'1','.','2','.','1','1','6',0};

// NOTE:  Tune_Version is read from FLASH, using file:   "tune_version.txt"
u8      Tune_Version[10]             = {'1','.','0','.','9',0};

//------------------------------------------------------------------------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------------------------------------------------------------------------
#define MAX_VERSION_STRING          12  //read max 12 bytes from tuneversion.txt

//------------------------------------------------------------------------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------------------------------------------------------------------------
//u8      Version_ToInt(u8 *ver_str, u32 *ver_int);
//u8      Version_CompareTuneVersion(s8 *result);
//u8      Version_CompareFirmwareVersion(s8 *result);

/*
//------------------------------------------------------------------------------------------------------------------------------------------------
// Input:   u8  *ver_str            version in string
// Output:  u32 *ver                u32 ver[3]
// Return:  u8  status              S_SUCCESS, S_FAIL
// Engineer: Quyen Leba
// Note: Assume version string in format M.B.R
//------------------------------------------------------------------------------------------------------------------------------------------------
u8 Version_ToInt(u8 *ver_str, u32 *ver_int)
{
    u8  i,j;
    u16 ver[10] = {0,0,0,0};    //only use 3

    j=0;
    for(i=0;i<MAX_VERSION_STRING;i++)
    {
        if (ver_str[i] == 0)
        {
            break;
        }
        else if (ver_str[i] == '.')
        {
            j++;
        }
        else if (ver_str[i] >= '0' && ver_str[i] <= '9')
        {
            ver[j] *= 10;
            ver[j] += ver_str[i] - '0';
        }
        else
        {
            break;
        }
    }

    if (j != 2)
    {
        return S_FAIL;
    }

    *ver_int = ver[0] << 24;
    *ver_int |= ver[1] << 16;
    *ver_int |= ver[2];

    return S_SUCCESS;
}
*/

/*
//------------------------------------------------------------------------------------------------------------------------------------------------
// Compare tuneversion.txt to firmware default tune version
// Output:  s8  *result     (VERSION_OLD,VERSION_CURRENT,VERSION_NEW)
// Return:  u8  status      (S_SUCCESS,S_FAIL)
// Engineer: Quyen Leba
// Note: assume version in format M.
//------------------------------------------------------------------------------------------------------------------------------------------------
u8 Version_CompareTuneVersion(s8 *result)
{
    F_FILE  *f;
    u8      ver_str[MAX_VERSION_STRING+2];
    u32     ver1;
    u32     ver2;

    f = __fopen("/tuneversion.txt","rb");
    if (f == NULL)
    {
        return S_FAIL;
    }

    memset((void*)ver_str,0,MAX_VERSION_STRING+2);
    f_read((void*)ver_str,1,MAX_VERSION_STRING,f);
    __fclose(f);

    if (Version_ToInt(ver_str, &ver1) != S_SUCCESS)
    {
        return S_FAIL;
    }
    if (Version_ToInt(Tune_Version, &ver2) != S_SUCCESS)
    {
        return S_FAIL;
    }

    if (ver1 == ver2)
    {
        *result = VERSION_CURRENT;
    }
    else if (ver1 > ver2)
    {
        *result = VERSION_NEW;
    }
    else
    {
        *result = VERSION_OLD;
    }

    return S_SUCCESS;
}
*/

/*
//------------------------------------------------------------------------------------------------------------------------------------------------
// Compare firmware with last updated firmware version store in settings
// Output:  s8  *result     (VERSION_BACKDATED,VERSION_CURRENT,VERSION_UPDATED)
// Return:  u8  status      (S_SUCCESS,S_FAIL)
// Engineer: Quyen Leba
// Note: assume version in format M.
//------------------------------------------------------------------------------------------------------------------------------------------------
u8 Version_CompareFirmwareVersion(s8 *result)
{
    u32     ver1;

    if (Version_ToInt(Firmware_Version, &ver1) != S_SUCCESS)
    {
        return S_FAIL;
    }

    if (ver1 == settings.FWVersion)
    {
        *result = VERSION_CURRENT;
    }
    else if (ver1 > settings.FWVersion)
    {
        *result = VERSION_UPDATED;
    }
    else
    {
        *result = VERSION_BACKDATED;
    }

    return S_SUCCESS;
}
*/

/*
//------------------------------------------------------------------------------------------------------------------------------------------------
// Check if device firmware is updated & run corresponding firmware update patch
// Engineer: Quyen Leba
//------------------------------------------------------------------------------------------------------------------------------------------------
void Version_FirmwareCheck(void)
{
    s8  status;

    if (Version_CompareFirmwareVersion(&status) == S_SUCCESS)
    {
        if (status == VERSION_UPDATED)
        {

        }
        else if (status == VERSION_BACKDATED)
        {

        }
    }
    else
    {
        //TODO
        //firmware version is invalid, do something?!?
    }
}
*/

/*
//------------------------------------------------------------------------------------------------------------------------------------------------
// Check if tune revision meet minimum tune version requirement of this firmware
// Engineer: Quyen Leba
//------------------------------------------------------------------------------------------------------------------------------------------------
#if (SUPPORT_STRATEGYTUNE)
void Version_TuneCheck(void)
{
    s8  status;

    while(1)
    {
        if (Version_CompareTuneVersion(&status) == S_SUCCESS)
        {
            if (status == VERSION_NEW)
            {
                //do nothing
            }
            else if (status == VERSION_OLD)
            {
                ShowMessage(SW_ENTER,ERRORMSG,(void*)*Language->HDR_DeviceInfo,
                            (void*)*Language->FDR_ContinueKeyMsg,
                            (void*)*Language->TXT_UpdateTuneRevision,
                            (void*)*Language->TXT_NULLSTR,
                            (void*)*Language->TXT_NULLSTR,CLEAR_ALL);
                //TODO: show current tune revision
            }
            else
            {
                return;
            }
        }
        else
        {
            //TODO
            //tune version is invalid, do something?!?
        }
    }
}
#endif  //#if (SUPPORT_STRATEGYTUNE)
*/

//------------------------------------------------------------------------------------------------------------------------------------------------
// Check device status & if firmware allowed to run
//------------------------------------------------------------------------------------------------------------------------------------------------
void Version_DeviceCheck(void)
{
    Settings_Init();
    SaveSettings();
    if (settings.Sct[0] != 'S' ||
        settings.Sct[1] != 'C' ||
        settings.Sct[2] != 'T')
    {
        //TODO: invalid settings
        //TODO: load from file & check again
        LCD_gclrvp();
        //LCD_gputspos(0,1,*Language->
        LCD_gputscpos(0,1,"Error on Settings");
        while(1);
    }

    //TODO: check device type, market byte, etc
// Check for correct firmware and device type
    if(Settings_GetDeviceType == DEVICE_TYPE)
    {
      if(Settings_GetMarketType == AU)
      {
        Firmware_Version[0] = '2'; // Change firmware to 2.X.XXX to signify Australian firmware
 //       SF_block_temp = SF_BLOCKED;
      }
      if(Settings_GetMarketType == UK)
      {
        Firmware_Version[0] = '3'; // Change firmware to 2.X.XXX to signify Australian firmware
 //       SF_block_temp = SF_BLOCKED;
      }
    }
    else
    {
        gclrvp();
        gselvp(0);
        if(MARKET_TYPE == US)
        {
#ifdef BANKS_ENG
            LCD_gputscpos(0,0,"Invalid firmware!");
            LCD_gputscpos(0,2,"Load the correct firmware or contact BANKS customer support.");
#else
            LCD_gputscpos(0,0,"Invalid firmware!");
            LCD_gputscpos(0,2,"Load the correct firmware or contact customer support @ www.sctflash.com");
#endif
        }
        if(MARKET_TYPE == UK ||MARKET_TYPE == AU)
        {
            LCD_gputscpos(0,0,"Invalid firmware!");
            LCD_gputscpos(0,2,"Load the correct firmware or contact customer support @ www.dreamscience.eu");
        }

        while(1);
    }
}

//------------------------------------------------------------------------------------------------------------------------------------------------
// EOF
//------------------------------------------------------------------------------------------------------------------------------------------------
