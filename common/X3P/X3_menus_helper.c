/**
  **************** (C) COPYRIGHT 2011 SCT Performance, LLC *********************
  * File Name          : X3_menus_helper.c
  * Device / System    : SCT products using the "CommonCode" platform
  * Author             : Quyen Leba
  *
  * Version            : 1 
  * Date               : 05/02/2011
  * Description        : 
  *                    : 
  *
  *
  * History            : 05/02/2011 M. Davis
  *                    :   Add SCT Copyright header,
  *
  ******************************************************************************
  */

#include <stdio.h>
#include <board/X3P/system.h>
#include <common/X3P/error.h>
#include <common/statuscode.h>
#include <common/settings.h>
#include <common/devicedef.h>
#include "X3_menus_helper.h"

//borrow from menus.c
extern MENUCODE    CurrentMenu;
extern MENUCODE    PreviousMenu;
extern u8          CurrentIcon;
extern u8          TotalIcon;
//borrow from menus_helper.c
extern u8          nav_direction;                          //nav_direction of LiveWire
extern u8          nav_changed;                            //nav_changed of LiveWire
//borrow from menus_helper.c
extern void        MenuRotateLeft(void);
extern void        MenuRotateRight(void);

//------------------------------------------------------------------------------
// Private Variables
//------------------------------------------------------------------------------
const u8        *MAINMENU_STR[]                 = {//Name               //Icon filename
                                                   "Program Vehicle",   "100_S.bin",
#ifdef BANKS_ENG
                                                   "Diagnostic Info",   "200_S.bin",
#else
                                                   "Vehicle Info",      "200_S.bin",
#endif
                                                   "Data Logging",      "500_S.bin",
                                                   "Dealer Info",       "400_S.bin",
                                                   "Device Settings",   "600_S.bin",
                                                   "Device Info",       "900_S.bin",
                                                  };
#define         TOTAL_MAINMENU_ICON             6
//----------------------------------------------------------
const u8        *PROGRAMVEHICLEMENU_STR[]       = {
#ifdef BANKS_ENG                           
                                                   "Custom Tune",       "110_S.bin",
                                                   "Banks Developed",   "110_S.bin",
                                                   "Return to Stock",   "330_S.bin",
                                              
                                                  };
#define         TOTAL_PROGRAMVEHICLEMENU_ICON   3

#else
                                                   "Custom Tune",       "110_S.bin",
                                                   "PreProgramed Tune", "110_S.bin",
                                                   "Return to Stock",   "330_S.bin",
                                                   "Upload Stock",       "210_S.bin",
                                                  };
#define         TOTAL_PROGRAMVEHICLEMENU_ICON   4

#endif                                                   
//----------------------------------------------------------
const u8        *DTCMENU_STR[]                  = {
#ifdef BANKS_ENG
                                                   "Read Family",       "300_S.bin",
#else
                                                   "Vehicle Info",      "300_S.bin",
#endif
                                                   "Read DTC",          "210_S.bin",
                                                   "Clear DTC",         "220_S.bin",
                                                  };
#define         TOTAL_DTCMENU_ICON              3
//----------------------------------------------------------
#if (__X3P_PLATFORM__)
  #ifdef BANKS_ENG
const u8        *PERFORMANCEMENU_STR[]          = {
                                                   "Setup Monitor",     "550_S.bin",
                                                   "Start Monitor",     "560_S.bin",
//                                                   "Setup Analog Input","580_S.bin",
                                                  };
#define         TOTAL_PERFORMANCEMENU_ICON      2
  #else
const u8        *PERFORMANCEMENU_STR[]          = {
                                                   "Setup Monitor",     "550_S.bin",
                                                   "Start Monitor",     "560_S.bin",
                                                   "Setup Analog Input","580_S.bin",
                                                  };
#define         TOTAL_PERFORMANCEMENU_ICON      3
  #endif
#else
    INVALID_PLATFORM;                          
#endif
//----------------------------------------------------------
const u8        *DEVICESETTINGSMENU_STR[]       = {
                                                   "Backlight",
                                                   "Contrast",
                                                   "Sound (USB-power)"
                                                  };
#define         TOTAL_DEVICESETTINGSMENU_ITEM   3

//------------------------------------------------------------------------------
// Private Prototypes
//------------------------------------------------------------------------------
void    DrawMenu(void);

//------------------------------------------------------------------------------
// Description: display icon menu & wait for user selection
// Input: u8 menu (0: main, 1: performance)
// Return: (u8) menu selection (zero based), 0xFF: escape
//------------------------------------------------------------------------------
MENUITEM ShowIconMenu(MENUCODE menu)
{
    u8 key;
    u32 device_type;
    device_type = SETTINGS_GetDeviceType();

    switch(menu)
    {
      case MAINMENU:
        if (menu != CurrentMenu)
        {
            FreeMenuInBuffer();
            PreviousMenu = CurrentMenu;
            CurrentMenu = menu;
#if (__X3P_PLATFORM__)
            TotalIcon = TOTAL_MAINMENU_ICON;
            CurrentIcon = 0;
#else
            INVALID_PLATFORM
#endif
        }
        ShowMessage(NOKEY,NOAUDIO,"X3 MAIN","","","","ABC",0);                  // 1
        break;

#if (__X3P_PLATFORM__)
      case PERFORMANCEMENU:
        if (menu != CurrentMenu)
        {
            TotalIcon = TOTAL_PERFORMANCEMENU_ICON;
            PreviousMenu = CurrentMenu;
            CurrentMenu = menu;
            CurrentIcon = 1;
        }
        break;
      case PROGRAMVEHICLEMENU:
        if (menu != CurrentMenu)
        {
            TotalIcon = TOTAL_PROGRAMVEHICLEMENU_ICON;
            PreviousMenu = CurrentMenu;
            CurrentMenu = menu;
            CurrentIcon = 1;
        }
        break;
      case DTCMENU:
        if (menu != CurrentMenu)
        {
            TotalIcon = TOTAL_DTCMENU_ICON;
            PreviousMenu = CurrentMenu;
            CurrentMenu = menu;
            CurrentIcon = 1;
        }
        break;
      case DEVICESETTINGSMENU:
        if (menu != CurrentMenu)
        {
            TotalIcon = TOTAL_DEVICESETTINGSMENU_ITEM;
            PreviousMenu = CurrentMenu;
            CurrentMenu = menu;
            CurrentIcon = 1;
        }
        break;
#endif
    }
#if (__X3P_PLATFORM__)
    DrawMenu();
#else
    INVALID_PLATFORM
#endif

    while(1)
    {
        //TODOQ:
        /*
        if (Datalog_State() == ENABLE)
        {
            return (MENUITEM)NULLMENUITEM;
        }
        */

        key = getKey();
        if (key == SW_ENTER)
        {
            key = CurrentIcon;
            if((device_type == X3PEO)&&(menu == PROGRAMVEHICLEMENU))
              key++;

            break;
        }
        else if (key == SW_EXIT)
        {
            key = 0xF0;
            break;
        }
        else if (key == SW_BACK || key == SW_NEXT)
        {
            nav_changed = TRUE;
            nav_direction = key;
        }

        if(nav_changed == TRUE)
        {
            nav_changed = FALSE;
            if(nav_direction == SW_BACK)
            {
                MenuRotateLeft();
            }
            else
            {
                MenuRotateRight();
            }
#if (__X3P_PLATFORM__)
            DrawMenu();
#else
            INVALID_PLATFORM
#endif
        }
    }
    gclrvp();
    return (MENUITEM)((u8)menu + key);
}

//------------------------------------------------------------------------------
// Inputs:  MENUCODE     CurrentMenu
//          u8           CurrentIcon
//------------------------------------------------------------------------------
void DrawMenu(void)
{
    u8  p;
    u8  str[2] = {0,0};
    u8  **menu_str;

    LCD_gclrvp();
    LCD_gselfont(&bold2);
    gsetmode(GALIGN_HCENTER);
    //LCD_gsetcpos(0,1);
    LCD_gsetcpos(0,0);

    if (CurrentMenu == MAINMENU)
    {
        menu_str = (u8**)MAINMENU_STR;
    }
    else if (CurrentMenu == PERFORMANCEMENU)
    {
        menu_str = (u8**)PERFORMANCEMENU_STR;
    }
    else if (CurrentMenu == PROGRAMVEHICLEMENU)
    {
        menu_str = (u8**)PROGRAMVEHICLEMENU_STR;
    }
    else if (CurrentMenu == DTCMENU)
    {
        menu_str = (u8**)DTCMENU_STR;
    }
    else
    {
        //X3 does not have other menus YET
    }
    LCD_gputs((void*)menu_str[CurrentIcon*2]);
    gsetmode(GNORMAL);

    //left icon
    if (CurrentIcon == 0)
    {
        p = TotalIcon - 1;
    }
    else
    {
        p = CurrentIcon - 1;
    }

    LCD_gputimage((void*)menu_str[p*2+1],0,26);

    //middle icon
    LCD_gputimage((void*)menu_str[CurrentIcon*2+1],46,20);
    LCD_ginvertvp(41,18,86,58);

    //right icon           
#ifdef BANKS_ENG                             
    if( TotalIcon > 2 )   // Performance_menu in BANKs device and Program_Vehicle_Menu 
    {                                     // only have 2 (Icon) Menu Options.  This code
#endif                                    // snippet disables right-hand (third) Icon.
        if ((CurrentIcon+1) >= TotalIcon)
        {
            p = 0;
        }
        else
        {
            p = (CurrentIcon + 1);
        }
        LCD_gputimage((void*)menu_str[p*2+1],92,26);
#ifdef BANKS_ENG
    }
#endif      

    LCD_gsetcpos(0,2);
    //str[0] = 0x1B;
    str[0] = 0x11;
    LCD_gputs((void*)str);
    LCD_gsetcpos(15,2);
    //str[0] = 0x1A;
    str[0] = 0x10;
    LCD_gputs((void*)str);

    //draw line above icons
//    LCD_gmoveto(0,23);
//    LCD_glineto(127,23);
//    LCD_gmoveto(0,24);
//    LCD_glineto(127,24);
}

//------------------------------------------------------------------------------
// Engineer: Quyen Leba
//------------------------------------------------------------------------------
MENUITEM ShowTextMenu(MENUCODE menu)
{
    MENUITEMSTR*    mitem;
    u8              i,totalitem;
    u8              status;
    u8              **menu_str;

    ShowMessage(NOKEY,NOAUDIO,(void*)*Language->HDR_DeviceSettings,
#ifdef BANKS_ENG
                (void*)*Language->FDR_SelectMsg,
#else
                (void*)*Language->FDR_ContinueKeyMsg,
#endif
                (void*)*Language->TXT_NULLSTR,
                (void*)*Language->TXT_NULLSTR,
                (void*)*Language->TXT_NULLSTR,CLEAR_TOP);

    switch(menu)
    {
      case DEVICESETTINGSMENU:
        menu_str = (u8**)DEVICESETTINGSMENU_STR;
        totalitem = TOTAL_DEVICESETTINGSMENU_ITEM;
        break;
      default:
        return (MENUITEM)NULLMENU;
    }

    mitem = __malloc(totalitem*sizeof(MENUITEMSTR));
    for(i=0;i<totalitem;i++)
    {
        ParseMenuItemStr(menu_str[i], i, mitem, 0);
    }
    status = Scroller(&i, 0, mitem, totalitem, 3, 0, 8, 0, 32, NullHelper, NullHelper);
    __free(mitem);

    if (status == S_USERABORT)
    {
        return (MENUITEM)NULLMENU;
    }
    else
    {
        return (MENUITEM)(menu + i);
    }
}

//------------------------------------------------------------------------------
// Engineer: Quyen Leba
//------------------------------------------------------------------------------
void MENUS_DealerInfo(void)
{
  F_FILE    *f;
  u8        contact_Name[21];
  u8        Contact_Phone[21];
  u8        Contact_Web[21];
  u8        buffer[256];
  u8        j, i;

  memset(contact_Name, 0 ,21);
  memset(Contact_Phone, 0 ,21);
  memset(Contact_Web, 0 ,21);
  memset(buffer, 0 ,256);
  
  f = __fopen("/dealerinfo.txt","rb");
  if(f != NULL)
  {
    f_read(buffer,1, sizeof(buffer),f);
    __fclose(f);
    
    for(j=0; j<21; j++)
    {
        if((buffer[j] == 0x0D)&&(buffer[j+1] == 0x0A))
            break;
         else
           contact_Name[j] = buffer[j];
    }

    j++;
    j++;

    for(i=0; i<21; i++)
    {
        if((buffer[j] == 0x0D)&&(buffer[j+1] == 0x0A))
            break;
         else
           Contact_Phone[i] = buffer[j++];
    }
    
    j++;
    j++;

    for(i=0; i<21; i++)
    {
        if((buffer[j] == 0x0D)&&(buffer[j+1] == 0x0A))
            break;
         else
           Contact_Web[i] = buffer[j++];
    }
    
    
        ShowMessage(SW_ENTER,NOAUDIO,(void*)*Language->HDR_DealerInfo,
                (void*)*Language->FDR_ContinueKeyMsg,
                contact_Name,
                Contact_Phone,
                Contact_Web,CLEAR_ALL);
  }
  else
  {

#ifdef BANKS_ENG
    ShowMessage( SW_ENTER,NOAUDIO,(void*)*Language->HDR_DealerInfo,             
                (void*)*Language->TXT_NULLSTR,
                (void*)*Language->TXT_NULLSTR,
                (void*)*Language->TXT_BANKS_Contact,
                (void*)*Language->TXT_NULLSTR, CLEAR_ALL );
#else    
      ShowMessage(SW_ENTER,NOAUDIO,(void*)*Language->HDR_DealerInfo,
                (void*)*Language->FDR_ContinueKeyMsg,
                (void*)*Language->TXT_ContactName,
                (void*)*Language->TXT_ContactPhone,
                (void*)*Language->TXT_ContactWeb,CLEAR_ALL);
#endif    
  }
}

//---------------------------------------------------------------------------------------------------
// Engineer: Quyen Leba
//---------------------------------------------------------------------------------------------------
void MENUS_DeviceInfo(void)
{
#if 0   //TODOQ:
    //TODO; language
    u8              status;
    u8              i;
    u8              buf[48];
    MENUITEMSTR     *mitem;
    F_FILE          *f;
//    u8 key =        0;
                     

    ShowMessage(NOKEY,NOAUDIO,(void*)*Language->HDR_DeviceInfo,                 // 4
                (void*)*Language->FDR_ContinueKeyMsg,
                (void*)*Language->TXT_NULLSTR,
                (void*)*Language->TXT_NULLSTR,
                (void*)*Language->TXT_NULLSTR,CLEAR_TOP);

    memset((void*)buf,' ', 20);

    mitem = __malloc(20*sizeof(MENUITEMSTR));
    i = 0;
    //TODO: language

//  ******** SERIAL NUMBER ************   
#ifdef BANKS_ENG
    memcpy( buf, "SN: ", 4 );
    memcpy( &buf[3], settings.serial_num, 13 );
#else    
    ParseMenuItemStr("Serial Number:", i++, mitem, 0);
    memcpy((void*)&buf[2],(void*)settings.serial_num,13);
#endif    
    buf[16] = 0;                           
    ParseMenuItemStr(buf, i++, mitem, 0);

//  ******* FW VERSION NUMBER *********
#ifdef BANKS_ENG
    memcpy( buf, "FW Rev: ", 20 );
    memcpy( &buf[7], (void*) Firmware_Version, 25 );
#else    
    ParseMenuItemStr("FW Version:", i++, mitem, 0);
    strcpy((void*)&buf[2],(void*)Firmware_Version);
    if(Settings_GetMarketType == AU)
        buf[2] = '2';
    else if(Settings_GetMarketType == UK)
        buf[2] = '3';
#endif
    buf[30] = 0;                           
    ParseMenuItemStr(buf, i++, mitem, 0);

//  ******* VEHICLE TYPE (NON-BANKS) **************
#ifndef BANKS_ENG    
    ParseMenuItemStr("FW Type: GM", i++, mitem, 0);
#endif    

    //  ******* TUNE VERSION **************

    f = __fopen("tuneversion.txt","rb");
    if (f == NULL)
    {
        strcpy((void*)&buf[2],"N/A");
    }
    else
    {
#ifdef BANKS_ENG
        
        memcpy( buf, "Tune:     ", 17 );           
        f_read((void*)&buf[8],1,17,f);        
        buf[17] = 0;
#else    
        memcpy( buf, "                ", 17 );
        f_read((void*)&buf[2],1,10,f);
        buf[15] = 0;
#endif        
        __fclose(f);
    }

#ifdef BANKS_ENG
    ParseMenuItemStr( buf, i++, mitem, 0);
#else
    ParseMenuItemStr("Tune Version:", i++, mitem, 0);
    ParseMenuItemStr((void*)buf, i++, mitem, 0);
#endif    

    
    //  ******* UNLOCKS LEFT **************
#ifdef BANKS_ENG
//    sprintf((void*)buf,"Unlock: %d Left",(5 - settings.married_cnt));
//    ParseMenuItemStr(buf, i++, mitem, 0);
#else
    sprintf((void*)buf,"Unlocks Left: %d",(5 - settings.married_cnt));
    ParseMenuItemStr(buf, i++, mitem, 0);
#endif    

    
    //  ******* MARRIAGE STATUS **********
    if (settings.married == 1)
    {
        //TODO: different in 6.4L
#ifdef BANKS_ENG
        ParseMenuItemStr("Status:  LOCKED", i++, mitem, 0);
        sprintf((void*)buf,"Unlocks: %d Left",(5 - settings.married_cnt));
        ParseMenuItemStr(buf, i++, mitem, 0);
        strcpy((void*)buf,"OS Part#  ");
        memcpy((void*)&buf[9],(void*)settings.cur_strat,7);
#else
        ParseMenuItemStr("Device Married", i++, mitem, 0);
        strcpy((void*)buf,"OS Part#:  ");
        memcpy((void*)&buf[9],(void*)settings.cur_strat,8);
#endif        
        ParseMenuItemStr(buf, i++, mitem, 0);
        if((settings.DownloadFailed == 1) &&(settings.EPATS[0] != 0))
        {
            ParseMenuItemStr("Return Flash Type:", i++, mitem, 0);
            if(settings.EPATS[0] == CAL_DOWNLOAD)
                ParseMenuItemStr("CAL_DOWNLOAD", i++, mitem, 0);
            else
                ParseMenuItemStr("FULL_DOWNLOAD", i++, mitem, 0);
        }
        else if(settings.VID[20] != NOTSET)
        {
          ParseMenuItemStr("Return Flash Type:", i++, mitem, 0);

            if(settings.VID[20] == CAL_DOWNLOAD)
                ParseMenuItemStr("CAL_DOWNLOAD", i++, mitem, 0);
            else
                ParseMenuItemStr("FULL_DOWNLOAD", i++, mitem, 0);
        }
        else if(settings.VID[30] != NOTSET)
        {
          ParseMenuItemStr("Last Flash Type:", i++, mitem, 0);

            if(settings.VID[30] == CAL_DOWNLOAD)
                ParseMenuItemStr("CAL_DOWNLOAD", i++, mitem, 0);
            else
                ParseMenuItemStr("FULL_DOWNLOAD", i++, mitem, 0);
        }
    }
    else
    {
#ifdef BANKS_ENG
        ParseMenuItemStr("Status: UNLOCKED", i++, mitem, 0);
#else
        ParseMenuItemStr("Device UnMarried", i++, mitem, 0);
#endif

        if(settings.VID[20] != NOTSET)
        {
          ParseMenuItemStr("Last Flash Type:", i++, mitem, 0);

            if(settings.VID[20] == CAL_DOWNLOAD)
                ParseMenuItemStr("CAL_DOWNLOAD", i++, mitem, 0);
            else
                ParseMenuItemStr("FULL_DOWNLOAD", i++, mitem, 0);
        }
    }

#ifdef BANKS_ENG
    Scroller(&status, 0, mitem, i, 3, 0, 8, 0, 24, NullHelper, NullHelper);
#else
    Scroller(&status, 0, mitem, i, 4, 0, 8, 0, 24, NullHelper, NullHelper);
#endif
    __free(mitem);
#endif
}

//------------------------------------------------------------------------------
//For X3, set backlight to On/Off
// Return:  u8  0
// Engineer: Quyen Leba
//------------------------------------------------------------------------------
u8 AdjustBrightnessHelper(void)
{
    MENUITEMSTR*    mitem;
    u8              status,index;

    ShowMessage(NOKEY,NOAUDIO,(void*)*Language->HDR_DeviceSettings,
#ifdef BANKS_ENG
                (void*)*Language->FDR_SelectMsg,
#else
                (void*)*Language->FDR_ContinueKeyMsg,
#endif
                (void*)*Language->TXT_NULLSTR,
                (void*)*Language->TXT_NULLSTR,
                (void*)*Language->TXT_NULLSTR,CLEAR_TOP);

    LCD_gputscpos(0,2,(void*)*Language->TXT_Backlight);
    mitem = __malloc(2*sizeof(MENUITEMSTR));
    ParseMenuItemStr((void*)*Language->TXT_On, 0, mitem, 0);
    ParseMenuItemStr((void*)*Language->TXT_Off, 1, mitem, 0);

    index = 0;
    status = Scroller(&index, 0, mitem, 2, 4, 0, 8, 0, 32, NullHelper, NullHelper);
    __free(mitem);
    if (status != S_USERABORT)
    {
        if (index == 0)     //ON
        {
            SETTINGS_GENERAL(backlight) = 1600;
            LCD_Backlight(ON);
        }
        else                //OFF
        {
            SETTINGS_GENERAL(backlight) = 0;
            LCD_Backlight(OFF);
        }
        SETTINGS_SetGeneralAreaDirty();
        settings_update(FALSE);
    }

    return 0;
}

//------------------------------------------------------------------------------
// X3-Plus... Enable or disable sounds when the device is running from USB power
// Return:  u8  0
// Engineer: Mark Davis
//------------------------------------------------------------------------------
u8 AdjustSound_USBpwr_Helper(void)
{
    MENUITEMSTR*    mitem;
    u8              status,index;

    ShowMessage(NOKEY,NOAUDIO,(void*)*Language->HDR_DeviceSettings,
                (void*)*Language->FDR_ContinueKeyMsg,
                (void*)*Language->TXT_NULLSTR,
                (void*)*Language->TXT_NULLSTR,
                (void*)*Language->TXT_NULLSTR,CLEAR_TOP);
    LCD_gputscpos(0,2,(void*)*Language->TXT_Sounds_USB_pwr);


    mitem = __malloc(2*sizeof(MENUITEMSTR));
    ParseMenuItemStr((void*)*Language->TXT_On, 0, mitem, 0);
    ParseMenuItemStr((void*)*Language->TXT_Off, 1, mitem, 0);

    index = 0;
    status = Scroller(&index, 0, mitem, 2, 4, 0, 8, 0, 32, NullHelper, NullHelper);
    __free(mitem);
    if (status != S_USERABORT)
    {
        if (index == 0)     // Enable sounds when USB-powered
        {
            settingsFlags.BuzzerDisabledUSB = FALSE;
        }
        else                // Disable sounds when USB-powered
        {
            settingsFlags.BuzzerDisabledUSB = TRUE;
        }
        //TODOQ: more on this
        SETTINGS_SetGeneralAreaDirty();
        settings_update(FALSE);
    }

    return 0;
}

//------------------------------------------------------------------------------
// For X3, set buzzer to On/Off
// Return:  u8  0
// Engineer: Quyen Leba
//------------------------------------------------------------------------------
u8 AdjustBuzzerHelper(void)
{
    MENUITEMSTR*    mitem;
    u8              status,index;

    ShowMessage(NOKEY,NOAUDIO,(void*)*Language->HDR_DeviceSettings,
                (void*)*Language->FDR_ContinueKeyMsg,
                (void*)*Language->TXT_NULLSTR,
                (void*)*Language->TXT_NULLSTR,
                (void*)*Language->TXT_NULLSTR,CLEAR_TOP);
    LCD_gputscpos(0,2,(void*)*Language->TXT_Audio);

    mitem = __malloc(2*sizeof(MENUITEMSTR));
    ParseMenuItemStr((void*)*Language->TXT_On, 0, mitem, 0);
    ParseMenuItemStr((void*)*Language->TXT_Off, 1, mitem, 0);

    index = 0;
    status = Scroller(&index, 0, mitem, 2, 4, 0, 8, 0, 32, NullHelper, NullHelper);
    __free(mitem);
    if (status != S_USERABORT)
    {
        if (index == 0)     //ON
        {
        }
        else                //OFF
        {
        }
        SETTINGS_SetGeneralAreaDirty();
        settings_update(FALSE);
    }

    return 0;
}

//------------------------------------------------------------------------------
// For X3, set language
// Return:  u8  0
// Engineer: Quyen Leba
//------------------------------------------------------------------------------
u8 AdjustLanguageHelper(void)
{
    MENUITEMSTR*    mitem;
    u8              status,index;

    ShowMessage(NOKEY,NOAUDIO,(void*)*Language->HDR_DeviceSettings,
                (void*)*Language->FDR_ContinueKeyMsg,
                (void*)*Language->TXT_NULLSTR,
                (void*)*Language->TXT_NULLSTR,
                (void*)*Language->TXT_NULLSTR,CLEAR_TOP);
    LCD_gputscpos(0,2,(void*)*Language->TXT_Audio);

    mitem = __malloc(2*sizeof(MENUITEMSTR));
    ParseMenuItemStr("English", 0, mitem, 0);
    ParseMenuItemStr("English", 1, mitem, 0);

    index = 0;
    status = Scroller(&index, 0, mitem, 2, 4, 0, 8, 0, 32, NullHelper, NullHelper);
    __free(mitem);
    if (status != S_USERABORT)
    {
        if (index == 0)     //ON
        {
        }
        else                //OFF
        {
        }
        SETTINGS_SetGeneralAreaDirty();
        settings_update(FALSE);
    }

    return 0;
}

//------------------------------------------------------------------------------
// For X3, adjust LCD contrast
// Return:  u8  0
// Engineer: Quyen Leba
//------------------------------------------------------------------------------
u8 AdjustContrastHelper(void)
{
#define MAX_CONTRAST    100
#define MIN_CONTRAST    0
    u8  contrast;
    u8  key;

    ShowMessage(NOKEY,NOAUDIO,(void*)*Language->HDR_DeviceSettings,
                (void*)*Language->FDR_ContinueKeyMsg,
                (void*)*Language->TXT_NULLSTR,
                (void*)*Language->TXT_NULLSTR,
                (void*)*Language->TXT_NULLSTR,CLEAR_TOP);

    contrast = SETTINGS_GENERAL(contrast);

    if (contrast > 100)
    {
        contrast = 100;
    }
    Progress_Bar(MAX_CONTRAST,contrast,1,TRUE);

    LCD_gsetmode(GALIGN_HCENTER);
    LCD_gputscpos(0,2,(void*)*Language->TXT_Contrast);

    while(1)
    {
        key = getKey();
        if (key == SW_EXIT)
        {
            LCD_ghw_cont_set(SETTINGS_GENERAL(contrast));
            break;
        }
        else if (key == SW_ENTER)
        {
            SETTINGS_GENERAL(contrast) = contrast;
            SETTINGS_SetGeneralAreaDirty();
            settings_update(FALSE);
            break;
        }
        else if (key == SW_BACK || key == SW_DOWN)
        {
            if (contrast > MIN_CONTRAST)
            {
                contrast--;
            }
            LCD_gsetmode(GNORMAL);
            LCD_gclearareafast(0,3,21,6);
            Progress_Bar(MAX_CONTRAST,contrast,1,TRUE);
            LCD_ghw_cont_set(contrast);
        }
        else if (key == SW_NEXT || key == SW_UP)
        {
            if (contrast < MAX_CONTRAST)
            {
                contrast++;
            }
            LCD_gsetmode(GNORMAL);
            LCD_gclearareafast(0,3,21,6);
            Progress_Bar(MAX_CONTRAST,contrast,1,TRUE);
            LCD_ghw_cont_set(contrast);
        }
    }
    return 0;
}

u8 Mis_match_OS(void)
{
    u8 key =0;


    ShowMessage(SW_ENTER, ERRORMSG, (void*)*Language->HDR_OS_Mismatch,
                    (void*)*Language->FDR_ContinueKeyMsg,
                    (void*)*Language->TXT_OS_Mismatch_1,
                    (void*)*Language->TXT_OS_Mismatch_2,
                    (void*)*Language->TXT_OS_Mismatch_3, CLEAR_TOP);

    
    key = ShowMessage(SW_ENTER, NOAUDIO, (void*)*Language->HDR_OS_Mismatch,
                    (void*)*Language->FDR_ContinueKeyMsg,
                    (void*)*Language->TXT_OS_Mismatch_4,
                    (void*)*Language->TXT_NULLSTR,
                    (void*)*Language->TXT_OS_Mismatch_5, CLEAR_ALL);

    return(key);
}


       
       
//------------------------------------------------------------------------------
// Compare OS with a text file contains supported OS
// Input:   u8 *tune (OS#), u8 *lookupfile (filename)
// Return:  status (S_FAIL, S_UNMATCH, S_SUCCESS)
//------------------------------------------------------------------------------
u8 GM_CompareOS(u8 index, u8* strategy, u8 trans_match)
{
#if 0   //TODOQ:
    F_FILE *file;
    u8  part_nm_OS[9];
    u8  part_nm_TN[9];
    u8  OS_compare[9];
    u8 Vin_num[20];
    u8 OSlookupfile[13];

    memset(OSlookupfile, 0, 13);
    memset(part_nm_OS, 0, 9);
    memset(part_nm_TN, 0, 9);
    memset(Vin_num, 0, 20);
    memset(OS_compare, 0, 9);
    memcpy(OS_compare, strategy, 8);
    

    strcpy((void*)OSlookupfile,"/tuneos1.txt");
    
    switch(index)  //get correct index for file name from user tune selection
    {
      case 0:
          OSlookupfile[7] = 0x31;
          break;
      case 1:
           OSlookupfile[7] = 0x32;
           break;
      case 2:
           OSlookupfile[7] = 0x33;
           break;
    }

    if((file = __fopen((char*)OSlookupfile, "rb"))== NULL)
    {
        ERROR_Message(OPENFILE);
        return S_FAIL;
    }
    else
    {

            if((f_seek(file, 0, SEEK_SET)) !=0)
            {
                __fclose(file);
                ERROR_Message(SEEKFILE);
                return S_FAIL;
            }

            if((f_read(Vin_num,1, 19, file)) < 1)
            {
                __fclose(file);
                ERROR_Message(READFILE);
                return S_FAIL;
            }

            Vin_num[17] = 0;
            Vin_num[18] = 0;
            Vin_num[19] = 0;

            if((f_read(part_nm_OS,1, 8, file)) < 1) // Read OS part number
            {
                __fclose(file);
                ERROR_Message(READFILE);
                return S_FAIL;
            }

            f_read(part_nm_TN,1, 2, file);          // Return and line feed

            f_read(part_nm_TN,1, 8, file);          // Read Trans part number, may or may not be there

            __fclose(file);
                
            if (strcmp((void*)VIN_temp,(void*)Vin_num) == 0)
            {
                if(trans_match)
                {
                    if (strcmp((void*)OS_compare,(void*)part_nm_TN) == 0)
                        return S_SUCCESS;                       //match Trans and VIN found = 0
                    else
                        return S_UNMATCH;                       // Vin matches, Trans does not =1 
                }
                else
                {
                    if (strcmp((void*)OS_compare,(void*)part_nm_OS) == 0)
                        return S_SUCCESS;                       //match OS and VIN found = 0
                    else
                        return S_UNMATCH;                       // Vin matches, OS does not =1
                }
            }
    }
    return S_VINUNMATCH;                                    // Vin does not match =2
#endif
}
