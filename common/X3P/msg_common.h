/**
  **************** (C) COPYRIGHT 2011 SCT Performance, LLC *********************
  * File Name          : msg_common.h
  * Device / System    : SCT products using the "CommonCode" platform
  * Author             : Quyen Leba
  *
  * Version            : 1 
  * Date               : 05/02/2011
  * Description        : 
  *                    : 
  *
  *
  * History            : 05/02/2011 M. Davis
  *                    :   Add SCT Copyright header,
  *
  ******************************************************************************
  */

#ifndef __MSG_COMMON_H
#define __MSG_COMMON_H

#include <arch/gentype.h>

typedef enum
{
    LANGUAGE_EN,
    LANGUAGE_ES,
}LANGUAGE_TYPE;

//------------------------------------------------------------------------
typedef struct
{
    u8**    HDR_EstimatedMPHTime;
    u8**    HDR_ReturnVehicleToStock;
    u8**    HDR_ReadDiagTroubleCode;
    u8**    HDR_ReadPendingDiagTroubleCode;
    u8**    HDR_DealerInfo;
    u8**    HDR_DeviceInfo;
    u8**    HDR_DeviceSettings;
    u8**    HDR_VehicleInfo;
    u8**    HDR_UploadECU;
    u8**    HDR_DownloadTune;
    u8**    HDR_DownloadStock;
    u8**    HDR_UnlockECU;
    u8**    HDR_SetupMonitor;
    u8**    HDR_Monitor;
    u8**    HDR_VehicleNotSupported;
    u8**    HDR_EstimatedHPTQ;
    u8**    HDR_SetupAnalog1;
    u8**    HDR_SetupAnalog2;
    u8**    HDR_EstimatedQuarterMileTime;
    u8**    HDR_PIDAlertSetup;
    u8**    HDR_ClearDiagTroubleCode;
    u8**    HDR_ProgramVehicleECU;
    u8**    HDR_SetupStorage;
    u8**    HDR_SetupECU;
    u8**    HDR_ResetECU;
    u8**    HDR_MatchingVehicle;
    u8**    HDR_SelectFuelType;
    u8**    HDR_UserAdjustmentOptions;
    u8**    HDR_CopyFile;
    u8**    HDR_ReadStrategy;
    u8**    HDR_SelectFile;
    u8**    HDR_SelectPID;
    u8**    HDR_SetupCustomEquation;
    u8**    HDR_SetupAnalog;
    u8**    HDR_Bootloader;
    u8**    HDR_StockfileUpdated;
    u8**    HDR_PassthroughDatalog;
    u8**    HDR_Disclaimer_Title; 
    u8**    HDR_Warning;
    u8**    HDR_OS_Mismatch;
    u8**    HDR_OS_Blocked;
    u8**    HDR_POWERDOWN;
#ifdef BANKS_ENG
    u8**    HDR_SelectVehicle;
    u8**    HDR_UserAdjOpts;
    u8**    HDR_TuneVehicleECU;
#else
    u8**    HDR_TuneType;
    u8**    HDR_SelectAuto;
#endif    
    u8**    HDR_Attention;    
    u8**    TXT_NULLSTR;
    u8**    TXT_PressWheelContinue;
    u8**    TXT_TurnKeyOn;
    u8**    TXT_TurnKeyOff;
    u8**    TXT_TurnKeyOff_Remove;
    u8**    TXT_DoNotStartEngine;
    u8**    TXT_UploadComplete;
    u8**    TXT_PrepareDownload;
    u8**    TXT_ProgramPCMOnly;
    u8**    TXT_DownloadComplete;
    u8**    TXT_ConfigAnalog;
    u8**    TXT_No_OSPartnumber;
    u8**    TXT_No_Strategy;
    u8**    TXT_Load_DLF_Files;    
    u8**    TXT_UnlockingECU;
    u8**    TXT_ECMStock;
    u8**    TXT_PleaseWait;
    u8**    TXT_SearchFiles;
    u8**    TXT_SelectAFile;
    u8**    TXT_PressWheelToSelect;
    u8**    TXT_TurnWheelToScroll;
    u8**    TXT_ProcessFile;
    u8**    TXT_PressWheelToSelectUnSelect;
    u8**    TXT_PressESCToSave;
    u8**    TXT_SelectPID;
    u8**    TXT_NoPIDs;
    u8**    TXT_StartEngineAndPrepare;
    u8**    TXT_CompleteStopVehicle;
    u8**    TXT_ClearDTCSuccessful;
    u8**    TXT_DLF_Files;
    u8**    TXT_ECU_CAL_DL;
    u8**    TXT_ECU_FULL_DL;
    u8**    TXT_TCU_CAL_DL;
    u8**    TXT_TCU_FULL_DL;
#ifdef BANKS_ENG     
    u8**    TXT_BANKS_Contact_Literal;
    u8**    TXT_BANKS_Disclaimer;
    u8**    TXT_BANKS_Disclaimer_2;
    u8**    TXT_BANKS_Yes_No;
    u8**    TXT_BANKS_Key_ON;
    u8**    TXT_BANKS_Key_OFF;
    u8**    TXT_BANKS_Key_OFF_ESC;
    u8**    TXT_BANKS_Scroll;
    u8**    TXT_BANKS_Begin_Program;
    u8**    TXT_BANKS_Cancel_Program;
    u8**    TXT_BANKS_Load_StockText;
    u8**    TXT_NoDTCInfo;
    u8**    TXT_BANKS_ENT_ESC;
    u8**    TXT_BANKS_Contact;
    u8**    TXT_BANKS_Backlight;  
    u8**    TXT_BANKS_ENT_Description;
    u8**    TXT_BANKS_TurnKeyOn_No_ENT;
#else
    u8**    TXT_SCT_Disclaimer;
    u8**    TXT_SCT_Accept;
    u8**    TXT_TurnKey_ON;
    u8**    TXT_Load_StockText;
#endif    
    u8**    TXT_SetupDevice;
    u8**    TXT_MemStorage;
    u8**    TXT_UNLOCKPleaseWait;
    u8**    TXT_NoDTCFound;
    u8**    TXT_ReturnStockMsg1;
    u8**    TXT_ReturnStockMsg2;
    u8**    TXT_ReturnStockMsg3;
    u8**    TXT_ProgramWithStrategy;
    u8**    TXT_ProgramWithCustom;
    u8**    TXT_On;
    u8**    TXT_Off;
    u8**    TXT_StandbyFlash;
    u8**    TXT_StandbyOff;
    u8**    TXT_SetupECUFail;
    u8**    TXT_DownloadFail;
    u8**    TXT_SCT_SEL_CAN;
    u8**    TXT_BATTERY_LOW;
    u8**    TXT_ReuploadStockfile;
    u8**    TXT_PressESCExit;
    u8**    TXT_DeviceUnlocked;
    u8**    TXT_PressKeyRetryProgram;
    u8**    TXT_OS_Mismatch_1;
    u8**    TXT_OS_Mismatch_2;
    u8**    TXT_OS_Mismatch_3;
    u8**    TXT_OS_Mismatch_4;
    u8**    TXT_OS_Mismatch_5;
    u8**    TXT_TCM_Mismatch_1;
    u8**    TXT_PressESCGoBack;
    u8**    TXT_SetWeight;
    u8**    TXT_WheelResetEscExit;
    u8**    TXT_ReadingOptions;
    u8**    TXT_UploadSuccessful;
    u8**    TXT_UploadFailed;
    u8**    TXT_UploadAborted;
    u8**    TXT_ReadDTCFail;
    u8**    TXT_ClearDTCFail;
    u8**    TXT_ContactName;
    u8**    TXT_ContactPhone;
    u8**    TXT_ContactWeb;
    u8**    TXT_ContactPhoneWeb;
    u8**    TXT_DefaultOptions;
    u8**    TXT_SavedOptions;
    u8**    TXT_AdjustOption;
    u8**    TXT_SkipOption;
    u8**    TXT_KeepPreviousOption;
    u8**    TXT_NoInfo;
    u8**    TXT_Backlight;
    u8**    TXT_Audio;
    u8**    TXT_Contrast;
    u8**    TXT_Sounds_USB_pwr;
    u8**    TXT_SelectOption;
    u8**    TXT_StartMonitor;
    u8**    TXT_SetupMonitor;
    u8**    TXT_Playback;
    u8**    TXT_RecordLog;
    u8**    TXT_DataViewFormat;
    u8**    TXT_SetDataViewFormat;
    u8**    TXT_Yes;
    u8**    TXT_No;
    u8**    TXT_RecordMonitorData;
    u8**    TXT_CommunicationLost;
    u8**    TXT_ResumeDatalog;
    u8**    TXT_Exit;
    u8**    TXT_CommunicationFail;
    u8**    TXT_CheckConnectionAndEngine;
    u8**    TXT_InitializeMonitor;
    u8**    TXT_StartEngine;
    u8**    TXT_Channel1;
    u8**    TXT_Channel2;
    u8**    TXT_SelectEquation;
    u8**    TXT_SelectEquationNote1;
    u8**    TXT_SelectEquationNote2;
    u8**    TXT_EnterValueA;
    u8**    TXT_EnterValueB;
    u8**    TXT_LiveLinkActivated;
    u8**    TXT_CANDetected;
    u8**    TXT_SCPDetected;
    u8**    TXT_VPWDetected;
    u8**    TXT_AlreadyStock;
    u8**    TXT_UpdateTuneRevision;
    u8**    TXT_InitFileSystem;
    u8**    TXT_InvalidFileSystem;
    u8**    TXT_ReloadFileSystem;
    u8**    TXT_Engine_DL;
    u8**    TXT_Transmission_DL;
    u8**    TXT_Auto_Upload;
    u8**    TXT_Calc_Checksum;
    u8**    TXT_Invalid;
    u8**    TXT_Blank;
    u8**    TXT_Unknown;
    u8**    FDR_ExitKeyMsg;
    u8**    FDR_ExitBackKeyMsg;
    u8**    FDR_ExitDoneKeyMsg;
    u8**    FDR_ContinueKeyMsg;
    u8**    FDR_SelectResume;
    u8**    FDR_ContinueMsg;
    u8**    FDR_DoNotUnplug;
    u8**    FDR_PressESCExit;
#ifdef BANKS_ENG
    u8**    FDR_SelectMsg;
    u8**    FDR_ESCExitMsg;
#endif    
}LANGUAGE;
//------------------------------------------------------------------------
/*
extern const u8*    HDR_SetupMonitor;
extern const u8*    HDR_Monitor;
extern const u8*    HDR_EstimatedMPHTime;
extern const u8*    HDR_ClearDiagTroubleCode;
extern const u8*    HDR_ReadDiagTroubleCode;
extern const u8*    HDR_ReadPendingDiagTroubleCode;
extern const u8*    HDR_PIDAlertSetup;
extern const u8*    HDR_VehicleInfo;
extern const u8*    HDR_UploadECU;
extern const u8*    HDR_DownloadTune;
extern const u8*    HDR_DownloadStock;
extern const u8*    HDR_UnlockECU;
extern const u8*    HDR_DealerInfo;
extern const u8*    HDR_DeviceInfo;
extern const u8*    HDR_DeviceSettings;
extern const u8*    HDR_SetupAnalog1;
extern const u8*    HDR_SetupAnalog2;
extern const u8*    HDR_EstimatedQuarterMileTime;
extern const u8*    HDR_ReturnVehicleToStock;
extern const u8*    HDR_MatchingVehicle;
extern const u8*    HDR_SelectFuelType;
extern const u8*    HDR_UserAdjustmentOptions;
extern const u8*    HDR_ProgramVehicleECU;
extern const u8*    HDR_CopyFile;
extern const u8*    HDR_SetupCustomEquation;
extern const u8*    HDR_SetupStorage;
extern const u8*    HDR_SetupECU;
extern const u8*    HDR_ResetECU;
extern const u8*    HDR_VehicleNotSupported;
extern const u8*    HDR_EstimatedHPTQ;

extern const u8*    TXT_ClearDTCSuccessful;
extern const u8*    TXT_ClearDTCFail;
extern const u8*    TXT_StockfileUpdated;
    
extern const u8*    TXT_AdjustOption;
extern const u8*    TXT_SkipOption;
extern const u8*    TXT_KeepPreviousOption;
extern const u8*    FDR_ContinueKeyMsg;
extern const u8*    FDR_ExitKeyMsg;
extern const u8*    FDR_ExitBackKeyMsg;

    //------------------------------------------------------------------------
extern const u8*    TXT_NULLSTR;
extern const u8*    TXT_PressWheelToSelect;
extern const u8*    TXT_PressWheelToSelectUnSelect;
extern const u8*    TXT_PressESCToSave;
extern const u8*    TXT_TurnWheelToScroll;
extern const u8*    TXT_TurnKeyOn;
extern const u8*    TXT_TurnKeyOff;
extern const u8*    TXT_DoNotStartEngine;
extern const u8*    TXT_UploadComplete;
extern const u8*    TXT_PrepareDownload;
extern const u8*    TXT_DownloadComplete;
extern const u8*    TXT_UnlockingECU;
extern const u8*    TXT_ECMStock;
extern const u8*    TXT_PleaseWait;
extern const u8*    TXT_SearchFiles;
extern const u8*    TXT_SelectAFile;
extern const u8*    TXT_ProcessFile;
extern const u8*    TXT_SelectPID;
extern const u8*    TXT_NoPIDs;
extern const u8*    TXT_StartEngine;
extern const u8*    TXT_CompleteStopVehicle;
extern const u8*    TXT_On;
extern const u8*    TXT_Off;
extern const u8*    TXT_StandbyFlash;
extern const u8*    TXT_StandbyOff;
extern const u8*    TXT_SetupECUFail;
extern const u8*    TXT_NoDTCFound;
extern const u8*    TXT_ReturnStockMsg1;
extern const u8*    TXT_ReturnStockMsg2;
extern const u8*    TXT_ReturnStockMsg3;
extern const u8*    TXT_ProgramWithStrategy;
extern const u8*    TXT_ProgramWithCustom;
extern const u8*    TXT_DownloadFail;
extern const u8*    TXT_ReuploadStockfile;
extern const u8*    TXT_DeviceUnlocked;
extern const u8*    TXT_PressWheelRetryProgram;
extern const u8*    TXT_PressESCGoBack;
    //const u8*   PressWheelEscape;
extern const u8*    TXT_SetWeight;
extern const u8*    TXT_WheelResetEscExit;
extern const u8*    TXT_ReadingOptions;
extern const u8*    TXT_ContactName;
extern const u8*    TXT_ContactPhone;
extern const u8*    TXT_ContactWeb;
extern const u8*    TXT_DefaultOptions;
extern const u8*    TXT_SavedOptions;
extern const u8*    TXT_NoInfo;
//------------------------------------------------------------------------
extern const u8*    HDR_ReadStrategy;
extern const u8*    FDR_DoNotUnplug;
*/

extern const LANGUAGE   Language_English;
extern LANGUAGE         *Language;

extern const u8         AnalogMenuIndex[];
extern const u8         *AnalogMenu[5][8];
extern const u8         AnalogUnit[5][8];

void    Language_Set(LANGUAGE_TYPE l);

#endif

//------------------------------------------------------------------------------------------------------------------------------------------------
// EOF
//------------------------------------------------------------------------------------------------------------------------------------------------
