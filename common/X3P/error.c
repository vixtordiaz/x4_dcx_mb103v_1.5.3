/**
  **************** (C) COPYRIGHT 2011 SCT Performance, LLC *********************
  * File Name          : error.c
  * Device / System    : SCT products using the "CommonCode" platform
  * Author             : Quyen Leba
  *
  * Version            : 1 
  * Date               : 05/02/2011
  * Description        : 
  *                    : 
  *
  *
  * History            : 05/02/2011 M. Davis
  *                    :   Add SCT Copyright header,
  *
  ******************************************************************************
  */

#include <ctype.h>       
#include <stdio.h>
#include <common/statuscode.h>
#include <common/settings.h>
#include "error.h"
#include "X3_menus_helper.h"

const u8 ERR_PRELOADTUNEBLOCKED[]       = "1000: Preloaded Tunes blocked. You must use a Custom Tune";
const u8 ERR_READFILE[]                 = "1001: Can't Read File";
const u8 ERR_OPENFILE[]                 = "1002: Can't Open File";
const u8 ERR_CLOSEFILE[]                = "1003: Can't Close File";
const u8 ERR_SEARCHFILE[]               = "1004: Can't Search File";
const u8 ERR_SEEKFILE[]                 = "1005: Can't Seek File";
const u8 ERR_WRITEFILE[]                = "1006: Can't Write File";
const u8 ERR_FILETRANSFER[]             = "1058: Can't Transfer File";
const u8 ERR_NOOPTIONS[]                = "1008: Error Writing Options To Tune";
const u8 ERR_NOTUNE[]                   = "1009: No Tune Changes Written";
const u8 ERR_STRATEGYCHANGES[]          = "1010: Error Writing Strategy Changes";
const u8 ERR_MALLOC[]                   = "1011: Can't Allocate Memory";
const u8 ERR_DECRYPT[]                  = "1012: Decrypt Failed";
const u8 ERR_NOSTRATEGY[]               = "1013: No Strategy";
const u8 ERR_NOSTOCK[]                  = "1014: Stock File Not Found";
const u8 ERR_TUNEFAIL[]                 = "1016: Program Tune Failed";
const u8 ERR_UPLOADSTOCKFAIL[]          = "1017: Upload Stock Failed";
const u8 ERR_READSTRATEGYFAIL[]         = "1018: Read Strategy Failed";
const u8 ERR_UNLOCKFAIL[]               = "1019: Unlock ECM Failed";
const u8 ERR_DTCCLEARFAIL[]             = "1020: Clear DTC Failed";
const u8 ERR_CUSTOMTUNE[]               = "Custom Tune";
const u8 ERR_STRATEGYTUNE[]             = "Strategy Tune";
const u8 ERR_PID[]                      = "1024: Fail to Get PID Info";
const u8 ERR_NOPID[]                    = "1025: No PIDs Selected";
const u8 ERR_MARRIEDFAIL[]              = "1027: Incorrect Vehicle";
const u8 ERR_USERABORT[]                = "1029: User Abort";
const u8 ERR_LOWVBATT[]                 = "1032: Low Battery Voltage";
const u8 ERR_LOWVPP[]                   = "1041: Low Programming Voltage";
const u8 ERR_DOWNLOADFAILDETECTED[]     = "Download Failure Detected.  Please Return Vehicle to Stock IF the Vehicle Does Not Start";
const u8 ERR_DNLDFAILDETECTED[]         = "Please Return Vehicle to Stock";
const u8 ERR_NOSECURITYBYTE[]           = "1035: ECM Has Been Updated";
const u8 ERR_INVALIDTUNE[]              = "1036: Invalid Tune for Married Vehicle";
const u8 ERR_STOCKFILECORRUPT[]         = "1037: Stock File Corrupt";
const u8 ERR_FLASHUPDATE[]              = "1038: Flash File Update Failed";
const u8 ERR_INVALIDBINARY[]            = "1039: Invalid Binary Type Your Device may need an update";   
const u8 ERR_NOCUSTOM[]                 = "1040: No Custom Tunes";
const u8 MSG_CONTACT1[]                 = "Contact Technical Support";
const u8 ERR_NOCUSTOMTUNE1_A[]          = "Please use\ndealer locator at www.sctflash.com";
const u8 ERR_NOCUSTOMTUNE1_B[]          = "to find Custom tuning dealer near you.";
const u8 ERR_NOSTRATEGYTUNES[]          = "No Strategy Tunes Found";
const u8 ERR_INVALIDBINARYMARRIED[]     = "Invalid Binary Type for Married Vehicle";
const u8 ERR_OLDSTRATEGYFILES[]         = "Strategy files are old, please update to newest tune revision.";

#ifdef BANKS_ENG
  const u8 ERR_SAVESETTINGS[]           = "Error saving settings Power cycle device. If problem persists please contact:\nBANKS Tech Support";
  const u8 ERR_FILESYSTEMCORRUPT[]      = "Filesystem Corrupt. Need to format memory\nPlease contact:\nBANKS Tech Support\n1-800-GET-POWER";
  const u8 MSG_CONTACT_98[]             = "Please Contact:\nBANKS Tech Support\n1-800-GET-POWER";
  const u8 MSG_CONTACT2[]               = "";
  const u8 ERR_DEVICELOCKED[]           = "Number of unlocks exceeded\nPlease contact Banks support";

#else
  const u8 ERR_SAVESETTINGS[]           = "Err saving settings. Power cycle device. If problem persists contact tech support:\n(407) 774-2447";
  const u8 ERR_FILESYSTEMCORRUPT[]      = "Filesystem corrupt. Need to format memory. Please contact support:\n(407) 774-2447";
  const u8 MSG_CONTACT_98[]             = "Contact:\nSCT Technical Support";
  const u8 MSG_CONTACT2[]               = "\n(407) 774-2447   www.sctflash.com";
  const u8 ERR_DEVICELOCKED[]           = "Number of unlocks exceeded\nPlease contact customer support";
#endif

const u8 ERR_INVALIDCOMM[]              = "Invalid Communication";
const u8 ERR_INITDOWNLOAD[]             = "Fail to Init Download";
const u8 ERR_CUSTOMTUNENOTSUPPORTED[]   = "Custom Tune Not Supported";
const u8 ERR_STRATEGYTUNENOTSUPPORTED[] = "Strategy Tune Not Supported";
const u8 ERR_OSPARTNUMBERFAIL[]         = "1013: Read OS partnumber Failed";
const u8 ERR_FLASHTYPENOTSET[]          = "Device failed to set Flash type.";
const u8 ERR_DEVICE_MARRIED[]           = "Device is married.\nReturn to stock first.";

//                                      = "0123456789012345678901234567890123456789";

//------------------------------------------------------------------------------
// Inputs:  ERRORCODE errorcode
//          u32 numbercode
// Display error message
// Engineer: Quyen Leba
//------------------------------------------------------------------------------
void ERROR_MessageCode(ERRORCODE errorcode, u32 numbercode)
{
    u8* msg = "";
    u8* msg1 = "";
    u8* msg2 = "";
    u8  header[16];
    u8  temp[80];
    u8  temp2[120];

    memset(temp,  0x00, 80);
    memset(temp2, 0x00, 120);
        
    switch(errorcode)
    {
    case DEBUG:
        msg = "Oops!!!";
        break;
    case READFILE:
        msg = (void*)ERR_READFILE;
        break;
    case OPENFILE:
        msg = (void*)ERR_OPENFILE;
        break;
    case CLOSEFILE:
        msg = (void*)ERR_CLOSEFILE;
        break;
    case SEARCHFILE:
        msg = (void*)ERR_SEARCHFILE;
        break;
    case SEEKFILE:
        msg = (void*)ERR_SEEKFILE;
        break;
    case WRITEFILE:
        msg = (void*)ERR_WRITEFILE;
        break;
    case FILETRANSFER:
        msg = (void*)ERR_FILETRANSFER;
        break;
    case NOTUNE:
        msg = (void*)ERR_NOTUNE;
        break;
    case MALLOC:
        msg = (void*)ERR_MALLOC;
        break;
    case DECRYPT:
        msg = (void*)ERR_DECRYPT;
        break;
    case NOSTRATEGY:
        msg = (void*)ERR_NOSTRATEGY;
        break;
    case NOSTOCK:
        msg = (void*)ERR_NOSTOCK;
        break;
    case TUNEFAIL:
        msg = (void*)ERR_TUNEFAIL;
        break;
    case UPLOADSTOCKFAIL:
        msg = (void*)ERR_UPLOADSTOCKFAIL;
        break;
    case READSTRATEGYFAIL:
        msg = (void*)ERR_READSTRATEGYFAIL;
        break;
    case UNLOCKFAIL:
        msg = (void*)ERR_UNLOCKFAIL;
        break;
    case DTCCLEARFAIL:
        msg = (void*)ERR_DTCCLEARFAIL;
        break;
    case NOCUSTOMTUNE_A:
#ifdef BANKS_ENG
        msg = (void*)ERR_NOCUSTOM;
        msg1 = (void*) MSG_CONTACT_98;
#else
        msg = "1023:";
        msg1 = (void*)ERR_NOCUSTOMTUNE1_A;
#endif
        break;
    case NOCUSTOMTUNE_B:
        msg = "1023:";
        msg1 = (void*)ERR_NOCUSTOMTUNE1_B;
        break;
    case PID:
        msg = (void*)ERR_PID;
        break;
    case NOPID:
        msg = (void*)ERR_NOPID;
        break;
    case INVALIDBINARY:
        msg = (void*)ERR_INVALIDBINARY;
        break;
    case MARRIEDFAIL:
        msg = (void*)ERR_MARRIEDFAIL;
        break;
    case USERABORT:
        msg = (void*)ERR_USERABORT;
        break;
    case LOWVBATT:
        msg = (void*)ERR_LOWVBATT;
        break;
    case LOWVPP:
        msg = (void*)ERR_LOWVPP;
        break;
    case DOWNLOADFAILDETECTED:
        msg = (void*)ERR_DOWNLOADFAILDETECTED;
        break;
    case NOSECURITYBYTE:
        msg  = (void*)ERR_NOSECURITYBYTE;
        msg1 = (void*)MSG_CONTACT_98;
        msg2 = (void*)MSG_CONTACT2;
        break;
    case INVALIDTUNE:
        msg = (void*)ERR_INVALIDTUNE;
        break;
    case STOCKFILECORRUPT:
        msg = (void*)ERR_STOCKFILECORRUPT;     // = "1037: Stock File Corrupt";
        msg1 = (void*)MSG_CONTACT_98;          // = "\nContact:\nBANKS Tech Support";
        msg2 = (void*)MSG_CONTACT2;
        break;
    case PRELOADTUNEBLOCKED:
        msg = (void*)ERR_PRELOADTUNEBLOCKED;
        break;
    case DEVICELOCKED:
        msg = (void*)ERR_DEVICELOCKED;
        break;                                                                      
    case FLASHUPDATE:
        msg = (void*)ERR_FLASHUPDATE;
        break;
    case NOSTRATEGYTUNE:
        msg = (void*)ERR_NOSTRATEGYTUNES;
        break;
    case NOSTRATEGYCHANGES:
        msg = (void*)ERR_STRATEGYCHANGES;
        break;
    case INVALIDBINARYMARRIED:
        msg = (void*)ERR_INVALIDBINARYMARRIED;
        break;
    case NOCHANGES:
        msg = (void*)ERR_NOOPTIONS;
        break;
    case SAVESETTINGS:
        msg = (void*)ERR_SAVESETTINGS;
        break;
    case FILESYSTEMCORRUPT:
        msg = (void*)ERR_FILESYSTEMCORRUPT;
        break;
    case INVALIDCOMM:
        msg = (void*)ERR_INVALIDCOMM;
        break;
    case INITDOWNLOAD:
        msg = (void*)ERR_INITDOWNLOAD;
        break;
    case STRATEGYTUNENOTSUPPORTED:
        msg = (void*)ERR_STRATEGYTUNENOTSUPPORTED;
        break;
    case CUSTOMTUNENOTSUPPORTED:
        msg = (void*)ERR_CUSTOMTUNENOTSUPPORTED;
        break;
    case OSPARTNUMBERFAIL:
        msg = (void*)ERR_OSPARTNUMBERFAIL;
        break;
    case FLASHTYPENOTSET:
        msg = (void*)ERR_FLASHTYPENOTSET;
        break;
    case DEVICE_MARRIED:
        msg = (void*)ERR_DEVICE_MARRIED;
        break;
    case OLDSTRATEGYFILES:
        msg = (void*)ERR_OLDSTRATEGYFILES;
        break;
        
    default:
        break;
    }

    strcpy((char*)header,"ERROR");
    if (numbercode > 0 && numbercode < 100000)
    {
        sprintf((char*)&header[5]," %d",numbercode);
    }

    ShowMessage(SW_ENTER,ERRORMSG,(void*)header,
                (void*)*Language->FDR_ContinueKeyMsg,msg,msg1,msg2,CLEAR_ALL);
}

//------------------------------------------------------------------------------
// Display error message
// Engineer: Quyen Leba
//------------------------------------------------------------------------------
void ERROR_Message(ERRORCODE errorcode)
{
    u8* msg = "";
    u8* msg1 = "";
    u8* msg2 = "";
    u8* header = "ERROR";
    u8  temp[80];
    u8  temp2[120];

    memset( temp,  0x00, 80 );
    memset( temp2, 0x00, 120 );
        
    switch(errorcode)
    {
    case DEBUG:
        msg = "some error";
        break;
    case READFILE:
        msg = (void*)ERR_READFILE;
        break;
    case OPENFILE:
        msg = (void*)ERR_OPENFILE;
        break;
    case CLOSEFILE:
        msg = (void*)ERR_CLOSEFILE;
        break;
    case SEARCHFILE:
        msg = (void*)ERR_SEARCHFILE;
        break;
    case SEEKFILE:
        msg = (void*)ERR_SEEKFILE;
        break;
    case WRITEFILE:
        msg = (void*)ERR_WRITEFILE;
        break;
    case FILETRANSFER:
        msg = (void*)ERR_FILETRANSFER;
        break;
    case NOTUNE:
        msg = (void*)ERR_NOTUNE;
        break;
    case MALLOC:
        msg = (void*)ERR_MALLOC;
        break;
    case DECRYPT:
        msg = (void*)ERR_DECRYPT;
        break;
    case NOSTRATEGY:
        msg = (void*)ERR_NOSTRATEGY;
        break;
    case NOSTOCK:
        msg = (void*)ERR_NOSTOCK;
        break;
    case TUNEFAIL:
        msg = (void*)ERR_TUNEFAIL;
        break;
    case UPLOADSTOCKFAIL:
        msg = (void*)ERR_UPLOADSTOCKFAIL;
        break;
    case READSTRATEGYFAIL:
        msg = (void*)ERR_READSTRATEGYFAIL;
        break;
    case UNLOCKFAIL:
        msg = (void*)ERR_UNLOCKFAIL;
        break;
    case DTCCLEARFAIL:
        msg = (void*)ERR_DTCCLEARFAIL;
        break;
    case NOCUSTOMTUNE_A:
#ifdef BANKS_ENG
        msg = (void*)ERR_NOCUSTOM;
        msg1 = (void*) MSG_CONTACT_98;
#else
        msg = "1023:";
        msg1 = (void*)ERR_NOCUSTOMTUNE1_A;
#endif
        break;
    case NOCUSTOMTUNE_B:
        msg = "1023:";
        msg1 = (void*)ERR_NOCUSTOMTUNE1_B;
        break;
    case PID:
        msg = (void*)ERR_PID;
        break;
    case NOPID:
        msg = (void*)ERR_NOPID;
        break;
    case INVALIDBINARY:
        msg = (void*)ERR_INVALIDBINARY;
        break;
    case MARRIEDFAIL:
        msg = (void*)ERR_MARRIEDFAIL;
        break;
    case USERABORT:
        msg = (void*)ERR_USERABORT;
        break;
    case LOWVBATT:
        msg = (void*)ERR_LOWVBATT;
        break;
    case LOWVPP:
        msg = (void*)ERR_LOWVPP;
        break;
    case DOWNLOADFAILDETECTED:
        msg = (void*)ERR_DOWNLOADFAILDETECTED;
        break;
    case NOSECURITYBYTE:
        msg  = (void*)ERR_NOSECURITYBYTE;
        msg1 = (void*)MSG_CONTACT_98;
        msg2 = (void*)MSG_CONTACT2;
        break;
    case INVALIDTUNE:
        msg = (void*)ERR_INVALIDTUNE;
        break;
    case STOCKFILECORRUPT:
        msg = (void*)ERR_STOCKFILECORRUPT;     // = "1037: Stock File Corrupt";
        msg1 = (void*)MSG_CONTACT_98;          // = "\nContact:\nBANKS Tech Support";
        msg2 = (void*)MSG_CONTACT2;
        break;
    case PRELOADTUNEBLOCKED:
        msg = (void*)ERR_PRELOADTUNEBLOCKED;
        break;
    case DEVICELOCKED:
        msg = (void*)ERR_DEVICELOCKED;
        break;                                                                      
    case FLASHUPDATE:
        msg = (void*)ERR_FLASHUPDATE;
        break;
    case NOSTRATEGYTUNE:
        msg = (void*)ERR_NOSTRATEGYTUNES;
        break;
    case NOSTRATEGYCHANGES:
        msg = (void*)ERR_STRATEGYCHANGES;
        break;
    case INVALIDBINARYMARRIED:
        msg = (void*)ERR_INVALIDBINARYMARRIED;
        break;
    case NOCHANGES:
        msg = (void*)ERR_NOOPTIONS;
        break;
    case SAVESETTINGS:
        msg = (void*)ERR_SAVESETTINGS;
        break;
    case FILESYSTEMCORRUPT:
        msg = (void*)ERR_FILESYSTEMCORRUPT;
        break;
    case INVALIDCOMM:
        msg = (void*)ERR_INVALIDCOMM;
        break;
    case INITDOWNLOAD:
        msg = (void*)ERR_INITDOWNLOAD;
        break;
    case STRATEGYTUNENOTSUPPORTED:
        msg = (void*)ERR_STRATEGYTUNENOTSUPPORTED;
        break;
    case CUSTOMTUNENOTSUPPORTED:
        msg = (void*)ERR_CUSTOMTUNENOTSUPPORTED;
        break;
    case OSPARTNUMBERFAIL:
        msg = (void*)ERR_OSPARTNUMBERFAIL;
        break;
    case FLASHTYPENOTSET:
        msg = (void*)ERR_FLASHTYPENOTSET;
        break;
    case DEVICE_MARRIED:
        msg = (void*)ERR_DEVICE_MARRIED;
        break;
    case OLDSTRATEGYFILES:
        msg = (void*)ERR_OLDSTRATEGYFILES;
        break;
        
    default:
        break;
    }

    // Move Error-Code to "Title Screen" to provide more room on "Text Screen"
    if( isdigit( msg[0] ))
    {
        sprintf( (char*)temp, "%8s", "ERROR: " ); 
        memcpy( &temp[8], &msg[0], 4 );
        header = temp;
        memcpy( &temp2[0], &msg[5], 120 );
        msg = temp2;
    }        

    ShowMessage(SW_ENTER,ERRORMSG,(void*)header,
                (void*)*Language->FDR_ContinueKeyMsg,msg,msg1,msg2,3);
}

//------------------------------------------------------------------------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------------------------------------------------------------------------
void ERROR_DownloadFail(void)
{
    //TODOQ: more on this, assign special enum
    //if(settings.DownloadFailed == 0x55)
    if (SETTINGS_IsDownloadFail())
    {
        //TODO: msg
#ifdef BANKS_ENG
        ShowMessage(SW_ENTER,ERRORMSG,(void*)"ERROR",                           // 1 
                    (void*)*Language->FDR_ContinueKeyMsg,
                    "Firmware Updated",
                    (void*)ERR_DNLDFAILDETECTED,"Prior to Downloading New Tune",3);
#else
        ShowMessage(SW_ENTER,ERRORMSG,(void*)"ERROR",                           // 2
                    (void*)*Language->FDR_ContinueKeyMsg,
                    "Firmware Updated",
                    "Please Return Vehicle\nto Stock, Prior to\nDownloading New Tune",
                    "",3);
#endif                    
    }
    else
    {
        ERROR_Message(DOWNLOADFAILDETECTED);
    }
}

//------------------------------------------------------------------------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------------------------------------------------------------------------
void ERROR_UnsupportVehicle(u8* tune, u8 upload_stock)
{
    u8 status = S_FAIL;
    u8 c;
    u8 k=0;
    u8 tbuff[10];
    u8 *text;

    if((upload_stock)&&(!SETTINGS_IsMarried()))
    {
        //TODOQ: status = Upload_unsupported_Stock(1);
        if(status == S_ALREADY_UNLOCKED)
        {
            text = (void*)*Language->TXT_DeviceUnlocked;

            ShowMessage(SW_EXIT,ERRORMSG,(void*)*Language->HDR_VehicleNotSupported,
                (void*)*Language->FDR_ExitKeyMsg,
                text,
                (void*)*Language->TXT_NULLSTR,
                (void*)*Language->TXT_NULLSTR,CLEAR_TOP);

            text = (void*)*Language->TXT_UploadFailed;
        }
        if(status == S_FAIL)
          text = (void*)*Language->TXT_UploadFailed;
        else if(status == S_SUCCESS)
          text = (void*)*Language->TXT_UploadSuccessful;
        else
          text = (void*)*Language->TXT_NULLSTR;
          
#ifdef BANKS_ENG                                                                // 3  
    ShowMessage(NOKEY,ERRORMSG,(void*)*Language->HDR_VehicleNotSupported,
                (void*)*Language->TXT_NULLSTR,
                (void*)*Language->TXT_NULLSTR,
                text,
                (void*)*Language->TXT_BANKS_Contact,CLEAR_TOP);
#else                                                                           // 4
    ShowMessage(NOKEY,ERRORMSG,(void*)*Language->HDR_VehicleNotSupported,
                (void*)*Language->FDR_ExitKeyMsg,
                (void*)*Language->TXT_NULLSTR,
                text,
                (void*)*Language->TXT_ContactPhoneWeb,CLEAR_TOP);
#endif

    }
    else
    {
#ifdef BANKS_ENG                                                                // 3  
    ShowMessage(NOKEY,ERRORMSG,(void*)*Language->HDR_VehicleNotSupported,
                (void*)*Language->TXT_NULLSTR,
                (void*)*Language->TXT_NULLSTR,
                (void*)*Language->TXT_NULLSTR,
                (void*)*Language->TXT_BANKS_Contact,CLEAR_TOP);
#else                                                                           // 4
    ShowMessage(NOKEY,ERRORMSG,(void*)*Language->HDR_VehicleNotSupported,
                (void*)*Language->FDR_ExitKeyMsg,
                (void*)*Language->TXT_NULLSTR,
                (void*)*Language->TXT_NULLSTR,
                (void*)*Language->TXT_ContactPhoneWeb,CLEAR_TOP);
#endif
    }
    LCD_gputscpos(0,2,"Vehicle Code:");

    // Display the strategy on the Screen
        c = 0;
        k=0;
        memset(tbuff, 0, 10);

        do
        {
            tbuff[c++] = tune[k++];
            if(c > 9)
              break;
        }while(tune[k] != '*');

        LCD_gputscpos(10,2,tbuff);

    while(1)
    {
        status = getKeyWait(SW_EXIT,TRUE);
        if((status == SW_EXIT)||(status == SW_ENTER))
        {
            break;
        }
    }
    return;
}

//---------------------------------------------------------------------------------------------------------------------------
// Note: If security byte error is found this function will give the user the option to re-upload stock
//---------------------------------------------------------------------------------------------------------------------------
void ERROR_SecurityByte(void)
{
    u8  status  = 0;

    ERROR_Message(NOSECURITYBYTE);
#ifdef BANKS_ENG                                                        // 5
    status = ShowMessage(SW_ENTER,ERRORMSG,(void*)*Language->HDR_StockfileUpdated,
                         (void*)*Language->FDR_ContinueKeyMsg,
                         (void*)*Language->TXT_ReuploadStockfile,
                         (void*)*Language->TXT_PressESCExit,
                         (void*)*Language->TXT_NULLSTR,CLEAR_ALL);
#else                                                                           // 6
    status = ShowMessage(SW_ENTER,ERRORMSG,(void*)*Language->HDR_StockfileUpdated,
                         (void*)*Language->TXT_NULLSTR,
                         (void*)*Language->TXT_NULLSTR,
                         (void*)*Language->TXT_ReuploadStockfile,
                         (void*)*Language->TXT_SCT_SEL_CAN,CLEAR_ALL);

#endif
    if (status == SW_ENTER)
    {
        SETTINGS_ClearMarried();
        SETTINGS_SetTuneAreaDirty();
        settings_update(FALSE);
#ifdef BANKS_ENG
        ShowMessage(SW_ENTER,NOAUDIO,(void*)*Language->HDR_StockfileUpdated,    // 7
                    (void*)*Language->TXT_NULLSTR,
                    (void*)*Language->TXT_DeviceUnlocked,
                    (void*)*Language->TXT_PressKeyRetryProgram,
                    (void*)*Language->TXT_NULLSTR,CLEAR_ALL);
#else
         ShowMessage(SW_ENTER,NOAUDIO,(void*)*Language->HDR_StockfileUpdated,   // 8
                    (void*)*Language->TXT_NULLSTR,
                    (void*)*Language->TXT_DeviceUnlocked,
                    (void*)*Language->TXT_NULLSTR,
                    (void*)*Language->TXT_PressKeyRetryProgram,CLEAR_ALL);
#endif                    
    }
}

//------------------------------------------------------------------------------------------------------------------------------------------------
// EOF
//------------------------------------------------------------------------------------------------------------------------------------------------
