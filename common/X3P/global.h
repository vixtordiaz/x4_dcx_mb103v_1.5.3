/**
  **************** (C) COPYRIGHT 2011 SCT Performance, LLC *********************
  * File Name          : global.h
  * Device / System    : SCT products using the "CommonCode" platform
  * Author             : Quyen Leba
  *
  * Version            : 1 
  * Date               : 05/02/2011
  * Description        : 
  *                    : 
  *
  *
  * History            : 05/02/2011 M. Davis
  *                    :   Add SCT Copyright header,
  *
  ******************************************************************************
  */

#ifndef __GLOBAL_H
#define __GLOBAL_H

//------------------------------------------------------------------------------------------------------------------------------------------------
// Communication
//------------------------------------------------------------------------------------------------------------------------------------------------
//enum
//{
//    SCP_COMM,
//    SCP32_COMM,
//    VPW_COMM,
//    CAN_COMM,
//    MSCAN_COMM,
//    SCI_COMM,
//    ISO9141_COMM,
//    ISO14230_COMM,
//    UNKNOWN_COMM,
//};

//---------------------------------------------------------------------------------------------------------------------------
//
//---------------------------------------------------------------------------------------------------------------------------
#define DATALOG_MAXITEMS        20      //max # of PIDs selected
#define DATALOG_MAXLINE         5       //max # of PIDs data displayed on LCD
#define DATALOG_MAXITEMSINDLF   22      //max # of PIDs from DLF
#define DATALOG_MAXANALOGCH     2       //max # of analog channels
#define DATALOG_MAXBLOCKCOUNT   0x800   //max # of block (x512bytes) can save

enum
{
    UNKNOWN_STYLE               = 0,
    OLD_FORD_STYLE              = 1,
    NEW_AM_STYLE                = 2
};
enum
{
    NOTSET                      = 0,
    CAL_DOWNLOAD                = 1,
    FULL_DOWNLOAD               = 2
};
enum
{
    STOCK_FILE                  = 0,
    TUNEFILE4                   = 1
};

#endif  //__GLOBAL_H