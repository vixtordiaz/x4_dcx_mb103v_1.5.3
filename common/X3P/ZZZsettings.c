/**
  **************** (C) COPYRIGHT 2011 SCT Performance, LLC *********************
  * File Name          : ZZZsettings.c
  * Device / System    : SCT products using the "CommonCode" platform
  * Author             : Quyen Leba
  *
  * Version            : 1 
  * Date               : 05/02/2011
  * Description        : 
  *                    : 
  *
  *
  * History            : 05/02/2011 M. Davis
  *                    :   Add SCT Copyright header,
  *
  ******************************************************************************
  */

#include "91x_fmi.h"
#include <board/X3P/system.h>
#include <common/statuscode.h>
#include <common/X3P/port.h>
#include <common/X3P/global.h>
#include "settings.h"
//#include "usb_hw.h"

//TODOQ: for now
#define USB_PID     0x3101
//TODOQ: why need this?
extern u8   Serial_number[13];

//TODO: use this
#define FMI_SETTINGS_STARTADDR  (0x00080000)
#define FMI_SETTINGS_ADDR_S0    (FMI_SETTINGS_STARTADDR)
#define FMI_SETTINGS_ADDR_S1    (FMI_SETTINGS_ADDR_S0+8*1024)
#define FMI_SETTINGS_ADDR_S2    (FMI_SETTINGS_ADDR_S1+8*1024)
#define FMI_SETTINGS_ADDR_S3    (FMI_SETTINGS_ADDR_S2+8*1024)
#define SETTINGS_ADDRESS        (FMI_SETTINGS_ADDR_S3)
#define SETTINGS_SECTOR         (FMI_B1S3)

const Settings defsettings =
{
    .Sct                = 'S','C','T',
    .Contrast           = 185,
    .Backlight          = 1600,
    .eec_type           = 0xFF,
    .comm_type          = 0xFF,
    .married            = 0,
    .married_cnt        = 0,
    .cur_strat          = {0},
    .num_of_proc        = 0,
    .Dwnld_Fail         = 0,
    .VID                = {0},
    .EPATS              = {0},
    .VIN                = {0},
    .RunBootloader      = 0,
    .serial_num         = {0},
    .Description        = {0},
    .RPM_flag           = 0,
    .RPM_int            = 2500,
    .dev_bit            = 0,
    .stockfilesize      = 0,
    .selected_PIDs      = {0xFF,0xFF,0xFF,0xFF,0xFF,0xFF},
    .car_type           = 0,
    .prev_VIN           = {'Q','Q','Q','Q','Q','Q','Q','Q','Q','Q','Q','Q','Q','Q','Q','Q','Q'},
    .PL_Enable          = 0,
    .PL_Flash_Enable    = 0,
    .Alert_Enable       = 0,
    .Alert_watch        = {0xFF,0xFF,0xFF,0xFF,0xFF,0xFF},
    .Alert_MAX          = {1.0,1.0,1.0,1.0,1.0,1.0},
    .A1_on              = 0,
    .A2_on              = 0,
    .Equate1            = 0,
    .Equate2            = 0,
    .Analog1_Des        = {0},
    .Analog2_Des        = {0},
    .A1_Units           = {0},
    .A2_Units           = {0},
    .A1_selected_num    = 0xFF,
    .A2_selected_num    = 0xFF,
#ifdef NDEBUG
    .DebugProtectStatus = 0x55,   // Set the Debug protection to ON if release
#else
    .DebugProtectStatus = 0x00,
#endif
    .FWVersion          = 0,
    .strategy_filename  = {0},
    .packedDeviceCfg  = 0,
    .DLF_filename       = {0},
    .DLfilesize         = {0,0,0,0,0,0,0,0,0,0},
    .datalogprecision   = 2,
    .record_monitor     = 0,
    .record_file        = {0,0,0,0,0,0,0,0,0,0,0,0,0,0},
    .AnalogCustomEq     = {{{1,1},{1,1},{0,0}},{{1,1},{1,1},{0,0}}},
    .battery            = 0,
    .user_okd_Lvol      = 0,    //User ok'd using low battery volatge to flash vehicle, even after warning
    .SplashScreen       = {0},
    .VID_can            = {0},
    .Tunefilesize       = {0,0,0},
    //.SF_block           = SF_STATUS,
    .bootloader         = {0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF},
    .Selected_PIDs      = {0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,
                           0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,
                           0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF
                          },
#ifdef SITRONIX_LCD         // LCD controller defined in port.h                 
    .LCD_Type           = SITRONIX,                              
#else
    .LCD_Type           = SAMSUNG,        
#endif
    .Trans_strat        = {0},
};
const SettingsFlags defsettingsFlags =
{
    .BuzzerDisabledUSB  = TRUE, // Sound disabled while USB-powered
    .PreviousMarketType = 0xFF,
    .PreviousDeviceType = 0,
    .PreviousFWVersion  = {0},
};
Settings    settings;
SettingsFlags settingsFlags;

// pad the settings area to 1024 bytes
// This array is initialized with zeroes in Settings_CheckPriorFirmwareUpdate(), which is always called during device start-up. 
u8 settingsPad[SETTINGS_LENGTH - (sizeof(Settings) + sizeof(SettingsFlags))];

//------------------------------------------------------------------------------------------------------------------------------------------------
// Restore settings to to factory-default values, with the 
// MarketType and DeviceType values defined in port.h
//
// !! NOTE !!
// This function is only called from the USB_CMD_INIT_SETTINGS case of USB_CMDS(), 
// in usbcmds.c. The calling function preserves the device serial number.
// 
//------------------------------------------------------------------------------------------------------------------------------------------------
void Settings_Init(void)
{
    memcpy((void*)&settings,(void*)&defsettings,sizeof(Settings));

    // Set the MarketType and DeviceType fields from the corresponding port.h #defines
    settings.MarketType = MARKET_TYPE;             
    settings.DeviceType = DEVICE_TYPE;
    
    // packedDeviceCfg is used by Advantage III desktop software
    settings.packedDeviceCfg = MARKET_TYPE | DEVICE_TYPE;
    
    SaveSettings();
}

//------------------------------------------------------------------------------------------------------------------------------------------------
// Read device serial # in OTP flash memory
// Output:  u8  *serial     to put serial number of device (u8 serial[13])
//------------------------------------------------------------------------------------------------------------------------------------------------
void Settings_ReadSerialOTP(u8 *serial)
{
    u32 s[8];

    s[0] = FMI_ReadOTPData(FMI_OTP_WORD_0);
    s[1] = FMI_ReadOTPData(FMI_OTP_WORD_1);
    s[2] = FMI_ReadOTPData(FMI_OTP_WORD_2);
    s[3] = FMI_ReadOTPData(FMI_OTP_WORD_3);
    s[4] = FMI_ReadOTPData(FMI_OTP_WORD_4);
    s[5] = FMI_ReadOTPData(FMI_OTP_WORD_5);
    s[6] = FMI_ReadOTPData(FMI_OTP_WORD_6);
    s[7] = FMI_ReadOTPData(FMI_OTP_WORD_7);

    memcpy((void*)serial,(void*)s,13);
}

//------------------------------------------------------------------------------------------------------------------------------------------------
// Write device serial # to OTP flash memory
// Input:   u8  *serial     the serial number to save to OTP (u8 serial[13])
//------------------------------------------------------------------------------------------------------------------------------------------------
void Settings_WriteSerialOTP(u8 *serial)
{
    u16 s[7];
    u8  i,addr;

    memset((void*)s,0xFF,7*2);
    memcpy((void*)s,(void*)serial,13);

    addr = 0;
    for(i=0;i<7;i++)
    {
        //FMI_OTP_LOW_HALFWORD_0 = 0, then 2, etc
        FMI_WriteOTPHalfWord(addr, s[i]);
        FMI_WaitForLastOperation(FMI_BANK_1);
        addr += 2;
    }
    memcpy((void*)settings.serial_num,(void*)serial,13);
    SaveSettings();
}

//------------------------------------------------------------------------------------------------------------------------------------------------
// TODO: use Patrick new SaveSettings()
//------------------------------------------------------------------------------------------------------------------------------------------------
void SaveSettings(void)
{
    u32 i;
    u16 *ptr = (u16*)&settings;
    u32 FlashAddr = SETTINGS_ADDRESS;

    FMI_WriteProtectionCmd(SETTINGS_SECTOR,DISABLE);    //disable flash protection
    FMI_EraseSector(SETTINGS_SECTOR);                   //erase bank 1 sector 3
    FMI_WaitForLastOperation(FMI_BANK_1);

    for (i=0;i<SETTINGS_LENGTH;i+=2)
    {
        FMI_WriteHalfWord(FlashAddr,*ptr);
        FlashAddr+=2;
        ptr++;
        FMI_WaitForLastOperation(FMI_BANK_1);
    }
}

//------------------------------------------------------------------------------------------------------------------------------------------------
// Retrieve the entire settings area from Flash memory
//------------------------------------------------------------------------------------------------------------------------------------------------
void LoadSettings(void)
{
    memcpy((void*)&settings,(void*)SETTINGS_ADDRESS,SETTINGS_LENGTH);
}

//------------------------------------------------------------------------------------------------------------------------------------------------
// Compare to VIN saved in settings
// Input:   u8  *vin
// Return:  u8  status      (S_SUCCESS, S_FAIL)
// Engineer: Quyen Leba
// Date: Feb 29, 2008
//------------------------------------------------------------------------------------------------------------------------------------------------
u8 Settings_CompareVIN(u8 *vin)
{
    u8  i;

    for(i=0;i<13;i++)
    {
        if (Serial_number[i] != settings.VID[i])
        {
            return S_FAIL;
        }
    }

    for(i=0;i<17;i++)
    {
        if (vin[i] != settings.VIN[i])
        {
            return S_FAIL;
        }
    }
    return S_SUCCESS;
}

//------------------------------------------------------------------------------------------------------------------------------------------------
// check for firmware-update prior to this start-up, using previous Market-Byte, Device-Type and Firmware_Version[] values
// If any of these settings have changed, force the "Sound (USB power)" option to OFF. 
//
// Input:  none
// Output: none
//
// Engineer: Mark Davis
//------------------------------------------------------------------------------------------------------------------------------------------------
void Settings_CheckPriorFirmwareUpdate(void)
{
    bool settingsChanged = FALSE; 
    
    // initialize settingsPad[] with zeroes
    memset((void*)settingsPad,0,sizeof(settingsPad));  

    // If the Device-Type or Market-Byte have changed, reset the settings area in Flash memory, while preserving
    // the serial-number, number or unlocks, married status, LCD_Type. Force the "Sound (USB power)" option to OFF.
    // Preserve the "previous" MarketType and DeviceType settings, to dis-allow changing device-type firmware
    // via the updater.
    if ( settings.MarketType != settingsFlags.PreviousMarketType || 
                                  settings.DeviceType != settingsFlags.PreviousDeviceType )
    {  
        // update the "previous" Device-Type Market-Byte values in settings. 
        settingsFlags.PreviousMarketType = MARKET_TYPE;;
        settingsFlags.PreviousDeviceType = DEVICE_TYPE;;
                        
        settingsChanged = TRUE;
    }

    if (memcmp((void*)settingsFlags.PreviousFWVersion, (void*)Firmware_Version, sizeof(Firmware_Version))!= 0)
    {
        memcpy((void*)settingsFlags.PreviousFWVersion,(void*)Firmware_Version,sizeof(Firmware_Version));

        settingsChanged = TRUE;
    }
    

    //  If Device-Type, Market-Byte or the Firmware Version-Number have changed, signifying a download,
    //  set the "Sound (USB power)" option to OFF, and save settings.    
    if(settingsChanged)
    {  
        // force the "Sound (USB power)" option to OFF
        settingsFlags.BuzzerDisabledUSB = TRUE;
        
        SaveSettings();
    }
}

//------------------------------------------------------------------------------------------------------------------------------------------------
// Enable bootload without keypress
// Input:   u8  mode    (0:normal, 1: app , 2: custom)
// Engineer: Quyen Leba
//------------------------------------------------------------------------------------------------------------------------------------------------
void Settings_SetBootloader(u8 mode, u16 custompid)
{
    const u8    bootload_sign_01[8] = {0xAA,0x55,0x5A,0xA5,
                                       0x55,0xAA,0xA5,0x5A};
    u8          bootload_sign_02[8] = {0x55,0x99,0xA5,0x5A,
                                       0xAA,0x55,0,0};

    if (mode == 1)          //app: use app pid
    {
        bootload_sign_02[6] = (u8)(USB_PID >> 8);
        bootload_sign_02[7] = (u8)(USB_PID & 0xFF);
        memcpy((void*)settings.bootloader,(void*)bootload_sign_02,8);
    }
    else if (mode == 2)     //custom: use custompid
    {
        bootload_sign_02[6] = (u8)(custompid >> 8);
        bootload_sign_02[7] = (u8)(custompid & 0xFF);
        memcpy((void*)settings.bootloader,(void*)bootload_sign_02,8);
    }
    else                    //normal mode (use boot PID)
    {
        memcpy((void*)settings.bootloader,(void*)bootload_sign_01,8);
    }
    SaveSettings();
}

//------------------------------------------------------------------------------------------------------------------------------------------------
// Enable bootload without keypress
// Engineer: Quyen Leba
//------------------------------------------------------------------------------------------------------------------------------------------------
void Settings_ClearBootloader(void)
{
    memset((void*)settings.bootloader,0xFF,8);
    SaveSettings();
}


//------------------------------------------------------------------------------------------------------------------------------------------------
// EOF
//------------------------------------------------------------------------------------------------------------------------------------------------
