/**
  **************** (C) COPYRIGHT 2011 SCT Performance, LLC *********************
  * File Name          : cmdif_x3p.c
  * Device / System    : SCT products using the "CommonCode" platform
  * Author             : Quyen Leba
  *
  * Version            : 1 
  * Date               : 05/02/2011
  * Description        : 
  *                    : 
  *
  *
  * History            : 05/02/2011 M. Davis
  *                    :   Add SCT Copyright header,
  *
  ******************************************************************************
  */

#include <string.h>
#include <common/cmdif.h>
#include <common/statuscode.h>
#include <common/obd2datalog.h>
#include "X3_menus_helper.h"
#include "msg_common.h"
#include "menus.h"
#include "error.h"

typedef enum
{
    LastMarriedCountWarning,
    DatalogScanSetValidPidCount,
    DatalogStartStep1,
    DatalogStartNormal,
    DatalogStartPaused,
    DatalogStartCommLost,
    DatalogStartCommRecovered,

    InvalidState = 0xFF,
}CmdIf_X3P_ReceiveCmdInternalState;

//TODOQ: put this in a structure
CmdIf_X3P_ReceiveCmdInternalState cmdif_x3p_internalstate = InvalidState;
u8  *cmdif_x3p_data = NULL;
u16 cmdif_x3p_datalength;

extern u8 keybuffer;                                //from menus.c
extern bool screenupdaterequest;                    //from menus.c
extern Datalog_Info *dataloginfo;                   //from obd2datalog.c

//used to control cmdif_receive_command_internal
//static cmdif_response_info cmdif_x3p_response_info;

////------------------------------------------------------------------------------
//// Return:  u8  status
////------------------------------------------------------------------------------
//u8 cmdif_x3p_getResponse(u8 *responsedata, u16 *responselength,
//                         u16 *responsecode)
//{
//    //while(isResponseAvailable == FALSE);
//    memcpy(responsedata,
//           cmdif_x3p_response_info.data,cmdif_x3p_response_info.length);
//    *responselength = cmdif_x3p_response_info.length;
//    *responsecode = cmdif_x3p_response_info.responsecode;
//    return S_SUCCESS;
//}

//------------------------------------------------------------------------------
// Return:  u8  status
//------------------------------------------------------------------------------
//u8 cmdif_response(u16 command, u16 response, u8 *data, u16 length)
//{
//    return S_SUCCESS;
//}

//------------------------------------------------------------------------------
// Return:  u8  status
//------------------------------------------------------------------------------
//u8 cmdif_x3p_command(u16 command, u8 *data, u16 length)
//{
//    switch(command)
//    {
//    default:
//        break;
//    }
//    return S_SUCCESS;
//}

//------------------------------------------------------------------------------
// Handle response (ACK) from other functions
// Return:  u8  status
// Engineer: Quyen Leba
//------------------------------------------------------------------------------
//u8 cmdif_x3p_response_ack(u16 command, u16 response, u8 *data, u16 length)
//{
//    switch(command)
//    {
//    //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//    case CMDIF_CMD_INIT_UPLOAD:
//        switch(response)
//        {
//        case CMDIF_ACK_STOCK_NOT_FOUND:
//            break;
//        case CMDIF_ACK_STOCK_CORRUPTED:
//            break;
//        case CMDIF_ACK_STOCK_EXIST:
//            break;
//        case CMDIF_ACK_NOT_MARRIED:
//            break;
//        default:
//            break;
//        }
//        break;
//    //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//    case CMDIF_CMD_DO_UPLOAD:
//        switch(response)
//        {
//        case CMDIF_ACK_VPP_LOW:
//            break;
//        case CMDIF_ACK_VBATT_LOW:
//            break;
//        case CMDIF_ACK_UNLOCK_ECU_FAIL:
//            break;
//        case CMDIF_ACK_UNLOCK_ECU:
//            break;
//        case CMDIF_ACK_MARRIEDCOUTNMAXED:
//            break;
//        case CMDIF_ACK_MARRIEDCOUNTWARN:
//            break;
//        case CMDIF_ACK_FAILED:
//            break;
//        case CMDIF_ACK_UPLOAD_DONE:
//            break;
//        default:
//            break;
//        }
//        break;
//    //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//    case CMDIF_CMD_MARRIEDCOUNTWARN_CANCEL:
//        break;
//    case CMDIF_CMD_MARRIEDCOUNTWARN_COMFIRM:
//        break;
//    //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//    case CMDIF_CMD_PROGRESS_BAR:
//        //CMDIF_ACK_PROGRESSBAR
//        //TODOQ: Tsx_SPP_Send_ACK_NoWait(BT_CMD_PROGRESS_BAR, BT_ACK_PROGRESSBAR, &progress_percent_complete, 1);
//        break;
//    //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//    default:
//        break;
//    }
//    return S_SUCCESS;
//}

//------------------------------------------------------------------------------
// Handle response (ACK) from other functions
// Return:  u8  status (a negative ACK doesn't mean S_FAIL)
// Engineer: Quyen Leba
//------------------------------------------------------------------------------
u8 cmdif_x3p_internalresponse_ack(u16 command, u16 response,
                                  u8 *data, u16 length)
{
    u32 errornumbercode;
    MENUITEMSTR *mitem;
    u8 selection;
    u8 status;

    errornumbercode = 0;
    if (length == 4 && data != NULL)
    {
        //potentially, an error numbercode
        memcpy((char*)&errornumbercode,(char*)data,4);
    }

    cmdif_x3p_internalstate = InvalidState; //used to control cmdif_receive_command_internal
    switch(command)
    {
    //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    case CMDIF_CMD_DATALOG_SCAN:
        //TODOQ: this works for now, but should clean this up
        if (data == NULL && length == 0)
        {
            //this is the 1st internal ACK, do nothing
        }
        else if (data != NULL && length == 4)
        {
            //this is the 2nd internal ACK
            
            //data: [1:commtype][1:commlevel][2:validpidcount]
            if (dataloginfo)
            {
                dataloginfo->commtype = (VehicleCommType)data[0];
                dataloginfo->commlevel = (VehicleCommLevel)data[1];
                dataloginfo->validpidcount = ((u16)data[2]) | (((u16)data[3]) << 8);
                
                cmdif_x3p_internalstate = DatalogScanSetValidPidCount;
                cmdif_x3p_data = __malloc(2);
                if (cmdif_x3p_data)
                {
                    cmdif_x3p_datalength = 2;
                    cmdif_x3p_data[1] = CMDIF_CMD_DATALOG_CONFIRM;
                }
                else
                {
                    return S_MALLOC;
                }
            }
        }
        break;
    //##########################################################################
    case CMDIF_CMD_DATALOG_START:
        screenupdaterequest = TRUE;
        if (response == CMDIF_ACK_OK)
        {
            cmdif_x3p_internalstate = DatalogStartStep1;    //TODOQ: this
            cmdif_x3p_data = __malloc(2);
            if (cmdif_x3p_data)
            {
                cmdif_x3p_datalength = 2;
                cmdif_x3p_data[1] = CMDIF_CMD_DATALOG_CONFIRM;
            }
            else
            {
                return S_MALLOC;
            }
        }
        else if (response == CMDIF_ACK_DATA)
        {
            //data: [4:timestamp][2:signalcount] { [2:dlxindex, 4:value] ... }
            MenuDatalogShowValue(dataloginfo);
            cmdif_x3p_internalstate = DatalogStartNormal;
        }
        else if (response == CMDIF_ACK_COMMLOST)
        {
            cmdif_x3p_internalstate = DatalogStartCommLost;
            ShowMessage(NOKEY,NOAUDIO,(void*)*Language->HDR_Monitor,
                        (void*)*Language->FDR_SelectResume,
                        (void*)*Language->TXT_NULLSTR,
                        (void*)*Language->TXT_CommunicationLost,
                        (void*)*Language->TXT_NULLSTR,CLEAR_TOP);
        }
        else if (response == CMDIF_ACK_COMMACTIVE)
        {
            screenupdaterequest = TRUE;
            cmdif_x3p_internalstate = DatalogStartCommRecovered;
            ShowMessage(NOKEY,NOAUDIO,(void*)*Language->HDR_Monitor,
                        (void*)*Language->TXT_NULLSTR,
                        (void*)*Language->TXT_NULLSTR,
                        "comm is ok",
                        (void*)*Language->TXT_NULLSTR,CLEAR_TOP);
        }
        break;
    case CMDIF_CMD_DATALOG_PAUSE:
        if (response == CMDIF_ACK_RESUME)
        {
            screenupdaterequest = TRUE;
            cmdif_x3p_internalstate = DatalogStartNormal;
            ShowMessage(NOKEY,NOAUDIO,(void*)*Language->HDR_Monitor,
                        (void*)*Language->TXT_NULLSTR,
                        (void*)*Language->TXT_NULLSTR,
                        "resumed",
                        (void*)*Language->TXT_NULLSTR,CLEAR_TOP);
        }
        else if (response == CMDIF_ACK_PAUSE)
        {
            cmdif_x3p_internalstate = DatalogStartPaused;
            ShowMessage(NOKEY,NOAUDIO,(void*)*Language->HDR_Monitor,
                        (void*)*Language->TXT_NULLSTR,
                        (void*)*Language->TXT_NULLSTR,
                        "paused",
                        (void*)*Language->TXT_NULLSTR,CLEAR_TOP);
        }
        break;
    //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    // these cases happen during a upload (specifically, CMDIF_CMD_DO_UPLOAD)
    case CMDIF_CMD_MARRIEDCOUNTWARN_COMFIRM:
        break;
    //##########################################################################
    case CMDIF_CMD_MARRIEDCOUNTWARN_CANCEL:
        break;
    //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    case CMDIF_CMD_DO_UPLOAD:
        if (response == CMDIF_ACK_UPLOAD_DONE)
        {
            //TODOQ: show some message
        }
        else if (response == CMDIF_ACK_MARRIEDCOUTNMAXED)
        {
            ShowMessage(SW_ENTER,NOAUDIO,(void*)*Language->HDR_Warning,
                        (void*)*Language->FDR_PressESCExit,
                        (void*)"Married count is Exceeded", //TODOQ:
                        (void*)*Language->TXT_NULLSTR,
                        (void*)"Contact SCT Tech Support",
                        CLEAR_TOP);
        }
        else if (response == CMDIF_ACK_MARRIEDCOUNTWARN)
        {
            ShowMessage(NOKEY,NOAUDIO,(void*)*Language->HDR_Warning,
                        (void*)*Language->FDR_ContinueKeyMsg,
                        (void*)"Only 1 Unlock Left",
                        (void*)"Confirm to use it?",
                        (void*)*Language->TXT_NULLSTR,CLEAR_TOP);
            mitem = __malloc(2*sizeof(MENUITEMSTR));
            ParseMenuItemStr((void*)*Language->TXT_Yes, 0, mitem, 0);
            ParseMenuItemStr((void*)*Language->TXT_No, 1, mitem, 0);
            status = Scroller(&selection,0,mitem,2, 2, 0, 8, 1, 40,NullHelper,NullHelper);
            __free(mitem);

            ShowMessage(NOKEY,NOAUDIO,(void*)*Language->HDR_Warning,
                        (void*)*Language->TXT_NULLSTR,
                        (void*)*Language->TXT_NULLSTR,
                        (void*)*Language->TXT_PleaseWait,
                        (void*)*Language->TXT_NULLSTR,CLEAR_TOP);
            
            cmdif_x3p_internalstate = LastMarriedCountWarning;
            cmdif_x3p_data = __malloc(2);
            if (cmdif_x3p_data)
            {
                cmdif_x3p_datalength = 2;
                if ((selection != 0) || (status == S_USERABORT))
                {
                    // user doesn't want to use the last unlock
                    cmdif_x3p_data[1] = CMDIF_CMD_MARRIEDCOUNTWARN_CANCEL;
                }
                else
                {
                    cmdif_x3p_data[1] = CMDIF_CMD_MARRIEDCOUNTWARN_COMFIRM;
                }
            }
            else
            {
                return S_MALLOC;
            }
        }
        else if (response == CMDIF_ACK_VBATT_LOW)
        {
            ERROR_MessageCode(LOWVBATT,10250);
            return S_FAIL;
        }
        else if (response == CMDIF_ACK_VPP_LOW)
        {
            ERROR_MessageCode(LOWVPP,10251);
            return S_FAIL;
        }
        else if (response == CMDIF_ACK_FAILED)
        {
            //TODOQ: assign some error here
            ERROR_MessageCode(DEBUG,10252);
            return S_FAIL;
        }
        else
        {
            //TODOQ: assign some error here
            ERROR_MessageCode(DEBUG,10253);
            return S_FAIL;
        }
        break;
    //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    case CMDIF_CMD_PCM_UNLOCK:
        if (response == CMDIF_ACK_KEY_ON)
        {
            ShowMessage(NOKEY,NOAUDIO,(void*)*Language->HDR_UnlockECU,
                        (void*)*Language->TXT_NULLSTR,
                        (void*)*Language->TXT_NULLSTR,
                        (void*)*Language->TXT_TurnKeyOn,
                        (void*)*Language->TXT_NULLSTR,CLEAR_TOP);
        }
        else if (response == CMDIF_ACK_KEY_OFF)
        {
            ShowMessage(NOKEY,NOAUDIO,(void*)*Language->HDR_UnlockECU,
                        (void*)*Language->TXT_NULLSTR,
                        (void*)*Language->TXT_NULLSTR,
                        (void*)*Language->TXT_TurnKeyOff,
                        (void*)*Language->TXT_NULLSTR,CLEAR_TOP);
        }
        else if (response == CMDIF_ACK_WAIT)
        {
            ShowMessage(NOKEY,NOAUDIO,(void*)*Language->HDR_UnlockECU,
                        (void*)*Language->TXT_NULLSTR,
                        (void*)*Language->TXT_NULLSTR,
                        (void*)*Language->TXT_PleaseWait,
                        (void*)*Language->TXT_NULLSTR,CLEAR_TOP);
        }
        else if (response == CMDIF_ACK_UNLOCK_ECU)
        {
            ShowMessage(NOKEY,NOAUDIO,(void*)*Language->HDR_UnlockECU,
                        (void*)*Language->TXT_NULLSTR,
                        (void*)*Language->TXT_NULLSTR,
                        (void*)*Language->TXT_PleaseWait,
                        (void*)*Language->TXT_NULLSTR,CLEAR_TOP);
        }
        else if (response == CMDIF_ACK_UNLOCK_ECU_FAIL)
        {
            //TODOQ: assign some error here
            ERROR_MessageCode(DEBUG,10700);
            return S_FAIL;
        }
        else
        {
        }
        break;
    //##########################################################################
    case CMDIF_CMD_PROGRESS_BAR:
        //Note: it is handled by cmdif_internalresponse_ack_nowait
        //don't use it here
        break;
    //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    case CMDIF_CMD_INIT_DOWNLOAD:
        if (response == CMDIF_ACK_OK)
        {
        }
        else if (response == CMDIF_ACK_BUILDING_TUNE)
        {
            ShowMessage(NOKEY,NOAUDIO,(void*)*Language->HDR_CopyFile,
                        (void*)*Language->TXT_NULLSTR,
                        (void*)*Language->TXT_NULLSTR,
                        (void*)*Language->TXT_PleaseWait,
                        (void*)*Language->TXT_NULLSTR,CLEAR_TOP);
        }
        else if (response == CMDIF_ACK_APPLY_STRATEGY)
        {
            ShowMessage(NOKEY,NOAUDIO,(void*)*Language->HDR_SetupStorage,
                        (void*)*Language->TXT_PleaseWait,
                        (void*)*Language->TXT_NULLSTR,
                        (void*)*Language->TXT_NULLSTR,  //TODOQ:
                        (void*)*Language->TXT_NULLSTR,CLEAR_TOP);
        }
        else if (response == CMDIF_ACK_APPLY_OPTIONS)
        {
            ShowMessage(NOKEY,NOAUDIO,(void*)*Language->HDR_SetupStorage,
                        (void*)*Language->TXT_PleaseWait,
                        (void*)*Language->TXT_NULLSTR,
                        (void*)*Language->TXT_ReadingOptions,   //TODOQ:
                        (void*)*Language->TXT_NULLSTR,CLEAR_TOP);
        }
        else if (response == CMDIF_ACK_MARRYING_VEHICLE)
        {
            //TODOQ: more on this, should I remove this case in DO_DOWNLOAD (TSXD use it here)
            ShowMessage(NOKEY,NOAUDIO,(void*)*Language->HDR_SetupStorage,
                        (void*)*Language->TXT_PleaseWait,
                        (void*)*Language->TXT_NULLSTR,
                        (void*)"Married Stuff 1",   //TODOQ:
                        (void*)*Language->TXT_NULLSTR,CLEAR_TOP);
        }
        //NOTE: there's still more
        break;
    //##########################################################################
    case CMDIF_CMD_DO_DOWNLOAD:
        if (response == CMDIF_ACK_DOWNLOAD_DONE)
        {
            //do nothing
        }
        else if (response == CMDIF_ACK_VBATT_LOW)
        {
            ERROR_MessageCode(LOWVBATT,10450);
            return S_FAIL;
        }
        else if (response == CMDIF_ACK_VPP_LOW)
        {
            ERROR_MessageCode(LOWVPP,10451);
            return S_FAIL;
        }
        else if (response == CMDIF_ACK_FAILED)
        {
            //TODOQ: assign some error here
            ERROR_MessageCode(DEBUG,10452);
            return S_FAIL;
        }
        else if (response == CMDIF_ACK_MARRYING_VEHICLE)
        {
            ShowMessage(NOKEY,NOAUDIO,(void*)*Language->HDR_SetupStorage,
                        (void*)*Language->TXT_PleaseWait,
                        (void*)*Language->TXT_NULLSTR,
                        (void*)"Married Stuff 2",   //TODOQ:
                        (void*)*Language->TXT_NULLSTR,CLEAR_TOP);
        }
        else if (response == CMDIF_ACK_CHECK_MARRIED)
        {
            ShowMessage(NOKEY,NOAUDIO,(void*)*Language->HDR_SetupStorage,   //TODOQ:
                        (void*)*Language->TXT_PleaseWait,
                        (void*)*Language->TXT_NULLSTR,
                        (void*)"Check Married 1",   //TODOQ:
                        (void*)*Language->TXT_NULLSTR,CLEAR_TOP);
        }
        else if (response == CMDIF_ACK_ERASE_ECU)
        {
            ShowMessage(NOKEY,NOAUDIO,(void*)*Language->HDR_SetupECU,
                        (void*)*Language->FDR_DoNotUnplug,
                        (void*)*Language->TXT_NULLSTR,
                        (void*)*Language->TXT_PleaseWait,
                        (void*)*Language->TXT_NULLSTR,CLEAR_TOP);
        }
        else if (response == CMDIF_ACK_RESET_ECU)
        {
            ShowMessage(NOKEY,NOAUDIO,(void*)*Language->HDR_ResetECU,
                        (void*)*Language->FDR_DoNotUnplug,
                        (void*)*Language->TXT_NULLSTR,
                        (void*)*Language->TXT_PleaseWait,
                        (void*)*Language->TXT_NULLSTR,CLEAR_TOP);
        }
        else if (response == CMDIF_ACK_CLEAR_DTC_ECU)
        {
            ShowMessage(NOKEY,NOAUDIO,(void*)*Language->HDR_ClearDiagTroubleCode,
                        (void*)*Language->FDR_DoNotUnplug,
                        (void*)*Language->TXT_NULLSTR,
                        (void*)*Language->TXT_PleaseWait,
                        (void*)*Language->TXT_NULLSTR,CLEAR_TOP);
        }
        else if (response == CMDIF_ACK_MARRIED_FAIL)
        {
            //TODOQ: assign some error here
            ERROR_MessageCode(DEBUG,errornumbercode);
            return S_FAIL;
        }
        else if (response == CMDIF_ACK_ERASE_FAIL)
        {
            //TODOQ: assign some error here
            ERROR_MessageCode(DEBUG,10455);
            return S_FAIL;
        }
        else if (response == CMDIF_ACK_MARRIED_FAIL)
        {
            //TODOQ: assign some error here
            ERROR_MessageCode(DEBUG,10456);
            return S_FAIL;
        }
        else
        {
            //TODOQ: assign some error here
            ERROR_MessageCode(DEBUG,10453);
            return S_FAIL;
        }
        break;
    //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    default:
        ERROR_MessageCode(DEBUG,10454);
        return S_FAIL;
    }
    return S_SUCCESS;
}

//------------------------------------------------------------------------------
// Handle response (ACK) with nowait from other functions
// Return:  u8  status
// Engineer: Quyen Leba
// TODOQ: get rid of this
//------------------------------------------------------------------------------
u8 cmdif_x3p_internalresponse_ack_nowait(u16 command, u16 response,
                                         u8 *data, u16 length)
{
    u8  *strptr;
    u32 percentage;

    switch(command)
    {
    //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    case CMDIF_CMD_DO_UPLOAD:
    case CMDIF_CMD_DO_DOWNLOAD:
    case CMDIF_CMD_DATALOG_SCAN:
        if (response == CMDIF_ACK_PROGRESSBAR)
        {
            if (command == CMDIF_CMD_DATALOG_SCAN)
            {
                strptr = (u8*)*Language->HDR_SetupMonitor;
            }
            else if (command == CMDIF_CMD_DO_DOWNLOAD)
            {
                strptr = (u8*)*Language->HDR_DownloadTune;
            }
            else if (command == CMDIF_CMD_DO_UPLOAD)
            {
                strptr = (u8*)*Language->HDR_UploadECU;
            }
            else
            {
                strptr = (u8*)*Language->TXT_NULLSTR;
            }
            ShowMessage(NOKEY, NOAUDIO, strptr,
                        (void*)*Language->FDR_DoNotUnplug,
                        (void*)*Language->TXT_PleaseWait,
                        (void*)*Language->TXT_NULLSTR,
                        (void*)*Language->TXT_NULLSTR, CLEAR_TOP);
            //data: [1:percentage]
            percentage = data[0];
            if (percentage > 100)
            {
                percentage = 100;
            }
            Progress_Bar(100,percentage,1,TRUE);
        }
        break;
    case CMDIF_CMD_PROGRESS_BAR:
        if (response == CMDIF_ACK_PROGRESSBAR)
        {
            u8  *header_bptr;
            //TODOQ: change Kii so we don't have so many cases here
            header_bptr = (void*)*Language->TXT_NULLSTR;
            if (data[1] != NULL)
            {
                header_bptr = &data[1];
            }
            ShowMessage(NOKEY, NOAUDIO, header_bptr,
                        (void*)*Language->FDR_DoNotUnplug,
                        (void*)*Language->TXT_PleaseWait,
                        (void*)*Language->TXT_NULLSTR,
                        (void*)*Language->TXT_NULLSTR, CLEAR_TOP);
            //data: [1:percentage]
            percentage = data[0];
            if (percentage > 100)
            {
                percentage = 100;
            }
            Progress_Bar(100,percentage,1,TRUE);
        }
        break;
    //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    default:
        break;
    }
    return S_SUCCESS;
}

//------------------------------------------------------------------------------
// Wait for a command during CMDIF; only used within CMDIF handler
// Outputs: u8  *data
//          u16 *datalength
// Return:  u8  status
// Engineer: Quyen Leba
//------------------------------------------------------------------------------
u8 cmdif_x3p_receive_command_internal(u8 *data, u16 *length)
{
    u8  key;
    u8  status;

    status = S_SUCCESS;

    switch(cmdif_x3p_internalstate)
    {
    case LastMarriedCountWarning:
    case DatalogScanSetValidPidCount:
    case DatalogStartStep1: //TODOQ: this
        if (cmdif_x3p_data)
        {
            memcpy(data,cmdif_x3p_data,cmdif_x3p_datalength);
            *length = cmdif_x3p_datalength;
            __free(cmdif_x3p_data);
            cmdif_x3p_data = NULL;
            cmdif_x3p_internalstate = DatalogStartNormal;
        }
        else
        {
            status = S_FAIL;
        }
        break;
    case DatalogStartNormal:    //comm is active, run normally
        key = getKey();
        if (key == SW_ENTER)
        {
            data[1] = CMDIF_CMD_DATALOG_PAUSE;
            data[4] = CMDIF_ACT_DATALOG_PAUSE;
            keybuffer = NOKEY;
        }
        else if (key == SW_EXIT)
        {
            data[1] = CMDIF_CMD_DATALOG_STOP;
            keybuffer = NOKEY;
        }
        else if (key == SW_UP || key == SW_DOWN)
        {
            keybuffer = key;
            status = S_TIMEOUT;
        }
        else
        {
            status = S_TIMEOUT;
        }
        break;
    case DatalogStartPaused:    //comm is active, operation paused
        key = getKey();
        if (key == SW_ENTER)
        {
            data[1] = CMDIF_CMD_DATALOG_PAUSE;
            data[4] = CMDIF_ACT_DATALOG_RESUME;
        }
        else if (key == SW_EXIT)
        {
            data[1] = CMDIF_CMD_DATALOG_STOP;
        }
        else 
        {
            status = S_TIMEOUT;
        }
        break;
    case DatalogStartCommLost:  //comm is lost
        key = getKey();
        if (key == SW_ENTER)
        {
            data[1] = CMDIF_CMD_DATALOG_PAUSE;
            data[4] = CMDIF_ACT_DATALOG_RESUME;
        }
        else if (key == SW_EXIT)
        {
            data[1] = CMDIF_CMD_DATALOG_STOP;
        }
        else 
        {
            status = S_TIMEOUT;
        }
        break;
    case DatalogStartCommRecovered:
        data[1] = CMDIF_CMD_DATALOG_PAUSE;
        data[4] = CMDIF_ACT_DATALOG_RESUME;
        break;
    default:
        break;
    }
    return status;
}
