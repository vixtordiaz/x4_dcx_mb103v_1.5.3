/**
  **************** (C) COPYRIGHT 2011 SCT Performance, LLC *********************
  * File Name          : obd2_x3p_function.h
  * Device / System    : SCT products using the "CommonCode" platform
  * Author             : Quyen Leba
  *
  * Version            : 1 
  * Date               : 05/02/2011
  * Description        : 
  *                    : 
  *
  *
  * History            : 05/02/2011 M. Davis
  *                    :   Add SCT Copyright header,
  *
  ******************************************************************************
  */

#ifndef __OBD2_X3P_FUNCTION_H
#define __OBD2_X3P_FUNCTION_H

#include <arch/gentype.h>
#include <common/obd2tune.h>

void obd2_x3p_function_readdtc();
void obd2_x3p_function_cleardtc();
void obd2_x3p_function_readvehicleinfo();
void obd2_x3p_function_programtunetovehicle(TuneType tunetype);
void obd2_x3p_function_returnvehicletostock();
u8 obd2_x3p_function_datalog_setup(bool isrequeststartdatalog);
void obd2_x3p_function_datalog_start(bool isrequestsetupdatalog);

#endif	//__OBD2_X3P_FUNCTION_H
