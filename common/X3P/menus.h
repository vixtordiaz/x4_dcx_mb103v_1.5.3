/**
  **************** (C) COPYRIGHT 2011 SCT Performance, LLC *********************
  * File Name          : menus.h
  * Device / System    : SCT products using the "CommonCode" platform
  * Author             : Quyen Leba
  *
  * Version            : 1 
  * Date               : 05/02/2011
  * Description        : 
  *                    : 
  *
  *
  * History            : 05/02/2011 M. Davis
  *                    :   Add SCT Copyright header,
  *
  ******************************************************************************
  */

/******************** (C) COPYRIGHT 2008, 2009 SCT LLC *************************
* File Name          : menus.h
* Device / System    : X3-Plus Tuner 
* Author             : J. Hess, R. Rusak, P. Downs, Q.Leba, D. Lin, M. Davis
* Version            : 
* Date               : 9/2/08
* Description        : Public vars, functions, defs, structs, etc. for menus.c
*
* Rev History, most recent first
* $History: /CommonCode/Mico/common/X3P/menus.h $
*  
*  ****************** Version 1 ****************** 
*  User: qleba   Date: 2011-05-17   Time: 23:01:27-04:00 
*  Updated in: /CommonCode/Mico/common/X3P 
*  
*  ****************** Version 1 ****************** 
*  User: qleba   Date: 2010-11-01   Time: 12:21:03-04:00 
*  Updated in: /mico/common/X3P 
*  
*  ****************** Version 5 ****************** 
*  User: rrusak   Date: 2009-04-06   Time: 13:15:54-04:00 
*  Updated in: /X3-PLUS/GMX3P/app 
*  Added Mark's USB 1.1 changes and removed the block for 2009 vehicles. 
*  Rhonda 
*  
*  ****************** Version 8 ****************** 
*  User: mdavis   Date: 2009-03-27   Time: 15:24:45-04:00 
*  Updated in: /X3-PLUS/X3P/app 
*  Support device-settings option for sound enable/disable while device is 
*  bus-powered. Default (in settings) is sound disabled. 
*  
*  ****************** Version 7 ****************** 
*  User: mdavis   Date: 2009-02-20   Time: 18:57:22-05:00 
*  Updated in: /X3-PLUS/X3P/app 
*  Applying standardized documentation headers to all source files, using the 
*  SourceGear Vault "Keyword Expansion" facility. 
*  
*******************************************************************************/

#ifndef __MENUS_H__
#define __MENUS_H__

#include <arch/gentype.h>
#include <board/X3P/system.h>
#include <common/obd2datalog.h>

#define CHECKMARK           251//'+'
#define STAR                0x2A

typedef u8 (funcptr)(u32 v, u8 k);
typedef struct
{
    u8 checked[2];
    u8 content[19];
}MENUITEMSTR;

typedef enum {
	MAINMENU                = 0,
	PERFORMANCEMENU         = 10,   //
	DEVICESETTINGSMENU      = 20,   // 0x14
	PROGRAMVEHICLEMENU      = 30,   // 0x1e
	DTCMENU                 = 40,   // 0x28
	ALERTSETUPMENU          = 50,   // 0x32
	EXTERNALINPUTSMENU      = 60,   // 0x3c
	SHIFTLIGHTMENU          = 70,   // 0x46
	NULLMENU                = 0xF0
} MENUCODE;

#ifdef BANKS_ENG
typedef enum {
	BANKSMAIN				= 0,
    BANKSPROGRAMVEHICLE		= 1,
    BANKSDTC				= 2,
    BANKSPERFORMANCE		= 3,
    BANKSDEVICESETTINGS		= 4,
    BANKSDEVICEINFO         = 5,
    BANKSDEALERINFO         = 6,
	BANKSNULL				= 0xF0
} BANKSMENU;
#endif

typedef enum {
    PROGRAMVEHICLE          = 0,    //100
    DTC                     = 1,    //200
    DEALERINFO              = 3,    //400
    DEVICESETTINGS          = 4,    //600
    DEVICEINFO              = 5,
    VEHICLEPARAMETER        = 6,    //300
    PERFORMANCE             = 2,    //500
    
    DEVICE_BACKLIGHT        = 20,   // 0x14
    DEVICE_CONTRAST         = 21,   // 0x15
//    DEVICE_BUZZER           = 22,   // 0x16
    DEVICE_SOUND_USB_PWR    = 22,   // 0x16
    DEVICE_LANGUAGE         = 23,   // 0x17
//    DEVICE_SOUND_USB_PWR    = 24,   // 0x18

    PROGRAM_STRATEGY        = 31,   // 0x1f
    PROGRAM_CUSTOM          = 30,   // 0x1e
    PROGRAM_STOCK           = 32,   // 0x20
    UPLOAD_STOCK            = 33,   // 0x21


    VEHICLE_INFO            = 40,   // 0x28
    DTC_READ                = 41,   // 0x29
    DTC_CLEAR               = 42,   // 0x2a

	PERFORMANCESETUP		= 10,              
	PERFORMANCEMONITOR		= 11,
    EXTERNALINPUTS			= 12,
	ALERTSETUP				= 13,
	QUARTERMILE				= 14,
	MPH						= 15,
	HPTQ					= 16,
	SHIFTLIGHT				= 17,
	NULLMENUITEM			= 0xF0
} MENUITEM;

#define         TotalIcon           TotalIconInBuffer   // 0: buffer empty, else # of icons

enum    //this used with ShowMessage(...) to clear screen, before and/or after displaying message
{
    CLEAR_TOP       = 0x01,
    CLEAR_BOTTOM    = 0x02,
    CLEAR_ALL       = CLEAR_TOP | CLEAR_BOTTOM,
    CLEAR_NONE      = 0,
};

u8      NullHelper(u32 v, u8 k);
void    MENUS_SplashScreen(void);
u8      ShowMessage(u8 key, AudioCode tone, u8* header, u8* footer, u8* line1, u8* line2, u8* line3, u8 clearcmd);
void    ParseMenuItemStr(u8* item, u8 index, MENUITEMSTR* menu, u8 ischecked);
void    RefreshListbox(MENUITEMSTR* content, u8 totalitem, u8 cursor, u8 prev_cursor, u8 requireclr, u8 top, u8 bottom, u8 lineheight, u8 X, u8 Y);
u8      Scroller(u8* selectionresult, u8 maxselection, MENUITEMSTR* content, u8 totalitem, u8 maxline, u8 ischeckboxenable, u8 lineheight, u8 startX, u8 startY, funcptr f_select, funcptr f_unselect);
u8      NumberGenerator2(u8 isstepadjustable, float* value, u8 isfloat, u8 issigned, u8 col, u8 row);
void    Progress_Bar(u32 filesize, u32 completed, u32 buffersize, bool bar_enable);
#ifdef BANKS_ENG
void      HandleMainMenu(void);
#else
u8      HandleMainMenu(void);
#endif
u8      HandleExternalInputsMenu(void);

//TODO:
#define RunPowerProgram(t)
//#define ReturnVehicleStock()        
//#define ReadDTC()
//#define ClearDTC()
//#define ReadStrategy()
#define FreeMenuInBuffer()          //do nothing
#define LoadMenuIconFromRawMMC(i)   //do nothing

void MenuDatalogShowValue(Datalog_Info *dataloginfo);

#endif
