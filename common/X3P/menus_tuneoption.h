/**
  **************** (C) COPYRIGHT 2011 SCT Performance, LLC *********************
  * File Name          : menus_tuneoption.h
  * Device / System    : SCT products using the "CommonCode" platform
  * Author             : Quyen Leba
  *
  * Version            : 1 
  * Date               : 05/02/2011
  * Description        : 
  *                    : 
  *
  *
  * History            : 05/02/2011 M. Davis
  *                    :   Add SCT Copyright header,
  *
  ******************************************************************************
  */

#ifndef __MENUS_TUNEOPTION_H
#define __MENUS_TUNEOPTION_H

#include <arch/gentype.h>

u8 RunOptionsMenu(u8 *optionfilename, u8 ecm_index, bool *is_option_use);

#endif	//__MENUS_TUNEOPTION_H
