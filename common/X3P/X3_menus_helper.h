/**
  **************** (C) COPYRIGHT 2011 SCT Performance, LLC *********************
  * File Name          : X3_menus_helper.h
  * Device / System    : SCT products using the "CommonCode" platform
  * Author             : Quyen Leba
  *
  * Version            : 1 
  * Date               : 05/02/2011
  * Description        : 
  *                    : 
  *
  *
  * History            : 05/02/2011 M. Davis
  *                    :   Add SCT Copyright header,
  *
  ******************************************************************************
  */

/******************** (C) COPYRIGHT 2008, 2009 SCT LLC *************************
* File Name          : X3_menus_helper.h
* Device / System    : X3-Plus Tuner 
* Author             : J. Hess, R. Rusak, P. Downs, Q.Leba, D. Lin, M. Davis
* Version            : 
* Date               : 9/2/08
* Description        : Public vars, functions, defs, structs, etc. for X3_menus_helper.c
*
* Rev History, most recent first
* $History: /CommonCode/Mico/common/X3P/X3_menus_helper.h $
*  
*  ****************** Version 1 ****************** 
*  User: qleba   Date: 2011-05-17   Time: 23:01:27-04:00 
*  Updated in: /CommonCode/Mico/common/X3P 
*  
*  ****************** Version 1 ****************** 
*  User: qleba   Date: 2010-11-01   Time: 12:21:04-04:00 
*  Updated in: /mico/common/X3P 
*  
*  ****************** Version 4 ****************** 
*  User: rrusak   Date: 2009-04-06   Time: 13:15:54-04:00 
*  Updated in: /X3-PLUS/GMX3P/app 
*  Added Mark's USB 1.1 changes and removed the block for 2009 vehicles. 
*  Rhonda 
*  
*  ****************** Version 5 ****************** 
*  User: mdavis   Date: 2009-03-27   Time: 15:35:13-04:00 
*  Updated in: /X3-PLUS/X3P/app 
*  Support device-settings option for sound enable/disable while device is 
*  bus-powered. Default (in settings) is sound disabled. 
*  
*  ****************** Version 4 ****************** 
*  User: mdavis   Date: 2009-02-20   Time: 19:38:07-05:00 
*  Updated in: /X3-PLUS/X3P/app 
*  Applying standardized documentation headers to all source files, using the 
*  SourceGear Vault "Keyword Expansion" facility. 
*  
*******************************************************************************/

#ifndef __X3_MENUS_HELPER_H__
#define __X3_MENUS_HELPER_H__

#include <board/X3P/system.h>
#include "menus.h"

#define     SW_UP               KEYUP
#define     SW_DOWN             KEYDOWN
#define     SW_BACK             KEYLEFT
#define     SW_NEXT             KEYRIGHT
#define     SW_ENTER            KEYENTER
#define     SW_EXIT             KEYCANCEL
#define     NOKEY               KEYNONE
#define     NOKEYREQUIRE        KEYNOTREQUIRED

MENUITEM    ShowIconMenu(MENUCODE menu);
MENUITEM    ShowTextMenu(MENUCODE menu);
void        MENUS_DealerInfo(void);
void        MENUS_DeviceInfo(void);

u8          AdjustBrightnessHelper(void);
u8          AdjustContrastHelper(void);
u8          AdjustSound_USBpwr_Helper(void);
u8          AdjustLanguageHelper(void);
u8          AdjustBuzzerHelper(void);
u8          AdjustLanguageHelper(void);
u8          Mis_match_OS(void);
u8          GM_CompareOS(u8 index, u8* strategy, u8 trans_match);
#endif

//------------------------------------------------------------------------------------------------------------------------------------------------
// EOF
//------------------------------------------------------------------------------------------------------------------------------------------------
