/**
  **************** (C) COPYRIGHT 2011 SCT Performance, LLC *********************
  * File Name          : error.h
  * Device / System    : SCT products using the "CommonCode" platform
  * Author             : Quyen Leba
  *
  * Version            : 1 
  * Date               : 05/02/2011
  * Description        : 
  *                    : 
  *
  *
  * History            : 05/02/2011 M. Davis
  *                    :   Add SCT Copyright header,
  *
  ******************************************************************************
  */

#ifndef __ERROR_H
#define __ERROR_H

#include <arch/gentype.h>
#include <common/X3P/msg_common.h>

extern const u8 MSG_CONTACT_98[];
extern const u8 MSG_CONTACT_1[];
extern const u8 MSG_CONTACT2[];

typedef enum
{
    DEBUG,
    READFILE,
    OPENFILE,
    CLOSEFILE,
    SEARCHFILE,
    SEEKFILE,
    WRITEFILE,
    FILETRANSFER,
    NOTUNE,
    MALLOC,
    DECRYPT,
    NOSTRATEGY,
    NOSTOCK,
    TUNEFAIL,
    UPLOADSTOCKFAIL,
    READSTRATEGYFAIL,
    UNLOCKFAIL,
    DTCCLEARFAIL,
    NOCUSTOMTUNE_A,      
    NOCUSTOMTUNE_B,
    PID,
    NOPID,
    INVALIDBINARY,
    PRELOADTUNEBLOCKED,
    MARRIEDFAIL,
    USERABORT,
    LOWVBATT,
    LOWVPP,
    DOWNLOADFAILDETECTED,
    NOSECURITYBYTE,
    INVALIDTUNE,
    STOCKFILECORRUPT,
    DEVICELOCKED,
    FLASHUPDATE,
    NOSTRATEGYTUNE,
    INVALIDBINARYMARRIED,
    NOSTRATEGYCHANGES,
    NOCHANGES,
    SAVESETTINGS,
    FILESYSTEMCORRUPT,
    INVALIDCOMM,
    INITDOWNLOAD,
    STRATEGYTUNENOTSUPPORTED,
    CUSTOMTUNENOTSUPPORTED,
    CHECKSUMERROR,
    OSPARTNUMBERFAIL,
    FLASHTYPENOTSET,
    DEVICE_MARRIED,
    OLDSTRATEGYFILES,
} ERRORCODE;

void ERROR_MessageCode(ERRORCODE errorcode, u32 numbercode);
void    ERROR_Message(ERRORCODE errorcode);
void    ERROR_DownloadFail(void);
void    ERROR_UnsupportVehicle(u8* tune, u8 upload_stock);
void    ERROR_SecurityByte(void);
#endif

//------------------------------------------------------------------------------------------------------------------------------------------------
// EOF
//------------------------------------------------------------------------------------------------------------------------------------------------
