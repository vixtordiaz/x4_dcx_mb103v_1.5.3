/**
  **************** (C) COPYRIGHT 2011 SCT Performance, LLC *********************
  * File Name          : menus.c
  * Device / System    : SCT products using the "CommonCode" platform
  * Author             : Quyen Leba
  *
  * Version            : 1 
  * Date               : 05/02/2011
  * Description        : 
  *                    : 
  *
  *
  * History            : 05/02/2011 M. Davis
  *                    :   Add SCT Copyright header,
  *
  ******************************************************************************
  */

#include <stdio.h>
#include <board/delays.h>
#include <common/statuscode.h>
#include <common/X3P/error.h>
#include <common/X3P/global.h>
#include <common/X3P/X3_menus_helper.h>
#include <common/settings.h>
#include "menus_helper.h"

#include "obd2_x3p_function.h"

//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
#ifdef BANKS_ENG
int     DisplayMenu(char buffer[][23], int choiceCount);
void    DisplayTitle(char *buffer);
u8      TuneType(void);
void    LCD_1_Area(void);
void    LCD_2_Areas(void);
bool    Is_custom_there(void);
int     menuIndex = 0;
int     subMenuIndex = 0;
int     prevSubMenuIndex = -1;
int     prevMainMenuIndex = 0;
#else
u8      HandleDTCMenu(u8 sel);
u8      HandlePerformanceMenu(u8 sel);
u8      HandleDeviceSettingsMenu(u8 sel);
u8      HandleProgramVehicleMenu(u8 sel);
u8      HandleDealerInfoMenu(u8 key);
u8      HandleAlertSetupMenu(void);
u8      HandleShiftLightMenu(void);
u8      HandleVehicleParameterMenu(void);
#endif

u8      ReadDTCHelper(u32 v, u8 k);
u8      ClearDTCHelper(u32 v, u8 k);
u8      ReadStrategyHelper(u32 v, u8 k);
u8      SetCustomEquation(u8 type, CustomEq *c);
u8      NumberGenerator(u8 isstepadjustable, float* value, u8 isfloat,
                        u8 issigned, u8 col, u8 row);
u8      SetCustomEquation(u8 type, CustomEq *c);
void    SetupAnalogChannel(u8 ch);

//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
MENUCODE    CurrentMenu         = NULLMENU;
MENUCODE    PreviousMenu        = NULLMENU;
u8          CurrentIcon         = 0;    // zero based; also serve as menu cursor
u8          TotalIconInBuffer   = 0;    // 0: buffer empty, else # of icons

extern u8   nav_direction;
extern u8   nav_changed;
#define     ICON_PER_SET        1
u8          keybuffer = NOKEY;
bool        screenupdaterequest = FALSE;

void        MenuRotateLeft(void);
void        MenuRotateRight(void);

//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
void MENUS_SplashScreen(void)
{
    F_FILE* file;

    LCD_gselvp(0);
    LCD_gclrvp();

    file = __fopen("/customlogo.bin", "rb");     // Check for a custom logo
    if (file != NULL)
    {
        __fclose(file);
        LCD_gputimage("/customlogo.bin",0,0);
        Buzzer(STARTUP);
        delays(1000, 'm');
        if(LCD_gputimage("/poweredbySCT.bin",0,0) == S_SUCCESS)
        {
            delays(1000, 'm');
        }
    }
    else                                        // No custom logo do default SCT
    {
        LCD_gputimage("/SCT.bin",0,0);
        Buzzer(STARTUP);
        delays(1000, 'm');
    }
}

//------------------------------------------------------------------------------
// Main system menu
// Engineer: Quyen Leba
//------------------------------------------------------------------------------

#ifdef BANKS_ENG
void HandleMainMenu(void)
{
    u8 refreshmenu = TRUE;
    u8 tunetype =0;
  
//-------------------------- Menu Strings --------------------------------------

    char MainMenuText[7][23] = {{"BANKS ECONOMIND"},{"PROGRAM"},{"DIAGNOSTICS AND INFO"},{"DATA ACQUISITION"},{"DISPLAY SETTINGS"},{"SOFTWARE INFO"},{"BANKS CONTACT INFO"}};   
    char ProgramMenuText[3][23] = {{"PROGRAM"},{"LOAD PROGRAM"},{"LOAD STOCK PROGRAM"}};
    char VehicleMenuText[4][23] = {{"DIAGNOSTIC INFO"},{"READ DTCs"},{"CLEAR DTCs"},{"READ FAMILY"}};  
    char DataMenuText[3][23] = {{"ACQUISITION"},{"ACQUIRE DATA"},{"SETUP DATA"}};
    char DeviceMenuText[4][23] = {{"DISPLAY"},{"LCD CONTRAST"},{"LCD BACKLIGHT"},{"SOUNDS"}};
 
    menuIndex = 0;
    subMenuIndex = 0;
    prevMainMenuIndex = 1; // Do not set = 0

    LCD_2_Areas();
    
    while(1)
    {
        switch(menuIndex)
        {
		    case BANKSMAIN:     
                menuIndex = DisplayMenu(MainMenuText, 7);
            break;
                        
            case BANKSPROGRAMVEHICLE:
                 prevMainMenuIndex = 1; // Do not set = 0
	             subMenuIndex = DisplayMenu(ProgramMenuText, 3);
                 if(subMenuIndex == 1)
                 {
                     LCD_1_Area();
                     tunetype = TuneType();
                     if((tunetype != STRATEGY_TUNE)&&(tunetype != CUSTOM_TUNE))
                        break;
                     if(settings.DownloadFailed == 0)
                     {
                        if(tunetype == STRATEGY_TUNE)
                        {
                            LCD_1_Area();
                            #if (SUPPORT_STRATEGYTUNE)
                                #if (__ENABLE_PROGRAMVEHICLE)
                                    if (Settings_IsStrategyTuneAllow)
                                    {
                                        FlashVehicle(STRATEGY_TUNE);
                                    }
                                    else
                                    {
                                        ERROR_Message(STRATEGYTUNENOTSUPPORTED);    
                                    }
                                #endif
                            #else
                                ERROR_Message(STRATEGYTUNENOTSUPPORTED);
                            #endif
                        }
                        else
                        {
                            LCD_1_Area();
                            #if (SUPPORT_CUSTOMTUNE)
                                #if (__ENABLE_PROGRAMVEHICLE)
                                    FlashVehicle(CUSTOM_TUNE);
                                #endif
                            #else
                                ERROR_Message(CUSTOMTUNENOTSUPPORTED);
                            #endif
                        }
                        menuIndex = 1;
                     }
                     else              //download failed; only thing to do is return to stock
                     {
                         LCD_2_Areas();
                         LCD_gselvp(1);
                         LCD_gclrvp();
                         gsetcpos(0,0);						// Set graphics Position
                         gputs("Download failure detected. If vehicle does not start, return vehicle to stock");
                         gsetcpos(0,6);
                         gputs("Press ENT");
                       
                         while(getKey() != SW_ENTER);
                     }
                     break;
                 }   
                 if(subMenuIndex == 2)
                 {
                     LCD_1_Area();
                     #if (__ENABLE_PROGRAMVEHICLE)
                         ReturnVehicleStock();
                     #endif
                     menuIndex = 1;
				     break;
                 }
                 prevMainMenuIndex = menuIndex;
                 subMenuIndex = 0;
                 menuIndex = 0;
                 LCD_2_Areas();
		    break;                        


            case BANKSDTC:
                 prevMainMenuIndex = 1; // Do not set = 0
                 subMenuIndex = DisplayMenu(VehicleMenuText, 4);
                 if(subMenuIndex == 1)
                 {
                     LCD_1_Area();
                     ReadDTC(); // Read DTCS
                     menuIndex = 2;
                     break;
                 }
                 if(subMenuIndex == 2)
                 {
                     LCD_1_Area();
                     ClearDTC();// Clear DTCS
                     menuIndex = 2;
                     break;
                 }   
                 if(subMenuIndex == 3)
                 {
                     LCD_1_Area();
                     ReadVehicleInfo();// Read Strategy
                     menuIndex = 2;
                     break;
                 }
                 prevMainMenuIndex = menuIndex;
                 menuIndex = 0;
                 LCD_2_Areas();
            break;


            case BANKSPERFORMANCE:
                 prevMainMenuIndex = 1; // Do not set = 0                    
                 subMenuIndex = DisplayMenu(DataMenuText, 3);
                 if(subMenuIndex == 1)
                 {
                    LCD_1_Area();
                    #if (__ENABLE_DATALOG)
                        Performance_Monitor(1);
                    #endif
                    menuIndex = 3;
                    break;
                 }  
                 if(subMenuIndex == 2)
                 {
                    LCD_1_Area();
                    #if (__ENABLE_DATALOG)
                        Performance_Setup(1);
                    #endif
                     menuIndex = 3;
			         break;
                 }

                 prevMainMenuIndex = menuIndex;
                 menuIndex = 0;
                 LCD_2_Areas();
            break;

            case BANKSDEVICESETTINGS:
                 prevMainMenuIndex = 1; // Do not set = 0                    
                 subMenuIndex = DisplayMenu(DeviceMenuText, 4);
                 if(subMenuIndex == 1)
                 {
                     LCD_1_Area();
                     AdjustContrastHelper();// LCD Contrast
                     menuIndex = 4;                    
	             	 break;
                 }
                 if(subMenuIndex == 2)
                 {
                     LCD_1_Area();
                     AdjustBrightnessHelper();// LCD Backlight
                     menuIndex = 4;                    
		     		 break;
                 }

                 if(subMenuIndex == 3)
                 {
                     LCD_1_Area();
                     AdjustSound_USBpwr_Helper();// Sounds while USB powered
                     menuIndex = 4;                    
                     break;
                 }

                 prevMainMenuIndex = menuIndex;
                 menuIndex = 0;
                 LCD_2_Areas();
            break;

            case BANKSDEVICEINFO:
                 LCD_1_Area();
                 MENUS_DeviceInfo();
                 prevMainMenuIndex = menuIndex;
                 menuIndex = 0;
                 LCD_2_Areas();
            break;

            case BANKSDEALERINFO:
                 LCD_1_Area();
                 MENUS_DealerInfo();
                 prevMainMenuIndex = menuIndex;
                 menuIndex = 0;
                 LCD_2_Areas();
            break;

        }
        
        refreshmenu = Datalog_Passthrough(refreshmenu);
    }
}

#else   /////////////////////////////////////////// NOT BANKS

u8 HandleMainMenu()
{
    MENUITEM    m;
    u8          refreshmenu = TRUE;
    u8          sel;
    
    while(1)
    {
        //TODOQ: refreshmenu = Datalog_Passthrough(refreshmenu);
        
        if(getKey() == SW_EXIT)
        {
            m = NULLMENUITEM;
            refreshmenu = TRUE;
            break;
        }
        
        if (refreshmenu == TRUE)
        {
            refreshmenu = FALSE;
            LCD_gclrvp();
            m = ShowIconMenu(MAINMENU);
        }
        
        switch(m)
        {
        case PROGRAMVEHICLE:
            FreeMenuInBuffer();
            sel = ShowIconMenu(PROGRAMVEHICLEMENU);
            HandleProgramVehicleMenu(sel);
            refreshmenu = TRUE;
            CurrentMenu = NULLMENU;
            break;
        case DTC:
            sel = ShowIconMenu(DTCMENU);
            refreshmenu = HandleDTCMenu(sel);
            break;
        case DEALERINFO:
            //TODOQ: MENUS_DealerInfo();
            refreshmenu = TRUE;
            break;
        case PERFORMANCE:
            sel = ShowIconMenu(PERFORMANCEMENU);
            refreshmenu = HandlePerformanceMenu(sel);
            break;
        case DEVICESETTINGS:
            sel = ShowTextMenu(DEVICESETTINGSMENU);
            refreshmenu = HandleDeviceSettingsMenu(sel);
            break;
        case DEVICEINFO:
            //TODOQ: MENUS_DeviceInfo();
            refreshmenu = TRUE;
            break;
        default:
            //if (isPTDatalog) refreshmenu = FALSE;
            //else refreshmenu = TRUE;
            refreshmenu = TRUE;
            m = NULLMENUITEM;
            break;
        }//switch(m)
    }//while(1)
    return 1;
}
#endif
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
void MenuRotateLeft(void)
{
    if (CurrentIcon == 0)
    {
        CurrentIcon = (TotalIcon/ICON_PER_SET) - 1;
    }
    else
    {
        CurrentIcon--;
    }
}
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
void MenuRotateRight(void)
{
    if ((CurrentIcon+1) >= (TotalIcon/ICON_PER_SET))
    {
        CurrentIcon = 0;
    }
    else
    {
        CurrentIcon++;
    }
}

//------------------------------------------------------------------------------
// a do nothing function for null function pointer
//------------------------------------------------------------------------------
u8 NullHelper(u32 v, u8 k)
{
    return S_SUCCESS;
}

//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
u8 HandleProgramVehicleMenu(u8 sel)
{
//TODOQ: for now
#define SUPPORT_STRATEGYTUNE    1
#define SUPPORT_CUSTOMTUNE      1
#define __ENABLE_PROGRAMVEHICLE 1
#define __ENABLE_DATALOG        1
#define Settings_IsStrategyTuneAllow (TRUE) //(settings.DeviceType != GMBINX3)

    switch(sel)
    {
    case PROGRAM_STRATEGY:
#if (SUPPORT_STRATEGYTUNE)
    #if (__ENABLE_PROGRAMVEHICLE)
        if (Settings_IsStrategyTuneAllow)
        {
            obd2_x3p_function_programtunetovehicle(PRELOADED_TUNE);
        }
        else
        {
            ERROR_Message(STRATEGYTUNENOTSUPPORTED);
        }
    #endif  //#if (__ENABLE_PROGRAMVEHICLE)...
#else
        ERROR_Message(STRATEGYTUNENOTSUPPORTED);
#endif
        break;
    case PROGRAM_CUSTOM:
#if (SUPPORT_CUSTOMTUNE)
    #if (__ENABLE_PROGRAMVEHICLE)
        obd2_x3p_function_programtunetovehicle(CUSTOM_TUNE);
    #endif
#else
        ERROR_Message(CUSTOMTUNENOTSUPPORTED);
#endif
        break;
    case PROGRAM_STOCK:
#if (__ENABLE_PROGRAMVEHICLE)
        obd2_x3p_function_returnvehicletostock();
#endif
        break;
    case UPLOAD_STOCK:
#if (__ENABLE_PROGRAMVEHICLE)
        /*  //TODOQ: commented for now
        if(settings.married == 0)
        {
            u8 *text;
            u8 *text2;
            AudioCode sound;
            
            status = Upload_unsupported_Stock(0);
            if(status == S_ALREADY_UNLOCKED)
            {
                sound = ERRORMSG;
                text = (void*)*Language->TXT_DeviceUnlocked;
                text2 = (void*)*Language->TXT_UploadAborted;
            }
            else if(status == S_FAIL)
            {
                sound = ERRORMSG;
                text = (void*)*Language->TXT_UploadFailed;
                text2 = (void*)*Language->TXT_NULLSTR;
            }
            else if(status == S_SUCCESS)
            {
                sound = PROGRAMDONE;
                text = (void*)*Language->TXT_UploadSuccessful;
                text2 = (void*)*Language->TXT_NULLSTR;
            }
            
            if((status != 0x98)&&(status != S_USERABORT))
            {
                ShowMessage(SW_ENTER,sound,(void*)*Language->HDR_UploadECU,
                            (void*)*Language->FDR_ContinueKeyMsg,
                            text,
                            text2,
                            (void*)*Language->TXT_NULLSTR,CLEAR_TOP);
            }
            
            settings.married =0;
            settings_update();
        }
        else
        {
            ERROR_Message(DEVICE_MARRIED);
        }
        */
#endif
        break;
    default:
        return TRUE;
    }
    return FALSE;
}



//------------------------------------------------------------------------------
// Input:   u8  sel             selection from previous menu
// Return:  u8  refreshmenu
//------------------------------------------------------------------------------
u8 HandleDTCMenu(u8 sel)
{
    switch(sel)
    {
    case DTC_READ:
        obd2_x3p_function_readdtc();
        break;
    case DTC_CLEAR:
        obd2_x3p_function_cleardtc();
        break;
    case VEHICLE_INFO:
        obd2_x3p_function_readvehicleinfo();
        break;
    default:
        return TRUE;
    }
    return FALSE;
}

//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
u8 HandlePerformanceMenu(u8 sel)
{

    switch(sel)
    {
    case PERFORMANCESETUP:
#if (__ENABLE_DATALOG)
        obd2_x3p_function_datalog_setup(TRUE);
#endif
        break;
    case PERFORMANCEMONITOR:
#if (__ENABLE_DATALOG)
        obd2_x3p_function_datalog_start(TRUE);
#endif
        break;

    case EXTERNALINPUTS:
        HandleExternalInputsMenu();
        break;

/* X3P does not support these functions
      case ALERTSETUP:
        break;
      case QUARTERMILE:
        break;
      case MPH:
        break;
      case HPTQ:
        break;
      case SHIFTLIGHT:
        break;
*/
    default:
        return TRUE;
    }

    return FALSE;
}

//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
u8 HandleDeviceSettingsMenu(u8 sel)
{
    switch(sel)
    {
      case DEVICE_BACKLIGHT:
        //AdjustBrightnessHelper();
        break;
      case DEVICE_CONTRAST:
        //AdjustContrastHelper();
        break;
/*
      case DEVICE_BUZZER:
        AdjustBuzzerHelper();
        break;
*/
      case DEVICE_SOUND_USB_PWR:
        //AdjustSound_USBpwr_Helper();
        break;
/*
      case DEVICE_LANGUAGE:
        AdjustLanguageHelper();
        break;
*/
      default:
        return TRUE;
    }
    return FALSE;
}


//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
u8 HandleExternalInputsMenu(void)
{
    MENUITEMSTR *mitem;
    u8          status,index;

    while(1)
    {
        mitem = __malloc(2*sizeof(MENUITEMSTR));
        ShowMessage(NOKEY, NOAUDIO, (void*)*Language->HDR_SetupAnalog,          // 1
                    (void*)*Language->FDR_ExitDoneKeyMsg,
                    (void*)*Language->TXT_NULLSTR,
                    (void*)*Language->TXT_NULLSTR,
                    (void*)*Language->TXT_NULLSTR, CLEAR_TOP);
        ParseMenuItemStr((void*)*Language->TXT_Channel1, 0, mitem, 0);
        ParseMenuItemStr((void*)*Language->TXT_Channel2, 1, mitem, 0);
        status = Scroller(&index,0,mitem,2, 2, 0, 8, 1, 32,NullHelper,NullHelper);
        __free(mitem);
        if (status == S_USERABORT)
        {
            return S_USERABORT;
        }
        SetupAnalogChannel(++index);
    }
}
        
//------------------------------------------------------------------------------
// Input:   float   *value              (initial value)
//          u8      isfloat             (1:float, 0: integer)
//          u8      isstepadjustable    (0: step = 1, else allow to adjust stepping)
//          u8      col
//          u8      row                 (position for instruction text)
// Output:  float   *value              (final value)
// Return:  u8      status              (S_USERABORT, S_SUCCESS)
//------------------------------------------------------------------------------
u8 NumberGenerator2(u8 isstepadjustable, float* value, u8 isfloat, u8 issigned,
                    u8 col, u8 row)
{
    const u8    dig_line[] = {'0','1','2','3','4','5','6','7','8','9','.','-',0x1B,0}; //font: info2
    u8          buf[10];
    u8          tmpbuf[10];
    u8          index,pos,decimal,negative;
    u8          i,key;
    u32         tmpv[2];
    u32         div;
    float       v;

    pos = 12;

    LCD_gselfont(&info2);
    LCD_gputscpos(col,(row+2),(void*)dig_line);
    LCD_ginvertvp((col+pos)*8,(row+2)*8,(col+pos)*8+7,(row+3)*8-1);

    memset((void*)buf,0,10);
    memset((void*)buf,'_',8);
    //TODO: check for oversize value
    sprintf((void*)buf,"%.3f",*value);

    for(i=0;i<8;i++)
    {
        if (buf[i] == 0)
        {
            buf[i] = '_';
        }
    }
    for(i=0;i<8;i++)
    {
        if (buf[i] == '_')
        {
            index = i;
            break;
        }
    }

    negative = FALSE;           // '-' is not selected
    /*
    if (*value < 0)
    {
        negative = TRUE;
    }
    */
    if (buf[0] == '-')
    {
        negative = TRUE;
    }
    
    decimal = FALSE;            // '.' is not selected
    for(i=0;i<8;i++)
    {
        if (buf[i] == '.')
        {
            decimal = TRUE;
            break;
        }
    }

    LCD_gputscpos(col,row,buf);
    
    while(1)
    {
        key = getKey();
        if (key == SW_EXIT)
        {
            break;
        }
        else if (key == SW_BACK)
        {
            if (pos == 0)
            {
                pos = 12;
            }
            else
            {
                pos--;
            }
            LCD_gputscpos(col,(row+2),(void*)dig_line);
            LCD_ginvertvp((col+pos)*8,(row+2)*8,(col+pos)*8+7,(row+3)*8-1);
        }
        else if (key == SW_NEXT)
        {
            if (pos >= 12)
            {
                pos = 0;
            }
            else
            {
                pos++;
            }
            LCD_gputscpos(col,(row+2),(void*)dig_line);
            LCD_ginvertvp((col+pos)*8,(row+2)*8,(col+pos)*8+7,(row+3)*8-1);
        }
        else if (key == SW_ENTER)
        {
            if (dig_line[pos] == 0x1B)  // '<-'
            {
                if (index != 0)
                {
                    index--;
                }
                if (buf[index] == '.')
                {
                    decimal = FALSE;
                }
                else if (buf[index] == '-')
                {
                    negative = FALSE;
                }
                else if (index == 0)
                {
                    Buzzer(ERRORMSG);
                }
                buf[index] = '_';
            }
            else if (dig_line[pos] == '-')
            {
                memcpy((void*)tmpbuf,(void*)buf,10);
                if (negative == FALSE)
                {
                    buf[0] = '-';
                    memcpy((void*)&buf[1],(void*)tmpbuf,9);
                    buf[9] = 0;
                    index++;
                    negative = TRUE;
                }
                else
                {
                    memcpy((void*)buf,(void*)&tmpbuf[1],9);
                    index--;
                    negative = FALSE;
                }
            }
            else if (dig_line[pos] == '.')
            {
                if (decimal == FALSE)
                {
                    if ((index == 0) ||
                        (index == 1 && buf[0] == '-'))
                    {
                        buf[index++] = '0';
                    }
                    decimal = TRUE;
                    buf[index++] = dig_line[pos];
                }
                else
                {
                    Buzzer(ERRORMSG);
                }
            }
            else
            {
                buf[index] = dig_line[pos];
                if ((index == 1 && buf[0] == '0' && buf[1] == '0') ||
                    (index == 2 && buf[0] == '-' && buf[1] == '0' && buf[2] == '0'))
                {
                    buf[index] = '_';
                }
                else if ((index == 1 && buf[0] == '0' && buf[1] != '0') ||
                         (index == 2 && buf[0] == '-' && buf[1] == '0' && buf[2] != '0'))
                {
                    buf[index] = '_';
                    buf[index-1] = dig_line[pos];
                }
                else
                {
                    index++;
                }
            }
            LCD_gputscpos(col,row,buf);
            if (index == 8)
            {
                break;
            }
        }
        else
        {
            continue;
        }
    }

    key = 0;
    div = 1;
    tmpv[0] = 0;
    tmpv[1] = 0;
    for(i=0;i<10;i++)
    {
        if (buf[i] == '_')
        {
            break;
        }
        else if (buf[i] == '.')
        {
            key = 1;
        }
        else if (buf[i] >= '0' && buf[i] <= '9')
        {
            tmpv[key] *= 10;
            tmpv[key] += buf[i] - '0';
            if (key == 1)
            {
                div *= 10;
            }
        }
    }
    v = (float)tmpv[0];
    v += (float)tmpv[1] / (float)div;
    if (negative == TRUE)
    {
        v *= -1.0;
    }
    *value = v;

    LCD_gselfont(&msfont);
    return 0;
}

//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
void Progress_Bar(u32 filesize, u32 completed, u32 buffersize, bool bar_enable)
{
    u32     totalblocks;
    u32     x;
    u32     percentage = 0;
    u8      DispPercent[5];

    if ((buffersize == 1) && (filesize <= 100))
    {
        completed *= 1000;
        filesize *= 1000;
    }
    
    memset(DispPercent,0,5);
    totalblocks = (u32)(filesize/buffersize);
    x = GDISPW*completed/totalblocks;
    percentage = 100*completed/totalblocks;

    sprintf((void*)DispPercent,"%u%%",percentage);
    
    LCD_gselvp(0);
    LCD_gselfont(&info2);
    LCD_gsetmode(GALIGN_HCENTER);        // Header is drawn horizontally aligned

    if(bar_enable == TRUE)
    {
        if(x > 1)
        {
            grectangle(0,29,128,44);
            if(x != 0)
            LCD_gfillvp(0,29,x,44,0xFFFF);        
        }
        else
        {
            LCD_grectangle(0,29,128,44);
        }
    }
    LCD_gsetpos(0,40);
    LCD_gputs((void*)DispPercent);
}

//------------------------------------------------------------------------------
// Input:   u8  ch      (ch1 = 1, ch2 = 2)
// Engineer: Quyen Leba
//------------------------------------------------------------------------------
void SetupAnalogChannel(u8 ch)
{
    u8 sel_result,status,sel_index,isdone;
    u8 tmpbuf[26];
    u8 sel_equation;    // in [][] format -> (0=nothing, 0x0F=off, 0xab=equate where a=category,b=sub) note: valid equate >= 0x10;
    u8 i,j;
    u8 adjustguide = 0; //0: nop 1:add 2: sub

    //TODOQ: check this again, why only A2
    for (j=0;j<DATALOG_MAXITEMS;j++)
    {
        if (SETTINGS_DATALOG(selected_pids)[j] == SETTINGS_DATALOG(analog2_selected_index))
        {
            break;
        }
    }
    if (SETTINGS_DATALOG(analog2_selected_index) == 0xFF || j >= DATALOG_MAXITEMS)
    {
        j=0xFF;
    }
    
    if (ch == 1)
    {
        strcpy((void*)tmpbuf,(void*)*Language->HDR_SetupAnalog1);
    }
    else
    {
        strcpy((void*)tmpbuf,(void*)*Language->HDR_SetupAnalog2);
    }

    do
    {
        isdone = 1;
        MENUITEMSTR* mitem;
        ShowMessage(NOKEY, NOAUDIO, (void*)tmpbuf,
                    (void*)*Language->FDR_ContinueKeyMsg,
                    (void*)*Language->TXT_NULLSTR,
                    (void*)*Language->TXT_NULLSTR,
                    (void*)*Language->TXT_NULLSTR, CLEAR_TOP);

        mitem = __malloc(AnalogMenuIndex[0]*sizeof(MENUITEMSTR));
        for(i=0;i<AnalogMenuIndex[0];i++)
        {
            ParseMenuItemStr((void*)AnalogMenu[0][i], i, mitem, 0);
        }
        sel_result = 0xFF;

        status = Scroller(&sel_result,0,mitem,
                          AnalogMenuIndex[0],
                          3,
                          0, 8, 1, 32,NullHelper,NullHelper);
        
        __free(mitem);
        
        if (status == S_USERABORT)        // do not update analog setup
        {
            sel_equation = 0x00;
        }
        else if (sel_result == 4)        // Off
        {
            sel_equation = 0x0F;
        }
        else if (status == S_SUCCESS)
        {
            //LCD_gclearareafast(1,3,240,14);
            ShowMessage(NOKEY, NOAUDIO, (void*)tmpbuf,                          // 3
                    (void*)*Language->FDR_ContinueKeyMsg,
                    (void*)*Language->TXT_NULLSTR,
                    (void*)*Language->TXT_NULLSTR,
                    (void*)*Language->TXT_NULLSTR, CLEAR_TOP);

            LCD_gputsrpos(1,24,(void*)AnalogMenu[0][sel_result]);

            sel_index = sel_result+1;
            sel_equation = sel_index<<4;
            
            mitem = __malloc(AnalogMenuIndex[sel_index]*sizeof(MENUITEMSTR));
            for(i=0;i<AnalogMenuIndex[sel_index];i++)
            {
                ParseMenuItemStr((void*)AnalogMenu[sel_index][i], i, mitem, 0);
            }
            sel_result = 0xFF;

            status = Scroller(&sel_result,0,mitem,
                              AnalogMenuIndex[sel_index],
                              3,
                              0, 8, 16, 32,NullHelper,NullHelper);
            __free(mitem);
            
            if (status == S_USERABORT)
            {
                isdone = 0;
                break;
            }
            else
            {
                sel_equation += sel_result;
            }
            CustomEq tmpEq = SETTINGS_DATALOG(analogcustomequation)[ch-1];
            status = S_FAIL;
            if (sel_index == 1 && sel_result == 7)
            {
                status = SetCustomEquation(0, &tmpEq);
            }
            else if (sel_index == 4 && sel_result == 1)
            {
                status = SetCustomEquation(1, &tmpEq);
            }
            if (status == S_SUCCESS)
            {
                SETTINGS_DATALOG(analogcustomequation)[ch-1] = tmpEq;
            }
        }
    } while(!isdone);

    if (status == S_USERABORT)
    {
        return;
    }
    
    //save selection
    //TODO: unit
    u8* Aselectednum;
    u8* Aon;
    u8* Aequation;
    u8* Adescription;
    //u8* Aunit;
    
    //TODO: change settings struct to get rid of this
    if (ch == 1)
    {
        Aon = &SETTINGS_DATALOG(analog1_on);
        Aequation = &SETTINGS_DATALOG(analog1_equation);
        Adescription = SETTINGS_DATALOG(analog1_description);
        Aselectednum = &SETTINGS_DATALOG(analog1_selected_index);
        if (SETTINGS_DATALOG(analog1_on) == 0)
        {
            adjustguide = 0;    //nop
        }
        else if (SETTINGS_DATALOG(analog1_on) == 0)
        {
            adjustguide = 1;    //add
        }
        else
        {
            adjustguide = 2;    //sub
        }
    }
    else
    {
        Aon = &SETTINGS_DATALOG(analog2_on);
        Aequation = &SETTINGS_DATALOG(analog2_equation);
        Adescription = SETTINGS_DATALOG(analog2_description);
        Aselectednum = &SETTINGS_DATALOG(analog2_selected_index);
    }
    
    if (sel_equation == 0x00)            // S_USERABORT
    {
        //do nothing
    }
    else if (sel_equation == 0x0F)        // Off
    {
        *Aon = 0;
        //TODO
        for (i=0;i<6;i++)
        {
            if (SETTINGS_DATALOG(selected_pids)[i] == *Aselectednum &&
                *Aselectednum != 0xFF)
            {
                SETTINGS_DATALOG(selected_pids)[i] = 0xFF;
                SETTINGS_DATALOG(alert_selected_index)[i] = 0xFF;
                break;
            }
        }
        *Aselectednum = 0xFF;
        if (adjustguide == 1)
        {
            adjustguide = 0;
        }
    }
    else
    {
        *Aon = 1;
        *Aequation = sel_equation;
        strcpy((char*)Adescription,
               (char*)AnalogMenu[sel_equation>>4][sel_equation&0x0F]);
        if (adjustguide == 2)
        {
            adjustguide = 0;
        }
    }

    if (j != 0xFF)
    {
        if (adjustguide == 1)
        {
            //TODO
            SETTINGS_DATALOG(analog1_selected_index)++;
            SETTINGS_DATALOG(selected_pids)[j]++;
            if (SETTINGS_DATALOG(alert_selected_index)[j] != 0xFF)
            {
                SETTINGS_DATALOG(alert_selected_index)[j]++;
            }
        }
        else if (adjustguide == 2)
        {
            //TODO
            SETTINGS_DATALOG(analog2_selected_index)--;
            SETTINGS_DATALOG(selected_pids)[j]--;
            if (SETTINGS_DATALOG(alert_selected_index)[j] != 0xFF)
            {
                SETTINGS_DATALOG(alert_selected_index)[j]--;
            }
            if (j > 0 && SETTINGS_DATALOG(selected_pids)[j-1] == 0xFF)
            {
                //TODO
                SETTINGS_DATALOG(selected_pids)[j-1] = SETTINGS_DATALOG(selected_pids)[j];
                SETTINGS_DATALOG(alert_selected_index)[j-1] = SETTINGS_DATALOG(alert_selected_index)[j];
                SETTINGS_DATALOG(alertmax)[j-1] = SETTINGS_DATALOG(alertmax)[j];
                
                SETTINGS_DATALOG(selected_pids)[j] = 0xFF;
                SETTINGS_DATALOG(alert_selected_index)[j] = 0xFF;
                SETTINGS_DATALOG(alertmax)[j] = 1.0;
            }
        }
    }//if (j != 0xFF)...

    SETTINGS_SetDatalogAreaDirty();
    settings_update(FALSE);
    LCD_gclrvp();
}

//------------------------------------------------------------------------------
// Input:   u8  type            (0: Wideband, 1: Raw)
// Engineer: Quyen Leba
//------------------------------------------------------------------------------
u8 SetCustomEquation(u8 type, CustomEq *c)
{
    u8          index, status;
    MENUITEMSTR *mitem;
    float       c1,c2;

    ShowMessage(NOKEY,NOAUDIO,(void*)*Language->HDR_SetupCustomEquation,        // 4
                (void*)*Language->TXT_NULLSTR,
                (void*)*Language->TXT_NULLSTR,
                (void*)*Language->TXT_NULLSTR,
                (void*)*Language->TXT_NULLSTR,CLEAR_TOP);

    LCD_gputsrpos(0,24,*Language->TXT_SelectEquation);
    LCD_gselfont(&msfont);
    LCD_gputsrpos(0,48,*Language->TXT_SelectEquationNote1);
    LCD_gputsrpos(0,56,*Language->TXT_SelectEquationNote2);


    mitem = __malloc(2*sizeof(MENUITEMSTR));
    ParseMenuItemStr("(v * a) + b", 0, mitem, 0);
    ParseMenuItemStr("(v + a) * b", 1, mitem, 0);

    status = Scroller(&index, 0, mitem, 2, 8, 0, 8, 0, 32, NullHelper, NullHelper);

    __free(mitem);
    
    if (type == 0 && index == c->type[0])
    {
        c1 = c->a[0];
        c2 = c->b[0];
    }
    else if (type == 1 && index == c->type[1])
    {
        c1 = c->a[1];
        c2 = c->b[1];
    }
    else
    {
        c1 = 1.0;
        c2 = 1.0;
    }

    LCD_gselfont(&msfont);
    if (status == S_USERABORT)
    {
        return S_USERABORT;
    }

    ShowMessage(NOKEY,NOAUDIO,(void*)*Language->HDR_SetupCustomEquation,
                (void*)*Language->FDR_ExitDoneKeyMsg,
                (void*)*Language->TXT_NULLSTR,
                (void*)*Language->TXT_NULLSTR,
                (void*)*Language->TXT_NULLSTR,CLEAR_TOP);
    LCD_gputsrpos(0,24,*Language->TXT_EnterValueA);
    status = NumberGenerator2(1,&c1,1,1,0,4);

    ShowMessage(NOKEY,NOAUDIO,(void*)*Language->HDR_SetupCustomEquation,
                (void*)*Language->FDR_ExitDoneKeyMsg,
                (void*)*Language->TXT_NULLSTR,
                (void*)*Language->TXT_NULLSTR,
                (void*)*Language->TXT_NULLSTR,CLEAR_TOP);
    LCD_gputsrpos(0,24,*Language->TXT_EnterValueB);
    status = NumberGenerator2(1,&c2,1,1,0,4);


    //Wideband
    if (type == 0)
    {
        c->a[0] = c1;
        c->b[0] = c2;
        c->type[0] = index;
    }
    else    // Raw
    {
        c->a[1] = c1;
        c->b[1] = c2;
        c->type[1] = index;
    }
    return S_SUCCESS;
}

//------------------------------------------------------------------------------
// Display/scroll datalog items on LCD
// Input:   Datalog_Info *dataloginfo
// Return:  none
// Engineer: Quyen Leba
// Note: this function is run at an interval (not frequent), it requires
// keybuffer updated by from DatalogStartNormal during datalogging
//------------------------------------------------------------------------------
void MenuDatalogShowValue(Datalog_Info *dataloginfo)
{
#define MAX_DATALOG_ITEM_COUNT          6
    static u8 currentindex = 0;
    static u8 max_item_count = MAX_DATALOG_ITEM_COUNT+1;
    static bool isscreenupdated = TRUE;
    static u8 xx = 0;
    u8  strbuf[24];
    u8  key;
    u8  i;
    u8  index;

    key = getKey();
    if (key == SW_DOWN || keybuffer == SW_DOWN)     // scroll down
    {
        keybuffer = NOKEY;
        currentindex++;
        if (currentindex >= dataloginfo->datalogsignalcount)
        {
            currentindex = 0;
        }
        isscreenupdated = TRUE;
    }
    else if (key == SW_UP || keybuffer == SW_UP)    // scroll up
    {
        keybuffer = NOKEY;
        if (currentindex > 0)
        {
            currentindex--;
        }
        else
        {
            currentindex = dataloginfo->datalogsignalcount - 1;
        }
        isscreenupdated = TRUE;
    }
    
    if (isscreenupdated || screenupdaterequest)
    {
        xx++;
        if (xx == 8)
        {
            xx = 0;
        }
        isscreenupdated = FALSE;
        LCD_gselfont(&msfont);
        ShowMessage(NOKEY,NOAUDIO,(void*)*Language->HDR_Monitor,
                    (void*)*Language->TXT_NULLSTR,
                    (void*)*Language->TXT_NULLSTR,
                    (void*)*Language->TXT_NULLSTR,
                    (void*)*Language->TXT_NULLSTR,CLEAR_TOP);
        if (dataloginfo->datalogsignalcount >= MAX_DATALOG_ITEM_COUNT)
        {
            max_item_count = MAX_DATALOG_ITEM_COUNT;
        }
        else
        {
            max_item_count = dataloginfo->datalogsignalcount;
        }
        
        index = currentindex;
        for(i=0;i<max_item_count;i++)
        {
            LCD_gputsrpos(0,24+8*i,dataloginfo->datalogsignals[index].ShortName);
            
            index++;
            if (index >= dataloginfo->datalogsignalcount)
            {
                index = 0;
            }
        }
    }
    
    index = currentindex;
    for(i=0;i<max_item_count;i++)
    {
        sprintf((char*)strbuf,"%8.2f",dataloginfo->datalogsignals[index].Value);
        LCD_gputsrpos(80,24+8*i,strbuf);
        
        index++;
        if (index >= dataloginfo->datalogsignalcount)
        {
            index = 0;
        }
    }
}













#ifdef BANKS_ENG
int DisplayMenu(char buffer[][23], int choiceCount)
{
  char keytemp = 0;
  
  DisplayTitle(&buffer[0][0]);

  LCD_gselvp(1);   // Switch to body view port
  LCD_gselfont(&msfont);
  LCD_gsetmode(GALIGN_LEFT); 
  
    int i;
    int cursor = 0;
    int cursorPos = 0;
    int screenOffset = 1;
    int maxScreenRows = 3;  // (6 lines)
    int maxItemRows = 0;   // space for 50 filenames malloc'd
    char updateScreen = 1, line = 0;

    // Display screen menu stuff

    if(choiceCount <= 3)
      maxScreenRows = choiceCount -1;


    
    maxItemRows = choiceCount;

while(1)
{  

    keytemp = getKey();    

    switch(keytemp)
    {
    case SW_DOWN:
      LCD_ginvertvp(0,cursorPos,128,cursorPos+10);
      cursor++;
      if(cursor > maxItemRows-1)
      {
         cursor = maxItemRows-1;
      }
      if(cursor > maxScreenRows-1)
      {
        cursor = maxScreenRows-1;
        if(cursor+screenOffset<maxItemRows-1)
        {
          screenOffset++;  // increment screen offset
          updateScreen = 1;
          break;
        }
      }
      switch(cursor)
      {
      case 0:
        cursorPos=6;
        break;
      case 1:
        cursorPos=22;
        break;
      case 2:
        cursorPos=38;
        break;
      }
      LCD_ginvertvp(0,cursorPos,128,cursorPos+10);
      break;
              
    case SW_UP:
        LCD_ginvertvp(0,cursorPos,128,cursorPos+10);  
        cursor--;
        if(cursor<0)
        {
          cursor = 0;       
          if(screenOffset>1)
          {
            screenOffset--;  // decrement screen offset
            updateScreen = 1;
            break;
          }
        }
        switch(cursor)
        {
        case 0:
          cursorPos=6;
          break;
        case 1:
          cursorPos=22;
          break;
        case 2:
          cursorPos=38;
          break;
        }
        LCD_ginvertvp(0,cursorPos,128,cursorPos+10);
      break;
      
    case SW_ENTER:
      return (cursor+screenOffset);
    case SW_NEXT:
      return (cursor+screenOffset);        
    case SW_BACK: 
      return 0;      
    case SW_EXIT:
      return 0;
    }
 
    if(updateScreen == 1)  // Redraws files names onto screen
    {
      LCD_gclrvp();           // clear viewport!!
      line = 1;
      // Display file names
      for (i=screenOffset; (i<(maxScreenRows+screenOffset)) && (i<maxItemRows); i++)
      {
        gsetcpos(0,line);
        line+=2;
        gputs(&buffer[i][0]);
      }
      
      LCD_gselfont(&bold2);
      if (screenOffset > 1)
      {
        LCD_gsetpos(119,15);
        gputch(0x0018);             // Draw arrow up
      }
      if (maxItemRows - screenOffset > maxScreenRows)
      {
        LCD_gsetpos(119,47);
        gputch(0x0019);             // Draw arrow down
      }
      LCD_gselfont(&msfont);

      switch(cursor)
        {
        case 0:
          cursorPos=6;
          break;
        case 1:
          cursorPos=22;
          break;
        case 2:
          cursorPos=38;
          break;
        }
      LCD_ginvertvp(0,cursorPos,128,cursorPos+10);
      updateScreen = 0;
    }
}
}

void DisplayTitle(char *buffer)
{
   char buf_temp[24];
   char i = 0;

   LCD_gselvp(0);
   LCD_gclrvp();           // clear viewport!!    
   LCD_gsetcpos(0,0);      // Set Character Position to 0,0
   LCD_gselfont(&bold2);
   LCD_gsetmode(GALIGN_HCENTER);

   for( i = 0; i < strlen( buffer ); i++ )   // setup BANKS ENG titles in Uppercase
   {
      buf_temp[i] = _f_toupper( buffer[i] ); 
   }  
   buf_temp[i] = NULL;
   
   buffer = NULL;
   buffer = buf_temp;
   LCD_gmoveto(0,8);
   LCD_glineto(128,8);
   LCD_gmoveto(0,9);
   LCD_glineto(128,9);

   LCD_gputs(buffer);       // Draw title on the screen

   
   return;
}
u8 TuneType(void)
{
    char    TuneTypeText2[3][23] = {{"SELECT PROGRAM"},{"BANKS DEVELOPED"},{"CUSTOM CAL"}};
    char    TuneTypeText1[2][23] = {{"SELECT PROGRAM"},{"BANKS DEVELOPED"}};
    int     tunetype = 0;
    u8      type =0;
    bool      custom_there = FALSE;
    u8      device_type = Settings_GetDeviceType;

#if (SUPPORT_STRATEGYTUNE)
    if(device_type == ECONO) // Block custom tuning
    {
        tunetype = 1;
    }
    else
    {
       custom_there = Is_custom_there();
       if(custom_there == TRUE)
         tunetype = DisplayMenu(TuneTypeText2, 3);
       else
         tunetype = DisplayMenu(TuneTypeText1, 2);
    }
#else
    tunetype = 1;
#endif
    if((u8)tunetype == 1)
        type = STRATEGY_TUNE;
    else if(((u8)tunetype == 2)&&(custom_there == TRUE))
        type = CUSTOM_TUNE;
    else
    {
        tunetype = 0;               
    }

    return(type);
}

void LCD_1_Area(void)
{
    LCD_gselvp(0);
    gsetvp(0,0,128,64); // Configure VP 0  
    LCD_gclrvp();   
  
}
void LCD_2_Areas(void)
{
    // Settings up Title viewport
    LCD_gselvp(0);
    gsetvp(0,0,128,10); // Configure VP 0  
    // Settings up Body Viewport
    LCD_gselvp(1);
    gsetvp(0,10,128,64);  // Configure VP 1
    LCD_gclrvp();           // clear viewport!!
  
    LCD_gselfont(&msfont);
    LCD_gsetmode(GALIGN_LEFT);    
  
}


bool Is_custom_there(void)
{
    F_FILE  *indexptr;

    indexptr = __fopen("/indexfile.txt","rb");
    if(indexptr == NULL)
    {
        return(FALSE);    // No custom tunes found
    }
        __fclose(indexptr);
  
    return (TRUE);
}
#endif
