/**
  **************** (C) COPYRIGHT 2011 SCT Performance, LLC *********************
  * File Name          : obd2_x3p_function.c
  * Device / System    : SCT products using the "CommonCode" platform
  * Author             : Quyen Leba
  *
  * Version            : 1 
  * Date               : 05/02/2011
  * Description        : 
  *                    : 
  *
  *
  * History            : 05/02/2011 M. Davis
  *                    :   Add SCT Copyright header,
  *
  ******************************************************************************
  */

#include <stdio.h>
#include <board/genplatform.h>
#include <board/power.h>
#include <board/indicator.h>
#include <common/statuscode.h>
#include <common/checksum.h>
#include <common/cmdif.h>
#include <common/obd2.h>
#include <common/obd2datalog.h>
#include <common/file.h>
#include <common/settings.h>
#include "msg_common.h"
#include "menus.h"
#include "menus_tuneoption.h"
#include "x3_menus_helper.h"
#include "error.h"
#include "obd2_x3p_function.h"

//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
extern Datalog_Info *dataloginfo;                   //from obd2datalog.c

//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
u8 GetFileNamesByExtension(u8 *extension, MENUITEMSTR *items, u8 *count);
u8 SelectFileByExtension(u8 *ext, u8 *filename);

//------------------------------------------------------------------------------
// Clear DTCs
// Engineer: Quyen Leba
// Date: Feb 25, 2008
//------------------------------------------------------------------------------
void obd2_x3p_function_cleardtc()
{
    u8  status;

#ifdef BANKS_ENG
    status = ShowMessage(SW_ENTER,NOAUDIO,
                         (void*)*Language->HDR_ClearDiagTroubleCode,
                         (void*)*Language->FDR_ContinueMsg,
                         (void*)*Language->TXT_NULLSTR,
                         (void*)*Language->TXT_TurnKeyOn,
                         (void*)*Language->TXT_NULLSTR,CLEAR_ALL);
#else
    status = ShowMessage(SW_ENTER,NOAUDIO,
                         (void*)*Language->HDR_ClearDiagTroubleCode,
                         (void*)*Language->FDR_ContinueKeyMsg,
                         (void*)*Language->TXT_TurnKeyOn,
                         (void*)*Language->TXT_DoNotStartEngine,
                         (void*)*Language->TXT_NULLSTR,CLEAR_ALL);
#endif
    if (status == SW_EXIT)
    {
        return;
    }

    ShowMessage(NOKEY,NOAUDIO,
                (void*)*Language->HDR_ClearDiagTroubleCode,
                (void*)*Language->TXT_NULLSTR,
                (void*)*Language->TXT_NULLSTR,
                (void*)*Language->TXT_PleaseWait,
                (void*)*Language->TXT_NULLSTR,CLEAR_TOP);

    status = cmdif_command(CMDIF_CMD_CLEAR_DTCS,NULL,0);
    if (status  == S_SUCCESS)
    {
        cmdif_datainfo *datainfo;
        cmdif_getResponseData(&datainfo);
        if (datainfo->responsecode == CMDIF_ACK_OK)
        {
            ShowMessage(SW_ENTER,NOAUDIO,
                        (void*)*Language->HDR_ClearDiagTroubleCode,
                        (void*)*Language->FDR_ContinueKeyMsg,
                        (void*)*Language->TXT_NULLSTR,
                        (void*)*Language->TXT_ClearDTCSuccessful,
                        (void*)*Language->TXT_NULLSTR,CLEAR_ALL);
            return;
        }
    }

    ShowMessage(SW_ENTER,NOAUDIO,
                (void*)*Language->HDR_ClearDiagTroubleCode,
                (void*)*Language->FDR_ContinueKeyMsg,
                (void*)*Language->TXT_NULLSTR,
                (void*)*Language->TXT_ClearDTCFail,
                (void*)*Language->TXT_NULLSTR,CLEAR_ALL);
}

//------------------------------------------------------------------------------
// Read DTCs & display on LCD
// Engineer: Quyen Leba
// Date: Feb 25, 2008
//------------------------------------------------------------------------------
void obd2_x3p_function_readdtc()
{
    u8 status;
    dtc_info *dtcinfo_ptr;

    status = ShowMessage(SW_ENTER,NOAUDIO,
                         (void*)*Language->HDR_ReadDiagTroubleCode,
#ifdef BANKS_ENG
                         (void*)*Language->FDR_ContinueMsg,
#else
                         (void*)*Language->FDR_ContinueKeyMsg,
#endif
                         (void*)*Language->TXT_NULLSTR,
                         (void*)*Language->TXT_TurnKeyOn,
                         (void*)*Language->TXT_NULLSTR,CLEAR_ALL);

    if (status == SW_EXIT)
    {
        return;
    }

    ShowMessage(NOKEY,NOAUDIO,
                (void*)*Language->HDR_ReadDiagTroubleCode,
                (void*)*Language->TXT_NULLSTR,
                (void*)*Language->TXT_NULLSTR,
                (void*)*Language->TXT_PleaseWait,
                (void*)*Language->TXT_NULLSTR,CLEAR_TOP);
    
    status = cmdif_command(CMDIF_CMD_READ_DTCS_RAW,NULL,0);
    if (status != S_SUCCESS)
    {
        ShowMessage(SW_ENTER,NOAUDIO,(void*)*Language->HDR_ReadDiagTroubleCode,
                    (void*)*Language->FDR_ContinueKeyMsg,
                    (void*)*Language->TXT_NULLSTR,
                    (void*)*Language->TXT_ReadDTCFail,
                    (void*)*Language->TXT_NULLSTR,CLEAR_ALL);
        return;
    }
    
    cmdif_datainfo *datainfo;
    cmdif_getResponseData(&datainfo);
    dtcinfo_ptr = (dtc_info*)datainfo->dataptr;
    
    if (dtcinfo_ptr->count == 0)
    {
        ShowMessage(SW_ENTER,NOAUDIO,(void*)*Language->HDR_ReadDiagTroubleCode,
                    (void*)*Language->FDR_ContinueKeyMsg,
                    (void*)*Language->TXT_NULLSTR,
                    (void*)*Language->TXT_NoDTCFound,
                    (void*)*Language->TXT_NULLSTR,CLEAR_ALL);
    }
    else
    {
        MENUITEMSTR* mitem;
        u8 i;
        u8 code_buffer[64];
        u8 desc_buffer[256];
        u16 dtccode;

        while(1)
        {
#ifdef BANKS_ENG
            ShowMessage(NOKEY,NOAUDIO,                              
                        (void*)*Language->HDR_ReadDiagTroubleCode, 
                        (void*)*Language->TXT_NULLSTR,
                        (void*)*Language->TXT_NULLSTR,
                        (void*)*Language->TXT_NULLSTR,
                        (void*)*Language->TXT_NULLSTR,CLEAR_TOP);
            LCD_gputscpos(0,2,"   Scroll Up/Down");
            LCD_gputscpos(1,6,(void*)*Language->TXT_BANKS_ENT_Description );   
            LCD_gputscpos(1,7, "ESC to Exit" );
#else

            ShowMessage(NOKEY,NOAUDIO,
                        (void*)*Language->HDR_ReadDiagTroubleCode,
                        (void*)*Language->TXT_NULLSTR,
                        (void*)*Language->TXT_NULLSTR,
                        (void*)*Language->TXT_NULLSTR,
                        (void*)*Language->TXT_NULLSTR,CLEAR_TOP);
            LCD_gputscpos(1,6, "SELECT: Description" );   
            LCD_gputscpos(1,7, "CANCEL: Exit" );
#endif
            mitem = __malloc(dtcinfo_ptr->count*sizeof(MENUITEMSTR));
            for(i=0;i<dtcinfo_ptr->count;i++)
            {
                dtccode = dtcinfo_ptr->codes[i];
                //get DTC code type
                code_buffer[0] = obd2_parsetype_dtc(dtccode);
                dtccode &= 0x3FFF;
                sprintf((void*)&code_buffer[1],"%04X",dtccode);
                ParseMenuItemStr(code_buffer, i, mitem, 0);
            }
            i = 0;
            status = Scroller(&i, 0, mitem, dtcinfo_ptr->count,
                              3, 0, 8, 0, 24, NullHelper, NullHelper);
            strcpy((char*)code_buffer,(char*)mitem[i].content);
            __free(mitem);
            
            if (status == S_USERABORT)
            {
                break;
            }
            
            //display description of the selected dtc
            memset(desc_buffer,0,256);

            if (obd2_getdtcinfo(code_buffer, desc_buffer) != S_SUCCESS)
            {
#ifdef BANKS_ENG
                ShowMessage(SW_ENTER,NOAUDIO,
                            (void*)*Language->HDR_ReadDiagTroubleCode,
                            (void*)*Language->FDR_ContinueKeyMsg,
                            (void*)code_buffer,
                            (void*)*Language->TXT_NoDTCInfo,
                            (void*)*Language->TXT_NULLSTR,CLEAR_TOP);
#else
                ShowMessage(SW_ENTER,NOAUDIO,
                            (void*)*Language->HDR_ReadDiagTroubleCode,
                            (void*)*Language->FDR_ContinueKeyMsg,
                            (void*)code_buffer,
                            (void*)*Language->TXT_NoInfo,
                            (void*)*Language->TXT_NULLSTR,CLEAR_TOP);
#endif
            }
            else
            {
                ShowMessage(SW_ENTER,NOAUDIO,
                            (void*)*Language->HDR_ReadDiagTroubleCode,
                            (void*)*Language->FDR_ContinueKeyMsg,
                            (void*)code_buffer,
                            (void*)desc_buffer,
                            (void*)*Language->TXT_NULLSTR,CLEAR_TOP);
                //TODO: this may not be enough space
            }
        }
        LCD_gclrvp();
    }
}

//------------------------------------------------------------------------------
// Read Vehicle Information
// Engineer: Quyen Leba
// Date: Feb 25, 2008
//------------------------------------------------------------------------------
void obd2_x3p_function_readvehicleinfo()
{
    u8 buffer[256];
    u8  status,i;

    //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

#ifdef BANKS_ENG
    status = ShowMessage(SW_ENTER,NOAUDIO,
                         (void*)*Language->HDR_VehicleInfo,
                         (void*)*Language->FDR_ContinueMsg,
                         (void*)*Language->TXT_NULLSTR,
                         (void*)*Language->TXT_TurnKeyOn,
                         (void*)*Language->TXT_DoNotStartEngine,CLEAR_ALL);
#else
    status = ShowMessage( SW_ENTER, NOAUDIO,
                        ( void* ) *Language->HDR_VehicleInfo,
                        ( void* ) *Language->FDR_ContinueKeyMsg,
                        ( void* ) *Language->TXT_NULLSTR,
                        ( void* ) *Language->TXT_TurnKeyOn,
                        ( void* ) *Language->TXT_NULLSTR, CLEAR_ALL );

#endif

    if (status == SW_EXIT)
    {
        return;
    }
    
    ShowMessage(NOKEY,NOAUDIO,
                (void*)*Language->HDR_VehicleInfo,
                (void*)*Language->TXT_NULLSTR,
                (void*)*Language->TXT_NULLSTR,
                (void*)*Language->TXT_PleaseWait,
                (void*)*Language->TXT_NULLSTR,CLEAR_TOP);

    //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    
    ecm_info ecminfo;
    status = cmdif_command(CMDIF_CMD_READ_ECM_INFO,NULL,0);
    if (status  == S_SUCCESS)
    {
        cmdif_datainfo *datainfo;
        
        cmdif_getResponseData(&datainfo);
        if (datainfo->responsecode == CMDIF_ACK_OK)
        {
            memcpy((void*)&ecminfo,datainfo->dataptr,datainfo->datalength);
        }
        else
        {
            status = S_FAIL;
        }
    }

    if ((status != S_SUCCESS) || (ecminfo.ecm_count == 0))
    {
        ShowMessage(SW_EXIT,NOAUDIO,(void*)*Language->HDR_VehicleInfo,
#ifdef BANKS_ENG
                    (void*)*Language->FDR_ExitDoneKeyMsg,
#else
                    (void*)*Language->FDR_ExitKeyMsg,
#endif
                    (void*)*Language->TXT_NULLSTR,
                    (void*)*Language->TXT_NoInfo,
                    (void*)*Language->TXT_NULLSTR,CLEAR_TOP);
        return;
    }
    
    //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    // Display VIN, commtype
    //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    strcpy((char*)buffer,"VIN:");
    switch(obd2_validatevin(ecminfo.vin))
    {
    case S_VINBLANK:
        strcat((char*)buffer,(void*)*Language->TXT_Blank);
        break;
    case S_VININVALID:
        strcat((char*)buffer,(void*)*Language->TXT_Unknown);
        break;
    case S_SUCCESS:
        strcat((char*)buffer,(char*)ecminfo.vin);
        break;
    default:
        strcat((char*)buffer,(void*)*Language->TXT_Unknown);
        break;
    }
    
#ifdef BANKS_ENG
    ShowMessage(NOKEY,NOAUDIO,
                (void*)*Language->HDR_VehicleInfo,
                (void*)*Language->FDR_ContinueMsg,
                (void*)*Language->TXT_NULLSTR,
                (void*)*Language->TXT_NULLSTR,
                (void*)*Language->TXT_NULLSTR,CLEAR_TOP);
#else
    
    ShowMessage(NOKEY,NOAUDIO,
                (void*)*Language->HDR_VehicleInfo,
                (void*)*Language->FDR_ContinueKeyMsg,
                (void*)*Language->TXT_NULLSTR,
                (void*)*Language->TXT_NULLSTR,
                (void*)*Language->TXT_NULLSTR,CLEAR_TOP);
#endif    

    LCD_gselfont(&msfont);
    LCD_gputscpos(0,3,buffer);  //display VIN
    
    strcpy((char*)buffer,"Comm:");
    obd2_vehiclecommtotext(ecminfo.commtype, &buffer[5]);
        
    LCD_gputscpos(0,5,buffer);  //display vehiclecommtype
    
    status = getKeyWait(SW_ENTER,KEYPAD_ALLOWABORT);
    if (status == SW_EXIT)
    {
        return;
    }

    //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    // Display ECM/PCM info
    //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    ShowMessage(NOKEY,NOAUDIO,(void*)*Language->HDR_VehicleInfo,
                (void*)*Language->FDR_ContinueKeyMsg,
                (void*)*Language->TXT_NULLSTR,
                (void*)*Language->TXT_NULLSTR,
                (void*)*Language->TXT_NULLSTR,CLEAR_TOP);

    LCD_gselfont(&msfont);
    //LCD_gsetmode(GALIGN_HCENTER);
    if (ecminfo.codecount[0] >= 1)
    {
        LCD_gputscpos(0,2,"ECM Info");
        strcpy((char*)buffer,"Strategy: ");
        strcat((char*)buffer,(char*)ecminfo.codes[0][0]);
        LCD_gputscpos(0,3,buffer);
    }
    if (ecminfo.codecount[0] >= 2)
    {
        strcpy((char*)buffer,"CalID: ");
        strcat((char*)buffer,(char*)ecminfo.codes[0][1]);
        LCD_gputscpos(0,4,buffer);
    }
    if (ecminfo.second_codes[0][0][0] != NULL)
    {
        strcpy((char*)buffer,"SWPN: ");
        strcat((char*)buffer,(char*)ecminfo.second_codes[0][0]);
        LCD_gputscpos(0,5,buffer);
    }
    
    status = getKeyWait(SW_ENTER,KEYPAD_ALLOWABORT);
    if (status == SW_EXIT)
    {
        return;
    }
    
    //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    // Display TCM info (if available)
    //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    
    ShowMessage(NOKEY,NOAUDIO,(void*)*Language->HDR_VehicleInfo,
                (void*)*Language->FDR_ContinueKeyMsg,
                (void*)*Language->TXT_NULLSTR,
                (void*)*Language->TXT_NULLSTR,
                (void*)*Language->TXT_NULLSTR,CLEAR_TOP);
    
    LCD_gselfont(&msfont);
    //LCD_gsetmode(GALIGN_HCENTER);
    if (ecminfo.ecm_count > 1)
    {
        if (ecminfo.codecount[1] >= 1)
        {
            LCD_gputscpos(0,2,"TCM Info");
            strcpy((char*)buffer,"Strategy: ");
            strcat((char*)buffer,(char*)ecminfo.codes[1][0]);
            LCD_gputscpos(0,3,buffer);
        }
        if (ecminfo.codecount[1] >= 2)
        {
            strcpy((char*)buffer,"CalID: ");
            strcat((char*)buffer,(char*)ecminfo.codes[1][1]);
            LCD_gputscpos(0,4,buffer);
        }
        if (ecminfo.second_codes[1][0][0] != NULL)
        {
            strcpy((char*)buffer,"SWPN: ");
            strcat((char*)buffer,(char*)ecminfo.second_codes[1][0]);
            LCD_gputscpos(0,5,buffer);
        }
        status = getKeyWait(SW_ENTER,KEYPAD_ALLOWABORT);
        if (status == SW_EXIT)
        {
            return;
        }
    }    
    LCD_gsetmode(GNORMAL);
    
    //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    // Display all ECM/PCM partnumbers/strategy
    //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    
    MENUITEMSTR* mitem;
    mitem = __malloc(ecminfo.codecount[0]*sizeof(MENUITEMSTR));

    ShowMessage(NOKEY,NOAUDIO,(void*)*Language->HDR_VehicleInfo,
                (void*)*Language->FDR_ContinueKeyMsg,
                (void*)*Language->TXT_NULLSTR,
                (void*)*Language->TXT_NULLSTR,
                (void*)*Language->TXT_NULLSTR,CLEAR_TOP);

    LCD_gputscpos(1,2,"ECM Part Numbers:");

    for(i=0;i<ecminfo.codecount[0];i++)
    {
        ParseMenuItemStr(ecminfo.codes[0][i], i, mitem, 0);
    }
    status = Scroller(&i, 0, mitem, ecminfo.codecount[0], 3, 0, 8, 0, 32,
                      NullHelper, NullHelper);
    __free(mitem);
    if (status == S_USERABORT)
    {
        return;
    }
    
    //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    // Display all TCM partnumbers/strategy (if available)
    //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    if (ecminfo.ecm_count > 1)
    {
        MENUITEMSTR* mitem;
        mitem = __malloc(ecminfo.codecount[0]*sizeof(MENUITEMSTR));
        
        ShowMessage(NOKEY,NOAUDIO,(void*)*Language->HDR_VehicleInfo,
                    (void*)*Language->FDR_ContinueKeyMsg,
                    (void*)*Language->TXT_NULLSTR,
                    (void*)*Language->TXT_NULLSTR,
                    (void*)*Language->TXT_NULLSTR,CLEAR_TOP);
        
        LCD_gputscpos(1,2,"TCM Part Numbers:");
        
        for(i=0;i<ecminfo.codecount[1];i++)
        {
            ParseMenuItemStr(ecminfo.codes[1][i], i, mitem, 0);
        }
        status = Scroller(&i, 0, mitem, ecminfo.codecount[1], 3, 0, 8, 0, 32,
                          NullHelper, NullHelper);
        __free(mitem);
        if (status == S_USERABORT)
        {
            return;
        }
    }//if (ecminfo.ecm_count > 1)...
    
    //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    
    if(ecminfo.vehicle_serial_number[0] != 0)
    {
        u8 strbuffer[32];

        ShowMessage(NOKEY,NOAUDIO,(void*)*Language->HDR_VehicleInfo,
                    (void*)*Language->FDR_ContinueKeyMsg,
                    (void*)*Language->TXT_NULLSTR,
                    (void*)*Language->TXT_NULLSTR,
                    (void*)*Language->TXT_NULLSTR,CLEAR_TOP);

        LCD_gselfont(&msfont);
        LCD_gsetmode(GALIGN_HCENTER);
        LCD_gputscpos(0,2,"Serial Number");
        LCD_gputscpos(0,3,ecminfo.vehicle_serial_number);

        strcpy((char*)strbuffer,"BBC:");
        strcat((char*)strbuffer,(char*)ecminfo.bbc);
        LCD_gputscpos(0,4,strbuffer);

        strcpy((char*)strbuffer,"HW:");
        strcat((char*)strbuffer,(char*)ecminfo.codes[0][2]);
        LCD_gputscpos(0,5,strbuffer);

        LCD_gsetmode(GNORMAL);
        getKeyWait(SW_ENTER,KEYPAD_ALLOWABORT);
    }
}

//------------------------------------------------------------------------------
// Program a tune to vehicle
// Input:   TuneType tunetype
// Engineer: Quyen Leba
// Date: Feb 25, 2008
//------------------------------------------------------------------------------
void obd2_x3p_function_programtunetovehicle(TuneType tunetype)
{
    vehicle_info vehicleinfo;
    CMDIF_ACTION cmdif_action;
    cmdif_datainfo *datainfo;
    MENUITEMSTR *mitem;
    u8  unsupportcode_buffer[128];
    u8  selection;
    u8  status;
    u32 i,j,k;
    u8  current_ecm_index;
    u8  current_supported_ecm_count;
    bool is_married;

    unsupportcode_buffer[0] = NULL;
    current_supported_ecm_count = 0;
    is_married = FALSE;
    
    //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    // Check tune type, if it is supported; check if download failure
    //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    switch(tunetype)
    {
    case PRELOADED_TUNE:
        cmdif_action = CMDIF_ACT_PRELOADED_FLASHER;
        break;
    case CUSTOM_TUNE:
        cmdif_action = CMDIF_ACT_CUSTOM_FLASHER;
        break;
    default:
        cmdif_action = CMDIF_ACT_INVALID;
        //TODOQ: assign some error here
        ERROR_MessageCode(DEBUG,10000);
        return;
    }

    status = cmdif_command(CMDIF_CMD_FLASHER,&cmdif_action,1);
    if (status != S_SUCCESS)
    {
        //TODOQ: assign some error here
        ERROR_MessageCode(DEBUG,10001);
        return;
    }

    cmdif_getResponseData(&datainfo);
    if (datainfo->responsecode == CMDIF_ACK_OK)
    {
        switch(datainfo->dataptr[0])
        {
        case CMDIF_ACK_STOCK_EXIST:
            is_married = TRUE;
            break;
        case CMDIF_ACK_NOT_MARRIED:
            is_married = FALSE;
            break;
        case CMDIF_ACK_DOWNLOAD_FAILED:
            //there was a download failure
            ERROR_MessageCode(DOWNLOADFAILDETECTED,10002);
            return;
        case CMDIF_ACK_STOCK_NOT_FOUND:
            //married but no stock file
            //TODOQ: assign some error here
            ERROR_MessageCode(DEBUG,10003);
            return;
        default:
            //TODOQ: assign some error here
            ERROR_MessageCode(DEBUG,10004);
            return;
        }
    }
    else if (datainfo->responsecode == CMDIF_ACK_NOT_SUPPORTED)
    {
        //TODOQ: assign some error here
        ERROR_MessageCode(DEBUG,10005);
        return;
    }
    else if (datainfo->responsecode == CMDIF_ACK_DOWNLOAD_FAILED)
    {
        //this case is not used; just put it here for now
        ERROR_MessageCode(DOWNLOADFAILDETECTED,10006);
        return;
    }
    else if (datainfo->responsecode == CMDIF_ACK_FAILED)
    {
        //TODOQ: assign some error here
        ERROR_MessageCode(DEBUG,10007);
        return;
    }

    //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    // Get information about this vehicle (VIN, commtype, tunecodes)
    //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

#ifdef BANKS_ENG
    status = ShowMessage(SW_ENTER,NOAUDIO,
                         (void*)*Language->HDR_ProgramVehicleECU,
                         (void*)*Language->FDR_ContinueMsg,
                         (void*)*Language->TXT_NULLSTR,
                         (void*)*Language->TXT_TurnKeyOn,
                         (void*)*Language->TXT_DoNotStartEngine,CLEAR_ALL);
#else
    status = ShowMessage(SW_ENTER, NOAUDIO,
                         (void*)*Language->HDR_ProgramVehicleECU,
                         (void*)*Language->FDR_ContinueKeyMsg,
                         (void*)*Language->TXT_NULLSTR,
                         (void*)*Language->TXT_TurnKeyOn,
                         (void*)*Language->TXT_NULLSTR,CLEAR_ALL);
#endif
    if (status == SW_EXIT)
    {
        return;
    }

    ShowMessage(NOKEY, NOAUDIO,
                (void*)*Language->HDR_ProgramVehicleECU,
                (void*)*Language->TXT_NULLSTR,
                (void*)*Language->TXT_NULLSTR,
                (void*)*Language->TXT_PleaseWait,
                (void*)*Language->TXT_NULLSTR,CLEAR_TOP);

    // read VIN
    status = cmdif_command(CMDIF_CMD_READ_VIN,NULL,0);
    if (status  != S_SUCCESS)
    {
        //TODOQ: assign some error here
        ERROR_MessageCode(DEBUG,10020);
        return;
    }
    else
    {
        cmdif_getResponseData(&datainfo);
        switch(datainfo->responsecode)
        {
        case CMDIF_ACK_OK:
            memcpy(vehicleinfo.vin,datainfo->dataptr,VIN_LENGTH);
            memcpy((void*)&vehicleinfo.commtype,
                   &datainfo->dataptr[VIN_LENGTH+1],sizeof(VehicleCommType));
            break;
        default:
            //TODOQ: assign some error here
            ERROR_MessageCode(DEBUG,10021);
            return;
        }//switch(datainfo->...
    }
    vehicleinfo.vin[VIN_LENGTH] = 0;

    //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    // Preloaded Tune -- read vehicle code
    //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    //IMPORTANT
    //TODOQ: if married, get tune code, strategy from stock file
    //status = Ford_GetsStrategyFromTuneFile(settings.eec_type, "stock.bin", strategy);
    
    status = cmdif_command(CMDIF_CMD_GET_STRATEGY_FLASHER,NULL,0);
    if (status != S_SUCCESS)
    {
        ERROR_MessageCode(READSTRATEGYFAIL,10030);
        return;
    }
    else
    {
        cmdif_getResponseData(&datainfo);
        switch(datainfo->responsecode)
        {
        case CMDIF_ACK_OK:
            j = k = 0;
            
            obd2_clearemcinfo(&vehicleinfo.ecm);
            //datainfo->dataptr contains: "code1*code2*" for example
            //each code is the (main/OS/strategy) ecm code
            for(i=0;i<strlen((char*)datainfo->dataptr);i++)
            {
                if (datainfo->dataptr[i] == TUNECODE_SEPARATOR)
                {
                    vehicleinfo.ecm.codes[j][0][k++] = TUNECODE_SEPARATOR;
                    vehicleinfo.ecm.codes[j][0][k++] = 0;
                    vehicleinfo.ecm.codecount[j] = 1;   //there's one code
                    k = 0;
                    j++;
                    if (j >= ECM_MAX_COUNT)
                    {
                        break;
                    }
                }
                else
                {
                    vehicleinfo.ecm.codes[j][0][k++] = datainfo->dataptr[i];
                }
            }//for(i=0;i<strlen...
            vehicleinfo.ecm.ecm_count = j;
            break;
        case CMDIF_ACK_READ_STRATEGY_FAILED:
            ERROR_MessageCode(READSTRATEGYFAIL,10031);
            return;
        case CMDIF_ACK_FAILED:
            ERROR_MessageCode(READSTRATEGYFAIL,10032);
            //TODOQ: check this too: ERROR_Message(OSPARTNUMBERFAIL);
            return;
        default:
            ERROR_MessageCode(READSTRATEGYFAIL,10033);
            //TODOQ: check this too: ERROR_Message(OSPARTNUMBERFAIL);
            return;
        }//switch(datainfo->...
    }
    
    //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    // If married, do some early checking; if it's not the same vehicle, just
    // don't go any further
    // use error numbercode 10040-50
    //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    if (is_married)
    {
        status = obd2tune_checkmarried(SETTINGS_TUNE(veh_type),NULL,NULL);
        if (status == S_NOSECURITYBYTE)
        {
            ERROR_MessageCode(DEBUG,10040);
            return;
        }
        else if (status != S_SUCCESS)
        {
            ERROR_MessageCode(DEBUG,10041);
            return;
        }
    }

    //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    // Check if tune code is blocked (not allowed to flash)
    // use error numbercode 10050-60
    //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    if (obd2tune_checkvehiclecode(&vehicleinfo.ecm,tunetype) != S_SUCCESS)
    {
        ERROR_MessageCode(DEBUG,10050);
        return;
    }
    
    //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    // Check VIN, classify vehice type, etc here
    // use error numbercode 10060-70
    //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    //TODOQ: this check, but might not be necessary anymore

    //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    // Handle tunes & options for preloaded tune flash
    // Note: the 1st ECM must be supported to continue, 
    //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    if (tunetype == PRELOADED_TUNE)
    {
        //used to track ECMs with supported tunes
        bool ecm_supported_list[ECM_MAX_COUNT];
        //used to track if at least 1 ECM is unsupported
        bool isunsupportedecmfound;
        
        isunsupportedecmfound = FALSE;
        memset(ecm_supported_list,TRUE,sizeof(ecm_supported_list));
        //###################################################
        // Go through each available ECMs
        // ask user to select tune, adjust options
        //###################################################
        for(current_ecm_index=0;
            current_ecm_index<vehicleinfo.ecm.ecm_count;current_ecm_index++)
        {
            u8 tunecode_listcount;
            u32 tunecode_bytecount;
            u32 tunecode_checksum;
            u32 calc_checksum;
            u16 id_list[MAX_TUNE_CODE_LIST_COUNT];
            u32 index;
            u8  tune_selection_data[3];
            
            //###################################################
            // step1: send tunecode to request more info about it
            //###################################################
            status = cmdif_command(CMDIF_CMD_GET_TUNE_LIST_INFO,
                                   vehicleinfo.ecm.codes[current_ecm_index][0],
                                   strlen((char*)vehicleinfo.ecm.codes[current_ecm_index][0]));
            if (status  != S_SUCCESS)
            {
                //TODOQ: assign some error here
                ERROR_MessageCode(DEBUG,10080);
                return;
            }
            else
            {
                cmdif_getResponseData(&datainfo);
                switch(datainfo->responsecode)
                {
                case CMDIF_ACK_OK:
                    tunecode_listcount = datainfo->dataptr[0];
                    tunecode_bytecount =
                        ((u32)datainfo->dataptr[1])         |
                        ((u32)datainfo->dataptr[2] << 8)    |
                        ((u32)datainfo->dataptr[3] << 16)   |
                        ((u32)datainfo->dataptr[4] << 24);
                    tunecode_checksum =
                        ((u32)datainfo->dataptr[5])         |
                        ((u32)datainfo->dataptr[6] << 8)    |
                        ((u32)datainfo->dataptr[7] << 16)   |
                        ((u32)datainfo->dataptr[8] << 24);
                    break;
                case CMDIF_ACK_VEHICLE_UNSUPPORTED:
                    isunsupportedecmfound = TRUE;
                    strcat((char*)unsupportcode_buffer,
                           (char*)vehicleinfo.ecm.codes[current_ecm_index][0]);
                    //mark this ECM has no tunes
                    ecm_supported_list[current_ecm_index] = FALSE;
                    continue;
                case CMDIF_ACK_NO_LOOKUP:
                    ERROR_MessageCode(NOSTRATEGYTUNE,10081);
                    return;
                default:
                    //TODOQ: assign some error here
                    ERROR_MessageCode(DEBUG,10083);
                    return;
                }
            }
            
            //##################################################################
            // step2: receive tune info about tunecode
            // ask user to make selection
            //##################################################################
            status = cmdif_command(CMDIF_CMD_GET_TUNE_LIST_DATA,NULL,0);
            if (status  != S_SUCCESS)
            {
                //failure from cmdif_command(CMDIF_CMD_GET_TUNE_LIST_DATA...
                //TODOQ: assign some error here
                ERROR_MessageCode(DEBUG,10090);
                return;
            }
            else
            {
                u8 optionfilename[30];
                
                cmdif_getResponseData(&datainfo);
                if (datainfo->responsecode == CMDIF_ACK_OK)
                {
                    calc_checksum = checksum_simpleadditive(datainfo->dataptr,
                                                            datainfo->datalength);
                    if ((calc_checksum != tunecode_checksum) ||
                        (tunecode_bytecount != datainfo->datalength))
                    {
                        //TODOQ: assign some error here
                        ERROR_MessageCode(DEBUG,10091);
                        return;
                    }
                    
                    index = 0;
                    mitem = __malloc(tunecode_listcount * sizeof(MENUITEMSTR));
                    
                    for(j=0;j<tunecode_listcount;j++)
                    {
                        id_list[j] = 
                            ((u16)datainfo->dataptr[index]) |
                                ((u16)datainfo->dataptr[index+1] << 8);
                        index += 2;
                        ParseMenuItemStr(&datainfo->dataptr[index], j, mitem, 0);
                        index += strlen((char*)&datainfo->dataptr[index]) + 1;
                    }
                    
                    ShowMessage(NOKEY, NOAUDIO, "Select Tune", //TODOQ: use correct title
                                (void*)*Language->FDR_ContinueKeyMsg,
                                (void*)*Language->TXT_NULLSTR,
                                (void*)*Language->TXT_NULLSTR,
                                (void*)*Language->TXT_NULLSTR, CLEAR_TOP);
                    status = Scroller(&selection,0,mitem,
                                      tunecode_listcount, 2, 0, 8, 1, 32,
                                      NullHelper,NullHelper);
                    __free(mitem);
                    if (status == S_USERABORT)
                    {
                        return;
                    }
                    
                    ShowMessage(NOKEY, NOAUDIO,
                                (void*)*Language->HDR_ProgramVehicleECU,
                                (void*)*Language->TXT_NULLSTR,
                                (void*)*Language->TXT_NULLSTR,
                                (void*)*Language->TXT_PleaseWait,
                                (void*)*Language->TXT_NULLSTR,CLEAR_TOP);
                    
                    // report user selection
                    tune_selection_data[0] = id_list[selection];
                    tune_selection_data[1] = id_list[selection] >> 8;
                    tune_selection_data[2] = current_ecm_index; //~~~!!!
                    status = cmdif_command(CMDIF_CMD_SELECT_TUNE_STRATEGY,
                                           tune_selection_data,3);
                    if (status != S_SUCCESS)
                    {
                        //TODOQ: assign some error here
                        ERROR_MessageCode(DEBUG,10092);
                        return;
                    }
                    else
                    {
                        cmdif_getResponseData(&datainfo);
                        switch(datainfo->responsecode)
                        {
                        case CMDIF_ACK_OK:
                            if (datainfo->datalength > 0)
                            {
                                bool is_option_use;
                                
                                strcpy((char*)optionfilename,
                                       (char*)datainfo->dataptr);
                                status = RunOptionsMenu(optionfilename,
                                                        current_ecm_index,
                                                        &is_option_use);
                                if (status != S_SUCCESS &&
                                    status != S_USERABORT)
                                {
                                    //TODOQ: assign some error here
                                    ERROR_MessageCode(DEBUG,0);
                                    return;
                                }
                                if (is_option_use)
                                {
                                    u8 useoptiondata[64];
                                    u16 useoptiondatalength;

                                    //useoptiondata: [1:ecm_id][n:optionfilename][1:NULL]
                                    useoptiondata[0] = current_ecm_index;
                                    useoptiondatalength = strlen((char*)optionfilename);
                                    if ((useoptiondatalength+2) < sizeof(useoptiondata))
                                    {
                                        strcpy((char*)&useoptiondata[1],
                                               (char*)optionfilename);
                                        useoptiondatalength += 2;
                                        status = cmdif_command
                                            (CMDIF_CMD_USE_OPTION,
                                             useoptiondata,useoptiondatalength);
                                        if (status == S_SUCCESS)
                                        {
                                            cmdif_getResponseData(&datainfo);
                                            switch(datainfo->responsecode)
                                            {
                                            case CMDIF_ACK_OK:
                                                //do nothing
                                                break;
                                            case CMDIF_ACK_INVALID_PCMNUMBER:
                                                //TODOQ: assign some error here
                                                ERROR_MessageCode(DEBUG,10093);
                                                return;
                                            default:
                                                //TODOQ: assign some error here
                                                ERROR_MessageCode(DEBUG,10094);
                                                return;
                                            }//switch(datainfo->...
                                        }
                                    }//if ((useoptiondatalength+2)...
                                }//if (is_option_use)...
                            }//if (datainfo->datalength > 0)...
                            break;
                        case CMDIF_ACK_INVALID_PCMNUMBER:
                            //TODOQ: assign some error here
                            ERROR_MessageCode(DEBUG,10095);
                            return;
                        case CMDIF_ACK_TUNE_NOT_FOUND:
                            ERROR_MessageCode(NOSTRATEGYTUNE,10096);
                            return;
                        case CMDIF_ACK_INVALID_BINARYTYPE:
                            ERROR_MessageCode(INVALIDBINARY,10097);
                            return;
                        default:
                            //TODOQ: assign some error here
                            ERROR_MessageCode(DEBUG,10098);
                            return;
                        }//switch(datainfo->...
                    }
                }//if (datainfo->responsecode...
            }
            
            current_supported_ecm_count++;
            
        }//for(i=0;i<vehicleinfo.ecm.ecm_count;i++)...
        
        //if 1st ECM is unsupported,  cannot continue
        if (ecm_supported_list[0] == FALSE)
        {
            ERROR_UnsupportVehicle(unsupportcode_buffer, 0);
            return;
        }
        else if (isunsupportedecmfound)
        {
            //ask user if they want to continue in this condition (at least one
            //ECM will not be programmed)
            ShowMessage(NOKEY,NOAUDIO,(void*)*Language->HDR_Warning,
                        (void*)*Language->FDR_ContinueKeyMsg,
                        //TODOQ: this msg makes sense because we only program
                        //PCM and/or TCM
                        (void*)*Language->TXT_ProgramPCMOnly,
                        (void*)*Language->TXT_NULLSTR,
                        (void*)*Language->TXT_NULLSTR, CLEAR_TOP );
            
            mitem = __malloc(2*sizeof(MENUITEMSTR));
            ParseMenuItemStr(*Language->TXT_Yes, 0, mitem, 0);
            ParseMenuItemStr(*Language->TXT_No, 1, mitem, 0);
            
            selection = 0;
            status = Scroller(&selection, 0, mitem, 2, 4, 0, 8, 0, 40,
                              NullHelper, NullHelper);
            __free(mitem);
            
            if ((selection != 0) || (status == S_USERABORT))
            {
                return;
            }
        }
        
    }//if (tunetype == PRELOADED_TUNE)...
    //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    // Handle tunes & options for custom tune flash
    //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    else if (tunetype == CUSTOM_TUNE)
    {
        tunecode_infolist tunecodeinfolist;
        MENUITEMSTR *mitem;
        u8 selection;
        u8 selectionbuffer[256];
        u32 selectionbufferlength;
        u32 tmplength;
        bool is_option_use;

        status = obd2tune_getcustomtune_infolist(&tunecodeinfolist);
        if (status != S_SUCCESS)
        {
            //TODOQ: assign some error here
            ERROR_MessageCode(DEBUG,0);
            return;
        }
        else if (tunecodeinfolist.count == 0)
        {
            //TODOQ: assign some error here
            ERROR_MessageCode(DEBUG,0);
            return;
        }

        //TODOQ: show some message here "select a custom tune" or something
        
        mitem = __malloc(tunecodeinfolist.count*sizeof(MENUITEMSTR));
        for(i=0;i<tunecodeinfolist.count;i++)
        {
            ParseMenuItemStr(tunecodeinfolist.description[i], i, mitem, 0);
        }
        status = Scroller(&selection,0,mitem,tunecodeinfolist.count,3,0,8,1,32,
                          NullHelper,NullHelper);
        __free(mitem);
        if (status == S_USERABORT)
        {
            return;
        }

        //customtune_selectionbuffer:
        //[1:reserved][1:vehicle type defined for veh_defs]
        //[n:tunefilename][1:NULL][m:tunedescription][1:NULL]
        selectionbuffer[0] = 0;          //reserved
        selectionbuffer[1] = tunecodeinfolist.veh_type[selection];
        selectionbufferlength = 2;
        selectionbufferlength += strlen((char*)tunecodeinfolist.tunefilename[selection]);
        selectionbufferlength += 1;      //NULL byte
        selectionbufferlength += strlen((char*)tunecodeinfolist.description[selection]);
        selectionbufferlength += 1;      //NULL byte
        if (selectionbufferlength >= sizeof(selectionbuffer))
        {
            //TODOQ: assign some error here
            ERROR_MessageCode(DEBUG,0);
            return;
        }
        
        strcpy((char*)&selectionbuffer[2],
               (char*)tunecodeinfolist.tunefilename[selection]);
        tmplength = 2 + 1 +
            strlen((char*)tunecodeinfolist.tunefilename[selection]);
        strcpy((char*)&selectionbuffer[tmplength],
               (char*)tunecodeinfolist.description[selection]);

        status = cmdif_command(CMDIF_CMD_SELECT_TUNE_CUSTOM,
                               selectionbuffer,selectionbufferlength);
        if (status  != S_SUCCESS)
        {
            //TODOQ: assign some error here
            ERROR_MessageCode(DEBUG,0);
            return;
        }
        else
        {
            cmdif_getResponseData(&datainfo);
            switch(datainfo->responsecode)
            {
            case CMDIF_ACK_OK:
                break;
            case CMDIF_ACK_INVALID_BINARYTYPE:
                //TODOQ: assign some error here
                ERROR_MessageCode(DEBUG,0);
                return;
            case CMDIF_ACK_MARRIED_FAIL:
                //tried to flash different tune(vehtype) to a married vehicle
                ERROR_MessageCode(DEBUG,0);
                break;
            case CMDIF_ACK_TUNE_NOT_FOUND:
                //TODOQ: assign some error here
                ERROR_MessageCode(DEBUG,0);
                return;
            case CMDIF_ACK_FAILED:
            default:
                //TODOQ: assign some error here
                ERROR_MessageCode(DEBUG,0);
                return;
            }//switch(datainfo->...
        }
        
        status = RunOptionsMenu(tunecodeinfolist.optionfilename[selection],
                                0,&is_option_use);
        if (status != S_SUCCESS)
        {
            //TODOQ: assign some error here
            ERROR_MessageCode(DEBUG,0);
            return;
        }
        
        if (is_option_use)
        {
            u8 useoptiondata[64];
            u16 useoptiondatalength;

            //useoptiondata: [1:ecm_id][n:optionfilename][1:NULL]
            useoptiondata[0] = current_ecm_index;
            useoptiondatalength = strlen((char*)tunecodeinfolist.optionfilename[selection]);
            if ((useoptiondatalength+2) < sizeof(useoptiondata))
            {
                strcpy((char*)&useoptiondata[1],
                       (char*)tunecodeinfolist.optionfilename[selection]);
                useoptiondatalength += 2;
                status = cmdif_command
                    (CMDIF_CMD_USE_OPTION,
                     useoptiondata,useoptiondatalength);
                if (status == S_SUCCESS)
                {
                    cmdif_getResponseData(&datainfo);
                    switch(datainfo->responsecode)
                    {
                    case CMDIF_ACK_OK:
                        //do nothing
                        break;
                    case CMDIF_ACK_INVALID_PCMNUMBER:
                        //TODOQ: assign some error here
                        ERROR_MessageCode(DEBUG,10093);
                        return;
                    default:
                        //TODOQ: assign some error here
                        ERROR_MessageCode(DEBUG,10094);
                        return;
                    }//switch(datainfo->...
                }
            }//if ((useoptiondatalength+2) <...
        }//if (is_option_use)...
        
    }//else if (tunetype == CUSTOM_TUNE)...
    else
    {
        // this case should never happen; tunetype was already checked at the
        // beginning
        return;
    }
    
    //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    // Confirm with user to begin program vehicle
    //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
#ifdef BANKS_ENG
    status = ShowMessage(NOKEY,NOAUDIO,(void*)*Language->HDR_TuneVehicleECU,
                         (void*)*Language->FDR_SelectMsg,
                         (void*)*Language->TXT_NULLSTR,
                         (void*)*Language->TXT_NULLSTR,
                         (void*)*Language->TXT_NULLSTR,CLEAR_TOP);
    mitem = __malloc(2*sizeof(MENUITEMSTR));
    ParseMenuItemStr((void*)*Language->TXT_BANKS_Begin_Program, 0, mitem, 0);
    ParseMenuItemStr((void*)*Language->TXT_BANKS_Cancel_Program, 1, mitem, 0);
#else
    ShowMessage(NOKEY,NOAUDIO,(void*)*Language->HDR_ProgramVehicleECU,
                (void*)*Language->FDR_ContinueKeyMsg,
                (void*)*Language->TXT_NULLSTR,
                (void*)*Language->TXT_NULLSTR,
                (void*)*Language->TXT_NULLSTR, CLEAR_TOP );
    
    mitem = __malloc(2*sizeof(MENUITEMSTR));
    ParseMenuItemStr("Begin  Program", 0, mitem, 0);
    ParseMenuItemStr("Cancel Program", 1, mitem, 0);
#endif
    selection = 0;
    status = Scroller(&selection, 0, mitem, 2, 4, 0, 8, 0, 32, NullHelper, NullHelper);
    __free(mitem);
    
    if ((selection != 0) || (status == S_USERABORT))    // Cancel Program
    {
        return;
    }

    //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    // Handle upload stock
    // Use error numbercode 10200->10399
    //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
#ifdef BANKS_ENG
    ShowMessage(NOKEY,NOAUDIO,(void*)*Language->HDR_TuneVehicleECU,
#else
    ShowMessage(NOKEY,NOAUDIO,(void*)*Language->HDR_ProgramVehicleECU,
#endif
                (void*)*Language->TXT_NULLSTR,
                (void*)*Language->TXT_NULLSTR,
                (void*)*Language->TXT_PleaseWait,
                (void*)*Language->TXT_NULLSTR,CLEAR_TOP);
    bool is_skip_upload = FALSE;

    status = cmdif_command(CMDIF_CMD_INIT_UPLOAD,NULL,0);
    if (status  != S_SUCCESS)
    {
        ERROR_MessageCode(UPLOADSTOCKFAIL,10200);
        goto obd2_x3p_function_programtunetovehicle_done;
    }
    else
    {
        cmdif_getResponseData(&datainfo);
        switch(datainfo->responsecode)
        {
        case CMDIF_ACK_INCORRECT_VEHTYPE:
            ERROR_MessageCode(INVALIDBINARYMARRIED,10209);
            goto obd2_x3p_function_programtunetovehicle_done;
        case CMDIF_ACK_NOT_MARRIED:
            // device is NOT married to a vehicle
            is_skip_upload = FALSE;
            break;
        case CMDIF_ACK_STOCK_EXIST:
            // device is MARRIED to a vehicle
            is_skip_upload = TRUE;
            break;
        case CMDIF_ACK_STOCK_NOT_FOUND:
            // error: device is MARRIED but stock file MISSING
            ERROR_MessageCode(NOSTOCK,10201);
            goto obd2_x3p_function_programtunetovehicle_done;
        case CMDIF_ACK_STOCK_CORRUPTED:
            // error: device is MARRIED but stock file CORRUPTED
            ERROR_MessageCode(STOCKFILECORRUPT,10202);
            goto obd2_x3p_function_programtunetovehicle_done;
        default:
            // any other errors
            ERROR_MessageCode(UPLOADSTOCKFAIL,10203);
            goto obd2_x3p_function_programtunetovehicle_done;
        }
    }

    if (is_skip_upload == FALSE)
    {
        //special case: all responses are handled internally
        //status reflects actual error (if any)
        //no need to use cmdif_getResponseData here
        status = cmdif_command(CMDIF_CMD_DO_UPLOAD,NULL,0);
        if (status  != S_SUCCESS)
        {
            goto obd2_x3p_function_programtunetovehicle_done;
        }
    }

    ShowMessage(NOKEY,NOAUDIO,(void*)*Language->HDR_UploadECU,
                (void*)*Language->TXT_PleaseWait,
                (void*)*Language->TXT_NULLSTR,
                (void*)*Language->TXT_UploadSuccessful,
                (void*)*Language->TXT_NULLSTR, CLEAR_TOP);
//@@@
//return;
    //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    // Handle download tune
    // Use error numbercode 10400->10599
    //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    status = cmdif_command(CMDIF_CMD_INIT_DOWNLOAD,NULL,0);
    if (status  != S_SUCCESS)
    {
        //TODOQ: assign some error here
        ERROR_MessageCode(DEBUG,10400);
        goto obd2_x3p_function_programtunetovehicle_done;
    }
    else
    {
        cmdif_getResponseData(&datainfo);
        switch(datainfo->responsecode)
        {
        case CMDIF_ACK_OK:
            //do nothing
            break;
        case CMDIF_ACK_FILECOPY_FAIL:
            //TODOQ: assign some error here
            ERROR_MessageCode(DEBUG,10401);
            goto obd2_x3p_function_programtunetovehicle_done;
        case CMDIF_ACK_STRAGEY_CHANGES_FAIL:
            //TODOQ: assign some error here
            ERROR_MessageCode(DEBUG,10402);
            goto obd2_x3p_function_programtunetovehicle_done;
        case CMDIF_ACK_FILESIZE_MISMATCH:
            ERROR_MessageCode(DEBUG,10404);
            goto obd2_x3p_function_programtunetovehicle_done;
        case CMDIF_ACK_MARRIED_FAIL:
            ERROR_MessageCode(DEBUG,10460);
            goto obd2_x3p_function_programtunetovehicle_done;
        case CMDIF_ACK_FAILED:
        default:
            //TODOQ: assign some error here
            ERROR_MessageCode(DEBUG,10403);
            goto obd2_x3p_function_programtunetovehicle_done;
        }//switch(datainfo->...
    }
    
    //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    //
    //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    status = cmdif_command(CMDIF_CMD_DO_DOWNLOAD,NULL,0);
    if (status != S_SUCCESS)
    {
        goto obd2_x3p_function_programtunetovehicle_done;
    }
    
    //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    // Download complete
    //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    if (status == S_SUCCESS)
    {
        ShowMessage(SW_ENTER,PROGRAMDONE,(void*)*Language->HDR_DownloadTune,
                    (void*)*Language->FDR_ContinueKeyMsg,
                    (void*)*Language->TXT_NULLSTR,
                    (void*)*Language->TXT_DownloadComplete,
                    (void*)*Language->TXT_NULLSTR, CLEAR_TOP );
        //TODOQ: do some key off power down here
    }
    
    
obd2_x3p_function_programtunetovehicle_done:
    indicator_clear();
    power_setvpp(Vpp_OFF);
    return;
}

//------------------------------------------------------------------------------
// Return Vehicle To Stock Tune
// Engineer: Quyen Leba
//------------------------------------------------------------------------------
void obd2_x3p_function_returnvehicletostock()
{
    CMDIF_ACTION cmdif_action;
    cmdif_datainfo *datainfo;
    u8 status;
    
    cmdif_action = CMDIF_ACT_STOCK_FLASHER;
    status = cmdif_command(CMDIF_CMD_FLASHER,&cmdif_action,1);
    if (status != S_SUCCESS)
    {
        //TODOQ: assign some error here
        ERROR_MessageCode(DEBUG,10001);
        return;
    }

#ifdef BANKS_ENG
    status = ShowMessage(SW_ENTER,NOAUDIO,
                         (void*)*Language->HDR_ReturnVehicleToStock,
                         (void*)*Language->FDR_ContinueMsg,
                         (void*)*Language->TXT_NULLSTR,
                         (void*)*Language->TXT_TurnKeyOn,
                         (void*)*Language->TXT_DoNotStartEngine,CLEAR_ALL);
#else
    status = ShowMessage(SW_ENTER, NOAUDIO,
                         (void*)*Language->HDR_ReturnVehicleToStock,
                         (void*)*Language->FDR_ContinueKeyMsg,
                         (void*)*Language->TXT_NULLSTR,
                         (void*)*Language->TXT_TurnKeyOn,
                         (void*)*Language->TXT_NULLSTR,CLEAR_ALL);
#endif
    if (status == SW_EXIT)
    {
        return;
    }
    ShowMessage(NOKEY,NOAUDIO,
                (void*)*Language->HDR_ReturnVehicleToStock,
                (void*)*Language->TXT_NULLSTR,
                (void*)*Language->TXT_NULLSTR,
                (void*)*Language->TXT_PleaseWait,
                (void*)*Language->TXT_NULLSTR,CLEAR_TOP);
    
    cmdif_getResponseData(&datainfo);
    if (datainfo->responsecode == CMDIF_ACK_OK)
    {
        switch(datainfo->dataptr[0])
        {
        case CMDIF_ACK_STOCK_EXIST:
            //do nothing
            break;
        case CMDIF_ACK_NOT_MARRIED:
            ShowMessage(SW_ENTER,NOAUDIO,(void*)*Language->HDR_ReturnVehicleToStock,
                        (void*)*Language->FDR_ContinueKeyMsg,
                        (void*)*Language->TXT_NULLSTR,
                        (void*)*Language->TXT_AlreadyStock,
                        (void*)*Language->TXT_NULLSTR,CLEAR_ALL);
            goto obd2_x3p_function_returnvehicletostock_done;
        case CMDIF_ACK_STOCK_NOT_FOUND:
            //married but no stock file
            //TODOQ: assign some error here
            ERROR_MessageCode(DEBUG,10003);
            goto obd2_x3p_function_returnvehicletostock_done;
        default:
            //TODOQ: assign some error here
            ERROR_MessageCode(DEBUG,10004);
            goto obd2_x3p_function_returnvehicletostock_done;
        }
    }
    else if (datainfo->responsecode == CMDIF_ACK_STOCK_EXIST)
    {
        //do nothing
    }
    else if (datainfo->responsecode == CMDIF_ACK_NOT_MARRIED)
    {
        ShowMessage(SW_ENTER,NOAUDIO,(void*)*Language->HDR_ReturnVehicleToStock,
                    (void*)*Language->FDR_ContinueKeyMsg,
                    (void*)*Language->TXT_NULLSTR,
                    (void*)*Language->TXT_AlreadyStock,
                    (void*)*Language->TXT_NULLSTR,CLEAR_ALL);
        goto obd2_x3p_function_returnvehicletostock_done;
    }
    else if (datainfo->responsecode == CMDIF_ACK_STOCK_NOT_FOUND)
    {
        //married but no stock file
        //TODOQ: assign some error here
        ERROR_MessageCode(DEBUG,10003);
        goto obd2_x3p_function_returnvehicletostock_done;
    }
    else if (datainfo->responsecode == CMDIF_ACK_FAILED)
    {
        //TODOQ: assign some error here
        ERROR_MessageCode(DEBUG,10007);
        goto obd2_x3p_function_returnvehicletostock_done;
    }
    
    //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    // Handle download stock
    // Use error numbercode 10400->10599
    //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    status = cmdif_command(CMDIF_CMD_INIT_RETURN_STOCK,"stock.bin",10); //TODOQ: use "recvstock.bin" (encrypted)
    if (status  != S_SUCCESS)
    {
        //TODOQ: assign some error here
        ERROR_MessageCode(DEBUG,10400);
        goto obd2_x3p_function_returnvehicletostock_done;
    }
    else
    {
        cmdif_getResponseData(&datainfo);
        switch(datainfo->responsecode)
        {
            //TODOQ: clear out some ACK, not all used by return to stock
        case CMDIF_ACK_OK:
            //do nothing
            break;
        case CMDIF_ACK_FILECOPY_FAIL:
            //TODOQ: assign some error here
            ERROR_MessageCode(DEBUG,10401);
            goto obd2_x3p_function_returnvehicletostock_done;
        case CMDIF_ACK_STRAGEY_CHANGES_FAIL:
            //TODOQ: assign some error here
            ERROR_MessageCode(DEBUG,10402);
            goto obd2_x3p_function_returnvehicletostock_done;
        case CMDIF_ACK_FILESIZE_MISMATCH:
            ERROR_MessageCode(DEBUG,10404);
            goto obd2_x3p_function_returnvehicletostock_done;
        case CMDIF_ACK_MARRIED_FAIL:
            ERROR_MessageCode(DEBUG,10460);
            goto obd2_x3p_function_returnvehicletostock_done;
        case CMDIF_ACK_FAILED:
        default:
            //TODOQ: assign some error here
            ERROR_MessageCode(DEBUG,10403);
            goto obd2_x3p_function_returnvehicletostock_done;
        }//switch(datainfo->...
    }
    
    //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    //
    //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    status = cmdif_command(CMDIF_CMD_DO_RETURN_STOCK,NULL,0);
    if (status != S_SUCCESS)
    {
        goto obd2_x3p_function_returnvehicletostock_done;
    }
    
    //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    // Download complete
    //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    if (status == S_SUCCESS)
    {
        ShowMessage(SW_ENTER,PROGRAMDONE,(void*)*Language->HDR_DownloadTune,
                    (void*)*Language->FDR_ContinueKeyMsg,
                    (void*)*Language->TXT_NULLSTR,
                    (void*)*Language->TXT_DownloadComplete,
                    (void*)*Language->TXT_NULLSTR, CLEAR_TOP );
        //TODOQ: do some key off power down here
    }

obd2_x3p_function_returnvehicletostock_done:
    indicator_clear();
    power_setvpp(Vpp_OFF);
    return;
}

//------------------------------------------------------------------------------
// Input:   u8  *extension (the file ext)
// Output:  MENUITEMSTR *items (list of files with the ext,
//                              must be able to store 20 items)
//          u8  *count (# of files found)
// Return:  u8  status
//------------------------------------------------------------------------------
u8 GetFileNamesByExtension(u8 *extension, MENUITEMSTR *items, u8 *count)
{
    u8  *test;
    u32 nameCount;
    u32 nameLength;
    u8  *file;
    u32 response;
    F_FIND findinfo;

    nameCount = 0;
    response = f_findfirst("/"DEFAULT_DATALOG_FOLDER_NAME"/*.*",&findinfo);
    if (response != 0)
    {
        *count = 0;
        return S_FAIL;
    }

    file = (u8*)__malloc(264);
    if (file == NULL)
    {
        ERROR_Message(MALLOC);
        return S_FAIL;
    }
    memset((void*)file, 0, 264);

    while (1)
    {
        response = f_findnext(&findinfo);
        if (response == 0)
        {
            memcpy((void*)file,findinfo.filename,256);
        }
        else
        {
            break;
        }
        
        ParseMenuItemStr(file,nameCount,items,0);
        
        strcpy((char*)&items[nameCount].content[0], (char*)file);
        nameLength = strlen((char*)&items[nameCount].content[0]);
        test = &items[nameCount].content[nameLength - 3];

        if (strcmp((char*)test, (char*)extension) == 0 && nameCount < 50)
        {
            nameCount++;
        }
    }

    __free(file);
    *count = nameCount;
    return S_SUCCESS;
}

//------------------------------------------------------------------------------
// Show a list of files by extension & process the selected file
// Max list is 20 files
// Input:   u8  *ext (the file extension)
// Output:  u8  *filename (must be able to store MAX_FILENAME_LENGTH+1 bytes)
// Return:  u8  status
// Engineer: Quyen Leba
//------------------------------------------------------------------------------
u8 SelectFileByExtension(u8 *ext, u8 *filename)
{
    u8          selection   = 0;
    u8          status;
    u8          totalfilefound;
    MENUITEMSTR *mitem;
    u8          selectedfilename[MAX_FILENAME_LENGTH+1];

    mitem = __malloc(20*sizeof(MENUITEMSTR));
    if (mitem == NULL)
    {
        return S_FAIL;
    }
#ifdef BANKS_ENG
    //BANKS do nothing here !?!
#else
    ShowMessage(NOKEY,NOAUDIO,(void*)*Language->HDR_SelectFile,
               (void*)*Language->TXT_NULLSTR,
               (void*)*Language->TXT_NULLSTR,
               (void*)*Language->TXT_SearchFiles,
               (void*)*Language->TXT_PleaseWait,CLEAR_TOP);
#endif
    status = GetFileNamesByExtension(ext, mitem, &totalfilefound);
    if (totalfilefound == 0 || status != S_SUCCESS)
    {
        LCD_gselfont(&msfont);
#ifdef BANKS_ENG
        LCD_gputscpos(5,3,"Contact:\n  www.bankspower.com\n   1-800-GET-POWER");

        ShowMessage(SW_ENTER,NOAUDIO,(void*)*Language->HDR_Warning,
                (void*)*Language->FDR_ContinueMsg,
                (void*)*Language->TXT_DLF_Files,
                (void*)*Language->TXT_NULLSTR,
                (void*)*Language->TXT_NULLSTR,CLEAR_NONE);        
#else

        ShowMessage(SW_ENTER,NOAUDIO,(void*)*Language->HDR_Warning,
                (void*)*Language->FDR_ContinueKeyMsg,
                (void*)*Language->TXT_NULLSTR,
                (void*)*Language->TXT_DLF_Files,
                (void*)*Language->TXT_Load_DLF_Files,CLEAR_TOP);        

#endif

        __free(mitem);
        return S_FAIL;
    }

    ShowMessage(NOKEY,NOAUDIO,(void*)*Language->HDR_SelectFile,
#ifdef BANKS_ENG
                (void*)*Language->FDR_ContinueMsg,
#else
                (void*)*Language->FDR_ContinueKeyMsg,
#endif
                (void*)*Language->TXT_NULLSTR,
                (void*)*Language->TXT_NULLSTR,
                (void*)*Language->TXT_NULLSTR,CLEAR_TOP);
    selection = 0;
    status = Scroller(&selection,0,mitem,totalfilefound,
                      3,0,8,1,24,NullHelper,NullHelper);
    if (strlen((char*)mitem[selection].content) <= MAX_FILENAME_LENGTH)
    {
        strcpy((char*)selectedfilename,(char*)mitem[selection].content);
    }
    else
    {
        selectedfilename[0] = NULL;
    }
    __free(mitem);

    if (status == S_USERABORT)
    {
        return S_USERABORT;
    }
    
    //it's a bit clumsy here
    if (selectedfilename[0] == NULL)
    {
        return S_FAIL;
    }
    strcpy((char*)filename,(char*)selectedfilename);

    return S_SUCCESS;
}

//------------------------------------------------------------------------------
// Setup Datalog
// Input:   bool isrequeststartdatalog (TRUE: ask to start datalog after setup)
// Return:  u8  status
// Engineer: Quyen Leba
//------------------------------------------------------------------------------
u8 obd2_x3p_function_datalog_setup(bool isrequeststartdatalog)
{
    MENUITEMSTR* mitem;
    cmdif_datainfo *datainfo;
    u8  datalogfilename[MAX_FILENAME_LENGTH+1];
    u8  featurefilename[MAX_FILENAME_LENGTH+1];
    u8  validpidlist[MAX_DATALOG_DLXBLOCK_COUNT];
    u16 validpidcount;
    u8  selection;
    u8  status;
    
#ifndef BANKS_ENG
    ShowMessage(NOKEY,NOAUDIO,(void*)*Language->HDR_SetupMonitor,
                (void*)*Language->FDR_ContinueKeyMsg,
                (void*)*Language->TXT_NULLSTR,
                (void*)*Language->TXT_NULLSTR,
                (void*)*Language->TXT_NULLSTR,CLEAR_TOP);

    LCD_gputsrpos(0,32,*Language->TXT_ConfigAnalog);
    mitem = __malloc(2*sizeof(MENUITEMSTR));
    ParseMenuItemStr(*Language->TXT_No, 0, mitem, 0);
    ParseMenuItemStr(*Language->TXT_Yes, 1, mitem, 0);
    status = Scroller(&selection, 0, mitem, 2, 2, 0, 8, 0, 40,
                      NullHelper, NullHelper);
    __free(mitem);
    if (status == S_USERABORT)
    {
        return S_USERABORT;
    }
    
    if (selection == 1)
    {
        HandleExternalInputsMenu();
    }
#endif
    
    ShowMessage(NOKEY,NOAUDIO,(void*)*Language->HDR_SetupMonitor,
                (void*)*Language->TXT_NULLSTR,
                (void*)*Language->TXT_NULLSTR,
                (void*)*Language->TXT_PleaseWait,
                (void*)*Language->TXT_NULLSTR,CLEAR_TOP);

    status = SelectFileByExtension(DEFAULT_DATALOG_FILE_EXTENSION,
                                   datalogfilename);
    if (status == S_USERABORT)
    {
        return S_USERABORT;
    }
    else if (status != S_SUCCESS)
    {
        //TODOQ: assign some error here
        ERROR_MessageCode(DEBUG,20001);
        return S_FAIL;
    }
    
    status = ShowMessage(SW_ENTER,NOAUDIO,(void*)*Language->HDR_SetupMonitor,
                         (void*)*Language->FDR_ContinueKeyMsg,
                         (void*)*Language->TXT_NULLSTR,
                         (void*)*Language->TXT_TurnKeyOn,
                         (void*)*Language->TXT_NULLSTR,CLEAR_TOP);
    if (status == SW_EXIT)
    {
        return S_USERABORT;
    }

    //##########################################################################
    // Process selected datalog file; validate pids, etc
    //##########################################################################
    ShowMessage(NOKEY,NOAUDIO,(void*)*Language->HDR_SetupMonitor,
                (void*)*Language->TXT_NULLSTR,
                (void*)*Language->TXT_NULLSTR,
                (void*)*Language->TXT_PleaseWait,
                (void*)*Language->TXT_NULLSTR,CLEAR_TOP);
    
    status = cmdif_command(CMDIF_CMD_DATALOG_SCAN,
                           datalogfilename,strlen((char*)datalogfilename+1));
    if (status != S_SUCCESS)
    {
        //TODOQ: assign some error here
        ERROR_MessageCode(DEBUG,20002);
        return S_FAIL;
    }
    cmdif_getResponseData(&datainfo);
    switch(datainfo->responsecode)
    {
    case CMDIF_ACK_OK:
        if (datainfo->responsetype != RawSPP)
        {
            //TODOQ: assign some error here
            ERROR_MessageCode(DEBUG,20003);
            return S_FAIL;
        }
        else if (datainfo->datalength > 0)
        {
            if (dataloginfo)
            {
                //TODOQ: validpidlist may become u16[], change this
                if (dataloginfo->validpidcount != datainfo->datalength)
                {
                    //TODOQ: assign some error here
                    ERROR_MessageCode(DEBUG,20004);
                    return S_FAIL;
                }
                else
                {
                    //TODOQ: validpidlist may become u16[], change this
                    memcpy((char*)validpidlist,
                           (char*)datainfo->dataptr,datainfo->datalength);
                    validpidcount = dataloginfo->validpidcount;
                }
            }
            else
            {
                //TODOQ: assign some error here
                ERROR_MessageCode(DEBUG,20005);
                return S_FAIL;
            }
        }
        break;
    default:
        //TODOQ: assign some error here
        ERROR_MessageCode(DEBUG,20006);
        return S_FAIL;
    }//switch(datainfo->...
    
    //##########################################################################
    // Get datalog features (analog inputs, etc)
    //##########################################################################
    status = cmdif_command(CMDIF_CMD_DATALOG_FEATURES,NULL,0);
    if (status != S_SUCCESS)
    {
        //TODOQ: assign some error here
        ERROR_MessageCode(DEBUG,20007);
        return S_FAIL;
    }
    cmdif_getResponseData(&datainfo);
    switch(datainfo->responsecode)
    {
    case CMDIF_ACK_OK:
        if (datainfo->datalength > sizeof(featurefilename))
        {
            //TODOQ: assign some error here
            ERROR_MessageCode(DEBUG,20007);
            return S_FAIL;
        }
        else
        {
            strcpy((char*)featurefilename,(char*)datainfo->dataptr);
        }
        break;
    default:
        //TODOQ: assign some error here
        ERROR_MessageCode(DEBUG,20008);
        return S_FAIL;
    }
    
    //##########################################################################
    // Generate valid pid list file
    //##########################################################################
    status = obd2datalog_generatevalidpidfile(datalogfilename,featurefilename,
                                              DATALOG_VALID_PID_FILENAME,
                                              validpidlist,validpidcount);
    
    if (isrequeststartdatalog)
    {
        ShowMessage(NOKEY,NOAUDIO,(void*)*Language->HDR_SetupMonitor,
                    (void*)*Language->FDR_ContinueKeyMsg,
                    (void*)*Language->TXT_NULLSTR,
                    (void*)*Language->TXT_NULLSTR,
                    (void*)*Language->TXT_NULLSTR,CLEAR_TOP);
        
        LCD_gputsrpos(0,32,*Language->TXT_StartMonitor);
        mitem = __malloc(2*sizeof(MENUITEMSTR));
        ParseMenuItemStr(*Language->TXT_Yes, 0, mitem, 0);
        ParseMenuItemStr(*Language->TXT_No, 1, mitem, 0);
        status = Scroller(&selection, 0, mitem, 2, 2, 0, 8, 0, 40,
                          NullHelper, NullHelper);
        __free(mitem);
        if (status == S_USERABORT)
        {
            return S_USERABORT;
        }
        if (selection == 0)
        {
            //TODOQ: start datalog here
        }
    }//if (isrequeststartdatalog)...
    
    return S_SUCCESS;
}

//------------------------------------------------------------------------------
// Start Datalog
// Input:   bool isrequestsetupdatalog (TRUE: ask to setup datalog before start)
// Engineer: Quyen Leba
//------------------------------------------------------------------------------
void obd2_x3p_function_datalog_start(bool isrequestsetupdatalog)
{
    cmdif_datainfo *datainfo;
    u8  datalogstartinfo[2+MAX_DATALOG_FILENAME_LENGTH+1];
    u8  status;
    
    if (!file_isexist(DATALOG_VALID_PID_FILENAME))
    {
        status = obd2_x3p_function_datalog_setup(FALSE);
        if (status == S_USERABORT)
        {
            //TODOQ: maybe some message here
            return;
        }
        else if (status != S_SUCCESS)
        {
            return;
        }
    }
    
    status = ShowMessage(NOKEY,NOAUDIO,(void*)*Language->HDR_Monitor,
                         (void*)*Language->TXT_NULLSTR,
                         (void*)*Language->TXT_NULLSTR,
                         (void*)*Language->TXT_PleaseWait,
                         (void*)*Language->TXT_NULLSTR,CLEAR_TOP);
    
    datalogstartinfo[0] = 0;    //attribute     //TODOQ: more later
    datalogstartinfo[1] = 0;    //speed         //TODOQ: more later
    strcpy((char*)&datalogstartinfo[2],DATALOG_VALID_PID_FILENAME);
    
    status = cmdif_command(CMDIF_CMD_DATALOG_START,datalogstartinfo,
                           2+strlen(DATALOG_VALID_PID_FILENAME)+1);
    if (status != S_SUCCESS)
    {
        //TODOQ: assign some error here
        ERROR_MessageCode(DEBUG,21000);
        return;
    }
    cmdif_getResponseData(&datainfo);
    switch(datainfo->responsecode)
    {
    default:
        break;
    }
    
}