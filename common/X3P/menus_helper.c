/**
  **************** (C) COPYRIGHT 2011 SCT Performance, LLC *********************
  * File Name          : menus_helper.c
  * Device / System    : SCT products using the "CommonCode" platform
  * Author             : Quyen Leba
  *
  * Version            : 1 
  * Date               : 05/02/2011
  * Description        : 
  *                    : 
  *
  *
  * History            : 05/02/2011 M. Davis
  *                    :   Add SCT Copyright header,
  *
  ******************************************************************************
  */

#include <board/X3P/system.h>
#include <board/delays.h>
#include <common/statuscode.h>
#include <common/settings.h>
#include "menus_helper.h"

#define     LCDX1				0
#define     LCDY1				0
#define     LCDX2				127
#define     LCDY2				63
#define     LCDwidth			128
#define     LCDheight			64

#define     ProgressBarHeight	15
#define     ProgressBarWidth	240
#define     ProgressBarPosY		55

const u8    SCROLLBLOCK[2]      = {0x12,0};  //{'*',0};//0x7B

u8          nav_direction;                          //nav_direction of LiveWire
u8          nav_changed;                            //nav_changed of LiveWire

//---------------------------------------------------------------------------------------------------------------------------
// Private function prototype
//---------------------------------------------------------------------------------------------------------------------------
u8      NullHelperMenu(int v, u8 k);

//---------------------------------------------------------------------------------------------------------------------------
// Inputs:	u8 isadjustabletype (adjust some value or a push wheel to run)
//			3 function pointers to handle selections
// Engineer: Quyen Leba
//---------------------------------------------------------------------------------------------------------------------------
u8 HandleSubMenuHelper(u8 isadjustabletype, u8 totalitem, funcptr f1, funcptr f2, funcptr f3)
{
    static u8   sel                 = 0;
    static u8   prevsel             = 0;
    u8          ispreviteminverted  = 0;
    u8          isitemselected      = 0;
    u8          key;
    u8          maxsel              = totalitem-1;
    u8          esc_status          = TRUE;

    if (maxsel < sel)
    {
        sel = 0;
        prevsel = 0;
    }

    f1(0xFF,0);
    f2(0xFF,0);
    f3(0xFF,0);

    LCD_ginvertvp(40,16+(sel*38),50,51+(sel*38));
    LCD_ginvertvp(230,16+(sel*38),240,51+(sel*38));
    LCD_ginvertvp(40,15+(sel*38),240,16+(sel*38));
    LCD_ginvertvp(40,51+(sel*38),240,52+(sel*38));


    while(1)
    {
        key = getKey();
        if (key == SW_EXIT)
        {
            if (isadjustabletype)
            {
                SETTINGS_SetGeneralAreaDirty();
                settings_update(FALSE);
            }
            break;
        }

        if (key == SW_ENTER)
        {
            if (isadjustabletype)
            {
                isitemselected = ~isitemselected;
                LCD_ginvertvp(0,15+(sel*38),36,52+(sel*38));
            }
            else
            {
                nav_direction = getKey();
                if (nav_direction == SW_BACK || nav_direction == SW_NEXT)
                {
                    nav_changed = TRUE;
                }

                switch(sel)
                {
                  case 0:
					f1(nav_direction,isitemselected);
					break;
                  case 1:
					f2(nav_direction,isitemselected);
					break;
                  case 2:
					f3(nav_direction,isitemselected);
					break;
				}
				esc_status = 0xFF;
				break;
            }
        }
		
        if(nav_changed == TRUE)
        {
            nav_changed = 0;
            if (!isitemselected)
            {
				prevsel = sel;
				ispreviteminverted = 1;
				if (nav_direction == SW_BACK)   // left
				{
					if (sel == 0) sel = maxsel;
					else sel--;
				}
				else if (nav_direction == SW_NEXT)
				{
					if (sel == maxsel) sel = 0;
					else sel++;
				}
			
				LCD_ginvertvp(40,16+(sel*38),50,51+(sel*38));
				LCD_ginvertvp(230,16+(sel*38),240,51+(sel*38));
				LCD_ginvertvp(40,15+(sel*38),240,16+(sel*38));
				LCD_ginvertvp(40,51+(sel*38),240,52+(sel*38));
				
				if (sel != prevsel && ispreviteminverted)
				{
					LCD_ginvertvp(40,16+(prevsel*38),50,51+(prevsel*38));
					LCD_ginvertvp(230,16+(prevsel*38),240,51+(prevsel*38));
					LCD_ginvertvp(40,15+(prevsel*38),240,16+(prevsel*38));
					LCD_ginvertvp(40,51+(prevsel*38),240,52+(prevsel*38));
					ispreviteminverted = 0;
				}
			}
			else if (isadjustabletype)
			{
				switch(sel)
				{
				case 0:
					f1(nav_direction,isitemselected);
					break;
				case 1:
					f2(nav_direction,isitemselected);
					break;
				case 2:
					f3(nav_direction,isitemselected);
					break;
				}
			}
		}
	}
	return esc_status;
}

//---------------------------------------------------------------------------------------------------------------------------
// Inputs:  u8 key
//		0 (no key required, screen remain @ return), 100 (no key required but will delay 2s then clear screen)
// tone: AUDIOCODE
// return: what SW is pressed (KEYSW1,2,3 SW_ENTER)
// Engineer: Quyen Leba
//---------------------------------------------------------------------------------------------------------------------------
u8 ShowMessage(       u8  key, 
               AudioCode  tone, 
                      u8* header, 
                      u8* footer, 
                      u8* line1, 
                      u8* line2, 
                      u8* line3, 
                      u8  clearcmd)
{
	u8 _key = NOKEY;
	u8 start_line = 0;
    
	if (clearcmd & 0x1) LCD_gclrvp();
	
	LCD_gsetpos(LCDX1, LCDY1+10);
    LCD_glineto(LCDX2, LCDY1+10);
	LCD_gsetpos(LCDX1, LCDY1+12);
    LCD_glineto(LCDX2, LCDY1+12);

    LCD_gselfont(&bold2);
    LCD_gsetmode(GALIGN_HCENTER);
	LCD_gsetcpos(0,0); LCD_gputs((void*)header);
	LCD_gsetcpos(0,7); LCD_gputs((void*)footer);

    start_line = 3;
	LCD_gselfont(&msfont);
	LCD_gsetcpos(0,2); 
    LCD_gputs((void*)line1);
	LCD_gsetcpos(0,start_line); LCD_gputs((void*)line2);
	LCD_gsetcpos(0,4); 
    LCD_gputs((void*)line3);
    LCD_gsetmode(GNORMAL);

	Buzzer(tone);
	
	while(_key == NOKEY)
	{
		_key = getKey();
		if (key == NOKEY) break;
		else if (key == NOKEYREQUIRE)
		{
			delays(2000,'m');
			gclrvp();
			break;
		}
		else if (_key == SW_EXIT)
		{
			gclrvp();
			break;
		}
		else if (_key == key)
		{
			gclrvp();
			break;
		}
		else _key = NOKEY;
	}
	
	if (clearcmd & 0x02) gclrvp();

    LCD_gsetmode(GNORMAL);
    
	return _key;
}


//---------------------------------------------------------------------------------------------------------------------------
// Parse an item to menu
// Engineer: Quyen Leba
//---------------------------------------------------------------------------------------------------------------------------
void ParseMenuItemStr(u8* item, u8 index, MENUITEMSTR* menu, u8 ischecked)
{
    u8  tmpbuf[128];

    strcpy((char*)tmpbuf,(char*)item);      //to avoid memory overlap
    tmpbuf[17] = 0;

	strcpy((char*)&menu[index].content[0],(char*)tmpbuf);
	if (ischecked)
	{
		menu[index].checked[0] = CHECKMARK;
		menu[index].checked[1] = 0;
	}
	else
    {
        strcpy((void*)menu[index].checked," ");
    }
    menu[index].content[17] = 0;

}

//---------------------------------------------------------------------------------------------------------------------------
//
// Engineer: Quyen Leba
//---------------------------------------------------------------------------------------------------------------------------
void RefreshListbox(MENUITEMSTR* content, u8 totalitem, u8 cursor, u8 prev_cursor, u8 requireclr, u8 top, u8 bottom, u8 lineheight, u8 X, u8 Y)
{
	u8 i;
	u8 linewidth;
	u8 highlightmaxX;
	int scrollbarpos;
    static u8 UseShortBox = FALSE;
                                                                                           
    if( prev_cursor == ( totalitem - 1 ))
    {
        UseShortBox = TRUE;
    }                                   
    else
    {
        UseShortBox = FALSE;
    }

    linewidth = 115;
    requireclr = 1;

    LCD_gsetmode(GNORMAL);
	LCD_gselfont(&msfont);
	if (requireclr)
	{
		//gclrvp();
		//gclearareafast(X,Y/8-1,X+linewidth,Y/8+(bottom-top));

        LCD_gselfont(&msfont);
		LCD_gclearareafast(0,Y/8-1,128/6,Y/8+(bottom-top));
        LCD_gselfont(&msfont);

		for(i=top;i<=bottom;i++)
		{
			gsetpos(X,Y+(i-top)*lineheight);
            //must be LCD_gselfont(&msfont);
            LCD_gputs((void*)content[i].checked); LCD_gputs(" ");
			LCD_gputs((void*)content[i].content);
			
			if (cursor == i)
			{
				highlightmaxX = X+linewidth;
                if (highlightmaxX > 116) 
                    highlightmaxX = 116;
                
                if( UseShortBox == TRUE )
                {
                    LCD_ginvertvp(X,Y+(i-top-1)*lineheight,highlightmaxX,Y+(i-top)*lineheight); 
                }
                else
                {
                    LCD_ginvertvp(X,Y+(i-top-1)*lineheight,highlightmaxX,Y+(i-top)*lineheight-1);
                }
			}
		}
	}
	else
	{
		highlightmaxX = X+linewidth;
        if (highlightmaxX > 116) 
            highlightmaxX = 116;

        LCD_ginvertvp(X,Y+(prev_cursor-top-1)*lineheight,highlightmaxX,Y+(prev_cursor-top)*lineheight-1);
        LCD_ginvertvp(X,Y+(cursor-top-1)*lineheight,highlightmaxX,Y+(cursor-top)*lineheight-1);
		
	}

    LCD_gselfont(&info2);
    LCD_gclearareafast(15,(Y-lineheight)/8,16,(Y+(bottom-top)*lineheight)/8);
    LCD_gselfont(&msfont);
	
	scrollbarpos = Y+ (int)(((float)cursor/(float)(totalitem-1))*(float)((bottom-top)*8));

    if( scrollbarpos < Y ) 
        scrollbarpos = Y;
	else if (scrollbarpos >= (Y+(bottom-top)*lineheight)) 
        scrollbarpos = (Y-1+(bottom-top)*lineheight);

    LCD_gselfont(&bold2);   
    LCD_gputsrpos(119,scrollbarpos,(void*)SCROLLBLOCK);

    LCD_gsetpos(127,Y-lineheight);                  LCD_glineto(127,Y+(bottom-top)*lineheight+1);
	LCD_gsetpos(117,Y-lineheight);                  LCD_glineto(117,Y+(bottom-top)*lineheight+1);
	LCD_gsetpos(117,Y-lineheight);                  LCD_glineto(127,Y-lineheight);
	LCD_gsetpos(117,Y+(bottom-top)*lineheight+1);   LCD_glineto(127,Y+(bottom-top)*lineheight+1);

    LCD_gselfont(&msfont);
}
//---------------------------------------------------------------------------------------------------------------------------
// Inputs:	u8 lineheight (should be 8), startX, startY (pixel position of scroll box)
//			u8 ischeckboxenable (setup menu as a list to select)
//			MENUITEMSTR* content (use ParseMenuItemStr to set it up)
// Output:	u8* selectionresult (1byte)
// Return:	STATUS  (S_USERABORT, S_SUCCESS)
// Note:	if ischeckboxenable, S_USERABORT == done selection (a list)
//			else S_SUCCESS == done selection (1 item)
// Engineer: Quyen Leba
//---------------------------------------------------------------------------------------------------------------------------
u8 Scroller(u8* selectionresult, u8 maxselection, MENUITEMSTR* content, u8 totalitem, u8 maxline, u8 ischeckboxenable, u8 lineheight, u8 startX, u8 startY, funcptr f_select, funcptr f_unselect)
{
	u8 cursor=0, prev_cursor=0, isclearrequire=1;
	u8 listbox_top=0, listbox_bottom;
	u8 key;
	u8 totalselection=0;
	u8 i;

	if (totalitem <= maxline)
	{
		listbox_bottom = totalitem-1;
	}
	else
	{
		listbox_bottom = maxline-1;
	}
	
	for (i=0;i<totalitem;i++)
	{
		if (content[i].checked[0] == CHECKMARK) totalselection++;
	}
	
	RefreshListbox(content, totalitem, cursor, prev_cursor, isclearrequire, listbox_top, listbox_bottom, lineheight, startX, startY);
	while(1)
	{
		key = getKey();

        if (key == SW_UP || key == SW_DOWN)
        {
            nav_direction = key;
            nav_changed = TRUE;
        }

		if (key == SW_EXIT)
		{
			*selectionresult = totalselection;		// only use for checkbox list - # of items selected
			return S_USERABORT;
		}
		else if (key == SW_ENTER)
		{
			//call function
			if (ischeckboxenable)
			{
				if (content[cursor].checked[0] != CHECKMARK && totalselection < maxselection)
				{
					content[cursor].checked[0] = CHECKMARK;
					totalselection++;
					if (f_select(0,cursor) != S_SUCCESS)
					{
						content[cursor].checked[0] = ' ';
						totalselection--;
					}
				}
				else if (content[cursor].checked[0] == CHECKMARK)
				{
					content[cursor].checked[0] = ' ';
					if (totalselection > 0) totalselection--;
					else totalselection = 0;
					if (f_unselect(0,cursor) != S_SUCCESS)
					{
						content[cursor].checked[0] = CHECKMARK;
						totalselection++;
					}
				}
				else
				{
					content[cursor].checked[0] = ' ';
				}
				RefreshListbox(content, totalitem, cursor, cursor, 1, listbox_top, listbox_bottom, lineheight, startX, startY);
			}
			else
			{
				*selectionresult = cursor;
				return S_SUCCESS;
			}
		}
		else if (nav_changed == TRUE)
		{
			nav_changed = FALSE;
			prev_cursor = cursor;
			if (nav_direction == SW_UP)	// WHEELLEFT
			{
				if (cursor == 0)
				{
					isclearrequire = 1;
					cursor = totalitem-1;
					if (totalitem <= maxline)
					{
						listbox_top = 0;
						listbox_bottom = totalitem - 1;
					}
					else
					{
						listbox_top = totalitem - maxline;
						listbox_bottom = totalitem - 1;
					}
				}
				else if (cursor == listbox_top)
				{
					isclearrequire = 1;
					//scroll up
					listbox_top--;
					listbox_bottom--;
					cursor--;
					//refresh listbox
				}
				else
				{
					isclearrequire = 0;
					cursor--;
				}
			}
			else						// WHEELRIGHT
			{
				if (cursor == (totalitem-1))
				{
					isclearrequire = 1;
					cursor = 0;
					if (totalitem <= maxline)
					{
						listbox_top = 0;
						listbox_bottom = totalitem - 1;
					}
					else
					{
						listbox_top = 0;
						listbox_bottom = maxline - 1;
					}
				}
				else if (cursor == listbox_bottom)
				{
					isclearrequire = 1;
					//scroll down
					listbox_top++;
					listbox_bottom++;
					cursor++;
					//refresh listbox
				}
				else
				{
					isclearrequire = 0;
					cursor++;
				}
			}
			RefreshListbox(content, totalitem, cursor, prev_cursor, isclearrequire, listbox_top, listbox_bottom, lineheight, startX, startY);
		}
	}
}


//------------------------------------------------------------------------------------------------------------------------------------------------
// EOF
//------------------------------------------------------------------------------------------------------------------------------------------------
