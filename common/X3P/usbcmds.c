/**
  **************** (C) COPYRIGHT 2011 SCT Performance, LLC *********************
  * File Name          : usbcmds.c
  * Device / System    : SCT products using the "CommonCode" platform
  * Author             : Quyen Leba
  *
  * Version            : 1 
  * Date               : 05/02/2011
  * Description        : 
  *                    : 
  *
  *
  * History            : 05/02/2011 M. Davis
  *                    :   Add SCT Copyright header,
  *
  ******************************************************************************
  */

#include <board/genplatform.h>
#include <common/settings.h>
#include <common/statuscode.h>
#include <fs/genfs.h>
#include "usbcmds.h"

enum
{
    USB_LOCKED,
    USB_UNLOCKED = 0x3A,
};

#define     USB_BUFFER_SIZE 4096

u8          USB_AccessState = USB_LOCKED;
F_DRIVER    *ftl_driver = NULL;
u32         ftl_chunk_index = 0;
F_FILE      *f_file_transfer;
u32         f_file_transfer_length;
u32         f_file_transfer_count;
F_FIND      f_find_info;
u8          *USB_Buffer;
u32         USB_NAND_Block;
u8          USB_NAND_Page;
u8          It_is_stock_bin =0;

void    USB_WriteFileHeader(u32 length);
void    USB_WriteFileData(u32 length);
void    USB_FTL_Write();
void    USB_FTL_Read();

//------------------------------------------------------------------------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------------------------------------------------------------------------
void USB_CMDS(USB_SETUP_PACKET *setup_pkg)
{
    u32         response = 1;
    static u8   _s_status;
    
    USB_AccessState = USB_UNLOCKED;
    //---------------------------------------------------------------------------------------------------
    //USB is in unlocked state
    //---------------------------------------------------------------------------------------------------
    if (USB_AccessState == USB_UNLOCKED)
    {
        volatile s32 randnum;
        switch(setup_pkg->packet.request)
        {
        case USB_CMD_WRITE_FILE:
            USB_WriteFileHeader(setup_pkg->packet.value);
            break;
        case USB_CMD_WRITE_FILE_DATA:
            USB_WriteFileData(setup_pkg->packet.value);
            break;
        case USB_CMD_SEND_FILE:
            response = 0;
            u8 fileinfo[264];
            memset((u8*)fileinfo,0,264);
            usb_rx((u8*)fileinfo,setup_pkg->packet.value);
            fileinfo[setup_pkg->packet.value] = 0;              //odd # bytes might stuff invalid data
            f_file_transfer = __fopen((void*)fileinfo,"r");
            if (f_file_transfer)
            {
                f_seek(f_file_transfer,0,SEEK_END);
                f_file_transfer_length = f_tell(f_file_transfer);
                f_seek(f_file_transfer,0,SEEK_SET);
            }
            else
            {
                f_file_transfer_length = 0;
            }
            f_file_transfer_count = 0;
            usb_tx((u8*)&f_file_transfer_length,4);
            break;
        case USB_CMD_SEND_FILE_DATA:
            response = 0;
            USB_Buffer = __malloc(USB_BUFFER_SIZE);
            if (USB_Buffer == NULL)
            {
                //ERROR_Message(MALLOC);  // may cause USB stuck until user response
                break;
            }
            response = f_read((u8*)USB_Buffer,1,USB_BUFFER_SIZE,f_file_transfer);
            usb_tx((u8*)USB_Buffer,(u32)(response));
            
            f_file_transfer_count += USB_BUFFER_SIZE;
            if (f_file_transfer_count >= f_file_transfer_length)
            {
                __fclose(f_file_transfer);
                f_file_transfer = (void*)0;
                f_file_transfer_length = 0;
            }
            __free(USB_Buffer);
            break;
        case USB_CMD_SEND_IMAGE:
            break;
        case USB_CMD_SEND_IMAGE_DATA:
            break;
        case USB_CMD_WRITE_IMAGE:
            break;
        case USB_CMD_WRITE_IMAGE_DATA:
            break;
        case USB_CMD_FTL_INIT:
            if (ftl_driver == NULL)
            {
                ftl_driver = fs_driver_init();
            }
            ftl_chunk_index = 0;
            response = S_SUCCESS;
            usb_tx((void*)&response,4);
            break;
        case USB_CMD_FTL_WRITE:
            USB_FTL_Write();
            break;
        case USB_CMD_FTL_READ:
            USB_FTL_Read();
            break;
        case USB_CMD_REINIT_FILESYSTEM:
            response = fs_reinit();
            usb_tx((void*)&response,4);
            break;
        case USB_CMD_FTL_CHUNK_INDEX:
            ftl_chunk_index = setup_pkg->packet.index;
            response = S_SUCCESS;
            usb_tx((void*)&response,4);
            break;
        case USB_CMD_GET_LISTING:
            //response = f_chdrive(0);
            //response = f_chdir("/");
            _s_status = USB_CMD_GET_LISTING;
            //f_find_info.filename[0] = 0;
            //response = f_findfirst("*.*",&f_find_info);
            usb_tx((u8*)response,4);                      //software currently does not process this
            break;
        case USB_CMD_GET_LISTING_NEXT:
            response = 0;
            u8 filebuf[264];
            if (_s_status == USB_CMD_GET_LISTING)
            {
                response = f_findfirst("/SCT/*.*",&f_find_info);
                _s_status = USB_CMD_GET_LISTING_NEXT;
            }
            else
            {
                response = f_findnext(&f_find_info);
            }
            if (response)
            {
                memset((void*)filebuf,0,264);
                _s_status = USB_CMD_GET_LISTING;
            }
            else
            {
                s32 *ptr = (s32*)&filebuf[260];
                strcpy((void*)filebuf,(void*)f_find_info.filename);
                *ptr = f_find_info.filesize;
            }
            usb_tx((u8*)filebuf,264);
            break;
        case USB_CMD_GET_FREE_SPACE:
            response = 0;
            F_SPACE f_space_info;
            response = f_getfreespace(0,&f_space_info);
            usb_tx((u8*)&f_space_info.free,4);
            break;
        case USB_CMD_DELETE_FILE:
            response = 0;
            USB_Buffer = __malloc(300);
            u8  *ptr;
            if (USB_Buffer == NULL)
            {
                //ERROR_Message(MALLOC);      //may stuck
                break;
            }
            memset((u8*)&USB_Buffer[5],0,264);
            usb_rx((u8*)&USB_Buffer[5],setup_pkg->packet.value);
            if (USB_Buffer[5] == '/')
            {
                memcpy((void*)&USB_Buffer[1],"/SCT",4);
                ptr = &USB_Buffer[1];
            }
            else
            {
                memcpy((void*)&USB_Buffer[0],"/SCT/",5);
                ptr = &USB_Buffer[0];
            }
            response = f_delete((void*)ptr);
            __free(USB_Buffer);
            usb_tx((u8*)&response,4);
            break;
        case USB_CMD_KEY_REQUEST:
            {
                u8 temp_seed[16];
                usb_tx((u8*)temp_seed,16);
            }
            break;
        case USB_CMD_KEY_AUTHENTICATE:
            {
                u8 temp_key[256];
                usb_rx((u8*)&temp_key,setup_pkg->packet.value);
            }
            response = 1;
            usb_tx((u8*)&response,4);
            break;
        case USB_CMD_ENABLE_LOCK:
            USB_AccessState = USB_LOCKED;
            break;
        case USB_CMD_LIVEWIRE_FW_VERSION:
            break;
        case USB_CMD_LIVEWIRE_TUNE_VERSION:
            break;
        case USB_CMD_CLEAR_SERIAL:
            break;
        default:
            break;
        }
    }
    //---------------------------------------------------------------------------------------------------
    //USB is in locked state
    //---------------------------------------------------------------------------------------------------
    else
    {
    }
}

//------------------------------------------------------------------------------------------------------------------------------------------------
// Setup file for a file write from HOST (i.e. get filename & filesize) & create entry in filesystem
// Input:   u32 length (length of incoming USB message)
// Engineer: Quyen Leba
// Date: Feb 18, 2008
//------------------------------------------------------------------------------------------------------------------------------------------------
void USB_WriteFileHeader(u32 length)
{
    u32 bytecount;

    USB_Buffer = __malloc(USB_BUFFER_SIZE);
    if (USB_Buffer == NULL)
    {
        //ERROR_Message(MALLOC);  // may cause USB stuck until user response
        return;
    }
    bytecount = usb_rx((u8*)USB_Buffer,length);
    f_file_transfer_length = *(u32*)&USB_Buffer[256];
    if (f_file_transfer_length > 0 && bytecount > 0)
    {
        f_file_transfer_count = 0;
        USB_Buffer[length] = 0;             // odd # of bytes might stuff invalid data
        f_file_transfer = __fopen((void*)&USB_Buffer[1],"w");
        if(strcmp((void*)&USB_Buffer[0],"/stock.bin") == 0)
          It_is_stock_bin =1;

    }
    else
    {
        f_file_transfer = (void*)0;
    }
    __free(USB_Buffer);
    usb_tx((u8*)&bytecount,4);
}

//------------------------------------------------------------------------------------------------------------------------------------------------
// Write data to file
// Input:   u32 length (length of incoming USB message)
// Engineer: Quyen Leba
// Date: Feb 18, 2008
//------------------------------------------------------------------------------------------------------------------------------------------------
void USB_WriteFileData(u32 length)
{
    u32 bytecount;

    USB_Buffer = __malloc(USB_BUFFER_SIZE);
    if (USB_Buffer == NULL)
    {
        //ERROR_Message(MALLOC);  // may cause USB stuck until user response
        return;
    }

    //intrstatus = (u32)USB_InterruptStatus;
    bytecount = usb_rx((u8*)USB_Buffer,length);
    //intrstatus = (u32)USB_InterruptStatus;

    f_file_transfer_count += bytecount;
    if (bytecount > 0)
    {
        f_write((u8*)USB_Buffer,1,length,f_file_transfer);
    }
    if (f_file_transfer_count >= f_file_transfer_length)
    {
        __fclose(f_file_transfer);
        if(It_is_stock_bin == 1)
        {
            SETTINGS_TUNE(stockfilesize) = f_file_transfer_count;
            SETTINGS_TUNE(married_status) |= 1;
            It_is_stock_bin =0;
            settings_update(TRUE);
        }
    }

    __free(USB_Buffer);
    usb_tx((u8*)&bytecount,4);
}

//------------------------------------------------------------------------------
// Read 4096-byte ftl chunk
// Input:   u16 chunk_index (1 chunk = 4096 bytes, 1 ftl sector = 512 bytes)
// Engineer: Quyen Leba
//------------------------------------------------------------------------------
void USB_FTL_Read()
{
    u32 status;
    
    if (ftl_driver == NULL)
    {
        return;
    }
    
    USB_Buffer = __malloc(4096);
    if (USB_Buffer == NULL)
    {
        return;
    }
    
    status = ftl_driver->readmultiplesector(ftl_driver,USB_Buffer,
                                            ftl_chunk_index*8,8);

    if (status == 0)    //ftl read success
    {
        ftl_chunk_index++;
        usb_tx(USB_Buffer,8*512);
    }
    else
    {
        //software will stuck unless it can timeout, but whatever
    }
    
    __free(USB_Buffer);
}

//------------------------------------------------------------------------------
// Write 4096-byte ftl chunk
// Input:   u16 chunk_index (1 chunk = 4096 bytes, 1 ftl sector = 512 bytes)
// Engineer: Quyen Leba
//------------------------------------------------------------------------------
void USB_FTL_Write()
{
    u32 status;
    u32 response;
    
    if (ftl_driver == NULL)
    {
        return;
    }
    
    USB_Buffer = __malloc(4096);
    if (USB_Buffer == NULL)
    {
        return;
    }
    
    usb_rx((void*)USB_Buffer,4096);
    
    if (ftl_driver)
    {
        status = ftl_driver->writemultiplesector(ftl_driver,USB_Buffer,
                                                 ftl_chunk_index*8,8);
        if (status == 0)
        {
            ftl_chunk_index++;
            response = S_SUCCESS;
        }
        else
        {
            response = S_FAIL;
        }
    }
    else
    {
        response = S_FAIL;
    }
    
    __free(USB_Buffer);
    
    usb_tx((u8*)&response,4);
}
