/**
  **************** (C) COPYRIGHT 2011 SCT Performance, LLC *********************
  * File Name          : ZZZsettings.h
  * Device / System    : SCT products using the "CommonCode" platform
  * Author             : Quyen Leba
  *
  * Version            : 1 
  * Date               : 05/02/2011
  * Description        : 
  *                    : 
  *
  *
  * History            : 05/02/2011 M. Davis
  *                    :   Add SCT Copyright header,
  *
  ******************************************************************************
  */

#ifndef __SETTINGS_H
#define __SETTINGS_H

#include <arch/gentype.h>
#include <common/X3P/version.h>

typedef struct
{
    float   a[2];
    float   b[2];
    u8      type[2];
}CUSTOMEQ;

#define         SETTINGS_LENGTH         1024

typedef struct
{
    u8          Sct[3];
    u8          Contrast;
    u32         Backlight;
    u8          eec_type;           // indicates which EEC this cal is for
    u8          comm_type;
    u8          married;            // indicates that this device is married
    u8          married_cnt;        // how many times married's been cleared
    u8          cur_strat[8];
    u8          num_of_proc;        // number of processors detected
    u8          Dwnld_Fail;	        // Processor erased, but failed download
    u8          VID[128];           // holds vehicle ID info
    u8          EPATS[14];          // holds evhicle EPATS info
    u8          VIN[17];
    u8          RunBootloader;
    u8          serial_num[13];     // Holds the device serial number
    u8          Description[20];
    u8          RPM_flag;
    u32         RPM_int;
    u8          dev_bit;            // 1 = Development Menu Opts, 0 = Regular Menu Opts
    u32         stockfilesize;
    u8          selected_PIDs[6];
    u8          car_type;           //1=gas-can,2=deisel-can,3 gas-scp, 4 deisel scp
    u8          prev_VIN[17];
    u8          PL_Enable;          // Power Light on/off
    u8          PL_Flash_Enable;    // Power Light flash when in stand by on/off
    u8          Alert_Enable;       // Enable and disable of Alert watch on PID monitor
    u8          Alert_watch[6];     // PIDs and analog inputs to be watched on alert
    float       Alert_MAX[6];       // Holds Max values for Alert watch
    u8          A1_on;              //analog 1 on
    u8          A2_on;              //analog 2 on
    u8          Equate1;            //equation for analog input channel 1 
    u8          Equate2;            //equation for analog input channel 2
    u8          Analog1_Des[19];    //analog input description channel 1
    u8          Analog2_Des[19];    //analog input description channel 2
    u8          A1_Units[11];       //analog units display channel 1
    u8          A2_Units[11];       //analog units display channel 2
    u8          A1_selected_num;    //Number in the selection of PIDs, need to clear this if any changes made to channel
    u8          A2_selected_num;    //Number in the selection of PIDs, need to clear this if any changes made to channel
    u8          DebugProtectStatus; // Debug Protection Status byte
    u32         FWVersion;          // unused
    u8          strategy_filename[21]; // Save the file name of the strategy file for the vehicle which the device is married to
    u8          packedDeviceCfg;    // OR'ed MarketType and DeviceType, used by Advantage desktop software
    u8          DLF_filename[20];
    u32         DLfilesize[10];     //filesize of datalogX.dat
    u8          SplashScreen[50];   // For the dealer info/splash screen
    u8          VID_can[256];       // holds larder vehicle ID info block which includes the trans ID block found in CAN processors.
    u32         Tunefilesize[3];    // Holds the tunefile raw area filesizes
    u8          datalogprecision;
    u8          record_monitor;
    u8          record_file[14];
    CUSTOMEQ    AnalogCustomEq[2];
    float       battery;
    u8          user_okd_Lvol;      //User ok'd using low battery volatge to flash vehicle, even after warning
    u8          prev_strat[8];
    u8          bootloader[8];
    u8          Selected_PIDs[30];  //30 for now
    u32         MarketType;
    u32         DeviceType;
    u8          LCD_Type;
    u8          Trans_strat[8];     // GM transmission strategy

    
}Settings;

typedef struct
{
    u8          BuzzerDisabledUSB; // Disable sounds when USB-powered
    u32         PreviousMarketType;   // test & update after firmware download
    u32         PreviousDeviceType;   // test & update after firmware download
    u8          PreviousFWVersion[FWverSize];   // test & update after firmware download
}SettingsFlags;

extern Settings settings;
extern SettingsFlags settingsFlags;

void    Settings_Init(void);
void    Settings_Reset(void);
void    Settings_ReadSerialOTP(u8 *serial);
void    Settings_WriteSerialOTP(u8 *serial);
u8      Settings_CompareVIN(u8 *vin);
void    Settings_SetBootloader(u8 mode, u16 custompid);
void    Settings_ClearBootloader(void);

#define Settings_GetMarketType       (settings.MarketType)
#define Settings_GetDeviceType       (settings.DeviceType)
#define Settings_IsStrategyTuneAllow (settings.DeviceType != GMBINX3)
#define Settings_IsCustomTuneAllow   (1)

void    Settings_CheckPriorFirmwareUpdate(void);
void    SaveSettings(void);
void    LoadSettings(void);

#endif

//------------------------------------------------------------------------------------------------------------------------------------------------
// EOF
//------------------------------------------------------------------------------------------------------------------------------------------------
