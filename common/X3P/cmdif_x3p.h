/**
  **************** (C) COPYRIGHT 2011 SCT Performance, LLC *********************
  * File Name          : cmdif_x3p.h
  * Device / System    : SCT products using the "CommonCode" platform
  * Author             : Quyen Leba
  *
  * Version            : 1 
  * Date               : 05/02/2011
  * Description        : 
  *                    : 
  *
  *
  * History            : 05/02/2011 M. Davis
  *                    :   Add SCT Copyright header,
  *
  ******************************************************************************
  */

#ifndef __CMDIF_X3P_H
#define __CMDIF_X3P_H

u8 cmdif_x3p_internalresponse_ack(u16 command, u16 response, u8 *data, u16 length);
#define cmdif_internalresponse_ack(command,response,data,length)        \
    cmdif_x3p_internalresponse_ack(command,response,data,length)

u8 cmdif_x3p_internalresponse_ack_nowait(u16 command, u16 response, u8 *data, u16 length);
#define cmdif_internalresponse_ack_nowait(command,response,data,length) \
    cmdif_x3p_internalresponse_ack_nowait(command,response,data,length)

u8 cmdif_x3p_receive_command_internal(u8 *data, u16 *length);
#define cmdif_receive_command_internal(data,length)     \
    cmdif_x3p_receive_command_internal(data,length)

#endif	//__CMDIF_X3P_H
