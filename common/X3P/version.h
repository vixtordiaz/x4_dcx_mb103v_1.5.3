/**
  **************** (C) COPYRIGHT 2011 SCT Performance, LLC *********************
  * File Name          : version.h
  * Device / System    : SCT products using the "CommonCode" platform
  * Author             : Quyen Leba
  *
  * Version            : 1 
  * Date               : 05/02/2011
  * Description        : 
  *                    : 
  *
  *
  * History            : 05/02/2011 M. Davis
  *                    :   Add SCT Copyright header,
  *
  ******************************************************************************
  */

#ifndef __VERSION_H__
#define __VERSION_H__

enum
{
    VERSION_BACKDATED   = -1,
    VERSION_OLD         = -1,
    VERSION_CURRENT     = 0,
    VERSION_UPDATED     = 1,
    VERSION_NEW         = 1,
};

#define     FWverSize 24

extern u8   Firmware_Version[FWverSize];
extern u8   Tune_Version[10];

//void        Version_FirmwareCheck(void);
//void        Version_TuneCheck(void);
void        Version_DeviceCheck(void);

#endif

//------------------------------------------------------------------------------------------------------------------------------------------------
// EOF
//------------------------------------------------------------------------------------------------------------------------------------------------
