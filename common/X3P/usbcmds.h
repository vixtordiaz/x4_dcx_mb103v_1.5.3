/**
  **************** (C) COPYRIGHT 2011 SCT Performance, LLC *********************
  * File Name          : usbcmds.h
  * Device / System    : SCT products using the "CommonCode" platform
  * Author             : Quyen Leba
  *
  * Version            : 1 
  * Date               : 05/02/2011
  * Description        : 
  *                    : 
  *
  *
  * History            : 05/02/2011 M. Davis
  *                    :   Add SCT Copyright header,
  *
  ******************************************************************************
  */

#ifndef __VERSION_H__
#define __VERSION_H__

#include <arch/gentype.h>
#include <board/X3P/usb.h>

enum
{
    USB_CMD_PASSTHROUGH             = 49u,
    USB_CMD_CANDETECTED             = 52u,
    USB_CMD_SCPDETECTED             = 53u,
    USB_CMD_VPWDETECTED             = 54u,

    USB_CMD_NAND_ADDRESS            = 101,
    USB_CMD_NAND_READ               = 102,
    USB_CMD_NAND_WRITE              = 103,
    USB_CMD_FTL_INIT                = 105,
    USB_CMD_FTL_READ                = 106,
    USB_CMD_FTL_WRITE               = 107,
    USB_CMD_REINIT_FILESYSTEM       = 108,
    USB_CMD_FTL_CHUNK_INDEX         = 109,
    
    USB_CMD_SET_ADDRESS             = 64u,
    USB_CMD_WRITE_MEM               = 65u,
    USB_CMD_READ_MEM                = 66u,
    USB_CMD_SET_BULK_TR_LENGTH      = 67u,
    USB_CMD_WRITE_MEM_BULK          = 68u,
    USB_CMD_READ_MEM_BULK           = 69u,
    USB_CMD_WRITE_FILE			    = 70u,
    USB_CMD_WRITE_FILE_DATA	        = 71u,
    USB_CMD_SEND_FILE			    = 72u,
    USB_CMD_SEND_FILE_DATA	 	    = 73u,
    USB_CMD_SEND_IMAGE			    = 74u,
    USB_CMD_SEND_IMAGE_DATA	  	    = 75u,
    USB_CMD_WRITE_IMAGE             = 76u,
    USB_CMD_WRITE_IMAGE_DATA        = 77u,
    USB_CMD_GET_LISTING             = 78u,
    USB_CMD_GET_LISTING_NEXT        = 79u,
    USB_CMD_GET_FREE_SPACE		    = 80u,
    USB_CMD_FORMAT_MMC			    = 81u,
    USB_CMD_DELETE_FILE			    = 82u,
    USB_CMD_START_BOOTLOADER        = 83u,
    USB_CMD_SEND_SETTINGS           = 84u,
    USB_CMD_WRITE_SETTINGS          = 85u,
    USB_CMD_KEY_REQUEST             = 86u,
    USB_CMD_KEY_AUTHENTICATE        = 87u,
    USB_CMD_INIT_SETTINGS           = 88u,
    USB_CMD_ENABLE_LOCK             = 89u,
    USB_CMD_ENABLE_DEBUGP           = 90u,
    USB_CMD_DISABLE_DEBUGP          = 91u,
    USB_CMD_LIVEWIRE_FW_VERSION     = 92u,
    USB_CMD_LIVEWIRE_TUNE_VERSION   = 93u,
    USB_CMD_CLEAR_SERIAL            = 94u,

    USB_CMD_GET_VERSION             = 110u,     //send 16chars. ex: BOOT2.01 (bootload) or "1.1.123" (app)
    USB_CMD_GET_SERIAL_OTP          = 111u,
    USB_CMD_SET_SERIAL_OTP          = 112u,
    USB_CMD_GET_SERIAL_SETTINGS     = 113u,
    USB_CMD_SET_SERIAL_SETTINGS     = 114u,
    USB_CMD_14229DETECTED           = 115u,
    USB_CMD_14229DISENGAGED         = 116u,
};

void    USB_CMDS(USB_SETUP_PACKET *setup_pkg);

#endif
