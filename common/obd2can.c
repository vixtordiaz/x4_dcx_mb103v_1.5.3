/**
  **************** (C) COPYRIGHT 2011 SCT Performance, LLC *********************
  * File Name          : obd2can.c
  * Device / System    : SCT products using the "CommonCode" platform
  * Author             : Quyen Leba
  *
  * Version            : 1 
  * Date               : 05/02/2011
  * Description        : 
  *                    : 
  *
  *
  * History            : 05/02/2011 M. Davis
  *                    :   Add SCT Copyright header,
  *
  ******************************************************************************
  */

//------------------------------------------------------------------------------
// Log error point: 0x20xx -> 0x21xx
//------------------------------------------------------------------------------

#include <board/delays.h>
#include <board/rtc.h>
#include <common/statuscode.h>
#include <common/log.h>
#include <string.h>
#include "obd2can.h"

//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
extern u8 obd2_rxbuffer[256];           /* from obd2.c */
extern obd2_info gObd2info;             /* from obd2.c */
extern Datalog_Mode gDatalogmode;       /* from obd2datalog.c */

//------------------------------------------------------------------------------
// Inputs:  Obd2canInitMode initmode
//          u32 custom_filter_addr (for Obd2canInitCustom, others use 0x00)
//------------------------------------------------------------------------------
void obd2can_init(Obd2canInitMode initmode, u32 custom_filter_addr)
{
    if (initmode == Obd2canInitProgram)
    {
        can_init(CAN_INITMODE_7EX,0);
    }
    else if (initmode == Obd2canInitCustom)
    {
        can_init(CAN_INITMODE_CUSTOM,custom_filter_addr);
    }
    else //if (initmode == Obd2canInitNormal)
    {
        can_init(CAN_INITMODE_NORMAL,0);
    }
}

//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
void obd2can_servicedata_init(Obd2can_ServiceData *servicedata)
{
    servicedata->service = 0xFF;
    servicedata->subservice = 0xFF;

    servicedata->ecm_id = 0x7E0;
    servicedata->response_id = NULL;
    servicedata->broadcast_id = 0x7E0;
    servicedata->broadcast = FALSE;
    servicedata->response_expected = TRUE;
    servicedata->nowait_cantx = FALSE;
    servicedata->tester_present_type = TESTER_PRESENT_NONE;

    memset(servicedata->service_controldatabuffer,
           0,sizeof(servicedata->service_controldatabuffer));
    servicedata->service_controldatalength = 0;    
    
    servicedata->txdata = NULL;
    servicedata->txdatalength = 0;
    servicedata->first_frame_data_offset = 0;
    servicedata->rxdata = obd2_rxbuffer;
    servicedata->rxdatalength = 0;
    servicedata->priv = NULL;
    servicedata->errorcode = 0;
    servicedata->initial_wait_extended = 1;     //default 1sec
    servicedata->busy_wait_extended = 3;        //default 30sec
    servicedata->rx_timeout_ms = 5000;          //default 5sec
    servicedata->commlevel = CommLevel_Default;
    servicedata->flowcontrol_blocksize = 0;
    servicedata->flowcontrol_separationTime = 0;
}

//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
void obd2can_rxinfo_init(obd2can_rxinfo *rxinfo)
{
    rxinfo->version = OBD2CAN_RXINFO_VERSION;
    rxinfo->secondary_response_ecm_id = 0xFFFFFFFF;
    rxinfo->rxbuffer = obd2_rxbuffer;
    rxinfo->rxlength = 0;
    rxinfo->cmd = 0xFF;
    rxinfo->response_cmd_offset = 0x40;
    rxinfo->first_frame_data_offset = 0;
    rxinfo->tester_present_type = TESTER_PRESENT_NONE;
    rxinfo->errorcode = 0;
    rxinfo->initial_wait_extended = 1;  //default 1sec
    rxinfo->busy_wait_extended = 3;     //default 30sec
    rxinfo->rx_timeout_ms = 5000;             //default 5sec
    rxinfo->commlevel = CommLevel_Default;
    rxinfo->flowcontrol_blocksize = 0;
    rxinfo->flowcontrol_separationTime = 0;
}

//------------------------------------------------------------------------------
// Send single frame only, frame data is as-is (at most 8 bytes)
// Inputs:  u32 ecm_id  (the ecm address; 0x7E0, etc)
//          u8  *txbuffer
//          u16 txlength (IMPORTANT: never more than 8)
// Return:  u8  status
// Engineer: Quyen Leba
//------------------------------------------------------------------------------
u8 obd2can_txraw(u32 ecm_id, u8 *txbuffer, u16 txlength, bool nowait_cantx)
{
    CanTxMsg txframe = CanMsgParseStdFrame(ecm_id);
    
    memcpy(CanMsgFrameData(txframe),txbuffer,txlength);
    memset(&CanMsgFrameData(txframe)[txlength],
           obd2can_get_padding_byte(),8-txlength);
    if (nowait_cantx == FALSE)
    {
        return can_tx(txframe);
    }
    else
    {
        return can_tx_s(txframe);
    }
}

//------------------------------------------------------------------------------
// Send single frame only, with extended ID
// Inputs:  u32 ecm_extended_id  (the ecm address; 0x18EF03FA, etc)
//          u8  *txbuffer
//          u16 txlength (IMPORTANT: never more than 8)
// Return:  u8  status
// Engineer: Quyen Leba
//------------------------------------------------------------------------------
u8 obd2can_txraw_ext(u32 ecm_extended_id, u8 *txbuffer, u16 txlength)
{
    CanTxMsg txframe = CanMsgParseExtFrame(ecm_extended_id,txlength);
    
    memcpy(CanMsgFrameData(txframe),txbuffer,txlength);
    return can_tx_ext(txframe);
}

//------------------------------------------------------------------------------
// Receive a CAN frame with extended ID
// Inputs:  u32 ecm_response_extended_id    (the ecm address; 0x18EF03FA, etc)
//          u8  *rxbuffer
//          u16 *rxlength
// Return:  u8  status
// Engineer: Quyen Leba
//------------------------------------------------------------------------------
u8 obd2can_rxraw_ext(u32 ecm_response_extended_id, u8 *rxbuffer, u16 *rxlength)
{
    u32 timeout_mscount;
    u32 time_mscount;
    u8  status;
    
    *rxlength = 0;
    timeout_mscount = 2000;
    time_mscount = rtc_getvalue();
    while(1)
    {
        status = can_rx_ext(10);
        if (status == S_SUCCESS)
        {
            timeout_mscount = 2000;
            time_mscount = rtc_getvalue();
            
            if (CanMsgFrameExtendedAddr(can_rxframe) == ecm_response_extended_id)
            {
                *rxlength = CanMsgFrameDataLength(can_rxframe);
                memcpy(rxbuffer,CanMsgFrameData(can_rxframe),
                       CanMsgFrameDataLength(can_rxframe));
                return S_SUCCESS;
            }
            else
            {
                //discard the frame
            }
        }
        else
        {
            if ((rtc_getvalue() - time_mscount) > timeout_mscount)
            {
                log_push_error_point(0x2007);
                return S_TIMEOUT;
            }
        }
    }//while(1)...
}

//------------------------------------------------------------------------------
// Inputs:  u32 ecm_id  (the ecm address; 0x7E0, etc)
//          u8  *txbuffer
//          u16 txlength
// Return:  u8  status
// Engineer: Quyen Leba
//TODOQ: get rid of this function
//------------------------------------------------------------------------------
u8 obd2can_tx(u32 ecm_id, u8 *txbuffer, u16 txlength, bool nowait_cantx)
{
    return obd2can_txcomplex(ecm_id, 0, 0xFF, 0xFF, NULL, 0, txbuffer, txlength,
                             nowait_cantx);
}

//------------------------------------------------------------------------------
// Inputs:  u32 ecm_id  (the ecm address; 0x7E0, etc)
//          u32 response_id (response address; 0x7E8, etc)
//          u8  *txbuffer
//          u16 txlength
//          bool nowait_cantx
// Return:  u8  status
// Engineer: Quyen Leba
//------------------------------------------------------------------------------
u8 obd2can_txcomplex(u32 ecm_id, u32 response_id, u8 cmd, u8 subcmd,
                     u8 *subcmdbuffer, u16 subcmdbufferlength,
                     u8 *txbuffer, u16 txlength, bool nowait_cantx)
{
    u8  data[24];
    u16 datalength;
    u32 cooldowntime;
    u8  status;

    datalength = 0;
    if (cmd != 0xFF)
    {
        data[datalength++] = cmd;
    }
    if (subcmd != 0xFF)
    {
        data[datalength++] = subcmd;
    }
    if ((datalength+subcmdbufferlength) > sizeof(data))
    {
        log_push_error_point(0x2009);
        return S_INPUT;
    }
    memcpy((char*)&data[datalength],(char*)subcmdbuffer,subcmdbufferlength);
    datalength += subcmdbufferlength;


    if (datalength)
    {
        status = mblif_link(MBLIF_CMD_WRITE_BUFFER, 0,
                            (u8*)data,&datalength);
        if (status != S_SUCCESS)
        {
            log_push_error_point(0x200A);
            return status;
        }
        if (txlength)
        {
            status = mblif_link(MBLIF_CMD_APPEND_BUFFER, 0,
                                (u8*)txbuffer,&txlength);
            if (status != S_SUCCESS)
            {
                log_push_error_point(0x200B);
                return status;
            }
            datalength += txlength;
        }
    }
    else if (txlength)
    {
        datalength = txlength;
        status = mblif_link(MBLIF_CMD_WRITE_BUFFER, 0,
                            (u8*)txbuffer,&datalength);
        if (status != S_SUCCESS)
        {
            log_push_error_point(0x200C);
            return status;
        }
    }

    cooldowntime = obd2_getcooldowntime();
    data[0] = 0;    //flags
    data[1] = 0;    //flags
    data[2] = 0;    //flags
    data[3] = 0;    //flags
    memcpy((char*)&data[4],(char*)&ecm_id,4);
    memcpy((char*)&data[8],(char*)&cooldowntime,4);
    memcpy((char*)&data[12],(char*)&datalength,2);
    memcpy((char*)&data[14],(char*)&response_id,4);

    status = mblexec_call(MBLEXEC_OPCODE_SEND_COMPLEX_MSG,
                          data,18,
                          NULL, NULL);
    
    return status;
}

//------------------------------------------------------------------------------
// Receive a CAN message without much processing, only use if you're sure that
// message is single-frame only.
// Outputs: u32 *response_ecm_id
//          u8  *data (upto 8 bytes)
// Return:  u8  status
//------------------------------------------------------------------------------
u8 obd2can_rxraw(u32 *response_ecm_id, u8 *data)
{
    u8 retry;
    u8 status;

    *response_ecm_id = 0;
    retry = 0;
    while(1)
    {
        status = can_rx(1000);
        if (status == S_SUCCESS)
        {
            *response_ecm_id = CanMsgFrameStandardAddr(can_rxframe);
            memcpy(data,CanMsgFrameData(can_rxframe),8);
            break;
        }
        else
        {
            retry++;
            if (retry > 3)
            {
                status = S_TIMEOUT;
                break;
            }
        }
    }
    return status;
}

/**
 * @brief   Receive a CAN message with basic processing
 *  
 * @param   [out] *response_ecm_id
 * @param   [in,out] *rxinfo
 * @param   [in] *packet_filter     Response ECM ID Packet Mask
 * @param   [in] packet_count       Number of Packet Masks
 *
 * @retval  u8 status
 *
 * @author  Tristen Pierson
 */
u8 obd2can_rxraw_complete(u32 *response_ecm_id, obd2can_rxinfo *rxinfo,
                          u32 *packet_filter, u8 packet_count)
{
    u32 ecm_id[3] = {Obd2CanEcuId_7E0, Obd2CanEcuId_7E1, Obd2CanEcuId_7E2};
    u8  flowcontrolframe[3] = {0x30, 0x00, 0x00};
    s16 bytecount = 0;
    u16 rxbuffer_index = 0;
    u8  frameid = 0x21;    
    u8  rxrawdata[8];
    u8  i;
    u8  status;
    
    while(1)
    {
        status = obd2can_rxraw(response_ecm_id, rxrawdata);
        
        // Return packet data if applicable
        if (status == S_SUCCESS)
        {
            for (i = 0; i < packet_count; i++)
            {
                if (*response_ecm_id == packet_filter[i])
                {
                    rxinfo->rxlength = 8;
                    memcpy(rxinfo->rxbuffer,&rxrawdata[0],8);
                    return S_SUCCESS;
                }
            }
        }
        // Process data but filter based on ecm id
        if (status == S_SUCCESS &&
            (*response_ecm_id == ECM_RESPONSE_ID(ecm_id[0]) || 
             *response_ecm_id == ECM_RESPONSE_ID(ecm_id[1]) ||
             *response_ecm_id == ECM_RESPONSE_ID(ecm_id[2])))
        {
            // Handle Single Frame
            if (rxrawdata[0] < 0x10 && rxrawdata[0] > 0x00)
            {
                // Handle negative response
                if (rxrawdata[1] == 0x7F)
                {                    
                    // Request Correctly Received-Response Pending
                    if (rxrawdata[3] == 0x78)
                    {
                        return S_BUSY;
                    }
                    // Negative Response
                    else
                    {                        
                        rxinfo->cmd = rxrawdata[2];
                        rxinfo->errorcode = rxrawdata[3];                        
                        bytecount = rxrawdata[0];
                        if (bytecount > 3)
                        {
                            rxinfo->rxlength = bytecount-3;
                            memcpy(rxinfo->rxbuffer,&rxrawdata[4],rxinfo->rxlength);
                        }                        
                        return S_ERROR;
                    }
                }
                // Assume successful otherwise
                else 
                {                    
                    bytecount = rxrawdata[0];                    
                    rxinfo->cmd = rxrawdata[1] - rxinfo->response_cmd_offset;
                    rxinfo->rxlength = --bytecount;
                    memcpy(rxinfo->rxbuffer,&rxrawdata[2],bytecount);                    
                    return S_SUCCESS;
                }
            }
            // Handle First Frame
            else if (rxrawdata[0] < 0x20)
            {                
                bytecount = ((u8)rxrawdata[0]&0x0F)<<8;
                bytecount |= (u8)rxrawdata[1];                
                rxinfo->rxlength = bytecount;                

                memcpy(rxinfo->rxbuffer,
                       &rxrawdata[3+rxinfo->first_frame_data_offset],
                       5-rxinfo->first_frame_data_offset);
                rxbuffer_index = 5-rxinfo->first_frame_data_offset;

                flowcontrolframe[1] = rxinfo->flowcontrol_blocksize;
                flowcontrolframe[2] = rxinfo->flowcontrol_separationTime;

                obd2can_txraw(ECM_ID(*response_ecm_id),flowcontrolframe,
                              sizeof(flowcontrolframe),FALSE);
                bytecount -= 6;                
                rxinfo->rxlength -= (1+rxinfo->first_frame_data_offset);

            }
            // Handle Consecutive Frames
            else if (rxrawdata[0] < 0x30)
            {
                if (frameid == rxrawdata[0])
                {
                    if (bytecount > 7)
                    {
                        memcpy(&rxinfo->rxbuffer[rxbuffer_index],&rxrawdata[1],7);
                        bytecount -= 7;
                    }
                    else
                    {
                        memcpy(&rxinfo->rxbuffer[rxbuffer_index],&rxrawdata[1],bytecount);
                        bytecount = 0;
                        return S_SUCCESS;
                    }
                    rxbuffer_index += 7;
                    frameid++;
                    frameid &= 0x2F;
                }
                // Frame Sequence Incorrect
                else
                {
                    return S_FAIL;
                }
            }
            // Frame Data Unrecognizable
            else
            {
                // Ignore this data
                return S_SUCCESS;
            }
        }//if (status == S_SUCCESS)...
        else
        {
            return status;
        }        
    }//while(1)...
}

//------------------------------------------------------------------------------
// Input:   u32 response_ecm_id (ecm response address; 0x7E8 ..., 0xFFFF: any)
//          u8 cmd (obd2 command; 0xFF = any)
//          u8 first_frame_data_offset (# bytes ignored in 1st frame of a
//             multiframe-message); at most 5 bytes
//          u8 allow_tester_present;
// Outputs: u8  *rxbuffer (strip length, cmd)
//          u16 *rxlength
//          u8  *errorcode (from 7F response); can be NULL if don't care
// Return:  u8  status
// Engineer: Quyen Leba
//------------------------------------------------------------------------------
#define TP_MSCOUNT_ELAPSED 2500
u8 obd2can_rx(u32 response_ecm_id, obd2can_rxinfo *rxinfo)
{
    u8 flowcontrolframe[3] = {0x30, 0x00, 0x00};
    s16 bytecount;
    u8 status;
    u8 frameid;
    u16 rxbuffer_index;
    u32 timeout_mscount;
    u32 time_mscount;
    u32 starttime_mscount;  //the beginning of this process
    u32 endtime_mscount;    //a desired message must be received before this
    u32 cooldowntime;
    bool tp_enable;
    u32  tp_mscount;
    u32  timeout;
    u32  tx_ecm_id = 0;
    
    if (rxinfo->first_frame_data_offset > 5)
    {
        log_push_error_point(0x2000);
        return S_INPUT;
    }
    
    rxbuffer_index = 0;
    frameid = 0x21;
    bytecount = 0;
    tp_enable = FALSE;
    tp_mscount = 0;    

    if (rxinfo->initial_wait_extended > 0)
    {
        timeout_mscount = 1000 * rxinfo->initial_wait_extended;
    }
    else
    {
        timeout_mscount = rxinfo->rx_timeout_ms;
    }    
        
    /* Set low level timeout */
    if (rxinfo->rx_timeout_ms > 1000)
    {
        timeout = 1000;
    }
    else
    {
        timeout = rxinfo->rx_timeout_ms;
    }
    
    time_mscount = rtc_getvalue();
    starttime_mscount = time_mscount;
    endtime_mscount = starttime_mscount + rxinfo->rx_timeout_ms;
    cooldowntime = obd2_getcooldowntime();


#ifdef __DCX_MANUF__

    tx_ecm_id = obd2can_dcx_findMatchingID(response_ecm_id, 1);
    
#else

    tx_ecm_id = ECM_ID(response_ecm_id);

#endif
    
    while(1)
    {
        status = can_rx(timeout);
        if (status == S_SUCCESS)
        {
            if ((CanMsgFrameStandardAddr(can_rxframe) == response_ecm_id) ||
                (response_ecm_id == OBD2CAN_ANY_ECMID))
            {
                time_mscount = rtc_getvalue();
                
                if (CanMsgFrameData(can_rxframe)[0] < 0x10)
                {
                    //##########################################################
                    //this is single frame
                    //##########################################################
                    if ((CanMsgFrameData(can_rxframe)[1] == 0x7F) &&
                        (CanMsgFrameData(can_rxframe)[2] == rxinfo->cmd))
                    {
                        //this is 7F response
                        if (CanMsgFrameData(can_rxframe)[3] == 0x78)
                        {
                            delays(30,'m'); //TODOQ: remove me
                            //wait
                            if (rxinfo->tester_present_type == TESTER_PRESENT_BROADCAST)
                            {
                                obd2can_testerpresent(TRUE,tx_ecm_id,
                                                      rxinfo->commlevel);
                            }
                            else if (rxinfo->tester_present_type == TESTER_PRESENT_NODE)
                            {
                                obd2can_testerpresent(FALSE,
                                                      tx_ecm_id,
                                                      rxinfo->commlevel);
                            }
                            timeout_mscount = (10*1000)*(u32)rxinfo->busy_wait_extended;
                            time_mscount = rtc_getvalue();
                            tp_mscount = time_mscount + TP_MSCOUNT_ELAPSED;
                            tp_enable = TRUE;
                        }
                        else
                        {
                            //Note: a negative response received
                            rxinfo->errorcode = CanMsgFrameData(can_rxframe)[3];
                            //rxinfo->rxbuffer becomes additional errorcode data (when applicable)
                            bytecount = CanMsgFrameData(can_rxframe)[0];
                            if (bytecount > 3)
                            {
                                rxinfo->rxlength = bytecount-3;
                                memcpy(rxinfo->rxbuffer,
                                   &CanMsgFrameData(can_rxframe)[4],rxinfo->rxlength);
                            }
                            else
                            {
                                rxinfo->rxlength = bytecount;
                                memcpy(rxinfo->rxbuffer, CanMsgFrameData(can_rxframe),bytecount);

                            }
                            //log_push_error_point(0x2001);
                            return S_ERROR;
                        }
                    }
                    else if (CanMsgFrameData(can_rxframe)[1] ==
                             (rxinfo->cmd + rxinfo->response_cmd_offset))
                    {
                        //this is successful response
                        bytecount = CanMsgFrameData(can_rxframe)[0];
                        rxinfo->rxlength = --bytecount;
                        memcpy(rxinfo->rxbuffer,
                               &CanMsgFrameData(can_rxframe)[2],bytecount);
                        //break;
                        return S_SUCCESS;
                    }
#ifdef __DCX_MANUF__
                    else if (CanMsgFrameData(can_rxframe)[0] == 1 &&
                             CanMsgFrameData(can_rxframe)[1] == 0x51)
                    {
                        //When Chrylser ECM's are in reset state
                        gObd2info.state = S_OBD2_STATE_DEAD;
                        return S_ERROR;
                    }
#endif
                    else
                    {
                        //TODOQ: what this? shouldn't happen
                        //while(1);
                    }
                }//if (CanMsgFrameData(can_rxframe)[0] < 0x10)...
                else if (CanMsgFrameData(can_rxframe)[0] < 0x20)
                {
                    //##########################################################
                    //this is 1st frame of multiframe-message
                    //##########################################################
                    bytecount = ((u8)CanMsgFrameData(can_rxframe)[0]&0x0F)<<8;
                    bytecount |= (u8)CanMsgFrameData(can_rxframe)[1];
                    rxinfo->rxlength = bytecount;
                    
                    if (CanMsgFrameData(can_rxframe)[2] ==
                        (rxinfo->cmd + rxinfo->response_cmd_offset))
                    {
                        memcpy(rxinfo->rxbuffer,
                               &CanMsgFrameData(can_rxframe)
                                   [3+rxinfo->first_frame_data_offset],
                               5-rxinfo->first_frame_data_offset);
                        rxbuffer_index = 5-rxinfo->first_frame_data_offset;

                        if (cooldowntime)
                        {
//                            delays(cooldowntime,'u');
                        }
                        
                        flowcontrolframe[1] = rxinfo->flowcontrol_blocksize;
                        flowcontrolframe[2] = rxinfo->flowcontrol_separationTime;
      
                        obd2can_txraw(tx_ecm_id,flowcontrolframe,
                                      sizeof(flowcontrolframe),FALSE);
                        bytecount -= 6;
                        //1byte command + data offset stripped
                        rxinfo->rxlength -= (1+rxinfo->first_frame_data_offset);
                    }
                    else
                    {
                        //picked up response of other(different) cmd
                        //TODOQ: for now, just reject/ignore it
                    }
                }//else if (CanMsgFrameData(can_rxframe)[0] < 0x20)...
                else if (CanMsgFrameData(can_rxframe)[0] < 0x30)
                {
                    //##########################################################
                    //this is sequential frame of multiframe-message
                    //##########################################################
                    if (frameid == CanMsgFrameData(can_rxframe)[0])
                    {
                        if (bytecount > 7)
                        {
                            memcpy(&rxinfo->rxbuffer[rxbuffer_index],
                                   &CanMsgFrameData(can_rxframe)[1],7);
                            bytecount -= 7;
                        }
                        else
                        {
                            memcpy(&rxinfo->rxbuffer[rxbuffer_index],
                                   &CanMsgFrameData(can_rxframe)[1],
                                   bytecount);
                            bytecount = 0;
                            return S_SUCCESS;
                        }

                        rxbuffer_index += 7;
                        frameid++;
                        frameid &= 0x2F;
                    }
                    else
                    {
                        //frame id sequence is incorrect
                        log_push_error_point(0x2002);
                        return S_FAIL;
                    }
                }//else if (CanMsgFrameData(can_rxframe)[0] < 0x30)...
                else
                {
                    //this case should never happens; we don't have any frame
                    //starting with >= 0x30 in this situation (receiving)
                    //there's case of 0x30 but it doesn't belong here; i.e.
                    //also never happens
                }
            }//if ((CanMsgFrameAddr...
            else
            {
                //Note: this is not an important error because
                //there could be messages with unwanted ID get
                //through the filter
                //Just discard these messages
                if (rtc_getvalue() > endtime_mscount)
                {
                    //there're messages but time-out before a message with
                    //desired ID is found
                    log_push_error_point(0x2004);
                    return S_TIMEOUT;
                }
            }
        }//if (status == S_SUCCESS)...
        else
        {
            if ((rtc_getvalue() - time_mscount) > timeout_mscount)
            {
                log_push_error_point(0x2003);
                return S_TIMEOUT;
            }
        }        
        if ((rtc_getvalue() > tp_mscount) && tp_enable)
        {
            if (rxinfo->tester_present_type == TESTER_PRESENT_BROADCAST)
            {
                obd2can_testerpresent(TRUE,tx_ecm_id,
                                      rxinfo->commlevel);
            }
            else if (rxinfo->tester_present_type == TESTER_PRESENT_NODE)
            {
                obd2can_testerpresent(FALSE,tx_ecm_id,
                                      rxinfo->commlevel);
            }
            tp_mscount = rtc_getvalue() + TP_MSCOUNT_ELAPSED;
        }
    }//while(1)...
}

//------------------------------------------------------------------------------
// Input:   u32 response_ecm_id (ecm response address; 0x7E8 ..., 0xFFFF: any)
//          u8 cmd (obd2 command; 0xFF = any)
//          u8 first_frame_data_offset (# bytes ignored in 1st frame of a
//             multiframe-message); at most 5 bytes
//          u8 allow_tester_present;
// Outputs: u8  *rxbuffer (strip length, cmd)
//          u16 *rxlength
//          u8  *errorcode (from 7F response); can be NULL if don't care
// Return:  u8  status
// Engineer: Quyen Leba
//------------------------------------------------------------------------------
u8 obd2can_rxcomplex(u32 response_ecm_id, obd2can_rxinfo *rxinfo)
{
    u8  privdata[200];
    u32 cooldowntime;

    cooldowntime = obd2_getcooldowntime();
    memcpy((char*)privdata,(char*)&response_ecm_id,4);
    memcpy((char*)&privdata[4],(char*)&cooldowntime,4);
    memcpy((char*)&privdata[8],(char*)rxinfo,sizeof(obd2can_rxinfo));
    
    return can_rxcomplex(privdata,sizeof(obd2can_rxinfo)+8,
                         rxinfo->rxbuffer,&rxinfo->rxlength);
}

/**
 *  @brief CAN tx & rx
 *  @param [in]     servicedata
 *  @return Status
 */
u8 obd2can_txrx(Obd2can_ServiceData *servicedata)
{
    obd2can_rxinfo rxinfo;
    u8 status;
    u32 response_id;
    u8 service_frame[8];
    u8 index;
        
    if(servicedata->response_id == NULL)
    {
        response_id = ECM_RESPONSE_ID(servicedata->ecm_id); 
    }
    else
    {
        response_id = servicedata->response_id;
    }

    if (servicedata->broadcast)
    {                                   
        if (servicedata->commlevel == CommLevel_GMLAN)
        {
            //always single frame
            memset(service_frame,obd2can_get_padding_byte(),8);
            service_frame[0] = 0xFE;
            service_frame[2] = (u8)servicedata->service;
            index = 3;
            if (servicedata->subservice != invalidSubServiceNumber)
            {
                service_frame[index++] = (u8)servicedata->subservice;
            }
            if ((servicedata->txdatalength + index) <= 8)
            {
                memcpy(&service_frame[index],
                       servicedata->txdata,servicedata->txdatalength);
            }
            else
            {
                return S_BADCONTENT;
            }
            service_frame[1] = servicedata->txdatalength + index - 2;
            status = obd2can_txraw(0x101, (u8*)service_frame, 8,
                                   servicedata->nowait_cantx);
        }
        else
        {
            //always single frame
            //TODOQ: this
            //return S_NOTSUPPORT;
            //TODOQ: not fully supported; only catch responses with ecm_id for now
            status = obd2can_txcomplex(servicedata->broadcast_id,
                                       servicedata->response_id,
                                       servicedata->service,
                                       servicedata->subservice,
                                       servicedata->service_controldatabuffer,
                                       servicedata->service_controldatalength,
                                       servicedata->txdata,
                                       servicedata->txdatalength,
                                       servicedata->nowait_cantx);
        }
    }
    else
    {

        status = obd2can_txcomplex(servicedata->ecm_id,
                                   servicedata->response_id,
                                   servicedata->service,
                                   servicedata->subservice,
                                   servicedata->service_controldatabuffer,
                                   servicedata->service_controldatalength,
                                   servicedata->txdata,
                                   servicedata->txdatalength,
                                   servicedata->nowait_cantx);
    }
    if (status != S_SUCCESS)
    {
        log_push_error_point(0x2013);
    }
    else if (status == S_SUCCESS && servicedata->response_expected == TRUE)
    {
        obd2can_rxinfo_init(&rxinfo);

        rxinfo.rxbuffer = servicedata->rxdata;
        rxinfo.cmd = (u8)servicedata->service;
        rxinfo.first_frame_data_offset = servicedata->first_frame_data_offset;
        rxinfo.tester_present_type = servicedata->tester_present_type;
        rxinfo.initial_wait_extended = servicedata->initial_wait_extended;
        rxinfo.busy_wait_extended = servicedata->busy_wait_extended;
        rxinfo.commlevel = servicedata->commlevel;
        rxinfo.rx_timeout_ms = servicedata->rx_timeout_ms;      
        rxinfo.flowcontrol_blocksize = servicedata->flowcontrol_blocksize;
        rxinfo.flowcontrol_separationTime = servicedata->flowcontrol_separationTime;
                
        status = obd2can_rx(response_id, &rxinfo);
        
        if (status == S_SUCCESS)
        {
            servicedata->rxdatalength = rxinfo.rxlength;
        }
        else
        {
            servicedata->errorcode = rxinfo.errorcode;
        }
    }

    return status;
}

/**
 *  @brief CAN tx & rx (complex)
 *  @param [in]     servicedata
 *  @return Status
 */
u8 obd2can_txrxcomplex(Obd2can_ServiceData *servicedata)
{
    obd2can_rxinfo rxinfo;
    u8 status;
    u32 response_id;
    
    if(servicedata->response_id == NULL)
    {
        response_id = ECM_RESPONSE_ID(servicedata->ecm_id);
    }
    else
    {
        response_id = servicedata->response_id;
    }
        
    if (servicedata->broadcast)
    {
        //always single frame
        //TODOQ: this
        return S_NOTSUPPORT;
    }
    else
    {
        status = obd2can_txcomplex(servicedata->ecm_id,
                                   servicedata->response_id,
                                   servicedata->service,
                                   servicedata->subservice,
                                   servicedata->service_controldatabuffer,
                                   servicedata->service_controldatalength,
                                   servicedata->txdata,
                                   servicedata->txdatalength,
                                   servicedata->nowait_cantx);
    }
    if (status != S_SUCCESS)
    {
        log_push_error_point(0x2014);
    }
    else if (status == S_SUCCESS && servicedata->response_expected == TRUE)
    {
        obd2can_rxinfo_init(&rxinfo);

        rxinfo.rxbuffer = servicedata->rxdata;
        rxinfo.cmd = (u8)servicedata->service;
        rxinfo.first_frame_data_offset = servicedata->first_frame_data_offset;
        rxinfo.tester_present_type = servicedata->tester_present_type;
        rxinfo.initial_wait_extended = servicedata->initial_wait_extended;
        rxinfo.busy_wait_extended = servicedata->busy_wait_extended;
        rxinfo.commlevel = servicedata->commlevel;
        rxinfo.rx_timeout_ms = servicedata->rx_timeout_ms;
        rxinfo.flowcontrol_blocksize = servicedata->flowcontrol_blocksize;
        rxinfo.flowcontrol_separationTime = servicedata->flowcontrol_separationTime;
        rxinfo.secondary_response_ecm_id = servicedata->ecm_id;
        
        status = obd2can_rxcomplex(response_id, &rxinfo);
        if (status == S_SUCCESS)
        {
            servicedata->rxdatalength = rxinfo.rxlength;
        }
    }

    return status;
}

/**
 *  @brief CAN testerpresent command
 *  @param [in] broadcast,ecm_id,commlevel
 *  @return  Nothing
 */
void obd2can_testerpresent(bool broadcast, u32 ecm_id,
                           VehicleCommLevel commlevel)
{
    switch (obd2_get_oemtype())
    {
#ifdef __DCX_MANUF__        
    case OemType_DCX:
        obd2can_dcx_testerpresent(broadcast,ecm_id,commlevel); 
        break;
#endif
#ifdef __FORD_MANUF__
    case OemType_FORD:
        obd2can_ford_testerpresent(broadcast,ecm_id,commlevel);
        break;
#endif
#ifdef __GM_MANUF__
    case OemType_GM:
        obd2can_gm_testerpresent(broadcast,ecm_id,commlevel); 
        break;
#endif
    default:
        break;
    }     
}

/**
 *  @brief Generic CAN Ping
 *
 *  @param [in] ecm_id
 *  @param [in] fast    Shorter timeout; used for faster COM detection
 *
 *  @return  Status
 */
u8 obd2can_ping(u32 ecm_id, bool fast)
{
    Obd2can_ServiceData servicedata;
    u8 status;

    obd2can_servicedata_init(&servicedata);
    servicedata.ecm_id = ecm_id;
    servicedata.nowait_cantx = TRUE;
    servicedata.service = 0x01;
    servicedata.subservice = 0x00;
    
    if (fast == TRUE)
    {
        servicedata.initial_wait_extended = 0;
        servicedata.rx_timeout_ms = 100;
    }
    else
    {
        servicedata.rx_timeout_ms = 1000;
    }
    
    status = obd2can_txrx(&servicedata);

    if (status == S_SUCCESS || status == S_ERROR)
    {
        status = S_SUCCESS;
    }
    else
    {
#ifdef __GM_MANUF__
        /* GM has a secondary ping. Handle it here. */
        status = obd2can_gm_secondaryping(ecm_id);
#endif
    }
    return status;
}

/**
 *  @brief J1979 UDS Ping
 *
 *  @param [in] ecm_id
 *  @param [in] fast    Shorter timeout; used for faster COM detection
 *
 *  @details This function performs a J1979 UDS ping. If UDS is unsupported,
 *           the function fails.
 *
 *  @return  Status
 */
u8 obd2can_uds_ping(u32 ecm_id)
{
    Obd2can_ServiceData servicedata;
    u8 status;
    
    obd2can_servicedata_init(&servicedata);
    servicedata.ecm_id = ecm_id;
    servicedata.nowait_cantx = TRUE;
    servicedata.initial_wait_extended = 0;
    servicedata.rx_timeout_ms = 100;
    servicedata.service = 0x22;
    servicedata.service_controldatabuffer[0] = 0xF8;
    servicedata.service_controldatabuffer[1] = 0x10;
    servicedata.service_controldatalength = 2;

    status = obd2can_txrx(&servicedata);

    if (status == S_SUCCESS)
    {
        if (servicedata.rxdatalength > 2)
        {
            /* 1 = UDS Services Supported */
            if (servicedata.rxdata[2] == 1)
            {
                status = S_SUCCESS;
            }
        }
        else
        {
            status = S_BADCONTENT;
        }
    }
    return status;    
}

/**
 * @brief	Key on test
 *
 * @details	This function performs a key on test. The test is performed by
 *          checking for a number consecutive successful pings.
 *          If any ping times out, a counter is incremented; when this counter
 *          reaches its target, the test fails indicating no processor is 
 *          present. This function was written to handle the ST 225 during 
 *          recent power up conditions.
 *
 *          Note: This function differs from the obd2_checkkeyon in that it is
 *          only successful when a number of consecutive pings have had a valid
 *          response.
 *
 * @param[in]   ecm_id      Node address
 *
 * @return      status
 *
 */
u8 obd2can_key_on_test(u32 ecm_id)
{
    u32 end;
    u8  target = 60;    /* Target number of consecutive successful pings */
    u8  count = 0;      /* Current number of consecutive successful pings */
    u8  status;
    
    obd2can_init(Obd2canInitProgram,0);
    
    end = rtc_getvalue() + 50000;   /* Allow 50 seconds to test */
    while (rtc_getvalue() < end)
    {
        status = obd2can_ping(ecm_id, FALSE);
        if (status == S_FAIL)
        {
            /* Fail; No Communication (No TX) */
            log_push_error_point(0x2011);
            break;
        }
        else if (status == S_TIMEOUT)
        {
            count = 0;
        }
        else if (status == S_SUCCESS)
        {
            if(count++ == target)
            {
                break;
            }
        }
        else
        {
            /* Shouldn't get here */
        }
    }
    
    if (count >= target)
    {
        status = S_SUCCESS;
    }
    else
    {
        log_push_error_point(0x2012);
        status = S_FAIL;
    }
    
    return status;
}

//------------------------------------------------------------------------------
// Inputs:  u32 ecm_id (NOT_USED)
//          u32 ecm_response_id (NOT_USED)
//          u32 address
//          u32 length (multiple of 4)
//          MemoryAddressType addrtype
// Outputs: u8  *data
//          u16 *datalength
// Engineer: Quyen Leba, Ricardo Cigarroa
//------------------------------------------------------------------------------
u8 obd2can_read_memory_address_by20extendedid(u32 ecm_id,
                                              u32 ecm_response_id,
                                              u32 address, u32 length,
                                              MemoryAddressType addrtype,
                                              u8 *data, u16 *datalength)
{
    u8 status; 
    
    switch (obd2_get_oemtype())
    {
#ifdef __DCX_MANUF__        
    case OemType_DCX:
        status = S_NOTSUPPORT;
        break;
#endif
#ifdef __FORD_MANUF__
    case OemType_FORD:
        status = S_NOTSUPPORT;
        break;
#endif
#ifdef __GM_MANUF__
    case OemType_GM:
        status = obd2can_gm_read_memory_address_by20extendedid(ecm_id,
                                                               ecm_response_id,
                                                               address,length,
                                                               addrtype,data,
                                                               datalength);
        break;
#endif
    default:
        status = S_NOTSUPPORT;
        break;
    }  
        
    return status;
}

//------------------------------------------------------------------------------
// Read Data by PID (by SAE PID)
// Inputs:  u32 ecm_id
//          u8  pid
// Outputs: u8  *data
//          u8  *datalength
// Return:  u8  status
// Engineer: Tristen Pierson
//------------------------------------------------------------------------------
u8 obd2can_read_data_bypid_mode1(bool broadcast, u32 ecm_id, u8 pid,
                                 u8 *data, u8 *datalength, bool response_expected)
{
    Obd2can_ServiceData servicedata;
    u8 status;

    if (response_expected == TRUE && (data == NULL || datalength == NULL))
    {
        log_push_error_point(0x200F);
        return S_INPUT;
    }    

    obd2can_servicedata_init(&servicedata);
    // Some processors such as LMM or LML only respond to mode $01 via 0x7DF
    if (broadcast == TRUE)
    {
        servicedata.broadcast_id = 0x7DF;
        servicedata.broadcast = TRUE;
        servicedata.commlevel = CommLevel_Unknown;  // This bypasses GMLAN 0xFE broadcast
    }
    servicedata.ecm_id = ecm_id;
    servicedata.nowait_cantx = FALSE;
    servicedata.busy_wait_extended = 0;
    servicedata.rx_timeout_ms = 100;
    servicedata.service = 0x01;
    servicedata.service_controldatabuffer[0] = pid;
    servicedata.service_controldatalength = 1;
    
    // Some ECU's require delay between requests
    delays(5, 'm');
    
    if (response_expected == FALSE)
    {    
        servicedata.response_expected = FALSE;
        return obd2can_txrx(&servicedata);
    }
    status = obd2can_txrx(&servicedata);
    
    *datalength = 0;
    if (status == S_SUCCESS)
    {
        if (servicedata.rxdatalength > 1)
        {
            *datalength = servicedata.rxdatalength - 1;
            memcpy((char*)data,(char*)&servicedata.rxdata[1],*datalength);
            return S_SUCCESS;
        }
        else
        {
            log_push_error_point(0x200E);
            return S_BADCONTENT;
        }
    }
    return status;
}

//------------------------------------------------------------------------------
// ReadByLocalIdentifier ($21)
// Inputs:  u32 ecm_id
//          u8  identifier
// Outputs: u8  *data
//          u16 *datalength
// Return:  u8  status
// Engineer: Ricardo Cigarroa
//------------------------------------------------------------------------------
u8 obd2can_read_local_identifier(u32 ecm_id, u8 identifier, u8 *data,
                                     u16 *datalength)
{
    Obd2can_ServiceData servicedata;
    u8  status;
        
     *datalength = 0;
       
    obd2can_servicedata_init(&servicedata);    
    servicedata.ecm_id = ecm_id;
    servicedata.service = 0x21;   
    servicedata.service_controldatabuffer[0] = identifier;    
    servicedata.service_controldatalength = 1;    
    
    servicedata.rxdata = data;
    servicedata.response_expected = TRUE;
    
    status = obd2can_txrxcomplex(&servicedata);
    
    if (status == S_SUCCESS)
    {
        *datalength = servicedata.rxdatalength;
    }
    
    return status;
}

//------------------------------------------------------------------------------
// Read Data by PID (by ParameterId; by CommonId)
// Inputs:  u32 ecm_id
//          u16 common_id
// Outputs: u8  *data
//          u8  *datalength
// Return:  u8  status
// Engineer: Quyen Leba
//------------------------------------------------------------------------------
u8 obd2can_read_data_bypid(u32 ecm_id, u16 pid, u8 *data, u8 *datalength,
                           bool response_expected)
{
    Obd2can_ServiceData servicedata;
    u8 status;

    if ((data == NULL || datalength == NULL) && response_expected == TRUE)
    {
        log_push_error_point(0x2006);
        return S_INPUT;
    }
    
    obd2can_servicedata_init(&servicedata);
    servicedata.ecm_id = ecm_id;
    servicedata.nowait_cantx = TRUE;
    servicedata.service = 0x22;
    servicedata.service_controldatabuffer[0] = (u8)(pid >> 8);
    servicedata.service_controldatabuffer[1] = (u8)(pid & 0xFF);
    servicedata.service_controldatalength = 2;
    if (response_expected == FALSE)
    {        
        servicedata.response_expected = FALSE;
        return obd2can_txrx(&servicedata);
    }
    status = obd2can_txrx(&servicedata);
    
    //example:
    //7E0 03 22 [D1 00]
    //7E8 04 62 [D1 00] [81]
    *datalength = 0;
    if (status == S_SUCCESS)
    {
        if (servicedata.rxdatalength > 2)
        {
            *datalength = servicedata.rxdatalength - 2;
            memcpy((char*)data,(char*)&servicedata.rxdata[2],*datalength);
            status = S_SUCCESS;
        }
        else
        {
            log_push_error_point(0x2005);
            status = S_BADCONTENT;
        }
    }
    return status;
}

//------------------------------------------------------------------------------
// Write Data by PID (by ParameterId; by CommonId)
// Inputs:  u32 ecm_id
//          u16 common_id
//          u8  *data
//          u8  *datalength
// Return:  u8  status
// Engineer: Patrick downs
//------------------------------------------------------------------------------
u8 obd2can_write_data_bypid(u32 ecm_id, u16 pid, u8 *data, u16 datalength)
{
    Obd2can_ServiceData servicedata;
    u8 status;

    if (data == NULL)
    {
        log_push_error_point(0x2015);
        return S_INPUT;
    }
    
    //example:
    //726 07 2E [DE 15] [01 04 2D 41] - Writes new values
    //72E 03 6E [DE 15] 
    
    obd2can_servicedata_init(&servicedata);
    servicedata.ecm_id = ecm_id;
    servicedata.nowait_cantx = TRUE;
    servicedata.service = 0x2E;
    servicedata.service_controldatabuffer[0] = (u8)(pid >> 8);
    servicedata.service_controldatabuffer[1] = (u8)(pid & 0xFF);    
    servicedata.service_controldatalength = 2;
    
    servicedata.txdata = data;
    servicedata.txdatalength = datalength;
    servicedata.response_expected = TRUE;
    
    status = obd2can_txrx(&servicedata);
    
    return status;
}

/**
 *  @brief Validate CAN PID
 *
 *  @param [in]     ecm_id      ECM ID (CAN only)
 *  @param [in]     *dlxblock   DLX data block to validate
 *  @param [in]     commlevel   Vehicle comm level
 *
 *  @return Status
 */
u8 obd2can_validatepid(u32 ecm_id, DlxBlock *dlxblock, VehicleCommLevel commlevel)
{
    u8 status = S_SUCCESS;

    obd2_open();

    if (obd2_findvehiclecommtype() == CommType_CAN)
    {
        switch (gObd2info.oemtype)
        {
#ifdef __DCX_MANUF__        
        case OemType_DCX:
            if (commlevel == CommLevel_KWP2000)
            {
                status = obd2can_dcx_validateByRapidPacketSetup(ecm_id, 
                                                                dlxblock, 
                                                                commlevel);
            }
            else
            {
                status = S_COMMLEVEL;
            }
            break;
#endif
#ifdef __FORD_MANUF__
        case OemType_FORD:
            if (commlevel == CommLevel_ISO14229 ||
                commlevel == CommLevel_KWP2000)
            {
                u8 buffer[DATALOG_MAX_PID_SIZE];
                u8 bufferlength;
                
                status = obd2can_read_data_bypid(ecm_id, (u16)dlxblock->pidAddr,
                                                 buffer, &bufferlength, TRUE);
            }
            else
            {
                status = S_COMMLEVEL;
            }
            break;
#endif
#ifdef __GM_MANUF__
        case OemType_GM:
            if (commlevel == CommLevel_GMLAN)
            {
                status = obd2can_gm_validateByRapidPacketSetup(ecm_id, 
                                                               dlxblock, 
                                                               commlevel);
            }
            else
            {
                status = S_COMMLEVEL;
            }
            break;
#endif
        default:
            status = S_COMMLEVEL;
            break;
        } /* switch(..) */
    } /* if (commtype == CommType_CAN) */
    else
    {
        status = S_COMMTYPE;
    }
    return status;
}

/**
 *  @brief Validate CAN PID
 *
 *  @param [in]     ecm_id      ECM ID (CAN only)
 *  @param [in]     *dlxblock   DLX data block to validate
 *  @param [in]     commlevel   Vehicle comm level
 *
 *  @return Status
 */
u8 obd2can_validatedmr(u32 ecm_id, DlxBlock *dlxblock, VehicleCommLevel commlevel)
{
    u8 status = S_SUCCESS;

    obd2_open();

    if (obd2_findvehiclecommtype() == CommType_CAN)
    {
        switch (gObd2info.oemtype)
        {
#ifdef __DCX_MANUF__        
        case OemType_DCX:
            if (commlevel == CommLevel_KWP2000)
            {
                status = obd2can_dcx_validatedmr(ecm_id, dlxblock, commlevel);
            }
            else
            {
                status = S_COMMTYPE;
            }
            break;
#endif
#ifdef __FORD_MANUF__
        case OemType_FORD:
            if (commlevel == CommLevel_ISO14229 ||
                commlevel == CommLevel_KWP2000)
            {
                status = obd2can_ford_validatedmr(ecm_id, dlxblock, commlevel);
            }
            else
            {
                status = S_COMMLEVEL;
            }
            break;
#endif
#ifdef __GM_MANUF__
        case OemType_GM:
            if (commlevel == CommLevel_GMLAN)
            {
                status = obd2can_gm_validatedmr(ecm_id, dlxblock, commlevel);
            }
            else
            {
                status = S_COMMLEVEL;
            }
            break;
#endif
        default:
            status = S_COMMLEVEL;
            break;
        } /* switch(..) */
    } /* if (commtype == CommType_CAN) */
    else
    {
        status = S_COMMTYPE;
    }
    return status;
}

/**
 *  @brief Validate CAN PID
 *
 *  @param [in]     ecm_id      ECM ID (CAN only)
 *  @param [in]     *dlxblock   DLX data block to validate
 *  @param [in]     commlevel   Vehicle comm level
 *
 *  @return Status
 */
u8 obd2can_validateByRapidPacketSetup(u32 ecm_id, DlxBlock *dlxblock, 
                                      VehicleCommLevel commlevel)
{
    u8 status = S_SUCCESS;

    obd2_open();

    if (obd2_findvehiclecommtype() == CommType_CAN)
    {
        switch (gObd2info.oemtype)
        {
#ifdef __DCX_MANUF__        
        case OemType_DCX:
            status = obd2can_dcx_validateByRapidPacketSetup(ecm_id, dlxblock, commlevel);
            break;
#endif
#ifdef __FORD_MANUF__
        case OemType_FORD:
            status = obd2can_ford_validateByRapidPacketSetup(ecm_id, dlxblock, commlevel);
            break;
#endif
#ifdef __GM_MANUF__
        case OemType_GM:
            if (commlevel == CommLevel_GMLAN)
            {
                status = obd2can_gm_validateByRapidPacketSetup(ecm_id, dlxblock, commlevel);
            }
            else
            {
                status = S_COMMLEVEL;
            }
            break;
#endif
        default:
            status = S_COMMLEVEL;
            break;
        } /* switch(..) */
    } /* if (commtype == CommType_CAN) */
    else
    {
        status = S_COMMTYPE;
    }
    return status;
}

/**
 *  @brief Validate CAN OSC PID
 *
 *  @param [in]     ecm_id      ECM ID (CAN only)
 *  @param [in]     *dlxblock   DLX data block to validate
 *  @param [in]     commlevel   Vehicle comm level
 *
 *  @return Status
 */
u8 obd2can_validate_oscpid(u32 ecm_id, DlxBlock *dlxblock, VehicleCommLevel commlevel)
{
    u8 status = S_SUCCESS;

    obd2_open();

    if (obd2_findvehiclecommtype() == CommType_CAN)
    {
        switch (gObd2info.oemtype)
        {
#ifdef __FORD_MANUF__
        case OemType_FORD:
            if (!SETTINGS_IsDemoMode())
            {
                status = obd2can_ford_validate_oscpid(ecm_id, dlxblock, commlevel);
            }
            break;
#endif
#ifdef __GM_MANUF__
        case OemType_GM:
            if (!SETTINGS_IsDemoMode())
            {
                status = obd2can_gm_validate_oscpid(ecm_id, dlxblock, commlevel);
            }
            break;
#endif
        default:
            status = S_NOTSUPPORT;
            break;
        }
    }
    else
    {
        status = S_COMMTYPE;
    }
    return status;
}

/**
 *  @brief Read DTCs using standard service (01 & 03 from J1979)
 *  
 *  @param [in]     ecm_id  ECU identifier
 *  @param [out]    info    DTC Info
 *
 *  @return Status
 *
 *  @details This function reads DTCs using standard OBD2 services ($01 and $03
 *           from J1979). If the OBD2 method fails (e.g. some Ford Chinese
 *           market vehicles do not support OBD2 commands), this function
 *           attempts an alternate function.
 */
u8 obd2can_readdtc(u32 ecm_id, dtc_info *info)
{
    u8 (*fpObd2can_alt_readdtc)(u32 ecm_id, dtc_info *info);
    u8 status;
    
    fpObd2can_alt_readdtc = NULL;
    
    /* Get alternate DTC function by OEM type */
    switch (gObd2info.oemtype)
    {
#ifdef __DCX_MANUF__
    case OemType_DCX:
        status = obd2can_dcx_readdtc_all(info);
        break;
#endif
#ifdef __FORD_MANUF__
    case OemType_FORD:
        fpObd2can_alt_readdtc = &obd2can_ford_alt_readdtc;
        break;
#endif
#ifdef __GM_MANUF__
    case OemType_GM:
        status = obd2can_generic_readdtc(ecm_id, info);
        break;
#endif
    default:
        fpObd2can_alt_readdtc = NULL;
        break;
    }
    
    if (fpObd2can_alt_readdtc != NULL)
    {
        status = (*fpObd2can_alt_readdtc)(ecm_id, info);
        if (status != S_SUCCESS)
        {
            status = obd2can_generic_readdtc(ecm_id, info);
        }
    }
    
    return status;
}

//------------------------------------------------------------------------------
// Disable all normal communication.
// Input:   None
// Return:  u8  status
// Engineer: Ricardo Cigarroa
//------------------------------------------------------------------------------
u8 obd2can_disableallnormalcomm()
{
    u8 status;

    switch (obd2_get_oemtype())
    {
#ifdef __DCX_MANUF__        
    case OemType_DCX:
        status = S_SUCCESS; 
        break;
#endif
#ifdef __FORD_MANUF__
    case OemType_FORD:
        status = S_SUCCESS;
        break;
#endif
#ifdef __GM_MANUF__
    case OemType_GM:
        status = obd2can_gm_disableallnormalcomm();
        break;
#endif
    default:
        status = S_NOTSUPPORT;
        break;
    }     
    return status;
}

//------------------------------------------------------------------------------
// Read VIN (using $23 search to find out indirect VIN overlay)
// Output:  u8  *vin
// Return:  u8  status
// Engineer: Quyen Leba
//------------------------------------------------------------------------------
u8 obd2can_readvin_indirect_blankvin_overlay(u8 *vin)
{
    u32 vin_list[VIN_LIST_CAN_COUNT] = {VIN_LIST_CAN};
    u8  buffer[32];
    u16 bufferlength;
    u16 i;
    u8  status;
    
    for (i = 0; i < sizeof(vin_list)/4; i++)
    {
        /* So far address is always 32bits. */
        status = obd2can_read_memory_address(gObd2info.ecu_block[0].ecu_id,vin_list[i], VIN_LENGTH,
                                                 Address_32_Bits, DEFAULT_CMD_MEMORY_SIZE, buffer, &bufferlength,
                                                 TRUE, gObd2info.ecu_block[0].commlevel);
        buffer[VIN_LENGTH] = NULL;
        if (status == S_SUCCESS &&
            obd2_validatevin(buffer) == S_SUCCESS &&
            memcmp((char*)buffer,"SCT",3) == 0)
        {
            memcpy((char*)vin,buffer,VIN_LENGTH);
            vin[VIN_LENGTH] = NULL;
            return S_SUCCESS;
        }
        
    }
    return S_FAIL;
}

//------------------------------------------------------------------------------
// Read VIN
// Output:  u8  *vin
// Return:  u8  status
// Engineer: Quyen Leba
//------------------------------------------------------------------------------
u8 obd2can_readvin(u8 *vin)
{
    Obd2can_ServiceData servicedata;
    u8 status;

    obd2can_servicedata_init(&servicedata);
    servicedata.nowait_cantx = TRUE;
    servicedata.ecm_id = 0x7E0;
    servicedata.broadcast_id = 0x7DF;
    servicedata.service = 0x09;
    servicedata.subservice = 0x02;
    servicedata.first_frame_data_offset = 2;

    // Some ECU's require delay between requests
    delays(5, 'm');
    status = obd2can_txrx(&servicedata);
    
    vin[0] = NULL;
    if (status == S_SUCCESS)
    {
        if (servicedata.rxdatalength == VIN_LENGTH)
        {
            memcpy(vin,servicedata.rxdata,VIN_LENGTH);
            vin[VIN_LENGTH] = NULL;
            if (obd2_validatevin(vin) == S_VINBLANK)
            {
                if (obd2can_readvin_indirect_blankvin_overlay(vin) == S_SUCCESS)
                {
                    status = S_SUCCESS;
                }
            }
        }
        else
        {
            status = S_BADCONTENT;
        }
    }

    return status;
}

/**
 *  @brief Get CAL ID
 *  
 *  @param [in]  ecm_id     Node ID
 *  @param [in]  calid      CAL ID Data
 *
 *  @return status
 */
u8 obd2can_read_calid(u32 ecm_id, u8 *calid)
{
    Obd2can_ServiceData servicedata;
    u8 i, validcount;
    u8 status;

    obd2can_servicedata_init(&servicedata);
    servicedata.nowait_cantx = TRUE;
    servicedata.ecm_id = ecm_id;
    servicedata.service = 0x09;
    servicedata.subservice = 0x04;
    servicedata.first_frame_data_offset = 2;

    // Some ECU's require delay between requests
    delays(5, 'm');
    status = obd2can_txrx(&servicedata);
    
    memset(calid,0,CALID_LENGTH);
    if (status == S_SUCCESS)
    {        
        for(i=0;i<CALID_LENGTH;i++)
        {
            if (isgraph(servicedata.rxdata[i]))
                validcount++;
            else
                break;
        }
        if (validcount >= 4)
        {
            memcpy(calid,servicedata.rxdata,CALID_LENGTH);
            status = S_SUCCESS;
        }
        else
        {
            status = S_STRATEGY;
        }
    }
    return status;
}
//------------------------------------------------------------------------------
// Reset ECM
// Input:   u32 ecm_id
// Return:  u8  status
// Engineer: Quyen Leba
//------------------------------------------------------------------------------
#ifndef OBD2CAN_RESETECM_DEFINED
u8 obd2can_resetecm(u32 ecm_id)
{
    Obd2can_ServiceData servicedata;
    u8 status;

    obd2can_servicedata_init(&servicedata);
    servicedata.ecm_id = ecm_id;
    servicedata.service = 0x11;
    servicedata.subservice = 0x01;
    status = obd2can_txrx(&servicedata);
    
    // Ford CAN spec says PCM is allowed 750ms re-init time after reset...
    delays(750,'m');
    
    return status;
}
#endif

//------------------------------------------------------------------------------
//TODOQ: check why need to do this? (and only do to 1st ecm)
//from GM, probably not work for Ford, move this
//------------------------------------------------------------------------------
#ifndef OBD2CAN_RESETFUELTRIM_DEFINED
u8 obd2can_resetfueltrim(u32 ecm_id)
{
    Obd2can_ServiceData servicedata;
    const u8 subdata[5] = {0x04,0x00,0x00,0x00,0x00};
    u8 status;

    //TODOQ: what are these tester present for?
    obd2can_testerpresent(FALSE,ecm_id,CommLevel_Default);
    delays(200,'m');
    obd2can_testerpresent(FALSE,ecm_id,CommLevel_Default);
    delays(300,'m');
    
    obd2can_servicedata_init(&servicedata);
    servicedata.ecm_id = ecm_id;
    servicedata.service = 0xAE;
    servicedata.subservice = 0x18;
    memcpy(servicedata.service_controldatabuffer,subdata,sizeof(subdata));
    servicedata.service_controldatalength = sizeof(subdata);
    status = obd2can_txrx(&servicedata);

    return status;
}
#endif

/**
 *  @brief Get ecm unlock algorithm
 *
 *  @param [in]     seed        Unlock seed
 *  @param [in]     algoindex   Algorithm index
 *  @param [in]     commtype    Vehicle comm type
 *  @param [out]    key         Generated key
 *
 *  @return Status
 */
u8 obd2can_getecmunlockalgorithm(u8 *seed, u32 algoindex, VehicleCommType commtype,
                                 u8 *key)
{
    u8 status;

    switch (gObd2info.oemtype)
    {
#ifdef __DCX_MANUF__        
    case OemType_DCX:
        status = obd2_dcx_generatekey(seed, algoindex, commtype, key);
        break;
#endif
#ifdef __FORD_MANUF__
    case OemType_FORD:
        status = obd2_ford_generatekey(seed, algoindex, commtype, key);
        break;
#endif
#ifdef __GM_MANUF__
    case OemType_GM:
        status = obd2_gm_generatekey(seed, algoindex, commtype, key);
        break;
#endif
    default:
        status = S_NOTSUPPORT;
        break;
    }
    return status; 
}

//------------------------------------------------------------------------------
// Unlock CAN ecm
// Input:   u16 ecm_type (defined for ecm_defs)
//          bool seed_peek (only get seed, don't send unlock key)
// Return:  u8  status
// Engineer: Quyen Leba
//------------------------------------------------------------------------------
#ifndef OBD2CAN_UNLOCKECM_DEFINED
u8 obd2can_unlockecm(UnlockTask unlocktask, u16 ecm_type, bool seed_peek)
{
    u32 ecm_id;
    u32 algoindex;
    u8  status;
    u8  ecmseed[8];
    u8  ecmseedlength;
    u8  ecmkey[8];
    u8  ecmkeylength;

    ecm_id = ECM_GetEcmId(ecm_type);
    switch(unlocktask)
    {
    case UnlockTask_Upload:
        algoindex = ECM_GetUnlockIndex(ecm_type);
        break;
    case UnlockTask_Download:
        algoindex = ECM_GetDownloadUnlockIndex(ecm_type);
        break;
    default:
        return S_INPUT;
    }

    if (algoindex == UnlockId_None)
    {
        return S_SUCCESS;
    }
    
    memset(ecmseed,0,sizeof(ecmseed));
    status = obd2can_security_access(ecm_id,SPSrequestSeed, 
                                     ecmseed,&ecmseedlength,
                                     algoindex);
    if (status == S_SUCCESS && ecmseedlength > 0)
    {
        if (seed_peek == FALSE)
        {
            if (ecmseed[0] == 0 && ecmseed[1] == 0 && ecmseed[2] == 0)
            {
                //already unlocked
                return S_SUCCESS;
            }
            status = obd2can_getecmunlockalgorithm(ecmseed,algoindex,
                                                   CommType_CAN,ecmkey);
            if (status == S_SUCCESS)
            {
                ecmkeylength = ecmseedlength;
                status = obd2can_security_access(ecm_id,SPSsendKey,
                                                 ecmkey,&ecmkeylength,
                                                 algoindex);
            }
        }//if (seed_peek == FALSE)...
    }
    else if (status == S_ERROR)
    {
        log_push_error_point(0x200D);
    }
    else
    {
        log_push_error_point(0x2008);
        status = S_FAIL;
    }
    return status;
}
#endif

//------------------------------------------------------------------------------
// Test if ignition key is on using download related command
// Input:   u16 ecm_type
// Return:  u8  status
// Engineer: Quyen Leba
//------------------------------------------------------------------------------
u8 obd2can_download_test_key_on(u16 ecm_type)
{
    u8  status;

    status = obd2can_unlockecm(UnlockTask_Download, ecm_type, TRUE);    //peek only
    if (status == S_ERROR)
    {
        //responded to unlock
        status = S_SUCCESS;
    }
    return status;
}

//------------------------------------------------------------------------------
// Read DTC using standard service (01 & 03 from J1979)
// For GM, DTC can also be read using service A9
// Input:   u32 ecm_id
// Output:  dtc_info *info
// Return:  u8  status
// Engineer: Quyen Leba
//------------------------------------------------------------------------------
u8 obd2can_generic_readdtc(u32 ecm_id, dtc_info *info)
{
    Obd2can_ServiceData servicedata;
    u8 status;
    u8 i;
    u8 dtccount;

    obd2can_servicedata_init(&servicedata);
    servicedata.commlevel = CommLevel_Unknown;
    servicedata.ecm_id = ecm_id;    
    servicedata.broadcast = TRUE;
    servicedata.broadcast_id = 0x7DF;
    servicedata.service = 0x01;
    servicedata.subservice = 0x01;
    
    // Some ECU's require delay between requests
    delays(5, 'm');
    status = obd2can_txrx(&servicedata);
    
    if (status == S_SUCCESS)
    {
        dtccount = obd2_rxbuffer[1] & 0x7F;  //bit 7: MILL ON (1)
        info->count = dtccount;
        if (dtccount)
        {
            servicedata.service = 0x03;
            servicedata.subservice = 0xFF;
            
            // Some ECU's require delay between requests
            delays(5, 'm');
            status = obd2can_txrx(&servicedata);
            if (status == S_SUCCESS && obd2_rxbuffer[0] == dtccount)
            {
                for(i=0;i<dtccount;i++)
                {
                    info->codes[i] = (u16)obd2_rxbuffer[1+i*2] << 8;
                    info->codes[i] |= (u16)obd2_rxbuffer[2+i*2];
                }
            }
            else
            {
                status = S_FAIL;
            }
        }
    }
    return status;
}

//------------------------------------------------------------------------------
// Clear DTC using standard service (04 from J1979)
// Input:   u32 ecm_id
// Return:  u8  status
// Engineer: Tristen Pierson
//------------------------------------------------------------------------------
u8 obd2can_generic_cleardtc(u32 ecm_id)
{
    Obd2can_ServiceData servicedata;
    
    obd2can_servicedata_init(&servicedata);
    servicedata.commlevel = CommLevel_Unknown;
    servicedata.ecm_id = ecm_id;
    servicedata.broadcast = TRUE;
    servicedata.broadcast_id = 0x7DF;
    servicedata.service = 0x04;
    
    return obd2can_txrx(&servicedata);
}

/**
 *  @brief Get CAN OBD2 Info
 *  
 *  @param [in] obd2info OBD2 info structure
 *  @return status
 *  
 *  @details This function identifies and returns if applicable CAN information
 *           to the obd2info structure.
 */
u8 obd2can_getinfo(obd2_info *obd2info)
{
    u8 vin[VIN_LENGTH+1];
    GenericCommLevel genericcommlevel;
    OemType tempOem;
    u8  status;
    
    /*****************************************/
    /* Add other ECU ID's here to search for */
    /*****************************************/
    Obd2CanEcuId ecu_id[3] = {Obd2CanEcuId_7E0, 
                              Obd2CanEcuId_7E1,
                              Obd2CanEcuId_7E2};
    /*****************************************/

    if (obd2info == NULL)
    {
        return S_INPUT;
    }
    
    obd2can_init(Obd2canInitProgram,0);
    
    /* Get vehicle information */
    for (u8 i = 0; i < sizeof(ecu_id)/sizeof(Obd2CanEcuId); i++)
    {
        genericcommlevel = GenericCommLevel_Legacy;
        
        /* Check for J1979 UDS support */
        status = obd2can_uds_ping(ecu_id[i]);
        if (status == S_SUCCESS)
        {
            genericcommlevel = GenericCommLevel_UDS;
        }
        else
        {
            /* Check for generic J1979 support */
            delays(5, 'm');
            if (i == 0)
            {
                /* Slower timeout to check for reset messages (e.g. $51) */
                status = obd2can_ping(ecu_id[i], FALSE);
            }
            else
            {
                status = obd2can_ping(ecu_id[i], TRUE);
            }
        }
        if (status == S_FAIL)
        {
            /* Fail; No Communication (No TX) */
            log_push_error_point(0x2010);
            break;
        }
        if (status == S_SUCCESS)
        {            
            /* Special case SCI + CAN, only allow 0x7E0, skip others */
            if (obd2info->ecu_block[0].commtype != CommType_CAN &&
                ecu_id[i] != Obd2CanEcuId_7E0)
            {
                continue;
            }
            
            /* Main OBD2 Info */
            obd2can_getcommlevelinfo(ecu_id[i], 
                                     &obd2info->ecu_block[obd2info->ecu_count].commlevel, 
                                     &tempOem);

            /* Read only if primary ECU */
            if (obd2info->ecu_count == 0)
            {
                switch (tempOem)
                {
#ifdef __DCX_MANUF__
                case OemType_DCX:
                    obd2can_readvin(vin);
                    break;
#endif
#ifdef __FORD_MANUF__
                case OemType_FORD:
                    obd2can_ford_readvin(vin);
                    break;
#endif
#ifdef __GM_MANUF__
                case OemType_GM:
                    obd2can_gm_readvin(vin);
                    break;
#endif
                default:
                    obd2can_readvin(vin);
                    break;
                }
                memcpy(obd2info->vin, vin, sizeof(vin));
                obd2info->oemtype = tempOem;
            }
            
            /* OBD2 Info Block */
            obd2info->ecu_block[obd2info->ecu_count].commtype = CommType_CAN;
            obd2info->ecu_block[obd2info->ecu_count].genericcommlevel = genericcommlevel;
            obd2info->ecu_block[obd2info->ecu_count].ecu_id = ecu_id[i];
            
            switch (ecu_id[i])
            {
            case Obd2CanEcuId_7E0:
                obd2info->ecu_block[obd2info->ecu_count].ecutype = EcuType_ECM;
                obd2info->ecu_block[obd2info->ecu_count].ecu_response_id = ECM_RESPONSE_ID(ecu_id[i]);
                break;
            case Obd2CanEcuId_7E1:
            case Obd2CanEcuId_7E2:
                obd2info->ecu_block[obd2info->ecu_count].ecutype = EcuType_TCM;
                obd2info->ecu_block[obd2info->ecu_count].ecu_response_id = ECM_RESPONSE_ID(ecu_id[i]);
                break;
            }
            
            /* OBD2 Part Numbers */
            switch (obd2info->oemtype)
            {
#ifdef __DCX_MANUF__
            case OemType_DCX:
                obd2can_dcx_getinfo(&obd2info->ecu_block[obd2info->ecu_count]);
                break;
#endif
#ifdef __FORD_MANUF__
            case OemType_FORD:
                obd2can_ford_getinfo(&obd2info->ecu_block[obd2info->ecu_count]);
                break;
#endif
#ifdef __GM_MANUF__
            case OemType_GM:
                obd2can_gm_getinfo(&obd2info->ecu_block[obd2info->ecu_count]);
                break;
#endif
            default:
                if (obd2can_read_calid(obd2info->ecu_block[obd2info->ecu_count].ecu_id,
                                       obd2info->ecu_block[obd2info->ecu_count].partnumbers.generic.strategy) == S_SUCCESS)
                {
                    obd2info->ecu_block[obd2info->ecu_count].partnumber_count = 1;
                }
                break;
            }
            /* Only allow ECM_MAX_COUNT ecu's to be detected */
            if (++obd2info->ecu_count >= ECM_MAX_COUNT)
            {
                break;
            }
        } 
    } /* for (..) */
    
    obd2can_init(Obd2canInitNormal,0);
    
    return status;
}

/**
 * @brief   Find commlevels and OEM
 *
 * @param [in] pcm_id       Node ID
 * @param [out] commlevel   Vehicle comm level
 * @param [out] oemtype     Vehicle OEM Type
 *
 * @retval  u8 status
 *
 * @author  Tristen Pierson
 *
 * @details This function tests for commlevel by using services only certain
 *          OEM commlevels will respond to.
 */
u8 obd2can_getcommlevelinfo(u32 pcm_id, VehicleCommLevel *commlevel, OemType *oemtype)
{
    Obd2can_ServiceData servicedata;
    u8 status;
    
    /*------------------------------------------------------
     * OEM: GM
     * Commlevel: GMLAN
     * Detection: Tester Present (Response is Mandatory)
     * 
     * Note: GMLAN must be detected before attempting to 
     *       detect Ford ISO14229 via Tester Present.
     *------------------------------------------------------*/
    obd2can_servicedata_init(&servicedata);
    servicedata.ecm_id = pcm_id;    
    servicedata.service = 0x3E;
    servicedata.rx_timeout_ms = 250;
    servicedata.initial_wait_extended = 0;
    status = obd2can_txrx(&servicedata);
    if (status == S_SUCCESS)
    {
        *commlevel = CommLevel_GMLAN;
        *oemtype = OemType_GM;
    }
    
    /*------------------------------------------------------*
     * OEM: FORD
     * Commlevel: ISO14229
     * Detection: Tester Present with Response Required
     *
     * Note: This service also works for some GM processors
     *       (LMM, etc.). Must detect GMLAN before ISO14229.
     *------------------------------------------------------*/
    if (status != S_SUCCESS)
    {
        obd2can_servicedata_init(&servicedata);
        servicedata.ecm_id = pcm_id;
        servicedata.service = 0x3E;     /* Tester Present */
        servicedata.subservice = 0x00;  /* Response Required */
        servicedata.rx_timeout_ms = 250;
        servicedata.initial_wait_extended = 0;
        status = obd2can_txrx(&servicedata);
        if (status == S_SUCCESS)
        {
            *commlevel = CommLevel_ISO14229;
            *oemtype = OemType_FORD;
        }
    }
    
    /*------------------------------------------------------
     * OEM: DCX
     * Commlevel: KWP2000
     * Detection: Vehicle Information (LID = $E5)
     *
     * Note: DCX must be detected before attempting to 
     *       detect Ford KWP2000 via Tester Present.
     *------------------------------------------------------*/
    if (status != S_SUCCESS)
    {
        obd2can_servicedata_init(&servicedata);
        servicedata.ecm_id = pcm_id;        
        servicedata.service = 0x21;     /* Read Data By Local ID (LID) */
        servicedata.subservice = 0xE5;  /* LID = Vehicle Information */
        servicedata.rx_timeout_ms = 250;
        servicedata.initial_wait_extended = 0;
        status = obd2can_txrx(&servicedata);
        if (status == S_SUCCESS && servicedata.rxdata[0] == 0xE5)
        {
            *commlevel = CommLevel_KWP2000;
            *oemtype = OemType_DCX;
        }
#ifdef __DCX_MANUF__
        else if (gObd2info.state == S_OBD2_STATE_DEAD)
        {
            /* If ECM is in reset state ($51) */
            *commlevel = CommLevel_KWP2000;
            *oemtype = OemType_DCX;
            status = S_SUCCESS;
        }
#endif
    }
    
    /*------------------------------------------------------
     * OEM: FORD
     * Commlevel: KWP2000
     * Detection: Tester Present with Response Required
     *           
     * Note: This service also works for DCX. Must detect
     *       DCX before getting here.
     *------------------------------------------------------*/
    if (status != S_SUCCESS)
    {
        obd2can_servicedata_init(&servicedata);
        servicedata.ecm_id = pcm_id;
        servicedata.service = 0x3E;     /* Tester Present */
        servicedata.subservice = 0x01;  /* Response Required */
        servicedata.rx_timeout_ms = 250;
        servicedata.initial_wait_extended = 0;
        status = obd2can_txrx(&servicedata);
        if (status == S_SUCCESS)
        { 
            /* Chrysler SCI + CAN will detect CAN here */ 
            if (*oemtype != OemType_DCX)
            {
                *commlevel = CommLevel_KWP2000;
                *oemtype = OemType_FORD;
            }
        }
    }
    
    /* No commlevels found */
    if (status != S_SUCCESS)
    {
        *commlevel = CommLevel_Unknown;
        *oemtype = OemType_Unknown;
    }
    
    return S_SUCCESS;
}

/**
 *  @brief Return Padding Byte based on detected OEM if available
 *
 *  @return Padding byte
 */
u8 obd2can_get_padding_byte(void)
{
    u8 padding_byte;
    
    switch(obd2_get_oemtype())
    {
    case OemType_DCX:
        padding_byte = CAN_PADDING_BYTE_DCX;
        break;
    case OemType_FORD:
        padding_byte = CAN_PADDING_BYTE_FORD;
        break;
    case OemType_GM:
        padding_byte = CAN_PADDING_BYTE_GM;
        break;
    default:
        padding_byte = 0x00;
        break;        
    }
    return padding_byte;
}

/**
 * @brief   Request transfer exit. 
 *
 * @param [in] ecm_id       
 * @param [in] commlevel
 * @retval  u8 status
 *
 * @author  Ricardo Cigarroa
 */
u8 obd2can_request_transfer_exit(u32 ecm_id, VehicleCommLevel commlevel)
{
    u8 status;
     
    switch (obd2_get_oemtype())
    {
#ifdef __DCX_MANUF__        
    case OemType_DCX:
        status = obd2can_dcx_request_transfer_exit(ecm_id,commlevel);
        break;
#endif
#ifdef __FORD_MANUF__
    case OemType_FORD:
        status = obd2can_ford_request_transfer_exit(ecm_id,commlevel);
        break;
#endif
#ifdef __GM_MANUF__
    case OemType_GM:
        status = obd2can_gm_request_transfer_exit(ecm_id,commlevel);
        break;
#endif
    default:
        status = S_NOTSUPPORT;
        break;
    }  
        
    return status;
}

/**
 * @brief   Request download 
 *
 * @param [in] ecm_id
 * @param [in] dataformat
 * @param [in] requestaddress
 * @param [in] requestlength
 * @param [in] reqtype
 * @param [in] commlevel
 * @param [out] maxdownloadblocklength
 * @retval  u8 status
 *
 * @author  Ricardo Cigarroa
 */
u8 obd2can_request_download(u32 ecm_id, u8 dataformat, u32 requestaddress, 
                            u32 requestlength, RequestLengthType reqtype,
                            VehicleCommLevel commlevel, 
                            u16 *maxdownloadblocklength)
{
    u8 status;
     
    switch (obd2_get_oemtype())
    {
#ifdef __DCX_MANUF__        
    case OemType_DCX:
        status = obd2can_dcx_request_download(ecm_id,dataformat,requestaddress, 
                                              requestlength,reqtype,commlevel, 
                                              maxdownloadblocklength);
        break;
#endif
#ifdef __FORD_MANUF__
    case OemType_FORD:
        status = obd2can_ford_request_download(ecm_id,dataformat,requestaddress, 
                                               requestlength,reqtype,commlevel, 
                                               maxdownloadblocklength);
        break;
#endif
#ifdef __GM_MANUF__
    case OemType_GM:
        status = obd2can_gm_request_download(ecm_id,dataformat,requestaddress, 
                                             requestlength,reqtype,commlevel, 
                                             maxdownloadblocklength);
        break;
#endif
    default:
        status = S_NOTSUPPORT;
        break;
    }  
        
    return status;
}

/**
 * @brief   Request upload 
 *
 * @param [in] ecm_id
 * @param [in] requestaddress
 * @param [in] requestlength
 * @param [in] reqtype
 * @param [in] commlevel
 * @param [out] maxuploadblocklength
 * @retval  u8 status
 *
 * @author  Ricardo Cigarroa
 */
u8 obd2can_request_upload(u32 ecm_id, u32 requestaddress, u32 requestlength,
                          RequestLengthType reqtype, VehicleCommLevel commlevel,
                          u16 *maxuploadblocklength)
{
    u8 status;
     
    switch (obd2_get_oemtype())
    {
#ifdef __DCX_MANUF__        
    case OemType_DCX:
        status = obd2can_dcx_request_upload(ecm_id,requestaddress,requestlength, 
                                            reqtype,commlevel, 
                                            maxuploadblocklength);
        break;
#endif
#ifdef __FORD_MANUF__
    case OemType_FORD:
        status = obd2can_ford_request_upload(ecm_id,requestaddress,requestlength, 
                                             reqtype,commlevel, 
                                             maxuploadblocklength);
        break;
#endif
#ifdef __GM_MANUF__
    case OemType_GM:
        status = obd2can_gm_request_upload(ecm_id,requestaddress,requestlength, 
                                           reqtype,commlevel, 
                                           maxuploadblocklength);
        break;
#endif
    default:
        status = S_NOTSUPPORT;
        break;
    }  
        
    return status;
}

/**
 * @brief   Transfer data upload
 *
 * @param [in] ecm_id
 * @param [in] blocktracking
 * @param [in] commlevel
 * @param [in] request_address
 * @param [in] request_type
 * @param [out] data
 * @param [out] datalength
 * @retval  u8 status
 *
 * @author  Ricardo Cigarroa
 */
u8 obd2can_transfer_data_upload(u32 ecm_id, u8 blocktracking,
                                VehicleCommLevel commlevel, u32 request_address,
                                RequestLengthType request_type, u8 *data, 
                                u16 *datalength)
{
    u8 status;
     
    switch (obd2_get_oemtype())
    {
#ifdef __DCX_MANUF__        
    case OemType_DCX:
        status = obd2can_dcx_transfer_data_upload(ecm_id,blocktracking,commlevel, 
                                                 request_address,request_type,
                                                 data,datalength);
        break;
#endif
#ifdef __FORD_MANUF__
    case OemType_FORD:
        status = obd2can_ford_transfer_data_upload(ecm_id,blocktracking,commlevel, 
                                                   request_address,request_type,
                                                   data,datalength);
        break;
#endif
#ifdef __GM_MANUF__
    case OemType_GM:
        status = obd2can_gm_transfer_data_upload(ecm_id,blocktracking,commlevel, 
                                                 request_address,request_type,
                                                 data,datalength);
        break;
#endif
    default:
        status = S_NOTSUPPORT;
        break;
    }  
        
    return status;
}

/**
 * @brief   Transfer data download
 *
 * @param [in] ecm_id
 * @param [in] subservice
 * @param [in] blocktracking
 * @param [in] startingaddr
 * @param [in] addrtype
 * @param [out] data
 * @param [in] datalength
 * @param [in] commlevel
 * @param [in] response_expected
 * @retval  u8 status
 *
 * @author  Ricardo Cigarroa
 */
u8 obd2can_transfer_data_download(u32 ecm_id, Obd2can_SubServiceNumber subservice,
                                  u8 blocktracking, u32 startingaddr,
                                  MemoryAddressType addrtype, u8 *data, 
                                  u16 datalength, VehicleCommLevel commlevel,
                                  bool response_expected)
{
    u8 status;
     
    switch (obd2_get_oemtype())
    {
#ifdef __DCX_MANUF__        
    case OemType_DCX:
        status = obd2can_dcx_transfer_data_download(ecm_id,subservice,
                                                    blocktracking,startingaddr,
                                                    addrtype,data,datalength,
                                                    commlevel,response_expected);
        break;
#endif
#ifdef __FORD_MANUF__
    case OemType_FORD:
        status = obd2can_ford_transfer_data_download(ecm_id,subservice,
                                                     blocktracking,startingaddr,
                                                     addrtype,data,datalength,
                                                     commlevel,response_expected);
        break;
#endif
#ifdef __GM_MANUF__
    case OemType_GM:
        status = obd2can_gm_transfer_data_download(ecm_id,subservice,
                                                   blocktracking,startingaddr,
                                                   addrtype,data,datalength,
                                                   commlevel,response_expected);
        break;
#endif
    default:
        status = S_NOTSUPPORT;
        break;
    }  
        
    return status;
}

/**
 * @brief  Security access
 *
 * @param [in] ecm_id
 * @param [in] subservice
 * @param [in] algoindex
 * @param [out] data
 * @param [out] datalength
 * @retval  u8 status
 *
 * @author  Ricardo Cigarroa
 */
u8 obd2can_security_access(u32 ecm_id, Obd2can_SubServiceNumber subservice,
                           u8 *data, u8 *datalength, u32 algoindex)
{
    u8 status;
     
    switch (obd2_get_oemtype())
    {
#ifdef __DCX_MANUF__        
    case OemType_DCX:
        status = obd2can_dcx_security_access(ecm_id,subservice,data,datalength,
                                             algoindex);
        break;
#endif
#ifdef __FORD_MANUF__
    case OemType_FORD:
        status = obd2can_ford_security_access(ecm_id,subservice,data,datalength,
                                              algoindex);
        break;
#endif
#ifdef __GM_MANUF__
    case OemType_GM:
        status = obd2can_gm_security_access(ecm_id,subservice,data,datalength,
                                            algoindex);
        break;
#endif
    default:
        status = S_NOTSUPPORT;
        break;
    }  
        
    return status;
}

/**
 * @brief  Read memory address
 *
 * @param [in] ecm_id
 * @param [in] address
 * @param [in] length
 * @param [in] memsize
 * @param [in] commlevel
 * @param [out] data
 * @param [out] datalength
 * @retval  u8 status
 *
 * @author  Ricardo Cigarroa
 */
u8 obd2can_read_memory_address(u32 ecm_id, u32 address, u16 length,  
                               MemoryAddressType addrtype, MemorySize memsize,
                               u8 *data, u16 *datalength, bool force_readby4bytes,
                               VehicleCommLevel commlevel)
{
    u8 status;
     
    switch (obd2_get_oemtype())
    {
#ifdef __DCX_MANUF__        
    case OemType_DCX:
        status = obd2can_dcx_read_memory_address(ecm_id,address,length,memsize,
                                                 data,datalength,commlevel);
        break;
#endif
#ifdef __FORD_MANUF__
    case OemType_FORD:
        status = obd2can_ford_read_memory_address(ecm_id,address,length,data,
                                                  datalength,force_readby4bytes,
                                                  commlevel);
        break;
#endif
#ifdef __GM_MANUF__
    case OemType_GM:
        status = obd2can_gm_read_memory_address(ecm_id,address,length,addrtype,
                                                data,datalength,force_readby4bytes);
        break;
#endif
    default:
        status = S_NOTSUPPORT;
        break;
    }  
        
    return status;
}
