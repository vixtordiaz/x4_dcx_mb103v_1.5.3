/**
  **************** (C) COPYRIGHT 2011 SCT Performance, LLC *********************
  * File Name          : obd2_ford.c
  * Device / System    : SCT products using the "CommonCode" platform
  * Author             : Quyen Leba
  *
  * Version            : 1 
  * Date               : 05/02/2011
  * Description        : 
  *                    : 
  *
  *
  * History            : 05/02/2011 M. Davis
  *                    :   Add SCT Copyright header,
  *
  ******************************************************************************
  */

#include <board/power.h>
#include <board/delays.h>
#include <board/rtc.h>
#include <common/statuscode.h>
#include <common/genmanuf_overload.h>
#include <common/obd2scp.h>
#include <common/obd2tune.h>
#include <common/filestock.h>
#include <common/filestock_option.h>
#include <common/settings.h>
#include <common/cmdif.h>
#include <common/cmdif_remotegui.h>
#include "obd2_ford_keygen.h"
#include "obd2_ford.h"

extern cmdif_datainfo cmdif_datainfo_responsedata;  /* from cmdif.c */
extern obd2_info gObd2info;                         /* from obd2.c */

//------------------------------------------------------------------------------
// Crank Relearn Parameters
//------------------------------------------------------------------------------
#define CRANK_RELEARN_TIMEOUT           10000   // Comm Timeout in ms
#define CRANK_RELEARN_REFRESH           500     // GUI Refreash in ms
#define CRANK_RELEARN_IDLE_MAX          1100    // Max Idle RPM
#define CRANK_RELEARN_IDLE_MIN          500     // Min Idle RPM
#define CRANK_RELEARN_ECT_MIN           80      // Degrees C (>= 176 Degrees F)
#define CRANK_RELEARN_RPM_FALL_THRESH   1200    // Relearn falling threshold (should be successful by this point)

//------------------------------------------------------------------------------
// Inputs:  u8  *seed
//          u32 algoindex (index algorithm)
//          VehicleCommType commtype
// Output:  u8  *key
// Return:  u8  status
//------------------------------------------------------------------------------
u8 obd2_ford_generatekey(u8 *seed, u32 algoindex, VehicleCommType commtype,
                         u8 *key)
{
    u32 calc_seed;
    u32 calc_key;
    u8 status;
        
    switch(commtype)
    {
    case CommType_CAN:
        if (algoindex == UnlockId_None)
        {
            status = S_NOTREQUIRED;
        }
        else
        {
            calc_seed = ((u32)seed[0]<<16) + ((u32)seed[1]<<8) + ((u32)seed[2]);
            status = obd2_ford_keygen(calc_seed, algoindex, &calc_key);
            
            key[0] = calc_key;
            key[1] = calc_key >> 8;
            key[2] = calc_key >> 16;
        }
        break;
    default:
        status = S_SUCCESS;
        break;
    }
    return status;
}

//------------------------------------------------------------------------------
// Check if a binary type is valid
// Input:   u16 veh_type (defined for veh_defs)
// Outputs: u32 *vidposition (position of VID in stock file)
// Return:  u8  status
// Engineer: Quyen Leba
// Note: all positions are assumed belonged to one ECM (the main ECM)
// Note: securitybyteposition is relevent to vidposition
// Note: all ranges/data must be inclusive
//------------------------------------------------------------------------------
u8 obd2_ford_getoverlayposition_instockfile(u16 veh_type,
                                            u32 *vidposition, u32 *patsposition,
                                            u32 *securitybyteposition)
{
    u16 ecm_type;
    VehicleCommType commtype;
    VehicleCommLevel commlevel;
    u32 source_vidposition;
    u32 source_patsposition;
    u32 vid_offset;
    u32 pats_offset;
    u32 securitybyte_offset;
    u32 ecm_bytecount;
    u8  ecm_index;
    u32 memblock_address;
    u32 memblock_size;
    bool vidposition_found;
    bool patsposition_found;
    u8  i;
    u8  status;
    
    *vidposition = 0xFFFFFFFF;          //mark invalid position
    *patsposition = 0xFFFFFFFF;
    *securitybyteposition = 0xFFFFFFFF;
    
    vid_offset = pats_offset = securitybyte_offset = 0;
    vidposition_found = patsposition_found = FALSE;
    status = S_FAIL;
    for(ecm_index=0;ecm_index<ECM_MAX_COUNT;ecm_index++)
    {
        ecm_type = VEH_GetMainEcmType(veh_type);
        if (ecm_type == INVALID_ECM_DEF)
        {
            break;
        }
        
        if (VEH_GetMainEcmIndex(veh_type) != ecm_index)
        {
            //offset the whole ecm
            status = obd2tune_getecmsize_byecmtype(ecm_type,&ecm_bytecount);
            if (status != S_SUCCESS)
            {
                return status;
            }
            vid_offset += ecm_bytecount;
            pats_offset += ecm_bytecount;
            securitybyte_offset += ecm_bytecount;
        }
        else
        {
            source_vidposition = ECM_GetVidAddress(ecm_type);
            source_patsposition = ECM_GetPatsAddress(ecm_type);
            
            commtype = ECM_GetCommType(ecm_type);
            commlevel = ECM_GetCommLevel(ecm_type);
            
            for(i=0;i<MAX_ECM_MEMORY_BLOCKS;i++)
            {
                if (ECM_IsUploadCalOnly(ecm_type))
                {
                    memblock_address = ECM_GetCalBlockAddr(ecm_type,i);
                    memblock_size = ECM_GetCalBlockSize(ecm_type,i);
                }
                else
                {
                    memblock_address = ECM_GetOSReadBlockAddr(ecm_type,i);
                    memblock_size = ECM_GetOSReadBlockSize(ecm_type,i);
                }
                
                if (source_vidposition >= memblock_address &&
                    source_vidposition < (memblock_address + memblock_size))
                {
                    vidposition_found = TRUE;
                    vid_offset += (source_vidposition - memblock_address);
                    
                    // Security byte address is 1 byte prior to the strategy
                    if (commtype == CommType_CAN)
                    {
                        if (commlevel == CommLevel_ISO14229)
                        {
                            securitybyte_offset = vid_offset - 126;
                        }
                        else if (commlevel == CommLevel_KWP2000)
                        {
                            securitybyte_offset = vid_offset - 123;
                        }
                        else
                        {
                            status = S_COMMLEVEL;
                            goto obd2_ford_getoverlayposition_instockfile_done;
                        }
                    }
                    else if (commtype == CommType_SCP || commtype == CommType_SCP32)
                    {
                        securitybyte_offset = vid_offset - 123;
                    }
                    else
                    {
                        status = S_COMMTYPE;
                        goto obd2_ford_getoverlayposition_instockfile_done;
                    }
                }
                else if (vidposition_found == FALSE)
                {
                    vid_offset += memblock_size;
                }
                
                if (source_patsposition >= memblock_address &&
                    source_patsposition < (memblock_address + memblock_size))
                {
                    patsposition_found = TRUE;
                    pats_offset += (source_patsposition - memblock_address);
                }
                else if (patsposition_found == FALSE)
                {
                    pats_offset += memblock_size;
                }
            }//for(i=...
            
            break;
        }
    }//for(ecm_index=...
    
    if (vidposition_found && patsposition_found)
    {
        *vidposition = vid_offset;
        *patsposition = pats_offset;
        *securitybyteposition = securitybyte_offset;
        status = S_SUCCESS;
    }
    else
    {
        status = S_FAIL;
    }

obd2_ford_getoverlayposition_instockfile_done:
    return status;
}

//------------------------------------------------------------------------------
// Handle overlay data
// Inputs:  u16 veh_type
//          u8  *vin (VIN_LENGTH+1)
//          bool isdoapply (TRUE: apply from settingstune to flash file;
//                          FALSE: save from stock file to settingstune)
// Return:  u8  status
// Engineer: Quyen Leba
//------------------------------------------------------------------------------
u8 obd2tune_ford_handle_overlaydata(u16 veh_type, u8 *vin, bool isdoapply)
{
    u32 vidposition;
    u32 patsposition;
    u32 securitybyteposition;
    u32 offset;
    VehicleCommType commtype;
    u16 vid_length;
    u16 ecm_type;
    u32 bytecount;
    u8  status;
    u32 i;
    
    u8 VIDtemp[256];
    u8 EPATStemp[14];

    ecm_type = VEH_GetEcmType(veh_type,0);
    if (ECM_IsAllowIndirectBlankVINOverlay(ecm_type) &&
        ECM_IsValidVINsAddress(ecm_type) && isdoapply)
    {
        //overlay indirect VIN
        status = obd2tune_translate_ecm_address_to_ecm_offset(ecm_type,ECM_GetVINsAddress(ecm_type),&offset);
        if (status == S_SUCCESS)
        {
            status = filestock_openflashfile("r+");
            if (status != S_SUCCESS)
            {
                goto obd2tune_ford_handle_overlaydata_done;
            }
            status = filestock_updateflashfile(offset,vin,VIN_LENGTH);
            if (status != S_SUCCESS)
            {
                goto obd2tune_ford_handle_overlaydata_done;
            }
            filestock_closeflashfile();
        }
        else
        {
            goto obd2tune_ford_handle_overlaydata_done;
        }
    }//if (ECM_IsAllowIndirectBlankVINOverlay...

    if (VEH_IsRequireOverlay(veh_type) == FALSE)
    {
        status = S_NOTREQUIRED;
        goto obd2tune_ford_handle_overlaydata_done;
    }
    
    commtype = ECM_GetCommType(VEH_GetMainEcmType(veh_type));
    if (commtype == CommType_CAN)
    {
        vid_length = 256;
    }
    else if (commtype == CommType_SCP || commtype == CommType_SCP32)
    {
        vid_length = 128;
    }
    else
    {
        status = S_COMMTYPE;
        goto obd2tune_ford_handle_overlaydata_done;
    }
    
    status = obd2_ford_getoverlayposition_instockfile
        (veh_type, &vidposition, &patsposition, &securitybyteposition);
    if (status != S_SUCCESS)
    {
        goto obd2tune_ford_handle_overlaydata_done;
    }
    
    if (isdoapply)
        status = filestock_openflashfile("r+");
    else
        status = filestock_openstockfile("r+", FALSE);
        
    if (status != S_SUCCESS)
    {
        goto obd2tune_ford_handle_overlaydata_done;
    }
    
    //##########################################################################
    // Handle VID block
    //##########################################################################
    if (isdoapply)
    {
        // 6.0L OTF tune writes a string at the end of the VID block
        // We need to scan VID block in flash.bin to see if we need to skip
        // overlaying those bytes.
        if(VEH_IsVehicleType_Ford_60L(veh_type))
        {
            status = filestock_read_at_position(vidposition, VIDtemp, vid_length,
                                                &bytecount);
            for(i=240;i<256;i++)
            {
                if(VIDtemp[i] != SETTINGS_TUNE(vid)[i] && 
                   VIDtemp[i] != 0xFF)
                {
                    // Unexpected data detected
                    // Overlay VID data up to this location
                    vid_length = i; 
                    break; 
                }
            }
        }
        
        memcpy(VIDtemp, SETTINGS_TUNE(vid), vid_length);
        
        status = filestock_updateflashfile(vidposition,VIDtemp,vid_length);
        if (status != S_SUCCESS)
        {
            goto obd2tune_ford_handle_overlaydata_done;
        }
    }
    else
    {
        status = filestock_set_position(vidposition);
        if (status != S_SUCCESS)
        {
            goto obd2tune_ford_handle_overlaydata_done;
        }
        status = filestock_read(SETTINGS_TUNE(vid),vid_length,&bytecount);
        if (status != S_SUCCESS || bytecount != vid_length)
        {
            status = S_READFILE;
            goto obd2tune_ford_handle_overlaydata_done;
        }
        // Calculate STOCK VID block CRC32e for later comparison
        crc32e_reset();
        u32 crctemp = 0xFFFFFFFF;
        crctemp = crc32e_calculateblock(crctemp, (u32*)SETTINGS_TUNE(vid), (vid_length/4));
        SETTINGS_TUNE(current_vid_crc32e) = crctemp;
        
        // Copy in VIN incase that it's generated VIN
        memcpy((char*)SETTINGS_TUNE(vid),(char*)vin,VIN_LENGTH);
    }
    
    //##########################################################################
    // Handle PATS block
    //##########################################################################
    if (isdoapply)
    {
        memcpy(EPATStemp, SETTINGS_TUNE(epats), 14);
        status = filestock_updateflashfile(patsposition,EPATStemp,14);
        if (status != S_SUCCESS)
        {
            goto obd2tune_ford_handle_overlaydata_done;
        }
    }
    else
    {
        status = filestock_set_position(patsposition);
        if (status != S_SUCCESS)
        {
            goto obd2tune_ford_handle_overlaydata_done;
        }
        status = filestock_read(SETTINGS_TUNE(epats),14,&bytecount);
        if (status != S_SUCCESS || bytecount != 14)
        {
            status = S_READFILE;
            goto obd2tune_ford_handle_overlaydata_done;
        }
    }
    
    //##########################################################################
    // Handle security byte
    //##########################################################################
    if (isdoapply)
    {
        const u8 securitybyte = 0xFE;
        status = filestock_updateflashfile(securitybyteposition,
                                           (u8*)&securitybyte,1);
        if (status != S_SUCCESS)
        {
            goto obd2tune_ford_handle_overlaydata_done;
        }
    }
    
    if (isdoapply == FALSE)
    {
        SETTINGS_SetTuneAreaDirty();
        settings_update(SETTINGS_UPDATE_IF_DIRTY);
    }
    
obd2tune_ford_handle_overlaydata_done:
    if(isdoapply)
        filestock_closeflashfile();
    else
        filestock_closestockfile();

    return status;
}

//------------------------------------------------------------------------------
// Set Vpp (used in upload/download for certain vehicles)
// Return:  u8  status
// Engineer: Quyen Leba
//------------------------------------------------------------------------------
u8 obd2_ford_setupvpp()
{
    return power_setvpp(Vpp_FEPS);
}

//------------------------------------------------------------------------------
// Check for vehicle wake up
// Inputs:  u16 veh_type
//          u16 ecm_index (0,1,etc; 0xFE: all)
// Return:  u8  status
// Engineer: Quyen Leba
// Some vehicles require this wakeup prior to an unlock; also, some only allow
// this wakeup once.
//------------------------------------------------------------------------------
u8 obd2_ford_wakeup(u16 veh_type, u16 ecm_index)
{
    VehicleCommType commtype;
    u32 loopcount;
    u32 i;
    u32 index;
    u8  status;
    u32 timeout;
    
    loopcount = 1;
    if (ecm_index == 0xFE)  //TODOQ: remove hardcoded 0xFE
    {
        loopcount = ECM_MAX_COUNT;
    }
    else if (ecm_index > ECM_MAX_COUNT)
    {
        return S_INPUT;
    }

    if (SETTINGS_IsDemoMode())
    {
        return S_SUCCESS;
    }

    status = S_SUCCESS;
    index = 0;
    if (VEH_IsRequireWakeUp(veh_type))
    {
        if (VEH_IsCheckKeyOnPriorWakeup(veh_type) && (SETTINGS_IsDownloadFail() == 0))
        {
            timeout = 100;
            do
            {
                delays(300,'m');
                //TODOQ: get commtype from main ecm instead
                status = obd2_checkkeyon(ECM_GetCommType(VEH_GetMainEcmType(veh_type)),TRUE);
                timeout--;
            }while(status != S_SUCCESS && timeout);
            
            if(timeout == 0)
            {
                return S_TIMEOUT;
            }
            //TODOQ: for now because iTSX app doesn't handle this yet
//            cmdif_internalresponse_ack(CMDIF_CMD_PCM_UNLOCK,
//                                       CMDIF_ACK_WAIT,NULL,0);
        }
        
        // Make sure ECM really is ready (Required for ST 225)
        if (ECM_IsRequireKeyOnTest(VEH_GetEcmType(veh_type,0)) &&
            ECM_GetCommType(VEH_GetEcmType(veh_type,0)) == CommType_CAN)
        {
            if (obd2can_key_on_test(Obd2CanEcuId_7E0) != S_SUCCESS)
            {
                return S_TIMEOUT;
            }
        }
        
        for(i=0;i<loopcount;i++)
        {
            if (loopcount == 1)
                index = ecm_index;
            else
                index = i;
            
            if (VEH_GetEcmType(veh_type,index) == INVALID_ECM_DEF)
                break;
            
            commtype = ECM_GetCommType(VEH_GetEcmType(veh_type,index));
            if (commtype == CommType_CAN)
            {
                status = obd2can_ford_wakeup(veh_type,index);
            }
            else if (commtype == CommType_SCP || commtype == CommType_SCP32)
            {
                status = obd2scp_wakeup(veh_type,index);
            }
            else
            {
                status = S_COMMTYPE;
                break;
            }
            if (status != S_SUCCESS)
            {
                break;
            }
        }
    }
    return status;
}

//------------------------------------------------------------------------------
// Process any vehicle specific tasks before program
// Input:   flasherinfo *f_info
// Return:  u8  status
// Engineer: Quyen Leba
//------------------------------------------------------------------------------
u8 obd2_ford_vehicle_specific_task_before_program(flasher_info *f_info)
{
    return S_SUCCESS;
}

//------------------------------------------------------------------------------
// Process any vehicle specific tasks after program
// Input:   flasher_info *f_info
// Return:  u8  status
// Engineer: Quyen Leba
//------------------------------------------------------------------------------
u8 obd2_ford_vehicle_specific_task_after_program(flasher_info *f_info)
{
    u8  status;

    if (SETTINGS_IsDemoMode())
    {
        return S_SUCCESS;
    }

    //vehicle reset after program, give vehicle some time to get ready
    delays(5000,'m');
    
    status = obd2tune_option_handler_postdownload(f_info);

    return status;
}

//------------------------------------------------------------------------------
// Format vehicleinfo to text string
// Input:   vehicle_info *vehicleinfo
// Output:  u8  *vehicleinfo_text
// Return:  u8  status
// Engineer: Quyen Leba
//------------------------------------------------------------------------------
u8 obd2_ford_vehicleinfototext(vehicle_info *vehicleinfo, u8 *vehicleinfo_text)
{
    u16 veh_type;
    u8  commtype_text[32];
    u8  *ptr;
    u32 i;
    u8  status;

    ptr = vehicleinfo_text;
    strcpy((char*)vehicleinfo->vin,(char*)vehicleinfo->ecm.vin);
    obd2_vehiclecommtotext(vehicleinfo->commtype,commtype_text);
    
    strcpy((char*)ptr,"<HEADER>VIN:</HEADER> ");
    status = obd2_validatevin(vehicleinfo->vin);
    if (status == S_VINBLANK || status == S_VININVALID)
    {
        strcat((char*)ptr,"Blank");
    }
    else if (status == S_SUCCESS)
    {
        strcat((char*)ptr,(char*)vehicleinfo->vin);
    }
    else
    {
        strcat((char*)ptr,"N/A");
    }

    strcat((char*)ptr,"<HEADER>COMM:</HEADER> ");
    strcat((char*)ptr,(char*)commtype_text);
    if (vehicleinfo->ecm.ecm_count > 0)
    {
        strcat((char*)ptr,"<HEADER>ECU Strategy:</HEADER> ");
        if (vehicleinfo->ecm.codes[0][0][0] != NULL)
        {
            strcat((char*)ptr,(char*)vehicleinfo->ecm.codes[0][0]);
        }
        else
        {
            strcat((char*)ptr, "Not Found");
        }
        
        if (vehicleinfo->ecm.codecount[0] >= 2)
        {
            strcat((char*)ptr," <HEADER>ECU CalID:</HEADER> ");
            strcat((char*)ptr,(char*)vehicleinfo->ecm.codes[0][1]);
        }
        
        if (vehicleinfo->ecm.second_codes[0][0][0] != NULL)
        {
            strcat((char*)ptr,"<HEADER>ECU SWPN:</HEADER> ");
            strcat((char*)ptr,(char*)vehicleinfo->ecm.second_codes[0][0]);
        }
        
        if (vehicleinfo->ecm.second_codes[0][1][0] != NULL)
        {
            strcat((char*)ptr,"<HEADER>ECU HWPN:</HEADER> ");
            strcat((char*)ptr,(char*)vehicleinfo->ecm.second_codes[0][1]);
        }
        
        if (vehicleinfo->ecm.second_codes[0][2][0] != NULL)
        {
            strcat((char*)ptr,"<HEADER>ECU MODULE PN:</HEADER> ");
            strcat((char*)ptr,(char*)vehicleinfo->ecm.second_codes[0][2]);
        }
        
        if (vehicleinfo->ecm.ecm_count > 1)
        {
            strcat((char*)ptr,"<HEADER>TCU Strategy:</HEADER> ");
            if (vehicleinfo->ecm.codes[1][0][0] != NULL)
            {
                strcat((char*)ptr,(char*)vehicleinfo->ecm.codes[1][0]);
            }
            else
            {
                strcat((char*)ptr, "Not Found");
            }
            
            if (vehicleinfo->ecm.codecount[0] >= 2)
            {
                strcat((char*)ptr," <HEADER>TCU CalID:</HEADER> ");
                strcat((char*)ptr,(char*)vehicleinfo->ecm.codes[1][1]);
            }
            
            if (vehicleinfo->ecm.second_codes[1][0][0] != NULL)
            {
                strcat((char*)ptr,"<HEADER>TCU SWPN:</HEADER> ");
                strcat((char*)ptr,(char*)vehicleinfo->ecm.second_codes[1][0]);
            }
            
            if (vehicleinfo->ecm.second_codes[1][1][0] != NULL)
            {
                strcat((char*)ptr,"<HEADER>TCU HWPN:</HEADER> ");
                strcat((char*)ptr,(char*)vehicleinfo->ecm.second_codes[1][1]);
            }
            
            if (vehicleinfo->ecm.second_codes[1][2][0] != NULL)
            {
                strcat((char*)ptr,"<HEADER>TCU MODULE PN:</HEADER> ");
                strcat((char*)ptr,(char*)vehicleinfo->ecm.second_codes[1][2]);
            }
        }
        if (vehicleinfo->ecm.codecount[0] > 1)
        {
            strcat((char*)ptr,"<HEADER>ECU PartNumbers:</HEADER> ");
            for(i=0;i<vehicleinfo->ecm.codecount[0];i++)
            {
                strcat((char*)ptr,(char*)vehicleinfo->ecm.codes[0][i]);
                if ((i+1) < vehicleinfo->ecm.codecount[0])
                {
                    strcat((char*)ptr," - ");
                }
            }
        }
        if (vehicleinfo->ecm.codecount[1] > 1)
        {
            strcat((char*)ptr,"<HEADER>TCU PartNumbers:</HEADER> ");
            for(i=0;i<vehicleinfo->ecm.codecount[1];i++)
            {
                strcat((char*)ptr,(char*)vehicleinfo->ecm.codes[1][i]);
                if ((i+1) < vehicleinfo->ecm.codecount[1])
                {
                    strcat((char*)ptr," - ");
                }
            }
        }
    }
    
    //######################################################################
    //
    //######################################################################
    
    if (vehicleinfo->ecm.vehicle_serial_number[0] != NULL)
    {
        strcat((char*)ptr,"<HEADER>Serial:</HEADER> ");
        strcat((char*)ptr,(char*)vehicleinfo->ecm.vehicle_serial_number);
    }

    //######################################################################
    // Vehicle specific info
    //######################################################################

    veh_type = gObd2info.vehicle_type;
    if (veh_type == INVALID_VEH_DEF && vehicleinfo->ecm.ecm_count > 0 &&
        isgraph(vehicleinfo->ecm.codes[0][0][0]))
    {
        status = obd2tune_attempt_to_find_vehicle_type_using_tunecode(vehicleinfo->ecm.codes[0][0],
                                                                      vehicleinfo->vin,&veh_type);
        if (status != S_SUCCESS)
        {
            veh_type = INVALID_VEH_DEF;
        }
    }

    if (veh_type != INVALID_VEH_DEF)
    {
        if (VEH_FORD_Is67L_PCM(VEH_GetMainEcmType(veh_type)))
        {
            obd2can_ford_67L_get_vehicle_info(vehicleinfo_text);
        }
        else if (VEH_FORD_Is60L_PCM(VEH_GetMainEcmType(veh_type)))
        {
            obd2can_ford_60L_get_vehicle_info(vehicleinfo_text);
        }
    }

    obd2can_ford_check_TPMS_support(vehicleinfo_text);

    return S_SUCCESS;
}

/**
 * @brief   Ford Special Functions Menu
 *  
 * @param   [in] commtype
 * @param   [in] commlevel
 *
 * @details This function handles the OEM specific special functions menu.
 *  
 * @author  Tristen Pierson
 */
void obd2_ford_vehiclefunction(VehicleCommType *commtype, VehicleCommLevel *commlevel)
{
    CMDIF_REMOTEGUI_RESPONSETYPE responsetype;
    CMDIF_COMMAND cmd = CMDIF_CMD_SPECIAL_FUNCTIONS;
    u16 responsechoice;
    u8  functions[MAX_VEHICLE_FUNCTIONS];
    u8  i;
  
    // Configure functions based on commlevel
    switch (commlevel[0])
    {
    case CommLevel_KWP2000:
        functions[0] = VehicleFunction_KAM_Reset;
        functions[1] = VehicleFunction_EOL;
        break;
    case CommLevel_ISO14229:
        functions[0] = VehicleFunction_KAM_Reset;
        functions[1] = VehicleFunction_CrankRelearn;
        functions[2] = VehicleFunction_EOL;
        break;
    default:
        functions[0] = VehicleFunction_EOL;
        break;
    }
    
    while(1)
    {
        if (cmdif_remotegui_listboxbox_init("SPECIAL FUNCTIONS", NULL) == S_SUCCESS)
        {
            // Build menu
            for (i = 0; i < MAX_VEHICLE_FUNCTIONS; i++)
            {
                if (functions[i] == VehicleFunction_EOL)
                {
                    break;
                }
                switch (functions[i])
                {
                case VehicleFunction_KAM_Reset:
                    cmdif_remotegui_listboxbox_additem("KAM RESET", "", CMDIF_REMOTEGUI_LISTBOX_FLAGS_CLICKABLE | CMDIF_REMOTEGUI_LISTBOX_FLAGS_ALIGN_CENTER);
                    break;
                case VehicleFunction_CrankRelearn:
                    cmdif_remotegui_listboxbox_additem("CRANK RELEARN", "", CMDIF_REMOTEGUI_LISTBOX_FLAGS_CLICKABLE | CMDIF_REMOTEGUI_LISTBOX_FLAGS_ALIGN_CENTER);
                    break;
                default:
                    break;
                }
            }        
            // If no functions are available (list is empty)
            if (i == 0)
            {
                cmdif_remotegui_listboxbox_cleanup();
                cmdif_remotegui_messagebox(cmd,
                        "SPECIAL FUNCTIONS", CMDIF_REMOTEGUI_ICON_NONE,
                        CMDIF_REMOTEGUI_BUTTONFACE_Exit, CMDIF_REMOTEGUI_BUTTONFACE_None, CMDIF_REMOTEGUI_BUTTONFACE_None,
                        CMDIF_REMOTEGUI_FOCUS_None,
                        "No special functions are available for this vehicle");
            }
            // Show listbox if functions are available
            else
            {
                cmdif_remotegui_listbox(cmd, 0, 0,
                                        CMDIF_REMOTEGUI_BUTTONFACE_Exit, CMDIF_REMOTEGUI_BUTTONFACE_None, CMDIF_REMOTEGUI_BUTTONFACE_None,
                                        CMDIF_REMOTEGUI_FOCUS_ListBox);
            }
            cmdif_remotegui_wait_user_response(&responsetype, &responsechoice, NULL);

            // Exit button selected
            if (responsetype == CMDIF_REMOTEGUI_RESPONSETYPE_BUTTON)
            {  
                return;
            }
            // Menu item selected
            else if (responsetype == CMDIF_REMOTEGUI_RESPONSETYPE_LIST)
            {       
                //key on and continue. 
                cmdif_remotegui_msg_keyon(cmd,"SPECIAL FUNCTIONS");
                
                switch (functions[responsechoice])
                {
                case VehicleFunction_KAM_Reset:
                    obd2_ford_clear_kam(commtype[0], commlevel[0]);
                    break;
                case VehicleFunction_CrankRelearn:
                    obd2_ford_crank_relearn(commtype[0], commlevel[0]);
                    break;
                default:
                    break;
                } 
                //key off and continue.
                cmdif_remotegui_msg_keyoff(cmd,"SPECIAL FUNCTIONS"); 
            }           
        } // if(...)
        else
        {
            break;
        }
    } // while(1)
    return;
}

/**
 * @brief   Clear KAM
 *  
 * @param   [in] commtype
 * @param   [in] commlevel
 *
 * @author  Tristen Pierson
 */
void obd2_ford_clear_kam(VehicleCommType commtype, VehicleCommLevel commlevel)
{    
    CMDIF_COMMAND cmd = CMDIF_CMD_SPECIAL_FUNCTIONS;
    CMDIF_REMOTEGUI_RESPONSETYPE responsetype;    
    u16 responsechoice;
    u8 status;
    
    enum {
        resetMemoryContinue = CMDIF_REMOTEGUI_BUTTONTYPE_RIGHT,
        resetMemoryCancel   = CMDIF_REMOTEGUI_BUTTONTYPE_LEFT,        
    };
    
    switch (commtype)
    {
    case CommType_SCP:
    case CommType_SCP32:
    case CommType_CAN:        
        cmdif_remotegui_messagebox(cmd,
                                   "SPECIAL FUNCTIONS", CMDIF_REMOTEGUI_ICON_NONE,
                                   CMDIF_REMOTEGUI_BUTTONFACE_Cancel, CMDIF_REMOTEGUI_BUTTONFACE_Continue, CMDIF_REMOTEGUI_BUTTONFACE_None,
                                   CMDIF_REMOTEGUI_FOCUS_Button0,
                                   "<HEADER>RESET KAM</HEADER>\nTurn Key On\nReset keep alive memory (KAM)");
       cmdif_remotegui_wait_user_response(&responsetype, &responsechoice, NULL);

        switch (responsechoice)
        {
        case resetMemoryContinue:
            cmdif_remotegui_messagebox(cmd,
                                       "SPECIAL FUNCTIONS", CMDIF_REMOTEGUI_ICON_NONE,
                                       CMDIF_REMOTEGUI_BUTTONFACE_None, CMDIF_REMOTEGUI_BUTTONFACE_None, CMDIF_REMOTEGUI_BUTTONFACE_None,
                                       CMDIF_REMOTEGUI_FOCUS_None,
                                       "<HEADER>RESET KAM</HEADER>\nProcessing...");
            if (commtype == CommType_SCP || commtype == CommType_SCP32)
            {
                obd2scp_init(Obd2scpType_24Bit);
                status = obd2scp_clear_kam();
            }
            else if (commtype == CommType_CAN)
            {
                status = obd2can_ford_clear_kam(Obd2CanEcuId_7E0, commlevel);
            }            
            break;
        case resetMemoryCancel:
            status = S_USERABORT;
            break;
        }
        break;
    default:
        status = S_NOTSUPPORT;
        break;
    }        
    
    if (status == S_SUCCESS)
    {
        cmdif_remotegui_messagebox(cmd,
                                   "SPECIAL FUNCTIONS", CMDIF_REMOTEGUI_ICON_NONE,
                                   CMDIF_REMOTEGUI_BUTTONFACE_Exit, CMDIF_REMOTEGUI_BUTTONFACE_None, CMDIF_REMOTEGUI_BUTTONFACE_None,
                                   CMDIF_REMOTEGUI_FOCUS_None,
                                   "<HEADER>RESET KAM</HEADER>\nKAM Reset Complete\nTurn Key Off");
    }
    else if (status == S_NOTSUPPORT)
    {
        cmdif_remotegui_messagebox(cmd,
                                   "SPECIAL FUNCTIONS", CMDIF_REMOTEGUI_ICON_NONE,
                                   CMDIF_REMOTEGUI_BUTTONFACE_Exit, CMDIF_REMOTEGUI_BUTTONFACE_None, CMDIF_REMOTEGUI_BUTTONFACE_None,
                                   CMDIF_REMOTEGUI_FOCUS_None,
                                   "<HEADER>RESET KAM</HEADER>\nERROR: This vehicle does not support KAM reset.");
    }
    else if (status == S_USERABORT)
    {
        return;
    }    
    else
    {
        cmdif_remotegui_messagebox(cmd,
                                   "SPECIAL FUNCTIONS", CMDIF_REMOTEGUI_ICON_NONE,
                                   CMDIF_REMOTEGUI_BUTTONFACE_Exit, CMDIF_REMOTEGUI_BUTTONFACE_None, CMDIF_REMOTEGUI_BUTTONFACE_None,
                                   CMDIF_REMOTEGUI_FOCUS_None,
                                   "<HEADER>RESET KAM</HEADER>\nERROR: KAM Reset Failed.");
    }
     cmdif_remotegui_wait_user_response(&responsetype, &responsechoice, NULL);
}

/**
 * @brief   Crank Relearn (Misfire Profile Relearn)
 *  
 * @param   [in] commtype
 * @param   [in] commlevel
 *
 * @author  Tristen Pierson
 *
 * @TODO    This function currently only supports ISO14229
 */
void obd2_ford_crank_relearn(VehicleCommType commtype,VehicleCommLevel commlevel)
{   
    u8  status = 0;
    u32 guiRefresh;
    u32 timeout;
    u8  retry;
    u8  targetReached;
    u8  AcRelay;
    u8  MpLearn;
    u8  LearnEnabled;
    u16 DownCount;
    float RPM;          // Engine Speed (BP = 2)
    float ECT;          // Engine Coolant Temperature (Degrees C) - Corrected
    float TargetRPM;
    float MaxRPM;
    u8  seed[3];
    u8  key[3];    
    u8  data[8];
    u8  datalength[1];
       
    CMDIF_REMOTEGUI_RESPONSETYPE responsetype;
    CMDIF_COMMAND cmd = CMDIF_CMD_SPECIAL_FUNCTIONS;
    u16 responsechoice;    
    u8 displayData[150]; // 5 Lines, 30 characters per line
    u8 statusMsg[30];

    retry = 0;
    targetReached = 0;
    
    enum {
        startPageAbout,
        startPage1,
        startPage2,
        startPage3,
        startPage4,
        startDone,
        startAbort,
    } startPageState;
    
    enum {
        processCheckPreConditions,
        processStart,
        processCommandSent,
        processFail,
        processAbort,
        processSuccess,
        processDone,
    } processState;
    
    if (commtype != CommType_CAN && commlevel != CommLevel_ISO14229)
    {
        cmdif_remotegui_messagebox(cmd,
                                   "CRANK RELEARN", CMDIF_REMOTEGUI_ICON_NONE,
                                   CMDIF_REMOTEGUI_BUTTONFACE_Exit, CMDIF_REMOTEGUI_BUTTONFACE_None, CMDIF_REMOTEGUI_BUTTONFACE_None,
                                   CMDIF_REMOTEGUI_FOCUS_None,
                                   "<HEADER>NOT SUPPORTED</HEADER>");        
        cmdif_remotegui_wait_user_response(&responsetype, &responsechoice, NULL);
        return;
    }
    
    // Crank Relearn Pre-Start Instructions
    startPageState = startPageAbout;
    while(startPageState != startDone)
    {
        switch (startPageState)
        {
        case startPageAbout:
            cmdif_remotegui_messagebox(cmd,
                                       "CRANK RELEARN", CMDIF_REMOTEGUI_ICON_NONE,
                                       CMDIF_REMOTEGUI_BUTTONFACE_Exit, CMDIF_REMOTEGUI_BUTTONFACE_Continue, CMDIF_REMOTEGUI_BUTTONFACE_None,
                                       CMDIF_REMOTEGUI_FOCUS_Button1,
                                       "<HEADER>NOTICE</HEADER>\nThis procedure should be performed following PCM replacement, " \
                                       "certain internal engine repairs, CKP sensor or trigger replacement, " \
                                       "clutch replacement, or when directed to by service bulletin or service manual.");            
            cmdif_remotegui_wait_user_response(&responsetype, &responsechoice, NULL);

            if (responsetype == CMDIF_REMOTEGUI_RESPONSETYPE_BUTTON && 
                responsechoice == CMDIF_REMOTEGUI_BUTTONTYPE_RIGHT)
            {
                startPageState = startPage1;
            }
            else
            {
                startPageState = startAbort;
            }
            break; 
        case startPage1:
            cmdif_remotegui_messagebox(cmd,
                                       "CRANK RELEARN", CMDIF_REMOTEGUI_ICON_NONE,
                                       CMDIF_REMOTEGUI_BUTTONFACE_Exit, CMDIF_REMOTEGUI_BUTTONFACE_Continue, CMDIF_REMOTEGUI_BUTTONFACE_Back,
                                       CMDIF_REMOTEGUI_FOCUS_Button1,
                                       "<HEADER>STEP 1</HEADER>\nVerify no DTCs are present and engine misfire events are not occuring.");            
            cmdif_remotegui_wait_user_response(&responsetype, &responsechoice, NULL);

            if (responsetype == CMDIF_REMOTEGUI_RESPONSETYPE_BUTTON && 
                responsechoice == CMDIF_REMOTEGUI_BUTTONTYPE_RIGHT)
            {
                startPageState = startPage2;
            }
            else if (responsetype == CMDIF_REMOTEGUI_RESPONSETYPE_BUTTON && 
                     responsechoice == CMDIF_REMOTEGUI_BUTTONTYPE_MIDDLE) 
            {
                startPageState = startPageAbout;
            }
            else
            {
                startPageState = startAbort;
            }
            break; 
        case startPage2:
            cmdif_remotegui_messagebox(cmd,
                                       "CRANK RELEARN", CMDIF_REMOTEGUI_ICON_NONE,
                                       CMDIF_REMOTEGUI_BUTTONFACE_Exit, CMDIF_REMOTEGUI_BUTTONFACE_Continue, CMDIF_REMOTEGUI_BUTTONFACE_Back,
                                       CMDIF_REMOTEGUI_FOCUS_Button1,
                                       "<HEADER>STEP 2</HEADER>\nPlease return rev limiter, engine idle, and misfire parameters to stock.");            
            cmdif_remotegui_wait_user_response(&responsetype, &responsechoice, NULL);

            if (responsetype == CMDIF_REMOTEGUI_RESPONSETYPE_BUTTON && 
                responsechoice == CMDIF_REMOTEGUI_BUTTONTYPE_RIGHT)
            {
                startPageState = startPage3;
            }
            else if (responsetype == CMDIF_REMOTEGUI_RESPONSETYPE_BUTTON && 
                     responsechoice == CMDIF_REMOTEGUI_BUTTONTYPE_MIDDLE) 
            {
                startPageState = startPage1;
            }
            else
            {
                startPageState = startAbort;
            }
            break;       
        case startPage3:
            cmdif_remotegui_messagebox(cmd,
                                       "CRANK RELEARN", CMDIF_REMOTEGUI_ICON_NONE,
                                       CMDIF_REMOTEGUI_BUTTONFACE_Exit, CMDIF_REMOTEGUI_BUTTONFACE_Continue, CMDIF_REMOTEGUI_BUTTONFACE_Back,
                                       CMDIF_REMOTEGUI_FOCUS_Button1,
                                       "<HEADER>STEP 3</HEADER>\nStart and idle engine. Vehicle must remain in park or neutral with parking brake applied.");            
            cmdif_remotegui_wait_user_response(&responsetype, &responsechoice, NULL);

            if (responsetype == CMDIF_REMOTEGUI_RESPONSETYPE_BUTTON && 
                responsechoice == CMDIF_REMOTEGUI_BUTTONTYPE_RIGHT)
            {
                startPageState = startPage4;
            }
            else if (responsetype == CMDIF_REMOTEGUI_RESPONSETYPE_BUTTON && 
                     responsechoice == CMDIF_REMOTEGUI_BUTTONTYPE_MIDDLE) 
            {
                startPageState = startPage2;
            }
            else
            {
                startPageState = startAbort;
            }
            break;            
        case startPage4:
            cmdif_remotegui_messagebox(cmd,
                                       "CRANK RELEARN", CMDIF_REMOTEGUI_ICON_NONE,
                                       CMDIF_REMOTEGUI_BUTTONFACE_Exit, CMDIF_REMOTEGUI_BUTTONFACE_Continue, CMDIF_REMOTEGUI_BUTTONFACE_Back,
                                       CMDIF_REMOTEGUI_FOCUS_Button1,
                                       "<HEADER>STEP 4</HEADER>\nTurn A/C and defrosters off. Power steering or " \
                                       "accessory loads must not be present. Fuel cut off Target RPM " \
                                       "will be indicated on screen when conditions are met. " \
                                       "Select CONTINUE to begin.");            
            cmdif_remotegui_wait_user_response(&responsetype, &responsechoice, NULL);

            if (responsetype == CMDIF_REMOTEGUI_RESPONSETYPE_BUTTON && 
                responsechoice == CMDIF_REMOTEGUI_BUTTONTYPE_RIGHT)
            {
                // Prepare for operation
                startPageState = startDone;
                cmdif_remotegui_messagebox(cmd,
                                           "CRANK RELEARN", CMDIF_REMOTEGUI_ICON_NONE,
                                           CMDIF_REMOTEGUI_BUTTONFACE_None, CMDIF_REMOTEGUI_BUTTONFACE_None, CMDIF_REMOTEGUI_BUTTONFACE_None,
                                           CMDIF_REMOTEGUI_FOCUS_None,
                                           "<HEADER>INITIALIZING</HEADER>\nPlease wait...");
                obd2can_ford_initiate_diagnostic_operation(FALSE,Obd2CanEcuId_7E0,
                                           extendedDianosticSession_14229,TRUE);
            }
            else if (responsetype == CMDIF_REMOTEGUI_RESPONSETYPE_BUTTON && 
                     responsechoice == CMDIF_REMOTEGUI_BUTTONTYPE_MIDDLE) 
            {
                startPageState = startPage3;
            }
            else
            {
                startPageState = startAbort;
            }
            break;
        case startAbort:
            return;
        default:
            startPageState = startAbort;
            break;
        }
    }
    
    // Crank Relearn Process State Machine
    processState = processCheckPreConditions;
    while (processState != processDone)
    {
        switch (processState)
        {
        case processCheckPreConditions: // Make sure conditions are correct
            // Get RPM
            *datalength = 2;
            status = obd2can_read_data_bypid(Obd2CanEcuId_7E0, 0xF40C, data, datalength,TRUE);                    
            RPM += (float)((data[0] << 8) + data[1]) * 0.25; // BP = 2, multiply by 0.5 ^ 2

            // Get Current Learn Status (0 = Incomplete, 1 = Complete)
            if (status == S_SUCCESS)
            {
                *datalength = 4;
                status = obd2can_read_data_bypid(Obd2CanEcuId_7E0, 0x0700, data, datalength,TRUE);                    
                MpLearn = (data[1] & 0x80) >> 7;
            }            
            // Get Learning Enabled Status (0 = False, 1 = True)
            if (status == S_SUCCESS)
            {
                *datalength = 4;
                status = obd2can_read_data_bypid(Obd2CanEcuId_7E0, 0x03AD, data, datalength,TRUE);                    
                LearnEnabled = (data[0] & 0x20) >> 5;
            }        
            // Get Down Count
            if (status == S_SUCCESS)
            {
                *datalength = 2;                        
                status = obd2can_read_data_bypid(Obd2CanEcuId_7E0, 0x03EF, data, datalength,TRUE);
                DownCount = (data[0] << 8) + data[1];                
            }
            // Catch all error handler
            if (status != S_SUCCESS)
            {
                sprintf((void*)displayData, "<HEADER>ERROR</HEADER>\nFailed to initialize.");                       
                processState = processFail;
                break;
            }            
            // Check idle RPM
            if (RPM < CRANK_RELEARN_IDLE_MIN || RPM > CRANK_RELEARN_IDLE_MAX)
            {
                sprintf((void*)displayData, "<HEADER>ERROR</HEADER>\nIdle RPM Out of Range.");                       
                processState = processFail;
                break;
            }
            // Get confirmation if previous profile learn has been completed
            if (MpLearn == 1)
            {
                cmdif_remotegui_messagebox(cmd,
                                           "CRANK RELEARN", CMDIF_REMOTEGUI_ICON_NONE,
                                           CMDIF_REMOTEGUI_BUTTONFACE_Exit, CMDIF_REMOTEGUI_BUTTONFACE_Continue, CMDIF_REMOTEGUI_BUTTONFACE_None,
                                           CMDIF_REMOTEGUI_FOCUS_Button0,
                                           "<HEADER>ERASE PROFILE?</HEADER>\nSelect CONTINUE to erase the existing Misfire Profile Correction and enable learning of a new profile.");            
                cmdif_remotegui_wait_user_response(&responsetype, &responsechoice, NULL);

                if (responsetype == CMDIF_REMOTEGUI_RESPONSETYPE_BUTTON && 
                    responsechoice == CMDIF_REMOTEGUI_BUTTONTYPE_RIGHT)
                {
                    obd2can_ford_initiate_diagnostic_operation(FALSE,Obd2CanEcuId_7E0,
                                           extendedDianosticSession_14229,TRUE);                    
                    processState = processStart;
                }
                else
                {
                    processState = processAbort;
                }
            }
            else
            {
                processState = processStart;
            }
            break;
        case processStart:          // Start crank relearn procedure            
            sprintf((void*)statusMsg, "Waiting for minimum ECT\0");
            timeout = rtc_getvalue();
            guiRefresh = timeout;
            TargetRPM = 0;
 
            // Main procedure loop
            while(1)
            {
                // Check for timeout
                if ((rtc_getvalue() - timeout) >= CRANK_RELEARN_TIMEOUT)
                {                    
                    sprintf((void*)displayData, "<HEADER>ERROR</HEADER>\nOperation timeout\0");
                    processState = processFail;                    
                    break;
                }

                //--------------------------------------------------------------
                // Poll Data 
                
                // Get RPM
                *datalength = 2;
                status = obd2can_read_data_bypid(Obd2CanEcuId_7E0, 0xF40C, data, datalength,TRUE);                    
                RPM = (float)((data[0] << 8) + data[1]) * 0.25; // BP = 2, multiply by 0.5 ^ 2
                
                if (status == S_SUCCESS)
                {
                    if (processState != processCommandSent)
                    {
                        // Get ECT
                        *datalength = 2;                        
                        status = obd2can_read_data_bypid(Obd2CanEcuId_7E0, 0x038F, data, datalength,TRUE);                    
                        ECT = (float)((data[0] << 8) + data[1]) * 0.015625; // BP = 6, multiply by 0.5 ^ 6
                        
                        if (status == S_SUCCESS)
                        {
                            // Get AC Relay
                            *datalength = 2;                        
                            status = obd2can_read_data_bypid(Obd2CanEcuId_7E0, 0x099B, data, datalength,TRUE);                    
                            AcRelay = (data[1] >> 5) & 0x01;
                        }
                        if (status == S_SUCCESS)
                        {
                            // Check idle RPM
                            if (RPM < CRANK_RELEARN_IDLE_MIN || RPM > CRANK_RELEARN_IDLE_MAX)
                            {
                                sprintf((void*)displayData, "<HEADER>ERROR</HEADER>\nIdle RPM Out of Range.");                       
                                processState = processFail;
                                break;
                            }
                        }
                    }
                    // Get Down Count
                    else
                    {
                        *datalength = 2;                        
                        status = obd2can_read_data_bypid(Obd2CanEcuId_7E0, 0x03EF, data, datalength,TRUE);
                        DownCount = (data[0] << 8) + data[1];
                    }
                }
                if (status != S_SUCCESS)
                {
                    sprintf((void*)displayData, "<HEADER>ERROR</HEADER>\nOperation timeout\0");
                    processState = processFail;                    
                    break;
                }
                else
                {
                    timeout = rtc_getvalue();
                }
                
                //--------------------------------------------------------------                
                // Update GUI                
                if ((rtc_getvalue() - guiRefresh) >= CRANK_RELEARN_REFRESH)
                {                    
                    if (processState == processCommandSent)
                    {
                        sprintf((void*)displayData, "<HEADER>STEP 5</HEADER>\nRPM: %5.0F\nTarget RPM: %5.0F\nECT: %3.0F C\n<HEADER>%s</HEADER>", RPM, TargetRPM, ECT, statusMsg);
                    }
                    else
                    {
                        sprintf((void*)displayData, "<HEADER>STEP 5</HEADER>\nRPM: %5.0F\nTarget RPM: Not Acquired\nECT: %3.0F C\n<HEADER>%s</HEADER>", RPM, ECT, statusMsg);
                    }
                    
                    cmdif_remotegui_messagebox(cmd,
                            "CRANK RELEARN", CMDIF_REMOTEGUI_ICON_NONE,
                            CMDIF_REMOTEGUI_BUTTONFACE_Abort, CMDIF_REMOTEGUI_BUTTONFACE_None, CMDIF_REMOTEGUI_BUTTONFACE_None,
                            CMDIF_REMOTEGUI_FOCUS_None,
                            displayData);
                    
                    guiRefresh = rtc_getvalue();
                }
                // Check for GUI response
                status = cmdif_remotegui_peek_user_response(&responsetype, &responsechoice, NULL);
                if (status == S_SUCCESS && responsetype == CMDIF_REMOTEGUI_RESPONSETYPE_BUTTON)
                {
                    processState = processAbort;
                    break;
                }
                // Check if A/C is on
                if (AcRelay != 0)
                {
                    sprintf((void*)displayData, "<HEADER>ERROR</HEADER>\nA/C must be kept off\0");
                    processState = processFail;                    
                    break;
                }
                // Check RPM
                if ((MaxRPM < RPM) && (MaxRPM > TargetRPM) && (processState == processCommandSent))
                {                    
                    sprintf((void*)statusMsg, "Return to Idle RPM\0");
                    targetReached = 1;
                }
                else
                {
                    MaxRPM = RPM;
                }
                if (targetReached == 1 && (RPM < CRANK_RELEARN_RPM_FALL_THRESH) && DownCount > 0)
                {
                    sprintf((void*)statusMsg, "Rev engine to Target RPM\0");
                    targetReached = 0;
                    MaxRPM = 0;
                    retry++;
                    if (retry > 3)
                    {
                        sprintf((void*)displayData, "<HEADER>ERROR</HEADER>\nMaximum attempts exceeded. Please restart the procedure.\0");
                        processState = processFail;
                        break;
                    }
                }
                // Check if we're done
                if(processState == processCommandSent && DownCount == 0x0)
                {                    
                    // Get Learning Enabled Status (0 = False, 1 = True)
                    *datalength = 4;
                    status = obd2can_read_data_bypid(Obd2CanEcuId_7E0, 0x03AD, data, datalength,TRUE);                    
                    LearnEnabled = (data[0] & 0x20) >> 5;
                    
                    if (status == S_SUCCESS && LearnEnabled == 0)
                    {
                        processState = processSuccess;
                    }
                    else
                    {
                        sprintf((void*)displayData, "<HEADER>ERROR</HEADER>\nCrank Relearn Failure\0");
                        processState = processFail;
                    }
                    break;            
                }
                // Send command if minimum coolant temperature is met                
                if((ECT > CRANK_RELEARN_ECT_MIN) && (processState != processCommandSent))
                {
                    // Send First Command
                    data[0] = 0x40;
                    data[1] = 0x36;
                    data[2] = 0x02;
                    data[3] = 0x00;                  
                    status = obd2can_ford_start_diagnostic_routine_bytestnumber(
                                            Obd2CanEcuId_7E0,0x01,data,4,commlevel);
                    // Send Second Command
                    if (status == S_SUCCESS)
                    {
                        data[0] = 0x40;
                        data[1] = 0x36;
                        data[2] = 0x01;
                        data[3] = 0x00;
                        status = obd2can_ford_start_diagnostic_routine_bytestnumber(
                                                Obd2CanEcuId_7E0,0x01,data,4,commlevel);
                    }
                    // Unlock ECM
                    if (status == S_SUCCESS)
                    {
                        *datalength = 8;
                        status = obd2can_ford_security_access(Obd2CanEcuId_7E0, 
                                        DevCtrlrequestSeed, seed, datalength,0);
                            
                        if (status == S_SUCCESS)
                        {                            
                            status = obd2_ford_generatekey(seed, UnlockId_OSC, CommType_CAN, key);
                                 
                        }                        
                        if (status == S_SUCCESS)
                        {
                            *datalength = 3;
                            status = obd2can_ford_security_access(Obd2CanEcuId_7E0, 
                                        DevCtrlsendKey, key, datalength,0);
                        }
                    }
                    // Clear Canister Purge Duty Cycle
                    if (status == S_SUCCESS)
                    {
                        data[0] = 0x00;
                        data[1] = 0x00;
                        status = obd2can_ford_input_output_control(
                            Obd2CanEcuId_7E0,0x1166,0x03,data,2,commlevel,
                            TRUE);
                    }
                    if (status == S_SUCCESS)
                    {
                        // Get Learning Enabled Status (0 = False, 1 = True)
                        *datalength = 4;
                        status = obd2can_read_data_bypid(Obd2CanEcuId_7E0, 0x03AD, data, datalength,TRUE);                    
                        LearnEnabled = (data[0] & 0x20) >> 5;
                    }
                    if (status == S_SUCCESS)
                    {
                        // Get Current Learn Status (0 = Incomplete, 1 = Complete)
                        *datalength = 4;
                        status = obd2can_read_data_bypid(Obd2CanEcuId_7E0, 0x0700, data, datalength,TRUE);                    
                        MpLearn = (data[1] & 0x80) >> 7;
                    }
                    if (status == S_SUCCESS)
                    {
                        // Get Down Count
                        *datalength = 2;                        
                        status = obd2can_read_data_bypid(Obd2CanEcuId_7E0, 0x03EF, data, datalength,TRUE);
                        DownCount = (data[0] << 8) + data[1];                
                    }
                    if (status == S_SUCCESS)
                    {
                        // Get Target RPM
                        *datalength = 2;                        
                        status = obd2can_read_data_bypid(Obd2CanEcuId_7E0, 0x03FD, data, datalength,TRUE);
                        TargetRPM = (float)((data[0] << 8) + data[1]) * 0.25; // BP = 2, multiply by 0.5 ^ 2
                    }
                    // Check conditions
                    if (status == S_SUCCESS && LearnEnabled == 1 && MpLearn == 0)
                    {
                        sprintf((void*)statusMsg, "Rev engine to Target RPM\0");
                        processState = processCommandSent;
                        retry = 0;
                    }
                    else                    
                    {
                        // First attempt after learning has already been successful
                        // will fail. Allow one retry which usually resolves this.
                        if (retry > 0)
                        {
                            sprintf((void*)displayData, "<HEADER>ERROR</HEADER>\nCrank Relearn Failure\0");
                            processState = processFail;
                            break;
                        }
                        retry++;
                        delays(100,'m');
                    }
                } // Send Command
            } // while(1)
            break;
        case processFail:           // Handle failure            
            obd2can_ford_initiate_diagnostic_operation(FALSE,Obd2CanEcuId_7E0,
                                               defaultSession_14229,TRUE);
            cmdif_remotegui_messagebox(cmd,
                                       "CRANK RELEARN", CMDIF_REMOTEGUI_ICON_NONE,
                                       CMDIF_REMOTEGUI_BUTTONFACE_Exit, CMDIF_REMOTEGUI_BUTTONFACE_None, CMDIF_REMOTEGUI_BUTTONFACE_None,
                                       CMDIF_REMOTEGUI_FOCUS_None,
                                       displayData);        
            cmdif_remotegui_wait_user_response(&responsetype, &responsechoice, NULL);

            processState = processDone;
            break;
        case processAbort:          // Handle abort            
            obd2can_ford_initiate_diagnostic_operation(FALSE,Obd2CanEcuId_7E0,
                                               defaultSession_14229,TRUE);
            cmdif_remotegui_messagebox(cmd,
                                       "CRANK RELEARN", CMDIF_REMOTEGUI_ICON_NONE,
                                       CMDIF_REMOTEGUI_BUTTONFACE_Exit, CMDIF_REMOTEGUI_BUTTONFACE_None, CMDIF_REMOTEGUI_BUTTONFACE_None,
                                       CMDIF_REMOTEGUI_FOCUS_None,
                                       "<HEADER>CRANK RELEARN ABORTED</HEADER>");        
            cmdif_remotegui_wait_user_response(&responsetype, &responsechoice, NULL);

            processState = processDone;
            break;
        case processSuccess:        // Handle success            
            obd2can_ford_initiate_diagnostic_operation(FALSE,Obd2CanEcuId_7E0,
                                               defaultSession_14229,TRUE);
            cmdif_remotegui_messagebox(cmd,
                                       "CRANK RELEARN", CMDIF_REMOTEGUI_ICON_NONE,
                                       CMDIF_REMOTEGUI_BUTTONFACE_Exit, CMDIF_REMOTEGUI_BUTTONFACE_None, CMDIF_REMOTEGUI_BUTTONFACE_None,
                                       CMDIF_REMOTEGUI_FOCUS_None,
                                       "<HEADER>CRANK RELEARN SUCCESSFUL</HEADER>");        
            cmdif_remotegui_wait_user_response(&responsetype, &responsechoice, NULL);

            processState = processDone;
            break;
        default:            
            break;            
        } // switch(..)
    } // while(..)
}
