/**
  **************** (C) COPYRIGHT 2011 SCT Performance, LLC *********************
  * File Name          : obd2tune_ford_upload.c
  * Device / System    : SCT products using the "CommonCode" platform
  * Author             : Quyen Leba
  *
  * Version            : 1 
  * Date               : 05/02/2011
  * Description        : 
  *                    : 
  *
  *
  * History            : 05/02/2011 M. Davis
  *                    :   Add SCT Copyright header,
  *
  ******************************************************************************
  */

#include <board/delays.h>
#include <common/statuscode.h>
#include <common/obd2.h>
#include <common/obd2scp.h>
#include <common/genmanuf.h>
#include <common/genmanuf_overload.h>
#include "obd2tune_ford_upload.h"

/*
u8 obd2tune_ford_upload(u8 ecm_type, u32 current_address, u16 bytetoupload,
                        u8 *readbuffer, u16 *readlength)
{
    VehicleCommType vehiclecommtype;
    VehicleCommLevel vehiclecommlevel;
    MemoryAddressType address_type;
    RequestLengthType request_type;
    u16 maxuploadblocklength;
    u8  status;
    
    vehiclecommtype = ECM_GetCommType(ecm_type);
    vehiclecommlevel = ECM_GetCommLevel(ecm_type);
    if (ECM_IsUploadAddr4B(ecm_type))
    {
        address_type = Address_32_Bits;     //TODOQ: simplify this
        request_type = Request_4_Byte;
    }
    else
    {
        address_type = Address_24_Bits;
        request_type = Request_3_Byte;
    }
    
    if (vehiclecommtype == CAN_COMM)
    {
        if (ECM_IsUploadByCmd23(ecm_type))
        {
            status = obd2can_read_memory_address
                (ECM_GetEcmId(ecm_type), current_address, bytetoupload,
                 address_type, ECM_GetRxFrameLengthCorrection(ecm_type),
                 readbuffer,readlength);
        }
        else
        {
            status = obd2can_request_upload(ECM_GetEcmId(ecm_type),
                                            current_address, bytetoupload,
                                            request_type,vehiclecommlevel,
                                            &maxuploadblocklength);
            if (status == S_SUCCESS)
            {
                status = obd2can_transfer_data_upload
                    (ECM_GetEcmId(ecm_type), readbuffer, readlength);
            }
        }
        delays(50,'m');
    }
#if (SUPPORT_COMM_VPW)
    //###############################
    // get data from ECM by VPW
    //###############################
    else if (vehiclecommtype == VPW_COMM)
    {
        if (ECM_IsUploadByCmd35Single36(ecm_type))
        {
            u32 feedbackaddress;
            status = obd2vpw_request_upload(current_address,bytetoupload);
            if (status == S_SUCCESS)
            {
                status = obd2vpw_transfer_data_upload
                    (&readbuffer[readbufferindex],
                     &feedbackaddress,&readlength);
            }
        }
        else
        {
            // so far (Feb052010), all VPW ECMs we flash use $35 to upload
            // this case is $23, and probably never need to implemented
            status = S_FAIL;
        }
    }
#endif
#if (SUPPORT_COMM_SCP)
    //###############################
    // get data from ECM by SCP
    //###############################
    else if (vehiclecommtype == SCP_COMM || vehiclecommtype == SCP32_COMM)
    {
        MemoryAddressType addrtype;
        
        if (vehiclecommtype == SCP32_COMM)
        {
            addrtype = Address_32_Bits;
        }
        else    //SCP_COMM
        {
            addrtype = Address_24_Bits;
        }
        
        if (ECM_IsUploadByCmd35Multi36(ecm_type))
        {
            //TODOQ: this is not yet multi36 handled
            //do a $35
            status = obd2scp_request_upload(current_address,bytetoupload,
                                            addrtype);
            if (status == S_SUCCESS)
            {
                //receive data from $36
                status = obd2scp_transfer_data_upload(readbuffer,bytetoupload);
                
                if (status == S_SUCCESS)
                {
                    //do a $37
                    status = obd2scp_request_transfer_exit
                        (addrtype,TransferExitFromUpload);
                }
            }
            //TODOQ: this is not yet multi36 handled
            status = S_FAIL;
        }
        else if (ECM_IsUploadByCmd23(ecm_type))
        {
            status = obd2scp_read_memory_address
                (current_address,bytetoupload,addrtype,readbuffer,readlength);
        }
        else
        {
            //invalid upload method
            status = S_FAIL;
        }
    }
#endif
    //###############################
    // get data from ECM by ???
    //###############################
    else
    {
        status = S_COMMTYPE;
    }
    
    return status;
}
*/
