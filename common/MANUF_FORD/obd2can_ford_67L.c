/**
  **************** (C) COPYRIGHT 2011 SCT Performance, LLC *********************
  * File Name          : obd2can_ford_60L.c
  * Device / System    : SCT products using the "CommonCode" platform
  * Author             : Patrick Downs
  *
  * Version            : 1 
  * Date               : 05/02/2011
  * Description        : Specific for 6.7L
  *                    : 
  *
  *
  * History            : 05/02/2011 M. Davis
  *                    :   Add SCT Copyright header,
  *
  ******************************************************************************
  */

#include <fs/genfs.h>
#include <common/statuscode.h>
#include <common/filestock.h>
#include <common/ftoa.h>
#include <common/itoa.h>
#include "obd2can_ford.h"
#include "obd2can_ford_67L.h"

//------------------------------------------------------------------------------
// Check file for modifcations that indicate it is not a stock file
//
// IMPORTANT: This uses filestock functions which, during a download, may already
//            be open. If stock file is already open this function will always FAIL.
//
// The basic logic for finding whether a file is modified or stock is simple:
// All OEM files by default have a 768 byte block of 0xFF at 0x1EFC00.
// Modified tune files all change this block in some way due to the behavior 
// of the modified bootloader.
// Thus, by reading this block out and checking to see that all values are
// 0xFF, we can check that whether the tune is modified or OEM.
//
// Input:  char *filename - filename of file to be checked
// Return:  u8  status - S_FAIL - File is STOCK
//                     - S_SUCCESS - File is MODIFIED
// Engineer: Patrick Downs (ported from X3P code)
//------------------------------------------------------------------------------
u8 obd2can_ford_67L_IsTunefileModified(void)
{
    char buffer[768];
    int i;
    u32 status;
    u32 bytecount;
    FileStock_Info filestock_info;
    u32 currentfileposition;
    
    currentfileposition = 0;
    filestock_info = filestock_getinfo();
    
    if(filestock_info.control.filehandlingtype == FILESTOCK_HANDLING_STOCKFILE)
    {
        return S_FAIL; // Assume we're in download and stock file is open
    }
    else if(filestock_info.control.filehandlingtype == FILESTOCK_HANDLING_FLASHFILE)
    {
        filestock_get_position(&currentfileposition); // Flash already open save current position
    }
    else // No files open
    {
        status = filestock_openflashfile("r");
        if(status != S_SUCCESS) return status;           
    }
    
    status = filestock_read_at_position(0x1EFC00, (u8*)buffer, 768, (u32*)&bytecount);
    if(status != S_SUCCESS) goto IsTunefileModified_DONE;
    
    if(bytecount != 768)
    {
        status = S_FAIL;
        goto IsTunefileModified_DONE;
    }

    // Check each byte against 0xFF. If any bytes are not 0xFF, we
    // declare the file modified.
    for (i = 0; i < 768; i++)
        if (buffer[i] != 0xFF) break;    
    
IsTunefileModified_DONE:    
    if(filestock_info.control.filehandlingtype == FILESTOCK_HANDLING_NONE)
       filestock_closeflashfile(); // Done with file
    else
        filestock_set_position(currentfileposition);    
                
    if(status == S_SUCCESS)
    {
        if (i == 768) 
            return S_FAIL; // All bytes are 0xff, file is STOCK
        else
            return S_SUCCESS; // Otherwise, the file was modified.
    }
    
    return status;
}

//------------------------------------------------------------------------------
// Get only axle ratio using Ford Method 1 (2011+ 6.7L, 5.0L, etc)
// Outputs: u32 *axle_ratio (/100 for display purpose value)
//                          (range: [330-490])
//
// Return:  u8  status
// Engineer: Patrick Downs
// Note: require ABS module; i.e. won't be S_SUCCESS on a bench
//------------------------------------------------------------------------------
u8 obd2can_ford_m1_get_axle_ratio(u32 *axle_ratio)
{
    u8 status;
    u32 tmp_axelratio;
    u32 tmp_tiresize;
    
    status = obd2can_ford_67L_get_axle_ratio_tire_size((u32*)&tmp_axelratio, (u32*)&tmp_tiresize);
    if(status == S_SUCCESS)
    {
        *axle_ratio = tmp_axelratio;
    }
    
    return status;
}

//------------------------------------------------------------------------------
// Get only tire size using Ford Method 1 (2011+ 6.7L, 5.0L, etc)
//          u32 *tire_size (in PPM, /77.18 for tire size in inch)
//                         (range: [2410-2635])
// Return:  u8  status
// Engineer: Patrick Downs
// Note: require ABS module; i.e. won't be S_SUCCESS on a bench
//------------------------------------------------------------------------------
u8 obd2can_ford_m1_get_tire_size(u32 *tire_size)
{
    u8 status;
    u32 tmp_axelratio;
    u32 tmp_tiresize;
    
    status = obd2can_ford_67L_get_axle_ratio_tire_size((u32*)&tmp_axelratio, (u32*)&tmp_tiresize);
    if(status == S_SUCCESS)
    {
        *tire_size = tmp_tiresize;
    }
    
    return status;
}

//------------------------------------------------------------------------------
// Get tire size & axle ratio of a 6.7L
// Outputs: u32 *axle_ratio (/100 for display purpose value)
//                          (range: [330-490])
//          u32 *tire_size (in PPM, /77.18 for tire size in inch)
//                         (range: [2410-2635])
// Return:  u8  status
// Engineer: Quyen Leba
// Note: require ABS module; i.e. won't be S_SUCCESS on a bench
//------------------------------------------------------------------------------
u8 obd2can_ford_67L_get_axle_ratio_tire_size(u32 *axle_ratio, u32 *tire_size)
{
    u8  data[64];
    u8  datalength;
    u16 tmp_u16;
    u8  status;

    obd2can_init(Obd2canInitCustom,0x720);
    status = obd2can_read_data_bypid(0x726,0xDE2C,data,&datalength,TRUE);
    if (status == S_SUCCESS && datalength >= 4)
    {
        tmp_u16 = (u16)data[0] << 8 | data[1];
        *axle_ratio = (u32)tmp_u16;

        tmp_u16 = (u16)data[2] << 8 | data[3];
        *tire_size = (u32)tmp_u16;
    }
    else
    {
        status = S_FAIL;
    }
    obd2can_init(Obd2canInitProgram,0);
    return status;
}

//------------------------------------------------------------------------------
// Set only axle ratio using Ford Method 1 (2011+ 6.7L, 5.0L, etc)
// Inpputs: u32 axle_ratio (/100 for display purpose value)
//                         (range: [330-490])
// Return:  u8  status
// Engineer: Patrick Downs
// Note: require ABS module; i.e. won't be S_SUCCESS on a bench
//------------------------------------------------------------------------------
u8 obd2can_ford_m1_set_axle_ratio(u32 axle_ratio)
{
    u8 status;
    
    status = obd2can_ford_67L_set_axle_ratio_tire_size(axle_ratio, NULL);
    
    return status;
}

//------------------------------------------------------------------------------
// Set only tire size using Ford Method 1 (2011+ 6.7L, 5.0L, etc)
// Inpputs: u32 tire_size (in PPM, /77.18 for tire size in inch)
//                        (range: [2410-2635])
// Return:  u8  status
// Engineer: Patrick Downs
// Note: require ABS module; i.e. won't be S_SUCCESS on a bench
//------------------------------------------------------------------------------
u8 obd2can_ford_m1_set_tire_size(u32 tire_size)
{
    u8 status;
    
    status = obd2can_ford_67L_set_axle_ratio_tire_size(NULL, tire_size);
    
    return status;
}

//------------------------------------------------------------------------------
// Set axle ratio & tire size of a 6.7L
// Inpputs: u32 axle_ratio (/100 for display purpose value)
//                         (range: [330-490])
//          u32 tire_size (in PPM, /77.18 for tire size in inch)
//                        (range: [2410-2635])
// Return:  u8  status
// Engineer: Quyen Leba
// Note: require ABS module; i.e. won't be S_SUCCESS on a bench
//------------------------------------------------------------------------------
u8 obd2can_ford_67L_set_axle_ratio_tire_size(u32 axle_ratio, u32 tire_size)
{
#define AXLE_RATIO_67L_LOW              331
#define AXLE_RATIO_67L_HIGH             488
#define TIRE_SIZE_67L_LOW               2200
#define TIRE_SIZE_67L_HIGH              2630
    Obd2can_ServiceData servicedata;
    u32 curr_tire_size, curr_axle_ratio;
    u8  data[64];
    u8  datalength;
    u16 tmp_u16;
    u8  status;

    if(axle_ratio != NULL)
    {
        //force axle_ratio to be within valid range        
        if (axle_ratio > AXLE_RATIO_67L_HIGH)
        {
            axle_ratio = AXLE_RATIO_67L_HIGH;
        }
        else if (axle_ratio < AXLE_RATIO_67L_LOW)
        {
            axle_ratio = AXLE_RATIO_67L_LOW;
        }
    }
    if(tire_size != NULL)
    {
        //force tire_size to be within valid range
        if (tire_size > TIRE_SIZE_67L_HIGH)
        {
            tire_size = TIRE_SIZE_67L_HIGH;
        }
        else if (tire_size < TIRE_SIZE_67L_LOW)
        {
            tire_size = TIRE_SIZE_67L_LOW;
        }
    }

    obd2can_init(Obd2canInitCustom,0x720);
    status = obd2can_read_data_bypid(0x726,0xDE2C,data,&datalength,TRUE);
    if(status == S_SUCCESS && datalength >= 6)
    {        
        curr_axle_ratio = (u16)(data[0] << 8 | data[1]);        
        curr_tire_size  = (u16)(data[2] << 8 | data[3]);
        
        if(((curr_axle_ratio == axle_ratio) || (axle_ratio == NULL)) &&
           ((curr_tire_size == tire_size) || (tire_size == NULL)))
        {
            status = S_SUCCESS; // No changes needed
        }
        else
        {
            if(axle_ratio == NULL)
            {
                axle_ratio = curr_axle_ratio;
            }
            if(tire_size == NULL)
            {
                tire_size = curr_tire_size;
            }
            
            obd2can_servicedata_init(&servicedata);
            servicedata.ecm_id = 0x726;
            servicedata.service = 0x2E; //not yet defined in FORD_ServiceNumber
            
            //0xDE2C PID
            servicedata.service_controldatabuffer[0] = 0xDE;
            servicedata.service_controldatabuffer[1] = 0x2C;
            servicedata.service_controldatalength = 2;
            
            tmp_u16 = (u16)axle_ratio;
            data[0] = tmp_u16 >> 8;
            data[1] = tmp_u16 & 0xFF;
            tmp_u16 = (u16)tire_size;
            data[2] = tmp_u16 >> 8;
            data[3] = tmp_u16 & 0xFF;
            servicedata.txdata = data;
            servicedata.txdatalength = datalength;
            
            status = obd2can_txrx(&servicedata);
            if (status != S_SUCCESS)
            {
                status = obd2can_ford_initiate_diagnostic_operation(FALSE,0x726,
                                                                    (SubServiceNumber)0x03,    //not sure defined name of this 0x03
                                                                    TRUE);
                if (status == S_SUCCESS)
                {
                    status = obd2can_txrx(&servicedata);
                }
            }
        }
    }
    else
    {
        status = S_FAIL;
    }

    obd2can_init(Obd2canInitProgram,0);
    return status;
}

//------------------------------------------------------------------------------
// Get TPMS front tire PSI setting method 1 (2011+ 6.7L, 5.0L, 6.2L, & 3.5L Ecoboost)
// Outputs: u8  *front_tire_pressure (in PSI, valid range [30-115])
// Return:  u8  status
// Engineer: Patrick Downs
// Note: require ABS module; i.e. won't be S_SUCCESS on a bench
//------------------------------------------------------------------------------
u8 obd2can_ford_m1_get_tpms_front(u8 *front_tire_pressure)
{
    u8 status;
    u8 tmp_front;
    u8 tmp_rear;
    
    status = obd2can_ford_67L_get_tpms_info(&tmp_front, &tmp_rear);
    if(status == S_SUCCESS)
    {
        *front_tire_pressure = tmp_front;
    }
    
    return status;
}

//------------------------------------------------------------------------------
// Get TPMS rear tire PSI setting method 1 (2011+ 6.7L, 5.0L, 6.2L, & 3.5L Ecoboost)
// Outputs: u8  *rear_tire_pressure (in PSI, valid range [30-115])
// Return:  u8  status
// Engineer: Patrick Downs
// Note: require ABS module; i.e. won't be S_SUCCESS on a bench
//------------------------------------------------------------------------------
u8 obd2can_ford_m1_get_tpms_rear(u8 *rear_tire_pressure)
{
    u8 status;
    u8 tmp_front;
    u8 tmp_rear;
    
    status = obd2can_ford_67L_get_tpms_info(&tmp_front, &tmp_rear);
    if(status == S_SUCCESS)
    {
        *rear_tire_pressure = tmp_rear;
    }
    
    return status;
}

//------------------------------------------------------------------------------
// Get TPMS info of a 6.7L (and 2013 F-150 3.5L Ecoboost)
// Outputs: u8  *front_tire_pressure (in PSI, valid range [30-115])
//          u8  *rear_tire_pressure (in PSI, valid range [30-115])
// Return:  u8  status
// Engineer: Quyen Leba
// Note: require ABS module; i.e. won't be S_SUCCESS on a bench
//------------------------------------------------------------------------------
u8 obd2can_ford_67L_get_tpms_info(u8 *front_tire_pressure, u8 *rear_tire_pressure)
{
#define TIRE_PRESSURE_67L_LOW           30
#define TIRE_PRESSURE_67L_HIGH          115
    u8  data[64];
    u8  datalength;
    u8  status;
    u8 front;
    u8 rear;

    *front_tire_pressure = 0;
    *rear_tire_pressure = 0;

    obd2can_init(Obd2canInitCustom,0x720);
    status = obd2can_read_data_bypid(0x726,0xDE15,data,&datalength,TRUE);
    if (status == S_SUCCESS && datalength >= 4)
    {
        front = data[2];
        rear = data[3];
        
        // check values
        if(front < TIRE_PRESSURE_67L_LOW || front > TIRE_PRESSURE_67L_HIGH ||
            rear < TIRE_PRESSURE_67L_LOW || rear > TIRE_PRESSURE_67L_HIGH)
        {
            log_push_error_point(0x4591); // Bad TPMS values
            status = S_ERROR;
        }
        else
        {   
            *front_tire_pressure = data[2];
            *rear_tire_pressure = data[3];
        }
    }
    else
    {
        status = S_FAIL;
    }
    
    obd2can_init(Obd2canInitProgram,0);

    return status;
}

//------------------------------------------------------------------------------
// Set TPMS front tire PSI setting method 1 (2011+ 6.7L, 5.0L, 6.2L, & 3.5L Ecoboost)
// Inputs:  u8  front_tire_pressure (in PSI, valid range [30-115])
//          u8  rear_tire_pressure (in PSI, valid range [30-115])
// Return:  u8  status
// Engineer: Quyen Leba
// Note: require ABS module; i.e. won't be S_SUCCESS on a bench
//------------------------------------------------------------------------------
u8 obd2can_ford_m1_set_tpms_front(u8 front_tire_pressure)
{
    return obd2can_ford_67L_set_tpms_info(front_tire_pressure, NULL);    
}

//------------------------------------------------------------------------------
// Set TPMS rear tire PSI setting method 1 (2011+ 6.7L, 5.0L, 6.2L, & 3.5L Ecoboost)
// Inputs:  u8  rear_tire_pressure (in PSI, valid range [30-115])
// Return:  u8  status
// Engineer: Quyen Leba
// Note: require ABS module; i.e. won't be S_SUCCESS on a bench
//------------------------------------------------------------------------------
u8 obd2can_ford_m1_set_tpms_rear(u8 rear_tire_pressure)
{
    return obd2can_ford_67L_set_tpms_info(NULL, rear_tire_pressure);
}    

//------------------------------------------------------------------------------
// Set TPMS info of a 6.7L (and 2013 F-150 3.5L Ecoboost)
// Inputs:  u8  front_tire_pressure (in PSI, valid range [30-115])
//          u8  rear_tire_pressure (in PSI, valid range [30-115])
// Return:  u8  status
// Engineer: Quyen Leba
// Note: require ABS module; i.e. won't be S_SUCCESS on a bench
//------------------------------------------------------------------------------
#define TPMS_DISABLE    0xFF
u8 obd2can_ford_67L_set_tpms_info(u8 front_tire_pressure, u8 rear_tire_pressure)
{
    u32 ecm_id;
    u16 pid;
    u16 datalength;
    u8  data[64];    
    u8  new_rear;
    u8  new_front;
    u8  status;
    
    // Check input params are within valid range
    // Null = keep current value, 0xFF = disable TPMS
    status = S_SUCCESS;
    if(front_tire_pressure != NULL && front_tire_pressure != TPMS_DISABLE) 
    {
        if(front_tire_pressure > TIRE_PRESSURE_67L_HIGH || front_tire_pressure < TIRE_PRESSURE_67L_LOW)
        {        
            log_push_error_point(0x4592);
            status = S_INPUT; // Bad input values
        }
    }
    if(rear_tire_pressure != NULL && rear_tire_pressure != TPMS_DISABLE)
    {
        if(rear_tire_pressure > TIRE_PRESSURE_67L_HIGH || rear_tire_pressure < TIRE_PRESSURE_67L_LOW)
        {
            log_push_error_point(0x4593);
            status = S_INPUT; // Bad input values
        }
    }
    
    if(status == S_SUCCESS)
    {
        obd2can_init(Obd2canInitCustom,0x720);
        ecm_id = 0x726; // Body Control Module (BCM)
        pid = 0xDE15;   // 0xDE15 TPMS PID
        datalength = 0;
        status = obd2can_read_data_bypid(ecm_id,pid,data,(u8*)&datalength,TRUE);
        // Read current values by PID, 4 bytes returned
        // Data[0] TPMS enabled status - 0x00 = disabled, 0x01 = enable
        // Data[1] Don't know what this byte is but don't want to hardcode it, normally 0x04
        // Data[2] Front TPMS warning min threshold in PSI
        // Data[3] Rear TPMS warning min threshold in PSI        
        if(status == S_SUCCESS && datalength >= 4)
        {
            // check values read from PID
            if((data[2] >= TIRE_PRESSURE_67L_LOW && data[2] <= TIRE_PRESSURE_67L_HIGH) &&
               (data[3] >= TIRE_PRESSURE_67L_LOW && data[3] <= TIRE_PRESSURE_67L_HIGH))
            {
                if(front_tire_pressure == TPMS_DISABLE || rear_tire_pressure == TPMS_DISABLE) // Need to disable TPMS
                {
                    if(data[0] == 0x00)
                    {
                        status = S_SUCCESS; // Already disabled, do nothing
                    }
                    else
                    {
                        data[0] = 0x00;    // (0x00) This changes to 0x00 to disable TPMS
                        // Leave data[1]-data[3] values from PID read unchanged
                        datalength = 4;
                        status = obd2can_write_data_bypid(ecm_id,pid,data,datalength);
                    }
                }
                else
                {
                    if(front_tire_pressure == NULL) // If input value is NULL, keep original value
                    {
                        new_front = data[2];
                    }
                    else
                    {
                        new_front = front_tire_pressure;
                    }
                    if(rear_tire_pressure == NULL)
                    {
                        new_rear = data[3];
                    }
                    else
                    {
                        new_rear = rear_tire_pressure;                
                    }
                    
                    if(new_front == data[2] && new_rear == data[3] && data[0] != 0x00)
                    {                
                        status = S_SUCCESS; // Values already set in vehicle
                    }
                    else
                    {            
                        if(data[0] == 0x00) // If currently disabled, need to re-enable
                        {
                            data[0] = 0x01;    // Set to 0x01 to enable TPMS
                        }
                        data[2] = new_front;
                        data[3] = new_rear;
                        datalength = 4;
                        status = obd2can_write_data_bypid(ecm_id,pid,data,datalength);
                    }
                }
                if (status != S_SUCCESS)
                {
                    // Failed, needs diagnostic mode change, then resend
                    status = obd2can_ford_initiate_diagnostic_operation(FALSE,0x726,
                                                                        (SubServiceNumber)0x03,    //not sure defined name of this 0x03
                                                                        TRUE);
                    if (status == S_SUCCESS)
                    {
                        status = obd2can_write_data_bypid(ecm_id,pid,data,datalength);
                    }
                }
            }
            else
            {
                log_push_error_point(0x4594); // Bad TPMS values
                status = S_ERROR;
            }
        }
        else
        {
            log_push_error_point(0x4595); // Bad TPMS values
            status = S_FAIL;
        }
    }
    
    obd2can_init(Obd2canInitProgram,0);
    
    return status;
}

//------------------------------------------------------------------------------
// Get vehicle info specific to 6.7L (tire size, axle ratio, TPMS)
// Output:  u8  *info_text
// Return:  u8  status
// Engineer: Quyen Leba
// Note: require ABS module; i.e. won't be S_SUCCESS on a bench
//------------------------------------------------------------------------------
u8 obd2can_ford_67L_get_vehicle_info(u8 *info_text)
{
    u8  buffer[32];
    u32 axle_ratio;
    u32 tire_size;
    u8  status;

    status = obd2can_ford_67L_get_axle_ratio_tire_size(&axle_ratio,&tire_size);
    if (status == S_SUCCESS)
    {
        strcat((char*)info_text,"<HEADER>Axle Ratio:</HEADER>");
        ftoa(((float)axle_ratio)/100.0,2,(char*)buffer);
        strcat((char*)info_text,(char*)buffer);

        strcat((char*)info_text,"<HEADER>Tire Size:</HEADER>");
        ftoa(((float)tire_size)/77.18,2,(char*)buffer);
        strcat((char*)info_text,(char*)buffer);
    }
    else
    {
      log_push_error_point(0x4598);
    }
    
    return S_SUCCESS;
}
