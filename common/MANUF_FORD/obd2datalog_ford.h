/**
  **************** (C) COPYRIGHT 2011 SCT Performance, LLC *********************
  * File Name          : obd2datalog_ford.h
  * Device / System    : SCT products using the "CommonCode" platform
  * Author             : Quyen Leba
  *
  * Version            : 1 
  * Date               : 05/02/2011
  * Description        : 
  *                    : 
  *
  *
  * History            : 05/02/2011 M. Davis
  *                    :   Add SCT Copyright header,
  *
  ******************************************************************************
  */

#ifndef __OBD2DATALOG_FORD_H
#define __OBD2DATALOG_FORD_H

#include <arch/gentype.h>
#include <common/obd2def.h>
#include <common/obd2datalog.h>

//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//
//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
u8 obd2datalog_ford_prevalidate_task(u16 ecm_index, VehicleCommType commtype, 
                                     VehicleCommLevel commlevel);

u8 obd2datalog_ford_validateByRapidPacketSetup(u32 ecm_id, DlxBlock *dlxblock,
                                VehicleCommType commtype,
                                VehicleCommLevel commlevel);
u8 obd2datalog_ford_evaluate_fit(VehicleCommType *vehiclecommtype,
                                 VehicleCommLevel *vehiclecommlevel,
                                 u16 *valid_index_list,
                                 u16 *valid_index_count);
u8 obd2datalog_ford_setupdatalogsession(VehicleCommType *vehiclecommtype,
                                      VehicleCommLevel *vehiclecommlevel);
u8 obd2datalog_ford_startdatalogsession(VehicleCommType *vehiclecommtype,
                                      VehicleCommLevel *vehiclecommlevel);
u8 obd2datalog_ford_stopdatalogsession(VehicleCommType *vehiclecommtype,
                                     VehicleCommLevel *vehiclecommlevel);
float obd2datalog_ford_calculate_bp(DatalogSignal *datalogsignal);
u8 obd2datalog_ford_unset_all_osc(VehicleCommType *vehiclecommtype,
                                  VehicleCommLevel *vehiclecommlevel);
        
#endif    //__OBD2DATALOG_FORD_H
