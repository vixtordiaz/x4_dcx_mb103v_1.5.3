/**
  **************** (C) COPYRIGHT 2011 SCT Performance, LLC *********************
  * File Name          : obd2_ford_keygen.h
  * Device / System    : SCT products using the "CommonCode" platform
  * Author             : Quyen Leba
  *
  * Version            : 1 
  * Date               : 05/02/2011
  * Description        : 
  *                    : 
  *
  *
  * History            : 05/02/2011 M. Davis
  *                    :   Add SCT Copyright header,
  *
  ******************************************************************************
  */

#ifndef __OBD2_FORD_KEYGEN_H
#define __OBD2_FORD_KEYGEN_H

#include <arch/gentype.h>

u8 obd2_ford_keygen(u32 _seed, u32 unlockid, u32 *key);

#endif    //__OBD2_FORD_KEYGEN_H
