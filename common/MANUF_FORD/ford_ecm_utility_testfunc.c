/**
  **************** (C) COPYRIGHT 2011 SCT Performance, LLC *********************
  * File Name          : ford_ecm_utility_testfunc.c
  * Device / System    : SCT products using the "CommonCode" platform
  * Author             : Quyen Leba
  *
  * Version            : 1 
  * Date               : 05/02/2011
  * Description        : 
  *                    : 
  *
  *
  * History            : 05/02/2011 M. Davis
  *                    :   Add SCT Copyright header,
  *
  ******************************************************************************
  */

#include <common/statuscode.h>
#include "ford_ecm_utility_testfunc.h"
#include "obd2can_ford_67L.h"

//------------------------------------------------------------------------------
// This is example function to test if its utility file is applicable
// Input:   u16 ecm_type (defined in ecm_defs)
// Return:  u8  status (S_SUCCESS if applicable)
// Engineer: Quyen Leba
//------------------------------------------------------------------------------
u8 ford_ecm_utility_testfunc_emptytest(u16 ecm_type)
{
    return S_SUCCESS;
}


//------------------------------------------------------------------------------
// Test for 6.7L to see if modified utility file is applicable
// Input:   u16 ecm_type (defined in ecm_defs)
// Return:  u8  status (S_SUCCESS if applicable)
// Engineer: Patrick Downs
//------------------------------------------------------------------------------
u8 ford_ecm_utility_testfunc_67L_PCM(u16 ecm_type)
{
    if(obd2can_ford_67L_IsTunefileModified() == S_SUCCESS)
        return S_SUCCESS;
    else
        return S_FAIL;
}

