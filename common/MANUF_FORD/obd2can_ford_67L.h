/**
  **************** (C) COPYRIGHT 2011 SCT Performance, LLC *********************
  * File Name          : obd2can_ford_60L.h
  * Device / System    : SCT products using the "CommonCode" platform
  * Author             : Quyen Leba
  *
  * Version            : 1 
  * Date               : 05/02/2011
  * Description        : Specific for 6.0L
  *                    : 
  *
  *
  * History            : 05/02/2011 M. Davis
  *                    :   Add SCT Copyright header,
  *
  ******************************************************************************
  */

#ifndef __OBD2CAN_FORD_67L_H
#define __OBD2CAN_FORD_67L_H

#include <common/obd2can.h>

u8 obd2can_ford_67L_IsTunefileModified(void);

u8 obd2can_ford_m1_get_axle_ratio(u32 *axle_ratio);
u8 obd2can_ford_m1_get_tire_size(u32 *tire_size);
u8 obd2can_ford_m1_set_axle_ratio(u32 axle_ratio);
u8 obd2can_ford_m1_set_tire_size(u32 tire_size);
u8 obd2can_ford_m1_get_tpms_front(u8 *front_tire_pressure);
u8 obd2can_ford_m1_get_tpms_rear(u8 *rear_tire_pressure);
u8 obd2can_ford_m1_set_tpms_front(u8 front_tire_pressure);
u8 obd2can_ford_m1_set_tpms_rear(u8 rear_tire_pressure);

u8 obd2can_ford_67L_get_axle_ratio_tire_size(u32 *axle_ratio, u32 *tire_size);
u8 obd2can_ford_67L_set_axle_ratio_tire_size(u32 axle_ratio, u32 tire_size);
u8 obd2can_ford_67L_get_tpms_info(u8 *front_tire_pressure, u8 *rear_tire_pressure);
u8 obd2can_ford_67L_set_tpms_info(u8 front_tire_pressure, u8 rear_tire_pressure);
u8 obd2can_ford_67L_get_vehicle_info(u8 *info_text);

#endif    //__OBD2CAN_FORD_67L_H
