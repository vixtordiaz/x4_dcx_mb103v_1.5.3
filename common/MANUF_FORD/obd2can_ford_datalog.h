/**
  **************** (C) COPYRIGHT 2011 SCT Performance, LLC *********************
  * File Name          : obd2can_ford_datalog.h
  * Device / System    : SCT products using the "CommonCode" platform
  * Author             : Quyen Leba
  *
  * Version            : 1 
  * Date               : 05/02/2011
  * Description        : 
  *                    : 
  *
  *
  * History            : 05/02/2011 M. Davis
  *                    :   Add SCT Copyright header,
  *
  ******************************************************************************
  */

#ifndef __OBD2CAN_FORD_DATALOG_H
#define __OBD2CAN_FORD_DATALOG_H

#include <arch/gentype.h>
#include <common/obd2def.h>

void obd2can_ford_datalog_sendtesterpresent();
u8 obd2can_ford_datalog_evaluatesignalsetup(VehicleCommLevel *vehiclecommlevel);
u8 obd2can_ford_datalog_change_diagnostic_mode(VehicleCommLevel *vehiclecommlevel);
u8 obd2can_ford_datalog_cleanup_packets(VehicleCommLevel *vehiclecommlevel);
u8 obd2can_ford_datalog_initiatepackets(VehicleCommLevel *vehiclecommlevel);
u8 obd2can_ford_datalog_getdata(VehicleCommLevel *vehiclecommlevel);

#endif    //__OBD2CAN_FORD_DATALOG_H
