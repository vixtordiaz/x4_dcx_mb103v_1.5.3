/**
  **************** (C) COPYRIGHT 2011 SCT Performance, LLC *********************
  * File Name          : obd2can_ford_60L.c
  * Device / System    : SCT products using the "CommonCode" platform
  * Author             : Quyen Leba
  *
  * Version            : 1 
  * Date               : 05/02/2011
  * Description        : Specific for 6.0L
  *                    : 
  *
  *
  * History            : 05/02/2011 M. Davis
  *                    :   Add SCT Copyright header,
  *
  ******************************************************************************
  */

#include <common/statuscode.h>
#include <common/itoa.h>
#include "obd2can_ford.h"
#include "obd2can_ford_60L.h"

#define TIRE_SIZE_60L_LOW               600
#define TIRE_SIZE_60L_HIGH              720

//------------------------------------------------------------------------------
// Get front tire size of a 6.0L
// Output:  u32 *rev_per_mile
// Return:  u8  status
// Engineer: Patrick Downs, Quyen Leba
// Note: require ABS module; i.e. won't be S_SUCCESS on a bench
//------------------------------------------------------------------------------
u8 obd2can_ford_60L_get_tire_rev_per_mile_front(u32 *rev_per_mile)
{
    u8  data[64];
    u32 datalength;
    u8  status;

    *rev_per_mile = 0;

    //0x760: ABS module
    obd2can_init(Obd2canInitCustom,0x760);
    status = obd2can_ford_read_data_by_local_id(0x760,0x00,data,&datalength);
    if (status == S_SUCCESS)
    {
        *rev_per_mile = ((u16)data[2] << 8) + (u16)data[3];
    }
    obd2can_init(Obd2canInitProgram,0);
    return status;
}

//------------------------------------------------------------------------------
// Set front tire size of a 6.0L
// Input:   u32 rev_per_mile
// Return:  u8  status
// Engineer: Patrick Downs, Quyen Leba
// Note: require ABS module; i.e. won't be S_SUCCESS on a bench
//------------------------------------------------------------------------------
u8 obd2can_ford_60L_set_tire_rev_per_mile_front(u32 rev_per_mile)
{
    u8  data[5] = {0x3C, 0x00, 0x00, 0x00, 0x00};
    u8  status;

    if (rev_per_mile < TIRE_SIZE_60L_LOW)
    {
        rev_per_mile = TIRE_SIZE_60L_LOW;
    }
    else if (rev_per_mile > TIRE_SIZE_60L_HIGH)
    {
        rev_per_mile = TIRE_SIZE_60L_HIGH;
    }
    data[1] = (u8)(rev_per_mile >> 8);
    data[2] = (u8)(rev_per_mile & 0xFF);

    //0x760: ABS module
    obd2can_init(Obd2canInitCustom,0x760);
    status = obd2can_ford_write_data_by_local_id(0x760,0x00,data,sizeof(data));
    obd2can_init(Obd2canInitProgram,0);
    return status;
}

//------------------------------------------------------------------------------
// Get rear tire size of a 6.0L !!! This actually affects speedometer
// Output:  u32 *rev_per_mile
// Return:  u8  status
// Engineer: Patrick Downs, Quyen Leba
// Note: require ABS module; i.e. won't be S_SUCCESS on a bench
//------------------------------------------------------------------------------
u8 obd2can_ford_60L_get_tire_rev_per_mile_rear(u32 *rev_per_mile)
{
    u8  data[64];
    u32 datalength;
    u8  status;

    *rev_per_mile = 0;

    //0x760: ABS module
    obd2can_init(Obd2canInitCustom,0x760);
    status = obd2can_ford_read_data_by_local_id(0x760,0x01,data,&datalength);
    if (status == S_SUCCESS)
    {
        *rev_per_mile = ((u16)data[2] << 8) + (u16)data[3];
    }
    obd2can_init(Obd2canInitProgram,0);
    return status;
}

//------------------------------------------------------------------------------
// Set rear tire size of a 6.0L !!! This actually affects speedometer
// Input:   u32 rev_per_mile
// Return:  u8  status
// Engineer: Patrick Downs, Quyen Leba
// Note: require ABS module; i.e. won't be S_SUCCESS on a bench
//------------------------------------------------------------------------------
u8 obd2can_ford_60L_set_tire_rev_per_mile_rear(u32 rev_per_mile)
{
    u8  data[5] = {0x78, 0x00, 0x00, 0x00, 0x00};
    u8  status;

    if (rev_per_mile < TIRE_SIZE_60L_LOW)
    {
        rev_per_mile = TIRE_SIZE_60L_LOW;
    }
    else if (rev_per_mile > TIRE_SIZE_60L_HIGH)
    {
        rev_per_mile = TIRE_SIZE_60L_HIGH;
    }
    data[1] = (u8)(rev_per_mile >> 8);
    data[2] = (u8)(rev_per_mile & 0xFF);

    //0x760: ABS module
    obd2can_init(Obd2canInitCustom,0x760);
    status = obd2can_ford_write_data_by_local_id(0x760,0x01,data,5);
    obd2can_init(Obd2canInitProgram,0);
    return status;
}

//------------------------------------------------------------------------------
// Get tire size of a 6.0L
// Outputs: u32 *front_rev_per_mile
//          u32 *rear_rev_per_mile
// Return:  u8  status
// Engineer: Patrick Downs, Quyen Leba
// Note: require ABS module; i.e. won't be S_SUCCESS on a bench
//------------------------------------------------------------------------------
u8 obd2can_ford_60L_get_tire_size(u32 *front_rev_per_mile, u32 *rear_rev_per_mile)
{
    u32 rev_per_mile[2];
    u8  status;

    *front_rev_per_mile = 0;
    *rear_rev_per_mile = 0;

    status = obd2can_ford_60L_get_tire_rev_per_mile_front(&rev_per_mile[0]);
    if (status != S_SUCCESS)
    {
        return status;
    }

    status = obd2can_ford_60L_get_tire_rev_per_mile_rear(&rev_per_mile[1]);
    if (status != S_SUCCESS)
    {
        return status;
    }

    *front_rev_per_mile = rev_per_mile[0];
    *rear_rev_per_mile = rev_per_mile[1];
    return S_SUCCESS;
}

//------------------------------------------------------------------------------
// Set tire size of a 6.0L
// Inputs:  u32 front_rev_per_mile
//          u32 rear_rev_per_mile
// Return:  u8  status
// Engineer: Patrick Downs, Quyen Leba
// Note: require ABS module; i.e. won't be S_SUCCESS on a bench
//------------------------------------------------------------------------------
u8 obd2can_ford_60L_set_tire_size(u32 front_rev_per_mile, u32 rear_rev_per_mile)
{
    u32 rev_per_mile;
    u8  status;

    status = obd2can_ford_60L_get_tire_rev_per_mile_front(&rev_per_mile);
    if (status != S_SUCCESS)
    {
        goto obd2can_ford_60L_set_tire_size_done;
    }
    if (front_rev_per_mile != rev_per_mile)
    {
        status = obd2can_ford_60L_set_tire_rev_per_mile_front(front_rev_per_mile);
        if (status != S_SUCCESS)
        {
            goto obd2can_ford_60L_set_tire_size_done;
        }
    }

    status = obd2can_ford_60L_get_tire_rev_per_mile_rear(&rev_per_mile);
    if (status != S_SUCCESS)
    {
        goto obd2can_ford_60L_set_tire_size_done;
    }
    if (rear_rev_per_mile != rev_per_mile)
    {
        status = obd2can_ford_60L_set_tire_rev_per_mile_rear(rear_rev_per_mile);
        if (status != S_SUCCESS)
        {
            goto obd2can_ford_60L_set_tire_size_done;
        }
        status = obd2can_ford_60L_get_tire_rev_per_mile_rear(&rev_per_mile);
        if (status != S_SUCCESS)
        {
            goto obd2can_ford_60L_set_tire_size_done;
        }
        else if (rev_per_mile != rear_rev_per_mile)
        {
            status = S_ERROR;
            goto obd2can_ford_60L_set_tire_size_done;
        }
    }

obd2can_ford_60L_set_tire_size_done:
    return status;
}

//------------------------------------------------------------------------------
// Get vehicle info specific to 6.0L (tire size)
// Output:  u8  *info_text
// Return:  u8  status
// Engineer: Quyen Leba
// Note: require ABS module; i.e. won't be S_SUCCESS on a bench
//------------------------------------------------------------------------------
u8 obd2can_ford_60L_get_vehicle_info(u8 *info_text)
{
    u8  buffer[32];
    u32 front_rev_per_mile;
    u32 rear_rev_per_mile;
    u8  status;

    status = obd2can_ford_60L_get_tire_size(&front_rev_per_mile,&rear_rev_per_mile);
    if (status == S_SUCCESS)
    {
        //only show rear tire size as it affects speedo
        strcat((char*)info_text,"<HEADER>Tire Size Settings:</HEADER>");
        itoa(rear_rev_per_mile,buffer,10);
        strcat((char*)info_text,(char*)buffer);
    }

    return S_SUCCESS;
}
