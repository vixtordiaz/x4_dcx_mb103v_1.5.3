/**
  **************** (C) COPYRIGHT 2011 SCT Performance, LLC *********************
  * File Name          : obd2tune_ford_checksum_boschtricore.c
  * Device / System    : SCT products using the "CommonCode" platform
  * Author             : Patrick Downs, Marty Pierson
  *
  * Version            : 1 
  * Date               : 08/15/2012
  * Description        : Checksum functions for Bosch PCMs using Infineon Tricore 
  *                      processors.
  *
  *
  *
  ******************************************************************************
  */

//------------------------------------------------------------------------------
// TODO:
// Log error point:
//------------------------------------------------------------------------------

#include <string.h>
#include <common/genmanuf_overload.h>
#include <common/statuscode.h>
#include <common/filestock.h>
#include <common/crc32.h>
#include "obd2tune_ford_checksum_boschtricore.h"

//------------------------------------------------------------------------------------------------------------------------------------------------
// Module variables
//------------------------------------------------------------------------------------------------------------------------------------------------
typedef struct
{
    u32 startaddress;
    u32 endaddress;
    u32 seed;
}BOSCH_CRC32_INFO;

#define MAX_BOSCH_ADDITIVES 16
typedef struct  {
    unsigned int startAddress[MAX_BOSCH_ADDITIVES];
    unsigned int endAddress[MAX_BOSCH_ADDITIVES];
    unsigned int checkSumAddress[MAX_BOSCH_ADDITIVES];
    unsigned int sum[MAX_BOSCH_ADDITIVES];
    unsigned char amount;
}_additives;

typedef union  {
    unsigned int CRCvalue;
    unsigned char buf[4];
}_word;

//------------------------------------------------------------------------------------------------------------------------------------------------
// Private Prototypes
//------------------------------------------------------------------------------------------------------------------------------------------------

u32 BoschAdditives(u32, u32, u32, unsigned char, u8);
u32 writeBlockCrc32(u32 offset, u32 length, u32 seed, u32 XorVal);
u32 getBlockCrc32Info(u8 dnld_def, BOSCH_CRC32_INFO *crc32Info, u32 *crccount);
unsigned char locateAdditives(unsigned int *, u32, u32, unsigned char, _additives *, u8);
u32 calcAdditives(unsigned int*, u32, u32, _additives *);
u32 writeAdditives(unsigned int*, u32, _additives*);
u32 Ford_67L_Insert_Segment_CRC(unsigned int Start, unsigned int Length);
u32 obd2tune_ford_calc_file_segment_crc(u32 seed, u32 address, u32 length, u32 *CRCvalue);
u32 Ford_67L_Additive_Checksum(u32 start, u32 length, u32 cs);
u8 validateAdditives(u32 segOffset, u32 segStartAddress, _additives* additives);

// NOTE: These definitions need to be validated for new Bosch Tricore PCMs
#define XORVALUE    0xD73C6E5B
u32 Bosch_Tricore_Checksum(u8 ecm_type, u32 ecm_offset)
{
    int status;
    u32 crccount;
    u32 offset;
    u32 length;
    BOSCH_CRC32_INFO crc32Info[MAX_ECM_MEMORY_BLOCKS];
    
    // TODO: Find a better way to identify which ECM Type has what and
    //       somewhere else to store these Additive addresses
    switch(ecm_type)
    {
    case 41: 
        status = BoschAdditives(0x120000,0x80140000,0xC0000,MAX_BOSCH_ADDITIVES, ecm_type);
        break;
    case 43:
        status = BoschAdditives(0,0x80020000,0x60000,MAX_BOSCH_ADDITIVES, ecm_type);
        break;
    default:
        return S_FAIL;
    }    
    if(status != S_SUCCESS)
        return status;
 
    memset((u8*)crc32Info, 0, sizeof(crc32Info));
    status = getBlockCrc32Info(ecm_type, crc32Info, &crccount);
    offset = 0;
    for(int i=0;i<crccount;i++)
    {
        offset += crc32Info[i].startaddress-ECM_GetOSBlockAddr(ecm_type,i);    // Get file offset relative to the current block
        length = (crc32Info[i].endaddress-crc32Info[i].startaddress)-3;                     // Calculate length
        status = writeBlockCrc32(offset,length,crc32Info[i].seed,XORVALUE);
        if(status != S_SUCCESS)
            return S_FAIL;
        offset += ECM_GetOSBlockSize(ecm_type,i);   // Add the current block length to file offset for the next block.
    }
    
    return S_SUCCESS;
}

/////////////////////////////////////////////////////////////////
//
// Bosch Tricore Additive Checksums applied to flash.bin
// 2011-2012 F-150 Eco Boost/Focus
//
// Entry:  bibary offset to segment (VBF segment start)
//         segment StartAddress (See VBF file)
//         segment Length (See VBF file)
//         quantity of additive blocks (maximum 15; see structure additives)
//
// Exit:  (1) = all checksums in segment updated
//        (2) = malloc error
//        (3) = file open error
//        (4) = additive quantity not found
//
/////////////////////////////////////////////////////////////////      
u32 BoschAdditives(u32 segOffset, u32 segStartAddress, u32 segLen, 
                                unsigned char quantity, u8 ecmtype)
{
    unsigned int  *databuf;
    _additives    additives;           // declare additives structure
    u32 status;
    
    databuf = __malloc(2048);
    if (databuf)
    {
        status = locateAdditives(databuf, segOffset, segLen, 
                                 quantity, &additives, ecmtype);
        if (status == S_SUCCESS)
        {
            status = validateAdditives(segOffset, segStartAddress, &additives);
            if (status == S_SUCCESS)
            {
                status = calcAdditives(databuf, segOffset, segStartAddress, &additives);
                if(status == S_SUCCESS)
                {
                    status = writeAdditives(databuf, segOffset, &additives);
                }
            }
        }

        __free(databuf);
    }
    else
    {
        status = S_FAIL;
    }
    
    return(status);
}

/////////////////////////////////////////////////////////////////
//   
// 2011-2012 Eco Boost
//      
// Purpose:   locate the additives by finding the additives and their 
//            complements in segment in flash.bin
//
// Entry:     malloced data area to read segment data
//            offset to the segment containing the additives
//            length of the segment containing the additives
//            quantity of additives to find
//            additive structure address
//
// Exit:      (1) on success
//            (4) requested quantity not found
//
/////////////////////////////////////////////////////////////////      
unsigned char locateAdditives(unsigned int* databuf, 
                              u32 segOffset, u32 segLen, 
                              unsigned char quantity, _additives* additives, u8 ecmtype)
{
    unsigned int cnt1, i;
    unsigned int first, second, temp;
    unsigned int startAddr, endAddr, csAddr;
    unsigned int ecmStartAddr, ecmEndAddr;
    unsigned int blockindex;
    unsigned long cnt, length;
    u32 bytes_read;
    
    additives->amount=0;
    length = segLen - 2048 - 16;
    if(filestock_set_position(segOffset) != S_SUCCESS)
    {
        return S_SEEKFILE;
    }
    
    for (cnt = 0; cnt < length;)         
    {
        if(filestock_read((u8*)databuf, 2048, (u32*)&bytes_read) != S_SUCCESS)
        {
            return S_READFILE;
        }
        
        // process 16 at a time
        // 4 4-byte words; end,start,complement,additive 
        i = 0;
        for (cnt1 = 0; cnt1 < 128; cnt1++)  
        {
            first = databuf[2 + i];     // complement of additive
            second = databuf[3 + i];    // additive
            temp = ~ second;            // complement additive
            if ( (first == temp) && (first != 0) && (second != 0) )
            {
                startAddr = databuf[0 + i];
                endAddr = databuf[1 + i];
                csAddr = cnt + (cnt1 * 16);
                
                // Verify the address is valid
                for(blockindex = 0; blockindex < 3; blockindex++)
                {
                    ecmStartAddr = ECM_GetOSReadBlockAddr(ecmtype,blockindex);
                    if(ecmStartAddr == 0)
                    {
                        break;
                    }
                    
                    ecmEndAddr = ecmStartAddr + ECM_GetOSReadBlockSize(ecmtype,0);
                    
                    if((startAddr >= ecmStartAddr) && endAddr <= ecmEndAddr) 
                    {
                        additives->startAddress[additives->amount] = startAddr;
                        additives->endAddress[additives->amount] = endAddr;
                        additives->checkSumAddress[additives->amount] = csAddr;
                        additives->amount++;
                        if (additives->amount >= quantity)
                        {                           
                            return S_FAIL;
                        }
                        break;
                    }
                }
            }
            i += 4;
        }
        cnt += 2048;
    }
    
    return S_SUCCESS; 
}

/////////////////////////////////////////////////////////////////
//   
// 2011-2012 Eco Boost
//   
// Purpose:   calculate the additives in flash.bin
//
// Entry:     malloced data area to read segment data
//            offset to the segment containing the additives
//            starting address of the segment containing the additives 
//                  (see VBF file)
//            additive structure address
//
// Exit:      none
//
/////////////////////////////////////////////////////////////////      
u32 calcAdditives(unsigned int* databuf, 
                   u32 segOffset, u32 segStartAddress, _additives* additives)
{
    unsigned char cnt;    
    unsigned long additiveStart, additiveLength, checksum = 0;
    unsigned int cnt1, byteCnt, i, temp, temp2;
    u32 status;
    
    for (cnt = 0; cnt < additives->amount; cnt++)
    {
        additiveStart = segOffset + additives->startAddress[cnt] - segStartAddress;
        additiveLength = additives->endAddress[cnt] - additives->startAddress[cnt];
        status = filestock_set_position(additiveStart);
        if(status != S_SUCCESS) return status;
        
        cnt1 = checksum = 0;
        while (cnt1 < additiveLength)
        {
            status = filestock_read((u8*)databuf, 2048, (u32*)&byteCnt);
            if(status != S_SUCCESS) return status;
                
            byteCnt/=4;                    // each read is 4 bytes
            for(i=0; i<byteCnt; i++)
            {
                temp = databuf[i]; 
                temp2=temp>>16;
                temp&=0xFFFF;
                checksum += temp;
                checksum += temp2;
                cnt1+=4;
                if (cnt1 >= additiveLength)
                {
                    break;
                }
            }
        }
        additives->sum[cnt] = checksum;
    }
    
    return S_SUCCESS;
}

/////////////////////////////////////////////////////////////////
//   
// 2011-2012 Eco Boost
//   
// Purpose:   write out the additives in flash.bin
//
// Entry:     fileptr to opened file cotaining segment
//            malloced data area to read segment data
//            offset to the segment containing the additives
//            additive structure address
//
// Exit:      none
//            
/////////////////////////////////////////////////////////////////      
u32 writeAdditives(unsigned int* databuf,
                    u32 segOffset, _additives* additives)
{
    unsigned char cnt;
    unsigned long checkAddress;
    unsigned long writeData;
    u32           status;
    
    for (cnt = 0; cnt < additives->amount; cnt++)
    {
        checkAddress = segOffset + additives->checkSumAddress[cnt] + 8;
        writeData = additives->sum[cnt];
        status = filestock_updateflashfile(checkAddress,(u8*)&writeData, 4);
        if(status != S_SUCCESS) return status;
        writeData = ~writeData;
        status = filestock_updateflashfile(checkAddress+4,(u8*)&writeData, 4);
        if(status != S_SUCCESS) return status;
    }
    return S_SUCCESS;
}

/////////////////////////////////////////////////////////////////
//
// 2011-2012 Eco Boost
//   
// Purpose:   Find CRC32 seed and address range in flash.bin
//
// Entry:     filename cotaining segment
//            offset to desired segment data
//            length of the segment-8 (last 8 bytes 4 CRC32; 4 ID)
//            seed
//            xor value
//
// Exit:      none
//            
/////////////////////////////////////////////////////////////////           
#define FILE_OFFSET         0x20000
#define STARTADDR_OFFSET    0x38
#define ENDADDR_OFFSET      0x3C
#define SEED_OFFSET         0x40
u32 getBlockCrc32Info(u8 ecm_type, BOSCH_CRC32_INFO *crc32Info, u32 *crccount)
{
    u32 bytecount;
    u32 offset;
    int i;
    u32 status;
        
    offset = 0;
    crccount[0] = 0;
    for(i=0;i<MAX_ECM_MEMORY_BLOCKS;i++)
    {
        if(ECM_GetOSBlockAddr(ecm_type,i) == 0)
            break;
        
        status = filestock_read_at_position(offset+STARTADDR_OFFSET, (u8*)crc32Info, sizeof(BOSCH_CRC32_INFO), (u32*)&bytecount);
        if(status != S_SUCCESS) return status;        
        
        if(bytecount != sizeof(BOSCH_CRC32_INFO))
            break;
        
        crc32Info++;
        offset += ECM_GetOSBlockSize(ecm_type,i);
        crccount[0]++;
    }
    
    return S_SUCCESS;
}

/////////////////////////////////////////////////////////////////
//   
// 2011-2012 Eco Boost
//   
// Purpose:   write out the block CRC      
//
// Entry:     filename cotaining segment
//            offset to desired segment data
//            length of the segment-8 (last 8 bytes 4 CRC32; 4 ID)
//            seed
//            xor value
//
// Exit:      none
//            
/////////////////////////////////////////////////////////////////  
u32 writeBlockCrc32(u32 offset, u32 length, u32 seed, u32 XorVal)
{
    _word word;
    u32 status;
    
    status = obd2tune_ford_calc_file_segment_crc(seed, offset, length, (u32*)&word.CRCvalue);
    if(status != S_SUCCESS) goto errorExit;
    
    word.CRCvalue ^= XorVal;
    
    // Write the CRC out to the end of the specified block.
    status = filestock_updateflashfile((offset + length),(u8*)&word.buf[0], 4);
        
errorExit:
    return status;      // return code
}

//------------------------------------------------------------------------------
// Calculate the CRC32 for a file segment
// Inputs:  F_FILE *file - File pointer to file with segment to be calulated
//          unsigned int seed - Starting CRC32 seed
//          unsigned int address - Starting address of segment
//          unsigned int length - Length of the segment
// Outputs: unsigned int CRCvalue - Return address of CRC value
// Return:  int  status
// Engineer: Patrick Downs, Quyen Leba
// Note: require flash file already opened; use filestock_openflashfile(...)
//------------------------------------------------------------------------------
#define CRC_BUFFER_SIZE 2048
u32 obd2tune_ford_calc_file_segment_crc(u32 seed, u32 address, u32 length, u32 *CRCvalue)
{
    unsigned int currentaddress = address;
    const unsigned int endaddress = address + length;
    char* buffer;
    u32 bytecount;
    u32 status;
    
    buffer = __malloc(CRC_BUFFER_SIZE);
    if(!buffer) return S_MALLOC;
    
    // Seek to the first spot in the file to be used in the CRC.
    status = filestock_set_position(address);
    if(status != S_SUCCESS)
    {
        __free(buffer);
        return status;
    }
    
    // Continue processing until we reach the end of the block.
    while (currentaddress != endaddress)
    {
        // Calculate the total amount of data left to be processed.
        length = endaddress - currentaddress;
        
        // If the amount left is greater than what can fit in our buffer,
        // cap it to the buffer size.
        if (length > CRC_BUFFER_SIZE)
            length = CRC_BUFFER_SIZE;
        
        // Read out the next block of data to be processed.
        status = filestock_read((u8*)buffer, length, (u32*)&bytecount);
        if(bytecount != length)
        {
            __free(buffer);
            return S_READFILE;
        }
                
        // Send it into the CRC function and capture the 
        seed = crc32_sw_calculateblock(seed, buffer, length);
        
        // Update the index into the file.
        currentaddress += length;
    }
    
    __free(buffer);
    
    // Update the CRC of this block.
    *CRCvalue = seed;
    
    return S_SUCCESS;
}

u32 Ford_67L_ModifyForSBLAndChecksum(void)
{
    int status = S_SUCCESS;
    u32 bytecount;
    
    // First check to see whether the file is modified or stock.
    if (obd2can_ford_67L_IsTunefileModified() == S_SUCCESS)
    {
        char buffer[768];
        
        status = filestock_openflashfile("r+");
        if(status != S_SUCCESS) return status;
        
        // Proper CRC calculation requires zero bytes be injected at this point
        // in the file.
        memset(buffer, 0, 768);

        status = filestock_updateflashfile(0x1EFC00,(u8*)buffer, 768);
        if(status != S_SUCCESS)
        {
            status = S_WRITEFILE;
            goto FORD_67L_CHECKSUM_END;
        }
        
        status = Ford_Calculate67L_Checksums(ChecksumId_67L);
        if(status != S_SUCCESS)
        {
            goto FORD_67L_CHECKSUM_END;
        }
        
        // For the modified bootloader we use to work, we need to pull 256 bytes 
        // from 0x10000, 0x30000 and 0xF0000 and append them, respectively, before
        // reinserting them at 0x1EFC00.

        status = filestock_read_at_position(0x10000, (u8*)buffer, 256, (u32*)&bytecount);
        if(status != S_SUCCESS)
        {
            status = S_READFILE;
            goto FORD_67L_CHECKSUM_END;
        }
        status = filestock_read_at_position(0x30000, (u8*)buffer+256, 256, (u32*)&bytecount);
        if(status != S_SUCCESS)
        {
            status = S_READFILE;
            goto FORD_67L_CHECKSUM_END;
        }
        status = filestock_read_at_position(0xF0000, (u8*)buffer+512, 256, (u32*)&bytecount);
        if(status != S_SUCCESS)
        {
            status = S_READFILE;
            goto FORD_67L_CHECKSUM_END;
        }

        status = filestock_updateflashfile(0x1EFC00,(u8*)buffer, 768);
        if(status != S_SUCCESS)
        {
            status = S_WRITEFILE;
            goto FORD_67L_CHECKSUM_END;
        }
        
    FORD_67L_CHECKSUM_END:
        filestock_closeflashfile();
    }
    
    return status;
}

// 2013+ Additive Checksum
#define ADDITIVE32_START             0x2E41B8
#define ADDITIVE32_LEN               0x1E90
#define ADDITIVE32_ADDR              0x2E6048
u8 Ford_Calculate67L_Checksums(u16 checksumid)
{
    u8 status;
    
    // Calculate CRC from StartAddress to BlockLength-4 block, CRC at (StartAddress+BlockLength)
    if(checksumid == ChecksumId_67L) // 2011-2012
    {
        status = Ford_67L_Insert_Segment_CRC(0x00000000, 0x0000FFF8); 
        if(status == S_SUCCESS)
        {
            status = Ford_67L_Insert_Segment_CRC(0x00010000, 0x0001FFF8); 
            if(status == S_SUCCESS)
            {
                status = Ford_67L_Insert_Segment_CRC(0x00030000, 0x000BFFF8);
                if(status == S_SUCCESS)
                {
                    status = Ford_67L_Insert_Segment_CRC(0x000F0000, 0x000FFFF8); 
                    if(status == S_SUCCESS)
                    {
                        status = Ford_67L_Insert_Segment_CRC(0x001F0000, 0x000DFFF8); 
                    }
                }
            }
        }
    }
    else if(checksumid == ChecksumId_67L_2013UP) // 2013+
    {
        // 2013+ has an Additive Checksum
        status = Ford_67L_Additive_Checksum(ADDITIVE32_START, ADDITIVE32_LEN, 
                                       ADDITIVE32_ADDR);
        
        status = Ford_67L_Insert_Segment_CRC(0x00000000, 0x00003FF8); 
        if(status == S_SUCCESS)
        {
            status = Ford_67L_Insert_Segment_CRC(0x00004000, 0x001DFFF8); 
            if(status == S_SUCCESS)
            {
                status = Ford_67L_Insert_Segment_CRC(0x001E8000, 0x0007BFF8);
                if(status == S_SUCCESS)
                {
                    status = Ford_67L_Insert_Segment_CRC(0x00264000, 0x0007FFF8); 
                    if(status == S_SUCCESS)
                    {
                        status = Ford_67L_Insert_Segment_CRC(0x002E4000, 0x000FFFF8); 
                    }
                }
            }
        }
    }
    else
    {
        status = S_INPUT;
    }
    
    return status;
}

u32 Ford_67L_Insert_Segment_CRC(unsigned int Start, unsigned int Length)
{
    // Initial segment CRC.
    const unsigned int seed = 0xFADECAFE;
    // Value with which to XOR the calculated CRC.
    const unsigned int xor = 0xD73C6E5B;
    unsigned int CRCvalue;
    char buffer[4];
    int status;   
    
    // Calculate the specified block's CRC32.
    status = obd2tune_ford_calc_file_segment_crc(seed, Start, Length, (u32*)&CRCvalue);
    if(status != S_SUCCESS)
        return status;

    // Perform the post CRC XOR on the bytes to get the final CRC put into the file.
    CRCvalue ^= xor;
    
    // Bytes are read big endian in the processor, reverse them before putting inserting into file.
    buffer[3] = CRCvalue >> 24;
    buffer[2] = (CRCvalue >> 16) & 0xFF;
    buffer[1] = (CRCvalue >> 8) & 0xFF;
    buffer[0] = CRCvalue & 0xFF;
    
    // Write the CRC out to the end of the specified block.
//    fseek(file, Start + Length, SEEK_SET);
//    fwrite(buffer, 1, 4, file);   
    status = filestock_updateflashfile((Start + Length),(u8*)buffer, 4);
        
    return S_SUCCESS;
}

/**
 *  Ford_67L_Additive_Checksum
 *  
 *  @brief Calculate additive checksum in 2013+ 67L
 *  
 *  @param [in] start   Start address of section to be checksumed in flash file
 *  @param [in] length  Length of section to be checksumed in flash file
 *  @param [in] cs      Address in file where to write the new checksum
 * 
 *  @retval             status
 *  
 *  @details Calculates additive checksum in 2013+ 67L PCMs. Add up all the words
 *           in the section, invert the result, and add the constant.
 *  
 *  @authors Patrick Downs
 *  
 */
u32 Ford_67L_Additive_Checksum(u32 start, u32 length, u32 cs)
{
    u8 status;
    u8 buffer[2048];
    u32 *ptr;
    u32 current;
    u32 end;
    u32 sum;
    u32 readlength;
    u32 bytecount;
        
    current = start;
    end = start + length;
    
    // Seek to the checksum segment starting address in the file
    status = filestock_set_position(start);
    if(status == S_SUCCESS)
    {
        sum = 0; // start value zero
        
        // Continue processing until we reach the end of the block.
        while (current != end)
        {
            // Calculate the total amount of data left to be processed.
            readlength = end - current;
            
            // If the amount left is greater than what can fit in our buffer,
            // cap it to the buffer size.
            if (readlength > sizeof(buffer))
                readlength = sizeof(buffer);
            
            // Read out the next block of data to be processed.
            status = filestock_read((u8*)buffer, readlength, (u32*)&bytecount);
            if(bytecount == readlength)
            {
                ptr = (u32*)buffer;
                for(int i = 0; i < (readlength/4); i++)
                {
                    sum += *(ptr+i);
                }
                
                // Update the index into the file.
                current += readlength;
            }
            else
            {
                status = S_READFILE;
                break;
            }
        }
        
        sum = ~sum;         // Invert the results
        sum += 0xD01FE501;  // Add the mystery constant. Got this from 2nd hand source code from a dealer
        
        // Write the checksum to the file.
        status = filestock_updateflashfile(cs,(u8*)&sum, 4);
    }
    else
    {
        status = S_READFILE;
    }
        
    return status;
}

/////////////////////////////////////////////////////////////////
//   
// 2011-2012 Eco Boost and 2012 Focus
//   
// Purpose:   calculate the additives in stock.bin
//
// Entry:     malloced data area to read segment data
//            offset to the segment containing the additives
//            starting address of the segment containing the additives 
//                  (see VBF file)
//            additive structure address
//
// Exit:      none
//
/////////////////////////////////////////////////////////////////       
u8 validateAdditives(u32 segOffset, u32 segStartAddress, _additives* additives)
{
    unsigned char cnt;    
    unsigned long additiveStart, additiveLength, additiveCompliment, checksum = 0;
    unsigned int cnt1, byteCnt, i, j, temp, temp2, falseadditives;
    u32 *databuffer; 
    u32 length = 2048; 
    u32 status;
    u32 status2; 
       
    status = S_SUCCESS; 
    databuffer = NULL; 
    
    //close flash.bin file 
    filestock_closeflashfile();
    
    //open stock.bin file
    status = filestock_openstockfile("r",FALSE);
    if (status != S_SUCCESS)
    {
        return S_OPENFILE; 
    }
    
    databuffer = __malloc(length);
    if (databuffer == NULL)
    {
        return S_MALLOC; 
    }
    
    //calculate all found additives. 
    for (cnt = 0; cnt < additives->amount; cnt++)
    {
        additiveStart = segOffset + additives->startAddress[cnt] - segStartAddress;
        additiveLength = additives->endAddress[cnt] - additives->startAddress[cnt];
        status = filestock_set_position(additiveStart);
        if(status != S_SUCCESS) 
        {
            status = S_FAIL;
            break;
        }
        
        cnt1 = checksum = 0;
        while (cnt1 < additiveLength)
        {
            status = filestock_read((u8*)databuffer, length, (u32*)&byteCnt);
            if(status != S_SUCCESS) 
            {
                status = S_FAIL;
                break;
            }
                
            byteCnt/=4;                    // each read is 4 bytes
            for(i=0; i<byteCnt; i++)
            {
                temp = databuffer[i]; 
                temp2=temp>>16;
                temp&=0xFFFF;
                checksum += temp;
                checksum += temp2;
                cnt1+=4;
                if (cnt1 >= additiveLength)
                {
                    break;
                }
            }
        }
        additives->sum[cnt] = checksum;
    }
    
    if (status == S_SUCCESS)
    {
        //validate the calculated additives against the value in the stock.bin file 
        //in order to eliminate any additive that was detected incorrectly.
        for (cnt = 0; cnt < additives->amount; cnt++)
        {
            temp = 0; 
            temp2 = 0; 
            falseadditives = 0; 
            status = filestock_set_position(segOffset + additives->checkSumAddress[cnt] + 8);
            if (status == S_SUCCESS)
            {
                status = filestock_read((u8*)databuffer, 8, (u32*)&byteCnt);  
                if (status != S_SUCCESS)
                {
                    status = S_FAIL;
                    break; 
                }
                else
                {
                    temp = databuffer[0];
                    temp2 = databuffer[1]; 
                    additiveCompliment = ~additives->sum[cnt];
                    if (temp != additives->sum[cnt] && temp2 != additiveCompliment)
                    {
                        //clear false additives 
                        additives->checkSumAddress[cnt] = 0; 
                        additives->endAddress[cnt] = 0 ;
                        additives->startAddress[cnt] = 0; 
                        additives->sum[cnt] = 0; 
                    }
                }
            }
        }
    }
    
    if (status == S_SUCCESS)
    {
        //Remove false addtives and rebuild additive structure. 
        for (cnt = 0; cnt < additives->amount; cnt++)
        {
            if(additives->checkSumAddress[cnt] == 0) 
            {
                //additives->amount--;
                falseadditives++; 
                for (j=0; j < (additives->amount - cnt); j++)
                {
                    additives->checkSumAddress[cnt + j] = additives->checkSumAddress[cnt + j + 1];          
                    additives->endAddress[cnt + j] = additives->endAddress[cnt + j + 1];
                    additives->startAddress[cnt + j] = additives->startAddress[cnt + j + 1]; 
                }
               
                if (cnt >= additives->amount)
                {
                    break;
                }
                else
                {
                    cnt--; 
                }
            }
        }
    }

    if (status == S_SUCCESS)
    {
        //clean up additive strucuture
        additives->amount = additives->amount - falseadditives; 
        for (cnt = 0; cnt < falseadditives; cnt++)
        {
            additives->checkSumAddress[additives->amount + cnt] = 0; 
        }
    }
     
    //close stock.bin file and reopen flash.bin file before exiting function. 
    filestock_closestockfile();
    
    //free memory
    if (databuffer)
    {
        __free(databuffer);
    }
    
    status2 = filestock_openflashfile("r+");
    if (status2 != S_SUCCESS)
    {
        status = status2; 
    }
        
    return status; 
}