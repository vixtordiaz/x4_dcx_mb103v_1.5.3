/**
  **************** (C) COPYRIGHT 2011 SCT Performance, LLC *********************
  * File Name          : ford_ecm_utility_defs.c
  * Device / System    : SCT products using the "CommonCode" platform
  * Author             : Quyen Leba
  *
  * Version            : 1 
  * Date               : 05/02/2011
  * Description        : 
  *                    : 
  *
  *
  * History            : 05/02/2011 M. Davis
  *                    :   Add SCT Copyright header,
  *
  ******************************************************************************
  */

#include "ford_ecm_utility_defs.h"
#include "ford_ecm_utility_testfunc.h"

//IMPORTANT: defined functions for utilityfunctest are in ford_ecm_utility_testfunc.c
//set utilityfunctest to ignore test and assume utility is applicable

//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
const utility_info utilityinfolist[] =
{
    [0] =
    {
        //Seimens 2048K Green Oak (6.4L Engine) (CAN)               //0x16
        // "2ndboot.bin"
        "UBF0.ubf",UTILITY_FLAGS_USE_ENCRYPTION,0x0,
        .utilityfunctest = NULL,
        .utility_block =
        {
            {0x00000000, 0x000002FC, ORDER_0 | MEMBLOCK_FLAGS_NONE, 0},
            {0, 0, ORDER_1 | MEMBLOCK_FLAGS_NONE, 0},
            {0, 0, ORDER_2 | MEMBLOCK_FLAGS_NONE, 0},
            {0, 0, ORDER_3 | MEMBLOCK_FLAGS_NONE, 0},
            {0, 0, ORDER_4 | MEMBLOCK_FLAGS_NONE, 0},
        },
    },
    [1] =
    {
        //PRISM: eSys CBP-C2 ISO14229 (CAN)                         //0x22
        // Related Vehicles: 5.0L Mustang, 6.2L Gas Trucks
        // BC3A-14C273-BC.bin - Old Version, Replaced
        // BC3A-14C273-BD.bin 
        "UBF1.ubf",UTILITY_FLAGS_USE_ENCRYPTION,0x40009500,
        .utilityfunctest = NULL,
        .utility_block =
        {
            {0x40009500, 0x00011B00, ORDER_0 | MEMBLOCK_FLAGS_NONE, 0},
            {0, 0, ORDER_1 | MEMBLOCK_FLAGS_NONE, 0},
            {0, 0, ORDER_2 | MEMBLOCK_FLAGS_NONE, 0},
            {0, 0, ORDER_3 | MEMBLOCK_FLAGS_NONE, 0},
            {0, 0, ORDER_4 | MEMBLOCK_FLAGS_NONE, 0},
        },
    },
    [2] = 
    {
        // Bosch - Diesel (A) EDC17 CP05 2870k                        //0x23
        // Related Vehicles: 6.7L Superduty Trucks
        // BC3A-14C273-AA.bin
        "UBF2.ubf",UTILITY_FLAGS_USE_ENCRYPTION,0xD4000000,
        .utilityfunctest = NULL,
        .utility_block =
        {
            {0xD4000000, 0x00000600, ORDER_0 | MEMBLOCK_FLAGS_NONE, 0},
            {0, 0, ORDER_1 | MEMBLOCK_FLAGS_NONE, 0},
            {0, 0, ORDER_2 | MEMBLOCK_FLAGS_NONE, 0},
            {0, 0, ORDER_3 | MEMBLOCK_FLAGS_NONE, 0},
            {0, 0, ORDER_4 | MEMBLOCK_FLAGS_NONE, 0},
        },
    },
    [3] = 
    {
        // PRISM: eSys CBP-A2 ISO14229 (CAN)                          //0x24
        // Related Vehicles: 3.7L Mustang
        // BR3A-14C273-AA.bin
        "UBF3.ubf",UTILITY_FLAGS_USE_ENCRYPTION,0x40009000,
        .utilityfunctest = NULL,
        .utility_block =
        {
            {0x40009000, 0x00000190, ORDER_0 | MEMBLOCK_FLAGS_NONE, 0},
            {0x400091A0, 0x000010B0, ORDER_1 | MEMBLOCK_FLAGS_NONE, 0},
            {0x4000A260, 0x00000420, ORDER_2 | MEMBLOCK_FLAGS_NONE, 0},
            {0x4000BD20, 0x000099C0, ORDER_3 | MEMBLOCK_FLAGS_NONE, 0},
            {0, 0, ORDER_4 | MEMBLOCK_FLAGS_NONE, 0},
        },
    },
    [4] =
    {
        // PRISM: Tricore UTCU2 ISO VBF 2048K 384K CAL               //0x25
        // Related Vehicles: 6.7L Superduty Trucks (TCM)
        // TODO: This bootloader is not an even multiple of 8, so we skip encryption for now
        // BC3A-14C339-AA.bin
        "UBF4.ubf",UTILITY_FLAGS_USE_ENCRYPTION,0xD0004000,
        .utilityfunctest = NULL,
        .utility_block =
        {
            {0xD0004000, 0x00008FEC, ORDER_0 | MEMBLOCK_FLAGS_NONE, 0},
            {0, 0, ORDER_1 | MEMBLOCK_FLAGS_NONE, 0},
            {0, 0, ORDER_2 | MEMBLOCK_FLAGS_NONE, 0},
            {0, 0, ORDER_3 | MEMBLOCK_FLAGS_NONE, 0},
            {0, 0, ORDER_4 | MEMBLOCK_FLAGS_NONE, 0},
        },
    },
    [5] =
    {
        // Contiental Tricore PCM (Need offical ford name)           //0x26
        // Related Vehicles: 2011 Fiesta (PCM)
        // AE81-14C273-AD.bin
        "UBF5.ubf",UTILITY_FLAGS_USE_ENCRYPTION,0xD0005E00,
        .utilityfunctest = NULL,
        .utility_block =
        {
            {0xD0005E00, 0x000073E8, ORDER_0 | MEMBLOCK_FLAGS_NONE, 0},
            {0, 0, ORDER_1 | MEMBLOCK_FLAGS_NONE, 0},
            {0, 0, ORDER_2 | MEMBLOCK_FLAGS_NONE, 0},
            {0, 0, ORDER_3 | MEMBLOCK_FLAGS_NONE, 0},
            {0, 0, ORDER_4 | MEMBLOCK_FLAGS_NONE, 0},
        },
    },
    [6] = 
    {
        // PRISM: Tricore DPS6 ISO VBF 2048K                        //0x27
        // Related Vehicles: 2011 Fiesta, 2012 Focus (TCM)
        // AE8P-7J244-AA.bin
        "UBF6.ubf",UTILITY_FLAGS_USE_ENCRYPTION,0xD0004EE8,
        .utilityfunctest = NULL,
        .utility_block =
        {
            {0xD0004E00, 0x00006EEC, ORDER_0 | MEMBLOCK_FLAGS_NONE, 0},
            {0, 0, ORDER_1 | MEMBLOCK_FLAGS_NONE, 0},
            {0, 0, ORDER_2 | MEMBLOCK_FLAGS_NONE, 0},
            {0, 0, ORDER_3 | MEMBLOCK_FLAGS_NONE, 0},
            {0, 0, ORDER_4 | MEMBLOCK_FLAGS_NONE, 0},
        },
    },
    [7] = 
    {
        // Bosch - Diesel (A) EDC17 CP05 2870k                        //0x23
        // MODIFIED SBL FOR TUNE FILES
        // Related Vehicles: 6.7L Superduty Trucks - 
        // BC3A-14C273-AA-SCT.bin
        "UBF7.ubf",UTILITY_FLAGS_USE_ENCRYPTION | UTILITY_FLAGS_LAST_36_FAIL_OK,0xD4000000,
        .utilityfunctest = (funcptr_utilfunctest*)&ford_ecm_utility_testfunc_67L_PCM,
        .utility_block =
        {
            {0xD4000000, 0x00000800, ORDER_0 | MEMBLOCK_FLAGS_NONE, 0},
            {0, 0, ORDER_1 | MEMBLOCK_FLAGS_NONE, 0},
            {0, 0, ORDER_2 | MEMBLOCK_FLAGS_NONE, 0},
            {0, 0, ORDER_3 | MEMBLOCK_FLAGS_NONE, 0},
            {0, 0, ORDER_4 | MEMBLOCK_FLAGS_NONE, 0},
        },
    },
    [8] = 
    {
        // PRISM: Tricore 1797 MEDG17.0 Bosch Gas GTDI ISO VBF      //0x29
        // Related Vehicles: 2012 F150 Ecoboost
        // BL3A-14C273-AB.bin
        "UBF8.ubf",UTILITY_FLAGS_USE_ENCRYPTION,0xC0002128,
        .utilityfunctest = NULL,
        .utility_block =
        {
            {0xC0002000, 0x00002000, ORDER_0 | MEMBLOCK_FLAGS_NONE, 0},
            {0xD0006000, 0x00008700, ORDER_1 | MEMBLOCK_FLAGS_NONE, 0},
            {0, 0, ORDER_2 | MEMBLOCK_FLAGS_NONE, 0},
            {0, 0, ORDER_3 | MEMBLOCK_FLAGS_NONE, 0},
            {0, 0, ORDER_4 | MEMBLOCK_FLAGS_NONE, 0},
        },
    },
    [9] =
    {
        // PRISM: Tricore 1767 MED17.0 Bosch Gas DI ISO VBF          //0x2B
        // Related Vehicles: 2012 2.0L Focus (PCM)
        // BB5A-14C273-AA.bin
        "UBF9.ubf",UTILITY_FLAGS_USE_ENCRYPTION,0xC0002128,
        .utilityfunctest = NULL,
        .utility_block =
        {
            {0xC0002000, 0x00002000, ORDER_0 | MEMBLOCK_FLAGS_NONE, 0},
            {0xD0006000, 0x00008700, ORDER_1 | MEMBLOCK_FLAGS_NONE, 0},
            {0, 0, ORDER_2 | MEMBLOCK_FLAGS_NONE, 0},
            {0, 0, ORDER_3 | MEMBLOCK_FLAGS_NONE, 0},
            {0, 0, ORDER_4 | MEMBLOCK_FLAGS_NONE, 0},
        },
    },
    [10] =
    {
        // Contiental Tricore PCM (Need offical ford name)           //0x2D
        // Related Vehicles: 2011 Fiesta (PCM) MODIFIED SBL FOR CUSTOM TUNE
        // AE81-14C273-AD-SCT.bin
        "UBF10.ubf",UTILITY_FLAGS_USE_ENCRYPTION,0xD0005E00,
        .utilityfunctest = NULL,
        .utility_block =
        {
            {0xD0005E00, 0x000073E8, ORDER_0 | MEMBLOCK_FLAGS_NONE, 0},
            {0, 0, ORDER_1 | MEMBLOCK_FLAGS_NONE, 0},
            {0, 0, ORDER_2 | MEMBLOCK_FLAGS_NONE, 0},
            {0, 0, ORDER_3 | MEMBLOCK_FLAGS_NONE, 0},
            {0, 0, ORDER_4 | MEMBLOCK_FLAGS_NONE, 0},
        },
    },
    [11] = 
    {
        // PRISM: DPS6 ISO VBF 2048K EXTENDED 128K                     //0x2C
        // Related Vehicles: 2012 Focus (TCM)
        // AE8P-7J244-AB.bin
        "UBF11.ubf",UTILITY_FLAGS_USE_ENCRYPTION,0xD0004EE8,
        .utilityfunctest = NULL,
        .utility_block =
        {
            {0xD0004E00, 0x00006EEC, ORDER_0 | MEMBLOCK_FLAGS_NONE, 0},
            {0, 0, ORDER_1 | MEMBLOCK_FLAGS_NONE, 0},
            {0, 0, ORDER_2 | MEMBLOCK_FLAGS_NONE, 0},
            {0, 0, ORDER_3 | MEMBLOCK_FLAGS_NONE, 0},
            {0, 0, ORDER_4 | MEMBLOCK_FLAGS_NONE, 0},
        },
    },
    [12] = 
    {
        // Bosch - Diesel (A) EDC17 CP05 2870k                        //0x2F
        // Related Vehicles: 2013-14 6.7L Superduty Trucks
        // DC3A-14C273-AA.bin
        "UBF12.ubf",UTILITY_FLAGS_USE_ENCRYPTION,0xC0002000,
        .utilityfunctest = NULL,
        .utility_block =
        {
            {0xC0002000, 0x00000800, ORDER_0 | MEMBLOCK_FLAGS_NONE, 0},
            {0, 0, ORDER_1 | MEMBLOCK_FLAGS_NONE, 0},
            {0, 0, ORDER_2 | MEMBLOCK_FLAGS_NONE, 0},
            {0, 0, ORDER_3 | MEMBLOCK_FLAGS_NONE, 0},
            {0, 0, ORDER_4 | MEMBLOCK_FLAGS_NONE, 0},
        },
    },
    [13] = 
    {
        // PRISM: CBP-A2.2 Viper 3072K                              //0x30
        // Related Vehicles: 2013+ 3.7L MKZ
        // DP5A-14C273-AA.bin
        "UBF13.ubf",UTILITY_FLAGS_USE_ENCRYPTION,0x40009000,
        .utilityfunctest = NULL,
        .utility_block =
        {
            {0x40009000, 0x00000190, ORDER_0 | MEMBLOCK_FLAGS_NONE, 0},
            {0x400091A0, 0x000010F0, ORDER_1 | MEMBLOCK_FLAGS_NONE, 0},
            {0x4000A2A0, 0x00000420, ORDER_2 | MEMBLOCK_FLAGS_NONE, 0},
            {0x4000BD60, 0x000099C0, ORDER_3 | MEMBLOCK_FLAGS_NONE, 0},
            {0, 0, ORDER_4 | MEMBLOCK_FLAGS_NONE, 0},
        },
    },
    [14] = 
    {
        // Bosch - Diesel (A) EDC17 CP05 2870k                        //0x2F
        // Related Vehicles: 2015 6.7L Superduty Trucks
        // Tested and found to work on 2013 & 2014 as well
        // FC3A-14C273-AA.bin
        "UBF14.ubf",UTILITY_FLAGS_USE_ENCRYPTION,0xC0002000,
        .utilityfunctest = NULL,
        .utility_block =
        {
            {0xC0002000, 0x00000800, ORDER_0 | MEMBLOCK_FLAGS_NONE, 0},
            {0, 0, ORDER_1 | MEMBLOCK_FLAGS_NONE, 0},
            {0, 0, ORDER_2 | MEMBLOCK_FLAGS_NONE, 0},
            {0, 0, ORDER_3 | MEMBLOCK_FLAGS_NONE, 0},
            {0, 0, ORDER_4 | MEMBLOCK_FLAGS_NONE, 0},
        },
    },
    [15] =
    {
        // PRISM: EMS24XX Tricore TC-1791 4M                        //0x32
        // Related Vehicles: 2015 5.0L Mustang (PCM)
        // FR3A-14C273-BA.bin
        "UBF15.ubf",UTILITY_FLAGS_USE_ENCRYPTION,0xD00132C0,
        .utilityfunctest = NULL,
        .utility_block =
        {
            {0xD00132C0, 0x000087F8, ORDER_0 | MEMBLOCK_FLAGS_NONE, 0},
            {0, 0, ORDER_1 | MEMBLOCK_FLAGS_NONE, 0},
            {0, 0, ORDER_2 | MEMBLOCK_FLAGS_NONE, 0},
            {0, 0, ORDER_3 | MEMBLOCK_FLAGS_NONE, 0},
            {0, 0, ORDER_4 | MEMBLOCK_FLAGS_NONE, 0},
        },
    },
};

