/**
  **************** (C) COPYRIGHT 2011 SCT Performance, LLC *********************
  * File Name          : obd2_ford.h
  * Device / System    : SCT products using the "CommonCode" platform
  * Author             : Quyen Leba
  *
  * Version            : 1 
  * Date               : 05/02/2011
  * Description        : 
  *                    : 
  *
  *
  * History            : 05/02/2011 M. Davis
  *                    :   Add SCT Copyright header,
  *
  ******************************************************************************
  */

#ifndef __OBD2_FORD_H
#define __OBD2_FORD_H

#include <arch/gentype.h>
#include <common/obd2def.h>
#include <common/obd2tune.h>

u8 obd2_ford_generatekey(u8 *seed, u32 algoindex, VehicleCommType commtype,
                         u8 *key);
u8 obd2_ford_getoverlayposition_instockfile(u16 veh_type,
                                            u32 *vidposition, u32 *patsposition,
                                            u32 *securitybyteposition);
u8 obd2tune_ford_handle_overlaydata(u16 veh_type, u8 *vin, bool isdoapply);
u8 obd2_ford_setupvpp();
u8 obd2_ford_wakeup(u16 veh_type, u16 ecm_index);
u8 obd2_ford_vehicle_specific_task_before_program(flasher_info *f_info);
u8 obd2_ford_vehicle_specific_task_after_program(flasher_info *f_info);

u8 obd2_ford_vehicleinfototext(vehicle_info *vehicleinfo, u8 *vehicleinfo_text);

void obd2_ford_vehiclefunction(VehicleCommType *commtype, 
                               VehicleCommLevel *commlevel);
void obd2_ford_clear_kam(VehicleCommType commtype, VehicleCommLevel commlevel);
void obd2_ford_crank_relearn(VehicleCommType commtype,
                             VehicleCommLevel commlevel);

#endif    //__OBD2_FORD_H
