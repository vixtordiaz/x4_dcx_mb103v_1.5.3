/**
  **************** (C) COPYRIGHT 2011 SCT Performance, LLC *********************
  * File Name          : obd2datalog_ford.c
  * Device / System    : SCT products using the "CommonCode" platform
  * Author             : Quyen Leba
  *
  * Version            : 1 
  * Date               : 05/02/2011
  * Description        : 
  *                    : 
  *
  *
  * History            : 05/02/2011 M. Davis
  *                    :   Add SCT Copyright header,
  *
  ******************************************************************************
  */

//------------------------------------------------------------------------------
// Log error point: 0x4900 -> 0x4AFF (shared with obd2datalog.c)
// Use 0x4980 -> 0x499F
//------------------------------------------------------------------------------

#include <board/delays.h>
#include <common/statuscode.h>
#include <common/obd2.h>
#include <common/obd2scp.h>
#include <common/genmanuf_overload.h>
#include <common/obd2datalog.h>
#include "obd2datalog_ford.h"

extern Datalog_Mode gDatalogmode;       //from obd2datalog.c
extern Datalog_Info *dataloginfo;       //from obd2datalog.c

//------------------------------------------------------------------------------
// Initialize datalog, determine commtype and commlevel
// Outputs: VehicleCommType commtype[ECM_MAX_COUNT]
//          VehicleCommLevel commlevel[ECM_MAX_COUNT]
// Return:  u8  status
// Engineer: Quyen Leba
//------------------------------------------------------------------------------
u8 obd2datalog_ford_init(VehicleCommType *commtype, VehicleCommLevel *commlevel)
{
    u8  status;
    
    status = obd2_findvehiclecommtypecommlevel(commtype, commlevel);
    if (status != S_SUCCESS)
    {
        log_push_error_point(0x4987);
    }
    if (status == S_SUCCESS && commtype[0] == CommType_CAN)
    {
        status = obd2can_ford_datalog_change_diagnostic_mode(commlevel);
    }
    return status;
}

/**
 *  @brief Ford Pre-PID validation task
 *
 *  @param [in]     ecm_index  ECM Index
 *  @param [in]     commtype   Vehicle comm type
 *  @param [in]     commlevel  Vehicle comm level
 *
 *  @return Status
 */
u8 obd2datalog_ford_prevalidate_task(u16 ecm_index, VehicleCommType commtype, 
                                     VehicleCommLevel commlevel)
{
    u32 ecm_id;
    u8 data[4];
    u8 status = S_SUCCESS;
    
    /* Only handle CAN here; other comm types handled in obd2datalog.c */
    if (commtype == CommType_CAN)
    {
        switch (ecm_index)
        {
        case 0:
            ecm_id = Obd2CanEcuId_7E0;
            break;
        case 1:
            ecm_id = Obd2CanEcuId_7E1;
            break;
        default:
            status = S_FAIL;
            break;
        }
        if (status == S_SUCCESS)
        {
            status = obd2_readpid_mode1(FALSE, ecm_id, 0x00, 4, data, commtype);
            if (status != S_SUCCESS)
            {
                gDatalogmode.ecmBroadcast = TRUE;
                status = S_SUCCESS;
            }
            obd2can_ford_datalog_change_diagnostic_mode(&commlevel);
            gDatalogmode.unlockRequired = FALSE;
        }
    }
    else
    {
        status = S_NOTSUPPORT;
    }
    return status;
}

//------------------------------------------------------------------------------
// Validate a dmr/pid with Rapid Packet Setup Mode 0x2C
// Inputs:  u32 ecm_id
//          u16 pid
//          VehicleCommType commtype,
//          VehicleCommLevel commlevel
// Return:  u8  status
// Engineer: Quyen Leba
//------------------------------------------------------------------------------
u8 obd2datalog_ford_validateByRapidPacketSetup(u32 ecm_id, DlxBlock *dlxblock,
                                VehicleCommType commtype,
                                VehicleCommLevel commlevel)
{
    u8 status;

    status = S_SUCCESS;
    if (commtype == CommType_CAN)
    {
        if (!SETTINGS_IsDemoMode())
        {
            status = obd2can_validateByRapidPacketSetup(ecm_id,dlxblock,commlevel);
        }
    }
    else if (commtype == CommType_SCP || commtype == CommType_SCP32)
    {
        if (!SETTINGS_IsDemoMode())
        {
            status = obd2scp_validateByRapidPacketSetup(dlxblock,commtype,commlevel);
        }
    }
    else
    {
        log_push_error_point(0x498B);
        status = S_COMMTYPE;
    }
    return status;
}

//------------------------------------------------------------------------------
// Evaluate all items for what can datalog in one session
// Inputs:  VehicleCommType vehiclecommtype[ECM_MAX_COUNT]
//          VehicleCommLevel vehiclecommlevel[ECM_MAX_COUNT]
// Outputs: u16 *valid_index_list
//          u16 *valid_index_count
// Return:  u8  status
// Engineer: Quyen Leba
//------------------------------------------------------------------------------
u8 obd2datalog_ford_evaluate_fit(VehicleCommType *vehiclecommtype,
                                 VehicleCommLevel *vehiclecommlevel,
                                 u16 *valid_index_list,
                                 u16 *valid_index_count)
{
    u16 i;
    u16 count;
    u8  status;

    if (dataloginfo == NULL)
    {
        log_push_error_point(0x498C);
        return S_BADCONTENT;
    }
    else if (valid_index_count == NULL || valid_index_list == NULL)
    {
        log_push_error_point(0x498D);
        return S_BADCONTENT;
    }

    *valid_index_count = 0;
    
    switch (vehiclecommtype[0])
    {
    case CommType_CAN:
        status = obd2can_ford_datalog_evaluatesignalsetup(vehiclecommlevel);
        break;
    case CommType_SCP:
    case CommType_SCP32:
        status = obd2scp_datalog_evaluatesignalsetup(vehiclecommlevel[0]);
        if (status == S_NOTFIT)
        {
            //allow to continue but use non rapid packet mode
            gDatalogmode.packetmode = SingleRate;
            status = obd2scp_datalog_evaluatesignalsetup(vehiclecommlevel[0]);
        }
        break;
    default:
        log_push_error_point(0x498E);
        return S_COMMTYPE;
    }
    
    if (status == S_SUCCESS || status == S_NOTFIT)
    {
        count = 0;
        for(i=0;i<dataloginfo->datalogsignalcount;i++)
        {
            switch (dataloginfo->datalogsignals[i].PidType)
            {
                case PidTypeRegular:
                case PidTypeDMR: 
                    if (vehiclecommtype[0] == CommType_SCP &&
                        gDatalogmode.packetmode == RapidRate)
                    {
                        // Only count valid packets
                        if (dataloginfo->datalogsignals[i].SignalType.Generic.PacketNumber == DATALOG_INVALID_PACKETID)
                        {
                            break;
                        }
                    }
                case PidTypeMode1:    
                case PidTypeAnalog:
                case PidTypeOSC:
                    valid_index_list[count++] = i;
                    break;
                default:
                    break;
            }     
            if (count >= DATALOG_MAX_VALID_PER_SESSION)
            {
                break;
            }
        }
        *valid_index_count = count;
        return S_SUCCESS;
    }
    log_push_error_point(0x498F);
    return S_FAIL;
}

//------------------------------------------------------------------------------
// Prepare for datalog, setup packets (if fit), setup datalog function calls
// Inputs:  VehicleCommType vehiclecommtype[ECM_MAX_COUNT]
//          VehicleCommLevel vehiclecommlevel[ECM_MAX_COUNT]
// Return:  u8  status
// Engineer: Quyen Leba
//------------------------------------------------------------------------------
u8 obd2datalog_ford_setupdatalogsession(VehicleCommType *vehiclecommtype,
                                        VehicleCommLevel *vehiclecommlevel)
{
    u8  status = S_SUCCESS;

    if (dataloginfo == NULL)
    {
        log_push_error_point(0x4990);
        return S_BADCONTENT;
    }
    
    // Initialize Tasks
    dataloginfo->scheduled_tasks = DATALOG_TASK_NONE;
    
    if (vehiclecommtype[0] == CommType_CAN)
    {
        if ((vehiclecommlevel[0] == CommLevel_KWP2000 || 
             vehiclecommlevel[0] == CommLevel_ISO14229) &&
             gDatalogmode.packetmode == RapidRate)
        {
            status = obd2can_ford_datalog_evaluatesignalsetup(vehiclecommlevel);        
            if (status == S_SUCCESS)
            {
                obd2can_ford_datalog_change_diagnostic_mode(vehiclecommlevel);
                obd2can_ford_datalog_cleanup_packets(vehiclecommlevel);
                status = obd2can_ford_datalog_initiatepackets(vehiclecommlevel);
                if (status != S_SUCCESS)
                {
                    // No packets being used or packets are unsupported
                    gDatalogmode.packetmode = SingleRate;
                    status = obd2_datalog_evaluatesignalsetup(vehiclecommlevel); 
                }
                if (status != S_SUCCESS)
                {
                    log_push_error_point(0x4991);                    
                    return status;
                }
            }
        }
        else
        {
            // Allow single rate SAE PID datalogging on other OEM's
            gDatalogmode.packetmode = SingleRate;
            status = obd2_datalog_evaluatesignalsetup(vehiclecommlevel);
        }
        if (status != S_SUCCESS)
        {
            log_push_error_point(0x4992);
            return status;
        }

        if (SETTINGS_IsDemoMode())
        {
            dataloginfo->getdatafunc = obd2datalog_demo_getdata;
            dataloginfo->getanalogdatafunc = obd2datalog_getdata_analog;
            dataloginfo->sendtesterpresent = obd2datalog_demo_sendtesterpresent;
            dataloginfo->scheduled_tasks |= DATALOG_TASK_GET_DATA
                                         |  DATALOG_TASK_ANALOG;
            status = S_SUCCESS;
        }
        else
        {            
            switch(gDatalogmode.packetmode)
            {
                case RapidRate:
                    dataloginfo->getdatafunc = obd2can_ford_datalog_getdata;                    
                    dataloginfo->sendtesterpresent = obd2can_ford_datalog_sendtesterpresent;
                    dataloginfo->scheduled_tasks |= DATALOG_TASK_GET_DATA                                                 
                                                 |  DATALOG_TASK_ANALOG;
                    break;
                case SingleRate:
                    dataloginfo->getdatafunc = obd2can_ford_datalog_getdata;
                    dataloginfo->scheduled_tasks |= DATALOG_TASK_GET_DATA
                                                 |  DATALOG_TASK_ANALOG;
                    break;
                default:
                    status = S_FAIL;
                    break;
            }
            if (dataloginfo->oscsignalcount > 0 && status == S_SUCCESS)
            {
                dataloginfo->scheduled_tasks |= DATALOG_TASK_OSC;
                dataloginfo->sendtesterpresent = obd2can_ford_datalog_sendtesterpresent;
            }
            dataloginfo->getanalogdatafunc = obd2datalog_getdata_analog;            
            obd2can_ford_unlock_osc(Obd2CanEcuId_7E0,vehiclecommlevel[0]);
        }
    }
    else if (vehiclecommtype[0] == CommType_SCP || vehiclecommtype[0] == CommType_SCP32)
    {
        status = obd2scp_datalog_evaluatesignalsetup(vehiclecommlevel[0]);
        
        if (status == S_SUCCESS && gDatalogmode.packetmode == RapidRate)
        {
            obd2scp_datalog_cleanup_packets();
            status = obd2scp_datalog_initiatepackets(vehiclecommlevel);          
        }
        
        if (status != S_SUCCESS)
        {
            // Rapid packets failed to set up or no packets needed
            log_push_error_point(0x4993);            
            gDatalogmode.packetmode = SingleRate;
            status = obd2scp_datalog_evaluatesignalsetup(vehiclecommlevel[0]);
        }
        
        if (status != S_SUCCESS)
        {
            log_push_error_point(0x4994);
            return status;
        }
        if (SETTINGS_IsDemoMode())
        {
            dataloginfo->getdatafunc = obd2datalog_demo_getdata;
            dataloginfo->getanalogdatafunc = obd2datalog_getdata_analog;
            dataloginfo->sendtesterpresent = obd2datalog_demo_sendtesterpresent;
            dataloginfo->scheduled_tasks |= DATALOG_TASK_GET_DATA
                                         |  DATALOG_TASK_ANALOG;
            status = S_SUCCESS;
        }
        else
        {            
            switch(gDatalogmode.packetmode)
            {
                case RapidRate:                    
                    dataloginfo->getdatafunc = obd2scp_datalog_getdata;                    
                    dataloginfo->sendtesterpresent = obd2scp_datalog_sendtesterpresent;
                    // SCP does not allow mixed rapid/single rate datalogging
                    dataloginfo->scheduled_tasks |= DATALOG_TASK_GET_DATA
                                                 |  DATALOG_TASK_ANALOG;
                    break;
                case SingleRate: 
                    dataloginfo->getdatafunc = obd2datalog_getdata_singlerate;
                    dataloginfo->scheduled_tasks |= DATALOG_TASK_GET_DATA_SINGLE_RATE
                                                 |  DATALOG_TASK_ANALOG;
                    break;
                default:
                    status = S_FAIL;
                    break;
            }
            dataloginfo->getanalogdatafunc = obd2datalog_getdata_analog;
        }
    }
    else if (vehiclecommtype[1] == CommType_CAN ||
             vehiclecommtype[1] == CommType_KLINE ||
             vehiclecommtype[1] == CommType_KWP2000 ||
             vehiclecommtype[1] == CommType_VPW)
    {
        status = obd2_datalog_evaluatesignalsetup(vehiclecommlevel);
        if (status == S_SUCCESS || status == S_NOTFIT)
        {
            gDatalogmode.packetmode = SingleRate;
            dataloginfo->getanalogdatafunc = obd2datalog_getdata_analog;
            dataloginfo->getdatafunc = obd2datalog_getdata_singlerate;
            dataloginfo->scheduled_tasks |= DATALOG_TASK_GET_DATA_SINGLE_RATE
                                         |  DATALOG_TASK_ANALOG;
        }
    }
    else
    {
        log_push_error_point(0x4995);
        return S_COMMTYPE;
    }
    return status;
}

//------------------------------------------------------------------------------
// Start datalog session
// Inputs:  VehicleCommType vehiclecommtype[ECM_MAX_COUNT]
//          VehicleCommLevel vehiclecommlevel[ECM_MAX_COUNT]
// Return:  u8  status
// Engineer: Quyen Leba
//------------------------------------------------------------------------------
u8 obd2datalog_ford_startdatalogsession(VehicleCommType *vehiclecommtype,
                                        VehicleCommLevel *vehiclecommlevel)
{
    u8  i;
    u8  status;

    if (dataloginfo == NULL)
    {
        log_push_error_point(0x4996);
        return S_BADCONTENT;
    }

    status = S_FAIL;
    
    if (vehiclecommtype[0] == CommType_CAN)
    {        
        if (gDatalogmode.packetmode == RapidRate)
        {
            const u32 ecm_id[ECM_MAX_COUNT] = {Obd2CanEcuId_7E0,Obd2CanEcuId_7E1,0};

            for(i=0;i<ECM_MAX_COUNT;i++)
            {
                if(dataloginfo->packetidcount[i] == 0)
                    continue;

                if (vehiclecommlevel[i] == CommLevel_ISO14229)
                {
                    status = obd2can_ford_requestdiagnosticdatapacket_2A
                        (ecm_id[i], 0x03,
                         dataloginfo->packetidlist[i], dataloginfo->packetidcount[i],
                         CommLevel_ISO14229);
                    
                    // Unset all OSC items (if no OSC PIDs, this function does nothing)
                    obd2datalog_ford_unset_all_osc(vehiclecommtype,vehiclecommlevel);
                }
                else if (vehiclecommlevel[i] == CommLevel_KWP2000)
                {
                    status = obd2can_ford_requestdiagnosticdatapacket
                        (ecm_id[i],0x08,NULL,0,CommLevel_KWP2000);
                }
                else if (vehiclecommlevel[i] == CommLevel_Unknown)
                {
                    break;
                }
                else
                {
                    log_push_error_point(0x4997);
                    status = S_COMMLEVEL;
                    break;
                }
            }
        }
        else
        {
            status = S_SUCCESS;
        }
    }
    else if (vehiclecommtype[0] == CommType_SCP || vehiclecommtype[0] == CommType_SCP32)
    {
        if (gDatalogmode.packetmode == RapidRate)
        {
        
            obd2scp_datalog_sendtesterpresent();
            status = obd2scp_request_diagnostic_data_packet
                (0x04,dataloginfo->packetidlist[0],dataloginfo->packetidcount[0]);
            if (status == S_SUCCESS)
            {
                obd2scp_datalogmode();
            }
            else
            {
                //obd2scp_request_diagnostic_data_packet(stopSending_$2A,NULL,0);
            }
        }
        else
        {
            status = S_SUCCESS;
            obd2scp_datalogmode();
        }
    }
    else if (vehiclecommtype[1] == CommType_CAN ||
             vehiclecommtype[1] == CommType_KLINE ||
             vehiclecommtype[1] == CommType_KWP2000 ||
             vehiclecommtype[1] == CommType_VPW)
    {
        status = S_SUCCESS;
    }
    else
    {
        log_push_error_point(0x4998);
        return S_COMMTYPE;
    }
    
    return status;
}

//------------------------------------------------------------------------------
// Stop datalog session
// Inputs:  VehicleCommType vehiclecommtype[ECM_MAX_COUNT]
//          VehicleCommLevel vehiclecommlevel[ECM_MAX_COUNT]
// Return:  u8  status
// Engineer: Quyen Leba
//------------------------------------------------------------------------------
u8 obd2datalog_ford_stopdatalogsession(VehicleCommType *vehiclecommtype,
                                       VehicleCommLevel *vehiclecommlevel)
{
    const u32 ecm_id[ECM_MAX_COUNT] = {Obd2CanEcuId_7E0,Obd2CanEcuId_7E1,0};
    u8  i;

    if (vehiclecommtype[0] == CommType_CAN)
    {
        for(i=0;i<ECM_MAX_COUNT;i++)
        {
            if(vehiclecommtype[i] == CommType_CAN)
            {
                if (vehiclecommlevel[i] == CommLevel_ISO14229)
                {
                    obd2can_ford_requestdiagnosticdatapacket_2A(ecm_id[i],0x04,NULL,0,
                                                                CommLevel_ISO14229);
                    delays(100,'m');
                    // Unset all OSC items (if no OSC PIDs, this function does nothing)
                    obd2datalog_ford_unset_all_osc(vehiclecommtype,vehiclecommlevel);
                }
                else if (vehiclecommlevel[i] == CommLevel_KWP2000)
                {
                    //TODOQ: this
                    //old code use $A0 0A to stop, but not quite sure that's right
                    //because 0A is just data rate, and might not supported
                    obd2can_ford_requestdiagnosticdatapacket(ecm_id[i],0x0A,NULL,0,
                                                             CommLevel_KWP2000);
                    delays(100,'m');
                }
                else if (vehiclecommlevel[i] == CommLevel_Unknown)
                {
                    break;
                }
                else
                {
                    log_push_error_point(0x4999);
                    return S_COMMLEVEL;
                }
            }//if(vehiclecommtype[i] == CommType_CAN)...
        }//for(i=0;i<ECM_MAX_COUNT;i++)...
    }
    else if (vehiclecommtype[0] == CommType_SCP || vehiclecommtype[0] == CommType_SCP32)
    {
        obd2scp_stop_transmitting_requested_data();
        obd2scp_return_normal_mode();
        obd2scp_normalmode();
    }
    else if (vehiclecommtype[1] == CommType_CAN ||
             vehiclecommtype[1] == CommType_KLINE ||
             vehiclecommtype[1] == CommType_KWP2000 ||
             vehiclecommtype[1] == CommType_VPW)
    {
        return S_SUCCESS;
    }
    else
    {
        log_push_error_point(0x499A);
        return S_COMMTYPE;
    }
    
    return S_SUCCESS;
}

/**
 * @brief   Unset all Ford OSC Signals
 *
 * @param   [in] VehicleCommType
 * @param   [in] VehicleCommLevel
 *
 * @retval  u8 status
 *
 * @author  Tristen Pierson
 *
 * @details This function resets all Ford OSC items to their defaults and
 *          returns control to the ECU.
 */
u8 obd2datalog_ford_unset_all_osc(VehicleCommType *vehiclecommtype,
                                  VehicleCommLevel *vehiclecommlevel)
{
    u32 ecm_id[2] = {Obd2CanEcuId_7E0,Obd2CanEcuId_7E1};
    u32 ecm_index;
    u16 i,j,k;
    u8  data[8];
    u8  skip;
    u8  status = S_SUCCESS;
    u16 addressList[MAX_DATALOG_DLXBLOCK_COUNT];
    
    memset(addressList,0x00,sizeof(addressList));
    memset(data,0x00,sizeof(data));
    k = 0;
    
    if (vehiclecommtype[0] == CommType_CAN && 
        vehiclecommlevel[0] == CommLevel_ISO14229)
    {
        for (i = 0; i < dataloginfo->datalogsignalcount; i++)
        {
            if (dataloginfo->datalogsignals[i].StructType == StructTypeOSC)
            {
                ecm_index = dataloginfo->datalogsignals[i].EcmIndex;
                skip = 0;
                // Skip addresses already processed
                for (j = 0; j < dataloginfo->datalogsignalcount; j++)
                {
                    if (dataloginfo->datalogsignals[i].Address == addressList[j])
                    {
                        skip = 1;
                        break;
                    }
                }
                // Reset current CPID
                if (skip == 0)
                {
                    addressList[k++] = dataloginfo->datalogsignals[i].Address;
                    // Return Control to ECU
                    status = obd2can_ford_input_output_control(
                            ecm_id[ecm_index],
                            (u16)dataloginfo->datalogsignals[i].Address,0x00,
                            data,0,*vehiclecommlevel,TRUE);

                    // Update status for all items in current CPID address
                    for (j = 0; j < dataloginfo->datalogsignalcount; j++)
                    {
                        if (dataloginfo->datalogsignals[j].StructType == StructTypeOSC &&
                            dataloginfo->datalogsignals[j].Address == dataloginfo->datalogsignals[i].Address)
                        {
                            if (status == S_SUCCESS)
                            {
                                DatalogControl_SetOSC(dataloginfo->datalogsignals[j].Control,OSC_Success);
                                dataloginfo->datalogsignals[j].Value = OSC_Default;
                            }
                            else
                            {
                                DatalogControl_SetOSC(dataloginfo->datalogsignals[j].Control,OSC_Fail);
                                dataloginfo->datalogsignals[j].Value = OSC_Fail;
                            }
                            dataloginfo->datalogsignals[j].Control |= DATALOG_CONTROL_NEWDATA_SET;
                        }
                    }
                }
            }
        } //for (i=0...
    } // if(vehiclecommlevel[0]...
    else
    {
        log_push_error_point(0x49BD);
        return S_COMMTYPE;
    }
    return status;
}