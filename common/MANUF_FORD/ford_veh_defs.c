/**
  **************** (C) COPYRIGHT 2011 SCT Performance, LLC *********************
  * File Name          : ford_veh_defs.c
  * Device / System    : SCT products using the "CommonCode" platform
  * Author             : Quyen Leba
  *
  * Version            : 1 
  * Date               : 05/02/2011
  * Description        : 
  *                    : 
  *
  *
  * History            : 05/02/2011 M. Davis
  *                    :   Add SCT Copyright header,
  *
  ******************************************************************************
  */

#include <common/obd2.h>
#include <common/ecm_defs.h>
#include <common/statuscode.h>
#include "ford_veh_defs.h"

// ONLY USED FOR TESTING! When offical release, need this needs to be cleared
#if (APPLICATION_VERSION_BUILD == 0)
#define TESTING_ALLOW_BLANK_VIN         0
#else
#define TESTING_ALLOW_BLANK_VIN         1
#endif

//------------------------------------------------------------------------------
// Get # of ecms defined by vehicle type
// Input:   u16 veh_type (defined for veh_defs)
// Return:  u8  count
//------------------------------------------------------------------------------
u8 ford_veh_get_ecmcount(u16 veh_type)
{
    u8 i;
    u8 count;

    //TODOQ: check veh_type first
    
    for(i=0,count=0;i<ECM_MAX_COUNT;i++)
    {
        if (VEH_GetEcmType(veh_type,i) != INVALID_ECM_DEF)
        {
            count++;
        }
    }
    return count;
}

//------------------------------------------------------------------------------
// Check that the vehicle type exists
// Input:   u16 veh_type (defined for veh_defs)
// Return:  u8 status
//------------------------------------------------------------------------------
u8 ford_veh_validate_vehicletype(u16 veh_type)
{
    u8 i;
    u16 index;

    for(i=0;i<VEH_DEFS_COUNT;i++)
    {
        index = VEH_GetIndex(veh_type);
        if ((index+FORD_VEH_DEF_START) == veh_type)
        {
            return S_SUCCESS;
        }
    }
    return S_FAIL;
}

//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
const VEH_Def ford_veh_defs[VEH_DEFS_COUNT] =
{
    //UNUSED SLOTS: [0,1,18-26,33,37,46-49, 55, 62-63, 67
    //IMPORTANT: use those slots first before making new ones

    [2] =   //220K EECV (SCP)                                           (0x02)
    {
        .index = 2,
        .flags = REQUIRE_OVERLAY | REQUIRE_WAKEUP | WAKEUP_CHECK_ONCE |
            CUSTOM_TUNE_TUNEOS_FILE_NOT_REQUIRED,
        .mainecm_index = 0,
        .ecm_types = {1, INVALID_ECM_DEF, INVALID_ECM_DEF},
        .templOSfilename = NULL,
    },
    [3] =   //88K EECV (SCP)                                            (0x03)
    {
        .index = 3,
        .flags = REQUIRE_OVERLAY | REQUIRE_WAKEUP | WAKEUP_CHECK_ONCE |
            CUSTOM_TUNE_TUNEOS_FILE_NOT_REQUIRED,
        .mainecm_index = 0,
        .ecm_types = {3, INVALID_ECM_DEF, INVALID_ECM_DEF},
        .templOSfilename = NULL,
    },
    [4] =   //112K EECV (SCP)                                           (0x04)
    {
        .index = 4,
        .flags = REQUIRE_OVERLAY | REQUIRE_WAKEUP | WAKEUP_CHECK_ONCE |
            CUSTOM_TUNE_TUNEOS_FILE_NOT_REQUIRED,
        .mainecm_index = 0,
        .ecm_types = {2, INVALID_ECM_DEF, INVALID_ECM_DEF},
        .templOSfilename = NULL,
    },
    [5] =   //216K EECV (SCP)                                           (0x05)
    {
        .index = 5,
        .flags = REQUIRE_OVERLAY | REQUIRE_WAKEUP | WAKEUP_CHECK_ONCE |
            CUSTOM_TUNE_TUNEOS_FILE_NOT_REQUIRED,
        .mainecm_index = 0,
        .ecm_types = {0, INVALID_ECM_DEF, INVALID_ECM_DEF},
        .templOSfilename = NULL,
    },
    [6] =   //1472K Black Oak (CAN)                                     (0x06)
    {
        .index = 6,
        .flags = REQUIRE_OVERLAY | REQUIRE_WAKEUP | SIMPLE_SETMARRIED |
            CUSTOM_TUNE_TUNEOS_FILE_NOT_REQUIRED,
        .mainecm_index = 0,
        .ecm_types = {4, INVALID_ECM_DEF, INVALID_ECM_DEF},
        .templOSfilename = NULL,
    },
    [7] =   //1024K Spanish Oak (CAN)                                   (0x07)
    {
        .index = 7,
        .flags = REQUIRE_OVERLAY | REQUIRE_WAKEUP | SIMPLE_SETMARRIED |
            CUSTOM_TUNE_TUNEOS_FILE_NOT_REQUIRED,
        .mainecm_index = 0,
        .ecm_types = {7, INVALID_ECM_DEF, INVALID_ECM_DEF},
        .templOSfilename = NULL,
    },
    [8] =   //1024K PTEC (SCP32)                                        (0x08)
    {
        .index = 8,
        .flags = REQUIRE_OVERLAY | REQUIRE_WAKEUP | WAKEUP_CHECK_ONCE |
            SIMPLE_SETMARRIED | CUSTOM_TUNE_TUNEOS_FILE_NOT_REQUIRED,
        .mainecm_index = 0,
        .ecm_types = {14, INVALID_ECM_DEF, INVALID_ECM_DEF},
        .templOSfilename = NULL,
    },
    [9] =   //1472K Black Oak (SCP32)                                   (0x09)
    {
        .index = 9,
        .flags = REQUIRE_OVERLAY | REQUIRE_WAKEUP | WAKEUP_CHECK_ONCE |
            SIMPLE_SETMARRIED | CUSTOM_TUNE_TUNEOS_FILE_NOT_REQUIRED,
        .mainecm_index = 0,
        .ecm_types = {13, INVALID_ECM_DEF, INVALID_ECM_DEF},
        .templOSfilename = NULL,
    },
    [10] =  //448K Black Oak Engine (CAN)                               (0x0A)
    {
        .index = 10,
        .flags = REQUIRE_OVERLAY | REQUIRE_WAKEUP | SIMPLE_SETMARRIED |
            CUSTOM_TUNE_TUNEOS_FILE_NOT_REQUIRED,
        .mainecm_index = 0,
        .ecm_types = {5, INVALID_ECM_DEF, INVALID_ECM_DEF},
        .templOSfilename = NULL,
    },
    [11] =  //512K Black Oak Engine (CAN)                               (0x0B)
    {
        .index = 11,
        .flags = REQUIRE_OVERLAY | REQUIRE_WAKEUP | SIMPLE_SETMARRIED |
            CUSTOM_TUNE_TUNEOS_FILE_NOT_REQUIRED,
        .mainecm_index = 0,
        .ecm_types = {15, INVALID_ECM_DEF, INVALID_ECM_DEF},
        .templOSfilename = NULL,
    },
    [12] =  //448K Black Oak (CAN) + 448K Black Oak Trans (CAN)         (0x0C)
    {
        .index = 12,
        //UNLOCK_ALL_ECMS_FIRST | 
        .flags = REQUIRE_OVERLAY | REQUIRE_WAKEUP | SIMPLE_SETMARRIED |
            CUSTOM_TUNE_TUNEOS_FILE_NOT_REQUIRED |
                CHECK_SUCCESS_AFTER_DOWNLOAD,
        .mainecm_index = 0,
        .ecm_types = {5, 6, INVALID_ECM_DEF},
        .templOSfilename = NULL,
    },
    [13] =  //512K Black Oak Engine (CAN) + 512K Black Oak Trans (CAN)  (0x0D)
    {
        .index = 13,
        //UNLOCK_ALL_ECMS_FIRST | 
        .flags = REQUIRE_OVERLAY | REQUIRE_WAKEUP | SIMPLE_SETMARRIED |
            CUSTOM_TUNE_TUNEOS_FILE_NOT_REQUIRED | 
                CHECK_SUCCESS_AFTER_DOWNLOAD,
        .mainecm_index = 0,
        .ecm_types = {15, 16, INVALID_ECM_DEF},
        .templOSfilename = NULL,
    },
    [14] =   //Seimens 2048K Green Oak (6.4L Trans) (CAN)               (0x0E)
    {
        .index = 14,
        .flags = REQUIRE_WAKEUP | CUSTOM_TUNE_TUNEOS_FILE_NOT_REQUIRED,
        .mainecm_index = 0,
        .ecm_types = {23, INVALID_ECM_DEF, INVALID_ECM_DEF},
        .templOSfilename = NULL,
    },
    [15] =  //Silver Oak 1024 (CAN)                                     (0x0F)
    {
        .index = 15,
        .flags = REQUIRE_OVERLAY | REQUIRE_WAKEUP | SIMPLE_SETMARRIED |
            CUSTOM_TUNE_TUNEOS_FILE_NOT_REQUIRED,
        .mainecm_index = 0,
        .ecm_types = {28, INVALID_ECM_DEF, INVALID_ECM_DEF},
        .templOSfilename = NULL,
    },
    [16] =  //Silver Oak 2048 -  (CAN)                                (0x10)
    {
        .index = 16,
        .flags = REQUIRE_OVERLAY | REQUIRE_WAKEUP | SIMPLE_SETMARRIED |
             ALLOW_SKIP_UNSUPPORTED_SUB_ECM | CUSTOM_TUNE_TUNEOS_FILE_NOT_REQUIRED,
        .mainecm_index = 0,
        .ecm_types = {21, INVALID_ECM_DEF, INVALID_ECM_DEF},
        .templOSfilename = NULL,
    },
    [17] =  //2048 Black Oak (CAN)                                      (0x11)
    {
        .index = 17,
        .flags = REQUIRE_OVERLAY | REQUIRE_WAKEUP | SIMPLE_SETMARRIED |
            CUSTOM_TUNE_TUNEOS_FILE_NOT_REQUIRED,
        .mainecm_index = 0,
        .ecm_types = {20, INVALID_ECM_DEF, INVALID_ECM_DEF},
        .templOSfilename = NULL,
    },
    [27] =  //Seimens 2048K Green Oak (6.4L Engine) (CAN)               (0x1B)
    {
        .index = 27,
        .flags = REQUIRE_WAKEUP | CUSTOM_TUNE_TUNEOS_FILE_NOT_REQUIRED,
        .mainecm_index = 0,
        .ecm_types = {22, INVALID_ECM_DEF, INVALID_ECM_DEF},
        .templOSfilename = NULL,
    },
    [28] =  //Bosch ME9 UK (CAN)                                        (0x1C)
    {
        .index = 28,
        .flags = REQUIRE_WAKEUP | SIMPLE_SETMARRIED |
            CUSTOM_TUNE_TUNEOS_FILE_NOT_REQUIRED,
        .mainecm_index = 0,
        .ecm_types = {49, INVALID_ECM_DEF, INVALID_ECM_DEF},
        .templOSfilename = NULL,
    },
    [29] =  //1536K Green Oak (CAN)                                     (0x1D)
    {
        .index = 29,
        .flags = REQUIRE_OVERLAY | REQUIRE_WAKEUP | SIMPLE_SETMARRIED |
            CUSTOM_TUNE_TUNEOS_FILE_NOT_REQUIRED,
        .mainecm_index = 0,
        .ecm_types = {19, INVALID_ECM_DEF, INVALID_ECM_DEF},
        .templOSfilename = NULL,
    },
    [30] =  //448K Black Oak (SCP32)                                    (0x1E)
    {
        .index = 30,
        .flags = REQUIRE_OVERLAY | REQUIRE_WAKEUP | WAKEUP_CHECK_ONCE |
            SIMPLE_SETMARRIED | CUSTOM_TUNE_TUNEOS_FILE_NOT_REQUIRED,
        .mainecm_index = 0,
        .ecm_types = {24, INVALID_ECM_DEF, INVALID_ECM_DEF},
        .templOSfilename = NULL,
    },
    [31] =  //256K EEC (SCP)                                            (0x1F)
    {
        .index = 31,
        .flags = REQUIRE_OVERLAY | REQUIRE_WAKEUP | WAKEUP_CHECK_ONCE |
            CUSTOM_TUNE_TUNEOS_FILE_NOT_REQUIRED,
        .mainecm_index = 0,
        .ecm_types = {25, INVALID_ECM_DEF, INVALID_ECM_DEF},
        .templOSfilename = NULL,
    },
    [32] =  //'08 Ford Focus - Green Oak 2368K (CAN)                    (0x20)
    {
        .index = 32,
        .flags = REQUIRE_OVERLAY | REQUIRE_WAKEUP | SIMPLE_SETMARRIED |
            CUSTOM_TUNE_TUNEOS_FILE_NOT_REQUIRED,
        .mainecm_index = 0,
        .ecm_types = {26, INVALID_ECM_DEF, INVALID_ECM_DEF},
        .templOSfilename = NULL,
    },
    [34] =  //GreenOak 1920k CBP-A1 ISO14229 (CAN)                        (0x22)
            //Related Vehicles: 2009+ Ford Escape 2.5L 
    {
        .index = 34,
        .flags = REQUIRE_OVERLAY | REQUIRE_WAKEUP | CHECK_KEYON_PRIOR_WAKEUP | 
            SIMPLE_SETMARRIED | CUSTOM_TUNE_TUNEOS_FILE_NOT_REQUIRED
#ifdef TESTING_ALLOW_BLANK_VIN
                | ALLOW_BLANK_VIN,
#else
                ,
#endif

        .mainecm_index = 0,
        .ecm_types = {32, INVALID_ECM_DEF, INVALID_ECM_DEF},
        .templOSfilename = NULL,
    },
    [35] =  //Spanish Oak 2048K ISO14229 (CAN)                          (0x23)
    {
        .index = 35,
        .flags = REQUIRE_OVERLAY | REQUIRE_WAKEUP | SIMPLE_SETMARRIED |
            CUSTOM_TUNE_TUNEOS_FILE_NOT_REQUIRED,
        .mainecm_index = 0,
        .ecm_types = {30, INVALID_ECM_DEF, INVALID_ECM_DEF},
        .templOSfilename = NULL,
    },
    [36] =  //'09 Ford F-150 - Green Oak 2368K (CAN)                    (0x24)
    {
        .index = 36,
        .flags = REQUIRE_OVERLAY | SKIP_RECYCLE_KEY_ON | REQUIRE_WAKEUP |
            SIMPLE_SETMARRIED | CUSTOM_TUNE_TUNEOS_FILE_NOT_REQUIRED,
        .mainecm_index = 0,
        .ecm_types = {27, INVALID_ECM_DEF, INVALID_ECM_DEF},
        .templOSfilename = NULL,
    },
    [38] =  //Green Oak 2368K (CAN) & Golden Oak 1024 trans (CAN)       (0x26)
    {
        .index = 38,
        .flags = REQUIRE_OVERLAY | SKIP_RECYCLE_KEY_ON | REQUIRE_WAKEUP |
            SIMPLE_SETMARRIED | CUSTOM_TUNE_TUNEOS_FILE_NOT_REQUIRED,
        .mainecm_index = 0,
        .ecm_types = {27, 28, INVALID_ECM_DEF},
        .templOSfilename = NULL,
    },
    [39] =  //Silver Oak 2048 Bosch ME9 US (CAN)                        (0x27)
    {
        .index = 39,
        .flags = REQUIRE_OVERLAY | REQUIRE_WAKEUP | SIMPLE_SETMARRIED |
            CUSTOM_TUNE_TUNEOS_FILE_NOT_REQUIRED,
        .mainecm_index = 0,
        .ecm_types = {29, INVALID_ECM_DEF, INVALID_ECM_DEF},
        .templOSfilename = NULL,
    },
    [40] =  //Silver Oak 2048 Bosch ME9 US (CAN) & Golden Oak 1024 (CAN)(0x28)
    {
        .index = 40,
        .flags = REQUIRE_OVERLAY | REQUIRE_WAKEUP | SIMPLE_SETMARRIED |
            CUSTOM_TUNE_TUNEOS_FILE_NOT_REQUIRED,
        .mainecm_index = 0,
        .ecm_types = {29, 28, INVALID_ECM_DEF},
        .templOSfilename = NULL,
    },
    [41] =  //1024K Spanish Oak (CAN) & Golden Oak 1024 (CAN)           (0x29)
    {
        .index = 41,
        .flags = REQUIRE_OVERLAY | REQUIRE_WAKEUP | SIMPLE_SETMARRIED |
            CUSTOM_TUNE_TUNEOS_FILE_NOT_REQUIRED,
        .mainecm_index = 0,
        .ecm_types = {7, 28, INVALID_ECM_DEF},
        .templOSfilename = NULL,
    },
    [42] =  //512K Black Oak Trans (CAN)                                (0x2A)
    {
        .index = 42,
        .flags = REQUIRE_WAKEUP | SIMPLE_SETMARRIED |
            CUSTOM_TUNE_TUNEOS_FILE_NOT_REQUIRED,
        .mainecm_index = 0,
        .ecm_types = {16, INVALID_ECM_DEF, INVALID_ECM_DEF},
        .templOSfilename = NULL,
    },
    [43] =  //6.4L 2048K Siemens (CAN) + 2048K Silver Oak Trans (CAN)   (0x2B)
    {
        .index = 43,
        .flags = REQUIRE_WAKEUP | CUSTOM_TUNE_TUNEOS_FILE_NOT_REQUIRED,
        .mainecm_index = 0,
        .ecm_types = {22, 23, INVALID_ECM_DEF},
        .templOSfilename = NULL,
    },
    [44] =  //Ryan's test/development processor (CAN)                   (0x2C)
    {
        .index = 44,
        .flags = 0,
        .mainecm_index = 0,
        .ecm_types = {INVALID_ECM_DEF, INVALID_ECM_DEF, INVALID_ECM_DEF},
        .templOSfilename = NULL,
    },
    [45] =  //448K Black Oak Trans (CAN)                                (0x2D)
    {
        .index = 45,
        .flags = REQUIRE_WAKEUP | SIMPLE_SETMARRIED |
            CUSTOM_TUNE_TUNEOS_FILE_NOT_REQUIRED,
        .mainecm_index = 0,
        .ecm_types = {6, INVALID_ECM_DEF, INVALID_ECM_DEF},
        .templOSfilename = NULL,
    },
    [50] =  //1664K GreenOak CBP-B ISO14229 (CAN)                       (0x32)
    {
        .index = 50,
        .flags = REQUIRE_OVERLAY | REQUIRE_WAKEUP | SKIP_RECYCLE_KEY_ON | 
            SIMPLE_SETMARRIED | CUSTOM_TUNE_TUNEOS_FILE_NOT_REQUIRED,
        .mainecm_index = 0,
        .ecm_types = {31, INVALID_ECM_DEF, INVALID_ECM_DEF},
        .templOSfilename = NULL,
    },
    [51] =  //Green Oak CBP-C2 ISO14229 (CAN)                           (0x33)
    {
        .index = 51,
        .flags = REQUIRE_WAKEUP | SKIP_RECYCLE_KEY_ON| SIMPLE_SETMARRIED |
            CUSTOM_TUNE_TUNEOS_FILE_NOT_REQUIRED | SUPPRESS_FULL_FLASH_MESSAGE,
        .mainecm_index = 0,
        .ecm_types = {34, INVALID_ECM_DEF, INVALID_ECM_DEF},
        .templOSfilename = NULL,
    },
    [52] =  //Green Oak CBP-A2 ISO14229 (CAN)                           (0x34)
    {
        .index = 52,
        .flags = REQUIRE_WAKEUP | SKIP_RECYCLE_KEY_ON | SIMPLE_SETMARRIED |
            CUSTOM_TUNE_TUNEOS_FILE_NOT_REQUIRED
#ifdef TESTING_ALLOW_BLANK_VIN
                | ALLOW_BLANK_VIN,
#else
                ,
#endif
        .mainecm_index = 0,
        .ecm_types = {36, INVALID_ECM_DEF, INVALID_ECM_DEF},
        .templOSfilename = NULL,
    },
    [53] =  //2011-2012 6.7L Bosch Diesel Scorpion (CAN)                 (0x35)
    {       //40/1/EDC17_CP05/57/P545//P545_C2_1447/
        .index = 53,
        .flags = REQUIRE_WAKEUP | CHECK_KEYON_PRIOR_WAKEUP | SIMPLE_SETMARRIED
             | SUPPRESS_FULL_FLASH_MESSAGE
#ifdef TESTING_ALLOW_BLANK_VIN
                | ALLOW_BLANK_VIN,
#else
                ,
#endif
        .mainecm_index = 0,
        .ecm_types = {35, INVALID_ECM_DEF, INVALID_ECM_DEF},
        .templOSfilename = NULL,
    },
    // ----------- NEW BELOW THIS LINE - NEEDS TESTING AND VERIFICATION --------
    [54] =  // AU ZF 6spd TCM TEMP EEC DEF                               (0x36)
    {
        // TODO: VERIFY THIS
        .index = 54,
        .flags = REQUIRE_WAKEUP | CHECK_KEYON_PRIOR_WAKEUP |  
            CUSTOM_TUNE_TUNEOS_FILE_NOT_REQUIRED,
        .mainecm_index = 0,
        .ecm_types = {33, INVALID_ECM_DEF, INVALID_ECM_DEF},
        .templOSfilename = NULL,
    },
    [56] =  // PRISM: Tricore UTCU2 ISO VBF 2048K 384K CAL              (0x38)
            // 2011-2013+ (6.7L TCM)
    {       
        .index = 56,
        .flags = REQUIRE_WAKEUP | CHECK_KEYON_PRIOR_WAKEUP | SIMPLE_SETMARRIED |
            CUSTOM_TUNE_TUNEOS_FILE_NOT_REQUIRED | SUPPRESS_FULL_FLASH_MESSAGE
#ifdef TESTING_ALLOW_BLANK_VIN
                | ALLOW_BLANK_VIN,
#else
,
#endif  
        .mainecm_index = 0,
        .ecm_types = {37, INVALID_ECM_DEF, INVALID_ECM_DEF},
        .templOSfilename = NULL,
    },
    [57] =  // 2011-2012 6.7L Stacked                                   (0x39)
    {       
        .index = 57,
        .flags = REQUIRE_WAKEUP | CHECK_KEYON_PRIOR_WAKEUP | SIMPLE_SETMARRIED
             | SUPPRESS_FULL_FLASH_MESSAGE
#ifdef TESTING_ALLOW_BLANK_VIN
                | ALLOW_BLANK_VIN,
#else
,
#endif  
        .mainecm_index = 0,
        .ecm_types = {35, 37, INVALID_ECM_DEF},
        .templOSfilename = NULL,
    },
    [58] =  // 2011-2012 6.7L Bosch Diesel Scorpion (CAN)                 (0x3A)          // !! MAYBE NOT NEEDED
    {       // CAL SECITON ONLY USING MODIFIED BOOTLOADER
        .index = 58,
        .flags = REQUIRE_WAKEUP | CHECK_KEYON_PRIOR_WAKEUP | SIMPLE_SETMARRIED
             | SUPPRESS_FULL_FLASH_MESSAGE
#ifdef TESTING_ALLOW_BLANK_VIN
                | ALLOW_BLANK_VIN,
#else
,
#endif  
        .mainecm_index = 0,
        .ecm_types = {35, INVALID_ECM_DEF, INVALID_ECM_DEF},
        .templOSfilename = NULL,
    },
    [59] =  // 2011 F-150 3.5L Ecoboost, 2013 Escape                    (0x3B)
    {
        .index = 59,
        .flags = REQUIRE_WAKEUP | CHECK_KEYON_PRIOR_WAKEUP | SIMPLE_SETMARRIED |
            CUSTOM_TUNE_TUNEOS_FILE_NOT_REQUIRED | SUPPRESS_FULL_FLASH_MESSAGE
#ifdef TESTING_ALLOW_BLANK_VIN
                | ALLOW_BLANK_VIN,
#else
,
#endif
        .mainecm_index = 0,
        .ecm_types = {41, INVALID_ECM_DEF, INVALID_ECM_DEF},
        .templOSfilename = NULL,
    },
    [60] =  //6.7L Bosch Diesel Scorpion (CAN)                          (0x3C)
    {       // Full stock from X3P, this necessary?
        .index = 60,
        .flags = REQUIRE_WAKEUP | CHECK_KEYON_PRIOR_WAKEUP | SIMPLE_SETMARRIED |
            CUSTOM_TUNE_TUNEOS_FILE_NOT_REQUIRED | SUPPRESS_FULL_FLASH_MESSAGE
#ifdef TESTING_ALLOW_BLANK_VIN
                | ALLOW_BLANK_VIN,
#else
,
#endif
        .mainecm_index = 0,
        .ecm_types = {35, INVALID_ECM_DEF, INVALID_ECM_DEF},
        .templOSfilename = NULL,
    },
    [61] =  //6.7L Bosch Diesel Scorpion (CAN)                          (0x3D)
    {       // Stock Stacked from X3P, this necessary?
        .index = 61,
        .flags = REQUIRE_WAKEUP | CHECK_KEYON_PRIOR_WAKEUP | SIMPLE_SETMARRIED |
            CUSTOM_TUNE_TUNEOS_FILE_NOT_REQUIRED | SUPPRESS_FULL_FLASH_MESSAGE
#ifdef TESTING_ALLOW_BLANK_VIN
                | ALLOW_BLANK_VIN,
#else
,
#endif
        .mainecm_index = 0,
        .ecm_types = {35, 37, INVALID_ECM_DEF},
        .templOSfilename = NULL,
    },    
    [64] =  //1024K Spanish Oak + 1024k Bosch TCM (6 speed)             (0x40)
    {       // Australia ONLY
        .index = 64,
        .flags = REQUIRE_OVERLAY | REQUIRE_WAKEUP | SIMPLE_SETMARRIED |
            CUSTOM_TUNE_TUNEOS_FILE_NOT_REQUIRED | APPROVE_SKIP_FLASH_TCM,
        .mainecm_index = 0,
        .ecm_types = {7, 33, INVALID_ECM_DEF},
        .templOSfilename = NULL,
    },
    [65] =  //1472K Black Oak + 1024k Bosch TCM (6 speed)               (0x41)
    {       //Australia ONLY
        .index = 65,
        .flags = REQUIRE_WAKEUP | CHECK_KEYON_PRIOR_WAKEUP | SIMPLE_SETMARRIED |
            CUSTOM_TUNE_TUNEOS_FILE_NOT_REQUIRED | APPROVE_SKIP_FLASH_TCM,
        .mainecm_index = 0,
        .ecm_types = {4, 33, INVALID_ECM_DEF},
        .templOSfilename = NULL,
    },
    [66] =  //2012 Focus C346 Tricore 1767 MED17.0 Bosch Gas DI ISO     (0x42)
    {
        .index = 66,
        .flags = REQUIRE_WAKEUP | SKIP_RECYCLE_KEY_ON | SIMPLE_SETMARRIED |
            CUSTOM_TUNE_TUNEOS_FILE_NOT_REQUIRED | SUPPRESS_FULL_FLASH_MESSAGE
#ifdef TESTING_ALLOW_BLANK_VIN
                | ALLOW_BLANK_VIN,
#else
,
#endif
        .mainecm_index = 0,
        .ecm_types = {43, INVALID_ECM_DEF, INVALID_ECM_DEF},
        .templOSfilename = NULL,
    },
    [67] =  //2012 Focus C346 Tricore 1767 MED17.0 Bosch Gas DI ISO     (0x43)
    {       // TCM Stacked
        .index = 67,
        .flags = REQUIRE_WAKEUP | CHECK_KEYON_PRIOR_WAKEUP | SIMPLE_SETMARRIED |
            CUSTOM_TUNE_TUNEOS_FILE_NOT_REQUIRED | SUPPRESS_FULL_FLASH_MESSAGE
#ifdef TESTING_ALLOW_BLANK_VIN
                | ALLOW_BLANK_VIN,
#else
,
#endif
        .mainecm_index = 0,
        .ecm_types = {43, 39, INVALID_ECM_DEF},
        .templOSfilename = NULL,
    },
    [68] =  // 2011 Fiesta VF99C 2.0L NA PCM                           (0x44)
    {
        .index = 68,
        .flags = REQUIRE_WAKEUP | CHECK_KEYON_PRIOR_WAKEUP | SIMPLE_SETMARRIED |
            CUSTOM_TUNE_TUNEOS_FILE_NOT_REQUIRED | SUPPRESS_FULL_FLASH_MESSAGE
#ifdef TESTING_ALLOW_BLANK_VIN
                | ALLOW_BLANK_VIN,
#else
,
#endif
        .mainecm_index = 0,
        .ecm_types = {38, INVALID_ECM_DEF, INVALID_ECM_DEF},
        .templOSfilename = NULL,
    },
    [69] =  //2012 Focus PCM/TCM-Extended128k STACKED                   (0x45)
    {
        .index = 69,
        .flags = REQUIRE_WAKEUP | CHECK_KEYON_PRIOR_WAKEUP | SIMPLE_SETMARRIED |
            CUSTOM_TUNE_TUNEOS_FILE_NOT_REQUIRED | SUPPRESS_FULL_FLASH_MESSAGE
#ifdef TESTING_ALLOW_BLANK_VIN
                | ALLOW_BLANK_VIN,
#else
,
#endif
        .mainecm_index = 0,
        .ecm_types = {43, 44, INVALID_ECM_DEF},
        .templOSfilename = NULL,
    },
    [71] =  //Fiesta TCM only                                           (0x47)
    {
        .index = 71,
        .flags = REQUIRE_WAKEUP | CHECK_KEYON_PRIOR_WAKEUP | SIMPLE_SETMARRIED |
            CUSTOM_TUNE_TUNEOS_FILE_NOT_REQUIRED
#ifdef TESTING_ALLOW_BLANK_VIN
                | ALLOW_BLANK_VIN,
#else
,
#endif
        .mainecm_index = 0,
        .ecm_types = {39, INVALID_ECM_DEF, INVALID_ECM_DEF},
        .templOSfilename = NULL,
    },
    [72] =  //Fiesta Stacked                                            (0x48)
    {       
        .index = 72,
        .flags = REQUIRE_WAKEUP | CHECK_KEYON_PRIOR_WAKEUP | SIMPLE_SETMARRIED |
            CUSTOM_TUNE_TUNEOS_FILE_NOT_REQUIRED | SUPPRESS_FULL_FLASH_MESSAGE
#ifdef TESTING_ALLOW_BLANK_VIN
                | ALLOW_BLANK_VIN,
#else
,
#endif
        .mainecm_index = 0,
        .ecm_types = {38, 39, INVALID_ECM_DEF},
        .templOSfilename = NULL,
    },
    [73] =  //6.7L Bosch Diesel (CAN)                                  (0x49)
    {       // 2013+ 
        .index = 73,
        .flags = REQUIRE_WAKEUP | CHECK_KEYON_PRIOR_WAKEUP | SIMPLE_SETMARRIED | 
            CUSTOM_TUNE_TUNEOS_FILE_NOT_REQUIRED | SUPPRESS_FULL_FLASH_MESSAGE
#ifdef TESTING_ALLOW_BLANK_VIN
                | ALLOW_BLANK_VIN,
#else
                ,
#endif
        .mainecm_index = 0,
        .ecm_types = {47, INVALID_ECM_DEF, INVALID_ECM_DEF},
        .templOSfilename = NULL,
    },
    [74] =  //6.7L Bosch Diesel Stacked (CAN)                          (0x4A)
    {       // 2013+ 
        .index = 74,
        .flags = REQUIRE_WAKEUP | CHECK_KEYON_PRIOR_WAKEUP | SIMPLE_SETMARRIED | 
            CUSTOM_TUNE_TUNEOS_FILE_NOT_REQUIRED | SUPPRESS_FULL_FLASH_MESSAGE
#ifdef TESTING_ALLOW_BLANK_VIN
                | ALLOW_BLANK_VIN,
#else
                ,
#endif
        .mainecm_index = 0,
        .ecm_types = {47, 37, INVALID_ECM_DEF},
        .templOSfilename = NULL,
    },
    [75] =  //PRISM: eSys CBP-A2 ISO14229 (CAN)                         (0x4B)
    {       // 2013+ 3.7L MKZ
        .index = 75,
        .flags = REQUIRE_WAKEUP | SKIP_RECYCLE_KEY_ON | SIMPLE_SETMARRIED |
            CUSTOM_TUNE_TUNEOS_FILE_NOT_REQUIRED
#ifdef TESTING_ALLOW_BLANK_VIN
                | ALLOW_BLANK_VIN,
#else
                ,
#endif
        .mainecm_index = 0,
        .ecm_types = {48, INVALID_ECM_DEF, INVALID_ECM_DEF},
        .templOSfilename = NULL,
    },
    [76] =  //PRISM: EMS24XX Tricore TC-1791 4M (CAN)                   (0x4C)
    {       // 2015+ 5.0L Mustang
        .index = 76,
        .flags = REQUIRE_WAKEUP | SKIP_RECYCLE_KEY_ON | SIMPLE_SETMARRIED |
            CUSTOM_TUNE_TUNEOS_FILE_NOT_REQUIRED | SUPPRESS_FULL_FLASH_MESSAGE
#ifdef TESTING_ALLOW_BLANK_VIN
                | ALLOW_BLANK_VIN,
#else
                ,
#endif
        .mainecm_index = 0,
        .ecm_types = {50, INVALID_ECM_DEF, INVALID_ECM_DEF},
        .templOSfilename = NULL,
    },
};

/*------------------------------------------------------------------------------
 IMPORTANT NOTES
 1) Maintain VEH_FORD_IsVehicleType60L, VEH_FORD_IsVehicleType67L & 
    VEH_FORD_IsVehicleTypeF150Ecoboost35L if new types are added.
------------------------------------------------------------------------------*/
