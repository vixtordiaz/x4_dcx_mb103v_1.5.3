/**
  **************** (C) COPYRIGHT 2011 SCT Performance, LLC *********************
  * File Name          : ford_ecm_utility_testfunc.h
  * Device / System    : SCT products using the "CommonCode" platform
  * Author             : Quyen Leba
  *
  * Version            : 1 
  * Date               : 05/02/2011
  * Description        : 
  *                    : 
  *
  *
  * History            : 05/02/2011 M. Davis
  *                    :   Add SCT Copyright header,
  *
  ******************************************************************************
  */

#ifndef __FORD_ECM_UTILITY_TESTFUNC_H
#define __FORD_ECM_UTILITY_TESTFUNC_H

#include <arch/gentype.h>
#include <common/obd2.h>
#include <common/ecm_defs.h>

u8 ford_ecm_utility_testfunc_emptytest(u16 ecm_type);
u8 ford_ecm_utility_testfunc_67L_PCM(u16 ecm_type);

#endif    // __FORD_ECM_UTILITY_TESTFUNC_H
