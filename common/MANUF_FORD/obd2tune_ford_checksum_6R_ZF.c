/**
  **************** (C) COPYRIGHT 2011 SCT Performance, LLC *********************
  * File Name          : obd2tune_ford_checksum_6R_ZF.c
  * Device / System    : SCT products using the "CommonCode" platform
  * Author             : Patrick Downs, Quyen Leba
  *
  * Version            : 1 
  * Date               : 08/15/2012
  * Description        : Checksum functions for Bosch 6R(US)/ZF(AU) TCMs 
  *                      
  *
  *
  *
  ******************************************************************************
  */

//------------------------------------------------------------------------------
// TODO:
// Log error point:
//------------------------------------------------------------------------------

#include <string.h>
#include <common/genmanuf_overload.h>
#include <common/statuscode.h>
#include <fs/genfs.h>
#include <common/ecm_defs.h>
#include "obd2tune_ford_checksum_6R_ZF.h"
#include <common/filestock.h>


// Private
u32 locate_end_address(u32 engine_file_size, u32 *end_address);
u32 ComputeChecksum(u32 start_address, u32 checksum_length, u8 *csreturn);

//------------------------------------------------------------------------------
// Apply checksum for Bosch 6R to flash.bin, needs to already be opened prior
// to calling
// Inputs:  u16 veh_type
//          u32 offset (flash file offset)
// Return:  u8  status
// Engineer: Patrick Downs, Quyen Leba
//------------------------------------------------------------------------------
int Bosch_6R_checksum(u8 ecm_type, u32 offset)
{
    u8  checksum[2] = {0,0};
    u32 start_address = 0x00060084;
    u32 end_address = 0;
    u32 checksum_length = 0;
    u32 chksum_address = 0;
    u32 engine_file_size = 0;
    int status;
    
    engine_file_size = offset;
    
    chksum_address = engine_file_size + 0x30080;
    
    status = locate_end_address(engine_file_size, (u32*)&end_address);
    if(status != S_SUCCESS)
        return S_FAIL;
    
    checksum_length = end_address - start_address + 1;
     
    start_address = engine_file_size + (start_address - (ECM_GetOSReadBlockSize(ecm_type,0))); 
        
    status = ComputeChecksum(start_address, checksum_length, &checksum[0]);
    if(status != S_SUCCESS)
    {
        return S_FAIL;
    }
    
    status = filestock_updateflashfile(chksum_address,checksum,2);
    
    return status;
}

//------------------------------------------------------------------------------
// Apply checksum for Bosch AU ZF to flash.bin, needs to already be opened prior
// to calling
// Inputs:  u16 ecm_type
//          u32 offset (flash file offset)
// Return:  u8  status
// Engineer: Patrick Downs, Quyen Leba
//------------------------------------------------------------------------------
int Bosch_AU_ZF_checksum(u8 ecm_type, u32 offset)
{
    char  tempbuffer[4];
    int ecmFileLength = 0;
    u16 newCrc;
    int status;
    char checkvalue;  
    long crcType = 0;
    long start = 0;
    long end = 0;
    long crcAddress = 0;
    u32 bytecount;
    
    ecmFileLength = offset;
    
    // Find the CRC Type value @ 0x1FEA0
    status = filestock_read_at_position((0x1FEA0+ecmFileLength), (u8*)&tempbuffer, 4, (u32*)&bytecount);
    if(status != S_SUCCESS)
        return status;
    
    crcType = (u32)((tempbuffer[0] << 24) | (tempbuffer[1]<< 16) | (tempbuffer[1]<< 16) |(tempbuffer[2]<< 8) | tempbuffer[3]);
    // Two checksum values to search for. 0x006E4A58 == orion tcm & 0x006E6D98 == copperhead tcm
    if((crcType != 0x006E4A58) && (crcType != 0x006E6D98))
        return S_FAIL; // Unable to locate valid CRC location
    
    // Look for check value 'D' 8 bytes before the checksum location @ 0x1FE98
    status = filestock_read_at_position((0x1FE98+ecmFileLength), (u8*)&checkvalue, 1, (u32*)&bytecount);
    if(status != S_SUCCESS) return status;
    
    if(checkvalue != 'D')
        return S_FAIL; // Unable to locate checkvalue 'D'
    
    // Setup address variables from crcType
    start = (crcType - 0x6e0080);
    end = start + (0x6fff37 - crcType);
    crcAddress = end + 1; 
    
    // Get auctual crc value, not used in calculating new CRC but good for debug.
    status = filestock_read_at_position((crcAddress+ecmFileLength), (u8*)&tempbuffer, 2, (u32*)&bytecount);
    if(status != S_SUCCESS)
        return status;
       
    // Add 1 to the checksumlength
    status = ComputeChecksum((start + ecmFileLength), (end-start)+1, (u8*)&newCrc);
    if(status != S_SUCCESS)
        return S_FAIL; // Failed to calculate CRC
    
    // Update new CRC value in flash.bin file
    status = filestock_updateflashfile((crcAddress+ecmFileLength),(u8*)&newCrc,2);    
    
    return status;
}

#define         RAM_BUFFER_SIZE     (4096)
u32 locate_end_address(u32 engine_file_size, u32 *end_address)
{
    F_FILE  *f;
    u8      *G_RAM_Buffer;
    u8      *buffer;
    u8      signature[5]= {0x00, 0x06, 0x00, 0x84, 0};
    s32     bytes_searched =0;
    s32     bytes_read = 0;
    u32     status;
    
    *end_address = 0;
    status = S_FAIL;
    
    G_RAM_Buffer = __malloc(RAM_BUFFER_SIZE);
    if (G_RAM_Buffer == NULL)
        return S_MALLOC;
    
    status = filestock_set_position(engine_file_size);
    if (status != S_SUCCESS)
    {
       status = S_SEEKFILE;
       goto LOCATE_END_ADDRESS_DONE;
    }
        
    f = filestock_getflashfile_fptr();    
    while(feof(f) == NULL)
    {
        status = filestock_read(G_RAM_Buffer, RAM_BUFFER_SIZE, (u32*)&bytes_read);
        if(status != S_SUCCESS)
            goto LOCATE_END_ADDRESS_DONE; // TODO: check that this won't fail unintentionaly when near end of file

        buffer = G_RAM_Buffer;
        bytes_searched = 0;
        
        while(bytes_searched < bytes_read)
        {
            if(buffer[bytes_searched] == signature[0])
            {
                if(memcmp((void*)&buffer[bytes_searched],(void*)signature, 4) == 0)
                {
                    bytes_searched += 4;
                    *end_address = (u32)(buffer[bytes_searched]<<24)+ (buffer[bytes_searched+1]<<16) +
                        (buffer[bytes_searched+2]<<8) + buffer[bytes_searched+3];
                    status = S_SUCCESS;
                    goto LOCATE_END_ADDRESS_DONE;                    
                }
            }
            bytes_searched++;
        }
    }

LOCATE_END_ADDRESS_DONE:
    if(G_RAM_Buffer)
        __free(G_RAM_Buffer);
    
    return status;
}

u32 ComputeChecksum(u32 start_address, u32 checksum_length, u8 *csreturn)
{
    u16       chksum = 0;
    u16       testbit;
    u32       i;
    u32       bytes_read = 0;
    u32       bytes_complete = 0;
    u32       length_left = checksum_length;
    u32       status;
    u8        *G_RAM_Buffer = 0;
    u8        j;
    u8        *bytes;
    
    status = S_SUCCESS;
    
    G_RAM_Buffer = __malloc(RAM_BUFFER_SIZE);
    if (G_RAM_Buffer == NULL)
        return S_MALLOC;
        
    status = filestock_set_position(start_address);
    if (status != S_SUCCESS)
    {
       status = S_SEEKFILE;
       goto COMPUTE_DONE;
    }
    
    while(bytes_complete < checksum_length)
    {
        if(length_left < RAM_BUFFER_SIZE)
        {
            status = filestock_read(G_RAM_Buffer, length_left, (u32*)&bytes_read);
            if(bytes_read != length_left)
            {
                status = S_READFILE;
                goto COMPUTE_DONE;
            }
        }
        else
        {
            status = filestock_read(G_RAM_Buffer, RAM_BUFFER_SIZE, (u32*)&bytes_read);
            if(bytes_read != RAM_BUFFER_SIZE)
            {
                status = S_READFILE;
                goto COMPUTE_DONE;
            }
        }
        
        bytes = G_RAM_Buffer;
        
        for(i =0; i<bytes_read; i++)
        {
            for(j = 0x80; j>0; j>>=1)
            {
                testbit = (unsigned short)(chksum & 0x8000);
                chksum <<=1;
                if((bytes[i] & j) != 0)
                    testbit ^= 0x8000;
                if(testbit != 0)
                    chksum ^= 0x1021;
            }
        }
        
        bytes_complete += bytes_read;
        length_left -= bytes_read;
    }
    
COMPUTE_DONE:
    if(G_RAM_Buffer)
        __free(G_RAM_Buffer);
    
    csreturn[1] = chksum & 0xff;
    csreturn[0] = (chksum >> 8) & 0xff;
    
    return status;
}






