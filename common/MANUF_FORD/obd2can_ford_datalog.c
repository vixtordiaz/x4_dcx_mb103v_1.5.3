/**
  **************** (C) COPYRIGHT 2011 SCT Performance, LLC *********************
  * File Name          : obd2can_ford_datalog.c
  * Device / System    : SCT products using the "CommonCode" platform
  * Author             : Quyen Leba
  *
  * Version            : 1 
  * Date               : 05/02/2011
  * Description        : 
  *                    : 
  *
  *
  * History            : 05/02/2011 M. Davis
  *                    :   Add SCT Copyright header,
  *
  ******************************************************************************
  */

//------------------------------------------------------------------------------
// Log error point: 0x4900 -> 0x4AFF (shared with obd2datalog.c)
// Use 0x4980 -> 0x499F (shared with obd2datalog_ford.c)
//------------------------------------------------------------------------------

#include <string.h>
#include <board/rtc.h>
#include <board/delays.h>
#include <common/statuscode.h>
#include <common/obd2datalog.h>
#include <common/obd2can.h>
#include "obd2can_ford_datalog.h"

extern Datalog_Mode gDatalogmode;       //from obd2datalog.c
extern Datalog_Info *dataloginfo;       //from obd2datalog.c

//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
void obd2can_ford_datalog_sendtesterpresent()
{
    u8  i;
    const u32 ecm_id[ECM_MAX_COUNT] = {Obd2CanEcuId_7E0,Obd2CanEcuId_7E1,0};
    Obd2can_ServiceData servicedata;

    obd2can_servicedata_init(&servicedata);
    servicedata.ecm_id = 0x7DF;
    servicedata.service = 0x3E;
    servicedata.subservice = 0x80;
    servicedata.response_expected = FALSE;
    for(i=0;i<ECM_MAX_COUNT;i++)
    {
        if (dataloginfo->commlevel[i] == CommLevel_ISO14229)
        {
            servicedata.ecm_id = 0x7DF; //TODOQ2: check this again
            servicedata.subservice = 0x80;
            servicedata.commlevel = CommLevel_ISO14229;
        }
        else if (dataloginfo->commlevel[i] == CommLevel_KWP2000)
        {
            servicedata.ecm_id = ecm_id[i];
            servicedata.subservice = 0x02;
            servicedata.commlevel = CommLevel_KWP2000;
        }
        else if (dataloginfo->commlevel[i] == CommLevel_Unknown)
        {
            break;
        }
        obd2can_txrx(&servicedata);
    }
}

//------------------------------------------------------------------------------
// Check if all signals fit in available packets for commlevel 14229
// Input:   u8  ecm_index (0: pcm, 1: tcm)
// Return:  u8  status
// Engineer: Quyen Leba
//------------------------------------------------------------------------------
u8 obd2can_ford_datalog_evaluatesignalsetup_commlevel_14229(u8 ecm_index)
{    
    u16 packetid;    
    u8  byteavailable[FORD_UDS_CAN_PACKETID_END-FORD_UDS_CAN_PACKETID_START];
    u8  processcheck[MAX_DATALOG_DLXBLOCK_COUNT];
    u8  processedperloop;
    u8  index;
    u16 i;
    u16 found_pidcount = 0;
    bool packet;
    
    if (dataloginfo == NULL)
    {
        log_push_error_point(0x4980);
        return S_BADCONTENT;
    }
    memset(processcheck,FALSE,sizeof(processcheck));
    memset((char*)byteavailable,
           FORD_UDS_CAN_MAX_DATABYTE_PER_PACKET,sizeof(byteavailable));

    dataloginfo->packetidcount[ecm_index] = 0;
    
    packetid = FORD_UDS_CAN_PACKETID_START;
    index = 0;
    packet = FALSE;
    
    while(packetid != FORD_UDS_CAN_PACKETID_END)
    {
        // Are all signals accounted for?
        if (dataloginfo->pidsignalcount + dataloginfo->dmrsignalcount + 
            dataloginfo->analogsignalcount + dataloginfo->oscsignalcount >= 
            dataloginfo->datalogsignalcount)
        { 
            break;
        }
            
        processedperloop = 0;
        packet = FALSE;
        
        for(i=0;i<dataloginfo->datalogsignalcount;i++)
        {
            if (processcheck[i])
            {
                //already processed
                continue;
            }
            
            if ((dataloginfo->datalogsignals[i].Control & DATALOG_CONTROL_SKIP) == 0 &&
                    dataloginfo->datalogsignals[i].SignalType.Generic.PacketNumber == DATALOG_INVALID_PACKETID &&
                    dataloginfo->datalogsignals[i].EcmIndex == ecm_index &&
                    dataloginfo->datalogsignals[i].Size <= byteavailable[index] &&
                    (dataloginfo->datalogsignals[i].PidType == PidTypeRegular ||
                    dataloginfo->datalogsignals[i].PidType == PidTypeDMR) &&
                    dataloginfo->dlxblocks[i].pidBit == 0) // Skip packet PIDs
            {                    
                processcheck[i] = TRUE;
                processedperloop++;
                found_pidcount++;
                switch (dataloginfo->datalogsignals[i].PidType)
                {
                    case PidTypeRegular:
                        dataloginfo->pidsignalcount++;                                   
                        break;
                    case PidTypeDMR:                        
                        dataloginfo->dmrsignalindex[dataloginfo->dmrsignalcount++] = i;                                
                        break;
                    default:
                        break;
                }
                dataloginfo->datalogsignals[i].SignalType.Generic.Position = 1 + FORD_UDS_CAN_MAX_DATABYTE_PER_PACKET - byteavailable[index];
                byteavailable[index] -= dataloginfo->datalogsignals[i].Size;
                dataloginfo->datalogsignals[i].SignalType.Generic.PacketNumber = packetid;               
                packet = TRUE;
            }                       
        }//for(i=0;i<dataloginfo->datalogsignalcount;i++)...
        // If valid PIDs were found, increment these
        if (packet == TRUE)
        {
            //Note: be careful here; packetid is F2xx but we only use xx part
            dataloginfo->packetidlist[ecm_index][dataloginfo->packetidcount[ecm_index]] = (u8)(packetid);
            dataloginfo->packetidcount[ecm_index]++;
            index++;
            packetid++;
        }
        if (processedperloop == 0)
        {
            break;
        }
    }//while(packetid...
    
    if(found_pidcount == 0)
    {
        dataloginfo->packetidcount[ecm_index] = 0;
    }
    
    return S_SUCCESS;
}

//------------------------------------------------------------------------------
// Check if all signals fit in available packets for commlevel regular
// Input:   u8  ecm_index (0: pcm, 1: tcm)
// Return:  u8  status
// Engineer: Quyen Leba
//------------------------------------------------------------------------------
u8 obd2can_ford_datalog_evaluatesignalsetup_commlevel_regular(u8 ecm_index)
{    
    u8  packetid;    
    u8  byteavailable[FORD_KWP_CAN_PACKETID_END-FORD_KWP_CAN_PACKETID_START];
    u8  processcheck[MAX_DATALOG_DLXBLOCK_COUNT];
    u8  processedperloop;
    u8  index;
    u16 i;    
    u16 found_pidcount = 0;
    bool packet;
    
    if (dataloginfo == NULL)
    {
        log_push_error_point(0x4981);
        return S_BADCONTENT;
    }
    memset(processcheck,FALSE,sizeof(processcheck));
    memset((char*)byteavailable,
           FORD_KWP_CAN_MAX_DATABYTE_PER_PACKET,sizeof(byteavailable));
    
    dataloginfo->packetidcount[ecm_index] = 0;
    
    packetid = FORD_KWP_CAN_PACKETID_START;
    index = 0;    
    
    while(packetid != FORD_KWP_CAN_PACKETID_END)
    {
        // Are all signals accounted for?
        if (dataloginfo->pidsignalcount + dataloginfo->dmrsignalcount + 
            dataloginfo->analogsignalcount + dataloginfo->oscsignalcount >= 
            dataloginfo->datalogsignalcount)
        { 
            break;
        }
            
        processedperloop = 0;
        packet = FALSE;
        
        for(i=0;i<dataloginfo->datalogsignalcount;i++)
        {
            if (processcheck[i])
            {
                //already processed
                continue;
            }
            
            if ((dataloginfo->datalogsignals[i].Control & DATALOG_CONTROL_SKIP) == 0 &&
                    dataloginfo->datalogsignals[i].SignalType.Generic.PacketNumber == DATALOG_INVALID_PACKETID &&
                    dataloginfo->datalogsignals[i].EcmIndex == ecm_index &&
                    dataloginfo->datalogsignals[i].Size <= byteavailable[index] &&
                    (dataloginfo->datalogsignals[i].PidType == PidTypeRegular ||
                    dataloginfo->datalogsignals[i].PidType == PidTypeDMR) &&
                    dataloginfo->dlxblocks[i].pidBit == 0) // Skip packet PIDs)
            {                
                processcheck[i] = TRUE;
                processedperloop++;
                found_pidcount++;
                switch (dataloginfo->datalogsignals[i].PidType)
                {
                    case PidTypeRegular:
                        dataloginfo->pidsignalcount++;
                        break;
                    case PidTypeDMR:                        
                        dataloginfo->dmrsignalindex[dataloginfo->dmrsignalcount++] = i;                                           
                        break;
                    default:
                        break;
                }
                // Position starts from 1
                dataloginfo->datalogsignals[i].SignalType.Generic.Position = 1 + FORD_KWP_CAN_MAX_DATABYTE_PER_PACKET - byteavailable[index];
                byteavailable[index] -= dataloginfo->datalogsignals[i].Size;
                dataloginfo->datalogsignals[i].SignalType.Generic.PacketNumber = packetid;
                packet = TRUE;                
            }
        }//for(..)
        if (packet == TRUE)
        {
            dataloginfo->packetidlist[ecm_index][dataloginfo->packetidcount[ecm_index]] = packetid;
            dataloginfo->packetidcount[ecm_index]++;
            index++;               
            packetid++;
        }
        if (processedperloop == 0)
        {
            break;
        }
    }//while(packetid...
    
    if(found_pidcount == 0)
    {
        dataloginfo->packetidcount[ecm_index] = 0;
    }

    return S_SUCCESS;
}

//------------------------------------------------------------------------------
// Check if all signals fit in available packets
// Input:   VehicleCommLevel commlevel[ECM_MAX_COUNT]
// Return:  u8  status
// Engineer: Quyen Leba
//------------------------------------------------------------------------------
u8 obd2can_ford_datalog_evaluatesignalsetup(VehicleCommLevel *vehiclecommlevel)
{
    u8  status;    
    u16 i;
    
    dataloginfo->pidsignalcount = 0;
    dataloginfo->dmrsignalcount = 0;
    dataloginfo->analogsignalcount = 0;
    dataloginfo->oscsignalcount = 0;

    // Evaluate non-packet signals
    for(i=0;i<dataloginfo->datalogsignalcount;i++)
    {
        if ((dataloginfo->datalogsignals[i].Control & DATALOG_CONTROL_SKIP) == 0)
        {
            switch (dataloginfo->datalogsignals[i].PidType)
            {
                case PidTypeAnalog:
                    if (dataloginfo->analogsignalcount < dataloginfo->analogsignalmaxavailable)
                    {
                        dataloginfo->analogsignalindex[dataloginfo->analogsignalcount++] = i;
                    }
                   break;
                case PidTypeMode1:
                    dataloginfo->pidsignalcount++;
                    break;
                case PidTypeOSC:
                    dataloginfo->oscsignalcount++;
                    break;
            }
        }
        if (dataloginfo->pidsignalcount + dataloginfo->analogsignalcount + 
            dataloginfo->oscsignalcount >= dataloginfo->datalogsignalcount)
        {
            break;
        }
    }
    // Handle 1st processor
    if (vehiclecommlevel[0] == CommLevel_ISO14229)
    {
        status = obd2can_ford_datalog_evaluatesignalsetup_commlevel_14229(0);
    }
    else if (vehiclecommlevel[0] == CommLevel_KWP2000)
    {
        status = obd2can_ford_datalog_evaluatesignalsetup_commlevel_regular(0);
    }
    else
    {
        log_push_error_point(0x4982);
        return S_NOTFIT;        
    }
    // Handle 2nd processor
    if (status == S_SUCCESS)
    {
        if (vehiclecommlevel[1] == CommLevel_ISO14229)
        {
            status = obd2can_ford_datalog_evaluatesignalsetup_commlevel_14229(1);
        }
        else if (vehiclecommlevel[1] == CommLevel_KWP2000)
        {
            status = obd2can_ford_datalog_evaluatesignalsetup_commlevel_regular(1);
        }
    }
    // If not enough packets, evaulate for single rate
    if (dataloginfo->pidsignalcount + dataloginfo->dmrsignalcount + 
        dataloginfo->analogsignalcount + dataloginfo->oscsignalcount < 
        dataloginfo->datalogsignalcount)
    { 
        // Evaluate non-packet signals
        for(i=0;i<dataloginfo->datalogsignalcount;i++)
        {
            if ((dataloginfo->datalogsignals[i].Control & DATALOG_CONTROL_SKIP) == 0 &&
                dataloginfo->datalogsignals[i].SignalType.Generic.PacketNumber == DATALOG_INVALID_PACKETID)
            {
                switch (dataloginfo->datalogsignals[i].PidType)
                {
                    case PidTypeRegular:
                        dataloginfo->pidsignalcount++;                
                        break;
                    case PidTypeDMR:
                        dataloginfo->dmrsignalcount++;
                        break;
                }
            }        
            if (dataloginfo->pidsignalcount + dataloginfo->dmrsignalcount + 
                dataloginfo->analogsignalcount + dataloginfo->oscsignalcount >= 
                dataloginfo->datalogsignalcount)
            {
                break;
            }
        }
    }
    if (dataloginfo->pidsignalcount + dataloginfo->dmrsignalcount + 
        dataloginfo->analogsignalcount + dataloginfo->oscsignalcount < 
        dataloginfo->datalogsignalcount)
    {
        // Cannot fit all signals
        log_push_error_point(0x4983);
        return S_NOTFIT;
    }    
    return status;
}

//------------------------------------------------------------------------------
// Change Datalog Diagnostic Mode
// Input:   VehicleCommLevel vehiclecommlevel[ECM_MAX_COUNT]
// Return:  u8  status
// Engineer: Quyen Leba, Tristen Pierson
// Description: Only changes diagnostic mode if not currently in mode.
//------------------------------------------------------------------------------
u8 obd2can_ford_datalog_change_diagnostic_mode(VehicleCommLevel *vehiclecommlevel)
{
    const u32 ecm_id[ECM_MAX_COUNT] = {Obd2CanEcuId_7E0,Obd2CanEcuId_7E1,0};
    u8  i;
    u8  data[4];
    u8  datalength[1];
    u8  status;
    
    status = S_SUCCESS;

    for(i=0;i<ECM_MAX_COUNT;i++)
    {
        if (vehiclecommlevel[i] == CommLevel_ISO14229)
        {
            data[0] = 0;
            obd2can_read_data_bypid(ecm_id[i], 0xD100, data, datalength, TRUE);
            if (data[0] != extendedDianosticSession_14229)
            {
                status = obd2can_ford_initiate_diagnostic_operation(FALSE,ecm_id[i],
                                                           extendedDianosticSession_14229,
                                                           TRUE);
            }
        }
        else if (vehiclecommlevel[i] == CommLevel_KWP2000)
        {
            //do nothing
        }
        else if (vehiclecommlevel[i] == CommLevel_Unknown)
        {
            break;
        }
        else
        {
            log_push_error_point(0x4984);
            status = S_NOTSUPPORT;
        }
    }

    return status;
}


//------------------------------------------------------------------------------
// Clean up previous defined packets
// Input:   VehicleCommLevel vehiclecommlevel[ECM_MAX_COUNT]
// Return:  u8  status
// Engineer: Quyen Leba
//------------------------------------------------------------------------------
u8 obd2can_ford_datalog_cleanup_packets(VehicleCommLevel *vehiclecommlevel)
{
    Obd2can_ServiceData servicedata;
    
    if (vehiclecommlevel[0] == CommLevel_ISO14229)
    {
        //TODOQ: is this correct !?! enough !?!
        obd2can_servicedata_init(&servicedata);
        servicedata.ecm_id = Obd2CanEcuId_7E0;
        servicedata.service = 0x2C;
        servicedata.subservice = 0x03;
        servicedata.commlevel = CommLevel_ISO14229;
        obd2can_txrx(&servicedata);
    }
    else if (vehiclecommlevel == CommLevel_KWP2000)
    {
        //do nothing
    }
    else
    {
        //do nothing
    }

    if (vehiclecommlevel[1] == CommLevel_ISO14229)
    {
        //TODOQ: is this correct !?! enough !?!
        obd2can_servicedata_init(&servicedata);
        servicedata.ecm_id = Obd2CanEcuId_7E1;
        servicedata.service = 0x2C;
        servicedata.subservice = 0x03;
        servicedata.commlevel = CommLevel_ISO14229;
        obd2can_txrx(&servicedata);
    }
    else if (vehiclecommlevel == CommLevel_KWP2000)
    {
        //do nothing
    }
    else
    {
        //do nothing
    }
    return S_SUCCESS;
}

//------------------------------------------------------------------------------
// Initiate pids into packets
// Input:   VehicleCommLevel commlevel[ECM_MAX_COUNT]
// Return:  u8  status
// Engineer: Quyen Leba
//------------------------------------------------------------------------------
u8 obd2can_ford_datalog_initiatepackets(VehicleCommLevel *vehiclecommlevel)
{
    u8  ecm_index;
    u32 ecm_id[ECM_MAX_COUNT] = {Obd2CanEcuId_7E0,Obd2CanEcuId_7E1,0};
    u16 packetid;
    u16 i;
    u8  status;
    int setup_pid_count;
    u32 response_ecm_id[1];
    u8  rxrawdata[8];
    u32 timeout;
    
    if (dataloginfo == NULL)
    {
        log_push_error_point(0x4986);
        return S_BADCONTENT;
    }

    setup_pid_count = 0; // A count of how many PIDs succesfully were setup in packet
    for(ecm_index=0;ecm_index<ECM_MAX_COUNT;ecm_index++)
    {
        if (vehiclecommlevel[ecm_index] == CommLevel_ISO14229)
        {
            obd2can_ford_datalog_change_diagnostic_mode(vehiclecommlevel);
            obd2can_ford_cleandynamicallydefinemessage_14229(ecm_id[ecm_index]);
            
            for(packetid=FORD_UDS_CAN_PACKETID_START;packetid<=FORD_UDS_CAN_PACKETID_END;packetid++)

            {
                for(i=0;i<dataloginfo->datalogsignalcount;i++)
                {
                    if (dataloginfo->datalogsignals[i].EcmIndex == ecm_index &&
                        dataloginfo->datalogsignals[i].SignalType.Generic.PacketNumber == packetid &&
                        (dataloginfo->datalogsignals[i].PidType == PidTypeRegular ||
                         dataloginfo->datalogsignals[i].PidType == PidTypeDMR))
                    {
                        status = obd2can_ford_dynamicallydefinemessage
                            (ecm_id[ecm_index],packetid,
                             (PidType)dataloginfo->datalogsignals[i].PidType,
                             dataloginfo->datalogsignals[i].Size,
                             //dataloginfo->datalogsignals[i].Position,
                             0x01,  //Note: ISO14229 specs this as position, but Ford must set to 0x01 - May 19, 2011
                             dataloginfo->datalogsignals[i].Address,
                             vehiclecommlevel[ecm_index]);
                        if (status == S_SUCCESS)
                            setup_pid_count++;
                        else
                            dataloginfo->datalogsignals[i].SignalType.Generic.PacketNumber = 0xFF; //mark this signal invalid
                    }
                }//for(i=...
            }//for(packetid=...
        }
        else if (vehiclecommlevel[ecm_index] == CommLevel_KWP2000)
        {
            obd2can_ford_requestdiagnosticdatapacket(ecm_id[ecm_index],0x0A,NULL,0,
                                                 CommLevel_KWP2000);    // This clears packets                                                     
            for(packetid=FORD_KWP_CAN_PACKETID_START;packetid<=FORD_KWP_CAN_PACKETID_END;packetid++)
            {
                for(i=0;i<dataloginfo->datalogsignalcount;i++)
                {
                    if (dataloginfo->datalogsignals[i].EcmIndex == ecm_index &&
                        dataloginfo->datalogsignals[i].SignalType.Generic.PacketNumber == packetid &&
                        (dataloginfo->datalogsignals[i].PidType == PidTypeRegular ||
                         dataloginfo->datalogsignals[i].PidType == PidTypeDMR))
                    {
                        status = obd2can_ford_dynamicallydefinediagnosticdatapacket
                            (ecm_id[ecm_index],packetid,
                             (PidType)dataloginfo->datalogsignals[i].PidType,
                             dataloginfo->datalogsignals[i].Size,
                             dataloginfo->datalogsignals[i].SignalType.Generic.Position,
                             dataloginfo->datalogsignals[i].Address,
                             vehiclecommlevel[ecm_index]);
                        if (status == S_SUCCESS)
                            setup_pid_count++;
                        else
                            dataloginfo->datalogsignals[i].SignalType.Generic.PacketNumber = 0xFF; //mark this signal invalid
                    }
                }//for(i=...
            }//for(packetid=...
        }
        else if (vehiclecommlevel[ecm_index] == CommLevel_Unknown)
        {
            break;
        }
        else
        {
            log_push_error_point(0x4985);
            return S_NOTSUPPORT;
        }
        if (setup_pid_count == 0)
            gDatalogmode.packetmode = SingleRate;
    }
    
    // If no pids setup on PCM or TCM, fail; firmware will treat this as single rate
    // since it is now assumed that there are SAE PIDs
    if (gDatalogmode.packetmode == SingleRate)
        return S_FAIL;
    
    // Make sure mixed mode works (some processors do not support this like the
    // 6.7L. In this case, if SAE PIDs are to be datalogged, force single rate.
    if (gDatalogmode.obdPids == TRUE)
    {
        for(i=0;i<ECM_MAX_COUNT;i++)
        {
            if(dataloginfo->packetidcount[i] == 0)
                continue;
            
            switch (vehiclecommlevel[i])
            {
                case CommLevel_ISO14229:
                    status = obd2can_ford_requestdiagnosticdatapacket_2A
                        (ecm_id[i], 0x03,
                         dataloginfo->packetidlist[i], dataloginfo->packetidcount[i],
                         CommLevel_ISO14229);
                    if (status != S_SUCCESS)
                    {
                        return S_FAIL;
                    }
                    delays(100, 'm');
                    obd2can_ping(ecm_id[i],FALSE);
                    delays(100, 'm');                    
                    timeout = rtc_getvalue();
                    while(rtc_getvalue() - timeout < 1000)
                    {
                        status = obd2can_rxraw(response_ecm_id, rxrawdata);
                        if (status == S_SUCCESS && response_ecm_id[0] >= 0x6A0 && response_ecm_id[0] <= 0x6A5)
                        {
                            break;
                        }                        
                        status = S_FAIL;
                    }
                    
                    obd2can_ford_requestdiagnosticdatapacket_2A(ecm_id[i],0x04,NULL,0,
                                                                CommLevel_ISO14229);
                    delays(100,'m');
                    if (status != S_SUCCESS)
                    {
                        return S_FAIL;
                    }
                    break;
                case CommLevel_KWP2000:
                    // Do nothing
                    break;
            }
        }
    }
    return S_SUCCESS;
}

//------------------------------------------------------------------------------
// Get datalog data (rapid and/or single rate)
// Input:   VehicleCommLevel commlevel[ECM_MAX_COUNT]
// Return:  u8  status
// Engineer: Quyen Leba, Tristen Pierson
//------------------------------------------------------------------------------
u8 obd2can_ford_datalog_getdata(VehicleCommLevel *vehiclecommlevel)
{
    static u8 index;                // Retains index count (do not remove static qualifier)        
    static u8 packetread;           // Retains consecutive packets read (do not remove static qualifier)
    static u8 expected_ecm_index;   // Retains expected ecm index
    obd2can_rxinfo rxinfo;
    u8  status;        
    u32 response_ecm_id;    
    u32 packet_filter[6] = {0x6A0, 0x6A1, 0x6A2, 0x6A3, 0x6A4, 0x6A5};
    u32 ecm_id[2] = {Obd2CanEcuId_7E0,Obd2CanEcuId_7E1};    
    u8  ecm_index;
    u32 tempValue;
    u8  pidData[4];
    u8  data[DATALOG_MAX_PID_SIZE]; // For OSC and Packet PIDs
    u8  datalength;                 // For Packet PIDs
    bool oscprocessed = FALSE;      // For OSC
    bool skip;                      // For Packet PIDs
    u8  i;
    
    // Need to detect ECM
    if (dataloginfo->control.ecm_stream)
    {
        if ((rtc_getvalue() - dataloginfo->control.ecm_laststreamdata) > DATALOG_MAX_DATAPOINT_INTERVAL)
        {
            return S_HALTED;
        }
    }
    else
    {
        packetread = 0;
    }

    // If only OSC session, update datapoint timestamp
    if (dataloginfo->oscsignalcount > 0 && dataloginfo->dmrsignalcount == 0 &&
        dataloginfo->pidsignalcount == 0)
    {
        dataloginfo->lastdatapoint_timestamp = rtc_getvalue();
    }
    
    status = S_SUCCESS;

    // Request non-packet data but only after reading a packet if available
    if ((packetread > 0 || dataloginfo->control.ecm_stream == 0) &&
        dataloginfo->pending == FALSE  && dataloginfo->osc_pending == FALSE)
    {    
        // Get next non-packet for request
        do
        {
            // Enforce signal loop bound
            if (++index >= dataloginfo->datalogsignalcount)
            {
                index = 0;
            }     
            // Only request non-packet data
            if (dataloginfo->datalogsignals[index].SignalType.Generic.PacketNumber == DATALOG_SKIPPED_PACKETID)
            {
                ecm_index = dataloginfo->datalogsignals[index].EcmIndex;
                if (ecm_index < 2)
                {
                     // Handle PIDs/DMRs
                    if (dataloginfo->datalogsignals[index].PidType == PidTypeRegular ||
                        dataloginfo->datalogsignals[index].PidType == PidTypeDMR ||
                        dataloginfo->datalogsignals[index].PidType == PidTypeMode1)
                    {
                        // Skip other bitmapped PIDs if covered by previous poll
                        if (dataloginfo->datalogsignals[index].Flags & DATALOG_PIDFLAGS_DATATYPE_BIT)
                        {
                            skip = FALSE;
                            for (i = 0; i < index; i++)
                            {
                                if (dataloginfo->datalogsignals[i].Flags & DATALOG_PIDFLAGS_DATATYPE_BIT &&
                                    dataloginfo->datalogsignals[i].Address == dataloginfo->datalogsignals[index].Address)
                                {
                                    skip = TRUE;
                                    break;
                                }
                            }
                            if (skip == TRUE)
                            {
                                continue;   // Next iteration
                            }
                        }
                        switch (dataloginfo->datalogsignals[index].PidType)
                        {
                            case PidTypeRegular:
                                // Handle packet PIDs - single rate only
                                if (dataloginfo->datalogsignals[index].Flags & DATALOG_PIDFLAGS_IS_PACKET_PID)
                                {
                                    // Skip other Packet PIDs if covered by previous Packet PID
                                    skip = FALSE;
                                    for (i = 0; i < index; i++)
                                    {
                                        if (dataloginfo->datalogsignals[i].Flags & DATALOG_PIDFLAGS_IS_PACKET_PID &&
                                            dataloginfo->datalogsignals[i].Address == dataloginfo->datalogsignals[index].Address)
                                        {
                                            skip = TRUE;
                                            break;
                                        }
                                    }
                                    if (skip == TRUE)
                                    {
                                        continue;   // Next iteration
                                    }
                                    
                                    status = obd2can_read_data_bypid(ecm_id[ecm_index],
                                                (u16)dataloginfo->datalogsignals[index].Address,
                                                &data[0], &datalength, TRUE);
                                    
                                    if (status != S_SUCCESS)
                                    {
                                        break;
                                    }
                                    
                                    // Parse all PIDs used in packet
                                    skip = TRUE;
                                    for (i = 0; i < dataloginfo->datalogsignalcount; i++)
                                    {
                                        if (dataloginfo->datalogsignals[i].Flags & DATALOG_PIDFLAGS_IS_PACKET_PID &&
                                            dataloginfo->datalogsignals[i].Address == dataloginfo->datalogsignals[index].Address)
                                        {
                                            obd2_parse_packet_pid(&data[0], (u8)datalength, 
                                                dataloginfo->datalogsignals[i].packetStartPos,
                                                &pidData[0],
                                                dataloginfo->datalogsignals[i].Size,
                                                (dataloginfo->datalogsignals[i].Flags & DATALOG_PIDFLAGS_IS_BITS ? 1 : 0));
                                            
                                            obd2datalog_parserdata(dataloginfo->datalogsignals[i].EcmIndex, 
                                                                   i, 0, &pidData[0]);
                                            
                                            skip = FALSE;
                                        }
                                    }
                                    if (skip == FALSE)
                                    {
                                        packetread = 0;
                                        dataloginfo->pending = FALSE;
                                        dataloginfo->lastsinglerate_timestamp = rtc_getvalue();
                                        dataloginfo->lastdatapoint_timestamp = rtc_getvalue();
                                        continue; // Next iteration
                                    }
                                }
                                else
                                {
                                    status = obd2can_read_data_bypid(ecm_id[ecm_index],
                                                (u16)dataloginfo->datalogsignals[index].Address,
                                                NULL, NULL, FALSE);
                                }
                                break;
                            case PidTypeDMR:
                                status = obd2can_ford_read_data_bydmr(ecm_id[ecm_index],
                                            dataloginfo->datalogsignals[index].Address,
                                            dataloginfo->datalogsignals[index].Size,
                                            NULL, NULL, FALSE, &vehiclecommlevel[ecm_index]);                    
                                break;
                            case PidTypeMode1:
                                // Handle packet OBD PIDs - single rate only
                                if (dataloginfo->datalogsignals[index].Flags & DATALOG_PIDFLAGS_IS_PACKET_PID)
                                {
                                    // Skip other Packet PIDs if covered by previous Packet PID
                                    skip = FALSE;
                                    for (i = 0; i < index; i++)
                                    {
                                        if (dataloginfo->datalogsignals[i].Flags & DATALOG_PIDFLAGS_IS_PACKET_PID &&
                                            dataloginfo->datalogsignals[i].Address == dataloginfo->datalogsignals[index].Address)
                                        {
                                            skip = TRUE;
                                            break;
                                        }
                                    }
                                    if (skip == TRUE)
                                    {
                                        continue;   // Next iteration
                                    }
                                    
                                    status = obd2can_read_data_bypid_mode1(
                                               gDatalogmode.ecmBroadcast,ecm_id[ecm_index],
                                               (u8)dataloginfo->datalogsignals[index].Address,
                                               &data[0], &datalength, TRUE);
                                    
                                    if (status != S_SUCCESS)
                                    {
                                        break;
                                    }
                                                                        
                                    // Parse all PIDs used in packet
                                    skip = TRUE;
                                    for (i = 0; i < dataloginfo->datalogsignalcount; i++)
                                    {
                                        if (dataloginfo->datalogsignals[i].Flags & DATALOG_PIDFLAGS_IS_PACKET_PID &&
                                            dataloginfo->datalogsignals[i].Address == dataloginfo->datalogsignals[index].Address)
                                        {
                                            obd2_parse_packet_pid(&data[0], (u8)datalength, 
                                                dataloginfo->datalogsignals[i].packetStartPos,
                                                &pidData[0], 
                                                dataloginfo->datalogsignals[i].Size,
                                                (dataloginfo->datalogsignals[i].Flags & DATALOG_PIDFLAGS_IS_BITS ? 1 : 0));
                                            
                                            obd2datalog_parserdata(dataloginfo->datalogsignals[i].EcmIndex, 
                                                                   i, 0, &pidData[0]);
                                            
                                            skip = FALSE;
                                        }
                                    }
                                    if (skip == FALSE)
                                    {
                                        packetread = 0;
                                        dataloginfo->pending = FALSE;
                                        dataloginfo->lastsinglerate_timestamp = rtc_getvalue();
                                        dataloginfo->lastdatapoint_timestamp = rtc_getvalue();
                                        continue; // Next iteration
                                    }
                                }
                                else
                                {
                                    status = obd2can_read_data_bypid_mode1(
                                               gDatalogmode.ecmBroadcast,ecm_id[ecm_index],
                                               (u8)dataloginfo->datalogsignals[index].Address,
                                               NULL, NULL, FALSE);
                                }
                                break;
                            default:
                                break;
                        }
                        if (status == S_SUCCESS)
                        {
                            expected_ecm_index = ecm_index;
                            dataloginfo->lastsinglerate_timestamp = rtc_getvalue();
                            dataloginfo->pending = TRUE;
                            break;
                        }
                    } // Handle PIDs/DMRs
                    
                    // Handle OSC
                    if (dataloginfo->datalogsignals[index].PidType == PidTypeOSC)
                    {
                        // If OSC CPID was changed, send command
                        if (dataloginfo->datalogsignals[index].StructType == StructTypeOSC &&
                            (DatalogControl_GetOSC(dataloginfo->datalogsignals[index].Control) == OSC_CmdSend ||
                             DatalogControl_GetOSC(dataloginfo->datalogsignals[index].Control) == OSC_CmdReset))
                        {
                            // Initialize data bytes to 0x00's
                            memset(data,0x00,sizeof(data));                            
                            oscprocessed = FALSE;
                            // Handle non-bitmapped data
                            if (!(dataloginfo->datalogsignals[i].Flags & DATALOG_PIDFLAGS_DATATYPE_BIT) &&
                                DatalogControl_GetOSC(dataloginfo->datalogsignals[index].Control) == OSC_CmdSend)
                            {
                                tempValue = (u32)obd2datalog_composedata(index, dataloginfo->datalogsignals[index].Data);
                                memcpy(data,&tempValue,dataloginfo->datalogsignals[index].Size);
                                oscprocessed = TRUE;
                                
                                if (dataloginfo->datalogsignals[index].SignalType.OSC.UiPidGroup != 0)
                                {
                                    for (i = 0; i < dataloginfo->datalogsignalcount; i++)
                                    {
                                        // If UI Group, unset other display values
                                        if (index != i &&
                                            ((dataloginfo->datalogsignals[index].SignalType.OSC.UiPidGroup != 0 &&
                                            dataloginfo->datalogsignals[index].SignalType.OSC.UiPidGroup ==
                                            dataloginfo->datalogsignals[i].SignalType.OSC.UiPidGroup)))
                                        {
                                            dataloginfo->datalogsignals[i].Value = OSC_Default;
                                            dataloginfo->datalogsignals[i].Control |= DATALOG_CONTROL_NEWDATA_SET;
                                        }
                                    }
                                }
                            }
                            // Handle bitmapped data
                            else if (dataloginfo->dlxblocks[index].pidBitMapped == TRUE ||
                                     DatalogControl_GetOSC(dataloginfo->datalogsignals[index].Control) == OSC_CmdReset)
                            {
                                // Parse and mask all bitmapped data within CPID
                                for (i = 0; i < dataloginfo->datalogsignalcount; i++)
                                {
                                    // Check for items belonging CPID
                                    if (dataloginfo->datalogsignals[i].Address == dataloginfo->datalogsignals[index].Address)
                                    {
                                        // If UI Group, unset other items
                                        if (index != i &&
                                            ((dataloginfo->datalogsignals[index].SignalType.OSC.UiPidGroup != 0 &&
                                            dataloginfo->datalogsignals[index].SignalType.OSC.UiPidGroup ==
                                            dataloginfo->datalogsignals[i].SignalType.OSC.UiPidGroup)))
                                        {
                                            DatalogControl_SetOSC(dataloginfo->datalogsignals[i].Control,OSC_CmdReset);
                                            dataloginfo->datalogsignals[i].Value = OSC_CmdReset;
                                            dataloginfo->datalogsignals[i].Control |= DATALOG_CONTROL_NEWDATA_SET;
                                        }
                                        // Mask active items
                                        if (DatalogControl_GetOSC(dataloginfo->datalogsignals[i].Control) == OSC_CmdSend ||
                                            DatalogControl_GetOSC(dataloginfo->datalogsignals[i].Control) == OSC_Pending ||
                                            DatalogControl_GetOSC(dataloginfo->datalogsignals[i].Control) == OSC_Success)
                                        {
                                            // Skip item if control byte position is invalid
                                            if (dataloginfo->datalogsignals[i].SignalType.OSC.ControlBytePosition < 1)
                                            {
                                                DatalogControl_SetOSC(dataloginfo->datalogsignals[i].Control,OSC_Fail);
                                                dataloginfo->datalogsignals[i].Value = OSC_Fail;
                                                dataloginfo->datalogsignals[i].Control |= DATALOG_CONTROL_NEWDATA_SET;
                                                continue;
                                            }
                                            
                                            // Mask control byte into position
                                            data[dataloginfo->datalogsignals[i].SignalType.OSC.ControlBytePosition-1] |=
                                                dataloginfo->datalogsignals[i].SignalType.OSC.ControlByteMask;
                                            // If data available, mask data byte to position
                                            if (dataloginfo->datalogsignals[i].Size > 0 &&
                                                dataloginfo->datalogsignals[i].SignalType.OSC.DataBytePosition > 0)
                                            {
                                                //Compose data                                            
                                                data[dataloginfo->datalogsignals[i].SignalType.OSC.DataBytePosition-1] |=
                                                    (u8)obd2datalog_composedata(i, dataloginfo->datalogsignals[i].Data);
                                            }
                                        }
                                        else if (DatalogControl_GetOSC(dataloginfo->datalogsignals[i].Control) == OSC_CmdReset)
                                        {
                                            DatalogControl_SetOSC(dataloginfo->datalogsignals[i].Control,OSC_Default);
                                            dataloginfo->datalogsignals[i].Value = OSC_Default;
                                            dataloginfo->datalogsignals[i].Control |= DATALOG_CONTROL_NEWDATA_SET;
                                        }
                                        oscprocessed = TRUE;
                                    }
                                } // for (i = 0...)
                            }
                            
                            if (oscprocessed == TRUE)
                            {
                                // OSC_CmdSend
                                if (DatalogControl_GetOSC(dataloginfo->datalogsignals[index].Control) == OSC_CmdSend)
                                {
                                    // Short Term Adjustment
                                    status = obd2can_ford_input_output_control(
                                        ecm_id[ecm_index],
                                        (u16)dataloginfo->datalogsignals[index].Address,
                                        0x03, data,
                                        (u8)dataloginfo->datalogsignals[index].Size,
                                        *vehiclecommlevel,FALSE);
                                }
                                // OSC_CmdReset
                                else
                                {
                                    // Return Control to ECU
                                    status = obd2can_ford_input_output_control(
                                        ecm_id[ecm_index],
                                        (u16)dataloginfo->datalogsignals[index].Address,
                                        0x00, data, 0, *vehiclecommlevel,FALSE);
                                }
                                if (status != S_SUCCESS)
                                {
                                    DatalogControl_SetOSC(dataloginfo->datalogsignals[index].Control,OSC_Fail);
                                    dataloginfo->datalogsignals[index].Value = OSC_Fail;
                                }
                                else
                                {
                                    switch (DatalogControl_GetOSC(dataloginfo->datalogsignals[index].Control))
                                    {                                    
                                        case OSC_CmdReset:
                                        case OSC_Default:
                                            dataloginfo->datalogsignals[index].Value = OSC_Default;                                          
                                            break;
                                        default:
                                            dataloginfo->datalogsignals[index].Value = OSC_Pending;
                                            break;
                                    }
                                    expected_ecm_index = ecm_index;
                                    DatalogControl_SetOSC(dataloginfo->datalogsignals[index].Control,OSC_Pending);
                                    dataloginfo->lastosc_timestamp = rtc_getvalue();
                                    dataloginfo->lastsinglerate_timestamp = rtc_getvalue();
                                    dataloginfo->osc_pending = TRUE;
                                }
                                dataloginfo->datalogsignals[index].Control |= DATALOG_CONTROL_NEWDATA_SET;
                                break;
                            } // if (oscprocessed == TRUE)
                        } // If OSC CPID was changed, send command
                    } // Handle OSC
                } // if (ecm_index < 2)
            } // Request non-packet data
        } while (index > 0); // do
    } // Request non-packet data   

    // Check for data
    status = S_FAIL;
    if (gDatalogmode.packetmode == RapidRate || dataloginfo->pending || dataloginfo->osc_pending)
    {
        obd2can_rxinfo_init(&rxinfo);
        status = obd2can_rxraw_complete(&response_ecm_id,&rxinfo,packet_filter,sizeof(packet_filter));
    }
 
    // Process data
    if (status == S_SUCCESS)
    {        
        switch (response_ecm_id)
        {
        case 0x6A0:     // Handle PCM Packets
        case 0x6A1:
        case 0x6A2:
        case 0x6A3:
            if (vehiclecommlevel[0] == CommLevel_ISO14229)
            {
                obd2datalog_parserapidpacketdata(0, (FORD_UDS_CAN_PACKETID_START+rxinfo.rxbuffer[0]), &rxinfo.rxbuffer[0]);
            }
            else
            {
                obd2datalog_parserapidpacketdata(0, rxinfo.rxbuffer[2], &rxinfo.rxbuffer[2]);
            }
            packetread = 1;
            dataloginfo->control.ecm_stream = 1;
            dataloginfo->control.ecm_laststreamdata = rtc_getvalue();
            dataloginfo->lastdatapoint_timestamp = rtc_getvalue();
            break;
        case 0x6A4:     // Handle TCM Packets
        case 0x6A5:
            if (vehiclecommlevel[1] == CommLevel_ISO14229)
            {
                obd2datalog_parserapidpacketdata(1, (FORD_UDS_CAN_PACKETID_START+rxinfo.rxbuffer[0]), &rxinfo.rxbuffer[0]);
            }
            else
            {
                obd2datalog_parserapidpacketdata(1, rxinfo.rxbuffer[2], &rxinfo.rxbuffer[2]);
            }
            packetread = 1;
            dataloginfo->control.tcm_stream = 1;
            dataloginfo->control.tcm_laststreamdata = rtc_getvalue();
            dataloginfo->lastdatapoint_timestamp = rtc_getvalue();
            break;
        case 0x7E9:     // Handle TCM Non-packets
        case 0x7E8:     // Handle PCM Non-packets
            ecm_index = 0;
            if (response_ecm_id == 0x7E9)
            {
                ecm_index = 1;
            }
            // Only accept expected ecm addresses
            if (ecm_index != expected_ecm_index)
            {
                break;
            }
            memset(&pidData, 0, sizeof(pidData));
            // Process non-packet data
            switch (rxinfo.cmd)
            {
            case 0x01:  // Handle SAE PID
                if (rxinfo.rxlength > 1)
                {
                    memcpy(&pidData,&rxinfo.rxbuffer[1],rxinfo.rxlength-1);
                }
                break;
            case 0x22:  // Handle OEM PID
                if (rxinfo.rxlength > 2)
                {
                    memcpy(&pidData,&rxinfo.rxbuffer[2],rxinfo.rxlength-2);
                }
                break;
            case 0x23:  // Handle DMR
                if (rxinfo.rxlength > 0)
                {
                    memcpy(&pidData,&rxinfo.rxbuffer[0],rxinfo.rxlength);
                }
                break;
            case 0x2F:  // Handle OSC Response
                // Do nothing here
                break;
            default:
                status = S_UNMATCH; 
                break;
            }                    
            // Process CAN Data
            if (status == S_SUCCESS)
            {
                switch (rxinfo.cmd)
                {
                case 0x01:  // Process SAE PID
                case 0x22:  // Process OEM PID
                case 0x23:  // Process DMR
                    // Parse all bitmapped data within PID
                    if (dataloginfo->datalogsignals[index].Flags & DATALOG_PIDFLAGS_DATATYPE_BIT)
                    {
                        for (i = 0; i < dataloginfo->datalogsignalcount; i++)
                        {
                            if (dataloginfo->datalogsignals[i].Flags & DATALOG_PIDFLAGS_DATATYPE_BIT &&
                                dataloginfo->datalogsignals[i].Address == dataloginfo->datalogsignals[index].Address)
                            {                                
                                obd2datalog_parserdata(dataloginfo->datalogsignals[i].EcmIndex, 
                                                       i, 0, &pidData[0]);
                            }
                        }
                    }
                    // Or parse single PID data
                    else
                    {
                        obd2datalog_parserdata(ecm_index, index, 0, pidData);
                    }
                    packetread = 0;
                    dataloginfo->pending = FALSE;
                    dataloginfo->lastsinglerate_timestamp = rtc_getvalue();
                    dataloginfo->lastdatapoint_timestamp = rtc_getvalue();
                    break;
                case 0x2F:  // Process OSC Response
                    // OSC_Default status holds true
                    if (dataloginfo->datalogsignals[index].Value != OSC_Default)
                    {
                        DatalogControl_SetOSC(dataloginfo->datalogsignals[index].Control,OSC_Success);
                        dataloginfo->datalogsignals[index].Value = OSC_Success;
                    }
                    packetread = 0;
                    dataloginfo->lastosc_timestamp = rtc_getvalue();
                    dataloginfo->osc_pending = FALSE;
                    dataloginfo->datalogsignals[index].Control |= DATALOG_CONTROL_NEWDATA_SET;
                    break;
                }
            }          
            break;
        default: // No expected data available
            break;
        } // switch(..)
    } // if (status == S_SUCCESS)
    // Process Negative Responses
    else if (status == S_ERROR)
    {
        switch (rxinfo.cmd)
        {
        case 0x2F:  // Process OSC Negative Response
            // Device Control Limits Exceeded            
            if (rxinfo.errorcode == 0x22 || rxinfo.errorcode == 0x31)
            {
                dataloginfo->datalogsignals[index].Value = OSC_RangeErr;
                DatalogControl_SetOSC(dataloginfo->datalogsignals[index].Control,OSC_RangeErr);
            }
            // Security access denied (should not happen but if it does, comm was
            // probably lost for some undetected period.
            else if (rxinfo.errorcode == 0x33)
            {
                return S_COMMLOST;
            }
            // Anything else
            else
            {
                dataloginfo->datalogsignals[index].Value = OSC_Fail;
                DatalogControl_SetOSC(dataloginfo->datalogsignals[index].Control,OSC_Fail);
            }
            packetread = 0;
            dataloginfo->osc_pending = FALSE;
            dataloginfo->lastosc_timestamp = rtc_getvalue();
            dataloginfo->datalogsignals[index].Control |= DATALOG_CONTROL_NEWDATA_SET;
            break;
        default:
            // Other negative responses are not handled
            break;
        }
    }
    
    // Check for single rate response timeout
    if (dataloginfo->pending == TRUE && 
        rtc_getvalue() - dataloginfo->lastsinglerate_timestamp >= DATALOG_SINGLERATE_TIMEOUT)
    {
        // Assume single rate PID response is lost
        dataloginfo->pending = FALSE;
    }
    
    // Check for OSC response timeout
    if (dataloginfo->osc_pending == TRUE && 
        rtc_getvalue() - dataloginfo->lastosc_timestamp >= DATALOG_OSC_TIMEOUT)
    {
        // Assume OSC PID response is lost
        dataloginfo->osc_pending = FALSE;
        dataloginfo->datalogsignals[index].Value = OSC_Timeout;
        DatalogControl_SetOSC(dataloginfo->datalogsignals[index].Control,OSC_Timeout);
        dataloginfo->datalogsignals[index].Control |= DATALOG_CONTROL_NEWDATA_SET;
    }
    
    // Data not received, session is probably dead
    if (rtc_getvalue() - dataloginfo->lastdatapoint_timestamp >= DATALOG_MAX_DATAPOINT_INTERVAL)
    {
        return S_COMMLOST;
    }
    
    return S_SUCCESS;
}
