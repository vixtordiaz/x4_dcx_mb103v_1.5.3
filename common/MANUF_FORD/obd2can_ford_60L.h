/**
  **************** (C) COPYRIGHT 2011 SCT Performance, LLC *********************
  * File Name          : obd2can_ford_60L.h
  * Device / System    : SCT products using the "CommonCode" platform
  * Author             : Quyen Leba
  *
  * Version            : 1 
  * Date               : 05/02/2011
  * Description        : Specific for 6.0L
  *                    : 
  *
  *
  * History            : 05/02/2011 M. Davis
  *                    :   Add SCT Copyright header,
  *
  ******************************************************************************
  */

#ifndef __OBD2CAN_FORD_60L_H
#define __OBD2CAN_FORD_60L_H

#include <common/obd2can.h>

u8 obd2can_ford_60L_get_tire_rev_per_mile_front(u32 *rev_per_mile);
u8 obd2can_ford_60L_set_tire_rev_per_mile_front(u32 rev_per_mile);
u8 obd2can_ford_60L_get_tire_rev_per_mile_rear(u32 *rev_per_mile);
u8 obd2can_ford_60L_set_tire_rev_per_mile_rear(u32 rev_per_mile);
u8 obd2can_ford_60L_get_tire_size(u32 *front_rev_per_mile, u32 *rear_rev_per_mile);
u8 obd2can_ford_60L_set_tire_size(u32 front_rev_per_mile, u32 rear_rev_per_mile);
u8 obd2can_ford_60L_get_vehicle_info(u8 *info_text);

#endif    //__OBD2CAN_FORD_60L_H
