/**
  **************** (C) COPYRIGHT 2011 SCT Performance, LLC *********************
  * File Name          : ford_ecm_defs.h
  * Device / System    : SCT products using the "CommonCode" platform
  * Author             : Quyen Leba
  *
  * Version            : 1 
  * Date               : 05/02/2011
  * Description        : 
  *                    : 
  *
  *
  * History            : 05/02/2011 M. Davis
  *                    :   Add SCT Copyright header,
  *
  ******************************************************************************
  */

#ifndef __FORD_ECM_DEFS_H
#define __FORD_ECM_DEFS_H

#include <arch/gentype.h>
#include <common/obd2.h>
#include <common/ecm_defs.h>

#define MAX_UPLOAD_BUFFER_LENGTH        0x1000
#define MAX_DOWNLOAD_BUFFER_LENGTH      0x1000

#define UPLOAD_CMD_MEMORY_SIZE          MemorySize_2_Byte
#define DEFAULT_CMD_MEMORY_SIZE         MemorySize_2_Byte

enum
{
    UnlockId_None               = 0,
    UnlockId_Regular            = 1,
    UnlockId_64L                = 2,
    UnlockId_67L                = 3,
    UnlockId_Bosch6R            = 4,
    UnlockId_Conti              = 5,
    UnlockId_Siemens_UTCU       = 6,
    UnlockId_ME9_UK             = 7,
    UnlockId_OSC                = 8,
    UnlockId_Diag               = 9,
};

enum
{
    ChecksumId_None             = 0x0000,
    ChecksumId_64L              = 0x0001,
    ChecksumId_6R               = 0x0002,
    ChecksumId_AU_ZF            = 0x0003,
    ChecksumId_67L              = 0x0004,
    ChecksumId_BoschTricore     = 0x0005,
    ChecksumId_Conti            = 0x0006,
    ChecksumId_67L_2013UP       = 0x0007,
    ChecksumId_ME9_UK           = 0x0008,
};

enum
{
    FinalId_None                  = 0x0000,
    FinalId_SendControlRoutine31  = 0x0001,
};

#define ECM_DEF_COUNT           51
extern const ECM_Def ford_ecm_defs[ECM_DEF_COUNT];

//------------------------------------------------------------------------------
// et:  ecm_type
//------------------------------------------------------------------------------
#define ecmvar(et,var)                      ford_ecm_defs[et].##var

#define VEH_FORD_Is60L_PCM(et)              ((et==5)  || (et==15))
#define VEH_FORD_Is67L_PCM(et)              ((et==35) || (et==47))

#endif    //__FORD_ECM_DEFS_H
