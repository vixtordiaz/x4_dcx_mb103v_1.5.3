/**
  **************** (C) COPYRIGHT 2011 SCT Performance, LLC *********************
  * File Name          : obd2tune_ford_checksum_continental.c
  * Device / System    : SCT products using the "CommonCode" platform
  * Author             : Patrick Downs, Quyen Leba
  *
  * Version            : 1 
  * Date               : 08/15/2012
  * Description        : Checksum functions for Continental PCMs
  *                      
  *
  *
  *
  ******************************************************************************
  */

#ifndef __OBD2TUNE_FORD_CHECKSUM_CONTINENTAL_H
#define __OBD2TUNE_FORD_CHECKSUM_CONTINENTAL_H

u32 Continental_Checksum(u8 ecm_type, u32 offset, bool *isstockcrc);

#endif    //__OBD2TUNE_FORD_CHECKSUM_CONTINENTAL_H