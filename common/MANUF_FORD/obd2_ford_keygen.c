/**
  **************** (C) COPYRIGHT 2011 SCT Performance, LLC *********************
  * File Name          : obd2_ford_keygen.c
  * Device / System    : SCT products using the "CommonCode" platform
  * Author             : Quyen Leba
  *
  * Version            : 1 
  * Date               : 05/02/2011
  * Description        : 
  *                    : 
  *
  *
  * History            : 05/02/2011 M. Davis
  *                    :   Add SCT Copyright header,
  *
  ******************************************************************************
  */

#include <common/statuscode.h>
#include <common/ecm_defs.h>
#include "obd2_ford_keygen.h"

// local prototypes
u32 generateKey(u32 _seed, u32 polynomial, u32 security, u8 type);
u32 getBitStatus(u32, u32);
u32 makeKey(u32, u32, u32);
void testSeedKeys(void);

u8 obd2_ford_keygen(u32 _seed, u32 unlockid, u32 *key)
{
    switch(unlockid)
    {
    case UnlockId_None:
        break;
    case UnlockId_Regular:
        *key = generateKey(_seed, 0xC541A9, 0xC5A46130, 0x08);
        break;
    case UnlockId_64L:
        *key = generateKey(_seed, 0xC541A9, 0xE19229F7, 0xAC);
        break;
    case UnlockId_67L:
        *key = generateKey(_seed, 0xC541A9, 0x9219E9C2, 0xA7);
        break;
    case UnlockId_Bosch6R:
        *key = generateKey(_seed, 0xC541A9, 0xAA556130, 0x08);
        break;
    case UnlockId_Siemens_UTCU:
        *key = generateKey(_seed, 0xC541A9, 0x4E414952, 0x41);
        break;
    case UnlockId_ME9_UK:
        *key = generateKey(_seed, 0xC541A9, 0x354A513B, 0x5A);
        break;
    case UnlockId_OSC:
        *key = generateKey(_seed, 0xC541A9, 0x45444F49, 0x44);
        break;
    case UnlockId_Diag:
        *key = generateKey(_seed, 0xC541A9, 0x42231615, 0x48);
        break;
    case UnlockId_Conti:
        *key = generateKey(_seed, 0xC541A9, 0x9214C0B2, 0xA3);
        break;
    default:
        return S_INPUT;
    }
    
    return S_SUCCESS;
}

///////////////////////////////////////////////////////////////////////
//
// entry point to generate a key
// entry conditions:
//      _seed is the seed key as sniffed 07 E8 67 01 aa bb cc; _seed=aabbcc
//      polynomial used in the algo     0xC541A9
//      security varies between PWM's  
//                                      Ford PowerTrain=0xC5A46130
//                                      Mazda-GDS=0x41647A61
//                                      Mazda-GGDS-PCM=0x52656676
//                                      Mazda-GGDS-PCM=0x50A3999C
//                                      ZF-Security=0xF122C303
//                                      AW-TCM(HEV)=0xF7D69362
//                                      AW-TCM(D219/58)=0x4E495349
//                                      Teves-HEV BSCM=0x9A510755
//                                      FCU-Ballard=0x9A510755
//                                      Siemens-UTCU=0x4E414952
//                                      Siemens-6.4L (A)=0xAA556130
//                                      Siemens-6.4L (B)=0xE19229F7
//                                      Bosch-6R=0xAA556130
//                                      Bosch-Diesel (A)=0x9219E9C2
//                                      Conti-FoE-(A)=0x9214C0B2 
//                                      NTK-NOX - (A)=0x7A2CE9C2
//                                      BECM (HEV)=0x57BC3914
//                                      BPCM (HEV)=0x9D87CA05
//                                      DCDC (HEV)=0x13635349
//      type varies between PWM's                                                       
//                                      Ford PowerTrain=0x08
//                                      Mazda-GDS=0x4D
//                                      Mazda-GGDS-PCM=0x49
//                                      Mazda-GGDS-PCM=0x5F
//                                      ZF-Security=0x7B
//                                      AW-TCM(HEV)=0x3A
//                                      AW-TCM(D219/58)=0x41
//                                      Teves-HEV BSCM=0x83
//                                      FCU-Ballard=0x83
//                                      Siemens-UTCU=0x41
//                                      Siemens-6.4L (A)=0x08
//                                      Siemens-6.4L (B)=0xAC
//                                      Bosch-6R=0x08
//                                      Bosch-Diesel (A)=0xA7
//                                      Conti-FoE-(A)=0xA3
//                                      NTK-NOX - (A)=0xA7
//                                      BECM (HEV)=0xA1
//                                      BPCM (HEV)=0x28
//                                      DCDC (HEV)=0x73
//
//////////////////////////////////////////////////////////////////////
u32 generateKey(u32 _seed, u32 polynomial, u32 security, u8 type)
{
u32 poly;
u16 key1, key2, key3;
u32 cnt=0;
u32 seed, temp1, temp2, temp3, temp4, temp5;

// assume _seed is as sniffed ie. 00 00 07 E8 67 01 aa bb cc
// aa bb cc is the seed
// this algo needs the format A3 cc bb aa
poly=polynomial;
seed=(_seed>>16)&0xFF;
seed+=_seed&0xFF00;
seed+=(_seed&0xFF)<<16;
seed|=((u32)type<<24);

// key generator loops 64 times
while (cnt<64)
  {
  if (cnt<32)
    {
    temp1=getBitStatus(cnt, seed);        // use the seed for the first 32
    }
  else
    {
    temp1=getBitStatus(cnt-32, security); // use security for the remaining 32
    }
  
  temp2=getBitStatus(0, poly);
  poly>>=1;
  temp3=temp1^temp2;
  
  poly=makeKey(temp3, 0x17, poly);
  temp4=getBitStatus(0x14, poly);
  temp5=temp3 ^  temp4;                    

  poly=makeKey(temp5,  0x14, poly);
  temp4=getBitStatus(0x0F, poly);
  temp5=temp3 ^  temp4;    

  poly=makeKey(temp5,  0x0F, poly);
  temp4=getBitStatus(0x0C, poly);            
  temp5=temp3 ^  temp4;

  poly=makeKey(temp5,  0x0C, poly);
  temp4=getBitStatus(0x05, poly);            
  temp5=temp3 ^  temp4;    

  poly=makeKey(temp5,  0x05, poly);
  temp4=getBitStatus(0x03, poly);            
  temp5=temp3 ^  temp4;

  poly=makeKey(temp5,  0x03, poly);    
  
  cnt+=1;
  }

// final changes to generate the key
// from poly
key1=(poly>>4)&0xFF;
key2=( (poly>>20)&0x0F )+( (poly>>8)&0xF0 );
key3=( (poly>>16)&0x0F )+( (poly<<4)&0xF0 );

// lets return in _seed the key
// key format is as sniffed ie. 00 00 07 E8 67 01 aa bb cc
// where _seed = 00aabbcc
return(((u32)key3 << 16) + ((u32)key2 << 8) + (u32)key1);
}
////////////////////////////////////////////////////////////////////////
//
// returns '1' if bit digit of object is '1'
// returns '0' if bit digit of object is '0'
//
////////////////////////////////////////////////////////////////////////
u32 getBitStatus(u32 digit, u32 object)
{
u32 temp;

temp=1;
temp<<=digit;
if ((temp&object)!=0)  
  return(1);
else
  return(0);
}
////////////////////////////////////////////////////////////////////////
//
// if bitValue=0, than bit digit is cleared in object
// if bitValue=1, than bit digit is set in object
//
////////////////////////////////////////////////////////////////////////
u32 makeKey(u32 bitValue, u32 digit, u32 object)
{
u32 temp;

temp=1;
temp<<=digit;
if (bitValue==0)
  {
  temp=~temp;
  return(temp&object);
  }
else
  return(temp|object);
}

//void testSeedKeys()
//{
//generateKey(0xBA3A92, 0xC541A9, 0x9214C0B2, 0xA0);  // 0x4172f2
//generateKey(0xc09408, 0xC541A9, 0x9214C0B2, 0xA0);  // 0x82f3a6
//generateKey(0x1ee61e, 0xC541A9, 0x9214C0B2, 0xA0);  // 0xe8c012
//generateKey(0xa8568f, 0xC541A9, 0x9214C0B2, 0xA0);  // 0x4badc9
//generateKey(0x41cc84, 0xC541A9, 0x9214C0B2, 0xA0);  // 0x708cab
//generateKey(0x36a7f3, 0xC541A9, 0x9214C0B2, 0xA0);  // 0xc9180a
//generateKey(0xb076de, 0xC541A9, 0x9214C0B2, 0xA0);  // 0x913978
//generateKey(0x26ae07, 0xC541A9, 0x9214C0B2, 0xA0);  // 0x73fba3
//generateKey(0xd20f02, 0xC541A9, 0x9214C0B2, 0xA0);  // 0x450058
//generateKey(0x7f9b45, 0xC541A9, 0x9214C0B2, 0xA0);  // 0x6cf2b8
//generateKey(0xc7e3ef, 0xC541A9, 0x9214C0B2, 0xA0);  // 0xd32c06
//}
