/**
  **************** (C) COPYRIGHT 2011 SCT Performance, LLC *********************
  * File Name          : obd2tune_ford_checksum.c
  * Device / System    : SCT products using the "CommonCode" platform
  * Author             : Quyen Leba
  *
  * Version            : 1 
  * Date               : 05/02/2011
  * Description        : 
  *                    : 
  *
  *
  * History            : 05/02/2011 M. Davis
  *                    :   Add SCT Copyright header,
  *
  ******************************************************************************
  */

//------------------------------------------------------------------------------
// Log error point: 0x44xx -> 0x46xx (shared with obd2tune.c)
//------------------------------------------------------------------------------

#include <string.h>
#include <common/genmanuf_overload.h>
#include <common/statuscode.h>
#include <common/obd2tune.h>
#include <common/filestock.h>
#include <common/checksum_ipc.h>
#include <common/MANUF_FORD/obd2tune_ford_checksum_6R_ZF.h>
#include <common/MANUF_FORD/obd2tune_ford_checksum_boschtricore.h>
#include <common/MANUF_FORD/obd2tune_ford_checksum_continental.h>
#include <vm_engine/ccvm/source/ccvm.h>

extern obd2_info gObd2info;             /* from obd2.c */
extern flasher_info *flasherinfo;       /* from obd2tune.c */

//------------------------------------------------------------------------------
// Private functions
//------------------------------------------------------------------------------
u8 obd2tune_ford_checksum_64L(u16 ecm_type, u32 offset);


//------------------------------------------------------------------------------
// IPC Checksum filename for preloaded tunes
// Input:   u16 veh_type
//          selected_tunelist_info *tuneinfo
// Return:  u8  status
// Engineer: Patrick Downs
//------------------------------------------------------------------------------
u8 obd2tune_ford_preloadedcsumfilename(u8 *tunefilename, u8 *checksumfilename)
{
    u8  ipc_filename[128];
    u32 status;
    u8  *bptr;
    
    status = S_SUCCESS;
    
    //get ipc checksum filename from tunefilename
    //for example: FPDL3_50MS.stg -> FPDL3.ipc
    //or FPDL3.stg -> FPDL3.ipc
    if ((strlen((char*)tunefilename)+strlen(GENFS_DEFAULT_FOLDER_PATH)) >= sizeof(ipc_filename))
    {
        log_push_error_point(0x4550);
        status = S_BADCONTENT;
        goto preloadedcsumfilename_done;
    }
    strcpy((char*)ipc_filename,(char*)tunefilename);
    bptr = (u8*)strstr((char*)ipc_filename,"_");
    if (bptr == NULL)
    {
        bptr = (u8*)strstr((char*)ipc_filename,".");
    }
    if (bptr)
    {
        if (strlen((char*)bptr) >= 4)
        {
            strcpy((char*)bptr,".ipc");
        }
        else
        {
            log_push_error_point(0x4551);
            status = S_BADCONTENT;
        }
    }
    else
    {
        log_push_error_point(0x453A);
        status = S_BADCONTENT;
    }
    
preloadedcsumfilename_done:    
    //All preloaded checksum files must be in default folder
    if(status == S_SUCCESS)
    {
        if(strstr((const char*)ipc_filename, GENFS_DEFAULT_FOLDER_PATH) == 0)
        {
            strcpy((char*)checksumfilename,GENFS_DEFAULT_FOLDER_PATH);
            strcat((char*)checksumfilename,(char*)ipc_filename);
        }
        else
            strcpy((char*)checksumfilename,(char*)ipc_filename);
    }
    else
    {
        memset((char*)checksumfilename, 0, MAX_TUNE_NAME_LENGTH);
    }
    
    return status;
}

//------------------------------------------------------------------------------
// Apply checksum
// Input:   u16 veh_type
//          selected_tunelist_info *tuneinfo
// Return:  u8  status
// Engineer: Quyen Leba
//------------------------------------------------------------------------------
u8 obd2tune_ford_apply_checksum(u16 veh_type, selected_tunelist_info *tuneinfo)
{
    u16 ecm_type;
    u32 ecm_size;
    u32 offset;     //the beginning of an ECM in flash file
    u8  i;
    u8  status;  
    bool isstockcrc = FALSE; 
    
    status = filestock_openflashfile("r+");
    if (status != S_SUCCESS)
    {
        log_push_error_point(0x4552);
        goto obd2tune_ford_apply_checksum_done;
    }
    
    status = S_FAIL;
    offset = 0;
    for(i=0;i<ECM_MAX_COUNT;i++)
    {
        ecm_type = VEH_GetEcmType(veh_type,i);
        if (ecm_type == INVALID_ECM_DEF)
        {
            //terminated
            break;
        }
        
        // Handle the IPC Checksums
        if(strlen((char*)tuneinfo->checksumfilename[i]) != 0)
        {
            //abandon error code: 0x453C
            status = checksum_ipc_apply_to_tunefile(filestock_getflashfile_fptr(),
                                                    tuneinfo->checksumfilename[i],
                                                    ecm_type,offset);
            if (status != S_SUCCESS && status != S_FILENOTFOUND)
            {
                log_push_error_point(0x453B);
                status = S_FAIL;
                goto obd2tune_ford_apply_checksum_done;
            }
        }
        
        // Processor Specific Checksums
        switch(ECM_GetChecksumId(ecm_type))
        {
        case ChecksumId_None:
            status = S_SUCCESS;
            break;
        case ChecksumId_64L:
            status = obd2tune_ford_checksum_64L(ecm_type,offset);
            break;
        case ChecksumId_6R:
            status = Bosch_6R_checksum(ecm_type, offset);
            break;
        case ChecksumId_AU_ZF:
            status = Bosch_AU_ZF_checksum(ecm_type, offset);
            break;
        case ChecksumId_67L:
            filestock_closeflashfile();
            status = Ford_67L_ModifyForSBLAndChecksum();
            if(filestock_openflashfile("r+")!= S_SUCCESS)
            {
                log_push_error_point(0x4553);
                goto obd2tune_ford_apply_checksum_done;
            }
            break;
        case ChecksumId_67L_2013UP:
            status = Ford_Calculate67L_Checksums(ChecksumId_67L_2013UP);
            break;
        case ChecksumId_BoschTricore:
            status = Bosch_Tricore_Checksum(ecm_type, offset);
            break;
        case ChecksumId_Conti:
            status = Continental_Checksum(ecm_type, offset, &isstockcrc);
            flasherinfo->isstockcrc = isstockcrc; 
            break;
        case ChecksumId_ME9_UK:
            {
                int32_t ret;
                int32_t argv[1];
                uint8_t argc = 1;
                
				if (gObd2info.ecu_block[0].ecutype != EcuType_Unknown)
                {
                    /* Get box code from strategy */
                    if (strcmp((char*)gObd2info.ecu_block[0].partnumbers.ford.strategy,"378731") == 0)
                    {
                        argv[0] = 37;                                       
                    }
                    else if (strcmp((char*)gObd2info.ecu_block[0].partnumbers.ford.strategy,"382957") == 0)
                    {
                        argv[0] = 38;
                    }
                    else if (strcmp((char*)gObd2info.ecu_block[0].partnumbers.ford.strategy,"393284") == 0)
                    {
                        argv[0] = 39;
                    }
                    else
                    {
                        argv[0] = 0;
                    }
                    
                    /* argv[0] = box code:
                     * ME9 AE = 37, ME9 AF = 38, ME9 AG = 39
                     */
                    status = vm_execute("\\STRATEGY\\CHECKSUM_ME9_UK.vmb",argc,argv,&ret);
                    if (status != S_VM_DONE)
                    {
                        log_push_error_point(0x454F);
                        status = S_FAIL;
                    }
                    else if (ret != S_SUCCESS)
                    {
                        log_push_error_point(0x4554);
                        status = S_FAIL;
                    }
                    else
                    {
                        status = S_SUCCESS;
                    }
                }
                else
                {
                    log_push_error_point(0x4565);
                    status = S_FAIL;
                }
            }
            break;
        default:
            log_push_error_point(0x453D);
            status = S_NOTSUPPORT;
            break;
        }
        if (status != S_SUCCESS)
        {
            log_push_error_point(0x453E);
            goto obd2tune_ford_apply_checksum_done;
        }

        status = obd2tune_getecmsize_byecmtype(ecm_type,&ecm_size);
        if (status != S_SUCCESS)
        {
            log_push_error_point(0x453F);
            break;
        }
        offset += ecm_size;
    }
    
obd2tune_ford_apply_checksum_done:
    filestock_closeflashfile();
    return status;
}

//------------------------------------------------------------------------------
// Check checksum of stock file
// Input:   u16 veh_type
//          selected_tunelist_info *tuneinfo
// Return:  u8  status
// Engineer: Quyen Leba
//------------------------------------------------------------------------------
u8 obd2tune_ford_check_stockfile_checksum(u16 veh_type, selected_tunelist_info *tuneinfo)
{
    return S_NOTREQUIRED;
}

static const u32 crc32_ccitt_tab[256]=
{
    0x00000000, 0x04C11DB7, 0x09823B6E, 0x0D4326D9, 0x130476DC, 0x17C56B6B, 0x1A864DB2, 0x1E475005,
    0x2608EDB8, 0x22C9F00F, 0x2F8AD6D6, 0x2B4BCB61, 0x350C9B64, 0x31CD86D3, 0x3C8EA00A, 0x384FBDBD,
    0x4C11DB70, 0x48D0C6C7, 0x4593E01E, 0x4152FDA9, 0x5F15ADAC, 0x5BD4B01B, 0x569796C2, 0x52568B75,
    0x6A1936C8, 0x6ED82B7F, 0x639B0DA6, 0x675A1011, 0x791D4014, 0x7DDC5DA3, 0x709F7B7A, 0x745E66CD,
    0x9823B6E0, 0x9CE2AB57, 0x91A18D8E, 0x95609039, 0x8B27C03C, 0x8FE6DD8B, 0x82A5FB52, 0x8664E6E5,
    0xBE2B5B58, 0xBAEA46EF, 0xB7A96036, 0xB3687D81, 0xAD2F2D84, 0xA9EE3033, 0xA4AD16EA, 0xA06C0B5D,
    0xD4326D90, 0xD0F37027, 0xDDB056FE, 0xD9714B49, 0xC7361B4C, 0xC3F706FB, 0xCEB42022, 0xCA753D95,
    0xF23A8028, 0xF6FB9D9F, 0xFBB8BB46, 0xFF79A6F1, 0xE13EF6F4, 0xE5FFEB43, 0xE8BCCD9A, 0xEC7DD02D,
    0x34867077, 0x30476DC0, 0x3D044B19, 0x39C556AE, 0x278206AB, 0x23431B1C, 0x2E003DC5, 0x2AC12072,
    0x128E9DCF, 0x164F8078, 0x1B0CA6A1, 0x1FCDBB16, 0x018AEB13, 0x054BF6A4, 0x0808D07D, 0x0CC9CDCA,
    0x7897AB07, 0x7C56B6B0, 0x71159069, 0x75D48DDE, 0x6B93DDDB, 0x6F52C06C, 0x6211E6B5, 0x66D0FB02,
    0x5E9F46BF, 0x5A5E5B08, 0x571D7DD1, 0x53DC6066, 0x4D9B3063, 0x495A2DD4, 0x44190B0D, 0x40D816BA,
    0xACA5C697, 0xA864DB20, 0xA527FDF9, 0xA1E6E04E, 0xBFA1B04B, 0xBB60ADFC, 0xB6238B25, 0xB2E29692,
    0x8AAD2B2F, 0x8E6C3698, 0x832F1041, 0x87EE0DF6, 0x99A95DF3, 0x9D684044, 0x902B669D, 0x94EA7B2A,
    0xE0B41DE7, 0xE4750050, 0xE9362689, 0xEDF73B3E, 0xF3B06B3B, 0xF771768C, 0xFA325055, 0xFEF34DE2,
    0xC6BCF05F, 0xC27DEDE8, 0xCF3ECB31, 0xCBFFD686, 0xD5B88683, 0xD1799B34, 0xDC3ABDED, 0xD8FBA05A,
    0x690CE0EE, 0x6DCDFD59, 0x608EDB80, 0x644FC637, 0x7A089632, 0x7EC98B85, 0x738AAD5C, 0x774BB0EB,
    0x4F040D56, 0x4BC510E1, 0x46863638, 0x42472B8F, 0x5C007B8A, 0x58C1663D, 0x558240E4, 0x51435D53,
    0x251D3B9E, 0x21DC2629, 0x2C9F00F0, 0x285E1D47, 0x36194D42, 0x32D850F5, 0x3F9B762C, 0x3B5A6B9B,
    0x0315D626, 0x07D4CB91, 0x0A97ED48, 0x0E56F0FF, 0x1011A0FA, 0x14D0BD4D, 0x19939B94, 0x1D528623,
    0xF12F560E, 0xF5EE4BB9, 0xF8AD6D60, 0xFC6C70D7, 0xE22B20D2, 0xE6EA3D65, 0xEBA91BBC, 0xEF68060B,
    0xD727BBB6, 0xD3E6A601, 0xDEA580D8, 0xDA649D6F, 0xC423CD6A, 0xC0E2D0DD, 0xCDA1F604, 0xC960EBB3,
    0xBD3E8D7E, 0xB9FF90C9, 0xB4BCB610, 0xB07DABA7, 0xAE3AFBA2, 0xAAFBE615, 0xA7B8C0CC, 0xA379DD7B,
    0x9B3660C6, 0x9FF77D71, 0x92B45BA8, 0x9675461F, 0x8832161A, 0x8CF30BAD, 0x81B02D74, 0x857130C3,
    0x5D8A9099, 0x594B8D2E, 0x5408ABF7, 0x50C9B640, 0x4E8EE645, 0x4A4FFBF2, 0x470CDD2B, 0x43CDC09C,
    0x7B827D21, 0x7F436096, 0x7200464F, 0x76C15BF8, 0x68860BFD, 0x6C47164A, 0x61043093, 0x65C52D24,
    0x119B4BE9, 0x155A565E, 0x18197087, 0x1CD86D30, 0x029F3D35, 0x065E2082, 0x0B1D065B, 0x0FDC1BEC,
    0x3793A651, 0x3352BBE6, 0x3E119D3F, 0x3AD08088, 0x2497D08D, 0x2056CD3A, 0x2D15EBE3, 0x29D4F654,
    0xC5A92679, 0xC1683BCE, 0xCC2B1D17, 0xC8EA00A0, 0xD6AD50A5, 0xD26C4D12, 0xDF2F6BCB, 0xDBEE767C,
    0xE3A1CBC1, 0xE760D676, 0xEA23F0AF, 0xEEE2ED18, 0xF0A5BD1D, 0xF464A0AA, 0xF9278673, 0xFDE69BC4,
    0x89B8FD09, 0x8D79E0BE, 0x803AC667, 0x84FBDBD0, 0x9ABC8BD5, 0x9E7D9662, 0x933EB0BB, 0x97FFAD0C,
    0xAFB010B1, 0xAB710D06, 0xA6322BDF, 0xA2F33668, 0xBCB4666D, 0xB8757BDA, 0xB5365D03, 0xB1F740B4
};

//------------------------------------------------------------------------------
// CRC32CCITT routine with seed
// Inputs:  u8  *buffer
//          u32 Start (in buffer)
//          u32 End (in buffer)
//          u32 Seed
// Return:  u32 crc
//------------------------------------------------------------------------------
u32 __crc32_ccitt(u8 *buffer, u32 Start, u32 End, u32 Seed)
{
    u32 crc;
    u32 counter;

    crc = Seed;
    for(counter = Start; counter <= End; counter++)
    {
        crc = (crc<<8) ^ crc32_ccitt_tab[((crc>>0x18) ^ buffer[counter]) & 0xFF];
    }
    return(crc);
}

//------------------------------------------------------------------------------
// Inputs:  u32 file_beginpos
//          u32 file_endpos
//          u32 *seed
// Output:  u32 *seed (new seed)
// Note: require flash file already opened; use filestock_openflashfile(...)
//------------------------------------------------------------------------------
u8 __checksum_64L_calculate_checksum_block(u32 file_beginpos, u32 file_endpos,
                                           u32 *seed)
{
    s32 length;
    u8  buffer[512];
    u32 bufferlength;
    u32 newseed;
    u8  status;

    if (file_beginpos > file_endpos)
    {
        log_push_error_point(0x4540);
        status = S_INPUT;
        goto __checksum_64L_calculate_checksum_block_done;
    }
    newseed = *seed;
    length = file_endpos - file_beginpos + 1;

    status = filestock_set_position(file_beginpos);
    while (length >= 512)
    {
        status = filestock_read(buffer,512,&bufferlength);
        if (status != S_SUCCESS || bufferlength != 512)
        {
            log_push_error_point(0x4541);
            status = S_READFILE;
            goto __checksum_64L_calculate_checksum_block_done;
        }
        newseed = __crc32_ccitt(buffer, 0, 511, newseed);
        length -= 512;
    }

    // less than 1 block left, so handle that
    if (length > 0)
    {
        status = filestock_read(buffer,length,&bufferlength);
        if (status != S_SUCCESS || bufferlength != length)
        {
            log_push_error_point(0x4542);
            status = S_READFILE;
            goto __checksum_64L_calculate_checksum_block_done;
        }
        newseed = __crc32_ccitt(buffer, 0, length - 1, newseed);
    }
    *seed = newseed;
    status = S_SUCCESS;
    
__checksum_64L_calculate_checksum_block_done:
    return status;
}

//------------------------------------------------------------------------------
// Apply checksum for 6.4L PCM
// Inputs:  u16 ecm_type
//          u32 offset (flash file offset)
// Return:  u8  status
// Engineer: Patrick Downs, Quyen Leba
// Note: require flash file already opened; use filestock_openflashfile(...)
//------------------------------------------------------------------------------
u8 obd2tune_ford_checksum_64L(u16 ecm_type, u32 offset)
{
    u32 seed, start, end, blocks, address;
    u32 calc_offset;
    u32 temp_u32;
    u32 i;
    u8  buffer[8];
    u32 bytecount;
    u8  status;

    status = obd2tune_translate_ecm_address_to_ecm_offset
        (ecm_type, 0x840000, &calc_offset);
    if (status != S_SUCCESS)
    {
        log_push_error_point(0x4543);
        goto obd2tune_ford_checksum_64L_done;
    }
    calc_offset += offset;

    //get seed
    status = filestock_read_at_position(calc_offset+0x100,buffer,4,&bytecount);
    if (status != S_SUCCESS || bytecount != 4)
    {
        log_push_error_point(0x4544);
        status = S_READFILE;
        goto obd2tune_ford_checksum_64L_done;
    }
    seed = (buffer[0]<<24) | (buffer[1]<<16) | (buffer[2]<<8) | (buffer[3]);
    //get blocks
    status = filestock_read_at_position(calc_offset+0x108,buffer,4,&bytecount);
    if (status != S_SUCCESS || bytecount != 4)
    {
        log_push_error_point(0x4545);
        status = S_READFILE;
        goto obd2tune_ford_checksum_64L_done;
    }
    blocks = (buffer[0]<<24) | (buffer[1]<<16) | (buffer[2]<<8) | (buffer[3]);

    address = calc_offset+0x10C;
    for (i = 0; i < blocks; i++)
    {
        //get start & end
        status = filestock_read_at_position(address,buffer,8,&bytecount);
        if (status != S_SUCCESS || bytecount != 8)
        {
            log_push_error_point(0x4546);
            status = S_READFILE;
            goto obd2tune_ford_checksum_64L_done;
        }
        start = (buffer[0]<<24) | (buffer[1]<<16) | (buffer[2]<<8) | (buffer[3]);
        end = (buffer[4]<<24) | (buffer[5]<<16) | (buffer[6]<<8) | (buffer[7]);
        address += 8;

        status = obd2tune_translate_ecm_address_to_ecm_offset
            (ecm_type, start, &temp_u32);
        if (status != S_SUCCESS)
        {
            log_push_error_point(0x4547);
            goto obd2tune_ford_checksum_64L_done;
        }
        start = temp_u32;   //translate start to position in file
        status = obd2tune_translate_ecm_address_to_ecm_offset
            (ecm_type, end, &temp_u32);
        if (status != S_SUCCESS)
        {
            log_push_error_point(0x4548);
            goto obd2tune_ford_checksum_64L_done;
        }
        end = temp_u32;   //translate end to position in file

        status =  __checksum_64L_calculate_checksum_block(start+offset,
                                                          end+offset,
                                                          &seed);
        if (status != S_SUCCESS)
        {
            log_push_error_point(0x4549);
            goto obd2tune_ford_checksum_64L_done;
        }
    }
    //new checksum value
    buffer[0] = seed >> 24;
    buffer[1] = seed >> 16;
    buffer[2] = seed >>  8;
    buffer[3] = seed;

    status = filestock_updateflashfile(calc_offset+0x104,buffer,4);

obd2tune_ford_checksum_64L_done:
    return status;
}

//------------------------------------------------------------------------------
// Test ECU OS checksum/crc to determine if an OS reflash is required
// Input:   u16 ecm_type
// Return:  u8  status
// Engineer: Rhonda Burns, Quyen Leba
//------------------------------------------------------------------------------
u8 obd2tune_ford_ecu_test_os_checksum(u16 ecm_type)
{
    return S_NOTREQUIRED;
}

//------------------------------------------------------------------------------
// Test file OS checksum/crc to determine if file is good
// Inputs:  const u8 *filename
//          u16 ecm_type
// Return:  u8  status
// Engineer: Rhonda Burns, Quyen Leba
//------------------------------------------------------------------------------
u8 obd2tune_ford_file_test_os_checksum(const u8 *filename, u16 ecm_type)
{
    return S_NOTREQUIRED;
}


