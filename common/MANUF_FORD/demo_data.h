/**
  **************** (C) COPYRIGHT 2011 SCT Performance, LLC *********************
  * File Name          : demo_data.h
  * Device / System    : SCT products using the "CommonCode" platform
  * Author             : Quyen Leba
  *
  * Version            : 1 
  * Date               : 05/02/2011
  * Description        : 
  *                    : 
  *
  *
  * History            : 05/02/2011 M. Davis
  *                    :   Add SCT Copyright header,
  *
  ******************************************************************************
  */

#ifndef __DEMO_DATA_H
#define __DEMO_DATA_H

#include <common/devicedef.h>
#include <common/obd2def.h>

#define DEMO_MUSTANG_5_0L               0
#define DEMO_SUPERDUTY_6_0L             1

typedef struct
{
    u8  upload_dummy_delay;     //in ms
    u8  download_dummy_delay;   //in ms
}DemoControl;

typedef struct
{
    u32 ecminfo_crc32e;
    ecm_info ecm;
}DemoVehicle;

typedef struct
{
    u8  count;
    u16 codes[3];
}DemoDTC;

typedef struct
{
    u8  count;
    struct
    {
        u32 address;
        float lo;
        float hi;
#define DUMMYVALUETYPE_RANDOM           0
#define DUMMYVALUETYPE_SWEEP            1
        u8  dummyvaluetype;
        float dummyattrvalue;
    }items[13];
}DemoDatalog;

//------------------------------------------------------------------------------
// Demo mode data
//------------------------------------------------------------------------------
static const DemoVehicle demo_vehicle =
{
    .ecminfo_crc32e = 0,
#if DEMO_MUSTANG_5_0L
    .ecm =
    {
        .ecm_count = 1,
        .codecount = {1,0,0},
        .codeids[0] = {0,0,0,0,0,0,0,0,0,0},
        .codeids[1] = {0,0,0,0,0,0,0,0,0,0},
        .codeids[2] = {0,0,0,0,0,0,0,0,0,0},
        .codes[0][0] = "FGDH3S6",                   //5.0L Mustang
        .second_codes[0][0] = "BR3A-14C204-BJA",
        .vehicle_serial_number[0] = 0,
        .vin = "SCT_MUSTANG__5.0L",
        .commtype = {CommType_CAN,CommType_Unknown,CommType_Unknown},
        .commlevel = {CommLevel_ISO14229,CommLevel_Unknown,CommLevel_Unknown},
    },
#endif
#if DEMO_SUPERDUTY_6_0L
    .ecm =
    {
        .ecm_count = 2,
        .codecount = {1,1,0},
        .codeids[0] = {0,0,0,0,0,0,0,0,0,0},
        .codeids[1] = {0,0,0,0,0,0,0,0,0,0},
        .codeids[2] = {0,0,0,0,0,0,0,0,0,0},
        .codes[0][0] = "VXCF4HB",                   //6.0l PCM
        .codes[1][0] = "TQCJ0H2",                   //6.0L TCM
        .second_codes[0][0][0] = 0,
        .second_codes[1][0][0] = 0,
        .vehicle_serial_number[0] = 0,
        .vin = "SCT_SUPERDUTY6.0L",
        .commtype = {CommType_CAN,CommType_CAN,CommType_Unknown},
        .commlevel = {CommLevel_KWP2000,CommLevel_KWP2000,CommLevel_Unknown},
    },
#endif
};

static const DemoControl demo_control =
{
    .upload_dummy_delay = 1,
    .download_dummy_delay = 1,
};

static const DemoDTC demo_dtc = 
{
    .count = 3,
    .codes = {0x0100, 0x0108, 0x0110},
};

static const DemoDatalog demo_datalog =
{
    .count = 13,
    //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    // '08 & newer
    //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    .items[0] =
    {
        .address = 0xF442,      //vbatt: 2 bytes
        .lo = 11,       .hi = 14,
        .dummyvaluetype = DUMMYVALUETYPE_RANDOM,        .dummyattrvalue = 0,
    },
    .items[1] =
    {
        .address = 0xF40F,      //iat F
        .lo = 110,      .hi = 120,
        .dummyvaluetype = DUMMYVALUETYPE_RANDOM,        .dummyattrvalue = 0,
    },
    .items[2] =
    {
        .address = 0xF40C,      //rpm: 2 bytes
        .lo = 2000,     .hi = 6000,
        .dummyvaluetype = DUMMYVALUETYPE_SWEEP,         .dummyattrvalue = 50,
    },
    .items[3] =
    {
        .address = 0xF40E,      //spark
        .lo = 20,       .hi = 21,
        .dummyvaluetype = DUMMYVALUETYPE_RANDOM,        .dummyattrvalue = 0,
    },
    .items[4] =
    {
        .address = 0xF40B,      //boost psi
        .lo = 10,       .hi = 12,
        .dummyvaluetype = DUMMYVALUETYPE_RANDOM,        .dummyattrvalue = 0,
    },
    //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    // '07 & older
    //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    .items[6] =
    {
        .address = 0x1172,      //vbatt: 1 byte
        .lo = 11,       .hi = 14,
        .dummyvaluetype = DUMMYVALUETYPE_RANDOM,        .dummyattrvalue = 0,
    },
    .items[7] =
    {
        .address = 0x1165,      //rpm: 2 bytes
        .lo = 2000,     .hi = 6000,
        .dummyvaluetype = DUMMYVALUETYPE_SWEEP,         .dummyattrvalue = 50,
    },
    .items[8] =
    {
        .address = 0x1123,      //iat
        .lo = 110,      .hi = 120,
        .dummyvaluetype = DUMMYVALUETYPE_RANDOM,        .dummyattrvalue = 0,
    },
    //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    // 6.0L & 7.3L
    //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    .items[9] =
    {
        .address = 0x1139,      //coolant temp
        .lo = 200,      .hi = 210,
        .dummyvaluetype = DUMMYVALUETYPE_RANDOM,        .dummyattrvalue = 0,
    },
    .items[10] =
    {
        .address = 0x1310,      //eng oil temp
        .lo = 220,      .hi = 240,
        .dummyvaluetype = DUMMYVALUETYPE_RANDOM,        .dummyattrvalue = 0,
    },
    .items[11] =
    {
        .address = 0x1441,      //turbo boost
        .lo = 9,        .hi = 11,
        .dummyvaluetype = DUMMYVALUETYPE_RANDOM,        .dummyattrvalue = 0,
    },
    .items[12] =
    {
        .address = 0x1674,      //trans temp
        .lo = 120,      .hi = 140,
        .dummyvaluetype = DUMMYVALUETYPE_RANDOM,        .dummyattrvalue = 0,
    },
};

#endif    //__DEMO_DATA_H
