/**
  **************** (C) COPYRIGHT 2011 SCT Performance, LLC *********************
  * File Name          : ford_ecm_defs.c
  * Device / System    : SCT products using the "CommonCode" platform
  * Author             : Quyen Leba
  *
  * Version            : 1 
  * Date               : 05/02/2011
  * Description        : 
  *                    : 
  *
  *
  * History            : 05/02/2011 M. Davis
  *                    :   Add SCT Copyright header,
  *
  ******************************************************************************
  */

#include "ford_ecm_utility_defs.h"
#include "ford_ecm_defs.h"

enum
{
    FILE_0      = 0,
    FILE_1      = 1,
    FILE_2      = 2,
    FILE_3      = 3,
};

typedef struct
{
    u32 filesize;
    u8 *filename;
}IndexedAppendFile;

//Filename used in some processor to append file content instead of upload from
//processor. Note: firmware won't rely on reading filesize from file while
//doing stock verification
const IndexedAppendFile IndexedAppendFiles[1] = {
    [FILE_0] = {65536,  "/XXX_1C0000_01.bin"}
};

//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
const ECM_Def ford_ecm_defs[ECM_DEF_COUNT] =
{
    //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    [0] =   //EEC 216K (SCP)                                            //0x00
    {
        .index = 0,
        .ecm_id = 0x10, .reponse_ecm_id = 0xF0,
        .unlock_flags = UNLOCK_FLAG_NONE,
        .upload_unlock_id = UnlockId_Regular,
        .download_unlock_id = UnlockId_Regular,
        .checksum_id = ChecksumId_None,
        .finalizing_id = FinalId_None,
        .flags = SIMPLE_WAKEUP_CHECK_UPLOAD | SIMPLE_WAKEUP_CHECK_DOWNLOAD | SINGLE_UNLOCK_ONLY |
            UPLOAD_ADDR_TYPE_3B | UPLOAD_BY_CMD_35_SINGLE_36 |
            USE_ERASE_COMMAND |
            DOWNLOAD_ADDR_TYPE_3B | DOWNLOAD_REQUEST_TYPE_3B |
            DOWNLOAD_BLOCK_BACKWARD | DOWNLOAD_REQUIRE_CMD_37 | DOWNLOAD_ALL | BLOCK_GENERATE_STOCK,
        .extended_flags = ECM_FLAGS_NONE,
        .commtype = CommType_SCP, .commlevel = CommLevel_KWP2000,
        .eraseall_cmd = {EraseCmdInfoFlags_None,EraseCmdInfoCount1,{0x00}},
        .erasecal_cmd = {EraseCmdInfoFlags_None,EraseCmdInfoCount1,{0x00}},
        .vid_address = 0x09ff80, .pats_address = 0x09ff1d,
        .vin_address = ECM_DEFS_INVALID_VIN_ADDRESS,
        .testerpresent_frequency = 0,
        
        .data_offset = 0x0,
        .ecm_startingaddr = 0x0,
        
        .util_upload = {NULL,NULL,NULL},
        .util_dnload = {NULL,NULL,NULL},
        .maxuploadblocksize = 0x400, .maxdownloadblocksize = 0x400,
        .os_block_read_skipblock_info = NULL,
        .os_block_read =
        {
            {0x002000,  0xe000,     ORDER_0 | MEMBLOCK_FLAGS_NONE, 0},
            {0x012000,  0xc000,     ORDER_1 | MEMBLOCK_FLAGS_NONE, 0},
            {0x092000,  0xe000,     ORDER_2 | MEMBLOCK_FLAGS_NONE, 0},
            {0x082000,  0xe000,     ORDER_3 | MEMBLOCK_FLAGS_NONE, 0},
            {0, 0, ORDER_4 | MEMBLOCK_FLAGS_NONE, 0},
        },
        .os_block =
        {
            {0x002000,  0xe000,     ORDER_0 | MEMBLOCK_FLAGS_NONE, 0},
            {0x012000,  0xc000,     ORDER_1 | MEMBLOCK_FLAGS_NONE, 0},
            {0x092000,  0xe000,     ORDER_2 | MEMBLOCK_FLAGS_NONE, 0},
            {0x082000,  0xe000,     ORDER_3 | MEMBLOCK_FLAGS_NONE, 0},
            {0, 0, ORDER_4 | MEMBLOCK_FLAGS_NONE, 0},
        },
        .cal_block =
        {
            {0x002000,  0xe000,     ORDER_0 | MEMBLOCK_FLAGS_NONE, 0},
            {0x012000,  0xc000,     ORDER_1 | MEMBLOCK_FLAGS_NONE, 0},
            {0x092000,  0xe000,     ORDER_2 | MEMBLOCK_FLAGS_NONE, 0},
            {0x082000,  0xe000,     ORDER_3 | MEMBLOCK_FLAGS_NONE, 0},
            {0, 0, ORDER_4 | MEMBLOCK_FLAGS_NONE, 0},
        },
    },
    //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    [1] =   //EEC 220K (SCP)                                            //0x01
    {
        .index = 1,
        .ecm_id = 0x10, .reponse_ecm_id = 0xF0,
        .unlock_flags = UNLOCK_FLAG_NONE,
        .upload_unlock_id = UnlockId_Regular,
        .download_unlock_id = UnlockId_Regular,
        .checksum_id = ChecksumId_None,
        .finalizing_id = FinalId_None,
        .flags = SIMPLE_WAKEUP_CHECK_UPLOAD | SIMPLE_WAKEUP_CHECK_DOWNLOAD | SINGLE_UNLOCK_ONLY |
            UPLOAD_ADDR_TYPE_3B | UPLOAD_BY_CMD_35_SINGLE_36 |
            USE_ERASE_COMMAND |
            DOWNLOAD_ADDR_TYPE_3B | DOWNLOAD_REQUEST_TYPE_3B |
            DOWNLOAD_BLOCK_BACKWARD | DOWNLOAD_REQUIRE_CMD_37,
        .extended_flags = ECM_FLAGS_NONE,
        .commtype = CommType_SCP, .commlevel = CommLevel_KWP2000,
        .eraseall_cmd = {EraseCmdInfoFlags_None,EraseCmdInfoCount1,{0x00}},
        .erasecal_cmd = {EraseCmdInfoFlags_None,EraseCmdInfoCount1,{0x00}},
        .vid_address = 0x09ff80, .pats_address = 0x09ff1d,
        .vin_address = ECM_DEFS_INVALID_VIN_ADDRESS,
        .testerpresent_frequency = 0,
        
        .data_offset = 0x0,
        .ecm_startingaddr = 0x0,
        
        .util_upload = {NULL,NULL,NULL},
        .util_dnload = {NULL,NULL,NULL},
        .maxuploadblocksize = 0x400, .maxdownloadblocksize = 0x400,
        .os_block_read_skipblock_info = NULL,
        .os_block_read =
        {
            {0x002000,  0xe000,     ORDER_0 | MEMBLOCK_FLAGS_NONE, 0},
            {0x012000,  0xd000,     ORDER_1 | MEMBLOCK_FLAGS_NONE, 0},
            {0x092000,  0xe000,     ORDER_2 | MEMBLOCK_FLAGS_NONE, 0},
            {0x082000,  0xe000,     ORDER_3 | MEMBLOCK_FLAGS_NONE, 0},
            {0, 0, ORDER_4 | MEMBLOCK_FLAGS_NONE, 0},
        },
        .os_block =
        {
            {0x002000,  0xe000,     ORDER_0 | MEMBLOCK_FLAGS_NONE, 0},
            {0x012000,  0xd000,     ORDER_1 | MEMBLOCK_FLAGS_NONE, 0},
            {0x092000,  0xe000,     ORDER_2 | MEMBLOCK_FLAGS_NONE, 0},
            {0x082000,  0xe000,     ORDER_3 | MEMBLOCK_FLAGS_NONE, 0},
            {0, 0, ORDER_4 | MEMBLOCK_FLAGS_NONE, 0},
        },
        .cal_block =
        {
            {0x002000,  0xe000,     ORDER_0 | MEMBLOCK_FLAGS_NONE, 0},
            {0x012000,  0xd000,     ORDER_1 | MEMBLOCK_FLAGS_NONE, 0},
            {0x092000,  0xe000,     ORDER_2 | MEMBLOCK_FLAGS_NONE, 0},
            {0x082000,  0xe000,     ORDER_3 | MEMBLOCK_FLAGS_NONE, 0},
            {0, 0, ORDER_4 | MEMBLOCK_FLAGS_NONE, 0},
        },
    },
    //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    [2] =   //EEC 112K (SCP)                                            //0x02
    {
        .index = 2,
        .ecm_id = 0x10, .reponse_ecm_id = 0xF0,
        .unlock_flags = UNLOCK_FLAG_NONE,
        .upload_unlock_id = UnlockId_Regular,
        .download_unlock_id = UnlockId_Regular,
        .checksum_id = ChecksumId_None,
        .finalizing_id = FinalId_None,
        .flags = SIMPLE_WAKEUP_CHECK_UPLOAD | SIMPLE_WAKEUP_CHECK_DOWNLOAD | SINGLE_UNLOCK_ONLY |
            UPLOAD_ADDR_TYPE_3B | UPLOAD_BY_CMD_23 |
            USE_ERASE_COMMAND |
            DOWNLOAD_ADDR_TYPE_3B | DOWNLOAD_REQUEST_TYPE_3B |
            DOWNLOAD_BLOCK_BACKWARD | DOWNLOAD_REQUIRE_CMD_37,
        .extended_flags = ECM_FLAGS_NONE,
        .commtype = CommType_SCP, .commlevel = CommLevel_KWP2000,
        .eraseall_cmd = {EraseCmdInfoFlags_None,EraseCmdInfoCount1,{0x00}},
        .erasecal_cmd = {EraseCmdInfoFlags_None,EraseCmdInfoCount1,{0x00}},
        .vid_address = 0x00ff80, .pats_address = 0x00ff1d,
        .vin_address = ECM_DEFS_INVALID_VIN_ADDRESS,
        .testerpresent_frequency = 0,
        
        .data_offset = 0x0,
        .ecm_startingaddr = 0x0,

        .util_upload = {NULL,NULL,NULL},
        .util_dnload = {NULL,NULL,NULL},
        .maxuploadblocksize = 0x800, .maxdownloadblocksize = 0x400,
        .os_block_read_skipblock_info = NULL,
        .os_block_read =
        {
            {0x002000,  0xe000,     ORDER_0 | MEMBLOCK_FLAGS_ADDRESS_OFFSET_BY_FDATA, 0x10000},
            {0x011000,  0xe000,     ORDER_1 | MEMBLOCK_FLAGS_ADDRESS_OFFSET_BY_FDATA, 0x71000},
            {0, 0, ORDER_2 | MEMBLOCK_FLAGS_NONE, 0},
            {0, 0, ORDER_3 | MEMBLOCK_FLAGS_NONE, 0},
            {0, 0, ORDER_4 | MEMBLOCK_FLAGS_NONE, 0},
        },
        .os_block =
        {
            {0x002000,  0xe000,     ORDER_0 | MEMBLOCK_FLAGS_NONE, 0},
            {0x011000,  0xe000,     ORDER_1 | MEMBLOCK_FLAGS_NONE, 0},
            {0, 0, ORDER_2 | MEMBLOCK_FLAGS_NONE, 0},
            {0, 0, ORDER_3 | MEMBLOCK_FLAGS_NONE, 0},
            {0, 0, ORDER_4 | MEMBLOCK_FLAGS_NONE, 0},
        },
        .cal_block =
        {
            {0x002000,  0xe000,     ORDER_0 | MEMBLOCK_FLAGS_NONE, 0},
            {0x011000,  0xe000,     ORDER_1 | MEMBLOCK_FLAGS_NONE, 0},
            {0, 0, ORDER_2 | MEMBLOCK_FLAGS_NONE, 0},
            {0, 0, ORDER_3 | MEMBLOCK_FLAGS_NONE, 0},
            {0, 0, ORDER_4 | MEMBLOCK_FLAGS_NONE, 0},
        },
    },
    //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    [3] =   //EEC 88K (SCP)                                             //0x03
    {
        .index = 3,
        .ecm_id = 0x10, .reponse_ecm_id = 0xF0,
        .unlock_flags = UNLOCK_FLAG_NONE,
        .upload_unlock_id = UnlockId_Regular,
        .download_unlock_id = UnlockId_Regular,
        .checksum_id = ChecksumId_None,
        .finalizing_id = FinalId_None,
        .flags = SIMPLE_WAKEUP_CHECK_UPLOAD | SIMPLE_WAKEUP_CHECK_DOWNLOAD | SINGLE_UNLOCK_ONLY |
            UPLOAD_ADDR_TYPE_3B | UPLOAD_BY_CMD_23 |
            USE_ERASE_COMMAND |
            DOWNLOAD_ADDR_TYPE_3B | DOWNLOAD_REQUEST_TYPE_3B |
            DOWNLOAD_BLOCK_BACKWARD | DOWNLOAD_REQUIRE_CMD_37,
        .extended_flags = ECM_FLAGS_NONE,
        .commtype = CommType_SCP, .commlevel = CommLevel_KWP2000,
        .eraseall_cmd = {EraseCmdInfoFlags_None,EraseCmdInfoCount1,{0x00}},
        .erasecal_cmd = {EraseCmdInfoFlags_None,EraseCmdInfoCount1,{0x00}},
        .vid_address = 0x009f80, .pats_address = 0x009f1d,
        .vin_address = ECM_DEFS_INVALID_VIN_ADDRESS,
        .testerpresent_frequency = 0,
        
        .data_offset = 0x0,
        .ecm_startingaddr = 0x0,
        
        .util_upload = {NULL,NULL,NULL},
        .util_dnload = {NULL,NULL,NULL},
        .maxuploadblocksize = 0x800, .maxdownloadblocksize = 0x400,
        .os_block_read_skipblock_info = NULL,
        .os_block_read =
        {
            {0x002000,  0x8000,     ORDER_0 | MEMBLOCK_FLAGS_ADDRESS_OFFSET_BY_FDATA, 0x10000},
            {0x011000,  0xe000,     ORDER_1 | MEMBLOCK_FLAGS_ADDRESS_OFFSET_BY_FDATA, 0x71000},
            {0, 0, ORDER_2 | MEMBLOCK_FLAGS_NONE, 0},
            {0, 0, ORDER_3 | MEMBLOCK_FLAGS_NONE, 0},
            {0, 0, ORDER_4 | MEMBLOCK_FLAGS_NONE, 0},
        },
        .os_block =
        {
            {0x002000,  0x8000,     ORDER_0 | MEMBLOCK_FLAGS_NONE, 0},
            {0x011000,  0xe000,     ORDER_1 | MEMBLOCK_FLAGS_NONE, 0},
            {0, 0, ORDER_2 | MEMBLOCK_FLAGS_NONE, 0},
            {0, 0, ORDER_3 | MEMBLOCK_FLAGS_NONE, 0},
            {0, 0, ORDER_4 | MEMBLOCK_FLAGS_NONE, 0},
        },
        .cal_block =
        {
            {0x002000,  0x8000,     ORDER_0 | MEMBLOCK_FLAGS_NONE, 0},
            {0x011000,  0xe000,     ORDER_1 | MEMBLOCK_FLAGS_NONE, 0},
            {0, 0, ORDER_2 | MEMBLOCK_FLAGS_NONE, 0},
            {0, 0, ORDER_3 | MEMBLOCK_FLAGS_NONE, 0},
            {0, 0, ORDER_4 | MEMBLOCK_FLAGS_NONE, 0},
        },
    },
    //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    [4] =   //Black Oak 1472K (CAN)                                     //0x04
    {
        .index = 4,
        .ecm_id = 0x7E0, .reponse_ecm_id = 0x7E8,
        .unlock_flags = UNLOCK_FLAG_NONE,
        .upload_unlock_id = UnlockId_Regular,
        .download_unlock_id = UnlockId_Regular,
        .checksum_id = ChecksumId_None,
        .finalizing_id = FinalId_None,
        .flags = UPLOAD_ADDR_TYPE_4B | UPLOAD_BY_CMD_23 |
            USE_ERASE_COMMAND |
            DOWNLOAD_ADDR_TYPE_4B |  DOWNLOAD_REQUEST_TYPE_4B |
            DOWNLOAD_REQUIRE_CMD_37,
        .extended_flags = DOWNLOAD_HEALTH_CHECK_STRICT,
        .commtype = CommType_CAN, .commlevel = CommLevel_KWP2000,
        .eraseall_cmd = {EraseCmdInfoFlags_None,EraseCmdInfoCount1,{0xAA}},
        .erasecal_cmd = {EraseCmdInfoFlags_None,EraseCmdInfoCount1,{0xAA}},
        .vid_address = 0x0100c0, .pats_address = 0x01005d,
        .vin_address = ECM_DEFS_INVALID_VIN_ADDRESS,
        .testerpresent_frequency = 0,
        
        .data_offset = 0x0,
        .ecm_startingaddr = 0x0,
        
        .util_upload = {NULL,NULL,NULL},
        .util_dnload = {NULL,NULL,NULL},
        .maxuploadblocksize = 0x400, .maxdownloadblocksize = 0x400,
        .os_block_read_skipblock_info = NULL,
        .os_block_read =
        {
            {0x010000,  0x60000,    ORDER_0 | MEMBLOCK_FLAGS_NONE, 0},
            {0x500000,  0x100000,   ORDER_1 | MEMBLOCK_FLAGS_NONE, 0},
            {0, 0, ORDER_2 | MEMBLOCK_FLAGS_NONE, 0},
            {0, 0, ORDER_3 | MEMBLOCK_FLAGS_NONE, 0},
            {0, 0, ORDER_4 | MEMBLOCK_FLAGS_NONE, 0},
        },
        .os_block =
        {
            {0x010000,  0x60000,    ORDER_0 | MEMBLOCK_FLAGS_NONE, 0},
            {0x500000,  0x100000,   ORDER_1 | MEMBLOCK_FLAGS_NONE, 0},
            {0, 0, ORDER_2 | MEMBLOCK_FLAGS_NONE, 0},
            {0, 0, ORDER_3 | MEMBLOCK_FLAGS_NONE, 0},
            {0, 0, ORDER_4 | MEMBLOCK_FLAGS_NONE, 0},
        },
        .cal_block =
        {
            {0x010000,  0x60000,    ORDER_0 | MEMBLOCK_FLAGS_NONE, 0},
            {0x500000,  0x100000,   ORDER_1 | MEMBLOCK_FLAGS_NONE, 0},
            {0, 0, ORDER_2 | MEMBLOCK_FLAGS_NONE, 0},
            {0, 0, ORDER_3 | MEMBLOCK_FLAGS_NONE, 0},
            {0, 0, ORDER_4 | MEMBLOCK_FLAGS_NONE, 0},
            //0x580000 - 0x080000 (90,91,92)
        },
    },
    //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    [5] =   //Black Oak 448K (CAN)                                      //0x05
    {
        .index = 5,
        .ecm_id = 0x7E0, .reponse_ecm_id = 0x7E8,
        .unlock_flags = UNLOCK_FLAG_NONE,
        .upload_unlock_id = UnlockId_Regular,
        .download_unlock_id = UnlockId_Regular,
        .checksum_id = ChecksumId_None,
        .finalizing_id = FinalId_None,
        .flags = UPLOAD_ADDR_TYPE_4B | UPLOAD_BY_CMD_23 |
            USE_ERASE_COMMAND |
            DOWNLOAD_ADDR_TYPE_4B |  DOWNLOAD_REQUEST_TYPE_4B |
            DOWNLOAD_REQUIRE_CMD_37 | DOWNLOAD_ALL | BLOCK_GENERATE_STOCK,
        .extended_flags = ECM_FLAGS_NONE,
        .commtype = CommType_CAN, .commlevel = CommLevel_KWP2000,
        .eraseall_cmd = {EraseCmdInfoFlags_None,EraseCmdInfoCount1,{0xAA}},
        .erasecal_cmd = {EraseCmdInfoFlags_None,EraseCmdInfoCount1,{0xAA}},
        .vid_address = 0x0100c0, .pats_address = 0x01005d,
        .vin_address = ECM_DEFS_INVALID_VIN_ADDRESS,
        .testerpresent_frequency = 10,
        
        .data_offset = 0x0,
        .ecm_startingaddr = 0x0,
        
        .util_upload = {NULL,NULL,NULL},
        .util_dnload = {NULL,NULL,NULL},
        .maxuploadblocksize = 0x400, .maxdownloadblocksize = 0x400,
        .os_block_read_skipblock_info = NULL,
        .os_block_read =
        {
            {0x010000,  0x60000,    ORDER_0 | MEMBLOCK_FLAGS_NONE, 0},
            {0, 0, ORDER_1 | MEMBLOCK_FLAGS_NONE, 0},
            {0, 0, ORDER_2 | MEMBLOCK_FLAGS_NONE, 0},
            {0, 0, ORDER_3 | MEMBLOCK_FLAGS_NONE, 0},
            {0, 0, ORDER_4 | MEMBLOCK_FLAGS_NONE, 0},
        },
        .os_block =
        {
            {0x010000,  0x60000,    ORDER_0 | MEMBLOCK_FLAGS_NONE, 0},
            {0, 0, ORDER_1 | MEMBLOCK_FLAGS_NONE, 0},
            {0, 0, ORDER_2 | MEMBLOCK_FLAGS_NONE, 0},
            {0, 0, ORDER_3 | MEMBLOCK_FLAGS_NONE, 0},
            {0, 0, ORDER_4 | MEMBLOCK_FLAGS_NONE, 0},
        },
        .cal_block =
        {
            {0x010000,  0x60000,    ORDER_0 | MEMBLOCK_FLAGS_NONE, 0},
            {0, 0, ORDER_1 | MEMBLOCK_FLAGS_NONE, 0},
            {0, 0, ORDER_2 | MEMBLOCK_FLAGS_NONE, 0},
            {0, 0, ORDER_3 | MEMBLOCK_FLAGS_NONE, 0},
            {0, 0, ORDER_4 | MEMBLOCK_FLAGS_NONE, 0},
        },
    },
    //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    [6] =   //Black Oak 448K Trans (CAN)                                //0x06
    {
        .index = 6,
        .ecm_id = 0x7E1, .reponse_ecm_id = 0x7E9,
        .unlock_flags = UNLOCK_FLAG_NONE,
        .upload_unlock_id = UnlockId_Regular,
        .download_unlock_id = UnlockId_Regular,
        .checksum_id = ChecksumId_None,
        .finalizing_id = FinalId_None,
        .flags = UPLOAD_ADDR_TYPE_4B | UPLOAD_BY_CMD_23 |
            USE_ERASE_COMMAND |
            DOWNLOAD_ADDR_TYPE_4B |  DOWNLOAD_REQUEST_TYPE_4B |
            DOWNLOAD_REQUIRE_CMD_37,
        .extended_flags = ECM_FLAGS_NONE,
        .commtype = CommType_CAN, .commlevel = CommLevel_KWP2000,
        .eraseall_cmd = {EraseCmdInfoFlags_None,EraseCmdInfoCount1,{0xAA}},
        .erasecal_cmd = {EraseCmdInfoFlags_None,EraseCmdInfoCount1,{0xAA}},
        .vid_address = 0, .pats_address = 0,
        .vin_address = ECM_DEFS_INVALID_VIN_ADDRESS,
        .testerpresent_frequency = 10,
        
        .data_offset = 0x0,
        .ecm_startingaddr = 0x0,
        
        .util_upload = {NULL,NULL,NULL},
        .util_dnload = {NULL,NULL,NULL},
        .maxuploadblocksize = 0x400, .maxdownloadblocksize = 0x400,
        .os_block_read_skipblock_info = NULL,
        .os_block_read =
        {
            {0x010000,  0x60000,    ORDER_0 | MEMBLOCK_FLAGS_NONE, 0},
            {0, 0, ORDER_1 | MEMBLOCK_FLAGS_NONE, 0},
            {0, 0, ORDER_2 | MEMBLOCK_FLAGS_NONE, 0},
            {0, 0, ORDER_3 | MEMBLOCK_FLAGS_NONE, 0},
            {0, 0, ORDER_4 | MEMBLOCK_FLAGS_NONE, 0},
        },
        .os_block =
        {
            {0x010000,  0x60000,    ORDER_0 | MEMBLOCK_FLAGS_NONE, 0},
            {0, 0, ORDER_1 | MEMBLOCK_FLAGS_NONE, 0},
            {0, 0, ORDER_2 | MEMBLOCK_FLAGS_NONE, 0},
            {0, 0, ORDER_3 | MEMBLOCK_FLAGS_NONE, 0},
            {0, 0, ORDER_4 | MEMBLOCK_FLAGS_NONE, 0},
        },
        .cal_block =
        {
            {0x010000,  0x60000,    ORDER_0 | MEMBLOCK_FLAGS_NONE, 0},
            {0, 0, ORDER_1 | MEMBLOCK_FLAGS_NONE, 0},
            {0, 0, ORDER_2 | MEMBLOCK_FLAGS_NONE, 0},
            {0, 0, ORDER_3 | MEMBLOCK_FLAGS_NONE, 0},
            {0, 0, ORDER_4 | MEMBLOCK_FLAGS_NONE, 0},
        },
    },
    //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    [7] =   //Spanish Oak 1024K (CAN)                                   //0x07
    {
        .index = 7,
        .ecm_id = 0x7E0, .reponse_ecm_id = 0x7E8,
        .unlock_flags = UNLOCK_FLAG_NONE,
        .upload_unlock_id = UnlockId_Regular,
        .download_unlock_id = UnlockId_Regular,
        .checksum_id = ChecksumId_None,
        .finalizing_id = FinalId_None,
        .flags = UPLOAD_ADDR_TYPE_4B | UPLOAD_BY_CMD_23 |
            USE_ERASE_COMMAND |
            DOWNLOAD_ADDR_TYPE_4B | DOWNLOAD_REQUEST_TYPE_4B |
            DOWNLOAD_REQUIRE_CMD_37 | COOLDOWNTIME_SHORT,
        .extended_flags = DOWNLOAD_HEALTH_CHECK_STRICT,
        .commtype = CommType_CAN, .commlevel = CommLevel_KWP2000,
        .eraseall_cmd = {EraseCmdInfoFlags_None,EraseCmdInfoCount1,{0xAA}},
        .erasecal_cmd = {EraseCmdInfoFlags_None,EraseCmdInfoCount1,{0xAA}},
        .vid_address = 0x100c0, .pats_address = 0x1005d,
        .vin_address = ECM_DEFS_INVALID_VIN_ADDRESS,
        .testerpresent_frequency = 0,
        
        .data_offset = 0x0,
        .ecm_startingaddr = 0x0,
        
        .util_upload = {NULL,NULL,NULL},
        .util_dnload = {NULL,NULL,NULL},
        .maxuploadblocksize = 0x400, .maxdownloadblocksize = 0x400,
        .os_block_read_skipblock_info = NULL,
        .os_block_read =
        {
            {0x010000,  0xf0000,    ORDER_0 | MEMBLOCK_FLAGS_NONE, 0},
            {0, 0, ORDER_1 | MEMBLOCK_FLAGS_NONE, 0},
            {0, 0, ORDER_2 | MEMBLOCK_FLAGS_NONE, 0},
            {0, 0, ORDER_3 | MEMBLOCK_FLAGS_NONE, 0},
            {0, 0, ORDER_4 | MEMBLOCK_FLAGS_NONE, 0},
        },
        .os_block =
        {
            {0x010000,  0xf0000,    ORDER_0 | MEMBLOCK_FLAGS_NONE, 0},
            {0, 0, ORDER_1 | MEMBLOCK_FLAGS_NONE, 0},
            {0, 0, ORDER_2 | MEMBLOCK_FLAGS_NONE, 0},
            {0, 0, ORDER_3 | MEMBLOCK_FLAGS_NONE, 0},
            {0, 0, ORDER_4 | MEMBLOCK_FLAGS_NONE, 0},
        },
        .cal_block =
        {
            {0x010000,  0xf0000,    ORDER_0 | MEMBLOCK_FLAGS_NONE, 0},
            {0, 0, ORDER_1 | MEMBLOCK_FLAGS_NONE, 0},
            {0, 0, ORDER_2 | MEMBLOCK_FLAGS_NONE, 0},
            {0, 0, ORDER_3 | MEMBLOCK_FLAGS_NONE, 0},
            {0, 0, ORDER_4 | MEMBLOCK_FLAGS_NONE, 0},
        },
    },
    //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    [8] =   //Green Oak 512K (CAN)                                      //0x08
    {
        .index = 8,
        .ecm_id = 0x7E0, .reponse_ecm_id = 0x7E8,
        .unlock_flags = UNLOCK_FLAG_NONE,
        .upload_unlock_id = UnlockId_Regular,
        .download_unlock_id = UnlockId_Regular,
        .checksum_id = ChecksumId_None,
        .finalizing_id = FinalId_None,
        .flags = UPLOAD_ADDR_TYPE_4B | UPLOAD_BY_CMD_23 |   //test me upload
            USE_ERASE_COMMAND |
            DOWNLOAD_ADDR_TYPE_4B |  DOWNLOAD_REQUEST_TYPE_4B |
            DOWNLOAD_REQUIRE_CMD_37 | DOWNLOAD_ALL | BLOCK_GENERATE_STOCK,
        .extended_flags = ECM_FLAGS_NONE,
        .commtype = CommType_CAN, .commlevel = CommLevel_KWP2000,
        .eraseall_cmd = {EraseCmdInfoFlags_None,EraseCmdInfoCount1,{0xAA}},
        .erasecal_cmd = {EraseCmdInfoFlags_None,EraseCmdInfoCount1,{0xAA}},
        .vid_address = 0x100c0, .pats_address = 0x1005d,
        .vin_address = ECM_DEFS_INVALID_VIN_ADDRESS,
        .testerpresent_frequency = 0,
        
        .data_offset = 0x0,
        .ecm_startingaddr = 0x0,
        
        .util_upload = {NULL,NULL,NULL},
        .util_dnload = {NULL,NULL,NULL},
        .maxuploadblocksize = 0x400, .maxdownloadblocksize = 0x400,
        .os_block_read_skipblock_info = NULL,
        .os_block_read =
        {
            {0x010000,  0x70000,    ORDER_0 | MEMBLOCK_FLAGS_NONE, 0},
            {0, 0, ORDER_1 | MEMBLOCK_FLAGS_NONE, 0},
            {0, 0, ORDER_2 | MEMBLOCK_FLAGS_NONE, 0},
            {0, 0, ORDER_3 | MEMBLOCK_FLAGS_NONE, 0},
            {0, 0, ORDER_4 | MEMBLOCK_FLAGS_NONE, 0},
        },
        .os_block =
        {
            {0x010000,  0x70000,    ORDER_0 | MEMBLOCK_FLAGS_NONE, 0},
            {0, 0, ORDER_1 | MEMBLOCK_FLAGS_NONE, 0},
            {0, 0, ORDER_2 | MEMBLOCK_FLAGS_NONE, 0},
            {0, 0, ORDER_3 | MEMBLOCK_FLAGS_NONE, 0},
            {0, 0, ORDER_4 | MEMBLOCK_FLAGS_NONE, 0},
        },
        .cal_block =
        {
            {0x010000,  0x70000,    ORDER_0 | MEMBLOCK_FLAGS_NONE, 0},
            {0, 0, ORDER_1 | MEMBLOCK_FLAGS_NONE, 0},
            {0, 0, ORDER_2 | MEMBLOCK_FLAGS_NONE, 0},
            {0, 0, ORDER_3 | MEMBLOCK_FLAGS_NONE, 0},
            {0, 0, ORDER_4 | MEMBLOCK_FLAGS_NONE, 0},
        },
    },
    //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    [9] =   //Equizzer HC12 (CAN)                                       //0x09
    {
        .index = 9,
        .ecm_id = 0x7E0, .reponse_ecm_id = 0x7E8,
        .unlock_flags = UNLOCK_FLAG_NONE,
        .upload_unlock_id = UnlockId_Regular,
        .download_unlock_id = UnlockId_Regular,
        .checksum_id = ChecksumId_None,
        .finalizing_id = FinalId_None,
        .flags = UPLOAD_ADDR_TYPE_4B | UPLOAD_BY_CMD_23 |   //test me upload
            USE_ERASE_COMMAND |
            DOWNLOAD_ADDR_TYPE_4B |  DOWNLOAD_REQUEST_TYPE_4B |
            DOWNLOAD_REQUIRE_CMD_37,
        .extended_flags = ECM_FLAGS_NONE,
        .commtype = CommType_CAN, .commlevel = CommLevel_KWP2000,
        .eraseall_cmd = {EraseCmdInfoFlags_None,EraseCmdInfoCount1,{0xCC}},
        .erasecal_cmd = {EraseCmdInfoFlags_None,EraseCmdInfoCount1,{0xCC}},
        .vid_address = 0, .pats_address = 0,
        .vin_address = ECM_DEFS_INVALID_VIN_ADDRESS,
        .testerpresent_frequency = 0,
        
        .data_offset = 0x0,
        .ecm_startingaddr = 0x0,
        
        .util_upload = {NULL,NULL,NULL},
        .util_dnload = {NULL,NULL,NULL},
        .maxuploadblocksize = 0x400, .maxdownloadblocksize = 0x400,
        .os_block_read_skipblock_info = NULL,
        .os_block_read =
        {
            {0xe2000d00,    0x300,  ORDER_0 | MEMBLOCK_FLAGS_NONE, 0},
            {0xe2008000,    0x7800, ORDER_1 | MEMBLOCK_FLAGS_NONE, 0},
            {0, 0, ORDER_2 | MEMBLOCK_FLAGS_NONE, 0},
            {0, 0, ORDER_3 | MEMBLOCK_FLAGS_NONE, 0},
            {0, 0, ORDER_4 | MEMBLOCK_FLAGS_NONE, 0},
        },
        .os_block =
        {
            {0xe2000d00,    0x300,  ORDER_0 | MEMBLOCK_FLAGS_NONE, 0},
            {0xe2008000,    0x7800, ORDER_1 | MEMBLOCK_FLAGS_NONE, 0},
            {0, 0, ORDER_2 | MEMBLOCK_FLAGS_NONE, 0},
            {0, 0, ORDER_3 | MEMBLOCK_FLAGS_NONE, 0},
            {0, 0, ORDER_4 | MEMBLOCK_FLAGS_NONE, 0},
        },
        .cal_block =
        {
            {0xe2000d00,    0x300,  ORDER_0 | MEMBLOCK_FLAGS_NONE, 0},
            {0xe2008000,    0x7800, ORDER_1 | MEMBLOCK_FLAGS_NONE, 0},
            {0, 0, ORDER_2 | MEMBLOCK_FLAGS_NONE, 0},
            {0, 0, ORDER_3 | MEMBLOCK_FLAGS_NONE, 0},
            {0, 0, ORDER_4 | MEMBLOCK_FLAGS_NONE, 0},
        },
    },
    //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    [10] =  //Equizzer Star-12 B (CAN)                                  //0x0A
    {
        .index = 10,
        .ecm_id = 0x7E0, .reponse_ecm_id = 0x7E8,
        .unlock_flags = UNLOCK_FLAG_NONE,
        .upload_unlock_id = UnlockId_Regular,
        .download_unlock_id = UnlockId_Regular,
        .checksum_id = ChecksumId_None,
        .finalizing_id = FinalId_None,
        .flags = UPLOAD_ADDR_TYPE_4B | UPLOAD_BY_CMD_23 |   //test me upload
            USE_ERASE_COMMAND |
            DOWNLOAD_ADDR_TYPE_4B |  DOWNLOAD_REQUEST_TYPE_4B |
            DOWNLOAD_REQUIRE_CMD_37,
        .extended_flags = ECM_FLAGS_NONE,
        .commtype = CommType_CAN, .commlevel = CommLevel_KWP2000,
        .eraseall_cmd = {EraseCmdInfoFlags_None,EraseCmdInfoCount1,{0xCC}},
        .erasecal_cmd = {EraseCmdInfoFlags_None,EraseCmdInfoCount1,{0xCC}},
        .vid_address = 0, .pats_address = 0,
        .vin_address = ECM_DEFS_INVALID_VIN_ADDRESS,
        .testerpresent_frequency = 0,
        
        .data_offset = 0x0,
        .ecm_startingaddr = 0x0,
        
        .util_upload = {NULL,NULL,NULL},
        .util_dnload = {NULL,NULL,NULL},
        .maxuploadblocksize = 0x400, .maxdownloadblocksize = 0x400,
        .os_block_read_skipblock_info = NULL,
        .os_block_read =
        {
            {0xe2000c00,    0x400,  ORDER_0 | MEMBLOCK_FLAGS_NONE, 0},
            {0xe20f8000,    0x7000, ORDER_1 | MEMBLOCK_FLAGS_NONE, 0},
            {0, 0, ORDER_2 | MEMBLOCK_FLAGS_NONE, 0},
            {0, 0, ORDER_3 | MEMBLOCK_FLAGS_NONE, 0},
            {0, 0, ORDER_4 | MEMBLOCK_FLAGS_NONE, 0},
        },
        .os_block =
        {
            {0xe2000c00,    0x400,  ORDER_0 | MEMBLOCK_FLAGS_NONE, 0},
            {0xe20f8000,    0x7000, ORDER_1 | MEMBLOCK_FLAGS_NONE, 0},
            {0, 0, ORDER_2 | MEMBLOCK_FLAGS_NONE, 0},
            {0, 0, ORDER_3 | MEMBLOCK_FLAGS_NONE, 0},
            {0, 0, ORDER_4 | MEMBLOCK_FLAGS_NONE, 0},
        },
        .cal_block =
        {
            {0xe2000c00,    0x400,  ORDER_0 | MEMBLOCK_FLAGS_NONE, 0},
            {0xe20f8000,    0x7000, ORDER_1 | MEMBLOCK_FLAGS_NONE, 0},
            {0, 0, ORDER_2 | MEMBLOCK_FLAGS_NONE, 0},
            {0, 0, ORDER_3 | MEMBLOCK_FLAGS_NONE, 0},
            {0, 0, ORDER_4 | MEMBLOCK_FLAGS_NONE, 0},
        },
    },
    //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    [11] =  //Equizzer Star-12 C (CAN)                                  //0x0B
    {
        .index = 11,
        .ecm_id = 0x7E0, .reponse_ecm_id = 0x7E8,
        .unlock_flags = UNLOCK_FLAG_NONE,
        .upload_unlock_id = UnlockId_Regular,
        .download_unlock_id = UnlockId_Regular,
        .checksum_id = ChecksumId_None,
        .finalizing_id = FinalId_None,
        .flags = UPLOAD_ADDR_TYPE_4B | UPLOAD_BY_CMD_23 |   //test me upload
            USE_ERASE_COMMAND |
            DOWNLOAD_ADDR_TYPE_4B |  DOWNLOAD_REQUEST_TYPE_4B |
            DOWNLOAD_REQUIRE_CMD_37,
        .extended_flags = ECM_FLAGS_NONE,
        .commtype = CommType_CAN, .commlevel = CommLevel_KWP2000,
        .eraseall_cmd = {EraseCmdInfoFlags_None,EraseCmdInfoCount1,{0xCC}},
        .erasecal_cmd = {EraseCmdInfoFlags_None,EraseCmdInfoCount1,{0xCC}},
        .vid_address = 0, .pats_address = 0,
        .vin_address = ECM_DEFS_INVALID_VIN_ADDRESS,
        .testerpresent_frequency = 0,
        
        .data_offset = 0x0,
        .ecm_startingaddr = 0x0,
        
        .util_upload = {NULL,NULL,NULL},
        .util_dnload = {NULL,NULL,NULL},
        .maxuploadblocksize = 0x400, .maxdownloadblocksize = 0x400,
        .os_block_read_skipblock_info = NULL,
        .os_block_read =
        {
            {0, 0, ORDER_0 | MEMBLOCK_FLAGS_NONE, 0},
            {0, 0, ORDER_1 | MEMBLOCK_FLAGS_NONE, 0},
            {0, 0, ORDER_2 | MEMBLOCK_FLAGS_NONE, 0},
            {0, 0, ORDER_3 | MEMBLOCK_FLAGS_NONE, 0},
            {0, 0, ORDER_4 | MEMBLOCK_FLAGS_NONE, 0},
        },
        .os_block =
        {
            {0, 0, ORDER_0 | MEMBLOCK_FLAGS_NONE, 0},
            {0, 0, ORDER_1 | MEMBLOCK_FLAGS_NONE, 0},
            {0, 0, ORDER_2 | MEMBLOCK_FLAGS_NONE, 0},
            {0, 0, ORDER_3 | MEMBLOCK_FLAGS_NONE, 0},
            {0, 0, ORDER_4 | MEMBLOCK_FLAGS_NONE, 0},
        },
        .cal_block =
        {
            {0, 0, ORDER_0 | MEMBLOCK_FLAGS_NONE, 0},
            {0, 0, ORDER_1 | MEMBLOCK_FLAGS_NONE, 0},
            {0, 0, ORDER_2 | MEMBLOCK_FLAGS_NONE, 0},
            {0, 0, ORDER_3 | MEMBLOCK_FLAGS_NONE, 0},
            {0, 0, ORDER_4 | MEMBLOCK_FLAGS_NONE, 0},
        },
    },
    //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    [12] =  //Equizzer Star-12 D (CAN)                                  //0x0C
    {
        .index = 12,
        .ecm_id = 0x7E0, .reponse_ecm_id = 0x7E8,
        .unlock_flags = UNLOCK_FLAG_NONE,
        .upload_unlock_id = UnlockId_Regular,
        .download_unlock_id = UnlockId_Regular,
        .checksum_id = ChecksumId_None,
        .finalizing_id = FinalId_None,
        .flags = UPLOAD_ADDR_TYPE_4B | UPLOAD_BY_CMD_23 |   //test me upload
            USE_ERASE_COMMAND |
            DOWNLOAD_ADDR_TYPE_4B |  DOWNLOAD_REQUEST_TYPE_4B |
            DOWNLOAD_REQUIRE_CMD_37,
        .extended_flags = ECM_FLAGS_NONE,
        .commtype = CommType_CAN, .commlevel = CommLevel_KWP2000,
        .eraseall_cmd = {EraseCmdInfoFlags_None,EraseCmdInfoCount1,{0xCC}},
        .erasecal_cmd = {EraseCmdInfoFlags_None,EraseCmdInfoCount1,{0xCC}},
        .vid_address = 0, .pats_address = 0,
        .vin_address = ECM_DEFS_INVALID_VIN_ADDRESS,
        .testerpresent_frequency = 0,
        
        .data_offset = 0x0,
        .ecm_startingaddr = 0x0,
        
        .util_upload = {NULL,NULL,NULL},
        .util_dnload = {NULL,NULL,NULL},
        .maxuploadblocksize = 0x400, .maxdownloadblocksize = 0x400,
        .os_block_read_skipblock_info = NULL,
        .os_block_read =
        {
            {0, 0, ORDER_0 | MEMBLOCK_FLAGS_NONE, 0},
            {0, 0, ORDER_1 | MEMBLOCK_FLAGS_NONE, 0},
            {0, 0, ORDER_2 | MEMBLOCK_FLAGS_NONE, 0},
            {0, 0, ORDER_3 | MEMBLOCK_FLAGS_NONE, 0},
            {0, 0, ORDER_4 | MEMBLOCK_FLAGS_NONE, 0},
        },
        .os_block =
        {
            {0, 0, ORDER_0 | MEMBLOCK_FLAGS_NONE, 0},
            {0, 0, ORDER_1 | MEMBLOCK_FLAGS_NONE, 0},
            {0, 0, ORDER_2 | MEMBLOCK_FLAGS_NONE, 0},
            {0, 0, ORDER_3 | MEMBLOCK_FLAGS_NONE, 0},
            {0, 0, ORDER_4 | MEMBLOCK_FLAGS_NONE, 0},
        },
        .cal_block =
        {
            {0, 0, ORDER_0 | MEMBLOCK_FLAGS_NONE, 0},
            {0, 0, ORDER_1 | MEMBLOCK_FLAGS_NONE, 0},
            {0, 0, ORDER_2 | MEMBLOCK_FLAGS_NONE, 0},
            {0, 0, ORDER_3 | MEMBLOCK_FLAGS_NONE, 0},
            {0, 0, ORDER_4 | MEMBLOCK_FLAGS_NONE, 0},
        },
    },
    //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    [13] =  //Black Oak 1472K (SCP32)                                   0x0D
    {
        .index = 13,
        .ecm_id = 0x10, .reponse_ecm_id = 0xF0,
        .unlock_flags = UNLOCK_FLAG_NONE,
        .upload_unlock_id = UnlockId_Regular,
        .download_unlock_id = UnlockId_Regular,
        .checksum_id = ChecksumId_None,
        .finalizing_id = FinalId_None,
        .flags = SIMPLE_WAKEUP_CHECK_UPLOAD | SIMPLE_WAKEUP_CHECK_DOWNLOAD | SINGLE_UNLOCK_ONLY |
            UPLOAD_ADDR_TYPE_4B | UPLOAD_BY_CMD_35_SINGLE_36 |
            USE_ERASE_COMMAND |
            DOWNLOAD_ADDR_TYPE_4B |  DOWNLOAD_REQUEST_TYPE_4B |
            DOWNLOAD_BLOCK_BACKWARD | DOWNLOAD_REQUIRE_CMD_37,
        .extended_flags = ECM_FLAGS_NONE,
        .commtype = CommType_SCP32, .commlevel = CommLevel_KWP2000,
        .eraseall_cmd = {EraseCmdInfoFlags_None,EraseCmdInfoCount1,{0x00}},
        .erasecal_cmd = {EraseCmdInfoFlags_None,EraseCmdInfoCount1,{0x00}},
        .vid_address = 0x100c0, .pats_address = 0x1005d,
        .vin_address = ECM_DEFS_INVALID_VIN_ADDRESS,
        .testerpresent_frequency = 0,
        
        .data_offset = 0x0,
        .ecm_startingaddr = 0x0,
        
        .util_upload = {NULL,NULL,NULL},
        .util_dnload = {NULL,NULL,NULL},
        .maxuploadblocksize = 0x400, .maxdownloadblocksize = 0x400,
        .os_block_read_skipblock_info = NULL,
        .os_block_read =
        {
            {0x010000,  0x060000,   ORDER_0 | MEMBLOCK_FLAGS_NONE, 0},
            {0x500000,  0x100000,   ORDER_1 | MEMBLOCK_FLAGS_NONE, 0},
            {0, 0, ORDER_2 | MEMBLOCK_FLAGS_NONE, 0},
            {0, 0, ORDER_3 | MEMBLOCK_FLAGS_NONE, 0},
            {0, 0, ORDER_4 | MEMBLOCK_FLAGS_NONE, 0},
        },
        .os_block =
        {
            {0x010000,  0x060000,   ORDER_0 | MEMBLOCK_FLAGS_NONE, 0},
            {0x500000,  0x100000,   ORDER_1 | MEMBLOCK_FLAGS_NONE, 0},
            {0, 0, ORDER_2 | MEMBLOCK_FLAGS_NONE, 0},
            {0, 0, ORDER_3 | MEMBLOCK_FLAGS_NONE, 0},
            {0, 0, ORDER_4 | MEMBLOCK_FLAGS_NONE, 0},
        },
        .cal_block =
        {
            {0x010000,  0x060000,   ORDER_0 | MEMBLOCK_FLAGS_NONE, 0},
            {0x500000,  0x100000,   ORDER_1 | MEMBLOCK_FLAGS_NONE, 0},
            {0, 0, ORDER_2 | MEMBLOCK_FLAGS_NONE, 0},
            {0, 0, ORDER_3 | MEMBLOCK_FLAGS_NONE, 0},
            {0, 0, ORDER_4 | MEMBLOCK_FLAGS_NONE, 0},
        },
    },
    //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    [14] =  //PTEC 1024K (SCP32)                                        //0x0E
    {
        .index = 14,
        .ecm_id = 0x10, .reponse_ecm_id = 0xF0,
        .unlock_flags = UNLOCK_FLAG_NONE,
        .upload_unlock_id = UnlockId_Regular,
        .download_unlock_id = UnlockId_Regular,
        .checksum_id = ChecksumId_None,
        .finalizing_id = FinalId_None,
        .flags = SIMPLE_WAKEUP_CHECK_UPLOAD | SIMPLE_WAKEUP_CHECK_DOWNLOAD | SINGLE_UNLOCK_ONLY |
            UPLOAD_ADDR_TYPE_4B | UPLOAD_BY_CMD_35_SINGLE_36 |
            USE_ERASE_COMMAND |
            DOWNLOAD_ADDR_TYPE_4B |  DOWNLOAD_REQUEST_TYPE_4B |
            DOWNLOAD_BLOCK_BACKWARD | DOWNLOAD_REQUIRE_CMD_37,
        .extended_flags = ECM_FLAGS_NONE,
        .commtype = CommType_SCP32, .commlevel = CommLevel_KWP2000,
        .eraseall_cmd = {EraseCmdInfoFlags_None,EraseCmdInfoCount1,{0x00}},
        .erasecal_cmd = {EraseCmdInfoFlags_None,EraseCmdInfoCount1,{0x00}},
        .vid_address = 0x00002e80, .pats_address = 0x00002e1d,
        .vin_address = ECM_DEFS_INVALID_VIN_ADDRESS,
        .testerpresent_frequency = 0,
        
        .data_offset = 0x0,
        .ecm_startingaddr = 0x0,
        
        .util_upload = {NULL,NULL,NULL},
        .util_dnload = {NULL,NULL,NULL},
        .maxuploadblocksize = 0x400, .maxdownloadblocksize = 0x400,
        .os_block_read_skipblock_info = NULL,
        .os_block_read =
        {
            {0, 0x100000, ORDER_0 | MEMBLOCK_FLAGS_NONE, 0},
            {0, 0, ORDER_1 | MEMBLOCK_FLAGS_NONE, 0},
            {0, 0, ORDER_2 | MEMBLOCK_FLAGS_NONE, 0},
            {0, 0, ORDER_3 | MEMBLOCK_FLAGS_NONE, 0},
            {0, 0, ORDER_4 | MEMBLOCK_FLAGS_NONE, 0},
        },
        .os_block =
        {
            {0, 0x100000,   ORDER_0 | MEMBLOCK_FLAGS_NONE, 0},
            {0, 0, ORDER_1 | MEMBLOCK_FLAGS_NONE, 0},
            {0, 0, ORDER_2 | MEMBLOCK_FLAGS_NONE, 0},
            {0, 0, ORDER_3 | MEMBLOCK_FLAGS_NONE, 0},
            {0, 0, ORDER_4 | MEMBLOCK_FLAGS_NONE, 0},
        },
        .cal_block =
        {
            {0, 0x100000,   ORDER_0 | MEMBLOCK_FLAGS_NONE, 0},
            {0, 0, ORDER_1 | MEMBLOCK_FLAGS_NONE, 0},
            {0, 0, ORDER_2 | MEMBLOCK_FLAGS_NONE, 0},
            {0, 0, ORDER_3 | MEMBLOCK_FLAGS_NONE, 0},
            {0, 0, ORDER_4 | MEMBLOCK_FLAGS_NONE, 0},
        },
    },
    //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    [15] =  //Black Oak 512K (CAN)                                      //0x0F
    {
        .index = 15,
        .ecm_id = 0x7E0, .reponse_ecm_id = 0x7E8,
        .unlock_flags = UNLOCK_FLAG_NONE,
        .upload_unlock_id = UnlockId_Regular,
        .download_unlock_id = UnlockId_Regular,
        .checksum_id = ChecksumId_None,
        .finalizing_id = FinalId_None,
        .flags = UPLOAD_ADDR_TYPE_4B | UPLOAD_BY_CMD_23 |
            USE_ERASE_COMMAND |
            DOWNLOAD_ADDR_TYPE_4B |  DOWNLOAD_REQUEST_TYPE_4B |
            DOWNLOAD_REQUIRE_CMD_37 | DOWNLOAD_ALL | BLOCK_GENERATE_STOCK,
        .extended_flags = ECM_FLAGS_NONE,
        .commtype = CommType_CAN, .commlevel = CommLevel_KWP2000,
        .eraseall_cmd = {EraseCmdInfoFlags_None,EraseCmdInfoCount1,{0xAA}},
        .erasecal_cmd = {EraseCmdInfoFlags_None,EraseCmdInfoCount1,{0xAA}},
        .vid_address = 0x100c0, .pats_address = 0x1005d,
        .vin_address = ECM_DEFS_INVALID_VIN_ADDRESS,
        .testerpresent_frequency = 0,
        
        .data_offset = 0x0,
        .ecm_startingaddr = 0x0,
        
        .util_upload = {NULL,NULL,NULL},
        .util_dnload = {NULL,NULL,NULL},
        .maxuploadblocksize = 0x400, .maxdownloadblocksize = 0x400,
        .os_block_read_skipblock_info = NULL,
        .os_block_read =
        {
            {0x010000,  0x70000,    ORDER_0 | MEMBLOCK_FLAGS_NONE, 0},
            {0, 0, ORDER_1 | MEMBLOCK_FLAGS_NONE, 0},
            {0, 0, ORDER_2 | MEMBLOCK_FLAGS_NONE, 0},
            {0, 0, ORDER_3 | MEMBLOCK_FLAGS_NONE, 0},
            {0, 0, ORDER_4 | MEMBLOCK_FLAGS_NONE, 0},
        },
        .os_block =
        {
            {0x010000,  0x70000,    ORDER_0 | MEMBLOCK_FLAGS_NONE, 0},
            {0, 0, ORDER_1 | MEMBLOCK_FLAGS_NONE, 0},
            {0, 0, ORDER_2 | MEMBLOCK_FLAGS_NONE, 0},
            {0, 0, ORDER_3 | MEMBLOCK_FLAGS_NONE, 0},
            {0, 0, ORDER_4 | MEMBLOCK_FLAGS_NONE, 0},
        },
        .cal_block =
        {
            {0x010000,  0x70000,    ORDER_0 | MEMBLOCK_FLAGS_NONE, 0},
            {0, 0, ORDER_1 | MEMBLOCK_FLAGS_NONE, 0},
            {0, 0, ORDER_2 | MEMBLOCK_FLAGS_NONE, 0},
            {0, 0, ORDER_3 | MEMBLOCK_FLAGS_NONE, 0},
            {0, 0, ORDER_4 | MEMBLOCK_FLAGS_NONE, 0},
        },
    },
    //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    [16] =  //Black Oak 512K Trans (CAN)                                //0x10
    {
        .index = 16,
        .ecm_id = 0x7E1, .reponse_ecm_id = 0x7E9,
        .unlock_flags = UNLOCK_FLAG_NONE,
        .upload_unlock_id = UnlockId_Regular,
        .download_unlock_id = UnlockId_Regular,
        .checksum_id = ChecksumId_None,
        .finalizing_id = FinalId_None,
        .flags = UPLOAD_ADDR_TYPE_4B | UPLOAD_BY_CMD_23 |
            USE_ERASE_COMMAND |
            DOWNLOAD_ADDR_TYPE_4B |  DOWNLOAD_REQUEST_TYPE_4B |
            DOWNLOAD_REQUIRE_CMD_37,
        .extended_flags = ECM_FLAGS_NONE,
        .commtype = CommType_CAN, .commlevel = CommLevel_KWP2000,
        .eraseall_cmd = {EraseCmdInfoFlags_None,EraseCmdInfoCount1,{0xAA}},
        .erasecal_cmd = {EraseCmdInfoFlags_None,EraseCmdInfoCount1,{0xAA}},
        .vid_address = 0, .pats_address = 0,
        .vin_address = ECM_DEFS_INVALID_VIN_ADDRESS,
        .testerpresent_frequency = 0,
        
        .data_offset = 0x0,
        .ecm_startingaddr = 0x0,
        
        .util_upload = {NULL,NULL,NULL},
        .util_dnload = {NULL,NULL,NULL},
        .maxuploadblocksize = 0x400, .maxdownloadblocksize = 0x400,
        .os_block_read_skipblock_info = NULL,
        .os_block_read =
        {
            {0x010000,  0x70000,    ORDER_0 | MEMBLOCK_FLAGS_NONE, 0},
            {0, 0, ORDER_1 | MEMBLOCK_FLAGS_NONE, 0},
            {0, 0, ORDER_2 | MEMBLOCK_FLAGS_NONE, 0},
            {0, 0, ORDER_3 | MEMBLOCK_FLAGS_NONE, 0},
            {0, 0, ORDER_4 | MEMBLOCK_FLAGS_NONE, 0},
        },
        .os_block =
        {
            {0x010000,  0x70000,    ORDER_0 | MEMBLOCK_FLAGS_NONE, 0},
            {0, 0, ORDER_1 | MEMBLOCK_FLAGS_NONE, 0},
            {0, 0, ORDER_2 | MEMBLOCK_FLAGS_NONE, 0},
            {0, 0, ORDER_3 | MEMBLOCK_FLAGS_NONE, 0},
            {0, 0, ORDER_4 | MEMBLOCK_FLAGS_NONE, 0},
        },
        .cal_block =
        {
            {0x010000,  0x70000,    ORDER_0 | MEMBLOCK_FLAGS_NONE, 0},
            {0, 0, ORDER_1 | MEMBLOCK_FLAGS_NONE, 0},
            {0, 0, ORDER_2 | MEMBLOCK_FLAGS_NONE, 0},
            {0, 0, ORDER_3 | MEMBLOCK_FLAGS_NONE, 0},
            {0, 0, ORDER_4 | MEMBLOCK_FLAGS_NONE, 0},
        },
    },
    //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    [17] =  //Equizzer Star-12 A (CAN)                                  //0x11
    {
        .index = 17,
        .ecm_id = 0x7E0, .reponse_ecm_id = 0x7E8,
        .unlock_flags = UNLOCK_FLAG_NONE,
        .upload_unlock_id = UnlockId_Regular,
        .download_unlock_id = UnlockId_Regular,
        .checksum_id = ChecksumId_None,
        .finalizing_id = FinalId_None,
        .flags = UPLOAD_ADDR_TYPE_4B | UPLOAD_BY_CMD_23 |   //test me upload
            USE_ERASE_COMMAND |
            DOWNLOAD_ADDR_TYPE_4B |  DOWNLOAD_REQUEST_TYPE_4B |
            DOWNLOAD_REQUIRE_CMD_37,
        .extended_flags = ECM_FLAGS_NONE,
        .commtype = CommType_CAN, .commlevel = CommLevel_KWP2000,
        .eraseall_cmd = {EraseCmdInfoFlags_None,EraseCmdInfoCount1,{0xCC}},
        .erasecal_cmd = {EraseCmdInfoFlags_None,EraseCmdInfoCount1,{0xCC}},
        .vid_address = 0, .pats_address = 0,
        .vin_address = ECM_DEFS_INVALID_VIN_ADDRESS,
        .testerpresent_frequency = 0,
        
        .data_offset = 0x0,
        .ecm_startingaddr = 0x0,
        
        .util_upload = {NULL,NULL,NULL},
        .util_dnload = {NULL,NULL,NULL},
        .maxuploadblocksize = 0x400, .maxdownloadblocksize = 0x400,
        .os_block_read_skipblock_info = NULL,
        .os_block_read =
        {
            {0xc00,     0x400,      ORDER_0 | MEMBLOCK_FLAGS_NONE, 0},
            {0xf8000,   0x7c00,     ORDER_1 | MEMBLOCK_FLAGS_NONE, 0},
            {0, 0, ORDER_2 | MEMBLOCK_FLAGS_NONE, 0},
            {0, 0, ORDER_3 | MEMBLOCK_FLAGS_NONE, 0},
            {0, 0, ORDER_4 | MEMBLOCK_FLAGS_NONE, 0},
        },
        .os_block =
        {
            {0xc00,     0x400,      ORDER_0 | MEMBLOCK_FLAGS_NONE, 0},
            {0xf8000,   0x7c00,     ORDER_1 | MEMBLOCK_FLAGS_NONE, 0},
            {0, 0, ORDER_2 | MEMBLOCK_FLAGS_NONE, 0},
            {0, 0, ORDER_3 | MEMBLOCK_FLAGS_NONE, 0},
            {0, 0, ORDER_4 | MEMBLOCK_FLAGS_NONE, 0},
        },
        .cal_block =
        {
            {0xc00,     0x400,      ORDER_0 | MEMBLOCK_FLAGS_NONE, 0},
            {0xf8000,   0x7c00,     ORDER_1 | MEMBLOCK_FLAGS_NONE, 0},
            {0, 0, ORDER_2 | MEMBLOCK_FLAGS_NONE, 0},
            {0, 0, ORDER_3 | MEMBLOCK_FLAGS_NONE, 0},
            {0, 0, ORDER_4 | MEMBLOCK_FLAGS_NONE, 0},
        },
    },
    //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    [18] =  //FICM (CAN)                                                //0x12
    {
        .index = 18,
        .ecm_id = 0x7E0, .reponse_ecm_id = 0x7E8,
        .unlock_flags = UNLOCK_FLAG_NONE,
        .upload_unlock_id = UnlockId_Regular,
        .download_unlock_id = UnlockId_Regular,
        .checksum_id = ChecksumId_None,
        .finalizing_id = FinalId_None,
        .flags = UPLOAD_ADDR_TYPE_4B | UPLOAD_BY_CMD_23 |   //test me upload
            USE_ERASE_COMMAND |
            DOWNLOAD_ADDR_TYPE_4B |  DOWNLOAD_REQUEST_TYPE_4B |
            DOWNLOAD_REQUIRE_CMD_37,
        .extended_flags = ECM_FLAGS_NONE,
        .commtype = CommType_CAN, .commlevel = CommLevel_KWP2000,
        .eraseall_cmd = {EraseCmdInfoFlags_None,EraseCmdInfoCount1,{0x01}},
        .erasecal_cmd = {EraseCmdInfoFlags_None,EraseCmdInfoCount1,{0x01}},
        .vid_address = 0, .pats_address = 0,
        .vin_address = ECM_DEFS_INVALID_VIN_ADDRESS,
        .testerpresent_frequency = 0,
        
        .data_offset = 0x0,
        .ecm_startingaddr = 0x0,
        
        .util_upload = {NULL,NULL,NULL},
        .util_dnload = {NULL,NULL,NULL},
        .maxuploadblocksize = 0x400, .maxdownloadblocksize = 0x400,
        .os_block_read_skipblock_info = NULL,
        .os_block_read =
        {
            {0, 0, ORDER_0 | MEMBLOCK_FLAGS_NONE, 0},
            {0, 0, ORDER_1 | MEMBLOCK_FLAGS_NONE, 0},
            {0, 0, ORDER_2 | MEMBLOCK_FLAGS_NONE, 0},
            {0, 0, ORDER_3 | MEMBLOCK_FLAGS_NONE, 0},
            {0, 0, ORDER_4 | MEMBLOCK_FLAGS_NONE, 0},
        },
        .os_block =
        {
            {0, 0, ORDER_0 | MEMBLOCK_FLAGS_NONE, 0},
            {0, 0, ORDER_1 | MEMBLOCK_FLAGS_NONE, 0},
            {0, 0, ORDER_2 | MEMBLOCK_FLAGS_NONE, 0},
            {0, 0, ORDER_3 | MEMBLOCK_FLAGS_NONE, 0},
            {0, 0, ORDER_4 | MEMBLOCK_FLAGS_NONE, 0},
        },
        .cal_block =
        {
            {0, 0, ORDER_0 | MEMBLOCK_FLAGS_NONE, 0},
            {0, 0, ORDER_1 | MEMBLOCK_FLAGS_NONE, 0},
            {0, 0, ORDER_2 | MEMBLOCK_FLAGS_NONE, 0},
            {0, 0, ORDER_3 | MEMBLOCK_FLAGS_NONE, 0},
            {0, 0, ORDER_4 | MEMBLOCK_FLAGS_NONE, 0},
        },
    },
    //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    [19] =  //Green Oak 1536K (CAN)                                     //0x13
    {
        .index = 19,
        .ecm_id = 0x7E0, .reponse_ecm_id = 0x7E8,
        .unlock_flags = UNLOCK_FLAG_NONE,
        .upload_unlock_id = UnlockId_Regular,
        .download_unlock_id = UnlockId_Regular,
        .checksum_id = ChecksumId_None,
        .finalizing_id = FinalId_None,
        .flags = UPLOAD_ADDR_TYPE_4B | UPLOAD_BY_CMD_23 |   //test me upload
            USE_ERASE_COMMAND |
            DOWNLOAD_ADDR_TYPE_4B |  DOWNLOAD_REQUEST_TYPE_4B |
            DOWNLOAD_REQUIRE_CMD_37,
        .extended_flags = ECM_FLAGS_NONE,
        .commtype = CommType_CAN, .commlevel = CommLevel_KWP2000,
        .eraseall_cmd = {EraseCmdInfoFlags_None,EraseCmdInfoCount1,{0xAA}},
        .erasecal_cmd = {EraseCmdInfoFlags_None,EraseCmdInfoCount1,{0xAA}},
        .vid_address = 0x0100c0, .pats_address = 0x01005d,
        .vin_address = ECM_DEFS_INVALID_VIN_ADDRESS,
        .testerpresent_frequency = 0,
        
        .data_offset = 0x0,
        .ecm_startingaddr = 0x0,
        
        .util_upload = {NULL,NULL,NULL},
        .util_dnload = {NULL,NULL,NULL},
        .maxuploadblocksize = 0x400, .maxdownloadblocksize = 0x400,
        .os_block_read_skipblock_info = NULL,
        .os_block_read =
        {
            {0x10000,   0x70000,    ORDER_0 | MEMBLOCK_FLAGS_NONE, 0},
            {0x500000,  0x100000,   ORDER_1 | MEMBLOCK_FLAGS_NONE, 0},
            {0, 0, ORDER_2 | MEMBLOCK_FLAGS_NONE, 0},
            {0, 0, ORDER_3 | MEMBLOCK_FLAGS_NONE, 0},
            {0, 0, ORDER_4 | MEMBLOCK_FLAGS_NONE, 0},
        },
        .os_block =
        {
            {0x10000,   0x70000,    ORDER_0 | MEMBLOCK_FLAGS_NONE, 0},
            {0x500000,  0x100000,   ORDER_1 | MEMBLOCK_FLAGS_NONE, 0},
            {0, 0, ORDER_2 | MEMBLOCK_FLAGS_NONE, 0},
            {0, 0, ORDER_3 | MEMBLOCK_FLAGS_NONE, 0},
            {0, 0, ORDER_4 | MEMBLOCK_FLAGS_NONE, 0},
        },
        .cal_block =
        {
            {0x10000,   0x70000,    ORDER_0 | MEMBLOCK_FLAGS_NONE, 0},
            {0x500000,  0x100000,   ORDER_1 | MEMBLOCK_FLAGS_NONE, 0},
            {0, 0, ORDER_2 | MEMBLOCK_FLAGS_NONE, 0},
            {0, 0, ORDER_3 | MEMBLOCK_FLAGS_NONE, 0},
            {0, 0, ORDER_4 | MEMBLOCK_FLAGS_NONE, 0},
        },
    },
    //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    [20] =  //Black Oak 2048K (CAN)                                     //0x14
    {
        .index = 20,
        .ecm_id = 0x7E0, .reponse_ecm_id = 0x7E8,
        .unlock_flags = UNLOCK_FLAG_NONE,
        .upload_unlock_id = UnlockId_Regular,
        .download_unlock_id = UnlockId_Regular,
        .checksum_id = ChecksumId_None,
        .finalizing_id = FinalId_None,
        .flags = UPLOAD_ADDR_TYPE_4B |  UPLOAD_BY_CMD_23 |   //test me upload
            USE_ERASE_COMMAND |
            DOWNLOAD_ADDR_TYPE_4B |  DOWNLOAD_REQUEST_TYPE_4B |
            DOWNLOAD_REQUIRE_CMD_37,
        .extended_flags = ECM_FLAGS_NONE,
        .commtype = CommType_CAN, .commlevel = CommLevel_KWP2000,
        .eraseall_cmd = {EraseCmdInfoFlags_None,EraseCmdInfoCount1,{0xAA}},
        .erasecal_cmd = {EraseCmdInfoFlags_None,EraseCmdInfoCount1,{0xAA}},
        .vid_address = 0x0100c0, .pats_address = 0x01005d,
        .vin_address = ECM_DEFS_INVALID_VIN_ADDRESS,
        .testerpresent_frequency = 0,
        
        .data_offset = 0x0,
        .ecm_startingaddr = 0x0,
        
        .util_upload = {NULL,NULL,NULL},
        .util_dnload = {NULL,NULL,NULL},
        .maxuploadblocksize = 0x400, .maxdownloadblocksize = 0x400,
        .os_block_read_skipblock_info = NULL,
        .os_block_read =
        {
            {0x010000,  0xF0000,    ORDER_0 | MEMBLOCK_FLAGS_NONE, 0},
            {0x800000,  0x100000,   ORDER_1 | MEMBLOCK_FLAGS_NONE, 0},
            {0, 0, ORDER_2 | MEMBLOCK_FLAGS_NONE, 0},
            {0, 0, ORDER_3 | MEMBLOCK_FLAGS_NONE, 0},
            {0, 0, ORDER_4 | MEMBLOCK_FLAGS_NONE, 0},
        },
        .os_block =
        {
            {0x010000,  0xF0000,    ORDER_0 | MEMBLOCK_FLAGS_NONE, 0},
            {0x800000,  0x100000,   ORDER_1 | MEMBLOCK_FLAGS_NONE, 0},
            {0, 0, ORDER_2 | MEMBLOCK_FLAGS_NONE, 0},
            {0, 0, ORDER_3 | MEMBLOCK_FLAGS_NONE, 0},
            {0, 0, ORDER_4 | MEMBLOCK_FLAGS_NONE, 0},
        },
        .cal_block =
        {
            {0x010000,  0xF0000,    ORDER_0 | MEMBLOCK_FLAGS_NONE, 0},
            {0x800000,  0x100000,   ORDER_1 | MEMBLOCK_FLAGS_NONE, 0},
            {0, 0, ORDER_2 | MEMBLOCK_FLAGS_NONE, 0},
            {0, 0, ORDER_3 | MEMBLOCK_FLAGS_NONE, 0},
            {0, 0, ORDER_4 | MEMBLOCK_FLAGS_NONE, 0},
        },
    },
    //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    [21] =  //2048 Silver Oak UK - Taiwan Focus (CAN)                   //0x15
    {
        .index = 21,
        .ecm_id = 0x7E0, .reponse_ecm_id = 0x7E8,
        .unlock_flags = UNLOCK_FLAG_NONE,
        .upload_unlock_id = UnlockId_Regular,
        .download_unlock_id = UnlockId_Regular,
        .checksum_id = ChecksumId_None,
        .finalizing_id = FinalId_None,
        .flags = UPLOAD_ADDR_TYPE_4B |  UPLOAD_BY_CMD_23 |   
            USE_ERASE_COMMAND | SINGLE_UNLOCK_ONLY |
            DOWNLOAD_ADDR_TYPE_4B |  DOWNLOAD_REQUEST_TYPE_4B |
            DOWNLOAD_REQUIRE_CMD_37,
        .extended_flags = ECM_FLAGS_NONE,
        .commtype = CommType_CAN, .commlevel = CommLevel_KWP2000,
        .eraseall_cmd = {EraseCmdInfoFlags_None,EraseCmdInfoCount1,{0xAA}},
        .erasecal_cmd = {EraseCmdInfoFlags_None,EraseCmdInfoCount1,{0xAA}},
        .vid_address = 0x0100c0, .pats_address = 0x01005d,
        .vin_address = ECM_DEFS_INVALID_VIN_ADDRESS,
        .testerpresent_frequency = 10,
        
        .data_offset = 0x0,
        .ecm_startingaddr = 0x0,
        
        .util_upload = {NULL,NULL,NULL},
        .util_dnload = {NULL,NULL,NULL},
        .maxuploadblocksize = 0x400, .maxdownloadblocksize = 0x400,
        .os_block_read_skipblock_info = NULL,
        .os_block_read =
        {
            {0x010000,  0x1F0000,   ORDER_0 | MEMBLOCK_FLAGS_NONE, 0},
            {0, 0, ORDER_1 | MEMBLOCK_FLAGS_NONE, 0},
            {0, 0, ORDER_2 | MEMBLOCK_FLAGS_NONE, 0},
            {0, 0, ORDER_3 | MEMBLOCK_FLAGS_NONE, 0},
            {0, 0, ORDER_4 | MEMBLOCK_FLAGS_NONE, 0},
        },
        .os_block =
        {
            {0x010000,  0x1F0000,   ORDER_0 | MEMBLOCK_FLAGS_NONE, 0},
            {0, 0, ORDER_1 | MEMBLOCK_FLAGS_NONE, 0},
            {0, 0, ORDER_2 | MEMBLOCK_FLAGS_NONE, 0},
            {0, 0, ORDER_3 | MEMBLOCK_FLAGS_NONE, 0},
            {0, 0, ORDER_4 | MEMBLOCK_FLAGS_NONE, 0},
        },
        .cal_block =
        {
            {0x010000,  0x1F0000,   ORDER_0 | MEMBLOCK_FLAGS_NONE, 0},
            {0, 0, ORDER_1 | MEMBLOCK_FLAGS_NONE, 0},
            {0, 0, ORDER_2 | MEMBLOCK_FLAGS_NONE, 0},
            {0, 0, ORDER_3 | MEMBLOCK_FLAGS_NONE, 0},
            {0, 0, ORDER_4 | MEMBLOCK_FLAGS_NONE, 0},
        },
    },
    //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    [22] =  //Seimens 2048K Green Oak (6.4L Engine) (CAN)               //0x16
    {
        .index = 22,
        .ecm_id = 0x7E0, .reponse_ecm_id = 0x7E8,
        .unlock_flags = UNLOCK_FLAG_NONE,
        .upload_unlock_id = UnlockId_64L,
        .download_unlock_id = UnlockId_64L,
        .checksum_id = ChecksumId_64L,
        .finalizing_id = FinalId_None,
        .flags = UPLOAD_ADDR_TYPE_4B | UPLOAD_BY_CMD_23 |
            USE_ERASE_COMMAND |
            DOWNLOAD_ADDR_TYPE_4B |  DOWNLOAD_REQUEST_TYPE_4B |
            DOWNLOAD_REQUIRE_CMD_37 | DOWNLOAD_ALL,
        .extended_flags = DOWNLOAD_HEALTH_CHECK_STRICT,
        .commtype = CommType_CAN, .commlevel = CommLevel_KWP2000,
        .eraseall_cmd = {EraseCmdInfoFlags_None,EraseCmdInfoCount1,{0x00}},
        .erasecal_cmd = {EraseCmdInfoFlags_None,EraseCmdInfoCount1,{0x00}},
        .vid_address = 0x0C0204,    // There is no VID block.
                                    // This is the strategy location.
        .pats_address = 0x000230,
        .vin_address = ECM_DEFS_INVALID_VIN_ADDRESS,
        .testerpresent_frequency = 0,
        
        .data_offset = 0x0,
        .ecm_startingaddr = 0x0,
        
        .util_upload = {NULL,NULL,NULL},
        .util_dnload = {(utility_info*)&utilityinfolist[0],NULL,NULL},
        .maxuploadblocksize = 0xF8,     .maxdownloadblocksize = 0x0C0,  //was 0xFE , Upload is 0xFE in upload as well
        .os_block_read_skipblock_info = NULL,
        .os_block_read =
        {
            {0x0C0000,  0x0D0000,   ORDER_0 | MEMBLOCK_FLAGS_SIMPLE_FF_APPEND, 0x20000},
            {0x400000,  0x080000,   ORDER_1 | MEMBLOCK_FLAGS_NONE, 0},
            {0x840000,  0x080000,   ORDER_2 | MEMBLOCK_FLAGS_NONE, 0},
            {0, 0, ORDER_3 | MEMBLOCK_FLAGS_NONE, 0},
            {0, 0, ORDER_4 | MEMBLOCK_FLAGS_NONE, 0},
        },
        .os_block =
        {
            {0x0C0000,  0x0F0000,   ORDER_0 | MEMBLOCK_FLAGS_NONE, 0},
            {0x400000,  0x080000,   ORDER_1 | MEMBLOCK_FLAGS_NONE, 0},
            {0x840000,  0x080000,   ORDER_2 | MEMBLOCK_FLAGS_NONE, 0},
            {0, 0, ORDER_3 | MEMBLOCK_FLAGS_NONE, 0},
            {0, 0, ORDER_4 | MEMBLOCK_FLAGS_NONE, 0},
        },
        .cal_block =
        {
            {0x0C0000,  0x0F0000,   ORDER_0 | MEMBLOCK_FLAGS_NONE, 0},
            {0x400000,  0x080000,   ORDER_1 | MEMBLOCK_FLAGS_NONE, 0},
            {0x840000,  0x080000,   ORDER_2 | MEMBLOCK_FLAGS_NONE, 0},
            {0, 0, ORDER_3 | MEMBLOCK_FLAGS_NONE, 0},
            {0, 0, ORDER_4 | MEMBLOCK_FLAGS_NONE, 0},
        },
    },
    //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    [23] =  //Seimens 2048K Green Oak (6.4L Trans) (CAN)                //0x17
    {
        .index = 23,
        .ecm_id = 0x7E1, .reponse_ecm_id = 0x7E9,
        .unlock_flags = UNLOCK_FLAG_NONE,
        .upload_unlock_id = UnlockId_Regular,
        .download_unlock_id = UnlockId_Regular,
        .checksum_id = ChecksumId_None,
        .finalizing_id = FinalId_None,
        .flags = UPLOAD_ADDR_TYPE_4B | UPLOAD_BY_CMD_23 |
            USE_ERASE_COMMAND |
            DOWNLOAD_ADDR_TYPE_4B |  DOWNLOAD_REQUEST_TYPE_4B |
            DOWNLOAD_REQUIRE_CMD_37,
        .extended_flags = ECM_FLAGS_NONE,
        .commtype = CommType_CAN, .commlevel = CommLevel_KWP2000,
        .eraseall_cmd = {EraseCmdInfoFlags_None,EraseCmdInfoCount1,{0xAA}},
        .erasecal_cmd = {EraseCmdInfoFlags_None,EraseCmdInfoCount1,{0xAA}},
        .vid_address = 0x0101C0,    // There is no VID block.
                                    // This is the strategy location. Not correct yet.
        .pats_address = 0xFF,
        .vin_address = ECM_DEFS_INVALID_VIN_ADDRESS,
        .testerpresent_frequency = 0,
        
        .data_offset = 0x0,
        .ecm_startingaddr = 0x0,
        
        .util_upload = {NULL,NULL,NULL},
        .util_dnload = {NULL,NULL,NULL},
        .maxuploadblocksize = 0xFF8, .maxdownloadblocksize = 0x400, //was 0xFFE
        .os_block_read_skipblock_info = NULL,
        .os_block_read =
        {
            {0x010000,  0x1F0000,   ORDER_0 | MEMBLOCK_FLAGS_NONE, 0},
            {0, 0, ORDER_1 | MEMBLOCK_FLAGS_NONE, 0},
            {0, 0, ORDER_2 | MEMBLOCK_FLAGS_NONE, 0},
            {0, 0, ORDER_3 | MEMBLOCK_FLAGS_NONE, 0},
            {0, 0, ORDER_4 | MEMBLOCK_FLAGS_NONE, 0},
        },
        .os_block =
        {
            {0x010000,  0x1F0000,   ORDER_0 | MEMBLOCK_FLAGS_NONE, 0},
            {0, 0, ORDER_1 | MEMBLOCK_FLAGS_NONE, 0},
            {0, 0, ORDER_2 | MEMBLOCK_FLAGS_NONE, 0},
            {0, 0, ORDER_3 | MEMBLOCK_FLAGS_NONE, 0},
            {0, 0, ORDER_4 | MEMBLOCK_FLAGS_NONE, 0},
        },
        .cal_block =
        {
            {0x010000,  0x1F0000,   ORDER_0 | MEMBLOCK_FLAGS_NONE, 0},
            {0, 0, ORDER_1 | MEMBLOCK_FLAGS_NONE, 0},
            {0, 0, ORDER_2 | MEMBLOCK_FLAGS_NONE, 0},
            {0, 0, ORDER_3 | MEMBLOCK_FLAGS_NONE, 0},
            {0, 0, ORDER_4 | MEMBLOCK_FLAGS_NONE, 0},
        },
    },
    //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    [24] =  //448K Black Oak (SCP32)                                    //0x18
    {
        .index = 24,
        .ecm_id = 0x10, .reponse_ecm_id = 0xF0,
        .unlock_flags = UNLOCK_FLAG_NONE,
        .upload_unlock_id = UnlockId_Regular,
        .download_unlock_id = UnlockId_Regular,
        .checksum_id = ChecksumId_None,
        .finalizing_id = FinalId_None,
        .flags = SIMPLE_WAKEUP_CHECK_UPLOAD | SIMPLE_WAKEUP_CHECK_DOWNLOAD | SINGLE_UNLOCK_ONLY |
            UPLOAD_ADDR_TYPE_4B | UPLOAD_BY_CMD_35_SINGLE_36 |
            USE_ERASE_COMMAND |
            DOWNLOAD_ADDR_TYPE_4B |  DOWNLOAD_REQUEST_TYPE_4B |
            DOWNLOAD_BLOCK_BACKWARD | DOWNLOAD_REQUIRE_CMD_37,
        .extended_flags = ECM_FLAGS_NONE,
        .commtype = CommType_SCP32, .commlevel = CommLevel_KWP2000,
        .eraseall_cmd = {EraseCmdInfoFlags_None,EraseCmdInfoCount1,{0x00}},
        .erasecal_cmd = {EraseCmdInfoFlags_None,EraseCmdInfoCount1,{0x00}},
        .vid_address = 0x0100c0, .pats_address = 0x01005d,
        .vin_address = ECM_DEFS_INVALID_VIN_ADDRESS,
        .testerpresent_frequency = 0,
        
        .data_offset = 0x0,
        .ecm_startingaddr = 0x0,
        
        .util_upload = {NULL,NULL,NULL},
        .util_dnload = {NULL,NULL,NULL},
        .maxuploadblocksize = 0x400, .maxdownloadblocksize = 0x400,
        .os_block_read_skipblock_info = NULL,
        .os_block_read =
        {
            {0x010000,  0x060000,   ORDER_0 | MEMBLOCK_FLAGS_NONE, 0},
            {0, 0, ORDER_1 | MEMBLOCK_FLAGS_NONE, 0},
            {0, 0, ORDER_2 | MEMBLOCK_FLAGS_NONE, 0},
            {0, 0, ORDER_3 | MEMBLOCK_FLAGS_NONE, 0},
            {0, 0, ORDER_4 | MEMBLOCK_FLAGS_NONE, 0},
        },
        .os_block =
        {
            {0x010000,  0x060000,   ORDER_0 | MEMBLOCK_FLAGS_NONE, 0},
            {0, 0, ORDER_1 | MEMBLOCK_FLAGS_NONE, 0},
            {0, 0, ORDER_2 | MEMBLOCK_FLAGS_NONE, 0},
            {0, 0, ORDER_3 | MEMBLOCK_FLAGS_NONE, 0},
            {0, 0, ORDER_4 | MEMBLOCK_FLAGS_NONE, 0},
        },
        .cal_block =
        {
            {0x010000,  0x060000,   ORDER_0 | MEMBLOCK_FLAGS_NONE, 0},
            {0, 0, ORDER_1 | MEMBLOCK_FLAGS_NONE, 0},
            {0, 0, ORDER_2 | MEMBLOCK_FLAGS_NONE, 0},
            {0, 0, ORDER_3 | MEMBLOCK_FLAGS_NONE, 0},
            {0, 0, ORDER_4 | MEMBLOCK_FLAGS_NONE, 0},
        },
    },
    //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    [25] =  //Lynx 256K (EECV diesel) (SCP)                             //0x19
    {
        .index = 25,
        .ecm_id = 0x10, .reponse_ecm_id = 0xF0,
        .unlock_flags = UNLOCK_FLAG_NONE,
        .upload_unlock_id = UnlockId_Regular,
        .download_unlock_id = UnlockId_Regular,
        .checksum_id = ChecksumId_None,
        .finalizing_id = FinalId_None,
        .flags = SIMPLE_WAKEUP_CHECK_UPLOAD | SIMPLE_WAKEUP_CHECK_DOWNLOAD | SINGLE_UNLOCK_ONLY |
            UPLOAD_ADDR_TYPE_3B | UPLOAD_BY_CMD_23 |
            USE_ERASE_COMMAND |
            DOWNLOAD_ADDR_TYPE_3B | DOWNLOAD_REQUEST_TYPE_3B |
            DOWNLOAD_BLOCK_BACKWARD | DOWNLOAD_REQUIRE_CMD_37,
        .extended_flags = ECM_FLAGS_NONE,
        .commtype = CommType_SCP, .commlevel = CommLevel_KWP2000,
        .eraseall_cmd = {EraseCmdInfoFlags_None,EraseCmdInfoCount1,{0x00}},
        .erasecal_cmd = {EraseCmdInfoFlags_None,EraseCmdInfoCount1,{0x00}},
        .vid_address = 0x1C8080, .pats_address = 0x1C801D,
        .vin_address = ECM_DEFS_INVALID_VIN_ADDRESS,
        .testerpresent_frequency = 0,
        
        .data_offset = 0x0,
        .ecm_startingaddr = 0x0,
        
        .util_upload = {NULL,NULL,NULL},
        .util_dnload = {NULL,NULL,NULL},
        .maxuploadblocksize = 0x400, .maxdownloadblocksize = 0x400,
        .os_block_read_skipblock_info = NULL,
        .os_block_read =
        {
            {0x1C8000,  0x038000,   ORDER_0 | MEMBLOCK_FLAGS_NONE, 0},
            {0, 0, ORDER_1 | MEMBLOCK_FLAGS_NONE, 0},
            {0, 0, ORDER_2 | MEMBLOCK_FLAGS_NONE, 0},
            {0, 0, ORDER_3 | MEMBLOCK_FLAGS_NONE, 0},
            {0, 0, ORDER_4 | MEMBLOCK_FLAGS_NONE, 0},
        },
        .os_block =
        {
            {0x1C8000,  0x038000,   ORDER_0 | MEMBLOCK_FLAGS_NONE, 0},
            {0, 0, ORDER_1 | MEMBLOCK_FLAGS_NONE, 0},
            {0, 0, ORDER_2 | MEMBLOCK_FLAGS_NONE, 0},
            {0, 0, ORDER_3 | MEMBLOCK_FLAGS_NONE, 0},
            {0, 0, ORDER_4 | MEMBLOCK_FLAGS_NONE, 0},
        },
        .cal_block =
        {
            {0x1C8000,  0x038000,   ORDER_0 | MEMBLOCK_FLAGS_NONE, 0},
            {0, 0, ORDER_1 | MEMBLOCK_FLAGS_NONE, 0},
            {0, 0, ORDER_2 | MEMBLOCK_FLAGS_NONE, 0},
            {0, 0, ORDER_3 | MEMBLOCK_FLAGS_NONE, 0},
            {0, 0, ORDER_4 | MEMBLOCK_FLAGS_NONE, 0},
        },
    },
    //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    [26] =  //'08 Ford Focus 2.0L (CAN) (ISO14229)                      //0x1A
            // factory .vbf says GreenOak 2432K, but Chris J. says
            // it actually is a GreenOak 2368K
    {
        .index = 26,
        .ecm_id = 0x7E0, .reponse_ecm_id = 0x7E8,
        .unlock_flags = UNLOCK_FLAG_NONE,
        .upload_unlock_id = UnlockId_Regular,    //TODOQ: check me
        .download_unlock_id = UnlockId_Regular,
        .checksum_id = ChecksumId_None,
        .finalizing_id = FinalId_None,
        .flags = UPLOAD_ADDR_TYPE_4B | UPLOAD_BY_CMD_35_MULTI_36 |
            UPLOAD_REQUIRE_CMD_37 |
            USE_ERASE_COMMAND |
            DOWNLOAD_ADDR_TYPE_4B |  DOWNLOAD_REQUEST_TYPE_4B |
            DOWNLOAD_REQUIRE_CMD_37,
        .extended_flags = ECM_FLAGS_NONE,
        .commtype = CommType_CAN, .commlevel = CommLevel_ISO14229,
        .eraseall_cmd = {EraseCmdInfoFlags_None,EraseCmdInfoCount0,{0}},
        .erasecal_cmd = {EraseCmdInfoFlags_None,EraseCmdInfoCount0,{0}},
        .vid_address = 0x0100C0, .pats_address = 0x01005D,
        .vin_address = ECM_DEFS_INVALID_VIN_ADDRESS,
        .testerpresent_frequency = 10,
        
        .data_offset = 0x0,
        .ecm_startingaddr = 0x0,
        
        .util_upload = {NULL,NULL,NULL},
        .util_dnload = {NULL,NULL,NULL},
        .maxuploadblocksize = 0x400, .maxdownloadblocksize = 0x400,
        .os_block_read_skipblock_info = NULL,
        .os_block_read =
        {
            {0x10000,   0x70000,    ORDER_0 | MEMBLOCK_FLAGS_NONE, 0},
            {0x800000,  0x1E0000,   ORDER_1 | MEMBLOCK_FLAGS_NONE, 0},
            {0, 0, ORDER_2 | MEMBLOCK_FLAGS_NONE, 0},
            {0, 0, ORDER_3 | MEMBLOCK_FLAGS_NONE, 0},
            {0, 0, ORDER_4 | MEMBLOCK_FLAGS_NONE, 0},
        },
        .os_block =
        {
            {0x10000,   0x70000,    ORDER_0 | MEMBLOCK_FLAGS_NONE, 0},
            {0x800000,  0x1E0000,   ORDER_1 | MEMBLOCK_FLAGS_NONE, 0},
            {0, 0, ORDER_2 | MEMBLOCK_FLAGS_NONE, 0},
            {0, 0, ORDER_3 | MEMBLOCK_FLAGS_NONE, 0},
            {0, 0, ORDER_4 | MEMBLOCK_FLAGS_NONE, 0},
        },
        .cal_block =
        {
            {0x10000,   0x70000,    ORDER_0 | MEMBLOCK_FLAGS_NONE, 0},
            {0x800000,  0x1E0000,   ORDER_1 | MEMBLOCK_FLAGS_NONE, 0},
            {0, 0, ORDER_2 | MEMBLOCK_FLAGS_NONE, 0},
            {0, 0, ORDER_3 | MEMBLOCK_FLAGS_NONE, 0},
            {0, 0, ORDER_4 | MEMBLOCK_FLAGS_NONE, 0},
        },
    },
    //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    [27] =  //Green Oak 2368K (CAN) (ISO14229)                          //0x1B
    {
        .index = 27,
        .ecm_id = 0x7E0, .reponse_ecm_id = 0x7E8,
        .unlock_flags = UNLOCK_FLAG_NONE,
        .upload_unlock_id = UnlockId_Regular,
        .download_unlock_id = UnlockId_Regular,
        .checksum_id = ChecksumId_None,
        .finalizing_id = FinalId_None,
        .flags = UPLOAD_ADDR_TYPE_4B | UPLOAD_BY_CMD_35_MULTI_36 |
            UPLOAD_REQUIRE_CMD_37 |
            USE_ERASE_COMMAND |
            DOWNLOAD_ADDR_TYPE_4B |  DOWNLOAD_REQUEST_TYPE_4B |
            DOWNLOAD_REQUIRE_CMD_37,
        .extended_flags = ECM_FLAGS_NONE,
        .commtype = CommType_CAN, .commlevel = CommLevel_ISO14229,
        .eraseall_cmd = {EraseCmdInfoFlags_None,EraseCmdInfoCount0,{0}},
        .erasecal_cmd = {EraseCmdInfoFlags_None,EraseCmdInfoCount0,{0}},
        .vid_address = 0x0100C0, .pats_address = 0x01005D,
        .vin_address = ECM_DEFS_INVALID_VIN_ADDRESS,
        .testerpresent_frequency = 10,
        
        .data_offset = 0x0,
        .ecm_startingaddr = 0x0,
        
        .util_upload = {NULL,NULL,NULL},
        .util_dnload = {NULL,NULL,NULL},
        .maxuploadblocksize = 0xFFC, .maxdownloadblocksize = 0x400,
        .os_block_read_skipblock_info = NULL,
        .os_block_read =
        {
            {0x10000,   0x010000,   ORDER_0 | MEMBLOCK_FLAGS_NONE, 0},
            {0x40000,   0x1C0000,   ORDER_1 | MEMBLOCK_FLAGS_NONE, 0},
            {0x400000,  0x080000,   ORDER_2 | MEMBLOCK_FLAGS_NONE, 0},
            {0, 0, ORDER_3 | MEMBLOCK_FLAGS_NONE, 0},
            {0, 0, ORDER_4 | MEMBLOCK_FLAGS_NONE, 0},
        },
        .os_block =
        {
            {0x10000,   0x010000,   ORDER_0 | MEMBLOCK_FLAGS_NONE, 0},
            {0x40000,   0x1C0000,   ORDER_1 | MEMBLOCK_FLAGS_NONE, 0},
            {0x400000,  0x080000,   ORDER_2 | MEMBLOCK_FLAGS_NONE, 0},
            {0, 0, ORDER_3 | MEMBLOCK_FLAGS_NONE, 0},
            {0, 0, ORDER_4 | MEMBLOCK_FLAGS_NONE, 0},
        },
        .cal_block =
        {
            {0x10000,   0x010000,   ORDER_0 | MEMBLOCK_FLAGS_NONE, 0},
            {0x40000,   0x1C0000,   ORDER_1 | MEMBLOCK_FLAGS_NONE, 0},
            {0x400000,  0x080000,   ORDER_2 | MEMBLOCK_FLAGS_NONE, 0},
            {0, 0, ORDER_3 | MEMBLOCK_FLAGS_NONE, 0},
            {0, 0, ORDER_4 | MEMBLOCK_FLAGS_NONE, 0},
        },
    },
    //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    [28] =  //Silver Oak 1024 - TCM (CAN)                               //0x1C
    {
        .index = 28,
        .ecm_id = 0x7E1, .reponse_ecm_id = 0x7E9,
        .unlock_flags = UNLOCK_FLAG_NONE,
        .upload_unlock_id = UnlockId_None,
        .download_unlock_id = UnlockId_Bosch6R,
        .checksum_id = ChecksumId_6R,
        .finalizing_id = FinalId_None,
        .flags = SIMPLE_WAKEUP_CHECK_UPLOAD |
            UPLOAD_ADDR_TYPE_4B | UPLOAD_BY_CMD_23 |
            USE_ERASE_COMMAND | ERASE_COMMAND_COMPLEX |
            DOWNLOAD_ADDR_TYPE_4B |  DOWNLOAD_REQUEST_TYPE_4B |
            DOWNLOAD_REQUIRE_CMD_37,
        .extended_flags = ECM_FLAGS_NONE,
        .commtype = CommType_CAN, .commlevel = CommLevel_KWP2000,
        .eraseall_cmd = {EraseCmdInfoFlags_None,EraseCmdInfoCount3,{0x01,0x02,0x03}},
        .erasecal_cmd = {EraseCmdInfoFlags_None,EraseCmdInfoCount3,{0x01,0x02,0x03}},
        .vid_address = 0x031D00,    // There is no VID block.
                                    // Strategy location is somewhere here
        .pats_address = 0xFF,
        .vin_address = ECM_DEFS_INVALID_VIN_ADDRESS,
        .testerpresent_frequency = 0,
        
        .data_offset = 0x0,
        .ecm_startingaddr = 0x0,
        
        .util_upload = {NULL,NULL,NULL},
        .util_dnload = {NULL,NULL,NULL},
        .maxuploadblocksize = 0xF8, .maxdownloadblocksize = 0xF8,   //was 0xFE & 0xFC
        .os_block_read_skipblock_info = NULL,
        .os_block_read =
        {
            {0x030000,  0x030000,   ORDER_0 | MEMBLOCK_FLAGS_NONE, 0},
            {0x060000,  0x020000,   ORDER_1 | MEMBLOCK_FLAGS_NONE, 0},
            {0x080000,  0x080000,   ORDER_2 | MEMBLOCK_FLAGS_NONE, 0},
            {0, 0, ORDER_3 | MEMBLOCK_FLAGS_NONE, 0},
            {0, 0, ORDER_4 | MEMBLOCK_FLAGS_NONE, 0},
        },
        .os_block =
        {
            {0x030000,  0x030000-0x80,  ORDER_1 | MEMBLOCK_FLAGS_NONE, 0},  //do not program last 0x80 bytes of this block
            {0x060000,  0x020000-0x80,  ORDER_2 | MEMBLOCK_FLAGS_NONE, 0},  //do not program last 0x80 bytes of this block
            {0x080000,  0x080000-0x80,  ORDER_0 | MEMBLOCK_FLAGS_NONE, 0},  //do not program last 0x80 bytes of this block
            {0, 0, ORDER_3 | MEMBLOCK_FLAGS_NONE, 0},
            {0, 0, ORDER_4 | MEMBLOCK_FLAGS_NONE, 0},
        },
        .cal_block =
        {
            {0x030000,  0x030000-0x80,  ORDER_1 | MEMBLOCK_FLAGS_NONE, 0},  //do not program last 0x80 bytes of this block
            {0x060000,  0x020000-0x80,  ORDER_2 | MEMBLOCK_FLAGS_NONE, 0},  //do not program last 0x80 bytes of this block
            {0x080000,  0x080000-0x80,  ORDER_0 | MEMBLOCK_FLAGS_NONE, 0},  //do not program last 0x80 bytes of this block
            {0, 0, ORDER_3 | MEMBLOCK_FLAGS_NONE, 0},
            {0, 0, ORDER_4 | MEMBLOCK_FLAGS_NONE, 0},
        },
    },
    //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    [29] =  //Silver Oak 2048K US (CAN)                                 //0x1D
    {
        .index = 29,
        .ecm_id = 0x7E0, .reponse_ecm_id = 0x7E8,
        .unlock_flags = UNLOCK_FLAG_NONE,
        .upload_unlock_id = UnlockId_Regular,
        .download_unlock_id = UnlockId_Regular,
        .checksum_id = ChecksumId_None,
        .finalizing_id = FinalId_None,
        .flags = UPLOAD_ADDR_TYPE_4B | UPLOAD_BY_CMD_23 |
            USE_ERASE_COMMAND |
            DOWNLOAD_ADDR_TYPE_4B |  DOWNLOAD_REQUEST_TYPE_4B |
            DOWNLOAD_REQUIRE_CMD_37,
        .extended_flags = ECM_FLAGS_NONE,
        .commtype = CommType_CAN, .commlevel = CommLevel_KWP2000,
        .eraseall_cmd = {EraseCmdInfoFlags_None,EraseCmdInfoCount1,{0xAA}},
        .erasecal_cmd = {EraseCmdInfoFlags_None,EraseCmdInfoCount1,{0xAA}},
        .vid_address = 0x0100c0, .pats_address = 0x01005d,
        .vin_address = ECM_DEFS_INVALID_VIN_ADDRESS,
        .testerpresent_frequency = 0,
        
        .data_offset = 0x0,
        .ecm_startingaddr = 0x0,
        
        .util_upload = {NULL,NULL,NULL},
        .util_dnload = {NULL,NULL,NULL},
        .maxuploadblocksize = 0x400, .maxdownloadblocksize = 0x400,
        .os_block_read_skipblock_info = NULL,
        .os_block_read =
        {
            {0x010000,  0x000800,   ORDER_0 | MEMBLOCK_FLAGS_NONE, 0},
            {0x010800,  0x02F800,   ORDER_1 | MEMBLOCK_FLAGS_PRE_FILL_WITH_FF, 0},
            {0x040000,  0x1C0000,   ORDER_2 | MEMBLOCK_FLAGS_NONE, 0},
            {0, 0, ORDER_3 | MEMBLOCK_FLAGS_NONE, 0},
            {0, 0, ORDER_4 | MEMBLOCK_FLAGS_NONE, 0},
        },
        .os_block =
        {
            {0x010000,  0x000800,   ORDER_0 | MEMBLOCK_FLAGS_NONE, 0},
            {0x010800,  0x02F800,   ORDER_1 | MEMBLOCK_FLAGS_SKIP_DOWNLOAD, 0},
            {0x040000,  0x1C0000,   ORDER_2 | MEMBLOCK_FLAGS_NONE, 0},
            {0, 0, ORDER_3 | MEMBLOCK_FLAGS_NONE, 0},
            {0, 0, ORDER_4 | MEMBLOCK_FLAGS_NONE, 0},
        },
        .cal_block =
        {
            {0x010000,  0x000800,   ORDER_0 | MEMBLOCK_FLAGS_NONE, 0},
            {0x010800,  0x02F800,   ORDER_1 | MEMBLOCK_FLAGS_SKIP_DOWNLOAD , 0},
            {0x040000,  0x1C0000,   ORDER_2 | MEMBLOCK_FLAGS_NONE, 0},
            {0, 0, ORDER_3 | MEMBLOCK_FLAGS_NONE, 0},
            {0, 0, ORDER_4 | MEMBLOCK_FLAGS_NONE, 0},
        },
    },
    //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    [30] =  //Spanish Oak 2048K ISO14229 US parameters (CAN)            //0x1E
    {
        .index = 30,
        .ecm_id = 0x7E0, .reponse_ecm_id = 0x7E8,
        .unlock_flags = UNLOCK_FLAG_NONE,
        .upload_unlock_id = UnlockId_Regular,
        .download_unlock_id = UnlockId_Regular,
        .checksum_id = ChecksumId_None,
        .finalizing_id = FinalId_None,
        .flags = UPLOAD_ADDR_TYPE_4B | UPLOAD_BY_CMD_35_MULTI_36 |
            UPLOAD_REQUIRE_CMD_37 |
            USE_ERASE_COMMAND |
            DOWNLOAD_ADDR_TYPE_4B |  DOWNLOAD_REQUEST_TYPE_4B |
            DOWNLOAD_REQUIRE_CMD_37,
        .extended_flags = ECM_FLAGS_NONE,
        .commtype = CommType_CAN, .commlevel = CommLevel_ISO14229,
        .eraseall_cmd = {EraseCmdInfoFlags_None,EraseCmdInfoCount0,{0}},
        .erasecal_cmd = {EraseCmdInfoFlags_None,EraseCmdInfoCount0,{0}},
        .vid_address = 0x0100c0, .pats_address = 0x01005d,
        .vin_address = ECM_DEFS_INVALID_VIN_ADDRESS,
        .testerpresent_frequency = 10,
        
        .data_offset = 0x0,
        .ecm_startingaddr = 0x0,
        
        .util_upload = {NULL,NULL,NULL},
        .util_dnload = {NULL,NULL,NULL},
        .maxuploadblocksize = 0x400, .maxdownloadblocksize = 0x400,
        .os_block_read_skipblock_info = NULL,
        .os_block_read =
        {
            {0x010000,  0x0F0000,   ORDER_0 | MEMBLOCK_FLAGS_NONE, 0},
            {0x800000,  0x100000,   ORDER_1 | MEMBLOCK_FLAGS_NONE, 0},
            {0, 0, ORDER_2 | MEMBLOCK_FLAGS_NONE, 0},
            {0, 0, ORDER_3 | MEMBLOCK_FLAGS_NONE, 0},
            {0, 0, ORDER_4 | MEMBLOCK_FLAGS_NONE, 0},
        },
        .os_block =
        {
            {0x010000,  0x0F0000,   ORDER_0 | MEMBLOCK_FLAGS_NONE, 0},
            {0x800000,  0x100000,   ORDER_1 | MEMBLOCK_FLAGS_NONE, 0},
            {0, 0, ORDER_2 | MEMBLOCK_FLAGS_NONE, 0},
            {0, 0, ORDER_3 | MEMBLOCK_FLAGS_NONE, 0},
            {0, 0, ORDER_4 | MEMBLOCK_FLAGS_NONE, 0},
        },
        .cal_block =
        {
            {0x010000,  0x0F0000,   ORDER_0 | MEMBLOCK_FLAGS_NONE, 0},
            {0x800000,  0x100000,   ORDER_1 | MEMBLOCK_FLAGS_NONE, 0},
            {0, 0, ORDER_2 | MEMBLOCK_FLAGS_NONE, 0},
            {0, 0, ORDER_3 | MEMBLOCK_FLAGS_NONE, 0},
            {0, 0, ORDER_4 | MEMBLOCK_FLAGS_NONE, 0},
        },
    },
    //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    [31] =  //1664k GreenOak CBP-B ISO14229 (CAN)                       //0x1F
            // 2010 Ford Focus
    {
        .index = 31,
        .ecm_id = 0x7E0, .reponse_ecm_id = 0x7E8,
        .unlock_flags = UNLOCK_FLAG_NONE,
        .upload_unlock_id = UnlockId_Regular,
        .download_unlock_id = UnlockId_Regular,
        .checksum_id = ChecksumId_None,
        .finalizing_id = FinalId_None,
        .flags = SIMPLE_WAKEUP_CHECK_UPLOAD | SIMPLE_WAKEUP_CHECK_DOWNLOAD |
            UPLOAD_ADDR_TYPE_4B | UPLOAD_BY_CMD_35_MULTI_36 |
            UPLOAD_REQUIRE_CMD_37 |
            USE_ERASE_COMMAND |
            DOWNLOAD_ADDR_TYPE_4B |  DOWNLOAD_REQUEST_TYPE_4B |
            DOWNLOAD_REQUIRE_CMD_37 | COOLDOWNTIME_SHORT,
        .extended_flags = ECM_FLAGS_NONE,
        .commtype = CommType_CAN, .commlevel = CommLevel_ISO14229,
        .eraseall_cmd = {EraseCmdInfoFlags_None,EraseCmdInfoCount0,{0}},
        .erasecal_cmd = {EraseCmdInfoFlags_None,EraseCmdInfoCount0,{0}},
        .vid_address = 0x0100c0, .pats_address = 0x01005d,
        .vin_address = ECM_DEFS_INVALID_VIN_ADDRESS,
        .testerpresent_frequency = 10,
        
        .data_offset = 0x0,
        .ecm_startingaddr = 0x0,
        
        .util_upload = {NULL,NULL,NULL},
        .util_dnload = {NULL,NULL,NULL},
        .maxuploadblocksize = 0xFFC, .maxdownloadblocksize = 0xFF8, //was 0xFF8 & 0xFFC
        .os_block_read_skipblock_info = NULL,
        .os_block_read =
        {
            {0x010000,  0x070000,   ORDER_0 | MEMBLOCK_FLAGS_NONE, 0},
            {0x810000,  0x130000,   ORDER_1 | MEMBLOCK_FLAGS_NONE, 0},
            {0, 0, ORDER_2 | MEMBLOCK_FLAGS_NONE, 0},
            {0, 0, ORDER_3 | MEMBLOCK_FLAGS_NONE, 0},
            {0, 0, ORDER_4 | MEMBLOCK_FLAGS_NONE, 0},
        },
        .os_block =
        {
            {0x010000,  0x070000,   ORDER_0 | MEMBLOCK_FLAGS_NONE, 0},
            {0x810000,  0x130000,   ORDER_1 | MEMBLOCK_FLAGS_NONE, 0},
            {0, 0, ORDER_2 | MEMBLOCK_FLAGS_NONE, 0},
            {0, 0, ORDER_3 | MEMBLOCK_FLAGS_NONE, 0},
            {0, 0, ORDER_4 | MEMBLOCK_FLAGS_NONE, 0},
        },
        .cal_block =
        {
            {0x010000,  0x070000,   ORDER_0 | MEMBLOCK_FLAGS_NONE, 0},
            {0x810000,  0x130000,   ORDER_1 | MEMBLOCK_FLAGS_NONE, 0},
            {0, 0, ORDER_2 | MEMBLOCK_FLAGS_NONE, 0},
            {0, 0, ORDER_3 | MEMBLOCK_FLAGS_NONE, 0},
            {0, 0, ORDER_4 | MEMBLOCK_FLAGS_NONE, 0},
        },
    },
    //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    [32] =  // GreenOak 1920k CBP-A1 ISO14229 (CAN)                       //0x20
            // Related Vehicles: 2009+ Ford Escape 2.5L
    {
        .index = 32,
        .ecm_id = 0x7E0, .reponse_ecm_id = 0x7E8,
        .unlock_flags = UNLOCK_FLAG_NONE,
        .upload_unlock_id = UnlockId_Regular,
        .download_unlock_id = UnlockId_Regular,
        .checksum_id = ChecksumId_None,
        .finalizing_id = FinalId_None,
        .flags = SIMPLE_WAKEUP_CHECK_UPLOAD | SIMPLE_WAKEUP_CHECK_DOWNLOAD |
            UPLOAD_ADDR_TYPE_4B | UPLOAD_BY_CMD_35_MULTI_36 |
            UPLOAD_REQUIRE_CMD_37 |
            USE_ERASE_COMMAND |
            DOWNLOAD_ADDR_TYPE_4B |  DOWNLOAD_REQUEST_TYPE_4B |
            DOWNLOAD_REQUIRE_CMD_37 | COOLDOWNTIME_SHORT,
        .extended_flags = ECM_FLAGS_NONE,
        .commtype = CommType_CAN, .commlevel = CommLevel_ISO14229,
        .eraseall_cmd = {EraseCmdInfoFlags_None,EraseCmdInfoCount0,{0}},
        .erasecal_cmd = {EraseCmdInfoFlags_None,EraseCmdInfoCount0,{0}},
        .vid_address = 0x0100C0, .pats_address = 0x0100C0,
        .vin_address = ECM_DEFS_INVALID_VIN_ADDRESS,
        .testerpresent_frequency = 10,
        
        .data_offset = 0x0,
        .ecm_startingaddr = 0x0,
        
        .util_upload = {NULL,NULL,NULL},
        .util_dnload = {NULL,NULL,NULL},
        .maxuploadblocksize = 0xFFC, .maxdownloadblocksize = 0xFFC,
        .os_block_read_skipblock_info = NULL,
        .os_block_read =
        {
            {0x010000,  0x070000,   ORDER_0 | MEMBLOCK_FLAGS_NONE, 0},
            {0x810000,  0x170000,   ORDER_1 | MEMBLOCK_FLAGS_NONE, 0},
            {0, 0, ORDER_2 | MEMBLOCK_FLAGS_NONE, 0},
            {0, 0, ORDER_3 | MEMBLOCK_FLAGS_NONE, 0},
            {0, 0, ORDER_4 | MEMBLOCK_FLAGS_NONE, 0},
        },
        .os_block =
        {
            {0x010000,  0x070000,   ORDER_0 | MEMBLOCK_FLAGS_NONE, 0},
            {0x810000,  0x170000,   ORDER_1 | MEMBLOCK_FLAGS_NONE, 0},
            {0, 0, ORDER_2 | MEMBLOCK_FLAGS_NONE, 0},
            {0, 0, ORDER_3 | MEMBLOCK_FLAGS_NONE, 0},
            {0, 0, ORDER_4 | MEMBLOCK_FLAGS_NONE, 0},
        },
        .cal_block =
        {
            {0x010008,  0x06FFF8,   ORDER_0 | MEMBLOCK_FLAGS_NONE, 0},
            {0x810000,  0x170000,   ORDER_1 | MEMBLOCK_FLAGS_NONE, 0},
            {0x010000,  0x000008,   ORDER_2 | MEMBLOCK_FLAGS_NONE, 0},
            {0, 0, ORDER_3 | MEMBLOCK_FLAGS_NONE, 0},
            {0, 0, ORDER_4 | MEMBLOCK_FLAGS_NONE, 0},
        },
    },
    //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    [33] =  // TCM ZF 6HP26 GS19.04.0 (CAN)                              //0x21
            // Related Vehicles: Austrailian Ford Vehicles
    {       
        .index = 33,
        .ecm_id = 0x7E1, .reponse_ecm_id = 0x7E9,
        .unlock_flags = UNLOCK_FLAG_NONE,
        .upload_unlock_id = UnlockId_None,
        .download_unlock_id = UnlockId_Bosch6R,
        .checksum_id = ChecksumId_AU_ZF,
        .finalizing_id = FinalId_None,
        .flags = SIMPLE_WAKEUP_CHECK_UPLOAD |
            UPLOAD_ADDR_TYPE_4B | UPLOAD_BY_CMD_23 |
            USE_ERASE_COMMAND | ERASE_COMMAND_COMPLEX |
            DOWNLOAD_ADDR_TYPE_4B | DOWNLOAD_REQUEST_TYPE_4B |
            DOWNLOAD_REQUIRE_CMD_37 | COOLDOWNTIME_SHORT,
        .extended_flags = REQUIRE_KEYOFF_BEFORE_DOWNLOAD,
        .commtype = CommType_CAN, .commlevel = CommLevel_KWP2000,
        .eraseall_cmd = {EraseCmdInfoFlags_None,EraseCmdInfoCount1,{0x01}},
        .erasecal_cmd = {EraseCmdInfoFlags_None,EraseCmdInfoCount1,{0x01}},
        .vid_address = 0x031D00,    // There is no VID block.
                                    // Strategy location is somewhere here
        .pats_address = 0xFF,
        .vin_address = ECM_DEFS_INVALID_VIN_ADDRESS,
        .testerpresent_frequency = 0,
        
        .data_offset = 0x0,
        .ecm_startingaddr = 0x0,
        
        .util_upload = {NULL,NULL,NULL},
        .util_dnload = {NULL,NULL,NULL},
        .maxuploadblocksize = 0xF8, .maxdownloadblocksize = 0xF8,
        .os_block_read_skipblock_info = NULL,
        .os_block_read =
        {
            {0x006E0080,  0x0001FF80,   ORDER_0 | MEMBLOCK_FLAGS_NONE, 0},
            {0, 0, ORDER_1 | MEMBLOCK_FLAGS_NONE, 0},
            {0, 0, ORDER_2 | MEMBLOCK_FLAGS_NONE, 0},
            {0, 0, ORDER_3 | MEMBLOCK_FLAGS_NONE, 0},
            {0, 0, ORDER_4 | MEMBLOCK_FLAGS_NONE, 0},
        },
        .os_block =
        {
            {0x006E0080,  0x0001FF80,   ORDER_0 | MEMBLOCK_FLAGS_NONE, 0},
            {0, 0, ORDER_1 | MEMBLOCK_FLAGS_NONE, 0},
            {0, 0, ORDER_2 | MEMBLOCK_FLAGS_NONE, 0},
            {0, 0, ORDER_3 | MEMBLOCK_FLAGS_NONE, 0},
            {0, 0, ORDER_4 | MEMBLOCK_FLAGS_NONE, 0},
        },
        .cal_block =
        {
            {0x006E0080,  0x0001FF80,   ORDER_0 | MEMBLOCK_FLAGS_NONE, 0},
            {0, 0, ORDER_1 | MEMBLOCK_FLAGS_NONE, 0},
            {0, 0, ORDER_2 | MEMBLOCK_FLAGS_NONE, 0},
            {0, 0, ORDER_3 | MEMBLOCK_FLAGS_NONE, 0},
            {0, 0, ORDER_4 | MEMBLOCK_FLAGS_NONE, 0},
        },
    },    
    //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    [34] =  //PRISM: eSys CBP-C2 ISO14229 (CAN)                         //0x22
    {       // Related Vehicles: 5.0L Mustang, 6.2L Gas Trucks
        .index = 34,
        .ecm_id = 0x7E0, .reponse_ecm_id = 0x7E8,
        .unlock_flags = UNLOCK_FLAG_NONE,
        .upload_unlock_id = UnlockId_Regular,
        .download_unlock_id = UnlockId_Regular,
        .checksum_id = ChecksumId_None,
        .finalizing_id = FinalId_None,
        .flags =  SIMPLE_WAKEUP_CHECK_UPLOAD | SIMPLE_WAKEUP_CHECK_DOWNLOAD |
            UPLOAD_ADDR_TYPE_4B | UPLOAD_BY_CMD_35_MULTI_36 |
            UPLOAD_REQUIRE_CMD_37 |
            USE_ERASE_COMMAND |
            DOWNLOAD_ADDR_TYPE_4B |  DOWNLOAD_REQUEST_TYPE_4B |
            DOWNLOAD_REQUIRE_CMD_37 | COOLDOWNTIME_SHORT | DOWNLOAD_ALL,
        .extended_flags = CLEAR_KAM_AFTER_DOWNLOAD | ALLOW_INDIRECT_BLANKVIN_OVERLAY |
            DOWNLOAD_HEALTH_CHECK_STRICT,
        .commtype = CommType_CAN, .commlevel = CommLevel_ISO14229,
        .eraseall_cmd = {EraseCmdInfoFlags_None,EraseCmdInfoCount0,{0}},
        .erasecal_cmd = {EraseCmdInfoFlags_None,EraseCmdInfoCount0,{0}},
        .vid_address = 0x0101c0, .pats_address = 0xFF,  //TODOQ: !?!
        .vin_address = 0x0100c0,
        .testerpresent_frequency = 10,
        
        .data_offset = 0x0,
        .ecm_startingaddr = 0x0,
        
        .util_upload = {(utility_info*)&utilityinfolist[1],NULL,NULL},
        .util_dnload = {(utility_info*)&utilityinfolist[1],NULL,NULL},
        .maxuploadblocksize = 0x100, .maxdownloadblocksize = 0x100,
        .os_block_read_skipblock_info = NULL,
        .os_block_read =
        {
            {0x010000,  0x170000,   ORDER_0 | MEMBLOCK_FLAGS_NONE, 0},
            {0x180000,  0x080000,   ORDER_1 | MEMBLOCK_FLAGS_NONE, 0},
            {0, 0, ORDER_2 | MEMBLOCK_FLAGS_NONE, 0},
            {0, 0, ORDER_3 | MEMBLOCK_FLAGS_NONE, 0},
            {0, 0, ORDER_4 | MEMBLOCK_FLAGS_NONE, 0},
        },
        .os_block =
        {
            {0x010000,  0x170000,   ORDER_0 | MEMBLOCK_FLAGS_NONE, 0},  //0x16FFF8
            {0x180000,  0x080000,   ORDER_1 | MEMBLOCK_FLAGS_NONE, 0},
            {0, 0, ORDER_2 | MEMBLOCK_FLAGS_NONE, 0},
            {0, 0, ORDER_3 | MEMBLOCK_FLAGS_NONE, 0},
            {0, 0, ORDER_4 | MEMBLOCK_FLAGS_NONE, 0},
        },
        .cal_block =
        {
            {0x010000,  0x170000,   ORDER_0 | MEMBLOCK_FLAGS_NONE, 0},
            {0x180000,  0x080000,   ORDER_1 | MEMBLOCK_FLAGS_NONE, 0},
            {0, 0, ORDER_2 | MEMBLOCK_FLAGS_NONE, 0},
            {0, 0, ORDER_3 | MEMBLOCK_FLAGS_NONE, 0},
            {0, 0, ORDER_4 | MEMBLOCK_FLAGS_NONE, 0},
        },
    },
    //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    [35] =  // Bosch - Diesel (A) EDC17 CP05 2870k                        //0x23
    {       // Related Vehicles: 6.7L Superduty Trucks
        .index = 35,
        .ecm_id = 0x7E0, .reponse_ecm_id = 0x7E8,
        .unlock_flags = UNLOCK_FLAG_NONE,
        .upload_unlock_id = UnlockId_67L,
        .download_unlock_id = UnlockId_67L,
        .checksum_id = ChecksumId_67L,
        .finalizing_id = FinalId_None,
        .flags =  SIMPLE_WAKEUP_CHECK_UPLOAD | SIMPLE_WAKEUP_CHECK_DOWNLOAD |
            UPLOAD_ADDR_TYPE_4B | UPLOAD_BY_CMD_35_MULTI_36 |
            UPLOAD_REQUIRE_CMD_37 | USE_ERASE_COMMAND |
            DOWNLOAD_ADDR_TYPE_4B |  DOWNLOAD_REQUEST_TYPE_4B |
            DOWNLOAD_REQUIRE_CMD_37 | FORCE_FULL_STOCK_FILE_BEFORE_FLASH |
            DOWNLOAD_ALL,
        .extended_flags = LITTLE_ENDIAN | TUNEOSDATA_REQUIRED |
            DOWNLOAD_HEALTH_CHECK_STRICT,
        .commtype = CommType_CAN, .commlevel = CommLevel_ISO14229,
        .eraseall_cmd = {EraseCmdInfoFlags_None,EraseCmdInfoCount0,{0}},
        .erasecal_cmd = {EraseCmdInfoFlags_None,EraseCmdInfoCount0,{0}},
        .vid_address = 0x809F0080, .pats_address = 0xFF,  //TODOQ: !?!
        .vin_address = ECM_DEFS_INVALID_VIN_ADDRESS,
        .testerpresent_frequency = 10,
        
        .data_offset = 0x0,
        .ecm_startingaddr = 0x0,
        
        .util_upload = {(utility_info*)&utilityinfolist[2],NULL,NULL},
        .util_dnload = {(utility_info*)&utilityinfolist[7],(utility_info*)&utilityinfolist[2],NULL},
        .maxuploadblocksize = 0x400, .maxdownloadblocksize = 0x400,
        .os_block_read_skipblock_info = NULL,
        .os_block_read =
        {
            {0x80000000, 0x00010000,   ORDER_0 | MEMBLOCK_FLAGS_NONE, 0},
            {0x80020000, 0x001e0000,   ORDER_1 | MEMBLOCK_FLAGS_NONE, 0},
            {0x80820000, 0x000e0000, ORDER_2 | MEMBLOCK_FLAGS_NONE, 0},
            {0x809f0000, 0x00010000, ORDER_3 | MEMBLOCK_FLAGS_NONE, 0},
            {0, 0, ORDER_4 | MEMBLOCK_FLAGS_NONE, 0},
        },
        .os_block =
        {
            {0x80000000, 0x00010000,   ORDER_0 | MEMBLOCK_FLAGS_NONE, 0},
            {0x80020000, 0x001e0000,   ORDER_1 | MEMBLOCK_FLAGS_NONE, 0},
            {0x80820000, 0x000e0000, ORDER_2 | MEMBLOCK_FLAGS_NONE, 0},
            {0x809f0000, 0x00010000, ORDER_3 | MEMBLOCK_FLAGS_NONE, 0},
            {0, 0, ORDER_4 | MEMBLOCK_FLAGS_NONE, 0},
        },
        .cal_block =
        {
            {0x80000000, 0x00010000,   ORDER_0 | MEMBLOCK_FLAGS_NONE, 0},
            {0x80020000, 0x001e0000,   ORDER_1 | MEMBLOCK_FLAGS_NONE, 0},
            {0, 0, ORDER_2 | MEMBLOCK_FLAGS_NONE, 0},
            {0, 0, ORDER_3 | MEMBLOCK_FLAGS_NONE, 0},
            {0, 0, ORDER_4 | MEMBLOCK_FLAGS_NONE, 0},
        },
    },
    //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    [36] =  // PRISM: eSys CBP-A2 ISO14229 (CAN)                          //0x24
            // Related Vehicles: 3.7L Mustang
    {
        .index = 36,
        .ecm_id = 0x7E0, .reponse_ecm_id = 0x7E8,
        .unlock_flags = UNLOCK_FLAG_NONE,
        .upload_unlock_id = UnlockId_Regular,
        .download_unlock_id = UnlockId_Regular,
        .checksum_id = ChecksumId_None,
        .finalizing_id = FinalId_None,
        .flags =  SIMPLE_WAKEUP_CHECK_UPLOAD | SIMPLE_WAKEUP_CHECK_DOWNLOAD |
            UPLOAD_ADDR_TYPE_4B | UPLOAD_BY_CMD_35_MULTI_36 |
            UPLOAD_REQUIRE_CMD_37 |
            USE_ERASE_COMMAND |
            DOWNLOAD_ADDR_TYPE_4B |  DOWNLOAD_REQUEST_TYPE_4B |
            DOWNLOAD_REQUIRE_CMD_37 | COOLDOWNTIME_SHORT,
        .extended_flags = CLEAR_KAM_AFTER_DOWNLOAD | ALLOW_INDIRECT_BLANKVIN_OVERLAY,
        .commtype = CommType_CAN, .commlevel = CommLevel_ISO14229,
        .eraseall_cmd = {EraseCmdInfoFlags_None,EraseCmdInfoCount0,{0}},
        .erasecal_cmd = {EraseCmdInfoFlags_None,EraseCmdInfoCount0,{0}},
        .vid_address = 0x0101c0, .pats_address = 0xFF,  //TODOQ: !?!
        .vin_address = ECM_DEFS_INVALID_VIN_ADDRESS,    //0x0100C0,
        .testerpresent_frequency = 10,
        
        .data_offset = 0x0,
        .ecm_startingaddr = 0x0,
        
        .util_upload = {(utility_info*)&utilityinfolist[3],NULL,NULL},
        .util_dnload = {(utility_info*)&utilityinfolist[3],NULL,NULL},
        .maxuploadblocksize = 0x400, .maxdownloadblocksize = 0x400,
        .os_block_read_skipblock_info = NULL,
        .os_block_read =
        {
            {0x010000,  0x170000,   ORDER_0 | MEMBLOCK_FLAGS_NONE, 0},
            {0x180000,  0x080000,   ORDER_1 | MEMBLOCK_FLAGS_NONE, 0},
            {0, 0, ORDER_2 | MEMBLOCK_FLAGS_NONE, 0},
            {0, 0, ORDER_3 | MEMBLOCK_FLAGS_NONE, 0},
            {0, 0, ORDER_4 | MEMBLOCK_FLAGS_NONE, 0},
        },
        .os_block =
        {
            {0x010000,  0x170000,   ORDER_0 | MEMBLOCK_FLAGS_NONE, 0},
            {0x180000,  0x080000,   ORDER_1 | MEMBLOCK_FLAGS_NONE, 0},
            {0, 0, ORDER_2 | MEMBLOCK_FLAGS_NONE, 0},
            {0, 0, ORDER_3 | MEMBLOCK_FLAGS_NONE, 0},
            {0, 0, ORDER_4 | MEMBLOCK_FLAGS_NONE, 0},
        },
        .cal_block =
        {
            {0x010000,  0x170000,   ORDER_0 | MEMBLOCK_FLAGS_NONE, 0},
            {0x180000,  0x080000,   ORDER_1 | MEMBLOCK_FLAGS_NONE, 0},
            {0, 0, ORDER_2 | MEMBLOCK_FLAGS_NONE, 0},
            {0, 0, ORDER_3 | MEMBLOCK_FLAGS_NONE, 0},
            {0, 0, ORDER_4 | MEMBLOCK_FLAGS_NONE, 0},
        },
    },
    //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    [37] =  // PRISM: Tricore UTCU2 ISO VBF 2048K 384K CAL               //0x25
            // Related Vehicles: 6.7L Superduty Trucks (TCM)
    {
        .index = 37,
        .ecm_id = 0x7E1, .reponse_ecm_id = 0x7E9,
        .unlock_flags = UNLOCK_FLAG_NONE,
        .upload_unlock_id = UnlockId_Regular,
        .download_unlock_id = UnlockId_Regular,
        .checksum_id = ChecksumId_None,
        .finalizing_id = FinalId_SendControlRoutine31,
        .flags = USE_ERASE_COMMAND | DOWNLOAD_ADDR_TYPE_4B |  
            DOWNLOAD_REQUEST_TYPE_4B | DOWNLOAD_REQUIRE_CMD_37,
        .extended_flags = REQUIRE_DOWNLOAD_FINALIZING | LITTLE_ENDIAN | 
            SPF_REQUIRED,
        .commtype = CommType_CAN, .commlevel = CommLevel_ISO14229,
        .eraseall_cmd = {EraseCmdInfoFlags_None,EraseCmdInfoCount0,{0}},
        .erasecal_cmd = {EraseCmdInfoFlags_None,EraseCmdInfoCount0,{0}},
        .vid_address = 0x809F0080, .pats_address = 0xFF,  //TODOQ: !?!
        .vin_address = ECM_DEFS_INVALID_VIN_ADDRESS,
        .testerpresent_frequency = 10,
        
        .data_offset = 0x0,
        .ecm_startingaddr = 0x0,
        
        .util_upload = {(utility_info*)&utilityinfolist[4],NULL,NULL},
        .util_dnload = {(utility_info*)&utilityinfolist[4],NULL,NULL},
        .maxuploadblocksize = 0xFC, .maxdownloadblocksize = 0xFC,
        .os_block_read_skipblock_info = NULL,
        .os_block_read =
        {
            {0x80080000,  0x000F7E00,   ORDER_0 | MEMBLOCK_FLAGS_NONE, 0},
            {0xA0020000,  0x0005FE00,   ORDER_1 | MEMBLOCK_FLAGS_NONE, 0},
            {0, 0, ORDER_2 | MEMBLOCK_FLAGS_NONE, 0},
            {0, 0, ORDER_3 | MEMBLOCK_FLAGS_NONE, 0},
            {0, 0, ORDER_4 | MEMBLOCK_FLAGS_NONE, 0},
        },
        .os_block =
        {
            {0x80080000,  0x000F7E00,   ORDER_0 | MEMBLOCK_FLAGS_NONE, 0},
            {0xA0020000,  0x0005FE00,   ORDER_1 | MEMBLOCK_FLAGS_NONE, 0},
            {0, 0, ORDER_2 | MEMBLOCK_FLAGS_NONE, 0},
            {0, 0, ORDER_3 | MEMBLOCK_FLAGS_NONE, 0},
            {0, 0, ORDER_4 | MEMBLOCK_FLAGS_NONE, 0},
        },
        .cal_block =
        {
            {0x80080000,  0x000F7E00,   ORDER_0 | MEMBLOCK_FLAGS_NONE, 0},
            {0xA0020000,  0x0005FE00,   ORDER_1 | MEMBLOCK_FLAGS_NONE, 0},
            {0, 0, ORDER_2 | MEMBLOCK_FLAGS_NONE, 0},
            {0, 0, ORDER_3 | MEMBLOCK_FLAGS_NONE, 0},
            {0, 0, ORDER_4 | MEMBLOCK_FLAGS_NONE, 0},
        },
    },  
    //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    [38] =  // Contiental Tricore PCM (Need offical ford name)           //0x26
            // Related Vehicles: 2011 Fiesta (PCM)
    {
        .index = 38,
        .ecm_id = 0x7E0, .reponse_ecm_id = 0x7E8,
        .unlock_flags = UNLOCK_FLAG_NONE,
        .upload_unlock_id = UnlockId_Conti,
        .download_unlock_id = UnlockId_Conti,
        .checksum_id = ChecksumId_Conti,
        .finalizing_id = FinalId_SendControlRoutine31,
        .flags = SIMPLE_WAKEUP_CHECK_UPLOAD | SIMPLE_WAKEUP_CHECK_DOWNLOAD |
            UPLOAD_ADDR_TYPE_4B | UPLOAD_BY_CMD_35_MULTI_36 |
            UPLOAD_REQUIRE_CMD_37 |
            USE_ERASE_COMMAND |
            DOWNLOAD_ADDR_TYPE_4B |  DOWNLOAD_REQUEST_TYPE_4B |
            DOWNLOAD_REQUIRE_CMD_37,
        .extended_flags = REQUIRE_DOWNLOAD_FINALIZING | LITTLE_ENDIAN,
        .commtype = CommType_CAN, .commlevel = CommLevel_ISO14229,
        .eraseall_cmd = {EraseCmdInfoFlags_None,EraseCmdInfoCount0,{0}},
        .erasecal_cmd = {EraseCmdInfoFlags_None,EraseCmdInfoCount0,{0}},
        .vid_address = 0xa004028E, .pats_address = 0xFF,  //TODOQ: !?!
        .vin_address = ECM_DEFS_INVALID_VIN_ADDRESS,
        .testerpresent_frequency = 10,
        
        .data_offset = 0x0,
        .ecm_startingaddr = 0x0,

        .util_upload = {(utility_info*)&utilityinfolist[5],NULL,NULL},
        .util_dnload = {(utility_info*)&utilityinfolist[5],NULL,NULL},
        .maxuploadblocksize = 0x400, .maxdownloadblocksize = 0x400,
        .os_block_read_skipblock_info = NULL,
        .os_block_read =
        {
            {0x80020000, 0x00020000, ORDER_0 | MEMBLOCK_FLAGS_NONE, 0},
            {0x80080000, 0x000f7e00, ORDER_1 | MEMBLOCK_FLAGS_NONE, 0},
            {0xa0040000, 0x0003df00, ORDER_2 | MEMBLOCK_FLAGS_NONE, 0},
            {0xa007fc00, 0x00000200, ORDER_3 | MEMBLOCK_FLAGS_NONE, 0},
            {0, 0, ORDER_4 | MEMBLOCK_FLAGS_NONE, 0},
        },
        .os_block =
        {
            {0x80020000, 0x00020000, ORDER_0 | MEMBLOCK_FLAGS_NONE, 0},
            {0x80080000, 0x000f7e00, ORDER_1 | MEMBLOCK_FLAGS_NONE, 0},
            {0xa0040000, 0x0003df00, ORDER_2 | MEMBLOCK_FLAGS_NONE, 0},
            {0xa007fc00, 0x00000200, ORDER_3 | MEMBLOCK_FLAGS_NONE, 0},
            {0, 0, ORDER_4 | MEMBLOCK_FLAGS_NONE, 0},
        },
        .cal_block =
        {
            {0x80020000, 0x00020000, ORDER_0 | MEMBLOCK_FLAGS_NONE, 0},
            {0x80080000, 0x000f7e00, ORDER_1 | MEMBLOCK_FLAGS_NONE, 0},
            {0xa0040000, 0x0003df00, ORDER_2 | MEMBLOCK_FLAGS_NONE, 0},
            {0xa007fc00, 0x00000200, ORDER_3 | MEMBLOCK_FLAGS_NONE, 0},
            {0, 0, ORDER_4 | MEMBLOCK_FLAGS_NONE, 0},
        },
    },
    //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    [39] =  // PRISM: Tricore DPS6 ISO VBF 2048K                        //0x27
            // Related Vehicles: 2011 Fiesta, 2012 Focus (TCM)
    {
        .index = 39,
        .ecm_id = 0x7E1, .reponse_ecm_id = 0x7E9,
        .unlock_flags = UNLOCK_FLAG_NONE,
        .upload_unlock_id = UnlockId_Siemens_UTCU,
        .download_unlock_id = UnlockId_Siemens_UTCU,
        .checksum_id = ChecksumId_None,
        .finalizing_id = FinalId_SendControlRoutine31,
        .flags = UPLOAD_ADDR_TYPE_4B | UPLOAD_BY_CMD_35_MULTI_36 | 
            UPLOAD_REQUIRE_CMD_37 | 
            USE_ERASE_COMMAND |
            DOWNLOAD_ADDR_TYPE_4B | DOWNLOAD_REQUEST_TYPE_4B |
            DOWNLOAD_REQUIRE_CMD_37,
        .extended_flags = REQUIRE_DOWNLOAD_FINALIZING | LITTLE_ENDIAN,
        .commtype = CommType_CAN, .commlevel = CommLevel_ISO14229,
        .eraseall_cmd = {EraseCmdInfoFlags_None,EraseCmdInfoCount0,{0}},
        .erasecal_cmd = {EraseCmdInfoFlags_None,EraseCmdInfoCount0,{0}},
        .vid_address = 0x02D0080, .pats_address = 0xFF,  //TODOQ: !?!
        .vin_address = ECM_DEFS_INVALID_VIN_ADDRESS,
        .testerpresent_frequency = 10,
        
        .data_offset = 0x0,
        .ecm_startingaddr = 0x0,
        
        .util_upload = {(utility_info*)&utilityinfolist[6],NULL,NULL},
        .util_dnload = {(utility_info*)&utilityinfolist[6],NULL,NULL},
        .maxuploadblocksize = 0x300, .maxdownloadblocksize = 0xF0,
        .os_block_read_skipblock_info = NULL,
        .os_block_read =
        {
            {0x80080000, 0x000F7E00, ORDER_0 | MEMBLOCK_FLAGS_NONE, 0},
            {0xA0040000, 0x0003FE00, ORDER_1 | MEMBLOCK_FLAGS_NONE, 0},
            {0, 0, ORDER_2 | MEMBLOCK_FLAGS_NONE, 0},
            {0, 0, ORDER_3 | MEMBLOCK_FLAGS_NONE, 0},
            {0, 0, ORDER_4 | MEMBLOCK_FLAGS_NONE, 0},
        },
        .os_block =
        {
            {0x80080000, 0x000F7E00, ORDER_0 | MEMBLOCK_FLAGS_NONE, 0},
            {0xA0040000, 0x0003FE00, ORDER_1 | MEMBLOCK_FLAGS_NONE, 0},
            {0, 0, ORDER_2 | MEMBLOCK_FLAGS_NONE, 0},
            {0, 0, ORDER_3 | MEMBLOCK_FLAGS_NONE, 0},
            {0, 0, ORDER_4 | MEMBLOCK_FLAGS_NONE, 0},
        },
        .cal_block =
        {
            {0x80080000, 0x000F7E00, ORDER_0 | MEMBLOCK_FLAGS_NONE, 0},
            {0xA0040000, 0x0003FE00, ORDER_1 | MEMBLOCK_FLAGS_NONE, 0},
            {0, 0, ORDER_2 | MEMBLOCK_FLAGS_NONE, 0},
            {0, 0, ORDER_3 | MEMBLOCK_FLAGS_NONE, 0},
            {0, 0, ORDER_4 | MEMBLOCK_FLAGS_NONE, 0},
        },
    },
    //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    [41] =  // PRISM: Tricore 1797 MEDG17.0 Bosch Gas GTDI ISO VBF      //0x29
        // Related Vehicles: 2012+ F150 Ecoboost 3.5L & 2015+ 2.7L, 2013 Escape, 2015+ Mustang 2.3L Ecoboost
    {
        .index = 41,
        .ecm_id = 0x7E0, .reponse_ecm_id = 0x7E8,
        .unlock_flags = UNLOCK_FLAG_NONE,
        .upload_unlock_id = UnlockId_Regular,
        .download_unlock_id = UnlockId_Regular,
        .checksum_id = ChecksumId_BoschTricore,
        .finalizing_id = FinalId_SendControlRoutine31,
        .flags = SIMPLE_WAKEUP_CHECK_UPLOAD | SIMPLE_WAKEUP_CHECK_DOWNLOAD |
            USE_ERASE_COMMAND |
            DOWNLOAD_ADDR_TYPE_4B |  DOWNLOAD_REQUEST_TYPE_4B |
            DOWNLOAD_REQUIRE_CMD_37 | DOWNLOAD_ALL,
        .extended_flags = REQUIRE_DOWNLOAD_FINALIZING | LITTLE_ENDIAN | 
            SPF_REQUIRED,
        .commtype = CommType_CAN, .commlevel = CommLevel_ISO14229,
        .eraseall_cmd = {EraseCmdInfoFlags_None,EraseCmdInfoCount0,{0}},
        .erasecal_cmd = {EraseCmdInfoFlags_None,EraseCmdInfoCount0,{0}},
        .vid_address = 0x803FFC84, .pats_address = 0xFF,  //TODOQ: !?!
        .vin_address = ECM_DEFS_INVALID_VIN_ADDRESS,
        .testerpresent_frequency = 10,
        
        .data_offset = 0x0,
        .ecm_startingaddr = 0x0,

        .util_upload = {(utility_info*)&utilityinfolist[8],NULL,NULL},
        .util_dnload = {(utility_info*)&utilityinfolist[8],NULL,NULL},
        .maxuploadblocksize = 0x400, .maxdownloadblocksize = 0x400,
        .os_block_read_skipblock_info = NULL,
        .os_block_read =
        {
            {0x80020000, 0x00120000, ORDER_0 | MEMBLOCK_FLAGS_NONE, 0},
            {0x80140000, 0x000C0000, ORDER_1 | MEMBLOCK_FLAGS_NONE, 0},
            {0x80220000, 0x001E0000, ORDER_2 | MEMBLOCK_FLAGS_NONE, 0},
            {0, 0, ORDER_3 | MEMBLOCK_FLAGS_NONE, 0},
            {0, 0, ORDER_4 | MEMBLOCK_FLAGS_NONE, 0},
        },
        .os_block =
        {
            {0x80020000, 0x00120000, ORDER_0 | MEMBLOCK_FLAGS_NONE, 0},
            {0x80140000, 0x000C0000, ORDER_1 | MEMBLOCK_FLAGS_NONE, 0},
            {0x80220000, 0x001E0000, ORDER_2 | MEMBLOCK_FLAGS_NONE, 0},
            {0, 0, ORDER_3 | MEMBLOCK_FLAGS_NONE, 0},
            {0, 0, ORDER_4 | MEMBLOCK_FLAGS_NONE, 0},
        },
        .cal_block =
        {
            {0x80020000, 0x00120000, ORDER_0 | MEMBLOCK_FLAGS_NONE, 0},
            {0x80140000, 0x000C0000, ORDER_1 | MEMBLOCK_FLAGS_NONE, 0},
            {0x80220000, 0x001E0000, ORDER_2 | MEMBLOCK_FLAGS_NONE, 0},
            {0, 0, ORDER_3 | MEMBLOCK_FLAGS_NONE, 0},
            {0, 0, ORDER_4 | MEMBLOCK_FLAGS_NONE, 0},
        },
    },
    //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    [43] =  // PRISM: Tricore 1767 MED17.0 Bosch Gas DI ISO VBF          //0x2B
            // Related Vehicles: 2012 2.0L Focus (PCM)
    {
        .index = 43,
        .ecm_id = 0x7E0, .reponse_ecm_id = 0x7E8,
        .unlock_flags = UNLOCK_FLAG_NONE,
        .upload_unlock_id = UnlockId_Regular,
        .download_unlock_id = UnlockId_Regular,
        .checksum_id = ChecksumId_BoschTricore,
        .finalizing_id = FinalId_SendControlRoutine31,
        .flags = SIMPLE_WAKEUP_CHECK_UPLOAD | SIMPLE_WAKEUP_CHECK_DOWNLOAD |
           USE_ERASE_COMMAND | DOWNLOAD_ADDR_TYPE_4B |  DOWNLOAD_REQUEST_TYPE_4B |
            DOWNLOAD_REQUIRE_CMD_37 | DOWNLOAD_ALL,
        .extended_flags = REQUIRE_DOWNLOAD_FINALIZING | LITTLE_ENDIAN | 
            SPF_REQUIRED,
        .commtype = CommType_CAN, .commlevel = CommLevel_ISO14229,
        .eraseall_cmd = {EraseCmdInfoFlags_None,EraseCmdInfoCount0,{0}},
        .erasecal_cmd = {EraseCmdInfoFlags_None,EraseCmdInfoCount0,{0}},
        .vid_address = 0x801FFF80, .pats_address = 0xFF,  //TODOQ: !?!
        .vin_address = ECM_DEFS_INVALID_VIN_ADDRESS,
        .testerpresent_frequency = 10,
        
        .data_offset = 0x0,
        .ecm_startingaddr = 0x0,

        .util_upload = {(utility_info*)&utilityinfolist[9],NULL,NULL},
        .util_dnload = {(utility_info*)&utilityinfolist[9],NULL,NULL},
        .maxuploadblocksize = 0x400, .maxdownloadblocksize = 0x400,
        .os_block_read_skipblock_info = NULL,
        .os_block_read =
        {
            {0x80020000, 0x00060000, ORDER_0 | MEMBLOCK_FLAGS_NONE, 0},
            {0x80080000, 0x00180000, ORDER_1 | MEMBLOCK_FLAGS_NONE, 0},
            {0, 0, ORDER_2 | MEMBLOCK_FLAGS_NONE, 0},
            {0, 0, ORDER_3 | MEMBLOCK_FLAGS_NONE, 0},
            {0, 0, ORDER_4 | MEMBLOCK_FLAGS_NONE, 0},
        },
        .os_block =
        {
            {0x80020000, 0x00060000, ORDER_0 | MEMBLOCK_FLAGS_NONE, 0},
            {0x80080000, 0x00180000, ORDER_1 | MEMBLOCK_FLAGS_NONE, 0},
            {0, 0, ORDER_2 | MEMBLOCK_FLAGS_NONE, 0},
            {0, 0, ORDER_3 | MEMBLOCK_FLAGS_NONE, 0},
            {0, 0, ORDER_4 | MEMBLOCK_FLAGS_NONE, 0},
        },
        .cal_block =
        {
            {0x80020000, 0x00060000, ORDER_0 | MEMBLOCK_FLAGS_NONE, 0},
            {0x80080000, 0x00180000, ORDER_1 | MEMBLOCK_FLAGS_NONE, 0},
            {0, 0, ORDER_2 | MEMBLOCK_FLAGS_NONE, 0},
            {0, 0, ORDER_3 | MEMBLOCK_FLAGS_NONE, 0},
            {0, 0, ORDER_4 | MEMBLOCK_FLAGS_NONE, 0},
        },
    },
    //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    [44] =  // PRISM: DPS6 ISO VBF 2048K EXTENDED 128K                   //0x2C
            // Related Vehicles: 2012 2.0L Focus (TCM)
    {
        .index = 44,
        .ecm_id = 0x7E1, .reponse_ecm_id = 0x7E9,
        .unlock_flags = UNLOCK_FLAG_NONE,
        .upload_unlock_id = UnlockId_Siemens_UTCU,
        .download_unlock_id = UnlockId_Siemens_UTCU,
        .checksum_id = ChecksumId_None,
        .finalizing_id = FinalId_SendControlRoutine31,
        .flags = UPLOAD_ADDR_TYPE_4B | UPLOAD_BY_CMD_35_MULTI_36 | 
            UPLOAD_REQUIRE_CMD_37 |
            USE_ERASE_COMMAND | 
            DOWNLOAD_ADDR_TYPE_4B | DOWNLOAD_REQUEST_TYPE_4B | 
            DOWNLOAD_REQUIRE_CMD_37,
        .extended_flags = REQUIRE_DOWNLOAD_FINALIZING | LITTLE_ENDIAN | 
            SPF_REQUIRED,
        .commtype = CommType_CAN, .commlevel = CommLevel_ISO14229,
        .eraseall_cmd = {EraseCmdInfoFlags_None,EraseCmdInfoCount0,{0}},
        .erasecal_cmd = {EraseCmdInfoFlags_None,EraseCmdInfoCount0,{0}},
        .vid_address = 0x801FFF80, .pats_address = 0xFF,  //TODOQ: !?!
        .vin_address = ECM_DEFS_INVALID_VIN_ADDRESS,
        .testerpresent_frequency = 10,
        
        .data_offset = 0x0,
        .ecm_startingaddr = 0x0,
        
        .util_upload = {(utility_info*)&utilityinfolist[11],NULL,NULL},
        .util_dnload = {(utility_info*)&utilityinfolist[11],NULL,NULL},
        .maxuploadblocksize = 0x300, .maxdownloadblocksize = 0x300,
        .os_block_read_skipblock_info = NULL,
        .os_block_read =
        {
            {0x80020000, 0x00020000, ORDER_0 | MEMBLOCK_FLAGS_NONE, 0},
            {0x80080000, 0x000F7E00, ORDER_1 | MEMBLOCK_FLAGS_NONE, 0},
            {0xA0040000, 0x0003FE00, ORDER_2 | MEMBLOCK_FLAGS_NONE, 0},
            {0, 0, ORDER_3 | MEMBLOCK_FLAGS_NONE, 0},
            {0, 0, ORDER_4 | MEMBLOCK_FLAGS_NONE, 0},
        },
        .os_block =
        {
            {0x80020000, 0x00020000, ORDER_0 | MEMBLOCK_FLAGS_NONE, 0},
            {0x80080000, 0x000F7E00, ORDER_1 | MEMBLOCK_FLAGS_NONE, 0},
            {0xA0040000, 0x0003FE00, ORDER_2 | MEMBLOCK_FLAGS_NONE, 0},
            {0, 0, ORDER_3 | MEMBLOCK_FLAGS_NONE, 0},
            {0, 0, ORDER_4 | MEMBLOCK_FLAGS_NONE, 0},
        },
        .cal_block =
        {
            {0x80020000, 0x00020000, ORDER_0 | MEMBLOCK_FLAGS_NONE, 0},
            {0x80080000, 0x000F7E00, ORDER_1 | MEMBLOCK_FLAGS_NONE, 0},
            {0xA0040000, 0x0003FE00, ORDER_2 | MEMBLOCK_FLAGS_NONE, 0},
            {0, 0, ORDER_3 | MEMBLOCK_FLAGS_NONE, 0},
            {0, 0, ORDER_4 | MEMBLOCK_FLAGS_NONE, 0},
        },
    },
    //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    [45] =  // Contiental Tricore PCM (Need offical ford name)           //0x2D
            // Related Vehicles: 2011 Fiesta (PCM)
    {
        .index = 45,
        .ecm_id = 0x7E0, .reponse_ecm_id = 0x7E8,
        .unlock_flags = UNLOCK_FLAG_NONE,
        .upload_unlock_id = UnlockId_Conti,
        .download_unlock_id = UnlockId_Conti,
        .checksum_id = ChecksumId_Conti,
        .finalizing_id = FinalId_SendControlRoutine31,
        .flags = SIMPLE_WAKEUP_CHECK_UPLOAD | SIMPLE_WAKEUP_CHECK_DOWNLOAD |
            UPLOAD_ADDR_TYPE_4B | UPLOAD_BY_CMD_35_MULTI_36 |
            UPLOAD_REQUIRE_CMD_37 |
            USE_ERASE_COMMAND |
            DOWNLOAD_ADDR_TYPE_4B |  DOWNLOAD_REQUEST_TYPE_4B |
            DOWNLOAD_REQUIRE_CMD_37,
        .extended_flags = REQUIRE_DOWNLOAD_FINALIZING | LITTLE_ENDIAN,
        .commtype = CommType_CAN, .commlevel = CommLevel_ISO14229,
        .eraseall_cmd = {EraseCmdInfoFlags_None,EraseCmdInfoCount0,{0}},
        .erasecal_cmd = {EraseCmdInfoFlags_None,EraseCmdInfoCount0,{0}},
        .vid_address = 0xa004028E, .pats_address = 0xFF,  //TODOQ: !?!
        .vin_address = ECM_DEFS_INVALID_VIN_ADDRESS,
        .testerpresent_frequency = 10,
        
        .data_offset = 0x0,
        .ecm_startingaddr = 0x0,

        .util_upload = {(utility_info*)&utilityinfolist[10],NULL,NULL},
        .util_dnload = {(utility_info*)&utilityinfolist[10],NULL,NULL},
        .maxuploadblocksize = 0x400, .maxdownloadblocksize = 0x400,
        .os_block_read_skipblock_info = NULL,
        .os_block_read =
        {
            {0xA007FDFC, 0x00000008, ORDER_0 | MEMBLOCK_FLAGS_NONE, 0},
            {0, 0, ORDER_1 | MEMBLOCK_FLAGS_NONE, 0},
            {0, 0, ORDER_2 | MEMBLOCK_FLAGS_NONE, 0},
            {0, 0, ORDER_3 | MEMBLOCK_FLAGS_NONE, 0},
            {0, 0, ORDER_4 | MEMBLOCK_FLAGS_NONE, 0},
        },
        .os_block =
        {
            {0xA007FDFC, 0x00000008, ORDER_0 | MEMBLOCK_FLAGS_NONE, 0},
            {0, 0, ORDER_1 | MEMBLOCK_FLAGS_NONE, 0},
            {0, 0, ORDER_2 | MEMBLOCK_FLAGS_NONE, 0},
            {0, 0, ORDER_3 | MEMBLOCK_FLAGS_NONE, 0},
            {0, 0, ORDER_4 | MEMBLOCK_FLAGS_NONE, 0},
        },
        .cal_block =
        {
            {0xA007FDFC, 0x00000008, ORDER_0 | MEMBLOCK_FLAGS_NONE, 0},
            {0, 0, ORDER_1 | MEMBLOCK_FLAGS_NONE, 0},
            {0, 0, ORDER_2 | MEMBLOCK_FLAGS_NONE, 0},
            {0, 0, ORDER_3 | MEMBLOCK_FLAGS_NONE, 0},
            {0, 0, ORDER_4 | MEMBLOCK_FLAGS_NONE, 0},
        },
    },
    //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    [46] =  // Tiawanese Focus TCM                                      //0x2E
    {       // Preliminary, NEEDS TO BE TESTED AND VERIFIED P.D. 08/16/2012
        .index = 46,
        .ecm_id = 0x7E1, .reponse_ecm_id = 0x7E9,
        .unlock_flags = UNLOCK_FLAG_NONE,
        .upload_unlock_id = UnlockId_Regular,
        .download_unlock_id = UnlockId_Regular,
        .checksum_id = ChecksumId_None,
        .finalizing_id = FinalId_None,
        .flags = UPLOAD_ADDR_TYPE_4B | UPLOAD_BY_CMD_23 |
            USE_ERASE_COMMAND |
            DOWNLOAD_ADDR_TYPE_4B |  DOWNLOAD_REQUEST_TYPE_4B |
            DOWNLOAD_REQUIRE_CMD_37,
        .extended_flags = ECM_FLAGS_NONE,
        .commtype = CommType_CAN, .commlevel = CommLevel_KWP2000,
        .eraseall_cmd = {EraseCmdInfoFlags_None,EraseCmdInfoCount1,{0xAA}},
        .erasecal_cmd = {EraseCmdInfoFlags_None,EraseCmdInfoCount1,{0xAA}},
        .vid_address = 0x002047A,    // There is no VID block.
                                    // This is the strategy location. Not correct yet.
        .pats_address = 0xFF,
        .vin_address = ECM_DEFS_INVALID_VIN_ADDRESS,
        .testerpresent_frequency = 0,
        
        .data_offset = 0x0,
        .ecm_startingaddr = 0x0,
        
        .util_upload = {NULL,NULL,NULL},
        .util_dnload = {NULL,NULL,NULL},
        .maxuploadblocksize = 0xFF8, .maxdownloadblocksize = 0x400, //was 0xFFE
        .os_block_read_skipblock_info = NULL,
        .os_block_read =
        {
            {0x00020000,  0x001E0000,   ORDER_0 | MEMBLOCK_FLAGS_NONE, 0},
            {0, 0, ORDER_1 | MEMBLOCK_FLAGS_NONE, 0},
            {0, 0, ORDER_2 | MEMBLOCK_FLAGS_NONE, 0},
            {0, 0, ORDER_3 | MEMBLOCK_FLAGS_NONE, 0},
            {0, 0, ORDER_4 | MEMBLOCK_FLAGS_NONE, 0},
        },
        .os_block =
        {
             {0x00020000,  0x001E0000,   ORDER_0 | MEMBLOCK_FLAGS_NONE, 0},
            {0, 0, ORDER_1 | MEMBLOCK_FLAGS_NONE, 0},
            {0, 0, ORDER_2 | MEMBLOCK_FLAGS_NONE, 0},
            {0, 0, ORDER_3 | MEMBLOCK_FLAGS_NONE, 0},
            {0, 0, ORDER_4 | MEMBLOCK_FLAGS_NONE, 0},
        },
        .cal_block =
        {
             {0x00020000,  0x001E0000,   ORDER_0 | MEMBLOCK_FLAGS_NONE, 0},
            {0, 0, ORDER_1 | MEMBLOCK_FLAGS_NONE, 0},
            {0, 0, ORDER_2 | MEMBLOCK_FLAGS_NONE, 0},
            {0, 0, ORDER_3 | MEMBLOCK_FLAGS_NONE, 0},
            {0, 0, ORDER_4 | MEMBLOCK_FLAGS_NONE, 0},
        },
    },
    //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    [47] =  // Bosch - Diesel (A) ?EDC17 CP05 2870k?                      //0x2F
    {       // Related Vehicles: 2013+ 6.7L Superduty Trucks
        .index = 47,
        .ecm_id = 0x7E0, .reponse_ecm_id = 0x7E8,
        .unlock_flags = UNLOCK_FLAG_NONE,
        .upload_unlock_id = UnlockId_67L,
        .download_unlock_id = UnlockId_67L,
        .checksum_id = ChecksumId_67L_2013UP,
        .finalizing_id = FinalId_None,
        .flags =  SIMPLE_WAKEUP_CHECK_UPLOAD | SIMPLE_WAKEUP_CHECK_DOWNLOAD |
            UPLOAD_ADDR_TYPE_4B | UPLOAD_BY_CMD_35_MULTI_36 |
            UPLOAD_REQUIRE_CMD_37 | USE_ERASE_COMMAND |
            DOWNLOAD_ADDR_TYPE_4B |  DOWNLOAD_REQUEST_TYPE_4B |
            DOWNLOAD_REQUIRE_CMD_37 | DOWNLOAD_ALL,
        .extended_flags = LITTLE_ENDIAN | TUNEOSDATA_REQUIRED | 
            SKIP_LAST_37_COMMAND | DOWNLOAD_HEALTH_CHECK_STRICT,
        .commtype = CommType_CAN, .commlevel = CommLevel_ISO14229,
        .eraseall_cmd = {EraseCmdInfoFlags_None,EraseCmdInfoCount0,{0}},
        .erasecal_cmd = {EraseCmdInfoFlags_None,EraseCmdInfoCount0,{0}},
        .vid_address = 0x809F0080, .pats_address = 0xFF,  //TODOQ: !?!
        .vin_address = ECM_DEFS_INVALID_VIN_ADDRESS,
        .testerpresent_frequency = 10,
        
        .data_offset = 0x0,
        .ecm_startingaddr = 0x0,
        
        .util_upload = {(utility_info*)&utilityinfolist[14],NULL,NULL},
        .util_dnload = {(utility_info*)&utilityinfolist[14],NULL,NULL},
        .maxuploadblocksize = 0x400, .maxdownloadblocksize = 0x400,
        .os_block_read_skipblock_info = NULL,
        .os_block_read =
        {
            {0x80000000, 0x00004000,   ORDER_0 | MEMBLOCK_FLAGS_NONE, 0},
            {0x80020000, 0x003E0000,   ORDER_1 | MEMBLOCK_FLAGS_NONE, 0},
            {0, 0, ORDER_2 | MEMBLOCK_FLAGS_NONE, 0},
            {0, 0, ORDER_3 | MEMBLOCK_FLAGS_NONE, 0},
            {0, 0, ORDER_4 | MEMBLOCK_FLAGS_NONE, 0},
        },
        .os_block =
        {
            {0x80000000, 0x00004000,   ORDER_0 | MEMBLOCK_FLAGS_NONE, 0},
            {0x80020000, 0x003E0000,   ORDER_1 | MEMBLOCK_FLAGS_NONE, 0},
            {0, 0, ORDER_2 | MEMBLOCK_FLAGS_NONE, 0},
            {0, 0, ORDER_3 | MEMBLOCK_FLAGS_NONE, 0},
            {0, 0, ORDER_4 | MEMBLOCK_FLAGS_NONE, 0},
        },
        .cal_block =
        {
            {0x80000000, 0x00004000,   ORDER_0 | MEMBLOCK_FLAGS_NONE, 0},
            {0x80020000, 0x003E0000,   ORDER_1 | MEMBLOCK_FLAGS_NONE, 0},
            {0, 0, ORDER_2 | MEMBLOCK_FLAGS_NONE, 0},
            {0, 0, ORDER_3 | MEMBLOCK_FLAGS_NONE, 0},
            {0, 0, ORDER_4 | MEMBLOCK_FLAGS_NONE, 0},
        },
    },
    
    //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    [48] =  // PRISM: CBP-A2.2 Viper 3072K                               //0x30
            // Related Vehicles: 2013+ 3.7L MKZ
    {
        .index = 47,
        .ecm_id = 0x7E0, .reponse_ecm_id = 0x7E8,
        .unlock_flags = UNLOCK_FLAG_NONE,
        .upload_unlock_id = UnlockId_Regular,
        .download_unlock_id = UnlockId_Regular,
        .checksum_id = ChecksumId_None,
        .finalizing_id = FinalId_None,
        .flags =  SIMPLE_WAKEUP_CHECK_UPLOAD | SIMPLE_WAKEUP_CHECK_DOWNLOAD |
            UPLOAD_ADDR_TYPE_4B | UPLOAD_BY_CMD_35_MULTI_36 |
            UPLOAD_REQUIRE_CMD_37 |
            USE_ERASE_COMMAND |
            DOWNLOAD_ADDR_TYPE_4B |  DOWNLOAD_REQUEST_TYPE_4B |
            DOWNLOAD_REQUIRE_CMD_37,
        .extended_flags = CLEAR_KAM_AFTER_DOWNLOAD | ALLOW_INDIRECT_BLANKVIN_OVERLAY,
        .commtype = CommType_CAN, .commlevel = CommLevel_ISO14229,
        .eraseall_cmd = {EraseCmdInfoFlags_None,EraseCmdInfoCount0,{0}},
        .erasecal_cmd = {EraseCmdInfoFlags_None,EraseCmdInfoCount0,{0}},
        .vid_address = 0x0100c0, .pats_address = 0x0100c0,  //TODOQ: !?!
        .vin_address = ECM_DEFS_INVALID_VIN_ADDRESS,        //0x0100C0,
        .testerpresent_frequency = 10,
        
        .data_offset = 0x0,
        .ecm_startingaddr = 0x0,
        
        .util_upload = {(utility_info*)&utilityinfolist[13],NULL,NULL},
        .util_dnload = {(utility_info*)&utilityinfolist[13],NULL,NULL},
        .maxuploadblocksize = 0x400, .maxdownloadblocksize = 0x400,
        .os_block_read_skipblock_info = NULL,
        .os_block_read =
        {
            {0x00010000, 0x001F0000,   ORDER_0 | MEMBLOCK_FLAGS_NONE, 0},
            {0x00200000, 0x00100000,   ORDER_1 | MEMBLOCK_FLAGS_NONE, 0},
            {0, 0, ORDER_2 | MEMBLOCK_FLAGS_NONE, 0},
            {0, 0, ORDER_3 | MEMBLOCK_FLAGS_NONE, 0},
            {0, 0, ORDER_4 | MEMBLOCK_FLAGS_NONE, 0},
        },
        .os_block =
        {
            {0x00010008, 0x001EFFF8,   ORDER_0 | MEMBLOCK_FLAGS_NONE, 0},
            {0x00200000, 0x00100000,   ORDER_1 | MEMBLOCK_FLAGS_NONE, 0},
            {0x00010000, 0x00000008,   ORDER_2 | MEMBLOCK_FLAGS_NONE, 0},
            {0, 0, ORDER_3 | MEMBLOCK_FLAGS_NONE, 0},
            {0, 0, ORDER_4 | MEMBLOCK_FLAGS_NONE, 0},
        },
        .cal_block =
        {
            {0x00010008, 0x001EFFF8,   ORDER_0 | MEMBLOCK_FLAGS_NONE, 0},
            {0x00200000, 0x00100000,   ORDER_1 | MEMBLOCK_FLAGS_NONE, 0},
            {0x00010000, 0x00000008,   ORDER_2 | MEMBLOCK_FLAGS_NONE, 0},
            {0, 0, ORDER_3 | MEMBLOCK_FLAGS_NONE, 0},
            {0, 0, ORDER_4 | MEMBLOCK_FLAGS_NONE, 0},
        },
    },
    //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    [49] =  // Bosch ME9 UK                                              //0x31
            // Related Vehicles: 2005 - 2008 UK Focus ST 225 (PCM)
    {
        .index = 49,
        .ecm_id = 0x7E0, .reponse_ecm_id = 0x7E8,
        .unlock_flags = UNLOCK_FLAG_NONE,
        .upload_unlock_id = UnlockId_ME9_UK,
        .download_unlock_id = UnlockId_ME9_UK,
        .checksum_id = ChecksumId_ME9_UK,
        .finalizing_id = FinalId_None,
        .flags =  SIMPLE_WAKEUP_CHECK_UPLOAD | SIMPLE_WAKEUP_CHECK_DOWNLOAD |
            UPLOAD_ADDR_TYPE_4B | UPLOAD_BY_CMD_23 |
            USE_ERASE_COMMAND |
            DOWNLOAD_ADDR_TYPE_4B |  DOWNLOAD_REQUEST_TYPE_4B |
            DOWNLOAD_REQUIRE_CMD_37,
        .extended_flags = ALLOW_INDIRECT_BLANKVIN_OVERLAY | SPF_REQUIRED |
            REQUIRE_KEY_ON_TEST | DOWNLOAD_HEALTH_CHECK_STRICT,
        .commtype = CommType_CAN, .commlevel = CommLevel_KWP2000,
        .eraseall_cmd = {EraseCmdInfoFlags_None,EraseCmdInfoCount1,{0x01}},
        .erasecal_cmd = {EraseCmdInfoFlags_None,EraseCmdInfoCount1,{0x01}},
        .vid_address = ECM_DEFS_INVALID_VID_ADDRESS, .pats_address = 0x08038D,
        .vin_address = 0x001C3220,
        .testerpresent_frequency = 10,
        
        .data_offset = 0x0,
        .ecm_startingaddr = 0x0,
        
        .util_upload = {NULL,NULL,NULL},
        .util_dnload = {NULL,NULL,NULL},
        .maxuploadblocksize = 0xFC, .maxdownloadblocksize = 0xFC,
        .os_block_read_skipblock_info = NULL,
        .os_block_read =
        {
            {0x00020000, 0x00080000,   ORDER_0 | MEMBLOCK_FLAGS_NONE, 0},
            {0x000A0000, 0x00120000,   ORDER_1 | MEMBLOCK_FLAGS_NONE, 0},
            {0x001C2000, 0x0001E000,   ORDER_2 | MEMBLOCK_FLAGS_NONE, 0},
            {0x001E0000, 0x00020000,   ORDER_3 | MEMBLOCK_FLAGS_NONE, 0},            
            {0, 0, ORDER_4 | MEMBLOCK_FLAGS_NONE, 0},
        },
        .os_block =
        {
            {0x00020000, 0x00060000,   ORDER_0 | MEMBLOCK_FLAGS_NONE, 0},
            {0x000A0000, 0x00120000,   ORDER_1 | MEMBLOCK_FLAGS_NONE, 0},
            {0x001C2000, 0x0001E000,   ORDER_2 | MEMBLOCK_FLAGS_NONE, 0},
            {0x001E0000, 0x00020000,   ORDER_3 | MEMBLOCK_FLAGS_NONE, 0},
            {0, 0, ORDER_4 | MEMBLOCK_FLAGS_NONE, 0},
        },
        .cal_block =
        {
            {0x00020000, 0x00060000,   ORDER_0 | MEMBLOCK_FLAGS_NONE, 0},
            {0x000A0000, 0x00120000,   ORDER_1 | MEMBLOCK_FLAGS_NONE, 0},
            {0x001C2000, 0x0001E000,   ORDER_2 | MEMBLOCK_FLAGS_NONE, 0},
            {0x001E0000, 0x00020000,   ORDER_3 | MEMBLOCK_FLAGS_NONE, 0},
            {0, 0, ORDER_4 | MEMBLOCK_FLAGS_NONE, 0},
        },
    },
    //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    [50] =  // PRISM: EMS24XX Tricore TC-1791 4M (CAN)              //0x32
            // Related Vehicles: 2015 5.0L Mustang & F-150 (PCM)
    {
        .index = 50,
        .ecm_id = 0x7E0, .reponse_ecm_id = 0x7E8,
        .unlock_flags = UNLOCK_FLAG_NONE,
        .upload_unlock_id = UnlockId_Regular,
        .download_unlock_id = UnlockId_Regular,
        .checksum_id = ChecksumId_Conti,
        .finalizing_id = FinalId_SendControlRoutine31,
        .flags = SIMPLE_WAKEUP_CHECK_UPLOAD | SIMPLE_WAKEUP_CHECK_DOWNLOAD |
            /*UPLOAD_ADDR_TYPE_4B | UPLOAD_BY_CMD_35_MULTI_36 |
            UPLOAD_REQUIRE_CMD_37 | */
            USE_ERASE_COMMAND |
            DOWNLOAD_ADDR_TYPE_4B |  DOWNLOAD_REQUEST_TYPE_4B |
            DOWNLOAD_REQUIRE_CMD_37 | COOLDOWNTIME_SHORT | DOWNLOAD_ALL,
        .extended_flags = REQUIRE_DOWNLOAD_FINALIZING | LITTLE_ENDIAN | 
            DOWNLOAD_HEALTH_CHECK_STRICT | SPF_REQUIRED,
        .commtype = CommType_CAN, .commlevel = CommLevel_ISO14229,
        .eraseall_cmd = {EraseCmdInfoFlags_None,EraseCmdInfoCount0,{0}},
        .erasecal_cmd = {EraseCmdInfoFlags_None,EraseCmdInfoCount0,{0}},
        .vid_address = 0xa004028E, .pats_address = 0xFF,  //TODOQ: !?!
        .vin_address = ECM_DEFS_INVALID_VIN_ADDRESS,
        .testerpresent_frequency = 3,
        
        .data_offset = 0x0,
        .ecm_startingaddr = 0x0,

        .util_upload = {(utility_info*)&utilityinfolist[15],NULL,NULL},
        .util_dnload = {(utility_info*)&utilityinfolist[15],NULL,NULL},
        .maxuploadblocksize = 0x0AFC, .maxdownloadblocksize = 0x0AFC,
        .os_block_read_skipblock_info = NULL,
        .os_block_read =
        {
            {0x80020000, 0x001E0000, ORDER_0 | MEMBLOCK_FLAGS_NONE, 0},
            {0x808C0000, 0x0013FE00, ORDER_1 | MEMBLOCK_FLAGS_NONE, 0},
            {0xA0800000, 0x000BFE00, ORDER_2 | MEMBLOCK_FLAGS_NONE, 0},
            {0, 0, ORDER_3 | MEMBLOCK_FLAGS_NONE, 0},
            {0, 0, ORDER_4 | MEMBLOCK_FLAGS_NONE, 0},
        },
        .os_block =
        {
            {0x80020000, 0x001E0000, ORDER_0 | MEMBLOCK_FLAGS_NONE, 0},
            {0x808C0000, 0x0013FE00, ORDER_1 | MEMBLOCK_FLAGS_NONE, 0},
            {0xA0800000, 0x000BFE00, ORDER_2 | MEMBLOCK_FLAGS_NONE, 0},
            {0, 0, ORDER_3 | MEMBLOCK_FLAGS_NONE, 0},
            {0, 0, ORDER_4 | MEMBLOCK_FLAGS_NONE, 0},
        },
        .cal_block =
        {
            {0x80020000, 0x001E0000, ORDER_0 | MEMBLOCK_FLAGS_NONE, 0},
            {0x808C0000, 0x0013FE00, ORDER_1 | MEMBLOCK_FLAGS_NONE, 0},
            {0xA0800000, 0x000BFE00, ORDER_2 | MEMBLOCK_FLAGS_NONE, 0},
            {0, 0, ORDER_3 | MEMBLOCK_FLAGS_NONE, 0},
            {0, 0, ORDER_4 | MEMBLOCK_FLAGS_NONE, 0},
        },
    },
};

/*------------------------------------------------------------------------------
 IMPORTANT NOTES
 1) Maintain VEH_FORD_Is60L_PCM, VEH_FORD_Is67L_PCM & 
    VEH_FORD_IsF150Ecoboost35L_PCM if new types are added.
------------------------------------------------------------------------------*/
