/**
  **************** (C) COPYRIGHT 2011 SCT Performance, LLC *********************
  * File Name          : obd2can_ford.h
  * Device / System    : SCT products using the "CommonCode" platform
  * Author             : Quyen Leba
  *
  * Version            : 1 
  * Date               : 05/02/2011
  * Description        : 
  *                    : 
  *
  *
  * History            : 05/02/2011 M. Davis
  *                    :   Add SCT Copyright header,
  *
  ******************************************************************************
  */

#ifndef __OBD2CAN_FORD_H
#define __OBD2CAN_FORD_H

#include <common/obd2can.h>
#include <common/obd2datalog.h>
#include <common/obd2def.h>

//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
// FORD funcions to support ECM communication
//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

u8 obd2can_ford_wakeup(u8 veh_type, u8 ecm_index);
u8 obd2can_ford_downloadutility(u32 ecm_type, u8 utilitychoiceindex,
                                bool isdownloadutility);
u8 obd2can_ford_exitutility(u32 ecm_type);
u8 obd2can_ford_downloadfinalizing(u32 ecm_type, flasher_info *flasherinfo);
u8 obd2can_ford_getvidaddress(u32 ecm_id, u32 *vidaddress);
u8 obd2can_ford_getmodulepartnumber(u32 ecm_id, u8 *modulepartnumber);
u8 obd2can_ford_getsoftwarepartnumber(u32 ecm_id, u8 *swpartnumber);
u8 obd2can_ford_gethardwarepartnumber(u32 ecm_id, u8 *hwpartnumber);
u8 obd2can_ford_readvin(u8 *vin);

//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
// Functions from J2190
//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
u8 obd2can_ford_clear_diagnostic_information(bool broadcast,            //$04
                                             u32 ecm_id);
u8 obd2can_ford_request_vehicle_information(u32 ecm_id, u8 infotype,    //$09
                                            u8 *data, u16 *datalength);
u8 obd2can_ford_initiate_diagnostic_operation(bool broadcast,           //$10
                                              u32 ecm_id,
                                              Obd2can_SubServiceNumber subservice,
                                              bool isreponse_expected);
u8 obd2can_ford_ecu_reset_request(bool broadcast,                       //$11
                                  u32 ecm_id,
                                  Obd2can_SubServiceNumber subservice,
                                  bool isreponse_expected);
u8 obd2can_ford_read_data_by_local_id(u32 ecm_id, u8 data_id,           //$21
                                      u8 *data, u32 *datalength);
u8 obd2can_ford_read_data_bydmr(u32 ecm_id, u32 address, u16 length,    //$23                              
                                u8 *data, u16 *datalength,
                                bool response_expected,
                                VehicleCommLevel *vehiclecommlevel);
u8 obd2can_ford_read_memory_address(u32 ecm_id,                         //$23
                                    u32 address, u16 length,
                                    u8 *data, u16 *datalength,
                                    bool force_readby4bytes,
                                    VehicleCommLevel commlevel);
u8 obd2can_ford_read_memory_address_kwp2000(u32 ecm_id, u32 address, u16 length,
                                    u8 *data, u16 *datalength,
                                    bool force_readby4bytes);
u8 obd2can_ford_read_memory_address_iso14229(u32 ecm_id, u32 address, u16 length,
                                    u8 *data, u16 *datalength);
u8 obd2can_ford_security_access(u32 ecm_id,                             //$27
                                Obd2can_SubServiceNumber subservice,
                                u8 *data, u8 *datalength, u32 algoindex);
u8 obd2can_ford_disablenormalcommunication(bool broadcast,              //$28
                                           u32 ecm_id);
u8 obd2can_ford_enablenormalcommunication(bool broadcast,               //$29
                                          u32 ecm_id);
u8 obd2can_ford_requestdiagnosticdatapacket_2A(u32 ecm_id, u8 datarate, //$2A
                                               u8 *packetidlist,
                                               u8 packetidcount,
                                               VehicleCommLevel commlevel);
u8 obd2can_ford_dynamicallydefinemessage(u32 ecm_id, u16 packetid,      //$2C
                                         PidType type, u8 size, u8 position,
                                         u32 address,
                                         VehicleCommLevel commlevel);
u8 obd2can_ford_cleandynamicallydefinemessage_14229(u32 ecm_id);        //$2C
u8 obd2can_ford_input_output_control(u32 ecm_id, u16 did, u8 ctrlParam, //$2F
                              u8 *data, u8 datalength, 
                              VehicleCommLevel commlevel,
                              bool response_expected);
u8 obd2can_ford_start_diagnostic_routine_bytestnumber(u32 ecm_id,       //$31
                                                      u8 testnumber,
                                                      u8 *testdata,
                                                      u8 testdatalength,
                                                      VehicleCommLevel commlevel);
u8 obd2can_ford_request_download(u32 ecm_id,                            //$34
                                 u8 dataformat,
                                 u32 requestaddress, u32 requestlength,
                                 RequestLengthType reqtype,
                                 VehicleCommLevel commlevel,
                                 u16 *maxdownloadblocklength);
u8 obd2can_ford_request_upload(u32 ecm_id,                              //$35
                               u32 requestaddress, u32 requestlength,
                               RequestLengthType reqtype,
                               VehicleCommLevel commlevel,
                               u16 *maxuploadblocklength);
u8 obd2can_ford_transfer_data_upload(u32 ecm_id, u8 blocktracking,      //$36
                                     VehicleCommLevel commlevel,
                                     u32 request_address,
                                     RequestLengthType request_type,
                                     u8 *data, u16 *datalength);
u8 obd2can_ford_transfer_data_download(u32 ecm_id,                      //$36
                                       Obd2can_SubServiceNumber subservice,
                                       u8 blocktracking, u32 startingaddr,
                                       MemoryAddressType addrtype,
                                       u8 *data, u16 datalength,
                                       VehicleCommLevel commlevel,
                                       bool response_expected);
u8 obd2can_ford_request_transfer_exit(u32 ecm_id,                       //$37
                                      VehicleCommLevel commlevel);
u8 obd2can_ford_write_data_by_local_id(u32 ecm_id, u8 data_id,          //$3B
                                       u8 *data, u32 datalength);
u8 obd2can_ford_requestdiagnosticdatapacket(u32 ecm_id, u8 datarate,    //$A0
                                            u8 *packetidlist, u8 packetidcount,
                                            VehicleCommLevel commlevel);
u8 obd2can_ford_dynamicallydefinediagnosticdatapacket(u32 ecm_id,       //$A1
                                                      u8 packetid,
                                                      PidType type,
                                                      u8 size, u8 position,
                                                      u32 address,
                                                      VehicleCommLevel commlevel);

u8 obd2can_ford_validate_oscpid(u32 ecm_id, DlxBlock *dlxblock,
                                VehicleCommLevel commlevel);
u8 obd2can_ford_validatedmr(u32 ecm_id, DlxBlock *dlxblock,
                            VehicleCommLevel commlevel);
u8 obd2can_ford_validateByRapidPacketSetup(u32 ecm_id, DlxBlock *dlxblock,
                            VehicleCommLevel commlevel);
u8 obd2can_ford_alt_readdtc(u32 ecm_id, dtc_info *info);
u8 obd2can_ford_unlock_osc(u32 ecm_id,VehicleCommLevel commlevel);
u8 obd2can_ford_getinfo(obd2_info_block *block);
u8 obd2can_ford_append_patch(u32 ecm_type, flasher_info *flasherinfo);
u8 obd2can_ford_download_signature_patch(u32 ecm_type);


/*All the function below use to be overloaded functions*/
u8 obd2can_ford_reset_ecm(u8 ecm_type);
u8 obd2can_ford_erase_ecm(u8 ecm_type, bool is_cal_only);
u8 obd2can_ford_clear_kam(u32 ecm_id, VehicleCommLevel commlevel);
void obd2can_ford_testerpresent(bool broadcast, u32 ecm_id,
                             VehicleCommLevel commlevel); 
u8 obd2can_ford_readecminfo(ecm_info *ecm);
u8 obd2can_ford_cleardtc();


u8 obd2can_ford_request_TPMS_info(u8 *front_tire_pressure, u8 *rear_tire_pressure);
u8 obd2can_ford_check_TPMS_support(u8 *info_text);

#endif    //__OBD2CAN_FORD_H
