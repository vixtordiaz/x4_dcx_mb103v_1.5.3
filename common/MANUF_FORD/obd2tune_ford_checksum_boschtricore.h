/**
  **************** (C) COPYRIGHT 2011 SCT Performance, LLC *********************
  * File Name          : obd2tune_ford_checksum_boschtricore.h
  * Device / System    : SCT products using the "CommonCode" platform
  * Author             : Patrick Downs
  *
  * Version            : 1 
  * Date               : 08/15/2012
  * Description        : Checksum functions for Bosch PCMs using Infineon Tricore 
  *                      processors.
  *
  * 
  ******************************************************************************
  */

#ifndef __OBD2TUNE_FORD_CHECKSUM_BOSCHTRICORE_H
#define __OBD2TUNE_FORD_CHECKSUM_BOSCHTRICORE_H

#include <arch/gentype.h>

u32 Bosch_Tricore_Checksum(u8 ecm_type, u32 ecm_offset);
u32 Ford_67L_ModifyForSBLAndChecksum(void);
u8 Ford_Calculate67L_Checksums(u16 checksumid);

#endif    //__OBD2TUNE_FORD_CHECKSUM_BOSCHTRICORE_H
