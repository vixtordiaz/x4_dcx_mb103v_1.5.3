/**
  **************** (C) COPYRIGHT 2011 SCT Performance, LLC *********************
  * File Name          : obd2can_ford.c
  * Device / System    : SCT products using the "CommonCode" platform
  * Author             : Quyen Leba
  *
  * Version            : 1 
  * Date               : 05/02/2011
  * Description        : 
  *                    : 
  *
  *
  * History            : 05/02/2011 M. Davis
  *                    :   Add SCT Copyright header,
  *
  ******************************************************************************
  */

#include <ctype.h>
#include <string.h>
#include <board/delays.h>
#include <fs/genfs.h>
#include <common/statuscode.h>
#include <common/crypto_blowfish.h>
#include <common/itoa.h>
#include "obd2can_ford.h"
#include <common/obd2def.h>
#include <common/ubf.h>

extern u8 obd2_rxbuffer[256];           /* from obd2.c */
extern obd2_info gObd2info;             /* from obd2.c */
extern Datalog_Mode gDatalogmode;       /* from obd2datalog.c */
extern VehDefLastBlock gVehDefLastBlock; /*from veh_defs.c*/

//------------------------------------------------------------------------------
// Private functions
//------------------------------------------------------------------------------
u8 obd2can_ford_read_strategy_bysearch(u32 ecm_id, u8 *strategy, VehicleCommLevel commlevel);

//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
u8 obd2can_ford_txrx(Obd2can_ServiceData *servicedata)
{
    obd2can_rxinfo rxinfo;
    u8 status;
    
    if (servicedata->broadcast)
    {
//        //always single frame
//        //TODOQ: this
//        return S_NOTSUPPORT;
        //TODOQ: not fully supported; only catch responses with ecm_id for now
        status = obd2can_txcomplex(servicedata->broadcast_id,
                                   servicedata->response_id,
                                   servicedata->service,
                                   servicedata->subservice,
                                   servicedata->service_controldatabuffer,
                                   servicedata->service_controldatalength,
                                   servicedata->txdata,
                                   servicedata->txdatalength,
                                   servicedata->nowait_cantx);
    }
    else
    {
        status = obd2can_txcomplex(servicedata->ecm_id,
                                   servicedata->response_id,
                                   servicedata->service,
                                   servicedata->subservice,
                                   servicedata->service_controldatabuffer,
                                   servicedata->service_controldatalength,
                                   servicedata->txdata,
                                   servicedata->txdatalength,
                                   servicedata->nowait_cantx);
    }
    
    if (status == S_SUCCESS && servicedata->response_expected == TRUE)
    {
        obd2can_rxinfo_init(&rxinfo);

        rxinfo.rxbuffer = servicedata->rxdata;
        rxinfo.cmd = (u8)servicedata->service;
        rxinfo.first_frame_data_offset = servicedata->first_frame_data_offset;
        rxinfo.tester_present_type = servicedata->tester_present_type;
        rxinfo.initial_wait_extended = servicedata->initial_wait_extended;
        rxinfo.busy_wait_extended = servicedata->busy_wait_extended;
        rxinfo.commlevel = servicedata->commlevel;
        rxinfo.rx_timeout_ms = servicedata->rx_timeout_ms;
        
        status = obd2can_rx(ECM_RESPONSE_ID(servicedata->ecm_id), &rxinfo);

        if (status == S_SUCCESS)
        {
            servicedata->rxdatalength = rxinfo.rxlength;
        }
    }
    
    if (status == S_ERROR)
    {
        servicedata->errorcode = rxinfo.errorcode;
    }

    return status;
}

//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
u8 obd2can_ford_txrxcomplex(Obd2can_ServiceData *servicedata)
{
    obd2can_rxinfo rxinfo;
    u8 status;
    
    if (servicedata->broadcast)
    {
        //always single frame
        //TODOQ: this
        return S_NOTSUPPORT;
    }
    else
    {
        status = obd2can_txcomplex(servicedata->ecm_id,
                                   servicedata->response_id,
                                   servicedata->service,
                                   servicedata->subservice,
                                   servicedata->service_controldatabuffer,
                                   servicedata->service_controldatalength,
                                   servicedata->txdata,
                                   servicedata->txdatalength,
                                   servicedata->nowait_cantx);
    }
    
    if (status == S_SUCCESS && servicedata->response_expected == TRUE)
    {
        obd2can_rxinfo_init(&rxinfo);

        rxinfo.rxbuffer = servicedata->rxdata;
        rxinfo.cmd = (u8)servicedata->service;
        rxinfo.first_frame_data_offset = servicedata->first_frame_data_offset;
        rxinfo.tester_present_type = servicedata->tester_present_type;
        rxinfo.initial_wait_extended = servicedata->initial_wait_extended;
        rxinfo.busy_wait_extended = servicedata->busy_wait_extended;
        rxinfo.commlevel = servicedata->commlevel;
        rxinfo.rx_timeout_ms = servicedata->rx_timeout_ms;
        status = obd2can_rxcomplex(ECM_RESPONSE_ID(servicedata->ecm_id), &rxinfo);
        if (status == S_SUCCESS)
        {
            servicedata->rxdatalength = rxinfo.rxlength;
        }
    }

    return status;
}

//------------------------------------------------------------------------------
// ClearDiagnosticInformation ($04)
// Inputs:  bool broadcast
//          u32 ecm_id
// Return:  u8  status
// Engineer: Quyen Leba
//------------------------------------------------------------------------------
u8 obd2can_ford_clear_diagnostic_information(bool broadcast, u32 ecm_id)
{
    Obd2can_ServiceData servicedata;
    u8 status;

    obd2can_servicedata_init(&servicedata);
    servicedata.ecm_id = ecm_id;
    servicedata.broadcast = broadcast;
    servicedata.service = FORD_ClearDiagnosticInformation;
    status = obd2can_txrx(&servicedata);
    
    return status;
}

//------------------------------------------------------------------------------
// ClearDiagnosticInformationExtended ($14)
// Inputs:  bool broadcast
//          u32 ecm_id
// Return:  u8  status
// Engineer: Quyen Leba
//------------------------------------------------------------------------------
u8 obd2can_ford_clear_diagnostic_information_extended(bool broadcast, u32 ecm_id)
{
    Obd2can_ServiceData servicedata;
    u8 status;

    obd2can_servicedata_init(&servicedata);
    servicedata.ecm_id = ecm_id;
    servicedata.broadcast_id = 0x7DF;
    servicedata.broadcast = broadcast;
    servicedata.service = FORD_ClearDiagnosticInfomationExt;
    servicedata.service_controldatabuffer[0] = 0xFF;
    servicedata.service_controldatabuffer[1] = 0x00;
    servicedata.service_controldatalength = 2;
    status = obd2can_txrx(&servicedata);
    
    return status;
}

//------------------------------------------------------------------------------
// ClearDiagnosticInformationISO14229 ($14)
// Inputs:  bool broadcast
//          u32 ecm_id
// Return:  u8  status
// Engineer: Quyen Leba
//------------------------------------------------------------------------------
u8 obd2can_ford_clear_diagnostic_information_iso14229(bool broadcast, u32 ecm_id)
{
    Obd2can_ServiceData servicedata;
    u8 status;

    obd2can_servicedata_init(&servicedata);
    servicedata.ecm_id = ecm_id;
    servicedata.broadcast_id = 0x7DF;
    servicedata.broadcast = broadcast;
    servicedata.service = FORD_ClearDiagnosticInfomationExt;
    servicedata.service_controldatabuffer[0] = 0xFF;
    servicedata.service_controldatabuffer[1] = 0xFF;
    servicedata.service_controldatabuffer[2] = 0xFF;
    servicedata.service_controldatalength = 3;
    status = obd2can_txrx(&servicedata);
    
    return status;
}

//------------------------------------------------------------------------------
// ReadVehicleInformation ($09)
// Inputs:  u32 ecm_id
//          u8  infotype - Type of data to be requested (ie. VIN, CalID, etc)
// Outputs: u8  *data
//          u16 *datalength
// Return:  u8  status
// Engineer: Quyen Leba
//------------------------------------------------------------------------------
u8 obd2can_ford_request_vehicle_information(u32 ecm_id, u8 infotype,
                                            u8 *data, u16 *datalength)
{
    Obd2can_ServiceData servicedata;
    u8  buffer[100];
    u8  length;
    u8  i;
    u8  readable = 0;       // Number of consecutive readable characters
    u8  blank = 0;          // Number of consecutive 0xFF's
    u8  zero = 0;           // Number of consecutive 0x00's
    u8  space = 0;          // Number of consecutive 0x20's (space characters)
    u8  start;
    u8  status;

    obd2can_servicedata_init(&servicedata);
    servicedata.ecm_id = ecm_id;
    servicedata.service = FORD_RequestVehicleInformation;
    servicedata.subservice = infotype;
    status = obd2can_txrx(&servicedata);
    
    //expected reponse: [1:infotype][1:messagecount][n:messagedata]
    if (status == S_SUCCESS && servicedata.rxdatalength > 2 &&
        servicedata.rxdatalength <= sizeof(buffer) &&
        servicedata.rxdata[0] == infotype)
    {
        length = servicedata.rxdatalength-2;
        memcpy(buffer,(char*)&servicedata.rxdata[2],length);
        for(i=0;i<length;i++)
        {
            // Readable Character
            if (isgraph(buffer[i]))
            {
                if (readable == 0)
                {
                    start = i;
                }
                readable++;
            }
            // Blank (0xFF) Character
            else if (buffer[i] == 0xFF)
            {
                readable = 0;
                space = 0;
                zero = 0;
                blank++;
            }
            // Zero (0x00) Character
            else if (buffer[i] == 0x00)
            {
                readable = 0;
                space = 0;
                blank = 0;
                zero++;
            }
            // Space Character
            else if (buffer[i] == 0x20)
            {
                readable = 0;
                blank = 0;
                zero = 0;
                space++;
            }
            else
            {
                break;
            }
            
            if (readable >=4 || blank >= 4 || space >= 4 || zero >= 4)
            {
                break;
            }
        }
        // Must see a minimum of 4 consecutive readable characters
        if (readable >= 4)
        {
            length -= start;
            memcpy(data,(char*)&buffer[start],length);
        }
        // Or a minimum of 4 consecutive 0xFF/0x00 or 0x20
        else if (blank >= 4 || space >= 4 || zero >= 4)
        {
            length = 0;
        }
        else
        {
            length = 0;
            status = S_FAIL;
        }
        data[length] = NULL;
        *datalength = length;
    }
    else
    {
        data[0] = NULL;
        *datalength = 0;
    }
    
    return status;
}

//------------------------------------------------------------------------------
// Clear KAM (Keep Alive Memory)
// Obtained from PTDIAG sniffs of clear KAM function
// Inputs:  u32 ecm_id
//          VehicleCommLevel commlevel
// Return:  u8  status
// Engineer: Patrick Downs
//------------------------------------------------------------------------------
u8 obd2can_ford_clear_kam(u32 ecm_id, VehicleCommLevel commlevel)
{
    u8 status;
    
    if(commlevel == CommLevel_KWP2000)
    {
        Obd2can_ServiceData servicedata;
        
        obd2can_servicedata_init(&servicedata);
        servicedata.ecm_id = ecm_id;
        servicedata.service = 0xB1;
        servicedata.service_controldatabuffer[0] = 0x80;
        servicedata.service_controldatabuffer[1] = 0x12;
        servicedata.service_controldatalength = 2;
        servicedata.initial_wait_extended = 30;
        servicedata.busy_wait_extended = 10;
        servicedata.tester_present_type = TESTER_PRESENT_NODE;
        
        status = obd2can_txrx(&servicedata);
        
        if (status == S_ERROR)
        {
            switch (servicedata.errorcode)
            {
            case 0x11: // Service not supported            
            case 0x31: // Request out of range
                status = S_NOTSUPPORT;
                break;
            default:
                break;
            }
        }
    }
    else if(commlevel == CommLevel_ISO14229)
    {
        u8 databuffer[2];
        
        memset(databuffer,0,sizeof(databuffer));
        databuffer[0] = 0x40;
        databuffer[1] = 0x61;
        
        status = obd2can_ford_start_diagnostic_routine_bytestnumber
            (ecm_id,0x01,databuffer,sizeof(databuffer),commlevel);
    }
    else
    {
        // Comm Level not valid
        status = S_INPUT;        
    }    
    return status;
}

//------------------------------------------------------------------------------
// InitiateDiagnosticOperation ($10)
// Inputs:  bool broadcast
//          u32 ecm_id
//          Obd2can_SubServiceNumber subservice (disableAllDTCs,
//              enableDTCsDuringDevCntrl, wakeUpLinks)
//          bool isreponse_expected (TRUE: wait for response)
// Return:  u8  status
// Engineer: Quyen Leba
//------------------------------------------------------------------------------
u8 obd2can_ford_initiate_diagnostic_operation(bool broadcast, u32 ecm_id,
                                              Obd2can_SubServiceNumber subservice,
                                              bool isreponse_expected)
{
    Obd2can_ServiceData servicedata;
    u8 status;

    obd2can_servicedata_init(&servicedata);
    servicedata.ecm_id = ecm_id;
    servicedata.broadcast = broadcast;
    servicedata.service = FORD_InitiateDiagnosticOperation;
    servicedata.subservice = subservice;
    servicedata.nowait_cantx = TRUE;
    servicedata.response_expected = isreponse_expected;
    status = obd2can_txrx(&servicedata);
    
    return status;
}

//------------------------------------------------------------------------------
// ECUResetRequest ($11)
// Inputs:  bool broadcast
//          u32 ecm_id
//          Obd2can_SubServiceNumber subservice
//          bool isreponse_expected (TRUE: wait for response)
// Return:  u8  status
// Engineer: Quyen Leba
//------------------------------------------------------------------------------
u8 obd2can_ford_ecu_reset_request(bool broadcast,                       //$11
                                  u32 ecm_id,
                                  Obd2can_SubServiceNumber subservice,
                                  bool isreponse_expected)
{
    Obd2can_ServiceData servicedata;
    u8 status;

    obd2can_servicedata_init(&servicedata);
    servicedata.ecm_id = ecm_id;
    servicedata.broadcast = broadcast;
    servicedata.service = FORD_RequestModuleReset;
    servicedata.subservice = subservice;
    servicedata.response_expected = isreponse_expected;
    status = obd2can_txrx(&servicedata);
    
    return status;
}

//------------------------------------------------------------------------------
// FORD_ReadDataByLocalId ($21)
// Inputs:  u32 ecm_id
//          u8  data_id
// Outputs: u8  *data
//          u32 *datalength
// Return:  u8  status
// Engineer: Quyen Leba
//------------------------------------------------------------------------------
u8 obd2can_ford_read_data_by_local_id(u32 ecm_id, u8 data_id,
                                      u8 *data, u32 *datalength)
{
    Obd2can_ServiceData servicedata;
    u8 status;

    *datalength = 0;

    obd2can_servicedata_init(&servicedata);
    servicedata.ecm_id = ecm_id;
    servicedata.service = FORD_ReadDataByLocalId;

    servicedata.service_controldatabuffer[0] = data_id;
    servicedata.service_controldatalength = 1;

    servicedata.rxdata = data;
    servicedata.response_expected = TRUE;

    status = obd2can_txrx(&servicedata);
    if (status == S_SUCCESS)
    {
        *datalength = servicedata.rxdatalength;
    }
    return status;
}

//------------------------------------------------------------------------------
// ReadMemoryByAddress ($23)
// Inputs:  u32 ecm_id
//          u32 address (memory address)
//          u16 length (to request upload)
//          MemoryAddressType addrtype
//          bool response_expected
//          VehicleCommLevel *vehiclecommlevel
// Output:  u8  *data
//          u16 *datalength
// Return:  u8  status
// Engineer: Tristen Pierson
//------------------------------------------------------------------------------
u8 obd2can_ford_read_data_bydmr(u32 ecm_id, u32 address, u16 length,                              
                                u8 *data, u16 *datalength,
                                bool response_expected,
                                VehicleCommLevel *vehiclecommlevel)
{
    Obd2can_ServiceData servicedata;
    u8 status;

    u8 addressAndLengthFormatIdentifier; 
    u8 memorySize;      // Upper nibble of addressAndLengthFormatIdentifier
    u8 memoryAddress;   // Lower nibble of addressAndLengthFormatIdentifier
    
    memorySize = 1;
    memoryAddress = 4;
    addressAndLengthFormatIdentifier = (u8)((memorySize << 4) | (memoryAddress));
    
    if (response_expected == TRUE && (data == NULL || datalength == NULL))
    {
        log_push_error_point(0x305C);
        return S_INPUT;
    }    
    obd2can_servicedata_init(&servicedata);
    servicedata.ecm_id = ecm_id;
    servicedata.service = FORD_ReadMemoryByAddress;
    servicedata.rxdata = data;
    
    switch (vehiclecommlevel[0])
    {
        case CommLevel_ISO14229:
            servicedata.service_controldatabuffer[0] = addressAndLengthFormatIdentifier;
            servicedata.service_controldatabuffer[1] = (u8)(address >> 24);
            servicedata.service_controldatabuffer[2] = (u8)(address >> 16);
            servicedata.service_controldatabuffer[3] = (u8)(address >> 8);
            servicedata.service_controldatabuffer[4] = (u8)(address & 0xFF);
            servicedata.service_controldatabuffer[5] = 4; // 4 byte read will work for all PCMs
            servicedata.service_controldatalength = 6;
            break;            
        case CommLevel_KWP2000:
            servicedata.service_controldatabuffer[0] = (u8)(address >> 24);
            servicedata.service_controldatabuffer[1] = (u8)(address >> 16);
            servicedata.service_controldatabuffer[2] = (u8)(address >> 8);
            servicedata.service_controldatabuffer[3] = (u8)(address & 0xFF);
            servicedata.service_controldatabuffer[4] = (u8)(length >> 8);
            servicedata.service_controldatabuffer[5] = (u8)(length & 0xFF);
            servicedata.service_controldatalength = 6;
            break;
        default:
            return S_INPUT;
    }
        
    if (response_expected == FALSE)
    {    
        servicedata.response_expected = FALSE;
        return obd2can_txrx(&servicedata);
    }
    status = obd2can_txrx(&servicedata);
    
    *datalength = 0;
    if (status == S_SUCCESS)
    {
        if (servicedata.rxdatalength > 1)
        {
            *datalength = servicedata.rxdatalength - 1;
            memcpy((char*)data,(char*)&servicedata.rxdata[1],*datalength);
            return S_SUCCESS;
        }
        else
        {
            log_push_error_point(0x305D);
            return S_BADCONTENT;
        }
    }
    return status;
}

//------------------------------------------------------------------------------
// ReadMemoryByAddress ($23)
// High Level Read Memory By Address function
// Inputs:  u32 ecm_id
//          u32 address (memory address)
//          u16 length (to request upload)
//          bool force_readby4bytes
//          VehicleCommLevel commlevel
// Output:  u8  *data
//          u16 *datalength
// Return:  u8  status
// Engineer: Quyen Leba, Patrick Downs
//------------------------------------------------------------------------------
u8 obd2can_ford_read_memory_address(u32 ecm_id, u32 address, u16 length,
                                    u8 *data, u16 *datalength,
                                    bool force_readby4bytes,
                                    VehicleCommLevel commlevel)
{
    u8  status;
    
    if(commlevel == CommLevel_ISO14229)
    {
        status = obd2can_ford_read_memory_address_iso14229(ecm_id, address, length,
                                                           data, datalength);
    }
    else
    {
        status = obd2can_ford_read_memory_address_kwp2000(ecm_id, address, length,
                                                          data, datalength, 
                                                          force_readby4bytes);
    }
    
    return status;
}

//------------------------------------------------------------------------------
// ReadMemoryByAddress (KWP200) ($23)
// Read memory by address formatted for KWP2000
// Inputs:  u32 ecm_id
//          u32 address (memory address)
//          u16 length (to request upload)
//          bool force_readby4bytes
// Output:  u8  *data
//          u16 *datalength
// Return:  u8  status
// Engineer: Quyen Leba, Patrick Downs
//------------------------------------------------------------------------------
u8 obd2can_ford_read_memory_address_kwp2000(u32 ecm_id, u32 address, u16 length,
                                    u8 *data, u16 *datalength,
                                    bool force_readby4bytes)
{
    Obd2can_ServiceData servicedata;
    u32 i;
    u8  status;

    if (datalength)
    {
        *datalength = 0;
    }
    status = S_FAIL;

    if (force_readby4bytes && length > 4)
    {
        u16 this_length;
        u16 this_datalength;

        i = 0;
        while(i<length)
        {
            this_length = length - i;
            if (this_length > 4)
            {
                this_length = 4;
            }
            status = obd2can_ford_read_memory_address_kwp2000(ecm_id,address+i,
                                                      this_length,
                                                      &data[i],&this_datalength,
                                                      FALSE);
            if (status != S_SUCCESS)
            {
                return status;
            }
            i += 4;
        }
        if (datalength)
        {
            *datalength = i;
        }
    }
    else
    {
        obd2can_servicedata_init(&servicedata);
        servicedata.ecm_id = ecm_id;
        servicedata.service = FORD_ReadMemoryByAddress;
        servicedata.rxdata = data;
                
        servicedata.service_controldatabuffer[0] = (u8)(address >> 24);
        servicedata.service_controldatabuffer[1] = (u8)(address >> 16);
        servicedata.service_controldatabuffer[2] = (u8)(address >> 8);
        servicedata.service_controldatabuffer[3] = (u8)(address & 0xFF);
        servicedata.service_controldatabuffer[4] = (u8)(length >> 8);
        servicedata.service_controldatabuffer[5] = (u8)(length & 0xFF);
        servicedata.service_controldatalength = 6;
        
        // TODOT: Remove this after obd2can_txrxcomplex is fixed (was not reading
        //        ST225 strategies in vehicle).
        if (*datalength <= 6)
        {
            status = obd2can_txrx(&servicedata);
        }
        else
        {
            status = obd2can_txrxcomplex(&servicedata);
        }
        
        if (datalength)
        {
            *datalength = 0;
            if (status == S_SUCCESS)
            {
                *datalength = servicedata.rxdatalength;
            }
        }
    }

    return status;
}

//------------------------------------------------------------------------------
// ReadMemoryByAddress (ISO14229) ($23)
// Read memory by address function formatted for ISO14229
// Inputs:  u32 ecm_id
//          u32 address (memory address)
//          u16 length (to request upload)
// Output:  u8  *data
//          u16 *datalength
// Return:  u8  status
// Engineer: Quyen Leba, Patrick Downs
//------------------------------------------------------------------------------
u8 obd2can_ford_read_memory_address_iso14229(u32 ecm_id, u32 address, u16 length,
                                    u8 *data, u16 *datalength)
{
    Obd2can_ServiceData servicedata;
    u32 i;
    u8  status;
    u32 _address;
    u8 addressAndLengthFormatIdentifier; 
    u8 memorySize;      // Upper nibble of addressAndLengthFormatIdentifier
    u8 memoryAddress;   // Lower nubble of addressAndLengthFormatIdentifier
    
    status = S_FAIL;
    
    // From sniffs of PTDIAG ford seems to only support the below formatter
    memorySize = 1;
    memoryAddress = 4;
    addressAndLengthFormatIdentifier = (u8)((memorySize << 4) | (memoryAddress));
    
    obd2can_servicedata_init(&servicedata);
    servicedata.ecm_id = ecm_id;
    servicedata.service = FORD_ReadMemoryByAddress;
    servicedata.service_controldatabuffer[0] = addressAndLengthFormatIdentifier;
    servicedata.service_controldatabuffer[5] = 4; // 4 byte read will work for all PCMs
                                                  // It could be as large as 0x80 but not known to work on all PCMs
    servicedata.service_controldatalength = 6;
    
    i = 0;
    while(i<length)
    {
        _address = address + i;
        servicedata.service_controldatabuffer[1] = (u8)(_address >> 24);
        servicedata.service_controldatabuffer[2] = (u8)(_address >> 16);
        servicedata.service_controldatabuffer[3] = (u8)(_address >> 8);
        servicedata.service_controldatabuffer[4] = (u8)(_address & 0xFF);
        
        servicedata.rxdata = &data[i];
        
        status = obd2can_txrxcomplex(&servicedata);
        if (status != S_SUCCESS)
        {
            return status;
        }
        i += 4;
    }
    if (datalength)
    {
        *datalength = 0;
        if (status == S_SUCCESS)
        {
            *datalength = i;
        }
    }
    
    return status;
}


//------------------------------------------------------------------------------
// SecurityAccess ($27)
// Inputs:  u32 ecm_id
//          Obd2can_SubServiceNumber subservice
// InOuts:  u8  *data (In:seed, Out:key)
//          u8  *datalength (Out:seedlength, In:keylength)
// Return:  u8  status
// Engineer: Quyen Leba
//------------------------------------------------------------------------------
u8 obd2can_ford_security_access(u32 ecm_id, Obd2can_SubServiceNumber subservice,
                                u8 *data, u8 *datalength, u32 algoindex)
{
    Obd2can_ServiceData servicedata;
    u8 status;

    obd2can_servicedata_init(&servicedata);
    servicedata.ecm_id = ecm_id;
    servicedata.service = FORD_SecurityAccess;
    servicedata.subservice = (u8)subservice;
    
    if (subservice == SPSsendKey || subservice == DevCtrlsendKey || subservice == DiagSendKey)
    {
        memcpy(servicedata.service_controldatabuffer,data,*datalength);
        servicedata.service_controldatalength = *datalength;
    }
    
    status = obd2can_txrx(&servicedata);

    if (status == S_SUCCESS)
    {
        if ((data && servicedata.rxdatalength >= 3) &&
            (subservice == SPSrequestSeed || subservice == DevCtrlrequestSeed || subservice == DiagRequestSeed))
        {
            *datalength = servicedata.rxdatalength - 1;
            memcpy(data,&servicedata.rxdata[1],*datalength);
        }
    }
    return status;
}

//------------------------------------------------------------------------------
// DisableNormalCommunication ($28)
// Inputs:  bool broadcast
//          u32 ecm_id
// Return:  u8  status
// Engineer: Quyen Leba
//------------------------------------------------------------------------------
u8 obd2can_ford_disablenormalcommunication(bool broadcast, u32 ecm_id)
{
    Obd2can_ServiceData servicedata;
    u8 status;

    obd2can_servicedata_init(&servicedata);
    servicedata.ecm_id = ecm_id;
    servicedata.broadcast = broadcast;
    servicedata.service = FORD_DisableNormalMessageTransmission;
    status = obd2can_txrx(&servicedata);
    
    return status;
}

//------------------------------------------------------------------------------
// EnableNormalCommunication ($29)
// Inputs:  bool broadcast
//          u32 ecm_id
// Return:  u8  status
// Engineer: Quyen Leba
//------------------------------------------------------------------------------
u8 obd2can_ford_enablenormalcommunication(bool broadcast, u32 ecm_id)
{
    Obd2can_ServiceData servicedata;
    u8 status;

    obd2can_servicedata_init(&servicedata);
    servicedata.ecm_id = ecm_id;
    servicedata.broadcast = broadcast;
    servicedata.service = FORD_EnableNormalMessageTransmission;
    status = obd2can_txrx(&servicedata);
    
    return status;
}

//------------------------------------------------------------------------------
// RequestDiagnosticDataPacket ($2A)
// ReadDataByPeriodIdentifier (ISO14229)
// Inputs:  u32 ecm_id
//          u18 datarate
//          u8  *packetidlist
//          u8  packetidcount
//          VehicleCommLevel commlevel
// Return:  u8  status
// Engineer: Quyen Leba
//------------------------------------------------------------------------------
u8 obd2can_ford_requestdiagnosticdatapacket_2A(u32 ecm_id, u8 datarate,
                                               u8 *packetidlist,
                                               u8 packetidcount,
                                               VehicleCommLevel commlevel)
{
    Obd2can_ServiceData servicedata;
    u8  txdata[64];
    u8  status;
    
    if(packetidcount == 0)
        return S_INPUT;
    
    obd2can_servicedata_init(&servicedata);
    servicedata.ecm_id = ecm_id;
    servicedata.service = FORD_RequestDiagnosticDataPacket;
    servicedata.commlevel = commlevel;
    if (commlevel == CommLevel_ISO14229)
    {
        txdata[0] = datarate;
        servicedata.txdata = txdata;
        if (packetidcount > 0 && packetidlist)
        {
            memcpy((char*)&txdata[1],(char*)packetidlist,packetidcount);
        }
        servicedata.txdatalength = 1 + packetidcount;
        
        status = obd2can_txrx(&servicedata);
    }
    else    //if (commlevel == CommLevel_KWP2000)
    {
        //TODOQ: do we need to implement this !?!
        status = S_NOTSUPPORT;
    }
    
    return status;
}

//------------------------------------------------------------------------------
// DynamicallyDefineMessage ($2C)
// Inputs:  u32 ecm_id
//          u16 packetid
//          PidType type
//          u8  size
//          u8  position
//          u32 address
//          VehicleCommLevel commlevel
// Return:  u8  status
// Engineer: Quyen Leba
//------------------------------------------------------------------------------
u8 obd2can_ford_dynamicallydefinemessage(u32 ecm_id, u16 packetid,
                                         PidType type, u8 size, u8 position,
                                         u32 address,
                                         VehicleCommLevel commlevel)
{
    Obd2can_ServiceData servicedata;
    u8  txdata[8];
    u8  status;
    
    obd2can_servicedata_init(&servicedata);
    servicedata.ecm_id = ecm_id;
    servicedata.service = FORD_DynamicallyDefineMessage;
    
    if (commlevel == CommLevel_ISO14229)
    {
        if (type == PidTypeRegular)
        {
            txdata[0] = 0x01;    //PID
            txdata[1] = (u8)(packetid >> 8);
            txdata[2] = (u8)(packetid);
            txdata[3] = (u8)(address >> 8);
            txdata[4] = (u8)(address);
            txdata[5] = position;
            txdata[6] = size;
            
            servicedata.txdata = txdata;
            servicedata.txdatalength = 7;
        }
        else if (type == PidTypeDMR)
        {
            //Note: because we can't specify position while setting up,
            //one DMR will use one packetid
            servicedata.service_controldatabuffer[0] = 0x02;    //DMR
            servicedata.service_controldatabuffer[1] = (u8)(packetid >> 8);
            servicedata.service_controldatabuffer[2] = (u8)(packetid);
            //1-byte length 4-byte address (i.e. 0x14)
            servicedata.service_controldatabuffer[3] = 0x14;
            servicedata.service_controldatalength = 4;
            
            txdata[0] = (u8)(address >> 24);
            txdata[1] = (u8)(address >> 16);
            txdata[2] = (u8)(address >> 8);
            txdata[3] = (u8)(address);
            txdata[4] = (u8)(size);
            servicedata.txdata = txdata;
            servicedata.txdatalength = 5;
        }
        else
        {
            return S_INPUT;
        }
        
        status = obd2can_txrx(&servicedata);
    }
    else    //if (commlevel == CommLevel_KWP2000)
    {
        //TODOQ: do we need to implement this !?!
        status = S_NOTSUPPORT;
    }
    
    if (status == S_ERROR)
    {
        switch (servicedata.errorcode)
        {
        case 0x31:  // Request Out Of Range (PID unsupported or DPID# invalid)
            status = S_OUTOFRANGE;
            break;            
        case 0x12: // Invalid Format (Message exceeds 7 bytes, or no bytes)
            status = S_NOTFIT;
            break;
        default:
            break;
        }
    }
    
    return status;
}

//------------------------------------------------------------------------------
// Clear dynamically defined message for ISO14229 ($2C)
// Inputs:  u32 ecm_id
// Return:  u8 status
// Engineer: Quyen Leba
//------------------------------------------------------------------------------
u8 obd2can_ford_cleandynamicallydefinemessage_14229(u32 ecm_id)
{
    Obd2can_ServiceData servicedata;
    u8  status;

    obd2can_servicedata_init(&servicedata);
    servicedata.ecm_id = ecm_id;
    servicedata.service = FORD_DynamicallyDefineMessage;
    servicedata.subservice = 0x03;
    status = obd2can_txrx(&servicedata);
   
    return status;
}

//------------------------------------------------------------------------------
// InputOutputControlByIdentifier ($2F)
// Inputs:  u32 ecm_id
//          u16 did
//          u8  ctrlParam (0x00 = returnControlToECU, 0x01 = resetToDefault,
//                         0x02 = freezeCurrentState, 0x03 = shortTermAdjustment
//          u8  *data
//          u8  *datalength
//          VehicleCommLevel commlevel
// Return:  u8  status
// Engineer: Tristen Pierson
// Note: This function currently only supports ISO14229
//------------------------------------------------------------------------------
u8 obd2can_ford_input_output_control(u32 ecm_id, u16 did, u8 ctrlParam, u8 *data,
                                     u8 datalength, VehicleCommLevel commlevel,
                                     bool response_expected)
{
    Obd2can_ServiceData servicedata;
    u8 status;

    obd2can_servicedata_init(&servicedata);
    servicedata.ecm_id = ecm_id;
        
    if (datalength > 0 && data != NULL)
    {
        servicedata.txdata = data;
        servicedata.txdatalength = datalength;
    }
    else
    {
        servicedata.txdatalength = 0;
    }

    if (commlevel == CommLevel_ISO14229)
    {
        servicedata.service = FORD_InputOutputControlByIdentifier;    
        servicedata.service_controldatabuffer[0] = (did >> 8);
        servicedata.service_controldatabuffer[1] = did;
        servicedata.service_controldatabuffer[2] = ctrlParam;
        servicedata.service_controldatalength = 3;
        servicedata.commlevel = commlevel;
        servicedata.tester_present_type = TESTER_PRESENT_BROADCAST;
        if (response_expected == TRUE)
        {
            status = obd2can_txrx(&servicedata);
            
            if (status == S_ERROR)
            {
                switch (servicedata.errorcode)
                {        
                case 0x22: // Conditions not correct
                    status = S_REJECT;
                    break;
                case 0x31: // Request out of range
                    status = S_NOTSUPPORT;
                    break;
                case 0x33: // Security Access Denied
                    status = S_UNLOCKFAIL;
                    break;
                default:
                    break;
                }
            }
        }
        else
        {
            servicedata.response_expected = FALSE;
            status = obd2can_txrx(&servicedata);
        }    
    }
    else
    {
        status = S_NOTSUPPORT;
    }
    return status;
}

//------------------------------------------------------------------------------
// StartDiagnosticRoutineByTestNumber ($31)
// Inputs:  u32 ecm_id
//          u8  testnumber
//          u8  *testdata
//          u8  testdatalength
//          VehicleCommLevel commlevel
// Return:  u8  status
// Engineer: Quyen Leba
//------------------------------------------------------------------------------
u8 obd2can_ford_start_diagnostic_routine_bytestnumber(u32 ecm_id, u8 testnumber,
                                                      u8 *testdata,
                                                      u8 testdatalength,
                                                      VehicleCommLevel commlevel)
{
    Obd2can_ServiceData servicedata;
    u8 status;

    obd2can_servicedata_init(&servicedata);
    servicedata.ecm_id = ecm_id;
    servicedata.txdata = testdata;
    servicedata.txdatalength = testdatalength;
    servicedata.service = FORD_StartDiagnosticRoutineByTestNumber;
    servicedata.subservice = testnumber;
    servicedata.commlevel = commlevel;
    if (commlevel == CommLevel_ISO14229)
    {
        servicedata.tester_present_type = TESTER_PRESENT_BROADCAST;
    }
    else
    {
        //do nothing for now
    }
    status = obd2can_txrx(&servicedata);
    
    if (status == S_ERROR)
    {
        switch (servicedata.errorcode)
        {        
        case 0x12: // Subfunction not supported
        case 0x31: // Request out of range
            status = S_NOTSUPPORT;
            break;
        default:
            break;
        }
    }
    
    return status;
}

//------------------------------------------------------------------------------
// RequestDownload ($34)
// Inputs:  u32 ecm_id
//          u8  dataformat
//          u32 requestaddress
//          u32 requestlength
//          RequestLengthType reqtype
//          VehicleCommLevel commlevel
// Output:  u16 *maxdownloadblocklength
// Return:  u8  status
// Engineer: Quyen Leba
//------------------------------------------------------------------------------
u8 obd2can_ford_request_download(u32 ecm_id, u8 dataformat,
                                 u32 requestaddress, u32 requestlength,
                                 RequestLengthType reqtype,
                                 VehicleCommLevel commlevel,
                                 u16 *maxdownloadblocklength)
{
    Obd2can_ServiceData servicedata;
    u8  requestdata[8];
    u8  status;
    u16 maxlength;

    maxlength = INVALID_MEMBLOCK_SIZE;
    
    obd2can_servicedata_init(&servicedata);
    servicedata.ecm_id = ecm_id;
    servicedata.service = FORD_RequestDownload;
    servicedata.tester_present_type = TESTER_PRESENT_NODE;
    servicedata.commlevel = commlevel;    
    if (commlevel == CommLevel_ISO14229)
    {
        // dataFormatIdentifier
        servicedata.service_controldatabuffer[0] = dataformat;
        // addressAndLengthFormatIdentifier, 4 bytes for both addr & len
        servicedata.service_controldatabuffer[1] = 0x44;
        servicedata.service_controldatalength = 2;
    }
    
    requestdata[0] = (u8)(requestaddress >> 24);
    requestdata[1] = (u8)(requestaddress >> 16);
    requestdata[2] = (u8)(requestaddress >> 8);
    requestdata[3] = (u8)(requestaddress & 0xFF);
    if (reqtype == Request_4_Byte)
    {
        if (commlevel == CommLevel_ISO14229)
        {
            requestdata[4] = (u8)(requestlength >> 24);
        }
        else if (commlevel == CommLevel_KWP2000)
        {
            // dataFormatIdentifier - should always be 0x00
            // 6.4L when download 2boot.bin, this byte is 0x01
            requestdata[4] = dataformat;
        }
        else
        {
            return S_COMMLEVEL;
        }
        requestdata[5] = (u8)(requestlength >> 16);
        requestdata[6] = (u8)(requestlength >> 8);
        requestdata[7] = (u8)(requestlength & 0xFF);
    }
    else
    {
        return S_BADCONTENT;
    }

    servicedata.txdata = requestdata;
    servicedata.txdatalength = 8;
    if (commlevel == CommLevel_ISO14229)
    {
        servicedata.tester_present_type = TESTER_PRESENT_BROADCAST;
    }
    else
    {
        //do nothing for now
    }
    
    status = obd2can_txrx(&servicedata);
    
    if (status == S_SUCCESS)
    {
        if (commlevel == CommLevel_ISO14229)
        {
            //expect: [7E8] [04 74 20 04 02]
            //servicedata.rxdata: [20 04 02]
            maxlength = ((u16)servicedata.rxdata[1]) << 8 |
                (u16)servicedata.rxdata[2];
            maxlength -= 2; // ISO14229 has 2 bytes over head in $36 service
        }
        else if (commlevel == CommLevel_KWP2000)
        {
            //expect: [7E8] [03 74 04 01]
            //servicedata.rxdata: [04 01]
            maxlength = ((u16)servicedata.rxdata[0]) << 8 |
                (u16)servicedata.rxdata[1];
            maxlength -= 1; // KWP2000 has 1 byte over head in $36 service
        }
        else
        {
            status = S_COMMLEVEL;
        }
    }
    
    if (maxdownloadblocklength)
    {
        *maxdownloadblocklength = maxlength;
    }
    
    return status;
}

//------------------------------------------------------------------------------
// RequestUpload ($35)
// Inputs:  u32 ecm_id
//          u32 requestaddress
//          u32 requestlength
//          RequestLengthType reqtype
//          VehicleCommLevel commlevel
// Output:  u16 *maxuploadblocklength
// Return:  u8  status
// Engineer: Quyen Leba
//------------------------------------------------------------------------------
u8 obd2can_ford_request_upload(u32 ecm_id,
                               u32 requrestaddress, u32 requestlength,
                               RequestLengthType reqtype,
                               VehicleCommLevel commlevel,
                               u16 *maxuploadblocklength)
{
    Obd2can_ServiceData servicedata;
    u8  requestbuffer[8];
    u8  status;

    if (maxuploadblocklength)
    {
        *maxuploadblocklength = INVALID_MEMBLOCK_SIZE;
    }
    
    obd2can_servicedata_init(&servicedata);
    servicedata.ecm_id = ecm_id;
    servicedata.service = FORD_RequestUpload;
    servicedata.tester_present_type = TESTER_PRESENT_NODE;
    servicedata.commlevel = commlevel;
    if (reqtype == Request_4_Byte)
    {
        requestbuffer[0] = (u8)(requrestaddress >> 24);
        requestbuffer[1] = (u8)(requrestaddress >> 16);
        requestbuffer[2] = (u8)(requrestaddress >> 8);
        requestbuffer[3] = (u8)(requrestaddress & 0xFF);
        
        if (commlevel == CommLevel_ISO14229)
        {
            // dataFormatIdentifier
            servicedata.service_controldatabuffer[0] = 0x00;
            // addressAndLengthFormatIdentifier, 4 bytes for both addr & len
            servicedata.service_controldatabuffer[1] = 0x44;
            servicedata.service_controldatalength = 2;
            
            requestbuffer[4] = (u8)(requestlength >> 24);
        }
        else if (commlevel == CommLevel_KWP2000)
        {
            // dataFormatIdentifier - should always be 0x00
            requestbuffer[4] = 0x00;
        }
        else
        {
            return S_COMMLEVEL;
        }
        requestbuffer[5] = (u8)(requestlength >> 16);
        requestbuffer[6] = (u8)(requestlength >> 8);
        requestbuffer[7] = (u8)(requestlength & 0xFF);
        
        servicedata.txdata = requestbuffer;
        servicedata.txdatalength = 8;
    }
    else
    {
        return S_BADCONTENT;
    }

    status = obd2can_txrx(&servicedata);
    if (status == S_SUCCESS && maxuploadblocklength)
    {
        //Grab the maxNumberOfBlockLength
        //max blocklength 4 transferData (0x36)
        if (commlevel == CommLevel_ISO14229)
        {
            *maxuploadblocklength = 
                (servicedata.rxdata[1] << 8) + servicedata.rxdata[2];
        }
        else if (commlevel == CommLevel_KWP2000)
        {
            *maxuploadblocklength = 
                (servicedata.rxdata[0] << 8) + servicedata.rxdata[1];
        }
        else
        {
            return S_COMMLEVEL;
        }
        *maxuploadblocklength &= 0xFFFC;
    }
    
    return status;
}

//------------------------------------------------------------------------------
// TransferData Upload ($36)
// Inputs:  u32 ecm_id
//          u8  blocktracking
//          VehicleCommLevel commlevel
//          u32 request_address (UNUSED)
//          RequestLengthType request_type (UNUSED)
// Outputs: u8  *data
//          u16 *datalength
// Return:  u8  status
// Engineer: Quyen Leba
//------------------------------------------------------------------------------
u8 obd2can_ford_transfer_data_upload(u32 ecm_id, u8 blocktracking,
                                     VehicleCommLevel commlevel,
                                     u32 request_address,
                                     RequestLengthType request_type,
                                     u8 *data, u16 *datalength)
{
    Obd2can_ServiceData servicedata;
    u8 status;

    *datalength = 0;
    obd2can_servicedata_init(&servicedata);
    servicedata.ecm_id = ecm_id;
    servicedata.service = FORD_TransferData;
    servicedata.rxdata = data;
    servicedata.commlevel = commlevel;
    if (commlevel == CommLevel_ISO14229)
    {
        servicedata.service_controldatabuffer[0] = blocktracking;
        servicedata.service_controldatalength = 1;
        //skip the blocktracking byte in response frame; we don't want that
        //byte to be in the stock data block
        servicedata.first_frame_data_offset = 1;
    }
    else if (commlevel == CommLevel_KWP2000)
    {
        //TODOQ: more on this later
        return S_NOTSUPPORT;
    }
    else
    {
        return S_COMMLEVEL;
    }
    
    status = obd2can_txrxcomplex(&servicedata);
    if (status == S_SUCCESS)
    {
        *datalength = servicedata.rxdatalength;
    }
    
    return status;
}

//------------------------------------------------------------------------------
// TransferData Download ($36)
// Inputs:  u32 ecm_id
//          Obd2can_SubServiceNumber subservice (UNUSED)
//          u8  blocktracking
//          u32 startingaddr
//          MemoryAddressType addrtype
//          u8  *data
//          u16 datalength
//          RequestLengthType reqtype
//          VehicleCommLevel commlevel
//          bool response_expected
// Return:  u8  status
// Engineer: Quyen Leba
//------------------------------------------------------------------------------
u8 obd2can_ford_transfer_data_download(u32 ecm_id,
                                       Obd2can_SubServiceNumber subservice,
                                       u8 blocktracking, u32 startingaddr,
                                       MemoryAddressType addrtype,
                                       u8 *data, u16 datalength,
                                       VehicleCommLevel commlevel,
                                       bool response_expected)
{
    Obd2can_ServiceData servicedata;
    u8 status;

    obd2can_servicedata_init(&servicedata);
    servicedata.ecm_id = ecm_id;
    servicedata.service = FORD_TransferData;
    servicedata.txdata = data;
    servicedata.txdatalength = datalength;
    servicedata.response_expected = response_expected;
    servicedata.commlevel = commlevel;    
    if (addrtype == Address_32_Bits)
    {
        if (commlevel == CommLevel_ISO14229)
        {
            servicedata.service_controldatabuffer[0] = blocktracking;
            servicedata.service_controldatalength = 1;
        }
        else if (commlevel == CommLevel_KWP2000)
        {
            //do nothing
        }
        else
        {
            return S_COMMLEVEL;
        }
    }
    else
    {
        return S_BADCONTENT;
    }

    if (commlevel == CommLevel_ISO14229)
    {
        servicedata.tester_present_type = TESTER_PRESENT_BROADCAST;
    }
    else
    {
        //do nothing for now
    }
    
    status = obd2can_txrx(&servicedata);
    return status;
}

//------------------------------------------------------------------------------
// RequestTransferExit ($37)
// Inputs:  u32 ecm_id
//          VehicleCommLevel commlevel (UNUSED)
// Return:  u8  status
// Engineer: Quyen Leba
//------------------------------------------------------------------------------
u8 obd2can_ford_request_transfer_exit(u32 ecm_id, VehicleCommLevel commlevel)
{
    Obd2can_ServiceData servicedata;
    u8 status;

    obd2can_servicedata_init(&servicedata);
    servicedata.ecm_id = ecm_id;
    servicedata.service = FORD_RequestDataTransferExit;
    servicedata.commlevel = commlevel;
    if (commlevel == CommLevel_ISO14229)
    {
        servicedata.tester_present_type = TESTER_PRESENT_BROADCAST;
    }
    else
    {
        //do nothing for now
    }
    
    status = obd2can_txrx(&servicedata);
    if(status == S_SUCCESS)
    {
        if (commlevel == CommLevel_ISO14229)
        {
            if(servicedata.rxdata[0] == 0x00 && servicedata.rxdata[1] == 0x00)
            {
                log_push_error_point(0x3050);
                SETTINGS_SetDatalogAreaDirty();
                SETTINGS_SetTuneAreaDirty();
                settings_update(FALSE);
                status = S_FAIL; // Check CRC for block, can't be all zeros
            }
        }
    }
    
    return status;
}

//------------------------------------------------------------------------------
// FORD_WriteDataByIdentifier ($3B)
// Inputs:  u32 ecm_id
//          u8  data_id
//          u8  *data
//          u8  datalength
// Return:  u8  status
// Engineer: Quyen Leba
//------------------------------------------------------------------------------
u8 obd2can_ford_write_data_by_local_id(u32 ecm_id, u8 data_id,
                                       u8 *data, u32 datalength)
{
    Obd2can_ServiceData servicedata;
    u8 status;

    obd2can_servicedata_init(&servicedata);
    servicedata.ecm_id = ecm_id;
    servicedata.service = FORD_WriteDataByIdentifier;

    servicedata.service_controldatabuffer[0] = data_id;
    servicedata.service_controldatalength = 1;

    servicedata.txdata = data;
    servicedata.txdatalength = datalength;
    servicedata.response_expected = TRUE;

    status = obd2can_txrx(&servicedata);
    return status;
}

//------------------------------------------------------------------------------
// RequestDiagnosticDataPacket ($A0)
// Inputs:  u32 ecm_id
//          u8  packetid
//          PidType type
//          u8  size
//          u8  position
//          u32 address
//          VehicleCommLevel commlevel
// Return:  u8  status
// Engineer: Quyen Leba

//------------------------------------------------------------------------------
u8 obd2can_ford_requestdiagnosticdatapacket(u32 ecm_id, u8 datarate,
                                            u8 *packetidlist, u8 packetidcount,
                                            VehicleCommLevel commlevel)
{
    Obd2can_ServiceData servicedata;
    u8  status;
    
    obd2can_servicedata_init(&servicedata);
    servicedata.ecm_id = ecm_id;
    servicedata.service = FORD_RequestDiagnosticDataPacket_A0;
    servicedata.service_controldatabuffer[0] = datarate;
    servicedata.service_controldatalength = 1;
    
    if (packetidcount > 5)
    {
        return S_INPUT;
    }
    if (packetidcount > 0)
    {
        memcpy((char*)&servicedata.service_controldatabuffer[1],
               packetidlist,packetidcount);
        servicedata.service_controldatalength += packetidcount;
    }
    else
    {
        if(datarate == 0x08)
        {
            servicedata.response_expected = FALSE;            
        }
        else
            servicedata.response_expected = TRUE;
    }
    
    status = obd2can_txrx(&servicedata);
    
    if(datarate == 0x08)
        delays(250, 'm');
        
    return status;
}

//------------------------------------------------------------------------------
// DynamicallyDefineDiagnosticDataPacket ($A1)
// Inputs:  u32 ecm_id
//          u8  packetid (1,2,...)
//          PidType type
//          u8  size (1,2,4)
//          u8  position (start from 1)
//          u32 address
//          VehicleCommLevel commlevel
// Return:  u8  status
// Engineer: Quyen Leba
//------------------------------------------------------------------------------
u8 obd2can_ford_dynamicallydefinediagnosticdatapacket(u32 ecm_id,
                                                      u8 packetid,
                                                      PidType type,
                                                      u8 size, u8 position,
                                                      u32 address,
                                                      VehicleCommLevel commlevel)
{
    Obd2can_ServiceData servicedata;
    u8  tpl;
    u8  status;
    
    tpl = (size) | (((u8)type) << 6) | (position << 3);
    
    obd2can_servicedata_init(&servicedata);
    servicedata.ecm_id = ecm_id;
    servicedata.service = FORD_DynamicallyDefineDiagnosticDataPacket;
    servicedata.service_controldatabuffer[0] = packetid;
    servicedata.service_controldatabuffer[1] = tpl;
    servicedata.commlevel = commlevel;
    
    if (type == PidTypeRegular)
    {
        servicedata.service_controldatabuffer[2] = (u8)(address >> 8);
        servicedata.service_controldatabuffer[3] = (u8)(address);
        servicedata.service_controldatalength = 4;
    }
    else if (type == PidTypeDMR)
    {
        servicedata.service_controldatabuffer[2] = (u8)(address >> 24);
        servicedata.service_controldatabuffer[3] = (u8)(address >> 16);
        servicedata.service_controldatabuffer[4] = (u8)(address >> 8);
        servicedata.service_controldatabuffer[5] = (u8)(address);
        servicedata.service_controldatalength = 6;
    }
    else
    {
        return S_INPUT;
    }
    
    status = obd2can_txrx(&servicedata);
    
    if (status == S_ERROR)
    {
        switch (servicedata.errorcode)
        {
        case 0x31:  // Request Out Of Range (PID unsupported or DPID# invalid)
            status = S_OUTOFRANGE;
            break;            
        case 0x12: // Invalid Format (Message exceeds 7 bytes, or no bytes)
            status = S_NOTFIT;
            break;
        default:
            break;
        }
    }
    else if (status == S_SUCCESS && (servicedata.rxdatalength == 0 || servicedata.rxdata[0] != packetid))
    // Make sure packetid is echoed. Some processors will give a positive response
    // with no packetid when the packet is not supported.
    {
        status = S_UNMATCH;      
    }
           
    return status;
}

//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
void obd2can_ford_testerpresent(bool broadcast, u32 ecm_id,
                                VehicleCommLevel commlevel)
{
    Obd2can_ServiceData servicedata;

    obd2can_servicedata_init(&servicedata);
    servicedata.commlevel = commlevel;
    if (broadcast && (commlevel != CommLevel_KWP2000))
    {
        servicedata.ecm_id = 0x7DF;
    }
    else
    {
        servicedata.ecm_id = ecm_id;
    }
    servicedata.service = 0x3E;
    if(commlevel == CommLevel_KWP2000)
    {
        servicedata.subservice = 0x02; // No response required KWP2000
    }
    else
    {
        servicedata.subservice = 0x80; // No response required ISO14229
    }
    servicedata.response_expected = FALSE;
    obd2can_txrx(&servicedata);
    
    delays(30,'m');     //this delay is important
}

/**
 *  @brief Clear DTCs on Ford CAN processors
 *
 *  @return Status
 */
u8 obd2can_ford_cleardtc()
{
    u8 i;
    u8 status;

    obd2_open();

    status = S_COMMLEVEL;
    for (i = 0; i < ECM_MAX_COUNT; i++)
    {
        if (gObd2info.ecu_block[i].ecutype != EcuType_Unknown)
        {
            switch(gObd2info.ecu_block[i].commlevel)
            {
            case CommLevel_ISO14229:
                status = obd2can_ford_clear_diagnostic_information_iso14229(TRUE, gObd2info.ecu_block[i].ecu_id);
                if (status == S_SUCCESS)
                {
                    status = obd2can_ford_clear_diagnostic_information(FALSE, gObd2info.ecu_block[i].ecu_id);
                }
                break;
            case CommLevel_KWP2000:
                status = S_SUCCESS;
                if (gObd2info.ecu_block[i].ecutype == EcuType_ECM)
                {
                    status = obd2can_ford_clear_diagnostic_information_extended(TRUE, gObd2info.ecu_block[i].ecu_id);
                }
                if (status == S_SUCCESS)
                {
                    status = obd2can_ford_clear_diagnostic_information_extended(FALSE, gObd2info.ecu_block[i].ecu_id);
                }
                if (status == S_SUCCESS)
                {                            
                    status = obd2can_ford_clear_diagnostic_information(FALSE, gObd2info.ecu_block[i].ecu_id);
                }
                break;
            default:
                break;
            }
        }
    }
    return status;
}

//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
u8 obd2can_ford_alt_readdtc(u32 ecm_id, dtc_info *info)
{
    Obd2can_ServiceData servicedata;
    u8 status;
    u8 i;
    u8 dtccount;
    u16 code;
    u32 codeindex;

    obd2can_servicedata_init(&servicedata);
    servicedata.ecm_id = ecm_id;
    servicedata.service = 0x18;
    // Fords only support these setup bytes, don't change control data
    servicedata.service_controldatabuffer[0] = (u8)0x00; // Status
    servicedata.service_controldatabuffer[1] = (u8)0xFF; // Group - 0xFF = ALL
    servicedata.service_controldatabuffer[2] = (u8)0x00; // 0x00 inlcuded by by spec
    servicedata.service_controldatalength = 3;
    status = obd2can_txrx(&servicedata);
    if (status == S_SUCCESS)
    {
        dtccount = obd2_rxbuffer[0]; // Number of DTCs reported
        info->count = dtccount;
        if (dtccount)
        {
            for(i=0; i<dtccount; i++)
            {
                // DTCs receieved in this format:
                // [DTC MSB] [DTC LSB] [Status Byte]
                // Refer to SAEJ2190 spec Mode $18 Section 5.8 Table 1 for a
                // CLOSE BUT NOT EXACTLY message format
                codeindex = (1 + (i*3));
                code = (u16)(obd2_rxbuffer[codeindex] << 8) & 0xFF00;
                code |= (u16)(obd2_rxbuffer[codeindex + 1] & 0x00FF);
                info->codes[i] = code;
                info->codestatus[i] = obd2_rxbuffer[codeindex + 2];
            }
        }
    }
    return status;
}

//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
u8 obd2can_ford_disableallnormalcomm()
{
    return S_NOTREQUIRED;
}

//------------------------------------------------------------------------------
// Get VID address
// Input:   u32 ecm_id
// Output:  u32 *vidaddress
// Return:  u8  status
// Engineer: Quyen Leba
//------------------------------------------------------------------------------
u8 obd2can_ford_getvidaddress(u32 ecm_id, u32 *vidaddress)
{
    u8  buffer[256];
    u8  bufferlength;
    u32 address;
    u8  status;
    
    address = 0xFFFFFFFF;
    status = obd2can_read_data_bypid(ecm_id, 0x1100, buffer, &bufferlength, TRUE);
    if (status == S_SUCCESS)
    {
        switch(bufferlength)
        {
        case 4:
            address = ((u32)buffer[0] << 24) | ((u32)buffer[1] << 16) | 
                ((u32)buffer[2] << 8) | ((u32)buffer[3]);
            break;
        case 3:
            address = ((u32)buffer[0] << 16) | ((u32)buffer[1] << 8) | 
                ((u32)buffer[2]);
            break;
        default:
            status = S_BADCONTENT;
            break;
        }
    }
    
    if (vidaddress)
    {
        *vidaddress = address;
    }
    
    return status;
}

//------------------------------------------------------------------------------
// Get Module Partnumber (ISO14229 only for now)
// Input:   u32 ecm_id
// Output:  u8  *hwpartnumber
// Return:  u8  status
// Engineer: Quyen Leba, Patrick Downs, Tristen Pierson
//------------------------------------------------------------------------------
u8 obd2can_ford_getmodulepartnumber(u32 ecm_id, u8 *modulepartnumber)
{
    u8  buffer[256];
    u8  bufferlength;
    u16 pidlist[1] = {0xF113}; // Taken from sniffs of PTDIAG read module and IDS
    u8  i;
    u8  status;
    
    modulepartnumber[0] = NULL;
    for(i=0;i<(sizeof(pidlist)/2);i++)
    {
        status = obd2can_read_data_bypid(ecm_id, pidlist[i],
                                         buffer, &bufferlength, TRUE);
        if (status == S_SUCCESS)
        {
            memcpy(modulepartnumber,buffer,bufferlength);
            modulepartnumber[bufferlength] = NULL;
            break;
        }
    }
    for(i=0;i<strlen((char*)modulepartnumber);i++)
    {
        if (!isprint(modulepartnumber[i]))
        {
            modulepartnumber[i] = NULL;
            break;
        }
    }
    
    return status;
}

//------------------------------------------------------------------------------
// Get Hardware Type
// Input:   u32 ecm_id
// Output:  u8  *hwtype
// Return:  u8  status
// Engineer: Quyen Leba, Patrick Downs, Tristen Pierson
//------------------------------------------------------------------------------
u8 obd2can_ford_gethardwarepartnumber(u32 ecm_id, u8 *hwpartnumber)
{
    u8  buffer[256];
    u8  bufferlength;
    u16 pidlist[2] = {0xE610, 0xF111}; // Taken from sniffs of PTDIAG read module and IDS
    u8  i;
    u8  status;
    
    hwpartnumber[0] = NULL;
    for(i=0;i<(sizeof(pidlist)/2);i++)
    {
        status = obd2can_read_data_bypid(ecm_id, pidlist[i],
                                         buffer, &bufferlength, TRUE);
        if (status == S_SUCCESS)
        {
            memcpy(hwpartnumber,buffer,bufferlength);
            hwpartnumber[bufferlength] = NULL;
            break;
        }
    }
    for(i=0;i<strlen((char*)hwpartnumber);i++)
    {
        if (!isprint(hwpartnumber[i]))
        {
            hwpartnumber[i] = NULL;
            break;
        }
    }
    
    return status;
}

//------------------------------------------------------------------------------
// Get Software Partnumber
// Input:   u32 ecm_id
// Output:  u8  *swpartnumber
// Return:  u8  status
// Engineer: Quyen Leba
//------------------------------------------------------------------------------
u8 obd2can_ford_getsoftwarepartnumber(u32 ecm_id, u8 *swpartnumber)
{
    u8  buffer[256];
    u8  bufferlength;
    u16 pidlist[4] = {0xE611, 0xF191, 0xF188, 0xF124}; // Taken from sniffs of PTDIAG read module and IDS
    u8  i;
    u8  status;
    
    swpartnumber[0] = NULL;
    for(i=0;i<(sizeof(pidlist)/2);i++)
    {
        status = obd2can_read_data_bypid(ecm_id, pidlist[i],
                                         buffer, &bufferlength, TRUE);
        if (status == S_SUCCESS)
        {
            memcpy(swpartnumber,buffer,bufferlength);
            swpartnumber[bufferlength] = NULL;
            break;
        }
    }
    for(i=0;i<strlen((char*)swpartnumber);i++)
    {
        if (!isprint(swpartnumber[i] ))
        {
            swpartnumber[i] = NULL;
            break;
        }
    }
    
    return status;
}

//------------------------------------------------------------------------------
// Read Strategy by Search
// This function is not intended for general use, but for processors that we
// cannot get strategy with regular method. This function only supports such
// processors.
// Input:   u32 ecm_id
// Output:  u8  *strategy
// Return:  u8  status
// Engineer: Quyen Leba
//------------------------------------------------------------------------------
u8 obd2can_ford_read_strategy_bysearch(u32 ecm_id, u8 *strategy, VehicleCommLevel commlevel)
{
    u8  *buffer;
    u16 bufferlength;
    u8  status;
    u32 vidaddress;
    u32 strategyaddress;
    u16 readlength;
    u32 count;
    u32 i;
    u8  *bptr;
    bool force_readby4bytes;
    u16 ecm_type;

    buffer = __malloc(4096);
    if(!buffer)
    {
        return S_MALLOC;
    }
    
    //##########################################################################
    // Trial 1: if it's possible to read VID address
    //##########################################################################
    status = obd2can_ford_getvidaddress(ecm_id,&vidaddress);
    if (status == S_SUCCESS)
    {
        force_readby4bytes = FALSE;
        //######################################################################
        // Manipulate with address and try to read data from new address
        // That data could be strategy
        //######################################################################
        if (ecm_id == 0x7E0 && vidaddress == 0x00840180)
        {
            //assume we have a 6.4L PCM
            strategyaddress = vidaddress - 0x138;
            readlength = 10;
        }
        else //if (ecm_id == 0x7E0 && vidaddress == 0x01004005)
        {
            strategyaddress = vidaddress >> 8;
            readlength = 32;
            force_readby4bytes = TRUE;
        }
        
        status = obd2can_ford_read_memory_address
            (ecm_id, strategyaddress, readlength, 
             buffer, &bufferlength, force_readby4bytes, commlevel);
        if (status != S_SUCCESS)
        {
            goto obd2can_ford_read_strategy_bysearch_done;
        }
        
        buffer[readlength] = 0;
        //######################################################################
        // Validate the data for possible strategy
        //######################################################################
        if (ecm_id == 0x7E0 && vidaddress == 0x00840180)
        {
            count = 0;
            for(i=0;i<readlength;i++)
            {
                if (isgraph(buffer[i]))
                {
                    count++;
                }
            }
            if (count == readlength && buffer[6] == '.' && buffer[7] == 'D')
            {
                memcpy((char*)strategy,(char*)buffer,6);
                strategy[6] = NULL;
            }
            else
            {
                status = S_BADCONTENT;
            }
        }
        else// if (ecm_id == 0x7E0 && vidaddress == 0x01004052)
        {
            for(i=6;i<readlength;i++)
            {
                if (!isprint(buffer[i]))
                {
                    buffer[i] = NULL;
                    break;
                }
            }
            bptr = (u8*)strstr((char*)&buffer[6],".H");
            if (bptr)
            {
                *bptr = NULL;
            }
            strcpy((char*)strategy,(char*)&buffer[6]);
        }
    }
    //##########################################################################
    // Trial 2: can't read VID address
    //##########################################################################
    else
    {
        if(ecm_id == Obd2CanEcuId_7E1)
        {            
            status = obd2can_ford_getvidaddress(Obd2CanEcuId_7E0,&vidaddress);
            /* Read 6.4L TCM Strategy (6.4L PCM + TCM Stack) */
            if (status == S_SUCCESS && vidaddress == 0x00840180)
            {
                force_readby4bytes = FALSE;
                status = obd2can_ford_wakeup(0x2B,1);
                if (status == S_SUCCESS)
                {
                    ecm_type = VEH_GetEcmType(0x2B,1); /* 6.4L TCM */
                    status = obd2can_unlockecm(UnlockTask_Upload,ecm_type,FALSE);
                    if (status == S_SUCCESS)
                    {
                        status = obd2can_ford_read_memory_address
                            (ecm_id, ECM_GetVidAddress(ecm_type), 6,
                             buffer, &bufferlength,force_readby4bytes, commlevel);
                        if (status == S_SUCCESS)
                        {
                            memcpy((char*)strategy,(char*)buffer,6);
                            strategy[5] = NULL;
                        }
                        obd2can_resetecm(ecm_id);
                    }
                }
            }
            /* Read Silver Oak 1024 TCM Strategy 
             * (09 F150 TCM / Green Oak 2368K + Silver Oak 1024 Stack)
             */
            else
            {
                //00 00 07 E0 03 22 11 00 00 00 00 00
                //00 00 07 E8 03 7F 22 31 00 00 00 00
                const u8 search_string[8] = {0x30, 0x41, 0x70, 0x40, 0x00, 0x00, 0x00, 0};
                force_readby4bytes = FALSE;
                ecm_type = VEH_GetEcmType(0x26,1);
                
                count = 0;
                while(count < (4096/254)*254)   /* 254 = 0xFE */
                {
                    status = obd2can_ford_read_memory_address
                        (ecm_id, ECM_GetVidAddress(ecm_type)+count, 0xFE,
                         &buffer[count], &bufferlength,
                         force_readby4bytes, commlevel);
                    if (status != S_SUCCESS)
                    {
                        goto obd2can_ford_read_strategy_bysearch_done;
                    }
                    count += 0xFE;
                }
                for(i=0;i<(4096/254)*254-7;i++)
                {
                    if (memcmp((char*)search_string,(char*)&buffer[i],7) == 0)
                    {
                        memcpy((char*)strategy,(char*)&buffer[i+7+6],6);
                        strategy[5] = NULL;
                        status = S_SUCCESS;
                        break;
                    }
                }//for(i=...
            }
        }//if (ecm_id == Obd2CanEcuId_7E1)...
        /* Read 2014 Focus ST Strategy */
        else
        {
             status = obd2can_ford_read_memory_address
             (ecm_id, 0x801FFF03, 0x0B, 
             buffer, &bufferlength, force_readby4bytes, commlevel);
             
              if (buffer[7] == '.' && buffer[8] == 'H')
              {
                 memcpy((char*)strategy,(char*)buffer,7);
                 strategy[7] = NULL;
              }
              else
              {
                 status = S_BADCONTENT;
              }
        }
    }  
    
obd2can_ford_read_strategy_bysearch_done:
    if(buffer)
    {
        __free(buffer);
    }
    
    return status;
}

//------------------------------------------------------------------------------
// Ford Read VIN
// This function reads VINs via $09 $02 and in cases where the VIN is invalid,
// will read some KWP2000 processor VID blocks.
// Output:  u8  *vin
// Return:  u8  status
// Engineer: Tristen Pierson
//------------------------------------------------------------------------------
u8 obd2can_ford_readvin(u8 *vin)
{
    Obd2can_ServiceData servicedata;
    VehicleCommLevel commlevel;
    u8  retry;
    u8  status;
    u32 vidaddress;
    u16 readlength;

    obd2can_servicedata_init(&servicedata);
    servicedata.nowait_cantx = TRUE;
    servicedata.ecm_id = 0x7E0;
    servicedata.broadcast_id = 0x7DF;
    servicedata.service = 0x09;
    servicedata.subservice = 0x02;
    servicedata.first_frame_data_offset = 2;

    retry = 3;
obd2can_readvin_retry:
    status = obd2can_txrx(&servicedata);
    
    vin[0] = NULL;
    if (status == S_SUCCESS)
    {
        if (servicedata.rxdatalength == VIN_LENGTH)
        {
            memcpy(vin,servicedata.rxdata,VIN_LENGTH);
            vin[VIN_LENGTH] = NULL;
            status = obd2_validatevin(vin); 
            if (status == S_VINBLANK)
            {
                status = S_SUCCESS;
                if (obd2can_readvin_indirect_blankvin_overlay(vin) == S_SUCCESS)
                {
                    // TODO: Is this redundant?
                    status = S_SUCCESS;
                }
            }            
            else if (status == S_VININVALID)
            {
                // Some KWP2000 processors do not return VIN via $09 $02. For these,
                // read the VID block.
                OemType tempOem;
                status = obd2can_getcommlevelinfo(Obd2CanEcuId_7E0, &commlevel, &tempOem);
                if (status == S_SUCCESS && commlevel == CommLevel_KWP2000)
                {
                    obd2can_ford_getvidaddress(Obd2CanEcuId_7E0, &vidaddress);
                    if (vidaddress == 0x01004005)
                    {
                        // Get vid address: 
                        // vinaddress = (vidaddress >> 8) + 0x80
                        // vinaddress = 0x000100C0                    
                        status = obd2can_ford_read_memory_address(
                                    Obd2CanEcuId_7E0, 0x000100C0, VIN_LENGTH, 
                                    vin, &readlength, TRUE, CommLevel_KWP2000);
                        vin[VIN_LENGTH] = NULL;
                    
                        if (status == S_SUCCESS)
                        { 
                            status = obd2_validatevin(vin);
                        }
                        else
                        {
                            status = S_FAIL;
                        }
                    }
                    else
                    {
                        status = S_VININVALID;
                    }
                }
                else
                {
                    status = S_VININVALID;
                }
            } // else if (status == S_VININVALID)
        }
        else
        {
            status = S_BADCONTENT;
        }
        return status;
    }    
    
    // Some Ford Chinese Market vehicles only respond to OBDII VIN request 
    // by functional address request.
    servicedata.broadcast = TRUE;
    status = obd2can_txrx(&servicedata);
    vin[0] = NULL;
    if (status == S_SUCCESS)
    {
        if (servicedata.rxdatalength == VIN_LENGTH)
        {
            memcpy(vin,servicedata.rxdata,VIN_LENGTH);
            vin[VIN_LENGTH] = NULL;
        }
        else
        {
            status = S_BADCONTENT;
        }
    }

    if (status != S_SUCCESS)
    {
        if (retry--)
        {
            delays(500,'m');
            goto obd2can_readvin_retry;
        }
    }
    return status;
}

/**
 *  @brief Read ECM info from OBD2 info
 *  
 *  @param [in]  ecm_info   ECM info structure
 *
 *  @return status
 *  
 *  @details This function reads all available part numbers and serial number.
 */
u8 obd2can_ford_readecminfo(ecm_info *ecm)
{
    ecm->ecm_count = gObd2info.ecu_count;
    memcpy(ecm->vin, gObd2info.vin, sizeof(ecm->vin));
    
    for(u8 i = 0; i < ECM_MAX_COUNT; i++)
    {
        if (gObd2info.ecu_block[i].ecutype != EcuType_Unknown)
        {
            ecm->commtype[i] = gObd2info.ecu_block[i].commtype;
            ecm->commlevel[i] = gObd2info.ecu_block[i].commlevel;
            ecm->codecount[i] = gObd2info.ecu_block[i].partnumber_count;
            memcpy(ecm->codes[i][0],
                   gObd2info.ecu_block[i].partnumbers.ford.strategy,
                   sizeof(gObd2info.ecu_block[i].partnumbers.ford.strategy));
            memcpy(ecm->second_codes[i][0],
                   gObd2info.ecu_block[i].partnumbers.ford.sw_part_number,
                   sizeof(gObd2info.ecu_block[i].partnumbers.ford.sw_part_number));
            memcpy(ecm->second_codes[i][1],
                   gObd2info.ecu_block[i].partnumbers.ford.hw_part_number,
                   sizeof(gObd2info.ecu_block[i].partnumbers.ford.hw_part_number));
            memcpy(ecm->second_codes[i][2],
                   gObd2info.ecu_block[i].partnumbers.ford.module_part_number,
                   sizeof(gObd2info.ecu_block[i].partnumbers.ford.module_part_number));
        }
    }
    return S_SUCCESS;
}

//------------------------------------------------------------------------------
// Check for vehicle wake up (CAN)
// Inputs:  u8  veh_type
//          u8  ecm_index
// Return:  u8  status
// Engineer: Quyen Leba
// Note: in vehicles that require FEPS, it'll require this wakeup to make sure
// it's ready for upload/download; only do it once
//------------------------------------------------------------------------------
u8 obd2can_ford_wakeup(u8 veh_type, u8 ecm_index)
{
    VehicleCommLevel commlevel;
    u32 ecm_id;
    u32 i;
    u8  retry;
    u8  status;
    
    ecm_id = ECM_GetEcmId(VEH_GetEcmType(veh_type,ecm_index));
    commlevel = ECM_GetCommLevel(VEH_GetEcmType(veh_type,ecm_index));
    
    retry = 30;
    while(retry-- > 0)
    {
        if (commlevel == CommLevel_ISO14229)
        {
            //After key on, send Diagnostic Session Control command
            //to change to Programming Session, with no response to 
            //functional address 0x7DF repeatedly every 5ms for about 2s
            //You�ll see the PCM actually responds to this message, 
            //even though it�s not suppose to.
            //It�ll first respond with a 0x78 and then a successful response.
            
            for(i=0;i<75;i++)
            {
                //TODOQ: this should belong to broadcast
                status = obd2can_ford_initiate_diagnostic_operation
                    (FALSE,0x7DF,programmingSessionNoResponse_14229,FALSE);
                if (status == S_SUCCESS)
                {
                    break;
                }
                delays(20,'m');
            }
            for(i=0;i<75;i++)
            {
                //TODOQ: this should belong to broadcast
                status = obd2can_ford_initiate_diagnostic_operation
                    (FALSE,0x7DF,programmingSessionNoResponse_14229,FALSE);
                delays(20,'m');
            }
            
            //Then send the Diagnostic Session Control with response to
            //ECM physical address (i.e. 0x7E0, etc)
            status = obd2can_ford_initiate_diagnostic_operation
                (FALSE,ecm_id,
                 programmingSession_14229,TRUE);
            //once this done successfully, unlock is possible
            delays(300,'m');
        }
        else if (commlevel == CommLevel_KWP2000)
        {
            delays(100,'m');
            status = obd2can_ford_initiate_diagnostic_operation
                (FALSE,ecm_id,wakeUpFEPS,TRUE);
            //delays(300,'m');
            delays(400,'m');
        }
        else
        {
            status = S_COMMLEVEL;
        }
        if (status == S_SUCCESS)
        {
            break;
        }
    }
    
    return status;
}

//------------------------------------------------------------------------------
// Utility file is a small worker loaded (downloaded) to ECM to help upload
// and download tune (i.e. read/write ECM flash memory). Not all ECMs requires.
// Inputs:  u32 ecm_type (defined for ecm_defs)
//          u8 utilitychoiceindex (which util to use)
//          bool isdownloadutility (TRUE: download utlity)
// Return:  u8  status
// Engineer: Quyen Leba
// Note: so far (Mar042010), only 6.4L use this function
// it uses ($34) ($36 $36 ...) ($37)
// Note: data is download from top to bottom (forward)
//------------------------------------------------------------------------------
u8 obd2can_ford_downloadutility(u32 ecm_type, u8 utilitychoiceindex,
                                bool isdownloadutility)
{
#define FORD_UTILITY_BLOCKSIZE          0x400
    Obd2can_SubServiceNumber subservice;
    F_FILE *fptr;
    u8 databuffer[FORD_UTILITY_BLOCKSIZE];
    u16 maxdownloadblocklength;
    u32 utilfilesize;
    u32 blocklength;
    u8 status;
    u32 ecm_id;
    u32 targetaddr;                     //ecm physical address
    VehicleCommLevel commlevel;
    u8  dataformat;
    u8  blocktracking;
    u32 bytecount;
    u8  order;
    u8  order_index;
    u8  order_current;
    u8  loop;

    u8 *utilityfilename;
    bool useencryption;    
    bool skipblock;
    u32 decryptblocklength;

    if (isdownloadutility)
    {
        utilityfilename = (u8*)ECM_GetDUtiFilename(ecm_type,utilitychoiceindex);
        useencryption = (bool)ECM_IsDUtiUseEncryption(ecm_type,utilitychoiceindex);
    }
    else
    {
        utilityfilename = (u8*)ECM_GetUUtiFilename(ecm_type,utilitychoiceindex);
        useencryption = (bool)ECM_IsUUtiUseEncryption(ecm_type,utilitychoiceindex);
    }
    if (utilityfilename == NULL)
    {
        return S_UTILITYNOTREQUIRED;
    }
    else if (utilityfilename[0] == NULL)
    {
        return S_UTILITYNOTREQUIRED;
    }
    if(useencryption)
    {
        status = ubf_validate_file(utilityfilename);
        if (status != S_SUCCESS)
        {
            return status;
        }
    }
    
    fptr = NULL;
    if((fptr = genfs_default_openfile(utilityfilename, "rb")) == NULL)
    {
        return S_OPENFILE;
    }
    if ((fseek(fptr,0,SEEK_END)) != 0)
    {
        status = S_SEEKFILE;
        goto obd2can_ford_downloadutility_done;
    }
    utilfilesize = ftell(fptr);
    if (fseek(fptr, 0, SEEK_SET) != 0)
    {
        status = S_SEEKFILE;
        goto obd2can_ford_downloadutility_done;
    }
    if (utilfilesize == 0)
    {
        status = S_BADCONTENT;
        goto obd2can_ford_downloadutility_done;
    }
    
    if(useencryption)
    {
        if(ubf_seek_start(fptr) != S_SUCCESS)
            return S_SEEKFILE;
    }
 
    //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    ecm_id = ECM_GetEcmId(ecm_type);
    commlevel = ECM_GetCommLevel(ecm_type);
//    require_separate_execution = ECM_IsUUtiSeparateExe(ecm_type);
    
    order_current = ORDER_0;
    for(loop=0;loop<MAX_ECM_MEMORY_BLOCKS;loop++)
    {
        for(order_index=0;
            order_index<MAX_ECM_MEMORY_BLOCKS;order_index++)
        {
            if (isdownloadutility)
                order = (u8)ECM_GetDUtiBlockFlagOrder(ecm_type,utilitychoiceindex,order_index);
            else
                order = (u8)ECM_GetUUtiBlockFlagOrder(ecm_type,utilitychoiceindex,order_index);
            
            if (order_current == order)
            {
                if (isdownloadutility)
                {
                    targetaddr = (u32)ECM_GetDUtiBlockAddr(ecm_type,utilitychoiceindex,order_index);
                    utilfilesize = (u32)ECM_GetDUtiBlockSize(ecm_type,utilitychoiceindex,order_index);
                    skipblock = (bool)ECM_IsDUtiBlockSkipDownload(ecm_type,utilitychoiceindex,order_index);
                }
                else
                {
                    targetaddr = (u32)ECM_GetUUtiBlockAddr(ecm_type,utilitychoiceindex,order_index);
                    utilfilesize = (u32)ECM_GetUUtiBlockSize(ecm_type,utilitychoiceindex,order_index);
                    skipblock = (bool)ECM_IsUUtiBlockSkipDownload(ecm_type,utilitychoiceindex,order_index);
                }
                
                if (skipblock)
                {
                    status = S_SUCCESS;
                    continue;
                }
                if(utilfilesize == 0)
                {
                    //memblock with blocksize zero reached
                    status = S_SUCCESS;
                    goto obd2can_ford_downloadutility_done;
                }
                
                if (commlevel == CommLevel_ISO14229)
                {
                    dataformat = 0x00;
                }
                else
                {
                    dataformat = 0x01;
                }
                status = obd2can_ford_request_download(ecm_id, dataformat,
                                                       targetaddr, utilfilesize,
                                                       Request_4_Byte,
                                                       commlevel,
                                                       &maxdownloadblocklength);
                if (maxdownloadblocklength == 0)
                {
                    status = S_FAIL;
                    goto obd2can_ford_downloadutility_done;
                }
                else if (maxdownloadblocklength > FORD_UTILITY_BLOCKSIZE)
                {
                    maxdownloadblocklength = FORD_UTILITY_BLOCKSIZE;
                }
                
                if (status == S_SUCCESS)
                {
                    //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
                    //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
                    //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
                    bytecount = 0;
                    blocktracking = 0;
                    while(bytecount < utilfilesize)
                    {
                        blocktracking++;
                        blocklength = utilfilesize - bytecount;
                        if (blocklength > maxdownloadblocklength)
                        {
                            blocklength = maxdownloadblocklength;
                        }
                        
                        //TODOQ: right now, it's always subservice = download (0x00)
                        subservice = download;
                        
                        if (useencryption)
                        {
                            decryptblocklength = blocklength;
                            
                            if(ubf_read_block(databuffer, decryptblocklength, fptr) != S_SUCCESS)
                            {
                                status = S_READFILE;
                                goto obd2can_ford_downloadutility_done;
                            }
                        }
                        else
                        {
                            if ((fread(databuffer,1,blocklength, fptr)) != blocklength)
                            {
                                status = S_READFILE;
                                goto obd2can_ford_downloadutility_done;
                            }
                        }
                        
                        //Note: startingaddr (0) is not used here
                        status = obd2can_ford_transfer_data_download
                            (ecm_id, subservice, blocktracking, 0, Address_32_Bits,
                             databuffer, blocklength, commlevel, TRUE);
                        
                        if (status != S_SUCCESS)
                        {
                            break;
                        }
                        
                        bytecount += blocklength;
                        
                        //TODOQ: more on this testerpresent
                        obd2can_ford_testerpresent(TRUE, ecm_id,commlevel);
                        
                    }//while(utilblockcount > 0)...
                }//if (status == S_SUCCESS)...
                
                if (status == S_SUCCESS)
                {
                    status = obd2can_ford_request_transfer_exit(ecm_id,commlevel);
                }
                else                
                {
                    //log_push_error_point(0x4219);
                    goto obd2can_ford_downloadutility_done;
                }
                break;
            }
        }//for(memblock_index...
        if(status != S_SUCCESS)
        {
            break;
        }
        order_current++;
    }//for(loop=...
    
obd2can_ford_downloadutility_done:    
    if (commlevel == CommLevel_ISO14229 && status == S_SUCCESS)
    {
        targetaddr = (u32)ECM_GetDUtiCallAddress(ecm_type,utilitychoiceindex);
        databuffer[0] = 0x03;
        databuffer[1] = 0x01;
        databuffer[2] = targetaddr >> 24;
        databuffer[3] = targetaddr >> 16;
        databuffer[4] = targetaddr >> 8;
        databuffer[5] = targetaddr & 0xFF;
        //TOODQ: this is ugly code
        //TODOQ: put it in a exec utility function
        status = obd2can_ford_start_diagnostic_routine_bytestnumber
            (ecm_id,0x01,databuffer,6,commlevel);
        
        //give utility some time to start
        obd2can_ford_testerpresent(TRUE, ecm_id,commlevel);
        delays(500,'m');
        obd2can_ford_testerpresent(TRUE, ecm_id,commlevel);
        delays(1000,'m');
        obd2can_ford_testerpresent(TRUE, ecm_id,commlevel);
        delays(1000,'m');
        obd2can_ford_testerpresent(TRUE, ecm_id,commlevel);
    }
    
    //TODOQ: require_separate_execution is not used here
    
//    if (ECM_IsUtilityCheck(ecm_type))
//    {
//        if (ECM_GetUtiCheckId(ecm_type) != UTILITY_CHECKID_INVALID)
//        {
//            //TODOQ: do check utility
//        }
//    }
    
    if (fptr)
    {
        genfs_closefile(fptr);
    }
    return status;
}

//------------------------------------------------------------------------------
// Some ECM requires utility file to upload/download tune, and requires this
// function to take ECM out of utility-file mode
// Input:   u32 ecm_type (defined in ecm_defs)
// Return:  u8  status
// Engineer: Quyen Leba
// Note: (Mar042010), only 6.4L use utility file; however, no exit
// Note: (May122010), 2011 F250 6.2L also use util, need reset module to exit
//------------------------------------------------------------------------------
u8 obd2can_ford_exitutility(u32 ecm_type)
{
// PD. 10/10/2012
// Reset/Unlocks can be affected by Vehicle Def or ECM def flags so we should not
// do them outside of the upload/download functions

/*
    VehicleCommType commtype;
    VehicleCommLevel commlevel;
    
    commtype = ECM_GetCommType(ecm_type);
    commlevel = ECM_GetCommLevel(ecm_type);
    
    if (commtype == CommType_CAN)
    {
        if (commlevel == CommLevel_ISO14229)
        {
            obd2can_ford_ecu_reset_request(FALSE,0x7DF,hardReset_14229,TRUE);
        }
        else
        {
            //do nothing (not used yet)
        }
    }
    else
    {
        //do nothing (not used yet)
    }
*/
    
    return S_SUCCESS;
}

//------------------------------------------------------------------------------
// Some ECM requires a finalization after a download
// Input:   u32 ecm_type (defined in ecm_defs)
// Return:  u8  status
// Engineer: Quyen Leba
//------------------------------------------------------------------------------
u8 obd2can_ford_downloadfinalizing(u32 ecm_type, flasher_info *flasherinfo)
{
    u16 FinalizeId;
    u32 ecm_id;
    VehicleCommLevel commlevel;
    u8 testdata[2];
    u8 testdatalength;
    u32 temp_ecm_type; 
    u8 status; 
    
    status = S_FAIL; 
    FinalizeId = ECM_GetFinalizeId(ecm_type);
    
    switch(FinalizeId)
    {
    case FinalId_None:
        status =  S_SUCCESS;
        break;
    case FinalId_SendControlRoutine31:
        // Some of the newer Tricore PCMs require a routine to be run after a download
        // It seems like it tells the PCM to run a final checksum check. If this doesn't
        // get sent the PCM wont start the OS and will appear to be dead even after a 
        // successful download.
        ecm_id = ECM_GetEcmId(ecm_type);
        commlevel = ECM_GetCommLevel(ecm_type);
        testdata[0] = 0x03;
        testdata[1] = 0x04;
        testdatalength = 2;
        status = obd2can_ford_start_diagnostic_routine_bytestnumber(ecm_id, 0x01, testdata, testdatalength, commlevel);
        break;
    default:
        break;        
    }
    
    if (ECM_IsSignaturePatchRequired(ecm_type) && status == S_SUCCESS)
    {
        if (flasherinfo->flashtype == CMDIF_ACT_PRELOADED_FLASHER || 
        flasherinfo->flashtype == CMDIF_ACT_CUSTOM_FLASHER)
        {
            cmdif_internalresponse_ack(CMDIF_CMD_DO_DOWNLOAD,
                                       CMDIF_ACK_PROCESSING_DATA,NULL,0);
            
            if (flasherinfo->isstockcrc == FALSE)
            {
                temp_ecm_type = ecm_type;
                switch(temp_ecm_type)
                {
                case 38:
                    temp_ecm_type = 45;
                    break;
                case 51:
                case 52:
                    temp_ecm_type = 53;
                    break;
                default:
                    status = S_FAIL; 
                    break;
                }
                
                if (status == S_SUCCESS)
                {
                    status = obd2can_ford_append_patch(temp_ecm_type,flasherinfo); 
                }
            }
        }
    }
    
    return status;
}

//------------------------------------------------------------------------------
// Reset an ECM by ecm_type
// Inputs:  u8  ecm_type
// Return:  u8  status
// Engineer: Quyen Leba
//------------------------------------------------------------------------------
u8 obd2can_ford_reset_ecm(u8 ecm_type)
{
    u32 ecm_id;
    VehicleCommType commtype;
    VehicleCommLevel commlevel;

    ecm_id = ECM_GetEcmId(ecm_type);
    commtype = ECM_GetCommType(ecm_type);
    commlevel = ECM_GetCommLevel(ecm_type);
    
    if (commtype != CommType_CAN)
    {
        return S_COMMTYPE;
    }
    
    if (commlevel == CommLevel_ISO14229)
    {
        //hardReset_14229: no response
        obd2can_ford_ecu_reset_request(FALSE,0x7DF,hardReset_14229,FALSE);
    }
    else if (commlevel == CommLevel_KWP2000)
    {
        obd2can_ford_ecu_reset_request(FALSE,ecm_id,hardReset,TRUE);
    }
    else
    {
        return S_COMMLEVEL;
    }
    delays(750, 'm');   // Ford CAN spec says PCM is allowed 750ms re-init time after reset...
    return S_SUCCESS;
}

//------------------------------------------------------------------------------
// Inputs:  u8  ecm_type
//          bool is_cal_only
// Return:  u8  status
// Engineer: Quyen Leba
//TODOQ: T43 handled differently, check it again
//------------------------------------------------------------------------------
u8 obd2can_ford_erase_ecm(u8 ecm_type, bool is_cal_only)
{
    Obd2can_ServiceData servicedata;
    VehicleCommLevel commlevel;
    u32 ecm_id;
    u8  i;
    u8  status;
    
    ecm_id = ECM_GetEcmId(ecm_type);
    commlevel = ECM_GetCommLevel(ecm_type);
    servicedata.commlevel = commlevel;
    if (commlevel == CommLevel_ISO14229)
    {
        u32 blockaddress;
        u32 blocksize;
        u8  erasedata[10];
        
        erasedata[0] = 0xFF;
        erasedata[1] = 0x00;
        for(i=0;i<MAX_ECM_MEMORY_BLOCKS;i++)
        {
            if (is_cal_only)
            {
                blockaddress = ECM_GetCalBlockAddr(ecm_type,i);
                blocksize = ECM_GetCalBlockSize(ecm_type,i);
            }
            else
            {
                blockaddress = ECM_GetOSBlockAddr(ecm_type,i);
                blocksize = ECM_GetOSBlockSize(ecm_type,i);
            }
            if (blocksize == 0)
            {
                //termination block
                break;
            }
            
            erasedata[2] = (u8)(blockaddress >> 24);
            erasedata[3] = (u8)(blockaddress >> 16);
            erasedata[4] = (u8)(blockaddress >> 8);
            erasedata[5] = (u8)(blockaddress & 0xFF);
            
            erasedata[6] = (u8)(blocksize >> 24);
            erasedata[7] = (u8)(blocksize >> 16);
            erasedata[8] = (u8)(blocksize >> 8);
            erasedata[9] = (u8)(blocksize & 0xFF);
            
            obd2can_ford_testerpresent(TRUE, ecm_id,commlevel);
            status = obd2can_ford_start_diagnostic_routine_bytestnumber
                (ECM_GetEcmId(ecm_type),0x01,erasedata,10,commlevel);
            obd2can_ford_testerpresent(TRUE, ecm_id,commlevel);
            if (status != S_SUCCESS)
            {
                break;
            }
        }
    }
    else if (commlevel == CommLevel_KWP2000)
    {
        u8  erasecmd_count;
        //u8  erasecmd_flags;
        const u8  *erasecmd_data;
        
        obd2can_servicedata_init(&servicedata);
        servicedata.ecm_id = ECM_GetEcmId(ecm_type);
        servicedata.service = 0xB1;
        servicedata.service_controldatabuffer[0] = 0x00;
        servicedata.service_controldatabuffer[1] = 0xB2;

        if (is_cal_only)
        {
            //erasecmd_flags = ECM_GetEraseCalCmd(ecm_type).flags;
            erasecmd_count = ECM_GetEraseCalCmd(ecm_type).count;
            erasecmd_data = ECM_GetEraseCalCmd(ecm_type).data;
        }
        else
        {
            //erasecmd_flags = ECM_GetEraseAllCmd(ecm_type).flags;
            erasecmd_count = ECM_GetEraseAllCmd(ecm_type).count;
            erasecmd_data = ECM_GetEraseAllCmd(ecm_type).data;
        }
        if (erasecmd_count == 0 ||
            erasecmd_count > sizeof(ECM_GetEraseAllCmd(ecm_type).data))
        {
            return S_BADCONTENT;
        }

        servicedata.service_controldatalength = 3;
        servicedata.initial_wait_extended = 30;
        //servicedata.busy_wait_extended = 3;
        servicedata.busy_wait_extended = 10;
        servicedata.tester_present_type = TESTER_PRESENT_NODE;
        
        for(i=0;i<erasecmd_count;i++)
        {
            servicedata.service_controldatabuffer[2] = erasecmd_data[i];
            status = obd2can_txrx(&servicedata);
            //TODOQ: check this for FORD_1024_SILVER_OAK & FORD_1024_SILVER_OAK_AU:
            //if [07 E9 03 7F B1 31], reset, unlock then retry
        }
    }
    else
    {
        status = S_COMMLEVEL;
    }

    return status;
}

//------------------------------------------------------------------------------
// Validate OSC PID
// Inputs:  u32 ecm_id
//          DlxBlock *dlxblock
//          VehicleCommLevel commlevel
// Return:  u8  status
// Engineer: Tristen Pierson
//------------------------------------------------------------------------------
u8 obd2can_ford_validate_oscpid(u32 ecm_id, DlxBlock *dlxblock,
                                VehicleCommLevel commlevel)
{
    u8 status;
    
    switch (commlevel)
    {
        case CommLevel_ISO14229:
            // Return Control to ECU (to validate CPID without making changes)
            status = obd2can_ford_input_output_control(ecm_id, (u16)dlxblock->pidAddr,
                                            0x00, NULL, 0, commlevel, TRUE);
            // If ECU needs security access for OSC
            if (status == S_UNLOCKFAIL && gDatalogmode.unlockRequired == FALSE)
            {
                // Attempt unlock; set unlockRequired to true so no more unlocks
                // are attempted during this datalogging session
                gDatalogmode.unlockRequired = TRUE;
                status = obd2can_ford_unlock_osc(ecm_id,commlevel);
                if (status == S_SUCCESS) 
                {
                    // Re-validate CPID
                    status = obd2can_ford_input_output_control(ecm_id, (u16)dlxblock->pidAddr,
                                            0x00, NULL, 0, commlevel, TRUE);
                }
                else
                {
                    status = S_FAIL;
                }
            }
            break;
        case CommLevel_KWP2000:
            status = S_NOTSUPPORT;
            break;
        default:
            status = S_COMMLEVEL;
            break;
    }
    
    return status;
}
//------------------------------------------------------------------------------
// Validate DMR
// Inputs:  u32 ecm_id
//          DlxBlock *dlxblock
//          VehicleCommLevel commlevel
// Return:  u8  status
// Engineer: Quyen Leba
//------------------------------------------------------------------------------
u8 obd2can_ford_validatedmr(u32 ecm_id, DlxBlock *dlxblock,
                            VehicleCommLevel commlevel)
{
    u8  status;
    u8  buffer[4];        // Required for read; Maximum DMR size is 4 bytes
    u16 bufferlength;     // Required for read
    
    // Validate by rapid-packet setup
    status = obd2can_ford_validateByRapidPacketSetup(ecm_id, dlxblock, commlevel);
    
    if (status != S_SUCCESS && status != S_OUTOFRANGE)
    {
        // Validate by mode 0x23 if rapid packet validation fails
        // (rapid-packets are unsupported)
        status = obd2can_ford_read_memory_address(ecm_id, dlxblock->pidAddr, 
                                              dlxblock->pidSize,
                                              buffer, &bufferlength,
                                              FALSE, commlevel);
    }            
    
    return status;
}

//------------------------------------------------------------------------------
// Validate By Rapid Packet Setup
// Inputs:  u32 ecm_id
//          DlxBlock *dlxblock
//          VehicleCommLevel commlevel
// Return:  u8  status
// Engineer: Quyen Leba/Patrick Downs
//------------------------------------------------------------------------------
u8 obd2can_ford_validateByRapidPacketSetup(u32 ecm_id, DlxBlock *dlxblock,
                            VehicleCommLevel commlevel)
{
    u8  status;
    
    if (commlevel == CommLevel_ISO14229)
    {
        obd2can_ford_datalog_change_diagnostic_mode(&commlevel);
        obd2can_ford_cleandynamicallydefinemessage_14229(ecm_id);
        status = obd2can_ford_dynamicallydefinemessage
            (ecm_id, 0xF200, (PidType)dlxblock->pidType, dlxblock->pidSize, 1,
             dlxblock->pidAddr,CommLevel_ISO14229);
    }
    else if (commlevel == CommLevel_KWP2000)
    {
        obd2can_ford_requestdiagnosticdatapacket(ecm_id,0x0A,NULL,0,
                                                 CommLevel_KWP2000);
        status = obd2can_ford_dynamicallydefinediagnosticdatapacket
            (ecm_id, 0x01, (PidType)dlxblock->pidType, dlxblock->pidSize, 1,
             dlxblock->pidAddr, CommLevel_KWP2000);
    }
    else
    {
        return S_COMMLEVEL;
    }

    return status;
}

//------------------------------------------------------------------------------
// Request TPMS info of vehicles with TPMS support
// Outputs: u8  *front_tire_pressure (in PSI, valid range [30-115])
//          u8  *rear_tire_pressure (in PSI, valid range [30-115])
// Return:  u8  status
// Engineer: Quyen Leba/Thomas Carter
// Note: require ABS module; i.e. won't be S_SUCCESS on a bench
//------------------------------------------------------------------------------
u8 obd2can_ford_request_TPMS_info(u8 *front_tire_pressure, u8 *rear_tire_pressure)
{
#define TIRE_PRESSURE_67L_LOW           30
#define TIRE_PRESSURE_67L_HIGH          115
    u8  data[64];
    u8  datalength;
    u8  status;

    *front_tire_pressure = 0;
    *rear_tire_pressure = 0;

    obd2can_init(Obd2canInitCustom,0x720);
    
    status = obd2can_ping(0x726, TRUE);
    if (status == S_SUCCESS)
    {
        status = obd2can_read_data_bypid(0x726,0xDE15,data,&datalength,TRUE);
        if (status == S_SUCCESS && datalength >= 4)
        {
            if(data[0] == 0x00)
            {
                // Disabled, show as 0 PSI
                *front_tire_pressure = 0;
                *rear_tire_pressure = 0;
            }
            else
            {
                // check values
                if(data[2] < TIRE_PRESSURE_67L_LOW || data[2] > TIRE_PRESSURE_67L_HIGH ||
                   data[3] < TIRE_PRESSURE_67L_LOW || data[3] > TIRE_PRESSURE_67L_HIGH)
                {
                    status = S_FAIL; // Bad values
                }
                else
                {
                    *front_tire_pressure = data[2];
                    *rear_tire_pressure = data[3];
                }
            }
        }
        else
        {
            status = S_FAIL;
        }
    }
    else
    {
        status = S_FAIL;
    }
    obd2can_init(Obd2canInitProgram,0);

    return status;
}

//------------------------------------------------------------------------------
// Check for TPMS support, if supported append data for Vehicle info
// Outputs: u8  *front_tire_pressure (in PSI, valid range [30-115])
//          u8  *rear_tire_pressure (in PSI, valid range [30-115])
// Return:  u8  status
// Engineer: Quyen Leba/Thomas Carter
// Note: require ABS module; i.e. won't be S_SUCCESS on a bench
//------------------------------------------------------------------------------
u8 obd2can_ford_check_TPMS_support(u8 *info_text)
{
    u8  buffer[32];
    u8 front_tire_pressure;
    u8 rear_tire_pressure;
    u8  status;

    status = obd2can_ford_request_TPMS_info(&front_tire_pressure,&rear_tire_pressure);
    if (status == S_SUCCESS)
    {
        strcat((char*)info_text,"<HEADER>TPMS:</HEADER>");
        itoa(front_tire_pressure,buffer,10);
        strcat((char*)info_text,(char*)buffer);
        strcat((char*)info_text,"(Front), ");
        itoa(rear_tire_pressure,buffer,10);
        strcat((char*)info_text,(char*)buffer);
        strcat((char*)info_text,"(Rear)");
    }
    else
    {
      status = S_FAIL;
    }

    return status;
}

//------------------------------------------------------------------------------
// Validate Unlock OSC Security
// Inputs:  u32 ecm_id
//          VehicleCommLevel commlevel
// Return:  u8  status
// Engineer: Tristen Pierson
//------------------------------------------------------------------------------
u8 obd2can_ford_unlock_osc(u32 ecm_id,VehicleCommLevel commlevel)
{
    u8 status;
    u8 seed[4];
    u8 key[4];    
    u8 datalength[1];

    memset(seed,0,sizeof(seed));
    memset(key,0,sizeof(key));
    *datalength = 8;
    status = obd2can_ford_security_access(ecm_id, DevCtrlrequestSeed, seed, 
                                          datalength,0);
    
    if (status == S_SUCCESS && *(u32*)seed != 0x000000)
    {                            
        status = obd2_ford_generatekey(seed, UnlockId_OSC, CommType_CAN, key);
    
        if (status == S_SUCCESS)
        {
            *datalength = 3;
            status = obd2can_ford_security_access(ecm_id, 
                        DevCtrlsendKey, key, datalength,0);
        }
    }
    return status;
}
   
/**
 *  @brief Get Ford CAN OBD2 Info
 *  
 *  @param [in] obd2_info_block_t OBD2 info block
 *
 *  @return status
 */
u8 obd2can_ford_getinfo(obd2_info_block *block)
{
    u8  buffer[100];
    u16 bufferlength;
    u8  strategy[32];
    u8  charcount;
    u8  status;
    u8  *bptr;
    bool iscodeempty;   //i.e. no bytes
    
    iscodeempty = FALSE;
    block->partnumber_count = 0;
    
    if (block->commtype == CommType_CAN && block->commlevel != CommLevel_Unknown)
    {
        status = obd2can_ford_request_vehicle_information(block->ecu_id, 0x04,
                                                          buffer, &bufferlength);
        
        if (status == S_SUCCESS)
        {
            if (bufferlength == 0)
            {
                iscodeempty = TRUE;
            }
            
            bptr = (u8*)strstr((char*)buffer,".H");  //expecting .HEX
            if (bptr == NULL)
            {
                charcount = 0;
                // Most Ford ECMs should return a Strategy Code(Alphanumeric
                // string ending in ".HEX" or ".H32") from CalID command. If
                // it returns a Partnumber (Alphanumeric string with Dashes '-' 
                // and no ".H") we have specific ID string we search the PCM for 
                // that will tell us calibration information about it. 
                // Other cases are when it's aftermarket calibration like Roush 
                // or Saleen. If it contains all printable ASCII characters, 
                // we'll accept it.
                if (strlen((char*)buffer) <= bufferlength)
                {
                    while(isprint(buffer[charcount]))
                    {
                        charcount++;
                    }
                    if (charcount == strlen((char*)buffer))
                    {
                        strcpy((char*)block->partnumbers.ford.sw_part_number,
                               (char*)buffer);
                        
                        iscodeempty = TRUE; 
                        status = obd2can_ford_read_strategy_bysearch
                            (block->ecu_id, strategy, block->commlevel);
                        
                        if (status == S_SUCCESS)
                        {
                            block->partnumber_count = 1;
                            strcpy((char*)block->partnumbers.ford.strategy,
                                   (char*)strategy);
                            iscodeempty = FALSE;
                        }
                        else if (block->commlevel == CommLevel_KWP2000)
                        {
                            //----------------------------------------------
                            // Attempt to read strategy for 2048K ME9
                            // (UK Focus ST 225)
                            // PATS data address: 0x00080314
                            //----------------------------------------------
                            
                            status = obd2can_ford_initiate_diagnostic_operation(FALSE, 
                                        block->ecu_id, wakeUpFEPS, TRUE);
                
                            // Attempt to unlock ME9UK - ecm_type 49
                            // If unlock is successful, ecm should be
                            // a ME9 2005-2008 UK ST 225
                            if (status == S_SUCCESS)
                            {
                                status = obd2can_unlockecm(UnlockTask_Upload,49,FALSE);
                            }
                            // Attempt to read strategy by 0x23
                            if (status == S_SUCCESS)
                            {   
                                status = obd2can_ford_read_memory_address(
                                        block->ecu_id, 0x00080314, 0x06, 
                                        buffer, &bufferlength, FALSE, CommLevel_KWP2000);
                            }
                            // Get variant id
                            if (status == S_SUCCESS)
                            {
                                // Use strategy from 0x23
                                memcpy((char*)strategy,(char*)buffer,6);
                                strategy[6] = NULL;
                                block->partnumber_count = 1;
                                strcpy((char*)block->partnumbers.ford.strategy,(char*)strategy);

                                //----------------------------------------------
                                // Attempt to read vin again...this time with
                                // processor unlocked
                                //----------------------------------------------
                                if (obd2_validatevin(gObd2info.vin) != S_SUCCESS)
                                {
                                    if (obd2can_readvin_indirect_blankvin_overlay(gObd2info.vin) != S_SUCCESS)
                                    {
                                        gObd2info.vin[0] = NULL;
                                    }
                                    else
                                    {
                                        gObd2info.vin[VIN_LENGTH] = NULL;
                                    }
                                }
                                obd2can_ford_initiate_diagnostic_operation(FALSE, 
                                    block->ecu_id, defaultSession_kwp2000, TRUE);
                            }
                            else
                            {
                                status = S_BADCONTENT;
                            }
                            //----------------------------------------------
                            // Accept part number as-is
                            //----------------------------------------------   
                            if (status == S_SUCCESS)
                            {
                                iscodeempty = FALSE;
                            }
                            else
                            {
                                block->partnumber_count = 1;
                                strcpy((char*)block->partnumbers.ford.strategy,
                                       (char*)buffer);
                            }                           
                        }
                    }
                }//if (strlen...
            }//if (bptr == NULL)...
            else
            {
                *bptr++ = NULL;
                block->partnumber_count = 1;
                strcpy((char*)block->partnumbers.ford.strategy,(char*)buffer);
            }
        }
        else
        {
            // Some PCMs in China do not accept the OBDII read vehicle info commands
            status = obd2can_ford_read_strategy_bysearch(block->ecu_id, 
                                                         strategy, 
                                                         block->commlevel);
            if (status == S_SUCCESS)
            {                                       
                block->partnumber_count = 1;
                strcpy((char*)block->partnumbers.ford.strategy,
                       (char*)strategy);
            }
        }
        
        /* Get Software P/N */
        status = obd2can_ford_getsoftwarepartnumber(block->ecu_id,
                                                    block->partnumbers.ford.sw_part_number);
        if (status != S_SUCCESS)
        {
            block->partnumbers.ford.sw_part_number[0] = NULL;
        }
        else if (!isprint(block->partnumbers.ford.sw_part_number[0]))
        {
            block->partnumbers.ford.sw_part_number[0] = NULL;
        }
        else if (iscodeempty)
        {
            //might be crate motor such as 5.0L Mustang (empty strategy but has part#)
            strcpy((char*)block->partnumbers.ford.strategy,"EMPTYCODE");
            block->partnumber_count = 1;
        }
        
        /* Get Hardware P/N */
        if (block->commlevel == CommLevel_ISO14229)
        {
            status = obd2can_ford_gethardwarepartnumber(block->ecu_id,
                                                        block->partnumbers.ford.hw_part_number);
            if (status != S_SUCCESS)
            {
                block->partnumbers.ford.hw_part_number[0] = NULL;
            }
            else if (!isprint(block->partnumbers.ford.hw_part_number[0]))
            {
                block->partnumbers.ford.hw_part_number[0] = NULL;
            }
        }
        
        /* Get Module P/N */
        if (block->commlevel == CommLevel_ISO14229)
        {
            status = obd2can_ford_getmodulepartnumber(block->ecu_id,
                                                      block->partnumbers.ford.module_part_number);
            if (status != S_SUCCESS)
            {
                block->partnumbers.ford.module_part_number[0] = NULL;
            }
            else if (!isprint(block->partnumbers.ford.module_part_number[0]))
            {
                block->partnumbers.ford.module_part_number[0] = NULL;
            }
        }
    }
    return S_SUCCESS;
}

/**
 *  @brief Append patch. Download modified bootloader. 
 *  
 *  @param [in] ecm_type, flasherinfo
 *
 *  @return status
 */
u8 obd2can_ford_append_patch(u32 ecm_type, flasher_info *flasherinfo)
{
    u8 status; 
    
    status = S_FAIL; 
   
    //reset processor
    status = obd2can_ford_reset_ecm(ecm_type);
    if (status == S_SUCCESS)
    {
        //wake up processor
        status = obd2can_ford_wakeup(flasherinfo->vehicle_type,0);
        if (status == S_SUCCESS)
        {
            //unlock processor
            status = obd2tune_unlockecm(UnlockTask_Download,
                                        ecm_type,ECM_GetCommType(ecm_type),
                                        0);
            //download modified bootloader
            if (status == S_SUCCESS)
            {
                status = obd2tune_downloadutility(ecm_type,TRUE, FALSE);
                if (status == S_SUCCESS)
                {
                    //download signature patch
                    status = obd2can_ford_download_signature_patch(ecm_type);
                }
            }
        }
    }
    
    return status; 
}

/**
 *  @brief Download signature patch
 *  
 *  @param [in] ecm_type
 *
 *  @return status
 */
u8 obd2can_ford_download_signature_patch(u32 ecm_type)
{
    u8 status; 
    u8 signaturedata[8] = {0x53,0x43,0x54,0x20,0x90,0x48,0x24,0x42};
    u32 memblock_address;
    u32 memblock_length;
    u8 requestdata[10]; 
    Obd2can_ServiceData servicedata;
    
    status = S_FAIL; 
    memblock_address = ECM_GetOSBlockAddr(ecm_type,0);
    memblock_length = ECM_GetOSBlockSize(ecm_type,0);
    
    requestdata[0] = 0x00;
    requestdata[1] = 0x44; 
    
    requestdata[2] = (u8)(memblock_address >> 24);
    requestdata[3] = (u8)(memblock_address >> 16);
    requestdata[4] = (u8)(memblock_address >> 8);
    requestdata[5] = (u8)(memblock_address & 0xFF);
    
    requestdata[6] = (u8)(memblock_length >> 24);
    requestdata[7] = (u8)(memblock_length >> 16);
    requestdata[8] = (u8)(memblock_length >> 8);
    requestdata[9] = (u8)(memblock_length & 0xFF);
    
    obd2can_servicedata_init(&servicedata);
    servicedata.ecm_id = ECM_GetEcmId(ecm_type);
    servicedata.service = 0x34; 
    servicedata.txdata = requestdata;
    servicedata.txdatalength = 10;    
    
    status = obd2can_txrxcomplex(&servicedata);
    if (status == S_SUCCESS)
    {
        obd2can_servicedata_init(&servicedata);
        servicedata.ecm_id = ECM_GetEcmId(ecm_type);
        servicedata.service = 0x36;
        servicedata.subservice = 0x01;
        servicedata.txdata = signaturedata;
        servicedata.txdatalength = memblock_length;
        
        status = obd2can_txrxcomplex(&servicedata);
        if (status == S_SUCCESS)
        {
            obd2can_servicedata_init(&servicedata);
            servicedata.ecm_id = ECM_GetEcmId(ecm_type);
            servicedata.service = 0x37;
            status = obd2can_txrx(&servicedata);
            if (status != S_SUCCESS)
            {
                //don't fail the download.
                status = S_SUCCESS; 
            }
        }
    }
    
    return status;     
} 
