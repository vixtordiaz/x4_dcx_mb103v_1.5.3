/**
  **************** (C) COPYRIGHT 2011 SCT Performance, LLC *********************
  * File Name          : obd2tune_ford.c
  * Device / System    : SCT products using the "CommonCode" platform
  * Author             : Quyen Leba
  *
  * Version            : 1 
  * Date               : 05/02/2011
  * Description        : 
  *                    : 
  *
  *
  * History            : 05/02/2011 M. Davis
  *                    :   Add SCT Copyright header,
  *
  ******************************************************************************
  */

//------------------------------------------------------------------------------
// Log error point: 0x44xx -> 0x46xx (shared with obd2tune.c)
//------------------------------------------------------------------------------

#include <string.h>
#include <fs/genfs.h>
#include <common/log.h>
#include <common/file.h>
#include <common/filestock.h>
#include <common/filestock_option.h>
#include <common/statuscode.h>
#include <common/obd2can.h>
#include <common/obd2scp.h>
#include "obd2tune_ford.h"


//------------------------------------------------------------------------------
// Check if the custom tune is allowed
// Input:   u16 veh_type
//          u8  *vin
//          u8  *preference_filename (contains check info)
//          selected_tunelist_info *tuneinfo
// Outputs: u8  *matchmasked (from LSB, 1=matched,0=unmatch)
//          bool *isvinmasked (TRUE: VIN is masked)
// Return:  u8  status
// Engineer: Quyen Leba
// Note: only useful for custom tune
//------------------------------------------------------------------------------
u8 obd2tune_ford_check_customtune_support(u16 veh_type,  u8 *vin,
                                          u8 *preference_filename,
                                          ecm_info *ecminfo, u8 *matchmasked,
                                          bool *isvinmasked)
{
    customtune_tuneosdata tuneosdata;
    bool isvinmatch;
    bool istunematch;
    bool tmpbool;
    u8  mask_cmp;
    u8  mask_reference;
    u8  i;
    u8  vinmaskcount;
    u8  status;
    u32 ecm_count;
    u8 tuneosfilepath[64];

    status = S_FAIL;
    if (preference_filename[0] == 0)
    {
        if (VEH_IsCustomTuneTuneOSNotRequired(veh_type))
        {
            status = S_NOTREQUIRED;
            goto obd2tune_ford_check_customtune_support_done;
        }
        else
        {
            status = S_ERROR;
            log_push_error_point(0x446B);
            goto obd2tune_ford_check_customtune_support_done;
        }
    }

    if (strstr((char*)preference_filename,".tos"))
    {
        genfs_userpathcorrection(preference_filename, sizeof(tuneosfilepath), 
                                 tuneosfilepath);
        status = obd2tune_extracttuneosdata_tos(tuneosfilepath,
                                                &tuneosdata);
    }
    else if (strstr((char*)preference_filename,".txt"))
    {
        status = obd2tune_extracttuneosdata_txt(preference_filename,
                                                &tuneosdata);
    }
    else
    {
        log_push_error_point(0x446C);
        return S_NOTSUPPORT;
    }
    if (status == S_NOTSUPPORT)
    {
        log_push_error_point(0x446E);
        return status;
    }
    else if (status != S_SUCCESS)
    {
        log_push_error_point(0x446D);
        return status;
    }

    //check VIN
    vinmaskcount = 0;
    *matchmasked = 0;
    *isvinmasked = FALSE;
    isvinmatch = TRUE;
    for(i=0;i<VIN_LENGTH;i++)   
    {
        if (tuneosdata.vin[i] == '*')
        {
            vinmaskcount++;
        }
        else if (vin[i] != tuneosdata.vin[i])
        {
            isvinmatch = FALSE;
            break;
        }
    }
    if(vinmaskcount == VIN_LENGTH)
    {
        // All VIN characters are masked, so don't care
    }
    else if(vinmaskcount > 0)
    {
        *isvinmasked = TRUE;
    }
    
    mask_reference = mask_cmp = 0;
    ecm_count = VEH_GetEcmCount(veh_type);
    for(i=0;i<ecm_count;i++)
    {
        mask_reference |= (u8)(1<<i);
    }
    
    for(i=0;i<ecm_count;i++)
    {
        //check ecminfo->codes[i][0] against tuneosdata.codes
        //then check ecminfo->second_codes[i][0] against tuneosdata.secondcodes
        tmpbool = FALSE;
        status = obd2tune_comparetunecode(tuneosdata.codes,
                                          ecminfo->codes[i][0],i);
        if (status == S_SUCCESS)
        {
            if (ecminfo->second_codes[i][0][0] == 0)
            {
                status = obd2tune_comparetunecode(tuneosdata.secondcodes,
                                                  "N/A",i);
            }
            else
            {
                status = obd2tune_comparetunecode(tuneosdata.secondcodes,
                                                  ecminfo->second_codes[i][0],i);
            }
            if (status == S_SUCCESS)
            {
                tmpbool = TRUE;
            }
        }

        if (tmpbool)
        {
            mask_cmp |= (u8)(1<<i);
        }
    }

    istunematch = FALSE;
    if (mask_cmp == mask_reference)
    {
        istunematch = TRUE;
    }
    status = S_SUCCESS;
    
obd2tune_ford_check_customtune_support_done:
    if (status == S_SUCCESS)
    {
        *matchmasked = mask_cmp;
        if (isvinmatch == FALSE)
        {
            return S_VINUNMATCH;
        }
        else if (istunematch == FALSE)
        {
            return S_TUNEUNMATCH;
        }
    }
    return status;
}

//------------------------------------------------------------------------------
// Compare Partnumbers in the settings with ones from the vehicle
// Inputs:  u8 *vehiclecodes_fromvehicle
//          u8 *secondvehiclecodes_fromvehicle
// Return:  u8  status
// Engineer: Patrick Downs
//------------------------------------------------------------------------------
u8 obd2tune_ford_compare_vehiclecodes_with_setting(flasher_info *flasherinfo)
{
    int i;
    u8 all_match_status;
    ecm_info ecminfo;
    u8 ecm_match_status[ECM_MAX_COUNT];
    
    ecminfo = flasherinfo->ecminfo;
    
    all_match_status = S_SUCCESS;
    for(i=0;i<ecminfo.ecm_count;i++)
    {
        if(ecminfo.second_codes[i][0] != 0) // Search for matching partnumbers
        {
            if(strstr((char const*)SETTINGS_TUNE(secondvehiclecodes), (char*)ecminfo.second_codes[i][0]) == 0)
            {
                ecm_match_status[i] = S_FAIL; // No match
                all_match_status = S_FAIL;
            }
            else
                ecm_match_status[i] = S_SUCCESS; 
        }
        else if(ecminfo.codes[i][0] != 0)   // Search for matching strategies
        {
            if(strstr((char const*)SETTINGS_TUNE(vehiclecodes), (char*)ecminfo.codes[i][0]) == 0)
            {
                ecm_match_status[i] = S_FAIL; // No match
                all_match_status = S_FAIL;
            }
            else
                ecm_match_status[i] = S_SUCCESS;
        }
        else
        {
            ecm_match_status[i] = S_SUCCESS; // There's probably an error so we'll skip marking it for full reflash
        }
    }
   
    // If part numbers do not match, mark for full reflash which will force 
    // preloaded stock file download before tune download. Currently only used
    // for 6.7L PCM modified bootloader flashing.
    
    i = 0;
//    for(i=0;i<ecminfo.ecm_count;i++)
//    {
    if(ecm_match_status[i] == S_FAIL)
    {
        flasherinfo->isfullreflash_already_set = TRUE;
        flasherinfo->isfullreflash[0] = TRUE;
    }
//    }
    
    return all_match_status;
}

//------------------------------------------------------------------------------
// Compare Partnumbers in the settings with ones from the vehicle
// Inputs:  u8 *vehiclecodes_fromvehicle
//          u8 *secondvehiclecodes_fromvehicle
// Return:  u8  status
// Engineer: Patrick Downs
//------------------------------------------------------------------------------
u8 obd2tune_ford_compare_vehiclecodes_with_tuneosdata(flasher_info *flasherinfo)
{
    int i;
    u8 all_match_status;
    ecm_info ecminfo;
    u8 ecm_match_status[ECM_MAX_COUNT];
    
    ecminfo = flasherinfo->ecminfo;
    
    all_match_status = S_SUCCESS;
    for(i=0;i<ecminfo.ecm_count;i++)
    {
        if(ecminfo.second_codes[i][0] != 0) // Search for matching partnumbers
        {
            if(strstr((char const*)flasherinfo->secondvehiclecodes, (char*)ecminfo.second_codes[i][0]) == 0)
            {
                ecm_match_status[i] = S_FAIL; // No match
                all_match_status = S_FAIL;
            }
            else
                ecm_match_status[i] = S_SUCCESS; 
        }
        else if(ecminfo.codes[i][0] != 0)   // Search for matching strategies
        {
            if(strstr((char const*)flasherinfo->vehiclecodes, (char*)ecminfo.codes[i][0]) == 0)
            {
                ecm_match_status[i] = S_FAIL; // No match
                all_match_status = S_FAIL;
            }
            else
                ecm_match_status[i] = S_SUCCESS;
        }
        else
        {
            ecm_match_status[i] = S_SUCCESS; // There's probably an error so we'll skip marking it for full reflash because we can't be sure
        }
    }
   
    // If part numbers do not match, mark for full reflash which will force 
    // preloaded stock file download before tune download. Currently only used
    // for 6.7L PCM modified bootloader flashing.
    
    i = 0;
//    for(i=0;i<ecminfo.ecm_count;i++)
//    {
    if(ecm_match_status[i] == S_FAIL)
    {
        flasherinfo->isfullreflash_already_set = TRUE;
        flasherinfo->isfullreflash[0] = TRUE;
    }
//    }
    
    return all_match_status;
}

//------------------------------------------------------------------------------
// Read the VID block data from vehicle
// Inputs:  u16 veh_type
//
// Outputs: u8* buffer
//          u32* length
//
// Return:  u8  status
// Engineer: Patrick Downs
//------------------------------------------------------------------------------
u8 obd2tune_ford_read_vid(u16 ecm_type, u8 *buffer, u32 *length)
{
    MemoryAddressType address_type;
    VehicleCommType commtype;
    VehicleCommLevel commlevel;
    u16 readlength;
    u16 vidlength;
    u32 vidaddress;
    u8 vidtemp[256];
    u8 status;
    
    vidaddress = ECM_GetVidAddress(ecm_type);
    commtype = ECM_GetCommType(ecm_type);
    commlevel = ECM_GetCommLevel(ecm_type);
    readlength = 0;
    
    if (ECM_IsUploadAddr4B(ecm_type))
        address_type = Address_32_Bits;
    else
        address_type = Address_24_Bits;
    
    if(commtype == CommType_CAN)
    {
        // Force 4 byte memory read when not PCM is not unlocked.
        vidlength = 256; // CAN VID Block length is 256 bytes
        status = obd2can_ford_read_memory_address(ECM_GetEcmId(ecm_type), vidaddress, 
                                             vidlength, vidtemp, 
                                             &readlength,TRUE, commlevel);
    }
    else if (commtype == CommType_SCP || commtype == CommType_SCP32)
    {
        /* VID address on read is offset by 0x10000 */
        if (ecm_type == 2 || ecm_type == 3)
        {
            vidaddress += 0x10000;
        }
        
        vidlength = 128; // SCP VID Block length is 128 bytes
        status = obd2scp_read_memory_address(vidaddress, vidlength, address_type, vidtemp, 
                                       &readlength,FALSE);
    }
    else
    {
        status = S_COMMTYPE;
    }
    
    if(status == S_SUCCESS)
    {
        memcpy(buffer, vidtemp, vidlength);
        *length = vidlength;
    }
    
    return status;
}

//------------------------------------------------------------------------------
// Read PATS data from vehicle
// Inputs:  u16 veh_type
//
// Outputs: u8* buffer
//          u32* length
//
// Return:  u8  status
// Engineer: Patrick Downs
//------------------------------------------------------------------------------
u8 obd2tune_ford_read_epats(u16 ecm_type, u8 *buffer, u32 *length)
{
    MemoryAddressType address_type;
    VehicleCommType commtype;
    VehicleCommLevel commlevel;
    u32 patsaddress;
    u16 readlength;
    u8 patstemp[14];
    u8 status;
    
    patsaddress = ECM_GetPatsAddress(ecm_type);
    commtype = ECM_GetCommType(ecm_type);
    commlevel = ECM_GetCommLevel(ecm_type);
    
    if (ECM_IsUploadAddr4B(ecm_type))
        address_type = Address_32_Bits;
    else
        address_type = Address_24_Bits;
    
    // Read EPATS, length is only 14 bytes    
    if(commtype == CommType_CAN)
    {
        // Force 4 byte memory read when not PCM is not unlocked.
        status = obd2can_ford_read_memory_address(ECM_GetEcmId(ecm_type), patsaddress, 
                                             sizeof(patstemp),
                                             patstemp,&readlength,TRUE,commlevel);
    }
    else if (commtype == CommType_SCP || commtype == CommType_SCP32)
    {
        /* PATS address on read is offset by 0x10000 */
        if (ecm_type == 2 || ecm_type == 3)
        {
            patsaddress += 0x10000;
        }
        
        status = obd2scp_read_memory_address(patsaddress, sizeof(patstemp), 
                                             address_type, patstemp, &readlength, 
                                             FALSE);
    }
    else
    {
        status = S_COMMTYPE;
    }
    
    if(status == S_SUCCESS)
    {
        memcpy(buffer, patstemp, sizeof(patstemp));
        *length = sizeof(patstemp);
    }
    return status;
}

//------------------------------------------------------------------------------
// Read the security byte data from vehicle
// Inputs:  u16 veh_type
//
// Outputs: u8* buffer
//          u32* length
//
// Return:  u8  status
// Engineer: Patrick Downs
//------------------------------------------------------------------------------
u8 obd2tune_ford_read_securitybyte(u16 ecm_type, u8 *buffer, u32 *length)
{
    MemoryAddressType address_type;
    VehicleCommType commtype;
    VehicleCommLevel commlevel;
    u32 securitybyteaddress;
    u32 vidaddress;
    u16 readlength;
    u8 status;
    u8 sbtemp[4];
    
    vidaddress = ECM_GetVidAddress(ecm_type);
    commtype = ECM_GetCommType(ecm_type);
    commlevel = ECM_GetCommLevel(ecm_type);
    
    if (ECM_IsUploadAddr4B(ecm_type))
    {
        address_type = Address_32_Bits;
    }
    else
    {
        address_type = Address_24_Bits;
    }
    
    // Security byte is 1 byte before the start of the strategy location
    if(commtype == CommType_CAN)
    {
        if (commlevel == CommLevel_ISO14229)
            securitybyteaddress = vidaddress - 126;
        else if (commlevel == CommLevel_KWP2000)
            securitybyteaddress = vidaddress - 123;
        else
            return S_COMMLEVEL;
        
        status = obd2can_ford_read_memory_address(ECM_GetEcmId(ecm_type), securitybyteaddress, 
                                             sizeof(sbtemp),
                                             sbtemp,&readlength,TRUE, commlevel);
    }
    else if (commtype == CommType_SCP || commtype == CommType_SCP32)
    {
        /* VID address on read is offset by 0x10000 */
        if (ecm_type == 2 || ecm_type == 3)
        {
            vidaddress += 0x10000;
        }
        
        securitybyteaddress = vidaddress - 123;
        status = obd2scp_read_memory_address(securitybyteaddress, sizeof(sbtemp), address_type,
                                             sbtemp, &readlength,FALSE);
    }
    else
    {
        status = S_COMMTYPE;
    }
    
    if(status == S_SUCCESS)
    {
        buffer[0] = sbtemp[0];
        *length = 1;
    }
        
    return S_SUCCESS;
}

//------------------------------------------------------------------------------
// Compare VID, PATS, and security byte data with married vehicle
// Inputs:  u16 veh_type
//
// Return:  u8  status
// Engineer: Patrick Downs
//------------------------------------------------------------------------------
u8 obd2tune_ford_compare_overlaydata(u16 veh_type)
{
    u32 readlength;            
    u16 ecm_type;
    u8 status;
    u8 vidtemp[256];
    u8 patstemp[14];
    u8 sbtemp[4];
    int i;
    
    if (VEH_IsRequireOverlay(veh_type) == FALSE)
    {
        return S_NOTREQUIRED;
    }
    
    ecm_type = VEH_GetMainEcmType(veh_type);
    if (ecm_type == INVALID_ECM_DEF)
    {
        return S_ECMTYPE;
    }
    
    status = obd2tune_ford_read_vid(ecm_type, vidtemp, &readlength);
    if(status != S_SUCCESS)
    {
        return status;
    }
    
    // 6.0L OTF tune writes a string at the end of the VID block
    // We need to scan VID block in flash.bin to see if we need to skip
    // overlaying those bytes.
    if(VEH_IsVehicleType_Ford_60L(veh_type))
    {
        for(i=240;i<256;i++)
        {
            if(vidtemp[i] != SETTINGS_TUNE(vid)[i])
            {
                // Unexpected data detected
                // Overlay VID data up to this location
                readlength = i; 
                break; 
            }
        }
    }
    
    if(memcmp((char*)SETTINGS_TUNE(vid),(char*)vidtemp, readlength) != 0)
    {
        return S_UNMATCH;
    }
    
    status = obd2tune_ford_read_epats(ecm_type, patstemp, &readlength);
    if(status != S_SUCCESS)
    {
        return status;
    }
    
    if(memcmp((char*)SETTINGS_TUNE(epats),(char*)patstemp,readlength) != 0)
    {
        return S_UNMATCH;
    }
    
    status = obd2tune_ford_read_securitybyte(ecm_type, sbtemp, &readlength);
    if(status != S_SUCCESS)
    {
        return status;
    }
    
    if(sbtemp[0] != 0xFE)
    {
        return S_UNMATCH;
    }
    
    return S_SUCCESS;
}

//------------------------------------------------------------------------------
// Upload vehicle stock options
// Input:   flasherinfo *f_info
// Return:  u8  status
// Engineer: Quyen Leba
//------------------------------------------------------------------------------
u8 obd2tune_ford_option_upload(flasher_info *f_info)
{
    return S_NOTSUPPORT;
}

//------------------------------------------------------------------------------
// Handle vehicle stock options after download
// Input:   flasher_info *f_info
// Return:  u8  status
// Engineer: Quyen Leba, Patrick Downs
// TODOQK: this function could get long; break it apart
//------------------------------------------------------------------------------
u8 obd2tune_ford_option_handler_postdownload(flasher_info *f_info)
{
    OptionFlags stock_options_flags;
    OptionFlags options_flags;
    u32 axle_ratio;
    u32 front_tire_size;
    u32 rear_tire_size;
    u16 front_tire_pressure;
    u16 rear_tire_pressure;
    
    u32 tmp_u32[4];
    u8 status;

    if (SETTINGS_IsDemoMode())
    {
        return S_SUCCESS;
    }
    
    status = filestock_option_download_init();
    if(status == S_SUCCESS)
    {
        memset((u8*)&stock_options_flags, 0, sizeof(stock_options_flags)); 
        if (f_info->uploadstock_required)
        {
            filestock_option_keep_options_flags(stock_options_flags); // If stockfile uploaded, overwrite old option flags with new
        }
        else
        {
            filestock_option_get_options_flags(&stock_options_flags); // Load current stock option flags
        }
        
        if (f_info->flashtype == CMDIF_ACT_STOCK_FLASHER) // Return Special Options to Stock Values
        {
            options_flags = stock_options_flags;
            filestock_option_get_tire_size(&front_tire_size,&rear_tire_size);
            filestock_option_get_axle_ratio(&axle_ratio);
            filestock_option_get_tire_pressure(&front_tire_pressure,&rear_tire_pressure);
        }
        else if (f_info->flashtype == CMDIF_ACT_PRELOADED_FLASHER ||
                 f_info->flashtype == CMDIF_ACT_CUSTOM_FLASHER)                 // Program Special Options if necessary
        {
            options_flags = f_info->options_flags;
            filestock_option_keep_new_options_flags(options_flags);
                       
            front_tire_size = f_info->actualtire.front;
            rear_tire_size = f_info->actualtire.rear;
            axle_ratio = f_info->actualaxleratio;
            front_tire_pressure = f_info->actualtpms.front;
            rear_tire_pressure = f_info->actualtpms.rear;
        }
        
        // --------------------------------------------------------------
        //  opcode_c_mathindex_10H - Ford 6.0L tire size - NOT SUPPORTED 
        // --------------------------------------------------------------
        // --------------------------------------------------------------
        //  opcode_c_mathindex_11H - Ford 6.4L tire size - NOT SUPPORTED 
        // --------------------------------------------------------------
        if(status == S_SUCCESS && options_flags.f.opcode_c_mathindex_12H) // opcode_c_mathindex_12H - Ford 6.0L truck tire size simple method
        {
            //Note: only rear tire size affects speedometer
            //Note: same option is used for both front and rear                
            if (!stock_options_flags.f.opcode_c_mathindex_12H) //New option change, save stock option values
            {
                status = obd2can_ford_60L_get_tire_size(&tmp_u32[0],&tmp_u32[1]); //tmp_u32[0]: front tire size, tmp_u32[1]: rear tire size
                if (status == S_SUCCESS)
                {
                    status = filestock_option_keep_tire_size(tmp_u32[0],tmp_u32[1]); // Save to filestock_option file                    
                }
            }
            if(status == S_SUCCESS)
            {
                status = obd2can_ford_60L_set_tire_size(front_tire_size, rear_tire_size); // Set new tire size
            }
        }
        //---------------------------------------------------------------------------------------------------
        //  opcode_c_mathindex_13H - Ford 6.4L tire size handled in tune file
        //---------------------------------------------------------------------------------------------------        
        if(status == S_SUCCESS && options_flags.f.opcode_c_mathindex_14H) // opcode_c_mathindex_14H - Ford Tire Size Method 1 - 6.7L, 6.2L, 5.0L Trucks
        {
            //No seperate front and rear tires size options, use rear tire size for single option
            if (!stock_options_flags.f.opcode_c_mathindex_14H)
            {
                status = obd2can_ford_m1_get_tire_size(&tmp_u32[0]);
                if(status == S_SUCCESS)
                {
                    status = filestock_option_keep_tire_size(tmp_u32[0],tmp_u32[0]);
                }
            }
            if(status == S_SUCCESS)
            {
                status = obd2can_ford_m1_set_tire_size(rear_tire_size);
            }
        }
        if(status == S_SUCCESS && options_flags.f.opcode_c_mathindex_15H) // opcode_c_mathindex_15H - Ford Axle Ratio Method 1 - 6.7L, 6.2L, 5.0L Trucks
        {
            if (!stock_options_flags.f.opcode_c_mathindex_15H)
            {
                status = obd2can_ford_m1_get_axle_ratio(&tmp_u32[0]);
                if(status == S_SUCCESS)
                {
                    status = filestock_option_keep_axle_ratio(tmp_u32[0]);
                }
            }
            if(status == S_SUCCESS)
            {
                status = obd2can_ford_m1_set_axle_ratio(axle_ratio);
            }
        }
        if(status == S_SUCCESS && (options_flags.f.opcode_c_mathindex_16H ||  options_flags.f.opcode_c_mathindex_17H)) // opcode_c_mathindex_16H - Ford TPMS Front Tire Method 1 - 6.7L, 6.2L, 5.0L, 3.5L Ecoboost trucks
        {
            u8 cur_front;
            u8 cur_rear;
            
            status = obd2can_ford_67L_get_tpms_info(&cur_front, &cur_rear);
            if(status == S_SUCCESS)
            {
                if(options_flags.f.opcode_c_mathindex_16H)
                {
                    if (!stock_options_flags.f.opcode_c_mathindex_16H)
                    {
                        status = filestock_option_keep_tire_pressure(cur_front, NULL);
                    }
                }
                else
                {
                    front_tire_pressure = 0;
                }
                if(options_flags.f.opcode_c_mathindex_17H)
                {
                    if (!stock_options_flags.f.opcode_c_mathindex_17H)
                    {                    
                        status = filestock_option_keep_tire_pressure(NULL, cur_rear);
                    }
                }
                else
                {
                    rear_tire_pressure = 0;
                }
                if(status == S_SUCCESS)
                {
                    status = obd2can_ford_67L_set_tpms_info(front_tire_pressure, rear_tire_pressure);
                }
            }
        }        
    }
    
    filestock_option_download_deinit();
    
    return status;    
}
