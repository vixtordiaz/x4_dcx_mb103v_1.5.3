/**
  **************** (C) COPYRIGHT 2011 SCT Performance, LLC *********************
  * File Name          : obd2tune_ford_checksum_6R_ZF.h
  * Device / System    : SCT products using the "CommonCode" platform
  * Author             : Patrick Downs, Quyen Leba
  *
  * Version            : 1 
  * Date               : 08/15/2012
  * Description        : Checksum functions for Bosch 6R(US)/ZF(AU) TCMs 
  *                      
  *
  *
  *
  ******************************************************************************
  */

#ifndef __OBD2TUNE_FORD_CHECKSUM_6R_ZF_H
#define __OBD2TUNE_FORD_CHECKSUM_6R_ZF_H

int Bosch_6R_checksum(u8 ecm_type, u32 offset);
int Bosch_AU_ZF_checksum(u8 ecm_type, u32 offset);

#endif // __OBD2TUNE_FORD_CHECKSUM_6R_ZF_H