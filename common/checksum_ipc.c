/**
  **************** (C) COPYRIGHT 2011 SCT Performance, LLC *********************
  * File Name          : checksum_ipc.c
  * Device / System    : SCT products using the "CommonCode" platform
  * Author             : Quyen Leba
  *
  * Version            : 1 
  * Date               : 05/02/2011
  * Description        : 
  *                    : 
  *
  *
  * History            : 05/02/2011 M. Davis
  *                    :   Add SCT Copyright header,
  *
  ******************************************************************************
  */

//------------------------------------------------------------------------------
// Log error point: 0x44xx -> 0x46xx (shared with obd2tune.c)
//------------------------------------------------------------------------------

#include <string.h>
#include <board/genplatform.h>
#include <common/genmanuf_overload.h>
#include <common/obd2tune.h>
#include <common/crypto_blowfish.h>
#include <common/statuscode.h>
#include <common/filestock.h>
#include "checksum_ipc.h"

#define IPC_MAX_SECTION             7
const u8 convert[16] = {0x0,0x1,0x2,0x3,0x4,0x5,0x6,0x7,0x8,0x9,0xA,0xB,0xC,0xD,0xE,0xF};

//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
u8 __set_ipc(F_FILE *fptr, u32 checksum_pos,
             u32 checksum_value, bool isLittleEndian);
u8 __get_ipc(F_FILE *fptr, u32 startpos, u32 endpos,
             u32 *checksum_value, bool isLittleEndian);

//------------------------------------------------------------------------------
// Apply IPC checksum to a file
// Inputs:  F_FILE  *tune_fptr (file to apply the checksum to)
//          u8  *ipc_filename (file contain ipc checksum info)
//          u16 ecm_type
//          u32 offset (starting position of ecm in file)
// Return:  u8  status
// Engineer: Quyen Leba, Patrick Downs
//------------------------------------------------------------------------------
u8 checksum_ipc_apply_to_tunefile(F_FILE *tune_fptr, u8 *ipc_filename,
                                  u16 ecm_type, u32 offset)
{
    F_FILE  *fptr;
    u8 *bptr;
    u32 addr_cnt;
    u32 i,j, index;
    u32 data;
    u32 *addr;
    u32 ipc_filesize;
    u32 bytecount;
    u8  status;

    fptr = NULL;
    bptr = NULL;
    addr = NULL;
    status = S_SUCCESS;

    addr_cnt = 0;

    // Check for .ipc file extension
    if(strstr((char*)ipc_filename, ".ipc") == 0)
    {
        log_push_error_point(0x4600);
        return S_INPUT;
    }

    //preloaded ipc from default folder & custom ipc from user folder
    fptr = genfs_general_openfile(ipc_filename, "rb");
    if (fptr == NULL)
    {
        log_push_error_point(0x4601);
        return S_FILENOTFOUND;
    }

    ipc_filesize = fgetfilesize(fptr);
    if(ipc_filesize > 1024)
    {
        // Should be smaller than 1024
        log_push_error_point(0x4602);
        status = S_FAIL;
        goto checksum_ipc_apply_to_tunefile_done;
    }
    
    bptr = __malloc(ipc_filesize);
    if (bptr == NULL)
    {
        log_push_error_point(0x4603);
        status = S_MALLOC;
        goto checksum_ipc_apply_to_tunefile_done;
    }

    memset((char*)bptr, 0, ipc_filesize);
    bytecount = fread((char*)bptr, 1, ipc_filesize, fptr);
    if (bytecount != ipc_filesize)
    {
        log_push_error_point(0x4604);
        status = S_READFILE;
        goto checksum_ipc_apply_to_tunefile_done;
    }
#if IPC_CHECKSUM_ENCRYPTION_ENABLED
    else if (bytecount % 8)
    {
        log_push_error_point(0x4605);
        status = S_BADCONTENT;
        goto checksum_ipc_apply_to_tunefile_done;
    }
#endif
    genfs_closefile(fptr);
    fptr = NULL;
    
#if IPC_CHECKSUM_ENCRYPTION_ENABLED
    crypto_blowfish_decryptblock_critical((u8*)bptr,ipc_filesize);
    while(ipc_filesize && bptr[ipc_filesize-1] == NULL)
    {
        ipc_filesize--; //remove padding ZERO from encryption
    }
#endif
    
    addr = __malloc(3*IPC_MAX_SECTION*4);
    if (addr == NULL)
    {
        log_push_error_point(0x4606);
        status = S_MALLOC;
        goto checksum_ipc_apply_to_tunefile_done;
    }

    for (i=0; i<ipc_filesize; i++)
    {
        if ((bptr[i]==',') && (bptr[i+1]==' '))
        {
            for (j=i+2, data=0; j<i+12; j++)
            {
                if ((bptr[j]==0x0d) && (bptr[j+1]==0x0a))
                {
                    break;
                }
                if (bptr[j] & 0x40)
                {
                    index = (bptr[j] & 0xf) + 9;
                }
                else
                {
                    index = bptr[j] & 0xf;
                }
                data = (data << 4) | convert[index];              
            }
            addr[addr_cnt++] = data;
        }
    }
    __free(bptr);
    bptr = NULL;
    
    for (i=0; i<addr_cnt; i=i+3)
    {
        status = __get_ipc(tune_fptr, addr[i] + offset, addr[i+1] + offset + 1,
                           &data, (bool)ECM_IsLittleEndian(ecm_type));
        if (status != S_SUCCESS)
        {
            log_push_error_point(0x4607);
            goto checksum_ipc_apply_to_tunefile_done;
        }
        status = __set_ipc(tune_fptr, addr[i+2] + offset,
                           data, (bool)ECM_IsLittleEndian(ecm_type));
        if (status != S_SUCCESS)
        {
            log_push_error_point(0x4608);
            goto checksum_ipc_apply_to_tunefile_done;
        }
    }
    
    __free(addr);
    addr = NULL;

checksum_ipc_apply_to_tunefile_done:
    if (fptr)
    {
        genfs_closefile(fptr);
    }
    if (bptr)
    {
        __free(bptr);
    }
    if (addr)
    {
        __free(addr);
    }
    return status;
}

//------------------------------------------------------------------------------
// Set IPC checksum to a file
// Inputs:  F_FILE  *fptr
//          u32 checksum_pos (in file)
//          u32 checksum_value
// Return:  u8  status
// Engineer: Quyen Leba, Patrick Downs
//------------------------------------------------------------------------------
u8 __set_ipc(F_FILE *fptr, u32 checksum_pos, u32 checksum_value, bool isLittleEndian)
{
    u8  data[4];

    if (fptr)
    {
        if(isLittleEndian)
        {
            data[0] = checksum_value & 0xff;
            data[1] = (checksum_value >> 8) & 0xff;
            data[2] = (checksum_value >> 16) & 0xff;
            data[3] = (checksum_value >> 24) & 0xff;
        }
        else
        {
            data[3] = checksum_value & 0xff;
            data[2] = (checksum_value >> 8) & 0xff;
            data[1] = (checksum_value >> 16) & 0xff;
            data[0] = (checksum_value >> 24) & 0xff;
        }
        
        if(filestock_updateflashfile(checksum_pos,
                                     (u8*)data,(u32)sizeof(data)) != S_SUCCESS)
        {
            log_push_error_point(0x4609);
            return S_WRITEFILE;
        }
        
        return S_SUCCESS;
    }
    log_push_error_point(0x460A);
    return S_FAIL;
}

//------------------------------------------------------------------------------
// Calculate IPC checksum
// Inputs:  F_FILE  *fptr
//          u32 startpos (begining of checksum block in file)
//          u32 endpos
// Output:  u32 *checksum_value
// Return:  u8  status
// Engineer: Quyen Leba, Patrick Downs
//------------------------------------------------------------------------------
u8 __get_ipc(F_FILE *fptr, u32 startpos, u32 endpos,
             u32 *checksum_value, bool isLittleEndian)
{
    u32 romCksmTemp;
    u32 bytecount;
    u32 byteread;
    u32 bytetotal;
    u32 i;
    u32 remainderTemp;
    u32 addr;
    u32 interVal;
    u8  data[4];
    u8 *buff;
    u32 index;
    u32 temp;

    *checksum_value = 0;
    romCksmTemp = 0;

    bytetotal = endpos - startpos;
    addr = startpos;
    
    if (startpos >= endpos)
    {
        log_push_error_point(0x460B);
        return S_INPUT;
    }
    
    if(filestock_set_position(addr) != S_SUCCESS)
    {
        log_push_error_point(0x460C);
        return S_SEEKFILE;
    }
          
    buff = __malloc(4096);
    if(buff == NULL)
    {
        log_push_error_point(0x460D);
        return S_MALLOC;
    }
    
    while(bytetotal > 0)
    {
        index = 0;
        if (bytetotal < 4096)
        {
            byteread = bytetotal;
        }
        else
        {
            byteread = 4096;
        }
        
        if(filestock_read((u8*)buff, byteread, (u32*)&bytecount) != S_SUCCESS)
        {
            log_push_error_point(0x460E);
            return S_FAIL;
        }
            
        if (bytecount == 0)
        {
            break;
        }

        // checkSum for integer part Of TuneItem
        for (i=0; i<(bytecount/4); i++)
        {
            if(isLittleEndian)
            {
                data[0] = buff[index++];
                data[1] = buff[index++];
                data[2] = buff[index++];
                data[3] = buff[index++];
            }
            else
            {
                data[3] = buff[index++];
                data[2] = buff[index++];
                data[1] = buff[index++];
                data[0] = buff[index++];
            }

            interVal = *((u32*)data);
            romCksmTemp += interVal;
        }
        
        // check Sum for remainder part of the TuneItem
        if ((bytecount%4) != 0)
        {   
            if(isLittleEndian)
            {
                data[0] = buff[index++];
                data[1] = buff[index++];
                data[2] = buff[index++];
                data[3] = buff[index++];
            }
            else
            {
                data[3] = buff[index++];
                data[2] = buff[index++];
                data[1] = buff[index++];
                data[0] = buff[index++];
            }

            interVal = *((u32*)data);
            //TODOQ: check again (use u32; original code use int)
            temp  = (u32)interVal  >>  (( 4 - ((u32)bytecount%4)) * 8);
            remainderTemp = (u32)temp;
            romCksmTemp += remainderTemp;
        }
        bytetotal -= bytecount;
    }
    __free(buff);

    *checksum_value = romCksmTemp;

    return S_SUCCESS;
}
