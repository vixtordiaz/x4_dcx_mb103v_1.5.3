/************************* gvpscrol.c ******************************

   Controls the use of view-ports

   Creation date: 980223

   Revision date:      02-01-27
   Revision Purpose:   Support for soft cursors change.
   Revision date:      03-01-26
   Revision Purpose:   Bit mask on GINVERSE mode
   Revision date:     13-08-04
   Revision Purpose:  Named viewport function _vp added

   Version number: 2.2
   Copyright (c) RAMTEX Engineering Aps 1998-2004

*********************************************************************/

#include <gi_disp.h> /* gLCD prototypes */

#ifdef GBASIC_TEXT

/********************************************************************
   Segment: Viewport
   Level: Fonts
   Scroll graphics and soft-fonts in view-port a character line up.
   The number of pixel lines depend on the current font height
   If HW FONT is selected the appropriate ghw_gscroll/ghw_tscroll
   function is called.
*/
void gscrollcvp(void)
   {
   glcd_err = 0;   /* Reset HW error flag */
   gi_datacheck(); /* check internal data for errors */

   if( gcurvp->gi_noupdate == 0)
      {
      gi_cursor( 0 ); /* kill cursor */
      }

   /* Scroll text fonts */
   ghw_tscroll(
      gcurvp->lt.x,
      gcurvp->lt.y,
      gcurvp->rb.x,
      gcurvp->rb.y);

   #if defined(GSOFT_FONTS) || defined(GSYMBOLS) || defined(GGRAPHIC)
   /* Scroll graphic symbols */
   ghw_gscroll(
            gcurvp->lt.x,
            gcurvp->lt.y,
            gcurvp->rb.x,
            gcurvp->rb.y,
            (GYT)gcurvp->pfont->symheight,
            (SGUINT)(G_IS_INVERSE() ? 0xFFFF : 0x0000));
   #endif

   if( gcurvp->gi_noupdate == 0)
      {
      gi_cursor( 1 ); /* set cursor */
      ghw_updatehw();
      }
   }

#ifdef GFUNC_VP

void gscrollcvp_vp( SGUCHAR vp )
   {
   GSETFUNCVP(vp, gscrollcvp() );
   }

#endif /* GFUNC_VP */
#endif /* GBASIC_TEXT */

