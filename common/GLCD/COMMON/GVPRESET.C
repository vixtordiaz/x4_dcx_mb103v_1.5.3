/************************* gvpreset.c ******************************

   Creation date: 980220

   Revision date:    05-05-03
   Revision Purpose: Cursor position tracks font symheight.
                     gresetposvp() added.

   Revision date:     13-08-04
   Revision Purpose:  Named viewport function _vp added

   Revision date:
   Revision Purpose:

   Version number: 2.1
   Copyright (c) RAMTEX Engineering Aps 1998-2004

*********************************************************************/

#include <gi_disp.h> /* gLCD prototypes */

#ifdef GVIEWPORT

static void gi_resetvp(SGUCHAR resetfont)
   {
   gi_datacheck();     /* check internal data for errors (assure valid defaults) */
   gi_cursor( 0 );     /* kill cursor (using old font size) */

   /* Set full screen coordiantes */
   gcurvp->lt.x = 0;
   gcurvp->lt.y = 0;
   gcurvp->rb.x = GDISPW-1;
   gcurvp->rb.y = GDISPH-1;

   #ifdef GGRAPHICS
   /* reset pixel pos */
   gcurvp->ppos.x = gcurvp->ppos.y = 0;
   #endif /* GGRAPHICS */

   gcurvp->cpos.x = 0;
   if (resetfont)
      {
      gcurvp->cpos.y = SYSFONT.symheight-1;

      /* reset mode */
      gcurvp->mode = GNORMAL;
      /* reset font */
      gcurvp->pfont = &SYSFONT;
      /* preset code-page */
      gcurvp->codepagep = SYSFONT.pcodepage;
      }
   else
      gcurvp->cpos.y = gcurvp->pfont->symheight-1;

   gi_cursor( 1 );     /* set cursor (using new font size) */
   gi_calcdatacheck(); /* correct VP to new settings */
   }

/********************************************************************
   Segment: Viewport
   Level: View-port
   Reset the current view-port to full screen,
   set the cursor position to the upper left corner,
   set the pixel position to the upper left corner,
   set code-page to NULL.
*/
void gresetposvp(void)
   {
   gi_resetvp(0);
   }

/********************************************************************
   Segment: Viewport
   Level: View-port
   Reset the current view-port to full screen,
   set the cursor position to the upper left corner,
   set the pixel position to the upper left corner,
   set mode to GNORMAL,
   set font to SYSFONT,
   set code-page to NULL.
*/
void gresetvp(void)
   {
   gi_resetvp(1);
   }

#ifdef GFUNC_VP

void gresetposvp_vp( SGUCHAR vp )
   {
   GSETFUNCVP(vp, gi_resetvp(0) );
   }

void gresetvp_vp( SGUCHAR vp )
   {
   GSETFUNCVP(vp, gi_resetvp(1) );
   }

#endif /* GFUNC_VP */
#endif

