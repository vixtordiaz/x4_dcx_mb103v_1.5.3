/************************* gstrlnw.c ********************************

   Creation date: 03-05-03

   Revision date:
   Revision Purpose:

   Version number: 1.0
   Copyright (c) RAMTEX Engineering Aps 2003

*********************************************************************/
#include <gi_disp.h> /* gLCD prototypes */

#if (defined(GBASIC_TEXT) && defined(GWIDECHAR))

/*
   Returns the number of text lines in a multi-line wide-char string
   Returns 0 if str = 0
   Returns 1 if *str = 0
*/
SGUCHAR gstrlinesw( PGCWSTR str )
   {
   SGUCHAR lh = 1;

   if (str == NULL)
      return 0;

   while (*str != L'\0')  /* Count number of lines */
      {
      if (*str == L'\n')
         {
         lh++;
         if (lh >= 255)
            break;       /* Prevent overflow */
         }
      str++;
      }
   return lh;
   }

#endif
