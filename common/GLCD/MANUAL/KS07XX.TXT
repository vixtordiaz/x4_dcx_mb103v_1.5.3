KS07XX NOTES
------------
This document describes configurations special for the KS07xx
family of LCD controllers. This is configuration possibilities
in extension to the configurations described in the printed
manual (and the file manual.txt).


LCD CONTROLLER SELECTION
------------------------
The LCD controller command set supported by the KS07xx driver
library is common for the KS07xx, NT750x, and SED156x LCD
controller families. The commands used during display write
and read are the same for all the LCD controller. There is
only minor differences is in the initialization commands.

These variations is handled by the LCD controller definitions
in GDISPCFG.H. Select one of the LCD controller families.

/* #define GHW_KS0719 */
   #define GHW_KS07xx 
/* #define GHW_NT750x */
/* #define GHW_SED1565 */

The purpose with the LCD controller definement is to select
the right initialization sequence and the pixel width of the
internal video RAM.


DISPLAY MODULE CONFIGURATIONS
-----------------------------
The scan direction of KS07xx family of LCD controllers can be
mirrored horizontally and vertically. The advantage is that it
gives the display module vendor some freedom in selecting the
most optimal display layout. The disadvantage is that the scan
direction must be configured individually for each display
module. Further more the LCD controllers is often used with
displays which has a pixel width less than the pixel width of
the internal video RAM, requiring an offset to be added to the
column address.

These display configurations is selected with these
configuration switches in GDISPCFG.H :

/*#define GHW_MIRROR_VER */  /* Define to mirror display vertically */
  #define GHW_MIRROR_HOR     /* Define to mirror display horizontally */
  #define GHW_XOFFSET  0     /* Add a column offset */

Modify these definitions to adjust the look on the actual display.


DISPLAY CONTRAST
----------------
The KS07xx LCD controller family uses internal contrast
regulation. In GDISPCFG.H select:

   #define GHW_INTERNAL_CONTRAST

to enable the contrast regulation functions.


USE OF SERIAL BUS DRIVER 
------------------------
The KS07xx LCD controller has no readback facility in serial
mode. Therefore buffered mode must be used in order to enable
read-modify-write operations on display data. 
Select buffered mode in GDISPCFG.H

   #defined GBUFFER

The serial interface must be implemented in user defined 
driver functions. 
In the compiler setting define: 
   GHW_SINGLE_CHIP

A template for a serial mode interface is located in 
\GLCD\controller\SERIAL\*.*


CONTROLLING EXTRA LCD SCREEN ICONS
----------------------------------
Some LCD screens has additional fixed graphical 
icons build-in outside the normal grahical pixel area. 
For instance as a row of fixed icons on the top or the 
bottom of the LCD screen.

These fixed icons are normally controlled on-off by 
setting pixel on an extra Y page outside the normal
screen area defined by the GXT, GYT setting. 
This extra Y page may only have one or two pixel rows.

You can reach the extra "pixels" by using these 
low-level functions in ghwinit.c
    ghw_set_xpos(..)  // Set X coordinate
    ghw_set_ypos(..)  // Set Y coordinate
    ghw_wr(..)        // Write vertical pixel byte

The functions are prototyped in the KS07XX.H header.

As nearly every display type is different it is
probably necessary to do some experiments to find 
out which pixel coordinates controls which icon 
symbol. 

Example:

  #include <gdisp.h>
  #include <ks07xx.h>

  void set_icon(GXT x, GYT y, SGUCHAR pagebyte)
     {
     ghw_set_xpos(x);  // Set X coordinate
     ghw_set_ypos(y);  // Set Y coordinate (internally turncated to whole pages)
     ghw_wr(pagebyte)  // Write vertical byte of 1 to 8 pixels
     }

If the display is 128x64 the normal y coordinates
are [0-63] = 8 y-pages. The icon row is typically 
on the next y-page = row 64 = GYT+1

All fixed icons can usually be turned on by making 
a test function like this:

  void turn_icons_on(void)
     {
     GXT x;
     for (x=0; x<GXT; x++)
        set_icon(x,GYT+1,0xff);
     }


