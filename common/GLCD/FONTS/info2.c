/****************************    info2.c ***********************

   info2 font table and code page structure definitions.
   This file has been auto-generated with the IconEdit / FontEdit tool.

   Copyright(c) RAMTEX 1998-2006

*****************************************************************/
#include <gdisphw.h>

/* Code page entry (one codepage range element) */
static struct
   {
   GCPHEAD chp;
   GCP_RANGE cpr[2];     /* Adjust this index if more codepage segments are added */
   }
GCODE FCODE info2cp =
   {
   #include "info2.cp" /* Codepage table */
   };

typedef struct          /* Structure used for automatic word alignment */
   {
   SGUCHAR b[8];       /* Symbol data, "variable length" array */
   } GSYMDAT;

/* Symbol table entry with fixed sized symbols */
static struct
   {
   GSYMHEAD sh;         /* Symbol header */
   SGUCHAR b[8];       /* Symbol data, "variable length" */
   }
GCODE FCODE info2sym[104] =
   {
   #include "info2.sym" /* Include symbols */
   };

/* Font structure */
GCODE GFONT FCODE info2 =
   {
   8,       /* width */
   8,       /* height */
   sizeof(GSYMDAT),    /* number of bytes in a symbol  (must include any alignment padding)*/
   (PGSYMBOL)info2sym, /* pointer to array of SYMBOLS */
   103,      /* num symbols */
   (PGCODEPAGE)&info2cp
   };

