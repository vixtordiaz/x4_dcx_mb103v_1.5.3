/****************************    bold2.c ***********************

   bold2 font table and code page structure definitions.
   This file has been auto-generated with the IconEdit / FontEdit tool.

   Copyright(c) RAMTEX 1998-2006

*****************************************************************/
#include <gdisphw.h>

/* Code page entry (one codepage range element) */
static struct
   {
   GCPHEAD chp;
   GCP_RANGE cpr[3];     /* Adjust this index if more codepage segments are added */
   }
GCODE FCODE bold2cp =
   {
   #include "bold2.cp" /* Codepage table */
   };

typedef struct          /* Structure used for automatic word alignment */
   {
   SGUCHAR b[8];       /* Symbol data, "variable length" array */
   } GSYMDAT;

/* Symbol table entry with fixed sized symbols */
static struct
   {
   GSYMHEAD sh;         /* Symbol header */
   SGUCHAR b[8];       /* Symbol data, "variable length" */
   }
GCODE FCODE bold2sym[102] =
   {
   #include "bold2.sym" /* Include symbols */
   };

/* Font structure */
GCODE GFONT FCODE bold2 =
   {
   8,       /* width */
   8,       /* height */
   sizeof(GSYMDAT),    /* number of bytes in a symbol  (must include any alignment padding)*/
   (PGSYMBOL)bold2sym, /* pointer to array of SYMBOLS */
   102,      /* num symbols */
   (PGCODEPAGE)&bold2cp
   };

