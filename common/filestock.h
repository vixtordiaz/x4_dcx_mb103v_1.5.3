/**
  **************** (C) COPYRIGHT 2011 SCT Performance, LLC *********************
  * File Name          : filestock.h
  * Device / System    : SCT products using the "CommonCode" platform
  * Author             : Quyen Leba
  *
  * Version            : 1 
  * Date               : 05/02/2011
  * Description        : 
  *                    : 
  *
  *
  * History            : 05/02/2011 M. Davis
  *                    :   Add SCT Copyright header,
  *
  ******************************************************************************
  */

#ifndef __FILESTOCK_H
#define __FILESTOCK_H

#include <arch/gentype.h>
#include <common/file.h>

#define FILESTOCK_USE_ENCRYPTION                1
#define FILESTOCK_SKIP_VALIDATE_STOCK_FILE      0

#define STOCK_FILENAME                          GENFS_USER_FOLDER_PATH"stock.bin"
#define STOCK_FILENAME_NOPATH                   "stock.bin"
//#define SPECIAL_STOCK_FILENAME                  "specialstock.bin"
#define SPECIAL_STOCK_FILENAME                  GENFS_USER_FOLDER_PATH"stock.bin"
#define FLASH_FILENAME                          GENFS_USER_FOLDER_PATH"flash.bin"
#define FILESTOCK_DATABUF_SIZE                  0x1000

typedef enum
{
    FILESTOCK_USE_STOCKFILE,
    FILESTOCK_USE_FLASHFILE,
}FileStock_Filetype;

#define FILESTOCK_HANDLING_NONE                 0
#define FILESTOCK_HANDLING_STOCKFILE            1
#define FILESTOCK_HANDLING_FLASHFILE            2

typedef struct
{
    F_FILE  *filestock_fptr;
    u32 *key;
    u8  residuebuffer[8];
    u8  residuebufferlength;
    struct
    {
        u8  filehandlingtype        : 4;
        u8  readonly                : 1;
        u8  reserved                : 3;
    }control;
}FileStock_Info;

u8 filestock_verify(u32 size_fromsettings, u32 size_fromvehdef);
u8 filestock_delete(bool isspecialstock);

u8 filestock_openfile(u8 *filename, u8 *attrib, FileStock_Filetype filetype);

u8 filestock_openstockfile(u8 *attrib, bool isspecialstock);
u8 filestock_closestockfile();
u8 filestock_openflashfile(u8 *attrib);
F_FILE * filestock_getflashfile_fptr();
u8 filestock_closeflashfile();

u8 filestock_upload_init(bool isspecialstock);
u8 filestock_upload_deinit();
u8 filestock_download_init(FileStock_Filetype filetype);
u8 filestock_download_deinit();

u8 filestock_write(u8 *data, u32 datalength);
u8 filestock_read(u8 *data, u32 datalength, u32 *bytecount);
u8 filestock_read_at_position(u32 position, u8 *data, u32 datalength,
                              u32 *bytecount);
u8 filestock_forward(u32 count);
u8 filestock_backward(u32 count);
u8 filestock_fill(u8 byte, u32 count);
u8 filestock_set_position(u32 pos);
u8 filestock_get_position(u32 *pos);
u8 filestock_getfilesize(u32 *size);
u8 filestock_generate_flashfile_fromstock(file_copy_progressreport progressreport_funcptr);
u8 filestock_generate_flashfile_fromstock_advance(file_info *basefilestockinfo,
                                                  file_copy_progressreport progressreport_funcptr);
u8 filestock_generate_flashfile_fromfile(u8 *filename,
                                         file_copy_progressreport progressreport_funcptr);
u8 filestock_updateflashfile(u32 position, u8 *pdata, u32 length);
u8 filestock_append_file(u8 *filename, u32 *appendlength);

u8 filestock_validate(FileStock_Filetype filetype, u32 *crc32e);

FileStock_Info filestock_getinfo(void);

u8 filestock_overlay_flashfile(u32 position, u32 length, 
                               file_copy_progressreport progressreport_funcptr);
u8 filestock_bootloader_position_check(void);

#endif     //__FILESTOCK_H
