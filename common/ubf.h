/**
  **************** (C) COPYRIGHT 2011, 2012 SCT Performance, LLC ***************
  * File Name          : ubf.h
  * Device / System    : SCT products using the "CommonCode" platform
  * Author             : Patrick Downs
  *
  * Version            : 1 
  * Date               : 09/05/2012
  * Description        : Utility Bootloader Files. These are preloaded utility/
  *                    : secondary bootloader files for PCMs/TCMs
  *
  * 
  *
  ******************************************************************************
  */

#ifndef __UBF_H
#define __UBF_H

#define ubf_extention           ".ubf"
#define ubf_start_offset        0x2800 // 10k from start of file

u8 ubf_validate_file(u8 *filename);
u8 ubf_get_data_info(F_FILE *f, u32 *position, u32 *length);
u8 ubf_seek_start(F_FILE *f);
u8 ubf_read_block(u8 *databuffer, u32 blocklength, F_FILE *f);

#endif  //__UBF_H