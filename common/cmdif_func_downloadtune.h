/**
  **************** (C) COPYRIGHT 2011 SCT Performance, LLC *********************
  * File Name          : cmdif_func_downloadtune.h
  * Device / System    : SCT products using the "CommonCode" platform
  * Author             : Quyen Leba
  *
  * Version            : 1 
  * Date               : 05/02/2011
  * Description        : 
  *                    : 
  *
  *
  * History            : 05/02/2011 M. Davis
  *                    :   Add SCT Copyright header,
  *
  ******************************************************************************
  */

#ifndef __CMDIF_FUNC_DOWNLOADTUNE_H
#define __CMDIF_FUNC_DOWNLOADTUNE_H

#include <arch/gentype.h>

u8 cmdif_func_downloadtune_init();
u8 cmdif_func_downloadtune_do();
u8 cmdif_func_downloadtune_health_check(u16 veh_type);

#endif    //__CMDIF_FUNC_DOWNLOADTUNE_H
