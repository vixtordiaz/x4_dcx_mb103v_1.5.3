/**
  **************** (C) COPYRIGHT 2011 SCT Performance, LLC *********************
  * File Name          : cmdif_func_production_test.h
  * Device / System    : SCT products using the "CommonCode" platform
  * Author             : Quyen Leba
  *
  * Version            : 1 
  * Date               : 05/02/2011
  * Description        : 
  *                    : 
  *
  *
  * History            : 05/02/2011 M. Davis
  *                    :   Add SCT Copyright header,
  *
  ******************************************************************************
  */

#ifndef __CMDIF_FUNC_PRODUCTION_TEST_H
#define __CMDIF_FUNC_PRODUCTION_TEST_H

#include <arch/gentype.h>

// Production Test Task Definitions
#define PRODUCTION_TEST_TASK_COMMLINK                   0
#define PRODUCTION_TEST_TASK_REV                        1
#define PRODUCTION_TEST_TASK_FORMAT                     2
#define PRODUCTION_TEST_TASK_VEHICLE_COMM               3
#define PRODUCTION_TEST_TASK_ADC                        4   //run after vehicle comm test
#define PRODUCTION_TEST_TASK_LED                        5
#define PRODUCTION_TEST_TASK_USB                        6
#define PRODUCTION_TEST_TASK_WIFI                       7
#define PRODUCTION_TEST_TASK_VBBOOTEN                   8
#define PRODUCTION_TEST_TASK_COMPLETED                  9

u8 cmdif_func_production_test_rev(u8 *testreport);
u8 cmdif_func_production_test_vbbooten(u8 *testreport);
u8 cmdif_func_production_test_format(u8 *testreport);
u8 cmdif_func_production_test_vehicle_comm(u8 *testreport);
u8 cmdif_func_production_test_adc(u8 *testreport);
u8 cmdif_func_production_test_wifi(u8 *testreport);
u8 cmdif_func_production_test_usb(u8 *testreport);
u8 cmdif_func_production_test_completion(u8 *testreport);

#endif    //__CMDIF_FUNC_PRODUCTION_TEST_H
