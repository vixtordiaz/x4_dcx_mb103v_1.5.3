/**
  **************** (C) COPYRIGHT 2011 SCT Performance, LLC *********************
  * File Name          : crypto_messageblock.c
  * Device / System    : SCT products using the "CommonCode" platform
  * Author             : Quyen Leba
  *
  * Version            : 1 
  * Date               : 05/02/2011
  * Description        : 
  *                    : 
  *
  *
  * History            : 05/02/2011 M. Davis
  *                    :   Add SCT Copyright header,
  *
  ******************************************************************************
  */

#include <string.h>
#include <board/genplatform.h>
#include <common/statuscode.h>
#include <common/crc32.h>
#include <common/crypto_blowfish.h>
#include <common/crypto_messageblock.h>

//------------------------------------------------------------------------------
// Decrypt messageblock to extract its original content
// Inputs:  u8 *encrypted_data
//          ([4:crc32e][1:offset][1:len][2:reserved][56:data]) - normal
//          ([4:crc32e][1:offset][2:len][1:reserved][n:data])  - extended
//          u32 encrypted_data_length
//          bool extendedlength
// Output:  u8  *decrypted_data (the data embedded in the encrypted block)
//          u32 *decrypted_data_length
// Return:  u8  status
// Note: encrypted_data is modified in this function
// Engineer: Quyen Leba
//------------------------------------------------------------------------------
u8 crypto_messageblock_decrypt(u8 *encrypted_data, u32 encrypted_data_length,
                               u8 *decrypted_data, u32 *decrypted_data_length,
                               bool extendedlength, bool useinternalkey)
{
    u8  *decbuffer;
    u32 crc32e;
    u32 crc32ecalc;
    u32 offset;
    u32 len;
    
    len = 0;
    *decrypted_data_length = 0;

    if ((encrypted_data_length == 0) || ((encrypted_data_length % 8) != 0))
    {
        return S_BADCONTENT;
    }
    else if (encrypted_data_length > (2048+16))
    {
        return S_BADCONTENT;
    }
    else if ((encrypted_data_length > MAX_CRYPTO_MESSAGE_BLOCK_LENGTH) &&
             (extendedlength == FALSE))
    {
        return S_BADCONTENT;
    }

    if (useinternalkey)
    {
        crypto_blowfish_decryptblock_internal_key(encrypted_data,
                                                  encrypted_data_length);
    }
    else
    {
        crypto_blowfish_decryptblock_external_key(encrypted_data,
                                                  encrypted_data_length);
    }
    decbuffer = encrypted_data;
    crc32e = ((u8)decbuffer[0] & 0xFF) | ((u8)decbuffer[1] << 8) |
             ((u8)decbuffer[2] << 16)  | ((u8)decbuffer[3] << 24);
    offset = (u8)decbuffer[4];
    if (extendedlength)
    {
        len = (u32)decbuffer[5];
        len |= ((u32)decbuffer[6]) << 8;
    }
    else
    {
        len = (u8)decbuffer[5];
    }

    if (len > 2048 || (len + offset > (2048+16)) || (len+offset > encrypted_data_length))
    {
        return S_BADCONTENT;
    }
    if ((extendedlength == FALSE) &&
         (((len + (4+4)) > MAX_CRYPTO_MESSAGE_BLOCK_LENGTH) ||
          (len+offset > MAX_CRYPTO_MESSAGE_BLOCK_LENGTH)))
    {
        return S_BADCONTENT;
    }
    else
    {
        crc32ecalc = 0xFFFFFFFF;
        crc32e_reset();
        crc32ecalc = crc32e_calculateblock(crc32ecalc,
                                           (u32*)(&decbuffer[4+4]),
                                           (encrypted_data_length - (4+4))/4);
        if (crc32ecalc != crc32e)
        {
            return S_CRC32E;
        }
        else
        {
            memcpy((char*)decrypted_data,(char*)&decbuffer[offset],len);
            *decrypted_data_length = len;
        }
    }

    return S_SUCCESS;
}

//------------------------------------------------------------------------------
// Wrap data into an encrypted message block
// Inputs:  u8  *raw_data
//          u32 raw_data_length
//          CryptoMessageBlockLength lengthtype (control result length)
// Output:  u8  *encrypted_data
//          ([4:crc32e][1:offset][1:length][2:reserved][56:data] - normal
//          ([4:crc32e][1:offset][2:length][1:reserved][n:data]  - extended
//          where:
//          crc32e = crc32 ethernet of the rest of this block (i.e. length - 4)
//          offset : where the settings content start in this block
//          length : length of the settings
//          data   : the settings is in here (upto 56 bytes)
//          u32 *encrypted_data_length
// Return:  u8  status
// Note:    the use of offset,length,random to generate different settings
//          encrypted block with same settings data
//------------------------------------------------------------------------------
u8 crypto_messageblock_encrypt(u8 *raw_data, u32 raw_data_length,
                               u8 *encrypted_data, u32 *encrypted_data_length,
                               CryptoMessageBlockLength lengthtype, bool useinternalkey)
{
    u32 crc32e;
    u32 offset;
    u32 range;
    u32 maxlength;
    u32 i;
    bool extendedlength;

    extendedlength = FALSE;
    if (lengthtype != CryptoMessageBlockLength_Exact64)
    {
        extendedlength = TRUE;
    }
    
    if ((raw_data_length > 2048) ||
        ((raw_data_length +4+4) > MAX_CRYPTO_MESSAGE_BLOCK_LENGTH && extendedlength == FALSE))
    {
        if (encrypted_data_length != NULL)
        {
            *encrypted_data_length = 0;
        }
        return S_INPUT;
    }
    else
    {
        range = (u32)(((double)rand()/((double)RAND_MAX + (double)1))*0x7E83AF);
        srand((u32)range);

        if (extendedlength == FALSE)
        {
            range = MAX_CRYPTO_MESSAGE_BLOCK_LENGTH - raw_data_length - (4+4);
            offset = (4+4) +
                (u32)(((double)rand()/((double)RAND_MAX + (double)1))*range);
            if ((offset + raw_data_length >= MAX_CRYPTO_MESSAGE_BLOCK_LENGTH) ||
                (offset > 255) ||
                (offset + raw_data_length < (4+4)))
            {
                offset = (4+4);
            }
            
            for(i=(4+4);i<MAX_CRYPTO_MESSAGE_BLOCK_LENGTH;i++)
            {
                range = (u32)(((double)rand()/((double)RAND_MAX + (double)1))*255);
                encrypted_data[i] = (char)(range & 0xFF);
            }

            memcpy((char*)&encrypted_data[offset],(char*)raw_data,raw_data_length);
            encrypted_data[4] = (offset & 0xFF);
            encrypted_data[5] = (raw_data_length & 0xFF);
            encrypted_data[6] = 0;
            
            crc32e_reset();
            crc32e = 0xFFFFFFFF;
            crc32e = crc32e_calculateblock(crc32e,(u32*)(&encrypted_data[4+4]),
                                           (MAX_CRYPTO_MESSAGE_BLOCK_LENGTH - (4+4))/4);
            
            encrypted_data[0] = (crc32e & 0xFF);
            encrypted_data[1] = (crc32e >> 8);
            encrypted_data[2] = (crc32e >> 16);
            encrypted_data[3] = (crc32e >> 24);
            
            if (useinternalkey)
            {
                crypto_blowfish_encryptblock_internal_key
                    (encrypted_data,MAX_CRYPTO_MESSAGE_BLOCK_LENGTH);
            }
            else
            {
                crypto_blowfish_encryptblock_external_key
                    (encrypted_data,MAX_CRYPTO_MESSAGE_BLOCK_LENGTH);
            }
            if (encrypted_data_length != NULL)
            {
                *encrypted_data_length = MAX_CRYPTO_MESSAGE_BLOCK_LENGTH;
            }
        }//if (extendedlength == FALSE)...
        else
        {
            maxlength = (raw_data_length/8)*8 + 24;
            maxlength += (((u32)(((double)rand()/((double)RAND_MAX + (double)1))*128))/8*8) + 8;
            if (maxlength < MAX_CRYPTO_MESSAGE_BLOCK_LENGTH)
            {
                maxlength = MAX_CRYPTO_MESSAGE_BLOCK_LENGTH;
            }
            else if (maxlength > (2048+16))
            {
                maxlength = 2048+16;
            }
            switch(lengthtype)
            {
            case CryptoMessageBlockLength_Exact64:
                maxlength = 64;
                break;
            case CryptoMessageBlockLength_Exact256:
                maxlength = 256;
                break;
            case CryptoMessageBlockLength_Exact512:
                maxlength = 512;
                break;
            case CryptoMessageBlockLength_Exact800:
                maxlength = 800;
                break;
            case CryptoMessageBlockLength_UptoMax:
            default:
                //no change
                break;
            }
            if ((raw_data_length +4+4) > maxlength)
            {
                //input message too big
                return S_INPUT;
            }

            range = maxlength - raw_data_length - (4+4);
            offset = (4+4) +
                (u32)(((double)rand()/((double)RAND_MAX + (double)1))*range);

            if ((offset + raw_data_length >= maxlength) ||
                (offset > 255) ||
                (offset + raw_data_length < (4+4)))
            {
                offset = (4+4);
            }
            for(i=(4+4);i<offset;i++)
            {
                range = (u32)(((double)rand()/((double)RAND_MAX + (double)1))*255);
                encrypted_data[i] = (char)(range & 0xFF);
            }
            for(i=offset+raw_data_length;i<maxlength;i++)
            {
                range = (u32)(((double)rand()/((double)RAND_MAX + (double)1))*255);
                encrypted_data[i] = (char)(range & 0xFF);
            }
            memcpy((char*)&encrypted_data[offset],(char*)raw_data,raw_data_length);
            
            encrypted_data[4] = (offset & 0xFF);
            encrypted_data[5] = (raw_data_length & 0xFF);
            encrypted_data[6] = (raw_data_length >> 8);
            
            crc32e_reset();
            crc32e = 0xFFFFFFFF;
            crc32e = crc32e_calculateblock(crc32e,(u32*)(&encrypted_data[4+4]),
                                           (maxlength - (4+4))/4);
            
            encrypted_data[0] = (crc32e & 0xFF);
            encrypted_data[1] = (crc32e >> 8);
            encrypted_data[2] = (crc32e >> 16);
            encrypted_data[3] = (crc32e >> 24);

            if (useinternalkey)
            {
                crypto_blowfish_encryptblock_internal_key(encrypted_data,maxlength);
            }
            else
            {
                crypto_blowfish_encryptblock_external_key(encrypted_data,maxlength);
            }
            if (encrypted_data_length != NULL)
            {
                *encrypted_data_length = maxlength;
            }
        }
    }

    return S_SUCCESS;
}
