/**
  **************** (C) COPYRIGHT 2011 SCT Performance, LLC *********************
  * File Name          : obd2sci.c
  * Device / System    : SCT products using the "CommonCode" platform
  * Author             : Ricardo Cigarroa
  *
  * Version            : 1 
  * Date               : 11/14/2014
  * Description        : 
  *                    : 
  *
  ******************************************************************************
  */
  
#include <board/delays.h>
#include <board/rtc.h>
#include <board/sci.h>
#include <common/statuscode.h>
#include <common/log.h>
#include <common/itoa.h>
#include <common/obd2comm.h>
#include <string.h>
#include "obd2sci.h"
#include "obd2iso.h"

//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------

extern u8 obd2_rxbuffer[256];                 /*from obd2.c*/
extern obd2_info gObd2info;                  /*from obd2.c*/

//------------------------------------------------------------------------------
// SCI txrx fucntion
// Input:   Obd2iso_ServiceData *servicedata
// Return:  u8  status
// Engineer: Ricardo Cigarroa
//------------------------------------------------------------------------------
u8 obd2sci_txrx(Obd2sci_ServiceData *servicedata)
{
    u8 status = S_SUCCESS; 
    
    status = obd2sci_tx(servicedata);
                          
    if(status == S_SUCCESS && servicedata->response_expected == TRUE)
    {
        status = obd2sci_rx(servicedata);
        if(status != S_SUCCESS)
        {
            status = S_FAIL; 
        }
    }
    else
    {
        status = S_FAIL; 
    }
    
    return status; 
}

//------------------------------------------------------------------------------
// SCI tx fucntion
// Input:   Obd2iso_ServiceData *servicedata
// Return:  u8  status
// Engineer: Ricardo Cigarroa
//------------------------------------------------------------------------------
u8 obd2sci_tx(Obd2sci_ServiceData *servicedata)
{
    u8 status;
    u8  data[3];
    u16 datalength;
    
    memset(data,0,sizeof(data));
    
    datalength = 0;
    if (servicedata->service != 0xFF)
    {
        data[datalength++] = servicedata->service;
    }
    if (servicedata->subservice != 0xFF)
    {
        data[datalength++] = servicedata->subservice;
    }
    if ((datalength+servicedata->service_controldatalength) > sizeof(data))
    {
        return S_INPUT;
    }
    memcpy((char*)&data[datalength],(char*)servicedata->service_controldatabuffer,
           servicedata->service_controldatalength);

    if(datalength)
    {
        servicedata->txlength = datalength + 
                                servicedata->service_controldatalength;
        status = sci_tx(data,servicedata->txlength,
                      servicedata->txflags);
    }
    else
    {
        servicedata->txlength = 0; 
        status = S_FAIL; 
    }
            
    return status; 
}

//------------------------------------------------------------------------------
// SCI rx fucntion
// Input:   Obd2iso_ServiceData *servicedata
// Return:  u8  status
// Engineer: Ricardo Cigarroa
//------------------------------------------------------------------------------   
u8 obd2sci_rx(Obd2sci_ServiceData *servicedata)
{
    u8 status; 
    u8 rxbuffer_index; 
    
    rxbuffer_index = 0;
    status = sci_rx(servicedata->rxbuffer,
                      &servicedata->rxlength,
                      servicedata->rxflags,
                      servicedata->rx_timeout_ms);
    
    if(status == S_SUCCESS)
    {
        /*Remove service and subservice from rx*/
        switch(servicedata->rxlength)
        {
            case 1:
                /*If the rx length is 1, accept it and verify it at the higher
                  level function.*/
                 break; 
            case 2: 
                 /*Remove service byte*/
                 servicedata->rxlength -= 1; 
                 rxbuffer_index = 1; 
                 break;
            case 3:
                /*Remove service and subservice byte*/
                 servicedata->rxlength -= 2; 
                 rxbuffer_index = 2; 
                 break; 
            case 4:
                 servicedata->rxlength -= 3; 
                 rxbuffer_index = 3;
                 break;
            default:
                if((servicedata->rxlength > 4) && (servicedata->rxlength <= 50))
                {
                    status = S_SUCCESS; 
                    servicedata->rxlength -= 1; 
                    rxbuffer_index = 1; 
                }
                else
                {
                    status = S_FAIL;
                    servicedata->rxlength = 0; 
                }
                break; 
        }
        
        memcpy((char*)servicedata->rxbuffer,(char*)&servicedata->rxbuffer[rxbuffer_index],
                   servicedata->rxlength); 
    }
    else
    {
        status = S_FAIL; 
        servicedata->rxlength = 0; 
    }
    
    return status; 
}

//------------------------------------------------------------------------------
// Initializes SCI servicedata.
// Input:   Obd2iso_ServiceData *servicedata
// Return:  void
// Engineer: Ricardo Cigarroa
//------------------------------------------------------------------------------
void obd2sci_servicedata_init(Obd2sci_ServiceData *servicedata)
{     
    servicedata->service = 0xFF; 
    servicedata->subservice = 0xFF; 
    
    memset(servicedata->service_controldatabuffer,
           0,sizeof(servicedata->service_controldatabuffer));
    servicedata->service_controldatalength = 0;   
    
    servicedata->rxbuffer = obd2_rxbuffer; 
    servicedata->rxlength = 0;
    servicedata->txbuffer = NULL; 
    servicedata->txlength = 2;
    servicedata->txflags = 0;
    servicedata->rxflags = 0;
     
    servicedata->expected_rxbufferlength = 10;
    
    servicedata->response_expected = TRUE; 
    servicedata->vehiclecommtype = CommType_Unknown;
    servicedata->vehiclecommlevel = CommLevel_Unknown;
    servicedata->rx_timeout_ms = 125;
}

//------------------------------------------------------------------------------
// SCI read part number
// Input:   void
// Return:  u8  status
// Engineer: Ricardo Cigarroa
//------------------------------------------------------------------------------
u8 obd2sci_getpartnumber(obd2_info_block *block)
{
    u8 status; 
    
    switch(block->commlevel)
    {
        case CommLevel_SCIA:
        case CommLevel_SBECA:
        case CommLevel_SBECB:
             status = obd2sci_getpartnumber_scia(block);
             break;
        case CommLevel_SCIB:
             status = obd2sci_getpartnumber_scib(block);
             break;
        default:
             status = S_INPUT;
             break;              
    }
    
    return status; 
}

//------------------------------------------------------------------------------
// Read VIN
// Inputs:   u8  *vin
//          
// Return:   u8  status
// Engineer: Ricardo Cigarroa
//------------------------------------------------------------------------------
u8 obd2sci_readvin(u8 *vin, VehicleCommType commtype, VehicleCommLevel commlevel)
{
    u8 status;
    
    status = sci_init(&commtype, &commlevel); 
    if(status == S_SUCCESS)
    {
        switch(commlevel)
        {
            case CommLevel_SCIA:
                 status = obd2sci_readvin_scia(vin, commtype, commlevel);
                 break;
            case CommLevel_SCIB:
                 status = obd2sci_readvin_scib(vin, commtype, commlevel);
                 break;
            case CommLevel_SBECA:
            case CommLevel_SBECB:
                 status = obd2sci_readvin_sbec(vin, commtype, commlevel);
                 break;
            default: 
                 status = S_INPUT;
                 break;
        }
    }
    else
    {
        status = S_FAIL; 
    }
    
    return status; 
}

/**
 *  @brief Read ECM info from OBD2 info
 *  
 *  @param [in]  ecm_info   ECM info structure
 *
 *  @return status
 *  
 *  @details This function reads all available part numbers and serial number.
 */
u8 obd2sci_readecminfo(ecm_info *ecm)
{
    ecm->ecm_count = gObd2info.ecu_count;
    memcpy(ecm->vin, gObd2info.vin, sizeof(ecm->vin));
    
    for(u8 i = 0; i < ECM_MAX_COUNT; i++)
    {
        if (gObd2info.ecu_block[i].ecutype != EcuType_Unknown)
        {
            ecm->commtype[i] = gObd2info.ecu_block[i].commtype;
            ecm->commlevel[i] = gObd2info.ecu_block[i].commlevel;
            ecm->codecount[i] = gObd2info.ecu_block[i].partnumber_count;
            memcpy(ecm->codes[i][0],
                   gObd2info.ecu_block[i].partnumbers.dcx.part_number,
                   sizeof(gObd2info.ecu_block[i].partnumbers.dcx.part_number));
        }
    }
    return S_SUCCESS;
}

/**
 *  @brief Get SCI OBD2 Info
 *  
 *  @param [in] obd2info OBD2 info structure
 *  @return status
 *  
 *  @details This function identifies and returns if applicable K-LINE/KWP2000
 *           information to the obd2info structure.
 */
u8 obd2sci_getinfo(obd2_info *obd2info, VehicleCommType vehiclecommtype, 
                   VehicleCommLevel vehiclecommlevel)
{
    u8 vin[VIN_LENGTH+1];
    u8 status;
    
    if (obd2info == NULL)
    {
        return S_INPUT;
    }
    
    status = sci_init(&vehiclecommtype, &vehiclecommlevel);
    if (status == S_SUCCESS)
    {   
        /* Main OBD2 Info (read only if primary ECU) */
        if (obd2info->ecu_count == 0)
        {
            obd2info->oemtype = OemType_DCX;
            obd2sci_readvin(vin,vehiclecommtype,vehiclecommlevel);
            memcpy(obd2info->vin, vin, sizeof(vin));
        }
        
        /* OBD2 Info Block */
        obd2info->ecu_block[obd2info->ecu_count].ecutype = EcuType_ECM;
        obd2info->ecu_block[obd2info->ecu_count].commtype = vehiclecommtype;
        obd2info->ecu_block[obd2info->ecu_count].commlevel = vehiclecommlevel;
        obd2info->ecu_block[obd2info->ecu_count].genericcommlevel = GenericCommLevel_Legacy;
        /* block.ecu_id <- N/A */
                
        /* OBD2 Part Numbers*/
        obd2sci_getpartnumber(&obd2info->ecu_block[obd2info->ecu_count]);
        
        obd2info->ecu_count++;
    }
    return status;
}

//------------------------------------------------------------------------------
// Read SCI comm type. (SCI A or SCI B)
// Input:   VehicleCommType *vehiclecommtype
// Return:  u8  status
// Engineer: Ricardo Cigarroa
//------------------------------------------------------------------------------
u8 obd2sci_get_sci_commtype_commlevel(VehicleCommType *vehiclecommtype, 
                                      VehicleCommLevel *vehiclecommlevel)
{
    u8 status;
    Obd2sci_ServiceData servicedata;
    
    obd2sci_servicedata_init(&servicedata);
    servicedata.service = 0x22;
    servicedata.service_controldatabuffer[0] = 0x01; 
    servicedata.service_controldatabuffer[1] = 0x00; 
    servicedata.service_controldatalength = 2; 
    servicedata.txflags = sci_rx_flags_has_check_crc; 
    servicedata.rxflags = sci_rx_flags_has_check_crc | sci_rx_flags_skip80;
    
    status = obd2sci_read_part_type(vehiclecommlevel);
    /* If comm type is SCI A */
    if(status == S_SUCCESS)
    {
        switch(*vehiclecommlevel)
        {
            case CommLevel_SCIA:
            case CommLevel_SBECA:
                *vehiclecommtype = CommType_SCIA;
                break;
            case CommLevel_SCIB:
            case CommLevel_SBECB:
                *vehiclecommtype = CommType_SCIB;
                break;
            default:
                status = S_INPUT; 
                break;
        }
    }
    /* Otherwise check for comm type SCI B (non-SBEC processors) */
    else
    {
        status = obd2sci_txrx(&servicedata);
        if(status == S_SUCCESS)
        {
            *vehiclecommtype = CommType_SCIB; 
            *vehiclecommlevel = CommLevel_SCIB;
        }
    }
    
    return status; 
}

//------------------------------------------------------------------------------
// Read part type (ex: JTEC, SBEC3)
// Note: We read the part type only to find out which processor type we are 
// working with. Once we know that, we can assume a comm level. 
// Input:   u8 *vehicleparttype
// Return:  u8  status
// Engineer: Ricardo Cigarroa
//------------------------------------------------------------------------------
u8 obd2sci_read_part_type(VehicleCommLevel *vehiclecommlevel)
{
    u8 status; 
    Obd2sci_ServiceData servicedata;
    
    obd2sci_servicedata_init(&servicedata);
    servicedata.service = 0x2A;
    servicedata.rx_timeout_ms = 250;
  
    status = obd2sci_txrx(&servicedata);    
    if(status == S_SUCCESS)
    {
        servicedata.service = 0x0F;
        servicedata.rxflags = 1; 
        status = obd2sci_txrx(&servicedata);
        if(status == S_SUCCESS)
        {
            switch(servicedata.rxbuffer[0])
            {
                case 0x13:
                     *vehiclecommlevel = CommLevel_SBECB;    //SBEC3A processor
                     break; 
                case 0x08:
                     *vehiclecommlevel = CommLevel_SBECA;   //SBEC3 processor
                     break; 
                case 0x0C:                                 //JTEC processor     
                case 0x06:                                 //JTEC plus processor
                     *vehiclecommlevel = CommLevel_SCIA;   
                     break;
                default:
                     break; 
            }
        }
        else
        {
            status = S_FAIL; 
        }
    }
    else
    {
        status = S_FAIL; 
    }
    
    return status; 
}

//------------------------------------------------------------------------------
// Clear DTCs
// Input:   VehicleCommType commtype
// Return:  u8  status
// Engineer: Ricardo Cigarroa
//------------------------------------------------------------------------------
u8 obd2sci_cleardtc(VehicleCommType commtype)
{
    u8 status; 
    VehicleCommLevel commlevel;
    
    status = sci_init(&commtype, &commlevel);
    if(status == S_SUCCESS)
    {
        switch(commtype)
        {
            case CommType_SCIA:
                 status = obd2sci_cleardtc_scia();
                 break;
            case CommType_SCIB:
                 status = obd2sci_cleardtc_scib();
                 break;
            default: 
                 status = S_INPUT;
                 break;
        }
    }
    else
    {
        status = S_FAIL; 
    }

    return status; 
}

//------------------------------------------------------------------------------
// Read DTCs
// Input:   dtc_info, *info, VehicleCommType commtype
// Return:  u8  status
// Engineer: Ricardo Cigarroa
//------------------------------------------------------------------------------
u8 obd2sci_readdtc(dtc_info *info, VehicleCommType commtype)
{
    u8 status; 
    VehicleCommLevel commlevel; 
     
    status = sci_init(&commtype, &commlevel);      
    switch(commtype)
    {
        case CommType_SCIA:
             status = obd2sci_readdtc_scia(info,commtype);
             break;
        case CommType_SCIB:
             status = obd2sci_readdtc_scib(info,commtype);
             break;
        default: 
             status = S_INPUT;
             break;
    }
    
    return status;  
}

//------------------------------------------------------------------------------
// SCI read part number (SCIA comm level)
// Input:   obd2_info_block *block
// Return:   u8  status
// Engineer: Ricardo Cigarroa
//------------------------------------------------------------------------------
u8 obd2sci_getpartnumber_scia(obd2_info_block *block)
{
    u8  i; 
    u8  status; 
    u8  partnumber_index;
    u8  hexstr[8]; 
    u8  buffer[8];
    u8  partnumber_temp[10];
    Obd2sci_ServiceData servicedata;
    
    
    obd2sci_servicedata_init(&servicedata);
    servicedata.service = 0x2A;
    servicedata.rxflags = sci_rx_flags_skip80;
    servicedata.rx_timeout_ms = 250;
    
    memset((char*)hexstr,0,sizeof(hexstr));
    memset((char*)buffer,0,sizeof(buffer));
    
    status = sci_init(&block->commtype, &block->commlevel);
    if(status == S_SUCCESS)
    {      
        block->partnumber_count = 1;
        partnumber_index = 0; 
        for(i=0; i<4; i++)
        {
            servicedata.service = 0x2A;
            status = obd2sci_txrx(&servicedata);
            if(status == S_SUCCESS)
            {
                /*Send request*/
                servicedata.service = 0x01 + i;
                status = obd2sci_txrx(&servicedata); 
                if(status != S_SUCCESS)
                {
                    status = S_FAIL;
                }
                else
                {
                    buffer[i] = servicedata.rxbuffer[0];
                }
            }
        }
        
        /*Store hex values (actual part number)*/
        memcpy(hexstr, &buffer[0], 20);
        for (i = 0; i < 4; i++)
        {
            char temp[3];
            sprintf(temp, "%02X", hexstr[i]);
            memcpy((void*)&partnumber_temp[partnumber_index],temp,2);
            partnumber_index += 2; 
        }
                
        servicedata.service = 0x2A;
        status = obd2sci_txrx(&servicedata); 
        if(status == S_SUCCESS)
        {
            if(status == S_SUCCESS)
            {          
                servicedata.service = 0x17;  
                status = obd2sci_txrx(&servicedata); 
                if(status == S_SUCCESS)
                {                
                    memcpy((void*)&partnumber_temp[partnumber_index],servicedata.rxbuffer,1);
                    partnumber_index++;
                                 
                    servicedata.service = 0x2A;
                    status = obd2sci_txrx(&servicedata);
                    if(status == S_SUCCESS)
                    {
                        servicedata.service = 0x18; 
                        status = obd2sci_txrx(&servicedata);
                        if(status == S_SUCCESS)
                        {
                            memcpy((void*)&partnumber_temp[partnumber_index],servicedata.rxbuffer,1);
                        }
                        else
                        {
                            status = S_FAIL; 
                        }
                    }
                }
                else
                {
                    status = S_FAIL; 
                }
            }
            else
            {
                status = S_FAIL; 
            }
        }
        else
        {
            status = S_FAIL;
        }       
        
        /* Store part number */
       strcpy((char*)block->partnumbers.dcx.part_number,
              (char*)partnumber_temp);
    }
    else
    {
      status = S_FAIL;
    }
    
    return status;
}

//------------------------------------------------------------------------------
// SCI read part number (SCIB comm level)
// Input:   obd2_info_block *block
// Return:   u8  status
// Engineer: Ricardo Cigarroa
//------------------------------------------------------------------------------
u8 obd2sci_getpartnumber_scib(obd2_info_block *block)
{
    u8  i; 
    u8  status; 
    u8  partnumber_index;
    u8  hexstr[8]; 
    u8  buffer[8];
    u8  partnumber_temp[10];
    u8  partnumber[10];
    Obd2sci_ServiceData servicedata;
        
    obd2sci_servicedata_init(&servicedata);
    servicedata.service = 0x22;
    servicedata.subservice = 0x20;
    servicedata.service_controldatabuffer[0] = 0x00;
    servicedata.service_controldatalength = 1;
    servicedata.txflags = sci_tx_flags_has_check_crc;
    servicedata.rxflags = sci_rx_flags_has_check_crc | sci_rx_flags_skip80;
    
    memset((char*)hexstr,0,sizeof(hexstr));
    memset((char*)buffer,0,sizeof(buffer));
    memset((char*)partnumber,0,sizeof(partnumber)); 
    
    status = sci_init(&block->commtype, &block->commlevel);
    if(status == S_SUCCESS)
    {      
        block->partnumber_count = 1;
        partnumber_index = 0; 
        for(i=0; i<3; i++)
        {
            /*Send request*/
            status = obd2sci_txrx(&servicedata); 
            if(status != S_SUCCESS)
            {
                status = S_FAIL;
            }
            else
            {
                servicedata.service_controldatabuffer[0]++; 
                buffer[i*2] = servicedata.rxbuffer[0];
                buffer[(i*2)+1] = servicedata.rxbuffer[1]; 
            }
        }
        
        /*Store hex values (actual part number)*/
        memcpy(hexstr, &buffer[0], 20);
        for (i = 0; i < 5; i++)
        {
            if(i < 4)
            {
                char temp[3];
                sprintf(temp, "%02X", hexstr[i]);
                memcpy((void*)&partnumber_temp[partnumber_index],temp,2);
                partnumber_index += 2; 
            }
            else
            {
                partnumber_temp[partnumber_index] = hexstr[i];
                i++;
                partnumber_index++;
                partnumber_temp[partnumber_index] = hexstr[i]; 
            }
        }
                        
        /* Store part number */
       memcpy((void*)&partnumber[0],partnumber_temp,sizeof(partnumber));
       strcpy((char*)block->partnumbers.dcx.part_number,
              (char*)partnumber);
    }
    else
    {
        status = S_FAIL;
    }
    
    return status;
}

//------------------------------------------------------------------------------
// Read VIN (comm level SCIA)
// Inputs:   u8  *vin
//          
// Return:   u8  status
// Engineer: Ricardo Cigarroa
//------------------------------------------------------------------------------
u8 obd2sci_readvin_scia(u8 *vin, VehicleCommType commtype, VehicleCommLevel commlevel)
{
    u8  status;
    u8  temp[20];
    u8  i = 0;
    Obd2sci_ServiceData servicedata;
    
    if (!vin)
    {
        return S_INPUT;
    }
    
    memset(vin,0x00,17);
     
    obd2sci_servicedata_init(&servicedata);
    servicedata.service = 0x28;
    servicedata.service_controldatabuffer[0] = 0x00;
    servicedata.service_controldatabuffer[1] = 0x62;
    servicedata.service_controldatalength = 2;  
       
    status = sci_init(&commtype, &commlevel);
    if(status == S_SUCCESS)
    {
        // Receive VIN 
        for (i = 0; i < VIN_LENGTH; i++)
        {
            status = obd2sci_txrx(&servicedata);
            if (status == S_SUCCESS)
            {                   
                servicedata.service_controldatabuffer[1]++; 
                memcpy(&temp[i],&servicedata.rxbuffer[0],1);
            }
            else
            {
                /* If VIN cannot be read, treat it as blank */
                status = S_VINBLANK; //add break after this 
            }
        }
        
        // Remove fill bytes and validate VIN
        if (status == S_SUCCESS)
        {
            memcpy(vin,&temp[0],17);
            status = obd2_validatevin(vin);
        }
    }
    else
    {
        status = S_FAIL; 
    }

    return status;
}

//------------------------------------------------------------------------------
// Read VIN (comm level SCIB)
// Inputs:   u8  *vin
//          
// Return:   u8  status
// Engineer: Ricardo Cigarroa
//------------------------------------------------------------------------------
u8 obd2sci_readvin_scib(u8 *vin, VehicleCommType commtype, VehicleCommLevel commlevel)
{
    u8  status;
    u8  temp[20];
    u8  i = 0;
    u8  vinindex = 0; 
    u8  retry = 0; 
    Obd2sci_ServiceData servicedata;
    
    if (!vin)
    {
        return S_INPUT;
    }
    
    memset(vin,0x00,17);
     
    obd2sci_servicedata_init(&servicedata);
    servicedata.service = 0x22;
    servicedata.service_controldatabuffer[0] = 0x2A;
    servicedata.service_controldatabuffer[1] = 0x00;
    servicedata.service_controldatalength = 2; 
    servicedata.txflags = sci_tx_flags_has_check_crc;
    servicedata.rxflags = sci_rx_flags_has_check_crc;
    servicedata.rx_timeout_ms = 250;
       
    status = sci_init(&commtype, &commlevel);
    if(status == S_SUCCESS)
    {
        // Receive VIN 
        for (i = 0; i < 7; i++)
        {
            status = obd2sci_txrx(&servicedata);
            if (status == S_SUCCESS)
            {                   
                servicedata.service_controldatabuffer[1]++; 
                temp[vinindex++] = servicedata.rxbuffer[0];              
                temp[vinindex++] = servicedata.rxbuffer[1];
                if(i > 3)                                   
                {   
                    temp[vinindex++] = servicedata.rxbuffer[2]; 
                }
            }
            else
            {
                i--;
                delays(100,'m');
                retry++;
                if(retry > 7);
                {
                    /* If VIN cannot be read, treat it as blank */
                    status = S_VINBLANK; //add break after this 
                    break;
                }
            }
        }
        
        // Remove fill bytes and validate VIN
        if (status == S_SUCCESS)
        {
            memcpy(vin,&temp[0],17);
            status = obd2_validatevin(vin);
        }
    }
    else
    {
        status = S_FAIL; 
    }

    return status;
}

//------------------------------------------------------------------------------
// Read VIN (comm level SBECA and SBECB)
// Inputs:   u8  *vin
//          
// Return:   u8  status
// Engineer: Ricardo Cigarroa
//------------------------------------------------------------------------------
u8 obd2sci_readvin_sbec(u8 *vin, VehicleCommType commtype, VehicleCommLevel commlevel)
{
    u8  status;
    u8  temp[20];
    u8  i = 0;
    Obd2sci_ServiceData servicedata;
    
    if (!vin)
    {
        return S_INPUT;
    }
    
    memset(vin,0x00,17);
    
    obd2sci_servicedata_init(&servicedata);
       
    status = sci_init(&commtype, &commlevel);
    if(status == S_SUCCESS)
    {
        // Receive VIN 
        for (i = 0; i < VIN_LENGTH; i++)
        {
            servicedata.service = 0x28;
            
            status = obd2sci_txrx(&servicedata);
            if (status == S_SUCCESS)
            {     
                servicedata.service = 0x00;
                
                status = obd2sci_txrx(&servicedata);
                if(status == S_SUCCESS)
                {
                    servicedata.service = 0x62 + i;
                    
                    status = obd2sci_txrx(&servicedata);
                    if(status == S_SUCCESS)
                    {
                         memcpy(&temp[i],&servicedata.rxbuffer[0],1);
                    }
                    else
                    {
                        status = S_FAIL;
                        break;
                    }
                }
                else
                {
                    status = S_FAIL;
                    break; 
                }
            }
            else
            {
                /* If VIN cannot be read, treat it as blank */
                status = S_VINBLANK; 
            }
        }
        
        // Remove fill bytes and validate VIN
        if (status == S_SUCCESS)
        {
            memcpy(vin,&temp[0],17);
            status = obd2_validatevin(vin);
        }
    }
    else
    {
        status = S_FAIL; 
    }

    return status;
}

//------------------------------------------------------------------------------
// Clear DTCs (SCI A comm type) 
// Input:   None
// Return:  u8  status
// Engineer: Ricardo Cigarroa
//------------------------------------------------------------------------------
u8 obd2sci_cleardtc_scia()
{
    u8 status; 
    Obd2sci_ServiceData servicedata;
    
    obd2sci_servicedata_init(&servicedata);
    servicedata.service = 0x17;
        
    status = obd2sci_txrx(&servicedata);
    if(status == S_SUCCESS)
    {
        if(servicedata.rxbuffer[0] == 0xE0)
        {
            status = S_SUCCESS;
        }
        else
        {
            status = S_FAIL; 
        }
    }
    else
    {
        /*Attempt clearing dtcs via secondary commtype*/
        if(gObd2info.ecu_block[1].commtype != CommType_Unknown)
        {
            switch(gObd2info.ecu_block[1].commtype)
            {
                case CommType_KLINE:
                     status = obd2iso_cleardtc(CommType_Unknown);
                     break;
                default:
                     status = S_INPUT;
                     break;
            }
        }
    }
    
    return status; 
}

//------------------------------------------------------------------------------
// Clear DTCs (SCI B comm type) 
// Input:   None
// Return:  u8  status
// Engineer: Ricardo Cigarroa
//------------------------------------------------------------------------------
u8 obd2sci_cleardtc_scib()
{
    u8 status; 
    Obd2sci_ServiceData servicedata;
    
    obd2sci_servicedata_init(&servicedata);
    servicedata.service = 0x14;
    servicedata.service_controldatabuffer[0] = 0x01;
    servicedata.service_controldatalength = 1; 
    servicedata.txflags = sci_tx_flags_has_check_crc;
    servicedata.rxflags = sci_rx_flags_skip80 | sci_rx_flags_has_check_crc;
    
    delays(500,'m');
    status = obd2sci_txrx(&servicedata);
    if(status == S_SUCCESS)
    {
        if(servicedata.rxbuffer[0] == 0x55)
        {
            status = S_SUCCESS;
        }
        else
        {
            status = S_FAIL; 
        }
    }
    else
    {
        delays(250,'m');
        status = obd2sci_cleardtc_scia(); 
    }
    
    return status; 
}

//------------------------------------------------------------------------------
// Read DTCs (SCI A comm type) 
// Input:   None
// Return:  u8  status
// Engineer: Ricardo Cigarroa
//------------------------------------------------------------------------------
u8 obd2sci_readdtc_scia(dtc_info *info, VehicleCommType commtype)
{
    u8 status; 
    u8 dtccount;
    u8 codeindex; 
    u8 i; 
    VehicleCommLevel commlevel; 
    Obd2sci_ServiceData servicedata;
    
    obd2sci_servicedata_init(&servicedata);
    servicedata.service = 0x32;
    servicedata.rxflags = sci_rx_flags_has_check_crc; 
    /*Max read dtc response length*/
    servicedata.expected_rxbufferlength = 50; 
    
    info->count = 0; 
    
    status = sci_init(&commtype, &commlevel);
    if(status == S_SUCCESS)
    {
        dtccount = 0; 
        codeindex = 0; 
        
        status = obd2sci_txrx(&servicedata);
        if(status == S_SUCCESS)
        {
            if(servicedata.rxlength != 0)
            {
                for(i=0; i<servicedata.rxlength; i++)
                {
                    info->codes[codeindex] = servicedata.rxbuffer[i*2] <<8;
                    info->codes[codeindex] |= servicedata.rxbuffer[(i*2)+1];
                    
                    if(info->codes[codeindex] == 0xFEFE)
                    {
                        /*These are the bytes (0xFE, 0xFE) returned by the processor, 
                        which indicates the end of the read dtc response*/
                        break; 
                    }
                    dtccount++; 
                    codeindex++;
                }
                info->count = dtccount; 
            }
        }
        else
        {
            /*Attempt reading dtcs via secondary commtype*/
            if(gObd2info.ecu_block[1].commtype != CommType_Unknown)
            {
                switch(gObd2info.ecu_block[1].commtype)
                {
                    case CommType_KLINE:
                         status = obd2iso_readdtc(info,commtype);
                         break;
                    default:
                         status = S_INPUT; 
                         break;
                }
                if(status != S_SUCCESS)
                {
                    status = S_FAIL; 
                }
            }
            else
            {
                status = S_FAIL; 
            }
        }
    }
    else
    {
        status = S_FAIL;
    }
     
    return status;
}

//------------------------------------------------------------------------------
// Read DTCs (SCI B comm type) 
// Input:   None
// Return:  u8  status
// Engineer: Ricardo Cigarroa
//------------------------------------------------------------------------------
u8 obd2sci_readdtc_scib(dtc_info *info, VehicleCommType commtype)
{
    u8 status; 
    u8 dtccount;
    u8 codeindex; 
    u8 i; 
    VehicleCommLevel commlevel; 
    Obd2sci_ServiceData servicedata;
    
    obd2sci_servicedata_init(&servicedata);
    servicedata.service = 0x18;
    servicedata.subservice = 0x10; 
    servicedata.service_controldatabuffer[0] = 0x00; 
    servicedata.service_controldatalength = 1; 
    servicedata.txflags = sci_tx_flags_has_check_crc;
    servicedata.rxflags = sci_rx_flags_has_check_crc; 
    /*Max read dtc response length*/
    servicedata.expected_rxbufferlength = 50; 
    
    info->count = 0; 
    
    status = sci_init(&commtype, &commlevel); 
    if(status == S_SUCCESS)
    {
        dtccount = 0; 
        codeindex = 0; 
        
        if(gObd2info.ecu_block[0].commlevel == CommLevel_SCIB)
        {
            status = obd2sci_txrx(&servicedata);
            if(status == S_SUCCESS)
            {
                dtccount = servicedata.rxbuffer[1]; 
                info->count = dtccount; 
                
                for(i=0; i<dtccount; i++)
                {
                    info->codes[codeindex] = servicedata.rxbuffer[(i*2)+2] <<8;    
                    info->codes[codeindex] |= servicedata.rxbuffer[(i*2)+3];     
                         
                    codeindex++; 
                }
            }
            else
            {
                /*Attempt reading dtc via secondary commtype*/
                if(gObd2info.ecu_block[1].commtype != CommType_Unknown)
                {
                    switch(gObd2info.ecu_block[1].commtype)
                    {
                    case CommType_CAN:
                        /* Clear with generic command (J1979) */
                        status = obd2can_generic_readdtc(Obd2CanEcuId_7E0,info);
                        break;
                    case CommType_VPW:
                        status = obd2vpw_readdtc(info); 
                        break; 
                    default:
                         status = S_INPUT;
                         break;
                    }
                }
                else
                {
                    status = S_FAIL; 
                }
            }
        }
        else
        {
            status = obd2sci_readdtc_scia(info,commtype);
        }
    }
    else
    {
        status = S_FAIL;
    }
      
    return status; 
}