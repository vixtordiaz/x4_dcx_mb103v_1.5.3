/**
  **************** (C) COPYRIGHT 2011 SCT Performance, LLC *********************
  * File Name          : usbcmdhandler_datalog.c
  * Device / System    : SCT products using the "CommonCode" platform
  * Author             : Quyen Leba
  *
  * Version            : 1 
  * Date               : 05/02/2011
  * Description        : 
  *                    : 
  *
  *
  * History            : 05/02/2011 M. Davis
  *                    :   Add SCT Copyright header,
  *
  ******************************************************************************
  */

//------------------------------------------------------------------------------
// Log error point: 0xB200 -> 0xB2FF; shared with usbcmdhandler.c
//------------------------------------------------------------------------------

#include <string.h>
#include <arch/gentype.h>
#include <board/indicator.h>
#include <board/rtc.h>
#include <common/obd2def.h>
#include <common/obd2.h>
#include <common/genmanuf_overload.h>
#include <common/obd2datalog.h>
#include <common/file.h>
#include <common/statuscode.h>
#include "usb_callback.h"
#include "usbcmdhandler.h"
#include "usbcmdhandler_tune.h"
#include "usbcmdhandler_datalog.h"
#include <common/cmdif.h>
#include "cmdif_function.h"

extern cmdif_datainfo cmdif_datainfo_responsedata;      /* from cmdif.c */
extern Datalog_Mode gDatalogmode;                       /* from obd2datalog.c */
extern Datalog_Info *dataloginfo;                       /* from obd2datalog.c */
extern USB_CALLBACK_INFO usb_callback_info;             /* from usb_callback.c */
extern u8  *usb_databuffer;
extern u32 usb_databufferindex;
extern u32 usb_databufferlength;

#define DATAREPORT_MIN_INTERVAL             4
#define DATAREPORT_MAX_INTERVAL             200

#define MAX_READDTC_DATALINK_BUFFER_LEN     2048

enum PassthroughDatalogState
{
    PassthroughDatalogState_Idle            = 0x0020,
//    PassthroughDatalogState_Scanning        = 0x0030,
//    PassthroughDatalogState_ScanDone        = 0x0031,
//    PassthroughDatalogState_Datalogging     = 0x0050,
    PassthroughDatalogState_CommLost        = 0x0051,
    PassthroughDatalogState_CommActive      = 0x0055,
//    PassthroughDatalogState_Paused          = 0x0052,
//    PassthroughDatalogState_KeyOff          = 0x0053,
    PassthroughDatalogState_StartingUp      = 0x0054,
};

typedef struct
{
    u8  version;
    u8  oemtype;
    u8  pcm_commtype;
    u8  pcm_commlevel;
    u8  tcm_commtype;
    u8  tcm_commlevel;
    u8  reserved1[42];
    u8  vin[VIN_LENGTH+1];
    u8  reserved2[62];
    u8  vehicle_codes[128];
    u8  reserved3[256];
}PassthroughDatalogVehicleInfo;
STRUCT_SIZE_CHECK(PassthroughDatalogVehicleInfo,512);

typedef struct
{
    u8  pcm_commtype;
    u8  pcm_commlevel;
    u8  tcm_commtype;
    u8  tcm_commlevel;
    u8  reserved[12];
}PassthroughDatalogVehicleCommInfo;

typedef struct
{
    u32 status;
    u8  percentage;
    u8  reserved[11];
}PassthroughDatalogStatus;

UsbCmdHandler_Datalog_Info usbcmdhandler_datalog_info = 
{
    .state = PassthroughDatalogState_Idle,
    .percentage = 0,
    .control.busy = 0,
    .control.critical = 0,
    .control.stop = 0,
    .flags = 0,
};

//------------------------------------------------------------------------------
// Prototypes
//------------------------------------------------------------------------------
u8 uch_datalog_getvehicleinfo(PassthroughDatalogVehicleInfo *info);
u8 uch_datalog_getvehiclecomminfo(PassthroughDatalogVehicleCommInfo *comm_info);
u8 uch_datalog_setvehiclecomm(PassthroughDatalogVehicleCommInfo *info);
u8 uch_datalog_getstatus(PassthroughDatalogStatus *datalog_status);
u8 uch_datalog_getpacket();
u8 uch_datalog_start_test_dlx_file(u8 *test_dlx_filename);
u8 uch_datalog_get_test_dlx_file_result(u16 chunk_index,
                                        u8 *chunk_data, u32 *chunk_datalength);
u8 uch_datalog_update_signal_osc(u8 *data, u16 count);
void uch_datalog_execute(UsbCmdHandler_DatalogExecuteCode execute_code, u16 ecm_index,
                         VehicleCommType commtype, VehicleCommLevel commlevel);
void uch_datalog_dtc(UsbCmdHandler_DatalogDTC execute_code);
u8 uch_datalog_get_dtc(u8 *dataptr, u32 *length);
u8 uch_datalog_scan_dlx_file(u8 *source_dlx_filename, u8 *scanned_dlx_filename);
u8 uch_datalog_scan_dlx_block(DlxBlock *dlxblock);
u8 uch_datalog_start(u8 attrib, u8 speed, u16 flags, u32 signature, u8 *dlx_filename);
u8 uch_datalog_stop();
void uch_datalog_start_trigger();
u8 uch_datalog_setdatareportinterval(u16 interval);
u8 uch_datalog_getfeatures(u8 *filename);

//------------------------------------------------------------------------------
// Handle passthrough datalog command
// Engineer: Quyen Leba
//------------------------------------------------------------------------------
void uch_datalog()
{
    u32 temp_u32;
    u8  status;

    status = S_FAIL;

    if (uch_tune_isbusy())
    {
        //not allowed while passthrough flashing is working
        usb_set_csw(usb_callback_info.pdev,S_REJECT,TRUE);
        return;
    }

    usbcmdhandler_datalog_info.control.busy = 1;

    switch((USBCmdDatalogFlags)usb_callback_info.cbw.flags)
    {
    //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    case USBCmdDatalog_GetVehicleInfo:
        status = uch_datalog_getvehicleinfo((PassthroughDatalogVehicleInfo*)usb_databuffer);
        SCTUSB_SetCmdStatus(status);
        if (status == S_SUCCESS)
        {
            usb_databufferlength = sizeof(PassthroughDatalogVehicleInfo);
            usb_trigger_send_databuffer_to_host();
        }
        else
        {
            log_push_error_point(0xB200);
            usb_set_csw(usb_callback_info.pdev,status,TRUE);
        }
        break;
    case USBCmdDatalog_GetVehicleCommInfo:               
        status = uch_datalog_getvehiclecomminfo((PassthroughDatalogVehicleCommInfo*)usb_databuffer);
        SCTUSB_SetCmdStatus(status);
        if (status == S_SUCCESS)
        {
            usb_databufferlength = sizeof(PassthroughDatalogVehicleCommInfo);
            usb_trigger_send_databuffer_to_host();
        }
        else
        {
            log_push_error_point(0xB201);
            usb_set_csw(usb_callback_info.pdev,status,TRUE);
        }
        break;
    case USBCmdDatalog_GetFeatures:
        status = uch_datalog_getfeatures(usb_databuffer);
        SCTUSB_SetCmdStatus(status);
        if (status == S_SUCCESS)
        {
            usb_databufferlength = 64;
            usb_trigger_send_databuffer_to_host();
        }
        else
        {
            log_push_error_point(0xB20B);
            usb_set_csw(usb_callback_info.pdev,status,TRUE);
        }
        break;
    case USBCmdDatalog_GetStatus:
        status = uch_datalog_getstatus((PassthroughDatalogStatus*)usb_databuffer);
        SCTUSB_SetCmdStatus(status);
        if (status == S_SUCCESS)
        {
            usb_databufferlength = sizeof(PassthroughDatalogStatus);
            usb_trigger_send_databuffer_to_host();
        }
        else
        {
            log_push_error_point(0xB202);
            usb_set_csw(usb_callback_info.pdev,status,TRUE);
        }
        break;
    case USBCmdDatalog_GetPacket:
        status = uch_datalog_getpacket();
        if (status != S_SUCCESS)
        {
            log_push_error_point(0xB203);
        }
        usb_set_csw(usb_callback_info.pdev,status,TRUE);
        break;
    case USBCmdDatalog_Stop:
        status = uch_datalog_stop();
        if (status != S_SUCCESS)
        {
            log_push_error_point(0xB204);
        }
        usb_set_csw(usb_callback_info.pdev,status,TRUE);
        break;
    case USBCmdDatalog_SetDataReportInterval:
        status = uch_datalog_setdatareportinterval(usb_callback_info.cbw.privatedata[0]);
        if (status != S_SUCCESS)
        {
            log_push_error_point(0xB205);
        }
        usb_set_csw(usb_callback_info.pdev,status,TRUE);
        break;
    //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    case USBCmdDatalog_SetVehicleComm:
        //data: [PassthroughDatalogVehicleCommInfo]
        if (usb_databufferlength == sizeof(PassthroughDatalogVehicleCommInfo))
        {
            status = uch_datalog_setvehiclecomm((PassthroughDatalogVehicleCommInfo*)usb_databuffer);
        }
        else
        {
            log_push_error_point(0xB206);
            status = S_BADCONTENT;
        }
        break;
    case USBCmdDatalog_StartTestDLX:
        //data: [n:test_dlx_filename][NULL]
        if (usb_databufferlength == (strlen((char*)usb_databuffer)+1))
        {
            status = uch_datalog_start_test_dlx_file(usb_databuffer);
        }
        else
        {
            log_push_error_point(0xB20C);
            status = S_BADCONTENT;
        }
        usb_set_csw(usb_callback_info.pdev,status,TRUE);
        break;
    case USBCmdDatalog_GetTestResultDLX:
        //data: none
        status = uch_datalog_get_test_dlx_file_result(usb_callback_info.cbw.privatedata[0],
                                                      usb_databuffer,&usb_databufferlength);
        SCTUSB_SetCmdStatus(status);
        if (status == S_SUCCESS)
        {
            usb_trigger_send_databuffer_to_host();
        }
        else
        {
            log_push_error_point(0xB20D);
            usb_set_csw(usb_callback_info.pdev,status,TRUE);
        }
        break;
    case USBCmdDatalog_Execute:        
        //privatedata: [2:execute code][2:ecm_index][1:commtype][1:commlevel]                   
        uch_datalog_execute((UsbCmdHandler_DatalogExecuteCode)usb_callback_info.cbw.privatedata[0], // execute_code
                            usb_callback_info.cbw.privatedata[1],                                   // ecm_index
                            (VehicleCommType)((usb_callback_info.cbw.privatedata[2] >> 8) & 0xFF),  // commtype
                            (VehicleCommLevel)(usb_callback_info.cbw.privatedata[2] & 0xFF));       // commlevel
                  
        break;
    case USBCmdDatalog_ScanDLX:
        //data: [n:source_dlx_filename][NULL][m:scanned_dlx_filename][NULL]
        temp_u32 = strlen((char*)usb_databuffer);
        if (usb_databufferlength == (strlen((char*)usb_databuffer)+1+strlen((char*)&usb_databuffer[temp_u32])+1))
        {
            status = uch_datalog_scan_dlx_file(usb_databuffer,
                                               &usb_databuffer[temp_u32+1]);
        }
        else
        {
            log_push_error_point(0xB207);
            status = S_BADCONTENT;
        }
        break;
    case USBCmdDatalog_ScanSingle:
        //data: [n: DlxBlock]
        if (usb_databufferlength == sizeof(DlxBlock))
        {
            status = uch_datalog_scan_dlx_block((DlxBlock*)usb_databuffer);
        }
        else
        {
            log_push_error_point(0xB208);
            status = S_BADCONTENT;
        }
        usb_set_csw(usb_callback_info.pdev,status,TRUE);
        break;
    case USBCmdDatalog_Start:
        //data: [1:attrib][1:speed][2:flags][4:signature][n:dlx filename][NULL]
        if (usb_databufferlength == (1+1+2+4+strlen((char*)&usb_databuffer[8])+1))
        {
            status = uch_datalog_start(usb_databuffer[0],usb_databuffer[1],
                                       *(u16*)&usb_databuffer[2],
                                       *(u32*)&usb_databuffer[4],
                                       &usb_databuffer[8]);
        }
        //data: [1:attrib][1:speed][2:flags][n:dlx filename][NULL]
        else if (usb_databufferlength == (1+1+2+strlen((char*)&usb_databuffer[4])+1))
        {
            status = uch_datalog_start(usb_databuffer[0],usb_databuffer[1],
                                       *(u16*)&usb_databuffer[2],
                                       0xAA557271,
                                       &usb_databuffer[4]);
        }
        else
        {
            log_push_error_point(0xB209);
            status = S_BADCONTENT;
        }
        usb_set_csw(usb_callback_info.pdev,status,TRUE);
        break;    
    case USBCmdDatalog_DTC:
        //privatedata: [2:execute code][4:reserved]
        uch_datalog_dtc((UsbCmdHandler_DatalogDTC) usb_callback_info.cbw.privatedata[0]);
        break;
    case USBCmdDatalog_SendSignalOSC:
        //data: [2:count=n][2:index 1][4:data 1][1:status 1]...[2:index n][4:data n][1:status n]
        if (usb_databufferlength == *(u16*)&usb_databuffer[0]*7+2)
        {
            status = uch_datalog_update_signal_osc(&usb_databuffer[2],*(u16*)&usb_databuffer[0]);
        }
        else
        {
            log_push_error_point(0xB217);
            status = S_FAIL;
        }
        usb_set_csw(usb_callback_info.pdev,status,TRUE);
        break;
    //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    default:
        //should never happen (because already checked usb_callback.c)
        log_push_error_point(0xB20A);
        usb_set_csw(usb_callback_info.pdev,S_USB_UNKNOWN_FLAGS,TRUE);
        break;
    }
    usbcmdhandler_datalog_info.control.busy = 0;
}

/**
 * @brief   Check if passthrough datalogging session is busy
 *
 * @retval  bool TRUE: busy
 *
 * @author  Quyen Leba
 *
 */
bool uch_datalog_isbusy()
{
    return (bool)(usbcmdhandler_datalog_info.control.busy || usbcmdhandler_datalog_info.control.critical);
}

//------------------------------------------------------------------------------
// Get vehicle info
// Output:  PassthroughDatalogVehicleInfo *info
// Engineer: Quyen Leba
//------------------------------------------------------------------------------
u8 uch_datalog_getvehicleinfo(PassthroughDatalogVehicleInfo *info)
{
    vehicle_info vehicleinfo;
    u8  status;

    usb_clear_alt_data();
    usbcmdhandler_datalog_info.max_data_report_frame_length = 0;

    vehicleinfo.commtype = CommType_Unknown;
    status = obd2_readecminfo(&vehicleinfo.ecm,&vehicleinfo.commtype);
    if (status == S_SUCCESS)
    {
        memset((char*)info,0,sizeof(PassthroughDatalogVehicleInfo));
        info->version = 0;
        info->oemtype = OEM_TYPE;
        memcpy((char*)info->vin,(char*)vehicleinfo.ecm.vin,sizeof(info->vin));

        info->pcm_commtype = vehicleinfo.ecm.commtype[0];
        info->pcm_commlevel = vehicleinfo.ecm.commlevel[0];
        if (vehicleinfo.ecm.ecm_count > 1)
        {
            info->tcm_commtype = vehicleinfo.ecm.commtype[1];
            info->tcm_commlevel = vehicleinfo.ecm.commlevel[1];
        }
        else
        {
            info->tcm_commtype = CommType_Unknown;
            info->tcm_commlevel = CommLevel_Unknown;
        }

        strcpy((char*)info->vehicle_codes,(char*)vehicleinfo.ecm.codes[0][0]);
        strcat((char*)info->vehicle_codes,TUNECODE_SEPARATOR_STRING);
        if (vehicleinfo.ecm.ecm_count > 1)
        {
            strcat((char*)info->vehicle_codes,(char*)vehicleinfo.ecm.codes[1][0]);
            strcat((char*)info->vehicle_codes,TUNECODE_SEPARATOR_STRING);
        }
    }
    else
    {
        log_push_error_point(0xB210);
    }
    return status;
}

//------------------------------------------------------------------------------
// Get vehicle comm info
// Output:  PassthroughDatalogVehicleCommInfo *comm_info
// Return:  u8  status
// Engineer: Quyen Leba
//------------------------------------------------------------------------------
u8 uch_datalog_getvehiclecomminfo(PassthroughDatalogVehicleCommInfo *comm_info)
{
    u8  status;

    comm_info->pcm_commtype = CommType_Unknown;
    comm_info->pcm_commlevel = CommLevel_Unknown;
    comm_info->tcm_commtype = CommType_Unknown;
    comm_info->tcm_commlevel = CommLevel_Unknown;
    status = obd2datalog_init(usbcmdhandler_datalog_info.commtype,
                              usbcmdhandler_datalog_info.commlevel);
    if (status == S_SUCCESS)
    {
        comm_info->pcm_commtype = (u8)usbcmdhandler_datalog_info.commtype[0];
        comm_info->pcm_commlevel = (u8)usbcmdhandler_datalog_info.commlevel[0];
        comm_info->tcm_commtype = (u8)usbcmdhandler_datalog_info.commtype[1];
        comm_info->tcm_commlevel = (u8)usbcmdhandler_datalog_info.commlevel[1];
    }
    else
    {
        log_push_error_point(0xB211);
    }
    return status;
}

//------------------------------------------------------------------------------
// Set vehicle comm
// Input:   PassthroughDatalogVehicleCommInfo *comm_info
// Return:  u8  status
// Engineer: Quyen Leba
//------------------------------------------------------------------------------
u8 uch_datalog_setvehiclecomm(PassthroughDatalogVehicleCommInfo *info)
{
    u8  i;

    for(i=0;i<ECM_MAX_COUNT;i++)
    {
        usbcmdhandler_datalog_info.commtype[i] = CommType_Unknown;
        usbcmdhandler_datalog_info.commlevel[i] = CommLevel_Unknown;
    }

    return obd2datalog_setcomm(usbcmdhandler_datalog_info.commtype,
                               usbcmdhandler_datalog_info.commlevel);
}

//------------------------------------------------------------------------------
// Get status
// Output:  u8  *datalog_status
// Engineer: Quyen Leba
//------------------------------------------------------------------------------
u8 uch_datalog_getstatus(PassthroughDatalogStatus *datalog_status)
{
    memset((char*)datalog_status,0,sizeof(PassthroughDatalogStatus));
    datalog_status->status = usbcmdhandler_datalog_info.state;
    datalog_status->percentage = usbcmdhandler_datalog_info.percentage;
    return S_SUCCESS;
}

//------------------------------------------------------------------------------
// Get packet
// Engineer: Quyen Leba
//------------------------------------------------------------------------------
u8 uch_datalog_getpacket()
{
    //TODOQ: push data to iso
    return S_SUCCESS;
}

//------------------------------------------------------------------------------
// Test a dlx file and generate result file with list of valid items
// Input:   u8  *test_dlx_filename
// Return:  u8  status
// Engineer: Quyen Leba
// Note: must do USBCmdDatalog_GetTestResultDLX right after to get accurate
// result
//------------------------------------------------------------------------------
u8 uch_datalog_start_test_dlx_file(u8 *test_dlx_filename)
{
    u16 index_count;
    u8  status;

    //temporary save result to usb_databuffer at [512]
    memset(&usb_databuffer[512],0,512);
    status = obd2datalog_evaluate_dlx_file_for_datalog_session
        (test_dlx_filename,(u16*)&usb_databuffer[512+2],&index_count);
    if (status == S_SUCCESS)
    {
        *(u16*)(&usb_databuffer[512]) = index_count;
    }
    return status;
}

//------------------------------------------------------------------------------
// Get result from uch_datalog_start_test_dlx_file(...)
// Input:   u16 chunk_index (each result chunk is 64 bytes)
// Outputs: u8  *chunk_data
//          u32 *chunk_datalength
// Return:  u8  status
// Engineer: Quyen Leba
//------------------------------------------------------------------------------
u8 uch_datalog_get_test_dlx_file_result(u16 chunk_index,
                                        u8 *chunk_data, u32 *chunk_datalength)
{
    *chunk_datalength = 0;
    if (chunk_index < 8)
    {
        memcpy(chunk_data,&usb_databuffer[512+chunk_index*64],64);
        *chunk_datalength = 64;
        return S_SUCCESS;
    }
    else
    {
        return S_INPUT;
    }
}

/**
 * @brief   Update OSC Signal
 *
 * @param   [in] *data
 * @param   [in] count
 *
 * @retval  u8 status
 *
 * @author  Tristen Pierson
 *
 * @details This function updates an OSC signal item from the USB IF. Data
 *          format is as follows: 
 *
 *          [2:index 1][4:data 1][1:status 1]...[2:index n][4:data n][1:status n]
 */
u8 uch_datalog_update_signal_osc(u8 *data, u16 count)
{
    return obd2_datalog_update_signal_osc(data, count);
}

/**
 * @brief   Execute datalog specific functions
 *
 * @param   [in] execute_code
 *
 * @retval  u8 status
 *
 * @author  Quyen Leba, Tristen Pierson
 *
 * @details This function handles multiple op codes (execute_code). This device
 *          will always return 32-bytes.
 *
 *          DatalogExecuteCode_PrevalidateTask returns: 
 *              [32: reserved]
 *
 *          DatalogExecuteCode_GetPacketInfo returns: 
 *              [1: packetmode] [1: packetcount] [1: packetsize] [29: reserved]
 *
 *          DatalogExecuteCode_GetItemInfo returns: 
 *              [1:max items] [31: reserved]
 */
void uch_datalog_execute(UsbCmdHandler_DatalogExecuteCode execute_code, u16 ecm_index, 
                         VehicleCommType commtype, VehicleCommLevel commlevel)
{
    u8  status;    
    u8  buffer[32];
    
    memset(buffer,0,sizeof(buffer));
    
    switch(execute_code)
    {
    case DatalogExecuteCode_PrevalidateTask:        
        //data out: [32:reserved]
        status = obd2datalog_prevalidate_task(ecm_index, commtype, commlevel);
        if (status != S_SUCCESS && status != S_NOTSUPPORT)
        {
            status = S_FAIL;
        }
        break;
    case DatalogExecuteCode_GetPacketInfo: 
        //data out: [1:packetmode][1:packetcount][1:packetsize]
        obd2datalog_datalogmode_get(commtype, commlevel);        
        buffer[0] = gDatalogmode.packetmode;
        buffer[1] = gDatalogmode.packetCount;
        buffer[2] = gDatalogmode.packetSize;       
        status = S_SUCCESS;
        break;
    case DatalogExecuteCode_GetItemInfo:
        //data out: [1:max items]
        obd2datalog_datalogmode_get(commtype, commlevel);        
        buffer[0] = (u8)MAX_DATALOG_DLXBLOCK_COUNT;
        status = S_SUCCESS;
        break;        
    default:
        status = S_NOTSUPPORT;
        break;
    }
    SCTUSB_SetCmdStatus(status);    
    SCTUSB_SetFlowControl(CB_FC_DATA_IN_LAST);
    usb_send_data_to_host(buffer, sizeof(buffer));
}

/**
 * @brief   Execute DTC functions
 *
 * @param   [in] execute_code
 *
 * @retval  u8 status
 *
 * @author  Tristen Pierson
 *
 * @details This function handles multiple op codes (execute_code). 
 *
 *          DatalogDTC_ClearDTC returns: 
 *              [32: reserved]
 *
 *          DatalogDTC_ScanDTC returns: 
 *              [2: listsize] [30: reserved]
 *
 *          DatalogDTC_GetDTCList returns: 
 *              [listsize: dtclist]
 */
void uch_datalog_dtc(UsbCmdHandler_DatalogDTC execute_code)
{    
    u8 status;
    u8 buffer[32];      // Used for Clear DTC, ScanDTC, and errors
    static u8 *dataptr; // Used for GetDTC
    static u32 length;  // Used for GetDTC
        
    memset(buffer,0,sizeof(buffer));
    
    switch(execute_code)
    {
    case DatalogDTC_ClearDTC:        
        //data out: [32:reserved]
        status = obd2_cleardtc(CommType_Unknown,0);
        if (status != S_SUCCESS)
        {
            log_push_error_point(0xB212);
            status = S_FAIL;
        }        
        break;
    case DatalogDTC_ScanDTC: 
        //data out: [2:listsize][30:reserved]
        status = S_FAIL;        
        if (dataptr != NULL)
        {
            // Free DTC data buffer before using if applicable
            __free(dataptr);
            dataptr = NULL;
        }
        // Allocate memory for DTC data buffer
        dataptr = __malloc(MAX_READDTC_DATALINK_BUFFER_LEN);
        if (dataptr != NULL)
        {
            status = uch_datalog_get_dtc(dataptr, &length);            
        }                        
        if (status == S_SUCCESS)
        {            
            memcpy(buffer,&length,2);
        }
        else
        {
            log_push_error_point(0xB213);
            status = S_FAIL;                                
        }            
        break;
    case DatalogDTC_GetDTC:      
        //data out: [listsize:dtclist]
        // Make sure memory has been allocated for DTC data buffer
        if (dataptr == NULL)
        {
            log_push_error_point(0xB214);
            status = S_FAIL;
        }
        else if (length > 0)
        {            
            usb_databufferlength = length;
            memcpy(usb_databuffer, dataptr, length);            
            SCTUSB_SetCmdStatus(S_SUCCESS);
            SCTUSB_SetStatusPrivData(usb_databufferlength);
            usb_trigger_send_databuffer_to_host();            
            // Free memory from DTC data buffer            
            __free(dataptr);
            dataptr = NULL;            
            return;
        }
        else
        {            
            log_push_error_point(0xB215);
            status = S_FAIL;            
        }
        break;
    default:
        log_push_error_point(0xB216);
        status = S_NOTSUPPORT;
        break;
    }

    SCTUSB_SetCmdStatus(status);    
    SCTUSB_SetFlowControl(CB_FC_DATA_IN_LAST);
    usb_send_data_to_host(buffer, sizeof(buffer));     
}

/**
 * @brief   Get DTCs
 *
 * @param   [in] *dataptr
 * @param   [in] *length
 *
 * @retval  u8 status
 *
 * @author  Tristen Pierson
 *
 * @details This function returns parsed DTC data
 */
u8 uch_datalog_get_dtc(u8 *dataptr, u32 *length)
{
    dtc_info dtc_info;
    u32 i;
    u32 tmplength;
    u16 tmpcode;
    u8  tmpecmindex;
    u8  tmpcodebuf[16];
    u8  tmpbuf[256];    
    u8  status;
    VehicleCommType vehiclecommtype[ECM_MAX_COUNT];
    VehicleCommLevel vehiclecommlevel[ECM_MAX_COUNT];
        
    status = obd2_findvehiclecommtypecommlevel(vehiclecommtype, vehiclecommlevel);
    
    if (status == S_SUCCESS)
    {
        status = obd2_readdtc(&dtc_info);
    }
    
    if (status == S_SUCCESS)
    {
        memset(dataptr,0,MAX_READDTC_DATALINK_BUFFER_LEN);       
        
        dataptr[0] = dtc_info.count;
        dataptr[1] = 0;
        dataptr[2] = 0;
        tmplength = 2;

        for(i=0;i<dtc_info.count;i++)
        {
            tmpcode = dtc_info.codes[i];
            tmpecmindex = dtc_info.code_ecmindex[i];
            tmpcodebuf[0] = obd2_parsetype_dtc(tmpcode);
            tmpcode &= 0x3FFF;
            sprintf((void*)&tmpcodebuf[1],"%04X",tmpcode);
            sprintf((char*)tmpbuf,"<C>%s<D>",tmpcodebuf);

            if ((tmplength + strlen((char*)tmpbuf)) < (MAX_READDTC_DATALINK_BUFFER_LEN-3))
            {
                strcat((char*)&dataptr[tmplength],(char*)tmpbuf);
                tmplength += strlen((char*)tmpbuf);

                status = obd2_getdtcinfo(vehiclecommlevel[tmpecmindex],tmpcodebuf,tmpbuf);
                if (status == S_SUCCESS)
                {
                    if ((tmplength + strlen((char*)tmpbuf)) < (MAX_READDTC_DATALINK_BUFFER_LEN-3))
                    {
                        strcat((char*)&dataptr[tmplength],(char*)tmpbuf);
                        tmplength += strlen((char*)tmpbuf);
                    }
                }
            }//if ((tmplength + ...
        }//for(i=0;i<dtc_info.count;i++)...
        status = S_SUCCESS;
        *length = strlen((char*)&dataptr[2]) + 3;
    }
    else
    {
#ifndef __DEBUG_JTAG_
        //in case VB crash or stuck. Only in RELEASE so we can see this in development
        peripherals_init_vehicleboard();
#endif
        status = S_FAIL;
    }
    return status;
}

//------------------------------------------------------------------------------
// Scan a dlx file and generate a dlx with only valid items
// Inputs:  u8  *source_dlx_filename
//          u8  *scanned_dlx_filename
// Engineer: Quyen Leba
//------------------------------------------------------------------------------
u8 uch_datalog_scan_dlx_file(u8 *source_dlx_filename, u8 *scanned_dlx_filename)
{
//    status = obd2datalog_init(&vehiclecommtype,&vehiclecommlevel);
    //interrupt_sw_trigger_set_func(,TRUE);
    return S_NOTSUPPORT;
}

//------------------------------------------------------------------------------
// Scan a dlx block
// Input:   DlxBlock dlxblock
// Return:  u8  status (S_SUCCESS: valid)
// Engineer: Quyen Leba
// Note: correct comm type must be initialized prior to this function
//------------------------------------------------------------------------------
u8 uch_datalog_scan_dlx_block(DlxBlock *dlxblock)
{
    u8  status;

    status = obd2datalog_validate_by_dlx_block(dlxblock,
                                               (VehicleCommType)dlxblock->pidCommType,
                                               (VehicleCommLevel)dlxblock->pidCommLevel);
    return status;
}

//------------------------------------------------------------------------------
// Start datalog
// Inputs:  u8 attrib
//          u8 speed (0-200 in ms)
//          u16 flags
//          u8  *dlx_filename
// Engineer: Quyen Leba
//------------------------------------------------------------------------------
u8 uch_datalog_start(u8 attrib, u8 speed, u16 flags, u32 signature, u8 *dlx_filename)
{
    u32 tmp_u32;
    u8  filename[MAX_DATALOG_FILENAME_LENGTH+1];
    u8  status;

    usb_clear_alt_data();
    usbcmdhandler_datalog_info.max_data_report_frame_length = 0;
    usbcmdhandler_datalog_info.signature = signature;
    SCTUSB_SetStatusPrivData(usbcmdhandler_datalog_info.max_data_report_frame_length);
    if (usbcmdhandler_datalog_info.state != PassthroughDatalogState_Idle)
    {
        log_push_error_point(0xB230);
        status = S_BUSY;
        goto uch_datalog_start_done;
    }
    else if (file_isfilenamevalid(dlx_filename,MAX_DATALOG_FILENAME_LENGTH) == FALSE)
    {
        log_push_error_point(0xB231);
        status = S_INPUT;
        goto uch_datalog_start_done;
    }

    usbcmdhandler_datalog_info.state = PassthroughDatalogState_StartingUp;
    status = file_user_getfilename(dlx_filename,filename);
    if (status != S_SUCCESS)
    {
        log_push_error_point(0xB232);
        status = S_INPUT;
        goto uch_datalog_start_done;
    }
    usbcmdhandler_datalog_info.flags = flags;

    indicator_set(Indicator_Datalog_Setup);
    status = obd2datalog_dataloginfo_init();
    if (status != S_SUCCESS)
    {
        log_push_error_point(0xB233);
        goto uch_datalog_start_done;
    }
    
    status = obd2datalog_parsedatalogfile(filename);
    if (status != S_SUCCESS)
    {
        log_push_error_point(0xB234);
        goto uch_datalog_start_done;
    }

    status = obd2datalog_get_total_signal(&tmp_u32);
    if (status != S_SUCCESS)
    {
        log_push_error_point(0xB235);
        goto uch_datalog_start_done;
    }
    
    usbcmdhandler_datalog_info.max_data_report_frame_length = 
        usbcmdhandler_datalog_info.max_data_report_frame_length / ALT_BULK_MAX_PACKET_SIZE*ALT_BULK_MAX_PACKET_SIZE;
    //CSW private data has data report frame max length
    SCTUSB_SetStatusPrivData(usbcmdhandler_datalog_info.max_data_report_frame_length);

    if (attrib & DATALOG_START_ATTRIB_DISABLE_NEWDATA_FILTER)
    {
        obd2datalog_newdata_filter(FALSE);
    }
    else
    {
        obd2datalog_newdata_filter(TRUE);
    }
    uch_datalog_setdatareportinterval(speed);
    usbcmdhandler_datalog_info.control.stop = 0;
    interrupt_sw_trigger_set_func(SoftwareIntrTrigger_0,
                                  uch_datalog_start_trigger,TRUE);
    status = interrupt_sw_trigger(SoftwareIntrTrigger_0);

uch_datalog_start_done:
    if (status != S_SUCCESS)
    {
        usbcmdhandler_datalog_info.state = PassthroughDatalogState_Idle;
        indicator_clear();
        indicator_link_status();
        obd2_garbagecollector();
    }
    return status;
}


//------------------------------------------------------------------------------
// Stop datalog
// Engineer: Quyen Leba
// This causes 'uch_datalog_start_trigger()' to quit datalog. This function
// may not stop datalog immediately; do USBCmdDatalog_GetStatus to check
// for PassthroughDatalogState_Idle
//------------------------------------------------------------------------------
u8 uch_datalog_stop()
{
    usbcmdhandler_datalog_info.control.stop = 1;
    return S_SUCCESS;
}

//------------------------------------------------------------------------------
// Used by a software interrupt trigger to start datalog session
// Engineer: Quyen Leba
//------------------------------------------------------------------------------
void uch_datalog_start_trigger()
{
    u8  report[4+4+2+6*MAX_DATALOG_DLXBLOCK_COUNT+4];
    u16 reportlength;
    u32 current_timestamp;
    u32 usblost_timestamp;
    u32 report_timestamp;
    
    u8  status;

    uch_ui_lock_time_reset();

    usb_clear_alt_data();
    //##########################################################################
    //
    //##########################################################################
    status = obd2datalog_init(usbcmdhandler_datalog_info.commtype,
                              usbcmdhandler_datalog_info.commlevel);

    if (status != S_SUCCESS)
    {
        dataloginfo->commstatus = DatalogCommLost;
        usbcmdhandler_datalog_info.state = PassthroughDatalogState_CommLost;
        indicator_set(Indicator_Datalog_CommLost);
    }
    else
    {
        memcpy((char*)dataloginfo->commtype,
               usbcmdhandler_datalog_info.commtype,ECM_MAX_COUNT);
        memcpy((char*)dataloginfo->commlevel,
               usbcmdhandler_datalog_info.commlevel,ECM_MAX_COUNT);

        obd2datalog_datastream_stop(usbcmdhandler_datalog_info.commtype,
                                    usbcmdhandler_datalog_info.commlevel);
        
        status = obd2datalog_startup(usbcmdhandler_datalog_info.commtype,
                                     usbcmdhandler_datalog_info.commlevel);
        if (status != S_SUCCESS)
        {
            goto uch_datalog_start_trigger_done;
        }
        dataloginfo->commstatus = DatalogCommActive;
        usbcmdhandler_datalog_info.state = PassthroughDatalogState_CommActive;
        indicator_set_normaloperation(TRUE);
        indicator_set(Indicator_Datalog_Active);
        rtc_reset();
    }
    
    // Handle analog port external power and outputs
    obd2datalog_analogport_init();

    //##########################################################################
    //
    //##########################################################################
    usb_clear_alt_data();
    usb_set_alt_data_max_frame_length(usbcmdhandler_datalog_info.max_data_report_frame_length);
    current_timestamp = rtc_getvalue();
    report_timestamp = current_timestamp;
    dataloginfo->lastdatapoint_timestamp = current_timestamp;
    dataloginfo->lastdatareport_timestamp = current_timestamp;
    dataloginfo->lasttesterpresent_timestamp = current_timestamp;
    dataloginfo->lastosc_timestamp = current_timestamp;
    usblost_timestamp = current_timestamp;
    
    //enter critical session of passthrough datalog
    usbcmdhandler_datalog_info.control.critical = 1;
    while(1)
    {
        uch_ui_lock_time_reset();
        //######################################################################
        // normal operation, getting data and detecting comm lost
        //######################################################################
        if (dataloginfo->commstatus == DatalogCommActive)
        {
            // Execute Tasks   
            status = S_SUCCESS;
            if (dataloginfo->scheduled_tasks & DATALOG_TASK_GET_DATA)
            {
                status = dataloginfo->getdatafunc(usbcmdhandler_datalog_info.commlevel);
            }
            if ((dataloginfo->scheduled_tasks & DATALOG_TASK_GET_DATA_SINGLE_RATE) && status == S_SUCCESS)
            {
                status = obd2datalog_getdata_singlerate(usbcmdhandler_datalog_info.commlevel);
            }
            if ((dataloginfo->scheduled_tasks & DATALOG_TASK_ANALOG) && status == S_SUCCESS)
            {
                status = dataloginfo->getanalogdatafunc();
            }
            
            if (status == S_SUCCESS)
            {
                current_timestamp = rtc_getvalue();
                //##############################################################
                // report data
                //##############################################################
                if ((current_timestamp-dataloginfo->lastdatareport_timestamp) > 
                    dataloginfo->datareport_interval && uch_is_usb_ready() == TRUE)
                {
                    *(u32*)report = usbcmdhandler_datalog_info.signature;

                    status = obd2datalog_getdatareport(&report[4],&reportlength);
                    if (status == S_SUCCESS && reportlength > 0)
                    {
                        *(u32*)&report[reportlength+4] = 0xF1F2F3F4;
                        usb_push_alt_data(report,reportlength+4+4);
                        report_timestamp = current_timestamp;
                    }
                    else if ((current_timestamp - report_timestamp) > 500)     //data unchanged for 0.5sec
                    {
                        u8  report[10];
                        
                        *(u32*)report = usbcmdhandler_datalog_info.signature;
                        *(u32*)(&report[4]) = current_timestamp;
                        *(u16*)(&report[8]) = 0;
                        usb_push_alt_data(report,10);
                        usb_trigger_send_altdatabuffer_to_host();
                        report_timestamp = current_timestamp;
                    }
                    dataloginfo->lastdatareport_timestamp = current_timestamp;
                }
                
                //##############################################################
                // send testerpreset
                //##############################################################
                if (dataloginfo->sendtesterpresent)
                {
                    if ((current_timestamp - 
                         dataloginfo->lasttesterpresent_timestamp) > 
                        DATALOG_MAX_TESTERPRESENT_INTERVAL)
                    {
                        dataloginfo->lasttesterpresent_timestamp = current_timestamp;
                        dataloginfo->sendtesterpresent();
                    }
                }
            }
            else if (status == S_COMMLOST)
            {
                indicator_set(Indicator_Datalog_CommLost);
                dataloginfo->commstatus = DatalogCommLost;
                
                usbcmdhandler_datalog_info.state = PassthroughDatalogState_CommLost;
                usb_clear_alt_data();
            }
        }
        else if (dataloginfo->commstatus == DatalogCommLost)
        {
            // Stop datalogging session if comm lost
            usbcmdhandler_datalog_info.control.stop = TRUE;
        }

        //######################################################################
        // check for other inputs and handle datalog conditions
        //######################################################################
        
        // Check for active USB datalink
        if (uch_is_usb_ready() == TRUE)
        {
            usblost_timestamp = rtc_getvalue();
        }
        // Check for USB timeout
        if (rtc_getvalue() - usblost_timestamp >= DATALOG_USB_TIMEOUT)
        {
            usbcmdhandler_datalog_info.control.stop = TRUE;
        }
        // Check for 'stop' condition
        if (usbcmdhandler_datalog_info.control.stop)
        {
            usb_clear_alt_data();
            obd2datalog_datastream_stop(usbcmdhandler_datalog_info.commtype,
                                        usbcmdhandler_datalog_info.commlevel);
            goto uch_datalog_start_trigger_done;
        }
    }//while(1)...

uch_datalog_start_trigger_done:
    obd2datalog_analogport_deinit();
    usbcmdhandler_datalog_info.state = PassthroughDatalogState_Idle;
    usb_clear_alt_data();
    usb_set_alt_data_max_frame_length(0);
    indicator_clear();
    indicator_link_status();
    obd2_garbagecollector();
    usbcmdhandler_datalog_info.control.critical = 0;
}

//------------------------------------------------------------------------------
// Set data report interval
// Input:   u16 interval (0-200 ms)
// Engineer: Quyen Leba
//------------------------------------------------------------------------------
u8 uch_datalog_setdatareportinterval(u16 interval)
{
    if (interval < DATAREPORT_MIN_INTERVAL)
    {
        interval = DATAREPORT_MIN_INTERVAL;
    }
    else if (interval > DATAREPORT_MAX_INTERVAL)
    {
        interval = DATAREPORT_MAX_INTERVAL;
    }
    return obd2datalog_setdatareportinterval(interval);
}

//------------------------------------------------------------------------------
// Get datalog features (analog inputs)
// Output:  u8  *filename (feature dlx filename)
// Engineer: Quyen Leba
//------------------------------------------------------------------------------
u8 uch_datalog_getfeatures(u8 *filename)
{
    u8  status;

    if (strlen(DATALOG_FEATURE_FILENAME) >= 64)
    {
        //should never happen
        return S_ERROR;
    }
    status = obd2datalog_createdatalogfeaturefile(DATALOG_FEATURE_FILENAME);
    if (status == S_SUCCESS)
    {
        strcpy((char*)filename,DATALOG_FEATURE_FILENAME);
    }
    else
    {
        filename[0] = NULL;
    }
    return status;
}
