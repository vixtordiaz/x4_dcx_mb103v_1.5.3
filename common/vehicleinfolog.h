/**
  **************** (C) COPYRIGHT 2012 SCT Performance, LLC *********************
  * File Name          : vehicleinfolog.h
  * Device / System    : SCT products using the "CommonCode" platform
  * Author             : Patrick Downs
  *
  * Version            : 1 
  * Date               : 09/12/2012
  * Description        : 
  *                    : 
  *
  *
  ******************************************************************************
  */

#ifndef __VEHICLEINFOLOG_H
#define __VEHICLEINFOLOG_H

#include <arch/gentype.h>

#define VIF_VERSION                     1
#define VIF_MAX_BLOCKS                  100
#define VIF_BLOCK_MAX_SIZE              4096
#define VIF_FILENAME                    GENFS_USER_FOLDER_PATH"vehicleinfo.vif"
//info block changelog:
//  - v1: changed ecm_info (MAX_TUNECODE_LENTH increased to 31 from 21)
#define VIF_INFO_BLOCK_VERSION          1

typedef struct
{
    u16 version;
    u16 flags;
    u32 header_crc32e;
    u32 content_crc32e;
    u16 currententryindex;
    u8  reserved0[2];
    u8  device_serial[SETTINGS_CRITICAL_SERIALNUMBER_LENGTH];   // 16 Bytes
    u8  reserved1[32];
}VehicleInfoLogHeader; // Total size is 64 bytes
STRUCT_SIZE_CHECK(VehicleInfoLogHeader,64);

typedef struct
{
    u32 fwversion;
    u32 blockcrc32e;
    u8  infoversion;
    u8  reserved[23];
}VehicleInfoLogBlockInfo;

typedef struct
{
    ecm_info ecminfo;
}VehicleInfoLogBlockECM;
//each block has VIF_BLOCK_MAX_SIZE bytes to contain VehicleInfoBlockInfo &
//VehicleInfoBlockECM with 0x00 padding
CCASSERT(VehicleInfoLogBlock,(sizeof(VehicleInfoLogBlockInfo)+sizeof(VehicleInfoLogBlockECM)) <= VIF_BLOCK_MAX_SIZE);

u8 vehicleinfolog_add_block(ecm_info *ecm_info);
u8 vehicleinfolog_get_latest_block(ecm_info *ecminfo);

#endif //__VEHICLEINFOLOG_H


