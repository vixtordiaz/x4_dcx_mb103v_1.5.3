/*
 *  CCVM - Common Code Virtual Machine
 *
 *  Copyright 2014 SCT Performance, LLC
 * 
 */

/**
 * @file    ccvm_funcs_checksum.c
 * @brief   Virtual machine checksum functions
 * @details This file handles checksum functions used in CCVM.
 * @date    7/23/2014 
 * @author  Tristen Pierson
 */

#include "ccvm.h"
#include <stdio.h>
#include <string.h>
#include <stdint.h>
#include "ccvm_funcs.h"

/******************************************************************************/
/* Project Specific Includes */
/******************************************************************************/

#include <common/checksum.h>
#include <common/rsa.h>

/******************************************************************************/
/* Global Variables */
/******************************************************************************/

extern uint8_t *vm_memory;  /* from ccvm_engine.c */
extern int32_t *vm_argv;    /* from ccvm_engine.c */
extern uint8_t vm_argc;     /* from ccvm_engine.c */
extern int32_t vm_ret;      /* from ccvm_engine.c */

/******************************************************************************/
/* CCVM CAN Functions */
/******************************************************************************/

/**
 * @brief       <B>VM Function:</B> int32_t vm_checksum_handle_additive(start_address, segment_length, segment_offset, quantity, iscomplement, ischeckonly)
 * @details     This function handles additive checksums
 *
 *  <B>VM Parameters:</B>
 *      - [in] start_address
 *      - [in] segment_length
 *      - [in] segment_offset
 *      - [in] quantity
 *      - [in] iscomplement? (1 = calc/apply complement, 0 = no complement)
 *      - [in] ischeckonly (1 = apply checksum, 0 = checksum only)
 *  <B>VM Returns:</B>
 *      - Status
 *
 * @param[in,out]    vm      Virtual machine state structure
 * @param[in]        argc    Argument count
 * @param[in]        argv    Argument list
 *
 * @return      Entry point argument count
 */ 
VM_FUNC_DEF(vm_checksum_handle_additive)
{    
    int32_t ret = 0;
    if (argc == 6)
    {        
        ret = checksum_apply_additives(argv[0], argv[1], argv[2], argv[3], (bool)argv[4], (bool)argv[5]);
    }
    else
    {
        vm->error = S_VM_FUNCARG;
    }
    return ret;
}

/**
 * @brief       <B>VM Function:</B> int32_t checksum_handle_crc32(start_address, end_address, seed, *CRC_Value, flags)
 * @details     This function handles additive checksums
 *
 *  <B>VM Parameters:</B>
 *      - [in] start_address
 *      - [in] end_address
 *      - [in] seed
 *      - [in] CRC_value
 *      - [in] flags (0 = CRC32_FLAGS_NONE, 1 = CRC_32_MASK_0xFF's)
 *  <B>VM Returns:</B>
 *      - Status
 *
 * @param[in,out]    vm      Virtual machine state structure
 * @param[in]        argc    Argument count
 * @param[in]        argv    Argument list
 *
 * @return      Entry point argument count
 */ 
VM_FUNC_DEF(vm_checksum_handle_crc32)
{    
    int32_t ret = 0;
    int32_t start_address;
    int32_t end_address;
    int32_t seed;
    int32_t vmpCrcValue;
    int32_t flags;
    uint32_t crc_value = 0;
    
    if (argc == 5)
    {
        start_address = argv[0];
        end_address = argv[1];
        seed = argv[2];
        vmpCrcValue = argv[3];
        flags = argv[4];
            
        ret = checksum_apply_crc32(start_address, end_address, seed, flags, &crc_value);
        
        VM_MEMWRITE_U32(crc_value,vmpCrcValue);
    }
    else
    {
        vm->error = S_VM_FUNCARG;
    }
    return ret;
}

/**
 * @brief       <B>VM Function:</B> int32_t checksum_handle_rsa_me9uk(start_address, end_address, *public_key, signature_address)
 * @details     This function handles additive checksums
 *
 *  <B>VM Parameters:</B>
 *      - [in] start_address
 *      - [in] end_address
 *      - [in] public_key
 *      - [in] signature
 *  <B>VM Returns:</B>
 *      - Status
 *
 * @param[in,out]    vm      Virtual machine state structure
 * @param[in]        argc    Argument count
 * @param[in]        argv    Argument list
 *
 * @return      Entry point argument count
 */ 
VM_FUNC_DEF(vm_checksum_handle_rsa_me9uk)
{    
    int32_t ret = 0;
    int32_t start_address;
    int32_t end_address;
    int32_t vmpPublicKey;
    int32_t signature_address;
    
    if (argc == 4)
    {
        start_address = argv[0];
        end_address = argv[1];        
        vmpPublicKey = argv[2];        
        signature_address = argv[3];
        
        ret = rsa_calc_signature_me9uk(start_address, end_address, &vm_memory[vmpPublicKey], signature_address);
    }
    else
    {
        vm->error = S_VM_FUNCARG;
    }
    return ret;
}

/**
 * @brief       <B>VM Function:</B> int32_t checksum_decrypt_rsa(*modulus, *public_exponent, *signature, *result)
 * @details     This function decrypts a 128-bit RSA signature.
 *
 *  <B>VM Parameters:</B>
 *      - [in] modulus
 *      - [in] public_exponent
 *      - [in] signature
 *      - [in] result
 *  <B>VM Returns:</B>
 *      - Status
 *
 * @param[in,out]    vm      Virtual machine state structure
 * @param[in]        argc    Argument count
 * @param[in]        argv    Argument list
 *
 * @return      Entry point argument count
 */
VM_FUNC_DEF(vm_checksum_decrypt_rsa)
{    
    int32_t ret = 0;
    int32_t vmpModulus;
    int32_t vmpPublicExponent;
    int32_t vmpSignature;
    int32_t vmpResult;
    
    if (argc == 4)
    {
        vmpModulus = argv[0];
        vmpPublicExponent = argv[1];        
        vmpSignature = argv[2];        
        vmpResult = argv[3];
        
        ret = rsa_decrypt(&vm_memory[vmpModulus], 
                          &vm_memory[vmpPublicExponent], 
                          &vm_memory[vmpSignature], 
                          &vm_memory[vmpResult]);
    }
    else
    {
        vm->error = S_VM_FUNCARG;
    }
    return ret;
}

/**
 * @brief       <B>VM Function:</B> int32_t checksum_handle_md5(start_address, end_address, *md5_hash)
 * @details     This function handles md5 checksums
 *
 *  <B>VM Parameters:</B>
 *      - [in] start_address
 *      - [in] end_address
 *      - [in] public_key
 *      - [in] signature
 *  <B>VM Returns:</B>
 *      - Status
 *
 * @param[in,out]    vm      Virtual machine state structure
 * @param[in]        argc    Argument count
 * @param[in]        argv    Argument list
 *
 * @return      Entry point argument count
 */ 
VM_FUNC_DEF(vm_checksum_handle_md5)
{    
    int32_t ret = 0;
    int32_t start_address;
    int32_t end_address;
    int32_t vmpMD5_hash;
    
    if (argc == 3)
    {
        start_address = argv[0];
        end_address = argv[1];        
        vmpMD5_hash = argv[2];
        
        ret = checksum_calc_file_segment_md5(start_address, end_address, &vm_memory[vmpMD5_hash]);
    }
    else
    {
        vm->error = S_VM_FUNCARG;
    }
    return ret;
}

