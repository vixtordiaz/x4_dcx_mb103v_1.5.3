/*
 *  CCVM - Common Code Virtual Machine
 *
 *  Copyright 2014 SCT Performance, LLC
 * 
 */

/**
 * @file    ccvm_funcs_checksum.c
 * @brief   Virtual machine checksum functions
 * @details This file handles checksum functions used in CCVM.
 * @date    7/23/2014 
 * @author  Tristen Pierson
 */

#include "ccvm.h"
#include <stdio.h>
#include <string.h>
#include <stdint.h>
#include "ccvm_funcs.h"

/******************************************************************************/
/* Project Specific Includes */
/******************************************************************************/

#include <common/filestock.h>

/******************************************************************************/
/* Global Variables */
/******************************************************************************/

extern uint8_t *vm_memory;  /* from ccvm_engine.c */
extern int32_t *vm_argv;    /* from ccvm_engine.c */
extern uint8_t vm_argc;     /* from ccvm_engine.c */
extern int32_t vm_ret;      /* from ccvm_engine.c */

/******************************************************************************/
/* CCVM Filestock Functions */
/******************************************************************************/

/**
 * @brief       <B>VM Function:</B> int32_t filestock_read(position, *data, datalength, *bytecount)
 * @details     Read data starting from a position
 *
 *  <B>VM Parameters:</B>
 *      - [in] position
 *      - [in] data
 *      - [in] datalength
 *      - [in] bytecount
 *  <B>VM Returns:</B>
 *      - Status
 *
 * @param[in,out]    vm      Virtual machine state structure
 * @param[in]        argc    Argument count
 * @param[in]        argv    Argument list
 *
 * @return      Entry point argument count
 */
VM_FUNC_DEF(vm_filestock_read)
{    
    int32_t ret = 0;
    int32_t position;
    int32_t vmpData;
    int32_t datalength;
    int32_t vmpBytecount;
    uint32_t bytecount;
        
    if (argc == 4)
    {        
        position = argv[0];    
        vmpData = argv[1];
        datalength = argv[2];
        vmpBytecount = argv[3];
            
        memset(&vm_memory[vmpData],0x00,datalength);
        bytecount = VM_MEMREAD_S32(vmpBytecount);
        
        ret = filestock_read_at_position(position, &vm_memory[vmpData], datalength, &bytecount);
        
        VM_MEMWRITE_S32(bytecount,vmpBytecount);
    }
    else
    {
        vm->error = S_VM_FUNCARG;
    }
    return ret;
}

/**
 * @brief       <B>VM Function:</B> int32_t filestock_write(*data, datalength)
 * @details     Write data to stock file
 *
 *  <B>VM Parameters:</B>
 *      - [in] position
 *      - [in] data
 *      - [in] datalength
 *  <B>VM Returns:</B>
 *      - Status
 *
 * @param[in,out]    vm      Virtual machine state structure
 * @param[in]        argc    Argument count
 * @param[in]        argv    Argument list
 *
 * @return      Entry point argument count
 */
VM_FUNC_DEF(vm_filestock_write)
{    
    int32_t ret = 0;  
    int32_t vmpData;   
    int32_t datalength;   
    
    if (argc == 2)
    {  
        vmpData  = argv[0];    
        datalength = argv[1];
        
        ret = filestock_write(&vm_memory[vmpData], datalength);
    }
    else
    {
        vm->error = S_VM_FUNCARG;
    }
    return ret;
}

/**
 * @brief       <B>VM Function:</B> int32_t filestock_updateflashfile(position, *data, datalength)
 * @details     Read data starting from a position
 *
 *  <B>VM Parameters:</B>
 *      - [in] position
 *      - [in] data
 *      - [in] datalength
 *  <B>VM Returns:</B>
 *      - Status
 *
 * @param[in,out]    vm      Virtual machine state structure
 * @param[in]        argc    Argument count
 * @param[in]        argv    Argument list
 *
 * @return      Entry point argument count
 */
VM_FUNC_DEF(vm_filestock_updateflashfile)
{    
    int32_t ret = 0;
    int32_t position;   
    int32_t vmpData;   
    int32_t datalength; 
    
    if (argc == 3)
    {        
        position = argv[0];    
        vmpData  = argv[1];    
        datalength = argv[2];
        
        ret = filestock_updateflashfile(position, &vm_memory[vmpData], datalength);
    }
    else
    {
        vm->error = S_VM_FUNCARG;
    }
    return ret;
}

/**
 * @brief       <B>VM Function:</B> int32_t filestock_fill(byte, count)
 * @details     Fill stock with count of byte
 *
 * @param[in,out]    vm      Virtual machine state structure
 * @param[in]        argc    Argument count
 * @param[in]        argv    Argument list
 *
 * @return      Entry point argument count
 */
VM_FUNC_DEF(vm_filestock_fill)
{    
    int32_t ret = 0;
    int32_t byte;   
    int32_t count;   
        
    if (argc == 2)
    {        
        byte = argv[0];    
        count  = argv[1]; 
        
        ret = filestock_fill(byte, count);
    }
    else
    {
        vm->error = S_VM_FUNCARG;
    }
    return ret;
}
