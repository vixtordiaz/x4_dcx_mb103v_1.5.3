/*
 *  CCVM - Common Code Virtual Machine
 *
 *  Original work Copyright (C) 2011 Clifford Wolf <clifford@clifford.at>
 *  Modified work Copyright 2014 SCT Performance, LLC
 *
 *  Permission to use, copy, modify, and/or distribute this software for any
 *  purpose with or without fee is hereby granted, provided that the above
 *  copyright notice and this permission notice appear in all copies.
 *  
 *  THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
 *  WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
 *  MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
 *  ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
 *  WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
 *  ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
 *  OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
 *
 */

/**
 * @file    ccvm.c
 * @brief   Stack machine implementation
 * @details This file contains the stack machine implementation and pushdown
 *          stack helpers. The stack machine implementation also includes
 *          memory checking and constraint enforcment to allow for different
 *          stack and memory sizes.
 * @date    7/11/2014 
 * @author  Tristen Pierson
 */ 

#include "ccvm.h"
#include <stdlib.h>
#include <string.h>
#include <stdint.h>

/******************************************************************************/
/* Global Variables */
/******************************************************************************/
     
extern uint8_t *vm_memory;  /* from ccvm_engine.c */
extern int32_t vm_ret;      /* from ccvm_engine.c */

/******************************************************************************/
/* Static Functions */
/******************************************************************************/

/**
 * @brief       Sign extension
 * @details     This function toggles sign extension bit
 *
 * @param[in]   val     Input value to apply sign extension
 * @param[in]   mask    A mask applied before applying sign extension
 *
 * @return      A value with sign extension bit
 */ 
static inline int32_t signext(uint32_t val, uint32_t mask)
{
	val = val & mask;
	if ((val & ~(mask >> 1)) != 0)
		val |= ~mask;
	return val;
}

/******************************************************************************/
/* Functions */
/******************************************************************************/
    
/**
 * @brief       Stack machine executor
 * @details     This function is the heart of the stack machine. Each time this
 *              function is called, at least one opcode is executed and the
 *              machine state is updated accordingly. 
 *
 * @param[in,out]   vm      Virtual machine state structure
 */ 
extern void ccvm_exec(struct ccvm_s *vm)
{
    uint8_t opcode = vm->mem_read(vm, vm->ip, 1);
	uint16_t addr = 0;
	int32_t a = 0, b = 0;
	int8_t sfa = 0;

	// Read only one opcode
    switch (opcode)
	{
    case 0x00:  // Push local variable to stack
		sfa = signext(vm->mem_read(vm, vm->ip+1, 1), 0xff);
        ccvm_push(vm, ccvm_local_read(vm, sfa));
		vm->ip+=2;
		break;
    case 0x10:  // Push 1 zero to the stack
    case 0x11:  // Push 2 zeros to the stack
    case 0x12:  // Push 3 zeros to the stack
    case 0x13:  // Push 4 zeros to the stack
    case 0x14:  // Push 5 zeros to the stack
    case 0x15:  // Push 6 zeros to the stack
    case 0x16:  // Push 7 zeros to the stack
    case 0x17:  // Push 8 zeros to the stack        
		for (sfa = 0; sfa <= (opcode & 0x07); sfa++)
			ccvm_push(vm, 0);
		vm->ip++;
		break;        
    case 0x40:  // Pop local variable from stack,
		sfa = signext(vm->mem_read(vm, vm->ip+1, 1), 0xff);
		ccvm_local_write(vm, sfa, ccvm_pop(vm));
		vm->ip+=2;
		break;
    case 0x50:  // Pop 1 value but keep top
    case 0x51:  // Pop 2 values but keep top
    case 0x52:  // Pop 3 values but keep top
    case 0x53:  // Pop 4 values but keep top
    case 0x54:  // Pop 5 values but keep top
    case 0x55:  // Pop 6 values but keep top
    case 0x56:  // Pop 7 values but keep top
    case 0x57:  // Pop 8 values but keep top        
		a = ccvm_pop(vm);
		vm->sp += 4 + 4*(opcode & 0x07);
		ccvm_push(vm, a);
		vm->ip++;
		break;
    case 0x60:  // Call User Function
		{
			uint8_t argc;
            int16_t funcid;
            int32_t *argv;
            
            // Get argument count
            argc = ccvm_pop(vm);
            // Reserve memory for function parameters
            argv = VM_MALLOC(argc*sizeof(int32_t));
            if (argv == NULL)
            {
                vm->error = S_VM_MALLOC;
                break;
            }
            
            // Get function parameters
            for (sfa=0; sfa<argc; sfa++)
            {
                argv[sfa] = ccvm_pop(vm);
            }
            // Get function identifier
            funcid = vm->mem_read(vm, vm->ip+1, 2);
            // Call user function
            a = vm->call_user(vm, funcid, argc, argv);
            ccvm_push(vm, a);
            vm->ip+=3;
            VM_FREE(argv);
		}
		break;        
    case 0x80:  // Add
    case 0x81:  // Sub
    case 0x82:  // Mul
    case 0x83:  // Div
    case 0x84:  // Mod
    case 0x85:  // Shift Left
    case 0x86:  // Shift Right
    case 0x87:  // Bitwise AND
    case 0x88:  // Bitwise OR
    case 0x89:  // Bitwise XOR
    case 0x8a:  // Logic AND
    case 0x8b:  // Logic OR
    case 0xa8:  // Compare "<"
    case 0xa9:  // Compare "<="
    case 0xaa:  // Compare "=="
    case 0xab:  // Compare "!="
    case 0xac:  // Compare ">="
    case 0xad:  // Compare ">"    
		b = ccvm_pop(vm);
    case 0x8c:  // Bitwise NOT
    case 0x8d:  // Arithmetic Invert
    case 0x8e:  // Logic NOT
		a = ccvm_pop(vm);
		switch (opcode)
		{
			case 0x80 +  0: ccvm_push(vm, a + b);  break;
			case 0x80 +  1: ccvm_push(vm, a - b);  break;
			case 0x80 +  2: ccvm_push(vm, a * b);  break;
			case 0x80 +  3: ccvm_push(vm, a / b);  break;
			case 0x80 +  4: ccvm_push(vm, a % b);  break;
			case 0x80 +  5: ccvm_push(vm, a << b); break;
			case 0x80 +  6: ccvm_push(vm, a >> b); break;
			case 0x80 +  7: ccvm_push(vm, a & b);  break;
			case 0x80 +  8: ccvm_push(vm, a | b);  break;
			case 0x80 +  9: ccvm_push(vm, a ^ b);  break;
			case 0x80 + 10: ccvm_push(vm, a && b); break;
			case 0x80 + 11: ccvm_push(vm, a || b); break;
			case 0x80 + 12: ccvm_push(vm, ~a);     break;
			case 0x80 + 13: ccvm_push(vm, -a);     break;
			case 0x80 + 14: ccvm_push(vm, !a);     break;
			case 0xa8 +  0: ccvm_push(vm, a <  b); break;
			case 0xa8 +  1: ccvm_push(vm, a <= b); break;
			case 0xa8 +  2: ccvm_push(vm, a == b); break;
			case 0xa8 +  3: ccvm_push(vm, a != b); break;
			case 0xa8 +  4: ccvm_push(vm, a >= b); break;
			case 0xa8 +  5: ccvm_push(vm, a >  b); break;
		}
		vm->ip++;
		break;
    case 0x90:  // Push immediate, VAL (3-bit signed, -4 to 3)
    case 0x91:
    case 0x92:
    case 0x93:
    case 0x94:
    case 0x95:
    case 0x96:
    case 0x97:
		a = signext(opcode, 0x07);
		if ((a & 0x04) != 0)
			a |= ~0x07;
		ccvm_push(vm, a);
		vm->ip++;
		break;
	case 0x98:  // Push u8
		a = vm->mem_read(vm, vm->ip+1, 1) & 0x000000ff;
		ccvm_push(vm, a);
		vm->ip += 2;
		break;
	case 0x99:  // Push s8
		a = vm->mem_read(vm, vm->ip+1, 1) & 0x000000ff;
		ccvm_push(vm, signext(a, 0x000000ff));
		vm->ip += 2;
		break;
	case 0x9a:  // Push u16
		a = vm->mem_read(vm, vm->ip+1, 2) & 0x0000ffff;
		ccvm_push(vm, a);
		vm->ip += 3;
		break;
	case 0x62:  // Push s16
        a = vm->mem_read(vm, vm->ip+1, 2) & 0x0000ffff;
		ccvm_push(vm, signext(a, 0x0000ffff));
		vm->ip += 3;
		break;
    case 0x61:  // Push u32
        a = vm->mem_read(vm, vm->ip+1, 4);
		ccvm_push(vm, a);
		vm->ip += 5;
        break;
    case 0x9b:  // Return from function
		a = ccvm_pop(vm);
		if (0) {
	case 0x9c:  // Return from function without value
			a = 0;
		}
		vm->sp = vm->sfp;
		vm->ip = ccvm_pop(vm);
		vm->sfp = ccvm_pop(vm);
		if ((vm->sfp & 1) != 0)
			vm->sfp &= ~1;
		else
			ccvm_push(vm, a);
		/* If end of program, set VM return */
        if (vm->sp == 0x0000 && vm->sfp == 0x0000)
        {
            vm_ret = a;
        }
        break;
	case 0x9d:  // Drop value - Pop 1
		ccvm_pop(vm);
		vm->ip++;
		break;
	case 0x9e:  // Call address (from SP)
		addr = ccvm_pop(vm);
		if (vm->mem_read(vm, vm->ip+1, 1) == 0x9d) {
			ccvm_push(vm, vm->sfp | 1);
			ccvm_push(vm, vm->ip + 2);
		} else {
			ccvm_push(vm, vm->sfp);
			ccvm_push(vm, vm->ip + 1);
		}
		vm->sfp = vm->sp;
		vm->ip = addr;
		break;
	case 0x9f:  // Jump to address (from SP)
		vm->ip = ccvm_pop(vm);
		break;
    case 0xa0:  // Jump (1-byte relative address)
    case 0xa1:  // Jump (2-byte relative address)
    case 0xa2:  // Call (1-byte relative address)
    case 0xa3:  // Call (2-byte relative address)
    case 0xa4:  // Jump IF (1-byte relative address)
    case 0xa5:  // Jump IF (2-byte relative address)
    case 0xa6:  // Jump UNLESS (1-byte relative address)
    case 0xa7:  // Jump UNLESS (2-byte relative address)
		if ((opcode & 1) == 0) {
			// 1-byte
            addr = vm->ip + signext(vm->mem_read(vm, vm->ip+1, 1), 0x00ff);
			vm->ip += 2;
		} else {
			// 2-byte
            addr = vm->ip + vm->mem_read(vm, vm->ip+1, 2) & 0xffff;            
			vm->ip += 3;
		}
		switch (opcode)
		{
		case 0xa0: // Jump
		case 0xa1:
			vm->ip = addr;
			break;
		case 0xa2: // Call
		case 0xa3:
			if (vm->mem_read(vm, vm->ip, 1) == 0x9d) {
				ccvm_push(vm, vm->sfp | 1);
				ccvm_push(vm, vm->ip + 1);
			} else {
				ccvm_push(vm, vm->sfp);
				ccvm_push(vm, vm->ip);
			}
			vm->sfp = vm->sp;
			vm->ip = addr;
			break;
		case 0xa4: // Jump IF
		case 0xa5:
			if (ccvm_pop(vm))
				vm->ip = addr;
			break;
		case 0xa6: // Jump UNLESS
		case 0xa7:
			if (!ccvm_pop(vm))
				vm->ip = addr;
			break;
		}
		break;
	case 0xae:  // Push Stack Pointer - SP
		ccvm_push(vm, vm->sp);
		vm->ip++;
		break;
	case 0xaf:  // Push Stack Frame Pointer - SFP
		ccvm_push(vm, vm->sfp);
		vm->ip++;
		break;
    case 0xb0:  // Load u8 from 1-byte address
    case 0xb1:  // Load u8 from 2-byte address
    case 0xb2:  // Pop SP
    case 0xb3:  // Load u8 from 1-byte SP
    case 0xb4:  // Load u8 from 2-byte SP
    case 0xb5:  // Bury; Duplicates the top element on the stack
    case 0xb6:  // Dig; Exchanges the two top elements of the stack
    case 0xb7:  // Reserved
    case 0xb8:  // Store u8 from 1-byte address
    case 0xb9:  // Store u8 from 2-byte address
    case 0xba:  // Pop SP
    case 0xbb:  // Store u8 from 1-byte SP
    case 0xbc:  // Store u8 from 2-byte SP
    case 0xbd:  // Bury; Duplicates the top element on the stack
    case 0xbe:  // Dig; Exchanges the two top elements of the stack
    case 0xbf:  // Reserved
    case 0xc0:  // Load s8 from 1-byte address
    case 0xc1:  // Load s8 from 2-byte address
    case 0xc2:  // Pop SP
    case 0xc3:  // Load s8 from 1-byte SP
    case 0xc4:  // Load s8 from 2-byte SP
    case 0xc5:  // Bury; Duplicates the top element on the stack
    case 0xc6:  // Dig; Exchanges the two top elements of the stack
    case 0xc7:  // Reserved
    case 0xc8:  // Store s8 from 1-byte address
    case 0xc9:  // Store s8 from 2-byte address
    case 0xca:  // Pop SP
    case 0xcb:  // Store s8 from 1-byte SP
    case 0xcc:  // Store s8 from 2-byte SP
    case 0xcd:  // Bury; Duplicates the top element on the stack
    case 0xce:  // Dig; Exchanges the two top elements of the stack
    case 0xcf:  // Reserved
    case 0xd0:  // Load u16 from 1-byte address
    case 0xd1:  // Load u16 from 2-byte address
    case 0xd2:  // Pop SP
    case 0xd3:  // Load u16 from 1-byte SP
    case 0xd4:  // Load u16 from 2-byte SP
    case 0xd5:  // Bury; Duplicates the top element on the stack
    case 0xd6:  // Dig; Exchanges the two top elements of the stack
    case 0xd7:  // Reserved
    case 0xd8:  // Store u16 from 1-byte address
    case 0xd9:  // Store u16 from 2-byte address
    case 0xda:  // Pop SP
    case 0xdb:  // Store u16 from 1-byte SP
    case 0xdc:  // Store u16 from 2-byte SP
    case 0xdd:  // Bury; Duplicates the top element on the stack
    case 0xde:  // Dig; Exchanges the two top elements of the stack
    case 0xdf:  // Reserved
    case 0xe0:  // Load s16 from 1-byte address
    case 0xe1:  // Load s16 from 2-byte address
    case 0xe2:  // Pop SP
    case 0xe3:  // Load s16 from 1-byte SP
    case 0xe4:  // Load s16 from 2-byte SP
    case 0xe5:  // Bury; Duplicates the top element on the stack
    case 0xe6:  // Dig; Exchanges the two top elements of the stack
    case 0xe7:  // Reserved
    case 0xe8:  // Store s16 from 1-byte address
    case 0xe9:  // Store s16 from 2-byte address
    case 0xea:  // Pop SP
    case 0xeb:  // Store s16 from 1-byte SP
    case 0xec:  // Store s16 from 2-byte SP
    case 0xed:  // Bury; Duplicates the top element on the stack
    case 0xee:  // Dig; Exchanges the two top elements of the stack
    case 0xef:  // Reserved
    case 0xf0:  // Load u32 from 1-byte address
    case 0xf1:  // Load u32 from 2-byte address
    case 0xf2:  // Pop SP
    case 0xf3:  // Load u32 from 1-byte SP
    case 0xf4:  // Load u32 from 2-byte SP
    case 0xf5:  // Bury; Duplicates the top element on the stack
    case 0xf6:  // Dig; Exchanges the two top elements of the stack
    case 0xf7:  // Reserved
    case 0xf8:  // Store u32 from 1-byte address
    case 0xf9:  // Store u32 from 2-byte address
    case 0xfa:  // Pop SP
    case 0xfb:  // Store u32 from 1-byte SP
    case 0xfc:  // Store u32 from 2-byte SP
    case 0xfd:  // Bury; Duplicates the top element on the stack
    case 0xfe:  // Dig; Exchanges the two top elements of the stack
    case 0xff:  // Reserved
		// Bury Instruction
        if ((opcode & 0x07) == 5) {
			uint8_t depth = (opcode >> 3) & 0x07;
            int32_t *stack;            
            stack = VM_MALLOC((depth+1)*sizeof(int32_t));
            if (stack == NULL)
            {
                vm->error = S_VM_MALLOC;
                break;
            }            
			for (sfa = 0; sfa <= depth; sfa++)
				stack[sfa] = ccvm_pop(vm);
			ccvm_push(vm, stack[0]);
			for (sfa = depth; sfa > 0; sfa--)
				ccvm_push(vm, stack[sfa]);
			ccvm_push(vm, stack[0]);
			vm->ip++;
            VM_FREE(stack);
            break;
		}
		// Dig Instruction
        if ((opcode & 0x07) == 6) {
			uint8_t depth = (opcode >> 3) & 0x07;            
            int32_t *stack;            
            stack = VM_MALLOC((depth+2)*sizeof(int32_t));
            if (stack == NULL)
            {
                vm->error = S_VM_MALLOC;
                break;
            }            
			for (sfa = 0; sfa < depth+2; sfa++)
				stack[sfa] = ccvm_pop(vm);
			for (sfa = depth+1; sfa > 0; sfa--)
				ccvm_push(vm, stack[sfa-1]);
			ccvm_push(vm, stack[depth+1]);
			vm->ip++;
            VM_FREE(stack);
            break;
		}		
		switch (((opcode >> 3) & 0x0F) - 0x6)
        {
        case 0: // 1-byte
        case 1:
        case 2:
        case 3:
            sfa = 0;
            break;
        case 4: // 2-byte
        case 5:
        case 6:
        case 7:
            sfa = 1;
            break;
        case 8: // 4-byte
        case 9:
            sfa = 2;
            break;
        default:
            sfa = 0;
            break;
        }
        switch (opcode & 0x07) // Address Mode
		{
		case 0: // 1-byte unsigned
			addr = vm->mem_read(vm, vm->ip+1, 1) & 0x000000ff;
			vm->ip += 2;
			break;
		case 1: // 2-byte
			addr = vm->mem_read(vm, vm->ip+1, 2) & 0x0000ffff;
			vm->ip += 3;
			break;
		case 2: // pop before data
			addr = ccvm_pop(vm);
			vm->ip++;
			break;
		case 3: // popped + 1-byte unsigned
			addr = (ccvm_pop(vm) << sfa) + (vm->mem_read(vm, vm->ip+1, 1) & 0x000000ff);
			vm->ip += 2;
			break;
		case 4: // popped + 2 byte
			addr = (ccvm_pop(vm) << sfa) + vm->mem_read(vm, vm->ip+1, 2) & 0x0000ffff;
			vm->ip += 3;
			break;
		}
        switch ( ((opcode >> 3) & 0x0F) - 0x6 ) // Instruction
		{
		case 0: // load u8
			ccvm_push(vm, vm->mem_read(vm, addr, 1) & 0x000000ff);
			break;
		case 1: // store u8
			vm->mem_write(vm, addr, ccvm_pop(vm), 1);
			break;
		case 2: // load s8
			ccvm_push(vm, signext(vm->mem_read(vm, addr, 1), 0x000000ff));
			break;
		case 3: // store s8
			vm->mem_write(vm, addr, ccvm_pop(vm), 1);
			break;
		case 4: // load u16
			ccvm_push(vm, vm->mem_read(vm, addr, 2) & 0x0000ffff);
			break;
		case 5: // store u16
			vm->mem_write(vm, addr, ccvm_pop(vm), 2);
			break;
        case 6: // load s16
			ccvm_push(vm, signext(vm->mem_read(vm, addr, 2), 0x0000ffff));
			break;
		case 7: // store s16
			vm->mem_write(vm, addr, ccvm_pop(vm), 2);
			break;
        case 8: // load u32
            ccvm_push(vm, vm->mem_read(vm, addr, 4));
			break;
		case 9: // store u32
			vm->mem_write(vm, addr, ccvm_pop(vm), 4);
			break;
		}
		break;
    default:    // Invalid opcode
        vm->error = S_VM_INVALID_OPCODE;
        break;
	}
}

/**
 * @brief       Initialize stack machine session
 * @details     This function pushes the stack and sets up the next IP. This
 *              function is usually used to set up a new VM session.
 *
 * @param[in,out]   vm      Virtual machine state structure
 * @param[in]       addr    Entry point address
 */ 
void ccvm_init(struct ccvm_s *vm, uint16_t addr)
{
    vm->ip = VM_STACK_SIZE + VM_MEMORY_SIZE - 1;
    vm->sp = 0;
	vm->sfp = 0;
    vm->max_sp = 0xFFFF;
    ccvm_push(vm, vm->sfp | 1);
	ccvm_push(vm, vm->ip);
	vm->sfp = vm->sp;
    vm->ip = addr;
    vm->error = S_VM_READY; 
}

/**
 * @brief       Stack pop
 * @details     This function pops a value from the stack. The SP boundry is
 *              also enforced to allow flexible stack sizes. Additionally,
 *              stack overflow condition is checked.
 *
 * @param[in,out]   vm      Virtual machine state structure
 *
 * @return      Value to popped from the stack
 */ 
int32_t ccvm_pop(struct ccvm_s *vm)
{
	int32_t value = vm->stack_read(vm, vm->sp);
    
    vm->sp += 4;
    // Enforce boundry
    if (vm->sp == VM_MEMORY_SIZE + VM_STACK_SIZE)
    {
        vm->sp = 0;
    }
    // Stack overflow check
    if (vm->sp > VM_MEMORY_SIZE + VM_STACK_SIZE)
    {
        vm->error = S_VM_STACK_OVERFLOW;
    }
	return value;
}

/**
 * @brief       Stack push
 * @details     This function pushes a value to the stack. The SP boundry is
 *              also enforced to allow flexible stack sizes.
 *
 * @param[in,out]   vm      Virtual machine state structure
 * @param[in]       value   Value to push to the stack
 */ 
void ccvm_push(struct ccvm_s *vm, int32_t value)
{
	// Enforce boundry
    if (vm->sp == 0)
    {
        vm->sp = VM_MEMORY_SIZE + VM_STACK_SIZE - 4;
    }
    else if (vm->sp <= 4)
    {
        vm->error = S_VM_STACK_OVERFLOW;
    }
    else
    {
        vm->sp -= 4;
    } 
	vm->stack_write(vm, vm->sp, value);
    
    // Log max SP
    if (vm->sp < vm->max_sp)
        vm->max_sp = vm->sp;
}

/**
 * @brief       Stack read
 * @details     This function reads a value from the stack. This function is
 *              usually used to read local variables.
 *
 * @param[in,out]   vm      Virtual machine state structure
 * @param[in]       sfa     Stack frame address
 *
 * @return      Value read from the stack
 */ 
int32_t ccvm_local_read(struct ccvm_s *vm, int8_t sfa)
{
	uint16_t addr = vm->sfp - 4*sfa + (sfa < 0 ? +4 : -4);
    return vm->stack_read(vm, addr);
}

/**
 * @brief       Stack write
 * @details     This function writes a value to the stack. This function is
 *              usually used to write to local variables.
 *
 * @param[in,out]   vm      Virtual machine state structure
 * @param[in]       sfa     Stack frame address
 * @param[in]       value   Value written to the stack
 */ 
void ccvm_local_write(struct ccvm_s *vm, int8_t sfa, int32_t value)
{
	uint16_t addr = vm->sfp - 4*sfa + (sfa < 0 ? +4 : -4);
    vm->stack_write(vm, addr, value);
}

