/*
 *  CCVM - Common Code Virtual Machine
 *
 *  Original work Copyright (C) 2011 Clifford Wolf <clifford@clifford.at>
 *  Modified work Copyright 2014 SCT Performance, LLC
 *
 *  Permission to use, copy, modify, and/or distribute this software for any
 *  purpose with or without fee is hereby granted, provided that the above
 *  copyright notice and this permission notice appear in all copies.
 *  
 *  THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
 *  WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
 *  MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
 *  ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
 *  WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
 *  ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
 *  OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
 *
 */

/**
 * @file    ccvm.h
 * @brief   Stack machine implementation definitions
 * @details This file contains definitions used in the stack machine
 *          implementation. This file needs to be included in the host
 *          application.
 * @date    7/11/2014 
 * @author  Tristen Pierson
 */ 

#ifndef ccvm_H
#define ccvm_H

#include "ccvm_config.h"

#ifdef  __cplusplus
extern "C" {
#endif

/******************************************************************************/
/* Configuration file check */
/******************************************************************************/
    
#ifndef CCVM_CONFIG_H

#error Configuration file 'ccvm_config.h' is missing

#else
    
#ifndef config_VM_MEMORY_SIZE
#warning "VM code memory size undefined. Using default set to 4KB."
#define config_VM_MEMORY_SIZE   (4*1024)        /**< Default code memory allocation = 4KB) */
#endif
#ifndef config_VM_STACK_SIZE
#warning "VM stack memory size undefined. Using default set to 1KB."
#define config_VM_STACK_SIZE    (1*1024)        /**< Default stack memory allocation = 1KB) */
#endif

#endif /* #ifndef CCVM_CONFIG_H */

/******************************************************************************/
/* Definitions */
/******************************************************************************/

#define VM_RT_VERSION       00000002                /**< Runtime version. Format: 11222333 (major 11, minor 222, build 333) */
#define VM_RT_VERSION_MIN   00000002                /**< Minimin runtime version. Format: 11222333 (major 11, minor 222, build 333) */
#define VM_MEMORY_SIZE      (config_VM_MEMORY_SIZE) /**< RAM allocated for VM Executable */
#define VM_STACK_SIZE       (config_VM_STACK_SIZE)  /**< RAM allocated for VM Stack */

/**
 * @brief   Status code
 * @details 0x0000 - 0x000F = Run State Status \n
 *          0x0010 - 0x00FF = Error Status \n
 *          0x0100 - 0x0200 = Stack Machine Error Status \n
 *          0x0200 - 0xFFFF = Reserved \n
 */  
typedef enum {
    /* Run State Status */
    S_VM_READY              = 0x0000,   /**< VM is ready for execution */
    S_VM_EXECUTING          = 0x0001,   /**< VM is currently running */
    S_VM_DONE               = 0x0002,   /**< VM has been terminated */
    S_VM_STOP               = 0x0003,   /**< VM has been stopped */
    /* Error Status */
    S_VM_ENTRYPOINT         = 0x0010,   /**< Entrypoint error */
    S_VM_MALLOC             = 0x0011,   /**< Error trying to malloc for VM */
    S_VM_OPENFILE           = 0x0012,   /**< Error opening binary file */
    S_VM_NOTFIT             = 0x0013,   /**< Binary file cannot fit into RAM */
    S_VM_READFILE           = 0x0014,   /**< Binary file could not be read */
    S_VM_FUNCID             = 0x0015,   /**< Invalid user function */
    S_VM_FUNCARG            = 0x0016,   /**< Invalid user function arguments */
    S_VM_VERSION_OLD        = 0x0017,   /**< Executable version is too old */
    S_VM_VERSION_NEW        = 0x0018,   /**< Executable version is too new */
    S_VM_INVALID_CHECKSUM   = 0x0019,   /**< Invalid executable file checksum */
    /* Stack Machine Error Status */
    S_VM_INVALID_OPCODE     = 0x0100,   /**< An invalid opcode was read */
    S_VM_MEMORY_RANGE       = 0x0101,   /**< Code memory access was out of range */
    S_VM_STACK_RANGE        = 0x0102,   /**< Stack access was out of range */
    S_VM_STACK_OVERFLOW     = 0x0103,   /**< Stack overflow error */
    S_VM_STACK_CONFIG       = 0x0104,   /**< Stack configuration unexpected */
}VM_STATUS_CODE;

/**
 * @brief   This structure holds the machine state and helper functions used by
 *          the VM.
 * @details 0x0000 - 0x000F = Run State Status \n
 *          0x0010 - 0x00FF = Error Status \n
 *          0x0100 - 0x0200 = Stack Machine Error Status \n
 *          0x0200 - 0xFFFF = Reserved \n
 */ 
struct ccvm_s
{
	/*@{*/
    uint16_t ip;        /**< Instruction pointer */
    uint16_t sp;        /**< Stack pointer */
    uint16_t sfp;       /**< Stack frame pointer */
    uint16_t max_sp;    /**< Maximum stack pointer value */
    uint16_t error;     /**< Stack machine error status */
	/*@}*/
	/**
     * @name Helper Functions
     */
    /*@{*/    
    int32_t (*mem_read)(struct ccvm_s *vm, uint16_t addr, uint8_t size);                /**< Helper function for reading VM Code Memory */
    
    void (*mem_write)(struct ccvm_s *vm, uint16_t addr, int32_t value, uint8_t size);   /**< Helper function for writing to VM Code Memory */
    
    int32_t (*stack_read)(struct ccvm_s *vm, uint16_t addr);                            /**< Helper function for reading VM Stack */
	
    void (*stack_write)(struct ccvm_s *vm, uint16_t addr, int32_t value);               /**< Helper function for writing to VM Stack */
	
    int32_t (*call_user)(struct ccvm_s *vm, uint16_t funcid, uint8_t argc,
                         int32_t *argv);                                                /**< Helper function for calling user functions */
    /*@}*/
};

/**
 * @brief   Binary file header 
 * @details This structure holds VM binary executable file header. The structure
 *          size should be no larger than 64 bytes.
 */ 
typedef struct
{
    uint32_t vm_version;        /**< VM Version */
    uint32_t script_version;    /**< Script File Version */
    uint32_t entry_point;       /**< Entry point address */
    uint32_t header_CRC32E;     /**< Header CRC-32 */
    uint32_t content_CRC32E;    /**< Content CRC-32 */
    uint8_t  reserved[44];      /**< Reserved for future use */
}CCVM_BIN_HEADER;
STRUCT_SIZE_CHECK(CCVM_BIN_HEADER,64);  /**< CCVM_BIN_HEADER size check, must be 64-bytes */

void ccvm_exec(struct ccvm_s *vm);
void ccvm_init(struct ccvm_s *vm, uint16_t addr);

int32_t mem_read(struct ccvm_s *vm, uint16_t addr, uint8_t size);
void mem_write(struct ccvm_s *vm, uint16_t addr, int32_t value, uint8_t size);
int32_t stack_read(struct ccvm_s *vm, uint16_t addr);
void stack_write(struct ccvm_s *vm, uint16_t addr, int32_t value);

int32_t ccvm_pop(struct ccvm_s *vm);
void ccvm_push(struct ccvm_s *vm, int32_t value);
int32_t ccvm_local_read(struct ccvm_s *vm, int8_t sfa);
void ccvm_local_write(struct ccvm_s *vm, int8_t sfa, int32_t value);

uint8_t vm_execute(char* filename, uint8_t argc, int32_t *argv, int32_t *ret);
int32_t ccvm_user_function(struct ccvm_s *vm, uint16_t funcid, uint8_t argc, 
                           int32_t *argv);

/******************************************************************************/
/* VM Standard Function Macros */
/******************************************************************************/

#define VM_MEMREAD_CHAR(addr)   (char)mem_read(vm,addr,1)     /**< Get Char from VM Memory */
#define VM_MEMREAD_U8(addr)     (uint8_t)mem_read(vm,addr,1)  /**< Get U8 from VM Memory */
#define VM_MEMREAD_S8(addr)     (int8_t)mem_read(vm,addr,1)   /**< Get S8 from VM Memory */
#define VM_MEMREAD_U16(addr)    (uint16_t)mem_read(vm,addr,2) /**< Get U16 from VM Memory */
#define VM_MEMREAD_S16(addr)    (int16_t)mem_read(vm,addr,2)  /**< Get S16 from VM Memory */
#define VM_MEMREAD_U32(addr)    (uint32_t)mem_read(vm,addr,4) /**< Get U32 from VM Memory */
#define VM_MEMREAD_S32(addr)    (int32_t)mem_read(vm,addr,4)  /**< Get S32 from VM Memory */

#define VM_MEMWRITE_CHAR(val,addr)  mem_write(vm,addr,(char)val,1)       /**< Write Char to VM Memory */
#define VM_MEMWRITE_U8(val,addr)    mem_write(vm,addr,(uint8_t)val,1)    /**< Write U8 to VM Memory */
#define VM_MEMWRITE_S8(val,addr)    mem_write(vm,addr,(int8_t)val,1)     /**< Write S8 to VM Memory */
#define VM_MEMWRITE_U16(val,addr)   mem_write(vm,addr,(uint16_t)val,2)   /**< Write U16 to VM Memory */
#define VM_MEMWRITE_S16(val,addr)   mem_write(vm,addr,(int16_t)val,2)    /**< Write S16 to VM Memory */
#define VM_MEMWRITE_U32(val,addr)   mem_write(vm,addr,(uint32_t)val,4)   /**< Write U32 to VM Memory */
#define VM_MEMWRITE_S32(val,addr)   mem_write(vm,addr,(int32_t)val,4)    /**< Write S32 to VM Memory */

#define VM_PTR_CHAR(addr)       (char*)&vm_memory[addr]         /**< Get Char* from VM Memory */
#define VM_PTR_U8(addr)         (uint8_t*)&vm_memory[addr]      /**< Get U8* from VM Memory */
#define VM_PTR_S8(addr)         (int8_t*)&vm_memory[addr]       /**< Get S8* from VM Memory */
#define VM_PTR_U16(addr)        (uint16_t*)&vm_memory[addr]     /**< Get U16* from VM Memory */
#define VM_PTR_S16(addr)        (int16_t*)&vm_memory[addr]      /**< Get S16* from VM Memory */
#define VM_PTR_U32(addr)        (uint32_t*)&vm_memory[addr]     /**< Get U32* from VM Memory */
#define VM_PTR_S32(addr)        (int32_t*)&vm_memory[addr]      /**< Get S32* from VM Memory */

#define VM_MEMREAD(dest,src,size)   for(int i=0;i<size;i++) dest[i]=VM_MEMREAD_U8(src+i)    /* Memread for CCVM */
#define VM_MEMWRITE(src,dest,size)  for(int i=0;i<size;i++) VM_MEMWRITE_U8(src[i],dest+i)   /* Memwrite for CCVM */

#ifdef __cplusplus
}
#endif

#endif
