/*
 *  CCVM - Common Code Virtual Machine
 *
 *  Copyright 2014 SCT Performance, LLC
 * 
 */

/**
 * @file    ccvm_funcs_obd2services.c
 * @brief   Virtual machine OBD2 functions
 * @details This file handles OBD2 functions used in CCVM.
 * @date    10/15/2014 
 * @author  Tristen Pierson
 */

#include "ccvm.h"
#include <stdio.h>
#include <string.h>
#include <stdint.h>
#include "ccvm_funcs.h"

/******************************************************************************/
/* Project Specific Includes */
/******************************************************************************/

#include <common/obd2service.h>

/******************************************************************************/
/* Global Variables */
/******************************************************************************/

extern uint8_t *vm_memory;  /* from ccvm_engine.c */
extern int32_t *vm_argv;    /* from ccvm_engine.c */
extern uint8_t vm_argc;     /* from ccvm_engine.c */
extern int32_t vm_ret;      /* from ccvm_engine.c */

/******************************************************************************/
/* CCVM OBD2 Functions */
/******************************************************************************/

/**
 * @brief       <B>VM Function:</B> int32_t obd2_read_memory_by_address(flags, 
 *              address, length, &data, &datalength)
 * @details     This function requests memory data from the server via starting
 *              address and size of memory to read.
 *
 * @param[in,out]    vm      Virtual machine state structure
 * @param[in]        argc    Argument count
 * @param[in]        argv    Argument list
 *
 * @return      Entry point argument count
 */ 
VM_FUNC_DEF(vm_obd2_read_memory_by_address)
{
    int32_t ret = 0;
    uint32_t ecu_id;
    uint32_t address;
    uint32_t length;
    uint32_t vmpData;
    uint32_t vmpDatalength;
    uint16_t datalength;    
    
    if (argc == 5)
    {
        ecu_id = argv[0];
        address = argv[1];
        length = argv[2];
        vmpData = argv[3];
        vmpDatalength = argv[4];

        memset(&vm_memory[vmpData],0x00,length);
    
        ret = obd2service_ReadMemoryByAddress(ecu_id, address, length, 
                                              &vm_memory[vmpData], &datalength);            
        
        VM_MEMWRITE_S32(datalength, vmpDatalength);
    }
    else
    {
        vm->error = S_VM_FUNCARG;
    }
    return ret;
}
