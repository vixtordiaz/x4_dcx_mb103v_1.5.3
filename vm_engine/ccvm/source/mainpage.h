/** 
 * @file	mainpage.h
 * 
 * @brief	Main Page
 * 
 * @author	Tristen Pierson
 * 
 * @date	07/14/2014
 *  
 * @details	This file contains the Doxygen Main Page for this source code
 * 			distribution.
 */

/** 
 * @mainpage 	Common Code Virtual Machine
 * @author		Tristen Pierson 
 * @version		1
 * @date		07/14/2014
 * 
 * <B>About:</B>
 *
 * This software distribution contains the common code virtual machine (CCVM).
 * CCVM is a 32-bit stack machine which can address up to 64K of memory. For
 * details on the VM engine or programming language specifications, click 
 * <a href="https://atlas.sctflash.net:8090/pages/viewpage.action?pageId=4718890">
 * here</a>.  
 * 
 * <B> Implementation: </B>
 *
 * Implementing the VM engine is simple. Just include vm_engine/ccvm/ccvm.h 
 * and call vm_execute(). Here is an example:
 * <CODE>
 *
 *      #include <vm_engine/ccvm/ccvm.h>\n\n
 *
 *      void main() {
 *
 *          int32_t ret;
 *          int32_t argv[10] = {'1','2','3','4','5','6','7','8','9','0'};
 *          uint8_t argc = 10;
 *          uint8_t status;
 *   
 *          status = vm_execute("\\USER\\test_entryargs.bin",argc,argv,&ret);
 *      }
 *
 * </CODE>
 *
 * <B> Customization: </B>
 * 	 
 * CCVM is customized using a configuration file called ccvm_config.h. Every
 * CCVM engine application must have a ccvm_config.h header file in its
 * preprocessor include path. ccvm_config.h tailors the virtual machine to the
 * host application. It is therefore specific to the firmware build, not the
 * VM engine, and should be located in an application directory, not in the
 * CCVM code directory. \n\n
 *
 * Here is a typical ccvm_config.h definition:
 *
 * <CODE>
 *
 *      #ifndef CCVM_CONFIG_H
 *      #define CCVM_CONFIG_H
 *
 *      #define configMEMORY_SIZE  (4*1024)
 *      #define configSTACK_SIZE   (1*1024)
 *
 *      #endif // CCVM_CONFIG_H //
 * 	
 * </CODE>
 *
 * <B> User Defined Functions: </B>
 *
 * The most important aspect of this VM is its ability to easily define user
 * functions which is what makes CCVM so flexible. Functions are defined in 
 * lexer.l (ccvmc, compiler) and ccvm_funcs.h. They are then implemented in
 * ccvm_funcs.c. Therefore, if functions are to be added to the VM, the compiler
 * must also have them added; this means the compiler must be updated and
 * rebuilt before compiling new CCVM executables. The following is an example of
 * how to define and implement several user functions:\n\n
 *  
 * Compiler definition (ccvmc project, lexer.l):
 *
 * <CODE>
 *
 *      "oldfunc"   { yylval.number = 0x000F; return TOK_USERFUNC; }
 *      "newfunc"   { yylval.number = 0x0010; return TOK_USERFUNC; }
 * 
 * </CODE>  
 *
 * <B><I>"oldfunc"</I></B> and <B><I>"newfunc"</I></B>: Literal text the lexer
 *      matches.\n
 * <B><I>yylval.number</I></B>: In this context, yylval.number represents 
 *      a function id the lexer passes to the parser. This number must match the
 *      number defined by VM_FUNCS found in ccvm_funcs.h.\n 
 * <B><I>TOK_USERFUNC</I></B>: This is a token the lexer returns to the parser
 *      which identifys the matched text as a function.\n\n
 *
 * VM definition (ccvm_funcs.h):
 *
 * <CODE>
 *
 *      typedef enum
 *      {
 *          VM_FUNC_OLDFUNC     = 0x000F,
 *          VM_FUNC_NEWFUNC     = 0x0010,
 *      }VM_FUNCS;
 *
 * </CODE>
 * 
 * VM implementation (ccvm_funcs.c):
 *
 * <CODE>
 *
 *      int32_t ccvm_user_function(struct ccvm_s *vm, uint16_t funcid, uint8_t argc, 
 *                                 int32_t *argv)
 *      {
 *          int32_t ret = 0;
 *          switch (funcid)
 *          {
 *              case VM_FUNC_OLDFUNC:
 *                  // Do something
 *                  ret = 1;
 *                  break;
 *              case VM_FUNC_NEWFUNC:
 *                  // Do something else
 *                  ret = 1;
 *                  break;
 *          }
 *          return ret;
 *      }
 *
 * </CODE>
 *
 * <B>Runtime Version:</B> 
 * 
 * If changes are made to CCVM (including adding a user 
 * function), the CCVM runtime version must be incremented in the compiler and 
 * VM. The runtime version is used to control which versions of compiled code 
 * should run on the VM. The runtime version for the VM Engine is found in 
 * ccvm.h. Likewise the target runtime version for the compiler is found in
 * ccvmc.h. VM_RT_VERSION_MIN is the minimum version the VM Engine will support.
 * VM_RT_VERSION is the current version. The Engine will not run executables
 * if the version is less than VM_RT_VERSION_MIN or greater tha VM_RT_VERSION.
 * Version format is: 11222333 where major = 11, minor = 222, and build = 333.
 */