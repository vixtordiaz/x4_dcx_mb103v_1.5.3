/*
 *  CCVM - Common Code Virtual Machine
 *
 *  Copyright 2014 SCT Performance, LLC
 *
 */

/**
 * @file    ccvm_config.h
 * @brief   CCVM configuration file
 * @details This file contains configuration definitions used in CCVM.
 * @date    7/14/2014 
 * @author  Tristen Pierson
 */ 

#ifndef CCVM_CONFIG_H
#define CCVM_CONFIG_H

/******************************************************************************/
/* Project Specific Includes */
/******************************************************************************/

#include <common/statuscode.h>
#include <common/crc32.h>
#include <common/crypto_blowfish.h>
#include <common/debug/debug_output.h>
#include <fs/genfs.h>

/******************************************************************************/
/* VM Debug Macros */
/******************************************************************************/

#ifdef __DEBUG_JTAG_
#define __VM_VERBOSE        /**< VM Verbose Mode, requires VM_PRINT */
#endif

/******************************************************************************/
/* VM Memory Settings */
/******************************************************************************/

#define config_VM_MEMORY_SIZE       (4*1024)    /**< VM Code Memory Size)  */
#define config_VM_STACK_SIZE        (1*1024)    /**< VM Stack Memory Size) */

/******************************************************************************/
/* VM Project Specific Function Macros */
/******************************************************************************/

#define VM_PRINT(str)           debug_out(str)  /**< Print function */
#define VM_MALLOC(size)         __malloc(size)  /**< Malloc function */
#define VM_FREE(ptr)            __free(ptr);    /**< Free function */

/**
 * @brief       VM Data Checksum
 *
 * @details     This function performs a checksum on virtual machine data. 
 *              CCVM requires the checksum be CRC32 Ethernet.
 *
 * @param[in]   seed        Checksum seed
 * @param[in]   data        Data
 * @param[in]   length      Data length
 *
 * @return      Status
 */
#define VM_CHECKSUM(seed,data,length) \
    crc32e_sw_calculateblock(seed,data,length); /**< Checksum function */

/**
 * @brief       Decrypt VM Data
 *
 * @details     This function decrypts virtual machine data.
 *
 * @param[in]   data        Data
 * @param[in]   length      Data length
 *
 * @return      Status
 */
#define VM_DECRYPT(data,length) \
    crypto_blowfish_decryptblock_critical(data,length);
    
/******************************************************************************/
/* VM Default Object Macros */
/******************************************************************************/

#define VM_FILE                 F_FILE          /**< File object */

#endif /* CCVM_CONFIG_H */