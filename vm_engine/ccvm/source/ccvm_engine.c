/*
 *  CCVM - Common Code Virtual Machine
 *
 *  Original work Copyright (C) 2011 Clifford Wolf <clifford@clifford.at>
 *  Modified work Copyright 2014 SCT Performance, LLC
 *
 *  Permission to use, copy, modify, and/or distribute this software for any
 *  purpose with or without fee is hereby granted, provided that the above
 *  copyright notice and this permission notice appear in all copies.
 *  
 *  THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
 *  WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
 *  MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
 *  ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
 *  WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
 *  ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
 *  OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
 *
 */

/**
 * @file    ccvm_engine.c
 * @brief   Virtual machine frontend
 * @details This file handles stack machine initialization, memory management, 
 *          and user function calls. This file also manages error handling,
 *          file checking, and entry point argument validation.
 * @date    7/11/2014 
 * @author  Tristen Pierson
 */ 

#include "ccvm.h"
#include <stdio.h>
#include <string.h>
#include <stdint.h>
#include <stdbool.h>
#include "ccvm_funcs.h"

/******************************************************************************/
/* Global Variables */
/******************************************************************************/

uint8_t *vm_memory;  /**< VM code memory pointer */
int32_t *vm_argv;    /**< Argument list for executable's entry point */
uint8_t vm_argc;     /**< Argument count for executable's entry point */
int32_t vm_ret;      /**< Executable's return value upon termination */

/******************************************************************************/
/* Static Variables */
/******************************************************************************/

static bool stop;           /**< Forces the VM to terminate execution */

/******************************************************************************/
/* Static Functions */
/******************************************************************************/

/**
 * @brief       Memory read
 * @details     This function reads a value from VM code memory. The address
 *              is checked to make sure it is within VM code memory range.
 *
 * @param[in,out]   vm      Virtual machine state structure
 * @param[in]       addr    Memory address
 * @param[in]       size    Bytes to read
 *
 * @return      Data read
 */ 
int32_t mem_read(struct ccvm_s *vm, uint16_t addr, uint8_t size)
{
	int32_t value;
    
    if (addr >= VM_MEMORY_SIZE + VM_STACK_SIZE)
    {
        vm->error = S_VM_MEMORY_RANGE;
        return 0;
    }
    switch (size)
    {
        case 4:
        case 3:
            value = (vm_memory[addr+0] << 24) | (vm_memory[addr+1] << 16) |
                    (vm_memory[addr+2] << 8) | vm_memory[addr+3];
            break;
        case 2:
            value = (vm_memory[addr+0] << 8) | vm_memory[addr+1];
            break;
        case 1:
        default:
            value = vm_memory[addr];
            break;
    }
    return value;
}

/**
 * @brief       Memory write
 * @details     This function writes a value into VM code memory. The address
 *              is checked to make sure it is within VM code memory range.
 *
 * @param[in,out]   vm      Virtual machine state structure
 * @param[in]       addr    Memory address
 * @param[in]       value   Data to write
 * @param[in]       size    Memory size in bytes (1, 2, or 4)
 */ 
void mem_write(struct ccvm_s *vm, uint16_t addr, int32_t value, uint8_t size)
{
    if (addr >= VM_MEMORY_SIZE + VM_STACK_SIZE)
    {
        vm->error = S_VM_MEMORY_RANGE;
        return;
    }
    switch (size)
    {
        case 4:
        case 3:
            vm_memory[addr+0] = value >> 24;
            vm_memory[addr+1] = value >> 16;
            vm_memory[addr+2] = value >> 8;
            vm_memory[addr+3] = value;
            break;
        case 2:
            vm_memory[addr+0] = value >> 8;
            vm_memory[addr+1] = value;
            break;
        case 1:
        default:
            vm_memory[addr] = value;
            break;
    }
}

/**
 * @brief       Stack read
 * @details     This function reads a value from the VM stack. The address
 *              is checked to make sure it is within stack memory range.
 *
 * @param[in,out]   vm      Virtual machine state structure
 * @param[in]       addr    Memory address
 *
 * @return      Data read
 */ 
int32_t stack_read(struct ccvm_s *vm, uint16_t addr)
{
	if (addr >= VM_MEMORY_SIZE + VM_STACK_SIZE ||
        addr < VM_MEMORY_SIZE)
    {
        vm->error = S_VM_STACK_RANGE;
        return 0;
    }
    return (vm_memory[addr+0] << 24) | (vm_memory[addr+1] << 16) | 
           (vm_memory[addr+2] << 8) | vm_memory[addr+3];
}

/**
 * @brief       Stack write
 * @details     This function writes a value to the VM stack. The address
 *              is checked to make sure it is within stack memory range.
 *
 * @param[in,out]   vm      Virtual machine state structure
 * @param[in]       addr    Memory address
 * @param[in]       value   Data to write
 */ 
void stack_write(struct ccvm_s *vm, uint16_t addr, int32_t value)
{
    if (addr >= VM_MEMORY_SIZE + VM_STACK_SIZE ||
        addr < VM_MEMORY_SIZE)
    {
        vm->error = S_VM_STACK_RANGE;
        return;
    }
    vm_memory[addr+0] = value >> 24;
    vm_memory[addr+1] = value >> 16;
    vm_memory[addr+2] = value >> 8;
    vm_memory[addr+3] = value;
}

/**
 * @brief       Call user function
 * @details     This function handles calling of user functions. All functions
 *              must be defined in the CCVM lexer. Invalid functions will throw
 *              a VM error.
 *
 * @param[in,out]   vm      Virtual machine state structure
 * @param[in]       funcid  Virtual machine state structure
 * @param[in]       argc    Argument count
 * @param[in]       argv    Argument list
 *
 * @return      Function return data
 */ 
static int32_t call_user(struct ccvm_s *vm, uint16_t funcid, uint8_t argc, 
                         int32_t *argv)
{
	int32_t ret = 0;    
    
#ifndef CCVM_FUNCS_H
#error "User functions file 'ccvm_funcs.h' is missing"
#endif
        
    ret = ccvm_user_function(vm, funcid, argc, argv);
    vm_ret = ret;
    return ret;
}

/******************************************************************************/
/* Structures */
/******************************************************************************/

/**
 * @brief   Machine state and helper functions structure initialization
 * @details Structure initial state: \n
 *          IP = VM_MEMORY_SIZE - 1 \n
 *          SP = 0x00 \n
 *          SFP = 0x00 \n
 *          error = S_VM_READY \n
 *          (*mem_read)(...) = mem_read(...) \n
 *          (*mem_write)(...) = mem_write(...) \n
 *          (*stack_read)(...) = stack_read(...) \n
 *          (*stack_write)(...) = stack_write(...) \n
 *          (*call_user)(...) = call_user(...) \n
 */ 
struct ccvm_s vm = {
	(VM_STACK_SIZE + VM_MEMORY_SIZE - 1), 0, 0, 0xFFFF, S_VM_READY,
	&mem_read, &mem_write, &stack_read, &stack_write, &call_user
};

/******************************************************************************/
/* Functions */
/******************************************************************************/

/**
 * @brief       VM execute
 * @details     This function handles VM initialization, execution, and 
 *              termination. This function checks entrypoint arguments, memory,
 *              and executable program binary. During execution, this function
 *              calls one opcode at a time and checks for errors each
 *              iteration. When a program terminates, status is checked to see
 *              if termination was due to error or expected.
 *
 * @param[in]   filename    Filename of executable program binary
 * @param[in]   argc        Argument count
 * @param[in]   argv        Argument list
 * @param[out]  ret         Return status of executed program
 *
 * @return      Final VM status
 */
uint8_t vm_execute(char* filename, uint8_t argc, int32_t *argv, int32_t *ret)
{
	CCVM_BIN_HEADER bin_header;
    VM_FILE *pFile;
	uint16_t status;
    uint32_t size;
    uint8_t  i;
    
#ifdef __VM_VERBOSE
    uint8_t temp[256];
#endif

    *ret = NULL;
	status = S_VM_READY;
    
    // Check VM Entry Point Arguments
    if ((argc > 0 && argv == NULL) || ret == NULL)
    {
        status = S_VM_ENTRYPOINT;
    }    
    // Malloc VM Entry Point Arguments
    if (status == S_VM_READY)
    {
        vm_argv = VM_MALLOC(argc*sizeof(int32_t));
        if (vm_argv == NULL)
        {
            status = S_VM_ENTRYPOINT;
        }
    }
    // Copy VM Entry Point Arguments
    if (status == S_VM_READY)
    {
        vm_argc = argc;
        for (i = 0; i < argc; i++)
        {
            vm_argv[i] = argv[i];
        }
    }
    // Malloc VM Memory Space
    vm_memory = VM_MALLOC(VM_MEMORY_SIZE + VM_STACK_SIZE);
    if (vm_memory == NULL)
    {
        status = S_VM_MALLOC;
    }
    // Open file
    if (status == S_VM_READY)
    {
        pFile = fopen(filename, "rb");
        if (!pFile)
        {
            status = S_VM_OPENFILE;
        }
    }
    // Check file size
    if (status == S_VM_READY)
    {
        fseek (pFile, 0, SEEK_END);
        size = ftell (pFile) - sizeof(bin_header);
        fseek (pFile, 0, SEEK_SET);
        if (size > VM_MEMORY_SIZE)
        {
            status = S_VM_NOTFIT;
        }
        else if (size <= sizeof(bin_header))
        {
            status = S_VM_READFILE;
        }
    }
    // Read binary file header
    if (status == S_VM_READY)
    {
        if (fread (&bin_header,1,sizeof(bin_header),pFile) != sizeof(bin_header))
        {
            status = S_VM_READFILE;
        }
    }
    // Decrypt binary file header
    if (status == S_VM_READY)
    {
        status = VM_DECRYPT((u8*)&bin_header,sizeof(bin_header));
    }
    // Validate binary file header checksum
    if (status == S_VM_READY)
    {
        uint32_t header_checksum = bin_header.header_CRC32E;
        bin_header.header_CRC32E = 0xFFFFFFFF;
        bin_header.header_CRC32E = VM_CHECKSUM(0xFFFFFFFF, 
                                        (uint32_t*)&bin_header,sizeof(bin_header)/4);
        if (bin_header.header_CRC32E != header_checksum)
        {
            status = S_VM_INVALID_CHECKSUM;
        } 
    }
    // Validate binary file version
    if (status == S_VM_READY)
    {
        if (bin_header.vm_version < VM_RT_VERSION_MIN)
        {
            status = S_VM_VERSION_OLD;
        }
        else if (bin_header.vm_version > VM_RT_VERSION)
        {
            status = S_VM_VERSION_NEW;
        }
    }
    // Load into memory
    if (status == S_VM_READY)
    {
        memset(vm_memory, 0, VM_MEMORY_SIZE + VM_STACK_SIZE);
        if (fread (vm_memory,1,size,pFile) != size)
        {
            status = S_VM_READFILE;
        }
        if (status == S_VM_READY)
        {
            // Decrypt contents
            status = VM_DECRYPT(vm_memory,size);
        }
    }
    // Validate binary file content checksum
    if (status == S_VM_READY)
    {
        uint32_t content_checksum;        
        content_checksum = VM_CHECKSUM(0xFFFFFFFF,(uint32_t*)&vm_memory[0],size/4);
        if (bin_header.content_CRC32E != content_checksum)
        {
            status = S_VM_INVALID_CHECKSUM;
        } 
    }
    
    // Close file
    fclose(pFile);

    // Start VM session
    if (status == S_VM_READY)
    {
        // Initialize the VM
        ccvm_init(&vm, (uint16_t)bin_header.entry_point);
        status = S_VM_EXECUTING;
        stop = false;
        
        // Begin executing the VM application
        while (!stop) {
            // Check for errors
            if (vm.error)
            {
#ifdef __VM_VERBOSE
                switch (vm.error)
                {
                case S_VM_MEMORY_RANGE:
                    VM_PRINT("ERROR: Illegal Memory Access => Terminating");
                    break;
                case S_VM_STACK_RANGE:
                    VM_PRINT("ERROR: Illegal Stack Access => Terminating");
                    break;
                case S_VM_INVALID_OPCODE:
                    VM_PRINT("ERROR: Invalid Opcode => Terminating");
                    break;
                case S_VM_STACK_OVERFLOW:
                    VM_PRINT("ERROR: Stack Overflow => Terminating");
                    break;
                case S_VM_STACK_CONFIG:
                    VM_PRINT("ERROR: Unexpected stack configuration => Terminating");
                    break;
                case S_VM_STOP:
                    VM_PRINT("Called user stop => stop");
                    break;
                default:
                    VM_PRINT("ERROR: Terminating");
                    break;
                }
                sprintf((char*)temp, "IP=%04x, SP=%04x, SFP=%04x", vm.ip, vm.sp, vm.sfp);
                VM_PRINT(temp);
#endif                
                status = vm.error;
                if (status == S_VM_STOP)
                {
                    stop = true;
                }
                break;
            }
            // Is program terminated?
            if (vm.ip == VM_STACK_SIZE + VM_MEMORY_SIZE - 1) {
#ifdef __VM_VERBOSE
                VM_PRINT("Main function returned => Terminating");
#endif
                // Is there an unexpected stack configuration
                if (vm.sp != 0 || vm.sfp != 0)
                {
#ifdef __VM_VERBOSE
                    sprintf((char*)temp, "WARNING: Unexpected stack configuration on program exit: ");
                    VM_PRINT(temp);
                    sprintf((char*)temp, "SP=%04x, SFP=%04x", vm.sp, vm.sfp);
                    VM_PRINT(temp);
#endif
                    status = S_VM_STACK_CONFIG;
                }
                // Stack configuration is expected
                else
                {
                    // Get return value from VM application
                    *ret = vm_ret;
                    status = S_VM_DONE;
                }
                break;
            }
            // Execute line
            ccvm_exec(&vm);
        } // while(..
        if (stop == true)
        {
            status = S_VM_STOP;
        }
    }

    // Garbage collection
    VM_FREE(vm_memory);
    VM_FREE(vm_argv);
    
    return status;
}

