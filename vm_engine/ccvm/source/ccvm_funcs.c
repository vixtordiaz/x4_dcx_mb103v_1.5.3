/*
 *  CCVM - Common Code Virtual Machine
 *
 *  Copyright 2014 SCT Performance, LLC
 * 
 */

/**
 * @file    ccvm_funcs.c
 * @brief   Virtual machine user functions
 * @details This file handles user defined functions used in CCVM.
 * @date    7/14/2014 
 * @author  Tristen Pierson
 */

#include "ccvm.h"
#include <stdio.h>
#include <string.h>
#include <stdint.h>
#include "ccvm_funcs.h"

/******************************************************************************/
/* Project Specific Includes */
/******************************************************************************/

#include <board/delays.h>
#include <board/rtc.h>

/******************************************************************************/
/* Global Variables */
/******************************************************************************/

extern uint8_t *vm_memory;  /* from ccvm_engine.c */
extern int32_t *vm_argv;    /* from ccvm_engine.c */
extern uint8_t vm_argc;     /* from ccvm_engine.c */
extern int32_t vm_ret;      /* from ccvm_engine.c */

/******************************************************************************/
/* Local Prototypes: CCVM Standard Functions */
/******************************************************************************/

static VM_FUNC_DEF(vm_std_get_argc);
static VM_FUNC_DEF(vm_std_get_argv);
static VM_FUNC_DEF(vm_std_stop);
static VM_FUNC_DEF(vm_std_debug_out);
static int32_t vm_std_uf_out(struct ccvm_s *vm, uint16_t funcid, uint8_t argc, int32_t *argv);
static VM_FUNC_DEF(vm_std_delayms);
static VM_FUNC_DEF(vm_std_memset);
static VM_FUNC_DEF(vm_std_memcpy);
static VM_FUNC_DEF(vm_std_get_rtc);

/******************************************************************************/
/* User functions handler */
/******************************************************************************/

/**
 * @brief        Call user function
 * @details      This function handles calling of user functions. All functions
 *               must be defined in the CCVM lexer. Invalid functions will throw
 *               a VM error.
 *
 * @param[in,out]    vm      Virtual machine state structure
 * @param[in]        funcid  Function identifier
 * @param[in]        argc    Argument count
 * @param[in]        argv    Argument list
 *
 * @return       Function return data
 */ 
int32_t ccvm_user_function(struct ccvm_s *vm, uint16_t funcid, uint8_t argc, 
                           int32_t *argv)
{
    int32_t ret = 0;

    switch (funcid)
    {
    /**************************************************************************/
    /* CCVM Standard Functions */
    /**************************************************************************/  
    case VM_FUNC_GET_ARGC:
        VM_FUNC(vm_std_get_argc);
        break;
    case VM_FUNC_GET_ARGV:
        VM_FUNC(vm_std_get_argv);
        break;
    case VM_FUNC_STOP:
        VM_FUNC(vm_std_stop);
        break;
    case VM_FUNC_DEBUG_OUT:
        VM_FUNC(vm_std_debug_out);
        break;
    case VM_FUNC_UF_OUT:
        ret = vm_std_uf_out(vm, funcid, argc, argv);
        break;
    case VM_FUNC_DELAYMS:
        VM_FUNC(vm_std_delayms);
        break;
    case VM_FUNC_MEMSET:
        VM_FUNC(vm_std_memset);
        break;
    case VM_FUNC_MEMCPY:
        VM_FUNC(vm_std_memcpy);
        break;
    case VM_FUNC_GET_RTC:
        VM_FUNC(vm_std_get_rtc);
        break;
    /**************************************************************************/
    /* CCVM OBD2 Functions */
    /**************************************************************************/  
    case VM_FUNC_OBD2_READ_MEMORY_BY_ADDRESS:
        VM_FUNC(vm_obd2_read_memory_by_address);
        break;
    /**************************************************************************/
    /* CCVM Checksum Functions: */
    /**************************************************************************/
    case VM_FUNC_CHECKSUM_HANDLE_ADDITIVE:
        VM_FUNC(vm_checksum_handle_additive);
        break;
    case VM_FUNC_CHECKSUM_HANDLE_CRC32:
        VM_FUNC(vm_checksum_handle_crc32);
        break;
    case VM_FUNC_CHECKSUM_HANDLE_RSA_ME9UK:
        VM_FUNC(vm_checksum_handle_rsa_me9uk);
        break;
    case VM_FUNC_CHECKSUM_HANDLE_MD5:
        VM_FUNC(vm_checksum_handle_md5);
        break;
    case VM_FUNC_CHECKSUM_DECRYPT_RSA:
        VM_FUNC(vm_checksum_decrypt_rsa);
        break;
    /**************************************************************************/
    /* CCVM Filestock Functions: */
    /**************************************************************************/
    case VM_FUNC_FILESTOCK_READ:
        VM_FUNC(vm_filestock_read);
        break;
    case VM_FUNC_FILESTOCK_WRITE:
        VM_FUNC(vm_filestock_write);
        break;
    case VM_FUNC_FILESTOCK_UPDATEFLASHFILE:
        VM_FUNC(vm_filestock_updateflashfile);
        break;
    case VM_FUNC_FILESTOCK_FILL:
        VM_FUNC(vm_filestock_fill);
        break;
    /**************************************************************************/
    /* CCVM Tune Functions: */
    /**************************************************************************/        
    case VM_FUNC_TUNE_SET:
        VM_FUNC(vm_tune_set);
        break;
    case VM_FUNC_TUNE_GET:
        VM_FUNC(vm_tune_get);
        break;
    case VM_FUNC_TUNE_REPORT_PROGRESS:
        VM_FUNC(vm_tune_report_progress);
        break;
    /**************************************************************************/
    /* Invalid Function ID */
    /**************************************************************************/
    default:
        vm->error = S_VM_FUNCID;
        break;
    }
    
    return ret;
}

/******************************************************************************/
/* CCVM Standard Functions */
/******************************************************************************/

/**
 * @brief       <B>VM Function:</B> count get_argc(void)
 * @details     This function returns the application entry point argument
 *              count to the VM application.\n\n
 *
 *  <B>VM Returns:</B>
 *      - Entry point argument count
 *
 * @param[in,out]    vm      Virtual machine state structure
 * @param[in]        argc    Argument count
 * @param[in]        argv    Argument list
 *
 * @return      Entry point argument count
 */ 
static VM_FUNC_DEF(vm_std_get_argc)
{
    int32_t ret = 0;
    if (argc == 0)
    {
        ret = vm_argc;
    }
    else
    {
        vm->error = S_VM_FUNCARG;
    }
    return ret;
}

/**
 * @brief       <B>VM Function:</B> argument get_argv(index)
 * @details     This function returns the application entry point argument
 *              to the VM application.\n\n
 * 
 *  <B>VM Parameters:</B>
 *      - [in] index: Argument index
 *
 *              <B>VM Returns:</B>
 *                  - Entry point argument
 *
 * @param[in,out]    vm      Virtual machine state structure
 * @param[in]        argc    Argument count
 * @param[in]        argv    Argument list
 *
 * @return      Entry point argument
 */ 
static VM_FUNC_DEF(vm_std_get_argv)
{
    int32_t ret = 0;
    if (argc == 1 && argv != NULL)
    {
        ret = vm_argv[argv[0]];
    }
    else
    {
        vm->error = S_VM_FUNCARG;
    }
    return ret;
}

/**
 * @brief       <B>VM Function:</B> void stop(void)
 * @details     This function immediatly terminates the VM application. 
 *
 * @param[in,out]    vm      Virtual machine state structure
 * @param[in]        argc    Argument count
 * @param[in]        argv    Argument list
 *
 * @return      0
 */ 
static VM_FUNC_DEF(vm_std_stop)
{
    if (argc == 0)
    {
        vm->error = S_VM_STOP;
    }
    else
    {
        vm->error = S_VM_FUNCARG;
    }
    return 0;
}

/**
 * @brief       <B>VM Function:</B> void debug_out(*strAddr)
 * @details     This function prints a string referenced.\n\n
 *
 *  <B>VM Parameters:</B>
 *      - [in] strAddr: VM memory address containing a string
 *
 * @param[in,out]    vm      Virtual machine state structure
 * @param[in]        argc    Argument count
 * @param[in]        argv    Argument list
 *
 * @return      0
 */ 
static VM_FUNC_DEF(vm_std_debug_out)
{
    uint8_t temp[256];
    if (argc == 1 && argv != NULL)
    {
        sprintf((char*)temp,"%s",VM_PTR_CHAR(argv[0]));
        VM_PRINT((uint8_t*)temp);
    }
    else
    {
        vm->error = S_VM_FUNCARG;
    }
    return 0;
}

/**
 * @brief       <B>VM Function:</B> void uf_out(arg1 ... arg_n)
 * @details     This function prints the function id, number of arguments, and
 *              each argument.\n\n
 *
 *  <B>VM Parameters:</B>
 *      - [in] arg1: First argument
 *      - [in] ...
 *      - [in] argn: Last argument (if more than 1)
 *
 * @param[in,out]    vm      Virtual machine state structure
 * @param[in]        funcid  Function identifier 
 * @param[in]        argc    Argument count
 * @param[in]        argv    Argument list
 *
 * @return      0
 */ 
static int32_t vm_std_uf_out(struct ccvm_s *vm, uint16_t funcid, uint8_t argc, int32_t *argv)
{
    uint8_t i;
    uint8_t temp[256];
    
    sprintf((char*)temp, "Called user function %d with %d args:", funcid, argc);
    VM_PRINT(temp);
    memset(temp,0,sizeof(temp));
    for (i = 0; i < argc; i++) {
        sprintf((char*)temp, "%s %d", temp, argv[i]);
    }
    VM_PRINT(temp);
    return 0;
}

/**
 * @brief       <B>VM Function:</B> void delay(ms_value)
 * @details     This function delays for a number of milliseconds.
 *
 *  <B>VM Parameters:</B>
 *      - [in] ms_value: Delay value in milliseconds
 *
 * @param[in,out]    vm      Virtual machine state structure
 * @param[in]        argc    Argument count
 * @param[in]        argv    Argument list
 *
 * @return      0
 */ 
static VM_FUNC_DEF(vm_std_delayms)
{
    if (argc == 1 && argv != NULL)
    {
        delays(argv[0], 'm');
    }
    else
    {
        vm->error = S_VM_FUNCARG;
    }
    return 0;
}

/**
 * @brief       <B>VM Function:</B> void memset(*ptr, value, size)
 * @details     This function fills a block of memory
 *
 *  <B>VM Parameters:</B>
 *      - [in,out] ptr:   Pointer to block of memory to fill
 *      - [in]     value: Value to be set
 *      - [in]     size:  Number of nytes to be set to the value
 *
 * @param[in,out]    vm      Virtual machine state structure
 * @param[in]        argc    Argument count
 * @param[in]        argv    Argument list
 *
 * @return      0
 */ 
static VM_FUNC_DEF(vm_std_memset)
{    
    int32_t vmpPtr;
    int32_t value;
    int32_t size;       
    
    if (argc == 3 && argv != NULL)
    {
        vmpPtr = argv[0];
        value = argv[1];
        size = argv[2];
        
        if (vmpPtr + size < VM_MEMORY_SIZE + VM_STACK_SIZE)
        {
            memset(&vm_memory[vmpPtr], value, size);
        }
        else
        {
            vm->error = S_VM_MEMORY_RANGE;
        }
    }
    else
    {
        vm->error = S_VM_FUNCARG;
    }
    return 0;
}

/**
 * @brief       <B>VM Function:</B> void memcpy(*dest, *src, size)
 * @details     This function fills a block of memory
 *
 *  <B>VM Parameters:</B>
 *      - [in] dest:  Pointer to the destination array
 *      - [in] src:   Pointer to the source of data to be copied
 *      - [in] size:  Number of nytes to copy
 *
 * @param[in,out]    vm      Virtual machine state structure
 * @param[in]        argc    Argument count
 * @param[in]        argv    Argument list
 *
 * @return      0
 */ 
static VM_FUNC_DEF(vm_std_memcpy)
{    
    int32_t vmpDest;
    int32_t vmpSrc;
    int32_t size;    
    
    if (argc == 3 && argv != NULL)
    {
        vmpDest = argv[0];
        vmpSrc = argv[1];
        size = argv[2];
        
        if (vmpDest + size < VM_MEMORY_SIZE + VM_STACK_SIZE &&
            vmpSrc + size < VM_MEMORY_SIZE + VM_STACK_SIZE)
        {
            memcpy(&vm_memory[vmpDest], &vm_memory[vmpSrc], size);
        }
        else
        {
            vm->error = S_VM_MEMORY_RANGE;
        }
    }
    else
    {
        vm->error = S_VM_FUNCARG;
    }
    return 0;
}

/**
 * @brief       <B>VM Function:</B> int32 get_rtc()
 * @details     This function returns the current RTC value
 *
 * @param[in,out]    vm      Virtual machine state structure
 * @param[in]        argc    Argument count
 * @param[in]        argv    Argument list
 *
 * @return      0
 */ 
static VM_FUNC_DEF(vm_std_get_rtc)
{
    int32_t ret = 0;   
    
    if (argc == 0)
    {
        ret = rtc_getvalue();
    }
    else
    {
        vm->error = S_VM_FUNCARG;
    }
    return ret;
}