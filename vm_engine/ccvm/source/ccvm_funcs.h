/*
 *  CCVM - Common Code Virtual Machine
 *
 *  Copyright 2014 SCT Performance, LLC
 *
 */

/**
 * @file    ccvm_funcs.h
 * @brief   CCVM function definitions
 * @details This file contains definitions of user functions used in CCVM.
 * @date    7/14/2014 
 * @author  Tristen Pierson
 */ 

#ifndef CCVM_FUNCS_H
#define CCVM_FUNCS_H

#define VM_FUNC(f)      ret = f(vm, argc, argv)         /**< CCVM function macro */
#define VM_FUNC_DEF(f)  int32_t \
    f(struct ccvm_s *vm, uint8_t argc, int32_t *argv)   /**< CCVM function definition macro */
/**
 * @brief    User defined functions
 * @details  These functions are defined by the CCVM compiler. To define a new
 *           function, lexer.l must have function tokens created linking the
 *           funcid to the function name. An example:\n\n
 *
 * ccvm_funcs.h (ccvm, virtual machine):
 *
 * <CODE>
 *
 *      typedef enum
 *      {
 *          VM_FUNC_OLDFUNC     = 0x000F,
 *          VM_FUNC_NEWFUNC     = 0x0010,
 *      }VM_FUNCS;
 *
 * </CODE>
 *
 * lexer.l (ccvmc, compiler):
 *
 * <CODE>
 *
 *      "oldfunc"   { yylval.number = 0x000F; return TOK_USERFUNC; }
 *      "newfunc"   { yylval.number = 0x0010; return TOK_USERFUNC; }
 * 
 * </CODE>
 */  
typedef enum
{
    /**************************************************************************/
    /* CCVM Standard Functions: 0x0000 - 0x00FF */
    /**************************************************************************/ 
    VM_FUNC_GET_ARGC        = 0x0000,
    VM_FUNC_GET_ARGV        = 0x0001,
    VM_FUNC_STOP            = 0x0002,
    VM_FUNC_DEBUG_OUT       = 0x0003,
    VM_FUNC_UF_OUT          = 0x0004,
    VM_FUNC_DELAYMS         = 0x0005,
    VM_FUNC_MEMSET          = 0x0006,
    VM_FUNC_MEMCPY          = 0x0007,
    VM_FUNC_GET_RTC         = 0x0008,
    /**************************************************************************/
    /* CCVM OBD2 Functions: 0x0100 - 0x01FF */
    /**************************************************************************/  
    VM_FUNC_OBD2_READ_MEMORY_BY_ADDRESS = 0x0100,
    
    /**************************************************************************/
    /* CCVM Checksum Functions: 0x0200 - 0x02FF */
    /**************************************************************************/
    VM_FUNC_CHECKSUM_HANDLE_ADDITIVE    = 0x0200,
    VM_FUNC_CHECKSUM_HANDLE_CRC32       = 0x0201,
    VM_FUNC_CHECKSUM_HANDLE_RSA_ME9UK   = 0x0202,
    VM_FUNC_CHECKSUM_HANDLE_MD5         = 0x0203,
    VM_FUNC_CHECKSUM_DECRYPT_RSA        = 0x0204,
    /**************************************************************************/
    /* CCVM Filestock Functions: 0x0300 - 0x03FF */
    /**************************************************************************/
    VM_FUNC_FILESTOCK_READ              = 0x0300,
    VM_FUNC_FILESTOCK_WRITE             = 0x0301,
    VM_FUNC_FILESTOCK_UPDATEFLASHFILE   = 0x0302,
    VM_FUNC_FILESTOCK_FILL              = 0x0303,
    /**************************************************************************/
    /* CCVM Tune Functions: 0x0400 - 0x04FF */
    /**************************************************************************/
    VM_FUNC_TUNE_SET                = 0x0400,
    VM_FUNC_TUNE_GET                = 0x0401,
    VM_FUNC_TUNE_REPORT_PROGRESS    = 0x0402,
}VM_FUNCS;

/**************************************************************************/
/* CCVM OBD2 Functions */
/**************************************************************************/ 

VM_FUNC_DEF(vm_obd2_read_memory_by_address);

/**************************************************************************/
/* CCVM Checksum Functions */
/**************************************************************************/ 

VM_FUNC_DEF(vm_checksum_handle_additive);
VM_FUNC_DEF(vm_checksum_handle_crc32);
VM_FUNC_DEF(vm_checksum_handle_rsa_me9uk);
VM_FUNC_DEF(vm_checksum_handle_md5);
VM_FUNC_DEF(vm_checksum_decrypt_rsa);

/**************************************************************************/
/* CCVM Filestock Functions */
/**************************************************************************/ 

VM_FUNC_DEF(vm_filestock_read);
VM_FUNC_DEF(vm_filestock_write);
VM_FUNC_DEF(vm_filestock_updateflashfile);
VM_FUNC_DEF(vm_filestock_fill);

/**************************************************************************/
/* CCVM Tune Functions */
/**************************************************************************/ 

VM_FUNC_DEF(vm_tune_set);
VM_FUNC_DEF(vm_tune_get);
VM_FUNC_DEF(vm_tune_report_progress);

#endif /* CCVM_FUNCS_H */
