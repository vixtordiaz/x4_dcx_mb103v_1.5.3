/*
 *  CCVM - Common Code Virtual Machine
 *
 *  Copyright 2014 SCT Performance, LLC
 * 
 */

/**
 * @file    ccvm_funcs_tune.c
 * @brief   Virtual machine tune handling functions
 * @details This file handles functions used to help tuning in CCVM.
 * @date    10/17/2014 
 * @author  Tristen Pierson
 */

#include "ccvm.h"
#include <stdio.h>
#include <string.h>
#include <stdint.h>
#include "ccvm_funcs.h"

/******************************************************************************/
/* Project Specific Includes */
/******************************************************************************/

#include <common/obd2tune.h>
#include <common/cmdif.h>

/******************************************************************************/
/* Global Variables */
/******************************************************************************/

extern uint8_t *vm_memory;  /* from ccvm_engine.c */
extern int32_t *vm_argv;    /* from ccvm_engine.c */
extern uint8_t vm_argc;     /* from ccvm_engine.c */
extern int32_t vm_ret;      /* from ccvm_engine.c */
extern flasher_info *flasherinfo;   /* from obd2tune.c */

/******************************************************************************/
/* Local Prototypes: CCVM Tune Functions */
/******************************************************************************/

static uint32_t vm_tune_get_set_helper(uint32_t parameter, uint32_t set_data, uint8_t operation);

/******************************************************************************/
/* Data structures */
/******************************************************************************/

typedef enum
{
    STATUS_PERCENTAGE = 0x0001,
} tune_parameter;

/******************************************************************************/
/* CCVM Tune Functions */
/******************************************************************************/

/**
 * @brief       <B>VM Function:</B> void tune_set(parameter, value)
 * @details     This function handles setting tune data
 *
 * @param[in,out]    vm      Virtual machine state structure
 * @param[in]        argc    Argument count
 * @param[in]        argv    Argument list
 *
 * @return      Entry point argument count
 */ 
VM_FUNC_DEF(vm_tune_set)
{    
    int32_t ret = 0;
    if (argc == 2)
    {        
        ret = vm_tune_get_set_helper(argv[0], argv[1], 1);        
    }
    else
    {
        vm->error = S_VM_FUNCARG;
        ret = 1;
    }
    return ret;
}

/**
 * @brief       <B>VM Function:</B> int32_t tune_get(parameter)
 * @details     This function handles reading tune data
 *
 * @param[in,out]    vm      Virtual machine state structure
 * @param[in]        argc    Argument count
 * @param[in]        argv    Argument list
 *
 * @return      Entry point argument count
 */ 
VM_FUNC_DEF(vm_tune_get)
{    
    int32_t ret = 0;
    if (argc == 1)
    {        
        ret = vm_tune_get_set_helper(argv[0], NULL, 0);        
    }
    else
    {
        vm->error = S_VM_FUNCARG;
        ret = 1;
    }
    return ret;
}

/**
 * @brief       <B>VM Function:</B> int32_t tune_report_progress(percentage, type)
 * @details     This function handles reporting progress.
 *              Type: 0 = Upload, 1 = Download
 *
 * @param[in,out]    vm      Virtual machine state structure
 * @param[in]        argc    Argument count
 * @param[in]        argv    Argument list
 *
 * @return      Entry point argument count
 */ 
VM_FUNC_DEF(vm_tune_report_progress)
{    
    int32_t ret = 0;
    uint8_t percentage;
    
    if (argc == 2 && argv != NULL)
    {        
        percentage = argv[0];
        if (argv[1] == 0)
        {
            /* Upload */
            cmdif_internalresponse_ack_nowait
                    (CMDIF_CMD_DO_UPLOAD, CMDIF_ACK_PROGRESSBAR,
                     &percentage,1);
        }
        else
        {
            /* Download */
            cmdif_internalresponse_ack_nowait
                    (CMDIF_CMD_DO_DOWNLOAD, CMDIF_ACK_PROGRESSBAR,
                     &percentage,1);
        }
    }
    else
    {
        vm->error = S_VM_FUNCARG;
        ret = 1;
    }
    return ret;
}

/**
 * @brief       Get/Set tune data helper
 * @details     This function is a helper designed to read or write to tune data
 *              structure (e.g. flasherinfo).
 *
 * @param[in]   parameter   data parameter
 * @param[in]   set_data    Data to set (not applicable if operation = 0)
 * @param[in]   operation   0 = Get Data, 1 = Set Data
 *
 * @return      Entry point argument count
 */ 
static uint32_t vm_tune_get_set_helper(uint32_t parameter, uint32_t set_data, uint8_t operation)
{
    uint32_t get_data;
    
    if (operation > 1)
    {
        return S_INPUT;
    }
    switch (parameter)
    {
    case STATUS_PERCENTAGE:
        if (operation)
        {
            flasherinfo->tune_programming_status.percentage = set_data;
        }
        else
        {
            get_data = flasherinfo->tune_programming_status.percentage;
        }
        break;
    default:
        return S_INPUT;
    }
    if (operation)
    {
        return S_SUCCESS;
    }
    else
    {
        return get_data;
    }
}
